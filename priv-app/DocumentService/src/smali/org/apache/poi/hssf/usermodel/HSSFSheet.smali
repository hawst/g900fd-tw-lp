.class public final Lorg/apache/poi/hssf/usermodel/HSSFSheet;
.super Ljava/lang/Object;
.source "HSSFSheet.java"

# interfaces
.implements Lorg/apache/poi/ss/usermodel/Sheet;


# static fields
.field private static final DEBUG:I = 0x1

.field public static final INITIAL_CAPACITY:I = 0x14

.field private static final log:Lorg/apache/poi/util/POILogger;


# instance fields
.field protected final _book:Lorg/apache/poi/hssf/model/InternalWorkbook;

.field private _firstrow:I

.field private _lastrow:I

.field private _patriarch:Lorg/apache/poi/hssf/usermodel/HSSFPatriarch;

.field private final _rows:Ljava/util/TreeMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/TreeMap",
            "<",
            "Ljava/lang/Integer;",
            "Lorg/apache/poi/hssf/usermodel/HSSFRow;",
            ">;"
        }
    .end annotation
.end field

.field private final _sheet:Lorg/apache/poi/hssf/model/InternalSheet;

.field protected final _workbook:Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 72
    const-class v0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;

    invoke-static {v0}, Lorg/apache/poi/util/POILogFactory;->getLogger(Ljava/lang/Class;)Lorg/apache/poi/util/POILogger;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->log:Lorg/apache/poi/util/POILogger;

    .line 80
    return-void
.end method

.method protected constructor <init>(Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;)V
    .locals 1
    .param p1, "workbook"    # Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;

    .prologue
    .line 103
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 104
    invoke-static {}, Lorg/apache/poi/hssf/model/InternalSheet;->createSheet()Lorg/apache/poi/hssf/model/InternalSheet;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_sheet:Lorg/apache/poi/hssf/model/InternalSheet;

    .line 105
    new-instance v0, Ljava/util/TreeMap;

    invoke-direct {v0}, Ljava/util/TreeMap;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_rows:Ljava/util/TreeMap;

    .line 106
    iput-object p1, p0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_workbook:Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;

    .line 107
    invoke-virtual {p1}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->getWorkbook()Lorg/apache/poi/hssf/model/InternalWorkbook;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_book:Lorg/apache/poi/hssf/model/InternalWorkbook;

    .line 108
    return-void
.end method

.method protected constructor <init>(Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;Lorg/apache/poi/hssf/model/InternalSheet;)V
    .locals 1
    .param p1, "workbook"    # Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;
    .param p2, "sheet"    # Lorg/apache/poi/hssf/model/InternalSheet;

    .prologue
    .line 118
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 119
    iput-object p2, p0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_sheet:Lorg/apache/poi/hssf/model/InternalSheet;

    .line 120
    new-instance v0, Ljava/util/TreeMap;

    invoke-direct {v0}, Ljava/util/TreeMap;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_rows:Ljava/util/TreeMap;

    .line 121
    iput-object p1, p0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_workbook:Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;

    .line 122
    invoke-virtual {p1}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->getWorkbook()Lorg/apache/poi/hssf/model/InternalWorkbook;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_book:Lorg/apache/poi/hssf/model/InternalWorkbook;

    .line 123
    invoke-direct {p0, p2}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->setPropertiesFromSheet(Lorg/apache/poi/hssf/model/InternalSheet;)V

    .line 124
    return-void
.end method

.method private addRow(Lorg/apache/poi/hssf/usermodel/HSSFRow;Z)V
    .locals 3
    .param p1, "row"    # Lorg/apache/poi/hssf/usermodel/HSSFRow;
    .param p2, "addLow"    # Z

    .prologue
    const/4 v0, 0x1

    .line 391
    iget-object v1, p0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_rows:Ljava/util/TreeMap;

    invoke-virtual {p1}, Lorg/apache/poi/hssf/usermodel/HSSFRow;->getRowNum()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2, p1}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 392
    if-eqz p2, :cond_0

    .line 393
    iget-object v1, p0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_sheet:Lorg/apache/poi/hssf/model/InternalSheet;

    invoke-virtual {p1}, Lorg/apache/poi/hssf/usermodel/HSSFRow;->getRowRecord()Lorg/apache/poi/hssf/record/RowRecord;

    move-result-object v2

    invoke-virtual {v1, v2}, Lorg/apache/poi/hssf/model/InternalSheet;->addRow(Lorg/apache/poi/hssf/record/RowRecord;)V

    .line 395
    :cond_0
    iget-object v1, p0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_rows:Ljava/util/TreeMap;

    invoke-virtual {v1}, Ljava/util/TreeMap;->size()I

    move-result v1

    if-ne v1, v0, :cond_5

    .line 396
    .local v0, "firstRow":Z
    :goto_0
    invoke-virtual {p1}, Lorg/apache/poi/hssf/usermodel/HSSFRow;->getRowNum()I

    move-result v1

    invoke-virtual {p0}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->getLastRowNum()I

    move-result v2

    if-gt v1, v2, :cond_1

    if-eqz v0, :cond_2

    .line 397
    :cond_1
    invoke-virtual {p1}, Lorg/apache/poi/hssf/usermodel/HSSFRow;->getRowNum()I

    move-result v1

    iput v1, p0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_lastrow:I

    .line 399
    :cond_2
    invoke-virtual {p1}, Lorg/apache/poi/hssf/usermodel/HSSFRow;->getRowNum()I

    move-result v1

    invoke-virtual {p0}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->getFirstRowNum()I

    move-result v2

    if-lt v1, v2, :cond_3

    if-eqz v0, :cond_4

    .line 400
    :cond_3
    invoke-virtual {p1}, Lorg/apache/poi/hssf/usermodel/HSSFRow;->getRowNum()I

    move-result v1

    iput v1, p0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_firstrow:I

    .line 402
    :cond_4
    return-void

    .line 395
    .end local v0    # "firstRow":Z
    :cond_5
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private checkRowtoAdd(Ljava/util/ArrayList;II)Z
    .locals 8
    .param p2, "rowNumber"    # I
    .param p3, "colNumber"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lorg/apache/poi/ss/util/CellRangeAddressBase;",
            ">;II)Z"
        }
    .end annotation

    .prologue
    .local p1, "cellRangeAddressBases":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/poi/ss/util/CellRangeAddressBase;>;"
    const/4 v6, 0x1

    .line 163
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 164
    .local v3, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/poi/ss/util/CellRangeAddressBase;>;"
    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-nez v7, :cond_1

    .line 181
    const/4 v6, 0x0

    :goto_0
    return v6

    .line 165
    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/ss/util/CellRangeAddress;

    .line 166
    .local v0, "cellRange":Lorg/apache/poi/ss/util/CellRangeAddress;
    invoke-virtual {v0}, Lorg/apache/poi/ss/util/CellRangeAddress;->getFirstRow()I

    move-result v2

    .line 167
    .local v2, "firstRow":I
    invoke-virtual {v0}, Lorg/apache/poi/ss/util/CellRangeAddress;->getLastRow()I

    move-result v5

    .line 168
    .local v5, "lastRow":I
    if-nez p3, :cond_2

    .line 169
    if-lt p2, v2, :cond_0

    if-gt p2, v5, :cond_0

    goto :goto_0

    .line 173
    :cond_2
    invoke-virtual {v0}, Lorg/apache/poi/ss/util/CellRangeAddress;->getFirstColumn()I

    move-result v1

    .line 174
    .local v1, "firstColumn":I
    invoke-virtual {v0}, Lorg/apache/poi/ss/util/CellRangeAddress;->getLastColumn()I

    move-result v4

    .line 175
    .local v4, "lastColumn":I
    if-lt p2, v2, :cond_0

    if-gt p2, v5, :cond_0

    .line 176
    if-lt p3, v1, :cond_0

    if-gt p3, v4, :cond_0

    goto :goto_0
.end method

.method private createRowFromRecord(Lorg/apache/poi/hssf/record/RowRecord;)Lorg/apache/poi/hssf/usermodel/HSSFRow;
    .locals 2
    .param p1, "row"    # Lorg/apache/poi/hssf/record/RowRecord;

    .prologue
    .line 306
    new-instance v0, Lorg/apache/poi/hssf/usermodel/HSSFRow;

    iget-object v1, p0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_workbook:Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;

    invoke-direct {v0, v1, p0, p1}, Lorg/apache/poi/hssf/usermodel/HSSFRow;-><init>(Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;Lorg/apache/poi/hssf/usermodel/HSSFSheet;Lorg/apache/poi/hssf/record/RowRecord;)V

    .line 308
    .local v0, "hrow":Lorg/apache/poi/hssf/usermodel/HSSFRow;
    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->addRow(Lorg/apache/poi/hssf/usermodel/HSSFRow;Z)V

    .line 309
    return-object v0
.end method

.method private findFirstRow(I)I
    .locals 3
    .param p1, "firstrow"    # I

    .prologue
    .line 371
    add-int/lit8 v1, p1, 0x1

    .line 372
    .local v1, "rownum":I
    invoke-virtual {p0, v1}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->getRow(I)Lorg/apache/poi/hssf/usermodel/HSSFRow;

    move-result-object v0

    .line 374
    .local v0, "r":Lorg/apache/poi/hssf/usermodel/HSSFRow;
    :goto_0
    if-nez v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->getLastRowNum()I

    move-result v2

    if-le v1, v2, :cond_2

    .line 378
    :cond_0
    invoke-virtual {p0}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->getLastRowNum()I

    move-result v2

    if-le v1, v2, :cond_1

    .line 379
    const/4 v1, 0x0

    .line 381
    .end local v1    # "rownum":I
    :cond_1
    return v1

    .line 375
    .restart local v1    # "rownum":I
    :cond_2
    add-int/lit8 v1, v1, 0x1

    invoke-virtual {p0, v1}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->getRow(I)Lorg/apache/poi/hssf/usermodel/HSSFRow;

    move-result-object v0

    goto :goto_0
.end method

.method private findLastRow(I)I
    .locals 4
    .param p1, "lastrow"    # I

    .prologue
    const/4 v2, 0x0

    .line 351
    const/4 v3, 0x1

    if-ge p1, v3, :cond_1

    move v1, v2

    .line 363
    :cond_0
    :goto_0
    return v1

    .line 354
    :cond_1
    add-int/lit8 v1, p1, -0x1

    .line 355
    .local v1, "rownum":I
    invoke-virtual {p0, v1}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->getRow(I)Lorg/apache/poi/hssf/usermodel/HSSFRow;

    move-result-object v0

    .line 357
    .local v0, "r":Lorg/apache/poi/hssf/usermodel/HSSFRow;
    :goto_1
    if-nez v0, :cond_2

    if-gtz v1, :cond_3

    .line 360
    :cond_2
    if-nez v0, :cond_0

    move v1, v2

    .line 361
    goto :goto_0

    .line 358
    :cond_3
    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p0, v1}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->getRow(I)Lorg/apache/poi/hssf/usermodel/HSSFRow;

    move-result-object v0

    goto :goto_1
.end method

.method private getBuiltinNameRecord(B)Lorg/apache/poi/hssf/record/NameRecord;
    .locals 3
    .param p1, "builtinCode"    # B

    .prologue
    .line 2326
    iget-object v2, p0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_workbook:Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;

    invoke-virtual {v2, p0}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->getSheetIndex(Lorg/apache/poi/ss/usermodel/Sheet;)I

    move-result v1

    .line 2328
    .local v1, "sheetIndex":I
    iget-object v2, p0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_workbook:Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;

    invoke-virtual {v2, v1, p1}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->findExistingBuiltinNameRecordIdx(IB)I

    move-result v0

    .line 2329
    .local v0, "recIndex":I
    const/4 v2, -0x1

    if-ne v0, v2, :cond_0

    .line 2330
    const/4 v2, 0x0

    .line 2332
    :goto_0
    return-object v2

    :cond_0
    iget-object v2, p0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_workbook:Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;

    invoke-virtual {v2, v0}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->getNameRecord(I)Lorg/apache/poi/hssf/record/NameRecord;

    move-result-object v2

    goto :goto_0
.end method

.method private getCellRange(Lorg/apache/poi/ss/util/CellRangeAddress;)Lorg/apache/poi/ss/usermodel/CellRange;
    .locals 12
    .param p1, "range"    # Lorg/apache/poi/ss/util/CellRangeAddress;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/poi/ss/util/CellRangeAddress;",
            ")",
            "Lorg/apache/poi/ss/usermodel/CellRange",
            "<",
            "Lorg/apache/poi/hssf/usermodel/HSSFCell;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2055
    invoke-virtual {p1}, Lorg/apache/poi/ss/util/CellRangeAddress;->getFirstRow()I

    move-result v0

    .line 2056
    .local v0, "firstRow":I
    invoke-virtual {p1}, Lorg/apache/poi/ss/util/CellRangeAddress;->getFirstColumn()I

    move-result v1

    .line 2057
    .local v1, "firstColumn":I
    invoke-virtual {p1}, Lorg/apache/poi/ss/util/CellRangeAddress;->getLastRow()I

    move-result v9

    .line 2058
    .local v9, "lastRow":I
    invoke-virtual {p1}, Lorg/apache/poi/ss/util/CellRangeAddress;->getLastColumn()I

    move-result v8

    .line 2059
    .local v8, "lastColumn":I
    sub-int v5, v9, v0

    add-int/lit8 v2, v5, 0x1

    .line 2060
    .local v2, "height":I
    sub-int v5, v8, v1

    add-int/lit8 v3, v5, 0x1

    .line 2061
    .local v3, "width":I
    new-instance v4, Ljava/util/ArrayList;

    mul-int v5, v2, v3

    invoke-direct {v4, v5}, Ljava/util/ArrayList;-><init>(I)V

    .line 2062
    .local v4, "temp":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/hssf/usermodel/HSSFCell;>;"
    move v11, v0

    .local v11, "rowIn":I
    :goto_0
    if-le v11, v9, :cond_0

    .line 2075
    const-class v5, Lorg/apache/poi/hssf/usermodel/HSSFCell;

    invoke-static/range {v0 .. v5}, Lorg/apache/poi/ss/util/SSCellRange;->create(IIIILjava/util/List;Ljava/lang/Class;)Lorg/apache/poi/ss/util/SSCellRange;

    move-result-object v5

    return-object v5

    .line 2063
    :cond_0
    move v7, v1

    .local v7, "colIn":I
    :goto_1
    if-le v7, v8, :cond_1

    .line 2062
    add-int/lit8 v11, v11, 0x1

    goto :goto_0

    .line 2064
    :cond_1
    invoke-virtual {p0, v11}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->getRow(I)Lorg/apache/poi/hssf/usermodel/HSSFRow;

    move-result-object v10

    .line 2065
    .local v10, "row":Lorg/apache/poi/hssf/usermodel/HSSFRow;
    if-nez v10, :cond_2

    .line 2066
    invoke-virtual {p0, v11}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->createRow(I)Lorg/apache/poi/hssf/usermodel/HSSFRow;

    move-result-object v10

    .line 2068
    :cond_2
    invoke-virtual {v10, v7}, Lorg/apache/poi/hssf/usermodel/HSSFRow;->getCell(I)Lorg/apache/poi/hssf/usermodel/HSSFCell;

    move-result-object v6

    .line 2069
    .local v6, "cell":Lorg/apache/poi/hssf/usermodel/HSSFCell;
    if-nez v6, :cond_3

    .line 2070
    invoke-virtual {v10, v7}, Lorg/apache/poi/hssf/usermodel/HSSFRow;->createCell(I)Lorg/apache/poi/hssf/usermodel/HSSFCell;

    move-result-object v6

    .line 2072
    :cond_3
    invoke-interface {v4, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2063
    add-int/lit8 v7, v7, 0x1

    goto :goto_1
.end method

.method private getPatriarch(Z)Lorg/apache/poi/hssf/usermodel/HSSFPatriarch;
    .locals 7
    .param p1, "createIfMissing"    # Z

    .prologue
    const/4 v4, 0x0

    .line 1878
    const/4 v2, 0x0

    .line 1879
    .local v2, "patriarch":Lorg/apache/poi/hssf/usermodel/HSSFPatriarch;
    iget-object v5, p0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_patriarch:Lorg/apache/poi/hssf/usermodel/HSSFPatriarch;

    if-eqz v5, :cond_1

    .line 1880
    iget-object v4, p0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_patriarch:Lorg/apache/poi/hssf/usermodel/HSSFPatriarch;

    .line 1907
    :cond_0
    :goto_0
    return-object v4

    .line 1882
    :cond_1
    iget-object v5, p0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_book:Lorg/apache/poi/hssf/model/InternalWorkbook;

    invoke-virtual {v5}, Lorg/apache/poi/hssf/model/InternalWorkbook;->findDrawingGroup()Lorg/apache/poi/hssf/model/DrawingManager2;

    move-result-object v1

    .line 1883
    .local v1, "dm":Lorg/apache/poi/hssf/model/DrawingManager2;
    if-nez v1, :cond_2

    .line 1884
    if-eqz p1, :cond_0

    .line 1887
    iget-object v5, p0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_book:Lorg/apache/poi/hssf/model/InternalWorkbook;

    invoke-virtual {v5}, Lorg/apache/poi/hssf/model/InternalWorkbook;->createDrawingGroup()V

    .line 1888
    iget-object v5, p0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_book:Lorg/apache/poi/hssf/model/InternalWorkbook;

    invoke-virtual {v5}, Lorg/apache/poi/hssf/model/InternalWorkbook;->getDrawingManager()Lorg/apache/poi/hssf/model/DrawingManager2;

    move-result-object v1

    .line 1891
    :cond_2
    iget-object v5, p0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_sheet:Lorg/apache/poi/hssf/model/InternalSheet;

    const/16 v6, 0x2694

    invoke-virtual {v5, v6}, Lorg/apache/poi/hssf/model/InternalSheet;->findFirstRecordBySid(S)Lorg/apache/poi/hssf/record/Record;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hssf/record/EscherAggregate;

    .line 1892
    .local v0, "agg":Lorg/apache/poi/hssf/record/EscherAggregate;
    if-nez v0, :cond_4

    .line 1893
    iget-object v5, p0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_sheet:Lorg/apache/poi/hssf/model/InternalSheet;

    const/4 v6, 0x0

    invoke-virtual {v5, v1, v6}, Lorg/apache/poi/hssf/model/InternalSheet;->aggregateDrawingRecords(Lorg/apache/poi/hssf/model/DrawingManager2;Z)I

    move-result v3

    .line 1894
    .local v3, "pos":I
    const/4 v5, -0x1

    if-ne v5, v3, :cond_3

    .line 1895
    if-eqz p1, :cond_0

    .line 1896
    iget-object v4, p0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_sheet:Lorg/apache/poi/hssf/model/InternalSheet;

    const/4 v5, 0x1

    invoke-virtual {v4, v1, v5}, Lorg/apache/poi/hssf/model/InternalSheet;->aggregateDrawingRecords(Lorg/apache/poi/hssf/model/DrawingManager2;Z)I

    move-result v3

    .line 1897
    iget-object v4, p0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_sheet:Lorg/apache/poi/hssf/model/InternalSheet;

    invoke-virtual {v4}, Lorg/apache/poi/hssf/model/InternalSheet;->getRecords()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "agg":Lorg/apache/poi/hssf/record/EscherAggregate;
    check-cast v0, Lorg/apache/poi/hssf/record/EscherAggregate;

    .line 1898
    .restart local v0    # "agg":Lorg/apache/poi/hssf/record/EscherAggregate;
    new-instance v2, Lorg/apache/poi/hssf/usermodel/HSSFPatriarch;

    .end local v2    # "patriarch":Lorg/apache/poi/hssf/usermodel/HSSFPatriarch;
    invoke-direct {v2, p0, v0}, Lorg/apache/poi/hssf/usermodel/HSSFPatriarch;-><init>(Lorg/apache/poi/hssf/usermodel/HSSFSheet;Lorg/apache/poi/hssf/record/EscherAggregate;)V

    .line 1899
    .restart local v2    # "patriarch":Lorg/apache/poi/hssf/usermodel/HSSFPatriarch;
    invoke-virtual {v2}, Lorg/apache/poi/hssf/usermodel/HSSFPatriarch;->afterCreate()V

    move-object v4, v2

    .line 1900
    goto :goto_0

    .line 1905
    :cond_3
    iget-object v4, p0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_sheet:Lorg/apache/poi/hssf/model/InternalSheet;

    invoke-virtual {v4}, Lorg/apache/poi/hssf/model/InternalSheet;->getRecords()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "agg":Lorg/apache/poi/hssf/record/EscherAggregate;
    check-cast v0, Lorg/apache/poi/hssf/record/EscherAggregate;

    .line 1907
    .end local v3    # "pos":I
    .restart local v0    # "agg":Lorg/apache/poi/hssf/record/EscherAggregate;
    :cond_4
    new-instance v4, Lorg/apache/poi/hssf/usermodel/HSSFPatriarch;

    invoke-direct {v4, p0, v0}, Lorg/apache/poi/hssf/usermodel/HSSFPatriarch;-><init>(Lorg/apache/poi/hssf/usermodel/HSSFSheet;Lorg/apache/poi/hssf/record/EscherAggregate;)V

    goto :goto_0
.end method

.method private getProtectionBlock()Lorg/apache/poi/hssf/record/aggregates/WorksheetProtectionBlock;
    .locals 1

    .prologue
    .line 1217
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_sheet:Lorg/apache/poi/hssf/model/InternalSheet;

    invoke-virtual {v0}, Lorg/apache/poi/hssf/model/InternalSheet;->getProtectionBlock()Lorg/apache/poi/hssf/record/aggregates/WorksheetProtectionBlock;

    move-result-object v0

    return-object v0
.end method

.method private getRepeatingRowsOrColums(Z)Lorg/apache/poi/ss/util/CellRangeAddress;
    .locals 12
    .param p1, "rows"    # Z

    .prologue
    const/4 v7, 0x0

    const/4 v11, -0x1

    .line 2283
    const/4 v8, 0x7

    invoke-direct {p0, v8}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->getBuiltinNameRecord(B)Lorg/apache/poi/hssf/record/NameRecord;

    move-result-object v6

    .line 2284
    .local v6, "rec":Lorg/apache/poi/hssf/record/NameRecord;
    if-nez v6, :cond_1

    .line 2321
    :cond_0
    :goto_0
    return-object v7

    .line 2288
    :cond_1
    invoke-virtual {v6}, Lorg/apache/poi/hssf/record/NameRecord;->getNameDefinition()[Lorg/apache/poi/ss/formula/ptg/Ptg;

    move-result-object v4

    .line 2289
    .local v4, "nameDefinition":[Lorg/apache/poi/ss/formula/ptg/Ptg;
    if-eqz v4, :cond_0

    .line 2293
    sget-object v8, Lorg/apache/poi/ss/SpreadsheetVersion;->EXCEL97:Lorg/apache/poi/ss/SpreadsheetVersion;

    invoke-virtual {v8}, Lorg/apache/poi/ss/SpreadsheetVersion;->getLastRowIndex()I

    move-result v3

    .line 2294
    .local v3, "maxRowIndex":I
    sget-object v8, Lorg/apache/poi/ss/SpreadsheetVersion;->EXCEL97:Lorg/apache/poi/ss/SpreadsheetVersion;

    invoke-virtual {v8}, Lorg/apache/poi/ss/SpreadsheetVersion;->getLastColumnIndex()I

    move-result v2

    .line 2296
    .local v2, "maxColIndex":I
    array-length v9, v4

    const/4 v8, 0x0

    :goto_1
    if-ge v8, v9, :cond_0

    aget-object v5, v4, v8

    .line 2298
    .local v5, "ptg":Lorg/apache/poi/ss/formula/ptg/Ptg;
    instance-of v10, v5, Lorg/apache/poi/ss/formula/ptg/Area3DPtg;

    if-eqz v10, :cond_3

    move-object v0, v5

    .line 2299
    check-cast v0, Lorg/apache/poi/ss/formula/ptg/Area3DPtg;

    .line 2301
    .local v0, "areaPtg":Lorg/apache/poi/ss/formula/ptg/Area3DPtg;
    invoke-virtual {v0}, Lorg/apache/poi/ss/formula/ptg/Area3DPtg;->getFirstColumn()I

    move-result v10

    if-nez v10, :cond_2

    .line 2302
    invoke-virtual {v0}, Lorg/apache/poi/ss/formula/ptg/Area3DPtg;->getLastColumn()I

    move-result v10

    if-ne v10, v2, :cond_2

    .line 2303
    if-eqz p1, :cond_3

    .line 2304
    new-instance v7, Lorg/apache/poi/ss/util/CellRangeAddress;

    .line 2305
    invoke-virtual {v0}, Lorg/apache/poi/ss/formula/ptg/Area3DPtg;->getFirstRow()I

    move-result v8

    invoke-virtual {v0}, Lorg/apache/poi/ss/formula/ptg/Area3DPtg;->getLastRow()I

    move-result v9

    .line 2304
    invoke-direct {v7, v8, v9, v11, v11}, Lorg/apache/poi/ss/util/CellRangeAddress;-><init>(IIII)V

    .line 2306
    .local v7, "rowRange":Lorg/apache/poi/ss/util/CellRangeAddress;
    goto :goto_0

    .line 2308
    .end local v7    # "rowRange":Lorg/apache/poi/ss/util/CellRangeAddress;
    :cond_2
    invoke-virtual {v0}, Lorg/apache/poi/ss/formula/ptg/Area3DPtg;->getFirstRow()I

    move-result v10

    if-nez v10, :cond_3

    .line 2309
    invoke-virtual {v0}, Lorg/apache/poi/ss/formula/ptg/Area3DPtg;->getLastRow()I

    move-result v10

    if-ne v10, v3, :cond_3

    .line 2310
    if-nez p1, :cond_3

    .line 2311
    new-instance v1, Lorg/apache/poi/ss/util/CellRangeAddress;

    .line 2312
    invoke-virtual {v0}, Lorg/apache/poi/ss/formula/ptg/Area3DPtg;->getFirstColumn()I

    move-result v8

    invoke-virtual {v0}, Lorg/apache/poi/ss/formula/ptg/Area3DPtg;->getLastColumn()I

    move-result v9

    .line 2311
    invoke-direct {v1, v11, v11, v8, v9}, Lorg/apache/poi/ss/util/CellRangeAddress;-><init>(IIII)V

    .local v1, "columnRange":Lorg/apache/poi/ss/util/CellRangeAddress;
    move-object v7, v1

    .line 2313
    goto :goto_0

    .line 2296
    .end local v0    # "areaPtg":Lorg/apache/poi/ss/formula/ptg/Area3DPtg;
    .end local v1    # "columnRange":Lorg/apache/poi/ss/util/CellRangeAddress;
    :cond_3
    add-int/lit8 v8, v8, 0x1

    goto :goto_1
.end method

.method private lookForComment(Lorg/apache/poi/hssf/usermodel/HSSFShapeContainer;II)Lorg/apache/poi/hssf/usermodel/HSSFComment;
    .locals 6
    .param p1, "container"    # Lorg/apache/poi/hssf/usermodel/HSSFShapeContainer;
    .param p2, "row"    # I
    .param p3, "column"    # I

    .prologue
    .line 2162
    invoke-interface {p1}, Lorg/apache/poi/hssf/usermodel/HSSFShapeContainer;->getChildren()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_1

    .line 2178
    const/4 v2, 0x0

    :goto_0
    return-object v2

    .line 2162
    :cond_1
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .local v1, "object":Ljava/lang/Object;
    move-object v3, v1

    .line 2163
    check-cast v3, Lorg/apache/poi/hssf/usermodel/HSSFShape;

    .line 2164
    .local v3, "shape":Lorg/apache/poi/hssf/usermodel/HSSFShape;
    instance-of v5, v3, Lorg/apache/poi/hssf/usermodel/HSSFShapeGroup;

    if-eqz v5, :cond_2

    .line 2165
    check-cast v3, Lorg/apache/poi/hssf/usermodel/HSSFShapeContainer;

    .end local v3    # "shape":Lorg/apache/poi/hssf/usermodel/HSSFShape;
    invoke-direct {p0, v3, p2, p3}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->lookForComment(Lorg/apache/poi/hssf/usermodel/HSSFShapeContainer;II)Lorg/apache/poi/hssf/usermodel/HSSFComment;

    move-result-object v2

    .line 2166
    .local v2, "res":Lorg/apache/poi/hssf/usermodel/HSSFShape;
    if-eqz v2, :cond_0

    .line 2167
    check-cast v2, Lorg/apache/poi/hssf/usermodel/HSSFComment;

    goto :goto_0

    .line 2171
    .end local v2    # "res":Lorg/apache/poi/hssf/usermodel/HSSFShape;
    .restart local v3    # "shape":Lorg/apache/poi/hssf/usermodel/HSSFShape;
    :cond_2
    instance-of v5, v3, Lorg/apache/poi/hssf/usermodel/HSSFComment;

    if-eqz v5, :cond_0

    move-object v0, v3

    .line 2172
    check-cast v0, Lorg/apache/poi/hssf/usermodel/HSSFComment;

    .line 2173
    .local v0, "comment":Lorg/apache/poi/hssf/usermodel/HSSFComment;
    invoke-virtual {v0}, Lorg/apache/poi/hssf/usermodel/HSSFComment;->getColumn()I

    move-result v5

    if-ne v5, p3, :cond_0

    invoke-virtual {v0}, Lorg/apache/poi/hssf/usermodel/HSSFComment;->getRow()I

    move-result v5

    if-ne v5, p2, :cond_0

    move-object v2, v0

    .line 2174
    goto :goto_0
.end method

.method private notifyRowShifting(Lorg/apache/poi/hssf/usermodel/HSSFRow;)V
    .locals 5
    .param p1, "row"    # Lorg/apache/poi/hssf/usermodel/HSSFRow;

    .prologue
    .line 1576
    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "Row[rownum="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lorg/apache/poi/hssf/usermodel/HSSFRow;->getRowNum()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "] contains cell(s) included in a multi-cell array formula. "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 1577
    const-string/jumbo v4, "You cannot change part of an array."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 1576
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1578
    .local v2, "msg":Ljava/lang/String;
    invoke-virtual {p1}, Lorg/apache/poi/hssf/usermodel/HSSFRow;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_1

    .line 1584
    return-void

    .line 1578
    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/ss/usermodel/Cell;

    .local v0, "cell":Lorg/apache/poi/ss/usermodel/Cell;
    move-object v1, v0

    .line 1579
    check-cast v1, Lorg/apache/poi/hssf/usermodel/HSSFCell;

    .line 1580
    .local v1, "hcell":Lorg/apache/poi/hssf/usermodel/HSSFCell;
    invoke-virtual {v1}, Lorg/apache/poi/hssf/usermodel/HSSFCell;->isPartOfArrayFormulaGroup()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1581
    invoke-virtual {v1, v2}, Lorg/apache/poi/hssf/usermodel/HSSFCell;->notifyArrayFormulaChanging(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private setPropertiesFromSheet(Lorg/apache/poi/hssf/model/InternalSheet;)V
    .locals 18
    .param p1, "sheet"    # Lorg/apache/poi/hssf/model/InternalSheet;

    .prologue
    .line 189
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hssf/model/InternalSheet;->getNextRow()Lorg/apache/poi/hssf/record/RowRecord;

    move-result-object v8

    .line 192
    .local v8, "row":Lorg/apache/poi/hssf/record/RowRecord;
    :goto_0
    if-nez v8, :cond_4

    .line 205
    :cond_0
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hssf/model/InternalSheet;->getCellValueIterator()Ljava/util/Iterator;

    move-result-object v6

    .line 206
    .local v6, "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/poi/hssf/record/CellValueRecordInterface;>;"
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    .line 208
    .local v10, "timestart":J
    sget-object v12, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->log:Lorg/apache/poi/util/POILogger;

    const/4 v13, 0x1

    invoke-virtual {v12, v13}, Lorg/apache/poi/util/POILogger;->check(I)Z

    move-result v12

    if-eqz v12, :cond_1

    .line 209
    sget-object v12, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->log:Lorg/apache/poi/util/POILogger;

    const/4 v13, 0x1

    const-string/jumbo v14, "Time at start of cell creating in HSSF sheet = "

    .line 210
    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v15

    .line 209
    invoke-virtual {v12, v13, v14, v15}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;Ljava/lang/Object;)V

    .line 212
    :cond_1
    const/4 v7, 0x0

    .line 215
    .local v7, "lastrow":Lorg/apache/poi/hssf/usermodel/HSSFRow;
    :cond_2
    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-nez v12, :cond_7

    .line 271
    sget-object v12, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->log:Lorg/apache/poi/util/POILogger;

    const/4 v13, 0x1

    invoke-virtual {v12, v13}, Lorg/apache/poi/util/POILogger;->check(I)Z

    move-result v12

    if-eqz v12, :cond_3

    .line 272
    sget-object v12, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->log:Lorg/apache/poi/util/POILogger;

    const/4 v13, 0x1

    const-string/jumbo v14, "total sheet cell creation took "

    .line 273
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v16

    sub-long v16, v16, v10

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v15

    .line 272
    invoke-virtual {v12, v13, v14, v15}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;Ljava/lang/Object;)V

    .line 275
    :cond_3
    return-void

    .line 193
    .end local v6    # "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/poi/hssf/record/CellValueRecordInterface;>;"
    .end local v7    # "lastrow":Lorg/apache/poi/hssf/usermodel/HSSFRow;
    .end local v10    # "timestart":J
    :cond_4
    invoke-virtual {v8}, Lorg/apache/poi/hssf/record/RowRecord;->getRowNumber()I

    move-result v12

    const/16 v13, 0x14

    if-gt v12, v13, :cond_6

    .line 194
    move-object/from16 v0, p0

    invoke-direct {v0, v8}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->createRowFromRecord(Lorg/apache/poi/hssf/record/RowRecord;)Lorg/apache/poi/hssf/usermodel/HSSFRow;

    .line 202
    :cond_5
    :goto_2
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hssf/model/InternalSheet;->getNextRow()Lorg/apache/poi/hssf/record/RowRecord;

    move-result-object v8

    goto :goto_0

    .line 195
    :cond_6
    move-object/from16 v0, p1

    iget-object v12, v0, Lorg/apache/poi/hssf/model/InternalSheet;->_cellRangeAddressBases:Ljava/util/ArrayList;

    if-eqz v12, :cond_0

    .line 196
    move-object/from16 v0, p1

    iget-object v12, v0, Lorg/apache/poi/hssf/model/InternalSheet;->_cellRangeAddressBases:Ljava/util/ArrayList;

    invoke-virtual {v12}, Ljava/util/ArrayList;->size()I

    move-result v12

    if-eqz v12, :cond_0

    .line 198
    move-object/from16 v0, p1

    iget-object v12, v0, Lorg/apache/poi/hssf/model/InternalSheet;->_cellRangeAddressBases:Ljava/util/ArrayList;

    .line 199
    invoke-virtual {v8}, Lorg/apache/poi/hssf/record/RowRecord;->getRowNumber()I

    move-result v13

    const/4 v14, 0x0

    .line 198
    move-object/from16 v0, p0

    invoke-direct {v0, v12, v13, v14}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->checkRowtoAdd(Ljava/util/ArrayList;II)Z

    move-result v12

    .line 199
    if-eqz v12, :cond_5

    .line 200
    move-object/from16 v0, p0

    invoke-direct {v0, v8}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->createRowFromRecord(Lorg/apache/poi/hssf/record/RowRecord;)Lorg/apache/poi/hssf/usermodel/HSSFRow;

    goto :goto_2

    .line 216
    .restart local v6    # "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/poi/hssf/record/CellValueRecordInterface;>;"
    .restart local v7    # "lastrow":Lorg/apache/poi/hssf/usermodel/HSSFRow;
    .restart local v10    # "timestart":J
    :cond_7
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/poi/hssf/record/CellValueRecordInterface;

    .line 229
    .local v4, "cval":Lorg/apache/poi/hssf/record/CellValueRecordInterface;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 230
    .local v2, "cellstart":J
    move-object v5, v7

    .line 233
    .local v5, "hrow":Lorg/apache/poi/hssf/usermodel/HSSFRow;
    if-eqz v5, :cond_8

    .line 234
    if-eqz v4, :cond_9

    invoke-virtual {v5}, Lorg/apache/poi/hssf/usermodel/HSSFRow;->getRowNum()I

    move-result v12

    invoke-interface {v4}, Lorg/apache/poi/hssf/record/CellValueRecordInterface;->getRow()I

    move-result v13

    if-eq v12, v13, :cond_9

    .line 235
    :cond_8
    invoke-interface {v4}, Lorg/apache/poi/hssf/record/CellValueRecordInterface;->getRow()I

    move-result v12

    move-object/from16 v0, p0

    invoke-virtual {v0, v12}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->getRow(I)Lorg/apache/poi/hssf/usermodel/HSSFRow;

    move-result-object v5

    .line 236
    move-object v7, v5

    .line 237
    if-nez v5, :cond_9

    .line 249
    new-instance v9, Lorg/apache/poi/hssf/record/RowRecord;

    invoke-interface {v4}, Lorg/apache/poi/hssf/record/CellValueRecordInterface;->getRow()I

    move-result v12

    invoke-direct {v9, v12}, Lorg/apache/poi/hssf/record/RowRecord;-><init>(I)V

    .line 250
    .local v9, "rowRec":Lorg/apache/poi/hssf/record/RowRecord;
    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, Lorg/apache/poi/hssf/model/InternalSheet;->addRow(Lorg/apache/poi/hssf/record/RowRecord;)V

    .line 251
    move-object/from16 v0, p0

    invoke-direct {v0, v9}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->createRowFromRecord(Lorg/apache/poi/hssf/record/RowRecord;)Lorg/apache/poi/hssf/usermodel/HSSFRow;

    move-result-object v5

    .line 254
    .end local v9    # "rowRec":Lorg/apache/poi/hssf/record/RowRecord;
    :cond_9
    sget-object v12, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->log:Lorg/apache/poi/util/POILogger;

    const/4 v13, 0x1

    invoke-virtual {v12, v13}, Lorg/apache/poi/util/POILogger;->check(I)Z

    move-result v12

    if-eqz v12, :cond_a

    .line 255
    instance-of v12, v4, Lorg/apache/poi/hssf/record/Record;

    if-eqz v12, :cond_b

    .line 256
    sget-object v13, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->log:Lorg/apache/poi/util/POILogger;

    const/4 v14, 0x1

    .line 257
    new-instance v15, Ljava/lang/StringBuilder;

    const-string/jumbo v12, "record id = "

    invoke-direct {v15, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object v12, v4

    .line 258
    check-cast v12, Lorg/apache/poi/hssf/record/Record;

    .line 259
    invoke-virtual {v12}, Lorg/apache/poi/hssf/record/Record;->getSid()S

    move-result v12

    .line 258
    invoke-static {v12}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v15, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    .line 257
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    .line 256
    invoke-virtual {v13, v14, v12}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;)V

    .line 264
    :cond_a
    :goto_3
    invoke-virtual {v5, v4}, Lorg/apache/poi/hssf/usermodel/HSSFRow;->createCellFromRecord(Lorg/apache/poi/hssf/record/CellValueRecordInterface;)Lorg/apache/poi/hssf/usermodel/HSSFCell;

    .line 265
    sget-object v12, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->log:Lorg/apache/poi/util/POILogger;

    const/4 v13, 0x1

    invoke-virtual {v12, v13}, Lorg/apache/poi/util/POILogger;->check(I)Z

    move-result v12

    if-eqz v12, :cond_2

    .line 266
    sget-object v12, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->log:Lorg/apache/poi/util/POILogger;

    const/4 v13, 0x1

    const-string/jumbo v14, "record took "

    .line 267
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v16

    sub-long v16, v16, v2

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v15

    .line 266
    invoke-virtual {v12, v13, v14, v15}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 261
    :cond_b
    sget-object v12, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->log:Lorg/apache/poi/util/POILogger;

    const/4 v13, 0x1

    new-instance v14, Ljava/lang/StringBuilder;

    const-string/jumbo v15, "record = "

    invoke-direct {v14, v15}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v14, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v12, v13, v14}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;)V

    goto :goto_3
.end method

.method private setRepeatingRowsAndColumns(Lorg/apache/poi/ss/util/CellRangeAddress;Lorg/apache/poi/ss/util/CellRangeAddress;)V
    .locals 30
    .param p1, "rowDef"    # Lorg/apache/poi/ss/util/CellRangeAddress;
    .param p2, "colDef"    # Lorg/apache/poi/ss/util/CellRangeAddress;

    .prologue
    .line 2206
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_workbook:Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;

    move-object/from16 v0, p0

    invoke-virtual {v3, v0}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->getSheetIndex(Lorg/apache/poi/ss/usermodel/Sheet;)I

    move-result v29

    .line 2207
    .local v29, "sheetIndex":I
    sget-object v3, Lorg/apache/poi/ss/SpreadsheetVersion;->EXCEL97:Lorg/apache/poi/ss/SpreadsheetVersion;

    invoke-virtual {v3}, Lorg/apache/poi/ss/SpreadsheetVersion;->getLastRowIndex()I

    move-result v4

    .line 2208
    .local v4, "maxRowIndex":I
    sget-object v3, Lorg/apache/poi/ss/SpreadsheetVersion;->EXCEL97:Lorg/apache/poi/ss/SpreadsheetVersion;

    invoke-virtual {v3}, Lorg/apache/poi/ss/SpreadsheetVersion;->getLastColumnIndex()I

    move-result v16

    .line 2210
    .local v16, "maxColIndex":I
    const/4 v5, -0x1

    .line 2211
    .local v5, "col1":I
    const/4 v6, -0x1

    .line 2212
    .local v6, "col2":I
    const/4 v13, -0x1

    .line 2213
    .local v13, "row1":I
    const/4 v14, -0x1

    .line 2215
    .local v14, "row2":I
    if-eqz p1, :cond_2

    .line 2216
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/ss/util/CellRangeAddress;->getFirstRow()I

    move-result v13

    .line 2217
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/ss/util/CellRangeAddress;->getLastRow()I

    move-result v14

    .line 2218
    const/4 v3, -0x1

    if-ne v13, v3, :cond_0

    const/4 v3, -0x1

    if-ne v14, v3, :cond_1

    :cond_0
    if-gt v13, v14, :cond_1

    .line 2219
    if-ltz v13, :cond_1

    if-gt v13, v4, :cond_1

    .line 2220
    if-ltz v14, :cond_1

    if-le v14, v4, :cond_2

    .line 2221
    :cond_1
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v7, "Invalid row range specification"

    invoke-direct {v3, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 2224
    :cond_2
    if-eqz p2, :cond_5

    .line 2225
    invoke-virtual/range {p2 .. p2}, Lorg/apache/poi/ss/util/CellRangeAddress;->getFirstColumn()I

    move-result v5

    .line 2226
    invoke-virtual/range {p2 .. p2}, Lorg/apache/poi/ss/util/CellRangeAddress;->getLastColumn()I

    move-result v6

    .line 2227
    const/4 v3, -0x1

    if-ne v5, v3, :cond_3

    const/4 v3, -0x1

    if-ne v6, v3, :cond_4

    :cond_3
    if-gt v5, v6, :cond_4

    .line 2228
    if-ltz v5, :cond_4

    move/from16 v0, v16

    if-gt v5, v0, :cond_4

    .line 2229
    if-ltz v6, :cond_4

    move/from16 v0, v16

    if-le v6, v0, :cond_5

    .line 2230
    :cond_4
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v7, "Invalid column range specification"

    invoke-direct {v3, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 2235
    :cond_5
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_workbook:Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;

    invoke-virtual {v3}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->getWorkbook()Lorg/apache/poi/hssf/model/InternalWorkbook;

    move-result-object v3

    move/from16 v0, v29

    invoke-virtual {v3, v0}, Lorg/apache/poi/hssf/model/InternalWorkbook;->checkExternSheet(I)S

    move-result v11

    .line 2237
    .local v11, "externSheetIndex":S
    if-eqz p1, :cond_7

    if-eqz p2, :cond_7

    const/16 v28, 0x1

    .line 2238
    .local v28, "setBoth":Z
    :goto_0
    if-nez p1, :cond_8

    if-nez p2, :cond_8

    const/16 v27, 0x1

    .line 2240
    .local v27, "removeAll":Z
    :goto_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_workbook:Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;

    .line 2241
    const/4 v7, 0x7

    .line 2240
    move/from16 v0, v29

    invoke-virtual {v3, v7, v0}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->getBuiltInName(BI)Lorg/apache/poi/hssf/usermodel/HSSFName;

    move-result-object v23

    .line 2242
    .local v23, "name":Lorg/apache/poi/hssf/usermodel/HSSFName;
    if-eqz v27, :cond_9

    .line 2243
    if-eqz v23, :cond_6

    .line 2244
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_workbook:Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;

    move-object/from16 v0, v23

    invoke-virtual {v3, v0}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->removeName(Lorg/apache/poi/hssf/usermodel/HSSFName;)V

    .line 2279
    :cond_6
    :goto_2
    return-void

    .line 2237
    .end local v23    # "name":Lorg/apache/poi/hssf/usermodel/HSSFName;
    .end local v27    # "removeAll":Z
    .end local v28    # "setBoth":Z
    :cond_7
    const/16 v28, 0x0

    goto :goto_0

    .line 2238
    .restart local v28    # "setBoth":Z
    :cond_8
    const/16 v27, 0x0

    goto :goto_1

    .line 2248
    .restart local v23    # "name":Lorg/apache/poi/hssf/usermodel/HSSFName;
    .restart local v27    # "removeAll":Z
    :cond_9
    if-nez v23, :cond_a

    .line 2249
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_workbook:Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;

    .line 2250
    const/4 v7, 0x7

    .line 2249
    move/from16 v0, v29

    invoke-virtual {v3, v7, v0}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->createBuiltInName(BI)Lorg/apache/poi/hssf/usermodel/HSSFName;

    move-result-object v23

    .line 2253
    :cond_a
    new-instance v25, Ljava/util/ArrayList;

    invoke-direct/range {v25 .. v25}, Ljava/util/ArrayList;-><init>()V

    .line 2254
    .local v25, "ptgList":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/ss/formula/ptg/Ptg;>;"
    if-eqz v28, :cond_b

    .line 2255
    const/16 v22, 0x17

    .line 2256
    .local v22, "exprsSize":I
    new-instance v3, Lorg/apache/poi/ss/formula/ptg/MemFuncPtg;

    const/16 v7, 0x17

    invoke-direct {v3, v7}, Lorg/apache/poi/ss/formula/ptg/MemFuncPtg;-><init>(I)V

    move-object/from16 v0, v25

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2258
    .end local v22    # "exprsSize":I
    :cond_b
    if-eqz p2, :cond_c

    .line 2259
    new-instance v2, Lorg/apache/poi/ss/formula/ptg/Area3DPtg;

    const/4 v3, 0x0

    .line 2260
    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    .line 2259
    invoke-direct/range {v2 .. v11}, Lorg/apache/poi/ss/formula/ptg/Area3DPtg;-><init>(IIIIZZZZI)V

    .line 2261
    .local v2, "colArea":Lorg/apache/poi/ss/formula/ptg/Area3DPtg;
    move-object/from16 v0, v25

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2263
    .end local v2    # "colArea":Lorg/apache/poi/ss/formula/ptg/Area3DPtg;
    :cond_c
    if-eqz p1, :cond_d

    .line 2264
    new-instance v12, Lorg/apache/poi/ss/formula/ptg/Area3DPtg;

    const/4 v15, 0x0

    .line 2265
    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    move/from16 v21, v11

    .line 2264
    invoke-direct/range {v12 .. v21}, Lorg/apache/poi/ss/formula/ptg/Area3DPtg;-><init>(IIIIZZZZI)V

    .line 2266
    .local v12, "rowArea":Lorg/apache/poi/ss/formula/ptg/Area3DPtg;
    move-object/from16 v0, v25

    invoke-interface {v0, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2268
    .end local v12    # "rowArea":Lorg/apache/poi/ss/formula/ptg/Area3DPtg;
    :cond_d
    if-eqz v28, :cond_e

    .line 2269
    sget-object v3, Lorg/apache/poi/ss/formula/ptg/UnionPtg;->instance:Lorg/apache/poi/ss/formula/ptg/OperationPtg;

    move-object/from16 v0, v25

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2272
    :cond_e
    invoke-interface/range {v25 .. v25}, Ljava/util/List;->size()I

    move-result v3

    new-array v0, v3, [Lorg/apache/poi/ss/formula/ptg/Ptg;

    move-object/from16 v26, v0

    .line 2273
    .local v26, "ptgs":[Lorg/apache/poi/ss/formula/ptg/Ptg;
    invoke-interface/range {v25 .. v26}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 2274
    move-object/from16 v0, v23

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/usermodel/HSSFName;->setNameDefinition([Lorg/apache/poi/ss/formula/ptg/Ptg;)V

    .line 2276
    invoke-virtual/range {p0 .. p0}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->getPrintSetup()Lorg/apache/poi/hssf/usermodel/HSSFPrintSetup;

    move-result-object v24

    .line 2277
    .local v24, "printSetup":Lorg/apache/poi/hssf/usermodel/HSSFPrintSetup;
    const/4 v3, 0x0

    move-object/from16 v0, v24

    invoke-virtual {v0, v3}, Lorg/apache/poi/hssf/usermodel/HSSFPrintSetup;->setValidSettings(Z)V

    .line 2278
    const/4 v3, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->setActive(Z)V

    goto :goto_2
.end method

.method private validateArrayFormulas(Lorg/apache/poi/ss/util/CellRangeAddress;)V
    .locals 12
    .param p1, "region"    # Lorg/apache/poi/ss/util/CellRangeAddress;

    .prologue
    .line 710
    invoke-virtual {p1}, Lorg/apache/poi/ss/util/CellRangeAddress;->getFirstRow()I

    move-result v4

    .line 711
    .local v4, "firstRow":I
    invoke-virtual {p1}, Lorg/apache/poi/ss/util/CellRangeAddress;->getFirstColumn()I

    move-result v3

    .line 712
    .local v3, "firstColumn":I
    invoke-virtual {p1}, Lorg/apache/poi/ss/util/CellRangeAddress;->getLastRow()I

    move-result v6

    .line 713
    .local v6, "lastRow":I
    invoke-virtual {p1}, Lorg/apache/poi/ss/util/CellRangeAddress;->getLastColumn()I

    move-result v5

    .line 714
    .local v5, "lastColumn":I
    move v9, v4

    .local v9, "rowIn":I
    :goto_0
    if-le v9, v6, :cond_0

    .line 735
    return-void

    .line 715
    :cond_0
    move v2, v3

    .local v2, "colIn":I
    :goto_1
    if-le v2, v5, :cond_1

    .line 714
    add-int/lit8 v9, v9, 0x1

    goto :goto_0

    .line 716
    :cond_1
    invoke-virtual {p0, v9}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->getRow(I)Lorg/apache/poi/hssf/usermodel/HSSFRow;

    move-result-object v8

    .line 717
    .local v8, "row":Lorg/apache/poi/hssf/usermodel/HSSFRow;
    if-nez v8, :cond_3

    .line 715
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 719
    :cond_3
    invoke-virtual {v8, v2}, Lorg/apache/poi/hssf/usermodel/HSSFRow;->getCell(I)Lorg/apache/poi/hssf/usermodel/HSSFCell;

    move-result-object v1

    .line 720
    .local v1, "cell":Lorg/apache/poi/hssf/usermodel/HSSFCell;
    if-eqz v1, :cond_2

    .line 722
    invoke-virtual {v1}, Lorg/apache/poi/hssf/usermodel/HSSFCell;->isPartOfArrayFormulaGroup()Z

    move-result v10

    if-eqz v10, :cond_2

    .line 723
    invoke-virtual {v1}, Lorg/apache/poi/hssf/usermodel/HSSFCell;->getArrayFormulaRange()Lorg/apache/poi/ss/util/CellRangeAddress;

    move-result-object v0

    .line 724
    .local v0, "arrayRange":Lorg/apache/poi/ss/util/CellRangeAddress;
    invoke-virtual {v0}, Lorg/apache/poi/ss/util/CellRangeAddress;->getNumberOfCells()I

    move-result v10

    const/4 v11, 0x1

    if-le v10, v11, :cond_2

    .line 725
    invoke-virtual {p1}, Lorg/apache/poi/ss/util/CellRangeAddress;->getFirstRow()I

    move-result v10

    invoke-virtual {p1}, Lorg/apache/poi/ss/util/CellRangeAddress;->getFirstColumn()I

    move-result v11

    invoke-virtual {v0, v10, v11}, Lorg/apache/poi/ss/util/CellRangeAddress;->isInRange(II)Z

    move-result v10

    if-nez v10, :cond_4

    .line 726
    invoke-virtual {p1}, Lorg/apache/poi/ss/util/CellRangeAddress;->getFirstRow()I

    move-result v10

    invoke-virtual {p1}, Lorg/apache/poi/ss/util/CellRangeAddress;->getFirstColumn()I

    move-result v11

    invoke-virtual {v0, v10, v11}, Lorg/apache/poi/ss/util/CellRangeAddress;->isInRange(II)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 727
    :cond_4
    new-instance v10, Ljava/lang/StringBuilder;

    const-string/jumbo v11, "The range "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lorg/apache/poi/ss/util/CellRangeAddress;->formatAsString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string/jumbo v11, " intersects with a multi-cell array formula. "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    .line 728
    const-string/jumbo v11, "You cannot merge cells of an array."

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    .line 727
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 729
    .local v7, "msg":Ljava/lang/String;
    new-instance v10, Ljava/lang/IllegalStateException;

    invoke-direct {v10, v7}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v10
.end method


# virtual methods
.method public addMergedRegion(Lorg/apache/poi/ss/util/CellRangeAddress;)I
    .locals 5
    .param p1, "region"    # Lorg/apache/poi/ss/util/CellRangeAddress;

    .prologue
    .line 697
    sget-object v0, Lorg/apache/poi/ss/SpreadsheetVersion;->EXCEL97:Lorg/apache/poi/ss/SpreadsheetVersion;

    invoke-virtual {p1, v0}, Lorg/apache/poi/ss/util/CellRangeAddress;->validate(Lorg/apache/poi/ss/SpreadsheetVersion;)V

    .line 701
    invoke-direct {p0, p1}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->validateArrayFormulas(Lorg/apache/poi/ss/util/CellRangeAddress;)V

    .line 703
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_sheet:Lorg/apache/poi/hssf/model/InternalSheet;

    invoke-virtual {p1}, Lorg/apache/poi/ss/util/CellRangeAddress;->getFirstRow()I

    move-result v1

    .line 704
    invoke-virtual {p1}, Lorg/apache/poi/ss/util/CellRangeAddress;->getFirstColumn()I

    move-result v2

    .line 705
    invoke-virtual {p1}, Lorg/apache/poi/ss/util/CellRangeAddress;->getLastRow()I

    move-result v3

    .line 706
    invoke-virtual {p1}, Lorg/apache/poi/ss/util/CellRangeAddress;->getLastColumn()I

    move-result v4

    .line 703
    invoke-virtual {v0, v1, v2, v3, v4}, Lorg/apache/poi/hssf/model/InternalSheet;->addMergedRegion(IIII)I

    move-result v0

    return v0
.end method

.method public addMergedRegion(Lorg/apache/poi/ss/util/Region;)I
    .locals 5
    .param p1, "region"    # Lorg/apache/poi/ss/util/Region;

    .prologue
    .line 683
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_sheet:Lorg/apache/poi/hssf/model/InternalSheet;

    invoke-virtual {p1}, Lorg/apache/poi/ss/util/Region;->getRowFrom()I

    move-result v1

    .line 684
    invoke-virtual {p1}, Lorg/apache/poi/ss/util/Region;->getColumnFrom()S

    move-result v2

    .line 686
    invoke-virtual {p1}, Lorg/apache/poi/ss/util/Region;->getRowTo()I

    move-result v3

    .line 687
    invoke-virtual {p1}, Lorg/apache/poi/ss/util/Region;->getColumnTo()S

    move-result v4

    .line 683
    invoke-virtual {v0, v1, v2, v3, v4}, Lorg/apache/poi/hssf/model/InternalSheet;->addMergedRegion(IIII)I

    move-result v0

    return v0
.end method

.method public addValidationData(Lorg/apache/poi/ss/usermodel/DataValidation;)V
    .locals 5
    .param p1, "dataValidation"    # Lorg/apache/poi/ss/usermodel/DataValidation;

    .prologue
    .line 454
    if-nez p1, :cond_0

    .line 455
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v4, "objValidation must not be null"

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_0
    move-object v2, p1

    .line 457
    check-cast v2, Lorg/apache/poi/hssf/usermodel/HSSFDataValidation;

    .line 458
    .local v2, "hssfDataValidation":Lorg/apache/poi/hssf/usermodel/HSSFDataValidation;
    iget-object v3, p0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_sheet:Lorg/apache/poi/hssf/model/InternalSheet;

    invoke-virtual {v3}, Lorg/apache/poi/hssf/model/InternalSheet;->getOrCreateDataValidityTable()Lorg/apache/poi/hssf/record/aggregates/DataValidityTable;

    move-result-object v1

    .line 460
    .local v1, "dvt":Lorg/apache/poi/hssf/record/aggregates/DataValidityTable;
    invoke-virtual {v2, p0}, Lorg/apache/poi/hssf/usermodel/HSSFDataValidation;->createDVRecord(Lorg/apache/poi/hssf/usermodel/HSSFSheet;)Lorg/apache/poi/hssf/record/DVRecord;

    move-result-object v0

    .line 461
    .local v0, "dvRecord":Lorg/apache/poi/hssf/record/DVRecord;
    invoke-virtual {v1, v0}, Lorg/apache/poi/hssf/record/aggregates/DataValidityTable;->addDataValidation(Lorg/apache/poi/hssf/record/DVRecord;)V

    .line 462
    return-void
.end method

.method public autoSizeColumn(I)V
    .locals 1
    .param p1, "column"    # I

    .prologue
    .line 1997
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->autoSizeColumn(IZ)V

    .line 1998
    return-void
.end method

.method public autoSizeColumn(IZ)V
    .locals 6
    .param p1, "column"    # I
    .param p2, "useMergedCells"    # Z

    .prologue
    .line 2014
    invoke-static {p0, p1, p2}, Lorg/apache/poi/ss/util/SheetUtil;->getColumnWidth(Lorg/apache/poi/ss/usermodel/Sheet;IZ)D

    move-result-wide v2

    .line 2016
    .local v2, "width":D
    const-wide/high16 v4, -0x4010000000000000L    # -1.0

    cmpl-double v1, v2, v4

    if-eqz v1, :cond_1

    .line 2017
    const-wide/high16 v4, 0x4070000000000000L    # 256.0

    mul-double/2addr v2, v4

    .line 2018
    const v0, 0xff00

    .line 2019
    .local v0, "maxColumnWidth":I
    int-to-double v4, v0

    cmpl-double v1, v2, v4

    if-lez v1, :cond_0

    .line 2020
    int-to-double v2, v0

    .line 2022
    :cond_0
    double-to-int v1, v2

    invoke-virtual {p0, p1, v1}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->setColumnWidth(II)V

    .line 2025
    .end local v0    # "maxColumnWidth":I
    :cond_1
    return-void
.end method

.method cloneSheet(Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;)Lorg/apache/poi/hssf/usermodel/HSSFSheet;
    .locals 6
    .param p1, "workbook"    # Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;

    .prologue
    const/16 v5, 0xec

    .line 127
    invoke-virtual {p0}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->getDrawingPatriarch()Lorg/apache/poi/hssf/usermodel/HSSFPatriarch;

    .line 128
    new-instance v3, Lorg/apache/poi/hssf/usermodel/HSSFSheet;

    iget-object v4, p0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_sheet:Lorg/apache/poi/hssf/model/InternalSheet;

    invoke-virtual {v4}, Lorg/apache/poi/hssf/model/InternalSheet;->cloneSheet()Lorg/apache/poi/hssf/model/InternalSheet;

    move-result-object v4

    invoke-direct {v3, p1, v4}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;-><init>(Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;Lorg/apache/poi/hssf/model/InternalSheet;)V

    .line 129
    .local v3, "sheet":Lorg/apache/poi/hssf/usermodel/HSSFSheet;
    iget-object v4, v3, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_sheet:Lorg/apache/poi/hssf/model/InternalSheet;

    invoke-virtual {v4, v5}, Lorg/apache/poi/hssf/model/InternalSheet;->findFirstRecordLocBySid(S)I

    move-result v2

    .line 130
    .local v2, "pos":I
    iget-object v4, v3, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_sheet:Lorg/apache/poi/hssf/model/InternalSheet;

    invoke-virtual {v4, v5}, Lorg/apache/poi/hssf/model/InternalSheet;->findFirstRecordBySid(S)Lorg/apache/poi/hssf/record/Record;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hssf/record/DrawingRecord;

    .line 131
    .local v0, "dr":Lorg/apache/poi/hssf/record/DrawingRecord;
    if-eqz v0, :cond_0

    .line 132
    iget-object v4, v3, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_sheet:Lorg/apache/poi/hssf/model/InternalSheet;

    invoke-virtual {v4}, Lorg/apache/poi/hssf/model/InternalSheet;->getRecords()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 134
    :cond_0
    invoke-virtual {p0}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->getDrawingPatriarch()Lorg/apache/poi/hssf/usermodel/HSSFPatriarch;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 135
    invoke-virtual {p0}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->getDrawingPatriarch()Lorg/apache/poi/hssf/usermodel/HSSFPatriarch;

    move-result-object v4

    invoke-static {v4, v3}, Lorg/apache/poi/hssf/usermodel/HSSFPatriarch;->createPatriarch(Lorg/apache/poi/hssf/usermodel/HSSFPatriarch;Lorg/apache/poi/hssf/usermodel/HSSFSheet;)Lorg/apache/poi/hssf/usermodel/HSSFPatriarch;

    move-result-object v1

    .line 136
    .local v1, "patr":Lorg/apache/poi/hssf/usermodel/HSSFPatriarch;
    iget-object v4, v3, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_sheet:Lorg/apache/poi/hssf/model/InternalSheet;

    invoke-virtual {v4}, Lorg/apache/poi/hssf/model/InternalSheet;->getRecords()Ljava/util/List;

    move-result-object v4

    invoke-virtual {v1}, Lorg/apache/poi/hssf/usermodel/HSSFPatriarch;->_getBoundAggregate()Lorg/apache/poi/hssf/record/EscherAggregate;

    move-result-object v5

    invoke-interface {v4, v2, v5}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 137
    iput-object v1, v3, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_patriarch:Lorg/apache/poi/hssf/usermodel/HSSFPatriarch;

    .line 139
    .end local v1    # "patr":Lorg/apache/poi/hssf/usermodel/HSSFPatriarch;
    :cond_1
    return-object v3
.end method

.method public createDrawingPatriarch()Lorg/apache/poi/hssf/usermodel/HSSFPatriarch;
    .locals 1

    .prologue
    .line 1873
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->getPatriarch(Z)Lorg/apache/poi/hssf/usermodel/HSSFPatriarch;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_patriarch:Lorg/apache/poi/hssf/usermodel/HSSFPatriarch;

    .line 1874
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_patriarch:Lorg/apache/poi/hssf/usermodel/HSSFPatriarch;

    return-object v0
.end method

.method public bridge synthetic createDrawingPatriarch()Lorg/apache/poi/ss/usermodel/Drawing;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->createDrawingPatriarch()Lorg/apache/poi/hssf/usermodel/HSSFPatriarch;

    move-result-object v0

    return-object v0
.end method

.method public createFreezePane(II)V
    .locals 0
    .param p1, "colSplit"    # I
    .param p2, "rowSplit"    # I

    .prologue
    .line 1619
    invoke-virtual {p0, p1, p2, p1, p2}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->createFreezePane(IIII)V

    .line 1620
    return-void
.end method

.method public createFreezePane(IIII)V
    .locals 2
    .param p1, "colSplit"    # I
    .param p2, "rowSplit"    # I
    .param p3, "leftmostColumn"    # I
    .param p4, "topRow"    # I

    .prologue
    .line 1599
    invoke-virtual {p0, p1}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->validateColumn(I)V

    .line 1600
    invoke-virtual {p0, p2}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->validateRow(I)V

    .line 1601
    if-ge p3, p1, :cond_0

    .line 1602
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "leftmostColumn parameter must not be less than colSplit parameter"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1603
    :cond_0
    if-ge p4, p2, :cond_1

    .line 1604
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "topRow parameter must not be less than leftmostColumn parameter"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1605
    :cond_1
    invoke-virtual {p0}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->getSheet()Lorg/apache/poi/hssf/model/InternalSheet;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p4, p3}, Lorg/apache/poi/hssf/model/InternalSheet;->createFreezePane(IIII)V

    .line 1606
    return-void
.end method

.method public createRow(I)Lorg/apache/poi/hssf/usermodel/HSSFRow;
    .locals 3
    .param p1, "rownum"    # I

    .prologue
    .line 288
    new-instance v0, Lorg/apache/poi/hssf/usermodel/HSSFRow;

    iget-object v1, p0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_workbook:Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;

    invoke-direct {v0, v1, p0, p1}, Lorg/apache/poi/hssf/usermodel/HSSFRow;-><init>(Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;Lorg/apache/poi/hssf/usermodel/HSSFSheet;I)V

    .line 290
    .local v0, "row":Lorg/apache/poi/hssf/usermodel/HSSFRow;
    invoke-virtual {p0}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->getDefaultRowHeight()S

    move-result v1

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/usermodel/HSSFRow;->setHeight(S)V

    .line 291
    invoke-virtual {v0}, Lorg/apache/poi/hssf/usermodel/HSSFRow;->getRowRecord()Lorg/apache/poi/hssf/record/RowRecord;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lorg/apache/poi/hssf/record/RowRecord;->setBadFontHeight(Z)V

    .line 293
    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->addRow(Lorg/apache/poi/hssf/usermodel/HSSFRow;Z)V

    .line 294
    return-object v0
.end method

.method public bridge synthetic createRow(I)Lorg/apache/poi/ss/usermodel/Row;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->createRow(I)Lorg/apache/poi/hssf/usermodel/HSSFRow;

    move-result-object v0

    return-object v0
.end method

.method public createSplitPane(IIIII)V
    .locals 6
    .param p1, "xSplitPos"    # I
    .param p2, "ySplitPos"    # I
    .param p3, "leftmostColumn"    # I
    .param p4, "topRow"    # I
    .param p5, "activePane"    # I

    .prologue
    .line 1637
    invoke-virtual {p0}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->getSheet()Lorg/apache/poi/hssf/model/InternalSheet;

    move-result-object v0

    move v1, p1

    move v2, p2

    move v3, p4

    move v4, p3

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Lorg/apache/poi/hssf/model/InternalSheet;->createSplitPane(IIIII)V

    .line 1638
    return-void
.end method

.method public dumpDrawingRecords(Z)V
    .locals 8
    .param p1, "fat"    # Z

    .prologue
    const/4 v7, 0x0

    .line 1812
    iget-object v5, p0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_sheet:Lorg/apache/poi/hssf/model/InternalSheet;

    iget-object v6, p0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_book:Lorg/apache/poi/hssf/model/InternalWorkbook;

    invoke-virtual {v6}, Lorg/apache/poi/hssf/model/InternalWorkbook;->getDrawingManager()Lorg/apache/poi/hssf/model/DrawingManager2;

    move-result-object v6

    invoke-virtual {v5, v6, v7}, Lorg/apache/poi/hssf/model/InternalSheet;->aggregateDrawingRecords(Lorg/apache/poi/hssf/model/DrawingManager2;Z)I

    .line 1814
    invoke-virtual {p0}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->getSheet()Lorg/apache/poi/hssf/model/InternalSheet;

    move-result-object v5

    const/16 v6, 0x2694

    invoke-virtual {v5, v6}, Lorg/apache/poi/hssf/model/InternalSheet;->findFirstRecordBySid(S)Lorg/apache/poi/hssf/record/Record;

    move-result-object v3

    check-cast v3, Lorg/apache/poi/hssf/record/EscherAggregate;

    .line 1815
    .local v3, "r":Lorg/apache/poi/hssf/record/EscherAggregate;
    invoke-virtual {v3}, Lorg/apache/poi/hssf/record/EscherAggregate;->getEscherRecords()Ljava/util/List;

    move-result-object v1

    .line 1816
    .local v1, "escherRecords":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/ddf/EscherRecord;>;"
    new-instance v4, Ljava/io/PrintWriter;

    sget-object v5, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-direct {v4, v5}, Ljava/io/PrintWriter;-><init>(Ljava/io/OutputStream;)V

    .line 1817
    .local v4, "w":Ljava/io/PrintWriter;
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/poi/ddf/EscherRecord;>;"
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_1

    .line 1825
    invoke-virtual {v4}, Ljava/io/PrintWriter;->flush()V

    .line 1826
    return-void

    .line 1818
    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/ddf/EscherRecord;

    .line 1819
    .local v0, "escherRecord":Lorg/apache/poi/ddf/EscherRecord;
    if-nez p1, :cond_0

    .line 1822
    invoke-virtual {v0, v4, v7}, Lorg/apache/poi/ddf/EscherRecord;->display(Ljava/io/PrintWriter;I)V

    goto :goto_0
.end method

.method protected findCellComment(II)Lorg/apache/poi/hssf/usermodel/HSSFComment;
    .locals 2
    .param p1, "row"    # I
    .param p2, "column"    # I

    .prologue
    .line 2154
    invoke-virtual {p0}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->getDrawingPatriarch()Lorg/apache/poi/hssf/usermodel/HSSFPatriarch;

    move-result-object v0

    .line 2155
    .local v0, "patriarch":Lorg/apache/poi/hssf/usermodel/HSSFPatriarch;
    if-nez v0, :cond_0

    .line 2156
    invoke-virtual {p0}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->createDrawingPatriarch()Lorg/apache/poi/hssf/usermodel/HSSFPatriarch;

    move-result-object v0

    .line 2158
    :cond_0
    invoke-direct {p0, v0, p1, p2}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->lookForComment(Lorg/apache/poi/hssf/usermodel/HSSFShapeContainer;II)Lorg/apache/poi/hssf/usermodel/HSSFComment;

    move-result-object v1

    return-object v1
.end method

.method public getAlternateExpression()Z
    .locals 2

    .prologue
    .line 1009
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_sheet:Lorg/apache/poi/hssf/model/InternalSheet;

    const/16 v1, 0x81

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/model/InternalSheet;->findFirstRecordBySid(S)Lorg/apache/poi/hssf/record/Record;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hssf/record/WSBoolRecord;

    .line 1010
    invoke-virtual {v0}, Lorg/apache/poi/hssf/record/WSBoolRecord;->getAlternateExpression()Z

    move-result v0

    .line 1009
    return v0
.end method

.method public getAlternateFormula()Z
    .locals 2

    .prologue
    .line 1019
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_sheet:Lorg/apache/poi/hssf/model/InternalSheet;

    const/16 v1, 0x81

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/model/InternalSheet;->findFirstRecordBySid(S)Lorg/apache/poi/hssf/record/Record;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hssf/record/WSBoolRecord;

    .line 1020
    invoke-virtual {v0}, Lorg/apache/poi/hssf/record/WSBoolRecord;->getAlternateFormula()Z

    move-result v0

    .line 1019
    return v0
.end method

.method public getAutobreaks()Z
    .locals 2

    .prologue
    .line 1029
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_sheet:Lorg/apache/poi/hssf/model/InternalSheet;

    const/16 v1, 0x81

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/model/InternalSheet;->findFirstRecordBySid(S)Lorg/apache/poi/hssf/record/Record;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hssf/record/WSBoolRecord;

    .line 1030
    invoke-virtual {v0}, Lorg/apache/poi/hssf/record/WSBoolRecord;->getAutobreaks()Z

    move-result v0

    .line 1029
    return v0
.end method

.method public getCellComment(II)Lorg/apache/poi/hssf/usermodel/HSSFComment;
    .locals 1
    .param p1, "row"    # I
    .param p2, "column"    # I

    .prologue
    .line 2033
    invoke-virtual {p0, p1, p2}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->findCellComment(II)Lorg/apache/poi/hssf/usermodel/HSSFComment;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getCellComment(II)Lorg/apache/poi/ss/usermodel/Comment;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0, p1, p2}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->getCellComment(II)Lorg/apache/poi/hssf/usermodel/HSSFComment;

    move-result-object v0

    return-object v0
.end method

.method public getColumnBreaks()[I
    .locals 1

    .prologue
    .line 1746
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_sheet:Lorg/apache/poi/hssf/model/InternalSheet;

    invoke-virtual {v0}, Lorg/apache/poi/hssf/model/InternalSheet;->getPageSettings()Lorg/apache/poi/hssf/record/aggregates/PageSettingsBlock;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/poi/hssf/record/aggregates/PageSettingsBlock;->getColumnBreaks()[I

    move-result-object v0

    return-object v0
.end method

.method public getColumnStyle(I)Lorg/apache/poi/hssf/usermodel/HSSFCellStyle;
    .locals 4
    .param p1, "column"    # I

    .prologue
    .line 648
    iget-object v2, p0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_sheet:Lorg/apache/poi/hssf/model/InternalSheet;

    int-to-short v3, p1

    invoke-virtual {v2, v3}, Lorg/apache/poi/hssf/model/InternalSheet;->getXFIndexForColAt(S)S

    move-result v0

    .line 650
    .local v0, "styleIndex":S
    const/16 v2, 0xf

    if-ne v0, v2, :cond_0

    .line 652
    const/4 v2, 0x0

    .line 656
    :goto_0
    return-object v2

    .line 655
    :cond_0
    iget-object v2, p0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_book:Lorg/apache/poi/hssf/model/InternalWorkbook;

    invoke-virtual {v2, v0}, Lorg/apache/poi/hssf/model/InternalWorkbook;->getExFormatAt(I)Lorg/apache/poi/hssf/record/ExtendedFormatRecord;

    move-result-object v1

    .line 656
    .local v1, "xf":Lorg/apache/poi/hssf/record/ExtendedFormatRecord;
    new-instance v2, Lorg/apache/poi/hssf/usermodel/HSSFCellStyle;

    iget-object v3, p0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_book:Lorg/apache/poi/hssf/model/InternalWorkbook;

    invoke-direct {v2, v0, v1, v3}, Lorg/apache/poi/hssf/usermodel/HSSFCellStyle;-><init>(SLorg/apache/poi/hssf/record/ExtendedFormatRecord;Lorg/apache/poi/hssf/model/InternalWorkbook;)V

    goto :goto_0
.end method

.method public bridge synthetic getColumnStyle(I)Lorg/apache/poi/ss/usermodel/CellStyle;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->getColumnStyle(I)Lorg/apache/poi/hssf/usermodel/HSSFCellStyle;

    move-result-object v0

    return-object v0
.end method

.method public getColumnWidth(I)I
    .locals 1
    .param p1, "columnIndex"    # I

    .prologue
    .line 575
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_sheet:Lorg/apache/poi/hssf/model/InternalSheet;

    invoke-virtual {v0, p1}, Lorg/apache/poi/hssf/model/InternalSheet;->getColumnWidth(I)I

    move-result v0

    return v0
.end method

.method public getColumnWidth(S)S
    .locals 1
    .param p1, "columnIndex"    # S

    .prologue
    .line 490
    const v0, 0xffff

    and-int/2addr v0, p1

    invoke-virtual {p0, v0}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->getColumnWidth(I)I

    move-result v0

    int-to-short v0, v0

    return v0
.end method

.method public getDataValidationHelper()Lorg/apache/poi/ss/usermodel/DataValidationHelper;
    .locals 1

    .prologue
    .line 2115
    new-instance v0, Lorg/apache/poi/hssf/usermodel/HSSFDataValidationHelper;

    invoke-direct {v0, p0}, Lorg/apache/poi/hssf/usermodel/HSSFDataValidationHelper;-><init>(Lorg/apache/poi/hssf/usermodel/HSSFSheet;)V

    return-object v0
.end method

.method public getDefaultColumnWidth()I
    .locals 1

    .prologue
    .line 585
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_sheet:Lorg/apache/poi/hssf/model/InternalSheet;

    invoke-virtual {v0}, Lorg/apache/poi/hssf/model/InternalSheet;->getDefaultColumnWidth()I

    move-result v0

    return v0
.end method

.method public getDefaultRowHeight()S
    .locals 1

    .prologue
    .line 606
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_sheet:Lorg/apache/poi/hssf/model/InternalSheet;

    invoke-virtual {v0}, Lorg/apache/poi/hssf/model/InternalSheet;->getDefaultRowHeight()S

    move-result v0

    return v0
.end method

.method public getDefaultRowHeightInPoints()F
    .locals 2

    .prologue
    .line 617
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_sheet:Lorg/apache/poi/hssf/model/InternalSheet;

    invoke-virtual {v0}, Lorg/apache/poi/hssf/model/InternalSheet;->getDefaultRowHeight()S

    move-result v0

    int-to-float v0, v0

    const/high16 v1, 0x41a00000    # 20.0f

    div-float/2addr v0, v1

    return v0
.end method

.method public getDialog()Z
    .locals 2

    .prologue
    .line 1039
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_sheet:Lorg/apache/poi/hssf/model/InternalSheet;

    const/16 v1, 0x81

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/model/InternalSheet;->findFirstRecordBySid(S)Lorg/apache/poi/hssf/record/Record;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hssf/record/WSBoolRecord;

    .line 1040
    invoke-virtual {v0}, Lorg/apache/poi/hssf/record/WSBoolRecord;->getDialog()Z

    move-result v0

    .line 1039
    return v0
.end method

.method public getDisplayGuts()Z
    .locals 2

    .prologue
    .line 1049
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_sheet:Lorg/apache/poi/hssf/model/InternalSheet;

    const/16 v1, 0x81

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/model/InternalSheet;->findFirstRecordBySid(S)Lorg/apache/poi/hssf/record/Record;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hssf/record/WSBoolRecord;

    .line 1050
    invoke-virtual {v0}, Lorg/apache/poi/hssf/record/WSBoolRecord;->getDisplayGuts()Z

    move-result v0

    .line 1049
    return v0
.end method

.method public getDrawingEscherAggregate()Lorg/apache/poi/hssf/record/EscherAggregate;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 1833
    iget-object v2, p0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_book:Lorg/apache/poi/hssf/model/InternalWorkbook;

    invoke-virtual {v2}, Lorg/apache/poi/hssf/model/InternalWorkbook;->findDrawingGroup()Lorg/apache/poi/hssf/model/DrawingManager2;

    .line 1837
    iget-object v2, p0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_book:Lorg/apache/poi/hssf/model/InternalWorkbook;

    invoke-virtual {v2}, Lorg/apache/poi/hssf/model/InternalWorkbook;->getDrawingManager()Lorg/apache/poi/hssf/model/DrawingManager2;

    move-result-object v2

    if-nez v2, :cond_1

    .line 1851
    :cond_0
    :goto_0
    return-object v0

    .line 1841
    :cond_1
    iget-object v2, p0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_sheet:Lorg/apache/poi/hssf/model/InternalSheet;

    .line 1842
    iget-object v3, p0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_book:Lorg/apache/poi/hssf/model/InternalWorkbook;

    invoke-virtual {v3}, Lorg/apache/poi/hssf/model/InternalWorkbook;->getDrawingManager()Lorg/apache/poi/hssf/model/DrawingManager2;

    move-result-object v3

    const/4 v4, 0x0

    .line 1841
    invoke-virtual {v2, v3, v4}, Lorg/apache/poi/hssf/model/InternalSheet;->aggregateDrawingRecords(Lorg/apache/poi/hssf/model/DrawingManager2;Z)I

    move-result v1

    .line 1844
    .local v1, "found":I
    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    .line 1850
    iget-object v2, p0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_sheet:Lorg/apache/poi/hssf/model/InternalSheet;

    const/16 v3, 0x2694

    invoke-virtual {v2, v3}, Lorg/apache/poi/hssf/model/InternalSheet;->findFirstRecordBySid(S)Lorg/apache/poi/hssf/record/Record;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hssf/record/EscherAggregate;

    .line 1851
    .local v0, "agg":Lorg/apache/poi/hssf/record/EscherAggregate;
    goto :goto_0
.end method

.method public getDrawingPatriarch()Lorg/apache/poi/hssf/usermodel/HSSFPatriarch;
    .locals 1

    .prologue
    .line 1860
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->getPatriarch(Z)Lorg/apache/poi/hssf/usermodel/HSSFPatriarch;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_patriarch:Lorg/apache/poi/hssf/usermodel/HSSFPatriarch;

    .line 1861
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_patriarch:Lorg/apache/poi/hssf/usermodel/HSSFPatriarch;

    return-object v0
.end method

.method public getFirstRowNum()I
    .locals 1

    .prologue
    .line 428
    iget v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_firstrow:I

    return v0
.end method

.method public getFitToPage()Z
    .locals 2

    .prologue
    .line 1086
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_sheet:Lorg/apache/poi/hssf/model/InternalSheet;

    const/16 v1, 0x81

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/model/InternalSheet;->findFirstRecordBySid(S)Lorg/apache/poi/hssf/record/Record;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hssf/record/WSBoolRecord;

    .line 1087
    invoke-virtual {v0}, Lorg/apache/poi/hssf/record/WSBoolRecord;->getFitToPage()Z

    move-result v0

    .line 1086
    return v0
.end method

.method public getFooter()Lorg/apache/poi/hssf/usermodel/HSSFFooter;
    .locals 2

    .prologue
    .line 1143
    new-instance v0, Lorg/apache/poi/hssf/usermodel/HSSFFooter;

    iget-object v1, p0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_sheet:Lorg/apache/poi/hssf/model/InternalSheet;

    invoke-virtual {v1}, Lorg/apache/poi/hssf/model/InternalSheet;->getPageSettings()Lorg/apache/poi/hssf/record/aggregates/PageSettingsBlock;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/poi/hssf/usermodel/HSSFFooter;-><init>(Lorg/apache/poi/hssf/record/aggregates/PageSettingsBlock;)V

    return-object v0
.end method

.method public bridge synthetic getFooter()Lorg/apache/poi/ss/usermodel/Footer;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->getFooter()Lorg/apache/poi/hssf/usermodel/HSSFFooter;

    move-result-object v0

    return-object v0
.end method

.method public getForceFormulaRecalculation()Z
    .locals 1

    .prologue
    .line 768
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_sheet:Lorg/apache/poi/hssf/model/InternalSheet;

    invoke-virtual {v0}, Lorg/apache/poi/hssf/model/InternalSheet;->getUncalced()Z

    move-result v0

    return v0
.end method

.method public getHeader()Lorg/apache/poi/hssf/usermodel/HSSFHeader;
    .locals 2

    .prologue
    .line 1139
    new-instance v0, Lorg/apache/poi/hssf/usermodel/HSSFHeader;

    iget-object v1, p0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_sheet:Lorg/apache/poi/hssf/model/InternalSheet;

    invoke-virtual {v1}, Lorg/apache/poi/hssf/model/InternalSheet;->getPageSettings()Lorg/apache/poi/hssf/record/aggregates/PageSettingsBlock;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/poi/hssf/usermodel/HSSFHeader;-><init>(Lorg/apache/poi/hssf/record/aggregates/PageSettingsBlock;)V

    return-object v0
.end method

.method public bridge synthetic getHeader()Lorg/apache/poi/ss/usermodel/Header;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->getHeader()Lorg/apache/poi/hssf/usermodel/HSSFHeader;

    move-result-object v0

    return-object v0
.end method

.method public getHorizontallyCenter()Z
    .locals 1

    .prologue
    .line 814
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_sheet:Lorg/apache/poi/hssf/model/InternalSheet;

    invoke-virtual {v0}, Lorg/apache/poi/hssf/model/InternalSheet;->getPageSettings()Lorg/apache/poi/hssf/record/aggregates/PageSettingsBlock;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/poi/hssf/record/aggregates/PageSettingsBlock;->getHCenter()Lorg/apache/poi/hssf/record/HCenterRecord;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/poi/hssf/record/HCenterRecord;->getHCenter()Z

    move-result v0

    return v0
.end method

.method public getLastRowNum()I
    .locals 1

    .prologue
    .line 445
    iget v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_lastrow:I

    return v0
.end method

.method public getLeftCol()S
    .locals 1

    .prologue
    .line 1300
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_sheet:Lorg/apache/poi/hssf/model/InternalSheet;

    invoke-virtual {v0}, Lorg/apache/poi/hssf/model/InternalSheet;->getLeftCol()S

    move-result v0

    return v0
.end method

.method public getMargin(S)D
    .locals 2
    .param p1, "margin"    # S

    .prologue
    .line 1187
    packed-switch p1, :pswitch_data_0

    .line 1193
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_sheet:Lorg/apache/poi/hssf/model/InternalSheet;

    invoke-virtual {v0}, Lorg/apache/poi/hssf/model/InternalSheet;->getPageSettings()Lorg/apache/poi/hssf/record/aggregates/PageSettingsBlock;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/poi/hssf/record/aggregates/PageSettingsBlock;->getMargin(S)D

    move-result-wide v0

    :goto_0
    return-wide v0

    .line 1189
    :pswitch_0
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_sheet:Lorg/apache/poi/hssf/model/InternalSheet;

    invoke-virtual {v0}, Lorg/apache/poi/hssf/model/InternalSheet;->getPageSettings()Lorg/apache/poi/hssf/record/aggregates/PageSettingsBlock;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/poi/hssf/record/aggregates/PageSettingsBlock;->getPrintSetup()Lorg/apache/poi/hssf/record/PrintSetupRecord;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/poi/hssf/record/PrintSetupRecord;->getFooterMargin()D

    move-result-wide v0

    goto :goto_0

    .line 1191
    :pswitch_1
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_sheet:Lorg/apache/poi/hssf/model/InternalSheet;

    invoke-virtual {v0}, Lorg/apache/poi/hssf/model/InternalSheet;->getPageSettings()Lorg/apache/poi/hssf/record/aggregates/PageSettingsBlock;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/poi/hssf/record/aggregates/PageSettingsBlock;->getPrintSetup()Lorg/apache/poi/hssf/record/PrintSetupRecord;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/poi/hssf/record/PrintSetupRecord;->getHeaderMargin()D

    move-result-wide v0

    goto :goto_0

    .line 1187
    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public getMergedRegion(I)Lorg/apache/poi/ss/util/CellRangeAddress;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 872
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_sheet:Lorg/apache/poi/hssf/model/InternalSheet;

    invoke-virtual {v0, p1}, Lorg/apache/poi/hssf/model/InternalSheet;->getMergedRegionAt(I)Lorg/apache/poi/ss/util/CellRangeAddress;

    move-result-object v0

    return-object v0
.end method

.method public getMergedRegionAt(I)Lorg/apache/poi/hssf/util/Region;
    .locals 6
    .param p1, "index"    # I

    .prologue
    .line 859
    invoke-virtual {p0, p1}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->getMergedRegion(I)Lorg/apache/poi/ss/util/CellRangeAddress;

    move-result-object v0

    .line 861
    .local v0, "cra":Lorg/apache/poi/ss/util/CellRangeAddress;
    if-eqz v0, :cond_0

    .line 862
    new-instance v1, Lorg/apache/poi/hssf/util/Region;

    invoke-virtual {v0}, Lorg/apache/poi/ss/util/CellRangeAddress;->getFirstRow()I

    move-result v2

    invoke-virtual {v0}, Lorg/apache/poi/ss/util/CellRangeAddress;->getFirstColumn()I

    move-result v3

    int-to-short v3, v3

    .line 863
    invoke-virtual {v0}, Lorg/apache/poi/ss/util/CellRangeAddress;->getLastRow()I

    move-result v4

    invoke-virtual {v0}, Lorg/apache/poi/ss/util/CellRangeAddress;->getLastColumn()I

    move-result v5

    int-to-short v5, v5

    .line 862
    invoke-direct {v1, v2, v3, v4, v5}, Lorg/apache/poi/hssf/util/Region;-><init>(ISIS)V

    .line 865
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getNumMergedRegions()I
    .locals 1

    .prologue
    .line 852
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_sheet:Lorg/apache/poi/hssf/model/InternalSheet;

    invoke-virtual {v0}, Lorg/apache/poi/hssf/model/InternalSheet;->getNumMergedRegions()I

    move-result v0

    return v0
.end method

.method public getObjectProtect()Z
    .locals 1

    .prologue
    .line 1242
    invoke-direct {p0}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->getProtectionBlock()Lorg/apache/poi/hssf/record/aggregates/WorksheetProtectionBlock;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/poi/hssf/record/aggregates/WorksheetProtectionBlock;->isObjectProtected()Z

    move-result v0

    return v0
.end method

.method public getPaneInformation()Lorg/apache/poi/hssf/util/PaneInformation;
    .locals 1

    .prologue
    .line 1646
    invoke-virtual {p0}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->getSheet()Lorg/apache/poi/hssf/model/InternalSheet;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/poi/hssf/model/InternalSheet;->getPaneInformation()Lorg/apache/poi/hssf/util/PaneInformation;

    move-result-object v0

    return-object v0
.end method

.method public getPassword()S
    .locals 1

    .prologue
    .line 1233
    invoke-direct {p0}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->getProtectionBlock()Lorg/apache/poi/hssf/record/aggregates/WorksheetProtectionBlock;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/poi/hssf/record/aggregates/WorksheetProtectionBlock;->getPasswordHash()I

    move-result v0

    int-to-short v0, v0

    return v0
.end method

.method public getPhysicalNumberOfRows()I
    .locals 1

    .prologue
    .line 419
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_rows:Ljava/util/TreeMap;

    invoke-virtual {v0}, Ljava/util/TreeMap;->size()I

    move-result v0

    return v0
.end method

.method public getPrintSetup()Lorg/apache/poi/hssf/usermodel/HSSFPrintSetup;
    .locals 2

    .prologue
    .line 1135
    new-instance v0, Lorg/apache/poi/hssf/usermodel/HSSFPrintSetup;

    iget-object v1, p0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_sheet:Lorg/apache/poi/hssf/model/InternalSheet;

    invoke-virtual {v1}, Lorg/apache/poi/hssf/model/InternalSheet;->getPageSettings()Lorg/apache/poi/hssf/record/aggregates/PageSettingsBlock;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/poi/hssf/record/aggregates/PageSettingsBlock;->getPrintSetup()Lorg/apache/poi/hssf/record/PrintSetupRecord;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/poi/hssf/usermodel/HSSFPrintSetup;-><init>(Lorg/apache/poi/hssf/record/PrintSetupRecord;)V

    return-object v0
.end method

.method public bridge synthetic getPrintSetup()Lorg/apache/poi/ss/usermodel/PrintSetup;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->getPrintSetup()Lorg/apache/poi/hssf/usermodel/HSSFPrintSetup;

    move-result-object v0

    return-object v0
.end method

.method public getProtect()Z
    .locals 1

    .prologue
    .line 1226
    invoke-direct {p0}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->getProtectionBlock()Lorg/apache/poi/hssf/record/aggregates/WorksheetProtectionBlock;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/poi/hssf/record/aggregates/WorksheetProtectionBlock;->isSheetProtected()Z

    move-result v0

    return v0
.end method

.method public getRepeatingColumns()Lorg/apache/poi/ss/util/CellRangeAddress;
    .locals 1

    .prologue
    .line 2188
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->getRepeatingRowsOrColums(Z)Lorg/apache/poi/ss/util/CellRangeAddress;

    move-result-object v0

    return-object v0
.end method

.method public getRepeatingRows()Lorg/apache/poi/ss/util/CellRangeAddress;
    .locals 1

    .prologue
    .line 2183
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->getRepeatingRowsOrColums(Z)Lorg/apache/poi/ss/util/CellRangeAddress;

    move-result-object v0

    return-object v0
.end method

.method public getRow(I)Lorg/apache/poi/hssf/usermodel/HSSFRow;
    .locals 2
    .param p1, "rowIndex"    # I

    .prologue
    .line 412
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_rows:Ljava/util/TreeMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/TreeMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hssf/usermodel/HSSFRow;

    return-object v0
.end method

.method public bridge synthetic getRow(I)Lorg/apache/poi/ss/usermodel/Row;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->getRow(I)Lorg/apache/poi/hssf/usermodel/HSSFRow;

    move-result-object v0

    return-object v0
.end method

.method public getRowBreaks()[I
    .locals 1

    .prologue
    .line 1738
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_sheet:Lorg/apache/poi/hssf/model/InternalSheet;

    invoke-virtual {v0}, Lorg/apache/poi/hssf/model/InternalSheet;->getPageSettings()Lorg/apache/poi/hssf/record/aggregates/PageSettingsBlock;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/poi/hssf/record/aggregates/PageSettingsBlock;->getRowBreaks()[I

    move-result-object v0

    return-object v0
.end method

.method public getRowSumsBelow()Z
    .locals 2

    .prologue
    .line 1096
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_sheet:Lorg/apache/poi/hssf/model/InternalSheet;

    const/16 v1, 0x81

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/model/InternalSheet;->findFirstRecordBySid(S)Lorg/apache/poi/hssf/record/Record;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hssf/record/WSBoolRecord;

    .line 1097
    invoke-virtual {v0}, Lorg/apache/poi/hssf/record/WSBoolRecord;->getRowSumsBelow()Z

    move-result v0

    .line 1096
    return v0
.end method

.method public getRowSumsRight()Z
    .locals 2

    .prologue
    .line 1106
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_sheet:Lorg/apache/poi/hssf/model/InternalSheet;

    const/16 v1, 0x81

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/model/InternalSheet;->findFirstRecordBySid(S)Lorg/apache/poi/hssf/record/Record;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hssf/record/WSBoolRecord;

    .line 1107
    invoke-virtual {v0}, Lorg/apache/poi/hssf/record/WSBoolRecord;->getRowSumsRight()Z

    move-result v0

    .line 1106
    return v0
.end method

.method public getScenarioProtect()Z
    .locals 1

    .prologue
    .line 1251
    invoke-direct {p0}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->getProtectionBlock()Lorg/apache/poi/hssf/record/aggregates/WorksheetProtectionBlock;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/poi/hssf/record/aggregates/WorksheetProtectionBlock;->isScenarioProtected()Z

    move-result v0

    return v0
.end method

.method getSheet()Lorg/apache/poi/hssf/model/InternalSheet;
    .locals 1

    .prologue
    .line 902
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_sheet:Lorg/apache/poi/hssf/model/InternalSheet;

    return-object v0
.end method

.method public getSheetConditionalFormatting()Lorg/apache/poi/hssf/usermodel/HSSFSheetConditionalFormatting;
    .locals 1

    .prologue
    .line 2037
    new-instance v0, Lorg/apache/poi/hssf/usermodel/HSSFSheetConditionalFormatting;

    invoke-direct {v0, p0}, Lorg/apache/poi/hssf/usermodel/HSSFSheetConditionalFormatting;-><init>(Lorg/apache/poi/hssf/usermodel/HSSFSheet;)V

    return-object v0
.end method

.method public bridge synthetic getSheetConditionalFormatting()Lorg/apache/poi/ss/usermodel/SheetConditionalFormatting;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->getSheetConditionalFormatting()Lorg/apache/poi/hssf/usermodel/HSSFSheetConditionalFormatting;

    move-result-object v0

    return-object v0
.end method

.method public getSheetName()Ljava/lang/String;
    .locals 3

    .prologue
    .line 2046
    invoke-virtual {p0}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->getWorkbook()Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;

    move-result-object v1

    .line 2047
    .local v1, "wb":Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;
    invoke-virtual {v1, p0}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->getSheetIndex(Lorg/apache/poi/ss/usermodel/Sheet;)I

    move-result v0

    .line 2048
    .local v0, "idx":I
    invoke-virtual {v1, v0}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->getSheetName(I)Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method public getTopRow()S
    .locals 1

    .prologue
    .line 1290
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_sheet:Lorg/apache/poi/hssf/model/InternalSheet;

    invoke-virtual {v0}, Lorg/apache/poi/hssf/model/InternalSheet;->getTopRow()S

    move-result v0

    return v0
.end method

.method public getVerticallyCenter()Z
    .locals 1

    .prologue
    .line 795
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_sheet:Lorg/apache/poi/hssf/model/InternalSheet;

    invoke-virtual {v0}, Lorg/apache/poi/hssf/model/InternalSheet;->getPageSettings()Lorg/apache/poi/hssf/record/aggregates/PageSettingsBlock;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/poi/hssf/record/aggregates/PageSettingsBlock;->getVCenter()Lorg/apache/poi/hssf/record/VCenterRecord;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/poi/hssf/record/VCenterRecord;->getVCenter()Z

    move-result v0

    return v0
.end method

.method public getVerticallyCenter(Z)Z
    .locals 1
    .param p1, "value"    # Z

    .prologue
    .line 788
    invoke-virtual {p0}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->getVerticallyCenter()Z

    move-result v0

    return v0
.end method

.method public getWorkbook()Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_workbook:Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;

    return-object v0
.end method

.method public bridge synthetic getWorkbook()Lorg/apache/poi/ss/usermodel/Workbook;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->getWorkbook()Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;

    move-result-object v0

    return-object v0
.end method

.method public groupColumn(II)V
    .locals 2
    .param p1, "fromColumn"    # I
    .param p2, "toColumn"    # I

    .prologue
    .line 1948
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_sheet:Lorg/apache/poi/hssf/model/InternalSheet;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, p2, v1}, Lorg/apache/poi/hssf/model/InternalSheet;->groupColumnRange(IIZ)V

    .line 1949
    return-void
.end method

.method public groupColumn(SS)V
    .locals 2
    .param p1, "fromColumn"    # S
    .param p2, "toColumn"    # S

    .prologue
    const v1, 0xffff

    .line 1921
    and-int v0, p1, v1

    and-int/2addr v1, p2

    invoke-virtual {p0, v0, v1}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->groupColumn(II)V

    .line 1922
    return-void
.end method

.method public groupRow(II)V
    .locals 2
    .param p1, "fromRow"    # I
    .param p2, "toRow"    # I

    .prologue
    .line 1962
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_sheet:Lorg/apache/poi/hssf/model/InternalSheet;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, p2, v1}, Lorg/apache/poi/hssf/model/InternalSheet;->groupRowRange(IIZ)V

    .line 1963
    return-void
.end method

.method protected insertChartRecords(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/apache/poi/hssf/record/Record;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1571
    .local p1, "records":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/hssf/record/Record;>;"
    iget-object v1, p0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_sheet:Lorg/apache/poi/hssf/model/InternalSheet;

    const/16 v2, 0x23e

    invoke-virtual {v1, v2}, Lorg/apache/poi/hssf/model/InternalSheet;->findFirstRecordLocBySid(S)I

    move-result v0

    .line 1572
    .local v0, "window2Loc":I
    iget-object v1, p0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_sheet:Lorg/apache/poi/hssf/model/InternalSheet;

    invoke-virtual {v1}, Lorg/apache/poi/hssf/model/InternalSheet;->getRecords()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v0, p1}, Ljava/util/List;->addAll(ILjava/util/Collection;)Z

    .line 1573
    return-void
.end method

.method public isActive()Z
    .locals 1

    .prologue
    .line 1168
    invoke-virtual {p0}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->getSheet()Lorg/apache/poi/hssf/model/InternalSheet;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/poi/hssf/model/InternalSheet;->getWindowTwo()Lorg/apache/poi/hssf/record/WindowTwoRecord;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/poi/hssf/record/WindowTwoRecord;->isActive()Z

    move-result v0

    return v0
.end method

.method public isColumnBroken(I)Z
    .locals 1
    .param p1, "column"    # I

    .prologue
    .line 1773
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_sheet:Lorg/apache/poi/hssf/model/InternalSheet;

    invoke-virtual {v0}, Lorg/apache/poi/hssf/model/InternalSheet;->getPageSettings()Lorg/apache/poi/hssf/record/aggregates/PageSettingsBlock;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/poi/hssf/record/aggregates/PageSettingsBlock;->isColumnBroken(I)Z

    move-result v0

    return v0
.end method

.method public isColumnHidden(I)Z
    .locals 1
    .param p1, "columnIndex"    # I

    .prologue
    .line 517
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_sheet:Lorg/apache/poi/hssf/model/InternalSheet;

    invoke-virtual {v0, p1}, Lorg/apache/poi/hssf/model/InternalSheet;->isColumnHidden(I)Z

    move-result v0

    return v0
.end method

.method public isColumnHidden(S)Z
    .locals 1
    .param p1, "columnIndex"    # S

    .prologue
    .line 476
    const v0, 0xffff

    and-int/2addr v0, p1

    invoke-virtual {p0, v0}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->isColumnHidden(I)Z

    move-result v0

    return v0
.end method

.method public isDisplayFormulas()Z
    .locals 1

    .prologue
    .line 1682
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_sheet:Lorg/apache/poi/hssf/model/InternalSheet;

    invoke-virtual {v0}, Lorg/apache/poi/hssf/model/InternalSheet;->isDisplayFormulas()Z

    move-result v0

    return v0
.end method

.method public isDisplayGridlines()Z
    .locals 1

    .prologue
    .line 1664
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_sheet:Lorg/apache/poi/hssf/model/InternalSheet;

    invoke-virtual {v0}, Lorg/apache/poi/hssf/model/InternalSheet;->isDisplayGridlines()Z

    move-result v0

    return v0
.end method

.method public isDisplayRowColHeadings()Z
    .locals 1

    .prologue
    .line 1700
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_sheet:Lorg/apache/poi/hssf/model/InternalSheet;

    invoke-virtual {v0}, Lorg/apache/poi/hssf/model/InternalSheet;->isDisplayRowColHeadings()Z

    move-result v0

    return v0
.end method

.method public isDisplayZeros()Z
    .locals 1

    .prologue
    .line 1064
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_sheet:Lorg/apache/poi/hssf/model/InternalSheet;

    invoke-virtual {v0}, Lorg/apache/poi/hssf/model/InternalSheet;->getWindowTwo()Lorg/apache/poi/hssf/record/WindowTwoRecord;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/poi/hssf/record/WindowTwoRecord;->getDisplayZeros()Z

    move-result v0

    return v0
.end method

.method public isGridsPrinted()Z
    .locals 1

    .prologue
    .line 666
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_sheet:Lorg/apache/poi/hssf/model/InternalSheet;

    invoke-virtual {v0}, Lorg/apache/poi/hssf/model/InternalSheet;->isGridsPrinted()Z

    move-result v0

    return v0
.end method

.method public isPrintGridlines()Z
    .locals 1

    .prologue
    .line 1116
    invoke-virtual {p0}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->getSheet()Lorg/apache/poi/hssf/model/InternalSheet;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/poi/hssf/model/InternalSheet;->getPrintGridlines()Lorg/apache/poi/hssf/record/PrintGridlinesRecord;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/poi/hssf/record/PrintGridlinesRecord;->getPrintGridlines()Z

    move-result v0

    return v0
.end method

.method public isRightToLeft()Z
    .locals 1

    .prologue
    .line 832
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_sheet:Lorg/apache/poi/hssf/model/InternalSheet;

    invoke-virtual {v0}, Lorg/apache/poi/hssf/model/InternalSheet;->getWindowTwo()Lorg/apache/poi/hssf/record/WindowTwoRecord;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/poi/hssf/record/WindowTwoRecord;->getArabic()Z

    move-result v0

    return v0
.end method

.method public isRowBroken(I)Z
    .locals 1
    .param p1, "row"    # I

    .prologue
    .line 1723
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_sheet:Lorg/apache/poi/hssf/model/InternalSheet;

    invoke-virtual {v0}, Lorg/apache/poi/hssf/model/InternalSheet;->getPageSettings()Lorg/apache/poi/hssf/record/aggregates/PageSettingsBlock;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/poi/hssf/record/aggregates/PageSettingsBlock;->isRowBroken(I)Z

    move-result v0

    return v0
.end method

.method public isSelected()Z
    .locals 1

    .prologue
    .line 1152
    invoke-virtual {p0}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->getSheet()Lorg/apache/poi/hssf/model/InternalSheet;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/poi/hssf/model/InternalSheet;->getWindowTwo()Lorg/apache/poi/hssf/record/WindowTwoRecord;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/poi/hssf/record/WindowTwoRecord;->getSelected()Z

    move-result v0

    return v0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Lorg/apache/poi/ss/usermodel/Row;",
            ">;"
        }
    .end annotation

    .prologue
    .line 891
    invoke-virtual {p0}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->rowIterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method protected preSerialize()V
    .locals 1

    .prologue
    .line 146
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_patriarch:Lorg/apache/poi/hssf/usermodel/HSSFPatriarch;

    if-eqz v0, :cond_0

    .line 147
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_patriarch:Lorg/apache/poi/hssf/usermodel/HSSFPatriarch;

    invoke-virtual {v0}, Lorg/apache/poi/hssf/usermodel/HSSFPatriarch;->preSerialize()V

    .line 149
    :cond_0
    return-void
.end method

.method public protectSheet(Ljava/lang/String;)V
    .locals 2
    .param p1, "password"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x1

    .line 1260
    invoke-direct {p0}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->getProtectionBlock()Lorg/apache/poi/hssf/record/aggregates/WorksheetProtectionBlock;

    move-result-object v0

    invoke-virtual {v0, p1, v1, v1}, Lorg/apache/poi/hssf/record/aggregates/WorksheetProtectionBlock;->protectSheet(Ljava/lang/String;ZZ)V

    .line 1261
    return-void
.end method

.method public removeArrayFormula(Lorg/apache/poi/ss/usermodel/Cell;)Lorg/apache/poi/ss/usermodel/CellRange;
    .locals 9
    .param p1, "cell"    # Lorg/apache/poi/ss/usermodel/Cell;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/poi/ss/usermodel/Cell;",
            ")",
            "Lorg/apache/poi/ss/usermodel/CellRange",
            "<",
            "Lorg/apache/poi/hssf/usermodel/HSSFCell;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2095
    invoke-interface {p1}, Lorg/apache/poi/ss/usermodel/Cell;->getSheet()Lorg/apache/poi/ss/usermodel/Sheet;

    move-result-object v6

    if-eq v6, p0, :cond_0

    .line 2096
    new-instance v6, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v7, "Specified cell does not belong to this sheet."

    invoke-direct {v6, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v6

    :cond_0
    move-object v6, p1

    .line 2098
    check-cast v6, Lorg/apache/poi/hssf/usermodel/HSSFCell;

    invoke-virtual {v6}, Lorg/apache/poi/hssf/usermodel/HSSFCell;->getCellValueRecord()Lorg/apache/poi/hssf/record/CellValueRecordInterface;

    move-result-object v3

    .line 2099
    .local v3, "rec":Lorg/apache/poi/hssf/record/CellValueRecordInterface;
    instance-of v6, v3, Lorg/apache/poi/hssf/record/aggregates/FormulaRecordAggregate;

    if-nez v6, :cond_1

    .line 2100
    new-instance v6, Lorg/apache/poi/ss/util/CellReference;

    invoke-direct {v6, p1}, Lorg/apache/poi/ss/util/CellReference;-><init>(Lorg/apache/poi/ss/usermodel/Cell;)V

    invoke-virtual {v6}, Lorg/apache/poi/ss/util/CellReference;->formatAsString()Ljava/lang/String;

    move-result-object v4

    .line 2101
    .local v4, "ref":Ljava/lang/String;
    new-instance v6, Ljava/lang/IllegalArgumentException;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string/jumbo v8, "Cell "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, " is not part of an array formula."

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v6

    .end local v4    # "ref":Ljava/lang/String;
    :cond_1
    move-object v1, v3

    .line 2103
    check-cast v1, Lorg/apache/poi/hssf/record/aggregates/FormulaRecordAggregate;

    .line 2104
    .local v1, "fra":Lorg/apache/poi/hssf/record/aggregates/FormulaRecordAggregate;
    invoke-interface {p1}, Lorg/apache/poi/ss/usermodel/Cell;->getRowIndex()I

    move-result v6

    invoke-interface {p1}, Lorg/apache/poi/ss/usermodel/Cell;->getColumnIndex()I

    move-result v7

    invoke-virtual {v1, v6, v7}, Lorg/apache/poi/hssf/record/aggregates/FormulaRecordAggregate;->removeArrayFormula(II)Lorg/apache/poi/ss/util/CellRangeAddress;

    move-result-object v2

    .line 2106
    .local v2, "range":Lorg/apache/poi/ss/util/CellRangeAddress;
    invoke-direct {p0, v2}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->getCellRange(Lorg/apache/poi/ss/util/CellRangeAddress;)Lorg/apache/poi/ss/usermodel/CellRange;

    move-result-object v5

    .line 2108
    .local v5, "result":Lorg/apache/poi/ss/usermodel/CellRange;, "Lorg/apache/poi/ss/usermodel/CellRange<Lorg/apache/poi/hssf/usermodel/HSSFCell;>;"
    invoke-interface {v5}, Lorg/apache/poi/ss/usermodel/CellRange;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-nez v7, :cond_2

    .line 2111
    return-object v5

    .line 2108
    :cond_2
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/ss/usermodel/Cell;

    .line 2109
    .local v0, "c":Lorg/apache/poi/ss/usermodel/Cell;
    const/4 v7, 0x3

    invoke-interface {v0, v7}, Lorg/apache/poi/ss/usermodel/Cell;->setCellType(I)V

    goto :goto_0
.end method

.method public removeColumnBreak(I)V
    .locals 1
    .param p1, "column"    # I

    .prologue
    .line 1782
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_sheet:Lorg/apache/poi/hssf/model/InternalSheet;

    invoke-virtual {v0}, Lorg/apache/poi/hssf/model/InternalSheet;->getPageSettings()Lorg/apache/poi/hssf/record/aggregates/PageSettingsBlock;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/poi/hssf/record/aggregates/PageSettingsBlock;->removeColumnBreak(I)V

    .line 1783
    return-void
.end method

.method public removeMergedRegion(I)V
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 842
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_sheet:Lorg/apache/poi/hssf/model/InternalSheet;

    invoke-virtual {v0, p1}, Lorg/apache/poi/hssf/model/InternalSheet;->removeMergedRegion(I)V

    .line 843
    return-void
.end method

.method public removeRow(Lorg/apache/poi/ss/usermodel/Row;)V
    .locals 9
    .param p1, "row"    # Lorg/apache/poi/ss/usermodel/Row;

    .prologue
    .line 318
    move-object v1, p1

    check-cast v1, Lorg/apache/poi/hssf/usermodel/HSSFRow;

    .line 319
    .local v1, "hrow":Lorg/apache/poi/hssf/usermodel/HSSFRow;
    invoke-interface {p1}, Lorg/apache/poi/ss/usermodel/Row;->getSheet()Lorg/apache/poi/ss/usermodel/Sheet;

    move-result-object v6

    if-eq v6, p0, :cond_0

    .line 320
    new-instance v6, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v7, "Specified row does not belong to this sheet"

    invoke-direct {v6, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 322
    :cond_0
    invoke-interface {p1}, Lorg/apache/poi/ss/usermodel/Row;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_1
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-nez v7, :cond_2

    .line 330
    iget-object v6, p0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_rows:Ljava/util/TreeMap;

    invoke-virtual {v6}, Ljava/util/TreeMap;->size()I

    move-result v6

    if-lez v6, :cond_6

    .line 331
    invoke-interface {p1}, Lorg/apache/poi/ss/usermodel/Row;->getRowNum()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 332
    .local v2, "key":Ljava/lang/Integer;
    iget-object v6, p0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_rows:Ljava/util/TreeMap;

    invoke-virtual {v6, v2}, Ljava/util/TreeMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/poi/hssf/usermodel/HSSFRow;

    .line 333
    .local v4, "removedRow":Lorg/apache/poi/hssf/usermodel/HSSFRow;
    if-eq v4, p1, :cond_3

    .line 335
    new-instance v6, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v7, "Specified row does not belong to this sheet"

    invoke-direct {v6, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 322
    .end local v2    # "key":Ljava/lang/Integer;
    .end local v4    # "removedRow":Lorg/apache/poi/hssf/usermodel/HSSFRow;
    :cond_2
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/ss/usermodel/Cell;

    .local v0, "cell":Lorg/apache/poi/ss/usermodel/Cell;
    move-object v5, v0

    .line 323
    check-cast v5, Lorg/apache/poi/hssf/usermodel/HSSFCell;

    .line 324
    .local v5, "xcell":Lorg/apache/poi/hssf/usermodel/HSSFCell;
    invoke-virtual {v5}, Lorg/apache/poi/hssf/usermodel/HSSFCell;->isPartOfArrayFormulaGroup()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 325
    new-instance v7, Ljava/lang/StringBuilder;

    const-string/jumbo v8, "Row[rownum="

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p1}, Lorg/apache/poi/ss/usermodel/Row;->getRowNum()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, "] contains cell(s) included in a multi-cell array formula. You cannot change part of an array."

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 326
    .local v3, "msg":Ljava/lang/String;
    invoke-virtual {v5, v3}, Lorg/apache/poi/hssf/usermodel/HSSFCell;->notifyArrayFormulaChanging(Ljava/lang/String;)V

    goto :goto_0

    .line 337
    .end local v0    # "cell":Lorg/apache/poi/ss/usermodel/Cell;
    .end local v3    # "msg":Ljava/lang/String;
    .end local v5    # "xcell":Lorg/apache/poi/hssf/usermodel/HSSFCell;
    .restart local v2    # "key":Ljava/lang/Integer;
    .restart local v4    # "removedRow":Lorg/apache/poi/hssf/usermodel/HSSFRow;
    :cond_3
    invoke-virtual {v1}, Lorg/apache/poi/hssf/usermodel/HSSFRow;->getRowNum()I

    move-result v6

    invoke-virtual {p0}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->getLastRowNum()I

    move-result v7

    if-ne v6, v7, :cond_4

    .line 338
    iget v6, p0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_lastrow:I

    invoke-direct {p0, v6}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->findLastRow(I)I

    move-result v6

    iput v6, p0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_lastrow:I

    .line 340
    :cond_4
    invoke-virtual {v1}, Lorg/apache/poi/hssf/usermodel/HSSFRow;->getRowNum()I

    move-result v6

    invoke-virtual {p0}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->getFirstRowNum()I

    move-result v7

    if-ne v6, v7, :cond_5

    .line 341
    iget v6, p0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_firstrow:I

    invoke-direct {p0, v6}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->findFirstRow(I)I

    move-result v6

    iput v6, p0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_firstrow:I

    .line 343
    :cond_5
    iget-object v6, p0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_sheet:Lorg/apache/poi/hssf/model/InternalSheet;

    invoke-virtual {v1}, Lorg/apache/poi/hssf/usermodel/HSSFRow;->getRowRecord()Lorg/apache/poi/hssf/record/RowRecord;

    move-result-object v7

    invoke-virtual {v6, v7}, Lorg/apache/poi/hssf/model/InternalSheet;->removeRow(Lorg/apache/poi/hssf/record/RowRecord;)V

    .line 345
    .end local v2    # "key":Ljava/lang/Integer;
    .end local v4    # "removedRow":Lorg/apache/poi/hssf/usermodel/HSSFRow;
    :cond_6
    return-void
.end method

.method public removeRowBreak(I)V
    .locals 1
    .param p1, "row"    # I

    .prologue
    .line 1730
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_sheet:Lorg/apache/poi/hssf/model/InternalSheet;

    invoke-virtual {v0}, Lorg/apache/poi/hssf/model/InternalSheet;->getPageSettings()Lorg/apache/poi/hssf/record/aggregates/PageSettingsBlock;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/poi/hssf/record/aggregates/PageSettingsBlock;->removeRowBreak(I)V

    .line 1731
    return-void
.end method

.method public rowIterator()Ljava/util/Iterator;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Lorg/apache/poi/ss/usermodel/Row;",
            ">;"
        }
    .end annotation

    .prologue
    .line 882
    iget-object v1, p0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_rows:Ljava/util/TreeMap;

    invoke-virtual {v1}, Ljava/util/TreeMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 883
    .local v0, "result":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/poi/ss/usermodel/Row;>;"
    return-object v0
.end method

.method public setActive(Z)V
    .locals 1
    .param p1, "sel"    # Z

    .prologue
    .line 1177
    invoke-virtual {p0}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->getSheet()Lorg/apache/poi/hssf/model/InternalSheet;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/poi/hssf/model/InternalSheet;->getWindowTwo()Lorg/apache/poi/hssf/record/WindowTwoRecord;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/poi/hssf/record/WindowTwoRecord;->setActive(Z)V

    .line 1178
    return-void
.end method

.method public setAlternativeExpression(Z)V
    .locals 3
    .param p1, "b"    # Z

    .prologue
    .line 912
    iget-object v1, p0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_sheet:Lorg/apache/poi/hssf/model/InternalSheet;

    const/16 v2, 0x81

    invoke-virtual {v1, v2}, Lorg/apache/poi/hssf/model/InternalSheet;->findFirstRecordBySid(S)Lorg/apache/poi/hssf/record/Record;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hssf/record/WSBoolRecord;

    .line 914
    .local v0, "record":Lorg/apache/poi/hssf/record/WSBoolRecord;
    invoke-virtual {v0, p1}, Lorg/apache/poi/hssf/record/WSBoolRecord;->setAlternateExpression(Z)V

    .line 915
    return-void
.end method

.method public setAlternativeFormula(Z)V
    .locals 3
    .param p1, "b"    # Z

    .prologue
    .line 924
    iget-object v1, p0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_sheet:Lorg/apache/poi/hssf/model/InternalSheet;

    const/16 v2, 0x81

    invoke-virtual {v1, v2}, Lorg/apache/poi/hssf/model/InternalSheet;->findFirstRecordBySid(S)Lorg/apache/poi/hssf/record/Record;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hssf/record/WSBoolRecord;

    .line 926
    .local v0, "record":Lorg/apache/poi/hssf/record/WSBoolRecord;
    invoke-virtual {v0, p1}, Lorg/apache/poi/hssf/record/WSBoolRecord;->setAlternateFormula(Z)V

    .line 927
    return-void
.end method

.method public setArrayFormula(Ljava/lang/String;Lorg/apache/poi/ss/util/CellRangeAddress;)Lorg/apache/poi/ss/usermodel/CellRange;
    .locals 8
    .param p1, "formula"    # Ljava/lang/String;
    .param p2, "range"    # Lorg/apache/poi/ss/util/CellRangeAddress;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lorg/apache/poi/ss/util/CellRangeAddress;",
            ")",
            "Lorg/apache/poi/ss/usermodel/CellRange",
            "<",
            "Lorg/apache/poi/hssf/usermodel/HSSFCell;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2080
    iget-object v6, p0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_workbook:Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;

    invoke-virtual {v6, p0}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->getSheetIndex(Lorg/apache/poi/ss/usermodel/Sheet;)I

    move-result v5

    .line 2081
    .local v5, "sheetIndex":I
    iget-object v6, p0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_workbook:Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;

    const/4 v7, 0x2

    invoke-static {p1, v6, v7, v5}, Lorg/apache/poi/hssf/model/HSSFFormulaParser;->parse(Ljava/lang/String;Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;II)[Lorg/apache/poi/ss/formula/ptg/Ptg;

    move-result-object v4

    .line 2082
    .local v4, "ptgs":[Lorg/apache/poi/ss/formula/ptg/Ptg;
    invoke-direct {p0, p2}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->getCellRange(Lorg/apache/poi/ss/util/CellRangeAddress;)Lorg/apache/poi/ss/usermodel/CellRange;

    move-result-object v2

    .line 2084
    .local v2, "cells":Lorg/apache/poi/ss/usermodel/CellRange;, "Lorg/apache/poi/ss/usermodel/CellRange<Lorg/apache/poi/hssf/usermodel/HSSFCell;>;"
    invoke-interface {v2}, Lorg/apache/poi/ss/usermodel/CellRange;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-nez v7, :cond_0

    .line 2087
    invoke-interface {v2}, Lorg/apache/poi/ss/usermodel/CellRange;->getTopLeftCell()Lorg/apache/poi/ss/usermodel/Cell;

    move-result-object v3

    check-cast v3, Lorg/apache/poi/hssf/usermodel/HSSFCell;

    .line 2088
    .local v3, "mainArrayFormulaCell":Lorg/apache/poi/hssf/usermodel/HSSFCell;
    invoke-virtual {v3}, Lorg/apache/poi/hssf/usermodel/HSSFCell;->getCellValueRecord()Lorg/apache/poi/hssf/record/CellValueRecordInterface;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hssf/record/aggregates/FormulaRecordAggregate;

    .line 2089
    .local v0, "agg":Lorg/apache/poi/hssf/record/aggregates/FormulaRecordAggregate;
    invoke-virtual {v0, p2, v4}, Lorg/apache/poi/hssf/record/aggregates/FormulaRecordAggregate;->setArrayFormula(Lorg/apache/poi/ss/util/CellRangeAddress;[Lorg/apache/poi/ss/formula/ptg/Ptg;)V

    .line 2090
    return-object v2

    .line 2084
    .end local v0    # "agg":Lorg/apache/poi/hssf/record/aggregates/FormulaRecordAggregate;
    .end local v3    # "mainArrayFormulaCell":Lorg/apache/poi/hssf/usermodel/HSSFCell;
    :cond_0
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/hssf/usermodel/HSSFCell;

    .line 2085
    .local v1, "c":Lorg/apache/poi/hssf/usermodel/HSSFCell;
    invoke-virtual {v1, p2}, Lorg/apache/poi/hssf/usermodel/HSSFCell;->setCellArrayFormula(Lorg/apache/poi/ss/util/CellRangeAddress;)V

    goto :goto_0
.end method

.method public setAutoFilter(Lorg/apache/poi/ss/util/CellRangeAddress;)Lorg/apache/poi/hssf/usermodel/HSSFAutoFilter;
    .locals 28
    .param p1, "range"    # Lorg/apache/poi/ss/util/CellRangeAddress;

    .prologue
    .line 2121
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_workbook:Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;

    invoke-virtual {v3}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->getWorkbook()Lorg/apache/poi/hssf/model/InternalWorkbook;

    move-result-object v27

    .line 2122
    .local v27, "workbook":Lorg/apache/poi/hssf/model/InternalWorkbook;
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_workbook:Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;

    move-object/from16 v0, p0

    invoke-virtual {v3, v0}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->getSheetIndex(Lorg/apache/poi/ss/usermodel/Sheet;)I

    move-result v11

    .line 2124
    .local v11, "sheetIndex":I
    const/16 v3, 0xd

    add-int/lit8 v4, v11, 0x1

    move-object/from16 v0, v27

    invoke-virtual {v0, v3, v4}, Lorg/apache/poi/hssf/model/InternalWorkbook;->getSpecificBuiltinRecord(BI)Lorg/apache/poi/hssf/record/NameRecord;

    move-result-object v23

    .line 2126
    .local v23, "name":Lorg/apache/poi/hssf/record/NameRecord;
    if-nez v23, :cond_0

    .line 2127
    const/16 v3, 0xd

    add-int/lit8 v4, v11, 0x1

    move-object/from16 v0, v27

    invoke-virtual {v0, v3, v4}, Lorg/apache/poi/hssf/model/InternalWorkbook;->createBuiltInName(BI)Lorg/apache/poi/hssf/record/NameRecord;

    move-result-object v23

    .line 2131
    :cond_0
    new-instance v2, Lorg/apache/poi/ss/formula/ptg/Area3DPtg;

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/ss/util/CellRangeAddress;->getFirstRow()I

    move-result v3

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/ss/util/CellRangeAddress;->getLastRow()I

    move-result v4

    .line 2132
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/ss/util/CellRangeAddress;->getFirstColumn()I

    move-result v5

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/ss/util/CellRangeAddress;->getLastColumn()I

    move-result v6

    .line 2133
    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    .line 2131
    invoke-direct/range {v2 .. v11}, Lorg/apache/poi/ss/formula/ptg/Area3DPtg;-><init>(IIIIZZZZI)V

    .line 2134
    .local v2, "ptg":Lorg/apache/poi/ss/formula/ptg/Area3DPtg;
    const/4 v3, 0x1

    new-array v3, v3, [Lorg/apache/poi/ss/formula/ptg/Ptg;

    const/4 v4, 0x0

    aput-object v2, v3, v4

    move-object/from16 v0, v23

    invoke-virtual {v0, v3}, Lorg/apache/poi/hssf/record/NameRecord;->setNameDefinition([Lorg/apache/poi/ss/formula/ptg/Ptg;)V

    .line 2136
    new-instance v26, Lorg/apache/poi/hssf/record/AutoFilterInfoRecord;

    invoke-direct/range {v26 .. v26}, Lorg/apache/poi/hssf/record/AutoFilterInfoRecord;-><init>()V

    .line 2138
    .local v26, "r":Lorg/apache/poi/hssf/record/AutoFilterInfoRecord;
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/ss/util/CellRangeAddress;->getLastColumn()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/ss/util/CellRangeAddress;->getFirstColumn()I

    move-result v4

    sub-int v24, v3, v4

    .line 2139
    .local v24, "numcols":I
    move/from16 v0, v24

    int-to-short v3, v0

    move-object/from16 v0, v26

    invoke-virtual {v0, v3}, Lorg/apache/poi/hssf/record/AutoFilterInfoRecord;->setNumEntries(S)V

    .line 2140
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_sheet:Lorg/apache/poi/hssf/model/InternalSheet;

    const/16 v4, 0x200

    invoke-virtual {v3, v4}, Lorg/apache/poi/hssf/model/InternalSheet;->findFirstRecordLocBySid(S)I

    move-result v22

    .line 2141
    .local v22, "idx":I
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_sheet:Lorg/apache/poi/hssf/model/InternalSheet;

    invoke-virtual {v3}, Lorg/apache/poi/hssf/model/InternalSheet;->getRecords()Ljava/util/List;

    move-result-object v3

    move/from16 v0, v22

    move-object/from16 v1, v26

    invoke-interface {v3, v0, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 2144
    invoke-virtual/range {p0 .. p0}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->createDrawingPatriarch()Lorg/apache/poi/hssf/usermodel/HSSFPatriarch;

    move-result-object v25

    .line 2145
    .local v25, "p":Lorg/apache/poi/hssf/usermodel/HSSFPatriarch;
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/ss/util/CellRangeAddress;->getFirstColumn()I

    move-result v21

    .local v21, "col":I
    :goto_0
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/ss/util/CellRangeAddress;->getLastColumn()I

    move-result v3

    move/from16 v0, v21

    if-le v0, v3, :cond_1

    .line 2150
    new-instance v3, Lorg/apache/poi/hssf/usermodel/HSSFAutoFilter;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lorg/apache/poi/hssf/usermodel/HSSFAutoFilter;-><init>(Lorg/apache/poi/hssf/usermodel/HSSFSheet;)V

    return-object v3

    .line 2146
    :cond_1
    new-instance v12, Lorg/apache/poi/hssf/usermodel/HSSFClientAnchor;

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    .line 2147
    move/from16 v0, v21

    int-to-short v0, v0

    move/from16 v17, v0

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/ss/util/CellRangeAddress;->getFirstRow()I

    move-result v18

    add-int/lit8 v3, v21, 0x1

    int-to-short v0, v3

    move/from16 v19, v0

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/ss/util/CellRangeAddress;->getFirstRow()I

    move-result v3

    add-int/lit8 v20, v3, 0x1

    invoke-direct/range {v12 .. v20}, Lorg/apache/poi/hssf/usermodel/HSSFClientAnchor;-><init>(IIIISISI)V

    .line 2146
    move-object/from16 v0, v25

    invoke-virtual {v0, v12}, Lorg/apache/poi/hssf/usermodel/HSSFPatriarch;->createComboBox(Lorg/apache/poi/hssf/usermodel/HSSFAnchor;)Lorg/apache/poi/hssf/usermodel/HSSFSimpleShape;

    .line 2145
    add-int/lit8 v21, v21, 0x1

    goto :goto_0
.end method

.method public bridge synthetic setAutoFilter(Lorg/apache/poi/ss/util/CellRangeAddress;)Lorg/apache/poi/ss/usermodel/AutoFilter;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->setAutoFilter(Lorg/apache/poi/ss/util/CellRangeAddress;)Lorg/apache/poi/hssf/usermodel/HSSFAutoFilter;

    move-result-object v0

    return-object v0
.end method

.method public setAutobreaks(Z)V
    .locals 3
    .param p1, "b"    # Z

    .prologue
    .line 936
    iget-object v1, p0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_sheet:Lorg/apache/poi/hssf/model/InternalSheet;

    const/16 v2, 0x81

    invoke-virtual {v1, v2}, Lorg/apache/poi/hssf/model/InternalSheet;->findFirstRecordBySid(S)Lorg/apache/poi/hssf/record/Record;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hssf/record/WSBoolRecord;

    .line 938
    .local v0, "record":Lorg/apache/poi/hssf/record/WSBoolRecord;
    invoke-virtual {v0, p1}, Lorg/apache/poi/hssf/record/WSBoolRecord;->setAutobreaks(Z)V

    .line 939
    return-void
.end method

.method public setColumnBreak(I)V
    .locals 4
    .param p1, "column"    # I

    .prologue
    .line 1762
    int-to-short v0, p1

    invoke-virtual {p0, v0}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->validateColumn(I)V

    .line 1763
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_sheet:Lorg/apache/poi/hssf/model/InternalSheet;

    invoke-virtual {v0}, Lorg/apache/poi/hssf/model/InternalSheet;->getPageSettings()Lorg/apache/poi/hssf/record/aggregates/PageSettingsBlock;

    move-result-object v0

    int-to-short v1, p1

    const/4 v2, 0x0

    sget-object v3, Lorg/apache/poi/ss/SpreadsheetVersion;->EXCEL97:Lorg/apache/poi/ss/SpreadsheetVersion;

    invoke-virtual {v3}, Lorg/apache/poi/ss/SpreadsheetVersion;->getLastRowIndex()I

    move-result v3

    int-to-short v3, v3

    invoke-virtual {v0, v1, v2, v3}, Lorg/apache/poi/hssf/record/aggregates/PageSettingsBlock;->setColumnBreak(SSS)V

    .line 1764
    return-void
.end method

.method public setColumnGroupCollapsed(IZ)V
    .locals 1
    .param p1, "columnNumber"    # I
    .param p2, "collapsed"    # Z

    .prologue
    .line 1938
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_sheet:Lorg/apache/poi/hssf/model/InternalSheet;

    invoke-virtual {v0, p1, p2}, Lorg/apache/poi/hssf/model/InternalSheet;->setColumnGroupCollapsed(IZ)V

    .line 1939
    return-void
.end method

.method public setColumnGroupCollapsed(SZ)V
    .locals 1
    .param p1, "columnNumber"    # S
    .param p2, "collapsed"    # Z

    .prologue
    .line 1914
    const v0, 0xffff

    and-int/2addr v0, p1

    invoke-virtual {p0, v0, p2}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->setColumnGroupCollapsed(IZ)V

    .line 1915
    return-void
.end method

.method public setColumnHidden(IZ)V
    .locals 1
    .param p1, "columnIndex"    # I
    .param p2, "hidden"    # Z

    .prologue
    .line 507
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_sheet:Lorg/apache/poi/hssf/model/InternalSheet;

    invoke-virtual {v0, p1, p2}, Lorg/apache/poi/hssf/model/InternalSheet;->setColumnHidden(IZ)V

    .line 508
    return-void
.end method

.method public setColumnHidden(SZ)V
    .locals 1
    .param p1, "columnIndex"    # S
    .param p2, "hidden"    # Z

    .prologue
    .line 469
    const v0, 0xffff

    and-int/2addr v0, p1

    invoke-virtual {p0, v0, p2}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->setColumnHidden(IZ)V

    .line 470
    return-void
.end method

.method public setColumnWidth(II)V
    .locals 1
    .param p1, "columnIndex"    # I
    .param p2, "width"    # I

    .prologue
    .line 565
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_sheet:Lorg/apache/poi/hssf/model/InternalSheet;

    invoke-virtual {v0, p1, p2}, Lorg/apache/poi/hssf/model/InternalSheet;->setColumnWidth(II)V

    .line 566
    return-void
.end method

.method public setColumnWidth(SS)V
    .locals 2
    .param p1, "columnIndex"    # S
    .param p2, "width"    # S

    .prologue
    const v1, 0xffff

    .line 483
    and-int v0, p1, v1

    and-int/2addr v1, p2

    invoke-virtual {p0, v0, v1}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->setColumnWidth(II)V

    .line 484
    return-void
.end method

.method public setDefaultColumnStyle(ILorg/apache/poi/ss/usermodel/CellStyle;)V
    .locals 2
    .param p1, "column"    # I
    .param p2, "style"    # Lorg/apache/poi/ss/usermodel/CellStyle;

    .prologue
    .line 1984
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_sheet:Lorg/apache/poi/hssf/model/InternalSheet;

    check-cast p2, Lorg/apache/poi/hssf/usermodel/HSSFCellStyle;

    .end local p2    # "style":Lorg/apache/poi/ss/usermodel/CellStyle;
    invoke-virtual {p2}, Lorg/apache/poi/hssf/usermodel/HSSFCellStyle;->getIndex()S

    move-result v1

    invoke-virtual {v0, p1, v1}, Lorg/apache/poi/hssf/model/InternalSheet;->setDefaultColumnStyle(II)V

    .line 1985
    return-void
.end method

.method public setDefaultColumnWidth(I)V
    .locals 1
    .param p1, "width"    # I

    .prologue
    .line 595
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_sheet:Lorg/apache/poi/hssf/model/InternalSheet;

    invoke-virtual {v0, p1}, Lorg/apache/poi/hssf/model/InternalSheet;->setDefaultColumnWidth(I)V

    .line 596
    return-void
.end method

.method public setDefaultColumnWidth(S)V
    .locals 1
    .param p1, "width"    # S

    .prologue
    .line 497
    const v0, 0xffff

    and-int/2addr v0, p1

    invoke-virtual {p0, v0}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->setDefaultColumnWidth(I)V

    .line 498
    return-void
.end method

.method public setDefaultRowHeight(S)V
    .locals 1
    .param p1, "height"    # S

    .prologue
    .line 628
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_sheet:Lorg/apache/poi/hssf/model/InternalSheet;

    invoke-virtual {v0, p1}, Lorg/apache/poi/hssf/model/InternalSheet;->setDefaultRowHeight(S)V

    .line 629
    return-void
.end method

.method public setDefaultRowHeightInPoints(F)V
    .locals 2
    .param p1, "height"    # F

    .prologue
    .line 639
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_sheet:Lorg/apache/poi/hssf/model/InternalSheet;

    const/high16 v1, 0x41a00000    # 20.0f

    mul-float/2addr v1, p1

    float-to-int v1, v1

    int-to-short v1, v1

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/model/InternalSheet;->setDefaultRowHeight(S)V

    .line 640
    return-void
.end method

.method public setDialog(Z)V
    .locals 3
    .param p1, "b"    # Z

    .prologue
    .line 948
    iget-object v1, p0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_sheet:Lorg/apache/poi/hssf/model/InternalSheet;

    const/16 v2, 0x81

    invoke-virtual {v1, v2}, Lorg/apache/poi/hssf/model/InternalSheet;->findFirstRecordBySid(S)Lorg/apache/poi/hssf/record/Record;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hssf/record/WSBoolRecord;

    .line 950
    .local v0, "record":Lorg/apache/poi/hssf/record/WSBoolRecord;
    invoke-virtual {v0, p1}, Lorg/apache/poi/hssf/record/WSBoolRecord;->setDialog(Z)V

    .line 951
    return-void
.end method

.method public setDisplayFormulas(Z)V
    .locals 1
    .param p1, "show"    # Z

    .prologue
    .line 1673
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_sheet:Lorg/apache/poi/hssf/model/InternalSheet;

    invoke-virtual {v0, p1}, Lorg/apache/poi/hssf/model/InternalSheet;->setDisplayFormulas(Z)V

    .line 1674
    return-void
.end method

.method public setDisplayGridlines(Z)V
    .locals 1
    .param p1, "show"    # Z

    .prologue
    .line 1655
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_sheet:Lorg/apache/poi/hssf/model/InternalSheet;

    invoke-virtual {v0, p1}, Lorg/apache/poi/hssf/model/InternalSheet;->setDisplayGridlines(Z)V

    .line 1656
    return-void
.end method

.method public setDisplayGuts(Z)V
    .locals 3
    .param p1, "b"    # Z

    .prologue
    .line 960
    iget-object v1, p0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_sheet:Lorg/apache/poi/hssf/model/InternalSheet;

    const/16 v2, 0x81

    invoke-virtual {v1, v2}, Lorg/apache/poi/hssf/model/InternalSheet;->findFirstRecordBySid(S)Lorg/apache/poi/hssf/record/Record;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hssf/record/WSBoolRecord;

    .line 962
    .local v0, "record":Lorg/apache/poi/hssf/record/WSBoolRecord;
    invoke-virtual {v0, p1}, Lorg/apache/poi/hssf/record/WSBoolRecord;->setDisplayGuts(Z)V

    .line 963
    return-void
.end method

.method public setDisplayRowColHeadings(Z)V
    .locals 1
    .param p1, "show"    # Z

    .prologue
    .line 1691
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_sheet:Lorg/apache/poi/hssf/model/InternalSheet;

    invoke-virtual {v0, p1}, Lorg/apache/poi/hssf/model/InternalSheet;->setDisplayRowColHeadings(Z)V

    .line 1692
    return-void
.end method

.method public setDisplayZeros(Z)V
    .locals 1
    .param p1, "value"    # Z

    .prologue
    .line 1077
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_sheet:Lorg/apache/poi/hssf/model/InternalSheet;

    invoke-virtual {v0}, Lorg/apache/poi/hssf/model/InternalSheet;->getWindowTwo()Lorg/apache/poi/hssf/record/WindowTwoRecord;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/poi/hssf/record/WindowTwoRecord;->setDisplayZeros(Z)V

    .line 1078
    return-void
.end method

.method public setFitToPage(Z)V
    .locals 3
    .param p1, "b"    # Z

    .prologue
    .line 972
    iget-object v1, p0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_sheet:Lorg/apache/poi/hssf/model/InternalSheet;

    const/16 v2, 0x81

    invoke-virtual {v1, v2}, Lorg/apache/poi/hssf/model/InternalSheet;->findFirstRecordBySid(S)Lorg/apache/poi/hssf/record/Record;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hssf/record/WSBoolRecord;

    .line 974
    .local v0, "record":Lorg/apache/poi/hssf/record/WSBoolRecord;
    invoke-virtual {v0, p1}, Lorg/apache/poi/hssf/record/WSBoolRecord;->setFitToPage(Z)V

    .line 975
    return-void
.end method

.method public setForceFormulaRecalculation(Z)V
    .locals 1
    .param p1, "value"    # Z

    .prologue
    .line 758
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_sheet:Lorg/apache/poi/hssf/model/InternalSheet;

    invoke-virtual {v0, p1}, Lorg/apache/poi/hssf/model/InternalSheet;->setUncalced(Z)V

    .line 759
    return-void
.end method

.method public setGridsPrinted(Z)V
    .locals 1
    .param p1, "value"    # Z

    .prologue
    .line 676
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_sheet:Lorg/apache/poi/hssf/model/InternalSheet;

    invoke-virtual {v0, p1}, Lorg/apache/poi/hssf/model/InternalSheet;->setGridsPrinted(Z)V

    .line 677
    return-void
.end method

.method public setHorizontallyCenter(Z)V
    .locals 1
    .param p1, "value"    # Z

    .prologue
    .line 805
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_sheet:Lorg/apache/poi/hssf/model/InternalSheet;

    invoke-virtual {v0}, Lorg/apache/poi/hssf/model/InternalSheet;->getPageSettings()Lorg/apache/poi/hssf/record/aggregates/PageSettingsBlock;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/poi/hssf/record/aggregates/PageSettingsBlock;->getHCenter()Lorg/apache/poi/hssf/record/HCenterRecord;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/poi/hssf/record/HCenterRecord;->setHCenter(Z)V

    .line 806
    return-void
.end method

.method public setMargin(SD)V
    .locals 2
    .param p1, "margin"    # S
    .param p2, "size"    # D

    .prologue
    .line 1204
    packed-switch p1, :pswitch_data_0

    .line 1212
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_sheet:Lorg/apache/poi/hssf/model/InternalSheet;

    invoke-virtual {v0}, Lorg/apache/poi/hssf/model/InternalSheet;->getPageSettings()Lorg/apache/poi/hssf/record/aggregates/PageSettingsBlock;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lorg/apache/poi/hssf/record/aggregates/PageSettingsBlock;->setMargin(SD)V

    .line 1214
    :goto_0
    return-void

    .line 1206
    :pswitch_0
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_sheet:Lorg/apache/poi/hssf/model/InternalSheet;

    invoke-virtual {v0}, Lorg/apache/poi/hssf/model/InternalSheet;->getPageSettings()Lorg/apache/poi/hssf/record/aggregates/PageSettingsBlock;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/poi/hssf/record/aggregates/PageSettingsBlock;->getPrintSetup()Lorg/apache/poi/hssf/record/PrintSetupRecord;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Lorg/apache/poi/hssf/record/PrintSetupRecord;->setFooterMargin(D)V

    goto :goto_0

    .line 1209
    :pswitch_1
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_sheet:Lorg/apache/poi/hssf/model/InternalSheet;

    invoke-virtual {v0}, Lorg/apache/poi/hssf/model/InternalSheet;->getPageSettings()Lorg/apache/poi/hssf/record/aggregates/PageSettingsBlock;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/poi/hssf/record/aggregates/PageSettingsBlock;->getPrintSetup()Lorg/apache/poi/hssf/record/PrintSetupRecord;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Lorg/apache/poi/hssf/record/PrintSetupRecord;->setHeaderMargin(D)V

    goto :goto_0

    .line 1204
    nop

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public setPrintGridlines(Z)V
    .locals 1
    .param p1, "newPrintGridlines"    # Z

    .prologue
    .line 1126
    invoke-virtual {p0}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->getSheet()Lorg/apache/poi/hssf/model/InternalSheet;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/poi/hssf/model/InternalSheet;->getPrintGridlines()Lorg/apache/poi/hssf/record/PrintGridlinesRecord;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/poi/hssf/record/PrintGridlinesRecord;->setPrintGridlines(Z)V

    .line 1127
    return-void
.end method

.method public setRepeatingColumns(Lorg/apache/poi/ss/util/CellRangeAddress;)V
    .locals 1
    .param p1, "columnRangeRef"    # Lorg/apache/poi/ss/util/CellRangeAddress;

    .prologue
    .line 2199
    invoke-virtual {p0}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->getRepeatingRows()Lorg/apache/poi/ss/util/CellRangeAddress;

    move-result-object v0

    .line 2200
    .local v0, "rowRangeRef":Lorg/apache/poi/ss/util/CellRangeAddress;
    invoke-direct {p0, v0, p1}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->setRepeatingRowsAndColumns(Lorg/apache/poi/ss/util/CellRangeAddress;Lorg/apache/poi/ss/util/CellRangeAddress;)V

    .line 2201
    return-void
.end method

.method public setRepeatingRows(Lorg/apache/poi/ss/util/CellRangeAddress;)V
    .locals 1
    .param p1, "rowRangeRef"    # Lorg/apache/poi/ss/util/CellRangeAddress;

    .prologue
    .line 2193
    invoke-virtual {p0}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->getRepeatingColumns()Lorg/apache/poi/ss/util/CellRangeAddress;

    move-result-object v0

    .line 2194
    .local v0, "columnRangeRef":Lorg/apache/poi/ss/util/CellRangeAddress;
    invoke-direct {p0, p1, v0}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->setRepeatingRowsAndColumns(Lorg/apache/poi/ss/util/CellRangeAddress;Lorg/apache/poi/ss/util/CellRangeAddress;)V

    .line 2195
    return-void
.end method

.method public setRightToLeft(Z)V
    .locals 1
    .param p1, "value"    # Z

    .prologue
    .line 823
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_sheet:Lorg/apache/poi/hssf/model/InternalSheet;

    invoke-virtual {v0}, Lorg/apache/poi/hssf/model/InternalSheet;->getWindowTwo()Lorg/apache/poi/hssf/record/WindowTwoRecord;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/poi/hssf/record/WindowTwoRecord;->setArabic(Z)V

    .line 824
    return-void
.end method

.method public setRowBreak(I)V
    .locals 3
    .param p1, "row"    # I

    .prologue
    .line 1715
    invoke-virtual {p0, p1}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->validateRow(I)V

    .line 1716
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_sheet:Lorg/apache/poi/hssf/model/InternalSheet;

    invoke-virtual {v0}, Lorg/apache/poi/hssf/model/InternalSheet;->getPageSettings()Lorg/apache/poi/hssf/record/aggregates/PageSettingsBlock;

    move-result-object v0

    const/4 v1, 0x0

    const/16 v2, 0xff

    invoke-virtual {v0, p1, v1, v2}, Lorg/apache/poi/hssf/record/aggregates/PageSettingsBlock;->setRowBreak(ISS)V

    .line 1717
    return-void
.end method

.method public setRowGroupCollapsed(IZ)V
    .locals 1
    .param p1, "rowIndex"    # I
    .param p2, "collapse"    # Z

    .prologue
    .line 1970
    if-eqz p2, :cond_0

    .line 1971
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_sheet:Lorg/apache/poi/hssf/model/InternalSheet;

    invoke-virtual {v0}, Lorg/apache/poi/hssf/model/InternalSheet;->getRowsAggregate()Lorg/apache/poi/hssf/record/aggregates/RowRecordsAggregate;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/poi/hssf/record/aggregates/RowRecordsAggregate;->collapseRow(I)V

    .line 1975
    :goto_0
    return-void

    .line 1973
    :cond_0
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_sheet:Lorg/apache/poi/hssf/model/InternalSheet;

    invoke-virtual {v0}, Lorg/apache/poi/hssf/model/InternalSheet;->getRowsAggregate()Lorg/apache/poi/hssf/record/aggregates/RowRecordsAggregate;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/poi/hssf/record/aggregates/RowRecordsAggregate;->expandRow(I)V

    goto :goto_0
.end method

.method public setRowSumsBelow(Z)V
    .locals 3
    .param p1, "b"    # Z

    .prologue
    .line 984
    iget-object v1, p0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_sheet:Lorg/apache/poi/hssf/model/InternalSheet;

    const/16 v2, 0x81

    invoke-virtual {v1, v2}, Lorg/apache/poi/hssf/model/InternalSheet;->findFirstRecordBySid(S)Lorg/apache/poi/hssf/record/Record;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hssf/record/WSBoolRecord;

    .line 986
    .local v0, "record":Lorg/apache/poi/hssf/record/WSBoolRecord;
    invoke-virtual {v0, p1}, Lorg/apache/poi/hssf/record/WSBoolRecord;->setRowSumsBelow(Z)V

    .line 988
    invoke-virtual {v0, p1}, Lorg/apache/poi/hssf/record/WSBoolRecord;->setAlternateExpression(Z)V

    .line 989
    return-void
.end method

.method public setRowSumsRight(Z)V
    .locals 3
    .param p1, "b"    # Z

    .prologue
    .line 998
    iget-object v1, p0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_sheet:Lorg/apache/poi/hssf/model/InternalSheet;

    const/16 v2, 0x81

    invoke-virtual {v1, v2}, Lorg/apache/poi/hssf/model/InternalSheet;->findFirstRecordBySid(S)Lorg/apache/poi/hssf/record/Record;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hssf/record/WSBoolRecord;

    .line 1000
    .local v0, "record":Lorg/apache/poi/hssf/record/WSBoolRecord;
    invoke-virtual {v0, p1}, Lorg/apache/poi/hssf/record/WSBoolRecord;->setRowSumsRight(Z)V

    .line 1001
    return-void
.end method

.method public setSelected(Z)V
    .locals 1
    .param p1, "sel"    # Z

    .prologue
    .line 1161
    invoke-virtual {p0}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->getSheet()Lorg/apache/poi/hssf/model/InternalSheet;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/poi/hssf/model/InternalSheet;->getWindowTwo()Lorg/apache/poi/hssf/record/WindowTwoRecord;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/poi/hssf/record/WindowTwoRecord;->setSelected(Z)V

    .line 1162
    return-void
.end method

.method public setVerticallyCenter(Z)V
    .locals 1
    .param p1, "value"    # Z

    .prologue
    .line 779
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_sheet:Lorg/apache/poi/hssf/model/InternalSheet;

    invoke-virtual {v0}, Lorg/apache/poi/hssf/model/InternalSheet;->getPageSettings()Lorg/apache/poi/hssf/record/aggregates/PageSettingsBlock;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/poi/hssf/record/aggregates/PageSettingsBlock;->getVCenter()Lorg/apache/poi/hssf/record/VCenterRecord;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/poi/hssf/record/VCenterRecord;->setVCenter(Z)V

    .line 780
    return-void
.end method

.method public setZoom(II)V
    .locals 3
    .param p1, "numerator"    # I
    .param p2, "denominator"    # I

    .prologue
    const v2, 0xffff

    const/4 v1, 0x1

    .line 1272
    if-lt p1, v1, :cond_0

    if-le p1, v2, :cond_1

    .line 1273
    :cond_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v2, "Numerator must be greater than 1 and less than 65536"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1274
    :cond_1
    if-lt p2, v1, :cond_2

    if-le p2, v2, :cond_3

    .line 1275
    :cond_2
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v2, "Denominator must be greater than 1 and less than 65536"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1277
    :cond_3
    new-instance v0, Lorg/apache/poi/hssf/record/SCLRecord;

    invoke-direct {v0}, Lorg/apache/poi/hssf/record/SCLRecord;-><init>()V

    .line 1278
    .local v0, "sclRecord":Lorg/apache/poi/hssf/record/SCLRecord;
    int-to-short v1, p1

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/record/SCLRecord;->setNumerator(S)V

    .line 1279
    int-to-short v1, p2

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/record/SCLRecord;->setDenominator(S)V

    .line 1280
    invoke-virtual {p0}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->getSheet()Lorg/apache/poi/hssf/model/InternalSheet;

    move-result-object v1

    invoke-virtual {v1, v0}, Lorg/apache/poi/hssf/model/InternalSheet;->setSCLRecord(Lorg/apache/poi/hssf/record/SCLRecord;)V

    .line 1281
    return-void
.end method

.method protected shiftMerged(IIIZ)V
    .locals 10
    .param p1, "startRow"    # I
    .param p2, "endRow"    # I
    .param p3, "n"    # I
    .param p4, "isRow"    # Z

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 1326
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 1328
    .local v6, "shiftedRegions":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/ss/util/CellRangeAddress;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p0}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->getNumMergedRegions()I

    move-result v9

    if-lt v0, v9, :cond_0

    .line 1357
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 1358
    .local v3, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/poi/ss/util/CellRangeAddress;>;"
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-nez v7, :cond_5

    .line 1363
    return-void

    .line 1329
    .end local v3    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/poi/ss/util/CellRangeAddress;>;"
    :cond_0
    invoke-virtual {p0, v0}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->getMergedRegion(I)Lorg/apache/poi/ss/util/CellRangeAddress;

    move-result-object v4

    .line 1331
    .local v4, "merged":Lorg/apache/poi/ss/util/CellRangeAddress;
    if-eqz v4, :cond_1

    .line 1332
    invoke-virtual {v4}, Lorg/apache/poi/ss/util/CellRangeAddress;->getFirstRow()I

    move-result v9

    if-ge v9, p1, :cond_2

    .line 1333
    invoke-virtual {v4}, Lorg/apache/poi/ss/util/CellRangeAddress;->getLastRow()I

    move-result v9

    .line 1332
    if-ge v9, p1, :cond_2

    move v2, v7

    .line 1334
    .local v2, "inStart":Z
    :goto_2
    invoke-virtual {v4}, Lorg/apache/poi/ss/util/CellRangeAddress;->getFirstRow()I

    move-result v9

    if-le v9, p2, :cond_3

    .line 1335
    invoke-virtual {v4}, Lorg/apache/poi/ss/util/CellRangeAddress;->getLastRow()I

    move-result v9

    .line 1334
    if-le v9, p2, :cond_3

    move v1, v7

    .line 1338
    .local v1, "inEnd":Z
    :goto_3
    if-eqz v2, :cond_1

    if-nez v1, :cond_4

    .line 1328
    .end local v1    # "inEnd":Z
    .end local v2    # "inStart":Z
    :cond_1
    :goto_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    move v2, v8

    .line 1332
    goto :goto_2

    .restart local v2    # "inStart":Z
    :cond_3
    move v1, v8

    .line 1334
    goto :goto_3

    .line 1344
    .restart local v1    # "inEnd":Z
    :cond_4
    add-int/lit8 v9, p1, -0x1

    invoke-static {v4, v9, v7}, Lorg/apache/poi/ss/util/SheetUtil;->containsCell(Lorg/apache/poi/ss/util/CellRangeAddress;II)Z

    move-result v9

    if-nez v9, :cond_1

    .line 1345
    add-int/lit8 v9, p2, 0x1

    invoke-static {v4, v9, v7}, Lorg/apache/poi/ss/util/SheetUtil;->containsCell(Lorg/apache/poi/ss/util/CellRangeAddress;II)Z

    move-result v9

    if-nez v9, :cond_1

    .line 1346
    invoke-virtual {v4}, Lorg/apache/poi/ss/util/CellRangeAddress;->getFirstRow()I

    move-result v9

    add-int/2addr v9, p3

    invoke-virtual {v4, v9}, Lorg/apache/poi/ss/util/CellRangeAddress;->setFirstRow(I)V

    .line 1347
    invoke-virtual {v4}, Lorg/apache/poi/ss/util/CellRangeAddress;->getLastRow()I

    move-result v9

    add-int/2addr v9, p3

    invoke-virtual {v4, v9}, Lorg/apache/poi/ss/util/CellRangeAddress;->setLastRow(I)V

    .line 1349
    invoke-interface {v6, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1350
    invoke-virtual {p0, v0}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->removeMergedRegion(I)V

    .line 1351
    add-int/lit8 v0, v0, -0x1

    goto :goto_4

    .line 1359
    .end local v1    # "inEnd":Z
    .end local v2    # "inStart":Z
    .end local v4    # "merged":Lorg/apache/poi/ss/util/CellRangeAddress;
    .restart local v3    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/poi/ss/util/CellRangeAddress;>;"
    :cond_5
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/apache/poi/ss/util/CellRangeAddress;

    .line 1361
    .local v5, "region":Lorg/apache/poi/ss/util/CellRangeAddress;
    invoke-virtual {p0, v5}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->addMergedRegion(Lorg/apache/poi/ss/util/CellRangeAddress;)I

    goto :goto_1
.end method

.method public shiftRows(III)V
    .locals 6
    .param p1, "startRow"    # I
    .param p2, "endRow"    # I
    .param p3, "n"    # I

    .prologue
    const/4 v4, 0x0

    .line 1381
    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    move v5, v4

    invoke-virtual/range {v0 .. v5}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->shiftRows(IIIZZ)V

    .line 1382
    return-void
.end method

.method public shiftRows(IIIZZ)V
    .locals 7
    .param p1, "startRow"    # I
    .param p2, "endRow"    # I
    .param p3, "n"    # I
    .param p4, "copyRowHeight"    # Z
    .param p5, "resetOriginalRowHeight"    # Z

    .prologue
    .line 1402
    const/4 v6, 0x1

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v6}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->shiftRows(IIIZZZ)V

    .line 1403
    return-void
.end method

.method public shiftRows(IIIZZZ)V
    .locals 26
    .param p1, "startRow"    # I
    .param p2, "endRow"    # I
    .param p3, "n"    # I
    .param p4, "copyRowHeight"    # Z
    .param p5, "resetOriginalRowHeight"    # Z
    .param p6, "moveComments"    # Z

    .prologue
    .line 1426
    if-gez p3, :cond_4

    .line 1427
    move/from16 v20, p1

    .line 1428
    .local v20, "s":I
    const/4 v11, 0x1

    .line 1445
    .local v11, "inc":I
    :goto_0
    const/16 v24, 0x1

    move-object/from16 v0, p0

    move/from16 v1, p1

    move/from16 v2, p2

    move/from16 v3, p3

    move/from16 v4, v24

    invoke-virtual {v0, v1, v2, v3, v4}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->shiftMerged(IIIZ)V

    .line 1446
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_sheet:Lorg/apache/poi/hssf/model/InternalSheet;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Lorg/apache/poi/hssf/model/InternalSheet;->getPageSettings()Lorg/apache/poi/hssf/record/aggregates/PageSettingsBlock;

    move-result-object v24

    move-object/from16 v0, v24

    move/from16 v1, p1

    move/from16 v2, p2

    move/from16 v3, p3

    invoke-virtual {v0, v1, v2, v3}, Lorg/apache/poi/hssf/record/aggregates/PageSettingsBlock;->shiftRowBreaks(III)V

    .line 1448
    move/from16 v19, v20

    .local v19, "rowNum":I
    :goto_1
    move/from16 v0, v19

    move/from16 v1, p1

    if-lt v0, v1, :cond_0

    move/from16 v0, v19

    move/from16 v1, p2

    if-gt v0, v1, :cond_0

    if-ltz v19, :cond_0

    const/high16 v24, 0x10000

    move/from16 v0, v19

    move/from16 v1, v24

    if-lt v0, v1, :cond_5

    .line 1519
    :cond_0
    if-lez p3, :cond_12

    .line 1521
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_firstrow:I

    move/from16 v24, v0

    move/from16 v0, p1

    move/from16 v1, v24

    if-ne v0, v1, :cond_1

    .line 1523
    add-int v24, p1, p3

    const/16 v25, 0x0

    invoke-static/range {v24 .. v25}, Ljava/lang/Math;->max(II)I

    move-result v24

    move/from16 v0, v24

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_firstrow:I

    .line 1524
    add-int/lit8 v10, p1, 0x1

    .local v10, "i":I
    :goto_2
    add-int v24, p1, p3

    move/from16 v0, v24

    if-lt v10, v0, :cond_10

    .line 1531
    .end local v10    # "i":I
    :cond_1
    :goto_3
    add-int v24, p2, p3

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_lastrow:I

    move/from16 v25, v0

    move/from16 v0, v24

    move/from16 v1, v25

    if-le v0, v1, :cond_2

    .line 1532
    add-int v24, p2, p3

    sget-object v25, Lorg/apache/poi/ss/SpreadsheetVersion;->EXCEL97:Lorg/apache/poi/ss/SpreadsheetVersion;

    invoke-virtual/range {v25 .. v25}, Lorg/apache/poi/ss/SpreadsheetVersion;->getLastRowIndex()I

    move-result v25

    invoke-static/range {v24 .. v25}, Ljava/lang/Math;->min(II)I

    move-result v24

    move/from16 v0, v24

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_lastrow:I

    .line 1553
    :cond_2
    :goto_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_workbook:Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->getSheetIndex(Lorg/apache/poi/ss/usermodel/Sheet;)I

    move-result v22

    .line 1554
    .local v22, "sheetIndex":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_book:Lorg/apache/poi/hssf/model/InternalWorkbook;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/model/InternalWorkbook;->checkExternSheet(I)S

    move-result v9

    .line 1555
    .local v9, "externSheetIndex":S
    move/from16 v0, p1

    move/from16 v1, p2

    move/from16 v2, p3

    invoke-static {v9, v0, v1, v2}, Lorg/apache/poi/ss/formula/FormulaShifter;->createForRowShift(IIII)Lorg/apache/poi/ss/formula/FormulaShifter;

    move-result-object v23

    .line 1556
    .local v23, "shifter":Lorg/apache/poi/ss/formula/FormulaShifter;
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_sheet:Lorg/apache/poi/hssf/model/InternalSheet;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    move-object/from16 v1, v23

    invoke-virtual {v0, v1, v9}, Lorg/apache/poi/hssf/model/InternalSheet;->updateFormulasAfterCellShift(Lorg/apache/poi/ss/formula/FormulaShifter;I)V

    .line 1558
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_workbook:Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->getNumberOfSheets()I

    move-result v13

    .line 1559
    .local v13, "nSheets":I
    const/4 v10, 0x0

    .restart local v10    # "i":I
    :goto_5
    if-lt v10, v13, :cond_15

    .line 1567
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_workbook:Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->getWorkbook()Lorg/apache/poi/hssf/model/InternalWorkbook;

    move-result-object v24

    move-object/from16 v0, v24

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/model/InternalWorkbook;->updateNamesAfterCellShift(Lorg/apache/poi/ss/formula/FormulaShifter;)V

    .line 1568
    .end local v9    # "externSheetIndex":S
    .end local v10    # "i":I
    .end local v11    # "inc":I
    .end local v13    # "nSheets":I
    .end local v19    # "rowNum":I
    .end local v20    # "s":I
    .end local v22    # "sheetIndex":I
    .end local v23    # "shifter":Lorg/apache/poi/ss/formula/FormulaShifter;
    :cond_3
    return-void

    .line 1429
    :cond_4
    if-lez p3, :cond_3

    .line 1430
    move/from16 v20, p2

    .line 1431
    .restart local v20    # "s":I
    const/4 v11, -0x1

    .line 1432
    .restart local v11    # "inc":I
    goto/16 :goto_0

    .line 1449
    .restart local v19    # "rowNum":I
    :cond_5
    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->getRow(I)Lorg/apache/poi/hssf/usermodel/HSSFRow;

    move-result-object v17

    .line 1453
    .local v17, "row":Lorg/apache/poi/hssf/usermodel/HSSFRow;
    if-eqz v17, :cond_6

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v0, v1}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->notifyRowShifting(Lorg/apache/poi/hssf/usermodel/HSSFRow;)V

    .line 1455
    :cond_6
    add-int v24, v19, p3

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->getRow(I)Lorg/apache/poi/hssf/usermodel/HSSFRow;

    move-result-object v18

    .line 1456
    .local v18, "row2Replace":Lorg/apache/poi/hssf/usermodel/HSSFRow;
    if-nez v18, :cond_7

    .line 1457
    add-int v24, v19, p3

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->createRow(I)Lorg/apache/poi/hssf/usermodel/HSSFRow;

    move-result-object v18

    .line 1465
    :cond_7
    invoke-virtual/range {v18 .. v18}, Lorg/apache/poi/hssf/usermodel/HSSFRow;->removeAllCells()V

    .line 1469
    if-nez v17, :cond_9

    .line 1448
    :cond_8
    add-int v19, v19, v11

    goto/16 :goto_1

    .line 1472
    :cond_9
    if-eqz p4, :cond_a

    .line 1473
    invoke-virtual/range {v17 .. v17}, Lorg/apache/poi/hssf/usermodel/HSSFRow;->getHeight()S

    move-result v24

    move-object/from16 v0, v18

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/usermodel/HSSFRow;->setHeight(S)V

    .line 1475
    :cond_a
    if-eqz p5, :cond_b

    .line 1476
    const/16 v24, 0xff

    move-object/from16 v0, v17

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/usermodel/HSSFRow;->setHeight(S)V

    .line 1481
    :cond_b
    invoke-virtual/range {v17 .. v17}, Lorg/apache/poi/hssf/usermodel/HSSFRow;->cellIterator()Ljava/util/Iterator;

    move-result-object v7

    .local v7, "cells":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/poi/ss/usermodel/Cell;>;"
    :cond_c
    :goto_6
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v24

    if-nez v24, :cond_e

    .line 1496
    invoke-virtual/range {v17 .. v17}, Lorg/apache/poi/hssf/usermodel/HSSFRow;->removeAllCells()V

    .line 1501
    if-eqz p6, :cond_8

    .line 1503
    invoke-virtual/range {p0 .. p0}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->createDrawingPatriarch()Lorg/apache/poi/hssf/usermodel/HSSFPatriarch;

    move-result-object v16

    .line 1504
    .local v16, "patriarch":Lorg/apache/poi/hssf/usermodel/HSSFPatriarch;
    invoke-virtual/range {v16 .. v16}, Lorg/apache/poi/hssf/usermodel/HSSFPatriarch;->getChildren()Ljava/util/List;

    move-result-object v24

    invoke-interface/range {v24 .. v24}, Ljava/util/List;->size()I

    move-result v24

    add-int/lit8 v10, v24, -0x1

    .restart local v10    # "i":I
    :goto_7
    if-ltz v10, :cond_8

    .line 1505
    invoke-virtual/range {v16 .. v16}, Lorg/apache/poi/hssf/usermodel/HSSFPatriarch;->getChildren()Ljava/util/List;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-interface {v0, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Lorg/apache/poi/hssf/usermodel/HSSFShape;

    .line 1506
    .local v21, "shape":Lorg/apache/poi/hssf/usermodel/HSSFShape;
    move-object/from16 v0, v21

    instance-of v0, v0, Lorg/apache/poi/hssf/usermodel/HSSFComment;

    move/from16 v24, v0

    if-nez v24, :cond_f

    .line 1504
    :cond_d
    :goto_8
    add-int/lit8 v10, v10, -0x1

    goto :goto_7

    .line 1482
    .end local v10    # "i":I
    .end local v16    # "patriarch":Lorg/apache/poi/hssf/usermodel/HSSFPatriarch;
    .end local v21    # "shape":Lorg/apache/poi/hssf/usermodel/HSSFShape;
    :cond_e
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/apache/poi/hssf/usermodel/HSSFCell;

    .line 1483
    .local v5, "cell":Lorg/apache/poi/hssf/usermodel/HSSFCell;
    move-object/from16 v0, v17

    invoke-virtual {v0, v5}, Lorg/apache/poi/hssf/usermodel/HSSFRow;->removeCell(Lorg/apache/poi/ss/usermodel/Cell;)V

    .line 1484
    invoke-virtual {v5}, Lorg/apache/poi/hssf/usermodel/HSSFCell;->getCellValueRecord()Lorg/apache/poi/hssf/record/CellValueRecordInterface;

    move-result-object v6

    .line 1485
    .local v6, "cellRecord":Lorg/apache/poi/hssf/record/CellValueRecordInterface;
    add-int v24, v19, p3

    move/from16 v0, v24

    invoke-interface {v6, v0}, Lorg/apache/poi/hssf/record/CellValueRecordInterface;->setRow(I)V

    .line 1486
    move-object/from16 v0, v18

    invoke-virtual {v0, v6}, Lorg/apache/poi/hssf/usermodel/HSSFRow;->createCellFromRecord(Lorg/apache/poi/hssf/record/CellValueRecordInterface;)Lorg/apache/poi/hssf/usermodel/HSSFCell;

    .line 1487
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_sheet:Lorg/apache/poi/hssf/model/InternalSheet;

    move-object/from16 v24, v0

    add-int v25, v19, p3

    move-object/from16 v0, v24

    move/from16 v1, v25

    invoke-virtual {v0, v1, v6}, Lorg/apache/poi/hssf/model/InternalSheet;->addValueRecord(ILorg/apache/poi/hssf/record/CellValueRecordInterface;)V

    .line 1489
    invoke-virtual {v5}, Lorg/apache/poi/hssf/usermodel/HSSFCell;->getHyperlink()Lorg/apache/poi/hssf/usermodel/HSSFHyperlink;

    move-result-object v12

    .line 1490
    .local v12, "link":Lorg/apache/poi/hssf/usermodel/HSSFHyperlink;
    if-eqz v12, :cond_c

    .line 1491
    invoke-virtual {v12}, Lorg/apache/poi/hssf/usermodel/HSSFHyperlink;->getFirstRow()I

    move-result v24

    add-int v24, v24, p3

    move/from16 v0, v24

    invoke-virtual {v12, v0}, Lorg/apache/poi/hssf/usermodel/HSSFHyperlink;->setFirstRow(I)V

    .line 1492
    invoke-virtual {v12}, Lorg/apache/poi/hssf/usermodel/HSSFHyperlink;->getLastRow()I

    move-result v24

    add-int v24, v24, p3

    move/from16 v0, v24

    invoke-virtual {v12, v0}, Lorg/apache/poi/hssf/usermodel/HSSFHyperlink;->setLastRow(I)V

    goto :goto_6

    .end local v5    # "cell":Lorg/apache/poi/hssf/usermodel/HSSFCell;
    .end local v6    # "cellRecord":Lorg/apache/poi/hssf/record/CellValueRecordInterface;
    .end local v12    # "link":Lorg/apache/poi/hssf/usermodel/HSSFHyperlink;
    .restart local v10    # "i":I
    .restart local v16    # "patriarch":Lorg/apache/poi/hssf/usermodel/HSSFPatriarch;
    .restart local v21    # "shape":Lorg/apache/poi/hssf/usermodel/HSSFShape;
    :cond_f
    move-object/from16 v8, v21

    .line 1509
    check-cast v8, Lorg/apache/poi/hssf/usermodel/HSSFComment;

    .line 1510
    .local v8, "comment":Lorg/apache/poi/hssf/usermodel/HSSFComment;
    invoke-virtual {v8}, Lorg/apache/poi/hssf/usermodel/HSSFComment;->getRow()I

    move-result v24

    move/from16 v0, v24

    move/from16 v1, v19

    if-ne v0, v1, :cond_d

    .line 1513
    add-int v24, v19, p3

    move/from16 v0, v24

    invoke-virtual {v8, v0}, Lorg/apache/poi/hssf/usermodel/HSSFComment;->setRow(I)V

    goto :goto_8

    .line 1525
    .end local v7    # "cells":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/poi/ss/usermodel/Cell;>;"
    .end local v8    # "comment":Lorg/apache/poi/hssf/usermodel/HSSFComment;
    .end local v16    # "patriarch":Lorg/apache/poi/hssf/usermodel/HSSFPatriarch;
    .end local v17    # "row":Lorg/apache/poi/hssf/usermodel/HSSFRow;
    .end local v18    # "row2Replace":Lorg/apache/poi/hssf/usermodel/HSSFRow;
    .end local v21    # "shape":Lorg/apache/poi/hssf/usermodel/HSSFShape;
    :cond_10
    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->getRow(I)Lorg/apache/poi/hssf/usermodel/HSSFRow;

    move-result-object v24

    if-eqz v24, :cond_11

    .line 1526
    move-object/from16 v0, p0

    iput v10, v0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_firstrow:I

    goto/16 :goto_3

    .line 1524
    :cond_11
    add-int/lit8 v10, v10, 0x1

    goto/16 :goto_2

    .line 1536
    .end local v10    # "i":I
    :cond_12
    add-int v24, p1, p3

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_firstrow:I

    move/from16 v25, v0

    move/from16 v0, v24

    move/from16 v1, v25

    if-ge v0, v1, :cond_13

    .line 1537
    add-int v24, p1, p3

    const/16 v25, 0x0

    invoke-static/range {v24 .. v25}, Ljava/lang/Math;->max(II)I

    move-result v24

    move/from16 v0, v24

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_firstrow:I

    .line 1539
    :cond_13
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_lastrow:I

    move/from16 v24, v0

    move/from16 v0, p2

    move/from16 v1, v24

    if-ne v0, v1, :cond_2

    .line 1541
    add-int v24, p2, p3

    sget-object v25, Lorg/apache/poi/ss/SpreadsheetVersion;->EXCEL97:Lorg/apache/poi/ss/SpreadsheetVersion;

    invoke-virtual/range {v25 .. v25}, Lorg/apache/poi/ss/SpreadsheetVersion;->getLastRowIndex()I

    move-result v25

    invoke-static/range {v24 .. v25}, Ljava/lang/Math;->min(II)I

    move-result v24

    move/from16 v0, v24

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_lastrow:I

    .line 1542
    add-int/lit8 v10, p2, -0x1

    .restart local v10    # "i":I
    :goto_9
    add-int v24, p2, p3

    move/from16 v0, v24

    if-le v10, v0, :cond_2

    .line 1543
    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->getRow(I)Lorg/apache/poi/hssf/usermodel/HSSFRow;

    move-result-object v24

    if-eqz v24, :cond_14

    .line 1544
    move-object/from16 v0, p0

    iput v10, v0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_lastrow:I

    goto/16 :goto_4

    .line 1542
    :cond_14
    add-int/lit8 v10, v10, 0x1

    goto :goto_9

    .line 1560
    .restart local v9    # "externSheetIndex":S
    .restart local v13    # "nSheets":I
    .restart local v22    # "sheetIndex":I
    .restart local v23    # "shifter":Lorg/apache/poi/ss/formula/FormulaShifter;
    :cond_15
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_workbook:Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    invoke-virtual {v0, v10}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->getSheetAt(I)Lorg/apache/poi/hssf/usermodel/HSSFSheet;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->getSheet()Lorg/apache/poi/hssf/model/InternalSheet;

    move-result-object v15

    .line 1561
    .local v15, "otherSheet":Lorg/apache/poi/hssf/model/InternalSheet;
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_sheet:Lorg/apache/poi/hssf/model/InternalSheet;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    if-ne v15, v0, :cond_16

    .line 1559
    :goto_a
    add-int/lit8 v10, v10, 0x1

    goto/16 :goto_5

    .line 1564
    :cond_16
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_book:Lorg/apache/poi/hssf/model/InternalWorkbook;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    invoke-virtual {v0, v10}, Lorg/apache/poi/hssf/model/InternalWorkbook;->checkExternSheet(I)S

    move-result v14

    .line 1565
    .local v14, "otherExtSheetIx":S
    move-object/from16 v0, v23

    invoke-virtual {v15, v0, v14}, Lorg/apache/poi/hssf/model/InternalSheet;->updateFormulasAfterCellShift(Lorg/apache/poi/ss/formula/FormulaShifter;I)V

    goto :goto_a
.end method

.method public showInPane(SS)V
    .locals 1
    .param p1, "toprow"    # S
    .param p2, "leftcol"    # S

    .prologue
    .line 1311
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_sheet:Lorg/apache/poi/hssf/model/InternalSheet;

    invoke-virtual {v0, p1}, Lorg/apache/poi/hssf/model/InternalSheet;->setTopRow(S)V

    .line 1312
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_sheet:Lorg/apache/poi/hssf/model/InternalSheet;

    invoke-virtual {v0, p2}, Lorg/apache/poi/hssf/model/InternalSheet;->setLeftCol(S)V

    .line 1313
    return-void
.end method

.method public ungroupColumn(II)V
    .locals 2
    .param p1, "fromColumn"    # I
    .param p2, "toColumn"    # I

    .prologue
    .line 1952
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_sheet:Lorg/apache/poi/hssf/model/InternalSheet;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, p2, v1}, Lorg/apache/poi/hssf/model/InternalSheet;->groupColumnRange(IIZ)V

    .line 1953
    return-void
.end method

.method public ungroupColumn(SS)V
    .locals 2
    .param p1, "fromColumn"    # S
    .param p2, "toColumn"    # S

    .prologue
    const v1, 0xffff

    .line 1928
    and-int v0, p1, v1

    and-int/2addr v1, p2

    invoke-virtual {p0, v0, v1}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->ungroupColumn(II)V

    .line 1929
    return-void
.end method

.method public ungroupRow(II)V
    .locals 2
    .param p1, "fromRow"    # I
    .param p2, "toRow"    # I

    .prologue
    .line 1966
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->_sheet:Lorg/apache/poi/hssf/model/InternalSheet;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, p2, v1}, Lorg/apache/poi/hssf/model/InternalSheet;->groupRowRange(IIZ)V

    .line 1967
    return-void
.end method

.method protected validateColumn(I)V
    .locals 4
    .param p1, "column"    # I

    .prologue
    .line 1802
    sget-object v1, Lorg/apache/poi/ss/SpreadsheetVersion;->EXCEL97:Lorg/apache/poi/ss/SpreadsheetVersion;

    invoke-virtual {v1}, Lorg/apache/poi/ss/SpreadsheetVersion;->getLastColumnIndex()I

    move-result v0

    .line 1803
    .local v0, "maxcol":I
    if-le p1, v0, :cond_0

    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Maximum column number is "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1804
    :cond_0
    if-gez p1, :cond_1

    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v2, "Minimum column number is 0"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1805
    :cond_1
    return-void
.end method

.method protected validateRow(I)V
    .locals 4
    .param p1, "row"    # I

    .prologue
    .line 1791
    sget-object v1, Lorg/apache/poi/ss/SpreadsheetVersion;->EXCEL97:Lorg/apache/poi/ss/SpreadsheetVersion;

    invoke-virtual {v1}, Lorg/apache/poi/ss/SpreadsheetVersion;->getLastRowIndex()I

    move-result v0

    .line 1792
    .local v0, "maxrow":I
    if-le p1, v0, :cond_0

    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Maximum row number is "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1793
    :cond_0
    if-gez p1, :cond_1

    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v2, "Minumum row number is 0"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1794
    :cond_1
    return-void
.end method
