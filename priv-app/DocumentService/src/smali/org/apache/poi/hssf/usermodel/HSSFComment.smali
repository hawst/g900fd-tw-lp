.class public Lorg/apache/poi/hssf/usermodel/HSSFComment;
.super Lorg/apache/poi/hssf/usermodel/HSSFTextbox;
.source "HSSFComment.java"

# interfaces
.implements Lorg/apache/poi/ss/usermodel/Comment;


# static fields
.field private static final FILL_TYPE_PICTURE:I = 0x3

.field private static final FILL_TYPE_SOLID:I = 0x0

.field private static final GROUP_SHAPE_HIDDEN_MASK:I = 0x1000002

.field private static final GROUP_SHAPE_NOT_HIDDEN_MASK:I = -0x1000003

.field private static final GROUP_SHAPE_PROPERTY_DEFAULT_VALUE:I = 0xa0002


# instance fields
.field private _note:Lorg/apache/poi/hssf/record/NoteRecord;


# direct methods
.method public constructor <init>(Lorg/apache/poi/ddf/EscherContainerRecord;Lorg/apache/poi/hssf/record/ObjRecord;Lorg/apache/poi/hssf/record/TextObjectRecord;Lorg/apache/poi/hssf/record/NoteRecord;)V
    .locals 0
    .param p1, "spContainer"    # Lorg/apache/poi/ddf/EscherContainerRecord;
    .param p2, "objRecord"    # Lorg/apache/poi/hssf/record/ObjRecord;
    .param p3, "textObjectRecord"    # Lorg/apache/poi/hssf/record/TextObjectRecord;
    .param p4, "_note"    # Lorg/apache/poi/hssf/record/NoteRecord;

    .prologue
    .line 50
    invoke-direct {p0, p1, p2, p3}, Lorg/apache/poi/hssf/usermodel/HSSFTextbox;-><init>(Lorg/apache/poi/ddf/EscherContainerRecord;Lorg/apache/poi/hssf/record/ObjRecord;Lorg/apache/poi/hssf/record/TextObjectRecord;)V

    .line 51
    iput-object p4, p0, Lorg/apache/poi/hssf/usermodel/HSSFComment;->_note:Lorg/apache/poi/hssf/record/NoteRecord;

    .line 52
    return-void
.end method

.method protected constructor <init>(Lorg/apache/poi/hssf/record/NoteRecord;Lorg/apache/poi/hssf/record/TextObjectRecord;)V
    .locals 2
    .param p1, "note"    # Lorg/apache/poi/hssf/record/NoteRecord;
    .param p2, "txo"    # Lorg/apache/poi/hssf/record/TextObjectRecord;

    .prologue
    .line 74
    const/4 v0, 0x0

    new-instance v1, Lorg/apache/poi/hssf/usermodel/HSSFClientAnchor;

    invoke-direct {v1}, Lorg/apache/poi/hssf/usermodel/HSSFClientAnchor;-><init>()V

    invoke-direct {p0, v0, v1}, Lorg/apache/poi/hssf/usermodel/HSSFComment;-><init>(Lorg/apache/poi/hssf/usermodel/HSSFShape;Lorg/apache/poi/hssf/usermodel/HSSFAnchor;)V

    .line 75
    iput-object p1, p0, Lorg/apache/poi/hssf/usermodel/HSSFComment;->_note:Lorg/apache/poi/hssf/record/NoteRecord;

    .line 76
    return-void
.end method

.method public constructor <init>(Lorg/apache/poi/hssf/usermodel/HSSFShape;Lorg/apache/poi/hssf/usermodel/HSSFAnchor;)V
    .locals 3
    .param p1, "parent"    # Lorg/apache/poi/hssf/usermodel/HSSFShape;
    .param p2, "anchor"    # Lorg/apache/poi/hssf/usermodel/HSSFAnchor;

    .prologue
    const/4 v2, 0x0

    .line 61
    invoke-direct {p0, p1, p2}, Lorg/apache/poi/hssf/usermodel/HSSFTextbox;-><init>(Lorg/apache/poi/hssf/usermodel/HSSFShape;Lorg/apache/poi/hssf/usermodel/HSSFAnchor;)V

    .line 62
    invoke-direct {p0}, Lorg/apache/poi/hssf/usermodel/HSSFComment;->createNoteRecord()Lorg/apache/poi/hssf/record/NoteRecord;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/poi/hssf/usermodel/HSSFComment;->_note:Lorg/apache/poi/hssf/record/NoteRecord;

    .line 64
    const v1, 0x8000050

    invoke-virtual {p0, v1}, Lorg/apache/poi/hssf/usermodel/HSSFComment;->setFillColor(I)V

    .line 67
    invoke-virtual {p0, v2}, Lorg/apache/poi/hssf/usermodel/HSSFComment;->setVisible(Z)V

    .line 68
    const-string/jumbo v1, ""

    invoke-virtual {p0, v1}, Lorg/apache/poi/hssf/usermodel/HSSFComment;->setAuthor(Ljava/lang/String;)V

    .line 69
    invoke-virtual {p0}, Lorg/apache/poi/hssf/usermodel/HSSFComment;->getObjRecord()Lorg/apache/poi/hssf/record/ObjRecord;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/poi/hssf/record/ObjRecord;->getSubRecords()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hssf/record/CommonObjectDataSubRecord;

    .line 70
    .local v0, "cod":Lorg/apache/poi/hssf/record/CommonObjectDataSubRecord;
    const/16 v1, 0x19

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/record/CommonObjectDataSubRecord;->setObjectType(S)V

    .line 71
    return-void
.end method

.method private createNoteRecord()Lorg/apache/poi/hssf/record/NoteRecord;
    .locals 2

    .prologue
    .line 115
    new-instance v0, Lorg/apache/poi/hssf/record/NoteRecord;

    invoke-direct {v0}, Lorg/apache/poi/hssf/record/NoteRecord;-><init>()V

    .line 116
    .local v0, "note":Lorg/apache/poi/hssf/record/NoteRecord;
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/record/NoteRecord;->setFlags(S)V

    .line 117
    const-string/jumbo v1, ""

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/record/NoteRecord;->setAuthor(Ljava/lang/String;)V

    .line 118
    return-object v0
.end method

.method private setHidden(Z)V
    .locals 6
    .param p1, "value"    # Z

    .prologue
    const/16 v5, 0x3bf

    const/4 v4, 0x0

    .line 261
    invoke-virtual {p0}, Lorg/apache/poi/hssf/usermodel/HSSFComment;->getOptRecord()Lorg/apache/poi/ddf/EscherOptRecord;

    move-result-object v1

    invoke-virtual {v1, v5}, Lorg/apache/poi/ddf/EscherOptRecord;->lookup(I)Lorg/apache/poi/ddf/EscherProperty;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/ddf/EscherSimpleProperty;

    .line 264
    .local v0, "property":Lorg/apache/poi/ddf/EscherSimpleProperty;
    if-eqz v0, :cond_1

    if-eqz p1, :cond_1

    .line 265
    new-instance v1, Lorg/apache/poi/ddf/EscherSimpleProperty;

    invoke-virtual {v0}, Lorg/apache/poi/ddf/EscherSimpleProperty;->getPropertyValue()I

    move-result v2

    const v3, 0x1000002

    or-int/2addr v2, v3

    invoke-direct {v1, v5, v4, v4, v2}, Lorg/apache/poi/ddf/EscherSimpleProperty;-><init>(SZZI)V

    invoke-virtual {p0, v1}, Lorg/apache/poi/hssf/usermodel/HSSFComment;->setPropertyValue(Lorg/apache/poi/ddf/EscherProperty;)V

    .line 271
    :cond_0
    :goto_0
    return-void

    .line 267
    :cond_1
    if-eqz v0, :cond_0

    .line 268
    new-instance v1, Lorg/apache/poi/ddf/EscherSimpleProperty;

    invoke-virtual {v0}, Lorg/apache/poi/ddf/EscherSimpleProperty;->getPropertyValue()I

    move-result v2

    const v3, -0x1000003

    and-int/2addr v2, v3

    invoke-direct {v1, v5, v4, v4, v2}, Lorg/apache/poi/ddf/EscherSimpleProperty;-><init>(SZZI)V

    invoke-virtual {p0, v1}, Lorg/apache/poi/hssf/usermodel/HSSFComment;->setPropertyValue(Lorg/apache/poi/ddf/EscherProperty;)V

    goto :goto_0
.end method


# virtual methods
.method afterInsert(Lorg/apache/poi/hssf/usermodel/HSSFPatriarch;)V
    .locals 2
    .param p1, "patriarch"    # Lorg/apache/poi/hssf/usermodel/HSSFPatriarch;

    .prologue
    .line 80
    invoke-super {p0, p1}, Lorg/apache/poi/hssf/usermodel/HSSFTextbox;->afterInsert(Lorg/apache/poi/hssf/usermodel/HSSFPatriarch;)V

    .line 81
    invoke-virtual {p1}, Lorg/apache/poi/hssf/usermodel/HSSFPatriarch;->_getBoundAggregate()Lorg/apache/poi/hssf/record/EscherAggregate;

    move-result-object v0

    invoke-virtual {p0}, Lorg/apache/poi/hssf/usermodel/HSSFComment;->getNoteRecord()Lorg/apache/poi/hssf/record/NoteRecord;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/record/EscherAggregate;->addTailRecord(Lorg/apache/poi/hssf/record/NoteRecord;)V

    .line 82
    return-void
.end method

.method public afterRemove(Lorg/apache/poi/hssf/usermodel/HSSFPatriarch;)V
    .locals 2
    .param p1, "patriarch"    # Lorg/apache/poi/hssf/usermodel/HSSFPatriarch;

    .prologue
    .line 223
    invoke-super {p0, p1}, Lorg/apache/poi/hssf/usermodel/HSSFTextbox;->afterRemove(Lorg/apache/poi/hssf/usermodel/HSSFPatriarch;)V

    .line 224
    invoke-virtual {p1}, Lorg/apache/poi/hssf/usermodel/HSSFPatriarch;->_getBoundAggregate()Lorg/apache/poi/hssf/record/EscherAggregate;

    move-result-object v0

    invoke-virtual {p0}, Lorg/apache/poi/hssf/usermodel/HSSFComment;->getNoteRecord()Lorg/apache/poi/hssf/record/NoteRecord;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/record/EscherAggregate;->removeTailRecord(Lorg/apache/poi/hssf/record/NoteRecord;)V

    .line 225
    return-void
.end method

.method protected cloneShape()Lorg/apache/poi/hssf/usermodel/HSSFShape;
    .locals 7

    .prologue
    .line 229
    invoke-virtual {p0}, Lorg/apache/poi/hssf/usermodel/HSSFComment;->getTextObjectRecord()Lorg/apache/poi/hssf/record/TextObjectRecord;

    move-result-object v5

    invoke-virtual {v5}, Lorg/apache/poi/hssf/record/TextObjectRecord;->cloneViaReserialise()Lorg/apache/poi/hssf/record/Record;

    move-result-object v4

    check-cast v4, Lorg/apache/poi/hssf/record/TextObjectRecord;

    .line 230
    .local v4, "txo":Lorg/apache/poi/hssf/record/TextObjectRecord;
    new-instance v3, Lorg/apache/poi/ddf/EscherContainerRecord;

    invoke-direct {v3}, Lorg/apache/poi/ddf/EscherContainerRecord;-><init>()V

    .line 231
    .local v3, "spContainer":Lorg/apache/poi/ddf/EscherContainerRecord;
    invoke-virtual {p0}, Lorg/apache/poi/hssf/usermodel/HSSFComment;->getEscherContainer()Lorg/apache/poi/ddf/EscherContainerRecord;

    move-result-object v5

    invoke-virtual {v5}, Lorg/apache/poi/ddf/EscherContainerRecord;->serialize()[B

    move-result-object v0

    .line 232
    .local v0, "inSp":[B
    const/4 v5, 0x0

    new-instance v6, Lorg/apache/poi/ddf/DefaultEscherRecordFactory;

    invoke-direct {v6}, Lorg/apache/poi/ddf/DefaultEscherRecordFactory;-><init>()V

    invoke-virtual {v3, v0, v5, v6}, Lorg/apache/poi/ddf/EscherContainerRecord;->fillFields([BILorg/apache/poi/ddf/EscherRecordFactory;)I

    .line 233
    invoke-virtual {p0}, Lorg/apache/poi/hssf/usermodel/HSSFComment;->getObjRecord()Lorg/apache/poi/hssf/record/ObjRecord;

    move-result-object v5

    invoke-virtual {v5}, Lorg/apache/poi/hssf/record/ObjRecord;->cloneViaReserialise()Lorg/apache/poi/hssf/record/Record;

    move-result-object v2

    check-cast v2, Lorg/apache/poi/hssf/record/ObjRecord;

    .line 234
    .local v2, "obj":Lorg/apache/poi/hssf/record/ObjRecord;
    invoke-virtual {p0}, Lorg/apache/poi/hssf/usermodel/HSSFComment;->getNoteRecord()Lorg/apache/poi/hssf/record/NoteRecord;

    move-result-object v5

    invoke-virtual {v5}, Lorg/apache/poi/hssf/record/NoteRecord;->cloneViaReserialise()Lorg/apache/poi/hssf/record/Record;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/hssf/record/NoteRecord;

    .line 235
    .local v1, "note":Lorg/apache/poi/hssf/record/NoteRecord;
    new-instance v5, Lorg/apache/poi/hssf/usermodel/HSSFComment;

    invoke-direct {v5, v3, v2, v4, v1}, Lorg/apache/poi/hssf/usermodel/HSSFComment;-><init>(Lorg/apache/poi/ddf/EscherContainerRecord;Lorg/apache/poi/hssf/record/ObjRecord;Lorg/apache/poi/hssf/record/TextObjectRecord;Lorg/apache/poi/hssf/record/NoteRecord;)V

    return-object v5
.end method

.method protected createObjRecord()Lorg/apache/poi/hssf/record/ObjRecord;
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 98
    new-instance v2, Lorg/apache/poi/hssf/record/ObjRecord;

    invoke-direct {v2}, Lorg/apache/poi/hssf/record/ObjRecord;-><init>()V

    .line 99
    .local v2, "obj":Lorg/apache/poi/hssf/record/ObjRecord;
    new-instance v0, Lorg/apache/poi/hssf/record/CommonObjectDataSubRecord;

    invoke-direct {v0}, Lorg/apache/poi/hssf/record/CommonObjectDataSubRecord;-><init>()V

    .line 100
    .local v0, "c":Lorg/apache/poi/hssf/record/CommonObjectDataSubRecord;
    const/16 v4, 0xca

    invoke-virtual {v0, v4}, Lorg/apache/poi/hssf/record/CommonObjectDataSubRecord;->setObjectType(S)V

    .line 101
    invoke-virtual {v0, v5}, Lorg/apache/poi/hssf/record/CommonObjectDataSubRecord;->setLocked(Z)V

    .line 102
    invoke-virtual {v0, v5}, Lorg/apache/poi/hssf/record/CommonObjectDataSubRecord;->setPrintable(Z)V

    .line 103
    const/4 v4, 0x0

    invoke-virtual {v0, v4}, Lorg/apache/poi/hssf/record/CommonObjectDataSubRecord;->setAutofill(Z)V

    .line 104
    invoke-virtual {v0, v5}, Lorg/apache/poi/hssf/record/CommonObjectDataSubRecord;->setAutoline(Z)V

    .line 106
    new-instance v3, Lorg/apache/poi/hssf/record/NoteStructureSubRecord;

    invoke-direct {v3}, Lorg/apache/poi/hssf/record/NoteStructureSubRecord;-><init>()V

    .line 107
    .local v3, "u":Lorg/apache/poi/hssf/record/NoteStructureSubRecord;
    new-instance v1, Lorg/apache/poi/hssf/record/EndSubRecord;

    invoke-direct {v1}, Lorg/apache/poi/hssf/record/EndSubRecord;-><init>()V

    .line 108
    .local v1, "e":Lorg/apache/poi/hssf/record/EndSubRecord;
    invoke-virtual {v2, v0}, Lorg/apache/poi/hssf/record/ObjRecord;->addSubRecord(Lorg/apache/poi/hssf/record/SubRecord;)Z

    .line 109
    invoke-virtual {v2, v3}, Lorg/apache/poi/hssf/record/ObjRecord;->addSubRecord(Lorg/apache/poi/hssf/record/SubRecord;)Z

    .line 110
    invoke-virtual {v2, v1}, Lorg/apache/poi/hssf/record/ObjRecord;->addSubRecord(Lorg/apache/poi/hssf/record/SubRecord;)Z

    .line 111
    return-object v2
.end method

.method protected createSpContainer()Lorg/apache/poi/ddf/EscherContainerRecord;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 86
    invoke-super {p0}, Lorg/apache/poi/hssf/usermodel/HSSFTextbox;->createSpContainer()Lorg/apache/poi/ddf/EscherContainerRecord;

    move-result-object v1

    .line 87
    .local v1, "spContainer":Lorg/apache/poi/ddf/EscherContainerRecord;
    const/16 v2, -0xff5

    invoke-virtual {v1, v2}, Lorg/apache/poi/ddf/EscherContainerRecord;->getChildById(S)Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/ddf/EscherOptRecord;

    .line 88
    .local v0, "opt":Lorg/apache/poi/ddf/EscherOptRecord;
    const/16 v2, 0x81

    invoke-virtual {v0, v2}, Lorg/apache/poi/ddf/EscherOptRecord;->removeEscherProperty(I)V

    .line 89
    const/16 v2, 0x83

    invoke-virtual {v0, v2}, Lorg/apache/poi/ddf/EscherOptRecord;->removeEscherProperty(I)V

    .line 90
    const/16 v2, 0x82

    invoke-virtual {v0, v2}, Lorg/apache/poi/ddf/EscherOptRecord;->removeEscherProperty(I)V

    .line 91
    const/16 v2, 0x84

    invoke-virtual {v0, v2}, Lorg/apache/poi/ddf/EscherOptRecord;->removeEscherProperty(I)V

    .line 92
    new-instance v2, Lorg/apache/poi/ddf/EscherSimpleProperty;

    const/16 v3, 0x3bf

    const v4, 0xa0002

    invoke-direct {v2, v3, v5, v5, v4}, Lorg/apache/poi/ddf/EscherSimpleProperty;-><init>(SZZI)V

    invoke-virtual {v0, v2}, Lorg/apache/poi/ddf/EscherOptRecord;->setEscherProperty(Lorg/apache/poi/ddf/EscherProperty;)V

    .line 93
    return-object v1
.end method

.method public getAuthor()Ljava/lang/String;
    .locals 1

    .prologue
    .line 198
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFComment;->_note:Lorg/apache/poi/hssf/record/NoteRecord;

    invoke-virtual {v0}, Lorg/apache/poi/hssf/record/NoteRecord;->getAuthor()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getBackgroundImageId()I
    .locals 3

    .prologue
    .line 256
    invoke-virtual {p0}, Lorg/apache/poi/hssf/usermodel/HSSFComment;->getOptRecord()Lorg/apache/poi/ddf/EscherOptRecord;

    move-result-object v1

    const/16 v2, 0x186

    invoke-virtual {v1, v2}, Lorg/apache/poi/ddf/EscherOptRecord;->lookup(I)Lorg/apache/poi/ddf/EscherProperty;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/ddf/EscherSimpleProperty;

    .line 257
    .local v0, "property":Lorg/apache/poi/ddf/EscherSimpleProperty;
    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_0
    invoke-virtual {v0}, Lorg/apache/poi/ddf/EscherSimpleProperty;->getPropertyValue()I

    move-result v1

    goto :goto_0
.end method

.method public getColumn()I
    .locals 1

    .prologue
    .line 172
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFComment;->_note:Lorg/apache/poi/hssf/record/NoteRecord;

    invoke-virtual {v0}, Lorg/apache/poi/hssf/record/NoteRecord;->getColumn()I

    move-result v0

    return v0
.end method

.method protected getNoteRecord()Lorg/apache/poi/hssf/record/NoteRecord;
    .locals 1

    .prologue
    .line 214
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFComment;->_note:Lorg/apache/poi/hssf/record/NoteRecord;

    return-object v0
.end method

.method public getRow()I
    .locals 1

    .prologue
    .line 154
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFComment;->_note:Lorg/apache/poi/hssf/record/NoteRecord;

    invoke-virtual {v0}, Lorg/apache/poi/hssf/record/NoteRecord;->getRow()I

    move-result v0

    return v0
.end method

.method public bridge synthetic getString()Lorg/apache/poi/ss/usermodel/RichTextString;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/poi/hssf/usermodel/HSSFComment;->getString()Lorg/apache/poi/hssf/usermodel/HSSFRichTextString;

    move-result-object v0

    return-object v0
.end method

.method public isVisible()Z
    .locals 2

    .prologue
    .line 145
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFComment;->_note:Lorg/apache/poi/hssf/record/NoteRecord;

    invoke-virtual {v0}, Lorg/apache/poi/hssf/record/NoteRecord;->getFlags()S

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public resetBackgroundImage()V
    .locals 6

    .prologue
    const/16 v5, 0x186

    const/4 v4, 0x0

    .line 246
    invoke-virtual {p0}, Lorg/apache/poi/hssf/usermodel/HSSFComment;->getOptRecord()Lorg/apache/poi/ddf/EscherOptRecord;

    move-result-object v2

    invoke-virtual {v2, v5}, Lorg/apache/poi/ddf/EscherOptRecord;->lookup(I)Lorg/apache/poi/ddf/EscherProperty;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/ddf/EscherSimpleProperty;

    .line 247
    .local v1, "property":Lorg/apache/poi/ddf/EscherSimpleProperty;
    if-eqz v1, :cond_0

    .line 248
    invoke-virtual {p0}, Lorg/apache/poi/hssf/usermodel/HSSFComment;->getPatriarch()Lorg/apache/poi/hssf/usermodel/HSSFPatriarch;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/poi/hssf/usermodel/HSSFPatriarch;->getSheet()Lorg/apache/poi/hssf/usermodel/HSSFSheet;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->getWorkbook()Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->getWorkbook()Lorg/apache/poi/hssf/model/InternalWorkbook;

    move-result-object v2

    invoke-virtual {v1}, Lorg/apache/poi/ddf/EscherSimpleProperty;->getPropertyValue()I

    move-result v3

    invoke-virtual {v2, v3}, Lorg/apache/poi/hssf/model/InternalWorkbook;->getBSERecord(I)Lorg/apache/poi/ddf/EscherBSERecord;

    move-result-object v0

    .line 249
    .local v0, "bse":Lorg/apache/poi/ddf/EscherBSERecord;
    invoke-virtual {v0}, Lorg/apache/poi/ddf/EscherBSERecord;->getRef()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v0, v2}, Lorg/apache/poi/ddf/EscherBSERecord;->setRef(I)V

    .line 250
    invoke-virtual {p0}, Lorg/apache/poi/hssf/usermodel/HSSFComment;->getOptRecord()Lorg/apache/poi/ddf/EscherOptRecord;

    move-result-object v2

    invoke-virtual {v2, v5}, Lorg/apache/poi/ddf/EscherOptRecord;->removeEscherProperty(I)V

    .line 252
    .end local v0    # "bse":Lorg/apache/poi/ddf/EscherBSERecord;
    :cond_0
    new-instance v2, Lorg/apache/poi/ddf/EscherSimpleProperty;

    const/16 v3, 0x180

    invoke-direct {v2, v3, v4, v4, v4}, Lorg/apache/poi/ddf/EscherSimpleProperty;-><init>(SZZI)V

    invoke-virtual {p0, v2}, Lorg/apache/poi/hssf/usermodel/HSSFComment;->setPropertyValue(Lorg/apache/poi/ddf/EscherProperty;)V

    .line 253
    return-void
.end method

.method public setAuthor(Ljava/lang/String;)V
    .locals 1
    .param p1, "author"    # Ljava/lang/String;

    .prologue
    .line 207
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFComment;->_note:Lorg/apache/poi/hssf/record/NoteRecord;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFComment;->_note:Lorg/apache/poi/hssf/record/NoteRecord;

    invoke-virtual {v0, p1}, Lorg/apache/poi/hssf/record/NoteRecord;->setAuthor(Ljava/lang/String;)V

    .line 208
    :cond_0
    return-void
.end method

.method public setBackgroundImage(I)V
    .locals 5
    .param p1, "pictureIndex"    # I

    .prologue
    const/4 v4, 0x0

    .line 239
    new-instance v1, Lorg/apache/poi/ddf/EscherSimpleProperty;

    const/16 v2, 0x186

    const/4 v3, 0x1

    invoke-direct {v1, v2, v4, v3, p1}, Lorg/apache/poi/ddf/EscherSimpleProperty;-><init>(SZZI)V

    invoke-virtual {p0, v1}, Lorg/apache/poi/hssf/usermodel/HSSFComment;->setPropertyValue(Lorg/apache/poi/ddf/EscherProperty;)V

    .line 240
    new-instance v1, Lorg/apache/poi/ddf/EscherSimpleProperty;

    const/16 v2, 0x180

    const/4 v3, 0x3

    invoke-direct {v1, v2, v4, v4, v3}, Lorg/apache/poi/ddf/EscherSimpleProperty;-><init>(SZZI)V

    invoke-virtual {p0, v1}, Lorg/apache/poi/hssf/usermodel/HSSFComment;->setPropertyValue(Lorg/apache/poi/ddf/EscherProperty;)V

    .line 241
    invoke-virtual {p0}, Lorg/apache/poi/hssf/usermodel/HSSFComment;->getPatriarch()Lorg/apache/poi/hssf/usermodel/HSSFPatriarch;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/poi/hssf/usermodel/HSSFPatriarch;->getSheet()Lorg/apache/poi/hssf/usermodel/HSSFSheet;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->getWorkbook()Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->getWorkbook()Lorg/apache/poi/hssf/model/InternalWorkbook;

    move-result-object v1

    invoke-virtual {v1, p1}, Lorg/apache/poi/hssf/model/InternalWorkbook;->getBSERecord(I)Lorg/apache/poi/ddf/EscherBSERecord;

    move-result-object v0

    .line 242
    .local v0, "bse":Lorg/apache/poi/ddf/EscherBSERecord;
    invoke-virtual {v0}, Lorg/apache/poi/ddf/EscherBSERecord;->getRef()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Lorg/apache/poi/ddf/EscherBSERecord;->setRef(I)V

    .line 243
    return-void
.end method

.method public setColumn(I)V
    .locals 1
    .param p1, "col"    # I

    .prologue
    .line 181
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFComment;->_note:Lorg/apache/poi/hssf/record/NoteRecord;

    invoke-virtual {v0, p1}, Lorg/apache/poi/hssf/record/NoteRecord;->setColumn(I)V

    .line 182
    return-void
.end method

.method public setColumn(S)V
    .locals 0
    .param p1, "col"    # S
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 189
    invoke-virtual {p0, p1}, Lorg/apache/poi/hssf/usermodel/HSSFComment;->setColumn(I)V

    .line 190
    return-void
.end method

.method public setRow(I)V
    .locals 1
    .param p1, "row"    # I

    .prologue
    .line 163
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFComment;->_note:Lorg/apache/poi/hssf/record/NoteRecord;

    invoke-virtual {v0, p1}, Lorg/apache/poi/hssf/record/NoteRecord;->setRow(I)V

    .line 164
    return-void
.end method

.method setShapeId(I)V
    .locals 3
    .param p1, "shapeId"    # I

    .prologue
    .line 123
    invoke-super {p0, p1}, Lorg/apache/poi/hssf/usermodel/HSSFTextbox;->setShapeId(I)V

    .line 124
    invoke-virtual {p0}, Lorg/apache/poi/hssf/usermodel/HSSFComment;->getObjRecord()Lorg/apache/poi/hssf/record/ObjRecord;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/poi/hssf/record/ObjRecord;->getSubRecords()Ljava/util/List;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hssf/record/CommonObjectDataSubRecord;

    .line 125
    .local v0, "cod":Lorg/apache/poi/hssf/record/CommonObjectDataSubRecord;
    rem-int/lit16 v1, p1, 0x400

    int-to-short v1, v1

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/record/CommonObjectDataSubRecord;->setObjectId(I)V

    .line 126
    iget-object v1, p0, Lorg/apache/poi/hssf/usermodel/HSSFComment;->_note:Lorg/apache/poi/hssf/record/NoteRecord;

    rem-int/lit16 v2, p1, 0x400

    invoke-virtual {v1, v2}, Lorg/apache/poi/hssf/record/NoteRecord;->setShapeId(I)V

    .line 127
    return-void
.end method

.method public setShapeType(I)V
    .locals 3
    .param p1, "shapeType"    # I

    .prologue
    .line 219
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Shape type can not be changed in "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setVisible(Z)V
    .locals 3
    .param p1, "visible"    # Z

    .prologue
    const/4 v1, 0x0

    .line 135
    iget-object v2, p0, Lorg/apache/poi/hssf/usermodel/HSSFComment;->_note:Lorg/apache/poi/hssf/record/NoteRecord;

    if-eqz p1, :cond_0

    const/4 v0, 0x2

    :goto_0
    invoke-virtual {v2, v0}, Lorg/apache/poi/hssf/record/NoteRecord;->setFlags(S)V

    .line 136
    if-eqz p1, :cond_1

    :goto_1
    invoke-direct {p0, v1}, Lorg/apache/poi/hssf/usermodel/HSSFComment;->setHidden(Z)V

    .line 137
    return-void

    :cond_0
    move v0, v1

    .line 135
    goto :goto_0

    .line 136
    :cond_1
    const/4 v1, 0x1

    goto :goto_1
.end method
