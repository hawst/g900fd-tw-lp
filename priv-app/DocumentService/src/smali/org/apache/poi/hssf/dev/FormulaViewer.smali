.class public Lorg/apache/poi/hssf/dev/FormulaViewer;
.super Ljava/lang/Object;
.source "FormulaViewer.java"


# instance fields
.field private file:Ljava/lang/String;

.field private list:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/poi/hssf/dev/FormulaViewer;->list:Z

    .line 50
    return-void
.end method

.method private listFormula(Lorg/apache/poi/hssf/record/FormulaRecord;)V
    .locals 7
    .param p1, "record"    # Lorg/apache/poi/hssf/record/FormulaRecord;

    .prologue
    .line 85
    const-string/jumbo v3, "~"

    .line 86
    .local v3, "sep":Ljava/lang/String;
    invoke-virtual {p1}, Lorg/apache/poi/hssf/record/FormulaRecord;->getParsedExpression()[Lorg/apache/poi/ss/formula/ptg/Ptg;

    move-result-object v5

    .line 88
    .local v5, "tokens":[Lorg/apache/poi/ss/formula/ptg/Ptg;
    array-length v2, v5

    .line 90
    .local v2, "numptgs":I
    add-int/lit8 v6, v2, -0x1

    aget-object v4, v5, v6

    .line 91
    .local v4, "token":Lorg/apache/poi/ss/formula/ptg/Ptg;
    instance-of v6, v4, Lorg/apache/poi/ss/formula/ptg/FuncPtg;

    if-eqz v6, :cond_0

    .line 92
    add-int/lit8 v6, v2, -0x1

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    .line 97
    .local v1, "numArg":Ljava/lang/String;
    :goto_0
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 99
    .local v0, "buf":Ljava/lang/StringBuffer;
    instance-of v6, v4, Lorg/apache/poi/ss/formula/ptg/ExpPtg;

    if-eqz v6, :cond_1

    .line 134
    :goto_1
    return-void

    .line 94
    .end local v0    # "buf":Ljava/lang/StringBuffer;
    .end local v1    # "numArg":Ljava/lang/String;
    :cond_0
    const/4 v6, -0x1

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    .restart local v1    # "numArg":Ljava/lang/String;
    goto :goto_0

    .restart local v0    # "buf":Ljava/lang/StringBuffer;
    :cond_1
    move-object v6, v4

    .line 100
    check-cast v6, Lorg/apache/poi/ss/formula/ptg/OperationPtg;

    invoke-virtual {v6}, Lorg/apache/poi/ss/formula/ptg/OperationPtg;->toFormulaString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 101
    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 102
    invoke-virtual {v4}, Lorg/apache/poi/ss/formula/ptg/Ptg;->getPtgClass()B

    move-result v6

    sparse-switch v6, :sswitch_data_0

    .line 114
    :goto_2
    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 115
    const/4 v6, 0x1

    if-le v2, v6, :cond_2

    .line 116
    add-int/lit8 v6, v2, -0x2

    aget-object v4, v5, v6

    .line 117
    invoke-virtual {v4}, Lorg/apache/poi/ss/formula/ptg/Ptg;->getPtgClass()B

    move-result v6

    sparse-switch v6, :sswitch_data_1

    .line 131
    :goto_3
    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 132
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_1

    .line 104
    :sswitch_0
    const-string/jumbo v6, "REF"

    invoke-virtual {v0, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_2

    .line 107
    :sswitch_1
    const-string/jumbo v6, "VALUE"

    invoke-virtual {v0, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_2

    .line 110
    :sswitch_2
    const-string/jumbo v6, "ARRAY"

    invoke-virtual {v0, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_2

    .line 119
    :sswitch_3
    const-string/jumbo v6, "REF"

    invoke-virtual {v0, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_3

    .line 122
    :sswitch_4
    const-string/jumbo v6, "VALUE"

    invoke-virtual {v0, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_3

    .line 125
    :sswitch_5
    const-string/jumbo v6, "ARRAY"

    invoke-virtual {v0, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_3

    .line 129
    :cond_2
    const-string/jumbo v6, "VALUE"

    invoke-virtual {v0, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_3

    .line 102
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x20 -> :sswitch_1
        0x40 -> :sswitch_2
    .end sparse-switch

    .line 117
    :sswitch_data_1
    .sparse-switch
        0x0 -> :sswitch_3
        0x20 -> :sswitch_4
        0x40 -> :sswitch_5
    .end sparse-switch
.end method

.method public static main([Ljava/lang/String;)V
    .locals 5
    .param p0, "args"    # [Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 215
    if-eqz p0, :cond_0

    array-length v2, p0

    const/4 v3, 0x2

    if-gt v2, v3, :cond_0

    .line 216
    aget-object v2, p0, v4

    const-string/jumbo v3, "--help"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 221
    aget-object v2, p0, v4

    const-string/jumbo v3, "--listFunctions"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 223
    :try_start_0
    new-instance v1, Lorg/apache/poi/hssf/dev/FormulaViewer;

    invoke-direct {v1}, Lorg/apache/poi/hssf/dev/FormulaViewer;-><init>()V

    .line 224
    .local v1, "viewer":Lorg/apache/poi/hssf/dev/FormulaViewer;
    const/4 v2, 0x1

    aget-object v2, p0, v2

    invoke-virtual {v1, v2}, Lorg/apache/poi/hssf/dev/FormulaViewer;->setFile(Ljava/lang/String;)V

    .line 225
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lorg/apache/poi/hssf/dev/FormulaViewer;->setList(Z)V

    .line 226
    invoke-virtual {v1}, Lorg/apache/poi/hssf/dev/FormulaViewer;->run()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 246
    .end local v1    # "viewer":Lorg/apache/poi/hssf/dev/FormulaViewer;
    :cond_0
    :goto_0
    return-void

    .line 228
    :catch_0
    move-exception v0

    .line 229
    .local v0, "e":Ljava/lang/Exception;
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string/jumbo v3, "Whoops!"

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0

    .line 236
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_1
    :try_start_1
    new-instance v1, Lorg/apache/poi/hssf/dev/FormulaViewer;

    invoke-direct {v1}, Lorg/apache/poi/hssf/dev/FormulaViewer;-><init>()V

    .line 238
    .restart local v1    # "viewer":Lorg/apache/poi/hssf/dev/FormulaViewer;
    const/4 v2, 0x0

    aget-object v2, p0, v2

    invoke-virtual {v1, v2}, Lorg/apache/poi/hssf/dev/FormulaViewer;->setFile(Ljava/lang/String;)V

    .line 239
    invoke-virtual {v1}, Lorg/apache/poi/hssf/dev/FormulaViewer;->run()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 241
    .end local v1    # "viewer":Lorg/apache/poi/hssf/dev/FormulaViewer;
    :catch_1
    move-exception v0

    .line 243
    .restart local v0    # "e":Ljava/lang/Exception;
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string/jumbo v3, "Whoops!"

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public parseFormulaRecord(Lorg/apache/poi/hssf/record/FormulaRecord;)V
    .locals 0
    .param p1, "record"    # Lorg/apache/poi/hssf/record/FormulaRecord;

    .prologue
    .line 156
    return-void
.end method

.method public run()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 64
    new-instance v0, Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;

    new-instance v4, Ljava/io/FileInputStream;

    iget-object v5, p0, Lorg/apache/poi/hssf/dev/FormulaViewer;->file:Ljava/lang/String;

    invoke-direct {v4, v5}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v4}, Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;-><init>(Ljava/io/InputStream;)V

    .line 67
    .local v0, "fs":Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;
    const-string/jumbo v4, "Workbook"

    invoke-virtual {v0, v4}, Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;->createDocumentInputStream(Ljava/lang/String;)Lorg/apache/poi/poifs/filesystem/DocumentInputStream;

    move-result-object v4

    invoke-static {v4}, Lorg/apache/poi/hssf/record/RecordFactory;->createRecords(Ljava/io/InputStream;)Ljava/util/List;

    move-result-object v3

    .line 69
    .local v3, "records":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/hssf/record/Record;>;"
    const/4 v1, 0x0

    .local v1, "k":I
    :goto_0
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    if-lt v1, v4, :cond_0

    .line 82
    return-void

    .line 71
    :cond_0
    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/poi/hssf/record/Record;

    .line 73
    .local v2, "record":Lorg/apache/poi/hssf/record/Record;
    invoke-virtual {v2}, Lorg/apache/poi/hssf/record/Record;->getSid()S

    move-result v4

    const/4 v5, 0x6

    if-ne v4, v5, :cond_1

    .line 75
    iget-boolean v4, p0, Lorg/apache/poi/hssf/dev/FormulaViewer;->list:Z

    if-eqz v4, :cond_2

    .line 76
    check-cast v2, Lorg/apache/poi/hssf/record/FormulaRecord;

    .end local v2    # "record":Lorg/apache/poi/hssf/record/Record;
    invoke-direct {p0, v2}, Lorg/apache/poi/hssf/dev/FormulaViewer;->listFormula(Lorg/apache/poi/hssf/record/FormulaRecord;)V

    .line 69
    :cond_1
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 78
    .restart local v2    # "record":Lorg/apache/poi/hssf/record/Record;
    :cond_2
    check-cast v2, Lorg/apache/poi/hssf/record/FormulaRecord;

    .end local v2    # "record":Lorg/apache/poi/hssf/record/Record;
    invoke-virtual {p0, v2}, Lorg/apache/poi/hssf/dev/FormulaViewer;->parseFormulaRecord(Lorg/apache/poi/hssf/record/FormulaRecord;)V

    goto :goto_1
.end method

.method public setFile(Ljava/lang/String;)V
    .locals 0
    .param p1, "file"    # Ljava/lang/String;

    .prologue
    .line 197
    iput-object p1, p0, Lorg/apache/poi/hssf/dev/FormulaViewer;->file:Ljava/lang/String;

    .line 198
    return-void
.end method

.method public setList(Z)V
    .locals 0
    .param p1, "list"    # Z

    .prologue
    .line 201
    iput-boolean p1, p0, Lorg/apache/poi/hssf/dev/FormulaViewer;->list:Z

    .line 202
    return-void
.end method
