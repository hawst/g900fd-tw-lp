.class public final Lorg/apache/poi/hssf/record/aggregates/RowRecordsAggregate;
.super Lorg/apache/poi/hssf/record/aggregates/RecordAggregate;
.source "RowRecordsAggregate.java"


# instance fields
.field private _firstrow:I

.field private _lastrow:I

.field private _rowRecordValues:[Lorg/apache/poi/hssf/record/RowRecord;

.field private final _rowRecords:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lorg/apache/poi/hssf/record/RowRecord;",
            ">;"
        }
    .end annotation
.end field

.field private final _sharedValueManager:Lorg/apache/poi/hssf/record/aggregates/SharedValueManager;

.field private final _unknownRecords:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/apache/poi/hssf/record/Record;",
            ">;"
        }
    .end annotation
.end field

.field private final _valuesAgg:Lorg/apache/poi/hssf/record/aggregates/ValueRecordsAggregate;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 68
    invoke-static {}, Lorg/apache/poi/hssf/record/aggregates/SharedValueManager;->createEmpty()Lorg/apache/poi/hssf/record/aggregates/SharedValueManager;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/apache/poi/hssf/record/aggregates/RowRecordsAggregate;-><init>(Lorg/apache/poi/hssf/record/aggregates/SharedValueManager;)V

    .line 69
    return-void
.end method

.method public constructor <init>(Lorg/apache/poi/hssf/model/RecordStream;Lorg/apache/poi/hssf/record/aggregates/SharedValueManager;Ljava/util/ArrayList;)V
    .locals 4
    .param p1, "rs"    # Lorg/apache/poi/hssf/model/RecordStream;
    .param p2, "svm"    # Lorg/apache/poi/hssf/record/aggregates/SharedValueManager;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/poi/hssf/model/RecordStream;",
            "Lorg/apache/poi/hssf/record/aggregates/SharedValueManager;",
            "Ljava/util/ArrayList",
            "<",
            "Lorg/apache/poi/ss/util/CellRangeAddressBase;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 171
    .local p3, "cellRangeAddressBases":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/poi/ss/util/CellRangeAddressBase;>;"
    invoke-direct {p0, p2}, Lorg/apache/poi/hssf/record/aggregates/RowRecordsAggregate;-><init>(Lorg/apache/poi/hssf/record/aggregates/SharedValueManager;)V

    .line 172
    :cond_0
    :goto_0
    :sswitch_0
    invoke-virtual {p1}, Lorg/apache/poi/hssf/model/RecordStream;->hasNext()Z

    move-result v2

    if-nez v2, :cond_2

    .line 219
    :cond_1
    return-void

    .line 173
    :cond_2
    invoke-virtual {p1}, Lorg/apache/poi/hssf/model/RecordStream;->getNext()Lorg/apache/poi/hssf/record/Record;

    move-result-object v1

    .line 174
    .local v1, "rec":Lorg/apache/poi/hssf/record/Record;
    invoke-virtual {v1}, Lorg/apache/poi/hssf/record/Record;->getSid()S

    move-result v2

    sparse-switch v2, :sswitch_data_0

    .line 189
    instance-of v2, v1, Lorg/apache/poi/hssf/record/UnknownRecord;

    if-eqz v2, :cond_3

    .line 191
    invoke-direct {p0, v1}, Lorg/apache/poi/hssf/record/aggregates/RowRecordsAggregate;->addUnknownRecord(Lorg/apache/poi/hssf/record/Record;)V

    .line 192
    :goto_1
    invoke-virtual {p1}, Lorg/apache/poi/hssf/model/RecordStream;->peekNextSid()I

    move-result v2

    const/16 v3, 0x3c

    if-ne v2, v3, :cond_0

    .line 193
    invoke-virtual {p1}, Lorg/apache/poi/hssf/model/RecordStream;->getNext()Lorg/apache/poi/hssf/record/Record;

    move-result-object v2

    invoke-direct {p0, v2}, Lorg/apache/poi/hssf/record/aggregates/RowRecordsAggregate;->addUnknownRecord(Lorg/apache/poi/hssf/record/Record;)V

    goto :goto_1

    :sswitch_1
    move-object v2, v1

    .line 177
    check-cast v2, Lorg/apache/poi/hssf/record/RowRecord;

    invoke-virtual {v2}, Lorg/apache/poi/hssf/record/RowRecord;->getRowNumber()I

    move-result v2

    const/4 v3, 0x0

    .line 176
    invoke-direct {p0, p3, v2, v3}, Lorg/apache/poi/hssf/record/aggregates/RowRecordsAggregate;->checkRowtoAdd(Ljava/util/ArrayList;II)Z

    move-result v2

    .line 177
    if-eqz v2, :cond_0

    .line 178
    check-cast v1, Lorg/apache/poi/hssf/record/RowRecord;

    .end local v1    # "rec":Lorg/apache/poi/hssf/record/Record;
    invoke-virtual {p0, v1}, Lorg/apache/poi/hssf/record/aggregates/RowRecordsAggregate;->insertRow(Lorg/apache/poi/hssf/record/RowRecord;)V

    goto :goto_0

    .line 181
    .restart local v1    # "rec":Lorg/apache/poi/hssf/record/Record;
    :sswitch_2
    invoke-direct {p0, v1}, Lorg/apache/poi/hssf/record/aggregates/RowRecordsAggregate;->addUnknownRecord(Lorg/apache/poi/hssf/record/Record;)V

    goto :goto_0

    .line 197
    :cond_3
    instance-of v2, v1, Lorg/apache/poi/hssf/record/MulBlankRecord;

    if-eqz v2, :cond_4

    .line 198
    iget-object v2, p0, Lorg/apache/poi/hssf/record/aggregates/RowRecordsAggregate;->_valuesAgg:Lorg/apache/poi/hssf/record/aggregates/ValueRecordsAggregate;

    check-cast v1, Lorg/apache/poi/hssf/record/MulBlankRecord;

    .end local v1    # "rec":Lorg/apache/poi/hssf/record/Record;
    invoke-virtual {v2, v1}, Lorg/apache/poi/hssf/record/aggregates/ValueRecordsAggregate;->addMultipleBlanks(Lorg/apache/poi/hssf/record/MulBlankRecord;)V

    goto :goto_0

    .line 201
    .restart local v1    # "rec":Lorg/apache/poi/hssf/record/Record;
    :cond_4
    instance-of v2, v1, Lorg/apache/poi/hssf/record/CellValueRecordInterface;

    if-eqz v2, :cond_0

    move-object v0, v1

    .line 207
    check-cast v0, Lorg/apache/poi/hssf/record/CellValueRecordInterface;

    .line 208
    .local v0, "cellInfo":Lorg/apache/poi/hssf/record/CellValueRecordInterface;
    invoke-interface {v0}, Lorg/apache/poi/hssf/record/CellValueRecordInterface;->getRow()I

    move-result v2

    const/16 v3, 0x14

    if-gt v2, v3, :cond_5

    .line 209
    invoke-interface {v0}, Lorg/apache/poi/hssf/record/CellValueRecordInterface;->getColumn()S

    move-result v2

    const/16 v3, 0x10

    if-gt v2, v3, :cond_5

    .line 210
    iget-object v2, p0, Lorg/apache/poi/hssf/record/aggregates/RowRecordsAggregate;->_valuesAgg:Lorg/apache/poi/hssf/record/aggregates/ValueRecordsAggregate;

    check-cast v1, Lorg/apache/poi/hssf/record/CellValueRecordInterface;

    .end local v1    # "rec":Lorg/apache/poi/hssf/record/Record;
    invoke-virtual {v2, v1, p1, p2}, Lorg/apache/poi/hssf/record/aggregates/ValueRecordsAggregate;->construct(Lorg/apache/poi/hssf/record/CellValueRecordInterface;Lorg/apache/poi/hssf/model/RecordStream;Lorg/apache/poi/hssf/record/aggregates/SharedValueManager;)V

    goto :goto_0

    .line 211
    .restart local v1    # "rec":Lorg/apache/poi/hssf/record/Record;
    :cond_5
    if-eqz p3, :cond_1

    .line 212
    invoke-virtual {p3}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-eqz v2, :cond_1

    .line 214
    invoke-interface {v0}, Lorg/apache/poi/hssf/record/CellValueRecordInterface;->getRow()I

    move-result v2

    .line 215
    invoke-interface {v0}, Lorg/apache/poi/hssf/record/CellValueRecordInterface;->getColumn()S

    move-result v3

    .line 214
    invoke-direct {p0, p3, v2, v3}, Lorg/apache/poi/hssf/record/aggregates/RowRecordsAggregate;->checkRowtoAdd(Ljava/util/ArrayList;II)Z

    move-result v2

    .line 215
    if-eqz v2, :cond_0

    .line 216
    iget-object v2, p0, Lorg/apache/poi/hssf/record/aggregates/RowRecordsAggregate;->_valuesAgg:Lorg/apache/poi/hssf/record/aggregates/ValueRecordsAggregate;

    check-cast v1, Lorg/apache/poi/hssf/record/CellValueRecordInterface;

    .end local v1    # "rec":Lorg/apache/poi/hssf/record/Record;
    invoke-virtual {v2, v1, p1, p2}, Lorg/apache/poi/hssf/record/aggregates/ValueRecordsAggregate;->construct(Lorg/apache/poi/hssf/record/CellValueRecordInterface;Lorg/apache/poi/hssf/model/RecordStream;Lorg/apache/poi/hssf/record/aggregates/SharedValueManager;)V

    goto/16 :goto_0

    .line 174
    :sswitch_data_0
    .sparse-switch
        0x51 -> :sswitch_2
        0xd7 -> :sswitch_0
        0x208 -> :sswitch_1
    .end sparse-switch
.end method

.method private constructor <init>(Lorg/apache/poi/hssf/record/aggregates/SharedValueManager;)V
    .locals 2
    .param p1, "svm"    # Lorg/apache/poi/hssf/record/aggregates/SharedValueManager;

    .prologue
    const/4 v0, -0x1

    .line 70
    invoke-direct {p0}, Lorg/apache/poi/hssf/record/aggregates/RecordAggregate;-><init>()V

    .line 55
    iput v0, p0, Lorg/apache/poi/hssf/record/aggregates/RowRecordsAggregate;->_firstrow:I

    .line 56
    iput v0, p0, Lorg/apache/poi/hssf/record/aggregates/RowRecordsAggregate;->_lastrow:I

    .line 64
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/poi/hssf/record/aggregates/RowRecordsAggregate;->_rowRecordValues:[Lorg/apache/poi/hssf/record/RowRecord;

    .line 71
    if-nez p1, :cond_0

    .line 72
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "SharedValueManager must be provided."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 74
    :cond_0
    new-instance v0, Ljava/util/TreeMap;

    invoke-direct {v0}, Ljava/util/TreeMap;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/hssf/record/aggregates/RowRecordsAggregate;->_rowRecords:Ljava/util/Map;

    .line 75
    new-instance v0, Lorg/apache/poi/hssf/record/aggregates/ValueRecordsAggregate;

    invoke-direct {v0}, Lorg/apache/poi/hssf/record/aggregates/ValueRecordsAggregate;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/hssf/record/aggregates/RowRecordsAggregate;->_valuesAgg:Lorg/apache/poi/hssf/record/aggregates/ValueRecordsAggregate;

    .line 76
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/hssf/record/aggregates/RowRecordsAggregate;->_unknownRecords:Ljava/util/List;

    .line 77
    iput-object p1, p0, Lorg/apache/poi/hssf/record/aggregates/RowRecordsAggregate;->_sharedValueManager:Lorg/apache/poi/hssf/record/aggregates/SharedValueManager;

    .line 78
    return-void
.end method

.method private addUnknownRecord(Lorg/apache/poi/hssf/record/Record;)V
    .locals 1
    .param p1, "rec"    # Lorg/apache/poi/hssf/record/Record;

    .prologue
    .line 232
    iget-object v0, p0, Lorg/apache/poi/hssf/record/aggregates/RowRecordsAggregate;->_unknownRecords:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 233
    return-void
.end method

.method private checkRowtoAdd(Ljava/util/ArrayList;II)Z
    .locals 9
    .param p2, "rowNumber"    # I
    .param p3, "colNumber"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lorg/apache/poi/ss/util/CellRangeAddressBase;",
            ">;II)Z"
        }
    .end annotation

    .prologue
    .local p1, "cellRangeAddressBases":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/poi/ss/util/CellRangeAddressBase;>;"
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 138
    const/16 v8, 0x14

    if-gt p2, v8, :cond_0

    .line 166
    :goto_0
    return v6

    .line 141
    :cond_0
    if-eqz p1, :cond_1

    .line 142
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-nez v8, :cond_2

    :cond_1
    move v6, v7

    .line 143
    goto :goto_0

    .line 146
    :cond_2
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 147
    .local v3, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/poi/ss/util/CellRangeAddressBase;>;"
    :cond_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-nez v8, :cond_4

    move v6, v7

    .line 166
    goto :goto_0

    .line 148
    :cond_4
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/ss/util/CellRangeAddress;

    .line 149
    .local v0, "cellRange":Lorg/apache/poi/ss/util/CellRangeAddress;
    invoke-virtual {v0}, Lorg/apache/poi/ss/util/CellRangeAddress;->getFirstRow()I

    move-result v2

    .line 150
    .local v2, "firstRow":I
    invoke-virtual {v0}, Lorg/apache/poi/ss/util/CellRangeAddress;->getLastRow()I

    move-result v5

    .line 151
    .local v5, "lastRow":I
    if-nez p3, :cond_5

    .line 152
    if-lt p2, v2, :cond_3

    if-gt p2, v5, :cond_3

    goto :goto_0

    .line 156
    :cond_5
    invoke-virtual {v0}, Lorg/apache/poi/ss/util/CellRangeAddress;->getFirstColumn()I

    move-result v1

    .line 157
    .local v1, "firstColumn":I
    invoke-virtual {v0}, Lorg/apache/poi/ss/util/CellRangeAddress;->getLastColumn()I

    move-result v4

    .line 158
    .local v4, "lastColumn":I
    if-lt p2, v2, :cond_3

    if-gt p2, v5, :cond_3

    .line 159
    if-lt p3, v1, :cond_3

    .line 160
    if-gt p3, v4, :cond_3

    goto :goto_0
.end method

.method public static createRow(I)Lorg/apache/poi/hssf/record/RowRecord;
    .locals 1
    .param p0, "rowNumber"    # I

    .prologue
    .line 479
    new-instance v0, Lorg/apache/poi/hssf/record/RowRecord;

    invoke-direct {v0, p0}, Lorg/apache/poi/hssf/record/RowRecord;-><init>(I)V

    return-object v0
.end method

.method private getEndRowNumberForBlock(I)I
    .locals 5
    .param p1, "block"    # I

    .prologue
    .line 329
    add-int/lit8 v2, p1, 0x1

    mul-int/lit8 v2, v2, 0x20

    add-int/lit8 v1, v2, -0x1

    .line 330
    .local v1, "endIndex":I
    iget-object v2, p0, Lorg/apache/poi/hssf/record/aggregates/RowRecordsAggregate;->_rowRecords:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->size()I

    move-result v2

    if-lt v1, v2, :cond_0

    .line 331
    iget-object v2, p0, Lorg/apache/poi/hssf/record/aggregates/RowRecordsAggregate;->_rowRecords:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->size()I

    move-result v2

    add-int/lit8 v1, v2, -0x1

    .line 333
    :cond_0
    iget-object v2, p0, Lorg/apache/poi/hssf/record/aggregates/RowRecordsAggregate;->_rowRecordValues:[Lorg/apache/poi/hssf/record/RowRecord;

    if-nez v2, :cond_1

    .line 334
    iget-object v2, p0, Lorg/apache/poi/hssf/record/aggregates/RowRecordsAggregate;->_rowRecords:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    iget-object v3, p0, Lorg/apache/poi/hssf/record/aggregates/RowRecordsAggregate;->_rowRecords:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->size()I

    move-result v3

    new-array v3, v3, [Lorg/apache/poi/hssf/record/RowRecord;

    invoke-interface {v2, v3}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Lorg/apache/poi/hssf/record/RowRecord;

    iput-object v2, p0, Lorg/apache/poi/hssf/record/aggregates/RowRecordsAggregate;->_rowRecordValues:[Lorg/apache/poi/hssf/record/RowRecord;

    .line 338
    :cond_1
    :try_start_0
    iget-object v2, p0, Lorg/apache/poi/hssf/record/aggregates/RowRecordsAggregate;->_rowRecordValues:[Lorg/apache/poi/hssf/record/RowRecord;

    aget-object v2, v2, v1

    invoke-virtual {v2}, Lorg/apache/poi/hssf/record/RowRecord;->getRowNumber()I
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    return v2

    .line 339
    :catch_0
    move-exception v0

    .line 340
    .local v0, "e":Ljava/lang/ArrayIndexOutOfBoundsException;
    new-instance v2, Ljava/lang/RuntimeException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "Did not find end row for block "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method private getRowBlockSize(I)I
    .locals 1
    .param p1, "block"    # I

    .prologue
    .line 299
    invoke-virtual {p0, p1}, Lorg/apache/poi/hssf/record/aggregates/RowRecordsAggregate;->getRowCountForBlock(I)I

    move-result v0

    mul-int/lit8 v0, v0, 0x14

    return v0
.end method

.method private getStartRowNumberForBlock(I)I
    .locals 5
    .param p1, "block"    # I

    .prologue
    .line 314
    mul-int/lit8 v1, p1, 0x20

    .line 316
    .local v1, "startIndex":I
    iget-object v2, p0, Lorg/apache/poi/hssf/record/aggregates/RowRecordsAggregate;->_rowRecordValues:[Lorg/apache/poi/hssf/record/RowRecord;

    if-nez v2, :cond_0

    .line 317
    iget-object v2, p0, Lorg/apache/poi/hssf/record/aggregates/RowRecordsAggregate;->_rowRecords:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    iget-object v3, p0, Lorg/apache/poi/hssf/record/aggregates/RowRecordsAggregate;->_rowRecords:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->size()I

    move-result v3

    new-array v3, v3, [Lorg/apache/poi/hssf/record/RowRecord;

    invoke-interface {v2, v3}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Lorg/apache/poi/hssf/record/RowRecord;

    iput-object v2, p0, Lorg/apache/poi/hssf/record/aggregates/RowRecordsAggregate;->_rowRecordValues:[Lorg/apache/poi/hssf/record/RowRecord;

    .line 321
    :cond_0
    :try_start_0
    iget-object v2, p0, Lorg/apache/poi/hssf/record/aggregates/RowRecordsAggregate;->_rowRecordValues:[Lorg/apache/poi/hssf/record/RowRecord;

    aget-object v2, v2, v1

    invoke-virtual {v2}, Lorg/apache/poi/hssf/record/RowRecord;->getRowNumber()I
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    return v2

    .line 322
    :catch_0
    move-exception v0

    .line 323
    .local v0, "e":Ljava/lang/ArrayIndexOutOfBoundsException;
    new-instance v2, Ljava/lang/RuntimeException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "Did not find start row for block "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method private visitRowRecordsForBlock(ILorg/apache/poi/hssf/record/aggregates/RecordAggregate$RecordVisitor;)I
    .locals 8
    .param p1, "blockIndex"    # I
    .param p2, "rv"    # Lorg/apache/poi/hssf/record/aggregates/RecordAggregate$RecordVisitor;

    .prologue
    .line 345
    mul-int/lit8 v6, p1, 0x20

    .line 346
    .local v6, "startIndex":I
    add-int/lit8 v0, v6, 0x20

    .line 348
    .local v0, "endIndex":I
    iget-object v7, p0, Lorg/apache/poi/hssf/record/aggregates/RowRecordsAggregate;->_rowRecords:Ljava/util/Map;

    invoke-interface {v7}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .line 354
    .local v5, "rowIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/poi/hssf/record/RowRecord;>;"
    const/4 v1, 0x0

    .line 355
    .local v1, "i":I
    :goto_0
    if-lt v1, v6, :cond_1

    .line 357
    const/4 v4, 0x0

    .line 358
    .local v4, "result":I
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_0

    add-int/lit8 v2, v1, 0x1

    .end local v1    # "i":I
    .local v2, "i":I
    if-lt v1, v0, :cond_2

    move v1, v2

    .line 363
    .end local v2    # "i":I
    .restart local v1    # "i":I
    :cond_0
    return v4

    .line 356
    .end local v4    # "result":I
    :cond_1
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 355
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 359
    .end local v1    # "i":I
    .restart local v2    # "i":I
    .restart local v4    # "result":I
    :cond_2
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/poi/hssf/record/Record;

    .line 360
    .local v3, "rec":Lorg/apache/poi/hssf/record/Record;
    invoke-virtual {v3}, Lorg/apache/poi/hssf/record/Record;->getRecordSize()I

    move-result v7

    add-int/2addr v4, v7

    .line 361
    invoke-interface {p2, v3}, Lorg/apache/poi/hssf/record/aggregates/RecordAggregate$RecordVisitor;->visitRecord(Lorg/apache/poi/hssf/record/Record;)V

    move v1, v2

    .end local v2    # "i":I
    .restart local v1    # "i":I
    goto :goto_1
.end method

.method private writeHidden(Lorg/apache/poi/hssf/record/RowRecord;I)I
    .locals 4
    .param p1, "pRowRecord"    # Lorg/apache/poi/hssf/record/RowRecord;
    .param p2, "row"    # I

    .prologue
    .line 442
    move v1, p2

    .line 443
    .local v1, "rowIx":I
    move-object v2, p1

    .line 444
    .local v2, "rowRecord":Lorg/apache/poi/hssf/record/RowRecord;
    invoke-virtual {v2}, Lorg/apache/poi/hssf/record/RowRecord;->getOutlineLevel()S

    move-result v0

    .line 445
    .local v0, "level":I
    :goto_0
    if-eqz v2, :cond_0

    invoke-virtual {p0, v1}, Lorg/apache/poi/hssf/record/aggregates/RowRecordsAggregate;->getRow(I)Lorg/apache/poi/hssf/record/RowRecord;

    move-result-object v3

    invoke-virtual {v3}, Lorg/apache/poi/hssf/record/RowRecord;->getOutlineLevel()S

    move-result v3

    if-ge v3, v0, :cond_1

    .line 450
    :cond_0
    return v1

    .line 446
    :cond_1
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lorg/apache/poi/hssf/record/RowRecord;->setZeroHeight(Z)V

    .line 447
    add-int/lit8 v1, v1, 0x1

    .line 448
    invoke-virtual {p0, v1}, Lorg/apache/poi/hssf/record/aggregates/RowRecordsAggregate;->getRow(I)Lorg/apache/poi/hssf/record/RowRecord;

    move-result-object v2

    goto :goto_0
.end method


# virtual methods
.method public collapseRow(I)V
    .locals 5
    .param p1, "rowNumber"    # I

    .prologue
    .line 456
    invoke-virtual {p0, p1}, Lorg/apache/poi/hssf/record/aggregates/RowRecordsAggregate;->findStartOfRowOutlineGroup(I)I

    move-result v3

    .line 457
    .local v3, "startRow":I
    invoke-virtual {p0, v3}, Lorg/apache/poi/hssf/record/aggregates/RowRecordsAggregate;->getRow(I)Lorg/apache/poi/hssf/record/RowRecord;

    move-result-object v2

    .line 460
    .local v2, "rowRecord":Lorg/apache/poi/hssf/record/RowRecord;
    invoke-direct {p0, v2, v3}, Lorg/apache/poi/hssf/record/aggregates/RowRecordsAggregate;->writeHidden(Lorg/apache/poi/hssf/record/RowRecord;I)I

    move-result v0

    .line 462
    .local v0, "nextRowIx":I
    invoke-virtual {p0, v0}, Lorg/apache/poi/hssf/record/aggregates/RowRecordsAggregate;->getRow(I)Lorg/apache/poi/hssf/record/RowRecord;

    move-result-object v1

    .line 463
    .local v1, "row":Lorg/apache/poi/hssf/record/RowRecord;
    if-nez v1, :cond_0

    .line 464
    invoke-static {v0}, Lorg/apache/poi/hssf/record/aggregates/RowRecordsAggregate;->createRow(I)Lorg/apache/poi/hssf/record/RowRecord;

    move-result-object v1

    .line 465
    invoke-virtual {p0, v1}, Lorg/apache/poi/hssf/record/aggregates/RowRecordsAggregate;->insertRow(Lorg/apache/poi/hssf/record/RowRecord;)V

    .line 468
    :cond_0
    const/4 v4, 0x1

    invoke-virtual {v1, v4}, Lorg/apache/poi/hssf/record/RowRecord;->setColapsed(Z)V

    .line 469
    return-void
.end method

.method public createDimensions()Lorg/apache/poi/hssf/record/DimensionsRecord;
    .locals 2

    .prologue
    .line 627
    new-instance v0, Lorg/apache/poi/hssf/record/DimensionsRecord;

    invoke-direct {v0}, Lorg/apache/poi/hssf/record/DimensionsRecord;-><init>()V

    .line 628
    .local v0, "result":Lorg/apache/poi/hssf/record/DimensionsRecord;
    iget v1, p0, Lorg/apache/poi/hssf/record/aggregates/RowRecordsAggregate;->_firstrow:I

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/record/DimensionsRecord;->setFirstRow(I)V

    .line 629
    iget v1, p0, Lorg/apache/poi/hssf/record/aggregates/RowRecordsAggregate;->_lastrow:I

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/record/DimensionsRecord;->setLastRow(I)V

    .line 630
    iget-object v1, p0, Lorg/apache/poi/hssf/record/aggregates/RowRecordsAggregate;->_valuesAgg:Lorg/apache/poi/hssf/record/aggregates/ValueRecordsAggregate;

    invoke-virtual {v1}, Lorg/apache/poi/hssf/record/aggregates/ValueRecordsAggregate;->getFirstCellNum()I

    move-result v1

    int-to-short v1, v1

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/record/DimensionsRecord;->setFirstCol(S)V

    .line 631
    iget-object v1, p0, Lorg/apache/poi/hssf/record/aggregates/RowRecordsAggregate;->_valuesAgg:Lorg/apache/poi/hssf/record/aggregates/ValueRecordsAggregate;

    invoke-virtual {v1}, Lorg/apache/poi/hssf/record/aggregates/ValueRecordsAggregate;->getLastCellNum()I

    move-result v1

    int-to-short v1, v1

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/record/DimensionsRecord;->setLastCol(S)V

    .line 632
    return-object v0
.end method

.method public createFormula(II)Lorg/apache/poi/hssf/record/aggregates/FormulaRecordAggregate;
    .locals 4
    .param p1, "row"    # I
    .param p2, "col"    # I

    .prologue
    .line 618
    new-instance v0, Lorg/apache/poi/hssf/record/FormulaRecord;

    invoke-direct {v0}, Lorg/apache/poi/hssf/record/FormulaRecord;-><init>()V

    .line 619
    .local v0, "fr":Lorg/apache/poi/hssf/record/FormulaRecord;
    invoke-virtual {v0, p1}, Lorg/apache/poi/hssf/record/FormulaRecord;->setRow(I)V

    .line 620
    int-to-short v1, p2

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/record/FormulaRecord;->setColumn(S)V

    .line 621
    new-instance v1, Lorg/apache/poi/hssf/record/aggregates/FormulaRecordAggregate;

    const/4 v2, 0x0

    iget-object v3, p0, Lorg/apache/poi/hssf/record/aggregates/RowRecordsAggregate;->_sharedValueManager:Lorg/apache/poi/hssf/record/aggregates/SharedValueManager;

    invoke-direct {v1, v0, v2, v3}, Lorg/apache/poi/hssf/record/aggregates/FormulaRecordAggregate;-><init>(Lorg/apache/poi/hssf/record/FormulaRecord;Lorg/apache/poi/hssf/record/StringRecord;Lorg/apache/poi/hssf/record/aggregates/SharedValueManager;)V

    return-object v1
.end method

.method public createIndexRecord(II)Lorg/apache/poi/hssf/record/IndexRecord;
    .locals 8
    .param p1, "indexRecordOffset"    # I
    .param p2, "sizeOfInitialSheetRecords"    # I

    .prologue
    .line 575
    new-instance v4, Lorg/apache/poi/hssf/record/IndexRecord;

    invoke-direct {v4}, Lorg/apache/poi/hssf/record/IndexRecord;-><init>()V

    .line 576
    .local v4, "result":Lorg/apache/poi/hssf/record/IndexRecord;
    iget v5, p0, Lorg/apache/poi/hssf/record/aggregates/RowRecordsAggregate;->_firstrow:I

    invoke-virtual {v4, v5}, Lorg/apache/poi/hssf/record/IndexRecord;->setFirstRow(I)V

    .line 577
    iget v5, p0, Lorg/apache/poi/hssf/record/aggregates/RowRecordsAggregate;->_lastrow:I

    add-int/lit8 v5, v5, 0x1

    invoke-virtual {v4, v5}, Lorg/apache/poi/hssf/record/IndexRecord;->setLastRowAdd1(I)V

    .line 585
    invoke-virtual {p0}, Lorg/apache/poi/hssf/record/aggregates/RowRecordsAggregate;->getRowBlockCount()I

    move-result v1

    .line 587
    .local v1, "blockCount":I
    invoke-static {v1}, Lorg/apache/poi/hssf/record/IndexRecord;->getRecordSizeForBlockCount(I)I

    move-result v3

    .line 589
    .local v3, "indexRecSize":I
    add-int v5, p1, v3

    add-int v2, v5, p2

    .line 591
    .local v2, "currentOffset":I
    const/4 v0, 0x0

    .local v0, "block":I
    :goto_0
    if-lt v0, v1, :cond_0

    .line 606
    return-object v4

    .line 596
    :cond_0
    invoke-direct {p0, v0}, Lorg/apache/poi/hssf/record/aggregates/RowRecordsAggregate;->getRowBlockSize(I)I

    move-result v5

    add-int/2addr v2, v5

    .line 598
    iget-object v5, p0, Lorg/apache/poi/hssf/record/aggregates/RowRecordsAggregate;->_valuesAgg:Lorg/apache/poi/hssf/record/aggregates/ValueRecordsAggregate;

    .line 599
    invoke-direct {p0, v0}, Lorg/apache/poi/hssf/record/aggregates/RowRecordsAggregate;->getStartRowNumberForBlock(I)I

    move-result v6

    invoke-direct {p0, v0}, Lorg/apache/poi/hssf/record/aggregates/RowRecordsAggregate;->getEndRowNumberForBlock(I)I

    move-result v7

    invoke-virtual {v5, v6, v7}, Lorg/apache/poi/hssf/record/aggregates/ValueRecordsAggregate;->getRowCellBlockSize(II)I

    move-result v5

    add-int/2addr v2, v5

    .line 602
    invoke-virtual {v4, v2}, Lorg/apache/poi/hssf/record/IndexRecord;->addDbcell(I)V

    .line 604
    invoke-virtual {p0, v0}, Lorg/apache/poi/hssf/record/aggregates/RowRecordsAggregate;->getRowCountForBlock(I)I

    move-result v5

    mul-int/lit8 v5, v5, 0x2

    add-int/lit8 v5, v5, 0x8

    add-int/2addr v2, v5

    .line 591
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public expandRow(I)V
    .locals 9
    .param p1, "rowNumber"    # I

    .prologue
    const/4 v8, 0x0

    .line 492
    move v2, p1

    .line 493
    .local v2, "idx":I
    const/4 v6, -0x1

    if-ne v2, v6, :cond_1

    .line 526
    :cond_0
    :goto_0
    return-void

    .line 497
    :cond_1
    invoke-virtual {p0, v2}, Lorg/apache/poi/hssf/record/aggregates/RowRecordsAggregate;->isRowGroupCollapsed(I)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 502
    invoke-virtual {p0, v2}, Lorg/apache/poi/hssf/record/aggregates/RowRecordsAggregate;->findStartOfRowOutlineGroup(I)I

    move-result v5

    .line 503
    .local v5, "startIdx":I
    invoke-virtual {p0, v5}, Lorg/apache/poi/hssf/record/aggregates/RowRecordsAggregate;->getRow(I)Lorg/apache/poi/hssf/record/RowRecord;

    move-result-object v4

    .line 506
    .local v4, "row":Lorg/apache/poi/hssf/record/RowRecord;
    invoke-virtual {p0, v2}, Lorg/apache/poi/hssf/record/aggregates/RowRecordsAggregate;->findEndOfRowOutlineGroup(I)I

    move-result v0

    .line 515
    .local v0, "endIdx":I
    invoke-virtual {p0, v2}, Lorg/apache/poi/hssf/record/aggregates/RowRecordsAggregate;->isRowGroupHiddenByParent(I)Z

    move-result v6

    if-nez v6, :cond_2

    .line 516
    move v1, v5

    .local v1, "i":I
    :goto_1
    if-le v1, v0, :cond_3

    .line 525
    .end local v1    # "i":I
    :cond_2
    add-int/lit8 v6, v0, 0x1

    invoke-virtual {p0, v6}, Lorg/apache/poi/hssf/record/aggregates/RowRecordsAggregate;->getRow(I)Lorg/apache/poi/hssf/record/RowRecord;

    move-result-object v6

    invoke-virtual {v6, v8}, Lorg/apache/poi/hssf/record/RowRecord;->setColapsed(Z)V

    goto :goto_0

    .line 517
    .restart local v1    # "i":I
    :cond_3
    invoke-virtual {p0, v1}, Lorg/apache/poi/hssf/record/aggregates/RowRecordsAggregate;->getRow(I)Lorg/apache/poi/hssf/record/RowRecord;

    move-result-object v3

    .line 518
    .local v3, "otherRow":Lorg/apache/poi/hssf/record/RowRecord;
    invoke-virtual {v4}, Lorg/apache/poi/hssf/record/RowRecord;->getOutlineLevel()S

    move-result v6

    invoke-virtual {v3}, Lorg/apache/poi/hssf/record/RowRecord;->getOutlineLevel()S

    move-result v7

    if-eq v6, v7, :cond_4

    invoke-virtual {p0, v1}, Lorg/apache/poi/hssf/record/aggregates/RowRecordsAggregate;->isRowGroupCollapsed(I)Z

    move-result v6

    if-nez v6, :cond_5

    .line 519
    :cond_4
    invoke-virtual {v3, v8}, Lorg/apache/poi/hssf/record/RowRecord;->setZeroHeight(Z)V

    .line 516
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method public findEndOfRowOutlineGroup(I)I
    .locals 3
    .param p1, "row"    # I

    .prologue
    .line 426
    invoke-virtual {p0, p1}, Lorg/apache/poi/hssf/record/aggregates/RowRecordsAggregate;->getRow(I)Lorg/apache/poi/hssf/record/RowRecord;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/poi/hssf/record/RowRecord;->getOutlineLevel()S

    move-result v1

    .line 428
    .local v1, "level":I
    move v0, p1

    .local v0, "currentRow":I
    :goto_0
    invoke-virtual {p0}, Lorg/apache/poi/hssf/record/aggregates/RowRecordsAggregate;->getLastRowNum()I

    move-result v2

    if-lt v0, v2, :cond_1

    .line 434
    :cond_0
    add-int/lit8 v2, v0, -0x1

    return v2

    .line 429
    :cond_1
    invoke-virtual {p0, v0}, Lorg/apache/poi/hssf/record/aggregates/RowRecordsAggregate;->getRow(I)Lorg/apache/poi/hssf/record/RowRecord;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {p0, v0}, Lorg/apache/poi/hssf/record/aggregates/RowRecordsAggregate;->getRow(I)Lorg/apache/poi/hssf/record/RowRecord;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/poi/hssf/record/RowRecord;->getOutlineLevel()S

    move-result v2

    if-lt v2, v1, :cond_0

    .line 428
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public findStartOfRowOutlineGroup(I)I
    .locals 4
    .param p1, "row"    # I

    .prologue
    .line 411
    invoke-virtual {p0, p1}, Lorg/apache/poi/hssf/record/aggregates/RowRecordsAggregate;->getRow(I)Lorg/apache/poi/hssf/record/RowRecord;

    move-result-object v2

    .line 412
    .local v2, "rowRecord":Lorg/apache/poi/hssf/record/RowRecord;
    invoke-virtual {v2}, Lorg/apache/poi/hssf/record/RowRecord;->getOutlineLevel()S

    move-result v1

    .line 413
    .local v1, "level":I
    move v0, p1

    .line 414
    .local v0, "currentRow":I
    :goto_0
    invoke-virtual {p0, v0}, Lorg/apache/poi/hssf/record/aggregates/RowRecordsAggregate;->getRow(I)Lorg/apache/poi/hssf/record/RowRecord;

    move-result-object v3

    if-nez v3, :cond_0

    .line 422
    add-int/lit8 v3, v0, 0x1

    :goto_1
    return v3

    .line 415
    :cond_0
    invoke-virtual {p0, v0}, Lorg/apache/poi/hssf/record/aggregates/RowRecordsAggregate;->getRow(I)Lorg/apache/poi/hssf/record/RowRecord;

    move-result-object v2

    .line 416
    invoke-virtual {v2}, Lorg/apache/poi/hssf/record/RowRecord;->getOutlineLevel()S

    move-result v3

    if-ge v3, v1, :cond_1

    .line 417
    add-int/lit8 v3, v0, 0x1

    goto :goto_1

    .line 419
    :cond_1
    add-int/lit8 v0, v0, -0x1

    goto :goto_0
.end method

.method public getCellValueIterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Lorg/apache/poi/hssf/record/CellValueRecordInterface;",
            ">;"
        }
    .end annotation

    .prologue
    .line 564
    iget-object v0, p0, Lorg/apache/poi/hssf/record/aggregates/RowRecordsAggregate;->_valuesAgg:Lorg/apache/poi/hssf/record/aggregates/ValueRecordsAggregate;

    invoke-virtual {v0}, Lorg/apache/poi/hssf/record/aggregates/ValueRecordsAggregate;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public getFirstRowNum()I
    .locals 1

    .prologue
    .line 279
    iget v0, p0, Lorg/apache/poi/hssf/record/aggregates/RowRecordsAggregate;->_firstrow:I

    return v0
.end method

.method public getIterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Lorg/apache/poi/hssf/record/RowRecord;",
            ">;"
        }
    .end annotation

    .prologue
    .line 406
    iget-object v0, p0, Lorg/apache/poi/hssf/record/aggregates/RowRecordsAggregate;->_rowRecords:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public getLastRowNum()I
    .locals 1

    .prologue
    .line 284
    iget v0, p0, Lorg/apache/poi/hssf/record/aggregates/RowRecordsAggregate;->_lastrow:I

    return v0
.end method

.method public getPhysicalNumberOfRows()I
    .locals 1

    .prologue
    .line 274
    iget-object v0, p0, Lorg/apache/poi/hssf/record/aggregates/RowRecordsAggregate;->_rowRecords:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    return v0
.end method

.method public getRow(I)Lorg/apache/poi/hssf/record/RowRecord;
    .locals 4
    .param p1, "rowIndex"    # I

    .prologue
    .line 265
    sget-object v1, Lorg/apache/poi/ss/SpreadsheetVersion;->EXCEL97:Lorg/apache/poi/ss/SpreadsheetVersion;

    invoke-virtual {v1}, Lorg/apache/poi/ss/SpreadsheetVersion;->getLastRowIndex()I

    move-result v0

    .line 266
    .local v0, "maxrow":I
    if-ltz p1, :cond_0

    if-le p1, v0, :cond_1

    .line 267
    :cond_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "The row number must be between 0 and "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 269
    :cond_1
    iget-object v1, p0, Lorg/apache/poi/hssf/record/aggregates/RowRecordsAggregate;->_rowRecords:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/hssf/record/RowRecord;

    return-object v1
.end method

.method public getRowBlockCount()I
    .locals 2

    .prologue
    .line 292
    iget-object v1, p0, Lorg/apache/poi/hssf/record/aggregates/RowRecordsAggregate;->_rowRecords:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v1

    div-int/lit8 v0, v1, 0x20

    .line 293
    .local v0, "size":I
    iget-object v1, p0, Lorg/apache/poi/hssf/record/aggregates/RowRecordsAggregate;->_rowRecords:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v1

    rem-int/lit8 v1, v1, 0x20

    if-eqz v1, :cond_0

    .line 294
    add-int/lit8 v0, v0, 0x1

    .line 295
    :cond_0
    return v0
.end method

.method public getRowCountForBlock(I)I
    .locals 3
    .param p1, "block"    # I

    .prologue
    .line 304
    mul-int/lit8 v1, p1, 0x20

    .line 305
    .local v1, "startIndex":I
    add-int/lit8 v2, v1, 0x20

    add-int/lit8 v0, v2, -0x1

    .line 306
    .local v0, "endIndex":I
    iget-object v2, p0, Lorg/apache/poi/hssf/record/aggregates/RowRecordsAggregate;->_rowRecords:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->size()I

    move-result v2

    if-lt v0, v2, :cond_0

    .line 307
    iget-object v2, p0, Lorg/apache/poi/hssf/record/aggregates/RowRecordsAggregate;->_rowRecords:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->size()I

    move-result v2

    add-int/lit8 v0, v2, -0x1

    .line 309
    :cond_0
    sub-int v2, v0, v1

    add-int/lit8 v2, v2, 0x1

    return v2
.end method

.method public getValueRecords()[Lorg/apache/poi/hssf/record/CellValueRecordInterface;
    .locals 1

    .prologue
    .line 571
    iget-object v0, p0, Lorg/apache/poi/hssf/record/aggregates/RowRecordsAggregate;->_valuesAgg:Lorg/apache/poi/hssf/record/aggregates/ValueRecordsAggregate;

    invoke-virtual {v0}, Lorg/apache/poi/hssf/record/aggregates/ValueRecordsAggregate;->getValueRecords()[Lorg/apache/poi/hssf/record/CellValueRecordInterface;

    move-result-object v0

    return-object v0
.end method

.method public insertCell(Lorg/apache/poi/hssf/record/CellValueRecordInterface;)V
    .locals 1
    .param p1, "cvRec"    # Lorg/apache/poi/hssf/record/CellValueRecordInterface;

    .prologue
    .line 609
    iget-object v0, p0, Lorg/apache/poi/hssf/record/aggregates/RowRecordsAggregate;->_valuesAgg:Lorg/apache/poi/hssf/record/aggregates/ValueRecordsAggregate;

    invoke-virtual {v0, p1}, Lorg/apache/poi/hssf/record/aggregates/ValueRecordsAggregate;->insertCell(Lorg/apache/poi/hssf/record/CellValueRecordInterface;)V

    .line 610
    return-void
.end method

.method public insertRow(Lorg/apache/poi/hssf/record/RowRecord;)V
    .locals 3
    .param p1, "row"    # Lorg/apache/poi/hssf/record/RowRecord;

    .prologue
    const/4 v2, -0x1

    .line 236
    iget-object v0, p0, Lorg/apache/poi/hssf/record/aggregates/RowRecordsAggregate;->_rowRecords:Ljava/util/Map;

    invoke-virtual {p1}, Lorg/apache/poi/hssf/record/RowRecord;->getRowNumber()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 238
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/poi/hssf/record/aggregates/RowRecordsAggregate;->_rowRecordValues:[Lorg/apache/poi/hssf/record/RowRecord;

    .line 239
    invoke-virtual {p1}, Lorg/apache/poi/hssf/record/RowRecord;->getRowNumber()I

    move-result v0

    iget v1, p0, Lorg/apache/poi/hssf/record/aggregates/RowRecordsAggregate;->_firstrow:I

    if-lt v0, v1, :cond_0

    iget v0, p0, Lorg/apache/poi/hssf/record/aggregates/RowRecordsAggregate;->_firstrow:I

    if-ne v0, v2, :cond_1

    .line 240
    :cond_0
    invoke-virtual {p1}, Lorg/apache/poi/hssf/record/RowRecord;->getRowNumber()I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hssf/record/aggregates/RowRecordsAggregate;->_firstrow:I

    .line 242
    :cond_1
    invoke-virtual {p1}, Lorg/apache/poi/hssf/record/RowRecord;->getRowNumber()I

    move-result v0

    iget v1, p0, Lorg/apache/poi/hssf/record/aggregates/RowRecordsAggregate;->_lastrow:I

    if-gt v0, v1, :cond_2

    iget v0, p0, Lorg/apache/poi/hssf/record/aggregates/RowRecordsAggregate;->_lastrow:I

    if-ne v0, v2, :cond_3

    .line 243
    :cond_2
    invoke-virtual {p1}, Lorg/apache/poi/hssf/record/RowRecord;->getRowNumber()I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hssf/record/aggregates/RowRecordsAggregate;->_lastrow:I

    .line 245
    :cond_3
    return-void
.end method

.method public isRowGroupCollapsed(I)Z
    .locals 2
    .param p1, "row"    # I

    .prologue
    .line 483
    invoke-virtual {p0, p1}, Lorg/apache/poi/hssf/record/aggregates/RowRecordsAggregate;->findEndOfRowOutlineGroup(I)I

    move-result v1

    add-int/lit8 v0, v1, 0x1

    .line 485
    .local v0, "collapseRow":I
    invoke-virtual {p0, v0}, Lorg/apache/poi/hssf/record/aggregates/RowRecordsAggregate;->getRow(I)Lorg/apache/poi/hssf/record/RowRecord;

    move-result-object v1

    if-nez v1, :cond_0

    .line 486
    const/4 v1, 0x0

    .line 488
    :goto_0
    return v1

    :cond_0
    invoke-virtual {p0, v0}, Lorg/apache/poi/hssf/record/aggregates/RowRecordsAggregate;->getRow(I)Lorg/apache/poi/hssf/record/RowRecord;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/poi/hssf/record/RowRecord;->getColapsed()Z

    move-result v1

    goto :goto_0
.end method

.method public isRowGroupHiddenByParent(I)Z
    .locals 7
    .param p1, "row"    # I

    .prologue
    .line 532
    invoke-virtual {p0, p1}, Lorg/apache/poi/hssf/record/aggregates/RowRecordsAggregate;->findEndOfRowOutlineGroup(I)I

    move-result v2

    .line 533
    .local v2, "endOfOutlineGroupIdx":I
    add-int/lit8 v6, v2, 0x1

    invoke-virtual {p0, v6}, Lorg/apache/poi/hssf/record/aggregates/RowRecordsAggregate;->getRow(I)Lorg/apache/poi/hssf/record/RowRecord;

    move-result-object v6

    if-nez v6, :cond_1

    .line 534
    const/4 v1, 0x0

    .line 535
    .local v1, "endLevel":I
    const/4 v0, 0x0

    .line 544
    .local v0, "endHidden":Z
    :goto_0
    invoke-virtual {p0, p1}, Lorg/apache/poi/hssf/record/aggregates/RowRecordsAggregate;->findStartOfRowOutlineGroup(I)I

    move-result v5

    .line 545
    .local v5, "startOfOutlineGroupIdx":I
    add-int/lit8 v6, v5, -0x1

    if-ltz v6, :cond_0

    add-int/lit8 v6, v5, -0x1

    invoke-virtual {p0, v6}, Lorg/apache/poi/hssf/record/aggregates/RowRecordsAggregate;->getRow(I)Lorg/apache/poi/hssf/record/RowRecord;

    move-result-object v6

    if-nez v6, :cond_2

    .line 546
    :cond_0
    const/4 v4, 0x0

    .line 547
    .local v4, "startLevel":I
    const/4 v3, 0x0

    .line 553
    .local v3, "startHidden":Z
    :goto_1
    if-le v1, v4, :cond_3

    .line 557
    .end local v0    # "endHidden":Z
    :goto_2
    return v0

    .line 537
    .end local v1    # "endLevel":I
    .end local v3    # "startHidden":Z
    .end local v4    # "startLevel":I
    .end local v5    # "startOfOutlineGroupIdx":I
    :cond_1
    add-int/lit8 v6, v2, 0x1

    invoke-virtual {p0, v6}, Lorg/apache/poi/hssf/record/aggregates/RowRecordsAggregate;->getRow(I)Lorg/apache/poi/hssf/record/RowRecord;

    move-result-object v6

    invoke-virtual {v6}, Lorg/apache/poi/hssf/record/RowRecord;->getOutlineLevel()S

    move-result v1

    .line 538
    .restart local v1    # "endLevel":I
    add-int/lit8 v6, v2, 0x1

    invoke-virtual {p0, v6}, Lorg/apache/poi/hssf/record/aggregates/RowRecordsAggregate;->getRow(I)Lorg/apache/poi/hssf/record/RowRecord;

    move-result-object v6

    invoke-virtual {v6}, Lorg/apache/poi/hssf/record/RowRecord;->getZeroHeight()Z

    move-result v0

    .restart local v0    # "endHidden":Z
    goto :goto_0

    .line 549
    .restart local v5    # "startOfOutlineGroupIdx":I
    :cond_2
    add-int/lit8 v6, v5, -0x1

    invoke-virtual {p0, v6}, Lorg/apache/poi/hssf/record/aggregates/RowRecordsAggregate;->getRow(I)Lorg/apache/poi/hssf/record/RowRecord;

    move-result-object v6

    invoke-virtual {v6}, Lorg/apache/poi/hssf/record/RowRecord;->getOutlineLevel()S

    move-result v4

    .line 550
    .restart local v4    # "startLevel":I
    add-int/lit8 v6, v5, -0x1

    invoke-virtual {p0, v6}, Lorg/apache/poi/hssf/record/aggregates/RowRecordsAggregate;->getRow(I)Lorg/apache/poi/hssf/record/RowRecord;

    move-result-object v6

    invoke-virtual {v6}, Lorg/apache/poi/hssf/record/RowRecord;->getZeroHeight()Z

    move-result v3

    .restart local v3    # "startHidden":Z
    goto :goto_1

    :cond_3
    move v0, v3

    .line 557
    goto :goto_2
.end method

.method public removeCell(Lorg/apache/poi/hssf/record/CellValueRecordInterface;)V
    .locals 1
    .param p1, "cvRec"    # Lorg/apache/poi/hssf/record/CellValueRecordInterface;

    .prologue
    .line 612
    instance-of v0, p1, Lorg/apache/poi/hssf/record/aggregates/FormulaRecordAggregate;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 613
    check-cast v0, Lorg/apache/poi/hssf/record/aggregates/FormulaRecordAggregate;

    invoke-virtual {v0}, Lorg/apache/poi/hssf/record/aggregates/FormulaRecordAggregate;->notifyFormulaChanging()V

    .line 615
    :cond_0
    iget-object v0, p0, Lorg/apache/poi/hssf/record/aggregates/RowRecordsAggregate;->_valuesAgg:Lorg/apache/poi/hssf/record/aggregates/ValueRecordsAggregate;

    invoke-virtual {v0, p1}, Lorg/apache/poi/hssf/record/aggregates/ValueRecordsAggregate;->removeCell(Lorg/apache/poi/hssf/record/CellValueRecordInterface;)V

    .line 616
    return-void
.end method

.method public removeRow(Lorg/apache/poi/hssf/record/RowRecord;)V
    .locals 6
    .param p1, "row"    # Lorg/apache/poi/hssf/record/RowRecord;

    .prologue
    .line 248
    invoke-virtual {p1}, Lorg/apache/poi/hssf/record/RowRecord;->getRowNumber()I

    move-result v1

    .line 249
    .local v1, "rowIndex":I
    iget-object v3, p0, Lorg/apache/poi/hssf/record/aggregates/RowRecordsAggregate;->_valuesAgg:Lorg/apache/poi/hssf/record/aggregates/ValueRecordsAggregate;

    invoke-virtual {v3, v1}, Lorg/apache/poi/hssf/record/aggregates/ValueRecordsAggregate;->removeAllCellsValuesForRow(I)V

    .line 250
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 251
    .local v0, "key":Ljava/lang/Integer;
    iget-object v3, p0, Lorg/apache/poi/hssf/record/aggregates/RowRecordsAggregate;->_rowRecords:Ljava/util/Map;

    invoke-interface {v3, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/poi/hssf/record/RowRecord;

    .line 252
    .local v2, "rr":Lorg/apache/poi/hssf/record/RowRecord;
    if-nez v2, :cond_0

    .line 253
    new-instance v3, Ljava/lang/RuntimeException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "Invalid row index ("

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 255
    :cond_0
    if-eq p1, v2, :cond_1

    .line 256
    iget-object v3, p0, Lorg/apache/poi/hssf/record/aggregates/RowRecordsAggregate;->_rowRecords:Ljava/util/Map;

    invoke-interface {v3, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 257
    new-instance v3, Ljava/lang/RuntimeException;

    const-string/jumbo v4, "Attempt to remove row that does not belong to this sheet"

    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 261
    :cond_1
    const/4 v3, 0x0

    iput-object v3, p0, Lorg/apache/poi/hssf/record/aggregates/RowRecordsAggregate;->_rowRecordValues:[Lorg/apache/poi/hssf/record/RowRecord;

    .line 262
    return-void
.end method

.method public updateFormulasAfterRowShift(Lorg/apache/poi/ss/formula/FormulaShifter;I)V
    .locals 1
    .param p1, "formulaShifter"    # Lorg/apache/poi/ss/formula/FormulaShifter;
    .param p2, "currentExternSheetIndex"    # I

    .prologue
    .line 624
    iget-object v0, p0, Lorg/apache/poi/hssf/record/aggregates/RowRecordsAggregate;->_valuesAgg:Lorg/apache/poi/hssf/record/aggregates/ValueRecordsAggregate;

    invoke-virtual {v0, p1, p2}, Lorg/apache/poi/hssf/record/aggregates/ValueRecordsAggregate;->updateFormulasAfterRowShift(Lorg/apache/poi/ss/formula/FormulaShifter;I)V

    .line 625
    return-void
.end method

.method public visitContainedRecords(Lorg/apache/poi/hssf/record/aggregates/RecordAggregate$RecordVisitor;)V
    .locals 14
    .param p1, "rv"    # Lorg/apache/poi/hssf/record/aggregates/RecordAggregate$RecordVisitor;

    .prologue
    const/4 v13, 0x0

    .line 368
    new-instance v11, Lorg/apache/poi/hssf/record/aggregates/RecordAggregate$PositionTrackingVisitor;

    invoke-direct {v11, p1, v13}, Lorg/apache/poi/hssf/record/aggregates/RecordAggregate$PositionTrackingVisitor;-><init>(Lorg/apache/poi/hssf/record/aggregates/RecordAggregate$RecordVisitor;I)V

    .line 370
    .local v11, "stv":Lorg/apache/poi/hssf/record/aggregates/RecordAggregate$PositionTrackingVisitor;
    invoke-virtual {p0}, Lorg/apache/poi/hssf/record/aggregates/RowRecordsAggregate;->getRowBlockCount()I

    move-result v0

    .line 371
    .local v0, "blockCount":I
    const/4 v1, 0x0

    .local v1, "blockIndex":I
    :goto_0
    if-lt v1, v0, :cond_0

    .line 399
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_1
    iget-object v12, p0, Lorg/apache/poi/hssf/record/aggregates/RowRecordsAggregate;->_unknownRecords:Ljava/util/List;

    invoke-interface {v12}, Ljava/util/List;->size()I

    move-result v12

    if-lt v5, v12, :cond_3

    .line 403
    return-void

    .line 374
    .end local v5    # "i":I
    :cond_0
    const/4 v6, 0x0

    .line 376
    .local v6, "pos":I
    invoke-direct {p0, v1, p1}, Lorg/apache/poi/hssf/record/aggregates/RowRecordsAggregate;->visitRowRecordsForBlock(ILorg/apache/poi/hssf/record/aggregates/RecordAggregate$RecordVisitor;)I

    move-result v8

    .line 377
    .local v8, "rowBlockSize":I
    add-int/2addr v6, v8

    .line 379
    invoke-direct {p0, v1}, Lorg/apache/poi/hssf/record/aggregates/RowRecordsAggregate;->getStartRowNumberForBlock(I)I

    move-result v10

    .line 380
    .local v10, "startRowNumber":I
    invoke-direct {p0, v1}, Lorg/apache/poi/hssf/record/aggregates/RowRecordsAggregate;->getEndRowNumberForBlock(I)I

    move-result v4

    .line 381
    .local v4, "endRowNumber":I
    new-instance v3, Lorg/apache/poi/hssf/record/DBCellRecord$Builder;

    invoke-direct {v3}, Lorg/apache/poi/hssf/record/DBCellRecord$Builder;-><init>()V

    .line 383
    .local v3, "dbcrBuilder":Lorg/apache/poi/hssf/record/DBCellRecord$Builder;
    add-int/lit8 v2, v8, -0x14

    .line 384
    .local v2, "cellRefOffset":I
    move v7, v10

    .local v7, "row":I
    :goto_2
    if-le v7, v4, :cond_1

    .line 397
    invoke-virtual {v3, v6}, Lorg/apache/poi/hssf/record/DBCellRecord$Builder;->build(I)Lorg/apache/poi/hssf/record/DBCellRecord;

    move-result-object v12

    invoke-interface {p1, v12}, Lorg/apache/poi/hssf/record/aggregates/RecordAggregate$RecordVisitor;->visitRecord(Lorg/apache/poi/hssf/record/Record;)V

    .line 371
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 385
    :cond_1
    iget-object v12, p0, Lorg/apache/poi/hssf/record/aggregates/RowRecordsAggregate;->_valuesAgg:Lorg/apache/poi/hssf/record/aggregates/ValueRecordsAggregate;

    invoke-virtual {v12, v7}, Lorg/apache/poi/hssf/record/aggregates/ValueRecordsAggregate;->rowHasCells(I)Z

    move-result v12

    if-eqz v12, :cond_2

    .line 386
    invoke-virtual {v11, v13}, Lorg/apache/poi/hssf/record/aggregates/RecordAggregate$PositionTrackingVisitor;->setPosition(I)V

    .line 387
    iget-object v12, p0, Lorg/apache/poi/hssf/record/aggregates/RowRecordsAggregate;->_valuesAgg:Lorg/apache/poi/hssf/record/aggregates/ValueRecordsAggregate;

    invoke-virtual {v12, v7, v11}, Lorg/apache/poi/hssf/record/aggregates/ValueRecordsAggregate;->visitCellsForRow(ILorg/apache/poi/hssf/record/aggregates/RecordAggregate$RecordVisitor;)V

    .line 388
    invoke-virtual {v11}, Lorg/apache/poi/hssf/record/aggregates/RecordAggregate$PositionTrackingVisitor;->getPosition()I

    move-result v9

    .line 389
    .local v9, "rowCellSize":I
    add-int/2addr v6, v9

    .line 392
    invoke-virtual {v3, v2}, Lorg/apache/poi/hssf/record/DBCellRecord$Builder;->addCellOffset(I)V

    .line 393
    move v2, v9

    .line 384
    .end local v9    # "rowCellSize":I
    :cond_2
    add-int/lit8 v7, v7, 0x1

    goto :goto_2

    .line 401
    .end local v2    # "cellRefOffset":I
    .end local v3    # "dbcrBuilder":Lorg/apache/poi/hssf/record/DBCellRecord$Builder;
    .end local v4    # "endRowNumber":I
    .end local v6    # "pos":I
    .end local v7    # "row":I
    .end local v8    # "rowBlockSize":I
    .end local v10    # "startRowNumber":I
    .restart local v5    # "i":I
    :cond_3
    iget-object v12, p0, Lorg/apache/poi/hssf/record/aggregates/RowRecordsAggregate;->_unknownRecords:Ljava/util/List;

    invoke-interface {v12, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lorg/apache/poi/hssf/record/Record;

    invoke-interface {p1, v12}, Lorg/apache/poi/hssf/record/aggregates/RecordAggregate$RecordVisitor;->visitRecord(Lorg/apache/poi/hssf/record/Record;)V

    .line 399
    add-int/lit8 v5, v5, 0x1

    goto :goto_1
.end method
