.class public final Lorg/apache/poi/hssf/util/HSSFColor$TAN;
.super Lorg/apache/poi/hssf/util/HSSFColor;
.source "HSSFColor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/poi/hssf/util/HSSFColor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "TAN"
.end annotation


# static fields
.field public static final hexString:Ljava/lang/String; = "FFFF:CCCC:9999"

.field public static final index:S = 0x2fs

.field public static final triplet:[S


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1254
    const/4 v0, 0x3

    new-array v0, v0, [S

    fill-array-data v0, :array_0

    .line 1253
    sput-object v0, Lorg/apache/poi/hssf/util/HSSFColor$TAN;->triplet:[S

    .line 1257
    return-void

    .line 1254
    nop

    :array_0
    .array-data 2
        0xffs
        0xccs
        0x99s
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1249
    invoke-direct {p0}, Lorg/apache/poi/hssf/util/HSSFColor;-><init>()V

    return-void
.end method


# virtual methods
.method public getHexString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1271
    const-string/jumbo v0, "FFFF:CCCC:9999"

    return-object v0
.end method

.method public getIndex()S
    .locals 1

    .prologue
    .line 1261
    const/16 v0, 0x2f

    return v0
.end method

.method public getTriplet()[S
    .locals 1

    .prologue
    .line 1266
    sget-object v0, Lorg/apache/poi/hssf/util/HSSFColor$TAN;->triplet:[S

    return-object v0
.end method
