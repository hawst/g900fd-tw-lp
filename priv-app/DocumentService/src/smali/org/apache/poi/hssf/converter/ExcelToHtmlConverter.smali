.class public Lorg/apache/poi/hssf/converter/ExcelToHtmlConverter;
.super Lorg/apache/poi/hssf/converter/AbstractExcelConverter;
.source "ExcelToHtmlConverter.java"


# static fields
.field private static final logger:Lorg/apache/poi/util/POILogger;


# instance fields
.field private cssClassContainerCell:Ljava/lang/String;

.field private cssClassContainerDiv:Ljava/lang/String;

.field private cssClassPrefixCell:Ljava/lang/String;

.field private cssClassPrefixDiv:Ljava/lang/String;

.field private cssClassPrefixRow:Ljava/lang/String;

.field private cssClassPrefixTable:Ljava/lang/String;

.field private excelStyleToClass:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Short;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final htmlDocumentFacade:Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;

.field private useDivsToSpan:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 67
    const-class v0, Lorg/apache/poi/hssf/converter/ExcelToHtmlConverter;

    invoke-static {v0}, Lorg/apache/poi/util/POILogFactory;->getLogger(Ljava/lang/Class;)Lorg/apache/poi/util/POILogger;

    move-result-object v0

    .line 66
    sput-object v0, Lorg/apache/poi/hssf/converter/ExcelToHtmlConverter;->logger:Lorg/apache/poi/util/POILogger;

    .line 67
    return-void
.end method

.method public constructor <init>(Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;)V
    .locals 1
    .param p1, "htmlDocumentFacade"    # Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;

    .prologue
    const/4 v0, 0x0

    .line 162
    invoke-direct {p0}, Lorg/apache/poi/hssf/converter/AbstractExcelConverter;-><init>()V

    .line 139
    iput-object v0, p0, Lorg/apache/poi/hssf/converter/ExcelToHtmlConverter;->cssClassContainerCell:Ljava/lang/String;

    .line 141
    iput-object v0, p0, Lorg/apache/poi/hssf/converter/ExcelToHtmlConverter;->cssClassContainerDiv:Ljava/lang/String;

    .line 143
    const-string/jumbo v0, "c"

    iput-object v0, p0, Lorg/apache/poi/hssf/converter/ExcelToHtmlConverter;->cssClassPrefixCell:Ljava/lang/String;

    .line 145
    const-string/jumbo v0, "d"

    iput-object v0, p0, Lorg/apache/poi/hssf/converter/ExcelToHtmlConverter;->cssClassPrefixDiv:Ljava/lang/String;

    .line 147
    const-string/jumbo v0, "r"

    iput-object v0, p0, Lorg/apache/poi/hssf/converter/ExcelToHtmlConverter;->cssClassPrefixRow:Ljava/lang/String;

    .line 149
    const-string/jumbo v0, "t"

    iput-object v0, p0, Lorg/apache/poi/hssf/converter/ExcelToHtmlConverter;->cssClassPrefixTable:Ljava/lang/String;

    .line 151
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/hssf/converter/ExcelToHtmlConverter;->excelStyleToClass:Ljava/util/Map;

    .line 155
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/poi/hssf/converter/ExcelToHtmlConverter;->useDivsToSpan:Z

    .line 164
    iput-object p1, p0, Lorg/apache/poi/hssf/converter/ExcelToHtmlConverter;->htmlDocumentFacade:Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;

    .line 165
    return-void
.end method

.method public constructor <init>(Lorg/w3c/dom/Document;)V
    .locals 1
    .param p1, "doc"    # Lorg/w3c/dom/Document;

    .prologue
    const/4 v0, 0x0

    .line 157
    invoke-direct {p0}, Lorg/apache/poi/hssf/converter/AbstractExcelConverter;-><init>()V

    .line 139
    iput-object v0, p0, Lorg/apache/poi/hssf/converter/ExcelToHtmlConverter;->cssClassContainerCell:Ljava/lang/String;

    .line 141
    iput-object v0, p0, Lorg/apache/poi/hssf/converter/ExcelToHtmlConverter;->cssClassContainerDiv:Ljava/lang/String;

    .line 143
    const-string/jumbo v0, "c"

    iput-object v0, p0, Lorg/apache/poi/hssf/converter/ExcelToHtmlConverter;->cssClassPrefixCell:Ljava/lang/String;

    .line 145
    const-string/jumbo v0, "d"

    iput-object v0, p0, Lorg/apache/poi/hssf/converter/ExcelToHtmlConverter;->cssClassPrefixDiv:Ljava/lang/String;

    .line 147
    const-string/jumbo v0, "r"

    iput-object v0, p0, Lorg/apache/poi/hssf/converter/ExcelToHtmlConverter;->cssClassPrefixRow:Ljava/lang/String;

    .line 149
    const-string/jumbo v0, "t"

    iput-object v0, p0, Lorg/apache/poi/hssf/converter/ExcelToHtmlConverter;->cssClassPrefixTable:Ljava/lang/String;

    .line 151
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/hssf/converter/ExcelToHtmlConverter;->excelStyleToClass:Ljava/util/Map;

    .line 155
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/poi/hssf/converter/ExcelToHtmlConverter;->useDivsToSpan:Z

    .line 159
    new-instance v0, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;

    invoke-direct {v0, p1}, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;-><init>(Lorg/w3c/dom/Document;)V

    iput-object v0, p0, Lorg/apache/poi/hssf/converter/ExcelToHtmlConverter;->htmlDocumentFacade:Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;

    .line 160
    return-void
.end method

.method private buildStyle_border(Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;Ljava/lang/StringBuilder;Ljava/lang/String;SS)V
    .locals 4
    .param p1, "workbook"    # Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;
    .param p2, "style"    # Ljava/lang/StringBuilder;
    .param p3, "type"    # Ljava/lang/String;
    .param p4, "xlsBorder"    # S
    .param p5, "borderColor"    # S

    .prologue
    const/16 v3, 0x20

    .line 213
    if-nez p4, :cond_0

    .line 230
    :goto_0
    return-void

    .line 216
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 217
    .local v0, "borderStyle":Ljava/lang/StringBuilder;
    invoke-static {p4}, Lorg/apache/poi/hssf/converter/ExcelToHtmlUtils;->getBorderWidth(S)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 218
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 219
    invoke-static {p4}, Lorg/apache/poi/hssf/converter/ExcelToHtmlUtils;->getBorderStyle(S)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 221
    invoke-virtual {p1}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->getCustomPalette()Lorg/apache/poi/hssf/usermodel/HSSFPalette;

    move-result-object v2

    invoke-virtual {v2, p5}, Lorg/apache/poi/hssf/usermodel/HSSFPalette;->getColor(S)Lorg/apache/poi/hssf/util/HSSFColor;

    move-result-object v1

    .line 223
    .local v1, "color":Lorg/apache/poi/hssf/util/HSSFColor;
    if-eqz v1, :cond_1

    .line 225
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 226
    invoke-static {v1}, Lorg/apache/poi/hssf/converter/ExcelToHtmlUtils;->getColor(Lorg/apache/poi/hssf/util/HSSFColor;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 229
    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "border-"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ";"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method public static main([Ljava/lang/String;)V
    .locals 12
    .param p0, "args"    # [Ljava/lang/String;

    .prologue
    .line 80
    array-length v8, p0

    const/4 v9, 0x2

    if-ge v8, v9, :cond_1

    .line 82
    sget-object v8, Ljava/lang/System;->err:Ljava/io/PrintStream;

    .line 83
    const-string/jumbo v9, "Usage: ExcelToHtmlConverter <inputFile.xls> <saveTo.html>"

    invoke-virtual {v8, v9}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 120
    :cond_0
    :goto_0
    return-void

    .line 90
    :cond_1
    const/4 v3, 0x0

    .line 93
    .local v3, "out":Ljava/io/FileWriter;
    :try_start_0
    new-instance v8, Ljava/io/File;

    const/4 v9, 0x0

    aget-object v9, p0, v9

    invoke-direct {v8, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v8}, Lorg/apache/poi/hssf/converter/ExcelToHtmlConverter;->process(Ljava/io/File;)Lorg/w3c/dom/Document;

    move-result-object v0

    .line 95
    .local v0, "doc":Lorg/w3c/dom/Document;
    new-instance v4, Ljava/io/FileWriter;

    const/4 v8, 0x1

    aget-object v8, p0, v8

    invoke-direct {v4, v8}, Ljava/io/FileWriter;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 96
    .end local v3    # "out":Ljava/io/FileWriter;
    .local v4, "out":Ljava/io/FileWriter;
    :try_start_1
    new-instance v1, Ljavax/xml/transform/dom/DOMSource;

    invoke-direct {v1, v0}, Ljavax/xml/transform/dom/DOMSource;-><init>(Lorg/w3c/dom/Node;)V

    .line 97
    .local v1, "domSource":Ljavax/xml/transform/dom/DOMSource;
    new-instance v6, Ljavax/xml/transform/stream/StreamResult;

    invoke-direct {v6, v4}, Ljavax/xml/transform/stream/StreamResult;-><init>(Ljava/io/Writer;)V

    .line 99
    .local v6, "streamResult":Ljavax/xml/transform/stream/StreamResult;
    invoke-static {}, Ljavax/xml/transform/TransformerFactory;->newInstance()Ljavax/xml/transform/TransformerFactory;

    move-result-object v7

    .line 100
    .local v7, "tf":Ljavax/xml/transform/TransformerFactory;
    invoke-virtual {v7}, Ljavax/xml/transform/TransformerFactory;->newTransformer()Ljavax/xml/transform/Transformer;

    move-result-object v5

    .line 102
    .local v5, "serializer":Ljavax/xml/transform/Transformer;
    const-string/jumbo v8, "encoding"

    const-string/jumbo v9, "UTF-8"

    invoke-virtual {v5, v8, v9}, Ljavax/xml/transform/Transformer;->setOutputProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 103
    const-string/jumbo v8, "indent"

    const-string/jumbo v9, "no"

    invoke-virtual {v5, v8, v9}, Ljavax/xml/transform/Transformer;->setOutputProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 104
    const-string/jumbo v8, "method"

    const-string/jumbo v9, "html"

    invoke-virtual {v5, v8, v9}, Ljavax/xml/transform/Transformer;->setOutputProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 105
    invoke-virtual {v5, v1, v6}, Ljavax/xml/transform/Transformer;->transform(Ljavax/xml/transform/Source;Ljavax/xml/transform/Result;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 113
    if-eqz v4, :cond_3

    .line 115
    :try_start_2
    invoke-virtual {v4}, Ljava/io/FileWriter;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3

    move-object v3, v4

    .line 116
    .end local v4    # "out":Ljava/io/FileWriter;
    .restart local v3    # "out":Ljava/io/FileWriter;
    goto :goto_0

    .line 108
    .end local v0    # "doc":Lorg/w3c/dom/Document;
    .end local v1    # "domSource":Ljavax/xml/transform/dom/DOMSource;
    .end local v5    # "serializer":Ljavax/xml/transform/Transformer;
    .end local v6    # "streamResult":Ljavax/xml/transform/stream/StreamResult;
    .end local v7    # "tf":Ljavax/xml/transform/TransformerFactory;
    :catch_0
    move-exception v2

    .line 110
    .local v2, "e":Ljava/lang/Exception;
    :goto_1
    :try_start_3
    const-string/jumbo v8, "DocumentService"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string/jumbo v10, "Exception: "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 113
    if-eqz v3, :cond_0

    .line 115
    :try_start_4
    invoke-virtual {v3}, Ljava/io/FileWriter;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_0

    .line 116
    :catch_1
    move-exception v2

    .line 117
    .local v2, "e":Ljava/io/IOException;
    const-string/jumbo v8, "DocumentService"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string/jumbo v10, "Exception: "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 112
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v8

    .line 113
    :goto_2
    if-eqz v3, :cond_2

    .line 115
    :try_start_5
    invoke-virtual {v3}, Ljava/io/FileWriter;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    .line 119
    :cond_2
    :goto_3
    throw v8

    .line 116
    :catch_2
    move-exception v2

    .line 117
    .restart local v2    # "e":Ljava/io/IOException;
    const-string/jumbo v9, "DocumentService"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string/jumbo v11, "Exception: "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 116
    .end local v2    # "e":Ljava/io/IOException;
    .end local v3    # "out":Ljava/io/FileWriter;
    .restart local v0    # "doc":Lorg/w3c/dom/Document;
    .restart local v1    # "domSource":Ljavax/xml/transform/dom/DOMSource;
    .restart local v4    # "out":Ljava/io/FileWriter;
    .restart local v5    # "serializer":Ljavax/xml/transform/Transformer;
    .restart local v6    # "streamResult":Ljavax/xml/transform/stream/StreamResult;
    .restart local v7    # "tf":Ljavax/xml/transform/TransformerFactory;
    :catch_3
    move-exception v2

    .line 117
    .restart local v2    # "e":Ljava/io/IOException;
    const-string/jumbo v8, "DocumentService"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string/jumbo v10, "Exception: "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .end local v2    # "e":Ljava/io/IOException;
    :cond_3
    move-object v3, v4

    .end local v4    # "out":Ljava/io/FileWriter;
    .restart local v3    # "out":Ljava/io/FileWriter;
    goto/16 :goto_0

    .line 112
    .end local v1    # "domSource":Ljavax/xml/transform/dom/DOMSource;
    .end local v3    # "out":Ljava/io/FileWriter;
    .end local v5    # "serializer":Ljavax/xml/transform/Transformer;
    .end local v6    # "streamResult":Ljavax/xml/transform/stream/StreamResult;
    .end local v7    # "tf":Ljavax/xml/transform/TransformerFactory;
    .restart local v4    # "out":Ljava/io/FileWriter;
    :catchall_1
    move-exception v8

    move-object v3, v4

    .end local v4    # "out":Ljava/io/FileWriter;
    .restart local v3    # "out":Ljava/io/FileWriter;
    goto :goto_2

    .line 108
    .end local v3    # "out":Ljava/io/FileWriter;
    .restart local v4    # "out":Ljava/io/FileWriter;
    :catch_4
    move-exception v2

    move-object v3, v4

    .end local v4    # "out":Ljava/io/FileWriter;
    .restart local v3    # "out":Ljava/io/FileWriter;
    goto/16 :goto_1
.end method

.method public static process(Ljava/io/File;)Lorg/w3c/dom/Document;
    .locals 3
    .param p0, "xlsFile"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 131
    invoke-static {p0}, Lorg/apache/poi/hssf/converter/ExcelToHtmlUtils;->loadXls(Ljava/io/File;)Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;

    move-result-object v1

    .line 132
    .local v1, "workbook":Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;
    new-instance v0, Lorg/apache/poi/hssf/converter/ExcelToHtmlConverter;

    .line 133
    invoke-static {}, Ljavax/xml/parsers/DocumentBuilderFactory;->newInstance()Ljavax/xml/parsers/DocumentBuilderFactory;

    move-result-object v2

    invoke-virtual {v2}, Ljavax/xml/parsers/DocumentBuilderFactory;->newDocumentBuilder()Ljavax/xml/parsers/DocumentBuilder;

    move-result-object v2

    .line 134
    invoke-virtual {v2}, Ljavax/xml/parsers/DocumentBuilder;->newDocument()Lorg/w3c/dom/Document;

    move-result-object v2

    .line 132
    invoke-direct {v0, v2}, Lorg/apache/poi/hssf/converter/ExcelToHtmlConverter;-><init>(Lorg/w3c/dom/Document;)V

    .line 135
    .local v0, "excelToHtmlConverter":Lorg/apache/poi/hssf/converter/ExcelToHtmlConverter;
    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/converter/ExcelToHtmlConverter;->processWorkbook(Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;)V

    .line 136
    invoke-virtual {v0}, Lorg/apache/poi/hssf/converter/ExcelToHtmlConverter;->getDocument()Lorg/w3c/dom/Document;

    move-result-object v2

    return-object v2
.end method


# virtual methods
.method protected buildStyle(Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;Lorg/apache/poi/hssf/usermodel/HSSFCellStyle;)Ljava/lang/String;
    .locals 9
    .param p1, "workbook"    # Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;
    .param p2, "cellStyle"    # Lorg/apache/poi/hssf/usermodel/HSSFCellStyle;

    .prologue
    .line 169
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 171
    .local v2, "style":Ljava/lang/StringBuilder;
    const-string/jumbo v0, "white-space:pre-wrap;"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 172
    invoke-virtual {p2}, Lorg/apache/poi/hssf/usermodel/HSSFCellStyle;->getAlignment()S

    move-result v0

    invoke-static {v2, v0}, Lorg/apache/poi/hssf/converter/ExcelToHtmlUtils;->appendAlign(Ljava/lang/StringBuilder;S)V

    .line 174
    invoke-virtual {p2}, Lorg/apache/poi/hssf/usermodel/HSSFCellStyle;->getFillPattern()S

    move-result v0

    if-eqz v0, :cond_0

    .line 178
    invoke-virtual {p2}, Lorg/apache/poi/hssf/usermodel/HSSFCellStyle;->getFillPattern()S

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 181
    invoke-virtual {p2}, Lorg/apache/poi/hssf/usermodel/HSSFCellStyle;->getFillForegroundColorColor()Lorg/apache/poi/hssf/util/HSSFColor;

    move-result-object v8

    .line 182
    .local v8, "foregroundColor":Lorg/apache/poi/hssf/util/HSSFColor;
    if-eqz v8, :cond_0

    .line 183
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "background-color:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 184
    invoke-static {v8}, Lorg/apache/poi/hssf/converter/ExcelToHtmlUtils;->getColor(Lorg/apache/poi/hssf/util/HSSFColor;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ";"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 183
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 195
    .end local v8    # "foregroundColor":Lorg/apache/poi/hssf/util/HSSFColor;
    :cond_0
    :goto_0
    const-string/jumbo v3, "top"

    invoke-virtual {p2}, Lorg/apache/poi/hssf/usermodel/HSSFCellStyle;->getBorderTop()S

    move-result v4

    .line 196
    invoke-virtual {p2}, Lorg/apache/poi/hssf/usermodel/HSSFCellStyle;->getTopBorderColor()S

    move-result v5

    move-object v0, p0

    move-object v1, p1

    .line 195
    invoke-direct/range {v0 .. v5}, Lorg/apache/poi/hssf/converter/ExcelToHtmlConverter;->buildStyle_border(Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;Ljava/lang/StringBuilder;Ljava/lang/String;SS)V

    .line 197
    const-string/jumbo v3, "right"

    .line 198
    invoke-virtual {p2}, Lorg/apache/poi/hssf/usermodel/HSSFCellStyle;->getBorderRight()S

    move-result v4

    invoke-virtual {p2}, Lorg/apache/poi/hssf/usermodel/HSSFCellStyle;->getRightBorderColor()S

    move-result v5

    move-object v0, p0

    move-object v1, p1

    .line 197
    invoke-direct/range {v0 .. v5}, Lorg/apache/poi/hssf/converter/ExcelToHtmlConverter;->buildStyle_border(Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;Ljava/lang/StringBuilder;Ljava/lang/String;SS)V

    .line 199
    const-string/jumbo v3, "bottom"

    .line 200
    invoke-virtual {p2}, Lorg/apache/poi/hssf/usermodel/HSSFCellStyle;->getBorderBottom()S

    move-result v4

    invoke-virtual {p2}, Lorg/apache/poi/hssf/usermodel/HSSFCellStyle;->getBottomBorderColor()S

    move-result v5

    move-object v0, p0

    move-object v1, p1

    .line 199
    invoke-direct/range {v0 .. v5}, Lorg/apache/poi/hssf/converter/ExcelToHtmlConverter;->buildStyle_border(Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;Ljava/lang/StringBuilder;Ljava/lang/String;SS)V

    .line 201
    const-string/jumbo v3, "left"

    invoke-virtual {p2}, Lorg/apache/poi/hssf/usermodel/HSSFCellStyle;->getBorderLeft()S

    move-result v4

    .line 202
    invoke-virtual {p2}, Lorg/apache/poi/hssf/usermodel/HSSFCellStyle;->getLeftBorderColor()S

    move-result v5

    move-object v0, p0

    move-object v1, p1

    .line 201
    invoke-direct/range {v0 .. v5}, Lorg/apache/poi/hssf/converter/ExcelToHtmlConverter;->buildStyle_border(Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;Ljava/lang/StringBuilder;Ljava/lang/String;SS)V

    .line 204
    invoke-virtual {p2, p1}, Lorg/apache/poi/hssf/usermodel/HSSFCellStyle;->getFont(Lorg/apache/poi/ss/usermodel/Workbook;)Lorg/apache/poi/hssf/usermodel/HSSFFont;

    move-result-object v7

    .line 205
    .local v7, "font":Lorg/apache/poi/hssf/usermodel/HSSFFont;
    invoke-virtual {p0, p1, v2, v7}, Lorg/apache/poi/hssf/converter/ExcelToHtmlConverter;->buildStyle_font(Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;Ljava/lang/StringBuilder;Lorg/apache/poi/hssf/usermodel/HSSFFont;)V

    .line 207
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 189
    .end local v7    # "font":Lorg/apache/poi/hssf/usermodel/HSSFFont;
    :cond_1
    invoke-virtual {p2}, Lorg/apache/poi/hssf/usermodel/HSSFCellStyle;->getFillBackgroundColorColor()Lorg/apache/poi/hssf/util/HSSFColor;

    move-result-object v6

    .line 190
    .local v6, "backgroundColor":Lorg/apache/poi/hssf/util/HSSFColor;
    if-eqz v6, :cond_0

    .line 191
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "background-color:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 192
    invoke-static {v6}, Lorg/apache/poi/hssf/converter/ExcelToHtmlUtils;->getColor(Lorg/apache/poi/hssf/util/HSSFColor;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ";"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 191
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method buildStyle_font(Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;Ljava/lang/StringBuilder;Lorg/apache/poi/hssf/usermodel/HSSFFont;)V
    .locals 3
    .param p1, "workbook"    # Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;
    .param p2, "style"    # Ljava/lang/StringBuilder;
    .param p3, "font"    # Lorg/apache/poi/hssf/usermodel/HSSFFont;

    .prologue
    .line 235
    invoke-virtual {p3}, Lorg/apache/poi/hssf/usermodel/HSSFFont;->getBoldweight()S

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 246
    :goto_0
    invoke-virtual {p1}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->getCustomPalette()Lorg/apache/poi/hssf/usermodel/HSSFPalette;

    move-result-object v1

    .line 247
    invoke-virtual {p3}, Lorg/apache/poi/hssf/usermodel/HSSFFont;->getColor()S

    move-result v2

    .line 246
    invoke-virtual {v1, v2}, Lorg/apache/poi/hssf/usermodel/HSSFPalette;->getColor(S)Lorg/apache/poi/hssf/util/HSSFColor;

    move-result-object v0

    .line 248
    .local v0, "fontColor":Lorg/apache/poi/hssf/util/HSSFColor;
    if-eqz v0, :cond_0

    .line 249
    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "color: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lorg/apache/poi/hssf/converter/ExcelToHtmlUtils;->getColor(Lorg/apache/poi/hssf/util/HSSFColor;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 250
    const-string/jumbo v2, "; "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 249
    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 252
    :cond_0
    invoke-virtual {p3}, Lorg/apache/poi/hssf/usermodel/HSSFFont;->getFontHeightInPoints()S

    move-result v1

    if-eqz v1, :cond_1

    .line 253
    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "font-size:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p3}, Lorg/apache/poi/hssf/usermodel/HSSFFont;->getFontHeightInPoints()S

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "pt;"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 255
    :cond_1
    invoke-virtual {p3}, Lorg/apache/poi/hssf/usermodel/HSSFFont;->getItalic()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 257
    const-string/jumbo v1, "font-style:italic;"

    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 259
    :cond_2
    return-void

    .line 238
    .end local v0    # "fontColor":Lorg/apache/poi/hssf/util/HSSFColor;
    :pswitch_0
    const-string/jumbo v1, "font-weight:bold;"

    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 235
    nop

    :pswitch_data_0
    .packed-switch 0x2bc
        :pswitch_0
    .end packed-switch
.end method

.method public getCssClassPrefixCell()Ljava/lang/String;
    .locals 1

    .prologue
    .line 263
    iget-object v0, p0, Lorg/apache/poi/hssf/converter/ExcelToHtmlConverter;->cssClassPrefixCell:Ljava/lang/String;

    return-object v0
.end method

.method public getCssClassPrefixDiv()Ljava/lang/String;
    .locals 1

    .prologue
    .line 268
    iget-object v0, p0, Lorg/apache/poi/hssf/converter/ExcelToHtmlConverter;->cssClassPrefixDiv:Ljava/lang/String;

    return-object v0
.end method

.method public getCssClassPrefixRow()Ljava/lang/String;
    .locals 1

    .prologue
    .line 273
    iget-object v0, p0, Lorg/apache/poi/hssf/converter/ExcelToHtmlConverter;->cssClassPrefixRow:Ljava/lang/String;

    return-object v0
.end method

.method public getCssClassPrefixTable()Ljava/lang/String;
    .locals 1

    .prologue
    .line 278
    iget-object v0, p0, Lorg/apache/poi/hssf/converter/ExcelToHtmlConverter;->cssClassPrefixTable:Ljava/lang/String;

    return-object v0
.end method

.method public getDocument()Lorg/w3c/dom/Document;
    .locals 1

    .prologue
    .line 283
    iget-object v0, p0, Lorg/apache/poi/hssf/converter/ExcelToHtmlConverter;->htmlDocumentFacade:Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;

    invoke-virtual {v0}, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->getDocument()Lorg/w3c/dom/Document;

    move-result-object v0

    return-object v0
.end method

.method protected getStyleClassName(Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;Lorg/apache/poi/hssf/usermodel/HSSFCellStyle;)Ljava/lang/String;
    .locals 6
    .param p1, "workbook"    # Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;
    .param p2, "cellStyle"    # Lorg/apache/poi/hssf/usermodel/HSSFCellStyle;

    .prologue
    .line 289
    invoke-virtual {p2}, Lorg/apache/poi/hssf/usermodel/HSSFCellStyle;->getIndex()S

    move-result v4

    invoke-static {v4}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v0

    .line 291
    .local v0, "cellStyleKey":Ljava/lang/Short;
    iget-object v4, p0, Lorg/apache/poi/hssf/converter/ExcelToHtmlConverter;->excelStyleToClass:Ljava/util/Map;

    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 292
    .local v3, "knownClass":Ljava/lang/String;
    if-eqz v3, :cond_0

    .line 299
    .end local v3    # "knownClass":Ljava/lang/String;
    :goto_0
    return-object v3

    .line 295
    .restart local v3    # "knownClass":Ljava/lang/String;
    :cond_0
    invoke-virtual {p0, p1, p2}, Lorg/apache/poi/hssf/converter/ExcelToHtmlConverter;->buildStyle(Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;Lorg/apache/poi/hssf/usermodel/HSSFCellStyle;)Ljava/lang/String;

    move-result-object v2

    .line 296
    .local v2, "cssStyle":Ljava/lang/String;
    iget-object v4, p0, Lorg/apache/poi/hssf/converter/ExcelToHtmlConverter;->htmlDocumentFacade:Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;

    .line 297
    iget-object v5, p0, Lorg/apache/poi/hssf/converter/ExcelToHtmlConverter;->cssClassPrefixCell:Ljava/lang/String;

    .line 296
    invoke-virtual {v4, v5, v2}, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->getOrCreateCssClass(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 298
    .local v1, "cssClass":Ljava/lang/String;
    iget-object v4, p0, Lorg/apache/poi/hssf/converter/ExcelToHtmlConverter;->excelStyleToClass:Ljava/util/Map;

    invoke-interface {v4, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v3, v1

    .line 299
    goto :goto_0
.end method

.method public isUseDivsToSpan()Z
    .locals 1

    .prologue
    .line 304
    iget-boolean v0, p0, Lorg/apache/poi/hssf/converter/ExcelToHtmlConverter;->useDivsToSpan:Z

    return v0
.end method

.method protected processCell(Lorg/apache/poi/hssf/usermodel/HSSFCell;Lorg/w3c/dom/Element;IIF)Z
    .locals 24
    .param p1, "cell"    # Lorg/apache/poi/hssf/usermodel/HSSFCell;
    .param p2, "tableCellElement"    # Lorg/w3c/dom/Element;
    .param p3, "normalWidthPx"    # I
    .param p4, "maxSpannedWidthPx"    # I
    .param p5, "normalHeightPt"    # F

    .prologue
    .line 310
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hssf/usermodel/HSSFCell;->getCellStyle()Lorg/apache/poi/hssf/usermodel/HSSFCellStyle;

    move-result-object v6

    .line 313
    .local v6, "cellStyle":Lorg/apache/poi/hssf/usermodel/HSSFCellStyle;
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hssf/usermodel/HSSFCell;->getCellType()I

    move-result v19

    packed-switch v19, :pswitch_data_0

    .line 374
    sget-object v19, Lorg/apache/poi/hssf/converter/ExcelToHtmlConverter;->logger:Lorg/apache/poi/util/POILogger;

    const/16 v20, 0x5

    .line 375
    new-instance v21, Ljava/lang/StringBuilder;

    const-string/jumbo v22, "Unexpected cell type ("

    invoke-direct/range {v21 .. v22}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hssf/usermodel/HSSFCell;->getCellType()I

    move-result v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string/jumbo v22, ")"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    .line 374
    invoke-virtual/range {v19 .. v21}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;)V

    .line 376
    const/16 v19, 0x1

    .line 460
    :goto_0
    return v19

    .line 317
    :pswitch_0
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hssf/usermodel/HSSFCell;->getRichStringCellValue()Lorg/apache/poi/hssf/usermodel/HSSFRichTextString;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/hssf/usermodel/HSSFRichTextString;->getString()Ljava/lang/String;

    move-result-object v16

    .line 379
    .local v16, "value":Ljava/lang/String;
    :goto_1
    invoke-static/range {v16 .. v16}, Lorg/apache/poi/hssf/converter/ExcelToHtmlUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v11

    .line 380
    .local v11, "noText":Z
    if-nez v11, :cond_7

    invoke-virtual/range {p0 .. p0}, Lorg/apache/poi/hssf/converter/ExcelToHtmlConverter;->isUseDivsToSpan()Z

    move-result v19

    if-eqz v19, :cond_7

    .line 381
    invoke-virtual {v6}, Lorg/apache/poi/hssf/usermodel/HSSFCellStyle;->getWrapText()Z

    move-result v19

    if-nez v19, :cond_7

    .line 380
    const/16 v18, 0x1

    .line 383
    .local v18, "wrapInDivs":Z
    :goto_2
    invoke-virtual {v6}, Lorg/apache/poi/hssf/usermodel/HSSFCellStyle;->getIndex()S

    move-result v7

    .line 384
    .local v7, "cellStyleIndex":S
    if-eqz v7, :cond_0

    .line 386
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hssf/usermodel/HSSFCell;->getRow()Lorg/apache/poi/hssf/usermodel/HSSFRow;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/hssf/usermodel/HSSFRow;->getSheet()Lorg/apache/poi/hssf/usermodel/HSSFSheet;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->getWorkbook()Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;

    move-result-object v17

    .line 387
    .local v17, "workbook":Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;
    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-virtual {v0, v1, v6}, Lorg/apache/poi/hssf/converter/ExcelToHtmlConverter;->getStyleClassName(Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;Lorg/apache/poi/hssf/usermodel/HSSFCellStyle;)Ljava/lang/String;

    move-result-object v10

    .line 388
    .local v10, "mainCssClass":Ljava/lang/String;
    if-eqz v18, :cond_8

    .line 390
    const-string/jumbo v19, "class"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-static {v10}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v21

    invoke-direct/range {v20 .. v21}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v21, " "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    .line 391
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/poi/hssf/converter/ExcelToHtmlConverter;->cssClassContainerCell:Ljava/lang/String;

    move-object/from16 v21, v0

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    .line 390
    move-object/from16 v0, p2

    move-object/from16 v1, v19

    move-object/from16 v2, v20

    invoke-interface {v0, v1, v2}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 398
    :goto_3
    if-eqz v11, :cond_0

    .line 405
    const-string/jumbo v16, "\u00a0"

    .line 409
    .end local v10    # "mainCssClass":Ljava/lang/String;
    .end local v17    # "workbook":Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;
    :cond_0
    invoke-virtual/range {p0 .. p0}, Lorg/apache/poi/hssf/converter/ExcelToHtmlConverter;->isOutputLeadingSpacesAsNonBreaking()Z

    move-result v19

    if-eqz v19, :cond_3

    const-string/jumbo v19, " "

    move-object/from16 v0, v16

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v19

    if-eqz v19, :cond_3

    .line 411
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 412
    .local v4, "builder":Ljava/lang/StringBuilder;
    const/4 v5, 0x0

    .local v5, "c":I
    :goto_4
    invoke-virtual/range {v16 .. v16}, Ljava/lang/String;->length()I

    move-result v19

    move/from16 v0, v19

    if-lt v5, v0, :cond_9

    .line 419
    :cond_1
    invoke-virtual/range {v16 .. v16}, Ljava/lang/String;->length()I

    move-result v19

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->length()I

    move-result v20

    move/from16 v0, v19

    move/from16 v1, v20

    if-eq v0, v1, :cond_2

    .line 420
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->length()I

    move-result v19

    move-object/from16 v0, v16

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 422
    :cond_2
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    .line 425
    .end local v4    # "builder":Ljava/lang/StringBuilder;
    .end local v5    # "c":I
    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/poi/hssf/converter/ExcelToHtmlConverter;->htmlDocumentFacade:Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->createText(Ljava/lang/String;)Lorg/w3c/dom/Text;

    move-result-object v15

    .line 427
    .local v15, "text":Lorg/w3c/dom/Text;
    if-eqz v18, :cond_a

    .line 429
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/poi/hssf/converter/ExcelToHtmlConverter;->htmlDocumentFacade:Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->createBlock()Lorg/w3c/dom/Element;

    move-result-object v12

    .line 430
    .local v12, "outerDiv":Lorg/w3c/dom/Element;
    const-string/jumbo v19, "class"

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/poi/hssf/converter/ExcelToHtmlConverter;->cssClassContainerDiv:Ljava/lang/String;

    move-object/from16 v20, v0

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-interface {v12, v0, v1}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 432
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/poi/hssf/converter/ExcelToHtmlConverter;->htmlDocumentFacade:Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->createBlock()Lorg/w3c/dom/Element;

    move-result-object v8

    .line 433
    .local v8, "innerDiv":Lorg/w3c/dom/Element;
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    .line 434
    .local v9, "innerDivStyle":Ljava/lang/StringBuilder;
    const-string/jumbo v19, "position:absolute;min-width:"

    move-object/from16 v0, v19

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 435
    move/from16 v0, p3

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 436
    const-string/jumbo v19, "px;"

    move-object/from16 v0, v19

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 437
    const v19, 0x7fffffff

    move/from16 v0, p4

    move/from16 v1, v19

    if-eq v0, v1, :cond_4

    .line 439
    const-string/jumbo v19, "max-width:"

    move-object/from16 v0, v19

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 440
    move/from16 v0, p4

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 441
    const-string/jumbo v19, "px;"

    move-object/from16 v0, v19

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 443
    :cond_4
    const-string/jumbo v19, "overflow:hidden;max-height:"

    move-object/from16 v0, v19

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 444
    move/from16 v0, p5

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 445
    const-string/jumbo v19, "pt;white-space:nowrap;"

    move-object/from16 v0, v19

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 447
    invoke-virtual {v6}, Lorg/apache/poi/hssf/usermodel/HSSFCellStyle;->getAlignment()S

    move-result v19

    .line 446
    move/from16 v0, v19

    invoke-static {v9, v0}, Lorg/apache/poi/hssf/converter/ExcelToHtmlUtils;->appendAlign(Ljava/lang/StringBuilder;S)V

    .line 448
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/poi/hssf/converter/ExcelToHtmlConverter;->htmlDocumentFacade:Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/poi/hssf/converter/ExcelToHtmlConverter;->cssClassPrefixDiv:Ljava/lang/String;

    move-object/from16 v20, v0

    .line 449
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    .line 448
    move-object/from16 v0, v19

    move-object/from16 v1, v20

    move-object/from16 v2, v21

    invoke-virtual {v0, v12, v1, v2}, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->addStyleClass(Lorg/w3c/dom/Element;Ljava/lang/String;Ljava/lang/String;)V

    .line 451
    invoke-interface {v8, v15}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 452
    invoke-interface {v12, v8}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 453
    move-object/from16 v0, p2

    invoke-interface {v0, v12}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 460
    .end local v8    # "innerDiv":Lorg/w3c/dom/Element;
    .end local v9    # "innerDivStyle":Ljava/lang/StringBuilder;
    .end local v12    # "outerDiv":Lorg/w3c/dom/Element;
    :goto_5
    invoke-static/range {v16 .. v16}, Lorg/apache/poi/hssf/converter/ExcelToHtmlUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v19

    if-eqz v19, :cond_b

    if-nez v7, :cond_b

    const/16 v19, 0x1

    goto/16 :goto_0

    .line 320
    .end local v7    # "cellStyleIndex":S
    .end local v11    # "noText":Z
    .end local v15    # "text":Lorg/w3c/dom/Text;
    .end local v16    # "value":Ljava/lang/String;
    .end local v18    # "wrapInDivs":Z
    :pswitch_1
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hssf/usermodel/HSSFCell;->getCachedFormulaResultType()I

    move-result v19

    packed-switch v19, :pswitch_data_1

    .line 353
    :pswitch_2
    sget-object v19, Lorg/apache/poi/hssf/converter/ExcelToHtmlConverter;->logger:Lorg/apache/poi/util/POILogger;

    .line 354
    const/16 v20, 0x5

    .line 355
    new-instance v21, Ljava/lang/StringBuilder;

    const-string/jumbo v22, "Unexpected cell cachedFormulaResultType ("

    invoke-direct/range {v21 .. v22}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 356
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hssf/usermodel/HSSFCell;->getCachedFormulaResultType()I

    move-result v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string/jumbo v22, ")"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    .line 355
    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    .line 353
    invoke-virtual/range {v19 .. v21}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;)V

    .line 357
    const-string/jumbo v16, ""

    .line 360
    .restart local v16    # "value":Ljava/lang/String;
    goto/16 :goto_1

    .line 323
    .end local v16    # "value":Ljava/lang/String;
    :pswitch_3
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hssf/usermodel/HSSFCell;->getRichStringCellValue()Lorg/apache/poi/hssf/usermodel/HSSFRichTextString;

    move-result-object v13

    .line 324
    .local v13, "str":Lorg/apache/poi/hssf/usermodel/HSSFRichTextString;
    if-eqz v13, :cond_5

    invoke-virtual {v13}, Lorg/apache/poi/hssf/usermodel/HSSFRichTextString;->length()I

    move-result v19

    if-lez v19, :cond_5

    .line 326
    invoke-virtual {v13}, Lorg/apache/poi/hssf/usermodel/HSSFRichTextString;->toString()Ljava/lang/String;

    move-result-object v16

    .line 327
    .restart local v16    # "value":Ljava/lang/String;
    goto/16 :goto_1

    .line 330
    .end local v16    # "value":Ljava/lang/String;
    :cond_5
    const-string/jumbo v16, ""

    .line 332
    .restart local v16    # "value":Ljava/lang/String;
    goto/16 :goto_1

    .line 334
    .end local v13    # "str":Lorg/apache/poi/hssf/usermodel/HSSFRichTextString;
    .end local v16    # "value":Ljava/lang/String;
    :pswitch_4
    move-object v14, v6

    .line 335
    .local v14, "style":Lorg/apache/poi/hssf/usermodel/HSSFCellStyle;
    if-nez v14, :cond_6

    .line 337
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hssf/usermodel/HSSFCell;->getNumericCellValue()D

    move-result-wide v20

    invoke-static/range {v20 .. v21}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v16

    .line 338
    .restart local v16    # "value":Ljava/lang/String;
    goto/16 :goto_1

    .line 341
    .end local v16    # "value":Ljava/lang/String;
    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/poi/hssf/converter/ExcelToHtmlConverter;->_formatter:Lorg/apache/poi/hssf/usermodel/HSSFDataFormatter;

    move-object/from16 v19, v0

    .line 342
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hssf/usermodel/HSSFCell;->getNumericCellValue()D

    move-result-wide v20

    invoke-virtual {v14}, Lorg/apache/poi/hssf/usermodel/HSSFCellStyle;->getDataFormat()S

    move-result v22

    .line 343
    invoke-virtual {v14}, Lorg/apache/poi/hssf/usermodel/HSSFCellStyle;->getDataFormatString()Ljava/lang/String;

    move-result-object v23

    .line 341
    invoke-virtual/range {v19 .. v23}, Lorg/apache/poi/hssf/usermodel/HSSFDataFormatter;->formatRawCellContents(DILjava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 345
    .restart local v16    # "value":Ljava/lang/String;
    goto/16 :goto_1

    .line 347
    .end local v14    # "style":Lorg/apache/poi/hssf/usermodel/HSSFCellStyle;
    .end local v16    # "value":Ljava/lang/String;
    :pswitch_5
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hssf/usermodel/HSSFCell;->getBooleanCellValue()Z

    move-result v19

    invoke-static/range {v19 .. v19}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v16

    .line 348
    .restart local v16    # "value":Ljava/lang/String;
    goto/16 :goto_1

    .line 350
    .end local v16    # "value":Ljava/lang/String;
    :pswitch_6
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hssf/usermodel/HSSFCell;->getErrorCellValue()B

    move-result v19

    invoke-static/range {v19 .. v19}, Lorg/apache/poi/ss/formula/eval/ErrorEval;->getText(I)Ljava/lang/String;

    move-result-object v16

    .line 351
    .restart local v16    # "value":Ljava/lang/String;
    goto/16 :goto_1

    .line 362
    .end local v16    # "value":Ljava/lang/String;
    :pswitch_7
    const-string/jumbo v16, ""

    .line 363
    .restart local v16    # "value":Ljava/lang/String;
    goto/16 :goto_1

    .line 365
    .end local v16    # "value":Ljava/lang/String;
    :pswitch_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/poi/hssf/converter/ExcelToHtmlConverter;->_formatter:Lorg/apache/poi/hssf/usermodel/HSSFDataFormatter;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/usermodel/HSSFDataFormatter;->formatCellValue(Lorg/apache/poi/ss/usermodel/Cell;)Ljava/lang/String;

    move-result-object v16

    .line 366
    .restart local v16    # "value":Ljava/lang/String;
    goto/16 :goto_1

    .line 368
    .end local v16    # "value":Ljava/lang/String;
    :pswitch_9
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hssf/usermodel/HSSFCell;->getBooleanCellValue()Z

    move-result v19

    invoke-static/range {v19 .. v19}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v16

    .line 369
    .restart local v16    # "value":Ljava/lang/String;
    goto/16 :goto_1

    .line 371
    .end local v16    # "value":Ljava/lang/String;
    :pswitch_a
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hssf/usermodel/HSSFCell;->getErrorCellValue()B

    move-result v19

    invoke-static/range {v19 .. v19}, Lorg/apache/poi/ss/formula/eval/ErrorEval;->getText(I)Ljava/lang/String;

    move-result-object v16

    .line 372
    .restart local v16    # "value":Ljava/lang/String;
    goto/16 :goto_1

    .line 380
    .restart local v11    # "noText":Z
    :cond_7
    const/16 v18, 0x0

    goto/16 :goto_2

    .line 395
    .restart local v7    # "cellStyleIndex":S
    .restart local v10    # "mainCssClass":Ljava/lang/String;
    .restart local v17    # "workbook":Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;
    .restart local v18    # "wrapInDivs":Z
    :cond_8
    const-string/jumbo v19, "class"

    move-object/from16 v0, p2

    move-object/from16 v1, v19

    invoke-interface {v0, v1, v10}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3

    .line 414
    .end local v10    # "mainCssClass":Ljava/lang/String;
    .end local v17    # "workbook":Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;
    .restart local v4    # "builder":Ljava/lang/StringBuilder;
    .restart local v5    # "c":I
    :cond_9
    move-object/from16 v0, v16

    invoke-virtual {v0, v5}, Ljava/lang/String;->charAt(I)C

    move-result v19

    const/16 v20, 0x20

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_1

    .line 416
    const/16 v19, 0xa0

    move/from16 v0, v19

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 412
    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_4

    .line 457
    .end local v4    # "builder":Ljava/lang/StringBuilder;
    .end local v5    # "c":I
    .restart local v15    # "text":Lorg/w3c/dom/Text;
    :cond_a
    move-object/from16 v0, p2

    invoke-interface {v0, v15}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    goto/16 :goto_5

    .line 460
    :cond_b
    const/16 v19, 0x0

    goto/16 :goto_0

    .line 313
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_8
        :pswitch_0
        :pswitch_1
        :pswitch_7
        :pswitch_9
        :pswitch_a
    .end packed-switch

    .line 320
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_2
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method protected processColumnHeaders(Lorg/apache/poi/hssf/usermodel/HSSFSheet;ILorg/w3c/dom/Element;)V
    .locals 6
    .param p1, "sheet"    # Lorg/apache/poi/hssf/usermodel/HSSFSheet;
    .param p2, "maxSheetColumns"    # I
    .param p3, "table"    # Lorg/w3c/dom/Element;

    .prologue
    .line 466
    iget-object v5, p0, Lorg/apache/poi/hssf/converter/ExcelToHtmlConverter;->htmlDocumentFacade:Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;

    invoke-virtual {v5}, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->createTableHeader()Lorg/w3c/dom/Element;

    move-result-object v1

    .line 467
    .local v1, "tableHeader":Lorg/w3c/dom/Element;
    invoke-interface {p3, v1}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 469
    iget-object v5, p0, Lorg/apache/poi/hssf/converter/ExcelToHtmlConverter;->htmlDocumentFacade:Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;

    invoke-virtual {v5}, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->createTableRow()Lorg/w3c/dom/Element;

    move-result-object v4

    .line 471
    .local v4, "tr":Lorg/w3c/dom/Element;
    invoke-virtual {p0}, Lorg/apache/poi/hssf/converter/ExcelToHtmlConverter;->isOutputRowNumbers()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 474
    iget-object v5, p0, Lorg/apache/poi/hssf/converter/ExcelToHtmlConverter;->htmlDocumentFacade:Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;

    invoke-virtual {v5}, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->createTableHeaderCell()Lorg/w3c/dom/Element;

    move-result-object v5

    invoke-interface {v4, v5}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 477
    :cond_0
    const/4 v0, 0x0

    .local v0, "c":I
    :goto_0
    if-lt v0, p2, :cond_1

    .line 487
    invoke-interface {v1, v4}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 488
    return-void

    .line 479
    :cond_1
    invoke-virtual {p0}, Lorg/apache/poi/hssf/converter/ExcelToHtmlConverter;->isOutputHiddenColumns()Z

    move-result v5

    if-nez v5, :cond_2

    invoke-virtual {p1, v0}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->isColumnHidden(I)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 477
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 482
    :cond_2
    iget-object v5, p0, Lorg/apache/poi/hssf/converter/ExcelToHtmlConverter;->htmlDocumentFacade:Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;

    invoke-virtual {v5}, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->createTableHeaderCell()Lorg/w3c/dom/Element;

    move-result-object v3

    .line 483
    .local v3, "th":Lorg/w3c/dom/Element;
    invoke-virtual {p0, v0}, Lorg/apache/poi/hssf/converter/ExcelToHtmlConverter;->getColumnName(I)Ljava/lang/String;

    move-result-object v2

    .line 484
    .local v2, "text":Ljava/lang/String;
    iget-object v5, p0, Lorg/apache/poi/hssf/converter/ExcelToHtmlConverter;->htmlDocumentFacade:Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;

    invoke-virtual {v5, v2}, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->createText(Ljava/lang/String;)Lorg/w3c/dom/Text;

    move-result-object v5

    invoke-interface {v3, v5}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 485
    invoke-interface {v4, v3}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    goto :goto_1
.end method

.method protected processColumnWidths(Lorg/apache/poi/hssf/usermodel/HSSFSheet;ILorg/w3c/dom/Element;)V
    .locals 5
    .param p1, "sheet"    # Lorg/apache/poi/hssf/usermodel/HSSFSheet;
    .param p2, "maxSheetColumns"    # I
    .param p3, "table"    # Lorg/w3c/dom/Element;

    .prologue
    .line 498
    iget-object v3, p0, Lorg/apache/poi/hssf/converter/ExcelToHtmlConverter;->htmlDocumentFacade:Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;

    invoke-virtual {v3}, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->createTableColumnGroup()Lorg/w3c/dom/Element;

    move-result-object v2

    .line 499
    .local v2, "columnGroup":Lorg/w3c/dom/Element;
    invoke-virtual {p0}, Lorg/apache/poi/hssf/converter/ExcelToHtmlConverter;->isOutputRowNumbers()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 501
    iget-object v3, p0, Lorg/apache/poi/hssf/converter/ExcelToHtmlConverter;->htmlDocumentFacade:Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;

    invoke-virtual {v3}, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->createTableColumn()Lorg/w3c/dom/Element;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 503
    :cond_0
    const/4 v0, 0x0

    .local v0, "c":I
    :goto_0
    if-lt v0, p2, :cond_1

    .line 513
    invoke-interface {p3, v2}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 514
    return-void

    .line 505
    :cond_1
    invoke-virtual {p0}, Lorg/apache/poi/hssf/converter/ExcelToHtmlConverter;->isOutputHiddenColumns()Z

    move-result v3

    if-nez v3, :cond_2

    invoke-virtual {p1, v0}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->isColumnHidden(I)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 503
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 508
    :cond_2
    iget-object v3, p0, Lorg/apache/poi/hssf/converter/ExcelToHtmlConverter;->htmlDocumentFacade:Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;

    invoke-virtual {v3}, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->createTableColumn()Lorg/w3c/dom/Element;

    move-result-object v1

    .line 509
    .local v1, "col":Lorg/w3c/dom/Element;
    const-string/jumbo v3, "width"

    .line 510
    invoke-static {p1, v0}, Lorg/apache/poi/hssf/converter/ExcelToHtmlConverter;->getColumnWidth(Lorg/apache/poi/hssf/usermodel/HSSFSheet;I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    .line 509
    invoke-interface {v1, v3, v4}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 511
    invoke-interface {v2, v1}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    goto :goto_1
.end method

.method protected processDocumentInformation(Lorg/apache/poi/hpsf/SummaryInformation;)V
    .locals 2
    .param p1, "summaryInformation"    # Lorg/apache/poi/hpsf/SummaryInformation;

    .prologue
    .line 519
    invoke-virtual {p1}, Lorg/apache/poi/hpsf/SummaryInformation;->getTitle()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/poi/hssf/converter/ExcelToHtmlUtils;->isNotEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 520
    iget-object v0, p0, Lorg/apache/poi/hssf/converter/ExcelToHtmlConverter;->htmlDocumentFacade:Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;

    invoke-virtual {p1}, Lorg/apache/poi/hpsf/SummaryInformation;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->setTitle(Ljava/lang/String;)V

    .line 522
    :cond_0
    invoke-virtual {p1}, Lorg/apache/poi/hpsf/SummaryInformation;->getAuthor()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/poi/hssf/converter/ExcelToHtmlUtils;->isNotEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 523
    iget-object v0, p0, Lorg/apache/poi/hssf/converter/ExcelToHtmlConverter;->htmlDocumentFacade:Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;

    invoke-virtual {p1}, Lorg/apache/poi/hpsf/SummaryInformation;->getAuthor()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->addAuthor(Ljava/lang/String;)V

    .line 525
    :cond_1
    invoke-virtual {p1}, Lorg/apache/poi/hpsf/SummaryInformation;->getKeywords()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/poi/hssf/converter/ExcelToHtmlUtils;->isNotEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 526
    iget-object v0, p0, Lorg/apache/poi/hssf/converter/ExcelToHtmlConverter;->htmlDocumentFacade:Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;

    invoke-virtual {p1}, Lorg/apache/poi/hpsf/SummaryInformation;->getKeywords()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->addKeywords(Ljava/lang/String;)V

    .line 528
    :cond_2
    invoke-virtual {p1}, Lorg/apache/poi/hpsf/SummaryInformation;->getComments()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/poi/hssf/converter/ExcelToHtmlUtils;->isNotEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 529
    iget-object v0, p0, Lorg/apache/poi/hssf/converter/ExcelToHtmlConverter;->htmlDocumentFacade:Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;

    .line 530
    invoke-virtual {p1}, Lorg/apache/poi/hpsf/SummaryInformation;->getComments()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->addDescription(Ljava/lang/String;)V

    .line 531
    :cond_3
    return-void
.end method

.method protected processRow([[Lorg/apache/poi/ss/util/CellRangeAddress;Lorg/apache/poi/hssf/usermodel/HSSFRow;Lorg/w3c/dom/Element;)I
    .locals 20
    .param p1, "mergedRanges"    # [[Lorg/apache/poi/ss/util/CellRangeAddress;
    .param p2, "row"    # Lorg/apache/poi/hssf/usermodel/HSSFRow;
    .param p3, "tableRowElement"    # Lorg/w3c/dom/Element;

    .prologue
    .line 539
    invoke-virtual/range {p2 .. p2}, Lorg/apache/poi/hssf/usermodel/HSSFRow;->getSheet()Lorg/apache/poi/hssf/usermodel/HSSFSheet;

    move-result-object v18

    .line 540
    .local v18, "sheet":Lorg/apache/poi/hssf/usermodel/HSSFSheet;
    invoke-virtual/range {p2 .. p2}, Lorg/apache/poi/hssf/usermodel/HSSFRow;->getLastCellNum()S

    move-result v14

    .line 541
    .local v14, "maxColIx":S
    if-gtz v14, :cond_0

    .line 542
    const/4 v3, 0x0

    .line 641
    :goto_0
    return v3

    .line 544
    :cond_0
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12, v14}, Ljava/util/ArrayList;-><init>(I)V

    .line 546
    .local v12, "emptyCells":Ljava/util/List;, "Ljava/util/List<Lorg/w3c/dom/Element;>;"
    invoke-virtual/range {p0 .. p0}, Lorg/apache/poi/hssf/converter/ExcelToHtmlConverter;->isOutputRowNumbers()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 548
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/poi/hssf/converter/ExcelToHtmlConverter;->htmlDocumentFacade:Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;

    .line 549
    invoke-virtual {v3}, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->createTableHeaderCell()Lorg/w3c/dom/Element;

    move-result-object v19

    .line 550
    .local v19, "tableRowNumberCellElement":Lorg/w3c/dom/Element;
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Lorg/apache/poi/hssf/converter/ExcelToHtmlConverter;->processRowNumber(Lorg/apache/poi/hssf/usermodel/HSSFRow;Lorg/w3c/dom/Element;)V

    .line 551
    move-object/from16 v0, v19

    invoke-interface {v12, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 554
    .end local v19    # "tableRowNumberCellElement":Lorg/w3c/dom/Element;
    :cond_1
    const/4 v15, 0x0

    .line 555
    .local v15, "maxRenderedColumn":I
    const/4 v9, 0x0

    .local v9, "colIx":I
    :goto_1
    if-lt v9, v14, :cond_2

    .line 641
    add-int/lit8 v3, v15, 0x1

    goto :goto_0

    .line 557
    :cond_2
    invoke-virtual/range {p0 .. p0}, Lorg/apache/poi/hssf/converter/ExcelToHtmlConverter;->isOutputHiddenColumns()Z

    move-result v3

    if-nez v3, :cond_4

    move-object/from16 v0, v18

    invoke-virtual {v0, v9}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->isColumnHidden(I)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 555
    :cond_3
    :goto_2
    add-int/lit8 v9, v9, 0x1

    goto :goto_1

    .line 561
    :cond_4
    invoke-virtual/range {p2 .. p2}, Lorg/apache/poi/hssf/usermodel/HSSFRow;->getRowNum()I

    move-result v3

    .line 560
    move-object/from16 v0, p1

    invoke-static {v0, v3, v9}, Lorg/apache/poi/hssf/converter/ExcelToHtmlUtils;->getMergedRange([[Lorg/apache/poi/ss/util/CellRangeAddress;II)Lorg/apache/poi/ss/util/CellRangeAddress;

    move-result-object v17

    .line 563
    .local v17, "range":Lorg/apache/poi/ss/util/CellRangeAddress;
    if-eqz v17, :cond_5

    .line 564
    invoke-virtual/range {v17 .. v17}, Lorg/apache/poi/ss/util/CellRangeAddress;->getFirstColumn()I

    move-result v3

    if-ne v3, v9, :cond_3

    invoke-virtual/range {v17 .. v17}, Lorg/apache/poi/ss/util/CellRangeAddress;->getFirstRow()I

    move-result v3

    .line 565
    invoke-virtual/range {p2 .. p2}, Lorg/apache/poi/hssf/usermodel/HSSFRow;->getRowNum()I

    move-result v6

    if-ne v3, v6, :cond_3

    .line 568
    :cond_5
    move-object/from16 v0, p2

    invoke-virtual {v0, v9}, Lorg/apache/poi/hssf/usermodel/HSSFRow;->getCell(I)Lorg/apache/poi/hssf/usermodel/HSSFCell;

    move-result-object v4

    .line 570
    .local v4, "cell":Lorg/apache/poi/hssf/usermodel/HSSFCell;
    const/4 v7, 0x0

    .line 571
    .local v7, "divWidthPx":I
    invoke-virtual/range {p0 .. p0}, Lorg/apache/poi/hssf/converter/ExcelToHtmlConverter;->isUseDivsToSpan()Z

    move-result v3

    if-eqz v3, :cond_6

    .line 573
    move-object/from16 v0, v18

    invoke-static {v0, v9}, Lorg/apache/poi/hssf/converter/ExcelToHtmlConverter;->getColumnWidth(Lorg/apache/poi/hssf/usermodel/HSSFSheet;I)I

    move-result v7

    .line 575
    const/4 v13, 0x0

    .line 576
    .local v13, "hasBreaks":Z
    add-int/lit8 v16, v9, 0x1

    .local v16, "nextColumnIndex":I
    :goto_3
    move/from16 v0, v16

    if-lt v0, v14, :cond_9

    .line 592
    :goto_4
    if-nez v13, :cond_6

    .line 593
    const v7, 0x7fffffff

    .line 596
    .end local v13    # "hasBreaks":Z
    .end local v16    # "nextColumnIndex":I
    :cond_6
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/poi/hssf/converter/ExcelToHtmlConverter;->htmlDocumentFacade:Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;

    invoke-virtual {v3}, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->createTableCell()Lorg/w3c/dom/Element;

    move-result-object v5

    .line 598
    .local v5, "tableCellElement":Lorg/w3c/dom/Element;
    if-eqz v17, :cond_8

    .line 600
    invoke-virtual/range {v17 .. v17}, Lorg/apache/poi/ss/util/CellRangeAddress;->getFirstColumn()I

    move-result v3

    invoke-virtual/range {v17 .. v17}, Lorg/apache/poi/ss/util/CellRangeAddress;->getLastColumn()I

    move-result v6

    if-eq v3, v6, :cond_7

    .line 602
    const-string/jumbo v3, "colspan"

    .line 603
    invoke-virtual/range {v17 .. v17}, Lorg/apache/poi/ss/util/CellRangeAddress;->getLastColumn()I

    move-result v6

    .line 604
    invoke-virtual/range {v17 .. v17}, Lorg/apache/poi/ss/util/CellRangeAddress;->getFirstColumn()I

    move-result v8

    .line 603
    sub-int/2addr v6, v8

    add-int/lit8 v6, v6, 0x1

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    .line 601
    invoke-interface {v5, v3, v6}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 605
    :cond_7
    invoke-virtual/range {v17 .. v17}, Lorg/apache/poi/ss/util/CellRangeAddress;->getFirstRow()I

    move-result v3

    invoke-virtual/range {v17 .. v17}, Lorg/apache/poi/ss/util/CellRangeAddress;->getLastRow()I

    move-result v6

    if-eq v3, v6, :cond_8

    .line 607
    const-string/jumbo v3, "rowspan"

    .line 608
    invoke-virtual/range {v17 .. v17}, Lorg/apache/poi/ss/util/CellRangeAddress;->getLastRow()I

    move-result v6

    .line 609
    invoke-virtual/range {v17 .. v17}, Lorg/apache/poi/ss/util/CellRangeAddress;->getFirstRow()I

    move-result v8

    .line 608
    sub-int/2addr v6, v8

    add-int/lit8 v6, v6, 0x1

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    .line 606
    invoke-interface {v5, v3, v6}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 613
    :cond_8
    if-eqz v4, :cond_c

    .line 616
    move-object/from16 v0, v18

    invoke-static {v0, v9}, Lorg/apache/poi/hssf/converter/ExcelToHtmlConverter;->getColumnWidth(Lorg/apache/poi/hssf/usermodel/HSSFSheet;I)I

    move-result v6

    .line 617
    invoke-virtual/range {p2 .. p2}, Lorg/apache/poi/hssf/usermodel/HSSFRow;->getHeight()S

    move-result v3

    int-to-float v3, v3

    const/high16 v8, 0x41a00000    # 20.0f

    div-float v8, v3, v8

    move-object/from16 v3, p0

    .line 615
    invoke-virtual/range {v3 .. v8}, Lorg/apache/poi/hssf/converter/ExcelToHtmlConverter;->processCell(Lorg/apache/poi/hssf/usermodel/HSSFCell;Lorg/w3c/dom/Element;IIF)Z

    move-result v10

    .line 624
    .local v10, "emptyCell":Z
    :goto_5
    if-eqz v10, :cond_d

    .line 626
    invoke-interface {v12, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    .line 578
    .end local v5    # "tableCellElement":Lorg/w3c/dom/Element;
    .end local v10    # "emptyCell":Z
    .restart local v13    # "hasBreaks":Z
    .restart local v16    # "nextColumnIndex":I
    :cond_9
    invoke-virtual/range {p0 .. p0}, Lorg/apache/poi/hssf/converter/ExcelToHtmlConverter;->isOutputHiddenColumns()Z

    move-result v3

    if-nez v3, :cond_a

    .line 579
    move-object/from16 v0, v18

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->isColumnHidden(I)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 576
    :goto_6
    add-int/lit8 v16, v16, 0x1

    goto/16 :goto_3

    .line 582
    :cond_a
    move-object/from16 v0, p2

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/usermodel/HSSFRow;->getCell(I)Lorg/apache/poi/hssf/usermodel/HSSFCell;

    move-result-object v3

    if-eqz v3, :cond_b

    .line 583
    move-object/from16 v0, p2

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/usermodel/HSSFRow;->getCell(I)Lorg/apache/poi/hssf/usermodel/HSSFCell;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lorg/apache/poi/hssf/converter/ExcelToHtmlConverter;->isTextEmpty(Lorg/apache/poi/hssf/usermodel/HSSFCell;)Z

    move-result v3

    if-nez v3, :cond_b

    .line 585
    const/4 v13, 0x1

    .line 586
    goto/16 :goto_4

    .line 589
    :cond_b
    move-object/from16 v0, v18

    move/from16 v1, v16

    invoke-static {v0, v1}, Lorg/apache/poi/hssf/converter/ExcelToHtmlConverter;->getColumnWidth(Lorg/apache/poi/hssf/usermodel/HSSFSheet;I)I

    move-result v3

    add-int/2addr v7, v3

    goto :goto_6

    .line 621
    .end local v13    # "hasBreaks":Z
    .end local v16    # "nextColumnIndex":I
    .restart local v5    # "tableCellElement":Lorg/w3c/dom/Element;
    :cond_c
    const/4 v10, 0x1

    .restart local v10    # "emptyCell":Z
    goto :goto_5

    .line 630
    :cond_d
    invoke-interface {v12}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_7
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-nez v6, :cond_e

    .line 634
    invoke-interface {v12}, Ljava/util/List;->clear()V

    .line 636
    move-object/from16 v0, p3

    invoke-interface {v0, v5}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 637
    move v15, v9

    goto/16 :goto_2

    .line 630
    :cond_e
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lorg/w3c/dom/Element;

    .line 632
    .local v11, "emptyCellElement":Lorg/w3c/dom/Element;
    move-object/from16 v0, p3

    invoke-interface {v0, v11}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    goto :goto_7
.end method

.method protected processRowNumber(Lorg/apache/poi/hssf/usermodel/HSSFRow;Lorg/w3c/dom/Element;)V
    .locals 3
    .param p1, "row"    # Lorg/apache/poi/hssf/usermodel/HSSFRow;
    .param p2, "tableRowNumberCellElement"    # Lorg/w3c/dom/Element;

    .prologue
    .line 647
    const-string/jumbo v1, "class"

    const-string/jumbo v2, "rownumber"

    invoke-interface {p2, v1, v2}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 648
    iget-object v1, p0, Lorg/apache/poi/hssf/converter/ExcelToHtmlConverter;->htmlDocumentFacade:Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;

    invoke-virtual {p0, p1}, Lorg/apache/poi/hssf/converter/ExcelToHtmlConverter;->getRowName(Lorg/apache/poi/hssf/usermodel/HSSFRow;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->createText(Ljava/lang/String;)Lorg/w3c/dom/Text;

    move-result-object v0

    .line 649
    .local v0, "text":Lorg/w3c/dom/Text;
    invoke-interface {p2, v0}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 650
    return-void
.end method

.method protected processSheet(Lorg/apache/poi/hssf/usermodel/HSSFSheet;)V
    .locals 18
    .param p1, "sheet"    # Lorg/apache/poi/hssf/usermodel/HSSFSheet;

    .prologue
    .line 654
    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/poi/hssf/converter/ExcelToHtmlConverter;->htmlDocumentFacade:Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;

    invoke-virtual {v13}, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->getBody()Lorg/w3c/dom/Element;

    move-result-object v13

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v13, v1}, Lorg/apache/poi/hssf/converter/ExcelToHtmlConverter;->processSheetHeader(Lorg/w3c/dom/Element;Lorg/apache/poi/hssf/usermodel/HSSFSheet;)V

    .line 656
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->getPhysicalNumberOfRows()I

    move-result v7

    .line 657
    .local v7, "physicalNumberOfRows":I
    if-gtz v7, :cond_0

    .line 720
    :goto_0
    return-void

    .line 660
    :cond_0
    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/poi/hssf/converter/ExcelToHtmlConverter;->htmlDocumentFacade:Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;

    invoke-virtual {v13}, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->createTable()Lorg/w3c/dom/Element;

    move-result-object v10

    .line 661
    .local v10, "table":Lorg/w3c/dom/Element;
    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/poi/hssf/converter/ExcelToHtmlConverter;->htmlDocumentFacade:Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;

    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/poi/hssf/converter/ExcelToHtmlConverter;->cssClassPrefixTable:Ljava/lang/String;

    .line 662
    const-string/jumbo v15, "border-collapse:collapse;border-spacing:0;"

    .line 661
    invoke-virtual {v13, v10, v14, v15}, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->addStyleClass(Lorg/w3c/dom/Element;Ljava/lang/String;Ljava/lang/String;)V

    .line 664
    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/poi/hssf/converter/ExcelToHtmlConverter;->htmlDocumentFacade:Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;

    invoke-virtual {v13}, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->createTableBody()Lorg/w3c/dom/Element;

    move-result-object v11

    .line 667
    .local v11, "tableBody":Lorg/w3c/dom/Element;
    invoke-static/range {p1 .. p1}, Lorg/apache/poi/hssf/converter/ExcelToHtmlUtils;->buildMergedRangesMap(Lorg/apache/poi/hssf/usermodel/HSSFSheet;)[[Lorg/apache/poi/ss/util/CellRangeAddress;

    move-result-object v6

    .line 669
    .local v6, "mergedRanges":[[Lorg/apache/poi/ss/util/CellRangeAddress;
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3, v7}, Ljava/util/ArrayList;-><init>(I)V

    .line 671
    .local v3, "emptyRowElements":Ljava/util/List;, "Ljava/util/List<Lorg/w3c/dom/Element;>;"
    const/4 v5, 0x1

    .line 672
    .local v5, "maxSheetColumns":I
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->getFirstRowNum()I

    move-result v8

    .local v8, "r":I
    :goto_1
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->getLastRowNum()I

    move-result v13

    if-le v8, v13, :cond_2

    .line 710
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v5, v10}, Lorg/apache/poi/hssf/converter/ExcelToHtmlConverter;->processColumnWidths(Lorg/apache/poi/hssf/usermodel/HSSFSheet;ILorg/w3c/dom/Element;)V

    .line 712
    invoke-virtual/range {p0 .. p0}, Lorg/apache/poi/hssf/converter/ExcelToHtmlConverter;->isOutputColumnHeaders()Z

    move-result v13

    if-eqz v13, :cond_1

    .line 714
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v5, v10}, Lorg/apache/poi/hssf/converter/ExcelToHtmlConverter;->processColumnHeaders(Lorg/apache/poi/hssf/usermodel/HSSFSheet;ILorg/w3c/dom/Element;)V

    .line 717
    :cond_1
    invoke-interface {v10, v11}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 719
    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/poi/hssf/converter/ExcelToHtmlConverter;->htmlDocumentFacade:Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;

    invoke-virtual {v13}, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->getBody()Lorg/w3c/dom/Element;

    move-result-object v13

    invoke-interface {v13, v10}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    goto :goto_0

    .line 674
    :cond_2
    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->getRow(I)Lorg/apache/poi/hssf/usermodel/HSSFRow;

    move-result-object v9

    .line 676
    .local v9, "row":Lorg/apache/poi/hssf/usermodel/HSSFRow;
    if-nez v9, :cond_4

    .line 672
    :cond_3
    :goto_2
    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    .line 679
    :cond_4
    invoke-virtual/range {p0 .. p0}, Lorg/apache/poi/hssf/converter/ExcelToHtmlConverter;->isOutputHiddenRows()Z

    move-result v13

    if-nez v13, :cond_5

    invoke-virtual {v9}, Lorg/apache/poi/hssf/usermodel/HSSFRow;->getZeroHeight()Z

    move-result v13

    if-nez v13, :cond_3

    .line 682
    :cond_5
    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/poi/hssf/converter/ExcelToHtmlConverter;->htmlDocumentFacade:Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;

    invoke-virtual {v13}, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->createTableRow()Lorg/w3c/dom/Element;

    move-result-object v12

    .line 683
    .local v12, "tableRowElement":Lorg/w3c/dom/Element;
    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/poi/hssf/converter/ExcelToHtmlConverter;->htmlDocumentFacade:Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;

    .line 684
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/poi/hssf/converter/ExcelToHtmlConverter;->cssClassPrefixRow:Ljava/lang/String;

    new-instance v15, Ljava/lang/StringBuilder;

    const-string/jumbo v16, "height:"

    invoke-direct/range {v15 .. v16}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Lorg/apache/poi/hssf/usermodel/HSSFRow;->getHeight()S

    move-result v16

    move/from16 v0, v16

    int-to-float v0, v0

    move/from16 v16, v0

    const/high16 v17, 0x41a00000    # 20.0f

    div-float v16, v16, v17

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v15

    .line 685
    const-string/jumbo v16, "pt;"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    .line 683
    invoke-virtual {v13, v12, v14, v15}, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->addStyleClass(Lorg/w3c/dom/Element;Ljava/lang/String;Ljava/lang/String;)V

    .line 687
    move-object/from16 v0, p0

    invoke-virtual {v0, v6, v9, v12}, Lorg/apache/poi/hssf/converter/ExcelToHtmlConverter;->processRow([[Lorg/apache/poi/ss/util/CellRangeAddress;Lorg/apache/poi/hssf/usermodel/HSSFRow;Lorg/w3c/dom/Element;)I

    move-result v4

    .line 690
    .local v4, "maxRowColumnNumber":I
    if-nez v4, :cond_6

    .line 692
    invoke-interface {v3, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 707
    :goto_3
    invoke-static {v5, v4}, Ljava/lang/Math;->max(II)I

    move-result v5

    goto :goto_2

    .line 696
    :cond_6
    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v13

    if-nez v13, :cond_7

    .line 698
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :goto_4
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-nez v14, :cond_8

    .line 702
    invoke-interface {v3}, Ljava/util/List;->clear()V

    .line 705
    :cond_7
    invoke-interface {v11, v12}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    goto :goto_3

    .line 698
    :cond_8
    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/w3c/dom/Element;

    .line 700
    .local v2, "emptyRowElement":Lorg/w3c/dom/Element;
    invoke-interface {v11, v2}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    goto :goto_4
.end method

.method protected processSheetHeader(Lorg/w3c/dom/Element;Lorg/apache/poi/hssf/usermodel/HSSFSheet;)V
    .locals 3
    .param p1, "htmlBody"    # Lorg/w3c/dom/Element;
    .param p2, "sheet"    # Lorg/apache/poi/hssf/usermodel/HSSFSheet;

    .prologue
    .line 724
    iget-object v1, p0, Lorg/apache/poi/hssf/converter/ExcelToHtmlConverter;->htmlDocumentFacade:Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;

    invoke-virtual {v1}, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->createHeader2()Lorg/w3c/dom/Element;

    move-result-object v0

    .line 725
    .local v0, "h2":Lorg/w3c/dom/Element;
    iget-object v1, p0, Lorg/apache/poi/hssf/converter/ExcelToHtmlConverter;->htmlDocumentFacade:Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;

    invoke-virtual {p2}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->getSheetName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->createText(Ljava/lang/String;)Lorg/w3c/dom/Text;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 726
    invoke-interface {p1, v0}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 727
    return-void
.end method

.method public processWorkbook(Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;)V
    .locals 6
    .param p1, "workbook"    # Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;

    .prologue
    .line 731
    .line 732
    invoke-virtual {p1}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->getSummaryInformation()Lorg/apache/poi/hpsf/SummaryInformation;

    move-result-object v2

    .line 733
    .local v2, "summaryInformation":Lorg/apache/poi/hpsf/SummaryInformation;
    if-eqz v2, :cond_0

    .line 735
    invoke-virtual {p0, v2}, Lorg/apache/poi/hssf/converter/ExcelToHtmlConverter;->processDocumentInformation(Lorg/apache/poi/hpsf/SummaryInformation;)V

    .line 738
    :cond_0
    invoke-virtual {p0}, Lorg/apache/poi/hssf/converter/ExcelToHtmlConverter;->isUseDivsToSpan()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 741
    iget-object v3, p0, Lorg/apache/poi/hssf/converter/ExcelToHtmlConverter;->htmlDocumentFacade:Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;

    .line 742
    iget-object v4, p0, Lorg/apache/poi/hssf/converter/ExcelToHtmlConverter;->cssClassPrefixCell:Ljava/lang/String;

    .line 743
    const-string/jumbo v5, "padding:0;margin:0;align:left;vertical-align:top;"

    .line 742
    invoke-virtual {v3, v4, v5}, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->getOrCreateCssClass(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 741
    iput-object v3, p0, Lorg/apache/poi/hssf/converter/ExcelToHtmlConverter;->cssClassContainerCell:Ljava/lang/String;

    .line 744
    iget-object v3, p0, Lorg/apache/poi/hssf/converter/ExcelToHtmlConverter;->htmlDocumentFacade:Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;

    .line 745
    iget-object v4, p0, Lorg/apache/poi/hssf/converter/ExcelToHtmlConverter;->cssClassPrefixDiv:Ljava/lang/String;

    const-string/jumbo v5, "position:relative;"

    .line 744
    invoke-virtual {v3, v4, v5}, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->getOrCreateCssClass(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lorg/apache/poi/hssf/converter/ExcelToHtmlConverter;->cssClassContainerDiv:Ljava/lang/String;

    .line 748
    :cond_1
    const/4 v0, 0x0

    .local v0, "s":I
    :goto_0
    invoke-virtual {p1}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->getNumberOfSheets()I

    move-result v3

    if-lt v0, v3, :cond_2

    .line 754
    iget-object v3, p0, Lorg/apache/poi/hssf/converter/ExcelToHtmlConverter;->htmlDocumentFacade:Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;

    invoke-virtual {v3}, Lorg/apache/poi/hwpf/converter/HtmlDocumentFacade;->updateStylesheet()V

    .line 755
    return-void

    .line 750
    :cond_2
    invoke-virtual {p1, v0}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->getSheetAt(I)Lorg/apache/poi/hssf/usermodel/HSSFSheet;

    move-result-object v1

    .line 751
    .local v1, "sheet":Lorg/apache/poi/hssf/usermodel/HSSFSheet;
    invoke-virtual {p0, v1}, Lorg/apache/poi/hssf/converter/ExcelToHtmlConverter;->processSheet(Lorg/apache/poi/hssf/usermodel/HSSFSheet;)V

    .line 748
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public setCssClassPrefixCell(Ljava/lang/String;)V
    .locals 0
    .param p1, "cssClassPrefixCell"    # Ljava/lang/String;

    .prologue
    .line 759
    iput-object p1, p0, Lorg/apache/poi/hssf/converter/ExcelToHtmlConverter;->cssClassPrefixCell:Ljava/lang/String;

    .line 760
    return-void
.end method

.method public setCssClassPrefixDiv(Ljava/lang/String;)V
    .locals 0
    .param p1, "cssClassPrefixDiv"    # Ljava/lang/String;

    .prologue
    .line 764
    iput-object p1, p0, Lorg/apache/poi/hssf/converter/ExcelToHtmlConverter;->cssClassPrefixDiv:Ljava/lang/String;

    .line 765
    return-void
.end method

.method public setCssClassPrefixRow(Ljava/lang/String;)V
    .locals 0
    .param p1, "cssClassPrefixRow"    # Ljava/lang/String;

    .prologue
    .line 769
    iput-object p1, p0, Lorg/apache/poi/hssf/converter/ExcelToHtmlConverter;->cssClassPrefixRow:Ljava/lang/String;

    .line 770
    return-void
.end method

.method public setCssClassPrefixTable(Ljava/lang/String;)V
    .locals 0
    .param p1, "cssClassPrefixTable"    # Ljava/lang/String;

    .prologue
    .line 774
    iput-object p1, p0, Lorg/apache/poi/hssf/converter/ExcelToHtmlConverter;->cssClassPrefixTable:Ljava/lang/String;

    .line 775
    return-void
.end method

.method public setUseDivsToSpan(Z)V
    .locals 0
    .param p1, "useDivsToSpan"    # Z

    .prologue
    .line 787
    iput-boolean p1, p0, Lorg/apache/poi/hssf/converter/ExcelToHtmlConverter;->useDivsToSpan:Z

    .line 788
    return-void
.end method
