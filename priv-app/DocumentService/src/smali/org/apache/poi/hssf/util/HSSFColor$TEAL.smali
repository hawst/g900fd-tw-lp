.class public final Lorg/apache/poi/hssf/util/HSSFColor$TEAL;
.super Lorg/apache/poi/hssf/util/HSSFColor;
.source "HSSFColor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/poi/hssf/util/HSSFColor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "TEAL"
.end annotation


# static fields
.field public static final hexString:Ljava/lang/String; = "0:8080:8080"

.field public static final index:S = 0x15s

.field public static final index2:S = 0x26s

.field public static final triplet:[S


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/16 v2, 0x80

    .line 596
    const/4 v0, 0x3

    new-array v0, v0, [S

    const/4 v1, 0x1

    .line 597
    aput-short v2, v0, v1

    const/4 v1, 0x2

    aput-short v2, v0, v1

    .line 595
    sput-object v0, Lorg/apache/poi/hssf/util/HSSFColor$TEAL;->triplet:[S

    .line 599
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 590
    invoke-direct {p0}, Lorg/apache/poi/hssf/util/HSSFColor;-><init>()V

    return-void
.end method


# virtual methods
.method public getHexString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 613
    const-string/jumbo v0, "0:8080:8080"

    return-object v0
.end method

.method public getIndex()S
    .locals 1

    .prologue
    .line 603
    const/16 v0, 0x15

    return v0
.end method

.method public getTriplet()[S
    .locals 1

    .prologue
    .line 608
    sget-object v0, Lorg/apache/poi/hssf/util/HSSFColor$TEAL;->triplet:[S

    return-object v0
.end method
