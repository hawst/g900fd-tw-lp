.class public final Lorg/apache/poi/hssf/record/aggregates/ChartSubstreamRecordAggregate;
.super Lorg/apache/poi/hssf/record/aggregates/RecordAggregate;
.source "ChartSubstreamRecordAggregate.java"


# instance fields
.field private final _bofRec:Lorg/apache/poi/hssf/record/BOFRecord;

.field private _linkedDataRecList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lorg/apache/poi/hssf/record/chart/LinkedDataRecord;",
            ">;"
        }
    .end annotation
.end field

.field private _psBlock:Lorg/apache/poi/hssf/record/aggregates/PageSettingsBlock;

.field private final _recs:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/apache/poi/hssf/record/RecordBase;",
            ">;"
        }
    .end annotation
.end field

.field private cellRangeAddressBases:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lorg/apache/poi/ss/util/CellRangeAddressBase;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lorg/apache/poi/hssf/model/RecordStream;)V
    .locals 5
    .param p1, "rs"    # Lorg/apache/poi/hssf/model/RecordStream;

    .prologue
    const/4 v3, 0x0

    .line 57
    invoke-direct {p0}, Lorg/apache/poi/hssf/record/aggregates/RecordAggregate;-><init>()V

    .line 50
    iput-object v3, p0, Lorg/apache/poi/hssf/record/aggregates/ChartSubstreamRecordAggregate;->_linkedDataRecList:Ljava/util/ArrayList;

    .line 51
    iput-object v3, p0, Lorg/apache/poi/hssf/record/aggregates/ChartSubstreamRecordAggregate;->cellRangeAddressBases:Ljava/util/ArrayList;

    .line 58
    invoke-virtual {p1}, Lorg/apache/poi/hssf/model/RecordStream;->getNext()Lorg/apache/poi/hssf/record/Record;

    move-result-object v3

    check-cast v3, Lorg/apache/poi/hssf/record/BOFRecord;

    iput-object v3, p0, Lorg/apache/poi/hssf/record/aggregates/ChartSubstreamRecordAggregate;->_bofRec:Lorg/apache/poi/hssf/record/BOFRecord;

    .line 59
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 60
    .local v2, "temp":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/hssf/record/RecordBase;>;"
    :goto_0
    invoke-virtual {p1}, Lorg/apache/poi/hssf/model/RecordStream;->peekNextClass()Ljava/lang/Class;

    move-result-object v3

    const-class v4, Lorg/apache/poi/hssf/record/EOFRecord;

    if-ne v3, v4, :cond_0

    .line 85
    iput-object v2, p0, Lorg/apache/poi/hssf/record/aggregates/ChartSubstreamRecordAggregate;->_recs:Ljava/util/List;

    .line 86
    invoke-virtual {p1}, Lorg/apache/poi/hssf/model/RecordStream;->getNext()Lorg/apache/poi/hssf/record/Record;

    move-result-object v0

    .line 87
    .local v0, "eof":Lorg/apache/poi/hssf/record/Record;
    instance-of v3, v0, Lorg/apache/poi/hssf/record/EOFRecord;

    if-nez v3, :cond_6

    .line 88
    new-instance v3, Ljava/lang/IllegalStateException;

    const-string/jumbo v4, "Bad chart EOF"

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 61
    .end local v0    # "eof":Lorg/apache/poi/hssf/record/Record;
    :cond_0
    invoke-virtual {p1}, Lorg/apache/poi/hssf/model/RecordStream;->getNext()Lorg/apache/poi/hssf/record/Record;

    move-result-object v1

    .line 62
    .local v1, "r":Lorg/apache/poi/hssf/record/Record;
    invoke-virtual {v1}, Lorg/apache/poi/hssf/record/Record;->getSid()S

    move-result v3

    const/16 v4, 0x1051

    if-ne v3, v4, :cond_2

    .line 63
    iget-object v3, p0, Lorg/apache/poi/hssf/record/aggregates/ChartSubstreamRecordAggregate;->_linkedDataRecList:Ljava/util/ArrayList;

    if-nez v3, :cond_1

    .line 64
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lorg/apache/poi/hssf/record/aggregates/ChartSubstreamRecordAggregate;->_linkedDataRecList:Ljava/util/ArrayList;

    .line 66
    :cond_1
    iget-object v4, p0, Lorg/apache/poi/hssf/record/aggregates/ChartSubstreamRecordAggregate;->_linkedDataRecList:Ljava/util/ArrayList;

    move-object v3, v1

    check-cast v3, Lorg/apache/poi/hssf/record/chart/LinkedDataRecord;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 68
    :cond_2
    invoke-virtual {p1}, Lorg/apache/poi/hssf/model/RecordStream;->peekNextSid()I

    move-result v3

    invoke-static {v3}, Lorg/apache/poi/hssf/record/aggregates/PageSettingsBlock;->isComponentRecord(I)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 69
    iget-object v3, p0, Lorg/apache/poi/hssf/record/aggregates/ChartSubstreamRecordAggregate;->_psBlock:Lorg/apache/poi/hssf/record/aggregates/PageSettingsBlock;

    if-eqz v3, :cond_4

    .line 70
    invoke-virtual {p1}, Lorg/apache/poi/hssf/model/RecordStream;->peekNextSid()I

    move-result v3

    const/16 v4, 0x89c

    if-ne v3, v4, :cond_3

    .line 73
    iget-object v3, p0, Lorg/apache/poi/hssf/record/aggregates/ChartSubstreamRecordAggregate;->_psBlock:Lorg/apache/poi/hssf/record/aggregates/PageSettingsBlock;

    check-cast v1, Lorg/apache/poi/hssf/record/HeaderFooterRecord;

    .end local v1    # "r":Lorg/apache/poi/hssf/record/Record;
    invoke-virtual {v3, v1}, Lorg/apache/poi/hssf/record/aggregates/PageSettingsBlock;->addLateHeaderFooter(Lorg/apache/poi/hssf/record/HeaderFooterRecord;)V

    goto :goto_0

    .line 76
    .restart local v1    # "r":Lorg/apache/poi/hssf/record/Record;
    :cond_3
    new-instance v3, Ljava/lang/IllegalStateException;

    .line 77
    const-string/jumbo v4, "Found more than one PageSettingsBlock in chart sub-stream"

    .line 76
    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 79
    :cond_4
    new-instance v3, Lorg/apache/poi/hssf/record/aggregates/PageSettingsBlock;

    invoke-direct {v3, p1}, Lorg/apache/poi/hssf/record/aggregates/PageSettingsBlock;-><init>(Lorg/apache/poi/hssf/model/RecordStream;)V

    iput-object v3, p0, Lorg/apache/poi/hssf/record/aggregates/ChartSubstreamRecordAggregate;->_psBlock:Lorg/apache/poi/hssf/record/aggregates/PageSettingsBlock;

    .line 80
    iget-object v3, p0, Lorg/apache/poi/hssf/record/aggregates/ChartSubstreamRecordAggregate;->_psBlock:Lorg/apache/poi/hssf/record/aggregates/PageSettingsBlock;

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 83
    :cond_5
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 92
    .end local v1    # "r":Lorg/apache/poi/hssf/record/Record;
    .restart local v0    # "eof":Lorg/apache/poi/hssf/record/Record;
    :cond_6
    :try_start_0
    iget-object v3, p0, Lorg/apache/poi/hssf/record/aggregates/ChartSubstreamRecordAggregate;->_linkedDataRecList:Ljava/util/ArrayList;

    invoke-virtual {p0, v3}, Lorg/apache/poi/hssf/record/aggregates/ChartSubstreamRecordAggregate;->getChartDataCellRangeAdd(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v3

    iput-object v3, p0, Lorg/apache/poi/hssf/record/aggregates/ChartSubstreamRecordAggregate;->cellRangeAddressBases:Ljava/util/ArrayList;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 95
    :goto_1
    return-void

    .line 93
    :catch_0
    move-exception v3

    goto :goto_1
.end method


# virtual methods
.method public checkDuplicates(Ljava/util/ArrayList;IIII)Z
    .locals 8
    .param p2, "firstRow"    # I
    .param p3, "lastRow"    # I
    .param p4, "firstCol"    # I
    .param p5, "lastCol"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lorg/apache/poi/ss/util/CellRangeAddressBase;",
            ">;IIII)Z"
        }
    .end annotation

    .prologue
    .local p1, "cellRangeAddressBases":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/poi/ss/util/CellRangeAddressBase;>;"
    const/4 v6, 0x1

    .line 143
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-nez v7, :cond_1

    .line 158
    :cond_0
    :goto_0
    return v6

    .line 146
    :cond_1
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v4

    .line 148
    .local v4, "size":I
    add-int/lit8 v7, v4, -0x1

    invoke-virtual {p1, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/apache/poi/ss/util/CellRangeAddressBase;

    .line 149
    .local v5, "tempCellRange":Lorg/apache/poi/ss/util/CellRangeAddressBase;
    invoke-virtual {v5}, Lorg/apache/poi/ss/util/CellRangeAddressBase;->getFirstRow()I

    move-result v1

    .line 150
    .local v1, "prevFirstRow":I
    invoke-virtual {v5}, Lorg/apache/poi/ss/util/CellRangeAddressBase;->getLastRow()I

    move-result v3

    .line 151
    .local v3, "prevLastRow":I
    invoke-virtual {v5}, Lorg/apache/poi/ss/util/CellRangeAddressBase;->getFirstColumn()I

    move-result v0

    .line 152
    .local v0, "prevFirstCol":I
    invoke-virtual {v5}, Lorg/apache/poi/ss/util/CellRangeAddressBase;->getLastColumn()I

    move-result v2

    .line 154
    .local v2, "prevLastCol":I
    if-ne v1, p2, :cond_0

    if-ne v0, p4, :cond_0

    .line 155
    if-ne v3, p3, :cond_0

    if-ne v2, p5, :cond_0

    .line 158
    const/4 v6, 0x0

    goto :goto_0
.end method

.method public getCellRangeAddressBases()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lorg/apache/poi/ss/util/CellRangeAddressBase;",
            ">;"
        }
    .end annotation

    .prologue
    .line 54
    iget-object v0, p0, Lorg/apache/poi/hssf/record/aggregates/ChartSubstreamRecordAggregate;->cellRangeAddressBases:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getChartDataCellRangeAdd(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lorg/apache/poi/hssf/record/chart/LinkedDataRecord;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Lorg/apache/poi/ss/util/CellRangeAddressBase;",
            ">;"
        }
    .end annotation

    .prologue
    .line 99
    .local p1, "_linkedDataRecList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/poi/hssf/record/chart/LinkedDataRecord;>;"
    if-nez p1, :cond_0

    .line 100
    const/4 v0, 0x0

    .line 137
    :goto_0
    return-object v0

    .line 103
    :cond_0
    const/4 v2, 0x0

    .line 104
    .local v2, "firstRow":I
    const/4 v3, 0x0

    .line 105
    .local v3, "lastRow":I
    const/4 v4, 0x0

    .line 106
    .local v4, "firstCol":I
    const/4 v5, 0x0

    .line 108
    .local v5, "lastCol":I
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .line 110
    .local v7, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/poi/hssf/record/chart/LinkedDataRecord;>;"
    :cond_1
    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_2

    .line 137
    iget-object v0, p0, Lorg/apache/poi/hssf/record/aggregates/ChartSubstreamRecordAggregate;->cellRangeAddressBases:Ljava/util/ArrayList;

    goto :goto_0

    .line 111
    :cond_2
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hssf/record/chart/LinkedDataRecord;

    invoke-virtual {v0}, Lorg/apache/poi/hssf/record/chart/LinkedDataRecord;->getFormulaOfLink()[Lorg/apache/poi/ss/formula/ptg/Ptg;

    move-result-object v1

    array-length v9, v1

    const/4 v0, 0x0

    :goto_2
    if-lt v0, v9, :cond_5

    .line 123
    iget-object v0, p0, Lorg/apache/poi/hssf/record/aggregates/ChartSubstreamRecordAggregate;->cellRangeAddressBases:Ljava/util/ArrayList;

    if-nez v0, :cond_4

    .line 124
    if-nez v2, :cond_3

    if-nez v4, :cond_3

    if-nez v3, :cond_3

    if-eqz v5, :cond_4

    .line 125
    :cond_3
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/hssf/record/aggregates/ChartSubstreamRecordAggregate;->cellRangeAddressBases:Ljava/util/ArrayList;

    .line 127
    :cond_4
    iget-object v0, p0, Lorg/apache/poi/hssf/record/aggregates/ChartSubstreamRecordAggregate;->cellRangeAddressBases:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    .line 130
    iget-object v1, p0, Lorg/apache/poi/hssf/record/aggregates/ChartSubstreamRecordAggregate;->cellRangeAddressBases:Ljava/util/ArrayList;

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lorg/apache/poi/hssf/record/aggregates/ChartSubstreamRecordAggregate;->checkDuplicates(Ljava/util/ArrayList;IIII)Z

    move-result v0

    .line 131
    if-eqz v0, :cond_1

    .line 132
    iget-object v0, p0, Lorg/apache/poi/hssf/record/aggregates/ChartSubstreamRecordAggregate;->cellRangeAddressBases:Ljava/util/ArrayList;

    new-instance v1, Lorg/apache/poi/ss/util/CellRangeAddress;

    .line 133
    invoke-direct {v1, v2, v3, v4, v5}, Lorg/apache/poi/ss/util/CellRangeAddress;-><init>(IIII)V

    .line 132
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 111
    :cond_5
    aget-object v8, v1, v0

    .line 112
    .local v8, "ptg":Lorg/apache/poi/ss/formula/ptg/Ptg;
    instance-of v10, v8, Lorg/apache/poi/ss/formula/ptg/AreaPtgBase;

    if-eqz v10, :cond_6

    move-object v6, v8

    .line 113
    check-cast v6, Lorg/apache/poi/ss/formula/ptg/AreaPtgBase;

    .line 115
    .local v6, "areaPtg":Lorg/apache/poi/ss/formula/ptg/AreaPtgBase;
    invoke-virtual {v6}, Lorg/apache/poi/ss/formula/ptg/AreaPtgBase;->getFirstRow()I

    move-result v2

    .line 116
    invoke-virtual {v6}, Lorg/apache/poi/ss/formula/ptg/AreaPtgBase;->getLastRow()I

    move-result v3

    .line 118
    invoke-virtual {v6}, Lorg/apache/poi/ss/formula/ptg/AreaPtgBase;->getFirstColumn()I

    move-result v4

    .line 119
    invoke-virtual {v6}, Lorg/apache/poi/ss/formula/ptg/AreaPtgBase;->getLastColumn()I

    move-result v5

    .line 111
    .end local v6    # "areaPtg":Lorg/apache/poi/ss/formula/ptg/AreaPtgBase;
    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_2
.end method

.method public visitContainedRecords(Lorg/apache/poi/hssf/record/aggregates/RecordAggregate$RecordVisitor;)V
    .locals 3
    .param p1, "rv"    # Lorg/apache/poi/hssf/record/aggregates/RecordAggregate$RecordVisitor;

    .prologue
    .line 163
    iget-object v2, p0, Lorg/apache/poi/hssf/record/aggregates/ChartSubstreamRecordAggregate;->_recs:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 176
    :goto_0
    return-void

    .line 166
    :cond_0
    iget-object v2, p0, Lorg/apache/poi/hssf/record/aggregates/ChartSubstreamRecordAggregate;->_bofRec:Lorg/apache/poi/hssf/record/BOFRecord;

    invoke-interface {p1, v2}, Lorg/apache/poi/hssf/record/aggregates/RecordAggregate$RecordVisitor;->visitRecord(Lorg/apache/poi/hssf/record/Record;)V

    .line 167
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget-object v2, p0, Lorg/apache/poi/hssf/record/aggregates/ChartSubstreamRecordAggregate;->_recs:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lt v0, v2, :cond_1

    .line 175
    sget-object v2, Lorg/apache/poi/hssf/record/EOFRecord;->instance:Lorg/apache/poi/hssf/record/EOFRecord;

    invoke-interface {p1, v2}, Lorg/apache/poi/hssf/record/aggregates/RecordAggregate$RecordVisitor;->visitRecord(Lorg/apache/poi/hssf/record/Record;)V

    goto :goto_0

    .line 168
    :cond_1
    iget-object v2, p0, Lorg/apache/poi/hssf/record/aggregates/ChartSubstreamRecordAggregate;->_recs:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/hssf/record/RecordBase;

    .line 169
    .local v1, "rb":Lorg/apache/poi/hssf/record/RecordBase;
    instance-of v2, v1, Lorg/apache/poi/hssf/record/aggregates/RecordAggregate;

    if-eqz v2, :cond_2

    .line 170
    check-cast v1, Lorg/apache/poi/hssf/record/aggregates/RecordAggregate;

    .end local v1    # "rb":Lorg/apache/poi/hssf/record/RecordBase;
    invoke-virtual {v1, p1}, Lorg/apache/poi/hssf/record/aggregates/RecordAggregate;->visitContainedRecords(Lorg/apache/poi/hssf/record/aggregates/RecordAggregate$RecordVisitor;)V

    .line 167
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 172
    .restart local v1    # "rb":Lorg/apache/poi/hssf/record/RecordBase;
    :cond_2
    check-cast v1, Lorg/apache/poi/hssf/record/Record;

    .end local v1    # "rb":Lorg/apache/poi/hssf/record/RecordBase;
    invoke-interface {p1, v1}, Lorg/apache/poi/hssf/record/aggregates/RecordAggregate$RecordVisitor;->visitRecord(Lorg/apache/poi/hssf/record/Record;)V

    goto :goto_2
.end method
