.class public final Lorg/apache/poi/hssf/record/RecordFactoryInputStream;
.super Ljava/lang/Object;
.source "RecordFactoryInputStream.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/poi/hssf/record/RecordFactoryInputStream$StreamEncryptionInfo;
    }
.end annotation


# instance fields
.field private _bofDepth:I

.field private _lastDrawingRecord:Lorg/apache/poi/hssf/record/DrawingRecord;

.field private _lastRecord:Lorg/apache/poi/hssf/record/Record;

.field private _lastRecordWasEOFLevelZero:Z

.field private final _recStream:Lorg/apache/poi/hssf/record/RecordInputStream;

.field private final _shouldIncludeContinueRecords:Z

.field private _unreadRecordBuffer:[Lorg/apache/poi/hssf/record/Record;

.field private _unreadRecordIndex:I


# direct methods
.method public constructor <init>(Ljava/io/InputStream;Z)V
    .locals 5
    .param p1, "in"    # Ljava/io/InputStream;
    .param p2, "shouldIncludeContinueRecords"    # Z

    .prologue
    const/4 v4, 0x0

    .line 179
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 158
    const/4 v3, -0x1

    iput v3, p0, Lorg/apache/poi/hssf/record/RecordFactoryInputStream;->_unreadRecordIndex:I

    .line 163
    const/4 v3, 0x0

    iput-object v3, p0, Lorg/apache/poi/hssf/record/RecordFactoryInputStream;->_lastRecord:Lorg/apache/poi/hssf/record/Record;

    .line 167
    new-instance v3, Lorg/apache/poi/hssf/record/DrawingRecord;

    invoke-direct {v3}, Lorg/apache/poi/hssf/record/DrawingRecord;-><init>()V

    iput-object v3, p0, Lorg/apache/poi/hssf/record/RecordFactoryInputStream;->_lastDrawingRecord:Lorg/apache/poi/hssf/record/DrawingRecord;

    .line 180
    new-instance v1, Lorg/apache/poi/hssf/record/RecordInputStream;

    invoke-direct {v1, p1}, Lorg/apache/poi/hssf/record/RecordInputStream;-><init>(Ljava/io/InputStream;)V

    .line 181
    .local v1, "rs":Lorg/apache/poi/hssf/record/RecordInputStream;
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 182
    .local v0, "records":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/hssf/record/Record;>;"
    new-instance v2, Lorg/apache/poi/hssf/record/RecordFactoryInputStream$StreamEncryptionInfo;

    invoke-direct {v2, v1, v0}, Lorg/apache/poi/hssf/record/RecordFactoryInputStream$StreamEncryptionInfo;-><init>(Lorg/apache/poi/hssf/record/RecordInputStream;Ljava/util/List;)V

    .line 183
    .local v2, "sei":Lorg/apache/poi/hssf/record/RecordFactoryInputStream$StreamEncryptionInfo;
    invoke-virtual {v2}, Lorg/apache/poi/hssf/record/RecordFactoryInputStream$StreamEncryptionInfo;->hasEncryption()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 184
    invoke-virtual {v2, p1}, Lorg/apache/poi/hssf/record/RecordFactoryInputStream$StreamEncryptionInfo;->createDecryptingStream(Ljava/io/InputStream;)Lorg/apache/poi/hssf/record/RecordInputStream;

    move-result-object v1

    .line 189
    :cond_0
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_1

    .line 190
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    new-array v3, v3, [Lorg/apache/poi/hssf/record/Record;

    iput-object v3, p0, Lorg/apache/poi/hssf/record/RecordFactoryInputStream;->_unreadRecordBuffer:[Lorg/apache/poi/hssf/record/Record;

    .line 191
    iget-object v3, p0, Lorg/apache/poi/hssf/record/RecordFactoryInputStream;->_unreadRecordBuffer:[Lorg/apache/poi/hssf/record/Record;

    invoke-interface {v0, v3}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 192
    iput v4, p0, Lorg/apache/poi/hssf/record/RecordFactoryInputStream;->_unreadRecordIndex:I

    .line 194
    :cond_1
    iput-object v1, p0, Lorg/apache/poi/hssf/record/RecordFactoryInputStream;->_recStream:Lorg/apache/poi/hssf/record/RecordInputStream;

    .line 195
    iput-boolean p2, p0, Lorg/apache/poi/hssf/record/RecordFactoryInputStream;->_shouldIncludeContinueRecords:Z

    .line 196
    invoke-virtual {v2}, Lorg/apache/poi/hssf/record/RecordFactoryInputStream$StreamEncryptionInfo;->getLastRecord()Lorg/apache/poi/hssf/record/Record;

    move-result-object v3

    iput-object v3, p0, Lorg/apache/poi/hssf/record/RecordFactoryInputStream;->_lastRecord:Lorg/apache/poi/hssf/record/Record;

    .line 215
    invoke-virtual {v2}, Lorg/apache/poi/hssf/record/RecordFactoryInputStream$StreamEncryptionInfo;->hasBOFRecord()Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 v3, 0x1

    :goto_0
    iput v3, p0, Lorg/apache/poi/hssf/record/RecordFactoryInputStream;->_bofDepth:I

    .line 216
    iput-boolean v4, p0, Lorg/apache/poi/hssf/record/RecordFactoryInputStream;->_lastRecordWasEOFLevelZero:Z

    .line 217
    return-void

    :cond_2
    move v3, v4

    .line 215
    goto :goto_0
.end method

.method private getNextUnreadRecord()Lorg/apache/poi/hssf/record/Record;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 265
    iget-object v2, p0, Lorg/apache/poi/hssf/record/RecordFactoryInputStream;->_unreadRecordBuffer:[Lorg/apache/poi/hssf/record/Record;

    if-eqz v2, :cond_0

    .line 266
    iget v0, p0, Lorg/apache/poi/hssf/record/RecordFactoryInputStream;->_unreadRecordIndex:I

    .line 267
    .local v0, "ix":I
    iget-object v2, p0, Lorg/apache/poi/hssf/record/RecordFactoryInputStream;->_unreadRecordBuffer:[Lorg/apache/poi/hssf/record/Record;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 268
    iget-object v2, p0, Lorg/apache/poi/hssf/record/RecordFactoryInputStream;->_unreadRecordBuffer:[Lorg/apache/poi/hssf/record/Record;

    aget-object v1, v2, v0

    .line 269
    .local v1, "result":Lorg/apache/poi/hssf/record/Record;
    add-int/lit8 v2, v0, 0x1

    iput v2, p0, Lorg/apache/poi/hssf/record/RecordFactoryInputStream;->_unreadRecordIndex:I

    .line 275
    .end local v0    # "ix":I
    .end local v1    # "result":Lorg/apache/poi/hssf/record/Record;
    :cond_0
    :goto_0
    return-object v1

    .line 272
    .restart local v0    # "ix":I
    :cond_1
    const/4 v2, -0x1

    iput v2, p0, Lorg/apache/poi/hssf/record/RecordFactoryInputStream;->_unreadRecordIndex:I

    .line 273
    iput-object v1, p0, Lorg/apache/poi/hssf/record/RecordFactoryInputStream;->_unreadRecordBuffer:[Lorg/apache/poi/hssf/record/Record;

    goto :goto_0
.end method

.method private readNextRecord()Lorg/apache/poi/hssf/record/Record;
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 285
    iget-object v4, p0, Lorg/apache/poi/hssf/record/RecordFactoryInputStream;->_recStream:Lorg/apache/poi/hssf/record/RecordInputStream;

    invoke-static {v4}, Lorg/apache/poi/hssf/record/RecordFactory;->createSingleRecord(Lorg/apache/poi/hssf/record/RecordInputStream;)Lorg/apache/poi/hssf/record/Record;

    move-result-object v2

    .line 286
    .local v2, "record":Lorg/apache/poi/hssf/record/Record;
    iput-boolean v7, p0, Lorg/apache/poi/hssf/record/RecordFactoryInputStream;->_lastRecordWasEOFLevelZero:Z

    .line 288
    instance-of v4, v2, Lorg/apache/poi/hssf/record/BOFRecord;

    if-eqz v4, :cond_1

    .line 289
    iget v4, p0, Lorg/apache/poi/hssf/record/RecordFactoryInputStream;->_bofDepth:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lorg/apache/poi/hssf/record/RecordFactoryInputStream;->_bofDepth:I

    .line 363
    .end local v2    # "record":Lorg/apache/poi/hssf/record/Record;
    :cond_0
    :goto_0
    return-object v2

    .line 293
    .restart local v2    # "record":Lorg/apache/poi/hssf/record/Record;
    :cond_1
    instance-of v4, v2, Lorg/apache/poi/hssf/record/EOFRecord;

    if-eqz v4, :cond_2

    .line 294
    iget v4, p0, Lorg/apache/poi/hssf/record/RecordFactoryInputStream;->_bofDepth:I

    add-int/lit8 v4, v4, -0x1

    iput v4, p0, Lorg/apache/poi/hssf/record/RecordFactoryInputStream;->_bofDepth:I

    .line 295
    iget v4, p0, Lorg/apache/poi/hssf/record/RecordFactoryInputStream;->_bofDepth:I

    if-ge v4, v6, :cond_0

    .line 296
    iput-boolean v6, p0, Lorg/apache/poi/hssf/record/RecordFactoryInputStream;->_lastRecordWasEOFLevelZero:Z

    goto :goto_0

    .line 302
    :cond_2
    instance-of v4, v2, Lorg/apache/poi/hssf/record/DBCellRecord;

    if-eqz v4, :cond_3

    move-object v2, v5

    .line 304
    goto :goto_0

    .line 307
    :cond_3
    instance-of v4, v2, Lorg/apache/poi/hssf/record/RKRecord;

    if-eqz v4, :cond_4

    .line 308
    check-cast v2, Lorg/apache/poi/hssf/record/RKRecord;

    .end local v2    # "record":Lorg/apache/poi/hssf/record/Record;
    invoke-static {v2}, Lorg/apache/poi/hssf/record/RecordFactory;->convertToNumberRecord(Lorg/apache/poi/hssf/record/RKRecord;)Lorg/apache/poi/hssf/record/NumberRecord;

    move-result-object v2

    goto :goto_0

    .line 311
    .restart local v2    # "record":Lorg/apache/poi/hssf/record/Record;
    :cond_4
    instance-of v4, v2, Lorg/apache/poi/hssf/record/MulRKRecord;

    if-eqz v4, :cond_5

    .line 312
    check-cast v2, Lorg/apache/poi/hssf/record/MulRKRecord;

    .end local v2    # "record":Lorg/apache/poi/hssf/record/Record;
    invoke-static {v2}, Lorg/apache/poi/hssf/record/RecordFactory;->convertRKRecords(Lorg/apache/poi/hssf/record/MulRKRecord;)[Lorg/apache/poi/hssf/record/NumberRecord;

    move-result-object v3

    .line 314
    .local v3, "records":[Lorg/apache/poi/hssf/record/Record;
    iput-object v3, p0, Lorg/apache/poi/hssf/record/RecordFactoryInputStream;->_unreadRecordBuffer:[Lorg/apache/poi/hssf/record/Record;

    .line 315
    iput v6, p0, Lorg/apache/poi/hssf/record/RecordFactoryInputStream;->_unreadRecordIndex:I

    .line 316
    aget-object v2, v3, v7

    goto :goto_0

    .line 319
    .end local v3    # "records":[Lorg/apache/poi/hssf/record/Record;
    .restart local v2    # "record":Lorg/apache/poi/hssf/record/Record;
    :cond_5
    invoke-virtual {v2}, Lorg/apache/poi/hssf/record/Record;->getSid()S

    move-result v4

    const/16 v6, 0xeb

    if-ne v4, v6, :cond_6

    .line 320
    iget-object v4, p0, Lorg/apache/poi/hssf/record/RecordFactoryInputStream;->_lastRecord:Lorg/apache/poi/hssf/record/Record;

    instance-of v4, v4, Lorg/apache/poi/hssf/record/DrawingGroupRecord;

    if-eqz v4, :cond_6

    .line 321
    iget-object v1, p0, Lorg/apache/poi/hssf/record/RecordFactoryInputStream;->_lastRecord:Lorg/apache/poi/hssf/record/Record;

    check-cast v1, Lorg/apache/poi/hssf/record/DrawingGroupRecord;

    .line 322
    .local v1, "lastDGRecord":Lorg/apache/poi/hssf/record/DrawingGroupRecord;
    check-cast v2, Lorg/apache/poi/hssf/record/AbstractEscherHolderRecord;

    .end local v2    # "record":Lorg/apache/poi/hssf/record/Record;
    invoke-virtual {v1, v2}, Lorg/apache/poi/hssf/record/DrawingGroupRecord;->join(Lorg/apache/poi/hssf/record/AbstractEscherHolderRecord;)V

    move-object v2, v5

    .line 323
    goto :goto_0

    .line 325
    .end local v1    # "lastDGRecord":Lorg/apache/poi/hssf/record/DrawingGroupRecord;
    .restart local v2    # "record":Lorg/apache/poi/hssf/record/Record;
    :cond_6
    invoke-virtual {v2}, Lorg/apache/poi/hssf/record/Record;->getSid()S

    move-result v4

    const/16 v6, 0x3c

    if-ne v4, v6, :cond_b

    move-object v0, v2

    .line 326
    check-cast v0, Lorg/apache/poi/hssf/record/ContinueRecord;

    .line 328
    .local v0, "contRec":Lorg/apache/poi/hssf/record/ContinueRecord;
    iget-object v4, p0, Lorg/apache/poi/hssf/record/RecordFactoryInputStream;->_lastRecord:Lorg/apache/poi/hssf/record/Record;

    instance-of v4, v4, Lorg/apache/poi/hssf/record/ObjRecord;

    if-nez v4, :cond_7

    iget-object v4, p0, Lorg/apache/poi/hssf/record/RecordFactoryInputStream;->_lastRecord:Lorg/apache/poi/hssf/record/Record;

    instance-of v4, v4, Lorg/apache/poi/hssf/record/TextObjectRecord;

    if-eqz v4, :cond_8

    .line 331
    :cond_7
    iget-object v4, p0, Lorg/apache/poi/hssf/record/RecordFactoryInputStream;->_lastDrawingRecord:Lorg/apache/poi/hssf/record/DrawingRecord;

    invoke-virtual {v0}, Lorg/apache/poi/hssf/record/ContinueRecord;->getData()[B

    move-result-object v6

    invoke-virtual {v4, v6}, Lorg/apache/poi/hssf/record/DrawingRecord;->processContinueRecord([B)V

    .line 334
    iget-boolean v4, p0, Lorg/apache/poi/hssf/record/RecordFactoryInputStream;->_shouldIncludeContinueRecords:Z

    if-nez v4, :cond_0

    move-object v2, v5

    .line 337
    goto :goto_0

    .line 339
    :cond_8
    iget-object v4, p0, Lorg/apache/poi/hssf/record/RecordFactoryInputStream;->_lastRecord:Lorg/apache/poi/hssf/record/Record;

    instance-of v4, v4, Lorg/apache/poi/hssf/record/DrawingGroupRecord;

    if-eqz v4, :cond_9

    .line 340
    iget-object v4, p0, Lorg/apache/poi/hssf/record/RecordFactoryInputStream;->_lastRecord:Lorg/apache/poi/hssf/record/Record;

    check-cast v4, Lorg/apache/poi/hssf/record/DrawingGroupRecord;

    invoke-virtual {v0}, Lorg/apache/poi/hssf/record/ContinueRecord;->getData()[B

    move-result-object v6

    invoke-virtual {v4, v6}, Lorg/apache/poi/hssf/record/DrawingGroupRecord;->processContinueRecord([B)V

    move-object v2, v5

    .line 341
    goto/16 :goto_0

    .line 343
    :cond_9
    iget-object v4, p0, Lorg/apache/poi/hssf/record/RecordFactoryInputStream;->_lastRecord:Lorg/apache/poi/hssf/record/Record;

    instance-of v4, v4, Lorg/apache/poi/hssf/record/DrawingRecord;

    if-eqz v4, :cond_a

    move-object v2, v0

    .line 345
    goto/16 :goto_0

    .line 347
    :cond_a
    iget-object v4, p0, Lorg/apache/poi/hssf/record/RecordFactoryInputStream;->_lastRecord:Lorg/apache/poi/hssf/record/Record;

    instance-of v4, v4, Lorg/apache/poi/hssf/record/UnknownRecord;

    if-nez v4, :cond_0

    .line 352
    iget-object v4, p0, Lorg/apache/poi/hssf/record/RecordFactoryInputStream;->_lastRecord:Lorg/apache/poi/hssf/record/Record;

    instance-of v4, v4, Lorg/apache/poi/hssf/record/EOFRecord;

    if-nez v4, :cond_0

    .line 357
    new-instance v4, Lorg/apache/poi/hssf/record/RecordFormatException;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "Unhandled Continue Record followining "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, p0, Lorg/apache/poi/hssf/record/RecordFactoryInputStream;->_lastRecord:Lorg/apache/poi/hssf/record/Record;

    invoke-virtual {v6}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Lorg/apache/poi/hssf/record/RecordFormatException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 359
    .end local v0    # "contRec":Lorg/apache/poi/hssf/record/ContinueRecord;
    :cond_b
    iput-object v2, p0, Lorg/apache/poi/hssf/record/RecordFactoryInputStream;->_lastRecord:Lorg/apache/poi/hssf/record/Record;

    .line 360
    instance-of v4, v2, Lorg/apache/poi/hssf/record/DrawingRecord;

    if-eqz v4, :cond_0

    move-object v4, v2

    .line 361
    check-cast v4, Lorg/apache/poi/hssf/record/DrawingRecord;

    iput-object v4, p0, Lorg/apache/poi/hssf/record/RecordFactoryInputStream;->_lastDrawingRecord:Lorg/apache/poi/hssf/record/DrawingRecord;

    goto/16 :goto_0
.end method


# virtual methods
.method public nextRecord()Lorg/apache/poi/hssf/record/Record;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 225
    invoke-direct {p0}, Lorg/apache/poi/hssf/record/RecordFactoryInputStream;->getNextUnreadRecord()Lorg/apache/poi/hssf/record/Record;

    move-result-object v0

    .line 226
    .local v0, "r":Lorg/apache/poi/hssf/record/Record;
    if-eqz v0, :cond_2

    move-object v1, v0

    .line 256
    :cond_0
    :goto_0
    return-object v1

    .line 249
    :cond_1
    iget-object v2, p0, Lorg/apache/poi/hssf/record/RecordFactoryInputStream;->_recStream:Lorg/apache/poi/hssf/record/RecordInputStream;

    invoke-virtual {v2}, Lorg/apache/poi/hssf/record/RecordInputStream;->nextRecord()V

    .line 251
    invoke-direct {p0}, Lorg/apache/poi/hssf/record/RecordFactoryInputStream;->readNextRecord()Lorg/apache/poi/hssf/record/Record;

    move-result-object v0

    .line 252
    if-nez v0, :cond_3

    .line 231
    :cond_2
    iget-object v2, p0, Lorg/apache/poi/hssf/record/RecordFactoryInputStream;->_recStream:Lorg/apache/poi/hssf/record/RecordInputStream;

    invoke-virtual {v2}, Lorg/apache/poi/hssf/record/RecordInputStream;->hasNextRecord()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 236
    iget-boolean v2, p0, Lorg/apache/poi/hssf/record/RecordFactoryInputStream;->_lastRecordWasEOFLevelZero:Z

    if-eqz v2, :cond_1

    .line 242
    iget-object v2, p0, Lorg/apache/poi/hssf/record/RecordFactoryInputStream;->_recStream:Lorg/apache/poi/hssf/record/RecordInputStream;

    invoke-virtual {v2}, Lorg/apache/poi/hssf/record/RecordInputStream;->getNextSid()I

    move-result v2

    const/16 v3, 0x809

    if-eq v2, v3, :cond_1

    goto :goto_0

    :cond_3
    move-object v1, v0

    .line 256
    goto :goto_0
.end method
