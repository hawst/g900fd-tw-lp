.class public final Lorg/apache/poi/hssf/util/HSSFColor$BLUE_GREY;
.super Lorg/apache/poi/hssf/util/HSSFColor;
.source "HSSFColor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/poi/hssf/util/HSSFColor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "BLUE_GREY"
.end annotation


# static fields
.field public static final hexString:Ljava/lang/String; = "6666:6666:9999"

.field public static final index:S = 0x36s

.field public static final triplet:[S


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 659
    const/4 v0, 0x3

    new-array v0, v0, [S

    fill-array-data v0, :array_0

    .line 658
    sput-object v0, Lorg/apache/poi/hssf/util/HSSFColor$BLUE_GREY;->triplet:[S

    .line 662
    return-void

    .line 659
    nop

    :array_0
    .array-data 2
        0x66s
        0x66s
        0x99s
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 654
    invoke-direct {p0}, Lorg/apache/poi/hssf/util/HSSFColor;-><init>()V

    return-void
.end method


# virtual methods
.method public getHexString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 676
    const-string/jumbo v0, "6666:6666:9999"

    return-object v0
.end method

.method public getIndex()S
    .locals 1

    .prologue
    .line 666
    const/16 v0, 0x36

    return v0
.end method

.method public getTriplet()[S
    .locals 1

    .prologue
    .line 671
    sget-object v0, Lorg/apache/poi/hssf/util/HSSFColor$BLUE_GREY;->triplet:[S

    return-object v0
.end method
