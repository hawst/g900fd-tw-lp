.class public final Lorg/apache/poi/hssf/util/HSSFColor$LEMON_CHIFFON;
.super Lorg/apache/poi/hssf/util/HSSFColor;
.source "HSSFColor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/poi/hssf/util/HSSFColor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "LEMON_CHIFFON"
.end annotation


# static fields
.field public static final hexString:Ljava/lang/String; = "FFFF:FFFF:CCCC"

.field public static final index:S = 0x1as

.field public static final triplet:[S


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1500
    const/4 v0, 0x3

    new-array v0, v0, [S

    fill-array-data v0, :array_0

    .line 1499
    sput-object v0, Lorg/apache/poi/hssf/util/HSSFColor$LEMON_CHIFFON;->triplet:[S

    .line 1503
    return-void

    .line 1500
    nop

    :array_0
    .array-data 2
        0xffs
        0xffs
        0xccs
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1495
    invoke-direct {p0}, Lorg/apache/poi/hssf/util/HSSFColor;-><init>()V

    return-void
.end method


# virtual methods
.method public getHexString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1517
    const-string/jumbo v0, "FFFF:FFFF:CCCC"

    return-object v0
.end method

.method public getIndex()S
    .locals 1

    .prologue
    .line 1507
    const/16 v0, 0x1a

    return v0
.end method

.method public getTriplet()[S
    .locals 1

    .prologue
    .line 1512
    sget-object v0, Lorg/apache/poi/hssf/util/HSSFColor$LEMON_CHIFFON;->triplet:[S

    return-object v0
.end method
