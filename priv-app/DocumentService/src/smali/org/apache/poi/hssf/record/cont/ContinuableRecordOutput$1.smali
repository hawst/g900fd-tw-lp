.class Lorg/apache/poi/hssf/record/cont/ContinuableRecordOutput$1;
.super Ljava/lang/Object;
.source "ContinuableRecordOutput.java"

# interfaces
.implements Lorg/apache/poi/util/DelayableLittleEndianOutput;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/poi/hssf/record/cont/ContinuableRecordOutput;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 247
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1
    return-void
.end method


# virtual methods
.method public createDelayedOutput(I)Lorg/apache/poi/util/LittleEndianOutput;
    .locals 0
    .param p1, "size"    # I

    .prologue
    .line 250
    return-object p0
.end method

.method public write([B)V
    .locals 0
    .param p1, "b"    # [B

    .prologue
    .line 254
    return-void
.end method

.method public write([BII)V
    .locals 0
    .param p1, "b"    # [B
    .param p2, "offset"    # I
    .param p3, "len"    # I

    .prologue
    .line 257
    return-void
.end method

.method public writeByte(I)V
    .locals 0
    .param p1, "v"    # I

    .prologue
    .line 260
    return-void
.end method

.method public writeDouble(D)V
    .locals 0
    .param p1, "v"    # D

    .prologue
    .line 263
    return-void
.end method

.method public writeInt(I)V
    .locals 0
    .param p1, "v"    # I

    .prologue
    .line 266
    return-void
.end method

.method public writeLong(J)V
    .locals 0
    .param p1, "v"    # J

    .prologue
    .line 269
    return-void
.end method

.method public writeShort(I)V
    .locals 0
    .param p1, "v"    # I

    .prologue
    .line 272
    return-void
.end method
