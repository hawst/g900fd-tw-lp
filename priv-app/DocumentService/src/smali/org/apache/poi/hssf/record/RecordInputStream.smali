.class public final Lorg/apache/poi/hssf/record/RecordInputStream;
.super Ljava/lang/Object;
.source "RecordInputStream.java"

# interfaces
.implements Lorg/apache/poi/util/LittleEndianInput;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/poi/hssf/record/RecordInputStream$LeftoverDataException;,
        Lorg/apache/poi/hssf/record/RecordInputStream$SimpleHeaderInput;
    }
.end annotation


# static fields
.field private static final DATA_LEN_NEEDS_TO_BE_READ:I = -0x1

.field private static final EMPTY_BYTE_ARRAY:[B

.field private static final INVALID_SID_VALUE:I = -0x1

.field public static final MAX_RECORD_DATA_SIZE:S = 0x2020s


# instance fields
.field private final _bhi:Lorg/apache/poi/hssf/record/BiffHeaderInput;

.field private _currentDataLength:I

.field private _currentDataOffset:I

.field private _currentSid:I

.field private final _dataInput:Lorg/apache/poi/util/LittleEndianInput;

.field private _nextSid:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 46
    const/4 v0, 0x0

    new-array v0, v0, [B

    sput-object v0, Lorg/apache/poi/hssf/record/RecordInputStream;->EMPTY_BYTE_ARRAY:[B

    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 2
    .param p1, "in"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/poi/hssf/record/RecordFormatException;
        }
    .end annotation

    .prologue
    .line 104
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lorg/apache/poi/hssf/record/RecordInputStream;-><init>(Ljava/io/InputStream;Lorg/apache/poi/hssf/record/crypto/Biff8EncryptionKey;I)V

    .line 105
    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;Lorg/apache/poi/hssf/record/crypto/Biff8EncryptionKey;I)V
    .locals 2
    .param p1, "in"    # Ljava/io/InputStream;
    .param p2, "key"    # Lorg/apache/poi/hssf/record/crypto/Biff8EncryptionKey;
    .param p3, "initialOffset"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/poi/hssf/record/RecordFormatException;
        }
    .end annotation

    .prologue
    .line 107
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 108
    if-nez p2, :cond_0

    .line 109
    invoke-static {p1}, Lorg/apache/poi/hssf/record/RecordInputStream;->getLEI(Ljava/io/InputStream;)Lorg/apache/poi/util/LittleEndianInput;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/poi/hssf/record/RecordInputStream;->_dataInput:Lorg/apache/poi/util/LittleEndianInput;

    .line 110
    new-instance v1, Lorg/apache/poi/hssf/record/RecordInputStream$SimpleHeaderInput;

    invoke-direct {v1, p1}, Lorg/apache/poi/hssf/record/RecordInputStream$SimpleHeaderInput;-><init>(Ljava/io/InputStream;)V

    iput-object v1, p0, Lorg/apache/poi/hssf/record/RecordInputStream;->_bhi:Lorg/apache/poi/hssf/record/BiffHeaderInput;

    .line 116
    :goto_0
    invoke-direct {p0}, Lorg/apache/poi/hssf/record/RecordInputStream;->readNextSid()I

    move-result v1

    iput v1, p0, Lorg/apache/poi/hssf/record/RecordInputStream;->_nextSid:I

    .line 117
    return-void

    .line 112
    :cond_0
    new-instance v0, Lorg/apache/poi/hssf/record/crypto/Biff8DecryptingStream;

    invoke-direct {v0, p1, p3, p2}, Lorg/apache/poi/hssf/record/crypto/Biff8DecryptingStream;-><init>(Ljava/io/InputStream;ILorg/apache/poi/hssf/record/crypto/Biff8EncryptionKey;)V

    .line 113
    .local v0, "bds":Lorg/apache/poi/hssf/record/crypto/Biff8DecryptingStream;
    iput-object v0, p0, Lorg/apache/poi/hssf/record/RecordInputStream;->_bhi:Lorg/apache/poi/hssf/record/BiffHeaderInput;

    .line 114
    iput-object v0, p0, Lorg/apache/poi/hssf/record/RecordInputStream;->_dataInput:Lorg/apache/poi/util/LittleEndianInput;

    goto :goto_0
.end method

.method private checkRecordPosition(I)V
    .locals 4
    .param p1, "requiredByteCount"    # I

    .prologue
    .line 223
    invoke-virtual {p0}, Lorg/apache/poi/hssf/record/RecordInputStream;->remaining()I

    move-result v0

    .line 224
    .local v0, "nAvailable":I
    if-lt v0, p1, :cond_0

    .line 230
    :goto_0
    return-void

    .line 228
    :cond_0
    if-nez v0, :cond_1

    invoke-direct {p0}, Lorg/apache/poi/hssf/record/RecordInputStream;->isContinueNext()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 229
    invoke-virtual {p0}, Lorg/apache/poi/hssf/record/RecordInputStream;->nextRecord()V

    goto :goto_0

    .line 232
    :cond_1
    new-instance v1, Lorg/apache/poi/hssf/record/RecordFormatException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Not enough data ("

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 233
    const-string/jumbo v3, ") to read requested ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ") bytes"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 232
    invoke-direct {v1, v2}, Lorg/apache/poi/hssf/record/RecordFormatException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method static getLEI(Ljava/io/InputStream;)Lorg/apache/poi/util/LittleEndianInput;
    .locals 1
    .param p0, "is"    # Ljava/io/InputStream;

    .prologue
    .line 120
    instance-of v0, p0, Lorg/apache/poi/util/LittleEndianInput;

    if-eqz v0, :cond_0

    .line 122
    check-cast p0, Lorg/apache/poi/util/LittleEndianInput;

    .line 125
    .end local p0    # "is":Ljava/io/InputStream;
    :goto_0
    return-object p0

    .restart local p0    # "is":Ljava/io/InputStream;
    :cond_0
    new-instance v0, Lorg/apache/poi/util/LittleEndianInputStream;

    invoke-direct {v0, p0}, Lorg/apache/poi/util/LittleEndianInputStream;-><init>(Ljava/io/InputStream;)V

    move-object p0, v0

    goto :goto_0
.end method

.method private isContinueNext()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 438
    iget v1, p0, Lorg/apache/poi/hssf/record/RecordInputStream;->_currentDataLength:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    iget v1, p0, Lorg/apache/poi/hssf/record/RecordInputStream;->_currentDataOffset:I

    iget v2, p0, Lorg/apache/poi/hssf/record/RecordInputStream;->_currentDataLength:I

    if-eq v1, v2, :cond_0

    .line 439
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Should never be called before end of current record"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 441
    :cond_0
    invoke-virtual {p0}, Lorg/apache/poi/hssf/record/RecordInputStream;->hasNextRecord()Z

    move-result v1

    if-nez v1, :cond_2

    .line 449
    :cond_1
    :goto_0
    return v0

    :cond_2
    iget v1, p0, Lorg/apache/poi/hssf/record/RecordInputStream;->_nextSid:I

    const/16 v2, 0x3c

    if-ne v1, v2, :cond_1

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private readNextSid()I
    .locals 5

    .prologue
    const/4 v2, -0x1

    .line 184
    iget-object v3, p0, Lorg/apache/poi/hssf/record/RecordInputStream;->_bhi:Lorg/apache/poi/hssf/record/BiffHeaderInput;

    invoke-interface {v3}, Lorg/apache/poi/hssf/record/BiffHeaderInput;->available()I

    move-result v0

    .line 185
    .local v0, "nAvailable":I
    const/4 v3, 0x4

    if-ge v0, v3, :cond_0

    move v1, v2

    .line 198
    :goto_0
    return v1

    .line 193
    :cond_0
    iget-object v3, p0, Lorg/apache/poi/hssf/record/RecordInputStream;->_bhi:Lorg/apache/poi/hssf/record/BiffHeaderInput;

    invoke-interface {v3}, Lorg/apache/poi/hssf/record/BiffHeaderInput;->readRecordSID()I

    move-result v1

    .line 194
    .local v1, "result":I
    if-ne v1, v2, :cond_1

    .line 195
    new-instance v2, Lorg/apache/poi/hssf/record/RecordFormatException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "Found invalid sid ("

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lorg/apache/poi/hssf/record/RecordFormatException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 197
    :cond_1
    iput v2, p0, Lorg/apache/poi/hssf/record/RecordInputStream;->_currentDataLength:I

    goto :goto_0
.end method

.method private readStringCommon(IZ)Ljava/lang/String;
    .locals 9
    .param p1, "requestedLength"    # I
    .param p2, "pIsCompressedEncoding"    # Z

    .prologue
    .line 336
    if-ltz p1, :cond_0

    const/high16 v6, 0x100000

    if-le p1, v6, :cond_1

    .line 337
    :cond_0
    new-instance v6, Ljava/lang/IllegalArgumentException;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string/jumbo v8, "Bad requested string length ("

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, ")"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 339
    :cond_1
    new-array v1, p1, [C

    .line 340
    .local v1, "buf":[C
    move v5, p2

    .line 341
    .local v5, "isCompressedEncoding":Z
    const/4 v4, 0x0

    .line 343
    .local v4, "curLen":I
    :goto_0
    if-eqz v5, :cond_2

    invoke-virtual {p0}, Lorg/apache/poi/hssf/record/RecordInputStream;->remaining()I

    move-result v0

    .line 344
    .local v0, "availableChars":I
    :goto_1
    sub-int v6, p1, v4

    if-gt v6, v0, :cond_6

    .line 346
    :goto_2
    if-lt v4, p1, :cond_3

    .line 356
    new-instance v6, Ljava/lang/String;

    invoke-direct {v6, v1}, Ljava/lang/String;-><init>([C)V

    return-object v6

    .line 343
    .end local v0    # "availableChars":I
    :cond_2
    invoke-virtual {p0}, Lorg/apache/poi/hssf/record/RecordInputStream;->remaining()I

    move-result v6

    div-int/lit8 v0, v6, 0x2

    goto :goto_1

    .line 348
    .restart local v0    # "availableChars":I
    :cond_3
    if-eqz v5, :cond_4

    .line 349
    invoke-virtual {p0}, Lorg/apache/poi/hssf/record/RecordInputStream;->readUByte()I

    move-result v6

    int-to-char v2, v6

    .line 353
    .local v2, "ch":C
    :goto_3
    aput-char v2, v1, v4

    .line 354
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 351
    .end local v2    # "ch":C
    :cond_4
    invoke-virtual {p0}, Lorg/apache/poi/hssf/record/RecordInputStream;->readShort()S

    move-result v6

    int-to-char v2, v6

    .restart local v2    # "ch":C
    goto :goto_3

    .line 362
    .end local v2    # "ch":C
    :cond_5
    if-eqz v5, :cond_7

    .line 363
    invoke-virtual {p0}, Lorg/apache/poi/hssf/record/RecordInputStream;->readUByte()I

    move-result v6

    int-to-char v2, v6

    .line 367
    .restart local v2    # "ch":C
    :goto_4
    aput-char v2, v1, v4

    .line 368
    add-int/lit8 v4, v4, 0x1

    .line 369
    add-int/lit8 v0, v0, -0x1

    .line 360
    .end local v2    # "ch":C
    :cond_6
    if-gtz v0, :cond_5

    .line 371
    invoke-direct {p0}, Lorg/apache/poi/hssf/record/RecordInputStream;->isContinueNext()Z

    move-result v6

    if-nez v6, :cond_8

    .line 372
    new-instance v6, Lorg/apache/poi/hssf/record/RecordFormatException;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string/jumbo v8, "Expected to find a ContinueRecord in order to read remaining "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 373
    sub-int v8, p1, v4

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, " of "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, " chars"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 372
    invoke-direct {v6, v7}, Lorg/apache/poi/hssf/record/RecordFormatException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 365
    :cond_7
    invoke-virtual {p0}, Lorg/apache/poi/hssf/record/RecordInputStream;->readShort()S

    move-result v6

    int-to-char v2, v6

    .restart local v2    # "ch":C
    goto :goto_4

    .line 375
    .end local v2    # "ch":C
    :cond_8
    invoke-virtual {p0}, Lorg/apache/poi/hssf/record/RecordInputStream;->remaining()I

    move-result v6

    if-eqz v6, :cond_9

    .line 376
    new-instance v6, Lorg/apache/poi/hssf/record/RecordFormatException;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string/jumbo v8, "Odd number of bytes("

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lorg/apache/poi/hssf/record/RecordInputStream;->remaining()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, ") left behind"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Lorg/apache/poi/hssf/record/RecordFormatException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 378
    :cond_9
    invoke-virtual {p0}, Lorg/apache/poi/hssf/record/RecordInputStream;->nextRecord()V

    .line 380
    invoke-virtual {p0}, Lorg/apache/poi/hssf/record/RecordInputStream;->readByte()B

    move-result v3

    .line 381
    .local v3, "compressFlag":B
    if-nez v3, :cond_a

    const/4 v5, 0x1

    .line 342
    :goto_5
    goto/16 :goto_0

    .line 381
    :cond_a
    const/4 v5, 0x0

    goto :goto_5
.end method


# virtual methods
.method public available()I
    .locals 1

    .prologue
    .line 133
    invoke-virtual {p0}, Lorg/apache/poi/hssf/record/RecordInputStream;->remaining()I

    move-result v0

    return v0
.end method

.method public getNextSid()I
    .locals 1

    .prologue
    .line 456
    iget v0, p0, Lorg/apache/poi/hssf/record/RecordInputStream;->_nextSid:I

    return v0
.end method

.method public getSid()S
    .locals 1

    .prologue
    .line 146
    iget v0, p0, Lorg/apache/poi/hssf/record/RecordInputStream;->_currentSid:I

    int-to-short v0, v0

    return v0
.end method

.method public hasNextRecord()Z
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/poi/hssf/record/RecordInputStream$LeftoverDataException;
        }
    .end annotation

    .prologue
    const/4 v4, -0x1

    .line 156
    iget v2, p0, Lorg/apache/poi/hssf/record/RecordInputStream;->_currentDataLength:I

    if-eq v2, v4, :cond_0

    .line 157
    iget v2, p0, Lorg/apache/poi/hssf/record/RecordInputStream;->_currentDataLength:I

    iget v3, p0, Lorg/apache/poi/hssf/record/RecordInputStream;->_currentDataOffset:I

    if-eq v2, v3, :cond_0

    .line 159
    iget v2, p0, Lorg/apache/poi/hssf/record/RecordInputStream;->_currentSid:I

    const/16 v3, 0x850

    if-ne v2, v3, :cond_3

    .line 160
    invoke-virtual {p0}, Lorg/apache/poi/hssf/record/RecordInputStream;->remaining()I

    move-result v0

    .line 161
    .local v0, "unreadBytes":I
    const/16 v2, 0x2020

    if-gt v0, v2, :cond_2

    .line 162
    new-array v1, v0, [B

    .line 163
    .local v1, "unreadData":[B
    invoke-virtual {p0, v1}, Lorg/apache/poi/hssf/record/RecordInputStream;->readFully([B)V

    .line 174
    .end local v0    # "unreadBytes":I
    .end local v1    # "unreadData":[B
    :cond_0
    iget v2, p0, Lorg/apache/poi/hssf/record/RecordInputStream;->_currentDataLength:I

    if-eq v2, v4, :cond_1

    .line 175
    invoke-direct {p0}, Lorg/apache/poi/hssf/record/RecordInputStream;->readNextSid()I

    move-result v2

    iput v2, p0, Lorg/apache/poi/hssf/record/RecordInputStream;->_nextSid:I

    .line 177
    :cond_1
    iget v2, p0, Lorg/apache/poi/hssf/record/RecordInputStream;->_nextSid:I

    if-eq v2, v4, :cond_4

    const/4 v2, 0x1

    :goto_0
    return v2

    .line 165
    .restart local v0    # "unreadBytes":I
    :cond_2
    new-instance v2, Lorg/apache/poi/hssf/record/RecordFormatException;

    .line 166
    const-string/jumbo v3, "The content of an excel record cannot exceed 8224 bytes"

    .line 165
    invoke-direct {v2, v3}, Lorg/apache/poi/hssf/record/RecordFormatException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 170
    .end local v0    # "unreadBytes":I
    :cond_3
    new-instance v2, Lorg/apache/poi/hssf/record/RecordInputStream$LeftoverDataException;

    iget v3, p0, Lorg/apache/poi/hssf/record/RecordInputStream;->_currentSid:I

    invoke-virtual {p0}, Lorg/apache/poi/hssf/record/RecordInputStream;->remaining()I

    move-result v4

    invoke-direct {v2, v3, v4}, Lorg/apache/poi/hssf/record/RecordInputStream$LeftoverDataException;-><init>(II)V

    throw v2

    .line 177
    :cond_4
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public nextRecord()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/poi/hssf/record/RecordFormatException;
        }
    .end annotation

    .prologue
    const/4 v1, -0x1

    .line 206
    iget v0, p0, Lorg/apache/poi/hssf/record/RecordInputStream;->_nextSid:I

    if-ne v0, v1, :cond_0

    .line 207
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "EOF - next record not available"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 209
    :cond_0
    iget v0, p0, Lorg/apache/poi/hssf/record/RecordInputStream;->_currentDataLength:I

    if-eq v0, v1, :cond_1

    .line 210
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Cannot call nextRecord() without checking hasNextRecord() first"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 212
    :cond_1
    iget v0, p0, Lorg/apache/poi/hssf/record/RecordInputStream;->_nextSid:I

    iput v0, p0, Lorg/apache/poi/hssf/record/RecordInputStream;->_currentSid:I

    .line 213
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/poi/hssf/record/RecordInputStream;->_currentDataOffset:I

    .line 214
    iget-object v0, p0, Lorg/apache/poi/hssf/record/RecordInputStream;->_bhi:Lorg/apache/poi/hssf/record/BiffHeaderInput;

    invoke-interface {v0}, Lorg/apache/poi/hssf/record/BiffHeaderInput;->readDataSize()I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hssf/record/RecordInputStream;->_currentDataLength:I

    .line 215
    iget v0, p0, Lorg/apache/poi/hssf/record/RecordInputStream;->_currentDataLength:I

    const/16 v1, 0x2020

    if-le v0, v1, :cond_2

    .line 216
    new-instance v0, Lorg/apache/poi/hssf/record/RecordFormatException;

    const-string/jumbo v1, "The content of an excel record cannot exceed 8224 bytes"

    invoke-direct {v0, v1}, Lorg/apache/poi/hssf/record/RecordFormatException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 219
    :cond_2
    return-void
.end method

.method public read([BII)I
    .locals 2
    .param p1, "b"    # [B
    .param p2, "off"    # I
    .param p3, "len"    # I

    .prologue
    .line 137
    invoke-virtual {p0}, Lorg/apache/poi/hssf/record/RecordInputStream;->remaining()I

    move-result v1

    invoke-static {p3, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 138
    .local v0, "limit":I
    if-nez v0, :cond_0

    .line 139
    const/4 v0, 0x0

    .line 142
    .end local v0    # "limit":I
    :goto_0
    return v0

    .line 141
    .restart local v0    # "limit":I
    :cond_0
    invoke-virtual {p0, p1, p2, v0}, Lorg/apache/poi/hssf/record/RecordInputStream;->readFully([BII)V

    goto :goto_0
.end method

.method public readAllContinuedRemainder()[B
    .locals 4

    .prologue
    .line 408
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    const/16 v2, 0x4040

    invoke-direct {v1, v2}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    .line 411
    .local v1, "out":Ljava/io/ByteArrayOutputStream;
    :goto_0
    invoke-virtual {p0}, Lorg/apache/poi/hssf/record/RecordInputStream;->readRemainder()[B

    move-result-object v0

    .line 412
    .local v0, "b":[B
    const/4 v2, 0x0

    array-length v3, v0

    invoke-virtual {v1, v0, v2, v3}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    .line 413
    invoke-direct {p0}, Lorg/apache/poi/hssf/record/RecordInputStream;->isContinueNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 418
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v2

    return-object v2

    .line 416
    :cond_0
    invoke-virtual {p0}, Lorg/apache/poi/hssf/record/RecordInputStream;->nextRecord()V

    goto :goto_0
.end method

.method public readByte()B
    .locals 1

    .prologue
    .line 240
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lorg/apache/poi/hssf/record/RecordInputStream;->checkRecordPosition(I)V

    .line 241
    iget v0, p0, Lorg/apache/poi/hssf/record/RecordInputStream;->_currentDataOffset:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/poi/hssf/record/RecordInputStream;->_currentDataOffset:I

    .line 242
    iget-object v0, p0, Lorg/apache/poi/hssf/record/RecordInputStream;->_dataInput:Lorg/apache/poi/util/LittleEndianInput;

    invoke-interface {v0}, Lorg/apache/poi/util/LittleEndianInput;->readByte()B

    move-result v0

    return v0
.end method

.method public readCompressedUnicode(I)Ljava/lang/String;
    .locals 1
    .param p1, "requestedLength"    # I

    .prologue
    .line 331
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lorg/apache/poi/hssf/record/RecordInputStream;->readStringCommon(IZ)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public readDouble()D
    .locals 4

    .prologue
    .line 289
    invoke-virtual {p0}, Lorg/apache/poi/hssf/record/RecordInputStream;->readLong()J

    move-result-wide v2

    .line 290
    .local v2, "valueLongBits":J
    invoke-static {v2, v3}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v0

    .line 291
    .local v0, "result":D
    invoke-static {v0, v1}, Ljava/lang/Double;->isNaN(D)Z

    .line 297
    return-wide v0
.end method

.method public readFully([B)V
    .locals 2
    .param p1, "buf"    # [B

    .prologue
    .line 300
    const/4 v0, 0x0

    array-length v1, p1

    invoke-virtual {p0, p1, v0, v1}, Lorg/apache/poi/hssf/record/RecordInputStream;->readFully([BII)V

    .line 301
    return-void
.end method

.method public readFully([BII)V
    .locals 1
    .param p1, "buf"    # [B
    .param p2, "off"    # I
    .param p3, "len"    # I

    .prologue
    .line 304
    invoke-direct {p0, p3}, Lorg/apache/poi/hssf/record/RecordInputStream;->checkRecordPosition(I)V

    .line 305
    iget-object v0, p0, Lorg/apache/poi/hssf/record/RecordInputStream;->_dataInput:Lorg/apache/poi/util/LittleEndianInput;

    invoke-interface {v0, p1, p2, p3}, Lorg/apache/poi/util/LittleEndianInput;->readFully([BII)V

    .line 306
    iget v0, p0, Lorg/apache/poi/hssf/record/RecordInputStream;->_currentDataOffset:I

    add-int/2addr v0, p3

    iput v0, p0, Lorg/apache/poi/hssf/record/RecordInputStream;->_currentDataOffset:I

    .line 307
    return-void
.end method

.method public readInt()I
    .locals 1

    .prologue
    .line 258
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lorg/apache/poi/hssf/record/RecordInputStream;->checkRecordPosition(I)V

    .line 259
    iget v0, p0, Lorg/apache/poi/hssf/record/RecordInputStream;->_currentDataOffset:I

    add-int/lit8 v0, v0, 0x4

    iput v0, p0, Lorg/apache/poi/hssf/record/RecordInputStream;->_currentDataOffset:I

    .line 260
    iget-object v0, p0, Lorg/apache/poi/hssf/record/RecordInputStream;->_dataInput:Lorg/apache/poi/util/LittleEndianInput;

    invoke-interface {v0}, Lorg/apache/poi/util/LittleEndianInput;->readInt()I

    move-result v0

    return v0
.end method

.method public readLong()J
    .locals 2

    .prologue
    .line 267
    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lorg/apache/poi/hssf/record/RecordInputStream;->checkRecordPosition(I)V

    .line 268
    iget v0, p0, Lorg/apache/poi/hssf/record/RecordInputStream;->_currentDataOffset:I

    add-int/lit8 v0, v0, 0x8

    iput v0, p0, Lorg/apache/poi/hssf/record/RecordInputStream;->_currentDataOffset:I

    .line 269
    iget-object v0, p0, Lorg/apache/poi/hssf/record/RecordInputStream;->_dataInput:Lorg/apache/poi/util/LittleEndianInput;

    invoke-interface {v0}, Lorg/apache/poi/util/LittleEndianInput;->readLong()J

    move-result-wide v0

    return-wide v0
.end method

.method public readRemainder()[B
    .locals 2

    .prologue
    .line 390
    invoke-virtual {p0}, Lorg/apache/poi/hssf/record/RecordInputStream;->remaining()I

    move-result v1

    .line 391
    .local v1, "size":I
    if-nez v1, :cond_0

    .line 392
    sget-object v0, Lorg/apache/poi/hssf/record/RecordInputStream;->EMPTY_BYTE_ARRAY:[B

    .line 396
    :goto_0
    return-object v0

    .line 394
    :cond_0
    new-array v0, v1, [B

    .line 395
    .local v0, "result":[B
    invoke-virtual {p0, v0}, Lorg/apache/poi/hssf/record/RecordInputStream;->readFully([B)V

    goto :goto_0
.end method

.method public readShort()S
    .locals 1

    .prologue
    .line 249
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lorg/apache/poi/hssf/record/RecordInputStream;->checkRecordPosition(I)V

    .line 250
    iget v0, p0, Lorg/apache/poi/hssf/record/RecordInputStream;->_currentDataOffset:I

    add-int/lit8 v0, v0, 0x2

    iput v0, p0, Lorg/apache/poi/hssf/record/RecordInputStream;->_currentDataOffset:I

    .line 251
    iget-object v0, p0, Lorg/apache/poi/hssf/record/RecordInputStream;->_dataInput:Lorg/apache/poi/util/LittleEndianInput;

    invoke-interface {v0}, Lorg/apache/poi/util/LittleEndianInput;->readShort()S

    move-result v0

    return v0
.end method

.method public readString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 310
    invoke-virtual {p0}, Lorg/apache/poi/hssf/record/RecordInputStream;->readUShort()I

    move-result v1

    .line 311
    .local v1, "requestedLength":I
    invoke-virtual {p0}, Lorg/apache/poi/hssf/record/RecordInputStream;->readByte()B

    move-result v0

    .line 312
    .local v0, "compressFlag":B
    if-nez v0, :cond_0

    const/4 v2, 0x1

    :goto_0
    invoke-direct {p0, v1, v2}, Lorg/apache/poi/hssf/record/RecordInputStream;->readStringCommon(IZ)Ljava/lang/String;

    move-result-object v2

    return-object v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public readUByte()I
    .locals 1

    .prologue
    .line 276
    invoke-virtual {p0}, Lorg/apache/poi/hssf/record/RecordInputStream;->readByte()B

    move-result v0

    and-int/lit16 v0, v0, 0xff

    return v0
.end method

.method public readUShort()I
    .locals 1

    .prologue
    .line 283
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lorg/apache/poi/hssf/record/RecordInputStream;->checkRecordPosition(I)V

    .line 284
    iget v0, p0, Lorg/apache/poi/hssf/record/RecordInputStream;->_currentDataOffset:I

    add-int/lit8 v0, v0, 0x2

    iput v0, p0, Lorg/apache/poi/hssf/record/RecordInputStream;->_currentDataOffset:I

    .line 285
    iget-object v0, p0, Lorg/apache/poi/hssf/record/RecordInputStream;->_dataInput:Lorg/apache/poi/util/LittleEndianInput;

    invoke-interface {v0}, Lorg/apache/poi/util/LittleEndianInput;->readUShort()I

    move-result v0

    return v0
.end method

.method public readUnicodeLEString(I)Ljava/lang/String;
    .locals 1
    .param p1, "requestedLength"    # I

    .prologue
    .line 327
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lorg/apache/poi/hssf/record/RecordInputStream;->readStringCommon(IZ)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public remaining()I
    .locals 2

    .prologue
    .line 426
    iget v0, p0, Lorg/apache/poi/hssf/record/RecordInputStream;->_currentDataLength:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 428
    const/4 v0, 0x0

    .line 430
    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lorg/apache/poi/hssf/record/RecordInputStream;->_currentDataLength:I

    iget v1, p0, Lorg/apache/poi/hssf/record/RecordInputStream;->_currentDataOffset:I

    sub-int/2addr v0, v1

    goto :goto_0
.end method
