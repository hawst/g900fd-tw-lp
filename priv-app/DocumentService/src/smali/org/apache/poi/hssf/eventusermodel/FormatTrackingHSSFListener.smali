.class public Lorg/apache/poi/hssf/eventusermodel/FormatTrackingHSSFListener;
.super Ljava/lang/Object;
.source "FormatTrackingHSSFListener.java"

# interfaces
.implements Lorg/apache/poi/hssf/eventusermodel/HSSFListener;


# static fields
.field private static logger:Lorg/apache/poi/util/POILogger;


# instance fields
.field private final _childListener:Lorg/apache/poi/hssf/eventusermodel/HSSFListener;

.field private final _customFormatRecords:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lorg/apache/poi/hssf/record/FormatRecord;",
            ">;"
        }
    .end annotation
.end field

.field private final _defaultFormat:Ljava/text/NumberFormat;

.field private final _formatter:Lorg/apache/poi/hssf/usermodel/HSSFDataFormatter;

.field private final _xfRecords:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/apache/poi/hssf/record/ExtendedFormatRecord;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 43
    const-class v0, Lorg/apache/poi/hssf/eventusermodel/FormatTrackingHSSFListener;

    invoke-static {v0}, Lorg/apache/poi/util/POILogFactory;->getLogger(Ljava/lang/Class;)Lorg/apache/poi/util/POILogger;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hssf/eventusermodel/FormatTrackingHSSFListener;->logger:Lorg/apache/poi/util/POILogger;

    return-void
.end method

.method public constructor <init>(Lorg/apache/poi/hssf/eventusermodel/HSSFListener;)V
    .locals 1
    .param p1, "childListener"    # Lorg/apache/poi/hssf/eventusermodel/HSSFListener;

    .prologue
    .line 55
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lorg/apache/poi/hssf/eventusermodel/FormatTrackingHSSFListener;-><init>(Lorg/apache/poi/hssf/eventusermodel/HSSFListener;Ljava/util/Locale;)V

    .line 56
    return-void
.end method

.method public constructor <init>(Lorg/apache/poi/hssf/eventusermodel/HSSFListener;Ljava/util/Locale;)V
    .locals 1
    .param p1, "childListener"    # Lorg/apache/poi/hssf/eventusermodel/HSSFListener;
    .param p2, "locale"    # Ljava/util/Locale;

    .prologue
    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/hssf/eventusermodel/FormatTrackingHSSFListener;->_customFormatRecords:Ljava/util/Map;

    .line 48
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/hssf/eventusermodel/FormatTrackingHSSFListener;->_xfRecords:Ljava/util/List;

    .line 64
    iput-object p1, p0, Lorg/apache/poi/hssf/eventusermodel/FormatTrackingHSSFListener;->_childListener:Lorg/apache/poi/hssf/eventusermodel/HSSFListener;

    .line 65
    new-instance v0, Lorg/apache/poi/hssf/usermodel/HSSFDataFormatter;

    invoke-direct {v0, p2}, Lorg/apache/poi/hssf/usermodel/HSSFDataFormatter;-><init>(Ljava/util/Locale;)V

    iput-object v0, p0, Lorg/apache/poi/hssf/eventusermodel/FormatTrackingHSSFListener;->_formatter:Lorg/apache/poi/hssf/usermodel/HSSFDataFormatter;

    .line 66
    invoke-static {p2}, Ljava/text/NumberFormat;->getInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/poi/hssf/eventusermodel/FormatTrackingHSSFListener;->_defaultFormat:Ljava/text/NumberFormat;

    .line 67
    return-void
.end method


# virtual methods
.method public formatNumberDateCell(Lorg/apache/poi/hssf/record/CellValueRecordInterface;)Ljava/lang/String;
    .locals 7
    .param p1, "cell"    # Lorg/apache/poi/hssf/record/CellValueRecordInterface;

    .prologue
    .line 115
    instance-of v4, p1, Lorg/apache/poi/hssf/record/NumberRecord;

    if-eqz v4, :cond_0

    move-object v4, p1

    .line 116
    check-cast v4, Lorg/apache/poi/hssf/record/NumberRecord;

    invoke-virtual {v4}, Lorg/apache/poi/hssf/record/NumberRecord;->getValue()D

    move-result-wide v2

    .line 124
    .local v2, "value":D
    :goto_0
    invoke-virtual {p0, p1}, Lorg/apache/poi/hssf/eventusermodel/FormatTrackingHSSFListener;->getFormatIndex(Lorg/apache/poi/hssf/record/CellValueRecordInterface;)I

    move-result v0

    .line 125
    .local v0, "formatIndex":I
    invoke-virtual {p0, p1}, Lorg/apache/poi/hssf/eventusermodel/FormatTrackingHSSFListener;->getFormatString(Lorg/apache/poi/hssf/record/CellValueRecordInterface;)Ljava/lang/String;

    move-result-object v1

    .line 127
    .local v1, "formatString":Ljava/lang/String;
    if-nez v1, :cond_2

    .line 128
    iget-object v4, p0, Lorg/apache/poi/hssf/eventusermodel/FormatTrackingHSSFListener;->_defaultFormat:Ljava/text/NumberFormat;

    invoke-virtual {v4, v2, v3}, Ljava/text/NumberFormat;->format(D)Ljava/lang/String;

    move-result-object v4

    .line 132
    :goto_1
    return-object v4

    .line 117
    .end local v0    # "formatIndex":I
    .end local v1    # "formatString":Ljava/lang/String;
    .end local v2    # "value":D
    :cond_0
    instance-of v4, p1, Lorg/apache/poi/hssf/record/FormulaRecord;

    if-eqz v4, :cond_1

    move-object v4, p1

    .line 118
    check-cast v4, Lorg/apache/poi/hssf/record/FormulaRecord;

    invoke-virtual {v4}, Lorg/apache/poi/hssf/record/FormulaRecord;->getValue()D

    move-result-wide v2

    .line 119
    .restart local v2    # "value":D
    goto :goto_0

    .line 120
    .end local v2    # "value":D
    :cond_1
    new-instance v4, Ljava/lang/IllegalArgumentException;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "Unsupported CellValue Record passed in "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 132
    .restart local v0    # "formatIndex":I
    .restart local v1    # "formatString":Ljava/lang/String;
    .restart local v2    # "value":D
    :cond_2
    iget-object v4, p0, Lorg/apache/poi/hssf/eventusermodel/FormatTrackingHSSFListener;->_formatter:Lorg/apache/poi/hssf/usermodel/HSSFDataFormatter;

    invoke-virtual {v4, v2, v3, v0, v1}, Lorg/apache/poi/hssf/usermodel/HSSFDataFormatter;->formatRawCellContents(DILjava/lang/String;)Ljava/lang/String;

    move-result-object v4

    goto :goto_1
.end method

.method public getFormatIndex(Lorg/apache/poi/hssf/record/CellValueRecordInterface;)I
    .locals 5
    .param p1, "cell"    # Lorg/apache/poi/hssf/record/CellValueRecordInterface;

    .prologue
    .line 171
    iget-object v1, p0, Lorg/apache/poi/hssf/eventusermodel/FormatTrackingHSSFListener;->_xfRecords:Ljava/util/List;

    invoke-interface {p1}, Lorg/apache/poi/hssf/record/CellValueRecordInterface;->getXFIndex()S

    move-result v2

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;

    .line 172
    .local v0, "xfr":Lorg/apache/poi/hssf/record/ExtendedFormatRecord;
    if-nez v0, :cond_0

    .line 173
    sget-object v1, Lorg/apache/poi/hssf/eventusermodel/FormatTrackingHSSFListener;->logger:Lorg/apache/poi/util/POILogger;

    const/4 v2, 0x7

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "Cell "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p1}, Lorg/apache/poi/hssf/record/CellValueRecordInterface;->getRow()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {p1}, Lorg/apache/poi/hssf/record/CellValueRecordInterface;->getColumn()S

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 174
    const-string/jumbo v4, " uses XF with index "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {p1}, Lorg/apache/poi/hssf/record/CellValueRecordInterface;->getXFIndex()S

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, ", but we don\'t have that"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 173
    invoke-virtual {v1, v2, v3}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;)V

    .line 175
    const/4 v1, -0x1

    .line 177
    :goto_0
    return v1

    :cond_0
    invoke-virtual {v0}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->getFormatIndex()S

    move-result v1

    goto :goto_0
.end method

.method public getFormatString(I)Ljava/lang/String;
    .locals 6
    .param p1, "formatIndex"    # I

    .prologue
    .line 139
    const/4 v0, 0x0

    .line 140
    .local v0, "format":Ljava/lang/String;
    invoke-static {}, Lorg/apache/poi/hssf/usermodel/HSSFDataFormat;->getNumberOfBuiltinBuiltinFormats()I

    move-result v2

    if-lt p1, v2, :cond_1

    .line 141
    iget-object v2, p0, Lorg/apache/poi/hssf/eventusermodel/FormatTrackingHSSFListener;->_customFormatRecords:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/hssf/record/FormatRecord;

    .line 142
    .local v1, "tfr":Lorg/apache/poi/hssf/record/FormatRecord;
    if-nez v1, :cond_0

    .line 143
    sget-object v2, Lorg/apache/poi/hssf/eventusermodel/FormatTrackingHSSFListener;->logger:Lorg/apache/poi/util/POILogger;

    const/4 v3, 0x7

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "Requested format at index "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 144
    const-string/jumbo v5, ", but it wasn\'t found"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 143
    invoke-virtual {v2, v3, v4}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;)V

    .line 151
    .end local v1    # "tfr":Lorg/apache/poi/hssf/record/FormatRecord;
    :goto_0
    return-object v0

    .line 146
    .restart local v1    # "tfr":Lorg/apache/poi/hssf/record/FormatRecord;
    :cond_0
    invoke-virtual {v1}, Lorg/apache/poi/hssf/record/FormatRecord;->getFormatString()Ljava/lang/String;

    move-result-object v0

    .line 148
    goto :goto_0

    .line 149
    .end local v1    # "tfr":Lorg/apache/poi/hssf/record/FormatRecord;
    :cond_1
    int-to-short v2, p1

    invoke-static {v2}, Lorg/apache/poi/hssf/usermodel/HSSFDataFormat;->getBuiltinFormat(S)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getFormatString(Lorg/apache/poi/hssf/record/CellValueRecordInterface;)Ljava/lang/String;
    .locals 2
    .param p1, "cell"    # Lorg/apache/poi/hssf/record/CellValueRecordInterface;

    .prologue
    .line 158
    invoke-virtual {p0, p1}, Lorg/apache/poi/hssf/eventusermodel/FormatTrackingHSSFListener;->getFormatIndex(Lorg/apache/poi/hssf/record/CellValueRecordInterface;)I

    move-result v0

    .line 159
    .local v0, "formatIndex":I
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 161
    const/4 v1, 0x0

    .line 163
    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {p0, v0}, Lorg/apache/poi/hssf/eventusermodel/FormatTrackingHSSFListener;->getFormatString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method protected getNumberOfCustomFormats()I
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lorg/apache/poi/hssf/eventusermodel/FormatTrackingHSSFListener;->_customFormatRecords:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    return v0
.end method

.method protected getNumberOfExtendedFormats()I
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lorg/apache/poi/hssf/eventusermodel/FormatTrackingHSSFListener;->_xfRecords:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public processRecord(Lorg/apache/poi/hssf/record/Record;)V
    .locals 1
    .param p1, "record"    # Lorg/apache/poi/hssf/record/Record;

    .prologue
    .line 82
    invoke-virtual {p0, p1}, Lorg/apache/poi/hssf/eventusermodel/FormatTrackingHSSFListener;->processRecordInternally(Lorg/apache/poi/hssf/record/Record;)V

    .line 85
    iget-object v0, p0, Lorg/apache/poi/hssf/eventusermodel/FormatTrackingHSSFListener;->_childListener:Lorg/apache/poi/hssf/eventusermodel/HSSFListener;

    invoke-interface {v0, p1}, Lorg/apache/poi/hssf/eventusermodel/HSSFListener;->processRecord(Lorg/apache/poi/hssf/record/Record;)V

    .line 86
    return-void
.end method

.method public processRecordInternally(Lorg/apache/poi/hssf/record/Record;)V
    .locals 4
    .param p1, "record"    # Lorg/apache/poi/hssf/record/Record;

    .prologue
    .line 95
    instance-of v2, p1, Lorg/apache/poi/hssf/record/FormatRecord;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 96
    check-cast v0, Lorg/apache/poi/hssf/record/FormatRecord;

    .line 97
    .local v0, "fr":Lorg/apache/poi/hssf/record/FormatRecord;
    iget-object v2, p0, Lorg/apache/poi/hssf/eventusermodel/FormatTrackingHSSFListener;->_customFormatRecords:Ljava/util/Map;

    invoke-virtual {v0}, Lorg/apache/poi/hssf/record/FormatRecord;->getIndexCode()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 99
    .end local v0    # "fr":Lorg/apache/poi/hssf/record/FormatRecord;
    :cond_0
    instance-of v2, p1, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;

    if-eqz v2, :cond_1

    move-object v1, p1

    .line 100
    check-cast v1, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;

    .line 101
    .local v1, "xr":Lorg/apache/poi/hssf/record/ExtendedFormatRecord;
    iget-object v2, p0, Lorg/apache/poi/hssf/eventusermodel/FormatTrackingHSSFListener;->_xfRecords:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 103
    .end local v1    # "xr":Lorg/apache/poi/hssf/record/ExtendedFormatRecord;
    :cond_1
    return-void
.end method
