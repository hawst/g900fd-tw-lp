.class public Lorg/apache/poi/hssf/record/common/UnicodeString$ExtRst;
.super Ljava/lang/Object;
.source "UnicodeString.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/poi/hssf/record/common/UnicodeString;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ExtRst"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lorg/apache/poi/hssf/record/common/UnicodeString$ExtRst;",
        ">;"
    }
.end annotation


# instance fields
.field private extraData:[B

.field private formattingFontIndex:S

.field private formattingOptions:S

.field private numberOfRuns:I

.field private phRuns:[Lorg/apache/poi/hssf/record/common/UnicodeString$PhRun;

.field private phoneticText:Ljava/lang/String;

.field private reserved:S


# direct methods
.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 125
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 126
    invoke-direct {p0}, Lorg/apache/poi/hssf/record/common/UnicodeString$ExtRst;->populateEmpty()V

    .line 127
    return-void
.end method

.method protected constructor <init>(Lorg/apache/poi/util/LittleEndianInput;I)V
    .locals 11
    .param p1, "in"    # Lorg/apache/poi/util/LittleEndianInput;
    .param p2, "expectedLength"    # I

    .prologue
    const/4 v10, 0x5

    .line 128
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 129
    invoke-interface {p1}, Lorg/apache/poi/util/LittleEndianInput;->readShort()S

    move-result v7

    iput-short v7, p0, Lorg/apache/poi/hssf/record/common/UnicodeString$ExtRst;->reserved:S

    .line 132
    iget-short v7, p0, Lorg/apache/poi/hssf/record/common/UnicodeString$ExtRst;->reserved:S

    const/4 v8, -0x1

    if-ne v7, v8, :cond_1

    .line 133
    invoke-direct {p0}, Lorg/apache/poi/hssf/record/common/UnicodeString$ExtRst;->populateEmpty()V

    .line 189
    :cond_0
    :goto_0
    return-void

    .line 138
    :cond_1
    iget-short v7, p0, Lorg/apache/poi/hssf/record/common/UnicodeString$ExtRst;->reserved:S

    const/4 v8, 0x1

    if-eq v7, v8, :cond_3

    .line 139
    # getter for: Lorg/apache/poi/hssf/record/common/UnicodeString;->_logger:Lorg/apache/poi/util/POILogger;
    invoke-static {}, Lorg/apache/poi/hssf/record/common/UnicodeString;->access$1()Lorg/apache/poi/util/POILogger;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuilder;

    const-string/jumbo v9, "Warning - ExtRst has wrong magic marker, expecting 1 but found "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-short v9, p0, Lorg/apache/poi/hssf/record/common/UnicodeString$ExtRst;->reserved:S

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, " - ignoring"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v10, v8}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;)V

    .line 141
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    add-int/lit8 v7, p2, -0x2

    if-lt v1, v7, :cond_2

    .line 145
    invoke-direct {p0}, Lorg/apache/poi/hssf/record/common/UnicodeString$ExtRst;->populateEmpty()V

    goto :goto_0

    .line 142
    :cond_2
    invoke-interface {p1}, Lorg/apache/poi/util/LittleEndianInput;->readByte()B

    .line 141
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 150
    .end local v1    # "i":I
    :cond_3
    invoke-interface {p1}, Lorg/apache/poi/util/LittleEndianInput;->readShort()S

    move-result v6

    .line 152
    .local v6, "stringDataSize":S
    invoke-interface {p1}, Lorg/apache/poi/util/LittleEndianInput;->readShort()S

    move-result v7

    iput-short v7, p0, Lorg/apache/poi/hssf/record/common/UnicodeString$ExtRst;->formattingFontIndex:S

    .line 153
    invoke-interface {p1}, Lorg/apache/poi/util/LittleEndianInput;->readShort()S

    move-result v7

    iput-short v7, p0, Lorg/apache/poi/hssf/record/common/UnicodeString$ExtRst;->formattingOptions:S

    .line 156
    invoke-interface {p1}, Lorg/apache/poi/util/LittleEndianInput;->readUShort()I

    move-result v7

    iput v7, p0, Lorg/apache/poi/hssf/record/common/UnicodeString$ExtRst;->numberOfRuns:I

    .line 157
    invoke-interface {p1}, Lorg/apache/poi/util/LittleEndianInput;->readShort()S

    move-result v2

    .line 160
    .local v2, "length1":S
    invoke-interface {p1}, Lorg/apache/poi/util/LittleEndianInput;->readShort()S

    move-result v3

    .line 162
    .local v3, "length2":S
    if-nez v2, :cond_4

    if-lez v3, :cond_4

    .line 163
    const/4 v3, 0x0

    .line 165
    :cond_4
    if-eq v2, v3, :cond_5

    .line 166
    new-instance v7, Ljava/lang/IllegalStateException;

    .line 167
    new-instance v8, Ljava/lang/StringBuilder;

    const-string/jumbo v9, "The two length fields of the Phonetic Text don\'t agree! "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 168
    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, " vs "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    .line 167
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 166
    invoke-direct {v7, v8}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 171
    :cond_5
    invoke-static {p1, v2}, Lorg/apache/poi/util/StringUtil;->readUnicodeLE(Lorg/apache/poi/util/LittleEndianInput;I)Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lorg/apache/poi/hssf/record/common/UnicodeString$ExtRst;->phoneticText:Ljava/lang/String;

    .line 173
    add-int/lit8 v7, v6, -0x4

    add-int/lit8 v7, v7, -0x6

    iget-object v8, p0, Lorg/apache/poi/hssf/record/common/UnicodeString$ExtRst;->phoneticText:Ljava/lang/String;

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    mul-int/lit8 v8, v8, 0x2

    sub-int v5, v7, v8

    .line 174
    .local v5, "runData":I
    div-int/lit8 v4, v5, 0x6

    .line 175
    .local v4, "numRuns":I
    new-array v7, v4, [Lorg/apache/poi/hssf/record/common/UnicodeString$PhRun;

    iput-object v7, p0, Lorg/apache/poi/hssf/record/common/UnicodeString$ExtRst;->phRuns:[Lorg/apache/poi/hssf/record/common/UnicodeString$PhRun;

    .line 176
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_2
    iget-object v7, p0, Lorg/apache/poi/hssf/record/common/UnicodeString$ExtRst;->phRuns:[Lorg/apache/poi/hssf/record/common/UnicodeString$PhRun;

    array-length v7, v7

    if-lt v1, v7, :cond_7

    .line 180
    mul-int/lit8 v7, v4, 0x6

    sub-int v0, v5, v7

    .line 181
    .local v0, "extraDataLength":I
    if-gez v0, :cond_6

    .line 182
    # getter for: Lorg/apache/poi/hssf/record/common/UnicodeString;->_logger:Lorg/apache/poi/util/POILogger;
    invoke-static {}, Lorg/apache/poi/hssf/record/common/UnicodeString;->access$1()Lorg/apache/poi/util/POILogger;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuilder;

    const-string/jumbo v9, "Warning - ExtRst overran by "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    rsub-int/lit8 v9, v0, 0x0

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, " bytes"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v10, v8}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;)V

    .line 183
    const/4 v0, 0x0

    .line 185
    :cond_6
    new-array v7, v0, [B

    iput-object v7, p0, Lorg/apache/poi/hssf/record/common/UnicodeString$ExtRst;->extraData:[B

    .line 186
    const/4 v1, 0x0

    :goto_3
    iget-object v7, p0, Lorg/apache/poi/hssf/record/common/UnicodeString$ExtRst;->extraData:[B

    array-length v7, v7

    if-ge v1, v7, :cond_0

    .line 187
    iget-object v7, p0, Lorg/apache/poi/hssf/record/common/UnicodeString$ExtRst;->extraData:[B

    invoke-interface {p1}, Lorg/apache/poi/util/LittleEndianInput;->readByte()B

    move-result v8

    aput-byte v8, v7, v1

    .line 186
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 177
    .end local v0    # "extraDataLength":I
    :cond_7
    iget-object v7, p0, Lorg/apache/poi/hssf/record/common/UnicodeString$ExtRst;->phRuns:[Lorg/apache/poi/hssf/record/common/UnicodeString$PhRun;

    new-instance v8, Lorg/apache/poi/hssf/record/common/UnicodeString$PhRun;

    const/4 v9, 0x0

    invoke-direct {v8, p1, v9}, Lorg/apache/poi/hssf/record/common/UnicodeString$PhRun;-><init>(Lorg/apache/poi/util/LittleEndianInput;Lorg/apache/poi/hssf/record/common/UnicodeString$PhRun;)V

    aput-object v8, v7, v1

    .line 176
    add-int/lit8 v1, v1, 0x1

    goto :goto_2
.end method

.method private populateEmpty()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 119
    const/4 v0, 0x1

    iput-short v0, p0, Lorg/apache/poi/hssf/record/common/UnicodeString$ExtRst;->reserved:S

    .line 120
    const-string/jumbo v0, ""

    iput-object v0, p0, Lorg/apache/poi/hssf/record/common/UnicodeString$ExtRst;->phoneticText:Ljava/lang/String;

    .line 121
    new-array v0, v1, [Lorg/apache/poi/hssf/record/common/UnicodeString$PhRun;

    iput-object v0, p0, Lorg/apache/poi/hssf/record/common/UnicodeString$ExtRst;->phRuns:[Lorg/apache/poi/hssf/record/common/UnicodeString$PhRun;

    .line 122
    new-array v0, v1, [B

    iput-object v0, p0, Lorg/apache/poi/hssf/record/common/UnicodeString$ExtRst;->extraData:[B

    .line 123
    return-void
.end method


# virtual methods
.method protected bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/poi/hssf/record/common/UnicodeString$ExtRst;->clone()Lorg/apache/poi/hssf/record/common/UnicodeString$ExtRst;

    move-result-object v0

    return-object v0
.end method

.method protected clone()Lorg/apache/poi/hssf/record/common/UnicodeString$ExtRst;
    .locals 7

    .prologue
    .line 263
    new-instance v0, Lorg/apache/poi/hssf/record/common/UnicodeString$ExtRst;

    invoke-direct {v0}, Lorg/apache/poi/hssf/record/common/UnicodeString$ExtRst;-><init>()V

    .line 264
    .local v0, "ext":Lorg/apache/poi/hssf/record/common/UnicodeString$ExtRst;
    iget-short v2, p0, Lorg/apache/poi/hssf/record/common/UnicodeString$ExtRst;->reserved:S

    iput-short v2, v0, Lorg/apache/poi/hssf/record/common/UnicodeString$ExtRst;->reserved:S

    .line 265
    iget-short v2, p0, Lorg/apache/poi/hssf/record/common/UnicodeString$ExtRst;->formattingFontIndex:S

    iput-short v2, v0, Lorg/apache/poi/hssf/record/common/UnicodeString$ExtRst;->formattingFontIndex:S

    .line 266
    iget-short v2, p0, Lorg/apache/poi/hssf/record/common/UnicodeString$ExtRst;->formattingOptions:S

    iput-short v2, v0, Lorg/apache/poi/hssf/record/common/UnicodeString$ExtRst;->formattingOptions:S

    .line 267
    iget v2, p0, Lorg/apache/poi/hssf/record/common/UnicodeString$ExtRst;->numberOfRuns:I

    iput v2, v0, Lorg/apache/poi/hssf/record/common/UnicodeString$ExtRst;->numberOfRuns:I

    .line 268
    iget-object v2, p0, Lorg/apache/poi/hssf/record/common/UnicodeString$ExtRst;->phoneticText:Ljava/lang/String;

    iput-object v2, v0, Lorg/apache/poi/hssf/record/common/UnicodeString$ExtRst;->phoneticText:Ljava/lang/String;

    .line 269
    iget-object v2, p0, Lorg/apache/poi/hssf/record/common/UnicodeString$ExtRst;->phRuns:[Lorg/apache/poi/hssf/record/common/UnicodeString$PhRun;

    array-length v2, v2

    new-array v2, v2, [Lorg/apache/poi/hssf/record/common/UnicodeString$PhRun;

    iput-object v2, v0, Lorg/apache/poi/hssf/record/common/UnicodeString$ExtRst;->phRuns:[Lorg/apache/poi/hssf/record/common/UnicodeString$PhRun;

    .line 270
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, v0, Lorg/apache/poi/hssf/record/common/UnicodeString$ExtRst;->phRuns:[Lorg/apache/poi/hssf/record/common/UnicodeString$PhRun;

    array-length v2, v2

    if-lt v1, v2, :cond_0

    .line 277
    return-object v0

    .line 271
    :cond_0
    iget-object v2, v0, Lorg/apache/poi/hssf/record/common/UnicodeString$ExtRst;->phRuns:[Lorg/apache/poi/hssf/record/common/UnicodeString$PhRun;

    new-instance v3, Lorg/apache/poi/hssf/record/common/UnicodeString$PhRun;

    .line 272
    iget-object v4, p0, Lorg/apache/poi/hssf/record/common/UnicodeString$ExtRst;->phRuns:[Lorg/apache/poi/hssf/record/common/UnicodeString$PhRun;

    aget-object v4, v4, v1

    # getter for: Lorg/apache/poi/hssf/record/common/UnicodeString$PhRun;->phoneticTextFirstCharacterOffset:I
    invoke-static {v4}, Lorg/apache/poi/hssf/record/common/UnicodeString$PhRun;->access$2(Lorg/apache/poi/hssf/record/common/UnicodeString$PhRun;)I

    move-result v4

    .line 273
    iget-object v5, p0, Lorg/apache/poi/hssf/record/common/UnicodeString$ExtRst;->phRuns:[Lorg/apache/poi/hssf/record/common/UnicodeString$PhRun;

    aget-object v5, v5, v1

    # getter for: Lorg/apache/poi/hssf/record/common/UnicodeString$PhRun;->realTextFirstCharacterOffset:I
    invoke-static {v5}, Lorg/apache/poi/hssf/record/common/UnicodeString$PhRun;->access$3(Lorg/apache/poi/hssf/record/common/UnicodeString$PhRun;)I

    move-result v5

    .line 274
    iget-object v6, p0, Lorg/apache/poi/hssf/record/common/UnicodeString$ExtRst;->phRuns:[Lorg/apache/poi/hssf/record/common/UnicodeString$PhRun;

    aget-object v6, v6, v1

    # getter for: Lorg/apache/poi/hssf/record/common/UnicodeString$PhRun;->realTextLength:I
    invoke-static {v6}, Lorg/apache/poi/hssf/record/common/UnicodeString$PhRun;->access$4(Lorg/apache/poi/hssf/record/common/UnicodeString$PhRun;)I

    move-result v6

    invoke-direct {v3, v4, v5, v6}, Lorg/apache/poi/hssf/record/common/UnicodeString$PhRun;-><init>(III)V

    .line 271
    aput-object v3, v2, v1

    .line 270
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 1
    check-cast p1, Lorg/apache/poi/hssf/record/common/UnicodeString$ExtRst;

    invoke-virtual {p0, p1}, Lorg/apache/poi/hssf/record/common/UnicodeString$ExtRst;->compareTo(Lorg/apache/poi/hssf/record/common/UnicodeString$ExtRst;)I

    move-result v0

    return v0
.end method

.method public compareTo(Lorg/apache/poi/hssf/record/common/UnicodeString$ExtRst;)I
    .locals 4
    .param p1, "o"    # Lorg/apache/poi/hssf/record/common/UnicodeString$ExtRst;

    .prologue
    .line 232
    iget-short v2, p0, Lorg/apache/poi/hssf/record/common/UnicodeString$ExtRst;->reserved:S

    iget-short v3, p1, Lorg/apache/poi/hssf/record/common/UnicodeString$ExtRst;->reserved:S

    sub-int v1, v2, v3

    .line 233
    .local v1, "result":I
    if-eqz v1, :cond_0

    move v2, v1

    .line 259
    :goto_0
    return v2

    .line 234
    :cond_0
    iget-short v2, p0, Lorg/apache/poi/hssf/record/common/UnicodeString$ExtRst;->formattingFontIndex:S

    iget-short v3, p1, Lorg/apache/poi/hssf/record/common/UnicodeString$ExtRst;->formattingFontIndex:S

    sub-int v1, v2, v3

    .line 235
    if-eqz v1, :cond_1

    move v2, v1

    goto :goto_0

    .line 236
    :cond_1
    iget-short v2, p0, Lorg/apache/poi/hssf/record/common/UnicodeString$ExtRst;->formattingOptions:S

    iget-short v3, p1, Lorg/apache/poi/hssf/record/common/UnicodeString$ExtRst;->formattingOptions:S

    sub-int v1, v2, v3

    .line 237
    if-eqz v1, :cond_2

    move v2, v1

    goto :goto_0

    .line 238
    :cond_2
    iget v2, p0, Lorg/apache/poi/hssf/record/common/UnicodeString$ExtRst;->numberOfRuns:I

    iget v3, p1, Lorg/apache/poi/hssf/record/common/UnicodeString$ExtRst;->numberOfRuns:I

    sub-int v1, v2, v3

    .line 239
    if-eqz v1, :cond_3

    move v2, v1

    goto :goto_0

    .line 241
    :cond_3
    iget-object v2, p0, Lorg/apache/poi/hssf/record/common/UnicodeString$ExtRst;->phoneticText:Ljava/lang/String;

    iget-object v3, p1, Lorg/apache/poi/hssf/record/common/UnicodeString$ExtRst;->phoneticText:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    .line 242
    if-eqz v1, :cond_4

    move v2, v1

    goto :goto_0

    .line 244
    :cond_4
    iget-object v2, p0, Lorg/apache/poi/hssf/record/common/UnicodeString$ExtRst;->phRuns:[Lorg/apache/poi/hssf/record/common/UnicodeString$PhRun;

    array-length v2, v2

    iget-object v3, p1, Lorg/apache/poi/hssf/record/common/UnicodeString$ExtRst;->phRuns:[Lorg/apache/poi/hssf/record/common/UnicodeString$PhRun;

    array-length v3, v3

    sub-int v1, v2, v3

    .line 245
    if-eqz v1, :cond_5

    move v2, v1

    goto :goto_0

    .line 246
    :cond_5
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget-object v2, p0, Lorg/apache/poi/hssf/record/common/UnicodeString$ExtRst;->phRuns:[Lorg/apache/poi/hssf/record/common/UnicodeString$PhRun;

    array-length v2, v2

    if-lt v0, v2, :cond_6

    .line 255
    iget-object v2, p0, Lorg/apache/poi/hssf/record/common/UnicodeString$ExtRst;->extraData:[B

    array-length v2, v2

    iget-object v3, p1, Lorg/apache/poi/hssf/record/common/UnicodeString$ExtRst;->extraData:[B

    array-length v3, v3

    sub-int v1, v2, v3

    .line 256
    if-eqz v1, :cond_a

    move v2, v1

    goto :goto_0

    .line 247
    :cond_6
    iget-object v2, p0, Lorg/apache/poi/hssf/record/common/UnicodeString$ExtRst;->phRuns:[Lorg/apache/poi/hssf/record/common/UnicodeString$PhRun;

    aget-object v2, v2, v0

    # getter for: Lorg/apache/poi/hssf/record/common/UnicodeString$PhRun;->phoneticTextFirstCharacterOffset:I
    invoke-static {v2}, Lorg/apache/poi/hssf/record/common/UnicodeString$PhRun;->access$2(Lorg/apache/poi/hssf/record/common/UnicodeString$PhRun;)I

    move-result v2

    iget-object v3, p1, Lorg/apache/poi/hssf/record/common/UnicodeString$ExtRst;->phRuns:[Lorg/apache/poi/hssf/record/common/UnicodeString$PhRun;

    aget-object v3, v3, v0

    # getter for: Lorg/apache/poi/hssf/record/common/UnicodeString$PhRun;->phoneticTextFirstCharacterOffset:I
    invoke-static {v3}, Lorg/apache/poi/hssf/record/common/UnicodeString$PhRun;->access$2(Lorg/apache/poi/hssf/record/common/UnicodeString$PhRun;)I

    move-result v3

    sub-int v1, v2, v3

    .line 248
    if-eqz v1, :cond_7

    move v2, v1

    goto :goto_0

    .line 249
    :cond_7
    iget-object v2, p0, Lorg/apache/poi/hssf/record/common/UnicodeString$ExtRst;->phRuns:[Lorg/apache/poi/hssf/record/common/UnicodeString$PhRun;

    aget-object v2, v2, v0

    # getter for: Lorg/apache/poi/hssf/record/common/UnicodeString$PhRun;->realTextFirstCharacterOffset:I
    invoke-static {v2}, Lorg/apache/poi/hssf/record/common/UnicodeString$PhRun;->access$3(Lorg/apache/poi/hssf/record/common/UnicodeString$PhRun;)I

    move-result v2

    iget-object v3, p1, Lorg/apache/poi/hssf/record/common/UnicodeString$ExtRst;->phRuns:[Lorg/apache/poi/hssf/record/common/UnicodeString$PhRun;

    aget-object v3, v3, v0

    # getter for: Lorg/apache/poi/hssf/record/common/UnicodeString$PhRun;->realTextFirstCharacterOffset:I
    invoke-static {v3}, Lorg/apache/poi/hssf/record/common/UnicodeString$PhRun;->access$3(Lorg/apache/poi/hssf/record/common/UnicodeString$PhRun;)I

    move-result v3

    sub-int v1, v2, v3

    .line 250
    if-eqz v1, :cond_8

    move v2, v1

    goto :goto_0

    .line 251
    :cond_8
    iget-object v2, p0, Lorg/apache/poi/hssf/record/common/UnicodeString$ExtRst;->phRuns:[Lorg/apache/poi/hssf/record/common/UnicodeString$PhRun;

    aget-object v2, v2, v0

    # getter for: Lorg/apache/poi/hssf/record/common/UnicodeString$PhRun;->realTextFirstCharacterOffset:I
    invoke-static {v2}, Lorg/apache/poi/hssf/record/common/UnicodeString$PhRun;->access$3(Lorg/apache/poi/hssf/record/common/UnicodeString$PhRun;)I

    move-result v2

    iget-object v3, p1, Lorg/apache/poi/hssf/record/common/UnicodeString$ExtRst;->phRuns:[Lorg/apache/poi/hssf/record/common/UnicodeString$PhRun;

    aget-object v3, v3, v0

    # getter for: Lorg/apache/poi/hssf/record/common/UnicodeString$PhRun;->realTextLength:I
    invoke-static {v3}, Lorg/apache/poi/hssf/record/common/UnicodeString$PhRun;->access$4(Lorg/apache/poi/hssf/record/common/UnicodeString$PhRun;)I

    move-result v3

    sub-int v1, v2, v3

    .line 252
    if-eqz v1, :cond_9

    move v2, v1

    goto/16 :goto_0

    .line 246
    :cond_9
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 259
    :cond_a
    const/4 v2, 0x0

    goto/16 :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 223
    instance-of v2, p1, Lorg/apache/poi/hssf/record/common/UnicodeString$ExtRst;

    if-nez v2, :cond_1

    .line 227
    :cond_0
    :goto_0
    return v1

    :cond_1
    move-object v0, p1

    .line 226
    check-cast v0, Lorg/apache/poi/hssf/record/common/UnicodeString$ExtRst;

    .line 227
    .local v0, "other":Lorg/apache/poi/hssf/record/common/UnicodeString$ExtRst;
    invoke-virtual {p0, v0}, Lorg/apache/poi/hssf/record/common/UnicodeString$ExtRst;->compareTo(Lorg/apache/poi/hssf/record/common/UnicodeString$ExtRst;)I

    move-result v2

    if-nez v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method protected getDataSize()I
    .locals 2

    .prologue
    .line 195
    iget-object v0, p0, Lorg/apache/poi/hssf/record/common/UnicodeString$ExtRst;->phoneticText:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    mul-int/lit8 v0, v0, 0x2

    add-int/lit8 v0, v0, 0xa

    .line 196
    iget-object v1, p0, Lorg/apache/poi/hssf/record/common/UnicodeString$ExtRst;->phRuns:[Lorg/apache/poi/hssf/record/common/UnicodeString$PhRun;

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x6

    .line 195
    add-int/2addr v0, v1

    .line 196
    iget-object v1, p0, Lorg/apache/poi/hssf/record/common/UnicodeString$ExtRst;->extraData:[B

    array-length v1, v1

    .line 195
    add-int/2addr v0, v1

    return v0
.end method

.method public getFormattingFontIndex()S
    .locals 1

    .prologue
    .line 281
    iget-short v0, p0, Lorg/apache/poi/hssf/record/common/UnicodeString$ExtRst;->formattingFontIndex:S

    return v0
.end method

.method public getFormattingOptions()S
    .locals 1

    .prologue
    .line 284
    iget-short v0, p0, Lorg/apache/poi/hssf/record/common/UnicodeString$ExtRst;->formattingOptions:S

    return v0
.end method

.method public getNumberOfRuns()I
    .locals 1

    .prologue
    .line 287
    iget v0, p0, Lorg/apache/poi/hssf/record/common/UnicodeString$ExtRst;->numberOfRuns:I

    return v0
.end method

.method public getPhRuns()[Lorg/apache/poi/hssf/record/common/UnicodeString$PhRun;
    .locals 1

    .prologue
    .line 293
    iget-object v0, p0, Lorg/apache/poi/hssf/record/common/UnicodeString$ExtRst;->phRuns:[Lorg/apache/poi/hssf/record/common/UnicodeString$PhRun;

    return-object v0
.end method

.method public getPhoneticText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 290
    iget-object v0, p0, Lorg/apache/poi/hssf/record/common/UnicodeString$ExtRst;->phoneticText:Ljava/lang/String;

    return-object v0
.end method

.method protected serialize(Lorg/apache/poi/hssf/record/cont/ContinuableRecordOutput;)V
    .locals 3
    .param p1, "out"    # Lorg/apache/poi/hssf/record/cont/ContinuableRecordOutput;

    .prologue
    .line 199
    invoke-virtual {p0}, Lorg/apache/poi/hssf/record/common/UnicodeString$ExtRst;->getDataSize()I

    move-result v0

    .line 201
    .local v0, "dataSize":I
    const/16 v2, 0x8

    invoke-virtual {p1, v2}, Lorg/apache/poi/hssf/record/cont/ContinuableRecordOutput;->writeContinueIfRequired(I)V

    .line 202
    iget-short v2, p0, Lorg/apache/poi/hssf/record/common/UnicodeString$ExtRst;->reserved:S

    invoke-virtual {p1, v2}, Lorg/apache/poi/hssf/record/cont/ContinuableRecordOutput;->writeShort(I)V

    .line 203
    invoke-virtual {p1, v0}, Lorg/apache/poi/hssf/record/cont/ContinuableRecordOutput;->writeShort(I)V

    .line 204
    iget-short v2, p0, Lorg/apache/poi/hssf/record/common/UnicodeString$ExtRst;->formattingFontIndex:S

    invoke-virtual {p1, v2}, Lorg/apache/poi/hssf/record/cont/ContinuableRecordOutput;->writeShort(I)V

    .line 205
    iget-short v2, p0, Lorg/apache/poi/hssf/record/common/UnicodeString$ExtRst;->formattingOptions:S

    invoke-virtual {p1, v2}, Lorg/apache/poi/hssf/record/cont/ContinuableRecordOutput;->writeShort(I)V

    .line 207
    const/4 v2, 0x6

    invoke-virtual {p1, v2}, Lorg/apache/poi/hssf/record/cont/ContinuableRecordOutput;->writeContinueIfRequired(I)V

    .line 208
    iget v2, p0, Lorg/apache/poi/hssf/record/common/UnicodeString$ExtRst;->numberOfRuns:I

    invoke-virtual {p1, v2}, Lorg/apache/poi/hssf/record/cont/ContinuableRecordOutput;->writeShort(I)V

    .line 209
    iget-object v2, p0, Lorg/apache/poi/hssf/record/common/UnicodeString$ExtRst;->phoneticText:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {p1, v2}, Lorg/apache/poi/hssf/record/cont/ContinuableRecordOutput;->writeShort(I)V

    .line 210
    iget-object v2, p0, Lorg/apache/poi/hssf/record/common/UnicodeString$ExtRst;->phoneticText:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {p1, v2}, Lorg/apache/poi/hssf/record/cont/ContinuableRecordOutput;->writeShort(I)V

    .line 212
    iget-object v2, p0, Lorg/apache/poi/hssf/record/common/UnicodeString$ExtRst;->phoneticText:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    mul-int/lit8 v2, v2, 0x2

    invoke-virtual {p1, v2}, Lorg/apache/poi/hssf/record/cont/ContinuableRecordOutput;->writeContinueIfRequired(I)V

    .line 213
    iget-object v2, p0, Lorg/apache/poi/hssf/record/common/UnicodeString$ExtRst;->phoneticText:Ljava/lang/String;

    invoke-static {v2, p1}, Lorg/apache/poi/util/StringUtil;->putUnicodeLE(Ljava/lang/String;Lorg/apache/poi/util/LittleEndianOutput;)V

    .line 215
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lorg/apache/poi/hssf/record/common/UnicodeString$ExtRst;->phRuns:[Lorg/apache/poi/hssf/record/common/UnicodeString$PhRun;

    array-length v2, v2

    if-lt v1, v2, :cond_0

    .line 219
    iget-object v2, p0, Lorg/apache/poi/hssf/record/common/UnicodeString$ExtRst;->extraData:[B

    invoke-virtual {p1, v2}, Lorg/apache/poi/hssf/record/cont/ContinuableRecordOutput;->write([B)V

    .line 220
    return-void

    .line 216
    :cond_0
    iget-object v2, p0, Lorg/apache/poi/hssf/record/common/UnicodeString$ExtRst;->phRuns:[Lorg/apache/poi/hssf/record/common/UnicodeString$PhRun;

    aget-object v2, v2, v1

    # invokes: Lorg/apache/poi/hssf/record/common/UnicodeString$PhRun;->serialize(Lorg/apache/poi/hssf/record/cont/ContinuableRecordOutput;)V
    invoke-static {v2, p1}, Lorg/apache/poi/hssf/record/common/UnicodeString$PhRun;->access$1(Lorg/apache/poi/hssf/record/common/UnicodeString$PhRun;Lorg/apache/poi/hssf/record/cont/ContinuableRecordOutput;)V

    .line 215
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method
