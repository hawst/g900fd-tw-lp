.class public final Lorg/apache/poi/hssf/util/HSSFColor$PALE_BLUE;
.super Lorg/apache/poi/hssf/util/HSSFColor;
.source "HSSFColor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/poi/hssf/util/HSSFColor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PALE_BLUE"
.end annotation


# static fields
.field public static final hexString:Ljava/lang/String; = "9999:CCCC:FFFF"

.field public static final index:S = 0x2cs

.field public static final triplet:[S


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1379
    const/4 v0, 0x3

    new-array v0, v0, [S

    fill-array-data v0, :array_0

    .line 1378
    sput-object v0, Lorg/apache/poi/hssf/util/HSSFColor$PALE_BLUE;->triplet:[S

    .line 1382
    return-void

    .line 1379
    nop

    :array_0
    .array-data 2
        0x99s
        0xccs
        0xffs
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1374
    invoke-direct {p0}, Lorg/apache/poi/hssf/util/HSSFColor;-><init>()V

    return-void
.end method


# virtual methods
.method public getHexString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1396
    const-string/jumbo v0, "9999:CCCC:FFFF"

    return-object v0
.end method

.method public getIndex()S
    .locals 1

    .prologue
    .line 1386
    const/16 v0, 0x2c

    return v0
.end method

.method public getTriplet()[S
    .locals 1

    .prologue
    .line 1391
    sget-object v0, Lorg/apache/poi/hssf/util/HSSFColor$PALE_BLUE;->triplet:[S

    return-object v0
.end method
