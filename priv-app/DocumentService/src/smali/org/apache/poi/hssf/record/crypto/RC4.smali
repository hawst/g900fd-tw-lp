.class final Lorg/apache/poi/hssf/record/crypto/RC4;
.super Ljava/lang/Object;
.source "RC4.java"


# instance fields
.field private _i:I

.field private _j:I

.field private final _s:[B


# direct methods
.method public constructor <init>([B)V
    .locals 8
    .param p1, "key"    # [B

    .prologue
    const/4 v7, 0x0

    const/16 v6, 0x100

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    new-array v4, v6, [B

    iput-object v4, p0, Lorg/apache/poi/hssf/record/crypto/RC4;->_s:[B

    .line 35
    array-length v2, p1

    .line 37
    .local v2, "key_length":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-lt v0, v6, :cond_0

    .line 40
    const/4 v0, 0x0

    const/4 v1, 0x0

    .local v1, "j":I
    :goto_1
    if-lt v0, v6, :cond_1

    .line 49
    iput v7, p0, Lorg/apache/poi/hssf/record/crypto/RC4;->_i:I

    .line 50
    iput v7, p0, Lorg/apache/poi/hssf/record/crypto/RC4;->_j:I

    .line 51
    return-void

    .line 38
    .end local v1    # "j":I
    :cond_0
    iget-object v4, p0, Lorg/apache/poi/hssf/record/crypto/RC4;->_s:[B

    int-to-byte v5, v0

    aput-byte v5, v4, v0

    .line 37
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 43
    .restart local v1    # "j":I
    :cond_1
    rem-int v4, v0, v2

    aget-byte v4, p1, v4

    add-int/2addr v4, v1

    iget-object v5, p0, Lorg/apache/poi/hssf/record/crypto/RC4;->_s:[B

    aget-byte v5, v5, v0

    add-int/2addr v4, v5

    and-int/lit16 v1, v4, 0xff

    .line 44
    iget-object v4, p0, Lorg/apache/poi/hssf/record/crypto/RC4;->_s:[B

    aget-byte v3, v4, v0

    .line 45
    .local v3, "temp":B
    iget-object v4, p0, Lorg/apache/poi/hssf/record/crypto/RC4;->_s:[B

    iget-object v5, p0, Lorg/apache/poi/hssf/record/crypto/RC4;->_s:[B

    aget-byte v5, v5, v1

    aput-byte v5, v4, v0

    .line 46
    iget-object v4, p0, Lorg/apache/poi/hssf/record/crypto/RC4;->_s:[B

    aput-byte v3, v4, v1

    .line 40
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method


# virtual methods
.method public encrypt([B)V
    .locals 3
    .param p1, "in"    # [B

    .prologue
    .line 66
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v1, p1

    if-lt v0, v1, :cond_0

    .line 69
    return-void

    .line 67
    :cond_0
    aget-byte v1, p1, v0

    invoke-virtual {p0}, Lorg/apache/poi/hssf/record/crypto/RC4;->output()B

    move-result v2

    xor-int/2addr v1, v2

    int-to-byte v1, v1

    aput-byte v1, p1, v0

    .line 66
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public encrypt([BII)V
    .locals 4
    .param p1, "in"    # [B
    .param p2, "offset"    # I
    .param p3, "len"    # I

    .prologue
    .line 71
    add-int v0, p2, p3

    .line 72
    .local v0, "end":I
    move v1, p2

    .local v1, "i":I
    :goto_0
    if-lt v1, v0, :cond_0

    .line 76
    return-void

    .line 73
    :cond_0
    aget-byte v2, p1, v1

    invoke-virtual {p0}, Lorg/apache/poi/hssf/record/crypto/RC4;->output()B

    move-result v3

    xor-int/2addr v2, v3

    int-to-byte v2, v2

    aput-byte v2, p1, v1

    .line 72
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public output()B
    .locals 5

    .prologue
    .line 55
    iget v1, p0, Lorg/apache/poi/hssf/record/crypto/RC4;->_i:I

    add-int/lit8 v1, v1, 0x1

    and-int/lit16 v1, v1, 0xff

    iput v1, p0, Lorg/apache/poi/hssf/record/crypto/RC4;->_i:I

    .line 56
    iget v1, p0, Lorg/apache/poi/hssf/record/crypto/RC4;->_j:I

    iget-object v2, p0, Lorg/apache/poi/hssf/record/crypto/RC4;->_s:[B

    iget v3, p0, Lorg/apache/poi/hssf/record/crypto/RC4;->_i:I

    aget-byte v2, v2, v3

    add-int/2addr v1, v2

    and-int/lit16 v1, v1, 0xff

    iput v1, p0, Lorg/apache/poi/hssf/record/crypto/RC4;->_j:I

    .line 58
    iget-object v1, p0, Lorg/apache/poi/hssf/record/crypto/RC4;->_s:[B

    iget v2, p0, Lorg/apache/poi/hssf/record/crypto/RC4;->_i:I

    aget-byte v0, v1, v2

    .line 59
    .local v0, "temp":B
    iget-object v1, p0, Lorg/apache/poi/hssf/record/crypto/RC4;->_s:[B

    iget v2, p0, Lorg/apache/poi/hssf/record/crypto/RC4;->_i:I

    iget-object v3, p0, Lorg/apache/poi/hssf/record/crypto/RC4;->_s:[B

    iget v4, p0, Lorg/apache/poi/hssf/record/crypto/RC4;->_j:I

    aget-byte v3, v3, v4

    aput-byte v3, v1, v2

    .line 60
    iget-object v1, p0, Lorg/apache/poi/hssf/record/crypto/RC4;->_s:[B

    iget v2, p0, Lorg/apache/poi/hssf/record/crypto/RC4;->_j:I

    aput-byte v0, v1, v2

    .line 62
    iget-object v1, p0, Lorg/apache/poi/hssf/record/crypto/RC4;->_s:[B

    iget-object v2, p0, Lorg/apache/poi/hssf/record/crypto/RC4;->_s:[B

    iget v3, p0, Lorg/apache/poi/hssf/record/crypto/RC4;->_i:I

    aget-byte v2, v2, v3

    iget-object v3, p0, Lorg/apache/poi/hssf/record/crypto/RC4;->_s:[B

    iget v4, p0, Lorg/apache/poi/hssf/record/crypto/RC4;->_j:I

    aget-byte v3, v3, v4

    add-int/2addr v2, v3

    and-int/lit16 v2, v2, 0xff

    aget-byte v1, v1, v2

    return v1
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 79
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 81
    .local v0, "sb":Ljava/lang/StringBuffer;
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, " ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 82
    const-string/jumbo v1, "i="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget v2, p0, Lorg/apache/poi/hssf/record/crypto/RC4;->_i:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 83
    const-string/jumbo v1, " j="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget v2, p0, Lorg/apache/poi/hssf/record/crypto/RC4;->_j:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 84
    const-string/jumbo v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 85
    const-string/jumbo v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 86
    iget-object v1, p0, Lorg/apache/poi/hssf/record/crypto/RC4;->_s:[B

    const-wide/16 v2, 0x0

    const/4 v4, 0x0

    invoke-static {v1, v2, v3, v4}, Lorg/apache/poi/util/HexDump;->dump([BJI)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 88
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
