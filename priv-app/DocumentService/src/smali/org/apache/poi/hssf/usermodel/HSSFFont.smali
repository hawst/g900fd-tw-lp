.class public final Lorg/apache/poi/hssf/usermodel/HSSFFont;
.super Ljava/lang/Object;
.source "HSSFFont.java"

# interfaces
.implements Lorg/apache/poi/ss/usermodel/Font;


# static fields
.field public static final FONT_ARIAL:Ljava/lang/String; = "Arial"


# instance fields
.field private font:Lorg/apache/poi/hssf/record/FontRecord;

.field private index:S


# direct methods
.method protected constructor <init>(SLorg/apache/poi/hssf/record/FontRecord;)V
    .locals 0
    .param p1, "index"    # S
    .param p2, "rec"    # Lorg/apache/poi/hssf/record/FontRecord;

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    iput-object p2, p0, Lorg/apache/poi/hssf/usermodel/HSSFFont;->font:Lorg/apache/poi/hssf/record/FontRecord;

    .line 50
    iput-short p1, p0, Lorg/apache/poi/hssf/usermodel/HSSFFont;->index:S

    .line 51
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 345
    if-ne p0, p1, :cond_1

    .line 358
    :cond_0
    :goto_0
    return v1

    .line 346
    :cond_1
    if-nez p1, :cond_2

    move v1, v2

    goto :goto_0

    .line 347
    :cond_2
    instance-of v3, p1, Lorg/apache/poi/hssf/usermodel/HSSFFont;

    if-eqz v3, :cond_5

    move-object v0, p1

    .line 348
    check-cast v0, Lorg/apache/poi/hssf/usermodel/HSSFFont;

    .line 349
    .local v0, "other":Lorg/apache/poi/hssf/usermodel/HSSFFont;
    iget-object v3, p0, Lorg/apache/poi/hssf/usermodel/HSSFFont;->font:Lorg/apache/poi/hssf/record/FontRecord;

    if-nez v3, :cond_3

    .line 350
    iget-object v3, v0, Lorg/apache/poi/hssf/usermodel/HSSFFont;->font:Lorg/apache/poi/hssf/record/FontRecord;

    if-eqz v3, :cond_4

    move v1, v2

    .line 351
    goto :goto_0

    .line 352
    :cond_3
    iget-object v3, p0, Lorg/apache/poi/hssf/usermodel/HSSFFont;->font:Lorg/apache/poi/hssf/record/FontRecord;

    iget-object v4, v0, Lorg/apache/poi/hssf/usermodel/HSSFFont;->font:Lorg/apache/poi/hssf/record/FontRecord;

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    move v1, v2

    .line 353
    goto :goto_0

    .line 354
    :cond_4
    iget-short v3, p0, Lorg/apache/poi/hssf/usermodel/HSSFFont;->index:S

    iget-short v4, v0, Lorg/apache/poi/hssf/usermodel/HSSFFont;->index:S

    if-eq v3, v4, :cond_0

    move v1, v2

    .line 355
    goto :goto_0

    .end local v0    # "other":Lorg/apache/poi/hssf/usermodel/HSSFFont;
    :cond_5
    move v1, v2

    .line 358
    goto :goto_0
.end method

.method public getBoldweight()S
    .locals 1

    .prologue
    .line 226
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFFont;->font:Lorg/apache/poi/hssf/record/FontRecord;

    invoke-virtual {v0}, Lorg/apache/poi/hssf/record/FontRecord;->getBoldWeight()S

    move-result v0

    return v0
.end method

.method public getCharSet()I
    .locals 2

    .prologue
    .line 295
    iget-object v1, p0, Lorg/apache/poi/hssf/usermodel/HSSFFont;->font:Lorg/apache/poi/hssf/record/FontRecord;

    invoke-virtual {v1}, Lorg/apache/poi/hssf/record/FontRecord;->getCharset()B

    move-result v0

    .line 296
    .local v0, "charset":B
    if-ltz v0, :cond_0

    .line 299
    .end local v0    # "charset":B
    :goto_0
    return v0

    .restart local v0    # "charset":B
    :cond_0
    add-int/lit16 v0, v0, 0x100

    goto :goto_0
.end method

.method public getColor()S
    .locals 1

    .prologue
    .line 193
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFFont;->font:Lorg/apache/poi/hssf/record/FontRecord;

    invoke-virtual {v0}, Lorg/apache/poi/hssf/record/FontRecord;->getColorPaletteIndex()S

    move-result v0

    return v0
.end method

.method public getFontHeight()S
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFFont;->font:Lorg/apache/poi/hssf/record/FontRecord;

    invoke-virtual {v0}, Lorg/apache/poi/hssf/record/FontRecord;->getFontHeight()S

    move-result v0

    return v0
.end method

.method public getFontHeightInPoints()S
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFFont;->font:Lorg/apache/poi/hssf/record/FontRecord;

    invoke-virtual {v0}, Lorg/apache/poi/hssf/record/FontRecord;->getFontHeight()S

    move-result v0

    div-int/lit8 v0, v0, 0x14

    int-to-short v0, v0

    return v0
.end method

.method public getFontName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFFont;->font:Lorg/apache/poi/hssf/record/FontRecord;

    invoke-virtual {v0}, Lorg/apache/poi/hssf/record/FontRecord;->getFontName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getHSSFColor(Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;)Lorg/apache/poi/hssf/util/HSSFColor;
    .locals 2
    .param p1, "wb"    # Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;

    .prologue
    .line 201
    invoke-virtual {p1}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->getCustomPalette()Lorg/apache/poi/hssf/usermodel/HSSFPalette;

    move-result-object v0

    .line 202
    .local v0, "pallette":Lorg/apache/poi/hssf/usermodel/HSSFPalette;
    invoke-virtual {p0}, Lorg/apache/poi/hssf/usermodel/HSSFFont;->getColor()S

    move-result v1

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/usermodel/HSSFPalette;->getColor(S)Lorg/apache/poi/hssf/util/HSSFColor;

    move-result-object v1

    return-object v1
.end method

.method public getIndex()S
    .locals 1

    .prologue
    .line 83
    iget-short v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFFont;->index:S

    return v0
.end method

.method public getItalic()Z
    .locals 1

    .prologue
    .line 149
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFFont;->font:Lorg/apache/poi/hssf/record/FontRecord;

    invoke-virtual {v0}, Lorg/apache/poi/hssf/record/FontRecord;->isItalic()Z

    move-result v0

    return v0
.end method

.method public getStrikeout()Z
    .locals 1

    .prologue
    .line 169
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFFont;->font:Lorg/apache/poi/hssf/record/FontRecord;

    invoke-virtual {v0}, Lorg/apache/poi/hssf/record/FontRecord;->isStruckout()Z

    move-result v0

    return v0
.end method

.method public getTypeOffset()S
    .locals 1

    .prologue
    .line 252
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFFont;->font:Lorg/apache/poi/hssf/record/FontRecord;

    invoke-virtual {v0}, Lorg/apache/poi/hssf/record/FontRecord;->getSuperSubScript()S

    move-result v0

    return v0
.end method

.method public getUnderline()B
    .locals 1

    .prologue
    .line 282
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFFont;->font:Lorg/apache/poi/hssf/record/FontRecord;

    invoke-virtual {v0}, Lorg/apache/poi/hssf/record/FontRecord;->getUnderline()B

    move-result v0

    return v0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 337
    const/16 v0, 0x1f

    .line 338
    .local v0, "prime":I
    const/4 v1, 0x1

    .line 339
    .local v1, "result":I
    iget-object v2, p0, Lorg/apache/poi/hssf/usermodel/HSSFFont;->font:Lorg/apache/poi/hssf/record/FontRecord;

    if-nez v2, :cond_0

    const/4 v2, 0x0

    :goto_0
    add-int/lit8 v1, v2, 0x1f

    .line 340
    mul-int/lit8 v2, v1, 0x1f

    iget-short v3, p0, Lorg/apache/poi/hssf/usermodel/HSSFFont;->index:S

    add-int v1, v2, v3

    .line 341
    return v1

    .line 339
    :cond_0
    iget-object v2, p0, Lorg/apache/poi/hssf/usermodel/HSSFFont;->font:Lorg/apache/poi/hssf/record/FontRecord;

    invoke-virtual {v2}, Lorg/apache/poi/hssf/record/FontRecord;->hashCode()I

    move-result v2

    goto :goto_0
.end method

.method public setBoldweight(S)V
    .locals 1
    .param p1, "boldweight"    # S

    .prologue
    .line 214
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFFont;->font:Lorg/apache/poi/hssf/record/FontRecord;

    invoke-virtual {v0, p1}, Lorg/apache/poi/hssf/record/FontRecord;->setBoldWeight(S)V

    .line 215
    return-void
.end method

.method public setCharSet(B)V
    .locals 1
    .param p1, "charset"    # B

    .prologue
    .line 326
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFFont;->font:Lorg/apache/poi/hssf/record/FontRecord;

    invoke-virtual {v0, p1}, Lorg/apache/poi/hssf/record/FontRecord;->setCharset(B)V

    .line 327
    return-void
.end method

.method public setCharSet(I)V
    .locals 2
    .param p1, "charset"    # I

    .prologue
    .line 311
    int-to-byte v0, p1

    .line 312
    .local v0, "cs":B
    const/16 v1, 0x7f

    if-le p1, v1, :cond_0

    .line 313
    add-int/lit16 v1, p1, -0x100

    int-to-byte v0, v1

    .line 315
    :cond_0
    invoke-virtual {p0, v0}, Lorg/apache/poi/hssf/usermodel/HSSFFont;->setCharSet(B)V

    .line 316
    return-void
.end method

.method public setColor(S)V
    .locals 1
    .param p1, "color"    # S

    .prologue
    .line 181
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFFont;->font:Lorg/apache/poi/hssf/record/FontRecord;

    invoke-virtual {v0, p1}, Lorg/apache/poi/hssf/record/FontRecord;->setColorPaletteIndex(S)V

    .line 182
    return-void
.end method

.method public setFontHeight(S)V
    .locals 1
    .param p1, "height"    # S

    .prologue
    .line 95
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFFont;->font:Lorg/apache/poi/hssf/record/FontRecord;

    invoke-virtual {v0, p1}, Lorg/apache/poi/hssf/record/FontRecord;->setFontHeight(S)V

    .line 96
    return-void
.end method

.method public setFontHeightInPoints(S)V
    .locals 2
    .param p1, "height"    # S

    .prologue
    .line 106
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFFont;->font:Lorg/apache/poi/hssf/record/FontRecord;

    mul-int/lit8 v1, p1, 0x14

    int-to-short v1, v1

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/record/FontRecord;->setFontHeight(S)V

    .line 107
    return-void
.end method

.method public setFontName(Ljava/lang/String;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 61
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFFont;->font:Lorg/apache/poi/hssf/record/FontRecord;

    invoke-virtual {v0, p1}, Lorg/apache/poi/hssf/record/FontRecord;->setFontName(Ljava/lang/String;)V

    .line 62
    return-void
.end method

.method public setItalic(Z)V
    .locals 1
    .param p1, "italic"    # Z

    .prologue
    .line 139
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFFont;->font:Lorg/apache/poi/hssf/record/FontRecord;

    invoke-virtual {v0, p1}, Lorg/apache/poi/hssf/record/FontRecord;->setItalic(Z)V

    .line 140
    return-void
.end method

.method public setStrikeout(Z)V
    .locals 1
    .param p1, "strikeout"    # Z

    .prologue
    .line 159
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFFont;->font:Lorg/apache/poi/hssf/record/FontRecord;

    invoke-virtual {v0, p1}, Lorg/apache/poi/hssf/record/FontRecord;->setStrikeout(Z)V

    .line 160
    return-void
.end method

.method public setTypeOffset(S)V
    .locals 1
    .param p1, "offset"    # S

    .prologue
    .line 239
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFFont;->font:Lorg/apache/poi/hssf/record/FontRecord;

    invoke-virtual {v0, p1}, Lorg/apache/poi/hssf/record/FontRecord;->setSuperSubScript(S)V

    .line 240
    return-void
.end method

.method public setUnderline(B)V
    .locals 1
    .param p1, "underline"    # B

    .prologue
    .line 267
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFFont;->font:Lorg/apache/poi/hssf/record/FontRecord;

    invoke-virtual {v0, p1}, Lorg/apache/poi/hssf/record/FontRecord;->setUnderline(B)V

    .line 268
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 331
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "org.apache.poi.hssf.usermodel.HSSFFont{"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 332
    iget-object v1, p0, Lorg/apache/poi/hssf/usermodel/HSSFFont;->font:Lorg/apache/poi/hssf/record/FontRecord;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 333
    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 331
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
