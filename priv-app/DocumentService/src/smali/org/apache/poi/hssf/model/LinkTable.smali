.class final Lorg/apache/poi/hssf/model/LinkTable;
.super Ljava/lang/Object;
.source "LinkTable.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/poi/hssf/model/LinkTable$CRNBlock;,
        Lorg/apache/poi/hssf/model/LinkTable$ExternalBookBlock;
    }
.end annotation


# instance fields
.field private final _definedNames:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/apache/poi/hssf/record/NameRecord;",
            ">;"
        }
    .end annotation
.end field

.field private final _externSheetRecord:Lorg/apache/poi/hssf/record/ExternSheetRecord;

.field private _externalBookBlocks:[Lorg/apache/poi/hssf/model/LinkTable$ExternalBookBlock;

.field private final _recordCount:I

.field private final _workbookRecordList:Lorg/apache/poi/hssf/model/WorkbookRecordList;


# direct methods
.method public constructor <init>(ILorg/apache/poi/hssf/model/WorkbookRecordList;)V
    .locals 5
    .param p1, "numberOfSheets"    # I
    .param p2, "workbookRecordList"    # Lorg/apache/poi/hssf/model/WorkbookRecordList;

    .prologue
    const/4 v4, 0x0

    .line 250
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 251
    iput-object p2, p0, Lorg/apache/poi/hssf/model/LinkTable;->_workbookRecordList:Lorg/apache/poi/hssf/model/WorkbookRecordList;

    .line 252
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lorg/apache/poi/hssf/model/LinkTable;->_definedNames:Ljava/util/List;

    .line 253
    const/4 v2, 0x1

    new-array v2, v2, [Lorg/apache/poi/hssf/model/LinkTable$ExternalBookBlock;

    .line 254
    new-instance v3, Lorg/apache/poi/hssf/model/LinkTable$ExternalBookBlock;

    invoke-direct {v3, p1}, Lorg/apache/poi/hssf/model/LinkTable$ExternalBookBlock;-><init>(I)V

    aput-object v3, v2, v4

    .line 253
    iput-object v2, p0, Lorg/apache/poi/hssf/model/LinkTable;->_externalBookBlocks:[Lorg/apache/poi/hssf/model/LinkTable$ExternalBookBlock;

    .line 256
    new-instance v2, Lorg/apache/poi/hssf/record/ExternSheetRecord;

    invoke-direct {v2}, Lorg/apache/poi/hssf/record/ExternSheetRecord;-><init>()V

    iput-object v2, p0, Lorg/apache/poi/hssf/model/LinkTable;->_externSheetRecord:Lorg/apache/poi/hssf/record/ExternSheetRecord;

    .line 257
    const/4 v2, 0x2

    iput v2, p0, Lorg/apache/poi/hssf/model/LinkTable;->_recordCount:I

    .line 261
    iget-object v2, p0, Lorg/apache/poi/hssf/model/LinkTable;->_externalBookBlocks:[Lorg/apache/poi/hssf/model/LinkTable$ExternalBookBlock;

    aget-object v2, v2, v4

    invoke-virtual {v2}, Lorg/apache/poi/hssf/model/LinkTable$ExternalBookBlock;->getExternalBookRecord()Lorg/apache/poi/hssf/record/SupBookRecord;

    move-result-object v1

    .line 263
    .local v1, "supbook":Lorg/apache/poi/hssf/record/SupBookRecord;
    const/16 v2, 0x8c

    invoke-direct {p0, v2}, Lorg/apache/poi/hssf/model/LinkTable;->findFirstRecordLocBySid(S)I

    move-result v0

    .line 264
    .local v0, "idx":I
    if-gez v0, :cond_0

    .line 265
    new-instance v2, Ljava/lang/RuntimeException;

    const-string/jumbo v3, "CountryRecord not found"

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 267
    :cond_0
    iget-object v2, p0, Lorg/apache/poi/hssf/model/LinkTable;->_workbookRecordList:Lorg/apache/poi/hssf/model/WorkbookRecordList;

    add-int/lit8 v3, v0, 0x1

    iget-object v4, p0, Lorg/apache/poi/hssf/model/LinkTable;->_externSheetRecord:Lorg/apache/poi/hssf/record/ExternSheetRecord;

    invoke-virtual {v2, v3, v4}, Lorg/apache/poi/hssf/model/WorkbookRecordList;->add(ILorg/apache/poi/hssf/record/Record;)V

    .line 268
    iget-object v2, p0, Lorg/apache/poi/hssf/model/LinkTable;->_workbookRecordList:Lorg/apache/poi/hssf/model/WorkbookRecordList;

    add-int/lit8 v3, v0, 0x1

    invoke-virtual {v2, v3, v1}, Lorg/apache/poi/hssf/model/WorkbookRecordList;->add(ILorg/apache/poi/hssf/record/Record;)V

    .line 269
    return-void
.end method

.method public constructor <init>(Ljava/util/List;ILorg/apache/poi/hssf/model/WorkbookRecordList;Ljava/util/Map;)V
    .locals 8
    .param p1, "inputList"    # Ljava/util/List;
    .param p2, "startIndex"    # I
    .param p3, "workbookRecordList"    # Lorg/apache/poi/hssf/model/WorkbookRecordList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List;",
            "I",
            "Lorg/apache/poi/hssf/model/WorkbookRecordList;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lorg/apache/poi/hssf/record/NameCommentRecord;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p4, "commentRecords":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lorg/apache/poi/hssf/record/NameCommentRecord;>;"
    const/4 v7, 0x0

    .line 180
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 182
    iput-object p3, p0, Lorg/apache/poi/hssf/model/LinkTable;->_workbookRecordList:Lorg/apache/poi/hssf/model/WorkbookRecordList;

    .line 183
    new-instance v3, Lorg/apache/poi/hssf/model/RecordStream;

    invoke-direct {v3, p1, p2}, Lorg/apache/poi/hssf/model/RecordStream;-><init>(Ljava/util/List;I)V

    .line 185
    .local v3, "rs":Lorg/apache/poi/hssf/model/RecordStream;
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 186
    .local v4, "temp":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/hssf/model/LinkTable$ExternalBookBlock;>;"
    :goto_0
    invoke-virtual {v3}, Lorg/apache/poi/hssf/model/RecordStream;->peekNextClass()Ljava/lang/Class;

    move-result-object v5

    const-class v6, Lorg/apache/poi/hssf/record/SupBookRecord;

    if-eq v5, v6, :cond_0

    .line 190
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v5

    new-array v5, v5, [Lorg/apache/poi/hssf/model/LinkTable$ExternalBookBlock;

    iput-object v5, p0, Lorg/apache/poi/hssf/model/LinkTable;->_externalBookBlocks:[Lorg/apache/poi/hssf/model/LinkTable$ExternalBookBlock;

    .line 191
    iget-object v5, p0, Lorg/apache/poi/hssf/model/LinkTable;->_externalBookBlocks:[Lorg/apache/poi/hssf/model/LinkTable$ExternalBookBlock;

    invoke-interface {v4, v5}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 192
    invoke-interface {v4}, Ljava/util/List;->clear()V

    .line 194
    iget-object v5, p0, Lorg/apache/poi/hssf/model/LinkTable;->_externalBookBlocks:[Lorg/apache/poi/hssf/model/LinkTable$ExternalBookBlock;

    array-length v5, v5

    if-lez v5, :cond_2

    .line 196
    invoke-virtual {v3}, Lorg/apache/poi/hssf/model/RecordStream;->peekNextClass()Ljava/lang/Class;

    move-result-object v5

    const-class v6, Lorg/apache/poi/hssf/record/ExternSheetRecord;

    if-eq v5, v6, :cond_1

    .line 198
    iput-object v7, p0, Lorg/apache/poi/hssf/model/LinkTable;->_externSheetRecord:Lorg/apache/poi/hssf/record/ExternSheetRecord;

    .line 206
    :goto_1
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    iput-object v5, p0, Lorg/apache/poi/hssf/model/LinkTable;->_definedNames:Ljava/util/List;

    .line 210
    :goto_2
    invoke-virtual {v3}, Lorg/apache/poi/hssf/model/RecordStream;->peekNextClass()Ljava/lang/Class;

    move-result-object v1

    .line 211
    .local v1, "nextClass":Ljava/lang/Class;
    const-class v5, Lorg/apache/poi/hssf/record/NameRecord;

    if-ne v1, v5, :cond_3

    .line 212
    invoke-virtual {v3}, Lorg/apache/poi/hssf/model/RecordStream;->getNext()Lorg/apache/poi/hssf/record/Record;

    move-result-object v2

    check-cast v2, Lorg/apache/poi/hssf/record/NameRecord;

    .line 213
    .local v2, "nr":Lorg/apache/poi/hssf/record/NameRecord;
    iget-object v5, p0, Lorg/apache/poi/hssf/model/LinkTable;->_definedNames:Ljava/util/List;

    invoke-interface {v5, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 187
    .end local v1    # "nextClass":Ljava/lang/Class;
    .end local v2    # "nr":Lorg/apache/poi/hssf/record/NameRecord;
    :cond_0
    new-instance v5, Lorg/apache/poi/hssf/model/LinkTable$ExternalBookBlock;

    invoke-direct {v5, v3}, Lorg/apache/poi/hssf/model/LinkTable$ExternalBookBlock;-><init>(Lorg/apache/poi/hssf/model/RecordStream;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 200
    :cond_1
    invoke-static {v3}, Lorg/apache/poi/hssf/model/LinkTable;->readExtSheetRecord(Lorg/apache/poi/hssf/model/RecordStream;)Lorg/apache/poi/hssf/record/ExternSheetRecord;

    move-result-object v5

    iput-object v5, p0, Lorg/apache/poi/hssf/model/LinkTable;->_externSheetRecord:Lorg/apache/poi/hssf/record/ExternSheetRecord;

    goto :goto_1

    .line 203
    :cond_2
    iput-object v7, p0, Lorg/apache/poi/hssf/model/LinkTable;->_externSheetRecord:Lorg/apache/poi/hssf/record/ExternSheetRecord;

    goto :goto_1

    .line 215
    .restart local v1    # "nextClass":Ljava/lang/Class;
    :cond_3
    const-class v5, Lorg/apache/poi/hssf/record/NameCommentRecord;

    if-ne v1, v5, :cond_4

    .line 216
    invoke-virtual {v3}, Lorg/apache/poi/hssf/model/RecordStream;->getNext()Lorg/apache/poi/hssf/record/Record;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hssf/record/NameCommentRecord;

    .line 217
    .local v0, "ncr":Lorg/apache/poi/hssf/record/NameCommentRecord;
    invoke-virtual {v0}, Lorg/apache/poi/hssf/record/NameCommentRecord;->getNameText()Ljava/lang/String;

    move-result-object v5

    invoke-interface {p4, v5, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 224
    .end local v0    # "ncr":Lorg/apache/poi/hssf/record/NameCommentRecord;
    :cond_4
    invoke-virtual {v3}, Lorg/apache/poi/hssf/model/RecordStream;->getCountRead()I

    move-result v5

    iput v5, p0, Lorg/apache/poi/hssf/model/LinkTable;->_recordCount:I

    .line 225
    iget-object v5, p0, Lorg/apache/poi/hssf/model/LinkTable;->_workbookRecordList:Lorg/apache/poi/hssf/model/WorkbookRecordList;

    invoke-virtual {v5}, Lorg/apache/poi/hssf/model/WorkbookRecordList;->getRecords()Ljava/util/List;

    move-result-object v5

    iget v6, p0, Lorg/apache/poi/hssf/model/LinkTable;->_recordCount:I

    add-int/2addr v6, p2

    invoke-interface {p1, p2, v6}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 226
    return-void
.end method

.method private findFirstRecordLocBySid(S)I
    .locals 4
    .param p1, "sid"    # S

    .prologue
    .line 453
    const/4 v0, 0x0

    .line 454
    .local v0, "index":I
    iget-object v3, p0, Lorg/apache/poi/hssf/model/LinkTable;->_workbookRecordList:Lorg/apache/poi/hssf/model/WorkbookRecordList;

    invoke-virtual {v3}, Lorg/apache/poi/hssf/model/WorkbookRecordList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "iterator":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_1

    .line 462
    const/4 v0, -0x1

    .end local v0    # "index":I
    :cond_0
    return v0

    .line 455
    .restart local v0    # "index":I
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/poi/hssf/record/Record;

    .line 457
    .local v2, "record":Lorg/apache/poi/hssf/record/Record;
    invoke-virtual {v2}, Lorg/apache/poi/hssf/record/Record;->getSid()S

    move-result v3

    if-eq v3, p1, :cond_0

    .line 460
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private findRefIndexFromExtBookIndex(I)I
    .locals 1
    .param p1, "extBookIndex"    # I

    .prologue
    .line 553
    iget-object v0, p0, Lorg/apache/poi/hssf/model/LinkTable;->_externSheetRecord:Lorg/apache/poi/hssf/record/ExternSheetRecord;

    invoke-virtual {v0, p1}, Lorg/apache/poi/hssf/record/ExternSheetRecord;->findRefIndexFromExtBookIndex(I)I

    move-result v0

    return v0
.end method

.method private static getSheetIndex([Ljava/lang/String;Ljava/lang/String;)I
    .locals 4
    .param p0, "sheetNames"    # [Ljava/lang/String;
    .param p1, "sheetName"    # Ljava/lang/String;

    .prologue
    .line 402
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v1, p0

    if-lt v0, v1, :cond_0

    .line 408
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "External workbook does not contain sheet \'"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "\'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 403
    :cond_0
    aget-object v1, p0, v0

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 404
    return v0

    .line 402
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private static isDuplicatedNames(Lorg/apache/poi/hssf/record/NameRecord;Lorg/apache/poi/hssf/record/NameRecord;)Z
    .locals 2
    .param p0, "firstName"    # Lorg/apache/poi/hssf/record/NameRecord;
    .param p1, "lastName"    # Lorg/apache/poi/hssf/record/NameRecord;

    .prologue
    .line 349
    invoke-virtual {p1}, Lorg/apache/poi/hssf/record/NameRecord;->getNameText()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lorg/apache/poi/hssf/record/NameRecord;->getNameText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 350
    invoke-static {p0, p1}, Lorg/apache/poi/hssf/model/LinkTable;->isSameSheetNames(Lorg/apache/poi/hssf/record/NameRecord;Lorg/apache/poi/hssf/record/NameRecord;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 349
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static isSameSheetNames(Lorg/apache/poi/hssf/record/NameRecord;Lorg/apache/poi/hssf/record/NameRecord;)Z
    .locals 2
    .param p0, "firstName"    # Lorg/apache/poi/hssf/record/NameRecord;
    .param p1, "lastName"    # Lorg/apache/poi/hssf/record/NameRecord;

    .prologue
    .line 353
    invoke-virtual {p1}, Lorg/apache/poi/hssf/record/NameRecord;->getSheetNumber()I

    move-result v0

    invoke-virtual {p0}, Lorg/apache/poi/hssf/record/NameRecord;->getSheetNumber()I

    move-result v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static readExtSheetRecord(Lorg/apache/poi/hssf/model/RecordStream;)Lorg/apache/poi/hssf/record/ExternSheetRecord;
    .locals 6
    .param p0, "rs"    # Lorg/apache/poi/hssf/model/RecordStream;

    .prologue
    const/4 v5, 0x1

    .line 229
    new-instance v2, Ljava/util/ArrayList;

    const/4 v3, 0x2

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 230
    .local v2, "temp":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/hssf/record/ExternSheetRecord;>;"
    :goto_0
    invoke-virtual {p0}, Lorg/apache/poi/hssf/model/RecordStream;->peekNextClass()Ljava/lang/Class;

    move-result-object v3

    const-class v4, Lorg/apache/poi/hssf/record/ExternSheetRecord;

    if-eq v3, v4, :cond_0

    .line 234
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v1

    .line 235
    .local v1, "nItems":I
    if-ge v1, v5, :cond_1

    .line 236
    new-instance v3, Ljava/lang/RuntimeException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "Expected an EXTERNSHEET record but got ("

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 237
    invoke-virtual {p0}, Lorg/apache/poi/hssf/model/RecordStream;->peekNextClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 236
    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 231
    .end local v1    # "nItems":I
    :cond_0
    invoke-virtual {p0}, Lorg/apache/poi/hssf/model/RecordStream;->getNext()Lorg/apache/poi/hssf/record/Record;

    move-result-object v3

    check-cast v3, Lorg/apache/poi/hssf/record/ExternSheetRecord;

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 239
    .restart local v1    # "nItems":I
    :cond_1
    if-ne v1, v5, :cond_2

    .line 241
    const/4 v3, 0x0

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/poi/hssf/record/ExternSheetRecord;

    .line 247
    :goto_1
    return-object v3

    .line 245
    :cond_2
    new-array v0, v1, [Lorg/apache/poi/hssf/record/ExternSheetRecord;

    .line 246
    .local v0, "esrs":[Lorg/apache/poi/hssf/record/ExternSheetRecord;
    invoke-interface {v2, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 247
    invoke-static {v0}, Lorg/apache/poi/hssf/record/ExternSheetRecord;->combine([Lorg/apache/poi/hssf/record/ExternSheetRecord;)Lorg/apache/poi/hssf/record/ExternSheetRecord;

    move-result-object v3

    goto :goto_1
.end method


# virtual methods
.method public addName(Lorg/apache/poi/hssf/record/NameRecord;)V
    .locals 4
    .param p1, "name"    # Lorg/apache/poi/hssf/record/NameRecord;

    .prologue
    const/4 v3, -0x1

    .line 317
    iget-object v2, p0, Lorg/apache/poi/hssf/model/LinkTable;->_definedNames:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 321
    const/16 v2, 0x17

    invoke-direct {p0, v2}, Lorg/apache/poi/hssf/model/LinkTable;->findFirstRecordLocBySid(S)I

    move-result v1

    .line 322
    .local v1, "idx":I
    if-ne v1, v3, :cond_0

    const/16 v2, 0x1ae

    invoke-direct {p0, v2}, Lorg/apache/poi/hssf/model/LinkTable;->findFirstRecordLocBySid(S)I

    move-result v1

    .line 323
    :cond_0
    if-ne v1, v3, :cond_1

    const/16 v2, 0x8c

    invoke-direct {p0, v2}, Lorg/apache/poi/hssf/model/LinkTable;->findFirstRecordLocBySid(S)I

    move-result v1

    .line 324
    :cond_1
    iget-object v2, p0, Lorg/apache/poi/hssf/model/LinkTable;->_definedNames:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    .line 325
    .local v0, "countNames":I
    iget-object v2, p0, Lorg/apache/poi/hssf/model/LinkTable;->_workbookRecordList:Lorg/apache/poi/hssf/model/WorkbookRecordList;

    add-int v3, v1, v0

    invoke-virtual {v2, v3, p1}, Lorg/apache/poi/hssf/model/WorkbookRecordList;->add(ILorg/apache/poi/hssf/record/Record;)V

    .line 326
    return-void
.end method

.method public addNameXPtg(Ljava/lang/String;)Lorg/apache/poi/ss/formula/ptg/NameXPtg;
    .locals 22
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 497
    const/4 v7, -0x1

    .line 498
    .local v7, "extBlockIndex":I
    const/4 v6, 0x0

    .line 501
    .local v6, "extBlock":Lorg/apache/poi/hssf/model/LinkTable$ExternalBookBlock;
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/poi/hssf/model/LinkTable;->_externalBookBlocks:[Lorg/apache/poi/hssf/model/LinkTable$ExternalBookBlock;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    array-length v0, v0

    move/from16 v18, v0

    move/from16 v0, v18

    if-lt v9, v0, :cond_2

    .line 510
    :goto_1
    if-nez v6, :cond_0

    .line 511
    new-instance v6, Lorg/apache/poi/hssf/model/LinkTable$ExternalBookBlock;

    .end local v6    # "extBlock":Lorg/apache/poi/hssf/model/LinkTable$ExternalBookBlock;
    invoke-direct {v6}, Lorg/apache/poi/hssf/model/LinkTable$ExternalBookBlock;-><init>()V

    .line 513
    .restart local v6    # "extBlock":Lorg/apache/poi/hssf/model/LinkTable$ExternalBookBlock;
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/poi/hssf/model/LinkTable;->_externalBookBlocks:[Lorg/apache/poi/hssf/model/LinkTable$ExternalBookBlock;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    array-length v0, v0

    move/from16 v18, v0

    add-int/lit8 v18, v18, 0x1

    move/from16 v0, v18

    new-array v0, v0, [Lorg/apache/poi/hssf/model/LinkTable$ExternalBookBlock;

    move-object/from16 v17, v0

    .line 514
    .local v17, "tmp":[Lorg/apache/poi/hssf/model/LinkTable$ExternalBookBlock;
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/poi/hssf/model/LinkTable;->_externalBookBlocks:[Lorg/apache/poi/hssf/model/LinkTable$ExternalBookBlock;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    const/16 v20, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/poi/hssf/model/LinkTable;->_externalBookBlocks:[Lorg/apache/poi/hssf/model/LinkTable$ExternalBookBlock;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    array-length v0, v0

    move/from16 v21, v0

    move-object/from16 v0, v18

    move/from16 v1, v19

    move-object/from16 v2, v17

    move/from16 v3, v20

    move/from16 v4, v21

    invoke-static {v0, v1, v2, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 515
    move-object/from16 v0, v17

    array-length v0, v0

    move/from16 v18, v0

    add-int/lit8 v18, v18, -0x1

    aput-object v6, v17, v18

    .line 516
    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/apache/poi/hssf/model/LinkTable;->_externalBookBlocks:[Lorg/apache/poi/hssf/model/LinkTable$ExternalBookBlock;

    .line 518
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/poi/hssf/model/LinkTable;->_externalBookBlocks:[Lorg/apache/poi/hssf/model/LinkTable$ExternalBookBlock;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    array-length v0, v0

    move/from16 v18, v0

    add-int/lit8 v7, v18, -0x1

    .line 521
    const/16 v18, 0x17

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-direct {v0, v1}, Lorg/apache/poi/hssf/model/LinkTable;->findFirstRecordLocBySid(S)I

    move-result v10

    .line 522
    .local v10, "idx":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/poi/hssf/model/LinkTable;->_workbookRecordList:Lorg/apache/poi/hssf/model/WorkbookRecordList;

    move-object/from16 v18, v0

    invoke-virtual {v6}, Lorg/apache/poi/hssf/model/LinkTable$ExternalBookBlock;->getExternalBookRecord()Lorg/apache/poi/hssf/record/SupBookRecord;

    move-result-object v19

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v0, v10, v1}, Lorg/apache/poi/hssf/model/WorkbookRecordList;->add(ILorg/apache/poi/hssf/record/Record;)V

    .line 526
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/poi/hssf/model/LinkTable;->_externSheetRecord:Lorg/apache/poi/hssf/record/ExternSheetRecord;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/poi/hssf/model/LinkTable;->_externalBookBlocks:[Lorg/apache/poi/hssf/model/LinkTable$ExternalBookBlock;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    array-length v0, v0

    move/from16 v19, v0

    add-int/lit8 v19, v19, -0x1

    const/16 v20, -0x2

    const/16 v21, -0x2

    invoke-virtual/range {v18 .. v21}, Lorg/apache/poi/hssf/record/ExternSheetRecord;->addRef(III)I

    .line 530
    .end local v10    # "idx":I
    .end local v17    # "tmp":[Lorg/apache/poi/hssf/model/LinkTable$ExternalBookBlock;
    :cond_0
    new-instance v8, Lorg/apache/poi/hssf/record/ExternalNameRecord;

    invoke-direct {v8}, Lorg/apache/poi/hssf/record/ExternalNameRecord;-><init>()V

    .line 531
    .local v8, "extNameRecord":Lorg/apache/poi/hssf/record/ExternalNameRecord;
    move-object/from16 v0, p1

    invoke-virtual {v8, v0}, Lorg/apache/poi/hssf/record/ExternalNameRecord;->setText(Ljava/lang/String;)V

    .line 533
    const/16 v18, 0x1

    move/from16 v0, v18

    new-array v0, v0, [Lorg/apache/poi/ss/formula/ptg/Ptg;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    sget-object v20, Lorg/apache/poi/ss/formula/ptg/ErrPtg;->REF_INVALID:Lorg/apache/poi/ss/formula/ptg/ErrPtg;

    aput-object v20, v18, v19

    move-object/from16 v0, v18

    invoke-virtual {v8, v0}, Lorg/apache/poi/hssf/record/ExternalNameRecord;->setParsedExpression([Lorg/apache/poi/ss/formula/ptg/Ptg;)V

    .line 535
    invoke-virtual {v6, v8}, Lorg/apache/poi/hssf/model/LinkTable$ExternalBookBlock;->addExternalName(Lorg/apache/poi/hssf/record/ExternalNameRecord;)I

    move-result v13

    .line 536
    .local v13, "nameIndex":I
    const/16 v16, 0x0

    .line 539
    .local v16, "supLinkIndex":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/poi/hssf/model/LinkTable;->_workbookRecordList:Lorg/apache/poi/hssf/model/WorkbookRecordList;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lorg/apache/poi/hssf/model/WorkbookRecordList;->iterator()Ljava/util/Iterator;

    move-result-object v11

    .local v11, "iterator":Ljava/util/Iterator;
    :goto_2
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v18

    if-nez v18, :cond_4

    .line 545
    :cond_1
    invoke-virtual {v6}, Lorg/apache/poi/hssf/model/LinkTable$ExternalBookBlock;->getNumberOfNames()I

    move-result v14

    .line 547
    .local v14, "numberOfNames":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/poi/hssf/model/LinkTable;->_workbookRecordList:Lorg/apache/poi/hssf/model/WorkbookRecordList;

    move-object/from16 v18, v0

    add-int v19, v16, v14

    move-object/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v0, v1, v8}, Lorg/apache/poi/hssf/model/WorkbookRecordList;->add(ILorg/apache/poi/hssf/record/Record;)V

    .line 548
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/poi/hssf/model/LinkTable;->_externSheetRecord:Lorg/apache/poi/hssf/record/ExternSheetRecord;

    move-object/from16 v18, v0

    const/16 v19, -0x2

    move-object/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v0, v7, v1}, Lorg/apache/poi/hssf/record/ExternSheetRecord;->getRefIxForSheet(II)I

    move-result v12

    .line 549
    .local v12, "ix":I
    new-instance v18, Lorg/apache/poi/ss/formula/ptg/NameXPtg;

    move-object/from16 v0, v18

    invoke-direct {v0, v12, v13}, Lorg/apache/poi/ss/formula/ptg/NameXPtg;-><init>(II)V

    return-object v18

    .line 502
    .end local v8    # "extNameRecord":Lorg/apache/poi/hssf/record/ExternalNameRecord;
    .end local v11    # "iterator":Ljava/util/Iterator;
    .end local v12    # "ix":I
    .end local v13    # "nameIndex":I
    .end local v14    # "numberOfNames":I
    .end local v16    # "supLinkIndex":I
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/poi/hssf/model/LinkTable;->_externalBookBlocks:[Lorg/apache/poi/hssf/model/LinkTable$ExternalBookBlock;

    move-object/from16 v18, v0

    aget-object v18, v18, v9

    invoke-virtual/range {v18 .. v18}, Lorg/apache/poi/hssf/model/LinkTable$ExternalBookBlock;->getExternalBookRecord()Lorg/apache/poi/hssf/record/SupBookRecord;

    move-result-object v5

    .line 503
    .local v5, "ebr":Lorg/apache/poi/hssf/record/SupBookRecord;
    invoke-virtual {v5}, Lorg/apache/poi/hssf/record/SupBookRecord;->isAddInFunctions()Z

    move-result v18

    if-eqz v18, :cond_3

    .line 504
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/poi/hssf/model/LinkTable;->_externalBookBlocks:[Lorg/apache/poi/hssf/model/LinkTable$ExternalBookBlock;

    move-object/from16 v18, v0

    aget-object v6, v18, v9

    .line 505
    move v7, v9

    .line 506
    goto/16 :goto_1

    .line 501
    :cond_3
    add-int/lit8 v9, v9, 0x1

    goto/16 :goto_0

    .line 540
    .end local v5    # "ebr":Lorg/apache/poi/hssf/record/SupBookRecord;
    .restart local v8    # "extNameRecord":Lorg/apache/poi/hssf/record/ExternalNameRecord;
    .restart local v11    # "iterator":Ljava/util/Iterator;
    .restart local v13    # "nameIndex":I
    .restart local v16    # "supLinkIndex":I
    :cond_4
    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lorg/apache/poi/hssf/record/Record;

    .line 541
    .local v15, "record":Lorg/apache/poi/hssf/record/Record;
    instance-of v0, v15, Lorg/apache/poi/hssf/record/SupBookRecord;

    move/from16 v18, v0

    if-eqz v18, :cond_5

    .line 542
    check-cast v15, Lorg/apache/poi/hssf/record/SupBookRecord;

    .end local v15    # "record":Lorg/apache/poi/hssf/record/Record;
    invoke-virtual {v15}, Lorg/apache/poi/hssf/record/SupBookRecord;->isAddInFunctions()Z

    move-result v18

    if-nez v18, :cond_1

    .line 539
    :cond_5
    add-int/lit8 v16, v16, 0x1

    goto :goto_2
.end method

.method public changeExternalReference(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 7
    .param p1, "oldUrl"    # Ljava/lang/String;
    .param p2, "newUrl"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 566
    iget-object v4, p0, Lorg/apache/poi/hssf/model/LinkTable;->_externalBookBlocks:[Lorg/apache/poi/hssf/model/LinkTable$ExternalBookBlock;

    array-length v5, v4

    move v3, v2

    :goto_0
    if-lt v3, v5, :cond_0

    .line 575
    :goto_1
    return v2

    .line 566
    :cond_0
    aget-object v0, v4, v3

    .line 567
    .local v0, "ex":Lorg/apache/poi/hssf/model/LinkTable$ExternalBookBlock;
    invoke-virtual {v0}, Lorg/apache/poi/hssf/model/LinkTable$ExternalBookBlock;->getExternalBookRecord()Lorg/apache/poi/hssf/record/SupBookRecord;

    move-result-object v1

    .line 568
    .local v1, "externalRecord":Lorg/apache/poi/hssf/record/SupBookRecord;
    invoke-virtual {v1}, Lorg/apache/poi/hssf/record/SupBookRecord;->isExternalReferences()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 569
    invoke-virtual {v1}, Lorg/apache/poi/hssf/record/SupBookRecord;->getURL()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 571
    invoke-virtual {v1, p2}, Lorg/apache/poi/hssf/record/SupBookRecord;->setURL(Ljava/lang/String;)V

    .line 572
    const/4 v2, 0x1

    goto :goto_1

    .line 566
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method

.method public checkExternSheet(I)I
    .locals 5
    .param p1, "sheetIndex"    # I

    .prologue
    .line 427
    const/4 v2, -0x1

    .line 428
    .local v2, "thisWbIndex":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lorg/apache/poi/hssf/model/LinkTable;->_externalBookBlocks:[Lorg/apache/poi/hssf/model/LinkTable$ExternalBookBlock;

    array-length v3, v3

    if-lt v1, v3, :cond_0

    .line 435
    :goto_1
    if-gez v2, :cond_2

    .line 436
    new-instance v3, Ljava/lang/RuntimeException;

    const-string/jumbo v4, "Could not find \'internal references\' EXTERNALBOOK"

    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 429
    :cond_0
    iget-object v3, p0, Lorg/apache/poi/hssf/model/LinkTable;->_externalBookBlocks:[Lorg/apache/poi/hssf/model/LinkTable$ExternalBookBlock;

    aget-object v3, v3, v1

    invoke-virtual {v3}, Lorg/apache/poi/hssf/model/LinkTable$ExternalBookBlock;->getExternalBookRecord()Lorg/apache/poi/hssf/record/SupBookRecord;

    move-result-object v0

    .line 430
    .local v0, "ebr":Lorg/apache/poi/hssf/record/SupBookRecord;
    invoke-virtual {v0}, Lorg/apache/poi/hssf/record/SupBookRecord;->isInternalReferences()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 431
    move v2, v1

    .line 432
    goto :goto_1

    .line 428
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 440
    .end local v0    # "ebr":Lorg/apache/poi/hssf/record/SupBookRecord;
    :cond_2
    iget-object v3, p0, Lorg/apache/poi/hssf/model/LinkTable;->_externSheetRecord:Lorg/apache/poi/hssf/record/ExternSheetRecord;

    invoke-virtual {v3, v2, p1}, Lorg/apache/poi/hssf/record/ExternSheetRecord;->getRefIxForSheet(II)I

    move-result v1

    .line 441
    if-ltz v1, :cond_3

    .line 445
    .end local v1    # "i":I
    :goto_2
    return v1

    .restart local v1    # "i":I
    :cond_3
    iget-object v3, p0, Lorg/apache/poi/hssf/model/LinkTable;->_externSheetRecord:Lorg/apache/poi/hssf/record/ExternSheetRecord;

    invoke-virtual {v3, v2, p1, p1}, Lorg/apache/poi/hssf/record/ExternSheetRecord;->addRef(III)I

    move-result v1

    goto :goto_2
.end method

.method public getExternalBookAndSheetName(I)[Ljava/lang/String;
    .locals 7
    .param p1, "extRefIndex"    # I

    .prologue
    .line 357
    iget-object v4, p0, Lorg/apache/poi/hssf/model/LinkTable;->_externSheetRecord:Lorg/apache/poi/hssf/record/ExternSheetRecord;

    invoke-virtual {v4, p1}, Lorg/apache/poi/hssf/record/ExternSheetRecord;->getExtbookIndexFromRefIndex(I)I

    move-result v0

    .line 358
    .local v0, "ebIx":I
    iget-object v4, p0, Lorg/apache/poi/hssf/model/LinkTable;->_externalBookBlocks:[Lorg/apache/poi/hssf/model/LinkTable$ExternalBookBlock;

    aget-object v4, v4, v0

    invoke-virtual {v4}, Lorg/apache/poi/hssf/model/LinkTable$ExternalBookBlock;->getExternalBookRecord()Lorg/apache/poi/hssf/record/SupBookRecord;

    move-result-object v1

    .line 359
    .local v1, "ebr":Lorg/apache/poi/hssf/record/SupBookRecord;
    invoke-virtual {v1}, Lorg/apache/poi/hssf/record/SupBookRecord;->isExternalReferences()Z

    move-result v4

    if-nez v4, :cond_0

    .line 360
    const/4 v4, 0x0

    .line 368
    :goto_0
    return-object v4

    .line 363
    :cond_0
    iget-object v4, p0, Lorg/apache/poi/hssf/model/LinkTable;->_externSheetRecord:Lorg/apache/poi/hssf/record/ExternSheetRecord;

    invoke-virtual {v4, p1}, Lorg/apache/poi/hssf/record/ExternSheetRecord;->getFirstSheetIndexFromRefIndex(I)I

    move-result v2

    .line 364
    .local v2, "shIx":I
    const/4 v3, 0x0

    .line 365
    .local v3, "usSheetName":Ljava/lang/String;
    if-ltz v2, :cond_1

    .line 366
    invoke-virtual {v1}, Lorg/apache/poi/hssf/record/SupBookRecord;->getSheetNames()[Ljava/lang/String;

    move-result-object v4

    aget-object v3, v4, v2

    .line 368
    :cond_1
    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    .line 369
    invoke-virtual {v1}, Lorg/apache/poi/hssf/record/SupBookRecord;->getURL()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    .line 370
    aput-object v3, v4, v5

    goto :goto_0
.end method

.method public getExternalSheetIndex(Ljava/lang/String;Ljava/lang/String;)I
    .locals 9
    .param p1, "workbookName"    # Ljava/lang/String;
    .param p2, "sheetName"    # Ljava/lang/String;

    .prologue
    .line 375
    const/4 v1, 0x0

    .line 376
    .local v1, "ebrTarget":Lorg/apache/poi/hssf/record/SupBookRecord;
    const/4 v2, -0x1

    .line 377
    .local v2, "externalBookIndex":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    iget-object v6, p0, Lorg/apache/poi/hssf/model/LinkTable;->_externalBookBlocks:[Lorg/apache/poi/hssf/model/LinkTable$ExternalBookBlock;

    array-length v6, v6

    if-lt v3, v6, :cond_0

    .line 388
    :goto_1
    if-nez v1, :cond_3

    .line 389
    new-instance v6, Ljava/lang/RuntimeException;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string/jumbo v8, "No external workbook with name \'"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, "\'"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 378
    :cond_0
    iget-object v6, p0, Lorg/apache/poi/hssf/model/LinkTable;->_externalBookBlocks:[Lorg/apache/poi/hssf/model/LinkTable$ExternalBookBlock;

    aget-object v6, v6, v3

    invoke-virtual {v6}, Lorg/apache/poi/hssf/model/LinkTable$ExternalBookBlock;->getExternalBookRecord()Lorg/apache/poi/hssf/record/SupBookRecord;

    move-result-object v0

    .line 379
    .local v0, "ebr":Lorg/apache/poi/hssf/record/SupBookRecord;
    invoke-virtual {v0}, Lorg/apache/poi/hssf/record/SupBookRecord;->isExternalReferences()Z

    move-result v6

    if-nez v6, :cond_2

    .line 377
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 382
    :cond_2
    invoke-virtual {v0}, Lorg/apache/poi/hssf/record/SupBookRecord;->getURL()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 383
    move-object v1, v0

    .line 384
    move v2, v3

    .line 385
    goto :goto_1

    .line 391
    .end local v0    # "ebr":Lorg/apache/poi/hssf/record/SupBookRecord;
    :cond_3
    invoke-virtual {v1}, Lorg/apache/poi/hssf/record/SupBookRecord;->getSheetNames()[Ljava/lang/String;

    move-result-object v6

    invoke-static {v6, p2}, Lorg/apache/poi/hssf/model/LinkTable;->getSheetIndex([Ljava/lang/String;Ljava/lang/String;)I

    move-result v5

    .line 393
    .local v5, "sheetIndex":I
    iget-object v6, p0, Lorg/apache/poi/hssf/model/LinkTable;->_externSheetRecord:Lorg/apache/poi/hssf/record/ExternSheetRecord;

    invoke-virtual {v6, v2, v5}, Lorg/apache/poi/hssf/record/ExternSheetRecord;->getRefIxForSheet(II)I

    move-result v4

    .line 394
    .local v4, "result":I
    if-gez v4, :cond_4

    .line 395
    new-instance v6, Ljava/lang/RuntimeException;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string/jumbo v8, "ExternSheetRecord does not contain combination ("

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 396
    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, ", "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, ")"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 395
    invoke-direct {v6, v7}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 398
    :cond_4
    return v4
.end method

.method public getIndexToInternalSheet(I)I
    .locals 1
    .param p1, "extRefIndex"    # I

    .prologue
    .line 416
    iget-object v0, p0, Lorg/apache/poi/hssf/model/LinkTable;->_externSheetRecord:Lorg/apache/poi/hssf/record/ExternSheetRecord;

    invoke-virtual {v0, p1}, Lorg/apache/poi/hssf/record/ExternSheetRecord;->getFirstSheetIndexFromRefIndex(I)I

    move-result v0

    return v0
.end method

.method public getNameRecord(I)Lorg/apache/poi/hssf/record/NameRecord;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 313
    iget-object v0, p0, Lorg/apache/poi/hssf/model/LinkTable;->_definedNames:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hssf/record/NameRecord;

    return-object v0
.end method

.method public getNameXPtg(Ljava/lang/String;)Lorg/apache/poi/ss/formula/ptg/NameXPtg;
    .locals 4
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 476
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lorg/apache/poi/hssf/model/LinkTable;->_externalBookBlocks:[Lorg/apache/poi/hssf/model/LinkTable$ExternalBookBlock;

    array-length v3, v3

    if-lt v1, v3, :cond_0

    .line 487
    const/4 v3, 0x0

    :goto_1
    return-object v3

    .line 477
    :cond_0
    iget-object v3, p0, Lorg/apache/poi/hssf/model/LinkTable;->_externalBookBlocks:[Lorg/apache/poi/hssf/model/LinkTable$ExternalBookBlock;

    aget-object v3, v3, v1

    invoke-virtual {v3, p1}, Lorg/apache/poi/hssf/model/LinkTable$ExternalBookBlock;->getIndexOfName(Ljava/lang/String;)I

    move-result v0

    .line 478
    .local v0, "definedNameIndex":I
    if-gez v0, :cond_2

    .line 476
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 482
    :cond_2
    invoke-direct {p0, v1}, Lorg/apache/poi/hssf/model/LinkTable;->findRefIndexFromExtBookIndex(I)I

    move-result v2

    .line 483
    .local v2, "sheetRefIndex":I
    if-ltz v2, :cond_1

    .line 484
    new-instance v3, Lorg/apache/poi/ss/formula/ptg/NameXPtg;

    invoke-direct {v3, v2, v0}, Lorg/apache/poi/ss/formula/ptg/NameXPtg;-><init>(II)V

    goto :goto_1
.end method

.method public getNumNames()I
    .locals 1

    .prologue
    .line 309
    iget-object v0, p0, Lorg/apache/poi/hssf/model/LinkTable;->_definedNames:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getRecordCount()I
    .locals 1

    .prologue
    .line 275
    iget v0, p0, Lorg/apache/poi/hssf/model/LinkTable;->_recordCount:I

    return v0
.end method

.method public getSheetIndexFromExternSheetIndex(I)I
    .locals 1
    .param p1, "extRefIndex"    # I

    .prologue
    .line 420
    iget-object v0, p0, Lorg/apache/poi/hssf/model/LinkTable;->_externSheetRecord:Lorg/apache/poi/hssf/record/ExternSheetRecord;

    invoke-virtual {v0}, Lorg/apache/poi/hssf/record/ExternSheetRecord;->getNumOfRefs()I

    move-result v0

    if-lt p1, v0, :cond_0

    .line 421
    const/4 v0, -0x1

    .line 423
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lorg/apache/poi/hssf/model/LinkTable;->_externSheetRecord:Lorg/apache/poi/hssf/record/ExternSheetRecord;

    invoke-virtual {v0, p1}, Lorg/apache/poi/hssf/record/ExternSheetRecord;->getFirstSheetIndexFromRefIndex(I)I

    move-result v0

    goto :goto_0
.end method

.method public getSpecificBuiltinRecord(BI)Lorg/apache/poi/hssf/record/NameRecord;
    .locals 3
    .param p1, "builtInCode"    # B
    .param p2, "sheetNumber"    # I

    .prologue
    .line 285
    iget-object v2, p0, Lorg/apache/poi/hssf/model/LinkTable;->_definedNames:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 286
    .local v0, "iterator":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_1

    .line 295
    const/4 v1, 0x0

    :goto_0
    return-object v1

    .line 287
    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/hssf/record/NameRecord;

    .line 290
    .local v1, "record":Lorg/apache/poi/hssf/record/NameRecord;
    invoke-virtual {v1}, Lorg/apache/poi/hssf/record/NameRecord;->getBuiltInName()B

    move-result v2

    if-ne v2, p1, :cond_0

    invoke-virtual {v1}, Lorg/apache/poi/hssf/record/NameRecord;->getSheetNumber()I

    move-result v2

    if-ne v2, p2, :cond_0

    goto :goto_0
.end method

.method public nameAlreadyExists(Lorg/apache/poi/hssf/record/NameRecord;)Z
    .locals 3
    .param p1, "name"    # Lorg/apache/poi/hssf/record/NameRecord;

    .prologue
    .line 338
    invoke-virtual {p0}, Lorg/apache/poi/hssf/model/LinkTable;->getNumNames()I

    move-result v2

    add-int/lit8 v0, v2, -0x1

    .local v0, "i":I
    :goto_0
    if-gez v0, :cond_0

    .line 345
    const/4 v2, 0x0

    :goto_1
    return v2

    .line 339
    :cond_0
    invoke-virtual {p0, v0}, Lorg/apache/poi/hssf/model/LinkTable;->getNameRecord(I)Lorg/apache/poi/hssf/record/NameRecord;

    move-result-object v1

    .line 340
    .local v1, "rec":Lorg/apache/poi/hssf/record/NameRecord;
    if-eq v1, p1, :cond_1

    .line 341
    invoke-static {p1, v1}, Lorg/apache/poi/hssf/model/LinkTable;->isDuplicatedNames(Lorg/apache/poi/hssf/record/NameRecord;Lorg/apache/poi/hssf/record/NameRecord;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 342
    const/4 v2, 0x1

    goto :goto_1

    .line 338
    :cond_1
    add-int/lit8 v0, v0, -0x1

    goto :goto_0
.end method

.method public removeBuiltinRecord(BI)V
    .locals 2
    .param p1, "name"    # B
    .param p2, "sheetIndex"    # I

    .prologue
    .line 301
    invoke-virtual {p0, p1, p2}, Lorg/apache/poi/hssf/model/LinkTable;->getSpecificBuiltinRecord(BI)Lorg/apache/poi/hssf/record/NameRecord;

    move-result-object v0

    .line 302
    .local v0, "record":Lorg/apache/poi/hssf/record/NameRecord;
    if-eqz v0, :cond_0

    .line 303
    iget-object v1, p0, Lorg/apache/poi/hssf/model/LinkTable;->_definedNames:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 306
    :cond_0
    return-void
.end method

.method public removeName(I)V
    .locals 1
    .param p1, "namenum"    # I

    .prologue
    .line 329
    iget-object v0, p0, Lorg/apache/poi/hssf/model/LinkTable;->_definedNames:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 330
    return-void
.end method

.method public resolveNameXIx(II)I
    .locals 2
    .param p1, "refIndex"    # I
    .param p2, "definedNameIndex"    # I

    .prologue
    .line 470
    iget-object v1, p0, Lorg/apache/poi/hssf/model/LinkTable;->_externSheetRecord:Lorg/apache/poi/hssf/record/ExternSheetRecord;

    invoke-virtual {v1, p1}, Lorg/apache/poi/hssf/record/ExternSheetRecord;->getExtbookIndexFromRefIndex(I)I

    move-result v0

    .line 471
    .local v0, "extBookIndex":I
    iget-object v1, p0, Lorg/apache/poi/hssf/model/LinkTable;->_externalBookBlocks:[Lorg/apache/poi/hssf/model/LinkTable$ExternalBookBlock;

    aget-object v1, v1, v0

    invoke-virtual {v1, p2}, Lorg/apache/poi/hssf/model/LinkTable$ExternalBookBlock;->getNameIx(I)I

    move-result v1

    return v1
.end method

.method public resolveNameXText(II)Ljava/lang/String;
    .locals 2
    .param p1, "refIndex"    # I
    .param p2, "definedNameIndex"    # I

    .prologue
    .line 466
    iget-object v1, p0, Lorg/apache/poi/hssf/model/LinkTable;->_externSheetRecord:Lorg/apache/poi/hssf/record/ExternSheetRecord;

    invoke-virtual {v1, p1}, Lorg/apache/poi/hssf/record/ExternSheetRecord;->getExtbookIndexFromRefIndex(I)I

    move-result v0

    .line 467
    .local v0, "extBookIndex":I
    iget-object v1, p0, Lorg/apache/poi/hssf/model/LinkTable;->_externalBookBlocks:[Lorg/apache/poi/hssf/model/LinkTable$ExternalBookBlock;

    aget-object v1, v1, v0

    invoke-virtual {v1, p2}, Lorg/apache/poi/hssf/model/LinkTable$ExternalBookBlock;->getNameText(I)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
