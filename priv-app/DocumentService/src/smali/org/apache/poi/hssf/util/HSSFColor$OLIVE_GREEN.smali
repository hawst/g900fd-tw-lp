.class public Lorg/apache/poi/hssf/util/HSSFColor$OLIVE_GREEN;
.super Lorg/apache/poi/hssf/util/HSSFColor;
.source "HSSFColor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/poi/hssf/util/HSSFColor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "OLIVE_GREEN"
.end annotation


# static fields
.field public static final hexString:Ljava/lang/String; = "3333:3333:0"

.field public static final index:S = 0x3bs

.field public static final triplet:[S


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/16 v2, 0x33

    .line 283
    const/4 v0, 0x3

    new-array v0, v0, [S

    const/4 v1, 0x0

    .line 284
    aput-short v2, v0, v1

    const/4 v1, 0x1

    aput-short v2, v0, v1

    .line 282
    sput-object v0, Lorg/apache/poi/hssf/util/HSSFColor$OLIVE_GREEN;->triplet:[S

    .line 286
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 278
    invoke-direct {p0}, Lorg/apache/poi/hssf/util/HSSFColor;-><init>()V

    return-void
.end method


# virtual methods
.method public getHexString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 300
    const-string/jumbo v0, "3333:3333:0"

    return-object v0
.end method

.method public getIndex()S
    .locals 1

    .prologue
    .line 290
    const/16 v0, 0x3b

    return v0
.end method

.method public getTriplet()[S
    .locals 1

    .prologue
    .line 295
    sget-object v0, Lorg/apache/poi/hssf/util/HSSFColor$OLIVE_GREEN;->triplet:[S

    return-object v0
.end method
