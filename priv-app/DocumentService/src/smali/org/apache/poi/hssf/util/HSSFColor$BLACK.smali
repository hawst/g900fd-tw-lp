.class public final Lorg/apache/poi/hssf/util/HSSFColor$BLACK;
.super Lorg/apache/poi/hssf/util/HSSFColor;
.source "HSSFColor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/poi/hssf/util/HSSFColor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "BLACK"
.end annotation


# static fields
.field public static final hexString:Ljava/lang/String; = "0:0:0"

.field public static final index:S = 0x8s

.field public static final triplet:[S


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 221
    const/4 v0, 0x3

    new-array v0, v0, [S

    .line 220
    sput-object v0, Lorg/apache/poi/hssf/util/HSSFColor$BLACK;->triplet:[S

    .line 224
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 216
    invoke-direct {p0}, Lorg/apache/poi/hssf/util/HSSFColor;-><init>()V

    return-void
.end method


# virtual methods
.method public getHexString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 238
    const-string/jumbo v0, "0:0:0"

    return-object v0
.end method

.method public getIndex()S
    .locals 1

    .prologue
    .line 228
    const/16 v0, 0x8

    return v0
.end method

.method public getTriplet()[S
    .locals 1

    .prologue
    .line 233
    sget-object v0, Lorg/apache/poi/hssf/util/HSSFColor$BLACK;->triplet:[S

    return-object v0
.end method
