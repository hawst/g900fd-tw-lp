.class public final Lorg/apache/poi/hssf/record/cont/ContinuableRecordOutput;
.super Ljava/lang/Object;
.source "ContinuableRecordOutput.java"

# interfaces
.implements Lorg/apache/poi/util/LittleEndianOutput;


# static fields
.field private static final NOPOutput:Lorg/apache/poi/util/LittleEndianOutput;


# instance fields
.field private final _out:Lorg/apache/poi/util/LittleEndianOutput;

.field private _totalPreviousRecordsSize:I

.field private _ulrOutput:Lorg/apache/poi/hssf/record/cont/UnknownLengthRecordOutput;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 247
    new-instance v0, Lorg/apache/poi/hssf/record/cont/ContinuableRecordOutput$1;

    invoke-direct {v0}, Lorg/apache/poi/hssf/record/cont/ContinuableRecordOutput$1;-><init>()V

    sput-object v0, Lorg/apache/poi/hssf/record/cont/ContinuableRecordOutput;->NOPOutput:Lorg/apache/poi/util/LittleEndianOutput;

    .line 273
    return-void
.end method

.method public constructor <init>(Lorg/apache/poi/util/LittleEndianOutput;I)V
    .locals 1
    .param p1, "out"    # Lorg/apache/poi/util/LittleEndianOutput;
    .param p2, "sid"    # I

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    new-instance v0, Lorg/apache/poi/hssf/record/cont/UnknownLengthRecordOutput;

    invoke-direct {v0, p1, p2}, Lorg/apache/poi/hssf/record/cont/UnknownLengthRecordOutput;-><init>(Lorg/apache/poi/util/LittleEndianOutput;I)V

    iput-object v0, p0, Lorg/apache/poi/hssf/record/cont/ContinuableRecordOutput;->_ulrOutput:Lorg/apache/poi/hssf/record/cont/UnknownLengthRecordOutput;

    .line 40
    iput-object p1, p0, Lorg/apache/poi/hssf/record/cont/ContinuableRecordOutput;->_out:Lorg/apache/poi/util/LittleEndianOutput;

    .line 41
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/poi/hssf/record/cont/ContinuableRecordOutput;->_totalPreviousRecordsSize:I

    .line 42
    return-void
.end method

.method public static createForCountingOnly()Lorg/apache/poi/hssf/record/cont/ContinuableRecordOutput;
    .locals 3

    .prologue
    .line 45
    new-instance v0, Lorg/apache/poi/hssf/record/cont/ContinuableRecordOutput;

    sget-object v1, Lorg/apache/poi/hssf/record/cont/ContinuableRecordOutput;->NOPOutput:Lorg/apache/poi/util/LittleEndianOutput;

    const/16 v2, -0x309

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/hssf/record/cont/ContinuableRecordOutput;-><init>(Lorg/apache/poi/util/LittleEndianOutput;I)V

    return-object v0
.end method

.method private writeCharacterData(Ljava/lang/String;Z)V
    .locals 6
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "is16bitEncoded"    # Z

    .prologue
    .line 174
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    .line 175
    .local v2, "nChars":I
    const/4 v0, 0x0

    .line 176
    .local v0, "i":I
    if-eqz p2, :cond_3

    .line 178
    :goto_0
    sub-int v4, v2, v0

    iget-object v5, p0, Lorg/apache/poi/hssf/record/cont/ContinuableRecordOutput;->_ulrOutput:Lorg/apache/poi/hssf/record/cont/UnknownLengthRecordOutput;

    invoke-virtual {v5}, Lorg/apache/poi/hssf/record/cont/UnknownLengthRecordOutput;->getAvailableSpace()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v3

    .local v3, "nWritableChars":I
    move v1, v0

    .line 179
    .end local v0    # "i":I
    .local v1, "i":I
    :goto_1
    if-gtz v3, :cond_0

    .line 182
    if-lt v1, v2, :cond_1

    move v0, v1

    .line 201
    .end local v1    # "i":I
    .restart local v0    # "i":I
    :goto_2
    return-void

    .line 180
    .end local v0    # "i":I
    .restart local v1    # "i":I
    :cond_0
    iget-object v4, p0, Lorg/apache/poi/hssf/record/cont/ContinuableRecordOutput;->_ulrOutput:Lorg/apache/poi/hssf/record/cont/UnknownLengthRecordOutput;

    add-int/lit8 v0, v1, 0x1

    .end local v1    # "i":I
    .restart local v0    # "i":I
    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    move-result v5

    invoke-virtual {v4, v5}, Lorg/apache/poi/hssf/record/cont/UnknownLengthRecordOutput;->writeShort(I)V

    .line 179
    add-int/lit8 v3, v3, -0x1

    move v1, v0

    .end local v0    # "i":I
    .restart local v1    # "i":I
    goto :goto_1

    .line 185
    :cond_1
    invoke-virtual {p0}, Lorg/apache/poi/hssf/record/cont/ContinuableRecordOutput;->writeContinue()V

    .line 186
    const/4 v4, 0x1

    invoke-virtual {p0, v4}, Lorg/apache/poi/hssf/record/cont/ContinuableRecordOutput;->writeByte(I)V

    move v0, v1

    .line 177
    .end local v1    # "i":I
    .restart local v0    # "i":I
    goto :goto_0

    .line 197
    .end local v0    # "i":I
    .restart local v1    # "i":I
    :cond_2
    invoke-virtual {p0}, Lorg/apache/poi/hssf/record/cont/ContinuableRecordOutput;->writeContinue()V

    .line 198
    const/4 v4, 0x0

    invoke-virtual {p0, v4}, Lorg/apache/poi/hssf/record/cont/ContinuableRecordOutput;->writeByte(I)V

    move v0, v1

    .line 190
    .end local v1    # "i":I
    .end local v3    # "nWritableChars":I
    .restart local v0    # "i":I
    :cond_3
    sub-int v4, v2, v0

    iget-object v5, p0, Lorg/apache/poi/hssf/record/cont/ContinuableRecordOutput;->_ulrOutput:Lorg/apache/poi/hssf/record/cont/UnknownLengthRecordOutput;

    invoke-virtual {v5}, Lorg/apache/poi/hssf/record/cont/UnknownLengthRecordOutput;->getAvailableSpace()I

    move-result v5

    div-int/lit8 v5, v5, 0x1

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v3

    .restart local v3    # "nWritableChars":I
    move v1, v0

    .line 191
    .end local v0    # "i":I
    .restart local v1    # "i":I
    :goto_3
    if-gtz v3, :cond_4

    .line 194
    if-lt v1, v2, :cond_2

    move v0, v1

    .line 195
    .end local v1    # "i":I
    .restart local v0    # "i":I
    goto :goto_2

    .line 192
    .end local v0    # "i":I
    .restart local v1    # "i":I
    :cond_4
    iget-object v4, p0, Lorg/apache/poi/hssf/record/cont/ContinuableRecordOutput;->_ulrOutput:Lorg/apache/poi/hssf/record/cont/UnknownLengthRecordOutput;

    add-int/lit8 v0, v1, 0x1

    .end local v1    # "i":I
    .restart local v0    # "i":I
    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    move-result v5

    invoke-virtual {v4, v5}, Lorg/apache/poi/hssf/record/cont/UnknownLengthRecordOutput;->writeByte(I)V

    .line 191
    add-int/lit8 v3, v3, -0x1

    move v1, v0

    .end local v0    # "i":I
    .restart local v1    # "i":I
    goto :goto_3
.end method


# virtual methods
.method public getAvailableSpace()I
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lorg/apache/poi/hssf/record/cont/ContinuableRecordOutput;->_ulrOutput:Lorg/apache/poi/hssf/record/cont/UnknownLengthRecordOutput;

    invoke-virtual {v0}, Lorg/apache/poi/hssf/record/cont/UnknownLengthRecordOutput;->getAvailableSpace()I

    move-result v0

    return v0
.end method

.method public getTotalSize()I
    .locals 2

    .prologue
    .line 52
    iget v0, p0, Lorg/apache/poi/hssf/record/cont/ContinuableRecordOutput;->_totalPreviousRecordsSize:I

    iget-object v1, p0, Lorg/apache/poi/hssf/record/cont/ContinuableRecordOutput;->_ulrOutput:Lorg/apache/poi/hssf/record/cont/UnknownLengthRecordOutput;

    invoke-virtual {v1}, Lorg/apache/poi/hssf/record/cont/UnknownLengthRecordOutput;->getTotalSize()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method terminate()V
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lorg/apache/poi/hssf/record/cont/ContinuableRecordOutput;->_ulrOutput:Lorg/apache/poi/hssf/record/cont/UnknownLengthRecordOutput;

    invoke-virtual {v0}, Lorg/apache/poi/hssf/record/cont/UnknownLengthRecordOutput;->terminate()V

    .line 59
    return-void
.end method

.method public write([B)V
    .locals 1
    .param p1, "b"    # [B

    .prologue
    .line 204
    array-length v0, p1

    invoke-virtual {p0, v0}, Lorg/apache/poi/hssf/record/cont/ContinuableRecordOutput;->writeContinueIfRequired(I)V

    .line 205
    iget-object v0, p0, Lorg/apache/poi/hssf/record/cont/ContinuableRecordOutput;->_ulrOutput:Lorg/apache/poi/hssf/record/cont/UnknownLengthRecordOutput;

    invoke-virtual {v0, p1}, Lorg/apache/poi/hssf/record/cont/UnknownLengthRecordOutput;->write([B)V

    .line 206
    return-void
.end method

.method public write([BII)V
    .locals 5
    .param p1, "b"    # [B
    .param p2, "offset"    # I
    .param p3, "len"    # I

    .prologue
    .line 210
    const/4 v0, 0x0

    .line 212
    .local v0, "i":I
    :goto_0
    sub-int v3, p3, v0

    iget-object v4, p0, Lorg/apache/poi/hssf/record/cont/ContinuableRecordOutput;->_ulrOutput:Lorg/apache/poi/hssf/record/cont/UnknownLengthRecordOutput;

    invoke-virtual {v4}, Lorg/apache/poi/hssf/record/cont/UnknownLengthRecordOutput;->getAvailableSpace()I

    move-result v4

    div-int/lit8 v4, v4, 0x1

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v2

    .local v2, "nWritableChars":I
    move v1, v0

    .line 213
    .end local v0    # "i":I
    .local v1, "i":I
    :goto_1
    if-gtz v2, :cond_0

    .line 216
    if-lt v1, p3, :cond_1

    .line 221
    return-void

    .line 214
    :cond_0
    iget-object v3, p0, Lorg/apache/poi/hssf/record/cont/ContinuableRecordOutput;->_ulrOutput:Lorg/apache/poi/hssf/record/cont/UnknownLengthRecordOutput;

    add-int/lit8 v0, v1, 0x1

    .end local v1    # "i":I
    .restart local v0    # "i":I
    add-int v4, p2, v1

    aget-byte v4, p1, v4

    invoke-virtual {v3, v4}, Lorg/apache/poi/hssf/record/cont/UnknownLengthRecordOutput;->writeByte(I)V

    .line 213
    add-int/lit8 v2, v2, -0x1

    move v1, v0

    .end local v0    # "i":I
    .restart local v1    # "i":I
    goto :goto_1

    .line 219
    :cond_1
    invoke-virtual {p0}, Lorg/apache/poi/hssf/record/cont/ContinuableRecordOutput;->writeContinue()V

    move v0, v1

    .line 211
    .end local v1    # "i":I
    .restart local v0    # "i":I
    goto :goto_0
.end method

.method public writeByte(I)V
    .locals 1
    .param p1, "v"    # I

    .prologue
    .line 224
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lorg/apache/poi/hssf/record/cont/ContinuableRecordOutput;->writeContinueIfRequired(I)V

    .line 225
    iget-object v0, p0, Lorg/apache/poi/hssf/record/cont/ContinuableRecordOutput;->_ulrOutput:Lorg/apache/poi/hssf/record/cont/UnknownLengthRecordOutput;

    invoke-virtual {v0, p1}, Lorg/apache/poi/hssf/record/cont/UnknownLengthRecordOutput;->writeByte(I)V

    .line 226
    return-void
.end method

.method public writeContinue()V
    .locals 3

    .prologue
    .line 72
    iget-object v0, p0, Lorg/apache/poi/hssf/record/cont/ContinuableRecordOutput;->_ulrOutput:Lorg/apache/poi/hssf/record/cont/UnknownLengthRecordOutput;

    invoke-virtual {v0}, Lorg/apache/poi/hssf/record/cont/UnknownLengthRecordOutput;->terminate()V

    .line 73
    iget v0, p0, Lorg/apache/poi/hssf/record/cont/ContinuableRecordOutput;->_totalPreviousRecordsSize:I

    iget-object v1, p0, Lorg/apache/poi/hssf/record/cont/ContinuableRecordOutput;->_ulrOutput:Lorg/apache/poi/hssf/record/cont/UnknownLengthRecordOutput;

    invoke-virtual {v1}, Lorg/apache/poi/hssf/record/cont/UnknownLengthRecordOutput;->getTotalSize()I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lorg/apache/poi/hssf/record/cont/ContinuableRecordOutput;->_totalPreviousRecordsSize:I

    .line 74
    new-instance v0, Lorg/apache/poi/hssf/record/cont/UnknownLengthRecordOutput;

    iget-object v1, p0, Lorg/apache/poi/hssf/record/cont/ContinuableRecordOutput;->_out:Lorg/apache/poi/util/LittleEndianOutput;

    const/16 v2, 0x3c

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/hssf/record/cont/UnknownLengthRecordOutput;-><init>(Lorg/apache/poi/util/LittleEndianOutput;I)V

    iput-object v0, p0, Lorg/apache/poi/hssf/record/cont/ContinuableRecordOutput;->_ulrOutput:Lorg/apache/poi/hssf/record/cont/UnknownLengthRecordOutput;

    .line 75
    return-void
.end method

.method public writeContinueIfRequired(I)V
    .locals 1
    .param p1, "requiredContinuousSize"    # I

    .prologue
    .line 82
    iget-object v0, p0, Lorg/apache/poi/hssf/record/cont/ContinuableRecordOutput;->_ulrOutput:Lorg/apache/poi/hssf/record/cont/UnknownLengthRecordOutput;

    invoke-virtual {v0}, Lorg/apache/poi/hssf/record/cont/UnknownLengthRecordOutput;->getAvailableSpace()I

    move-result v0

    if-ge v0, p1, :cond_0

    .line 83
    invoke-virtual {p0}, Lorg/apache/poi/hssf/record/cont/ContinuableRecordOutput;->writeContinue()V

    .line 85
    :cond_0
    return-void
.end method

.method public writeDouble(D)V
    .locals 1
    .param p1, "v"    # D

    .prologue
    .line 228
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lorg/apache/poi/hssf/record/cont/ContinuableRecordOutput;->writeContinueIfRequired(I)V

    .line 229
    iget-object v0, p0, Lorg/apache/poi/hssf/record/cont/ContinuableRecordOutput;->_ulrOutput:Lorg/apache/poi/hssf/record/cont/UnknownLengthRecordOutput;

    invoke-virtual {v0, p1, p2}, Lorg/apache/poi/hssf/record/cont/UnknownLengthRecordOutput;->writeDouble(D)V

    .line 230
    return-void
.end method

.method public writeInt(I)V
    .locals 1
    .param p1, "v"    # I

    .prologue
    .line 232
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lorg/apache/poi/hssf/record/cont/ContinuableRecordOutput;->writeContinueIfRequired(I)V

    .line 233
    iget-object v0, p0, Lorg/apache/poi/hssf/record/cont/ContinuableRecordOutput;->_ulrOutput:Lorg/apache/poi/hssf/record/cont/UnknownLengthRecordOutput;

    invoke-virtual {v0, p1}, Lorg/apache/poi/hssf/record/cont/UnknownLengthRecordOutput;->writeInt(I)V

    .line 234
    return-void
.end method

.method public writeLong(J)V
    .locals 1
    .param p1, "v"    # J

    .prologue
    .line 236
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lorg/apache/poi/hssf/record/cont/ContinuableRecordOutput;->writeContinueIfRequired(I)V

    .line 237
    iget-object v0, p0, Lorg/apache/poi/hssf/record/cont/ContinuableRecordOutput;->_ulrOutput:Lorg/apache/poi/hssf/record/cont/UnknownLengthRecordOutput;

    invoke-virtual {v0, p1, p2}, Lorg/apache/poi/hssf/record/cont/UnknownLengthRecordOutput;->writeLong(J)V

    .line 238
    return-void
.end method

.method public writeShort(I)V
    .locals 1
    .param p1, "v"    # I

    .prologue
    .line 240
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lorg/apache/poi/hssf/record/cont/ContinuableRecordOutput;->writeContinueIfRequired(I)V

    .line 241
    iget-object v0, p0, Lorg/apache/poi/hssf/record/cont/ContinuableRecordOutput;->_ulrOutput:Lorg/apache/poi/hssf/record/cont/UnknownLengthRecordOutput;

    invoke-virtual {v0, p1}, Lorg/apache/poi/hssf/record/cont/UnknownLengthRecordOutput;->writeShort(I)V

    .line 242
    return-void
.end method

.method public writeString(Ljava/lang/String;II)V
    .locals 4
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "numberOfRichTextRuns"    # I
    .param p3, "extendedDataSize"    # I

    .prologue
    .line 144
    invoke-static {p1}, Lorg/apache/poi/util/StringUtil;->hasMultibyte(Ljava/lang/String;)Z

    move-result v0

    .line 146
    .local v0, "is16bitEncoded":Z
    const/4 v1, 0x4

    .line 147
    .local v1, "keepTogetherSize":I
    const/4 v2, 0x0

    .line 148
    .local v2, "optionFlags":I
    if-eqz v0, :cond_0

    .line 149
    or-int/lit8 v2, v2, 0x1

    .line 150
    add-int/lit8 v1, v1, 0x1

    .line 152
    :cond_0
    if-lez p2, :cond_1

    .line 153
    or-int/lit8 v2, v2, 0x8

    .line 154
    add-int/lit8 v1, v1, 0x2

    .line 156
    :cond_1
    if-lez p3, :cond_2

    .line 157
    or-int/lit8 v2, v2, 0x4

    .line 158
    add-int/lit8 v1, v1, 0x4

    .line 160
    :cond_2
    invoke-virtual {p0, v1}, Lorg/apache/poi/hssf/record/cont/ContinuableRecordOutput;->writeContinueIfRequired(I)V

    .line 161
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    invoke-virtual {p0, v3}, Lorg/apache/poi/hssf/record/cont/ContinuableRecordOutput;->writeShort(I)V

    .line 162
    invoke-virtual {p0, v2}, Lorg/apache/poi/hssf/record/cont/ContinuableRecordOutput;->writeByte(I)V

    .line 163
    if-lez p2, :cond_3

    .line 164
    invoke-virtual {p0, p2}, Lorg/apache/poi/hssf/record/cont/ContinuableRecordOutput;->writeShort(I)V

    .line 166
    :cond_3
    if-lez p3, :cond_4

    .line 167
    invoke-virtual {p0, p3}, Lorg/apache/poi/hssf/record/cont/ContinuableRecordOutput;->writeInt(I)V

    .line 169
    :cond_4
    invoke-direct {p0, p1, v0}, Lorg/apache/poi/hssf/record/cont/ContinuableRecordOutput;->writeCharacterData(Ljava/lang/String;Z)V

    .line 170
    return-void
.end method

.method public writeStringData(Ljava/lang/String;)V
    .locals 3
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 105
    invoke-static {p1}, Lorg/apache/poi/util/StringUtil;->hasMultibyte(Ljava/lang/String;)Z

    move-result v0

    .line 107
    .local v0, "is16bitEncoded":Z
    const/4 v1, 0x2

    .line 108
    .local v1, "keepTogetherSize":I
    const/4 v2, 0x0

    .line 109
    .local v2, "optionFlags":I
    if-eqz v0, :cond_0

    .line 110
    or-int/lit8 v2, v2, 0x1

    .line 111
    add-int/lit8 v1, v1, 0x1

    .line 113
    :cond_0
    invoke-virtual {p0, v1}, Lorg/apache/poi/hssf/record/cont/ContinuableRecordOutput;->writeContinueIfRequired(I)V

    .line 114
    invoke-virtual {p0, v2}, Lorg/apache/poi/hssf/record/cont/ContinuableRecordOutput;->writeByte(I)V

    .line 115
    invoke-direct {p0, p1, v0}, Lorg/apache/poi/hssf/record/cont/ContinuableRecordOutput;->writeCharacterData(Ljava/lang/String;Z)V

    .line 116
    return-void
.end method
