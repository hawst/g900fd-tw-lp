.class public Lorg/apache/poi/hssf/usermodel/HSSFChart$HSSFSeries;
.super Ljava/lang/Object;
.source "HSSFChart.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/poi/hssf/usermodel/HSSFChart;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "HSSFSeries"
.end annotation


# instance fields
.field private dataCategoryLabels:Lorg/apache/poi/hssf/record/chart/LinkedDataRecord;

.field private dataName:Lorg/apache/poi/hssf/record/chart/LinkedDataRecord;

.field private dataSecondaryCategoryLabels:Lorg/apache/poi/hssf/record/chart/LinkedDataRecord;

.field private dataValues:Lorg/apache/poi/hssf/record/chart/LinkedDataRecord;

.field private series:Lorg/apache/poi/hssf/record/chart/SeriesRecord;

.field private seriesTitleText:Lorg/apache/poi/hssf/record/chart/SeriesTextRecord;

.field final synthetic this$0:Lorg/apache/poi/hssf/usermodel/HSSFChart;


# direct methods
.method constructor <init>(Lorg/apache/poi/hssf/usermodel/HSSFChart;Lorg/apache/poi/hssf/record/chart/SeriesRecord;)V
    .locals 0
    .param p2, "series"    # Lorg/apache/poi/hssf/record/chart/SeriesRecord;

    .prologue
    .line 981
    iput-object p1, p0, Lorg/apache/poi/hssf/usermodel/HSSFChart$HSSFSeries;->this$0:Lorg/apache/poi/hssf/usermodel/HSSFChart;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 982
    iput-object p2, p0, Lorg/apache/poi/hssf/usermodel/HSSFChart$HSSFSeries;->series:Lorg/apache/poi/hssf/record/chart/SeriesRecord;

    .line 983
    return-void
.end method

.method static synthetic access$0(Lorg/apache/poi/hssf/usermodel/HSSFChart$HSSFSeries;Lorg/apache/poi/hssf/record/chart/SeriesTextRecord;)V
    .locals 0

    .prologue
    .line 975
    iput-object p1, p0, Lorg/apache/poi/hssf/usermodel/HSSFChart$HSSFSeries;->seriesTitleText:Lorg/apache/poi/hssf/record/chart/SeriesTextRecord;

    return-void
.end method

.method static synthetic access$1(Lorg/apache/poi/hssf/usermodel/HSSFChart$HSSFSeries;)Lorg/apache/poi/hssf/record/chart/SeriesRecord;
    .locals 1

    .prologue
    .line 974
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFChart$HSSFSeries;->series:Lorg/apache/poi/hssf/record/chart/SeriesRecord;

    return-object v0
.end method

.method private getCellRange(Lorg/apache/poi/hssf/record/chart/LinkedDataRecord;)Lorg/apache/poi/ss/util/CellRangeAddressBase;
    .locals 10
    .param p1, "linkedDataRecord"    # Lorg/apache/poi/hssf/record/chart/LinkedDataRecord;

    .prologue
    .line 1073
    if-nez p1, :cond_0

    .line 1075
    const/4 v6, 0x0

    .line 1095
    :goto_0
    return-object v6

    .line 1078
    :cond_0
    const/4 v2, 0x0

    .line 1079
    .local v2, "firstRow":I
    const/4 v4, 0x0

    .line 1080
    .local v4, "lastRow":I
    const/4 v1, 0x0

    .line 1081
    .local v1, "firstCol":I
    const/4 v3, 0x0

    .line 1083
    .local v3, "lastCol":I
    invoke-virtual {p1}, Lorg/apache/poi/hssf/record/chart/LinkedDataRecord;->getFormulaOfLink()[Lorg/apache/poi/ss/formula/ptg/Ptg;

    move-result-object v7

    array-length v8, v7

    const/4 v6, 0x0

    :goto_1
    if-lt v6, v8, :cond_1

    .line 1095
    new-instance v6, Lorg/apache/poi/ss/util/CellRangeAddress;

    invoke-direct {v6, v2, v4, v1, v3}, Lorg/apache/poi/ss/util/CellRangeAddress;-><init>(IIII)V

    goto :goto_0

    .line 1083
    :cond_1
    aget-object v5, v7, v6

    .line 1084
    .local v5, "ptg":Lorg/apache/poi/ss/formula/ptg/Ptg;
    instance-of v9, v5, Lorg/apache/poi/ss/formula/ptg/AreaPtgBase;

    if-eqz v9, :cond_2

    move-object v0, v5

    .line 1085
    check-cast v0, Lorg/apache/poi/ss/formula/ptg/AreaPtgBase;

    .line 1087
    .local v0, "areaPtg":Lorg/apache/poi/ss/formula/ptg/AreaPtgBase;
    invoke-virtual {v0}, Lorg/apache/poi/ss/formula/ptg/AreaPtgBase;->getFirstRow()I

    move-result v2

    .line 1088
    invoke-virtual {v0}, Lorg/apache/poi/ss/formula/ptg/AreaPtgBase;->getLastRow()I

    move-result v4

    .line 1090
    invoke-virtual {v0}, Lorg/apache/poi/ss/formula/ptg/AreaPtgBase;->getFirstColumn()I

    move-result v1

    .line 1091
    invoke-virtual {v0}, Lorg/apache/poi/ss/formula/ptg/AreaPtgBase;->getLastColumn()I

    move-result v3

    .line 1083
    .end local v0    # "areaPtg":Lorg/apache/poi/ss/formula/ptg/AreaPtgBase;
    :cond_2
    add-int/lit8 v6, v6, 0x1

    goto :goto_1
.end method

.method private setVerticalCellRange(Lorg/apache/poi/hssf/record/chart/LinkedDataRecord;Lorg/apache/poi/ss/util/CellRangeAddressBase;)Ljava/lang/Integer;
    .locals 9
    .param p1, "linkedDataRecord"    # Lorg/apache/poi/hssf/record/chart/LinkedDataRecord;
    .param p2, "range"    # Lorg/apache/poi/ss/util/CellRangeAddressBase;

    .prologue
    .line 1108
    if-nez p1, :cond_0

    .line 1110
    const/4 v5, 0x0

    .line 1133
    :goto_0
    return-object v5

    .line 1113
    :cond_0
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1115
    .local v3, "ptgList":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/ss/formula/ptg/Ptg;>;"
    invoke-virtual {p2}, Lorg/apache/poi/ss/util/CellRangeAddressBase;->getLastRow()I

    move-result v5

    invoke-virtual {p2}, Lorg/apache/poi/ss/util/CellRangeAddressBase;->getFirstRow()I

    move-result v6

    sub-int/2addr v5, v6

    add-int/lit8 v4, v5, 0x1

    .line 1116
    .local v4, "rowCount":I
    invoke-virtual {p2}, Lorg/apache/poi/ss/util/CellRangeAddressBase;->getLastColumn()I

    move-result v5

    invoke-virtual {p2}, Lorg/apache/poi/ss/util/CellRangeAddressBase;->getFirstColumn()I

    move-result v6

    sub-int/2addr v5, v6

    add-int/lit8 v1, v5, 0x1

    .line 1118
    .local v1, "colCount":I
    invoke-virtual {p1}, Lorg/apache/poi/hssf/record/chart/LinkedDataRecord;->getFormulaOfLink()[Lorg/apache/poi/ss/formula/ptg/Ptg;

    move-result-object v6

    array-length v7, v6

    const/4 v5, 0x0

    :goto_1
    if-lt v5, v7, :cond_1

    .line 1131
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v5

    new-array v5, v5, [Lorg/apache/poi/ss/formula/ptg/Ptg;

    invoke-interface {v3, v5}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [Lorg/apache/poi/ss/formula/ptg/Ptg;

    invoke-virtual {p1, v5}, Lorg/apache/poi/hssf/record/chart/LinkedDataRecord;->setFormulaOfLink([Lorg/apache/poi/ss/formula/ptg/Ptg;)V

    .line 1133
    mul-int v5, v4, v1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    goto :goto_0

    .line 1118
    :cond_1
    aget-object v2, v6, v5

    .line 1119
    .local v2, "ptg":Lorg/apache/poi/ss/formula/ptg/Ptg;
    instance-of v8, v2, Lorg/apache/poi/ss/formula/ptg/AreaPtgBase;

    if-eqz v8, :cond_2

    move-object v0, v2

    .line 1120
    check-cast v0, Lorg/apache/poi/ss/formula/ptg/AreaPtgBase;

    .line 1122
    .local v0, "areaPtg":Lorg/apache/poi/ss/formula/ptg/AreaPtgBase;
    invoke-virtual {p2}, Lorg/apache/poi/ss/util/CellRangeAddressBase;->getFirstRow()I

    move-result v8

    invoke-virtual {v0, v8}, Lorg/apache/poi/ss/formula/ptg/AreaPtgBase;->setFirstRow(I)V

    .line 1123
    invoke-virtual {p2}, Lorg/apache/poi/ss/util/CellRangeAddressBase;->getLastRow()I

    move-result v8

    invoke-virtual {v0, v8}, Lorg/apache/poi/ss/formula/ptg/AreaPtgBase;->setLastRow(I)V

    .line 1125
    invoke-virtual {p2}, Lorg/apache/poi/ss/util/CellRangeAddressBase;->getFirstColumn()I

    move-result v8

    invoke-virtual {v0, v8}, Lorg/apache/poi/ss/formula/ptg/AreaPtgBase;->setFirstColumn(I)V

    .line 1126
    invoke-virtual {p2}, Lorg/apache/poi/ss/util/CellRangeAddressBase;->getLastColumn()I

    move-result v8

    invoke-virtual {v0, v8}, Lorg/apache/poi/ss/formula/ptg/AreaPtgBase;->setLastColumn(I)V

    .line 1127
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1118
    .end local v0    # "areaPtg":Lorg/apache/poi/ss/formula/ptg/AreaPtgBase;
    :cond_2
    add-int/lit8 v5, v5, 0x1

    goto :goto_1
.end method


# virtual methods
.method public getCategoryLabelsCellRange()Lorg/apache/poi/ss/util/CellRangeAddressBase;
    .locals 1

    .prologue
    .line 1103
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFChart$HSSFSeries;->dataCategoryLabels:Lorg/apache/poi/hssf/record/chart/LinkedDataRecord;

    invoke-direct {p0, v0}, Lorg/apache/poi/hssf/usermodel/HSSFChart$HSSFSeries;->getCellRange(Lorg/apache/poi/hssf/record/chart/LinkedDataRecord;)Lorg/apache/poi/ss/util/CellRangeAddressBase;

    move-result-object v0

    return-object v0
.end method

.method public getDataCategoryLabels()Lorg/apache/poi/hssf/record/chart/LinkedDataRecord;
    .locals 1

    .prologue
    .line 1055
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFChart$HSSFSeries;->dataCategoryLabels:Lorg/apache/poi/hssf/record/chart/LinkedDataRecord;

    return-object v0
.end method

.method public getDataName()Lorg/apache/poi/hssf/record/chart/LinkedDataRecord;
    .locals 1

    .prologue
    .line 1041
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFChart$HSSFSeries;->dataName:Lorg/apache/poi/hssf/record/chart/LinkedDataRecord;

    return-object v0
.end method

.method public getDataSecondaryCategoryLabels()Lorg/apache/poi/hssf/record/chart/LinkedDataRecord;
    .locals 1

    .prologue
    .line 1062
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFChart$HSSFSeries;->dataSecondaryCategoryLabels:Lorg/apache/poi/hssf/record/chart/LinkedDataRecord;

    return-object v0
.end method

.method public getDataValues()Lorg/apache/poi/hssf/record/chart/LinkedDataRecord;
    .locals 1

    .prologue
    .line 1048
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFChart$HSSFSeries;->dataValues:Lorg/apache/poi/hssf/record/chart/LinkedDataRecord;

    return-object v0
.end method

.method public getNumValues()S
    .locals 1

    .prologue
    .line 1004
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFChart$HSSFSeries;->series:Lorg/apache/poi/hssf/record/chart/SeriesRecord;

    invoke-virtual {v0}, Lorg/apache/poi/hssf/record/chart/SeriesRecord;->getNumValues()S

    move-result v0

    return v0
.end method

.method public getSeries()Lorg/apache/poi/hssf/record/chart/SeriesRecord;
    .locals 1

    .prologue
    .line 1069
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFChart$HSSFSeries;->series:Lorg/apache/poi/hssf/record/chart/SeriesRecord;

    return-object v0
.end method

.method public getSeriesTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1018
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFChart$HSSFSeries;->seriesTitleText:Lorg/apache/poi/hssf/record/chart/SeriesTextRecord;

    if-eqz v0, :cond_0

    .line 1019
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFChart$HSSFSeries;->seriesTitleText:Lorg/apache/poi/hssf/record/chart/SeriesTextRecord;

    invoke-virtual {v0}, Lorg/apache/poi/hssf/record/chart/SeriesTextRecord;->getText()Ljava/lang/String;

    move-result-object v0

    .line 1021
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getValueType()S
    .locals 1

    .prologue
    .line 1010
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFChart$HSSFSeries;->series:Lorg/apache/poi/hssf/record/chart/SeriesRecord;

    invoke-virtual {v0}, Lorg/apache/poi/hssf/record/chart/SeriesRecord;->getValuesDataType()S

    move-result v0

    return v0
.end method

.method public getValuesCellRange()Lorg/apache/poi/ss/util/CellRangeAddressBase;
    .locals 1

    .prologue
    .line 1099
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFChart$HSSFSeries;->dataValues:Lorg/apache/poi/hssf/record/chart/LinkedDataRecord;

    invoke-direct {p0, v0}, Lorg/apache/poi/hssf/usermodel/HSSFChart$HSSFSeries;->getCellRange(Lorg/apache/poi/hssf/record/chart/LinkedDataRecord;)Lorg/apache/poi/ss/util/CellRangeAddressBase;

    move-result-object v0

    return-object v0
.end method

.method insertData(Lorg/apache/poi/hssf/record/chart/LinkedDataRecord;)V
    .locals 1
    .param p1, "data"    # Lorg/apache/poi/hssf/record/chart/LinkedDataRecord;

    .prologue
    .line 986
    invoke-virtual {p1}, Lorg/apache/poi/hssf/record/chart/LinkedDataRecord;->getLinkType()B

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 996
    :goto_0
    return-void

    .line 987
    :pswitch_0
    iput-object p1, p0, Lorg/apache/poi/hssf/usermodel/HSSFChart$HSSFSeries;->dataName:Lorg/apache/poi/hssf/record/chart/LinkedDataRecord;

    goto :goto_0

    .line 989
    :pswitch_1
    iput-object p1, p0, Lorg/apache/poi/hssf/usermodel/HSSFChart$HSSFSeries;->dataValues:Lorg/apache/poi/hssf/record/chart/LinkedDataRecord;

    goto :goto_0

    .line 991
    :pswitch_2
    iput-object p1, p0, Lorg/apache/poi/hssf/usermodel/HSSFChart$HSSFSeries;->dataCategoryLabels:Lorg/apache/poi/hssf/record/chart/LinkedDataRecord;

    goto :goto_0

    .line 993
    :pswitch_3
    iput-object p1, p0, Lorg/apache/poi/hssf/usermodel/HSSFChart$HSSFSeries;->dataSecondaryCategoryLabels:Lorg/apache/poi/hssf/record/chart/LinkedDataRecord;

    goto :goto_0

    .line 986
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public setCategoryLabelsCellRange(Lorg/apache/poi/ss/util/CellRangeAddressBase;)V
    .locals 3
    .param p1, "range"    # Lorg/apache/poi/ss/util/CellRangeAddressBase;

    .prologue
    .line 1147
    iget-object v1, p0, Lorg/apache/poi/hssf/usermodel/HSSFChart$HSSFSeries;->dataCategoryLabels:Lorg/apache/poi/hssf/record/chart/LinkedDataRecord;

    invoke-direct {p0, v1, p1}, Lorg/apache/poi/hssf/usermodel/HSSFChart$HSSFSeries;->setVerticalCellRange(Lorg/apache/poi/hssf/record/chart/LinkedDataRecord;Lorg/apache/poi/ss/util/CellRangeAddressBase;)Ljava/lang/Integer;

    move-result-object v0

    .line 1148
    .local v0, "count":Ljava/lang/Integer;
    if-nez v0, :cond_0

    .line 1154
    :goto_0
    return-void

    .line 1153
    :cond_0
    iget-object v1, p0, Lorg/apache/poi/hssf/usermodel/HSSFChart$HSSFSeries;->series:Lorg/apache/poi/hssf/record/chart/SeriesRecord;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    int-to-short v2, v2

    invoke-virtual {v1, v2}, Lorg/apache/poi/hssf/record/chart/SeriesRecord;->setNumCategories(S)V

    goto :goto_0
.end method

.method public setSeriesTitle(Ljava/lang/String;)V
    .locals 2
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 1030
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFChart$HSSFSeries;->seriesTitleText:Lorg/apache/poi/hssf/record/chart/SeriesTextRecord;

    if-eqz v0, :cond_0

    .line 1031
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFChart$HSSFSeries;->seriesTitleText:Lorg/apache/poi/hssf/record/chart/SeriesTextRecord;

    invoke-virtual {v0, p1}, Lorg/apache/poi/hssf/record/chart/SeriesTextRecord;->setText(Ljava/lang/String;)V

    .line 1035
    return-void

    .line 1033
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "No series title found to change"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method setSeriesTitleText(Lorg/apache/poi/hssf/record/chart/SeriesTextRecord;)V
    .locals 0
    .param p1, "seriesTitleText"    # Lorg/apache/poi/hssf/record/chart/SeriesTextRecord;

    .prologue
    .line 1000
    iput-object p1, p0, Lorg/apache/poi/hssf/usermodel/HSSFChart$HSSFSeries;->seriesTitleText:Lorg/apache/poi/hssf/record/chart/SeriesTextRecord;

    .line 1001
    return-void
.end method

.method public setValuesCellRange(Lorg/apache/poi/ss/util/CellRangeAddressBase;)V
    .locals 3
    .param p1, "range"    # Lorg/apache/poi/ss/util/CellRangeAddressBase;

    .prologue
    .line 1137
    iget-object v1, p0, Lorg/apache/poi/hssf/usermodel/HSSFChart$HSSFSeries;->dataValues:Lorg/apache/poi/hssf/record/chart/LinkedDataRecord;

    invoke-direct {p0, v1, p1}, Lorg/apache/poi/hssf/usermodel/HSSFChart$HSSFSeries;->setVerticalCellRange(Lorg/apache/poi/hssf/record/chart/LinkedDataRecord;Lorg/apache/poi/ss/util/CellRangeAddressBase;)Ljava/lang/Integer;

    move-result-object v0

    .line 1138
    .local v0, "count":Ljava/lang/Integer;
    if-nez v0, :cond_0

    .line 1144
    :goto_0
    return-void

    .line 1143
    :cond_0
    iget-object v1, p0, Lorg/apache/poi/hssf/usermodel/HSSFChart$HSSFSeries;->series:Lorg/apache/poi/hssf/record/chart/SeriesRecord;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    int-to-short v2, v2

    invoke-virtual {v1, v2}, Lorg/apache/poi/hssf/record/chart/SeriesRecord;->setNumValues(S)V

    goto :goto_0
.end method
