.class public final Lorg/apache/poi/hssf/util/HSSFColor$GREY_25_PERCENT;
.super Lorg/apache/poi/hssf/util/HSSFColor;
.source "HSSFColor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/poi/hssf/util/HSSFColor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "GREY_25_PERCENT"
.end annotation


# static fields
.field public static final hexString:Ljava/lang/String; = "C0C0:C0C0:C0C0"

.field public static final index:S = 0x16s

.field public static final triplet:[S


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1192
    const/4 v0, 0x3

    new-array v0, v0, [S

    fill-array-data v0, :array_0

    .line 1191
    sput-object v0, Lorg/apache/poi/hssf/util/HSSFColor$GREY_25_PERCENT;->triplet:[S

    .line 1195
    return-void

    .line 1192
    nop

    :array_0
    .array-data 2
        0xc0s
        0xc0s
        0xc0s
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1187
    invoke-direct {p0}, Lorg/apache/poi/hssf/util/HSSFColor;-><init>()V

    return-void
.end method


# virtual methods
.method public getHexString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1209
    const-string/jumbo v0, "C0C0:C0C0:C0C0"

    return-object v0
.end method

.method public getIndex()S
    .locals 1

    .prologue
    .line 1199
    const/16 v0, 0x16

    return v0
.end method

.method public getTriplet()[S
    .locals 1

    .prologue
    .line 1204
    sget-object v0, Lorg/apache/poi/hssf/util/HSSFColor$GREY_25_PERCENT;->triplet:[S

    return-object v0
.end method
