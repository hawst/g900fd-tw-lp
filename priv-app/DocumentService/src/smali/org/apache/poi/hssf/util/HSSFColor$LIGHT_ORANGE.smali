.class public final Lorg/apache/poi/hssf/util/HSSFColor$LIGHT_ORANGE;
.super Lorg/apache/poi/hssf/util/HSSFColor;
.source "HSSFColor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/poi/hssf/util/HSSFColor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "LIGHT_ORANGE"
.end annotation


# static fields
.field public static final hexString:Ljava/lang/String; = "FFFF:9999:0"

.field public static final index:S = 0x34s

.field public static final triplet:[S


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 752
    const/4 v0, 0x3

    new-array v0, v0, [S

    const/4 v1, 0x0

    .line 753
    const/16 v2, 0xff

    aput-short v2, v0, v1

    const/4 v1, 0x1

    const/16 v2, 0x99

    aput-short v2, v0, v1

    .line 751
    sput-object v0, Lorg/apache/poi/hssf/util/HSSFColor$LIGHT_ORANGE;->triplet:[S

    .line 755
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 747
    invoke-direct {p0}, Lorg/apache/poi/hssf/util/HSSFColor;-><init>()V

    return-void
.end method


# virtual methods
.method public getHexString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 769
    const-string/jumbo v0, "FFFF:9999:0"

    return-object v0
.end method

.method public getIndex()S
    .locals 1

    .prologue
    .line 759
    const/16 v0, 0x34

    return v0
.end method

.method public getTriplet()[S
    .locals 1

    .prologue
    .line 764
    sget-object v0, Lorg/apache/poi/hssf/util/HSSFColor$LIGHT_ORANGE;->triplet:[S

    return-object v0
.end method
