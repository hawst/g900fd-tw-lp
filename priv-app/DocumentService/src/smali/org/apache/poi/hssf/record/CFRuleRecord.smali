.class public final Lorg/apache/poi/hssf/record/CFRuleRecord;
.super Lorg/apache/poi/hssf/record/StandardRecord;
.source "CFRuleRecord.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/poi/hssf/record/CFRuleRecord$ComparisonOperator;
    }
.end annotation


# static fields
.field public static final CONDITION_TYPE_CELL_VALUE_IS:B = 0x1t

.field public static final CONDITION_TYPE_FORMULA:B = 0x2t

.field private static final align:Lorg/apache/poi/util/BitField;

.field private static final bord:Lorg/apache/poi/util/BitField;

.field private static final bordBlTr:Lorg/apache/poi/util/BitField;

.field private static final bordBot:Lorg/apache/poi/util/BitField;

.field private static final bordLeft:Lorg/apache/poi/util/BitField;

.field private static final bordRight:Lorg/apache/poi/util/BitField;

.field private static final bordTlBr:Lorg/apache/poi/util/BitField;

.field private static final bordTop:Lorg/apache/poi/util/BitField;

.field private static final fmtBlockBits:Lorg/apache/poi/util/BitField;

.field private static final font:Lorg/apache/poi/util/BitField;

.field private static final modificationBits:Lorg/apache/poi/util/BitField;

.field private static final patt:Lorg/apache/poi/util/BitField;

.field private static final pattBgCol:Lorg/apache/poi/util/BitField;

.field private static final pattCol:Lorg/apache/poi/util/BitField;

.field private static final pattStyle:Lorg/apache/poi/util/BitField;

.field private static final prot:Lorg/apache/poi/util/BitField;

.field public static final sid:S = 0x1b1s

.field private static final undocumented:Lorg/apache/poi/util/BitField;


# instance fields
.field private _borderFormatting:Lorg/apache/poi/hssf/record/cf/BorderFormatting;

.field private _fontFormatting:Lorg/apache/poi/hssf/record/cf/FontFormatting;

.field private _patternFormatting:Lorg/apache/poi/hssf/record/cf/PatternFormatting;

.field private field_17_formula1:Lorg/apache/poi/ss/formula/Formula;

.field private field_18_formula2:Lorg/apache/poi/ss/formula/Formula;

.field private field_1_condition_type:B

.field private field_2_comparison_operator:B

.field private field_5_options:I

.field private field_6_not_used:S


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 61
    const v0, 0x3fffff

    invoke-static {v0}, Lorg/apache/poi/hssf/record/CFRuleRecord;->bf(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hssf/record/CFRuleRecord;->modificationBits:Lorg/apache/poi/util/BitField;

    .line 81
    const/16 v0, 0x400

    invoke-static {v0}, Lorg/apache/poi/hssf/record/CFRuleRecord;->bf(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hssf/record/CFRuleRecord;->bordLeft:Lorg/apache/poi/util/BitField;

    .line 82
    const/16 v0, 0x800

    invoke-static {v0}, Lorg/apache/poi/hssf/record/CFRuleRecord;->bf(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hssf/record/CFRuleRecord;->bordRight:Lorg/apache/poi/util/BitField;

    .line 83
    const/16 v0, 0x1000

    invoke-static {v0}, Lorg/apache/poi/hssf/record/CFRuleRecord;->bf(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hssf/record/CFRuleRecord;->bordTop:Lorg/apache/poi/util/BitField;

    .line 84
    const/16 v0, 0x2000

    invoke-static {v0}, Lorg/apache/poi/hssf/record/CFRuleRecord;->bf(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hssf/record/CFRuleRecord;->bordBot:Lorg/apache/poi/util/BitField;

    .line 85
    const/16 v0, 0x4000

    invoke-static {v0}, Lorg/apache/poi/hssf/record/CFRuleRecord;->bf(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hssf/record/CFRuleRecord;->bordTlBr:Lorg/apache/poi/util/BitField;

    .line 86
    const v0, 0x8000

    invoke-static {v0}, Lorg/apache/poi/hssf/record/CFRuleRecord;->bf(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hssf/record/CFRuleRecord;->bordBlTr:Lorg/apache/poi/util/BitField;

    .line 87
    const/high16 v0, 0x10000

    invoke-static {v0}, Lorg/apache/poi/hssf/record/CFRuleRecord;->bf(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hssf/record/CFRuleRecord;->pattStyle:Lorg/apache/poi/util/BitField;

    .line 88
    const/high16 v0, 0x20000

    invoke-static {v0}, Lorg/apache/poi/hssf/record/CFRuleRecord;->bf(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hssf/record/CFRuleRecord;->pattCol:Lorg/apache/poi/util/BitField;

    .line 89
    const/high16 v0, 0x40000

    invoke-static {v0}, Lorg/apache/poi/hssf/record/CFRuleRecord;->bf(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hssf/record/CFRuleRecord;->pattBgCol:Lorg/apache/poi/util/BitField;

    .line 91
    const/high16 v0, 0x3c00000

    invoke-static {v0}, Lorg/apache/poi/hssf/record/CFRuleRecord;->bf(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hssf/record/CFRuleRecord;->undocumented:Lorg/apache/poi/util/BitField;

    .line 92
    const/high16 v0, 0x7c000000

    invoke-static {v0}, Lorg/apache/poi/hssf/record/CFRuleRecord;->bf(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hssf/record/CFRuleRecord;->fmtBlockBits:Lorg/apache/poi/util/BitField;

    .line 93
    const/high16 v0, 0x4000000

    invoke-static {v0}, Lorg/apache/poi/hssf/record/CFRuleRecord;->bf(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hssf/record/CFRuleRecord;->font:Lorg/apache/poi/util/BitField;

    .line 94
    const/high16 v0, 0x8000000

    invoke-static {v0}, Lorg/apache/poi/hssf/record/CFRuleRecord;->bf(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hssf/record/CFRuleRecord;->align:Lorg/apache/poi/util/BitField;

    .line 95
    const/high16 v0, 0x10000000

    invoke-static {v0}, Lorg/apache/poi/hssf/record/CFRuleRecord;->bf(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hssf/record/CFRuleRecord;->bord:Lorg/apache/poi/util/BitField;

    .line 96
    const/high16 v0, 0x20000000

    invoke-static {v0}, Lorg/apache/poi/hssf/record/CFRuleRecord;->bf(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hssf/record/CFRuleRecord;->patt:Lorg/apache/poi/util/BitField;

    .line 97
    const/high16 v0, 0x40000000    # 2.0f

    invoke-static {v0}, Lorg/apache/poi/hssf/record/CFRuleRecord;->bf(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hssf/record/CFRuleRecord;->prot:Lorg/apache/poi/util/BitField;

    return-void
.end method

.method private constructor <init>(BB)V
    .locals 4
    .param p1, "conditionType"    # B
    .param p2, "comparisonOperation"    # B

    .prologue
    const/4 v3, 0x0

    .line 118
    invoke-direct {p0}, Lorg/apache/poi/hssf/record/StandardRecord;-><init>()V

    .line 120
    iput-byte p1, p0, Lorg/apache/poi/hssf/record/CFRuleRecord;->field_1_condition_type:B

    .line 121
    iput-byte p2, p0, Lorg/apache/poi/hssf/record/CFRuleRecord;->field_2_comparison_operator:B

    .line 124
    sget-object v0, Lorg/apache/poi/hssf/record/CFRuleRecord;->modificationBits:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hssf/record/CFRuleRecord;->field_5_options:I

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Lorg/apache/poi/util/BitField;->setValue(II)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hssf/record/CFRuleRecord;->field_5_options:I

    .line 126
    sget-object v0, Lorg/apache/poi/hssf/record/CFRuleRecord;->fmtBlockBits:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hssf/record/CFRuleRecord;->field_5_options:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lorg/apache/poi/util/BitField;->setValue(II)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hssf/record/CFRuleRecord;->field_5_options:I

    .line 127
    sget-object v0, Lorg/apache/poi/hssf/record/CFRuleRecord;->undocumented:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hssf/record/CFRuleRecord;->field_5_options:I

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->clear(I)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hssf/record/CFRuleRecord;->field_5_options:I

    .line 129
    const/16 v0, -0x7ffe

    iput-short v0, p0, Lorg/apache/poi/hssf/record/CFRuleRecord;->field_6_not_used:S

    .line 130
    iput-object v3, p0, Lorg/apache/poi/hssf/record/CFRuleRecord;->_fontFormatting:Lorg/apache/poi/hssf/record/cf/FontFormatting;

    .line 131
    iput-object v3, p0, Lorg/apache/poi/hssf/record/CFRuleRecord;->_borderFormatting:Lorg/apache/poi/hssf/record/cf/BorderFormatting;

    .line 132
    iput-object v3, p0, Lorg/apache/poi/hssf/record/CFRuleRecord;->_patternFormatting:Lorg/apache/poi/hssf/record/cf/PatternFormatting;

    .line 133
    sget-object v0, Lorg/apache/poi/ss/formula/ptg/Ptg;->EMPTY_PTG_ARRAY:[Lorg/apache/poi/ss/formula/ptg/Ptg;

    invoke-static {v0}, Lorg/apache/poi/ss/formula/Formula;->create([Lorg/apache/poi/ss/formula/ptg/Ptg;)Lorg/apache/poi/ss/formula/Formula;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/poi/hssf/record/CFRuleRecord;->field_17_formula1:Lorg/apache/poi/ss/formula/Formula;

    .line 134
    sget-object v0, Lorg/apache/poi/ss/formula/ptg/Ptg;->EMPTY_PTG_ARRAY:[Lorg/apache/poi/ss/formula/ptg/Ptg;

    invoke-static {v0}, Lorg/apache/poi/ss/formula/Formula;->create([Lorg/apache/poi/ss/formula/ptg/Ptg;)Lorg/apache/poi/ss/formula/Formula;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/poi/hssf/record/CFRuleRecord;->field_18_formula2:Lorg/apache/poi/ss/formula/Formula;

    .line 135
    return-void
.end method

.method private constructor <init>(BB[Lorg/apache/poi/ss/formula/ptg/Ptg;[Lorg/apache/poi/ss/formula/ptg/Ptg;)V
    .locals 1
    .param p1, "conditionType"    # B
    .param p2, "comparisonOperation"    # B
    .param p3, "formula1"    # [Lorg/apache/poi/ss/formula/ptg/Ptg;
    .param p4, "formula2"    # [Lorg/apache/poi/ss/formula/ptg/Ptg;

    .prologue
    .line 138
    invoke-direct {p0, p1, p2}, Lorg/apache/poi/hssf/record/CFRuleRecord;-><init>(BB)V

    .line 139
    invoke-static {p3}, Lorg/apache/poi/ss/formula/Formula;->create([Lorg/apache/poi/ss/formula/ptg/Ptg;)Lorg/apache/poi/ss/formula/Formula;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/poi/hssf/record/CFRuleRecord;->field_17_formula1:Lorg/apache/poi/ss/formula/Formula;

    .line 140
    invoke-static {p4}, Lorg/apache/poi/ss/formula/Formula;->create([Lorg/apache/poi/ss/formula/ptg/Ptg;)Lorg/apache/poi/ss/formula/Formula;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/poi/hssf/record/CFRuleRecord;->field_18_formula2:Lorg/apache/poi/ss/formula/Formula;

    .line 141
    return-void
.end method

.method public constructor <init>(Lorg/apache/poi/hssf/record/RecordInputStream;)V
    .locals 3
    .param p1, "in"    # Lorg/apache/poi/hssf/record/RecordInputStream;

    .prologue
    .line 161
    invoke-direct {p0}, Lorg/apache/poi/hssf/record/StandardRecord;-><init>()V

    .line 162
    invoke-virtual {p1}, Lorg/apache/poi/hssf/record/RecordInputStream;->readByte()B

    move-result v2

    iput-byte v2, p0, Lorg/apache/poi/hssf/record/CFRuleRecord;->field_1_condition_type:B

    .line 163
    invoke-virtual {p1}, Lorg/apache/poi/hssf/record/RecordInputStream;->readByte()B

    move-result v2

    iput-byte v2, p0, Lorg/apache/poi/hssf/record/CFRuleRecord;->field_2_comparison_operator:B

    .line 164
    invoke-virtual {p1}, Lorg/apache/poi/hssf/record/RecordInputStream;->readUShort()I

    move-result v0

    .line 165
    .local v0, "field_3_formula1_len":I
    invoke-virtual {p1}, Lorg/apache/poi/hssf/record/RecordInputStream;->readUShort()I

    move-result v1

    .line 166
    .local v1, "field_4_formula2_len":I
    invoke-virtual {p1}, Lorg/apache/poi/hssf/record/RecordInputStream;->readInt()I

    move-result v2

    iput v2, p0, Lorg/apache/poi/hssf/record/CFRuleRecord;->field_5_options:I

    .line 167
    invoke-virtual {p1}, Lorg/apache/poi/hssf/record/RecordInputStream;->readShort()S

    move-result v2

    iput-short v2, p0, Lorg/apache/poi/hssf/record/CFRuleRecord;->field_6_not_used:S

    .line 169
    invoke-virtual {p0}, Lorg/apache/poi/hssf/record/CFRuleRecord;->containsFontFormattingBlock()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 170
    new-instance v2, Lorg/apache/poi/hssf/record/cf/FontFormatting;

    invoke-direct {v2, p1}, Lorg/apache/poi/hssf/record/cf/FontFormatting;-><init>(Lorg/apache/poi/hssf/record/RecordInputStream;)V

    iput-object v2, p0, Lorg/apache/poi/hssf/record/CFRuleRecord;->_fontFormatting:Lorg/apache/poi/hssf/record/cf/FontFormatting;

    .line 173
    :cond_0
    invoke-virtual {p0}, Lorg/apache/poi/hssf/record/CFRuleRecord;->containsBorderFormattingBlock()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 174
    new-instance v2, Lorg/apache/poi/hssf/record/cf/BorderFormatting;

    invoke-direct {v2, p1}, Lorg/apache/poi/hssf/record/cf/BorderFormatting;-><init>(Lorg/apache/poi/util/LittleEndianInput;)V

    iput-object v2, p0, Lorg/apache/poi/hssf/record/CFRuleRecord;->_borderFormatting:Lorg/apache/poi/hssf/record/cf/BorderFormatting;

    .line 177
    :cond_1
    invoke-virtual {p0}, Lorg/apache/poi/hssf/record/CFRuleRecord;->containsPatternFormattingBlock()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 178
    new-instance v2, Lorg/apache/poi/hssf/record/cf/PatternFormatting;

    invoke-direct {v2, p1}, Lorg/apache/poi/hssf/record/cf/PatternFormatting;-><init>(Lorg/apache/poi/util/LittleEndianInput;)V

    iput-object v2, p0, Lorg/apache/poi/hssf/record/CFRuleRecord;->_patternFormatting:Lorg/apache/poi/hssf/record/cf/PatternFormatting;

    .line 182
    :cond_2
    invoke-static {v0, p1}, Lorg/apache/poi/ss/formula/Formula;->read(ILorg/apache/poi/util/LittleEndianInput;)Lorg/apache/poi/ss/formula/Formula;

    move-result-object v2

    iput-object v2, p0, Lorg/apache/poi/hssf/record/CFRuleRecord;->field_17_formula1:Lorg/apache/poi/ss/formula/Formula;

    .line 183
    invoke-static {v1, p1}, Lorg/apache/poi/ss/formula/Formula;->read(ILorg/apache/poi/util/LittleEndianInput;)Lorg/apache/poi/ss/formula/Formula;

    move-result-object v2

    iput-object v2, p0, Lorg/apache/poi/hssf/record/CFRuleRecord;->field_18_formula2:Lorg/apache/poi/ss/formula/Formula;

    .line 184
    return-void
.end method

.method private static bf(I)Lorg/apache/poi/util/BitField;
    .locals 1
    .param p0, "i"    # I

    .prologue
    .line 103
    invoke-static {p0}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    return-object v0
.end method

.method public static create(Lorg/apache/poi/hssf/usermodel/HSSFSheet;BLjava/lang/String;Ljava/lang/String;)Lorg/apache/poi/hssf/record/CFRuleRecord;
    .locals 4
    .param p0, "sheet"    # Lorg/apache/poi/hssf/usermodel/HSSFSheet;
    .param p1, "comparisonOperation"    # B
    .param p2, "formulaText1"    # Ljava/lang/String;
    .param p3, "formulaText2"    # Ljava/lang/String;

    .prologue
    .line 156
    invoke-static {p2, p0}, Lorg/apache/poi/hssf/record/CFRuleRecord;->parseFormula(Ljava/lang/String;Lorg/apache/poi/hssf/usermodel/HSSFSheet;)[Lorg/apache/poi/ss/formula/ptg/Ptg;

    move-result-object v0

    .line 157
    .local v0, "formula1":[Lorg/apache/poi/ss/formula/ptg/Ptg;
    invoke-static {p3, p0}, Lorg/apache/poi/hssf/record/CFRuleRecord;->parseFormula(Ljava/lang/String;Lorg/apache/poi/hssf/usermodel/HSSFSheet;)[Lorg/apache/poi/ss/formula/ptg/Ptg;

    move-result-object v1

    .line 158
    .local v1, "formula2":[Lorg/apache/poi/ss/formula/ptg/Ptg;
    new-instance v2, Lorg/apache/poi/hssf/record/CFRuleRecord;

    const/4 v3, 0x1

    invoke-direct {v2, v3, p1, v0, v1}, Lorg/apache/poi/hssf/record/CFRuleRecord;-><init>(BB[Lorg/apache/poi/ss/formula/ptg/Ptg;[Lorg/apache/poi/ss/formula/ptg/Ptg;)V

    return-object v2
.end method

.method public static create(Lorg/apache/poi/hssf/usermodel/HSSFSheet;Ljava/lang/String;)Lorg/apache/poi/hssf/record/CFRuleRecord;
    .locals 5
    .param p0, "sheet"    # Lorg/apache/poi/hssf/usermodel/HSSFSheet;
    .param p1, "formulaText"    # Ljava/lang/String;

    .prologue
    .line 147
    invoke-static {p1, p0}, Lorg/apache/poi/hssf/record/CFRuleRecord;->parseFormula(Ljava/lang/String;Lorg/apache/poi/hssf/usermodel/HSSFSheet;)[Lorg/apache/poi/ss/formula/ptg/Ptg;

    move-result-object v0

    .line 148
    .local v0, "formula1":[Lorg/apache/poi/ss/formula/ptg/Ptg;
    new-instance v1, Lorg/apache/poi/hssf/record/CFRuleRecord;

    const/4 v2, 0x2

    const/4 v3, 0x0

    .line 149
    const/4 v4, 0x0

    .line 148
    invoke-direct {v1, v2, v3, v0, v4}, Lorg/apache/poi/hssf/record/CFRuleRecord;-><init>(BB[Lorg/apache/poi/ss/formula/ptg/Ptg;[Lorg/apache/poi/ss/formula/ptg/Ptg;)V

    return-object v1
.end method

.method private static getFormulaSize(Lorg/apache/poi/ss/formula/Formula;)I
    .locals 1
    .param p0, "formula"    # Lorg/apache/poi/ss/formula/Formula;

    .prologue
    .line 433
    invoke-virtual {p0}, Lorg/apache/poi/ss/formula/Formula;->getEncodedTokenSize()I

    move-result v0

    return v0
.end method

.method private getOptionFlag(Lorg/apache/poi/util/BitField;)Z
    .locals 1
    .param p1, "field"    # Lorg/apache/poi/util/BitField;

    .prologue
    .line 386
    iget v0, p0, Lorg/apache/poi/hssf/record/CFRuleRecord;->field_5_options:I

    invoke-virtual {p1, v0}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method private isModified(Lorg/apache/poi/util/BitField;)Z
    .locals 1
    .param p1, "field"    # Lorg/apache/poi/util/BitField;

    .prologue
    .line 286
    iget v0, p0, Lorg/apache/poi/hssf/record/CFRuleRecord;->field_5_options:I

    invoke-virtual {p1, v0}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private static parseFormula(Ljava/lang/String;Lorg/apache/poi/hssf/usermodel/HSSFSheet;)[Lorg/apache/poi/ss/formula/ptg/Ptg;
    .locals 3
    .param p0, "formula"    # Ljava/lang/String;
    .param p1, "sheet"    # Lorg/apache/poi/hssf/usermodel/HSSFSheet;

    .prologue
    .line 531
    if-nez p0, :cond_0

    .line 532
    const/4 v1, 0x0

    .line 535
    :goto_0
    return-object v1

    .line 534
    :cond_0
    invoke-virtual {p1}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->getWorkbook()Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;

    move-result-object v1

    invoke-virtual {v1, p1}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->getSheetIndex(Lorg/apache/poi/ss/usermodel/Sheet;)I

    move-result v0

    .line 535
    .local v0, "sheetIndex":I
    invoke-virtual {p1}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->getWorkbook()Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {p0, v1, v2, v0}, Lorg/apache/poi/hssf/model/HSSFFormulaParser;->parse(Ljava/lang/String;Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;II)[Lorg/apache/poi/ss/formula/ptg/Ptg;

    move-result-object v1

    goto :goto_0
.end method

.method private setModified(ZLorg/apache/poi/util/BitField;)V
    .locals 2
    .param p1, "modified"    # Z
    .param p2, "field"    # Lorg/apache/poi/util/BitField;

    .prologue
    .line 291
    iget v1, p0, Lorg/apache/poi/hssf/record/CFRuleRecord;->field_5_options:I

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p2, v1, v0}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hssf/record/CFRuleRecord;->field_5_options:I

    .line 292
    return-void

    .line 291
    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private setOptionFlag(ZLorg/apache/poi/util/BitField;)V
    .locals 1
    .param p1, "flag"    # Z
    .param p2, "field"    # Lorg/apache/poi/util/BitField;

    .prologue
    .line 391
    iget v0, p0, Lorg/apache/poi/hssf/record/CFRuleRecord;->field_5_options:I

    invoke-virtual {p2, v0, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hssf/record/CFRuleRecord;->field_5_options:I

    .line 392
    return-void
.end method


# virtual methods
.method public clone()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 504
    new-instance v0, Lorg/apache/poi/hssf/record/CFRuleRecord;

    iget-byte v1, p0, Lorg/apache/poi/hssf/record/CFRuleRecord;->field_1_condition_type:B

    iget-byte v2, p0, Lorg/apache/poi/hssf/record/CFRuleRecord;->field_2_comparison_operator:B

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/hssf/record/CFRuleRecord;-><init>(BB)V

    .line 505
    .local v0, "rec":Lorg/apache/poi/hssf/record/CFRuleRecord;
    iget v1, p0, Lorg/apache/poi/hssf/record/CFRuleRecord;->field_5_options:I

    iput v1, v0, Lorg/apache/poi/hssf/record/CFRuleRecord;->field_5_options:I

    .line 506
    iget-short v1, p0, Lorg/apache/poi/hssf/record/CFRuleRecord;->field_6_not_used:S

    iput-short v1, v0, Lorg/apache/poi/hssf/record/CFRuleRecord;->field_6_not_used:S

    .line 507
    invoke-virtual {p0}, Lorg/apache/poi/hssf/record/CFRuleRecord;->containsFontFormattingBlock()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 508
    iget-object v1, p0, Lorg/apache/poi/hssf/record/CFRuleRecord;->_fontFormatting:Lorg/apache/poi/hssf/record/cf/FontFormatting;

    invoke-virtual {v1}, Lorg/apache/poi/hssf/record/cf/FontFormatting;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/hssf/record/cf/FontFormatting;

    iput-object v1, v0, Lorg/apache/poi/hssf/record/CFRuleRecord;->_fontFormatting:Lorg/apache/poi/hssf/record/cf/FontFormatting;

    .line 510
    :cond_0
    invoke-virtual {p0}, Lorg/apache/poi/hssf/record/CFRuleRecord;->containsBorderFormattingBlock()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 511
    iget-object v1, p0, Lorg/apache/poi/hssf/record/CFRuleRecord;->_borderFormatting:Lorg/apache/poi/hssf/record/cf/BorderFormatting;

    invoke-virtual {v1}, Lorg/apache/poi/hssf/record/cf/BorderFormatting;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/hssf/record/cf/BorderFormatting;

    iput-object v1, v0, Lorg/apache/poi/hssf/record/CFRuleRecord;->_borderFormatting:Lorg/apache/poi/hssf/record/cf/BorderFormatting;

    .line 513
    :cond_1
    invoke-virtual {p0}, Lorg/apache/poi/hssf/record/CFRuleRecord;->containsPatternFormattingBlock()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 514
    iget-object v1, p0, Lorg/apache/poi/hssf/record/CFRuleRecord;->_patternFormatting:Lorg/apache/poi/hssf/record/cf/PatternFormatting;

    invoke-virtual {v1}, Lorg/apache/poi/hssf/record/cf/PatternFormatting;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/hssf/record/cf/PatternFormatting;

    iput-object v1, v0, Lorg/apache/poi/hssf/record/CFRuleRecord;->_patternFormatting:Lorg/apache/poi/hssf/record/cf/PatternFormatting;

    .line 516
    :cond_2
    iget-object v1, p0, Lorg/apache/poi/hssf/record/CFRuleRecord;->field_17_formula1:Lorg/apache/poi/ss/formula/Formula;

    invoke-virtual {v1}, Lorg/apache/poi/ss/formula/Formula;->copy()Lorg/apache/poi/ss/formula/Formula;

    move-result-object v1

    iput-object v1, v0, Lorg/apache/poi/hssf/record/CFRuleRecord;->field_17_formula1:Lorg/apache/poi/ss/formula/Formula;

    .line 517
    iget-object v1, p0, Lorg/apache/poi/hssf/record/CFRuleRecord;->field_17_formula1:Lorg/apache/poi/ss/formula/Formula;

    invoke-virtual {v1}, Lorg/apache/poi/ss/formula/Formula;->copy()Lorg/apache/poi/ss/formula/Formula;

    move-result-object v1

    iput-object v1, v0, Lorg/apache/poi/hssf/record/CFRuleRecord;->field_18_formula2:Lorg/apache/poi/ss/formula/Formula;

    .line 519
    return-object v0
.end method

.method public containsAlignFormattingBlock()Z
    .locals 1

    .prologue
    .line 211
    sget-object v0, Lorg/apache/poi/hssf/record/CFRuleRecord;->align:Lorg/apache/poi/util/BitField;

    invoke-direct {p0, v0}, Lorg/apache/poi/hssf/record/CFRuleRecord;->getOptionFlag(Lorg/apache/poi/util/BitField;)Z

    move-result v0

    return v0
.end method

.method public containsBorderFormattingBlock()Z
    .locals 1

    .prologue
    .line 220
    sget-object v0, Lorg/apache/poi/hssf/record/CFRuleRecord;->bord:Lorg/apache/poi/util/BitField;

    invoke-direct {p0, v0}, Lorg/apache/poi/hssf/record/CFRuleRecord;->getOptionFlag(Lorg/apache/poi/util/BitField;)Z

    move-result v0

    return v0
.end method

.method public containsFontFormattingBlock()Z
    .locals 1

    .prologue
    .line 193
    sget-object v0, Lorg/apache/poi/hssf/record/CFRuleRecord;->font:Lorg/apache/poi/util/BitField;

    invoke-direct {p0, v0}, Lorg/apache/poi/hssf/record/CFRuleRecord;->getOptionFlag(Lorg/apache/poi/util/BitField;)Z

    move-result v0

    return v0
.end method

.method public containsPatternFormattingBlock()Z
    .locals 1

    .prologue
    .line 238
    sget-object v0, Lorg/apache/poi/hssf/record/CFRuleRecord;->patt:Lorg/apache/poi/util/BitField;

    invoke-direct {p0, v0}, Lorg/apache/poi/hssf/record/CFRuleRecord;->getOptionFlag(Lorg/apache/poi/util/BitField;)Z

    move-result v0

    return v0
.end method

.method public containsProtectionFormattingBlock()Z
    .locals 1

    .prologue
    .line 256
    sget-object v0, Lorg/apache/poi/hssf/record/CFRuleRecord;->prot:Lorg/apache/poi/util/BitField;

    invoke-direct {p0, v0}, Lorg/apache/poi/hssf/record/CFRuleRecord;->getOptionFlag(Lorg/apache/poi/util/BitField;)Z

    move-result v0

    return v0
.end method

.method public getBorderFormatting()Lorg/apache/poi/hssf/record/cf/BorderFormatting;
    .locals 1

    .prologue
    .line 229
    invoke-virtual {p0}, Lorg/apache/poi/hssf/record/CFRuleRecord;->containsBorderFormattingBlock()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 231
    iget-object v0, p0, Lorg/apache/poi/hssf/record/CFRuleRecord;->_borderFormatting:Lorg/apache/poi/hssf/record/cf/BorderFormatting;

    .line 233
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getComparisonOperation()B
    .locals 1

    .prologue
    .line 270
    iget-byte v0, p0, Lorg/apache/poi/hssf/record/CFRuleRecord;->field_2_comparison_operator:B

    return v0
.end method

.method public getConditionType()B
    .locals 1

    .prologue
    .line 188
    iget-byte v0, p0, Lorg/apache/poi/hssf/record/CFRuleRecord;->field_1_condition_type:B

    return v0
.end method

.method protected getDataSize()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 473
    .line 474
    invoke-virtual {p0}, Lorg/apache/poi/hssf/record/CFRuleRecord;->containsFontFormattingBlock()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lorg/apache/poi/hssf/record/CFRuleRecord;->_fontFormatting:Lorg/apache/poi/hssf/record/cf/FontFormatting;

    invoke-virtual {v0}, Lorg/apache/poi/hssf/record/cf/FontFormatting;->getRawRecord()[B

    move-result-object v0

    array-length v0, v0

    .line 473
    :goto_0
    add-int/lit8 v2, v0, 0xc

    .line 475
    invoke-virtual {p0}, Lorg/apache/poi/hssf/record/CFRuleRecord;->containsBorderFormattingBlock()Z

    move-result v0

    if-eqz v0, :cond_2

    const/16 v0, 0x8

    .line 473
    :goto_1
    add-int/2addr v0, v2

    .line 476
    invoke-virtual {p0}, Lorg/apache/poi/hssf/record/CFRuleRecord;->containsPatternFormattingBlock()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x4

    .line 473
    :cond_0
    add-int/2addr v0, v1

    .line 477
    iget-object v1, p0, Lorg/apache/poi/hssf/record/CFRuleRecord;->field_17_formula1:Lorg/apache/poi/ss/formula/Formula;

    invoke-static {v1}, Lorg/apache/poi/hssf/record/CFRuleRecord;->getFormulaSize(Lorg/apache/poi/ss/formula/Formula;)I

    move-result v1

    .line 473
    add-int/2addr v0, v1

    .line 478
    iget-object v1, p0, Lorg/apache/poi/hssf/record/CFRuleRecord;->field_18_formula2:Lorg/apache/poi/ss/formula/Formula;

    invoke-static {v1}, Lorg/apache/poi/hssf/record/CFRuleRecord;->getFormulaSize(Lorg/apache/poi/ss/formula/Formula;)I

    move-result v1

    .line 473
    add-int/2addr v0, v1

    return v0

    :cond_1
    move v0, v1

    .line 474
    goto :goto_0

    :cond_2
    move v0, v1

    .line 475
    goto :goto_1
.end method

.method public getFontFormatting()Lorg/apache/poi/hssf/record/cf/FontFormatting;
    .locals 1

    .prologue
    .line 202
    invoke-virtual {p0}, Lorg/apache/poi/hssf/record/CFRuleRecord;->containsFontFormattingBlock()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 204
    iget-object v0, p0, Lorg/apache/poi/hssf/record/CFRuleRecord;->_fontFormatting:Lorg/apache/poi/hssf/record/cf/FontFormatting;

    .line 206
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getOptions()I
    .locals 1

    .prologue
    .line 281
    iget v0, p0, Lorg/apache/poi/hssf/record/CFRuleRecord;->field_5_options:I

    return v0
.end method

.method public getParsedExpression1()[Lorg/apache/poi/ss/formula/ptg/Ptg;
    .locals 1

    .prologue
    .line 405
    iget-object v0, p0, Lorg/apache/poi/hssf/record/CFRuleRecord;->field_17_formula1:Lorg/apache/poi/ss/formula/Formula;

    invoke-virtual {v0}, Lorg/apache/poi/ss/formula/Formula;->getTokens()[Lorg/apache/poi/ss/formula/ptg/Ptg;

    move-result-object v0

    return-object v0
.end method

.method public getParsedExpression2()[Lorg/apache/poi/ss/formula/ptg/Ptg;
    .locals 1

    .prologue
    .line 417
    iget-object v0, p0, Lorg/apache/poi/hssf/record/CFRuleRecord;->field_18_formula2:Lorg/apache/poi/ss/formula/Formula;

    invoke-static {v0}, Lorg/apache/poi/ss/formula/Formula;->getTokens(Lorg/apache/poi/ss/formula/Formula;)[Lorg/apache/poi/ss/formula/ptg/Ptg;

    move-result-object v0

    return-object v0
.end method

.method public getPatternFormatting()Lorg/apache/poi/hssf/record/cf/PatternFormatting;
    .locals 1

    .prologue
    .line 247
    invoke-virtual {p0}, Lorg/apache/poi/hssf/record/CFRuleRecord;->containsPatternFormattingBlock()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 249
    iget-object v0, p0, Lorg/apache/poi/hssf/record/CFRuleRecord;->_patternFormatting:Lorg/apache/poi/hssf/record/cf/PatternFormatting;

    .line 251
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getSid()S
    .locals 1

    .prologue
    .line 425
    const/16 v0, 0x1b1

    return v0
.end method

.method public isBottomBorderModified()Z
    .locals 1

    .prologue
    .line 326
    sget-object v0, Lorg/apache/poi/hssf/record/CFRuleRecord;->bordBot:Lorg/apache/poi/util/BitField;

    invoke-direct {p0, v0}, Lorg/apache/poi/hssf/record/CFRuleRecord;->isModified(Lorg/apache/poi/util/BitField;)Z

    move-result v0

    return v0
.end method

.method public isBottomLeftTopRightBorderModified()Z
    .locals 1

    .prologue
    .line 346
    sget-object v0, Lorg/apache/poi/hssf/record/CFRuleRecord;->bordBlTr:Lorg/apache/poi/util/BitField;

    invoke-direct {p0, v0}, Lorg/apache/poi/hssf/record/CFRuleRecord;->isModified(Lorg/apache/poi/util/BitField;)Z

    move-result v0

    return v0
.end method

.method public isLeftBorderModified()Z
    .locals 1

    .prologue
    .line 296
    sget-object v0, Lorg/apache/poi/hssf/record/CFRuleRecord;->bordLeft:Lorg/apache/poi/util/BitField;

    invoke-direct {p0, v0}, Lorg/apache/poi/hssf/record/CFRuleRecord;->isModified(Lorg/apache/poi/util/BitField;)Z

    move-result v0

    return v0
.end method

.method public isPatternBackgroundColorModified()Z
    .locals 1

    .prologue
    .line 376
    sget-object v0, Lorg/apache/poi/hssf/record/CFRuleRecord;->pattBgCol:Lorg/apache/poi/util/BitField;

    invoke-direct {p0, v0}, Lorg/apache/poi/hssf/record/CFRuleRecord;->isModified(Lorg/apache/poi/util/BitField;)Z

    move-result v0

    return v0
.end method

.method public isPatternColorModified()Z
    .locals 1

    .prologue
    .line 366
    sget-object v0, Lorg/apache/poi/hssf/record/CFRuleRecord;->pattCol:Lorg/apache/poi/util/BitField;

    invoke-direct {p0, v0}, Lorg/apache/poi/hssf/record/CFRuleRecord;->isModified(Lorg/apache/poi/util/BitField;)Z

    move-result v0

    return v0
.end method

.method public isPatternStyleModified()Z
    .locals 1

    .prologue
    .line 356
    sget-object v0, Lorg/apache/poi/hssf/record/CFRuleRecord;->pattStyle:Lorg/apache/poi/util/BitField;

    invoke-direct {p0, v0}, Lorg/apache/poi/hssf/record/CFRuleRecord;->isModified(Lorg/apache/poi/util/BitField;)Z

    move-result v0

    return v0
.end method

.method public isRightBorderModified()Z
    .locals 1

    .prologue
    .line 306
    sget-object v0, Lorg/apache/poi/hssf/record/CFRuleRecord;->bordRight:Lorg/apache/poi/util/BitField;

    invoke-direct {p0, v0}, Lorg/apache/poi/hssf/record/CFRuleRecord;->isModified(Lorg/apache/poi/util/BitField;)Z

    move-result v0

    return v0
.end method

.method public isTopBorderModified()Z
    .locals 1

    .prologue
    .line 316
    sget-object v0, Lorg/apache/poi/hssf/record/CFRuleRecord;->bordTop:Lorg/apache/poi/util/BitField;

    invoke-direct {p0, v0}, Lorg/apache/poi/hssf/record/CFRuleRecord;->isModified(Lorg/apache/poi/util/BitField;)Z

    move-result v0

    return v0
.end method

.method public isTopLeftBottomRightBorderModified()Z
    .locals 1

    .prologue
    .line 336
    sget-object v0, Lorg/apache/poi/hssf/record/CFRuleRecord;->bordTlBr:Lorg/apache/poi/util/BitField;

    invoke-direct {p0, v0}, Lorg/apache/poi/hssf/record/CFRuleRecord;->isModified(Lorg/apache/poi/util/BitField;)Z

    move-result v0

    return v0
.end method

.method public serialize(Lorg/apache/poi/util/LittleEndianOutput;)V
    .locals 4
    .param p1, "out"    # Lorg/apache/poi/util/LittleEndianOutput;

    .prologue
    .line 445
    iget-object v3, p0, Lorg/apache/poi/hssf/record/CFRuleRecord;->field_17_formula1:Lorg/apache/poi/ss/formula/Formula;

    invoke-static {v3}, Lorg/apache/poi/hssf/record/CFRuleRecord;->getFormulaSize(Lorg/apache/poi/ss/formula/Formula;)I

    move-result v1

    .line 446
    .local v1, "formula1Len":I
    iget-object v3, p0, Lorg/apache/poi/hssf/record/CFRuleRecord;->field_18_formula2:Lorg/apache/poi/ss/formula/Formula;

    invoke-static {v3}, Lorg/apache/poi/hssf/record/CFRuleRecord;->getFormulaSize(Lorg/apache/poi/ss/formula/Formula;)I

    move-result v2

    .line 448
    .local v2, "formula2Len":I
    iget-byte v3, p0, Lorg/apache/poi/hssf/record/CFRuleRecord;->field_1_condition_type:B

    invoke-interface {p1, v3}, Lorg/apache/poi/util/LittleEndianOutput;->writeByte(I)V

    .line 449
    iget-byte v3, p0, Lorg/apache/poi/hssf/record/CFRuleRecord;->field_2_comparison_operator:B

    invoke-interface {p1, v3}, Lorg/apache/poi/util/LittleEndianOutput;->writeByte(I)V

    .line 450
    invoke-interface {p1, v1}, Lorg/apache/poi/util/LittleEndianOutput;->writeShort(I)V

    .line 451
    invoke-interface {p1, v2}, Lorg/apache/poi/util/LittleEndianOutput;->writeShort(I)V

    .line 452
    iget v3, p0, Lorg/apache/poi/hssf/record/CFRuleRecord;->field_5_options:I

    invoke-interface {p1, v3}, Lorg/apache/poi/util/LittleEndianOutput;->writeInt(I)V

    .line 453
    iget-short v3, p0, Lorg/apache/poi/hssf/record/CFRuleRecord;->field_6_not_used:S

    invoke-interface {p1, v3}, Lorg/apache/poi/util/LittleEndianOutput;->writeShort(I)V

    .line 455
    invoke-virtual {p0}, Lorg/apache/poi/hssf/record/CFRuleRecord;->containsFontFormattingBlock()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 456
    iget-object v3, p0, Lorg/apache/poi/hssf/record/CFRuleRecord;->_fontFormatting:Lorg/apache/poi/hssf/record/cf/FontFormatting;

    invoke-virtual {v3}, Lorg/apache/poi/hssf/record/cf/FontFormatting;->getRawRecord()[B

    move-result-object v0

    .line 457
    .local v0, "fontFormattingRawRecord":[B
    invoke-interface {p1, v0}, Lorg/apache/poi/util/LittleEndianOutput;->write([B)V

    .line 460
    .end local v0    # "fontFormattingRawRecord":[B
    :cond_0
    invoke-virtual {p0}, Lorg/apache/poi/hssf/record/CFRuleRecord;->containsBorderFormattingBlock()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 461
    iget-object v3, p0, Lorg/apache/poi/hssf/record/CFRuleRecord;->_borderFormatting:Lorg/apache/poi/hssf/record/cf/BorderFormatting;

    invoke-virtual {v3, p1}, Lorg/apache/poi/hssf/record/cf/BorderFormatting;->serialize(Lorg/apache/poi/util/LittleEndianOutput;)V

    .line 464
    :cond_1
    invoke-virtual {p0}, Lorg/apache/poi/hssf/record/CFRuleRecord;->containsPatternFormattingBlock()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 465
    iget-object v3, p0, Lorg/apache/poi/hssf/record/CFRuleRecord;->_patternFormatting:Lorg/apache/poi/hssf/record/cf/PatternFormatting;

    invoke-virtual {v3, p1}, Lorg/apache/poi/hssf/record/cf/PatternFormatting;->serialize(Lorg/apache/poi/util/LittleEndianOutput;)V

    .line 468
    :cond_2
    iget-object v3, p0, Lorg/apache/poi/hssf/record/CFRuleRecord;->field_17_formula1:Lorg/apache/poi/ss/formula/Formula;

    invoke-virtual {v3, p1}, Lorg/apache/poi/ss/formula/Formula;->serializeTokens(Lorg/apache/poi/util/LittleEndianOutput;)V

    .line 469
    iget-object v3, p0, Lorg/apache/poi/hssf/record/CFRuleRecord;->field_18_formula2:Lorg/apache/poi/ss/formula/Formula;

    invoke-virtual {v3, p1}, Lorg/apache/poi/ss/formula/Formula;->serializeTokens(Lorg/apache/poi/util/LittleEndianOutput;)V

    .line 470
    return-void
.end method

.method public setAlignFormattingUnchanged()V
    .locals 2

    .prologue
    .line 215
    const/4 v0, 0x0

    sget-object v1, Lorg/apache/poi/hssf/record/CFRuleRecord;->align:Lorg/apache/poi/util/BitField;

    invoke-direct {p0, v0, v1}, Lorg/apache/poi/hssf/record/CFRuleRecord;->setOptionFlag(ZLorg/apache/poi/util/BitField;)V

    .line 216
    return-void
.end method

.method public setBorderFormatting(Lorg/apache/poi/hssf/record/cf/BorderFormatting;)V
    .locals 2
    .param p1, "borderFormatting"    # Lorg/apache/poi/hssf/record/cf/BorderFormatting;

    .prologue
    .line 224
    iput-object p1, p0, Lorg/apache/poi/hssf/record/CFRuleRecord;->_borderFormatting:Lorg/apache/poi/hssf/record/cf/BorderFormatting;

    .line 225
    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    sget-object v1, Lorg/apache/poi/hssf/record/CFRuleRecord;->bord:Lorg/apache/poi/util/BitField;

    invoke-direct {p0, v0, v1}, Lorg/apache/poi/hssf/record/CFRuleRecord;->setOptionFlag(ZLorg/apache/poi/util/BitField;)V

    .line 226
    return-void

    .line 225
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setBottomBorderModified(Z)V
    .locals 1
    .param p1, "modified"    # Z

    .prologue
    .line 331
    sget-object v0, Lorg/apache/poi/hssf/record/CFRuleRecord;->bordBot:Lorg/apache/poi/util/BitField;

    invoke-direct {p0, p1, v0}, Lorg/apache/poi/hssf/record/CFRuleRecord;->setModified(ZLorg/apache/poi/util/BitField;)V

    .line 332
    return-void
.end method

.method public setBottomLeftTopRightBorderModified(Z)V
    .locals 1
    .param p1, "modified"    # Z

    .prologue
    .line 351
    sget-object v0, Lorg/apache/poi/hssf/record/CFRuleRecord;->bordBlTr:Lorg/apache/poi/util/BitField;

    invoke-direct {p0, p1, v0}, Lorg/apache/poi/hssf/record/CFRuleRecord;->setModified(ZLorg/apache/poi/util/BitField;)V

    .line 352
    return-void
.end method

.method public setComparisonOperation(B)V
    .locals 0
    .param p1, "operation"    # B

    .prologue
    .line 265
    iput-byte p1, p0, Lorg/apache/poi/hssf/record/CFRuleRecord;->field_2_comparison_operator:B

    .line 266
    return-void
.end method

.method public setFontFormatting(Lorg/apache/poi/hssf/record/cf/FontFormatting;)V
    .locals 2
    .param p1, "fontFormatting"    # Lorg/apache/poi/hssf/record/cf/FontFormatting;

    .prologue
    .line 197
    iput-object p1, p0, Lorg/apache/poi/hssf/record/CFRuleRecord;->_fontFormatting:Lorg/apache/poi/hssf/record/cf/FontFormatting;

    .line 198
    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    sget-object v1, Lorg/apache/poi/hssf/record/CFRuleRecord;->font:Lorg/apache/poi/util/BitField;

    invoke-direct {p0, v0, v1}, Lorg/apache/poi/hssf/record/CFRuleRecord;->setOptionFlag(ZLorg/apache/poi/util/BitField;)V

    .line 199
    return-void

    .line 198
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setLeftBorderModified(Z)V
    .locals 1
    .param p1, "modified"    # Z

    .prologue
    .line 301
    sget-object v0, Lorg/apache/poi/hssf/record/CFRuleRecord;->bordLeft:Lorg/apache/poi/util/BitField;

    invoke-direct {p0, p1, v0}, Lorg/apache/poi/hssf/record/CFRuleRecord;->setModified(ZLorg/apache/poi/util/BitField;)V

    .line 302
    return-void
.end method

.method public setParsedExpression1([Lorg/apache/poi/ss/formula/ptg/Ptg;)V
    .locals 1
    .param p1, "ptgs"    # [Lorg/apache/poi/ss/formula/ptg/Ptg;

    .prologue
    .line 408
    invoke-static {p1}, Lorg/apache/poi/ss/formula/Formula;->create([Lorg/apache/poi/ss/formula/ptg/Ptg;)Lorg/apache/poi/ss/formula/Formula;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/poi/hssf/record/CFRuleRecord;->field_17_formula1:Lorg/apache/poi/ss/formula/Formula;

    .line 409
    return-void
.end method

.method public setParsedExpression2([Lorg/apache/poi/ss/formula/ptg/Ptg;)V
    .locals 1
    .param p1, "ptgs"    # [Lorg/apache/poi/ss/formula/ptg/Ptg;

    .prologue
    .line 420
    invoke-static {p1}, Lorg/apache/poi/ss/formula/Formula;->create([Lorg/apache/poi/ss/formula/ptg/Ptg;)Lorg/apache/poi/ss/formula/Formula;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/poi/hssf/record/CFRuleRecord;->field_18_formula2:Lorg/apache/poi/ss/formula/Formula;

    .line 421
    return-void
.end method

.method public setPatternBackgroundColorModified(Z)V
    .locals 1
    .param p1, "modified"    # Z

    .prologue
    .line 381
    sget-object v0, Lorg/apache/poi/hssf/record/CFRuleRecord;->pattBgCol:Lorg/apache/poi/util/BitField;

    invoke-direct {p0, p1, v0}, Lorg/apache/poi/hssf/record/CFRuleRecord;->setModified(ZLorg/apache/poi/util/BitField;)V

    .line 382
    return-void
.end method

.method public setPatternColorModified(Z)V
    .locals 1
    .param p1, "modified"    # Z

    .prologue
    .line 371
    sget-object v0, Lorg/apache/poi/hssf/record/CFRuleRecord;->pattCol:Lorg/apache/poi/util/BitField;

    invoke-direct {p0, p1, v0}, Lorg/apache/poi/hssf/record/CFRuleRecord;->setModified(ZLorg/apache/poi/util/BitField;)V

    .line 372
    return-void
.end method

.method public setPatternFormatting(Lorg/apache/poi/hssf/record/cf/PatternFormatting;)V
    .locals 2
    .param p1, "patternFormatting"    # Lorg/apache/poi/hssf/record/cf/PatternFormatting;

    .prologue
    .line 242
    iput-object p1, p0, Lorg/apache/poi/hssf/record/CFRuleRecord;->_patternFormatting:Lorg/apache/poi/hssf/record/cf/PatternFormatting;

    .line 243
    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    sget-object v1, Lorg/apache/poi/hssf/record/CFRuleRecord;->patt:Lorg/apache/poi/util/BitField;

    invoke-direct {p0, v0, v1}, Lorg/apache/poi/hssf/record/CFRuleRecord;->setOptionFlag(ZLorg/apache/poi/util/BitField;)V

    .line 244
    return-void

    .line 243
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setPatternStyleModified(Z)V
    .locals 1
    .param p1, "modified"    # Z

    .prologue
    .line 361
    sget-object v0, Lorg/apache/poi/hssf/record/CFRuleRecord;->pattStyle:Lorg/apache/poi/util/BitField;

    invoke-direct {p0, p1, v0}, Lorg/apache/poi/hssf/record/CFRuleRecord;->setModified(ZLorg/apache/poi/util/BitField;)V

    .line 362
    return-void
.end method

.method public setProtectionFormattingUnchanged()V
    .locals 2

    .prologue
    .line 260
    const/4 v0, 0x0

    sget-object v1, Lorg/apache/poi/hssf/record/CFRuleRecord;->prot:Lorg/apache/poi/util/BitField;

    invoke-direct {p0, v0, v1}, Lorg/apache/poi/hssf/record/CFRuleRecord;->setOptionFlag(ZLorg/apache/poi/util/BitField;)V

    .line 261
    return-void
.end method

.method public setRightBorderModified(Z)V
    .locals 1
    .param p1, "modified"    # Z

    .prologue
    .line 311
    sget-object v0, Lorg/apache/poi/hssf/record/CFRuleRecord;->bordRight:Lorg/apache/poi/util/BitField;

    invoke-direct {p0, p1, v0}, Lorg/apache/poi/hssf/record/CFRuleRecord;->setModified(ZLorg/apache/poi/util/BitField;)V

    .line 312
    return-void
.end method

.method public setTopBorderModified(Z)V
    .locals 1
    .param p1, "modified"    # Z

    .prologue
    .line 321
    sget-object v0, Lorg/apache/poi/hssf/record/CFRuleRecord;->bordTop:Lorg/apache/poi/util/BitField;

    invoke-direct {p0, p1, v0}, Lorg/apache/poi/hssf/record/CFRuleRecord;->setModified(ZLorg/apache/poi/util/BitField;)V

    .line 322
    return-void
.end method

.method public setTopLeftBottomRightBorderModified(Z)V
    .locals 1
    .param p1, "modified"    # Z

    .prologue
    .line 341
    sget-object v0, Lorg/apache/poi/hssf/record/CFRuleRecord;->bordTlBr:Lorg/apache/poi/util/BitField;

    invoke-direct {p0, p1, v0}, Lorg/apache/poi/hssf/record/CFRuleRecord;->setModified(ZLorg/apache/poi/util/BitField;)V

    .line 342
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 484
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 485
    .local v0, "buffer":Ljava/lang/StringBuffer;
    const-string/jumbo v1, "[CFRULE]\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 486
    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "    .condition_type   ="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-byte v2, p0, Lorg/apache/poi/hssf/record/CFRuleRecord;->field_1_condition_type:B

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 487
    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "    OPTION FLAGS=0x"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lorg/apache/poi/hssf/record/CFRuleRecord;->getOptions()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 500
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
