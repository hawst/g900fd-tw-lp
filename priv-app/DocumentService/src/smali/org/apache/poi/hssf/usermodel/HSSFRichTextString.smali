.class public final Lorg/apache/poi/hssf/usermodel/HSSFRichTextString;
.super Ljava/lang/Object;
.source "HSSFRichTextString.java"

# interfaces
.implements Ljava/lang/Comparable;
.implements Lorg/apache/poi/ss/usermodel/RichTextString;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lorg/apache/poi/hssf/usermodel/HSSFRichTextString;",
        ">;",
        "Lorg/apache/poi/ss/usermodel/RichTextString;"
    }
.end annotation


# static fields
.field public static final NO_FONT:S


# instance fields
.field private _book:Lorg/apache/poi/hssf/model/InternalWorkbook;

.field private _record:Lorg/apache/poi/hssf/record/LabelSSTRecord;

.field private _string:Lorg/apache/poi/hssf/record/common/UnicodeString;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 82
    const-string/jumbo v0, ""

    invoke-direct {p0, v0}, Lorg/apache/poi/hssf/usermodel/HSSFRichTextString;-><init>(Ljava/lang/String;)V

    .line 83
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 2
    .param p1, "string"    # Ljava/lang/String;

    .prologue
    .line 85
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 86
    if-nez p1, :cond_0

    .line 87
    new-instance v0, Lorg/apache/poi/hssf/record/common/UnicodeString;

    const-string/jumbo v1, ""

    invoke-direct {v0, v1}, Lorg/apache/poi/hssf/record/common/UnicodeString;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFRichTextString;->_string:Lorg/apache/poi/hssf/record/common/UnicodeString;

    .line 91
    :goto_0
    return-void

    .line 89
    :cond_0
    new-instance v0, Lorg/apache/poi/hssf/record/common/UnicodeString;

    invoke-direct {v0, p1}, Lorg/apache/poi/hssf/record/common/UnicodeString;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFRichTextString;->_string:Lorg/apache/poi/hssf/record/common/UnicodeString;

    goto :goto_0
.end method

.method constructor <init>(Lorg/apache/poi/hssf/model/InternalWorkbook;Lorg/apache/poi/hssf/record/LabelSSTRecord;)V
    .locals 1
    .param p1, "book"    # Lorg/apache/poi/hssf/model/InternalWorkbook;
    .param p2, "record"    # Lorg/apache/poi/hssf/record/LabelSSTRecord;

    .prologue
    .line 93
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 94
    invoke-virtual {p0, p1, p2}, Lorg/apache/poi/hssf/usermodel/HSSFRichTextString;->setWorkbookReferences(Lorg/apache/poi/hssf/model/InternalWorkbook;Lorg/apache/poi/hssf/record/LabelSSTRecord;)V

    .line 96
    invoke-virtual {p2}, Lorg/apache/poi/hssf/record/LabelSSTRecord;->getSSTIndex()I

    move-result v0

    invoke-virtual {p1, v0}, Lorg/apache/poi/hssf/model/InternalWorkbook;->getSSTString(I)Lorg/apache/poi/hssf/record/common/UnicodeString;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFRichTextString;->_string:Lorg/apache/poi/hssf/record/common/UnicodeString;

    .line 97
    return-void
.end method

.method private addToSSTIfRequired()V
    .locals 3

    .prologue
    .line 119
    iget-object v1, p0, Lorg/apache/poi/hssf/usermodel/HSSFRichTextString;->_book:Lorg/apache/poi/hssf/model/InternalWorkbook;

    if-eqz v1, :cond_0

    .line 120
    iget-object v1, p0, Lorg/apache/poi/hssf/usermodel/HSSFRichTextString;->_book:Lorg/apache/poi/hssf/model/InternalWorkbook;

    iget-object v2, p0, Lorg/apache/poi/hssf/usermodel/HSSFRichTextString;->_string:Lorg/apache/poi/hssf/record/common/UnicodeString;

    invoke-virtual {v1, v2}, Lorg/apache/poi/hssf/model/InternalWorkbook;->addSSTString(Lorg/apache/poi/hssf/record/common/UnicodeString;)I

    move-result v0

    .line 121
    .local v0, "index":I
    iget-object v1, p0, Lorg/apache/poi/hssf/usermodel/HSSFRichTextString;->_record:Lorg/apache/poi/hssf/record/LabelSSTRecord;

    invoke-virtual {v1, v0}, Lorg/apache/poi/hssf/record/LabelSSTRecord;->setSSTIndex(I)V

    .line 124
    iget-object v1, p0, Lorg/apache/poi/hssf/usermodel/HSSFRichTextString;->_book:Lorg/apache/poi/hssf/model/InternalWorkbook;

    invoke-virtual {v1, v0}, Lorg/apache/poi/hssf/model/InternalWorkbook;->getSSTString(I)Lorg/apache/poi/hssf/record/common/UnicodeString;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/poi/hssf/usermodel/HSSFRichTextString;->_string:Lorg/apache/poi/hssf/record/common/UnicodeString;

    .line 126
    .end local v0    # "index":I
    :cond_0
    return-void
.end method

.method private cloneStringIfRequired()Lorg/apache/poi/hssf/record/common/UnicodeString;
    .locals 2

    .prologue
    .line 112
    iget-object v1, p0, Lorg/apache/poi/hssf/usermodel/HSSFRichTextString;->_book:Lorg/apache/poi/hssf/model/InternalWorkbook;

    if-nez v1, :cond_0

    .line 113
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFRichTextString;->_string:Lorg/apache/poi/hssf/record/common/UnicodeString;

    .line 115
    :goto_0
    return-object v0

    .line 114
    :cond_0
    iget-object v1, p0, Lorg/apache/poi/hssf/usermodel/HSSFRichTextString;->_string:Lorg/apache/poi/hssf/record/common/UnicodeString;

    invoke-virtual {v1}, Lorg/apache/poi/hssf/record/common/UnicodeString;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hssf/record/common/UnicodeString;

    .line 115
    .local v0, "s":Lorg/apache/poi/hssf/record/common/UnicodeString;
    goto :goto_0
.end method


# virtual methods
.method public applyFont(IILorg/apache/poi/ss/usermodel/Font;)V
    .locals 1
    .param p1, "startIndex"    # I
    .param p2, "endIndex"    # I
    .param p3, "font"    # Lorg/apache/poi/ss/usermodel/Font;

    .prologue
    .line 180
    check-cast p3, Lorg/apache/poi/hssf/usermodel/HSSFFont;

    .end local p3    # "font":Lorg/apache/poi/ss/usermodel/Font;
    invoke-virtual {p3}, Lorg/apache/poi/hssf/usermodel/HSSFFont;->getIndex()S

    move-result v0

    invoke-virtual {p0, p1, p2, v0}, Lorg/apache/poi/hssf/usermodel/HSSFRichTextString;->applyFont(IIS)V

    .line 181
    return-void
.end method

.method public applyFont(IIS)V
    .locals 6
    .param p1, "startIndex"    # I
    .param p2, "endIndex"    # I
    .param p3, "fontIndex"    # S

    .prologue
    .line 138
    if-le p1, p2, :cond_0

    .line 139
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v4, "Start index must be less than end index."

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 140
    :cond_0
    if-ltz p1, :cond_1

    invoke-virtual {p0}, Lorg/apache/poi/hssf/usermodel/HSSFRichTextString;->length()I

    move-result v3

    if-le p2, v3, :cond_2

    .line 141
    :cond_1
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v4, "Start and end index not in range."

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 142
    :cond_2
    if-ne p1, p2, :cond_3

    .line 169
    :goto_0
    return-void

    .line 147
    :cond_3
    const/4 v0, 0x0

    .line 148
    .local v0, "currentFont":S
    invoke-virtual {p0}, Lorg/apache/poi/hssf/usermodel/HSSFRichTextString;->length()I

    move-result v3

    if-eq p2, v3, :cond_4

    .line 149
    invoke-virtual {p0, p2}, Lorg/apache/poi/hssf/usermodel/HSSFRichTextString;->getFontAtIndex(I)S

    move-result v0

    .line 153
    :cond_4
    invoke-direct {p0}, Lorg/apache/poi/hssf/usermodel/HSSFRichTextString;->cloneStringIfRequired()Lorg/apache/poi/hssf/record/common/UnicodeString;

    move-result-object v3

    iput-object v3, p0, Lorg/apache/poi/hssf/usermodel/HSSFRichTextString;->_string:Lorg/apache/poi/hssf/record/common/UnicodeString;

    .line 154
    iget-object v3, p0, Lorg/apache/poi/hssf/usermodel/HSSFRichTextString;->_string:Lorg/apache/poi/hssf/record/common/UnicodeString;

    invoke-virtual {v3}, Lorg/apache/poi/hssf/record/common/UnicodeString;->formatIterator()Ljava/util/Iterator;

    move-result-object v1

    .line 155
    .local v1, "formatting":Ljava/util/Iterator;
    if-eqz v1, :cond_6

    .line 156
    :cond_5
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_8

    .line 164
    :cond_6
    iget-object v3, p0, Lorg/apache/poi/hssf/usermodel/HSSFRichTextString;->_string:Lorg/apache/poi/hssf/record/common/UnicodeString;

    new-instance v4, Lorg/apache/poi/hssf/record/common/UnicodeString$FormatRun;

    int-to-short v5, p1

    invoke-direct {v4, v5, p3}, Lorg/apache/poi/hssf/record/common/UnicodeString$FormatRun;-><init>(SS)V

    invoke-virtual {v3, v4}, Lorg/apache/poi/hssf/record/common/UnicodeString;->addFormatRun(Lorg/apache/poi/hssf/record/common/UnicodeString$FormatRun;)V

    .line 165
    invoke-virtual {p0}, Lorg/apache/poi/hssf/usermodel/HSSFRichTextString;->length()I

    move-result v3

    if-eq p2, v3, :cond_7

    .line 166
    iget-object v3, p0, Lorg/apache/poi/hssf/usermodel/HSSFRichTextString;->_string:Lorg/apache/poi/hssf/record/common/UnicodeString;

    new-instance v4, Lorg/apache/poi/hssf/record/common/UnicodeString$FormatRun;

    int-to-short v5, p2

    invoke-direct {v4, v5, v0}, Lorg/apache/poi/hssf/record/common/UnicodeString$FormatRun;-><init>(SS)V

    invoke-virtual {v3, v4}, Lorg/apache/poi/hssf/record/common/UnicodeString;->addFormatRun(Lorg/apache/poi/hssf/record/common/UnicodeString$FormatRun;)V

    .line 168
    :cond_7
    invoke-direct {p0}, Lorg/apache/poi/hssf/usermodel/HSSFRichTextString;->addToSSTIfRequired()V

    goto :goto_0

    .line 157
    :cond_8
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/poi/hssf/record/common/UnicodeString$FormatRun;

    .line 158
    .local v2, "r":Lorg/apache/poi/hssf/record/common/UnicodeString$FormatRun;
    invoke-virtual {v2}, Lorg/apache/poi/hssf/record/common/UnicodeString$FormatRun;->getCharacterPos()S

    move-result v3

    if-lt v3, p1, :cond_5

    invoke-virtual {v2}, Lorg/apache/poi/hssf/record/common/UnicodeString$FormatRun;->getCharacterPos()S

    move-result v3

    if-ge v3, p2, :cond_5

    .line 159
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_1
.end method

.method public applyFont(Lorg/apache/poi/ss/usermodel/Font;)V
    .locals 2
    .param p1, "font"    # Lorg/apache/poi/ss/usermodel/Font;

    .prologue
    .line 189
    const/4 v0, 0x0

    iget-object v1, p0, Lorg/apache/poi/hssf/usermodel/HSSFRichTextString;->_string:Lorg/apache/poi/hssf/record/common/UnicodeString;

    invoke-virtual {v1}, Lorg/apache/poi/hssf/record/common/UnicodeString;->getCharCount()I

    move-result v1

    invoke-virtual {p0, v0, v1, p1}, Lorg/apache/poi/hssf/usermodel/HSSFRichTextString;->applyFont(IILorg/apache/poi/ss/usermodel/Font;)V

    .line 190
    return-void
.end method

.method public applyFont(S)V
    .locals 2
    .param p1, "fontIndex"    # S

    .prologue
    .line 340
    const/4 v0, 0x0

    iget-object v1, p0, Lorg/apache/poi/hssf/usermodel/HSSFRichTextString;->_string:Lorg/apache/poi/hssf/record/common/UnicodeString;

    invoke-virtual {v1}, Lorg/apache/poi/hssf/record/common/UnicodeString;->getCharCount()I

    move-result v1

    invoke-virtual {p0, v0, v1, p1}, Lorg/apache/poi/hssf/usermodel/HSSFRichTextString;->applyFont(IIS)V

    .line 341
    return-void
.end method

.method public clearFormatting()V
    .locals 1

    .prologue
    .line 196
    invoke-direct {p0}, Lorg/apache/poi/hssf/usermodel/HSSFRichTextString;->cloneStringIfRequired()Lorg/apache/poi/hssf/record/common/UnicodeString;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFRichTextString;->_string:Lorg/apache/poi/hssf/record/common/UnicodeString;

    .line 197
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFRichTextString;->_string:Lorg/apache/poi/hssf/record/common/UnicodeString;

    invoke-virtual {v0}, Lorg/apache/poi/hssf/record/common/UnicodeString;->clearFormatting()V

    .line 198
    invoke-direct {p0}, Lorg/apache/poi/hssf/usermodel/HSSFRichTextString;->addToSSTIfRequired()V

    .line 199
    return-void
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 1
    check-cast p1, Lorg/apache/poi/hssf/usermodel/HSSFRichTextString;

    invoke-virtual {p0, p1}, Lorg/apache/poi/hssf/usermodel/HSSFRichTextString;->compareTo(Lorg/apache/poi/hssf/usermodel/HSSFRichTextString;)I

    move-result v0

    return v0
.end method

.method public compareTo(Lorg/apache/poi/hssf/usermodel/HSSFRichTextString;)I
    .locals 2
    .param p1, "r"    # Lorg/apache/poi/hssf/usermodel/HSSFRichTextString;

    .prologue
    .line 314
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFRichTextString;->_string:Lorg/apache/poi/hssf/record/common/UnicodeString;

    iget-object v1, p1, Lorg/apache/poi/hssf/usermodel/HSSFRichTextString;->_string:Lorg/apache/poi/hssf/record/common/UnicodeString;

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/record/common/UnicodeString;->compareTo(Lorg/apache/poi/hssf/record/common/UnicodeString;)I

    move-result v0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 318
    instance-of v0, p1, Lorg/apache/poi/hssf/usermodel/HSSFRichTextString;

    if-eqz v0, :cond_0

    .line 319
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFRichTextString;->_string:Lorg/apache/poi/hssf/record/common/UnicodeString;

    check-cast p1, Lorg/apache/poi/hssf/usermodel/HSSFRichTextString;

    .end local p1    # "o":Ljava/lang/Object;
    iget-object v1, p1, Lorg/apache/poi/hssf/usermodel/HSSFRichTextString;->_string:Lorg/apache/poi/hssf/record/common/UnicodeString;

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/record/common/UnicodeString;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 321
    :goto_0
    return v0

    .restart local p1    # "o":Ljava/lang/Object;
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getFontAtIndex(I)S
    .locals 5
    .param p1, "index"    # I

    .prologue
    .line 252
    iget-object v4, p0, Lorg/apache/poi/hssf/usermodel/HSSFRichTextString;->_string:Lorg/apache/poi/hssf/record/common/UnicodeString;

    invoke-virtual {v4}, Lorg/apache/poi/hssf/record/common/UnicodeString;->getFormatRunCount()I

    move-result v3

    .line 253
    .local v3, "size":I
    const/4 v0, 0x0

    .line 254
    .local v0, "currentRun":Lorg/apache/poi/hssf/record/common/UnicodeString$FormatRun;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-lt v1, v3, :cond_1

    .line 262
    :cond_0
    if-nez v0, :cond_3

    .line 263
    const/4 v4, 0x0

    .line 265
    :goto_1
    return v4

    .line 255
    :cond_1
    iget-object v4, p0, Lorg/apache/poi/hssf/usermodel/HSSFRichTextString;->_string:Lorg/apache/poi/hssf/record/common/UnicodeString;

    invoke-virtual {v4, v1}, Lorg/apache/poi/hssf/record/common/UnicodeString;->getFormatRun(I)Lorg/apache/poi/hssf/record/common/UnicodeString$FormatRun;

    move-result-object v2

    .line 257
    .local v2, "r":Lorg/apache/poi/hssf/record/common/UnicodeString$FormatRun;
    if-eqz v2, :cond_2

    invoke-virtual {v2}, Lorg/apache/poi/hssf/record/common/UnicodeString$FormatRun;->getCharacterPos()S

    move-result v4

    if-gt v4, p1, :cond_0

    .line 260
    :cond_2
    move-object v0, v2

    .line 254
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 265
    .end local v2    # "r":Lorg/apache/poi/hssf/record/common/UnicodeString$FormatRun;
    :cond_3
    invoke-virtual {v0}, Lorg/apache/poi/hssf/record/common/UnicodeString$FormatRun;->getFontIndex()S

    move-result v4

    goto :goto_1
.end method

.method public getFontOfFormattingRun(I)S
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 302
    iget-object v1, p0, Lorg/apache/poi/hssf/usermodel/HSSFRichTextString;->_string:Lorg/apache/poi/hssf/record/common/UnicodeString;

    invoke-virtual {v1, p1}, Lorg/apache/poi/hssf/record/common/UnicodeString;->getFormatRun(I)Lorg/apache/poi/hssf/record/common/UnicodeString$FormatRun;

    move-result-object v0

    .line 304
    .local v0, "r":Lorg/apache/poi/hssf/record/common/UnicodeString$FormatRun;
    if-eqz v0, :cond_0

    .line 305
    invoke-virtual {v0}, Lorg/apache/poi/hssf/record/common/UnicodeString$FormatRun;->getFontIndex()S

    move-result v1

    .line 307
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getIndexOfFormattingRun(I)I
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 286
    iget-object v1, p0, Lorg/apache/poi/hssf/usermodel/HSSFRichTextString;->_string:Lorg/apache/poi/hssf/record/common/UnicodeString;

    invoke-virtual {v1, p1}, Lorg/apache/poi/hssf/record/common/UnicodeString;->getFormatRun(I)Lorg/apache/poi/hssf/record/common/UnicodeString$FormatRun;

    move-result-object v0

    .line 288
    .local v0, "r":Lorg/apache/poi/hssf/record/common/UnicodeString$FormatRun;
    if-eqz v0, :cond_0

    .line 289
    invoke-virtual {v0}, Lorg/apache/poi/hssf/record/common/UnicodeString$FormatRun;->getCharacterPos()S

    move-result v1

    .line 291
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method getRawUnicodeString()Lorg/apache/poi/hssf/record/common/UnicodeString;
    .locals 1

    .prologue
    .line 226
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFRichTextString;->_string:Lorg/apache/poi/hssf/record/common/UnicodeString;

    return-object v0
.end method

.method public getString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 206
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFRichTextString;->_string:Lorg/apache/poi/hssf/record/common/UnicodeString;

    invoke-virtual {v0}, Lorg/apache/poi/hssf/record/common/UnicodeString;->getString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method getUnicodeString()Lorg/apache/poi/hssf/record/common/UnicodeString;
    .locals 1

    .prologue
    .line 215
    invoke-direct {p0}, Lorg/apache/poi/hssf/usermodel/HSSFRichTextString;->cloneStringIfRequired()Lorg/apache/poi/hssf/record/common/UnicodeString;

    move-result-object v0

    return-object v0
.end method

.method public length()I
    .locals 1

    .prologue
    .line 239
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFRichTextString;->_string:Lorg/apache/poi/hssf/record/common/UnicodeString;

    invoke-virtual {v0}, Lorg/apache/poi/hssf/record/common/UnicodeString;->getCharCount()I

    move-result v0

    return v0
.end method

.method public numFormattingRuns()I
    .locals 1

    .prologue
    .line 276
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFRichTextString;->_string:Lorg/apache/poi/hssf/record/common/UnicodeString;

    invoke-virtual {v0}, Lorg/apache/poi/hssf/record/common/UnicodeString;->getFormatRunCount()I

    move-result v0

    return v0
.end method

.method setUnicodeString(Lorg/apache/poi/hssf/record/common/UnicodeString;)V
    .locals 0
    .param p1, "str"    # Lorg/apache/poi/hssf/record/common/UnicodeString;

    .prologue
    .line 231
    iput-object p1, p0, Lorg/apache/poi/hssf/usermodel/HSSFRichTextString;->_string:Lorg/apache/poi/hssf/record/common/UnicodeString;

    .line 232
    return-void
.end method

.method setWorkbookReferences(Lorg/apache/poi/hssf/model/InternalWorkbook;Lorg/apache/poi/hssf/record/LabelSSTRecord;)V
    .locals 0
    .param p1, "book"    # Lorg/apache/poi/hssf/model/InternalWorkbook;
    .param p2, "record"    # Lorg/apache/poi/hssf/record/LabelSSTRecord;

    .prologue
    .line 103
    iput-object p1, p0, Lorg/apache/poi/hssf/usermodel/HSSFRichTextString;->_book:Lorg/apache/poi/hssf/model/InternalWorkbook;

    .line 104
    iput-object p2, p0, Lorg/apache/poi/hssf/usermodel/HSSFRichTextString;->_record:Lorg/apache/poi/hssf/record/LabelSSTRecord;

    .line 105
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 330
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFRichTextString;->_string:Lorg/apache/poi/hssf/record/common/UnicodeString;

    invoke-virtual {v0}, Lorg/apache/poi/hssf/record/common/UnicodeString;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
