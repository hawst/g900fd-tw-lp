.class public final Lorg/apache/poi/hssf/util/HSSFColor$MAROON;
.super Lorg/apache/poi/hssf/util/HSSFColor;
.source "HSSFColor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/poi/hssf/util/HSSFColor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "MAROON"
.end annotation


# static fields
.field public static final hexString:Ljava/lang/String; = "8000:0:0"

.field public static final index:S = 0x19s

.field public static final triplet:[S


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1529
    const/4 v0, 0x3

    new-array v0, v0, [S

    const/4 v1, 0x0

    .line 1530
    const/16 v2, 0x7f

    aput-short v2, v0, v1

    .line 1528
    sput-object v0, Lorg/apache/poi/hssf/util/HSSFColor$MAROON;->triplet:[S

    .line 1532
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1524
    invoke-direct {p0}, Lorg/apache/poi/hssf/util/HSSFColor;-><init>()V

    return-void
.end method


# virtual methods
.method public getHexString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1546
    const-string/jumbo v0, "8000:0:0"

    return-object v0
.end method

.method public getIndex()S
    .locals 1

    .prologue
    .line 1536
    const/16 v0, 0x19

    return v0
.end method

.method public getTriplet()[S
    .locals 1

    .prologue
    .line 1541
    sget-object v0, Lorg/apache/poi/hssf/util/HSSFColor$MAROON;->triplet:[S

    return-object v0
.end method
