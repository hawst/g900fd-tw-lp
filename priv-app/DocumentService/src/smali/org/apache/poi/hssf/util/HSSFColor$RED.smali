.class public final Lorg/apache/poi/hssf/util/HSSFColor$RED;
.super Lorg/apache/poi/hssf/util/HSSFColor;
.source "HSSFColor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/poi/hssf/util/HSSFColor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "RED"
.end annotation


# static fields
.field public static final hexString:Ljava/lang/String; = "FFFF:0:0"

.field public static final index:S = 0xas

.field public static final triplet:[S


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 721
    const/4 v0, 0x3

    new-array v0, v0, [S

    const/4 v1, 0x0

    .line 722
    const/16 v2, 0xff

    aput-short v2, v0, v1

    .line 720
    sput-object v0, Lorg/apache/poi/hssf/util/HSSFColor$RED;->triplet:[S

    .line 724
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 716
    invoke-direct {p0}, Lorg/apache/poi/hssf/util/HSSFColor;-><init>()V

    return-void
.end method


# virtual methods
.method public getHexString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 738
    const-string/jumbo v0, "FFFF:0:0"

    return-object v0
.end method

.method public getIndex()S
    .locals 1

    .prologue
    .line 728
    const/16 v0, 0xa

    return v0
.end method

.method public getTriplet()[S
    .locals 1

    .prologue
    .line 733
    sget-object v0, Lorg/apache/poi/hssf/util/HSSFColor$RED;->triplet:[S

    return-object v0
.end method
