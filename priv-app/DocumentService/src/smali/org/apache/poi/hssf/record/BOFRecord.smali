.class public final Lorg/apache/poi/hssf/record/BOFRecord;
.super Lorg/apache/poi/hssf/record/StandardRecord;
.source "BOFRecord.java"


# static fields
.field public static final BUILD:I = 0x10d3

.field public static final BUILD_YEAR:I = 0x7cc

.field public static final HISTORY_MASK:I = 0x41

.field public static final TYPE_CHART:I = 0x20

.field public static final TYPE_EXCEL_4_MACRO:I = 0x40

.field public static final TYPE_VB_MODULE:I = 0x6

.field public static final TYPE_WORKBOOK:I = 0x5

.field public static final TYPE_WORKSHEET:I = 0x10

.field public static final TYPE_WORKSPACE_FILE:I = 0x100

.field public static final VERSION:I = 0x600

.field public static final sid:S = 0x809s


# instance fields
.field private field_1_version:I

.field private field_2_type:I

.field private field_3_build:I

.field private field_4_year:I

.field private field_5_history:I

.field private field_6_rversion:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 64
    invoke-direct {p0}, Lorg/apache/poi/hssf/record/StandardRecord;-><init>()V

    .line 65
    return-void
.end method

.method private constructor <init>(I)V
    .locals 2
    .param p1, "type"    # I

    .prologue
    const/16 v1, 0x600

    .line 67
    invoke-direct {p0}, Lorg/apache/poi/hssf/record/StandardRecord;-><init>()V

    .line 68
    iput v1, p0, Lorg/apache/poi/hssf/record/BOFRecord;->field_1_version:I

    .line 69
    iput p1, p0, Lorg/apache/poi/hssf/record/BOFRecord;->field_2_type:I

    .line 70
    const/16 v0, 0x10d3

    iput v0, p0, Lorg/apache/poi/hssf/record/BOFRecord;->field_3_build:I

    .line 71
    const/16 v0, 0x7cc

    iput v0, p0, Lorg/apache/poi/hssf/record/BOFRecord;->field_4_year:I

    .line 72
    const/4 v0, 0x1

    iput v0, p0, Lorg/apache/poi/hssf/record/BOFRecord;->field_5_history:I

    .line 73
    iput v1, p0, Lorg/apache/poi/hssf/record/BOFRecord;->field_6_rversion:I

    .line 74
    return-void
.end method

.method public constructor <init>(Lorg/apache/poi/hssf/record/RecordInputStream;)V
    .locals 3
    .param p1, "in"    # Lorg/apache/poi/hssf/record/RecordInputStream;

    .prologue
    const/4 v2, 0x4

    const/4 v1, 0x2

    .line 80
    invoke-direct {p0}, Lorg/apache/poi/hssf/record/StandardRecord;-><init>()V

    .line 81
    invoke-virtual {p1}, Lorg/apache/poi/hssf/record/RecordInputStream;->readShort()S

    move-result v0

    iput v0, p0, Lorg/apache/poi/hssf/record/BOFRecord;->field_1_version:I

    .line 82
    invoke-virtual {p1}, Lorg/apache/poi/hssf/record/RecordInputStream;->readShort()S

    move-result v0

    iput v0, p0, Lorg/apache/poi/hssf/record/BOFRecord;->field_2_type:I

    .line 86
    invoke-virtual {p1}, Lorg/apache/poi/hssf/record/RecordInputStream;->remaining()I

    move-result v0

    if-lt v0, v1, :cond_0

    .line 87
    invoke-virtual {p1}, Lorg/apache/poi/hssf/record/RecordInputStream;->readShort()S

    move-result v0

    iput v0, p0, Lorg/apache/poi/hssf/record/BOFRecord;->field_3_build:I

    .line 89
    :cond_0
    invoke-virtual {p1}, Lorg/apache/poi/hssf/record/RecordInputStream;->remaining()I

    move-result v0

    if-lt v0, v1, :cond_1

    .line 90
    invoke-virtual {p1}, Lorg/apache/poi/hssf/record/RecordInputStream;->readShort()S

    move-result v0

    iput v0, p0, Lorg/apache/poi/hssf/record/BOFRecord;->field_4_year:I

    .line 92
    :cond_1
    invoke-virtual {p1}, Lorg/apache/poi/hssf/record/RecordInputStream;->remaining()I

    move-result v0

    if-lt v0, v2, :cond_2

    .line 93
    invoke-virtual {p1}, Lorg/apache/poi/hssf/record/RecordInputStream;->readInt()I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hssf/record/BOFRecord;->field_5_history:I

    .line 95
    :cond_2
    invoke-virtual {p1}, Lorg/apache/poi/hssf/record/RecordInputStream;->remaining()I

    move-result v0

    if-lt v0, v2, :cond_3

    .line 96
    invoke-virtual {p1}, Lorg/apache/poi/hssf/record/RecordInputStream;->readInt()I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hssf/record/BOFRecord;->field_6_rversion:I

    .line 98
    :cond_3
    return-void
.end method

.method public static createSheetBOF()Lorg/apache/poi/hssf/record/BOFRecord;
    .locals 2

    .prologue
    .line 77
    new-instance v0, Lorg/apache/poi/hssf/record/BOFRecord;

    const/16 v1, 0x10

    invoke-direct {v0, v1}, Lorg/apache/poi/hssf/record/BOFRecord;-><init>(I)V

    return-object v0
.end method

.method private getTypeName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 236
    iget v0, p0, Lorg/apache/poi/hssf/record/BOFRecord;->field_2_type:I

    sparse-switch v0, :sswitch_data_0

    .line 244
    const-string/jumbo v0, "#error unknown type#"

    :goto_0
    return-object v0

    .line 237
    :sswitch_0
    const-string/jumbo v0, "chart"

    goto :goto_0

    .line 238
    :sswitch_1
    const-string/jumbo v0, "excel 4 macro"

    goto :goto_0

    .line 239
    :sswitch_2
    const-string/jumbo v0, "vb module"

    goto :goto_0

    .line 240
    :sswitch_3
    const-string/jumbo v0, "workbook"

    goto :goto_0

    .line 241
    :sswitch_4
    const-string/jumbo v0, "worksheet"

    goto :goto_0

    .line 242
    :sswitch_5
    const-string/jumbo v0, "workspace file"

    goto :goto_0

    .line 236
    nop

    :sswitch_data_0
    .sparse-switch
        0x5 -> :sswitch_3
        0x6 -> :sswitch_2
        0x10 -> :sswitch_4
        0x20 -> :sswitch_0
        0x40 -> :sswitch_1
        0x100 -> :sswitch_5
    .end sparse-switch
.end method


# virtual methods
.method public clone()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 265
    new-instance v0, Lorg/apache/poi/hssf/record/BOFRecord;

    invoke-direct {v0}, Lorg/apache/poi/hssf/record/BOFRecord;-><init>()V

    .line 266
    .local v0, "rec":Lorg/apache/poi/hssf/record/BOFRecord;
    iget v1, p0, Lorg/apache/poi/hssf/record/BOFRecord;->field_1_version:I

    iput v1, v0, Lorg/apache/poi/hssf/record/BOFRecord;->field_1_version:I

    .line 267
    iget v1, p0, Lorg/apache/poi/hssf/record/BOFRecord;->field_2_type:I

    iput v1, v0, Lorg/apache/poi/hssf/record/BOFRecord;->field_2_type:I

    .line 268
    iget v1, p0, Lorg/apache/poi/hssf/record/BOFRecord;->field_3_build:I

    iput v1, v0, Lorg/apache/poi/hssf/record/BOFRecord;->field_3_build:I

    .line 269
    iget v1, p0, Lorg/apache/poi/hssf/record/BOFRecord;->field_4_year:I

    iput v1, v0, Lorg/apache/poi/hssf/record/BOFRecord;->field_4_year:I

    .line 270
    iget v1, p0, Lorg/apache/poi/hssf/record/BOFRecord;->field_5_history:I

    iput v1, v0, Lorg/apache/poi/hssf/record/BOFRecord;->field_5_history:I

    .line 271
    iget v1, p0, Lorg/apache/poi/hssf/record/BOFRecord;->field_6_rversion:I

    iput v1, v0, Lorg/apache/poi/hssf/record/BOFRecord;->field_6_rversion:I

    .line 272
    return-object v0
.end method

.method public getBuild()I
    .locals 1

    .prologue
    .line 189
    iget v0, p0, Lorg/apache/poi/hssf/record/BOFRecord;->field_3_build:I

    return v0
.end method

.method public getBuildYear()I
    .locals 1

    .prologue
    .line 198
    iget v0, p0, Lorg/apache/poi/hssf/record/BOFRecord;->field_4_year:I

    return v0
.end method

.method protected getDataSize()I
    .locals 1

    .prologue
    .line 257
    const/16 v0, 0x10

    return v0
.end method

.method public getHistoryBitMask()I
    .locals 1

    .prologue
    .line 207
    iget v0, p0, Lorg/apache/poi/hssf/record/BOFRecord;->field_5_history:I

    return v0
.end method

.method public getRequiredVersion()I
    .locals 1

    .prologue
    .line 217
    iget v0, p0, Lorg/apache/poi/hssf/record/BOFRecord;->field_6_rversion:I

    return v0
.end method

.method public getSid()S
    .locals 1

    .prologue
    .line 261
    const/16 v0, 0x809

    return v0
.end method

.method public getType()I
    .locals 1

    .prologue
    .line 180
    iget v0, p0, Lorg/apache/poi/hssf/record/BOFRecord;->field_2_type:I

    return v0
.end method

.method public getVersion()I
    .locals 1

    .prologue
    .line 166
    iget v0, p0, Lorg/apache/poi/hssf/record/BOFRecord;->field_1_version:I

    return v0
.end method

.method public serialize(Lorg/apache/poi/util/LittleEndianOutput;)V
    .locals 1
    .param p1, "out"    # Lorg/apache/poi/util/LittleEndianOutput;

    .prologue
    .line 248
    invoke-virtual {p0}, Lorg/apache/poi/hssf/record/BOFRecord;->getVersion()I

    move-result v0

    invoke-interface {p1, v0}, Lorg/apache/poi/util/LittleEndianOutput;->writeShort(I)V

    .line 249
    invoke-virtual {p0}, Lorg/apache/poi/hssf/record/BOFRecord;->getType()I

    move-result v0

    invoke-interface {p1, v0}, Lorg/apache/poi/util/LittleEndianOutput;->writeShort(I)V

    .line 250
    invoke-virtual {p0}, Lorg/apache/poi/hssf/record/BOFRecord;->getBuild()I

    move-result v0

    invoke-interface {p1, v0}, Lorg/apache/poi/util/LittleEndianOutput;->writeShort(I)V

    .line 251
    invoke-virtual {p0}, Lorg/apache/poi/hssf/record/BOFRecord;->getBuildYear()I

    move-result v0

    invoke-interface {p1, v0}, Lorg/apache/poi/util/LittleEndianOutput;->writeShort(I)V

    .line 252
    invoke-virtual {p0}, Lorg/apache/poi/hssf/record/BOFRecord;->getHistoryBitMask()I

    move-result v0

    invoke-interface {p1, v0}, Lorg/apache/poi/util/LittleEndianOutput;->writeInt(I)V

    .line 253
    invoke-virtual {p0}, Lorg/apache/poi/hssf/record/BOFRecord;->getRequiredVersion()I

    move-result v0

    invoke-interface {p1, v0}, Lorg/apache/poi/util/LittleEndianOutput;->writeInt(I)V

    .line 254
    return-void
.end method

.method public setBuild(I)V
    .locals 0
    .param p1, "build"    # I

    .prologue
    .line 129
    iput p1, p0, Lorg/apache/poi/hssf/record/BOFRecord;->field_3_build:I

    .line 130
    return-void
.end method

.method public setBuildYear(I)V
    .locals 0
    .param p1, "year"    # I

    .prologue
    .line 138
    iput p1, p0, Lorg/apache/poi/hssf/record/BOFRecord;->field_4_year:I

    .line 139
    return-void
.end method

.method public setHistoryBitMask(I)V
    .locals 0
    .param p1, "bitmask"    # I

    .prologue
    .line 147
    iput p1, p0, Lorg/apache/poi/hssf/record/BOFRecord;->field_5_history:I

    .line 148
    return-void
.end method

.method public setRequiredVersion(I)V
    .locals 0
    .param p1, "version"    # I

    .prologue
    .line 157
    iput p1, p0, Lorg/apache/poi/hssf/record/BOFRecord;->field_6_rversion:I

    .line 158
    return-void
.end method

.method public setType(I)V
    .locals 0
    .param p1, "type"    # I

    .prologue
    .line 120
    iput p1, p0, Lorg/apache/poi/hssf/record/BOFRecord;->field_2_type:I

    .line 121
    return-void
.end method

.method public setVersion(I)V
    .locals 0
    .param p1, "version"    # I

    .prologue
    .line 106
    iput p1, p0, Lorg/apache/poi/hssf/record/BOFRecord;->field_1_version:I

    .line 107
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 221
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 223
    .local v0, "buffer":Ljava/lang/StringBuffer;
    const-string/jumbo v1, "[BOF RECORD]\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 224
    const-string/jumbo v1, "    .version  = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hssf/record/BOFRecord;->getVersion()I

    move-result v2

    invoke-static {v2}, Lorg/apache/poi/util/HexDump;->shortToHex(I)[C

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append([C)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 225
    const-string/jumbo v1, "    .type     = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hssf/record/BOFRecord;->getType()I

    move-result v2

    invoke-static {v2}, Lorg/apache/poi/util/HexDump;->shortToHex(I)[C

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append([C)Ljava/lang/StringBuffer;

    .line 226
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-direct {p0}, Lorg/apache/poi/hssf/record/BOFRecord;->getTypeName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 227
    const-string/jumbo v1, "    .build    = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hssf/record/BOFRecord;->getBuild()I

    move-result v2

    invoke-static {v2}, Lorg/apache/poi/util/HexDump;->shortToHex(I)[C

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append([C)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 228
    const-string/jumbo v1, "    .buildyear= "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hssf/record/BOFRecord;->getBuildYear()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 229
    const-string/jumbo v1, "    .history  = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hssf/record/BOFRecord;->getHistoryBitMask()I

    move-result v2

    invoke-static {v2}, Lorg/apache/poi/util/HexDump;->intToHex(I)[C

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append([C)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 230
    const-string/jumbo v1, "    .reqver   = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hssf/record/BOFRecord;->getRequiredVersion()I

    move-result v2

    invoke-static {v2}, Lorg/apache/poi/util/HexDump;->intToHex(I)[C

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append([C)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 231
    const-string/jumbo v1, "[/BOF RECORD]\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 232
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
