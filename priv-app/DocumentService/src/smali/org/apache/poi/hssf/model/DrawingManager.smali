.class public Lorg/apache/poi/hssf/model/DrawingManager;
.super Ljava/lang/Object;
.source "DrawingManager.java"


# instance fields
.field dgMap:Ljava/util/Map;

.field dgg:Lorg/apache/poi/ddf/EscherDggRecord;


# direct methods
.method public constructor <init>(Lorg/apache/poi/ddf/EscherDggRecord;)V
    .locals 1
    .param p1, "dgg"    # Lorg/apache/poi/ddf/EscherDggRecord;

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/hssf/model/DrawingManager;->dgMap:Ljava/util/Map;

    .line 38
    iput-object p1, p0, Lorg/apache/poi/hssf/model/DrawingManager;->dgg:Lorg/apache/poi/ddf/EscherDggRecord;

    .line 39
    return-void
.end method


# virtual methods
.method public allocateShapeId(S)I
    .locals 7
    .param p1, "drawingGroupId"    # S

    .prologue
    .line 63
    iget-object v5, p0, Lorg/apache/poi/hssf/model/DrawingManager;->dgMap:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/ddf/EscherDgRecord;

    .line 64
    .local v1, "dg":Lorg/apache/poi/ddf/EscherDgRecord;
    invoke-virtual {v1}, Lorg/apache/poi/ddf/EscherDgRecord;->getLastMSOSPID()I

    move-result v3

    .line 68
    .local v3, "lastShapeId":I
    const/4 v4, 0x0

    .line 69
    .local v4, "newShapeId":I
    rem-int/lit16 v5, v3, 0x400

    const/16 v6, 0x3ff

    if-ne v5, v6, :cond_2

    .line 73
    invoke-virtual {p0}, Lorg/apache/poi/hssf/model/DrawingManager;->findFreeSPIDBlock()I

    move-result v4

    .line 75
    iget-object v5, p0, Lorg/apache/poi/hssf/model/DrawingManager;->dgg:Lorg/apache/poi/ddf/EscherDggRecord;

    const/4 v6, 0x1

    invoke-virtual {v5, p1, v6}, Lorg/apache/poi/ddf/EscherDggRecord;->addCluster(II)V

    .line 105
    :cond_0
    iget-object v5, p0, Lorg/apache/poi/hssf/model/DrawingManager;->dgg:Lorg/apache/poi/ddf/EscherDggRecord;

    iget-object v6, p0, Lorg/apache/poi/hssf/model/DrawingManager;->dgg:Lorg/apache/poi/ddf/EscherDggRecord;

    invoke-virtual {v6}, Lorg/apache/poi/ddf/EscherDggRecord;->getNumShapesSaved()I

    move-result v6

    add-int/lit8 v6, v6, 0x1

    invoke-virtual {v5, v6}, Lorg/apache/poi/ddf/EscherDggRecord;->setNumShapesSaved(I)V

    .line 107
    iget-object v5, p0, Lorg/apache/poi/hssf/model/DrawingManager;->dgg:Lorg/apache/poi/ddf/EscherDggRecord;

    invoke-virtual {v5}, Lorg/apache/poi/ddf/EscherDggRecord;->getShapeIdMax()I

    move-result v5

    if-lt v4, v5, :cond_1

    .line 111
    iget-object v5, p0, Lorg/apache/poi/hssf/model/DrawingManager;->dgg:Lorg/apache/poi/ddf/EscherDggRecord;

    add-int/lit8 v6, v4, 0x1

    invoke-virtual {v5, v6}, Lorg/apache/poi/ddf/EscherDggRecord;->setShapeIdMax(I)V

    .line 114
    :cond_1
    invoke-virtual {v1, v4}, Lorg/apache/poi/ddf/EscherDgRecord;->setLastMSOSPID(I)V

    .line 116
    invoke-virtual {v1}, Lorg/apache/poi/ddf/EscherDgRecord;->incrementShapeCount()V

    .line 119
    return v4

    .line 81
    :cond_2
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v5, p0, Lorg/apache/poi/hssf/model/DrawingManager;->dgg:Lorg/apache/poi/ddf/EscherDggRecord;

    invoke-virtual {v5}, Lorg/apache/poi/ddf/EscherDggRecord;->getFileIdClusters()[Lorg/apache/poi/ddf/EscherDggRecord$FileIdCluster;

    move-result-object v5

    array-length v5, v5

    if-ge v2, v5, :cond_0

    .line 83
    iget-object v5, p0, Lorg/apache/poi/hssf/model/DrawingManager;->dgg:Lorg/apache/poi/ddf/EscherDggRecord;

    invoke-virtual {v5}, Lorg/apache/poi/ddf/EscherDggRecord;->getFileIdClusters()[Lorg/apache/poi/ddf/EscherDggRecord$FileIdCluster;

    move-result-object v5

    aget-object v0, v5, v2

    .line 84
    .local v0, "c":Lorg/apache/poi/ddf/EscherDggRecord$FileIdCluster;
    invoke-virtual {v0}, Lorg/apache/poi/ddf/EscherDggRecord$FileIdCluster;->getDrawingGroupId()I

    move-result v5

    if-ne v5, p1, :cond_3

    .line 86
    invoke-virtual {v0}, Lorg/apache/poi/ddf/EscherDggRecord$FileIdCluster;->getNumShapeIdsUsed()I

    move-result v5

    const/16 v6, 0x400

    if-eq v5, v6, :cond_3

    .line 89
    invoke-virtual {v0}, Lorg/apache/poi/ddf/EscherDggRecord$FileIdCluster;->incrementShapeId()V

    .line 93
    :cond_3
    invoke-virtual {v1}, Lorg/apache/poi/ddf/EscherDgRecord;->getLastMSOSPID()I

    move-result v5

    const/4 v6, -0x1

    if-ne v5, v6, :cond_4

    .line 95
    invoke-virtual {p0}, Lorg/apache/poi/hssf/model/DrawingManager;->findFreeSPIDBlock()I

    move-result v4

    .line 81
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 100
    :cond_4
    invoke-virtual {v1}, Lorg/apache/poi/ddf/EscherDgRecord;->getLastMSOSPID()I

    move-result v5

    add-int/lit8 v4, v5, 0x1

    goto :goto_1
.end method

.method public createDgRecord()Lorg/apache/poi/ddf/EscherDgRecord;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 43
    new-instance v0, Lorg/apache/poi/ddf/EscherDgRecord;

    invoke-direct {v0}, Lorg/apache/poi/ddf/EscherDgRecord;-><init>()V

    .line 44
    .local v0, "dg":Lorg/apache/poi/ddf/EscherDgRecord;
    const/16 v2, -0xff8

    invoke-virtual {v0, v2}, Lorg/apache/poi/ddf/EscherDgRecord;->setRecordId(S)V

    .line 45
    invoke-virtual {p0}, Lorg/apache/poi/hssf/model/DrawingManager;->findNewDrawingGroupId()S

    move-result v1

    .line 46
    .local v1, "dgId":S
    shl-int/lit8 v2, v1, 0x4

    int-to-short v2, v2

    invoke-virtual {v0, v2}, Lorg/apache/poi/ddf/EscherDgRecord;->setOptions(S)V

    .line 47
    invoke-virtual {v0, v3}, Lorg/apache/poi/ddf/EscherDgRecord;->setNumShapes(I)V

    .line 48
    const/4 v2, -0x1

    invoke-virtual {v0, v2}, Lorg/apache/poi/ddf/EscherDgRecord;->setLastMSOSPID(I)V

    .line 49
    iget-object v2, p0, Lorg/apache/poi/hssf/model/DrawingManager;->dgg:Lorg/apache/poi/ddf/EscherDggRecord;

    invoke-virtual {v2, v1, v3}, Lorg/apache/poi/ddf/EscherDggRecord;->addCluster(II)V

    .line 50
    iget-object v2, p0, Lorg/apache/poi/hssf/model/DrawingManager;->dgg:Lorg/apache/poi/ddf/EscherDggRecord;

    iget-object v3, p0, Lorg/apache/poi/hssf/model/DrawingManager;->dgg:Lorg/apache/poi/ddf/EscherDggRecord;

    invoke-virtual {v3}, Lorg/apache/poi/ddf/EscherDggRecord;->getDrawingsSaved()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {v2, v3}, Lorg/apache/poi/ddf/EscherDggRecord;->setDrawingsSaved(I)V

    .line 51
    iget-object v2, p0, Lorg/apache/poi/hssf/model/DrawingManager;->dgMap:Ljava/util/Map;

    invoke-static {v1}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v3

    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 52
    return-object v0
.end method

.method drawingGroupExists(S)Z
    .locals 2
    .param p1, "dgId"    # S

    .prologue
    .line 133
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lorg/apache/poi/hssf/model/DrawingManager;->dgg:Lorg/apache/poi/ddf/EscherDggRecord;

    invoke-virtual {v1}, Lorg/apache/poi/ddf/EscherDggRecord;->getFileIdClusters()[Lorg/apache/poi/ddf/EscherDggRecord$FileIdCluster;

    move-result-object v1

    array-length v1, v1

    if-lt v0, v1, :cond_0

    .line 138
    const/4 v1, 0x0

    :goto_1
    return v1

    .line 135
    :cond_0
    iget-object v1, p0, Lorg/apache/poi/hssf/model/DrawingManager;->dgg:Lorg/apache/poi/ddf/EscherDggRecord;

    invoke-virtual {v1}, Lorg/apache/poi/ddf/EscherDggRecord;->getFileIdClusters()[Lorg/apache/poi/ddf/EscherDggRecord$FileIdCluster;

    move-result-object v1

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lorg/apache/poi/ddf/EscherDggRecord$FileIdCluster;->getDrawingGroupId()I

    move-result v1

    if-ne v1, p1, :cond_1

    .line 136
    const/4 v1, 0x1

    goto :goto_1

    .line 133
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method findFreeSPIDBlock()I
    .locals 3

    .prologue
    .line 143
    iget-object v2, p0, Lorg/apache/poi/hssf/model/DrawingManager;->dgg:Lorg/apache/poi/ddf/EscherDggRecord;

    invoke-virtual {v2}, Lorg/apache/poi/ddf/EscherDggRecord;->getShapeIdMax()I

    move-result v0

    .line 144
    .local v0, "max":I
    div-int/lit16 v2, v0, 0x400

    add-int/lit8 v2, v2, 0x1

    mul-int/lit16 v1, v2, 0x400

    .line 145
    .local v1, "next":I
    return v1
.end method

.method findNewDrawingGroupId()S
    .locals 2

    .prologue
    .line 125
    const/4 v0, 0x1

    .line 126
    .local v0, "dgId":S
    :goto_0
    invoke-virtual {p0, v0}, Lorg/apache/poi/hssf/model/DrawingManager;->drawingGroupExists(S)Z

    move-result v1

    if-nez v1, :cond_0

    .line 128
    return v0

    .line 127
    :cond_0
    add-int/lit8 v1, v0, 0x1

    int-to-short v0, v1

    goto :goto_0
.end method

.method public getDgg()Lorg/apache/poi/ddf/EscherDggRecord;
    .locals 1

    .prologue
    .line 150
    iget-object v0, p0, Lorg/apache/poi/hssf/model/DrawingManager;->dgg:Lorg/apache/poi/ddf/EscherDggRecord;

    return-object v0
.end method
