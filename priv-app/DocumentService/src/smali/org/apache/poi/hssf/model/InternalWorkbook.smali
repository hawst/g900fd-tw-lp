.class public final Lorg/apache/poi/hssf/model/InternalWorkbook;
.super Ljava/lang/Object;
.source "InternalWorkbook.java"


# annotations
.annotation runtime Lorg/apache/poi/util/Internal;
.end annotation


# static fields
.field private static final CODEPAGE:S = 0x4b0s

.field private static final DEBUG:I = 0x1

.field private static final MAX_SENSITIVE_SHEET_NAME_LEN:I = 0x1f

.field private static final log:Lorg/apache/poi/util/POILogger;


# instance fields
.field private final boundsheets:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/apache/poi/hssf/record/BoundSheetRecord;",
            ">;"
        }
    .end annotation
.end field

.field private final commentRecords:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lorg/apache/poi/hssf/record/NameCommentRecord;",
            ">;"
        }
    .end annotation
.end field

.field private drawingManager:Lorg/apache/poi/hssf/model/DrawingManager2;

.field private escherBSERecords:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/apache/poi/ddf/EscherBSERecord;",
            ">;"
        }
    .end annotation
.end field

.field private fileShare:Lorg/apache/poi/hssf/record/FileSharingRecord;

.field private final formats:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/apache/poi/hssf/record/FormatRecord;",
            ">;"
        }
    .end annotation
.end field

.field private final hyperlinks:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/apache/poi/hssf/record/HyperlinkRecord;",
            ">;"
        }
    .end annotation
.end field

.field private linkTable:Lorg/apache/poi/hssf/model/LinkTable;

.field private maxformatid:I

.field private numfonts:I

.field private numxfs:I

.field private final records:Lorg/apache/poi/hssf/model/WorkbookRecordList;

.field protected sst:Lorg/apache/poi/hssf/record/SSTRecord;

.field private uses1904datewindowing:Z

.field private windowOne:Lorg/apache/poi/hssf/record/WindowOneRecord;

.field private writeAccess:Lorg/apache/poi/hssf/record/WriteAccessRecord;

.field private writeProtect:Lorg/apache/poi/hssf/record/WriteProtectRecord;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 132
    const-class v0, Lorg/apache/poi/hssf/model/InternalWorkbook;

    invoke-static {v0}, Lorg/apache/poi/util/POILogFactory;->getLogger(Ljava/lang/Class;)Lorg/apache/poi/util/POILogger;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hssf/model/InternalWorkbook;->log:Lorg/apache/poi/util/POILogger;

    .line 139
    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 183
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 184
    new-instance v0, Lorg/apache/poi/hssf/model/WorkbookRecordList;

    invoke-direct {v0}, Lorg/apache/poi/hssf/model/WorkbookRecordList;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->records:Lorg/apache/poi/hssf/model/WorkbookRecordList;

    .line 186
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->boundsheets:Ljava/util/List;

    .line 187
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->formats:Ljava/util/List;

    .line 188
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->hyperlinks:Ljava/util/List;

    .line 189
    iput v1, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->numxfs:I

    .line 190
    iput v1, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->numfonts:I

    .line 191
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->maxformatid:I

    .line 192
    iput-boolean v1, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->uses1904datewindowing:Z

    .line 193
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->escherBSERecords:Ljava/util/List;

    .line 194
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->commentRecords:Ljava/util/Map;

    .line 195
    return-void
.end method

.method private checkSheets(I)V
    .locals 3
    .param p1, "sheetnum"    # I

    .prologue
    .line 732
    iget-object v1, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->boundsheets:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-gt v1, p1, :cond_1

    .line 733
    iget-object v1, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->boundsheets:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    if-gt v1, p1, :cond_0

    .line 734
    new-instance v1, Ljava/lang/RuntimeException;

    const-string/jumbo v2, "Sheet number out of bounds!"

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 736
    :cond_0
    invoke-static {p1}, Lorg/apache/poi/hssf/model/InternalWorkbook;->createBoundSheet(I)Lorg/apache/poi/hssf/record/BoundSheetRecord;

    move-result-object v0

    .line 738
    .local v0, "bsr":Lorg/apache/poi/hssf/record/BoundSheetRecord;
    iget-object v1, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->records:Lorg/apache/poi/hssf/model/WorkbookRecordList;

    iget-object v2, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->records:Lorg/apache/poi/hssf/model/WorkbookRecordList;

    invoke-virtual {v2}, Lorg/apache/poi/hssf/model/WorkbookRecordList;->getBspos()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v1, v2, v0}, Lorg/apache/poi/hssf/model/WorkbookRecordList;->add(ILorg/apache/poi/hssf/record/Record;)V

    .line 739
    iget-object v1, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->records:Lorg/apache/poi/hssf/model/WorkbookRecordList;

    iget-object v2, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->records:Lorg/apache/poi/hssf/model/WorkbookRecordList;

    invoke-virtual {v2}, Lorg/apache/poi/hssf/model/WorkbookRecordList;->getBspos()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v1, v2}, Lorg/apache/poi/hssf/model/WorkbookRecordList;->setBspos(I)V

    .line 740
    iget-object v1, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->boundsheets:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 741
    invoke-direct {p0}, Lorg/apache/poi/hssf/model/InternalWorkbook;->getOrCreateLinkTable()Lorg/apache/poi/hssf/model/LinkTable;

    move-result-object v1

    invoke-virtual {v1, p1}, Lorg/apache/poi/hssf/model/LinkTable;->checkExternSheet(I)I

    .line 742
    invoke-direct {p0}, Lorg/apache/poi/hssf/model/InternalWorkbook;->fixTabIdRecord()I

    .line 744
    .end local v0    # "bsr":Lorg/apache/poi/hssf/record/BoundSheetRecord;
    :cond_1
    return-void
.end method

.method private static createBOF()Lorg/apache/poi/hssf/record/BOFRecord;
    .locals 2

    .prologue
    .line 1100
    new-instance v0, Lorg/apache/poi/hssf/record/BOFRecord;

    invoke-direct {v0}, Lorg/apache/poi/hssf/record/BOFRecord;-><init>()V

    .line 1102
    .local v0, "retval":Lorg/apache/poi/hssf/record/BOFRecord;
    const/16 v1, 0x600

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/record/BOFRecord;->setVersion(I)V

    .line 1103
    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/record/BOFRecord;->setType(I)V

    .line 1104
    const/16 v1, 0x10d3

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/record/BOFRecord;->setBuild(I)V

    .line 1105
    const/16 v1, 0x7cc

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/record/BOFRecord;->setBuildYear(I)V

    .line 1106
    const/16 v1, 0x41

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/record/BOFRecord;->setHistoryBitMask(I)V

    .line 1107
    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/record/BOFRecord;->setRequiredVersion(I)V

    .line 1108
    return-object v0
.end method

.method private static createBackup()Lorg/apache/poi/hssf/record/BackupRecord;
    .locals 2

    .prologue
    .line 1239
    new-instance v0, Lorg/apache/poi/hssf/record/BackupRecord;

    invoke-direct {v0}, Lorg/apache/poi/hssf/record/BackupRecord;-><init>()V

    .line 1241
    .local v0, "retval":Lorg/apache/poi/hssf/record/BackupRecord;
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/record/BackupRecord;->setBackup(S)V

    .line 1242
    return-object v0
.end method

.method private static createBookBool()Lorg/apache/poi/hssf/record/BookBoolRecord;
    .locals 2

    .prologue
    .line 1284
    new-instance v0, Lorg/apache/poi/hssf/record/BookBoolRecord;

    invoke-direct {v0}, Lorg/apache/poi/hssf/record/BookBoolRecord;-><init>()V

    .line 1285
    .local v0, "retval":Lorg/apache/poi/hssf/record/BookBoolRecord;
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/record/BookBoolRecord;->setSaveLinkValues(S)V

    .line 1286
    return-object v0
.end method

.method private static createBoundSheet(I)Lorg/apache/poi/hssf/record/BoundSheetRecord;
    .locals 3
    .param p0, "id"    # I

    .prologue
    .line 1755
    new-instance v0, Lorg/apache/poi/hssf/record/BoundSheetRecord;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Sheet"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    add-int/lit8 v2, p0, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/poi/hssf/record/BoundSheetRecord;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method private static createCodepage()Lorg/apache/poi/hssf/record/CodepageRecord;
    .locals 2

    .prologue
    .line 1142
    new-instance v0, Lorg/apache/poi/hssf/record/CodepageRecord;

    invoke-direct {v0}, Lorg/apache/poi/hssf/record/CodepageRecord;-><init>()V

    .line 1144
    .local v0, "retval":Lorg/apache/poi/hssf/record/CodepageRecord;
    const/16 v1, 0x4b0

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/record/CodepageRecord;->setCodepage(S)V

    .line 1145
    return-object v0
.end method

.method private static createCountry()Lorg/apache/poi/hssf/record/CountryRecord;
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1763
    new-instance v0, Lorg/apache/poi/hssf/record/CountryRecord;

    invoke-direct {v0}, Lorg/apache/poi/hssf/record/CountryRecord;-><init>()V

    .line 1765
    .local v0, "retval":Lorg/apache/poi/hssf/record/CountryRecord;
    invoke-virtual {v0, v3}, Lorg/apache/poi/hssf/record/CountryRecord;->setDefaultCountry(S)V

    .line 1768
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "ru_RU"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1769
    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/record/CountryRecord;->setCurrentCountry(S)V

    .line 1775
    :goto_0
    return-object v0

    .line 1772
    :cond_0
    invoke-virtual {v0, v3}, Lorg/apache/poi/hssf/record/CountryRecord;->setCurrentCountry(S)V

    goto :goto_0
.end method

.method private static createDSF()Lorg/apache/poi/hssf/record/DSFRecord;
    .locals 2

    .prologue
    .line 1149
    new-instance v0, Lorg/apache/poi/hssf/record/DSFRecord;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lorg/apache/poi/hssf/record/DSFRecord;-><init>(Z)V

    return-object v0
.end method

.method private static createDateWindow1904()Lorg/apache/poi/hssf/record/DateWindow1904Record;
    .locals 2

    .prologue
    .line 1258
    new-instance v0, Lorg/apache/poi/hssf/record/DateWindow1904Record;

    invoke-direct {v0}, Lorg/apache/poi/hssf/record/DateWindow1904Record;-><init>()V

    .line 1260
    .local v0, "retval":Lorg/apache/poi/hssf/record/DateWindow1904Record;
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/record/DateWindow1904Record;->setWindowing(S)V

    .line 1261
    return-object v0
.end method

.method private static createExtendedFormat()Lorg/apache/poi/hssf/record/ExtendedFormatRecord;
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 1665
    new-instance v0, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;

    invoke-direct {v0}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;-><init>()V

    .line 1667
    .local v0, "retval":Lorg/apache/poi/hssf/record/ExtendedFormatRecord;
    invoke-virtual {v0, v2}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setFontIndex(S)V

    .line 1668
    invoke-virtual {v0, v2}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setFormatIndex(S)V

    .line 1669
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setCellOptions(S)V

    .line 1670
    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setAlignmentOptions(S)V

    .line 1671
    invoke-virtual {v0, v2}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setIndentionOptions(S)V

    .line 1672
    invoke-virtual {v0, v2}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setBorderOptions(S)V

    .line 1673
    invoke-virtual {v0, v2}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setPaletteOptions(S)V

    .line 1674
    invoke-virtual {v0, v2}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setAdtlPaletteOptions(S)V

    .line 1675
    const/16 v1, 0x20c0

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setFillPaletteOptions(S)V

    .line 1676
    invoke-virtual {v0, v3}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setTopBorderPaletteIdx(S)V

    .line 1677
    invoke-virtual {v0, v3}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setBottomBorderPaletteIdx(S)V

    .line 1678
    invoke-virtual {v0, v3}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setLeftBorderPaletteIdx(S)V

    .line 1679
    invoke-virtual {v0, v3}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setRightBorderPaletteIdx(S)V

    .line 1680
    return-object v0
.end method

.method private static createExtendedFormat(I)Lorg/apache/poi/hssf/record/ExtendedFormatRecord;
    .locals 7
    .param p0, "id"    # I

    .prologue
    const/16 v6, -0xc00

    const/16 v5, -0xb

    const/16 v4, 0x20c0

    const/16 v3, 0x20

    const/4 v2, 0x0

    .line 1338
    new-instance v0, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;

    invoke-direct {v0}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;-><init>()V

    .line 1340
    .local v0, "retval":Lorg/apache/poi/hssf/record/ExtendedFormatRecord;
    packed-switch p0, :pswitch_data_0

    .line 1657
    :goto_0
    return-object v0

    .line 1343
    :pswitch_0
    invoke-virtual {v0, v2}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setFontIndex(S)V

    .line 1344
    invoke-virtual {v0, v2}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setFormatIndex(S)V

    .line 1345
    invoke-virtual {v0, v5}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setCellOptions(S)V

    .line 1346
    invoke-virtual {v0, v3}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setAlignmentOptions(S)V

    .line 1347
    invoke-virtual {v0, v2}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setIndentionOptions(S)V

    .line 1348
    invoke-virtual {v0, v2}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setBorderOptions(S)V

    .line 1349
    invoke-virtual {v0, v2}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setPaletteOptions(S)V

    .line 1350
    invoke-virtual {v0, v2}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setAdtlPaletteOptions(S)V

    .line 1351
    invoke-virtual {v0, v4}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setFillPaletteOptions(S)V

    goto :goto_0

    .line 1355
    :pswitch_1
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setFontIndex(S)V

    .line 1356
    invoke-virtual {v0, v2}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setFormatIndex(S)V

    .line 1357
    invoke-virtual {v0, v5}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setCellOptions(S)V

    .line 1358
    invoke-virtual {v0, v3}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setAlignmentOptions(S)V

    .line 1359
    invoke-virtual {v0, v6}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setIndentionOptions(S)V

    .line 1360
    invoke-virtual {v0, v2}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setBorderOptions(S)V

    .line 1361
    invoke-virtual {v0, v2}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setPaletteOptions(S)V

    .line 1362
    invoke-virtual {v0, v2}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setAdtlPaletteOptions(S)V

    .line 1363
    invoke-virtual {v0, v4}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setFillPaletteOptions(S)V

    goto :goto_0

    .line 1367
    :pswitch_2
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setFontIndex(S)V

    .line 1368
    invoke-virtual {v0, v2}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setFormatIndex(S)V

    .line 1369
    invoke-virtual {v0, v5}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setCellOptions(S)V

    .line 1370
    invoke-virtual {v0, v3}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setAlignmentOptions(S)V

    .line 1371
    invoke-virtual {v0, v6}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setIndentionOptions(S)V

    .line 1372
    invoke-virtual {v0, v2}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setBorderOptions(S)V

    .line 1373
    invoke-virtual {v0, v2}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setPaletteOptions(S)V

    .line 1374
    invoke-virtual {v0, v2}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setAdtlPaletteOptions(S)V

    .line 1375
    invoke-virtual {v0, v4}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setFillPaletteOptions(S)V

    goto :goto_0

    .line 1379
    :pswitch_3
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setFontIndex(S)V

    .line 1380
    invoke-virtual {v0, v2}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setFormatIndex(S)V

    .line 1381
    invoke-virtual {v0, v5}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setCellOptions(S)V

    .line 1382
    invoke-virtual {v0, v3}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setAlignmentOptions(S)V

    .line 1383
    invoke-virtual {v0, v6}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setIndentionOptions(S)V

    .line 1384
    invoke-virtual {v0, v2}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setBorderOptions(S)V

    .line 1385
    invoke-virtual {v0, v2}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setPaletteOptions(S)V

    .line 1386
    invoke-virtual {v0, v2}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setAdtlPaletteOptions(S)V

    .line 1387
    invoke-virtual {v0, v4}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setFillPaletteOptions(S)V

    goto :goto_0

    .line 1391
    :pswitch_4
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setFontIndex(S)V

    .line 1392
    invoke-virtual {v0, v2}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setFormatIndex(S)V

    .line 1393
    invoke-virtual {v0, v5}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setCellOptions(S)V

    .line 1394
    invoke-virtual {v0, v3}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setAlignmentOptions(S)V

    .line 1395
    invoke-virtual {v0, v6}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setIndentionOptions(S)V

    .line 1396
    invoke-virtual {v0, v2}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setBorderOptions(S)V

    .line 1397
    invoke-virtual {v0, v2}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setPaletteOptions(S)V

    .line 1398
    invoke-virtual {v0, v2}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setAdtlPaletteOptions(S)V

    .line 1399
    invoke-virtual {v0, v4}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setFillPaletteOptions(S)V

    goto/16 :goto_0

    .line 1403
    :pswitch_5
    invoke-virtual {v0, v2}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setFontIndex(S)V

    .line 1404
    invoke-virtual {v0, v2}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setFormatIndex(S)V

    .line 1405
    invoke-virtual {v0, v5}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setCellOptions(S)V

    .line 1406
    invoke-virtual {v0, v3}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setAlignmentOptions(S)V

    .line 1407
    invoke-virtual {v0, v6}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setIndentionOptions(S)V

    .line 1408
    invoke-virtual {v0, v2}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setBorderOptions(S)V

    .line 1409
    invoke-virtual {v0, v2}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setPaletteOptions(S)V

    .line 1410
    invoke-virtual {v0, v2}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setAdtlPaletteOptions(S)V

    .line 1411
    invoke-virtual {v0, v4}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setFillPaletteOptions(S)V

    goto/16 :goto_0

    .line 1415
    :pswitch_6
    invoke-virtual {v0, v2}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setFontIndex(S)V

    .line 1416
    invoke-virtual {v0, v2}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setFormatIndex(S)V

    .line 1417
    invoke-virtual {v0, v5}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setCellOptions(S)V

    .line 1418
    invoke-virtual {v0, v3}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setAlignmentOptions(S)V

    .line 1419
    invoke-virtual {v0, v6}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setIndentionOptions(S)V

    .line 1420
    invoke-virtual {v0, v2}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setBorderOptions(S)V

    .line 1421
    invoke-virtual {v0, v2}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setPaletteOptions(S)V

    .line 1422
    invoke-virtual {v0, v2}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setAdtlPaletteOptions(S)V

    .line 1423
    invoke-virtual {v0, v4}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setFillPaletteOptions(S)V

    goto/16 :goto_0

    .line 1427
    :pswitch_7
    invoke-virtual {v0, v2}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setFontIndex(S)V

    .line 1428
    invoke-virtual {v0, v2}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setFormatIndex(S)V

    .line 1429
    invoke-virtual {v0, v5}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setCellOptions(S)V

    .line 1430
    invoke-virtual {v0, v3}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setAlignmentOptions(S)V

    .line 1431
    invoke-virtual {v0, v6}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setIndentionOptions(S)V

    .line 1432
    invoke-virtual {v0, v2}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setBorderOptions(S)V

    .line 1433
    invoke-virtual {v0, v2}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setPaletteOptions(S)V

    .line 1434
    invoke-virtual {v0, v2}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setAdtlPaletteOptions(S)V

    .line 1435
    invoke-virtual {v0, v4}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setFillPaletteOptions(S)V

    goto/16 :goto_0

    .line 1439
    :pswitch_8
    invoke-virtual {v0, v2}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setFontIndex(S)V

    .line 1440
    invoke-virtual {v0, v2}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setFormatIndex(S)V

    .line 1441
    invoke-virtual {v0, v5}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setCellOptions(S)V

    .line 1442
    invoke-virtual {v0, v3}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setAlignmentOptions(S)V

    .line 1443
    invoke-virtual {v0, v6}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setIndentionOptions(S)V

    .line 1444
    invoke-virtual {v0, v2}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setBorderOptions(S)V

    .line 1445
    invoke-virtual {v0, v2}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setPaletteOptions(S)V

    .line 1446
    invoke-virtual {v0, v2}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setAdtlPaletteOptions(S)V

    .line 1447
    invoke-virtual {v0, v4}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setFillPaletteOptions(S)V

    goto/16 :goto_0

    .line 1451
    :pswitch_9
    invoke-virtual {v0, v2}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setFontIndex(S)V

    .line 1452
    invoke-virtual {v0, v2}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setFormatIndex(S)V

    .line 1453
    invoke-virtual {v0, v5}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setCellOptions(S)V

    .line 1454
    invoke-virtual {v0, v3}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setAlignmentOptions(S)V

    .line 1455
    invoke-virtual {v0, v6}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setIndentionOptions(S)V

    .line 1456
    invoke-virtual {v0, v2}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setBorderOptions(S)V

    .line 1457
    invoke-virtual {v0, v2}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setPaletteOptions(S)V

    .line 1458
    invoke-virtual {v0, v2}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setAdtlPaletteOptions(S)V

    .line 1459
    invoke-virtual {v0, v4}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setFillPaletteOptions(S)V

    goto/16 :goto_0

    .line 1463
    :pswitch_a
    invoke-virtual {v0, v2}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setFontIndex(S)V

    .line 1464
    invoke-virtual {v0, v2}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setFormatIndex(S)V

    .line 1465
    invoke-virtual {v0, v5}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setCellOptions(S)V

    .line 1466
    invoke-virtual {v0, v3}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setAlignmentOptions(S)V

    .line 1467
    invoke-virtual {v0, v6}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setIndentionOptions(S)V

    .line 1468
    invoke-virtual {v0, v2}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setBorderOptions(S)V

    .line 1469
    invoke-virtual {v0, v2}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setPaletteOptions(S)V

    .line 1470
    invoke-virtual {v0, v2}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setAdtlPaletteOptions(S)V

    .line 1471
    invoke-virtual {v0, v4}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setFillPaletteOptions(S)V

    goto/16 :goto_0

    .line 1475
    :pswitch_b
    invoke-virtual {v0, v2}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setFontIndex(S)V

    .line 1476
    invoke-virtual {v0, v2}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setFormatIndex(S)V

    .line 1477
    invoke-virtual {v0, v5}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setCellOptions(S)V

    .line 1478
    invoke-virtual {v0, v3}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setAlignmentOptions(S)V

    .line 1479
    invoke-virtual {v0, v6}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setIndentionOptions(S)V

    .line 1480
    invoke-virtual {v0, v2}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setBorderOptions(S)V

    .line 1481
    invoke-virtual {v0, v2}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setPaletteOptions(S)V

    .line 1482
    invoke-virtual {v0, v2}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setAdtlPaletteOptions(S)V

    .line 1483
    invoke-virtual {v0, v4}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setFillPaletteOptions(S)V

    goto/16 :goto_0

    .line 1487
    :pswitch_c
    invoke-virtual {v0, v2}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setFontIndex(S)V

    .line 1488
    invoke-virtual {v0, v2}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setFormatIndex(S)V

    .line 1489
    invoke-virtual {v0, v5}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setCellOptions(S)V

    .line 1490
    invoke-virtual {v0, v3}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setAlignmentOptions(S)V

    .line 1491
    invoke-virtual {v0, v6}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setIndentionOptions(S)V

    .line 1492
    invoke-virtual {v0, v2}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setBorderOptions(S)V

    .line 1493
    invoke-virtual {v0, v2}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setPaletteOptions(S)V

    .line 1494
    invoke-virtual {v0, v2}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setAdtlPaletteOptions(S)V

    .line 1495
    invoke-virtual {v0, v4}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setFillPaletteOptions(S)V

    goto/16 :goto_0

    .line 1499
    :pswitch_d
    invoke-virtual {v0, v2}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setFontIndex(S)V

    .line 1500
    invoke-virtual {v0, v2}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setFormatIndex(S)V

    .line 1501
    invoke-virtual {v0, v5}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setCellOptions(S)V

    .line 1502
    invoke-virtual {v0, v3}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setAlignmentOptions(S)V

    .line 1503
    invoke-virtual {v0, v6}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setIndentionOptions(S)V

    .line 1504
    invoke-virtual {v0, v2}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setBorderOptions(S)V

    .line 1505
    invoke-virtual {v0, v2}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setPaletteOptions(S)V

    .line 1506
    invoke-virtual {v0, v2}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setAdtlPaletteOptions(S)V

    .line 1507
    invoke-virtual {v0, v4}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setFillPaletteOptions(S)V

    goto/16 :goto_0

    .line 1511
    :pswitch_e
    invoke-virtual {v0, v2}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setFontIndex(S)V

    .line 1512
    invoke-virtual {v0, v2}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setFormatIndex(S)V

    .line 1513
    invoke-virtual {v0, v5}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setCellOptions(S)V

    .line 1514
    invoke-virtual {v0, v3}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setAlignmentOptions(S)V

    .line 1515
    invoke-virtual {v0, v6}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setIndentionOptions(S)V

    .line 1516
    invoke-virtual {v0, v2}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setBorderOptions(S)V

    .line 1517
    invoke-virtual {v0, v2}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setPaletteOptions(S)V

    .line 1518
    invoke-virtual {v0, v2}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setAdtlPaletteOptions(S)V

    .line 1519
    invoke-virtual {v0, v4}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setFillPaletteOptions(S)V

    goto/16 :goto_0

    .line 1524
    :pswitch_f
    invoke-virtual {v0, v2}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setFontIndex(S)V

    .line 1525
    invoke-virtual {v0, v2}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setFormatIndex(S)V

    .line 1526
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setCellOptions(S)V

    .line 1527
    invoke-virtual {v0, v3}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setAlignmentOptions(S)V

    .line 1528
    invoke-virtual {v0, v2}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setIndentionOptions(S)V

    .line 1529
    invoke-virtual {v0, v2}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setBorderOptions(S)V

    .line 1530
    invoke-virtual {v0, v2}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setPaletteOptions(S)V

    .line 1531
    invoke-virtual {v0, v2}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setAdtlPaletteOptions(S)V

    .line 1532
    invoke-virtual {v0, v4}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setFillPaletteOptions(S)V

    goto/16 :goto_0

    .line 1537
    :pswitch_10
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setFontIndex(S)V

    .line 1538
    const/16 v1, 0x2b

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setFormatIndex(S)V

    .line 1539
    invoke-virtual {v0, v5}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setCellOptions(S)V

    .line 1540
    invoke-virtual {v0, v3}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setAlignmentOptions(S)V

    .line 1541
    const/16 v1, -0x800

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setIndentionOptions(S)V

    .line 1542
    invoke-virtual {v0, v2}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setBorderOptions(S)V

    .line 1543
    invoke-virtual {v0, v2}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setPaletteOptions(S)V

    .line 1544
    invoke-virtual {v0, v2}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setAdtlPaletteOptions(S)V

    .line 1545
    invoke-virtual {v0, v4}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setFillPaletteOptions(S)V

    goto/16 :goto_0

    .line 1549
    :pswitch_11
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setFontIndex(S)V

    .line 1550
    const/16 v1, 0x29

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setFormatIndex(S)V

    .line 1551
    invoke-virtual {v0, v5}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setCellOptions(S)V

    .line 1552
    invoke-virtual {v0, v3}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setAlignmentOptions(S)V

    .line 1553
    const/16 v1, -0x800

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setIndentionOptions(S)V

    .line 1554
    invoke-virtual {v0, v2}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setBorderOptions(S)V

    .line 1555
    invoke-virtual {v0, v2}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setPaletteOptions(S)V

    .line 1556
    invoke-virtual {v0, v2}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setAdtlPaletteOptions(S)V

    .line 1557
    invoke-virtual {v0, v4}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setFillPaletteOptions(S)V

    goto/16 :goto_0

    .line 1561
    :pswitch_12
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setFontIndex(S)V

    .line 1562
    const/16 v1, 0x2c

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setFormatIndex(S)V

    .line 1563
    invoke-virtual {v0, v5}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setCellOptions(S)V

    .line 1564
    invoke-virtual {v0, v3}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setAlignmentOptions(S)V

    .line 1565
    const/16 v1, -0x800

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setIndentionOptions(S)V

    .line 1566
    invoke-virtual {v0, v2}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setBorderOptions(S)V

    .line 1567
    invoke-virtual {v0, v2}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setPaletteOptions(S)V

    .line 1568
    invoke-virtual {v0, v2}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setAdtlPaletteOptions(S)V

    .line 1569
    invoke-virtual {v0, v4}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setFillPaletteOptions(S)V

    goto/16 :goto_0

    .line 1573
    :pswitch_13
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setFontIndex(S)V

    .line 1574
    const/16 v1, 0x2a

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setFormatIndex(S)V

    .line 1575
    invoke-virtual {v0, v5}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setCellOptions(S)V

    .line 1576
    invoke-virtual {v0, v3}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setAlignmentOptions(S)V

    .line 1577
    const/16 v1, -0x800

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setIndentionOptions(S)V

    .line 1578
    invoke-virtual {v0, v2}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setBorderOptions(S)V

    .line 1579
    invoke-virtual {v0, v2}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setPaletteOptions(S)V

    .line 1580
    invoke-virtual {v0, v2}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setAdtlPaletteOptions(S)V

    .line 1581
    invoke-virtual {v0, v4}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setFillPaletteOptions(S)V

    goto/16 :goto_0

    .line 1585
    :pswitch_14
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setFontIndex(S)V

    .line 1586
    const/16 v1, 0x9

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setFormatIndex(S)V

    .line 1587
    invoke-virtual {v0, v5}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setCellOptions(S)V

    .line 1588
    invoke-virtual {v0, v3}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setAlignmentOptions(S)V

    .line 1589
    const/16 v1, -0x800

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setIndentionOptions(S)V

    .line 1590
    invoke-virtual {v0, v2}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setBorderOptions(S)V

    .line 1591
    invoke-virtual {v0, v2}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setPaletteOptions(S)V

    .line 1592
    invoke-virtual {v0, v2}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setAdtlPaletteOptions(S)V

    .line 1593
    invoke-virtual {v0, v4}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setFillPaletteOptions(S)V

    goto/16 :goto_0

    .line 1598
    :pswitch_15
    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setFontIndex(S)V

    .line 1599
    invoke-virtual {v0, v2}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setFormatIndex(S)V

    .line 1600
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setCellOptions(S)V

    .line 1601
    invoke-virtual {v0, v3}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setAlignmentOptions(S)V

    .line 1602
    const/16 v1, 0x800

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setIndentionOptions(S)V

    .line 1603
    invoke-virtual {v0, v2}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setBorderOptions(S)V

    .line 1604
    invoke-virtual {v0, v2}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setPaletteOptions(S)V

    .line 1605
    invoke-virtual {v0, v2}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setAdtlPaletteOptions(S)V

    .line 1606
    invoke-virtual {v0, v4}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setFillPaletteOptions(S)V

    goto/16 :goto_0

    .line 1610
    :pswitch_16
    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setFontIndex(S)V

    .line 1611
    invoke-virtual {v0, v2}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setFormatIndex(S)V

    .line 1612
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setCellOptions(S)V

    .line 1613
    invoke-virtual {v0, v3}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setAlignmentOptions(S)V

    .line 1614
    const/16 v1, 0x5c00

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setIndentionOptions(S)V

    .line 1615
    invoke-virtual {v0, v2}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setBorderOptions(S)V

    .line 1616
    invoke-virtual {v0, v2}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setPaletteOptions(S)V

    .line 1617
    invoke-virtual {v0, v2}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setAdtlPaletteOptions(S)V

    .line 1618
    invoke-virtual {v0, v4}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setFillPaletteOptions(S)V

    goto/16 :goto_0

    .line 1622
    :pswitch_17
    invoke-virtual {v0, v2}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setFontIndex(S)V

    .line 1623
    const/16 v1, 0x31

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setFormatIndex(S)V

    .line 1624
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setCellOptions(S)V

    .line 1625
    invoke-virtual {v0, v3}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setAlignmentOptions(S)V

    .line 1626
    const/16 v1, 0x5c00

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setIndentionOptions(S)V

    .line 1627
    invoke-virtual {v0, v2}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setBorderOptions(S)V

    .line 1628
    invoke-virtual {v0, v2}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setPaletteOptions(S)V

    .line 1629
    invoke-virtual {v0, v2}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setAdtlPaletteOptions(S)V

    .line 1630
    invoke-virtual {v0, v4}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setFillPaletteOptions(S)V

    goto/16 :goto_0

    .line 1634
    :pswitch_18
    invoke-virtual {v0, v2}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setFontIndex(S)V

    .line 1635
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setFormatIndex(S)V

    .line 1636
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setCellOptions(S)V

    .line 1637
    invoke-virtual {v0, v3}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setAlignmentOptions(S)V

    .line 1638
    const/16 v1, 0x5c00

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setIndentionOptions(S)V

    .line 1639
    invoke-virtual {v0, v2}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setBorderOptions(S)V

    .line 1640
    invoke-virtual {v0, v2}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setPaletteOptions(S)V

    .line 1641
    invoke-virtual {v0, v2}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setAdtlPaletteOptions(S)V

    .line 1642
    invoke-virtual {v0, v4}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setFillPaletteOptions(S)V

    goto/16 :goto_0

    .line 1646
    :pswitch_19
    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setFontIndex(S)V

    .line 1647
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setFormatIndex(S)V

    .line 1648
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setCellOptions(S)V

    .line 1649
    invoke-virtual {v0, v3}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setAlignmentOptions(S)V

    .line 1650
    const/16 v1, 0x5c00

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setIndentionOptions(S)V

    .line 1651
    invoke-virtual {v0, v2}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setBorderOptions(S)V

    .line 1652
    invoke-virtual {v0, v2}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setPaletteOptions(S)V

    .line 1653
    invoke-virtual {v0, v2}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setAdtlPaletteOptions(S)V

    .line 1654
    invoke-virtual {v0, v4}, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;->setFillPaletteOptions(S)V

    goto/16 :goto_0

    .line 1340
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_19
    .end packed-switch
.end method

.method private static createExtendedSST()Lorg/apache/poi/hssf/record/ExtSSTRecord;
    .locals 2

    .prologue
    .line 1784
    new-instance v0, Lorg/apache/poi/hssf/record/ExtSSTRecord;

    invoke-direct {v0}, Lorg/apache/poi/hssf/record/ExtSSTRecord;-><init>()V

    .line 1785
    .local v0, "retval":Lorg/apache/poi/hssf/record/ExtSSTRecord;
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/record/ExtSSTRecord;->setNumStringsPerBucket(S)V

    .line 1786
    return-object v0
.end method

.method private static createFnGroupCount()Lorg/apache/poi/hssf/record/FnGroupCountRecord;
    .locals 2

    .prologue
    .line 1163
    new-instance v0, Lorg/apache/poi/hssf/record/FnGroupCountRecord;

    invoke-direct {v0}, Lorg/apache/poi/hssf/record/FnGroupCountRecord;-><init>()V

    .line 1165
    .local v0, "retval":Lorg/apache/poi/hssf/record/FnGroupCountRecord;
    const/16 v1, 0xe

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/record/FnGroupCountRecord;->setCount(S)V

    .line 1166
    return-object v0
.end method

.method private static createFont()Lorg/apache/poi/hssf/record/FontRecord;
    .locals 2

    .prologue
    .line 1299
    new-instance v0, Lorg/apache/poi/hssf/record/FontRecord;

    invoke-direct {v0}, Lorg/apache/poi/hssf/record/FontRecord;-><init>()V

    .line 1301
    .local v0, "retval":Lorg/apache/poi/hssf/record/FontRecord;
    const/16 v1, 0xc8

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/record/FontRecord;->setFontHeight(S)V

    .line 1302
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/record/FontRecord;->setAttributes(S)V

    .line 1303
    const/16 v1, 0x7fff

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/record/FontRecord;->setColorPaletteIndex(S)V

    .line 1304
    const/16 v1, 0x190

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/record/FontRecord;->setBoldWeight(S)V

    .line 1305
    const-string/jumbo v1, "Arial"

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/record/FontRecord;->setFontName(Ljava/lang/String;)V

    .line 1306
    return-object v0
.end method

.method private static createFormat(I)Lorg/apache/poi/hssf/record/FormatRecord;
    .locals 7
    .param p0, "id"    # I

    .prologue
    const/16 v6, 0x29

    const/16 v5, 0x8

    const/4 v4, 0x7

    const/4 v3, 0x6

    const/4 v2, 0x5

    .line 1319
    packed-switch p0, :pswitch_data_0

    .line 1329
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Unexpected id "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1320
    :pswitch_0
    new-instance v0, Lorg/apache/poi/hssf/record/FormatRecord;

    invoke-static {v2}, Lorg/apache/poi/ss/usermodel/BuiltinFormats;->getBuiltinFormat(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v2, v1}, Lorg/apache/poi/hssf/record/FormatRecord;-><init>(ILjava/lang/String;)V

    .line 1327
    :goto_0
    return-object v0

    .line 1321
    :pswitch_1
    new-instance v0, Lorg/apache/poi/hssf/record/FormatRecord;

    invoke-static {v3}, Lorg/apache/poi/ss/usermodel/BuiltinFormats;->getBuiltinFormat(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v3, v1}, Lorg/apache/poi/hssf/record/FormatRecord;-><init>(ILjava/lang/String;)V

    goto :goto_0

    .line 1322
    :pswitch_2
    new-instance v0, Lorg/apache/poi/hssf/record/FormatRecord;

    invoke-static {v4}, Lorg/apache/poi/ss/usermodel/BuiltinFormats;->getBuiltinFormat(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v4, v1}, Lorg/apache/poi/hssf/record/FormatRecord;-><init>(ILjava/lang/String;)V

    goto :goto_0

    .line 1323
    :pswitch_3
    new-instance v0, Lorg/apache/poi/hssf/record/FormatRecord;

    invoke-static {v5}, Lorg/apache/poi/ss/usermodel/BuiltinFormats;->getBuiltinFormat(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v5, v1}, Lorg/apache/poi/hssf/record/FormatRecord;-><init>(ILjava/lang/String;)V

    goto :goto_0

    .line 1324
    :pswitch_4
    new-instance v0, Lorg/apache/poi/hssf/record/FormatRecord;

    const/16 v1, 0x2a

    const/16 v2, 0x2a

    invoke-static {v2}, Lorg/apache/poi/ss/usermodel/BuiltinFormats;->getBuiltinFormat(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/hssf/record/FormatRecord;-><init>(ILjava/lang/String;)V

    goto :goto_0

    .line 1325
    :pswitch_5
    new-instance v0, Lorg/apache/poi/hssf/record/FormatRecord;

    invoke-static {v6}, Lorg/apache/poi/ss/usermodel/BuiltinFormats;->getBuiltinFormat(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v6, v1}, Lorg/apache/poi/hssf/record/FormatRecord;-><init>(ILjava/lang/String;)V

    goto :goto_0

    .line 1326
    :pswitch_6
    new-instance v0, Lorg/apache/poi/hssf/record/FormatRecord;

    const/16 v1, 0x2c

    const/16 v2, 0x2c

    invoke-static {v2}, Lorg/apache/poi/ss/usermodel/BuiltinFormats;->getBuiltinFormat(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/hssf/record/FormatRecord;-><init>(ILjava/lang/String;)V

    goto :goto_0

    .line 1327
    :pswitch_7
    new-instance v0, Lorg/apache/poi/hssf/record/FormatRecord;

    const/16 v1, 0x2b

    const/16 v2, 0x2b

    invoke-static {v2}, Lorg/apache/poi/ss/usermodel/BuiltinFormats;->getBuiltinFormat(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/hssf/record/FormatRecord;-><init>(ILjava/lang/String;)V

    goto :goto_0

    .line 1319
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method private static createHideObj()Lorg/apache/poi/hssf/record/HideObjRecord;
    .locals 2

    .prologue
    .line 1249
    new-instance v0, Lorg/apache/poi/hssf/record/HideObjRecord;

    invoke-direct {v0}, Lorg/apache/poi/hssf/record/HideObjRecord;-><init>()V

    .line 1250
    .local v0, "retval":Lorg/apache/poi/hssf/record/HideObjRecord;
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/record/HideObjRecord;->setHideObj(S)V

    .line 1251
    return-object v0
.end method

.method private static createMMS()Lorg/apache/poi/hssf/record/MMSRecord;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1113
    new-instance v0, Lorg/apache/poi/hssf/record/MMSRecord;

    invoke-direct {v0}, Lorg/apache/poi/hssf/record/MMSRecord;-><init>()V

    .line 1115
    .local v0, "retval":Lorg/apache/poi/hssf/record/MMSRecord;
    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/record/MMSRecord;->setAddMenuCount(B)V

    .line 1116
    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/record/MMSRecord;->setDelMenuCount(B)V

    .line 1117
    return-object v0
.end method

.method private static createPalette()Lorg/apache/poi/hssf/record/PaletteRecord;
    .locals 1

    .prologue
    .line 1736
    new-instance v0, Lorg/apache/poi/hssf/record/PaletteRecord;

    invoke-direct {v0}, Lorg/apache/poi/hssf/record/PaletteRecord;-><init>()V

    return-object v0
.end method

.method private static createPassword()Lorg/apache/poi/hssf/record/PasswordRecord;
    .locals 2

    .prologue
    .line 1191
    new-instance v0, Lorg/apache/poi/hssf/record/PasswordRecord;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lorg/apache/poi/hssf/record/PasswordRecord;-><init>(I)V

    return-object v0
.end method

.method private static createPasswordRev4()Lorg/apache/poi/hssf/record/PasswordRev4Record;
    .locals 2

    .prologue
    .line 1205
    new-instance v0, Lorg/apache/poi/hssf/record/PasswordRev4Record;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lorg/apache/poi/hssf/record/PasswordRev4Record;-><init>(I)V

    return-object v0
.end method

.method private static createPrecision()Lorg/apache/poi/hssf/record/PrecisionRecord;
    .locals 2

    .prologue
    .line 1268
    new-instance v0, Lorg/apache/poi/hssf/record/PrecisionRecord;

    invoke-direct {v0}, Lorg/apache/poi/hssf/record/PrecisionRecord;-><init>()V

    .line 1269
    .local v0, "retval":Lorg/apache/poi/hssf/record/PrecisionRecord;
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/record/PrecisionRecord;->setFullPrecision(Z)V

    .line 1270
    return-object v0
.end method

.method private static createProtect()Lorg/apache/poi/hssf/record/ProtectRecord;
    .locals 2

    .prologue
    .line 1184
    new-instance v0, Lorg/apache/poi/hssf/record/ProtectRecord;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lorg/apache/poi/hssf/record/ProtectRecord;-><init>(Z)V

    return-object v0
.end method

.method private static createProtectionRev4()Lorg/apache/poi/hssf/record/ProtectionRev4Record;
    .locals 2

    .prologue
    .line 1198
    new-instance v0, Lorg/apache/poi/hssf/record/ProtectionRev4Record;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lorg/apache/poi/hssf/record/ProtectionRev4Record;-><init>(Z)V

    return-object v0
.end method

.method private static createRefreshAll()Lorg/apache/poi/hssf/record/RefreshAllRecord;
    .locals 2

    .prologue
    .line 1277
    new-instance v0, Lorg/apache/poi/hssf/record/RefreshAllRecord;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lorg/apache/poi/hssf/record/RefreshAllRecord;-><init>(Z)V

    return-object v0
.end method

.method private static createStyle(I)Lorg/apache/poi/hssf/record/StyleRecord;
    .locals 3
    .param p0, "id"    # I

    .prologue
    const/4 v1, 0x0

    const/4 v2, -0x1

    .line 1689
    new-instance v0, Lorg/apache/poi/hssf/record/StyleRecord;

    invoke-direct {v0}, Lorg/apache/poi/hssf/record/StyleRecord;-><init>()V

    .line 1691
    .local v0, "retval":Lorg/apache/poi/hssf/record/StyleRecord;
    packed-switch p0, :pswitch_data_0

    .line 1729
    :goto_0
    return-object v0

    .line 1694
    :pswitch_0
    const/16 v1, 0x10

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/record/StyleRecord;->setXFIndex(I)V

    .line 1695
    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/record/StyleRecord;->setBuiltinStyle(I)V

    .line 1696
    invoke-virtual {v0, v2}, Lorg/apache/poi/hssf/record/StyleRecord;->setOutlineStyleLevel(I)V

    goto :goto_0

    .line 1700
    :pswitch_1
    const/16 v1, 0x11

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/record/StyleRecord;->setXFIndex(I)V

    .line 1701
    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/record/StyleRecord;->setBuiltinStyle(I)V

    .line 1702
    invoke-virtual {v0, v2}, Lorg/apache/poi/hssf/record/StyleRecord;->setOutlineStyleLevel(I)V

    goto :goto_0

    .line 1706
    :pswitch_2
    const/16 v1, 0x12

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/record/StyleRecord;->setXFIndex(I)V

    .line 1707
    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/record/StyleRecord;->setBuiltinStyle(I)V

    .line 1708
    invoke-virtual {v0, v2}, Lorg/apache/poi/hssf/record/StyleRecord;->setOutlineStyleLevel(I)V

    goto :goto_0

    .line 1712
    :pswitch_3
    const/16 v1, 0x13

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/record/StyleRecord;->setXFIndex(I)V

    .line 1713
    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/record/StyleRecord;->setBuiltinStyle(I)V

    .line 1714
    invoke-virtual {v0, v2}, Lorg/apache/poi/hssf/record/StyleRecord;->setOutlineStyleLevel(I)V

    goto :goto_0

    .line 1718
    :pswitch_4
    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/record/StyleRecord;->setXFIndex(I)V

    .line 1719
    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/record/StyleRecord;->setBuiltinStyle(I)V

    .line 1720
    invoke-virtual {v0, v2}, Lorg/apache/poi/hssf/record/StyleRecord;->setOutlineStyleLevel(I)V

    goto :goto_0

    .line 1724
    :pswitch_5
    const/16 v1, 0x14

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/record/StyleRecord;->setXFIndex(I)V

    .line 1725
    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/record/StyleRecord;->setBuiltinStyle(I)V

    .line 1726
    invoke-virtual {v0, v2}, Lorg/apache/poi/hssf/record/StyleRecord;->setOutlineStyleLevel(I)V

    goto :goto_0

    .line 1691
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private static createTabId()Lorg/apache/poi/hssf/record/TabIdRecord;
    .locals 1

    .prologue
    .line 1156
    new-instance v0, Lorg/apache/poi/hssf/record/TabIdRecord;

    invoke-direct {v0}, Lorg/apache/poi/hssf/record/TabIdRecord;-><init>()V

    return-object v0
.end method

.method private static createUseSelFS()Lorg/apache/poi/hssf/record/UseSelFSRecord;
    .locals 2

    .prologue
    .line 1743
    new-instance v0, Lorg/apache/poi/hssf/record/UseSelFSRecord;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lorg/apache/poi/hssf/record/UseSelFSRecord;-><init>(Z)V

    return-object v0
.end method

.method private static createWindowOne()Lorg/apache/poi/hssf/record/WindowOneRecord;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1221
    new-instance v0, Lorg/apache/poi/hssf/record/WindowOneRecord;

    invoke-direct {v0}, Lorg/apache/poi/hssf/record/WindowOneRecord;-><init>()V

    .line 1223
    .local v0, "retval":Lorg/apache/poi/hssf/record/WindowOneRecord;
    const/16 v1, 0x168

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/record/WindowOneRecord;->setHorizontalHold(S)V

    .line 1224
    const/16 v1, 0x10e

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/record/WindowOneRecord;->setVerticalHold(S)V

    .line 1225
    const/16 v1, 0x3a5c

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/record/WindowOneRecord;->setWidth(S)V

    .line 1226
    const/16 v1, 0x23be

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/record/WindowOneRecord;->setHeight(S)V

    .line 1227
    const/16 v1, 0x38

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/record/WindowOneRecord;->setOptions(S)V

    .line 1228
    invoke-virtual {v0, v2}, Lorg/apache/poi/hssf/record/WindowOneRecord;->setActiveSheetIndex(I)V

    .line 1229
    invoke-virtual {v0, v2}, Lorg/apache/poi/hssf/record/WindowOneRecord;->setFirstVisibleTab(I)V

    .line 1230
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/record/WindowOneRecord;->setNumSelectedTabs(S)V

    .line 1231
    const/16 v1, 0x258

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/record/WindowOneRecord;->setTabWidthRatio(S)V

    .line 1232
    return-object v0
.end method

.method private static createWindowProtect()Lorg/apache/poi/hssf/record/WindowProtectRecord;
    .locals 2

    .prologue
    .line 1175
    new-instance v0, Lorg/apache/poi/hssf/record/WindowProtectRecord;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lorg/apache/poi/hssf/record/WindowProtectRecord;-><init>(Z)V

    return-object v0
.end method

.method public static createWorkbook()Lorg/apache/poi/hssf/model/InternalWorkbook;
    .locals 11

    .prologue
    const/4 v10, 0x1

    .line 360
    sget-object v8, Lorg/apache/poi/hssf/model/InternalWorkbook;->log:Lorg/apache/poi/util/POILogger;

    invoke-virtual {v8, v10}, Lorg/apache/poi/util/POILogger;->check(I)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 361
    sget-object v8, Lorg/apache/poi/hssf/model/InternalWorkbook;->log:Lorg/apache/poi/util/POILogger;

    const-string/jumbo v9, "creating new workbook from scratch"

    invoke-virtual {v8, v10, v9}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;)V

    .line 362
    :cond_0
    new-instance v7, Lorg/apache/poi/hssf/model/InternalWorkbook;

    invoke-direct {v7}, Lorg/apache/poi/hssf/model/InternalWorkbook;-><init>()V

    .line 363
    .local v7, "retval":Lorg/apache/poi/hssf/model/InternalWorkbook;
    new-instance v6, Ljava/util/ArrayList;

    const/16 v8, 0x1e

    invoke-direct {v6, v8}, Ljava/util/ArrayList;-><init>(I)V

    .line 364
    .local v6, "records":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/hssf/record/Record;>;"
    iget-object v8, v7, Lorg/apache/poi/hssf/model/InternalWorkbook;->records:Lorg/apache/poi/hssf/model/WorkbookRecordList;

    invoke-virtual {v8, v6}, Lorg/apache/poi/hssf/model/WorkbookRecordList;->setRecords(Ljava/util/List;)V

    .line 365
    iget-object v1, v7, Lorg/apache/poi/hssf/model/InternalWorkbook;->formats:Ljava/util/List;

    .line 367
    .local v1, "formats":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/hssf/record/FormatRecord;>;"
    invoke-static {}, Lorg/apache/poi/hssf/model/InternalWorkbook;->createBOF()Lorg/apache/poi/hssf/record/BOFRecord;

    move-result-object v8

    invoke-interface {v6, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 368
    new-instance v8, Lorg/apache/poi/hssf/record/InterfaceHdrRecord;

    const/16 v9, 0x4b0

    invoke-direct {v8, v9}, Lorg/apache/poi/hssf/record/InterfaceHdrRecord;-><init>(I)V

    invoke-interface {v6, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 369
    invoke-static {}, Lorg/apache/poi/hssf/model/InternalWorkbook;->createMMS()Lorg/apache/poi/hssf/record/MMSRecord;

    move-result-object v8

    invoke-interface {v6, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 370
    sget-object v8, Lorg/apache/poi/hssf/record/InterfaceEndRecord;->instance:Lorg/apache/poi/hssf/record/InterfaceEndRecord;

    invoke-interface {v6, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 371
    invoke-static {}, Lorg/apache/poi/hssf/model/InternalWorkbook;->createWriteAccess()Lorg/apache/poi/hssf/record/WriteAccessRecord;

    move-result-object v8

    invoke-interface {v6, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 372
    invoke-static {}, Lorg/apache/poi/hssf/model/InternalWorkbook;->createCodepage()Lorg/apache/poi/hssf/record/CodepageRecord;

    move-result-object v8

    invoke-interface {v6, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 373
    invoke-static {}, Lorg/apache/poi/hssf/model/InternalWorkbook;->createDSF()Lorg/apache/poi/hssf/record/DSFRecord;

    move-result-object v8

    invoke-interface {v6, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 374
    invoke-static {}, Lorg/apache/poi/hssf/model/InternalWorkbook;->createTabId()Lorg/apache/poi/hssf/record/TabIdRecord;

    move-result-object v8

    invoke-interface {v6, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 375
    iget-object v8, v7, Lorg/apache/poi/hssf/model/InternalWorkbook;->records:Lorg/apache/poi/hssf/model/WorkbookRecordList;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v9

    add-int/lit8 v9, v9, -0x1

    invoke-virtual {v8, v9}, Lorg/apache/poi/hssf/model/WorkbookRecordList;->setTabpos(I)V

    .line 376
    invoke-static {}, Lorg/apache/poi/hssf/model/InternalWorkbook;->createFnGroupCount()Lorg/apache/poi/hssf/record/FnGroupCountRecord;

    move-result-object v8

    invoke-interface {v6, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 377
    invoke-static {}, Lorg/apache/poi/hssf/model/InternalWorkbook;->createWindowProtect()Lorg/apache/poi/hssf/record/WindowProtectRecord;

    move-result-object v8

    invoke-interface {v6, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 378
    invoke-static {}, Lorg/apache/poi/hssf/model/InternalWorkbook;->createProtect()Lorg/apache/poi/hssf/record/ProtectRecord;

    move-result-object v8

    invoke-interface {v6, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 379
    iget-object v8, v7, Lorg/apache/poi/hssf/model/InternalWorkbook;->records:Lorg/apache/poi/hssf/model/WorkbookRecordList;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v9

    add-int/lit8 v9, v9, -0x1

    invoke-virtual {v8, v9}, Lorg/apache/poi/hssf/model/WorkbookRecordList;->setProtpos(I)V

    .line 380
    invoke-static {}, Lorg/apache/poi/hssf/model/InternalWorkbook;->createPassword()Lorg/apache/poi/hssf/record/PasswordRecord;

    move-result-object v8

    invoke-interface {v6, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 381
    invoke-static {}, Lorg/apache/poi/hssf/model/InternalWorkbook;->createProtectionRev4()Lorg/apache/poi/hssf/record/ProtectionRev4Record;

    move-result-object v8

    invoke-interface {v6, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 382
    invoke-static {}, Lorg/apache/poi/hssf/model/InternalWorkbook;->createPasswordRev4()Lorg/apache/poi/hssf/record/PasswordRev4Record;

    move-result-object v8

    invoke-interface {v6, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 383
    invoke-static {}, Lorg/apache/poi/hssf/model/InternalWorkbook;->createWindowOne()Lorg/apache/poi/hssf/record/WindowOneRecord;

    move-result-object v8

    iput-object v8, v7, Lorg/apache/poi/hssf/model/InternalWorkbook;->windowOne:Lorg/apache/poi/hssf/record/WindowOneRecord;

    .line 384
    iget-object v8, v7, Lorg/apache/poi/hssf/model/InternalWorkbook;->windowOne:Lorg/apache/poi/hssf/record/WindowOneRecord;

    invoke-interface {v6, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 385
    invoke-static {}, Lorg/apache/poi/hssf/model/InternalWorkbook;->createBackup()Lorg/apache/poi/hssf/record/BackupRecord;

    move-result-object v8

    invoke-interface {v6, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 386
    iget-object v8, v7, Lorg/apache/poi/hssf/model/InternalWorkbook;->records:Lorg/apache/poi/hssf/model/WorkbookRecordList;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v9

    add-int/lit8 v9, v9, -0x1

    invoke-virtual {v8, v9}, Lorg/apache/poi/hssf/model/WorkbookRecordList;->setBackuppos(I)V

    .line 387
    invoke-static {}, Lorg/apache/poi/hssf/model/InternalWorkbook;->createHideObj()Lorg/apache/poi/hssf/record/HideObjRecord;

    move-result-object v8

    invoke-interface {v6, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 388
    invoke-static {}, Lorg/apache/poi/hssf/model/InternalWorkbook;->createDateWindow1904()Lorg/apache/poi/hssf/record/DateWindow1904Record;

    move-result-object v8

    invoke-interface {v6, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 389
    invoke-static {}, Lorg/apache/poi/hssf/model/InternalWorkbook;->createPrecision()Lorg/apache/poi/hssf/record/PrecisionRecord;

    move-result-object v8

    invoke-interface {v6, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 390
    invoke-static {}, Lorg/apache/poi/hssf/model/InternalWorkbook;->createRefreshAll()Lorg/apache/poi/hssf/record/RefreshAllRecord;

    move-result-object v8

    invoke-interface {v6, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 391
    invoke-static {}, Lorg/apache/poi/hssf/model/InternalWorkbook;->createBookBool()Lorg/apache/poi/hssf/record/BookBoolRecord;

    move-result-object v8

    invoke-interface {v6, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 392
    invoke-static {}, Lorg/apache/poi/hssf/model/InternalWorkbook;->createFont()Lorg/apache/poi/hssf/record/FontRecord;

    move-result-object v8

    invoke-interface {v6, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 393
    invoke-static {}, Lorg/apache/poi/hssf/model/InternalWorkbook;->createFont()Lorg/apache/poi/hssf/record/FontRecord;

    move-result-object v8

    invoke-interface {v6, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 394
    invoke-static {}, Lorg/apache/poi/hssf/model/InternalWorkbook;->createFont()Lorg/apache/poi/hssf/record/FontRecord;

    move-result-object v8

    invoke-interface {v6, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 395
    invoke-static {}, Lorg/apache/poi/hssf/model/InternalWorkbook;->createFont()Lorg/apache/poi/hssf/record/FontRecord;

    move-result-object v8

    invoke-interface {v6, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 396
    iget-object v8, v7, Lorg/apache/poi/hssf/model/InternalWorkbook;->records:Lorg/apache/poi/hssf/model/WorkbookRecordList;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v9

    add-int/lit8 v9, v9, -0x1

    invoke-virtual {v8, v9}, Lorg/apache/poi/hssf/model/WorkbookRecordList;->setFontpos(I)V

    .line 397
    const/4 v8, 0x4

    iput v8, v7, Lorg/apache/poi/hssf/model/InternalWorkbook;->numfonts:I

    .line 400
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    const/4 v8, 0x7

    if-le v2, v8, :cond_2

    .line 407
    const/4 v3, 0x0

    .local v3, "k":I
    :goto_1
    const/16 v8, 0x15

    if-lt v3, v8, :cond_4

    .line 411
    iget-object v8, v7, Lorg/apache/poi/hssf/model/InternalWorkbook;->records:Lorg/apache/poi/hssf/model/WorkbookRecordList;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v9

    add-int/lit8 v9, v9, -0x1

    invoke-virtual {v8, v9}, Lorg/apache/poi/hssf/model/WorkbookRecordList;->setXfpos(I)V

    .line 412
    const/4 v3, 0x0

    :goto_2
    const/4 v8, 0x6

    if-lt v3, v8, :cond_5

    .line 415
    invoke-static {}, Lorg/apache/poi/hssf/model/InternalWorkbook;->createUseSelFS()Lorg/apache/poi/hssf/record/UseSelFSRecord;

    move-result-object v8

    invoke-interface {v6, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 417
    const/4 v4, 0x1

    .line 418
    .local v4, "nBoundSheets":I
    const/4 v3, 0x0

    :goto_3
    if-lt v3, v4, :cond_6

    .line 425
    invoke-static {}, Lorg/apache/poi/hssf/model/InternalWorkbook;->createCountry()Lorg/apache/poi/hssf/record/CountryRecord;

    move-result-object v8

    invoke-interface {v6, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 426
    const/4 v3, 0x0

    :goto_4
    if-lt v3, v4, :cond_7

    .line 429
    new-instance v8, Lorg/apache/poi/hssf/record/SSTRecord;

    invoke-direct {v8}, Lorg/apache/poi/hssf/record/SSTRecord;-><init>()V

    iput-object v8, v7, Lorg/apache/poi/hssf/model/InternalWorkbook;->sst:Lorg/apache/poi/hssf/record/SSTRecord;

    .line 430
    iget-object v8, v7, Lorg/apache/poi/hssf/model/InternalWorkbook;->sst:Lorg/apache/poi/hssf/record/SSTRecord;

    invoke-interface {v6, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 431
    invoke-static {}, Lorg/apache/poi/hssf/model/InternalWorkbook;->createExtendedSST()Lorg/apache/poi/hssf/record/ExtSSTRecord;

    move-result-object v8

    invoke-interface {v6, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 433
    sget-object v8, Lorg/apache/poi/hssf/record/EOFRecord;->instance:Lorg/apache/poi/hssf/record/EOFRecord;

    invoke-interface {v6, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 434
    sget-object v8, Lorg/apache/poi/hssf/model/InternalWorkbook;->log:Lorg/apache/poi/util/POILogger;

    invoke-virtual {v8, v10}, Lorg/apache/poi/util/POILogger;->check(I)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 435
    sget-object v8, Lorg/apache/poi/hssf/model/InternalWorkbook;->log:Lorg/apache/poi/util/POILogger;

    const-string/jumbo v9, "exit create new workbook from scratch"

    invoke-virtual {v8, v10, v9}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;)V

    .line 436
    :cond_1
    return-object v7

    .line 401
    .end local v3    # "k":I
    .end local v4    # "nBoundSheets":I
    :cond_2
    invoke-static {v2}, Lorg/apache/poi/hssf/model/InternalWorkbook;->createFormat(I)Lorg/apache/poi/hssf/record/FormatRecord;

    move-result-object v5

    .line 402
    .local v5, "rec":Lorg/apache/poi/hssf/record/FormatRecord;
    iget v8, v7, Lorg/apache/poi/hssf/model/InternalWorkbook;->maxformatid:I

    invoke-virtual {v5}, Lorg/apache/poi/hssf/record/FormatRecord;->getIndexCode()I

    move-result v9

    if-lt v8, v9, :cond_3

    iget v8, v7, Lorg/apache/poi/hssf/model/InternalWorkbook;->maxformatid:I

    :goto_5
    iput v8, v7, Lorg/apache/poi/hssf/model/InternalWorkbook;->maxformatid:I

    .line 403
    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 404
    invoke-interface {v6, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 400
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 402
    :cond_3
    invoke-virtual {v5}, Lorg/apache/poi/hssf/record/FormatRecord;->getIndexCode()I

    move-result v8

    goto :goto_5

    .line 408
    .end local v5    # "rec":Lorg/apache/poi/hssf/record/FormatRecord;
    .restart local v3    # "k":I
    :cond_4
    invoke-static {v3}, Lorg/apache/poi/hssf/model/InternalWorkbook;->createExtendedFormat(I)Lorg/apache/poi/hssf/record/ExtendedFormatRecord;

    move-result-object v8

    invoke-interface {v6, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 409
    iget v8, v7, Lorg/apache/poi/hssf/model/InternalWorkbook;->numxfs:I

    add-int/lit8 v8, v8, 0x1

    iput v8, v7, Lorg/apache/poi/hssf/model/InternalWorkbook;->numxfs:I

    .line 407
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 413
    :cond_5
    invoke-static {v3}, Lorg/apache/poi/hssf/model/InternalWorkbook;->createStyle(I)Lorg/apache/poi/hssf/record/StyleRecord;

    move-result-object v8

    invoke-interface {v6, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 412
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 419
    .restart local v4    # "nBoundSheets":I
    :cond_6
    invoke-static {v3}, Lorg/apache/poi/hssf/model/InternalWorkbook;->createBoundSheet(I)Lorg/apache/poi/hssf/record/BoundSheetRecord;

    move-result-object v0

    .line 421
    .local v0, "bsr":Lorg/apache/poi/hssf/record/BoundSheetRecord;
    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 422
    iget-object v8, v7, Lorg/apache/poi/hssf/model/InternalWorkbook;->boundsheets:Ljava/util/List;

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 423
    iget-object v8, v7, Lorg/apache/poi/hssf/model/InternalWorkbook;->records:Lorg/apache/poi/hssf/model/WorkbookRecordList;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v9

    add-int/lit8 v9, v9, -0x1

    invoke-virtual {v8, v9}, Lorg/apache/poi/hssf/model/WorkbookRecordList;->setBspos(I)V

    .line 418
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_3

    .line 427
    .end local v0    # "bsr":Lorg/apache/poi/hssf/record/BoundSheetRecord;
    :cond_7
    invoke-direct {v7}, Lorg/apache/poi/hssf/model/InternalWorkbook;->getOrCreateLinkTable()Lorg/apache/poi/hssf/model/LinkTable;

    move-result-object v8

    invoke-virtual {v8, v3}, Lorg/apache/poi/hssf/model/LinkTable;->checkExternSheet(I)I

    .line 426
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_4
.end method

.method public static createWorkbook(Ljava/util/List;)Lorg/apache/poi/hssf/model/InternalWorkbook;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/apache/poi/hssf/record/Record;",
            ">;)",
            "Lorg/apache/poi/hssf/model/InternalWorkbook;"
        }
    .end annotation

    .prologue
    .local p0, "recs":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/hssf/record/Record;>;"
    const/4 v6, 0x1

    .line 210
    sget-object v5, Lorg/apache/poi/hssf/model/InternalWorkbook;->log:Lorg/apache/poi/util/POILogger;

    invoke-virtual {v5, v6}, Lorg/apache/poi/util/POILogger;->check(I)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 211
    sget-object v5, Lorg/apache/poi/hssf/model/InternalWorkbook;->log:Lorg/apache/poi/util/POILogger;

    const-string/jumbo v7, "Workbook (readfile) created with reclen="

    .line 212
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    .line 211
    invoke-virtual {v5, v6, v7, v8}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;Ljava/lang/Object;)V

    .line 213
    :cond_0
    new-instance v4, Lorg/apache/poi/hssf/model/InternalWorkbook;

    invoke-direct {v4}, Lorg/apache/poi/hssf/model/InternalWorkbook;-><init>()V

    .line 214
    .local v4, "retval":Lorg/apache/poi/hssf/model/InternalWorkbook;
    new-instance v3, Ljava/util/ArrayList;

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v5

    div-int/lit8 v5, v5, 0x3

    invoke-direct {v3, v5}, Ljava/util/ArrayList;-><init>(I)V

    .line 215
    .local v3, "records":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/hssf/record/Record;>;"
    iget-object v5, v4, Lorg/apache/poi/hssf/model/InternalWorkbook;->records:Lorg/apache/poi/hssf/model/WorkbookRecordList;

    invoke-virtual {v5, v3}, Lorg/apache/poi/hssf/model/WorkbookRecordList;->setRecords(Ljava/util/List;)V

    .line 218
    const/4 v0, 0x0

    .local v0, "k":I
    :goto_0
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v5

    if-lt v0, v5, :cond_4

    .line 337
    :cond_1
    :goto_1
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v5

    if-lt v0, v5, :cond_18

    .line 346
    iget-object v5, v4, Lorg/apache/poi/hssf/model/InternalWorkbook;->windowOne:Lorg/apache/poi/hssf/record/WindowOneRecord;

    if-nez v5, :cond_2

    .line 347
    invoke-static {}, Lorg/apache/poi/hssf/model/InternalWorkbook;->createWindowOne()Lorg/apache/poi/hssf/record/WindowOneRecord;

    move-result-object v5

    iput-object v5, v4, Lorg/apache/poi/hssf/model/InternalWorkbook;->windowOne:Lorg/apache/poi/hssf/record/WindowOneRecord;

    .line 349
    :cond_2
    sget-object v5, Lorg/apache/poi/hssf/model/InternalWorkbook;->log:Lorg/apache/poi/util/POILogger;

    invoke-virtual {v5, v6}, Lorg/apache/poi/util/POILogger;->check(I)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 350
    sget-object v5, Lorg/apache/poi/hssf/model/InternalWorkbook;->log:Lorg/apache/poi/util/POILogger;

    const-string/jumbo v7, "exit create workbook from existing file function"

    invoke-virtual {v5, v6, v7}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;)V

    .line 351
    :cond_3
    return-object v4

    .line 219
    :cond_4
    invoke-interface {p0, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/poi/hssf/record/Record;

    .line 221
    .local v2, "rec":Lorg/apache/poi/hssf/record/Record;
    invoke-virtual {v2}, Lorg/apache/poi/hssf/record/Record;->getSid()S

    move-result v5

    const/16 v7, 0xa

    if-ne v5, v7, :cond_5

    .line 222
    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 223
    sget-object v5, Lorg/apache/poi/hssf/model/InternalWorkbook;->log:Lorg/apache/poi/util/POILogger;

    invoke-virtual {v5, v6}, Lorg/apache/poi/util/POILogger;->check(I)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 224
    sget-object v5, Lorg/apache/poi/hssf/model/InternalWorkbook;->log:Lorg/apache/poi/util/POILogger;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string/jumbo v8, "found workbook eof record at "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;)V

    goto :goto_1

    .line 227
    :cond_5
    invoke-virtual {v2}, Lorg/apache/poi/hssf/record/Record;->getSid()S

    move-result v5

    sparse-switch v5, :sswitch_data_0

    .line 327
    :goto_2
    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 218
    :goto_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 230
    :sswitch_0
    sget-object v5, Lorg/apache/poi/hssf/model/InternalWorkbook;->log:Lorg/apache/poi/util/POILogger;

    invoke-virtual {v5, v6}, Lorg/apache/poi/util/POILogger;->check(I)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 231
    sget-object v5, Lorg/apache/poi/hssf/model/InternalWorkbook;->log:Lorg/apache/poi/util/POILogger;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string/jumbo v8, "found boundsheet record at "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;)V

    .line 232
    :cond_6
    iget-object v7, v4, Lorg/apache/poi/hssf/model/InternalWorkbook;->boundsheets:Ljava/util/List;

    move-object v5, v2

    check-cast v5, Lorg/apache/poi/hssf/record/BoundSheetRecord;

    invoke-interface {v7, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 233
    iget-object v5, v4, Lorg/apache/poi/hssf/model/InternalWorkbook;->records:Lorg/apache/poi/hssf/model/WorkbookRecordList;

    invoke-virtual {v5, v0}, Lorg/apache/poi/hssf/model/WorkbookRecordList;->setBspos(I)V

    goto :goto_2

    .line 237
    :sswitch_1
    sget-object v5, Lorg/apache/poi/hssf/model/InternalWorkbook;->log:Lorg/apache/poi/util/POILogger;

    invoke-virtual {v5, v6}, Lorg/apache/poi/util/POILogger;->check(I)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 238
    sget-object v5, Lorg/apache/poi/hssf/model/InternalWorkbook;->log:Lorg/apache/poi/util/POILogger;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string/jumbo v8, "found sst record at "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;)V

    :cond_7
    move-object v5, v2

    .line 239
    check-cast v5, Lorg/apache/poi/hssf/record/SSTRecord;

    iput-object v5, v4, Lorg/apache/poi/hssf/model/InternalWorkbook;->sst:Lorg/apache/poi/hssf/record/SSTRecord;

    goto :goto_2

    .line 243
    :sswitch_2
    sget-object v5, Lorg/apache/poi/hssf/model/InternalWorkbook;->log:Lorg/apache/poi/util/POILogger;

    invoke-virtual {v5, v6}, Lorg/apache/poi/util/POILogger;->check(I)Z

    move-result v5

    if-eqz v5, :cond_8

    .line 244
    sget-object v5, Lorg/apache/poi/hssf/model/InternalWorkbook;->log:Lorg/apache/poi/util/POILogger;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string/jumbo v8, "found font record at "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;)V

    .line 245
    :cond_8
    iget-object v5, v4, Lorg/apache/poi/hssf/model/InternalWorkbook;->records:Lorg/apache/poi/hssf/model/WorkbookRecordList;

    invoke-virtual {v5, v0}, Lorg/apache/poi/hssf/model/WorkbookRecordList;->setFontpos(I)V

    .line 246
    iget v5, v4, Lorg/apache/poi/hssf/model/InternalWorkbook;->numfonts:I

    add-int/lit8 v5, v5, 0x1

    iput v5, v4, Lorg/apache/poi/hssf/model/InternalWorkbook;->numfonts:I

    goto :goto_2

    .line 250
    :sswitch_3
    sget-object v5, Lorg/apache/poi/hssf/model/InternalWorkbook;->log:Lorg/apache/poi/util/POILogger;

    invoke-virtual {v5, v6}, Lorg/apache/poi/util/POILogger;->check(I)Z

    move-result v5

    if-eqz v5, :cond_9

    .line 251
    sget-object v5, Lorg/apache/poi/hssf/model/InternalWorkbook;->log:Lorg/apache/poi/util/POILogger;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string/jumbo v8, "found XF record at "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;)V

    .line 252
    :cond_9
    iget-object v5, v4, Lorg/apache/poi/hssf/model/InternalWorkbook;->records:Lorg/apache/poi/hssf/model/WorkbookRecordList;

    invoke-virtual {v5, v0}, Lorg/apache/poi/hssf/model/WorkbookRecordList;->setXfpos(I)V

    .line 253
    iget v5, v4, Lorg/apache/poi/hssf/model/InternalWorkbook;->numxfs:I

    add-int/lit8 v5, v5, 0x1

    iput v5, v4, Lorg/apache/poi/hssf/model/InternalWorkbook;->numxfs:I

    goto/16 :goto_2

    .line 257
    :sswitch_4
    sget-object v5, Lorg/apache/poi/hssf/model/InternalWorkbook;->log:Lorg/apache/poi/util/POILogger;

    invoke-virtual {v5, v6}, Lorg/apache/poi/util/POILogger;->check(I)Z

    move-result v5

    if-eqz v5, :cond_a

    .line 258
    sget-object v5, Lorg/apache/poi/hssf/model/InternalWorkbook;->log:Lorg/apache/poi/util/POILogger;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string/jumbo v8, "found tabid record at "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;)V

    .line 259
    :cond_a
    iget-object v5, v4, Lorg/apache/poi/hssf/model/InternalWorkbook;->records:Lorg/apache/poi/hssf/model/WorkbookRecordList;

    invoke-virtual {v5, v0}, Lorg/apache/poi/hssf/model/WorkbookRecordList;->setTabpos(I)V

    goto/16 :goto_2

    .line 263
    :sswitch_5
    sget-object v5, Lorg/apache/poi/hssf/model/InternalWorkbook;->log:Lorg/apache/poi/util/POILogger;

    invoke-virtual {v5, v6}, Lorg/apache/poi/util/POILogger;->check(I)Z

    move-result v5

    if-eqz v5, :cond_b

    .line 264
    sget-object v5, Lorg/apache/poi/hssf/model/InternalWorkbook;->log:Lorg/apache/poi/util/POILogger;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string/jumbo v8, "found protect record at "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;)V

    .line 265
    :cond_b
    iget-object v5, v4, Lorg/apache/poi/hssf/model/InternalWorkbook;->records:Lorg/apache/poi/hssf/model/WorkbookRecordList;

    invoke-virtual {v5, v0}, Lorg/apache/poi/hssf/model/WorkbookRecordList;->setProtpos(I)V

    goto/16 :goto_2

    .line 269
    :sswitch_6
    sget-object v5, Lorg/apache/poi/hssf/model/InternalWorkbook;->log:Lorg/apache/poi/util/POILogger;

    invoke-virtual {v5, v6}, Lorg/apache/poi/util/POILogger;->check(I)Z

    move-result v5

    if-eqz v5, :cond_c

    .line 270
    sget-object v5, Lorg/apache/poi/hssf/model/InternalWorkbook;->log:Lorg/apache/poi/util/POILogger;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string/jumbo v8, "found backup record at "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;)V

    .line 271
    :cond_c
    iget-object v5, v4, Lorg/apache/poi/hssf/model/InternalWorkbook;->records:Lorg/apache/poi/hssf/model/WorkbookRecordList;

    invoke-virtual {v5, v0}, Lorg/apache/poi/hssf/model/WorkbookRecordList;->setBackuppos(I)V

    goto/16 :goto_2

    .line 274
    :sswitch_7
    new-instance v5, Ljava/lang/RuntimeException;

    const-string/jumbo v6, "Extern sheet is part of LinkTable"

    invoke-direct {v5, v6}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 278
    :sswitch_8
    sget-object v5, Lorg/apache/poi/hssf/model/InternalWorkbook;->log:Lorg/apache/poi/util/POILogger;

    invoke-virtual {v5, v6}, Lorg/apache/poi/util/POILogger;->check(I)Z

    move-result v5

    if-eqz v5, :cond_d

    .line 279
    sget-object v5, Lorg/apache/poi/hssf/model/InternalWorkbook;->log:Lorg/apache/poi/util/POILogger;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string/jumbo v8, "found SupBook record at "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;)V

    .line 280
    :cond_d
    new-instance v5, Lorg/apache/poi/hssf/model/LinkTable;

    iget-object v7, v4, Lorg/apache/poi/hssf/model/InternalWorkbook;->records:Lorg/apache/poi/hssf/model/WorkbookRecordList;

    iget-object v8, v4, Lorg/apache/poi/hssf/model/InternalWorkbook;->commentRecords:Ljava/util/Map;

    invoke-direct {v5, p0, v0, v7, v8}, Lorg/apache/poi/hssf/model/LinkTable;-><init>(Ljava/util/List;ILorg/apache/poi/hssf/model/WorkbookRecordList;Ljava/util/Map;)V

    iput-object v5, v4, Lorg/apache/poi/hssf/model/InternalWorkbook;->linkTable:Lorg/apache/poi/hssf/model/LinkTable;

    .line 281
    iget-object v5, v4, Lorg/apache/poi/hssf/model/InternalWorkbook;->linkTable:Lorg/apache/poi/hssf/model/LinkTable;

    invoke-virtual {v5}, Lorg/apache/poi/hssf/model/LinkTable;->getRecordCount()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    add-int/2addr v0, v5

    .line 282
    goto/16 :goto_3

    .line 284
    :sswitch_9
    sget-object v5, Lorg/apache/poi/hssf/model/InternalWorkbook;->log:Lorg/apache/poi/util/POILogger;

    invoke-virtual {v5, v6}, Lorg/apache/poi/util/POILogger;->check(I)Z

    move-result v5

    if-eqz v5, :cond_e

    .line 285
    sget-object v5, Lorg/apache/poi/hssf/model/InternalWorkbook;->log:Lorg/apache/poi/util/POILogger;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string/jumbo v8, "found format record at "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;)V

    .line 286
    :cond_e
    iget-object v7, v4, Lorg/apache/poi/hssf/model/InternalWorkbook;->formats:Ljava/util/List;

    move-object v5, v2

    check-cast v5, Lorg/apache/poi/hssf/record/FormatRecord;

    invoke-interface {v7, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 287
    iget v7, v4, Lorg/apache/poi/hssf/model/InternalWorkbook;->maxformatid:I

    move-object v5, v2

    check-cast v5, Lorg/apache/poi/hssf/record/FormatRecord;

    invoke-virtual {v5}, Lorg/apache/poi/hssf/record/FormatRecord;->getIndexCode()I

    move-result v5

    if-lt v7, v5, :cond_f

    iget v5, v4, Lorg/apache/poi/hssf/model/InternalWorkbook;->maxformatid:I

    :goto_4
    iput v5, v4, Lorg/apache/poi/hssf/model/InternalWorkbook;->maxformatid:I

    goto/16 :goto_2

    :cond_f
    move-object v5, v2

    check-cast v5, Lorg/apache/poi/hssf/record/FormatRecord;

    invoke-virtual {v5}, Lorg/apache/poi/hssf/record/FormatRecord;->getIndexCode()I

    move-result v5

    goto :goto_4

    .line 290
    :sswitch_a
    sget-object v5, Lorg/apache/poi/hssf/model/InternalWorkbook;->log:Lorg/apache/poi/util/POILogger;

    invoke-virtual {v5, v6}, Lorg/apache/poi/util/POILogger;->check(I)Z

    move-result v5

    if-eqz v5, :cond_10

    .line 291
    sget-object v5, Lorg/apache/poi/hssf/model/InternalWorkbook;->log:Lorg/apache/poi/util/POILogger;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string/jumbo v8, "found datewindow1904 record at "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;)V

    :cond_10
    move-object v5, v2

    .line 292
    check-cast v5, Lorg/apache/poi/hssf/record/DateWindow1904Record;

    invoke-virtual {v5}, Lorg/apache/poi/hssf/record/DateWindow1904Record;->getWindowing()S

    move-result v5

    if-ne v5, v6, :cond_11

    move v5, v6

    :goto_5
    iput-boolean v5, v4, Lorg/apache/poi/hssf/model/InternalWorkbook;->uses1904datewindowing:Z

    goto/16 :goto_2

    :cond_11
    const/4 v5, 0x0

    goto :goto_5

    .line 295
    :sswitch_b
    sget-object v5, Lorg/apache/poi/hssf/model/InternalWorkbook;->log:Lorg/apache/poi/util/POILogger;

    invoke-virtual {v5, v6}, Lorg/apache/poi/util/POILogger;->check(I)Z

    move-result v5

    if-eqz v5, :cond_12

    .line 296
    sget-object v5, Lorg/apache/poi/hssf/model/InternalWorkbook;->log:Lorg/apache/poi/util/POILogger;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string/jumbo v8, "found palette record at "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;)V

    .line 297
    :cond_12
    iget-object v5, v4, Lorg/apache/poi/hssf/model/InternalWorkbook;->records:Lorg/apache/poi/hssf/model/WorkbookRecordList;

    invoke-virtual {v5, v0}, Lorg/apache/poi/hssf/model/WorkbookRecordList;->setPalettepos(I)V

    goto/16 :goto_2

    .line 300
    :sswitch_c
    sget-object v5, Lorg/apache/poi/hssf/model/InternalWorkbook;->log:Lorg/apache/poi/util/POILogger;

    invoke-virtual {v5, v6}, Lorg/apache/poi/util/POILogger;->check(I)Z

    move-result v5

    if-eqz v5, :cond_13

    .line 301
    sget-object v5, Lorg/apache/poi/hssf/model/InternalWorkbook;->log:Lorg/apache/poi/util/POILogger;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string/jumbo v8, "found WindowOneRecord at "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;)V

    :cond_13
    move-object v5, v2

    .line 302
    check-cast v5, Lorg/apache/poi/hssf/record/WindowOneRecord;

    iput-object v5, v4, Lorg/apache/poi/hssf/model/InternalWorkbook;->windowOne:Lorg/apache/poi/hssf/record/WindowOneRecord;

    goto/16 :goto_2

    .line 305
    :sswitch_d
    sget-object v5, Lorg/apache/poi/hssf/model/InternalWorkbook;->log:Lorg/apache/poi/util/POILogger;

    invoke-virtual {v5, v6}, Lorg/apache/poi/util/POILogger;->check(I)Z

    move-result v5

    if-eqz v5, :cond_14

    .line 306
    sget-object v5, Lorg/apache/poi/hssf/model/InternalWorkbook;->log:Lorg/apache/poi/util/POILogger;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string/jumbo v8, "found WriteAccess at "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;)V

    :cond_14
    move-object v5, v2

    .line 307
    check-cast v5, Lorg/apache/poi/hssf/record/WriteAccessRecord;

    iput-object v5, v4, Lorg/apache/poi/hssf/model/InternalWorkbook;->writeAccess:Lorg/apache/poi/hssf/record/WriteAccessRecord;

    goto/16 :goto_2

    .line 310
    :sswitch_e
    sget-object v5, Lorg/apache/poi/hssf/model/InternalWorkbook;->log:Lorg/apache/poi/util/POILogger;

    invoke-virtual {v5, v6}, Lorg/apache/poi/util/POILogger;->check(I)Z

    move-result v5

    if-eqz v5, :cond_15

    .line 311
    sget-object v5, Lorg/apache/poi/hssf/model/InternalWorkbook;->log:Lorg/apache/poi/util/POILogger;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string/jumbo v8, "found WriteProtect at "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;)V

    :cond_15
    move-object v5, v2

    .line 312
    check-cast v5, Lorg/apache/poi/hssf/record/WriteProtectRecord;

    iput-object v5, v4, Lorg/apache/poi/hssf/model/InternalWorkbook;->writeProtect:Lorg/apache/poi/hssf/record/WriteProtectRecord;

    goto/16 :goto_2

    .line 315
    :sswitch_f
    sget-object v5, Lorg/apache/poi/hssf/model/InternalWorkbook;->log:Lorg/apache/poi/util/POILogger;

    invoke-virtual {v5, v6}, Lorg/apache/poi/util/POILogger;->check(I)Z

    move-result v5

    if-eqz v5, :cond_16

    .line 316
    sget-object v5, Lorg/apache/poi/hssf/model/InternalWorkbook;->log:Lorg/apache/poi/util/POILogger;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string/jumbo v8, "found FileSharing at "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;)V

    :cond_16
    move-object v5, v2

    .line 317
    check-cast v5, Lorg/apache/poi/hssf/record/FileSharingRecord;

    iput-object v5, v4, Lorg/apache/poi/hssf/model/InternalWorkbook;->fileShare:Lorg/apache/poi/hssf/record/FileSharingRecord;

    goto/16 :goto_2

    :sswitch_10
    move-object v1, v2

    .line 321
    check-cast v1, Lorg/apache/poi/hssf/record/NameCommentRecord;

    .line 322
    .local v1, "ncr":Lorg/apache/poi/hssf/record/NameCommentRecord;
    sget-object v5, Lorg/apache/poi/hssf/model/InternalWorkbook;->log:Lorg/apache/poi/util/POILogger;

    invoke-virtual {v5, v6}, Lorg/apache/poi/util/POILogger;->check(I)Z

    move-result v5

    if-eqz v5, :cond_17

    .line 323
    sget-object v5, Lorg/apache/poi/hssf/model/InternalWorkbook;->log:Lorg/apache/poi/util/POILogger;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string/jumbo v8, "found NameComment at "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;)V

    .line 324
    :cond_17
    iget-object v5, v4, Lorg/apache/poi/hssf/model/InternalWorkbook;->commentRecords:Ljava/util/Map;

    invoke-virtual {v1}, Lorg/apache/poi/hssf/record/NameCommentRecord;->getNameText()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v5, v7, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_2

    .line 338
    .end local v1    # "ncr":Lorg/apache/poi/hssf/record/NameCommentRecord;
    .end local v2    # "rec":Lorg/apache/poi/hssf/record/Record;
    :cond_18
    invoke-interface {p0, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/poi/hssf/record/Record;

    .line 339
    .restart local v2    # "rec":Lorg/apache/poi/hssf/record/Record;
    invoke-virtual {v2}, Lorg/apache/poi/hssf/record/Record;->getSid()S

    move-result v5

    packed-switch v5, :pswitch_data_0

    .line 337
    .end local v2    # "rec":Lorg/apache/poi/hssf/record/Record;
    :goto_6
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_1

    .line 341
    .restart local v2    # "rec":Lorg/apache/poi/hssf/record/Record;
    :pswitch_0
    iget-object v5, v4, Lorg/apache/poi/hssf/model/InternalWorkbook;->hyperlinks:Ljava/util/List;

    check-cast v2, Lorg/apache/poi/hssf/record/HyperlinkRecord;

    .end local v2    # "rec":Lorg/apache/poi/hssf/record/Record;
    invoke-interface {v5, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_6

    .line 227
    :sswitch_data_0
    .sparse-switch
        0x12 -> :sswitch_5
        0x17 -> :sswitch_7
        0x18 -> :sswitch_8
        0x22 -> :sswitch_a
        0x31 -> :sswitch_2
        0x3d -> :sswitch_c
        0x40 -> :sswitch_6
        0x5b -> :sswitch_f
        0x5c -> :sswitch_d
        0x85 -> :sswitch_0
        0x86 -> :sswitch_e
        0x92 -> :sswitch_b
        0xe0 -> :sswitch_3
        0xfc -> :sswitch_1
        0x13d -> :sswitch_4
        0x1ae -> :sswitch_8
        0x41e -> :sswitch_9
        0x894 -> :sswitch_10
    .end sparse-switch

    .line 339
    :pswitch_data_0
    .packed-switch 0x1b8
        :pswitch_0
    .end packed-switch
.end method

.method private static createWriteAccess()Lorg/apache/poi/hssf/record/WriteAccessRecord;
    .locals 5

    .prologue
    .line 1124
    new-instance v2, Lorg/apache/poi/hssf/record/WriteAccessRecord;

    invoke-direct {v2}, Lorg/apache/poi/hssf/record/WriteAccessRecord;-><init>()V

    .line 1126
    .local v2, "retval":Lorg/apache/poi/hssf/record/WriteAccessRecord;
    const-string/jumbo v0, "POI"

    .line 1128
    .local v0, "defaultUserName":Ljava/lang/String;
    :try_start_0
    const-string/jumbo v4, "user.name"

    invoke-static {v4}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1130
    .local v3, "username":Ljava/lang/String;
    if-nez v3, :cond_0

    move-object v3, v0

    .line 1132
    :cond_0
    invoke-virtual {v2, v3}, Lorg/apache/poi/hssf/record/WriteAccessRecord;->setUsername(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/security/AccessControlException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1138
    .end local v3    # "username":Ljava/lang/String;
    :goto_0
    return-object v2

    .line 1133
    :catch_0
    move-exception v1

    .line 1136
    .local v1, "e":Ljava/security/AccessControlException;
    invoke-virtual {v2, v0}, Lorg/apache/poi/hssf/record/WriteAccessRecord;->setUsername(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private fixTabIdRecord()I
    .locals 6

    .prologue
    .line 784
    iget-object v4, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->records:Lorg/apache/poi/hssf/model/WorkbookRecordList;

    iget-object v5, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->records:Lorg/apache/poi/hssf/model/WorkbookRecordList;

    invoke-virtual {v5}, Lorg/apache/poi/hssf/model/WorkbookRecordList;->getTabpos()I

    move-result v5

    invoke-virtual {v4, v5}, Lorg/apache/poi/hssf/model/WorkbookRecordList;->get(I)Lorg/apache/poi/hssf/record/Record;

    move-result-object v3

    check-cast v3, Lorg/apache/poi/hssf/record/TabIdRecord;

    .line 785
    .local v3, "tir":Lorg/apache/poi/hssf/record/TabIdRecord;
    invoke-virtual {v3}, Lorg/apache/poi/hssf/record/TabIdRecord;->getRecordSize()I

    move-result v1

    .line 786
    .local v1, "sz":I
    iget-object v4, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->boundsheets:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    new-array v2, v4, [S

    .line 788
    .local v2, "tia":[S
    const/4 v0, 0x0

    .local v0, "k":S
    :goto_0
    array-length v4, v2

    if-lt v0, v4, :cond_0

    .line 791
    invoke-virtual {v3, v2}, Lorg/apache/poi/hssf/record/TabIdRecord;->setTabIdArray([S)V

    .line 792
    invoke-virtual {v3}, Lorg/apache/poi/hssf/record/TabIdRecord;->getRecordSize()I

    move-result v4

    sub-int/2addr v4, v1

    return v4

    .line 789
    :cond_0
    aput-short v0, v2, v0

    .line 788
    add-int/lit8 v4, v0, 0x1

    int-to-short v0, v4

    goto :goto_0
.end method

.method private getBoundSheetRec(I)Lorg/apache/poi/hssf/record/BoundSheetRecord;
    .locals 1
    .param p1, "sheetIndex"    # I

    .prologue
    .line 564
    iget-object v0, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->boundsheets:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hssf/record/BoundSheetRecord;

    return-object v0
.end method

.method private getOrCreateLinkTable()Lorg/apache/poi/hssf/model/LinkTable;
    .locals 3

    .prologue
    .line 1794
    iget-object v0, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->linkTable:Lorg/apache/poi/hssf/model/LinkTable;

    if-nez v0, :cond_0

    .line 1795
    new-instance v0, Lorg/apache/poi/hssf/model/LinkTable;

    invoke-virtual {p0}, Lorg/apache/poi/hssf/model/InternalWorkbook;->getNumSheets()I

    move-result v1

    int-to-short v1, v1

    iget-object v2, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->records:Lorg/apache/poi/hssf/model/WorkbookRecordList;

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/hssf/model/LinkTable;-><init>(ILorg/apache/poi/hssf/model/WorkbookRecordList;)V

    iput-object v0, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->linkTable:Lorg/apache/poi/hssf/model/LinkTable;

    .line 1797
    :cond_0
    iget-object v0, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->linkTable:Lorg/apache/poi/hssf/model/LinkTable;

    return-object v0
.end method


# virtual methods
.method public addBSERecord(Lorg/apache/poi/ddf/EscherBSERecord;)I
    .locals 8
    .param p1, "e"    # Lorg/apache/poi/ddf/EscherBSERecord;

    .prologue
    const/16 v7, -0xfff

    const/4 v6, 0x1

    .line 2234
    invoke-virtual {p0}, Lorg/apache/poi/hssf/model/InternalWorkbook;->createDrawingGroup()V

    .line 2237
    iget-object v5, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->escherBSERecords:Ljava/util/List;

    invoke-interface {v5, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2239
    const/16 v5, 0xeb

    invoke-virtual {p0, v5}, Lorg/apache/poi/hssf/model/InternalWorkbook;->findFirstRecordLocBySid(S)I

    move-result v2

    .line 2240
    .local v2, "dgLoc":I
    invoke-virtual {p0}, Lorg/apache/poi/hssf/model/InternalWorkbook;->getRecords()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/poi/hssf/record/DrawingGroupRecord;

    .line 2242
    .local v4, "drawingGroup":Lorg/apache/poi/hssf/record/DrawingGroupRecord;
    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lorg/apache/poi/hssf/record/DrawingGroupRecord;->getEscherRecord(I)Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v3

    check-cast v3, Lorg/apache/poi/ddf/EscherContainerRecord;

    .line 2244
    .local v3, "dggContainer":Lorg/apache/poi/ddf/EscherContainerRecord;
    invoke-virtual {v3, v6}, Lorg/apache/poi/ddf/EscherContainerRecord;->getChild(I)Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v5

    invoke-virtual {v5}, Lorg/apache/poi/ddf/EscherRecord;->getRecordId()S

    move-result v5

    if-ne v5, v7, :cond_0

    .line 2246
    invoke-virtual {v3, v6}, Lorg/apache/poi/ddf/EscherContainerRecord;->getChild(I)Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/ddf/EscherContainerRecord;

    .line 2256
    .local v0, "bstoreContainer":Lorg/apache/poi/ddf/EscherContainerRecord;
    :goto_0
    iget-object v5, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->escherBSERecords:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    shl-int/lit8 v5, v5, 0x4

    or-int/lit8 v5, v5, 0xf

    int-to-short v5, v5

    invoke-virtual {v0, v5}, Lorg/apache/poi/ddf/EscherContainerRecord;->setOptions(S)V

    .line 2258
    invoke-virtual {v0, p1}, Lorg/apache/poi/ddf/EscherContainerRecord;->addChildRecord(Lorg/apache/poi/ddf/EscherRecord;)V

    .line 2260
    iget-object v5, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->escherBSERecords:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    return v5

    .line 2250
    .end local v0    # "bstoreContainer":Lorg/apache/poi/ddf/EscherContainerRecord;
    :cond_0
    new-instance v0, Lorg/apache/poi/ddf/EscherContainerRecord;

    invoke-direct {v0}, Lorg/apache/poi/ddf/EscherContainerRecord;-><init>()V

    .line 2251
    .restart local v0    # "bstoreContainer":Lorg/apache/poi/ddf/EscherContainerRecord;
    invoke-virtual {v0, v7}, Lorg/apache/poi/ddf/EscherContainerRecord;->setRecordId(S)V

    .line 2252
    invoke-virtual {v3}, Lorg/apache/poi/ddf/EscherContainerRecord;->getChildRecords()Ljava/util/List;

    move-result-object v1

    .line 2253
    .local v1, "childRecords":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/ddf/EscherRecord;>;"
    invoke-interface {v1, v6, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 2254
    invoke-virtual {v3, v1}, Lorg/apache/poi/ddf/EscherContainerRecord;->setChildRecords(Ljava/util/List;)V

    goto :goto_0
.end method

.method public addName(Lorg/apache/poi/hssf/record/NameRecord;)Lorg/apache/poi/hssf/record/NameRecord;
    .locals 1
    .param p1, "name"    # Lorg/apache/poi/hssf/record/NameRecord;

    .prologue
    .line 1898
    invoke-direct {p0}, Lorg/apache/poi/hssf/model/InternalWorkbook;->getOrCreateLinkTable()Lorg/apache/poi/hssf/model/LinkTable;

    move-result-object v0

    .line 1899
    .local v0, "linkTable":Lorg/apache/poi/hssf/model/LinkTable;
    invoke-virtual {v0, p1}, Lorg/apache/poi/hssf/model/LinkTable;->addName(Lorg/apache/poi/hssf/record/NameRecord;)V

    .line 1901
    return-object p1
.end method

.method public addSSTString(Lorg/apache/poi/hssf/record/common/UnicodeString;)I
    .locals 3
    .param p1, "string"    # Lorg/apache/poi/hssf/record/common/UnicodeString;

    .prologue
    const/4 v2, 0x1

    .line 943
    sget-object v0, Lorg/apache/poi/hssf/model/InternalWorkbook;->log:Lorg/apache/poi/util/POILogger;

    invoke-virtual {v0, v2}, Lorg/apache/poi/util/POILogger;->check(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 944
    sget-object v0, Lorg/apache/poi/hssf/model/InternalWorkbook;->log:Lorg/apache/poi/util/POILogger;

    const-string/jumbo v1, "insert to sst string=\'"

    invoke-virtual {v0, v2, v1, p1}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;Ljava/lang/Object;)V

    .line 945
    :cond_0
    iget-object v0, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->sst:Lorg/apache/poi/hssf/record/SSTRecord;

    if-nez v0, :cond_1

    .line 946
    invoke-virtual {p0}, Lorg/apache/poi/hssf/model/InternalWorkbook;->insertSST()V

    .line 948
    :cond_1
    iget-object v0, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->sst:Lorg/apache/poi/hssf/record/SSTRecord;

    invoke-virtual {v0, p1}, Lorg/apache/poi/hssf/record/SSTRecord;->addString(Lorg/apache/poi/hssf/record/common/UnicodeString;)I

    move-result v0

    return v0
.end method

.method public changeExternalReference(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1
    .param p1, "oldUrl"    # Ljava/lang/String;
    .param p2, "newUrl"    # Ljava/lang/String;

    .prologue
    .line 2507
    iget-object v0, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->linkTable:Lorg/apache/poi/hssf/model/LinkTable;

    invoke-virtual {v0, p1, p2}, Lorg/apache/poi/hssf/model/LinkTable;->changeExternalReference(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public checkExternSheet(I)S
    .locals 1
    .param p1, "sheetNumber"    # I

    .prologue
    .line 1850
    invoke-direct {p0}, Lorg/apache/poi/hssf/model/InternalWorkbook;->getOrCreateLinkTable()Lorg/apache/poi/hssf/model/LinkTable;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/poi/hssf/model/LinkTable;->checkExternSheet(I)I

    move-result v0

    int-to-short v0, v0

    return v0
.end method

.method public cloneDrawings(Lorg/apache/poi/hssf/model/InternalSheet;)V
    .locals 25
    .param p1, "sheet"    # Lorg/apache/poi/hssf/model/InternalSheet;

    .prologue
    .line 2381
    invoke-virtual/range {p0 .. p0}, Lorg/apache/poi/hssf/model/InternalWorkbook;->findDrawingGroup()Lorg/apache/poi/hssf/model/DrawingManager2;

    .line 2383
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/poi/hssf/model/InternalWorkbook;->drawingManager:Lorg/apache/poi/hssf/model/DrawingManager2;

    move-object/from16 v22, v0

    if-nez v22, :cond_1

    .line 2441
    :cond_0
    return-void

    .line 2389
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/poi/hssf/model/InternalWorkbook;->drawingManager:Lorg/apache/poi/hssf/model/DrawingManager2;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    move/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Lorg/apache/poi/hssf/model/InternalSheet;->aggregateDrawingRecords(Lorg/apache/poi/hssf/model/DrawingManager2;Z)I

    move-result v4

    .line 2390
    .local v4, "aggLoc":I
    const/16 v22, -0x1

    move/from16 v0, v22

    if-eq v4, v0, :cond_0

    .line 2391
    const/16 v22, 0x2694

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/model/InternalSheet;->findFirstRecordBySid(S)Lorg/apache/poi/hssf/record/Record;

    move-result-object v3

    check-cast v3, Lorg/apache/poi/hssf/record/EscherAggregate;

    .line 2392
    .local v3, "agg":Lorg/apache/poi/hssf/record/EscherAggregate;
    invoke-virtual {v3}, Lorg/apache/poi/hssf/record/EscherAggregate;->getEscherContainer()Lorg/apache/poi/ddf/EscherContainerRecord;

    move-result-object v11

    .line 2393
    .local v11, "escherContainer":Lorg/apache/poi/ddf/EscherContainerRecord;
    if-eqz v11, :cond_0

    .line 2397
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/poi/hssf/model/InternalWorkbook;->drawingManager:Lorg/apache/poi/hssf/model/DrawingManager2;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lorg/apache/poi/hssf/model/DrawingManager2;->getDgg()Lorg/apache/poi/ddf/EscherDggRecord;

    move-result-object v9

    .line 2400
    .local v9, "dgg":Lorg/apache/poi/ddf/EscherDggRecord;
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/poi/hssf/model/InternalWorkbook;->drawingManager:Lorg/apache/poi/hssf/model/DrawingManager2;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lorg/apache/poi/hssf/model/DrawingManager2;->findNewDrawingGroupId()S

    move-result v8

    .line 2401
    .local v8, "dgId":I
    const/16 v22, 0x0

    move/from16 v0, v22

    invoke-virtual {v9, v8, v0}, Lorg/apache/poi/ddf/EscherDggRecord;->addCluster(II)V

    .line 2402
    invoke-virtual {v9}, Lorg/apache/poi/ddf/EscherDggRecord;->getDrawingsSaved()I

    move-result v22

    add-int/lit8 v22, v22, 0x1

    move/from16 v0, v22

    invoke-virtual {v9, v0}, Lorg/apache/poi/ddf/EscherDggRecord;->setDrawingsSaved(I)V

    .line 2404
    const/4 v7, 0x0

    .line 2405
    .local v7, "dg":Lorg/apache/poi/ddf/EscherDgRecord;
    invoke-virtual {v11}, Lorg/apache/poi/ddf/EscherContainerRecord;->getChildIterator()Ljava/util/Iterator;

    move-result-object v12

    .local v12, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/poi/ddf/EscherRecord;>;"
    :cond_2
    :goto_0
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v22

    if-eqz v22, :cond_0

    .line 2406
    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lorg/apache/poi/ddf/EscherRecord;

    .line 2407
    .local v10, "er":Lorg/apache/poi/ddf/EscherRecord;
    instance-of v0, v10, Lorg/apache/poi/ddf/EscherDgRecord;

    move/from16 v22, v0

    if-eqz v22, :cond_3

    move-object v7, v10

    .line 2408
    check-cast v7, Lorg/apache/poi/ddf/EscherDgRecord;

    .line 2410
    shl-int/lit8 v22, v8, 0x4

    move/from16 v0, v22

    int-to-short v0, v0

    move/from16 v22, v0

    move/from16 v0, v22

    invoke-virtual {v7, v0}, Lorg/apache/poi/ddf/EscherDgRecord;->setOptions(S)V

    goto :goto_0

    .line 2411
    :cond_3
    instance-of v0, v10, Lorg/apache/poi/ddf/EscherContainerRecord;

    move/from16 v22, v0

    if-eqz v22, :cond_2

    move-object v6, v10

    .line 2413
    check-cast v6, Lorg/apache/poi/ddf/EscherContainerRecord;

    .line 2414
    .local v6, "cp":Lorg/apache/poi/ddf/EscherContainerRecord;
    invoke-virtual {v6}, Lorg/apache/poi/ddf/EscherContainerRecord;->getChildRecords()Ljava/util/List;

    move-result-object v22

    invoke-interface/range {v22 .. v22}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v21

    .local v21, "spIt":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/poi/ddf/EscherRecord;>;"
    :cond_4
    invoke-interface/range {v21 .. v21}, Ljava/util/Iterator;->hasNext()Z

    move-result v22

    if-eqz v22, :cond_2

    .line 2415
    invoke-interface/range {v21 .. v21}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lorg/apache/poi/ddf/EscherContainerRecord;

    .line 2416
    .local v18, "shapeContainer":Lorg/apache/poi/ddf/EscherContainerRecord;
    invoke-virtual/range {v18 .. v18}, Lorg/apache/poi/ddf/EscherContainerRecord;->getChildRecords()Ljava/util/List;

    move-result-object v22

    invoke-interface/range {v22 .. v22}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v22

    :cond_5
    :goto_1
    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->hasNext()Z

    move-result v23

    if-eqz v23, :cond_4

    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lorg/apache/poi/ddf/EscherRecord;

    .line 2417
    .local v17, "shapeChildRecord":Lorg/apache/poi/ddf/EscherRecord;
    invoke-virtual/range {v17 .. v17}, Lorg/apache/poi/ddf/EscherRecord;->getRecordId()S

    move-result v16

    .line 2418
    .local v16, "recordId":I
    const/16 v23, -0xff6

    move/from16 v0, v16

    move/from16 v1, v23

    if-ne v0, v1, :cond_6

    move-object/from16 v20, v17

    .line 2419
    check-cast v20, Lorg/apache/poi/ddf/EscherSpRecord;

    .line 2420
    .local v20, "sp":Lorg/apache/poi/ddf/EscherSpRecord;
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/poi/hssf/model/InternalWorkbook;->drawingManager:Lorg/apache/poi/hssf/model/DrawingManager2;

    move-object/from16 v23, v0

    int-to-short v0, v8

    move/from16 v24, v0

    move-object/from16 v0, v23

    move/from16 v1, v24

    invoke-virtual {v0, v1, v7}, Lorg/apache/poi/hssf/model/DrawingManager2;->allocateShapeId(SLorg/apache/poi/ddf/EscherDgRecord;)I

    move-result v19

    .line 2422
    .local v19, "shapeId":I
    invoke-virtual {v7}, Lorg/apache/poi/ddf/EscherDgRecord;->getNumShapes()I

    move-result v23

    add-int/lit8 v23, v23, -0x1

    move/from16 v0, v23

    invoke-virtual {v7, v0}, Lorg/apache/poi/ddf/EscherDgRecord;->setNumShapes(I)V

    .line 2423
    move-object/from16 v0, v20

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Lorg/apache/poi/ddf/EscherSpRecord;->setShapeId(I)V

    goto :goto_1

    .line 2424
    .end local v19    # "shapeId":I
    .end local v20    # "sp":Lorg/apache/poi/ddf/EscherSpRecord;
    :cond_6
    const/16 v23, -0xff5

    move/from16 v0, v16

    move/from16 v1, v23

    if-ne v0, v1, :cond_5

    move-object/from16 v13, v17

    .line 2425
    check-cast v13, Lorg/apache/poi/ddf/EscherOptRecord;

    .line 2427
    .local v13, "opt":Lorg/apache/poi/ddf/EscherOptRecord;
    const/16 v23, 0x104

    .line 2426
    move/from16 v0, v23

    invoke-virtual {v13, v0}, Lorg/apache/poi/ddf/EscherOptRecord;->lookup(I)Lorg/apache/poi/ddf/EscherProperty;

    move-result-object v15

    check-cast v15, Lorg/apache/poi/ddf/EscherSimpleProperty;

    .line 2428
    .local v15, "prop":Lorg/apache/poi/ddf/EscherSimpleProperty;
    if-eqz v15, :cond_5

    .line 2429
    invoke-virtual {v15}, Lorg/apache/poi/ddf/EscherSimpleProperty;->getPropertyValue()I

    move-result v14

    .line 2431
    .local v14, "pictureIndex":I
    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lorg/apache/poi/hssf/model/InternalWorkbook;->getBSERecord(I)Lorg/apache/poi/ddf/EscherBSERecord;

    move-result-object v5

    .line 2432
    .local v5, "bse":Lorg/apache/poi/ddf/EscherBSERecord;
    invoke-virtual {v5}, Lorg/apache/poi/ddf/EscherBSERecord;->getRef()I

    move-result v23

    add-int/lit8 v23, v23, 0x1

    move/from16 v0, v23

    invoke-virtual {v5, v0}, Lorg/apache/poi/ddf/EscherBSERecord;->setRef(I)V

    goto :goto_1
.end method

.method public cloneFilter(II)Lorg/apache/poi/hssf/record/NameRecord;
    .locals 10
    .param p1, "filterDbNameIndex"    # I
    .param p2, "newSheetIndex"    # I

    .prologue
    .line 2444
    invoke-virtual {p0, p1}, Lorg/apache/poi/hssf/model/InternalWorkbook;->getNameRecord(I)Lorg/apache/poi/hssf/record/NameRecord;

    move-result-object v4

    .line 2446
    .local v4, "origNameRecord":Lorg/apache/poi/hssf/record/NameRecord;
    invoke-virtual {p0, p2}, Lorg/apache/poi/hssf/model/InternalWorkbook;->checkExternSheet(I)S

    move-result v2

    .line 2447
    .local v2, "newExtSheetIx":I
    invoke-virtual {v4}, Lorg/apache/poi/hssf/record/NameRecord;->getNameDefinition()[Lorg/apache/poi/ss/formula/ptg/Ptg;

    move-result-object v6

    .line 2448
    .local v6, "ptgs":[Lorg/apache/poi/ss/formula/ptg/Ptg;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v8, v6

    if-lt v1, v8, :cond_0

    .line 2461
    const/16 v8, 0xd

    add-int/lit8 v9, p2, 0x1

    invoke-virtual {p0, v8, v9}, Lorg/apache/poi/hssf/model/InternalWorkbook;->createBuiltInName(BI)Lorg/apache/poi/hssf/record/NameRecord;

    move-result-object v3

    .line 2462
    .local v3, "newNameRecord":Lorg/apache/poi/hssf/record/NameRecord;
    invoke-virtual {v3, v6}, Lorg/apache/poi/hssf/record/NameRecord;->setNameDefinition([Lorg/apache/poi/ss/formula/ptg/Ptg;)V

    .line 2463
    const/4 v8, 0x1

    invoke-virtual {v3, v8}, Lorg/apache/poi/hssf/record/NameRecord;->setHidden(Z)V

    .line 2464
    return-object v3

    .line 2449
    .end local v3    # "newNameRecord":Lorg/apache/poi/hssf/record/NameRecord;
    :cond_0
    aget-object v5, v6, v1

    .line 2451
    .local v5, "ptg":Lorg/apache/poi/ss/formula/ptg/Ptg;
    instance-of v8, v5, Lorg/apache/poi/ss/formula/ptg/Area3DPtg;

    if-eqz v8, :cond_2

    .line 2452
    check-cast v5, Lorg/apache/poi/ss/formula/ptg/OperandPtg;

    .end local v5    # "ptg":Lorg/apache/poi/ss/formula/ptg/Ptg;
    invoke-virtual {v5}, Lorg/apache/poi/ss/formula/ptg/OperandPtg;->copy()Lorg/apache/poi/ss/formula/ptg/OperandPtg;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/ss/formula/ptg/Area3DPtg;

    .line 2453
    .local v0, "a3p":Lorg/apache/poi/ss/formula/ptg/Area3DPtg;
    invoke-virtual {v0, v2}, Lorg/apache/poi/ss/formula/ptg/Area3DPtg;->setExternSheetIndex(I)V

    .line 2454
    aput-object v0, v6, v1

    .line 2448
    .end local v0    # "a3p":Lorg/apache/poi/ss/formula/ptg/Area3DPtg;
    :cond_1
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2455
    .restart local v5    # "ptg":Lorg/apache/poi/ss/formula/ptg/Ptg;
    :cond_2
    instance-of v8, v5, Lorg/apache/poi/ss/formula/ptg/Ref3DPtg;

    if-eqz v8, :cond_1

    .line 2456
    check-cast v5, Lorg/apache/poi/ss/formula/ptg/OperandPtg;

    .end local v5    # "ptg":Lorg/apache/poi/ss/formula/ptg/Ptg;
    invoke-virtual {v5}, Lorg/apache/poi/ss/formula/ptg/OperandPtg;->copy()Lorg/apache/poi/ss/formula/ptg/OperandPtg;

    move-result-object v7

    check-cast v7, Lorg/apache/poi/ss/formula/ptg/Ref3DPtg;

    .line 2457
    .local v7, "r3p":Lorg/apache/poi/ss/formula/ptg/Ref3DPtg;
    invoke-virtual {v7, v2}, Lorg/apache/poi/ss/formula/ptg/Ref3DPtg;->setExternSheetIndex(I)V

    .line 2458
    aput-object v7, v6, v1

    goto :goto_1
.end method

.method public createBuiltInName(BI)Lorg/apache/poi/hssf/record/NameRecord;
    .locals 4
    .param p1, "builtInName"    # B
    .param p2, "sheetNumber"    # I

    .prologue
    .line 1909
    if-ltz p2, :cond_0

    add-int/lit8 v1, p2, 0x1

    const/16 v2, 0x7fff

    if-le v1, v2, :cond_1

    .line 1910
    :cond_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Sheet number ["

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "]is not valid "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1913
    :cond_1
    new-instance v0, Lorg/apache/poi/hssf/record/NameRecord;

    invoke-direct {v0, p1, p2}, Lorg/apache/poi/hssf/record/NameRecord;-><init>(BI)V

    .line 1915
    .local v0, "name":Lorg/apache/poi/hssf/record/NameRecord;
    iget-object v1, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->linkTable:Lorg/apache/poi/hssf/model/LinkTable;

    invoke-virtual {v1, v0}, Lorg/apache/poi/hssf/model/LinkTable;->nameAlreadyExists(Lorg/apache/poi/hssf/record/NameRecord;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1916
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Builtin ("

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 1917
    const-string/jumbo v3, ") already exists for sheet ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1916
    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1919
    :cond_2
    invoke-virtual {p0, v0}, Lorg/apache/poi/hssf/model/InternalWorkbook;->addName(Lorg/apache/poi/hssf/record/NameRecord;)Lorg/apache/poi/hssf/record/NameRecord;

    .line 1920
    return-object v0
.end method

.method public createCellXF()Lorg/apache/poi/hssf/record/ExtendedFormatRecord;
    .locals 3

    .prologue
    .line 870
    invoke-static {}, Lorg/apache/poi/hssf/model/InternalWorkbook;->createExtendedFormat()Lorg/apache/poi/hssf/record/ExtendedFormatRecord;

    move-result-object v0

    .line 872
    .local v0, "xf":Lorg/apache/poi/hssf/record/ExtendedFormatRecord;
    iget-object v1, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->records:Lorg/apache/poi/hssf/model/WorkbookRecordList;

    iget-object v2, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->records:Lorg/apache/poi/hssf/model/WorkbookRecordList;

    invoke-virtual {v2}, Lorg/apache/poi/hssf/model/WorkbookRecordList;->getXfpos()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v1, v2, v0}, Lorg/apache/poi/hssf/model/WorkbookRecordList;->add(ILorg/apache/poi/hssf/record/Record;)V

    .line 873
    iget-object v1, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->records:Lorg/apache/poi/hssf/model/WorkbookRecordList;

    iget-object v2, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->records:Lorg/apache/poi/hssf/model/WorkbookRecordList;

    invoke-virtual {v2}, Lorg/apache/poi/hssf/model/WorkbookRecordList;->getXfpos()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v1, v2}, Lorg/apache/poi/hssf/model/WorkbookRecordList;->setXfpos(I)V

    .line 874
    iget v1, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->numxfs:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->numxfs:I

    .line 875
    return-object v0
.end method

.method public createDrawingGroup()V
    .locals 12

    .prologue
    const/4 v10, 0x0

    .line 2166
    iget-object v9, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->drawingManager:Lorg/apache/poi/hssf/model/DrawingManager2;

    if-nez v9, :cond_2

    .line 2167
    new-instance v3, Lorg/apache/poi/ddf/EscherContainerRecord;

    invoke-direct {v3}, Lorg/apache/poi/ddf/EscherContainerRecord;-><init>()V

    .line 2168
    .local v3, "dggContainer":Lorg/apache/poi/ddf/EscherContainerRecord;
    new-instance v2, Lorg/apache/poi/ddf/EscherDggRecord;

    invoke-direct {v2}, Lorg/apache/poi/ddf/EscherDggRecord;-><init>()V

    .line 2169
    .local v2, "dgg":Lorg/apache/poi/ddf/EscherDggRecord;
    new-instance v7, Lorg/apache/poi/ddf/EscherOptRecord;

    invoke-direct {v7}, Lorg/apache/poi/ddf/EscherOptRecord;-><init>()V

    .line 2170
    .local v7, "opt":Lorg/apache/poi/ddf/EscherOptRecord;
    new-instance v8, Lorg/apache/poi/ddf/EscherSplitMenuColorsRecord;

    invoke-direct {v8}, Lorg/apache/poi/ddf/EscherSplitMenuColorsRecord;-><init>()V

    .line 2172
    .local v8, "splitMenuColors":Lorg/apache/poi/ddf/EscherSplitMenuColorsRecord;
    const/16 v9, -0x1000

    invoke-virtual {v3, v9}, Lorg/apache/poi/ddf/EscherContainerRecord;->setRecordId(S)V

    .line 2173
    const/16 v9, 0xf

    invoke-virtual {v3, v9}, Lorg/apache/poi/ddf/EscherContainerRecord;->setOptions(S)V

    .line 2174
    const/16 v9, -0xffa

    invoke-virtual {v2, v9}, Lorg/apache/poi/ddf/EscherDggRecord;->setRecordId(S)V

    .line 2175
    invoke-virtual {v2, v10}, Lorg/apache/poi/ddf/EscherDggRecord;->setOptions(S)V

    .line 2176
    const/16 v9, 0x400

    invoke-virtual {v2, v9}, Lorg/apache/poi/ddf/EscherDggRecord;->setShapeIdMax(I)V

    .line 2177
    invoke-virtual {v2, v10}, Lorg/apache/poi/ddf/EscherDggRecord;->setNumShapesSaved(I)V

    .line 2178
    invoke-virtual {v2, v10}, Lorg/apache/poi/ddf/EscherDggRecord;->setDrawingsSaved(I)V

    .line 2179
    new-array v9, v10, [Lorg/apache/poi/ddf/EscherDggRecord$FileIdCluster;

    invoke-virtual {v2, v9}, Lorg/apache/poi/ddf/EscherDggRecord;->setFileIdClusters([Lorg/apache/poi/ddf/EscherDggRecord$FileIdCluster;)V

    .line 2180
    new-instance v9, Lorg/apache/poi/hssf/model/DrawingManager2;

    invoke-direct {v9, v2}, Lorg/apache/poi/hssf/model/DrawingManager2;-><init>(Lorg/apache/poi/ddf/EscherDggRecord;)V

    iput-object v9, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->drawingManager:Lorg/apache/poi/hssf/model/DrawingManager2;

    .line 2181
    const/4 v0, 0x0

    .line 2182
    .local v0, "bstoreContainer":Lorg/apache/poi/ddf/EscherContainerRecord;
    iget-object v9, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->escherBSERecords:Ljava/util/List;

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v9

    if-lez v9, :cond_0

    .line 2184
    new-instance v0, Lorg/apache/poi/ddf/EscherContainerRecord;

    .end local v0    # "bstoreContainer":Lorg/apache/poi/ddf/EscherContainerRecord;
    invoke-direct {v0}, Lorg/apache/poi/ddf/EscherContainerRecord;-><init>()V

    .line 2185
    .restart local v0    # "bstoreContainer":Lorg/apache/poi/ddf/EscherContainerRecord;
    const/16 v9, -0xfff

    invoke-virtual {v0, v9}, Lorg/apache/poi/ddf/EscherContainerRecord;->setRecordId(S)V

    .line 2186
    iget-object v9, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->escherBSERecords:Ljava/util/List;

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v9

    shl-int/lit8 v9, v9, 0x4

    or-int/lit8 v9, v9, 0xf

    int-to-short v9, v9

    invoke-virtual {v0, v9}, Lorg/apache/poi/ddf/EscherContainerRecord;->setOptions(S)V

    .line 2187
    iget-object v9, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->escherBSERecords:Ljava/util/List;

    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-nez v10, :cond_3

    .line 2191
    :cond_0
    const/16 v9, -0xff5

    invoke-virtual {v7, v9}, Lorg/apache/poi/ddf/EscherOptRecord;->setRecordId(S)V

    .line 2192
    const/16 v9, 0x33

    invoke-virtual {v7, v9}, Lorg/apache/poi/ddf/EscherOptRecord;->setOptions(S)V

    .line 2193
    new-instance v9, Lorg/apache/poi/ddf/EscherBoolProperty;

    const/16 v10, 0xbf

    const v11, 0x80008

    invoke-direct {v9, v10, v11}, Lorg/apache/poi/ddf/EscherBoolProperty;-><init>(SI)V

    invoke-virtual {v7, v9}, Lorg/apache/poi/ddf/EscherOptRecord;->addEscherProperty(Lorg/apache/poi/ddf/EscherProperty;)V

    .line 2194
    new-instance v9, Lorg/apache/poi/ddf/EscherRGBProperty;

    const/16 v10, 0x181

    const v11, 0x8000041

    invoke-direct {v9, v10, v11}, Lorg/apache/poi/ddf/EscherRGBProperty;-><init>(SI)V

    invoke-virtual {v7, v9}, Lorg/apache/poi/ddf/EscherOptRecord;->addEscherProperty(Lorg/apache/poi/ddf/EscherProperty;)V

    .line 2195
    new-instance v9, Lorg/apache/poi/ddf/EscherRGBProperty;

    const/16 v10, 0x1c0

    const v11, 0x8000040

    invoke-direct {v9, v10, v11}, Lorg/apache/poi/ddf/EscherRGBProperty;-><init>(SI)V

    invoke-virtual {v7, v9}, Lorg/apache/poi/ddf/EscherOptRecord;->addEscherProperty(Lorg/apache/poi/ddf/EscherProperty;)V

    .line 2196
    const/16 v9, -0xee2

    invoke-virtual {v8, v9}, Lorg/apache/poi/ddf/EscherSplitMenuColorsRecord;->setRecordId(S)V

    .line 2197
    const/16 v9, 0x40

    invoke-virtual {v8, v9}, Lorg/apache/poi/ddf/EscherSplitMenuColorsRecord;->setOptions(S)V

    .line 2198
    const v9, 0x800000d

    invoke-virtual {v8, v9}, Lorg/apache/poi/ddf/EscherSplitMenuColorsRecord;->setColor1(I)V

    .line 2199
    const v9, 0x800000c

    invoke-virtual {v8, v9}, Lorg/apache/poi/ddf/EscherSplitMenuColorsRecord;->setColor2(I)V

    .line 2200
    const v9, 0x8000017

    invoke-virtual {v8, v9}, Lorg/apache/poi/ddf/EscherSplitMenuColorsRecord;->setColor3(I)V

    .line 2201
    const v9, 0x100000f7

    invoke-virtual {v8, v9}, Lorg/apache/poi/ddf/EscherSplitMenuColorsRecord;->setColor4(I)V

    .line 2203
    invoke-virtual {v3, v2}, Lorg/apache/poi/ddf/EscherContainerRecord;->addChildRecord(Lorg/apache/poi/ddf/EscherRecord;)V

    .line 2204
    if-eqz v0, :cond_1

    .line 2205
    invoke-virtual {v3, v0}, Lorg/apache/poi/ddf/EscherContainerRecord;->addChildRecord(Lorg/apache/poi/ddf/EscherRecord;)V

    .line 2206
    :cond_1
    invoke-virtual {v3, v7}, Lorg/apache/poi/ddf/EscherContainerRecord;->addChildRecord(Lorg/apache/poi/ddf/EscherRecord;)V

    .line 2207
    invoke-virtual {v3, v8}, Lorg/apache/poi/ddf/EscherContainerRecord;->addChildRecord(Lorg/apache/poi/ddf/EscherRecord;)V

    .line 2209
    const/16 v9, 0xeb

    invoke-virtual {p0, v9}, Lorg/apache/poi/hssf/model/InternalWorkbook;->findFirstRecordLocBySid(S)I

    move-result v1

    .line 2210
    .local v1, "dgLoc":I
    const/4 v9, -0x1

    if-ne v1, v9, :cond_4

    .line 2211
    new-instance v4, Lorg/apache/poi/hssf/record/DrawingGroupRecord;

    invoke-direct {v4}, Lorg/apache/poi/hssf/record/DrawingGroupRecord;-><init>()V

    .line 2212
    .local v4, "drawingGroup":Lorg/apache/poi/hssf/record/DrawingGroupRecord;
    invoke-virtual {v4, v3}, Lorg/apache/poi/hssf/record/DrawingGroupRecord;->addEscherRecord(Lorg/apache/poi/ddf/EscherRecord;)Z

    .line 2213
    const/16 v9, 0x8c

    invoke-virtual {p0, v9}, Lorg/apache/poi/hssf/model/InternalWorkbook;->findFirstRecordLocBySid(S)I

    move-result v6

    .line 2215
    .local v6, "loc":I
    invoke-virtual {p0}, Lorg/apache/poi/hssf/model/InternalWorkbook;->getRecords()Ljava/util/List;

    move-result-object v9

    add-int/lit8 v10, v6, 0x1

    invoke-interface {v9, v10, v4}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 2223
    .end local v0    # "bstoreContainer":Lorg/apache/poi/ddf/EscherContainerRecord;
    .end local v1    # "dgLoc":I
    .end local v2    # "dgg":Lorg/apache/poi/ddf/EscherDggRecord;
    .end local v3    # "dggContainer":Lorg/apache/poi/ddf/EscherContainerRecord;
    .end local v4    # "drawingGroup":Lorg/apache/poi/hssf/record/DrawingGroupRecord;
    .end local v6    # "loc":I
    .end local v7    # "opt":Lorg/apache/poi/ddf/EscherOptRecord;
    .end local v8    # "splitMenuColors":Lorg/apache/poi/ddf/EscherSplitMenuColorsRecord;
    :cond_2
    :goto_1
    return-void

    .line 2187
    .restart local v0    # "bstoreContainer":Lorg/apache/poi/ddf/EscherContainerRecord;
    .restart local v2    # "dgg":Lorg/apache/poi/ddf/EscherDggRecord;
    .restart local v3    # "dggContainer":Lorg/apache/poi/ddf/EscherContainerRecord;
    .restart local v7    # "opt":Lorg/apache/poi/ddf/EscherOptRecord;
    .restart local v8    # "splitMenuColors":Lorg/apache/poi/ddf/EscherSplitMenuColorsRecord;
    :cond_3
    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/apache/poi/ddf/EscherRecord;

    .line 2188
    .local v5, "escherRecord":Lorg/apache/poi/ddf/EscherRecord;
    invoke-virtual {v0, v5}, Lorg/apache/poi/ddf/EscherContainerRecord;->addChildRecord(Lorg/apache/poi/ddf/EscherRecord;)V

    goto/16 :goto_0

    .line 2217
    .end local v5    # "escherRecord":Lorg/apache/poi/ddf/EscherRecord;
    .restart local v1    # "dgLoc":I
    :cond_4
    new-instance v4, Lorg/apache/poi/hssf/record/DrawingGroupRecord;

    invoke-direct {v4}, Lorg/apache/poi/hssf/record/DrawingGroupRecord;-><init>()V

    .line 2218
    .restart local v4    # "drawingGroup":Lorg/apache/poi/hssf/record/DrawingGroupRecord;
    invoke-virtual {v4, v3}, Lorg/apache/poi/hssf/record/DrawingGroupRecord;->addEscherRecord(Lorg/apache/poi/ddf/EscherRecord;)Z

    .line 2219
    invoke-virtual {p0}, Lorg/apache/poi/hssf/model/InternalWorkbook;->getRecords()Ljava/util/List;

    move-result-object v9

    invoke-interface {v9, v1, v4}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto :goto_1
.end method

.method public createFormat(Ljava/lang/String;)I
    .locals 4
    .param p1, "formatString"    # Ljava/lang/String;

    .prologue
    const/16 v2, 0xa4

    .line 1989
    iget v3, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->maxformatid:I

    if-lt v3, v2, :cond_0

    iget v2, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->maxformatid:I

    add-int/lit8 v2, v2, 0x1

    :cond_0
    iput v2, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->maxformatid:I

    .line 1990
    new-instance v1, Lorg/apache/poi/hssf/record/FormatRecord;

    iget v2, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->maxformatid:I

    invoke-direct {v1, v2, p1}, Lorg/apache/poi/hssf/record/FormatRecord;-><init>(ILjava/lang/String;)V

    .line 1992
    .local v1, "rec":Lorg/apache/poi/hssf/record/FormatRecord;
    const/4 v0, 0x0

    .line 1993
    .local v0, "pos":I
    :goto_0
    iget-object v2, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->records:Lorg/apache/poi/hssf/model/WorkbookRecordList;

    invoke-virtual {v2}, Lorg/apache/poi/hssf/model/WorkbookRecordList;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    iget-object v2, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->records:Lorg/apache/poi/hssf/model/WorkbookRecordList;

    invoke-virtual {v2, v0}, Lorg/apache/poi/hssf/model/WorkbookRecordList;->get(I)Lorg/apache/poi/hssf/record/Record;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/poi/hssf/record/Record;->getSid()S

    move-result v2

    const/16 v3, 0x41e

    if-ne v2, v3, :cond_2

    .line 1995
    :cond_1
    iget-object v2, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->formats:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/2addr v0, v2

    .line 1996
    iget-object v2, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->formats:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1997
    iget-object v2, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->records:Lorg/apache/poi/hssf/model/WorkbookRecordList;

    invoke-virtual {v2, v0, v1}, Lorg/apache/poi/hssf/model/WorkbookRecordList;->add(ILorg/apache/poi/hssf/record/Record;)V

    .line 1998
    iget v2, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->maxformatid:I

    return v2

    .line 1994
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public createName()Lorg/apache/poi/hssf/record/NameRecord;
    .locals 1

    .prologue
    .line 1888
    new-instance v0, Lorg/apache/poi/hssf/record/NameRecord;

    invoke-direct {v0}, Lorg/apache/poi/hssf/record/NameRecord;-><init>()V

    invoke-virtual {p0, v0}, Lorg/apache/poi/hssf/model/InternalWorkbook;->addName(Lorg/apache/poi/hssf/record/NameRecord;)Lorg/apache/poi/hssf/record/NameRecord;

    move-result-object v0

    return-object v0
.end method

.method public createNewFont()Lorg/apache/poi/hssf/record/FontRecord;
    .locals 3

    .prologue
    .line 518
    invoke-static {}, Lorg/apache/poi/hssf/model/InternalWorkbook;->createFont()Lorg/apache/poi/hssf/record/FontRecord;

    move-result-object v0

    .line 520
    .local v0, "rec":Lorg/apache/poi/hssf/record/FontRecord;
    iget-object v1, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->records:Lorg/apache/poi/hssf/model/WorkbookRecordList;

    iget-object v2, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->records:Lorg/apache/poi/hssf/model/WorkbookRecordList;

    invoke-virtual {v2}, Lorg/apache/poi/hssf/model/WorkbookRecordList;->getFontpos()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v1, v2, v0}, Lorg/apache/poi/hssf/model/WorkbookRecordList;->add(ILorg/apache/poi/hssf/record/Record;)V

    .line 521
    iget-object v1, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->records:Lorg/apache/poi/hssf/model/WorkbookRecordList;

    iget-object v2, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->records:Lorg/apache/poi/hssf/model/WorkbookRecordList;

    invoke-virtual {v2}, Lorg/apache/poi/hssf/model/WorkbookRecordList;->getFontpos()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v1, v2}, Lorg/apache/poi/hssf/model/WorkbookRecordList;->setFontpos(I)V

    .line 522
    iget v1, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->numfonts:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->numfonts:I

    .line 523
    return-object v0
.end method

.method public createStyleRecord(I)Lorg/apache/poi/hssf/record/StyleRecord;
    .locals 6
    .param p1, "xfIndex"    # I

    .prologue
    const/4 v5, -0x1

    .line 909
    new-instance v2, Lorg/apache/poi/hssf/record/StyleRecord;

    invoke-direct {v2}, Lorg/apache/poi/hssf/record/StyleRecord;-><init>()V

    .line 910
    .local v2, "newSR":Lorg/apache/poi/hssf/record/StyleRecord;
    invoke-virtual {v2, p1}, Lorg/apache/poi/hssf/record/StyleRecord;->setXFIndex(I)V

    .line 913
    const/4 v0, -0x1

    .line 914
    .local v0, "addAt":I
    iget-object v4, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->records:Lorg/apache/poi/hssf/model/WorkbookRecordList;

    invoke-virtual {v4}, Lorg/apache/poi/hssf/model/WorkbookRecordList;->getXfpos()I

    move-result v1

    .local v1, "i":I
    :goto_0
    iget-object v4, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->records:Lorg/apache/poi/hssf/model/WorkbookRecordList;

    invoke-virtual {v4}, Lorg/apache/poi/hssf/model/WorkbookRecordList;->size()I

    move-result v4

    if-ge v1, v4, :cond_0

    .line 915
    if-eq v0, v5, :cond_1

    .line 924
    :cond_0
    if-ne v0, v5, :cond_3

    .line 925
    new-instance v4, Ljava/lang/IllegalStateException;

    const-string/jumbo v5, "No XF Records found!"

    invoke-direct {v4, v5}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 916
    :cond_1
    iget-object v4, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->records:Lorg/apache/poi/hssf/model/WorkbookRecordList;

    invoke-virtual {v4, v1}, Lorg/apache/poi/hssf/model/WorkbookRecordList;->get(I)Lorg/apache/poi/hssf/record/Record;

    move-result-object v3

    .line 917
    .local v3, "r":Lorg/apache/poi/hssf/record/Record;
    instance-of v4, v3, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;

    if-nez v4, :cond_2

    .line 918
    instance-of v4, v3, Lorg/apache/poi/hssf/record/StyleRecord;

    if-nez v4, :cond_2

    .line 921
    move v0, v1

    .line 915
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 927
    .end local v3    # "r":Lorg/apache/poi/hssf/record/Record;
    :cond_3
    iget-object v4, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->records:Lorg/apache/poi/hssf/model/WorkbookRecordList;

    invoke-virtual {v4, v0, v2}, Lorg/apache/poi/hssf/model/WorkbookRecordList;->add(ILorg/apache/poi/hssf/record/Record;)V

    .line 929
    return-object v2
.end method

.method public doesContainsSheetName(Ljava/lang/String;I)Z
    .locals 7
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "excludeSheetIdx"    # I

    .prologue
    const/4 v4, 0x0

    const/16 v6, 0x1f

    .line 603
    move-object v0, p1

    .line 604
    .local v0, "aName":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    if-le v5, v6, :cond_0

    .line 605
    invoke-virtual {v0, v4, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 607
    :cond_0
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    iget-object v5, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->boundsheets:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-lt v3, v5, :cond_1

    .line 620
    :goto_1
    return v4

    .line 608
    :cond_1
    invoke-direct {p0, v3}, Lorg/apache/poi/hssf/model/InternalWorkbook;->getBoundSheetRec(I)Lorg/apache/poi/hssf/record/BoundSheetRecord;

    move-result-object v2

    .line 609
    .local v2, "boundSheetRecord":Lorg/apache/poi/hssf/record/BoundSheetRecord;
    if-ne p2, v3, :cond_3

    .line 607
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 612
    :cond_3
    invoke-virtual {v2}, Lorg/apache/poi/hssf/record/BoundSheetRecord;->getSheetname()Ljava/lang/String;

    move-result-object v1

    .line 613
    .local v1, "bName":Ljava/lang/String;
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v5

    if-le v5, v6, :cond_4

    .line 614
    invoke-virtual {v1, v4, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 616
    :cond_4
    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 617
    const/4 v4, 0x1

    goto :goto_1
.end method

.method public findDrawingGroup()Lorg/apache/poi/hssf/model/DrawingManager2;
    .locals 12

    .prologue
    const/16 v11, -0xfff

    .line 2092
    iget-object v9, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->drawingManager:Lorg/apache/poi/hssf/model/DrawingManager2;

    if-eqz v9, :cond_0

    .line 2094
    iget-object v9, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->drawingManager:Lorg/apache/poi/hssf/model/DrawingManager2;

    .line 2158
    :goto_0
    return-object v9

    .line 2099
    :cond_0
    iget-object v9, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->records:Lorg/apache/poi/hssf/model/WorkbookRecordList;

    invoke-virtual {v9}, Lorg/apache/poi/hssf/model/WorkbookRecordList;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-nez v10, :cond_5

    .line 2134
    const/16 v9, 0xeb

    invoke-virtual {p0, v9}, Lorg/apache/poi/hssf/model/InternalWorkbook;->findFirstRecordLocBySid(S)I

    move-result v4

    .line 2137
    .local v4, "dgLoc":I
    const/4 v9, -0x1

    if-eq v4, v9, :cond_4

    .line 2138
    iget-object v9, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->records:Lorg/apache/poi/hssf/model/WorkbookRecordList;

    invoke-virtual {v9, v4}, Lorg/apache/poi/hssf/model/WorkbookRecordList;->get(I)Lorg/apache/poi/hssf/record/Record;

    move-result-object v3

    check-cast v3, Lorg/apache/poi/hssf/record/DrawingGroupRecord;

    .line 2139
    .local v3, "dg":Lorg/apache/poi/hssf/record/DrawingGroupRecord;
    const/4 v5, 0x0

    .line 2140
    .local v5, "dgg":Lorg/apache/poi/ddf/EscherDggRecord;
    const/4 v0, 0x0

    .line 2141
    .local v0, "bStore":Lorg/apache/poi/ddf/EscherContainerRecord;
    invoke-virtual {v3}, Lorg/apache/poi/hssf/record/DrawingGroupRecord;->getEscherRecords()Ljava/util/List;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_2
    :goto_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-nez v10, :cond_c

    .line 2149
    if-eqz v5, :cond_4

    .line 2150
    new-instance v9, Lorg/apache/poi/hssf/model/DrawingManager2;

    invoke-direct {v9, v5}, Lorg/apache/poi/hssf/model/DrawingManager2;-><init>(Lorg/apache/poi/ddf/EscherDggRecord;)V

    iput-object v9, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->drawingManager:Lorg/apache/poi/hssf/model/DrawingManager2;

    .line 2151
    if-eqz v0, :cond_4

    .line 2152
    invoke-virtual {v0}, Lorg/apache/poi/ddf/EscherContainerRecord;->getChildRecords()Ljava/util/List;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_3
    :goto_2
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-nez v10, :cond_e

    .line 2158
    .end local v0    # "bStore":Lorg/apache/poi/ddf/EscherContainerRecord;
    .end local v3    # "dg":Lorg/apache/poi/hssf/record/DrawingGroupRecord;
    .end local v5    # "dgg":Lorg/apache/poi/ddf/EscherDggRecord;
    :cond_4
    iget-object v9, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->drawingManager:Lorg/apache/poi/hssf/model/DrawingManager2;

    goto :goto_0

    .line 2099
    .end local v4    # "dgLoc":I
    :cond_5
    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lorg/apache/poi/hssf/record/Record;

    .line 2100
    .local v8, "r":Lorg/apache/poi/hssf/record/Record;
    instance-of v10, v8, Lorg/apache/poi/hssf/record/DrawingGroupRecord;

    if-eqz v10, :cond_1

    move-object v3, v8

    .line 2101
    check-cast v3, Lorg/apache/poi/hssf/record/DrawingGroupRecord;

    .line 2102
    .restart local v3    # "dg":Lorg/apache/poi/hssf/record/DrawingGroupRecord;
    invoke-virtual {v3}, Lorg/apache/poi/hssf/record/DrawingGroupRecord;->processChildRecords()V

    .line 2105
    invoke-virtual {v3}, Lorg/apache/poi/hssf/record/DrawingGroupRecord;->getEscherContainer()Lorg/apache/poi/ddf/EscherContainerRecord;

    move-result-object v2

    .line 2106
    .local v2, "cr":Lorg/apache/poi/ddf/EscherContainerRecord;
    if-eqz v2, :cond_1

    .line 2110
    const/4 v5, 0x0

    .line 2111
    .restart local v5    # "dgg":Lorg/apache/poi/ddf/EscherDggRecord;
    const/4 v0, 0x0

    .line 2112
    .restart local v0    # "bStore":Lorg/apache/poi/ddf/EscherContainerRecord;
    invoke-virtual {v2}, Lorg/apache/poi/ddf/EscherContainerRecord;->getChildIterator()Ljava/util/Iterator;

    move-result-object v7

    .local v7, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/poi/ddf/EscherRecord;>;"
    :cond_6
    :goto_3
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-nez v10, :cond_9

    .line 2121
    if-eqz v5, :cond_1

    .line 2122
    new-instance v9, Lorg/apache/poi/hssf/model/DrawingManager2;

    invoke-direct {v9, v5}, Lorg/apache/poi/hssf/model/DrawingManager2;-><init>(Lorg/apache/poi/ddf/EscherDggRecord;)V

    iput-object v9, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->drawingManager:Lorg/apache/poi/hssf/model/DrawingManager2;

    .line 2123
    if-eqz v0, :cond_8

    .line 2124
    invoke-virtual {v0}, Lorg/apache/poi/ddf/EscherContainerRecord;->getChildRecords()Ljava/util/List;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_7
    :goto_4
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-nez v10, :cond_b

    .line 2128
    :cond_8
    iget-object v9, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->drawingManager:Lorg/apache/poi/hssf/model/DrawingManager2;

    goto/16 :goto_0

    .line 2113
    :cond_9
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lorg/apache/poi/ddf/EscherRecord;

    .line 2114
    .local v6, "er":Lorg/apache/poi/ddf/EscherRecord;
    instance-of v10, v6, Lorg/apache/poi/ddf/EscherDggRecord;

    if-eqz v10, :cond_a

    move-object v5, v6

    .line 2115
    check-cast v5, Lorg/apache/poi/ddf/EscherDggRecord;

    .line 2116
    goto :goto_3

    :cond_a
    invoke-virtual {v6}, Lorg/apache/poi/ddf/EscherRecord;->getRecordId()S

    move-result v10

    if-ne v10, v11, :cond_6

    move-object v0, v6

    .line 2117
    check-cast v0, Lorg/apache/poi/ddf/EscherContainerRecord;

    goto :goto_3

    .line 2124
    .end local v6    # "er":Lorg/apache/poi/ddf/EscherRecord;
    :cond_b
    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/ddf/EscherRecord;

    .line 2125
    .local v1, "bs":Lorg/apache/poi/ddf/EscherRecord;
    instance-of v10, v1, Lorg/apache/poi/ddf/EscherBSERecord;

    if-eqz v10, :cond_7

    iget-object v10, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->escherBSERecords:Ljava/util/List;

    check-cast v1, Lorg/apache/poi/ddf/EscherBSERecord;

    .end local v1    # "bs":Lorg/apache/poi/ddf/EscherRecord;
    invoke-interface {v10, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 2141
    .end local v2    # "cr":Lorg/apache/poi/ddf/EscherContainerRecord;
    .end local v7    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/poi/ddf/EscherRecord;>;"
    .end local v8    # "r":Lorg/apache/poi/hssf/record/Record;
    .restart local v4    # "dgLoc":I
    :cond_c
    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lorg/apache/poi/ddf/EscherRecord;

    .line 2142
    .restart local v6    # "er":Lorg/apache/poi/ddf/EscherRecord;
    instance-of v10, v6, Lorg/apache/poi/ddf/EscherDggRecord;

    if-eqz v10, :cond_d

    move-object v5, v6

    .line 2143
    check-cast v5, Lorg/apache/poi/ddf/EscherDggRecord;

    .line 2144
    goto/16 :goto_1

    :cond_d
    invoke-virtual {v6}, Lorg/apache/poi/ddf/EscherRecord;->getRecordId()S

    move-result v10

    if-ne v10, v11, :cond_2

    move-object v0, v6

    .line 2145
    check-cast v0, Lorg/apache/poi/ddf/EscherContainerRecord;

    goto/16 :goto_1

    .line 2152
    .end local v6    # "er":Lorg/apache/poi/ddf/EscherRecord;
    :cond_e
    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/ddf/EscherRecord;

    .line 2153
    .restart local v1    # "bs":Lorg/apache/poi/ddf/EscherRecord;
    instance-of v10, v1, Lorg/apache/poi/ddf/EscherBSERecord;

    if-eqz v10, :cond_3

    iget-object v10, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->escherBSERecords:Ljava/util/List;

    check-cast v1, Lorg/apache/poi/ddf/EscherBSERecord;

    .end local v1    # "bs":Lorg/apache/poi/ddf/EscherRecord;
    invoke-interface {v10, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2
.end method

.method public findFirstRecordBySid(S)Lorg/apache/poi/hssf/record/Record;
    .locals 3
    .param p1, "sid"    # S

    .prologue
    .line 2007
    iget-object v1, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->records:Lorg/apache/poi/hssf/model/WorkbookRecordList;

    invoke-virtual {v1}, Lorg/apache/poi/hssf/model/WorkbookRecordList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_1

    .line 2012
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2007
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hssf/record/Record;

    .line 2008
    .local v0, "record":Lorg/apache/poi/hssf/record/Record;
    invoke-virtual {v0}, Lorg/apache/poi/hssf/record/Record;->getSid()S

    move-result v2

    if-ne v2, p1, :cond_0

    goto :goto_0
.end method

.method public findFirstRecordLocBySid(S)I
    .locals 4
    .param p1, "sid"    # S

    .prologue
    .line 2021
    const/4 v0, 0x0

    .line 2022
    .local v0, "index":I
    iget-object v2, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->records:Lorg/apache/poi/hssf/model/WorkbookRecordList;

    invoke-virtual {v2}, Lorg/apache/poi/hssf/model/WorkbookRecordList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_1

    .line 2028
    const/4 v0, -0x1

    .end local v0    # "index":I
    :cond_0
    return v0

    .line 2022
    .restart local v0    # "index":I
    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/hssf/record/Record;

    .line 2023
    .local v1, "record":Lorg/apache/poi/hssf/record/Record;
    invoke-virtual {v1}, Lorg/apache/poi/hssf/record/Record;->getSid()S

    move-result v3

    if-eq v3, p1, :cond_0

    .line 2026
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public findNextRecordBySid(SI)Lorg/apache/poi/hssf/record/Record;
    .locals 5
    .param p1, "sid"    # S
    .param p2, "pos"    # I

    .prologue
    .line 2035
    const/4 v0, 0x0

    .line 2036
    .local v0, "matches":I
    iget-object v3, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->records:Lorg/apache/poi/hssf/model/WorkbookRecordList;

    invoke-virtual {v3}, Lorg/apache/poi/hssf/model/WorkbookRecordList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_1

    .line 2042
    const/4 v2, 0x0

    :goto_1
    return-object v2

    .line 2036
    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/poi/hssf/record/Record;

    .line 2037
    .local v2, "record":Lorg/apache/poi/hssf/record/Record;
    invoke-virtual {v2}, Lorg/apache/poi/hssf/record/Record;->getSid()S

    move-result v4

    if-ne v4, p1, :cond_0

    .line 2038
    add-int/lit8 v1, v0, 0x1

    .end local v0    # "matches":I
    .local v1, "matches":I
    if-ne v0, p2, :cond_2

    move v0, v1

    .line 2039
    .end local v1    # "matches":I
    .restart local v0    # "matches":I
    goto :goto_1

    .end local v0    # "matches":I
    .restart local v1    # "matches":I
    :cond_2
    move v0, v1

    .end local v1    # "matches":I
    .restart local v0    # "matches":I
    goto :goto_0
.end method

.method public findSheetNameFromExternSheet(I)Ljava/lang/String;
    .locals 2
    .param p1, "externSheetIndex"    # I

    .prologue
    .line 1806
    iget-object v1, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->linkTable:Lorg/apache/poi/hssf/model/LinkTable;

    invoke-virtual {v1, p1}, Lorg/apache/poi/hssf/model/LinkTable;->getIndexToInternalSheet(I)I

    move-result v0

    .line 1807
    .local v0, "indexToSheet":I
    if-gez v0, :cond_0

    .line 1810
    const-string/jumbo v1, ""

    .line 1816
    :goto_0
    return-object v1

    .line 1812
    :cond_0
    iget-object v1, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->boundsheets:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lt v0, v1, :cond_1

    .line 1814
    const-string/jumbo v1, ""

    goto :goto_0

    .line 1816
    :cond_1
    invoke-virtual {p0, v0}, Lorg/apache/poi/hssf/model/InternalWorkbook;->getSheetName(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public getBSERecord(I)Lorg/apache/poi/ddf/EscherBSERecord;
    .locals 2
    .param p1, "pictureIndex"    # I

    .prologue
    .line 2230
    iget-object v0, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->escherBSERecords:Ljava/util/List;

    add-int/lit8 v1, p1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/ddf/EscherBSERecord;

    return-object v0
.end method

.method public getBackupRecord()Lorg/apache/poi/hssf/record/BackupRecord;
    .locals 2

    .prologue
    .line 572
    iget-object v0, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->records:Lorg/apache/poi/hssf/model/WorkbookRecordList;

    iget-object v1, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->records:Lorg/apache/poi/hssf/model/WorkbookRecordList;

    invoke-virtual {v1}, Lorg/apache/poi/hssf/model/WorkbookRecordList;->getBackuppos()I

    move-result v1

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/model/WorkbookRecordList;->get(I)Lorg/apache/poi/hssf/record/Record;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hssf/record/BackupRecord;

    return-object v0
.end method

.method public getCustomPalette()Lorg/apache/poi/hssf/record/PaletteRecord;
    .locals 6

    .prologue
    const/4 v4, 0x1

    .line 2071
    iget-object v3, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->records:Lorg/apache/poi/hssf/model/WorkbookRecordList;

    invoke-virtual {v3}, Lorg/apache/poi/hssf/model/WorkbookRecordList;->getPalettepos()I

    move-result v1

    .line 2072
    .local v1, "palettePos":I
    const/4 v3, -0x1

    if-eq v1, v3, :cond_1

    .line 2073
    iget-object v3, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->records:Lorg/apache/poi/hssf/model/WorkbookRecordList;

    invoke-virtual {v3, v1}, Lorg/apache/poi/hssf/model/WorkbookRecordList;->get(I)Lorg/apache/poi/hssf/record/Record;

    move-result-object v2

    .line 2074
    .local v2, "rec":Lorg/apache/poi/hssf/record/Record;
    instance-of v3, v2, Lorg/apache/poi/hssf/record/PaletteRecord;

    if-eqz v3, :cond_0

    move-object v0, v2

    .line 2075
    check-cast v0, Lorg/apache/poi/hssf/record/PaletteRecord;

    .line 2085
    .end local v2    # "rec":Lorg/apache/poi/hssf/record/Record;
    .local v0, "palette":Lorg/apache/poi/hssf/record/PaletteRecord;
    :goto_0
    return-object v0

    .line 2076
    .end local v0    # "palette":Lorg/apache/poi/hssf/record/PaletteRecord;
    .restart local v2    # "rec":Lorg/apache/poi/hssf/record/Record;
    :cond_0
    new-instance v3, Ljava/lang/RuntimeException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "InternalError: Expected PaletteRecord but got a \'"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 2080
    .end local v2    # "rec":Lorg/apache/poi/hssf/record/Record;
    :cond_1
    invoke-static {}, Lorg/apache/poi/hssf/model/InternalWorkbook;->createPalette()Lorg/apache/poi/hssf/record/PaletteRecord;

    move-result-object v0

    .line 2082
    .restart local v0    # "palette":Lorg/apache/poi/hssf/record/PaletteRecord;
    iget-object v3, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->records:Lorg/apache/poi/hssf/model/WorkbookRecordList;

    invoke-virtual {v3, v4, v0}, Lorg/apache/poi/hssf/model/WorkbookRecordList;->add(ILorg/apache/poi/hssf/record/Record;)V

    .line 2083
    iget-object v3, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->records:Lorg/apache/poi/hssf/model/WorkbookRecordList;

    invoke-virtual {v3, v4}, Lorg/apache/poi/hssf/model/WorkbookRecordList;->setPalettepos(I)V

    goto :goto_0
.end method

.method public getDrawingManager()Lorg/apache/poi/hssf/model/DrawingManager2;
    .locals 1

    .prologue
    .line 2265
    iget-object v0, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->drawingManager:Lorg/apache/poi/hssf/model/DrawingManager2;

    return-object v0
.end method

.method public getExFormatAt(I)Lorg/apache/poi/hssf/record/ExtendedFormatRecord;
    .locals 4
    .param p1, "index"    # I

    .prologue
    .line 828
    iget-object v2, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->records:Lorg/apache/poi/hssf/model/WorkbookRecordList;

    invoke-virtual {v2}, Lorg/apache/poi/hssf/model/WorkbookRecordList;->getXfpos()I

    move-result v2

    iget v3, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->numxfs:I

    add-int/lit8 v3, v3, -0x1

    sub-int v1, v2, v3

    .line 830
    .local v1, "xfptr":I
    add-int/2addr v1, p1

    .line 832
    iget-object v2, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->records:Lorg/apache/poi/hssf/model/WorkbookRecordList;

    invoke-virtual {v2, v1}, Lorg/apache/poi/hssf/model/WorkbookRecordList;->get(I)Lorg/apache/poi/hssf/record/Record;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;

    .line 834
    .local v0, "retval":Lorg/apache/poi/hssf/record/ExtendedFormatRecord;
    return-object v0
.end method

.method public getExternalName(II)Lorg/apache/poi/ss/formula/EvaluationWorkbook$ExternalName;
    .locals 3
    .param p1, "externSheetIndex"    # I
    .param p2, "externNameIndex"    # I

    .prologue
    .line 1826
    iget-object v2, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->linkTable:Lorg/apache/poi/hssf/model/LinkTable;

    invoke-virtual {v2, p1, p2}, Lorg/apache/poi/hssf/model/LinkTable;->resolveNameXText(II)Ljava/lang/String;

    move-result-object v1

    .line 1827
    .local v1, "nameName":Ljava/lang/String;
    if-nez v1, :cond_0

    .line 1828
    const/4 v2, 0x0

    .line 1831
    :goto_0
    return-object v2

    .line 1830
    :cond_0
    iget-object v2, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->linkTable:Lorg/apache/poi/hssf/model/LinkTable;

    invoke-virtual {v2, p1, p2}, Lorg/apache/poi/hssf/model/LinkTable;->resolveNameXIx(II)I

    move-result v0

    .line 1831
    .local v0, "ix":I
    new-instance v2, Lorg/apache/poi/ss/formula/EvaluationWorkbook$ExternalName;

    invoke-direct {v2, v1, p2, v0}, Lorg/apache/poi/ss/formula/EvaluationWorkbook$ExternalName;-><init>(Ljava/lang/String;II)V

    goto :goto_0
.end method

.method public getExternalSheet(I)Lorg/apache/poi/ss/formula/EvaluationWorkbook$ExternalSheet;
    .locals 4
    .param p1, "externSheetIndex"    # I

    .prologue
    .line 1819
    iget-object v1, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->linkTable:Lorg/apache/poi/hssf/model/LinkTable;

    invoke-virtual {v1, p1}, Lorg/apache/poi/hssf/model/LinkTable;->getExternalBookAndSheetName(I)[Ljava/lang/String;

    move-result-object v0

    .line 1820
    .local v0, "extNames":[Ljava/lang/String;
    if-nez v0, :cond_0

    .line 1821
    const/4 v1, 0x0

    .line 1823
    :goto_0
    return-object v1

    :cond_0
    new-instance v1, Lorg/apache/poi/ss/formula/EvaluationWorkbook$ExternalSheet;

    const/4 v2, 0x0

    aget-object v2, v0, v2

    const/4 v3, 0x1

    aget-object v3, v0, v3

    invoke-direct {v1, v2, v3}, Lorg/apache/poi/ss/formula/EvaluationWorkbook$ExternalSheet;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public getExternalSheetIndex(Ljava/lang/String;Ljava/lang/String;)I
    .locals 1
    .param p1, "workbookName"    # Ljava/lang/String;
    .param p2, "sheetName"    # Ljava/lang/String;

    .prologue
    .line 1854
    invoke-direct {p0}, Lorg/apache/poi/hssf/model/InternalWorkbook;->getOrCreateLinkTable()Lorg/apache/poi/hssf/model/LinkTable;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lorg/apache/poi/hssf/model/LinkTable;->getExternalSheetIndex(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public getFileSharing()Lorg/apache/poi/hssf/record/FileSharingRecord;
    .locals 4

    .prologue
    .line 2295
    iget-object v1, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->fileShare:Lorg/apache/poi/hssf/record/FileSharingRecord;

    if-nez v1, :cond_1

    .line 2296
    new-instance v1, Lorg/apache/poi/hssf/record/FileSharingRecord;

    invoke-direct {v1}, Lorg/apache/poi/hssf/record/FileSharingRecord;-><init>()V

    iput-object v1, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->fileShare:Lorg/apache/poi/hssf/record/FileSharingRecord;

    .line 2297
    const/4 v0, 0x0

    .line 2298
    .local v0, "i":I
    const/4 v0, 0x0

    .line 2299
    :goto_0
    iget-object v1, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->records:Lorg/apache/poi/hssf/model/WorkbookRecordList;

    invoke-virtual {v1}, Lorg/apache/poi/hssf/model/WorkbookRecordList;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->records:Lorg/apache/poi/hssf/model/WorkbookRecordList;

    invoke-virtual {v1, v0}, Lorg/apache/poi/hssf/model/WorkbookRecordList;->get(I)Lorg/apache/poi/hssf/record/Record;

    move-result-object v1

    instance-of v1, v1, Lorg/apache/poi/hssf/record/WriteAccessRecord;

    if-eqz v1, :cond_2

    .line 2302
    :cond_0
    iget-object v1, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->records:Lorg/apache/poi/hssf/model/WorkbookRecordList;

    add-int/lit8 v2, v0, 0x1

    iget-object v3, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->fileShare:Lorg/apache/poi/hssf/record/FileSharingRecord;

    invoke-virtual {v1, v2, v3}, Lorg/apache/poi/hssf/model/WorkbookRecordList;->add(ILorg/apache/poi/hssf/record/Record;)V

    .line 2304
    .end local v0    # "i":I
    :cond_1
    iget-object v1, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->fileShare:Lorg/apache/poi/hssf/record/FileSharingRecord;

    return-object v1

    .line 2300
    .restart local v0    # "i":I
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public getFontIndex(Lorg/apache/poi/hssf/record/FontRecord;)I
    .locals 5
    .param p1, "font"    # Lorg/apache/poi/hssf/record/FontRecord;

    .prologue
    .line 495
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v2, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->numfonts:I

    if-le v0, v2, :cond_0

    .line 506
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v3, "Could not find that font!"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 497
    :cond_0
    iget-object v2, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->records:Lorg/apache/poi/hssf/model/WorkbookRecordList;

    iget-object v3, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->records:Lorg/apache/poi/hssf/model/WorkbookRecordList;

    invoke-virtual {v3}, Lorg/apache/poi/hssf/model/WorkbookRecordList;->getFontpos()I

    move-result v3

    iget v4, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->numfonts:I

    add-int/lit8 v4, v4, -0x1

    sub-int/2addr v3, v4

    add-int/2addr v3, v0

    invoke-virtual {v2, v3}, Lorg/apache/poi/hssf/model/WorkbookRecordList;->get(I)Lorg/apache/poi/hssf/record/Record;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/hssf/record/FontRecord;

    .line 498
    .local v1, "thisFont":Lorg/apache/poi/hssf/record/FontRecord;
    if-ne v1, p1, :cond_2

    .line 500
    const/4 v2, 0x3

    if-le v0, v2, :cond_1

    .line 501
    add-int/lit8 v0, v0, 0x1

    .line 503
    .end local v0    # "i":I
    :cond_1
    return v0

    .line 495
    .restart local v0    # "i":I
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public getFontRecordAt(I)Lorg/apache/poi/hssf/record/FontRecord;
    .locals 5
    .param p1, "idx"    # I

    .prologue
    .line 475
    move v0, p1

    .line 477
    .local v0, "index":I
    const/4 v2, 0x4

    if-le v0, v2, :cond_0

    .line 478
    add-int/lit8 v0, v0, -0x1

    .line 480
    :cond_0
    iget v2, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->numfonts:I

    add-int/lit8 v2, v2, -0x1

    if-le v0, v2, :cond_1

    .line 481
    new-instance v2, Ljava/lang/ArrayIndexOutOfBoundsException;

    .line 482
    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "There are only "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v4, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->numfonts:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 483
    const-string/jumbo v4, " font records, you asked for "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 482
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 481
    invoke-direct {v2, v3}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 486
    :cond_1
    iget-object v2, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->records:Lorg/apache/poi/hssf/model/WorkbookRecordList;

    iget-object v3, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->records:Lorg/apache/poi/hssf/model/WorkbookRecordList;

    invoke-virtual {v3}, Lorg/apache/poi/hssf/model/WorkbookRecordList;->getFontpos()I

    move-result v3

    iget v4, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->numfonts:I

    add-int/lit8 v4, v4, -0x1

    sub-int/2addr v3, v4

    add-int/2addr v3, v0

    invoke-virtual {v2, v3}, Lorg/apache/poi/hssf/model/WorkbookRecordList;->get(I)Lorg/apache/poi/hssf/record/Record;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/hssf/record/FontRecord;

    .line 488
    .local v1, "retval":Lorg/apache/poi/hssf/record/FontRecord;
    return-object v1
.end method

.method public getFormat(Ljava/lang/String;Z)S
    .locals 3
    .param p1, "format"    # Ljava/lang/String;
    .param p2, "createIfNotFound"    # Z

    .prologue
    .line 1959
    iget-object v1, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->formats:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_1

    .line 1965
    if-eqz p2, :cond_2

    .line 1966
    invoke-virtual {p0, p1}, Lorg/apache/poi/hssf/model/InternalWorkbook;->createFormat(Ljava/lang/String;)I

    move-result v1

    int-to-short v1, v1

    .line 1969
    :goto_0
    return v1

    .line 1959
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hssf/record/FormatRecord;

    .line 1960
    .local v0, "r":Lorg/apache/poi/hssf/record/FormatRecord;
    invoke-virtual {v0}, Lorg/apache/poi/hssf/record/FormatRecord;->getFormatString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1961
    invoke-virtual {v0}, Lorg/apache/poi/hssf/record/FormatRecord;->getIndexCode()I

    move-result v1

    int-to-short v1, v1

    goto :goto_0

    .line 1969
    .end local v0    # "r":Lorg/apache/poi/hssf/record/FormatRecord;
    :cond_2
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public getFormats()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lorg/apache/poi/hssf/record/FormatRecord;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1977
    iget-object v0, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->formats:Ljava/util/List;

    return-object v0
.end method

.method public getHyperlinks()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lorg/apache/poi/hssf/record/HyperlinkRecord;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2047
    iget-object v0, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->hyperlinks:Ljava/util/List;

    return-object v0
.end method

.method public getNameCommentRecord(Lorg/apache/poi/hssf/record/NameRecord;)Lorg/apache/poi/hssf/record/NameCommentRecord;
    .locals 2
    .param p1, "nameRecord"    # Lorg/apache/poi/hssf/record/NameRecord;

    .prologue
    .line 1881
    iget-object v0, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->commentRecords:Ljava/util/Map;

    invoke-virtual {p1}, Lorg/apache/poi/hssf/record/NameRecord;->getNameText()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hssf/record/NameCommentRecord;

    return-object v0
.end method

.method public getNameRecord(I)Lorg/apache/poi/hssf/record/NameRecord;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 1873
    iget-object v0, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->linkTable:Lorg/apache/poi/hssf/model/LinkTable;

    invoke-virtual {v0, p1}, Lorg/apache/poi/hssf/model/LinkTable;->getNameRecord(I)Lorg/apache/poi/hssf/record/NameRecord;

    move-result-object v0

    return-object v0
.end method

.method public getNameXPtg(Ljava/lang/String;Lorg/apache/poi/ss/formula/udf/UDFFinder;)Lorg/apache/poi/ss/formula/ptg/NameXPtg;
    .locals 3
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "udf"    # Lorg/apache/poi/ss/formula/udf/UDFFinder;

    .prologue
    .line 2362
    invoke-direct {p0}, Lorg/apache/poi/hssf/model/InternalWorkbook;->getOrCreateLinkTable()Lorg/apache/poi/hssf/model/LinkTable;

    move-result-object v0

    .line 2363
    .local v0, "lnk":Lorg/apache/poi/hssf/model/LinkTable;
    invoke-virtual {v0, p1}, Lorg/apache/poi/hssf/model/LinkTable;->getNameXPtg(Ljava/lang/String;)Lorg/apache/poi/ss/formula/ptg/NameXPtg;

    move-result-object v1

    .line 2365
    .local v1, "xptg":Lorg/apache/poi/ss/formula/ptg/NameXPtg;
    if-nez v1, :cond_0

    invoke-interface {p2, p1}, Lorg/apache/poi/ss/formula/udf/UDFFinder;->findFunction(Ljava/lang/String;)Lorg/apache/poi/ss/formula/functions/FreeRefFunction;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 2368
    invoke-virtual {v0, p1}, Lorg/apache/poi/hssf/model/LinkTable;->addNameXPtg(Ljava/lang/String;)Lorg/apache/poi/ss/formula/ptg/NameXPtg;

    move-result-object v1

    .line 2370
    :cond_0
    return-object v1
.end method

.method public getNumExFormats()I
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 815
    sget-object v0, Lorg/apache/poi/hssf/model/InternalWorkbook;->log:Lorg/apache/poi/util/POILogger;

    invoke-virtual {v0, v3}, Lorg/apache/poi/util/POILogger;->check(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 816
    sget-object v0, Lorg/apache/poi/hssf/model/InternalWorkbook;->log:Lorg/apache/poi/util/POILogger;

    const-string/jumbo v1, "getXF="

    iget v2, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->numxfs:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;Ljava/lang/Object;)V

    .line 817
    :cond_0
    iget v0, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->numxfs:I

    return v0
.end method

.method public getNumNames()I
    .locals 1

    .prologue
    .line 1862
    iget-object v0, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->linkTable:Lorg/apache/poi/hssf/model/LinkTable;

    if-nez v0, :cond_0

    .line 1863
    const/4 v0, 0x0

    .line 1865
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->linkTable:Lorg/apache/poi/hssf/model/LinkTable;

    invoke-virtual {v0}, Lorg/apache/poi/hssf/model/LinkTable;->getNumNames()I

    move-result v0

    goto :goto_0
.end method

.method public getNumRecords()I
    .locals 1

    .prologue
    .line 462
    iget-object v0, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->records:Lorg/apache/poi/hssf/model/WorkbookRecordList;

    invoke-virtual {v0}, Lorg/apache/poi/hssf/model/WorkbookRecordList;->size()I

    move-result v0

    return v0
.end method

.method public getNumSheets()I
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 803
    sget-object v0, Lorg/apache/poi/hssf/model/InternalWorkbook;->log:Lorg/apache/poi/util/POILogger;

    invoke-virtual {v0, v3}, Lorg/apache/poi/util/POILogger;->check(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 804
    sget-object v0, Lorg/apache/poi/hssf/model/InternalWorkbook;->log:Lorg/apache/poi/util/POILogger;

    const-string/jumbo v1, "getNumSheets="

    iget-object v2, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->boundsheets:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;Ljava/lang/Object;)V

    .line 805
    :cond_0
    iget-object v0, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->boundsheets:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getNumberOfFontRecords()I
    .locals 1

    .prologue
    .line 544
    iget v0, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->numfonts:I

    return v0
.end method

.method public getRecalcId()Lorg/apache/poi/hssf/record/RecalcIdRecord;
    .locals 4

    .prologue
    .line 2486
    const/16 v2, 0x1c1

    invoke-virtual {p0, v2}, Lorg/apache/poi/hssf/model/InternalWorkbook;->findFirstRecordBySid(S)Lorg/apache/poi/hssf/record/Record;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/hssf/record/RecalcIdRecord;

    .line 2487
    .local v1, "record":Lorg/apache/poi/hssf/record/RecalcIdRecord;
    if-nez v1, :cond_0

    .line 2488
    new-instance v1, Lorg/apache/poi/hssf/record/RecalcIdRecord;

    .end local v1    # "record":Lorg/apache/poi/hssf/record/RecalcIdRecord;
    invoke-direct {v1}, Lorg/apache/poi/hssf/record/RecalcIdRecord;-><init>()V

    .line 2490
    .restart local v1    # "record":Lorg/apache/poi/hssf/record/RecalcIdRecord;
    const/16 v2, 0x8c

    invoke-virtual {p0, v2}, Lorg/apache/poi/hssf/model/InternalWorkbook;->findFirstRecordLocBySid(S)I

    move-result v0

    .line 2491
    .local v0, "pos":I
    iget-object v2, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->records:Lorg/apache/poi/hssf/model/WorkbookRecordList;

    add-int/lit8 v3, v0, 0x1

    invoke-virtual {v2, v3, v1}, Lorg/apache/poi/hssf/model/WorkbookRecordList;->add(ILorg/apache/poi/hssf/record/Record;)V

    .line 2493
    .end local v0    # "pos":I
    :cond_0
    return-object v1
.end method

.method public getRecords()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lorg/apache/poi/hssf/record/Record;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2051
    iget-object v0, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->records:Lorg/apache/poi/hssf/model/WorkbookRecordList;

    invoke-virtual {v0}, Lorg/apache/poi/hssf/model/WorkbookRecordList;->getRecords()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getSSTString(I)Lorg/apache/poi/hssf/record/common/UnicodeString;
    .locals 6
    .param p1, "str"    # I

    .prologue
    const/4 v1, 0x1

    .line 957
    iget-object v0, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->sst:Lorg/apache/poi/hssf/record/SSTRecord;

    if-nez v0, :cond_0

    .line 958
    invoke-virtual {p0}, Lorg/apache/poi/hssf/model/InternalWorkbook;->insertSST()V

    .line 960
    :cond_0
    iget-object v0, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->sst:Lorg/apache/poi/hssf/record/SSTRecord;

    invoke-virtual {v0, p1}, Lorg/apache/poi/hssf/record/SSTRecord;->getString(I)Lorg/apache/poi/hssf/record/common/UnicodeString;

    move-result-object v5

    .line 962
    .local v5, "retval":Lorg/apache/poi/hssf/record/common/UnicodeString;
    sget-object v0, Lorg/apache/poi/hssf/model/InternalWorkbook;->log:Lorg/apache/poi/util/POILogger;

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/POILogger;->check(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 963
    sget-object v0, Lorg/apache/poi/hssf/model/InternalWorkbook;->log:Lorg/apache/poi/util/POILogger;

    const-string/jumbo v2, "Returning SST for index="

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    .line 964
    const-string/jumbo v4, " String= "

    .line 963
    invoke-virtual/range {v0 .. v5}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 965
    :cond_1
    return-object v5
.end method

.method public getSheetIndex(Ljava/lang/String;)I
    .locals 4
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 714
    const/4 v1, -0x1

    .line 716
    .local v1, "retval":I
    const/4 v0, 0x0

    .local v0, "k":I
    :goto_0
    iget-object v3, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->boundsheets:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-lt v0, v3, :cond_0

    .line 724
    :goto_1
    return v1

    .line 717
    :cond_0
    invoke-virtual {p0, v0}, Lorg/apache/poi/hssf/model/InternalWorkbook;->getSheetName(I)Ljava/lang/String;

    move-result-object v2

    .line 719
    .local v2, "sheet":Ljava/lang/String;
    invoke-virtual {v2, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 720
    move v1, v0

    .line 721
    goto :goto_1

    .line 716
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public getSheetIndexFromExternSheetIndex(I)I
    .locals 1
    .param p1, "externSheetNumber"    # I

    .prologue
    .line 1841
    iget-object v0, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->linkTable:Lorg/apache/poi/hssf/model/LinkTable;

    invoke-virtual {v0, p1}, Lorg/apache/poi/hssf/model/LinkTable;->getSheetIndexFromExternSheetIndex(I)I

    move-result v0

    return v0
.end method

.method public getSheetName(I)Ljava/lang/String;
    .locals 1
    .param p1, "sheetIndex"    # I

    .prologue
    .line 643
    invoke-direct {p0, p1}, Lorg/apache/poi/hssf/model/InternalWorkbook;->getBoundSheetRec(I)Lorg/apache/poi/hssf/record/BoundSheetRecord;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/poi/hssf/record/BoundSheetRecord;->getSheetname()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSize()I
    .locals 6

    .prologue
    .line 1082
    const/4 v2, 0x0

    .line 1084
    .local v2, "retval":I
    const/4 v3, 0x0

    .line 1085
    .local v3, "sst":Lorg/apache/poi/hssf/record/SSTRecord;
    const/4 v0, 0x0

    .local v0, "k":I
    :goto_0
    iget-object v4, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->records:Lorg/apache/poi/hssf/model/WorkbookRecordList;

    invoke-virtual {v4}, Lorg/apache/poi/hssf/model/WorkbookRecordList;->size()I

    move-result v4

    if-lt v0, v4, :cond_0

    .line 1096
    return v2

    .line 1087
    :cond_0
    iget-object v4, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->records:Lorg/apache/poi/hssf/model/WorkbookRecordList;

    invoke-virtual {v4, v0}, Lorg/apache/poi/hssf/model/WorkbookRecordList;->get(I)Lorg/apache/poi/hssf/record/Record;

    move-result-object v1

    .line 1088
    .local v1, "record":Lorg/apache/poi/hssf/record/Record;
    instance-of v4, v1, Lorg/apache/poi/hssf/record/SSTRecord;

    if-eqz v4, :cond_1

    move-object v3, v1

    .line 1089
    check-cast v3, Lorg/apache/poi/hssf/record/SSTRecord;

    .line 1091
    :cond_1
    if-eqz v3, :cond_2

    invoke-virtual {v1}, Lorg/apache/poi/hssf/record/Record;->getSid()S

    move-result v4

    const/16 v5, 0xff

    if-ne v4, v5, :cond_2

    .line 1092
    invoke-virtual {v3}, Lorg/apache/poi/hssf/record/SSTRecord;->calcExtSSTRecordSize()I

    move-result v4

    add-int/2addr v2, v4

    .line 1085
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1094
    :cond_2
    invoke-virtual {v1}, Lorg/apache/poi/hssf/record/Record;->getRecordSize()I

    move-result v4

    add-int/2addr v2, v4

    goto :goto_1
.end method

.method public getSpecificBuiltinRecord(BI)Lorg/apache/poi/hssf/record/NameRecord;
    .locals 1
    .param p1, "name"    # B
    .param p2, "sheetNumber"    # I

    .prologue
    .line 448
    invoke-direct {p0}, Lorg/apache/poi/hssf/model/InternalWorkbook;->getOrCreateLinkTable()Lorg/apache/poi/hssf/model/LinkTable;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lorg/apache/poi/hssf/model/LinkTable;->getSpecificBuiltinRecord(BI)Lorg/apache/poi/hssf/record/NameRecord;

    move-result-object v0

    return-object v0
.end method

.method public getStyleRecord(I)Lorg/apache/poi/hssf/record/StyleRecord;
    .locals 4
    .param p1, "xfIndex"    # I

    .prologue
    .line 886
    iget-object v3, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->records:Lorg/apache/poi/hssf/model/WorkbookRecordList;

    invoke-virtual {v3}, Lorg/apache/poi/hssf/model/WorkbookRecordList;->getXfpos()I

    move-result v0

    .local v0, "i":I
    :goto_0
    iget-object v3, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->records:Lorg/apache/poi/hssf/model/WorkbookRecordList;

    invoke-virtual {v3}, Lorg/apache/poi/hssf/model/WorkbookRecordList;->size()I

    move-result v3

    if-lt v0, v3, :cond_0

    .line 899
    const/4 v2, 0x0

    :goto_1
    return-object v2

    .line 887
    :cond_0
    iget-object v3, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->records:Lorg/apache/poi/hssf/model/WorkbookRecordList;

    invoke-virtual {v3, v0}, Lorg/apache/poi/hssf/model/WorkbookRecordList;->get(I)Lorg/apache/poi/hssf/record/Record;

    move-result-object v1

    .line 888
    .local v1, "r":Lorg/apache/poi/hssf/record/Record;
    instance-of v3, v1, Lorg/apache/poi/hssf/record/ExtendedFormatRecord;

    if-eqz v3, :cond_2

    .line 886
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 891
    :cond_2
    instance-of v3, v1, Lorg/apache/poi/hssf/record/StyleRecord;

    if-eqz v3, :cond_1

    move-object v2, v1

    .line 894
    check-cast v2, Lorg/apache/poi/hssf/record/StyleRecord;

    .line 895
    .local v2, "sr":Lorg/apache/poi/hssf/record/StyleRecord;
    invoke-virtual {v2}, Lorg/apache/poi/hssf/record/StyleRecord;->getXFIndex()I

    move-result v3

    if-ne v3, p1, :cond_1

    goto :goto_1
.end method

.method public getWindowOne()Lorg/apache/poi/hssf/record/WindowOneRecord;
    .locals 1

    .prologue
    .line 2226
    iget-object v0, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->windowOne:Lorg/apache/poi/hssf/record/WindowOneRecord;

    return-object v0
.end method

.method public getWriteAccess()Lorg/apache/poi/hssf/record/WriteAccessRecord;
    .locals 4

    .prologue
    .line 2282
    iget-object v1, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->writeAccess:Lorg/apache/poi/hssf/record/WriteAccessRecord;

    if-nez v1, :cond_1

    .line 2283
    invoke-static {}, Lorg/apache/poi/hssf/model/InternalWorkbook;->createWriteAccess()Lorg/apache/poi/hssf/record/WriteAccessRecord;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->writeAccess:Lorg/apache/poi/hssf/record/WriteAccessRecord;

    .line 2284
    const/4 v0, 0x0

    .line 2285
    .local v0, "i":I
    const/4 v0, 0x0

    .line 2286
    :goto_0
    iget-object v1, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->records:Lorg/apache/poi/hssf/model/WorkbookRecordList;

    invoke-virtual {v1}, Lorg/apache/poi/hssf/model/WorkbookRecordList;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->records:Lorg/apache/poi/hssf/model/WorkbookRecordList;

    invoke-virtual {v1, v0}, Lorg/apache/poi/hssf/model/WorkbookRecordList;->get(I)Lorg/apache/poi/hssf/record/Record;

    move-result-object v1

    instance-of v1, v1, Lorg/apache/poi/hssf/record/InterfaceEndRecord;

    if-eqz v1, :cond_2

    .line 2289
    :cond_0
    iget-object v1, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->records:Lorg/apache/poi/hssf/model/WorkbookRecordList;

    add-int/lit8 v2, v0, 0x1

    iget-object v3, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->writeAccess:Lorg/apache/poi/hssf/record/WriteAccessRecord;

    invoke-virtual {v1, v2, v3}, Lorg/apache/poi/hssf/model/WorkbookRecordList;->add(ILorg/apache/poi/hssf/record/Record;)V

    .line 2291
    .end local v0    # "i":I
    :cond_1
    iget-object v1, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->writeAccess:Lorg/apache/poi/hssf/record/WriteAccessRecord;

    return-object v1

    .line 2287
    .restart local v0    # "i":I
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public getWriteProtect()Lorg/apache/poi/hssf/record/WriteProtectRecord;
    .locals 4

    .prologue
    .line 2269
    iget-object v1, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->writeProtect:Lorg/apache/poi/hssf/record/WriteProtectRecord;

    if-nez v1, :cond_1

    .line 2270
    new-instance v1, Lorg/apache/poi/hssf/record/WriteProtectRecord;

    invoke-direct {v1}, Lorg/apache/poi/hssf/record/WriteProtectRecord;-><init>()V

    iput-object v1, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->writeProtect:Lorg/apache/poi/hssf/record/WriteProtectRecord;

    .line 2271
    const/4 v0, 0x0

    .line 2272
    .local v0, "i":I
    const/4 v0, 0x0

    .line 2273
    :goto_0
    iget-object v1, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->records:Lorg/apache/poi/hssf/model/WorkbookRecordList;

    invoke-virtual {v1}, Lorg/apache/poi/hssf/model/WorkbookRecordList;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->records:Lorg/apache/poi/hssf/model/WorkbookRecordList;

    invoke-virtual {v1, v0}, Lorg/apache/poi/hssf/model/WorkbookRecordList;->get(I)Lorg/apache/poi/hssf/record/Record;

    move-result-object v1

    instance-of v1, v1, Lorg/apache/poi/hssf/record/BOFRecord;

    if-eqz v1, :cond_2

    .line 2276
    :cond_0
    iget-object v1, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->records:Lorg/apache/poi/hssf/model/WorkbookRecordList;

    add-int/lit8 v2, v0, 0x1

    iget-object v3, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->writeProtect:Lorg/apache/poi/hssf/record/WriteProtectRecord;

    invoke-virtual {v1, v2, v3}, Lorg/apache/poi/hssf/model/WorkbookRecordList;->add(ILorg/apache/poi/hssf/record/Record;)V

    .line 2278
    .end local v0    # "i":I
    :cond_1
    iget-object v1, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->writeProtect:Lorg/apache/poi/hssf/record/WriteProtectRecord;

    return-object v1

    .line 2274
    .restart local v0    # "i":I
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public insertSST()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 976
    sget-object v0, Lorg/apache/poi/hssf/model/InternalWorkbook;->log:Lorg/apache/poi/util/POILogger;

    invoke-virtual {v0, v2}, Lorg/apache/poi/util/POILogger;->check(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 977
    sget-object v0, Lorg/apache/poi/hssf/model/InternalWorkbook;->log:Lorg/apache/poi/util/POILogger;

    const-string/jumbo v1, "creating new SST via insertSST!"

    invoke-virtual {v0, v2, v1}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;)V

    .line 978
    :cond_0
    new-instance v0, Lorg/apache/poi/hssf/record/SSTRecord;

    invoke-direct {v0}, Lorg/apache/poi/hssf/record/SSTRecord;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->sst:Lorg/apache/poi/hssf/record/SSTRecord;

    .line 979
    iget-object v0, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->records:Lorg/apache/poi/hssf/model/WorkbookRecordList;

    iget-object v1, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->records:Lorg/apache/poi/hssf/model/WorkbookRecordList;

    invoke-virtual {v1}, Lorg/apache/poi/hssf/model/WorkbookRecordList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-static {}, Lorg/apache/poi/hssf/model/InternalWorkbook;->createExtendedSST()Lorg/apache/poi/hssf/record/ExtSSTRecord;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/apache/poi/hssf/model/WorkbookRecordList;->add(ILorg/apache/poi/hssf/record/Record;)V

    .line 980
    iget-object v0, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->records:Lorg/apache/poi/hssf/model/WorkbookRecordList;

    iget-object v1, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->records:Lorg/apache/poi/hssf/model/WorkbookRecordList;

    invoke-virtual {v1}, Lorg/apache/poi/hssf/model/WorkbookRecordList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x2

    iget-object v2, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->sst:Lorg/apache/poi/hssf/record/SSTRecord;

    invoke-virtual {v0, v1, v2}, Lorg/apache/poi/hssf/model/WorkbookRecordList;->add(ILorg/apache/poi/hssf/record/Record;)V

    .line 981
    return-void
.end method

.method public isSheetHidden(I)Z
    .locals 1
    .param p1, "sheetnum"    # I

    .prologue
    .line 656
    invoke-direct {p0, p1}, Lorg/apache/poi/hssf/model/InternalWorkbook;->getBoundSheetRec(I)Lorg/apache/poi/hssf/record/BoundSheetRecord;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/poi/hssf/record/BoundSheetRecord;->isHidden()Z

    move-result v0

    return v0
.end method

.method public isSheetVeryHidden(I)Z
    .locals 1
    .param p1, "sheetnum"    # I

    .prologue
    .line 669
    invoke-direct {p0, p1}, Lorg/apache/poi/hssf/model/InternalWorkbook;->getBoundSheetRec(I)Lorg/apache/poi/hssf/record/BoundSheetRecord;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/poi/hssf/record/BoundSheetRecord;->isVeryHidden()Z

    move-result v0

    return v0
.end method

.method public isUsing1904DateWindowing()Z
    .locals 1

    .prologue
    .line 2061
    iget-boolean v0, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->uses1904datewindowing:Z

    return v0
.end method

.method public isWriteProtected()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2311
    iget-object v3, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->fileShare:Lorg/apache/poi/hssf/record/FileSharingRecord;

    if-nez v3, :cond_1

    .line 2315
    :cond_0
    :goto_0
    return v1

    .line 2314
    :cond_1
    invoke-virtual {p0}, Lorg/apache/poi/hssf/model/InternalWorkbook;->getFileSharing()Lorg/apache/poi/hssf/record/FileSharingRecord;

    move-result-object v0

    .line 2315
    .local v0, "frec":Lorg/apache/poi/hssf/record/FileSharingRecord;
    invoke-virtual {v0}, Lorg/apache/poi/hssf/record/FileSharingRecord;->getReadOnly()S

    move-result v3

    if-ne v3, v2, :cond_0

    move v1, v2

    goto :goto_0
.end method

.method public preSerialize()V
    .locals 3

    .prologue
    .line 1072
    iget-object v1, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->records:Lorg/apache/poi/hssf/model/WorkbookRecordList;

    invoke-virtual {v1}, Lorg/apache/poi/hssf/model/WorkbookRecordList;->getTabpos()I

    move-result v1

    if-lez v1, :cond_0

    .line 1073
    iget-object v1, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->records:Lorg/apache/poi/hssf/model/WorkbookRecordList;

    iget-object v2, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->records:Lorg/apache/poi/hssf/model/WorkbookRecordList;

    invoke-virtual {v2}, Lorg/apache/poi/hssf/model/WorkbookRecordList;->getTabpos()I

    move-result v2

    invoke-virtual {v1, v2}, Lorg/apache/poi/hssf/model/WorkbookRecordList;->get(I)Lorg/apache/poi/hssf/record/Record;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hssf/record/TabIdRecord;

    .line 1074
    .local v0, "tir":Lorg/apache/poi/hssf/record/TabIdRecord;
    iget-object v1, v0, Lorg/apache/poi/hssf/record/TabIdRecord;->_tabids:[S

    array-length v1, v1

    iget-object v2, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->boundsheets:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 1075
    invoke-direct {p0}, Lorg/apache/poi/hssf/model/InternalWorkbook;->fixTabIdRecord()I

    .line 1078
    .end local v0    # "tir":Lorg/apache/poi/hssf/record/TabIdRecord;
    :cond_0
    return-void
.end method

.method public removeBuiltinRecord(BI)V
    .locals 1
    .param p1, "name"    # B
    .param p2, "sheetIndex"    # I

    .prologue
    .line 457
    iget-object v0, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->linkTable:Lorg/apache/poi/hssf/model/LinkTable;

    invoke-virtual {v0, p1, p2}, Lorg/apache/poi/hssf/model/LinkTable;->removeBuiltinRecord(BI)V

    .line 459
    return-void
.end method

.method public removeExFormatRecord(I)V
    .locals 3
    .param p1, "index"    # I

    .prologue
    .line 856
    iget-object v1, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->records:Lorg/apache/poi/hssf/model/WorkbookRecordList;

    invoke-virtual {v1}, Lorg/apache/poi/hssf/model/WorkbookRecordList;->getXfpos()I

    move-result v1

    iget v2, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->numxfs:I

    add-int/lit8 v2, v2, -0x1

    sub-int/2addr v1, v2

    add-int v0, v1, p1

    .line 857
    .local v0, "xfptr":I
    iget-object v1, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->records:Lorg/apache/poi/hssf/model/WorkbookRecordList;

    invoke-virtual {v1, v0}, Lorg/apache/poi/hssf/model/WorkbookRecordList;->remove(I)V

    .line 858
    iget v1, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->numxfs:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->numxfs:I

    .line 859
    return-void
.end method

.method public removeExFormatRecord(Lorg/apache/poi/hssf/record/ExtendedFormatRecord;)V
    .locals 1
    .param p1, "rec"    # Lorg/apache/poi/hssf/record/ExtendedFormatRecord;

    .prologue
    .line 844
    iget-object v0, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->records:Lorg/apache/poi/hssf/model/WorkbookRecordList;

    invoke-virtual {v0, p1}, Lorg/apache/poi/hssf/model/WorkbookRecordList;->remove(Ljava/lang/Object;)V

    .line 845
    iget v0, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->numxfs:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->numxfs:I

    .line 846
    return-void
.end method

.method public removeFontRecord(Lorg/apache/poi/hssf/record/FontRecord;)V
    .locals 1
    .param p1, "rec"    # Lorg/apache/poi/hssf/record/FontRecord;

    .prologue
    .line 533
    iget-object v0, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->records:Lorg/apache/poi/hssf/model/WorkbookRecordList;

    invoke-virtual {v0, p1}, Lorg/apache/poi/hssf/model/WorkbookRecordList;->remove(Ljava/lang/Object;)V

    .line 534
    iget v0, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->numfonts:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->numfonts:I

    .line 535
    return-void
.end method

.method public removeName(I)V
    .locals 3
    .param p1, "nameIndex"    # I

    .prologue
    .line 1929
    iget-object v1, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->linkTable:Lorg/apache/poi/hssf/model/LinkTable;

    invoke-virtual {v1}, Lorg/apache/poi/hssf/model/LinkTable;->getNumNames()I

    move-result v1

    if-le v1, p1, :cond_0

    .line 1930
    const/16 v1, 0x18

    invoke-virtual {p0, v1}, Lorg/apache/poi/hssf/model/InternalWorkbook;->findFirstRecordLocBySid(S)I

    move-result v0

    .line 1931
    .local v0, "idx":I
    iget-object v1, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->records:Lorg/apache/poi/hssf/model/WorkbookRecordList;

    add-int v2, v0, p1

    invoke-virtual {v1, v2}, Lorg/apache/poi/hssf/model/WorkbookRecordList;->remove(I)V

    .line 1932
    iget-object v1, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->linkTable:Lorg/apache/poi/hssf/model/LinkTable;

    invoke-virtual {v1, p1}, Lorg/apache/poi/hssf/model/LinkTable;->removeName(I)V

    .line 1934
    .end local v0    # "idx":I
    :cond_0
    return-void
.end method

.method public removeSheet(I)V
    .locals 6
    .param p1, "sheetIndex"    # I

    .prologue
    .line 750
    iget-object v3, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->boundsheets:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-le v3, p1, :cond_0

    .line 751
    iget-object v3, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->records:Lorg/apache/poi/hssf/model/WorkbookRecordList;

    iget-object v4, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->records:Lorg/apache/poi/hssf/model/WorkbookRecordList;

    invoke-virtual {v4}, Lorg/apache/poi/hssf/model/WorkbookRecordList;->getBspos()I

    move-result v4

    iget-object v5, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->boundsheets:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    sub-int/2addr v4, v5

    add-int/2addr v4, p1

    invoke-virtual {v3, v4}, Lorg/apache/poi/hssf/model/WorkbookRecordList;->remove(I)V

    .line 752
    iget-object v3, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->boundsheets:Ljava/util/List;

    invoke-interface {v3, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 753
    invoke-direct {p0}, Lorg/apache/poi/hssf/model/InternalWorkbook;->fixTabIdRecord()I

    .line 763
    :cond_0
    add-int/lit8 v2, p1, 0x1

    .line 764
    .local v2, "sheetNum1Based":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p0}, Lorg/apache/poi/hssf/model/InternalWorkbook;->getNumNames()I

    move-result v3

    if-lt v0, v3, :cond_1

    .line 776
    return-void

    .line 765
    :cond_1
    invoke-virtual {p0, v0}, Lorg/apache/poi/hssf/model/InternalWorkbook;->getNameRecord(I)Lorg/apache/poi/hssf/record/NameRecord;

    move-result-object v1

    .line 767
    .local v1, "nr":Lorg/apache/poi/hssf/record/NameRecord;
    invoke-virtual {v1}, Lorg/apache/poi/hssf/record/NameRecord;->getSheetNumber()I

    move-result v3

    if-ne v3, v2, :cond_3

    .line 769
    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Lorg/apache/poi/hssf/record/NameRecord;->setSheetNumber(I)V

    .line 764
    :cond_2
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 770
    :cond_3
    invoke-virtual {v1}, Lorg/apache/poi/hssf/record/NameRecord;->getSheetNumber()I

    move-result v3

    if-le v3, v2, :cond_2

    .line 773
    invoke-virtual {v1}, Lorg/apache/poi/hssf/record/NameRecord;->getSheetNumber()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v1, v3}, Lorg/apache/poi/hssf/record/NameRecord;->setSheetNumber(I)V

    goto :goto_1
.end method

.method public resolveNameXText(II)Ljava/lang/String;
    .locals 1
    .param p1, "refIndex"    # I
    .param p2, "definedNameIndex"    # I

    .prologue
    .line 2352
    iget-object v0, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->linkTable:Lorg/apache/poi/hssf/model/LinkTable;

    invoke-virtual {v0, p1, p2}, Lorg/apache/poi/hssf/model/LinkTable;->resolveNameXText(II)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public serialize(I[B)I
    .locals 11
    .param p1, "offset"    # I
    .param p2, "data"    # [B

    .prologue
    const/4 v10, 0x1

    .line 1020
    sget-object v8, Lorg/apache/poi/hssf/model/InternalWorkbook;->log:Lorg/apache/poi/util/POILogger;

    invoke-virtual {v8, v10}, Lorg/apache/poi/util/POILogger;->check(I)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 1021
    sget-object v8, Lorg/apache/poi/hssf/model/InternalWorkbook;->log:Lorg/apache/poi/util/POILogger;

    const-string/jumbo v9, "Serializing Workbook with offsets"

    invoke-virtual {v8, v10, v9}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;)V

    .line 1023
    :cond_0
    const/4 v3, 0x0

    .line 1025
    .local v3, "pos":I
    const/4 v5, 0x0

    .line 1026
    .local v5, "sst":Lorg/apache/poi/hssf/record/SSTRecord;
    const/4 v6, 0x0

    .line 1027
    .local v6, "sstPos":I
    const/4 v7, 0x0

    .line 1028
    .local v7, "wroteBoundSheets":Z
    const/4 v1, 0x0

    .local v1, "k":I
    :goto_0
    iget-object v8, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->records:Lorg/apache/poi/hssf/model/WorkbookRecordList;

    invoke-virtual {v8}, Lorg/apache/poi/hssf/model/WorkbookRecordList;->size()I

    move-result v8

    if-lt v1, v8, :cond_2

    .line 1059
    sget-object v8, Lorg/apache/poi/hssf/model/InternalWorkbook;->log:Lorg/apache/poi/util/POILogger;

    invoke-virtual {v8, v10}, Lorg/apache/poi/util/POILogger;->check(I)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 1060
    sget-object v8, Lorg/apache/poi/hssf/model/InternalWorkbook;->log:Lorg/apache/poi/util/POILogger;

    const-string/jumbo v9, "Exiting serialize workbook"

    invoke-virtual {v8, v10, v9}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;)V

    .line 1061
    :cond_1
    return v3

    .line 1031
    :cond_2
    iget-object v8, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->records:Lorg/apache/poi/hssf/model/WorkbookRecordList;

    invoke-virtual {v8, v1}, Lorg/apache/poi/hssf/model/WorkbookRecordList;->get(I)Lorg/apache/poi/hssf/record/Record;

    move-result-object v4

    .line 1032
    .local v4, "record":Lorg/apache/poi/hssf/record/Record;
    const/4 v2, 0x0

    .line 1033
    .local v2, "len":I
    instance-of v8, v4, Lorg/apache/poi/hssf/record/SSTRecord;

    if-eqz v8, :cond_3

    move-object v5, v4

    .line 1035
    check-cast v5, Lorg/apache/poi/hssf/record/SSTRecord;

    .line 1036
    move v6, v3

    .line 1038
    :cond_3
    invoke-virtual {v4}, Lorg/apache/poi/hssf/record/Record;->getSid()S

    move-result v8

    const/16 v9, 0xff

    if-ne v8, v9, :cond_4

    if-eqz v5, :cond_4

    .line 1040
    add-int v8, v6, p1

    invoke-virtual {v5, v8}, Lorg/apache/poi/hssf/record/SSTRecord;->createExtSSTRecord(I)Lorg/apache/poi/hssf/record/ExtSSTRecord;

    move-result-object v4

    .line 1042
    :cond_4
    instance-of v8, v4, Lorg/apache/poi/hssf/record/BoundSheetRecord;

    if-eqz v8, :cond_7

    .line 1043
    if-nez v7, :cond_5

    .line 1044
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget-object v8, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->boundsheets:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v8

    if-lt v0, v8, :cond_6

    .line 1048
    const/4 v7, 0x1

    .line 1057
    .end local v0    # "i":I
    :cond_5
    :goto_2
    add-int/2addr v3, v2

    .line 1028
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1045
    .restart local v0    # "i":I
    :cond_6
    invoke-direct {p0, v0}, Lorg/apache/poi/hssf/model/InternalWorkbook;->getBoundSheetRec(I)Lorg/apache/poi/hssf/record/BoundSheetRecord;

    move-result-object v8

    .line 1046
    add-int v9, v3, p1

    add-int/2addr v9, v2

    invoke-virtual {v8, v9, p2}, Lorg/apache/poi/hssf/record/BoundSheetRecord;->serialize(I[B)I

    move-result v8

    add-int/2addr v2, v8

    .line 1044
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1051
    .end local v0    # "i":I
    :cond_7
    add-int v8, v3, p1

    invoke-virtual {v4, v8, p2}, Lorg/apache/poi/hssf/record/Record;->serialize(I[B)I

    move-result v2

    goto :goto_2
.end method

.method public setSheetBof(II)V
    .locals 6
    .param p1, "sheetIndex"    # I
    .param p2, "pos"    # I

    .prologue
    const/4 v1, 0x1

    .line 555
    sget-object v0, Lorg/apache/poi/hssf/model/InternalWorkbook;->log:Lorg/apache/poi/util/POILogger;

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/POILogger;->check(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 556
    sget-object v0, Lorg/apache/poi/hssf/model/InternalWorkbook;->log:Lorg/apache/poi/util/POILogger;

    const-string/jumbo v2, "setting bof for sheetnum ="

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    .line 557
    const-string/jumbo v4, " at pos="

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    .line 556
    invoke-virtual/range {v0 .. v5}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 558
    :cond_0
    invoke-direct {p0, p1}, Lorg/apache/poi/hssf/model/InternalWorkbook;->checkSheets(I)V

    .line 559
    invoke-direct {p0, p1}, Lorg/apache/poi/hssf/model/InternalWorkbook;->getBoundSheetRec(I)Lorg/apache/poi/hssf/record/BoundSheetRecord;

    move-result-object v0

    .line 560
    invoke-virtual {v0, p2}, Lorg/apache/poi/hssf/record/BoundSheetRecord;->setPositionOfBof(I)V

    .line 561
    return-void
.end method

.method public setSheetHidden(II)V
    .locals 6
    .param p1, "sheetnum"    # I
    .param p2, "hidden"    # I

    .prologue
    .line 692
    invoke-direct {p0, p1}, Lorg/apache/poi/hssf/model/InternalWorkbook;->getBoundSheetRec(I)Lorg/apache/poi/hssf/record/BoundSheetRecord;

    move-result-object v0

    .line 693
    .local v0, "bsr":Lorg/apache/poi/hssf/record/BoundSheetRecord;
    const/4 v1, 0x0

    .line 694
    .local v1, "h":Z
    const/4 v2, 0x0

    .line 695
    .local v2, "vh":Z
    if-eqz p2, :cond_0

    .line 696
    const/4 v3, 0x1

    if-ne p2, v3, :cond_1

    .line 697
    const/4 v1, 0x1

    .line 703
    :cond_0
    :goto_0
    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/record/BoundSheetRecord;->setHidden(Z)V

    .line 704
    invoke-virtual {v0, v2}, Lorg/apache/poi/hssf/record/BoundSheetRecord;->setVeryHidden(Z)V

    .line 705
    return-void

    .line 698
    :cond_1
    const/4 v3, 0x2

    if-ne p2, v3, :cond_2

    .line 699
    const/4 v2, 0x1

    .line 700
    goto :goto_0

    .line 701
    :cond_2
    new-instance v3, Ljava/lang/IllegalArgumentException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "Invalid hidden flag "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " given, must be 0, 1 or 2"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3
.end method

.method public setSheetHidden(IZ)V
    .locals 1
    .param p1, "sheetnum"    # I
    .param p2, "hidden"    # Z

    .prologue
    .line 679
    invoke-direct {p0, p1}, Lorg/apache/poi/hssf/model/InternalWorkbook;->getBoundSheetRec(I)Lorg/apache/poi/hssf/record/BoundSheetRecord;

    move-result-object v0

    invoke-virtual {v0, p2}, Lorg/apache/poi/hssf/record/BoundSheetRecord;->setHidden(Z)V

    .line 680
    return-void
.end method

.method public setSheetName(ILjava/lang/String;)V
    .locals 3
    .param p1, "sheetnum"    # I
    .param p2, "sheetname"    # Ljava/lang/String;

    .prologue
    const/16 v2, 0x1f

    .line 585
    invoke-direct {p0, p1}, Lorg/apache/poi/hssf/model/InternalWorkbook;->checkSheets(I)V

    .line 588
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v1

    if-le v1, v2, :cond_0

    const/4 v1, 0x0

    invoke-virtual {p2, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p2

    .line 590
    :cond_0
    iget-object v1, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->boundsheets:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hssf/record/BoundSheetRecord;

    .line 591
    .local v0, "sheet":Lorg/apache/poi/hssf/record/BoundSheetRecord;
    invoke-virtual {v0, p2}, Lorg/apache/poi/hssf/record/BoundSheetRecord;->setSheetname(Ljava/lang/String;)V

    .line 592
    return-void
.end method

.method public setSheetOrder(Ljava/lang/String;I)V
    .locals 3
    .param p1, "sheetname"    # Ljava/lang/String;
    .param p2, "pos"    # I

    .prologue
    .line 631
    invoke-virtual {p0, p1}, Lorg/apache/poi/hssf/model/InternalWorkbook;->getSheetIndex(Ljava/lang/String;)I

    move-result v0

    .line 633
    .local v0, "sheetNumber":I
    iget-object v2, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->boundsheets:Ljava/util/List;

    iget-object v1, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->boundsheets:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/hssf/record/BoundSheetRecord;

    invoke-interface {v2, p2, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 634
    return-void
.end method

.method public unwriteProtectWorkbook()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2340
    iget-object v0, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->records:Lorg/apache/poi/hssf/model/WorkbookRecordList;

    iget-object v1, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->fileShare:Lorg/apache/poi/hssf/record/FileSharingRecord;

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/model/WorkbookRecordList;->remove(Ljava/lang/Object;)V

    .line 2341
    iget-object v0, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->records:Lorg/apache/poi/hssf/model/WorkbookRecordList;

    iget-object v1, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->writeProtect:Lorg/apache/poi/hssf/record/WriteProtectRecord;

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/model/WorkbookRecordList;->remove(Ljava/lang/Object;)V

    .line 2342
    iput-object v2, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->fileShare:Lorg/apache/poi/hssf/record/FileSharingRecord;

    .line 2343
    iput-object v2, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->writeProtect:Lorg/apache/poi/hssf/record/WriteProtectRecord;

    .line 2344
    return-void
.end method

.method public updateNameCommentRecordCache(Lorg/apache/poi/hssf/record/NameCommentRecord;)V
    .locals 3
    .param p1, "commentRecord"    # Lorg/apache/poi/hssf/record/NameCommentRecord;

    .prologue
    .line 1941
    iget-object v1, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->commentRecords:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->containsValue(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1942
    iget-object v1, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->commentRecords:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_2

    .line 1949
    :cond_1
    :goto_0
    iget-object v1, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->commentRecords:Ljava/util/Map;

    invoke-virtual {p1}, Lorg/apache/poi/hssf/record/NameCommentRecord;->getNameText()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1950
    return-void

    .line 1942
    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1943
    .local v0, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lorg/apache/poi/hssf/record/NameCommentRecord;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/hssf/record/NameCommentRecord;

    invoke-virtual {v1, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1944
    iget-object v1, p0, Lorg/apache/poi/hssf/model/InternalWorkbook;->commentRecords:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public updateNamesAfterCellShift(Lorg/apache/poi/ss/formula/FormulaShifter;)V
    .locals 4
    .param p1, "shifter"    # Lorg/apache/poi/ss/formula/FormulaShifter;

    .prologue
    .line 2471
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p0}, Lorg/apache/poi/hssf/model/InternalWorkbook;->getNumNames()I

    move-result v3

    if-lt v0, v3, :cond_0

    .line 2478
    return-void

    .line 2472
    :cond_0
    invoke-virtual {p0, v0}, Lorg/apache/poi/hssf/model/InternalWorkbook;->getNameRecord(I)Lorg/apache/poi/hssf/record/NameRecord;

    move-result-object v1

    .line 2473
    .local v1, "nr":Lorg/apache/poi/hssf/record/NameRecord;
    invoke-virtual {v1}, Lorg/apache/poi/hssf/record/NameRecord;->getNameDefinition()[Lorg/apache/poi/ss/formula/ptg/Ptg;

    move-result-object v2

    .line 2474
    .local v2, "ptgs":[Lorg/apache/poi/ss/formula/ptg/Ptg;
    invoke-virtual {v1}, Lorg/apache/poi/hssf/record/NameRecord;->getSheetNumber()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lorg/apache/poi/ss/formula/FormulaShifter;->adjustFormula([Lorg/apache/poi/ss/formula/ptg/Ptg;I)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2475
    invoke-virtual {v1, v2}, Lorg/apache/poi/hssf/record/NameRecord;->setNameDefinition([Lorg/apache/poi/ss/formula/ptg/Ptg;)V

    .line 2471
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public writeProtectWorkbook(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "password"    # Ljava/lang/String;
    .param p2, "username"    # Ljava/lang/String;

    .prologue
    .line 2326
    invoke-virtual {p0}, Lorg/apache/poi/hssf/model/InternalWorkbook;->getFileSharing()Lorg/apache/poi/hssf/record/FileSharingRecord;

    move-result-object v0

    .line 2327
    .local v0, "frec":Lorg/apache/poi/hssf/record/FileSharingRecord;
    invoke-virtual {p0}, Lorg/apache/poi/hssf/model/InternalWorkbook;->getWriteAccess()Lorg/apache/poi/hssf/record/WriteAccessRecord;

    move-result-object v1

    .line 2330
    .local v1, "waccess":Lorg/apache/poi/hssf/record/WriteAccessRecord;
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lorg/apache/poi/hssf/record/FileSharingRecord;->setReadOnly(S)V

    .line 2331
    invoke-static {p1}, Lorg/apache/poi/hssf/record/FileSharingRecord;->hashPassword(Ljava/lang/String;)S

    move-result v2

    invoke-virtual {v0, v2}, Lorg/apache/poi/hssf/record/FileSharingRecord;->setPassword(S)V

    .line 2332
    invoke-virtual {v0, p2}, Lorg/apache/poi/hssf/record/FileSharingRecord;->setUsername(Ljava/lang/String;)V

    .line 2333
    invoke-virtual {v1, p2}, Lorg/apache/poi/hssf/record/WriteAccessRecord;->setUsername(Ljava/lang/String;)V

    .line 2334
    return-void
.end method
