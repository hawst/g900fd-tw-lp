.class public final Lorg/apache/poi/hssf/record/CFHeaderRecord;
.super Lorg/apache/poi/hssf/record/StandardRecord;
.source "CFHeaderRecord.java"

# interfaces
.implements Ljava/lang/Cloneable;


# static fields
.field public static final sid:S = 0x1b0s


# instance fields
.field private field_1_numcf:I

.field private field_2_need_recalculation:I

.field private field_3_enclosing_cell_range:Lorg/apache/poi/ss/util/CellRangeAddress;

.field private field_4_cell_ranges:Lorg/apache/poi/ss/util/CellRangeAddressList;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0}, Lorg/apache/poi/hssf/record/StandardRecord;-><init>()V

    .line 41
    new-instance v0, Lorg/apache/poi/ss/util/CellRangeAddressList;

    invoke-direct {v0}, Lorg/apache/poi/ss/util/CellRangeAddressList;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/hssf/record/CFHeaderRecord;->field_4_cell_ranges:Lorg/apache/poi/ss/util/CellRangeAddressList;

    .line 42
    return-void
.end method

.method public constructor <init>(Lorg/apache/poi/hssf/record/RecordInputStream;)V
    .locals 1
    .param p1, "in"    # Lorg/apache/poi/hssf/record/RecordInputStream;

    .prologue
    .line 50
    invoke-direct {p0}, Lorg/apache/poi/hssf/record/StandardRecord;-><init>()V

    .line 52
    invoke-virtual {p1}, Lorg/apache/poi/hssf/record/RecordInputStream;->readShort()S

    move-result v0

    iput v0, p0, Lorg/apache/poi/hssf/record/CFHeaderRecord;->field_1_numcf:I

    .line 53
    invoke-virtual {p1}, Lorg/apache/poi/hssf/record/RecordInputStream;->readShort()S

    move-result v0

    iput v0, p0, Lorg/apache/poi/hssf/record/CFHeaderRecord;->field_2_need_recalculation:I

    .line 54
    new-instance v0, Lorg/apache/poi/ss/util/CellRangeAddress;

    invoke-direct {v0, p1}, Lorg/apache/poi/ss/util/CellRangeAddress;-><init>(Lorg/apache/poi/hssf/record/RecordInputStream;)V

    iput-object v0, p0, Lorg/apache/poi/hssf/record/CFHeaderRecord;->field_3_enclosing_cell_range:Lorg/apache/poi/ss/util/CellRangeAddress;

    .line 55
    new-instance v0, Lorg/apache/poi/ss/util/CellRangeAddressList;

    invoke-direct {v0, p1}, Lorg/apache/poi/ss/util/CellRangeAddressList;-><init>(Lorg/apache/poi/hssf/record/RecordInputStream;)V

    iput-object v0, p0, Lorg/apache/poi/hssf/record/CFHeaderRecord;->field_4_cell_ranges:Lorg/apache/poi/ss/util/CellRangeAddressList;

    .line 56
    return-void
.end method

.method public constructor <init>([Lorg/apache/poi/ss/util/CellRangeAddress;I)V
    .locals 2
    .param p1, "regions"    # [Lorg/apache/poi/ss/util/CellRangeAddress;
    .param p2, "nRules"    # I

    .prologue
    .line 43
    invoke-direct {p0}, Lorg/apache/poi/hssf/record/StandardRecord;-><init>()V

    .line 44
    move-object v1, p1

    .line 45
    .local v1, "unmergedRanges":[Lorg/apache/poi/ss/util/CellRangeAddress;
    invoke-static {v1}, Lorg/apache/poi/hssf/record/cf/CellRangeUtil;->mergeCellRanges([Lorg/apache/poi/ss/util/CellRangeAddress;)[Lorg/apache/poi/ss/util/CellRangeAddress;

    move-result-object v0

    .line 46
    .local v0, "mergeCellRanges":[Lorg/apache/poi/ss/util/CellRangeAddress;
    invoke-virtual {p0, v0}, Lorg/apache/poi/hssf/record/CFHeaderRecord;->setCellRanges([Lorg/apache/poi/ss/util/CellRangeAddress;)V

    .line 47
    iput p2, p0, Lorg/apache/poi/hssf/record/CFHeaderRecord;->field_1_numcf:I

    .line 48
    return-void
.end method


# virtual methods
.method public clone()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 153
    new-instance v0, Lorg/apache/poi/hssf/record/CFHeaderRecord;

    invoke-direct {v0}, Lorg/apache/poi/hssf/record/CFHeaderRecord;-><init>()V

    .line 154
    .local v0, "result":Lorg/apache/poi/hssf/record/CFHeaderRecord;
    iget v1, p0, Lorg/apache/poi/hssf/record/CFHeaderRecord;->field_1_numcf:I

    iput v1, v0, Lorg/apache/poi/hssf/record/CFHeaderRecord;->field_1_numcf:I

    .line 155
    iget v1, p0, Lorg/apache/poi/hssf/record/CFHeaderRecord;->field_2_need_recalculation:I

    iput v1, v0, Lorg/apache/poi/hssf/record/CFHeaderRecord;->field_2_need_recalculation:I

    .line 156
    iget-object v1, p0, Lorg/apache/poi/hssf/record/CFHeaderRecord;->field_3_enclosing_cell_range:Lorg/apache/poi/ss/util/CellRangeAddress;

    iput-object v1, v0, Lorg/apache/poi/hssf/record/CFHeaderRecord;->field_3_enclosing_cell_range:Lorg/apache/poi/ss/util/CellRangeAddress;

    .line 157
    iget-object v1, p0, Lorg/apache/poi/hssf/record/CFHeaderRecord;->field_4_cell_ranges:Lorg/apache/poi/ss/util/CellRangeAddressList;

    invoke-virtual {v1}, Lorg/apache/poi/ss/util/CellRangeAddressList;->copy()Lorg/apache/poi/ss/util/CellRangeAddressList;

    move-result-object v1

    iput-object v1, v0, Lorg/apache/poi/hssf/record/CFHeaderRecord;->field_4_cell_ranges:Lorg/apache/poi/ss/util/CellRangeAddressList;

    .line 158
    return-object v0
.end method

.method public getCellRanges()[Lorg/apache/poi/ss/util/CellRangeAddress;
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lorg/apache/poi/hssf/record/CFHeaderRecord;->field_4_cell_ranges:Lorg/apache/poi/ss/util/CellRangeAddressList;

    invoke-virtual {v0}, Lorg/apache/poi/ss/util/CellRangeAddressList;->getCellRangeAddresses()[Lorg/apache/poi/ss/util/CellRangeAddress;

    move-result-object v0

    return-object v0
.end method

.method protected getDataSize()I
    .locals 1

    .prologue
    .line 134
    .line 136
    iget-object v0, p0, Lorg/apache/poi/hssf/record/CFHeaderRecord;->field_4_cell_ranges:Lorg/apache/poi/ss/util/CellRangeAddressList;

    invoke-virtual {v0}, Lorg/apache/poi/ss/util/CellRangeAddressList;->getSize()I

    move-result v0

    .line 134
    add-int/lit8 v0, v0, 0xc

    return v0
.end method

.method public getEnclosingCellRange()Lorg/apache/poi/ss/util/CellRangeAddress;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lorg/apache/poi/hssf/record/CFHeaderRecord;->field_3_enclosing_cell_range:Lorg/apache/poi/ss/util/CellRangeAddress;

    return-object v0
.end method

.method public getNeedRecalculation()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 69
    iget v1, p0, Lorg/apache/poi/hssf/record/CFHeaderRecord;->field_2_need_recalculation:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getNumberOfConditionalFormats()I
    .locals 1

    .prologue
    .line 60
    iget v0, p0, Lorg/apache/poi/hssf/record/CFHeaderRecord;->field_1_numcf:I

    return v0
.end method

.method public getSid()S
    .locals 1

    .prologue
    .line 148
    const/16 v0, 0x1b0

    return v0
.end method

.method public serialize(Lorg/apache/poi/util/LittleEndianOutput;)V
    .locals 1
    .param p1, "out"    # Lorg/apache/poi/util/LittleEndianOutput;

    .prologue
    .line 141
    iget v0, p0, Lorg/apache/poi/hssf/record/CFHeaderRecord;->field_1_numcf:I

    invoke-interface {p1, v0}, Lorg/apache/poi/util/LittleEndianOutput;->writeShort(I)V

    .line 142
    iget v0, p0, Lorg/apache/poi/hssf/record/CFHeaderRecord;->field_2_need_recalculation:I

    invoke-interface {p1, v0}, Lorg/apache/poi/util/LittleEndianOutput;->writeShort(I)V

    .line 143
    iget-object v0, p0, Lorg/apache/poi/hssf/record/CFHeaderRecord;->field_3_enclosing_cell_range:Lorg/apache/poi/ss/util/CellRangeAddress;

    invoke-virtual {v0, p1}, Lorg/apache/poi/ss/util/CellRangeAddress;->serialize(Lorg/apache/poi/util/LittleEndianOutput;)V

    .line 144
    iget-object v0, p0, Lorg/apache/poi/hssf/record/CFHeaderRecord;->field_4_cell_ranges:Lorg/apache/poi/ss/util/CellRangeAddressList;

    invoke-virtual {v0, p1}, Lorg/apache/poi/ss/util/CellRangeAddressList;->serialize(Lorg/apache/poi/util/LittleEndianOutput;)V

    .line 145
    return-void
.end method

.method public setCellRanges([Lorg/apache/poi/ss/util/CellRangeAddress;)V
    .locals 6
    .param p1, "cellRanges"    # [Lorg/apache/poi/ss/util/CellRangeAddress;

    .prologue
    .line 94
    if-nez p1, :cond_0

    .line 96
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v5, "cellRanges must not be null"

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 98
    :cond_0
    new-instance v1, Lorg/apache/poi/ss/util/CellRangeAddressList;

    invoke-direct {v1}, Lorg/apache/poi/ss/util/CellRangeAddressList;-><init>()V

    .line 99
    .local v1, "cral":Lorg/apache/poi/ss/util/CellRangeAddressList;
    const/4 v2, 0x0

    .line 100
    .local v2, "enclosingRange":Lorg/apache/poi/ss/util/CellRangeAddress;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    array-length v4, p1

    if-lt v3, v4, :cond_1

    .line 106
    iput-object v2, p0, Lorg/apache/poi/hssf/record/CFHeaderRecord;->field_3_enclosing_cell_range:Lorg/apache/poi/ss/util/CellRangeAddress;

    .line 107
    iput-object v1, p0, Lorg/apache/poi/hssf/record/CFHeaderRecord;->field_4_cell_ranges:Lorg/apache/poi/ss/util/CellRangeAddressList;

    .line 108
    return-void

    .line 102
    :cond_1
    aget-object v0, p1, v3

    .line 103
    .local v0, "cr":Lorg/apache/poi/ss/util/CellRangeAddress;
    invoke-static {v0, v2}, Lorg/apache/poi/hssf/record/cf/CellRangeUtil;->createEnclosingCellRange(Lorg/apache/poi/ss/util/CellRangeAddress;Lorg/apache/poi/ss/util/CellRangeAddress;)Lorg/apache/poi/ss/util/CellRangeAddress;

    move-result-object v2

    .line 104
    invoke-virtual {v1, v0}, Lorg/apache/poi/ss/util/CellRangeAddressList;->addCellRangeAddress(Lorg/apache/poi/ss/util/CellRangeAddress;)V

    .line 100
    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method

.method public setEnclosingCellRange(Lorg/apache/poi/ss/util/CellRangeAddress;)V
    .locals 0
    .param p1, "cr"    # Lorg/apache/poi/ss/util/CellRangeAddress;

    .prologue
    .line 84
    iput-object p1, p0, Lorg/apache/poi/hssf/record/CFHeaderRecord;->field_3_enclosing_cell_range:Lorg/apache/poi/ss/util/CellRangeAddress;

    .line 85
    return-void
.end method

.method public setNeedRecalculation(Z)V
    .locals 1
    .param p1, "b"    # Z

    .prologue
    .line 74
    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput v0, p0, Lorg/apache/poi/hssf/record/CFHeaderRecord;->field_2_need_recalculation:I

    .line 75
    return-void

    .line 74
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setNumberOfConditionalFormats(I)V
    .locals 0
    .param p1, "n"    # I

    .prologue
    .line 64
    iput p1, p0, Lorg/apache/poi/hssf/record/CFHeaderRecord;->field_1_numcf:I

    .line 65
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 116
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 118
    .local v0, "buffer":Ljava/lang/StringBuffer;
    const-string/jumbo v2, "[CFHEADER]\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 119
    const-string/jumbo v2, "\t.id\t\t= "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    const/16 v3, 0x1b0

    invoke-static {v3}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string/jumbo v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 120
    const-string/jumbo v2, "\t.numCF\t\t\t= "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {p0}, Lorg/apache/poi/hssf/record/CFHeaderRecord;->getNumberOfConditionalFormats()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string/jumbo v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 121
    const-string/jumbo v2, "\t.needRecalc\t   = "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {p0}, Lorg/apache/poi/hssf/record/CFHeaderRecord;->getNeedRecalculation()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string/jumbo v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 122
    const-string/jumbo v2, "\t.enclosingCellRange= "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {p0}, Lorg/apache/poi/hssf/record/CFHeaderRecord;->getEnclosingCellRange()Lorg/apache/poi/ss/util/CellRangeAddress;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string/jumbo v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 123
    const-string/jumbo v2, "\t.cfranges=["

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 124
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lorg/apache/poi/hssf/record/CFHeaderRecord;->field_4_cell_ranges:Lorg/apache/poi/ss/util/CellRangeAddressList;

    invoke-virtual {v2}, Lorg/apache/poi/ss/util/CellRangeAddressList;->countRanges()I

    move-result v2

    if-lt v1, v2, :cond_0

    .line 128
    const-string/jumbo v2, "]\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 129
    const-string/jumbo v2, "[/CFHEADER]\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 130
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2

    .line 126
    :cond_0
    if-nez v1, :cond_1

    const-string/jumbo v2, ""

    :goto_1
    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    iget-object v3, p0, Lorg/apache/poi/hssf/record/CFHeaderRecord;->field_4_cell_ranges:Lorg/apache/poi/ss/util/CellRangeAddressList;

    invoke-virtual {v3, v1}, Lorg/apache/poi/ss/util/CellRangeAddressList;->getCellRangeAddress(I)Lorg/apache/poi/ss/util/CellRangeAddress;

    move-result-object v3

    invoke-virtual {v3}, Lorg/apache/poi/ss/util/CellRangeAddress;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 124
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 126
    :cond_1
    const-string/jumbo v2, ","

    goto :goto_1
.end method
