.class public final Lorg/apache/poi/hssf/record/EscherAggregate;
.super Lorg/apache/poi/hssf/record/AbstractEscherHolderRecord;
.source "EscherAggregate.java"


# static fields
.field public static final ST_ACCENTBORDERCALLOUT1:S = 0x32s

.field public static final ST_ACCENTBORDERCALLOUT2:S = 0x33s

.field public static final ST_ACCENTBORDERCALLOUT3:S = 0x34s

.field public static final ST_ACCENTBORDERCALLOUT90:S = 0xb5s

.field public static final ST_ACCENTCALLOUT1:S = 0x2cs

.field public static final ST_ACCENTCALLOUT2:S = 0x2ds

.field public static final ST_ACCENTCALLOUT3:S = 0x2es

.field public static final ST_ACCENTCALLOUT90:S = 0xb3s

.field public static final ST_ACTIONBUTTONBACKPREVIOUS:S = 0xc2s

.field public static final ST_ACTIONBUTTONBEGINNING:S = 0xc4s

.field public static final ST_ACTIONBUTTONBLANK:S = 0xbds

.field public static final ST_ACTIONBUTTONDOCUMENT:S = 0xc6s

.field public static final ST_ACTIONBUTTONEND:S = 0xc3s

.field public static final ST_ACTIONBUTTONFORWARDNEXT:S = 0xc1s

.field public static final ST_ACTIONBUTTONHELP:S = 0xbfs

.field public static final ST_ACTIONBUTTONHOME:S = 0xbes

.field public static final ST_ACTIONBUTTONINFORMATION:S = 0xc0s

.field public static final ST_ACTIONBUTTONMOVIE:S = 0xc8s

.field public static final ST_ACTIONBUTTONRETURN:S = 0xc5s

.field public static final ST_ACTIONBUTTONSOUND:S = 0xc7s

.field public static final ST_ARC:S = 0x13s

.field public static final ST_ARROW:S = 0xds

.field public static final ST_BALLOON:S = 0x11s

.field public static final ST_BENTARROW:S = 0x5bs

.field public static final ST_BENTCONNECTOR2:S = 0x21s

.field public static final ST_BENTCONNECTOR3:S = 0x22s

.field public static final ST_BENTCONNECTOR4:S = 0x23s

.field public static final ST_BENTCONNECTOR5:S = 0x24s

.field public static final ST_BENTUPARROW:S = 0x5as

.field public static final ST_BEVEL:S = 0x54s

.field public static final ST_BLOCKARC:S = 0x5fs

.field public static final ST_BORDERCALLOUT1:S = 0x2fs

.field public static final ST_BORDERCALLOUT2:S = 0x30s

.field public static final ST_BORDERCALLOUT3:S = 0x31s

.field public static final ST_BORDERCALLOUT90:S = 0xb4s

.field public static final ST_BRACEPAIR:S = 0xbas

.field public static final ST_BRACKETPAIR:S = 0xb9s

.field public static final ST_CALLOUT1:S = 0x29s

.field public static final ST_CALLOUT2:S = 0x2as

.field public static final ST_CALLOUT3:S = 0x2bs

.field public static final ST_CALLOUT90:S = 0xb2s

.field public static final ST_CAN:S = 0x16s

.field public static final ST_CHEVRON:S = 0x37s

.field public static final ST_CIRCULARARROW:S = 0x63s

.field public static final ST_CLOUDCALLOUT:S = 0x6as

.field public static final ST_CUBE:S = 0x10s

.field public static final ST_CURVEDCONNECTOR2:S = 0x25s

.field public static final ST_CURVEDCONNECTOR3:S = 0x26s

.field public static final ST_CURVEDCONNECTOR4:S = 0x27s

.field public static final ST_CURVEDCONNECTOR5:S = 0x28s

.field public static final ST_CURVEDDOWNARROW:S = 0x69s

.field public static final ST_CURVEDLEFTARROW:S = 0x67s

.field public static final ST_CURVEDRIGHTARROW:S = 0x66s

.field public static final ST_CURVEDUPARROW:S = 0x68s

.field public static final ST_DIAMOND:S = 0x4s

.field public static final ST_DONUT:S = 0x17s

.field public static final ST_DOUBLEWAVE:S = 0xbcs

.field public static final ST_DOWNARROW:S = 0x43s

.field public static final ST_DOWNARROWCALLOUT:S = 0x50s

.field public static final ST_ELLIPSE:S = 0x3s

.field public static final ST_ELLIPSERIBBON:S = 0x6bs

.field public static final ST_ELLIPSERIBBON2:S = 0x6cs

.field public static final ST_FLOWCHARTALTERNATEPROCESS:S = 0xb0s

.field public static final ST_FLOWCHARTCOLLATE:S = 0x7ds

.field public static final ST_FLOWCHARTCONNECTOR:S = 0x78s

.field public static final ST_FLOWCHARTDECISION:S = 0x6es

.field public static final ST_FLOWCHARTDELAY:S = 0x87s

.field public static final ST_FLOWCHARTDISPLAY:S = 0x86s

.field public static final ST_FLOWCHARTDOCUMENT:S = 0x72s

.field public static final ST_FLOWCHARTEXTRACT:S = 0x7fs

.field public static final ST_FLOWCHARTINPUTOUTPUT:S = 0x6fs

.field public static final ST_FLOWCHARTINTERNALSTORAGE:S = 0x71s

.field public static final ST_FLOWCHARTMAGNETICDISK:S = 0x84s

.field public static final ST_FLOWCHARTMAGNETICDRUM:S = 0x85s

.field public static final ST_FLOWCHARTMAGNETICTAPE:S = 0x83s

.field public static final ST_FLOWCHARTMANUALINPUT:S = 0x76s

.field public static final ST_FLOWCHARTMANUALOPERATION:S = 0x77s

.field public static final ST_FLOWCHARTMERGE:S = 0x80s

.field public static final ST_FLOWCHARTMULTIDOCUMENT:S = 0x73s

.field public static final ST_FLOWCHARTOFFLINESTORAGE:S = 0x81s

.field public static final ST_FLOWCHARTOFFPAGECONNECTOR:S = 0xb1s

.field public static final ST_FLOWCHARTONLINESTORAGE:S = 0x82s

.field public static final ST_FLOWCHARTOR:S = 0x7cs

.field public static final ST_FLOWCHARTPREDEFINEDPROCESS:S = 0x70s

.field public static final ST_FLOWCHARTPREPARATION:S = 0x75s

.field public static final ST_FLOWCHARTPROCESS:S = 0x6ds

.field public static final ST_FLOWCHARTPUNCHEDCARD:S = 0x79s

.field public static final ST_FLOWCHARTPUNCHEDTAPE:S = 0x7as

.field public static final ST_FLOWCHARTSORT:S = 0x7es

.field public static final ST_FLOWCHARTSUMMINGJUNCTION:S = 0x7bs

.field public static final ST_FLOWCHARTTERMINATOR:S = 0x74s

.field public static final ST_FOLDEDCORNER:S = 0x41s

.field public static final ST_HEART:S = 0x4as

.field public static final ST_HEXAGON:S = 0x9s

.field public static final ST_HOMEPLATE:S = 0xfs

.field public static final ST_HORIZONTALSCROLL:S = 0x62s

.field public static final ST_HOSTCONTROL:S = 0xc9s

.field public static final ST_IRREGULARSEAL1:S = 0x47s

.field public static final ST_IRREGULARSEAL2:S = 0x48s

.field public static final ST_ISOCELESTRIANGLE:S = 0x5s

.field public static final ST_LEFTARROW:S = 0x42s

.field public static final ST_LEFTARROWCALLOUT:S = 0x4ds

.field public static final ST_LEFTBRACE:S = 0x57s

.field public static final ST_LEFTBRACKET:S = 0x55s

.field public static final ST_LEFTRIGHTARROW:S = 0x45s

.field public static final ST_LEFTRIGHTARROWCALLOUT:S = 0x51s

.field public static final ST_LEFTRIGHTUPARROW:S = 0xb6s

.field public static final ST_LEFTUPARROW:S = 0x59s

.field public static final ST_LIGHTNINGBOLT:S = 0x49s

.field public static final ST_LINE:S = 0x14s

.field public static final ST_MIN:S = 0x0s

.field public static final ST_MOON:S = 0xb8s

.field public static final ST_NIL:S = 0xfffs

.field public static final ST_NOSMOKING:S = 0x39s

.field public static final ST_NOTCHEDCIRCULARARROW:S = 0x64s

.field public static final ST_NOTCHEDRIGHTARROW:S = 0x5es

.field public static final ST_NOT_PRIMATIVE:S = 0x0s

.field public static final ST_OCTAGON:S = 0xas

.field public static final ST_PARALLELOGRAM:S = 0x7s

.field public static final ST_PENTAGON:S = 0x38s

.field public static final ST_PICTUREFRAME:S = 0x4bs

.field public static final ST_PLAQUE:S = 0x15s

.field public static final ST_PLUS:S = 0xbs

.field public static final ST_QUADARROW:S = 0x4cs

.field public static final ST_QUADARROWCALLOUT:S = 0x53s

.field public static final ST_RECTANGLE:S = 0x1s

.field public static final ST_RIBBON:S = 0x35s

.field public static final ST_RIBBON2:S = 0x36s

.field public static final ST_RIGHTARROWCALLOUT:S = 0x4es

.field public static final ST_RIGHTBRACE:S = 0x58s

.field public static final ST_RIGHTBRACKET:S = 0x56s

.field public static final ST_RIGHTTRIANGLE:S = 0x6s

.field public static final ST_ROUNDRECTANGLE:S = 0x2s

.field public static final ST_SEAL:S = 0x12s

.field public static final ST_SEAL16:S = 0x3bs

.field public static final ST_SEAL24:S = 0x5cs

.field public static final ST_SEAL32:S = 0x3cs

.field public static final ST_SEAL4:S = 0xbbs

.field public static final ST_SEAL8:S = 0x3as

.field public static final ST_SMILEYFACE:S = 0x60s

.field public static final ST_STAR:S = 0xcs

.field public static final ST_STRAIGHTCONNECTOR1:S = 0x20s

.field public static final ST_STRIPEDRIGHTARROW:S = 0x5ds

.field public static final ST_SUN:S = 0xb7s

.field public static final ST_TEXTARCHDOWNCURVE:S = 0x91s

.field public static final ST_TEXTARCHDOWNPOUR:S = 0x95s

.field public static final ST_TEXTARCHUPCURVE:S = 0x90s

.field public static final ST_TEXTARCHUPPOUR:S = 0x94s

.field public static final ST_TEXTBOX:S = 0xcas

.field public static final ST_TEXTBUTTONCURVE:S = 0x93s

.field public static final ST_TEXTBUTTONPOUR:S = 0x97s

.field public static final ST_TEXTCANDOWN:S = 0xafs

.field public static final ST_TEXTCANUP:S = 0xaes

.field public static final ST_TEXTCASCADEDOWN:S = 0x9bs

.field public static final ST_TEXTCASCADEUP:S = 0x9as

.field public static final ST_TEXTCHEVRON:S = 0x8cs

.field public static final ST_TEXTCHEVRONINVERTED:S = 0x8ds

.field public static final ST_TEXTCIRCLECURVE:S = 0x92s

.field public static final ST_TEXTCIRCLEPOUR:S = 0x96s

.field public static final ST_TEXTCURVE:S = 0x1bs

.field public static final ST_TEXTCURVEDOWN:S = 0x99s

.field public static final ST_TEXTCURVEUP:S = 0x98s

.field public static final ST_TEXTDEFLATE:S = 0xa1s

.field public static final ST_TEXTDEFLATEBOTTOM:S = 0xa3s

.field public static final ST_TEXTDEFLATEINFLATE:S = 0xa6s

.field public static final ST_TEXTDEFLATEINFLATEDEFLATE:S = 0xa7s

.field public static final ST_TEXTDEFLATETOP:S = 0xa5s

.field public static final ST_TEXTFADEDOWN:S = 0xabs

.field public static final ST_TEXTFADELEFT:S = 0xa9s

.field public static final ST_TEXTFADERIGHT:S = 0xa8s

.field public static final ST_TEXTFADEUP:S = 0xaas

.field public static final ST_TEXTHEXAGON:S = 0x1as

.field public static final ST_TEXTINFLATE:S = 0xa0s

.field public static final ST_TEXTINFLATEBOTTOM:S = 0xa2s

.field public static final ST_TEXTINFLATETOP:S = 0xa4s

.field public static final ST_TEXTOCTAGON:S = 0x19s

.field public static final ST_TEXTONCURVE:S = 0x1es

.field public static final ST_TEXTONRING:S = 0x1fs

.field public static final ST_TEXTPLAINTEXT:S = 0x88s

.field public static final ST_TEXTRING:S = 0x1ds

.field public static final ST_TEXTRINGINSIDE:S = 0x8es

.field public static final ST_TEXTRINGOUTSIDE:S = 0x8fs

.field public static final ST_TEXTSIMPLE:S = 0x18s

.field public static final ST_TEXTSLANTDOWN:S = 0xads

.field public static final ST_TEXTSLANTUP:S = 0xacs

.field public static final ST_TEXTSTOP:S = 0x89s

.field public static final ST_TEXTTRIANGLE:S = 0x8as

.field public static final ST_TEXTTRIANGLEINVERTED:S = 0x8bs

.field public static final ST_TEXTWAVE:S = 0x1cs

.field public static final ST_TEXTWAVE1:S = 0x9cs

.field public static final ST_TEXTWAVE2:S = 0x9ds

.field public static final ST_TEXTWAVE3:S = 0x9es

.field public static final ST_TEXTWAVE4:S = 0x9fs

.field public static final ST_THICKARROW:S = 0xes

.field public static final ST_TRAPEZOID:S = 0x8s

.field public static final ST_UPARROW:S = 0x44s

.field public static final ST_UPARROWCALLOUT:S = 0x4fs

.field public static final ST_UPDOWNARROW:S = 0x46s

.field public static final ST_UPDOWNARROWCALLOUT:S = 0x52s

.field public static final ST_UTURNARROW:S = 0x65s

.field public static final ST_VERTICALSCROLL:S = 0x61s

.field public static final ST_WAVE:S = 0x40s

.field public static final ST_WEDGEELLIPSECALLOUT:S = 0x3fs

.field public static final ST_WEDGERECTCALLOUT:S = 0x3ds

.field public static final ST_WEDGERRECTCALLOUT:S = 0x3es

.field private static log:Lorg/apache/poi/util/POILogger; = null

.field public static final sid:S = 0x2694s


# instance fields
.field private mShapeIDTextObjMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lorg/apache/poi/hssf/record/Record;",
            ">;"
        }
    .end annotation
.end field

.field private final shapeToObj:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lorg/apache/poi/ddf/EscherRecord;",
            "Lorg/apache/poi/hssf/record/Record;",
            ">;"
        }
    .end annotation
.end field

.field private tailRec:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lorg/apache/poi/hssf/record/NoteRecord;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 84
    const-class v0, Lorg/apache/poi/hssf/record/EscherAggregate;

    invoke-static {v0}, Lorg/apache/poi/util/POILogFactory;->getLogger(Ljava/lang/Class;)Lorg/apache/poi/util/POILogger;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hssf/record/EscherAggregate;->log:Lorg/apache/poi/util/POILogger;

    .line 290
    return-void
.end method

.method public constructor <init>(Z)V
    .locals 1
    .param p1, "createDefaultTree"    # Z

    .prologue
    .line 307
    invoke-direct {p0}, Lorg/apache/poi/hssf/record/AbstractEscherHolderRecord;-><init>()V

    .line 295
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/hssf/record/EscherAggregate;->shapeToObj:Ljava/util/Map;

    .line 300
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/hssf/record/EscherAggregate;->tailRec:Ljava/util/Map;

    .line 383
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/hssf/record/EscherAggregate;->mShapeIDTextObjMap:Ljava/util/Map;

    .line 308
    if-eqz p1, :cond_0

    .line 309
    invoke-direct {p0}, Lorg/apache/poi/hssf/record/EscherAggregate;->buildBaseTree()V

    .line 311
    :cond_0
    return-void
.end method

.method private buildBaseTree()V
    .locals 11

    .prologue
    const/4 v10, 0x2

    const/16 v9, 0xf

    const/4 v8, 0x0

    .line 843
    new-instance v1, Lorg/apache/poi/ddf/EscherContainerRecord;

    invoke-direct {v1}, Lorg/apache/poi/ddf/EscherContainerRecord;-><init>()V

    .line 844
    .local v1, "dgContainer":Lorg/apache/poi/ddf/EscherContainerRecord;
    new-instance v6, Lorg/apache/poi/ddf/EscherContainerRecord;

    invoke-direct {v6}, Lorg/apache/poi/ddf/EscherContainerRecord;-><init>()V

    .line 845
    .local v6, "spgrContainer":Lorg/apache/poi/ddf/EscherContainerRecord;
    new-instance v4, Lorg/apache/poi/ddf/EscherContainerRecord;

    invoke-direct {v4}, Lorg/apache/poi/ddf/EscherContainerRecord;-><init>()V

    .line 846
    .local v4, "spContainer1":Lorg/apache/poi/ddf/EscherContainerRecord;
    new-instance v5, Lorg/apache/poi/ddf/EscherSpgrRecord;

    invoke-direct {v5}, Lorg/apache/poi/ddf/EscherSpgrRecord;-><init>()V

    .line 847
    .local v5, "spgr":Lorg/apache/poi/ddf/EscherSpgrRecord;
    new-instance v3, Lorg/apache/poi/ddf/EscherSpRecord;

    invoke-direct {v3}, Lorg/apache/poi/ddf/EscherSpRecord;-><init>()V

    .line 848
    .local v3, "sp1":Lorg/apache/poi/ddf/EscherSpRecord;
    const/16 v7, -0xffe

    invoke-virtual {v1, v7}, Lorg/apache/poi/ddf/EscherContainerRecord;->setRecordId(S)V

    .line 849
    invoke-virtual {v1, v9}, Lorg/apache/poi/ddf/EscherContainerRecord;->setOptions(S)V

    .line 850
    new-instance v0, Lorg/apache/poi/ddf/EscherDgRecord;

    invoke-direct {v0}, Lorg/apache/poi/ddf/EscherDgRecord;-><init>()V

    .line 851
    .local v0, "dg":Lorg/apache/poi/ddf/EscherDgRecord;
    const/16 v7, -0xff8

    invoke-virtual {v0, v7}, Lorg/apache/poi/ddf/EscherDgRecord;->setRecordId(S)V

    .line 852
    const/4 v2, 0x1

    .line 853
    .local v2, "dgId":S
    const/16 v7, 0x10

    int-to-short v7, v7

    invoke-virtual {v0, v7}, Lorg/apache/poi/ddf/EscherDgRecord;->setOptions(S)V

    .line 854
    invoke-virtual {v0, v8}, Lorg/apache/poi/ddf/EscherDgRecord;->setNumShapes(I)V

    .line 855
    const/16 v7, 0x400

    invoke-virtual {v0, v7}, Lorg/apache/poi/ddf/EscherDgRecord;->setLastMSOSPID(I)V

    .line 856
    const/16 v7, -0xffd

    invoke-virtual {v6, v7}, Lorg/apache/poi/ddf/EscherContainerRecord;->setRecordId(S)V

    .line 857
    invoke-virtual {v6, v9}, Lorg/apache/poi/ddf/EscherContainerRecord;->setOptions(S)V

    .line 858
    const/16 v7, -0xffc

    invoke-virtual {v4, v7}, Lorg/apache/poi/ddf/EscherContainerRecord;->setRecordId(S)V

    .line 859
    invoke-virtual {v4, v9}, Lorg/apache/poi/ddf/EscherContainerRecord;->setOptions(S)V

    .line 860
    const/16 v7, -0xff7

    invoke-virtual {v5, v7}, Lorg/apache/poi/ddf/EscherSpgrRecord;->setRecordId(S)V

    .line 861
    const/4 v7, 0x1

    invoke-virtual {v5, v7}, Lorg/apache/poi/ddf/EscherSpgrRecord;->setOptions(S)V

    .line 862
    invoke-virtual {v5, v8}, Lorg/apache/poi/ddf/EscherSpgrRecord;->setRectX1(I)V

    .line 863
    invoke-virtual {v5, v8}, Lorg/apache/poi/ddf/EscherSpgrRecord;->setRectY1(I)V

    .line 864
    const/16 v7, 0x3ff

    invoke-virtual {v5, v7}, Lorg/apache/poi/ddf/EscherSpgrRecord;->setRectX2(I)V

    .line 865
    const/16 v7, 0xff

    invoke-virtual {v5, v7}, Lorg/apache/poi/ddf/EscherSpgrRecord;->setRectY2(I)V

    .line 866
    const/16 v7, -0xff6

    invoke-virtual {v3, v7}, Lorg/apache/poi/ddf/EscherSpRecord;->setRecordId(S)V

    .line 868
    invoke-virtual {v3, v10}, Lorg/apache/poi/ddf/EscherSpRecord;->setOptions(S)V

    .line 869
    invoke-virtual {v3, v10}, Lorg/apache/poi/ddf/EscherSpRecord;->setVersion(S)V

    .line 870
    const/4 v7, -0x1

    invoke-virtual {v3, v7}, Lorg/apache/poi/ddf/EscherSpRecord;->setShapeId(I)V

    .line 871
    const/4 v7, 0x5

    invoke-virtual {v3, v7}, Lorg/apache/poi/ddf/EscherSpRecord;->setFlags(I)V

    .line 872
    invoke-virtual {v1, v0}, Lorg/apache/poi/ddf/EscherContainerRecord;->addChildRecord(Lorg/apache/poi/ddf/EscherRecord;)V

    .line 873
    invoke-virtual {v1, v6}, Lorg/apache/poi/ddf/EscherContainerRecord;->addChildRecord(Lorg/apache/poi/ddf/EscherRecord;)V

    .line 874
    invoke-virtual {v6, v4}, Lorg/apache/poi/ddf/EscherContainerRecord;->addChildRecord(Lorg/apache/poi/ddf/EscherRecord;)V

    .line 875
    invoke-virtual {v4, v5}, Lorg/apache/poi/ddf/EscherContainerRecord;->addChildRecord(Lorg/apache/poi/ddf/EscherRecord;)V

    .line 876
    invoke-virtual {v4, v3}, Lorg/apache/poi/ddf/EscherContainerRecord;->addChildRecord(Lorg/apache/poi/ddf/EscherRecord;)V

    .line 877
    invoke-virtual {p0, v1}, Lorg/apache/poi/hssf/record/EscherAggregate;->addEscherRecord(Lorg/apache/poi/ddf/EscherRecord;)Z

    .line 878
    return-void
.end method

.method public static createAggregate(Ljava/util/List;I)Lorg/apache/poi/hssf/record/EscherAggregate;
    .locals 26
    .param p1, "locFirstDrawingRecord"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/apache/poi/hssf/record/RecordBase;",
            ">;I)",
            "Lorg/apache/poi/hssf/record/EscherAggregate;"
        }
    .end annotation

    .prologue
    .line 448
    .local p0, "records":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/hssf/record/RecordBase;>;"
    new-instance v18, Ljava/util/ArrayList;

    invoke-direct/range {v18 .. v18}, Ljava/util/ArrayList;-><init>()V

    .line 449
    .local v18, "shapeRecords":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/ddf/EscherRecord;>;"
    new-instance v15, Lorg/apache/poi/hssf/record/EscherAggregate$1;

    move-object/from16 v0, v18

    invoke-direct {v15, v0}, Lorg/apache/poi/hssf/record/EscherAggregate$1;-><init>(Ljava/util/List;)V

    .line 460
    .local v15, "recordFactory":Lorg/apache/poi/ddf/EscherRecordFactory;
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 461
    .local v9, "escherSpRecordList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/poi/ddf/EscherSpRecord;>;"
    new-instance v23, Ljava/util/ArrayList;

    invoke-direct/range {v23 .. v23}, Ljava/util/ArrayList;-><init>()V

    .line 465
    .local v23, "textObjects":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/poi/hssf/record/Record;>;"
    new-instance v3, Lorg/apache/poi/hssf/record/EscherAggregate;

    const/16 v24, 0x0

    move/from16 v0, v24

    invoke-direct {v3, v0}, Lorg/apache/poi/hssf/record/EscherAggregate;-><init>(Z)V

    .line 466
    .local v3, "agg":Lorg/apache/poi/hssf/record/EscherAggregate;
    move/from16 v11, p1

    .line 467
    .local v11, "loc":I
    move/from16 v10, p1

    .line 468
    .local v10, "initialval":I
    move/from16 v21, p1

    .line 469
    .local v21, "temploc":I
    const/4 v6, 0x0

    .line 471
    .local v6, "count":I
    :goto_0
    add-int/lit8 v24, v11, 0x2

    invoke-interface/range {p0 .. p0}, Ljava/util/List;->size()I

    move-result v25

    move/from16 v0, v24

    move/from16 v1, v25

    if-gt v0, v1, :cond_0

    .line 472
    const/16 v24, -0x1

    move/from16 v0, v24

    if-ne v11, v0, :cond_1

    .line 605
    :cond_0
    move-object/from16 v0, p0

    invoke-interface {v0, v10, v3}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 607
    return-object v3

    .line 476
    :cond_1
    invoke-virtual/range {v23 .. v23}, Ljava/util/ArrayList;->clear()V

    .line 478
    new-instance v4, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v4}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 479
    .local v4, "buffer":Ljava/io/ByteArrayOutputStream;
    :goto_1
    add-int/lit8 v24, v11, 0x1

    invoke-interface/range {p0 .. p0}, Ljava/util/List;->size()I

    move-result v25

    move/from16 v0, v24

    move/from16 v1, v25

    if-ge v0, v1, :cond_3

    .line 480
    move-object/from16 v0, p0

    invoke-static {v0, v11}, Lorg/apache/poi/hssf/record/EscherAggregate;->sid(Ljava/util/List;I)S

    move-result v24

    const/16 v25, 0xec

    move/from16 v0, v24

    move/from16 v1, v25

    if-eq v0, v1, :cond_2

    move-object/from16 v0, p0

    invoke-static {v0, v11}, Lorg/apache/poi/hssf/record/EscherAggregate;->sid(Ljava/util/List;I)S

    move-result v24

    const/16 v25, 0x3c

    move/from16 v0, v24

    move/from16 v1, v25

    if-ne v0, v1, :cond_3

    :cond_2
    add-int/lit8 v24, v11, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-static {v0, v1}, Lorg/apache/poi/hssf/record/EscherAggregate;->isObjectRecord(Ljava/util/List;I)Z

    move-result v24

    if-nez v24, :cond_9

    .line 523
    :cond_3
    const/4 v13, 0x0

    .line 524
    .local v13, "pos":I
    :goto_2
    invoke-virtual {v4}, Ljava/io/ByteArrayOutputStream;->size()I

    move-result v24

    move/from16 v0, v24

    if-lt v13, v0, :cond_d

    .line 556
    add-int/lit8 v11, p1, 0x1

    .line 557
    const/16 v16, 0x0

    .line 558
    .local v16, "shapeIndex":I
    :goto_3
    invoke-interface/range {p0 .. p0}, Ljava/util/List;->size()I

    move-result v24

    move/from16 v0, v24

    if-ge v11, v0, :cond_5

    .line 559
    move-object/from16 v0, p0

    invoke-static {v0, v11}, Lorg/apache/poi/hssf/record/EscherAggregate;->sid(Ljava/util/List;I)S

    move-result v24

    const/16 v25, 0xec

    move/from16 v0, v24

    move/from16 v1, v25

    if-eq v0, v1, :cond_4

    move-object/from16 v0, p0

    invoke-static {v0, v11}, Lorg/apache/poi/hssf/record/EscherAggregate;->sid(Ljava/util/List;I)S

    move-result v24

    const/16 v25, 0x3c

    move/from16 v0, v24

    move/from16 v1, v25

    if-ne v0, v1, :cond_5

    :cond_4
    add-int/lit8 v24, v11, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-static {v0, v1}, Lorg/apache/poi/hssf/record/EscherAggregate;->isObjectRecord(Ljava/util/List;I)Z

    move-result v24

    if-nez v24, :cond_11

    .line 570
    :cond_5
    add-int/lit8 v24, v11, 0x1

    invoke-interface/range {p0 .. p0}, Ljava/util/List;->size()I

    move-result v25

    move/from16 v0, v24

    move/from16 v1, v25

    if-ge v0, v1, :cond_7

    .line 571
    move-object/from16 v0, p0

    invoke-static {v0, v11}, Lorg/apache/poi/hssf/record/EscherAggregate;->sid(Ljava/util/List;I)S

    move-result v24

    const/16 v25, 0xec

    move/from16 v0, v24

    move/from16 v1, v25

    if-eq v0, v1, :cond_6

    move-object/from16 v0, p0

    invoke-static {v0, v11}, Lorg/apache/poi/hssf/record/EscherAggregate;->sid(Ljava/util/List;I)S

    move-result v24

    const/16 v25, 0x3c

    move/from16 v0, v24

    move/from16 v1, v25

    if-ne v0, v1, :cond_7

    .line 572
    :cond_6
    add-int/lit8 v24, v11, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-static {v0, v1}, Lorg/apache/poi/hssf/record/EscherAggregate;->isObjectRecord(Ljava/util/List;I)Z

    move-result v24

    if-nez v24, :cond_7

    .line 575
    add-int/lit8 v11, v11, 0x1

    .line 580
    :cond_7
    :goto_4
    invoke-interface/range {p0 .. p0}, Ljava/util/List;->size()I

    move-result v24

    move/from16 v0, v24

    if-lt v11, v0, :cond_13

    .line 591
    :cond_8
    move/from16 v21, v11

    .line 592
    const/16 v24, 0xec

    move-object/from16 v0, p0

    move/from16 v1, v24

    move/from16 v2, v21

    invoke-static {v0, v1, v2}, Lorg/apache/poi/hssf/record/EscherAggregate;->findRecordLocAfterNBySid(Ljava/util/List;SI)I

    move-result v11

    .line 593
    move/from16 p1, v11

    goto/16 :goto_0

    .line 484
    .end local v13    # "pos":I
    .end local v16    # "shapeIndex":I
    :cond_9
    add-int/lit8 v24, v11, 0x1

    :try_start_0
    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-static {v0, v1}, Lorg/apache/poi/hssf/record/EscherAggregate;->isTextObjectRecord(Ljava/util/List;I)Z

    move-result v24

    if-eqz v24, :cond_a

    .line 486
    add-int/lit8 v24, v11, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lorg/apache/poi/hssf/record/Record;

    .line 487
    .local v12, "objRecord":Lorg/apache/poi/hssf/record/Record;
    move-object/from16 v0, v23

    invoke-virtual {v0, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 489
    const/16 v24, 0x0

    invoke-virtual/range {v23 .. v24}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Lorg/apache/poi/hssf/record/TextObjectRecord;

    .line 491
    .local v22, "textObjRecord":Lorg/apache/poi/hssf/record/TextObjectRecord;
    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v24

    if-lez v24, :cond_a

    .line 492
    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v24

    add-int/lit8 v24, v24, -0x1

    move/from16 v0, v24

    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lorg/apache/poi/ddf/EscherSpRecord;

    .line 493
    .local v19, "spTempRecord":Lorg/apache/poi/ddf/EscherSpRecord;
    if-eqz v19, :cond_a

    if-eqz v22, :cond_a

    .line 498
    iget-object v0, v3, Lorg/apache/poi/hssf/record/EscherAggregate;->mShapeIDTextObjMap:Ljava/util/Map;

    move-object/from16 v24, v0

    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/ddf/EscherSpRecord;->getShapeId()I

    move-result v25

    invoke-static/range {v25 .. v25}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v25

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    move-object/from16 v2, v22

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 506
    .end local v12    # "objRecord":Lorg/apache/poi/hssf/record/Record;
    .end local v19    # "spTempRecord":Lorg/apache/poi/ddf/EscherSpRecord;
    .end local v22    # "textObjRecord":Lorg/apache/poi/hssf/record/TextObjectRecord;
    :cond_a
    move-object/from16 v0, p0

    invoke-static {v0, v11}, Lorg/apache/poi/hssf/record/EscherAggregate;->sid(Ljava/util/List;I)S

    move-result v24

    const/16 v25, 0xec

    move/from16 v0, v24

    move/from16 v1, v25

    if-eq v0, v1, :cond_b

    move-object/from16 v0, p0

    invoke-static {v0, v11}, Lorg/apache/poi/hssf/record/EscherAggregate;->sid(Ljava/util/List;I)S

    move-result v24

    const/16 v25, 0x3c

    move/from16 v0, v24

    move/from16 v1, v25

    if-eq v0, v1, :cond_b

    .line 507
    add-int/lit8 v11, v11, 0x1

    goto/16 :goto_1

    .line 510
    :cond_b
    move-object/from16 v0, p0

    invoke-static {v0, v11}, Lorg/apache/poi/hssf/record/EscherAggregate;->sid(Ljava/util/List;I)S

    move-result v24

    const/16 v25, 0xec

    move/from16 v0, v24

    move/from16 v1, v25

    if-ne v0, v1, :cond_c

    .line 511
    move-object/from16 v0, p0

    invoke-interface {v0, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Lorg/apache/poi/hssf/record/DrawingRecord;

    invoke-virtual/range {v24 .. v24}, Lorg/apache/poi/hssf/record/DrawingRecord;->getRecordData()[B

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v4, v0}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 518
    :goto_5
    add-int/lit8 v11, v11, 0x1

    goto/16 :goto_1

    .line 513
    :cond_c
    move-object/from16 v0, p0

    invoke-interface {v0, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Lorg/apache/poi/hssf/record/ContinueRecord;

    invoke-virtual/range {v24 .. v24}, Lorg/apache/poi/hssf/record/ContinueRecord;->getData()[B

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v4, v0}, Ljava/io/ByteArrayOutputStream;->write([B)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_5

    .line 515
    :catch_0
    move-exception v7

    .line 516
    .local v7, "e":Ljava/io/IOException;
    new-instance v24, Ljava/lang/RuntimeException;

    const-string/jumbo v25, "Couldn\'t get data from drawing/continue records"

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    invoke-direct {v0, v1, v7}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v24

    .line 525
    .end local v7    # "e":Ljava/io/IOException;
    .restart local v13    # "pos":I
    :cond_d
    invoke-virtual {v4}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v24

    move-object/from16 v0, v24

    invoke-interface {v15, v0, v13}, Lorg/apache/poi/ddf/EscherRecordFactory;->createRecord([BI)Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v14

    .line 526
    .local v14, "r":Lorg/apache/poi/ddf/EscherRecord;
    invoke-virtual {v4}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v14, v0, v13, v15}, Lorg/apache/poi/ddf/EscherRecord;->fillFields([BILorg/apache/poi/ddf/EscherRecordFactory;)I

    move-result v5

    .line 527
    .local v5, "bytesRead":I
    invoke-virtual {v3, v14}, Lorg/apache/poi/hssf/record/EscherAggregate;->addEscherRecord(Lorg/apache/poi/ddf/EscherRecord;)Z

    .line 529
    instance-of v0, v14, Lorg/apache/poi/ddf/EscherContainerRecord;

    move/from16 v24, v0

    if-eqz v24, :cond_10

    move-object v8, v14

    .line 530
    check-cast v8, Lorg/apache/poi/ddf/EscherContainerRecord;

    .line 533
    .local v8, "escherContainer":Lorg/apache/poi/ddf/EscherContainerRecord;
    const/16 v24, -0xff6

    move/from16 v0, v24

    invoke-static {v8, v0}, Lorg/apache/poi/hssf/record/EscherAggregate;->getEscherRecordList(Lorg/apache/poi/ddf/EscherContainerRecord;I)Ljava/util/List;

    move-result-object v20

    .line 536
    .local v20, "tempescherSpRecordList":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/ddf/EscherSpRecord;>;"
    if-eqz v20, :cond_10

    .line 538
    if-nez v6, :cond_e

    .line 540
    const/16 v24, 0x0

    move-object/from16 v0, v20

    move/from16 v1, v24

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 543
    :cond_e
    invoke-interface/range {v20 .. v20}, Ljava/util/List;->size()I

    move-result v24

    if-lez v24, :cond_f

    .line 544
    const/16 v24, 0x0

    move-object/from16 v0, v20

    move/from16 v1, v24

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lorg/apache/poi/ddf/EscherSpRecord;

    .line 545
    .restart local v19    # "spTempRecord":Lorg/apache/poi/ddf/EscherSpRecord;
    move-object/from16 v0, v19

    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 548
    .end local v19    # "spTempRecord":Lorg/apache/poi/ddf/EscherSpRecord;
    :cond_f
    add-int/lit8 v6, v6, 0x1

    .line 552
    .end local v8    # "escherContainer":Lorg/apache/poi/ddf/EscherContainerRecord;
    .end local v20    # "tempescherSpRecordList":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/ddf/EscherSpRecord;>;"
    :cond_10
    add-int/2addr v13, v5

    goto/16 :goto_2

    .line 560
    .end local v5    # "bytesRead":I
    .end local v14    # "r":Lorg/apache/poi/ddf/EscherRecord;
    .restart local v16    # "shapeIndex":I
    :cond_11
    move-object/from16 v0, p0

    invoke-static {v0, v11}, Lorg/apache/poi/hssf/record/EscherAggregate;->isObjectRecord(Ljava/util/List;I)Z

    move-result v24

    if-nez v24, :cond_12

    .line 561
    add-int/lit8 v11, v11, 0x1

    .line 562
    goto/16 :goto_3

    .line 564
    :cond_12
    move-object/from16 v0, p0

    invoke-interface {v0, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lorg/apache/poi/hssf/record/Record;

    .line 565
    .restart local v12    # "objRecord":Lorg/apache/poi/hssf/record/Record;
    iget-object v0, v3, Lorg/apache/poi/hssf/record/EscherAggregate;->shapeToObj:Ljava/util/Map;

    move-object/from16 v25, v0

    add-int/lit8 v17, v16, 0x1

    .end local v16    # "shapeIndex":I
    .local v17, "shapeIndex":I
    move-object/from16 v0, v18

    move/from16 v1, v16

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Lorg/apache/poi/ddf/EscherRecord;

    move-object/from16 v0, v25

    move-object/from16 v1, v24

    invoke-interface {v0, v1, v12}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 566
    add-int/lit8 v11, v11, 0x1

    move/from16 v16, v17

    .end local v17    # "shapeIndex":I
    .restart local v16    # "shapeIndex":I
    goto/16 :goto_3

    .line 581
    .end local v12    # "objRecord":Lorg/apache/poi/hssf/record/Record;
    :cond_13
    move-object/from16 v0, p0

    invoke-static {v0, v11}, Lorg/apache/poi/hssf/record/EscherAggregate;->sid(Ljava/util/List;I)S

    move-result v24

    const/16 v25, 0x1c

    move/from16 v0, v24

    move/from16 v1, v25

    if-ne v0, v1, :cond_8

    .line 582
    move-object/from16 v0, p0

    invoke-interface {v0, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lorg/apache/poi/hssf/record/NoteRecord;

    .line 583
    .local v14, "r":Lorg/apache/poi/hssf/record/NoteRecord;
    iget-object v0, v3, Lorg/apache/poi/hssf/record/EscherAggregate;->tailRec:Ljava/util/Map;

    move-object/from16 v24, v0

    invoke-virtual {v14}, Lorg/apache/poi/hssf/record/NoteRecord;->getShapeId()I

    move-result v25

    invoke-static/range {v25 .. v25}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v25

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    invoke-interface {v0, v1, v14}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 587
    add-int/lit8 v11, v11, 0x1

    goto/16 :goto_4
.end method

.method public static findRecordLocAfterNBySid(Ljava/util/List;SI)I
    .locals 6
    .param p0, "records"    # Ljava/util/List;
    .param p1, "sid"    # S
    .param p2, "n"    # I

    .prologue
    .line 366
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v1

    .line 368
    .local v1, "max":I
    move v0, p2

    .local v0, "i":I
    :goto_0
    if-lt v0, v1, :cond_1

    .line 378
    const/4 v0, -0x1

    .end local v0    # "i":I
    :cond_0
    :goto_1
    return v0

    .line 369
    .restart local v0    # "i":I
    :cond_1
    invoke-interface {p0, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    .line 370
    .local v2, "rb":Ljava/lang/Object;
    instance-of v4, v2, Lorg/apache/poi/hssf/record/Record;

    if-nez v4, :cond_3

    .line 368
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    move-object v3, v2

    .line 373
    check-cast v3, Lorg/apache/poi/hssf/record/Record;

    .line 374
    .local v3, "record":Lorg/apache/poi/hssf/record/Record;
    invoke-virtual {v3}, Lorg/apache/poi/hssf/record/Record;->getSid()S

    move-result v4

    if-eq v4, p1, :cond_0

    invoke-virtual {v3}, Lorg/apache/poi/hssf/record/Record;->getSid()S

    move-result v4

    const/16 v5, 0x3c

    if-ne v4, v5, :cond_2

    goto :goto_1
.end method

.method private static getEscherRecordList(Lorg/apache/poi/ddf/EscherContainerRecord;I)Ljava/util/List;
    .locals 8
    .param p0, "records"    # Lorg/apache/poi/ddf/EscherContainerRecord;
    .param p1, "recordId"    # I

    .prologue
    .line 406
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 408
    .local v6, "list":Ljava/util/List;
    invoke-virtual {p0}, Lorg/apache/poi/ddf/EscherContainerRecord;->getChildIterator()Ljava/util/Iterator;

    move-result-object v5

    .local v5, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/poi/ddf/EscherRecord;>;"
    :cond_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-nez v7, :cond_1

    .line 432
    return-object v6

    .line 409
    :cond_1
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/poi/ddf/EscherRecord;

    .line 410
    .local v2, "escherRecord":Lorg/apache/poi/ddf/EscherRecord;
    invoke-virtual {v2}, Lorg/apache/poi/ddf/EscherRecord;->getRecordId()S

    move-result v7

    if-ne v7, p1, :cond_2

    .line 411
    invoke-interface {v6, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 413
    :cond_2
    invoke-virtual {v2}, Lorg/apache/poi/ddf/EscherRecord;->isContainerRecord()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 414
    invoke-virtual {v2}, Lorg/apache/poi/ddf/EscherRecord;->getChildRecords()Ljava/util/List;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "childIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/poi/ddf/EscherRecord;>;"
    :cond_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 416
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/ddf/EscherRecord;

    .line 417
    .local v0, "childEscherRecord":Lorg/apache/poi/ddf/EscherRecord;
    invoke-virtual {v0}, Lorg/apache/poi/ddf/EscherRecord;->getRecordId()S

    move-result v7

    if-ne v7, p1, :cond_4

    .line 418
    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 420
    :cond_4
    invoke-virtual {v0}, Lorg/apache/poi/ddf/EscherRecord;->isContainerRecord()Z

    move-result v7

    if-eqz v7, :cond_3

    .line 421
    invoke-virtual {v0}, Lorg/apache/poi/ddf/EscherRecord;->getChildRecords()Ljava/util/List;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/poi/ddf/EscherRecord;>;"
    :cond_5
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_3

    .line 423
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/poi/ddf/EscherRecord;

    .line 424
    .local v3, "found":Lorg/apache/poi/ddf/EscherRecord;
    invoke-virtual {v3}, Lorg/apache/poi/ddf/EscherRecord;->getRecordId()S

    move-result v7

    if-ne v7, p1, :cond_5

    .line 425
    invoke-interface {v6, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private getEscherRecordSize(Ljava/util/List;)I
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/apache/poi/ddf/EscherRecord;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 735
    .local p1, "records":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/ddf/EscherRecord;>;"
    const/4 v1, 0x0

    .line 736
    .local v1, "size":I
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_0

    .line 739
    return v1

    .line 736
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/ddf/EscherRecord;

    .line 737
    .local v0, "record":Lorg/apache/poi/ddf/EscherRecord;
    invoke-virtual {v0}, Lorg/apache/poi/ddf/EscherRecord;->getRecordSize()I

    move-result v3

    add-int/2addr v1, v3

    goto :goto_0
.end method

.method private static isObjectRecord(Ljava/util/List;I)Z
    .locals 2
    .param p1, "loc"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/apache/poi/hssf/record/RecordBase;",
            ">;I)Z"
        }
    .end annotation

    .prologue
    .line 827
    .local p0, "records":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/hssf/record/RecordBase;>;"
    invoke-static {p0, p1}, Lorg/apache/poi/hssf/record/EscherAggregate;->sid(Ljava/util/List;I)S

    move-result v0

    const/16 v1, 0x5d

    if-eq v0, v1, :cond_0

    invoke-static {p0, p1}, Lorg/apache/poi/hssf/record/EscherAggregate;->sid(Ljava/util/List;I)S

    move-result v0

    const/16 v1, 0x1b6

    if-eq v0, v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private static isTextObjectRecord(Ljava/util/List;I)Z
    .locals 2
    .param p0, "records"    # Ljava/util/List;
    .param p1, "loc"    # I

    .prologue
    .line 399
    invoke-static {p0, p1}, Lorg/apache/poi/hssf/record/EscherAggregate;->sid(Ljava/util/List;I)S

    move-result v0

    const/16 v1, 0x1b6

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static sid(Ljava/util/List;I)S
    .locals 1
    .param p1, "loc"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/apache/poi/hssf/record/RecordBase;",
            ">;I)S"
        }
    .end annotation

    .prologue
    .line 930
    .local p0, "records":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/hssf/record/RecordBase;>;"
    invoke-interface {p0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hssf/record/Record;

    invoke-virtual {v0}, Lorg/apache/poi/hssf/record/Record;->getSid()S

    move-result v0

    return v0
.end method

.method private writeDataIntoDrawingRecord([BII[BI)I
    .locals 7
    .param p1, "drawingData"    # [B
    .param p2, "writtenEscherBytes"    # I
    .param p3, "pos"    # I
    .param p4, "data"    # [B
    .param p5, "i"    # I

    .prologue
    const/4 v6, 0x0

    const/16 v5, 0x2020

    .line 700
    const/4 v3, 0x0

    .line 702
    .local v3, "temp":I
    array-length v4, p1

    add-int/2addr v4, p2

    if-le v4, v5, :cond_2

    const/4 v4, 0x1

    if-eq p5, v4, :cond_2

    .line 703
    const/4 v2, 0x0

    .local v2, "j":I
    :goto_0
    array-length v4, p1

    if-lt v2, v4, :cond_1

    .line 725
    :cond_0
    return v3

    .line 704
    :cond_1
    array-length v4, p1

    sub-int/2addr v4, v2

    invoke-static {v5, v4}, Ljava/lang/Math;->min(II)I

    move-result v4

    new-array v0, v4, [B

    .line 705
    .local v0, "buf":[B
    array-length v4, p1

    sub-int/2addr v4, v2

    invoke-static {v5, v4}, Ljava/lang/Math;->min(II)I

    move-result v4

    invoke-static {p1, v2, v0, v6, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 706
    new-instance v1, Lorg/apache/poi/hssf/record/ContinueRecord;

    invoke-direct {v1, v0}, Lorg/apache/poi/hssf/record/ContinueRecord;-><init>([B)V

    .line 707
    .local v1, "drawing":Lorg/apache/poi/hssf/record/ContinueRecord;
    add-int v4, p3, v3

    invoke-virtual {v1, v4, p4}, Lorg/apache/poi/hssf/record/ContinueRecord;->serialize(I[B)I

    move-result v4

    add-int/2addr v3, v4

    .line 703
    add-int/lit16 v2, v2, 0x2020

    goto :goto_0

    .line 710
    .end local v0    # "buf":[B
    .end local v1    # "drawing":Lorg/apache/poi/hssf/record/ContinueRecord;
    .end local v2    # "j":I
    :cond_2
    const/4 v2, 0x0

    .restart local v2    # "j":I
    :goto_1
    array-length v4, p1

    if-ge v2, v4, :cond_0

    .line 711
    if-nez v2, :cond_3

    .line 712
    new-instance v1, Lorg/apache/poi/hssf/record/DrawingRecord;

    invoke-direct {v1}, Lorg/apache/poi/hssf/record/DrawingRecord;-><init>()V

    .line 713
    .local v1, "drawing":Lorg/apache/poi/hssf/record/DrawingRecord;
    array-length v4, p1

    sub-int/2addr v4, v2

    invoke-static {v5, v4}, Ljava/lang/Math;->min(II)I

    move-result v4

    new-array v0, v4, [B

    .line 714
    .restart local v0    # "buf":[B
    array-length v4, p1

    sub-int/2addr v4, v2

    invoke-static {v5, v4}, Ljava/lang/Math;->min(II)I

    move-result v4

    invoke-static {p1, v2, v0, v6, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 715
    invoke-virtual {v1, v0}, Lorg/apache/poi/hssf/record/DrawingRecord;->setData([B)V

    .line 716
    add-int v4, p3, v3

    invoke-virtual {v1, v4, p4}, Lorg/apache/poi/hssf/record/DrawingRecord;->serialize(I[B)I

    move-result v4

    add-int/2addr v3, v4

    .line 710
    .end local v1    # "drawing":Lorg/apache/poi/hssf/record/DrawingRecord;
    :goto_2
    add-int/lit16 v2, v2, 0x2020

    goto :goto_1

    .line 718
    .end local v0    # "buf":[B
    :cond_3
    array-length v4, p1

    sub-int/2addr v4, v2

    invoke-static {v5, v4}, Ljava/lang/Math;->min(II)I

    move-result v4

    new-array v0, v4, [B

    .line 719
    .restart local v0    # "buf":[B
    array-length v4, p1

    sub-int/2addr v4, v2

    invoke-static {v5, v4}, Ljava/lang/Math;->min(II)I

    move-result v4

    invoke-static {p1, v2, v0, v6, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 720
    new-instance v1, Lorg/apache/poi/hssf/record/ContinueRecord;

    invoke-direct {v1, v0}, Lorg/apache/poi/hssf/record/ContinueRecord;-><init>([B)V

    .line 721
    .local v1, "drawing":Lorg/apache/poi/hssf/record/ContinueRecord;
    add-int v4, p3, v3

    invoke-virtual {v1, v4, p4}, Lorg/apache/poi/hssf/record/ContinueRecord;->serialize(I[B)I

    move-result v4

    add-int/2addr v3, v4

    goto :goto_2
.end method


# virtual methods
.method public addTailRecord(Lorg/apache/poi/hssf/record/NoteRecord;)V
    .locals 2
    .param p1, "note"    # Lorg/apache/poi/hssf/record/NoteRecord;

    .prologue
    .line 965
    iget-object v0, p0, Lorg/apache/poi/hssf/record/EscherAggregate;->tailRec:Ljava/util/Map;

    invoke-virtual {p1}, Lorg/apache/poi/hssf/record/NoteRecord;->getShapeId()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 966
    return-void
.end method

.method public associateShapeToObjRecord(Lorg/apache/poi/ddf/EscherRecord;Lorg/apache/poi/hssf/record/Record;)V
    .locals 1
    .param p1, "r"    # Lorg/apache/poi/ddf/EscherRecord;
    .param p2, "objRecord"    # Lorg/apache/poi/hssf/record/Record;

    .prologue
    .line 800
    iget-object v0, p0, Lorg/apache/poi/hssf/record/EscherAggregate;->shapeToObj:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 801
    return-void
.end method

.method public getNoteRecordByObj(Lorg/apache/poi/hssf/record/ObjRecord;)Lorg/apache/poi/hssf/record/NoteRecord;
    .locals 3
    .param p1, "obj"    # Lorg/apache/poi/hssf/record/ObjRecord;

    .prologue
    .line 956
    invoke-virtual {p1}, Lorg/apache/poi/hssf/record/ObjRecord;->getSubRecords()Ljava/util/List;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hssf/record/CommonObjectDataSubRecord;

    .line 957
    .local v0, "cod":Lorg/apache/poi/hssf/record/CommonObjectDataSubRecord;
    iget-object v1, p0, Lorg/apache/poi/hssf/record/EscherAggregate;->tailRec:Ljava/util/Map;

    invoke-virtual {v0}, Lorg/apache/poi/hssf/record/CommonObjectDataSubRecord;->getObjectId()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/hssf/record/NoteRecord;

    return-object v1
.end method

.method protected getRecordName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 815
    const-string/jumbo v0, "ESCHERAGGREGATE"

    return-object v0
.end method

.method public getRecordSize()I
    .locals 15

    .prologue
    .line 748
    const/4 v1, 0x0

    .line 750
    .local v1, "continueRecordsHeadersSize":I
    invoke-virtual {p0}, Lorg/apache/poi/hssf/record/EscherAggregate;->getEscherRecords()Ljava/util/List;

    move-result-object v10

    .line 751
    .local v10, "records":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/ddf/EscherRecord;>;"
    invoke-direct {p0, v10}, Lorg/apache/poi/hssf/record/EscherAggregate;->getEscherRecordSize(Ljava/util/List;)I

    move-result v9

    .line 752
    .local v9, "rawEscherSize":I
    new-array v0, v9, [B

    .line 753
    .local v0, "buffer":[B
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 754
    .local v11, "spEndingOffsets":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    const/4 v7, 0x0

    .line 755
    .local v7, "pos":I
    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :goto_0
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-nez v14, :cond_1

    .line 767
    const/4 v13, 0x0

    const/4 v14, 0x0

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    invoke-interface {v11, v13, v14}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 769
    const/4 v4, 0x1

    .local v4, "i":I
    :goto_1
    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v13

    if-lt v4, v13, :cond_2

    .line 779
    iget-object v13, p0, Lorg/apache/poi/hssf/record/EscherAggregate;->shapeToObj:Ljava/util/Map;

    invoke-interface {v13}, Ljava/util/Map;->size()I

    move-result v13

    mul-int/lit8 v13, v13, 0x4

    add-int v2, v9, v13

    .line 780
    .local v2, "drawingRecordSize":I
    if-eqz v9, :cond_0

    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v13

    const/4 v14, 0x1

    if-ne v13, v14, :cond_0

    .line 781
    add-int/lit8 v1, v1, 0x4

    .line 783
    :cond_0
    const/4 v6, 0x0

    .line 784
    .local v6, "objRecordSize":I
    iget-object v13, p0, Lorg/apache/poi/hssf/record/EscherAggregate;->shapeToObj:Ljava/util/Map;

    invoke-interface {v13}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v13

    invoke-interface {v13}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :goto_2
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-nez v14, :cond_5

    .line 787
    const/4 v12, 0x0

    .line 788
    .local v12, "tailRecordSize":I
    iget-object v13, p0, Lorg/apache/poi/hssf/record/EscherAggregate;->tailRec:Ljava/util/Map;

    invoke-interface {v13}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v13

    invoke-interface {v13}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :goto_3
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-nez v14, :cond_6

    .line 791
    add-int v13, v2, v6

    add-int/2addr v13, v12

    add-int/2addr v13, v1

    return v13

    .line 755
    .end local v2    # "drawingRecordSize":I
    .end local v4    # "i":I
    .end local v6    # "objRecordSize":I
    .end local v12    # "tailRecordSize":I
    :cond_1
    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/poi/ddf/EscherRecord;

    .line 756
    .local v3, "e":Lorg/apache/poi/ddf/EscherRecord;
    new-instance v14, Lorg/apache/poi/hssf/record/EscherAggregate$3;

    invoke-direct {v14, p0, v11}, Lorg/apache/poi/hssf/record/EscherAggregate$3;-><init>(Lorg/apache/poi/hssf/record/EscherAggregate;Ljava/util/List;)V

    invoke-virtual {v3, v7, v0, v14}, Lorg/apache/poi/ddf/EscherRecord;->serialize(I[BLorg/apache/poi/ddf/EscherSerializationListener;)I

    move-result v14

    add-int/2addr v7, v14

    goto :goto_0

    .line 770
    .end local v3    # "e":Lorg/apache/poi/ddf/EscherRecord;
    .restart local v4    # "i":I
    :cond_2
    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v13

    add-int/lit8 v13, v13, -0x1

    if-ne v4, v13, :cond_3

    invoke-interface {v11, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/Integer;

    invoke-virtual {v13}, Ljava/lang/Integer;->intValue()I

    move-result v13

    if-ge v13, v7, :cond_3

    .line 771
    add-int/lit8 v1, v1, 0x4

    .line 773
    :cond_3
    invoke-interface {v11, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/Integer;

    invoke-virtual {v13}, Ljava/lang/Integer;->intValue()I

    move-result v14

    add-int/lit8 v13, v4, -0x1

    invoke-interface {v11, v13}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/Integer;

    invoke-virtual {v13}, Ljava/lang/Integer;->intValue()I

    move-result v13

    sub-int v13, v14, v13

    const/16 v14, 0x2020

    if-gt v13, v14, :cond_4

    .line 769
    :goto_4
    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_1

    .line 776
    :cond_4
    invoke-interface {v11, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/Integer;

    invoke-virtual {v13}, Ljava/lang/Integer;->intValue()I

    move-result v14

    add-int/lit8 v13, v4, -0x1

    invoke-interface {v11, v13}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/Integer;

    invoke-virtual {v13}, Ljava/lang/Integer;->intValue()I

    move-result v13

    sub-int v13, v14, v13

    div-int/lit16 v13, v13, 0x2020

    mul-int/lit8 v13, v13, 0x4

    add-int/2addr v1, v13

    goto :goto_4

    .line 784
    .restart local v2    # "drawingRecordSize":I
    .restart local v6    # "objRecordSize":I
    :cond_5
    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lorg/apache/poi/hssf/record/Record;

    .line 785
    .local v8, "r":Lorg/apache/poi/hssf/record/Record;
    invoke-virtual {v8}, Lorg/apache/poi/hssf/record/Record;->getRecordSize()I

    move-result v14

    add-int/2addr v6, v14

    goto/16 :goto_2

    .line 788
    .end local v8    # "r":Lorg/apache/poi/hssf/record/Record;
    .restart local v12    # "tailRecordSize":I
    :cond_6
    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/apache/poi/hssf/record/NoteRecord;

    .line 789
    .local v5, "noteRecord":Lorg/apache/poi/hssf/record/NoteRecord;
    invoke-virtual {v5}, Lorg/apache/poi/hssf/record/NoteRecord;->getRecordSize()I

    move-result v14

    add-int/2addr v12, v14

    goto/16 :goto_3
.end method

.method public getShapeToObjMapping()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Lorg/apache/poi/ddf/EscherRecord;",
            "Lorg/apache/poi/hssf/record/Record;",
            ">;"
        }
    .end annotation

    .prologue
    .line 940
    iget-object v0, p0, Lorg/apache/poi/hssf/record/EscherAggregate;->shapeToObj:Ljava/util/Map;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public getSid()S
    .locals 1

    .prologue
    .line 317
    const/16 v0, 0x2694

    return v0
.end method

.method public getTailRecords()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lorg/apache/poi/hssf/record/NoteRecord;",
            ">;"
        }
    .end annotation

    .prologue
    .line 948
    iget-object v0, p0, Lorg/apache/poi/hssf/record/EscherAggregate;->tailRec:Ljava/util/Map;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public getTextForShape(Ljava/lang/Integer;)Lorg/apache/poi/hssf/record/TextObjectRecord;
    .locals 1
    .param p1, "shapeID"    # Ljava/lang/Integer;

    .prologue
    .line 389
    iget-object v0, p0, Lorg/apache/poi/hssf/record/EscherAggregate;->mShapeIDTextObjMap:Ljava/util/Map;

    if-eqz v0, :cond_0

    .line 390
    iget-object v0, p0, Lorg/apache/poi/hssf/record/EscherAggregate;->mShapeIDTextObjMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hssf/record/TextObjectRecord;

    .line 392
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public removeShapeToObjRecord(Lorg/apache/poi/ddf/EscherRecord;)V
    .locals 1
    .param p1, "rec"    # Lorg/apache/poi/ddf/EscherRecord;

    .prologue
    .line 808
    iget-object v0, p0, Lorg/apache/poi/hssf/record/EscherAggregate;->shapeToObj:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 809
    return-void
.end method

.method public removeTailRecord(Lorg/apache/poi/hssf/record/NoteRecord;)V
    .locals 2
    .param p1, "note"    # Lorg/apache/poi/hssf/record/NoteRecord;

    .prologue
    .line 973
    iget-object v0, p0, Lorg/apache/poi/hssf/record/EscherAggregate;->tailRec:Ljava/util/Map;

    invoke-virtual {p1}, Lorg/apache/poi/hssf/record/NoteRecord;->getShapeId()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 974
    return-void
.end method

.method public serialize(I[B)I
    .locals 22
    .param p1, "offset"    # I
    .param p2, "data"    # [B

    .prologue
    .line 620
    invoke-virtual/range {p0 .. p0}, Lorg/apache/poi/hssf/record/EscherAggregate;->getEscherRecords()Ljava/util/List;

    move-result-object v16

    .line 621
    .local v16, "records":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/ddf/EscherRecord;>;"
    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v0, v1}, Lorg/apache/poi/hssf/record/EscherAggregate;->getEscherRecordSize(Ljava/util/List;)I

    move-result v18

    .line 622
    .local v18, "size":I
    move/from16 v0, v18

    new-array v9, v0, [B

    .line 625
    .local v9, "buffer":[B
    new-instance v19, Ljava/util/ArrayList;

    invoke-direct/range {v19 .. v19}, Ljava/util/ArrayList;-><init>()V

    .line 626
    .local v19, "spEndingOffsets":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    new-instance v17, Ljava/util/ArrayList;

    invoke-direct/range {v17 .. v17}, Ljava/util/ArrayList;-><init>()V

    .line 627
    .local v17, "shapes":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/ddf/EscherRecord;>;"
    const/4 v6, 0x0

    .line 628
    .local v6, "pos":I
    invoke-interface/range {v16 .. v16}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-nez v7, :cond_1

    .line 642
    const/4 v3, 0x0

    const/4 v7, 0x0

    move-object/from16 v0, v17

    invoke-interface {v0, v3, v7}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 643
    const/4 v3, 0x0

    const/4 v7, 0x0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    move-object/from16 v0, v19

    invoke-interface {v0, v3, v7}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 647
    move/from16 v6, p1

    .line 648
    const/4 v5, 0x0

    .line 650
    .local v5, "writtenEscherBytes":I
    const/4 v8, 0x1

    .local v8, "i":I
    :goto_1
    invoke-interface/range {v17 .. v17}, Ljava/util/List;->size()I

    move-result v3

    if-lt v8, v3, :cond_2

    .line 674
    sub-int v3, v6, p1

    array-length v7, v9

    add-int/lit8 v7, v7, -0x1

    if-ge v3, v7, :cond_0

    .line 675
    array-length v3, v9

    sub-int v7, v6, p1

    sub-int/2addr v3, v7

    new-array v4, v3, [B

    .line 676
    .local v4, "drawingData":[B
    sub-int v3, v6, p1

    const/4 v7, 0x0

    array-length v0, v4

    move/from16 v21, v0

    move/from16 v0, v21

    invoke-static {v9, v3, v4, v7, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    move-object/from16 v3, p0

    move-object/from16 v7, p2

    .line 677
    invoke-direct/range {v3 .. v8}, Lorg/apache/poi/hssf/record/EscherAggregate;->writeDataIntoDrawingRecord([BII[BI)I

    move-result v3

    add-int/2addr v6, v3

    .line 680
    .end local v4    # "drawingData":[B
    :cond_0
    const/4 v8, 0x0

    :goto_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/poi/hssf/record/EscherAggregate;->tailRec:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->size()I

    move-result v3

    if-lt v8, v3, :cond_5

    .line 684
    sub-int v10, v6, p1

    .line 685
    .local v10, "bytesWritten":I
    invoke-virtual/range {p0 .. p0}, Lorg/apache/poi/hssf/record/EscherAggregate;->getRecordSize()I

    move-result v3

    if-eq v10, v3, :cond_6

    .line 686
    new-instance v3, Lorg/apache/poi/hssf/record/RecordFormatException;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-static {v10}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-direct {v7, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v21, " bytes written but getRecordSize() reports "

    move-object/from16 v0, v21

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual/range {p0 .. p0}, Lorg/apache/poi/hssf/record/EscherAggregate;->getRecordSize()I

    move-result v21

    move/from16 v0, v21

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v3, v7}, Lorg/apache/poi/hssf/record/RecordFormatException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 628
    .end local v5    # "writtenEscherBytes":I
    .end local v8    # "i":I
    .end local v10    # "bytesWritten":I
    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    .local v15, "record":Ljava/lang/Object;
    move-object v11, v15

    .line 629
    check-cast v11, Lorg/apache/poi/ddf/EscherRecord;

    .line 630
    .local v11, "e":Lorg/apache/poi/ddf/EscherRecord;
    new-instance v7, Lorg/apache/poi/hssf/record/EscherAggregate$2;

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    move-object/from16 v2, v17

    invoke-direct {v7, v0, v1, v2}, Lorg/apache/poi/hssf/record/EscherAggregate$2;-><init>(Lorg/apache/poi/hssf/record/EscherAggregate;Ljava/util/List;Ljava/util/List;)V

    invoke-virtual {v11, v6, v9, v7}, Lorg/apache/poi/ddf/EscherRecord;->serialize(I[BLorg/apache/poi/ddf/EscherSerializationListener;)I

    move-result v7

    add-int/2addr v6, v7

    goto/16 :goto_0

    .line 651
    .end local v11    # "e":Lorg/apache/poi/ddf/EscherRecord;
    .end local v15    # "record":Ljava/lang/Object;
    .restart local v5    # "writtenEscherBytes":I
    .restart local v8    # "i":I
    :cond_2
    move-object/from16 v0, v19

    invoke-interface {v0, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    add-int/lit8 v12, v3, -0x1

    .line 653
    .local v12, "endOffset":I
    const/4 v3, 0x1

    if-ne v8, v3, :cond_4

    .line 654
    const/16 v20, 0x0

    .line 658
    .local v20, "startOffset":I
    :goto_3
    sub-int v3, v12, v20

    add-int/lit8 v3, v3, 0x1

    new-array v4, v3, [B

    .line 659
    .restart local v4    # "drawingData":[B
    const/4 v3, 0x0

    array-length v7, v4

    move/from16 v0, v20

    invoke-static {v9, v0, v4, v3, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    move-object/from16 v3, p0

    move-object/from16 v7, p2

    .line 660
    invoke-direct/range {v3 .. v8}, Lorg/apache/poi/hssf/record/EscherAggregate;->writeDataIntoDrawingRecord([BII[BI)I

    move-result v3

    add-int/2addr v6, v3

    .line 662
    array-length v3, v4

    add-int/2addr v5, v3

    .line 665
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/poi/hssf/record/EscherAggregate;->shapeToObj:Ljava/util/Map;

    move-object/from16 v0, v17

    invoke-interface {v0, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    invoke-interface {v3, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lorg/apache/poi/hssf/record/Record;

    .line 666
    .local v13, "obj":Lorg/apache/poi/hssf/record/Record;
    move-object/from16 v0, p2

    invoke-virtual {v13, v6, v0}, Lorg/apache/poi/hssf/record/Record;->serialize(I[B)I

    move-result v3

    add-int/2addr v6, v3

    .line 668
    invoke-interface/range {v17 .. v17}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-ne v8, v3, :cond_3

    array-length v3, v9

    add-int/lit8 v3, v3, -0x1

    if-ge v12, v3, :cond_3

    .line 669
    array-length v3, v9

    sub-int/2addr v3, v12

    add-int/lit8 v3, v3, -0x1

    new-array v4, v3, [B

    .line 670
    add-int/lit8 v3, v12, 0x1

    const/4 v7, 0x0

    array-length v0, v4

    move/from16 v21, v0

    move/from16 v0, v21

    invoke-static {v9, v3, v4, v7, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    move-object/from16 v3, p0

    move-object/from16 v7, p2

    .line 671
    invoke-direct/range {v3 .. v8}, Lorg/apache/poi/hssf/record/EscherAggregate;->writeDataIntoDrawingRecord([BII[BI)I

    move-result v3

    add-int/2addr v6, v3

    .line 650
    :cond_3
    add-int/lit8 v8, v8, 0x1

    goto/16 :goto_1

    .line 656
    .end local v4    # "drawingData":[B
    .end local v13    # "obj":Lorg/apache/poi/hssf/record/Record;
    .end local v20    # "startOffset":I
    :cond_4
    add-int/lit8 v3, v8, -0x1

    move-object/from16 v0, v19

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v20

    .restart local v20    # "startOffset":I
    goto :goto_3

    .line 681
    .end local v12    # "endOffset":I
    .end local v20    # "startOffset":I
    :cond_5
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/poi/hssf/record/EscherAggregate;->tailRec:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->toArray()[Ljava/lang/Object;

    move-result-object v3

    aget-object v14, v3, v8

    check-cast v14, Lorg/apache/poi/hssf/record/Record;

    .line 682
    .local v14, "rec":Lorg/apache/poi/hssf/record/Record;
    move-object/from16 v0, p2

    invoke-virtual {v14, v6, v0}, Lorg/apache/poi/hssf/record/Record;->serialize(I[B)I

    move-result v3

    add-int/2addr v6, v3

    .line 680
    add-int/lit8 v8, v8, 0x1

    goto/16 :goto_2

    .line 687
    .end local v14    # "rec":Lorg/apache/poi/hssf/record/Record;
    .restart local v10    # "bytesWritten":I
    :cond_6
    return v10
.end method

.method public setDgId(S)V
    .locals 3
    .param p1, "dgId"    # S

    .prologue
    .line 888
    invoke-virtual {p0}, Lorg/apache/poi/hssf/record/EscherAggregate;->getEscherContainer()Lorg/apache/poi/ddf/EscherContainerRecord;

    move-result-object v1

    .line 889
    .local v1, "dgContainer":Lorg/apache/poi/ddf/EscherContainerRecord;
    if-nez v1, :cond_1

    .line 894
    :cond_0
    :goto_0
    return-void

    .line 891
    :cond_1
    const/16 v2, -0xff8

    invoke-virtual {v1, v2}, Lorg/apache/poi/ddf/EscherContainerRecord;->getChildById(S)Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/ddf/EscherDgRecord;

    .line 892
    .local v0, "dg":Lorg/apache/poi/ddf/EscherDgRecord;
    if-eqz v0, :cond_0

    .line 893
    shl-int/lit8 v2, p1, 0x4

    int-to-short v2, v2

    invoke-virtual {v0, v2}, Lorg/apache/poi/ddf/EscherDgRecord;->setOptions(S)V

    goto :goto_0
.end method

.method public setMainSpRecordId(I)V
    .locals 5
    .param p1, "shapeId"    # I

    .prologue
    .line 908
    invoke-virtual {p0}, Lorg/apache/poi/hssf/record/EscherAggregate;->getEscherContainer()Lorg/apache/poi/ddf/EscherContainerRecord;

    move-result-object v0

    .line 909
    .local v0, "dgContainer":Lorg/apache/poi/ddf/EscherContainerRecord;
    if-eqz v0, :cond_0

    .line 911
    const/16 v4, -0xffd

    invoke-virtual {v0, v4}, Lorg/apache/poi/ddf/EscherContainerRecord;->getChildById(S)Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v3

    check-cast v3, Lorg/apache/poi/ddf/EscherContainerRecord;

    .line 912
    .local v3, "spgrConatiner":Lorg/apache/poi/ddf/EscherContainerRecord;
    if-eqz v3, :cond_0

    .line 914
    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lorg/apache/poi/ddf/EscherContainerRecord;->getChild(I)Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v2

    .line 913
    check-cast v2, Lorg/apache/poi/ddf/EscherContainerRecord;

    .line 916
    .local v2, "spContainer":Lorg/apache/poi/ddf/EscherContainerRecord;
    const/16 v4, -0xff6

    invoke-virtual {v2, v4}, Lorg/apache/poi/ddf/EscherContainerRecord;->getChildById(S)Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/ddf/EscherSpRecord;

    .line 917
    .local v1, "sp":Lorg/apache/poi/ddf/EscherSpRecord;
    if-eqz v1, :cond_0

    .line 918
    invoke-virtual {v1, p1}, Lorg/apache/poi/ddf/EscherSpRecord;->setShapeId(I)V

    .line 921
    .end local v1    # "sp":Lorg/apache/poi/ddf/EscherSpRecord;
    .end local v2    # "spContainer":Lorg/apache/poi/ddf/EscherContainerRecord;
    .end local v3    # "spgrConatiner":Lorg/apache/poi/ddf/EscherContainerRecord;
    :cond_0
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .prologue
    const/16 v5, 0x5d

    .line 325
    const-string/jumbo v3, "line.separtor"

    invoke-static {v3}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 327
    .local v1, "nl":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 328
    .local v2, "result":Ljava/lang/StringBuilder;
    const/16 v3, 0x5b

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lorg/apache/poi/hssf/record/EscherAggregate;->getRecordName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 329
    invoke-virtual {p0}, Lorg/apache/poi/hssf/record/EscherAggregate;->getEscherRecords()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_0

    .line 332
    const-string/jumbo v3, "[/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lorg/apache/poi/hssf/record/EscherAggregate;->getRecordName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 334
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3

    .line 329
    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/ddf/EscherRecord;

    .line 330
    .local v0, "escherRecord":Lorg/apache/poi/ddf/EscherRecord;
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method public toXml(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p1, "tab"    # Ljava/lang/String;

    .prologue
    .line 344
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 345
    .local v0, "builder":Ljava/lang/StringBuilder;
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "<"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lorg/apache/poi/hssf/record/EscherAggregate;->getRecordName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ">\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 346
    invoke-virtual {p0}, Lorg/apache/poi/hssf/record/EscherAggregate;->getEscherRecords()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_0

    .line 349
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "</"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lorg/apache/poi/hssf/record/EscherAggregate;->getRecordName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ">\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 350
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2

    .line 346
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/ddf/EscherRecord;

    .line 347
    .local v1, "escherRecord":Lorg/apache/poi/ddf/EscherRecord;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v4, "\t"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lorg/apache/poi/ddf/EscherRecord;->toXml(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method
