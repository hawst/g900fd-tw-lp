.class final Lorg/apache/poi/hssf/record/HyperlinkRecord$GUID;
.super Ljava/lang/Object;
.source "HyperlinkRecord.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/poi/hssf/record/HyperlinkRecord;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "GUID"
.end annotation


# static fields
.field public static final ENCODED_SIZE:I = 0x10

.field private static final TEXT_FORMAT_LENGTH:I = 0x24


# instance fields
.field private final _d1:I

.field private final _d2:I

.field private final _d3:I

.field private final _d4:J


# direct methods
.method public constructor <init>(IIIJ)V
    .locals 0
    .param p1, "d1"    # I
    .param p2, "d2"    # I
    .param p3, "d3"    # I
    .param p4, "d4"    # J

    .prologue
    .line 72
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 73
    iput p1, p0, Lorg/apache/poi/hssf/record/HyperlinkRecord$GUID;->_d1:I

    .line 74
    iput p2, p0, Lorg/apache/poi/hssf/record/HyperlinkRecord$GUID;->_d2:I

    .line 75
    iput p3, p0, Lorg/apache/poi/hssf/record/HyperlinkRecord$GUID;->_d3:I

    .line 76
    iput-wide p4, p0, Lorg/apache/poi/hssf/record/HyperlinkRecord$GUID;->_d4:J

    .line 77
    return-void
.end method

.method public constructor <init>(Lorg/apache/poi/util/LittleEndianInput;)V
    .locals 6
    .param p1, "in"    # Lorg/apache/poi/util/LittleEndianInput;

    .prologue
    .line 69
    invoke-interface {p1}, Lorg/apache/poi/util/LittleEndianInput;->readInt()I

    move-result v1

    invoke-interface {p1}, Lorg/apache/poi/util/LittleEndianInput;->readUShort()I

    move-result v2

    invoke-interface {p1}, Lorg/apache/poi/util/LittleEndianInput;->readUShort()I

    move-result v3

    invoke-interface {p1}, Lorg/apache/poi/util/LittleEndianInput;->readLong()J

    move-result-wide v4

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lorg/apache/poi/hssf/record/HyperlinkRecord$GUID;-><init>(IIIJ)V

    .line 70
    return-void
.end method

.method public static parse(Ljava/lang/String;)Lorg/apache/poi/hssf/record/HyperlinkRecord$GUID;
    .locals 9
    .param p0, "rep"    # Ljava/lang/String;

    .prologue
    .line 158
    invoke-virtual {p0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v6

    .line 159
    .local v6, "cc":[C
    array-length v0, v6

    const/16 v8, 0x24

    if-eq v0, v8, :cond_0

    .line 160
    new-instance v0, Lorg/apache/poi/hssf/record/RecordFormatException;

    .line 161
    const-string/jumbo v8, "supplied text is the wrong length for a GUID"

    .line 160
    invoke-direct {v0, v8}, Lorg/apache/poi/hssf/record/RecordFormatException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 163
    :cond_0
    const/4 v0, 0x0

    invoke-static {v6, v0}, Lorg/apache/poi/hssf/record/HyperlinkRecord$GUID;->parseShort([CI)I

    move-result v0

    shl-int/lit8 v0, v0, 0x10

    const/4 v8, 0x4

    invoke-static {v6, v8}, Lorg/apache/poi/hssf/record/HyperlinkRecord$GUID;->parseShort([CI)I

    move-result v8

    shl-int/lit8 v8, v8, 0x0

    add-int v1, v0, v8

    .line 164
    .local v1, "d0":I
    const/16 v0, 0x9

    invoke-static {v6, v0}, Lorg/apache/poi/hssf/record/HyperlinkRecord$GUID;->parseShort([CI)I

    move-result v2

    .line 165
    .local v2, "d1":I
    const/16 v0, 0xe

    invoke-static {v6, v0}, Lorg/apache/poi/hssf/record/HyperlinkRecord$GUID;->parseShort([CI)I

    move-result v3

    .line 166
    .local v3, "d2":I
    const/16 v7, 0x17

    .local v7, "i":I
    :goto_0
    const/16 v0, 0x13

    if-gt v7, v0, :cond_1

    .line 169
    const/16 v0, 0x14

    invoke-static {v6, v0}, Lorg/apache/poi/hssf/record/HyperlinkRecord$GUID;->parseLELong([CI)J

    move-result-wide v4

    .line 171
    .local v4, "d3":J
    new-instance v0, Lorg/apache/poi/hssf/record/HyperlinkRecord$GUID;

    invoke-direct/range {v0 .. v5}, Lorg/apache/poi/hssf/record/HyperlinkRecord$GUID;-><init>(IIIJ)V

    return-object v0

    .line 167
    .end local v4    # "d3":J
    :cond_1
    add-int/lit8 v0, v7, -0x1

    aget-char v0, v6, v0

    aput-char v0, v6, v7

    .line 166
    add-int/lit8 v7, v7, -0x1

    goto :goto_0
.end method

.method private static parseHexChar(C)I
    .locals 3
    .param p0, "c"    # C

    .prologue
    .line 195
    const/16 v0, 0x30

    if-lt p0, v0, :cond_0

    const/16 v0, 0x39

    if-gt p0, v0, :cond_0

    .line 196
    add-int/lit8 v0, p0, -0x30

    .line 202
    :goto_0
    return v0

    .line 198
    :cond_0
    const/16 v0, 0x41

    if-lt p0, v0, :cond_1

    const/16 v0, 0x46

    if-gt p0, v0, :cond_1

    .line 199
    add-int/lit8 v0, p0, -0x41

    add-int/lit8 v0, v0, 0xa

    goto :goto_0

    .line 201
    :cond_1
    const/16 v0, 0x61

    if-lt p0, v0, :cond_2

    const/16 v0, 0x66

    if-gt p0, v0, :cond_2

    .line 202
    add-int/lit8 v0, p0, -0x61

    add-int/lit8 v0, v0, 0xa

    goto :goto_0

    .line 204
    :cond_2
    new-instance v0, Lorg/apache/poi/hssf/record/RecordFormatException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Bad hex char \'"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/poi/hssf/record/RecordFormatException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static parseLELong([CI)J
    .locals 7
    .param p0, "cc"    # [C
    .param p1, "startIndex"    # I

    .prologue
    const/4 v6, 0x4

    .line 175
    const-wide/16 v0, 0x0

    .line 176
    .local v0, "acc":J
    add-int/lit8 v2, p1, 0xe

    .local v2, "i":I
    :goto_0
    if-ge v2, p1, :cond_0

    .line 182
    return-wide v0

    .line 177
    :cond_0
    shl-long/2addr v0, v6

    .line 178
    add-int/lit8 v3, v2, 0x0

    aget-char v3, p0, v3

    invoke-static {v3}, Lorg/apache/poi/hssf/record/HyperlinkRecord$GUID;->parseHexChar(C)I

    move-result v3

    int-to-long v4, v3

    add-long/2addr v0, v4

    .line 179
    shl-long/2addr v0, v6

    .line 180
    add-int/lit8 v3, v2, 0x1

    aget-char v3, p0, v3

    invoke-static {v3}, Lorg/apache/poi/hssf/record/HyperlinkRecord$GUID;->parseHexChar(C)I

    move-result v3

    int-to-long v4, v3

    add-long/2addr v0, v4

    .line 176
    add-int/lit8 v2, v2, -0x2

    goto :goto_0
.end method

.method private static parseShort([CI)I
    .locals 3
    .param p0, "cc"    # [C
    .param p1, "startIndex"    # I

    .prologue
    .line 186
    const/4 v0, 0x0

    .line 187
    .local v0, "acc":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    const/4 v2, 0x4

    if-lt v1, v2, :cond_0

    .line 191
    return v0

    .line 188
    :cond_0
    shl-int/lit8 v0, v0, 0x4

    .line 189
    add-int v2, p1, v1

    aget-char v2, p0, v2

    invoke-static {v2}, Lorg/apache/poi/hssf/record/HyperlinkRecord$GUID;->parseHexChar(C)I

    move-result v2

    add-int/2addr v0, v2

    .line 187
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 6
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 88
    if-eqz p1, :cond_0

    instance-of v2, p1, Lorg/apache/poi/hssf/record/HyperlinkRecord$GUID;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 89
    check-cast v0, Lorg/apache/poi/hssf/record/HyperlinkRecord$GUID;

    .line 94
    .local v0, "other":Lorg/apache/poi/hssf/record/HyperlinkRecord$GUID;
    iget v2, p0, Lorg/apache/poi/hssf/record/HyperlinkRecord$GUID;->_d1:I

    iget v3, v0, Lorg/apache/poi/hssf/record/HyperlinkRecord$GUID;->_d1:I

    if-ne v2, v3, :cond_0

    iget v2, p0, Lorg/apache/poi/hssf/record/HyperlinkRecord$GUID;->_d2:I

    iget v3, v0, Lorg/apache/poi/hssf/record/HyperlinkRecord$GUID;->_d2:I

    if-ne v2, v3, :cond_0

    iget v2, p0, Lorg/apache/poi/hssf/record/HyperlinkRecord$GUID;->_d3:I

    iget v3, v0, Lorg/apache/poi/hssf/record/HyperlinkRecord$GUID;->_d3:I

    if-ne v2, v3, :cond_0

    .line 95
    iget-wide v2, p0, Lorg/apache/poi/hssf/record/HyperlinkRecord$GUID;->_d4:J

    iget-wide v4, v0, Lorg/apache/poi/hssf/record/HyperlinkRecord$GUID;->_d4:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    .line 94
    const/4 v1, 0x1

    .line 97
    .end local v0    # "other":Lorg/apache/poi/hssf/record/HyperlinkRecord$GUID;
    :cond_0
    return v1
.end method

.method public formatAsString()Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v6, 0x4

    .line 126
    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x24

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 128
    .local v2, "sb":Ljava/lang/StringBuilder;
    const-string/jumbo v3, "0x"

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v0

    .line 129
    .local v0, "PREFIX_LEN":I
    iget v3, p0, Lorg/apache/poi/hssf/record/HyperlinkRecord$GUID;->_d1:I

    invoke-static {v3}, Lorg/apache/poi/util/HexDump;->intToHex(I)[C

    move-result-object v3

    const/16 v4, 0x8

    invoke-virtual {v2, v3, v0, v4}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;

    .line 130
    const-string/jumbo v3, "-"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 131
    iget v3, p0, Lorg/apache/poi/hssf/record/HyperlinkRecord$GUID;->_d2:I

    invoke-static {v3}, Lorg/apache/poi/util/HexDump;->shortToHex(I)[C

    move-result-object v3

    invoke-virtual {v2, v3, v0, v6}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;

    .line 132
    const-string/jumbo v3, "-"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 133
    iget v3, p0, Lorg/apache/poi/hssf/record/HyperlinkRecord$GUID;->_d3:I

    invoke-static {v3}, Lorg/apache/poi/util/HexDump;->shortToHex(I)[C

    move-result-object v3

    invoke-virtual {v2, v3, v0, v6}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;

    .line 134
    const-string/jumbo v3, "-"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 135
    invoke-virtual {p0}, Lorg/apache/poi/hssf/record/HyperlinkRecord$GUID;->getD4()J

    move-result-wide v4

    invoke-static {v4, v5}, Lorg/apache/poi/util/HexDump;->longToHex(J)[C

    move-result-object v1

    .line 136
    .local v1, "d4Chars":[C
    invoke-virtual {v2, v1, v0, v6}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;

    .line 137
    const-string/jumbo v3, "-"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 138
    add-int/lit8 v3, v0, 0x4

    const/16 v4, 0xc

    invoke-virtual {v2, v1, v3, v4}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;

    .line 139
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method public getD1()I
    .locals 1

    .prologue
    .line 101
    iget v0, p0, Lorg/apache/poi/hssf/record/HyperlinkRecord$GUID;->_d1:I

    return v0
.end method

.method public getD2()I
    .locals 1

    .prologue
    .line 105
    iget v0, p0, Lorg/apache/poi/hssf/record/HyperlinkRecord$GUID;->_d2:I

    return v0
.end method

.method public getD3()I
    .locals 1

    .prologue
    .line 109
    iget v0, p0, Lorg/apache/poi/hssf/record/HyperlinkRecord$GUID;->_d3:I

    return v0
.end method

.method public getD4()J
    .locals 6

    .prologue
    .line 114
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    const/16 v3, 0x8

    invoke-direct {v0, v3}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    .line 116
    .local v0, "baos":Ljava/io/ByteArrayOutputStream;
    :try_start_0
    new-instance v3, Ljava/io/DataOutputStream;

    invoke-direct {v3, v0}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    iget-wide v4, p0, Lorg/apache/poi/hssf/record/HyperlinkRecord$GUID;->_d4:J

    invoke-virtual {v3, v4, v5}, Ljava/io/DataOutputStream;->writeLong(J)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 120
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    .line 121
    .local v1, "buf":[B
    new-instance v3, Lorg/apache/poi/util/LittleEndianByteArrayInputStream;

    invoke-direct {v3, v1}, Lorg/apache/poi/util/LittleEndianByteArrayInputStream;-><init>([B)V

    invoke-virtual {v3}, Lorg/apache/poi/util/LittleEndianByteArrayInputStream;->readLong()J

    move-result-wide v4

    return-wide v4

    .line 117
    .end local v1    # "buf":[B
    :catch_0
    move-exception v2

    .line 118
    .local v2, "e":Ljava/io/IOException;
    new-instance v3, Ljava/lang/RuntimeException;

    invoke-direct {v3, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v3
.end method

.method public serialize(Lorg/apache/poi/util/LittleEndianOutput;)V
    .locals 2
    .param p1, "out"    # Lorg/apache/poi/util/LittleEndianOutput;

    .prologue
    .line 80
    iget v0, p0, Lorg/apache/poi/hssf/record/HyperlinkRecord$GUID;->_d1:I

    invoke-interface {p1, v0}, Lorg/apache/poi/util/LittleEndianOutput;->writeInt(I)V

    .line 81
    iget v0, p0, Lorg/apache/poi/hssf/record/HyperlinkRecord$GUID;->_d2:I

    invoke-interface {p1, v0}, Lorg/apache/poi/util/LittleEndianOutput;->writeShort(I)V

    .line 82
    iget v0, p0, Lorg/apache/poi/hssf/record/HyperlinkRecord$GUID;->_d3:I

    invoke-interface {p1, v0}, Lorg/apache/poi/util/LittleEndianOutput;->writeShort(I)V

    .line 83
    iget-wide v0, p0, Lorg/apache/poi/hssf/record/HyperlinkRecord$GUID;->_d4:J

    invoke-interface {p1, v0, v1}, Lorg/apache/poi/util/LittleEndianOutput;->writeLong(J)V

    .line 84
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 144
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x40

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 145
    .local v0, "sb":Ljava/lang/StringBuilder;
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 146
    invoke-virtual {p0}, Lorg/apache/poi/hssf/record/HyperlinkRecord$GUID;->formatAsString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 147
    const-string/jumbo v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 148
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
