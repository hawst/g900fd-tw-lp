.class public final Lorg/apache/poi/hssf/util/HSSFColor$BLUE;
.super Lorg/apache/poi/hssf/util/HSSFColor;
.source "HSSFColor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/poi/hssf/util/HSSFColor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "BLUE"
.end annotation


# static fields
.field public static final hexString:Ljava/lang/String; = "0:0:FFFF"

.field public static final index:S = 0xcs

.field public static final index2:S = 0x27s

.field public static final triplet:[S


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 628
    const/4 v0, 0x3

    new-array v0, v0, [S

    const/4 v1, 0x2

    .line 629
    const/16 v2, 0xff

    aput-short v2, v0, v1

    .line 627
    sput-object v0, Lorg/apache/poi/hssf/util/HSSFColor$BLUE;->triplet:[S

    .line 631
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 622
    invoke-direct {p0}, Lorg/apache/poi/hssf/util/HSSFColor;-><init>()V

    return-void
.end method


# virtual methods
.method public getHexString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 645
    const-string/jumbo v0, "0:0:FFFF"

    return-object v0
.end method

.method public getIndex()S
    .locals 1

    .prologue
    .line 635
    const/16 v0, 0xc

    return v0
.end method

.method public getTriplet()[S
    .locals 1

    .prologue
    .line 640
    sget-object v0, Lorg/apache/poi/hssf/util/HSSFColor$BLUE;->triplet:[S

    return-object v0
.end method
