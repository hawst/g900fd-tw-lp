.class public final Lorg/apache/poi/hssf/usermodel/HSSFClientAnchor;
.super Lorg/apache/poi/hssf/usermodel/HSSFAnchor;
.source "HSSFClientAnchor.java"

# interfaces
.implements Lorg/apache/poi/ss/usermodel/ClientAnchor;


# instance fields
.field private _escherClientAnchor:Lorg/apache/poi/ddf/EscherClientAnchorRecord;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Lorg/apache/poi/hssf/usermodel/HSSFAnchor;-><init>()V

    .line 42
    return-void
.end method

.method public constructor <init>(IIIISISI)V
    .locals 6
    .param p1, "dx1"    # I
    .param p2, "dy1"    # I
    .param p3, "dx2"    # I
    .param p4, "dy2"    # I
    .param p5, "col1"    # S
    .param p6, "row1"    # I
    .param p7, "col2"    # S
    .param p8, "row2"    # I

    .prologue
    const v5, 0xff00

    const/16 v4, 0x3ff

    const/4 v3, 0x1

    const/16 v2, 0xff

    const/4 v1, 0x0

    .line 58
    invoke-direct {p0, p1, p2, p3, p4}, Lorg/apache/poi/hssf/usermodel/HSSFAnchor;-><init>(IIII)V

    .line 60
    const-string/jumbo v0, "dx1"

    invoke-direct {p0, p1, v1, v4, v0}, Lorg/apache/poi/hssf/usermodel/HSSFClientAnchor;->checkRange(IIILjava/lang/String;)V

    .line 61
    const-string/jumbo v0, "dx2"

    invoke-direct {p0, p3, v1, v4, v0}, Lorg/apache/poi/hssf/usermodel/HSSFClientAnchor;->checkRange(IIILjava/lang/String;)V

    .line 62
    const-string/jumbo v0, "dy1"

    invoke-direct {p0, p2, v1, v2, v0}, Lorg/apache/poi/hssf/usermodel/HSSFClientAnchor;->checkRange(IIILjava/lang/String;)V

    .line 63
    const-string/jumbo v0, "dy2"

    invoke-direct {p0, p4, v1, v2, v0}, Lorg/apache/poi/hssf/usermodel/HSSFClientAnchor;->checkRange(IIILjava/lang/String;)V

    .line 64
    const-string/jumbo v0, "col1"

    invoke-direct {p0, p5, v1, v2, v0}, Lorg/apache/poi/hssf/usermodel/HSSFClientAnchor;->checkRange(IIILjava/lang/String;)V

    .line 65
    const-string/jumbo v0, "col2"

    invoke-direct {p0, p7, v1, v2, v0}, Lorg/apache/poi/hssf/usermodel/HSSFClientAnchor;->checkRange(IIILjava/lang/String;)V

    .line 66
    const-string/jumbo v0, "row1"

    invoke-direct {p0, p6, v1, v5, v0}, Lorg/apache/poi/hssf/usermodel/HSSFClientAnchor;->checkRange(IIILjava/lang/String;)V

    .line 67
    const-string/jumbo v0, "row2"

    invoke-direct {p0, p8, v1, v5, v0}, Lorg/apache/poi/hssf/usermodel/HSSFClientAnchor;->checkRange(IIILjava/lang/String;)V

    .line 69
    invoke-static {p5, p7}, Ljava/lang/Math;->min(II)I

    move-result v0

    int-to-short v0, v0

    invoke-virtual {p0, v0}, Lorg/apache/poi/hssf/usermodel/HSSFClientAnchor;->setCol1(S)V

    .line 70
    invoke-static {p5, p7}, Ljava/lang/Math;->max(II)I

    move-result v0

    int-to-short v0, v0

    invoke-virtual {p0, v0}, Lorg/apache/poi/hssf/usermodel/HSSFClientAnchor;->setCol2(S)V

    .line 71
    invoke-static {p6, p8}, Ljava/lang/Math;->min(II)I

    move-result v0

    int-to-short v0, v0

    invoke-virtual {p0, v0}, Lorg/apache/poi/hssf/usermodel/HSSFClientAnchor;->setRow1(I)V

    .line 72
    invoke-static {p6, p8}, Ljava/lang/Math;->max(II)I

    move-result v0

    int-to-short v0, v0

    invoke-virtual {p0, v0}, Lorg/apache/poi/hssf/usermodel/HSSFClientAnchor;->setRow2(I)V

    .line 74
    if-le p5, p7, :cond_0

    .line 75
    iput-boolean v3, p0, Lorg/apache/poi/hssf/usermodel/HSSFClientAnchor;->_isHorizontallyFlipped:Z

    .line 77
    :cond_0
    if-le p6, p8, :cond_1

    .line 78
    iput-boolean v3, p0, Lorg/apache/poi/hssf/usermodel/HSSFClientAnchor;->_isVerticallyFlipped:Z

    .line 80
    :cond_1
    return-void
.end method

.method public constructor <init>(Lorg/apache/poi/ddf/EscherClientAnchorRecord;)V
    .locals 0
    .param p1, "escherClientAnchorRecord"    # Lorg/apache/poi/ddf/EscherClientAnchorRecord;

    .prologue
    .line 34
    invoke-direct {p0}, Lorg/apache/poi/hssf/usermodel/HSSFAnchor;-><init>()V

    .line 35
    iput-object p1, p0, Lorg/apache/poi/hssf/usermodel/HSSFClientAnchor;->_escherClientAnchor:Lorg/apache/poi/ddf/EscherClientAnchorRecord;

    .line 36
    return-void
.end method

.method private checkRange(IIILjava/lang/String;)V
    .locals 3
    .param p1, "value"    # I
    .param p2, "minRange"    # I
    .param p3, "maxRange"    # I
    .param p4, "varName"    # Ljava/lang/String;

    .prologue
    .line 260
    if-lt p1, p2, :cond_0

    if-le p1, p3, :cond_1

    .line 261
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {p4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v2, " must be between "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " and "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 262
    :cond_1
    return-void
.end method

.method private getRowHeightInPoints(Lorg/apache/poi/hssf/usermodel/HSSFSheet;I)F
    .locals 2
    .param p1, "sheet"    # Lorg/apache/poi/hssf/usermodel/HSSFSheet;
    .param p2, "rowNum"    # I

    .prologue
    .line 109
    invoke-virtual {p1, p2}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->getRow(I)Lorg/apache/poi/hssf/usermodel/HSSFRow;

    move-result-object v0

    .line 110
    .local v0, "row":Lorg/apache/poi/hssf/usermodel/HSSFRow;
    if-nez v0, :cond_0

    .line 111
    invoke-virtual {p1}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->getDefaultRowHeightInPoints()F

    move-result v1

    .line 113
    :goto_0
    return v1

    :cond_0
    invoke-virtual {v0}, Lorg/apache/poi/hssf/usermodel/HSSFRow;->getHeightInPoints()F

    move-result v1

    goto :goto_0
.end method


# virtual methods
.method protected createEscherAnchor()V
    .locals 1

    .prologue
    .line 238
    new-instance v0, Lorg/apache/poi/ddf/EscherClientAnchorRecord;

    invoke-direct {v0}, Lorg/apache/poi/ddf/EscherClientAnchorRecord;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFClientAnchor;->_escherClientAnchor:Lorg/apache/poi/ddf/EscherClientAnchorRecord;

    .line 239
    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 266
    if-nez p1, :cond_1

    .line 274
    :cond_0
    :goto_0
    return v1

    .line 268
    :cond_1
    if-ne p1, p0, :cond_2

    move v1, v2

    .line 269
    goto :goto_0

    .line 270
    :cond_2
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-ne v3, v4, :cond_0

    move-object v0, p1

    .line 272
    check-cast v0, Lorg/apache/poi/hssf/usermodel/HSSFClientAnchor;

    .line 274
    .local v0, "anchor":Lorg/apache/poi/hssf/usermodel/HSSFClientAnchor;
    invoke-virtual {v0}, Lorg/apache/poi/hssf/usermodel/HSSFClientAnchor;->getCol1()S

    move-result v3

    invoke-virtual {p0}, Lorg/apache/poi/hssf/usermodel/HSSFClientAnchor;->getCol1()S

    move-result v4

    if-ne v3, v4, :cond_0

    invoke-virtual {v0}, Lorg/apache/poi/hssf/usermodel/HSSFClientAnchor;->getCol2()S

    move-result v3

    invoke-virtual {p0}, Lorg/apache/poi/hssf/usermodel/HSSFClientAnchor;->getCol2()S

    move-result v4

    if-ne v3, v4, :cond_0

    invoke-virtual {v0}, Lorg/apache/poi/hssf/usermodel/HSSFClientAnchor;->getDx1()I

    move-result v3

    invoke-virtual {p0}, Lorg/apache/poi/hssf/usermodel/HSSFClientAnchor;->getDx1()I

    move-result v4

    if-ne v3, v4, :cond_0

    .line 275
    invoke-virtual {v0}, Lorg/apache/poi/hssf/usermodel/HSSFClientAnchor;->getDx2()I

    move-result v3

    invoke-virtual {p0}, Lorg/apache/poi/hssf/usermodel/HSSFClientAnchor;->getDx2()I

    move-result v4

    if-ne v3, v4, :cond_0

    invoke-virtual {v0}, Lorg/apache/poi/hssf/usermodel/HSSFClientAnchor;->getDy1()I

    move-result v3

    invoke-virtual {p0}, Lorg/apache/poi/hssf/usermodel/HSSFClientAnchor;->getDy1()I

    move-result v4

    if-ne v3, v4, :cond_0

    invoke-virtual {v0}, Lorg/apache/poi/hssf/usermodel/HSSFClientAnchor;->getDy2()I

    move-result v3

    invoke-virtual {p0}, Lorg/apache/poi/hssf/usermodel/HSSFClientAnchor;->getDy2()I

    move-result v4

    if-ne v3, v4, :cond_0

    .line 276
    invoke-virtual {v0}, Lorg/apache/poi/hssf/usermodel/HSSFClientAnchor;->getRow1()I

    move-result v3

    invoke-virtual {p0}, Lorg/apache/poi/hssf/usermodel/HSSFClientAnchor;->getRow1()I

    move-result v4

    if-ne v3, v4, :cond_0

    invoke-virtual {v0}, Lorg/apache/poi/hssf/usermodel/HSSFClientAnchor;->getRow2()I

    move-result v3

    invoke-virtual {p0}, Lorg/apache/poi/hssf/usermodel/HSSFClientAnchor;->getRow2()I

    move-result v4

    if-ne v3, v4, :cond_0

    invoke-virtual {v0}, Lorg/apache/poi/hssf/usermodel/HSSFClientAnchor;->getAnchorType()I

    move-result v3

    invoke-virtual {p0}, Lorg/apache/poi/hssf/usermodel/HSSFClientAnchor;->getAnchorType()I

    move-result v4

    if-ne v3, v4, :cond_0

    move v1, v2

    .line 274
    goto :goto_0
.end method

.method public getAnchorHeightInPoints(Lorg/apache/poi/hssf/usermodel/HSSFSheet;)F
    .locals 9
    .param p1, "sheet"    # Lorg/apache/poi/hssf/usermodel/HSSFSheet;

    .prologue
    const/high16 v8, 0x43800000    # 256.0f

    .line 89
    invoke-virtual {p0}, Lorg/apache/poi/hssf/usermodel/HSSFClientAnchor;->getDy1()I

    move-result v4

    .line 90
    .local v4, "y1":I
    invoke-virtual {p0}, Lorg/apache/poi/hssf/usermodel/HSSFClientAnchor;->getDy2()I

    move-result v5

    .line 91
    .local v5, "y2":I
    invoke-virtual {p0}, Lorg/apache/poi/hssf/usermodel/HSSFClientAnchor;->getRow1()I

    move-result v6

    invoke-virtual {p0}, Lorg/apache/poi/hssf/usermodel/HSSFClientAnchor;->getRow2()I

    move-result v7

    invoke-static {v6, v7}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 92
    .local v2, "row1":I
    invoke-virtual {p0}, Lorg/apache/poi/hssf/usermodel/HSSFClientAnchor;->getRow1()I

    move-result v6

    invoke-virtual {p0}, Lorg/apache/poi/hssf/usermodel/HSSFClientAnchor;->getRow2()I

    move-result v7

    invoke-static {v6, v7}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 94
    .local v3, "row2":I
    const/4 v1, 0x0

    .line 95
    .local v1, "points":F
    if-ne v2, v3, :cond_0

    .line 96
    sub-int v6, v5, v4

    int-to-float v6, v6

    div-float/2addr v6, v8

    invoke-direct {p0, p1, v3}, Lorg/apache/poi/hssf/usermodel/HSSFClientAnchor;->getRowHeightInPoints(Lorg/apache/poi/hssf/usermodel/HSSFSheet;I)F

    move-result v7

    mul-float v1, v6, v7

    .line 105
    :goto_0
    return v1

    .line 98
    :cond_0
    int-to-float v6, v4

    sub-float v6, v8, v6

    div-float/2addr v6, v8

    invoke-direct {p0, p1, v2}, Lorg/apache/poi/hssf/usermodel/HSSFClientAnchor;->getRowHeightInPoints(Lorg/apache/poi/hssf/usermodel/HSSFSheet;I)F

    move-result v7

    mul-float/2addr v6, v7

    add-float/2addr v1, v6

    .line 99
    add-int/lit8 v0, v2, 0x1

    .local v0, "i":I
    :goto_1
    if-lt v0, v3, :cond_1

    .line 102
    int-to-float v6, v5

    div-float/2addr v6, v8

    invoke-direct {p0, p1, v3}, Lorg/apache/poi/hssf/usermodel/HSSFClientAnchor;->getRowHeightInPoints(Lorg/apache/poi/hssf/usermodel/HSSFSheet;I)F

    move-result v7

    mul-float/2addr v6, v7

    add-float/2addr v1, v6

    goto :goto_0

    .line 100
    :cond_1
    invoke-direct {p0, p1, v0}, Lorg/apache/poi/hssf/usermodel/HSSFClientAnchor;->getRowHeightInPoints(Lorg/apache/poi/hssf/usermodel/HSSFSheet;I)F

    move-result v6

    add-float/2addr v1, v6

    .line 99
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public getAnchorType()I
    .locals 1

    .prologue
    .line 247
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFClientAnchor;->_escherClientAnchor:Lorg/apache/poi/ddf/EscherClientAnchorRecord;

    invoke-virtual {v0}, Lorg/apache/poi/ddf/EscherClientAnchorRecord;->getFlag()S

    move-result v0

    return v0
.end method

.method public getCol1()S
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFClientAnchor;->_escherClientAnchor:Lorg/apache/poi/ddf/EscherClientAnchorRecord;

    invoke-virtual {v0}, Lorg/apache/poi/ddf/EscherClientAnchorRecord;->getCol1()S

    move-result v0

    return v0
.end method

.method public getCol2()S
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFClientAnchor;->_escherClientAnchor:Lorg/apache/poi/ddf/EscherClientAnchorRecord;

    invoke-virtual {v0}, Lorg/apache/poi/ddf/EscherClientAnchorRecord;->getCol2()S

    move-result v0

    return v0
.end method

.method public getDx1()I
    .locals 1

    .prologue
    .line 281
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFClientAnchor;->_escherClientAnchor:Lorg/apache/poi/ddf/EscherClientAnchorRecord;

    invoke-virtual {v0}, Lorg/apache/poi/ddf/EscherClientAnchorRecord;->getDx1()S

    move-result v0

    return v0
.end method

.method public getDx2()I
    .locals 1

    .prologue
    .line 311
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFClientAnchor;->_escherClientAnchor:Lorg/apache/poi/ddf/EscherClientAnchorRecord;

    invoke-virtual {v0}, Lorg/apache/poi/ddf/EscherClientAnchorRecord;->getDx2()S

    move-result v0

    return v0
.end method

.method public getDy1()I
    .locals 1

    .prologue
    .line 291
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFClientAnchor;->_escherClientAnchor:Lorg/apache/poi/ddf/EscherClientAnchorRecord;

    invoke-virtual {v0}, Lorg/apache/poi/ddf/EscherClientAnchorRecord;->getDy1()S

    move-result v0

    return v0
.end method

.method public getDy2()I
    .locals 1

    .prologue
    .line 301
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFClientAnchor;->_escherClientAnchor:Lorg/apache/poi/ddf/EscherClientAnchorRecord;

    invoke-virtual {v0}, Lorg/apache/poi/ddf/EscherClientAnchorRecord;->getDy2()S

    move-result v0

    return v0
.end method

.method protected getEscherAnchor()Lorg/apache/poi/ddf/EscherRecord;
    .locals 1

    .prologue
    .line 233
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFClientAnchor;->_escherClientAnchor:Lorg/apache/poi/ddf/EscherClientAnchorRecord;

    return-object v0
.end method

.method public getRow1()I
    .locals 1

    .prologue
    .line 164
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFClientAnchor;->_escherClientAnchor:Lorg/apache/poi/ddf/EscherClientAnchorRecord;

    invoke-virtual {v0}, Lorg/apache/poi/ddf/EscherClientAnchorRecord;->getRow1()S

    move-result v0

    return v0
.end method

.method public getRow2()I
    .locals 1

    .prologue
    .line 179
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFClientAnchor;->_escherClientAnchor:Lorg/apache/poi/ddf/EscherClientAnchorRecord;

    invoke-virtual {v0}, Lorg/apache/poi/ddf/EscherClientAnchorRecord;->getRow2()S

    move-result v0

    return v0
.end method

.method public isHorizontallyFlipped()Z
    .locals 1

    .prologue
    .line 224
    iget-boolean v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFClientAnchor;->_isHorizontallyFlipped:Z

    return v0
.end method

.method public isVerticallyFlipped()Z
    .locals 1

    .prologue
    .line 228
    iget-boolean v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFClientAnchor;->_isVerticallyFlipped:Z

    return v0
.end method

.method public setAnchor(SIIISIII)V
    .locals 6
    .param p1, "col1"    # S
    .param p2, "row1"    # I
    .param p3, "x1"    # I
    .param p4, "y1"    # I
    .param p5, "col2"    # S
    .param p6, "row2"    # I
    .param p7, "x2"    # I
    .param p8, "y2"    # I

    .prologue
    const v5, 0xff00

    const/16 v4, 0x3ff

    const/16 v3, 0xff

    const/4 v2, 0x0

    .line 204
    invoke-virtual {p0}, Lorg/apache/poi/hssf/usermodel/HSSFClientAnchor;->getDx1()I

    move-result v0

    const-string/jumbo v1, "dx1"

    invoke-direct {p0, v0, v2, v4, v1}, Lorg/apache/poi/hssf/usermodel/HSSFClientAnchor;->checkRange(IIILjava/lang/String;)V

    .line 205
    invoke-virtual {p0}, Lorg/apache/poi/hssf/usermodel/HSSFClientAnchor;->getDx2()I

    move-result v0

    const-string/jumbo v1, "dx2"

    invoke-direct {p0, v0, v2, v4, v1}, Lorg/apache/poi/hssf/usermodel/HSSFClientAnchor;->checkRange(IIILjava/lang/String;)V

    .line 206
    invoke-virtual {p0}, Lorg/apache/poi/hssf/usermodel/HSSFClientAnchor;->getDy1()I

    move-result v0

    const-string/jumbo v1, "dy1"

    invoke-direct {p0, v0, v2, v3, v1}, Lorg/apache/poi/hssf/usermodel/HSSFClientAnchor;->checkRange(IIILjava/lang/String;)V

    .line 207
    invoke-virtual {p0}, Lorg/apache/poi/hssf/usermodel/HSSFClientAnchor;->getDy2()I

    move-result v0

    const-string/jumbo v1, "dy2"

    invoke-direct {p0, v0, v2, v3, v1}, Lorg/apache/poi/hssf/usermodel/HSSFClientAnchor;->checkRange(IIILjava/lang/String;)V

    .line 208
    invoke-virtual {p0}, Lorg/apache/poi/hssf/usermodel/HSSFClientAnchor;->getCol1()S

    move-result v0

    const-string/jumbo v1, "col1"

    invoke-direct {p0, v0, v2, v3, v1}, Lorg/apache/poi/hssf/usermodel/HSSFClientAnchor;->checkRange(IIILjava/lang/String;)V

    .line 209
    invoke-virtual {p0}, Lorg/apache/poi/hssf/usermodel/HSSFClientAnchor;->getCol2()S

    move-result v0

    const-string/jumbo v1, "col2"

    invoke-direct {p0, v0, v2, v3, v1}, Lorg/apache/poi/hssf/usermodel/HSSFClientAnchor;->checkRange(IIILjava/lang/String;)V

    .line 210
    invoke-virtual {p0}, Lorg/apache/poi/hssf/usermodel/HSSFClientAnchor;->getRow1()I

    move-result v0

    const-string/jumbo v1, "row1"

    invoke-direct {p0, v0, v2, v5, v1}, Lorg/apache/poi/hssf/usermodel/HSSFClientAnchor;->checkRange(IIILjava/lang/String;)V

    .line 211
    invoke-virtual {p0}, Lorg/apache/poi/hssf/usermodel/HSSFClientAnchor;->getRow2()I

    move-result v0

    const-string/jumbo v1, "row2"

    invoke-direct {p0, v0, v2, v5, v1}, Lorg/apache/poi/hssf/usermodel/HSSFClientAnchor;->checkRange(IIILjava/lang/String;)V

    .line 213
    invoke-virtual {p0, p1}, Lorg/apache/poi/hssf/usermodel/HSSFClientAnchor;->setCol1(S)V

    .line 214
    invoke-virtual {p0, p2}, Lorg/apache/poi/hssf/usermodel/HSSFClientAnchor;->setRow1(I)V

    .line 215
    invoke-virtual {p0, p3}, Lorg/apache/poi/hssf/usermodel/HSSFClientAnchor;->setDx1(I)V

    .line 216
    invoke-virtual {p0, p4}, Lorg/apache/poi/hssf/usermodel/HSSFClientAnchor;->setDy1(I)V

    .line 217
    invoke-virtual {p0, p5}, Lorg/apache/poi/hssf/usermodel/HSSFClientAnchor;->setCol2(S)V

    .line 218
    invoke-virtual {p0, p6}, Lorg/apache/poi/hssf/usermodel/HSSFClientAnchor;->setRow2(I)V

    .line 219
    invoke-virtual {p0, p7}, Lorg/apache/poi/hssf/usermodel/HSSFClientAnchor;->setDx2(I)V

    .line 220
    invoke-virtual {p0, p8}, Lorg/apache/poi/hssf/usermodel/HSSFClientAnchor;->setDy2(I)V

    .line 221
    return-void
.end method

.method public setAnchorType(I)V
    .locals 2
    .param p1, "anchorType"    # I

    .prologue
    .line 256
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFClientAnchor;->_escherClientAnchor:Lorg/apache/poi/ddf/EscherClientAnchorRecord;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->shortValue()S

    move-result v1

    invoke-virtual {v0, v1}, Lorg/apache/poi/ddf/EscherClientAnchorRecord;->setFlag(S)V

    .line 257
    return-void
.end method

.method public setCol1(I)V
    .locals 1
    .param p1, "col1"    # I

    .prologue
    .line 135
    int-to-short v0, p1

    invoke-virtual {p0, v0}, Lorg/apache/poi/hssf/usermodel/HSSFClientAnchor;->setCol1(S)V

    .line 136
    return-void
.end method

.method public setCol1(S)V
    .locals 3
    .param p1, "col1"    # S

    .prologue
    .line 127
    const/4 v0, 0x0

    const/16 v1, 0xff

    const-string/jumbo v2, "col1"

    invoke-direct {p0, p1, v0, v1, v2}, Lorg/apache/poi/hssf/usermodel/HSSFClientAnchor;->checkRange(IIILjava/lang/String;)V

    .line 128
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFClientAnchor;->_escherClientAnchor:Lorg/apache/poi/ddf/EscherClientAnchorRecord;

    invoke-virtual {v0, p1}, Lorg/apache/poi/ddf/EscherClientAnchorRecord;->setCol1(S)V

    .line 129
    return-void
.end method

.method public setCol2(I)V
    .locals 1
    .param p1, "col2"    # I

    .prologue
    .line 157
    int-to-short v0, p1

    invoke-virtual {p0, v0}, Lorg/apache/poi/hssf/usermodel/HSSFClientAnchor;->setCol2(S)V

    .line 158
    return-void
.end method

.method public setCol2(S)V
    .locals 3
    .param p1, "col2"    # S

    .prologue
    .line 149
    const/4 v0, 0x0

    const/16 v1, 0xff

    const-string/jumbo v2, "col2"

    invoke-direct {p0, p1, v0, v1, v2}, Lorg/apache/poi/hssf/usermodel/HSSFClientAnchor;->checkRange(IIILjava/lang/String;)V

    .line 150
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFClientAnchor;->_escherClientAnchor:Lorg/apache/poi/ddf/EscherClientAnchorRecord;

    invoke-virtual {v0, p1}, Lorg/apache/poi/ddf/EscherClientAnchorRecord;->setCol2(S)V

    .line 151
    return-void
.end method

.method public setDx1(I)V
    .locals 2
    .param p1, "dx1"    # I

    .prologue
    .line 286
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFClientAnchor;->_escherClientAnchor:Lorg/apache/poi/ddf/EscherClientAnchorRecord;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->shortValue()S

    move-result v1

    invoke-virtual {v0, v1}, Lorg/apache/poi/ddf/EscherClientAnchorRecord;->setDx1(S)V

    .line 287
    return-void
.end method

.method public setDx2(I)V
    .locals 2
    .param p1, "dx2"    # I

    .prologue
    .line 316
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFClientAnchor;->_escherClientAnchor:Lorg/apache/poi/ddf/EscherClientAnchorRecord;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->shortValue()S

    move-result v1

    invoke-virtual {v0, v1}, Lorg/apache/poi/ddf/EscherClientAnchorRecord;->setDx2(S)V

    .line 317
    return-void
.end method

.method public setDy1(I)V
    .locals 2
    .param p1, "dy1"    # I

    .prologue
    .line 296
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFClientAnchor;->_escherClientAnchor:Lorg/apache/poi/ddf/EscherClientAnchorRecord;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->shortValue()S

    move-result v1

    invoke-virtual {v0, v1}, Lorg/apache/poi/ddf/EscherClientAnchorRecord;->setDy1(S)V

    .line 297
    return-void
.end method

.method public setDy2(I)V
    .locals 2
    .param p1, "dy2"    # I

    .prologue
    .line 306
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFClientAnchor;->_escherClientAnchor:Lorg/apache/poi/ddf/EscherClientAnchorRecord;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->shortValue()S

    move-result v1

    invoke-virtual {v0, v1}, Lorg/apache/poi/ddf/EscherClientAnchorRecord;->setDy2(S)V

    .line 307
    return-void
.end method

.method public setRow1(I)V
    .locals 3
    .param p1, "row1"    # I

    .prologue
    .line 171
    const/4 v0, 0x0

    const/high16 v1, 0x10000

    const-string/jumbo v2, "row1"

    invoke-direct {p0, p1, v0, v1, v2}, Lorg/apache/poi/hssf/usermodel/HSSFClientAnchor;->checkRange(IIILjava/lang/String;)V

    .line 172
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFClientAnchor;->_escherClientAnchor:Lorg/apache/poi/ddf/EscherClientAnchorRecord;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->shortValue()S

    move-result v1

    invoke-virtual {v0, v1}, Lorg/apache/poi/ddf/EscherClientAnchorRecord;->setRow1(S)V

    .line 173
    return-void
.end method

.method public setRow2(I)V
    .locals 3
    .param p1, "row2"    # I

    .prologue
    .line 186
    const/4 v0, 0x0

    const/high16 v1, 0x10000

    const-string/jumbo v2, "row2"

    invoke-direct {p0, p1, v0, v1, v2}, Lorg/apache/poi/hssf/usermodel/HSSFClientAnchor;->checkRange(IIILjava/lang/String;)V

    .line 187
    iget-object v0, p0, Lorg/apache/poi/hssf/usermodel/HSSFClientAnchor;->_escherClientAnchor:Lorg/apache/poi/ddf/EscherClientAnchorRecord;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->shortValue()S

    move-result v1

    invoke-virtual {v0, v1}, Lorg/apache/poi/ddf/EscherClientAnchorRecord;->setRow2(S)V

    .line 188
    return-void
.end method
