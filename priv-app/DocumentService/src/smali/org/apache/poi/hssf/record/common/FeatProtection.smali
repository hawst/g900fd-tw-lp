.class public final Lorg/apache/poi/hssf/record/common/FeatProtection;
.super Ljava/lang/Object;
.source "FeatProtection.java"

# interfaces
.implements Lorg/apache/poi/hssf/record/common/SharedFeature;


# static fields
.field public static HAS_SELF_RELATIVE_SECURITY_FEATURE:J

.field public static NO_SELF_RELATIVE_SECURITY_FEATURE:J


# instance fields
.field private fSD:I

.field private passwordVerifier:I

.field private securityDescriptor:[B

.field private title:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 37
    const-wide/16 v0, 0x0

    sput-wide v0, Lorg/apache/poi/hssf/record/common/FeatProtection;->NO_SELF_RELATIVE_SECURITY_FEATURE:J

    .line 38
    const-wide/16 v0, 0x1

    sput-wide v0, Lorg/apache/poi/hssf/record/common/FeatProtection;->HAS_SELF_RELATIVE_SECURITY_FEATURE:J

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    const/4 v0, 0x0

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/apache/poi/hssf/record/common/FeatProtection;->securityDescriptor:[B

    .line 55
    return-void
.end method

.method public constructor <init>(Lorg/apache/poi/hssf/record/RecordInputStream;)V
    .locals 1
    .param p1, "in"    # Lorg/apache/poi/hssf/record/RecordInputStream;

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    invoke-virtual {p1}, Lorg/apache/poi/hssf/record/RecordInputStream;->readInt()I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hssf/record/common/FeatProtection;->fSD:I

    .line 59
    invoke-virtual {p1}, Lorg/apache/poi/hssf/record/RecordInputStream;->readInt()I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hssf/record/common/FeatProtection;->passwordVerifier:I

    .line 61
    invoke-static {p1}, Lorg/apache/poi/util/StringUtil;->readUnicodeString(Lorg/apache/poi/util/LittleEndianInput;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/poi/hssf/record/common/FeatProtection;->title:Ljava/lang/String;

    .line 63
    invoke-virtual {p1}, Lorg/apache/poi/hssf/record/RecordInputStream;->readRemainder()[B

    move-result-object v0

    iput-object v0, p0, Lorg/apache/poi/hssf/record/common/FeatProtection;->securityDescriptor:[B

    .line 64
    return-void
.end method


# virtual methods
.method public getDataSize()I
    .locals 2

    .prologue
    .line 85
    iget-object v0, p0, Lorg/apache/poi/hssf/record/common/FeatProtection;->title:Ljava/lang/String;

    invoke-static {v0}, Lorg/apache/poi/util/StringUtil;->getEncodedSize(Ljava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x8

    iget-object v1, p0, Lorg/apache/poi/hssf/record/common/FeatProtection;->securityDescriptor:[B

    array-length v1, v1

    add-int/2addr v0, v1

    return v0
.end method

.method public getFSD()I
    .locals 1

    .prologue
    .line 103
    iget v0, p0, Lorg/apache/poi/hssf/record/common/FeatProtection;->fSD:I

    return v0
.end method

.method public getPasswordVerifier()I
    .locals 1

    .prologue
    .line 89
    iget v0, p0, Lorg/apache/poi/hssf/record/common/FeatProtection;->passwordVerifier:I

    return v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lorg/apache/poi/hssf/record/common/FeatProtection;->title:Ljava/lang/String;

    return-object v0
.end method

.method public serialize(Lorg/apache/poi/util/LittleEndianOutput;)V
    .locals 1
    .param p1, "out"    # Lorg/apache/poi/util/LittleEndianOutput;

    .prologue
    .line 78
    iget v0, p0, Lorg/apache/poi/hssf/record/common/FeatProtection;->fSD:I

    invoke-interface {p1, v0}, Lorg/apache/poi/util/LittleEndianOutput;->writeInt(I)V

    .line 79
    iget v0, p0, Lorg/apache/poi/hssf/record/common/FeatProtection;->passwordVerifier:I

    invoke-interface {p1, v0}, Lorg/apache/poi/util/LittleEndianOutput;->writeInt(I)V

    .line 80
    iget-object v0, p0, Lorg/apache/poi/hssf/record/common/FeatProtection;->title:Ljava/lang/String;

    invoke-static {p1, v0}, Lorg/apache/poi/util/StringUtil;->writeUnicodeString(Lorg/apache/poi/util/LittleEndianOutput;Ljava/lang/String;)V

    .line 81
    iget-object v0, p0, Lorg/apache/poi/hssf/record/common/FeatProtection;->securityDescriptor:[B

    invoke-interface {p1, v0}, Lorg/apache/poi/util/LittleEndianOutput;->write([B)V

    .line 82
    return-void
.end method

.method public setPasswordVerifier(I)V
    .locals 0
    .param p1, "passwordVerifier"    # I

    .prologue
    .line 92
    iput p1, p0, Lorg/apache/poi/hssf/record/common/FeatProtection;->passwordVerifier:I

    .line 93
    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 0
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 99
    iput-object p1, p0, Lorg/apache/poi/hssf/record/common/FeatProtection;->title:Ljava/lang/String;

    .line 100
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 67
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 68
    .local v0, "buffer":Ljava/lang/StringBuffer;
    const-string/jumbo v1, " [FEATURE PROTECTION]\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 69
    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "   Self Relative = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lorg/apache/poi/hssf/record/common/FeatProtection;->fSD:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 70
    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "   Password Verifier = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lorg/apache/poi/hssf/record/common/FeatProtection;->passwordVerifier:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 71
    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "   Title = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lorg/apache/poi/hssf/record/common/FeatProtection;->title:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 72
    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "   Security Descriptor Size = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lorg/apache/poi/hssf/record/common/FeatProtection;->securityDescriptor:[B

    array-length v2, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 73
    const-string/jumbo v1, " [/FEATURE PROTECTION]\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 74
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
