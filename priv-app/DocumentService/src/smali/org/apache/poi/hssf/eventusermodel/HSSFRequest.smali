.class public Lorg/apache/poi/hssf/eventusermodel/HSSFRequest;
.super Ljava/lang/Object;
.source "HSSFRequest.java"


# instance fields
.field private final _records:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Short;",
            "Ljava/util/List",
            "<",
            "Lorg/apache/poi/hssf/eventusermodel/HSSFListener;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    new-instance v0, Ljava/util/HashMap;

    const/16 v1, 0x32

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, Lorg/apache/poi/hssf/eventusermodel/HSSFRequest;->_records:Ljava/util/Map;

    .line 44
    return-void
.end method


# virtual methods
.method public addListener(Lorg/apache/poi/hssf/eventusermodel/HSSFListener;S)V
    .locals 3
    .param p1, "lsnr"    # Lorg/apache/poi/hssf/eventusermodel/HSSFListener;
    .param p2, "sid"    # S

    .prologue
    .line 60
    iget-object v1, p0, Lorg/apache/poi/hssf/eventusermodel/HSSFRequest;->_records:Ljava/util/Map;

    invoke-static {p2}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 62
    .local v0, "list":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/hssf/eventusermodel/HSSFListener;>;"
    if-nez v0, :cond_0

    .line 63
    new-instance v0, Ljava/util/ArrayList;

    .end local v0    # "list":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/hssf/eventusermodel/HSSFListener;>;"
    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 64
    .restart local v0    # "list":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/hssf/eventusermodel/HSSFListener;>;"
    iget-object v1, p0, Lorg/apache/poi/hssf/eventusermodel/HSSFRequest;->_records:Ljava/util/Map;

    invoke-static {p2}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 66
    :cond_0
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 67
    return-void
.end method

.method public addListenerForAllRecords(Lorg/apache/poi/hssf/eventusermodel/HSSFListener;)V
    .locals 3
    .param p1, "lsnr"    # Lorg/apache/poi/hssf/eventusermodel/HSSFListener;

    .prologue
    .line 80
    invoke-static {}, Lorg/apache/poi/hssf/record/RecordFactory;->getAllKnownRecordSIDs()[S

    move-result-object v1

    .line 82
    .local v1, "rectypes":[S
    const/4 v0, 0x0

    .local v0, "k":I
    :goto_0
    array-length v2, v1

    if-lt v0, v2, :cond_0

    .line 85
    return-void

    .line 83
    :cond_0
    aget-short v2, v1, v0

    invoke-virtual {p0, p1, v2}, Lorg/apache/poi/hssf/eventusermodel/HSSFRequest;->addListener(Lorg/apache/poi/hssf/eventusermodel/HSSFListener;S)V

    .line 82
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method protected processRecord(Lorg/apache/poi/hssf/record/Record;)S
    .locals 8
    .param p1, "rec"    # Lorg/apache/poi/hssf/record/Record;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/poi/hssf/eventusermodel/HSSFUserException;
        }
    .end annotation

    .prologue
    .line 97
    iget-object v6, p0, Lorg/apache/poi/hssf/eventusermodel/HSSFRequest;->_records:Ljava/util/Map;

    invoke-virtual {p1}, Lorg/apache/poi/hssf/record/Record;->getSid()S

    move-result v7

    invoke-static {v7}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    .line 98
    .local v4, "obj":Ljava/lang/Object;
    const/4 v5, 0x0

    .line 100
    .local v5, "userCode":S
    if-eqz v4, :cond_0

    move-object v3, v4

    .line 101
    check-cast v3, Ljava/util/List;

    .line 103
    .local v3, "listeners":Ljava/util/List;
    const/4 v0, 0x0

    .local v0, "k":I
    :goto_0
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v6

    if-lt v0, v6, :cond_1

    .line 116
    .end local v0    # "k":I
    .end local v3    # "listeners":Ljava/util/List;
    :cond_0
    return v5

    .line 104
    .restart local v0    # "k":I
    .restart local v3    # "listeners":Ljava/util/List;
    :cond_1
    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    .line 105
    .local v1, "listenObj":Ljava/lang/Object;
    instance-of v6, v1, Lorg/apache/poi/hssf/eventusermodel/AbortableHSSFListener;

    if-eqz v6, :cond_2

    move-object v2, v1

    .line 106
    check-cast v2, Lorg/apache/poi/hssf/eventusermodel/AbortableHSSFListener;

    .line 107
    .local v2, "listener":Lorg/apache/poi/hssf/eventusermodel/AbortableHSSFListener;
    invoke-virtual {v2, p1}, Lorg/apache/poi/hssf/eventusermodel/AbortableHSSFListener;->abortableProcessRecord(Lorg/apache/poi/hssf/record/Record;)S

    move-result v5

    .line 108
    if-nez v5, :cond_0

    .line 103
    .end local v2    # "listener":Lorg/apache/poi/hssf/eventusermodel/AbortableHSSFListener;
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    move-object v2, v1

    .line 111
    check-cast v2, Lorg/apache/poi/hssf/eventusermodel/HSSFListener;

    .line 112
    .local v2, "listener":Lorg/apache/poi/hssf/eventusermodel/HSSFListener;
    invoke-interface {v2, p1}, Lorg/apache/poi/hssf/eventusermodel/HSSFListener;->processRecord(Lorg/apache/poi/hssf/record/Record;)V

    goto :goto_1
.end method
