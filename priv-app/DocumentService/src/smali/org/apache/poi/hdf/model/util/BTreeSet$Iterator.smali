.class Lorg/apache/poi/hdf/model/util/BTreeSet$Iterator;
.super Ljava/lang/Object;
.source "BTreeSet.java"

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/poi/hdf/model/util/BTreeSet;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "Iterator"
.end annotation


# instance fields
.field private currentNode:Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;

.field private index:I

.field private lastReturned:Ljava/lang/Object;

.field private next:Ljava/lang/Object;

.field private parentIndex:Ljava/util/Stack;

.field final synthetic this$0:Lorg/apache/poi/hdf/model/util/BTreeSet;


# direct methods
.method constructor <init>(Lorg/apache/poi/hdf/model/util/BTreeSet;)V
    .locals 1

    .prologue
    .line 204
    iput-object p1, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$Iterator;->this$0:Lorg/apache/poi/hdf/model/util/BTreeSet;

    .line 203
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 197
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$Iterator;->index:I

    .line 198
    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$Iterator;->parentIndex:Ljava/util/Stack;

    .line 199
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$Iterator;->lastReturned:Ljava/lang/Object;

    .line 205
    invoke-direct {p0}, Lorg/apache/poi/hdf/model/util/BTreeSet$Iterator;->firstNode()Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$Iterator;->currentNode:Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;

    .line 206
    invoke-direct {p0}, Lorg/apache/poi/hdf/model/util/BTreeSet$Iterator;->nextElement()Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$Iterator;->next:Ljava/lang/Object;

    .line 207
    return-void
.end method

.method private firstNode()Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 233
    iget-object v1, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$Iterator;->this$0:Lorg/apache/poi/hdf/model/util/BTreeSet;

    iget-object v0, v1, Lorg/apache/poi/hdf/model/util/BTreeSet;->root:Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;

    .line 235
    .local v0, "temp":Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;
    :goto_0
    iget-object v1, v0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    aget-object v1, v1, v3

    iget-object v1, v1, Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;->child:Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;

    if-nez v1, :cond_0

    .line 241
    return-object v0

    .line 237
    :cond_0
    iget-object v1, v0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    aget-object v1, v1, v3

    iget-object v0, v1, Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;->child:Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;

    .line 238
    iget-object v1, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$Iterator;->parentIndex:Ljava/util/Stack;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method private nextElement()Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 246
    iget-object v0, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$Iterator;->currentNode:Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;

    # invokes: Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->isLeaf()Z
    invoke-static {v0}, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->access$0(Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 248
    iget v0, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$Iterator;->index:I

    iget-object v2, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$Iterator;->currentNode:Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;

    # getter for: Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->nrElements:I
    invoke-static {v2}, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->access$1(Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;)I

    move-result v2

    if-ge v0, v2, :cond_0

    iget-object v0, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$Iterator;->currentNode:Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;

    iget-object v0, v0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    iget v1, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$Iterator;->index:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$Iterator;->index:I

    aget-object v0, v0, v1

    iget-object v0, v0, Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;->element:Ljava/lang/Object;

    .line 284
    :goto_0
    return-object v0

    .line 250
    :cond_0
    iget-object v0, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$Iterator;->parentIndex:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->empty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 252
    iget-object v0, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$Iterator;->currentNode:Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;

    iget-object v0, v0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->parent:Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;

    iput-object v0, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$Iterator;->currentNode:Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;

    .line 253
    iget-object v0, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$Iterator;->parentIndex:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$Iterator;->index:I

    .line 255
    :goto_1
    iget v0, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$Iterator;->index:I

    iget-object v2, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$Iterator;->currentNode:Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;

    # getter for: Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->nrElements:I
    invoke-static {v2}, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->access$1(Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;)I

    move-result v2

    if-eq v0, v2, :cond_2

    .line 262
    :cond_1
    iget v0, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$Iterator;->index:I

    iget-object v2, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$Iterator;->currentNode:Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;

    # getter for: Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->nrElements:I
    invoke-static {v2}, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->access$1(Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;)I

    move-result v2

    if-ne v0, v2, :cond_3

    move-object v0, v1

    goto :goto_0

    .line 257
    :cond_2
    iget-object v0, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$Iterator;->parentIndex:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->empty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 258
    iget-object v0, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$Iterator;->currentNode:Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;

    iget-object v0, v0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->parent:Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;

    iput-object v0, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$Iterator;->currentNode:Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;

    .line 259
    iget-object v0, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$Iterator;->parentIndex:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$Iterator;->index:I

    goto :goto_1

    .line 263
    :cond_3
    iget-object v0, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$Iterator;->currentNode:Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;

    iget-object v0, v0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    iget v1, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$Iterator;->index:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$Iterator;->index:I

    aget-object v0, v0, v1

    iget-object v0, v0, Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;->element:Ljava/lang/Object;

    goto :goto_0

    .line 268
    :cond_4
    iget v0, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$Iterator;->index:I

    iget-object v2, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$Iterator;->currentNode:Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;

    # getter for: Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->nrElements:I
    invoke-static {v2}, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->access$1(Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;)I

    move-result v2

    if-ne v0, v2, :cond_5

    move-object v0, v1

    goto :goto_0

    .line 269
    :cond_5
    iget-object v0, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$Iterator;->currentNode:Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;

    iget-object v0, v0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    iget v1, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$Iterator;->index:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$Iterator;->index:I

    aget-object v0, v0, v1

    iget-object v0, v0, Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;->element:Ljava/lang/Object;

    goto :goto_0

    .line 274
    :cond_6
    iget-object v0, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$Iterator;->currentNode:Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;

    iget-object v0, v0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    iget v1, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$Iterator;->index:I

    aget-object v0, v0, v1

    iget-object v0, v0, Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;->child:Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;

    iput-object v0, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$Iterator;->currentNode:Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;

    .line 275
    iget-object v0, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$Iterator;->parentIndex:Ljava/util/Stack;

    iget v1, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$Iterator;->index:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 277
    :goto_2
    iget-object v0, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$Iterator;->currentNode:Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;

    iget-object v0, v0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    aget-object v0, v0, v2

    iget-object v0, v0, Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;->child:Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;

    if-nez v0, :cond_7

    .line 283
    const/4 v0, 0x1

    iput v0, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$Iterator;->index:I

    .line 284
    iget-object v0, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$Iterator;->currentNode:Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;

    iget-object v0, v0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    aget-object v0, v0, v2

    iget-object v0, v0, Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;->element:Ljava/lang/Object;

    goto/16 :goto_0

    .line 279
    :cond_7
    iget-object v0, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$Iterator;->currentNode:Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;

    iget-object v0, v0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    aget-object v0, v0, v2

    iget-object v0, v0, Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;->child:Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;

    iput-object v0, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$Iterator;->currentNode:Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;

    .line 280
    iget-object v0, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$Iterator;->parentIndex:Ljava/util/Stack;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2
.end method


# virtual methods
.method public hasNext()Z
    .locals 1

    .prologue
    .line 211
    iget-object v0, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$Iterator;->next:Ljava/lang/Object;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public next()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 216
    iget-object v0, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$Iterator;->next:Ljava/lang/Object;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 218
    :cond_0
    iget-object v0, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$Iterator;->next:Ljava/lang/Object;

    iput-object v0, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$Iterator;->lastReturned:Ljava/lang/Object;

    .line 219
    invoke-direct {p0}, Lorg/apache/poi/hdf/model/util/BTreeSet$Iterator;->nextElement()Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$Iterator;->next:Ljava/lang/Object;

    .line 220
    iget-object v0, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$Iterator;->lastReturned:Ljava/lang/Object;

    return-object v0
.end method

.method public remove()V
    .locals 2

    .prologue
    .line 225
    iget-object v0, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$Iterator;->lastReturned:Ljava/lang/Object;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 227
    :cond_0
    iget-object v0, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$Iterator;->this$0:Lorg/apache/poi/hdf/model/util/BTreeSet;

    iget-object v1, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$Iterator;->lastReturned:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lorg/apache/poi/hdf/model/util/BTreeSet;->remove(Ljava/lang/Object;)Z

    .line 228
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$Iterator;->lastReturned:Ljava/lang/Object;

    .line 229
    return-void
.end method
