.class public final Lorg/apache/poi/hdf/model/hdftypes/HeaderFooter;
.super Ljava/lang/Object;
.source "HeaderFooter.java"


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field public static final FOOTER_EVEN:I = 0x3

.field public static final FOOTER_FIRST:I = 0x6

.field public static final FOOTER_ODD:I = 0x4

.field public static final HEADER_EVEN:I = 0x1

.field public static final HEADER_FIRST:I = 0x5

.field public static final HEADER_ODD:I = 0x2


# instance fields
.field private _end:I

.field private _start:I


# direct methods
.method public constructor <init>(III)V
    .locals 0
    .param p1, "type"    # I
    .param p2, "startFC"    # I
    .param p3, "endFC"    # I

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput p2, p0, Lorg/apache/poi/hdf/model/hdftypes/HeaderFooter;->_start:I

    .line 43
    iput p3, p0, Lorg/apache/poi/hdf/model/hdftypes/HeaderFooter;->_end:I

    .line 44
    return-void
.end method


# virtual methods
.method public getEnd()I
    .locals 1

    .prologue
    .line 51
    iget v0, p0, Lorg/apache/poi/hdf/model/hdftypes/HeaderFooter;->_end:I

    return v0
.end method

.method public getStart()I
    .locals 1

    .prologue
    .line 47
    iget v0, p0, Lorg/apache/poi/hdf/model/hdftypes/HeaderFooter;->_start:I

    return v0
.end method

.method public isEmpty()Z
    .locals 2

    .prologue
    .line 55
    iget v0, p0, Lorg/apache/poi/hdf/model/hdftypes/HeaderFooter;->_start:I

    iget v1, p0, Lorg/apache/poi/hdf/model/hdftypes/HeaderFooter;->_end:I

    sub-int/2addr v0, v1

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
