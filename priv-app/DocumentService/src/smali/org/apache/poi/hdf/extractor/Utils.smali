.class public final Lorg/apache/poi/hdf/extractor/Utils;
.super Ljava/lang/Object;
.source "Utils.java"


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static convertBytesToInt(BBBB)I
    .locals 6
    .param p0, "firstByte"    # B
    .param p1, "secondByte"    # B
    .param p2, "thirdByte"    # B
    .param p3, "fourthByte"    # B

    .prologue
    .line 36
    and-int/lit16 v0, p0, 0xff

    .line 37
    .local v0, "firstInt":I
    and-int/lit16 v2, p1, 0xff

    .line 38
    .local v2, "secondInt":I
    and-int/lit16 v3, p2, 0xff

    .line 39
    .local v3, "thirdInt":I
    and-int/lit16 v1, p3, 0xff

    .line 41
    .local v1, "fourthInt":I
    shl-int/lit8 v4, v0, 0x18

    shl-int/lit8 v5, v2, 0x10

    or-int/2addr v4, v5

    shl-int/lit8 v5, v3, 0x8

    or-int/2addr v4, v5

    or-int/2addr v4, v1

    return v4
.end method

.method public static convertBytesToInt([BI)I
    .locals 4
    .param p0, "array"    # [B
    .param p1, "offset"    # I

    .prologue
    .line 49
    add-int/lit8 v0, p1, 0x3

    aget-byte v0, p0, v0

    add-int/lit8 v1, p1, 0x2

    aget-byte v1, p0, v1

    add-int/lit8 v2, p1, 0x1

    aget-byte v2, p0, v2

    aget-byte v3, p0, p1

    invoke-static {v0, v1, v2, v3}, Lorg/apache/poi/hdf/extractor/Utils;->convertBytesToInt(BBBB)I

    move-result v0

    return v0
.end method

.method public static convertBytesToShort(BB)S
    .locals 1
    .param p0, "firstByte"    # B
    .param p1, "secondByte"    # B

    .prologue
    const/4 v0, 0x0

    .line 31
    invoke-static {v0, v0, p0, p1}, Lorg/apache/poi/hdf/extractor/Utils;->convertBytesToInt(BBBB)I

    move-result v0

    int-to-short v0, v0

    return v0
.end method

.method public static convertBytesToShort([BI)S
    .locals 2
    .param p0, "array"    # [B
    .param p1, "offset"    # I

    .prologue
    .line 45
    add-int/lit8 v0, p1, 0x1

    aget-byte v0, p0, v0

    aget-byte v1, p0, p1

    invoke-static {v0, v1}, Lorg/apache/poi/hdf/extractor/Utils;->convertBytesToShort(BB)S

    move-result v0

    return v0
.end method

.method public static convertUnsignedByteToInt(B)I
    .locals 1
    .param p0, "b"    # B

    .prologue
    .line 53
    and-int/lit16 v0, p0, 0xff

    return v0
.end method

.method public static getUnicodeCharacter([BI)C
    .locals 1
    .param p0, "array"    # [B
    .param p1, "offset"    # I

    .prologue
    .line 57
    invoke-static {p0, p1}, Lorg/apache/poi/hdf/extractor/Utils;->convertBytesToShort([BI)S

    move-result v0

    int-to-char v0, v0

    return v0
.end method
