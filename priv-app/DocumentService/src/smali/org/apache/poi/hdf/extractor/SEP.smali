.class public final Lorg/apache/poi/hdf/extractor/SEP;
.super Ljava/lang/Object;
.source "SEP.java"


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field _bkc:B

.field _brcBottom:[S

.field _brcLeft:[S

.field _brcRight:[S

.field _brcTop:[S

.field _ccolM1:S

.field _clm:S

.field _cnsPgn:B

.field _dmBinFirst:S

.field _dmBinOther:S

.field _dmOrientFirst:B

.field _dmOrientPage:B

.field _dmPaperReq:S

.field _dxaColumns:I

.field _dxaLeft:I

.field _dxaLnn:I

.field _dxaPgn:S

.field _dxaRight:I

.field _dxtCharSpace:I

.field _dyaBottom:I

.field _dyaHdrBottom:I

.field _dyaHdrTop:I

.field _dyaLinePitch:I

.field _dyaPgn:S

.field _dyaTop:I

.field _dzaGutter:I

.field _fAutoPgn:Z

.field _fEndNote:Z

.field _fEvenlySpaced:Z

.field _fLBetween:Z

.field _fPgnRestart:Z

.field _fPropMark:Z

.field _fTitlePage:Z

.field _fUnlocked:Z

.field _grpfIhdt:B

.field _iHeadingPgn:B

.field _index:I

.field _lnc:B

.field _lnnMin:S

.field _nLnnMod:S

.field _nfcPgn:B

.field _olstAnn:[B

.field _pgbProp:S

.field _pgnStart:S

.field _rgdxaColumnWidthSpacing:[I

.field _vjc:B

.field _wTextFlow:S

.field _xaPage:I

.field _yaPage:I


# direct methods
.method public constructor <init>()V
    .locals 6

    .prologue
    const/16 v5, 0x708

    const/16 v4, 0x5a0

    const/4 v3, 0x1

    const/16 v2, 0x2d0

    const/4 v1, 0x2

    .line 80
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    new-array v0, v1, [S

    iput-object v0, p0, Lorg/apache/poi/hdf/extractor/SEP;->_brcTop:[S

    .line 49
    new-array v0, v1, [S

    iput-object v0, p0, Lorg/apache/poi/hdf/extractor/SEP;->_brcLeft:[S

    .line 50
    new-array v0, v1, [S

    iput-object v0, p0, Lorg/apache/poi/hdf/extractor/SEP;->_brcBottom:[S

    .line 51
    new-array v0, v1, [S

    iput-object v0, p0, Lorg/apache/poi/hdf/extractor/SEP;->_brcRight:[S

    .line 82
    iput-byte v1, p0, Lorg/apache/poi/hdf/extractor/SEP;->_bkc:B

    .line 83
    iput-short v2, p0, Lorg/apache/poi/hdf/extractor/SEP;->_dyaPgn:S

    .line 84
    iput-short v2, p0, Lorg/apache/poi/hdf/extractor/SEP;->_dxaPgn:S

    .line 85
    iput-boolean v3, p0, Lorg/apache/poi/hdf/extractor/SEP;->_fEndNote:Z

    .line 86
    iput-boolean v3, p0, Lorg/apache/poi/hdf/extractor/SEP;->_fEvenlySpaced:Z

    .line 87
    const/16 v0, 0x2fd0

    iput v0, p0, Lorg/apache/poi/hdf/extractor/SEP;->_xaPage:I

    .line 88
    const/16 v0, 0x3de0

    iput v0, p0, Lorg/apache/poi/hdf/extractor/SEP;->_yaPage:I

    .line 89
    iput v2, p0, Lorg/apache/poi/hdf/extractor/SEP;->_dyaHdrTop:I

    .line 90
    iput v2, p0, Lorg/apache/poi/hdf/extractor/SEP;->_dyaHdrBottom:I

    .line 91
    iput-byte v3, p0, Lorg/apache/poi/hdf/extractor/SEP;->_dmOrientPage:B

    .line 92
    iput v2, p0, Lorg/apache/poi/hdf/extractor/SEP;->_dxaColumns:I

    .line 93
    iput v4, p0, Lorg/apache/poi/hdf/extractor/SEP;->_dyaTop:I

    .line 94
    iput v5, p0, Lorg/apache/poi/hdf/extractor/SEP;->_dxaLeft:I

    .line 95
    iput v4, p0, Lorg/apache/poi/hdf/extractor/SEP;->_dyaBottom:I

    .line 96
    iput v5, p0, Lorg/apache/poi/hdf/extractor/SEP;->_dxaRight:I

    .line 97
    iput-short v3, p0, Lorg/apache/poi/hdf/extractor/SEP;->_pgnStart:S

    .line 99
    return-void
.end method
