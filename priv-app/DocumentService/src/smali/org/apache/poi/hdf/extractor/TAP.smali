.class public final Lorg/apache/poi/hdf/extractor/TAP;
.super Ljava/lang/Object;
.source "TAP.java"


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field _brcBottom:[S

.field _brcHorizontal:[S

.field _brcLeft:[S

.field _brcRight:[S

.field _brcTop:[S

.field _brcVertical:[S

.field _dxaGapHalf:I

.field _dyaRowHeight:I

.field _fCantSplit:Z

.field _fLastRow:Z

.field _fTableHeader:Z

.field _itcMac:S

.field _jc:S

.field _rgdxaCenter:[S

.field _rgtc:[Lorg/apache/poi/hdf/extractor/TC;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x2

    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    new-array v0, v1, [S

    iput-object v0, p0, Lorg/apache/poi/hdf/extractor/TAP;->_brcLeft:[S

    .line 37
    new-array v0, v1, [S

    iput-object v0, p0, Lorg/apache/poi/hdf/extractor/TAP;->_brcRight:[S

    .line 38
    new-array v0, v1, [S

    iput-object v0, p0, Lorg/apache/poi/hdf/extractor/TAP;->_brcTop:[S

    .line 39
    new-array v0, v1, [S

    iput-object v0, p0, Lorg/apache/poi/hdf/extractor/TAP;->_brcBottom:[S

    .line 40
    new-array v0, v1, [S

    iput-object v0, p0, Lorg/apache/poi/hdf/extractor/TAP;->_brcHorizontal:[S

    .line 41
    new-array v0, v1, [S

    iput-object v0, p0, Lorg/apache/poi/hdf/extractor/TAP;->_brcVertical:[S

    .line 48
    return-void
.end method
