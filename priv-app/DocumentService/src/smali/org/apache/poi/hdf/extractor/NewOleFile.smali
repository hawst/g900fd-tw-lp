.class public final Lorg/apache/poi/hdf/extractor/NewOleFile;
.super Ljava/io/RandomAccessFile;
.source "NewOleFile.java"


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private LAOLA_ID_ARRAY:[B

.field private _bbd_list:[I

.field protected _big_block_depot:[I

.field private _num_bbd_blocks:I

.field _propertySetsHT:Ljava/util/Hashtable;

.field _propertySetsV:Ljava/util/Vector;

.field private _root_startblock:I

.field private _sbd_startblock:I

.field private _size:J

.field protected _small_block_depot:[I


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "fileName"    # Ljava/lang/String;
    .param p2, "mode"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .prologue
    .line 52
    invoke-direct {p0, p1, p2}, Ljava/io/RandomAccessFile;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 37
    const/16 v1, 0x8

    new-array v1, v1, [B

    fill-array-data v1, :array_0

    .line 39
    iput-object v1, p0, Lorg/apache/poi/hdf/extractor/NewOleFile;->LAOLA_ID_ARRAY:[B

    .line 47
    new-instance v1, Ljava/util/Hashtable;

    invoke-direct {v1}, Ljava/util/Hashtable;-><init>()V

    iput-object v1, p0, Lorg/apache/poi/hdf/extractor/NewOleFile;->_propertySetsHT:Ljava/util/Hashtable;

    .line 48
    new-instance v1, Ljava/util/Vector;

    invoke-direct {v1}, Ljava/util/Vector;-><init>()V

    iput-object v1, p0, Lorg/apache/poi/hdf/extractor/NewOleFile;->_propertySetsV:Ljava/util/Vector;

    .line 55
    :try_start_0
    invoke-direct {p0}, Lorg/apache/poi/hdf/extractor/NewOleFile;->init()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 61
    :goto_0
    return-void

    .line 57
    :catch_0
    move-exception v0

    .line 59
    .local v0, "e":Ljava/lang/Throwable;
    const-string/jumbo v1, "DocumentService"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Exception: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 37
    :array_0
    .array-data 1
        -0x30t
        -0x31t
        0x11t
        -0x20t
        -0x5ft
        -0x4ft
        0x1at
        -0x1ft
    .end array-data
.end method

.method private createSmallBlockDepot()[I
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 184
    iget-object v5, p0, Lorg/apache/poi/hdf/extractor/NewOleFile;->_big_block_depot:[I

    iget v6, p0, Lorg/apache/poi/hdf/extractor/NewOleFile;->_sbd_startblock:I

    invoke-virtual {p0, v5, v6}, Lorg/apache/poi/hdf/extractor/NewOleFile;->readChain([II)[I

    move-result-object v1

    .line 185
    .local v1, "sbd_list":[I
    array-length v5, v1

    mul-int/lit16 v5, v5, 0x80

    new-array v2, v5, [I

    .line 187
    .local v2, "small_block_depot":[I
    const/4 v3, 0x0

    .local v3, "x":I
    :goto_0
    array-length v5, v1

    if-ge v3, v5, :cond_0

    aget v5, v1, v3

    const/4 v6, -0x2

    if-ne v5, v6, :cond_1

    .line 196
    :cond_0
    return-object v2

    .line 189
    :cond_1
    aget v5, v1, v3

    add-int/lit8 v5, v5, 0x1

    mul-int/lit16 v0, v5, 0x200

    .line 190
    .local v0, "offset":I
    int-to-long v6, v0

    invoke-virtual {p0, v6, v7}, Lorg/apache/poi/hdf/extractor/NewOleFile;->seek(J)V

    .line 191
    const/4 v4, 0x0

    .local v4, "y":I
    :goto_1
    const/16 v5, 0x80

    if-lt v4, v5, :cond_2

    .line 187
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 193
    :cond_2
    invoke-direct {p0}, Lorg/apache/poi/hdf/extractor/NewOleFile;->readIntLE()I

    move-result v5

    aput v5, v2, v4

    .line 191
    add-int/lit8 v4, v4, 0x1

    goto :goto_1
.end method

.method private init()V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 66
    const/4 v4, 0x0

    .local v4, "x":I
    :goto_0
    iget-object v6, p0, Lorg/apache/poi/hdf/extractor/NewOleFile;->LAOLA_ID_ARRAY:[B

    array-length v6, v6

    if-lt v4, v6, :cond_0

    .line 73
    invoke-virtual {p0}, Lorg/apache/poi/hdf/extractor/NewOleFile;->length()J

    move-result-wide v6

    iput-wide v6, p0, Lorg/apache/poi/hdf/extractor/NewOleFile;->_size:J

    .line 74
    const-wide/16 v6, 0x2c

    invoke-direct {p0, v6, v7}, Lorg/apache/poi/hdf/extractor/NewOleFile;->readInt(J)I

    move-result v6

    iput v6, p0, Lorg/apache/poi/hdf/extractor/NewOleFile;->_num_bbd_blocks:I

    .line 75
    const-wide/16 v6, 0x30

    invoke-direct {p0, v6, v7}, Lorg/apache/poi/hdf/extractor/NewOleFile;->readInt(J)I

    move-result v6

    iput v6, p0, Lorg/apache/poi/hdf/extractor/NewOleFile;->_root_startblock:I

    .line 76
    const-wide/16 v6, 0x3c

    invoke-direct {p0, v6, v7}, Lorg/apache/poi/hdf/extractor/NewOleFile;->readInt(J)I

    move-result v6

    iput v6, p0, Lorg/apache/poi/hdf/extractor/NewOleFile;->_sbd_startblock:I

    .line 77
    iget v6, p0, Lorg/apache/poi/hdf/extractor/NewOleFile;->_num_bbd_blocks:I

    new-array v6, v6, [I

    iput-object v6, p0, Lorg/apache/poi/hdf/extractor/NewOleFile;->_bbd_list:[I

    .line 80
    iget v6, p0, Lorg/apache/poi/hdf/extractor/NewOleFile;->_num_bbd_blocks:I

    const/16 v7, 0x6d

    if-gt v6, v7, :cond_3

    .line 82
    const-wide/16 v6, 0x4c

    invoke-virtual {p0, v6, v7}, Lorg/apache/poi/hdf/extractor/NewOleFile;->seek(J)V

    .line 83
    const/4 v4, 0x0

    :goto_1
    iget v6, p0, Lorg/apache/poi/hdf/extractor/NewOleFile;->_num_bbd_blocks:I

    if-lt v4, v6, :cond_2

    .line 93
    :goto_2
    iget v6, p0, Lorg/apache/poi/hdf/extractor/NewOleFile;->_num_bbd_blocks:I

    mul-int/lit16 v6, v6, 0x80

    new-array v6, v6, [I

    iput-object v6, p0, Lorg/apache/poi/hdf/extractor/NewOleFile;->_big_block_depot:[I

    .line 94
    const/4 v0, 0x0

    .line 95
    .local v0, "counter":I
    const/4 v4, 0x0

    :goto_3
    iget v6, p0, Lorg/apache/poi/hdf/extractor/NewOleFile;->_num_bbd_blocks:I

    if-lt v4, v6, :cond_4

    .line 104
    invoke-direct {p0}, Lorg/apache/poi/hdf/extractor/NewOleFile;->createSmallBlockDepot()[I

    move-result-object v6

    iput-object v6, p0, Lorg/apache/poi/hdf/extractor/NewOleFile;->_small_block_depot:[I

    .line 105
    iget-object v6, p0, Lorg/apache/poi/hdf/extractor/NewOleFile;->_big_block_depot:[I

    iget v7, p0, Lorg/apache/poi/hdf/extractor/NewOleFile;->_root_startblock:I

    invoke-virtual {p0, v6, v7}, Lorg/apache/poi/hdf/extractor/NewOleFile;->readChain([II)[I

    move-result-object v3

    .line 106
    .local v3, "rootChain":[I
    invoke-direct {p0, v3}, Lorg/apache/poi/hdf/extractor/NewOleFile;->initializePropertySets([I)V

    .line 108
    return-void

    .line 68
    .end local v0    # "counter":I
    .end local v3    # "rootChain":[I
    :cond_0
    iget-object v6, p0, Lorg/apache/poi/hdf/extractor/NewOleFile;->LAOLA_ID_ARRAY:[B

    aget-byte v6, v6, v4

    invoke-virtual {p0}, Lorg/apache/poi/hdf/extractor/NewOleFile;->readByte()B

    move-result v7

    if-eq v6, v7, :cond_1

    .line 70
    new-instance v6, Ljava/io/IOException;

    const-string/jumbo v7, "Not an OLE file"

    invoke-direct {v6, v7}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 66
    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 85
    :cond_2
    iget-object v6, p0, Lorg/apache/poi/hdf/extractor/NewOleFile;->_bbd_list:[I

    invoke-direct {p0}, Lorg/apache/poi/hdf/extractor/NewOleFile;->readIntLE()I

    move-result v7

    aput v7, v6, v4

    .line 83
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 90
    :cond_3
    invoke-direct {p0}, Lorg/apache/poi/hdf/extractor/NewOleFile;->populateBbdList()V

    goto :goto_2

    .line 97
    .restart local v0    # "counter":I
    :cond_4
    iget-object v6, p0, Lorg/apache/poi/hdf/extractor/NewOleFile;->_bbd_list:[I

    aget v6, v6, v4

    add-int/lit8 v6, v6, 0x1

    mul-int/lit16 v2, v6, 0x200

    .line 98
    .local v2, "offset":I
    int-to-long v6, v2

    invoke-virtual {p0, v6, v7}, Lorg/apache/poi/hdf/extractor/NewOleFile;->seek(J)V

    .line 99
    const/4 v5, 0x0

    .local v5, "y":I
    move v1, v0

    .end local v0    # "counter":I
    .local v1, "counter":I
    :goto_4
    const/16 v6, 0x80

    if-lt v5, v6, :cond_5

    .line 95
    add-int/lit8 v4, v4, 0x1

    move v0, v1

    .end local v1    # "counter":I
    .restart local v0    # "counter":I
    goto :goto_3

    .line 101
    .end local v0    # "counter":I
    .restart local v1    # "counter":I
    :cond_5
    iget-object v6, p0, Lorg/apache/poi/hdf/extractor/NewOleFile;->_big_block_depot:[I

    add-int/lit8 v0, v1, 0x1

    .end local v1    # "counter":I
    .restart local v0    # "counter":I
    invoke-direct {p0}, Lorg/apache/poi/hdf/extractor/NewOleFile;->readIntLE()I

    move-result v7

    aput v7, v6, v1

    .line 99
    add-int/lit8 v5, v5, 0x1

    move v1, v0

    .end local v0    # "counter":I
    .restart local v1    # "counter":I
    goto :goto_4
.end method

.method private initializePropertySets([I)V
    .locals 22
    .param p1, "rootChain"    # [I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 144
    const/16 v17, 0x0

    .local v17, "x":I
    :goto_0
    move-object/from16 v0, p1

    array-length v5, v0

    move/from16 v0, v17

    if-lt v0, v5, :cond_0

    .line 180
    return-void

    .line 146
    :cond_0
    aget v5, p1, v17

    add-int/lit8 v5, v5, 0x1

    mul-int/lit16 v15, v5, 0x200

    .line 147
    .local v15, "offset":I
    int-to-long v0, v15

    move-wide/from16 v20, v0

    move-object/from16 v0, p0

    move-wide/from16 v1, v20

    invoke-virtual {v0, v1, v2}, Lorg/apache/poi/hdf/extractor/NewOleFile;->seek(J)V

    .line 148
    const/16 v18, 0x0

    .local v18, "y":I
    :goto_1
    const/4 v5, 0x4

    move/from16 v0, v18

    if-lt v0, v5, :cond_1

    .line 144
    add-int/lit8 v17, v17, 0x1

    goto :goto_0

    .line 151
    :cond_1
    const/16 v5, 0x80

    new-array v0, v5, [B

    move-object/from16 v16, v0

    .line 152
    .local v16, "propArray":[B
    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Lorg/apache/poi/hdf/extractor/NewOleFile;->read([B)I

    .line 155
    const/16 v5, 0x41

    aget-byte v5, v16, v5

    const/16 v12, 0x40

    aget-byte v12, v16, v12

    invoke-static {v5, v12}, Lorg/apache/poi/hdf/extractor/Utils;->convertBytesToShort(BB)S

    move-result v5

    div-int/lit8 v5, v5, 0x2

    add-int/lit8 v14, v5, -0x1

    .line 156
    .local v14, "nameSize":I
    if-lez v14, :cond_2

    .line 158
    new-instance v13, Ljava/lang/StringBuffer;

    invoke-direct {v13, v14}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 159
    .local v13, "nameBuffer":Ljava/lang/StringBuffer;
    const/16 v19, 0x0

    .local v19, "z":I
    :goto_2
    move/from16 v0, v19

    if-lt v0, v14, :cond_3

    .line 163
    const/16 v5, 0x42

    aget-byte v6, v16, v5

    .line 164
    .local v6, "type":I
    const/16 v5, 0x47

    aget-byte v5, v16, v5

    const/16 v12, 0x46

    aget-byte v12, v16, v12

    const/16 v20, 0x45

    aget-byte v20, v16, v20

    const/16 v21, 0x44

    aget-byte v21, v16, v21

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-static {v5, v12, v0, v1}, Lorg/apache/poi/hdf/extractor/Utils;->convertBytesToInt(BBBB)I

    move-result v7

    .line 165
    .local v7, "previous_pps":I
    const/16 v5, 0x4b

    aget-byte v5, v16, v5

    const/16 v12, 0x4a

    aget-byte v12, v16, v12

    const/16 v20, 0x49

    aget-byte v20, v16, v20

    const/16 v21, 0x48

    aget-byte v21, v16, v21

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-static {v5, v12, v0, v1}, Lorg/apache/poi/hdf/extractor/Utils;->convertBytesToInt(BBBB)I

    move-result v8

    .line 166
    .local v8, "next_pps":I
    const/16 v5, 0x4f

    aget-byte v5, v16, v5

    const/16 v12, 0x4e

    aget-byte v12, v16, v12

    const/16 v20, 0x4d

    aget-byte v20, v16, v20

    const/16 v21, 0x4c

    aget-byte v21, v16, v21

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-static {v5, v12, v0, v1}, Lorg/apache/poi/hdf/extractor/Utils;->convertBytesToInt(BBBB)I

    move-result v9

    .line 167
    .local v9, "pps_dir":I
    const/16 v5, 0x77

    aget-byte v5, v16, v5

    const/16 v12, 0x76

    aget-byte v12, v16, v12

    const/16 v20, 0x75

    aget-byte v20, v16, v20

    const/16 v21, 0x74

    aget-byte v21, v16, v21

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-static {v5, v12, v0, v1}, Lorg/apache/poi/hdf/extractor/Utils;->convertBytesToInt(BBBB)I

    move-result v10

    .line 168
    .local v10, "pps_sb":I
    const/16 v5, 0x7b

    aget-byte v5, v16, v5

    const/16 v12, 0x7a

    aget-byte v12, v16, v12

    const/16 v20, 0x79

    aget-byte v20, v16, v20

    const/16 v21, 0x78

    aget-byte v21, v16, v21

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-static {v5, v12, v0, v1}, Lorg/apache/poi/hdf/extractor/Utils;->convertBytesToInt(BBBB)I

    move-result v11

    .line 170
    .local v11, "pps_size":I
    new-instance v4, Lorg/apache/poi/hdf/extractor/PropertySet;

    invoke-virtual {v13}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    .line 173
    mul-int/lit8 v12, v17, 0x4

    add-int v12, v12, v18

    .line 170
    invoke-direct/range {v4 .. v12}, Lorg/apache/poi/hdf/extractor/PropertySet;-><init>(Ljava/lang/String;IIIIIII)V

    .line 174
    .local v4, "propSet":Lorg/apache/poi/hdf/extractor/PropertySet;
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/poi/hdf/extractor/NewOleFile;->_propertySetsHT:Ljava/util/Hashtable;

    invoke-virtual {v13}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v5, v12, v4}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 175
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/poi/hdf/extractor/NewOleFile;->_propertySetsV:Ljava/util/Vector;

    invoke-virtual {v5, v4}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 148
    .end local v4    # "propSet":Lorg/apache/poi/hdf/extractor/PropertySet;
    .end local v6    # "type":I
    .end local v7    # "previous_pps":I
    .end local v8    # "next_pps":I
    .end local v9    # "pps_dir":I
    .end local v10    # "pps_sb":I
    .end local v11    # "pps_size":I
    .end local v13    # "nameBuffer":Ljava/lang/StringBuffer;
    .end local v19    # "z":I
    :cond_2
    add-int/lit8 v18, v18, 0x1

    goto/16 :goto_1

    .line 161
    .restart local v13    # "nameBuffer":Ljava/lang/StringBuffer;
    .restart local v19    # "z":I
    :cond_3
    mul-int/lit8 v5, v19, 0x2

    aget-byte v5, v16, v5

    int-to-char v5, v5

    invoke-virtual {v13, v5}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 159
    add-int/lit8 v19, v19, 0x1

    goto/16 :goto_2
.end method

.method public static main([Ljava/lang/String;)V
    .locals 0
    .param p0, "args"    # [Ljava/lang/String;

    .prologue
    .line 119
    return-void
.end method

.method private populateBbdList()V
    .locals 13
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v12, 0x7f

    .line 201
    const-wide/16 v10, 0x4c

    invoke-virtual {p0, v10, v11}, Lorg/apache/poi/hdf/extractor/NewOleFile;->seek(J)V

    .line 202
    const/4 v6, 0x0

    .local v6, "x":I
    :goto_0
    const/16 v9, 0x6d

    if-lt v6, v9, :cond_0

    .line 206
    const/16 v2, 0x6d

    .line 207
    .local v2, "pos":I
    iget v9, p0, Lorg/apache/poi/hdf/extractor/NewOleFile;->_num_bbd_blocks:I

    add-int/lit8 v4, v9, -0x6d

    .line 208
    .local v4, "remainder":I
    const-wide/16 v10, 0x48

    invoke-virtual {p0, v10, v11}, Lorg/apache/poi/hdf/extractor/NewOleFile;->seek(J)V

    .line 209
    invoke-direct {p0}, Lorg/apache/poi/hdf/extractor/NewOleFile;->readIntLE()I

    move-result v1

    .line 210
    .local v1, "numLists":I
    const-wide/16 v10, 0x44

    invoke-virtual {p0, v10, v11}, Lorg/apache/poi/hdf/extractor/NewOleFile;->seek(J)V

    .line 211
    invoke-direct {p0}, Lorg/apache/poi/hdf/extractor/NewOleFile;->readIntLE()I

    move-result v0

    .line 213
    .local v0, "firstList":I
    add-int/lit8 v9, v0, 0x1

    mul-int/lit16 v0, v9, 0x200

    .line 215
    const/4 v7, 0x0

    .local v7, "y":I
    :goto_1
    if-lt v7, v1, :cond_1

    .line 232
    return-void

    .line 204
    .end local v0    # "firstList":I
    .end local v1    # "numLists":I
    .end local v2    # "pos":I
    .end local v4    # "remainder":I
    .end local v7    # "y":I
    :cond_0
    iget-object v9, p0, Lorg/apache/poi/hdf/extractor/NewOleFile;->_bbd_list:[I

    invoke-direct {p0}, Lorg/apache/poi/hdf/extractor/NewOleFile;->readIntLE()I

    move-result v10

    aput v10, v9, v6

    .line 202
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 217
    .restart local v0    # "firstList":I
    .restart local v1    # "numLists":I
    .restart local v2    # "pos":I
    .restart local v4    # "remainder":I
    .restart local v7    # "y":I
    :cond_1
    invoke-static {v12, v4}, Ljava/lang/Math;->min(II)I

    move-result v5

    .line 218
    .local v5, "size":I
    const/4 v8, 0x0

    .local v8, "z":I
    move v3, v2

    .end local v2    # "pos":I
    .local v3, "pos":I
    :goto_2
    if-lt v8, v5, :cond_3

    .line 223
    if-ne v5, v12, :cond_2

    .line 225
    add-int/lit16 v9, v0, 0x1fc

    int-to-long v10, v9

    invoke-virtual {p0, v10, v11}, Lorg/apache/poi/hdf/extractor/NewOleFile;->seek(J)V

    .line 226
    invoke-direct {p0}, Lorg/apache/poi/hdf/extractor/NewOleFile;->readIntLE()I

    move-result v0

    .line 227
    add-int/lit8 v9, v0, 0x1

    mul-int/lit16 v0, v9, 0x200

    .line 228
    add-int/lit8 v4, v4, -0x7f

    .line 215
    :cond_2
    add-int/lit8 v7, v7, 0x1

    move v2, v3

    .end local v3    # "pos":I
    .restart local v2    # "pos":I
    goto :goto_1

    .line 220
    .end local v2    # "pos":I
    .restart local v3    # "pos":I
    :cond_3
    mul-int/lit8 v9, v8, 0x4

    add-int/2addr v9, v0

    int-to-long v10, v9

    invoke-virtual {p0, v10, v11}, Lorg/apache/poi/hdf/extractor/NewOleFile;->seek(J)V

    .line 221
    iget-object v9, p0, Lorg/apache/poi/hdf/extractor/NewOleFile;->_bbd_list:[I

    add-int/lit8 v2, v3, 0x1

    .end local v3    # "pos":I
    .restart local v2    # "pos":I
    invoke-direct {p0}, Lorg/apache/poi/hdf/extractor/NewOleFile;->readIntLE()I

    move-result v10

    aput v10, v9, v3

    .line 218
    add-int/lit8 v8, v8, 0x1

    move v3, v2

    .end local v2    # "pos":I
    .restart local v3    # "pos":I
    goto :goto_2
.end method

.method private readInt(J)I
    .locals 1
    .param p1, "offset"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 235
    invoke-virtual {p0, p1, p2}, Lorg/apache/poi/hdf/extractor/NewOleFile;->seek(J)V

    .line 236
    invoke-direct {p0}, Lorg/apache/poi/hdf/extractor/NewOleFile;->readIntLE()I

    move-result v0

    return v0
.end method

.method private readIntLE()I
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 240
    const/4 v1, 0x4

    new-array v0, v1, [B

    .line 241
    .local v0, "intBytes":[B
    invoke-virtual {p0, v0}, Lorg/apache/poi/hdf/extractor/NewOleFile;->read([B)I

    .line 242
    const/4 v1, 0x3

    aget-byte v1, v0, v1

    const/4 v2, 0x2

    aget-byte v2, v0, v2

    const/4 v3, 0x1

    aget-byte v3, v0, v3

    const/4 v4, 0x0

    aget-byte v4, v0, v4

    invoke-static {v1, v2, v3, v4}, Lorg/apache/poi/hdf/extractor/Utils;->convertBytesToInt(BBBB)I

    move-result v1

    return v1
.end method


# virtual methods
.method protected readChain([II)[I
    .locals 6
    .param p1, "blockChain"    # [I
    .param p2, "startBlock"    # I

    .prologue
    const/4 v5, 0x0

    .line 122
    array-length v4, p1

    new-array v2, v4, [I

    .line 123
    .local v2, "tempChain":[I
    aput p2, v2, v5

    .line 124
    const/4 v3, 0x1

    .line 127
    .local v3, "x":I
    :goto_0
    add-int/lit8 v4, v3, -0x1

    aget v4, v2, v4

    aget v1, p1, v4

    .line 128
    .local v1, "nextVal":I
    const/4 v4, -0x2

    if-eq v1, v4, :cond_0

    .line 130
    aput v1, v2, v3

    .line 125
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 137
    :cond_0
    new-array v0, v3, [I

    .line 138
    .local v0, "newChain":[I
    invoke-static {v2, v5, v0, v5, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 140
    return-object v0
.end method
