.class public final Lorg/apache/poi/hdf/model/hdftypes/PlexOfCps;
.super Ljava/lang/Object;
.source "PlexOfCps.java"


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private _count:I

.field private _sizeOfStruct:I


# direct methods
.method public constructor <init>(II)V
    .locals 2
    .param p1, "size"    # I
    .param p2, "sizeOfStruct"    # I

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    add-int/lit8 v0, p1, -0x4

    add-int/lit8 v1, p2, 0x4

    div-int/2addr v0, v1

    iput v0, p0, Lorg/apache/poi/hdf/model/hdftypes/PlexOfCps;->_count:I

    .line 49
    iput p2, p0, Lorg/apache/poi/hdf/model/hdftypes/PlexOfCps;->_sizeOfStruct:I

    .line 50
    return-void
.end method


# virtual methods
.method public getIntOffset(I)I
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 53
    mul-int/lit8 v0, p1, 0x4

    return v0
.end method

.method public getStructOffset(I)I
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 75
    iget v0, p0, Lorg/apache/poi/hdf/model/hdftypes/PlexOfCps;->_count:I

    add-int/lit8 v0, v0, 0x1

    mul-int/lit8 v0, v0, 0x4

    iget v1, p0, Lorg/apache/poi/hdf/model/hdftypes/PlexOfCps;->_sizeOfStruct:I

    mul-int/2addr v1, p1

    add-int/2addr v0, v1

    return v0
.end method

.method public length()I
    .locals 1

    .prologue
    .line 62
    iget v0, p0, Lorg/apache/poi/hdf/model/hdftypes/PlexOfCps;->_count:I

    return v0
.end method
