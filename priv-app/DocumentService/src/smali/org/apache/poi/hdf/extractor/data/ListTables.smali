.class public final Lorg/apache/poi/hdf/extractor/data/ListTables;
.super Ljava/lang/Object;
.source "ListTables.java"


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field _lists:Ljava/util/Hashtable;

.field _pllfo:[Lorg/apache/poi/hdf/extractor/data/LFO;


# direct methods
.method public constructor <init>([B[B)V
    .locals 1
    .param p1, "plcflst"    # [B
    .param p2, "plflfo"    # [B

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/hdf/extractor/data/ListTables;->_lists:Ljava/util/Hashtable;

    .line 39
    invoke-direct {p0, p1}, Lorg/apache/poi/hdf/extractor/data/ListTables;->initLST([B)V

    .line 40
    invoke-direct {p0, p2}, Lorg/apache/poi/hdf/extractor/data/ListTables;->initLFO([B)V

    .line 41
    return-void
.end method

.method private createLVL([BILorg/apache/poi/hdf/extractor/data/LVL;)I
    .locals 9
    .param p1, "data"    # [B
    .param p2, "offset"    # I
    .param p3, "lvl"    # Lorg/apache/poi/hdf/extractor/data/LVL;

    .prologue
    const/4 v8, 0x0

    .line 155
    invoke-static {p1, p2}, Lorg/apache/poi/hdf/extractor/Utils;->convertBytesToInt([BI)I

    move-result v5

    iput v5, p3, Lorg/apache/poi/hdf/extractor/data/LVL;->_iStartAt:I

    .line 156
    add-int/lit8 v5, p2, 0x4

    aget-byte v5, p1, v5

    iput-byte v5, p3, Lorg/apache/poi/hdf/extractor/data/LVL;->_nfc:B

    .line 157
    add-int/lit8 v5, p2, 0x5

    invoke-static {p1, v5}, Lorg/apache/poi/hdf/extractor/Utils;->convertBytesToInt([BI)I

    move-result v1

    .line 158
    .local v1, "code":I
    and-int/lit8 v5, v1, 0x3

    int-to-byte v5, v5

    iput-byte v5, p3, Lorg/apache/poi/hdf/extractor/data/LVL;->_jc:B

    .line 159
    and-int/lit8 v5, v1, 0x4

    invoke-static {v5}, Lorg/apache/poi/hdf/extractor/StyleSheet;->getFlag(I)Z

    move-result v5

    iput-boolean v5, p3, Lorg/apache/poi/hdf/extractor/data/LVL;->_fLegal:Z

    .line 160
    and-int/lit8 v5, v1, 0x8

    invoke-static {v5}, Lorg/apache/poi/hdf/extractor/StyleSheet;->getFlag(I)Z

    move-result v5

    iput-boolean v5, p3, Lorg/apache/poi/hdf/extractor/data/LVL;->_fNoRestart:Z

    .line 161
    and-int/lit8 v5, v1, 0x10

    invoke-static {v5}, Lorg/apache/poi/hdf/extractor/StyleSheet;->getFlag(I)Z

    move-result v5

    iput-boolean v5, p3, Lorg/apache/poi/hdf/extractor/data/LVL;->_fPrev:Z

    .line 162
    and-int/lit8 v5, v1, 0x20

    invoke-static {v5}, Lorg/apache/poi/hdf/extractor/StyleSheet;->getFlag(I)Z

    move-result v5

    iput-boolean v5, p3, Lorg/apache/poi/hdf/extractor/data/LVL;->_fPrevSpace:Z

    .line 163
    and-int/lit8 v5, v1, 0x40

    invoke-static {v5}, Lorg/apache/poi/hdf/extractor/StyleSheet;->getFlag(I)Z

    move-result v5

    iput-boolean v5, p3, Lorg/apache/poi/hdf/extractor/data/LVL;->_fWord6:Z

    .line 164
    add-int/lit8 v5, p2, 0x6

    iget-object v6, p3, Lorg/apache/poi/hdf/extractor/data/LVL;->_rgbxchNums:[B

    const/16 v7, 0x9

    invoke-static {p1, v5, v6, v8, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 165
    add-int/lit8 v5, p2, 0xf

    aget-byte v5, p1, v5

    iput-byte v5, p3, Lorg/apache/poi/hdf/extractor/data/LVL;->_ixchFollow:B

    .line 166
    add-int/lit8 v5, p2, 0x18

    aget-byte v0, p1, v5

    .line 167
    .local v0, "chpxSize":I
    add-int/lit8 v5, p2, 0x19

    aget-byte v2, p1, v5

    .line 168
    .local v2, "papxSize":I
    new-array v5, v0, [B

    iput-object v5, p3, Lorg/apache/poi/hdf/extractor/data/LVL;->_chpx:[B

    .line 169
    new-array v5, v2, [B

    iput-object v5, p3, Lorg/apache/poi/hdf/extractor/data/LVL;->_papx:[B

    .line 170
    add-int/lit8 v5, p2, 0x1c

    iget-object v6, p3, Lorg/apache/poi/hdf/extractor/data/LVL;->_papx:[B

    invoke-static {p1, v5, v6, v8, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 171
    add-int/lit8 v5, p2, 0x1c

    add-int/2addr v5, v2

    iget-object v6, p3, Lorg/apache/poi/hdf/extractor/data/LVL;->_chpx:[B

    invoke-static {p1, v5, v6, v8, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 172
    add-int/lit8 v5, v2, 0x1c

    add-int/2addr v5, v0

    add-int/2addr p2, v5

    .line 173
    invoke-static {p1, p2}, Lorg/apache/poi/hdf/extractor/Utils;->convertBytesToShort([BI)S

    move-result v4

    .line 174
    .local v4, "xstSize":I
    new-array v5, v4, [C

    iput-object v5, p3, Lorg/apache/poi/hdf/extractor/data/LVL;->_xst:[C

    .line 176
    add-int/lit8 p2, p2, 0x2

    .line 177
    const/4 v3, 0x0

    .local v3, "x":I
    :goto_0
    if-lt v3, v4, :cond_0

    .line 181
    add-int/lit8 v5, v2, 0x1c

    add-int/2addr v5, v0

    add-int/lit8 v5, v5, 0x2

    mul-int/lit8 v6, v4, 0x2

    add-int/2addr v5, v6

    return v5

    .line 179
    :cond_0
    iget-object v5, p3, Lorg/apache/poi/hdf/extractor/data/LVL;->_xst:[C

    mul-int/lit8 v6, v3, 0x2

    add-int/2addr v6, p2

    invoke-static {p1, v6}, Lorg/apache/poi/hdf/extractor/Utils;->convertBytesToShort([BI)S

    move-result v6

    int-to-char v6, v6

    aput-char v6, v5, v3

    .line 177
    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method

.method private initLFO([B)V
    .locals 10
    .param p1, "plflfo"    # [B

    .prologue
    .line 115
    const/4 v9, 0x0

    invoke-static {p1, v9}, Lorg/apache/poi/hdf/extractor/Utils;->convertBytesToInt([BI)I

    move-result v0

    .line 116
    .local v0, "lfoSize":I
    new-array v9, v0, [Lorg/apache/poi/hdf/extractor/data/LFO;

    iput-object v9, p0, Lorg/apache/poi/hdf/extractor/data/ListTables;->_pllfo:[Lorg/apache/poi/hdf/extractor/data/LFO;

    .line 117
    const/4 v7, 0x0

    .local v7, "x":I
    :goto_0
    if-lt v7, v0, :cond_0

    .line 126
    mul-int/lit8 v9, v0, 0x10

    add-int/lit8 v3, v9, 0x4

    .line 127
    .local v3, "lfolvlOffset":I
    const/4 v4, 0x0

    .line 128
    .local v4, "lvlOffset":I
    const/4 v2, 0x0

    .line 129
    .local v2, "lfolvlNum":I
    const/4 v7, 0x0

    :goto_1
    if-lt v7, v0, :cond_1

    .line 151
    return-void

    .line 119
    .end local v2    # "lfolvlNum":I
    .end local v3    # "lfolvlOffset":I
    .end local v4    # "lvlOffset":I
    :cond_0
    new-instance v5, Lorg/apache/poi/hdf/extractor/data/LFO;

    invoke-direct {v5}, Lorg/apache/poi/hdf/extractor/data/LFO;-><init>()V

    .line 120
    .local v5, "nextLFO":Lorg/apache/poi/hdf/extractor/data/LFO;
    mul-int/lit8 v9, v7, 0x10

    add-int/lit8 v9, v9, 0x4

    invoke-static {p1, v9}, Lorg/apache/poi/hdf/extractor/Utils;->convertBytesToInt([BI)I

    move-result v9

    iput v9, v5, Lorg/apache/poi/hdf/extractor/data/LFO;->_lsid:I

    .line 121
    mul-int/lit8 v9, v7, 0x10

    add-int/lit8 v9, v9, 0x10

    aget-byte v9, p1, v9

    iput v9, v5, Lorg/apache/poi/hdf/extractor/data/LFO;->_clfolvl:I

    .line 122
    iget v9, v5, Lorg/apache/poi/hdf/extractor/data/LFO;->_clfolvl:I

    new-array v9, v9, [Lorg/apache/poi/hdf/extractor/data/LFOLVL;

    iput-object v9, v5, Lorg/apache/poi/hdf/extractor/data/LFO;->_levels:[Lorg/apache/poi/hdf/extractor/data/LFOLVL;

    .line 123
    iget-object v9, p0, Lorg/apache/poi/hdf/extractor/data/ListTables;->_pllfo:[Lorg/apache/poi/hdf/extractor/data/LFO;

    aput-object v5, v9, v7

    .line 117
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 131
    .end local v5    # "nextLFO":Lorg/apache/poi/hdf/extractor/data/LFO;
    .restart local v2    # "lfolvlNum":I
    .restart local v3    # "lfolvlOffset":I
    .restart local v4    # "lvlOffset":I
    :cond_1
    const/4 v8, 0x0

    .local v8, "y":I
    :goto_2
    iget-object v9, p0, Lorg/apache/poi/hdf/extractor/data/ListTables;->_pllfo:[Lorg/apache/poi/hdf/extractor/data/LFO;

    aget-object v9, v9, v7

    iget v9, v9, Lorg/apache/poi/hdf/extractor/data/LFO;->_clfolvl:I

    if-lt v8, v9, :cond_2

    .line 129
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 133
    :cond_2
    mul-int/lit8 v9, v2, 0x8

    add-int/2addr v9, v3

    add-int v6, v9, v4

    .line 134
    .local v6, "offset":I
    new-instance v1, Lorg/apache/poi/hdf/extractor/data/LFOLVL;

    invoke-direct {v1}, Lorg/apache/poi/hdf/extractor/data/LFOLVL;-><init>()V

    .line 135
    .local v1, "lfolvl":Lorg/apache/poi/hdf/extractor/data/LFOLVL;
    invoke-static {p1, v6}, Lorg/apache/poi/hdf/extractor/Utils;->convertBytesToInt([BI)I

    move-result v9

    iput v9, v1, Lorg/apache/poi/hdf/extractor/data/LFOLVL;->_iStartAt:I

    .line 136
    add-int/lit8 v9, v6, 0x4

    invoke-static {p1, v9}, Lorg/apache/poi/hdf/extractor/Utils;->convertBytesToInt([BI)I

    move-result v9

    iput v9, v1, Lorg/apache/poi/hdf/extractor/data/LFOLVL;->_ilvl:I

    .line 137
    iget v9, v1, Lorg/apache/poi/hdf/extractor/data/LFOLVL;->_ilvl:I

    and-int/lit8 v9, v9, 0x10

    invoke-static {v9}, Lorg/apache/poi/hdf/extractor/StyleSheet;->getFlag(I)Z

    move-result v9

    iput-boolean v9, v1, Lorg/apache/poi/hdf/extractor/data/LFOLVL;->_fStartAt:Z

    .line 138
    iget v9, v1, Lorg/apache/poi/hdf/extractor/data/LFOLVL;->_ilvl:I

    and-int/lit8 v9, v9, 0x20

    invoke-static {v9}, Lorg/apache/poi/hdf/extractor/StyleSheet;->getFlag(I)Z

    move-result v9

    iput-boolean v9, v1, Lorg/apache/poi/hdf/extractor/data/LFOLVL;->_fFormatting:Z

    .line 139
    iget v9, v1, Lorg/apache/poi/hdf/extractor/data/LFOLVL;->_ilvl:I

    and-int/lit8 v9, v9, 0xf

    iput v9, v1, Lorg/apache/poi/hdf/extractor/data/LFOLVL;->_ilvl:I

    .line 140
    add-int/lit8 v2, v2, 0x1

    .line 142
    iget-boolean v9, v1, Lorg/apache/poi/hdf/extractor/data/LFOLVL;->_fFormatting:Z

    if-eqz v9, :cond_3

    .line 144
    mul-int/lit8 v9, v2, 0xc

    add-int/2addr v9, v3

    add-int v6, v9, v4

    .line 145
    new-instance v9, Lorg/apache/poi/hdf/extractor/data/LVL;

    invoke-direct {v9}, Lorg/apache/poi/hdf/extractor/data/LVL;-><init>()V

    iput-object v9, v1, Lorg/apache/poi/hdf/extractor/data/LFOLVL;->_override:Lorg/apache/poi/hdf/extractor/data/LVL;

    .line 146
    iget-object v9, v1, Lorg/apache/poi/hdf/extractor/data/LFOLVL;->_override:Lorg/apache/poi/hdf/extractor/data/LVL;

    invoke-direct {p0, p1, v6, v9}, Lorg/apache/poi/hdf/extractor/data/ListTables;->createLVL([BILorg/apache/poi/hdf/extractor/data/LVL;)I

    move-result v9

    add-int/2addr v4, v9

    .line 148
    :cond_3
    iget-object v9, p0, Lorg/apache/poi/hdf/extractor/data/ListTables;->_pllfo:[Lorg/apache/poi/hdf/extractor/data/LFO;

    aget-object v9, v9, v7

    iget-object v9, v9, Lorg/apache/poi/hdf/extractor/data/LFO;->_levels:[Lorg/apache/poi/hdf/extractor/data/LFOLVL;

    aput-object v1, v9, v8

    .line 131
    add-int/lit8 v8, v8, 0x1

    goto :goto_2
.end method

.method private initLST([B)V
    .locals 11
    .param p1, "plcflst"    # [B

    .prologue
    const/4 v10, 0x0

    .line 80
    invoke-static {p1, v10}, Lorg/apache/poi/hdf/extractor/Utils;->convertBytesToShort([BI)S

    move-result v1

    .line 81
    .local v1, "length":S
    const/4 v3, 0x0

    .line 83
    .local v3, "nextLevelOffset":I
    const/4 v5, 0x0

    .local v5, "x":I
    :goto_0
    if-lt v5, v1, :cond_0

    .line 112
    return-void

    .line 85
    :cond_0
    new-instance v2, Lorg/apache/poi/hdf/extractor/data/LST;

    invoke-direct {v2}, Lorg/apache/poi/hdf/extractor/data/LST;-><init>()V

    .line 86
    .local v2, "lst":Lorg/apache/poi/hdf/extractor/data/LST;
    mul-int/lit8 v7, v5, 0x1c

    add-int/lit8 v7, v7, 0x2

    invoke-static {p1, v7}, Lorg/apache/poi/hdf/extractor/Utils;->convertBytesToInt([BI)I

    move-result v7

    iput v7, v2, Lorg/apache/poi/hdf/extractor/data/LST;->_lsid:I

    .line 87
    mul-int/lit8 v7, v5, 0x1c

    add-int/lit8 v7, v7, 0x6

    invoke-static {p1, v7}, Lorg/apache/poi/hdf/extractor/Utils;->convertBytesToInt([BI)I

    move-result v7

    iput v7, v2, Lorg/apache/poi/hdf/extractor/data/LST;->_tplc:I

    .line 88
    mul-int/lit8 v7, v5, 0x1c

    add-int/lit8 v7, v7, 0xa

    iget-object v8, v2, Lorg/apache/poi/hdf/extractor/data/LST;->_rgistd:[B

    const/16 v9, 0x12

    invoke-static {p1, v7, v8, v10, v9}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 89
    mul-int/lit8 v7, v5, 0x1c

    add-int/lit8 v7, v7, 0x1c

    aget-byte v0, p1, v7

    .line 90
    .local v0, "code":B
    and-int/lit8 v7, v0, 0x1

    invoke-static {v7}, Lorg/apache/poi/hdf/extractor/StyleSheet;->getFlag(I)Z

    move-result v7

    iput-boolean v7, v2, Lorg/apache/poi/hdf/extractor/data/LST;->_fSimpleList:Z

    .line 92
    iget-object v7, p0, Lorg/apache/poi/hdf/extractor/data/ListTables;->_lists:Ljava/util/Hashtable;

    iget v8, v2, Lorg/apache/poi/hdf/extractor/data/LST;->_lsid:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v7, v8, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 94
    iget-boolean v7, v2, Lorg/apache/poi/hdf/extractor/data/LST;->_fSimpleList:Z

    if-eqz v7, :cond_1

    .line 96
    const/4 v7, 0x1

    new-array v7, v7, [Lorg/apache/poi/hdf/extractor/data/LVL;

    iput-object v7, v2, Lorg/apache/poi/hdf/extractor/data/LST;->_levels:[Lorg/apache/poi/hdf/extractor/data/LVL;

    .line 103
    :goto_1
    const/4 v6, 0x0

    .local v6, "y":I
    :goto_2
    iget-object v7, v2, Lorg/apache/poi/hdf/extractor/data/LST;->_levels:[Lorg/apache/poi/hdf/extractor/data/LVL;

    array-length v7, v7

    if-lt v6, v7, :cond_2

    .line 83
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 100
    .end local v6    # "y":I
    :cond_1
    const/16 v7, 0x9

    new-array v7, v7, [Lorg/apache/poi/hdf/extractor/data/LVL;

    iput-object v7, v2, Lorg/apache/poi/hdf/extractor/data/LST;->_levels:[Lorg/apache/poi/hdf/extractor/data/LVL;

    goto :goto_1

    .line 105
    .restart local v6    # "y":I
    :cond_2
    mul-int/lit8 v7, v1, 0x1c

    add-int/lit8 v7, v7, 0x2

    add-int v4, v7, v3

    .line 106
    .local v4, "offset":I
    iget-object v7, v2, Lorg/apache/poi/hdf/extractor/data/LST;->_levels:[Lorg/apache/poi/hdf/extractor/data/LVL;

    new-instance v8, Lorg/apache/poi/hdf/extractor/data/LVL;

    invoke-direct {v8}, Lorg/apache/poi/hdf/extractor/data/LVL;-><init>()V

    aput-object v8, v7, v6

    .line 107
    iget-object v7, v2, Lorg/apache/poi/hdf/extractor/data/LST;->_levels:[Lorg/apache/poi/hdf/extractor/data/LVL;

    aget-object v7, v7, v6

    invoke-direct {p0, p1, v4, v7}, Lorg/apache/poi/hdf/extractor/data/ListTables;->createLVL([BILorg/apache/poi/hdf/extractor/data/LVL;)I

    move-result v7

    add-int/2addr v3, v7

    .line 103
    add-int/lit8 v6, v6, 0x1

    goto :goto_2
.end method


# virtual methods
.method public getLevel(II)Lorg/apache/poi/hdf/extractor/data/LVL;
    .locals 8
    .param p1, "list"    # I
    .param p2, "level"    # I

    .prologue
    .line 45
    iget-object v6, p0, Lorg/apache/poi/hdf/extractor/data/ListTables;->_pllfo:[Lorg/apache/poi/hdf/extractor/data/LFO;

    add-int/lit8 v7, p1, -0x1

    aget-object v4, v6, v7

    .line 47
    .local v4, "override":Lorg/apache/poi/hdf/extractor/data/LFO;
    const/4 v5, 0x0

    .local v5, "x":I
    :goto_0
    iget v6, v4, Lorg/apache/poi/hdf/extractor/data/LFO;->_clfolvl:I

    if-lt v5, v6, :cond_0

    .line 71
    iget-object v6, p0, Lorg/apache/poi/hdf/extractor/data/ListTables;->_lists:Ljava/util/Hashtable;

    iget v7, v4, Lorg/apache/poi/hdf/extractor/data/LFO;->_lsid:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/hdf/extractor/data/LST;

    .line 72
    .local v1, "lst":Lorg/apache/poi/hdf/extractor/data/LST;
    iget-object v6, v1, Lorg/apache/poi/hdf/extractor/data/LST;->_levels:[Lorg/apache/poi/hdf/extractor/data/LVL;

    aget-object v2, v6, p2

    .line 73
    .local v2, "lvl":Lorg/apache/poi/hdf/extractor/data/LVL;
    iget-object v6, v1, Lorg/apache/poi/hdf/extractor/data/LST;->_rgistd:[B

    mul-int/lit8 v7, p2, 0x2

    invoke-static {v6, v7}, Lorg/apache/poi/hdf/extractor/Utils;->convertBytesToShort([BI)S

    move-result v6

    iput-short v6, v2, Lorg/apache/poi/hdf/extractor/data/LVL;->_istd:S

    .line 74
    .end local v2    # "lvl":Lorg/apache/poi/hdf/extractor/data/LVL;
    :goto_1
    return-object v2

    .line 49
    .end local v1    # "lst":Lorg/apache/poi/hdf/extractor/data/LST;
    :cond_0
    iget-object v6, v4, Lorg/apache/poi/hdf/extractor/data/LFO;->_levels:[Lorg/apache/poi/hdf/extractor/data/LFOLVL;

    aget-object v6, v6, v5

    iget v6, v6, Lorg/apache/poi/hdf/extractor/data/LFOLVL;->_ilvl:I

    if-ne v6, p2, :cond_2

    .line 51
    iget-object v6, v4, Lorg/apache/poi/hdf/extractor/data/LFO;->_levels:[Lorg/apache/poi/hdf/extractor/data/LFOLVL;

    aget-object v0, v6, v5

    .line 52
    .local v0, "lfolvl":Lorg/apache/poi/hdf/extractor/data/LFOLVL;
    iget-boolean v6, v0, Lorg/apache/poi/hdf/extractor/data/LFOLVL;->_fFormatting:Z

    if-eqz v6, :cond_1

    .line 54
    iget-object v6, p0, Lorg/apache/poi/hdf/extractor/data/ListTables;->_lists:Ljava/util/Hashtable;

    iget v7, v4, Lorg/apache/poi/hdf/extractor/data/LFO;->_lsid:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/hdf/extractor/data/LST;

    .line 55
    .restart local v1    # "lst":Lorg/apache/poi/hdf/extractor/data/LST;
    iget-object v2, v0, Lorg/apache/poi/hdf/extractor/data/LFOLVL;->_override:Lorg/apache/poi/hdf/extractor/data/LVL;

    .line 56
    .restart local v2    # "lvl":Lorg/apache/poi/hdf/extractor/data/LVL;
    iget-object v6, v1, Lorg/apache/poi/hdf/extractor/data/LST;->_rgistd:[B

    mul-int/lit8 v7, p2, 0x2

    invoke-static {v6, v7}, Lorg/apache/poi/hdf/extractor/Utils;->convertBytesToShort([BI)S

    move-result v6

    iput-short v6, v2, Lorg/apache/poi/hdf/extractor/data/LVL;->_istd:S

    goto :goto_1

    .line 59
    .end local v1    # "lst":Lorg/apache/poi/hdf/extractor/data/LST;
    .end local v2    # "lvl":Lorg/apache/poi/hdf/extractor/data/LVL;
    :cond_1
    iget-boolean v6, v0, Lorg/apache/poi/hdf/extractor/data/LFOLVL;->_fStartAt:Z

    if-eqz v6, :cond_2

    .line 61
    iget-object v6, p0, Lorg/apache/poi/hdf/extractor/data/ListTables;->_lists:Ljava/util/Hashtable;

    iget v7, v4, Lorg/apache/poi/hdf/extractor/data/LFO;->_lsid:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/hdf/extractor/data/LST;

    .line 62
    .restart local v1    # "lst":Lorg/apache/poi/hdf/extractor/data/LST;
    iget-object v6, v1, Lorg/apache/poi/hdf/extractor/data/LST;->_levels:[Lorg/apache/poi/hdf/extractor/data/LVL;

    aget-object v2, v6, p2

    .line 63
    .restart local v2    # "lvl":Lorg/apache/poi/hdf/extractor/data/LVL;
    invoke-virtual {v2}, Lorg/apache/poi/hdf/extractor/data/LVL;->clone()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/poi/hdf/extractor/data/LVL;

    .line 64
    .local v3, "newLvl":Lorg/apache/poi/hdf/extractor/data/LVL;
    iget-object v6, v1, Lorg/apache/poi/hdf/extractor/data/LST;->_rgistd:[B

    mul-int/lit8 v7, p2, 0x2

    invoke-static {v6, v7}, Lorg/apache/poi/hdf/extractor/Utils;->convertBytesToShort([BI)S

    move-result v6

    iput-short v6, v3, Lorg/apache/poi/hdf/extractor/data/LVL;->_istd:S

    .line 65
    iget v6, v0, Lorg/apache/poi/hdf/extractor/data/LFOLVL;->_iStartAt:I

    iput v6, v3, Lorg/apache/poi/hdf/extractor/data/LVL;->_iStartAt:I

    move-object v2, v3

    .line 66
    goto :goto_1

    .line 47
    .end local v0    # "lfolvl":Lorg/apache/poi/hdf/extractor/data/LFOLVL;
    .end local v1    # "lst":Lorg/apache/poi/hdf/extractor/data/LST;
    .end local v2    # "lvl":Lorg/apache/poi/hdf/extractor/data/LVL;
    .end local v3    # "newLvl":Lorg/apache/poi/hdf/extractor/data/LVL;
    :cond_2
    add-int/lit8 v5, v5, 0x1

    goto :goto_0
.end method
