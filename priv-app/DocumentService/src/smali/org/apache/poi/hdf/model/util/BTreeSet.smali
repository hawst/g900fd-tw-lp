.class public final Lorg/apache/poi/hdf/model/util/BTreeSet;
.super Ljava/util/AbstractSet;
.source "BTreeSet.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;,
        Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;,
        Lorg/apache/poi/hdf/model/util/BTreeSet$Iterator;
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private comparator:Ljava/util/Comparator;

.field private order:I

.field public root:Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;

.field size:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 59
    const/4 v0, 0x6

    invoke-direct {p0, v0}, Lorg/apache/poi/hdf/model/util/BTreeSet;-><init>(I)V

    .line 60
    return-void
.end method

.method public constructor <init>(I)V
    .locals 1
    .param p1, "order"    # I

    .prologue
    .line 70
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lorg/apache/poi/hdf/model/util/BTreeSet;-><init>(ILjava/util/Comparator;)V

    .line 71
    return-void
.end method

.method public constructor <init>(ILjava/util/Comparator;)V
    .locals 2
    .param p1, "order"    # I
    .param p2, "comparator"    # Ljava/util/Comparator;

    .prologue
    const/4 v1, 0x0

    .line 73
    invoke-direct {p0}, Ljava/util/AbstractSet;-><init>()V

    .line 46
    iput-object v1, p0, Lorg/apache/poi/hdf/model/util/BTreeSet;->comparator:Ljava/util/Comparator;

    .line 48
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/poi/hdf/model/util/BTreeSet;->size:I

    .line 75
    iput p1, p0, Lorg/apache/poi/hdf/model/util/BTreeSet;->order:I

    .line 76
    iput-object p2, p0, Lorg/apache/poi/hdf/model/util/BTreeSet;->comparator:Ljava/util/Comparator;

    .line 77
    new-instance v0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;

    invoke-direct {v0, p0, v1}, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;-><init>(Lorg/apache/poi/hdf/model/util/BTreeSet;Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;)V

    iput-object v0, p0, Lorg/apache/poi/hdf/model/util/BTreeSet;->root:Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;

    .line 78
    return-void
.end method

.method public constructor <init>(Ljava/util/Collection;)V
    .locals 1
    .param p1, "c"    # Ljava/util/Collection;

    .prologue
    .line 64
    const/4 v0, 0x6

    invoke-direct {p0, v0}, Lorg/apache/poi/hdf/model/util/BTreeSet;-><init>(I)V

    .line 65
    invoke-virtual {p0, p1}, Lorg/apache/poi/hdf/model/util/BTreeSet;->addAll(Ljava/util/Collection;)Z

    .line 66
    return-void
.end method

.method static synthetic access$0(Lorg/apache/poi/hdf/model/util/BTreeSet;)I
    .locals 1

    .prologue
    .line 47
    iget v0, p0, Lorg/apache/poi/hdf/model/util/BTreeSet;->order:I

    return v0
.end method

.method public static findProperties(IILorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;)Ljava/util/ArrayList;
    .locals 10
    .param p0, "start"    # I
    .param p1, "end"    # I
    .param p2, "root"    # Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;

    .prologue
    .line 119
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 120
    .local v4, "results":Ljava/util/ArrayList;
    iget-object v3, p2, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    .line 122
    .local v3, "entries":[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;
    const/4 v5, 0x0

    .local v5, "x":I
    :goto_0
    array-length v9, v3

    if-lt v5, v9, :cond_1

    .line 170
    :cond_0
    :goto_1
    return-object v4

    .line 124
    :cond_1
    aget-object v9, v3, v5

    if-eqz v9, :cond_0

    .line 126
    aget-object v9, v3, v5

    iget-object v2, v9, Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;->child:Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;

    .line 127
    .local v2, "child":Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;
    aget-object v9, v3, v5

    iget-object v7, v9, Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;->element:Ljava/lang/Object;

    check-cast v7, Lorg/apache/poi/hdf/model/hdftypes/PropertyNode;

    .line 128
    .local v7, "xNode":Lorg/apache/poi/hdf/model/hdftypes/PropertyNode;
    if-eqz v7, :cond_6

    .line 130
    invoke-virtual {v7}, Lorg/apache/poi/hdf/model/hdftypes/PropertyNode;->getStart()I

    move-result v8

    .line 131
    .local v8, "xStart":I
    invoke-virtual {v7}, Lorg/apache/poi/hdf/model/hdftypes/PropertyNode;->getEnd()I

    move-result v6

    .line 132
    .local v6, "xEnd":I
    if-ge v8, p1, :cond_5

    .line 134
    if-lt v8, p0, :cond_4

    .line 136
    if-eqz v2, :cond_2

    .line 138
    invoke-static {p0, p1, v2}, Lorg/apache/poi/hdf/model/util/BTreeSet;->findProperties(IILorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;)Ljava/util/ArrayList;

    move-result-object v1

    .line 139
    .local v1, "beforeItems":Ljava/util/ArrayList;
    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 141
    .end local v1    # "beforeItems":Ljava/util/ArrayList;
    :cond_2
    invoke-virtual {v4, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 122
    .end local v6    # "xEnd":I
    .end local v8    # "xStart":I
    :cond_3
    :goto_2
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 143
    .restart local v6    # "xEnd":I
    .restart local v8    # "xStart":I
    :cond_4
    if-ge p0, v6, :cond_3

    .line 145
    invoke-virtual {v4, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 151
    :cond_5
    if-eqz v2, :cond_0

    .line 153
    invoke-static {p0, p1, v2}, Lorg/apache/poi/hdf/model/util/BTreeSet;->findProperties(IILorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;)Ljava/util/ArrayList;

    move-result-object v1

    .line 154
    .restart local v1    # "beforeItems":Ljava/util/ArrayList;
    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    goto :goto_1

    .line 159
    .end local v1    # "beforeItems":Ljava/util/ArrayList;
    .end local v6    # "xEnd":I
    .end local v8    # "xStart":I
    :cond_6
    if-eqz v2, :cond_3

    .line 161
    invoke-static {p0, p1, v2}, Lorg/apache/poi/hdf/model/util/BTreeSet;->findProperties(IILorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;)Ljava/util/ArrayList;

    move-result-object v0

    .line 162
    .local v0, "afterItems":Ljava/util/ArrayList;
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    goto :goto_2
.end method


# virtual methods
.method public add(Ljava/lang/Object;)Z
    .locals 2
    .param p1, "x"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 86
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 87
    :cond_0
    iget-object v0, p0, Lorg/apache/poi/hdf/model/util/BTreeSet;->root:Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;

    const/4 v1, -0x1

    invoke-virtual {v0, p1, v1}, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->insert(Ljava/lang/Object;I)Z

    move-result v0

    return v0
.end method

.method public clear()V
    .locals 2

    .prologue
    .line 108
    new-instance v0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;-><init>(Lorg/apache/poi/hdf/model/util/BTreeSet;Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;)V

    iput-object v0, p0, Lorg/apache/poi/hdf/model/util/BTreeSet;->root:Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;

    .line 109
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/poi/hdf/model/util/BTreeSet;->size:I

    .line 110
    return-void
.end method

.method compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .param p1, "x"    # Ljava/lang/Object;
    .param p2, "y"    # Ljava/lang/Object;

    .prologue
    .line 177
    iget-object v0, p0, Lorg/apache/poi/hdf/model/util/BTreeSet;->comparator:Ljava/util/Comparator;

    if-nez v0, :cond_0

    check-cast p1, Ljava/lang/Comparable;

    .end local p1    # "x":Ljava/lang/Object;
    invoke-interface {p1, p2}, Ljava/lang/Comparable;->compareTo(Ljava/lang/Object;)I

    move-result v0

    :goto_0
    return v0

    .restart local p1    # "x":Ljava/lang/Object;
    :cond_0
    iget-object v0, p0, Lorg/apache/poi/hdf/model/util/BTreeSet;->comparator:Ljava/util/Comparator;

    invoke-interface {v0, p1, p2}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    goto :goto_0
.end method

.method public contains(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "x"    # Ljava/lang/Object;

    .prologue
    .line 92
    iget-object v0, p0, Lorg/apache/poi/hdf/model/util/BTreeSet;->root:Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;

    invoke-virtual {v0, p1}, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->includes(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 1

    .prologue
    .line 114
    new-instance v0, Lorg/apache/poi/hdf/model/util/BTreeSet$Iterator;

    invoke-direct {v0, p0}, Lorg/apache/poi/hdf/model/util/BTreeSet$Iterator;-><init>(Lorg/apache/poi/hdf/model/util/BTreeSet;)V

    return-object v0
.end method

.method public remove(Ljava/lang/Object;)Z
    .locals 2
    .param p1, "x"    # Ljava/lang/Object;

    .prologue
    .line 97
    if-nez p1, :cond_0

    const/4 v0, 0x0

    .line 98
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lorg/apache/poi/hdf/model/util/BTreeSet;->root:Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;

    const/4 v1, -0x1

    invoke-virtual {v0, p1, v1}, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->delete(Ljava/lang/Object;I)Z

    move-result v0

    goto :goto_0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 103
    iget v0, p0, Lorg/apache/poi/hdf/model/util/BTreeSet;->size:I

    return v0
.end method
