.class public final Lorg/apache/poi/hdf/model/util/ParsingState;
.super Ljava/lang/Object;
.source "ParsingState.java"


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field _currentPageIndex:I

.field _currentPropIndex:I

.field _fkp:Lorg/apache/poi/hdf/model/hdftypes/FormattedDiskPage;


# direct methods
.method public constructor <init>(ILorg/apache/poi/hdf/model/hdftypes/FormattedDiskPage;)V
    .locals 1
    .param p1, "firstPage"    # I
    .param p2, "fkp"    # Lorg/apache/poi/hdf/model/hdftypes/FormattedDiskPage;

    .prologue
    const/4 v0, 0x0

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput v0, p0, Lorg/apache/poi/hdf/model/util/ParsingState;->_currentPageIndex:I

    .line 29
    iput v0, p0, Lorg/apache/poi/hdf/model/util/ParsingState;->_currentPropIndex:I

    .line 34
    iput-object p2, p0, Lorg/apache/poi/hdf/model/util/ParsingState;->_fkp:Lorg/apache/poi/hdf/model/hdftypes/FormattedDiskPage;

    .line 35
    return-void
.end method


# virtual methods
.method public getCurrentPageIndex()I
    .locals 1

    .prologue
    .line 46
    iget v0, p0, Lorg/apache/poi/hdf/model/util/ParsingState;->_currentPageIndex:I

    return v0
.end method

.method public getCurrentPropIndex()I
    .locals 1

    .prologue
    .line 54
    iget v0, p0, Lorg/apache/poi/hdf/model/util/ParsingState;->_currentPropIndex:I

    return v0
.end method

.method public getFkp()Lorg/apache/poi/hdf/model/hdftypes/FormattedDiskPage;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lorg/apache/poi/hdf/model/util/ParsingState;->_fkp:Lorg/apache/poi/hdf/model/hdftypes/FormattedDiskPage;

    return-object v0
.end method

.method public setState(ILorg/apache/poi/hdf/model/hdftypes/FormattedDiskPage;I)V
    .locals 0
    .param p1, "currentPageIndex"    # I
    .param p2, "fkp"    # Lorg/apache/poi/hdf/model/hdftypes/FormattedDiskPage;
    .param p3, "currentPropIndex"    # I

    .prologue
    .line 60
    iput p1, p0, Lorg/apache/poi/hdf/model/util/ParsingState;->_currentPageIndex:I

    .line 61
    iput-object p2, p0, Lorg/apache/poi/hdf/model/util/ParsingState;->_fkp:Lorg/apache/poi/hdf/model/hdftypes/FormattedDiskPage;

    .line 62
    iput p3, p0, Lorg/apache/poi/hdf/model/util/ParsingState;->_currentPropIndex:I

    .line 64
    return-void
.end method
