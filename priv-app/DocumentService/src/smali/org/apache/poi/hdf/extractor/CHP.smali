.class public final Lorg/apache/poi/hdf/extractor/CHP;
.super Ljava/lang/Object;
.source "CHP.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field _baseIstd:I

.field _bold:Z

.field _brc:[S

.field _chYsr:B

.field _chse:S

.field _dttmDispFldRMark:I

.field _dttmPropRMark:I

.field _dttmRMark:[I

.field _dttmRMarkDel:[I

.field _dxaSpace:I

.field _fCaps:Z

.field _fChsDiff:Z

.field _fDStrike:Z

.field _fData:Z

.field _fDispFldRMark:Z

.field _fEmboss:Z

.field _fImprint:Z

.field _fLowerCase:Z

.field _fObj:Z

.field _fOle2:Z

.field _fOutline:Z

.field _fPropMark:Z

.field _fRMark:Z

.field _fRMarkDel:Z

.field _fShadow:Z

.field _fSmallCaps:Z

.field _fSpec:Z

.field _fStrike:Z

.field _fVanish:Z

.field _fcObj:I

.field _fcPic:I

.field _ftc:S

.field _ftcAscii:S

.field _ftcFE:S

.field _ftcOther:S

.field _ftcSym:S

.field _highlighted:Z

.field _hps:I

.field _hpsKern:I

.field _hpsPos:S

.field _ibstDispFldRMark:S

.field _ibstPropRMark:S

.field _ibstRMark:S

.field _ibstRMarkDel:S

.field _ico:B

.field _icoHighlight:B

.field _idctHint:B

.field _iss:B

.field _istd:I

.field _italic:Z

.field _kul:B

.field _lidDefault:S

.field _lidFE:S

.field _paddingEnd:S

.field _paddingStart:S

.field _sfxtText:B

.field _shd:S

.field _specialFC:I

.field _wCharScale:I

.field _xchSym:S

.field _xstDispFldRMark:[B

.field _ysr:B


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/16 v3, 0x400

    const/4 v2, 0x0

    const/4 v1, 0x2

    .line 95
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    new-array v0, v1, [I

    iput-object v0, p0, Lorg/apache/poi/hdf/extractor/CHP;->_dttmRMark:[I

    .line 68
    new-array v0, v1, [I

    iput-object v0, p0, Lorg/apache/poi/hdf/extractor/CHP;->_dttmRMarkDel:[I

    .line 71
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/poi/hdf/extractor/CHP;->_baseIstd:I

    .line 89
    const/16 v0, 0x20

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/apache/poi/hdf/extractor/CHP;->_xstDispFldRMark:[B

    .line 91
    new-array v0, v1, [S

    iput-object v0, p0, Lorg/apache/poi/hdf/extractor/CHP;->_brc:[S

    .line 92
    iput-short v2, p0, Lorg/apache/poi/hdf/extractor/CHP;->_paddingStart:S

    .line 93
    iput-short v2, p0, Lorg/apache/poi/hdf/extractor/CHP;->_paddingEnd:S

    .line 97
    const/16 v0, 0xa

    iput v0, p0, Lorg/apache/poi/hdf/extractor/CHP;->_istd:I

    .line 98
    const/16 v0, 0x14

    iput v0, p0, Lorg/apache/poi/hdf/extractor/CHP;->_hps:I

    .line 99
    iput-short v3, p0, Lorg/apache/poi/hdf/extractor/CHP;->_lidDefault:S

    .line 100
    iput-short v3, p0, Lorg/apache/poi/hdf/extractor/CHP;->_lidFE:S

    .line 102
    return-void
.end method


# virtual methods
.method public clone()Ljava/lang/Object;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x0

    .line 173
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hdf/extractor/CHP;

    .line 174
    .local v0, "clone":Lorg/apache/poi/hdf/extractor/CHP;
    new-array v1, v4, [S

    iput-object v1, v0, Lorg/apache/poi/hdf/extractor/CHP;->_brc:[S

    .line 175
    iget-object v1, p0, Lorg/apache/poi/hdf/extractor/CHP;->_brc:[S

    iget-object v2, v0, Lorg/apache/poi/hdf/extractor/CHP;->_brc:[S

    invoke-static {v1, v3, v2, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 176
    return-object v0
.end method

.method public copy(Lorg/apache/poi/hdf/extractor/CHP;)V
    .locals 1
    .param p1, "toCopy"    # Lorg/apache/poi/hdf/extractor/CHP;

    .prologue
    .line 105
    iget-boolean v0, p1, Lorg/apache/poi/hdf/extractor/CHP;->_bold:Z

    iput-boolean v0, p0, Lorg/apache/poi/hdf/extractor/CHP;->_bold:Z

    .line 106
    iget-boolean v0, p1, Lorg/apache/poi/hdf/extractor/CHP;->_italic:Z

    iput-boolean v0, p0, Lorg/apache/poi/hdf/extractor/CHP;->_italic:Z

    .line 107
    iget-boolean v0, p1, Lorg/apache/poi/hdf/extractor/CHP;->_fRMarkDel:Z

    iput-boolean v0, p0, Lorg/apache/poi/hdf/extractor/CHP;->_fRMarkDel:Z

    .line 108
    iget-boolean v0, p1, Lorg/apache/poi/hdf/extractor/CHP;->_fOutline:Z

    iput-boolean v0, p0, Lorg/apache/poi/hdf/extractor/CHP;->_fOutline:Z

    .line 109
    iget-boolean v0, p1, Lorg/apache/poi/hdf/extractor/CHP;->_fSmallCaps:Z

    iput-boolean v0, p0, Lorg/apache/poi/hdf/extractor/CHP;->_fSmallCaps:Z

    .line 110
    iget-boolean v0, p1, Lorg/apache/poi/hdf/extractor/CHP;->_fCaps:Z

    iput-boolean v0, p0, Lorg/apache/poi/hdf/extractor/CHP;->_fCaps:Z

    .line 111
    iget-boolean v0, p1, Lorg/apache/poi/hdf/extractor/CHP;->_fVanish:Z

    iput-boolean v0, p0, Lorg/apache/poi/hdf/extractor/CHP;->_fVanish:Z

    .line 112
    iget-boolean v0, p1, Lorg/apache/poi/hdf/extractor/CHP;->_fRMark:Z

    iput-boolean v0, p0, Lorg/apache/poi/hdf/extractor/CHP;->_fRMark:Z

    .line 113
    iget-boolean v0, p1, Lorg/apache/poi/hdf/extractor/CHP;->_fSpec:Z

    iput-boolean v0, p0, Lorg/apache/poi/hdf/extractor/CHP;->_fSpec:Z

    .line 114
    iget-boolean v0, p1, Lorg/apache/poi/hdf/extractor/CHP;->_fStrike:Z

    iput-boolean v0, p0, Lorg/apache/poi/hdf/extractor/CHP;->_fStrike:Z

    .line 115
    iget-boolean v0, p1, Lorg/apache/poi/hdf/extractor/CHP;->_fObj:Z

    iput-boolean v0, p0, Lorg/apache/poi/hdf/extractor/CHP;->_fObj:Z

    .line 116
    iget-boolean v0, p1, Lorg/apache/poi/hdf/extractor/CHP;->_fShadow:Z

    iput-boolean v0, p0, Lorg/apache/poi/hdf/extractor/CHP;->_fShadow:Z

    .line 117
    iget-boolean v0, p1, Lorg/apache/poi/hdf/extractor/CHP;->_fLowerCase:Z

    iput-boolean v0, p0, Lorg/apache/poi/hdf/extractor/CHP;->_fLowerCase:Z

    .line 118
    iget-boolean v0, p1, Lorg/apache/poi/hdf/extractor/CHP;->_fData:Z

    iput-boolean v0, p0, Lorg/apache/poi/hdf/extractor/CHP;->_fData:Z

    .line 119
    iget-boolean v0, p1, Lorg/apache/poi/hdf/extractor/CHP;->_fOle2:Z

    iput-boolean v0, p0, Lorg/apache/poi/hdf/extractor/CHP;->_fOle2:Z

    .line 120
    iget-boolean v0, p1, Lorg/apache/poi/hdf/extractor/CHP;->_fEmboss:Z

    iput-boolean v0, p0, Lorg/apache/poi/hdf/extractor/CHP;->_fEmboss:Z

    .line 121
    iget-boolean v0, p1, Lorg/apache/poi/hdf/extractor/CHP;->_fImprint:Z

    iput-boolean v0, p0, Lorg/apache/poi/hdf/extractor/CHP;->_fImprint:Z

    .line 122
    iget-boolean v0, p1, Lorg/apache/poi/hdf/extractor/CHP;->_fDStrike:Z

    iput-boolean v0, p0, Lorg/apache/poi/hdf/extractor/CHP;->_fDStrike:Z

    .line 124
    iget-short v0, p1, Lorg/apache/poi/hdf/extractor/CHP;->_ftcAscii:S

    iput-short v0, p0, Lorg/apache/poi/hdf/extractor/CHP;->_ftcAscii:S

    .line 125
    iget-short v0, p1, Lorg/apache/poi/hdf/extractor/CHP;->_ftcFE:S

    iput-short v0, p0, Lorg/apache/poi/hdf/extractor/CHP;->_ftcFE:S

    .line 126
    iget-short v0, p1, Lorg/apache/poi/hdf/extractor/CHP;->_ftcOther:S

    iput-short v0, p0, Lorg/apache/poi/hdf/extractor/CHP;->_ftcOther:S

    .line 127
    iget-short v0, p1, Lorg/apache/poi/hdf/extractor/CHP;->_ftc:S

    iput-short v0, p0, Lorg/apache/poi/hdf/extractor/CHP;->_ftc:S

    .line 128
    iget v0, p1, Lorg/apache/poi/hdf/extractor/CHP;->_hps:I

    iput v0, p0, Lorg/apache/poi/hdf/extractor/CHP;->_hps:I

    .line 129
    iget v0, p1, Lorg/apache/poi/hdf/extractor/CHP;->_dxaSpace:I

    iput v0, p0, Lorg/apache/poi/hdf/extractor/CHP;->_dxaSpace:I

    .line 130
    iget-byte v0, p1, Lorg/apache/poi/hdf/extractor/CHP;->_iss:B

    iput-byte v0, p0, Lorg/apache/poi/hdf/extractor/CHP;->_iss:B

    .line 131
    iget-byte v0, p1, Lorg/apache/poi/hdf/extractor/CHP;->_kul:B

    iput-byte v0, p0, Lorg/apache/poi/hdf/extractor/CHP;->_kul:B

    .line 132
    iget-byte v0, p1, Lorg/apache/poi/hdf/extractor/CHP;->_ico:B

    iput-byte v0, p0, Lorg/apache/poi/hdf/extractor/CHP;->_ico:B

    .line 133
    iget-short v0, p1, Lorg/apache/poi/hdf/extractor/CHP;->_hpsPos:S

    iput-short v0, p0, Lorg/apache/poi/hdf/extractor/CHP;->_hpsPos:S

    .line 134
    iget-short v0, p1, Lorg/apache/poi/hdf/extractor/CHP;->_lidDefault:S

    iput-short v0, p0, Lorg/apache/poi/hdf/extractor/CHP;->_lidDefault:S

    .line 135
    iget-short v0, p1, Lorg/apache/poi/hdf/extractor/CHP;->_lidFE:S

    iput-short v0, p0, Lorg/apache/poi/hdf/extractor/CHP;->_lidFE:S

    .line 136
    iget-byte v0, p1, Lorg/apache/poi/hdf/extractor/CHP;->_idctHint:B

    iput-byte v0, p0, Lorg/apache/poi/hdf/extractor/CHP;->_idctHint:B

    .line 137
    iget v0, p1, Lorg/apache/poi/hdf/extractor/CHP;->_wCharScale:I

    iput v0, p0, Lorg/apache/poi/hdf/extractor/CHP;->_wCharScale:I

    .line 138
    iget-short v0, p1, Lorg/apache/poi/hdf/extractor/CHP;->_chse:S

    iput-short v0, p0, Lorg/apache/poi/hdf/extractor/CHP;->_chse:S

    .line 140
    iget v0, p1, Lorg/apache/poi/hdf/extractor/CHP;->_specialFC:I

    iput v0, p0, Lorg/apache/poi/hdf/extractor/CHP;->_specialFC:I

    .line 141
    iget-short v0, p1, Lorg/apache/poi/hdf/extractor/CHP;->_ibstRMark:S

    iput-short v0, p0, Lorg/apache/poi/hdf/extractor/CHP;->_ibstRMark:S

    .line 142
    iget-short v0, p1, Lorg/apache/poi/hdf/extractor/CHP;->_ibstRMarkDel:S

    iput-short v0, p0, Lorg/apache/poi/hdf/extractor/CHP;->_ibstRMarkDel:S

    .line 143
    iget-object v0, p1, Lorg/apache/poi/hdf/extractor/CHP;->_dttmRMark:[I

    iput-object v0, p0, Lorg/apache/poi/hdf/extractor/CHP;->_dttmRMark:[I

    .line 144
    iget-object v0, p1, Lorg/apache/poi/hdf/extractor/CHP;->_dttmRMarkDel:[I

    iput-object v0, p0, Lorg/apache/poi/hdf/extractor/CHP;->_dttmRMarkDel:[I

    .line 146
    iget v0, p1, Lorg/apache/poi/hdf/extractor/CHP;->_istd:I

    iput v0, p0, Lorg/apache/poi/hdf/extractor/CHP;->_istd:I

    .line 147
    iget v0, p1, Lorg/apache/poi/hdf/extractor/CHP;->_baseIstd:I

    iput v0, p0, Lorg/apache/poi/hdf/extractor/CHP;->_baseIstd:I

    .line 148
    iget v0, p1, Lorg/apache/poi/hdf/extractor/CHP;->_fcPic:I

    iput v0, p0, Lorg/apache/poi/hdf/extractor/CHP;->_fcPic:I

    .line 149
    iget-short v0, p1, Lorg/apache/poi/hdf/extractor/CHP;->_ftcSym:S

    iput-short v0, p0, Lorg/apache/poi/hdf/extractor/CHP;->_ftcSym:S

    .line 150
    iget-short v0, p1, Lorg/apache/poi/hdf/extractor/CHP;->_xchSym:S

    iput-short v0, p0, Lorg/apache/poi/hdf/extractor/CHP;->_xchSym:S

    .line 151
    iget-byte v0, p1, Lorg/apache/poi/hdf/extractor/CHP;->_ysr:B

    iput-byte v0, p0, Lorg/apache/poi/hdf/extractor/CHP;->_ysr:B

    .line 152
    iget-byte v0, p1, Lorg/apache/poi/hdf/extractor/CHP;->_chYsr:B

    iput-byte v0, p0, Lorg/apache/poi/hdf/extractor/CHP;->_chYsr:B

    .line 153
    iget v0, p1, Lorg/apache/poi/hdf/extractor/CHP;->_hpsKern:I

    iput v0, p0, Lorg/apache/poi/hdf/extractor/CHP;->_hpsKern:I

    .line 154
    iget v0, p1, Lorg/apache/poi/hdf/extractor/CHP;->_fcObj:I

    iput v0, p0, Lorg/apache/poi/hdf/extractor/CHP;->_fcObj:I

    .line 155
    iget-byte v0, p1, Lorg/apache/poi/hdf/extractor/CHP;->_icoHighlight:B

    iput-byte v0, p0, Lorg/apache/poi/hdf/extractor/CHP;->_icoHighlight:B

    .line 156
    iget-boolean v0, p1, Lorg/apache/poi/hdf/extractor/CHP;->_fChsDiff:Z

    iput-boolean v0, p0, Lorg/apache/poi/hdf/extractor/CHP;->_fChsDiff:Z

    .line 157
    iget-boolean v0, p1, Lorg/apache/poi/hdf/extractor/CHP;->_highlighted:Z

    iput-boolean v0, p0, Lorg/apache/poi/hdf/extractor/CHP;->_highlighted:Z

    .line 158
    iget-boolean v0, p1, Lorg/apache/poi/hdf/extractor/CHP;->_fPropMark:Z

    iput-boolean v0, p0, Lorg/apache/poi/hdf/extractor/CHP;->_fPropMark:Z

    .line 159
    iget-short v0, p1, Lorg/apache/poi/hdf/extractor/CHP;->_ibstPropRMark:S

    iput-short v0, p0, Lorg/apache/poi/hdf/extractor/CHP;->_ibstPropRMark:S

    .line 160
    iget v0, p1, Lorg/apache/poi/hdf/extractor/CHP;->_dttmPropRMark:I

    iput v0, p0, Lorg/apache/poi/hdf/extractor/CHP;->_dttmPropRMark:I

    .line 161
    iget-byte v0, p1, Lorg/apache/poi/hdf/extractor/CHP;->_sfxtText:B

    iput-byte v0, p0, Lorg/apache/poi/hdf/extractor/CHP;->_sfxtText:B

    .line 162
    iget-boolean v0, p1, Lorg/apache/poi/hdf/extractor/CHP;->_fDispFldRMark:Z

    iput-boolean v0, p0, Lorg/apache/poi/hdf/extractor/CHP;->_fDispFldRMark:Z

    .line 163
    iget-short v0, p1, Lorg/apache/poi/hdf/extractor/CHP;->_ibstDispFldRMark:S

    iput-short v0, p0, Lorg/apache/poi/hdf/extractor/CHP;->_ibstDispFldRMark:S

    .line 164
    iget v0, p1, Lorg/apache/poi/hdf/extractor/CHP;->_dttmDispFldRMark:I

    iput v0, p0, Lorg/apache/poi/hdf/extractor/CHP;->_dttmDispFldRMark:I

    .line 165
    iget-object v0, p1, Lorg/apache/poi/hdf/extractor/CHP;->_xstDispFldRMark:[B

    iput-object v0, p0, Lorg/apache/poi/hdf/extractor/CHP;->_xstDispFldRMark:[B

    .line 166
    iget-short v0, p1, Lorg/apache/poi/hdf/extractor/CHP;->_shd:S

    iput-short v0, p0, Lorg/apache/poi/hdf/extractor/CHP;->_shd:S

    .line 167
    iget-object v0, p1, Lorg/apache/poi/hdf/extractor/CHP;->_brc:[S

    iput-object v0, p0, Lorg/apache/poi/hdf/extractor/CHP;->_brc:[S

    .line 169
    return-void
.end method
