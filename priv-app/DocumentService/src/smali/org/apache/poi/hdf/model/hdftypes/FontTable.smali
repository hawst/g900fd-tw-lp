.class public final Lorg/apache/poi/hdf/model/hdftypes/FontTable;
.super Ljava/lang/Object;
.source "FontTable.java"

# interfaces
.implements Lorg/apache/poi/hdf/model/hdftypes/HDFType;


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field fontNames:[Ljava/lang/String;


# direct methods
.method public constructor <init>([B)V
    .locals 9
    .param p1, "fontTable"    # [B

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    const/4 v7, 0x0

    invoke-static {p1, v7}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v5

    .line 34
    .local v5, "size":I
    new-array v7, v5, [Ljava/lang/String;

    iput-object v7, p0, Lorg/apache/poi/hdf/model/hdftypes/FontTable;->fontNames:[Ljava/lang/String;

    .line 36
    const/4 v1, 0x4

    .line 37
    .local v1, "currentIndex":I
    const/4 v6, 0x0

    .local v6, "x":I
    :goto_0
    if-lt v6, v5, :cond_0

    .line 55
    return-void

    .line 39
    :cond_0
    aget-byte v2, p1, v1

    .line 41
    .local v2, "ffnLength":B
    add-int/lit8 v4, v1, 0x28

    .line 42
    .local v4, "nameOffset":I
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    .line 44
    .local v3, "nameBuf":Ljava/lang/StringBuffer;
    invoke-static {p1, v4}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v7

    int-to-char v0, v7

    .line 45
    .local v0, "ch":C
    :goto_1
    if-nez v0, :cond_1

    .line 51
    iget-object v7, p0, Lorg/apache/poi/hdf/model/hdftypes/FontTable;->fontNames:[Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v6

    .line 52
    add-int/lit8 v7, v2, 0x1

    add-int/2addr v1, v7

    .line 37
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 47
    :cond_1
    invoke-virtual {v3, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 48
    add-int/lit8 v4, v4, 0x2

    .line 49
    invoke-static {p1, v4}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v7

    int-to-char v0, v7

    goto :goto_1
.end method


# virtual methods
.method public getFont(I)Ljava/lang/String;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 58
    iget-object v0, p0, Lorg/apache/poi/hdf/model/hdftypes/FontTable;->fontNames:[Ljava/lang/String;

    aget-object v0, v0, p1

    return-object v0
.end method
