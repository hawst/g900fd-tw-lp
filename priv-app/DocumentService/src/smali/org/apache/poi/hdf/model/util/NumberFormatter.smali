.class public final Lorg/apache/poi/hdf/model/util/NumberFormatter;
.super Ljava/lang/Object;
.source "NumberFormatter.java"


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field private static final ARABIC:I = 0x0

.field private static final LOWER_LETTER:I = 0x4

.field private static final LOWER_ROMAN:I = 0x2

.field private static final ORDINAL:I = 0x5

.field private static final UPPER_LETTER:I = 0x3

.field private static final UPPER_ROMAN:I = 0x1

.field private static _arabic:[Ljava/lang/String;

.field private static _letter:[Ljava/lang/String;

.field private static _roman:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 36
    const/16 v0, 0x35

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "1"

    aput-object v1, v0, v3

    const-string/jumbo v1, "2"

    aput-object v1, v0, v4

    const-string/jumbo v1, "3"

    aput-object v1, v0, v5

    const-string/jumbo v1, "4"

    aput-object v1, v0, v6

    const-string/jumbo v1, "5"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string/jumbo v2, "6"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    .line 37
    const-string/jumbo v2, "7"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string/jumbo v2, "8"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string/jumbo v2, "9"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string/jumbo v2, "10"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string/jumbo v2, "11"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string/jumbo v2, "12"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    .line 38
    const-string/jumbo v2, "13"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string/jumbo v2, "14"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string/jumbo v2, "15"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string/jumbo v2, "16"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string/jumbo v2, "17"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string/jumbo v2, "18"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    .line 39
    const-string/jumbo v2, "19"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string/jumbo v2, "20"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string/jumbo v2, "21"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string/jumbo v2, "22"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string/jumbo v2, "23"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    .line 40
    const-string/jumbo v2, "24"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string/jumbo v2, "25"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string/jumbo v2, "26"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string/jumbo v2, "27"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string/jumbo v2, "28"

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    .line 41
    const-string/jumbo v2, "29"

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    const-string/jumbo v2, "30"

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    const-string/jumbo v2, "31"

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    const-string/jumbo v2, "32"

    aput-object v2, v0, v1

    const/16 v1, 0x20

    const-string/jumbo v2, "33"

    aput-object v2, v0, v1

    const/16 v1, 0x21

    .line 42
    const-string/jumbo v2, "34"

    aput-object v2, v0, v1

    const/16 v1, 0x22

    const-string/jumbo v2, "35"

    aput-object v2, v0, v1

    const/16 v1, 0x23

    const-string/jumbo v2, "36"

    aput-object v2, v0, v1

    const/16 v1, 0x24

    const-string/jumbo v2, "37"

    aput-object v2, v0, v1

    const/16 v1, 0x25

    const-string/jumbo v2, "38"

    aput-object v2, v0, v1

    const/16 v1, 0x26

    .line 43
    const-string/jumbo v2, "39"

    aput-object v2, v0, v1

    const/16 v1, 0x27

    const-string/jumbo v2, "40"

    aput-object v2, v0, v1

    const/16 v1, 0x28

    const-string/jumbo v2, "41"

    aput-object v2, v0, v1

    const/16 v1, 0x29

    const-string/jumbo v2, "42"

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    const-string/jumbo v2, "43"

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    .line 44
    const-string/jumbo v2, "44"

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    const-string/jumbo v2, "45"

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    const-string/jumbo v2, "46"

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    const-string/jumbo v2, "47"

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    const-string/jumbo v2, "48"

    aput-object v2, v0, v1

    const/16 v1, 0x30

    .line 45
    const-string/jumbo v2, "49"

    aput-object v2, v0, v1

    const/16 v1, 0x31

    const-string/jumbo v2, "50"

    aput-object v2, v0, v1

    const/16 v1, 0x32

    const-string/jumbo v2, "51"

    aput-object v2, v0, v1

    const/16 v1, 0x33

    const-string/jumbo v2, "52"

    aput-object v2, v0, v1

    const/16 v1, 0x34

    const-string/jumbo v2, "53"

    aput-object v2, v0, v1

    .line 36
    sput-object v0, Lorg/apache/poi/hdf/model/util/NumberFormatter;->_arabic:[Ljava/lang/String;

    .line 46
    const/16 v0, 0x33

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "i"

    aput-object v1, v0, v3

    const-string/jumbo v1, "ii"

    aput-object v1, v0, v4

    const-string/jumbo v1, "iii"

    aput-object v1, v0, v5

    const-string/jumbo v1, "iv"

    aput-object v1, v0, v6

    const-string/jumbo v1, "v"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string/jumbo v2, "vi"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    .line 47
    const-string/jumbo v2, "vii"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string/jumbo v2, "viii"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string/jumbo v2, "ix"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string/jumbo v2, "x"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string/jumbo v2, "xi"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string/jumbo v2, "xii"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    .line 48
    const-string/jumbo v2, "xiii"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string/jumbo v2, "xiv"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string/jumbo v2, "xv"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string/jumbo v2, "xvi"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string/jumbo v2, "xvii"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    .line 49
    const-string/jumbo v2, "xviii"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string/jumbo v2, "xix"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string/jumbo v2, "xx"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string/jumbo v2, "xxi"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string/jumbo v2, "xxii"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    .line 50
    const-string/jumbo v2, "xxiii"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string/jumbo v2, "xxiv"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string/jumbo v2, "xxv"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string/jumbo v2, "xxvi"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    .line 51
    const-string/jumbo v2, "xxvii"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string/jumbo v2, "xxviii"

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string/jumbo v2, "xxix"

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    const-string/jumbo v2, "xxx"

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    .line 52
    const-string/jumbo v2, "xxxi"

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    const-string/jumbo v2, "xxxii"

    aput-object v2, v0, v1

    const/16 v1, 0x20

    const-string/jumbo v2, "xxxiii"

    aput-object v2, v0, v1

    const/16 v1, 0x21

    const-string/jumbo v2, "xxxiv"

    aput-object v2, v0, v1

    const/16 v1, 0x22

    .line 53
    const-string/jumbo v2, "xxxv"

    aput-object v2, v0, v1

    const/16 v1, 0x23

    const-string/jumbo v2, "xxxvi"

    aput-object v2, v0, v1

    const/16 v1, 0x24

    const-string/jumbo v2, "xxxvii"

    aput-object v2, v0, v1

    const/16 v1, 0x25

    const-string/jumbo v2, "xxxvii"

    aput-object v2, v0, v1

    const/16 v1, 0x26

    .line 54
    const-string/jumbo v2, "xxxviii"

    aput-object v2, v0, v1

    const/16 v1, 0x27

    const-string/jumbo v2, "xxxix"

    aput-object v2, v0, v1

    const/16 v1, 0x28

    const-string/jumbo v2, "xl"

    aput-object v2, v0, v1

    const/16 v1, 0x29

    const-string/jumbo v2, "xli"

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    const-string/jumbo v2, "xlii"

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    .line 55
    const-string/jumbo v2, "xliii"

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    const-string/jumbo v2, "xliv"

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    const-string/jumbo v2, "xlv"

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    const-string/jumbo v2, "xlvi"

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    const-string/jumbo v2, "xlvii"

    aput-object v2, v0, v1

    const/16 v1, 0x30

    .line 56
    const-string/jumbo v2, "xlviii"

    aput-object v2, v0, v1

    const/16 v1, 0x31

    const-string/jumbo v2, "xlix"

    aput-object v2, v0, v1

    const/16 v1, 0x32

    const-string/jumbo v2, "l"

    aput-object v2, v0, v1

    .line 46
    sput-object v0, Lorg/apache/poi/hdf/model/util/NumberFormatter;->_roman:[Ljava/lang/String;

    .line 57
    const/16 v0, 0x19

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "a"

    aput-object v1, v0, v3

    const-string/jumbo v1, "b"

    aput-object v1, v0, v4

    const-string/jumbo v1, "c"

    aput-object v1, v0, v5

    const-string/jumbo v1, "d"

    aput-object v1, v0, v6

    const-string/jumbo v1, "e"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string/jumbo v2, "f"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string/jumbo v2, "g"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    .line 58
    const-string/jumbo v2, "h"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string/jumbo v2, "i"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string/jumbo v2, "j"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string/jumbo v2, "k"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string/jumbo v2, "l"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string/jumbo v2, "m"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string/jumbo v2, "n"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    .line 59
    const-string/jumbo v2, "o"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string/jumbo v2, "p"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string/jumbo v2, "q"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string/jumbo v2, "r"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string/jumbo v2, "s"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string/jumbo v2, "t"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string/jumbo v2, "u"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    .line 60
    const-string/jumbo v2, "v"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string/jumbo v2, "x"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string/jumbo v2, "y"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string/jumbo v2, "z"

    aput-object v2, v0, v1

    .line 57
    sput-object v0, Lorg/apache/poi/hdf/model/util/NumberFormatter;->_letter:[Ljava/lang/String;

    .line 60
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    return-void
.end method

.method public static getNumber(II)Ljava/lang/String;
    .locals 2
    .param p0, "num"    # I
    .param p1, "style"    # I

    .prologue
    .line 66
    packed-switch p1, :pswitch_data_0

    .line 81
    sget-object v0, Lorg/apache/poi/hdf/model/util/NumberFormatter;->_arabic:[Ljava/lang/String;

    add-int/lit8 v1, p0, -0x1

    aget-object v0, v0, v1

    :goto_0
    return-object v0

    .line 69
    :pswitch_0
    sget-object v0, Lorg/apache/poi/hdf/model/util/NumberFormatter;->_arabic:[Ljava/lang/String;

    add-int/lit8 v1, p0, -0x1

    aget-object v0, v0, v1

    goto :goto_0

    .line 71
    :pswitch_1
    sget-object v0, Lorg/apache/poi/hdf/model/util/NumberFormatter;->_roman:[Ljava/lang/String;

    add-int/lit8 v1, p0, -0x1

    aget-object v0, v0, v1

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 73
    :pswitch_2
    sget-object v0, Lorg/apache/poi/hdf/model/util/NumberFormatter;->_roman:[Ljava/lang/String;

    add-int/lit8 v1, p0, -0x1

    aget-object v0, v0, v1

    goto :goto_0

    .line 75
    :pswitch_3
    sget-object v0, Lorg/apache/poi/hdf/model/util/NumberFormatter;->_letter:[Ljava/lang/String;

    add-int/lit8 v1, p0, -0x1

    aget-object v0, v0, v1

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 77
    :pswitch_4
    sget-object v0, Lorg/apache/poi/hdf/model/util/NumberFormatter;->_letter:[Ljava/lang/String;

    add-int/lit8 v1, p0, -0x1

    aget-object v0, v0, v1

    goto :goto_0

    .line 79
    :pswitch_5
    sget-object v0, Lorg/apache/poi/hdf/model/util/NumberFormatter;->_arabic:[Ljava/lang/String;

    add-int/lit8 v1, p0, -0x1

    aget-object v0, v0, v1

    goto :goto_0

    .line 66
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method
