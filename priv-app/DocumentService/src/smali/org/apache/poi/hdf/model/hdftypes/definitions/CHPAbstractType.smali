.class public abstract Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;
.super Ljava/lang/Object;
.source "CHPAbstractType.java"

# interfaces
.implements Lorg/apache/poi/hdf/model/hdftypes/HDFType;


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field private static fBold:Lorg/apache/poi/util/BitField;

.field private static fCaps:Lorg/apache/poi/util/BitField;

.field private static fChsDiff:Lorg/apache/poi/util/BitField;

.field private static fDStrike:Lorg/apache/poi/util/BitField;

.field private static fData:Lorg/apache/poi/util/BitField;

.field private static fEmboss:Lorg/apache/poi/util/BitField;

.field private static fFldVanish:Lorg/apache/poi/util/BitField;

.field private static fFtcAsciSym:Lorg/apache/poi/util/BitField;

.field private static fHighlight:Lorg/apache/poi/util/BitField;

.field private static fImprint:Lorg/apache/poi/util/BitField;

.field private static fItalic:Lorg/apache/poi/util/BitField;

.field private static fLowerCase:Lorg/apache/poi/util/BitField;

.field private static fMacChs:Lorg/apache/poi/util/BitField;

.field private static fNavHighlight:Lorg/apache/poi/util/BitField;

.field private static fObj:Lorg/apache/poi/util/BitField;

.field private static fOle2:Lorg/apache/poi/util/BitField;

.field private static fOutline:Lorg/apache/poi/util/BitField;

.field private static fRMark:Lorg/apache/poi/util/BitField;

.field private static fRMarkDel:Lorg/apache/poi/util/BitField;

.field private static fShadow:Lorg/apache/poi/util/BitField;

.field private static fSmallCaps:Lorg/apache/poi/util/BitField;

.field private static fSpec:Lorg/apache/poi/util/BitField;

.field private static fStrike:Lorg/apache/poi/util/BitField;

.field private static fUsePgsuSettings:Lorg/apache/poi/util/BitField;

.field private static fVanish:Lorg/apache/poi/util/BitField;

.field private static icoHighlight:Lorg/apache/poi/util/BitField;

.field private static kcd:Lorg/apache/poi/util/BitField;


# instance fields
.field private field_10_kul:B

.field private field_11_ico:B

.field private field_12_hpsPos:I

.field private field_13_lidDefault:I

.field private field_14_lidFE:I

.field private field_15_idctHint:B

.field private field_16_wCharScale:I

.field private field_17_fcPic:I

.field private field_18_fcObj:I

.field private field_19_lTagObj:I

.field private field_1_chse:S

.field private field_20_ibstRMark:I

.field private field_21_ibstRMarkDel:I

.field private field_22_dttmRMark:[S

.field private field_23_dttmRMarkDel:[S

.field private field_24_istd:I

.field private field_25_baseIstd:I

.field private field_26_ftcSym:I

.field private field_27_xchSym:I

.field private field_28_idslRMReason:I

.field private field_29_idslReasonDel:I

.field private field_2_format_flags:I

.field private field_30_ysr:B

.field private field_31_chYsr:B

.field private field_32_hpsKern:I

.field private field_33_Highlight:S

.field private field_34_fPropMark:S

.field private field_35_ibstPropRMark:I

.field private field_36_dttmPropRMark:I

.field private field_37_sfxtText:B

.field private field_38_fDispFldRMark:B

.field private field_39_ibstDispFldRMark:I

.field private field_3_format_flags1:I

.field private field_40_dttmDispFldRMark:I

.field private field_41_xstDispFldRMark:[B

.field private field_42_shd:I

.field private field_43_brc:[S

.field private field_4_ftcAscii:I

.field private field_5_ftcFE:I

.field private field_6_ftcOther:I

.field private field_7_hps:I

.field private field_8_dxaSpace:I

.field private field_9_iss:B


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/16 v5, 0x20

    const/16 v4, 0x8

    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 39
    invoke-static {v1}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->fBold:Lorg/apache/poi/util/BitField;

    .line 40
    invoke-static {v2}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->fItalic:Lorg/apache/poi/util/BitField;

    .line 41
    invoke-static {v3}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->fRMarkDel:Lorg/apache/poi/util/BitField;

    .line 42
    invoke-static {v4}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->fOutline:Lorg/apache/poi/util/BitField;

    .line 43
    const/16 v0, 0x10

    invoke-static {v0}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->fFldVanish:Lorg/apache/poi/util/BitField;

    .line 44
    invoke-static {v5}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->fSmallCaps:Lorg/apache/poi/util/BitField;

    .line 45
    const/16 v0, 0x40

    invoke-static {v0}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->fCaps:Lorg/apache/poi/util/BitField;

    .line 46
    const/16 v0, 0x80

    invoke-static {v0}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->fVanish:Lorg/apache/poi/util/BitField;

    .line 47
    const/16 v0, 0x100

    invoke-static {v0}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->fRMark:Lorg/apache/poi/util/BitField;

    .line 48
    const/16 v0, 0x200

    invoke-static {v0}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->fSpec:Lorg/apache/poi/util/BitField;

    .line 49
    const/16 v0, 0x400

    invoke-static {v0}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->fStrike:Lorg/apache/poi/util/BitField;

    .line 50
    const/16 v0, 0x800

    invoke-static {v0}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->fObj:Lorg/apache/poi/util/BitField;

    .line 51
    const/16 v0, 0x1000

    invoke-static {v0}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->fShadow:Lorg/apache/poi/util/BitField;

    .line 52
    const/16 v0, 0x2000

    invoke-static {v0}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->fLowerCase:Lorg/apache/poi/util/BitField;

    .line 53
    const/16 v0, 0x4000

    invoke-static {v0}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->fData:Lorg/apache/poi/util/BitField;

    .line 54
    const v0, 0x8000

    invoke-static {v0}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->fOle2:Lorg/apache/poi/util/BitField;

    .line 56
    invoke-static {v1}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->fEmboss:Lorg/apache/poi/util/BitField;

    .line 57
    invoke-static {v2}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->fImprint:Lorg/apache/poi/util/BitField;

    .line 58
    invoke-static {v3}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->fDStrike:Lorg/apache/poi/util/BitField;

    .line 59
    invoke-static {v4}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->fUsePgsuSettings:Lorg/apache/poi/util/BitField;

    .line 90
    const/16 v0, 0x1f

    invoke-static {v0}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->icoHighlight:Lorg/apache/poi/util/BitField;

    .line 91
    invoke-static {v5}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->fHighlight:Lorg/apache/poi/util/BitField;

    .line 92
    const/16 v0, 0x1c0

    invoke-static {v0}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->kcd:Lorg/apache/poi/util/BitField;

    .line 93
    const/16 v0, 0x200

    invoke-static {v0}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->fNavHighlight:Lorg/apache/poi/util/BitField;

    .line 94
    const/16 v0, 0x400

    invoke-static {v0}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->fChsDiff:Lorg/apache/poi/util/BitField;

    .line 95
    const/16 v0, 0x800

    invoke-static {v0}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->fMacChs:Lorg/apache/poi/util/BitField;

    .line 96
    const/16 v0, 0x1000

    invoke-static {v0}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->fFtcAsciSym:Lorg/apache/poi/util/BitField;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 109
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 112
    return-void
.end method


# virtual methods
.method public getBaseIstd()I
    .locals 1

    .prologue
    .line 513
    iget v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_25_baseIstd:I

    return v0
.end method

.method public getBrc()[S
    .locals 1

    .prologue
    .line 801
    iget-object v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_43_brc:[S

    return-object v0
.end method

.method public getChYsr()B
    .locals 1

    .prologue
    .line 609
    iget-byte v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_31_chYsr:B

    return v0
.end method

.method public getChse()S
    .locals 1

    .prologue
    .line 129
    iget-short v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_1_chse:S

    return v0
.end method

.method public getDttmDispFldRMark()I
    .locals 1

    .prologue
    .line 753
    iget v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_40_dttmDispFldRMark:I

    return v0
.end method

.method public getDttmPropRMark()I
    .locals 1

    .prologue
    .line 689
    iget v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_36_dttmPropRMark:I

    return v0
.end method

.method public getDttmRMark()[S
    .locals 1

    .prologue
    .line 465
    iget-object v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_22_dttmRMark:[S

    return-object v0
.end method

.method public getDttmRMarkDel()[S
    .locals 1

    .prologue
    .line 481
    iget-object v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_23_dttmRMarkDel:[S

    return-object v0
.end method

.method public getDxaSpace()I
    .locals 1

    .prologue
    .line 241
    iget v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_8_dxaSpace:I

    return v0
.end method

.method public getFDispFldRMark()B
    .locals 1

    .prologue
    .line 721
    iget-byte v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_38_fDispFldRMark:B

    return v0
.end method

.method public getFPropMark()S
    .locals 1

    .prologue
    .line 657
    iget-short v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_34_fPropMark:S

    return v0
.end method

.method public getFcObj()I
    .locals 1

    .prologue
    .line 401
    iget v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_18_fcObj:I

    return v0
.end method

.method public getFcPic()I
    .locals 1

    .prologue
    .line 385
    iget v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_17_fcPic:I

    return v0
.end method

.method public getFormat_flags()I
    .locals 1

    .prologue
    .line 145
    iget v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_2_format_flags:I

    return v0
.end method

.method public getFormat_flags1()I
    .locals 1

    .prologue
    .line 161
    iget v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_3_format_flags1:I

    return v0
.end method

.method public getFtcAscii()I
    .locals 1

    .prologue
    .line 177
    iget v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_4_ftcAscii:I

    return v0
.end method

.method public getFtcFE()I
    .locals 1

    .prologue
    .line 193
    iget v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_5_ftcFE:I

    return v0
.end method

.method public getFtcOther()I
    .locals 1

    .prologue
    .line 209
    iget v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_6_ftcOther:I

    return v0
.end method

.method public getFtcSym()I
    .locals 1

    .prologue
    .line 529
    iget v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_26_ftcSym:I

    return v0
.end method

.method public getHighlight()S
    .locals 1

    .prologue
    .line 641
    iget-short v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_33_Highlight:S

    return v0
.end method

.method public getHps()I
    .locals 1

    .prologue
    .line 225
    iget v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_7_hps:I

    return v0
.end method

.method public getHpsKern()I
    .locals 1

    .prologue
    .line 625
    iget v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_32_hpsKern:I

    return v0
.end method

.method public getHpsPos()I
    .locals 1

    .prologue
    .line 305
    iget v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_12_hpsPos:I

    return v0
.end method

.method public getIbstDispFldRMark()I
    .locals 1

    .prologue
    .line 737
    iget v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_39_ibstDispFldRMark:I

    return v0
.end method

.method public getIbstPropRMark()I
    .locals 1

    .prologue
    .line 673
    iget v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_35_ibstPropRMark:I

    return v0
.end method

.method public getIbstRMark()I
    .locals 1

    .prologue
    .line 433
    iget v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_20_ibstRMark:I

    return v0
.end method

.method public getIbstRMarkDel()I
    .locals 1

    .prologue
    .line 449
    iget v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_21_ibstRMarkDel:I

    return v0
.end method

.method public getIco()B
    .locals 1

    .prologue
    .line 289
    iget-byte v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_11_ico:B

    return v0
.end method

.method public getIcoHighlight()B
    .locals 2

    .prologue
    .line 1187
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->icoHighlight:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_33_Highlight:S

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->getValue(I)I

    move-result v0

    int-to-byte v0, v0

    return v0
.end method

.method public getIdctHint()B
    .locals 1

    .prologue
    .line 353
    iget-byte v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_15_idctHint:B

    return v0
.end method

.method public getIdslRMReason()I
    .locals 1

    .prologue
    .line 561
    iget v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_28_idslRMReason:I

    return v0
.end method

.method public getIdslReasonDel()I
    .locals 1

    .prologue
    .line 577
    iget v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_29_idslReasonDel:I

    return v0
.end method

.method public getIss()B
    .locals 1

    .prologue
    .line 257
    iget-byte v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_9_iss:B

    return v0
.end method

.method public getIstd()I
    .locals 1

    .prologue
    .line 497
    iget v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_24_istd:I

    return v0
.end method

.method public getKcd()B
    .locals 2

    .prologue
    .line 1223
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->kcd:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_33_Highlight:S

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->getValue(I)I

    move-result v0

    int-to-byte v0, v0

    return v0
.end method

.method public getKul()B
    .locals 1

    .prologue
    .line 273
    iget-byte v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_10_kul:B

    return v0
.end method

.method public getLTagObj()I
    .locals 1

    .prologue
    .line 417
    iget v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_19_lTagObj:I

    return v0
.end method

.method public getLidDefault()I
    .locals 1

    .prologue
    .line 321
    iget v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_13_lidDefault:I

    return v0
.end method

.method public getLidFE()I
    .locals 1

    .prologue
    .line 337
    iget v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_14_lidFE:I

    return v0
.end method

.method public getSfxtText()B
    .locals 1

    .prologue
    .line 705
    iget-byte v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_37_sfxtText:B

    return v0
.end method

.method public getShd()I
    .locals 1

    .prologue
    .line 785
    iget v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_42_shd:I

    return v0
.end method

.method public getSize()I
    .locals 1

    .prologue
    .line 119
    const/16 v0, 0x82

    return v0
.end method

.method public getWCharScale()I
    .locals 1

    .prologue
    .line 369
    iget v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_16_wCharScale:I

    return v0
.end method

.method public getXchSym()I
    .locals 1

    .prologue
    .line 545
    iget v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_27_xchSym:I

    return v0
.end method

.method public getXstDispFldRMark()[B
    .locals 1

    .prologue
    .line 769
    iget-object v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_41_xstDispFldRMark:[B

    return-object v0
.end method

.method public getYsr()B
    .locals 1

    .prologue
    .line 593
    iget-byte v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_30_ysr:B

    return v0
.end method

.method public isFBold()Z
    .locals 2

    .prologue
    .line 827
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->fBold:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_2_format_flags:I

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFCaps()Z
    .locals 2

    .prologue
    .line 935
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->fCaps:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_2_format_flags:I

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFChsDiff()Z
    .locals 2

    .prologue
    .line 1259
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->fChsDiff:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_33_Highlight:S

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFDStrike()Z
    .locals 2

    .prologue
    .line 1151
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->fDStrike:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_3_format_flags1:I

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFData()Z
    .locals 2

    .prologue
    .line 1079
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->fData:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_2_format_flags:I

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFEmboss()Z
    .locals 2

    .prologue
    .line 1115
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->fEmboss:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_3_format_flags1:I

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFFldVanish()Z
    .locals 2

    .prologue
    .line 899
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->fFldVanish:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_2_format_flags:I

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFFtcAsciSym()Z
    .locals 2

    .prologue
    .line 1295
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->fFtcAsciSym:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_33_Highlight:S

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFHighlight()Z
    .locals 2

    .prologue
    .line 1205
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->fHighlight:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_33_Highlight:S

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFImprint()Z
    .locals 2

    .prologue
    .line 1133
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->fImprint:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_3_format_flags1:I

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFItalic()Z
    .locals 2

    .prologue
    .line 845
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->fItalic:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_2_format_flags:I

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFLowerCase()Z
    .locals 2

    .prologue
    .line 1061
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->fLowerCase:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_2_format_flags:I

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFMacChs()Z
    .locals 2

    .prologue
    .line 1277
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->fMacChs:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_33_Highlight:S

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFNavHighlight()Z
    .locals 2

    .prologue
    .line 1241
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->fNavHighlight:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_33_Highlight:S

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFObj()Z
    .locals 2

    .prologue
    .line 1025
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->fObj:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_2_format_flags:I

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFOle2()Z
    .locals 2

    .prologue
    .line 1097
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->fOle2:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_2_format_flags:I

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFOutline()Z
    .locals 2

    .prologue
    .line 881
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->fOutline:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_2_format_flags:I

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFRMark()Z
    .locals 2

    .prologue
    .line 971
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->fRMark:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_2_format_flags:I

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFRMarkDel()Z
    .locals 2

    .prologue
    .line 863
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->fRMarkDel:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_2_format_flags:I

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFShadow()Z
    .locals 2

    .prologue
    .line 1043
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->fShadow:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_2_format_flags:I

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFSmallCaps()Z
    .locals 2

    .prologue
    .line 917
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->fSmallCaps:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_2_format_flags:I

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFSpec()Z
    .locals 2

    .prologue
    .line 989
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->fSpec:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_2_format_flags:I

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFStrike()Z
    .locals 2

    .prologue
    .line 1007
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->fStrike:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_2_format_flags:I

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFUsePgsuSettings()Z
    .locals 2

    .prologue
    .line 1169
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->fUsePgsuSettings:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_3_format_flags1:I

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFVanish()Z
    .locals 2

    .prologue
    .line 953
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->fVanish:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_2_format_flags:I

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public setBaseIstd(I)V
    .locals 0
    .param p1, "field_25_baseIstd"    # I

    .prologue
    .line 521
    iput p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_25_baseIstd:I

    .line 522
    return-void
.end method

.method public setBrc([S)V
    .locals 0
    .param p1, "field_43_brc"    # [S

    .prologue
    .line 809
    iput-object p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_43_brc:[S

    .line 810
    return-void
.end method

.method public setChYsr(B)V
    .locals 0
    .param p1, "field_31_chYsr"    # B

    .prologue
    .line 617
    iput-byte p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_31_chYsr:B

    .line 618
    return-void
.end method

.method public setChse(S)V
    .locals 0
    .param p1, "field_1_chse"    # S

    .prologue
    .line 137
    iput-short p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_1_chse:S

    .line 138
    return-void
.end method

.method public setDttmDispFldRMark(I)V
    .locals 0
    .param p1, "field_40_dttmDispFldRMark"    # I

    .prologue
    .line 761
    iput p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_40_dttmDispFldRMark:I

    .line 762
    return-void
.end method

.method public setDttmPropRMark(I)V
    .locals 0
    .param p1, "field_36_dttmPropRMark"    # I

    .prologue
    .line 697
    iput p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_36_dttmPropRMark:I

    .line 698
    return-void
.end method

.method public setDttmRMark([S)V
    .locals 0
    .param p1, "field_22_dttmRMark"    # [S

    .prologue
    .line 473
    iput-object p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_22_dttmRMark:[S

    .line 474
    return-void
.end method

.method public setDttmRMarkDel([S)V
    .locals 0
    .param p1, "field_23_dttmRMarkDel"    # [S

    .prologue
    .line 489
    iput-object p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_23_dttmRMarkDel:[S

    .line 490
    return-void
.end method

.method public setDxaSpace(I)V
    .locals 0
    .param p1, "field_8_dxaSpace"    # I

    .prologue
    .line 249
    iput p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_8_dxaSpace:I

    .line 250
    return-void
.end method

.method public setFBold(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 818
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->fBold:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_2_format_flags:I

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_2_format_flags:I

    .line 819
    return-void
.end method

.method public setFCaps(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 926
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->fCaps:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_2_format_flags:I

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_2_format_flags:I

    .line 927
    return-void
.end method

.method public setFChsDiff(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 1250
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->fChsDiff:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_33_Highlight:S

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_33_Highlight:S

    .line 1251
    return-void
.end method

.method public setFDStrike(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 1142
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->fDStrike:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_3_format_flags1:I

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_3_format_flags1:I

    .line 1143
    return-void
.end method

.method public setFData(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 1070
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->fData:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_2_format_flags:I

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_2_format_flags:I

    .line 1071
    return-void
.end method

.method public setFDispFldRMark(B)V
    .locals 0
    .param p1, "field_38_fDispFldRMark"    # B

    .prologue
    .line 729
    iput-byte p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_38_fDispFldRMark:B

    .line 730
    return-void
.end method

.method public setFEmboss(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 1106
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->fEmboss:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_3_format_flags1:I

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_3_format_flags1:I

    .line 1107
    return-void
.end method

.method public setFFldVanish(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 890
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->fFldVanish:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_2_format_flags:I

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_2_format_flags:I

    .line 891
    return-void
.end method

.method public setFFtcAsciSym(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 1286
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->fFtcAsciSym:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_33_Highlight:S

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_33_Highlight:S

    .line 1287
    return-void
.end method

.method public setFHighlight(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 1196
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->fHighlight:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_33_Highlight:S

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_33_Highlight:S

    .line 1197
    return-void
.end method

.method public setFImprint(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 1124
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->fImprint:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_3_format_flags1:I

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_3_format_flags1:I

    .line 1125
    return-void
.end method

.method public setFItalic(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 836
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->fItalic:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_2_format_flags:I

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_2_format_flags:I

    .line 837
    return-void
.end method

.method public setFLowerCase(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 1052
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->fLowerCase:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_2_format_flags:I

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_2_format_flags:I

    .line 1053
    return-void
.end method

.method public setFMacChs(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 1268
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->fMacChs:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_33_Highlight:S

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_33_Highlight:S

    .line 1269
    return-void
.end method

.method public setFNavHighlight(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 1232
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->fNavHighlight:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_33_Highlight:S

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_33_Highlight:S

    .line 1233
    return-void
.end method

.method public setFObj(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 1016
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->fObj:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_2_format_flags:I

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_2_format_flags:I

    .line 1017
    return-void
.end method

.method public setFOle2(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 1088
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->fOle2:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_2_format_flags:I

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_2_format_flags:I

    .line 1089
    return-void
.end method

.method public setFOutline(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 872
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->fOutline:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_2_format_flags:I

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_2_format_flags:I

    .line 873
    return-void
.end method

.method public setFPropMark(S)V
    .locals 0
    .param p1, "field_34_fPropMark"    # S

    .prologue
    .line 665
    iput-short p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_34_fPropMark:S

    .line 666
    return-void
.end method

.method public setFRMark(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 962
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->fRMark:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_2_format_flags:I

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_2_format_flags:I

    .line 963
    return-void
.end method

.method public setFRMarkDel(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 854
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->fRMarkDel:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_2_format_flags:I

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_2_format_flags:I

    .line 855
    return-void
.end method

.method public setFShadow(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 1034
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->fShadow:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_2_format_flags:I

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_2_format_flags:I

    .line 1035
    return-void
.end method

.method public setFSmallCaps(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 908
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->fSmallCaps:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_2_format_flags:I

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_2_format_flags:I

    .line 909
    return-void
.end method

.method public setFSpec(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 980
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->fSpec:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_2_format_flags:I

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_2_format_flags:I

    .line 981
    return-void
.end method

.method public setFStrike(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 998
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->fStrike:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_2_format_flags:I

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_2_format_flags:I

    .line 999
    return-void
.end method

.method public setFUsePgsuSettings(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 1160
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->fUsePgsuSettings:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_3_format_flags1:I

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_3_format_flags1:I

    .line 1161
    return-void
.end method

.method public setFVanish(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 944
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->fVanish:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_2_format_flags:I

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_2_format_flags:I

    .line 945
    return-void
.end method

.method public setFcObj(I)V
    .locals 0
    .param p1, "field_18_fcObj"    # I

    .prologue
    .line 409
    iput p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_18_fcObj:I

    .line 410
    return-void
.end method

.method public setFcPic(I)V
    .locals 0
    .param p1, "field_17_fcPic"    # I

    .prologue
    .line 393
    iput p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_17_fcPic:I

    .line 394
    return-void
.end method

.method public setFormat_flags(I)V
    .locals 0
    .param p1, "field_2_format_flags"    # I

    .prologue
    .line 153
    iput p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_2_format_flags:I

    .line 154
    return-void
.end method

.method public setFormat_flags1(I)V
    .locals 0
    .param p1, "field_3_format_flags1"    # I

    .prologue
    .line 169
    iput p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_3_format_flags1:I

    .line 170
    return-void
.end method

.method public setFtcAscii(I)V
    .locals 0
    .param p1, "field_4_ftcAscii"    # I

    .prologue
    .line 185
    iput p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_4_ftcAscii:I

    .line 186
    return-void
.end method

.method public setFtcFE(I)V
    .locals 0
    .param p1, "field_5_ftcFE"    # I

    .prologue
    .line 201
    iput p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_5_ftcFE:I

    .line 202
    return-void
.end method

.method public setFtcOther(I)V
    .locals 0
    .param p1, "field_6_ftcOther"    # I

    .prologue
    .line 217
    iput p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_6_ftcOther:I

    .line 218
    return-void
.end method

.method public setFtcSym(I)V
    .locals 0
    .param p1, "field_26_ftcSym"    # I

    .prologue
    .line 537
    iput p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_26_ftcSym:I

    .line 538
    return-void
.end method

.method public setHighlight(S)V
    .locals 0
    .param p1, "field_33_Highlight"    # S

    .prologue
    .line 649
    iput-short p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_33_Highlight:S

    .line 650
    return-void
.end method

.method public setHps(I)V
    .locals 0
    .param p1, "field_7_hps"    # I

    .prologue
    .line 233
    iput p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_7_hps:I

    .line 234
    return-void
.end method

.method public setHpsKern(I)V
    .locals 0
    .param p1, "field_32_hpsKern"    # I

    .prologue
    .line 633
    iput p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_32_hpsKern:I

    .line 634
    return-void
.end method

.method public setHpsPos(I)V
    .locals 0
    .param p1, "field_12_hpsPos"    # I

    .prologue
    .line 313
    iput p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_12_hpsPos:I

    .line 314
    return-void
.end method

.method public setIbstDispFldRMark(I)V
    .locals 0
    .param p1, "field_39_ibstDispFldRMark"    # I

    .prologue
    .line 745
    iput p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_39_ibstDispFldRMark:I

    .line 746
    return-void
.end method

.method public setIbstPropRMark(I)V
    .locals 0
    .param p1, "field_35_ibstPropRMark"    # I

    .prologue
    .line 681
    iput p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_35_ibstPropRMark:I

    .line 682
    return-void
.end method

.method public setIbstRMark(I)V
    .locals 0
    .param p1, "field_20_ibstRMark"    # I

    .prologue
    .line 441
    iput p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_20_ibstRMark:I

    .line 442
    return-void
.end method

.method public setIbstRMarkDel(I)V
    .locals 0
    .param p1, "field_21_ibstRMarkDel"    # I

    .prologue
    .line 457
    iput p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_21_ibstRMarkDel:I

    .line 458
    return-void
.end method

.method public setIco(B)V
    .locals 0
    .param p1, "field_11_ico"    # B

    .prologue
    .line 297
    iput-byte p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_11_ico:B

    .line 298
    return-void
.end method

.method public setIcoHighlight(B)V
    .locals 2
    .param p1, "value"    # B

    .prologue
    .line 1178
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->icoHighlight:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_33_Highlight:S

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setValue(II)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_33_Highlight:S

    .line 1179
    return-void
.end method

.method public setIdctHint(B)V
    .locals 0
    .param p1, "field_15_idctHint"    # B

    .prologue
    .line 361
    iput-byte p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_15_idctHint:B

    .line 362
    return-void
.end method

.method public setIdslRMReason(I)V
    .locals 0
    .param p1, "field_28_idslRMReason"    # I

    .prologue
    .line 569
    iput p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_28_idslRMReason:I

    .line 570
    return-void
.end method

.method public setIdslReasonDel(I)V
    .locals 0
    .param p1, "field_29_idslReasonDel"    # I

    .prologue
    .line 585
    iput p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_29_idslReasonDel:I

    .line 586
    return-void
.end method

.method public setIss(B)V
    .locals 0
    .param p1, "field_9_iss"    # B

    .prologue
    .line 265
    iput-byte p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_9_iss:B

    .line 266
    return-void
.end method

.method public setIstd(I)V
    .locals 0
    .param p1, "field_24_istd"    # I

    .prologue
    .line 505
    iput p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_24_istd:I

    .line 506
    return-void
.end method

.method public setKcd(B)V
    .locals 2
    .param p1, "value"    # B

    .prologue
    .line 1214
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->kcd:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_33_Highlight:S

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setValue(II)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_33_Highlight:S

    .line 1215
    return-void
.end method

.method public setKul(B)V
    .locals 0
    .param p1, "field_10_kul"    # B

    .prologue
    .line 281
    iput-byte p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_10_kul:B

    .line 282
    return-void
.end method

.method public setLTagObj(I)V
    .locals 0
    .param p1, "field_19_lTagObj"    # I

    .prologue
    .line 425
    iput p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_19_lTagObj:I

    .line 426
    return-void
.end method

.method public setLidDefault(I)V
    .locals 0
    .param p1, "field_13_lidDefault"    # I

    .prologue
    .line 329
    iput p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_13_lidDefault:I

    .line 330
    return-void
.end method

.method public setLidFE(I)V
    .locals 0
    .param p1, "field_14_lidFE"    # I

    .prologue
    .line 345
    iput p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_14_lidFE:I

    .line 346
    return-void
.end method

.method public setSfxtText(B)V
    .locals 0
    .param p1, "field_37_sfxtText"    # B

    .prologue
    .line 713
    iput-byte p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_37_sfxtText:B

    .line 714
    return-void
.end method

.method public setShd(I)V
    .locals 0
    .param p1, "field_42_shd"    # I

    .prologue
    .line 793
    iput p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_42_shd:I

    .line 794
    return-void
.end method

.method public setWCharScale(I)V
    .locals 0
    .param p1, "field_16_wCharScale"    # I

    .prologue
    .line 377
    iput p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_16_wCharScale:I

    .line 378
    return-void
.end method

.method public setXchSym(I)V
    .locals 0
    .param p1, "field_27_xchSym"    # I

    .prologue
    .line 553
    iput p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_27_xchSym:I

    .line 554
    return-void
.end method

.method public setXstDispFldRMark([B)V
    .locals 0
    .param p1, "field_41_xstDispFldRMark"    # [B

    .prologue
    .line 777
    iput-object p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_41_xstDispFldRMark:[B

    .line 778
    return-void
.end method

.method public setYsr(B)V
    .locals 0
    .param p1, "field_30_ysr"    # B

    .prologue
    .line 601
    iput-byte p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;->field_30_ysr:B

    .line 602
    return-void
.end method
