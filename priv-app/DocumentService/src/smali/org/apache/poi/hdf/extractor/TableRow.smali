.class public final Lorg/apache/poi/hdf/extractor/TableRow;
.super Ljava/lang/Object;
.source "TableRow.java"


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field _cells:Ljava/util/ArrayList;

.field _descriptor:Lorg/apache/poi/hdf/extractor/TAP;


# direct methods
.method public constructor <init>(Ljava/util/ArrayList;Lorg/apache/poi/hdf/extractor/TAP;)V
    .locals 0
    .param p1, "cells"    # Ljava/util/ArrayList;
    .param p2, "descriptor"    # Lorg/apache/poi/hdf/extractor/TAP;

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object p1, p0, Lorg/apache/poi/hdf/extractor/TableRow;->_cells:Ljava/util/ArrayList;

    .line 36
    iput-object p2, p0, Lorg/apache/poi/hdf/extractor/TableRow;->_descriptor:Lorg/apache/poi/hdf/extractor/TAP;

    .line 37
    return-void
.end method


# virtual methods
.method public getCells()Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lorg/apache/poi/hdf/extractor/TableRow;->_cells:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getTAP()Lorg/apache/poi/hdf/extractor/TAP;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lorg/apache/poi/hdf/extractor/TableRow;->_descriptor:Lorg/apache/poi/hdf/extractor/TAP;

    return-object v0
.end method
