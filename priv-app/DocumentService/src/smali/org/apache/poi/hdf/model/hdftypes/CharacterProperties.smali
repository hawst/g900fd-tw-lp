.class public final Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;
.super Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;
.source "CharacterProperties.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/16 v2, 0x400

    const/4 v1, 0x2

    .line 30
    invoke-direct {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/CHPAbstractType;-><init>()V

    .line 32
    new-array v0, v1, [S

    invoke-virtual {p0, v0}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->setDttmRMark([S)V

    .line 33
    new-array v0, v1, [S

    invoke-virtual {p0, v0}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->setDttmRMarkDel([S)V

    .line 34
    const/16 v0, 0x20

    new-array v0, v0, [B

    invoke-virtual {p0, v0}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->setXstDispFldRMark([B)V

    .line 35
    new-array v0, v1, [S

    invoke-virtual {p0, v0}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->setBrc([S)V

    .line 36
    const/16 v0, 0x14

    invoke-virtual {p0, v0}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->setHps(I)V

    .line 37
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->setFcPic(I)V

    .line 38
    const/16 v0, 0xa

    invoke-virtual {p0, v0}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->setIstd(I)V

    .line 39
    invoke-virtual {p0, v2}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->setLidFE(I)V

    .line 40
    invoke-virtual {p0, v2}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->setLidDefault(I)V

    .line 41
    const/16 v0, 0x64

    invoke-virtual {p0, v0}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->setWCharScale(I)V

    .line 43
    return-void
.end method


# virtual methods
.method public clone()Ljava/lang/Object;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x2

    const/4 v4, 0x0

    .line 49
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;

    .line 50
    .local v0, "clone":Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;
    new-array v1, v3, [S

    invoke-virtual {v0, v1}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->setBrc([S)V

    .line 51
    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->getBrc()[S

    move-result-object v1

    invoke-virtual {v0}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->getBrc()[S

    move-result-object v2

    invoke-static {v1, v4, v2, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 52
    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->getDttmRMark()[S

    move-result-object v1

    invoke-virtual {v0}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->getDttmRMark()[S

    move-result-object v2

    invoke-static {v1, v4, v2, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 53
    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->getDttmRMarkDel()[S

    move-result-object v1

    invoke-virtual {v0}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->getDttmRMarkDel()[S

    move-result-object v2

    invoke-static {v1, v4, v2, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 54
    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->getXstDispFldRMark()[B

    move-result-object v1

    invoke-virtual {v0}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->getXstDispFldRMark()[B

    move-result-object v2

    const/16 v3, 0x20

    invoke-static {v1, v4, v2, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 55
    return-object v0
.end method
