.class public final Lorg/apache/poi/hdf/extractor/data/LVL;
.super Ljava/lang/Object;
.source "LVL.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field public _chpx:[B

.field _fLegal:Z

.field _fNoRestart:Z

.field _fPrev:Z

.field _fPrevSpace:Z

.field _fWord6:Z

.field public _iStartAt:I

.field public _istd:S

.field public _ixchFollow:B

.field _jc:B

.field public _nfc:B

.field public _papx:[B

.field public _rgbxchNums:[B

.field public _xst:[C


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    const/16 v0, 0x9

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/apache/poi/hdf/extractor/data/LVL;->_rgbxchNums:[B

    .line 53
    return-void
.end method


# virtual methods
.method public clone()Ljava/lang/Object;
    .locals 6

    .prologue
    .line 56
    const/4 v2, 0x0

    .line 59
    .local v2, "obj":Lorg/apache/poi/hdf/extractor/data/LVL;
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Lorg/apache/poi/hdf/extractor/data/LVL;

    move-object v2, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 65
    :goto_0
    return-object v2

    .line 61
    :catch_0
    move-exception v1

    .line 63
    .local v1, "e":Ljava/lang/Exception;
    const-string/jumbo v3, "DocumentService"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "Exception: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
