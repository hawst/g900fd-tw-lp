.class public abstract Lorg/apache/poi/hdf/model/hdftypes/FormattedDiskPage;
.super Ljava/lang/Object;
.source "FormattedDiskPage.java"


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field protected _crun:I

.field protected _fkp:[B


# direct methods
.method public constructor <init>([B)V
    .locals 1
    .param p1, "fkp"    # [B

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    const/16 v0, 0x1ff

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getUnsignedByte([BI)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hdf/model/hdftypes/FormattedDiskPage;->_crun:I

    .line 54
    iput-object p1, p0, Lorg/apache/poi/hdf/model/hdftypes/FormattedDiskPage;->_fkp:[B

    .line 55
    return-void
.end method


# virtual methods
.method public getEnd(I)I
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 72
    iget-object v0, p0, Lorg/apache/poi/hdf/model/hdftypes/FormattedDiskPage;->_fkp:[B

    add-int/lit8 v1, p1, 0x1

    mul-int/lit8 v1, v1, 0x4

    invoke-static {v0, v1}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    return v0
.end method

.method public abstract getGrpprl(I)[B
.end method

.method public getStart(I)I
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 63
    iget-object v0, p0, Lorg/apache/poi/hdf/model/hdftypes/FormattedDiskPage;->_fkp:[B

    mul-int/lit8 v1, p1, 0x4

    invoke-static {v0, v1}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    return v0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 80
    iget v0, p0, Lorg/apache/poi/hdf/model/hdftypes/FormattedDiskPage;->_crun:I

    return v0
.end method
