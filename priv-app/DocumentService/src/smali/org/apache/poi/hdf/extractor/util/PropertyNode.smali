.class public abstract Lorg/apache/poi/hdf/extractor/util/PropertyNode;
.super Ljava/lang/Object;
.source "PropertyNode.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private _fcEnd:I

.field private _fcStart:I

.field private _grpprl:[B


# direct methods
.method public constructor <init>(II[B)V
    .locals 0
    .param p1, "fcStart"    # I
    .param p2, "fcEnd"    # I
    .param p3, "grpprl"    # [B

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput p1, p0, Lorg/apache/poi/hdf/extractor/util/PropertyNode;->_fcStart:I

    .line 34
    iput p2, p0, Lorg/apache/poi/hdf/extractor/util/PropertyNode;->_fcEnd:I

    .line 35
    iput-object p3, p0, Lorg/apache/poi/hdf/extractor/util/PropertyNode;->_grpprl:[B

    .line 36
    return-void
.end method


# virtual methods
.method public compareTo(Ljava/lang/Object;)I
    .locals 2
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 51
    check-cast p1, Lorg/apache/poi/hdf/extractor/util/PropertyNode;

    .end local p1    # "o":Ljava/lang/Object;
    invoke-virtual {p1}, Lorg/apache/poi/hdf/extractor/util/PropertyNode;->getStart()I

    move-result v0

    .line 52
    .local v0, "fcStart":I
    iget v1, p0, Lorg/apache/poi/hdf/extractor/util/PropertyNode;->_fcStart:I

    if-ne v1, v0, :cond_0

    .line 54
    const/4 v1, 0x0

    .line 62
    :goto_0
    return v1

    .line 56
    :cond_0
    iget v1, p0, Lorg/apache/poi/hdf/extractor/util/PropertyNode;->_fcStart:I

    if-ge v1, v0, :cond_1

    .line 58
    const/4 v1, -0x1

    goto :goto_0

    .line 62
    :cond_1
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public getEnd()I
    .locals 1

    .prologue
    .line 43
    iget v0, p0, Lorg/apache/poi/hdf/extractor/util/PropertyNode;->_fcEnd:I

    return v0
.end method

.method protected getGrpprl()[B
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lorg/apache/poi/hdf/extractor/util/PropertyNode;->_grpprl:[B

    return-object v0
.end method

.method public getStart()I
    .locals 1

    .prologue
    .line 39
    iget v0, p0, Lorg/apache/poi/hdf/extractor/util/PropertyNode;->_fcStart:I

    return v0
.end method
