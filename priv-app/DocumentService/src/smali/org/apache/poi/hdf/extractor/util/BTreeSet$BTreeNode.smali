.class public Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;
.super Ljava/lang/Object;
.source "BTreeSet.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/poi/hdf/extractor/util/BTreeSet;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "BTreeNode"
.end annotation


# instance fields
.field private final MIN:I

.field public _entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

.field _nrElements:I

.field public _parent:Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;

.field final synthetic this$0:Lorg/apache/poi/hdf/extractor/util/BTreeSet;


# direct methods
.method constructor <init>(Lorg/apache/poi/hdf/extractor/util/BTreeSet;Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;)V
    .locals 3
    .param p2, "parent"    # Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;

    .prologue
    const/4 v2, 0x0

    .line 223
    iput-object p1, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->this$0:Lorg/apache/poi/hdf/extractor/util/BTreeSet;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 220
    iput v2, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_nrElements:I

    .line 221
    iget v0, p1, Lorg/apache/poi/hdf/extractor/util/BTreeSet;->order:I

    add-int/lit8 v0, v0, -0x1

    div-int/lit8 v0, v0, 0x2

    iput v0, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->MIN:I

    .line 224
    iput-object p2, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_parent:Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;

    .line 225
    iget v0, p1, Lorg/apache/poi/hdf/extractor/util/BTreeSet;->order:I

    new-array v0, v0, [Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    iput-object v0, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    .line 226
    iget-object v0, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    new-instance v1, Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    invoke-direct {v1}, Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;-><init>()V

    aput-object v1, v0, v2

    .line 227
    return-void
.end method

.method private childToInsertAt(Ljava/lang/Object;Z)I
    .locals 5
    .param p1, "x"    # Ljava/lang/Object;
    .param p2, "position"    # Z

    .prologue
    .line 384
    iget v3, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_nrElements:I

    div-int/lit8 v1, v3, 0x2

    .line 386
    .local v1, "index":I
    iget-object v3, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    aget-object v3, v3, v1

    if-eqz v3, :cond_0

    iget-object v3, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    aget-object v3, v3, v1

    iget-object v3, v3, Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;->element:Ljava/lang/Object;

    if-nez v3, :cond_2

    :cond_0
    move v0, v1

    .line 402
    :cond_1
    :goto_0
    return v0

    .line 388
    :cond_2
    const/4 v2, 0x0

    .local v2, "lo":I
    iget v3, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_nrElements:I

    add-int/lit8 v0, v3, -0x1

    .line 389
    .local v0, "hi":I
    :goto_1
    if-le v2, v0, :cond_3

    .line 400
    add-int/lit8 v0, v0, 0x1

    .line 401
    iget-object v3, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    aget-object v3, v3, v0

    if-eqz v3, :cond_1

    iget-object v3, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    aget-object v3, v3, v0

    iget-object v3, v3, Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;->element:Ljava/lang/Object;

    if-eqz v3, :cond_1

    .line 402
    if-eqz p2, :cond_1

    iget-object v3, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->this$0:Lorg/apache/poi/hdf/extractor/util/BTreeSet;

    iget-object v4, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    aget-object v4, v4, v0

    iget-object v4, v4, Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;->element:Ljava/lang/Object;

    invoke-virtual {v3, p1, v4}, Lorg/apache/poi/hdf/extractor/util/BTreeSet;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v3

    if-nez v3, :cond_1

    const/4 v0, -0x1

    goto :goto_0

    .line 390
    :cond_3
    iget-object v3, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->this$0:Lorg/apache/poi/hdf/extractor/util/BTreeSet;

    iget-object v4, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    aget-object v4, v4, v1

    iget-object v4, v4, Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;->element:Ljava/lang/Object;

    invoke-virtual {v3, p1, v4}, Lorg/apache/poi/hdf/extractor/util/BTreeSet;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v3

    if-lez v3, :cond_4

    .line 391
    add-int/lit8 v2, v1, 0x1

    .line 392
    add-int v3, v0, v2

    div-int/lit8 v1, v3, 0x2

    .line 393
    goto :goto_1

    .line 395
    :cond_4
    add-int/lit8 v0, v1, -0x1

    .line 396
    add-int v3, v0, v2

    div-int/lit8 v1, v3, 0x2

    goto :goto_1
.end method

.method private deleteElement(Ljava/lang/Object;)V
    .locals 4
    .param p1, "x"    # Ljava/lang/Object;

    .prologue
    .line 407
    const/4 v1, 0x0

    invoke-direct {p0, p1, v1}, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->childToInsertAt(Ljava/lang/Object;Z)I

    move-result v0

    .line 408
    .local v0, "index":I
    :goto_0
    iget v1, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_nrElements:I

    add-int/lit8 v1, v1, -0x1

    if-lt v0, v1, :cond_0

    .line 410
    iget v1, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_nrElements:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    iget-object v1, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    new-instance v2, Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    invoke-direct {v2}, Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;-><init>()V

    aput-object v2, v1, v0

    .line 413
    :goto_1
    iget v1, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_nrElements:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_nrElements:I

    .line 414
    return-void

    .line 408
    :cond_0
    iget-object v1, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    iget-object v2, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    add-int/lit8 v3, v0, 0x1

    aget-object v2, v2, v3

    aput-object v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 411
    :cond_1
    iget-object v1, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    const/4 v2, 0x0

    aput-object v2, v1, v0

    goto :goto_1
.end method

.method private fixAfterDeletion(I)V
    .locals 5
    .param p1, "parentIndex"    # I

    .prologue
    .line 442
    invoke-direct {p0}, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->isRoot()Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_parent:Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;

    invoke-direct {v3}, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->isRoot()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 456
    :cond_0
    :goto_0
    return-void

    .line 444
    :cond_1
    iget-object v3, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_parent:Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;

    iget v3, v3, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_nrElements:I

    iget v4, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->MIN:I

    if-ge v3, v4, :cond_0

    .line 445
    iget-object v1, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_parent:Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;

    .line 446
    .local v1, "temp":Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;
    invoke-direct {v1, p1}, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->prepareForDeletion(I)V

    .line 447
    iget-object v3, v1, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_parent:Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;

    if-eqz v3, :cond_0

    .line 448
    iget-object v3, v1, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_parent:Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;

    invoke-direct {v3}, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->isRoot()Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, v1, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_parent:Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;

    iget v3, v3, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_nrElements:I

    iget v4, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->MIN:I

    if-ge v3, v4, :cond_0

    .line 449
    iget-object v3, v1, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_parent:Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;

    iget-object v2, v3, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_parent:Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;

    .line 450
    .local v2, "x":Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;
    const/4 v0, 0x0

    .line 452
    .local v0, "i":I
    :goto_1
    iget-object v3, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    array-length v3, v3

    if-lt v0, v3, :cond_3

    .line 453
    :cond_2
    iget-object v3, v1, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_parent:Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;

    invoke-direct {v3, v0}, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->fixAfterDeletion(I)V

    goto :goto_0

    .line 452
    :cond_3
    iget-object v3, v2, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    aget-object v3, v3, v0

    iget-object v3, v3, Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;->child:Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;

    iget-object v4, v1, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_parent:Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;

    if-eq v3, v4, :cond_2

    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method private insertNewElement(Ljava/lang/Object;I)V
    .locals 4
    .param p1, "x"    # Ljava/lang/Object;
    .param p2, "insertAt"    # I

    .prologue
    .line 366
    iget v0, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_nrElements:I

    .local v0, "i":I
    :goto_0
    if-gt v0, p2, :cond_0

    .line 368
    iget-object v1, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    new-instance v2, Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    invoke-direct {v2}, Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;-><init>()V

    aput-object v2, v1, p2

    .line 369
    iget-object v1, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    aget-object v1, v1, p2

    iput-object p1, v1, Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;->element:Ljava/lang/Object;

    .line 371
    iget v1, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_nrElements:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_nrElements:I

    .line 372
    return-void

    .line 366
    :cond_0
    iget-object v1, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    iget-object v2, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    add-int/lit8 v3, v0, -0x1

    aget-object v2, v2, v3

    aput-object v2, v1, v0

    add-int/lit8 v0, v0, -0x1

    goto :goto_0
.end method

.method private insertSplitNode(Ljava/lang/Object;Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;I)V
    .locals 4
    .param p1, "splitNode"    # Ljava/lang/Object;
    .param p2, "left"    # Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;
    .param p3, "right"    # Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;
    .param p4, "insertAt"    # I

    .prologue
    .line 354
    iget v0, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_nrElements:I

    .local v0, "i":I
    :goto_0
    if-ge v0, p4, :cond_0

    .line 356
    iget-object v1, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    new-instance v2, Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    invoke-direct {v2}, Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;-><init>()V

    aput-object v2, v1, p4

    .line 357
    iget-object v1, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    aget-object v1, v1, p4

    iput-object p1, v1, Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;->element:Ljava/lang/Object;

    .line 358
    iget-object v1, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    aget-object v1, v1, p4

    iput-object p2, v1, Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;->child:Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;

    .line 359
    iget-object v1, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    add-int/lit8 v2, p4, 0x1

    aget-object v1, v1, v2

    iput-object p3, v1, Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;->child:Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;

    .line 361
    iget v1, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_nrElements:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_nrElements:I

    .line 362
    return-void

    .line 354
    :cond_0
    iget-object v1, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    add-int/lit8 v2, v0, 0x1

    iget-object v3, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    aget-object v3, v3, v0

    aput-object v3, v1, v2

    add-int/lit8 v0, v0, -0x1

    goto :goto_0
.end method

.method private isFull()Z
    .locals 2

    .prologue
    .line 310
    iget v0, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_nrElements:I

    iget-object v1, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->this$0:Lorg/apache/poi/hdf/extractor/util/BTreeSet;

    iget v1, v1, Lorg/apache/poi/hdf/extractor/util/BTreeSet;->order:I

    add-int/lit8 v1, v1, -0x1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isRoot()Z
    .locals 1

    .prologue
    .line 314
    iget-object v0, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_parent:Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private mergeLeft(I)V
    .locals 12
    .param p1, "parentIndex"    # I

    .prologue
    const/4 v11, 0x0

    const/4 v10, 0x0

    .line 538
    iget-object v4, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_parent:Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;

    .line 539
    .local v4, "p":Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;
    iget-object v7, v4, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    add-int/lit8 v8, p1, -0x1

    aget-object v7, v7, v8

    iget-object v2, v7, Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;->child:Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;

    .line 541
    .local v2, "ls":Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;
    invoke-virtual {p0}, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->isLeaf()Z

    move-result v7

    if-eqz v7, :cond_6

    .line 542
    iget-object v7, v4, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    add-int/lit8 v8, p1, -0x1

    aget-object v7, v7, v8

    iget-object v7, v7, Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;->element:Ljava/lang/Object;

    const/4 v8, 0x1

    invoke-direct {p0, v7, v8}, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->childToInsertAt(Ljava/lang/Object;Z)I

    move-result v0

    .line 543
    .local v0, "add":I
    iget-object v7, v4, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    add-int/lit8 v8, p1, -0x1

    aget-object v7, v7, v8

    iget-object v7, v7, Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;->element:Ljava/lang/Object;

    invoke-direct {p0, v7, v0}, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->insertNewElement(Ljava/lang/Object;I)V

    .line 544
    iget-object v7, v4, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    add-int/lit8 v8, p1, -0x1

    aget-object v7, v7, v8

    iput-object v11, v7, Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;->element:Ljava/lang/Object;

    .line 546
    iget v7, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_nrElements:I

    add-int/lit8 v1, v7, -0x1

    .local v1, "i":I
    iget v3, v2, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_nrElements:I

    .local v3, "nr":I
    :goto_0
    if-gez v1, :cond_1

    .line 549
    iget v7, v2, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_nrElements:I

    add-int/lit8 v1, v7, -0x1

    :goto_1
    if-gez v1, :cond_2

    .line 554
    iget v7, v4, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_nrElements:I

    iget v8, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->MIN:I

    if-ne v7, v8, :cond_4

    iget-object v7, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->this$0:Lorg/apache/poi/hdf/extractor/util/BTreeSet;

    iget-object v7, v7, Lorg/apache/poi/hdf/extractor/util/BTreeSet;->root:Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;

    if-eq v4, v7, :cond_4

    .line 556
    add-int/lit8 v5, p1, -0x1

    .local v5, "x":I
    add-int/lit8 v6, p1, -0x2

    .local v6, "y":I
    :goto_2
    if-gez v6, :cond_3

    .line 558
    iget-object v7, v4, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    new-instance v8, Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    invoke-direct {v8}, Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;-><init>()V

    aput-object v8, v7, v10

    .line 559
    iget-object v7, v4, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    aget-object v7, v7, v10

    iput-object v2, v7, Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;->child:Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;

    .line 569
    :goto_3
    iget v7, v4, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_nrElements:I

    add-int/lit8 v7, v7, -0x1

    iput v7, v4, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_nrElements:I

    .line 571
    invoke-direct {v4}, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->isRoot()Z

    move-result v7

    if-eqz v7, :cond_0

    iget v7, v4, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_nrElements:I

    if-nez v7, :cond_0

    .line 572
    iget-object v7, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->this$0:Lorg/apache/poi/hdf/extractor/util/BTreeSet;

    iput-object p0, v7, Lorg/apache/poi/hdf/extractor/util/BTreeSet;->root:Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;

    .line 573
    iput-object v11, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_parent:Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;

    .line 611
    .end local v0    # "add":I
    .end local v1    # "i":I
    :cond_0
    :goto_4
    return-void

    .line 547
    .end local v5    # "x":I
    .end local v6    # "y":I
    .restart local v0    # "add":I
    .restart local v1    # "i":I
    :cond_1
    iget-object v7, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    add-int v8, v1, v3

    iget-object v9, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    aget-object v9, v9, v1

    aput-object v9, v7, v8

    .line 546
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    .line 550
    :cond_2
    iget-object v7, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    iget-object v8, v2, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    aget-object v8, v8, v1

    aput-object v8, v7, v1

    .line 551
    iget v7, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_nrElements:I

    add-int/lit8 v7, v7, 0x1

    iput v7, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_nrElements:I

    .line 549
    add-int/lit8 v1, v1, -0x1

    goto :goto_1

    .line 557
    .restart local v5    # "x":I
    .restart local v6    # "y":I
    :cond_3
    iget-object v7, v4, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    iget-object v8, v4, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    aget-object v8, v8, v6

    aput-object v8, v7, v5

    .line 556
    add-int/lit8 v5, v5, -0x1

    add-int/lit8 v6, v6, -0x1

    goto :goto_2

    .line 564
    .end local v5    # "x":I
    .end local v6    # "y":I
    :cond_4
    add-int/lit8 v5, p1, -0x1

    .restart local v5    # "x":I
    move v6, p1

    .restart local v6    # "y":I
    :goto_5
    iget v7, v4, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_nrElements:I

    if-le v6, v7, :cond_5

    .line 566
    iget-object v7, v4, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    iget v8, v4, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_nrElements:I

    aput-object v11, v7, v8

    goto :goto_3

    .line 565
    :cond_5
    iget-object v7, v4, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    iget-object v8, v4, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    aget-object v8, v8, v6

    aput-object v8, v7, v5

    .line 564
    add-int/lit8 v5, v5, 0x1

    add-int/lit8 v6, v6, 0x1

    goto :goto_5

    .line 578
    .end local v0    # "add":I
    .end local v1    # "i":I
    .end local v3    # "nr":I
    .end local v5    # "x":I
    .end local v6    # "y":I
    :cond_6
    iget-object v7, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    aget-object v7, v7, v10

    iget-object v8, v4, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    add-int/lit8 v9, p1, -0x1

    aget-object v8, v8, v9

    iget-object v8, v8, Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;->element:Ljava/lang/Object;

    iput-object v8, v7, Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;->element:Ljava/lang/Object;

    .line 579
    iget-object v7, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    aget-object v7, v7, v10

    iget-object v8, v2, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    iget v9, v2, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_nrElements:I

    aget-object v8, v8, v9

    iget-object v8, v8, Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;->child:Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;

    iput-object v8, v7, Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;->child:Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;

    .line 580
    iget v7, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_nrElements:I

    add-int/lit8 v7, v7, 0x1

    iput v7, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_nrElements:I

    .line 582
    iget v5, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_nrElements:I

    .restart local v5    # "x":I
    iget v3, v2, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_nrElements:I

    .restart local v3    # "nr":I
    :goto_6
    if-gez v5, :cond_7

    .line 585
    iget v7, v2, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_nrElements:I

    add-int/lit8 v5, v7, -0x1

    :goto_7
    if-gez v5, :cond_8

    .line 591
    iget v7, v4, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_nrElements:I

    iget v8, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->MIN:I

    if-ne v7, v8, :cond_a

    iget-object v7, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->this$0:Lorg/apache/poi/hdf/extractor/util/BTreeSet;

    iget-object v7, v7, Lorg/apache/poi/hdf/extractor/util/BTreeSet;->root:Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;

    if-eq v4, v7, :cond_a

    .line 592
    add-int/lit8 v5, p1, -0x1

    add-int/lit8 v6, p1, -0x2

    .restart local v6    # "y":I
    :goto_8
    if-gez v6, :cond_9

    .line 595
    iget-object v7, v4, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    new-instance v8, Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    invoke-direct {v8}, Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;-><init>()V

    aput-object v8, v7, v10

    .line 604
    :goto_9
    iget v7, v4, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_nrElements:I

    add-int/lit8 v7, v7, -0x1

    iput v7, v4, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_nrElements:I

    .line 606
    invoke-direct {v4}, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->isRoot()Z

    move-result v7

    if-eqz v7, :cond_0

    iget v7, v4, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_nrElements:I

    if-nez v7, :cond_0

    .line 607
    iget-object v7, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->this$0:Lorg/apache/poi/hdf/extractor/util/BTreeSet;

    iput-object p0, v7, Lorg/apache/poi/hdf/extractor/util/BTreeSet;->root:Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;

    .line 608
    iput-object v11, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_parent:Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;

    goto/16 :goto_4

    .line 583
    .end local v6    # "y":I
    :cond_7
    iget-object v7, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    add-int v8, v5, v3

    iget-object v9, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    aget-object v9, v9, v5

    aput-object v9, v7, v8

    .line 582
    add-int/lit8 v5, v5, -0x1

    goto :goto_6

    .line 586
    :cond_8
    iget-object v7, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    iget-object v8, v2, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    aget-object v8, v8, v5

    aput-object v8, v7, v5

    .line 587
    iget-object v7, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    aget-object v7, v7, v5

    iget-object v7, v7, Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;->child:Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;

    iput-object p0, v7, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_parent:Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;

    .line 588
    iget v7, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_nrElements:I

    add-int/lit8 v7, v7, 0x1

    iput v7, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_nrElements:I

    .line 585
    add-int/lit8 v5, v5, -0x1

    goto :goto_7

    .line 594
    .restart local v6    # "y":I
    :cond_9
    iget-object v7, v4, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    iget-object v8, v4, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    aget-object v8, v8, v6

    aput-object v8, v7, v5

    .line 592
    add-int/lit8 v5, v5, 0x1

    add-int/lit8 v6, v6, 0x1

    goto :goto_8

    .line 599
    .end local v6    # "y":I
    :cond_a
    add-int/lit8 v5, p1, -0x1

    move v6, p1

    .restart local v6    # "y":I
    :goto_a
    iget v7, v4, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_nrElements:I

    if-le v6, v7, :cond_b

    .line 601
    iget-object v7, v4, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    iget v8, v4, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_nrElements:I

    aput-object v11, v7, v8

    goto :goto_9

    .line 600
    :cond_b
    iget-object v7, v4, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    iget-object v8, v4, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    aget-object v8, v8, v6

    aput-object v8, v7, v5

    .line 599
    add-int/lit8 v5, v5, 0x1

    add-int/lit8 v6, v6, 0x1

    goto :goto_a
.end method

.method private mergeRight(I)V
    .locals 11
    .param p1, "parentIndex"    # I

    .prologue
    const/4 v10, 0x0

    const/4 v9, 0x0

    .line 623
    iget-object v2, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_parent:Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;

    .line 624
    .local v2, "p":Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;
    iget-object v6, v2, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    add-int/lit8 v7, p1, 0x1

    aget-object v6, v6, v7

    iget-object v3, v6, Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;->child:Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;

    .line 626
    .local v3, "rs":Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;
    invoke-virtual {p0}, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->isLeaf()Z

    move-result v6

    if-eqz v6, :cond_5

    .line 627
    iget-object v6, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    iget v7, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_nrElements:I

    new-instance v8, Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    invoke-direct {v8}, Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;-><init>()V

    aput-object v8, v6, v7

    .line 628
    iget-object v6, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    iget v7, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_nrElements:I

    aget-object v6, v6, v7

    iget-object v7, v2, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    aget-object v7, v7, p1

    iget-object v7, v7, Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;->element:Ljava/lang/Object;

    iput-object v7, v6, Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;->element:Ljava/lang/Object;

    .line 629
    iget v6, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_nrElements:I

    add-int/lit8 v6, v6, 0x1

    iput v6, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_nrElements:I

    .line 630
    const/4 v0, 0x0

    .local v0, "i":I
    iget v1, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_nrElements:I

    .local v1, "nr":I
    :goto_0
    iget v6, v3, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_nrElements:I

    if-lt v0, v6, :cond_1

    .line 634
    iget-object v6, v2, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    aget-object v6, v6, p1

    iget-object v7, v2, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    add-int/lit8 v8, p1, 0x1

    aget-object v7, v7, v8

    iget-object v7, v7, Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;->element:Ljava/lang/Object;

    iput-object v7, v6, Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;->element:Ljava/lang/Object;

    .line 635
    iget v6, v2, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_nrElements:I

    iget v7, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->MIN:I

    if-ne v6, v7, :cond_3

    iget-object v6, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->this$0:Lorg/apache/poi/hdf/extractor/util/BTreeSet;

    iget-object v6, v6, Lorg/apache/poi/hdf/extractor/util/BTreeSet;->root:Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;

    if-eq v2, v6, :cond_3

    .line 636
    add-int/lit8 v4, p1, 0x1

    .local v4, "x":I
    move v5, p1

    .local v5, "y":I
    :goto_1
    if-gez v5, :cond_2

    .line 638
    iget-object v6, v2, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    new-instance v7, Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    invoke-direct {v7}, Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;-><init>()V

    aput-object v7, v6, v10

    .line 639
    iget-object v6, v2, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    aget-object v6, v6, v10

    iput-object v3, v6, Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;->child:Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;

    .line 648
    :goto_2
    iget v6, v2, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_nrElements:I

    add-int/lit8 v6, v6, -0x1

    iput v6, v2, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_nrElements:I

    .line 649
    invoke-direct {v2}, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->isRoot()Z

    move-result v6

    if-eqz v6, :cond_0

    iget v6, v2, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_nrElements:I

    if-nez v6, :cond_0

    .line 650
    iget-object v6, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->this$0:Lorg/apache/poi/hdf/extractor/util/BTreeSet;

    iput-object p0, v6, Lorg/apache/poi/hdf/extractor/util/BTreeSet;->root:Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;

    .line 651
    iput-object v9, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_parent:Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;

    .line 688
    .end local v0    # "i":I
    .end local v1    # "nr":I
    :cond_0
    :goto_3
    return-void

    .line 631
    .end local v4    # "x":I
    .end local v5    # "y":I
    .restart local v0    # "i":I
    .restart local v1    # "nr":I
    :cond_1
    iget-object v6, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    iget-object v7, v3, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    aget-object v7, v7, v0

    aput-object v7, v6, v1

    .line 632
    iget v6, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_nrElements:I

    add-int/lit8 v6, v6, 0x1

    iput v6, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_nrElements:I

    .line 630
    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 637
    .restart local v4    # "x":I
    .restart local v5    # "y":I
    :cond_2
    iget-object v6, v2, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    iget-object v7, v2, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    aget-object v7, v7, v5

    aput-object v7, v6, v4

    .line 636
    add-int/lit8 v4, v4, -0x1

    add-int/lit8 v5, v5, -0x1

    goto :goto_1

    .line 643
    .end local v4    # "x":I
    .end local v5    # "y":I
    :cond_3
    add-int/lit8 v4, p1, 0x1

    .restart local v4    # "x":I
    add-int/lit8 v5, p1, 0x2

    .restart local v5    # "y":I
    :goto_4
    iget v6, v2, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_nrElements:I

    if-le v5, v6, :cond_4

    .line 645
    iget-object v6, v2, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    iget v7, v2, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_nrElements:I

    aput-object v9, v6, v7

    goto :goto_2

    .line 644
    :cond_4
    iget-object v6, v2, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    iget-object v7, v2, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    aget-object v7, v7, v5

    aput-object v7, v6, v4

    .line 643
    add-int/lit8 v4, v4, 0x1

    add-int/lit8 v5, v5, 0x1

    goto :goto_4

    .line 657
    .end local v0    # "i":I
    .end local v1    # "nr":I
    .end local v4    # "x":I
    .end local v5    # "y":I
    :cond_5
    iget-object v6, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    iget v7, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_nrElements:I

    aget-object v6, v6, v7

    iget-object v7, v2, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    aget-object v7, v7, p1

    iget-object v7, v7, Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;->element:Ljava/lang/Object;

    iput-object v7, v6, Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;->element:Ljava/lang/Object;

    .line 658
    iget v6, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_nrElements:I

    add-int/lit8 v6, v6, 0x1

    iput v6, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_nrElements:I

    .line 660
    iget v6, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_nrElements:I

    add-int/lit8 v4, v6, 0x1

    .restart local v4    # "x":I
    const/4 v5, 0x0

    .restart local v5    # "y":I
    :goto_5
    iget v6, v3, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_nrElements:I

    if-le v5, v6, :cond_6

    .line 665
    iget v6, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_nrElements:I

    add-int/lit8 v6, v6, -0x1

    iput v6, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_nrElements:I

    .line 667
    iget-object v6, v2, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    add-int/lit8 p1, p1, 0x1

    aget-object v6, v6, p1

    iput-object p0, v6, Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;->child:Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;

    .line 669
    iget v6, v2, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_nrElements:I

    iget v7, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->MIN:I

    if-ne v6, v7, :cond_8

    iget-object v6, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->this$0:Lorg/apache/poi/hdf/extractor/util/BTreeSet;

    iget-object v6, v6, Lorg/apache/poi/hdf/extractor/util/BTreeSet;->root:Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;

    if-eq v2, v6, :cond_8

    .line 670
    add-int/lit8 v4, p1, -0x1

    add-int/lit8 v5, p1, -0x2

    :goto_6
    if-gez v5, :cond_7

    .line 672
    iget-object v6, v2, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    new-instance v7, Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    invoke-direct {v7}, Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;-><init>()V

    aput-object v7, v6, v10

    .line 681
    :goto_7
    iget v6, v2, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_nrElements:I

    add-int/lit8 v6, v6, -0x1

    iput v6, v2, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_nrElements:I

    .line 683
    invoke-direct {v2}, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->isRoot()Z

    move-result v6

    if-eqz v6, :cond_0

    iget v6, v2, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_nrElements:I

    if-nez v6, :cond_0

    .line 684
    iget-object v6, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->this$0:Lorg/apache/poi/hdf/extractor/util/BTreeSet;

    iput-object p0, v6, Lorg/apache/poi/hdf/extractor/util/BTreeSet;->root:Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;

    .line 685
    iput-object v9, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_parent:Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;

    goto/16 :goto_3

    .line 661
    :cond_6
    iget-object v6, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    iget-object v7, v3, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    aget-object v7, v7, v5

    aput-object v7, v6, v4

    .line 662
    iget-object v6, v3, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    aget-object v6, v6, v5

    iget-object v6, v6, Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;->child:Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;

    iput-object p0, v6, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_parent:Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;

    .line 663
    iget v6, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_nrElements:I

    add-int/lit8 v6, v6, 0x1

    iput v6, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_nrElements:I

    .line 660
    add-int/lit8 v4, v4, 0x1

    add-int/lit8 v5, v5, 0x1

    goto :goto_5

    .line 671
    :cond_7
    iget-object v6, v2, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    iget-object v7, v2, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    aget-object v7, v7, v5

    aput-object v7, v6, v4

    .line 670
    add-int/lit8 v4, v4, -0x1

    add-int/lit8 v5, v5, -0x1

    goto :goto_6

    .line 676
    :cond_8
    add-int/lit8 v4, p1, -0x1

    move v5, p1

    :goto_8
    iget v6, v2, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_nrElements:I

    if-le v5, v6, :cond_9

    .line 678
    iget-object v6, v2, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    iget v7, v2, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_nrElements:I

    aput-object v9, v6, v7

    goto :goto_7

    .line 677
    :cond_9
    iget-object v6, v2, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    iget-object v7, v2, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    aget-object v7, v7, v5

    aput-object v7, v6, v4

    .line 676
    add-int/lit8 v4, v4, 0x1

    add-int/lit8 v5, v5, 0x1

    goto :goto_8
.end method

.method private prepareForDeletion(I)V
    .locals 2
    .param p1, "parentIndex"    # I

    .prologue
    .line 417
    invoke-direct {p0}, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->isRoot()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 439
    :goto_0
    return-void

    .line 420
    :cond_0
    if-eqz p1, :cond_1

    iget-object v0, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_parent:Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;

    iget-object v0, v0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    add-int/lit8 v1, p1, -0x1

    aget-object v0, v0, v1

    iget-object v0, v0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;->child:Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;

    iget v0, v0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_nrElements:I

    iget v1, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->MIN:I

    if-le v0, v1, :cond_1

    .line 421
    invoke-direct {p0, p1}, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->stealLeft(I)V

    goto :goto_0

    .line 426
    :cond_1
    iget-object v0, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    array-length v0, v0

    if-ge p1, v0, :cond_2

    iget-object v0, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_parent:Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;

    iget-object v0, v0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    add-int/lit8 v1, p1, 0x1

    aget-object v0, v0, v1

    if-eqz v0, :cond_2

    iget-object v0, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_parent:Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;

    iget-object v0, v0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    add-int/lit8 v1, p1, 0x1

    aget-object v0, v0, v1

    iget-object v0, v0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;->child:Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_parent:Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;

    iget-object v0, v0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    add-int/lit8 v1, p1, 0x1

    aget-object v0, v0, v1

    iget-object v0, v0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;->child:Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;

    iget v0, v0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_nrElements:I

    iget v1, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->MIN:I

    if-le v0, v1, :cond_2

    .line 427
    invoke-direct {p0, p1}, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->stealRight(I)V

    goto :goto_0

    .line 432
    :cond_2
    if-eqz p1, :cond_3

    .line 433
    invoke-direct {p0, p1}, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->mergeLeft(I)V

    goto :goto_0

    .line 438
    :cond_3
    invoke-direct {p0, p1}, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->mergeRight(I)V

    goto :goto_0
.end method

.method private split()Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 321
    new-instance v4, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;

    iget-object v5, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->this$0:Lorg/apache/poi/hdf/extractor/util/BTreeSet;

    iget-object v6, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_parent:Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;

    invoke-direct {v4, v5, v6}, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;-><init>(Lorg/apache/poi/hdf/extractor/util/BTreeSet;Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;)V

    .line 322
    .local v4, "rightSibling":Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;
    iget v5, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_nrElements:I

    div-int/lit8 v1, v5, 0x2

    .line 323
    .local v1, "index":I
    iget-object v5, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    add-int/lit8 v2, v1, 0x1

    .end local v1    # "index":I
    .local v2, "index":I
    aget-object v5, v5, v1

    iput-object v7, v5, Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;->element:Ljava/lang/Object;

    .line 325
    const/4 v0, 0x0

    .local v0, "i":I
    iget v3, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_nrElements:I

    .local v3, "nr":I
    move v1, v2

    .end local v2    # "index":I
    .restart local v1    # "index":I
    :goto_0
    if-le v1, v3, :cond_0

    .line 334
    iget v5, v4, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_nrElements:I

    add-int/lit8 v5, v5, -0x1

    iput v5, v4, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_nrElements:I

    .line 335
    return-object v4

    .line 326
    :cond_0
    iget-object v5, v4, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    iget-object v6, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    aget-object v6, v6, v1

    aput-object v6, v5, v0

    .line 327
    iget-object v5, v4, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    aget-object v5, v5, v0

    if-eqz v5, :cond_1

    iget-object v5, v4, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    aget-object v5, v5, v0

    iget-object v5, v5, Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;->child:Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;

    if-eqz v5, :cond_1

    .line 328
    iget-object v5, v4, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    aget-object v5, v5, v0

    iget-object v5, v5, Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;->child:Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;

    iput-object v4, v5, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_parent:Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;

    .line 329
    :cond_1
    iget-object v5, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    aput-object v7, v5, v1

    .line 330
    iget v5, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_nrElements:I

    add-int/lit8 v5, v5, -0x1

    iput v5, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_nrElements:I

    .line 331
    iget v5, v4, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_nrElements:I

    add-int/lit8 v5, v5, 0x1

    iput v5, v4, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_nrElements:I

    .line 325
    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private splitRoot(Ljava/lang/Object;Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;)V
    .locals 5
    .param p1, "splitNode"    # Ljava/lang/Object;
    .param p2, "left"    # Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;
    .param p3, "right"    # Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 343
    new-instance v0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;

    iget-object v1, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->this$0:Lorg/apache/poi/hdf/extractor/util/BTreeSet;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;-><init>(Lorg/apache/poi/hdf/extractor/util/BTreeSet;Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;)V

    .line 344
    .local v0, "newRoot":Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;
    iget-object v1, v0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    aget-object v1, v1, v4

    iput-object p1, v1, Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;->element:Ljava/lang/Object;

    .line 345
    iget-object v1, v0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    aget-object v1, v1, v4

    iput-object p2, v1, Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;->child:Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;

    .line 346
    iget-object v1, v0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    new-instance v2, Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    invoke-direct {v2}, Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;-><init>()V

    aput-object v2, v1, v3

    .line 347
    iget-object v1, v0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    aget-object v1, v1, v3

    iput-object p3, v1, Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;->child:Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;

    .line 348
    iput v3, v0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_nrElements:I

    .line 349
    iput-object v0, p3, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_parent:Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;

    iput-object v0, p2, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_parent:Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;

    .line 350
    iget-object v1, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->this$0:Lorg/apache/poi/hdf/extractor/util/BTreeSet;

    iput-object v0, v1, Lorg/apache/poi/hdf/extractor/util/BTreeSet;->root:Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;

    .line 351
    return-void
.end method

.method private stealLeft(I)V
    .locals 8
    .param p1, "parentIndex"    # I

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 472
    iget-object v2, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_parent:Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;

    .line 473
    .local v2, "p":Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;
    iget-object v3, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_parent:Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;

    iget-object v3, v3, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    add-int/lit8 v4, p1, -0x1

    aget-object v3, v3, v4

    iget-object v1, v3, Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;->child:Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;

    .line 475
    .local v1, "ls":Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;
    invoke-virtual {p0}, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->isLeaf()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 476
    iget-object v3, v2, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    add-int/lit8 v4, p1, -0x1

    aget-object v3, v3, v4

    iget-object v3, v3, Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;->element:Ljava/lang/Object;

    const/4 v4, 0x1

    invoke-direct {p0, v3, v4}, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->childToInsertAt(Ljava/lang/Object;Z)I

    move-result v0

    .line 477
    .local v0, "add":I
    iget-object v3, v2, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    add-int/lit8 v4, p1, -0x1

    aget-object v3, v3, v4

    iget-object v3, v3, Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;->element:Ljava/lang/Object;

    invoke-direct {p0, v3, v0}, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->insertNewElement(Ljava/lang/Object;I)V

    .line 478
    iget-object v3, v2, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    add-int/lit8 v4, p1, -0x1

    aget-object v3, v3, v4

    iget-object v4, v1, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    iget v5, v1, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_nrElements:I

    add-int/lit8 v5, v5, -0x1

    aget-object v4, v4, v5

    iget-object v4, v4, Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;->element:Ljava/lang/Object;

    iput-object v4, v3, Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;->element:Ljava/lang/Object;

    .line 479
    iget-object v3, v1, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    iget v4, v1, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_nrElements:I

    add-int/lit8 v4, v4, -0x1

    aput-object v7, v3, v4

    .line 480
    iget v3, v1, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_nrElements:I

    add-int/lit8 v3, v3, -0x1

    iput v3, v1, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_nrElements:I

    .line 493
    .end local v0    # "add":I
    :goto_0
    return-void

    .line 484
    :cond_0
    iget-object v3, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    aget-object v3, v3, v6

    iget-object v4, v2, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    add-int/lit8 v5, p1, -0x1

    aget-object v4, v4, v5

    iget-object v4, v4, Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;->element:Ljava/lang/Object;

    iput-object v4, v3, Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;->element:Ljava/lang/Object;

    .line 485
    iget-object v3, v2, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    add-int/lit8 v4, p1, -0x1

    aget-object v3, v3, v4

    iget-object v4, v1, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    iget v5, v1, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_nrElements:I

    add-int/lit8 v5, v5, -0x1

    aget-object v4, v4, v5

    iget-object v4, v4, Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;->element:Ljava/lang/Object;

    iput-object v4, v3, Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;->element:Ljava/lang/Object;

    .line 486
    iget-object v3, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    aget-object v3, v3, v6

    iget-object v4, v1, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    iget v5, v1, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_nrElements:I

    aget-object v4, v4, v5

    iget-object v4, v4, Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;->child:Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;

    iput-object v4, v3, Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;->child:Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;

    .line 487
    iget-object v3, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    aget-object v3, v3, v6

    iget-object v3, v3, Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;->child:Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;

    iput-object p0, v3, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_parent:Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;

    .line 488
    iget-object v3, v1, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    iget v4, v1, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_nrElements:I

    aput-object v7, v3, v4

    .line 489
    iget-object v3, v1, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    iget v4, v1, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_nrElements:I

    add-int/lit8 v4, v4, -0x1

    aget-object v3, v3, v4

    iput-object v7, v3, Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;->element:Ljava/lang/Object;

    .line 490
    iget v3, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_nrElements:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_nrElements:I

    .line 491
    iget v3, v1, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_nrElements:I

    add-int/lit8 v3, v3, -0x1

    iput v3, v1, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_nrElements:I

    goto :goto_0
.end method

.method private stealRight(I)V
    .locals 8
    .param p1, "parentIndex"    # I

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 501
    iget-object v1, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_parent:Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;

    .line 502
    .local v1, "p":Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;
    iget-object v3, v1, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    add-int/lit8 v4, p1, 0x1

    aget-object v3, v3, v4

    iget-object v2, v3, Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;->child:Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;

    .line 504
    .local v2, "rs":Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;
    invoke-virtual {p0}, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->isLeaf()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 505
    iget-object v3, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    iget v4, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_nrElements:I

    new-instance v5, Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    invoke-direct {v5}, Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;-><init>()V

    aput-object v5, v3, v4

    .line 506
    iget-object v3, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    iget v4, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_nrElements:I

    aget-object v3, v3, v4

    iget-object v4, v1, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    aget-object v4, v4, p1

    iget-object v4, v4, Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;->element:Ljava/lang/Object;

    iput-object v4, v3, Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;->element:Ljava/lang/Object;

    .line 507
    iget-object v3, v1, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    aget-object v3, v3, p1

    iget-object v4, v2, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    aget-object v4, v4, v6

    iget-object v4, v4, Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;->element:Ljava/lang/Object;

    iput-object v4, v3, Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;->element:Ljava/lang/Object;

    .line 508
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v3, v2, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_nrElements:I

    if-lt v0, v3, :cond_0

    .line 509
    iget-object v3, v2, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    iget v4, v2, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_nrElements:I

    add-int/lit8 v4, v4, -0x1

    aput-object v7, v3, v4

    .line 510
    iget v3, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_nrElements:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_nrElements:I

    .line 511
    iget v3, v2, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_nrElements:I

    add-int/lit8 v3, v3, -0x1

    iput v3, v2, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_nrElements:I

    .line 526
    :goto_1
    return-void

    .line 508
    :cond_0
    iget-object v3, v2, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    iget-object v4, v2, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    add-int/lit8 v5, v0, 0x1

    aget-object v4, v4, v5

    aput-object v4, v3, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 515
    .end local v0    # "i":I
    :cond_1
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_2
    iget v3, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_nrElements:I

    if-le v0, v3, :cond_2

    .line 516
    iget-object v3, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    iget v4, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_nrElements:I

    aget-object v3, v3, v4

    iget-object v4, v1, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    aget-object v4, v4, p1

    iget-object v4, v4, Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;->element:Ljava/lang/Object;

    iput-object v4, v3, Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;->element:Ljava/lang/Object;

    .line 517
    iget-object v3, v1, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    aget-object v3, v3, p1

    iget-object v4, v2, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    aget-object v4, v4, v6

    iget-object v4, v4, Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;->element:Ljava/lang/Object;

    iput-object v4, v3, Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;->element:Ljava/lang/Object;

    .line 518
    iget-object v3, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    iget v4, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_nrElements:I

    add-int/lit8 v4, v4, 0x1

    new-instance v5, Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    invoke-direct {v5}, Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;-><init>()V

    aput-object v5, v3, v4

    .line 519
    iget-object v3, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    iget v4, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_nrElements:I

    add-int/lit8 v4, v4, 0x1

    aget-object v3, v3, v4

    iget-object v4, v2, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    aget-object v4, v4, v6

    iget-object v4, v4, Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;->child:Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;

    iput-object v4, v3, Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;->child:Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;

    .line 520
    iget-object v3, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    iget v4, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_nrElements:I

    add-int/lit8 v4, v4, 0x1

    aget-object v3, v3, v4

    iget-object v3, v3, Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;->child:Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;

    iput-object p0, v3, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_parent:Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;

    .line 521
    const/4 v0, 0x0

    :goto_3
    iget v3, v2, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_nrElements:I

    if-le v0, v3, :cond_3

    .line 522
    iget-object v3, v2, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    iget v4, v2, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_nrElements:I

    aput-object v7, v3, v4

    .line 523
    iget v3, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_nrElements:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_nrElements:I

    .line 524
    iget v3, v2, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_nrElements:I

    add-int/lit8 v3, v3, -0x1

    iput v3, v2, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_nrElements:I

    goto :goto_1

    .line 515
    :cond_2
    iget-object v3, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    iget-object v4, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    add-int/lit8 v5, v0, 0x1

    aget-object v4, v4, v5

    aput-object v4, v3, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 521
    :cond_3
    iget-object v3, v2, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    iget-object v4, v2, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    add-int/lit8 v5, v0, 0x1

    aget-object v4, v4, v5

    aput-object v4, v3, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_3
.end method

.method private switchWithSuccessor(Ljava/lang/Object;)V
    .locals 6
    .param p1, "x"    # Ljava/lang/Object;

    .prologue
    const/4 v5, 0x0

    .line 459
    invoke-direct {p0, p1, v5}, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->childToInsertAt(Ljava/lang/Object;Z)I

    move-result v0

    .line 460
    .local v0, "index":I
    iget-object v3, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    add-int/lit8 v4, v0, 0x1

    aget-object v3, v3, v4

    iget-object v2, v3, Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;->child:Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;

    .line 461
    .local v2, "temp":Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;
    :goto_0
    iget-object v3, v2, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    aget-object v3, v3, v5

    if-eqz v3, :cond_0

    iget-object v3, v2, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    aget-object v3, v3, v5

    iget-object v3, v3, Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;->child:Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;

    if-nez v3, :cond_1

    .line 462
    :cond_0
    iget-object v3, v2, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    aget-object v3, v3, v5

    iget-object v1, v3, Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;->element:Ljava/lang/Object;

    .line 463
    .local v1, "successor":Ljava/lang/Object;
    iget-object v3, v2, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    aget-object v3, v3, v5

    iget-object v4, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    aget-object v4, v4, v0

    iget-object v4, v4, Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;->element:Ljava/lang/Object;

    iput-object v4, v3, Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;->element:Ljava/lang/Object;

    .line 464
    iget-object v3, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    aget-object v3, v3, v0

    iput-object v1, v3, Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;->element:Ljava/lang/Object;

    .line 465
    return-void

    .line 461
    .end local v1    # "successor":Ljava/lang/Object;
    :cond_1
    iget-object v3, v2, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    aget-object v3, v3, v5

    iget-object v2, v3, Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;->child:Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;

    goto :goto_0
.end method


# virtual methods
.method delete(Ljava/lang/Object;I)Z
    .locals 7
    .param p1, "x"    # Ljava/lang/Object;
    .param p2, "parentIndex"    # I

    .prologue
    const/4 v4, 0x0

    const/4 v6, -0x1

    const/4 v3, 0x1

    .line 275
    invoke-direct {p0, p1, v3}, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->childToInsertAt(Ljava/lang/Object;Z)I

    move-result v0

    .line 276
    .local v0, "i":I
    move v1, p2

    .line 277
    .local v1, "priorParentIndex":I
    move-object v2, p0

    .line 278
    .local v2, "temp":Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;
    if-eq v0, v6, :cond_3

    .line 280
    :cond_0
    iget-object v5, v2, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    aget-object v5, v5, v0

    if-eqz v5, :cond_1

    iget-object v5, v2, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    aget-object v5, v5, v0

    iget-object v5, v5, Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;->child:Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;

    if-nez v5, :cond_2

    :cond_1
    move v3, v4

    .line 306
    :goto_0
    return v3

    .line 281
    :cond_2
    iget-object v5, v2, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    aget-object v5, v5, v0

    iget-object v2, v5, Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;->child:Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;

    .line 282
    move v1, p2

    .line 283
    move p2, v0

    .line 284
    invoke-direct {v2, p1, v3}, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->childToInsertAt(Ljava/lang/Object;Z)I

    move-result v0

    .line 285
    if-ne v0, v6, :cond_0

    .line 288
    :cond_3
    invoke-virtual {v2}, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->isLeaf()Z

    move-result v5

    if-eqz v5, :cond_5

    .line 289
    iget v4, v2, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_nrElements:I

    iget v5, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->MIN:I

    if-le v4, v5, :cond_4

    .line 290
    invoke-direct {v2, p1}, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->deleteElement(Ljava/lang/Object;)V

    .line 291
    iget-object v4, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->this$0:Lorg/apache/poi/hdf/extractor/util/BTreeSet;

    iget v5, v4, Lorg/apache/poi/hdf/extractor/util/BTreeSet;->size:I

    add-int/lit8 v5, v5, -0x1

    iput v5, v4, Lorg/apache/poi/hdf/extractor/util/BTreeSet;->size:I

    goto :goto_0

    .line 296
    :cond_4
    invoke-direct {v2, p2}, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->prepareForDeletion(I)V

    .line 297
    invoke-direct {v2, p1}, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->deleteElement(Ljava/lang/Object;)V

    .line 298
    iget-object v4, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->this$0:Lorg/apache/poi/hdf/extractor/util/BTreeSet;

    iget v5, v4, Lorg/apache/poi/hdf/extractor/util/BTreeSet;->size:I

    add-int/lit8 v5, v5, -0x1

    iput v5, v4, Lorg/apache/poi/hdf/extractor/util/BTreeSet;->size:I

    .line 299
    invoke-direct {v2, v1}, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->fixAfterDeletion(I)V

    goto :goto_0

    .line 304
    :cond_5
    invoke-direct {v2, p1}, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->switchWithSuccessor(Ljava/lang/Object;)V

    .line 305
    invoke-direct {v2, p1, v4}, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->childToInsertAt(Ljava/lang/Object;Z)I

    move-result v3

    add-int/lit8 p2, v3, 0x1

    .line 306
    iget-object v3, v2, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    aget-object v3, v3, p2

    iget-object v3, v3, Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;->child:Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;

    invoke-virtual {v3, p1, p2}, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->delete(Ljava/lang/Object;I)Z

    move-result v3

    goto :goto_0
.end method

.method includes(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "x"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    .line 268
    invoke-direct {p0, p1, v1}, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->childToInsertAt(Ljava/lang/Object;Z)I

    move-result v0

    .line 269
    .local v0, "index":I
    const/4 v2, -0x1

    if-ne v0, v2, :cond_0

    .line 271
    :goto_0
    return v1

    .line 270
    :cond_0
    iget-object v1, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    aget-object v1, v1, v0

    if-eqz v1, :cond_1

    iget-object v1, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    aget-object v1, v1, v0

    iget-object v1, v1, Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;->child:Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;

    if-nez v1, :cond_2

    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 271
    :cond_2
    iget-object v1, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    aget-object v1, v1, v0

    iget-object v1, v1, Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;->child:Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;

    invoke-virtual {v1, p1}, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->includes(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0
.end method

.method insert(Ljava/lang/Object;I)Z
    .locals 7
    .param p1, "x"    # Ljava/lang/Object;
    .param p2, "parentIndex"    # I

    .prologue
    const/4 v6, -0x1

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 230
    invoke-direct {p0}, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->isFull()Z

    move-result v5

    if-eqz v5, :cond_4

    .line 231
    iget-object v5, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    iget v6, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_nrElements:I

    div-int/lit8 v6, v6, 0x2

    aget-object v5, v5, v6

    iget-object v2, v5, Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;->element:Ljava/lang/Object;

    .line 232
    .local v2, "splitNode":Ljava/lang/Object;
    invoke-direct {p0}, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->split()Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;

    move-result-object v1

    .line 234
    .local v1, "rightSibling":Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;
    invoke-direct {p0}, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->isRoot()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 235
    invoke-direct {p0, v2, p0, v1}, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->splitRoot(Ljava/lang/Object;Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;)V

    .line 237
    iget-object v5, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->this$0:Lorg/apache/poi/hdf/extractor/util/BTreeSet;

    iget-object v6, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->this$0:Lorg/apache/poi/hdf/extractor/util/BTreeSet;

    iget-object v6, v6, Lorg/apache/poi/hdf/extractor/util/BTreeSet;->root:Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;

    iget-object v6, v6, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    aget-object v6, v6, v3

    iget-object v6, v6, Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;->element:Ljava/lang/Object;

    invoke-virtual {v5, p1, v6}, Lorg/apache/poi/hdf/extractor/util/BTreeSet;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v5

    if-gez v5, :cond_1

    invoke-virtual {p0, p1, v3}, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->insert(Ljava/lang/Object;I)Z

    .line 264
    .end local v1    # "rightSibling":Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;
    .end local v2    # "splitNode":Ljava/lang/Object;
    :cond_0
    :goto_0
    return v3

    .line 238
    .restart local v1    # "rightSibling":Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;
    .restart local v2    # "splitNode":Ljava/lang/Object;
    :cond_1
    invoke-virtual {v1, p1, v4}, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->insert(Ljava/lang/Object;I)Z

    goto :goto_0

    .line 242
    :cond_2
    iget-object v3, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_parent:Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;

    invoke-direct {v3, v2, p0, v1, p2}, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->insertSplitNode(Ljava/lang/Object;Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;I)V

    .line 243
    iget-object v3, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->this$0:Lorg/apache/poi/hdf/extractor/util/BTreeSet;

    iget-object v4, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_parent:Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;

    iget-object v4, v4, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    aget-object v4, v4, p2

    iget-object v4, v4, Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;->element:Ljava/lang/Object;

    invoke-virtual {v3, p1, v4}, Lorg/apache/poi/hdf/extractor/util/BTreeSet;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v3

    if-gez v3, :cond_3

    .line 244
    invoke-virtual {p0, p1, p2}, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->insert(Ljava/lang/Object;I)Z

    move-result v3

    goto :goto_0

    .line 246
    :cond_3
    add-int/lit8 v3, p2, 0x1

    invoke-virtual {v1, p1, v3}, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->insert(Ljava/lang/Object;I)Z

    move-result v3

    goto :goto_0

    .line 250
    .end local v1    # "rightSibling":Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;
    .end local v2    # "splitNode":Ljava/lang/Object;
    :cond_4
    invoke-virtual {p0}, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->isLeaf()Z

    move-result v5

    if-eqz v5, :cond_5

    .line 251
    invoke-direct {p0, p1, v4}, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->childToInsertAt(Ljava/lang/Object;Z)I

    move-result v0

    .line 252
    .local v0, "insertAt":I
    if-eq v0, v6, :cond_0

    .line 255
    invoke-direct {p0, p1, v0}, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->insertNewElement(Ljava/lang/Object;I)V

    .line 256
    iget-object v3, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->this$0:Lorg/apache/poi/hdf/extractor/util/BTreeSet;

    iget v5, v3, Lorg/apache/poi/hdf/extractor/util/BTreeSet;->size:I

    add-int/lit8 v5, v5, 0x1

    iput v5, v3, Lorg/apache/poi/hdf/extractor/util/BTreeSet;->size:I

    move v3, v4

    .line 257
    goto :goto_0

    .line 261
    .end local v0    # "insertAt":I
    :cond_5
    invoke-direct {p0, p1, v4}, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->childToInsertAt(Ljava/lang/Object;Z)I

    move-result v0

    .line 262
    .restart local v0    # "insertAt":I
    if-eq v0, v6, :cond_0

    iget-object v3, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    aget-object v3, v3, v0

    iget-object v3, v3, Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;->child:Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;

    invoke-virtual {v3, p1, v0}, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->insert(Ljava/lang/Object;I)Z

    move-result v3

    goto :goto_0
.end method

.method isLeaf()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 312
    iget-object v1, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    aget-object v1, v1, v0

    iget-object v1, v1, Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;->child:Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method
