.class public final Lorg/apache/poi/hdf/extractor/TC;
.super Ljava/lang/Object;
.source "TC.java"


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field _brcBottom:[S

.field _brcLeft:[S

.field _brcRight:[S

.field _brcTop:[S

.field _fBackward:Z

.field _fFirstMerged:Z

.field _fMerged:Z

.field _fRotateFont:Z

.field _fVertMerge:Z

.field _fVertRestart:Z

.field _fVertical:Z

.field _vertAlign:S


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x2

    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    new-array v0, v1, [S

    iput-object v0, p0, Lorg/apache/poi/hdf/extractor/TC;->_brcTop:[S

    .line 38
    new-array v0, v1, [S

    iput-object v0, p0, Lorg/apache/poi/hdf/extractor/TC;->_brcLeft:[S

    .line 39
    new-array v0, v1, [S

    iput-object v0, p0, Lorg/apache/poi/hdf/extractor/TC;->_brcBottom:[S

    .line 40
    new-array v0, v1, [S

    iput-object v0, p0, Lorg/apache/poi/hdf/extractor/TC;->_brcRight:[S

    .line 44
    return-void
.end method

.method static convertBytesToTC([BI)Lorg/apache/poi/hdf/extractor/TC;
    .locals 6
    .param p0, "array"    # [B
    .param p1, "offset"    # I

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 47
    new-instance v1, Lorg/apache/poi/hdf/extractor/TC;

    invoke-direct {v1}, Lorg/apache/poi/hdf/extractor/TC;-><init>()V

    .line 48
    .local v1, "tc":Lorg/apache/poi/hdf/extractor/TC;
    invoke-static {p0, p1}, Lorg/apache/poi/hdf/extractor/Utils;->convertBytesToShort([BI)S

    move-result v0

    .line 49
    .local v0, "rgf":I
    and-int/lit8 v2, v0, 0x1

    if-lez v2, :cond_0

    move v2, v3

    :goto_0
    iput-boolean v2, v1, Lorg/apache/poi/hdf/extractor/TC;->_fFirstMerged:Z

    .line 50
    and-int/lit8 v2, v0, 0x2

    if-lez v2, :cond_1

    move v2, v3

    :goto_1
    iput-boolean v2, v1, Lorg/apache/poi/hdf/extractor/TC;->_fMerged:Z

    .line 51
    and-int/lit8 v2, v0, 0x4

    if-lez v2, :cond_2

    move v2, v3

    :goto_2
    iput-boolean v2, v1, Lorg/apache/poi/hdf/extractor/TC;->_fVertical:Z

    .line 52
    and-int/lit8 v2, v0, 0x8

    if-lez v2, :cond_3

    move v2, v3

    :goto_3
    iput-boolean v2, v1, Lorg/apache/poi/hdf/extractor/TC;->_fBackward:Z

    .line 53
    and-int/lit8 v2, v0, 0x10

    if-lez v2, :cond_4

    move v2, v3

    :goto_4
    iput-boolean v2, v1, Lorg/apache/poi/hdf/extractor/TC;->_fRotateFont:Z

    .line 54
    and-int/lit8 v2, v0, 0x20

    if-lez v2, :cond_5

    move v2, v3

    :goto_5
    iput-boolean v2, v1, Lorg/apache/poi/hdf/extractor/TC;->_fVertMerge:Z

    .line 55
    and-int/lit8 v2, v0, 0x40

    if-lez v2, :cond_6

    move v2, v3

    :goto_6
    iput-boolean v2, v1, Lorg/apache/poi/hdf/extractor/TC;->_fVertRestart:Z

    .line 56
    and-int/lit16 v2, v0, 0x180

    shr-int/lit8 v2, v2, 0x7

    int-to-short v2, v2

    iput-short v2, v1, Lorg/apache/poi/hdf/extractor/TC;->_vertAlign:S

    .line 58
    iget-object v2, v1, Lorg/apache/poi/hdf/extractor/TC;->_brcTop:[S

    add-int/lit8 v5, p1, 0x4

    invoke-static {p0, v5}, Lorg/apache/poi/hdf/extractor/Utils;->convertBytesToShort([BI)S

    move-result v5

    aput-short v5, v2, v4

    .line 59
    iget-object v2, v1, Lorg/apache/poi/hdf/extractor/TC;->_brcTop:[S

    add-int/lit8 v5, p1, 0x6

    invoke-static {p0, v5}, Lorg/apache/poi/hdf/extractor/Utils;->convertBytesToShort([BI)S

    move-result v5

    aput-short v5, v2, v3

    .line 61
    iget-object v2, v1, Lorg/apache/poi/hdf/extractor/TC;->_brcLeft:[S

    add-int/lit8 v5, p1, 0x8

    invoke-static {p0, v5}, Lorg/apache/poi/hdf/extractor/Utils;->convertBytesToShort([BI)S

    move-result v5

    aput-short v5, v2, v4

    .line 62
    iget-object v2, v1, Lorg/apache/poi/hdf/extractor/TC;->_brcLeft:[S

    add-int/lit8 v5, p1, 0xa

    invoke-static {p0, v5}, Lorg/apache/poi/hdf/extractor/Utils;->convertBytesToShort([BI)S

    move-result v5

    aput-short v5, v2, v3

    .line 64
    iget-object v2, v1, Lorg/apache/poi/hdf/extractor/TC;->_brcBottom:[S

    add-int/lit8 v5, p1, 0xc

    invoke-static {p0, v5}, Lorg/apache/poi/hdf/extractor/Utils;->convertBytesToShort([BI)S

    move-result v5

    aput-short v5, v2, v4

    .line 65
    iget-object v2, v1, Lorg/apache/poi/hdf/extractor/TC;->_brcBottom:[S

    add-int/lit8 v5, p1, 0xe

    invoke-static {p0, v5}, Lorg/apache/poi/hdf/extractor/Utils;->convertBytesToShort([BI)S

    move-result v5

    aput-short v5, v2, v3

    .line 67
    iget-object v2, v1, Lorg/apache/poi/hdf/extractor/TC;->_brcRight:[S

    add-int/lit8 v5, p1, 0x10

    invoke-static {p0, v5}, Lorg/apache/poi/hdf/extractor/Utils;->convertBytesToShort([BI)S

    move-result v5

    aput-short v5, v2, v4

    .line 68
    iget-object v2, v1, Lorg/apache/poi/hdf/extractor/TC;->_brcRight:[S

    add-int/lit8 v4, p1, 0x12

    invoke-static {p0, v4}, Lorg/apache/poi/hdf/extractor/Utils;->convertBytesToShort([BI)S

    move-result v4

    aput-short v4, v2, v3

    .line 70
    return-object v1

    :cond_0
    move v2, v4

    .line 49
    goto/16 :goto_0

    :cond_1
    move v2, v4

    .line 50
    goto/16 :goto_1

    :cond_2
    move v2, v4

    .line 51
    goto :goto_2

    :cond_3
    move v2, v4

    .line 52
    goto :goto_3

    :cond_4
    move v2, v4

    .line 53
    goto :goto_4

    :cond_5
    move v2, v4

    .line 54
    goto :goto_5

    :cond_6
    move v2, v4

    .line 55
    goto :goto_6
.end method
