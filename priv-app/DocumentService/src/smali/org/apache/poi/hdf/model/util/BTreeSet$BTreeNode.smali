.class public Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;
.super Ljava/lang/Object;
.source "BTreeSet.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/poi/hdf/model/util/BTreeSet;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "BTreeNode"
.end annotation


# instance fields
.field private final MIN:I

.field public entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

.field private nrElements:I

.field public parent:Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;

.field final synthetic this$0:Lorg/apache/poi/hdf/model/util/BTreeSet;


# direct methods
.method constructor <init>(Lorg/apache/poi/hdf/model/util/BTreeSet;Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;)V
    .locals 3
    .param p2, "parent"    # Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;

    .prologue
    const/4 v2, 0x0

    .line 306
    iput-object p1, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->this$0:Lorg/apache/poi/hdf/model/util/BTreeSet;

    .line 305
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 302
    iput v2, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->nrElements:I

    .line 303
    # getter for: Lorg/apache/poi/hdf/model/util/BTreeSet;->order:I
    invoke-static {p1}, Lorg/apache/poi/hdf/model/util/BTreeSet;->access$0(Lorg/apache/poi/hdf/model/util/BTreeSet;)I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    div-int/lit8 v0, v0, 0x2

    iput v0, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->MIN:I

    .line 307
    iput-object p2, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->parent:Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;

    .line 308
    # getter for: Lorg/apache/poi/hdf/model/util/BTreeSet;->order:I
    invoke-static {p1}, Lorg/apache/poi/hdf/model/util/BTreeSet;->access$0(Lorg/apache/poi/hdf/model/util/BTreeSet;)I

    move-result v0

    new-array v0, v0, [Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    iput-object v0, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    .line 309
    iget-object v0, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    new-instance v1, Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    invoke-direct {v1}, Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;-><init>()V

    aput-object v1, v0, v2

    .line 310
    return-void
.end method

.method static synthetic access$0(Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;)Z
    .locals 1

    .prologue
    .line 409
    invoke-direct {p0}, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->isLeaf()Z

    move-result v0

    return v0
.end method

.method static synthetic access$1(Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;)I
    .locals 1

    .prologue
    .line 302
    iget v0, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->nrElements:I

    return v0
.end method

.method private childToInsertAt(Ljava/lang/Object;Z)I
    .locals 5
    .param p1, "x"    # Ljava/lang/Object;
    .param p2, "position"    # Z

    .prologue
    .line 487
    iget v3, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->nrElements:I

    div-int/lit8 v1, v3, 0x2

    .line 489
    .local v1, "index":I
    iget-object v3, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    aget-object v3, v3, v1

    if-eqz v3, :cond_0

    iget-object v3, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    aget-object v3, v3, v1

    iget-object v3, v3, Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;->element:Ljava/lang/Object;

    if-nez v3, :cond_2

    :cond_0
    move v0, v1

    .line 508
    :cond_1
    :goto_0
    return v0

    .line 491
    :cond_2
    const/4 v2, 0x0

    .local v2, "lo":I
    iget v3, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->nrElements:I

    add-int/lit8 v0, v3, -0x1

    .line 492
    .local v0, "hi":I
    :goto_1
    if-le v2, v0, :cond_3

    .line 506
    add-int/lit8 v0, v0, 0x1

    .line 507
    iget-object v3, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    aget-object v3, v3, v0

    if-eqz v3, :cond_1

    iget-object v3, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    aget-object v3, v3, v0

    iget-object v3, v3, Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;->element:Ljava/lang/Object;

    if-eqz v3, :cond_1

    .line 508
    if-eqz p2, :cond_1

    iget-object v3, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->this$0:Lorg/apache/poi/hdf/model/util/BTreeSet;

    iget-object v4, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    aget-object v4, v4, v0

    iget-object v4, v4, Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;->element:Ljava/lang/Object;

    invoke-virtual {v3, p1, v4}, Lorg/apache/poi/hdf/model/util/BTreeSet;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v3

    if-nez v3, :cond_1

    const/4 v0, -0x1

    goto :goto_0

    .line 494
    :cond_3
    iget-object v3, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->this$0:Lorg/apache/poi/hdf/model/util/BTreeSet;

    iget-object v4, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    aget-object v4, v4, v1

    iget-object v4, v4, Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;->element:Ljava/lang/Object;

    invoke-virtual {v3, p1, v4}, Lorg/apache/poi/hdf/model/util/BTreeSet;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v3

    if-lez v3, :cond_4

    .line 496
    add-int/lit8 v2, v1, 0x1

    .line 497
    add-int v3, v0, v2

    div-int/lit8 v1, v3, 0x2

    .line 498
    goto :goto_1

    .line 501
    :cond_4
    add-int/lit8 v0, v1, -0x1

    .line 502
    add-int v3, v0, v2

    div-int/lit8 v1, v3, 0x2

    goto :goto_1
.end method

.method private deleteElement(Ljava/lang/Object;)V
    .locals 4
    .param p1, "x"    # Ljava/lang/Object;

    .prologue
    .line 514
    const/4 v1, 0x0

    invoke-direct {p0, p1, v1}, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->childToInsertAt(Ljava/lang/Object;Z)I

    move-result v0

    .line 515
    .local v0, "index":I
    :goto_0
    iget v1, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->nrElements:I

    add-int/lit8 v1, v1, -0x1

    if-lt v0, v1, :cond_0

    .line 517
    iget v1, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->nrElements:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    iget-object v1, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    new-instance v2, Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    invoke-direct {v2}, Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;-><init>()V

    aput-object v2, v1, v0

    .line 520
    :goto_1
    iget v1, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->nrElements:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->nrElements:I

    .line 521
    return-void

    .line 515
    :cond_0
    iget-object v1, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    iget-object v2, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    add-int/lit8 v3, v0, 0x1

    aget-object v2, v2, v3

    aput-object v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 518
    :cond_1
    iget-object v1, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    const/4 v2, 0x0

    aput-object v2, v1, v0

    goto :goto_1
.end method

.method private fixAfterDeletion(I)V
    .locals 5
    .param p1, "parentIndex"    # I

    .prologue
    .line 553
    invoke-direct {p0}, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->isRoot()Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->parent:Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;

    invoke-direct {v3}, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->isRoot()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 569
    :cond_0
    :goto_0
    return-void

    .line 555
    :cond_1
    iget-object v3, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->parent:Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;

    iget v3, v3, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->nrElements:I

    iget v4, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->MIN:I

    if-ge v3, v4, :cond_0

    .line 557
    iget-object v1, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->parent:Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;

    .line 558
    .local v1, "temp":Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;
    invoke-direct {v1, p1}, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->prepareForDeletion(I)V

    .line 559
    iget-object v3, v1, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->parent:Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;

    if-eqz v3, :cond_0

    .line 560
    iget-object v3, v1, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->parent:Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;

    invoke-direct {v3}, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->isRoot()Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, v1, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->parent:Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;

    iget v3, v3, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->nrElements:I

    iget v4, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->MIN:I

    if-ge v3, v4, :cond_0

    .line 562
    iget-object v3, v1, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->parent:Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;

    iget-object v2, v3, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->parent:Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;

    .line 563
    .local v2, "x":Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;
    const/4 v0, 0x0

    .line 565
    .local v0, "i":I
    :goto_1
    iget-object v3, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    array-length v3, v3

    if-lt v0, v3, :cond_3

    .line 566
    :cond_2
    iget-object v3, v1, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->parent:Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;

    invoke-direct {v3, v0}, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->fixAfterDeletion(I)V

    goto :goto_0

    .line 565
    :cond_3
    iget-object v3, v2, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    aget-object v3, v3, v0

    iget-object v3, v3, Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;->child:Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;

    iget-object v4, v1, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->parent:Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;

    if-eq v3, v4, :cond_2

    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method private insertNewElement(Ljava/lang/Object;I)V
    .locals 4
    .param p1, "x"    # Ljava/lang/Object;
    .param p2, "insertAt"    # I

    .prologue
    .line 468
    iget v0, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->nrElements:I

    .local v0, "i":I
    :goto_0
    if-gt v0, p2, :cond_0

    .line 470
    iget-object v1, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    new-instance v2, Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    invoke-direct {v2}, Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;-><init>()V

    aput-object v2, v1, p2

    .line 471
    iget-object v1, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    aget-object v1, v1, p2

    iput-object p1, v1, Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;->element:Ljava/lang/Object;

    .line 473
    iget v1, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->nrElements:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->nrElements:I

    .line 474
    return-void

    .line 468
    :cond_0
    iget-object v1, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    iget-object v2, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    add-int/lit8 v3, v0, -0x1

    aget-object v2, v2, v3

    aput-object v2, v1, v0

    add-int/lit8 v0, v0, -0x1

    goto :goto_0
.end method

.method private insertSplitNode(Ljava/lang/Object;Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;I)V
    .locals 4
    .param p1, "splitNode"    # Ljava/lang/Object;
    .param p2, "left"    # Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;
    .param p3, "right"    # Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;
    .param p4, "insertAt"    # I

    .prologue
    .line 455
    iget v0, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->nrElements:I

    .local v0, "i":I
    :goto_0
    if-ge v0, p4, :cond_0

    .line 457
    iget-object v1, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    new-instance v2, Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    invoke-direct {v2}, Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;-><init>()V

    aput-object v2, v1, p4

    .line 458
    iget-object v1, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    aget-object v1, v1, p4

    iput-object p1, v1, Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;->element:Ljava/lang/Object;

    .line 459
    iget-object v1, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    aget-object v1, v1, p4

    iput-object p2, v1, Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;->child:Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;

    .line 460
    iget-object v1, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    add-int/lit8 v2, p4, 0x1

    aget-object v1, v1, v2

    iput-object p3, v1, Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;->child:Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;

    .line 462
    iget v1, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->nrElements:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->nrElements:I

    .line 463
    return-void

    .line 455
    :cond_0
    iget-object v1, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    add-int/lit8 v2, v0, 0x1

    iget-object v3, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    aget-object v3, v3, v0

    aput-object v3, v1, v2

    add-int/lit8 v0, v0, -0x1

    goto :goto_0
.end method

.method private isFull()Z
    .locals 2

    .prologue
    .line 407
    iget v0, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->nrElements:I

    iget-object v1, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->this$0:Lorg/apache/poi/hdf/model/util/BTreeSet;

    # getter for: Lorg/apache/poi/hdf/model/util/BTreeSet;->order:I
    invoke-static {v1}, Lorg/apache/poi/hdf/model/util/BTreeSet;->access$0(Lorg/apache/poi/hdf/model/util/BTreeSet;)I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isLeaf()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 409
    iget-object v1, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    aget-object v1, v1, v0

    iget-object v1, v1, Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;->child:Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method private isRoot()Z
    .locals 1

    .prologue
    .line 411
    iget-object v0, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->parent:Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private mergeLeft(I)V
    .locals 12
    .param p1, "parentIndex"    # I

    .prologue
    const/4 v11, 0x0

    const/4 v10, 0x0

    .line 659
    iget-object v4, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->parent:Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;

    .line 660
    .local v4, "p":Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;
    iget-object v7, v4, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    add-int/lit8 v8, p1, -0x1

    aget-object v7, v7, v8

    iget-object v2, v7, Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;->child:Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;

    .line 662
    .local v2, "ls":Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;
    invoke-direct {p0}, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->isLeaf()Z

    move-result v7

    if-eqz v7, :cond_6

    .line 664
    iget-object v7, v4, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    add-int/lit8 v8, p1, -0x1

    aget-object v7, v7, v8

    iget-object v7, v7, Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;->element:Ljava/lang/Object;

    const/4 v8, 0x1

    invoke-direct {p0, v7, v8}, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->childToInsertAt(Ljava/lang/Object;Z)I

    move-result v0

    .line 665
    .local v0, "add":I
    iget-object v7, v4, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    add-int/lit8 v8, p1, -0x1

    aget-object v7, v7, v8

    iget-object v7, v7, Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;->element:Ljava/lang/Object;

    invoke-direct {p0, v7, v0}, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->insertNewElement(Ljava/lang/Object;I)V

    .line 666
    iget-object v7, v4, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    add-int/lit8 v8, p1, -0x1

    aget-object v7, v7, v8

    iput-object v11, v7, Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;->element:Ljava/lang/Object;

    .line 668
    iget v7, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->nrElements:I

    add-int/lit8 v1, v7, -0x1

    .local v1, "i":I
    iget v3, v2, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->nrElements:I

    .local v3, "nr":I
    :goto_0
    if-gez v1, :cond_1

    .line 671
    iget v7, v2, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->nrElements:I

    add-int/lit8 v1, v7, -0x1

    :goto_1
    if-gez v1, :cond_2

    .line 677
    iget v7, v4, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->nrElements:I

    iget v8, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->MIN:I

    if-ne v7, v8, :cond_4

    iget-object v7, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->this$0:Lorg/apache/poi/hdf/model/util/BTreeSet;

    iget-object v7, v7, Lorg/apache/poi/hdf/model/util/BTreeSet;->root:Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;

    if-eq v4, v7, :cond_4

    .line 680
    add-int/lit8 v5, p1, -0x1

    .local v5, "x":I
    add-int/lit8 v6, p1, -0x2

    .local v6, "y":I
    :goto_2
    if-gez v6, :cond_3

    .line 682
    iget-object v7, v4, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    new-instance v8, Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    invoke-direct {v8}, Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;-><init>()V

    aput-object v8, v7, v10

    .line 683
    iget-object v7, v4, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    aget-object v7, v7, v10

    iput-object v2, v7, Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;->child:Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;

    .line 694
    :goto_3
    iget v7, v4, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->nrElements:I

    add-int/lit8 v7, v7, -0x1

    iput v7, v4, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->nrElements:I

    .line 696
    invoke-direct {v4}, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->isRoot()Z

    move-result v7

    if-eqz v7, :cond_0

    iget v7, v4, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->nrElements:I

    if-nez v7, :cond_0

    .line 698
    iget-object v7, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->this$0:Lorg/apache/poi/hdf/model/util/BTreeSet;

    iput-object p0, v7, Lorg/apache/poi/hdf/model/util/BTreeSet;->root:Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;

    .line 699
    iput-object v11, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->parent:Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;

    .line 744
    .end local v0    # "add":I
    .end local v1    # "i":I
    :cond_0
    :goto_4
    return-void

    .line 669
    .end local v5    # "x":I
    .end local v6    # "y":I
    .restart local v0    # "add":I
    .restart local v1    # "i":I
    :cond_1
    iget-object v7, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    add-int v8, v1, v3

    iget-object v9, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    aget-object v9, v9, v1

    aput-object v9, v7, v8

    .line 668
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    .line 673
    :cond_2
    iget-object v7, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    iget-object v8, v2, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    aget-object v8, v8, v1

    aput-object v8, v7, v1

    .line 674
    iget v7, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->nrElements:I

    add-int/lit8 v7, v7, 0x1

    iput v7, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->nrElements:I

    .line 671
    add-int/lit8 v1, v1, -0x1

    goto :goto_1

    .line 681
    .restart local v5    # "x":I
    .restart local v6    # "y":I
    :cond_3
    iget-object v7, v4, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    iget-object v8, v4, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    aget-object v8, v8, v6

    aput-object v8, v7, v5

    .line 680
    add-int/lit8 v5, v5, -0x1

    add-int/lit8 v6, v6, -0x1

    goto :goto_2

    .line 689
    .end local v5    # "x":I
    .end local v6    # "y":I
    :cond_4
    add-int/lit8 v5, p1, -0x1

    .restart local v5    # "x":I
    move v6, p1

    .restart local v6    # "y":I
    :goto_5
    iget v7, v4, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->nrElements:I

    if-le v6, v7, :cond_5

    .line 691
    iget-object v7, v4, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    iget v8, v4, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->nrElements:I

    aput-object v11, v7, v8

    goto :goto_3

    .line 690
    :cond_5
    iget-object v7, v4, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    iget-object v8, v4, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    aget-object v8, v8, v6

    aput-object v8, v7, v5

    .line 689
    add-int/lit8 v5, v5, 0x1

    add-int/lit8 v6, v6, 0x1

    goto :goto_5

    .line 705
    .end local v0    # "add":I
    .end local v1    # "i":I
    .end local v3    # "nr":I
    .end local v5    # "x":I
    .end local v6    # "y":I
    :cond_6
    iget-object v7, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    aget-object v7, v7, v10

    iget-object v8, v4, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    add-int/lit8 v9, p1, -0x1

    aget-object v8, v8, v9

    iget-object v8, v8, Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;->element:Ljava/lang/Object;

    iput-object v8, v7, Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;->element:Ljava/lang/Object;

    .line 706
    iget-object v7, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    aget-object v7, v7, v10

    iget-object v8, v2, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    iget v9, v2, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->nrElements:I

    aget-object v8, v8, v9

    iget-object v8, v8, Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;->child:Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;

    iput-object v8, v7, Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;->child:Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;

    .line 707
    iget v7, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->nrElements:I

    add-int/lit8 v7, v7, 0x1

    iput v7, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->nrElements:I

    .line 709
    iget v5, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->nrElements:I

    .restart local v5    # "x":I
    iget v3, v2, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->nrElements:I

    .restart local v3    # "nr":I
    :goto_6
    if-gez v5, :cond_7

    .line 712
    iget v7, v2, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->nrElements:I

    add-int/lit8 v5, v7, -0x1

    :goto_7
    if-gez v5, :cond_8

    .line 719
    iget v7, v4, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->nrElements:I

    iget v8, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->MIN:I

    if-ne v7, v8, :cond_a

    iget-object v7, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->this$0:Lorg/apache/poi/hdf/model/util/BTreeSet;

    iget-object v7, v7, Lorg/apache/poi/hdf/model/util/BTreeSet;->root:Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;

    if-eq v4, v7, :cond_a

    .line 721
    add-int/lit8 v5, p1, -0x1

    add-int/lit8 v6, p1, -0x2

    .restart local v6    # "y":I
    :goto_8
    if-gez v6, :cond_9

    .line 726
    iget-object v7, v4, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    new-instance v8, Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    invoke-direct {v8}, Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;-><init>()V

    aput-object v8, v7, v10

    .line 736
    :goto_9
    iget v7, v4, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->nrElements:I

    add-int/lit8 v7, v7, -0x1

    iput v7, v4, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->nrElements:I

    .line 738
    invoke-direct {v4}, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->isRoot()Z

    move-result v7

    if-eqz v7, :cond_0

    iget v7, v4, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->nrElements:I

    if-nez v7, :cond_0

    .line 740
    iget-object v7, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->this$0:Lorg/apache/poi/hdf/model/util/BTreeSet;

    iput-object p0, v7, Lorg/apache/poi/hdf/model/util/BTreeSet;->root:Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;

    .line 741
    iput-object v11, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->parent:Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;

    goto/16 :goto_4

    .line 710
    .end local v6    # "y":I
    :cond_7
    iget-object v7, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    add-int v8, v5, v3

    iget-object v9, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    aget-object v9, v9, v5

    aput-object v9, v7, v8

    .line 709
    add-int/lit8 v5, v5, -0x1

    goto :goto_6

    .line 714
    :cond_8
    iget-object v7, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    iget-object v8, v2, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    aget-object v8, v8, v5

    aput-object v8, v7, v5

    .line 715
    iget-object v7, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    aget-object v7, v7, v5

    iget-object v7, v7, Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;->child:Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;

    iput-object p0, v7, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->parent:Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;

    .line 716
    iget v7, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->nrElements:I

    add-int/lit8 v7, v7, 0x1

    iput v7, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->nrElements:I

    .line 712
    add-int/lit8 v5, v5, -0x1

    goto :goto_7

    .line 724
    .restart local v6    # "y":I
    :cond_9
    iget-object v7, v4, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    iget-object v8, v4, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    aget-object v8, v8, v6

    aput-object v8, v7, v5

    .line 721
    add-int/lit8 v5, v5, 0x1

    add-int/lit8 v6, v6, 0x1

    goto :goto_8

    .line 731
    .end local v6    # "y":I
    :cond_a
    add-int/lit8 v5, p1, -0x1

    move v6, p1

    .restart local v6    # "y":I
    :goto_a
    iget v7, v4, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->nrElements:I

    if-le v6, v7, :cond_b

    .line 733
    iget-object v7, v4, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    iget v8, v4, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->nrElements:I

    aput-object v11, v7, v8

    goto :goto_9

    .line 732
    :cond_b
    iget-object v7, v4, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    iget-object v8, v4, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    aget-object v8, v8, v6

    aput-object v8, v7, v5

    .line 731
    add-int/lit8 v5, v5, 0x1

    add-int/lit8 v6, v6, 0x1

    goto :goto_a
.end method

.method private mergeRight(I)V
    .locals 11
    .param p1, "parentIndex"    # I

    .prologue
    const/4 v10, 0x0

    const/4 v9, 0x0

    .line 757
    iget-object v2, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->parent:Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;

    .line 758
    .local v2, "p":Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;
    iget-object v6, v2, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    add-int/lit8 v7, p1, 0x1

    aget-object v6, v6, v7

    iget-object v3, v6, Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;->child:Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;

    .line 760
    .local v3, "rs":Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;
    invoke-direct {p0}, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->isLeaf()Z

    move-result v6

    if-eqz v6, :cond_5

    .line 762
    iget-object v6, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    iget v7, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->nrElements:I

    new-instance v8, Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    invoke-direct {v8}, Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;-><init>()V

    aput-object v8, v6, v7

    .line 763
    iget-object v6, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    iget v7, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->nrElements:I

    aget-object v6, v6, v7

    iget-object v7, v2, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    aget-object v7, v7, p1

    iget-object v7, v7, Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;->element:Ljava/lang/Object;

    iput-object v7, v6, Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;->element:Ljava/lang/Object;

    .line 764
    iget v6, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->nrElements:I

    add-int/lit8 v6, v6, 0x1

    iput v6, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->nrElements:I

    .line 765
    const/4 v0, 0x0

    .local v0, "i":I
    iget v1, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->nrElements:I

    .local v1, "nr":I
    :goto_0
    iget v6, v3, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->nrElements:I

    if-lt v0, v6, :cond_1

    .line 770
    iget-object v6, v2, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    aget-object v6, v6, p1

    iget-object v7, v2, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    add-int/lit8 v8, p1, 0x1

    aget-object v7, v7, v8

    iget-object v7, v7, Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;->element:Ljava/lang/Object;

    iput-object v7, v6, Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;->element:Ljava/lang/Object;

    .line 771
    iget v6, v2, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->nrElements:I

    iget v7, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->MIN:I

    if-ne v6, v7, :cond_3

    iget-object v6, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->this$0:Lorg/apache/poi/hdf/model/util/BTreeSet;

    iget-object v6, v6, Lorg/apache/poi/hdf/model/util/BTreeSet;->root:Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;

    if-eq v2, v6, :cond_3

    .line 773
    add-int/lit8 v4, p1, 0x1

    .local v4, "x":I
    move v5, p1

    .local v5, "y":I
    :goto_1
    if-gez v5, :cond_2

    .line 775
    iget-object v6, v2, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    new-instance v7, Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    invoke-direct {v7}, Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;-><init>()V

    aput-object v7, v6, v10

    .line 776
    iget-object v6, v2, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    aget-object v6, v6, v10

    iput-object v3, v6, Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;->child:Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;

    .line 786
    :goto_2
    iget v6, v2, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->nrElements:I

    add-int/lit8 v6, v6, -0x1

    iput v6, v2, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->nrElements:I

    .line 787
    invoke-direct {v2}, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->isRoot()Z

    move-result v6

    if-eqz v6, :cond_0

    iget v6, v2, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->nrElements:I

    if-nez v6, :cond_0

    .line 789
    iget-object v6, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->this$0:Lorg/apache/poi/hdf/model/util/BTreeSet;

    iput-object p0, v6, Lorg/apache/poi/hdf/model/util/BTreeSet;->root:Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;

    .line 790
    iput-object v9, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->parent:Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;

    .line 832
    .end local v0    # "i":I
    .end local v1    # "nr":I
    :cond_0
    :goto_3
    return-void

    .line 767
    .end local v4    # "x":I
    .end local v5    # "y":I
    .restart local v0    # "i":I
    .restart local v1    # "nr":I
    :cond_1
    iget-object v6, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    iget-object v7, v3, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    aget-object v7, v7, v0

    aput-object v7, v6, v1

    .line 768
    iget v6, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->nrElements:I

    add-int/lit8 v6, v6, 0x1

    iput v6, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->nrElements:I

    .line 765
    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 774
    .restart local v4    # "x":I
    .restart local v5    # "y":I
    :cond_2
    iget-object v6, v2, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    iget-object v7, v2, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    aget-object v7, v7, v5

    aput-object v7, v6, v4

    .line 773
    add-int/lit8 v4, v4, -0x1

    add-int/lit8 v5, v5, -0x1

    goto :goto_1

    .line 781
    .end local v4    # "x":I
    .end local v5    # "y":I
    :cond_3
    add-int/lit8 v4, p1, 0x1

    .restart local v4    # "x":I
    add-int/lit8 v5, p1, 0x2

    .restart local v5    # "y":I
    :goto_4
    iget v6, v2, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->nrElements:I

    if-le v5, v6, :cond_4

    .line 783
    iget-object v6, v2, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    iget v7, v2, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->nrElements:I

    aput-object v9, v6, v7

    goto :goto_2

    .line 782
    :cond_4
    iget-object v6, v2, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    iget-object v7, v2, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    aget-object v7, v7, v5

    aput-object v7, v6, v4

    .line 781
    add-int/lit8 v4, v4, 0x1

    add-int/lit8 v5, v5, 0x1

    goto :goto_4

    .line 797
    .end local v0    # "i":I
    .end local v1    # "nr":I
    .end local v4    # "x":I
    .end local v5    # "y":I
    :cond_5
    iget-object v6, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    iget v7, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->nrElements:I

    aget-object v6, v6, v7

    iget-object v7, v2, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    aget-object v7, v7, p1

    iget-object v7, v7, Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;->element:Ljava/lang/Object;

    iput-object v7, v6, Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;->element:Ljava/lang/Object;

    .line 798
    iget v6, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->nrElements:I

    add-int/lit8 v6, v6, 0x1

    iput v6, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->nrElements:I

    .line 800
    iget v6, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->nrElements:I

    add-int/lit8 v4, v6, 0x1

    .restart local v4    # "x":I
    const/4 v5, 0x0

    .restart local v5    # "y":I
    :goto_5
    iget v6, v3, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->nrElements:I

    if-le v5, v6, :cond_6

    .line 806
    iget v6, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->nrElements:I

    add-int/lit8 v6, v6, -0x1

    iput v6, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->nrElements:I

    .line 808
    iget-object v6, v2, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    add-int/lit8 p1, p1, 0x1

    aget-object v6, v6, p1

    iput-object p0, v6, Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;->child:Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;

    .line 810
    iget v6, v2, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->nrElements:I

    iget v7, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->MIN:I

    if-ne v6, v7, :cond_8

    iget-object v6, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->this$0:Lorg/apache/poi/hdf/model/util/BTreeSet;

    iget-object v6, v6, Lorg/apache/poi/hdf/model/util/BTreeSet;->root:Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;

    if-eq v2, v6, :cond_8

    .line 812
    add-int/lit8 v4, p1, -0x1

    add-int/lit8 v5, p1, -0x2

    :goto_6
    if-gez v5, :cond_7

    .line 814
    iget-object v6, v2, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    new-instance v7, Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    invoke-direct {v7}, Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;-><init>()V

    aput-object v7, v6, v10

    .line 824
    :goto_7
    iget v6, v2, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->nrElements:I

    add-int/lit8 v6, v6, -0x1

    iput v6, v2, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->nrElements:I

    .line 826
    invoke-direct {v2}, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->isRoot()Z

    move-result v6

    if-eqz v6, :cond_0

    iget v6, v2, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->nrElements:I

    if-nez v6, :cond_0

    .line 828
    iget-object v6, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->this$0:Lorg/apache/poi/hdf/model/util/BTreeSet;

    iput-object p0, v6, Lorg/apache/poi/hdf/model/util/BTreeSet;->root:Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;

    .line 829
    iput-object v9, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->parent:Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;

    goto/16 :goto_3

    .line 802
    :cond_6
    iget-object v6, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    iget-object v7, v3, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    aget-object v7, v7, v5

    aput-object v7, v6, v4

    .line 803
    iget-object v6, v3, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    aget-object v6, v6, v5

    iget-object v6, v6, Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;->child:Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;

    iput-object p0, v6, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->parent:Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;

    .line 804
    iget v6, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->nrElements:I

    add-int/lit8 v6, v6, 0x1

    iput v6, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->nrElements:I

    .line 800
    add-int/lit8 v4, v4, 0x1

    add-int/lit8 v5, v5, 0x1

    goto :goto_5

    .line 813
    :cond_7
    iget-object v6, v2, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    iget-object v7, v2, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    aget-object v7, v7, v5

    aput-object v7, v6, v4

    .line 812
    add-int/lit8 v4, v4, -0x1

    add-int/lit8 v5, v5, -0x1

    goto :goto_6

    .line 819
    :cond_8
    add-int/lit8 v4, p1, -0x1

    move v5, p1

    :goto_8
    iget v6, v2, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->nrElements:I

    if-le v5, v6, :cond_9

    .line 821
    iget-object v6, v2, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    iget v7, v2, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->nrElements:I

    aput-object v9, v6, v7

    goto :goto_7

    .line 820
    :cond_9
    iget-object v6, v2, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    iget-object v7, v2, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    aget-object v7, v7, v5

    aput-object v7, v6, v4

    .line 819
    add-int/lit8 v4, v4, 0x1

    add-int/lit8 v5, v5, 0x1

    goto :goto_8
.end method

.method private prepareForDeletion(I)V
    .locals 2
    .param p1, "parentIndex"    # I

    .prologue
    .line 525
    invoke-direct {p0}, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->isRoot()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 549
    :goto_0
    return-void

    .line 528
    :cond_0
    if-eqz p1, :cond_1

    iget-object v0, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->parent:Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;

    iget-object v0, v0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    add-int/lit8 v1, p1, -0x1

    aget-object v0, v0, v1

    iget-object v0, v0, Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;->child:Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;

    iget v0, v0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->nrElements:I

    iget v1, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->MIN:I

    if-le v0, v1, :cond_1

    .line 530
    invoke-direct {p0, p1}, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->stealLeft(I)V

    goto :goto_0

    .line 535
    :cond_1
    iget-object v0, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    array-length v0, v0

    if-ge p1, v0, :cond_2

    iget-object v0, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->parent:Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;

    iget-object v0, v0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    add-int/lit8 v1, p1, 0x1

    aget-object v0, v0, v1

    if-eqz v0, :cond_2

    iget-object v0, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->parent:Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;

    iget-object v0, v0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    add-int/lit8 v1, p1, 0x1

    aget-object v0, v0, v1

    iget-object v0, v0, Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;->child:Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->parent:Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;

    iget-object v0, v0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    add-int/lit8 v1, p1, 0x1

    aget-object v0, v0, v1

    iget-object v0, v0, Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;->child:Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;

    iget v0, v0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->nrElements:I

    iget v1, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->MIN:I

    if-le v0, v1, :cond_2

    .line 537
    invoke-direct {p0, p1}, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->stealRight(I)V

    goto :goto_0

    .line 542
    :cond_2
    if-eqz p1, :cond_3

    .line 543
    invoke-direct {p0, p1}, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->mergeLeft(I)V

    goto :goto_0

    .line 548
    :cond_3
    invoke-direct {p0, p1}, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->mergeRight(I)V

    goto :goto_0
.end method

.method private split()Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 419
    new-instance v4, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;

    iget-object v5, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->this$0:Lorg/apache/poi/hdf/model/util/BTreeSet;

    iget-object v6, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->parent:Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;

    invoke-direct {v4, v5, v6}, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;-><init>(Lorg/apache/poi/hdf/model/util/BTreeSet;Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;)V

    .line 420
    .local v4, "rightSibling":Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;
    iget v5, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->nrElements:I

    div-int/lit8 v1, v5, 0x2

    .line 421
    .local v1, "index":I
    iget-object v5, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    add-int/lit8 v2, v1, 0x1

    .end local v1    # "index":I
    .local v2, "index":I
    aget-object v5, v5, v1

    iput-object v7, v5, Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;->element:Ljava/lang/Object;

    .line 423
    const/4 v0, 0x0

    .local v0, "i":I
    iget v3, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->nrElements:I

    .local v3, "nr":I
    move v1, v2

    .end local v2    # "index":I
    .restart local v1    # "index":I
    :goto_0
    if-le v1, v3, :cond_0

    .line 433
    iget v5, v4, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->nrElements:I

    add-int/lit8 v5, v5, -0x1

    iput v5, v4, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->nrElements:I

    .line 434
    return-object v4

    .line 425
    :cond_0
    iget-object v5, v4, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    iget-object v6, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    aget-object v6, v6, v1

    aput-object v6, v5, v0

    .line 426
    iget-object v5, v4, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    aget-object v5, v5, v0

    if-eqz v5, :cond_1

    iget-object v5, v4, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    aget-object v5, v5, v0

    iget-object v5, v5, Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;->child:Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;

    if-eqz v5, :cond_1

    .line 427
    iget-object v5, v4, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    aget-object v5, v5, v0

    iget-object v5, v5, Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;->child:Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;

    iput-object v4, v5, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->parent:Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;

    .line 428
    :cond_1
    iget-object v5, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    aput-object v7, v5, v1

    .line 429
    iget v5, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->nrElements:I

    add-int/lit8 v5, v5, -0x1

    iput v5, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->nrElements:I

    .line 430
    iget v5, v4, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->nrElements:I

    add-int/lit8 v5, v5, 0x1

    iput v5, v4, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->nrElements:I

    .line 423
    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private splitRoot(Ljava/lang/Object;Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;)V
    .locals 5
    .param p1, "splitNode"    # Ljava/lang/Object;
    .param p2, "left"    # Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;
    .param p3, "right"    # Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 443
    new-instance v0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;

    iget-object v1, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->this$0:Lorg/apache/poi/hdf/model/util/BTreeSet;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;-><init>(Lorg/apache/poi/hdf/model/util/BTreeSet;Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;)V

    .line 444
    .local v0, "newRoot":Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;
    iget-object v1, v0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    aget-object v1, v1, v4

    iput-object p1, v1, Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;->element:Ljava/lang/Object;

    .line 445
    iget-object v1, v0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    aget-object v1, v1, v4

    iput-object p2, v1, Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;->child:Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;

    .line 446
    iget-object v1, v0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    new-instance v2, Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    invoke-direct {v2}, Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;-><init>()V

    aput-object v2, v1, v3

    .line 447
    iget-object v1, v0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    aget-object v1, v1, v3

    iput-object p3, v1, Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;->child:Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;

    .line 448
    iput v3, v0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->nrElements:I

    .line 449
    iput-object v0, p3, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->parent:Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;

    iput-object v0, p2, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->parent:Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;

    .line 450
    iget-object v1, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->this$0:Lorg/apache/poi/hdf/model/util/BTreeSet;

    iput-object v0, v1, Lorg/apache/poi/hdf/model/util/BTreeSet;->root:Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;

    .line 451
    return-void
.end method

.method private stealLeft(I)V
    .locals 8
    .param p1, "parentIndex"    # I

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 587
    iget-object v2, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->parent:Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;

    .line 588
    .local v2, "p":Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;
    iget-object v3, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->parent:Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;

    iget-object v3, v3, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    add-int/lit8 v4, p1, -0x1

    aget-object v3, v3, v4

    iget-object v1, v3, Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;->child:Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;

    .line 590
    .local v1, "ls":Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;
    invoke-direct {p0}, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->isLeaf()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 592
    iget-object v3, v2, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    add-int/lit8 v4, p1, -0x1

    aget-object v3, v3, v4

    iget-object v3, v3, Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;->element:Ljava/lang/Object;

    const/4 v4, 0x1

    invoke-direct {p0, v3, v4}, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->childToInsertAt(Ljava/lang/Object;Z)I

    move-result v0

    .line 593
    .local v0, "add":I
    iget-object v3, v2, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    add-int/lit8 v4, p1, -0x1

    aget-object v3, v3, v4

    iget-object v3, v3, Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;->element:Ljava/lang/Object;

    invoke-direct {p0, v3, v0}, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->insertNewElement(Ljava/lang/Object;I)V

    .line 594
    iget-object v3, v2, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    add-int/lit8 v4, p1, -0x1

    aget-object v3, v3, v4

    iget-object v4, v1, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    iget v5, v1, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->nrElements:I

    add-int/lit8 v5, v5, -0x1

    aget-object v4, v4, v5

    iget-object v4, v4, Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;->element:Ljava/lang/Object;

    iput-object v4, v3, Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;->element:Ljava/lang/Object;

    .line 595
    iget-object v3, v1, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    iget v4, v1, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->nrElements:I

    add-int/lit8 v4, v4, -0x1

    aput-object v7, v3, v4

    .line 596
    iget v3, v1, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->nrElements:I

    add-int/lit8 v3, v3, -0x1

    iput v3, v1, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->nrElements:I

    .line 610
    .end local v0    # "add":I
    :goto_0
    return-void

    .line 601
    :cond_0
    iget-object v3, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    aget-object v3, v3, v6

    iget-object v4, v2, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    add-int/lit8 v5, p1, -0x1

    aget-object v4, v4, v5

    iget-object v4, v4, Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;->element:Ljava/lang/Object;

    iput-object v4, v3, Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;->element:Ljava/lang/Object;

    .line 602
    iget-object v3, v2, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    add-int/lit8 v4, p1, -0x1

    aget-object v3, v3, v4

    iget-object v4, v1, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    iget v5, v1, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->nrElements:I

    add-int/lit8 v5, v5, -0x1

    aget-object v4, v4, v5

    iget-object v4, v4, Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;->element:Ljava/lang/Object;

    iput-object v4, v3, Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;->element:Ljava/lang/Object;

    .line 603
    iget-object v3, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    aget-object v3, v3, v6

    iget-object v4, v1, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    iget v5, v1, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->nrElements:I

    aget-object v4, v4, v5

    iget-object v4, v4, Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;->child:Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;

    iput-object v4, v3, Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;->child:Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;

    .line 604
    iget-object v3, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    aget-object v3, v3, v6

    iget-object v3, v3, Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;->child:Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;

    iput-object p0, v3, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->parent:Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;

    .line 605
    iget-object v3, v1, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    iget v4, v1, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->nrElements:I

    aput-object v7, v3, v4

    .line 606
    iget-object v3, v1, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    iget v4, v1, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->nrElements:I

    add-int/lit8 v4, v4, -0x1

    aget-object v3, v3, v4

    iput-object v7, v3, Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;->element:Ljava/lang/Object;

    .line 607
    iget v3, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->nrElements:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->nrElements:I

    .line 608
    iget v3, v1, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->nrElements:I

    add-int/lit8 v3, v3, -0x1

    iput v3, v1, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->nrElements:I

    goto :goto_0
.end method

.method private stealRight(I)V
    .locals 8
    .param p1, "parentIndex"    # I

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 619
    iget-object v1, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->parent:Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;

    .line 620
    .local v1, "p":Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;
    iget-object v3, v1, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    add-int/lit8 v4, p1, 0x1

    aget-object v3, v3, v4

    iget-object v2, v3, Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;->child:Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;

    .line 622
    .local v2, "rs":Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;
    invoke-direct {p0}, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->isLeaf()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 624
    iget-object v3, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    iget v4, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->nrElements:I

    new-instance v5, Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    invoke-direct {v5}, Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;-><init>()V

    aput-object v5, v3, v4

    .line 625
    iget-object v3, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    iget v4, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->nrElements:I

    aget-object v3, v3, v4

    iget-object v4, v1, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    aget-object v4, v4, p1

    iget-object v4, v4, Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;->element:Ljava/lang/Object;

    iput-object v4, v3, Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;->element:Ljava/lang/Object;

    .line 626
    iget-object v3, v1, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    aget-object v3, v3, p1

    iget-object v4, v2, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    aget-object v4, v4, v6

    iget-object v4, v4, Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;->element:Ljava/lang/Object;

    iput-object v4, v3, Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;->element:Ljava/lang/Object;

    .line 627
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v3, v2, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->nrElements:I

    if-lt v0, v3, :cond_0

    .line 628
    iget-object v3, v2, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    iget v4, v2, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->nrElements:I

    add-int/lit8 v4, v4, -0x1

    aput-object v7, v3, v4

    .line 629
    iget v3, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->nrElements:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->nrElements:I

    .line 630
    iget v3, v2, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->nrElements:I

    add-int/lit8 v3, v3, -0x1

    iput v3, v2, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->nrElements:I

    .line 646
    :goto_1
    return-void

    .line 627
    :cond_0
    iget-object v3, v2, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    iget-object v4, v2, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    add-int/lit8 v5, v0, 0x1

    aget-object v4, v4, v5

    aput-object v4, v3, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 635
    .end local v0    # "i":I
    :cond_1
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_2
    iget v3, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->nrElements:I

    if-le v0, v3, :cond_2

    .line 636
    iget-object v3, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    iget v4, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->nrElements:I

    aget-object v3, v3, v4

    iget-object v4, v1, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    aget-object v4, v4, p1

    iget-object v4, v4, Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;->element:Ljava/lang/Object;

    iput-object v4, v3, Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;->element:Ljava/lang/Object;

    .line 637
    iget-object v3, v1, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    aget-object v3, v3, p1

    iget-object v4, v2, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    aget-object v4, v4, v6

    iget-object v4, v4, Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;->element:Ljava/lang/Object;

    iput-object v4, v3, Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;->element:Ljava/lang/Object;

    .line 638
    iget-object v3, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    iget v4, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->nrElements:I

    add-int/lit8 v4, v4, 0x1

    new-instance v5, Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    invoke-direct {v5}, Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;-><init>()V

    aput-object v5, v3, v4

    .line 639
    iget-object v3, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    iget v4, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->nrElements:I

    add-int/lit8 v4, v4, 0x1

    aget-object v3, v3, v4

    iget-object v4, v2, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    aget-object v4, v4, v6

    iget-object v4, v4, Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;->child:Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;

    iput-object v4, v3, Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;->child:Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;

    .line 640
    iget-object v3, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    iget v4, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->nrElements:I

    add-int/lit8 v4, v4, 0x1

    aget-object v3, v3, v4

    iget-object v3, v3, Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;->child:Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;

    iput-object p0, v3, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->parent:Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;

    .line 641
    const/4 v0, 0x0

    :goto_3
    iget v3, v2, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->nrElements:I

    if-le v0, v3, :cond_3

    .line 642
    iget-object v3, v2, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    iget v4, v2, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->nrElements:I

    aput-object v7, v3, v4

    .line 643
    iget v3, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->nrElements:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->nrElements:I

    .line 644
    iget v3, v2, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->nrElements:I

    add-int/lit8 v3, v3, -0x1

    iput v3, v2, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->nrElements:I

    goto :goto_1

    .line 635
    :cond_2
    iget-object v3, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    iget-object v4, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    add-int/lit8 v5, v0, 0x1

    aget-object v4, v4, v5

    aput-object v4, v3, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 641
    :cond_3
    iget-object v3, v2, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    iget-object v4, v2, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    add-int/lit8 v5, v0, 0x1

    aget-object v4, v4, v5

    aput-object v4, v3, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_3
.end method

.method private switchWithSuccessor(Ljava/lang/Object;)V
    .locals 6
    .param p1, "x"    # Ljava/lang/Object;

    .prologue
    const/4 v5, 0x0

    .line 573
    invoke-direct {p0, p1, v5}, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->childToInsertAt(Ljava/lang/Object;Z)I

    move-result v0

    .line 574
    .local v0, "index":I
    iget-object v3, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    add-int/lit8 v4, v0, 0x1

    aget-object v3, v3, v4

    iget-object v2, v3, Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;->child:Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;

    .line 575
    .local v2, "temp":Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;
    :goto_0
    iget-object v3, v2, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    aget-object v3, v3, v5

    if-eqz v3, :cond_0

    iget-object v3, v2, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    aget-object v3, v3, v5

    iget-object v3, v3, Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;->child:Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;

    if-nez v3, :cond_1

    .line 576
    :cond_0
    iget-object v3, v2, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    aget-object v3, v3, v5

    iget-object v1, v3, Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;->element:Ljava/lang/Object;

    .line 577
    .local v1, "successor":Ljava/lang/Object;
    iget-object v3, v2, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    aget-object v3, v3, v5

    iget-object v4, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    aget-object v4, v4, v0

    iget-object v4, v4, Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;->element:Ljava/lang/Object;

    iput-object v4, v3, Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;->element:Ljava/lang/Object;

    .line 578
    iget-object v3, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    aget-object v3, v3, v0

    iput-object v1, v3, Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;->element:Ljava/lang/Object;

    .line 579
    return-void

    .line 575
    .end local v1    # "successor":Ljava/lang/Object;
    :cond_1
    iget-object v3, v2, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    aget-object v3, v3, v5

    iget-object v2, v3, Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;->child:Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;

    goto :goto_0
.end method


# virtual methods
.method delete(Ljava/lang/Object;I)Z
    .locals 7
    .param p1, "x"    # Ljava/lang/Object;
    .param p2, "parentIndex"    # I

    .prologue
    const/4 v4, 0x0

    const/4 v6, -0x1

    const/4 v3, 0x1

    .line 367
    invoke-direct {p0, p1, v3}, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->childToInsertAt(Ljava/lang/Object;Z)I

    move-result v0

    .line 368
    .local v0, "i":I
    move v1, p2

    .line 369
    .local v1, "priorParentIndex":I
    move-object v2, p0

    .line 370
    .local v2, "temp":Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;
    if-eq v0, v6, :cond_3

    .line 374
    :cond_0
    iget-object v5, v2, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    aget-object v5, v5, v0

    if-eqz v5, :cond_1

    iget-object v5, v2, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    aget-object v5, v5, v0

    iget-object v5, v5, Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;->child:Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;

    if-nez v5, :cond_2

    :cond_1
    move v3, v4

    .line 402
    :goto_0
    return v3

    .line 375
    :cond_2
    iget-object v5, v2, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    aget-object v5, v5, v0

    iget-object v2, v5, Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;->child:Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;

    .line 376
    move v1, p2

    .line 377
    move p2, v0

    .line 378
    invoke-direct {v2, p1, v3}, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->childToInsertAt(Ljava/lang/Object;Z)I

    move-result v0

    .line 379
    if-ne v0, v6, :cond_0

    .line 382
    :cond_3
    invoke-direct {v2}, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->isLeaf()Z

    move-result v5

    if-eqz v5, :cond_5

    .line 384
    iget v4, v2, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->nrElements:I

    iget v5, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->MIN:I

    if-le v4, v5, :cond_4

    .line 386
    invoke-direct {v2, p1}, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->deleteElement(Ljava/lang/Object;)V

    .line 387
    iget-object v4, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->this$0:Lorg/apache/poi/hdf/model/util/BTreeSet;

    iget v5, v4, Lorg/apache/poi/hdf/model/util/BTreeSet;->size:I

    add-int/lit8 v5, v5, -0x1

    iput v5, v4, Lorg/apache/poi/hdf/model/util/BTreeSet;->size:I

    goto :goto_0

    .line 392
    :cond_4
    invoke-direct {v2, p2}, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->prepareForDeletion(I)V

    .line 393
    invoke-direct {v2, p1}, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->deleteElement(Ljava/lang/Object;)V

    .line 394
    iget-object v4, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->this$0:Lorg/apache/poi/hdf/model/util/BTreeSet;

    iget v5, v4, Lorg/apache/poi/hdf/model/util/BTreeSet;->size:I

    add-int/lit8 v5, v5, -0x1

    iput v5, v4, Lorg/apache/poi/hdf/model/util/BTreeSet;->size:I

    .line 395
    invoke-direct {v2, v1}, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->fixAfterDeletion(I)V

    goto :goto_0

    .line 400
    :cond_5
    invoke-direct {v2, p1}, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->switchWithSuccessor(Ljava/lang/Object;)V

    .line 401
    invoke-direct {v2, p1, v4}, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->childToInsertAt(Ljava/lang/Object;Z)I

    move-result v3

    add-int/lit8 p2, v3, 0x1

    .line 402
    iget-object v3, v2, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    aget-object v3, v3, p2

    iget-object v3, v3, Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;->child:Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;

    invoke-virtual {v3, p1, p2}, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->delete(Ljava/lang/Object;I)Z

    move-result v3

    goto :goto_0
.end method

.method includes(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "x"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    .line 359
    invoke-direct {p0, p1, v1}, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->childToInsertAt(Ljava/lang/Object;Z)I

    move-result v0

    .line 360
    .local v0, "index":I
    const/4 v2, -0x1

    if-ne v0, v2, :cond_0

    .line 362
    :goto_0
    return v1

    .line 361
    :cond_0
    iget-object v1, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    aget-object v1, v1, v0

    if-eqz v1, :cond_1

    iget-object v1, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    aget-object v1, v1, v0

    iget-object v1, v1, Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;->child:Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;

    if-nez v1, :cond_2

    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 362
    :cond_2
    iget-object v1, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    aget-object v1, v1, v0

    iget-object v1, v1, Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;->child:Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;

    invoke-virtual {v1, p1}, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->includes(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0
.end method

.method insert(Ljava/lang/Object;I)Z
    .locals 7
    .param p1, "x"    # Ljava/lang/Object;
    .param p2, "parentIndex"    # I

    .prologue
    const/4 v6, -0x1

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 314
    invoke-direct {p0}, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->isFull()Z

    move-result v5

    if-eqz v5, :cond_4

    .line 316
    iget-object v5, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    iget v6, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->nrElements:I

    div-int/lit8 v6, v6, 0x2

    aget-object v5, v5, v6

    iget-object v2, v5, Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;->element:Ljava/lang/Object;

    .line 317
    .local v2, "splitNode":Ljava/lang/Object;
    invoke-direct {p0}, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->split()Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;

    move-result-object v1

    .line 319
    .local v1, "rightSibling":Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;
    invoke-direct {p0}, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->isRoot()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 321
    invoke-direct {p0, v2, p0, v1}, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->splitRoot(Ljava/lang/Object;Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;)V

    .line 323
    iget-object v5, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->this$0:Lorg/apache/poi/hdf/model/util/BTreeSet;

    iget-object v6, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->this$0:Lorg/apache/poi/hdf/model/util/BTreeSet;

    iget-object v6, v6, Lorg/apache/poi/hdf/model/util/BTreeSet;->root:Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;

    iget-object v6, v6, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    aget-object v6, v6, v3

    iget-object v6, v6, Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;->element:Ljava/lang/Object;

    invoke-virtual {v5, p1, v6}, Lorg/apache/poi/hdf/model/util/BTreeSet;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v5

    if-gez v5, :cond_1

    invoke-virtual {p0, p1, v3}, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->insert(Ljava/lang/Object;I)Z

    .line 354
    .end local v1    # "rightSibling":Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;
    .end local v2    # "splitNode":Ljava/lang/Object;
    :cond_0
    :goto_0
    return v3

    .line 324
    .restart local v1    # "rightSibling":Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;
    .restart local v2    # "splitNode":Ljava/lang/Object;
    :cond_1
    invoke-virtual {v1, p1, v4}, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->insert(Ljava/lang/Object;I)Z

    goto :goto_0

    .line 329
    :cond_2
    iget-object v3, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->parent:Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;

    invoke-direct {v3, v2, p0, v1, p2}, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->insertSplitNode(Ljava/lang/Object;Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;I)V

    .line 330
    iget-object v3, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->this$0:Lorg/apache/poi/hdf/model/util/BTreeSet;

    iget-object v4, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->parent:Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;

    iget-object v4, v4, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    aget-object v4, v4, p2

    iget-object v4, v4, Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;->element:Ljava/lang/Object;

    invoke-virtual {v3, p1, v4}, Lorg/apache/poi/hdf/model/util/BTreeSet;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v3

    if-gez v3, :cond_3

    .line 331
    invoke-virtual {p0, p1, p2}, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->insert(Ljava/lang/Object;I)Z

    move-result v3

    goto :goto_0

    .line 333
    :cond_3
    add-int/lit8 v3, p2, 0x1

    invoke-virtual {v1, p1, v3}, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->insert(Ljava/lang/Object;I)Z

    move-result v3

    goto :goto_0

    .line 337
    .end local v1    # "rightSibling":Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;
    .end local v2    # "splitNode":Ljava/lang/Object;
    :cond_4
    invoke-direct {p0}, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->isLeaf()Z

    move-result v5

    if-eqz v5, :cond_5

    .line 339
    invoke-direct {p0, p1, v4}, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->childToInsertAt(Ljava/lang/Object;Z)I

    move-result v0

    .line 341
    .local v0, "insertAt":I
    if-eq v0, v6, :cond_0

    .line 344
    invoke-direct {p0, p1, v0}, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->insertNewElement(Ljava/lang/Object;I)V

    .line 345
    iget-object v3, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->this$0:Lorg/apache/poi/hdf/model/util/BTreeSet;

    iget v5, v3, Lorg/apache/poi/hdf/model/util/BTreeSet;->size:I

    add-int/lit8 v5, v5, 0x1

    iput v5, v3, Lorg/apache/poi/hdf/model/util/BTreeSet;->size:I

    move v3, v4

    .line 346
    goto :goto_0

    .line 351
    .end local v0    # "insertAt":I
    :cond_5
    invoke-direct {p0, p1, v4}, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->childToInsertAt(Ljava/lang/Object;Z)I

    move-result v0

    .line 352
    .restart local v0    # "insertAt":I
    if-eq v0, v6, :cond_0

    iget-object v3, p0, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->entries:[Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;

    aget-object v3, v3, v0

    iget-object v3, v3, Lorg/apache/poi/hdf/model/util/BTreeSet$Entry;->child:Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;

    invoke-virtual {v3, p1, v0}, Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;->insert(Ljava/lang/Object;I)Z

    move-result v3

    goto :goto_0
.end method
