.class final Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTIterator;
.super Ljava/lang/Object;
.source "BTreeSet.java"

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/poi/hdf/extractor/util/BTreeSet;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "BTIterator"
.end annotation


# instance fields
.field currentNode:Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;

.field private index:I

.field private lastReturned:Ljava/lang/Object;

.field private next:Ljava/lang/Object;

.field parentIndex:Ljava/util/Stack;

.field final synthetic this$0:Lorg/apache/poi/hdf/extractor/util/BTreeSet;


# direct methods
.method constructor <init>(Lorg/apache/poi/hdf/extractor/util/BTreeSet;)V
    .locals 1

    .prologue
    .line 135
    iput-object p1, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTIterator;->this$0:Lorg/apache/poi/hdf/extractor/util/BTreeSet;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 129
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTIterator;->index:I

    .line 130
    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTIterator;->parentIndex:Ljava/util/Stack;

    .line 131
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTIterator;->lastReturned:Ljava/lang/Object;

    .line 136
    invoke-direct {p0}, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTIterator;->firstNode()Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTIterator;->currentNode:Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;

    .line 137
    invoke-direct {p0}, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTIterator;->nextElement()Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTIterator;->next:Ljava/lang/Object;

    .line 138
    return-void
.end method

.method private firstNode()Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 160
    iget-object v1, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTIterator;->this$0:Lorg/apache/poi/hdf/extractor/util/BTreeSet;

    iget-object v0, v1, Lorg/apache/poi/hdf/extractor/util/BTreeSet;->root:Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;

    .line 162
    .local v0, "temp":Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;
    :goto_0
    iget-object v1, v0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    aget-object v1, v1, v3

    iget-object v1, v1, Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;->child:Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;

    if-nez v1, :cond_0

    .line 167
    return-object v0

    .line 163
    :cond_0
    iget-object v1, v0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    aget-object v1, v1, v3

    iget-object v0, v1, Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;->child:Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;

    .line 164
    iget-object v1, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTIterator;->parentIndex:Ljava/util/Stack;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method private nextElement()Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 171
    iget-object v0, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTIterator;->currentNode:Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;

    invoke-virtual {v0}, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->isLeaf()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 172
    iget v0, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTIterator;->index:I

    iget-object v2, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTIterator;->currentNode:Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;

    iget v2, v2, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_nrElements:I

    if-ge v0, v2, :cond_0

    iget-object v0, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTIterator;->currentNode:Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;

    iget-object v0, v0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    iget v1, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTIterator;->index:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTIterator;->index:I

    aget-object v0, v0, v1

    iget-object v0, v0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;->element:Ljava/lang/Object;

    .line 204
    :goto_0
    return-object v0

    .line 174
    :cond_0
    iget-object v0, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTIterator;->parentIndex:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->empty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 175
    iget-object v0, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTIterator;->currentNode:Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;

    iget-object v0, v0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_parent:Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;

    iput-object v0, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTIterator;->currentNode:Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;

    .line 176
    iget-object v0, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTIterator;->parentIndex:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTIterator;->index:I

    .line 178
    :goto_1
    iget v0, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTIterator;->index:I

    iget-object v2, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTIterator;->currentNode:Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;

    iget v2, v2, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_nrElements:I

    if-eq v0, v2, :cond_2

    .line 184
    :cond_1
    iget v0, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTIterator;->index:I

    iget-object v2, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTIterator;->currentNode:Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;

    iget v2, v2, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_nrElements:I

    if-ne v0, v2, :cond_3

    move-object v0, v1

    goto :goto_0

    .line 179
    :cond_2
    iget-object v0, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTIterator;->parentIndex:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->empty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 180
    iget-object v0, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTIterator;->currentNode:Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;

    iget-object v0, v0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_parent:Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;

    iput-object v0, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTIterator;->currentNode:Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;

    .line 181
    iget-object v0, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTIterator;->parentIndex:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTIterator;->index:I

    goto :goto_1

    .line 185
    :cond_3
    iget-object v0, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTIterator;->currentNode:Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;

    iget-object v0, v0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    iget v1, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTIterator;->index:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTIterator;->index:I

    aget-object v0, v0, v1

    iget-object v0, v0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;->element:Ljava/lang/Object;

    goto :goto_0

    .line 189
    :cond_4
    iget v0, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTIterator;->index:I

    iget-object v2, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTIterator;->currentNode:Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;

    iget v2, v2, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_nrElements:I

    if-ne v0, v2, :cond_5

    move-object v0, v1

    goto :goto_0

    .line 190
    :cond_5
    iget-object v0, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTIterator;->currentNode:Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;

    iget-object v0, v0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    iget v1, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTIterator;->index:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTIterator;->index:I

    aget-object v0, v0, v1

    iget-object v0, v0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;->element:Ljava/lang/Object;

    goto :goto_0

    .line 195
    :cond_6
    iget-object v0, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTIterator;->currentNode:Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;

    iget-object v0, v0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    iget v1, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTIterator;->index:I

    aget-object v0, v0, v1

    iget-object v0, v0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;->child:Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;

    iput-object v0, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTIterator;->currentNode:Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;

    .line 196
    iget-object v0, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTIterator;->parentIndex:Ljava/util/Stack;

    iget v1, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTIterator;->index:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 198
    :goto_2
    iget-object v0, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTIterator;->currentNode:Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;

    iget-object v0, v0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    aget-object v0, v0, v2

    iget-object v0, v0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;->child:Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;

    if-nez v0, :cond_7

    .line 203
    const/4 v0, 0x1

    iput v0, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTIterator;->index:I

    .line 204
    iget-object v0, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTIterator;->currentNode:Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;

    iget-object v0, v0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    aget-object v0, v0, v2

    iget-object v0, v0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;->element:Ljava/lang/Object;

    goto/16 :goto_0

    .line 199
    :cond_7
    iget-object v0, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTIterator;->currentNode:Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;

    iget-object v0, v0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->_entries:[Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;

    aget-object v0, v0, v2

    iget-object v0, v0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;->child:Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;

    iput-object v0, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTIterator;->currentNode:Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;

    .line 200
    iget-object v0, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTIterator;->parentIndex:Ljava/util/Stack;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2
.end method


# virtual methods
.method public hasNext()Z
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTIterator;->next:Ljava/lang/Object;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public next()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTIterator;->next:Ljava/lang/Object;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 147
    :cond_0
    iget-object v0, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTIterator;->next:Ljava/lang/Object;

    iput-object v0, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTIterator;->lastReturned:Ljava/lang/Object;

    .line 148
    invoke-direct {p0}, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTIterator;->nextElement()Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTIterator;->next:Ljava/lang/Object;

    .line 149
    iget-object v0, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTIterator;->lastReturned:Ljava/lang/Object;

    return-object v0
.end method

.method public remove()V
    .locals 2

    .prologue
    .line 153
    iget-object v0, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTIterator;->lastReturned:Ljava/lang/Object;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 155
    :cond_0
    iget-object v0, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTIterator;->this$0:Lorg/apache/poi/hdf/extractor/util/BTreeSet;

    iget-object v1, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTIterator;->lastReturned:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lorg/apache/poi/hdf/extractor/util/BTreeSet;->remove(Ljava/lang/Object;)Z

    .line 156
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTIterator;->lastReturned:Ljava/lang/Object;

    .line 157
    return-void
.end method
