.class public abstract Lorg/apache/poi/hdf/model/hdftypes/definitions/TCAbstractType;
.super Ljava/lang/Object;
.source "TCAbstractType.java"

# interfaces
.implements Lorg/apache/poi/hdf/model/hdftypes/HDFType;


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field private static fBackward:Lorg/apache/poi/util/BitField;

.field private static fFirstMerged:Lorg/apache/poi/util/BitField;

.field private static fMerged:Lorg/apache/poi/util/BitField;

.field private static fRotateFont:Lorg/apache/poi/util/BitField;

.field private static fVertMerge:Lorg/apache/poi/util/BitField;

.field private static fVertRestart:Lorg/apache/poi/util/BitField;

.field private static fVertical:Lorg/apache/poi/util/BitField;

.field private static vertAlign:Lorg/apache/poi/util/BitField;


# instance fields
.field private field_1_rgf:S

.field private field_2_unused:S

.field private field_3_brcTop:[S

.field private field_4_brcLeft:[S

.field private field_5_brcBottom:[S

.field private field_6_brcRight:[S


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 37
    const/4 v0, 0x1

    invoke-static {v0}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/TCAbstractType;->fFirstMerged:Lorg/apache/poi/util/BitField;

    .line 38
    const/4 v0, 0x2

    invoke-static {v0}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/TCAbstractType;->fMerged:Lorg/apache/poi/util/BitField;

    .line 39
    const/4 v0, 0x4

    invoke-static {v0}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/TCAbstractType;->fVertical:Lorg/apache/poi/util/BitField;

    .line 40
    const/16 v0, 0x8

    invoke-static {v0}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/TCAbstractType;->fBackward:Lorg/apache/poi/util/BitField;

    .line 41
    const/16 v0, 0x10

    invoke-static {v0}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/TCAbstractType;->fRotateFont:Lorg/apache/poi/util/BitField;

    .line 42
    const/16 v0, 0x20

    invoke-static {v0}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/TCAbstractType;->fVertMerge:Lorg/apache/poi/util/BitField;

    .line 43
    const/16 v0, 0x40

    invoke-static {v0}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/TCAbstractType;->fVertRestart:Lorg/apache/poi/util/BitField;

    .line 44
    const/16 v0, 0x180

    invoke-static {v0}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/TCAbstractType;->vertAlign:Lorg/apache/poi/util/BitField;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    return-void
.end method


# virtual methods
.method public getBrcBottom()[S
    .locals 1

    .prologue
    .line 136
    iget-object v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/TCAbstractType;->field_5_brcBottom:[S

    return-object v0
.end method

.method public getBrcLeft()[S
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/TCAbstractType;->field_4_brcLeft:[S

    return-object v0
.end method

.method public getBrcRight()[S
    .locals 1

    .prologue
    .line 152
    iget-object v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/TCAbstractType;->field_6_brcRight:[S

    return-object v0
.end method

.method public getBrcTop()[S
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/TCAbstractType;->field_3_brcTop:[S

    return-object v0
.end method

.method public getRgf()S
    .locals 1

    .prologue
    .line 72
    iget-short v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/TCAbstractType;->field_1_rgf:S

    return v0
.end method

.method public getSize()I
    .locals 1

    .prologue
    .line 62
    const/16 v0, 0x18

    return v0
.end method

.method public getUnused()S
    .locals 1

    .prologue
    .line 88
    iget-short v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/TCAbstractType;->field_2_unused:S

    return v0
.end method

.method public getVertAlign()B
    .locals 2

    .prologue
    .line 327
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/TCAbstractType;->vertAlign:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/TCAbstractType;->field_1_rgf:S

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->getValue(I)I

    move-result v0

    int-to-byte v0, v0

    return v0
.end method

.method public isFBackward()Z
    .locals 2

    .prologue
    .line 243
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/TCAbstractType;->fBackward:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/TCAbstractType;->field_1_rgf:S

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFFirstMerged()Z
    .locals 2

    .prologue
    .line 180
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/TCAbstractType;->fFirstMerged:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/TCAbstractType;->field_1_rgf:S

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFMerged()Z
    .locals 2

    .prologue
    .line 201
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/TCAbstractType;->fMerged:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/TCAbstractType;->field_1_rgf:S

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFRotateFont()Z
    .locals 2

    .prologue
    .line 264
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/TCAbstractType;->fRotateFont:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/TCAbstractType;->field_1_rgf:S

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFVertMerge()Z
    .locals 2

    .prologue
    .line 285
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/TCAbstractType;->fVertMerge:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/TCAbstractType;->field_1_rgf:S

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFVertRestart()Z
    .locals 2

    .prologue
    .line 306
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/TCAbstractType;->fVertRestart:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/TCAbstractType;->field_1_rgf:S

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFVertical()Z
    .locals 2

    .prologue
    .line 222
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/TCAbstractType;->fVertical:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/TCAbstractType;->field_1_rgf:S

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public setBrcBottom([S)V
    .locals 0
    .param p1, "field_5_brcBottom"    # [S

    .prologue
    .line 144
    iput-object p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/TCAbstractType;->field_5_brcBottom:[S

    .line 145
    return-void
.end method

.method public setBrcLeft([S)V
    .locals 0
    .param p1, "field_4_brcLeft"    # [S

    .prologue
    .line 128
    iput-object p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/TCAbstractType;->field_4_brcLeft:[S

    .line 129
    return-void
.end method

.method public setBrcRight([S)V
    .locals 0
    .param p1, "field_6_brcRight"    # [S

    .prologue
    .line 160
    iput-object p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/TCAbstractType;->field_6_brcRight:[S

    .line 161
    return-void
.end method

.method public setBrcTop([S)V
    .locals 0
    .param p1, "field_3_brcTop"    # [S

    .prologue
    .line 112
    iput-object p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/TCAbstractType;->field_3_brcTop:[S

    .line 113
    return-void
.end method

.method public setFBackward(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 232
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/TCAbstractType;->fBackward:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/TCAbstractType;->field_1_rgf:S

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/TCAbstractType;->field_1_rgf:S

    .line 235
    return-void
.end method

.method public setFFirstMerged(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 169
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/TCAbstractType;->fFirstMerged:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/TCAbstractType;->field_1_rgf:S

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/TCAbstractType;->field_1_rgf:S

    .line 172
    return-void
.end method

.method public setFMerged(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 190
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/TCAbstractType;->fMerged:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/TCAbstractType;->field_1_rgf:S

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/TCAbstractType;->field_1_rgf:S

    .line 193
    return-void
.end method

.method public setFRotateFont(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 253
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/TCAbstractType;->fRotateFont:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/TCAbstractType;->field_1_rgf:S

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/TCAbstractType;->field_1_rgf:S

    .line 256
    return-void
.end method

.method public setFVertMerge(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 274
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/TCAbstractType;->fVertMerge:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/TCAbstractType;->field_1_rgf:S

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/TCAbstractType;->field_1_rgf:S

    .line 277
    return-void
.end method

.method public setFVertRestart(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 295
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/TCAbstractType;->fVertRestart:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/TCAbstractType;->field_1_rgf:S

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/TCAbstractType;->field_1_rgf:S

    .line 298
    return-void
.end method

.method public setFVertical(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 211
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/TCAbstractType;->fVertical:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/TCAbstractType;->field_1_rgf:S

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/TCAbstractType;->field_1_rgf:S

    .line 214
    return-void
.end method

.method public setRgf(S)V
    .locals 0
    .param p1, "field_1_rgf"    # S

    .prologue
    .line 80
    iput-short p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/TCAbstractType;->field_1_rgf:S

    .line 81
    return-void
.end method

.method public setUnused(S)V
    .locals 0
    .param p1, "field_2_unused"    # S

    .prologue
    .line 96
    iput-short p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/TCAbstractType;->field_2_unused:S

    .line 97
    return-void
.end method

.method public setVertAlign(B)V
    .locals 2
    .param p1, "value"    # B

    .prologue
    .line 316
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/TCAbstractType;->vertAlign:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/TCAbstractType;->field_1_rgf:S

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setValue(II)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/TCAbstractType;->field_1_rgf:S

    .line 319
    return-void
.end method
