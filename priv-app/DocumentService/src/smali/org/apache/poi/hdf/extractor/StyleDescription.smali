.class public final Lorg/apache/poi/hdf/extractor/StyleDescription;
.super Ljava/lang/Object;
.source "StyleDescription.java"


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field private static CHARACTER_STYLE:I

.field private static PARAGRAPH_STYLE:I


# instance fields
.field _baseStyleIndex:I

.field _chp:Lorg/apache/poi/hdf/extractor/CHP;

.field _chpx:[B

.field _numUPX:I

.field _pap:Lorg/apache/poi/hdf/extractor/PAP;

.field _papx:[B

.field _styleTypeCode:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    const/4 v0, 0x1

    sput v0, Lorg/apache/poi/hdf/extractor/StyleDescription;->PARAGRAPH_STYLE:I

    .line 31
    const/4 v0, 0x2

    sput v0, Lorg/apache/poi/hdf/extractor/StyleDescription;->CHARACTER_STYLE:I

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    new-instance v0, Lorg/apache/poi/hdf/extractor/PAP;

    invoke-direct {v0}, Lorg/apache/poi/hdf/extractor/PAP;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/hdf/extractor/StyleDescription;->_pap:Lorg/apache/poi/hdf/extractor/PAP;

    .line 44
    new-instance v0, Lorg/apache/poi/hdf/extractor/CHP;

    invoke-direct {v0}, Lorg/apache/poi/hdf/extractor/CHP;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/hdf/extractor/StyleDescription;->_chp:Lorg/apache/poi/hdf/extractor/CHP;

    .line 45
    return-void
.end method

.method public constructor <init>([BIZ)V
    .locals 11
    .param p1, "std"    # [B
    .param p2, "baseLength"    # I
    .param p3, "word9"    # Z

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    const/4 v7, 0x2

    invoke-static {p1, v7}, Lorg/apache/poi/hdf/extractor/Utils;->convertBytesToShort([BI)S

    move-result v1

    .line 49
    .local v1, "infoShort":I
    and-int/lit8 v7, v1, 0xf

    iput v7, p0, Lorg/apache/poi/hdf/extractor/StyleDescription;->_styleTypeCode:I

    .line 50
    const v7, 0xfff0

    and-int/2addr v7, v1

    shr-int/lit8 v7, v7, 0x4

    iput v7, p0, Lorg/apache/poi/hdf/extractor/StyleDescription;->_baseStyleIndex:I

    .line 52
    const/4 v7, 0x4

    invoke-static {p1, v7}, Lorg/apache/poi/hdf/extractor/Utils;->convertBytesToShort([BI)S

    move-result v1

    .line 53
    and-int/lit8 v7, v1, 0xf

    iput v7, p0, Lorg/apache/poi/hdf/extractor/StyleDescription;->_numUPX:I

    .line 57
    const/4 v3, 0x0

    .line 58
    .local v3, "nameLength":I
    const/4 v2, 0x1

    .line 59
    .local v2, "multiplier":I
    if-eqz p3, :cond_0

    .line 61
    invoke-static {p1, p2}, Lorg/apache/poi/hdf/extractor/Utils;->convertBytesToShort([BI)S

    move-result v3

    .line 62
    const/4 v2, 0x2

    .line 69
    :goto_0
    add-int/lit8 v7, v3, 0x1

    mul-int/2addr v7, v2

    add-int/2addr v7, v2

    add-int v0, v7, p2

    .line 71
    .local v0, "grupxStart":I
    const/4 v4, 0x0

    .line 72
    .local v4, "offset":I
    const/4 v6, 0x0

    .local v6, "x":I
    :goto_1
    iget v7, p0, Lorg/apache/poi/hdf/extractor/StyleDescription;->_numUPX:I

    if-lt v6, v7, :cond_1

    .line 103
    return-void

    .line 66
    .end local v0    # "grupxStart":I
    .end local v4    # "offset":I
    .end local v6    # "x":I
    :cond_0
    aget-byte v3, p1, p2

    goto :goto_0

    .line 74
    .restart local v0    # "grupxStart":I
    .restart local v4    # "offset":I
    .restart local v6    # "x":I
    :cond_1
    add-int v7, v0, v4

    invoke-static {p1, v7}, Lorg/apache/poi/hdf/extractor/Utils;->convertBytesToShort([BI)S

    move-result v5

    .line 75
    .local v5, "upxSize":I
    iget v7, p0, Lorg/apache/poi/hdf/extractor/StyleDescription;->_styleTypeCode:I

    sget v8, Lorg/apache/poi/hdf/extractor/StyleDescription;->PARAGRAPH_STYLE:I

    if-ne v7, v8, :cond_5

    .line 77
    if-nez v6, :cond_4

    .line 79
    new-array v7, v5, [B

    iput-object v7, p0, Lorg/apache/poi/hdf/extractor/StyleDescription;->_papx:[B

    .line 80
    add-int v7, v0, v4

    add-int/lit8 v7, v7, 0x2

    iget-object v8, p0, Lorg/apache/poi/hdf/extractor/StyleDescription;->_papx:[B

    invoke-static {p1, v7, v8, v9, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 94
    :cond_2
    :goto_2
    rem-int/lit8 v7, v5, 0x2

    if-ne v7, v10, :cond_3

    .line 96
    add-int/lit8 v5, v5, 0x1

    .line 98
    :cond_3
    add-int/lit8 v7, v5, 0x2

    add-int/2addr v4, v7

    .line 72
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 82
    :cond_4
    if-ne v6, v10, :cond_2

    .line 84
    new-array v7, v5, [B

    iput-object v7, p0, Lorg/apache/poi/hdf/extractor/StyleDescription;->_chpx:[B

    .line 85
    add-int v7, v0, v4

    add-int/lit8 v7, v7, 0x2

    iget-object v8, p0, Lorg/apache/poi/hdf/extractor/StyleDescription;->_chpx:[B

    invoke-static {p1, v7, v8, v9, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_2

    .line 88
    :cond_5
    iget v7, p0, Lorg/apache/poi/hdf/extractor/StyleDescription;->_styleTypeCode:I

    sget v8, Lorg/apache/poi/hdf/extractor/StyleDescription;->CHARACTER_STYLE:I

    if-ne v7, v8, :cond_2

    if-nez v6, :cond_2

    .line 90
    new-array v7, v5, [B

    iput-object v7, p0, Lorg/apache/poi/hdf/extractor/StyleDescription;->_chpx:[B

    .line 91
    add-int v7, v0, v4

    add-int/lit8 v7, v7, 0x2

    iget-object v8, p0, Lorg/apache/poi/hdf/extractor/StyleDescription;->_chpx:[B

    invoke-static {p1, v7, v8, v9, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_2
.end method


# virtual methods
.method public getBaseStyle()I
    .locals 1

    .prologue
    .line 106
    iget v0, p0, Lorg/apache/poi/hdf/extractor/StyleDescription;->_baseStyleIndex:I

    return v0
.end method

.method public getCHP()Lorg/apache/poi/hdf/extractor/CHP;
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lorg/apache/poi/hdf/extractor/StyleDescription;->_chp:Lorg/apache/poi/hdf/extractor/CHP;

    return-object v0
.end method

.method public getCHPX()[B
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lorg/apache/poi/hdf/extractor/StyleDescription;->_chpx:[B

    return-object v0
.end method

.method public getPAP()Lorg/apache/poi/hdf/extractor/PAP;
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lorg/apache/poi/hdf/extractor/StyleDescription;->_pap:Lorg/apache/poi/hdf/extractor/PAP;

    return-object v0
.end method

.method public getPAPX()[B
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, Lorg/apache/poi/hdf/extractor/StyleDescription;->_papx:[B

    return-object v0
.end method

.method public setCHP(Lorg/apache/poi/hdf/extractor/CHP;)V
    .locals 0
    .param p1, "chp"    # Lorg/apache/poi/hdf/extractor/CHP;

    .prologue
    .line 130
    iput-object p1, p0, Lorg/apache/poi/hdf/extractor/StyleDescription;->_chp:Lorg/apache/poi/hdf/extractor/CHP;

    .line 131
    return-void
.end method

.method public setPAP(Lorg/apache/poi/hdf/extractor/PAP;)V
    .locals 0
    .param p1, "pap"    # Lorg/apache/poi/hdf/extractor/PAP;

    .prologue
    .line 126
    iput-object p1, p0, Lorg/apache/poi/hdf/extractor/StyleDescription;->_pap:Lorg/apache/poi/hdf/extractor/PAP;

    .line 127
    return-void
.end method
