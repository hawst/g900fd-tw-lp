.class public abstract Lorg/apache/poi/hdf/model/hdftypes/definitions/SEPAbstractType;
.super Ljava/lang/Object;
.source "SEPAbstractType.java"

# interfaces
.implements Lorg/apache/poi/hdf/model/hdftypes/HDFType;


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private field_10_grpfIhdt:B

.field private field_11_nLnnMod:I

.field private field_12_dxaLnn:I

.field private field_13_dxaPgn:I

.field private field_14_dyaPgn:I

.field private field_15_fLBetween:Z

.field private field_16_vjc:B

.field private field_17_dmBinFirst:I

.field private field_18_dmBinOther:I

.field private field_19_dmPaperReq:I

.field private field_1_bkc:B

.field private field_20_brcTop:[S

.field private field_21_brcLeft:[S

.field private field_22_brcBottom:[S

.field private field_23_brcRight:[S

.field private field_24_fPropMark:Z

.field private field_25_ibstPropRMark:I

.field private field_26_dttmPropRMark:I

.field private field_27_dxtCharSpace:I

.field private field_28_dyaLinePitch:I

.field private field_29_clm:I

.field private field_2_fTitlePage:Z

.field private field_30_unused2:I

.field private field_31_dmOrientPage:B

.field private field_32_iHeadingPgn:B

.field private field_33_pgnStart:I

.field private field_34_lnnMin:I

.field private field_35_wTextFlow:I

.field private field_36_unused3:S

.field private field_37_pgbProp:I

.field private field_38_unused4:S

.field private field_39_xaPage:I

.field private field_3_fAutoPgn:Z

.field private field_40_yaPage:I

.field private field_41_xaPageNUp:I

.field private field_42_yaPageNUp:I

.field private field_43_dxaLeft:I

.field private field_44_dxaRight:I

.field private field_45_dyaTop:I

.field private field_46_dyaBottom:I

.field private field_47_dzaGutter:I

.field private field_48_dyaHdrTop:I

.field private field_49_dyaHdrBottom:I

.field private field_4_nfcPgn:B

.field private field_50_ccolM1:I

.field private field_51_fEvenlySpaced:Z

.field private field_52_unused5:B

.field private field_53_dxaColumns:I

.field private field_54_rgdxaColumn:[I

.field private field_55_dxaColumnWidth:I

.field private field_56_dmOrientFirst:B

.field private field_57_fLayout:B

.field private field_58_unused6:S

.field private field_59_olstAnm:[B

.field private field_5_fUnlocked:Z

.field private field_6_cnsPgn:B

.field private field_7_fPgnRestart:Z

.field private field_8_fEndNote:Z

.field private field_9_lnc:B


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 96
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 99
    return-void
.end method


# virtual methods
.method public getBkc()B
    .locals 1

    .prologue
    .line 116
    iget-byte v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/SEPAbstractType;->field_1_bkc:B

    return v0
.end method

.method public getBrcBottom()[S
    .locals 1

    .prologue
    .line 452
    iget-object v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/SEPAbstractType;->field_22_brcBottom:[S

    return-object v0
.end method

.method public getBrcLeft()[S
    .locals 1

    .prologue
    .line 436
    iget-object v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/SEPAbstractType;->field_21_brcLeft:[S

    return-object v0
.end method

.method public getBrcRight()[S
    .locals 1

    .prologue
    .line 468
    iget-object v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/SEPAbstractType;->field_23_brcRight:[S

    return-object v0
.end method

.method public getBrcTop()[S
    .locals 1

    .prologue
    .line 420
    iget-object v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/SEPAbstractType;->field_20_brcTop:[S

    return-object v0
.end method

.method public getCcolM1()I
    .locals 1

    .prologue
    .line 900
    iget v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/SEPAbstractType;->field_50_ccolM1:I

    return v0
.end method

.method public getClm()I
    .locals 1

    .prologue
    .line 564
    iget v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/SEPAbstractType;->field_29_clm:I

    return v0
.end method

.method public getCnsPgn()B
    .locals 1

    .prologue
    .line 196
    iget-byte v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/SEPAbstractType;->field_6_cnsPgn:B

    return v0
.end method

.method public getDmBinFirst()I
    .locals 1

    .prologue
    .line 372
    iget v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/SEPAbstractType;->field_17_dmBinFirst:I

    return v0
.end method

.method public getDmBinOther()I
    .locals 1

    .prologue
    .line 388
    iget v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/SEPAbstractType;->field_18_dmBinOther:I

    return v0
.end method

.method public getDmOrientFirst()B
    .locals 1

    .prologue
    .line 996
    iget-byte v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/SEPAbstractType;->field_56_dmOrientFirst:B

    return v0
.end method

.method public getDmOrientPage()B
    .locals 1

    .prologue
    .line 596
    iget-byte v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/SEPAbstractType;->field_31_dmOrientPage:B

    return v0
.end method

.method public getDmPaperReq()I
    .locals 1

    .prologue
    .line 404
    iget v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/SEPAbstractType;->field_19_dmPaperReq:I

    return v0
.end method

.method public getDttmPropRMark()I
    .locals 1

    .prologue
    .line 516
    iget v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/SEPAbstractType;->field_26_dttmPropRMark:I

    return v0
.end method

.method public getDxaColumnWidth()I
    .locals 1

    .prologue
    .line 980
    iget v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/SEPAbstractType;->field_55_dxaColumnWidth:I

    return v0
.end method

.method public getDxaColumns()I
    .locals 1

    .prologue
    .line 948
    iget v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/SEPAbstractType;->field_53_dxaColumns:I

    return v0
.end method

.method public getDxaLeft()I
    .locals 1

    .prologue
    .line 788
    iget v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/SEPAbstractType;->field_43_dxaLeft:I

    return v0
.end method

.method public getDxaLnn()I
    .locals 1

    .prologue
    .line 292
    iget v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/SEPAbstractType;->field_12_dxaLnn:I

    return v0
.end method

.method public getDxaPgn()I
    .locals 1

    .prologue
    .line 308
    iget v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/SEPAbstractType;->field_13_dxaPgn:I

    return v0
.end method

.method public getDxaRight()I
    .locals 1

    .prologue
    .line 804
    iget v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/SEPAbstractType;->field_44_dxaRight:I

    return v0
.end method

.method public getDxtCharSpace()I
    .locals 1

    .prologue
    .line 532
    iget v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/SEPAbstractType;->field_27_dxtCharSpace:I

    return v0
.end method

.method public getDyaBottom()I
    .locals 1

    .prologue
    .line 836
    iget v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/SEPAbstractType;->field_46_dyaBottom:I

    return v0
.end method

.method public getDyaHdrBottom()I
    .locals 1

    .prologue
    .line 884
    iget v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/SEPAbstractType;->field_49_dyaHdrBottom:I

    return v0
.end method

.method public getDyaHdrTop()I
    .locals 1

    .prologue
    .line 868
    iget v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/SEPAbstractType;->field_48_dyaHdrTop:I

    return v0
.end method

.method public getDyaLinePitch()I
    .locals 1

    .prologue
    .line 548
    iget v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/SEPAbstractType;->field_28_dyaLinePitch:I

    return v0
.end method

.method public getDyaPgn()I
    .locals 1

    .prologue
    .line 324
    iget v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/SEPAbstractType;->field_14_dyaPgn:I

    return v0
.end method

.method public getDyaTop()I
    .locals 1

    .prologue
    .line 820
    iget v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/SEPAbstractType;->field_45_dyaTop:I

    return v0
.end method

.method public getDzaGutter()I
    .locals 1

    .prologue
    .line 852
    iget v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/SEPAbstractType;->field_47_dzaGutter:I

    return v0
.end method

.method public getFAutoPgn()Z
    .locals 1

    .prologue
    .line 148
    iget-boolean v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/SEPAbstractType;->field_3_fAutoPgn:Z

    return v0
.end method

.method public getFEndNote()Z
    .locals 1

    .prologue
    .line 228
    iget-boolean v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/SEPAbstractType;->field_8_fEndNote:Z

    return v0
.end method

.method public getFEvenlySpaced()Z
    .locals 1

    .prologue
    .line 916
    iget-boolean v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/SEPAbstractType;->field_51_fEvenlySpaced:Z

    return v0
.end method

.method public getFLBetween()Z
    .locals 1

    .prologue
    .line 340
    iget-boolean v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/SEPAbstractType;->field_15_fLBetween:Z

    return v0
.end method

.method public getFLayout()B
    .locals 1

    .prologue
    .line 1012
    iget-byte v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/SEPAbstractType;->field_57_fLayout:B

    return v0
.end method

.method public getFPgnRestart()Z
    .locals 1

    .prologue
    .line 212
    iget-boolean v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/SEPAbstractType;->field_7_fPgnRestart:Z

    return v0
.end method

.method public getFPropMark()Z
    .locals 1

    .prologue
    .line 484
    iget-boolean v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/SEPAbstractType;->field_24_fPropMark:Z

    return v0
.end method

.method public getFTitlePage()Z
    .locals 1

    .prologue
    .line 132
    iget-boolean v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/SEPAbstractType;->field_2_fTitlePage:Z

    return v0
.end method

.method public getFUnlocked()Z
    .locals 1

    .prologue
    .line 180
    iget-boolean v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/SEPAbstractType;->field_5_fUnlocked:Z

    return v0
.end method

.method public getGrpfIhdt()B
    .locals 1

    .prologue
    .line 260
    iget-byte v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/SEPAbstractType;->field_10_grpfIhdt:B

    return v0
.end method

.method public getIHeadingPgn()B
    .locals 1

    .prologue
    .line 612
    iget-byte v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/SEPAbstractType;->field_32_iHeadingPgn:B

    return v0
.end method

.method public getIbstPropRMark()I
    .locals 1

    .prologue
    .line 500
    iget v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/SEPAbstractType;->field_25_ibstPropRMark:I

    return v0
.end method

.method public getLnc()B
    .locals 1

    .prologue
    .line 244
    iget-byte v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/SEPAbstractType;->field_9_lnc:B

    return v0
.end method

.method public getLnnMin()I
    .locals 1

    .prologue
    .line 644
    iget v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/SEPAbstractType;->field_34_lnnMin:I

    return v0
.end method

.method public getNLnnMod()I
    .locals 1

    .prologue
    .line 276
    iget v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/SEPAbstractType;->field_11_nLnnMod:I

    return v0
.end method

.method public getNfcPgn()B
    .locals 1

    .prologue
    .line 164
    iget-byte v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/SEPAbstractType;->field_4_nfcPgn:B

    return v0
.end method

.method public getOlstAnm()[B
    .locals 1

    .prologue
    .line 1044
    iget-object v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/SEPAbstractType;->field_59_olstAnm:[B

    return-object v0
.end method

.method public getPgbProp()I
    .locals 1

    .prologue
    .line 692
    iget v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/SEPAbstractType;->field_37_pgbProp:I

    return v0
.end method

.method public getPgnStart()I
    .locals 1

    .prologue
    .line 628
    iget v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/SEPAbstractType;->field_33_pgnStart:I

    return v0
.end method

.method public getRgdxaColumn()[I
    .locals 1

    .prologue
    .line 964
    iget-object v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/SEPAbstractType;->field_54_rgdxaColumn:[I

    return-object v0
.end method

.method public getSize()I
    .locals 1

    .prologue
    .line 106
    const/16 v0, 0x2bd

    return v0
.end method

.method public getUnused2()I
    .locals 1

    .prologue
    .line 580
    iget v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/SEPAbstractType;->field_30_unused2:I

    return v0
.end method

.method public getUnused3()S
    .locals 1

    .prologue
    .line 676
    iget-short v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/SEPAbstractType;->field_36_unused3:S

    return v0
.end method

.method public getUnused4()S
    .locals 1

    .prologue
    .line 708
    iget-short v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/SEPAbstractType;->field_38_unused4:S

    return v0
.end method

.method public getUnused5()B
    .locals 1

    .prologue
    .line 932
    iget-byte v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/SEPAbstractType;->field_52_unused5:B

    return v0
.end method

.method public getUnused6()S
    .locals 1

    .prologue
    .line 1028
    iget-short v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/SEPAbstractType;->field_58_unused6:S

    return v0
.end method

.method public getVjc()B
    .locals 1

    .prologue
    .line 356
    iget-byte v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/SEPAbstractType;->field_16_vjc:B

    return v0
.end method

.method public getWTextFlow()I
    .locals 1

    .prologue
    .line 660
    iget v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/SEPAbstractType;->field_35_wTextFlow:I

    return v0
.end method

.method public getXaPage()I
    .locals 1

    .prologue
    .line 724
    iget v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/SEPAbstractType;->field_39_xaPage:I

    return v0
.end method

.method public getXaPageNUp()I
    .locals 1

    .prologue
    .line 756
    iget v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/SEPAbstractType;->field_41_xaPageNUp:I

    return v0
.end method

.method public getYaPage()I
    .locals 1

    .prologue
    .line 740
    iget v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/SEPAbstractType;->field_40_yaPage:I

    return v0
.end method

.method public getYaPageNUp()I
    .locals 1

    .prologue
    .line 772
    iget v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/SEPAbstractType;->field_42_yaPageNUp:I

    return v0
.end method

.method public setBkc(B)V
    .locals 0
    .param p1, "field_1_bkc"    # B

    .prologue
    .line 124
    iput-byte p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/SEPAbstractType;->field_1_bkc:B

    .line 125
    return-void
.end method

.method public setBrcBottom([S)V
    .locals 0
    .param p1, "field_22_brcBottom"    # [S

    .prologue
    .line 460
    iput-object p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/SEPAbstractType;->field_22_brcBottom:[S

    .line 461
    return-void
.end method

.method public setBrcLeft([S)V
    .locals 0
    .param p1, "field_21_brcLeft"    # [S

    .prologue
    .line 444
    iput-object p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/SEPAbstractType;->field_21_brcLeft:[S

    .line 445
    return-void
.end method

.method public setBrcRight([S)V
    .locals 0
    .param p1, "field_23_brcRight"    # [S

    .prologue
    .line 476
    iput-object p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/SEPAbstractType;->field_23_brcRight:[S

    .line 477
    return-void
.end method

.method public setBrcTop([S)V
    .locals 0
    .param p1, "field_20_brcTop"    # [S

    .prologue
    .line 428
    iput-object p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/SEPAbstractType;->field_20_brcTop:[S

    .line 429
    return-void
.end method

.method public setCcolM1(I)V
    .locals 0
    .param p1, "field_50_ccolM1"    # I

    .prologue
    .line 908
    iput p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/SEPAbstractType;->field_50_ccolM1:I

    .line 909
    return-void
.end method

.method public setClm(I)V
    .locals 0
    .param p1, "field_29_clm"    # I

    .prologue
    .line 572
    iput p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/SEPAbstractType;->field_29_clm:I

    .line 573
    return-void
.end method

.method public setCnsPgn(B)V
    .locals 0
    .param p1, "field_6_cnsPgn"    # B

    .prologue
    .line 204
    iput-byte p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/SEPAbstractType;->field_6_cnsPgn:B

    .line 205
    return-void
.end method

.method public setDmBinFirst(I)V
    .locals 0
    .param p1, "field_17_dmBinFirst"    # I

    .prologue
    .line 380
    iput p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/SEPAbstractType;->field_17_dmBinFirst:I

    .line 381
    return-void
.end method

.method public setDmBinOther(I)V
    .locals 0
    .param p1, "field_18_dmBinOther"    # I

    .prologue
    .line 396
    iput p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/SEPAbstractType;->field_18_dmBinOther:I

    .line 397
    return-void
.end method

.method public setDmOrientFirst(B)V
    .locals 0
    .param p1, "field_56_dmOrientFirst"    # B

    .prologue
    .line 1004
    iput-byte p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/SEPAbstractType;->field_56_dmOrientFirst:B

    .line 1005
    return-void
.end method

.method public setDmOrientPage(B)V
    .locals 0
    .param p1, "field_31_dmOrientPage"    # B

    .prologue
    .line 604
    iput-byte p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/SEPAbstractType;->field_31_dmOrientPage:B

    .line 605
    return-void
.end method

.method public setDmPaperReq(I)V
    .locals 0
    .param p1, "field_19_dmPaperReq"    # I

    .prologue
    .line 412
    iput p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/SEPAbstractType;->field_19_dmPaperReq:I

    .line 413
    return-void
.end method

.method public setDttmPropRMark(I)V
    .locals 0
    .param p1, "field_26_dttmPropRMark"    # I

    .prologue
    .line 524
    iput p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/SEPAbstractType;->field_26_dttmPropRMark:I

    .line 525
    return-void
.end method

.method public setDxaColumnWidth(I)V
    .locals 0
    .param p1, "field_55_dxaColumnWidth"    # I

    .prologue
    .line 988
    iput p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/SEPAbstractType;->field_55_dxaColumnWidth:I

    .line 989
    return-void
.end method

.method public setDxaColumns(I)V
    .locals 0
    .param p1, "field_53_dxaColumns"    # I

    .prologue
    .line 956
    iput p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/SEPAbstractType;->field_53_dxaColumns:I

    .line 957
    return-void
.end method

.method public setDxaLeft(I)V
    .locals 0
    .param p1, "field_43_dxaLeft"    # I

    .prologue
    .line 796
    iput p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/SEPAbstractType;->field_43_dxaLeft:I

    .line 797
    return-void
.end method

.method public setDxaLnn(I)V
    .locals 0
    .param p1, "field_12_dxaLnn"    # I

    .prologue
    .line 300
    iput p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/SEPAbstractType;->field_12_dxaLnn:I

    .line 301
    return-void
.end method

.method public setDxaPgn(I)V
    .locals 0
    .param p1, "field_13_dxaPgn"    # I

    .prologue
    .line 316
    iput p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/SEPAbstractType;->field_13_dxaPgn:I

    .line 317
    return-void
.end method

.method public setDxaRight(I)V
    .locals 0
    .param p1, "field_44_dxaRight"    # I

    .prologue
    .line 812
    iput p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/SEPAbstractType;->field_44_dxaRight:I

    .line 813
    return-void
.end method

.method public setDxtCharSpace(I)V
    .locals 0
    .param p1, "field_27_dxtCharSpace"    # I

    .prologue
    .line 540
    iput p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/SEPAbstractType;->field_27_dxtCharSpace:I

    .line 541
    return-void
.end method

.method public setDyaBottom(I)V
    .locals 0
    .param p1, "field_46_dyaBottom"    # I

    .prologue
    .line 844
    iput p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/SEPAbstractType;->field_46_dyaBottom:I

    .line 845
    return-void
.end method

.method public setDyaHdrBottom(I)V
    .locals 0
    .param p1, "field_49_dyaHdrBottom"    # I

    .prologue
    .line 892
    iput p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/SEPAbstractType;->field_49_dyaHdrBottom:I

    .line 893
    return-void
.end method

.method public setDyaHdrTop(I)V
    .locals 0
    .param p1, "field_48_dyaHdrTop"    # I

    .prologue
    .line 876
    iput p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/SEPAbstractType;->field_48_dyaHdrTop:I

    .line 877
    return-void
.end method

.method public setDyaLinePitch(I)V
    .locals 0
    .param p1, "field_28_dyaLinePitch"    # I

    .prologue
    .line 556
    iput p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/SEPAbstractType;->field_28_dyaLinePitch:I

    .line 557
    return-void
.end method

.method public setDyaPgn(I)V
    .locals 0
    .param p1, "field_14_dyaPgn"    # I

    .prologue
    .line 332
    iput p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/SEPAbstractType;->field_14_dyaPgn:I

    .line 333
    return-void
.end method

.method public setDyaTop(I)V
    .locals 0
    .param p1, "field_45_dyaTop"    # I

    .prologue
    .line 828
    iput p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/SEPAbstractType;->field_45_dyaTop:I

    .line 829
    return-void
.end method

.method public setDzaGutter(I)V
    .locals 0
    .param p1, "field_47_dzaGutter"    # I

    .prologue
    .line 860
    iput p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/SEPAbstractType;->field_47_dzaGutter:I

    .line 861
    return-void
.end method

.method public setFAutoPgn(Z)V
    .locals 0
    .param p1, "field_3_fAutoPgn"    # Z

    .prologue
    .line 156
    iput-boolean p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/SEPAbstractType;->field_3_fAutoPgn:Z

    .line 157
    return-void
.end method

.method public setFEndNote(Z)V
    .locals 0
    .param p1, "field_8_fEndNote"    # Z

    .prologue
    .line 236
    iput-boolean p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/SEPAbstractType;->field_8_fEndNote:Z

    .line 237
    return-void
.end method

.method public setFEvenlySpaced(Z)V
    .locals 0
    .param p1, "field_51_fEvenlySpaced"    # Z

    .prologue
    .line 924
    iput-boolean p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/SEPAbstractType;->field_51_fEvenlySpaced:Z

    .line 925
    return-void
.end method

.method public setFLBetween(Z)V
    .locals 0
    .param p1, "field_15_fLBetween"    # Z

    .prologue
    .line 348
    iput-boolean p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/SEPAbstractType;->field_15_fLBetween:Z

    .line 349
    return-void
.end method

.method public setFLayout(B)V
    .locals 0
    .param p1, "field_57_fLayout"    # B

    .prologue
    .line 1020
    iput-byte p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/SEPAbstractType;->field_57_fLayout:B

    .line 1021
    return-void
.end method

.method public setFPgnRestart(Z)V
    .locals 0
    .param p1, "field_7_fPgnRestart"    # Z

    .prologue
    .line 220
    iput-boolean p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/SEPAbstractType;->field_7_fPgnRestart:Z

    .line 221
    return-void
.end method

.method public setFPropMark(Z)V
    .locals 0
    .param p1, "field_24_fPropMark"    # Z

    .prologue
    .line 492
    iput-boolean p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/SEPAbstractType;->field_24_fPropMark:Z

    .line 493
    return-void
.end method

.method public setFTitlePage(Z)V
    .locals 0
    .param p1, "field_2_fTitlePage"    # Z

    .prologue
    .line 140
    iput-boolean p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/SEPAbstractType;->field_2_fTitlePage:Z

    .line 141
    return-void
.end method

.method public setFUnlocked(Z)V
    .locals 0
    .param p1, "field_5_fUnlocked"    # Z

    .prologue
    .line 188
    iput-boolean p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/SEPAbstractType;->field_5_fUnlocked:Z

    .line 189
    return-void
.end method

.method public setGrpfIhdt(B)V
    .locals 0
    .param p1, "field_10_grpfIhdt"    # B

    .prologue
    .line 268
    iput-byte p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/SEPAbstractType;->field_10_grpfIhdt:B

    .line 269
    return-void
.end method

.method public setIHeadingPgn(B)V
    .locals 0
    .param p1, "field_32_iHeadingPgn"    # B

    .prologue
    .line 620
    iput-byte p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/SEPAbstractType;->field_32_iHeadingPgn:B

    .line 621
    return-void
.end method

.method public setIbstPropRMark(I)V
    .locals 0
    .param p1, "field_25_ibstPropRMark"    # I

    .prologue
    .line 508
    iput p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/SEPAbstractType;->field_25_ibstPropRMark:I

    .line 509
    return-void
.end method

.method public setLnc(B)V
    .locals 0
    .param p1, "field_9_lnc"    # B

    .prologue
    .line 252
    iput-byte p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/SEPAbstractType;->field_9_lnc:B

    .line 253
    return-void
.end method

.method public setLnnMin(I)V
    .locals 0
    .param p1, "field_34_lnnMin"    # I

    .prologue
    .line 652
    iput p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/SEPAbstractType;->field_34_lnnMin:I

    .line 653
    return-void
.end method

.method public setNLnnMod(I)V
    .locals 0
    .param p1, "field_11_nLnnMod"    # I

    .prologue
    .line 284
    iput p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/SEPAbstractType;->field_11_nLnnMod:I

    .line 285
    return-void
.end method

.method public setNfcPgn(B)V
    .locals 0
    .param p1, "field_4_nfcPgn"    # B

    .prologue
    .line 172
    iput-byte p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/SEPAbstractType;->field_4_nfcPgn:B

    .line 173
    return-void
.end method

.method public setOlstAnm([B)V
    .locals 0
    .param p1, "field_59_olstAnm"    # [B

    .prologue
    .line 1052
    iput-object p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/SEPAbstractType;->field_59_olstAnm:[B

    .line 1053
    return-void
.end method

.method public setPgbProp(I)V
    .locals 0
    .param p1, "field_37_pgbProp"    # I

    .prologue
    .line 700
    iput p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/SEPAbstractType;->field_37_pgbProp:I

    .line 701
    return-void
.end method

.method public setPgnStart(I)V
    .locals 0
    .param p1, "field_33_pgnStart"    # I

    .prologue
    .line 636
    iput p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/SEPAbstractType;->field_33_pgnStart:I

    .line 637
    return-void
.end method

.method public setRgdxaColumn([I)V
    .locals 0
    .param p1, "field_54_rgdxaColumn"    # [I

    .prologue
    .line 972
    iput-object p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/SEPAbstractType;->field_54_rgdxaColumn:[I

    .line 973
    return-void
.end method

.method public setUnused2(I)V
    .locals 0
    .param p1, "field_30_unused2"    # I

    .prologue
    .line 588
    iput p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/SEPAbstractType;->field_30_unused2:I

    .line 589
    return-void
.end method

.method public setUnused3(S)V
    .locals 0
    .param p1, "field_36_unused3"    # S

    .prologue
    .line 684
    iput-short p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/SEPAbstractType;->field_36_unused3:S

    .line 685
    return-void
.end method

.method public setUnused4(S)V
    .locals 0
    .param p1, "field_38_unused4"    # S

    .prologue
    .line 716
    iput-short p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/SEPAbstractType;->field_38_unused4:S

    .line 717
    return-void
.end method

.method public setUnused5(B)V
    .locals 0
    .param p1, "field_52_unused5"    # B

    .prologue
    .line 940
    iput-byte p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/SEPAbstractType;->field_52_unused5:B

    .line 941
    return-void
.end method

.method public setUnused6(S)V
    .locals 0
    .param p1, "field_58_unused6"    # S

    .prologue
    .line 1036
    iput-short p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/SEPAbstractType;->field_58_unused6:S

    .line 1037
    return-void
.end method

.method public setVjc(B)V
    .locals 0
    .param p1, "field_16_vjc"    # B

    .prologue
    .line 364
    iput-byte p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/SEPAbstractType;->field_16_vjc:B

    .line 365
    return-void
.end method

.method public setWTextFlow(I)V
    .locals 0
    .param p1, "field_35_wTextFlow"    # I

    .prologue
    .line 668
    iput p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/SEPAbstractType;->field_35_wTextFlow:I

    .line 669
    return-void
.end method

.method public setXaPage(I)V
    .locals 0
    .param p1, "field_39_xaPage"    # I

    .prologue
    .line 732
    iput p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/SEPAbstractType;->field_39_xaPage:I

    .line 733
    return-void
.end method

.method public setXaPageNUp(I)V
    .locals 0
    .param p1, "field_41_xaPageNUp"    # I

    .prologue
    .line 764
    iput p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/SEPAbstractType;->field_41_xaPageNUp:I

    .line 765
    return-void
.end method

.method public setYaPage(I)V
    .locals 0
    .param p1, "field_40_yaPage"    # I

    .prologue
    .line 748
    iput p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/SEPAbstractType;->field_40_yaPage:I

    .line 749
    return-void
.end method

.method public setYaPageNUp(I)V
    .locals 0
    .param p1, "field_42_yaPageNUp"    # I

    .prologue
    .line 780
    iput p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/SEPAbstractType;->field_42_yaPageNUp:I

    .line 781
    return-void
.end method
