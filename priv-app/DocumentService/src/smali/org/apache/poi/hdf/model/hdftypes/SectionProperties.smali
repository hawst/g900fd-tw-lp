.class public final Lorg/apache/poi/hdf/model/hdftypes/SectionProperties;
.super Lorg/apache/poi/hdf/model/hdftypes/definitions/SEPAbstractType;
.source "SectionProperties.java"

# interfaces
.implements Lorg/apache/poi/hdf/model/hdftypes/HDFType;


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 5

    .prologue
    const/16 v4, 0x708

    const/16 v3, 0x5a0

    const/4 v2, 0x1

    const/16 v1, 0x2d0

    .line 81
    invoke-direct {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/SEPAbstractType;-><init>()V

    .line 83
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lorg/apache/poi/hdf/model/hdftypes/SectionProperties;->setBkc(B)V

    .line 84
    invoke-virtual {p0, v1}, Lorg/apache/poi/hdf/model/hdftypes/SectionProperties;->setDyaPgn(I)V

    .line 85
    invoke-virtual {p0, v1}, Lorg/apache/poi/hdf/model/hdftypes/SectionProperties;->setDxaPgn(I)V

    .line 86
    invoke-virtual {p0, v2}, Lorg/apache/poi/hdf/model/hdftypes/SectionProperties;->setFEndNote(Z)V

    .line 87
    invoke-virtual {p0, v2}, Lorg/apache/poi/hdf/model/hdftypes/SectionProperties;->setFEvenlySpaced(Z)V

    .line 88
    const/16 v0, 0x2fd0

    invoke-virtual {p0, v0}, Lorg/apache/poi/hdf/model/hdftypes/SectionProperties;->setXaPage(I)V

    .line 89
    const/16 v0, 0x3de0

    invoke-virtual {p0, v0}, Lorg/apache/poi/hdf/model/hdftypes/SectionProperties;->setYaPage(I)V

    .line 90
    invoke-virtual {p0, v1}, Lorg/apache/poi/hdf/model/hdftypes/SectionProperties;->setDyaHdrTop(I)V

    .line 91
    invoke-virtual {p0, v1}, Lorg/apache/poi/hdf/model/hdftypes/SectionProperties;->setDyaHdrBottom(I)V

    .line 92
    invoke-virtual {p0, v2}, Lorg/apache/poi/hdf/model/hdftypes/SectionProperties;->setDmOrientPage(B)V

    .line 93
    invoke-virtual {p0, v1}, Lorg/apache/poi/hdf/model/hdftypes/SectionProperties;->setDxaColumns(I)V

    .line 94
    invoke-virtual {p0, v3}, Lorg/apache/poi/hdf/model/hdftypes/SectionProperties;->setDyaTop(I)V

    .line 95
    invoke-virtual {p0, v4}, Lorg/apache/poi/hdf/model/hdftypes/SectionProperties;->setDxaLeft(I)V

    .line 96
    invoke-virtual {p0, v3}, Lorg/apache/poi/hdf/model/hdftypes/SectionProperties;->setDyaBottom(I)V

    .line 97
    invoke-virtual {p0, v4}, Lorg/apache/poi/hdf/model/hdftypes/SectionProperties;->setDxaRight(I)V

    .line 98
    invoke-virtual {p0, v2}, Lorg/apache/poi/hdf/model/hdftypes/SectionProperties;->setPgnStart(I)V

    .line 100
    return-void
.end method
