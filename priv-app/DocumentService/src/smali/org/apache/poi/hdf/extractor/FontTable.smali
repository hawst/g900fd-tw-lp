.class public final Lorg/apache/poi/hdf/extractor/FontTable;
.super Ljava/lang/Object;
.source "FontTable.java"


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field fontNames:[Ljava/lang/String;


# direct methods
.method public constructor <init>([B)V
    .locals 9
    .param p1, "fontTable"    # [B

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    const/4 v7, 0x0

    invoke-static {p1, v7}, Lorg/apache/poi/hdf/extractor/Utils;->convertBytesToShort([BI)S

    move-result v5

    .line 33
    .local v5, "size":I
    new-array v7, v5, [Ljava/lang/String;

    iput-object v7, p0, Lorg/apache/poi/hdf/extractor/FontTable;->fontNames:[Ljava/lang/String;

    .line 35
    const/4 v1, 0x4

    .line 36
    .local v1, "currentIndex":I
    const/4 v6, 0x0

    .local v6, "x":I
    :goto_0
    if-lt v6, v5, :cond_0

    .line 58
    return-void

    .line 38
    :cond_0
    aget-byte v2, p1, v1

    .line 40
    .local v2, "ffnLength":B
    add-int/lit8 v4, v1, 0x28

    .line 41
    .local v4, "nameOffset":I
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    .line 42
    .local v3, "nameBuf":Ljava/lang/StringBuffer;
    invoke-static {p1, v4}, Lorg/apache/poi/hdf/extractor/Utils;->getUnicodeCharacter([BI)C

    move-result v0

    .line 43
    .local v0, "ch":C
    :goto_1
    if-nez v0, :cond_2

    .line 49
    iget-object v7, p0, Lorg/apache/poi/hdf/extractor/FontTable;->fontNames:[Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v6

    .line 50
    iget-object v7, p0, Lorg/apache/poi/hdf/extractor/FontTable;->fontNames:[Ljava/lang/String;

    aget-object v7, v7, v6

    const-string/jumbo v8, "Times"

    invoke-virtual {v7, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 52
    iget-object v7, p0, Lorg/apache/poi/hdf/extractor/FontTable;->fontNames:[Ljava/lang/String;

    const-string/jumbo v8, "Times"

    aput-object v8, v7, v6

    .line 55
    :cond_1
    add-int/lit8 v7, v2, 0x1

    add-int/2addr v1, v7

    .line 36
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 45
    :cond_2
    invoke-virtual {v3, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 46
    add-int/lit8 v4, v4, 0x2

    .line 47
    invoke-static {p1, v4}, Lorg/apache/poi/hdf/extractor/Utils;->getUnicodeCharacter([BI)C

    move-result v0

    goto :goto_1
.end method


# virtual methods
.method public getFont(I)Ljava/lang/String;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 61
    iget-object v0, p0, Lorg/apache/poi/hdf/extractor/FontTable;->fontNames:[Ljava/lang/String;

    aget-object v0, v0, p1

    return-object v0
.end method
