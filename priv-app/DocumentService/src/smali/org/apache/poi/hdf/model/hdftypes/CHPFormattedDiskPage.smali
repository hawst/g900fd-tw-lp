.class public final Lorg/apache/poi/hdf/model/hdftypes/CHPFormattedDiskPage;
.super Lorg/apache/poi/hdf/model/hdftypes/FormattedDiskPage;
.source "CHPFormattedDiskPage.java"


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# direct methods
.method public constructor <init>([B)V
    .locals 0
    .param p1, "fkp"    # [B

    .prologue
    .line 51
    invoke-direct {p0, p1}, Lorg/apache/poi/hdf/model/hdftypes/FormattedDiskPage;-><init>([B)V

    .line 52
    return-void
.end method


# virtual methods
.method public getGrpprl(I)[B
    .locals 6
    .param p1, "index"    # I

    .prologue
    const/4 v5, 0x0

    .line 62
    iget-object v3, p0, Lorg/apache/poi/hdf/model/hdftypes/CHPFormattedDiskPage;->_fkp:[B

    iget v4, p0, Lorg/apache/poi/hdf/model/hdftypes/CHPFormattedDiskPage;->_crun:I

    add-int/lit8 v4, v4, 0x1

    mul-int/lit8 v4, v4, 0x4

    add-int/2addr v4, p1

    invoke-static {v3, v4}, Lorg/apache/poi/util/LittleEndian;->getUnsignedByte([BI)I

    move-result v3

    mul-int/lit8 v1, v3, 0x2

    .line 65
    .local v1, "chpxOffset":I
    if-nez v1, :cond_0

    .line 67
    new-array v0, v5, [B

    .line 76
    :goto_0
    return-object v0

    .line 71
    :cond_0
    iget-object v3, p0, Lorg/apache/poi/hdf/model/hdftypes/CHPFormattedDiskPage;->_fkp:[B

    invoke-static {v3, v1}, Lorg/apache/poi/util/LittleEndian;->getUnsignedByte([BI)I

    move-result v2

    .line 73
    .local v2, "size":I
    new-array v0, v2, [B

    .line 75
    .local v0, "chpx":[B
    iget-object v3, p0, Lorg/apache/poi/hdf/model/hdftypes/CHPFormattedDiskPage;->_fkp:[B

    add-int/lit8 v1, v1, 0x1

    invoke-static {v3, v1, v0, v5, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_0
.end method
