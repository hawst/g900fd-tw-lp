.class public final Lorg/apache/poi/hdf/event/EventBridge;
.super Ljava/lang/Object;
.source "EventBridge.java"

# interfaces
.implements Lorg/apache/poi/hdf/event/HDFLowLevelParsingListener;


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field private static FOOTER_EVEN_INDEX:I

.field private static FOOTER_FIRST_INDEX:I

.field private static FOOTER_ODD_INDEX:I

.field private static HEADER_EVEN_INDEX:I

.field private static HEADER_FIRST_INDEX:I

.field private static HEADER_ODD_INDEX:I


# instance fields
.field private _beginHeaders:Z

.field _ccpFtn:I

.field _ccpText:I

.field _currentStd:Lorg/apache/poi/hdf/model/hdftypes/StyleDescription;

.field _dop:Lorg/apache/poi/hdf/model/hdftypes/DocumentProperties;

.field private _endHoldIndex:I

.field _fcMin:I

.field _hdrCharacterRuns:Lorg/apache/poi/hdf/model/util/BTreeSet;

.field _hdrOffset:I

.field _hdrParagraphs:Lorg/apache/poi/hdf/model/util/BTreeSet;

.field _hdrSections:Lorg/apache/poi/hdf/model/util/BTreeSet;

.field _hdrSize:I

.field _hdrs:Ljava/util/ArrayList;

.field private _holdParagraph:Z

.field _listTables:Lorg/apache/poi/hdf/model/hdftypes/ListTables;

.field _listener:Lorg/apache/poi/hdf/event/HDFParsingListener;

.field _mainDocument:[B

.field private _onHold:Ljava/util/ArrayList;

.field _sectionCounter:I

.field _stsh:Lorg/apache/poi/hdf/model/hdftypes/StyleSheet;

.field _tableStream:[B

.field _text:Lorg/apache/poi/hdf/model/util/BTreeSet;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    const/4 v0, 0x0

    sput v0, Lorg/apache/poi/hdf/event/EventBridge;->HEADER_EVEN_INDEX:I

    .line 34
    const/4 v0, 0x1

    sput v0, Lorg/apache/poi/hdf/event/EventBridge;->HEADER_ODD_INDEX:I

    .line 35
    const/4 v0, 0x2

    sput v0, Lorg/apache/poi/hdf/event/EventBridge;->FOOTER_EVEN_INDEX:I

    .line 36
    const/4 v0, 0x3

    sput v0, Lorg/apache/poi/hdf/event/EventBridge;->FOOTER_ODD_INDEX:I

    .line 37
    const/4 v0, 0x4

    sput v0, Lorg/apache/poi/hdf/event/EventBridge;->HEADER_FIRST_INDEX:I

    .line 38
    const/4 v0, 0x5

    sput v0, Lorg/apache/poi/hdf/event/EventBridge;->FOOTER_FIRST_INDEX:I

    return-void
.end method

.method public constructor <init>(Lorg/apache/poi/hdf/event/HDFParsingListener;)V
    .locals 1
    .param p1, "listener"    # Lorg/apache/poi/hdf/event/HDFParsingListener;

    .prologue
    .line 80
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66
    new-instance v0, Lorg/apache/poi/hdf/model/util/BTreeSet;

    invoke-direct {v0}, Lorg/apache/poi/hdf/model/util/BTreeSet;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/hdf/event/EventBridge;->_text:Lorg/apache/poi/hdf/model/util/BTreeSet;

    .line 69
    new-instance v0, Lorg/apache/poi/hdf/model/util/BTreeSet;

    invoke-direct {v0}, Lorg/apache/poi/hdf/model/util/BTreeSet;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/hdf/event/EventBridge;->_hdrSections:Lorg/apache/poi/hdf/model/util/BTreeSet;

    .line 70
    new-instance v0, Lorg/apache/poi/hdf/model/util/BTreeSet;

    invoke-direct {v0}, Lorg/apache/poi/hdf/model/util/BTreeSet;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/hdf/event/EventBridge;->_hdrParagraphs:Lorg/apache/poi/hdf/model/util/BTreeSet;

    .line 71
    new-instance v0, Lorg/apache/poi/hdf/model/util/BTreeSet;

    invoke-direct {v0}, Lorg/apache/poi/hdf/model/util/BTreeSet;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/hdf/event/EventBridge;->_hdrCharacterRuns:Lorg/apache/poi/hdf/model/util/BTreeSet;

    .line 73
    const/4 v0, 0x1

    iput v0, p0, Lorg/apache/poi/hdf/event/EventBridge;->_sectionCounter:I

    .line 74
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/hdf/event/EventBridge;->_hdrs:Ljava/util/ArrayList;

    .line 76
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/poi/hdf/event/EventBridge;->_holdParagraph:Z

    .line 77
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/poi/hdf/event/EventBridge;->_endHoldIndex:I

    .line 82
    iput-object p1, p0, Lorg/apache/poi/hdf/event/EventBridge;->_listener:Lorg/apache/poi/hdf/event/HDFParsingListener;

    .line 83
    return-void
.end method

.method private createSectionHdrFtr(II)Lorg/apache/poi/hdf/model/hdftypes/HeaderFooter;
    .locals 7
    .param p1, "index"    # I
    .param p2, "type"    # I

    .prologue
    const/4 v6, 0x0

    .line 394
    iget v4, p0, Lorg/apache/poi/hdf/event/EventBridge;->_hdrSize:I

    const/16 v5, 0x32

    if-ge v4, v5, :cond_1

    .line 396
    new-instance v2, Lorg/apache/poi/hdf/model/hdftypes/HeaderFooter;

    invoke-direct {v2, v6, v6, v6}, Lorg/apache/poi/hdf/model/hdftypes/HeaderFooter;-><init>(III)V

    .line 433
    :cond_0
    :goto_0
    return-object v2

    .line 399
    :cond_1
    iget v4, p0, Lorg/apache/poi/hdf/event/EventBridge;->_fcMin:I

    iget v5, p0, Lorg/apache/poi/hdf/event/EventBridge;->_ccpText:I

    add-int/2addr v4, v5

    iget v5, p0, Lorg/apache/poi/hdf/event/EventBridge;->_ccpFtn:I

    add-int v3, v4, v5

    .line 400
    .local v3, "start":I
    move v1, v3

    .line 401
    .local v1, "end":I
    const/4 v0, 0x0

    .line 403
    .local v0, "arrayIndex":I
    packed-switch p2, :pswitch_data_0

    .line 424
    :goto_1
    iget-object v4, p0, Lorg/apache/poi/hdf/event/EventBridge;->_tableStream:[B

    iget v5, p0, Lorg/apache/poi/hdf/event/EventBridge;->_hdrOffset:I

    mul-int/lit8 v6, v0, 0x4

    add-int/2addr v5, v6

    invoke-static {v4, v5}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v4

    add-int/2addr v3, v4

    .line 425
    iget-object v4, p0, Lorg/apache/poi/hdf/event/EventBridge;->_tableStream:[B

    iget v5, p0, Lorg/apache/poi/hdf/event/EventBridge;->_hdrOffset:I

    add-int/lit8 v6, v0, 0x1

    mul-int/lit8 v6, v6, 0x4

    add-int/2addr v5, v6

    invoke-static {v4, v5}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v4

    add-int/2addr v1, v4

    .line 427
    new-instance v2, Lorg/apache/poi/hdf/model/hdftypes/HeaderFooter;

    invoke-direct {v2, p2, v3, v1}, Lorg/apache/poi/hdf/model/hdftypes/HeaderFooter;-><init>(III)V

    .line 429
    .local v2, "retValue":Lorg/apache/poi/hdf/model/hdftypes/HeaderFooter;
    sub-int v4, v1, v3

    if-nez v4, :cond_0

    const/4 v4, 0x1

    if-le p1, v4, :cond_0

    .line 431
    add-int/lit8 v4, p1, -0x1

    invoke-direct {p0, p2, v4}, Lorg/apache/poi/hdf/event/EventBridge;->createSectionHdrFtr(II)Lorg/apache/poi/hdf/model/hdftypes/HeaderFooter;

    move-result-object v2

    goto :goto_0

    .line 406
    .end local v2    # "retValue":Lorg/apache/poi/hdf/model/hdftypes/HeaderFooter;
    :pswitch_0
    sget v4, Lorg/apache/poi/hdf/event/EventBridge;->HEADER_EVEN_INDEX:I

    mul-int/lit8 v5, p1, 0x6

    add-int v0, v4, v5

    .line 407
    goto :goto_1

    .line 409
    :pswitch_1
    sget v4, Lorg/apache/poi/hdf/event/EventBridge;->FOOTER_EVEN_INDEX:I

    mul-int/lit8 v5, p1, 0x6

    add-int v0, v4, v5

    .line 410
    goto :goto_1

    .line 412
    :pswitch_2
    sget v4, Lorg/apache/poi/hdf/event/EventBridge;->HEADER_ODD_INDEX:I

    mul-int/lit8 v5, p1, 0x6

    add-int v0, v4, v5

    .line 413
    goto :goto_1

    .line 415
    :pswitch_3
    sget v4, Lorg/apache/poi/hdf/event/EventBridge;->FOOTER_ODD_INDEX:I

    mul-int/lit8 v5, p1, 0x6

    add-int v0, v4, v5

    .line 416
    goto :goto_1

    .line 418
    :pswitch_4
    sget v4, Lorg/apache/poi/hdf/event/EventBridge;->HEADER_FIRST_INDEX:I

    mul-int/lit8 v5, p1, 0x6

    add-int v0, v4, v5

    .line 419
    goto :goto_1

    .line 421
    :pswitch_5
    sget v4, Lorg/apache/poi/hdf/event/EventBridge;->FOOTER_FIRST_INDEX:I

    mul-int/lit8 v5, p1, 0x6

    add-int v0, v4, v5

    goto :goto_1

    .line 403
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private findSectionHdrFtrs(I)[Lorg/apache/poi/hdf/model/hdftypes/HeaderFooter;
    .locals 4
    .param p1, "index"    # I

    .prologue
    .line 382
    const/4 v2, 0x6

    new-array v0, v2, [Lorg/apache/poi/hdf/model/hdftypes/HeaderFooter;

    .line 384
    .local v0, "hdrArray":[Lorg/apache/poi/hdf/model/hdftypes/HeaderFooter;
    const/4 v1, 0x1

    .local v1, "x":I
    :goto_0
    const/4 v2, 0x7

    if-lt v1, v2, :cond_0

    .line 389
    return-object v0

    .line 386
    :cond_0
    add-int/lit8 v2, v1, -0x1

    invoke-direct {p0, p1, v1}, Lorg/apache/poi/hdf/event/EventBridge;->createSectionHdrFtr(II)Lorg/apache/poi/hdf/model/hdftypes/HeaderFooter;

    move-result-object v3

    aput-object v3, v0, v2

    .line 384
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private flushHeaderProps(II)V
    .locals 24
    .param p1, "start"    # I
    .param p2, "end"    # I

    .prologue
    .line 244
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/poi/hdf/event/EventBridge;->_hdrSections:Lorg/apache/poi/hdf/model/util/BTreeSet;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v0, v0, Lorg/apache/poi/hdf/model/util/BTreeSet;->root:Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;

    move-object/from16 v23, v0

    move/from16 v0, p1

    move/from16 v1, p2

    move-object/from16 v2, v23

    invoke-static {v0, v1, v2}, Lorg/apache/poi/hdf/model/util/BTreeSet;->findProperties(IILorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;)Ljava/util/ArrayList;

    move-result-object v8

    .line 245
    .local v8, "list":Ljava/util/ArrayList;
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v19

    .line 247
    .local v19, "size":I
    const/16 v20, 0x0

    .local v20, "x":I
    :goto_0
    move/from16 v0, v20

    move/from16 v1, v19

    if-lt v0, v1, :cond_0

    .line 284
    return-void

    .line 249
    :cond_0
    move/from16 v0, v20

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lorg/apache/poi/hdf/model/hdftypes/SepxNode;

    .line 250
    .local v10, "oldNode":Lorg/apache/poi/hdf/model/hdftypes/SepxNode;
    invoke-virtual {v10}, Lorg/apache/poi/hdf/model/hdftypes/SepxNode;->getStart()I

    move-result v23

    move/from16 v0, v23

    move/from16 v1, p1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v18

    .line 251
    .local v18, "secStart":I
    invoke-virtual {v10}, Lorg/apache/poi/hdf/model/hdftypes/SepxNode;->getEnd()I

    move-result v23

    move/from16 v0, v23

    move/from16 v1, p2

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v17

    .line 256
    .local v17, "secEnd":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/poi/hdf/event/EventBridge;->_hdrParagraphs:Lorg/apache/poi/hdf/model/util/BTreeSet;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v0, v0, Lorg/apache/poi/hdf/model/util/BTreeSet;->root:Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;

    move-object/from16 v23, v0

    move/from16 v0, v18

    move/from16 v1, v17

    move-object/from16 v2, v23

    invoke-static {v0, v1, v2}, Lorg/apache/poi/hdf/model/util/BTreeSet;->findProperties(IILorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;)Ljava/util/ArrayList;

    move-result-object v13

    .line 257
    .local v13, "parList":Ljava/util/ArrayList;
    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v15

    .line 259
    .local v15, "parSize":I
    const/16 v21, 0x0

    .local v21, "y":I
    :goto_1
    move/from16 v0, v21

    if-lt v0, v15, :cond_1

    .line 247
    add-int/lit8 v20, v20, 0x1

    goto :goto_0

    .line 261
    :cond_1
    move/from16 v0, v21

    invoke-virtual {v13, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lorg/apache/poi/hdf/model/hdftypes/PapxNode;

    .line 262
    .local v11, "oldParNode":Lorg/apache/poi/hdf/model/hdftypes/PapxNode;
    invoke-virtual {v11}, Lorg/apache/poi/hdf/model/hdftypes/PapxNode;->getStart()I

    move-result v23

    move/from16 v0, v23

    move/from16 v1, v18

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v16

    .line 263
    .local v16, "parStart":I
    invoke-virtual {v11}, Lorg/apache/poi/hdf/model/hdftypes/PapxNode;->getEnd()I

    move-result v23

    move/from16 v0, v23

    move/from16 v1, v17

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v12

    .line 265
    .local v12, "parEnd":I
    new-instance v14, Lorg/apache/poi/hdf/model/hdftypes/PapxNode;

    invoke-virtual {v11}, Lorg/apache/poi/hdf/model/hdftypes/PapxNode;->getPapx()[B

    move-result-object v23

    move/from16 v0, v16

    move-object/from16 v1, v23

    invoke-direct {v14, v0, v12, v1}, Lorg/apache/poi/hdf/model/hdftypes/PapxNode;-><init>(II[B)V

    .line 266
    .local v14, "parNode":Lorg/apache/poi/hdf/model/hdftypes/PapxNode;
    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lorg/apache/poi/hdf/event/EventBridge;->paragraph(Lorg/apache/poi/hdf/model/hdftypes/PapxNode;)V

    .line 268
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/poi/hdf/event/EventBridge;->_hdrCharacterRuns:Lorg/apache/poi/hdf/model/util/BTreeSet;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v0, v0, Lorg/apache/poi/hdf/model/util/BTreeSet;->root:Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;

    move-object/from16 v23, v0

    move/from16 v0, v16

    move-object/from16 v1, v23

    invoke-static {v0, v12, v1}, Lorg/apache/poi/hdf/model/util/BTreeSet;->findProperties(IILorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;)Ljava/util/ArrayList;

    move-result-object v4

    .line 269
    .local v4, "charList":Ljava/util/ArrayList;
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v6

    .line 271
    .local v6, "charSize":I
    const/16 v22, 0x0

    .local v22, "z":I
    :goto_2
    move/from16 v0, v22

    if-lt v0, v6, :cond_2

    .line 259
    add-int/lit8 v21, v21, 0x1

    goto :goto_1

    .line 273
    :cond_2
    move/from16 v0, v22

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lorg/apache/poi/hdf/model/hdftypes/ChpxNode;

    .line 274
    .local v9, "oldCharNode":Lorg/apache/poi/hdf/model/hdftypes/ChpxNode;
    invoke-virtual {v9}, Lorg/apache/poi/hdf/model/hdftypes/ChpxNode;->getStart()I

    move-result v23

    move/from16 v0, v23

    move/from16 v1, v16

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v7

    .line 275
    .local v7, "charStart":I
    invoke-virtual {v9}, Lorg/apache/poi/hdf/model/hdftypes/ChpxNode;->getEnd()I

    move-result v23

    move/from16 v0, v23

    invoke-static {v0, v12}, Ljava/lang/Math;->min(II)I

    move-result v3

    .line 277
    .local v3, "charEnd":I
    new-instance v5, Lorg/apache/poi/hdf/model/hdftypes/ChpxNode;

    invoke-virtual {v9}, Lorg/apache/poi/hdf/model/hdftypes/ChpxNode;->getChpx()[B

    move-result-object v23

    move-object/from16 v0, v23

    invoke-direct {v5, v7, v3, v0}, Lorg/apache/poi/hdf/model/hdftypes/ChpxNode;-><init>(II[B)V

    .line 278
    .local v5, "charNode":Lorg/apache/poi/hdf/model/hdftypes/ChpxNode;
    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lorg/apache/poi/hdf/event/EventBridge;->characterRun(Lorg/apache/poi/hdf/model/hdftypes/ChpxNode;)V

    .line 271
    add-int/lit8 v22, v22, 0x1

    goto :goto_2
.end method

.method private flushHeldParagraph()V
    .locals 14

    .prologue
    const/4 v13, 0x0

    .line 317
    iget-object v0, p0, Lorg/apache/poi/hdf/event/EventBridge;->_onHold:Ljava/util/ArrayList;

    invoke-virtual {v0, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lorg/apache/poi/hdf/model/hdftypes/PapxNode;

    .line 318
    .local v9, "papx":Lorg/apache/poi/hdf/model/hdftypes/PapxNode;
    invoke-virtual {v9}, Lorg/apache/poi/hdf/model/hdftypes/PapxNode;->getPapx()[B

    move-result-object v6

    .line 319
    .local v6, "bytePapx":[B
    invoke-static {v6, v13}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v7

    .line 320
    .local v7, "istd":I
    iget-object v0, p0, Lorg/apache/poi/hdf/event/EventBridge;->_stsh:Lorg/apache/poi/hdf/model/hdftypes/StyleSheet;

    invoke-virtual {v0, v7}, Lorg/apache/poi/hdf/model/hdftypes/StyleSheet;->getStyleDescription(I)Lorg/apache/poi/hdf/model/hdftypes/StyleDescription;

    move-result-object v11

    .line 322
    .local v11, "std":Lorg/apache/poi/hdf/model/hdftypes/StyleDescription;
    iget-object v0, p0, Lorg/apache/poi/hdf/event/EventBridge;->_currentStd:Lorg/apache/poi/hdf/model/hdftypes/StyleDescription;

    invoke-virtual {v0}, Lorg/apache/poi/hdf/model/hdftypes/StyleDescription;->getPAP()Lorg/apache/poi/hdf/model/hdftypes/ParagraphProperties;

    move-result-object v0

    iget-object v4, p0, Lorg/apache/poi/hdf/event/EventBridge;->_stsh:Lorg/apache/poi/hdf/model/hdftypes/StyleSheet;

    invoke-static {v6, v0, v4}, Lorg/apache/poi/hdf/model/hdftypes/StyleSheet;->uncompressProperty([BLjava/lang/Object;Lorg/apache/poi/hdf/model/hdftypes/StyleSheet;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/poi/hdf/model/hdftypes/ParagraphProperties;

    .line 323
    .local v3, "pap":Lorg/apache/poi/hdf/model/hdftypes/ParagraphProperties;
    iget-object v0, p0, Lorg/apache/poi/hdf/event/EventBridge;->_listTables:Lorg/apache/poi/hdf/model/hdftypes/ListTables;

    invoke-virtual {v3}, Lorg/apache/poi/hdf/model/hdftypes/ParagraphProperties;->getIlfo()I

    move-result v4

    invoke-virtual {v3}, Lorg/apache/poi/hdf/model/hdftypes/ParagraphProperties;->getIlvl()B

    move-result v5

    invoke-virtual {v0, v4, v5}, Lorg/apache/poi/hdf/model/hdftypes/ListTables;->getLevel(II)Lorg/apache/poi/hdf/model/hdftypes/LVL;

    move-result-object v8

    .line 324
    .local v8, "lvl":Lorg/apache/poi/hdf/model/hdftypes/LVL;
    iget-object v0, v8, Lorg/apache/poi/hdf/model/hdftypes/LVL;->_papx:[B

    iget-object v4, p0, Lorg/apache/poi/hdf/event/EventBridge;->_stsh:Lorg/apache/poi/hdf/model/hdftypes/StyleSheet;

    invoke-static {v0, v3, v4, v13}, Lorg/apache/poi/hdf/model/hdftypes/StyleSheet;->uncompressProperty([BLjava/lang/Object;Lorg/apache/poi/hdf/model/hdftypes/StyleSheet;Z)Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "pap":Lorg/apache/poi/hdf/model/hdftypes/ParagraphProperties;
    check-cast v3, Lorg/apache/poi/hdf/model/hdftypes/ParagraphProperties;

    .line 326
    .restart local v3    # "pap":Lorg/apache/poi/hdf/model/hdftypes/ParagraphProperties;
    iget-object v0, p0, Lorg/apache/poi/hdf/event/EventBridge;->_onHold:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v10, v0, -0x1

    .line 328
    .local v10, "size":I
    iget-object v0, p0, Lorg/apache/poi/hdf/event/EventBridge;->_onHold:Ljava/util/ArrayList;

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hdf/model/hdftypes/ChpxNode;

    invoke-virtual {v0}, Lorg/apache/poi/hdf/model/hdftypes/ChpxNode;->getChpx()[B

    move-result-object v0

    invoke-virtual {v11}, Lorg/apache/poi/hdf/model/hdftypes/StyleDescription;->getCHP()Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;

    move-result-object v4

    iget-object v5, p0, Lorg/apache/poi/hdf/event/EventBridge;->_stsh:Lorg/apache/poi/hdf/model/hdftypes/StyleSheet;

    invoke-static {v0, v4, v5}, Lorg/apache/poi/hdf/model/hdftypes/StyleSheet;->uncompressProperty([BLjava/lang/Object;Lorg/apache/poi/hdf/model/hdftypes/StyleSheet;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;

    .line 330
    .local v2, "numChp":Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;
    iget-object v0, v8, Lorg/apache/poi/hdf/model/hdftypes/LVL;->_chpx:[B

    iget-object v4, p0, Lorg/apache/poi/hdf/event/EventBridge;->_stsh:Lorg/apache/poi/hdf/model/hdftypes/StyleSheet;

    invoke-static {v0, v2, v4}, Lorg/apache/poi/hdf/model/hdftypes/StyleSheet;->uncompressProperty([BLjava/lang/Object;Lorg/apache/poi/hdf/model/hdftypes/StyleSheet;)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "numChp":Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;
    check-cast v2, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;

    .line 331
    .restart local v2    # "numChp":Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;
    invoke-direct {p0, v8, v3}, Lorg/apache/poi/hdf/event/EventBridge;->getBulletText(Lorg/apache/poi/hdf/model/hdftypes/LVL;Lorg/apache/poi/hdf/model/hdftypes/ParagraphProperties;)Ljava/lang/String;

    move-result-object v1

    .line 333
    .local v1, "bulletText":Ljava/lang/String;
    iget-object v0, p0, Lorg/apache/poi/hdf/event/EventBridge;->_listener:Lorg/apache/poi/hdf/event/HDFParsingListener;

    invoke-virtual {v9}, Lorg/apache/poi/hdf/model/hdftypes/PapxNode;->getStart()I

    move-result v4

    iget v5, p0, Lorg/apache/poi/hdf/event/EventBridge;->_fcMin:I

    sub-int/2addr v4, v5

    invoke-virtual {v9}, Lorg/apache/poi/hdf/model/hdftypes/PapxNode;->getEnd()I

    move-result v5

    iget v13, p0, Lorg/apache/poi/hdf/event/EventBridge;->_fcMin:I

    sub-int/2addr v5, v13

    invoke-interface/range {v0 .. v5}, Lorg/apache/poi/hdf/event/HDFParsingListener;->listEntry(Ljava/lang/String;Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;Lorg/apache/poi/hdf/model/hdftypes/ParagraphProperties;II)V

    .line 334
    const/4 v12, 0x1

    .local v12, "x":I
    :goto_0
    if-le v12, v10, :cond_0

    .line 339
    return-void

    .line 336
    :cond_0
    iget-object v0, p0, Lorg/apache/poi/hdf/event/EventBridge;->_onHold:Ljava/util/ArrayList;

    invoke-virtual {v0, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hdf/model/hdftypes/ChpxNode;

    invoke-virtual {p0, v0}, Lorg/apache/poi/hdf/event/EventBridge;->characterRun(Lorg/apache/poi/hdf/model/hdftypes/ChpxNode;)V

    .line 334
    add-int/lit8 v12, v12, 0x1

    goto :goto_0
.end method

.method private getBulletText(Lorg/apache/poi/hdf/model/hdftypes/LVL;Lorg/apache/poi/hdf/model/hdftypes/ParagraphProperties;)Ljava/lang/String;
    .locals 8
    .param p1, "lvl"    # Lorg/apache/poi/hdf/model/hdftypes/LVL;
    .param p2, "pap"    # Lorg/apache/poi/hdf/model/hdftypes/ParagraphProperties;

    .prologue
    const/16 v7, 0x9

    .line 343
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 344
    .local v0, "bulletBuffer":Ljava/lang/StringBuffer;
    const/4 v3, 0x0

    .local v3, "x":I
    :goto_0
    iget-object v4, p1, Lorg/apache/poi/hdf/model/hdftypes/LVL;->_xst:[C

    array-length v4, v4

    if-lt v3, v4, :cond_0

    .line 368
    iget-byte v4, p1, Lorg/apache/poi/hdf/model/hdftypes/LVL;->_ixchFollow:B

    packed-switch v4, :pswitch_data_0

    .line 377
    :goto_1
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4

    .line 346
    :cond_0
    iget-object v4, p1, Lorg/apache/poi/hdf/model/hdftypes/LVL;->_xst:[C

    aget-char v4, v4, v3

    if-ge v4, v7, :cond_3

    .line 348
    iget-object v4, p0, Lorg/apache/poi/hdf/event/EventBridge;->_listTables:Lorg/apache/poi/hdf/model/hdftypes/ListTables;

    invoke-virtual {p2}, Lorg/apache/poi/hdf/model/hdftypes/ParagraphProperties;->getIlfo()I

    move-result v5

    iget-object v6, p1, Lorg/apache/poi/hdf/model/hdftypes/LVL;->_xst:[C

    aget-char v6, v6, v3

    invoke-virtual {v4, v5, v6}, Lorg/apache/poi/hdf/model/hdftypes/ListTables;->getLevel(II)Lorg/apache/poi/hdf/model/hdftypes/LVL;

    move-result-object v2

    .line 349
    .local v2, "numLevel":Lorg/apache/poi/hdf/model/hdftypes/LVL;
    iget v1, v2, Lorg/apache/poi/hdf/model/hdftypes/LVL;->_iStartAt:I

    .line 350
    .local v1, "num":I
    if-ne p1, v2, :cond_2

    .line 352
    iget v4, v2, Lorg/apache/poi/hdf/model/hdftypes/LVL;->_iStartAt:I

    add-int/lit8 v4, v4, 0x1

    iput v4, v2, Lorg/apache/poi/hdf/model/hdftypes/LVL;->_iStartAt:I

    .line 358
    :cond_1
    :goto_2
    iget-byte v4, p1, Lorg/apache/poi/hdf/model/hdftypes/LVL;->_nfc:B

    invoke-static {v1, v4}, Lorg/apache/poi/hdf/model/util/NumberFormatter;->getNumber(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 344
    .end local v1    # "num":I
    .end local v2    # "numLevel":Lorg/apache/poi/hdf/model/hdftypes/LVL;
    :goto_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 354
    .restart local v1    # "num":I
    .restart local v2    # "numLevel":Lorg/apache/poi/hdf/model/hdftypes/LVL;
    :cond_2
    const/4 v4, 0x1

    if-le v1, v4, :cond_1

    .line 356
    add-int/lit8 v1, v1, -0x1

    goto :goto_2

    .line 363
    .end local v1    # "num":I
    .end local v2    # "numLevel":Lorg/apache/poi/hdf/model/hdftypes/LVL;
    :cond_3
    iget-object v4, p1, Lorg/apache/poi/hdf/model/hdftypes/LVL;->_xst:[C

    aget-char v4, v4, v3

    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_3

    .line 371
    :pswitch_0
    invoke-virtual {v0, v7}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_1

    .line 374
    :pswitch_1
    const/16 v4, 0x20

    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_1

    .line 368
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private getTextFromNodes(Ljava/util/ArrayList;II)Ljava/lang/String;
    .locals 8
    .param p1, "list"    # Ljava/util/ArrayList;
    .param p2, "start"    # I
    .param p3, "end"    # I

    .prologue
    .line 287
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v4

    .line 289
    .local v4, "size":I
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    .line 291
    .local v3, "sb":Ljava/lang/StringBuffer;
    const/4 v5, 0x0

    .local v5, "x":I
    :goto_0
    if-lt v5, v4, :cond_0

    .line 312
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v7

    return-object v7

    .line 293
    :cond_0
    invoke-virtual {p1, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/poi/hdf/model/hdftypes/TextPiece;

    .line 294
    .local v2, "piece":Lorg/apache/poi/hdf/model/hdftypes/TextPiece;
    invoke-virtual {v2}, Lorg/apache/poi/hdf/model/hdftypes/TextPiece;->getStart()I

    move-result v7

    invoke-static {p2, v7}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 295
    .local v1, "charStart":I
    invoke-virtual {v2}, Lorg/apache/poi/hdf/model/hdftypes/TextPiece;->getEnd()I

    move-result v7

    invoke-static {p3, v7}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 297
    .local v0, "charEnd":I
    invoke-virtual {v2}, Lorg/apache/poi/hdf/model/hdftypes/TextPiece;->usesUnicode()Z

    move-result v7

    if-eqz v7, :cond_3

    .line 299
    move v6, v1

    .local v6, "y":I
    :goto_1
    if-lt v6, v0, :cond_2

    .line 291
    :cond_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 301
    :cond_2
    iget-object v7, p0, Lorg/apache/poi/hdf/event/EventBridge;->_mainDocument:[B

    invoke-static {v7, v6}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v7

    int-to-char v7, v7

    invoke-virtual {v3, v7}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 299
    add-int/lit8 v6, v6, 0x2

    goto :goto_1

    .line 306
    .end local v6    # "y":I
    :cond_3
    move v6, v1

    .restart local v6    # "y":I
    :goto_2
    if-ge v6, v0, :cond_1

    .line 308
    iget-object v7, p0, Lorg/apache/poi/hdf/event/EventBridge;->_mainDocument:[B

    aget-byte v7, v7, v6

    invoke-virtual {v3, v7}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 306
    add-int/lit8 v6, v6, 0x1

    goto :goto_2
.end method


# virtual methods
.method public bodySection(Lorg/apache/poi/hdf/model/hdftypes/SepxNode;)V
    .locals 6
    .param p1, "sepx"    # Lorg/apache/poi/hdf/model/hdftypes/SepxNode;

    .prologue
    .line 106
    invoke-virtual {p1}, Lorg/apache/poi/hdf/model/hdftypes/SepxNode;->getSepx()[B

    move-result-object v2

    new-instance v3, Lorg/apache/poi/hdf/model/hdftypes/SectionProperties;

    invoke-direct {v3}, Lorg/apache/poi/hdf/model/hdftypes/SectionProperties;-><init>()V

    iget-object v4, p0, Lorg/apache/poi/hdf/event/EventBridge;->_stsh:Lorg/apache/poi/hdf/model/hdftypes/StyleSheet;

    invoke-static {v2, v3, v4}, Lorg/apache/poi/hdf/model/hdftypes/StyleSheet;->uncompressProperty([BLjava/lang/Object;Lorg/apache/poi/hdf/model/hdftypes/StyleSheet;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/hdf/model/hdftypes/SectionProperties;

    .line 107
    .local v1, "sep":Lorg/apache/poi/hdf/model/hdftypes/SectionProperties;
    iget v2, p0, Lorg/apache/poi/hdf/event/EventBridge;->_sectionCounter:I

    invoke-direct {p0, v2}, Lorg/apache/poi/hdf/event/EventBridge;->findSectionHdrFtrs(I)[Lorg/apache/poi/hdf/model/hdftypes/HeaderFooter;

    move-result-object v0

    .line 108
    .local v0, "hdrArray":[Lorg/apache/poi/hdf/model/hdftypes/HeaderFooter;
    iget-object v2, p0, Lorg/apache/poi/hdf/event/EventBridge;->_hdrs:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 109
    iget-object v2, p0, Lorg/apache/poi/hdf/event/EventBridge;->_listener:Lorg/apache/poi/hdf/event/HDFParsingListener;

    invoke-virtual {p1}, Lorg/apache/poi/hdf/model/hdftypes/SepxNode;->getStart()I

    move-result v3

    iget v4, p0, Lorg/apache/poi/hdf/event/EventBridge;->_fcMin:I

    sub-int/2addr v3, v4

    invoke-virtual {p1}, Lorg/apache/poi/hdf/model/hdftypes/SepxNode;->getEnd()I

    move-result v4

    iget v5, p0, Lorg/apache/poi/hdf/event/EventBridge;->_fcMin:I

    sub-int/2addr v4, v5

    invoke-interface {v2, v1, v3, v4}, Lorg/apache/poi/hdf/event/HDFParsingListener;->section(Lorg/apache/poi/hdf/model/hdftypes/SectionProperties;II)V

    .line 110
    iget v2, p0, Lorg/apache/poi/hdf/event/EventBridge;->_sectionCounter:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lorg/apache/poi/hdf/event/EventBridge;->_sectionCounter:I

    .line 111
    return-void
.end method

.method public characterRun(Lorg/apache/poi/hdf/model/hdftypes/ChpxNode;)V
    .locals 9
    .param p1, "chpx"    # Lorg/apache/poi/hdf/model/hdftypes/ChpxNode;

    .prologue
    .line 197
    iget-boolean v6, p0, Lorg/apache/poi/hdf/event/EventBridge;->_beginHeaders:Z

    if-eqz v6, :cond_0

    .line 199
    iget-object v6, p0, Lorg/apache/poi/hdf/event/EventBridge;->_hdrCharacterRuns:Lorg/apache/poi/hdf/model/util/BTreeSet;

    invoke-virtual {v6, p1}, Lorg/apache/poi/hdf/model/util/BTreeSet;->add(Ljava/lang/Object;)Z

    .line 202
    :cond_0
    invoke-virtual {p1}, Lorg/apache/poi/hdf/model/hdftypes/ChpxNode;->getStart()I

    move-result v3

    .line 203
    .local v3, "start":I
    invoke-virtual {p1}, Lorg/apache/poi/hdf/model/hdftypes/ChpxNode;->getEnd()I

    move-result v2

    .line 205
    .local v2, "end":I
    iget-boolean v6, p0, Lorg/apache/poi/hdf/event/EventBridge;->_holdParagraph:Z

    if-eqz v6, :cond_1

    .line 207
    iget-object v6, p0, Lorg/apache/poi/hdf/event/EventBridge;->_onHold:Ljava/util/ArrayList;

    invoke-virtual {v6, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 208
    iget v6, p0, Lorg/apache/poi/hdf/event/EventBridge;->_endHoldIndex:I

    if-lt v2, v6, :cond_1

    .line 210
    const/4 v6, 0x0

    iput-boolean v6, p0, Lorg/apache/poi/hdf/event/EventBridge;->_holdParagraph:Z

    .line 211
    const/4 v6, -0x1

    iput v6, p0, Lorg/apache/poi/hdf/event/EventBridge;->_endHoldIndex:I

    .line 212
    invoke-direct {p0}, Lorg/apache/poi/hdf/event/EventBridge;->flushHeldParagraph()V

    .line 213
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    iput-object v6, p0, Lorg/apache/poi/hdf/event/EventBridge;->_onHold:Ljava/util/ArrayList;

    .line 217
    :cond_1
    invoke-virtual {p1}, Lorg/apache/poi/hdf/model/hdftypes/ChpxNode;->getChpx()[B

    move-result-object v0

    .line 220
    .local v0, "byteChpx":[B
    iget-object v6, p0, Lorg/apache/poi/hdf/event/EventBridge;->_currentStd:Lorg/apache/poi/hdf/model/hdftypes/StyleDescription;

    invoke-virtual {v6}, Lorg/apache/poi/hdf/model/hdftypes/StyleDescription;->getCHP()Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;

    move-result-object v6

    iget-object v7, p0, Lorg/apache/poi/hdf/event/EventBridge;->_stsh:Lorg/apache/poi/hdf/model/hdftypes/StyleSheet;

    invoke-static {v0, v6, v7}, Lorg/apache/poi/hdf/model/hdftypes/StyleSheet;->uncompressProperty([BLjava/lang/Object;Lorg/apache/poi/hdf/model/hdftypes/StyleSheet;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;

    .line 222
    .local v1, "chp":Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;
    iget-object v6, p0, Lorg/apache/poi/hdf/event/EventBridge;->_text:Lorg/apache/poi/hdf/model/util/BTreeSet;

    iget-object v6, v6, Lorg/apache/poi/hdf/model/util/BTreeSet;->root:Lorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;

    invoke-static {v3, v2, v6}, Lorg/apache/poi/hdf/model/util/BTreeSet;->findProperties(IILorg/apache/poi/hdf/model/util/BTreeSet$BTreeNode;)Ljava/util/ArrayList;

    move-result-object v5

    .line 223
    .local v5, "textList":Ljava/util/ArrayList;
    invoke-direct {p0, v5, v3, v2}, Lorg/apache/poi/hdf/event/EventBridge;->getTextFromNodes(Ljava/util/ArrayList;II)Ljava/lang/String;

    move-result-object v4

    .line 225
    .local v4, "text":Ljava/lang/String;
    iget-object v6, p0, Lorg/apache/poi/hdf/event/EventBridge;->_listener:Lorg/apache/poi/hdf/event/HDFParsingListener;

    iget v7, p0, Lorg/apache/poi/hdf/event/EventBridge;->_fcMin:I

    sub-int v7, v3, v7

    iget v8, p0, Lorg/apache/poi/hdf/event/EventBridge;->_fcMin:I

    sub-int v8, v2, v8

    invoke-interface {v6, v1, v4, v7, v8}, Lorg/apache/poi/hdf/event/HDFParsingListener;->characterRun(Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;Ljava/lang/String;II)V

    .line 226
    return-void
.end method

.method public document(Lorg/apache/poi/hdf/model/hdftypes/DocumentProperties;)V
    .locals 0
    .param p1, "dop"    # Lorg/apache/poi/hdf/model/hdftypes/DocumentProperties;

    .prologue
    .line 102
    iput-object p1, p0, Lorg/apache/poi/hdf/event/EventBridge;->_dop:Lorg/apache/poi/hdf/model/hdftypes/DocumentProperties;

    .line 103
    return-void
.end method

.method public endSections()V
    .locals 11

    .prologue
    const/4 v10, 0x0

    const/4 v9, 0x5

    const/4 v8, 0x3

    const/4 v7, 0x1

    const/4 v6, 0x2

    .line 120
    const/4 v2, 0x1

    .local v2, "x":I
    :goto_0
    iget v3, p0, Lorg/apache/poi/hdf/event/EventBridge;->_sectionCounter:I

    if-lt v2, v3, :cond_0

    .line 162
    return-void

    .line 122
    :cond_0
    iget-object v3, p0, Lorg/apache/poi/hdf/event/EventBridge;->_hdrs:Ljava/util/ArrayList;

    add-int/lit8 v4, v2, -0x1

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lorg/apache/poi/hdf/model/hdftypes/HeaderFooter;

    .line 123
    .local v0, "hdrArray":[Lorg/apache/poi/hdf/model/hdftypes/HeaderFooter;
    const/4 v1, 0x0

    .line 125
    .local v1, "hf":Lorg/apache/poi/hdf/model/hdftypes/HeaderFooter;
    aget-object v3, v0, v10

    invoke-virtual {v3}, Lorg/apache/poi/hdf/model/hdftypes/HeaderFooter;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_1

    .line 127
    aget-object v1, v0, v10

    .line 128
    iget-object v3, p0, Lorg/apache/poi/hdf/event/EventBridge;->_listener:Lorg/apache/poi/hdf/event/HDFParsingListener;

    add-int/lit8 v4, v2, -0x1

    invoke-interface {v3, v4, v7}, Lorg/apache/poi/hdf/event/HDFParsingListener;->header(II)V

    .line 129
    invoke-virtual {v1}, Lorg/apache/poi/hdf/model/hdftypes/HeaderFooter;->getStart()I

    move-result v3

    invoke-virtual {v1}, Lorg/apache/poi/hdf/model/hdftypes/HeaderFooter;->getEnd()I

    move-result v4

    invoke-direct {p0, v3, v4}, Lorg/apache/poi/hdf/event/EventBridge;->flushHeaderProps(II)V

    .line 131
    :cond_1
    aget-object v3, v0, v7

    invoke-virtual {v3}, Lorg/apache/poi/hdf/model/hdftypes/HeaderFooter;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_2

    .line 133
    aget-object v1, v0, v7

    .line 134
    iget-object v3, p0, Lorg/apache/poi/hdf/event/EventBridge;->_listener:Lorg/apache/poi/hdf/event/HDFParsingListener;

    add-int/lit8 v4, v2, -0x1

    invoke-interface {v3, v4, v6}, Lorg/apache/poi/hdf/event/HDFParsingListener;->header(II)V

    .line 135
    invoke-virtual {v1}, Lorg/apache/poi/hdf/model/hdftypes/HeaderFooter;->getStart()I

    move-result v3

    invoke-virtual {v1}, Lorg/apache/poi/hdf/model/hdftypes/HeaderFooter;->getEnd()I

    move-result v4

    invoke-direct {p0, v3, v4}, Lorg/apache/poi/hdf/event/EventBridge;->flushHeaderProps(II)V

    .line 137
    :cond_2
    aget-object v3, v0, v6

    invoke-virtual {v3}, Lorg/apache/poi/hdf/model/hdftypes/HeaderFooter;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_3

    .line 139
    aget-object v1, v0, v6

    .line 140
    iget-object v3, p0, Lorg/apache/poi/hdf/event/EventBridge;->_listener:Lorg/apache/poi/hdf/event/HDFParsingListener;

    add-int/lit8 v4, v2, -0x1

    invoke-interface {v3, v4, v8}, Lorg/apache/poi/hdf/event/HDFParsingListener;->footer(II)V

    .line 141
    invoke-virtual {v1}, Lorg/apache/poi/hdf/model/hdftypes/HeaderFooter;->getStart()I

    move-result v3

    invoke-virtual {v1}, Lorg/apache/poi/hdf/model/hdftypes/HeaderFooter;->getEnd()I

    move-result v4

    invoke-direct {p0, v3, v4}, Lorg/apache/poi/hdf/event/EventBridge;->flushHeaderProps(II)V

    .line 143
    :cond_3
    aget-object v3, v0, v8

    invoke-virtual {v3}, Lorg/apache/poi/hdf/model/hdftypes/HeaderFooter;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_4

    .line 145
    aget-object v1, v0, v6

    .line 146
    iget-object v3, p0, Lorg/apache/poi/hdf/event/EventBridge;->_listener:Lorg/apache/poi/hdf/event/HDFParsingListener;

    add-int/lit8 v4, v2, -0x1

    invoke-interface {v3, v4, v8}, Lorg/apache/poi/hdf/event/HDFParsingListener;->footer(II)V

    .line 147
    invoke-virtual {v1}, Lorg/apache/poi/hdf/model/hdftypes/HeaderFooter;->getStart()I

    move-result v3

    invoke-virtual {v1}, Lorg/apache/poi/hdf/model/hdftypes/HeaderFooter;->getEnd()I

    move-result v4

    invoke-direct {p0, v3, v4}, Lorg/apache/poi/hdf/event/EventBridge;->flushHeaderProps(II)V

    .line 149
    :cond_4
    const/4 v3, 0x4

    aget-object v3, v0, v3

    invoke-virtual {v3}, Lorg/apache/poi/hdf/model/hdftypes/HeaderFooter;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_5

    .line 151
    const/4 v3, 0x4

    aget-object v1, v0, v3

    .line 152
    iget-object v3, p0, Lorg/apache/poi/hdf/event/EventBridge;->_listener:Lorg/apache/poi/hdf/event/HDFParsingListener;

    add-int/lit8 v4, v2, -0x1

    invoke-interface {v3, v4, v9}, Lorg/apache/poi/hdf/event/HDFParsingListener;->header(II)V

    .line 153
    invoke-virtual {v1}, Lorg/apache/poi/hdf/model/hdftypes/HeaderFooter;->getStart()I

    move-result v3

    invoke-virtual {v1}, Lorg/apache/poi/hdf/model/hdftypes/HeaderFooter;->getEnd()I

    move-result v4

    invoke-direct {p0, v3, v4}, Lorg/apache/poi/hdf/event/EventBridge;->flushHeaderProps(II)V

    .line 155
    :cond_5
    aget-object v3, v0, v9

    invoke-virtual {v3}, Lorg/apache/poi/hdf/model/hdftypes/HeaderFooter;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_6

    .line 157
    aget-object v1, v0, v9

    .line 158
    iget-object v3, p0, Lorg/apache/poi/hdf/event/EventBridge;->_listener:Lorg/apache/poi/hdf/event/HDFParsingListener;

    add-int/lit8 v4, v2, -0x1

    const/4 v5, 0x6

    invoke-interface {v3, v4, v5}, Lorg/apache/poi/hdf/event/HDFParsingListener;->footer(II)V

    .line 159
    invoke-virtual {v1}, Lorg/apache/poi/hdf/model/hdftypes/HeaderFooter;->getStart()I

    move-result v3

    invoke-virtual {v1}, Lorg/apache/poi/hdf/model/hdftypes/HeaderFooter;->getEnd()I

    move-result v4

    invoke-direct {p0, v3, v4}, Lorg/apache/poi/hdf/event/EventBridge;->flushHeaderProps(II)V

    .line 120
    :cond_6
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_0
.end method

.method public fonts(Lorg/apache/poi/hdf/model/hdftypes/FontTable;)V
    .locals 0
    .param p1, "fontTbl"    # Lorg/apache/poi/hdf/model/hdftypes/FontTable;

    .prologue
    .line 233
    return-void
.end method

.method public hdrSection(Lorg/apache/poi/hdf/model/hdftypes/SepxNode;)V
    .locals 1
    .param p1, "sepx"    # Lorg/apache/poi/hdf/model/hdftypes/SepxNode;

    .prologue
    .line 115
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/poi/hdf/event/EventBridge;->_beginHeaders:Z

    .line 116
    iget-object v0, p0, Lorg/apache/poi/hdf/event/EventBridge;->_hdrSections:Lorg/apache/poi/hdf/model/util/BTreeSet;

    invoke-virtual {v0, p1}, Lorg/apache/poi/hdf/model/util/BTreeSet;->add(Ljava/lang/Object;)Z

    .line 117
    return-void
.end method

.method public lists(Lorg/apache/poi/hdf/model/hdftypes/ListTables;)V
    .locals 0
    .param p1, "listTbl"    # Lorg/apache/poi/hdf/model/hdftypes/ListTables;

    .prologue
    .line 236
    iput-object p1, p0, Lorg/apache/poi/hdf/event/EventBridge;->_listTables:Lorg/apache/poi/hdf/model/hdftypes/ListTables;

    .line 237
    return-void
.end method

.method public mainDocument([B)V
    .locals 0
    .param p1, "mainDocument"    # [B

    .prologue
    .line 86
    iput-object p1, p0, Lorg/apache/poi/hdf/event/EventBridge;->_mainDocument:[B

    .line 87
    return-void
.end method

.method public miscellaneous(IIIII)V
    .locals 0
    .param p1, "fcMin"    # I
    .param p2, "ccpText"    # I
    .param p3, "ccpFtn"    # I
    .param p4, "fcPlcfhdd"    # I
    .param p5, "lcbPlcfhdd"    # I

    .prologue
    .line 94
    iput p1, p0, Lorg/apache/poi/hdf/event/EventBridge;->_fcMin:I

    .line 95
    iput p2, p0, Lorg/apache/poi/hdf/event/EventBridge;->_ccpText:I

    .line 96
    iput p3, p0, Lorg/apache/poi/hdf/event/EventBridge;->_ccpFtn:I

    .line 97
    iput p4, p0, Lorg/apache/poi/hdf/event/EventBridge;->_hdrOffset:I

    .line 98
    iput p5, p0, Lorg/apache/poi/hdf/event/EventBridge;->_hdrSize:I

    .line 99
    return-void
.end method

.method public paragraph(Lorg/apache/poi/hdf/model/hdftypes/PapxNode;)V
    .locals 8
    .param p1, "papx"    # Lorg/apache/poi/hdf/model/hdftypes/PapxNode;

    .prologue
    .line 168
    iget-boolean v4, p0, Lorg/apache/poi/hdf/event/EventBridge;->_beginHeaders:Z

    if-eqz v4, :cond_0

    .line 170
    iget-object v4, p0, Lorg/apache/poi/hdf/event/EventBridge;->_hdrParagraphs:Lorg/apache/poi/hdf/model/util/BTreeSet;

    invoke-virtual {v4, p1}, Lorg/apache/poi/hdf/model/util/BTreeSet;->add(Ljava/lang/Object;)Z

    .line 172
    :cond_0
    invoke-virtual {p1}, Lorg/apache/poi/hdf/model/hdftypes/PapxNode;->getPapx()[B

    move-result-object v0

    .line 173
    .local v0, "bytePapx":[B
    const/4 v4, 0x0

    invoke-static {v0, v4}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v1

    .line 174
    .local v1, "istd":I
    iget-object v4, p0, Lorg/apache/poi/hdf/event/EventBridge;->_stsh:Lorg/apache/poi/hdf/model/hdftypes/StyleSheet;

    invoke-virtual {v4, v1}, Lorg/apache/poi/hdf/model/hdftypes/StyleSheet;->getStyleDescription(I)Lorg/apache/poi/hdf/model/hdftypes/StyleDescription;

    move-result-object v4

    iput-object v4, p0, Lorg/apache/poi/hdf/event/EventBridge;->_currentStd:Lorg/apache/poi/hdf/model/hdftypes/StyleDescription;

    .line 176
    iget-object v4, p0, Lorg/apache/poi/hdf/event/EventBridge;->_currentStd:Lorg/apache/poi/hdf/model/hdftypes/StyleDescription;

    invoke-virtual {v4}, Lorg/apache/poi/hdf/model/hdftypes/StyleDescription;->getPAP()Lorg/apache/poi/hdf/model/hdftypes/ParagraphProperties;

    move-result-object v4

    iget-object v5, p0, Lorg/apache/poi/hdf/event/EventBridge;->_stsh:Lorg/apache/poi/hdf/model/hdftypes/StyleSheet;

    invoke-static {v0, v4, v5}, Lorg/apache/poi/hdf/model/hdftypes/StyleSheet;->uncompressProperty([BLjava/lang/Object;Lorg/apache/poi/hdf/model/hdftypes/StyleSheet;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/poi/hdf/model/hdftypes/ParagraphProperties;

    .line 178
    .local v2, "pap":Lorg/apache/poi/hdf/model/hdftypes/ParagraphProperties;
    invoke-virtual {v2}, Lorg/apache/poi/hdf/model/hdftypes/ParagraphProperties;->getFTtp()B

    move-result v4

    if-lez v4, :cond_1

    .line 180
    new-instance v4, Lorg/apache/poi/hdf/model/hdftypes/TableProperties;

    invoke-direct {v4}, Lorg/apache/poi/hdf/model/hdftypes/TableProperties;-><init>()V

    iget-object v5, p0, Lorg/apache/poi/hdf/event/EventBridge;->_stsh:Lorg/apache/poi/hdf/model/hdftypes/StyleSheet;

    invoke-static {v0, v4, v5}, Lorg/apache/poi/hdf/model/hdftypes/StyleSheet;->uncompressProperty([BLjava/lang/Object;Lorg/apache/poi/hdf/model/hdftypes/StyleSheet;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/poi/hdf/model/hdftypes/TableProperties;

    .line 181
    .local v3, "tap":Lorg/apache/poi/hdf/model/hdftypes/TableProperties;
    iget-object v4, p0, Lorg/apache/poi/hdf/event/EventBridge;->_listener:Lorg/apache/poi/hdf/event/HDFParsingListener;

    invoke-virtual {p1}, Lorg/apache/poi/hdf/model/hdftypes/PapxNode;->getStart()I

    move-result v5

    iget v6, p0, Lorg/apache/poi/hdf/event/EventBridge;->_fcMin:I

    sub-int/2addr v5, v6

    invoke-virtual {p1}, Lorg/apache/poi/hdf/model/hdftypes/PapxNode;->getEnd()I

    move-result v6

    iget v7, p0, Lorg/apache/poi/hdf/event/EventBridge;->_fcMin:I

    sub-int/2addr v6, v7

    invoke-interface {v4, v3, v5, v6}, Lorg/apache/poi/hdf/event/HDFParsingListener;->tableRowEnd(Lorg/apache/poi/hdf/model/hdftypes/TableProperties;II)V

    .line 193
    .end local v3    # "tap":Lorg/apache/poi/hdf/model/hdftypes/TableProperties;
    :goto_0
    return-void

    .line 183
    :cond_1
    invoke-virtual {v2}, Lorg/apache/poi/hdf/model/hdftypes/ParagraphProperties;->getIlfo()I

    move-result v4

    if-lez v4, :cond_2

    .line 185
    const/4 v4, 0x1

    iput-boolean v4, p0, Lorg/apache/poi/hdf/event/EventBridge;->_holdParagraph:Z

    .line 186
    invoke-virtual {p1}, Lorg/apache/poi/hdf/model/hdftypes/PapxNode;->getEnd()I

    move-result v4

    iput v4, p0, Lorg/apache/poi/hdf/event/EventBridge;->_endHoldIndex:I

    .line 187
    iget-object v4, p0, Lorg/apache/poi/hdf/event/EventBridge;->_onHold:Ljava/util/ArrayList;

    invoke-virtual {v4, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 191
    :cond_2
    iget-object v4, p0, Lorg/apache/poi/hdf/event/EventBridge;->_listener:Lorg/apache/poi/hdf/event/HDFParsingListener;

    invoke-virtual {p1}, Lorg/apache/poi/hdf/model/hdftypes/PapxNode;->getStart()I

    move-result v5

    iget v6, p0, Lorg/apache/poi/hdf/event/EventBridge;->_fcMin:I

    sub-int/2addr v5, v6

    invoke-virtual {p1}, Lorg/apache/poi/hdf/model/hdftypes/PapxNode;->getEnd()I

    move-result v6

    iget v7, p0, Lorg/apache/poi/hdf/event/EventBridge;->_fcMin:I

    sub-int/2addr v6, v7

    invoke-interface {v4, v2, v5, v6}, Lorg/apache/poi/hdf/event/HDFParsingListener;->paragraph(Lorg/apache/poi/hdf/model/hdftypes/ParagraphProperties;II)V

    goto :goto_0
.end method

.method public styleSheet(Lorg/apache/poi/hdf/model/hdftypes/StyleSheet;)V
    .locals 0
    .param p1, "stsh"    # Lorg/apache/poi/hdf/model/hdftypes/StyleSheet;

    .prologue
    .line 240
    iput-object p1, p0, Lorg/apache/poi/hdf/event/EventBridge;->_stsh:Lorg/apache/poi/hdf/model/hdftypes/StyleSheet;

    .line 241
    return-void
.end method

.method public tableStream([B)V
    .locals 0
    .param p1, "tableStream"    # [B

    .prologue
    .line 90
    iput-object p1, p0, Lorg/apache/poi/hdf/event/EventBridge;->_tableStream:[B

    .line 91
    return-void
.end method

.method public text(Lorg/apache/poi/hdf/model/hdftypes/TextPiece;)V
    .locals 1
    .param p1, "t"    # Lorg/apache/poi/hdf/model/hdftypes/TextPiece;

    .prologue
    .line 229
    iget-object v0, p0, Lorg/apache/poi/hdf/event/EventBridge;->_text:Lorg/apache/poi/hdf/model/util/BTreeSet;

    invoke-virtual {v0, p1}, Lorg/apache/poi/hdf/model/util/BTreeSet;->add(Ljava/lang/Object;)Z

    .line 230
    return-void
.end method
