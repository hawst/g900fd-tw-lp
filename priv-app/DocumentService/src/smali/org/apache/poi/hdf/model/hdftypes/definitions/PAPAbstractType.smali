.class public abstract Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;
.super Ljava/lang/Object;
.source "PAPAbstractType.java"

# interfaces
.implements Lorg/apache/poi/hdf/model/hdftypes/HDFType;


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field private static fBackward:Lorg/apache/poi/util/BitField;

.field private static fRotateFont:Lorg/apache/poi/util/BitField;

.field private static fVertical:Lorg/apache/poi/util/BitField;


# instance fields
.field private field_10_brcp:B

.field private field_11_brcl:B

.field private field_12_ilvl:B

.field private field_13_fNoLnn:B

.field private field_14_ilfo:I

.field private field_15_fSideBySide:B

.field private field_16_fNoAutoHyph:B

.field private field_17_fWidowControl:B

.field private field_18_dxaRight:I

.field private field_19_dxaLeft:I

.field private field_1_istd:I

.field private field_20_dxaLeft1:I

.field private field_21_lspd:[S

.field private field_22_dyaBefore:I

.field private field_23_dyaAfter:I

.field private field_24_phe:[B

.field private field_25_fCrLf:B

.field private field_26_fUsePgsuSettings:B

.field private field_27_fAdjustRight:B

.field private field_28_fKinsoku:B

.field private field_29_fWordWrap:B

.field private field_2_jc:B

.field private field_30_fOverflowPunct:B

.field private field_31_fTopLinePunct:B

.field private field_32_fAutoSpaceDE:B

.field private field_33_fAutoSpaceDN:B

.field private field_34_wAlignFont:I

.field private field_35_fontAlign:S

.field private field_36_fBackward:B

.field private field_37_fRotateFont:B

.field private field_38_fInTable:B

.field private field_39_fTtp:B

.field private field_3_fKeep:B

.field private field_40_wr:B

.field private field_41_fLocked:B

.field private field_42_ptap:[B

.field private field_43_dxaAbs:I

.field private field_44_dyaAbs:I

.field private field_45_dxaWidth:I

.field private field_46_brcTop:[S

.field private field_47_brcLeft:[S

.field private field_48_brcBottom:[S

.field private field_49_brcRight:[S

.field private field_4_fKeepFollow:B

.field private field_50_brcBetween:[S

.field private field_51_brcBar:[S

.field private field_52_dxaFromText:I

.field private field_53_dyaFromText:I

.field private field_54_dyaHeight:I

.field private field_55_fMinHeight:B

.field private field_56_shd:S

.field private field_57_dcs:S

.field private field_58_lvl:B

.field private field_59_fNumRMIns:B

.field private field_5_fPageBreakBefore:B

.field private field_60_anld:[B

.field private field_61_fPropRMark:I

.field private field_62_ibstPropRMark:I

.field private field_63_dttmPropRMark:[B

.field private field_64_numrm:[B

.field private field_65_itbdMac:I

.field private field_66_rgdxaTab:[B

.field private field_67_rgtbd:[B

.field private field_6_fBrLnAbove:B

.field private field_7_fBrLnBelow:B

.field private field_8_pcVert:B

.field private field_9_pcHorz:B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 72
    const/4 v0, 0x1

    invoke-static {v0}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->fVertical:Lorg/apache/poi/util/BitField;

    .line 73
    const/4 v0, 0x2

    invoke-static {v0}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->fBackward:Lorg/apache/poi/util/BitField;

    .line 74
    const/4 v0, 0x4

    invoke-static {v0}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->fRotateFont:Lorg/apache/poi/util/BitField;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 109
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 112
    return-void
.end method


# virtual methods
.method public getAnld()[B
    .locals 1

    .prologue
    .line 1073
    iget-object v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->field_60_anld:[B

    return-object v0
.end method

.method public getBrcBar()[S
    .locals 1

    .prologue
    .line 929
    iget-object v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->field_51_brcBar:[S

    return-object v0
.end method

.method public getBrcBetween()[S
    .locals 1

    .prologue
    .line 913
    iget-object v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->field_50_brcBetween:[S

    return-object v0
.end method

.method public getBrcBottom()[S
    .locals 1

    .prologue
    .line 881
    iget-object v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->field_48_brcBottom:[S

    return-object v0
.end method

.method public getBrcLeft()[S
    .locals 1

    .prologue
    .line 865
    iget-object v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->field_47_brcLeft:[S

    return-object v0
.end method

.method public getBrcRight()[S
    .locals 1

    .prologue
    .line 897
    iget-object v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->field_49_brcRight:[S

    return-object v0
.end method

.method public getBrcTop()[S
    .locals 1

    .prologue
    .line 849
    iget-object v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->field_46_brcTop:[S

    return-object v0
.end method

.method public getBrcl()B
    .locals 1

    .prologue
    .line 289
    iget-byte v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->field_11_brcl:B

    return v0
.end method

.method public getBrcp()B
    .locals 1

    .prologue
    .line 273
    iget-byte v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->field_10_brcp:B

    return v0
.end method

.method public getDcs()S
    .locals 1

    .prologue
    .line 1025
    iget-short v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->field_57_dcs:S

    return v0
.end method

.method public getDttmPropRMark()[B
    .locals 1

    .prologue
    .line 1121
    iget-object v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->field_63_dttmPropRMark:[B

    return-object v0
.end method

.method public getDxaAbs()I
    .locals 1

    .prologue
    .line 801
    iget v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->field_43_dxaAbs:I

    return v0
.end method

.method public getDxaFromText()I
    .locals 1

    .prologue
    .line 945
    iget v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->field_52_dxaFromText:I

    return v0
.end method

.method public getDxaLeft()I
    .locals 1

    .prologue
    .line 417
    iget v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->field_19_dxaLeft:I

    return v0
.end method

.method public getDxaLeft1()I
    .locals 1

    .prologue
    .line 433
    iget v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->field_20_dxaLeft1:I

    return v0
.end method

.method public getDxaRight()I
    .locals 1

    .prologue
    .line 401
    iget v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->field_18_dxaRight:I

    return v0
.end method

.method public getDxaWidth()I
    .locals 1

    .prologue
    .line 833
    iget v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->field_45_dxaWidth:I

    return v0
.end method

.method public getDyaAbs()I
    .locals 1

    .prologue
    .line 817
    iget v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->field_44_dyaAbs:I

    return v0
.end method

.method public getDyaAfter()I
    .locals 1

    .prologue
    .line 481
    iget v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->field_23_dyaAfter:I

    return v0
.end method

.method public getDyaBefore()I
    .locals 1

    .prologue
    .line 465
    iget v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->field_22_dyaBefore:I

    return v0
.end method

.method public getDyaFromText()I
    .locals 1

    .prologue
    .line 961
    iget v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->field_53_dyaFromText:I

    return v0
.end method

.method public getDyaHeight()I
    .locals 1

    .prologue
    .line 977
    iget v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->field_54_dyaHeight:I

    return v0
.end method

.method public getFAdjustRight()B
    .locals 1

    .prologue
    .line 545
    iget-byte v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->field_27_fAdjustRight:B

    return v0
.end method

.method public getFAutoSpaceDE()B
    .locals 1

    .prologue
    .line 625
    iget-byte v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->field_32_fAutoSpaceDE:B

    return v0
.end method

.method public getFAutoSpaceDN()B
    .locals 1

    .prologue
    .line 641
    iget-byte v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->field_33_fAutoSpaceDN:B

    return v0
.end method

.method public getFBackward()B
    .locals 1

    .prologue
    .line 689
    iget-byte v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->field_36_fBackward:B

    return v0
.end method

.method public getFBrLnAbove()B
    .locals 1

    .prologue
    .line 209
    iget-byte v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->field_6_fBrLnAbove:B

    return v0
.end method

.method public getFBrLnBelow()B
    .locals 1

    .prologue
    .line 225
    iget-byte v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->field_7_fBrLnBelow:B

    return v0
.end method

.method public getFCrLf()B
    .locals 1

    .prologue
    .line 513
    iget-byte v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->field_25_fCrLf:B

    return v0
.end method

.method public getFInTable()B
    .locals 1

    .prologue
    .line 721
    iget-byte v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->field_38_fInTable:B

    return v0
.end method

.method public getFKeep()B
    .locals 1

    .prologue
    .line 161
    iget-byte v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->field_3_fKeep:B

    return v0
.end method

.method public getFKeepFollow()B
    .locals 1

    .prologue
    .line 177
    iget-byte v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->field_4_fKeepFollow:B

    return v0
.end method

.method public getFKinsoku()B
    .locals 1

    .prologue
    .line 561
    iget-byte v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->field_28_fKinsoku:B

    return v0
.end method

.method public getFLocked()B
    .locals 1

    .prologue
    .line 769
    iget-byte v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->field_41_fLocked:B

    return v0
.end method

.method public getFMinHeight()B
    .locals 1

    .prologue
    .line 993
    iget-byte v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->field_55_fMinHeight:B

    return v0
.end method

.method public getFNoAutoHyph()B
    .locals 1

    .prologue
    .line 369
    iget-byte v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->field_16_fNoAutoHyph:B

    return v0
.end method

.method public getFNoLnn()B
    .locals 1

    .prologue
    .line 321
    iget-byte v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->field_13_fNoLnn:B

    return v0
.end method

.method public getFNumRMIns()B
    .locals 1

    .prologue
    .line 1057
    iget-byte v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->field_59_fNumRMIns:B

    return v0
.end method

.method public getFOverflowPunct()B
    .locals 1

    .prologue
    .line 593
    iget-byte v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->field_30_fOverflowPunct:B

    return v0
.end method

.method public getFPageBreakBefore()B
    .locals 1

    .prologue
    .line 193
    iget-byte v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->field_5_fPageBreakBefore:B

    return v0
.end method

.method public getFPropRMark()I
    .locals 1

    .prologue
    .line 1089
    iget v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->field_61_fPropRMark:I

    return v0
.end method

.method public getFRotateFont()B
    .locals 1

    .prologue
    .line 705
    iget-byte v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->field_37_fRotateFont:B

    return v0
.end method

.method public getFSideBySide()B
    .locals 1

    .prologue
    .line 353
    iget-byte v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->field_15_fSideBySide:B

    return v0
.end method

.method public getFTopLinePunct()B
    .locals 1

    .prologue
    .line 609
    iget-byte v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->field_31_fTopLinePunct:B

    return v0
.end method

.method public getFTtp()B
    .locals 1

    .prologue
    .line 737
    iget-byte v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->field_39_fTtp:B

    return v0
.end method

.method public getFUsePgsuSettings()B
    .locals 1

    .prologue
    .line 529
    iget-byte v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->field_26_fUsePgsuSettings:B

    return v0
.end method

.method public getFWidowControl()B
    .locals 1

    .prologue
    .line 385
    iget-byte v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->field_17_fWidowControl:B

    return v0
.end method

.method public getFWordWrap()B
    .locals 1

    .prologue
    .line 577
    iget-byte v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->field_29_fWordWrap:B

    return v0
.end method

.method public getFontAlign()S
    .locals 1

    .prologue
    .line 673
    iget-short v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->field_35_fontAlign:S

    return v0
.end method

.method public getIbstPropRMark()I
    .locals 1

    .prologue
    .line 1105
    iget v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->field_62_ibstPropRMark:I

    return v0
.end method

.method public getIlfo()I
    .locals 1

    .prologue
    .line 337
    iget v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->field_14_ilfo:I

    return v0
.end method

.method public getIlvl()B
    .locals 1

    .prologue
    .line 305
    iget-byte v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->field_12_ilvl:B

    return v0
.end method

.method public getIstd()I
    .locals 1

    .prologue
    .line 129
    iget v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->field_1_istd:I

    return v0
.end method

.method public getItbdMac()I
    .locals 1

    .prologue
    .line 1153
    iget v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->field_65_itbdMac:I

    return v0
.end method

.method public getJc()B
    .locals 1

    .prologue
    .line 145
    iget-byte v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->field_2_jc:B

    return v0
.end method

.method public getLspd()[S
    .locals 1

    .prologue
    .line 449
    iget-object v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->field_21_lspd:[S

    return-object v0
.end method

.method public getLvl()B
    .locals 1

    .prologue
    .line 1041
    iget-byte v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->field_58_lvl:B

    return v0
.end method

.method public getNumrm()[B
    .locals 1

    .prologue
    .line 1137
    iget-object v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->field_64_numrm:[B

    return-object v0
.end method

.method public getPcHorz()B
    .locals 1

    .prologue
    .line 257
    iget-byte v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->field_9_pcHorz:B

    return v0
.end method

.method public getPcVert()B
    .locals 1

    .prologue
    .line 241
    iget-byte v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->field_8_pcVert:B

    return v0
.end method

.method public getPhe()[B
    .locals 1

    .prologue
    .line 497
    iget-object v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->field_24_phe:[B

    return-object v0
.end method

.method public getPtap()[B
    .locals 1

    .prologue
    .line 785
    iget-object v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->field_42_ptap:[B

    return-object v0
.end method

.method public getRgdxaTab()[B
    .locals 1

    .prologue
    .line 1169
    iget-object v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->field_66_rgdxaTab:[B

    return-object v0
.end method

.method public getRgtbd()[B
    .locals 1

    .prologue
    .line 1185
    iget-object v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->field_67_rgtbd:[B

    return-object v0
.end method

.method public getShd()S
    .locals 1

    .prologue
    .line 1009
    iget-short v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->field_56_shd:S

    return v0
.end method

.method public getSize()I
    .locals 1

    .prologue
    .line 119
    const/16 v0, 0x264

    return v0
.end method

.method public getWAlignFont()I
    .locals 1

    .prologue
    .line 657
    iget v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->field_34_wAlignFont:I

    return v0
.end method

.method public getWr()B
    .locals 1

    .prologue
    .line 753
    iget-byte v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->field_40_wr:B

    return v0
.end method

.method public isFBackward()Z
    .locals 2

    .prologue
    .line 1234
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->fBackward:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->field_35_fontAlign:S

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFRotateFont()Z
    .locals 2

    .prologue
    .line 1255
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->fRotateFont:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->field_35_fontAlign:S

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFVertical()Z
    .locals 2

    .prologue
    .line 1213
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->fVertical:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->field_35_fontAlign:S

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public setAnld([B)V
    .locals 0
    .param p1, "field_60_anld"    # [B

    .prologue
    .line 1081
    iput-object p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->field_60_anld:[B

    .line 1082
    return-void
.end method

.method public setBrcBar([S)V
    .locals 0
    .param p1, "field_51_brcBar"    # [S

    .prologue
    .line 937
    iput-object p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->field_51_brcBar:[S

    .line 938
    return-void
.end method

.method public setBrcBetween([S)V
    .locals 0
    .param p1, "field_50_brcBetween"    # [S

    .prologue
    .line 921
    iput-object p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->field_50_brcBetween:[S

    .line 922
    return-void
.end method

.method public setBrcBottom([S)V
    .locals 0
    .param p1, "field_48_brcBottom"    # [S

    .prologue
    .line 889
    iput-object p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->field_48_brcBottom:[S

    .line 890
    return-void
.end method

.method public setBrcLeft([S)V
    .locals 0
    .param p1, "field_47_brcLeft"    # [S

    .prologue
    .line 873
    iput-object p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->field_47_brcLeft:[S

    .line 874
    return-void
.end method

.method public setBrcRight([S)V
    .locals 0
    .param p1, "field_49_brcRight"    # [S

    .prologue
    .line 905
    iput-object p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->field_49_brcRight:[S

    .line 906
    return-void
.end method

.method public setBrcTop([S)V
    .locals 0
    .param p1, "field_46_brcTop"    # [S

    .prologue
    .line 857
    iput-object p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->field_46_brcTop:[S

    .line 858
    return-void
.end method

.method public setBrcl(B)V
    .locals 0
    .param p1, "field_11_brcl"    # B

    .prologue
    .line 297
    iput-byte p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->field_11_brcl:B

    .line 298
    return-void
.end method

.method public setBrcp(B)V
    .locals 0
    .param p1, "field_10_brcp"    # B

    .prologue
    .line 281
    iput-byte p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->field_10_brcp:B

    .line 282
    return-void
.end method

.method public setDcs(S)V
    .locals 0
    .param p1, "field_57_dcs"    # S

    .prologue
    .line 1033
    iput-short p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->field_57_dcs:S

    .line 1034
    return-void
.end method

.method public setDttmPropRMark([B)V
    .locals 0
    .param p1, "field_63_dttmPropRMark"    # [B

    .prologue
    .line 1129
    iput-object p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->field_63_dttmPropRMark:[B

    .line 1130
    return-void
.end method

.method public setDxaAbs(I)V
    .locals 0
    .param p1, "field_43_dxaAbs"    # I

    .prologue
    .line 809
    iput p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->field_43_dxaAbs:I

    .line 810
    return-void
.end method

.method public setDxaFromText(I)V
    .locals 0
    .param p1, "field_52_dxaFromText"    # I

    .prologue
    .line 953
    iput p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->field_52_dxaFromText:I

    .line 954
    return-void
.end method

.method public setDxaLeft(I)V
    .locals 0
    .param p1, "field_19_dxaLeft"    # I

    .prologue
    .line 425
    iput p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->field_19_dxaLeft:I

    .line 426
    return-void
.end method

.method public setDxaLeft1(I)V
    .locals 0
    .param p1, "field_20_dxaLeft1"    # I

    .prologue
    .line 441
    iput p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->field_20_dxaLeft1:I

    .line 442
    return-void
.end method

.method public setDxaRight(I)V
    .locals 0
    .param p1, "field_18_dxaRight"    # I

    .prologue
    .line 409
    iput p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->field_18_dxaRight:I

    .line 410
    return-void
.end method

.method public setDxaWidth(I)V
    .locals 0
    .param p1, "field_45_dxaWidth"    # I

    .prologue
    .line 841
    iput p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->field_45_dxaWidth:I

    .line 842
    return-void
.end method

.method public setDyaAbs(I)V
    .locals 0
    .param p1, "field_44_dyaAbs"    # I

    .prologue
    .line 825
    iput p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->field_44_dyaAbs:I

    .line 826
    return-void
.end method

.method public setDyaAfter(I)V
    .locals 0
    .param p1, "field_23_dyaAfter"    # I

    .prologue
    .line 489
    iput p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->field_23_dyaAfter:I

    .line 490
    return-void
.end method

.method public setDyaBefore(I)V
    .locals 0
    .param p1, "field_22_dyaBefore"    # I

    .prologue
    .line 473
    iput p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->field_22_dyaBefore:I

    .line 474
    return-void
.end method

.method public setDyaFromText(I)V
    .locals 0
    .param p1, "field_53_dyaFromText"    # I

    .prologue
    .line 969
    iput p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->field_53_dyaFromText:I

    .line 970
    return-void
.end method

.method public setDyaHeight(I)V
    .locals 0
    .param p1, "field_54_dyaHeight"    # I

    .prologue
    .line 985
    iput p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->field_54_dyaHeight:I

    .line 986
    return-void
.end method

.method public setFAdjustRight(B)V
    .locals 0
    .param p1, "field_27_fAdjustRight"    # B

    .prologue
    .line 553
    iput-byte p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->field_27_fAdjustRight:B

    .line 554
    return-void
.end method

.method public setFAutoSpaceDE(B)V
    .locals 0
    .param p1, "field_32_fAutoSpaceDE"    # B

    .prologue
    .line 633
    iput-byte p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->field_32_fAutoSpaceDE:B

    .line 634
    return-void
.end method

.method public setFAutoSpaceDN(B)V
    .locals 0
    .param p1, "field_33_fAutoSpaceDN"    # B

    .prologue
    .line 649
    iput-byte p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->field_33_fAutoSpaceDN:B

    .line 650
    return-void
.end method

.method public setFBackward(B)V
    .locals 0
    .param p1, "field_36_fBackward"    # B

    .prologue
    .line 697
    iput-byte p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->field_36_fBackward:B

    .line 698
    return-void
.end method

.method public setFBackward(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 1223
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->fBackward:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->field_35_fontAlign:S

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->field_35_fontAlign:S

    .line 1226
    return-void
.end method

.method public setFBrLnAbove(B)V
    .locals 0
    .param p1, "field_6_fBrLnAbove"    # B

    .prologue
    .line 217
    iput-byte p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->field_6_fBrLnAbove:B

    .line 218
    return-void
.end method

.method public setFBrLnBelow(B)V
    .locals 0
    .param p1, "field_7_fBrLnBelow"    # B

    .prologue
    .line 233
    iput-byte p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->field_7_fBrLnBelow:B

    .line 234
    return-void
.end method

.method public setFCrLf(B)V
    .locals 0
    .param p1, "field_25_fCrLf"    # B

    .prologue
    .line 521
    iput-byte p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->field_25_fCrLf:B

    .line 522
    return-void
.end method

.method public setFInTable(B)V
    .locals 0
    .param p1, "field_38_fInTable"    # B

    .prologue
    .line 729
    iput-byte p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->field_38_fInTable:B

    .line 730
    return-void
.end method

.method public setFKeep(B)V
    .locals 0
    .param p1, "field_3_fKeep"    # B

    .prologue
    .line 169
    iput-byte p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->field_3_fKeep:B

    .line 170
    return-void
.end method

.method public setFKeepFollow(B)V
    .locals 0
    .param p1, "field_4_fKeepFollow"    # B

    .prologue
    .line 185
    iput-byte p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->field_4_fKeepFollow:B

    .line 186
    return-void
.end method

.method public setFKinsoku(B)V
    .locals 0
    .param p1, "field_28_fKinsoku"    # B

    .prologue
    .line 569
    iput-byte p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->field_28_fKinsoku:B

    .line 570
    return-void
.end method

.method public setFLocked(B)V
    .locals 0
    .param p1, "field_41_fLocked"    # B

    .prologue
    .line 777
    iput-byte p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->field_41_fLocked:B

    .line 778
    return-void
.end method

.method public setFMinHeight(B)V
    .locals 0
    .param p1, "field_55_fMinHeight"    # B

    .prologue
    .line 1001
    iput-byte p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->field_55_fMinHeight:B

    .line 1002
    return-void
.end method

.method public setFNoAutoHyph(B)V
    .locals 0
    .param p1, "field_16_fNoAutoHyph"    # B

    .prologue
    .line 377
    iput-byte p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->field_16_fNoAutoHyph:B

    .line 378
    return-void
.end method

.method public setFNoLnn(B)V
    .locals 0
    .param p1, "field_13_fNoLnn"    # B

    .prologue
    .line 329
    iput-byte p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->field_13_fNoLnn:B

    .line 330
    return-void
.end method

.method public setFNumRMIns(B)V
    .locals 0
    .param p1, "field_59_fNumRMIns"    # B

    .prologue
    .line 1065
    iput-byte p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->field_59_fNumRMIns:B

    .line 1066
    return-void
.end method

.method public setFOverflowPunct(B)V
    .locals 0
    .param p1, "field_30_fOverflowPunct"    # B

    .prologue
    .line 601
    iput-byte p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->field_30_fOverflowPunct:B

    .line 602
    return-void
.end method

.method public setFPageBreakBefore(B)V
    .locals 0
    .param p1, "field_5_fPageBreakBefore"    # B

    .prologue
    .line 201
    iput-byte p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->field_5_fPageBreakBefore:B

    .line 202
    return-void
.end method

.method public setFPropRMark(I)V
    .locals 0
    .param p1, "field_61_fPropRMark"    # I

    .prologue
    .line 1097
    iput p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->field_61_fPropRMark:I

    .line 1098
    return-void
.end method

.method public setFRotateFont(B)V
    .locals 0
    .param p1, "field_37_fRotateFont"    # B

    .prologue
    .line 713
    iput-byte p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->field_37_fRotateFont:B

    .line 714
    return-void
.end method

.method public setFRotateFont(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 1244
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->fRotateFont:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->field_35_fontAlign:S

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->field_35_fontAlign:S

    .line 1247
    return-void
.end method

.method public setFSideBySide(B)V
    .locals 0
    .param p1, "field_15_fSideBySide"    # B

    .prologue
    .line 361
    iput-byte p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->field_15_fSideBySide:B

    .line 362
    return-void
.end method

.method public setFTopLinePunct(B)V
    .locals 0
    .param p1, "field_31_fTopLinePunct"    # B

    .prologue
    .line 617
    iput-byte p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->field_31_fTopLinePunct:B

    .line 618
    return-void
.end method

.method public setFTtp(B)V
    .locals 0
    .param p1, "field_39_fTtp"    # B

    .prologue
    .line 745
    iput-byte p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->field_39_fTtp:B

    .line 746
    return-void
.end method

.method public setFUsePgsuSettings(B)V
    .locals 0
    .param p1, "field_26_fUsePgsuSettings"    # B

    .prologue
    .line 537
    iput-byte p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->field_26_fUsePgsuSettings:B

    .line 538
    return-void
.end method

.method public setFVertical(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 1202
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->fVertical:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->field_35_fontAlign:S

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->field_35_fontAlign:S

    .line 1205
    return-void
.end method

.method public setFWidowControl(B)V
    .locals 0
    .param p1, "field_17_fWidowControl"    # B

    .prologue
    .line 393
    iput-byte p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->field_17_fWidowControl:B

    .line 394
    return-void
.end method

.method public setFWordWrap(B)V
    .locals 0
    .param p1, "field_29_fWordWrap"    # B

    .prologue
    .line 585
    iput-byte p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->field_29_fWordWrap:B

    .line 586
    return-void
.end method

.method public setFontAlign(S)V
    .locals 0
    .param p1, "field_35_fontAlign"    # S

    .prologue
    .line 681
    iput-short p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->field_35_fontAlign:S

    .line 682
    return-void
.end method

.method public setIbstPropRMark(I)V
    .locals 0
    .param p1, "field_62_ibstPropRMark"    # I

    .prologue
    .line 1113
    iput p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->field_62_ibstPropRMark:I

    .line 1114
    return-void
.end method

.method public setIlfo(I)V
    .locals 0
    .param p1, "field_14_ilfo"    # I

    .prologue
    .line 345
    iput p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->field_14_ilfo:I

    .line 346
    return-void
.end method

.method public setIlvl(B)V
    .locals 0
    .param p1, "field_12_ilvl"    # B

    .prologue
    .line 313
    iput-byte p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->field_12_ilvl:B

    .line 314
    return-void
.end method

.method public setIstd(I)V
    .locals 0
    .param p1, "field_1_istd"    # I

    .prologue
    .line 137
    iput p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->field_1_istd:I

    .line 138
    return-void
.end method

.method public setItbdMac(I)V
    .locals 0
    .param p1, "field_65_itbdMac"    # I

    .prologue
    .line 1161
    iput p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->field_65_itbdMac:I

    .line 1162
    return-void
.end method

.method public setJc(B)V
    .locals 0
    .param p1, "field_2_jc"    # B

    .prologue
    .line 153
    iput-byte p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->field_2_jc:B

    .line 154
    return-void
.end method

.method public setLspd([S)V
    .locals 0
    .param p1, "field_21_lspd"    # [S

    .prologue
    .line 457
    iput-object p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->field_21_lspd:[S

    .line 458
    return-void
.end method

.method public setLvl(B)V
    .locals 0
    .param p1, "field_58_lvl"    # B

    .prologue
    .line 1049
    iput-byte p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->field_58_lvl:B

    .line 1050
    return-void
.end method

.method public setNumrm([B)V
    .locals 0
    .param p1, "field_64_numrm"    # [B

    .prologue
    .line 1145
    iput-object p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->field_64_numrm:[B

    .line 1146
    return-void
.end method

.method public setPcHorz(B)V
    .locals 0
    .param p1, "field_9_pcHorz"    # B

    .prologue
    .line 265
    iput-byte p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->field_9_pcHorz:B

    .line 266
    return-void
.end method

.method public setPcVert(B)V
    .locals 0
    .param p1, "field_8_pcVert"    # B

    .prologue
    .line 249
    iput-byte p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->field_8_pcVert:B

    .line 250
    return-void
.end method

.method public setPhe([B)V
    .locals 0
    .param p1, "field_24_phe"    # [B

    .prologue
    .line 505
    iput-object p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->field_24_phe:[B

    .line 506
    return-void
.end method

.method public setPtap([B)V
    .locals 0
    .param p1, "field_42_ptap"    # [B

    .prologue
    .line 793
    iput-object p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->field_42_ptap:[B

    .line 794
    return-void
.end method

.method public setRgdxaTab([B)V
    .locals 0
    .param p1, "field_66_rgdxaTab"    # [B

    .prologue
    .line 1177
    iput-object p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->field_66_rgdxaTab:[B

    .line 1178
    return-void
.end method

.method public setRgtbd([B)V
    .locals 0
    .param p1, "field_67_rgtbd"    # [B

    .prologue
    .line 1193
    iput-object p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->field_67_rgtbd:[B

    .line 1194
    return-void
.end method

.method public setShd(S)V
    .locals 0
    .param p1, "field_56_shd"    # S

    .prologue
    .line 1017
    iput-short p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->field_56_shd:S

    .line 1018
    return-void
.end method

.method public setWAlignFont(I)V
    .locals 0
    .param p1, "field_34_wAlignFont"    # I

    .prologue
    .line 665
    iput p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->field_34_wAlignFont:I

    .line 666
    return-void
.end method

.method public setWr(B)V
    .locals 0
    .param p1, "field_40_wr"    # B

    .prologue
    .line 761
    iput-byte p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;->field_40_wr:B

    .line 762
    return-void
.end method
