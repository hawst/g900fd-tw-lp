.class public abstract Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;
.super Ljava/lang/Object;
.source "DOPAbstractType.java"

# interfaces
.implements Lorg/apache/poi/hdf/model/hdftypes/HDFType;


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field private static KeyVirusSession30:Lorg/apache/poi/util/BitField;

.field private static epc:Lorg/apache/poi/util/BitField;

.field private static fAutoHyphen:Lorg/apache/poi/util/BitField;

.field private static fAutoVersions:Lorg/apache/poi/util/BitField;

.field private static fBackup:Lorg/apache/poi/util/BitField;

.field private static fConvMailMergeEsc:Lorg/apache/poi/util/BitField;

.field private static fDfltTrueType:Lorg/apache/poi/util/BitField;

.field private static fDispFormFldSel:Lorg/apache/poi/util/BitField;

.field private static fEmbedFonts:Lorg/apache/poi/util/BitField;

.field private static fExactCWords:Lorg/apache/poi/util/BitField;

.field private static fFacingPages:Lorg/apache/poi/util/BitField;

.field private static fForcePageSizePag:Lorg/apache/poi/util/BitField;

.field private static fFormNoFields:Lorg/apache/poi/util/BitField;

.field private static fGramAllClean:Lorg/apache/poi/util/BitField;

.field private static fGramAllDone:Lorg/apache/poi/util/BitField;

.field private static fHaveVersions:Lorg/apache/poi/util/BitField;

.field private static fHideLastVersion:Lorg/apache/poi/util/BitField;

.field private static fHtmlDoc:Lorg/apache/poi/util/BitField;

.field private static fHyphCapitals:Lorg/apache/poi/util/BitField;

.field private static fIncludeFooter:Lorg/apache/poi/util/BitField;

.field private static fIncludeHeader:Lorg/apache/poi/util/BitField;

.field private static fLabelDoc:Lorg/apache/poi/util/BitField;

.field private static fLinkStyles:Lorg/apache/poi/util/BitField;

.field private static fLockAtn:Lorg/apache/poi/util/BitField;

.field private static fLockRev:Lorg/apache/poi/util/BitField;

.field private static fMWSmallCaps:Lorg/apache/poi/util/BitField;

.field private static fMapPrintTextColor:Lorg/apache/poi/util/BitField;

.field private static fMinFontSizePag:Lorg/apache/poi/util/BitField;

.field private static fMirrorMargins:Lorg/apache/poi/util/BitField;

.field private static fNoColumnBalance:Lorg/apache/poi/util/BitField;

.field private static fNoLeading:Lorg/apache/poi/util/BitField;

.field private static fNoSpaceRaiseLower:Lorg/apache/poi/util/BitField;

.field private static fNoTabForInd:Lorg/apache/poi/util/BitField;

.field private static fOnlyMacPics:Lorg/apache/poi/util/BitField;

.field private static fOnlyWinPics:Lorg/apache/poi/util/BitField;

.field private static fOrigWordTableRules:Lorg/apache/poi/util/BitField;

.field private static fPMHMainDoc:Lorg/apache/poi/util/BitField;

.field private static fPagHidden:Lorg/apache/poi/util/BitField;

.field private static fPagResults:Lorg/apache/poi/util/BitField;

.field private static fPagSupressTopSpacing:Lorg/apache/poi/util/BitField;

.field private static fPrintBodyBeforeHdr:Lorg/apache/poi/util/BitField;

.field private static fPrintFormData:Lorg/apache/poi/util/BitField;

.field private static fProtEnabled:Lorg/apache/poi/util/BitField;

.field private static fRMPrint:Lorg/apache/poi/util/BitField;

.field private static fRMView:Lorg/apache/poi/util/BitField;

.field private static fRevMarking:Lorg/apache/poi/util/BitField;

.field private static fRotateFontW6:Lorg/apache/poi/util/BitField;

.field private static fSaveFormData:Lorg/apache/poi/util/BitField;

.field private static fShadeFormData:Lorg/apache/poi/util/BitField;

.field private static fShowBreaksInFrames:Lorg/apache/poi/util/BitField;

.field private static fSnapBorder:Lorg/apache/poi/util/BitField;

.field private static fSubsetFonts:Lorg/apache/poi/util/BitField;

.field private static fSuppressTopSPacingMac5:Lorg/apache/poi/util/BitField;

.field private static fSupressSpdfAfterPageBreak:Lorg/apache/poi/util/BitField;

.field private static fSupressTopSpacing:Lorg/apache/poi/util/BitField;

.field private static fSwapBordersFacingPgs:Lorg/apache/poi/util/BitField;

.field private static fTransparentMetafiles:Lorg/apache/poi/util/BitField;

.field private static fTruncDxaExpand:Lorg/apache/poi/util/BitField;

.field private static fVirusLoadSafe:Lorg/apache/poi/util/BitField;

.field private static fVirusPrompted:Lorg/apache/poi/util/BitField;

.field private static fWCFtnEdn:Lorg/apache/poi/util/BitField;

.field private static fWidowControl:Lorg/apache/poi/util/BitField;

.field private static fWrapTrailSpaces:Lorg/apache/poi/util/BitField;

.field private static fpc:Lorg/apache/poi/util/BitField;

.field private static grfSupression:Lorg/apache/poi/util/BitField;

.field private static iGutterPos:Lorg/apache/poi/util/BitField;

.field private static lvl:Lorg/apache/poi/util/BitField;

.field private static nEdn:Lorg/apache/poi/util/BitField;

.field private static nFtn:Lorg/apache/poi/util/BitField;

.field private static nfcEdnRef1:Lorg/apache/poi/util/BitField;

.field private static nfcFtnRef1:Lorg/apache/poi/util/BitField;

.field private static oldfConvMailMergeEsc:Lorg/apache/poi/util/BitField;

.field private static oldfMapPrintTextColor:Lorg/apache/poi/util/BitField;

.field private static oldfNoColumnBalance:Lorg/apache/poi/util/BitField;

.field private static oldfNoSpaceRaiseLower:Lorg/apache/poi/util/BitField;

.field private static oldfNoTabForInd:Lorg/apache/poi/util/BitField;

.field private static oldfOrigWordTableRules:Lorg/apache/poi/util/BitField;

.field private static oldfShowBreaksInFrames:Lorg/apache/poi/util/BitField;

.field private static oldfSuppressSpbfAfterPageBreak:Lorg/apache/poi/util/BitField;

.field private static oldfSupressTopSpacing:Lorg/apache/poi/util/BitField;

.field private static oldfSwapBordersFacingPgs:Lorg/apache/poi/util/BitField;

.field private static oldfTransparentMetafiles:Lorg/apache/poi/util/BitField;

.field private static oldfWrapTrailSpaces:Lorg/apache/poi/util/BitField;

.field private static rncEdn:Lorg/apache/poi/util/BitField;

.field private static rncFtn:Lorg/apache/poi/util/BitField;

.field private static unused1:Lorg/apache/poi/util/BitField;

.field private static unused3:Lorg/apache/poi/util/BitField;

.field private static unused4:Lorg/apache/poi/util/BitField;

.field private static unused5:Lorg/apache/poi/util/BitField;

.field private static wScaleSaved:Lorg/apache/poi/util/BitField;

.field private static wvkSaved:Lorg/apache/poi/util/BitField;

.field private static zkSaved:Lorg/apache/poi/util/BitField;


# instance fields
.field private field_10_wSpare:I

.field private field_11_dxaHotz:I

.field private field_12_cConsexHypLim:I

.field private field_13_wSpare2:I

.field private field_14_dttmCreated:I

.field private field_15_dttmRevised:I

.field private field_16_dttmLastPrint:I

.field private field_17_nRevision:I

.field private field_18_tmEdited:I

.field private field_19_cWords:I

.field private field_1_formatFlags:B

.field private field_20_cCh:I

.field private field_21_cPg:I

.field private field_22_cParas:I

.field private field_23_Edn:S

.field private field_24_Edn1:S

.field private field_25_cLines:I

.field private field_26_cWordsFtnEnd:I

.field private field_27_cChFtnEdn:I

.field private field_28_cPgFtnEdn:S

.field private field_29_cParasFtnEdn:I

.field private field_2_unused2:B

.field private field_30_cLinesFtnEdn:I

.field private field_31_lKeyProtDoc:I

.field private field_32_view:S

.field private field_33_docinfo4:I

.field private field_34_adt:S

.field private field_35_doptypography:[B

.field private field_36_dogrid:[B

.field private field_37_docinfo5:S

.field private field_38_docinfo6:S

.field private field_39_asumyi:[B

.field private field_3_footnoteInfo:S

.field private field_40_cChWS:I

.field private field_41_cChWSFtnEdn:I

.field private field_42_grfDocEvents:I

.field private field_43_virusinfo:I

.field private field_44_Spare:[B

.field private field_45_reserved1:I

.field private field_46_reserved2:I

.field private field_47_cDBC:I

.field private field_48_cDBCFtnEdn:I

.field private field_49_reserved:I

.field private field_4_fOutlineDirtySave:B

.field private field_50_nfcFtnRef:S

.field private field_51_nfcEdnRef:S

.field private field_52_hpsZoonFontPag:S

.field private field_53_dywDispPag:S

.field private field_5_docinfo:B

.field private field_6_docinfo1:B

.field private field_7_docinfo2:B

.field private field_8_docinfo3:S

.field private field_9_dxaTab:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/16 v5, 0x20

    const/4 v4, 0x4

    const/16 v3, 0x80

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 41
    invoke-static {v1}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fFacingPages:Lorg/apache/poi/util/BitField;

    .line 42
    invoke-static {v2}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fWidowControl:Lorg/apache/poi/util/BitField;

    .line 43
    invoke-static {v4}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fPMHMainDoc:Lorg/apache/poi/util/BitField;

    .line 44
    const/16 v0, 0x18

    invoke-static {v0}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->grfSupression:Lorg/apache/poi/util/BitField;

    .line 45
    const/16 v0, 0x60

    invoke-static {v0}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fpc:Lorg/apache/poi/util/BitField;

    .line 46
    invoke-static {v3}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->unused1:Lorg/apache/poi/util/BitField;

    .line 49
    const/4 v0, 0x3

    invoke-static {v0}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->rncFtn:Lorg/apache/poi/util/BitField;

    .line 50
    const v0, 0xfffc

    invoke-static {v0}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->nFtn:Lorg/apache/poi/util/BitField;

    .line 53
    invoke-static {v1}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fOnlyMacPics:Lorg/apache/poi/util/BitField;

    .line 54
    invoke-static {v2}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fOnlyWinPics:Lorg/apache/poi/util/BitField;

    .line 55
    invoke-static {v4}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fLabelDoc:Lorg/apache/poi/util/BitField;

    .line 56
    const/16 v0, 0x8

    invoke-static {v0}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fHyphCapitals:Lorg/apache/poi/util/BitField;

    .line 57
    const/16 v0, 0x10

    invoke-static {v0}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fAutoHyphen:Lorg/apache/poi/util/BitField;

    .line 58
    invoke-static {v5}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fFormNoFields:Lorg/apache/poi/util/BitField;

    .line 59
    const/16 v0, 0x40

    invoke-static {v0}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fLinkStyles:Lorg/apache/poi/util/BitField;

    .line 60
    invoke-static {v3}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fRevMarking:Lorg/apache/poi/util/BitField;

    .line 62
    invoke-static {v1}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fBackup:Lorg/apache/poi/util/BitField;

    .line 63
    invoke-static {v2}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fExactCWords:Lorg/apache/poi/util/BitField;

    .line 64
    invoke-static {v4}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fPagHidden:Lorg/apache/poi/util/BitField;

    .line 65
    const/16 v0, 0x8

    invoke-static {v0}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fPagResults:Lorg/apache/poi/util/BitField;

    .line 66
    const/16 v0, 0x10

    invoke-static {v0}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fLockAtn:Lorg/apache/poi/util/BitField;

    .line 67
    invoke-static {v5}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fMirrorMargins:Lorg/apache/poi/util/BitField;

    .line 68
    const/16 v0, 0x40

    invoke-static {v0}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->unused3:Lorg/apache/poi/util/BitField;

    .line 69
    invoke-static {v3}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fDfltTrueType:Lorg/apache/poi/util/BitField;

    .line 71
    invoke-static {v1}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fPagSupressTopSpacing:Lorg/apache/poi/util/BitField;

    .line 72
    invoke-static {v2}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fProtEnabled:Lorg/apache/poi/util/BitField;

    .line 73
    invoke-static {v4}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fDispFormFldSel:Lorg/apache/poi/util/BitField;

    .line 74
    const/16 v0, 0x8

    invoke-static {v0}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fRMView:Lorg/apache/poi/util/BitField;

    .line 75
    const/16 v0, 0x10

    invoke-static {v0}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fRMPrint:Lorg/apache/poi/util/BitField;

    .line 76
    invoke-static {v5}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->unused4:Lorg/apache/poi/util/BitField;

    .line 77
    const/16 v0, 0x40

    invoke-static {v0}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fLockRev:Lorg/apache/poi/util/BitField;

    .line 78
    invoke-static {v3}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fEmbedFonts:Lorg/apache/poi/util/BitField;

    .line 80
    invoke-static {v1}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->oldfNoTabForInd:Lorg/apache/poi/util/BitField;

    .line 81
    invoke-static {v2}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->oldfNoSpaceRaiseLower:Lorg/apache/poi/util/BitField;

    .line 82
    invoke-static {v4}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->oldfSuppressSpbfAfterPageBreak:Lorg/apache/poi/util/BitField;

    .line 83
    const/16 v0, 0x8

    invoke-static {v0}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->oldfWrapTrailSpaces:Lorg/apache/poi/util/BitField;

    .line 84
    const/16 v0, 0x10

    invoke-static {v0}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->oldfMapPrintTextColor:Lorg/apache/poi/util/BitField;

    .line 85
    invoke-static {v5}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->oldfNoColumnBalance:Lorg/apache/poi/util/BitField;

    .line 86
    const/16 v0, 0x40

    invoke-static {v0}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->oldfConvMailMergeEsc:Lorg/apache/poi/util/BitField;

    .line 87
    invoke-static {v3}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->oldfSupressTopSpacing:Lorg/apache/poi/util/BitField;

    .line 88
    const/16 v0, 0x100

    invoke-static {v0}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->oldfOrigWordTableRules:Lorg/apache/poi/util/BitField;

    .line 89
    const/16 v0, 0x200

    invoke-static {v0}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->oldfTransparentMetafiles:Lorg/apache/poi/util/BitField;

    .line 90
    const/16 v0, 0x400

    invoke-static {v0}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->oldfShowBreaksInFrames:Lorg/apache/poi/util/BitField;

    .line 91
    const/16 v0, 0x800

    invoke-static {v0}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->oldfSwapBordersFacingPgs:Lorg/apache/poi/util/BitField;

    .line 92
    const v0, 0xf000

    invoke-static {v0}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->unused5:Lorg/apache/poi/util/BitField;

    .line 108
    const/4 v0, 0x3

    invoke-static {v0}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->rncEdn:Lorg/apache/poi/util/BitField;

    .line 109
    const v0, 0xfffc

    invoke-static {v0}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->nEdn:Lorg/apache/poi/util/BitField;

    .line 111
    const/4 v0, 0x3

    invoke-static {v0}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->epc:Lorg/apache/poi/util/BitField;

    .line 112
    const/16 v0, 0x3c

    invoke-static {v0}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->nfcFtnRef1:Lorg/apache/poi/util/BitField;

    .line 113
    const/16 v0, 0x3c0

    invoke-static {v0}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->nfcEdnRef1:Lorg/apache/poi/util/BitField;

    .line 114
    const/16 v0, 0x400

    invoke-static {v0}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fPrintFormData:Lorg/apache/poi/util/BitField;

    .line 115
    const/16 v0, 0x800

    invoke-static {v0}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fSaveFormData:Lorg/apache/poi/util/BitField;

    .line 116
    const/16 v0, 0x1000

    invoke-static {v0}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fShadeFormData:Lorg/apache/poi/util/BitField;

    .line 117
    const v0, 0x8000

    invoke-static {v0}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fWCFtnEdn:Lorg/apache/poi/util/BitField;

    .line 126
    const/4 v0, 0x7

    invoke-static {v0}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->wvkSaved:Lorg/apache/poi/util/BitField;

    .line 127
    const/16 v0, 0xff8

    invoke-static {v0}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->wScaleSaved:Lorg/apache/poi/util/BitField;

    .line 128
    const/16 v0, 0x3000

    invoke-static {v0}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->zkSaved:Lorg/apache/poi/util/BitField;

    .line 129
    const/16 v0, 0x4000

    invoke-static {v0}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fRotateFontW6:Lorg/apache/poi/util/BitField;

    .line 130
    const v0, 0x8000

    invoke-static {v0}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->iGutterPos:Lorg/apache/poi/util/BitField;

    .line 132
    invoke-static {v1}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fNoTabForInd:Lorg/apache/poi/util/BitField;

    .line 133
    invoke-static {v2}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fNoSpaceRaiseLower:Lorg/apache/poi/util/BitField;

    .line 134
    invoke-static {v4}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fSupressSpdfAfterPageBreak:Lorg/apache/poi/util/BitField;

    .line 135
    const/16 v0, 0x8

    invoke-static {v0}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fWrapTrailSpaces:Lorg/apache/poi/util/BitField;

    .line 136
    const/16 v0, 0x10

    invoke-static {v0}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fMapPrintTextColor:Lorg/apache/poi/util/BitField;

    .line 137
    invoke-static {v5}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fNoColumnBalance:Lorg/apache/poi/util/BitField;

    .line 138
    const/16 v0, 0x40

    invoke-static {v0}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fConvMailMergeEsc:Lorg/apache/poi/util/BitField;

    .line 139
    invoke-static {v3}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fSupressTopSpacing:Lorg/apache/poi/util/BitField;

    .line 140
    const/16 v0, 0x100

    invoke-static {v0}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fOrigWordTableRules:Lorg/apache/poi/util/BitField;

    .line 141
    const/16 v0, 0x200

    invoke-static {v0}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fTransparentMetafiles:Lorg/apache/poi/util/BitField;

    .line 142
    const/16 v0, 0x400

    invoke-static {v0}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fShowBreaksInFrames:Lorg/apache/poi/util/BitField;

    .line 143
    const/16 v0, 0x800

    invoke-static {v0}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fSwapBordersFacingPgs:Lorg/apache/poi/util/BitField;

    .line 144
    const/high16 v0, 0x10000

    invoke-static {v0}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fSuppressTopSPacingMac5:Lorg/apache/poi/util/BitField;

    .line 145
    const/high16 v0, 0x20000

    invoke-static {v0}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fTruncDxaExpand:Lorg/apache/poi/util/BitField;

    .line 146
    const/high16 v0, 0x40000

    invoke-static {v0}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fPrintBodyBeforeHdr:Lorg/apache/poi/util/BitField;

    .line 147
    const/high16 v0, 0x80000

    invoke-static {v0}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fNoLeading:Lorg/apache/poi/util/BitField;

    .line 148
    const/high16 v0, 0x200000

    invoke-static {v0}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fMWSmallCaps:Lorg/apache/poi/util/BitField;

    .line 153
    const/16 v0, 0x1e

    invoke-static {v0}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->lvl:Lorg/apache/poi/util/BitField;

    .line 154
    invoke-static {v5}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fGramAllDone:Lorg/apache/poi/util/BitField;

    .line 155
    const/16 v0, 0x40

    invoke-static {v0}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fGramAllClean:Lorg/apache/poi/util/BitField;

    .line 156
    invoke-static {v3}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fSubsetFonts:Lorg/apache/poi/util/BitField;

    .line 157
    const/16 v0, 0x100

    invoke-static {v0}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fHideLastVersion:Lorg/apache/poi/util/BitField;

    .line 158
    const/16 v0, 0x200

    invoke-static {v0}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fHtmlDoc:Lorg/apache/poi/util/BitField;

    .line 159
    const/16 v0, 0x800

    invoke-static {v0}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fSnapBorder:Lorg/apache/poi/util/BitField;

    .line 160
    const/16 v0, 0x1000

    invoke-static {v0}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fIncludeHeader:Lorg/apache/poi/util/BitField;

    .line 161
    const/16 v0, 0x2000

    invoke-static {v0}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fIncludeFooter:Lorg/apache/poi/util/BitField;

    .line 162
    const/16 v0, 0x4000

    invoke-static {v0}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fForcePageSizePag:Lorg/apache/poi/util/BitField;

    .line 163
    const v0, 0x8000

    invoke-static {v0}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fMinFontSizePag:Lorg/apache/poi/util/BitField;

    .line 165
    invoke-static {v1}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fHaveVersions:Lorg/apache/poi/util/BitField;

    .line 166
    invoke-static {v2}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fAutoVersions:Lorg/apache/poi/util/BitField;

    .line 172
    invoke-static {v1}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fVirusPrompted:Lorg/apache/poi/util/BitField;

    .line 173
    invoke-static {v2}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fVirusLoadSafe:Lorg/apache/poi/util/BitField;

    .line 174
    const/4 v0, -0x4

    invoke-static {v0}, Lorg/apache/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->KeyVirusSession30:Lorg/apache/poi/util/BitField;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 187
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 190
    return-void
.end method


# virtual methods
.method protected fillFields([BSI)V
    .locals 2
    .param p1, "data"    # [B
    .param p2, "size"    # S
    .param p3, "offset"    # I

    .prologue
    .line 194
    add-int/lit8 v0, p3, 0x0

    aget-byte v0, p1, v0

    iput-byte v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_1_formatFlags:B

    .line 195
    add-int/lit8 v0, p3, 0x1

    aget-byte v0, p1, v0

    iput-byte v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_2_unused2:B

    .line 196
    add-int/lit8 v0, p3, 0x2

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v0

    iput-short v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_3_footnoteInfo:S

    .line 197
    add-int/lit8 v0, p3, 0x4

    aget-byte v0, p1, v0

    iput-byte v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_4_fOutlineDirtySave:B

    .line 198
    add-int/lit8 v0, p3, 0x5

    aget-byte v0, p1, v0

    iput-byte v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_5_docinfo:B

    .line 199
    add-int/lit8 v0, p3, 0x6

    aget-byte v0, p1, v0

    iput-byte v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_6_docinfo1:B

    .line 200
    add-int/lit8 v0, p3, 0x7

    aget-byte v0, p1, v0

    iput-byte v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_7_docinfo2:B

    .line 201
    add-int/lit8 v0, p3, 0x8

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v0

    iput-short v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_8_docinfo3:S

    .line 202
    add-int/lit8 v0, p3, 0xa

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v0

    iput v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_9_dxaTab:I

    .line 203
    add-int/lit8 v0, p3, 0xc

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v0

    iput v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_10_wSpare:I

    .line 204
    add-int/lit8 v0, p3, 0xe

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v0

    iput v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_11_dxaHotz:I

    .line 205
    add-int/lit8 v0, p3, 0x10

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v0

    iput v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_12_cConsexHypLim:I

    .line 206
    add-int/lit8 v0, p3, 0x12

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v0

    iput v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_13_wSpare2:I

    .line 207
    add-int/lit8 v0, p3, 0x14

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_14_dttmCreated:I

    .line 208
    add-int/lit8 v0, p3, 0x18

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_15_dttmRevised:I

    .line 209
    add-int/lit8 v0, p3, 0x1c

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_16_dttmLastPrint:I

    .line 210
    add-int/lit8 v0, p3, 0x20

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v0

    iput v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_17_nRevision:I

    .line 211
    add-int/lit8 v0, p3, 0x22

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_18_tmEdited:I

    .line 212
    add-int/lit8 v0, p3, 0x26

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_19_cWords:I

    .line 213
    add-int/lit8 v0, p3, 0x2a

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_20_cCh:I

    .line 214
    add-int/lit8 v0, p3, 0x2e

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v0

    iput v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_21_cPg:I

    .line 215
    add-int/lit8 v0, p3, 0x30

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_22_cParas:I

    .line 216
    add-int/lit8 v0, p3, 0x34

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v0

    iput-short v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_23_Edn:S

    .line 217
    add-int/lit8 v0, p3, 0x36

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v0

    iput-short v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_24_Edn1:S

    .line 218
    add-int/lit8 v0, p3, 0x38

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_25_cLines:I

    .line 219
    add-int/lit8 v0, p3, 0x3c

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_26_cWordsFtnEnd:I

    .line 220
    add-int/lit8 v0, p3, 0x40

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_27_cChFtnEdn:I

    .line 221
    add-int/lit8 v0, p3, 0x44

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v0

    iput-short v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_28_cPgFtnEdn:S

    .line 222
    add-int/lit8 v0, p3, 0x46

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_29_cParasFtnEdn:I

    .line 223
    add-int/lit8 v0, p3, 0x4a

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_30_cLinesFtnEdn:I

    .line 224
    add-int/lit8 v0, p3, 0x4e

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_31_lKeyProtDoc:I

    .line 225
    add-int/lit8 v0, p3, 0x52

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v0

    iput-short v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_32_view:S

    .line 226
    add-int/lit8 v0, p3, 0x54

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_33_docinfo4:I

    .line 227
    add-int/lit8 v0, p3, 0x58

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v0

    iput-short v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_34_adt:S

    .line 228
    add-int/lit8 v0, p3, 0x5a

    const/16 v1, 0x136

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->getByteArray([BII)[B

    move-result-object v0

    iput-object v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_35_doptypography:[B

    .line 229
    add-int/lit16 v0, p3, 0x190

    const/16 v1, 0xa

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->getByteArray([BII)[B

    move-result-object v0

    iput-object v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_36_dogrid:[B

    .line 230
    add-int/lit16 v0, p3, 0x19a

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v0

    iput-short v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_37_docinfo5:S

    .line 231
    add-int/lit16 v0, p3, 0x19c

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v0

    iput-short v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_38_docinfo6:S

    .line 232
    add-int/lit16 v0, p3, 0x19e

    const/16 v1, 0xc

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->getByteArray([BII)[B

    move-result-object v0

    iput-object v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_39_asumyi:[B

    .line 233
    add-int/lit16 v0, p3, 0x1aa

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_40_cChWS:I

    .line 234
    add-int/lit16 v0, p3, 0x1ae

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_41_cChWSFtnEdn:I

    .line 235
    add-int/lit16 v0, p3, 0x1b2

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_42_grfDocEvents:I

    .line 236
    add-int/lit16 v0, p3, 0x1b6

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_43_virusinfo:I

    .line 237
    add-int/lit16 v0, p3, 0x1ba

    const/16 v1, 0x1e

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->getByteArray([BII)[B

    move-result-object v0

    iput-object v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_44_Spare:[B

    .line 238
    add-int/lit16 v0, p3, 0x1d8

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_45_reserved1:I

    .line 239
    add-int/lit16 v0, p3, 0x1dc

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_46_reserved2:I

    .line 240
    add-int/lit16 v0, p3, 0x1e0

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_47_cDBC:I

    .line 241
    add-int/lit16 v0, p3, 0x1e4

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_48_cDBCFtnEdn:I

    .line 242
    add-int/lit16 v0, p3, 0x1e8

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_49_reserved:I

    .line 243
    add-int/lit16 v0, p3, 0x1ec

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v0

    iput-short v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_50_nfcFtnRef:S

    .line 244
    add-int/lit16 v0, p3, 0x1ee

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v0

    iput-short v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_51_nfcEdnRef:S

    .line 245
    add-int/lit16 v0, p3, 0x1f0

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v0

    iput-short v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_52_hpsZoonFontPag:S

    .line 246
    add-int/lit16 v0, p3, 0x1f2

    invoke-static {p1, v0}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v0

    iput-short v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_53_dywDispPag:S

    .line 247
    return-void
.end method

.method public getAdt()S
    .locals 1

    .prologue
    .line 1163
    iget-short v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_34_adt:S

    return v0
.end method

.method public getAsumyi()[B
    .locals 1

    .prologue
    .line 1243
    iget-object v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_39_asumyi:[B

    return-object v0
.end method

.method public getCCh()I
    .locals 1

    .prologue
    .line 939
    iget v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_20_cCh:I

    return v0
.end method

.method public getCChFtnEdn()I
    .locals 1

    .prologue
    .line 1051
    iget v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_27_cChFtnEdn:I

    return v0
.end method

.method public getCChWS()I
    .locals 1

    .prologue
    .line 1259
    iget v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_40_cChWS:I

    return v0
.end method

.method public getCChWSFtnEdn()I
    .locals 1

    .prologue
    .line 1275
    iget v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_41_cChWSFtnEdn:I

    return v0
.end method

.method public getCConsexHypLim()I
    .locals 1

    .prologue
    .line 811
    iget v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_12_cConsexHypLim:I

    return v0
.end method

.method public getCDBC()I
    .locals 1

    .prologue
    .line 1371
    iget v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_47_cDBC:I

    return v0
.end method

.method public getCDBCFtnEdn()I
    .locals 1

    .prologue
    .line 1387
    iget v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_48_cDBCFtnEdn:I

    return v0
.end method

.method public getCLines()I
    .locals 1

    .prologue
    .line 1019
    iget v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_25_cLines:I

    return v0
.end method

.method public getCLinesFtnEdn()I
    .locals 1

    .prologue
    .line 1099
    iget v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_30_cLinesFtnEdn:I

    return v0
.end method

.method public getCParas()I
    .locals 1

    .prologue
    .line 971
    iget v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_22_cParas:I

    return v0
.end method

.method public getCParasFtnEdn()I
    .locals 1

    .prologue
    .line 1083
    iget v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_29_cParasFtnEdn:I

    return v0
.end method

.method public getCPg()I
    .locals 1

    .prologue
    .line 955
    iget v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_21_cPg:I

    return v0
.end method

.method public getCPgFtnEdn()S
    .locals 1

    .prologue
    .line 1067
    iget-short v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_28_cPgFtnEdn:S

    return v0
.end method

.method public getCWords()I
    .locals 1

    .prologue
    .line 923
    iget v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_19_cWords:I

    return v0
.end method

.method public getCWordsFtnEnd()I
    .locals 1

    .prologue
    .line 1035
    iget v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_26_cWordsFtnEnd:I

    return v0
.end method

.method public getDocinfo()B
    .locals 1

    .prologue
    .line 699
    iget-byte v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_5_docinfo:B

    return v0
.end method

.method public getDocinfo1()B
    .locals 1

    .prologue
    .line 715
    iget-byte v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_6_docinfo1:B

    return v0
.end method

.method public getDocinfo2()B
    .locals 1

    .prologue
    .line 731
    iget-byte v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_7_docinfo2:B

    return v0
.end method

.method public getDocinfo3()S
    .locals 1

    .prologue
    .line 747
    iget-short v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_8_docinfo3:S

    return v0
.end method

.method public getDocinfo4()I
    .locals 1

    .prologue
    .line 1147
    iget v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_33_docinfo4:I

    return v0
.end method

.method public getDocinfo5()S
    .locals 1

    .prologue
    .line 1211
    iget-short v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_37_docinfo5:S

    return v0
.end method

.method public getDocinfo6()S
    .locals 1

    .prologue
    .line 1227
    iget-short v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_38_docinfo6:S

    return v0
.end method

.method public getDogrid()[B
    .locals 1

    .prologue
    .line 1195
    iget-object v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_36_dogrid:[B

    return-object v0
.end method

.method public getDoptypography()[B
    .locals 1

    .prologue
    .line 1179
    iget-object v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_35_doptypography:[B

    return-object v0
.end method

.method public getDttmCreated()I
    .locals 1

    .prologue
    .line 843
    iget v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_14_dttmCreated:I

    return v0
.end method

.method public getDttmLastPrint()I
    .locals 1

    .prologue
    .line 875
    iget v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_16_dttmLastPrint:I

    return v0
.end method

.method public getDttmRevised()I
    .locals 1

    .prologue
    .line 859
    iget v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_15_dttmRevised:I

    return v0
.end method

.method public getDxaHotz()I
    .locals 1

    .prologue
    .line 795
    iget v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_11_dxaHotz:I

    return v0
.end method

.method public getDxaTab()I
    .locals 1

    .prologue
    .line 763
    iget v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_9_dxaTab:I

    return v0
.end method

.method public getDywDispPag()S
    .locals 1

    .prologue
    .line 1467
    iget-short v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_53_dywDispPag:S

    return v0
.end method

.method public getEdn()S
    .locals 1

    .prologue
    .line 987
    iget-short v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_23_Edn:S

    return v0
.end method

.method public getEdn1()S
    .locals 1

    .prologue
    .line 1003
    iget-short v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_24_Edn1:S

    return v0
.end method

.method public getEpc()B
    .locals 2

    .prologue
    .line 2339
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->epc:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_24_Edn1:S

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->getValue(I)I

    move-result v0

    int-to-byte v0, v0

    return v0
.end method

.method public getFOutlineDirtySave()B
    .locals 1

    .prologue
    .line 683
    iget-byte v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_4_fOutlineDirtySave:B

    return v0
.end method

.method public getFootnoteInfo()S
    .locals 1

    .prologue
    .line 667
    iget-short v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_3_footnoteInfo:S

    return v0
.end method

.method public getFormatFlags()B
    .locals 1

    .prologue
    .line 635
    iget-byte v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_1_formatFlags:B

    return v0
.end method

.method public getFpc()B
    .locals 2

    .prologue
    .line 1565
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fpc:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_1_formatFlags:B

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->getValue(I)I

    move-result v0

    int-to-byte v0, v0

    return v0
.end method

.method public getGrfDocEvents()I
    .locals 1

    .prologue
    .line 1291
    iget v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_42_grfDocEvents:I

    return v0
.end method

.method public getGrfSupression()B
    .locals 2

    .prologue
    .line 1547
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->grfSupression:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_1_formatFlags:B

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->getValue(I)I

    move-result v0

    int-to-byte v0, v0

    return v0
.end method

.method public getHpsZoonFontPag()S
    .locals 1

    .prologue
    .line 1451
    iget-short v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_52_hpsZoonFontPag:S

    return v0
.end method

.method public getKeyVirusSession30()I
    .locals 2

    .prologue
    .line 3131
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->KeyVirusSession30:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_43_virusinfo:I

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->getValue(I)I

    move-result v0

    return v0
.end method

.method public getLKeyProtDoc()I
    .locals 1

    .prologue
    .line 1115
    iget v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_31_lKeyProtDoc:I

    return v0
.end method

.method public getLvl()B
    .locals 2

    .prologue
    .line 2861
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->lvl:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_37_docinfo5:S

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->getValue(I)I

    move-result v0

    int-to-byte v0, v0

    return v0
.end method

.method public getNEdn()S
    .locals 2

    .prologue
    .line 2321
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->nEdn:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_23_Edn:S

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->getValue(I)I

    move-result v0

    int-to-short v0, v0

    return v0
.end method

.method public getNFtn()S
    .locals 2

    .prologue
    .line 1619
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->nFtn:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_3_footnoteInfo:S

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->getValue(I)I

    move-result v0

    int-to-short v0, v0

    return v0
.end method

.method public getNRevision()I
    .locals 1

    .prologue
    .line 891
    iget v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_17_nRevision:I

    return v0
.end method

.method public getNfcEdnRef()S
    .locals 1

    .prologue
    .line 1435
    iget-short v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_51_nfcEdnRef:S

    return v0
.end method

.method public getNfcEdnRef1()B
    .locals 2

    .prologue
    .line 2375
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->nfcEdnRef1:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_24_Edn1:S

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->getValue(I)I

    move-result v0

    int-to-byte v0, v0

    return v0
.end method

.method public getNfcFtnRef()S
    .locals 1

    .prologue
    .line 1419
    iget-short v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_50_nfcFtnRef:S

    return v0
.end method

.method public getNfcFtnRef1()B
    .locals 2

    .prologue
    .line 2357
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->nfcFtnRef1:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_24_Edn1:S

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->getValue(I)I

    move-result v0

    int-to-byte v0, v0

    return v0
.end method

.method public getReserved()I
    .locals 1

    .prologue
    .line 1403
    iget v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_49_reserved:I

    return v0
.end method

.method public getReserved1()I
    .locals 1

    .prologue
    .line 1339
    iget v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_45_reserved1:I

    return v0
.end method

.method public getReserved2()I
    .locals 1

    .prologue
    .line 1355
    iget v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_46_reserved2:I

    return v0
.end method

.method public getRncEdn()B
    .locals 2

    .prologue
    .line 2303
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->rncEdn:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_23_Edn:S

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->getValue(I)I

    move-result v0

    int-to-byte v0, v0

    return v0
.end method

.method public getRncFtn()B
    .locals 2

    .prologue
    .line 1601
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->rncFtn:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_3_footnoteInfo:S

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->getValue(I)I

    move-result v0

    int-to-byte v0, v0

    return v0
.end method

.method public getSize()I
    .locals 1

    .prologue
    .line 625
    const/16 v0, 0x1f8

    return v0
.end method

.method public getSpare()[B
    .locals 1

    .prologue
    .line 1323
    iget-object v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_44_Spare:[B

    return-object v0
.end method

.method public getTmEdited()I
    .locals 1

    .prologue
    .line 907
    iget v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_18_tmEdited:I

    return v0
.end method

.method public getUnused2()B
    .locals 1

    .prologue
    .line 651
    iget-byte v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_2_unused2:B

    return v0
.end method

.method public getUnused5()B
    .locals 2

    .prologue
    .line 2285
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->unused5:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_8_docinfo3:S

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->getValue(I)I

    move-result v0

    int-to-byte v0, v0

    return v0
.end method

.method public getView()S
    .locals 1

    .prologue
    .line 1131
    iget-short v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_32_view:S

    return v0
.end method

.method public getVirusinfo()I
    .locals 1

    .prologue
    .line 1307
    iget v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_43_virusinfo:I

    return v0
.end method

.method public getWScaleSaved()S
    .locals 2

    .prologue
    .line 2483
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->wScaleSaved:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_32_view:S

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->getValue(I)I

    move-result v0

    int-to-short v0, v0

    return v0
.end method

.method public getWSpare()I
    .locals 1

    .prologue
    .line 779
    iget v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_10_wSpare:I

    return v0
.end method

.method public getWSpare2()I
    .locals 1

    .prologue
    .line 827
    iget v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_13_wSpare2:I

    return v0
.end method

.method public getWvkSaved()B
    .locals 2

    .prologue
    .line 2465
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->wvkSaved:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_32_view:S

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->getValue(I)I

    move-result v0

    int-to-byte v0, v0

    return v0
.end method

.method public getZkSaved()B
    .locals 2

    .prologue
    .line 2501
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->zkSaved:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_32_view:S

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->getValue(I)I

    move-result v0

    int-to-byte v0, v0

    return v0
.end method

.method public isFAutoHyphen()Z
    .locals 2

    .prologue
    .line 1709
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fAutoHyphen:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_5_docinfo:B

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFAutoVersions()Z
    .locals 2

    .prologue
    .line 3077
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fAutoVersions:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_38_docinfo6:S

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFBackup()Z
    .locals 2

    .prologue
    .line 1781
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fBackup:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_6_docinfo1:B

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFConvMailMergeEsc()Z
    .locals 2

    .prologue
    .line 2663
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fConvMailMergeEsc:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_33_docinfo4:I

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFDfltTrueType()Z
    .locals 2

    .prologue
    .line 1907
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fDfltTrueType:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_6_docinfo1:B

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFDispFormFldSel()Z
    .locals 2

    .prologue
    .line 1961
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fDispFormFldSel:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_7_docinfo2:B

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFEmbedFonts()Z
    .locals 2

    .prologue
    .line 2051
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fEmbedFonts:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_7_docinfo2:B

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFExactCWords()Z
    .locals 2

    .prologue
    .line 1799
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fExactCWords:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_6_docinfo1:B

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFFacingPages()Z
    .locals 2

    .prologue
    .line 1493
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fFacingPages:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_1_formatFlags:B

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFForcePageSizePag()Z
    .locals 2

    .prologue
    .line 3023
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fForcePageSizePag:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_37_docinfo5:S

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFFormNoFields()Z
    .locals 2

    .prologue
    .line 1727
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fFormNoFields:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_5_docinfo:B

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFGramAllClean()Z
    .locals 2

    .prologue
    .line 2897
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fGramAllClean:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_37_docinfo5:S

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFGramAllDone()Z
    .locals 2

    .prologue
    .line 2879
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fGramAllDone:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_37_docinfo5:S

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFHaveVersions()Z
    .locals 2

    .prologue
    .line 3059
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fHaveVersions:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_38_docinfo6:S

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFHideLastVersion()Z
    .locals 2

    .prologue
    .line 2933
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fHideLastVersion:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_37_docinfo5:S

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFHtmlDoc()Z
    .locals 2

    .prologue
    .line 2951
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fHtmlDoc:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_37_docinfo5:S

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFHyphCapitals()Z
    .locals 2

    .prologue
    .line 1691
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fHyphCapitals:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_5_docinfo:B

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFIncludeFooter()Z
    .locals 2

    .prologue
    .line 3005
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fIncludeFooter:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_37_docinfo5:S

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFIncludeHeader()Z
    .locals 2

    .prologue
    .line 2987
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fIncludeHeader:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_37_docinfo5:S

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFLabelDoc()Z
    .locals 2

    .prologue
    .line 1673
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fLabelDoc:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_5_docinfo:B

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFLinkStyles()Z
    .locals 2

    .prologue
    .line 1745
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fLinkStyles:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_5_docinfo:B

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFLockAtn()Z
    .locals 2

    .prologue
    .line 1853
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fLockAtn:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_6_docinfo1:B

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFLockRev()Z
    .locals 2

    .prologue
    .line 2033
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fLockRev:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_7_docinfo2:B

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFMWSmallCaps()Z
    .locals 2

    .prologue
    .line 2843
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fMWSmallCaps:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_33_docinfo4:I

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFMapPrintTextColor()Z
    .locals 2

    .prologue
    .line 2627
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fMapPrintTextColor:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_33_docinfo4:I

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFMinFontSizePag()Z
    .locals 2

    .prologue
    .line 3041
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fMinFontSizePag:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_37_docinfo5:S

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFMirrorMargins()Z
    .locals 2

    .prologue
    .line 1871
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fMirrorMargins:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_6_docinfo1:B

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFNoColumnBalance()Z
    .locals 2

    .prologue
    .line 2645
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fNoColumnBalance:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_33_docinfo4:I

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFNoLeading()Z
    .locals 2

    .prologue
    .line 2825
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fNoLeading:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_33_docinfo4:I

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFNoSpaceRaiseLower()Z
    .locals 2

    .prologue
    .line 2573
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fNoSpaceRaiseLower:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_33_docinfo4:I

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFNoTabForInd()Z
    .locals 2

    .prologue
    .line 2555
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fNoTabForInd:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_33_docinfo4:I

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFOnlyMacPics()Z
    .locals 2

    .prologue
    .line 1637
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fOnlyMacPics:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_5_docinfo:B

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFOnlyWinPics()Z
    .locals 2

    .prologue
    .line 1655
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fOnlyWinPics:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_5_docinfo:B

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFOrigWordTableRules()Z
    .locals 2

    .prologue
    .line 2699
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fOrigWordTableRules:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_33_docinfo4:I

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFPMHMainDoc()Z
    .locals 2

    .prologue
    .line 1529
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fPMHMainDoc:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_1_formatFlags:B

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFPagHidden()Z
    .locals 2

    .prologue
    .line 1817
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fPagHidden:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_6_docinfo1:B

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFPagResults()Z
    .locals 2

    .prologue
    .line 1835
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fPagResults:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_6_docinfo1:B

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFPagSupressTopSpacing()Z
    .locals 2

    .prologue
    .line 1925
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fPagSupressTopSpacing:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_7_docinfo2:B

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFPrintBodyBeforeHdr()Z
    .locals 2

    .prologue
    .line 2807
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fPrintBodyBeforeHdr:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_33_docinfo4:I

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFPrintFormData()Z
    .locals 2

    .prologue
    .line 2393
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fPrintFormData:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_24_Edn1:S

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFProtEnabled()Z
    .locals 2

    .prologue
    .line 1943
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fProtEnabled:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_7_docinfo2:B

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFRMPrint()Z
    .locals 2

    .prologue
    .line 1997
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fRMPrint:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_7_docinfo2:B

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFRMView()Z
    .locals 2

    .prologue
    .line 1979
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fRMView:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_7_docinfo2:B

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFRevMarking()Z
    .locals 2

    .prologue
    .line 1763
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fRevMarking:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_5_docinfo:B

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFRotateFontW6()Z
    .locals 2

    .prologue
    .line 2519
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fRotateFontW6:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_32_view:S

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFSaveFormData()Z
    .locals 2

    .prologue
    .line 2411
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fSaveFormData:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_24_Edn1:S

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFShadeFormData()Z
    .locals 2

    .prologue
    .line 2429
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fShadeFormData:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_24_Edn1:S

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFShowBreaksInFrames()Z
    .locals 2

    .prologue
    .line 2735
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fShowBreaksInFrames:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_33_docinfo4:I

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFSnapBorder()Z
    .locals 2

    .prologue
    .line 2969
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fSnapBorder:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_37_docinfo5:S

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFSubsetFonts()Z
    .locals 2

    .prologue
    .line 2915
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fSubsetFonts:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_37_docinfo5:S

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFSuppressTopSPacingMac5()Z
    .locals 2

    .prologue
    .line 2771
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fSuppressTopSPacingMac5:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_33_docinfo4:I

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFSupressSpdfAfterPageBreak()Z
    .locals 2

    .prologue
    .line 2591
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fSupressSpdfAfterPageBreak:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_33_docinfo4:I

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFSupressTopSpacing()Z
    .locals 2

    .prologue
    .line 2681
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fSupressTopSpacing:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_33_docinfo4:I

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFSwapBordersFacingPgs()Z
    .locals 2

    .prologue
    .line 2753
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fSwapBordersFacingPgs:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_33_docinfo4:I

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFTransparentMetafiles()Z
    .locals 2

    .prologue
    .line 2717
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fTransparentMetafiles:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_33_docinfo4:I

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFTruncDxaExpand()Z
    .locals 2

    .prologue
    .line 2789
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fTruncDxaExpand:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_33_docinfo4:I

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFVirusLoadSafe()Z
    .locals 2

    .prologue
    .line 3113
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fVirusLoadSafe:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_43_virusinfo:I

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFVirusPrompted()Z
    .locals 2

    .prologue
    .line 3095
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fVirusPrompted:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_43_virusinfo:I

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFWCFtnEdn()Z
    .locals 2

    .prologue
    .line 2447
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fWCFtnEdn:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_24_Edn1:S

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFWidowControl()Z
    .locals 2

    .prologue
    .line 1511
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fWidowControl:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_1_formatFlags:B

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFWrapTrailSpaces()Z
    .locals 2

    .prologue
    .line 2609
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fWrapTrailSpaces:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_33_docinfo4:I

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isIGutterPos()Z
    .locals 2

    .prologue
    .line 2537
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->iGutterPos:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_32_view:S

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isOldfConvMailMergeEsc()Z
    .locals 2

    .prologue
    .line 2177
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->oldfConvMailMergeEsc:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_8_docinfo3:S

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isOldfMapPrintTextColor()Z
    .locals 2

    .prologue
    .line 2141
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->oldfMapPrintTextColor:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_8_docinfo3:S

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isOldfNoColumnBalance()Z
    .locals 2

    .prologue
    .line 2159
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->oldfNoColumnBalance:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_8_docinfo3:S

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isOldfNoSpaceRaiseLower()Z
    .locals 2

    .prologue
    .line 2087
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->oldfNoSpaceRaiseLower:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_8_docinfo3:S

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isOldfNoTabForInd()Z
    .locals 2

    .prologue
    .line 2069
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->oldfNoTabForInd:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_8_docinfo3:S

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isOldfOrigWordTableRules()Z
    .locals 2

    .prologue
    .line 2213
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->oldfOrigWordTableRules:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_8_docinfo3:S

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isOldfShowBreaksInFrames()Z
    .locals 2

    .prologue
    .line 2249
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->oldfShowBreaksInFrames:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_8_docinfo3:S

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isOldfSuppressSpbfAfterPageBreak()Z
    .locals 2

    .prologue
    .line 2105
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->oldfSuppressSpbfAfterPageBreak:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_8_docinfo3:S

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isOldfSupressTopSpacing()Z
    .locals 2

    .prologue
    .line 2195
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->oldfSupressTopSpacing:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_8_docinfo3:S

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isOldfSwapBordersFacingPgs()Z
    .locals 2

    .prologue
    .line 2267
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->oldfSwapBordersFacingPgs:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_8_docinfo3:S

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isOldfTransparentMetafiles()Z
    .locals 2

    .prologue
    .line 2231
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->oldfTransparentMetafiles:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_8_docinfo3:S

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isOldfWrapTrailSpaces()Z
    .locals 2

    .prologue
    .line 2123
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->oldfWrapTrailSpaces:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_8_docinfo3:S

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isUnused1()Z
    .locals 2

    .prologue
    .line 1583
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->unused1:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_1_formatFlags:B

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isUnused3()Z
    .locals 2

    .prologue
    .line 1889
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->unused3:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_6_docinfo1:B

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isUnused4()Z
    .locals 2

    .prologue
    .line 2015
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->unused4:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_7_docinfo2:B

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public serialize([BI)V
    .locals 2
    .param p1, "data"    # [B
    .param p2, "offset"    # I

    .prologue
    .line 251
    add-int/lit8 v0, p2, 0x0

    iget-byte v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_1_formatFlags:B

    aput-byte v1, p1, v0

    .line 252
    add-int/lit8 v0, p2, 0x1

    iget-byte v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_2_unused2:B

    aput-byte v1, p1, v0

    .line 253
    add-int/lit8 v0, p2, 0x2

    iget-short v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_3_footnoteInfo:S

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putShort([BIS)V

    .line 254
    add-int/lit8 v0, p2, 0x4

    iget-byte v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_4_fOutlineDirtySave:B

    aput-byte v1, p1, v0

    .line 255
    add-int/lit8 v0, p2, 0x5

    iget-byte v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_5_docinfo:B

    aput-byte v1, p1, v0

    .line 256
    add-int/lit8 v0, p2, 0x6

    iget-byte v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_6_docinfo1:B

    aput-byte v1, p1, v0

    .line 257
    add-int/lit8 v0, p2, 0x7

    iget-byte v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_7_docinfo2:B

    aput-byte v1, p1, v0

    .line 258
    add-int/lit8 v0, p2, 0x8

    iget-short v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_8_docinfo3:S

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putShort([BIS)V

    .line 259
    add-int/lit8 v0, p2, 0xa

    iget v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_9_dxaTab:I

    int-to-short v1, v1

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putShort([BIS)V

    .line 260
    add-int/lit8 v0, p2, 0xc

    iget v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_10_wSpare:I

    int-to-short v1, v1

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putShort([BIS)V

    .line 261
    add-int/lit8 v0, p2, 0xe

    iget v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_11_dxaHotz:I

    int-to-short v1, v1

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putShort([BIS)V

    .line 262
    add-int/lit8 v0, p2, 0x10

    iget v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_12_cConsexHypLim:I

    int-to-short v1, v1

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putShort([BIS)V

    .line 263
    add-int/lit8 v0, p2, 0x12

    iget v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_13_wSpare2:I

    int-to-short v1, v1

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putShort([BIS)V

    .line 264
    add-int/lit8 v0, p2, 0x14

    iget v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_14_dttmCreated:I

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 265
    add-int/lit8 v0, p2, 0x18

    iget v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_15_dttmRevised:I

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 266
    add-int/lit8 v0, p2, 0x1c

    iget v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_16_dttmLastPrint:I

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 267
    add-int/lit8 v0, p2, 0x20

    iget v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_17_nRevision:I

    int-to-short v1, v1

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putShort([BIS)V

    .line 268
    add-int/lit8 v0, p2, 0x22

    iget v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_18_tmEdited:I

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 269
    add-int/lit8 v0, p2, 0x26

    iget v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_19_cWords:I

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 270
    add-int/lit8 v0, p2, 0x2a

    iget v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_20_cCh:I

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 271
    add-int/lit8 v0, p2, 0x2e

    iget v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_21_cPg:I

    int-to-short v1, v1

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putShort([BIS)V

    .line 272
    add-int/lit8 v0, p2, 0x30

    iget v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_22_cParas:I

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 273
    add-int/lit8 v0, p2, 0x34

    iget-short v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_23_Edn:S

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putShort([BIS)V

    .line 274
    add-int/lit8 v0, p2, 0x36

    iget-short v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_24_Edn1:S

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putShort([BIS)V

    .line 275
    add-int/lit8 v0, p2, 0x38

    iget v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_25_cLines:I

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 276
    add-int/lit8 v0, p2, 0x3c

    iget v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_26_cWordsFtnEnd:I

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 277
    add-int/lit8 v0, p2, 0x40

    iget v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_27_cChFtnEdn:I

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 278
    add-int/lit8 v0, p2, 0x44

    iget-short v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_28_cPgFtnEdn:S

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putShort([BIS)V

    .line 279
    add-int/lit8 v0, p2, 0x46

    iget v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_29_cParasFtnEdn:I

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 280
    add-int/lit8 v0, p2, 0x4a

    iget v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_30_cLinesFtnEdn:I

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 281
    add-int/lit8 v0, p2, 0x4e

    iget v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_31_lKeyProtDoc:I

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 282
    add-int/lit8 v0, p2, 0x52

    iget-short v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_32_view:S

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putShort([BIS)V

    .line 283
    add-int/lit8 v0, p2, 0x54

    iget v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_33_docinfo4:I

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 284
    add-int/lit8 v0, p2, 0x58

    iget-short v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_34_adt:S

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putShort([BIS)V

    .line 287
    add-int/lit16 v0, p2, 0x19a

    iget-short v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_37_docinfo5:S

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putShort([BIS)V

    .line 288
    add-int/lit16 v0, p2, 0x19c

    iget-short v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_38_docinfo6:S

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putShort([BIS)V

    .line 290
    add-int/lit16 v0, p2, 0x1aa

    iget v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_40_cChWS:I

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 291
    add-int/lit16 v0, p2, 0x1ae

    iget v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_41_cChWSFtnEdn:I

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 292
    add-int/lit16 v0, p2, 0x1b2

    iget v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_42_grfDocEvents:I

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 293
    add-int/lit16 v0, p2, 0x1b6

    iget v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_43_virusinfo:I

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 295
    add-int/lit16 v0, p2, 0x1d8

    iget v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_45_reserved1:I

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 296
    add-int/lit16 v0, p2, 0x1dc

    iget v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_46_reserved2:I

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 297
    add-int/lit16 v0, p2, 0x1e0

    iget v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_47_cDBC:I

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 298
    add-int/lit16 v0, p2, 0x1e4

    iget v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_48_cDBCFtnEdn:I

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 299
    add-int/lit16 v0, p2, 0x1e8

    iget v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_49_reserved:I

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 300
    add-int/lit16 v0, p2, 0x1ec

    iget-short v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_50_nfcFtnRef:S

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putShort([BIS)V

    .line 301
    add-int/lit16 v0, p2, 0x1ee

    iget-short v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_51_nfcEdnRef:S

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putShort([BIS)V

    .line 302
    add-int/lit16 v0, p2, 0x1f0

    iget-short v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_52_hpsZoonFontPag:S

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putShort([BIS)V

    .line 303
    add-int/lit16 v0, p2, 0x1f2

    iget-short v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_53_dywDispPag:S

    invoke-static {p1, v0, v1}, Lorg/apache/poi/util/LittleEndian;->putShort([BIS)V

    .line 304
    return-void
.end method

.method public setAdt(S)V
    .locals 0
    .param p1, "field_34_adt"    # S

    .prologue
    .line 1171
    iput-short p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_34_adt:S

    .line 1172
    return-void
.end method

.method public setAsumyi([B)V
    .locals 0
    .param p1, "field_39_asumyi"    # [B

    .prologue
    .line 1251
    iput-object p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_39_asumyi:[B

    .line 1252
    return-void
.end method

.method public setCCh(I)V
    .locals 0
    .param p1, "field_20_cCh"    # I

    .prologue
    .line 947
    iput p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_20_cCh:I

    .line 948
    return-void
.end method

.method public setCChFtnEdn(I)V
    .locals 0
    .param p1, "field_27_cChFtnEdn"    # I

    .prologue
    .line 1059
    iput p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_27_cChFtnEdn:I

    .line 1060
    return-void
.end method

.method public setCChWS(I)V
    .locals 0
    .param p1, "field_40_cChWS"    # I

    .prologue
    .line 1267
    iput p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_40_cChWS:I

    .line 1268
    return-void
.end method

.method public setCChWSFtnEdn(I)V
    .locals 0
    .param p1, "field_41_cChWSFtnEdn"    # I

    .prologue
    .line 1283
    iput p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_41_cChWSFtnEdn:I

    .line 1284
    return-void
.end method

.method public setCConsexHypLim(I)V
    .locals 0
    .param p1, "field_12_cConsexHypLim"    # I

    .prologue
    .line 819
    iput p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_12_cConsexHypLim:I

    .line 820
    return-void
.end method

.method public setCDBC(I)V
    .locals 0
    .param p1, "field_47_cDBC"    # I

    .prologue
    .line 1379
    iput p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_47_cDBC:I

    .line 1380
    return-void
.end method

.method public setCDBCFtnEdn(I)V
    .locals 0
    .param p1, "field_48_cDBCFtnEdn"    # I

    .prologue
    .line 1395
    iput p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_48_cDBCFtnEdn:I

    .line 1396
    return-void
.end method

.method public setCLines(I)V
    .locals 0
    .param p1, "field_25_cLines"    # I

    .prologue
    .line 1027
    iput p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_25_cLines:I

    .line 1028
    return-void
.end method

.method public setCLinesFtnEdn(I)V
    .locals 0
    .param p1, "field_30_cLinesFtnEdn"    # I

    .prologue
    .line 1107
    iput p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_30_cLinesFtnEdn:I

    .line 1108
    return-void
.end method

.method public setCParas(I)V
    .locals 0
    .param p1, "field_22_cParas"    # I

    .prologue
    .line 979
    iput p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_22_cParas:I

    .line 980
    return-void
.end method

.method public setCParasFtnEdn(I)V
    .locals 0
    .param p1, "field_29_cParasFtnEdn"    # I

    .prologue
    .line 1091
    iput p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_29_cParasFtnEdn:I

    .line 1092
    return-void
.end method

.method public setCPg(I)V
    .locals 0
    .param p1, "field_21_cPg"    # I

    .prologue
    .line 963
    iput p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_21_cPg:I

    .line 964
    return-void
.end method

.method public setCPgFtnEdn(S)V
    .locals 0
    .param p1, "field_28_cPgFtnEdn"    # S

    .prologue
    .line 1075
    iput-short p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_28_cPgFtnEdn:S

    .line 1076
    return-void
.end method

.method public setCWords(I)V
    .locals 0
    .param p1, "field_19_cWords"    # I

    .prologue
    .line 931
    iput p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_19_cWords:I

    .line 932
    return-void
.end method

.method public setCWordsFtnEnd(I)V
    .locals 0
    .param p1, "field_26_cWordsFtnEnd"    # I

    .prologue
    .line 1043
    iput p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_26_cWordsFtnEnd:I

    .line 1044
    return-void
.end method

.method public setDocinfo(B)V
    .locals 0
    .param p1, "field_5_docinfo"    # B

    .prologue
    .line 707
    iput-byte p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_5_docinfo:B

    .line 708
    return-void
.end method

.method public setDocinfo1(B)V
    .locals 0
    .param p1, "field_6_docinfo1"    # B

    .prologue
    .line 723
    iput-byte p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_6_docinfo1:B

    .line 724
    return-void
.end method

.method public setDocinfo2(B)V
    .locals 0
    .param p1, "field_7_docinfo2"    # B

    .prologue
    .line 739
    iput-byte p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_7_docinfo2:B

    .line 740
    return-void
.end method

.method public setDocinfo3(S)V
    .locals 0
    .param p1, "field_8_docinfo3"    # S

    .prologue
    .line 755
    iput-short p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_8_docinfo3:S

    .line 756
    return-void
.end method

.method public setDocinfo4(I)V
    .locals 0
    .param p1, "field_33_docinfo4"    # I

    .prologue
    .line 1155
    iput p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_33_docinfo4:I

    .line 1156
    return-void
.end method

.method public setDocinfo5(S)V
    .locals 0
    .param p1, "field_37_docinfo5"    # S

    .prologue
    .line 1219
    iput-short p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_37_docinfo5:S

    .line 1220
    return-void
.end method

.method public setDocinfo6(S)V
    .locals 0
    .param p1, "field_38_docinfo6"    # S

    .prologue
    .line 1235
    iput-short p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_38_docinfo6:S

    .line 1236
    return-void
.end method

.method public setDogrid([B)V
    .locals 0
    .param p1, "field_36_dogrid"    # [B

    .prologue
    .line 1203
    iput-object p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_36_dogrid:[B

    .line 1204
    return-void
.end method

.method public setDoptypography([B)V
    .locals 0
    .param p1, "field_35_doptypography"    # [B

    .prologue
    .line 1187
    iput-object p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_35_doptypography:[B

    .line 1188
    return-void
.end method

.method public setDttmCreated(I)V
    .locals 0
    .param p1, "field_14_dttmCreated"    # I

    .prologue
    .line 851
    iput p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_14_dttmCreated:I

    .line 852
    return-void
.end method

.method public setDttmLastPrint(I)V
    .locals 0
    .param p1, "field_16_dttmLastPrint"    # I

    .prologue
    .line 883
    iput p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_16_dttmLastPrint:I

    .line 884
    return-void
.end method

.method public setDttmRevised(I)V
    .locals 0
    .param p1, "field_15_dttmRevised"    # I

    .prologue
    .line 867
    iput p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_15_dttmRevised:I

    .line 868
    return-void
.end method

.method public setDxaHotz(I)V
    .locals 0
    .param p1, "field_11_dxaHotz"    # I

    .prologue
    .line 803
    iput p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_11_dxaHotz:I

    .line 804
    return-void
.end method

.method public setDxaTab(I)V
    .locals 0
    .param p1, "field_9_dxaTab"    # I

    .prologue
    .line 771
    iput p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_9_dxaTab:I

    .line 772
    return-void
.end method

.method public setDywDispPag(S)V
    .locals 0
    .param p1, "field_53_dywDispPag"    # S

    .prologue
    .line 1475
    iput-short p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_53_dywDispPag:S

    .line 1476
    return-void
.end method

.method public setEdn(S)V
    .locals 0
    .param p1, "field_23_Edn"    # S

    .prologue
    .line 995
    iput-short p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_23_Edn:S

    .line 996
    return-void
.end method

.method public setEdn1(S)V
    .locals 0
    .param p1, "field_24_Edn1"    # S

    .prologue
    .line 1011
    iput-short p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_24_Edn1:S

    .line 1012
    return-void
.end method

.method public setEpc(B)V
    .locals 2
    .param p1, "value"    # B

    .prologue
    .line 2330
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->epc:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_24_Edn1:S

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setValue(II)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_24_Edn1:S

    .line 2331
    return-void
.end method

.method public setFAutoHyphen(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 1700
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fAutoHyphen:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_5_docinfo:B

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-byte v0, v0

    iput-byte v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_5_docinfo:B

    .line 1701
    return-void
.end method

.method public setFAutoVersions(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 3068
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fAutoVersions:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_38_docinfo6:S

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_38_docinfo6:S

    .line 3069
    return-void
.end method

.method public setFBackup(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 1772
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fBackup:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_6_docinfo1:B

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-byte v0, v0

    iput-byte v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_6_docinfo1:B

    .line 1773
    return-void
.end method

.method public setFConvMailMergeEsc(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 2654
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fConvMailMergeEsc:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_33_docinfo4:I

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_33_docinfo4:I

    .line 2655
    return-void
.end method

.method public setFDfltTrueType(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 1898
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fDfltTrueType:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_6_docinfo1:B

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-byte v0, v0

    iput-byte v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_6_docinfo1:B

    .line 1899
    return-void
.end method

.method public setFDispFormFldSel(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 1952
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fDispFormFldSel:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_7_docinfo2:B

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-byte v0, v0

    iput-byte v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_7_docinfo2:B

    .line 1953
    return-void
.end method

.method public setFEmbedFonts(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 2042
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fEmbedFonts:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_7_docinfo2:B

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-byte v0, v0

    iput-byte v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_7_docinfo2:B

    .line 2043
    return-void
.end method

.method public setFExactCWords(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 1790
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fExactCWords:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_6_docinfo1:B

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-byte v0, v0

    iput-byte v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_6_docinfo1:B

    .line 1791
    return-void
.end method

.method public setFFacingPages(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 1484
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fFacingPages:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_1_formatFlags:B

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-byte v0, v0

    iput-byte v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_1_formatFlags:B

    .line 1485
    return-void
.end method

.method public setFForcePageSizePag(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 3014
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fForcePageSizePag:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_37_docinfo5:S

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_37_docinfo5:S

    .line 3015
    return-void
.end method

.method public setFFormNoFields(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 1718
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fFormNoFields:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_5_docinfo:B

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-byte v0, v0

    iput-byte v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_5_docinfo:B

    .line 1719
    return-void
.end method

.method public setFGramAllClean(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 2888
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fGramAllClean:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_37_docinfo5:S

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_37_docinfo5:S

    .line 2889
    return-void
.end method

.method public setFGramAllDone(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 2870
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fGramAllDone:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_37_docinfo5:S

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_37_docinfo5:S

    .line 2871
    return-void
.end method

.method public setFHaveVersions(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 3050
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fHaveVersions:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_38_docinfo6:S

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_38_docinfo6:S

    .line 3051
    return-void
.end method

.method public setFHideLastVersion(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 2924
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fHideLastVersion:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_37_docinfo5:S

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_37_docinfo5:S

    .line 2925
    return-void
.end method

.method public setFHtmlDoc(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 2942
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fHtmlDoc:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_37_docinfo5:S

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_37_docinfo5:S

    .line 2943
    return-void
.end method

.method public setFHyphCapitals(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 1682
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fHyphCapitals:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_5_docinfo:B

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-byte v0, v0

    iput-byte v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_5_docinfo:B

    .line 1683
    return-void
.end method

.method public setFIncludeFooter(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 2996
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fIncludeFooter:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_37_docinfo5:S

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_37_docinfo5:S

    .line 2997
    return-void
.end method

.method public setFIncludeHeader(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 2978
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fIncludeHeader:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_37_docinfo5:S

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_37_docinfo5:S

    .line 2979
    return-void
.end method

.method public setFLabelDoc(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 1664
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fLabelDoc:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_5_docinfo:B

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-byte v0, v0

    iput-byte v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_5_docinfo:B

    .line 1665
    return-void
.end method

.method public setFLinkStyles(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 1736
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fLinkStyles:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_5_docinfo:B

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-byte v0, v0

    iput-byte v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_5_docinfo:B

    .line 1737
    return-void
.end method

.method public setFLockAtn(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 1844
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fLockAtn:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_6_docinfo1:B

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-byte v0, v0

    iput-byte v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_6_docinfo1:B

    .line 1845
    return-void
.end method

.method public setFLockRev(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 2024
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fLockRev:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_7_docinfo2:B

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-byte v0, v0

    iput-byte v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_7_docinfo2:B

    .line 2025
    return-void
.end method

.method public setFMWSmallCaps(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 2834
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fMWSmallCaps:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_33_docinfo4:I

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_33_docinfo4:I

    .line 2835
    return-void
.end method

.method public setFMapPrintTextColor(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 2618
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fMapPrintTextColor:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_33_docinfo4:I

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_33_docinfo4:I

    .line 2619
    return-void
.end method

.method public setFMinFontSizePag(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 3032
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fMinFontSizePag:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_37_docinfo5:S

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_37_docinfo5:S

    .line 3033
    return-void
.end method

.method public setFMirrorMargins(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 1862
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fMirrorMargins:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_6_docinfo1:B

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-byte v0, v0

    iput-byte v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_6_docinfo1:B

    .line 1863
    return-void
.end method

.method public setFNoColumnBalance(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 2636
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fNoColumnBalance:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_33_docinfo4:I

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_33_docinfo4:I

    .line 2637
    return-void
.end method

.method public setFNoLeading(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 2816
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fNoLeading:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_33_docinfo4:I

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_33_docinfo4:I

    .line 2817
    return-void
.end method

.method public setFNoSpaceRaiseLower(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 2564
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fNoSpaceRaiseLower:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_33_docinfo4:I

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_33_docinfo4:I

    .line 2565
    return-void
.end method

.method public setFNoTabForInd(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 2546
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fNoTabForInd:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_33_docinfo4:I

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_33_docinfo4:I

    .line 2547
    return-void
.end method

.method public setFOnlyMacPics(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 1628
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fOnlyMacPics:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_5_docinfo:B

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-byte v0, v0

    iput-byte v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_5_docinfo:B

    .line 1629
    return-void
.end method

.method public setFOnlyWinPics(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 1646
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fOnlyWinPics:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_5_docinfo:B

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-byte v0, v0

    iput-byte v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_5_docinfo:B

    .line 1647
    return-void
.end method

.method public setFOrigWordTableRules(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 2690
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fOrigWordTableRules:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_33_docinfo4:I

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_33_docinfo4:I

    .line 2691
    return-void
.end method

.method public setFOutlineDirtySave(B)V
    .locals 0
    .param p1, "field_4_fOutlineDirtySave"    # B

    .prologue
    .line 691
    iput-byte p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_4_fOutlineDirtySave:B

    .line 692
    return-void
.end method

.method public setFPMHMainDoc(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 1520
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fPMHMainDoc:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_1_formatFlags:B

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-byte v0, v0

    iput-byte v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_1_formatFlags:B

    .line 1521
    return-void
.end method

.method public setFPagHidden(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 1808
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fPagHidden:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_6_docinfo1:B

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-byte v0, v0

    iput-byte v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_6_docinfo1:B

    .line 1809
    return-void
.end method

.method public setFPagResults(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 1826
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fPagResults:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_6_docinfo1:B

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-byte v0, v0

    iput-byte v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_6_docinfo1:B

    .line 1827
    return-void
.end method

.method public setFPagSupressTopSpacing(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 1916
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fPagSupressTopSpacing:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_7_docinfo2:B

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-byte v0, v0

    iput-byte v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_7_docinfo2:B

    .line 1917
    return-void
.end method

.method public setFPrintBodyBeforeHdr(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 2798
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fPrintBodyBeforeHdr:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_33_docinfo4:I

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_33_docinfo4:I

    .line 2799
    return-void
.end method

.method public setFPrintFormData(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 2384
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fPrintFormData:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_24_Edn1:S

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_24_Edn1:S

    .line 2385
    return-void
.end method

.method public setFProtEnabled(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 1934
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fProtEnabled:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_7_docinfo2:B

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-byte v0, v0

    iput-byte v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_7_docinfo2:B

    .line 1935
    return-void
.end method

.method public setFRMPrint(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 1988
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fRMPrint:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_7_docinfo2:B

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-byte v0, v0

    iput-byte v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_7_docinfo2:B

    .line 1989
    return-void
.end method

.method public setFRMView(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 1970
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fRMView:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_7_docinfo2:B

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-byte v0, v0

    iput-byte v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_7_docinfo2:B

    .line 1971
    return-void
.end method

.method public setFRevMarking(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 1754
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fRevMarking:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_5_docinfo:B

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-byte v0, v0

    iput-byte v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_5_docinfo:B

    .line 1755
    return-void
.end method

.method public setFRotateFontW6(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 2510
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fRotateFontW6:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_32_view:S

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_32_view:S

    .line 2511
    return-void
.end method

.method public setFSaveFormData(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 2402
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fSaveFormData:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_24_Edn1:S

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_24_Edn1:S

    .line 2403
    return-void
.end method

.method public setFShadeFormData(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 2420
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fShadeFormData:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_24_Edn1:S

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_24_Edn1:S

    .line 2421
    return-void
.end method

.method public setFShowBreaksInFrames(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 2726
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fShowBreaksInFrames:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_33_docinfo4:I

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_33_docinfo4:I

    .line 2727
    return-void
.end method

.method public setFSnapBorder(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 2960
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fSnapBorder:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_37_docinfo5:S

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_37_docinfo5:S

    .line 2961
    return-void
.end method

.method public setFSubsetFonts(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 2906
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fSubsetFonts:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_37_docinfo5:S

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_37_docinfo5:S

    .line 2907
    return-void
.end method

.method public setFSuppressTopSPacingMac5(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 2762
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fSuppressTopSPacingMac5:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_33_docinfo4:I

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_33_docinfo4:I

    .line 2763
    return-void
.end method

.method public setFSupressSpdfAfterPageBreak(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 2582
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fSupressSpdfAfterPageBreak:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_33_docinfo4:I

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_33_docinfo4:I

    .line 2583
    return-void
.end method

.method public setFSupressTopSpacing(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 2672
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fSupressTopSpacing:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_33_docinfo4:I

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_33_docinfo4:I

    .line 2673
    return-void
.end method

.method public setFSwapBordersFacingPgs(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 2744
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fSwapBordersFacingPgs:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_33_docinfo4:I

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_33_docinfo4:I

    .line 2745
    return-void
.end method

.method public setFTransparentMetafiles(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 2708
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fTransparentMetafiles:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_33_docinfo4:I

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_33_docinfo4:I

    .line 2709
    return-void
.end method

.method public setFTruncDxaExpand(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 2780
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fTruncDxaExpand:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_33_docinfo4:I

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_33_docinfo4:I

    .line 2781
    return-void
.end method

.method public setFVirusLoadSafe(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 3104
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fVirusLoadSafe:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_43_virusinfo:I

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_43_virusinfo:I

    .line 3105
    return-void
.end method

.method public setFVirusPrompted(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 3086
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fVirusPrompted:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_43_virusinfo:I

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_43_virusinfo:I

    .line 3087
    return-void
.end method

.method public setFWCFtnEdn(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 2438
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fWCFtnEdn:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_24_Edn1:S

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_24_Edn1:S

    .line 2439
    return-void
.end method

.method public setFWidowControl(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 1502
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fWidowControl:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_1_formatFlags:B

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-byte v0, v0

    iput-byte v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_1_formatFlags:B

    .line 1503
    return-void
.end method

.method public setFWrapTrailSpaces(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 2600
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fWrapTrailSpaces:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_33_docinfo4:I

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_33_docinfo4:I

    .line 2601
    return-void
.end method

.method public setFootnoteInfo(S)V
    .locals 0
    .param p1, "field_3_footnoteInfo"    # S

    .prologue
    .line 675
    iput-short p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_3_footnoteInfo:S

    .line 676
    return-void
.end method

.method public setFormatFlags(B)V
    .locals 0
    .param p1, "field_1_formatFlags"    # B

    .prologue
    .line 643
    iput-byte p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_1_formatFlags:B

    .line 644
    return-void
.end method

.method public setFpc(B)V
    .locals 2
    .param p1, "value"    # B

    .prologue
    .line 1556
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->fpc:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_1_formatFlags:B

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setValue(II)I

    move-result v0

    int-to-byte v0, v0

    iput-byte v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_1_formatFlags:B

    .line 1557
    return-void
.end method

.method public setGrfDocEvents(I)V
    .locals 0
    .param p1, "field_42_grfDocEvents"    # I

    .prologue
    .line 1299
    iput p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_42_grfDocEvents:I

    .line 1300
    return-void
.end method

.method public setGrfSupression(B)V
    .locals 2
    .param p1, "value"    # B

    .prologue
    .line 1538
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->grfSupression:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_1_formatFlags:B

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setValue(II)I

    move-result v0

    int-to-byte v0, v0

    iput-byte v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_1_formatFlags:B

    .line 1539
    return-void
.end method

.method public setHpsZoonFontPag(S)V
    .locals 0
    .param p1, "field_52_hpsZoonFontPag"    # S

    .prologue
    .line 1459
    iput-short p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_52_hpsZoonFontPag:S

    .line 1460
    return-void
.end method

.method public setIGutterPos(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 2528
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->iGutterPos:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_32_view:S

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_32_view:S

    .line 2529
    return-void
.end method

.method public setKeyVirusSession30(I)V
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 3122
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->KeyVirusSession30:Lorg/apache/poi/util/BitField;

    iget v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_43_virusinfo:I

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setValue(II)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_43_virusinfo:I

    .line 3123
    return-void
.end method

.method public setLKeyProtDoc(I)V
    .locals 0
    .param p1, "field_31_lKeyProtDoc"    # I

    .prologue
    .line 1123
    iput p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_31_lKeyProtDoc:I

    .line 1124
    return-void
.end method

.method public setLvl(B)V
    .locals 2
    .param p1, "value"    # B

    .prologue
    .line 2852
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->lvl:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_37_docinfo5:S

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setValue(II)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_37_docinfo5:S

    .line 2853
    return-void
.end method

.method public setNEdn(S)V
    .locals 2
    .param p1, "value"    # S

    .prologue
    .line 2312
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->nEdn:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_23_Edn:S

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setValue(II)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_23_Edn:S

    .line 2313
    return-void
.end method

.method public setNFtn(S)V
    .locals 2
    .param p1, "value"    # S

    .prologue
    .line 1610
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->nFtn:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_3_footnoteInfo:S

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setValue(II)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_3_footnoteInfo:S

    .line 1611
    return-void
.end method

.method public setNRevision(I)V
    .locals 0
    .param p1, "field_17_nRevision"    # I

    .prologue
    .line 899
    iput p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_17_nRevision:I

    .line 900
    return-void
.end method

.method public setNfcEdnRef(S)V
    .locals 0
    .param p1, "field_51_nfcEdnRef"    # S

    .prologue
    .line 1443
    iput-short p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_51_nfcEdnRef:S

    .line 1444
    return-void
.end method

.method public setNfcEdnRef1(B)V
    .locals 2
    .param p1, "value"    # B

    .prologue
    .line 2366
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->nfcEdnRef1:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_24_Edn1:S

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setValue(II)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_24_Edn1:S

    .line 2367
    return-void
.end method

.method public setNfcFtnRef(S)V
    .locals 0
    .param p1, "field_50_nfcFtnRef"    # S

    .prologue
    .line 1427
    iput-short p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_50_nfcFtnRef:S

    .line 1428
    return-void
.end method

.method public setNfcFtnRef1(B)V
    .locals 2
    .param p1, "value"    # B

    .prologue
    .line 2348
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->nfcFtnRef1:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_24_Edn1:S

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setValue(II)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_24_Edn1:S

    .line 2349
    return-void
.end method

.method public setOldfConvMailMergeEsc(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 2168
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->oldfConvMailMergeEsc:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_8_docinfo3:S

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_8_docinfo3:S

    .line 2169
    return-void
.end method

.method public setOldfMapPrintTextColor(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 2132
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->oldfMapPrintTextColor:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_8_docinfo3:S

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_8_docinfo3:S

    .line 2133
    return-void
.end method

.method public setOldfNoColumnBalance(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 2150
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->oldfNoColumnBalance:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_8_docinfo3:S

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_8_docinfo3:S

    .line 2151
    return-void
.end method

.method public setOldfNoSpaceRaiseLower(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 2078
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->oldfNoSpaceRaiseLower:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_8_docinfo3:S

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_8_docinfo3:S

    .line 2079
    return-void
.end method

.method public setOldfNoTabForInd(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 2060
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->oldfNoTabForInd:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_8_docinfo3:S

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_8_docinfo3:S

    .line 2061
    return-void
.end method

.method public setOldfOrigWordTableRules(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 2204
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->oldfOrigWordTableRules:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_8_docinfo3:S

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_8_docinfo3:S

    .line 2205
    return-void
.end method

.method public setOldfShowBreaksInFrames(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 2240
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->oldfShowBreaksInFrames:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_8_docinfo3:S

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_8_docinfo3:S

    .line 2241
    return-void
.end method

.method public setOldfSuppressSpbfAfterPageBreak(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 2096
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->oldfSuppressSpbfAfterPageBreak:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_8_docinfo3:S

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_8_docinfo3:S

    .line 2097
    return-void
.end method

.method public setOldfSupressTopSpacing(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 2186
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->oldfSupressTopSpacing:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_8_docinfo3:S

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_8_docinfo3:S

    .line 2187
    return-void
.end method

.method public setOldfSwapBordersFacingPgs(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 2258
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->oldfSwapBordersFacingPgs:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_8_docinfo3:S

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_8_docinfo3:S

    .line 2259
    return-void
.end method

.method public setOldfTransparentMetafiles(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 2222
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->oldfTransparentMetafiles:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_8_docinfo3:S

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_8_docinfo3:S

    .line 2223
    return-void
.end method

.method public setOldfWrapTrailSpaces(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 2114
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->oldfWrapTrailSpaces:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_8_docinfo3:S

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_8_docinfo3:S

    .line 2115
    return-void
.end method

.method public setReserved(I)V
    .locals 0
    .param p1, "field_49_reserved"    # I

    .prologue
    .line 1411
    iput p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_49_reserved:I

    .line 1412
    return-void
.end method

.method public setReserved1(I)V
    .locals 0
    .param p1, "field_45_reserved1"    # I

    .prologue
    .line 1347
    iput p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_45_reserved1:I

    .line 1348
    return-void
.end method

.method public setReserved2(I)V
    .locals 0
    .param p1, "field_46_reserved2"    # I

    .prologue
    .line 1363
    iput p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_46_reserved2:I

    .line 1364
    return-void
.end method

.method public setRncEdn(B)V
    .locals 2
    .param p1, "value"    # B

    .prologue
    .line 2294
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->rncEdn:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_23_Edn:S

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setValue(II)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_23_Edn:S

    .line 2295
    return-void
.end method

.method public setRncFtn(B)V
    .locals 2
    .param p1, "value"    # B

    .prologue
    .line 1592
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->rncFtn:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_3_footnoteInfo:S

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setValue(II)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_3_footnoteInfo:S

    .line 1593
    return-void
.end method

.method public setSpare([B)V
    .locals 0
    .param p1, "field_44_Spare"    # [B

    .prologue
    .line 1331
    iput-object p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_44_Spare:[B

    .line 1332
    return-void
.end method

.method public setTmEdited(I)V
    .locals 0
    .param p1, "field_18_tmEdited"    # I

    .prologue
    .line 915
    iput p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_18_tmEdited:I

    .line 916
    return-void
.end method

.method public setUnused1(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 1574
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->unused1:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_1_formatFlags:B

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-byte v0, v0

    iput-byte v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_1_formatFlags:B

    .line 1575
    return-void
.end method

.method public setUnused2(B)V
    .locals 0
    .param p1, "field_2_unused2"    # B

    .prologue
    .line 659
    iput-byte p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_2_unused2:B

    .line 660
    return-void
.end method

.method public setUnused3(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 1880
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->unused3:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_6_docinfo1:B

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-byte v0, v0

    iput-byte v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_6_docinfo1:B

    .line 1881
    return-void
.end method

.method public setUnused4(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 2006
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->unused4:Lorg/apache/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_7_docinfo2:B

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-byte v0, v0

    iput-byte v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_7_docinfo2:B

    .line 2007
    return-void
.end method

.method public setUnused5(B)V
    .locals 2
    .param p1, "value"    # B

    .prologue
    .line 2276
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->unused5:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_8_docinfo3:S

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setValue(II)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_8_docinfo3:S

    .line 2277
    return-void
.end method

.method public setView(S)V
    .locals 0
    .param p1, "field_32_view"    # S

    .prologue
    .line 1139
    iput-short p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_32_view:S

    .line 1140
    return-void
.end method

.method public setVirusinfo(I)V
    .locals 0
    .param p1, "field_43_virusinfo"    # I

    .prologue
    .line 1315
    iput p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_43_virusinfo:I

    .line 1316
    return-void
.end method

.method public setWScaleSaved(S)V
    .locals 2
    .param p1, "value"    # S

    .prologue
    .line 2474
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->wScaleSaved:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_32_view:S

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setValue(II)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_32_view:S

    .line 2475
    return-void
.end method

.method public setWSpare(I)V
    .locals 0
    .param p1, "field_10_wSpare"    # I

    .prologue
    .line 787
    iput p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_10_wSpare:I

    .line 788
    return-void
.end method

.method public setWSpare2(I)V
    .locals 0
    .param p1, "field_13_wSpare2"    # I

    .prologue
    .line 835
    iput p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_13_wSpare2:I

    .line 836
    return-void
.end method

.method public setWvkSaved(B)V
    .locals 2
    .param p1, "value"    # B

    .prologue
    .line 2456
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->wvkSaved:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_32_view:S

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setValue(II)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_32_view:S

    .line 2457
    return-void
.end method

.method public setZkSaved(B)V
    .locals 2
    .param p1, "value"    # B

    .prologue
    .line 2492
    sget-object v0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->zkSaved:Lorg/apache/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_32_view:S

    invoke-virtual {v0, v1, p1}, Lorg/apache/poi/util/BitField;->setValue(II)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->field_32_view:S

    .line 2493
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    const/16 v3, 0xa

    .line 308
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 310
    .local v0, "buffer":Ljava/lang/StringBuffer;
    const-string/jumbo v1, "[DOP]\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 312
    const-string/jumbo v1, "    .formatFlags          = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 313
    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->getFormatFlags()B

    move-result v1

    invoke-static {v1}, Lorg/apache/poi/util/HexDump;->byteToHex(I)[C

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append([C)Ljava/lang/StringBuffer;

    .line 314
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->getFormatFlags()B

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 315
    const-string/jumbo v1, "         .fFacingPages             = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->isFFacingPages()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 316
    const-string/jumbo v1, "         .fWidowControl            = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->isFWidowControl()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 317
    const-string/jumbo v1, "         .fPMHMainDoc              = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->isFPMHMainDoc()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 318
    const-string/jumbo v1, "         .grfSupression            = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->getGrfSupression()B

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 319
    const-string/jumbo v1, "         .fpc                      = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->getFpc()B

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 320
    const-string/jumbo v1, "         .unused1                  = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->isUnused1()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 322
    const-string/jumbo v1, "    .unused2              = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 323
    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->getUnused2()B

    move-result v1

    invoke-static {v1}, Lorg/apache/poi/util/HexDump;->byteToHex(I)[C

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append([C)Ljava/lang/StringBuffer;

    .line 324
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->getUnused2()B

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 326
    const-string/jumbo v1, "    .footnoteInfo         = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 327
    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->getFootnoteInfo()S

    move-result v1

    invoke-static {v1}, Lorg/apache/poi/util/HexDump;->shortToHex(I)[C

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append([C)Ljava/lang/StringBuffer;

    .line 328
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->getFootnoteInfo()S

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 329
    const-string/jumbo v1, "         .rncFtn                   = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->getRncFtn()B

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 330
    const-string/jumbo v1, "         .nFtn                     = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->getNFtn()S

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 332
    const-string/jumbo v1, "    .fOutlineDirtySave    = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 333
    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->getFOutlineDirtySave()B

    move-result v1

    invoke-static {v1}, Lorg/apache/poi/util/HexDump;->byteToHex(I)[C

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append([C)Ljava/lang/StringBuffer;

    .line 334
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->getFOutlineDirtySave()B

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 336
    const-string/jumbo v1, "    .docinfo              = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 337
    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->getDocinfo()B

    move-result v1

    invoke-static {v1}, Lorg/apache/poi/util/HexDump;->byteToHex(I)[C

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append([C)Ljava/lang/StringBuffer;

    .line 338
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->getDocinfo()B

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 339
    const-string/jumbo v1, "         .fOnlyMacPics             = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->isFOnlyMacPics()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 340
    const-string/jumbo v1, "         .fOnlyWinPics             = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->isFOnlyWinPics()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 341
    const-string/jumbo v1, "         .fLabelDoc                = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->isFLabelDoc()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 342
    const-string/jumbo v1, "         .fHyphCapitals            = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->isFHyphCapitals()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 343
    const-string/jumbo v1, "         .fAutoHyphen              = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->isFAutoHyphen()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 344
    const-string/jumbo v1, "         .fFormNoFields            = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->isFFormNoFields()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 345
    const-string/jumbo v1, "         .fLinkStyles              = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->isFLinkStyles()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 346
    const-string/jumbo v1, "         .fRevMarking              = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->isFRevMarking()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 348
    const-string/jumbo v1, "    .docinfo1             = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 349
    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->getDocinfo1()B

    move-result v1

    invoke-static {v1}, Lorg/apache/poi/util/HexDump;->byteToHex(I)[C

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append([C)Ljava/lang/StringBuffer;

    .line 350
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->getDocinfo1()B

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 351
    const-string/jumbo v1, "         .fBackup                  = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->isFBackup()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 352
    const-string/jumbo v1, "         .fExactCWords             = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->isFExactCWords()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 353
    const-string/jumbo v1, "         .fPagHidden               = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->isFPagHidden()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 354
    const-string/jumbo v1, "         .fPagResults              = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->isFPagResults()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 355
    const-string/jumbo v1, "         .fLockAtn                 = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->isFLockAtn()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 356
    const-string/jumbo v1, "         .fMirrorMargins           = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->isFMirrorMargins()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 357
    const-string/jumbo v1, "         .unused3                  = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->isUnused3()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 358
    const-string/jumbo v1, "         .fDfltTrueType            = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->isFDfltTrueType()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 360
    const-string/jumbo v1, "    .docinfo2             = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 361
    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->getDocinfo2()B

    move-result v1

    invoke-static {v1}, Lorg/apache/poi/util/HexDump;->byteToHex(I)[C

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append([C)Ljava/lang/StringBuffer;

    .line 362
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->getDocinfo2()B

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 363
    const-string/jumbo v1, "         .fPagSupressTopSpacing     = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->isFPagSupressTopSpacing()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 364
    const-string/jumbo v1, "         .fProtEnabled             = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->isFProtEnabled()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 365
    const-string/jumbo v1, "         .fDispFormFldSel          = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->isFDispFormFldSel()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 366
    const-string/jumbo v1, "         .fRMView                  = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->isFRMView()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 367
    const-string/jumbo v1, "         .fRMPrint                 = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->isFRMPrint()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 368
    const-string/jumbo v1, "         .unused4                  = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->isUnused4()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 369
    const-string/jumbo v1, "         .fLockRev                 = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->isFLockRev()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 370
    const-string/jumbo v1, "         .fEmbedFonts              = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->isFEmbedFonts()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 372
    const-string/jumbo v1, "    .docinfo3             = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 373
    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->getDocinfo3()S

    move-result v1

    invoke-static {v1}, Lorg/apache/poi/util/HexDump;->shortToHex(I)[C

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append([C)Ljava/lang/StringBuffer;

    .line 374
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->getDocinfo3()S

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 375
    const-string/jumbo v1, "         .oldfNoTabForInd          = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->isOldfNoTabForInd()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 376
    const-string/jumbo v1, "         .oldfNoSpaceRaiseLower     = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->isOldfNoSpaceRaiseLower()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 377
    const-string/jumbo v1, "         .oldfSuppressSpbfAfterPageBreak     = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->isOldfSuppressSpbfAfterPageBreak()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 378
    const-string/jumbo v1, "         .oldfWrapTrailSpaces      = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->isOldfWrapTrailSpaces()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 379
    const-string/jumbo v1, "         .oldfMapPrintTextColor     = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->isOldfMapPrintTextColor()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 380
    const-string/jumbo v1, "         .oldfNoColumnBalance      = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->isOldfNoColumnBalance()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 381
    const-string/jumbo v1, "         .oldfConvMailMergeEsc     = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->isOldfConvMailMergeEsc()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 382
    const-string/jumbo v1, "         .oldfSupressTopSpacing     = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->isOldfSupressTopSpacing()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 383
    const-string/jumbo v1, "         .oldfOrigWordTableRules     = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->isOldfOrigWordTableRules()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 384
    const-string/jumbo v1, "         .oldfTransparentMetafiles     = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->isOldfTransparentMetafiles()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 385
    const-string/jumbo v1, "         .oldfShowBreaksInFrames     = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->isOldfShowBreaksInFrames()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 386
    const-string/jumbo v1, "         .oldfSwapBordersFacingPgs     = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->isOldfSwapBordersFacingPgs()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 387
    const-string/jumbo v1, "         .unused5                  = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->getUnused5()B

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 389
    const-string/jumbo v1, "    .dxaTab               = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 390
    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->getDxaTab()I

    move-result v1

    invoke-static {v1}, Lorg/apache/poi/util/HexDump;->intToHex(I)[C

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append([C)Ljava/lang/StringBuffer;

    .line 391
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->getDxaTab()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 393
    const-string/jumbo v1, "    .wSpare               = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 394
    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->getWSpare()I

    move-result v1

    invoke-static {v1}, Lorg/apache/poi/util/HexDump;->intToHex(I)[C

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append([C)Ljava/lang/StringBuffer;

    .line 395
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->getWSpare()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 397
    const-string/jumbo v1, "    .dxaHotz              = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 398
    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->getDxaHotz()I

    move-result v1

    invoke-static {v1}, Lorg/apache/poi/util/HexDump;->intToHex(I)[C

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append([C)Ljava/lang/StringBuffer;

    .line 399
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->getDxaHotz()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 401
    const-string/jumbo v1, "    .cConsexHypLim        = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 402
    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->getCConsexHypLim()I

    move-result v1

    invoke-static {v1}, Lorg/apache/poi/util/HexDump;->intToHex(I)[C

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append([C)Ljava/lang/StringBuffer;

    .line 403
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->getCConsexHypLim()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 405
    const-string/jumbo v1, "    .wSpare2              = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 406
    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->getWSpare2()I

    move-result v1

    invoke-static {v1}, Lorg/apache/poi/util/HexDump;->intToHex(I)[C

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append([C)Ljava/lang/StringBuffer;

    .line 407
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->getWSpare2()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 409
    const-string/jumbo v1, "    .dttmCreated          = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 410
    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->getDttmCreated()I

    move-result v1

    invoke-static {v1}, Lorg/apache/poi/util/HexDump;->intToHex(I)[C

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append([C)Ljava/lang/StringBuffer;

    .line 411
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->getDttmCreated()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 413
    const-string/jumbo v1, "    .dttmRevised          = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 414
    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->getDttmRevised()I

    move-result v1

    invoke-static {v1}, Lorg/apache/poi/util/HexDump;->intToHex(I)[C

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append([C)Ljava/lang/StringBuffer;

    .line 415
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->getDttmRevised()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 417
    const-string/jumbo v1, "    .dttmLastPrint        = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 418
    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->getDttmLastPrint()I

    move-result v1

    invoke-static {v1}, Lorg/apache/poi/util/HexDump;->intToHex(I)[C

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append([C)Ljava/lang/StringBuffer;

    .line 419
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->getDttmLastPrint()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 421
    const-string/jumbo v1, "    .nRevision            = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 422
    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->getNRevision()I

    move-result v1

    invoke-static {v1}, Lorg/apache/poi/util/HexDump;->intToHex(I)[C

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append([C)Ljava/lang/StringBuffer;

    .line 423
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->getNRevision()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 425
    const-string/jumbo v1, "    .tmEdited             = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 426
    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->getTmEdited()I

    move-result v1

    invoke-static {v1}, Lorg/apache/poi/util/HexDump;->intToHex(I)[C

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append([C)Ljava/lang/StringBuffer;

    .line 427
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->getTmEdited()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 429
    const-string/jumbo v1, "    .cWords               = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 430
    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->getCWords()I

    move-result v1

    invoke-static {v1}, Lorg/apache/poi/util/HexDump;->intToHex(I)[C

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append([C)Ljava/lang/StringBuffer;

    .line 431
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->getCWords()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 433
    const-string/jumbo v1, "    .cCh                  = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 434
    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->getCCh()I

    move-result v1

    invoke-static {v1}, Lorg/apache/poi/util/HexDump;->intToHex(I)[C

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append([C)Ljava/lang/StringBuffer;

    .line 435
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->getCCh()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 437
    const-string/jumbo v1, "    .cPg                  = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 438
    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->getCPg()I

    move-result v1

    invoke-static {v1}, Lorg/apache/poi/util/HexDump;->intToHex(I)[C

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append([C)Ljava/lang/StringBuffer;

    .line 439
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->getCPg()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 441
    const-string/jumbo v1, "    .cParas               = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 442
    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->getCParas()I

    move-result v1

    invoke-static {v1}, Lorg/apache/poi/util/HexDump;->intToHex(I)[C

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append([C)Ljava/lang/StringBuffer;

    .line 443
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->getCParas()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 445
    const-string/jumbo v1, "    .Edn                  = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 446
    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->getEdn()S

    move-result v1

    invoke-static {v1}, Lorg/apache/poi/util/HexDump;->shortToHex(I)[C

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append([C)Ljava/lang/StringBuffer;

    .line 447
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->getEdn()S

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 448
    const-string/jumbo v1, "         .rncEdn                   = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->getRncEdn()B

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 449
    const-string/jumbo v1, "         .nEdn                     = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->getNEdn()S

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 451
    const-string/jumbo v1, "    .Edn1                 = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 452
    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->getEdn1()S

    move-result v1

    invoke-static {v1}, Lorg/apache/poi/util/HexDump;->shortToHex(I)[C

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append([C)Ljava/lang/StringBuffer;

    .line 453
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->getEdn1()S

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 454
    const-string/jumbo v1, "         .epc                      = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->getEpc()B

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 455
    const-string/jumbo v1, "         .nfcFtnRef1               = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->getNfcFtnRef1()B

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 456
    const-string/jumbo v1, "         .nfcEdnRef1               = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->getNfcEdnRef1()B

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 457
    const-string/jumbo v1, "         .fPrintFormData           = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->isFPrintFormData()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 458
    const-string/jumbo v1, "         .fSaveFormData            = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->isFSaveFormData()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 459
    const-string/jumbo v1, "         .fShadeFormData           = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->isFShadeFormData()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 460
    const-string/jumbo v1, "         .fWCFtnEdn                = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->isFWCFtnEdn()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 462
    const-string/jumbo v1, "    .cLines               = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 463
    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->getCLines()I

    move-result v1

    invoke-static {v1}, Lorg/apache/poi/util/HexDump;->intToHex(I)[C

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append([C)Ljava/lang/StringBuffer;

    .line 464
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->getCLines()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 466
    const-string/jumbo v1, "    .cWordsFtnEnd         = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 467
    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->getCWordsFtnEnd()I

    move-result v1

    invoke-static {v1}, Lorg/apache/poi/util/HexDump;->intToHex(I)[C

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append([C)Ljava/lang/StringBuffer;

    .line 468
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->getCWordsFtnEnd()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 470
    const-string/jumbo v1, "    .cChFtnEdn            = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 471
    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->getCChFtnEdn()I

    move-result v1

    invoke-static {v1}, Lorg/apache/poi/util/HexDump;->intToHex(I)[C

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append([C)Ljava/lang/StringBuffer;

    .line 472
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->getCChFtnEdn()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 474
    const-string/jumbo v1, "    .cPgFtnEdn            = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 475
    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->getCPgFtnEdn()S

    move-result v1

    invoke-static {v1}, Lorg/apache/poi/util/HexDump;->shortToHex(I)[C

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append([C)Ljava/lang/StringBuffer;

    .line 476
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->getCPgFtnEdn()S

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 478
    const-string/jumbo v1, "    .cParasFtnEdn         = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 479
    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->getCParasFtnEdn()I

    move-result v1

    invoke-static {v1}, Lorg/apache/poi/util/HexDump;->intToHex(I)[C

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append([C)Ljava/lang/StringBuffer;

    .line 480
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->getCParasFtnEdn()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 482
    const-string/jumbo v1, "    .cLinesFtnEdn         = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 483
    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->getCLinesFtnEdn()I

    move-result v1

    invoke-static {v1}, Lorg/apache/poi/util/HexDump;->intToHex(I)[C

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append([C)Ljava/lang/StringBuffer;

    .line 484
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->getCLinesFtnEdn()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 486
    const-string/jumbo v1, "    .lKeyProtDoc          = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 487
    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->getLKeyProtDoc()I

    move-result v1

    invoke-static {v1}, Lorg/apache/poi/util/HexDump;->intToHex(I)[C

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append([C)Ljava/lang/StringBuffer;

    .line 488
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->getLKeyProtDoc()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 490
    const-string/jumbo v1, "    .view                 = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 491
    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->getView()S

    move-result v1

    invoke-static {v1}, Lorg/apache/poi/util/HexDump;->shortToHex(I)[C

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append([C)Ljava/lang/StringBuffer;

    .line 492
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->getView()S

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 493
    const-string/jumbo v1, "         .wvkSaved                 = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->getWvkSaved()B

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 494
    const-string/jumbo v1, "         .wScaleSaved              = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->getWScaleSaved()S

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 495
    const-string/jumbo v1, "         .zkSaved                  = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->getZkSaved()B

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 496
    const-string/jumbo v1, "         .fRotateFontW6            = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->isFRotateFontW6()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 497
    const-string/jumbo v1, "         .iGutterPos               = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->isIGutterPos()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 499
    const-string/jumbo v1, "    .docinfo4             = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 500
    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->getDocinfo4()I

    move-result v1

    invoke-static {v1}, Lorg/apache/poi/util/HexDump;->intToHex(I)[C

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append([C)Ljava/lang/StringBuffer;

    .line 501
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->getDocinfo4()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 502
    const-string/jumbo v1, "         .fNoTabForInd             = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->isFNoTabForInd()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 503
    const-string/jumbo v1, "         .fNoSpaceRaiseLower       = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->isFNoSpaceRaiseLower()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 504
    const-string/jumbo v1, "         .fSupressSpdfAfterPageBreak     = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->isFSupressSpdfAfterPageBreak()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 505
    const-string/jumbo v1, "         .fWrapTrailSpaces         = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->isFWrapTrailSpaces()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 506
    const-string/jumbo v1, "         .fMapPrintTextColor       = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->isFMapPrintTextColor()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 507
    const-string/jumbo v1, "         .fNoColumnBalance         = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->isFNoColumnBalance()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 508
    const-string/jumbo v1, "         .fConvMailMergeEsc        = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->isFConvMailMergeEsc()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 509
    const-string/jumbo v1, "         .fSupressTopSpacing       = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->isFSupressTopSpacing()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 510
    const-string/jumbo v1, "         .fOrigWordTableRules      = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->isFOrigWordTableRules()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 511
    const-string/jumbo v1, "         .fTransparentMetafiles     = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->isFTransparentMetafiles()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 512
    const-string/jumbo v1, "         .fShowBreaksInFrames      = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->isFShowBreaksInFrames()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 513
    const-string/jumbo v1, "         .fSwapBordersFacingPgs     = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->isFSwapBordersFacingPgs()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 514
    const-string/jumbo v1, "         .fSuppressTopSPacingMac5     = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->isFSuppressTopSPacingMac5()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 515
    const-string/jumbo v1, "         .fTruncDxaExpand          = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->isFTruncDxaExpand()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 516
    const-string/jumbo v1, "         .fPrintBodyBeforeHdr      = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->isFPrintBodyBeforeHdr()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 517
    const-string/jumbo v1, "         .fNoLeading               = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->isFNoLeading()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 518
    const-string/jumbo v1, "         .fMWSmallCaps             = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->isFMWSmallCaps()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 520
    const-string/jumbo v1, "    .adt                  = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 521
    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->getAdt()S

    move-result v1

    invoke-static {v1}, Lorg/apache/poi/util/HexDump;->shortToHex(I)[C

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append([C)Ljava/lang/StringBuffer;

    .line 522
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->getAdt()S

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 524
    const-string/jumbo v1, "    .doptypography        = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 525
    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->getDoptypography()[B

    move-result-object v1

    invoke-static {v1}, Lorg/apache/poi/util/HexDump;->toHex([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 526
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->getDoptypography()[B

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 528
    const-string/jumbo v1, "    .dogrid               = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 529
    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->getDogrid()[B

    move-result-object v1

    invoke-static {v1}, Lorg/apache/poi/util/HexDump;->toHex([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 530
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->getDogrid()[B

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 532
    const-string/jumbo v1, "    .docinfo5             = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 533
    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->getDocinfo5()S

    move-result v1

    invoke-static {v1}, Lorg/apache/poi/util/HexDump;->shortToHex(I)[C

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append([C)Ljava/lang/StringBuffer;

    .line 534
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->getDocinfo5()S

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 535
    const-string/jumbo v1, "         .lvl                      = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->getLvl()B

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 536
    const-string/jumbo v1, "         .fGramAllDone             = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->isFGramAllDone()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 537
    const-string/jumbo v1, "         .fGramAllClean            = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->isFGramAllClean()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 538
    const-string/jumbo v1, "         .fSubsetFonts             = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->isFSubsetFonts()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 539
    const-string/jumbo v1, "         .fHideLastVersion         = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->isFHideLastVersion()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 540
    const-string/jumbo v1, "         .fHtmlDoc                 = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->isFHtmlDoc()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 541
    const-string/jumbo v1, "         .fSnapBorder              = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->isFSnapBorder()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 542
    const-string/jumbo v1, "         .fIncludeHeader           = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->isFIncludeHeader()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 543
    const-string/jumbo v1, "         .fIncludeFooter           = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->isFIncludeFooter()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 544
    const-string/jumbo v1, "         .fForcePageSizePag        = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->isFForcePageSizePag()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 545
    const-string/jumbo v1, "         .fMinFontSizePag          = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->isFMinFontSizePag()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 547
    const-string/jumbo v1, "    .docinfo6             = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 548
    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->getDocinfo6()S

    move-result v1

    invoke-static {v1}, Lorg/apache/poi/util/HexDump;->shortToHex(I)[C

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append([C)Ljava/lang/StringBuffer;

    .line 549
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->getDocinfo6()S

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 550
    const-string/jumbo v1, "         .fHaveVersions            = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->isFHaveVersions()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 551
    const-string/jumbo v1, "         .fAutoVersions            = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->isFAutoVersions()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 553
    const-string/jumbo v1, "    .asumyi               = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 554
    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->getAsumyi()[B

    move-result-object v1

    invoke-static {v1}, Lorg/apache/poi/util/HexDump;->toHex([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 555
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->getAsumyi()[B

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 557
    const-string/jumbo v1, "    .cChWS                = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 558
    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->getCChWS()I

    move-result v1

    invoke-static {v1}, Lorg/apache/poi/util/HexDump;->intToHex(I)[C

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append([C)Ljava/lang/StringBuffer;

    .line 559
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->getCChWS()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 561
    const-string/jumbo v1, "    .cChWSFtnEdn          = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 562
    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->getCChWSFtnEdn()I

    move-result v1

    invoke-static {v1}, Lorg/apache/poi/util/HexDump;->intToHex(I)[C

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append([C)Ljava/lang/StringBuffer;

    .line 563
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->getCChWSFtnEdn()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 565
    const-string/jumbo v1, "    .grfDocEvents         = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 566
    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->getGrfDocEvents()I

    move-result v1

    invoke-static {v1}, Lorg/apache/poi/util/HexDump;->intToHex(I)[C

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append([C)Ljava/lang/StringBuffer;

    .line 567
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->getGrfDocEvents()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 569
    const-string/jumbo v1, "    .virusinfo            = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 570
    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->getVirusinfo()I

    move-result v1

    invoke-static {v1}, Lorg/apache/poi/util/HexDump;->intToHex(I)[C

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append([C)Ljava/lang/StringBuffer;

    .line 571
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->getVirusinfo()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 572
    const-string/jumbo v1, "         .fVirusPrompted           = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->isFVirusPrompted()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 573
    const-string/jumbo v1, "         .fVirusLoadSafe           = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->isFVirusLoadSafe()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 574
    const-string/jumbo v1, "         .KeyVirusSession30        = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->getKeyVirusSession30()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 576
    const-string/jumbo v1, "    .Spare                = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 577
    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->getSpare()[B

    move-result-object v1

    invoke-static {v1}, Lorg/apache/poi/util/HexDump;->toHex([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 578
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->getSpare()[B

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 580
    const-string/jumbo v1, "    .reserved1            = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 581
    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->getReserved1()I

    move-result v1

    invoke-static {v1}, Lorg/apache/poi/util/HexDump;->intToHex(I)[C

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append([C)Ljava/lang/StringBuffer;

    .line 582
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->getReserved1()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 584
    const-string/jumbo v1, "    .reserved2            = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 585
    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->getReserved2()I

    move-result v1

    invoke-static {v1}, Lorg/apache/poi/util/HexDump;->intToHex(I)[C

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append([C)Ljava/lang/StringBuffer;

    .line 586
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->getReserved2()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 588
    const-string/jumbo v1, "    .cDBC                 = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 589
    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->getCDBC()I

    move-result v1

    invoke-static {v1}, Lorg/apache/poi/util/HexDump;->intToHex(I)[C

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append([C)Ljava/lang/StringBuffer;

    .line 590
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->getCDBC()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 592
    const-string/jumbo v1, "    .cDBCFtnEdn           = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 593
    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->getCDBCFtnEdn()I

    move-result v1

    invoke-static {v1}, Lorg/apache/poi/util/HexDump;->intToHex(I)[C

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append([C)Ljava/lang/StringBuffer;

    .line 594
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->getCDBCFtnEdn()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 596
    const-string/jumbo v1, "    .reserved             = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 597
    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->getReserved()I

    move-result v1

    invoke-static {v1}, Lorg/apache/poi/util/HexDump;->intToHex(I)[C

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append([C)Ljava/lang/StringBuffer;

    .line 598
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->getReserved()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 600
    const-string/jumbo v1, "    .nfcFtnRef            = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 601
    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->getNfcFtnRef()S

    move-result v1

    invoke-static {v1}, Lorg/apache/poi/util/HexDump;->shortToHex(I)[C

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append([C)Ljava/lang/StringBuffer;

    .line 602
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->getNfcFtnRef()S

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 604
    const-string/jumbo v1, "    .nfcEdnRef            = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 605
    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->getNfcEdnRef()S

    move-result v1

    invoke-static {v1}, Lorg/apache/poi/util/HexDump;->shortToHex(I)[C

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append([C)Ljava/lang/StringBuffer;

    .line 606
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->getNfcEdnRef()S

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 608
    const-string/jumbo v1, "    .hpsZoonFontPag       = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 609
    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->getHpsZoonFontPag()S

    move-result v1

    invoke-static {v1}, Lorg/apache/poi/util/HexDump;->shortToHex(I)[C

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append([C)Ljava/lang/StringBuffer;

    .line 610
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->getHpsZoonFontPag()S

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 612
    const-string/jumbo v1, "    .dywDispPag           = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 613
    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->getDywDispPag()S

    move-result v1

    invoke-static {v1}, Lorg/apache/poi/util/HexDump;->shortToHex(I)[C

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append([C)Ljava/lang/StringBuffer;

    .line 614
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/DOPAbstractType;->getDywDispPag()S

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 616
    const-string/jumbo v1, "[/DOP]\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 617
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
