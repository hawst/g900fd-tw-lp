.class public final Lorg/apache/poi/hdf/model/hdftypes/DocumentProperties;
.super Ljava/lang/Object;
.source "DocumentProperties.java"

# interfaces
.implements Lorg/apache/poi/hdf/model/hdftypes/HDFType;


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field public _epc:I

.field public _fFacingPages:Z

.field public _fpc:I

.field public _nEdn:I

.field public _nFtn:I

.field public _rncEdn:I

.field public _rncFtn:I


# direct methods
.method public constructor <init>([B)V
    .locals 4
    .param p1, "dopArray"    # [B

    .prologue
    const v3, 0xfffc

    const/4 v2, 0x0

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    aget-byte v1, p1, v2

    and-int/lit8 v1, v1, 0x1

    if-lez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    iput-boolean v1, p0, Lorg/apache/poi/hdf/model/hdftypes/DocumentProperties;->_fFacingPages:Z

    .line 41
    aget-byte v1, p1, v2

    and-int/lit8 v1, v1, 0x60

    shr-int/lit8 v1, v1, 0x5

    iput v1, p0, Lorg/apache/poi/hdf/model/hdftypes/DocumentProperties;->_fpc:I

    .line 43
    const/4 v1, 0x2

    invoke-static {p1, v1}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v0

    .line 44
    .local v0, "num":S
    and-int/lit8 v1, v0, 0x3

    iput v1, p0, Lorg/apache/poi/hdf/model/hdftypes/DocumentProperties;->_rncFtn:I

    .line 45
    and-int v1, v0, v3

    int-to-short v1, v1

    shr-int/lit8 v1, v1, 0x2

    iput v1, p0, Lorg/apache/poi/hdf/model/hdftypes/DocumentProperties;->_nFtn:I

    .line 46
    const/16 v1, 0x34

    invoke-static {p1, v1}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v0

    .line 47
    and-int/lit8 v1, v0, 0x3

    iput v1, p0, Lorg/apache/poi/hdf/model/hdftypes/DocumentProperties;->_rncEdn:I

    .line 48
    and-int v1, v0, v3

    int-to-short v1, v1

    shr-int/lit8 v1, v1, 0x2

    iput v1, p0, Lorg/apache/poi/hdf/model/hdftypes/DocumentProperties;->_nEdn:I

    .line 49
    const/16 v1, 0x36

    invoke-static {p1, v1}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v0

    .line 50
    and-int/lit8 v1, v0, 0x3

    iput v1, p0, Lorg/apache/poi/hdf/model/hdftypes/DocumentProperties;->_epc:I

    .line 51
    return-void

    .end local v0    # "num":S
    :cond_0
    move v1, v2

    .line 40
    goto :goto_0
.end method
