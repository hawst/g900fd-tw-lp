.class public final Lorg/apache/poi/hdf/model/hdftypes/TableCellDescriptor;
.super Lorg/apache/poi/hdf/model/hdftypes/definitions/TCAbstractType;
.source "TableCellDescriptor.java"

# interfaces
.implements Lorg/apache/poi/hdf/model/hdftypes/HDFType;


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/TCAbstractType;-><init>()V

    .line 46
    return-void
.end method

.method static convertBytesToTC([BI)Lorg/apache/poi/hdf/model/hdftypes/TableCellDescriptor;
    .locals 10
    .param p0, "array"    # [B
    .param p1, "offset"    # I

    .prologue
    const/4 v9, 0x2

    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 49
    new-instance v5, Lorg/apache/poi/hdf/model/hdftypes/TableCellDescriptor;

    invoke-direct {v5}, Lorg/apache/poi/hdf/model/hdftypes/TableCellDescriptor;-><init>()V

    .line 50
    .local v5, "tc":Lorg/apache/poi/hdf/model/hdftypes/TableCellDescriptor;
    invoke-static {p0, p1}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v4

    .line 51
    .local v4, "rgf":I
    and-int/lit8 v6, v4, 0x1

    if-lez v6, :cond_0

    move v6, v7

    :goto_0
    invoke-virtual {v5, v6}, Lorg/apache/poi/hdf/model/hdftypes/TableCellDescriptor;->setFFirstMerged(Z)V

    .line 52
    and-int/lit8 v6, v4, 0x2

    if-lez v6, :cond_1

    move v6, v7

    :goto_1
    invoke-virtual {v5, v6}, Lorg/apache/poi/hdf/model/hdftypes/TableCellDescriptor;->setFMerged(Z)V

    .line 53
    and-int/lit8 v6, v4, 0x4

    if-lez v6, :cond_2

    move v6, v7

    :goto_2
    invoke-virtual {v5, v6}, Lorg/apache/poi/hdf/model/hdftypes/TableCellDescriptor;->setFVertical(Z)V

    .line 54
    and-int/lit8 v6, v4, 0x8

    if-lez v6, :cond_3

    move v6, v7

    :goto_3
    invoke-virtual {v5, v6}, Lorg/apache/poi/hdf/model/hdftypes/TableCellDescriptor;->setFBackward(Z)V

    .line 55
    and-int/lit8 v6, v4, 0x10

    if-lez v6, :cond_4

    move v6, v7

    :goto_4
    invoke-virtual {v5, v6}, Lorg/apache/poi/hdf/model/hdftypes/TableCellDescriptor;->setFRotateFont(Z)V

    .line 56
    and-int/lit8 v6, v4, 0x20

    if-lez v6, :cond_5

    move v6, v7

    :goto_5
    invoke-virtual {v5, v6}, Lorg/apache/poi/hdf/model/hdftypes/TableCellDescriptor;->setFVertMerge(Z)V

    .line 57
    and-int/lit8 v6, v4, 0x40

    if-lez v6, :cond_6

    move v6, v7

    :goto_6
    invoke-virtual {v5, v6}, Lorg/apache/poi/hdf/model/hdftypes/TableCellDescriptor;->setFVertRestart(Z)V

    .line 58
    and-int/lit16 v6, v4, 0x180

    shr-int/lit8 v6, v6, 0x7

    int-to-byte v6, v6

    invoke-virtual {v5, v6}, Lorg/apache/poi/hdf/model/hdftypes/TableCellDescriptor;->setVertAlign(B)V

    .line 60
    new-array v3, v9, [S

    .line 61
    .local v3, "brcTop":[S
    new-array v1, v9, [S

    .line 62
    .local v1, "brcLeft":[S
    new-array v0, v9, [S

    .line 63
    .local v0, "brcBottom":[S
    new-array v2, v9, [S

    .line 65
    .local v2, "brcRight":[S
    add-int/lit8 v6, p1, 0x4

    invoke-static {p0, v6}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v6

    aput-short v6, v3, v8

    .line 66
    add-int/lit8 v6, p1, 0x6

    invoke-static {p0, v6}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v6

    aput-short v6, v3, v7

    .line 68
    add-int/lit8 v6, p1, 0x8

    invoke-static {p0, v6}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v6

    aput-short v6, v1, v8

    .line 69
    add-int/lit8 v6, p1, 0xa

    invoke-static {p0, v6}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v6

    aput-short v6, v1, v7

    .line 71
    add-int/lit8 v6, p1, 0xc

    invoke-static {p0, v6}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v6

    aput-short v6, v0, v8

    .line 72
    add-int/lit8 v6, p1, 0xe

    invoke-static {p0, v6}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v6

    aput-short v6, v0, v7

    .line 74
    add-int/lit8 v6, p1, 0x10

    invoke-static {p0, v6}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v6

    aput-short v6, v2, v8

    .line 75
    add-int/lit8 v6, p1, 0x12

    invoke-static {p0, v6}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v6

    aput-short v6, v2, v7

    .line 77
    return-object v5

    .end local v0    # "brcBottom":[S
    .end local v1    # "brcLeft":[S
    .end local v2    # "brcRight":[S
    .end local v3    # "brcTop":[S
    :cond_0
    move v6, v8

    .line 51
    goto/16 :goto_0

    :cond_1
    move v6, v8

    .line 52
    goto :goto_1

    :cond_2
    move v6, v8

    .line 53
    goto :goto_2

    :cond_3
    move v6, v8

    .line 54
    goto :goto_3

    :cond_4
    move v6, v8

    .line 55
    goto :goto_4

    :cond_5
    move v6, v8

    .line 56
    goto :goto_5

    :cond_6
    move v6, v8

    .line 57
    goto :goto_6
.end method
