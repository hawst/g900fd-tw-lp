.class public final Lorg/apache/poi/hdf/model/hdftypes/FileInformationBlock;
.super Lorg/apache/poi/hdf/model/hdftypes/definitions/FIBAbstractType;
.source "FileInformationBlock.java"


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# direct methods
.method public constructor <init>([B)V
    .locals 1
    .param p1, "mainDocument"    # [B

    .prologue
    const/4 v0, 0x0

    .line 202
    invoke-direct {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/FIBAbstractType;-><init>()V

    .line 204
    invoke-virtual {p0, p1, v0, v0}, Lorg/apache/poi/hdf/model/hdftypes/FileInformationBlock;->fillFields([BSI)V

    .line 240
    return-void
.end method
