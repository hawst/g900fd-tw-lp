.class public final Lorg/apache/poi/hdf/extractor/PAP;
.super Ljava/lang/Object;
.source "PAP.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field _anld:[B

.field _brcBar:[S

.field _brcBar1:S

.field _brcBetween:[S

.field _brcBetween1:S

.field _brcBottom:[S

.field _brcBottom1:S

.field _brcLeft:[S

.field _brcLeft1:S

.field _brcRight:[S

.field _brcRight1:S

.field _brcTop:[S

.field _brcTop1:S

.field _brcl:B

.field _brcp:B

.field _dcs:I

.field _dttmPropRMark:[B

.field _dxaAbs:I

.field _dxaFromText:I

.field _dxaLeft:I

.field _dxaLeft1:I

.field _dxaRight:I

.field _dxaWidth:I

.field _dyaAbs:I

.field _dyaAfter:I

.field _dyaBefore:I

.field _dyaFromText:I

.field _dyaHeight:I

.field _fAdjustRight:B

.field _fAutoSpaceDE:B

.field _fAutoSpaceDN:B

.field _fCrLf:B

.field _fInTable:B

.field _fKeep:B

.field _fKeepFollow:B

.field _fKinsoku:B

.field _fLocked:B

.field _fNoAutoHyph:B

.field _fNoLnn:B

.field _fOverflowPunct:B

.field _fPageBreakBefore:B

.field _fPropRMark:S

.field _fSideBySide:B

.field _fTopLinePunct:B

.field _fTtp:B

.field _fUsePgsuSettings:B

.field _fWindowControl:B

.field _fWordWrap:B

.field _fontAlign:S

.field _ibstPropRMark:S

.field _ilfo:I

.field _ilvl:B

.field _istd:I

.field _itbdMac:S

.field _jc:B

.field _lspd:[I

.field _numrm:[B

.field _phe:[B

.field _positionByte:B

.field _shd:I

.field _wAlignFont:I

.field _wr:B


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x2

    .line 93
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    new-array v0, v1, [I

    iput-object v0, p0, Lorg/apache/poi/hdf/extractor/PAP;->_lspd:[I

    .line 48
    const/16 v0, 0xc

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/apache/poi/hdf/extractor/PAP;->_phe:[B

    .line 67
    new-array v0, v1, [S

    iput-object v0, p0, Lorg/apache/poi/hdf/extractor/PAP;->_brcTop:[S

    .line 68
    new-array v0, v1, [S

    iput-object v0, p0, Lorg/apache/poi/hdf/extractor/PAP;->_brcLeft:[S

    .line 69
    new-array v0, v1, [S

    iput-object v0, p0, Lorg/apache/poi/hdf/extractor/PAP;->_brcBottom:[S

    .line 70
    new-array v0, v1, [S

    iput-object v0, p0, Lorg/apache/poi/hdf/extractor/PAP;->_brcRight:[S

    .line 71
    new-array v0, v1, [S

    iput-object v0, p0, Lorg/apache/poi/hdf/extractor/PAP;->_brcBetween:[S

    .line 72
    new-array v0, v1, [S

    iput-object v0, p0, Lorg/apache/poi/hdf/extractor/PAP;->_brcBar:[S

    .line 84
    const/16 v0, 0x54

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/apache/poi/hdf/extractor/PAP;->_anld:[B

    .line 87
    const/4 v0, 0x4

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/apache/poi/hdf/extractor/PAP;->_dttmPropRMark:[B

    .line 88
    const/16 v0, 0x8

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/apache/poi/hdf/extractor/PAP;->_numrm:[B

    .line 95
    iput-byte v2, p0, Lorg/apache/poi/hdf/extractor/PAP;->_fWindowControl:B

    .line 97
    iget-object v0, p0, Lorg/apache/poi/hdf/extractor/PAP;->_lspd:[I

    aput v2, v0, v2

    .line 98
    const/16 v0, 0x9

    iput-byte v0, p0, Lorg/apache/poi/hdf/extractor/PAP;->_ilvl:B

    .line 99
    return-void
.end method


# virtual methods
.method public clone()Ljava/lang/Object;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    const/16 v7, 0xc

    const/16 v6, 0x8

    const/4 v5, 0x4

    const/4 v3, 0x2

    const/4 v4, 0x0

    .line 102
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/hdf/extractor/PAP;

    .line 104
    .local v0, "clone":Lorg/apache/poi/hdf/extractor/PAP;
    new-array v1, v3, [S

    iput-object v1, v0, Lorg/apache/poi/hdf/extractor/PAP;->_brcBar:[S

    .line 105
    new-array v1, v3, [S

    iput-object v1, v0, Lorg/apache/poi/hdf/extractor/PAP;->_brcBottom:[S

    .line 106
    new-array v1, v3, [S

    iput-object v1, v0, Lorg/apache/poi/hdf/extractor/PAP;->_brcLeft:[S

    .line 107
    new-array v1, v3, [S

    iput-object v1, v0, Lorg/apache/poi/hdf/extractor/PAP;->_brcBetween:[S

    .line 108
    new-array v1, v3, [S

    iput-object v1, v0, Lorg/apache/poi/hdf/extractor/PAP;->_brcRight:[S

    .line 109
    new-array v1, v3, [S

    iput-object v1, v0, Lorg/apache/poi/hdf/extractor/PAP;->_brcTop:[S

    .line 110
    new-array v1, v3, [I

    iput-object v1, v0, Lorg/apache/poi/hdf/extractor/PAP;->_lspd:[I

    .line 111
    new-array v1, v7, [B

    iput-object v1, v0, Lorg/apache/poi/hdf/extractor/PAP;->_phe:[B

    .line 112
    const/16 v1, 0x54

    new-array v1, v1, [B

    iput-object v1, v0, Lorg/apache/poi/hdf/extractor/PAP;->_anld:[B

    .line 113
    new-array v1, v5, [B

    iput-object v1, v0, Lorg/apache/poi/hdf/extractor/PAP;->_dttmPropRMark:[B

    .line 114
    new-array v1, v6, [B

    iput-object v1, v0, Lorg/apache/poi/hdf/extractor/PAP;->_numrm:[B

    .line 116
    iget-object v1, p0, Lorg/apache/poi/hdf/extractor/PAP;->_brcBar:[S

    iget-object v2, v0, Lorg/apache/poi/hdf/extractor/PAP;->_brcBar:[S

    invoke-static {v1, v4, v2, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 117
    iget-object v1, p0, Lorg/apache/poi/hdf/extractor/PAP;->_brcBottom:[S

    iget-object v2, v0, Lorg/apache/poi/hdf/extractor/PAP;->_brcBottom:[S

    invoke-static {v1, v4, v2, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 118
    iget-object v1, p0, Lorg/apache/poi/hdf/extractor/PAP;->_brcLeft:[S

    iget-object v2, v0, Lorg/apache/poi/hdf/extractor/PAP;->_brcLeft:[S

    invoke-static {v1, v4, v2, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 119
    iget-object v1, p0, Lorg/apache/poi/hdf/extractor/PAP;->_brcBetween:[S

    iget-object v2, v0, Lorg/apache/poi/hdf/extractor/PAP;->_brcBetween:[S

    invoke-static {v1, v4, v2, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 120
    iget-object v1, p0, Lorg/apache/poi/hdf/extractor/PAP;->_brcRight:[S

    iget-object v2, v0, Lorg/apache/poi/hdf/extractor/PAP;->_brcRight:[S

    invoke-static {v1, v4, v2, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 121
    iget-object v1, p0, Lorg/apache/poi/hdf/extractor/PAP;->_brcTop:[S

    iget-object v2, v0, Lorg/apache/poi/hdf/extractor/PAP;->_brcTop:[S

    invoke-static {v1, v4, v2, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 122
    iget-object v1, p0, Lorg/apache/poi/hdf/extractor/PAP;->_lspd:[I

    iget-object v2, v0, Lorg/apache/poi/hdf/extractor/PAP;->_lspd:[I

    invoke-static {v1, v4, v2, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 123
    iget-object v1, p0, Lorg/apache/poi/hdf/extractor/PAP;->_phe:[B

    iget-object v2, v0, Lorg/apache/poi/hdf/extractor/PAP;->_phe:[B

    invoke-static {v1, v4, v2, v4, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 124
    iget-object v1, p0, Lorg/apache/poi/hdf/extractor/PAP;->_anld:[B

    iget-object v2, v0, Lorg/apache/poi/hdf/extractor/PAP;->_anld:[B

    const/16 v3, 0x54

    invoke-static {v1, v4, v2, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 125
    iget-object v1, p0, Lorg/apache/poi/hdf/extractor/PAP;->_dttmPropRMark:[B

    iget-object v2, v0, Lorg/apache/poi/hdf/extractor/PAP;->_dttmPropRMark:[B

    invoke-static {v1, v4, v2, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 126
    iget-object v1, p0, Lorg/apache/poi/hdf/extractor/PAP;->_numrm:[B

    iget-object v2, v0, Lorg/apache/poi/hdf/extractor/PAP;->_numrm:[B

    invoke-static {v1, v4, v2, v4, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 128
    return-object v0
.end method
