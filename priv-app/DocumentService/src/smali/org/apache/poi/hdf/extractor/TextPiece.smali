.class public final Lorg/apache/poi/hdf/extractor/TextPiece;
.super Lorg/apache/poi/hdf/extractor/util/PropertyNode;
.source "TextPiece.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private _usesUnicode:Z


# direct methods
.method public constructor <init>(IIZ)V
    .locals 2
    .param p1, "start"    # I
    .param p2, "length"    # I
    .param p3, "unicode"    # Z

    .prologue
    .line 35
    add-int v0, p1, p2

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lorg/apache/poi/hdf/extractor/util/PropertyNode;-><init>(II[B)V

    .line 36
    iput-boolean p3, p0, Lorg/apache/poi/hdf/extractor/TextPiece;->_usesUnicode:Z

    .line 41
    return-void
.end method


# virtual methods
.method public compareTo(Ljava/lang/Object;)I
    .locals 1
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    .line 48
    const/4 v0, 0x0

    return v0
.end method

.method public usesUnicode()Z
    .locals 1

    .prologue
    .line 44
    iget-boolean v0, p0, Lorg/apache/poi/hdf/extractor/TextPiece;->_usesUnicode:Z

    return v0
.end method
