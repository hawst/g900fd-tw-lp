.class public final Lorg/apache/poi/hdf/model/hdftypes/ParagraphProperties;
.super Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;
.source "ParagraphProperties.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x2

    .line 31
    invoke-direct {p0}, Lorg/apache/poi/hdf/model/hdftypes/definitions/PAPAbstractType;-><init>()V

    .line 33
    new-array v0, v2, [S

    .line 34
    .local v0, "lspd":[S
    invoke-virtual {p0, v1}, Lorg/apache/poi/hdf/model/hdftypes/ParagraphProperties;->setFWidowControl(B)V

    .line 36
    aput-short v1, v0, v1

    .line 37
    const/16 v1, 0x9

    invoke-virtual {p0, v1}, Lorg/apache/poi/hdf/model/hdftypes/ParagraphProperties;->setIlvl(B)V

    .line 39
    invoke-virtual {p0, v0}, Lorg/apache/poi/hdf/model/hdftypes/ParagraphProperties;->setLspd([S)V

    .line 40
    new-array v1, v2, [S

    invoke-virtual {p0, v1}, Lorg/apache/poi/hdf/model/hdftypes/ParagraphProperties;->setBrcBar([S)V

    .line 41
    new-array v1, v2, [S

    invoke-virtual {p0, v1}, Lorg/apache/poi/hdf/model/hdftypes/ParagraphProperties;->setBrcBottom([S)V

    .line 42
    new-array v1, v2, [S

    invoke-virtual {p0, v1}, Lorg/apache/poi/hdf/model/hdftypes/ParagraphProperties;->setBrcLeft([S)V

    .line 43
    new-array v1, v2, [S

    invoke-virtual {p0, v1}, Lorg/apache/poi/hdf/model/hdftypes/ParagraphProperties;->setBrcBetween([S)V

    .line 44
    new-array v1, v2, [S

    invoke-virtual {p0, v1}, Lorg/apache/poi/hdf/model/hdftypes/ParagraphProperties;->setBrcRight([S)V

    .line 45
    new-array v1, v2, [S

    invoke-virtual {p0, v1}, Lorg/apache/poi/hdf/model/hdftypes/ParagraphProperties;->setBrcTop([S)V

    .line 46
    const/16 v1, 0xc

    new-array v1, v1, [B

    invoke-virtual {p0, v1}, Lorg/apache/poi/hdf/model/hdftypes/ParagraphProperties;->setPhe([B)V

    .line 47
    const/16 v1, 0x54

    new-array v1, v1, [B

    invoke-virtual {p0, v1}, Lorg/apache/poi/hdf/model/hdftypes/ParagraphProperties;->setAnld([B)V

    .line 48
    const/4 v1, 0x4

    new-array v1, v1, [B

    invoke-virtual {p0, v1}, Lorg/apache/poi/hdf/model/hdftypes/ParagraphProperties;->setDttmPropRMark([B)V

    .line 49
    const/16 v1, 0x8

    new-array v1, v1, [B

    invoke-virtual {p0, v1}, Lorg/apache/poi/hdf/model/hdftypes/ParagraphProperties;->setNumrm([B)V

    .line 52
    return-void
.end method


# virtual methods
.method public clone()Ljava/lang/Object;
    .locals 16
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 55
    invoke-super/range {p0 .. p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lorg/apache/poi/hdf/model/hdftypes/ParagraphProperties;

    .line 57
    .local v7, "clone":Lorg/apache/poi/hdf/model/hdftypes/ParagraphProperties;
    const/4 v12, 0x2

    new-array v1, v12, [S

    .line 58
    .local v1, "brcBar":[S
    const/4 v12, 0x2

    new-array v3, v12, [S

    .line 59
    .local v3, "brcBottom":[S
    const/4 v12, 0x2

    new-array v4, v12, [S

    .line 60
    .local v4, "brcLeft":[S
    const/4 v12, 0x2

    new-array v2, v12, [S

    .line 61
    .local v2, "brcBetween":[S
    const/4 v12, 0x2

    new-array v5, v12, [S

    .line 62
    .local v5, "brcRight":[S
    const/4 v12, 0x2

    new-array v6, v12, [S

    .line 63
    .local v6, "brcTop":[S
    const/4 v12, 0x2

    new-array v9, v12, [S

    .line 64
    .local v9, "lspd":[S
    const/16 v12, 0xc

    new-array v11, v12, [B

    .line 65
    .local v11, "phe":[B
    const/16 v12, 0x54

    new-array v0, v12, [B

    .line 66
    .local v0, "anld":[B
    const/4 v12, 0x4

    new-array v8, v12, [B

    .line 67
    .local v8, "dttmPropRMark":[B
    const/16 v12, 0x8

    new-array v10, v12, [B

    .line 69
    .local v10, "numrm":[B
    invoke-virtual/range {p0 .. p0}, Lorg/apache/poi/hdf/model/hdftypes/ParagraphProperties;->getBrcBar()[S

    move-result-object v12

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x2

    invoke-static {v12, v13, v1, v14, v15}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 70
    invoke-virtual/range {p0 .. p0}, Lorg/apache/poi/hdf/model/hdftypes/ParagraphProperties;->getBrcBottom()[S

    move-result-object v12

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x2

    invoke-static {v12, v13, v3, v14, v15}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 71
    invoke-virtual/range {p0 .. p0}, Lorg/apache/poi/hdf/model/hdftypes/ParagraphProperties;->getBrcLeft()[S

    move-result-object v12

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x2

    invoke-static {v12, v13, v4, v14, v15}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 72
    invoke-virtual/range {p0 .. p0}, Lorg/apache/poi/hdf/model/hdftypes/ParagraphProperties;->getBrcBetween()[S

    move-result-object v12

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x2

    invoke-static {v12, v13, v2, v14, v15}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 73
    invoke-virtual/range {p0 .. p0}, Lorg/apache/poi/hdf/model/hdftypes/ParagraphProperties;->getBrcRight()[S

    move-result-object v12

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x2

    invoke-static {v12, v13, v5, v14, v15}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 74
    invoke-virtual/range {p0 .. p0}, Lorg/apache/poi/hdf/model/hdftypes/ParagraphProperties;->getBrcTop()[S

    move-result-object v12

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x2

    invoke-static {v12, v13, v6, v14, v15}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 75
    invoke-virtual/range {p0 .. p0}, Lorg/apache/poi/hdf/model/hdftypes/ParagraphProperties;->getLspd()[S

    move-result-object v12

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x2

    invoke-static {v12, v13, v9, v14, v15}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 76
    invoke-virtual/range {p0 .. p0}, Lorg/apache/poi/hdf/model/hdftypes/ParagraphProperties;->getPhe()[B

    move-result-object v12

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/16 v15, 0xc

    invoke-static {v12, v13, v11, v14, v15}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 77
    invoke-virtual/range {p0 .. p0}, Lorg/apache/poi/hdf/model/hdftypes/ParagraphProperties;->getAnld()[B

    move-result-object v12

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/16 v15, 0x54

    invoke-static {v12, v13, v0, v14, v15}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 78
    invoke-virtual/range {p0 .. p0}, Lorg/apache/poi/hdf/model/hdftypes/ParagraphProperties;->getDttmPropRMark()[B

    move-result-object v12

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x4

    invoke-static {v12, v13, v8, v14, v15}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 79
    invoke-virtual/range {p0 .. p0}, Lorg/apache/poi/hdf/model/hdftypes/ParagraphProperties;->getNumrm()[B

    move-result-object v12

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/16 v15, 0x8

    invoke-static {v12, v13, v10, v14, v15}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 82
    invoke-virtual {v7, v1}, Lorg/apache/poi/hdf/model/hdftypes/ParagraphProperties;->setBrcBar([S)V

    .line 83
    invoke-virtual {v7, v3}, Lorg/apache/poi/hdf/model/hdftypes/ParagraphProperties;->setBrcBottom([S)V

    .line 84
    invoke-virtual {v7, v4}, Lorg/apache/poi/hdf/model/hdftypes/ParagraphProperties;->setBrcLeft([S)V

    .line 85
    invoke-virtual {v7, v2}, Lorg/apache/poi/hdf/model/hdftypes/ParagraphProperties;->setBrcBetween([S)V

    .line 86
    invoke-virtual {v7, v5}, Lorg/apache/poi/hdf/model/hdftypes/ParagraphProperties;->setBrcRight([S)V

    .line 87
    invoke-virtual {v7, v6}, Lorg/apache/poi/hdf/model/hdftypes/ParagraphProperties;->setBrcTop([S)V

    .line 88
    invoke-virtual {v7, v9}, Lorg/apache/poi/hdf/model/hdftypes/ParagraphProperties;->setLspd([S)V

    .line 89
    invoke-virtual {v7, v11}, Lorg/apache/poi/hdf/model/hdftypes/ParagraphProperties;->setPhe([B)V

    .line 90
    invoke-virtual {v7, v0}, Lorg/apache/poi/hdf/model/hdftypes/ParagraphProperties;->setAnld([B)V

    .line 91
    invoke-virtual {v7, v8}, Lorg/apache/poi/hdf/model/hdftypes/ParagraphProperties;->setDttmPropRMark([B)V

    .line 92
    invoke-virtual {v7, v10}, Lorg/apache/poi/hdf/model/hdftypes/ParagraphProperties;->setNumrm([B)V

    .line 93
    return-object v7
.end method
