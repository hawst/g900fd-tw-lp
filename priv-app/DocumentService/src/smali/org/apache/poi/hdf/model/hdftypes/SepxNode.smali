.class public final Lorg/apache/poi/hdf/model/hdftypes/SepxNode;
.super Lorg/apache/poi/hdf/model/hdftypes/PropertyNode;
.source "SepxNode.java"


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field _index:I


# direct methods
.method public constructor <init>(III[B)V
    .locals 0
    .param p1, "index"    # I
    .param p2, "start"    # I
    .param p3, "end"    # I
    .param p4, "sepx"    # [B

    .prologue
    .line 33
    invoke-direct {p0, p2, p3, p4}, Lorg/apache/poi/hdf/model/hdftypes/PropertyNode;-><init>(II[B)V

    .line 34
    return-void
.end method


# virtual methods
.method public compareTo(Ljava/lang/Object;)I
    .locals 1
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    .line 41
    const/4 v0, 0x0

    return v0
.end method

.method public getSepx()[B
    .locals 1

    .prologue
    .line 37
    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/SepxNode;->getGrpprl()[B

    move-result-object v0

    return-object v0
.end method
