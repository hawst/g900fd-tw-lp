.class public final Lorg/apache/poi/hdf/extractor/PropertySet;
.super Ljava/lang/Object;
.source "PropertySet.java"


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private _sb:I

.field private _size:I


# direct methods
.method public constructor <init>(Ljava/lang/String;IIIIIII)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "type"    # I
    .param p3, "previous"    # I
    .param p4, "next"    # I
    .param p5, "dir"    # I
    .param p6, "sb"    # I
    .param p7, "size"    # I
    .param p8, "num"    # I

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    iput p6, p0, Lorg/apache/poi/hdf/extractor/PropertySet;->_sb:I

    .line 46
    iput p7, p0, Lorg/apache/poi/hdf/extractor/PropertySet;->_size:I

    .line 48
    return-void
.end method


# virtual methods
.method public getSize()I
    .locals 1

    .prologue
    .line 51
    iget v0, p0, Lorg/apache/poi/hdf/extractor/PropertySet;->_size:I

    return v0
.end method

.method public getStartBlock()I
    .locals 1

    .prologue
    .line 55
    iget v0, p0, Lorg/apache/poi/hdf/extractor/PropertySet;->_sb:I

    return v0
.end method
