.class public final Lorg/apache/poi/hdf/extractor/StyleSheet;
.super Ljava/lang/Object;
.source "StyleSheet.java"


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field private static final CHP_TYPE:I = 0x2

.field private static final NIL_STYLE:I = 0xfff

.field private static final PAP_TYPE:I = 0x1

.field private static final SEP_TYPE:I = 0x4

.field private static final TAP_TYPE:I = 0x5


# instance fields
.field _nilStyle:Lorg/apache/poi/hdf/extractor/StyleDescription;

.field _styleDescriptions:[Lorg/apache/poi/hdf/extractor/StyleDescription;


# direct methods
.method public constructor <init>([B)V
    .locals 14
    .param p1, "styleSheet"    # [B

    .prologue
    const/4 v13, 0x2

    const/4 v12, 0x1

    const/4 v11, 0x0

    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    new-instance v10, Lorg/apache/poi/hdf/extractor/StyleDescription;

    invoke-direct {v10}, Lorg/apache/poi/hdf/extractor/StyleDescription;-><init>()V

    iput-object v10, p0, Lorg/apache/poi/hdf/extractor/StyleSheet;->_nilStyle:Lorg/apache/poi/hdf/extractor/StyleDescription;

    .line 41
    invoke-static {p1, v11}, Lorg/apache/poi/hdf/extractor/Utils;->convertBytesToShort([BI)S

    move-result v8

    .line 42
    .local v8, "stshiLength":I
    invoke-static {p1, v13}, Lorg/apache/poi/hdf/extractor/Utils;->convertBytesToShort([BI)S

    move-result v5

    .line 43
    .local v5, "stdCount":I
    const/4 v10, 0x4

    invoke-static {p1, v10}, Lorg/apache/poi/hdf/extractor/Utils;->convertBytesToShort([BI)S

    move-result v1

    .line 44
    .local v1, "baseLength":I
    const/4 v10, 0x3

    new-array v3, v10, [I

    .line 46
    .local v3, "rgftc":[I
    const/16 v10, 0xe

    invoke-static {p1, v10}, Lorg/apache/poi/hdf/extractor/Utils;->convertBytesToInt([BI)I

    move-result v10

    aput v10, v3, v11

    .line 47
    const/16 v10, 0x12

    invoke-static {p1, v10}, Lorg/apache/poi/hdf/extractor/Utils;->convertBytesToInt([BI)I

    move-result v10

    aput v10, v3, v12

    .line 48
    const/16 v10, 0x16

    invoke-static {p1, v10}, Lorg/apache/poi/hdf/extractor/Utils;->convertBytesToInt([BI)I

    move-result v10

    aput v10, v3, v13

    .line 50
    const/4 v2, 0x0

    .line 51
    .local v2, "offset":I
    new-array v10, v5, [Lorg/apache/poi/hdf/extractor/StyleDescription;

    iput-object v10, p0, Lorg/apache/poi/hdf/extractor/StyleSheet;->_styleDescriptions:[Lorg/apache/poi/hdf/extractor/StyleDescription;

    .line 52
    const/4 v9, 0x0

    .local v9, "x":I
    :goto_0
    if-lt v9, v5, :cond_0

    .line 72
    const/4 v9, 0x0

    :goto_1
    iget-object v10, p0, Lorg/apache/poi/hdf/extractor/StyleSheet;->_styleDescriptions:[Lorg/apache/poi/hdf/extractor/StyleDescription;

    array-length v10, v10

    if-lt v9, v10, :cond_2

    .line 80
    return-void

    .line 54
    :cond_0
    add-int/lit8 v10, v8, 0x2

    add-int v6, v10, v2

    .line 55
    .local v6, "stdOffset":I
    invoke-static {p1, v6}, Lorg/apache/poi/hdf/extractor/Utils;->convertBytesToShort([BI)S

    move-result v7

    .line 56
    .local v7, "stdSize":I
    if-lez v7, :cond_1

    .line 58
    new-array v4, v7, [B

    .line 61
    .local v4, "std":[B
    add-int/lit8 v6, v6, 0x2

    .line 62
    invoke-static {p1, v6, v4, v11, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 63
    new-instance v0, Lorg/apache/poi/hdf/extractor/StyleDescription;

    invoke-direct {v0, v4, v1, v12}, Lorg/apache/poi/hdf/extractor/StyleDescription;-><init>([BIZ)V

    .line 65
    .local v0, "aStyle":Lorg/apache/poi/hdf/extractor/StyleDescription;
    iget-object v10, p0, Lorg/apache/poi/hdf/extractor/StyleSheet;->_styleDescriptions:[Lorg/apache/poi/hdf/extractor/StyleDescription;

    aput-object v0, v10, v9

    .line 69
    .end local v0    # "aStyle":Lorg/apache/poi/hdf/extractor/StyleDescription;
    .end local v4    # "std":[B
    :cond_1
    add-int/lit8 v10, v7, 0x2

    add-int/2addr v2, v10

    .line 52
    add-int/lit8 v9, v9, 0x1

    goto :goto_0

    .line 74
    .end local v6    # "stdOffset":I
    .end local v7    # "stdSize":I
    :cond_2
    iget-object v10, p0, Lorg/apache/poi/hdf/extractor/StyleSheet;->_styleDescriptions:[Lorg/apache/poi/hdf/extractor/StyleDescription;

    aget-object v10, v10, v9

    if-eqz v10, :cond_3

    .line 76
    invoke-direct {p0, v9}, Lorg/apache/poi/hdf/extractor/StyleSheet;->createPap(I)V

    .line 77
    invoke-direct {p0, v9}, Lorg/apache/poi/hdf/extractor/StyleSheet;->createChp(I)V

    .line 72
    :cond_3
    add-int/lit8 v9, v9, 0x1

    goto :goto_1
.end method

.method private createChp(I)V
    .locals 6
    .param p1, "istd"    # I

    .prologue
    .line 108
    iget-object v5, p0, Lorg/apache/poi/hdf/extractor/StyleSheet;->_styleDescriptions:[Lorg/apache/poi/hdf/extractor/StyleDescription;

    aget-object v4, v5, p1

    .line 109
    .local v4, "sd":Lorg/apache/poi/hdf/extractor/StyleDescription;
    invoke-virtual {v4}, Lorg/apache/poi/hdf/extractor/StyleDescription;->getCHP()Lorg/apache/poi/hdf/extractor/CHP;

    move-result-object v1

    .line 110
    .local v1, "chp":Lorg/apache/poi/hdf/extractor/CHP;
    invoke-virtual {v4}, Lorg/apache/poi/hdf/extractor/StyleDescription;->getCHPX()[B

    move-result-object v2

    .line 111
    .local v2, "chpx":[B
    invoke-virtual {v4}, Lorg/apache/poi/hdf/extractor/StyleDescription;->getBaseStyle()I

    move-result v0

    .line 112
    .local v0, "baseIndex":I
    if-nez v1, :cond_1

    if-eqz v2, :cond_1

    .line 114
    iget-object v5, p0, Lorg/apache/poi/hdf/extractor/StyleSheet;->_nilStyle:Lorg/apache/poi/hdf/extractor/StyleDescription;

    invoke-virtual {v5}, Lorg/apache/poi/hdf/extractor/StyleDescription;->getCHP()Lorg/apache/poi/hdf/extractor/CHP;

    move-result-object v3

    .line 115
    .local v3, "parentCHP":Lorg/apache/poi/hdf/extractor/CHP;
    const/16 v5, 0xfff

    if-eq v0, v5, :cond_0

    .line 118
    iget-object v5, p0, Lorg/apache/poi/hdf/extractor/StyleSheet;->_styleDescriptions:[Lorg/apache/poi/hdf/extractor/StyleDescription;

    aget-object v5, v5, v0

    invoke-virtual {v5}, Lorg/apache/poi/hdf/extractor/StyleDescription;->getCHP()Lorg/apache/poi/hdf/extractor/CHP;

    move-result-object v3

    .line 119
    if-nez v3, :cond_0

    .line 121
    invoke-direct {p0, v0}, Lorg/apache/poi/hdf/extractor/StyleSheet;->createChp(I)V

    .line 122
    iget-object v5, p0, Lorg/apache/poi/hdf/extractor/StyleSheet;->_styleDescriptions:[Lorg/apache/poi/hdf/extractor/StyleDescription;

    aget-object v5, v5, v0

    invoke-virtual {v5}, Lorg/apache/poi/hdf/extractor/StyleDescription;->getCHP()Lorg/apache/poi/hdf/extractor/CHP;

    move-result-object v3

    .line 127
    :cond_0
    invoke-static {v2, v3, p0}, Lorg/apache/poi/hdf/extractor/StyleSheet;->uncompressProperty([BLjava/lang/Object;Lorg/apache/poi/hdf/extractor/StyleSheet;)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "chp":Lorg/apache/poi/hdf/extractor/CHP;
    check-cast v1, Lorg/apache/poi/hdf/extractor/CHP;

    .line 128
    .restart local v1    # "chp":Lorg/apache/poi/hdf/extractor/CHP;
    invoke-virtual {v4, v1}, Lorg/apache/poi/hdf/extractor/StyleDescription;->setCHP(Lorg/apache/poi/hdf/extractor/CHP;)V

    .line 130
    .end local v3    # "parentCHP":Lorg/apache/poi/hdf/extractor/CHP;
    :cond_1
    return-void
.end method

.method private createPap(I)V
    .locals 6
    .param p1, "istd"    # I

    .prologue
    .line 83
    iget-object v5, p0, Lorg/apache/poi/hdf/extractor/StyleSheet;->_styleDescriptions:[Lorg/apache/poi/hdf/extractor/StyleDescription;

    aget-object v4, v5, p1

    .line 84
    .local v4, "sd":Lorg/apache/poi/hdf/extractor/StyleDescription;
    invoke-virtual {v4}, Lorg/apache/poi/hdf/extractor/StyleDescription;->getPAP()Lorg/apache/poi/hdf/extractor/PAP;

    move-result-object v1

    .line 85
    .local v1, "pap":Lorg/apache/poi/hdf/extractor/PAP;
    invoke-virtual {v4}, Lorg/apache/poi/hdf/extractor/StyleDescription;->getPAPX()[B

    move-result-object v2

    .line 86
    .local v2, "papx":[B
    invoke-virtual {v4}, Lorg/apache/poi/hdf/extractor/StyleDescription;->getBaseStyle()I

    move-result v0

    .line 87
    .local v0, "baseIndex":I
    if-nez v1, :cond_1

    if-eqz v2, :cond_1

    .line 89
    iget-object v5, p0, Lorg/apache/poi/hdf/extractor/StyleSheet;->_nilStyle:Lorg/apache/poi/hdf/extractor/StyleDescription;

    invoke-virtual {v5}, Lorg/apache/poi/hdf/extractor/StyleDescription;->getPAP()Lorg/apache/poi/hdf/extractor/PAP;

    move-result-object v3

    .line 90
    .local v3, "parentPAP":Lorg/apache/poi/hdf/extractor/PAP;
    const/16 v5, 0xfff

    if-eq v0, v5, :cond_0

    .line 93
    iget-object v5, p0, Lorg/apache/poi/hdf/extractor/StyleSheet;->_styleDescriptions:[Lorg/apache/poi/hdf/extractor/StyleDescription;

    aget-object v5, v5, v0

    invoke-virtual {v5}, Lorg/apache/poi/hdf/extractor/StyleDescription;->getPAP()Lorg/apache/poi/hdf/extractor/PAP;

    move-result-object v3

    .line 94
    if-nez v3, :cond_0

    .line 96
    invoke-direct {p0, v0}, Lorg/apache/poi/hdf/extractor/StyleSheet;->createPap(I)V

    .line 97
    iget-object v5, p0, Lorg/apache/poi/hdf/extractor/StyleSheet;->_styleDescriptions:[Lorg/apache/poi/hdf/extractor/StyleDescription;

    aget-object v5, v5, v0

    invoke-virtual {v5}, Lorg/apache/poi/hdf/extractor/StyleDescription;->getPAP()Lorg/apache/poi/hdf/extractor/PAP;

    move-result-object v3

    .line 102
    :cond_0
    invoke-static {v2, v3, p0}, Lorg/apache/poi/hdf/extractor/StyleSheet;->uncompressProperty([BLjava/lang/Object;Lorg/apache/poi/hdf/extractor/StyleSheet;)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "pap":Lorg/apache/poi/hdf/extractor/PAP;
    check-cast v1, Lorg/apache/poi/hdf/extractor/PAP;

    .line 103
    .restart local v1    # "pap":Lorg/apache/poi/hdf/extractor/PAP;
    invoke-virtual {v4, v1}, Lorg/apache/poi/hdf/extractor/StyleDescription;->setPAP(Lorg/apache/poi/hdf/extractor/PAP;)V

    .line 105
    .end local v3    # "parentPAP":Lorg/apache/poi/hdf/extractor/PAP;
    :cond_1
    return-void
.end method

.method static doCHPOperation(Lorg/apache/poi/hdf/extractor/CHP;Lorg/apache/poi/hdf/extractor/CHP;II[B[BILorg/apache/poi/hdf/extractor/StyleSheet;)V
    .locals 21
    .param p0, "oldCHP"    # Lorg/apache/poi/hdf/extractor/CHP;
    .param p1, "newCHP"    # Lorg/apache/poi/hdf/extractor/CHP;
    .param p2, "operand"    # I
    .param p3, "param"    # I
    .param p4, "varParam"    # [B
    .param p5, "grpprl"    # [B
    .param p6, "offset"    # I
    .param p7, "styleSheet"    # Lorg/apache/poi/hdf/extractor/StyleSheet;

    .prologue
    .line 139
    packed-switch p2, :pswitch_data_0

    .line 577
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 142
    :pswitch_1
    invoke-static/range {p3 .. p3}, Lorg/apache/poi/hdf/extractor/StyleSheet;->getFlag(I)Z

    move-result v2

    move-object/from16 v0, p1

    iput-boolean v2, v0, Lorg/apache/poi/hdf/extractor/CHP;->_fRMarkDel:Z

    goto :goto_0

    .line 145
    :pswitch_2
    invoke-static/range {p3 .. p3}, Lorg/apache/poi/hdf/extractor/StyleSheet;->getFlag(I)Z

    move-result v2

    move-object/from16 v0, p1

    iput-boolean v2, v0, Lorg/apache/poi/hdf/extractor/CHP;->_fRMark:Z

    goto :goto_0

    .line 150
    :pswitch_3
    move/from16 v0, p3

    move-object/from16 v1, p1

    iput v0, v1, Lorg/apache/poi/hdf/extractor/CHP;->_fcPic:I

    .line 151
    const/4 v2, 0x1

    move-object/from16 v0, p1

    iput-boolean v2, v0, Lorg/apache/poi/hdf/extractor/CHP;->_fSpec:Z

    goto :goto_0

    .line 154
    :pswitch_4
    move/from16 v0, p3

    int-to-short v2, v0

    move-object/from16 v0, p1

    iput-short v2, v0, Lorg/apache/poi/hdf/extractor/CHP;->_ibstRMark:S

    goto :goto_0

    .line 157
    :pswitch_5
    move-object/from16 v0, p1

    iget-object v2, v0, Lorg/apache/poi/hdf/extractor/CHP;->_dttmRMark:[I

    const/4 v3, 0x0

    add-int/lit8 v4, p6, -0x4

    move-object/from16 v0, p5

    invoke-static {v0, v4}, Lorg/apache/poi/hdf/extractor/Utils;->convertBytesToShort([BI)S

    move-result v4

    aput v4, v2, v3

    .line 158
    move-object/from16 v0, p1

    iget-object v2, v0, Lorg/apache/poi/hdf/extractor/CHP;->_dttmRMark:[I

    const/4 v3, 0x1

    add-int/lit8 v4, p6, -0x2

    move-object/from16 v0, p5

    invoke-static {v0, v4}, Lorg/apache/poi/hdf/extractor/Utils;->convertBytesToShort([BI)S

    move-result v4

    aput v4, v2, v3

    goto :goto_0

    .line 161
    :pswitch_6
    invoke-static/range {p3 .. p3}, Lorg/apache/poi/hdf/extractor/StyleSheet;->getFlag(I)Z

    move-result v2

    move-object/from16 v0, p1

    iput-boolean v2, v0, Lorg/apache/poi/hdf/extractor/CHP;->_fData:Z

    goto :goto_0

    .line 167
    :pswitch_7
    const/high16 v2, 0xff0000

    and-int v2, v2, p3

    ushr-int/lit8 v2, v2, 0x8

    int-to-short v12, v2

    .line 168
    .local v12, "chsDiff":S
    invoke-static {v12}, Lorg/apache/poi/hdf/extractor/StyleSheet;->getFlag(I)Z

    move-result v2

    move-object/from16 v0, p1

    iput-boolean v2, v0, Lorg/apache/poi/hdf/extractor/CHP;->_fChsDiff:Z

    .line 169
    const v2, 0xffff

    and-int v2, v2, p3

    int-to-short v2, v2

    move-object/from16 v0, p1

    iput-short v2, v0, Lorg/apache/poi/hdf/extractor/CHP;->_chse:S

    goto :goto_0

    .line 172
    .end local v12    # "chsDiff":S
    :pswitch_8
    const/4 v2, 0x1

    move-object/from16 v0, p1

    iput-boolean v2, v0, Lorg/apache/poi/hdf/extractor/CHP;->_fSpec:Z

    .line 174
    if-eqz p4, :cond_0

    .line 175
    const/4 v2, 0x0

    move-object/from16 v0, p4

    invoke-static {v0, v2}, Lorg/apache/poi/hdf/extractor/Utils;->convertBytesToShort([BI)S

    move-result v2

    move-object/from16 v0, p1

    iput-short v2, v0, Lorg/apache/poi/hdf/extractor/CHP;->_ftcSym:S

    .line 176
    const/4 v2, 0x2

    move-object/from16 v0, p4

    invoke-static {v0, v2}, Lorg/apache/poi/hdf/extractor/Utils;->convertBytesToShort([BI)S

    move-result v2

    move-object/from16 v0, p1

    iput-short v2, v0, Lorg/apache/poi/hdf/extractor/CHP;->_xchSym:S

    goto/16 :goto_0

    .line 180
    :pswitch_9
    invoke-static/range {p3 .. p3}, Lorg/apache/poi/hdf/extractor/StyleSheet;->getFlag(I)Z

    move-result v2

    move-object/from16 v0, p1

    iput-boolean v2, v0, Lorg/apache/poi/hdf/extractor/CHP;->_fOle2:Z

    goto/16 :goto_0

    .line 186
    :pswitch_a
    move/from16 v0, p3

    int-to-byte v2, v0

    move-object/from16 v0, p1

    iput-byte v2, v0, Lorg/apache/poi/hdf/extractor/CHP;->_icoHighlight:B

    .line 187
    invoke-static/range {p3 .. p3}, Lorg/apache/poi/hdf/extractor/StyleSheet;->getFlag(I)Z

    move-result v2

    move-object/from16 v0, p1

    iput-boolean v2, v0, Lorg/apache/poi/hdf/extractor/CHP;->_highlighted:Z

    goto/16 :goto_0

    .line 192
    :pswitch_b
    move/from16 v0, p3

    move-object/from16 v1, p1

    iput v0, v1, Lorg/apache/poi/hdf/extractor/CHP;->_fcObj:I

    goto/16 :goto_0

    .line 262
    :pswitch_c
    move/from16 v0, p3

    move-object/from16 v1, p1

    iput v0, v1, Lorg/apache/poi/hdf/extractor/CHP;->_istd:I

    goto/16 :goto_0

    .line 268
    :pswitch_d
    const/4 v2, 0x0

    move-object/from16 v0, p1

    iput-boolean v2, v0, Lorg/apache/poi/hdf/extractor/CHP;->_bold:Z

    .line 269
    const/4 v2, 0x0

    move-object/from16 v0, p1

    iput-boolean v2, v0, Lorg/apache/poi/hdf/extractor/CHP;->_italic:Z

    .line 270
    const/4 v2, 0x0

    move-object/from16 v0, p1

    iput-boolean v2, v0, Lorg/apache/poi/hdf/extractor/CHP;->_fOutline:Z

    .line 271
    const/4 v2, 0x0

    move-object/from16 v0, p1

    iput-boolean v2, v0, Lorg/apache/poi/hdf/extractor/CHP;->_fStrike:Z

    .line 272
    const/4 v2, 0x0

    move-object/from16 v0, p1

    iput-boolean v2, v0, Lorg/apache/poi/hdf/extractor/CHP;->_fShadow:Z

    .line 273
    const/4 v2, 0x0

    move-object/from16 v0, p1

    iput-boolean v2, v0, Lorg/apache/poi/hdf/extractor/CHP;->_fSmallCaps:Z

    .line 274
    const/4 v2, 0x0

    move-object/from16 v0, p1

    iput-boolean v2, v0, Lorg/apache/poi/hdf/extractor/CHP;->_fCaps:Z

    .line 275
    const/4 v2, 0x0

    move-object/from16 v0, p1

    iput-boolean v2, v0, Lorg/apache/poi/hdf/extractor/CHP;->_fVanish:Z

    .line 276
    const/4 v2, 0x0

    move-object/from16 v0, p1

    iput-byte v2, v0, Lorg/apache/poi/hdf/extractor/CHP;->_kul:B

    .line 277
    const/4 v2, 0x0

    move-object/from16 v0, p1

    iput-byte v2, v0, Lorg/apache/poi/hdf/extractor/CHP;->_ico:B

    goto/16 :goto_0

    .line 280
    :pswitch_e
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Lorg/apache/poi/hdf/extractor/CHP;->copy(Lorg/apache/poi/hdf/extractor/CHP;)V

    goto/16 :goto_0

    .line 285
    :pswitch_f
    move/from16 v0, p3

    int-to-byte v2, v0

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lorg/apache/poi/hdf/extractor/CHP;->_bold:Z

    invoke-static {v2, v3}, Lorg/apache/poi/hdf/extractor/StyleSheet;->getCHPFlag(BZ)Z

    move-result v2

    move-object/from16 v0, p1

    iput-boolean v2, v0, Lorg/apache/poi/hdf/extractor/CHP;->_bold:Z

    goto/16 :goto_0

    .line 288
    :pswitch_10
    move/from16 v0, p3

    int-to-byte v2, v0

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lorg/apache/poi/hdf/extractor/CHP;->_italic:Z

    invoke-static {v2, v3}, Lorg/apache/poi/hdf/extractor/StyleSheet;->getCHPFlag(BZ)Z

    move-result v2

    move-object/from16 v0, p1

    iput-boolean v2, v0, Lorg/apache/poi/hdf/extractor/CHP;->_italic:Z

    goto/16 :goto_0

    .line 291
    :pswitch_11
    move/from16 v0, p3

    int-to-byte v2, v0

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lorg/apache/poi/hdf/extractor/CHP;->_fStrike:Z

    invoke-static {v2, v3}, Lorg/apache/poi/hdf/extractor/StyleSheet;->getCHPFlag(BZ)Z

    move-result v2

    move-object/from16 v0, p1

    iput-boolean v2, v0, Lorg/apache/poi/hdf/extractor/CHP;->_fStrike:Z

    goto/16 :goto_0

    .line 294
    :pswitch_12
    move/from16 v0, p3

    int-to-byte v2, v0

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lorg/apache/poi/hdf/extractor/CHP;->_fOutline:Z

    invoke-static {v2, v3}, Lorg/apache/poi/hdf/extractor/StyleSheet;->getCHPFlag(BZ)Z

    move-result v2

    move-object/from16 v0, p1

    iput-boolean v2, v0, Lorg/apache/poi/hdf/extractor/CHP;->_fOutline:Z

    goto/16 :goto_0

    .line 297
    :pswitch_13
    move/from16 v0, p3

    int-to-byte v2, v0

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lorg/apache/poi/hdf/extractor/CHP;->_fShadow:Z

    invoke-static {v2, v3}, Lorg/apache/poi/hdf/extractor/StyleSheet;->getCHPFlag(BZ)Z

    move-result v2

    move-object/from16 v0, p1

    iput-boolean v2, v0, Lorg/apache/poi/hdf/extractor/CHP;->_fShadow:Z

    goto/16 :goto_0

    .line 300
    :pswitch_14
    move/from16 v0, p3

    int-to-byte v2, v0

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lorg/apache/poi/hdf/extractor/CHP;->_fSmallCaps:Z

    invoke-static {v2, v3}, Lorg/apache/poi/hdf/extractor/StyleSheet;->getCHPFlag(BZ)Z

    move-result v2

    move-object/from16 v0, p1

    iput-boolean v2, v0, Lorg/apache/poi/hdf/extractor/CHP;->_fSmallCaps:Z

    goto/16 :goto_0

    .line 303
    :pswitch_15
    move/from16 v0, p3

    int-to-byte v2, v0

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lorg/apache/poi/hdf/extractor/CHP;->_fCaps:Z

    invoke-static {v2, v3}, Lorg/apache/poi/hdf/extractor/StyleSheet;->getCHPFlag(BZ)Z

    move-result v2

    move-object/from16 v0, p1

    iput-boolean v2, v0, Lorg/apache/poi/hdf/extractor/CHP;->_fCaps:Z

    goto/16 :goto_0

    .line 306
    :pswitch_16
    move/from16 v0, p3

    int-to-byte v2, v0

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lorg/apache/poi/hdf/extractor/CHP;->_fVanish:Z

    invoke-static {v2, v3}, Lorg/apache/poi/hdf/extractor/StyleSheet;->getCHPFlag(BZ)Z

    move-result v2

    move-object/from16 v0, p1

    iput-boolean v2, v0, Lorg/apache/poi/hdf/extractor/CHP;->_fVanish:Z

    goto/16 :goto_0

    .line 309
    :pswitch_17
    move/from16 v0, p3

    int-to-short v2, v0

    move-object/from16 v0, p1

    iput-short v2, v0, Lorg/apache/poi/hdf/extractor/CHP;->_ftc:S

    goto/16 :goto_0

    .line 312
    :pswitch_18
    move/from16 v0, p3

    int-to-byte v2, v0

    move-object/from16 v0, p1

    iput-byte v2, v0, Lorg/apache/poi/hdf/extractor/CHP;->_kul:B

    goto/16 :goto_0

    .line 315
    :pswitch_19
    move/from16 v0, p3

    and-int/lit16 v15, v0, 0xff

    .line 316
    .local v15, "hps":I
    if-eqz v15, :cond_1

    .line 318
    move-object/from16 v0, p1

    iput v15, v0, Lorg/apache/poi/hdf/extractor/CHP;->_hps:I

    .line 320
    :cond_1
    const v2, 0xfe00

    and-int v2, v2, p3

    int-to-byte v2, v2

    ushr-int/lit8 v2, v2, 0x4

    shr-int/lit8 v2, v2, 0x1

    int-to-byte v11, v2

    .line 321
    .local v11, "cInc":B
    if-eqz v11, :cond_2

    .line 323
    move-object/from16 v0, p1

    iget v2, v0, Lorg/apache/poi/hdf/extractor/CHP;->_hps:I

    mul-int/lit8 v3, v11, 0x2

    add-int/2addr v2, v3

    const/4 v3, 0x2

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    move-object/from16 v0, p1

    iput v2, v0, Lorg/apache/poi/hdf/extractor/CHP;->_hps:I

    .line 325
    :cond_2
    const/high16 v2, 0xff0000

    and-int v2, v2, p3

    ushr-int/lit8 v2, v2, 0x8

    int-to-byte v0, v2

    move/from16 v17, v0

    .line 326
    .local v17, "hpsPos":B
    const/16 v2, 0x80

    move/from16 v0, v17

    if-eq v0, v2, :cond_3

    .line 328
    move/from16 v0, v17

    move-object/from16 v1, p1

    iput-short v0, v1, Lorg/apache/poi/hdf/extractor/CHP;->_hpsPos:S

    .line 330
    :cond_3
    move/from16 v0, p3

    and-int/lit16 v2, v0, 0x100

    if-lez v2, :cond_5

    const/4 v13, 0x1

    .line 331
    .local v13, "fAdjust":Z
    :goto_1
    if-eqz v13, :cond_4

    const/16 v2, 0x80

    move/from16 v0, v17

    if-eq v0, v2, :cond_4

    if-eqz v17, :cond_4

    move-object/from16 v0, p0

    iget-short v2, v0, Lorg/apache/poi/hdf/extractor/CHP;->_hpsPos:S

    if-nez v2, :cond_4

    .line 333
    move-object/from16 v0, p1

    iget v2, v0, Lorg/apache/poi/hdf/extractor/CHP;->_hps:I

    add-int/lit8 v2, v2, -0x2

    const/4 v3, 0x2

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    move-object/from16 v0, p1

    iput v2, v0, Lorg/apache/poi/hdf/extractor/CHP;->_hps:I

    .line 335
    :cond_4
    if-eqz v13, :cond_0

    if-nez v17, :cond_0

    move-object/from16 v0, p0

    iget-short v2, v0, Lorg/apache/poi/hdf/extractor/CHP;->_hpsPos:S

    if-eqz v2, :cond_0

    .line 337
    move-object/from16 v0, p1

    iget v2, v0, Lorg/apache/poi/hdf/extractor/CHP;->_hps:I

    add-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    move-object/from16 v0, p1

    iput v2, v0, Lorg/apache/poi/hdf/extractor/CHP;->_hps:I

    goto/16 :goto_0

    .line 330
    .end local v13    # "fAdjust":Z
    :cond_5
    const/4 v13, 0x0

    goto :goto_1

    .line 341
    .end local v11    # "cInc":B
    .end local v15    # "hps":I
    .end local v17    # "hpsPos":B
    :pswitch_1a
    move/from16 v0, p3

    move-object/from16 v1, p1

    iput v0, v1, Lorg/apache/poi/hdf/extractor/CHP;->_dxaSpace:I

    goto/16 :goto_0

    .line 344
    :pswitch_1b
    move/from16 v0, p3

    int-to-short v2, v0

    move-object/from16 v0, p1

    iput-short v2, v0, Lorg/apache/poi/hdf/extractor/CHP;->_lidDefault:S

    goto/16 :goto_0

    .line 347
    :pswitch_1c
    move/from16 v0, p3

    int-to-byte v2, v0

    move-object/from16 v0, p1

    iput-byte v2, v0, Lorg/apache/poi/hdf/extractor/CHP;->_ico:B

    goto/16 :goto_0

    .line 350
    :pswitch_1d
    move/from16 v0, p3

    move-object/from16 v1, p1

    iput v0, v1, Lorg/apache/poi/hdf/extractor/CHP;->_hps:I

    goto/16 :goto_0

    .line 353
    :pswitch_1e
    move/from16 v0, p3

    int-to-byte v0, v0

    move/from16 v16, v0

    .line 354
    .local v16, "hpsLvl":B
    move-object/from16 v0, p1

    iget v2, v0, Lorg/apache/poi/hdf/extractor/CHP;->_hps:I

    mul-int/lit8 v3, v16, 0x2

    add-int/2addr v2, v3

    const/4 v3, 0x2

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    move-object/from16 v0, p1

    iput v2, v0, Lorg/apache/poi/hdf/extractor/CHP;->_hps:I

    goto/16 :goto_0

    .line 357
    .end local v16    # "hpsLvl":B
    :pswitch_1f
    move/from16 v0, p3

    int-to-short v2, v0

    move-object/from16 v0, p1

    iput-short v2, v0, Lorg/apache/poi/hdf/extractor/CHP;->_hpsPos:S

    goto/16 :goto_0

    .line 360
    :pswitch_20
    if-eqz p3, :cond_6

    .line 362
    move-object/from16 v0, p0

    iget-short v2, v0, Lorg/apache/poi/hdf/extractor/CHP;->_hpsPos:S

    if-nez v2, :cond_0

    .line 364
    move-object/from16 v0, p1

    iget v2, v0, Lorg/apache/poi/hdf/extractor/CHP;->_hps:I

    add-int/lit8 v2, v2, -0x2

    const/4 v3, 0x2

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    move-object/from16 v0, p1

    iput v2, v0, Lorg/apache/poi/hdf/extractor/CHP;->_hps:I

    goto/16 :goto_0

    .line 369
    :cond_6
    move-object/from16 v0, p0

    iget-short v2, v0, Lorg/apache/poi/hdf/extractor/CHP;->_hpsPos:S

    if-eqz v2, :cond_0

    .line 371
    move-object/from16 v0, p1

    iget v2, v0, Lorg/apache/poi/hdf/extractor/CHP;->_hps:I

    add-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    move-object/from16 v0, p1

    iput v2, v0, Lorg/apache/poi/hdf/extractor/CHP;->_hps:I

    goto/16 :goto_0

    .line 376
    :pswitch_21
    new-instance v14, Lorg/apache/poi/hdf/extractor/CHP;

    invoke-direct {v14}, Lorg/apache/poi/hdf/extractor/CHP;-><init>()V

    .line 377
    .local v14, "genCHP":Lorg/apache/poi/hdf/extractor/CHP;
    const/4 v2, 0x4

    iput-short v2, v14, Lorg/apache/poi/hdf/extractor/CHP;->_ftc:S

    .line 379
    if-eqz p4, :cond_7

    .line 380
    move-object/from16 v0, p4

    move-object/from16 v1, p7

    invoke-static {v0, v14, v1}, Lorg/apache/poi/hdf/extractor/StyleSheet;->uncompressProperty([BLjava/lang/Object;Lorg/apache/poi/hdf/extractor/StyleSheet;)Ljava/lang/Object;

    move-result-object v14

    .end local v14    # "genCHP":Lorg/apache/poi/hdf/extractor/CHP;
    check-cast v14, Lorg/apache/poi/hdf/extractor/CHP;

    .line 382
    .restart local v14    # "genCHP":Lorg/apache/poi/hdf/extractor/CHP;
    :cond_7
    move-object/from16 v0, p0

    iget v2, v0, Lorg/apache/poi/hdf/extractor/CHP;->_baseIstd:I

    move-object/from16 v0, p7

    invoke-virtual {v0, v2}, Lorg/apache/poi/hdf/extractor/StyleSheet;->getStyleDescription(I)Lorg/apache/poi/hdf/extractor/StyleDescription;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/poi/hdf/extractor/StyleDescription;->getCHP()Lorg/apache/poi/hdf/extractor/CHP;

    move-result-object v20

    .line 383
    .local v20, "styleCHP":Lorg/apache/poi/hdf/extractor/CHP;
    iget-boolean v2, v14, Lorg/apache/poi/hdf/extractor/CHP;->_bold:Z

    move-object/from16 v0, p1

    iget-boolean v3, v0, Lorg/apache/poi/hdf/extractor/CHP;->_bold:Z

    if-ne v2, v3, :cond_8

    .line 385
    move-object/from16 v0, v20

    iget-boolean v2, v0, Lorg/apache/poi/hdf/extractor/CHP;->_bold:Z

    move-object/from16 v0, p1

    iput-boolean v2, v0, Lorg/apache/poi/hdf/extractor/CHP;->_bold:Z

    .line 387
    :cond_8
    iget-boolean v2, v14, Lorg/apache/poi/hdf/extractor/CHP;->_italic:Z

    move-object/from16 v0, p1

    iget-boolean v3, v0, Lorg/apache/poi/hdf/extractor/CHP;->_italic:Z

    if-ne v2, v3, :cond_9

    .line 389
    move-object/from16 v0, v20

    iget-boolean v2, v0, Lorg/apache/poi/hdf/extractor/CHP;->_italic:Z

    move-object/from16 v0, p1

    iput-boolean v2, v0, Lorg/apache/poi/hdf/extractor/CHP;->_italic:Z

    .line 391
    :cond_9
    iget-boolean v2, v14, Lorg/apache/poi/hdf/extractor/CHP;->_fSmallCaps:Z

    move-object/from16 v0, p1

    iget-boolean v3, v0, Lorg/apache/poi/hdf/extractor/CHP;->_fSmallCaps:Z

    if-ne v2, v3, :cond_a

    .line 393
    move-object/from16 v0, v20

    iget-boolean v2, v0, Lorg/apache/poi/hdf/extractor/CHP;->_fSmallCaps:Z

    move-object/from16 v0, p1

    iput-boolean v2, v0, Lorg/apache/poi/hdf/extractor/CHP;->_fSmallCaps:Z

    .line 395
    :cond_a
    iget-boolean v2, v14, Lorg/apache/poi/hdf/extractor/CHP;->_fVanish:Z

    move-object/from16 v0, p1

    iget-boolean v3, v0, Lorg/apache/poi/hdf/extractor/CHP;->_fVanish:Z

    if-ne v2, v3, :cond_b

    .line 397
    move-object/from16 v0, v20

    iget-boolean v2, v0, Lorg/apache/poi/hdf/extractor/CHP;->_fVanish:Z

    move-object/from16 v0, p1

    iput-boolean v2, v0, Lorg/apache/poi/hdf/extractor/CHP;->_fVanish:Z

    .line 399
    :cond_b
    iget-boolean v2, v14, Lorg/apache/poi/hdf/extractor/CHP;->_fStrike:Z

    move-object/from16 v0, p1

    iget-boolean v3, v0, Lorg/apache/poi/hdf/extractor/CHP;->_fStrike:Z

    if-ne v2, v3, :cond_c

    .line 401
    move-object/from16 v0, v20

    iget-boolean v2, v0, Lorg/apache/poi/hdf/extractor/CHP;->_fStrike:Z

    move-object/from16 v0, p1

    iput-boolean v2, v0, Lorg/apache/poi/hdf/extractor/CHP;->_fStrike:Z

    .line 403
    :cond_c
    iget-boolean v2, v14, Lorg/apache/poi/hdf/extractor/CHP;->_fCaps:Z

    move-object/from16 v0, p1

    iget-boolean v3, v0, Lorg/apache/poi/hdf/extractor/CHP;->_fCaps:Z

    if-ne v2, v3, :cond_d

    .line 405
    move-object/from16 v0, v20

    iget-boolean v2, v0, Lorg/apache/poi/hdf/extractor/CHP;->_fCaps:Z

    move-object/from16 v0, p1

    iput-boolean v2, v0, Lorg/apache/poi/hdf/extractor/CHP;->_fCaps:Z

    .line 407
    :cond_d
    iget-short v2, v14, Lorg/apache/poi/hdf/extractor/CHP;->_ftcAscii:S

    move-object/from16 v0, p1

    iget-short v3, v0, Lorg/apache/poi/hdf/extractor/CHP;->_ftcAscii:S

    if-ne v2, v3, :cond_e

    .line 409
    move-object/from16 v0, v20

    iget-short v2, v0, Lorg/apache/poi/hdf/extractor/CHP;->_ftcAscii:S

    move-object/from16 v0, p1

    iput-short v2, v0, Lorg/apache/poi/hdf/extractor/CHP;->_ftcAscii:S

    .line 411
    :cond_e
    iget-short v2, v14, Lorg/apache/poi/hdf/extractor/CHP;->_ftcFE:S

    move-object/from16 v0, p1

    iget-short v3, v0, Lorg/apache/poi/hdf/extractor/CHP;->_ftcFE:S

    if-ne v2, v3, :cond_f

    .line 413
    move-object/from16 v0, v20

    iget-short v2, v0, Lorg/apache/poi/hdf/extractor/CHP;->_ftcFE:S

    move-object/from16 v0, p1

    iput-short v2, v0, Lorg/apache/poi/hdf/extractor/CHP;->_ftcFE:S

    .line 415
    :cond_f
    iget-short v2, v14, Lorg/apache/poi/hdf/extractor/CHP;->_ftcOther:S

    move-object/from16 v0, p1

    iget-short v3, v0, Lorg/apache/poi/hdf/extractor/CHP;->_ftcOther:S

    if-ne v2, v3, :cond_10

    .line 417
    move-object/from16 v0, v20

    iget-short v2, v0, Lorg/apache/poi/hdf/extractor/CHP;->_ftcOther:S

    move-object/from16 v0, p1

    iput-short v2, v0, Lorg/apache/poi/hdf/extractor/CHP;->_ftcOther:S

    .line 419
    :cond_10
    iget v2, v14, Lorg/apache/poi/hdf/extractor/CHP;->_hps:I

    move-object/from16 v0, p1

    iget v3, v0, Lorg/apache/poi/hdf/extractor/CHP;->_hps:I

    if-ne v2, v3, :cond_11

    .line 421
    move-object/from16 v0, v20

    iget v2, v0, Lorg/apache/poi/hdf/extractor/CHP;->_hps:I

    move-object/from16 v0, p1

    iput v2, v0, Lorg/apache/poi/hdf/extractor/CHP;->_hps:I

    .line 423
    :cond_11
    iget-short v2, v14, Lorg/apache/poi/hdf/extractor/CHP;->_hpsPos:S

    move-object/from16 v0, p1

    iget-short v3, v0, Lorg/apache/poi/hdf/extractor/CHP;->_hpsPos:S

    if-ne v2, v3, :cond_12

    .line 425
    move-object/from16 v0, v20

    iget-short v2, v0, Lorg/apache/poi/hdf/extractor/CHP;->_hpsPos:S

    move-object/from16 v0, p1

    iput-short v2, v0, Lorg/apache/poi/hdf/extractor/CHP;->_hpsPos:S

    .line 427
    :cond_12
    iget-byte v2, v14, Lorg/apache/poi/hdf/extractor/CHP;->_kul:B

    move-object/from16 v0, p1

    iget-byte v3, v0, Lorg/apache/poi/hdf/extractor/CHP;->_kul:B

    if-ne v2, v3, :cond_13

    .line 429
    move-object/from16 v0, v20

    iget-byte v2, v0, Lorg/apache/poi/hdf/extractor/CHP;->_kul:B

    move-object/from16 v0, p1

    iput-byte v2, v0, Lorg/apache/poi/hdf/extractor/CHP;->_kul:B

    .line 431
    :cond_13
    iget v2, v14, Lorg/apache/poi/hdf/extractor/CHP;->_dxaSpace:I

    move-object/from16 v0, p1

    iget v3, v0, Lorg/apache/poi/hdf/extractor/CHP;->_dxaSpace:I

    if-ne v2, v3, :cond_14

    .line 433
    move-object/from16 v0, v20

    iget v2, v0, Lorg/apache/poi/hdf/extractor/CHP;->_dxaSpace:I

    move-object/from16 v0, p1

    iput v2, v0, Lorg/apache/poi/hdf/extractor/CHP;->_dxaSpace:I

    .line 435
    :cond_14
    iget-byte v2, v14, Lorg/apache/poi/hdf/extractor/CHP;->_ico:B

    move-object/from16 v0, p1

    iget-byte v3, v0, Lorg/apache/poi/hdf/extractor/CHP;->_ico:B

    if-ne v2, v3, :cond_15

    .line 437
    move-object/from16 v0, v20

    iget-byte v2, v0, Lorg/apache/poi/hdf/extractor/CHP;->_ico:B

    move-object/from16 v0, p1

    iput-byte v2, v0, Lorg/apache/poi/hdf/extractor/CHP;->_ico:B

    .line 439
    :cond_15
    iget-short v2, v14, Lorg/apache/poi/hdf/extractor/CHP;->_lidDefault:S

    move-object/from16 v0, p1

    iget-short v3, v0, Lorg/apache/poi/hdf/extractor/CHP;->_lidDefault:S

    if-ne v2, v3, :cond_16

    .line 441
    move-object/from16 v0, v20

    iget-short v2, v0, Lorg/apache/poi/hdf/extractor/CHP;->_lidDefault:S

    move-object/from16 v0, p1

    iput-short v2, v0, Lorg/apache/poi/hdf/extractor/CHP;->_lidDefault:S

    .line 443
    :cond_16
    iget-short v2, v14, Lorg/apache/poi/hdf/extractor/CHP;->_lidFE:S

    move-object/from16 v0, p1

    iget-short v3, v0, Lorg/apache/poi/hdf/extractor/CHP;->_lidFE:S

    if-ne v2, v3, :cond_0

    .line 445
    move-object/from16 v0, v20

    iget-short v2, v0, Lorg/apache/poi/hdf/extractor/CHP;->_lidFE:S

    move-object/from16 v0, p1

    iput-short v2, v0, Lorg/apache/poi/hdf/extractor/CHP;->_lidFE:S

    goto/16 :goto_0

    .line 449
    .end local v14    # "genCHP":Lorg/apache/poi/hdf/extractor/CHP;
    .end local v20    # "styleCHP":Lorg/apache/poi/hdf/extractor/CHP;
    :pswitch_22
    move/from16 v0, p3

    int-to-byte v2, v0

    move-object/from16 v0, p1

    iput-byte v2, v0, Lorg/apache/poi/hdf/extractor/CHP;->_iss:B

    goto/16 :goto_0

    .line 453
    :pswitch_23
    if-eqz p4, :cond_0

    .line 454
    const/4 v2, 0x0

    move-object/from16 v0, p4

    invoke-static {v0, v2}, Lorg/apache/poi/hdf/extractor/Utils;->convertBytesToShort([BI)S

    move-result v2

    move-object/from16 v0, p1

    iput v2, v0, Lorg/apache/poi/hdf/extractor/CHP;->_hps:I

    goto/16 :goto_0

    .line 459
    :pswitch_24
    if-eqz p4, :cond_0

    .line 460
    const/4 v2, 0x0

    move-object/from16 v0, p4

    invoke-static {v0, v2}, Lorg/apache/poi/hdf/extractor/Utils;->convertBytesToShort([BI)S

    move-result v18

    .line 461
    .local v18, "increment":I
    move-object/from16 v0, p1

    iget v2, v0, Lorg/apache/poi/hdf/extractor/CHP;->_hps:I

    add-int v2, v2, v18

    const/16 v3, 0x8

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    move-object/from16 v0, p1

    iput v2, v0, Lorg/apache/poi/hdf/extractor/CHP;->_hps:I

    goto/16 :goto_0

    .line 465
    .end local v18    # "increment":I
    :pswitch_25
    move/from16 v0, p3

    move-object/from16 v1, p1

    iput v0, v1, Lorg/apache/poi/hdf/extractor/CHP;->_hpsKern:I

    goto/16 :goto_0

    .line 468
    :pswitch_26
    const/16 v4, 0x47

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move/from16 v5, p3

    move-object/from16 v6, p4

    move-object/from16 v7, p5

    move/from16 v8, p6

    move-object/from16 v9, p7

    invoke-static/range {v2 .. v9}, Lorg/apache/poi/hdf/extractor/StyleSheet;->doCHPOperation(Lorg/apache/poi/hdf/extractor/CHP;Lorg/apache/poi/hdf/extractor/CHP;II[B[BILorg/apache/poi/hdf/extractor/StyleSheet;)V

    goto/16 :goto_0

    .line 471
    :pswitch_27
    move/from16 v0, p3

    int-to-float v2, v0

    const/high16 v3, 0x42c80000    # 100.0f

    div-float v19, v2, v3

    .line 472
    .local v19, "percentage":F
    move-object/from16 v0, p1

    iget v2, v0, Lorg/apache/poi/hdf/extractor/CHP;->_hps:I

    int-to-float v2, v2

    mul-float v2, v2, v19

    float-to-int v10, v2

    .line 473
    .local v10, "add":I
    move-object/from16 v0, p1

    iget v2, v0, Lorg/apache/poi/hdf/extractor/CHP;->_hps:I

    add-int/2addr v2, v10

    move-object/from16 v0, p1

    iput v2, v0, Lorg/apache/poi/hdf/extractor/CHP;->_hps:I

    goto/16 :goto_0

    .line 476
    .end local v10    # "add":I
    .end local v19    # "percentage":F
    :pswitch_28
    move/from16 v0, p3

    int-to-byte v2, v0

    move-object/from16 v0, p1

    iput-byte v2, v0, Lorg/apache/poi/hdf/extractor/CHP;->_ysr:B

    goto/16 :goto_0

    .line 479
    :pswitch_29
    move/from16 v0, p3

    int-to-short v2, v0

    move-object/from16 v0, p1

    iput-short v2, v0, Lorg/apache/poi/hdf/extractor/CHP;->_ftcAscii:S

    goto/16 :goto_0

    .line 482
    :pswitch_2a
    move/from16 v0, p3

    int-to-short v2, v0

    move-object/from16 v0, p1

    iput-short v2, v0, Lorg/apache/poi/hdf/extractor/CHP;->_ftcFE:S

    goto/16 :goto_0

    .line 485
    :pswitch_2b
    move/from16 v0, p3

    int-to-short v2, v0

    move-object/from16 v0, p1

    iput-short v2, v0, Lorg/apache/poi/hdf/extractor/CHP;->_ftcOther:S

    goto/16 :goto_0

    .line 490
    :pswitch_2c
    invoke-static/range {p3 .. p3}, Lorg/apache/poi/hdf/extractor/StyleSheet;->getFlag(I)Z

    move-result v2

    move-object/from16 v0, p1

    iput-boolean v2, v0, Lorg/apache/poi/hdf/extractor/CHP;->_fDStrike:Z

    goto/16 :goto_0

    .line 493
    :pswitch_2d
    invoke-static/range {p3 .. p3}, Lorg/apache/poi/hdf/extractor/StyleSheet;->getFlag(I)Z

    move-result v2

    move-object/from16 v0, p1

    iput-boolean v2, v0, Lorg/apache/poi/hdf/extractor/CHP;->_fImprint:Z

    goto/16 :goto_0

    .line 496
    :pswitch_2e
    invoke-static/range {p3 .. p3}, Lorg/apache/poi/hdf/extractor/StyleSheet;->getFlag(I)Z

    move-result v2

    move-object/from16 v0, p1

    iput-boolean v2, v0, Lorg/apache/poi/hdf/extractor/CHP;->_fSpec:Z

    goto/16 :goto_0

    .line 499
    :pswitch_2f
    invoke-static/range {p3 .. p3}, Lorg/apache/poi/hdf/extractor/StyleSheet;->getFlag(I)Z

    move-result v2

    move-object/from16 v0, p1

    iput-boolean v2, v0, Lorg/apache/poi/hdf/extractor/CHP;->_fObj:Z

    goto/16 :goto_0

    .line 503
    :pswitch_30
    if-eqz p4, :cond_0

    .line 504
    const/4 v2, 0x0

    aget-byte v2, p4, v2

    invoke-static {v2}, Lorg/apache/poi/hdf/extractor/StyleSheet;->getFlag(I)Z

    move-result v2

    move-object/from16 v0, p1

    iput-boolean v2, v0, Lorg/apache/poi/hdf/extractor/CHP;->_fPropMark:Z

    .line 505
    const/4 v2, 0x1

    move-object/from16 v0, p4

    invoke-static {v0, v2}, Lorg/apache/poi/hdf/extractor/Utils;->convertBytesToShort([BI)S

    move-result v2

    move-object/from16 v0, p1

    iput-short v2, v0, Lorg/apache/poi/hdf/extractor/CHP;->_ibstPropRMark:S

    .line 506
    const/4 v2, 0x3

    move-object/from16 v0, p4

    invoke-static {v0, v2}, Lorg/apache/poi/hdf/extractor/Utils;->convertBytesToInt([BI)I

    move-result v2

    move-object/from16 v0, p1

    iput v2, v0, Lorg/apache/poi/hdf/extractor/CHP;->_dttmPropRMark:I

    goto/16 :goto_0

    .line 510
    :pswitch_31
    invoke-static/range {p3 .. p3}, Lorg/apache/poi/hdf/extractor/StyleSheet;->getFlag(I)Z

    move-result v2

    move-object/from16 v0, p1

    iput-boolean v2, v0, Lorg/apache/poi/hdf/extractor/CHP;->_fEmboss:Z

    goto/16 :goto_0

    .line 513
    :pswitch_32
    move/from16 v0, p3

    int-to-byte v2, v0

    move-object/from16 v0, p1

    iput-byte v2, v0, Lorg/apache/poi/hdf/extractor/CHP;->_sfxtText:B

    goto/16 :goto_0

    .line 533
    :pswitch_33
    if-eqz p4, :cond_0

    .line 534
    const/4 v2, 0x0

    aget-byte v2, p4, v2

    invoke-static {v2}, Lorg/apache/poi/hdf/extractor/StyleSheet;->getFlag(I)Z

    move-result v2

    move-object/from16 v0, p1

    iput-boolean v2, v0, Lorg/apache/poi/hdf/extractor/CHP;->_fDispFldRMark:Z

    .line 536
    const/4 v2, 0x1

    .line 535
    move-object/from16 v0, p4

    invoke-static {v0, v2}, Lorg/apache/poi/hdf/extractor/Utils;->convertBytesToShort([BI)S

    move-result v2

    move-object/from16 v0, p1

    iput-short v2, v0, Lorg/apache/poi/hdf/extractor/CHP;->_ibstDispFldRMark:S

    .line 537
    const/4 v2, 0x3

    move-object/from16 v0, p4

    invoke-static {v0, v2}, Lorg/apache/poi/hdf/extractor/Utils;->convertBytesToInt([BI)I

    move-result v2

    move-object/from16 v0, p1

    iput v2, v0, Lorg/apache/poi/hdf/extractor/CHP;->_dttmDispFldRMark:I

    .line 538
    const/4 v2, 0x7

    move-object/from16 v0, p1

    iget-object v3, v0, Lorg/apache/poi/hdf/extractor/CHP;->_xstDispFldRMark:[B

    const/4 v4, 0x0

    const/16 v5, 0x20

    move-object/from16 v0, p4

    invoke-static {v0, v2, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto/16 :goto_0

    .line 542
    :pswitch_34
    move/from16 v0, p3

    int-to-short v2, v0

    move-object/from16 v0, p1

    iput-short v2, v0, Lorg/apache/poi/hdf/extractor/CHP;->_ibstRMarkDel:S

    goto/16 :goto_0

    .line 545
    :pswitch_35
    move-object/from16 v0, p1

    iget-object v2, v0, Lorg/apache/poi/hdf/extractor/CHP;->_dttmRMarkDel:[I

    const/4 v3, 0x0

    add-int/lit8 v4, p6, -0x4

    move-object/from16 v0, p5

    invoke-static {v0, v4}, Lorg/apache/poi/hdf/extractor/Utils;->convertBytesToShort([BI)S

    move-result v4

    aput v4, v2, v3

    .line 546
    move-object/from16 v0, p1

    iget-object v2, v0, Lorg/apache/poi/hdf/extractor/CHP;->_dttmRMarkDel:[I

    const/4 v3, 0x1

    add-int/lit8 v4, p6, -0x2

    move-object/from16 v0, p5

    invoke-static {v0, v4}, Lorg/apache/poi/hdf/extractor/Utils;->convertBytesToShort([BI)S

    move-result v4

    aput v4, v2, v3

    goto/16 :goto_0

    .line 549
    :pswitch_36
    move-object/from16 v0, p1

    iget-object v2, v0, Lorg/apache/poi/hdf/extractor/CHP;->_brc:[S

    const/4 v3, 0x0

    add-int/lit8 v4, p6, -0x4

    move-object/from16 v0, p5

    invoke-static {v0, v4}, Lorg/apache/poi/hdf/extractor/Utils;->convertBytesToShort([BI)S

    move-result v4

    aput-short v4, v2, v3

    .line 550
    move-object/from16 v0, p1

    iget-object v2, v0, Lorg/apache/poi/hdf/extractor/CHP;->_brc:[S

    const/4 v3, 0x1

    add-int/lit8 v4, p6, -0x2

    move-object/from16 v0, p5

    invoke-static {v0, v4}, Lorg/apache/poi/hdf/extractor/Utils;->convertBytesToShort([BI)S

    move-result v4

    aput-short v4, v2, v3

    goto/16 :goto_0

    .line 553
    :pswitch_37
    move/from16 v0, p3

    int-to-short v2, v0

    move-object/from16 v0, p1

    iput-short v2, v0, Lorg/apache/poi/hdf/extractor/CHP;->_shd:S

    goto/16 :goto_0

    .line 568
    :pswitch_38
    move/from16 v0, p3

    int-to-short v2, v0

    move-object/from16 v0, p1

    iput-short v2, v0, Lorg/apache/poi/hdf/extractor/CHP;->_lidDefault:S

    goto/16 :goto_0

    .line 571
    :pswitch_39
    move/from16 v0, p3

    int-to-short v2, v0

    move-object/from16 v0, p1

    iput-short v2, v0, Lorg/apache/poi/hdf/extractor/CHP;->_lidFE:S

    goto/16 :goto_0

    .line 574
    :pswitch_3a
    move/from16 v0, p3

    int-to-byte v2, v0

    move-object/from16 v0, p1

    iput-byte v2, v0, Lorg/apache/poi/hdf/extractor/CHP;->_idctHint:B

    goto/16 :goto_0

    .line 139
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_0
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_0
        :pswitch_a
        :pswitch_0
        :pswitch_b
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_c
        :pswitch_0
        :pswitch_d
        :pswitch_e
        :pswitch_0
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_19
        :pswitch_1a
        :pswitch_1b
        :pswitch_1c
        :pswitch_1d
        :pswitch_1e
        :pswitch_1f
        :pswitch_20
        :pswitch_21
        :pswitch_22
        :pswitch_23
        :pswitch_24
        :pswitch_25
        :pswitch_26
        :pswitch_27
        :pswitch_28
        :pswitch_29
        :pswitch_2a
        :pswitch_2b
        :pswitch_0
        :pswitch_2c
        :pswitch_2d
        :pswitch_2e
        :pswitch_2f
        :pswitch_30
        :pswitch_31
        :pswitch_32
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_33
        :pswitch_34
        :pswitch_35
        :pswitch_36
        :pswitch_37
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_38
        :pswitch_39
        :pswitch_3a
    .end packed-switch
.end method

.method static doPAPOperation(Lorg/apache/poi/hdf/extractor/PAP;II[B[BII)V
    .locals 4
    .param p0, "newPAP"    # Lorg/apache/poi/hdf/extractor/PAP;
    .param p1, "operand"    # I
    .param p2, "param"    # I
    .param p3, "varParam"    # [B
    .param p4, "grpprl"    # [B
    .param p5, "offset"    # I
    .param p6, "spra"    # I

    .prologue
    const/16 v1, 0x9

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 727
    packed-switch p1, :pswitch_data_0

    .line 977
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 730
    :pswitch_1
    iput p2, p0, Lorg/apache/poi/hdf/extractor/PAP;->_istd:I

    goto :goto_0

    .line 736
    :pswitch_2
    iget v0, p0, Lorg/apache/poi/hdf/extractor/PAP;->_istd:I

    if-le v0, v1, :cond_1

    iget v0, p0, Lorg/apache/poi/hdf/extractor/PAP;->_istd:I

    if-lt v0, v2, :cond_0

    .line 738
    :cond_1
    iget v0, p0, Lorg/apache/poi/hdf/extractor/PAP;->_istd:I

    add-int/2addr v0, p2

    iput v0, p0, Lorg/apache/poi/hdf/extractor/PAP;->_istd:I

    .line 739
    if-lez p2, :cond_2

    .line 741
    iget v0, p0, Lorg/apache/poi/hdf/extractor/PAP;->_istd:I

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hdf/extractor/PAP;->_istd:I

    goto :goto_0

    .line 745
    :cond_2
    iget v0, p0, Lorg/apache/poi/hdf/extractor/PAP;->_istd:I

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hdf/extractor/PAP;->_istd:I

    goto :goto_0

    .line 750
    :pswitch_3
    int-to-byte v0, p2

    iput-byte v0, p0, Lorg/apache/poi/hdf/extractor/PAP;->_jc:B

    goto :goto_0

    .line 753
    :pswitch_4
    int-to-byte v0, p2

    iput-byte v0, p0, Lorg/apache/poi/hdf/extractor/PAP;->_fSideBySide:B

    goto :goto_0

    .line 756
    :pswitch_5
    int-to-byte v0, p2

    iput-byte v0, p0, Lorg/apache/poi/hdf/extractor/PAP;->_fKeep:B

    goto :goto_0

    .line 759
    :pswitch_6
    int-to-byte v0, p2

    iput-byte v0, p0, Lorg/apache/poi/hdf/extractor/PAP;->_fKeepFollow:B

    goto :goto_0

    .line 762
    :pswitch_7
    int-to-byte v0, p2

    iput-byte v0, p0, Lorg/apache/poi/hdf/extractor/PAP;->_fPageBreakBefore:B

    goto :goto_0

    .line 765
    :pswitch_8
    int-to-byte v0, p2

    iput-byte v0, p0, Lorg/apache/poi/hdf/extractor/PAP;->_brcl:B

    goto :goto_0

    .line 768
    :pswitch_9
    int-to-byte v0, p2

    iput-byte v0, p0, Lorg/apache/poi/hdf/extractor/PAP;->_brcp:B

    goto :goto_0

    .line 771
    :pswitch_a
    int-to-byte v0, p2

    iput-byte v0, p0, Lorg/apache/poi/hdf/extractor/PAP;->_ilvl:B

    goto :goto_0

    .line 774
    :pswitch_b
    iput p2, p0, Lorg/apache/poi/hdf/extractor/PAP;->_ilfo:I

    goto :goto_0

    .line 777
    :pswitch_c
    int-to-byte v0, p2

    iput-byte v0, p0, Lorg/apache/poi/hdf/extractor/PAP;->_fNoLnn:B

    goto :goto_0

    .line 783
    :pswitch_d
    iput p2, p0, Lorg/apache/poi/hdf/extractor/PAP;->_dxaRight:I

    goto :goto_0

    .line 786
    :pswitch_e
    iput p2, p0, Lorg/apache/poi/hdf/extractor/PAP;->_dxaLeft:I

    goto :goto_0

    .line 789
    :pswitch_f
    iget v0, p0, Lorg/apache/poi/hdf/extractor/PAP;->_dxaLeft:I

    add-int/2addr v0, p2

    iput v0, p0, Lorg/apache/poi/hdf/extractor/PAP;->_dxaLeft:I

    .line 790
    iget v0, p0, Lorg/apache/poi/hdf/extractor/PAP;->_dxaLeft:I

    invoke-static {v3, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lorg/apache/poi/hdf/extractor/PAP;->_dxaLeft:I

    goto :goto_0

    .line 793
    :pswitch_10
    iput p2, p0, Lorg/apache/poi/hdf/extractor/PAP;->_dxaLeft1:I

    goto :goto_0

    .line 796
    :pswitch_11
    iget-object v0, p0, Lorg/apache/poi/hdf/extractor/PAP;->_lspd:[I

    add-int/lit8 v1, p5, -0x4

    invoke-static {p4, v1}, Lorg/apache/poi/hdf/extractor/Utils;->convertBytesToShort([BI)S

    move-result v1

    aput v1, v0, v3

    .line 797
    iget-object v0, p0, Lorg/apache/poi/hdf/extractor/PAP;->_lspd:[I

    add-int/lit8 v1, p5, -0x2

    invoke-static {p4, v1}, Lorg/apache/poi/hdf/extractor/Utils;->convertBytesToShort([BI)S

    move-result v1

    aput v1, v0, v2

    goto :goto_0

    .line 800
    :pswitch_12
    iput p2, p0, Lorg/apache/poi/hdf/extractor/PAP;->_dyaBefore:I

    goto :goto_0

    .line 803
    :pswitch_13
    iput p2, p0, Lorg/apache/poi/hdf/extractor/PAP;->_dyaAfter:I

    goto :goto_0

    .line 809
    :pswitch_14
    int-to-byte v0, p2

    iput-byte v0, p0, Lorg/apache/poi/hdf/extractor/PAP;->_fInTable:B

    goto/16 :goto_0

    .line 812
    :pswitch_15
    int-to-byte v0, p2

    iput-byte v0, p0, Lorg/apache/poi/hdf/extractor/PAP;->_fTtp:B

    goto/16 :goto_0

    .line 815
    :pswitch_16
    iput p2, p0, Lorg/apache/poi/hdf/extractor/PAP;->_dxaAbs:I

    goto/16 :goto_0

    .line 818
    :pswitch_17
    iput p2, p0, Lorg/apache/poi/hdf/extractor/PAP;->_dyaAbs:I

    goto/16 :goto_0

    .line 821
    :pswitch_18
    iput p2, p0, Lorg/apache/poi/hdf/extractor/PAP;->_dxaWidth:I

    goto/16 :goto_0

    .line 837
    :pswitch_19
    int-to-short v0, p2

    iput-short v0, p0, Lorg/apache/poi/hdf/extractor/PAP;->_brcTop1:S

    goto/16 :goto_0

    .line 840
    :pswitch_1a
    int-to-short v0, p2

    iput-short v0, p0, Lorg/apache/poi/hdf/extractor/PAP;->_brcLeft1:S

    goto/16 :goto_0

    .line 843
    :pswitch_1b
    int-to-short v0, p2

    iput-short v0, p0, Lorg/apache/poi/hdf/extractor/PAP;->_brcBottom1:S

    goto/16 :goto_0

    .line 846
    :pswitch_1c
    int-to-short v0, p2

    iput-short v0, p0, Lorg/apache/poi/hdf/extractor/PAP;->_brcRight1:S

    goto/16 :goto_0

    .line 849
    :pswitch_1d
    int-to-short v0, p2

    iput-short v0, p0, Lorg/apache/poi/hdf/extractor/PAP;->_brcBetween1:S

    goto/16 :goto_0

    .line 852
    :pswitch_1e
    int-to-byte v0, p2

    iput-short v0, p0, Lorg/apache/poi/hdf/extractor/PAP;->_brcBar1:S

    goto/16 :goto_0

    .line 855
    :pswitch_1f
    iput p2, p0, Lorg/apache/poi/hdf/extractor/PAP;->_dxaFromText:I

    goto/16 :goto_0

    .line 858
    :pswitch_20
    int-to-byte v0, p2

    iput-byte v0, p0, Lorg/apache/poi/hdf/extractor/PAP;->_wr:B

    goto/16 :goto_0

    .line 861
    :pswitch_21
    iget-object v0, p0, Lorg/apache/poi/hdf/extractor/PAP;->_brcTop:[S

    add-int/lit8 v1, p5, -0x4

    invoke-static {p4, v1}, Lorg/apache/poi/hdf/extractor/Utils;->convertBytesToShort([BI)S

    move-result v1

    aput-short v1, v0, v3

    .line 862
    iget-object v0, p0, Lorg/apache/poi/hdf/extractor/PAP;->_brcTop:[S

    add-int/lit8 v1, p5, -0x2

    invoke-static {p4, v1}, Lorg/apache/poi/hdf/extractor/Utils;->convertBytesToShort([BI)S

    move-result v1

    aput-short v1, v0, v2

    goto/16 :goto_0

    .line 865
    :pswitch_22
    iget-object v0, p0, Lorg/apache/poi/hdf/extractor/PAP;->_brcLeft:[S

    add-int/lit8 v1, p5, -0x4

    invoke-static {p4, v1}, Lorg/apache/poi/hdf/extractor/Utils;->convertBytesToShort([BI)S

    move-result v1

    aput-short v1, v0, v3

    .line 866
    iget-object v0, p0, Lorg/apache/poi/hdf/extractor/PAP;->_brcLeft:[S

    add-int/lit8 v1, p5, -0x2

    invoke-static {p4, v1}, Lorg/apache/poi/hdf/extractor/Utils;->convertBytesToShort([BI)S

    move-result v1

    aput-short v1, v0, v2

    goto/16 :goto_0

    .line 869
    :pswitch_23
    iget-object v0, p0, Lorg/apache/poi/hdf/extractor/PAP;->_brcBottom:[S

    add-int/lit8 v1, p5, -0x4

    invoke-static {p4, v1}, Lorg/apache/poi/hdf/extractor/Utils;->convertBytesToShort([BI)S

    move-result v1

    aput-short v1, v0, v3

    .line 870
    iget-object v0, p0, Lorg/apache/poi/hdf/extractor/PAP;->_brcBottom:[S

    add-int/lit8 v1, p5, -0x2

    invoke-static {p4, v1}, Lorg/apache/poi/hdf/extractor/Utils;->convertBytesToShort([BI)S

    move-result v1

    aput-short v1, v0, v2

    goto/16 :goto_0

    .line 873
    :pswitch_24
    iget-object v0, p0, Lorg/apache/poi/hdf/extractor/PAP;->_brcRight:[S

    add-int/lit8 v1, p5, -0x4

    invoke-static {p4, v1}, Lorg/apache/poi/hdf/extractor/Utils;->convertBytesToShort([BI)S

    move-result v1

    aput-short v1, v0, v3

    .line 874
    iget-object v0, p0, Lorg/apache/poi/hdf/extractor/PAP;->_brcRight:[S

    add-int/lit8 v1, p5, -0x2

    invoke-static {p4, v1}, Lorg/apache/poi/hdf/extractor/Utils;->convertBytesToShort([BI)S

    move-result v1

    aput-short v1, v0, v2

    goto/16 :goto_0

    .line 877
    :pswitch_25
    iget-object v0, p0, Lorg/apache/poi/hdf/extractor/PAP;->_brcBetween:[S

    add-int/lit8 v1, p5, -0x4

    invoke-static {p4, v1}, Lorg/apache/poi/hdf/extractor/Utils;->convertBytesToShort([BI)S

    move-result v1

    aput-short v1, v0, v3

    .line 878
    iget-object v0, p0, Lorg/apache/poi/hdf/extractor/PAP;->_brcBetween:[S

    add-int/lit8 v1, p5, -0x2

    invoke-static {p4, v1}, Lorg/apache/poi/hdf/extractor/Utils;->convertBytesToShort([BI)S

    move-result v1

    aput-short v1, v0, v2

    goto/16 :goto_0

    .line 881
    :pswitch_26
    iget-object v0, p0, Lorg/apache/poi/hdf/extractor/PAP;->_brcBar:[S

    add-int/lit8 v1, p5, -0x4

    invoke-static {p4, v1}, Lorg/apache/poi/hdf/extractor/Utils;->convertBytesToShort([BI)S

    move-result v1

    aput-short v1, v0, v3

    .line 882
    iget-object v0, p0, Lorg/apache/poi/hdf/extractor/PAP;->_brcBar:[S

    add-int/lit8 v1, p5, -0x2

    invoke-static {p4, v1}, Lorg/apache/poi/hdf/extractor/Utils;->convertBytesToShort([BI)S

    move-result v1

    aput-short v1, v0, v2

    goto/16 :goto_0

    .line 885
    :pswitch_27
    int-to-byte v0, p2

    iput-byte v0, p0, Lorg/apache/poi/hdf/extractor/PAP;->_fNoAutoHyph:B

    goto/16 :goto_0

    .line 888
    :pswitch_28
    iput p2, p0, Lorg/apache/poi/hdf/extractor/PAP;->_dyaHeight:I

    goto/16 :goto_0

    .line 891
    :pswitch_29
    iput p2, p0, Lorg/apache/poi/hdf/extractor/PAP;->_dcs:I

    goto/16 :goto_0

    .line 894
    :pswitch_2a
    iput p2, p0, Lorg/apache/poi/hdf/extractor/PAP;->_shd:I

    goto/16 :goto_0

    .line 897
    :pswitch_2b
    iput p2, p0, Lorg/apache/poi/hdf/extractor/PAP;->_dyaFromText:I

    goto/16 :goto_0

    .line 900
    :pswitch_2c
    iput p2, p0, Lorg/apache/poi/hdf/extractor/PAP;->_dxaFromText:I

    goto/16 :goto_0

    .line 903
    :pswitch_2d
    int-to-byte v0, p2

    iput-byte v0, p0, Lorg/apache/poi/hdf/extractor/PAP;->_fLocked:B

    goto/16 :goto_0

    .line 906
    :pswitch_2e
    int-to-byte v0, p2

    iput-byte v0, p0, Lorg/apache/poi/hdf/extractor/PAP;->_fWindowControl:B

    goto/16 :goto_0

    .line 912
    :pswitch_2f
    int-to-byte v0, p2

    iput-byte v0, p0, Lorg/apache/poi/hdf/extractor/PAP;->_fKinsoku:B

    goto/16 :goto_0

    .line 915
    :pswitch_30
    int-to-byte v0, p2

    iput-byte v0, p0, Lorg/apache/poi/hdf/extractor/PAP;->_fWordWrap:B

    goto/16 :goto_0

    .line 918
    :pswitch_31
    int-to-byte v0, p2

    iput-byte v0, p0, Lorg/apache/poi/hdf/extractor/PAP;->_fOverflowPunct:B

    goto/16 :goto_0

    .line 921
    :pswitch_32
    int-to-byte v0, p2

    iput-byte v0, p0, Lorg/apache/poi/hdf/extractor/PAP;->_fTopLinePunct:B

    goto/16 :goto_0

    .line 924
    :pswitch_33
    int-to-byte v0, p2

    iput-byte v0, p0, Lorg/apache/poi/hdf/extractor/PAP;->_fAutoSpaceDE:B

    goto/16 :goto_0

    .line 927
    :pswitch_34
    int-to-byte v0, p2

    iput-byte v0, p0, Lorg/apache/poi/hdf/extractor/PAP;->_fAutoSpaceDN:B

    goto/16 :goto_0

    .line 930
    :pswitch_35
    iput p2, p0, Lorg/apache/poi/hdf/extractor/PAP;->_wAlignFont:I

    goto/16 :goto_0

    .line 933
    :pswitch_36
    int-to-short v0, p2

    iput-short v0, p0, Lorg/apache/poi/hdf/extractor/PAP;->_fontAlign:S

    goto/16 :goto_0

    .line 939
    :pswitch_37
    iput-object p3, p0, Lorg/apache/poi/hdf/extractor/PAP;->_anld:[B

    goto/16 :goto_0

    .line 958
    :pswitch_38
    const/4 v0, 0x6

    if-ne p6, v0, :cond_0

    .line 960
    iput-object p3, p0, Lorg/apache/poi/hdf/extractor/PAP;->_numrm:[B

    goto/16 :goto_0

    .line 969
    :pswitch_39
    int-to-byte v0, p2

    iput-byte v0, p0, Lorg/apache/poi/hdf/extractor/PAP;->_fUsePgsuSettings:B

    goto/16 :goto_0

    .line 972
    :pswitch_3a
    int-to-byte v0, p2

    iput-byte v0, p0, Lorg/apache/poi/hdf/extractor/PAP;->_fAdjustRight:B

    goto/16 :goto_0

    .line 727
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_0
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_0
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_0
        :pswitch_19
        :pswitch_1a
        :pswitch_1b
        :pswitch_1c
        :pswitch_1d
        :pswitch_1e
        :pswitch_1f
        :pswitch_20
        :pswitch_21
        :pswitch_22
        :pswitch_23
        :pswitch_24
        :pswitch_25
        :pswitch_26
        :pswitch_27
        :pswitch_28
        :pswitch_29
        :pswitch_2a
        :pswitch_2b
        :pswitch_2c
        :pswitch_2d
        :pswitch_2e
        :pswitch_0
        :pswitch_2f
        :pswitch_30
        :pswitch_31
        :pswitch_32
        :pswitch_33
        :pswitch_34
        :pswitch_35
        :pswitch_36
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_37
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_38
        :pswitch_0
        :pswitch_39
        :pswitch_3a
    .end packed-switch
.end method

.method static doSEPOperation(Lorg/apache/poi/hdf/extractor/SEP;II[B)V
    .locals 5
    .param p0, "newSEP"    # Lorg/apache/poi/hdf/extractor/SEP;
    .param p1, "operand"    # I
    .param p2, "param"    # I
    .param p3, "varParam"    # [B

    .prologue
    const v1, 0xffff

    const/4 v4, 0x1

    const/high16 v3, -0x10000

    const/4 v2, 0x0

    .line 1141
    packed-switch p1, :pswitch_data_0

    .line 1304
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 1144
    :pswitch_1
    int-to-byte v0, p2

    iput-byte v0, p0, Lorg/apache/poi/hdf/extractor/SEP;->_cnsPgn:B

    goto :goto_0

    .line 1147
    :pswitch_2
    int-to-byte v0, p2

    iput-byte v0, p0, Lorg/apache/poi/hdf/extractor/SEP;->_iHeadingPgn:B

    goto :goto_0

    .line 1150
    :pswitch_3
    iput-object p3, p0, Lorg/apache/poi/hdf/extractor/SEP;->_olstAnn:[B

    goto :goto_0

    .line 1159
    :pswitch_4
    invoke-static {p2}, Lorg/apache/poi/hdf/extractor/StyleSheet;->getFlag(I)Z

    move-result v0

    iput-boolean v0, p0, Lorg/apache/poi/hdf/extractor/SEP;->_fEvenlySpaced:Z

    goto :goto_0

    .line 1162
    :pswitch_5
    invoke-static {p2}, Lorg/apache/poi/hdf/extractor/StyleSheet;->getFlag(I)Z

    move-result v0

    iput-boolean v0, p0, Lorg/apache/poi/hdf/extractor/SEP;->_fUnlocked:Z

    goto :goto_0

    .line 1165
    :pswitch_6
    int-to-short v0, p2

    iput-short v0, p0, Lorg/apache/poi/hdf/extractor/SEP;->_dmBinFirst:S

    goto :goto_0

    .line 1168
    :pswitch_7
    int-to-short v0, p2

    iput-short v0, p0, Lorg/apache/poi/hdf/extractor/SEP;->_dmBinOther:S

    goto :goto_0

    .line 1171
    :pswitch_8
    int-to-byte v0, p2

    iput-byte v0, p0, Lorg/apache/poi/hdf/extractor/SEP;->_bkc:B

    goto :goto_0

    .line 1174
    :pswitch_9
    invoke-static {p2}, Lorg/apache/poi/hdf/extractor/StyleSheet;->getFlag(I)Z

    move-result v0

    iput-boolean v0, p0, Lorg/apache/poi/hdf/extractor/SEP;->_fTitlePage:Z

    goto :goto_0

    .line 1177
    :pswitch_a
    int-to-short v0, p2

    iput-short v0, p0, Lorg/apache/poi/hdf/extractor/SEP;->_ccolM1:S

    goto :goto_0

    .line 1180
    :pswitch_b
    iput p2, p0, Lorg/apache/poi/hdf/extractor/SEP;->_dxaColumns:I

    goto :goto_0

    .line 1183
    :pswitch_c
    invoke-static {p2}, Lorg/apache/poi/hdf/extractor/StyleSheet;->getFlag(I)Z

    move-result v0

    iput-boolean v0, p0, Lorg/apache/poi/hdf/extractor/SEP;->_fAutoPgn:Z

    goto :goto_0

    .line 1186
    :pswitch_d
    int-to-byte v0, p2

    iput-byte v0, p0, Lorg/apache/poi/hdf/extractor/SEP;->_nfcPgn:B

    goto :goto_0

    .line 1189
    :pswitch_e
    int-to-short v0, p2

    iput-short v0, p0, Lorg/apache/poi/hdf/extractor/SEP;->_dyaPgn:S

    goto :goto_0

    .line 1192
    :pswitch_f
    int-to-short v0, p2

    iput-short v0, p0, Lorg/apache/poi/hdf/extractor/SEP;->_dxaPgn:S

    goto :goto_0

    .line 1195
    :pswitch_10
    invoke-static {p2}, Lorg/apache/poi/hdf/extractor/StyleSheet;->getFlag(I)Z

    move-result v0

    iput-boolean v0, p0, Lorg/apache/poi/hdf/extractor/SEP;->_fPgnRestart:Z

    goto :goto_0

    .line 1198
    :pswitch_11
    invoke-static {p2}, Lorg/apache/poi/hdf/extractor/StyleSheet;->getFlag(I)Z

    move-result v0

    iput-boolean v0, p0, Lorg/apache/poi/hdf/extractor/SEP;->_fEndNote:Z

    goto :goto_0

    .line 1201
    :pswitch_12
    int-to-byte v0, p2

    iput-byte v0, p0, Lorg/apache/poi/hdf/extractor/SEP;->_lnc:B

    goto :goto_0

    .line 1204
    :pswitch_13
    int-to-byte v0, p2

    iput-byte v0, p0, Lorg/apache/poi/hdf/extractor/SEP;->_grpfIhdt:B

    goto :goto_0

    .line 1207
    :pswitch_14
    int-to-short v0, p2

    iput-short v0, p0, Lorg/apache/poi/hdf/extractor/SEP;->_nLnnMod:S

    goto :goto_0

    .line 1210
    :pswitch_15
    iput p2, p0, Lorg/apache/poi/hdf/extractor/SEP;->_dxaLnn:I

    goto :goto_0

    .line 1213
    :pswitch_16
    iput p2, p0, Lorg/apache/poi/hdf/extractor/SEP;->_dyaHdrTop:I

    goto :goto_0

    .line 1216
    :pswitch_17
    iput p2, p0, Lorg/apache/poi/hdf/extractor/SEP;->_dyaHdrBottom:I

    goto :goto_0

    .line 1219
    :pswitch_18
    invoke-static {p2}, Lorg/apache/poi/hdf/extractor/StyleSheet;->getFlag(I)Z

    move-result v0

    iput-boolean v0, p0, Lorg/apache/poi/hdf/extractor/SEP;->_fLBetween:Z

    goto :goto_0

    .line 1222
    :pswitch_19
    int-to-byte v0, p2

    iput-byte v0, p0, Lorg/apache/poi/hdf/extractor/SEP;->_vjc:B

    goto :goto_0

    .line 1225
    :pswitch_1a
    int-to-short v0, p2

    iput-short v0, p0, Lorg/apache/poi/hdf/extractor/SEP;->_lnnMin:S

    goto :goto_0

    .line 1228
    :pswitch_1b
    int-to-short v0, p2

    iput-short v0, p0, Lorg/apache/poi/hdf/extractor/SEP;->_pgnStart:S

    goto :goto_0

    .line 1231
    :pswitch_1c
    int-to-byte v0, p2

    iput-byte v0, p0, Lorg/apache/poi/hdf/extractor/SEP;->_dmOrientPage:B

    goto :goto_0

    .line 1237
    :pswitch_1d
    iput p2, p0, Lorg/apache/poi/hdf/extractor/SEP;->_xaPage:I

    goto/16 :goto_0

    .line 1240
    :pswitch_1e
    iput p2, p0, Lorg/apache/poi/hdf/extractor/SEP;->_yaPage:I

    goto/16 :goto_0

    .line 1243
    :pswitch_1f
    iput p2, p0, Lorg/apache/poi/hdf/extractor/SEP;->_dxaLeft:I

    goto/16 :goto_0

    .line 1246
    :pswitch_20
    iput p2, p0, Lorg/apache/poi/hdf/extractor/SEP;->_dxaRight:I

    goto/16 :goto_0

    .line 1249
    :pswitch_21
    iput p2, p0, Lorg/apache/poi/hdf/extractor/SEP;->_dyaTop:I

    goto/16 :goto_0

    .line 1252
    :pswitch_22
    iput p2, p0, Lorg/apache/poi/hdf/extractor/SEP;->_dyaBottom:I

    goto/16 :goto_0

    .line 1255
    :pswitch_23
    iput p2, p0, Lorg/apache/poi/hdf/extractor/SEP;->_dzaGutter:I

    goto/16 :goto_0

    .line 1258
    :pswitch_24
    int-to-short v0, p2

    iput-short v0, p0, Lorg/apache/poi/hdf/extractor/SEP;->_dmPaperReq:S

    goto/16 :goto_0

    .line 1262
    :pswitch_25
    if-eqz p3, :cond_0

    .line 1263
    aget-byte v0, p3, v2

    invoke-static {v0}, Lorg/apache/poi/hdf/extractor/StyleSheet;->getFlag(I)Z

    move-result v0

    iput-boolean v0, p0, Lorg/apache/poi/hdf/extractor/SEP;->_fPropMark:Z

    goto/16 :goto_0

    .line 1273
    :pswitch_26
    iget-object v0, p0, Lorg/apache/poi/hdf/extractor/SEP;->_brcTop:[S

    and-int/2addr v1, p2

    int-to-short v1, v1

    aput-short v1, v0, v2

    .line 1274
    iget-object v0, p0, Lorg/apache/poi/hdf/extractor/SEP;->_brcTop:[S

    and-int v1, p2, v3

    shr-int/lit8 v1, v1, 0x10

    int-to-short v1, v1

    aput-short v1, v0, v4

    goto/16 :goto_0

    .line 1277
    :pswitch_27
    iget-object v0, p0, Lorg/apache/poi/hdf/extractor/SEP;->_brcLeft:[S

    and-int/2addr v1, p2

    int-to-short v1, v1

    aput-short v1, v0, v2

    .line 1278
    iget-object v0, p0, Lorg/apache/poi/hdf/extractor/SEP;->_brcLeft:[S

    and-int v1, p2, v3

    shr-int/lit8 v1, v1, 0x10

    int-to-short v1, v1

    aput-short v1, v0, v4

    goto/16 :goto_0

    .line 1281
    :pswitch_28
    iget-object v0, p0, Lorg/apache/poi/hdf/extractor/SEP;->_brcBottom:[S

    and-int/2addr v1, p2

    int-to-short v1, v1

    aput-short v1, v0, v2

    .line 1282
    iget-object v0, p0, Lorg/apache/poi/hdf/extractor/SEP;->_brcBottom:[S

    and-int v1, p2, v3

    shr-int/lit8 v1, v1, 0x10

    int-to-short v1, v1

    aput-short v1, v0, v4

    goto/16 :goto_0

    .line 1285
    :pswitch_29
    iget-object v0, p0, Lorg/apache/poi/hdf/extractor/SEP;->_brcRight:[S

    and-int/2addr v1, p2

    int-to-short v1, v1

    aput-short v1, v0, v2

    .line 1286
    iget-object v0, p0, Lorg/apache/poi/hdf/extractor/SEP;->_brcRight:[S

    and-int v1, p2, v3

    shr-int/lit8 v1, v1, 0x10

    int-to-short v1, v1

    aput-short v1, v0, v4

    goto/16 :goto_0

    .line 1289
    :pswitch_2a
    int-to-short v0, p2

    iput-short v0, p0, Lorg/apache/poi/hdf/extractor/SEP;->_pgbProp:S

    goto/16 :goto_0

    .line 1292
    :pswitch_2b
    iput p2, p0, Lorg/apache/poi/hdf/extractor/SEP;->_dxtCharSpace:I

    goto/16 :goto_0

    .line 1295
    :pswitch_2c
    iput p2, p0, Lorg/apache/poi/hdf/extractor/SEP;->_dyaLinePitch:I

    goto/16 :goto_0

    .line 1298
    :pswitch_2d
    int-to-short v0, p2

    iput-short v0, p0, Lorg/apache/poi/hdf/extractor/SEP;->_wTextFlow:S

    goto/16 :goto_0

    .line 1141
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_19
        :pswitch_1a
        :pswitch_1b
        :pswitch_1c
        :pswitch_0
        :pswitch_1d
        :pswitch_1e
        :pswitch_1f
        :pswitch_20
        :pswitch_21
        :pswitch_22
        :pswitch_23
        :pswitch_24
        :pswitch_25
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_26
        :pswitch_27
        :pswitch_28
        :pswitch_29
        :pswitch_2a
        :pswitch_2b
        :pswitch_2c
        :pswitch_0
        :pswitch_2d
    .end packed-switch
.end method

.method static doTAPOperation(Lorg/apache/poi/hdf/extractor/TAP;II[B)V
    .locals 11
    .param p0, "newTAP"    # Lorg/apache/poi/hdf/extractor/TAP;
    .param p1, "operand"    # I
    .param p2, "param"    # I
    .param p3, "varParam"    # [B

    .prologue
    .line 980
    packed-switch p1, :pswitch_data_0

    .line 1138
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 983
    :pswitch_1
    int-to-short v7, p2

    iput-short v7, p0, Lorg/apache/poi/hdf/extractor/TAP;->_jc:S

    goto :goto_0

    .line 987
    :pswitch_2
    iget-object v7, p0, Lorg/apache/poi/hdf/extractor/TAP;->_rgdxaCenter:[S

    const/4 v8, 0x0

    aget-short v7, v7, v8

    iget v8, p0, Lorg/apache/poi/hdf/extractor/TAP;->_dxaGapHalf:I

    add-int/2addr v7, v8

    sub-int v0, p2, v7

    .line 988
    .local v0, "adjust":I
    const/4 v6, 0x0

    .local v6, "x":I
    :goto_1
    iget-short v7, p0, Lorg/apache/poi/hdf/extractor/TAP;->_itcMac:S

    if-ge v6, v7, :cond_0

    .line 990
    iget-object v7, p0, Lorg/apache/poi/hdf/extractor/TAP;->_rgdxaCenter:[S

    aget-short v8, v7, v6

    add-int/2addr v8, v0

    int-to-short v8, v8

    aput-short v8, v7, v6

    .line 988
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 995
    .end local v0    # "adjust":I
    .end local v6    # "x":I
    :pswitch_3
    iget-object v7, p0, Lorg/apache/poi/hdf/extractor/TAP;->_rgdxaCenter:[S

    if-eqz v7, :cond_1

    .line 997
    iget v7, p0, Lorg/apache/poi/hdf/extractor/TAP;->_dxaGapHalf:I

    sub-int v0, v7, p2

    .line 998
    .restart local v0    # "adjust":I
    iget-object v7, p0, Lorg/apache/poi/hdf/extractor/TAP;->_rgdxaCenter:[S

    const/4 v8, 0x0

    aget-short v9, v7, v8

    add-int/2addr v9, v0

    int-to-short v9, v9

    aput-short v9, v7, v8

    .line 1000
    .end local v0    # "adjust":I
    :cond_1
    iput p2, p0, Lorg/apache/poi/hdf/extractor/TAP;->_dxaGapHalf:I

    goto :goto_0

    .line 1003
    :pswitch_4
    invoke-static {p2}, Lorg/apache/poi/hdf/extractor/StyleSheet;->getFlag(I)Z

    move-result v7

    iput-boolean v7, p0, Lorg/apache/poi/hdf/extractor/TAP;->_fCantSplit:Z

    goto :goto_0

    .line 1006
    :pswitch_5
    invoke-static {p2}, Lorg/apache/poi/hdf/extractor/StyleSheet;->getFlag(I)Z

    move-result v7

    iput-boolean v7, p0, Lorg/apache/poi/hdf/extractor/TAP;->_fTableHeader:Z

    goto :goto_0

    .line 1010
    :pswitch_6
    if-eqz p3, :cond_0

    .line 1011
    iget-object v7, p0, Lorg/apache/poi/hdf/extractor/TAP;->_brcTop:[S

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-static {p3, v9}, Lorg/apache/poi/hdf/extractor/Utils;->convertBytesToShort([BI)S

    move-result v9

    aput-short v9, v7, v8

    .line 1012
    iget-object v7, p0, Lorg/apache/poi/hdf/extractor/TAP;->_brcTop:[S

    const/4 v8, 0x1

    const/4 v9, 0x2

    invoke-static {p3, v9}, Lorg/apache/poi/hdf/extractor/Utils;->convertBytesToShort([BI)S

    move-result v9

    aput-short v9, v7, v8

    .line 1014
    iget-object v7, p0, Lorg/apache/poi/hdf/extractor/TAP;->_brcLeft:[S

    const/4 v8, 0x0

    const/4 v9, 0x4

    invoke-static {p3, v9}, Lorg/apache/poi/hdf/extractor/Utils;->convertBytesToShort([BI)S

    move-result v9

    aput-short v9, v7, v8

    .line 1015
    iget-object v7, p0, Lorg/apache/poi/hdf/extractor/TAP;->_brcLeft:[S

    const/4 v8, 0x1

    const/4 v9, 0x6

    invoke-static {p3, v9}, Lorg/apache/poi/hdf/extractor/Utils;->convertBytesToShort([BI)S

    move-result v9

    aput-short v9, v7, v8

    .line 1017
    iget-object v7, p0, Lorg/apache/poi/hdf/extractor/TAP;->_brcBottom:[S

    const/4 v8, 0x0

    const/16 v9, 0x8

    invoke-static {p3, v9}, Lorg/apache/poi/hdf/extractor/Utils;->convertBytesToShort([BI)S

    move-result v9

    aput-short v9, v7, v8

    .line 1018
    iget-object v7, p0, Lorg/apache/poi/hdf/extractor/TAP;->_brcBottom:[S

    const/4 v8, 0x1

    const/16 v9, 0xa

    invoke-static {p3, v9}, Lorg/apache/poi/hdf/extractor/Utils;->convertBytesToShort([BI)S

    move-result v9

    aput-short v9, v7, v8

    .line 1020
    iget-object v7, p0, Lorg/apache/poi/hdf/extractor/TAP;->_brcRight:[S

    const/4 v8, 0x0

    const/16 v9, 0xc

    invoke-static {p3, v9}, Lorg/apache/poi/hdf/extractor/Utils;->convertBytesToShort([BI)S

    move-result v9

    aput-short v9, v7, v8

    .line 1021
    iget-object v7, p0, Lorg/apache/poi/hdf/extractor/TAP;->_brcRight:[S

    const/4 v8, 0x1

    const/16 v9, 0xe

    invoke-static {p3, v9}, Lorg/apache/poi/hdf/extractor/Utils;->convertBytesToShort([BI)S

    move-result v9

    aput-short v9, v7, v8

    .line 1023
    iget-object v7, p0, Lorg/apache/poi/hdf/extractor/TAP;->_brcHorizontal:[S

    const/4 v8, 0x0

    .line 1024
    const/16 v9, 0x10

    .line 1023
    invoke-static {p3, v9}, Lorg/apache/poi/hdf/extractor/Utils;->convertBytesToShort([BI)S

    move-result v9

    aput-short v9, v7, v8

    .line 1025
    iget-object v7, p0, Lorg/apache/poi/hdf/extractor/TAP;->_brcHorizontal:[S

    const/4 v8, 0x1

    .line 1026
    const/16 v9, 0x12

    .line 1025
    invoke-static {p3, v9}, Lorg/apache/poi/hdf/extractor/Utils;->convertBytesToShort([BI)S

    move-result v9

    aput-short v9, v7, v8

    .line 1028
    iget-object v7, p0, Lorg/apache/poi/hdf/extractor/TAP;->_brcVertical:[S

    const/4 v8, 0x0

    .line 1029
    const/16 v9, 0x14

    invoke-static {p3, v9}, Lorg/apache/poi/hdf/extractor/Utils;->convertBytesToShort([BI)S

    move-result v9

    .line 1028
    aput-short v9, v7, v8

    .line 1030
    iget-object v7, p0, Lorg/apache/poi/hdf/extractor/TAP;->_brcVertical:[S

    const/4 v8, 0x1

    .line 1031
    const/16 v9, 0x16

    invoke-static {p3, v9}, Lorg/apache/poi/hdf/extractor/Utils;->convertBytesToShort([BI)S

    move-result v9

    .line 1030
    aput-short v9, v7, v8

    goto/16 :goto_0

    .line 1038
    :pswitch_7
    iput p2, p0, Lorg/apache/poi/hdf/extractor/TAP;->_dyaRowHeight:I

    goto/16 :goto_0

    .line 1042
    :pswitch_8
    if-eqz p3, :cond_0

    .line 1044
    const/4 v7, 0x0

    aget-byte v7, p3, v7

    iput-short v7, p0, Lorg/apache/poi/hdf/extractor/TAP;->_itcMac:S

    .line 1045
    const/4 v7, 0x0

    aget-byte v7, p3, v7

    add-int/lit8 v7, v7, 0x1

    new-array v7, v7, [S

    iput-object v7, p0, Lorg/apache/poi/hdf/extractor/TAP;->_rgdxaCenter:[S

    .line 1046
    const/4 v7, 0x0

    aget-byte v7, p3, v7

    new-array v7, v7, [Lorg/apache/poi/hdf/extractor/TC;

    iput-object v7, p0, Lorg/apache/poi/hdf/extractor/TAP;->_rgtc:[Lorg/apache/poi/hdf/extractor/TC;

    .line 1048
    const/4 v6, 0x0

    .restart local v6    # "x":I
    :goto_2
    iget-short v7, p0, Lorg/apache/poi/hdf/extractor/TAP;->_itcMac:S

    if-lt v6, v7, :cond_2

    .line 1054
    iget-object v7, p0, Lorg/apache/poi/hdf/extractor/TAP;->_rgdxaCenter:[S

    iget-short v8, p0, Lorg/apache/poi/hdf/extractor/TAP;->_itcMac:S

    .line 1055
    iget-short v9, p0, Lorg/apache/poi/hdf/extractor/TAP;->_itcMac:S

    mul-int/lit8 v9, v9, 0x2

    add-int/lit8 v9, v9, 0x1

    invoke-static {p3, v9}, Lorg/apache/poi/hdf/extractor/Utils;->convertBytesToShort([BI)S

    move-result v9

    .line 1054
    aput-short v9, v7, v8

    goto/16 :goto_0

    .line 1049
    :cond_2
    iget-object v7, p0, Lorg/apache/poi/hdf/extractor/TAP;->_rgdxaCenter:[S

    .line 1050
    mul-int/lit8 v8, v6, 0x2

    add-int/lit8 v8, v8, 0x1

    .line 1049
    invoke-static {p3, v8}, Lorg/apache/poi/hdf/extractor/Utils;->convertBytesToShort([BI)S

    move-result v8

    aput-short v8, v7, v6

    .line 1051
    iget-object v7, p0, Lorg/apache/poi/hdf/extractor/TAP;->_rgtc:[Lorg/apache/poi/hdf/extractor/TC;

    .line 1052
    const/4 v8, 0x0

    aget-byte v8, p3, v8

    add-int/lit8 v8, v8, 0x1

    mul-int/lit8 v8, v8, 0x2

    add-int/lit8 v8, v8, 0x1

    mul-int/lit8 v9, v6, 0x14

    add-int/2addr v8, v9

    .line 1051
    invoke-static {p3, v8}, Lorg/apache/poi/hdf/extractor/TC;->convertBytesToTC([BI)Lorg/apache/poi/hdf/extractor/TC;

    move-result-object v8

    aput-object v8, v7, v6

    .line 1048
    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    .line 1066
    .end local v6    # "x":I
    :pswitch_9
    if-eqz p3, :cond_0

    .line 1067
    const/4 v7, 0x0

    aget-byte v6, p3, v7

    .restart local v6    # "x":I
    :goto_3
    const/4 v7, 0x1

    aget-byte v7, p3, v7

    if-ge v6, v7, :cond_0

    .line 1068
    const/4 v7, 0x2

    aget-byte v7, p3, v7

    and-int/lit8 v7, v7, 0x8

    if-lez v7, :cond_4

    .line 1069
    iget-object v7, p0, Lorg/apache/poi/hdf/extractor/TAP;->_rgtc:[Lorg/apache/poi/hdf/extractor/TC;

    aget-object v7, v7, v6

    iget-object v7, v7, Lorg/apache/poi/hdf/extractor/TC;->_brcRight:[S

    const/4 v8, 0x0

    .line 1070
    const/4 v9, 0x6

    invoke-static {p3, v9}, Lorg/apache/poi/hdf/extractor/Utils;->convertBytesToShort([BI)S

    move-result v9

    .line 1069
    aput-short v9, v7, v8

    .line 1071
    iget-object v7, p0, Lorg/apache/poi/hdf/extractor/TAP;->_rgtc:[Lorg/apache/poi/hdf/extractor/TC;

    aget-object v7, v7, v6

    iget-object v7, v7, Lorg/apache/poi/hdf/extractor/TC;->_brcRight:[S

    const/4 v8, 0x1

    .line 1072
    const/16 v9, 0x8

    invoke-static {p3, v9}, Lorg/apache/poi/hdf/extractor/Utils;->convertBytesToShort([BI)S

    move-result v9

    .line 1071
    aput-short v9, v7, v8

    .line 1067
    :cond_3
    :goto_4
    add-int/lit8 v6, v6, 0x1

    goto :goto_3

    .line 1073
    :cond_4
    const/4 v7, 0x2

    aget-byte v7, p3, v7

    and-int/lit8 v7, v7, 0x4

    if-lez v7, :cond_5

    .line 1074
    iget-object v7, p0, Lorg/apache/poi/hdf/extractor/TAP;->_rgtc:[Lorg/apache/poi/hdf/extractor/TC;

    aget-object v7, v7, v6

    iget-object v7, v7, Lorg/apache/poi/hdf/extractor/TC;->_brcBottom:[S

    const/4 v8, 0x0

    .line 1075
    const/4 v9, 0x6

    invoke-static {p3, v9}, Lorg/apache/poi/hdf/extractor/Utils;->convertBytesToShort([BI)S

    move-result v9

    .line 1074
    aput-short v9, v7, v8

    .line 1076
    iget-object v7, p0, Lorg/apache/poi/hdf/extractor/TAP;->_rgtc:[Lorg/apache/poi/hdf/extractor/TC;

    aget-object v7, v7, v6

    iget-object v7, v7, Lorg/apache/poi/hdf/extractor/TC;->_brcBottom:[S

    const/4 v8, 0x1

    .line 1077
    const/16 v9, 0x8

    invoke-static {p3, v9}, Lorg/apache/poi/hdf/extractor/Utils;->convertBytesToShort([BI)S

    move-result v9

    .line 1076
    aput-short v9, v7, v8

    goto :goto_4

    .line 1078
    :cond_5
    const/4 v7, 0x2

    aget-byte v7, p3, v7

    and-int/lit8 v7, v7, 0x2

    if-lez v7, :cond_6

    .line 1079
    iget-object v7, p0, Lorg/apache/poi/hdf/extractor/TAP;->_rgtc:[Lorg/apache/poi/hdf/extractor/TC;

    aget-object v7, v7, v6

    iget-object v7, v7, Lorg/apache/poi/hdf/extractor/TC;->_brcLeft:[S

    const/4 v8, 0x0

    .line 1080
    const/4 v9, 0x6

    invoke-static {p3, v9}, Lorg/apache/poi/hdf/extractor/Utils;->convertBytesToShort([BI)S

    move-result v9

    .line 1079
    aput-short v9, v7, v8

    .line 1081
    iget-object v7, p0, Lorg/apache/poi/hdf/extractor/TAP;->_rgtc:[Lorg/apache/poi/hdf/extractor/TC;

    aget-object v7, v7, v6

    iget-object v7, v7, Lorg/apache/poi/hdf/extractor/TC;->_brcLeft:[S

    const/4 v8, 0x1

    .line 1082
    const/16 v9, 0x8

    invoke-static {p3, v9}, Lorg/apache/poi/hdf/extractor/Utils;->convertBytesToShort([BI)S

    move-result v9

    .line 1081
    aput-short v9, v7, v8

    goto :goto_4

    .line 1083
    :cond_6
    const/4 v7, 0x2

    aget-byte v7, p3, v7

    and-int/lit8 v7, v7, 0x1

    if-lez v7, :cond_3

    .line 1084
    iget-object v7, p0, Lorg/apache/poi/hdf/extractor/TAP;->_rgtc:[Lorg/apache/poi/hdf/extractor/TC;

    aget-object v7, v7, v6

    iget-object v7, v7, Lorg/apache/poi/hdf/extractor/TC;->_brcTop:[S

    const/4 v8, 0x0

    .line 1085
    const/4 v9, 0x6

    .line 1084
    invoke-static {p3, v9}, Lorg/apache/poi/hdf/extractor/Utils;->convertBytesToShort([BI)S

    move-result v9

    aput-short v9, v7, v8

    .line 1086
    iget-object v7, p0, Lorg/apache/poi/hdf/extractor/TAP;->_rgtc:[Lorg/apache/poi/hdf/extractor/TC;

    aget-object v7, v7, v6

    iget-object v7, v7, Lorg/apache/poi/hdf/extractor/TC;->_brcTop:[S

    const/4 v8, 0x1

    .line 1087
    const/16 v9, 0x8

    .line 1086
    invoke-static {p3, v9}, Lorg/apache/poi/hdf/extractor/Utils;->convertBytesToShort([BI)S

    move-result v9

    aput-short v9, v7, v8

    goto :goto_4

    .line 1093
    .end local v6    # "x":I
    :pswitch_a
    const/high16 v7, -0x1000000

    and-int/2addr v7, p2

    shr-int/lit8 v2, v7, 0x18

    .line 1094
    .local v2, "index":I
    const/high16 v7, 0xff0000

    and-int/2addr v7, p2

    shr-int/lit8 v1, v7, 0x10

    .line 1095
    .local v1, "count":I
    const v7, 0xffff

    and-int v5, p2, v7

    .line 1097
    .local v5, "width":I
    iget-short v7, p0, Lorg/apache/poi/hdf/extractor/TAP;->_itcMac:S

    add-int/2addr v7, v1

    add-int/lit8 v7, v7, 0x1

    new-array v3, v7, [S

    .line 1098
    .local v3, "rgdxaCenter":[S
    iget-short v7, p0, Lorg/apache/poi/hdf/extractor/TAP;->_itcMac:S

    add-int/2addr v7, v1

    new-array v4, v7, [Lorg/apache/poi/hdf/extractor/TC;

    .line 1099
    .local v4, "rgtc":[Lorg/apache/poi/hdf/extractor/TC;
    iget-short v7, p0, Lorg/apache/poi/hdf/extractor/TAP;->_itcMac:S

    if-lt v2, v7, :cond_7

    .line 1101
    iget-short v2, p0, Lorg/apache/poi/hdf/extractor/TAP;->_itcMac:S

    .line 1102
    iget-object v7, p0, Lorg/apache/poi/hdf/extractor/TAP;->_rgdxaCenter:[S

    const/4 v8, 0x0

    const/4 v9, 0x0

    iget-short v10, p0, Lorg/apache/poi/hdf/extractor/TAP;->_itcMac:S

    add-int/lit8 v10, v10, 0x1

    invoke-static {v7, v8, v3, v9, v10}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1103
    iget-object v7, p0, Lorg/apache/poi/hdf/extractor/TAP;->_rgtc:[Lorg/apache/poi/hdf/extractor/TC;

    const/4 v8, 0x0

    const/4 v9, 0x0

    iget-short v10, p0, Lorg/apache/poi/hdf/extractor/TAP;->_itcMac:S

    invoke-static {v7, v8, v4, v9, v10}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1115
    :goto_5
    move v6, v2

    .restart local v6    # "x":I
    :goto_6
    add-int v7, v2, v1

    if-lt v6, v7, :cond_8

    .line 1120
    add-int v7, v2, v1

    add-int v8, v2, v1

    add-int/lit8 v8, v8, -0x1

    aget-short v8, v3, v8

    add-int/2addr v8, v5

    int-to-short v8, v8

    aput-short v8, v3, v7

    goto/16 :goto_0

    .line 1108
    .end local v6    # "x":I
    :cond_7
    iget-object v7, p0, Lorg/apache/poi/hdf/extractor/TAP;->_rgdxaCenter:[S

    const/4 v8, 0x0

    const/4 v9, 0x0

    add-int/lit8 v10, v2, 0x1

    invoke-static {v7, v8, v3, v9, v10}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1109
    iget-object v7, p0, Lorg/apache/poi/hdf/extractor/TAP;->_rgdxaCenter:[S

    add-int/lit8 v8, v2, 0x1

    add-int v9, v2, v1

    iget-short v10, p0, Lorg/apache/poi/hdf/extractor/TAP;->_itcMac:S

    sub-int/2addr v10, v2

    invoke-static {v7, v8, v3, v9, v10}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1111
    iget-object v7, p0, Lorg/apache/poi/hdf/extractor/TAP;->_rgtc:[Lorg/apache/poi/hdf/extractor/TC;

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-static {v7, v8, v4, v9, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1112
    iget-object v7, p0, Lorg/apache/poi/hdf/extractor/TAP;->_rgtc:[Lorg/apache/poi/hdf/extractor/TC;

    add-int v8, v2, v1

    iget-short v9, p0, Lorg/apache/poi/hdf/extractor/TAP;->_itcMac:S

    sub-int/2addr v9, v2

    invoke-static {v7, v2, v4, v8, v9}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_5

    .line 1117
    .restart local v6    # "x":I
    :cond_8
    new-instance v7, Lorg/apache/poi/hdf/extractor/TC;

    invoke-direct {v7}, Lorg/apache/poi/hdf/extractor/TC;-><init>()V

    aput-object v7, v4, v6

    .line 1118
    add-int/lit8 v7, v6, -0x1

    aget-short v7, v3, v7

    add-int/2addr v7, v5

    int-to-short v7, v7

    aput-short v7, v3, v6

    .line 1115
    add-int/lit8 v6, v6, 0x1

    goto :goto_6

    .line 980
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_0
        :pswitch_7
        :pswitch_8
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_9
        :pswitch_a
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private static getCHPFlag(BZ)Z
    .locals 2
    .param p0, "x"    # B
    .param p1, "oldVal"    # Z

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1307
    sparse-switch p0, :sswitch_data_0

    .line 1318
    :cond_0
    :goto_0
    :sswitch_0
    return v0

    :sswitch_1
    move v0, v1

    .line 1312
    goto :goto_0

    :sswitch_2
    move v0, p1

    .line 1314
    goto :goto_0

    .line 1316
    :sswitch_3
    if-nez p1, :cond_0

    move v0, v1

    goto :goto_0

    .line 1307
    :sswitch_data_0
    .sparse-switch
        -0x80 -> :sswitch_2
        -0x7f -> :sswitch_3
        0x0 -> :sswitch_0
        0x1 -> :sswitch_1
    .end sparse-switch
.end method

.method public static getFlag(I)Z
    .locals 1
    .param p0, "x"    # I

    .prologue
    .line 1323
    if-eqz p0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static uncompressProperty([BLjava/lang/Object;Lorg/apache/poi/hdf/extractor/StyleSheet;)Ljava/lang/Object;
    .locals 1
    .param p0, "grpprl"    # [B
    .param p1, "parent"    # Ljava/lang/Object;
    .param p2, "styleSheet"    # Lorg/apache/poi/hdf/extractor/StyleSheet;

    .prologue
    .line 581
    const/4 v0, 0x1

    invoke-static {p0, p1, p2, v0}, Lorg/apache/poi/hdf/extractor/StyleSheet;->uncompressProperty([BLjava/lang/Object;Lorg/apache/poi/hdf/extractor/StyleSheet;Z)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static uncompressProperty([BLjava/lang/Object;Lorg/apache/poi/hdf/extractor/StyleSheet;Z)Ljava/lang/Object;
    .locals 24
    .param p0, "grpprl"    # [B
    .param p1, "parent"    # Ljava/lang/Object;
    .param p2, "styleSheet"    # Lorg/apache/poi/hdf/extractor/StyleSheet;
    .param p3, "doIstd"    # Z

    .prologue
    .line 587
    const/16 v18, 0x0

    .line 588
    .local v18, "newProperty":Ljava/lang/Object;
    const/4 v7, 0x0

    .line 589
    .local v7, "offset":I
    const/16 v20, 0x1

    .line 592
    .local v20, "propertyType":I
    move-object/from16 v0, p1

    instance-of v2, v0, Lorg/apache/poi/hdf/extractor/PAP;

    if-eqz v2, :cond_1

    .line 596
    :try_start_0
    move-object/from16 v0, p1

    check-cast v0, Lorg/apache/poi/hdf/extractor/PAP;

    move-object v2, v0

    invoke-virtual {v2}, Lorg/apache/poi/hdf/extractor/PAP;->clone()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v18

    .line 599
    .end local v18    # "newProperty":Ljava/lang/Object;
    :goto_0
    if-eqz p3, :cond_7

    move-object/from16 v2, v18

    .line 601
    check-cast v2, Lorg/apache/poi/hdf/extractor/PAP;

    const/4 v6, 0x0

    move-object/from16 v0, p0

    invoke-static {v0, v6}, Lorg/apache/poi/hdf/extractor/Utils;->convertBytesToShort([BI)S

    move-result v6

    iput v6, v2, Lorg/apache/poi/hdf/extractor/PAP;->_istd:I

    .line 603
    const/4 v7, 0x2

    move-object/from16 v23, v18

    .line 634
    :cond_0
    :goto_1
    move-object/from16 v0, p0

    array-length v2, v0

    if-lt v7, v2, :cond_5

    .line 720
    :goto_2
    return-object v23

    .line 606
    .restart local v18    # "newProperty":Ljava/lang/Object;
    :cond_1
    move-object/from16 v0, p1

    instance-of v2, v0, Lorg/apache/poi/hdf/extractor/CHP;

    if-eqz v2, :cond_2

    .line 610
    :try_start_1
    move-object/from16 v0, p1

    check-cast v0, Lorg/apache/poi/hdf/extractor/CHP;

    move-object v2, v0

    invoke-virtual {v2}, Lorg/apache/poi/hdf/extractor/CHP;->clone()Ljava/lang/Object;

    move-result-object v18

    .line 611
    move-object/from16 v0, v18

    check-cast v0, Lorg/apache/poi/hdf/extractor/CHP;

    move-object v2, v0

    move-object/from16 v0, p1

    check-cast v0, Lorg/apache/poi/hdf/extractor/CHP;

    move-object v6, v0

    iget v6, v6, Lorg/apache/poi/hdf/extractor/CHP;->_istd:I

    iput v6, v2, Lorg/apache/poi/hdf/extractor/CHP;->_baseIstd:I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 616
    .end local v18    # "newProperty":Ljava/lang/Object;
    :goto_3
    const/16 v20, 0x2

    move-object/from16 v23, v18

    .line 617
    goto :goto_1

    .line 613
    :catch_0
    move-exception v17

    .line 614
    .local v17, "e":Ljava/lang/Exception;
    const-string/jumbo v2, "DocumentService"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string/jumbo v9, "Exception: "

    invoke-direct {v6, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v2, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 618
    .end local v17    # "e":Ljava/lang/Exception;
    .restart local v18    # "newProperty":Ljava/lang/Object;
    :cond_2
    move-object/from16 v0, p1

    instance-of v2, v0, Lorg/apache/poi/hdf/extractor/SEP;

    if-eqz v2, :cond_3

    .line 620
    move-object/from16 v18, p1

    .line 621
    const/16 v20, 0x4

    move-object/from16 v23, v18

    .line 622
    goto :goto_1

    .line 623
    :cond_3
    move-object/from16 v0, p1

    instance-of v2, v0, Lorg/apache/poi/hdf/extractor/TAP;

    if-eqz v2, :cond_4

    .line 625
    move-object/from16 v18, p1

    .line 626
    const/16 v20, 0x5

    .line 627
    const/4 v7, 0x2

    move-object/from16 v23, v18

    .line 628
    goto :goto_1

    .line 631
    :cond_4
    const/16 v23, 0x0

    goto :goto_2

    .line 636
    .end local v18    # "newProperty":Ljava/lang/Object;
    :cond_5
    move-object/from16 v0, p0

    invoke-static {v0, v7}, Lorg/apache/poi/hdf/extractor/Utils;->convertBytesToShort([BI)S

    move-result v21

    .line 637
    .local v21, "sprm":S
    add-int/lit8 v7, v7, 0x2

    .line 639
    const v2, 0xe000

    and-int v2, v2, v21

    shr-int/lit8 v2, v2, 0xd

    int-to-byte v8, v2

    .line 640
    .local v8, "spra":B
    const/16 v19, 0x0

    .line 641
    .local v19, "opSize":I
    const/4 v4, 0x0

    .line 642
    .local v4, "param":I
    const/4 v5, 0x0

    .line 644
    .local v5, "varParam":[B
    packed-switch v8, :pswitch_data_0

    .line 686
    new-instance v2, Ljava/lang/RuntimeException;

    const-string/jumbo v6, "unrecognized pap opcode"

    invoke-direct {v2, v6}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 648
    :pswitch_0
    const/16 v19, 0x1

    .line 649
    aget-byte v4, p0, v7

    .line 689
    :goto_4
    add-int v7, v7, v19

    .line 690
    move/from16 v0, v21

    and-int/lit16 v2, v0, 0x1ff

    int-to-short v3, v2

    .line 691
    .local v3, "operand":S
    move/from16 v0, v21

    and-int/lit16 v2, v0, 0x1c00

    shr-int/lit8 v2, v2, 0xa

    int-to-byte v0, v2

    move/from16 v22, v0

    .line 692
    .local v22, "type":B
    packed-switch v20, :pswitch_data_1

    :pswitch_1
    goto/16 :goto_1

    .line 695
    :pswitch_2
    const/4 v2, 0x1

    move/from16 v0, v22

    if-ne v0, v2, :cond_0

    move-object/from16 v2, v23

    .line 697
    check-cast v2, Lorg/apache/poi/hdf/extractor/PAP;

    move-object/from16 v6, p0

    invoke-static/range {v2 .. v8}, Lorg/apache/poi/hdf/extractor/StyleSheet;->doPAPOperation(Lorg/apache/poi/hdf/extractor/PAP;II[B[BII)V

    goto/16 :goto_1

    .line 652
    .end local v3    # "operand":S
    .end local v22    # "type":B
    :pswitch_3
    const/16 v19, 0x2

    .line 653
    move-object/from16 v0, p0

    invoke-static {v0, v7}, Lorg/apache/poi/hdf/extractor/Utils;->convertBytesToShort([BI)S

    move-result v4

    .line 654
    goto :goto_4

    .line 656
    :pswitch_4
    const/16 v19, 0x4

    .line 657
    move-object/from16 v0, p0

    invoke-static {v0, v7}, Lorg/apache/poi/hdf/extractor/Utils;->convertBytesToInt([BI)I

    move-result v4

    .line 658
    goto :goto_4

    .line 661
    :pswitch_5
    const/16 v19, 0x2

    .line 662
    move-object/from16 v0, p0

    invoke-static {v0, v7}, Lorg/apache/poi/hdf/extractor/Utils;->convertBytesToShort([BI)S

    move-result v4

    .line 663
    goto :goto_4

    .line 667
    :pswitch_6
    const/16 v2, -0x29f8

    move/from16 v0, v21

    if-eq v0, v2, :cond_6

    .line 669
    aget-byte v2, p0, v7

    invoke-static {v2}, Lorg/apache/poi/hdf/extractor/Utils;->convertUnsignedByteToInt(B)I

    move-result v19

    .line 670
    add-int/lit8 v7, v7, 0x1

    .line 677
    :goto_5
    move/from16 v0, v19

    new-array v5, v0, [B

    .line 678
    const/4 v2, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-static {v0, v7, v5, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_4

    .line 674
    :cond_6
    move-object/from16 v0, p0

    invoke-static {v0, v7}, Lorg/apache/poi/hdf/extractor/Utils;->convertBytesToShort([BI)S

    move-result v2

    add-int/lit8 v19, v2, -0x1

    .line 675
    add-int/lit8 v7, v7, 0x2

    goto :goto_5

    .line 682
    :pswitch_7
    const/16 v19, 0x3

    .line 683
    const/4 v2, 0x0

    add-int/lit8 v6, v7, 0x2

    aget-byte v6, p0, v6

    add-int/lit8 v9, v7, 0x1

    aget-byte v9, p0, v9

    aget-byte v10, p0, v7

    invoke-static {v2, v6, v9, v10}, Lorg/apache/poi/hdf/extractor/Utils;->convertBytesToInt(BBBB)I

    move-result v4

    .line 684
    goto :goto_4

    .restart local v3    # "operand":S
    .restart local v22    # "type":B
    :pswitch_8
    move-object/from16 v9, p1

    .line 703
    check-cast v9, Lorg/apache/poi/hdf/extractor/CHP;

    move-object/from16 v10, v23

    check-cast v10, Lorg/apache/poi/hdf/extractor/CHP;

    move v11, v3

    move v12, v4

    move-object v13, v5

    move-object/from16 v14, p0

    move v15, v7

    move-object/from16 v16, p2

    invoke-static/range {v9 .. v16}, Lorg/apache/poi/hdf/extractor/StyleSheet;->doCHPOperation(Lorg/apache/poi/hdf/extractor/CHP;Lorg/apache/poi/hdf/extractor/CHP;II[B[BILorg/apache/poi/hdf/extractor/StyleSheet;)V

    goto/16 :goto_1

    :pswitch_9
    move-object/from16 v2, v23

    .line 708
    check-cast v2, Lorg/apache/poi/hdf/extractor/SEP;

    invoke-static {v2, v3, v4, v5}, Lorg/apache/poi/hdf/extractor/StyleSheet;->doSEPOperation(Lorg/apache/poi/hdf/extractor/SEP;II[B)V

    goto/16 :goto_1

    .line 711
    :pswitch_a
    const/4 v2, 0x5

    move/from16 v0, v22

    if-ne v0, v2, :cond_0

    move-object/from16 v2, v23

    .line 713
    check-cast v2, Lorg/apache/poi/hdf/extractor/TAP;

    invoke-static {v2, v3, v4, v5}, Lorg/apache/poi/hdf/extractor/StyleSheet;->doTAPOperation(Lorg/apache/poi/hdf/extractor/TAP;II[B)V

    goto/16 :goto_1

    .line 598
    .end local v3    # "operand":S
    .end local v4    # "param":I
    .end local v5    # "varParam":[B
    .end local v8    # "spra":B
    .end local v19    # "opSize":I
    .end local v21    # "sprm":S
    .end local v22    # "type":B
    .restart local v18    # "newProperty":Ljava/lang/Object;
    :catch_1
    move-exception v2

    goto/16 :goto_0

    .end local v18    # "newProperty":Ljava/lang/Object;
    :cond_7
    move-object/from16 v23, v18

    goto/16 :goto_1

    .line 644
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch

    .line 692
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_8
        :pswitch_1
        :pswitch_9
        :pswitch_a
    .end packed-switch
.end method


# virtual methods
.method public getStyleDescription(I)Lorg/apache/poi/hdf/extractor/StyleDescription;
    .locals 1
    .param p1, "x"    # I

    .prologue
    .line 133
    iget-object v0, p0, Lorg/apache/poi/hdf/extractor/StyleSheet;->_styleDescriptions:[Lorg/apache/poi/hdf/extractor/StyleDescription;

    aget-object v0, v0, p1

    return-object v0
.end method
