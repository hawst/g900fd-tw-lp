.class public final Lorg/apache/poi/hdf/extractor/util/BTreeSet;
.super Ljava/util/AbstractSet;
.source "BTreeSet.java"

# interfaces
.implements Ljava/util/Set;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTIterator;,
        Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;,
        Lorg/apache/poi/hdf/extractor/util/BTreeSet$Entry;
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private comparator:Ljava/util/Comparator;

.field order:I

.field public root:Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;

.field size:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 56
    const/4 v0, 0x6

    invoke-direct {p0, v0}, Lorg/apache/poi/hdf/extractor/util/BTreeSet;-><init>(I)V

    .line 57
    return-void
.end method

.method public constructor <init>(I)V
    .locals 1
    .param p1, "order"    # I

    .prologue
    .line 65
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lorg/apache/poi/hdf/extractor/util/BTreeSet;-><init>(ILjava/util/Comparator;)V

    .line 66
    return-void
.end method

.method public constructor <init>(ILjava/util/Comparator;)V
    .locals 2
    .param p1, "order"    # I
    .param p2, "comparator"    # Ljava/util/Comparator;

    .prologue
    const/4 v1, 0x0

    .line 68
    invoke-direct {p0}, Ljava/util/AbstractSet;-><init>()V

    .line 44
    iput-object v1, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet;->comparator:Ljava/util/Comparator;

    .line 46
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet;->size:I

    .line 69
    iput p1, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet;->order:I

    .line 70
    iput-object p2, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet;->comparator:Ljava/util/Comparator;

    .line 71
    new-instance v0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;

    invoke-direct {v0, p0, v1}, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;-><init>(Lorg/apache/poi/hdf/extractor/util/BTreeSet;Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;)V

    iput-object v0, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet;->root:Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;

    .line 72
    return-void
.end method

.method public constructor <init>(Ljava/util/Collection;)V
    .locals 1
    .param p1, "c"    # Ljava/util/Collection;

    .prologue
    .line 60
    const/4 v0, 0x6

    invoke-direct {p0, v0}, Lorg/apache/poi/hdf/extractor/util/BTreeSet;-><init>(I)V

    .line 61
    invoke-virtual {p0, p1}, Lorg/apache/poi/hdf/extractor/util/BTreeSet;->addAll(Ljava/util/Collection;)Z

    .line 62
    return-void
.end method


# virtual methods
.method public add(Ljava/lang/Object;)Z
    .locals 2
    .param p1, "x"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 79
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 80
    :cond_0
    iget-object v0, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet;->root:Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;

    const/4 v1, -0x1

    invoke-virtual {v0, p1, v1}, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->insert(Ljava/lang/Object;I)Z

    move-result v0

    return v0
.end method

.method public clear()V
    .locals 2

    .prologue
    .line 97
    new-instance v0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;-><init>(Lorg/apache/poi/hdf/extractor/util/BTreeSet;Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;)V

    iput-object v0, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet;->root:Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;

    .line 98
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet;->size:I

    .line 99
    return-void
.end method

.method compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .param p1, "x"    # Ljava/lang/Object;
    .param p2, "y"    # Ljava/lang/Object;

    .prologue
    .line 110
    iget-object v0, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet;->comparator:Ljava/util/Comparator;

    if-nez v0, :cond_0

    check-cast p1, Ljava/lang/Comparable;

    .end local p1    # "x":Ljava/lang/Object;
    invoke-interface {p1, p2}, Ljava/lang/Comparable;->compareTo(Ljava/lang/Object;)I

    move-result v0

    :goto_0
    return v0

    .restart local p1    # "x":Ljava/lang/Object;
    :cond_0
    iget-object v0, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet;->comparator:Ljava/util/Comparator;

    invoke-interface {v0, p1, p2}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    goto :goto_0
.end method

.method public contains(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "x"    # Ljava/lang/Object;

    .prologue
    .line 84
    iget-object v0, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet;->root:Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;

    invoke-virtual {v0, p1}, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->includes(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 1

    .prologue
    .line 102
    new-instance v0, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTIterator;

    invoke-direct {v0, p0}, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTIterator;-><init>(Lorg/apache/poi/hdf/extractor/util/BTreeSet;)V

    return-object v0
.end method

.method public remove(Ljava/lang/Object;)Z
    .locals 2
    .param p1, "x"    # Ljava/lang/Object;

    .prologue
    .line 88
    if-nez p1, :cond_0

    const/4 v0, 0x0

    .line 89
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet;->root:Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;

    const/4 v1, -0x1

    invoke-virtual {v0, p1, v1}, Lorg/apache/poi/hdf/extractor/util/BTreeSet$BTreeNode;->delete(Ljava/lang/Object;I)Z

    move-result v0

    goto :goto_0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 93
    iget v0, p0, Lorg/apache/poi/hdf/extractor/util/BTreeSet;->size:I

    return v0
.end method
