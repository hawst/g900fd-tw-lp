.class public final Lorg/apache/poi/hdf/model/hdftypes/ListTables;
.super Ljava/lang/Object;
.source "ListTables.java"

# interfaces
.implements Lorg/apache/poi/hdf/model/hdftypes/HDFType;


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field _lists:Ljava/util/Hashtable;

.field _pllfo:[Lorg/apache/poi/hdf/model/hdftypes/LFO;


# direct methods
.method public constructor <init>([B[B)V
    .locals 1
    .param p1, "plcflst"    # [B
    .param p2, "plflfo"    # [B

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/hdf/model/hdftypes/ListTables;->_lists:Ljava/util/Hashtable;

    .line 39
    invoke-direct {p0, p1}, Lorg/apache/poi/hdf/model/hdftypes/ListTables;->initLST([B)V

    .line 40
    invoke-direct {p0, p2}, Lorg/apache/poi/hdf/model/hdftypes/ListTables;->initLFO([B)V

    .line 41
    return-void
.end method

.method private createLVL([BILorg/apache/poi/hdf/model/hdftypes/LVL;)I
    .locals 10
    .param p1, "data"    # [B
    .param p2, "offset"    # I
    .param p3, "lvl"    # Lorg/apache/poi/hdf/model/hdftypes/LVL;

    .prologue
    const/4 v9, 0x0

    .line 163
    move v4, p2

    .line 164
    .local v4, "startingOffset":I
    invoke-static {p1, p2}, Lorg/apache/poi/hdf/extractor/Utils;->convertBytesToInt([BI)I

    move-result v7

    iput v7, p3, Lorg/apache/poi/hdf/model/hdftypes/LVL;->_iStartAt:I

    .line 165
    add-int/lit8 p2, p2, 0x4

    .line 166
    add-int/lit8 v2, p2, 0x1

    .end local p2    # "offset":I
    .local v2, "offset":I
    aget-byte v7, p1, p2

    iput-byte v7, p3, Lorg/apache/poi/hdf/model/hdftypes/LVL;->_nfc:B

    .line 167
    add-int/lit8 p2, v2, 0x1

    .end local v2    # "offset":I
    .restart local p2    # "offset":I
    aget-byte v1, p1, v2

    .line 168
    .local v1, "code":B
    and-int/lit8 v7, v1, 0x3

    int-to-byte v7, v7

    iput-byte v7, p3, Lorg/apache/poi/hdf/model/hdftypes/LVL;->_jc:B

    .line 169
    and-int/lit8 v7, v1, 0x4

    invoke-static {v7}, Lorg/apache/poi/hdf/model/hdftypes/StyleSheet;->getFlag(I)Z

    move-result v7

    iput-boolean v7, p3, Lorg/apache/poi/hdf/model/hdftypes/LVL;->_fLegal:Z

    .line 170
    and-int/lit8 v7, v1, 0x8

    invoke-static {v7}, Lorg/apache/poi/hdf/model/hdftypes/StyleSheet;->getFlag(I)Z

    move-result v7

    iput-boolean v7, p3, Lorg/apache/poi/hdf/model/hdftypes/LVL;->_fNoRestart:Z

    .line 171
    and-int/lit8 v7, v1, 0x10

    invoke-static {v7}, Lorg/apache/poi/hdf/model/hdftypes/StyleSheet;->getFlag(I)Z

    move-result v7

    iput-boolean v7, p3, Lorg/apache/poi/hdf/model/hdftypes/LVL;->_fPrev:Z

    .line 172
    and-int/lit8 v7, v1, 0x20

    invoke-static {v7}, Lorg/apache/poi/hdf/model/hdftypes/StyleSheet;->getFlag(I)Z

    move-result v7

    iput-boolean v7, p3, Lorg/apache/poi/hdf/model/hdftypes/LVL;->_fPrevSpace:Z

    .line 173
    and-int/lit8 v7, v1, 0x40

    invoke-static {v7}, Lorg/apache/poi/hdf/model/hdftypes/StyleSheet;->getFlag(I)Z

    move-result v7

    iput-boolean v7, p3, Lorg/apache/poi/hdf/model/hdftypes/LVL;->_fWord6:Z

    .line 177
    iget-object v7, p3, Lorg/apache/poi/hdf/model/hdftypes/LVL;->_rgbxchNums:[B

    const/16 v8, 0x9

    invoke-static {p1, p2, v7, v9, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 178
    add-int/lit8 p2, p2, 0x9

    .line 180
    add-int/lit8 v2, p2, 0x1

    .end local p2    # "offset":I
    .restart local v2    # "offset":I
    aget-byte v7, p1, p2

    iput-byte v7, p3, Lorg/apache/poi/hdf/model/hdftypes/LVL;->_ixchFollow:B

    .line 182
    iget-boolean v7, p3, Lorg/apache/poi/hdf/model/hdftypes/LVL;->_fWord6:Z

    if-eqz v7, :cond_0

    .line 184
    invoke-static {p1, v2}, Lorg/apache/poi/hdf/extractor/Utils;->convertBytesToInt([BI)I

    move-result v7

    iput v7, p3, Lorg/apache/poi/hdf/model/hdftypes/LVL;->_dxaSpace:I

    .line 185
    add-int/lit8 v7, v2, 0x4

    invoke-static {p1, v7}, Lorg/apache/poi/hdf/extractor/Utils;->convertBytesToInt([BI)I

    move-result v7

    iput v7, p3, Lorg/apache/poi/hdf/model/hdftypes/LVL;->_dxaIndent:I

    .line 187
    :cond_0
    add-int/lit8 p2, v2, 0x8

    .line 189
    .end local v2    # "offset":I
    .restart local p2    # "offset":I
    add-int/lit8 v2, p2, 0x1

    .end local p2    # "offset":I
    .restart local v2    # "offset":I
    aget-byte v0, p1, p2

    .line 190
    .local v0, "chpxSize":I
    add-int/lit8 p2, v2, 0x1

    .end local v2    # "offset":I
    .restart local p2    # "offset":I
    aget-byte v3, p1, v2

    .line 191
    .local v3, "papxSize":I
    new-array v7, v0, [B

    iput-object v7, p3, Lorg/apache/poi/hdf/model/hdftypes/LVL;->_chpx:[B

    .line 192
    new-array v7, v3, [B

    iput-object v7, p3, Lorg/apache/poi/hdf/model/hdftypes/LVL;->_papx:[B

    .line 194
    iget-object v7, p3, Lorg/apache/poi/hdf/model/hdftypes/LVL;->_chpx:[B

    invoke-static {p1, p2, v7, v9, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 195
    add-int v7, p2, v0

    iget-object v8, p3, Lorg/apache/poi/hdf/model/hdftypes/LVL;->_papx:[B

    invoke-static {p1, v7, v8, v9, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 197
    add-int v7, v3, v0

    add-int/lit8 v7, v7, 0x2

    add-int/2addr p2, v7

    .line 198
    invoke-static {p1, p2}, Lorg/apache/poi/hdf/extractor/Utils;->convertBytesToShort([BI)S

    move-result v6

    .line 199
    .local v6, "xstSize":I
    add-int/lit8 p2, p2, 0x2

    .line 200
    new-array v7, v6, [C

    iput-object v7, p3, Lorg/apache/poi/hdf/model/hdftypes/LVL;->_xst:[C

    .line 202
    const/4 v5, 0x0

    .local v5, "x":I
    :goto_0
    if-lt v5, v6, :cond_1

    .line 206
    mul-int/lit8 v7, v6, 0x2

    add-int/2addr v7, p2

    sub-int/2addr v7, v4

    return v7

    .line 204
    :cond_1
    iget-object v7, p3, Lorg/apache/poi/hdf/model/hdftypes/LVL;->_xst:[C

    mul-int/lit8 v8, v5, 0x2

    add-int/2addr v8, p2

    invoke-static {p1, v8}, Lorg/apache/poi/hdf/extractor/Utils;->convertBytesToShort([BI)S

    move-result v8

    int-to-char v8, v8

    aput-char v8, v7, v5

    .line 202
    add-int/lit8 v5, v5, 0x1

    goto :goto_0
.end method

.method private initLFO([B)V
    .locals 10
    .param p1, "plflfo"    # [B

    .prologue
    .line 115
    const/4 v9, 0x0

    invoke-static {p1, v9}, Lorg/apache/poi/hdf/extractor/Utils;->convertBytesToInt([BI)I

    move-result v0

    .line 116
    .local v0, "lfoSize":I
    new-array v9, v0, [Lorg/apache/poi/hdf/model/hdftypes/LFO;

    iput-object v9, p0, Lorg/apache/poi/hdf/model/hdftypes/ListTables;->_pllfo:[Lorg/apache/poi/hdf/model/hdftypes/LFO;

    .line 117
    const/4 v7, 0x0

    .local v7, "x":I
    :goto_0
    if-lt v7, v0, :cond_0

    .line 126
    mul-int/lit8 v9, v0, 0x10

    add-int/lit8 v3, v9, 0x4

    .line 127
    .local v3, "lfolvlOffset":I
    const/4 v4, 0x0

    .line 128
    .local v4, "lvlOffset":I
    const/4 v2, 0x0

    .line 129
    .local v2, "lfolvlNum":I
    const/4 v7, 0x0

    :goto_1
    if-lt v7, v0, :cond_1

    .line 160
    return-void

    .line 119
    .end local v2    # "lfolvlNum":I
    .end local v3    # "lfolvlOffset":I
    .end local v4    # "lvlOffset":I
    :cond_0
    new-instance v5, Lorg/apache/poi/hdf/model/hdftypes/LFO;

    invoke-direct {v5}, Lorg/apache/poi/hdf/model/hdftypes/LFO;-><init>()V

    .line 120
    .local v5, "nextLFO":Lorg/apache/poi/hdf/model/hdftypes/LFO;
    mul-int/lit8 v9, v7, 0x10

    add-int/lit8 v9, v9, 0x4

    invoke-static {p1, v9}, Lorg/apache/poi/hdf/extractor/Utils;->convertBytesToInt([BI)I

    move-result v9

    iput v9, v5, Lorg/apache/poi/hdf/model/hdftypes/LFO;->_lsid:I

    .line 121
    mul-int/lit8 v9, v7, 0x10

    add-int/lit8 v9, v9, 0x10

    aget-byte v9, p1, v9

    iput v9, v5, Lorg/apache/poi/hdf/model/hdftypes/LFO;->_clfolvl:I

    .line 122
    iget v9, v5, Lorg/apache/poi/hdf/model/hdftypes/LFO;->_clfolvl:I

    new-array v9, v9, [Lorg/apache/poi/hdf/model/hdftypes/LFOLVL;

    iput-object v9, v5, Lorg/apache/poi/hdf/model/hdftypes/LFO;->_levels:[Lorg/apache/poi/hdf/model/hdftypes/LFOLVL;

    .line 123
    iget-object v9, p0, Lorg/apache/poi/hdf/model/hdftypes/ListTables;->_pllfo:[Lorg/apache/poi/hdf/model/hdftypes/LFO;

    aput-object v5, v9, v7

    .line 117
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 131
    .end local v5    # "nextLFO":Lorg/apache/poi/hdf/model/hdftypes/LFO;
    .restart local v2    # "lfolvlNum":I
    .restart local v3    # "lfolvlOffset":I
    .restart local v4    # "lvlOffset":I
    :cond_1
    iget-object v9, p0, Lorg/apache/poi/hdf/model/hdftypes/ListTables;->_pllfo:[Lorg/apache/poi/hdf/model/hdftypes/LFO;

    aget-object v9, v9, v7

    iget v9, v9, Lorg/apache/poi/hdf/model/hdftypes/LFO;->_clfolvl:I

    if-nez v9, :cond_3

    .line 135
    add-int/lit8 v2, v2, 0x1

    .line 129
    :cond_2
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 138
    :cond_3
    const/4 v8, 0x0

    .local v8, "y":I
    :goto_2
    iget-object v9, p0, Lorg/apache/poi/hdf/model/hdftypes/ListTables;->_pllfo:[Lorg/apache/poi/hdf/model/hdftypes/LFO;

    aget-object v9, v9, v7

    iget v9, v9, Lorg/apache/poi/hdf/model/hdftypes/LFO;->_clfolvl:I

    if-ge v8, v9, :cond_2

    .line 140
    mul-int/lit8 v9, v2, 0x8

    add-int/2addr v9, v3

    add-int v6, v9, v4

    .line 141
    .local v6, "offset":I
    new-instance v1, Lorg/apache/poi/hdf/model/hdftypes/LFOLVL;

    invoke-direct {v1}, Lorg/apache/poi/hdf/model/hdftypes/LFOLVL;-><init>()V

    .line 142
    .local v1, "lfolvl":Lorg/apache/poi/hdf/model/hdftypes/LFOLVL;
    invoke-static {p1, v6}, Lorg/apache/poi/hdf/extractor/Utils;->convertBytesToInt([BI)I

    move-result v9

    iput v9, v1, Lorg/apache/poi/hdf/model/hdftypes/LFOLVL;->_iStartAt:I

    .line 143
    add-int/lit8 v9, v6, 0x4

    invoke-static {p1, v9}, Lorg/apache/poi/hdf/extractor/Utils;->convertBytesToInt([BI)I

    move-result v9

    iput v9, v1, Lorg/apache/poi/hdf/model/hdftypes/LFOLVL;->_ilvl:I

    .line 144
    iget v9, v1, Lorg/apache/poi/hdf/model/hdftypes/LFOLVL;->_ilvl:I

    and-int/lit8 v9, v9, 0x10

    invoke-static {v9}, Lorg/apache/poi/hdf/model/hdftypes/StyleSheet;->getFlag(I)Z

    move-result v9

    iput-boolean v9, v1, Lorg/apache/poi/hdf/model/hdftypes/LFOLVL;->_fStartAt:Z

    .line 145
    iget v9, v1, Lorg/apache/poi/hdf/model/hdftypes/LFOLVL;->_ilvl:I

    and-int/lit8 v9, v9, 0x20

    invoke-static {v9}, Lorg/apache/poi/hdf/model/hdftypes/StyleSheet;->getFlag(I)Z

    move-result v9

    iput-boolean v9, v1, Lorg/apache/poi/hdf/model/hdftypes/LFOLVL;->_fFormatting:Z

    .line 146
    iget v9, v1, Lorg/apache/poi/hdf/model/hdftypes/LFOLVL;->_ilvl:I

    and-int/lit8 v9, v9, 0xf

    iput v9, v1, Lorg/apache/poi/hdf/model/hdftypes/LFOLVL;->_ilvl:I

    .line 147
    add-int/lit8 v2, v2, 0x1

    .line 149
    iget-boolean v9, v1, Lorg/apache/poi/hdf/model/hdftypes/LFOLVL;->_fFormatting:Z

    if-eqz v9, :cond_4

    .line 152
    mul-int/lit8 v9, v2, 0x8

    add-int/2addr v9, v3

    add-int v6, v9, v4

    .line 153
    new-instance v9, Lorg/apache/poi/hdf/model/hdftypes/LVL;

    invoke-direct {v9}, Lorg/apache/poi/hdf/model/hdftypes/LVL;-><init>()V

    iput-object v9, v1, Lorg/apache/poi/hdf/model/hdftypes/LFOLVL;->_override:Lorg/apache/poi/hdf/model/hdftypes/LVL;

    .line 154
    iget-object v9, v1, Lorg/apache/poi/hdf/model/hdftypes/LFOLVL;->_override:Lorg/apache/poi/hdf/model/hdftypes/LVL;

    invoke-direct {p0, p1, v6, v9}, Lorg/apache/poi/hdf/model/hdftypes/ListTables;->createLVL([BILorg/apache/poi/hdf/model/hdftypes/LVL;)I

    move-result v9

    add-int/2addr v4, v9

    .line 156
    :cond_4
    iget-object v9, p0, Lorg/apache/poi/hdf/model/hdftypes/ListTables;->_pllfo:[Lorg/apache/poi/hdf/model/hdftypes/LFO;

    aget-object v9, v9, v7

    iget-object v9, v9, Lorg/apache/poi/hdf/model/hdftypes/LFO;->_levels:[Lorg/apache/poi/hdf/model/hdftypes/LFOLVL;

    aput-object v1, v9, v8

    .line 138
    add-int/lit8 v8, v8, 0x1

    goto :goto_2
.end method

.method private initLST([B)V
    .locals 11
    .param p1, "plcflst"    # [B

    .prologue
    const/4 v10, 0x0

    .line 80
    invoke-static {p1, v10}, Lorg/apache/poi/hdf/extractor/Utils;->convertBytesToShort([BI)S

    move-result v1

    .line 81
    .local v1, "length":S
    const/4 v3, 0x0

    .line 83
    .local v3, "nextLevelOffset":I
    const/4 v5, 0x0

    .local v5, "x":I
    :goto_0
    if-lt v5, v1, :cond_0

    .line 112
    return-void

    .line 85
    :cond_0
    new-instance v2, Lorg/apache/poi/hdf/model/hdftypes/LST;

    invoke-direct {v2}, Lorg/apache/poi/hdf/model/hdftypes/LST;-><init>()V

    .line 86
    .local v2, "lst":Lorg/apache/poi/hdf/model/hdftypes/LST;
    mul-int/lit8 v7, v5, 0x1c

    add-int/lit8 v7, v7, 0x2

    invoke-static {p1, v7}, Lorg/apache/poi/hdf/extractor/Utils;->convertBytesToInt([BI)I

    move-result v7

    iput v7, v2, Lorg/apache/poi/hdf/model/hdftypes/LST;->_lsid:I

    .line 87
    mul-int/lit8 v7, v5, 0x1c

    add-int/lit8 v7, v7, 0x6

    invoke-static {p1, v7}, Lorg/apache/poi/hdf/extractor/Utils;->convertBytesToInt([BI)I

    move-result v7

    iput v7, v2, Lorg/apache/poi/hdf/model/hdftypes/LST;->_tplc:I

    .line 88
    mul-int/lit8 v7, v5, 0x1c

    add-int/lit8 v7, v7, 0xa

    iget-object v8, v2, Lorg/apache/poi/hdf/model/hdftypes/LST;->_rgistd:[B

    const/16 v9, 0x12

    invoke-static {p1, v7, v8, v10, v9}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 89
    mul-int/lit8 v7, v5, 0x1c

    add-int/lit8 v7, v7, 0x1c

    aget-byte v0, p1, v7

    .line 90
    .local v0, "code":B
    and-int/lit8 v7, v0, 0x1

    invoke-static {v7}, Lorg/apache/poi/hdf/model/hdftypes/StyleSheet;->getFlag(I)Z

    move-result v7

    iput-boolean v7, v2, Lorg/apache/poi/hdf/model/hdftypes/LST;->_fSimpleList:Z

    .line 92
    iget-object v7, p0, Lorg/apache/poi/hdf/model/hdftypes/ListTables;->_lists:Ljava/util/Hashtable;

    iget v8, v2, Lorg/apache/poi/hdf/model/hdftypes/LST;->_lsid:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v7, v8, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 94
    iget-boolean v7, v2, Lorg/apache/poi/hdf/model/hdftypes/LST;->_fSimpleList:Z

    if-eqz v7, :cond_1

    .line 96
    const/4 v7, 0x1

    new-array v7, v7, [Lorg/apache/poi/hdf/model/hdftypes/LVL;

    iput-object v7, v2, Lorg/apache/poi/hdf/model/hdftypes/LST;->_levels:[Lorg/apache/poi/hdf/model/hdftypes/LVL;

    .line 103
    :goto_1
    const/4 v6, 0x0

    .local v6, "y":I
    :goto_2
    iget-object v7, v2, Lorg/apache/poi/hdf/model/hdftypes/LST;->_levels:[Lorg/apache/poi/hdf/model/hdftypes/LVL;

    array-length v7, v7

    if-lt v6, v7, :cond_2

    .line 83
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 100
    .end local v6    # "y":I
    :cond_1
    const/16 v7, 0x9

    new-array v7, v7, [Lorg/apache/poi/hdf/model/hdftypes/LVL;

    iput-object v7, v2, Lorg/apache/poi/hdf/model/hdftypes/LST;->_levels:[Lorg/apache/poi/hdf/model/hdftypes/LVL;

    goto :goto_1

    .line 105
    .restart local v6    # "y":I
    :cond_2
    mul-int/lit8 v7, v1, 0x1c

    add-int/lit8 v7, v7, 0x2

    add-int v4, v7, v3

    .line 106
    .local v4, "offset":I
    iget-object v7, v2, Lorg/apache/poi/hdf/model/hdftypes/LST;->_levels:[Lorg/apache/poi/hdf/model/hdftypes/LVL;

    new-instance v8, Lorg/apache/poi/hdf/model/hdftypes/LVL;

    invoke-direct {v8}, Lorg/apache/poi/hdf/model/hdftypes/LVL;-><init>()V

    aput-object v8, v7, v6

    .line 107
    iget-object v7, v2, Lorg/apache/poi/hdf/model/hdftypes/LST;->_levels:[Lorg/apache/poi/hdf/model/hdftypes/LVL;

    aget-object v7, v7, v6

    invoke-direct {p0, p1, v4, v7}, Lorg/apache/poi/hdf/model/hdftypes/ListTables;->createLVL([BILorg/apache/poi/hdf/model/hdftypes/LVL;)I

    move-result v7

    add-int/2addr v3, v7

    .line 103
    add-int/lit8 v6, v6, 0x1

    goto :goto_2
.end method


# virtual methods
.method public getLevel(II)Lorg/apache/poi/hdf/model/hdftypes/LVL;
    .locals 8
    .param p1, "list"    # I
    .param p2, "level"    # I

    .prologue
    .line 45
    iget-object v6, p0, Lorg/apache/poi/hdf/model/hdftypes/ListTables;->_pllfo:[Lorg/apache/poi/hdf/model/hdftypes/LFO;

    add-int/lit8 v7, p1, -0x1

    aget-object v4, v6, v7

    .line 47
    .local v4, "override":Lorg/apache/poi/hdf/model/hdftypes/LFO;
    const/4 v5, 0x0

    .local v5, "x":I
    :goto_0
    iget v6, v4, Lorg/apache/poi/hdf/model/hdftypes/LFO;->_clfolvl:I

    if-lt v5, v6, :cond_0

    .line 71
    iget-object v6, p0, Lorg/apache/poi/hdf/model/hdftypes/ListTables;->_lists:Ljava/util/Hashtable;

    iget v7, v4, Lorg/apache/poi/hdf/model/hdftypes/LFO;->_lsid:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/hdf/model/hdftypes/LST;

    .line 72
    .local v1, "lst":Lorg/apache/poi/hdf/model/hdftypes/LST;
    iget-object v6, v1, Lorg/apache/poi/hdf/model/hdftypes/LST;->_levels:[Lorg/apache/poi/hdf/model/hdftypes/LVL;

    aget-object v2, v6, p2

    .line 73
    .local v2, "lvl":Lorg/apache/poi/hdf/model/hdftypes/LVL;
    iget-object v6, v1, Lorg/apache/poi/hdf/model/hdftypes/LST;->_rgistd:[B

    mul-int/lit8 v7, p2, 0x2

    invoke-static {v6, v7}, Lorg/apache/poi/hdf/extractor/Utils;->convertBytesToShort([BI)S

    move-result v6

    iput-short v6, v2, Lorg/apache/poi/hdf/model/hdftypes/LVL;->_istd:S

    .line 74
    .end local v2    # "lvl":Lorg/apache/poi/hdf/model/hdftypes/LVL;
    :goto_1
    return-object v2

    .line 49
    .end local v1    # "lst":Lorg/apache/poi/hdf/model/hdftypes/LST;
    :cond_0
    iget-object v6, v4, Lorg/apache/poi/hdf/model/hdftypes/LFO;->_levels:[Lorg/apache/poi/hdf/model/hdftypes/LFOLVL;

    aget-object v6, v6, v5

    iget v6, v6, Lorg/apache/poi/hdf/model/hdftypes/LFOLVL;->_ilvl:I

    if-ne v6, p2, :cond_2

    .line 51
    iget-object v6, v4, Lorg/apache/poi/hdf/model/hdftypes/LFO;->_levels:[Lorg/apache/poi/hdf/model/hdftypes/LFOLVL;

    aget-object v0, v6, v5

    .line 52
    .local v0, "lfolvl":Lorg/apache/poi/hdf/model/hdftypes/LFOLVL;
    iget-boolean v6, v0, Lorg/apache/poi/hdf/model/hdftypes/LFOLVL;->_fFormatting:Z

    if-eqz v6, :cond_1

    .line 54
    iget-object v6, p0, Lorg/apache/poi/hdf/model/hdftypes/ListTables;->_lists:Ljava/util/Hashtable;

    iget v7, v4, Lorg/apache/poi/hdf/model/hdftypes/LFO;->_lsid:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/hdf/model/hdftypes/LST;

    .line 55
    .restart local v1    # "lst":Lorg/apache/poi/hdf/model/hdftypes/LST;
    iget-object v2, v0, Lorg/apache/poi/hdf/model/hdftypes/LFOLVL;->_override:Lorg/apache/poi/hdf/model/hdftypes/LVL;

    .line 56
    .restart local v2    # "lvl":Lorg/apache/poi/hdf/model/hdftypes/LVL;
    iget-object v6, v1, Lorg/apache/poi/hdf/model/hdftypes/LST;->_rgistd:[B

    mul-int/lit8 v7, p2, 0x2

    invoke-static {v6, v7}, Lorg/apache/poi/hdf/extractor/Utils;->convertBytesToShort([BI)S

    move-result v6

    iput-short v6, v2, Lorg/apache/poi/hdf/model/hdftypes/LVL;->_istd:S

    goto :goto_1

    .line 59
    .end local v1    # "lst":Lorg/apache/poi/hdf/model/hdftypes/LST;
    .end local v2    # "lvl":Lorg/apache/poi/hdf/model/hdftypes/LVL;
    :cond_1
    iget-boolean v6, v0, Lorg/apache/poi/hdf/model/hdftypes/LFOLVL;->_fStartAt:Z

    if-eqz v6, :cond_2

    .line 61
    iget-object v6, p0, Lorg/apache/poi/hdf/model/hdftypes/ListTables;->_lists:Ljava/util/Hashtable;

    iget v7, v4, Lorg/apache/poi/hdf/model/hdftypes/LFO;->_lsid:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/hdf/model/hdftypes/LST;

    .line 62
    .restart local v1    # "lst":Lorg/apache/poi/hdf/model/hdftypes/LST;
    iget-object v6, v1, Lorg/apache/poi/hdf/model/hdftypes/LST;->_levels:[Lorg/apache/poi/hdf/model/hdftypes/LVL;

    aget-object v2, v6, p2

    .line 63
    .restart local v2    # "lvl":Lorg/apache/poi/hdf/model/hdftypes/LVL;
    invoke-virtual {v2}, Lorg/apache/poi/hdf/model/hdftypes/LVL;->clone()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/poi/hdf/model/hdftypes/LVL;

    .line 64
    .local v3, "newLvl":Lorg/apache/poi/hdf/model/hdftypes/LVL;
    iget-object v6, v1, Lorg/apache/poi/hdf/model/hdftypes/LST;->_rgistd:[B

    mul-int/lit8 v7, p2, 0x2

    invoke-static {v6, v7}, Lorg/apache/poi/hdf/extractor/Utils;->convertBytesToShort([BI)S

    move-result v6

    iput-short v6, v3, Lorg/apache/poi/hdf/model/hdftypes/LVL;->_istd:S

    .line 65
    iget v6, v0, Lorg/apache/poi/hdf/model/hdftypes/LFOLVL;->_iStartAt:I

    iput v6, v3, Lorg/apache/poi/hdf/model/hdftypes/LVL;->_iStartAt:I

    move-object v2, v3

    .line 66
    goto :goto_1

    .line 47
    .end local v0    # "lfolvl":Lorg/apache/poi/hdf/model/hdftypes/LFOLVL;
    .end local v1    # "lst":Lorg/apache/poi/hdf/model/hdftypes/LST;
    .end local v2    # "lvl":Lorg/apache/poi/hdf/model/hdftypes/LVL;
    .end local v3    # "newLvl":Lorg/apache/poi/hdf/model/hdftypes/LVL;
    :cond_2
    add-int/lit8 v5, v5, 0x1

    goto :goto_0
.end method
