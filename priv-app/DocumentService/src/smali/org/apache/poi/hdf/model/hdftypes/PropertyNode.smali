.class public abstract Lorg/apache/poi/hdf/model/hdftypes/PropertyNode;
.super Ljava/lang/Object;
.source "PropertyNode.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private _fcEnd:I

.field private _fcStart:I

.field private _grpprl:[B


# direct methods
.method public constructor <init>(II[B)V
    .locals 0
    .param p1, "fcStart"    # I
    .param p2, "fcEnd"    # I
    .param p3, "grpprl"    # [B

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput p1, p0, Lorg/apache/poi/hdf/model/hdftypes/PropertyNode;->_fcStart:I

    .line 41
    iput p2, p0, Lorg/apache/poi/hdf/model/hdftypes/PropertyNode;->_fcEnd:I

    .line 42
    iput-object p3, p0, Lorg/apache/poi/hdf/model/hdftypes/PropertyNode;->_grpprl:[B

    .line 43
    return-void
.end method


# virtual methods
.method public compareTo(Ljava/lang/Object;)I
    .locals 2
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 70
    check-cast p1, Lorg/apache/poi/hdf/model/hdftypes/PropertyNode;

    .end local p1    # "o":Ljava/lang/Object;
    invoke-virtual {p1}, Lorg/apache/poi/hdf/model/hdftypes/PropertyNode;->getEnd()I

    move-result v0

    .line 71
    .local v0, "fcEnd":I
    iget v1, p0, Lorg/apache/poi/hdf/model/hdftypes/PropertyNode;->_fcEnd:I

    if-ne v1, v0, :cond_0

    .line 73
    const/4 v1, 0x0

    .line 81
    :goto_0
    return v1

    .line 75
    :cond_0
    iget v1, p0, Lorg/apache/poi/hdf/model/hdftypes/PropertyNode;->_fcEnd:I

    if-ge v1, v0, :cond_1

    .line 77
    const/4 v1, -0x1

    goto :goto_0

    .line 81
    :cond_1
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public getEnd()I
    .locals 1

    .prologue
    .line 56
    iget v0, p0, Lorg/apache/poi/hdf/model/hdftypes/PropertyNode;->_fcEnd:I

    return v0
.end method

.method protected getGrpprl()[B
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lorg/apache/poi/hdf/model/hdftypes/PropertyNode;->_grpprl:[B

    return-object v0
.end method

.method public getStart()I
    .locals 1

    .prologue
    .line 49
    iget v0, p0, Lorg/apache/poi/hdf/model/hdftypes/PropertyNode;->_fcStart:I

    return v0
.end method
