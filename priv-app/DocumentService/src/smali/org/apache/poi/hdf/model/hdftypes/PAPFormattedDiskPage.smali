.class public final Lorg/apache/poi/hdf/model/hdftypes/PAPFormattedDiskPage;
.super Lorg/apache/poi/hdf/model/hdftypes/FormattedDiskPage;
.source "PAPFormattedDiskPage.java"


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# direct methods
.method public constructor <init>([B)V
    .locals 0
    .param p1, "fkp"    # [B

    .prologue
    .line 49
    invoke-direct {p0, p1}, Lorg/apache/poi/hdf/model/hdftypes/FormattedDiskPage;-><init>([B)V

    .line 50
    return-void
.end method


# virtual methods
.method public getGrpprl(I)[B
    .locals 6
    .param p1, "index"    # I

    .prologue
    .line 60
    iget-object v3, p0, Lorg/apache/poi/hdf/model/hdftypes/PAPFormattedDiskPage;->_fkp:[B

    iget v4, p0, Lorg/apache/poi/hdf/model/hdftypes/PAPFormattedDiskPage;->_crun:I

    add-int/lit8 v4, v4, 0x1

    mul-int/lit8 v4, v4, 0x4

    mul-int/lit8 v5, p1, 0xd

    add-int/2addr v4, v5

    invoke-static {v3, v4}, Lorg/apache/poi/util/LittleEndian;->getUnsignedByte([BI)I

    move-result v3

    mul-int/lit8 v1, v3, 0x2

    .line 61
    .local v1, "papxOffset":I
    iget-object v3, p0, Lorg/apache/poi/hdf/model/hdftypes/PAPFormattedDiskPage;->_fkp:[B

    invoke-static {v3, v1}, Lorg/apache/poi/util/LittleEndian;->getUnsignedByte([BI)I

    move-result v3

    mul-int/lit8 v2, v3, 0x2

    .line 62
    .local v2, "size":I
    if-nez v2, :cond_0

    .line 64
    iget-object v3, p0, Lorg/apache/poi/hdf/model/hdftypes/PAPFormattedDiskPage;->_fkp:[B

    add-int/lit8 v1, v1, 0x1

    invoke-static {v3, v1}, Lorg/apache/poi/util/LittleEndian;->getUnsignedByte([BI)I

    move-result v3

    mul-int/lit8 v2, v3, 0x2

    .line 71
    :goto_0
    new-array v0, v2, [B

    .line 72
    .local v0, "papx":[B
    iget-object v3, p0, Lorg/apache/poi/hdf/model/hdftypes/PAPFormattedDiskPage;->_fkp:[B

    add-int/lit8 v1, v1, 0x1

    const/4 v4, 0x0

    invoke-static {v3, v1, v0, v4, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 73
    return-object v0

    .line 68
    .end local v0    # "papx":[B
    :cond_0
    add-int/lit8 v2, v2, -0x1

    goto :goto_0
.end method
