.class public final Lorg/apache/poi/hdf/model/hdftypes/StyleSheet;
.super Ljava/lang/Object;
.source "StyleSheet.java"

# interfaces
.implements Lorg/apache/poi/hdf/model/hdftypes/HDFType;


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field private static final CHP_TYPE:I = 0x2

.field private static final NIL_STYLE:I = 0xfff

.field private static final PAP_TYPE:I = 0x1

.field private static final SEP_TYPE:I = 0x4

.field private static final TAP_TYPE:I = 0x5


# instance fields
.field _nilStyle:Lorg/apache/poi/hdf/model/hdftypes/StyleDescription;

.field _styleDescriptions:[Lorg/apache/poi/hdf/model/hdftypes/StyleDescription;


# direct methods
.method public constructor <init>([B)V
    .locals 14
    .param p1, "styleSheet"    # [B

    .prologue
    const/4 v13, 0x2

    const/4 v12, 0x1

    const/4 v11, 0x0

    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    new-instance v10, Lorg/apache/poi/hdf/model/hdftypes/StyleDescription;

    invoke-direct {v10}, Lorg/apache/poi/hdf/model/hdftypes/StyleDescription;-><init>()V

    iput-object v10, p0, Lorg/apache/poi/hdf/model/hdftypes/StyleSheet;->_nilStyle:Lorg/apache/poi/hdf/model/hdftypes/StyleDescription;

    .line 56
    invoke-static {p1, v11}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v8

    .line 57
    .local v8, "stshiLength":I
    invoke-static {p1, v13}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v5

    .line 58
    .local v5, "stdCount":I
    const/4 v10, 0x4

    invoke-static {p1, v10}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v1

    .line 59
    .local v1, "baseLength":I
    const/4 v10, 0x3

    new-array v3, v10, [I

    .line 61
    .local v3, "rgftc":[I
    const/16 v10, 0xe

    invoke-static {p1, v10}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v10

    aput v10, v3, v11

    .line 62
    const/16 v10, 0x12

    invoke-static {p1, v10}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v10

    aput v10, v3, v12

    .line 63
    const/16 v10, 0x16

    invoke-static {p1, v10}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v10

    aput v10, v3, v13

    .line 65
    const/4 v2, 0x0

    .line 66
    .local v2, "offset":I
    new-array v10, v5, [Lorg/apache/poi/hdf/model/hdftypes/StyleDescription;

    iput-object v10, p0, Lorg/apache/poi/hdf/model/hdftypes/StyleSheet;->_styleDescriptions:[Lorg/apache/poi/hdf/model/hdftypes/StyleDescription;

    .line 67
    const/4 v9, 0x0

    .local v9, "x":I
    :goto_0
    if-lt v9, v5, :cond_0

    .line 87
    const/4 v9, 0x0

    :goto_1
    iget-object v10, p0, Lorg/apache/poi/hdf/model/hdftypes/StyleSheet;->_styleDescriptions:[Lorg/apache/poi/hdf/model/hdftypes/StyleDescription;

    array-length v10, v10

    if-lt v9, v10, :cond_2

    .line 95
    return-void

    .line 69
    :cond_0
    add-int/lit8 v10, v8, 0x2

    add-int v6, v10, v2

    .line 70
    .local v6, "stdOffset":I
    invoke-static {p1, v6}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v7

    .line 71
    .local v7, "stdSize":I
    if-lez v7, :cond_1

    .line 73
    new-array v4, v7, [B

    .line 76
    .local v4, "std":[B
    add-int/lit8 v6, v6, 0x2

    .line 77
    invoke-static {p1, v6, v4, v11, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 78
    new-instance v0, Lorg/apache/poi/hdf/model/hdftypes/StyleDescription;

    invoke-direct {v0, v4, v1, v12}, Lorg/apache/poi/hdf/model/hdftypes/StyleDescription;-><init>([BIZ)V

    .line 80
    .local v0, "aStyle":Lorg/apache/poi/hdf/model/hdftypes/StyleDescription;
    iget-object v10, p0, Lorg/apache/poi/hdf/model/hdftypes/StyleSheet;->_styleDescriptions:[Lorg/apache/poi/hdf/model/hdftypes/StyleDescription;

    aput-object v0, v10, v9

    .line 84
    .end local v0    # "aStyle":Lorg/apache/poi/hdf/model/hdftypes/StyleDescription;
    .end local v4    # "std":[B
    :cond_1
    add-int/lit8 v10, v7, 0x2

    add-int/2addr v2, v10

    .line 67
    add-int/lit8 v9, v9, 0x1

    goto :goto_0

    .line 89
    .end local v6    # "stdOffset":I
    .end local v7    # "stdSize":I
    :cond_2
    iget-object v10, p0, Lorg/apache/poi/hdf/model/hdftypes/StyleSheet;->_styleDescriptions:[Lorg/apache/poi/hdf/model/hdftypes/StyleDescription;

    aget-object v10, v10, v9

    if-eqz v10, :cond_3

    .line 91
    invoke-direct {p0, v9}, Lorg/apache/poi/hdf/model/hdftypes/StyleSheet;->createPap(I)V

    .line 92
    invoke-direct {p0, v9}, Lorg/apache/poi/hdf/model/hdftypes/StyleSheet;->createChp(I)V

    .line 87
    :cond_3
    add-int/lit8 v9, v9, 0x1

    goto :goto_1
.end method

.method private createChp(I)V
    .locals 6
    .param p1, "istd"    # I

    .prologue
    .line 143
    iget-object v5, p0, Lorg/apache/poi/hdf/model/hdftypes/StyleSheet;->_styleDescriptions:[Lorg/apache/poi/hdf/model/hdftypes/StyleDescription;

    aget-object v4, v5, p1

    .line 144
    .local v4, "sd":Lorg/apache/poi/hdf/model/hdftypes/StyleDescription;
    invoke-virtual {v4}, Lorg/apache/poi/hdf/model/hdftypes/StyleDescription;->getCHP()Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;

    move-result-object v1

    .line 145
    .local v1, "chp":Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;
    invoke-virtual {v4}, Lorg/apache/poi/hdf/model/hdftypes/StyleDescription;->getCHPX()[B

    move-result-object v2

    .line 146
    .local v2, "chpx":[B
    invoke-virtual {v4}, Lorg/apache/poi/hdf/model/hdftypes/StyleDescription;->getBaseStyle()I

    move-result v0

    .line 147
    .local v0, "baseIndex":I
    if-nez v1, :cond_1

    if-eqz v2, :cond_1

    .line 149
    iget-object v5, p0, Lorg/apache/poi/hdf/model/hdftypes/StyleSheet;->_nilStyle:Lorg/apache/poi/hdf/model/hdftypes/StyleDescription;

    invoke-virtual {v5}, Lorg/apache/poi/hdf/model/hdftypes/StyleDescription;->getCHP()Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;

    move-result-object v3

    .line 150
    .local v3, "parentCHP":Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;
    const/16 v5, 0xfff

    if-eq v0, v5, :cond_0

    .line 153
    iget-object v5, p0, Lorg/apache/poi/hdf/model/hdftypes/StyleSheet;->_styleDescriptions:[Lorg/apache/poi/hdf/model/hdftypes/StyleDescription;

    aget-object v5, v5, v0

    invoke-virtual {v5}, Lorg/apache/poi/hdf/model/hdftypes/StyleDescription;->getCHP()Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;

    move-result-object v3

    .line 154
    if-nez v3, :cond_0

    .line 156
    invoke-direct {p0, v0}, Lorg/apache/poi/hdf/model/hdftypes/StyleSheet;->createChp(I)V

    .line 157
    iget-object v5, p0, Lorg/apache/poi/hdf/model/hdftypes/StyleSheet;->_styleDescriptions:[Lorg/apache/poi/hdf/model/hdftypes/StyleDescription;

    aget-object v5, v5, v0

    invoke-virtual {v5}, Lorg/apache/poi/hdf/model/hdftypes/StyleDescription;->getCHP()Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;

    move-result-object v3

    .line 162
    :cond_0
    invoke-static {v2, v3, p0}, Lorg/apache/poi/hdf/model/hdftypes/StyleSheet;->uncompressProperty([BLjava/lang/Object;Lorg/apache/poi/hdf/model/hdftypes/StyleSheet;)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "chp":Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;
    check-cast v1, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;

    .line 163
    .restart local v1    # "chp":Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;
    invoke-virtual {v4, v1}, Lorg/apache/poi/hdf/model/hdftypes/StyleDescription;->setCHP(Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;)V

    .line 165
    .end local v3    # "parentCHP":Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;
    :cond_1
    return-void
.end method

.method private createPap(I)V
    .locals 6
    .param p1, "istd"    # I

    .prologue
    .line 108
    iget-object v5, p0, Lorg/apache/poi/hdf/model/hdftypes/StyleSheet;->_styleDescriptions:[Lorg/apache/poi/hdf/model/hdftypes/StyleDescription;

    aget-object v4, v5, p1

    .line 109
    .local v4, "sd":Lorg/apache/poi/hdf/model/hdftypes/StyleDescription;
    invoke-virtual {v4}, Lorg/apache/poi/hdf/model/hdftypes/StyleDescription;->getPAP()Lorg/apache/poi/hdf/model/hdftypes/ParagraphProperties;

    move-result-object v1

    .line 110
    .local v1, "pap":Lorg/apache/poi/hdf/model/hdftypes/ParagraphProperties;
    invoke-virtual {v4}, Lorg/apache/poi/hdf/model/hdftypes/StyleDescription;->getPAPX()[B

    move-result-object v2

    .line 111
    .local v2, "papx":[B
    invoke-virtual {v4}, Lorg/apache/poi/hdf/model/hdftypes/StyleDescription;->getBaseStyle()I

    move-result v0

    .line 112
    .local v0, "baseIndex":I
    if-nez v1, :cond_1

    if-eqz v2, :cond_1

    .line 114
    iget-object v5, p0, Lorg/apache/poi/hdf/model/hdftypes/StyleSheet;->_nilStyle:Lorg/apache/poi/hdf/model/hdftypes/StyleDescription;

    invoke-virtual {v5}, Lorg/apache/poi/hdf/model/hdftypes/StyleDescription;->getPAP()Lorg/apache/poi/hdf/model/hdftypes/ParagraphProperties;

    move-result-object v3

    .line 115
    .local v3, "parentPAP":Lorg/apache/poi/hdf/model/hdftypes/ParagraphProperties;
    const/16 v5, 0xfff

    if-eq v0, v5, :cond_0

    .line 118
    iget-object v5, p0, Lorg/apache/poi/hdf/model/hdftypes/StyleSheet;->_styleDescriptions:[Lorg/apache/poi/hdf/model/hdftypes/StyleDescription;

    aget-object v5, v5, v0

    invoke-virtual {v5}, Lorg/apache/poi/hdf/model/hdftypes/StyleDescription;->getPAP()Lorg/apache/poi/hdf/model/hdftypes/ParagraphProperties;

    move-result-object v3

    .line 119
    if-nez v3, :cond_0

    .line 121
    invoke-direct {p0, v0}, Lorg/apache/poi/hdf/model/hdftypes/StyleSheet;->createPap(I)V

    .line 122
    iget-object v5, p0, Lorg/apache/poi/hdf/model/hdftypes/StyleSheet;->_styleDescriptions:[Lorg/apache/poi/hdf/model/hdftypes/StyleDescription;

    aget-object v5, v5, v0

    invoke-virtual {v5}, Lorg/apache/poi/hdf/model/hdftypes/StyleDescription;->getPAP()Lorg/apache/poi/hdf/model/hdftypes/ParagraphProperties;

    move-result-object v3

    .line 127
    :cond_0
    invoke-static {v2, v3, p0}, Lorg/apache/poi/hdf/model/hdftypes/StyleSheet;->uncompressProperty([BLjava/lang/Object;Lorg/apache/poi/hdf/model/hdftypes/StyleSheet;)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "pap":Lorg/apache/poi/hdf/model/hdftypes/ParagraphProperties;
    check-cast v1, Lorg/apache/poi/hdf/model/hdftypes/ParagraphProperties;

    .line 128
    .restart local v1    # "pap":Lorg/apache/poi/hdf/model/hdftypes/ParagraphProperties;
    invoke-virtual {v4, v1}, Lorg/apache/poi/hdf/model/hdftypes/StyleDescription;->setPAP(Lorg/apache/poi/hdf/model/hdftypes/ParagraphProperties;)V

    .line 130
    .end local v3    # "parentPAP":Lorg/apache/poi/hdf/model/hdftypes/ParagraphProperties;
    :cond_1
    return-void
.end method

.method static doCHPOperation(Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;II[B[BILorg/apache/poi/hdf/model/hdftypes/StyleSheet;)V
    .locals 27
    .param p0, "oldCHP"    # Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;
    .param p1, "newCHP"    # Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;
    .param p2, "operand"    # I
    .param p3, "param"    # I
    .param p4, "varParam"    # [B
    .param p5, "grpprl"    # [B
    .param p6, "offset"    # I
    .param p7, "styleSheet"    # Lorg/apache/poi/hdf/model/hdftypes/StyleSheet;

    .prologue
    .line 196
    packed-switch p2, :pswitch_data_0

    .line 646
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 199
    :pswitch_1
    invoke-static/range {p3 .. p3}, Lorg/apache/poi/hdf/model/hdftypes/StyleSheet;->getFlag(I)Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->setFRMarkDel(Z)V

    goto :goto_0

    .line 202
    :pswitch_2
    invoke-static/range {p3 .. p3}, Lorg/apache/poi/hdf/model/hdftypes/StyleSheet;->getFlag(I)Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->setFRMark(Z)V

    goto :goto_0

    .line 207
    :pswitch_3
    move-object/from16 v0, p1

    move/from16 v1, p3

    invoke-virtual {v0, v1}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->setFcPic(I)V

    .line 208
    const/4 v3, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->setFSpec(Z)V

    goto :goto_0

    .line 211
    :pswitch_4
    move/from16 v0, p3

    int-to-short v3, v0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->setIbstRMark(I)V

    goto :goto_0

    .line 214
    :pswitch_5
    const/4 v3, 0x2

    new-array v15, v3, [S

    .line 215
    .local v15, "dttmRMark":[S
    const/4 v3, 0x0

    add-int/lit8 v4, p6, -0x4

    move-object/from16 v0, p5

    invoke-static {v0, v4}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v4

    aput-short v4, v15, v3

    .line 216
    const/4 v3, 0x1

    add-int/lit8 v4, p6, -0x2

    move-object/from16 v0, p5

    invoke-static {v0, v4}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v4

    aput-short v4, v15, v3

    .line 217
    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->setDttmRMark([S)V

    goto :goto_0

    .line 220
    .end local v15    # "dttmRMark":[S
    :pswitch_6
    invoke-static/range {p3 .. p3}, Lorg/apache/poi/hdf/model/hdftypes/StyleSheet;->getFlag(I)Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->setFData(Z)V

    goto :goto_0

    .line 226
    :pswitch_7
    const/high16 v3, 0xff0000

    and-int v3, v3, p3

    ushr-int/lit8 v3, v3, 0x8

    int-to-short v14, v3

    .line 227
    .local v14, "chsDiff":S
    invoke-static {v14}, Lorg/apache/poi/hdf/model/hdftypes/StyleSheet;->getFlag(I)Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->setFChsDiff(Z)V

    .line 228
    const v3, 0xffff

    and-int v3, v3, p3

    int-to-short v3, v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->setChse(S)V

    goto :goto_0

    .line 231
    .end local v14    # "chsDiff":S
    :pswitch_8
    const/4 v3, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->setFSpec(Z)V

    .line 233
    if-eqz p4, :cond_0

    .line 234
    const/4 v3, 0x0

    move-object/from16 v0, p4

    invoke-static {v0, v3}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->setFtcSym(I)V

    .line 235
    const/4 v3, 0x2

    move-object/from16 v0, p4

    invoke-static {v0, v3}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->setXchSym(I)V

    goto/16 :goto_0

    .line 239
    :pswitch_9
    invoke-static/range {p3 .. p3}, Lorg/apache/poi/hdf/model/hdftypes/StyleSheet;->getFlag(I)Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->setFOle2(Z)V

    goto/16 :goto_0

    .line 245
    :pswitch_a
    move/from16 v0, p3

    int-to-byte v3, v0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->setIcoHighlight(B)V

    .line 246
    invoke-static/range {p3 .. p3}, Lorg/apache/poi/hdf/model/hdftypes/StyleSheet;->getFlag(I)Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->setFHighlight(Z)V

    goto/16 :goto_0

    .line 251
    :pswitch_b
    move-object/from16 v0, p1

    move/from16 v1, p3

    invoke-virtual {v0, v1}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->setFcObj(I)V

    goto/16 :goto_0

    .line 321
    :pswitch_c
    move-object/from16 v0, p1

    move/from16 v1, p3

    invoke-virtual {v0, v1}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->setIstd(I)V

    goto/16 :goto_0

    .line 327
    :pswitch_d
    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->setFBold(Z)V

    .line 328
    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->setFItalic(Z)V

    .line 329
    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->setFOutline(Z)V

    .line 330
    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->setFStrike(Z)V

    .line 331
    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->setFShadow(Z)V

    .line 332
    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->setFSmallCaps(Z)V

    .line 333
    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->setFCaps(Z)V

    .line 334
    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->setFVanish(Z)V

    .line 335
    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->setKul(B)V

    .line 336
    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->setIco(B)V

    goto/16 :goto_0

    .line 341
    :pswitch_e
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->clone()Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;

    move-object/from16 p1, v0
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 343
    :catch_0
    move-exception v17

    .line 346
    .local v17, "e":Ljava/lang/CloneNotSupportedException;
    const-string/jumbo v3, "DocumentService"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "Exception: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v17 .. v17}, Ljava/lang/CloneNotSupportedException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 352
    .end local v17    # "e":Ljava/lang/CloneNotSupportedException;
    :pswitch_f
    move/from16 v0, p3

    int-to-byte v3, v0

    invoke-virtual/range {p0 .. p0}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->isFBold()Z

    move-result v4

    invoke-static {v3, v4}, Lorg/apache/poi/hdf/model/hdftypes/StyleSheet;->getCHPFlag(BZ)Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->setFBold(Z)V

    goto/16 :goto_0

    .line 355
    :pswitch_10
    move/from16 v0, p3

    int-to-byte v3, v0

    invoke-virtual/range {p0 .. p0}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->isFItalic()Z

    move-result v4

    invoke-static {v3, v4}, Lorg/apache/poi/hdf/model/hdftypes/StyleSheet;->getCHPFlag(BZ)Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->setFItalic(Z)V

    goto/16 :goto_0

    .line 358
    :pswitch_11
    move/from16 v0, p3

    int-to-byte v3, v0

    invoke-virtual/range {p0 .. p0}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->isFStrike()Z

    move-result v4

    invoke-static {v3, v4}, Lorg/apache/poi/hdf/model/hdftypes/StyleSheet;->getCHPFlag(BZ)Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->setFStrike(Z)V

    goto/16 :goto_0

    .line 361
    :pswitch_12
    move/from16 v0, p3

    int-to-byte v3, v0

    invoke-virtual/range {p0 .. p0}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->isFOutline()Z

    move-result v4

    invoke-static {v3, v4}, Lorg/apache/poi/hdf/model/hdftypes/StyleSheet;->getCHPFlag(BZ)Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->setFOutline(Z)V

    goto/16 :goto_0

    .line 364
    :pswitch_13
    move/from16 v0, p3

    int-to-byte v3, v0

    invoke-virtual/range {p0 .. p0}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->isFShadow()Z

    move-result v4

    invoke-static {v3, v4}, Lorg/apache/poi/hdf/model/hdftypes/StyleSheet;->getCHPFlag(BZ)Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->setFShadow(Z)V

    goto/16 :goto_0

    .line 367
    :pswitch_14
    move/from16 v0, p3

    int-to-byte v3, v0

    invoke-virtual/range {p0 .. p0}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->isFSmallCaps()Z

    move-result v4

    invoke-static {v3, v4}, Lorg/apache/poi/hdf/model/hdftypes/StyleSheet;->getCHPFlag(BZ)Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->setFSmallCaps(Z)V

    goto/16 :goto_0

    .line 370
    :pswitch_15
    move/from16 v0, p3

    int-to-byte v3, v0

    invoke-virtual/range {p0 .. p0}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->isFCaps()Z

    move-result v4

    invoke-static {v3, v4}, Lorg/apache/poi/hdf/model/hdftypes/StyleSheet;->getCHPFlag(BZ)Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->setFCaps(Z)V

    goto/16 :goto_0

    .line 373
    :pswitch_16
    move/from16 v0, p3

    int-to-byte v3, v0

    invoke-virtual/range {p0 .. p0}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->isFVanish()Z

    move-result v4

    invoke-static {v3, v4}, Lorg/apache/poi/hdf/model/hdftypes/StyleSheet;->getCHPFlag(BZ)Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->setFVanish(Z)V

    goto/16 :goto_0

    .line 376
    :pswitch_17
    move/from16 v0, p3

    int-to-short v3, v0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->setFtcAscii(I)V

    goto/16 :goto_0

    .line 379
    :pswitch_18
    move/from16 v0, p3

    int-to-byte v3, v0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->setKul(B)V

    goto/16 :goto_0

    .line 382
    :pswitch_19
    move/from16 v0, p3

    and-int/lit16 v0, v0, 0xff

    move/from16 v20, v0

    .line 383
    .local v20, "hps":I
    if-eqz v20, :cond_1

    .line 385
    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->setHps(I)V

    .line 387
    :cond_1
    const v3, 0xfe00

    and-int v3, v3, p3

    int-to-byte v3, v3

    ushr-int/lit8 v3, v3, 0x4

    shr-int/lit8 v3, v3, 0x1

    int-to-byte v13, v3

    .line 388
    .local v13, "cInc":B
    if-eqz v13, :cond_2

    .line 390
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->getHps()I

    move-result v3

    mul-int/lit8 v4, v13, 0x2

    add-int/2addr v3, v4

    const/4 v4, 0x2

    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->setHps(I)V

    .line 392
    :cond_2
    const/high16 v3, 0xff0000

    and-int v3, v3, p3

    ushr-int/lit8 v3, v3, 0x8

    int-to-byte v0, v3

    move/from16 v22, v0

    .line 393
    .local v22, "hpsPos":B
    const/16 v3, 0x80

    move/from16 v0, v22

    if-eq v0, v3, :cond_3

    .line 395
    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->setHpsPos(I)V

    .line 397
    :cond_3
    move/from16 v0, p3

    and-int/lit16 v3, v0, 0x100

    if-lez v3, :cond_5

    const/16 v18, 0x1

    .line 398
    .local v18, "fAdjust":Z
    :goto_1
    if-eqz v18, :cond_4

    const/16 v3, 0x80

    move/from16 v0, v22

    if-eq v0, v3, :cond_4

    if-eqz v22, :cond_4

    invoke-virtual/range {p0 .. p0}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->getHpsPos()I

    move-result v3

    if-nez v3, :cond_4

    .line 400
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->getHps()I

    move-result v3

    add-int/lit8 v3, v3, -0x2

    const/4 v4, 0x2

    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->setHps(I)V

    .line 402
    :cond_4
    if-eqz v18, :cond_0

    if-nez v22, :cond_0

    invoke-virtual/range {p0 .. p0}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->getHpsPos()I

    move-result v3

    if-eqz v3, :cond_0

    .line 404
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->getHps()I

    move-result v3

    add-int/lit8 v3, v3, 0x2

    const/4 v4, 0x2

    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->setHps(I)V

    goto/16 :goto_0

    .line 397
    .end local v18    # "fAdjust":Z
    :cond_5
    const/16 v18, 0x0

    goto :goto_1

    .line 408
    .end local v13    # "cInc":B
    .end local v20    # "hps":I
    .end local v22    # "hpsPos":B
    :pswitch_1a
    move-object/from16 v0, p1

    move/from16 v1, p3

    invoke-virtual {v0, v1}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->setDxaSpace(I)V

    goto/16 :goto_0

    .line 411
    :pswitch_1b
    move/from16 v0, p3

    int-to-short v3, v0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->setLidDefault(I)V

    goto/16 :goto_0

    .line 414
    :pswitch_1c
    move/from16 v0, p3

    int-to-byte v3, v0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->setIco(B)V

    goto/16 :goto_0

    .line 417
    :pswitch_1d
    move-object/from16 v0, p1

    move/from16 v1, p3

    invoke-virtual {v0, v1}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->setHps(I)V

    goto/16 :goto_0

    .line 420
    :pswitch_1e
    move/from16 v0, p3

    int-to-byte v0, v0

    move/from16 v21, v0

    .line 421
    .local v21, "hpsLvl":B
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->getHps()I

    move-result v3

    mul-int/lit8 v4, v21, 0x2

    add-int/2addr v3, v4

    const/4 v4, 0x2

    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->setHps(I)V

    goto/16 :goto_0

    .line 424
    .end local v21    # "hpsLvl":B
    :pswitch_1f
    move/from16 v0, p3

    int-to-short v3, v0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->setHpsPos(I)V

    goto/16 :goto_0

    .line 427
    :pswitch_20
    if-eqz p3, :cond_6

    .line 429
    invoke-virtual/range {p0 .. p0}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->getHpsPos()I

    move-result v3

    if-nez v3, :cond_0

    .line 431
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->getHps()I

    move-result v3

    add-int/lit8 v3, v3, -0x2

    const/4 v4, 0x2

    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->setHps(I)V

    goto/16 :goto_0

    .line 436
    :cond_6
    invoke-virtual/range {p0 .. p0}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->getHpsPos()I

    move-result v3

    if-eqz v3, :cond_0

    .line 438
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->getHps()I

    move-result v3

    add-int/lit8 v3, v3, 0x2

    const/4 v4, 0x2

    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->setHps(I)V

    goto/16 :goto_0

    .line 443
    :pswitch_21
    new-instance v19, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;

    invoke-direct/range {v19 .. v19}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;-><init>()V

    .line 444
    .local v19, "genCHP":Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;
    const/4 v3, 0x4

    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->setFtcAscii(I)V

    .line 446
    if-eqz p4, :cond_7

    .line 447
    move-object/from16 v0, p4

    move-object/from16 v1, v19

    move-object/from16 v2, p7

    invoke-static {v0, v1, v2}, Lorg/apache/poi/hdf/model/hdftypes/StyleSheet;->uncompressProperty([BLjava/lang/Object;Lorg/apache/poi/hdf/model/hdftypes/StyleSheet;)Ljava/lang/Object;

    move-result-object v19

    .end local v19    # "genCHP":Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;
    check-cast v19, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;

    .line 450
    .restart local v19    # "genCHP":Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;
    :cond_7
    invoke-virtual/range {p0 .. p0}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->getBaseIstd()I

    move-result v3

    move-object/from16 v0, p7

    invoke-virtual {v0, v3}, Lorg/apache/poi/hdf/model/hdftypes/StyleSheet;->getStyleDescription(I)Lorg/apache/poi/hdf/model/hdftypes/StyleDescription;

    move-result-object v3

    invoke-virtual {v3}, Lorg/apache/poi/hdf/model/hdftypes/StyleDescription;->getCHP()Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;

    move-result-object v25

    .line 451
    .local v25, "styleCHP":Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;
    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->isFBold()Z

    move-result v3

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->isFBold()Z

    move-result v4

    if-ne v3, v4, :cond_8

    .line 453
    invoke-virtual/range {v25 .. v25}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->isFBold()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->setFBold(Z)V

    .line 455
    :cond_8
    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->isFItalic()Z

    move-result v3

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->isFItalic()Z

    move-result v4

    if-ne v3, v4, :cond_9

    .line 457
    invoke-virtual/range {v25 .. v25}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->isFItalic()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->setFItalic(Z)V

    .line 459
    :cond_9
    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->isFSmallCaps()Z

    move-result v3

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->isFSmallCaps()Z

    move-result v4

    if-ne v3, v4, :cond_a

    .line 461
    invoke-virtual/range {v25 .. v25}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->isFSmallCaps()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->setFSmallCaps(Z)V

    .line 463
    :cond_a
    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->isFVanish()Z

    move-result v3

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->isFVanish()Z

    move-result v4

    if-ne v3, v4, :cond_b

    .line 465
    invoke-virtual/range {v25 .. v25}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->isFVanish()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->setFVanish(Z)V

    .line 467
    :cond_b
    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->isFStrike()Z

    move-result v3

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->isFStrike()Z

    move-result v4

    if-ne v3, v4, :cond_c

    .line 469
    invoke-virtual/range {v25 .. v25}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->isFStrike()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->setFStrike(Z)V

    .line 471
    :cond_c
    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->isFCaps()Z

    move-result v3

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->isFCaps()Z

    move-result v4

    if-ne v3, v4, :cond_d

    .line 473
    invoke-virtual/range {v25 .. v25}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->isFCaps()Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->setFCaps(Z)V

    .line 475
    :cond_d
    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->getFtcAscii()I

    move-result v3

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->getFtcAscii()I

    move-result v4

    if-ne v3, v4, :cond_e

    .line 477
    invoke-virtual/range {v25 .. v25}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->getFtcAscii()I

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->setFtcAscii(I)V

    .line 479
    :cond_e
    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->getFtcFE()I

    move-result v3

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->getFtcFE()I

    move-result v4

    if-ne v3, v4, :cond_f

    .line 481
    invoke-virtual/range {v25 .. v25}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->getFtcFE()I

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->setFtcFE(I)V

    .line 483
    :cond_f
    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->getFtcOther()I

    move-result v3

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->getFtcOther()I

    move-result v4

    if-ne v3, v4, :cond_10

    .line 485
    invoke-virtual/range {v25 .. v25}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->getFtcOther()I

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->setFtcOther(I)V

    .line 487
    :cond_10
    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->getHps()I

    move-result v3

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->getHps()I

    move-result v4

    if-ne v3, v4, :cond_11

    .line 489
    invoke-virtual/range {v25 .. v25}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->getHps()I

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->setHps(I)V

    .line 491
    :cond_11
    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->getHpsPos()I

    move-result v3

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->getHpsPos()I

    move-result v4

    if-ne v3, v4, :cond_12

    .line 493
    invoke-virtual/range {v25 .. v25}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->getHpsPos()I

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->setHpsPos(I)V

    .line 495
    :cond_12
    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->getKul()B

    move-result v3

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->getKul()B

    move-result v4

    if-ne v3, v4, :cond_13

    .line 497
    invoke-virtual/range {v25 .. v25}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->getKul()B

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->setKul(B)V

    .line 499
    :cond_13
    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->getDxaSpace()I

    move-result v3

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->getDxaSpace()I

    move-result v4

    if-ne v3, v4, :cond_14

    .line 501
    invoke-virtual/range {v25 .. v25}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->getDxaSpace()I

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->setDxaSpace(I)V

    .line 503
    :cond_14
    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->getIco()B

    move-result v3

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->getIco()B

    move-result v4

    if-ne v3, v4, :cond_15

    .line 505
    invoke-virtual/range {v25 .. v25}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->getIco()B

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->setIco(B)V

    .line 507
    :cond_15
    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->getLidDefault()I

    move-result v3

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->getLidDefault()I

    move-result v4

    if-ne v3, v4, :cond_16

    .line 509
    invoke-virtual/range {v25 .. v25}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->getLidDefault()I

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->setLidDefault(I)V

    .line 511
    :cond_16
    invoke-virtual/range {v19 .. v19}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->getLidFE()I

    move-result v3

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->getLidFE()I

    move-result v4

    if-ne v3, v4, :cond_0

    .line 513
    invoke-virtual/range {v25 .. v25}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->getLidFE()I

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->setLidFE(I)V

    goto/16 :goto_0

    .line 517
    .end local v19    # "genCHP":Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;
    .end local v25    # "styleCHP":Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;
    :pswitch_22
    move/from16 v0, p3

    int-to-byte v3, v0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->setIss(B)V

    goto/16 :goto_0

    .line 520
    :pswitch_23
    if-eqz p4, :cond_0

    .line 521
    const/4 v3, 0x0

    move-object/from16 v0, p4

    invoke-static {v0, v3}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->setHps(I)V

    goto/16 :goto_0

    .line 525
    :pswitch_24
    if-eqz p4, :cond_17

    .line 526
    const/4 v3, 0x0

    move-object/from16 v0, p4

    invoke-static {v0, v3}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v23

    .line 527
    .local v23, "increment":I
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->getHps()I

    move-result v3

    add-int v3, v3, v23

    const/16 v4, 0x8

    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->setHps(I)V

    goto/16 :goto_0

    .line 531
    .end local v23    # "increment":I
    :cond_17
    :pswitch_25
    move-object/from16 v0, p1

    move/from16 v1, p3

    invoke-virtual {v0, v1}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->setHpsKern(I)V

    goto/16 :goto_0

    .line 534
    :pswitch_26
    const/16 v5, 0x47

    move-object/from16 v3, p0

    move-object/from16 v4, p1

    move/from16 v6, p3

    move-object/from16 v7, p4

    move-object/from16 v8, p5

    move/from16 v9, p6

    move-object/from16 v10, p7

    invoke-static/range {v3 .. v10}, Lorg/apache/poi/hdf/model/hdftypes/StyleSheet;->doCHPOperation(Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;II[B[BILorg/apache/poi/hdf/model/hdftypes/StyleSheet;)V

    goto/16 :goto_0

    .line 537
    :pswitch_27
    move/from16 v0, p3

    int-to-float v3, v0

    const/high16 v4, 0x42c80000    # 100.0f

    div-float v24, v3, v4

    .line 538
    .local v24, "percentage":F
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->getHps()I

    move-result v3

    int-to-float v3, v3

    mul-float v3, v3, v24

    float-to-int v11, v3

    .line 539
    .local v11, "add":I
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->getHps()I

    move-result v3

    add-int/2addr v3, v11

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->setHps(I)V

    goto/16 :goto_0

    .line 542
    .end local v11    # "add":I
    .end local v24    # "percentage":F
    :pswitch_28
    move/from16 v0, p3

    int-to-byte v3, v0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->setYsr(B)V

    goto/16 :goto_0

    .line 545
    :pswitch_29
    move/from16 v0, p3

    int-to-short v3, v0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->setFtcAscii(I)V

    goto/16 :goto_0

    .line 548
    :pswitch_2a
    move/from16 v0, p3

    int-to-short v3, v0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->setFtcFE(I)V

    goto/16 :goto_0

    .line 551
    :pswitch_2b
    move/from16 v0, p3

    int-to-short v3, v0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->setFtcOther(I)V

    goto/16 :goto_0

    .line 556
    :pswitch_2c
    invoke-static/range {p3 .. p3}, Lorg/apache/poi/hdf/model/hdftypes/StyleSheet;->getFlag(I)Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->setFDStrike(Z)V

    goto/16 :goto_0

    .line 559
    :pswitch_2d
    invoke-static/range {p3 .. p3}, Lorg/apache/poi/hdf/model/hdftypes/StyleSheet;->getFlag(I)Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->setFImprint(Z)V

    goto/16 :goto_0

    .line 562
    :pswitch_2e
    invoke-static/range {p3 .. p3}, Lorg/apache/poi/hdf/model/hdftypes/StyleSheet;->getFlag(I)Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->setFSpec(Z)V

    goto/16 :goto_0

    .line 565
    :pswitch_2f
    invoke-static/range {p3 .. p3}, Lorg/apache/poi/hdf/model/hdftypes/StyleSheet;->getFlag(I)Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->setFObj(Z)V

    goto/16 :goto_0

    .line 568
    :pswitch_30
    if-eqz p4, :cond_0

    .line 569
    const/4 v3, 0x0

    aget-byte v3, p4, v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->setFPropMark(S)V

    .line 570
    const/4 v3, 0x1

    move-object/from16 v0, p4

    invoke-static {v0, v3}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->setIbstPropRMark(I)V

    .line 571
    const/4 v3, 0x3

    move-object/from16 v0, p4

    invoke-static {v0, v3}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->setDttmPropRMark(I)V

    goto/16 :goto_0

    .line 575
    :pswitch_31
    invoke-static/range {p3 .. p3}, Lorg/apache/poi/hdf/model/hdftypes/StyleSheet;->getFlag(I)Z

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->setFEmboss(Z)V

    goto/16 :goto_0

    .line 578
    :pswitch_32
    move/from16 v0, p3

    int-to-byte v3, v0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->setSfxtText(B)V

    goto/16 :goto_0

    .line 597
    :pswitch_33
    if-eqz p4, :cond_0

    .line 598
    const/16 v3, 0x20

    new-array v0, v3, [B

    move-object/from16 v26, v0

    .line 599
    .local v26, "xstDispFldRMark":[B
    const/4 v3, 0x0

    aget-byte v3, p4, v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->setFDispFldRMark(B)V

    .line 600
    const/4 v3, 0x1

    move-object/from16 v0, p4

    invoke-static {v0, v3}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->setIbstDispFldRMark(I)V

    .line 601
    const/4 v3, 0x3

    move-object/from16 v0, p4

    invoke-static {v0, v3}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->setDttmDispFldRMark(I)V

    .line 602
    const/4 v3, 0x7

    const/4 v4, 0x0

    const/16 v5, 0x20

    move-object/from16 v0, p4

    move-object/from16 v1, v26

    invoke-static {v0, v3, v1, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 603
    move-object/from16 v0, p1

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->setXstDispFldRMark([B)V

    goto/16 :goto_0

    .line 607
    .end local v26    # "xstDispFldRMark":[B
    :pswitch_34
    move/from16 v0, p3

    int-to-short v3, v0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->setIbstRMarkDel(I)V

    goto/16 :goto_0

    .line 610
    :pswitch_35
    const/4 v3, 0x2

    new-array v0, v3, [S

    move-object/from16 v16, v0

    .line 611
    .local v16, "dttmRMarkDel":[S
    const/4 v3, 0x0

    add-int/lit8 v4, p6, -0x4

    move-object/from16 v0, p5

    invoke-static {v0, v4}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v4

    aput-short v4, v16, v3

    .line 612
    const/4 v3, 0x1

    add-int/lit8 v4, p6, -0x2

    move-object/from16 v0, p5

    invoke-static {v0, v4}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v4

    aput-short v4, v16, v3

    .line 613
    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->setDttmRMarkDel([S)V

    goto/16 :goto_0

    .line 616
    .end local v16    # "dttmRMarkDel":[S
    :pswitch_36
    const/4 v3, 0x2

    new-array v12, v3, [S

    .line 617
    .local v12, "brc":[S
    const/4 v3, 0x0

    add-int/lit8 v4, p6, -0x4

    move-object/from16 v0, p5

    invoke-static {v0, v4}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v4

    aput-short v4, v12, v3

    .line 618
    const/4 v3, 0x1

    add-int/lit8 v4, p6, -0x2

    move-object/from16 v0, p5

    invoke-static {v0, v4}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v4

    aput-short v4, v12, v3

    .line 619
    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->setBrc([S)V

    goto/16 :goto_0

    .line 622
    .end local v12    # "brc":[S
    :pswitch_37
    move/from16 v0, p3

    int-to-short v3, v0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->setShd(I)V

    goto/16 :goto_0

    .line 637
    :pswitch_38
    move/from16 v0, p3

    int-to-short v3, v0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->setLidDefault(I)V

    goto/16 :goto_0

    .line 640
    :pswitch_39
    move/from16 v0, p3

    int-to-short v3, v0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->setLidFE(I)V

    goto/16 :goto_0

    .line 643
    :pswitch_3a
    move/from16 v0, p3

    int-to-byte v3, v0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->setIdctHint(B)V

    goto/16 :goto_0

    .line 196
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_0
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_0
        :pswitch_a
        :pswitch_0
        :pswitch_b
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_c
        :pswitch_0
        :pswitch_d
        :pswitch_e
        :pswitch_0
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_19
        :pswitch_1a
        :pswitch_1b
        :pswitch_1c
        :pswitch_1d
        :pswitch_1e
        :pswitch_1f
        :pswitch_20
        :pswitch_21
        :pswitch_22
        :pswitch_23
        :pswitch_24
        :pswitch_25
        :pswitch_26
        :pswitch_27
        :pswitch_28
        :pswitch_29
        :pswitch_2a
        :pswitch_2b
        :pswitch_0
        :pswitch_2c
        :pswitch_2d
        :pswitch_2e
        :pswitch_2f
        :pswitch_30
        :pswitch_31
        :pswitch_32
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_33
        :pswitch_34
        :pswitch_35
        :pswitch_36
        :pswitch_37
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_38
        :pswitch_39
        :pswitch_3a
    .end packed-switch
.end method

.method static doPAPOperation(Lorg/apache/poi/hdf/model/hdftypes/ParagraphProperties;II[B[BII)V
    .locals 9
    .param p0, "newPAP"    # Lorg/apache/poi/hdf/model/hdftypes/ParagraphProperties;
    .param p1, "operand"    # I
    .param p2, "param"    # I
    .param p3, "varParam"    # [B
    .param p4, "grpprl"    # [B
    .param p5, "offset"    # I
    .param p6, "spra"    # I

    .prologue
    .line 837
    packed-switch p1, :pswitch_data_0

    .line 1094
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 840
    :pswitch_1
    invoke-virtual {p0, p2}, Lorg/apache/poi/hdf/model/hdftypes/ParagraphProperties;->setIstd(I)V

    goto :goto_0

    .line 846
    :pswitch_2
    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/ParagraphProperties;->getIstd()I

    move-result v7

    const/16 v8, 0x9

    if-le v7, v8, :cond_1

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/ParagraphProperties;->getIstd()I

    move-result v7

    const/4 v8, 0x1

    if-lt v7, v8, :cond_0

    .line 848
    :cond_1
    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/ParagraphProperties;->getIstd()I

    move-result v7

    add-int/2addr v7, p2

    invoke-virtual {p0, v7}, Lorg/apache/poi/hdf/model/hdftypes/ParagraphProperties;->setIstd(I)V

    .line 849
    if-lez p2, :cond_2

    .line 851
    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/ParagraphProperties;->getIstd()I

    move-result v7

    const/16 v8, 0x9

    invoke-static {v7, v8}, Ljava/lang/Math;->max(II)I

    move-result v7

    invoke-virtual {p0, v7}, Lorg/apache/poi/hdf/model/hdftypes/ParagraphProperties;->setIstd(I)V

    goto :goto_0

    .line 855
    :cond_2
    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/ParagraphProperties;->getIstd()I

    move-result v7

    const/4 v8, 0x1

    invoke-static {v7, v8}, Ljava/lang/Math;->min(II)I

    move-result v7

    invoke-virtual {p0, v7}, Lorg/apache/poi/hdf/model/hdftypes/ParagraphProperties;->setIstd(I)V

    goto :goto_0

    .line 860
    :pswitch_3
    int-to-byte v7, p2

    invoke-virtual {p0, v7}, Lorg/apache/poi/hdf/model/hdftypes/ParagraphProperties;->setJc(B)V

    goto :goto_0

    .line 863
    :pswitch_4
    int-to-byte v7, p2

    invoke-virtual {p0, v7}, Lorg/apache/poi/hdf/model/hdftypes/ParagraphProperties;->setFSideBySide(B)V

    goto :goto_0

    .line 866
    :pswitch_5
    int-to-byte v7, p2

    invoke-virtual {p0, v7}, Lorg/apache/poi/hdf/model/hdftypes/ParagraphProperties;->setFKeep(B)V

    goto :goto_0

    .line 869
    :pswitch_6
    int-to-byte v7, p2

    invoke-virtual {p0, v7}, Lorg/apache/poi/hdf/model/hdftypes/ParagraphProperties;->setFKeepFollow(B)V

    goto :goto_0

    .line 872
    :pswitch_7
    int-to-byte v7, p2

    invoke-virtual {p0, v7}, Lorg/apache/poi/hdf/model/hdftypes/ParagraphProperties;->setFPageBreakBefore(B)V

    goto :goto_0

    .line 875
    :pswitch_8
    int-to-byte v7, p2

    invoke-virtual {p0, v7}, Lorg/apache/poi/hdf/model/hdftypes/ParagraphProperties;->setBrcl(B)V

    goto :goto_0

    .line 878
    :pswitch_9
    int-to-byte v7, p2

    invoke-virtual {p0, v7}, Lorg/apache/poi/hdf/model/hdftypes/ParagraphProperties;->setBrcp(B)V

    goto :goto_0

    .line 881
    :pswitch_a
    int-to-byte v7, p2

    invoke-virtual {p0, v7}, Lorg/apache/poi/hdf/model/hdftypes/ParagraphProperties;->setIlvl(B)V

    goto :goto_0

    .line 884
    :pswitch_b
    invoke-virtual {p0, p2}, Lorg/apache/poi/hdf/model/hdftypes/ParagraphProperties;->setIlfo(I)V

    goto :goto_0

    .line 887
    :pswitch_c
    int-to-byte v7, p2

    invoke-virtual {p0, v7}, Lorg/apache/poi/hdf/model/hdftypes/ParagraphProperties;->setFNoLnn(B)V

    goto :goto_0

    .line 893
    :pswitch_d
    invoke-virtual {p0, p2}, Lorg/apache/poi/hdf/model/hdftypes/ParagraphProperties;->setDxaRight(I)V

    goto :goto_0

    .line 896
    :pswitch_e
    invoke-virtual {p0, p2}, Lorg/apache/poi/hdf/model/hdftypes/ParagraphProperties;->setDxaLeft(I)V

    goto :goto_0

    .line 899
    :pswitch_f
    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/ParagraphProperties;->getDxaLeft()I

    move-result v7

    add-int/2addr v7, p2

    invoke-virtual {p0, v7}, Lorg/apache/poi/hdf/model/hdftypes/ParagraphProperties;->setDxaLeft(I)V

    .line 900
    const/4 v7, 0x0

    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/ParagraphProperties;->getDxaLeft()I

    move-result v8

    invoke-static {v7, v8}, Ljava/lang/Math;->max(II)I

    move-result v7

    invoke-virtual {p0, v7}, Lorg/apache/poi/hdf/model/hdftypes/ParagraphProperties;->setDxaLeft(I)V

    goto/16 :goto_0

    .line 903
    :pswitch_10
    invoke-virtual {p0, p2}, Lorg/apache/poi/hdf/model/hdftypes/ParagraphProperties;->setDxaLeft1(I)V

    goto/16 :goto_0

    .line 906
    :pswitch_11
    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/ParagraphProperties;->getLspd()[S

    move-result-object v6

    .line 907
    .local v6, "lspd":[S
    const/4 v7, 0x0

    add-int/lit8 v8, p5, -0x4

    invoke-static {p4, v8}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v8

    aput-short v8, v6, v7

    .line 908
    const/4 v7, 0x1

    add-int/lit8 v8, p5, -0x2

    invoke-static {p4, v8}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v8

    aput-short v8, v6, v7

    goto/16 :goto_0

    .line 911
    .end local v6    # "lspd":[S
    :pswitch_12
    invoke-virtual {p0, p2}, Lorg/apache/poi/hdf/model/hdftypes/ParagraphProperties;->setDyaBefore(I)V

    goto/16 :goto_0

    .line 914
    :pswitch_13
    invoke-virtual {p0, p2}, Lorg/apache/poi/hdf/model/hdftypes/ParagraphProperties;->setDyaAfter(I)V

    goto/16 :goto_0

    .line 920
    :pswitch_14
    int-to-byte v7, p2

    invoke-virtual {p0, v7}, Lorg/apache/poi/hdf/model/hdftypes/ParagraphProperties;->setFInTable(B)V

    goto/16 :goto_0

    .line 923
    :pswitch_15
    int-to-byte v7, p2

    invoke-virtual {p0, v7}, Lorg/apache/poi/hdf/model/hdftypes/ParagraphProperties;->setFTtp(B)V

    goto/16 :goto_0

    .line 926
    :pswitch_16
    invoke-virtual {p0, p2}, Lorg/apache/poi/hdf/model/hdftypes/ParagraphProperties;->setDxaAbs(I)V

    goto/16 :goto_0

    .line 929
    :pswitch_17
    invoke-virtual {p0, p2}, Lorg/apache/poi/hdf/model/hdftypes/ParagraphProperties;->setDyaAbs(I)V

    goto/16 :goto_0

    .line 932
    :pswitch_18
    invoke-virtual {p0, p2}, Lorg/apache/poi/hdf/model/hdftypes/ParagraphProperties;->setDxaWidth(I)V

    goto/16 :goto_0

    .line 966
    :pswitch_19
    invoke-virtual {p0, p2}, Lorg/apache/poi/hdf/model/hdftypes/ParagraphProperties;->setDxaFromText(I)V

    goto/16 :goto_0

    .line 969
    :pswitch_1a
    int-to-byte v7, p2

    invoke-virtual {p0, v7}, Lorg/apache/poi/hdf/model/hdftypes/ParagraphProperties;->setWr(B)V

    goto/16 :goto_0

    .line 972
    :pswitch_1b
    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/ParagraphProperties;->getBrcTop()[S

    move-result-object v5

    .line 973
    .local v5, "brcTop":[S
    const/4 v7, 0x0

    add-int/lit8 v8, p5, -0x4

    invoke-static {p4, v8}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v8

    aput-short v8, v5, v7

    .line 974
    const/4 v7, 0x1

    add-int/lit8 v8, p5, -0x2

    invoke-static {p4, v8}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v8

    aput-short v8, v5, v7

    goto/16 :goto_0

    .line 977
    .end local v5    # "brcTop":[S
    :pswitch_1c
    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/ParagraphProperties;->getBrcLeft()[S

    move-result-object v3

    .line 978
    .local v3, "brcLeft":[S
    const/4 v7, 0x0

    add-int/lit8 v8, p5, -0x4

    invoke-static {p4, v8}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v8

    aput-short v8, v3, v7

    .line 979
    const/4 v7, 0x1

    add-int/lit8 v8, p5, -0x2

    invoke-static {p4, v8}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v8

    aput-short v8, v3, v7

    goto/16 :goto_0

    .line 982
    .end local v3    # "brcLeft":[S
    :pswitch_1d
    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/ParagraphProperties;->getBrcBottom()[S

    move-result-object v2

    .line 983
    .local v2, "brcBottom":[S
    const/4 v7, 0x0

    add-int/lit8 v8, p5, -0x4

    invoke-static {p4, v8}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v8

    aput-short v8, v2, v7

    .line 984
    const/4 v7, 0x1

    add-int/lit8 v8, p5, -0x2

    invoke-static {p4, v8}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v8

    aput-short v8, v2, v7

    goto/16 :goto_0

    .line 987
    .end local v2    # "brcBottom":[S
    :pswitch_1e
    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/ParagraphProperties;->getBrcRight()[S

    move-result-object v4

    .line 988
    .local v4, "brcRight":[S
    const/4 v7, 0x0

    add-int/lit8 v8, p5, -0x4

    invoke-static {p4, v8}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v8

    aput-short v8, v4, v7

    .line 989
    const/4 v7, 0x1

    add-int/lit8 v8, p5, -0x2

    invoke-static {p4, v8}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v8

    aput-short v8, v4, v7

    goto/16 :goto_0

    .line 992
    .end local v4    # "brcRight":[S
    :pswitch_1f
    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/ParagraphProperties;->getBrcBetween()[S

    move-result-object v1

    .line 993
    .local v1, "brcBetween":[S
    const/4 v7, 0x0

    add-int/lit8 v8, p5, -0x4

    invoke-static {p4, v8}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v8

    aput-short v8, v1, v7

    .line 994
    const/4 v7, 0x1

    add-int/lit8 v8, p5, -0x2

    invoke-static {p4, v8}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v8

    aput-short v8, v1, v7

    goto/16 :goto_0

    .line 997
    .end local v1    # "brcBetween":[S
    :pswitch_20
    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/ParagraphProperties;->getBrcBar()[S

    move-result-object v0

    .line 998
    .local v0, "brcBar":[S
    const/4 v7, 0x0

    add-int/lit8 v8, p5, -0x4

    invoke-static {p4, v8}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v8

    aput-short v8, v0, v7

    .line 999
    const/4 v7, 0x1

    add-int/lit8 v8, p5, -0x2

    invoke-static {p4, v8}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v8

    aput-short v8, v0, v7

    goto/16 :goto_0

    .line 1002
    .end local v0    # "brcBar":[S
    :pswitch_21
    int-to-byte v7, p2

    invoke-virtual {p0, v7}, Lorg/apache/poi/hdf/model/hdftypes/ParagraphProperties;->setFNoAutoHyph(B)V

    goto/16 :goto_0

    .line 1005
    :pswitch_22
    invoke-virtual {p0, p2}, Lorg/apache/poi/hdf/model/hdftypes/ParagraphProperties;->setDyaHeight(I)V

    goto/16 :goto_0

    .line 1008
    :pswitch_23
    int-to-short v7, p2

    invoke-virtual {p0, v7}, Lorg/apache/poi/hdf/model/hdftypes/ParagraphProperties;->setDcs(S)V

    goto/16 :goto_0

    .line 1011
    :pswitch_24
    int-to-short v7, p2

    invoke-virtual {p0, v7}, Lorg/apache/poi/hdf/model/hdftypes/ParagraphProperties;->setShd(S)V

    goto/16 :goto_0

    .line 1014
    :pswitch_25
    invoke-virtual {p0, p2}, Lorg/apache/poi/hdf/model/hdftypes/ParagraphProperties;->setDyaFromText(I)V

    goto/16 :goto_0

    .line 1017
    :pswitch_26
    invoke-virtual {p0, p2}, Lorg/apache/poi/hdf/model/hdftypes/ParagraphProperties;->setDxaFromText(I)V

    goto/16 :goto_0

    .line 1020
    :pswitch_27
    int-to-byte v7, p2

    invoke-virtual {p0, v7}, Lorg/apache/poi/hdf/model/hdftypes/ParagraphProperties;->setFLocked(B)V

    goto/16 :goto_0

    .line 1023
    :pswitch_28
    int-to-byte v7, p2

    invoke-virtual {p0, v7}, Lorg/apache/poi/hdf/model/hdftypes/ParagraphProperties;->setFWidowControl(B)V

    goto/16 :goto_0

    .line 1029
    :pswitch_29
    int-to-byte v7, p2

    invoke-virtual {p0, v7}, Lorg/apache/poi/hdf/model/hdftypes/ParagraphProperties;->setFKinsoku(B)V

    goto/16 :goto_0

    .line 1032
    :pswitch_2a
    int-to-byte v7, p2

    invoke-virtual {p0, v7}, Lorg/apache/poi/hdf/model/hdftypes/ParagraphProperties;->setFWordWrap(B)V

    goto/16 :goto_0

    .line 1035
    :pswitch_2b
    int-to-byte v7, p2

    invoke-virtual {p0, v7}, Lorg/apache/poi/hdf/model/hdftypes/ParagraphProperties;->setFOverflowPunct(B)V

    goto/16 :goto_0

    .line 1038
    :pswitch_2c
    int-to-byte v7, p2

    invoke-virtual {p0, v7}, Lorg/apache/poi/hdf/model/hdftypes/ParagraphProperties;->setFTopLinePunct(B)V

    goto/16 :goto_0

    .line 1041
    :pswitch_2d
    int-to-byte v7, p2

    invoke-virtual {p0, v7}, Lorg/apache/poi/hdf/model/hdftypes/ParagraphProperties;->setFAutoSpaceDE(B)V

    goto/16 :goto_0

    .line 1044
    :pswitch_2e
    int-to-byte v7, p2

    invoke-virtual {p0, v7}, Lorg/apache/poi/hdf/model/hdftypes/ParagraphProperties;->setFAutoSpaceDN(B)V

    goto/16 :goto_0

    .line 1047
    :pswitch_2f
    invoke-virtual {p0, p2}, Lorg/apache/poi/hdf/model/hdftypes/ParagraphProperties;->setWAlignFont(I)V

    goto/16 :goto_0

    .line 1050
    :pswitch_30
    int-to-short v7, p2

    invoke-virtual {p0, v7}, Lorg/apache/poi/hdf/model/hdftypes/ParagraphProperties;->setFontAlign(S)V

    goto/16 :goto_0

    .line 1056
    :pswitch_31
    invoke-virtual {p0, p3}, Lorg/apache/poi/hdf/model/hdftypes/ParagraphProperties;->setAnld([B)V

    goto/16 :goto_0

    .line 1075
    :pswitch_32
    const/4 v7, 0x6

    if-ne p6, v7, :cond_0

    .line 1077
    invoke-virtual {p0, p3}, Lorg/apache/poi/hdf/model/hdftypes/ParagraphProperties;->setNumrm([B)V

    goto/16 :goto_0

    .line 1086
    :pswitch_33
    int-to-byte v7, p2

    invoke-virtual {p0, v7}, Lorg/apache/poi/hdf/model/hdftypes/ParagraphProperties;->setFUsePgsuSettings(B)V

    goto/16 :goto_0

    .line 1089
    :pswitch_34
    int-to-byte v7, p2

    invoke-virtual {p0, v7}, Lorg/apache/poi/hdf/model/hdftypes/ParagraphProperties;->setFAdjustRight(B)V

    goto/16 :goto_0

    .line 837
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_0
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_0
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_19
        :pswitch_1a
        :pswitch_1b
        :pswitch_1c
        :pswitch_1d
        :pswitch_1e
        :pswitch_1f
        :pswitch_20
        :pswitch_21
        :pswitch_22
        :pswitch_23
        :pswitch_24
        :pswitch_25
        :pswitch_26
        :pswitch_27
        :pswitch_28
        :pswitch_0
        :pswitch_29
        :pswitch_2a
        :pswitch_2b
        :pswitch_2c
        :pswitch_2d
        :pswitch_2e
        :pswitch_2f
        :pswitch_30
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_31
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_32
        :pswitch_0
        :pswitch_33
        :pswitch_34
    .end packed-switch
.end method

.method static doSEPOperation(Lorg/apache/poi/hdf/model/hdftypes/SectionProperties;II[B)V
    .locals 8
    .param p0, "newSEP"    # Lorg/apache/poi/hdf/model/hdftypes/SectionProperties;
    .param p1, "operand"    # I
    .param p2, "param"    # I
    .param p3, "varParam"    # [B

    .prologue
    const v4, 0xffff

    const/4 v7, 0x1

    const/high16 v6, -0x10000

    const/4 v5, 0x0

    .line 1289
    packed-switch p1, :pswitch_data_0

    .line 1456
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 1292
    :pswitch_1
    int-to-byte v4, p2

    invoke-virtual {p0, v4}, Lorg/apache/poi/hdf/model/hdftypes/SectionProperties;->setCnsPgn(B)V

    goto :goto_0

    .line 1295
    :pswitch_2
    int-to-byte v4, p2

    invoke-virtual {p0, v4}, Lorg/apache/poi/hdf/model/hdftypes/SectionProperties;->setIHeadingPgn(B)V

    goto :goto_0

    .line 1298
    :pswitch_3
    invoke-virtual {p0, p3}, Lorg/apache/poi/hdf/model/hdftypes/SectionProperties;->setOlstAnm([B)V

    goto :goto_0

    .line 1307
    :pswitch_4
    invoke-static {p2}, Lorg/apache/poi/hdf/model/hdftypes/StyleSheet;->getFlag(I)Z

    move-result v4

    invoke-virtual {p0, v4}, Lorg/apache/poi/hdf/model/hdftypes/SectionProperties;->setFEvenlySpaced(Z)V

    goto :goto_0

    .line 1310
    :pswitch_5
    invoke-static {p2}, Lorg/apache/poi/hdf/model/hdftypes/StyleSheet;->getFlag(I)Z

    move-result v4

    invoke-virtual {p0, v4}, Lorg/apache/poi/hdf/model/hdftypes/SectionProperties;->setFUnlocked(Z)V

    goto :goto_0

    .line 1313
    :pswitch_6
    int-to-short v4, p2

    invoke-virtual {p0, v4}, Lorg/apache/poi/hdf/model/hdftypes/SectionProperties;->setDmBinFirst(I)V

    goto :goto_0

    .line 1316
    :pswitch_7
    int-to-short v4, p2

    invoke-virtual {p0, v4}, Lorg/apache/poi/hdf/model/hdftypes/SectionProperties;->setDmBinOther(I)V

    goto :goto_0

    .line 1319
    :pswitch_8
    int-to-byte v4, p2

    invoke-virtual {p0, v4}, Lorg/apache/poi/hdf/model/hdftypes/SectionProperties;->setBkc(B)V

    goto :goto_0

    .line 1322
    :pswitch_9
    invoke-static {p2}, Lorg/apache/poi/hdf/model/hdftypes/StyleSheet;->getFlag(I)Z

    move-result v4

    invoke-virtual {p0, v4}, Lorg/apache/poi/hdf/model/hdftypes/SectionProperties;->setFTitlePage(Z)V

    goto :goto_0

    .line 1325
    :pswitch_a
    int-to-short v4, p2

    invoke-virtual {p0, v4}, Lorg/apache/poi/hdf/model/hdftypes/SectionProperties;->setCcolM1(I)V

    goto :goto_0

    .line 1328
    :pswitch_b
    invoke-virtual {p0, p2}, Lorg/apache/poi/hdf/model/hdftypes/SectionProperties;->setDxaColumns(I)V

    goto :goto_0

    .line 1331
    :pswitch_c
    invoke-static {p2}, Lorg/apache/poi/hdf/model/hdftypes/StyleSheet;->getFlag(I)Z

    move-result v4

    invoke-virtual {p0, v4}, Lorg/apache/poi/hdf/model/hdftypes/SectionProperties;->setFAutoPgn(Z)V

    goto :goto_0

    .line 1334
    :pswitch_d
    int-to-byte v4, p2

    invoke-virtual {p0, v4}, Lorg/apache/poi/hdf/model/hdftypes/SectionProperties;->setNfcPgn(B)V

    goto :goto_0

    .line 1337
    :pswitch_e
    int-to-short v4, p2

    invoke-virtual {p0, v4}, Lorg/apache/poi/hdf/model/hdftypes/SectionProperties;->setDyaPgn(I)V

    goto :goto_0

    .line 1340
    :pswitch_f
    int-to-short v4, p2

    invoke-virtual {p0, v4}, Lorg/apache/poi/hdf/model/hdftypes/SectionProperties;->setDxaPgn(I)V

    goto :goto_0

    .line 1343
    :pswitch_10
    invoke-static {p2}, Lorg/apache/poi/hdf/model/hdftypes/StyleSheet;->getFlag(I)Z

    move-result v4

    invoke-virtual {p0, v4}, Lorg/apache/poi/hdf/model/hdftypes/SectionProperties;->setFPgnRestart(Z)V

    goto :goto_0

    .line 1346
    :pswitch_11
    invoke-static {p2}, Lorg/apache/poi/hdf/model/hdftypes/StyleSheet;->getFlag(I)Z

    move-result v4

    invoke-virtual {p0, v4}, Lorg/apache/poi/hdf/model/hdftypes/SectionProperties;->setFEndNote(Z)V

    goto :goto_0

    .line 1349
    :pswitch_12
    int-to-byte v4, p2

    invoke-virtual {p0, v4}, Lorg/apache/poi/hdf/model/hdftypes/SectionProperties;->setLnc(B)V

    goto :goto_0

    .line 1352
    :pswitch_13
    int-to-byte v4, p2

    invoke-virtual {p0, v4}, Lorg/apache/poi/hdf/model/hdftypes/SectionProperties;->setGrpfIhdt(B)V

    goto :goto_0

    .line 1355
    :pswitch_14
    int-to-short v4, p2

    invoke-virtual {p0, v4}, Lorg/apache/poi/hdf/model/hdftypes/SectionProperties;->setNLnnMod(I)V

    goto :goto_0

    .line 1358
    :pswitch_15
    invoke-virtual {p0, p2}, Lorg/apache/poi/hdf/model/hdftypes/SectionProperties;->setDxaLnn(I)V

    goto :goto_0

    .line 1361
    :pswitch_16
    invoke-virtual {p0, p2}, Lorg/apache/poi/hdf/model/hdftypes/SectionProperties;->setDyaHdrTop(I)V

    goto :goto_0

    .line 1364
    :pswitch_17
    invoke-virtual {p0, p2}, Lorg/apache/poi/hdf/model/hdftypes/SectionProperties;->setDyaHdrBottom(I)V

    goto :goto_0

    .line 1367
    :pswitch_18
    invoke-static {p2}, Lorg/apache/poi/hdf/model/hdftypes/StyleSheet;->getFlag(I)Z

    move-result v4

    invoke-virtual {p0, v4}, Lorg/apache/poi/hdf/model/hdftypes/SectionProperties;->setFLBetween(Z)V

    goto/16 :goto_0

    .line 1370
    :pswitch_19
    int-to-byte v4, p2

    invoke-virtual {p0, v4}, Lorg/apache/poi/hdf/model/hdftypes/SectionProperties;->setVjc(B)V

    goto/16 :goto_0

    .line 1373
    :pswitch_1a
    int-to-short v4, p2

    invoke-virtual {p0, v4}, Lorg/apache/poi/hdf/model/hdftypes/SectionProperties;->setLnnMin(I)V

    goto/16 :goto_0

    .line 1376
    :pswitch_1b
    int-to-short v4, p2

    invoke-virtual {p0, v4}, Lorg/apache/poi/hdf/model/hdftypes/SectionProperties;->setPgnStart(I)V

    goto/16 :goto_0

    .line 1379
    :pswitch_1c
    int-to-byte v4, p2

    invoke-virtual {p0, v4}, Lorg/apache/poi/hdf/model/hdftypes/SectionProperties;->setDmOrientPage(B)V

    goto/16 :goto_0

    .line 1385
    :pswitch_1d
    invoke-virtual {p0, p2}, Lorg/apache/poi/hdf/model/hdftypes/SectionProperties;->setXaPage(I)V

    goto/16 :goto_0

    .line 1388
    :pswitch_1e
    invoke-virtual {p0, p2}, Lorg/apache/poi/hdf/model/hdftypes/SectionProperties;->setYaPage(I)V

    goto/16 :goto_0

    .line 1391
    :pswitch_1f
    invoke-virtual {p0, p2}, Lorg/apache/poi/hdf/model/hdftypes/SectionProperties;->setDxaLeft(I)V

    goto/16 :goto_0

    .line 1394
    :pswitch_20
    invoke-virtual {p0, p2}, Lorg/apache/poi/hdf/model/hdftypes/SectionProperties;->setDxaRight(I)V

    goto/16 :goto_0

    .line 1397
    :pswitch_21
    invoke-virtual {p0, p2}, Lorg/apache/poi/hdf/model/hdftypes/SectionProperties;->setDyaTop(I)V

    goto/16 :goto_0

    .line 1400
    :pswitch_22
    invoke-virtual {p0, p2}, Lorg/apache/poi/hdf/model/hdftypes/SectionProperties;->setDyaBottom(I)V

    goto/16 :goto_0

    .line 1403
    :pswitch_23
    invoke-virtual {p0, p2}, Lorg/apache/poi/hdf/model/hdftypes/SectionProperties;->setDzaGutter(I)V

    goto/16 :goto_0

    .line 1406
    :pswitch_24
    int-to-short v4, p2

    invoke-virtual {p0, v4}, Lorg/apache/poi/hdf/model/hdftypes/SectionProperties;->setDmPaperReq(I)V

    goto/16 :goto_0

    .line 1410
    :pswitch_25
    if-eqz p3, :cond_0

    .line 1411
    aget-byte v4, p3, v5

    invoke-static {v4}, Lorg/apache/poi/hdf/model/hdftypes/StyleSheet;->getFlag(I)Z

    move-result v4

    invoke-virtual {p0, v4}, Lorg/apache/poi/hdf/model/hdftypes/SectionProperties;->setFPropMark(Z)V

    goto/16 :goto_0

    .line 1421
    :pswitch_26
    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/SectionProperties;->getBrcTop()[S

    move-result-object v3

    .line 1422
    .local v3, "brcTop":[S
    and-int/2addr v4, p2

    int-to-short v4, v4

    aput-short v4, v3, v5

    .line 1423
    and-int v4, p2, v6

    shr-int/lit8 v4, v4, 0x10

    int-to-short v4, v4

    aput-short v4, v3, v7

    goto/16 :goto_0

    .line 1426
    .end local v3    # "brcTop":[S
    :pswitch_27
    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/SectionProperties;->getBrcLeft()[S

    move-result-object v1

    .line 1427
    .local v1, "brcLeft":[S
    and-int/2addr v4, p2

    int-to-short v4, v4

    aput-short v4, v1, v5

    .line 1428
    and-int v4, p2, v6

    shr-int/lit8 v4, v4, 0x10

    int-to-short v4, v4

    aput-short v4, v1, v7

    goto/16 :goto_0

    .line 1431
    .end local v1    # "brcLeft":[S
    :pswitch_28
    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/SectionProperties;->getBrcBottom()[S

    move-result-object v0

    .line 1432
    .local v0, "brcBottom":[S
    and-int/2addr v4, p2

    int-to-short v4, v4

    aput-short v4, v0, v5

    .line 1433
    and-int v4, p2, v6

    shr-int/lit8 v4, v4, 0x10

    int-to-short v4, v4

    aput-short v4, v0, v7

    goto/16 :goto_0

    .line 1436
    .end local v0    # "brcBottom":[S
    :pswitch_29
    invoke-virtual {p0}, Lorg/apache/poi/hdf/model/hdftypes/SectionProperties;->getBrcRight()[S

    move-result-object v2

    .line 1437
    .local v2, "brcRight":[S
    and-int/2addr v4, p2

    int-to-short v4, v4

    aput-short v4, v2, v5

    .line 1438
    and-int v4, p2, v6

    shr-int/lit8 v4, v4, 0x10

    int-to-short v4, v4

    aput-short v4, v2, v7

    goto/16 :goto_0

    .line 1441
    .end local v2    # "brcRight":[S
    :pswitch_2a
    invoke-virtual {p0, p2}, Lorg/apache/poi/hdf/model/hdftypes/SectionProperties;->setPgbProp(I)V

    goto/16 :goto_0

    .line 1444
    :pswitch_2b
    invoke-virtual {p0, p2}, Lorg/apache/poi/hdf/model/hdftypes/SectionProperties;->setDxtCharSpace(I)V

    goto/16 :goto_0

    .line 1447
    :pswitch_2c
    invoke-virtual {p0, p2}, Lorg/apache/poi/hdf/model/hdftypes/SectionProperties;->setDyaLinePitch(I)V

    goto/16 :goto_0

    .line 1450
    :pswitch_2d
    int-to-short v4, p2

    invoke-virtual {p0, v4}, Lorg/apache/poi/hdf/model/hdftypes/SectionProperties;->setWTextFlow(I)V

    goto/16 :goto_0

    .line 1289
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_19
        :pswitch_1a
        :pswitch_1b
        :pswitch_1c
        :pswitch_0
        :pswitch_1d
        :pswitch_1e
        :pswitch_1f
        :pswitch_20
        :pswitch_21
        :pswitch_22
        :pswitch_23
        :pswitch_24
        :pswitch_25
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_26
        :pswitch_27
        :pswitch_28
        :pswitch_29
        :pswitch_2a
        :pswitch_2b
        :pswitch_2c
        :pswitch_0
        :pswitch_2d
    .end packed-switch
.end method

.method static doTAPOperation(Lorg/apache/poi/hdf/model/hdftypes/TableProperties;II[B)V
    .locals 22
    .param p0, "newTAP"    # Lorg/apache/poi/hdf/model/hdftypes/TableProperties;
    .param p1, "operand"    # I
    .param p2, "param"    # I
    .param p3, "varParam"    # [B

    .prologue
    .line 1106
    packed-switch p1, :pswitch_data_0

    .line 1277
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 1109
    :pswitch_1
    move/from16 v0, p2

    int-to-short v0, v0

    move/from16 v18, v0

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lorg/apache/poi/hdf/model/hdftypes/TableProperties;->setJc(I)V

    goto :goto_0

    .line 1113
    :pswitch_2
    invoke-virtual/range {p0 .. p0}, Lorg/apache/poi/hdf/model/hdftypes/TableProperties;->getRgdxaCenter()[S

    move-result-object v14

    .line 1114
    .local v14, "rgdxaCenter":[S
    invoke-virtual/range {p0 .. p0}, Lorg/apache/poi/hdf/model/hdftypes/TableProperties;->getItcMac()S

    move-result v13

    .line 1115
    .local v13, "itcMac":S
    const/16 v18, 0x0

    aget-short v18, v14, v18

    invoke-virtual/range {p0 .. p0}, Lorg/apache/poi/hdf/model/hdftypes/TableProperties;->getDxaGapHalf()I

    move-result v19

    add-int v18, v18, v19

    sub-int v4, p2, v18

    .line 1116
    .local v4, "adjust":I
    const/16 v17, 0x0

    .local v17, "x":I
    :goto_1
    move/from16 v0, v17

    if-ge v0, v13, :cond_0

    .line 1118
    aget-short v18, v14, v17

    add-int v18, v18, v4

    move/from16 v0, v18

    int-to-short v0, v0

    move/from16 v18, v0

    aput-short v18, v14, v17

    .line 1116
    add-int/lit8 v17, v17, 0x1

    goto :goto_1

    .line 1124
    .end local v4    # "adjust":I
    .end local v13    # "itcMac":S
    .end local v14    # "rgdxaCenter":[S
    .end local v17    # "x":I
    :pswitch_3
    invoke-virtual/range {p0 .. p0}, Lorg/apache/poi/hdf/model/hdftypes/TableProperties;->getRgdxaCenter()[S

    move-result-object v14

    .line 1125
    .restart local v14    # "rgdxaCenter":[S
    if-eqz v14, :cond_1

    .line 1127
    invoke-virtual/range {p0 .. p0}, Lorg/apache/poi/hdf/model/hdftypes/TableProperties;->getDxaGapHalf()I

    move-result v18

    sub-int v4, v18, p2

    .line 1128
    .restart local v4    # "adjust":I
    const/16 v18, 0x0

    aget-short v19, v14, v18

    add-int v19, v19, v4

    move/from16 v0, v19

    int-to-short v0, v0

    move/from16 v19, v0

    aput-short v19, v14, v18

    .line 1130
    .end local v4    # "adjust":I
    :cond_1
    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Lorg/apache/poi/hdf/model/hdftypes/TableProperties;->setDxaGapHalf(I)V

    goto :goto_0

    .line 1134
    .end local v14    # "rgdxaCenter":[S
    :pswitch_4
    invoke-static/range {p2 .. p2}, Lorg/apache/poi/hdf/model/hdftypes/StyleSheet;->getFlag(I)Z

    move-result v18

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lorg/apache/poi/hdf/model/hdftypes/TableProperties;->setFCantSplit(Z)V

    goto :goto_0

    .line 1137
    :pswitch_5
    invoke-static/range {p2 .. p2}, Lorg/apache/poi/hdf/model/hdftypes/StyleSheet;->getFlag(I)Z

    move-result v18

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lorg/apache/poi/hdf/model/hdftypes/TableProperties;->setFTableHeader(Z)V

    goto :goto_0

    .line 1141
    :pswitch_6
    invoke-virtual/range {p0 .. p0}, Lorg/apache/poi/hdf/model/hdftypes/TableProperties;->getBrcTop()[S

    move-result-object v9

    .line 1142
    .local v9, "brcTop":[S
    invoke-virtual/range {p0 .. p0}, Lorg/apache/poi/hdf/model/hdftypes/TableProperties;->getBrcLeft()[S

    move-result-object v7

    .line 1143
    .local v7, "brcLeft":[S
    invoke-virtual/range {p0 .. p0}, Lorg/apache/poi/hdf/model/hdftypes/TableProperties;->getBrcBottom()[S

    move-result-object v5

    .line 1144
    .local v5, "brcBottom":[S
    invoke-virtual/range {p0 .. p0}, Lorg/apache/poi/hdf/model/hdftypes/TableProperties;->getBrcRight()[S

    move-result-object v8

    .line 1145
    .local v8, "brcRight":[S
    invoke-virtual/range {p0 .. p0}, Lorg/apache/poi/hdf/model/hdftypes/TableProperties;->getBrcVertical()[S

    move-result-object v10

    .line 1146
    .local v10, "brcVertical":[S
    invoke-virtual/range {p0 .. p0}, Lorg/apache/poi/hdf/model/hdftypes/TableProperties;->getBrcHorizontal()[S

    move-result-object v6

    .line 1148
    .local v6, "brcHorizontal":[S
    if-eqz p3, :cond_0

    .line 1149
    const/16 v18, 0x0

    const/16 v19, 0x0

    move-object/from16 v0, p3

    move/from16 v1, v19

    invoke-static {v0, v1}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v19

    aput-short v19, v9, v18

    .line 1150
    const/16 v18, 0x1

    const/16 v19, 0x2

    move-object/from16 v0, p3

    move/from16 v1, v19

    invoke-static {v0, v1}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v19

    aput-short v19, v9, v18

    .line 1152
    const/16 v18, 0x0

    const/16 v19, 0x4

    move-object/from16 v0, p3

    move/from16 v1, v19

    invoke-static {v0, v1}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v19

    aput-short v19, v7, v18

    .line 1153
    const/16 v18, 0x1

    const/16 v19, 0x6

    move-object/from16 v0, p3

    move/from16 v1, v19

    invoke-static {v0, v1}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v19

    aput-short v19, v7, v18

    .line 1155
    const/16 v18, 0x0

    const/16 v19, 0x8

    move-object/from16 v0, p3

    move/from16 v1, v19

    invoke-static {v0, v1}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v19

    aput-short v19, v5, v18

    .line 1156
    const/16 v18, 0x1

    const/16 v19, 0xa

    move-object/from16 v0, p3

    move/from16 v1, v19

    invoke-static {v0, v1}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v19

    aput-short v19, v5, v18

    .line 1158
    const/16 v18, 0x0

    const/16 v19, 0xc

    move-object/from16 v0, p3

    move/from16 v1, v19

    invoke-static {v0, v1}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v19

    aput-short v19, v8, v18

    .line 1159
    const/16 v18, 0x1

    const/16 v19, 0xe

    move-object/from16 v0, p3

    move/from16 v1, v19

    invoke-static {v0, v1}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v19

    aput-short v19, v8, v18

    .line 1161
    const/16 v18, 0x0

    const/16 v19, 0x10

    move-object/from16 v0, p3

    move/from16 v1, v19

    invoke-static {v0, v1}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v19

    aput-short v19, v6, v18

    .line 1162
    const/16 v18, 0x1

    const/16 v19, 0x12

    move-object/from16 v0, p3

    move/from16 v1, v19

    invoke-static {v0, v1}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v19

    aput-short v19, v6, v18

    .line 1164
    const/16 v18, 0x0

    const/16 v19, 0x14

    move-object/from16 v0, p3

    move/from16 v1, v19

    invoke-static {v0, v1}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v19

    aput-short v19, v10, v18

    .line 1165
    const/16 v18, 0x1

    const/16 v19, 0x16

    move-object/from16 v0, p3

    move/from16 v1, v19

    invoke-static {v0, v1}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v19

    aput-short v19, v10, v18

    goto/16 :goto_0

    .line 1173
    .end local v5    # "brcBottom":[S
    .end local v6    # "brcHorizontal":[S
    .end local v7    # "brcLeft":[S
    .end local v8    # "brcRight":[S
    .end local v9    # "brcTop":[S
    .end local v10    # "brcVertical":[S
    :pswitch_7
    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Lorg/apache/poi/hdf/model/hdftypes/TableProperties;->setDyaRowHeight(I)V

    goto/16 :goto_0

    .line 1177
    :pswitch_8
    if-eqz p3, :cond_0

    .line 1178
    const/16 v18, 0x0

    aget-byte v18, p3, v18

    add-int/lit8 v18, v18, 0x1

    move/from16 v0, v18

    new-array v14, v0, [S

    .line 1179
    .restart local v14    # "rgdxaCenter":[S
    const/16 v18, 0x0

    aget-byte v18, p3, v18

    move/from16 v0, v18

    new-array v15, v0, [Lorg/apache/poi/hdf/model/hdftypes/TableCellDescriptor;

    .line 1180
    .local v15, "rgtc":[Lorg/apache/poi/hdf/model/hdftypes/TableCellDescriptor;
    const/16 v18, 0x0

    aget-byte v13, p3, v18

    .line 1182
    .restart local v13    # "itcMac":S
    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lorg/apache/poi/hdf/model/hdftypes/TableProperties;->setItcMac(S)V

    .line 1183
    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lorg/apache/poi/hdf/model/hdftypes/TableProperties;->setRgdxaCenter([S)V

    .line 1184
    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lorg/apache/poi/hdf/model/hdftypes/TableProperties;->setRgtc([Lorg/apache/poi/hdf/model/hdftypes/definitions/TCAbstractType;)V

    .line 1186
    const/16 v17, 0x0

    .restart local v17    # "x":I
    :goto_2
    move/from16 v0, v17

    if-lt v0, v13, :cond_2

    .line 1193
    mul-int/lit8 v18, v13, 0x2

    add-int/lit8 v18, v18, 0x1

    .line 1192
    move-object/from16 v0, p3

    move/from16 v1, v18

    invoke-static {v0, v1}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v18

    aput-short v18, v14, v13

    goto/16 :goto_0

    .line 1188
    :cond_2
    mul-int/lit8 v18, v17, 0x2

    add-int/lit8 v18, v18, 0x1

    .line 1187
    move-object/from16 v0, p3

    move/from16 v1, v18

    invoke-static {v0, v1}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v18

    aput-short v18, v14, v17

    .line 1190
    add-int/lit8 v18, v13, 0x1

    mul-int/lit8 v18, v18, 0x2

    add-int/lit8 v18, v18, 0x1

    mul-int/lit8 v19, v17, 0x14

    add-int v18, v18, v19

    .line 1189
    move-object/from16 v0, p3

    move/from16 v1, v18

    invoke-static {v0, v1}, Lorg/apache/poi/hdf/model/hdftypes/TableCellDescriptor;->convertBytesToTC([BI)Lorg/apache/poi/hdf/model/hdftypes/TableCellDescriptor;

    move-result-object v18

    aput-object v18, v15, v17

    .line 1186
    add-int/lit8 v17, v17, 0x1

    goto :goto_2

    .line 1205
    .end local v13    # "itcMac":S
    .end local v14    # "rgdxaCenter":[S
    .end local v15    # "rgtc":[Lorg/apache/poi/hdf/model/hdftypes/TableCellDescriptor;
    .end local v17    # "x":I
    :pswitch_9
    invoke-virtual/range {p0 .. p0}, Lorg/apache/poi/hdf/model/hdftypes/TableProperties;->getRgtc()[Lorg/apache/poi/hdf/model/hdftypes/definitions/TCAbstractType;

    move-result-object v15

    .line 1206
    .local v15, "rgtc":[Lorg/apache/poi/hdf/model/hdftypes/definitions/TCAbstractType;
    if-eqz p3, :cond_0

    .line 1207
    const/16 v18, 0x0

    aget-byte v17, p3, v18

    .restart local v17    # "x":I
    :goto_3
    const/16 v18, 0x1

    aget-byte v18, p3, v18

    move/from16 v0, v17

    move/from16 v1, v18

    if-ge v0, v1, :cond_0

    .line 1209
    const/16 v18, 0x2

    aget-byte v18, p3, v18

    and-int/lit8 v18, v18, 0x8

    if-lez v18, :cond_4

    .line 1210
    aget-object v18, v15, v17

    invoke-virtual/range {v18 .. v18}, Lorg/apache/poi/hdf/model/hdftypes/definitions/TCAbstractType;->getBrcRight()[S

    move-result-object v8

    .line 1211
    .restart local v8    # "brcRight":[S
    const/16 v18, 0x0

    const/16 v19, 0x6

    move-object/from16 v0, p3

    move/from16 v1, v19

    invoke-static {v0, v1}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v19

    aput-short v19, v8, v18

    .line 1212
    const/16 v18, 0x1

    const/16 v19, 0x8

    move-object/from16 v0, p3

    move/from16 v1, v19

    invoke-static {v0, v1}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v19

    aput-short v19, v8, v18

    .line 1207
    .end local v8    # "brcRight":[S
    :cond_3
    :goto_4
    add-int/lit8 v17, v17, 0x1

    goto :goto_3

    .line 1213
    :cond_4
    const/16 v18, 0x2

    aget-byte v18, p3, v18

    and-int/lit8 v18, v18, 0x4

    if-lez v18, :cond_5

    .line 1214
    aget-object v18, v15, v17

    invoke-virtual/range {v18 .. v18}, Lorg/apache/poi/hdf/model/hdftypes/definitions/TCAbstractType;->getBrcBottom()[S

    move-result-object v5

    .line 1215
    .restart local v5    # "brcBottom":[S
    const/16 v18, 0x0

    const/16 v19, 0x6

    move-object/from16 v0, p3

    move/from16 v1, v19

    invoke-static {v0, v1}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v19

    aput-short v19, v5, v18

    .line 1216
    const/16 v18, 0x1

    const/16 v19, 0x8

    move-object/from16 v0, p3

    move/from16 v1, v19

    invoke-static {v0, v1}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v19

    aput-short v19, v5, v18

    goto :goto_4

    .line 1217
    .end local v5    # "brcBottom":[S
    :cond_5
    const/16 v18, 0x2

    aget-byte v18, p3, v18

    and-int/lit8 v18, v18, 0x2

    if-lez v18, :cond_6

    .line 1218
    aget-object v18, v15, v17

    invoke-virtual/range {v18 .. v18}, Lorg/apache/poi/hdf/model/hdftypes/definitions/TCAbstractType;->getBrcLeft()[S

    move-result-object v7

    .line 1219
    .restart local v7    # "brcLeft":[S
    const/16 v18, 0x0

    const/16 v19, 0x6

    move-object/from16 v0, p3

    move/from16 v1, v19

    invoke-static {v0, v1}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v19

    aput-short v19, v7, v18

    .line 1220
    const/16 v18, 0x1

    const/16 v19, 0x8

    move-object/from16 v0, p3

    move/from16 v1, v19

    invoke-static {v0, v1}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v19

    aput-short v19, v7, v18

    goto :goto_4

    .line 1221
    .end local v7    # "brcLeft":[S
    :cond_6
    const/16 v18, 0x2

    aget-byte v18, p3, v18

    and-int/lit8 v18, v18, 0x1

    if-lez v18, :cond_3

    .line 1222
    aget-object v18, v15, v17

    invoke-virtual/range {v18 .. v18}, Lorg/apache/poi/hdf/model/hdftypes/definitions/TCAbstractType;->getBrcTop()[S

    move-result-object v9

    .line 1223
    .restart local v9    # "brcTop":[S
    const/16 v18, 0x0

    const/16 v19, 0x6

    move-object/from16 v0, p3

    move/from16 v1, v19

    invoke-static {v0, v1}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v19

    aput-short v19, v9, v18

    .line 1224
    const/16 v18, 0x1

    const/16 v19, 0x8

    move-object/from16 v0, p3

    move/from16 v1, v19

    invoke-static {v0, v1}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v19

    aput-short v19, v9, v18

    goto/16 :goto_4

    .line 1231
    .end local v9    # "brcTop":[S
    .end local v15    # "rgtc":[Lorg/apache/poi/hdf/model/hdftypes/definitions/TCAbstractType;
    .end local v17    # "x":I
    :pswitch_a
    const/high16 v18, -0x1000000

    and-int v18, v18, p2

    shr-int/lit8 v12, v18, 0x18

    .line 1232
    .local v12, "index":I
    const/high16 v18, 0xff0000

    and-int v18, v18, p2

    shr-int/lit8 v11, v18, 0x10

    .line 1233
    .local v11, "count":I
    const v18, 0xffff

    and-int v16, p2, v18

    .line 1234
    .local v16, "width":I
    invoke-virtual/range {p0 .. p0}, Lorg/apache/poi/hdf/model/hdftypes/TableProperties;->getItcMac()S

    move-result v13

    .line 1236
    .local v13, "itcMac":I
    add-int v18, v13, v11

    add-int/lit8 v18, v18, 0x1

    move/from16 v0, v18

    new-array v14, v0, [S

    .line 1237
    .restart local v14    # "rgdxaCenter":[S
    add-int v18, v13, v11

    move/from16 v0, v18

    new-array v15, v0, [Lorg/apache/poi/hdf/model/hdftypes/TableCellDescriptor;

    .line 1238
    .local v15, "rgtc":[Lorg/apache/poi/hdf/model/hdftypes/TableCellDescriptor;
    if-lt v12, v13, :cond_7

    .line 1240
    move v12, v13

    .line 1241
    invoke-virtual/range {p0 .. p0}, Lorg/apache/poi/hdf/model/hdftypes/TableProperties;->getRgdxaCenter()[S

    move-result-object v18

    const/16 v19, 0x0

    const/16 v20, 0x0

    add-int/lit8 v21, v13, 0x1

    move-object/from16 v0, v18

    move/from16 v1, v19

    move/from16 v2, v20

    move/from16 v3, v21

    invoke-static {v0, v1, v14, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1242
    invoke-virtual/range {p0 .. p0}, Lorg/apache/poi/hdf/model/hdftypes/TableProperties;->getRgtc()[Lorg/apache/poi/hdf/model/hdftypes/definitions/TCAbstractType;

    move-result-object v18

    const/16 v19, 0x0

    const/16 v20, 0x0

    move-object/from16 v0, v18

    move/from16 v1, v19

    move/from16 v2, v20

    invoke-static {v0, v1, v15, v2, v13}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1254
    :goto_5
    move/from16 v17, v12

    .restart local v17    # "x":I
    :goto_6
    add-int v18, v12, v11

    move/from16 v0, v17

    move/from16 v1, v18

    if-lt v0, v1, :cond_8

    .line 1259
    add-int v18, v12, v11

    add-int v19, v12, v11

    add-int/lit8 v19, v19, -0x1

    aget-short v19, v14, v19

    add-int v19, v19, v16

    move/from16 v0, v19

    int-to-short v0, v0

    move/from16 v19, v0

    aput-short v19, v14, v18

    goto/16 :goto_0

    .line 1247
    .end local v17    # "x":I
    :cond_7
    invoke-virtual/range {p0 .. p0}, Lorg/apache/poi/hdf/model/hdftypes/TableProperties;->getRgdxaCenter()[S

    move-result-object v18

    const/16 v19, 0x0

    const/16 v20, 0x0

    add-int/lit8 v21, v12, 0x1

    move-object/from16 v0, v18

    move/from16 v1, v19

    move/from16 v2, v20

    move/from16 v3, v21

    invoke-static {v0, v1, v14, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1248
    invoke-virtual/range {p0 .. p0}, Lorg/apache/poi/hdf/model/hdftypes/TableProperties;->getRgdxaCenter()[S

    move-result-object v18

    add-int/lit8 v19, v12, 0x1

    add-int v20, v12, v11

    sub-int v21, v13, v12

    move-object/from16 v0, v18

    move/from16 v1, v19

    move/from16 v2, v20

    move/from16 v3, v21

    invoke-static {v0, v1, v14, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1250
    invoke-virtual/range {p0 .. p0}, Lorg/apache/poi/hdf/model/hdftypes/TableProperties;->getRgtc()[Lorg/apache/poi/hdf/model/hdftypes/definitions/TCAbstractType;

    move-result-object v18

    const/16 v19, 0x0

    const/16 v20, 0x0

    move-object/from16 v0, v18

    move/from16 v1, v19

    move/from16 v2, v20

    invoke-static {v0, v1, v15, v2, v12}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1251
    invoke-virtual/range {p0 .. p0}, Lorg/apache/poi/hdf/model/hdftypes/TableProperties;->getRgtc()[Lorg/apache/poi/hdf/model/hdftypes/definitions/TCAbstractType;

    move-result-object v18

    add-int v19, v12, v11

    sub-int v20, v13, v12

    move-object/from16 v0, v18

    move/from16 v1, v19

    move/from16 v2, v20

    invoke-static {v0, v12, v15, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_5

    .line 1256
    .restart local v17    # "x":I
    :cond_8
    new-instance v18, Lorg/apache/poi/hdf/model/hdftypes/TableCellDescriptor;

    invoke-direct/range {v18 .. v18}, Lorg/apache/poi/hdf/model/hdftypes/TableCellDescriptor;-><init>()V

    aput-object v18, v15, v17

    .line 1257
    add-int/lit8 v18, v17, -0x1

    aget-short v18, v14, v18

    add-int v18, v18, v16

    move/from16 v0, v18

    int-to-short v0, v0

    move/from16 v18, v0

    aput-short v18, v14, v17

    .line 1254
    add-int/lit8 v17, v17, 0x1

    goto :goto_6

    .line 1106
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_0
        :pswitch_7
        :pswitch_8
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_9
        :pswitch_a
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private static getCHPFlag(BZ)Z
    .locals 2
    .param p0, "x"    # B
    .param p1, "oldVal"    # Z

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1469
    sparse-switch p0, :sswitch_data_0

    .line 1480
    :cond_0
    :goto_0
    :sswitch_0
    return v0

    :sswitch_1
    move v0, v1

    .line 1474
    goto :goto_0

    :sswitch_2
    move v0, p1

    .line 1476
    goto :goto_0

    .line 1478
    :sswitch_3
    if-nez p1, :cond_0

    move v0, v1

    goto :goto_0

    .line 1469
    :sswitch_data_0
    .sparse-switch
        -0x80 -> :sswitch_2
        -0x7f -> :sswitch_3
        0x0 -> :sswitch_0
        0x1 -> :sswitch_1
    .end sparse-switch
.end method

.method public static getFlag(I)Z
    .locals 1
    .param p0, "x"    # I

    .prologue
    .line 1494
    if-eqz p0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static uncompressProperty([BLjava/lang/Object;Lorg/apache/poi/hdf/model/hdftypes/StyleSheet;)Ljava/lang/Object;
    .locals 1
    .param p0, "grpprl"    # [B
    .param p1, "parent"    # Ljava/lang/Object;
    .param p2, "styleSheet"    # Lorg/apache/poi/hdf/model/hdftypes/StyleSheet;

    .prologue
    .line 661
    const/4 v0, 0x1

    invoke-static {p0, p1, p2, v0}, Lorg/apache/poi/hdf/model/hdftypes/StyleSheet;->uncompressProperty([BLjava/lang/Object;Lorg/apache/poi/hdf/model/hdftypes/StyleSheet;Z)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public static uncompressProperty([BLjava/lang/Object;Lorg/apache/poi/hdf/model/hdftypes/StyleSheet;Z)Ljava/lang/Object;
    .locals 25
    .param p0, "grpprl"    # [B
    .param p1, "parent"    # Ljava/lang/Object;
    .param p2, "styleSheet"    # Lorg/apache/poi/hdf/model/hdftypes/StyleSheet;
    .param p3, "doIstd"    # Z

    .prologue
    .line 677
    const/16 v18, 0x0

    .line 678
    .local v18, "newProperty":Ljava/lang/Object;
    const/4 v7, 0x0

    .line 679
    .local v7, "offset":I
    const/16 v20, 0x1

    .line 682
    .local v20, "propertyType":I
    move-object/from16 v0, p1

    instance-of v2, v0, Lorg/apache/poi/hdf/model/hdftypes/ParagraphProperties;

    if-eqz v2, :cond_1

    .line 686
    :try_start_0
    move-object/from16 v0, p1

    check-cast v0, Lorg/apache/poi/hdf/model/hdftypes/ParagraphProperties;

    move-object v2, v0

    invoke-virtual {v2}, Lorg/apache/poi/hdf/model/hdftypes/ParagraphProperties;->clone()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v18

    .line 689
    .end local v18    # "newProperty":Ljava/lang/Object;
    :goto_0
    if-eqz p3, :cond_7

    move-object/from16 v2, v18

    .line 691
    check-cast v2, Lorg/apache/poi/hdf/model/hdftypes/ParagraphProperties;

    const/4 v6, 0x0

    move-object/from16 v0, p0

    invoke-static {v0, v6}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v6

    invoke-virtual {v2, v6}, Lorg/apache/poi/hdf/model/hdftypes/ParagraphProperties;->setIstd(I)V

    .line 693
    const/4 v7, 0x2

    move-object/from16 v24, v18

    .line 724
    :cond_0
    :goto_1
    move-object/from16 v0, p0

    array-length v2, v0

    if-lt v7, v2, :cond_5

    .line 818
    :goto_2
    return-object v24

    .line 696
    .restart local v18    # "newProperty":Ljava/lang/Object;
    :cond_1
    move-object/from16 v0, p1

    instance-of v2, v0, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;

    if-eqz v2, :cond_2

    .line 700
    :try_start_1
    move-object/from16 v0, p1

    check-cast v0, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;

    move-object v2, v0

    invoke-virtual {v2}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->clone()Ljava/lang/Object;

    move-result-object v18

    .line 701
    move-object/from16 v0, v18

    check-cast v0, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;

    move-object v2, v0

    move-object/from16 v0, p1

    check-cast v0, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;

    move-object v6, v0

    invoke-virtual {v6}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->getIstd()I

    move-result v6

    invoke-virtual {v2, v6}, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;->setBaseIstd(I)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 706
    .end local v18    # "newProperty":Ljava/lang/Object;
    :goto_3
    const/16 v20, 0x2

    move-object/from16 v24, v18

    .line 707
    goto :goto_1

    .line 703
    :catch_0
    move-exception v17

    .line 704
    .local v17, "e":Ljava/lang/Exception;
    const-string/jumbo v2, "DocumentService"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string/jumbo v9, "Exception: "

    invoke-direct {v6, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v2, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 708
    .end local v17    # "e":Ljava/lang/Exception;
    .restart local v18    # "newProperty":Ljava/lang/Object;
    :cond_2
    move-object/from16 v0, p1

    instance-of v2, v0, Lorg/apache/poi/hdf/model/hdftypes/SectionProperties;

    if-eqz v2, :cond_3

    .line 710
    move-object/from16 v18, p1

    .line 711
    const/16 v20, 0x4

    move-object/from16 v24, v18

    .line 712
    goto :goto_1

    .line 713
    :cond_3
    move-object/from16 v0, p1

    instance-of v2, v0, Lorg/apache/poi/hdf/model/hdftypes/TableProperties;

    if-eqz v2, :cond_4

    .line 715
    move-object/from16 v18, p1

    .line 716
    const/16 v20, 0x5

    .line 717
    const/4 v7, 0x2

    move-object/from16 v24, v18

    .line 718
    goto :goto_1

    .line 721
    :cond_4
    const/16 v24, 0x0

    goto :goto_2

    .line 726
    .end local v18    # "newProperty":Ljava/lang/Object;
    :cond_5
    move-object/from16 v0, p0

    invoke-static {v0, v7}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v21

    .line 727
    .local v21, "sprm":S
    add-int/lit8 v7, v7, 0x2

    .line 729
    const v2, 0xe000

    and-int v2, v2, v21

    shr-int/lit8 v2, v2, 0xd

    int-to-byte v8, v2

    .line 730
    .local v8, "spra":B
    const/16 v19, 0x0

    .line 731
    .local v19, "opSize":I
    const/4 v4, 0x0

    .line 732
    .local v4, "param":I
    const/4 v5, 0x0

    .line 734
    .local v5, "varParam":[B
    packed-switch v8, :pswitch_data_0

    .line 781
    new-instance v2, Ljava/lang/RuntimeException;

    const-string/jumbo v6, "unrecognized pap opcode"

    invoke-direct {v2, v6}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 738
    :pswitch_0
    const/16 v19, 0x1

    .line 739
    aget-byte v4, p0, v7

    .line 784
    :goto_4
    add-int v7, v7, v19

    .line 785
    move/from16 v0, v21

    and-int/lit16 v2, v0, 0x1ff

    int-to-short v3, v2

    .line 786
    .local v3, "operand":S
    move/from16 v0, v21

    and-int/lit16 v2, v0, 0x1c00

    shr-int/lit8 v2, v2, 0xa

    int-to-byte v0, v2

    move/from16 v23, v0

    .line 787
    .local v23, "type":B
    packed-switch v20, :pswitch_data_1

    :pswitch_1
    goto/16 :goto_1

    .line 790
    :pswitch_2
    const/4 v2, 0x1

    move/from16 v0, v23

    if-ne v0, v2, :cond_0

    move-object/from16 v2, v24

    .line 792
    check-cast v2, Lorg/apache/poi/hdf/model/hdftypes/ParagraphProperties;

    move-object/from16 v6, p0

    invoke-static/range {v2 .. v8}, Lorg/apache/poi/hdf/model/hdftypes/StyleSheet;->doPAPOperation(Lorg/apache/poi/hdf/model/hdftypes/ParagraphProperties;II[B[BII)V

    goto/16 :goto_1

    .line 742
    .end local v3    # "operand":S
    .end local v23    # "type":B
    :pswitch_3
    const/16 v19, 0x2

    .line 743
    move-object/from16 v0, p0

    invoke-static {v0, v7}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v4

    .line 744
    goto :goto_4

    .line 746
    :pswitch_4
    const/16 v19, 0x4

    .line 747
    move-object/from16 v0, p0

    invoke-static {v0, v7}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v4

    .line 748
    goto :goto_4

    .line 751
    :pswitch_5
    const/16 v19, 0x2

    .line 752
    move-object/from16 v0, p0

    invoke-static {v0, v7}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v4

    .line 753
    goto :goto_4

    .line 757
    :pswitch_6
    const/16 v2, -0x29f8

    move/from16 v0, v21

    if-eq v0, v2, :cond_6

    .line 759
    move-object/from16 v0, p0

    invoke-static {v0, v7}, Lorg/apache/poi/util/LittleEndian;->getUnsignedByte([BI)I

    move-result v19

    .line 760
    add-int/lit8 v7, v7, 0x1

    .line 767
    :goto_5
    move/from16 v0, v19

    new-array v5, v0, [B

    .line 768
    const/4 v2, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-static {v0, v7, v5, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_4

    .line 764
    :cond_6
    move-object/from16 v0, p0

    invoke-static {v0, v7}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v2

    add-int/lit8 v19, v2, -0x1

    .line 765
    add-int/lit8 v7, v7, 0x2

    goto :goto_5

    .line 772
    :pswitch_7
    const/16 v19, 0x3

    .line 773
    const/4 v2, 0x4

    new-array v0, v2, [B

    move-object/from16 v22, v0

    .line 774
    .local v22, "threeByteInt":[B
    const/4 v2, 0x0

    aget-byte v6, p0, v7

    aput-byte v6, v22, v2

    .line 775
    const/4 v2, 0x1

    add-int/lit8 v6, v7, 0x1

    aget-byte v6, p0, v6

    aput-byte v6, v22, v2

    .line 776
    const/4 v2, 0x2

    add-int/lit8 v6, v7, 0x2

    aget-byte v6, p0, v6

    aput-byte v6, v22, v2

    .line 777
    const/4 v2, 0x3

    const/4 v6, 0x0

    aput-byte v6, v22, v2

    .line 778
    const/4 v2, 0x0

    move-object/from16 v0, v22

    invoke-static {v0, v2}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v4

    .line 779
    goto/16 :goto_4

    .end local v22    # "threeByteInt":[B
    .restart local v3    # "operand":S
    .restart local v23    # "type":B
    :pswitch_8
    move-object/from16 v9, p1

    .line 799
    check-cast v9, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;

    move-object/from16 v10, v24

    .line 800
    check-cast v10, Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;

    move v11, v3

    move v12, v4

    move-object v13, v5

    move-object/from16 v14, p0

    move v15, v7

    move-object/from16 v16, p2

    .line 799
    invoke-static/range {v9 .. v16}, Lorg/apache/poi/hdf/model/hdftypes/StyleSheet;->doCHPOperation(Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;Lorg/apache/poi/hdf/model/hdftypes/CharacterProperties;II[B[BILorg/apache/poi/hdf/model/hdftypes/StyleSheet;)V

    goto/16 :goto_1

    :pswitch_9
    move-object/from16 v2, v24

    .line 806
    check-cast v2, Lorg/apache/poi/hdf/model/hdftypes/SectionProperties;

    invoke-static {v2, v3, v4, v5}, Lorg/apache/poi/hdf/model/hdftypes/StyleSheet;->doSEPOperation(Lorg/apache/poi/hdf/model/hdftypes/SectionProperties;II[B)V

    goto/16 :goto_1

    .line 809
    :pswitch_a
    const/4 v2, 0x5

    move/from16 v0, v23

    if-ne v0, v2, :cond_0

    move-object/from16 v2, v24

    .line 811
    check-cast v2, Lorg/apache/poi/hdf/model/hdftypes/TableProperties;

    invoke-static {v2, v3, v4, v5}, Lorg/apache/poi/hdf/model/hdftypes/StyleSheet;->doTAPOperation(Lorg/apache/poi/hdf/model/hdftypes/TableProperties;II[B)V

    goto/16 :goto_1

    .line 688
    .end local v3    # "operand":S
    .end local v4    # "param":I
    .end local v5    # "varParam":[B
    .end local v8    # "spra":B
    .end local v19    # "opSize":I
    .end local v21    # "sprm":S
    .end local v23    # "type":B
    .restart local v18    # "newProperty":Ljava/lang/Object;
    :catch_1
    move-exception v2

    goto/16 :goto_0

    .end local v18    # "newProperty":Ljava/lang/Object;
    :cond_7
    move-object/from16 v24, v18

    goto/16 :goto_1

    .line 734
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch

    .line 787
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_8
        :pswitch_1
        :pswitch_9
        :pswitch_a
    .end packed-switch
.end method


# virtual methods
.method public getStyleDescription(I)Lorg/apache/poi/hdf/model/hdftypes/StyleDescription;
    .locals 1
    .param p1, "x"    # I

    .prologue
    .line 174
    iget-object v0, p0, Lorg/apache/poi/hdf/model/hdftypes/StyleSheet;->_styleDescriptions:[Lorg/apache/poi/hdf/model/hdftypes/StyleDescription;

    aget-object v0, v0, p1

    return-object v0
.end method
