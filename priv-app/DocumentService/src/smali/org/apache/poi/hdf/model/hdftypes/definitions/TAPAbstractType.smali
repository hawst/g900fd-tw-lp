.class public abstract Lorg/apache/poi/hdf/model/hdftypes/definitions/TAPAbstractType;
.super Ljava/lang/Object;
.source "TAPAbstractType.java"

# interfaces
.implements Lorg/apache/poi/hdf/model/hdftypes/HDFType;


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private field_10_rgshd:[B

.field private field_11_brcBottom:[S

.field private field_12_brcTop:[S

.field private field_13_brcLeft:[S

.field private field_14_brcRight:[S

.field private field_15_brcVertical:[S

.field private field_16_brcHorizontal:[S

.field private field_1_jc:I

.field private field_2_dxaGapHalf:I

.field private field_3_dyaRowHeight:I

.field private field_4_fCantSplit:Z

.field private field_5_fTableHeader:Z

.field private field_6_tlp:I

.field private field_7_itcMac:S

.field private field_8_rgdxaCenter:[S

.field private field_9_rgtc:[Lorg/apache/poi/hdf/model/hdftypes/definitions/TCAbstractType;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    return-void
.end method


# virtual methods
.method public getBrcBottom()[S
    .locals 1

    .prologue
    .line 232
    iget-object v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/TAPAbstractType;->field_11_brcBottom:[S

    return-object v0
.end method

.method public getBrcHorizontal()[S
    .locals 1

    .prologue
    .line 312
    iget-object v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/TAPAbstractType;->field_16_brcHorizontal:[S

    return-object v0
.end method

.method public getBrcLeft()[S
    .locals 1

    .prologue
    .line 264
    iget-object v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/TAPAbstractType;->field_13_brcLeft:[S

    return-object v0
.end method

.method public getBrcRight()[S
    .locals 1

    .prologue
    .line 280
    iget-object v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/TAPAbstractType;->field_14_brcRight:[S

    return-object v0
.end method

.method public getBrcTop()[S
    .locals 1

    .prologue
    .line 248
    iget-object v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/TAPAbstractType;->field_12_brcTop:[S

    return-object v0
.end method

.method public getBrcVertical()[S
    .locals 1

    .prologue
    .line 296
    iget-object v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/TAPAbstractType;->field_15_brcVertical:[S

    return-object v0
.end method

.method public getDxaGapHalf()I
    .locals 1

    .prologue
    .line 88
    iget v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/TAPAbstractType;->field_2_dxaGapHalf:I

    return v0
.end method

.method public getDyaRowHeight()I
    .locals 1

    .prologue
    .line 104
    iget v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/TAPAbstractType;->field_3_dyaRowHeight:I

    return v0
.end method

.method public getFCantSplit()Z
    .locals 1

    .prologue
    .line 120
    iget-boolean v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/TAPAbstractType;->field_4_fCantSplit:Z

    return v0
.end method

.method public getFTableHeader()Z
    .locals 1

    .prologue
    .line 136
    iget-boolean v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/TAPAbstractType;->field_5_fTableHeader:Z

    return v0
.end method

.method public getItcMac()S
    .locals 1

    .prologue
    .line 168
    iget-short v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/TAPAbstractType;->field_7_itcMac:S

    return v0
.end method

.method public getJc()I
    .locals 1

    .prologue
    .line 72
    iget v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/TAPAbstractType;->field_1_jc:I

    return v0
.end method

.method public getRgdxaCenter()[S
    .locals 1

    .prologue
    .line 184
    iget-object v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/TAPAbstractType;->field_8_rgdxaCenter:[S

    return-object v0
.end method

.method public getRgshd()[B
    .locals 1

    .prologue
    .line 216
    iget-object v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/TAPAbstractType;->field_10_rgshd:[B

    return-object v0
.end method

.method public getRgtc()[Lorg/apache/poi/hdf/model/hdftypes/definitions/TCAbstractType;
    .locals 1

    .prologue
    .line 200
    iget-object v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/TAPAbstractType;->field_9_rgtc:[Lorg/apache/poi/hdf/model/hdftypes/definitions/TCAbstractType;

    return-object v0
.end method

.method public getSize()I
    .locals 1

    .prologue
    .line 62
    const/16 v0, 0xae

    return v0
.end method

.method public getTlp()I
    .locals 1

    .prologue
    .line 152
    iget v0, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/TAPAbstractType;->field_6_tlp:I

    return v0
.end method

.method public setBrcBottom([S)V
    .locals 0
    .param p1, "field_11_brcBottom"    # [S

    .prologue
    .line 240
    iput-object p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/TAPAbstractType;->field_11_brcBottom:[S

    .line 241
    return-void
.end method

.method public setBrcHorizontal([S)V
    .locals 0
    .param p1, "field_16_brcHorizontal"    # [S

    .prologue
    .line 320
    iput-object p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/TAPAbstractType;->field_16_brcHorizontal:[S

    .line 321
    return-void
.end method

.method public setBrcLeft([S)V
    .locals 0
    .param p1, "field_13_brcLeft"    # [S

    .prologue
    .line 272
    iput-object p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/TAPAbstractType;->field_13_brcLeft:[S

    .line 273
    return-void
.end method

.method public setBrcRight([S)V
    .locals 0
    .param p1, "field_14_brcRight"    # [S

    .prologue
    .line 288
    iput-object p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/TAPAbstractType;->field_14_brcRight:[S

    .line 289
    return-void
.end method

.method public setBrcTop([S)V
    .locals 0
    .param p1, "field_12_brcTop"    # [S

    .prologue
    .line 256
    iput-object p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/TAPAbstractType;->field_12_brcTop:[S

    .line 257
    return-void
.end method

.method public setBrcVertical([S)V
    .locals 0
    .param p1, "field_15_brcVertical"    # [S

    .prologue
    .line 304
    iput-object p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/TAPAbstractType;->field_15_brcVertical:[S

    .line 305
    return-void
.end method

.method public setDxaGapHalf(I)V
    .locals 0
    .param p1, "field_2_dxaGapHalf"    # I

    .prologue
    .line 96
    iput p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/TAPAbstractType;->field_2_dxaGapHalf:I

    .line 97
    return-void
.end method

.method public setDyaRowHeight(I)V
    .locals 0
    .param p1, "field_3_dyaRowHeight"    # I

    .prologue
    .line 112
    iput p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/TAPAbstractType;->field_3_dyaRowHeight:I

    .line 113
    return-void
.end method

.method public setFCantSplit(Z)V
    .locals 0
    .param p1, "field_4_fCantSplit"    # Z

    .prologue
    .line 128
    iput-boolean p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/TAPAbstractType;->field_4_fCantSplit:Z

    .line 129
    return-void
.end method

.method public setFTableHeader(Z)V
    .locals 0
    .param p1, "field_5_fTableHeader"    # Z

    .prologue
    .line 144
    iput-boolean p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/TAPAbstractType;->field_5_fTableHeader:Z

    .line 145
    return-void
.end method

.method public setItcMac(S)V
    .locals 0
    .param p1, "field_7_itcMac"    # S

    .prologue
    .line 176
    iput-short p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/TAPAbstractType;->field_7_itcMac:S

    .line 177
    return-void
.end method

.method public setJc(I)V
    .locals 0
    .param p1, "field_1_jc"    # I

    .prologue
    .line 80
    iput p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/TAPAbstractType;->field_1_jc:I

    .line 81
    return-void
.end method

.method public setRgdxaCenter([S)V
    .locals 0
    .param p1, "field_8_rgdxaCenter"    # [S

    .prologue
    .line 192
    iput-object p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/TAPAbstractType;->field_8_rgdxaCenter:[S

    .line 193
    return-void
.end method

.method public setRgshd([B)V
    .locals 0
    .param p1, "field_10_rgshd"    # [B

    .prologue
    .line 224
    iput-object p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/TAPAbstractType;->field_10_rgshd:[B

    .line 225
    return-void
.end method

.method public setRgtc([Lorg/apache/poi/hdf/model/hdftypes/definitions/TCAbstractType;)V
    .locals 0
    .param p1, "field_9_rgtc"    # [Lorg/apache/poi/hdf/model/hdftypes/definitions/TCAbstractType;

    .prologue
    .line 208
    iput-object p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/TAPAbstractType;->field_9_rgtc:[Lorg/apache/poi/hdf/model/hdftypes/definitions/TCAbstractType;

    .line 209
    return-void
.end method

.method public setTlp(I)V
    .locals 0
    .param p1, "field_6_tlp"    # I

    .prologue
    .line 160
    iput p1, p0, Lorg/apache/poi/hdf/model/hdftypes/definitions/TAPAbstractType;->field_6_tlp:I

    .line 161
    return-void
.end method
