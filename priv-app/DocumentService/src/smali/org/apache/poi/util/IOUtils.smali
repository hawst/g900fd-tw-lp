.class public final Lorg/apache/poi/util/IOUtils;
.super Ljava/lang/Object;
.source "IOUtils.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "Utils"

.field private static final logger:Lorg/apache/poi/util/POILogger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 41
    const-class v0, Lorg/apache/poi/util/IOUtils;

    invoke-static {v0}, Lorg/apache/poi/util/POILogFactory;->getLogger(Ljava/lang/Class;)Lorg/apache/poi/util/POILogger;

    move-result-object v0

    .line 40
    sput-object v0, Lorg/apache/poi/util/IOUtils;->logger:Lorg/apache/poi/util/POILogger;

    .line 41
    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    return-void
.end method

.method public static calculateChecksum([B)J
    .locals 4
    .param p0, "data"    # [B

    .prologue
    .line 161
    new-instance v0, Ljava/util/zip/CRC32;

    invoke-direct {v0}, Ljava/util/zip/CRC32;-><init>()V

    .line 162
    .local v0, "sum":Ljava/util/zip/Checksum;
    const/4 v1, 0x0

    array-length v2, p0

    invoke-interface {v0, p0, v1, v2}, Ljava/util/zip/Checksum;->update([BII)V

    .line 163
    invoke-interface {v0}, Ljava/util/zip/Checksum;->getValue()J

    move-result-wide v2

    return-wide v2
.end method

.method public static closeQuietly(Ljava/io/Closeable;)V
    .locals 5
    .param p0, "closeable"    # Ljava/io/Closeable;

    .prologue
    .line 177
    :try_start_0
    invoke-interface {p0}, Ljava/io/Closeable;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 184
    :goto_0
    return-void

    .line 179
    :catch_0
    move-exception v0

    .line 181
    .local v0, "exc":Ljava/lang/Exception;
    sget-object v1, Lorg/apache/poi/util/IOUtils;->logger:Lorg/apache/poi/util/POILogger;

    const/4 v2, 0x7

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "Unable to close resource: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3, v0}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public static copy(Ljava/io/InputStream;Ljava/io/OutputStream;)V
    .locals 3
    .param p0, "inp"    # Ljava/io/InputStream;
    .param p1, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 142
    if-eqz p0, :cond_0

    if-nez p1, :cond_2

    .line 143
    :cond_0
    if-eqz p0, :cond_1

    .line 144
    invoke-virtual {p0}, Ljava/io/InputStream;->close()V

    .line 158
    :cond_1
    :goto_0
    return-void

    .line 148
    :cond_2
    const/16 v2, 0x1000

    new-array v0, v2, [B

    .line 150
    .local v0, "buff":[B
    :cond_3
    :goto_1
    invoke-virtual {p0, v0}, Ljava/io/InputStream;->read([B)I

    move-result v1

    .local v1, "count":I
    const/4 v2, -0x1

    if-ne v1, v2, :cond_4

    .line 156
    if-eqz p0, :cond_1

    .line 157
    invoke-virtual {p0}, Ljava/io/InputStream;->close()V

    goto :goto_0

    .line 151
    :cond_4
    if-lez v1, :cond_3

    .line 152
    const/4 v2, 0x0

    invoke-virtual {p1, v0, v2, v1}, Ljava/io/OutputStream;->write([BII)V

    goto :goto_1
.end method

.method public static readFully(Ljava/io/InputStream;[B)I
    .locals 2
    .param p0, "in"    # Ljava/io/InputStream;
    .param p1, "b"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 87
    const/4 v0, 0x0

    array-length v1, p1

    invoke-static {p0, p1, v0, v1}, Lorg/apache/poi/util/IOUtils;->readFully(Ljava/io/InputStream;[BII)I

    move-result v0

    return v0
.end method

.method public static readFully(Ljava/io/InputStream;[BII)I
    .locals 4
    .param p0, "in"    # Ljava/io/InputStream;
    .param p1, "b"    # [B
    .param p2, "off"    # I
    .param p3, "len"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 100
    const/4 v1, 0x0

    .line 102
    .local v1, "total":I
    :cond_0
    add-int v2, p2, v1

    sub-int v3, p3, v1

    invoke-virtual {p0, p1, v2, v3}, Ljava/io/InputStream;->read([BII)I

    move-result v0

    .line 103
    .local v0, "got":I
    if-gez v0, :cond_2

    .line 104
    if-nez v1, :cond_1

    const/4 v2, -0x1

    .line 108
    :goto_0
    return v2

    :cond_1
    move v2, v1

    .line 104
    goto :goto_0

    .line 106
    :cond_2
    add-int/2addr v1, v0

    .line 107
    if-ne v1, p3, :cond_0

    move v2, v1

    .line 108
    goto :goto_0
.end method

.method public static readFully(Ljava/nio/channels/ReadableByteChannel;Ljava/nio/ByteBuffer;)I
    .locals 4
    .param p0, "channel"    # Ljava/nio/channels/ReadableByteChannel;
    .param p1, "b"    # Ljava/nio/ByteBuffer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 123
    const/4 v1, 0x0

    .line 125
    .local v1, "total":I
    :cond_0
    invoke-interface {p0, p1}, Ljava/nio/channels/ReadableByteChannel;->read(Ljava/nio/ByteBuffer;)I

    move-result v0

    .line 126
    .local v0, "got":I
    if-gez v0, :cond_2

    .line 127
    if-nez v1, :cond_1

    const/4 v2, -0x1

    .line 131
    :goto_0
    return v2

    :cond_1
    move v2, v1

    .line 127
    goto :goto_0

    .line 129
    :cond_2
    add-int/2addr v1, v0

    .line 130
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v2

    if-eq v1, v2, :cond_3

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->position()I

    move-result v2

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v3

    if-ne v2, v3, :cond_0

    :cond_3
    move v2, v1

    .line 131
    goto :goto_0
.end method

.method public static toByteArray(Ljava/io/InputStream;)[B
    .locals 4
    .param p0, "stream"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 51
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 53
    .local v0, "baos":Ljava/io/ByteArrayOutputStream;
    const/16 v3, 0x1000

    new-array v1, v3, [B

    .line 54
    .local v1, "buffer":[B
    const/4 v2, 0x0

    .line 55
    .local v2, "read":I
    :cond_0
    :goto_0
    const/4 v3, -0x1

    if-ne v2, v3, :cond_2

    .line 62
    if-eqz p0, :cond_1

    .line 63
    invoke-virtual {p0}, Ljava/io/InputStream;->close()V

    .line 64
    :cond_1
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v3

    return-object v3

    .line 56
    :cond_2
    invoke-virtual {p0, v1}, Ljava/io/InputStream;->read([B)I

    move-result v2

    .line 57
    if-lez v2, :cond_0

    .line 58
    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3, v2}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    goto :goto_0
.end method

.method public static toByteArray(Ljava/nio/ByteBuffer;I)[B
    .locals 2
    .param p0, "buffer"    # Ljava/nio/ByteBuffer;
    .param p1, "length"    # I

    .prologue
    .line 73
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->hasArray()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->arrayOffset()I

    move-result v1

    if-nez v1, :cond_0

    .line 75
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v0

    .line 80
    :goto_0
    return-object v0

    .line 78
    :cond_0
    new-array v0, p1, [B

    .line 79
    .local v0, "data":[B
    invoke-virtual {p0, v0}, Ljava/nio/ByteBuffer;->get([B)Ljava/nio/ByteBuffer;

    goto :goto_0
.end method

.method public static writeToFile(Ljava/io/InputStream;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 9
    .param p0, "stream"    # Ljava/io/InputStream;
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "fileName"    # Ljava/lang/String;
    .param p3, "folderName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 191
    const v6, 0x8001

    invoke-virtual {p1, p3, v6}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v1

    .line 192
    .local v1, "dir":Ljava/io/File;
    new-instance v3, Ljava/io/FileOutputStream;

    new-instance v6, Ljava/io/File;

    invoke-direct {v6, v1, p2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-direct {v3, v6}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 194
    .local v3, "fos":Ljava/io/FileOutputStream;
    new-instance v4, Ljava/io/BufferedOutputStream;

    invoke-direct {v4, v3}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 196
    .local v4, "out":Ljava/io/OutputStream;
    const/16 v6, 0x1000

    new-array v0, v6, [B

    .line 197
    .local v0, "buffer":[B
    const/4 v5, 0x0

    .line 200
    .local v5, "read":I
    :cond_0
    :goto_0
    const/4 v6, -0x1

    if-ne v5, v6, :cond_4

    .line 210
    if-eqz p0, :cond_1

    .line 211
    invoke-virtual {p0}, Ljava/io/InputStream;->close()V

    .line 214
    :cond_1
    if-eqz v4, :cond_2

    .line 215
    invoke-virtual {v4}, Ljava/io/OutputStream;->close()V

    .line 217
    :cond_2
    if-eqz v3, :cond_3

    .line 218
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V

    .line 221
    :cond_3
    :goto_1
    return-void

    .line 201
    :cond_4
    :try_start_0
    invoke-virtual {p0, v0}, Ljava/io/InputStream;->read([B)I

    move-result v5

    .line 202
    if-lez v5, :cond_0

    .line 203
    const/4 v6, 0x0

    invoke-virtual {v4, v0, v6, v5}, Ljava/io/OutputStream;->write([BII)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 206
    :catch_0
    move-exception v2

    .line 207
    .local v2, "e":Ljava/lang/Exception;
    :try_start_1
    const-string/jumbo v6, "Utils"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string/jumbo v8, "ERROR: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 210
    if-eqz p0, :cond_5

    .line 211
    invoke-virtual {p0}, Ljava/io/InputStream;->close()V

    .line 214
    :cond_5
    if-eqz v4, :cond_6

    .line 215
    invoke-virtual {v4}, Ljava/io/OutputStream;->close()V

    .line 217
    :cond_6
    if-eqz v3, :cond_3

    .line 218
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V

    goto :goto_1

    .line 208
    .end local v2    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v6

    .line 210
    if-eqz p0, :cond_7

    .line 211
    invoke-virtual {p0}, Ljava/io/InputStream;->close()V

    .line 214
    :cond_7
    if-eqz v4, :cond_8

    .line 215
    invoke-virtual {v4}, Ljava/io/OutputStream;->close()V

    .line 217
    :cond_8
    if-eqz v3, :cond_9

    .line 218
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V

    .line 220
    :cond_9
    throw v6
.end method
