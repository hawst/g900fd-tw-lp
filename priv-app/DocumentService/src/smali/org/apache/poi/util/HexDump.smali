.class public Lorg/apache/poi/util/HexDump;
.super Ljava/lang/Object;
.source "HexDump.java"


# static fields
.field public static final EOL:Ljava/lang/String;

.field private static final _hexcodes:[C

.field private static final _shifts:[I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/16 v6, 0x10

    const/16 v5, 0xc

    const/16 v4, 0x8

    const/4 v3, 0x4

    .line 35
    const-string/jumbo v0, "line.separator"

    invoke-static {v0}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/util/HexDump;->EOL:Ljava/lang/String;

    .line 36
    const-string/jumbo v0, "0123456789ABCDEF"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    sput-object v0, Lorg/apache/poi/util/HexDump;->_hexcodes:[C

    .line 38
    new-array v0, v6, [I

    const/4 v1, 0x0

    .line 39
    const/16 v2, 0x3c

    aput v2, v0, v1

    const/4 v1, 0x1

    const/16 v2, 0x38

    aput v2, v0, v1

    const/4 v1, 0x2

    const/16 v2, 0x34

    aput v2, v0, v1

    const/4 v1, 0x3

    const/16 v2, 0x30

    aput v2, v0, v1

    const/16 v1, 0x2c

    aput v1, v0, v3

    const/4 v1, 0x5

    const/16 v2, 0x28

    aput v2, v0, v1

    const/4 v1, 0x6

    const/16 v2, 0x24

    aput v2, v0, v1

    const/4 v1, 0x7

    const/16 v2, 0x20

    aput v2, v0, v1

    const/16 v1, 0x1c

    aput v1, v0, v4

    const/16 v1, 0x9

    const/16 v2, 0x18

    aput v2, v0, v1

    const/16 v1, 0xa

    const/16 v2, 0x14

    aput v2, v0, v1

    const/16 v1, 0xb

    aput v6, v0, v1

    aput v5, v0, v5

    const/16 v1, 0xd

    aput v4, v0, v1

    const/16 v1, 0xe

    aput v3, v0, v1

    .line 37
    sput-object v0, Lorg/apache/poi/util/HexDump;->_shifts:[I

    .line 40
    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    return-void
.end method

.method public static byteToHex(I)[C
    .locals 3
    .param p0, "value"    # I

    .prologue
    .line 451
    int-to-long v0, p0

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lorg/apache/poi/util/HexDump;->toHexChars(JI)[C

    move-result-object v0

    return-object v0
.end method

.method private static dump(B)Ljava/lang/String;
    .locals 5
    .param p0, "value"    # B

    .prologue
    .line 234
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 235
    .local v0, "buf":Ljava/lang/StringBuffer;
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->setLength(I)V

    .line 236
    const/4 v1, 0x0

    .local v1, "j":I
    :goto_0
    const/4 v2, 0x2

    if-lt v1, v2, :cond_0

    .line 240
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2

    .line 238
    :cond_0
    sget-object v2, Lorg/apache/poi/util/HexDump;->_hexcodes:[C

    sget-object v3, Lorg/apache/poi/util/HexDump;->_shifts:[I

    add-int/lit8 v4, v1, 0x6

    aget v3, v3, v4

    shr-int v3, p0, v3

    and-int/lit8 v3, v3, 0xf

    aget-char v2, v2, v3

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 236
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private static dump(J)Ljava/lang/String;
    .locals 6
    .param p0, "value"    # J

    .prologue
    .line 223
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 224
    .local v0, "buf":Ljava/lang/StringBuffer;
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->setLength(I)V

    .line 225
    const/4 v1, 0x0

    .local v1, "j":I
    :goto_0
    const/16 v2, 0x8

    if-lt v1, v2, :cond_0

    .line 229
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2

    .line 227
    :cond_0
    sget-object v2, Lorg/apache/poi/util/HexDump;->_hexcodes:[C

    sget-object v3, Lorg/apache/poi/util/HexDump;->_shifts:[I

    sget-object v4, Lorg/apache/poi/util/HexDump;->_shifts:[I

    array-length v4, v4

    add-int/2addr v4, v1

    add-int/lit8 v4, v4, -0x8

    aget v3, v3, v4

    shr-long v4, p0, v3

    long-to-int v3, v4

    and-int/lit8 v3, v3, 0xf

    aget-char v2, v2, v3

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 225
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public static dump([BJI)Ljava/lang/String;
    .locals 11
    .param p0, "data"    # [B
    .param p1, "offset"    # J
    .param p3, "index"    # I

    .prologue
    const/16 v9, 0x10

    const/16 v8, 0x20

    .line 173
    if-ltz p3, :cond_0

    array-length v6, p0

    if-lt p3, v6, :cond_1

    .line 175
    :cond_0
    new-instance v6, Ljava/lang/ArrayIndexOutOfBoundsException;

    .line 176
    new-instance v7, Ljava/lang/StringBuilder;

    const-string/jumbo v8, "illegal index: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, " into array of length "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 177
    array-length v8, p0

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 176
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 175
    invoke-direct {v6, v7}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 179
    :cond_1
    int-to-long v6, p3

    add-long v2, p1, v6

    .line 180
    .local v2, "display_offset":J
    new-instance v0, Ljava/lang/StringBuffer;

    const/16 v6, 0x4a

    invoke-direct {v0, v6}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 182
    .local v0, "buffer":Ljava/lang/StringBuffer;
    move v4, p3

    .local v4, "j":I
    :goto_0
    array-length v6, p0

    if-lt v4, v6, :cond_2

    .line 217
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    return-object v6

    .line 184
    :cond_2
    array-length v6, p0

    sub-int v1, v6, v4

    .line 186
    .local v1, "chars_read":I
    if-le v1, v9, :cond_3

    .line 188
    const/16 v1, 0x10

    .line 190
    :cond_3
    invoke-static {v2, v3}, Lorg/apache/poi/util/HexDump;->dump(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v6, v8}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 191
    const/4 v5, 0x0

    .local v5, "k":I
    :goto_1
    if-lt v5, v9, :cond_4

    .line 203
    const/4 v5, 0x0

    :goto_2
    if-lt v5, v1, :cond_6

    .line 214
    sget-object v6, Lorg/apache/poi/util/HexDump;->EOL:Ljava/lang/String;

    invoke-virtual {v0, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 215
    int-to-long v6, v1

    add-long/2addr v2, v6

    .line 182
    add-int/lit8 v4, v4, 0x10

    goto :goto_0

    .line 193
    :cond_4
    if-ge v5, v1, :cond_5

    .line 195
    add-int v6, v5, v4

    aget-byte v6, p0, v6

    invoke-static {v6}, Lorg/apache/poi/util/HexDump;->dump(B)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 201
    :goto_3
    invoke-virtual {v0, v8}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 191
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 199
    :cond_5
    const-string/jumbo v6, "  "

    invoke-virtual {v0, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_3

    .line 205
    :cond_6
    add-int v6, v5, v4

    aget-byte v6, p0, v6

    if-lt v6, v8, :cond_7

    add-int v6, v5, v4

    aget-byte v6, p0, v6

    const/16 v7, 0x7f

    if-ge v6, v7, :cond_7

    .line 207
    add-int v6, v5, v4

    aget-byte v6, p0, v6

    int-to-char v6, v6

    invoke-virtual {v0, v6}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 203
    :goto_4
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 211
    :cond_7
    const/16 v6, 0x2e

    invoke-virtual {v0, v6}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_4
.end method

.method public static dump(Ljava/io/InputStream;Ljava/io/PrintStream;II)V
    .locals 10
    .param p0, "in"    # Ljava/io/InputStream;
    .param p1, "out"    # Ljava/io/PrintStream;
    .param p2, "start"    # I
    .param p3, "bytesToDump"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, -0x1

    .line 383
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 384
    .local v0, "buf":Ljava/io/ByteArrayOutputStream;
    if-ne p3, v2, :cond_2

    .line 386
    invoke-virtual {p0}, Ljava/io/InputStream;->read()I

    move-result v9

    .line 387
    .local v9, "c":I
    :goto_0
    if-ne v9, v2, :cond_1

    .line 406
    .end local v9    # "c":I
    :cond_0
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    .line 407
    .local v1, "data":[B
    const-wide/16 v2, 0x0

    array-length v6, v1

    move-object v4, p1

    move v5, p2

    invoke-static/range {v1 .. v6}, Lorg/apache/poi/util/HexDump;->dump([BJLjava/io/OutputStream;II)V

    .line 408
    return-void

    .line 389
    .end local v1    # "data":[B
    .restart local v9    # "c":I
    :cond_1
    invoke-virtual {v0, v9}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 390
    invoke-virtual {p0}, Ljava/io/InputStream;->read()I

    move-result v9

    goto :goto_0

    .line 395
    .end local v9    # "c":I
    :cond_2
    move v7, p3

    .local v7, "bytesRemaining":I
    move v8, v7

    .line 396
    .end local v7    # "bytesRemaining":I
    .local v8, "bytesRemaining":I
    :goto_1
    add-int/lit8 v7, v8, -0x1

    .end local v8    # "bytesRemaining":I
    .restart local v7    # "bytesRemaining":I
    if-lez v8, :cond_0

    .line 398
    invoke-virtual {p0}, Ljava/io/InputStream;->read()I

    move-result v9

    .line 399
    .restart local v9    # "c":I
    if-eq v9, v2, :cond_0

    .line 402
    invoke-virtual {v0, v9}, Ljava/io/ByteArrayOutputStream;->write(I)V

    move v8, v7

    .end local v7    # "bytesRemaining":I
    .restart local v8    # "bytesRemaining":I
    goto :goto_1
.end method

.method public static declared-synchronized dump([BJLjava/io/OutputStream;I)V
    .locals 9
    .param p0, "data"    # [B
    .param p1, "offset"    # J
    .param p3, "stream"    # Ljava/io/OutputStream;
    .param p4, "index"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ArrayIndexOutOfBoundsException;,
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 155
    const-class v7, Lorg/apache/poi/util/HexDump;

    monitor-enter v7

    :try_start_0
    array-length v0, p0

    sub-int v6, v0, p4

    move-object v1, p0

    move-wide v2, p1

    move-object v4, p3

    move v5, p4

    invoke-static/range {v1 .. v6}, Lorg/apache/poi/util/HexDump;->dump([BJLjava/io/OutputStream;II)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 156
    monitor-exit v7

    return-void

    .line 155
    :catchall_0
    move-exception v0

    monitor-exit v7

    throw v0
.end method

.method public static dump([BJLjava/io/OutputStream;II)V
    .locals 11
    .param p0, "data"    # [B
    .param p1, "offset"    # J
    .param p3, "stream"    # Ljava/io/OutputStream;
    .param p4, "index"    # I
    .param p5, "length"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ArrayIndexOutOfBoundsException;,
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 68
    array-length v7, p0

    if-nez v7, :cond_1

    .line 70
    new-instance v7, Ljava/lang/StringBuilder;

    const-string/jumbo v8, "No Data"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v8, "line.separator"

    invoke-static {v8}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->getBytes()[B

    move-result-object v7

    invoke-virtual {p3, v7}, Ljava/io/OutputStream;->write([B)V

    .line 71
    invoke-virtual {p3}, Ljava/io/OutputStream;->flush()V

    .line 131
    :cond_0
    return-void

    .line 74
    :cond_1
    if-ltz p4, :cond_2

    array-length v7, p0

    if-lt p4, v7, :cond_3

    .line 76
    :cond_2
    new-instance v7, Ljava/lang/ArrayIndexOutOfBoundsException;

    .line 77
    new-instance v8, Ljava/lang/StringBuilder;

    const-string/jumbo v9, "illegal index: "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, " into array of length "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    .line 78
    array-length v9, p0

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    .line 77
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 76
    invoke-direct {v7, v8}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 80
    :cond_3
    if-nez p3, :cond_4

    .line 82
    new-instance v7, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v8, "cannot write to nullstream"

    invoke-direct {v7, v8}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 85
    :cond_4
    int-to-long v8, p4

    add-long v4, p1, v8

    .line 86
    .local v4, "display_offset":J
    new-instance v0, Ljava/lang/StringBuffer;

    const/16 v7, 0x4a

    invoke-direct {v0, v7}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 89
    .local v0, "buffer":Ljava/lang/StringBuffer;
    array-length v7, p0

    add-int v8, p4, p5

    invoke-static {v7, v8}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 90
    .local v2, "data_length":I
    move v3, p4

    .local v3, "j":I
    :goto_0
    if-ge v3, v2, :cond_0

    .line 92
    sub-int v1, v2, v3

    .line 94
    .local v1, "chars_read":I
    const/16 v7, 0x10

    if-le v1, v7, :cond_5

    .line 96
    const/16 v1, 0x10

    .line 99
    :cond_5
    invoke-static {v4, v5}, Lorg/apache/poi/util/HexDump;->dump(J)Ljava/lang/String;

    move-result-object v7

    .line 98
    invoke-virtual {v0, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    .line 100
    const/16 v8, 0x20

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 101
    const/4 v6, 0x0

    .local v6, "k":I
    :goto_1
    const/16 v7, 0x10

    if-lt v6, v7, :cond_6

    .line 113
    const/4 v6, 0x0

    :goto_2
    if-lt v6, v1, :cond_8

    .line 124
    sget-object v7, Lorg/apache/poi/util/HexDump;->EOL:Ljava/lang/String;

    invoke-virtual {v0, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 125
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->getBytes()[B

    move-result-object v7

    invoke-virtual {p3, v7}, Ljava/io/OutputStream;->write([B)V

    .line 126
    invoke-virtual {p3}, Ljava/io/OutputStream;->flush()V

    .line 127
    const/4 v7, 0x0

    invoke-virtual {v0, v7}, Ljava/lang/StringBuffer;->setLength(I)V

    .line 128
    int-to-long v8, v1

    add-long/2addr v4, v8

    .line 90
    add-int/lit8 v3, v3, 0x10

    goto :goto_0

    .line 103
    :cond_6
    if-ge v6, v1, :cond_7

    .line 105
    add-int v7, v6, v3

    aget-byte v7, p0, v7

    invoke-static {v7}, Lorg/apache/poi/util/HexDump;->dump(B)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 111
    :goto_3
    const/16 v7, 0x20

    invoke-virtual {v0, v7}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 101
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 109
    :cond_7
    const-string/jumbo v7, "  "

    invoke-virtual {v0, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_3

    .line 115
    :cond_8
    add-int v7, v6, v3

    aget-byte v7, p0, v7

    const/16 v8, 0x20

    if-lt v7, v8, :cond_9

    add-int v7, v6, v3

    aget-byte v7, p0, v7

    const/16 v8, 0x7f

    if-ge v7, v8, :cond_9

    .line 117
    add-int v7, v6, v3

    aget-byte v7, p0, v7

    int-to-char v7, v7

    invoke-virtual {v0, v7}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 113
    :goto_4
    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    .line 121
    :cond_9
    const/16 v7, 0x2e

    invoke-virtual {v0, v7}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_4
.end method

.method public static intToHex(I)[C
    .locals 3
    .param p0, "value"    # I

    .prologue
    .line 439
    int-to-long v0, p0

    const/4 v2, 0x4

    invoke-static {v0, v1, v2}, Lorg/apache/poi/util/HexDump;->toHexChars(JI)[C

    move-result-object v0

    return-object v0
.end method

.method public static longToHex(J)[C
    .locals 2
    .param p0, "value"    # J

    .prologue
    .line 433
    const/16 v0, 0x8

    invoke-static {p0, p1, v0}, Lorg/apache/poi/util/HexDump;->toHexChars(JI)[C

    move-result-object v0

    return-object v0
.end method

.method public static shortToHex(I)[C
    .locals 3
    .param p0, "value"    # I

    .prologue
    .line 445
    int-to-long v0, p0

    const/4 v2, 0x2

    invoke-static {v0, v1, v2}, Lorg/apache/poi/util/HexDump;->toHexChars(JI)[C

    move-result-object v0

    return-object v0
.end method

.method public static toHex(B)Ljava/lang/String;
    .locals 3
    .param p0, "value"    # B

    .prologue
    .line 337
    int-to-long v0, p0

    const/4 v2, 0x2

    invoke-static {v0, v1, v2}, Lorg/apache/poi/util/HexDump;->toHex(JI)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static toHex(I)Ljava/lang/String;
    .locals 3
    .param p0, "value"    # I

    .prologue
    .line 348
    int-to-long v0, p0

    const/16 v2, 0x8

    invoke-static {v0, v1, v2}, Lorg/apache/poi/util/HexDump;->toHex(JI)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static toHex(J)Ljava/lang/String;
    .locals 2
    .param p0, "value"    # J

    .prologue
    .line 359
    const/16 v0, 0x10

    invoke-static {p0, p1, v0}, Lorg/apache/poi/util/HexDump;->toHex(JI)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static toHex(JI)Ljava/lang/String;
    .locals 8
    .param p0, "value"    # J
    .param p2, "digits"    # I

    .prologue
    .line 365
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1, p2}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 366
    .local v1, "result":Ljava/lang/StringBuffer;
    const/4 v0, 0x0

    .local v0, "j":I
    :goto_0
    if-lt v0, p2, :cond_0

    .line 370
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2

    .line 368
    :cond_0
    sget-object v2, Lorg/apache/poi/util/HexDump;->_hexcodes:[C

    sget-object v3, Lorg/apache/poi/util/HexDump;->_shifts:[I

    rsub-int/lit8 v4, p2, 0x10

    add-int/2addr v4, v0

    aget v3, v3, v4

    shr-long v4, p0, v3

    const-wide/16 v6, 0xf

    and-long/2addr v4, v6

    long-to-int v3, v4

    aget-char v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 366
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public static toHex(S)Ljava/lang/String;
    .locals 3
    .param p0, "value"    # S

    .prologue
    .line 326
    int-to-long v0, p0

    const/4 v2, 0x4

    invoke-static {v0, v1, v2}, Lorg/apache/poi/util/HexDump;->toHex(JI)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static toHex([B)Ljava/lang/String;
    .locals 3
    .param p0, "value"    # [B

    .prologue
    .line 251
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 252
    .local v0, "retVal":Ljava/lang/StringBuffer;
    const/16 v2, 0x5b

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 253
    const/4 v1, 0x0

    .local v1, "x":I
    :goto_0
    array-length v2, p0

    if-lt v1, v2, :cond_0

    .line 260
    const/16 v2, 0x5d

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 261
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2

    .line 255
    :cond_0
    if-lez v1, :cond_1

    .line 256
    const-string/jumbo v2, ", "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 258
    :cond_1
    aget-byte v2, p0, v1

    invoke-static {v2}, Lorg/apache/poi/util/HexDump;->toHex(B)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 253
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public static toHex([BI)Ljava/lang/String;
    .locals 10
    .param p0, "value"    # [B
    .param p1, "bytesPerLine"    # I

    .prologue
    .line 295
    array-length v6, p0

    int-to-double v6, v6

    invoke-static {v6, v7}, Ljava/lang/Math;->log(D)D

    move-result-wide v6

    const-wide/high16 v8, 0x4024000000000000L    # 10.0

    invoke-static {v8, v9}, Ljava/lang/Math;->log(D)D

    move-result-wide v8

    div-double/2addr v6, v8

    const-wide/high16 v8, 0x3fe0000000000000L    # 0.5

    add-double/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Math;->round(D)J

    move-result-wide v6

    long-to-int v0, v6

    .line 296
    .local v0, "digits":I
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    .line 297
    .local v2, "formatString":Ljava/lang/StringBuffer;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-lt v3, v0, :cond_0

    .line 299
    const-string/jumbo v6, ": "

    invoke-virtual {v2, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 300
    new-instance v1, Ljava/text/DecimalFormat;

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v1, v6}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    .line 301
    .local v1, "format":Ljava/text/DecimalFormat;
    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    .line 302
    .local v4, "retVal":Ljava/lang/StringBuffer;
    const-wide/16 v6, 0x0

    invoke-virtual {v1, v6, v7}, Ljava/text/DecimalFormat;->format(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 303
    const/4 v3, -0x1

    .line 304
    const/4 v5, 0x0

    .local v5, "x":I
    :goto_1
    array-length v6, p0

    if-lt v5, v6, :cond_1

    .line 315
    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    return-object v6

    .line 298
    .end local v1    # "format":Ljava/text/DecimalFormat;
    .end local v4    # "retVal":Ljava/lang/StringBuffer;
    .end local v5    # "x":I
    :cond_0
    const/16 v6, 0x30

    invoke-virtual {v2, v6}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 297
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 306
    .restart local v1    # "format":Ljava/text/DecimalFormat;
    .restart local v4    # "retVal":Ljava/lang/StringBuffer;
    .restart local v5    # "x":I
    :cond_1
    add-int/lit8 v3, v3, 0x1

    if-ne v3, p1, :cond_2

    .line 308
    const/16 v6, 0xa

    invoke-virtual {v4, v6}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 309
    int-to-long v6, v5

    invoke-virtual {v1, v6, v7}, Ljava/text/DecimalFormat;->format(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 310
    const/4 v3, 0x0

    .line 312
    :cond_2
    aget-byte v6, p0, v5

    invoke-static {v6}, Lorg/apache/poi/util/HexDump;->toHex(B)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 313
    const-string/jumbo v6, ", "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 304
    add-int/lit8 v5, v5, 0x1

    goto :goto_1
.end method

.method public static toHex([S)Ljava/lang/String;
    .locals 3
    .param p0, "value"    # [S

    .prologue
    .line 272
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 273
    .local v0, "retVal":Ljava/lang/StringBuffer;
    const/16 v2, 0x5b

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 274
    const/4 v1, 0x0

    .local v1, "x":I
    :goto_0
    array-length v2, p0

    if-lt v1, v2, :cond_0

    .line 279
    const/16 v2, 0x5d

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 280
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2

    .line 276
    :cond_0
    aget-short v2, p0, v1

    invoke-static {v2}, Lorg/apache/poi/util/HexDump;->toHex(S)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 277
    const-string/jumbo v2, ", "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 274
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private static toHexChars(JI)[C
    .locals 10
    .param p0, "pValue"    # J
    .param p2, "nBytes"    # I

    .prologue
    const/4 v8, 0x1

    .line 413
    mul-int/lit8 v4, p2, 0x2

    add-int/lit8 v0, v4, 0x2

    .line 416
    .local v0, "charPos":I
    new-array v1, v0, [C

    .line 418
    .local v1, "result":[C
    move-wide v2, p0

    .line 420
    .local v2, "value":J
    :cond_0
    add-int/lit8 v0, v0, -0x1

    sget-object v4, Lorg/apache/poi/util/HexDump;->_hexcodes:[C

    const-wide/16 v6, 0xf

    and-long/2addr v6, v2

    long-to-int v5, v6

    aget-char v4, v4, v5

    aput-char v4, v1, v0

    .line 421
    const/4 v4, 0x4

    ushr-long/2addr v2, v4

    .line 419
    if-gt v0, v8, :cond_0

    .line 425
    const/4 v4, 0x0

    const/16 v5, 0x30

    aput-char v5, v1, v4

    .line 426
    const/16 v4, 0x78

    aput-char v4, v1, v8

    .line 427
    return-object v1
.end method
