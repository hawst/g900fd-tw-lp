.class public Lorg/apache/poi/util/BitFieldFactory;
.super Ljava/lang/Object;
.source "BitFieldFactory.java"


# static fields
.field private static instances:Ljava/util/Map;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lorg/apache/poi/util/BitFieldFactory;->instances:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getInstance(I)Lorg/apache/poi/util/BitField;
    .locals 3
    .param p0, "mask"    # I

    .prologue
    .line 33
    sget-object v1, Lorg/apache/poi/util/BitFieldFactory;->instances:Ljava/util/Map;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/util/BitField;

    .line 34
    .local v0, "f":Lorg/apache/poi/util/BitField;
    if-nez v0, :cond_0

    .line 35
    new-instance v0, Lorg/apache/poi/util/BitField;

    .end local v0    # "f":Lorg/apache/poi/util/BitField;
    invoke-direct {v0, p0}, Lorg/apache/poi/util/BitField;-><init>(I)V

    .line 36
    .restart local v0    # "f":Lorg/apache/poi/util/BitField;
    sget-object v1, Lorg/apache/poi/util/BitFieldFactory;->instances:Ljava/util/Map;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 38
    :cond_0
    return-object v0
.end method
