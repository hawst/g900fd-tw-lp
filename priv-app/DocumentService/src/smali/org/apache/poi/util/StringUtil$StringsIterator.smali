.class public Lorg/apache/poi/util/StringUtil$StringsIterator;
.super Ljava/lang/Object;
.source "StringUtil.java"

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/poi/util/StringUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "StringsIterator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Iterator",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field private position:I

.field private strings:[Ljava/lang/String;


# direct methods
.method public constructor <init>([Ljava/lang/String;)V
    .locals 1
    .param p1, "strings"    # [Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 403
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 402
    iput v0, p0, Lorg/apache/poi/util/StringUtil$StringsIterator;->position:I

    .line 404
    if-eqz p1, :cond_0

    .line 405
    iput-object p1, p0, Lorg/apache/poi/util/StringUtil$StringsIterator;->strings:[Ljava/lang/String;

    .line 409
    :goto_0
    return-void

    .line 407
    :cond_0
    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lorg/apache/poi/util/StringUtil$StringsIterator;->strings:[Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method public hasNext()Z
    .locals 2

    .prologue
    .line 412
    iget v0, p0, Lorg/apache/poi/util/StringUtil$StringsIterator;->position:I

    iget-object v1, p0, Lorg/apache/poi/util/StringUtil$StringsIterator;->strings:[Ljava/lang/String;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic next()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/poi/util/StringUtil$StringsIterator;->next()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public next()Ljava/lang/String;
    .locals 2

    .prologue
    .line 415
    iget v0, p0, Lorg/apache/poi/util/StringUtil$StringsIterator;->position:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lorg/apache/poi/util/StringUtil$StringsIterator;->position:I

    .line 416
    .local v0, "ourPos":I
    iget-object v1, p0, Lorg/apache/poi/util/StringUtil$StringsIterator;->strings:[Ljava/lang/String;

    array-length v1, v1

    if-lt v0, v1, :cond_0

    .line 417
    new-instance v1, Ljava/lang/ArrayIndexOutOfBoundsException;

    invoke-direct {v1, v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(I)V

    throw v1

    .line 418
    :cond_0
    iget-object v1, p0, Lorg/apache/poi/util/StringUtil$StringsIterator;->strings:[Ljava/lang/String;

    aget-object v1, v1, v0

    return-object v1
.end method

.method public remove()V
    .locals 0

    .prologue
    .line 420
    return-void
.end method
