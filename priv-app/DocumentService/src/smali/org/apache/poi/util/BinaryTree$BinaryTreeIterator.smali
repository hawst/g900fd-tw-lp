.class abstract Lorg/apache/poi/util/BinaryTree$BinaryTreeIterator;
.super Ljava/lang/Object;
.source "BinaryTree.java"

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/poi/util/BinaryTree;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x402
    name = "BinaryTreeIterator"
.end annotation


# instance fields
.field private _expected_modifications:I

.field protected _last_returned_node:Lorg/apache/poi/util/BinaryTree$Node;

.field private _next_node:Lorg/apache/poi/util/BinaryTree$Node;

.field private _type:I

.field final synthetic this$0:Lorg/apache/poi/util/BinaryTree;


# direct methods
.method constructor <init>(Lorg/apache/poi/util/BinaryTree;I)V
    .locals 2
    .param p2, "type"    # I

    .prologue
    .line 1740
    iput-object p1, p0, Lorg/apache/poi/util/BinaryTree$BinaryTreeIterator;->this$0:Lorg/apache/poi/util/BinaryTree;

    .line 1739
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1741
    iput p2, p0, Lorg/apache/poi/util/BinaryTree$BinaryTreeIterator;->_type:I

    .line 1742
    iget v0, p1, Lorg/apache/poi/util/BinaryTree;->_modifications:I

    iput v0, p0, Lorg/apache/poi/util/BinaryTree$BinaryTreeIterator;->_expected_modifications:I

    .line 1743
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/poi/util/BinaryTree$BinaryTreeIterator;->_last_returned_node:Lorg/apache/poi/util/BinaryTree$Node;

    .line 1744
    iget-object v0, p1, Lorg/apache/poi/util/BinaryTree;->_root:[Lorg/apache/poi/util/BinaryTree$Node;

    iget v1, p0, Lorg/apache/poi/util/BinaryTree$BinaryTreeIterator;->_type:I

    aget-object v0, v0, v1

    iget v1, p0, Lorg/apache/poi/util/BinaryTree$BinaryTreeIterator;->_type:I

    invoke-static {v0, v1}, Lorg/apache/poi/util/BinaryTree;->leastNode(Lorg/apache/poi/util/BinaryTree$Node;I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/poi/util/BinaryTree$BinaryTreeIterator;->_next_node:Lorg/apache/poi/util/BinaryTree$Node;

    .line 1745
    return-void
.end method


# virtual methods
.method protected abstract doGetNext()Ljava/lang/Object;
.end method

.method public hasNext()Z
    .locals 1

    .prologue
    .line 1762
    iget-object v0, p0, Lorg/apache/poi/util/BinaryTree$BinaryTreeIterator;->_next_node:Lorg/apache/poi/util/BinaryTree$Node;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public next()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/util/NoSuchElementException;,
            Ljava/util/ConcurrentModificationException;
        }
    .end annotation

    .prologue
    .line 1780
    iget-object v0, p0, Lorg/apache/poi/util/BinaryTree$BinaryTreeIterator;->_next_node:Lorg/apache/poi/util/BinaryTree$Node;

    if-nez v0, :cond_0

    .line 1782
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 1784
    :cond_0
    iget-object v0, p0, Lorg/apache/poi/util/BinaryTree$BinaryTreeIterator;->this$0:Lorg/apache/poi/util/BinaryTree;

    iget v0, v0, Lorg/apache/poi/util/BinaryTree;->_modifications:I

    iget v1, p0, Lorg/apache/poi/util/BinaryTree$BinaryTreeIterator;->_expected_modifications:I

    if-eq v0, v1, :cond_1

    .line 1786
    new-instance v0, Ljava/util/ConcurrentModificationException;

    invoke-direct {v0}, Ljava/util/ConcurrentModificationException;-><init>()V

    throw v0

    .line 1788
    :cond_1
    iget-object v0, p0, Lorg/apache/poi/util/BinaryTree$BinaryTreeIterator;->_next_node:Lorg/apache/poi/util/BinaryTree$Node;

    iput-object v0, p0, Lorg/apache/poi/util/BinaryTree$BinaryTreeIterator;->_last_returned_node:Lorg/apache/poi/util/BinaryTree$Node;

    .line 1789
    iget-object v0, p0, Lorg/apache/poi/util/BinaryTree$BinaryTreeIterator;->_next_node:Lorg/apache/poi/util/BinaryTree$Node;

    iget v1, p0, Lorg/apache/poi/util/BinaryTree$BinaryTreeIterator;->_type:I

    invoke-static {v0, v1}, Lorg/apache/poi/util/BinaryTree;->nextGreater(Lorg/apache/poi/util/BinaryTree$Node;I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/poi/util/BinaryTree$BinaryTreeIterator;->_next_node:Lorg/apache/poi/util/BinaryTree$Node;

    .line 1790
    invoke-virtual {p0}, Lorg/apache/poi/util/BinaryTree$BinaryTreeIterator;->doGetNext()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public remove()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;,
            Ljava/util/ConcurrentModificationException;
        }
    .end annotation

    .prologue
    .line 1816
    iget-object v0, p0, Lorg/apache/poi/util/BinaryTree$BinaryTreeIterator;->_last_returned_node:Lorg/apache/poi/util/BinaryTree$Node;

    if-nez v0, :cond_0

    .line 1818
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 1820
    :cond_0
    iget-object v0, p0, Lorg/apache/poi/util/BinaryTree$BinaryTreeIterator;->this$0:Lorg/apache/poi/util/BinaryTree;

    iget v0, v0, Lorg/apache/poi/util/BinaryTree;->_modifications:I

    iget v1, p0, Lorg/apache/poi/util/BinaryTree$BinaryTreeIterator;->_expected_modifications:I

    if-eq v0, v1, :cond_1

    .line 1822
    new-instance v0, Ljava/util/ConcurrentModificationException;

    invoke-direct {v0}, Ljava/util/ConcurrentModificationException;-><init>()V

    throw v0

    .line 1824
    :cond_1
    iget-object v0, p0, Lorg/apache/poi/util/BinaryTree$BinaryTreeIterator;->this$0:Lorg/apache/poi/util/BinaryTree;

    iget-object v1, p0, Lorg/apache/poi/util/BinaryTree$BinaryTreeIterator;->_last_returned_node:Lorg/apache/poi/util/BinaryTree$Node;

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BinaryTree;->doRedBlackDelete(Lorg/apache/poi/util/BinaryTree$Node;)V

    .line 1825
    iget v0, p0, Lorg/apache/poi/util/BinaryTree$BinaryTreeIterator;->_expected_modifications:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/poi/util/BinaryTree$BinaryTreeIterator;->_expected_modifications:I

    .line 1826
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/poi/util/BinaryTree$BinaryTreeIterator;->_last_returned_node:Lorg/apache/poi/util/BinaryTree$Node;

    .line 1827
    return-void
.end method
