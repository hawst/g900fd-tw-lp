.class public Lorg/apache/poi/util/DrawingDump;
.super Ljava/lang/Object;
.source "DrawingDump.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static main([Ljava/lang/String;)V
    .locals 7
    .param p0, "args"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    .line 36
    new-instance v0, Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;

    new-instance v4, Ljava/io/FileInputStream;

    const/4 v5, 0x0

    aget-object v5, p0, v5

    invoke-direct {v4, v5}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v4}, Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;-><init>(Ljava/io/InputStream;)V

    .line 37
    .local v0, "fs":Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;
    new-instance v3, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;

    invoke-direct {v3, v0}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;-><init>(Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;)V

    .line 39
    .local v3, "wb":Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;
    invoke-virtual {v3, v6}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->dumpDrawingGroupRecords(Z)V

    .line 41
    const/4 v2, 0x1

    .local v2, "sheetNum":I
    :goto_0
    invoke-virtual {v3}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->getNumberOfSheets()I

    move-result v4

    if-le v2, v4, :cond_0

    .line 48
    return-void

    .line 44
    :cond_0
    add-int/lit8 v4, v2, -0x1

    invoke-virtual {v3, v4}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->getSheetAt(I)Lorg/apache/poi/hssf/usermodel/HSSFSheet;

    move-result-object v1

    .line 45
    .local v1, "sheet":Lorg/apache/poi/hssf/usermodel/HSSFSheet;
    invoke-virtual {v1, v6}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->dumpDrawingRecords(Z)V

    .line 41
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method
