.class public Lorg/apache/poi/util/BinaryTree;
.super Ljava/util/AbstractMap;
.source "BinaryTree.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/poi/util/BinaryTree$BinaryTreeIterator;,
        Lorg/apache/poi/util/BinaryTree$Node;
    }
.end annotation


# static fields
.field private static _INDEX_COUNT:I

.field private static _INDEX_SUM:I

.field static _KEY:I

.field private static _MINIMUM_INDEX:I

.field static _VALUE:I

.field private static _data_name:[Ljava/lang/String;


# instance fields
.field private final _entry_set:[Ljava/util/Set;

.field private final _key_set:[Ljava/util/Set;

.field _modifications:I

.field final _root:[Lorg/apache/poi/util/BinaryTree$Node;

.field _size:I

.field private final _value_collection:[Ljava/util/Collection;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 98
    sput v2, Lorg/apache/poi/util/BinaryTree;->_KEY:I

    .line 99
    sput v3, Lorg/apache/poi/util/BinaryTree;->_VALUE:I

    .line 100
    sget v0, Lorg/apache/poi/util/BinaryTree;->_KEY:I

    sget v1, Lorg/apache/poi/util/BinaryTree;->_VALUE:I

    add-int/2addr v0, v1

    sput v0, Lorg/apache/poi/util/BinaryTree;->_INDEX_SUM:I

    .line 101
    sput v2, Lorg/apache/poi/util/BinaryTree;->_MINIMUM_INDEX:I

    .line 102
    sput v4, Lorg/apache/poi/util/BinaryTree;->_INDEX_COUNT:I

    .line 104
    new-array v0, v4, [Ljava/lang/String;

    .line 105
    const-string/jumbo v1, "key"

    aput-object v1, v0, v2

    const-string/jumbo v1, "value"

    aput-object v1, v0, v3

    .line 103
    sput-object v0, Lorg/apache/poi/util/BinaryTree;->_data_name:[Ljava/lang/String;

    .line 106
    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x2

    .line 111
    invoke-direct {p0}, Ljava/util/AbstractMap;-><init>()V

    .line 93
    iput v0, p0, Lorg/apache/poi/util/BinaryTree;->_size:I

    .line 94
    iput v0, p0, Lorg/apache/poi/util/BinaryTree;->_modifications:I

    .line 95
    new-array v0, v1, [Ljava/util/Set;

    iput-object v0, p0, Lorg/apache/poi/util/BinaryTree;->_key_set:[Ljava/util/Set;

    .line 96
    new-array v0, v1, [Ljava/util/Set;

    iput-object v0, p0, Lorg/apache/poi/util/BinaryTree;->_entry_set:[Ljava/util/Set;

    .line 97
    new-array v0, v1, [Ljava/util/Collection;

    iput-object v0, p0, Lorg/apache/poi/util/BinaryTree;->_value_collection:[Ljava/util/Collection;

    .line 112
    new-array v0, v1, [Lorg/apache/poi/util/BinaryTree$Node;

    iput-object v0, p0, Lorg/apache/poi/util/BinaryTree;->_root:[Lorg/apache/poi/util/BinaryTree$Node;

    .line 113
    return-void
.end method

.method public constructor <init>(Ljava/util/Map;)V
    .locals 0
    .param p1, "map"    # Ljava/util/Map;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/ClassCastException;,
            Ljava/lang/NullPointerException;,
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 136
    invoke-direct {p0}, Lorg/apache/poi/util/BinaryTree;-><init>()V

    .line 137
    invoke-virtual {p0, p1}, Lorg/apache/poi/util/BinaryTree;->putAll(Ljava/util/Map;)V

    .line 138
    return-void
.end method

.method private static checkKey(Ljava/lang/Object;)V
    .locals 1
    .param p0, "key"    # Ljava/lang/Object;

    .prologue
    .line 1223
    sget v0, Lorg/apache/poi/util/BinaryTree;->_KEY:I

    invoke-static {p0, v0}, Lorg/apache/poi/util/BinaryTree;->checkNonNullComparable(Ljava/lang/Object;I)V

    .line 1224
    return-void
.end method

.method private static checkKeyAndValue(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p0, "key"    # Ljava/lang/Object;
    .param p1, "value"    # Ljava/lang/Object;

    .prologue
    .line 1251
    invoke-static {p0}, Lorg/apache/poi/util/BinaryTree;->checkKey(Ljava/lang/Object;)V

    .line 1252
    invoke-static {p1}, Lorg/apache/poi/util/BinaryTree;->checkValue(Ljava/lang/Object;)V

    .line 1253
    return-void
.end method

.method private static checkNonNullComparable(Ljava/lang/Object;I)V
    .locals 3
    .param p0, "o"    # Ljava/lang/Object;
    .param p1, "index"    # I

    .prologue
    .line 1201
    if-nez p0, :cond_0

    .line 1203
    new-instance v0, Ljava/lang/NullPointerException;

    new-instance v1, Ljava/lang/StringBuilder;

    sget-object v2, Lorg/apache/poi/util/BinaryTree;->_data_name:[Ljava/lang/String;

    aget-object v2, v2, p1

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1204
    const-string/jumbo v2, " cannot be null"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1203
    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1206
    :cond_0
    instance-of v0, p0, Ljava/lang/Comparable;

    if-nez v0, :cond_1

    .line 1208
    new-instance v0, Ljava/lang/ClassCastException;

    new-instance v1, Ljava/lang/StringBuilder;

    sget-object v2, Lorg/apache/poi/util/BinaryTree;->_data_name:[Ljava/lang/String;

    aget-object v2, v2, p1

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1209
    const-string/jumbo v2, " must be Comparable"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1208
    invoke-direct {v0, v1}, Ljava/lang/ClassCastException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1211
    :cond_1
    return-void
.end method

.method private static checkValue(Ljava/lang/Object;)V
    .locals 1
    .param p0, "value"    # Ljava/lang/Object;

    .prologue
    .line 1236
    sget v0, Lorg/apache/poi/util/BinaryTree;->_VALUE:I

    invoke-static {p0, v0}, Lorg/apache/poi/util/BinaryTree;->checkNonNullComparable(Ljava/lang/Object;I)V

    .line 1237
    return-void
.end method

.method private static compare(Ljava/lang/Comparable;Ljava/lang/Comparable;)I
    .locals 1
    .param p0, "o1"    # Ljava/lang/Comparable;
    .param p1, "o2"    # Ljava/lang/Comparable;

    .prologue
    .line 494
    invoke-interface {p0, p1}, Ljava/lang/Comparable;->compareTo(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method private static copyColor(Lorg/apache/poi/util/BinaryTree$Node;Lorg/apache/poi/util/BinaryTree$Node;I)V
    .locals 0
    .param p0, "from"    # Lorg/apache/poi/util/BinaryTree$Node;
    .param p1, "to"    # Lorg/apache/poi/util/BinaryTree$Node;
    .param p2, "index"    # I

    .prologue
    .line 576
    if-eqz p1, :cond_0

    .line 578
    if-nez p0, :cond_1

    .line 582
    invoke-virtual {p1, p2}, Lorg/apache/poi/util/BinaryTree$Node;->setBlack(I)V

    .line 589
    :cond_0
    :goto_0
    return-void

    .line 586
    :cond_1
    invoke-virtual {p1, p0, p2}, Lorg/apache/poi/util/BinaryTree$Node;->copyColor(Lorg/apache/poi/util/BinaryTree$Node;I)V

    goto :goto_0
.end method

.method private doGet(Ljava/lang/Comparable;I)Ljava/lang/Object;
    .locals 2
    .param p1, "o"    # Ljava/lang/Comparable;
    .param p2, "index"    # I

    .prologue
    .line 431
    invoke-static {p1, p2}, Lorg/apache/poi/util/BinaryTree;->checkNonNullComparable(Ljava/lang/Object;I)V

    .line 432
    invoke-virtual {p0, p1, p2}, Lorg/apache/poi/util/BinaryTree;->lookup(Ljava/lang/Comparable;I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v0

    .line 434
    .local v0, "node":Lorg/apache/poi/util/BinaryTree$Node;
    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    .line 435
    :cond_0
    invoke-direct {p0, p2}, Lorg/apache/poi/util/BinaryTree;->oppositeIndex(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lorg/apache/poi/util/BinaryTree$Node;->getData(I)Ljava/lang/Comparable;

    move-result-object v1

    goto :goto_0
.end method

.method private doRedBlackDeleteFixup(Lorg/apache/poi/util/BinaryTree$Node;I)V
    .locals 3
    .param p1, "replacement_node"    # Lorg/apache/poi/util/BinaryTree$Node;
    .param p2, "index"    # I

    .prologue
    .line 982
    move-object v0, p1

    .line 984
    .local v0, "current_node":Lorg/apache/poi/util/BinaryTree$Node;
    :goto_0
    iget-object v2, p0, Lorg/apache/poi/util/BinaryTree;->_root:[Lorg/apache/poi/util/BinaryTree$Node;

    aget-object v2, v2, p2

    if-eq v0, v2, :cond_0

    .line 985
    invoke-static {v0, p2}, Lorg/apache/poi/util/BinaryTree;->isBlack(Lorg/apache/poi/util/BinaryTree$Node;I)Z

    move-result v2

    if-nez v2, :cond_1

    .line 1064
    :cond_0
    invoke-static {v0, p2}, Lorg/apache/poi/util/BinaryTree;->makeBlack(Lorg/apache/poi/util/BinaryTree$Node;I)V

    .line 1065
    return-void

    .line 987
    :cond_1
    invoke-static {v0, p2}, Lorg/apache/poi/util/BinaryTree;->isLeftChild(Lorg/apache/poi/util/BinaryTree$Node;I)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 990
    invoke-static {v0, p2}, Lorg/apache/poi/util/BinaryTree;->getParent(Lorg/apache/poi/util/BinaryTree$Node;I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v2

    invoke-static {v2, p2}, Lorg/apache/poi/util/BinaryTree;->getRightChild(Lorg/apache/poi/util/BinaryTree$Node;I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v1

    .line 992
    .local v1, "sibling_node":Lorg/apache/poi/util/BinaryTree$Node;
    invoke-static {v1, p2}, Lorg/apache/poi/util/BinaryTree;->isRed(Lorg/apache/poi/util/BinaryTree$Node;I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 994
    invoke-static {v1, p2}, Lorg/apache/poi/util/BinaryTree;->makeBlack(Lorg/apache/poi/util/BinaryTree$Node;I)V

    .line 995
    invoke-static {v0, p2}, Lorg/apache/poi/util/BinaryTree;->getParent(Lorg/apache/poi/util/BinaryTree$Node;I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v2

    invoke-static {v2, p2}, Lorg/apache/poi/util/BinaryTree;->makeRed(Lorg/apache/poi/util/BinaryTree$Node;I)V

    .line 996
    invoke-static {v0, p2}, Lorg/apache/poi/util/BinaryTree;->getParent(Lorg/apache/poi/util/BinaryTree$Node;I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v2

    invoke-direct {p0, v2, p2}, Lorg/apache/poi/util/BinaryTree;->rotateLeft(Lorg/apache/poi/util/BinaryTree$Node;I)V

    .line 998
    invoke-static {v0, p2}, Lorg/apache/poi/util/BinaryTree;->getParent(Lorg/apache/poi/util/BinaryTree$Node;I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v2

    invoke-static {v2, p2}, Lorg/apache/poi/util/BinaryTree;->getRightChild(Lorg/apache/poi/util/BinaryTree$Node;I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v1

    .line 1000
    :cond_2
    invoke-static {v1, p2}, Lorg/apache/poi/util/BinaryTree;->getLeftChild(Lorg/apache/poi/util/BinaryTree$Node;I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v2

    invoke-static {v2, p2}, Lorg/apache/poi/util/BinaryTree;->isBlack(Lorg/apache/poi/util/BinaryTree$Node;I)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1001
    invoke-static {v1, p2}, Lorg/apache/poi/util/BinaryTree;->getRightChild(Lorg/apache/poi/util/BinaryTree$Node;I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v2

    invoke-static {v2, p2}, Lorg/apache/poi/util/BinaryTree;->isBlack(Lorg/apache/poi/util/BinaryTree$Node;I)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1003
    invoke-static {v1, p2}, Lorg/apache/poi/util/BinaryTree;->makeRed(Lorg/apache/poi/util/BinaryTree$Node;I)V

    .line 1004
    invoke-static {v0, p2}, Lorg/apache/poi/util/BinaryTree;->getParent(Lorg/apache/poi/util/BinaryTree$Node;I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v0

    .line 1005
    goto :goto_0

    .line 1008
    :cond_3
    invoke-static {v1, p2}, Lorg/apache/poi/util/BinaryTree;->getRightChild(Lorg/apache/poi/util/BinaryTree$Node;I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v2

    invoke-static {v2, p2}, Lorg/apache/poi/util/BinaryTree;->isBlack(Lorg/apache/poi/util/BinaryTree$Node;I)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1010
    invoke-static {v1, p2}, Lorg/apache/poi/util/BinaryTree;->getLeftChild(Lorg/apache/poi/util/BinaryTree$Node;I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v2

    invoke-static {v2, p2}, Lorg/apache/poi/util/BinaryTree;->makeBlack(Lorg/apache/poi/util/BinaryTree$Node;I)V

    .line 1011
    invoke-static {v1, p2}, Lorg/apache/poi/util/BinaryTree;->makeRed(Lorg/apache/poi/util/BinaryTree$Node;I)V

    .line 1012
    invoke-direct {p0, v1, p2}, Lorg/apache/poi/util/BinaryTree;->rotateRight(Lorg/apache/poi/util/BinaryTree$Node;I)V

    .line 1014
    invoke-static {v0, p2}, Lorg/apache/poi/util/BinaryTree;->getParent(Lorg/apache/poi/util/BinaryTree$Node;I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v2

    invoke-static {v2, p2}, Lorg/apache/poi/util/BinaryTree;->getRightChild(Lorg/apache/poi/util/BinaryTree$Node;I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v1

    .line 1017
    :cond_4
    invoke-static {v0, p2}, Lorg/apache/poi/util/BinaryTree;->getParent(Lorg/apache/poi/util/BinaryTree$Node;I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v2

    invoke-static {v2, v1, p2}, Lorg/apache/poi/util/BinaryTree;->copyColor(Lorg/apache/poi/util/BinaryTree$Node;Lorg/apache/poi/util/BinaryTree$Node;I)V

    .line 1019
    invoke-static {v0, p2}, Lorg/apache/poi/util/BinaryTree;->getParent(Lorg/apache/poi/util/BinaryTree$Node;I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v2

    invoke-static {v2, p2}, Lorg/apache/poi/util/BinaryTree;->makeBlack(Lorg/apache/poi/util/BinaryTree$Node;I)V

    .line 1020
    invoke-static {v1, p2}, Lorg/apache/poi/util/BinaryTree;->getRightChild(Lorg/apache/poi/util/BinaryTree$Node;I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v2

    invoke-static {v2, p2}, Lorg/apache/poi/util/BinaryTree;->makeBlack(Lorg/apache/poi/util/BinaryTree$Node;I)V

    .line 1021
    invoke-static {v0, p2}, Lorg/apache/poi/util/BinaryTree;->getParent(Lorg/apache/poi/util/BinaryTree$Node;I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v2

    invoke-direct {p0, v2, p2}, Lorg/apache/poi/util/BinaryTree;->rotateLeft(Lorg/apache/poi/util/BinaryTree$Node;I)V

    .line 1022
    iget-object v2, p0, Lorg/apache/poi/util/BinaryTree;->_root:[Lorg/apache/poi/util/BinaryTree$Node;

    aget-object v0, v2, p2

    .line 1024
    goto/16 :goto_0

    .line 1028
    .end local v1    # "sibling_node":Lorg/apache/poi/util/BinaryTree$Node;
    :cond_5
    invoke-static {v0, p2}, Lorg/apache/poi/util/BinaryTree;->getParent(Lorg/apache/poi/util/BinaryTree$Node;I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v2

    invoke-static {v2, p2}, Lorg/apache/poi/util/BinaryTree;->getLeftChild(Lorg/apache/poi/util/BinaryTree$Node;I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v1

    .line 1030
    .restart local v1    # "sibling_node":Lorg/apache/poi/util/BinaryTree$Node;
    invoke-static {v1, p2}, Lorg/apache/poi/util/BinaryTree;->isRed(Lorg/apache/poi/util/BinaryTree$Node;I)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 1032
    invoke-static {v1, p2}, Lorg/apache/poi/util/BinaryTree;->makeBlack(Lorg/apache/poi/util/BinaryTree$Node;I)V

    .line 1033
    invoke-static {v0, p2}, Lorg/apache/poi/util/BinaryTree;->getParent(Lorg/apache/poi/util/BinaryTree$Node;I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v2

    invoke-static {v2, p2}, Lorg/apache/poi/util/BinaryTree;->makeRed(Lorg/apache/poi/util/BinaryTree$Node;I)V

    .line 1034
    invoke-static {v0, p2}, Lorg/apache/poi/util/BinaryTree;->getParent(Lorg/apache/poi/util/BinaryTree$Node;I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v2

    invoke-direct {p0, v2, p2}, Lorg/apache/poi/util/BinaryTree;->rotateRight(Lorg/apache/poi/util/BinaryTree$Node;I)V

    .line 1036
    invoke-static {v0, p2}, Lorg/apache/poi/util/BinaryTree;->getParent(Lorg/apache/poi/util/BinaryTree$Node;I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v2

    invoke-static {v2, p2}, Lorg/apache/poi/util/BinaryTree;->getLeftChild(Lorg/apache/poi/util/BinaryTree$Node;I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v1

    .line 1038
    :cond_6
    invoke-static {v1, p2}, Lorg/apache/poi/util/BinaryTree;->getRightChild(Lorg/apache/poi/util/BinaryTree$Node;I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v2

    invoke-static {v2, p2}, Lorg/apache/poi/util/BinaryTree;->isBlack(Lorg/apache/poi/util/BinaryTree$Node;I)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 1039
    invoke-static {v1, p2}, Lorg/apache/poi/util/BinaryTree;->getLeftChild(Lorg/apache/poi/util/BinaryTree$Node;I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v2

    invoke-static {v2, p2}, Lorg/apache/poi/util/BinaryTree;->isBlack(Lorg/apache/poi/util/BinaryTree$Node;I)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 1041
    invoke-static {v1, p2}, Lorg/apache/poi/util/BinaryTree;->makeRed(Lorg/apache/poi/util/BinaryTree$Node;I)V

    .line 1042
    invoke-static {v0, p2}, Lorg/apache/poi/util/BinaryTree;->getParent(Lorg/apache/poi/util/BinaryTree$Node;I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v0

    .line 1043
    goto/16 :goto_0

    .line 1046
    :cond_7
    invoke-static {v1, p2}, Lorg/apache/poi/util/BinaryTree;->getLeftChild(Lorg/apache/poi/util/BinaryTree$Node;I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v2

    invoke-static {v2, p2}, Lorg/apache/poi/util/BinaryTree;->isBlack(Lorg/apache/poi/util/BinaryTree$Node;I)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 1048
    invoke-static {v1, p2}, Lorg/apache/poi/util/BinaryTree;->getRightChild(Lorg/apache/poi/util/BinaryTree$Node;I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v2

    invoke-static {v2, p2}, Lorg/apache/poi/util/BinaryTree;->makeBlack(Lorg/apache/poi/util/BinaryTree$Node;I)V

    .line 1049
    invoke-static {v1, p2}, Lorg/apache/poi/util/BinaryTree;->makeRed(Lorg/apache/poi/util/BinaryTree$Node;I)V

    .line 1050
    invoke-direct {p0, v1, p2}, Lorg/apache/poi/util/BinaryTree;->rotateLeft(Lorg/apache/poi/util/BinaryTree$Node;I)V

    .line 1052
    invoke-static {v0, p2}, Lorg/apache/poi/util/BinaryTree;->getParent(Lorg/apache/poi/util/BinaryTree$Node;I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v2

    invoke-static {v2, p2}, Lorg/apache/poi/util/BinaryTree;->getLeftChild(Lorg/apache/poi/util/BinaryTree$Node;I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v1

    .line 1055
    :cond_8
    invoke-static {v0, p2}, Lorg/apache/poi/util/BinaryTree;->getParent(Lorg/apache/poi/util/BinaryTree$Node;I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v2

    invoke-static {v2, v1, p2}, Lorg/apache/poi/util/BinaryTree;->copyColor(Lorg/apache/poi/util/BinaryTree$Node;Lorg/apache/poi/util/BinaryTree$Node;I)V

    .line 1057
    invoke-static {v0, p2}, Lorg/apache/poi/util/BinaryTree;->getParent(Lorg/apache/poi/util/BinaryTree$Node;I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v2

    invoke-static {v2, p2}, Lorg/apache/poi/util/BinaryTree;->makeBlack(Lorg/apache/poi/util/BinaryTree$Node;I)V

    .line 1058
    invoke-static {v1, p2}, Lorg/apache/poi/util/BinaryTree;->getLeftChild(Lorg/apache/poi/util/BinaryTree$Node;I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v2

    invoke-static {v2, p2}, Lorg/apache/poi/util/BinaryTree;->makeBlack(Lorg/apache/poi/util/BinaryTree$Node;I)V

    .line 1059
    invoke-static {v0, p2}, Lorg/apache/poi/util/BinaryTree;->getParent(Lorg/apache/poi/util/BinaryTree$Node;I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v2

    invoke-direct {p0, v2, p2}, Lorg/apache/poi/util/BinaryTree;->rotateRight(Lorg/apache/poi/util/BinaryTree$Node;I)V

    .line 1060
    iget-object v2, p0, Lorg/apache/poi/util/BinaryTree;->_root:[Lorg/apache/poi/util/BinaryTree$Node;

    aget-object v0, v2, p2

    goto/16 :goto_0
.end method

.method private doRedBlackInsert(Lorg/apache/poi/util/BinaryTree$Node;I)V
    .locals 3
    .param p1, "inserted_node"    # Lorg/apache/poi/util/BinaryTree$Node;
    .param p2, "index"    # I

    .prologue
    .line 814
    move-object v0, p1

    .line 816
    .local v0, "current_node":Lorg/apache/poi/util/BinaryTree$Node;
    invoke-static {v0, p2}, Lorg/apache/poi/util/BinaryTree;->makeRed(Lorg/apache/poi/util/BinaryTree$Node;I)V

    .line 817
    :cond_0
    :goto_0
    if-eqz v0, :cond_1

    iget-object v2, p0, Lorg/apache/poi/util/BinaryTree;->_root:[Lorg/apache/poi/util/BinaryTree$Node;

    aget-object v2, v2, p2

    if-eq v0, v2, :cond_1

    .line 818
    invoke-virtual {v0, p2}, Lorg/apache/poi/util/BinaryTree$Node;->getParent(I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v2

    invoke-static {v2, p2}, Lorg/apache/poi/util/BinaryTree;->isRed(Lorg/apache/poi/util/BinaryTree$Node;I)Z

    move-result v2

    if-nez v2, :cond_2

    .line 879
    :cond_1
    iget-object v2, p0, Lorg/apache/poi/util/BinaryTree;->_root:[Lorg/apache/poi/util/BinaryTree$Node;

    aget-object v2, v2, p2

    invoke-static {v2, p2}, Lorg/apache/poi/util/BinaryTree;->makeBlack(Lorg/apache/poi/util/BinaryTree$Node;I)V

    .line 880
    return-void

    .line 820
    :cond_2
    invoke-static {v0, p2}, Lorg/apache/poi/util/BinaryTree;->getParent(Lorg/apache/poi/util/BinaryTree$Node;I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v2

    invoke-static {v2, p2}, Lorg/apache/poi/util/BinaryTree;->isLeftChild(Lorg/apache/poi/util/BinaryTree$Node;I)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 822
    invoke-static {v0, p2}, Lorg/apache/poi/util/BinaryTree;->getGrandParent(Lorg/apache/poi/util/BinaryTree$Node;I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v2

    invoke-static {v2, p2}, Lorg/apache/poi/util/BinaryTree;->getRightChild(Lorg/apache/poi/util/BinaryTree$Node;I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v1

    .line 825
    .local v1, "y":Lorg/apache/poi/util/BinaryTree$Node;
    invoke-static {v1, p2}, Lorg/apache/poi/util/BinaryTree;->isRed(Lorg/apache/poi/util/BinaryTree$Node;I)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 827
    invoke-static {v0, p2}, Lorg/apache/poi/util/BinaryTree;->getParent(Lorg/apache/poi/util/BinaryTree$Node;I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v2

    invoke-static {v2, p2}, Lorg/apache/poi/util/BinaryTree;->makeBlack(Lorg/apache/poi/util/BinaryTree$Node;I)V

    .line 828
    invoke-static {v1, p2}, Lorg/apache/poi/util/BinaryTree;->makeBlack(Lorg/apache/poi/util/BinaryTree$Node;I)V

    .line 829
    invoke-static {v0, p2}, Lorg/apache/poi/util/BinaryTree;->getGrandParent(Lorg/apache/poi/util/BinaryTree$Node;I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v2

    invoke-static {v2, p2}, Lorg/apache/poi/util/BinaryTree;->makeRed(Lorg/apache/poi/util/BinaryTree$Node;I)V

    .line 830
    invoke-static {v0, p2}, Lorg/apache/poi/util/BinaryTree;->getGrandParent(Lorg/apache/poi/util/BinaryTree$Node;I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v0

    .line 831
    goto :goto_0

    .line 834
    :cond_3
    invoke-static {v0, p2}, Lorg/apache/poi/util/BinaryTree;->isRightChild(Lorg/apache/poi/util/BinaryTree$Node;I)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 836
    invoke-static {v0, p2}, Lorg/apache/poi/util/BinaryTree;->getParent(Lorg/apache/poi/util/BinaryTree$Node;I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v0

    .line 837
    invoke-direct {p0, v0, p2}, Lorg/apache/poi/util/BinaryTree;->rotateLeft(Lorg/apache/poi/util/BinaryTree$Node;I)V

    .line 839
    :cond_4
    invoke-static {v0, p2}, Lorg/apache/poi/util/BinaryTree;->getParent(Lorg/apache/poi/util/BinaryTree$Node;I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v2

    invoke-static {v2, p2}, Lorg/apache/poi/util/BinaryTree;->makeBlack(Lorg/apache/poi/util/BinaryTree$Node;I)V

    .line 840
    invoke-static {v0, p2}, Lorg/apache/poi/util/BinaryTree;->getGrandParent(Lorg/apache/poi/util/BinaryTree$Node;I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v2

    invoke-static {v2, p2}, Lorg/apache/poi/util/BinaryTree;->makeRed(Lorg/apache/poi/util/BinaryTree$Node;I)V

    .line 841
    invoke-static {v0, p2}, Lorg/apache/poi/util/BinaryTree;->getGrandParent(Lorg/apache/poi/util/BinaryTree$Node;I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 843
    invoke-static {v0, p2}, Lorg/apache/poi/util/BinaryTree;->getGrandParent(Lorg/apache/poi/util/BinaryTree$Node;I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v2

    invoke-direct {p0, v2, p2}, Lorg/apache/poi/util/BinaryTree;->rotateRight(Lorg/apache/poi/util/BinaryTree$Node;I)V

    goto :goto_0

    .line 852
    .end local v1    # "y":Lorg/apache/poi/util/BinaryTree$Node;
    :cond_5
    invoke-static {v0, p2}, Lorg/apache/poi/util/BinaryTree;->getGrandParent(Lorg/apache/poi/util/BinaryTree$Node;I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v2

    invoke-static {v2, p2}, Lorg/apache/poi/util/BinaryTree;->getLeftChild(Lorg/apache/poi/util/BinaryTree$Node;I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v1

    .line 855
    .restart local v1    # "y":Lorg/apache/poi/util/BinaryTree$Node;
    invoke-static {v1, p2}, Lorg/apache/poi/util/BinaryTree;->isRed(Lorg/apache/poi/util/BinaryTree$Node;I)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 857
    invoke-static {v0, p2}, Lorg/apache/poi/util/BinaryTree;->getParent(Lorg/apache/poi/util/BinaryTree$Node;I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v2

    invoke-static {v2, p2}, Lorg/apache/poi/util/BinaryTree;->makeBlack(Lorg/apache/poi/util/BinaryTree$Node;I)V

    .line 858
    invoke-static {v1, p2}, Lorg/apache/poi/util/BinaryTree;->makeBlack(Lorg/apache/poi/util/BinaryTree$Node;I)V

    .line 859
    invoke-static {v0, p2}, Lorg/apache/poi/util/BinaryTree;->getGrandParent(Lorg/apache/poi/util/BinaryTree$Node;I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v2

    invoke-static {v2, p2}, Lorg/apache/poi/util/BinaryTree;->makeRed(Lorg/apache/poi/util/BinaryTree$Node;I)V

    .line 860
    invoke-static {v0, p2}, Lorg/apache/poi/util/BinaryTree;->getGrandParent(Lorg/apache/poi/util/BinaryTree$Node;I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v0

    .line 861
    goto/16 :goto_0

    .line 864
    :cond_6
    invoke-static {v0, p2}, Lorg/apache/poi/util/BinaryTree;->isLeftChild(Lorg/apache/poi/util/BinaryTree$Node;I)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 866
    invoke-static {v0, p2}, Lorg/apache/poi/util/BinaryTree;->getParent(Lorg/apache/poi/util/BinaryTree$Node;I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v0

    .line 867
    invoke-direct {p0, v0, p2}, Lorg/apache/poi/util/BinaryTree;->rotateRight(Lorg/apache/poi/util/BinaryTree$Node;I)V

    .line 869
    :cond_7
    invoke-static {v0, p2}, Lorg/apache/poi/util/BinaryTree;->getParent(Lorg/apache/poi/util/BinaryTree$Node;I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v2

    invoke-static {v2, p2}, Lorg/apache/poi/util/BinaryTree;->makeBlack(Lorg/apache/poi/util/BinaryTree$Node;I)V

    .line 870
    invoke-static {v0, p2}, Lorg/apache/poi/util/BinaryTree;->getGrandParent(Lorg/apache/poi/util/BinaryTree$Node;I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v2

    invoke-static {v2, p2}, Lorg/apache/poi/util/BinaryTree;->makeRed(Lorg/apache/poi/util/BinaryTree$Node;I)V

    .line 871
    invoke-static {v0, p2}, Lorg/apache/poi/util/BinaryTree;->getGrandParent(Lorg/apache/poi/util/BinaryTree$Node;I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 873
    invoke-static {v0, p2}, Lorg/apache/poi/util/BinaryTree;->getGrandParent(Lorg/apache/poi/util/BinaryTree$Node;I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v2

    invoke-direct {p0, v2, p2}, Lorg/apache/poi/util/BinaryTree;->rotateLeft(Lorg/apache/poi/util/BinaryTree$Node;I)V

    goto/16 :goto_0
.end method

.method private doRemove(Ljava/lang/Comparable;I)Ljava/lang/Object;
    .locals 3
    .param p1, "o"    # Ljava/lang/Comparable;
    .param p2, "index"    # I

    .prologue
    .line 408
    invoke-virtual {p0, p1, p2}, Lorg/apache/poi/util/BinaryTree;->lookup(Ljava/lang/Comparable;I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v0

    .line 409
    .local v0, "node":Lorg/apache/poi/util/BinaryTree$Node;
    const/4 v1, 0x0

    .line 411
    .local v1, "rval":Ljava/lang/Object;
    if-eqz v0, :cond_0

    .line 413
    invoke-direct {p0, p2}, Lorg/apache/poi/util/BinaryTree;->oppositeIndex(I)I

    move-result v2

    invoke-virtual {v0, v2}, Lorg/apache/poi/util/BinaryTree$Node;->getData(I)Ljava/lang/Comparable;

    move-result-object v1

    .line 414
    .local v1, "rval":Ljava/lang/Comparable;
    invoke-virtual {p0, v0}, Lorg/apache/poi/util/BinaryTree;->doRedBlackDelete(Lorg/apache/poi/util/BinaryTree$Node;)V

    .line 416
    .end local v1    # "rval":Ljava/lang/Comparable;
    :cond_0
    return-object v1
.end method

.method private static getGrandParent(Lorg/apache/poi/util/BinaryTree$Node;I)Lorg/apache/poi/util/BinaryTree$Node;
    .locals 1
    .param p0, "node"    # Lorg/apache/poi/util/BinaryTree$Node;
    .param p1, "index"    # I

    .prologue
    .line 652
    invoke-static {p0, p1}, Lorg/apache/poi/util/BinaryTree;->getParent(Lorg/apache/poi/util/BinaryTree$Node;I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v0

    invoke-static {v0, p1}, Lorg/apache/poi/util/BinaryTree;->getParent(Lorg/apache/poi/util/BinaryTree$Node;I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v0

    return-object v0
.end method

.method private static getLeftChild(Lorg/apache/poi/util/BinaryTree$Node;I)Lorg/apache/poi/util/BinaryTree$Node;
    .locals 1
    .param p0, "node"    # Lorg/apache/poi/util/BinaryTree$Node;
    .param p1, "index"    # I

    .prologue
    .line 690
    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 691
    :cond_0
    invoke-virtual {p0, p1}, Lorg/apache/poi/util/BinaryTree$Node;->getLeft(I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v0

    goto :goto_0
.end method

.method private static getParent(Lorg/apache/poi/util/BinaryTree$Node;I)Lorg/apache/poi/util/BinaryTree$Node;
    .locals 1
    .param p0, "node"    # Lorg/apache/poi/util/BinaryTree$Node;
    .param p1, "index"    # I

    .prologue
    .line 664
    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 665
    :cond_0
    invoke-virtual {p0, p1}, Lorg/apache/poi/util/BinaryTree$Node;->getParent(I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v0

    goto :goto_0
.end method

.method private static getRightChild(Lorg/apache/poi/util/BinaryTree$Node;I)Lorg/apache/poi/util/BinaryTree$Node;
    .locals 1
    .param p0, "node"    # Lorg/apache/poi/util/BinaryTree$Node;
    .param p1, "index"    # I

    .prologue
    .line 677
    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 678
    :cond_0
    invoke-virtual {p0, p1}, Lorg/apache/poi/util/BinaryTree$Node;->getRight(I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v0

    goto :goto_0
.end method

.method private grow()V
    .locals 1

    .prologue
    .line 1270
    invoke-direct {p0}, Lorg/apache/poi/util/BinaryTree;->modify()V

    .line 1271
    iget v0, p0, Lorg/apache/poi/util/BinaryTree;->_size:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/poi/util/BinaryTree;->_size:I

    .line 1272
    return-void
.end method

.method private insertValue(Lorg/apache/poi/util/BinaryTree$Node;)V
    .locals 5
    .param p1, "newNode"    # Lorg/apache/poi/util/BinaryTree$Node;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 1294
    iget-object v2, p0, Lorg/apache/poi/util/BinaryTree;->_root:[Lorg/apache/poi/util/BinaryTree$Node;

    sget v3, Lorg/apache/poi/util/BinaryTree;->_VALUE:I

    aget-object v1, v2, v3

    .line 1298
    .local v1, "node":Lorg/apache/poi/util/BinaryTree$Node;
    :goto_0
    sget v2, Lorg/apache/poi/util/BinaryTree;->_VALUE:I

    invoke-virtual {p1, v2}, Lorg/apache/poi/util/BinaryTree$Node;->getData(I)Ljava/lang/Comparable;

    move-result-object v2

    sget v3, Lorg/apache/poi/util/BinaryTree;->_VALUE:I

    invoke-virtual {v1, v3}, Lorg/apache/poi/util/BinaryTree$Node;->getData(I)Ljava/lang/Comparable;

    move-result-object v3

    invoke-static {v2, v3}, Lorg/apache/poi/util/BinaryTree;->compare(Ljava/lang/Comparable;Ljava/lang/Comparable;)I

    move-result v0

    .line 1300
    .local v0, "cmp":I
    if-nez v0, :cond_0

    .line 1302
    new-instance v2, Ljava/lang/IllegalArgumentException;

    .line 1303
    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "Cannot store a duplicate value (\""

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1304
    sget v4, Lorg/apache/poi/util/BinaryTree;->_VALUE:I

    invoke-virtual {p1, v4}, Lorg/apache/poi/util/BinaryTree$Node;->getData(I)Ljava/lang/Comparable;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "\") in this Map"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 1303
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1302
    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 1306
    :cond_0
    if-gez v0, :cond_2

    .line 1308
    sget v2, Lorg/apache/poi/util/BinaryTree;->_VALUE:I

    invoke-virtual {v1, v2}, Lorg/apache/poi/util/BinaryTree$Node;->getLeft(I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 1310
    sget v2, Lorg/apache/poi/util/BinaryTree;->_VALUE:I

    invoke-virtual {v1, v2}, Lorg/apache/poi/util/BinaryTree$Node;->getLeft(I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v1

    .line 1311
    goto :goto_0

    .line 1314
    :cond_1
    sget v2, Lorg/apache/poi/util/BinaryTree;->_VALUE:I

    invoke-virtual {v1, p1, v2}, Lorg/apache/poi/util/BinaryTree$Node;->setLeft(Lorg/apache/poi/util/BinaryTree$Node;I)V

    .line 1315
    sget v2, Lorg/apache/poi/util/BinaryTree;->_VALUE:I

    invoke-virtual {p1, v1, v2}, Lorg/apache/poi/util/BinaryTree$Node;->setParent(Lorg/apache/poi/util/BinaryTree$Node;I)V

    .line 1316
    sget v2, Lorg/apache/poi/util/BinaryTree;->_VALUE:I

    invoke-direct {p0, p1, v2}, Lorg/apache/poi/util/BinaryTree;->doRedBlackInsert(Lorg/apache/poi/util/BinaryTree$Node;I)V

    .line 1335
    :goto_1
    return-void

    .line 1322
    :cond_2
    sget v2, Lorg/apache/poi/util/BinaryTree;->_VALUE:I

    invoke-virtual {v1, v2}, Lorg/apache/poi/util/BinaryTree$Node;->getRight(I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 1324
    sget v2, Lorg/apache/poi/util/BinaryTree;->_VALUE:I

    invoke-virtual {v1, v2}, Lorg/apache/poi/util/BinaryTree$Node;->getRight(I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v1

    .line 1325
    goto :goto_0

    .line 1328
    :cond_3
    sget v2, Lorg/apache/poi/util/BinaryTree;->_VALUE:I

    invoke-virtual {v1, p1, v2}, Lorg/apache/poi/util/BinaryTree$Node;->setRight(Lorg/apache/poi/util/BinaryTree$Node;I)V

    .line 1329
    sget v2, Lorg/apache/poi/util/BinaryTree;->_VALUE:I

    invoke-virtual {p1, v1, v2}, Lorg/apache/poi/util/BinaryTree$Node;->setParent(Lorg/apache/poi/util/BinaryTree$Node;I)V

    .line 1330
    sget v2, Lorg/apache/poi/util/BinaryTree;->_VALUE:I

    invoke-direct {p0, p1, v2}, Lorg/apache/poi/util/BinaryTree;->doRedBlackInsert(Lorg/apache/poi/util/BinaryTree$Node;I)V

    goto :goto_1
.end method

.method private static isBlack(Lorg/apache/poi/util/BinaryTree$Node;I)Z
    .locals 1
    .param p0, "node"    # Lorg/apache/poi/util/BinaryTree$Node;
    .param p1, "index"    # I

    .prologue
    .line 612
    if-nez p0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0, p1}, Lorg/apache/poi/util/BinaryTree$Node;->isBlack(I)Z

    move-result v0

    goto :goto_0
.end method

.method private static isLeftChild(Lorg/apache/poi/util/BinaryTree$Node;I)Z
    .locals 3
    .param p0, "node"    # Lorg/apache/poi/util/BinaryTree$Node;
    .param p1, "index"    # I

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 706
    if-nez p0, :cond_1

    .line 712
    :cond_0
    :goto_0
    return v0

    .line 709
    :cond_1
    invoke-virtual {p0, p1}, Lorg/apache/poi/util/BinaryTree$Node;->getParent(I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v2

    if-nez v2, :cond_2

    move v0, v1

    .line 710
    goto :goto_0

    .line 712
    :cond_2
    invoke-virtual {p0, p1}, Lorg/apache/poi/util/BinaryTree$Node;->getParent(I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v2

    invoke-virtual {v2, p1}, Lorg/apache/poi/util/BinaryTree$Node;->getLeft(I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v2

    if-eq p0, v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method private static isRed(Lorg/apache/poi/util/BinaryTree$Node;I)Z
    .locals 1
    .param p0, "node"    # Lorg/apache/poi/util/BinaryTree$Node;
    .param p1, "index"    # I

    .prologue
    .line 600
    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0, p1}, Lorg/apache/poi/util/BinaryTree$Node;->isRed(I)Z

    move-result v0

    goto :goto_0
.end method

.method private static isRightChild(Lorg/apache/poi/util/BinaryTree$Node;I)Z
    .locals 3
    .param p0, "node"    # Lorg/apache/poi/util/BinaryTree$Node;
    .param p1, "index"    # I

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 728
    if-nez p0, :cond_1

    .line 734
    :cond_0
    :goto_0
    return v0

    .line 731
    :cond_1
    invoke-virtual {p0, p1}, Lorg/apache/poi/util/BinaryTree$Node;->getParent(I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v2

    if-nez v2, :cond_2

    move v0, v1

    .line 732
    goto :goto_0

    .line 734
    :cond_2
    invoke-virtual {p0, p1}, Lorg/apache/poi/util/BinaryTree$Node;->getParent(I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v2

    invoke-virtual {v2, p1}, Lorg/apache/poi/util/BinaryTree$Node;->getRight(I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v2

    if-eq p0, v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method static leastNode(Lorg/apache/poi/util/BinaryTree$Node;I)Lorg/apache/poi/util/BinaryTree$Node;
    .locals 2
    .param p0, "node"    # Lorg/apache/poi/util/BinaryTree$Node;
    .param p1, "index"    # I

    .prologue
    .line 509
    move-object v0, p0

    .line 511
    .local v0, "rval":Lorg/apache/poi/util/BinaryTree$Node;
    if-eqz v0, :cond_0

    .line 513
    :goto_0
    invoke-virtual {v0, p1}, Lorg/apache/poi/util/BinaryTree$Node;->getLeft(I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v1

    if-nez v1, :cond_1

    .line 518
    :cond_0
    return-object v0

    .line 515
    :cond_1
    invoke-virtual {v0, p1}, Lorg/apache/poi/util/BinaryTree$Node;->getLeft(I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v0

    goto :goto_0
.end method

.method private static makeBlack(Lorg/apache/poi/util/BinaryTree$Node;I)V
    .locals 0
    .param p0, "node"    # Lorg/apache/poi/util/BinaryTree$Node;
    .param p1, "index"    # I

    .prologue
    .line 637
    if-eqz p0, :cond_0

    .line 639
    invoke-virtual {p0, p1}, Lorg/apache/poi/util/BinaryTree$Node;->setBlack(I)V

    .line 641
    :cond_0
    return-void
.end method

.method private static makeRed(Lorg/apache/poi/util/BinaryTree$Node;I)V
    .locals 0
    .param p0, "node"    # Lorg/apache/poi/util/BinaryTree$Node;
    .param p1, "index"    # I

    .prologue
    .line 623
    if-eqz p0, :cond_0

    .line 625
    invoke-virtual {p0, p1}, Lorg/apache/poi/util/BinaryTree$Node;->setRed(I)V

    .line 627
    :cond_0
    return-void
.end method

.method private modify()V
    .locals 1

    .prologue
    .line 1262
    iget v0, p0, Lorg/apache/poi/util/BinaryTree;->_modifications:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/poi/util/BinaryTree;->_modifications:I

    .line 1263
    return-void
.end method

.method static nextGreater(Lorg/apache/poi/util/BinaryTree$Node;I)Lorg/apache/poi/util/BinaryTree$Node;
    .locals 4
    .param p0, "node"    # Lorg/apache/poi/util/BinaryTree$Node;
    .param p1, "index"    # I

    .prologue
    .line 531
    const/4 v2, 0x0

    .line 533
    .local v2, "rval":Lorg/apache/poi/util/BinaryTree$Node;
    if-nez p0, :cond_0

    .line 535
    const/4 v2, 0x0

    .line 563
    :goto_0
    return-object v2

    .line 537
    :cond_0
    invoke-virtual {p0, p1}, Lorg/apache/poi/util/BinaryTree$Node;->getRight(I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 542
    invoke-virtual {p0, p1}, Lorg/apache/poi/util/BinaryTree$Node;->getRight(I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v3

    invoke-static {v3, p1}, Lorg/apache/poi/util/BinaryTree;->leastNode(Lorg/apache/poi/util/BinaryTree$Node;I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v2

    .line 543
    goto :goto_0

    .line 553
    :cond_1
    invoke-virtual {p0, p1}, Lorg/apache/poi/util/BinaryTree$Node;->getParent(I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v1

    .line 554
    .local v1, "parent":Lorg/apache/poi/util/BinaryTree$Node;
    move-object v0, p0

    .line 556
    .local v0, "child":Lorg/apache/poi/util/BinaryTree$Node;
    :goto_1
    if-eqz v1, :cond_2

    invoke-virtual {v1, p1}, Lorg/apache/poi/util/BinaryTree$Node;->getRight(I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v3

    if-eq v0, v3, :cond_3

    .line 561
    :cond_2
    move-object v2, v1

    goto :goto_0

    .line 558
    :cond_3
    move-object v0, v1

    .line 559
    invoke-virtual {v1, p1}, Lorg/apache/poi/util/BinaryTree$Node;->getParent(I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v1

    goto :goto_1
.end method

.method private oppositeIndex(I)I
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 451
    sget v0, Lorg/apache/poi/util/BinaryTree;->_INDEX_SUM:I

    sub-int/2addr v0, p1

    return v0
.end method

.method private rotateLeft(Lorg/apache/poi/util/BinaryTree$Node;I)V
    .locals 2
    .param p1, "node"    # Lorg/apache/poi/util/BinaryTree$Node;
    .param p2, "index"    # I

    .prologue
    .line 745
    invoke-virtual {p1, p2}, Lorg/apache/poi/util/BinaryTree$Node;->getRight(I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v0

    .line 747
    .local v0, "right_child":Lorg/apache/poi/util/BinaryTree$Node;
    invoke-virtual {v0, p2}, Lorg/apache/poi/util/BinaryTree$Node;->getLeft(I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v1

    invoke-virtual {p1, v1, p2}, Lorg/apache/poi/util/BinaryTree$Node;->setRight(Lorg/apache/poi/util/BinaryTree$Node;I)V

    .line 748
    invoke-virtual {v0, p2}, Lorg/apache/poi/util/BinaryTree$Node;->getLeft(I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 750
    invoke-virtual {v0, p2}, Lorg/apache/poi/util/BinaryTree$Node;->getLeft(I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Lorg/apache/poi/util/BinaryTree$Node;->setParent(Lorg/apache/poi/util/BinaryTree$Node;I)V

    .line 752
    :cond_0
    invoke-virtual {p1, p2}, Lorg/apache/poi/util/BinaryTree$Node;->getParent(I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, Lorg/apache/poi/util/BinaryTree$Node;->setParent(Lorg/apache/poi/util/BinaryTree$Node;I)V

    .line 753
    invoke-virtual {p1, p2}, Lorg/apache/poi/util/BinaryTree$Node;->getParent(I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v1

    if-nez v1, :cond_1

    .line 757
    iget-object v1, p0, Lorg/apache/poi/util/BinaryTree;->_root:[Lorg/apache/poi/util/BinaryTree$Node;

    aput-object v0, v1, p2

    .line 767
    :goto_0
    invoke-virtual {v0, p1, p2}, Lorg/apache/poi/util/BinaryTree$Node;->setLeft(Lorg/apache/poi/util/BinaryTree$Node;I)V

    .line 768
    invoke-virtual {p1, v0, p2}, Lorg/apache/poi/util/BinaryTree$Node;->setParent(Lorg/apache/poi/util/BinaryTree$Node;I)V

    .line 769
    return-void

    .line 759
    :cond_1
    invoke-virtual {p1, p2}, Lorg/apache/poi/util/BinaryTree$Node;->getParent(I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v1

    invoke-virtual {v1, p2}, Lorg/apache/poi/util/BinaryTree$Node;->getLeft(I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v1

    if-ne v1, p1, :cond_2

    .line 761
    invoke-virtual {p1, p2}, Lorg/apache/poi/util/BinaryTree$Node;->getParent(I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v1

    invoke-virtual {v1, v0, p2}, Lorg/apache/poi/util/BinaryTree$Node;->setLeft(Lorg/apache/poi/util/BinaryTree$Node;I)V

    goto :goto_0

    .line 765
    :cond_2
    invoke-virtual {p1, p2}, Lorg/apache/poi/util/BinaryTree$Node;->getParent(I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v1

    invoke-virtual {v1, v0, p2}, Lorg/apache/poi/util/BinaryTree$Node;->setRight(Lorg/apache/poi/util/BinaryTree$Node;I)V

    goto :goto_0
.end method

.method private rotateRight(Lorg/apache/poi/util/BinaryTree$Node;I)V
    .locals 2
    .param p1, "node"    # Lorg/apache/poi/util/BinaryTree$Node;
    .param p2, "index"    # I

    .prologue
    .line 779
    invoke-virtual {p1, p2}, Lorg/apache/poi/util/BinaryTree$Node;->getLeft(I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v0

    .line 781
    .local v0, "left_child":Lorg/apache/poi/util/BinaryTree$Node;
    invoke-virtual {v0, p2}, Lorg/apache/poi/util/BinaryTree$Node;->getRight(I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v1

    invoke-virtual {p1, v1, p2}, Lorg/apache/poi/util/BinaryTree$Node;->setLeft(Lorg/apache/poi/util/BinaryTree$Node;I)V

    .line 782
    invoke-virtual {v0, p2}, Lorg/apache/poi/util/BinaryTree$Node;->getRight(I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 784
    invoke-virtual {v0, p2}, Lorg/apache/poi/util/BinaryTree$Node;->getRight(I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Lorg/apache/poi/util/BinaryTree$Node;->setParent(Lorg/apache/poi/util/BinaryTree$Node;I)V

    .line 786
    :cond_0
    invoke-virtual {p1, p2}, Lorg/apache/poi/util/BinaryTree$Node;->getParent(I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, Lorg/apache/poi/util/BinaryTree$Node;->setParent(Lorg/apache/poi/util/BinaryTree$Node;I)V

    .line 787
    invoke-virtual {p1, p2}, Lorg/apache/poi/util/BinaryTree$Node;->getParent(I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v1

    if-nez v1, :cond_1

    .line 791
    iget-object v1, p0, Lorg/apache/poi/util/BinaryTree;->_root:[Lorg/apache/poi/util/BinaryTree$Node;

    aput-object v0, v1, p2

    .line 801
    :goto_0
    invoke-virtual {v0, p1, p2}, Lorg/apache/poi/util/BinaryTree$Node;->setRight(Lorg/apache/poi/util/BinaryTree$Node;I)V

    .line 802
    invoke-virtual {p1, v0, p2}, Lorg/apache/poi/util/BinaryTree$Node;->setParent(Lorg/apache/poi/util/BinaryTree$Node;I)V

    .line 803
    return-void

    .line 793
    :cond_1
    invoke-virtual {p1, p2}, Lorg/apache/poi/util/BinaryTree$Node;->getParent(I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v1

    invoke-virtual {v1, p2}, Lorg/apache/poi/util/BinaryTree$Node;->getRight(I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v1

    if-ne v1, p1, :cond_2

    .line 795
    invoke-virtual {p1, p2}, Lorg/apache/poi/util/BinaryTree$Node;->getParent(I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v1

    invoke-virtual {v1, v0, p2}, Lorg/apache/poi/util/BinaryTree$Node;->setRight(Lorg/apache/poi/util/BinaryTree$Node;I)V

    goto :goto_0

    .line 799
    :cond_2
    invoke-virtual {p1, p2}, Lorg/apache/poi/util/BinaryTree$Node;->getParent(I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v1

    invoke-virtual {v1, v0, p2}, Lorg/apache/poi/util/BinaryTree$Node;->setLeft(Lorg/apache/poi/util/BinaryTree$Node;I)V

    goto :goto_0
.end method

.method private shrink()V
    .locals 1

    .prologue
    .line 1279
    invoke-direct {p0}, Lorg/apache/poi/util/BinaryTree;->modify()V

    .line 1280
    iget v0, p0, Lorg/apache/poi/util/BinaryTree;->_size:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lorg/apache/poi/util/BinaryTree;->_size:I

    .line 1281
    return-void
.end method

.method private swapPosition(Lorg/apache/poi/util/BinaryTree$Node;Lorg/apache/poi/util/BinaryTree$Node;I)V
    .locals 11
    .param p1, "x"    # Lorg/apache/poi/util/BinaryTree$Node;
    .param p2, "y"    # Lorg/apache/poi/util/BinaryTree$Node;
    .param p3, "index"    # I

    .prologue
    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 1080
    invoke-virtual {p1, p3}, Lorg/apache/poi/util/BinaryTree$Node;->getParent(I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v1

    .line 1081
    .local v1, "x_old_parent":Lorg/apache/poi/util/BinaryTree$Node;
    invoke-virtual {p1, p3}, Lorg/apache/poi/util/BinaryTree$Node;->getLeft(I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v0

    .line 1082
    .local v0, "x_old_left_child":Lorg/apache/poi/util/BinaryTree$Node;
    invoke-virtual {p1, p3}, Lorg/apache/poi/util/BinaryTree$Node;->getRight(I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v2

    .line 1083
    .local v2, "x_old_right_child":Lorg/apache/poi/util/BinaryTree$Node;
    invoke-virtual {p2, p3}, Lorg/apache/poi/util/BinaryTree$Node;->getParent(I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v5

    .line 1084
    .local v5, "y_old_parent":Lorg/apache/poi/util/BinaryTree$Node;
    invoke-virtual {p2, p3}, Lorg/apache/poi/util/BinaryTree$Node;->getLeft(I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v4

    .line 1085
    .local v4, "y_old_left_child":Lorg/apache/poi/util/BinaryTree$Node;
    invoke-virtual {p2, p3}, Lorg/apache/poi/util/BinaryTree$Node;->getRight(I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v6

    .line 1087
    .local v6, "y_old_right_child":Lorg/apache/poi/util/BinaryTree$Node;
    invoke-virtual {p1, p3}, Lorg/apache/poi/util/BinaryTree$Node;->getParent(I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v10

    if-eqz v10, :cond_5

    .line 1088
    invoke-virtual {p1, p3}, Lorg/apache/poi/util/BinaryTree$Node;->getParent(I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v10

    invoke-virtual {v10, p3}, Lorg/apache/poi/util/BinaryTree$Node;->getLeft(I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v10

    if-ne p1, v10, :cond_5

    move v3, v8

    .line 1090
    .local v3, "x_was_left_child":Z
    :goto_0
    invoke-virtual {p2, p3}, Lorg/apache/poi/util/BinaryTree$Node;->getParent(I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v10

    if-eqz v10, :cond_6

    .line 1091
    invoke-virtual {p2, p3}, Lorg/apache/poi/util/BinaryTree$Node;->getParent(I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v10

    invoke-virtual {v10, p3}, Lorg/apache/poi/util/BinaryTree$Node;->getLeft(I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v10

    if-ne p2, v10, :cond_6

    move v7, v8

    .line 1094
    .local v7, "y_was_left_child":Z
    :goto_1
    if-ne p1, v5, :cond_8

    .line 1096
    invoke-virtual {p1, p2, p3}, Lorg/apache/poi/util/BinaryTree$Node;->setParent(Lorg/apache/poi/util/BinaryTree$Node;I)V

    .line 1097
    if-eqz v7, :cond_7

    .line 1099
    invoke-virtual {p2, p1, p3}, Lorg/apache/poi/util/BinaryTree$Node;->setLeft(Lorg/apache/poi/util/BinaryTree$Node;I)V

    .line 1100
    invoke-virtual {p2, v2, p3}, Lorg/apache/poi/util/BinaryTree$Node;->setRight(Lorg/apache/poi/util/BinaryTree$Node;I)V

    .line 1125
    :goto_2
    if-ne p2, v1, :cond_c

    .line 1127
    invoke-virtual {p2, p1, p3}, Lorg/apache/poi/util/BinaryTree$Node;->setParent(Lorg/apache/poi/util/BinaryTree$Node;I)V

    .line 1128
    if-eqz v3, :cond_b

    .line 1130
    invoke-virtual {p1, p2, p3}, Lorg/apache/poi/util/BinaryTree$Node;->setLeft(Lorg/apache/poi/util/BinaryTree$Node;I)V

    .line 1131
    invoke-virtual {p1, v6, p3}, Lorg/apache/poi/util/BinaryTree$Node;->setRight(Lorg/apache/poi/util/BinaryTree$Node;I)V

    .line 1158
    :goto_3
    invoke-virtual {p1, p3}, Lorg/apache/poi/util/BinaryTree$Node;->getLeft(I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v8

    if-eqz v8, :cond_0

    .line 1160
    invoke-virtual {p1, p3}, Lorg/apache/poi/util/BinaryTree$Node;->getLeft(I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v8

    invoke-virtual {v8, p1, p3}, Lorg/apache/poi/util/BinaryTree$Node;->setParent(Lorg/apache/poi/util/BinaryTree$Node;I)V

    .line 1162
    :cond_0
    invoke-virtual {p1, p3}, Lorg/apache/poi/util/BinaryTree$Node;->getRight(I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v8

    if-eqz v8, :cond_1

    .line 1164
    invoke-virtual {p1, p3}, Lorg/apache/poi/util/BinaryTree$Node;->getRight(I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v8

    invoke-virtual {v8, p1, p3}, Lorg/apache/poi/util/BinaryTree$Node;->setParent(Lorg/apache/poi/util/BinaryTree$Node;I)V

    .line 1166
    :cond_1
    invoke-virtual {p2, p3}, Lorg/apache/poi/util/BinaryTree$Node;->getLeft(I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v8

    if-eqz v8, :cond_2

    .line 1168
    invoke-virtual {p2, p3}, Lorg/apache/poi/util/BinaryTree$Node;->getLeft(I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v8

    invoke-virtual {v8, p2, p3}, Lorg/apache/poi/util/BinaryTree$Node;->setParent(Lorg/apache/poi/util/BinaryTree$Node;I)V

    .line 1170
    :cond_2
    invoke-virtual {p2, p3}, Lorg/apache/poi/util/BinaryTree$Node;->getRight(I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v8

    if-eqz v8, :cond_3

    .line 1172
    invoke-virtual {p2, p3}, Lorg/apache/poi/util/BinaryTree$Node;->getRight(I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v8

    invoke-virtual {v8, p2, p3}, Lorg/apache/poi/util/BinaryTree$Node;->setParent(Lorg/apache/poi/util/BinaryTree$Node;I)V

    .line 1174
    :cond_3
    invoke-virtual {p1, p2, p3}, Lorg/apache/poi/util/BinaryTree$Node;->swapColors(Lorg/apache/poi/util/BinaryTree$Node;I)V

    .line 1177
    iget-object v8, p0, Lorg/apache/poi/util/BinaryTree;->_root:[Lorg/apache/poi/util/BinaryTree$Node;

    aget-object v8, v8, p3

    if-ne v8, p1, :cond_f

    .line 1179
    iget-object v8, p0, Lorg/apache/poi/util/BinaryTree;->_root:[Lorg/apache/poi/util/BinaryTree$Node;

    aput-object p2, v8, p3

    .line 1185
    :cond_4
    :goto_4
    return-void

    .end local v3    # "x_was_left_child":Z
    .end local v7    # "y_was_left_child":Z
    :cond_5
    move v3, v9

    .line 1086
    goto :goto_0

    .restart local v3    # "x_was_left_child":Z
    :cond_6
    move v7, v9

    .line 1089
    goto :goto_1

    .line 1104
    .restart local v7    # "y_was_left_child":Z
    :cond_7
    invoke-virtual {p2, p1, p3}, Lorg/apache/poi/util/BinaryTree$Node;->setRight(Lorg/apache/poi/util/BinaryTree$Node;I)V

    .line 1105
    invoke-virtual {p2, v0, p3}, Lorg/apache/poi/util/BinaryTree$Node;->setLeft(Lorg/apache/poi/util/BinaryTree$Node;I)V

    goto :goto_2

    .line 1110
    :cond_8
    invoke-virtual {p1, v5, p3}, Lorg/apache/poi/util/BinaryTree$Node;->setParent(Lorg/apache/poi/util/BinaryTree$Node;I)V

    .line 1111
    if-eqz v5, :cond_9

    .line 1113
    if-eqz v7, :cond_a

    .line 1115
    invoke-virtual {v5, p1, p3}, Lorg/apache/poi/util/BinaryTree$Node;->setLeft(Lorg/apache/poi/util/BinaryTree$Node;I)V

    .line 1122
    :cond_9
    :goto_5
    invoke-virtual {p2, v0, p3}, Lorg/apache/poi/util/BinaryTree$Node;->setLeft(Lorg/apache/poi/util/BinaryTree$Node;I)V

    .line 1123
    invoke-virtual {p2, v2, p3}, Lorg/apache/poi/util/BinaryTree$Node;->setRight(Lorg/apache/poi/util/BinaryTree$Node;I)V

    goto :goto_2

    .line 1119
    :cond_a
    invoke-virtual {v5, p1, p3}, Lorg/apache/poi/util/BinaryTree$Node;->setRight(Lorg/apache/poi/util/BinaryTree$Node;I)V

    goto :goto_5

    .line 1135
    :cond_b
    invoke-virtual {p1, p2, p3}, Lorg/apache/poi/util/BinaryTree$Node;->setRight(Lorg/apache/poi/util/BinaryTree$Node;I)V

    .line 1136
    invoke-virtual {p1, v4, p3}, Lorg/apache/poi/util/BinaryTree$Node;->setLeft(Lorg/apache/poi/util/BinaryTree$Node;I)V

    goto :goto_3

    .line 1141
    :cond_c
    invoke-virtual {p2, v1, p3}, Lorg/apache/poi/util/BinaryTree$Node;->setParent(Lorg/apache/poi/util/BinaryTree$Node;I)V

    .line 1142
    if-eqz v1, :cond_d

    .line 1144
    if-eqz v3, :cond_e

    .line 1146
    invoke-virtual {v1, p2, p3}, Lorg/apache/poi/util/BinaryTree$Node;->setLeft(Lorg/apache/poi/util/BinaryTree$Node;I)V

    .line 1153
    :cond_d
    :goto_6
    invoke-virtual {p1, v4, p3}, Lorg/apache/poi/util/BinaryTree$Node;->setLeft(Lorg/apache/poi/util/BinaryTree$Node;I)V

    .line 1154
    invoke-virtual {p1, v6, p3}, Lorg/apache/poi/util/BinaryTree$Node;->setRight(Lorg/apache/poi/util/BinaryTree$Node;I)V

    goto :goto_3

    .line 1150
    :cond_e
    invoke-virtual {v1, p2, p3}, Lorg/apache/poi/util/BinaryTree$Node;->setRight(Lorg/apache/poi/util/BinaryTree$Node;I)V

    goto :goto_6

    .line 1181
    :cond_f
    iget-object v8, p0, Lorg/apache/poi/util/BinaryTree;->_root:[Lorg/apache/poi/util/BinaryTree$Node;

    aget-object v8, v8, p3

    if-ne v8, p2, :cond_4

    .line 1183
    iget-object v8, p0, Lorg/apache/poi/util/BinaryTree;->_root:[Lorg/apache/poi/util/BinaryTree$Node;

    aput-object p1, v8, p3

    goto :goto_4
.end method


# virtual methods
.method public clear()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1513
    invoke-direct {p0}, Lorg/apache/poi/util/BinaryTree;->modify()V

    .line 1514
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/poi/util/BinaryTree;->_size:I

    .line 1515
    iget-object v0, p0, Lorg/apache/poi/util/BinaryTree;->_root:[Lorg/apache/poi/util/BinaryTree$Node;

    sget v1, Lorg/apache/poi/util/BinaryTree;->_KEY:I

    aput-object v2, v0, v1

    .line 1516
    iget-object v0, p0, Lorg/apache/poi/util/BinaryTree;->_root:[Lorg/apache/poi/util/BinaryTree$Node;

    sget v1, Lorg/apache/poi/util/BinaryTree;->_VALUE:I

    aput-object v2, v0, v1

    .line 1517
    return-void
.end method

.method public containsKey(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "key"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/ClassCastException;,
            Ljava/lang/NullPointerException;
        }
    .end annotation

    .prologue
    .line 1367
    invoke-static {p1}, Lorg/apache/poi/util/BinaryTree;->checkKey(Ljava/lang/Object;)V

    .line 1368
    check-cast p1, Ljava/lang/Comparable;

    .end local p1    # "key":Ljava/lang/Object;
    sget v0, Lorg/apache/poi/util/BinaryTree;->_KEY:I

    invoke-virtual {p0, p1, v0}, Lorg/apache/poi/util/BinaryTree;->lookup(Ljava/lang/Comparable;I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public containsValue(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "value"    # Ljava/lang/Object;

    .prologue
    .line 1382
    invoke-static {p1}, Lorg/apache/poi/util/BinaryTree;->checkValue(Ljava/lang/Object;)V

    .line 1383
    check-cast p1, Ljava/lang/Comparable;

    .end local p1    # "value":Ljava/lang/Object;
    sget v0, Lorg/apache/poi/util/BinaryTree;->_VALUE:I

    invoke-virtual {p0, p1, v0}, Lorg/apache/poi/util/BinaryTree;->lookup(Ljava/lang/Comparable;I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method doRedBlackDelete(Lorg/apache/poi/util/BinaryTree$Node;)V
    .locals 4
    .param p1, "deleted_node"    # Lorg/apache/poi/util/BinaryTree$Node;

    .prologue
    const/4 v3, 0x0

    .line 890
    sget v0, Lorg/apache/poi/util/BinaryTree;->_MINIMUM_INDEX:I

    .local v0, "index":I
    :goto_0
    sget v2, Lorg/apache/poi/util/BinaryTree;->_INDEX_COUNT:I

    if-lt v0, v2, :cond_0

    .line 967
    invoke-direct {p0}, Lorg/apache/poi/util/BinaryTree;->shrink()V

    .line 968
    return-void

    .line 895
    :cond_0
    invoke-virtual {p1, v0}, Lorg/apache/poi/util/BinaryTree$Node;->getLeft(I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 896
    invoke-virtual {p1, v0}, Lorg/apache/poi/util/BinaryTree$Node;->getRight(I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 898
    invoke-static {p1, v0}, Lorg/apache/poi/util/BinaryTree;->nextGreater(Lorg/apache/poi/util/BinaryTree$Node;I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v2

    invoke-direct {p0, v2, p1, v0}, Lorg/apache/poi/util/BinaryTree;->swapPosition(Lorg/apache/poi/util/BinaryTree$Node;Lorg/apache/poi/util/BinaryTree$Node;I)V

    .line 901
    :cond_1
    invoke-virtual {p1, v0}, Lorg/apache/poi/util/BinaryTree$Node;->getLeft(I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 902
    invoke-virtual {p1, v0}, Lorg/apache/poi/util/BinaryTree$Node;->getLeft(I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v1

    .line 905
    .local v1, "replacement":Lorg/apache/poi/util/BinaryTree$Node;
    :goto_1
    if-eqz v1, :cond_6

    .line 907
    invoke-virtual {p1, v0}, Lorg/apache/poi/util/BinaryTree$Node;->getParent(I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lorg/apache/poi/util/BinaryTree$Node;->setParent(Lorg/apache/poi/util/BinaryTree$Node;I)V

    .line 908
    invoke-virtual {p1, v0}, Lorg/apache/poi/util/BinaryTree$Node;->getParent(I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v2

    if-nez v2, :cond_4

    .line 910
    iget-object v2, p0, Lorg/apache/poi/util/BinaryTree;->_root:[Lorg/apache/poi/util/BinaryTree$Node;

    aput-object v1, v2, v0

    .line 922
    :goto_2
    invoke-virtual {p1, v3, v0}, Lorg/apache/poi/util/BinaryTree$Node;->setLeft(Lorg/apache/poi/util/BinaryTree$Node;I)V

    .line 923
    invoke-virtual {p1, v3, v0}, Lorg/apache/poi/util/BinaryTree$Node;->setRight(Lorg/apache/poi/util/BinaryTree$Node;I)V

    .line 924
    invoke-virtual {p1, v3, v0}, Lorg/apache/poi/util/BinaryTree$Node;->setParent(Lorg/apache/poi/util/BinaryTree$Node;I)V

    .line 925
    invoke-static {p1, v0}, Lorg/apache/poi/util/BinaryTree;->isBlack(Lorg/apache/poi/util/BinaryTree$Node;I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 927
    invoke-direct {p0, v1, v0}, Lorg/apache/poi/util/BinaryTree;->doRedBlackDeleteFixup(Lorg/apache/poi/util/BinaryTree$Node;I)V

    .line 890
    :cond_2
    :goto_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 903
    .end local v1    # "replacement":Lorg/apache/poi/util/BinaryTree$Node;
    :cond_3
    invoke-virtual {p1, v0}, Lorg/apache/poi/util/BinaryTree$Node;->getRight(I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v1

    goto :goto_1

    .line 913
    .restart local v1    # "replacement":Lorg/apache/poi/util/BinaryTree$Node;
    :cond_4
    invoke-virtual {p1, v0}, Lorg/apache/poi/util/BinaryTree$Node;->getParent(I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v2

    invoke-virtual {v2, v0}, Lorg/apache/poi/util/BinaryTree$Node;->getLeft(I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v2

    .line 912
    if-ne p1, v2, :cond_5

    .line 915
    invoke-virtual {p1, v0}, Lorg/apache/poi/util/BinaryTree$Node;->getParent(I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v2

    invoke-virtual {v2, v1, v0}, Lorg/apache/poi/util/BinaryTree$Node;->setLeft(Lorg/apache/poi/util/BinaryTree$Node;I)V

    goto :goto_2

    .line 919
    :cond_5
    invoke-virtual {p1, v0}, Lorg/apache/poi/util/BinaryTree$Node;->getParent(I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v2

    invoke-virtual {v2, v1, v0}, Lorg/apache/poi/util/BinaryTree$Node;->setRight(Lorg/apache/poi/util/BinaryTree$Node;I)V

    goto :goto_2

    .line 934
    :cond_6
    invoke-virtual {p1, v0}, Lorg/apache/poi/util/BinaryTree$Node;->getParent(I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v2

    if-nez v2, :cond_7

    .line 938
    iget-object v2, p0, Lorg/apache/poi/util/BinaryTree;->_root:[Lorg/apache/poi/util/BinaryTree$Node;

    aput-object v3, v2, v0

    goto :goto_3

    .line 944
    :cond_7
    invoke-static {p1, v0}, Lorg/apache/poi/util/BinaryTree;->isBlack(Lorg/apache/poi/util/BinaryTree$Node;I)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 946
    invoke-direct {p0, p1, v0}, Lorg/apache/poi/util/BinaryTree;->doRedBlackDeleteFixup(Lorg/apache/poi/util/BinaryTree$Node;I)V

    .line 948
    :cond_8
    invoke-virtual {p1, v0}, Lorg/apache/poi/util/BinaryTree$Node;->getParent(I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 951
    invoke-virtual {p1, v0}, Lorg/apache/poi/util/BinaryTree$Node;->getParent(I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v2

    .line 952
    invoke-virtual {v2, v0}, Lorg/apache/poi/util/BinaryTree$Node;->getLeft(I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v2

    .line 950
    if-ne p1, v2, :cond_9

    .line 954
    invoke-virtual {p1, v0}, Lorg/apache/poi/util/BinaryTree$Node;->getParent(I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v2

    invoke-virtual {v2, v3, v0}, Lorg/apache/poi/util/BinaryTree$Node;->setLeft(Lorg/apache/poi/util/BinaryTree$Node;I)V

    .line 962
    :goto_4
    invoke-virtual {p1, v3, v0}, Lorg/apache/poi/util/BinaryTree$Node;->setParent(Lorg/apache/poi/util/BinaryTree$Node;I)V

    goto :goto_3

    .line 959
    :cond_9
    invoke-virtual {p1, v0}, Lorg/apache/poi/util/BinaryTree$Node;->getParent(I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v2

    invoke-virtual {v2, v3, v0}, Lorg/apache/poi/util/BinaryTree$Node;->setRight(Lorg/apache/poi/util/BinaryTree$Node;I)V

    goto :goto_4
.end method

.method public entrySet()Ljava/util/Set;
    .locals 3

    .prologue
    .line 1662
    iget-object v0, p0, Lorg/apache/poi/util/BinaryTree;->_entry_set:[Ljava/util/Set;

    sget v1, Lorg/apache/poi/util/BinaryTree;->_KEY:I

    aget-object v0, v0, v1

    if-nez v0, :cond_0

    .line 1664
    iget-object v0, p0, Lorg/apache/poi/util/BinaryTree;->_entry_set:[Ljava/util/Set;

    sget v1, Lorg/apache/poi/util/BinaryTree;->_KEY:I

    new-instance v2, Lorg/apache/poi/util/BinaryTree$6;

    invoke-direct {v2, p0}, Lorg/apache/poi/util/BinaryTree$6;-><init>(Lorg/apache/poi/util/BinaryTree;)V

    aput-object v2, v0, v1

    .line 1722
    :cond_0
    iget-object v0, p0, Lorg/apache/poi/util/BinaryTree;->_entry_set:[Ljava/util/Set;

    sget v1, Lorg/apache/poi/util/BinaryTree;->_KEY:I

    aget-object v0, v0, v1

    return-object v0
.end method

.method public entrySetByValue()Ljava/util/Set;
    .locals 3

    .prologue
    .line 193
    iget-object v0, p0, Lorg/apache/poi/util/BinaryTree;->_entry_set:[Ljava/util/Set;

    sget v1, Lorg/apache/poi/util/BinaryTree;->_VALUE:I

    aget-object v0, v0, v1

    if-nez v0, :cond_0

    .line 195
    iget-object v0, p0, Lorg/apache/poi/util/BinaryTree;->_entry_set:[Ljava/util/Set;

    sget v1, Lorg/apache/poi/util/BinaryTree;->_VALUE:I

    new-instance v2, Lorg/apache/poi/util/BinaryTree$1;

    invoke-direct {v2, p0}, Lorg/apache/poi/util/BinaryTree$1;-><init>(Lorg/apache/poi/util/BinaryTree;)V

    aput-object v2, v0, v1

    .line 252
    :cond_0
    iget-object v0, p0, Lorg/apache/poi/util/BinaryTree;->_entry_set:[Ljava/util/Set;

    sget v1, Lorg/apache/poi/util/BinaryTree;->_VALUE:I

    aget-object v0, v0, v1

    return-object v0
.end method

.method public get(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "key"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/ClassCastException;,
            Ljava/lang/NullPointerException;
        }
    .end annotation

    .prologue
    .line 1402
    check-cast p1, Ljava/lang/Comparable;

    .end local p1    # "key":Ljava/lang/Object;
    sget v0, Lorg/apache/poi/util/BinaryTree;->_KEY:I

    invoke-direct {p0, p1, v0}, Lorg/apache/poi/util/BinaryTree;->doGet(Ljava/lang/Comparable;I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getKeyForValue(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "value"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/ClassCastException;,
            Ljava/lang/NullPointerException;
        }
    .end annotation

    .prologue
    .line 156
    check-cast p1, Ljava/lang/Comparable;

    .end local p1    # "value":Ljava/lang/Object;
    sget v0, Lorg/apache/poi/util/BinaryTree;->_VALUE:I

    invoke-direct {p0, p1, v0}, Lorg/apache/poi/util/BinaryTree;->doGet(Ljava/lang/Comparable;I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public keySet()Ljava/util/Set;
    .locals 3

    .prologue
    .line 1533
    iget-object v0, p0, Lorg/apache/poi/util/BinaryTree;->_key_set:[Ljava/util/Set;

    sget v1, Lorg/apache/poi/util/BinaryTree;->_KEY:I

    aget-object v0, v0, v1

    if-nez v0, :cond_0

    .line 1535
    iget-object v0, p0, Lorg/apache/poi/util/BinaryTree;->_key_set:[Ljava/util/Set;

    sget v1, Lorg/apache/poi/util/BinaryTree;->_KEY:I

    new-instance v2, Lorg/apache/poi/util/BinaryTree$4;

    invoke-direct {v2, p0}, Lorg/apache/poi/util/BinaryTree$4;-><init>(Lorg/apache/poi/util/BinaryTree;)V

    aput-object v2, v0, v1

    .line 1572
    :cond_0
    iget-object v0, p0, Lorg/apache/poi/util/BinaryTree;->_key_set:[Ljava/util/Set;

    sget v1, Lorg/apache/poi/util/BinaryTree;->_KEY:I

    aget-object v0, v0, v1

    return-object v0
.end method

.method public keySetByValue()Ljava/util/Set;
    .locals 3

    .prologue
    .line 276
    iget-object v0, p0, Lorg/apache/poi/util/BinaryTree;->_key_set:[Ljava/util/Set;

    sget v1, Lorg/apache/poi/util/BinaryTree;->_VALUE:I

    aget-object v0, v0, v1

    if-nez v0, :cond_0

    .line 278
    iget-object v0, p0, Lorg/apache/poi/util/BinaryTree;->_key_set:[Ljava/util/Set;

    sget v1, Lorg/apache/poi/util/BinaryTree;->_VALUE:I

    new-instance v2, Lorg/apache/poi/util/BinaryTree$2;

    invoke-direct {v2, p0}, Lorg/apache/poi/util/BinaryTree$2;-><init>(Lorg/apache/poi/util/BinaryTree;)V

    aput-object v2, v0, v1

    .line 315
    :cond_0
    iget-object v0, p0, Lorg/apache/poi/util/BinaryTree;->_key_set:[Ljava/util/Set;

    sget v1, Lorg/apache/poi/util/BinaryTree;->_VALUE:I

    aget-object v0, v0, v1

    return-object v0
.end method

.method public lookup(Ljava/lang/Comparable;I)Lorg/apache/poi/util/BinaryTree$Node;
    .locals 4
    .param p1, "data"    # Ljava/lang/Comparable;
    .param p2, "index"    # I

    .prologue
    .line 465
    const/4 v2, 0x0

    .line 466
    .local v2, "rval":Lorg/apache/poi/util/BinaryTree$Node;
    iget-object v3, p0, Lorg/apache/poi/util/BinaryTree;->_root:[Lorg/apache/poi/util/BinaryTree$Node;

    aget-object v1, v3, p2

    .line 468
    .local v1, "node":Lorg/apache/poi/util/BinaryTree$Node;
    :goto_0
    if-nez v1, :cond_0

    .line 480
    :goto_1
    return-object v2

    .line 470
    :cond_0
    invoke-virtual {v1, p2}, Lorg/apache/poi/util/BinaryTree$Node;->getData(I)Ljava/lang/Comparable;

    move-result-object v3

    invoke-static {p1, v3}, Lorg/apache/poi/util/BinaryTree;->compare(Ljava/lang/Comparable;Ljava/lang/Comparable;)I

    move-result v0

    .line 472
    .local v0, "cmp":I
    if-nez v0, :cond_1

    .line 474
    move-object v2, v1

    .line 475
    goto :goto_1

    .line 477
    :cond_1
    if-gez v0, :cond_2

    invoke-virtual {v1, p2}, Lorg/apache/poi/util/BinaryTree$Node;->getLeft(I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v1

    :goto_2
    goto :goto_0

    .line 478
    :cond_2
    invoke-virtual {v1, p2}, Lorg/apache/poi/util/BinaryTree$Node;->getRight(I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v1

    goto :goto_2
.end method

.method public put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 7
    .param p1, "key"    # Ljava/lang/Object;
    .param p2, "value"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/ClassCastException;,
            Ljava/lang/NullPointerException;,
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 1429
    invoke-static {p1, p2}, Lorg/apache/poi/util/BinaryTree;->checkKeyAndValue(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 1430
    iget-object v4, p0, Lorg/apache/poi/util/BinaryTree;->_root:[Lorg/apache/poi/util/BinaryTree$Node;

    sget v5, Lorg/apache/poi/util/BinaryTree;->_KEY:I

    aget-object v2, v4, v5

    .line 1432
    .local v2, "node":Lorg/apache/poi/util/BinaryTree$Node;
    if-nez v2, :cond_1

    .line 1434
    new-instance v3, Lorg/apache/poi/util/BinaryTree$Node;

    check-cast p1, Ljava/lang/Comparable;

    .end local p1    # "key":Ljava/lang/Object;
    check-cast p2, Ljava/lang/Comparable;

    .end local p2    # "value":Ljava/lang/Object;
    invoke-direct {v3, p1, p2}, Lorg/apache/poi/util/BinaryTree$Node;-><init>(Ljava/lang/Comparable;Ljava/lang/Comparable;)V

    .line 1436
    .local v3, "root":Lorg/apache/poi/util/BinaryTree$Node;
    iget-object v4, p0, Lorg/apache/poi/util/BinaryTree;->_root:[Lorg/apache/poi/util/BinaryTree$Node;

    sget v5, Lorg/apache/poi/util/BinaryTree;->_KEY:I

    aput-object v3, v4, v5

    .line 1437
    iget-object v4, p0, Lorg/apache/poi/util/BinaryTree;->_root:[Lorg/apache/poi/util/BinaryTree$Node;

    sget v5, Lorg/apache/poi/util/BinaryTree;->_VALUE:I

    aput-object v3, v4, v5

    .line 1438
    invoke-direct {p0}, Lorg/apache/poi/util/BinaryTree;->grow()V

    .line 1492
    .end local v3    # "root":Lorg/apache/poi/util/BinaryTree$Node;
    :goto_0
    const/4 v4, 0x0

    return-object v4

    .line 1452
    .local v0, "cmp":I
    .restart local p1    # "key":Ljava/lang/Object;
    .restart local p2    # "value":Ljava/lang/Object;
    :cond_0
    if-gez v0, :cond_3

    .line 1454
    sget v4, Lorg/apache/poi/util/BinaryTree;->_KEY:I

    invoke-virtual {v2, v4}, Lorg/apache/poi/util/BinaryTree$Node;->getLeft(I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v4

    if-eqz v4, :cond_2

    .line 1456
    sget v4, Lorg/apache/poi/util/BinaryTree;->_KEY:I

    invoke-virtual {v2, v4}, Lorg/apache/poi/util/BinaryTree$Node;->getLeft(I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v2

    .end local v0    # "cmp":I
    :cond_1
    :goto_1
    move-object v4, p1

    .line 1444
    check-cast v4, Ljava/lang/Comparable;

    sget v5, Lorg/apache/poi/util/BinaryTree;->_KEY:I

    invoke-virtual {v2, v5}, Lorg/apache/poi/util/BinaryTree$Node;->getData(I)Ljava/lang/Comparable;

    move-result-object v5

    invoke-static {v4, v5}, Lorg/apache/poi/util/BinaryTree;->compare(Ljava/lang/Comparable;Ljava/lang/Comparable;)I

    move-result v0

    .line 1446
    .restart local v0    # "cmp":I
    if-nez v0, :cond_0

    .line 1448
    new-instance v4, Ljava/lang/IllegalArgumentException;

    .line 1449
    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "Cannot store a duplicate key (\""

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 1450
    const-string/jumbo v6, "\") in this Map"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 1449
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 1448
    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 1460
    :cond_2
    new-instance v1, Lorg/apache/poi/util/BinaryTree$Node;

    check-cast p1, Ljava/lang/Comparable;

    .line 1461
    .end local p1    # "key":Ljava/lang/Object;
    check-cast p2, Ljava/lang/Comparable;

    .line 1460
    .end local p2    # "value":Ljava/lang/Object;
    invoke-direct {v1, p1, p2}, Lorg/apache/poi/util/BinaryTree$Node;-><init>(Ljava/lang/Comparable;Ljava/lang/Comparable;)V

    .line 1463
    .local v1, "newNode":Lorg/apache/poi/util/BinaryTree$Node;
    invoke-direct {p0, v1}, Lorg/apache/poi/util/BinaryTree;->insertValue(Lorg/apache/poi/util/BinaryTree$Node;)V

    .line 1464
    sget v4, Lorg/apache/poi/util/BinaryTree;->_KEY:I

    invoke-virtual {v2, v1, v4}, Lorg/apache/poi/util/BinaryTree$Node;->setLeft(Lorg/apache/poi/util/BinaryTree$Node;I)V

    .line 1465
    sget v4, Lorg/apache/poi/util/BinaryTree;->_KEY:I

    invoke-virtual {v1, v2, v4}, Lorg/apache/poi/util/BinaryTree$Node;->setParent(Lorg/apache/poi/util/BinaryTree$Node;I)V

    .line 1466
    sget v4, Lorg/apache/poi/util/BinaryTree;->_KEY:I

    invoke-direct {p0, v1, v4}, Lorg/apache/poi/util/BinaryTree;->doRedBlackInsert(Lorg/apache/poi/util/BinaryTree$Node;I)V

    .line 1467
    invoke-direct {p0}, Lorg/apache/poi/util/BinaryTree;->grow()V

    goto :goto_0

    .line 1473
    .end local v1    # "newNode":Lorg/apache/poi/util/BinaryTree$Node;
    .restart local p1    # "key":Ljava/lang/Object;
    .restart local p2    # "value":Ljava/lang/Object;
    :cond_3
    sget v4, Lorg/apache/poi/util/BinaryTree;->_KEY:I

    invoke-virtual {v2, v4}, Lorg/apache/poi/util/BinaryTree$Node;->getRight(I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v4

    if-eqz v4, :cond_4

    .line 1475
    sget v4, Lorg/apache/poi/util/BinaryTree;->_KEY:I

    invoke-virtual {v2, v4}, Lorg/apache/poi/util/BinaryTree$Node;->getRight(I)Lorg/apache/poi/util/BinaryTree$Node;

    move-result-object v2

    .line 1476
    goto :goto_1

    .line 1479
    :cond_4
    new-instance v1, Lorg/apache/poi/util/BinaryTree$Node;

    check-cast p1, Ljava/lang/Comparable;

    .line 1480
    .end local p1    # "key":Ljava/lang/Object;
    check-cast p2, Ljava/lang/Comparable;

    .line 1479
    .end local p2    # "value":Ljava/lang/Object;
    invoke-direct {v1, p1, p2}, Lorg/apache/poi/util/BinaryTree$Node;-><init>(Ljava/lang/Comparable;Ljava/lang/Comparable;)V

    .line 1482
    .restart local v1    # "newNode":Lorg/apache/poi/util/BinaryTree$Node;
    invoke-direct {p0, v1}, Lorg/apache/poi/util/BinaryTree;->insertValue(Lorg/apache/poi/util/BinaryTree$Node;)V

    .line 1483
    sget v4, Lorg/apache/poi/util/BinaryTree;->_KEY:I

    invoke-virtual {v2, v1, v4}, Lorg/apache/poi/util/BinaryTree$Node;->setRight(Lorg/apache/poi/util/BinaryTree$Node;I)V

    .line 1484
    sget v4, Lorg/apache/poi/util/BinaryTree;->_KEY:I

    invoke-virtual {v1, v2, v4}, Lorg/apache/poi/util/BinaryTree$Node;->setParent(Lorg/apache/poi/util/BinaryTree$Node;I)V

    .line 1485
    sget v4, Lorg/apache/poi/util/BinaryTree;->_KEY:I

    invoke-direct {p0, v1, v4}, Lorg/apache/poi/util/BinaryTree;->doRedBlackInsert(Lorg/apache/poi/util/BinaryTree$Node;I)V

    .line 1486
    invoke-direct {p0}, Lorg/apache/poi/util/BinaryTree;->grow()V

    goto/16 :goto_0
.end method

.method public remove(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "key"    # Ljava/lang/Object;

    .prologue
    .line 1505
    check-cast p1, Ljava/lang/Comparable;

    .end local p1    # "key":Ljava/lang/Object;
    sget v0, Lorg/apache/poi/util/BinaryTree;->_KEY:I

    invoke-direct {p0, p1, v0}, Lorg/apache/poi/util/BinaryTree;->doRemove(Ljava/lang/Comparable;I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public removeValue(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "value"    # Ljava/lang/Object;

    .prologue
    .line 169
    check-cast p1, Ljava/lang/Comparable;

    .end local p1    # "value":Ljava/lang/Object;
    sget v0, Lorg/apache/poi/util/BinaryTree;->_VALUE:I

    invoke-direct {p0, p1, v0}, Lorg/apache/poi/util/BinaryTree;->doRemove(Ljava/lang/Comparable;I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 1348
    iget v0, p0, Lorg/apache/poi/util/BinaryTree;->_size:I

    return v0
.end method

.method public values()Ljava/util/Collection;
    .locals 3

    .prologue
    .line 1590
    iget-object v0, p0, Lorg/apache/poi/util/BinaryTree;->_value_collection:[Ljava/util/Collection;

    sget v1, Lorg/apache/poi/util/BinaryTree;->_KEY:I

    aget-object v0, v0, v1

    if-nez v0, :cond_0

    .line 1592
    iget-object v0, p0, Lorg/apache/poi/util/BinaryTree;->_value_collection:[Ljava/util/Collection;

    sget v1, Lorg/apache/poi/util/BinaryTree;->_KEY:I

    new-instance v2, Lorg/apache/poi/util/BinaryTree$5;

    invoke-direct {v2, p0}, Lorg/apache/poi/util/BinaryTree$5;-><init>(Lorg/apache/poi/util/BinaryTree;)V

    aput-object v2, v0, v1

    .line 1644
    :cond_0
    iget-object v0, p0, Lorg/apache/poi/util/BinaryTree;->_value_collection:[Ljava/util/Collection;

    sget v1, Lorg/apache/poi/util/BinaryTree;->_KEY:I

    aget-object v0, v0, v1

    return-object v0
.end method

.method public valuesByValue()Ljava/util/Collection;
    .locals 3

    .prologue
    .line 339
    iget-object v0, p0, Lorg/apache/poi/util/BinaryTree;->_value_collection:[Ljava/util/Collection;

    sget v1, Lorg/apache/poi/util/BinaryTree;->_VALUE:I

    aget-object v0, v0, v1

    if-nez v0, :cond_0

    .line 341
    iget-object v0, p0, Lorg/apache/poi/util/BinaryTree;->_value_collection:[Ljava/util/Collection;

    sget v1, Lorg/apache/poi/util/BinaryTree;->_VALUE:I

    new-instance v2, Lorg/apache/poi/util/BinaryTree$3;

    invoke-direct {v2, p0}, Lorg/apache/poi/util/BinaryTree$3;-><init>(Lorg/apache/poi/util/BinaryTree;)V

    aput-object v2, v0, v1

    .line 393
    :cond_0
    iget-object v0, p0, Lorg/apache/poi/util/BinaryTree;->_value_collection:[Ljava/util/Collection;

    sget v1, Lorg/apache/poi/util/BinaryTree;->_VALUE:I

    aget-object v0, v0, v1

    return-object v0
.end method
