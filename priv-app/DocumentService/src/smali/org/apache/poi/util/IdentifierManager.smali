.class public Lorg/apache/poi/util/IdentifierManager;
.super Ljava/lang/Object;
.source "IdentifierManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/poi/util/IdentifierManager$Segment;
    }
.end annotation


# static fields
.field public static final MAX_ID:J = 0x7ffffffffffffffeL

.field public static final MIN_ID:J


# instance fields
.field private final lowerbound:J

.field private segments:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lorg/apache/poi/util/IdentifierManager$Segment;",
            ">;"
        }
    .end annotation
.end field

.field private final upperbound:J


# direct methods
.method public constructor <init>(JJ)V
    .locals 9
    .param p1, "lowerbound"    # J
    .param p3, "upperbound"    # J

    .prologue
    const-wide v6, 0x7ffffffffffffffeL

    const-wide/16 v4, 0x0

    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    cmp-long v1, p1, p3

    if-lez v1, :cond_0

    .line 57
    const-string/jumbo v0, "lowerbound must not be greater than upperbound"

    .line 58
    .local v0, "message":Ljava/lang/String;
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 60
    .end local v0    # "message":Ljava/lang/String;
    :cond_0
    cmp-long v1, p1, v4

    if-gez v1, :cond_1

    .line 61
    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "lowerbound must be greater than or equal to "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 62
    .restart local v0    # "message":Ljava/lang/String;
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 64
    .end local v0    # "message":Ljava/lang/String;
    :cond_1
    cmp-long v1, p3, v6

    if-lez v1, :cond_2

    .line 69
    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "upperbound must be less thean or equal "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 70
    .restart local v0    # "message":Ljava/lang/String;
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 72
    .end local v0    # "message":Ljava/lang/String;
    :cond_2
    iput-wide p1, p0, Lorg/apache/poi/util/IdentifierManager;->lowerbound:J

    .line 73
    iput-wide p3, p0, Lorg/apache/poi/util/IdentifierManager;->upperbound:J

    .line 74
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    iput-object v1, p0, Lorg/apache/poi/util/IdentifierManager;->segments:Ljava/util/LinkedList;

    .line 75
    iget-object v1, p0, Lorg/apache/poi/util/IdentifierManager;->segments:Ljava/util/LinkedList;

    new-instance v2, Lorg/apache/poi/util/IdentifierManager$Segment;

    invoke-direct {v2, p1, p2, p3, p4}, Lorg/apache/poi/util/IdentifierManager$Segment;-><init>(JJ)V

    invoke-virtual {v1, v2}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 76
    return-void
.end method

.method private verifyIdentifiersLeft()V
    .locals 2

    .prologue
    .line 244
    iget-object v0, p0, Lorg/apache/poi/util/IdentifierManager;->segments:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 245
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "No identifiers left"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 247
    :cond_0
    return-void
.end method


# virtual methods
.method public getRemainingIdentifiers()J
    .locals 8

    .prologue
    .line 232
    const-wide/16 v0, 0x0

    .line 233
    .local v0, "result":J
    iget-object v3, p0, Lorg/apache/poi/util/IdentifierManager;->segments:Ljava/util/LinkedList;

    invoke-virtual {v3}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_0

    .line 237
    return-wide v0

    .line 233
    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/poi/util/IdentifierManager$Segment;

    .line 234
    .local v2, "segment":Lorg/apache/poi/util/IdentifierManager$Segment;
    iget-wide v4, v2, Lorg/apache/poi/util/IdentifierManager$Segment;->start:J

    sub-long/2addr v0, v4

    .line 235
    iget-wide v4, v2, Lorg/apache/poi/util/IdentifierManager$Segment;->end:J

    add-long/2addr v4, v0

    const-wide/16 v6, 0x1

    add-long v0, v4, v6

    goto :goto_0
.end method

.method public release(J)Z
    .locals 21
    .param p1, "id"    # J

    .prologue
    .line 163
    move-object/from16 v0, p0

    iget-wide v14, v0, Lorg/apache/poi/util/IdentifierManager;->lowerbound:J

    cmp-long v13, p1, v14

    if-ltz v13, :cond_0

    move-object/from16 v0, p0

    iget-wide v14, v0, Lorg/apache/poi/util/IdentifierManager;->upperbound:J

    cmp-long v13, p1, v14

    if-lez v13, :cond_1

    .line 164
    :cond_0
    new-instance v13, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v14, "Value for parameter \'id\' was out of bounds"

    invoke-direct {v13, v14}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v13

    .line 167
    :cond_1
    move-object/from16 v0, p0

    iget-wide v14, v0, Lorg/apache/poi/util/IdentifierManager;->upperbound:J

    cmp-long v13, p1, v14

    if-nez v13, :cond_4

    .line 168
    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/poi/util/IdentifierManager;->segments:Ljava/util/LinkedList;

    invoke-virtual {v13}, Ljava/util/LinkedList;->getLast()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lorg/apache/poi/util/IdentifierManager$Segment;

    .line 169
    .local v8, "lastSegment":Lorg/apache/poi/util/IdentifierManager$Segment;
    iget-wide v14, v8, Lorg/apache/poi/util/IdentifierManager$Segment;->end:J

    move-object/from16 v0, p0

    iget-wide v0, v0, Lorg/apache/poi/util/IdentifierManager;->upperbound:J

    move-wide/from16 v16, v0

    const-wide/16 v18, 0x1

    sub-long v16, v16, v18

    cmp-long v13, v14, v16

    if-nez v13, :cond_2

    .line 170
    move-object/from16 v0, p0

    iget-wide v14, v0, Lorg/apache/poi/util/IdentifierManager;->upperbound:J

    iput-wide v14, v8, Lorg/apache/poi/util/IdentifierManager$Segment;->end:J

    .line 171
    const/4 v13, 0x1

    .line 228
    .end local v8    # "lastSegment":Lorg/apache/poi/util/IdentifierManager$Segment;
    :goto_0
    return v13

    .line 172
    .restart local v8    # "lastSegment":Lorg/apache/poi/util/IdentifierManager$Segment;
    :cond_2
    iget-wide v14, v8, Lorg/apache/poi/util/IdentifierManager$Segment;->end:J

    move-object/from16 v0, p0

    iget-wide v0, v0, Lorg/apache/poi/util/IdentifierManager;->upperbound:J

    move-wide/from16 v16, v0

    cmp-long v13, v14, v16

    if-nez v13, :cond_3

    .line 173
    const/4 v13, 0x0

    goto :goto_0

    .line 175
    :cond_3
    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/poi/util/IdentifierManager;->segments:Ljava/util/LinkedList;

    new-instance v14, Lorg/apache/poi/util/IdentifierManager$Segment;

    move-object/from16 v0, p0

    iget-wide v0, v0, Lorg/apache/poi/util/IdentifierManager;->upperbound:J

    move-wide/from16 v16, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lorg/apache/poi/util/IdentifierManager;->upperbound:J

    move-wide/from16 v18, v0

    move-wide/from16 v0, v16

    move-wide/from16 v2, v18

    invoke-direct {v14, v0, v1, v2, v3}, Lorg/apache/poi/util/IdentifierManager$Segment;-><init>(JJ)V

    invoke-virtual {v13, v14}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 176
    const/4 v13, 0x1

    goto :goto_0

    .line 180
    .end local v8    # "lastSegment":Lorg/apache/poi/util/IdentifierManager$Segment;
    :cond_4
    move-object/from16 v0, p0

    iget-wide v14, v0, Lorg/apache/poi/util/IdentifierManager;->lowerbound:J

    cmp-long v13, p1, v14

    if-nez v13, :cond_7

    .line 181
    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/poi/util/IdentifierManager;->segments:Ljava/util/LinkedList;

    invoke-virtual {v13}, Ljava/util/LinkedList;->getFirst()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/poi/util/IdentifierManager$Segment;

    .line 182
    .local v4, "firstSegment":Lorg/apache/poi/util/IdentifierManager$Segment;
    iget-wide v14, v4, Lorg/apache/poi/util/IdentifierManager$Segment;->start:J

    move-object/from16 v0, p0

    iget-wide v0, v0, Lorg/apache/poi/util/IdentifierManager;->lowerbound:J

    move-wide/from16 v16, v0

    const-wide/16 v18, 0x1

    add-long v16, v16, v18

    cmp-long v13, v14, v16

    if-nez v13, :cond_5

    .line 183
    move-object/from16 v0, p0

    iget-wide v14, v0, Lorg/apache/poi/util/IdentifierManager;->lowerbound:J

    iput-wide v14, v4, Lorg/apache/poi/util/IdentifierManager$Segment;->start:J

    .line 184
    const/4 v13, 0x1

    goto :goto_0

    .line 185
    :cond_5
    iget-wide v14, v4, Lorg/apache/poi/util/IdentifierManager$Segment;->start:J

    move-object/from16 v0, p0

    iget-wide v0, v0, Lorg/apache/poi/util/IdentifierManager;->lowerbound:J

    move-wide/from16 v16, v0

    cmp-long v13, v14, v16

    if-nez v13, :cond_6

    .line 186
    const/4 v13, 0x0

    goto :goto_0

    .line 188
    :cond_6
    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/poi/util/IdentifierManager;->segments:Ljava/util/LinkedList;

    new-instance v14, Lorg/apache/poi/util/IdentifierManager$Segment;

    move-object/from16 v0, p0

    iget-wide v0, v0, Lorg/apache/poi/util/IdentifierManager;->lowerbound:J

    move-wide/from16 v16, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lorg/apache/poi/util/IdentifierManager;->lowerbound:J

    move-wide/from16 v18, v0

    move-wide/from16 v0, v16

    move-wide/from16 v2, v18

    invoke-direct {v14, v0, v1, v2, v3}, Lorg/apache/poi/util/IdentifierManager$Segment;-><init>(JJ)V

    invoke-virtual {v13, v14}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    .line 189
    const/4 v13, 0x1

    goto/16 :goto_0

    .line 193
    .end local v4    # "firstSegment":Lorg/apache/poi/util/IdentifierManager$Segment;
    :cond_7
    const-wide/16 v14, 0x1

    add-long v6, p1, v14

    .line 194
    .local v6, "higher":J
    const-wide/16 v14, 0x1

    sub-long v10, p1, v14

    .line 195
    .local v10, "lower":J
    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/poi/util/IdentifierManager;->segments:Ljava/util/LinkedList;

    invoke-virtual {v13}, Ljava/util/LinkedList;->listIterator()Ljava/util/ListIterator;

    move-result-object v5

    .line 197
    .local v5, "iter":Ljava/util/ListIterator;, "Ljava/util/ListIterator<Lorg/apache/poi/util/IdentifierManager$Segment;>;"
    :cond_8
    invoke-interface {v5}, Ljava/util/ListIterator;->hasNext()Z

    move-result v13

    if-nez v13, :cond_a

    .line 228
    :cond_9
    const/4 v13, 0x0

    goto/16 :goto_0

    .line 198
    :cond_a
    invoke-interface {v5}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lorg/apache/poi/util/IdentifierManager$Segment;

    .line 199
    .local v12, "segment":Lorg/apache/poi/util/IdentifierManager$Segment;
    iget-wide v14, v12, Lorg/apache/poi/util/IdentifierManager$Segment;->end:J

    cmp-long v13, v14, v10

    if-ltz v13, :cond_8

    .line 202
    iget-wide v14, v12, Lorg/apache/poi/util/IdentifierManager$Segment;->start:J

    cmp-long v13, v14, v6

    if-lez v13, :cond_b

    .line 203
    invoke-interface {v5}, Ljava/util/ListIterator;->previous()Ljava/lang/Object;

    .line 204
    new-instance v13, Lorg/apache/poi/util/IdentifierManager$Segment;

    move-wide/from16 v0, p1

    move-wide/from16 v2, p1

    invoke-direct {v13, v0, v1, v2, v3}, Lorg/apache/poi/util/IdentifierManager$Segment;-><init>(JJ)V

    invoke-interface {v5, v13}, Ljava/util/ListIterator;->add(Ljava/lang/Object;)V

    .line 205
    const/4 v13, 0x1

    goto/16 :goto_0

    .line 207
    :cond_b
    iget-wide v14, v12, Lorg/apache/poi/util/IdentifierManager$Segment;->start:J

    cmp-long v13, v14, v6

    if-nez v13, :cond_c

    .line 208
    move-wide/from16 v0, p1

    iput-wide v0, v12, Lorg/apache/poi/util/IdentifierManager$Segment;->start:J

    .line 209
    const/4 v13, 0x1

    goto/16 :goto_0

    .line 211
    :cond_c
    iget-wide v14, v12, Lorg/apache/poi/util/IdentifierManager$Segment;->end:J

    cmp-long v13, v14, v10

    if-nez v13, :cond_9

    .line 212
    move-wide/from16 v0, p1

    iput-wide v0, v12, Lorg/apache/poi/util/IdentifierManager$Segment;->end:J

    .line 214
    invoke-interface {v5}, Ljava/util/ListIterator;->hasNext()Z

    move-result v13

    if-eqz v13, :cond_d

    .line 215
    invoke-interface {v5}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lorg/apache/poi/util/IdentifierManager$Segment;

    .line 216
    .local v9, "next":Lorg/apache/poi/util/IdentifierManager$Segment;
    iget-wide v14, v9, Lorg/apache/poi/util/IdentifierManager$Segment;->start:J

    iget-wide v0, v12, Lorg/apache/poi/util/IdentifierManager$Segment;->end:J

    move-wide/from16 v16, v0

    const-wide/16 v18, 0x1

    add-long v16, v16, v18

    cmp-long v13, v14, v16

    if-nez v13, :cond_d

    .line 217
    iget-wide v14, v9, Lorg/apache/poi/util/IdentifierManager$Segment;->end:J

    iput-wide v14, v12, Lorg/apache/poi/util/IdentifierManager$Segment;->end:J

    .line 218
    invoke-interface {v5}, Ljava/util/ListIterator;->remove()V

    .line 221
    .end local v9    # "next":Lorg/apache/poi/util/IdentifierManager$Segment;
    :cond_d
    const/4 v13, 0x1

    goto/16 :goto_0
.end method

.method public reserve(J)J
    .locals 13
    .param p1, "id"    # J

    .prologue
    const-wide/16 v10, 0x1

    .line 79
    iget-wide v4, p0, Lorg/apache/poi/util/IdentifierManager;->lowerbound:J

    cmp-long v4, p1, v4

    if-ltz v4, :cond_0

    iget-wide v4, p0, Lorg/apache/poi/util/IdentifierManager;->upperbound:J

    cmp-long v4, p1, v4

    if-lez v4, :cond_1

    .line 80
    :cond_0
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v5, "Value for parameter \'id\' was out of bounds"

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 82
    :cond_1
    invoke-direct {p0}, Lorg/apache/poi/util/IdentifierManager;->verifyIdentifiersLeft()V

    .line 84
    iget-wide v4, p0, Lorg/apache/poi/util/IdentifierManager;->upperbound:J

    cmp-long v4, p1, v4

    if-nez v4, :cond_4

    .line 85
    iget-object v4, p0, Lorg/apache/poi/util/IdentifierManager;->segments:Ljava/util/LinkedList;

    invoke-virtual {v4}, Ljava/util/LinkedList;->getLast()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/poi/util/IdentifierManager$Segment;

    .line 86
    .local v2, "lastSegment":Lorg/apache/poi/util/IdentifierManager$Segment;
    iget-wide v4, v2, Lorg/apache/poi/util/IdentifierManager$Segment;->end:J

    iget-wide v6, p0, Lorg/apache/poi/util/IdentifierManager;->upperbound:J

    cmp-long v4, v4, v6

    if-nez v4, :cond_3

    .line 87
    iget-wide v4, p0, Lorg/apache/poi/util/IdentifierManager;->upperbound:J

    sub-long/2addr v4, v10

    iput-wide v4, v2, Lorg/apache/poi/util/IdentifierManager$Segment;->end:J

    .line 88
    iget-wide v4, v2, Lorg/apache/poi/util/IdentifierManager$Segment;->start:J

    iget-wide v6, v2, Lorg/apache/poi/util/IdentifierManager$Segment;->end:J

    cmp-long v4, v4, v6

    if-lez v4, :cond_2

    .line 89
    iget-object v4, p0, Lorg/apache/poi/util/IdentifierManager;->segments:Ljava/util/LinkedList;

    invoke-virtual {v4}, Ljava/util/LinkedList;->removeLast()Ljava/lang/Object;

    .line 137
    .end local v2    # "lastSegment":Lorg/apache/poi/util/IdentifierManager$Segment;
    .end local p1    # "id":J
    :cond_2
    :goto_0
    return-wide p1

    .line 93
    .restart local v2    # "lastSegment":Lorg/apache/poi/util/IdentifierManager$Segment;
    .restart local p1    # "id":J
    :cond_3
    invoke-virtual {p0}, Lorg/apache/poi/util/IdentifierManager;->reserveNew()J

    move-result-wide p1

    goto :goto_0

    .line 96
    .end local v2    # "lastSegment":Lorg/apache/poi/util/IdentifierManager$Segment;
    :cond_4
    iget-wide v4, p0, Lorg/apache/poi/util/IdentifierManager;->lowerbound:J

    cmp-long v4, p1, v4

    if-nez v4, :cond_6

    .line 97
    iget-object v4, p0, Lorg/apache/poi/util/IdentifierManager;->segments:Ljava/util/LinkedList;

    invoke-virtual {v4}, Ljava/util/LinkedList;->getFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/util/IdentifierManager$Segment;

    .line 98
    .local v0, "firstSegment":Lorg/apache/poi/util/IdentifierManager$Segment;
    iget-wide v4, v0, Lorg/apache/poi/util/IdentifierManager$Segment;->start:J

    iget-wide v6, p0, Lorg/apache/poi/util/IdentifierManager;->lowerbound:J

    cmp-long v4, v4, v6

    if-nez v4, :cond_5

    .line 99
    iget-wide v4, p0, Lorg/apache/poi/util/IdentifierManager;->lowerbound:J

    add-long/2addr v4, v10

    iput-wide v4, v0, Lorg/apache/poi/util/IdentifierManager$Segment;->start:J

    .line 100
    iget-wide v4, v0, Lorg/apache/poi/util/IdentifierManager$Segment;->end:J

    iget-wide v6, v0, Lorg/apache/poi/util/IdentifierManager$Segment;->start:J

    cmp-long v4, v4, v6

    if-gez v4, :cond_2

    .line 101
    iget-object v4, p0, Lorg/apache/poi/util/IdentifierManager;->segments:Ljava/util/LinkedList;

    invoke-virtual {v4}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    goto :goto_0

    .line 105
    :cond_5
    invoke-virtual {p0}, Lorg/apache/poi/util/IdentifierManager;->reserveNew()J

    move-result-wide p1

    goto :goto_0

    .line 108
    .end local v0    # "firstSegment":Lorg/apache/poi/util/IdentifierManager$Segment;
    :cond_6
    iget-object v4, p0, Lorg/apache/poi/util/IdentifierManager;->segments:Ljava/util/LinkedList;

    invoke-virtual {v4}, Ljava/util/LinkedList;->listIterator()Ljava/util/ListIterator;

    move-result-object v1

    .line 109
    .local v1, "iter":Ljava/util/ListIterator;, "Ljava/util/ListIterator<Lorg/apache/poi/util/IdentifierManager$Segment;>;"
    :cond_7
    invoke-interface {v1}, Ljava/util/ListIterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_9

    .line 137
    :cond_8
    invoke-virtual {p0}, Lorg/apache/poi/util/IdentifierManager;->reserveNew()J

    move-result-wide p1

    goto :goto_0

    .line 110
    :cond_9
    invoke-interface {v1}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/poi/util/IdentifierManager$Segment;

    .line 111
    .local v3, "segment":Lorg/apache/poi/util/IdentifierManager$Segment;
    iget-wide v4, v3, Lorg/apache/poi/util/IdentifierManager$Segment;->end:J

    cmp-long v4, v4, p1

    if-ltz v4, :cond_7

    .line 114
    iget-wide v4, v3, Lorg/apache/poi/util/IdentifierManager$Segment;->start:J

    cmp-long v4, v4, p1

    if-gtz v4, :cond_8

    .line 117
    iget-wide v4, v3, Lorg/apache/poi/util/IdentifierManager$Segment;->start:J

    cmp-long v4, v4, p1

    if-nez v4, :cond_a

    .line 118
    add-long v4, p1, v10

    iput-wide v4, v3, Lorg/apache/poi/util/IdentifierManager$Segment;->start:J

    .line 119
    iget-wide v4, v3, Lorg/apache/poi/util/IdentifierManager$Segment;->end:J

    iget-wide v6, v3, Lorg/apache/poi/util/IdentifierManager$Segment;->start:J

    cmp-long v4, v4, v6

    if-gez v4, :cond_2

    .line 120
    invoke-interface {v1}, Ljava/util/ListIterator;->remove()V

    goto :goto_0

    .line 124
    :cond_a
    iget-wide v4, v3, Lorg/apache/poi/util/IdentifierManager$Segment;->end:J

    cmp-long v4, v4, p1

    if-nez v4, :cond_b

    .line 125
    sub-long v4, p1, v10

    iput-wide v4, v3, Lorg/apache/poi/util/IdentifierManager$Segment;->end:J

    .line 126
    iget-wide v4, v3, Lorg/apache/poi/util/IdentifierManager$Segment;->start:J

    iget-wide v6, v3, Lorg/apache/poi/util/IdentifierManager$Segment;->end:J

    cmp-long v4, v4, v6

    if-lez v4, :cond_2

    .line 127
    invoke-interface {v1}, Ljava/util/ListIterator;->remove()V

    goto/16 :goto_0

    .line 132
    :cond_b
    new-instance v4, Lorg/apache/poi/util/IdentifierManager$Segment;

    add-long v6, p1, v10

    iget-wide v8, v3, Lorg/apache/poi/util/IdentifierManager$Segment;->end:J

    invoke-direct {v4, v6, v7, v8, v9}, Lorg/apache/poi/util/IdentifierManager$Segment;-><init>(JJ)V

    invoke-interface {v1, v4}, Ljava/util/ListIterator;->add(Ljava/lang/Object;)V

    .line 133
    sub-long v4, p1, v10

    iput-wide v4, v3, Lorg/apache/poi/util/IdentifierManager$Segment;->end:J

    goto/16 :goto_0
.end method

.method public reserveNew()J
    .locals 8

    .prologue
    .line 145
    invoke-direct {p0}, Lorg/apache/poi/util/IdentifierManager;->verifyIdentifiersLeft()V

    .line 146
    iget-object v3, p0, Lorg/apache/poi/util/IdentifierManager;->segments:Ljava/util/LinkedList;

    invoke-virtual {v3}, Ljava/util/LinkedList;->getFirst()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/poi/util/IdentifierManager$Segment;

    .line 147
    .local v2, "segment":Lorg/apache/poi/util/IdentifierManager$Segment;
    iget-wide v0, v2, Lorg/apache/poi/util/IdentifierManager$Segment;->start:J

    .line 148
    .local v0, "result":J
    iget-wide v4, v2, Lorg/apache/poi/util/IdentifierManager$Segment;->start:J

    const-wide/16 v6, 0x1

    add-long/2addr v4, v6

    iput-wide v4, v2, Lorg/apache/poi/util/IdentifierManager$Segment;->start:J

    .line 149
    iget-wide v4, v2, Lorg/apache/poi/util/IdentifierManager$Segment;->start:J

    iget-wide v6, v2, Lorg/apache/poi/util/IdentifierManager$Segment;->end:J

    cmp-long v3, v4, v6

    if-lez v3, :cond_0

    .line 150
    iget-object v3, p0, Lorg/apache/poi/util/IdentifierManager;->segments:Ljava/util/LinkedList;

    invoke-virtual {v3}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    .line 152
    :cond_0
    return-wide v0
.end method
