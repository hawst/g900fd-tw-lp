.class public Lorg/apache/poi/util/Units;
.super Ljava/lang/Object;
.source "Units.java"


# static fields
.field public static final EMU_PER_PIXEL:I = 0x2535

.field public static final EMU_PER_POINT:I = 0x319c


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static toEMU(D)I
    .locals 2
    .param p0, "value"    # D

    .prologue
    .line 27
    const-wide v0, 0x40c8ce0000000000L    # 12700.0

    mul-double/2addr v0, p0

    invoke-static {v0, v1}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method

.method public static toPoints(J)D
    .locals 4
    .param p0, "emu"    # J

    .prologue
    .line 31
    long-to-double v0, p0

    const-wide v2, 0x40c8ce0000000000L    # 12700.0

    div-double/2addr v0, v2

    return-wide v0
.end method
