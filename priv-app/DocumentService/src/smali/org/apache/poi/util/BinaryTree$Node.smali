.class final Lorg/apache/poi/util/BinaryTree$Node;
.super Ljava/lang/Object;
.source "BinaryTree.java"

# interfaces
.implements Ljava/util/Map$Entry;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/poi/util/BinaryTree;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "Node"
.end annotation


# instance fields
.field private _black:[Z

.field private _calculated_hashcode:Z

.field private _data:[Ljava/lang/Comparable;

.field private _hashcode:I

.field private _left:[Lorg/apache/poi/util/BinaryTree$Node;

.field private _parent:[Lorg/apache/poi/util/BinaryTree$Node;

.field private _right:[Lorg/apache/poi/util/BinaryTree$Node;


# direct methods
.method constructor <init>(Ljava/lang/Comparable;Ljava/lang/Comparable;)V
    .locals 4
    .param p1, "key"    # Ljava/lang/Comparable;
    .param p2, "value"    # Ljava/lang/Comparable;

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x2

    .line 1852
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1855
    new-array v0, v2, [Ljava/lang/Comparable;

    .line 1856
    aput-object p1, v0, v3

    const/4 v1, 0x1

    aput-object p2, v0, v1

    .line 1854
    iput-object v0, p0, Lorg/apache/poi/util/BinaryTree$Node;->_data:[Ljava/lang/Comparable;

    .line 1859
    new-array v0, v2, [Lorg/apache/poi/util/BinaryTree$Node;

    .line 1858
    iput-object v0, p0, Lorg/apache/poi/util/BinaryTree$Node;->_left:[Lorg/apache/poi/util/BinaryTree$Node;

    .line 1863
    new-array v0, v2, [Lorg/apache/poi/util/BinaryTree$Node;

    .line 1862
    iput-object v0, p0, Lorg/apache/poi/util/BinaryTree$Node;->_right:[Lorg/apache/poi/util/BinaryTree$Node;

    .line 1867
    new-array v0, v2, [Lorg/apache/poi/util/BinaryTree$Node;

    .line 1866
    iput-object v0, p0, Lorg/apache/poi/util/BinaryTree$Node;->_parent:[Lorg/apache/poi/util/BinaryTree$Node;

    .line 1871
    new-array v0, v2, [Z

    fill-array-data v0, :array_0

    .line 1870
    iput-object v0, p0, Lorg/apache/poi/util/BinaryTree$Node;->_black:[Z

    .line 1874
    iput-boolean v3, p0, Lorg/apache/poi/util/BinaryTree$Node;->_calculated_hashcode:Z

    .line 1875
    return-void

    .line 1871
    :array_0
    .array-data 1
        0x1t
        0x1t
    .end array-data
.end method


# virtual methods
.method public copyColor(Lorg/apache/poi/util/BinaryTree$Node;I)V
    .locals 2
    .param p1, "node"    # Lorg/apache/poi/util/BinaryTree$Node;
    .param p2, "index"    # I

    .prologue
    .line 2027
    iget-object v0, p0, Lorg/apache/poi/util/BinaryTree$Node;->_black:[Z

    iget-object v1, p1, Lorg/apache/poi/util/BinaryTree$Node;->_black:[Z

    aget-boolean v1, v1, p2

    aput-boolean v1, v0, p2

    .line 2028
    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2075
    if-ne p0, p1, :cond_1

    .line 2085
    :cond_0
    :goto_0
    return v1

    .line 2079
    :cond_1
    instance-of v3, p1, Ljava/util/Map$Entry;

    if-nez v3, :cond_2

    move v1, v2

    .line 2081
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 2083
    check-cast v0, Ljava/util/Map$Entry;

    .line 2085
    .local v0, "e":Ljava/util/Map$Entry;
    iget-object v3, p0, Lorg/apache/poi/util/BinaryTree$Node;->_data:[Ljava/lang/Comparable;

    sget v4, Lorg/apache/poi/util/BinaryTree;->_KEY:I

    aget-object v3, v3, v4

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 2086
    iget-object v3, p0, Lorg/apache/poi/util/BinaryTree$Node;->_data:[Ljava/lang/Comparable;

    sget v4, Lorg/apache/poi/util/BinaryTree;->_VALUE:I

    aget-object v3, v3, v4

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    .line 2085
    goto :goto_0
.end method

.method public getData(I)Ljava/lang/Comparable;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 1886
    iget-object v0, p0, Lorg/apache/poi/util/BinaryTree$Node;->_data:[Ljava/lang/Comparable;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public getKey()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2037
    iget-object v0, p0, Lorg/apache/poi/util/BinaryTree$Node;->_data:[Ljava/lang/Comparable;

    sget v1, Lorg/apache/poi/util/BinaryTree;->_KEY:I

    aget-object v0, v0, v1

    return-object v0
.end method

.method public getLeft(I)Lorg/apache/poi/util/BinaryTree$Node;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 1910
    iget-object v0, p0, Lorg/apache/poi/util/BinaryTree$Node;->_left:[Lorg/apache/poi/util/BinaryTree$Node;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public getParent(I)Lorg/apache/poi/util/BinaryTree$Node;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 1957
    iget-object v0, p0, Lorg/apache/poi/util/BinaryTree$Node;->_parent:[Lorg/apache/poi/util/BinaryTree$Node;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public getRight(I)Lorg/apache/poi/util/BinaryTree$Node;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 1934
    iget-object v0, p0, Lorg/apache/poi/util/BinaryTree$Node;->_right:[Lorg/apache/poi/util/BinaryTree$Node;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public getValue()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2045
    iget-object v0, p0, Lorg/apache/poi/util/BinaryTree$Node;->_data:[Ljava/lang/Comparable;

    sget v1, Lorg/apache/poi/util/BinaryTree;->_VALUE:I

    aget-object v0, v0, v1

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 2095
    iget-boolean v0, p0, Lorg/apache/poi/util/BinaryTree$Node;->_calculated_hashcode:Z

    if-nez v0, :cond_0

    .line 2097
    iget-object v0, p0, Lorg/apache/poi/util/BinaryTree$Node;->_data:[Ljava/lang/Comparable;

    sget v1, Lorg/apache/poi/util/BinaryTree;->_KEY:I

    aget-object v0, v0, v1

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    .line 2098
    iget-object v1, p0, Lorg/apache/poi/util/BinaryTree$Node;->_data:[Ljava/lang/Comparable;

    sget v2, Lorg/apache/poi/util/BinaryTree;->_VALUE:I

    aget-object v1, v1, v2

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 2097
    iput v0, p0, Lorg/apache/poi/util/BinaryTree$Node;->_hashcode:I

    .line 2099
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/poi/util/BinaryTree$Node;->_calculated_hashcode:Z

    .line 2101
    :cond_0
    iget v0, p0, Lorg/apache/poi/util/BinaryTree$Node;->_hashcode:I

    return v0
.end method

.method public isBlack(I)Z
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 1984
    iget-object v0, p0, Lorg/apache/poi/util/BinaryTree$Node;->_black:[Z

    aget-boolean v0, v0, p1

    return v0
.end method

.method public isRed(I)Z
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 1996
    iget-object v0, p0, Lorg/apache/poi/util/BinaryTree$Node;->_black:[Z

    aget-boolean v0, v0, p1

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public setBlack(I)V
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 2006
    iget-object v0, p0, Lorg/apache/poi/util/BinaryTree$Node;->_black:[Z

    const/4 v1, 0x1

    aput-boolean v1, v0, p1

    .line 2007
    return-void
.end method

.method public setLeft(Lorg/apache/poi/util/BinaryTree$Node;I)V
    .locals 1
    .param p1, "node"    # Lorg/apache/poi/util/BinaryTree$Node;
    .param p2, "index"    # I

    .prologue
    .line 1897
    iget-object v0, p0, Lorg/apache/poi/util/BinaryTree$Node;->_left:[Lorg/apache/poi/util/BinaryTree$Node;

    aput-object p1, v0, p2

    .line 1898
    return-void
.end method

.method public setParent(Lorg/apache/poi/util/BinaryTree$Node;I)V
    .locals 1
    .param p1, "node"    # Lorg/apache/poi/util/BinaryTree$Node;
    .param p2, "index"    # I

    .prologue
    .line 1945
    iget-object v0, p0, Lorg/apache/poi/util/BinaryTree$Node;->_parent:[Lorg/apache/poi/util/BinaryTree$Node;

    aput-object p1, v0, p2

    .line 1946
    return-void
.end method

.method public setRed(I)V
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 2016
    iget-object v0, p0, Lorg/apache/poi/util/BinaryTree$Node;->_black:[Z

    const/4 v1, 0x0

    aput-boolean v1, v0, p1

    .line 2017
    return-void
.end method

.method public setRight(Lorg/apache/poi/util/BinaryTree$Node;I)V
    .locals 1
    .param p1, "node"    # Lorg/apache/poi/util/BinaryTree$Node;
    .param p2, "index"    # I

    .prologue
    .line 1921
    iget-object v0, p0, Lorg/apache/poi/util/BinaryTree$Node;->_right:[Lorg/apache/poi/util/BinaryTree$Node;

    aput-object p1, v0, p2

    .line 1922
    return-void
.end method

.method public setValue(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p1, "ignored"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/UnsupportedOperationException;
        }
    .end annotation

    .prologue
    .line 2059
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    .line 2060
    const-string/jumbo v1, "Map.Entry.setValue is not supported"

    .line 2059
    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public swapColors(Lorg/apache/poi/util/BinaryTree$Node;I)V
    .locals 3
    .param p1, "node"    # Lorg/apache/poi/util/BinaryTree$Node;
    .param p2, "index"    # I

    .prologue
    .line 1970
    iget-object v0, p0, Lorg/apache/poi/util/BinaryTree$Node;->_black:[Z

    aget-boolean v1, v0, p2

    iget-object v2, p1, Lorg/apache/poi/util/BinaryTree$Node;->_black:[Z

    aget-boolean v2, v2, p2

    xor-int/2addr v1, v2

    aput-boolean v1, v0, p2

    .line 1971
    iget-object v0, p1, Lorg/apache/poi/util/BinaryTree$Node;->_black:[Z

    aget-boolean v1, v0, p2

    iget-object v2, p0, Lorg/apache/poi/util/BinaryTree$Node;->_black:[Z

    aget-boolean v2, v2, p2

    xor-int/2addr v1, v2

    aput-boolean v1, v0, p2

    .line 1972
    iget-object v0, p0, Lorg/apache/poi/util/BinaryTree$Node;->_black:[Z

    aget-boolean v1, v0, p2

    iget-object v2, p1, Lorg/apache/poi/util/BinaryTree$Node;->_black:[Z

    aget-boolean v2, v2, p2

    xor-int/2addr v1, v2

    aput-boolean v1, v0, p2

    .line 1973
    return-void
.end method
