.class public Lorg/apache/poi/util/LittleEndianInputStream;
.super Ljava/io/FilterInputStream;
.source "LittleEndianInputStream.java"

# interfaces
.implements Lorg/apache/poi/util/LittleEndianInput;


# direct methods
.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 0
    .param p1, "is"    # Ljava/io/InputStream;

    .prologue
    .line 34
    invoke-direct {p0, p1}, Ljava/io/FilterInputStream;-><init>(Ljava/io/InputStream;)V

    .line 35
    return-void
.end method

.method private static checkEOF(I)V
    .locals 2
    .param p0, "value"    # I

    .prologue
    .line 122
    if-gez p0, :cond_0

    .line 123
    new-instance v0, Ljava/lang/RuntimeException;

    const-string/jumbo v1, "Unexpected end-of-file"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 125
    :cond_0
    return-void
.end method


# virtual methods
.method public available()I
    .locals 2

    .prologue
    .line 38
    :try_start_0
    invoke-super {p0}, Ljava/io/FilterInputStream;->available()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    return v1

    .line 39
    :catch_0
    move-exception v0

    .line 40
    .local v0, "e":Ljava/io/IOException;
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public readByte()B
    .locals 1

    .prologue
    .line 44
    invoke-virtual {p0}, Lorg/apache/poi/util/LittleEndianInputStream;->readUByte()I

    move-result v0

    int-to-byte v0, v0

    return v0
.end method

.method public readDouble()D
    .locals 2

    .prologue
    .line 57
    invoke-virtual {p0}, Lorg/apache/poi/util/LittleEndianInputStream;->readLong()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v0

    return-wide v0
.end method

.method public readFully([B)V
    .locals 2
    .param p1, "buf"    # [B

    .prologue
    .line 128
    const/4 v0, 0x0

    array-length v1, p1

    invoke-virtual {p0, p1, v0, v1}, Lorg/apache/poi/util/LittleEndianInputStream;->readFully([BII)V

    .line 129
    return-void
.end method

.method public readFully([BII)V
    .locals 5
    .param p1, "buf"    # [B
    .param p2, "off"    # I
    .param p3, "len"    # I

    .prologue
    .line 132
    add-int v3, p2, p3

    .line 133
    .local v3, "max":I
    move v2, p2

    .local v2, "i":I
    :goto_0
    if-lt v2, v3, :cond_0

    .line 143
    return-void

    .line 136
    :cond_0
    :try_start_0
    iget-object v4, p0, Lorg/apache/poi/util/LittleEndianInputStream;->in:Ljava/io/InputStream;

    invoke-virtual {v4}, Ljava/io/InputStream;->read()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 140
    .local v0, "ch":I
    invoke-static {v0}, Lorg/apache/poi/util/LittleEndianInputStream;->checkEOF(I)V

    .line 141
    int-to-byte v4, v0

    aput-byte v4, p1, v2

    .line 133
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 137
    .end local v0    # "ch":I
    :catch_0
    move-exception v1

    .line 138
    .local v1, "e":Ljava/io/IOException;
    new-instance v4, Ljava/lang/RuntimeException;

    invoke-direct {v4, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v4
.end method

.method public readInt()I
    .locals 7

    .prologue
    .line 65
    :try_start_0
    iget-object v5, p0, Lorg/apache/poi/util/LittleEndianInputStream;->in:Ljava/io/InputStream;

    invoke-virtual {v5}, Ljava/io/InputStream;->read()I

    move-result v0

    .line 66
    .local v0, "ch1":I
    iget-object v5, p0, Lorg/apache/poi/util/LittleEndianInputStream;->in:Ljava/io/InputStream;

    invoke-virtual {v5}, Ljava/io/InputStream;->read()I

    move-result v1

    .line 67
    .local v1, "ch2":I
    iget-object v5, p0, Lorg/apache/poi/util/LittleEndianInputStream;->in:Ljava/io/InputStream;

    invoke-virtual {v5}, Ljava/io/InputStream;->read()I

    move-result v2

    .line 68
    .local v2, "ch3":I
    iget-object v5, p0, Lorg/apache/poi/util/LittleEndianInputStream;->in:Ljava/io/InputStream;

    invoke-virtual {v5}, Ljava/io/InputStream;->read()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    .line 72
    .local v3, "ch4":I
    or-int v5, v0, v1

    or-int/2addr v5, v2

    or-int/2addr v5, v3

    invoke-static {v5}, Lorg/apache/poi/util/LittleEndianInputStream;->checkEOF(I)V

    .line 73
    shl-int/lit8 v5, v3, 0x18

    shl-int/lit8 v6, v2, 0x10

    add-int/2addr v5, v6

    shl-int/lit8 v6, v1, 0x8

    add-int/2addr v5, v6

    shl-int/lit8 v6, v0, 0x0

    add-int/2addr v5, v6

    return v5

    .line 69
    .end local v0    # "ch1":I
    .end local v1    # "ch2":I
    .end local v2    # "ch3":I
    .end local v3    # "ch4":I
    :catch_0
    move-exception v4

    .line 70
    .local v4, "e":Ljava/io/IOException;
    new-instance v5, Ljava/lang/RuntimeException;

    invoke-direct {v5, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v5
.end method

.method public readLong()J
    .locals 14

    .prologue
    .line 85
    :try_start_0
    iget-object v9, p0, Lorg/apache/poi/util/LittleEndianInputStream;->in:Ljava/io/InputStream;

    invoke-virtual {v9}, Ljava/io/InputStream;->read()I

    move-result v0

    .line 86
    .local v0, "b0":I
    iget-object v9, p0, Lorg/apache/poi/util/LittleEndianInputStream;->in:Ljava/io/InputStream;

    invoke-virtual {v9}, Ljava/io/InputStream;->read()I

    move-result v1

    .line 87
    .local v1, "b1":I
    iget-object v9, p0, Lorg/apache/poi/util/LittleEndianInputStream;->in:Ljava/io/InputStream;

    invoke-virtual {v9}, Ljava/io/InputStream;->read()I

    move-result v2

    .line 88
    .local v2, "b2":I
    iget-object v9, p0, Lorg/apache/poi/util/LittleEndianInputStream;->in:Ljava/io/InputStream;

    invoke-virtual {v9}, Ljava/io/InputStream;->read()I

    move-result v3

    .line 89
    .local v3, "b3":I
    iget-object v9, p0, Lorg/apache/poi/util/LittleEndianInputStream;->in:Ljava/io/InputStream;

    invoke-virtual {v9}, Ljava/io/InputStream;->read()I

    move-result v4

    .line 90
    .local v4, "b4":I
    iget-object v9, p0, Lorg/apache/poi/util/LittleEndianInputStream;->in:Ljava/io/InputStream;

    invoke-virtual {v9}, Ljava/io/InputStream;->read()I

    move-result v5

    .line 91
    .local v5, "b5":I
    iget-object v9, p0, Lorg/apache/poi/util/LittleEndianInputStream;->in:Ljava/io/InputStream;

    invoke-virtual {v9}, Ljava/io/InputStream;->read()I

    move-result v6

    .line 92
    .local v6, "b6":I
    iget-object v9, p0, Lorg/apache/poi/util/LittleEndianInputStream;->in:Ljava/io/InputStream;

    invoke-virtual {v9}, Ljava/io/InputStream;->read()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v7

    .line 96
    .local v7, "b7":I
    or-int v9, v0, v1

    or-int/2addr v9, v2

    or-int/2addr v9, v3

    or-int/2addr v9, v4

    or-int/2addr v9, v5

    or-int/2addr v9, v6

    or-int/2addr v9, v7

    invoke-static {v9}, Lorg/apache/poi/util/LittleEndianInputStream;->checkEOF(I)V

    .line 97
    int-to-long v10, v7

    const/16 v9, 0x38

    shl-long/2addr v10, v9

    .line 98
    int-to-long v12, v6

    const/16 v9, 0x30

    shl-long/2addr v12, v9

    .line 97
    add-long/2addr v10, v12

    .line 99
    int-to-long v12, v5

    const/16 v9, 0x28

    shl-long/2addr v12, v9

    .line 97
    add-long/2addr v10, v12

    .line 100
    int-to-long v12, v4

    const/16 v9, 0x20

    shl-long/2addr v12, v9

    .line 97
    add-long/2addr v10, v12

    .line 101
    int-to-long v12, v3

    const/16 v9, 0x18

    shl-long/2addr v12, v9

    .line 97
    add-long/2addr v10, v12

    .line 102
    shl-int/lit8 v9, v2, 0x10

    int-to-long v12, v9

    .line 97
    add-long/2addr v10, v12

    .line 103
    shl-int/lit8 v9, v1, 0x8

    int-to-long v12, v9

    .line 97
    add-long/2addr v10, v12

    .line 104
    shl-int/lit8 v9, v0, 0x0

    int-to-long v12, v9

    .line 97
    add-long/2addr v10, v12

    return-wide v10

    .line 93
    .end local v0    # "b0":I
    .end local v1    # "b1":I
    .end local v2    # "b2":I
    .end local v3    # "b3":I
    .end local v4    # "b4":I
    .end local v5    # "b5":I
    .end local v6    # "b6":I
    .end local v7    # "b7":I
    :catch_0
    move-exception v8

    .line 94
    .local v8, "e":Ljava/io/IOException;
    new-instance v9, Ljava/lang/RuntimeException;

    invoke-direct {v9, v8}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v9
.end method

.method public readShort()S
    .locals 1

    .prologue
    .line 107
    invoke-virtual {p0}, Lorg/apache/poi/util/LittleEndianInputStream;->readUShort()I

    move-result v0

    int-to-short v0, v0

    return v0
.end method

.method public readUByte()I
    .locals 3

    .prologue
    .line 49
    :try_start_0
    iget-object v2, p0, Lorg/apache/poi/util/LittleEndianInputStream;->in:Ljava/io/InputStream;

    invoke-virtual {v2}, Ljava/io/InputStream;->read()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 53
    .local v0, "ch":I
    invoke-static {v0}, Lorg/apache/poi/util/LittleEndianInputStream;->checkEOF(I)V

    .line 54
    return v0

    .line 50
    .end local v0    # "ch":I
    :catch_0
    move-exception v1

    .line 51
    .local v1, "e":Ljava/io/IOException;
    new-instance v2, Ljava/lang/RuntimeException;

    invoke-direct {v2, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v2
.end method

.method public readUShort()I
    .locals 5

    .prologue
    .line 113
    :try_start_0
    iget-object v3, p0, Lorg/apache/poi/util/LittleEndianInputStream;->in:Ljava/io/InputStream;

    invoke-virtual {v3}, Ljava/io/InputStream;->read()I

    move-result v0

    .line 114
    .local v0, "ch1":I
    iget-object v3, p0, Lorg/apache/poi/util/LittleEndianInputStream;->in:Ljava/io/InputStream;

    invoke-virtual {v3}, Ljava/io/InputStream;->read()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 118
    .local v1, "ch2":I
    or-int v3, v0, v1

    invoke-static {v3}, Lorg/apache/poi/util/LittleEndianInputStream;->checkEOF(I)V

    .line 119
    shl-int/lit8 v3, v1, 0x8

    shl-int/lit8 v4, v0, 0x0

    add-int/2addr v3, v4

    return v3

    .line 115
    .end local v0    # "ch1":I
    .end local v1    # "ch2":I
    :catch_0
    move-exception v2

    .line 116
    .local v2, "e":Ljava/io/IOException;
    new-instance v3, Ljava/lang/RuntimeException;

    invoke-direct {v3, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v3
.end method
