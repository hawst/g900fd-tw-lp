.class Lorg/apache/poi/poifs/filesystem/FilteringDirectoryNode$FilteringIterator;
.super Ljava/lang/Object;
.source "FilteringDirectoryNode.java"

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/poi/poifs/filesystem/FilteringDirectoryNode;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "FilteringIterator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Iterator",
        "<",
        "Lorg/apache/poi/poifs/filesystem/Entry;",
        ">;"
    }
.end annotation


# instance fields
.field private next:Lorg/apache/poi/poifs/filesystem/Entry;

.field private parent:Ljava/util/Iterator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Iterator",
            "<",
            "Lorg/apache/poi/poifs/filesystem/Entry;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lorg/apache/poi/poifs/filesystem/FilteringDirectoryNode;


# direct methods
.method private constructor <init>(Lorg/apache/poi/poifs/filesystem/FilteringDirectoryNode;)V
    .locals 1

    .prologue
    .line 182
    iput-object p1, p0, Lorg/apache/poi/poifs/filesystem/FilteringDirectoryNode$FilteringIterator;->this$0:Lorg/apache/poi/poifs/filesystem/FilteringDirectoryNode;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 183
    # getter for: Lorg/apache/poi/poifs/filesystem/FilteringDirectoryNode;->directory:Lorg/apache/poi/poifs/filesystem/DirectoryEntry;
    invoke-static {p1}, Lorg/apache/poi/poifs/filesystem/FilteringDirectoryNode;->access$0(Lorg/apache/poi/poifs/filesystem/FilteringDirectoryNode;)Lorg/apache/poi/poifs/filesystem/DirectoryEntry;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/poi/poifs/filesystem/DirectoryEntry;->getEntries()Ljava/util/Iterator;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/poi/poifs/filesystem/FilteringDirectoryNode$FilteringIterator;->parent:Ljava/util/Iterator;

    .line 184
    invoke-direct {p0}, Lorg/apache/poi/poifs/filesystem/FilteringDirectoryNode$FilteringIterator;->locateNext()V

    .line 185
    return-void
.end method

.method synthetic constructor <init>(Lorg/apache/poi/poifs/filesystem/FilteringDirectoryNode;Lorg/apache/poi/poifs/filesystem/FilteringDirectoryNode$FilteringIterator;)V
    .locals 0

    .prologue
    .line 182
    invoke-direct {p0, p1}, Lorg/apache/poi/poifs/filesystem/FilteringDirectoryNode$FilteringIterator;-><init>(Lorg/apache/poi/poifs/filesystem/FilteringDirectoryNode;)V

    return-void
.end method

.method private locateNext()V
    .locals 3

    .prologue
    .line 187
    const/4 v1, 0x0

    iput-object v1, p0, Lorg/apache/poi/poifs/filesystem/FilteringDirectoryNode$FilteringIterator;->next:Lorg/apache/poi/poifs/filesystem/Entry;

    .line 189
    :cond_0
    :goto_0
    iget-object v1, p0, Lorg/apache/poi/poifs/filesystem/FilteringDirectoryNode$FilteringIterator;->parent:Ljava/util/Iterator;

    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lorg/apache/poi/poifs/filesystem/FilteringDirectoryNode$FilteringIterator;->next:Lorg/apache/poi/poifs/filesystem/Entry;

    if-eqz v1, :cond_2

    .line 195
    :cond_1
    return-void

    .line 190
    :cond_2
    iget-object v1, p0, Lorg/apache/poi/poifs/filesystem/FilteringDirectoryNode$FilteringIterator;->parent:Ljava/util/Iterator;

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/poifs/filesystem/Entry;

    .line 191
    .local v0, "e":Lorg/apache/poi/poifs/filesystem/Entry;
    iget-object v1, p0, Lorg/apache/poi/poifs/filesystem/FilteringDirectoryNode$FilteringIterator;->this$0:Lorg/apache/poi/poifs/filesystem/FilteringDirectoryNode;

    # getter for: Lorg/apache/poi/poifs/filesystem/FilteringDirectoryNode;->excludes:Ljava/util/Set;
    invoke-static {v1}, Lorg/apache/poi/poifs/filesystem/FilteringDirectoryNode;->access$1(Lorg/apache/poi/poifs/filesystem/FilteringDirectoryNode;)Ljava/util/Set;

    move-result-object v1

    invoke-interface {v0}, Lorg/apache/poi/poifs/filesystem/Entry;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 192
    iget-object v1, p0, Lorg/apache/poi/poifs/filesystem/FilteringDirectoryNode$FilteringIterator;->this$0:Lorg/apache/poi/poifs/filesystem/FilteringDirectoryNode;

    # invokes: Lorg/apache/poi/poifs/filesystem/FilteringDirectoryNode;->wrapEntry(Lorg/apache/poi/poifs/filesystem/Entry;)Lorg/apache/poi/poifs/filesystem/Entry;
    invoke-static {v1, v0}, Lorg/apache/poi/poifs/filesystem/FilteringDirectoryNode;->access$2(Lorg/apache/poi/poifs/filesystem/FilteringDirectoryNode;Lorg/apache/poi/poifs/filesystem/Entry;)Lorg/apache/poi/poifs/filesystem/Entry;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/poi/poifs/filesystem/FilteringDirectoryNode$FilteringIterator;->next:Lorg/apache/poi/poifs/filesystem/Entry;

    goto :goto_0
.end method


# virtual methods
.method public hasNext()Z
    .locals 1

    .prologue
    .line 198
    iget-object v0, p0, Lorg/apache/poi/poifs/filesystem/FilteringDirectoryNode$FilteringIterator;->next:Lorg/apache/poi/poifs/filesystem/Entry;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic next()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/poi/poifs/filesystem/FilteringDirectoryNode$FilteringIterator;->next()Lorg/apache/poi/poifs/filesystem/Entry;

    move-result-object v0

    return-object v0
.end method

.method public next()Lorg/apache/poi/poifs/filesystem/Entry;
    .locals 1

    .prologue
    .line 202
    iget-object v0, p0, Lorg/apache/poi/poifs/filesystem/FilteringDirectoryNode$FilteringIterator;->next:Lorg/apache/poi/poifs/filesystem/Entry;

    .line 203
    .local v0, "e":Lorg/apache/poi/poifs/filesystem/Entry;
    invoke-direct {p0}, Lorg/apache/poi/poifs/filesystem/FilteringDirectoryNode$FilteringIterator;->locateNext()V

    .line 204
    return-object v0
.end method

.method public remove()V
    .locals 2

    .prologue
    .line 208
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string/jumbo v1, "Remove not supported"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
