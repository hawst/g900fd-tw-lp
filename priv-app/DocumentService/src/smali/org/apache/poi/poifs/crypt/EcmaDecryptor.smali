.class public Lorg/apache/poi/poifs/crypt/EcmaDecryptor;
.super Lorg/apache/poi/poifs/crypt/Decryptor;
.source "EcmaDecryptor.java"


# instance fields
.field private _length:J

.field private final info:Lorg/apache/poi/poifs/crypt/EncryptionInfo;

.field private passwordHash:[B


# direct methods
.method public constructor <init>(Lorg/apache/poi/poifs/crypt/EncryptionInfo;)V
    .locals 2
    .param p1, "info"    # Lorg/apache/poi/poifs/crypt/EncryptionInfo;

    .prologue
    .line 44
    invoke-direct {p0}, Lorg/apache/poi/poifs/crypt/Decryptor;-><init>()V

    .line 42
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lorg/apache/poi/poifs/crypt/EcmaDecryptor;->_length:J

    .line 45
    iput-object p1, p0, Lorg/apache/poi/poifs/crypt/EcmaDecryptor;->info:Lorg/apache/poi/poifs/crypt/EncryptionInfo;

    .line 46
    return-void
.end method

.method private generateKey(I)[B
    .locals 12
    .param p1, "block"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/NoSuchAlgorithmException;
        }
    .end annotation

    .prologue
    const/4 v11, 0x0

    .line 49
    const-string/jumbo v9, "SHA-1"

    invoke-static {v9}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v5

    .line 51
    .local v5, "sha1":Ljava/security/MessageDigest;
    iget-object v9, p0, Lorg/apache/poi/poifs/crypt/EcmaDecryptor;->passwordHash:[B

    invoke-virtual {v5, v9}, Ljava/security/MessageDigest;->update([B)V

    .line 52
    const/4 v9, 0x4

    new-array v0, v9, [B

    .line 53
    .local v0, "blockValue":[B
    invoke-static {v0, v11, p1}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 54
    invoke-virtual {v5, v0}, Ljava/security/MessageDigest;->digest([B)[B

    move-result-object v2

    .line 56
    .local v2, "finalHash":[B
    iget-object v9, p0, Lorg/apache/poi/poifs/crypt/EcmaDecryptor;->info:Lorg/apache/poi/poifs/crypt/EncryptionInfo;

    invoke-virtual {v9}, Lorg/apache/poi/poifs/crypt/EncryptionInfo;->getHeader()Lorg/apache/poi/poifs/crypt/EncryptionHeader;

    move-result-object v9

    invoke-virtual {v9}, Lorg/apache/poi/poifs/crypt/EncryptionHeader;->getKeySize()I

    move-result v9

    div-int/lit8 v4, v9, 0x8

    .line 58
    .local v4, "requiredKeyLength":I
    const/16 v9, 0x40

    new-array v1, v9, [B

    .line 60
    .local v1, "buff":[B
    const/16 v9, 0x36

    invoke-static {v1, v9}, Ljava/util/Arrays;->fill([BB)V

    .line 62
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    array-length v9, v2

    if-lt v3, v9, :cond_0

    .line 66
    invoke-virtual {v5}, Ljava/security/MessageDigest;->reset()V

    .line 67
    invoke-virtual {v5, v1}, Ljava/security/MessageDigest;->digest([B)[B

    move-result-object v6

    .line 69
    .local v6, "x1":[B
    const/16 v9, 0x5c

    invoke-static {v1, v9}, Ljava/util/Arrays;->fill([BB)V

    .line 70
    const/4 v3, 0x0

    :goto_1
    array-length v9, v2

    if-lt v3, v9, :cond_1

    .line 74
    invoke-virtual {v5}, Ljava/security/MessageDigest;->reset()V

    .line 75
    invoke-virtual {v5, v1}, Ljava/security/MessageDigest;->digest([B)[B

    move-result-object v7

    .line 77
    .local v7, "x2":[B
    array-length v9, v6

    array-length v10, v7

    add-int/2addr v9, v10

    new-array v8, v9, [B

    .line 78
    .local v8, "x3":[B
    array-length v9, v6

    invoke-static {v6, v11, v8, v11, v9}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 79
    array-length v9, v6

    array-length v10, v7

    invoke-static {v7, v11, v8, v9, v10}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 81
    invoke-direct {p0, v8, v4}, Lorg/apache/poi/poifs/crypt/EcmaDecryptor;->truncateOrPad([BI)[B

    move-result-object v9

    return-object v9

    .line 63
    .end local v6    # "x1":[B
    .end local v7    # "x2":[B
    .end local v8    # "x3":[B
    :cond_0
    aget-byte v9, v1, v3

    aget-byte v10, v2, v3

    xor-int/2addr v9, v10

    int-to-byte v9, v9

    aput-byte v9, v1, v3

    .line 62
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 71
    .restart local v6    # "x1":[B
    :cond_1
    aget-byte v9, v1, v3

    aget-byte v10, v2, v3

    xor-int/2addr v9, v10

    int-to-byte v9, v9

    aput-byte v9, v1, v3

    .line 70
    add-int/lit8 v3, v3, 0x1

    goto :goto_1
.end method

.method private getCipher()Ljavax/crypto/Cipher;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/GeneralSecurityException;
        }
    .end annotation

    .prologue
    .line 118
    const/4 v3, 0x0

    invoke-direct {p0, v3}, Lorg/apache/poi/poifs/crypt/EcmaDecryptor;->generateKey(I)[B

    move-result-object v1

    .line 119
    .local v1, "key":[B
    const-string/jumbo v3, "AES/ECB/NoPadding"

    invoke-static {v3}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v0

    .line 120
    .local v0, "cipher":Ljavax/crypto/Cipher;
    new-instance v2, Ljavax/crypto/spec/SecretKeySpec;

    const-string/jumbo v3, "AES"

    invoke-direct {v2, v1, v3}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    .line 121
    .local v2, "skey":Ljavax/crypto/SecretKey;
    const/4 v3, 0x2

    invoke-virtual {v0, v3, v2}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;)V

    .line 123
    return-object v0
.end method

.method private truncateOrPad([BI)[B
    .locals 4
    .param p1, "source"    # [B
    .param p2, "length"    # I

    .prologue
    const/4 v3, 0x0

    .line 107
    new-array v1, p2, [B

    .line 108
    .local v1, "result":[B
    array-length v2, p1

    invoke-static {p2, v2}, Ljava/lang/Math;->min(II)I

    move-result v2

    invoke-static {p1, v3, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 109
    array-length v2, p1

    if-le p2, v2, :cond_0

    .line 110
    array-length v0, p1

    .local v0, "i":I
    :goto_0
    if-lt v0, p2, :cond_1

    .line 114
    .end local v0    # "i":I
    :cond_0
    return-object v1

    .line 111
    .restart local v0    # "i":I
    :cond_1
    aput-byte v3, v1, v0

    .line 110
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public getDataStream(Lorg/apache/poi/poifs/filesystem/DirectoryNode;)Ljava/io/InputStream;
    .locals 4
    .param p1, "dir"    # Lorg/apache/poi/poifs/filesystem/DirectoryNode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/security/GeneralSecurityException;
        }
    .end annotation

    .prologue
    .line 127
    const-string/jumbo v1, "EncryptedPackage"

    invoke-virtual {p1, v1}, Lorg/apache/poi/poifs/filesystem/DirectoryNode;->createDocumentInputStream(Ljava/lang/String;)Lorg/apache/poi/poifs/filesystem/DocumentInputStream;

    move-result-object v0

    .line 129
    .local v0, "dis":Lorg/apache/poi/poifs/filesystem/DocumentInputStream;
    invoke-virtual {v0}, Lorg/apache/poi/poifs/filesystem/DocumentInputStream;->readLong()J

    move-result-wide v2

    iput-wide v2, p0, Lorg/apache/poi/poifs/crypt/EcmaDecryptor;->_length:J

    .line 131
    if-eqz v0, :cond_0

    .line 132
    invoke-virtual {v0}, Lorg/apache/poi/poifs/filesystem/DocumentInputStream;->close()V

    .line 133
    :cond_0
    new-instance v1, Ljavax/crypto/CipherInputStream;

    invoke-direct {p0}, Lorg/apache/poi/poifs/crypt/EcmaDecryptor;->getCipher()Ljavax/crypto/Cipher;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Ljavax/crypto/CipherInputStream;-><init>(Ljava/io/InputStream;Ljavax/crypto/Cipher;)V

    return-object v1
.end method

.method public getLength()J
    .locals 4

    .prologue
    .line 137
    iget-wide v0, p0, Lorg/apache/poi/poifs/crypt/EcmaDecryptor;->_length:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "EcmaDecryptor.getDataStream() was not called"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 138
    :cond_0
    iget-wide v0, p0, Lorg/apache/poi/poifs/crypt/EcmaDecryptor;->_length:J

    return-wide v0
.end method

.method public verifyPassword(Ljava/lang/String;)Z
    .locals 1
    .param p1, "password"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/GeneralSecurityException;
        }
    .end annotation

    .prologue
    .line 98
    const/4 v0, 0x0

    return v0
.end method
