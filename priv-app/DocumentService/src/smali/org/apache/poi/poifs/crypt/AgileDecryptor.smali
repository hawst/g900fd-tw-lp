.class public Lorg/apache/poi/poifs/crypt/AgileDecryptor;
.super Lorg/apache/poi/poifs/crypt/Decryptor;
.source "AgileDecryptor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/poi/poifs/crypt/AgileDecryptor$ChunkedCipherInputStream;
    }
.end annotation


# instance fields
.field private final _info:Lorg/apache/poi/poifs/crypt/EncryptionInfo;

.field private _length:J

.field private _secretKey:Ljavax/crypto/SecretKey;


# direct methods
.method protected constructor <init>(Lorg/apache/poi/poifs/crypt/EncryptionInfo;)V
    .locals 2
    .param p1, "info"    # Lorg/apache/poi/poifs/crypt/EncryptionInfo;

    .prologue
    .line 76
    invoke-direct {p0}, Lorg/apache/poi/poifs/crypt/Decryptor;-><init>()V

    .line 42
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lorg/apache/poi/poifs/crypt/AgileDecryptor;->_length:J

    .line 77
    iput-object p1, p0, Lorg/apache/poi/poifs/crypt/AgileDecryptor;->_info:Lorg/apache/poi/poifs/crypt/EncryptionInfo;

    .line 78
    return-void
.end method

.method static synthetic access$0(Lorg/apache/poi/poifs/crypt/AgileDecryptor;)Lorg/apache/poi/poifs/crypt/EncryptionInfo;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lorg/apache/poi/poifs/crypt/AgileDecryptor;->_info:Lorg/apache/poi/poifs/crypt/EncryptionInfo;

    return-object v0
.end method

.method static synthetic access$1(Lorg/apache/poi/poifs/crypt/AgileDecryptor;)Ljavax/crypto/SecretKey;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lorg/apache/poi/poifs/crypt/AgileDecryptor;->_secretKey:Ljavax/crypto/SecretKey;

    return-object v0
.end method

.method static synthetic access$2(Lorg/apache/poi/poifs/crypt/AgileDecryptor;IILjavax/crypto/SecretKey;[B)Ljavax/crypto/Cipher;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/GeneralSecurityException;
        }
    .end annotation

    .prologue
    .line 164
    invoke-direct {p0, p1, p2, p3, p4}, Lorg/apache/poi/poifs/crypt/AgileDecryptor;->getCipher(IILjavax/crypto/SecretKey;[B)Ljavax/crypto/Cipher;

    move-result-object v0

    return-object v0
.end method

.method private getBlock(I[B)[B
    .locals 4
    .param p1, "algorithm"    # I
    .param p2, "hash"    # [B

    .prologue
    const/4 v3, 0x0

    .line 186
    invoke-static {p1}, Lorg/apache/poi/poifs/crypt/AgileDecryptor;->getBlockSize(I)I

    move-result v1

    new-array v0, v1, [B

    .line 187
    .local v0, "result":[B
    const/16 v1, 0x36

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([BB)V

    .line 188
    array-length v1, v0

    array-length v2, p2

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-static {p2, v3, v0, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 189
    return-object v0
.end method

.method private getCipher(IILjavax/crypto/SecretKey;[B)Ljavax/crypto/Cipher;
    .locals 7
    .param p1, "algorithm"    # I
    .param p2, "mode"    # I
    .param p3, "key"    # Ljavax/crypto/SecretKey;
    .param p4, "vec"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/GeneralSecurityException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x2

    .line 166
    const/4 v3, 0x0

    .line 167
    .local v3, "name":Ljava/lang/String;
    const/4 v0, 0x0

    .line 169
    .local v0, "chain":Ljava/lang/String;
    const/16 v4, 0x660e

    if-eq p1, v4, :cond_0

    .line 170
    const/16 v4, 0x660f

    if-eq p1, v4, :cond_0

    .line 171
    const/16 v4, 0x6610

    if-ne p1, v4, :cond_1

    .line 172
    :cond_0
    const-string/jumbo v3, "AES"

    .line 174
    :cond_1
    if-ne p2, v6, :cond_3

    .line 175
    const-string/jumbo v0, "CBC"

    .line 179
    :cond_2
    :goto_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "/NoPadding"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v1

    .line 180
    .local v1, "cipher":Ljavax/crypto/Cipher;
    new-instance v2, Ljavax/crypto/spec/IvParameterSpec;

    invoke-direct {v2, p4}, Ljavax/crypto/spec/IvParameterSpec;-><init>([B)V

    .line 181
    .local v2, "iv":Ljavax/crypto/spec/IvParameterSpec;
    invoke-virtual {v1, v6, p3, v2}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;Ljava/security/spec/AlgorithmParameterSpec;)V

    .line 182
    return-object v1

    .line 176
    .end local v1    # "cipher":Ljavax/crypto/Cipher;
    .end local v2    # "iv":Ljavax/crypto/spec/IvParameterSpec;
    :cond_3
    const/4 v4, 0x3

    if-ne p2, v4, :cond_2

    .line 177
    const-string/jumbo v0, "CFB"

    goto :goto_0
.end method


# virtual methods
.method protected generateIv(I[B[B)[B
    .locals 2
    .param p1, "algorithm"    # I
    .param p2, "salt"    # [B
    .param p3, "blockKey"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/NoSuchAlgorithmException;
        }
    .end annotation

    .prologue
    .line 203
    if-nez p3, :cond_0

    .line 204
    invoke-direct {p0, p1, p2}, Lorg/apache/poi/poifs/crypt/AgileDecryptor;->getBlock(I[B)[B

    move-result-object v1

    .line 208
    :goto_0
    return-object v1

    .line 206
    :cond_0
    const-string/jumbo v1, "SHA-1"

    invoke-static {v1}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v0

    .line 207
    .local v0, "sha1":Ljava/security/MessageDigest;
    invoke-virtual {v0, p2}, Ljava/security/MessageDigest;->update([B)V

    .line 208
    invoke-virtual {v0, p3}, Ljava/security/MessageDigest;->digest([B)[B

    move-result-object v1

    invoke-direct {p0, p1, v1}, Lorg/apache/poi/poifs/crypt/AgileDecryptor;->getBlock(I[B)[B

    move-result-object v1

    goto :goto_0
.end method

.method public getDataStream(Lorg/apache/poi/poifs/filesystem/DirectoryNode;)Ljava/io/InputStream;
    .locals 4
    .param p1, "dir"    # Lorg/apache/poi/poifs/filesystem/DirectoryNode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/security/GeneralSecurityException;
        }
    .end annotation

    .prologue
    .line 66
    const-string/jumbo v1, "EncryptedPackage"

    invoke-virtual {p1, v1}, Lorg/apache/poi/poifs/filesystem/DirectoryNode;->createDocumentInputStream(Ljava/lang/String;)Lorg/apache/poi/poifs/filesystem/DocumentInputStream;

    move-result-object v0

    .line 67
    .local v0, "dis":Lorg/apache/poi/poifs/filesystem/DocumentInputStream;
    invoke-virtual {v0}, Lorg/apache/poi/poifs/filesystem/DocumentInputStream;->readLong()J

    move-result-wide v2

    iput-wide v2, p0, Lorg/apache/poi/poifs/crypt/AgileDecryptor;->_length:J

    .line 68
    new-instance v1, Lorg/apache/poi/poifs/crypt/AgileDecryptor$ChunkedCipherInputStream;

    iget-wide v2, p0, Lorg/apache/poi/poifs/crypt/AgileDecryptor;->_length:J

    invoke-direct {v1, p0, v0, v2, v3}, Lorg/apache/poi/poifs/crypt/AgileDecryptor$ChunkedCipherInputStream;-><init>(Lorg/apache/poi/poifs/crypt/AgileDecryptor;Lorg/apache/poi/poifs/filesystem/DocumentInputStream;J)V

    return-object v1
.end method

.method public getLength()J
    .locals 4

    .prologue
    .line 72
    iget-wide v0, p0, Lorg/apache/poi/poifs/crypt/AgileDecryptor;->_length:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "EcmaDecryptor.getDataStream() was not called"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 73
    :cond_0
    iget-wide v0, p0, Lorg/apache/poi/poifs/crypt/AgileDecryptor;->_length:J

    return-wide v0
.end method

.method public verifyPassword(Ljava/lang/String;)Z
    .locals 1
    .param p1, "password"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/GeneralSecurityException;
        }
    .end annotation

    .prologue
    .line 62
    const/4 v0, 0x0

    return v0
.end method
