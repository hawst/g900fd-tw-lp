.class Lorg/apache/poi/poifs/eventfilesystem/POIFSReader$SampleListener;
.super Ljava/lang/Object;
.source "POIFSReader.java"

# interfaces
.implements Lorg/apache/poi/poifs/eventfilesystem/POIFSReaderListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/poi/poifs/eventfilesystem/POIFSReader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SampleListener"
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 301
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 303
    return-void
.end method


# virtual methods
.method public processPOIFSReaderEvent(Lorg/apache/poi/poifs/eventfilesystem/POIFSReaderEvent;)V
    .locals 6
    .param p1, "event"    # Lorg/apache/poi/poifs/eventfilesystem/POIFSReaderEvent;

    .prologue
    .line 313
    invoke-virtual {p1}, Lorg/apache/poi/poifs/eventfilesystem/POIFSReaderEvent;->getStream()Lorg/apache/poi/poifs/filesystem/DocumentInputStream;

    move-result-object v1

    .line 314
    .local v1, "istream":Lorg/apache/poi/poifs/filesystem/DocumentInputStream;
    invoke-virtual {p1}, Lorg/apache/poi/poifs/eventfilesystem/POIFSReaderEvent;->getPath()Lorg/apache/poi/poifs/filesystem/POIFSDocumentPath;

    move-result-object v3

    .line 319
    .local v3, "path":Lorg/apache/poi/poifs/filesystem/POIFSDocumentPath;
    :try_start_0
    invoke-virtual {v1}, Lorg/apache/poi/poifs/filesystem/DocumentInputStream;->available()I

    move-result v5

    new-array v0, v5, [B

    .line 321
    .local v0, "data":[B
    invoke-virtual {v1, v0}, Lorg/apache/poi/poifs/filesystem/DocumentInputStream;->read([B)I

    .line 322
    invoke-virtual {v3}, Lorg/apache/poi/poifs/filesystem/POIFSDocumentPath;->length()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    .line 324
    .local v4, "pathLength":I
    const/4 v2, 0x0

    .local v2, "k":I
    :goto_0
    if-lt v2, v4, :cond_0

    .line 334
    .end local v0    # "data":[B
    .end local v2    # "k":I
    .end local v4    # "pathLength":I
    :goto_1
    return-void

    .line 324
    .restart local v0    # "data":[B
    .restart local v2    # "k":I
    .restart local v4    # "pathLength":I
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 331
    .end local v0    # "data":[B
    .end local v2    # "k":I
    .end local v4    # "pathLength":I
    :catch_0
    move-exception v5

    goto :goto_1
.end method
