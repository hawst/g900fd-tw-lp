.class public Lorg/apache/poi/poifs/dev/POIFSHeaderDumper;
.super Ljava/lang/Object;
.source "POIFSHeaderDumper.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static displayBATReader(Lorg/apache/poi/poifs/storage/BlockAllocationTableReader;)V
    .locals 3
    .param p0, "batReader"    # Lorg/apache/poi/poifs/storage/BlockAllocationTableReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 139
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-string/jumbo v2, "_entries"

    invoke-virtual {v1, v2}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    .line 140
    .local v0, "entriesF":Ljava/lang/reflect/Field;
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 160
    return-void
.end method

.method public static displayHeader(Lorg/apache/poi/poifs/storage/HeaderBlock;)V
    .locals 0
    .param p0, "header_block"    # Lorg/apache/poi/poifs/storage/HeaderBlock;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 116
    return-void
.end method

.method public static displayRawBlocksSummary(Lorg/apache/poi/poifs/storage/RawDataBlockList;)V
    .locals 10
    .param p0, "data_blocks"    # Lorg/apache/poi/poifs/storage/RawDataBlockList;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 122
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object v4

    const-string/jumbo v5, "get"

    new-array v6, v9, [Ljava/lang/Class;

    sget-object v7, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v7, v6, v8

    invoke-virtual {v4, v5, v6}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    .line 123
    .local v2, "gbm":Ljava/lang/reflect/Method;
    invoke-virtual {v2, v9}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 125
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    const/16 v4, 0x10

    invoke-virtual {p0}, Lorg/apache/poi/poifs/storage/RawDataBlockList;->blockCount()I

    move-result v5

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v4

    if-lt v3, v4, :cond_0

    .line 135
    return-void

    .line 126
    :cond_0
    new-array v4, v9, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v8

    invoke-virtual {v2, p0, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/poifs/storage/ListManagedBlock;

    .line 127
    .local v0, "block":Lorg/apache/poi/poifs/storage/ListManagedBlock;
    const/16 v4, 0x30

    invoke-interface {v0}, Lorg/apache/poi/poifs/storage/ListManagedBlock;->getData()[B

    move-result-object v5

    array-length v5, v5

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v4

    new-array v1, v4, [B

    .line 128
    .local v1, "data":[B
    invoke-interface {v0}, Lorg/apache/poi/poifs/storage/ListManagedBlock;->getData()[B

    move-result-object v4

    array-length v5, v1

    invoke-static {v4, v8, v1, v8, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 125
    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method

.method public static main([Ljava/lang/String;)V
    .locals 3
    .param p0, "args"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 48
    array-length v1, p0

    if-nez v1, :cond_0

    .line 49
    sget-object v1, Ljava/lang/System;->err:Ljava/io/PrintStream;

    const-string/jumbo v2, "Must specify at least one file to view"

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 50
    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/System;->exit(I)V

    .line 53
    :cond_0
    const/4 v0, 0x0

    .local v0, "j":I
    :goto_0
    array-length v1, p0

    if-lt v0, v1, :cond_1

    .line 56
    return-void

    .line 54
    :cond_1
    aget-object v1, p0, v0

    invoke-static {v1}, Lorg/apache/poi/poifs/dev/POIFSHeaderDumper;->viewFile(Ljava/lang/String;)V

    .line 53
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public static viewFile(Ljava/lang/String;)V
    .locals 13
    .param p0, "filename"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 60
    const/4 v11, 0x0

    .line 61
    .local v11, "inp":Ljava/io/InputStream;
    const/4 v9, 0x0

    .line 62
    .local v9, "header_block":Lorg/apache/poi/poifs/storage/HeaderBlock;
    const/4 v7, 0x0

    .line 63
    .local v7, "bigBlockSize":Lorg/apache/poi/poifs/common/POIFSBigBlockSize;
    const/4 v8, 0x0

    .line 66
    .local v8, "data_blocks":Lorg/apache/poi/poifs/storage/RawDataBlockList;
    :try_start_0
    new-instance v12, Ljava/io/FileInputStream;

    invoke-direct {v12, p0}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 69
    .end local v11    # "inp":Ljava/io/InputStream;
    .local v12, "inp":Ljava/io/InputStream;
    :try_start_1
    new-instance v10, Lorg/apache/poi/poifs/storage/HeaderBlock;

    invoke-direct {v10, v12}, Lorg/apache/poi/poifs/storage/HeaderBlock;-><init>(Ljava/io/InputStream;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 70
    .end local v9    # "header_block":Lorg/apache/poi/poifs/storage/HeaderBlock;
    .local v10, "header_block":Lorg/apache/poi/poifs/storage/HeaderBlock;
    :try_start_2
    invoke-static {v10}, Lorg/apache/poi/poifs/dev/POIFSHeaderDumper;->displayHeader(Lorg/apache/poi/poifs/storage/HeaderBlock;)V

    .line 73
    invoke-virtual {v10}, Lorg/apache/poi/poifs/storage/HeaderBlock;->getBigBlockSize()Lorg/apache/poi/poifs/common/POIFSBigBlockSize;

    move-result-object v7

    .line 74
    new-instance v6, Lorg/apache/poi/poifs/storage/RawDataBlockList;

    invoke-direct {v6, v12, v7}, Lorg/apache/poi/poifs/storage/RawDataBlockList;-><init>(Ljava/io/InputStream;Lorg/apache/poi/poifs/common/POIFSBigBlockSize;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 75
    .end local v8    # "data_blocks":Lorg/apache/poi/poifs/storage/RawDataBlockList;
    .local v6, "data_blocks":Lorg/apache/poi/poifs/storage/RawDataBlockList;
    :try_start_3
    invoke-static {v6}, Lorg/apache/poi/poifs/dev/POIFSHeaderDumper;->displayRawBlocksSummary(Lorg/apache/poi/poifs/storage/RawDataBlockList;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    .line 78
    if-eqz v12, :cond_0

    .line 79
    invoke-virtual {v12}, Ljava/io/InputStream;->close()V

    .line 83
    :cond_0
    new-instance v0, Lorg/apache/poi/poifs/storage/BlockAllocationTableReader;

    .line 84
    invoke-virtual {v10}, Lorg/apache/poi/poifs/storage/HeaderBlock;->getBigBlockSize()Lorg/apache/poi/poifs/common/POIFSBigBlockSize;

    move-result-object v1

    .line 85
    invoke-virtual {v10}, Lorg/apache/poi/poifs/storage/HeaderBlock;->getBATCount()I

    move-result v2

    .line 86
    invoke-virtual {v10}, Lorg/apache/poi/poifs/storage/HeaderBlock;->getBATArray()[I

    move-result-object v3

    .line 87
    invoke-virtual {v10}, Lorg/apache/poi/poifs/storage/HeaderBlock;->getXBATCount()I

    move-result v4

    .line 88
    invoke-virtual {v10}, Lorg/apache/poi/poifs/storage/HeaderBlock;->getXBATIndex()I

    move-result v5

    .line 83
    invoke-direct/range {v0 .. v6}, Lorg/apache/poi/poifs/storage/BlockAllocationTableReader;-><init>(Lorg/apache/poi/poifs/common/POIFSBigBlockSize;I[IIILorg/apache/poi/poifs/storage/BlockList;)V

    .line 90
    .local v0, "batReader":Lorg/apache/poi/poifs/storage/BlockAllocationTableReader;
    invoke-static {v0}, Lorg/apache/poi/poifs/dev/POIFSHeaderDumper;->displayBATReader(Lorg/apache/poi/poifs/storage/BlockAllocationTableReader;)V

    .line 103
    return-void

    .line 77
    .end local v0    # "batReader":Lorg/apache/poi/poifs/storage/BlockAllocationTableReader;
    .end local v6    # "data_blocks":Lorg/apache/poi/poifs/storage/RawDataBlockList;
    .end local v10    # "header_block":Lorg/apache/poi/poifs/storage/HeaderBlock;
    .end local v12    # "inp":Ljava/io/InputStream;
    .restart local v8    # "data_blocks":Lorg/apache/poi/poifs/storage/RawDataBlockList;
    .restart local v9    # "header_block":Lorg/apache/poi/poifs/storage/HeaderBlock;
    .restart local v11    # "inp":Ljava/io/InputStream;
    :catchall_0
    move-exception v1

    move-object v6, v8

    .line 78
    .end local v8    # "data_blocks":Lorg/apache/poi/poifs/storage/RawDataBlockList;
    .restart local v6    # "data_blocks":Lorg/apache/poi/poifs/storage/RawDataBlockList;
    :goto_0
    if-eqz v11, :cond_1

    .line 79
    invoke-virtual {v11}, Ljava/io/InputStream;->close()V

    .line 80
    :cond_1
    throw v1

    .line 77
    .end local v6    # "data_blocks":Lorg/apache/poi/poifs/storage/RawDataBlockList;
    .end local v11    # "inp":Ljava/io/InputStream;
    .restart local v8    # "data_blocks":Lorg/apache/poi/poifs/storage/RawDataBlockList;
    .restart local v12    # "inp":Ljava/io/InputStream;
    :catchall_1
    move-exception v1

    move-object v6, v8

    .end local v8    # "data_blocks":Lorg/apache/poi/poifs/storage/RawDataBlockList;
    .restart local v6    # "data_blocks":Lorg/apache/poi/poifs/storage/RawDataBlockList;
    move-object v11, v12

    .end local v12    # "inp":Ljava/io/InputStream;
    .restart local v11    # "inp":Ljava/io/InputStream;
    goto :goto_0

    .end local v6    # "data_blocks":Lorg/apache/poi/poifs/storage/RawDataBlockList;
    .end local v9    # "header_block":Lorg/apache/poi/poifs/storage/HeaderBlock;
    .end local v11    # "inp":Ljava/io/InputStream;
    .restart local v8    # "data_blocks":Lorg/apache/poi/poifs/storage/RawDataBlockList;
    .restart local v10    # "header_block":Lorg/apache/poi/poifs/storage/HeaderBlock;
    .restart local v12    # "inp":Ljava/io/InputStream;
    :catchall_2
    move-exception v1

    move-object v6, v8

    .end local v8    # "data_blocks":Lorg/apache/poi/poifs/storage/RawDataBlockList;
    .restart local v6    # "data_blocks":Lorg/apache/poi/poifs/storage/RawDataBlockList;
    move-object v9, v10

    .end local v10    # "header_block":Lorg/apache/poi/poifs/storage/HeaderBlock;
    .restart local v9    # "header_block":Lorg/apache/poi/poifs/storage/HeaderBlock;
    move-object v11, v12

    .end local v12    # "inp":Ljava/io/InputStream;
    .restart local v11    # "inp":Ljava/io/InputStream;
    goto :goto_0

    .end local v9    # "header_block":Lorg/apache/poi/poifs/storage/HeaderBlock;
    .end local v11    # "inp":Ljava/io/InputStream;
    .restart local v10    # "header_block":Lorg/apache/poi/poifs/storage/HeaderBlock;
    .restart local v12    # "inp":Ljava/io/InputStream;
    :catchall_3
    move-exception v1

    move-object v9, v10

    .end local v10    # "header_block":Lorg/apache/poi/poifs/storage/HeaderBlock;
    .restart local v9    # "header_block":Lorg/apache/poi/poifs/storage/HeaderBlock;
    move-object v11, v12

    .end local v12    # "inp":Ljava/io/InputStream;
    .restart local v11    # "inp":Ljava/io/InputStream;
    goto :goto_0
.end method
