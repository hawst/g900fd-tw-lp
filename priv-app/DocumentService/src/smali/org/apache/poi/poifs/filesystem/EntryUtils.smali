.class public Lorg/apache/poi/poifs/filesystem/EntryUtils;
.super Ljava/lang/Object;
.source "EntryUtils.java"


# annotations
.annotation runtime Lorg/apache/poi/util/Internal;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static areDirectoriesIdentical(Lorg/apache/poi/poifs/filesystem/DirectoryEntry;Lorg/apache/poi/poifs/filesystem/DirectoryEntry;)Z
    .locals 12
    .param p0, "dirA"    # Lorg/apache/poi/poifs/filesystem/DirectoryEntry;
    .param p1, "dirB"    # Lorg/apache/poi/poifs/filesystem/DirectoryEntry;

    .prologue
    const/4 v10, 0x0

    .line 168
    invoke-interface {p0}, Lorg/apache/poi/poifs/filesystem/DirectoryEntry;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-interface {p1}, Lorg/apache/poi/poifs/filesystem/DirectoryEntry;->getName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v9, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_0

    move v9, v10

    .line 237
    :goto_0
    return v9

    .line 173
    :cond_0
    invoke-interface {p0}, Lorg/apache/poi/poifs/filesystem/DirectoryEntry;->getEntryCount()I

    move-result v9

    invoke-interface {p1}, Lorg/apache/poi/poifs/filesystem/DirectoryEntry;->getEntryCount()I

    move-result v11

    if-eq v9, v11, :cond_1

    move v9, v10

    .line 174
    goto :goto_0

    .line 178
    :cond_1
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 179
    .local v2, "aSizes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;"
    const/16 v6, -0x3039

    .line 180
    .local v6, "isDirectory":I
    invoke-interface {p0}, Lorg/apache/poi/poifs/filesystem/DirectoryEntry;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-nez v11, :cond_2

    .line 188
    invoke-interface {p1}, Lorg/apache/poi/poifs/filesystem/DirectoryEntry;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :goto_2
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-nez v9, :cond_4

    .line 209
    invoke-interface {v2}, Ljava/util/Map;->isEmpty()Z

    move-result v9

    if-nez v9, :cond_8

    move v9, v10

    .line 211
    goto :goto_0

    .line 180
    :cond_2
    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/poifs/filesystem/Entry;

    .line 181
    .local v0, "a":Lorg/apache/poi/poifs/filesystem/Entry;
    invoke-interface {v0}, Lorg/apache/poi/poifs/filesystem/Entry;->getName()Ljava/lang/String;

    move-result-object v1

    .line 182
    .local v1, "aName":Ljava/lang/String;
    invoke-interface {v0}, Lorg/apache/poi/poifs/filesystem/Entry;->isDirectoryEntry()Z

    move-result v11

    if-eqz v11, :cond_3

    .line 183
    const/16 v11, -0x3039

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-interface {v2, v1, v11}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 185
    :cond_3
    check-cast v0, Lorg/apache/poi/poifs/filesystem/DocumentNode;

    .end local v0    # "a":Lorg/apache/poi/poifs/filesystem/Entry;
    invoke-virtual {v0}, Lorg/apache/poi/poifs/filesystem/DocumentNode;->getSize()I

    move-result v11

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-interface {v2, v1, v11}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 188
    .end local v1    # "aName":Ljava/lang/String;
    :cond_4
    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/poi/poifs/filesystem/Entry;

    .line 189
    .local v3, "b":Lorg/apache/poi/poifs/filesystem/Entry;
    invoke-interface {v3}, Lorg/apache/poi/poifs/filesystem/Entry;->getName()Ljava/lang/String;

    move-result-object v4

    .line 190
    .local v4, "bName":Ljava/lang/String;
    invoke-interface {v2, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_5

    move v9, v10

    .line 192
    goto :goto_0

    .line 196
    :cond_5
    invoke-interface {v3}, Lorg/apache/poi/poifs/filesystem/Entry;->isDirectoryEntry()Z

    move-result v9

    if-eqz v9, :cond_6

    .line 197
    const/16 v8, -0x3039

    .line 201
    .end local v3    # "b":Lorg/apache/poi/poifs/filesystem/Entry;
    .local v8, "size":I
    :goto_3
    invoke-interface {v2, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Integer;

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v9

    if-eq v8, v9, :cond_7

    move v9, v10

    .line 203
    goto :goto_0

    .line 199
    .end local v8    # "size":I
    .restart local v3    # "b":Lorg/apache/poi/poifs/filesystem/Entry;
    :cond_6
    check-cast v3, Lorg/apache/poi/poifs/filesystem/DocumentNode;

    .end local v3    # "b":Lorg/apache/poi/poifs/filesystem/Entry;
    invoke-virtual {v3}, Lorg/apache/poi/poifs/filesystem/DocumentNode;->getSize()I

    move-result v8

    .restart local v8    # "size":I
    goto :goto_3

    .line 207
    :cond_7
    invoke-interface {v2, v4}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 215
    .end local v4    # "bName":Ljava/lang/String;
    .end local v8    # "size":I
    :cond_8
    invoke-interface {p0}, Lorg/apache/poi/poifs/filesystem/DirectoryEntry;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_9
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-nez v11, :cond_a

    .line 237
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 215
    :cond_a
    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/poifs/filesystem/Entry;

    .line 217
    .restart local v0    # "a":Lorg/apache/poi/poifs/filesystem/Entry;
    :try_start_0
    invoke-interface {v0}, Lorg/apache/poi/poifs/filesystem/Entry;->getName()Ljava/lang/String;

    move-result-object v11

    invoke-interface {p1, v11}, Lorg/apache/poi/poifs/filesystem/DirectoryEntry;->getEntry(Ljava/lang/String;)Lorg/apache/poi/poifs/filesystem/Entry;

    move-result-object v3

    .line 219
    .restart local v3    # "b":Lorg/apache/poi/poifs/filesystem/Entry;
    invoke-interface {v0}, Lorg/apache/poi/poifs/filesystem/Entry;->isDirectoryEntry()Z

    move-result v11

    if-eqz v11, :cond_b

    .line 221
    check-cast v0, Lorg/apache/poi/poifs/filesystem/DirectoryEntry;

    .end local v0    # "a":Lorg/apache/poi/poifs/filesystem/Entry;
    check-cast v3, Lorg/apache/poi/poifs/filesystem/DirectoryEntry;

    .line 220
    .end local v3    # "b":Lorg/apache/poi/poifs/filesystem/Entry;
    invoke-static {v0, v3}, Lorg/apache/poi/poifs/filesystem/EntryUtils;->areDirectoriesIdentical(Lorg/apache/poi/poifs/filesystem/DirectoryEntry;Lorg/apache/poi/poifs/filesystem/DirectoryEntry;)Z

    move-result v7

    .line 226
    .local v7, "match":Z
    :goto_4
    if-nez v7, :cond_9

    move v9, v10

    goto/16 :goto_0

    .line 224
    .end local v7    # "match":Z
    .restart local v0    # "a":Lorg/apache/poi/poifs/filesystem/Entry;
    .restart local v3    # "b":Lorg/apache/poi/poifs/filesystem/Entry;
    :cond_b
    check-cast v0, Lorg/apache/poi/poifs/filesystem/DocumentEntry;

    .end local v0    # "a":Lorg/apache/poi/poifs/filesystem/Entry;
    check-cast v3, Lorg/apache/poi/poifs/filesystem/DocumentEntry;

    .line 223
    .end local v3    # "b":Lorg/apache/poi/poifs/filesystem/Entry;
    invoke-static {v0, v3}, Lorg/apache/poi/poifs/filesystem/EntryUtils;->areDocumentsIdentical(Lorg/apache/poi/poifs/filesystem/DocumentEntry;Lorg/apache/poi/poifs/filesystem/DocumentEntry;)Z
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v7

    .restart local v7    # "match":Z
    goto :goto_4

    .line 227
    .end local v7    # "match":Z
    :catch_0
    move-exception v5

    .local v5, "e":Ljava/io/FileNotFoundException;
    move v9, v10

    .line 229
    goto/16 :goto_0

    .line 230
    .end local v5    # "e":Ljava/io/FileNotFoundException;
    :catch_1
    move-exception v5

    .local v5, "e":Ljava/io/IOException;
    move v9, v10

    .line 232
    goto/16 :goto_0
.end method

.method public static areDocumentsIdentical(Lorg/apache/poi/poifs/filesystem/DocumentEntry;Lorg/apache/poi/poifs/filesystem/DocumentEntry;)Z
    .locals 10
    .param p0, "docA"    # Lorg/apache/poi/poifs/filesystem/DocumentEntry;
    .param p1, "docB"    # Lorg/apache/poi/poifs/filesystem/DocumentEntry;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    const/4 v9, -0x1

    .line 246
    invoke-interface {p0}, Lorg/apache/poi/poifs/filesystem/DocumentEntry;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-interface {p1}, Lorg/apache/poi/poifs/filesystem/DocumentEntry;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 275
    :cond_0
    :goto_0
    return v4

    .line 250
    :cond_1
    invoke-interface {p0}, Lorg/apache/poi/poifs/filesystem/DocumentEntry;->getSize()I

    move-result v7

    invoke-interface {p1}, Lorg/apache/poi/poifs/filesystem/DocumentEntry;->getSize()I

    move-result v8

    if-ne v7, v8, :cond_0

    .line 255
    const/4 v4, 0x1

    .line 256
    .local v4, "matches":Z
    const/4 v0, 0x0

    .local v0, "inpA":Lorg/apache/poi/poifs/filesystem/DocumentInputStream;
    const/4 v2, 0x0

    .line 258
    .local v2, "inpB":Lorg/apache/poi/poifs/filesystem/DocumentInputStream;
    :try_start_0
    new-instance v1, Lorg/apache/poi/poifs/filesystem/DocumentInputStream;

    invoke-direct {v1, p0}, Lorg/apache/poi/poifs/filesystem/DocumentInputStream;-><init>(Lorg/apache/poi/poifs/filesystem/DocumentEntry;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 259
    .end local v0    # "inpA":Lorg/apache/poi/poifs/filesystem/DocumentInputStream;
    .local v1, "inpA":Lorg/apache/poi/poifs/filesystem/DocumentInputStream;
    :try_start_1
    new-instance v3, Lorg/apache/poi/poifs/filesystem/DocumentInputStream;

    invoke-direct {v3, p1}, Lorg/apache/poi/poifs/filesystem/DocumentInputStream;-><init>(Lorg/apache/poi/poifs/filesystem/DocumentEntry;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 263
    .end local v2    # "inpB":Lorg/apache/poi/poifs/filesystem/DocumentInputStream;
    .local v3, "inpB":Lorg/apache/poi/poifs/filesystem/DocumentInputStream;
    :cond_2
    :try_start_2
    invoke-virtual {v1}, Lorg/apache/poi/poifs/filesystem/DocumentInputStream;->read()I

    move-result v5

    .line 264
    .local v5, "readA":I
    invoke-virtual {v3}, Lorg/apache/poi/poifs/filesystem/DocumentInputStream;->read()I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    move-result v6

    .line 265
    .local v6, "readB":I
    if-eq v5, v6, :cond_5

    .line 266
    const/4 v4, 0x0

    .line 271
    :cond_3
    :goto_1
    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lorg/apache/poi/poifs/filesystem/DocumentInputStream;->close()V

    .line 272
    :cond_4
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Lorg/apache/poi/poifs/filesystem/DocumentInputStream;->close()V

    goto :goto_0

    .line 269
    :cond_5
    if-eq v5, v9, :cond_3

    if-ne v6, v9, :cond_2

    goto :goto_1

    .line 270
    .end local v1    # "inpA":Lorg/apache/poi/poifs/filesystem/DocumentInputStream;
    .end local v3    # "inpB":Lorg/apache/poi/poifs/filesystem/DocumentInputStream;
    .end local v5    # "readA":I
    .end local v6    # "readB":I
    .restart local v0    # "inpA":Lorg/apache/poi/poifs/filesystem/DocumentInputStream;
    .restart local v2    # "inpB":Lorg/apache/poi/poifs/filesystem/DocumentInputStream;
    :catchall_0
    move-exception v7

    .line 271
    :goto_2
    if-eqz v0, :cond_6

    invoke-virtual {v0}, Lorg/apache/poi/poifs/filesystem/DocumentInputStream;->close()V

    .line 272
    :cond_6
    if-eqz v2, :cond_7

    invoke-virtual {v2}, Lorg/apache/poi/poifs/filesystem/DocumentInputStream;->close()V

    .line 273
    :cond_7
    throw v7

    .line 270
    .end local v0    # "inpA":Lorg/apache/poi/poifs/filesystem/DocumentInputStream;
    .restart local v1    # "inpA":Lorg/apache/poi/poifs/filesystem/DocumentInputStream;
    :catchall_1
    move-exception v7

    move-object v0, v1

    .end local v1    # "inpA":Lorg/apache/poi/poifs/filesystem/DocumentInputStream;
    .restart local v0    # "inpA":Lorg/apache/poi/poifs/filesystem/DocumentInputStream;
    goto :goto_2

    .end local v0    # "inpA":Lorg/apache/poi/poifs/filesystem/DocumentInputStream;
    .end local v2    # "inpB":Lorg/apache/poi/poifs/filesystem/DocumentInputStream;
    .restart local v1    # "inpA":Lorg/apache/poi/poifs/filesystem/DocumentInputStream;
    .restart local v3    # "inpB":Lorg/apache/poi/poifs/filesystem/DocumentInputStream;
    :catchall_2
    move-exception v7

    move-object v2, v3

    .end local v3    # "inpB":Lorg/apache/poi/poifs/filesystem/DocumentInputStream;
    .restart local v2    # "inpB":Lorg/apache/poi/poifs/filesystem/DocumentInputStream;
    move-object v0, v1

    .end local v1    # "inpA":Lorg/apache/poi/poifs/filesystem/DocumentInputStream;
    .restart local v0    # "inpA":Lorg/apache/poi/poifs/filesystem/DocumentInputStream;
    goto :goto_2
.end method

.method public static copyNodeRecursively(Lorg/apache/poi/poifs/filesystem/Entry;Lorg/apache/poi/poifs/filesystem/DirectoryEntry;)V
    .locals 5
    .param p0, "entry"    # Lorg/apache/poi/poifs/filesystem/Entry;
    .param p1, "target"    # Lorg/apache/poi/poifs/filesystem/DirectoryEntry;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .prologue
    .line 41
    const/4 v3, 0x0

    .line 42
    .local v3, "newTarget":Lorg/apache/poi/poifs/filesystem/DirectoryEntry;
    invoke-interface {p0}, Lorg/apache/poi/poifs/filesystem/Entry;->isDirectoryEntry()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 44
    invoke-interface {p0}, Lorg/apache/poi/poifs/filesystem/Entry;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-interface {p1, v4}, Lorg/apache/poi/poifs/filesystem/DirectoryEntry;->createDirectory(Ljava/lang/String;)Lorg/apache/poi/poifs/filesystem/DirectoryEntry;

    move-result-object v3

    .line 45
    check-cast p0, Lorg/apache/poi/poifs/filesystem/DirectoryEntry;

    .end local p0    # "entry":Lorg/apache/poi/poifs/filesystem/Entry;
    invoke-interface {p0}, Lorg/apache/poi/poifs/filesystem/DirectoryEntry;->getEntries()Ljava/util/Iterator;

    move-result-object v2

    .line 47
    .local v2, "entries":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/poi/poifs/filesystem/Entry;>;"
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_1

    .line 67
    .end local v2    # "entries":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/poi/poifs/filesystem/Entry;>;"
    :cond_0
    :goto_1
    return-void

    .line 49
    .restart local v2    # "entries":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/poi/poifs/filesystem/Entry;>;"
    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/poi/poifs/filesystem/Entry;

    invoke-static {v4, v3}, Lorg/apache/poi/poifs/filesystem/EntryUtils;->copyNodeRecursively(Lorg/apache/poi/poifs/filesystem/Entry;Lorg/apache/poi/poifs/filesystem/DirectoryEntry;)V

    goto :goto_0

    .end local v2    # "entries":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/poi/poifs/filesystem/Entry;>;"
    .restart local p0    # "entry":Lorg/apache/poi/poifs/filesystem/Entry;
    :cond_2
    move-object v0, p0

    .line 54
    check-cast v0, Lorg/apache/poi/poifs/filesystem/DocumentEntry;

    .line 55
    .local v0, "dentry":Lorg/apache/poi/poifs/filesystem/DocumentEntry;
    new-instance v1, Lorg/apache/poi/poifs/filesystem/DocumentInputStream;

    invoke-direct {v1, v0}, Lorg/apache/poi/poifs/filesystem/DocumentInputStream;-><init>(Lorg/apache/poi/poifs/filesystem/DocumentEntry;)V

    .line 59
    .local v1, "dstream":Lorg/apache/poi/poifs/filesystem/DocumentInputStream;
    :try_start_0
    invoke-interface {v0}, Lorg/apache/poi/poifs/filesystem/DocumentEntry;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-interface {p1, v4, v1}, Lorg/apache/poi/poifs/filesystem/DirectoryEntry;->createDocument(Ljava/lang/String;Ljava/io/InputStream;)Lorg/apache/poi/poifs/filesystem/DocumentEntry;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 63
    if-eqz v1, :cond_0

    .line 64
    invoke-virtual {v1}, Lorg/apache/poi/poifs/filesystem/DocumentInputStream;->close()V

    goto :goto_1

    .line 62
    :catchall_0
    move-exception v4

    .line 63
    if-eqz v1, :cond_3

    .line 64
    invoke-virtual {v1}, Lorg/apache/poi/poifs/filesystem/DocumentInputStream;->close()V

    .line 65
    :cond_3
    throw v4
.end method

.method public static copyNodes(Lorg/apache/poi/poifs/filesystem/DirectoryEntry;Lorg/apache/poi/poifs/filesystem/DirectoryEntry;)V
    .locals 3
    .param p0, "sourceRoot"    # Lorg/apache/poi/poifs/filesystem/DirectoryEntry;
    .param p1, "targetRoot"    # Lorg/apache/poi/poifs/filesystem/DirectoryEntry;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 80
    invoke-interface {p0}, Lorg/apache/poi/poifs/filesystem/DirectoryEntry;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 83
    return-void

    .line 80
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/poifs/filesystem/Entry;

    .line 81
    .local v0, "entry":Lorg/apache/poi/poifs/filesystem/Entry;
    invoke-static {v0, p1}, Lorg/apache/poi/poifs/filesystem/EntryUtils;->copyNodeRecursively(Lorg/apache/poi/poifs/filesystem/Entry;Lorg/apache/poi/poifs/filesystem/DirectoryEntry;)V

    goto :goto_0
.end method

.method public static copyNodes(Lorg/apache/poi/poifs/filesystem/DirectoryEntry;Lorg/apache/poi/poifs/filesystem/DirectoryEntry;Ljava/util/List;)V
    .locals 3
    .param p0, "sourceRoot"    # Lorg/apache/poi/poifs/filesystem/DirectoryEntry;
    .param p1, "targetRoot"    # Lorg/apache/poi/poifs/filesystem/DirectoryEntry;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/poi/poifs/filesystem/DirectoryEntry;",
            "Lorg/apache/poi/poifs/filesystem/DirectoryEntry;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 114
    .local p2, "excepts":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {p0}, Lorg/apache/poi/poifs/filesystem/DirectoryEntry;->getEntries()Ljava/util/Iterator;

    move-result-object v0

    .line 115
    .local v0, "entries":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/poi/poifs/filesystem/Entry;>;"
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_1

    .line 123
    return-void

    .line 117
    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/poifs/filesystem/Entry;

    .line 118
    .local v1, "entry":Lorg/apache/poi/poifs/filesystem/Entry;
    invoke-interface {v1}, Lorg/apache/poi/poifs/filesystem/Entry;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p2, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 120
    invoke-static {v1, p1}, Lorg/apache/poi/poifs/filesystem/EntryUtils;->copyNodeRecursively(Lorg/apache/poi/poifs/filesystem/Entry;Lorg/apache/poi/poifs/filesystem/DirectoryEntry;)V

    goto :goto_0
.end method

.method public static copyNodes(Lorg/apache/poi/poifs/filesystem/FilteringDirectoryNode;Lorg/apache/poi/poifs/filesystem/FilteringDirectoryNode;)V
    .locals 0
    .param p0, "filteredSource"    # Lorg/apache/poi/poifs/filesystem/FilteringDirectoryNode;
    .param p1, "filteredTarget"    # Lorg/apache/poi/poifs/filesystem/FilteringDirectoryNode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 96
    invoke-static {p0, p1}, Lorg/apache/poi/poifs/filesystem/EntryUtils;->copyNodes(Lorg/apache/poi/poifs/filesystem/DirectoryEntry;Lorg/apache/poi/poifs/filesystem/DirectoryEntry;)V

    .line 97
    return-void
.end method

.method public static copyNodes(Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;)V
    .locals 2
    .param p0, "source"    # Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;
    .param p1, "target"    # Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 136
    invoke-virtual {p0}, Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;->getRoot()Lorg/apache/poi/poifs/filesystem/DirectoryNode;

    move-result-object v0

    invoke-virtual {p1}, Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;->getRoot()Lorg/apache/poi/poifs/filesystem/DirectoryNode;

    move-result-object v1

    invoke-static {v0, v1}, Lorg/apache/poi/poifs/filesystem/EntryUtils;->copyNodes(Lorg/apache/poi/poifs/filesystem/DirectoryEntry;Lorg/apache/poi/poifs/filesystem/DirectoryEntry;)V

    .line 137
    return-void
.end method

.method public static copyNodes(Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;Ljava/util/List;)V
    .locals 3
    .param p0, "source"    # Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;
    .param p1, "target"    # Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;",
            "Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 153
    .local p2, "excepts":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v0, Lorg/apache/poi/poifs/filesystem/FilteringDirectoryNode;

    invoke-virtual {p0}, Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;->getRoot()Lorg/apache/poi/poifs/filesystem/DirectoryNode;

    move-result-object v1

    invoke-direct {v0, v1, p2}, Lorg/apache/poi/poifs/filesystem/FilteringDirectoryNode;-><init>(Lorg/apache/poi/poifs/filesystem/DirectoryEntry;Ljava/util/Collection;)V

    .line 154
    new-instance v1, Lorg/apache/poi/poifs/filesystem/FilteringDirectoryNode;

    invoke-virtual {p1}, Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;->getRoot()Lorg/apache/poi/poifs/filesystem/DirectoryNode;

    move-result-object v2

    invoke-direct {v1, v2, p2}, Lorg/apache/poi/poifs/filesystem/FilteringDirectoryNode;-><init>(Lorg/apache/poi/poifs/filesystem/DirectoryEntry;Ljava/util/Collection;)V

    .line 152
    invoke-static {v0, v1}, Lorg/apache/poi/poifs/filesystem/EntryUtils;->copyNodes(Lorg/apache/poi/poifs/filesystem/FilteringDirectoryNode;Lorg/apache/poi/poifs/filesystem/FilteringDirectoryNode;)V

    .line 156
    return-void
.end method
