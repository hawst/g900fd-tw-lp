.class public abstract Lorg/apache/poi/poifs/crypt/Decryptor;
.super Ljava/lang/Object;
.source "Decryptor.java"


# static fields
.field public static final DEFAULT_PASSWORD:Ljava/lang/String; = "VelvetSweatshop"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method protected static getBlockSize(I)I
    .locals 2
    .param p0, "algorithm"    # I

    .prologue
    .line 89
    packed-switch p0, :pswitch_data_0

    .line 94
    new-instance v0, Lorg/apache/poi/EncryptedDocumentException;

    const-string/jumbo v1, "Unknown block size"

    invoke-direct {v0, v1}, Lorg/apache/poi/EncryptedDocumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 90
    :pswitch_0
    const/16 v0, 0x10

    .line 92
    :goto_0
    return v0

    .line 91
    :pswitch_1
    const/16 v0, 0x18

    goto :goto_0

    .line 92
    :pswitch_2
    const/16 v0, 0x20

    goto :goto_0

    .line 89
    nop

    :pswitch_data_0
    .packed-switch 0x660e
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static getInstance(Lorg/apache/poi/poifs/crypt/EncryptionInfo;)Lorg/apache/poi/poifs/crypt/Decryptor;
    .locals 4
    .param p0, "info"    # Lorg/apache/poi/poifs/crypt/EncryptionInfo;

    .prologue
    const/4 v3, 0x4

    .line 69
    invoke-virtual {p0}, Lorg/apache/poi/poifs/crypt/EncryptionInfo;->getVersionMajor()I

    move-result v0

    .line 70
    .local v0, "major":I
    invoke-virtual {p0}, Lorg/apache/poi/poifs/crypt/EncryptionInfo;->getVersionMinor()I

    move-result v1

    .line 72
    .local v1, "minor":I
    if-ne v0, v3, :cond_0

    if-ne v1, v3, :cond_0

    .line 73
    new-instance v2, Lorg/apache/poi/poifs/crypt/AgileDecryptor;

    invoke-direct {v2, p0}, Lorg/apache/poi/poifs/crypt/AgileDecryptor;-><init>(Lorg/apache/poi/poifs/crypt/EncryptionInfo;)V

    .line 75
    :goto_0
    return-object v2

    .line 74
    :cond_0
    const/4 v2, 0x2

    if-ne v1, v2, :cond_2

    const/4 v2, 0x3

    if-eq v0, v2, :cond_1

    if-ne v0, v3, :cond_2

    .line 75
    :cond_1
    new-instance v2, Lorg/apache/poi/poifs/crypt/EcmaDecryptor;

    invoke-direct {v2, p0}, Lorg/apache/poi/poifs/crypt/EcmaDecryptor;-><init>(Lorg/apache/poi/poifs/crypt/EncryptionInfo;)V

    goto :goto_0

    .line 77
    :cond_2
    new-instance v2, Lorg/apache/poi/EncryptedDocumentException;

    const-string/jumbo v3, "Unsupported version"

    invoke-direct {v2, v3}, Lorg/apache/poi/EncryptedDocumentException;-><init>(Ljava/lang/String;)V

    throw v2
.end method


# virtual methods
.method public abstract getDataStream(Lorg/apache/poi/poifs/filesystem/DirectoryNode;)Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/security/GeneralSecurityException;
        }
    .end annotation
.end method

.method public getDataStream(Lorg/apache/poi/poifs/filesystem/NPOIFSFileSystem;)Ljava/io/InputStream;
    .locals 1
    .param p1, "fs"    # Lorg/apache/poi/poifs/filesystem/NPOIFSFileSystem;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/security/GeneralSecurityException;
        }
    .end annotation

    .prologue
    .line 81
    invoke-virtual {p1}, Lorg/apache/poi/poifs/filesystem/NPOIFSFileSystem;->getRoot()Lorg/apache/poi/poifs/filesystem/DirectoryNode;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/apache/poi/poifs/crypt/Decryptor;->getDataStream(Lorg/apache/poi/poifs/filesystem/DirectoryNode;)Ljava/io/InputStream;

    move-result-object v0

    return-object v0
.end method

.method public getDataStream(Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;)Ljava/io/InputStream;
    .locals 1
    .param p1, "fs"    # Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/security/GeneralSecurityException;
        }
    .end annotation

    .prologue
    .line 85
    invoke-virtual {p1}, Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;->getRoot()Lorg/apache/poi/poifs/filesystem/DirectoryNode;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/apache/poi/poifs/crypt/Decryptor;->getDataStream(Lorg/apache/poi/poifs/filesystem/DirectoryNode;)Ljava/io/InputStream;

    move-result-object v0

    return-object v0
.end method

.method public abstract getLength()J
.end method

.method protected hashPassword(Lorg/apache/poi/poifs/crypt/EncryptionInfo;Ljava/lang/String;)[B
    .locals 6
    .param p1, "info"    # Lorg/apache/poi/poifs/crypt/EncryptionInfo;
    .param p2, "password"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/NoSuchAlgorithmException;
        }
    .end annotation

    .prologue
    .line 99
    const-string/jumbo v4, "SHA-1"

    invoke-static {v4}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v3

    .line 102
    .local v3, "sha1":Ljava/security/MessageDigest;
    :try_start_0
    const-string/jumbo v4, "UTF-16LE"

    invoke-virtual {p2, v4}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 108
    .local v0, "bytes":[B
    invoke-virtual {v3, v0}, Ljava/security/MessageDigest;->digest([B)[B

    move-result-object v2

    .line 119
    .local v2, "hash":[B
    return-object v2

    .line 103
    .end local v0    # "bytes":[B
    .end local v2    # "hash":[B
    :catch_0
    move-exception v1

    .line 104
    .local v1, "e":Ljava/io/UnsupportedEncodingException;
    new-instance v4, Lorg/apache/poi/EncryptedDocumentException;

    const-string/jumbo v5, "UTF16 not supported"

    invoke-direct {v4, v5}, Lorg/apache/poi/EncryptedDocumentException;-><init>(Ljava/lang/String;)V

    throw v4
.end method

.method public abstract verifyPassword(Ljava/lang/String;)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/GeneralSecurityException;
        }
    .end annotation
.end method
