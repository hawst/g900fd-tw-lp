.class public Lorg/apache/poi/poifs/dev/POIFSDump;
.super Ljava/lang/Object;
.source "POIFSDump.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static dump(Lorg/apache/poi/poifs/filesystem/DirectoryEntry;Ljava/io/File;)V
    .locals 13
    .param p0, "root"    # Lorg/apache/poi/poifs/filesystem/DirectoryEntry;
    .param p1, "parent"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 52
    invoke-interface {p0}, Lorg/apache/poi/poifs/filesystem/DirectoryEntry;->getEntries()Ljava/util/Iterator;

    move-result-object v6

    .local v6, "it":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-nez v10, :cond_1

    .line 87
    return-void

    .line 53
    :cond_1
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/poi/poifs/filesystem/Entry;

    .line 55
    .local v2, "entry":Lorg/apache/poi/poifs/filesystem/Entry;
    const/4 v0, 0x0

    .line 56
    .local v0, "bytes":[B
    instance-of v10, v2, Lorg/apache/poi/poifs/filesystem/DocumentNode;

    if-eqz v10, :cond_5

    move-object v7, v2

    .line 57
    check-cast v7, Lorg/apache/poi/poifs/filesystem/DocumentNode;

    .line 58
    .local v7, "node":Lorg/apache/poi/poifs/filesystem/DocumentNode;
    const/4 v4, 0x0

    .line 60
    .local v4, "is":Lorg/apache/poi/poifs/filesystem/DocumentInputStream;
    :try_start_0
    new-instance v5, Lorg/apache/poi/poifs/filesystem/DocumentInputStream;

    invoke-direct {v5, v7}, Lorg/apache/poi/poifs/filesystem/DocumentInputStream;-><init>(Lorg/apache/poi/poifs/filesystem/DocumentEntry;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 61
    .end local v4    # "is":Lorg/apache/poi/poifs/filesystem/DocumentInputStream;
    .local v5, "is":Lorg/apache/poi/poifs/filesystem/DocumentInputStream;
    :try_start_1
    invoke-virtual {v7}, Lorg/apache/poi/poifs/filesystem/DocumentNode;->getSize()I

    move-result v10

    new-array v0, v10, [B

    .line 62
    invoke-virtual {v5, v0}, Lorg/apache/poi/poifs/filesystem/DocumentInputStream;->read([B)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_3

    .line 65
    if-eqz v5, :cond_2

    .line 66
    invoke-virtual {v5}, Lorg/apache/poi/poifs/filesystem/DocumentInputStream;->close()V

    .line 69
    :cond_2
    const/4 v8, 0x0

    .line 71
    .local v8, "out":Ljava/io/FileOutputStream;
    :try_start_2
    new-instance v9, Ljava/io/FileOutputStream;

    new-instance v10, Ljava/io/File;

    invoke-virtual {v7}, Lorg/apache/poi/poifs/filesystem/DocumentNode;->getName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, p1, v11}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-direct {v9, v10}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 72
    .end local v8    # "out":Ljava/io/FileOutputStream;
    .local v9, "out":Ljava/io/FileOutputStream;
    :try_start_3
    invoke-virtual {v9, v0}, Ljava/io/FileOutputStream;->write([B)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 75
    if-eqz v9, :cond_0

    .line 76
    invoke-virtual {v9}, Ljava/io/FileOutputStream;->close()V

    goto :goto_0

    .line 64
    .end local v5    # "is":Lorg/apache/poi/poifs/filesystem/DocumentInputStream;
    .end local v9    # "out":Ljava/io/FileOutputStream;
    .restart local v4    # "is":Lorg/apache/poi/poifs/filesystem/DocumentInputStream;
    :catchall_0
    move-exception v10

    .line 65
    :goto_1
    if-eqz v4, :cond_3

    .line 66
    invoke-virtual {v4}, Lorg/apache/poi/poifs/filesystem/DocumentInputStream;->close()V

    .line 67
    :cond_3
    throw v10

    .line 74
    .end local v4    # "is":Lorg/apache/poi/poifs/filesystem/DocumentInputStream;
    .restart local v5    # "is":Lorg/apache/poi/poifs/filesystem/DocumentInputStream;
    .restart local v8    # "out":Ljava/io/FileOutputStream;
    :catchall_1
    move-exception v10

    .line 75
    :goto_2
    if-eqz v8, :cond_4

    .line 76
    invoke-virtual {v8}, Ljava/io/FileOutputStream;->close()V

    .line 77
    :cond_4
    throw v10

    .line 78
    .end local v5    # "is":Lorg/apache/poi/poifs/filesystem/DocumentInputStream;
    .end local v7    # "node":Lorg/apache/poi/poifs/filesystem/DocumentNode;
    .end local v8    # "out":Ljava/io/FileOutputStream;
    :cond_5
    instance-of v10, v2, Lorg/apache/poi/poifs/filesystem/DirectoryEntry;

    if-eqz v10, :cond_6

    move-object v1, v2

    .line 79
    check-cast v1, Lorg/apache/poi/poifs/filesystem/DirectoryEntry;

    .line 80
    .local v1, "dir":Lorg/apache/poi/poifs/filesystem/DirectoryEntry;
    new-instance v3, Ljava/io/File;

    invoke-interface {v2}, Lorg/apache/poi/poifs/filesystem/Entry;->getName()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v3, p1, v10}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 81
    .local v3, "file":Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->mkdir()Z

    .line 82
    invoke-static {v1, v3}, Lorg/apache/poi/poifs/dev/POIFSDump;->dump(Lorg/apache/poi/poifs/filesystem/DirectoryEntry;Ljava/io/File;)V

    goto :goto_0

    .line 84
    .end local v1    # "dir":Lorg/apache/poi/poifs/filesystem/DirectoryEntry;
    .end local v3    # "file":Ljava/io/File;
    :cond_6
    sget-object v10, Ljava/lang/System;->err:Ljava/io/PrintStream;

    new-instance v11, Ljava/lang/StringBuilder;

    const-string/jumbo v12, "Skipping unsupported POIFS entry: "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0

    .line 74
    .restart local v5    # "is":Lorg/apache/poi/poifs/filesystem/DocumentInputStream;
    .restart local v7    # "node":Lorg/apache/poi/poifs/filesystem/DocumentNode;
    .restart local v9    # "out":Ljava/io/FileOutputStream;
    :catchall_2
    move-exception v10

    move-object v8, v9

    .end local v9    # "out":Ljava/io/FileOutputStream;
    .restart local v8    # "out":Ljava/io/FileOutputStream;
    goto :goto_2

    .line 64
    .end local v8    # "out":Ljava/io/FileOutputStream;
    :catchall_3
    move-exception v10

    move-object v4, v5

    .end local v5    # "is":Lorg/apache/poi/poifs/filesystem/DocumentInputStream;
    .restart local v4    # "is":Lorg/apache/poi/poifs/filesystem/DocumentInputStream;
    goto :goto_1
.end method

.method public static main([Ljava/lang/String;)V
    .locals 6
    .param p0, "args"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 36
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v5, p0

    if-lt v2, v5, :cond_0

    .line 48
    return-void

    .line 38
    :cond_0
    new-instance v3, Ljava/io/FileInputStream;

    aget-object v5, p0, v2

    invoke-direct {v3, v5}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    .line 39
    .local v3, "is":Ljava/io/FileInputStream;
    new-instance v1, Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;

    invoke-direct {v1, v3}, Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;-><init>(Ljava/io/InputStream;)V

    .line 40
    .local v1, "fs":Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V

    .line 42
    invoke-virtual {v1}, Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;->getRoot()Lorg/apache/poi/poifs/filesystem/DirectoryNode;

    move-result-object v4

    .line 43
    .local v4, "root":Lorg/apache/poi/poifs/filesystem/DirectoryEntry;
    new-instance v0, Ljava/io/File;

    invoke-interface {v4}, Lorg/apache/poi/poifs/filesystem/DirectoryEntry;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v0, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 44
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->mkdir()Z

    .line 46
    invoke-static {v4, v0}, Lorg/apache/poi/poifs/dev/POIFSDump;->dump(Lorg/apache/poi/poifs/filesystem/DirectoryEntry;Ljava/io/File;)V

    .line 36
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method
