.class Lorg/apache/poi/poifs/crypt/AgileDecryptor$ChunkedCipherInputStream;
.super Ljava/io/InputStream;
.source "AgileDecryptor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/poi/poifs/crypt/AgileDecryptor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ChunkedCipherInputStream"
.end annotation


# instance fields
.field private _chunk:[B

.field private _cipher:Ljavax/crypto/Cipher;

.field private _lastIndex:I

.field private _pos:J

.field private final _size:J

.field private final _stream:Lorg/apache/poi/poifs/filesystem/DocumentInputStream;

.field final synthetic this$0:Lorg/apache/poi/poifs/crypt/AgileDecryptor;


# direct methods
.method public constructor <init>(Lorg/apache/poi/poifs/crypt/AgileDecryptor;Lorg/apache/poi/poifs/filesystem/DocumentInputStream;J)V
    .locals 5
    .param p2, "stream"    # Lorg/apache/poi/poifs/filesystem/DocumentInputStream;
    .param p3, "size"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/GeneralSecurityException;
        }
    .end annotation

    .prologue
    .line 89
    iput-object p1, p0, Lorg/apache/poi/poifs/crypt/AgileDecryptor$ChunkedCipherInputStream;->this$0:Lorg/apache/poi/poifs/crypt/AgileDecryptor;

    .line 88
    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    .line 81
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/poi/poifs/crypt/AgileDecryptor$ChunkedCipherInputStream;->_lastIndex:I

    .line 82
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lorg/apache/poi/poifs/crypt/AgileDecryptor$ChunkedCipherInputStream;->_pos:J

    .line 90
    iput-wide p3, p0, Lorg/apache/poi/poifs/crypt/AgileDecryptor$ChunkedCipherInputStream;->_size:J

    .line 91
    iput-object p2, p0, Lorg/apache/poi/poifs/crypt/AgileDecryptor$ChunkedCipherInputStream;->_stream:Lorg/apache/poi/poifs/filesystem/DocumentInputStream;

    .line 92
    # getter for: Lorg/apache/poi/poifs/crypt/AgileDecryptor;->_info:Lorg/apache/poi/poifs/crypt/EncryptionInfo;
    invoke-static {p1}, Lorg/apache/poi/poifs/crypt/AgileDecryptor;->access$0(Lorg/apache/poi/poifs/crypt/AgileDecryptor;)Lorg/apache/poi/poifs/crypt/EncryptionInfo;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/poi/poifs/crypt/EncryptionInfo;->getHeader()Lorg/apache/poi/poifs/crypt/EncryptionHeader;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/poi/poifs/crypt/EncryptionHeader;->getAlgorithm()I

    move-result v0

    .line 93
    # getter for: Lorg/apache/poi/poifs/crypt/AgileDecryptor;->_info:Lorg/apache/poi/poifs/crypt/EncryptionInfo;
    invoke-static {p1}, Lorg/apache/poi/poifs/crypt/AgileDecryptor;->access$0(Lorg/apache/poi/poifs/crypt/AgileDecryptor;)Lorg/apache/poi/poifs/crypt/EncryptionInfo;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/poi/poifs/crypt/EncryptionInfo;->getHeader()Lorg/apache/poi/poifs/crypt/EncryptionHeader;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/poi/poifs/crypt/EncryptionHeader;->getCipherMode()I

    move-result v1

    .line 94
    # getter for: Lorg/apache/poi/poifs/crypt/AgileDecryptor;->_secretKey:Ljavax/crypto/SecretKey;
    invoke-static {p1}, Lorg/apache/poi/poifs/crypt/AgileDecryptor;->access$1(Lorg/apache/poi/poifs/crypt/AgileDecryptor;)Ljavax/crypto/SecretKey;

    move-result-object v2

    # getter for: Lorg/apache/poi/poifs/crypt/AgileDecryptor;->_info:Lorg/apache/poi/poifs/crypt/EncryptionInfo;
    invoke-static {p1}, Lorg/apache/poi/poifs/crypt/AgileDecryptor;->access$0(Lorg/apache/poi/poifs/crypt/AgileDecryptor;)Lorg/apache/poi/poifs/crypt/EncryptionInfo;

    move-result-object v3

    invoke-virtual {v3}, Lorg/apache/poi/poifs/crypt/EncryptionInfo;->getHeader()Lorg/apache/poi/poifs/crypt/EncryptionHeader;

    move-result-object v3

    invoke-virtual {v3}, Lorg/apache/poi/poifs/crypt/EncryptionHeader;->getKeySalt()[B

    move-result-object v3

    .line 92
    # invokes: Lorg/apache/poi/poifs/crypt/AgileDecryptor;->getCipher(IILjavax/crypto/SecretKey;[B)Ljavax/crypto/Cipher;
    invoke-static {p1, v0, v1, v2, v3}, Lorg/apache/poi/poifs/crypt/AgileDecryptor;->access$2(Lorg/apache/poi/poifs/crypt/AgileDecryptor;IILjavax/crypto/SecretKey;[B)Ljavax/crypto/Cipher;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/poi/poifs/crypt/AgileDecryptor$ChunkedCipherInputStream;->_cipher:Ljavax/crypto/Cipher;

    .line 95
    return-void
.end method

.method private nextChunk()[B
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/GeneralSecurityException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 148
    iget-wide v4, p0, Lorg/apache/poi/poifs/crypt/AgileDecryptor$ChunkedCipherInputStream;->_pos:J

    const/16 v6, 0xc

    shr-long/2addr v4, v6

    long-to-int v2, v4

    .line 149
    .local v2, "index":I
    const/4 v4, 0x4

    new-array v1, v4, [B

    .line 150
    .local v1, "blockKey":[B
    const/4 v4, 0x0

    invoke-static {v1, v4, v2}, Lorg/apache/poi/util/LittleEndian;->putInt([BII)V

    .line 151
    iget-object v4, p0, Lorg/apache/poi/poifs/crypt/AgileDecryptor$ChunkedCipherInputStream;->this$0:Lorg/apache/poi/poifs/crypt/AgileDecryptor;

    iget-object v5, p0, Lorg/apache/poi/poifs/crypt/AgileDecryptor$ChunkedCipherInputStream;->this$0:Lorg/apache/poi/poifs/crypt/AgileDecryptor;

    # getter for: Lorg/apache/poi/poifs/crypt/AgileDecryptor;->_info:Lorg/apache/poi/poifs/crypt/EncryptionInfo;
    invoke-static {v5}, Lorg/apache/poi/poifs/crypt/AgileDecryptor;->access$0(Lorg/apache/poi/poifs/crypt/AgileDecryptor;)Lorg/apache/poi/poifs/crypt/EncryptionInfo;

    move-result-object v5

    invoke-virtual {v5}, Lorg/apache/poi/poifs/crypt/EncryptionInfo;->getHeader()Lorg/apache/poi/poifs/crypt/EncryptionHeader;

    move-result-object v5

    invoke-virtual {v5}, Lorg/apache/poi/poifs/crypt/EncryptionHeader;->getAlgorithm()I

    move-result v5

    .line 152
    iget-object v6, p0, Lorg/apache/poi/poifs/crypt/AgileDecryptor$ChunkedCipherInputStream;->this$0:Lorg/apache/poi/poifs/crypt/AgileDecryptor;

    # getter for: Lorg/apache/poi/poifs/crypt/AgileDecryptor;->_info:Lorg/apache/poi/poifs/crypt/EncryptionInfo;
    invoke-static {v6}, Lorg/apache/poi/poifs/crypt/AgileDecryptor;->access$0(Lorg/apache/poi/poifs/crypt/AgileDecryptor;)Lorg/apache/poi/poifs/crypt/EncryptionInfo;

    move-result-object v6

    invoke-virtual {v6}, Lorg/apache/poi/poifs/crypt/EncryptionInfo;->getHeader()Lorg/apache/poi/poifs/crypt/EncryptionHeader;

    move-result-object v6

    invoke-virtual {v6}, Lorg/apache/poi/poifs/crypt/EncryptionHeader;->getKeySalt()[B

    move-result-object v6

    .line 151
    invoke-virtual {v4, v5, v6, v1}, Lorg/apache/poi/poifs/crypt/AgileDecryptor;->generateIv(I[B[B)[B

    move-result-object v3

    .line 153
    .local v3, "iv":[B
    iget-object v4, p0, Lorg/apache/poi/poifs/crypt/AgileDecryptor$ChunkedCipherInputStream;->_cipher:Ljavax/crypto/Cipher;

    const/4 v5, 0x2

    iget-object v6, p0, Lorg/apache/poi/poifs/crypt/AgileDecryptor$ChunkedCipherInputStream;->this$0:Lorg/apache/poi/poifs/crypt/AgileDecryptor;

    # getter for: Lorg/apache/poi/poifs/crypt/AgileDecryptor;->_secretKey:Ljavax/crypto/SecretKey;
    invoke-static {v6}, Lorg/apache/poi/poifs/crypt/AgileDecryptor;->access$1(Lorg/apache/poi/poifs/crypt/AgileDecryptor;)Ljavax/crypto/SecretKey;

    move-result-object v6

    new-instance v7, Ljavax/crypto/spec/IvParameterSpec;

    invoke-direct {v7, v3}, Ljavax/crypto/spec/IvParameterSpec;-><init>([B)V

    invoke-virtual {v4, v5, v6, v7}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;Ljava/security/spec/AlgorithmParameterSpec;)V

    .line 154
    iget v4, p0, Lorg/apache/poi/poifs/crypt/AgileDecryptor$ChunkedCipherInputStream;->_lastIndex:I

    if-eq v4, v2, :cond_0

    .line 155
    iget-object v4, p0, Lorg/apache/poi/poifs/crypt/AgileDecryptor$ChunkedCipherInputStream;->_stream:Lorg/apache/poi/poifs/filesystem/DocumentInputStream;

    iget v5, p0, Lorg/apache/poi/poifs/crypt/AgileDecryptor$ChunkedCipherInputStream;->_lastIndex:I

    sub-int v5, v2, v5

    shl-int/lit8 v5, v5, 0xc

    int-to-long v6, v5

    invoke-virtual {v4, v6, v7}, Lorg/apache/poi/poifs/filesystem/DocumentInputStream;->skip(J)J

    .line 157
    :cond_0
    iget-object v4, p0, Lorg/apache/poi/poifs/crypt/AgileDecryptor$ChunkedCipherInputStream;->_stream:Lorg/apache/poi/poifs/filesystem/DocumentInputStream;

    invoke-virtual {v4}, Lorg/apache/poi/poifs/filesystem/DocumentInputStream;->available()I

    move-result v4

    const/16 v5, 0x1000

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v4

    new-array v0, v4, [B

    .line 158
    .local v0, "block":[B
    iget-object v4, p0, Lorg/apache/poi/poifs/crypt/AgileDecryptor$ChunkedCipherInputStream;->_stream:Lorg/apache/poi/poifs/filesystem/DocumentInputStream;

    invoke-virtual {v4, v0}, Lorg/apache/poi/poifs/filesystem/DocumentInputStream;->readFully([B)V

    .line 159
    add-int/lit8 v4, v2, 0x1

    iput v4, p0, Lorg/apache/poi/poifs/crypt/AgileDecryptor$ChunkedCipherInputStream;->_lastIndex:I

    .line 160
    iget-object v4, p0, Lorg/apache/poi/poifs/crypt/AgileDecryptor$ChunkedCipherInputStream;->_cipher:Ljavax/crypto/Cipher;

    invoke-virtual {v4, v0}, Ljavax/crypto/Cipher;->doFinal([B)[B

    move-result-object v4

    return-object v4
.end method


# virtual methods
.method public available()I
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 143
    iget-wide v0, p0, Lorg/apache/poi/poifs/crypt/AgileDecryptor$ChunkedCipherInputStream;->_size:J

    iget-wide v2, p0, Lorg/apache/poi/poifs/crypt/AgileDecryptor$ChunkedCipherInputStream;->_pos:J

    sub-long/2addr v0, v2

    long-to-int v0, v0

    return v0
.end method

.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 144
    iget-object v0, p0, Lorg/apache/poi/poifs/crypt/AgileDecryptor$ChunkedCipherInputStream;->_stream:Lorg/apache/poi/poifs/filesystem/DocumentInputStream;

    invoke-virtual {v0}, Lorg/apache/poi/poifs/filesystem/DocumentInputStream;->close()V

    return-void
.end method

.method public markSupported()Z
    .locals 1

    .prologue
    .line 145
    const/4 v0, 0x0

    return v0
.end method

.method public read()I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 98
    new-array v0, v2, [B

    .line 99
    .local v0, "b":[B
    invoke-virtual {p0, v0}, Lorg/apache/poi/poifs/crypt/AgileDecryptor$ChunkedCipherInputStream;->read([B)I

    move-result v1

    if-ne v1, v2, :cond_0

    .line 100
    const/4 v1, 0x0

    aget-byte v1, v0, v1

    .line 101
    :goto_0
    return v1

    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public read([B)I
    .locals 2
    .param p1, "b"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 105
    const/4 v0, 0x0

    array-length v1, p1

    invoke-virtual {p0, p1, v0, v1}, Lorg/apache/poi/poifs/crypt/AgileDecryptor$ChunkedCipherInputStream;->read([BII)I

    move-result v0

    return v0
.end method

.method public read([BII)I
    .locals 10
    .param p1, "b"    # [B
    .param p2, "off"    # I
    .param p3, "len"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v8, 0xfff

    .line 109
    const/4 v2, 0x0

    .line 111
    .local v2, "total":I
    :goto_0
    if-gtz p3, :cond_0

    .line 130
    return v2

    .line 112
    :cond_0
    iget-object v3, p0, Lorg/apache/poi/poifs/crypt/AgileDecryptor$ChunkedCipherInputStream;->_chunk:[B

    if-nez v3, :cond_1

    .line 114
    :try_start_0
    invoke-direct {p0}, Lorg/apache/poi/poifs/crypt/AgileDecryptor$ChunkedCipherInputStream;->nextChunk()[B

    move-result-object v3

    iput-object v3, p0, Lorg/apache/poi/poifs/crypt/AgileDecryptor$ChunkedCipherInputStream;->_chunk:[B
    :try_end_0
    .catch Ljava/security/GeneralSecurityException; {:try_start_0 .. :try_end_0} :catch_0

    .line 119
    :cond_1
    const-wide/16 v4, 0x1000

    iget-wide v6, p0, Lorg/apache/poi/poifs/crypt/AgileDecryptor$ChunkedCipherInputStream;->_pos:J

    and-long/2addr v6, v8

    sub-long/2addr v4, v6

    long-to-int v0, v4

    .line 120
    .local v0, "count":I
    invoke-virtual {p0}, Lorg/apache/poi/poifs/crypt/AgileDecryptor$ChunkedCipherInputStream;->available()I

    move-result v3

    invoke-static {v0, p3}, Ljava/lang/Math;->min(II)I

    move-result v4

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 121
    iget-object v3, p0, Lorg/apache/poi/poifs/crypt/AgileDecryptor$ChunkedCipherInputStream;->_chunk:[B

    iget-wide v4, p0, Lorg/apache/poi/poifs/crypt/AgileDecryptor$ChunkedCipherInputStream;->_pos:J

    and-long/2addr v4, v8

    long-to-int v4, v4

    invoke-static {v3, v4, p1, p2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 122
    add-int/2addr p2, v0

    .line 123
    sub-int/2addr p3, v0

    .line 124
    iget-wide v4, p0, Lorg/apache/poi/poifs/crypt/AgileDecryptor$ChunkedCipherInputStream;->_pos:J

    int-to-long v6, v0

    add-long/2addr v4, v6

    iput-wide v4, p0, Lorg/apache/poi/poifs/crypt/AgileDecryptor$ChunkedCipherInputStream;->_pos:J

    .line 125
    iget-wide v4, p0, Lorg/apache/poi/poifs/crypt/AgileDecryptor$ChunkedCipherInputStream;->_pos:J

    and-long/2addr v4, v8

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-nez v3, :cond_2

    .line 126
    const/4 v3, 0x0

    iput-object v3, p0, Lorg/apache/poi/poifs/crypt/AgileDecryptor$ChunkedCipherInputStream;->_chunk:[B

    .line 127
    :cond_2
    add-int/2addr v2, v0

    goto :goto_0

    .line 115
    .end local v0    # "count":I
    :catch_0
    move-exception v1

    .line 116
    .local v1, "e":Ljava/security/GeneralSecurityException;
    new-instance v3, Lorg/apache/poi/EncryptedDocumentException;

    invoke-virtual {v1}, Ljava/security/GeneralSecurityException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Lorg/apache/poi/EncryptedDocumentException;-><init>(Ljava/lang/String;)V

    throw v3
.end method

.method public skip(J)J
    .locals 9
    .param p1, "n"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 134
    iget-wide v2, p0, Lorg/apache/poi/poifs/crypt/AgileDecryptor$ChunkedCipherInputStream;->_pos:J

    .line 135
    .local v2, "start":J
    invoke-virtual {p0}, Lorg/apache/poi/poifs/crypt/AgileDecryptor$ChunkedCipherInputStream;->available()I

    move-result v4

    int-to-long v4, v4

    invoke-static {v4, v5, p1, p2}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    .line 137
    .local v0, "skip":J
    iget-wide v4, p0, Lorg/apache/poi/poifs/crypt/AgileDecryptor$ChunkedCipherInputStream;->_pos:J

    add-long/2addr v4, v0

    xor-long/2addr v4, v2

    const-wide/16 v6, -0x1000

    and-long/2addr v4, v6

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-eqz v4, :cond_0

    .line 138
    const/4 v4, 0x0

    iput-object v4, p0, Lorg/apache/poi/poifs/crypt/AgileDecryptor$ChunkedCipherInputStream;->_chunk:[B

    .line 139
    :cond_0
    iget-wide v4, p0, Lorg/apache/poi/poifs/crypt/AgileDecryptor$ChunkedCipherInputStream;->_pos:J

    add-long/2addr v4, v0

    iput-wide v4, p0, Lorg/apache/poi/poifs/crypt/AgileDecryptor$ChunkedCipherInputStream;->_pos:J

    .line 140
    return-wide v0
.end method
