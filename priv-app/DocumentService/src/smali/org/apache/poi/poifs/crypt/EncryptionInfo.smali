.class public Lorg/apache/poi/poifs/crypt/EncryptionInfo;
.super Ljava/lang/Object;
.source "EncryptionInfo.java"


# instance fields
.field private final encryptionFlags:I

.field private final header:Lorg/apache/poi/poifs/crypt/EncryptionHeader;

.field private final verifier:Lorg/apache/poi/poifs/crypt/EncryptionVerifier;

.field private final versionMajor:I

.field private final versionMinor:I


# direct methods
.method public constructor <init>(Lorg/apache/poi/poifs/filesystem/DirectoryNode;)V
    .locals 9
    .param p1, "dir"    # Lorg/apache/poi/poifs/filesystem/DirectoryNode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x4

    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    const/4 v3, 0x0

    .line 48
    .local v3, "dis":Lorg/apache/poi/poifs/filesystem/DocumentInputStream;
    :try_start_0
    const-string/jumbo v6, "EncryptionInfo"

    invoke-virtual {p1, v6}, Lorg/apache/poi/poifs/filesystem/DirectoryNode;->createDocumentInputStream(Ljava/lang/String;)Lorg/apache/poi/poifs/filesystem/DocumentInputStream;

    move-result-object v3

    .line 49
    invoke-virtual {v3}, Lorg/apache/poi/poifs/filesystem/DocumentInputStream;->readShort()S

    move-result v6

    iput v6, p0, Lorg/apache/poi/poifs/crypt/EncryptionInfo;->versionMajor:I

    .line 50
    invoke-virtual {v3}, Lorg/apache/poi/poifs/filesystem/DocumentInputStream;->readShort()S

    move-result v6

    iput v6, p0, Lorg/apache/poi/poifs/crypt/EncryptionInfo;->versionMinor:I

    .line 52
    invoke-virtual {v3}, Lorg/apache/poi/poifs/filesystem/DocumentInputStream;->readInt()I

    move-result v6

    iput v6, p0, Lorg/apache/poi/poifs/crypt/EncryptionInfo;->encryptionFlags:I

    .line 54
    iget v6, p0, Lorg/apache/poi/poifs/crypt/EncryptionInfo;->versionMajor:I

    if-ne v6, v7, :cond_2

    iget v6, p0, Lorg/apache/poi/poifs/crypt/EncryptionInfo;->versionMinor:I

    if-ne v6, v7, :cond_2

    iget v6, p0, Lorg/apache/poi/poifs/crypt/EncryptionInfo;->encryptionFlags:I

    const/16 v7, 0x40

    if-ne v6, v7, :cond_2

    .line 55
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 56
    .local v1, "builder":Ljava/lang/StringBuilder;
    invoke-virtual {v3}, Lorg/apache/poi/poifs/filesystem/DocumentInputStream;->available()I

    move-result v6

    new-array v5, v6, [B

    .line 57
    .local v5, "xmlDescriptor":[B
    invoke-virtual {v3, v5}, Lorg/apache/poi/poifs/filesystem/DocumentInputStream;->read([B)I

    .line 58
    array-length v7, v5

    const/4 v6, 0x0

    :goto_0
    if-lt v6, v7, :cond_1

    .line 60
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 61
    .local v2, "descriptor":Ljava/lang/String;
    new-instance v6, Lorg/apache/poi/poifs/crypt/EncryptionHeader;

    invoke-direct {v6, v2}, Lorg/apache/poi/poifs/crypt/EncryptionHeader;-><init>(Ljava/lang/String;)V

    iput-object v6, p0, Lorg/apache/poi/poifs/crypt/EncryptionInfo;->header:Lorg/apache/poi/poifs/crypt/EncryptionHeader;

    .line 62
    new-instance v6, Lorg/apache/poi/poifs/crypt/EncryptionVerifier;

    invoke-direct {v6, v2}, Lorg/apache/poi/poifs/crypt/EncryptionVerifier;-><init>(Ljava/lang/String;)V

    iput-object v6, p0, Lorg/apache/poi/poifs/crypt/EncryptionInfo;->verifier:Lorg/apache/poi/poifs/crypt/EncryptionVerifier;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 74
    .end local v1    # "builder":Ljava/lang/StringBuilder;
    .end local v2    # "descriptor":Ljava/lang/String;
    .end local v5    # "xmlDescriptor":[B
    :goto_1
    if-eqz v3, :cond_0

    .line 75
    invoke-virtual {v3}, Lorg/apache/poi/poifs/filesystem/DocumentInputStream;->close()V

    .line 77
    :cond_0
    return-void

    .line 58
    .restart local v1    # "builder":Ljava/lang/StringBuilder;
    .restart local v5    # "xmlDescriptor":[B
    :cond_1
    :try_start_1
    aget-byte v0, v5, v6

    .line 59
    .local v0, "b":B
    int-to-char v8, v0

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 58
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 64
    .end local v0    # "b":B
    .end local v1    # "builder":Ljava/lang/StringBuilder;
    .end local v5    # "xmlDescriptor":[B
    :cond_2
    invoke-virtual {v3}, Lorg/apache/poi/poifs/filesystem/DocumentInputStream;->readInt()I

    move-result v4

    .line 65
    .local v4, "hSize":I
    new-instance v6, Lorg/apache/poi/poifs/crypt/EncryptionHeader;

    invoke-direct {v6, v3}, Lorg/apache/poi/poifs/crypt/EncryptionHeader;-><init>(Lorg/apache/poi/poifs/filesystem/DocumentInputStream;)V

    iput-object v6, p0, Lorg/apache/poi/poifs/crypt/EncryptionInfo;->header:Lorg/apache/poi/poifs/crypt/EncryptionHeader;

    .line 66
    iget-object v6, p0, Lorg/apache/poi/poifs/crypt/EncryptionInfo;->header:Lorg/apache/poi/poifs/crypt/EncryptionHeader;

    invoke-virtual {v6}, Lorg/apache/poi/poifs/crypt/EncryptionHeader;->getAlgorithm()I

    move-result v6

    const/16 v7, 0x6801

    if-ne v6, v7, :cond_4

    .line 67
    new-instance v6, Lorg/apache/poi/poifs/crypt/EncryptionVerifier;

    const/16 v7, 0x14

    invoke-direct {v6, v3, v7}, Lorg/apache/poi/poifs/crypt/EncryptionVerifier;-><init>(Lorg/apache/poi/poifs/filesystem/DocumentInputStream;I)V

    iput-object v6, p0, Lorg/apache/poi/poifs/crypt/EncryptionInfo;->verifier:Lorg/apache/poi/poifs/crypt/EncryptionVerifier;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 72
    .end local v4    # "hSize":I
    :catchall_0
    move-exception v6

    .line 74
    if-eqz v3, :cond_3

    .line 75
    invoke-virtual {v3}, Lorg/apache/poi/poifs/filesystem/DocumentInputStream;->close()V

    .line 76
    :cond_3
    throw v6

    .line 69
    .restart local v4    # "hSize":I
    :cond_4
    :try_start_2
    new-instance v6, Lorg/apache/poi/poifs/crypt/EncryptionVerifier;

    const/16 v7, 0x20

    invoke-direct {v6, v3, v7}, Lorg/apache/poi/poifs/crypt/EncryptionVerifier;-><init>(Lorg/apache/poi/poifs/filesystem/DocumentInputStream;I)V

    iput-object v6, p0, Lorg/apache/poi/poifs/crypt/EncryptionInfo;->verifier:Lorg/apache/poi/poifs/crypt/EncryptionVerifier;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method

.method public constructor <init>(Lorg/apache/poi/poifs/filesystem/NPOIFSFileSystem;)V
    .locals 1
    .param p1, "fs"    # Lorg/apache/poi/poifs/filesystem/NPOIFSFileSystem;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 42
    invoke-virtual {p1}, Lorg/apache/poi/poifs/filesystem/NPOIFSFileSystem;->getRoot()Lorg/apache/poi/poifs/filesystem/DirectoryNode;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/apache/poi/poifs/crypt/EncryptionInfo;-><init>(Lorg/apache/poi/poifs/filesystem/DirectoryNode;)V

    .line 43
    return-void
.end method

.method public constructor <init>(Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;)V
    .locals 1
    .param p1, "fs"    # Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 39
    invoke-virtual {p1}, Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;->getRoot()Lorg/apache/poi/poifs/filesystem/DirectoryNode;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/apache/poi/poifs/crypt/EncryptionInfo;-><init>(Lorg/apache/poi/poifs/filesystem/DirectoryNode;)V

    .line 40
    return-void
.end method


# virtual methods
.method public getEncryptionFlags()I
    .locals 1

    .prologue
    .line 88
    iget v0, p0, Lorg/apache/poi/poifs/crypt/EncryptionInfo;->encryptionFlags:I

    return v0
.end method

.method public getHeader()Lorg/apache/poi/poifs/crypt/EncryptionHeader;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lorg/apache/poi/poifs/crypt/EncryptionInfo;->header:Lorg/apache/poi/poifs/crypt/EncryptionHeader;

    return-object v0
.end method

.method public getVerifier()Lorg/apache/poi/poifs/crypt/EncryptionVerifier;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lorg/apache/poi/poifs/crypt/EncryptionInfo;->verifier:Lorg/apache/poi/poifs/crypt/EncryptionVerifier;

    return-object v0
.end method

.method public getVersionMajor()I
    .locals 1

    .prologue
    .line 80
    iget v0, p0, Lorg/apache/poi/poifs/crypt/EncryptionInfo;->versionMajor:I

    return v0
.end method

.method public getVersionMinor()I
    .locals 1

    .prologue
    .line 84
    iget v0, p0, Lorg/apache/poi/poifs/crypt/EncryptionInfo;->versionMinor:I

    return v0
.end method
