.class public Lorg/apache/poi/poifs/dev/POIFSViewEngine;
.super Ljava/lang/Object;
.source "POIFSViewEngine.java"


# static fields
.field private static final _EOL:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    const-string/jumbo v0, "line.separator"

    invoke-static {v0}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/poifs/dev/POIFSViewEngine;->_EOL:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static indent(ILjava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 8
    .param p0, "indentLevel"    # I
    .param p1, "indentString"    # Ljava/lang/String;
    .param p2, "data"    # Ljava/lang/String;

    .prologue
    .line 101
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 102
    .local v1, "finalBuffer":Ljava/lang/StringBuffer;
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    .line 104
    .local v2, "indentPrefix":Ljava/lang/StringBuffer;
    const/4 v3, 0x0

    .local v3, "j":I
    :goto_0
    if-lt v3, p0, :cond_0

    .line 109
    new-instance v5, Ljava/io/LineNumberReader;

    new-instance v6, Ljava/io/StringReader;

    invoke-direct {v6, p2}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v5, v6}, Ljava/io/LineNumberReader;-><init>(Ljava/io/Reader;)V

    .line 113
    .local v5, "reader":Ljava/io/LineNumberReader;
    :try_start_0
    invoke-virtual {v5}, Ljava/io/LineNumberReader;->readLine()Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 115
    .local v4, "line":Ljava/lang/String;
    :goto_1
    if-nez v4, :cond_1

    .line 126
    .end local v4    # "line":Ljava/lang/String;
    :goto_2
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    return-object v6

    .line 106
    .end local v5    # "reader":Ljava/io/LineNumberReader;
    :cond_0
    invoke-virtual {v2, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 104
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 117
    .restart local v4    # "line":Ljava/lang/String;
    .restart local v5    # "reader":Ljava/io/LineNumberReader;
    :cond_1
    :try_start_1
    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    sget-object v7, Lorg/apache/poi/poifs/dev/POIFSViewEngine;->_EOL:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 118
    invoke-virtual {v5}, Ljava/io/LineNumberReader;->readLine()Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v4

    goto :goto_1

    .line 121
    .end local v4    # "line":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 123
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    .line 124
    sget-object v7, Lorg/apache/poi/poifs/dev/POIFSViewEngine;->_EOL:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_2
.end method

.method public static inspectViewable(Ljava/lang/Object;ZILjava/lang/String;)Ljava/util/List;
    .locals 7
    .param p0, "viewable"    # Ljava/lang/Object;
    .param p1, "drilldown"    # Z
    .param p2, "indentLevel"    # I
    .param p3, "indentString"    # Ljava/lang/String;

    .prologue
    .line 55
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 57
    .local v4, "objects":Ljava/util/List;
    instance-of v5, p0, Lorg/apache/poi/poifs/dev/POIFSViewable;

    if-eqz v5, :cond_3

    move-object v1, p0

    .line 59
    check-cast v1, Lorg/apache/poi/poifs/dev/POIFSViewable;

    .line 62
    .local v1, "inspected":Lorg/apache/poi/poifs/dev/POIFSViewable;
    invoke-interface {v1}, Lorg/apache/poi/poifs/dev/POIFSViewable;->getShortDescription()Ljava/lang/String;

    move-result-object v5

    .line 61
    invoke-static {p2, p3, v5}, Lorg/apache/poi/poifs/dev/POIFSViewEngine;->indent(ILjava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 63
    if-eqz p1, :cond_0

    .line 65
    invoke-interface {v1}, Lorg/apache/poi/poifs/dev/POIFSViewable;->preferArray()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 67
    invoke-interface {v1}, Lorg/apache/poi/poifs/dev/POIFSViewable;->getViewableArray()[Ljava/lang/Object;

    move-result-object v0

    .line 69
    .local v0, "data":[Ljava/lang/Object;
    const/4 v3, 0x0

    .local v3, "j":I
    :goto_0
    array-length v5, v0

    if-lt v3, v5, :cond_1

    .line 95
    .end local v0    # "data":[Ljava/lang/Object;
    .end local v1    # "inspected":Lorg/apache/poi/poifs/dev/POIFSViewable;
    .end local v3    # "j":I
    :cond_0
    :goto_1
    return-object v4

    .line 71
    .restart local v0    # "data":[Ljava/lang/Object;
    .restart local v1    # "inspected":Lorg/apache/poi/poifs/dev/POIFSViewable;
    .restart local v3    # "j":I
    :cond_1
    aget-object v5, v0, v3

    .line 72
    add-int/lit8 v6, p2, 0x1

    .line 71
    invoke-static {v5, p1, v6, p3}, Lorg/apache/poi/poifs/dev/POIFSViewEngine;->inspectViewable(Ljava/lang/Object;ZILjava/lang/String;)Ljava/util/List;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 69
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 78
    .end local v0    # "data":[Ljava/lang/Object;
    .end local v3    # "j":I
    :cond_2
    invoke-interface {v1}, Lorg/apache/poi/poifs/dev/POIFSViewable;->getViewableIterator()Ljava/util/Iterator;

    move-result-object v2

    .line 80
    .local v2, "iter":Ljava/util/Iterator;
    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 82
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    .line 84
    add-int/lit8 v6, p2, 0x1

    .line 82
    invoke-static {v5, p1, v6, p3}, Lorg/apache/poi/poifs/dev/POIFSViewEngine;->inspectViewable(Ljava/lang/Object;ZILjava/lang/String;)Ljava/util/List;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_2

    .line 93
    .end local v1    # "inspected":Lorg/apache/poi/poifs/dev/POIFSViewable;
    .end local v2    # "iter":Ljava/util/Iterator;
    :cond_3
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    .line 92
    invoke-static {p2, p3, v5}, Lorg/apache/poi/poifs/dev/POIFSViewEngine;->indent(ILjava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method
