.class public Lorg/apache/poi/poifs/filesystem/Ole10Native;
.super Ljava/lang/Object;
.source "Ole10Native.java"


# static fields
.field public static final OLE10_NATIVE:Ljava/lang/String; = "\u0001Ole10Native"


# instance fields
.field private final command:Ljava/lang/String;

.field private final dataBuffer:[B

.field private final dataSize:I

.field private final fileName:Ljava/lang/String;

.field private flags1:S

.field private flags2:S

.field private flags3:S

.field private final label:Ljava/lang/String;

.field private final totalSize:I

.field private unknown1:[B

.field private unknown2:[B


# direct methods
.method public constructor <init>([BI)V
    .locals 1
    .param p1, "data"    # [B
    .param p2, "offset"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/poi/poifs/filesystem/Ole10NativeException;
        }
    .end annotation

    .prologue
    .line 104
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/poi/poifs/filesystem/Ole10Native;-><init>([BIZ)V

    .line 105
    return-void
.end method

.method public constructor <init>([BIZ)V
    .locals 8
    .param p1, "data"    # [B
    .param p2, "offset"    # I
    .param p3, "plain"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/poi/poifs/filesystem/Ole10NativeException;
        }
    .end annotation

    .prologue
    const/16 v7, 0x8

    const/4 v6, 0x4

    const/4 v5, 0x0

    .line 114
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 115
    move v1, p2

    .line 117
    .local v1, "ofs":I
    array-length v3, p1

    add-int/lit8 v4, p2, 0x2

    if-ge v3, v4, :cond_0

    .line 118
    new-instance v3, Lorg/apache/poi/poifs/filesystem/Ole10NativeException;

    const-string/jumbo v4, "data is too small"

    invoke-direct {v3, v4}, Lorg/apache/poi/poifs/filesystem/Ole10NativeException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 121
    :cond_0
    invoke-static {p1, v1}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v3

    iput v3, p0, Lorg/apache/poi/poifs/filesystem/Ole10Native;->totalSize:I

    .line 122
    add-int/lit8 v1, v1, 0x4

    .line 124
    if-eqz p3, :cond_1

    .line 125
    iget v3, p0, Lorg/apache/poi/poifs/filesystem/Ole10Native;->totalSize:I

    add-int/lit8 v3, v3, -0x4

    new-array v3, v3, [B

    iput-object v3, p0, Lorg/apache/poi/poifs/filesystem/Ole10Native;->dataBuffer:[B

    .line 126
    iget-object v3, p0, Lorg/apache/poi/poifs/filesystem/Ole10Native;->dataBuffer:[B

    iget-object v4, p0, Lorg/apache/poi/poifs/filesystem/Ole10Native;->dataBuffer:[B

    array-length v4, v4

    invoke-static {p1, v6, v3, v5, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 127
    iget v3, p0, Lorg/apache/poi/poifs/filesystem/Ole10Native;->totalSize:I

    add-int/lit8 v3, v3, -0x4

    iput v3, p0, Lorg/apache/poi/poifs/filesystem/Ole10Native;->dataSize:I

    .line 129
    new-array v2, v7, [B

    .line 130
    .local v2, "oleLabel":[B
    iget-object v3, p0, Lorg/apache/poi/poifs/filesystem/Ole10Native;->dataBuffer:[B

    iget-object v4, p0, Lorg/apache/poi/poifs/filesystem/Ole10Native;->dataBuffer:[B

    array-length v4, v4

    invoke-static {v4, v7}, Ljava/lang/Math;->min(II)I

    move-result v4

    invoke-static {v3, v5, v2, v5, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 131
    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "ole-"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v2}, Lorg/apache/poi/util/HexDump;->toHex([B)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lorg/apache/poi/poifs/filesystem/Ole10Native;->label:Ljava/lang/String;

    .line 132
    iget-object v3, p0, Lorg/apache/poi/poifs/filesystem/Ole10Native;->label:Ljava/lang/String;

    iput-object v3, p0, Lorg/apache/poi/poifs/filesystem/Ole10Native;->fileName:Ljava/lang/String;

    .line 133
    iget-object v3, p0, Lorg/apache/poi/poifs/filesystem/Ole10Native;->label:Ljava/lang/String;

    iput-object v3, p0, Lorg/apache/poi/poifs/filesystem/Ole10Native;->command:Ljava/lang/String;

    .line 177
    .end local v2    # "oleLabel":[B
    :goto_0
    return-void

    .line 135
    :cond_1
    invoke-static {p1, v1}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v3

    iput-short v3, p0, Lorg/apache/poi/poifs/filesystem/Ole10Native;->flags1:S

    .line 136
    add-int/lit8 v1, v1, 0x2

    .line 137
    invoke-static {p1, v1}, Lorg/apache/poi/poifs/filesystem/Ole10Native;->getStringLength([BI)I

    move-result v0

    .line 138
    .local v0, "len":I
    add-int/lit8 v3, v0, -0x1

    invoke-static {p1, v1, v3}, Lorg/apache/poi/util/StringUtil;->getFromCompressedUnicode([BII)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lorg/apache/poi/poifs/filesystem/Ole10Native;->label:Ljava/lang/String;

    .line 139
    add-int/2addr v1, v0

    .line 140
    invoke-static {p1, v1}, Lorg/apache/poi/poifs/filesystem/Ole10Native;->getStringLength([BI)I

    move-result v0

    .line 141
    add-int/lit8 v3, v0, -0x1

    invoke-static {p1, v1, v3}, Lorg/apache/poi/util/StringUtil;->getFromCompressedUnicode([BII)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lorg/apache/poi/poifs/filesystem/Ole10Native;->fileName:Ljava/lang/String;

    .line 142
    add-int/2addr v1, v0

    .line 143
    invoke-static {p1, v1}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v3

    iput-short v3, p0, Lorg/apache/poi/poifs/filesystem/Ole10Native;->flags2:S

    .line 144
    add-int/lit8 v1, v1, 0x2

    .line 145
    invoke-static {p1, v1}, Lorg/apache/poi/util/LittleEndian;->getUByte([BI)S

    move-result v0

    .line 146
    new-array v3, v0, [B

    iput-object v3, p0, Lorg/apache/poi/poifs/filesystem/Ole10Native;->unknown1:[B

    .line 147
    add-int/2addr v1, v0

    .line 148
    const/4 v0, 0x3

    .line 149
    new-array v3, v0, [B

    iput-object v3, p0, Lorg/apache/poi/poifs/filesystem/Ole10Native;->unknown2:[B

    .line 150
    add-int/2addr v1, v0

    .line 151
    invoke-static {p1, v1}, Lorg/apache/poi/poifs/filesystem/Ole10Native;->getStringLength([BI)I

    move-result v0

    .line 152
    add-int/lit8 v3, v0, -0x1

    invoke-static {p1, v1, v3}, Lorg/apache/poi/util/StringUtil;->getFromCompressedUnicode([BII)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lorg/apache/poi/poifs/filesystem/Ole10Native;->command:Ljava/lang/String;

    .line 153
    add-int/2addr v1, v0

    .line 155
    iget v3, p0, Lorg/apache/poi/poifs/filesystem/Ole10Native;->totalSize:I

    add-int/lit8 v3, v3, 0x4

    sub-int/2addr v3, v1

    if-le v3, v6, :cond_5

    .line 156
    invoke-static {p1, v1}, Lorg/apache/poi/util/LittleEndian;->getInt([BI)I

    move-result v3

    iput v3, p0, Lorg/apache/poi/poifs/filesystem/Ole10Native;->dataSize:I

    .line 157
    add-int/lit8 v1, v1, 0x4

    .line 159
    iget v3, p0, Lorg/apache/poi/poifs/filesystem/Ole10Native;->dataSize:I

    iget v4, p0, Lorg/apache/poi/poifs/filesystem/Ole10Native;->totalSize:I

    if-gt v3, v4, :cond_2

    iget v3, p0, Lorg/apache/poi/poifs/filesystem/Ole10Native;->dataSize:I

    if-gez v3, :cond_3

    .line 160
    :cond_2
    new-instance v3, Lorg/apache/poi/poifs/filesystem/Ole10NativeException;

    const-string/jumbo v4, "Invalid Ole10Native"

    invoke-direct {v3, v4}, Lorg/apache/poi/poifs/filesystem/Ole10NativeException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 163
    :cond_3
    iget v3, p0, Lorg/apache/poi/poifs/filesystem/Ole10Native;->dataSize:I

    new-array v3, v3, [B

    iput-object v3, p0, Lorg/apache/poi/poifs/filesystem/Ole10Native;->dataBuffer:[B

    .line 164
    iget-object v3, p0, Lorg/apache/poi/poifs/filesystem/Ole10Native;->dataBuffer:[B

    iget v4, p0, Lorg/apache/poi/poifs/filesystem/Ole10Native;->dataSize:I

    invoke-static {p1, v1, v3, v5, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 165
    iget v3, p0, Lorg/apache/poi/poifs/filesystem/Ole10Native;->dataSize:I

    add-int/2addr v1, v3

    .line 167
    iget-object v3, p0, Lorg/apache/poi/poifs/filesystem/Ole10Native;->unknown1:[B

    array-length v3, v3

    if-lez v3, :cond_4

    .line 168
    invoke-static {p1, v1}, Lorg/apache/poi/util/LittleEndian;->getShort([BI)S

    move-result v3

    iput-short v3, p0, Lorg/apache/poi/poifs/filesystem/Ole10Native;->flags3:S

    .line 169
    add-int/lit8 v1, v1, 0x2

    .line 170
    goto/16 :goto_0

    .line 171
    :cond_4
    iput-short v5, p0, Lorg/apache/poi/poifs/filesystem/Ole10Native;->flags3:S

    goto/16 :goto_0

    .line 174
    :cond_5
    new-instance v3, Lorg/apache/poi/poifs/filesystem/Ole10NativeException;

    const-string/jumbo v4, "Invalid Ole10Native"

    invoke-direct {v3, v4}, Lorg/apache/poi/poifs/filesystem/Ole10NativeException;-><init>(Ljava/lang/String;)V

    throw v3
.end method

.method public static createFromEmbeddedOleObject(Lorg/apache/poi/poifs/filesystem/DirectoryNode;)Lorg/apache/poi/poifs/filesystem/Ole10Native;
    .locals 7
    .param p0, "directory"    # Lorg/apache/poi/poifs/filesystem/DirectoryNode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/apache/poi/poifs/filesystem/Ole10NativeException;
        }
    .end annotation

    .prologue
    .line 75
    const/4 v4, 0x0

    .line 78
    .local v4, "plain":Z
    :try_start_0
    const-string/jumbo v5, "\u0001Ole10ItemName"

    invoke-virtual {p0, v5}, Lorg/apache/poi/poifs/filesystem/DirectoryNode;->getEntry(Ljava/lang/String;)Lorg/apache/poi/poifs/filesystem/Entry;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 79
    const/4 v4, 0x1

    .line 85
    :goto_0
    const-string/jumbo v5, "\u0001Ole10Native"

    invoke-virtual {p0, v5}, Lorg/apache/poi/poifs/filesystem/DirectoryNode;->getEntry(Ljava/lang/String;)Lorg/apache/poi/poifs/filesystem/Entry;

    move-result-object v3

    check-cast v3, Lorg/apache/poi/poifs/filesystem/DocumentEntry;

    .line 86
    .local v3, "nativeEntry":Lorg/apache/poi/poifs/filesystem/DocumentEntry;
    invoke-interface {v3}, Lorg/apache/poi/poifs/filesystem/DocumentEntry;->getSize()I

    move-result v5

    new-array v0, v5, [B

    .line 89
    .local v0, "data":[B
    invoke-virtual {p0, v3}, Lorg/apache/poi/poifs/filesystem/DirectoryNode;->createDocumentInputStream(Lorg/apache/poi/poifs/filesystem/Entry;)Lorg/apache/poi/poifs/filesystem/DocumentInputStream;

    move-result-object v1

    .line 90
    .local v1, "dis":Lorg/apache/poi/poifs/filesystem/DocumentInputStream;
    invoke-virtual {v1, v0}, Lorg/apache/poi/poifs/filesystem/DocumentInputStream;->readFully([B)V

    .line 91
    invoke-virtual {v1}, Lorg/apache/poi/poifs/filesystem/DocumentInputStream;->close()V

    .line 93
    new-instance v5, Lorg/apache/poi/poifs/filesystem/Ole10Native;

    const/4 v6, 0x0

    invoke-direct {v5, v0, v6, v4}, Lorg/apache/poi/poifs/filesystem/Ole10Native;-><init>([BIZ)V

    return-object v5

    .line 80
    .end local v0    # "data":[B
    .end local v1    # "dis":Lorg/apache/poi/poifs/filesystem/DocumentInputStream;
    .end local v3    # "nativeEntry":Lorg/apache/poi/poifs/filesystem/DocumentEntry;
    :catch_0
    move-exception v2

    .line 81
    .local v2, "ex":Ljava/io/FileNotFoundException;
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public static createFromEmbeddedOleObject(Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;)Lorg/apache/poi/poifs/filesystem/Ole10Native;
    .locals 1
    .param p0, "poifs"    # Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/apache/poi/poifs/filesystem/Ole10NativeException;
        }
    .end annotation

    .prologue
    .line 61
    invoke-virtual {p0}, Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;->getRoot()Lorg/apache/poi/poifs/filesystem/DirectoryNode;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/poi/poifs/filesystem/Ole10Native;->createFromEmbeddedOleObject(Lorg/apache/poi/poifs/filesystem/DirectoryNode;)Lorg/apache/poi/poifs/filesystem/Ole10Native;

    move-result-object v0

    return-object v0
.end method

.method private static getStringLength([BI)I
    .locals 3
    .param p0, "data"    # [B
    .param p1, "ofs"    # I

    .prologue
    .line 183
    const/4 v0, 0x0

    .line 184
    .local v0, "len":I
    :goto_0
    add-int v1, v0, p1

    array-length v2, p0

    if-ge v1, v2, :cond_0

    add-int v1, p1, v0

    aget-byte v1, p0, v1

    if-nez v1, :cond_1

    .line 187
    :cond_0
    add-int/lit8 v0, v0, 0x1

    .line 188
    return v0

    .line 185
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public getCommand()Ljava/lang/String;
    .locals 1

    .prologue
    .line 264
    iget-object v0, p0, Lorg/apache/poi/poifs/filesystem/Ole10Native;->command:Ljava/lang/String;

    return-object v0
.end method

.method public getDataBuffer()[B
    .locals 1

    .prologue
    .line 287
    iget-object v0, p0, Lorg/apache/poi/poifs/filesystem/Ole10Native;->dataBuffer:[B

    return-object v0
.end method

.method public getDataSize()I
    .locals 1

    .prologue
    .line 275
    iget v0, p0, Lorg/apache/poi/poifs/filesystem/Ole10Native;->dataSize:I

    return v0
.end method

.method public getFileName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 227
    iget-object v0, p0, Lorg/apache/poi/poifs/filesystem/Ole10Native;->fileName:Ljava/lang/String;

    return-object v0
.end method

.method public getFlags1()S
    .locals 1

    .prologue
    .line 207
    iget-short v0, p0, Lorg/apache/poi/poifs/filesystem/Ole10Native;->flags1:S

    return v0
.end method

.method public getFlags2()S
    .locals 1

    .prologue
    .line 236
    iget-short v0, p0, Lorg/apache/poi/poifs/filesystem/Ole10Native;->flags2:S

    return v0
.end method

.method public getFlags3()S
    .locals 1

    .prologue
    .line 296
    iget-short v0, p0, Lorg/apache/poi/poifs/filesystem/Ole10Native;->flags3:S

    return v0
.end method

.method public getLabel()Ljava/lang/String;
    .locals 1

    .prologue
    .line 217
    iget-object v0, p0, Lorg/apache/poi/poifs/filesystem/Ole10Native;->label:Ljava/lang/String;

    return-object v0
.end method

.method public getTotalSize()I
    .locals 1

    .prologue
    .line 198
    iget v0, p0, Lorg/apache/poi/poifs/filesystem/Ole10Native;->totalSize:I

    return v0
.end method

.method public getUnknown1()[B
    .locals 1

    .prologue
    .line 245
    iget-object v0, p0, Lorg/apache/poi/poifs/filesystem/Ole10Native;->unknown1:[B

    return-object v0
.end method

.method public getUnknown2()[B
    .locals 1

    .prologue
    .line 254
    iget-object v0, p0, Lorg/apache/poi/poifs/filesystem/Ole10Native;->unknown2:[B

    return-object v0
.end method
