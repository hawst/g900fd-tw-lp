.class public Lorg/apache/poi/poifs/crypt/EncryptionHeader;
.super Ljava/lang/Object;
.source "EncryptionHeader.java"


# static fields
.field public static final ALGORITHM_AES_128:I = 0x660e

.field public static final ALGORITHM_AES_192:I = 0x660f

.field public static final ALGORITHM_AES_256:I = 0x6610

.field public static final ALGORITHM_RC4:I = 0x6801

.field public static final HASH_SHA1:I = 0x8004

.field public static final MODE_CBC:I = 0x2

.field public static final MODE_CFB:I = 0x3

.field public static final MODE_ECB:I = 0x1

.field public static final PROVIDER_AES:I = 0x18

.field public static final PROVIDER_RC4:I = 0x1


# instance fields
.field private final algorithm:I

.field private final cipherMode:I

.field private final cspName:Ljava/lang/String;

.field private final flags:I

.field private final hashAlgorithm:I

.field private final keySize:I

.field private final providerType:I

.field private final sizeExtra:I


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 12
    .param p1, "descriptor"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v11, 0x18

    const/4 v10, 0x0

    .line 84
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 88
    :try_start_0
    new-instance v6, Ljava/io/ByteArrayInputStream;

    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v8

    invoke-direct {v6, v8}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 89
    .local v6, "is":Ljava/io/ByteArrayInputStream;
    invoke-static {}, Ljavax/xml/parsers/DocumentBuilderFactory;->newInstance()Ljavax/xml/parsers/DocumentBuilderFactory;

    move-result-object v8

    .line 90
    invoke-virtual {v8}, Ljavax/xml/parsers/DocumentBuilderFactory;->newDocumentBuilder()Ljavax/xml/parsers/DocumentBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljavax/xml/parsers/DocumentBuilder;->parse(Ljava/io/InputStream;)Lorg/w3c/dom/Document;

    move-result-object v8

    .line 91
    const-string/jumbo v9, "keyData"

    invoke-interface {v8, v9}, Lorg/w3c/dom/Document;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v8

    const/4 v9, 0x0

    invoke-interface {v8, v9}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v8

    invoke-interface {v8}, Lorg/w3c/dom/Node;->getAttributes()Lorg/w3c/dom/NamedNodeMap;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v7

    .line 96
    .local v7, "keyData":Lorg/w3c/dom/NamedNodeMap;
    const-string/jumbo v8, "keyBits"

    invoke-interface {v7, v8}, Lorg/w3c/dom/NamedNodeMap;->getNamedItem(Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v8

    .line 97
    invoke-interface {v8}, Lorg/w3c/dom/Node;->getNodeValue()Ljava/lang/String;

    move-result-object v8

    .line 96
    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    iput v8, p0, Lorg/apache/poi/poifs/crypt/EncryptionHeader;->keySize:I

    .line 98
    iput v10, p0, Lorg/apache/poi/poifs/crypt/EncryptionHeader;->flags:I

    .line 99
    iput v10, p0, Lorg/apache/poi/poifs/crypt/EncryptionHeader;->sizeExtra:I

    .line 100
    const/4 v8, 0x0

    iput-object v8, p0, Lorg/apache/poi/poifs/crypt/EncryptionHeader;->cspName:Ljava/lang/String;

    .line 102
    const-string/jumbo v8, "blockSize"

    invoke-interface {v7, v8}, Lorg/w3c/dom/NamedNodeMap;->getNamedItem(Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v8

    .line 103
    invoke-interface {v8}, Lorg/w3c/dom/Node;->getNodeValue()Ljava/lang/String;

    move-result-object v8

    .line 102
    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 104
    .local v0, "blockSize":I
    const-string/jumbo v8, "cipherAlgorithm"

    invoke-interface {v7, v8}, Lorg/w3c/dom/NamedNodeMap;->getNamedItem(Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v8

    invoke-interface {v8}, Lorg/w3c/dom/Node;->getNodeValue()Ljava/lang/String;

    move-result-object v2

    .line 106
    .local v2, "cipher":Ljava/lang/String;
    const-string/jumbo v8, "AES"

    invoke-virtual {v8, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 107
    iput v11, p0, Lorg/apache/poi/poifs/crypt/EncryptionHeader;->providerType:I

    .line 108
    const/16 v8, 0x10

    if-ne v0, v8, :cond_0

    .line 109
    const/16 v8, 0x660e

    iput v8, p0, Lorg/apache/poi/poifs/crypt/EncryptionHeader;->algorithm:I

    .line 120
    :goto_0
    const-string/jumbo v8, "cipherChaining"

    invoke-interface {v7, v8}, Lorg/w3c/dom/NamedNodeMap;->getNamedItem(Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v8

    invoke-interface {v8}, Lorg/w3c/dom/Node;->getNodeValue()Ljava/lang/String;

    move-result-object v1

    .line 122
    .local v1, "chaining":Ljava/lang/String;
    const-string/jumbo v8, "ChainingModeCBC"

    invoke-virtual {v8, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 123
    const/4 v8, 0x2

    iput v8, p0, Lorg/apache/poi/poifs/crypt/EncryptionHeader;->cipherMode:I

    .line 129
    :goto_1
    const-string/jumbo v8, "hashAlgorithm"

    invoke-interface {v7, v8}, Lorg/w3c/dom/NamedNodeMap;->getNamedItem(Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v8

    invoke-interface {v8}, Lorg/w3c/dom/Node;->getNodeValue()Ljava/lang/String;

    move-result-object v4

    .line 130
    .local v4, "hashAlg":Ljava/lang/String;
    const-string/jumbo v8, "hashSize"

    invoke-interface {v7, v8}, Lorg/w3c/dom/NamedNodeMap;->getNamedItem(Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v8

    .line 131
    invoke-interface {v8}, Lorg/w3c/dom/Node;->getNodeValue()Ljava/lang/String;

    move-result-object v8

    .line 130
    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    .line 133
    .local v5, "hashSize":I
    const-string/jumbo v8, "SHA1"

    invoke-virtual {v8, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_6

    const/16 v8, 0x14

    if-ne v5, v8, :cond_6

    .line 134
    const v8, 0x8004

    iput v8, p0, Lorg/apache/poi/poifs/crypt/EncryptionHeader;->hashAlgorithm:I

    .line 145
    return-void

    .line 92
    .end local v0    # "blockSize":I
    .end local v1    # "chaining":Ljava/lang/String;
    .end local v2    # "cipher":Ljava/lang/String;
    .end local v4    # "hashAlg":Ljava/lang/String;
    .end local v5    # "hashSize":I
    .end local v6    # "is":Ljava/io/ByteArrayInputStream;
    .end local v7    # "keyData":Lorg/w3c/dom/NamedNodeMap;
    :catch_0
    move-exception v3

    .line 93
    .local v3, "e":Ljava/lang/Exception;
    new-instance v8, Lorg/apache/poi/EncryptedDocumentException;

    const-string/jumbo v9, "Unable to parse keyData"

    invoke-direct {v8, v9}, Lorg/apache/poi/EncryptedDocumentException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 110
    .end local v3    # "e":Ljava/lang/Exception;
    .restart local v0    # "blockSize":I
    .restart local v2    # "cipher":Ljava/lang/String;
    .restart local v6    # "is":Ljava/io/ByteArrayInputStream;
    .restart local v7    # "keyData":Lorg/w3c/dom/NamedNodeMap;
    :cond_0
    if-ne v0, v11, :cond_1

    .line 111
    const/16 v8, 0x660f

    iput v8, p0, Lorg/apache/poi/poifs/crypt/EncryptionHeader;->algorithm:I

    goto :goto_0

    .line 112
    :cond_1
    const/16 v8, 0x20

    if-ne v0, v8, :cond_2

    .line 113
    const/16 v8, 0x6610

    iput v8, p0, Lorg/apache/poi/poifs/crypt/EncryptionHeader;->algorithm:I

    goto :goto_0

    .line 115
    :cond_2
    new-instance v8, Lorg/apache/poi/EncryptedDocumentException;

    const-string/jumbo v9, "Unsupported key length"

    invoke-direct {v8, v9}, Lorg/apache/poi/EncryptedDocumentException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 117
    :cond_3
    new-instance v8, Lorg/apache/poi/EncryptedDocumentException;

    const-string/jumbo v9, "Unsupported cipher"

    invoke-direct {v8, v9}, Lorg/apache/poi/EncryptedDocumentException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 124
    .restart local v1    # "chaining":Ljava/lang/String;
    :cond_4
    const-string/jumbo v8, "ChainingModeCFB"

    invoke-virtual {v8, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 125
    const/4 v8, 0x3

    iput v8, p0, Lorg/apache/poi/poifs/crypt/EncryptionHeader;->cipherMode:I

    goto :goto_1

    .line 127
    :cond_5
    new-instance v8, Lorg/apache/poi/EncryptedDocumentException;

    const-string/jumbo v9, "Unsupported chaining mode"

    invoke-direct {v8, v9}, Lorg/apache/poi/EncryptedDocumentException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 136
    .restart local v4    # "hashAlg":Ljava/lang/String;
    .restart local v5    # "hashSize":I
    :cond_6
    new-instance v8, Lorg/apache/poi/EncryptedDocumentException;

    const-string/jumbo v9, "Unsupported hash algorithm"

    invoke-direct {v8, v9}, Lorg/apache/poi/EncryptedDocumentException;-><init>(Ljava/lang/String;)V

    throw v8
.end method

.method public constructor <init>(Lorg/apache/poi/poifs/filesystem/DocumentInputStream;)V
    .locals 3
    .param p1, "is"    # Lorg/apache/poi/poifs/filesystem/DocumentInputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    invoke-virtual {p1}, Lorg/apache/poi/poifs/filesystem/DocumentInputStream;->readInt()I

    move-result v2

    iput v2, p0, Lorg/apache/poi/poifs/crypt/EncryptionHeader;->flags:I

    .line 60
    invoke-virtual {p1}, Lorg/apache/poi/poifs/filesystem/DocumentInputStream;->readInt()I

    move-result v2

    iput v2, p0, Lorg/apache/poi/poifs/crypt/EncryptionHeader;->sizeExtra:I

    .line 61
    invoke-virtual {p1}, Lorg/apache/poi/poifs/filesystem/DocumentInputStream;->readInt()I

    move-result v2

    iput v2, p0, Lorg/apache/poi/poifs/crypt/EncryptionHeader;->algorithm:I

    .line 62
    invoke-virtual {p1}, Lorg/apache/poi/poifs/filesystem/DocumentInputStream;->readInt()I

    move-result v2

    iput v2, p0, Lorg/apache/poi/poifs/crypt/EncryptionHeader;->hashAlgorithm:I

    .line 63
    invoke-virtual {p1}, Lorg/apache/poi/poifs/filesystem/DocumentInputStream;->readInt()I

    move-result v2

    iput v2, p0, Lorg/apache/poi/poifs/crypt/EncryptionHeader;->keySize:I

    .line 64
    invoke-virtual {p1}, Lorg/apache/poi/poifs/filesystem/DocumentInputStream;->readInt()I

    move-result v2

    iput v2, p0, Lorg/apache/poi/poifs/crypt/EncryptionHeader;->providerType:I

    .line 66
    invoke-virtual {p1}, Lorg/apache/poi/poifs/filesystem/DocumentInputStream;->readLong()J

    .line 68
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 71
    .local v0, "builder":Ljava/lang/StringBuilder;
    :goto_0
    invoke-virtual {p1}, Lorg/apache/poi/poifs/filesystem/DocumentInputStream;->readShort()S

    move-result v2

    int-to-char v1, v2

    .line 73
    .local v1, "c":C
    if-nez v1, :cond_0

    .line 79
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lorg/apache/poi/poifs/crypt/EncryptionHeader;->cspName:Ljava/lang/String;

    .line 80
    const/4 v2, 0x1

    iput v2, p0, Lorg/apache/poi/poifs/crypt/EncryptionHeader;->cipherMode:I

    .line 82
    return-void

    .line 77
    :cond_0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_0
.end method


# virtual methods
.method public getAlgorithm()I
    .locals 1

    .prologue
    .line 160
    iget v0, p0, Lorg/apache/poi/poifs/crypt/EncryptionHeader;->algorithm:I

    return v0
.end method

.method public getCipherMode()I
    .locals 1

    .prologue
    .line 148
    iget v0, p0, Lorg/apache/poi/poifs/crypt/EncryptionHeader;->cipherMode:I

    return v0
.end method

.method public getCspName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 180
    iget-object v0, p0, Lorg/apache/poi/poifs/crypt/EncryptionHeader;->cspName:Ljava/lang/String;

    return-object v0
.end method

.method public getFlags()I
    .locals 1

    .prologue
    .line 152
    iget v0, p0, Lorg/apache/poi/poifs/crypt/EncryptionHeader;->flags:I

    return v0
.end method

.method public getHashAlgorithm()I
    .locals 1

    .prologue
    .line 164
    iget v0, p0, Lorg/apache/poi/poifs/crypt/EncryptionHeader;->hashAlgorithm:I

    return v0
.end method

.method public getKeySalt()[B
    .locals 1

    .prologue
    .line 172
    const/4 v0, 0x0

    return-object v0
.end method

.method public getKeySize()I
    .locals 1

    .prologue
    .line 168
    iget v0, p0, Lorg/apache/poi/poifs/crypt/EncryptionHeader;->keySize:I

    return v0
.end method

.method public getProviderType()I
    .locals 1

    .prologue
    .line 176
    iget v0, p0, Lorg/apache/poi/poifs/crypt/EncryptionHeader;->providerType:I

    return v0
.end method

.method public getSizeExtra()I
    .locals 1

    .prologue
    .line 156
    iget v0, p0, Lorg/apache/poi/poifs/crypt/EncryptionHeader;->sizeExtra:I

    return v0
.end method
