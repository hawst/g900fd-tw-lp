.class public Lorg/apache/poi/poifs/filesystem/NPOIFSStream;
.super Ljava/lang/Object;
.source "NPOIFSStream.java"

# interfaces
.implements Ljava/lang/Iterable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/poi/poifs/filesystem/NPOIFSStream$StreamBlockByteBufferIterator;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Iterable",
        "<",
        "Ljava/nio/ByteBuffer;",
        ">;"
    }
.end annotation


# instance fields
.field private blockStore:Lorg/apache/poi/poifs/filesystem/BlockStore;

.field private startBlock:I


# direct methods
.method public constructor <init>(Lorg/apache/poi/poifs/filesystem/BlockStore;)V
    .locals 1
    .param p1, "blockStore"    # Lorg/apache/poi/poifs/filesystem/BlockStore;

    .prologue
    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66
    iput-object p1, p0, Lorg/apache/poi/poifs/filesystem/NPOIFSStream;->blockStore:Lorg/apache/poi/poifs/filesystem/BlockStore;

    .line 67
    const/4 v0, -0x2

    iput v0, p0, Lorg/apache/poi/poifs/filesystem/NPOIFSStream;->startBlock:I

    .line 68
    return-void
.end method

.method public constructor <init>(Lorg/apache/poi/poifs/filesystem/BlockStore;I)V
    .locals 0
    .param p1, "blockStore"    # Lorg/apache/poi/poifs/filesystem/BlockStore;
    .param p2, "startBlock"    # I

    .prologue
    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    iput-object p1, p0, Lorg/apache/poi/poifs/filesystem/NPOIFSStream;->blockStore:Lorg/apache/poi/poifs/filesystem/BlockStore;

    .line 58
    iput p2, p0, Lorg/apache/poi/poifs/filesystem/NPOIFSStream;->startBlock:I

    .line 59
    return-void
.end method

.method static synthetic access$0(Lorg/apache/poi/poifs/filesystem/NPOIFSStream;)Lorg/apache/poi/poifs/filesystem/BlockStore;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lorg/apache/poi/poifs/filesystem/NPOIFSStream;->blockStore:Lorg/apache/poi/poifs/filesystem/BlockStore;

    return-object v0
.end method

.method private free(Lorg/apache/poi/poifs/filesystem/BlockStore$ChainLoopDetector;)V
    .locals 5
    .param p1, "loopDetector"    # Lorg/apache/poi/poifs/filesystem/BlockStore$ChainLoopDetector;

    .prologue
    const/4 v4, -0x2

    .line 173
    iget v0, p0, Lorg/apache/poi/poifs/filesystem/NPOIFSStream;->startBlock:I

    .line 174
    .local v0, "nextBlock":I
    :goto_0
    if-ne v0, v4, :cond_0

    .line 180
    iput v4, p0, Lorg/apache/poi/poifs/filesystem/NPOIFSStream;->startBlock:I

    .line 181
    return-void

    .line 175
    :cond_0
    move v1, v0

    .line 176
    .local v1, "thisBlock":I
    invoke-virtual {p1, v1}, Lorg/apache/poi/poifs/filesystem/BlockStore$ChainLoopDetector;->claim(I)V

    .line 177
    iget-object v2, p0, Lorg/apache/poi/poifs/filesystem/NPOIFSStream;->blockStore:Lorg/apache/poi/poifs/filesystem/BlockStore;

    invoke-virtual {v2, v1}, Lorg/apache/poi/poifs/filesystem/BlockStore;->getNextBlock(I)I

    move-result v0

    .line 178
    iget-object v2, p0, Lorg/apache/poi/poifs/filesystem/NPOIFSStream;->blockStore:Lorg/apache/poi/poifs/filesystem/BlockStore;

    const/4 v3, -0x1

    invoke-virtual {v2, v1, v3}, Lorg/apache/poi/poifs/filesystem/BlockStore;->setNextBlock(II)V

    goto :goto_0
.end method


# virtual methods
.method public free()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 169
    iget-object v1, p0, Lorg/apache/poi/poifs/filesystem/NPOIFSStream;->blockStore:Lorg/apache/poi/poifs/filesystem/BlockStore;

    invoke-virtual {v1}, Lorg/apache/poi/poifs/filesystem/BlockStore;->getChainLoopDetector()Lorg/apache/poi/poifs/filesystem/BlockStore$ChainLoopDetector;

    move-result-object v0

    .line 170
    .local v0, "loopDetector":Lorg/apache/poi/poifs/filesystem/BlockStore$ChainLoopDetector;
    invoke-direct {p0, v0}, Lorg/apache/poi/poifs/filesystem/NPOIFSStream;->free(Lorg/apache/poi/poifs/filesystem/BlockStore$ChainLoopDetector;)V

    .line 171
    return-void
.end method

.method public getBlockIterator()Ljava/util/Iterator;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Ljava/nio/ByteBuffer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 88
    iget v0, p0, Lorg/apache/poi/poifs/filesystem/NPOIFSStream;->startBlock:I

    const/4 v1, -0x2

    if-ne v0, v1, :cond_0

    .line 89
    new-instance v0, Ljava/lang/IllegalStateException;

    .line 90
    const-string/jumbo v1, "Can\'t read from a new stream before it has been written to"

    .line 89
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 93
    :cond_0
    new-instance v0, Lorg/apache/poi/poifs/filesystem/NPOIFSStream$StreamBlockByteBufferIterator;

    iget v1, p0, Lorg/apache/poi/poifs/filesystem/NPOIFSStream;->startBlock:I

    invoke-direct {v0, p0, v1}, Lorg/apache/poi/poifs/filesystem/NPOIFSStream$StreamBlockByteBufferIterator;-><init>(Lorg/apache/poi/poifs/filesystem/NPOIFSStream;I)V

    return-object v0
.end method

.method public getStartBlock()I
    .locals 1

    .prologue
    .line 76
    iget v0, p0, Lorg/apache/poi/poifs/filesystem/NPOIFSStream;->startBlock:I

    return v0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Ljava/nio/ByteBuffer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 84
    invoke-virtual {p0}, Lorg/apache/poi/poifs/filesystem/NPOIFSStream;->getBlockIterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public updateContents([B)V
    .locals 18
    .param p1, "contents"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 104
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/poi/poifs/filesystem/NPOIFSStream;->blockStore:Lorg/apache/poi/poifs/filesystem/BlockStore;

    invoke-virtual {v14}, Lorg/apache/poi/poifs/filesystem/BlockStore;->getBlockStoreBlockSize()I

    move-result v2

    .line 105
    .local v2, "blockSize":I
    move-object/from16 v0, p1

    array-length v14, v0

    int-to-double v14, v14

    int-to-double v0, v2

    move-wide/from16 v16, v0

    div-double v14, v14, v16

    invoke-static {v14, v15}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v14

    double-to-int v3, v14

    .line 109
    .local v3, "blocks":I
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/poi/poifs/filesystem/NPOIFSStream;->blockStore:Lorg/apache/poi/poifs/filesystem/BlockStore;

    invoke-virtual {v14}, Lorg/apache/poi/poifs/filesystem/BlockStore;->getChainLoopDetector()Lorg/apache/poi/poifs/filesystem/BlockStore$ChainLoopDetector;

    move-result-object v8

    .line 112
    .local v8, "loopDetector":Lorg/apache/poi/poifs/filesystem/BlockStore$ChainLoopDetector;
    const/4 v10, -0x2

    .line 113
    .local v10, "prevBlock":I
    move-object/from16 v0, p0

    iget v9, v0, Lorg/apache/poi/poifs/filesystem/NPOIFSStream;->startBlock:I

    .line 114
    .local v9, "nextBlock":I
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_0
    if-lt v6, v3, :cond_0

    .line 151
    move v7, v10

    .line 154
    .local v7, "lastBlock":I
    new-instance v13, Lorg/apache/poi/poifs/filesystem/NPOIFSStream;

    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/poi/poifs/filesystem/NPOIFSStream;->blockStore:Lorg/apache/poi/poifs/filesystem/BlockStore;

    invoke-direct {v13, v14, v9}, Lorg/apache/poi/poifs/filesystem/NPOIFSStream;-><init>(Lorg/apache/poi/poifs/filesystem/BlockStore;I)V

    .line 155
    .local v13, "toFree":Lorg/apache/poi/poifs/filesystem/NPOIFSStream;
    invoke-direct {v13, v8}, Lorg/apache/poi/poifs/filesystem/NPOIFSStream;->free(Lorg/apache/poi/poifs/filesystem/BlockStore$ChainLoopDetector;)V

    .line 158
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/poi/poifs/filesystem/NPOIFSStream;->blockStore:Lorg/apache/poi/poifs/filesystem/BlockStore;

    const/4 v15, -0x2

    invoke-virtual {v14, v7, v15}, Lorg/apache/poi/poifs/filesystem/BlockStore;->setNextBlock(II)V

    .line 159
    return-void

    .line 115
    .end local v7    # "lastBlock":I
    .end local v13    # "toFree":Lorg/apache/poi/poifs/filesystem/NPOIFSStream;
    :cond_0
    move v12, v9

    .line 119
    .local v12, "thisBlock":I
    const/4 v14, -0x2

    if-ne v12, v14, :cond_3

    .line 120
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/poi/poifs/filesystem/NPOIFSStream;->blockStore:Lorg/apache/poi/poifs/filesystem/BlockStore;

    invoke-virtual {v14}, Lorg/apache/poi/poifs/filesystem/BlockStore;->getFreeBlock()I

    move-result v12

    .line 121
    invoke-virtual {v8, v12}, Lorg/apache/poi/poifs/filesystem/BlockStore$ChainLoopDetector;->claim(I)V

    .line 124
    const/4 v9, -0x2

    .line 127
    const/4 v14, -0x2

    if-eq v10, v14, :cond_1

    .line 128
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/poi/poifs/filesystem/NPOIFSStream;->blockStore:Lorg/apache/poi/poifs/filesystem/BlockStore;

    invoke-virtual {v14, v10, v12}, Lorg/apache/poi/poifs/filesystem/BlockStore;->setNextBlock(II)V

    .line 130
    :cond_1
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/poi/poifs/filesystem/NPOIFSStream;->blockStore:Lorg/apache/poi/poifs/filesystem/BlockStore;

    const/4 v15, -0x2

    invoke-virtual {v14, v12, v15}, Lorg/apache/poi/poifs/filesystem/BlockStore;->setNextBlock(II)V

    .line 134
    move-object/from16 v0, p0

    iget v14, v0, Lorg/apache/poi/poifs/filesystem/NPOIFSStream;->startBlock:I

    const/4 v15, -0x2

    if-ne v14, v15, :cond_2

    .line 135
    move-object/from16 v0, p0

    iput v12, v0, Lorg/apache/poi/poifs/filesystem/NPOIFSStream;->startBlock:I

    .line 143
    :cond_2
    :goto_1
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/poi/poifs/filesystem/NPOIFSStream;->blockStore:Lorg/apache/poi/poifs/filesystem/BlockStore;

    invoke-virtual {v14, v12}, Lorg/apache/poi/poifs/filesystem/BlockStore;->createBlockIfNeeded(I)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 144
    .local v4, "buffer":Ljava/nio/ByteBuffer;
    mul-int v11, v6, v2

    .line 145
    .local v11, "startAt":I
    move-object/from16 v0, p1

    array-length v14, v0

    sub-int/2addr v14, v11

    invoke-static {v14, v2}, Ljava/lang/Math;->min(II)I

    move-result v5

    .line 146
    .local v5, "endAt":I
    move-object/from16 v0, p1

    invoke-virtual {v4, v0, v11, v5}, Ljava/nio/ByteBuffer;->put([BII)Ljava/nio/ByteBuffer;

    .line 149
    move v10, v12

    .line 114
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 138
    .end local v4    # "buffer":Ljava/nio/ByteBuffer;
    .end local v5    # "endAt":I
    .end local v11    # "startAt":I
    :cond_3
    invoke-virtual {v8, v12}, Lorg/apache/poi/poifs/filesystem/BlockStore$ChainLoopDetector;->claim(I)V

    .line 139
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/poi/poifs/filesystem/NPOIFSStream;->blockStore:Lorg/apache/poi/poifs/filesystem/BlockStore;

    invoke-virtual {v14, v12}, Lorg/apache/poi/poifs/filesystem/BlockStore;->getNextBlock(I)I

    move-result v9

    goto :goto_1
.end method
