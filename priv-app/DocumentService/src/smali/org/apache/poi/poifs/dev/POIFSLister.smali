.class public Lorg/apache/poi/poifs/dev/POIFSLister;
.super Ljava/lang/Object;
.source "POIFSLister.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static displayDirectory(Lorg/apache/poi/poifs/filesystem/DirectoryNode;Ljava/lang/String;Z)V
    .locals 12
    .param p0, "dir"    # Lorg/apache/poi/poifs/filesystem/DirectoryNode;
    .param p1, "indent"    # Ljava/lang/String;
    .param p2, "withSizes"    # Z

    .prologue
    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 77
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v9, "  "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 79
    .local v6, "newIndent":Ljava/lang/String;
    const/4 v3, 0x0

    .line 80
    .local v3, "hadChildren":Z
    invoke-virtual {p0}, Lorg/apache/poi/poifs/filesystem/DirectoryNode;->getEntries()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/poi/poifs/filesystem/Entry;>;"
    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-nez v8, :cond_1

    .line 103
    return-void

    .line 81
    :cond_1
    const/4 v3, 0x1

    .line 82
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/poi/poifs/filesystem/Entry;

    .line 83
    .local v2, "entry":Lorg/apache/poi/poifs/filesystem/Entry;
    instance-of v8, v2, Lorg/apache/poi/poifs/filesystem/DirectoryNode;

    if-eqz v8, :cond_2

    .line 84
    check-cast v2, Lorg/apache/poi/poifs/filesystem/DirectoryNode;

    .end local v2    # "entry":Lorg/apache/poi/poifs/filesystem/Entry;
    invoke-static {v2, v6, p2}, Lorg/apache/poi/poifs/dev/POIFSLister;->displayDirectory(Lorg/apache/poi/poifs/filesystem/DirectoryNode;Ljava/lang/String;Z)V

    goto :goto_0

    .restart local v2    # "entry":Lorg/apache/poi/poifs/filesystem/Entry;
    :cond_2
    move-object v1, v2

    .line 86
    check-cast v1, Lorg/apache/poi/poifs/filesystem/DocumentNode;

    .line 87
    .local v1, "doc":Lorg/apache/poi/poifs/filesystem/DocumentNode;
    invoke-virtual {v1}, Lorg/apache/poi/poifs/filesystem/DocumentNode;->getName()Ljava/lang/String;

    move-result-object v5

    .line 88
    .local v5, "name":Ljava/lang/String;
    const-string/jumbo v7, ""

    .line 89
    .local v7, "size":Ljava/lang/String;
    invoke-virtual {v5, v10}, Ljava/lang/String;->charAt(I)C

    move-result v8

    const/16 v9, 0xa

    if-ge v8, v9, :cond_3

    .line 90
    new-instance v8, Ljava/lang/StringBuilder;

    const-string/jumbo v9, "(0x0"

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v10}, Ljava/lang/String;->charAt(I)C

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, ")"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v5, v11}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 91
    .local v0, "altname":Ljava/lang/String;
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-virtual {v5, v11}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v9, " <"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, ">"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 93
    .end local v0    # "altname":Ljava/lang/String;
    :cond_3
    if-eqz p2, :cond_0

    .line 94
    new-instance v8, Ljava/lang/StringBuilder;

    const-string/jumbo v9, " ["

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lorg/apache/poi/poifs/filesystem/DocumentNode;->getSize()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, " / 0x"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    .line 95
    invoke-virtual {v1}, Lorg/apache/poi/poifs/filesystem/DocumentNode;->getSize()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, "]"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    .line 94
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    goto/16 :goto_0
.end method

.method public static main([Ljava/lang/String;)V
    .locals 5
    .param p0, "args"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 43
    array-length v3, p0

    if-nez v3, :cond_0

    .line 44
    sget-object v3, Ljava/lang/System;->err:Ljava/io/PrintStream;

    const-string/jumbo v4, "Must specify at least one file to view"

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 45
    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/System;->exit(I)V

    .line 48
    :cond_0
    const/4 v2, 0x0

    .line 49
    .local v2, "withSizes":Z
    const/4 v1, 0x1

    .line 50
    .local v1, "newPOIFS":Z
    const/4 v0, 0x0

    .local v0, "j":I
    :goto_0
    array-length v3, p0

    if-lt v0, v3, :cond_1

    .line 63
    return-void

    .line 51
    :cond_1
    aget-object v3, p0, v0

    const-string/jumbo v4, "-size"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    aget-object v3, p0, v0

    const-string/jumbo v4, "-sizes"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 52
    :cond_2
    const/4 v2, 0x1

    .line 50
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 53
    :cond_3
    aget-object v3, p0, v0

    const-string/jumbo v4, "-old"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_4

    aget-object v3, p0, v0

    const-string/jumbo v4, "-old-poifs"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 54
    :cond_4
    const/4 v1, 0x0

    .line 55
    goto :goto_1

    .line 56
    :cond_5
    if-eqz v1, :cond_6

    .line 57
    aget-object v3, p0, v0

    invoke-static {v3, v2}, Lorg/apache/poi/poifs/dev/POIFSLister;->viewFile(Ljava/lang/String;Z)V

    goto :goto_1

    .line 59
    :cond_6
    aget-object v3, p0, v0

    invoke-static {v3, v2}, Lorg/apache/poi/poifs/dev/POIFSLister;->viewFileOld(Ljava/lang/String;Z)V

    goto :goto_1
.end method

.method public static viewFile(Ljava/lang/String;Z)V
    .locals 3
    .param p0, "filename"    # Ljava/lang/String;
    .param p1, "withSizes"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 66
    new-instance v0, Lorg/apache/poi/poifs/filesystem/NPOIFSFileSystem;

    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v1}, Lorg/apache/poi/poifs/filesystem/NPOIFSFileSystem;-><init>(Ljava/io/File;)V

    .line 67
    .local v0, "fs":Lorg/apache/poi/poifs/filesystem/NPOIFSFileSystem;
    invoke-virtual {v0}, Lorg/apache/poi/poifs/filesystem/NPOIFSFileSystem;->getRoot()Lorg/apache/poi/poifs/filesystem/DirectoryNode;

    move-result-object v1

    const-string/jumbo v2, ""

    invoke-static {v1, v2, p1}, Lorg/apache/poi/poifs/dev/POIFSLister;->displayDirectory(Lorg/apache/poi/poifs/filesystem/DirectoryNode;Ljava/lang/String;Z)V

    .line 68
    return-void
.end method

.method public static viewFileOld(Ljava/lang/String;Z)V
    .locals 3
    .param p0, "filename"    # Ljava/lang/String;
    .param p1, "withSizes"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 71
    new-instance v0, Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;

    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, p0}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v1}, Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;-><init>(Ljava/io/InputStream;)V

    .line 72
    .local v0, "fs":Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;
    invoke-virtual {v0}, Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;->getRoot()Lorg/apache/poi/poifs/filesystem/DirectoryNode;

    move-result-object v1

    const-string/jumbo v2, ""

    invoke-static {v1, v2, p1}, Lorg/apache/poi/poifs/dev/POIFSLister;->displayDirectory(Lorg/apache/poi/poifs/filesystem/DirectoryNode;Ljava/lang/String;Z)V

    .line 73
    return-void
.end method
