.class public Lorg/apache/tika/utils/CharsetUtils;
.super Ljava/lang/Object;
.source "CharsetUtils.java"


# static fields
.field private static final CHARSET_NAME_PATTERN:Ljava/util/regex/Pattern;

.field private static final COMMON_CHARSETS:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/nio/charset/Charset;",
            ">;"
        }
    .end annotation
.end field

.field private static final CP_NAME_PATTERN:Ljava/util/regex/Pattern;

.field private static final ISO_NAME_PATTERN:Ljava/util/regex/Pattern;

.field private static final WIN_NAME_PATTERN:Ljava/util/regex/Pattern;

.field private static getCharsetICU:Ljava/lang/reflect/Method;

.field private static isSupportedICU:Ljava/lang/reflect/Method;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 33
    const-string/jumbo v2, "[ \\\"]*([^ >,;\\\"]+).*"

    invoke-static {v2}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v2

    .line 32
    sput-object v2, Lorg/apache/tika/utils/CharsetUtils;->CHARSET_NAME_PATTERN:Ljava/util/regex/Pattern;

    .line 36
    const-string/jumbo v2, ".*8859-(\\d+)"

    invoke-static {v2}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v2

    .line 35
    sput-object v2, Lorg/apache/tika/utils/CharsetUtils;->ISO_NAME_PATTERN:Ljava/util/regex/Pattern;

    .line 39
    const-string/jumbo v2, "cp-(\\d+)"

    invoke-static {v2}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v2

    .line 38
    sput-object v2, Lorg/apache/tika/utils/CharsetUtils;->CP_NAME_PATTERN:Ljava/util/regex/Pattern;

    .line 42
    const-string/jumbo v2, "win-?(\\d+)"

    invoke-static {v2}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v2

    .line 41
    sput-object v2, Lorg/apache/tika/utils/CharsetUtils;->WIN_NAME_PATTERN:Ljava/util/regex/Pattern;

    .line 45
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 44
    sput-object v2, Lorg/apache/tika/utils/CharsetUtils;->COMMON_CHARSETS:Ljava/util/Map;

    .line 47
    sput-object v3, Lorg/apache/tika/utils/CharsetUtils;->getCharsetICU:Ljava/lang/reflect/Method;

    .line 48
    sput-object v3, Lorg/apache/tika/utils/CharsetUtils;->isSupportedICU:Ljava/lang/reflect/Method;

    .line 66
    const/16 v2, 0x20

    new-array v2, v2, [Ljava/lang/String;

    .line 68
    const-string/jumbo v3, "Big5"

    aput-object v3, v2, v4

    .line 69
    const-string/jumbo v3, "EUC-JP"

    aput-object v3, v2, v5

    const/4 v3, 0x2

    const-string/jumbo v4, "EUC-KR"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    const-string/jumbo v4, "x-EUC-TW"

    aput-object v4, v2, v3

    const/4 v3, 0x4

    .line 70
    const-string/jumbo v4, "GB18030"

    aput-object v4, v2, v3

    const/4 v3, 0x5

    .line 71
    const-string/jumbo v4, "IBM855"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string/jumbo v4, "IBM866"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    .line 72
    const-string/jumbo v4, "ISO-2022-CN"

    aput-object v4, v2, v3

    const/16 v3, 0x8

    const-string/jumbo v4, "ISO-2022-JP"

    aput-object v4, v2, v3

    const/16 v3, 0x9

    const-string/jumbo v4, "ISO-2022-KR"

    aput-object v4, v2, v3

    const/16 v3, 0xa

    .line 73
    const-string/jumbo v4, "ISO-8859-1"

    aput-object v4, v2, v3

    const/16 v3, 0xb

    const-string/jumbo v4, "ISO-8859-2"

    aput-object v4, v2, v3

    const/16 v3, 0xc

    const-string/jumbo v4, "ISO-8859-3"

    aput-object v4, v2, v3

    const/16 v3, 0xd

    const-string/jumbo v4, "ISO-8859-4"

    aput-object v4, v2, v3

    const/16 v3, 0xe

    .line 74
    const-string/jumbo v4, "ISO-8859-5"

    aput-object v4, v2, v3

    const/16 v3, 0xf

    const-string/jumbo v4, "ISO-8859-6"

    aput-object v4, v2, v3

    const/16 v3, 0x10

    const-string/jumbo v4, "ISO-8859-7"

    aput-object v4, v2, v3

    const/16 v3, 0x11

    const-string/jumbo v4, "ISO-8859-8"

    aput-object v4, v2, v3

    const/16 v3, 0x12

    .line 75
    const-string/jumbo v4, "ISO-8859-9"

    aput-object v4, v2, v3

    const/16 v3, 0x13

    const-string/jumbo v4, "ISO-8859-11"

    aput-object v4, v2, v3

    const/16 v3, 0x14

    const-string/jumbo v4, "ISO-8859-13"

    aput-object v4, v2, v3

    const/16 v3, 0x15

    const-string/jumbo v4, "ISO-8859-15"

    aput-object v4, v2, v3

    const/16 v3, 0x16

    .line 76
    const-string/jumbo v4, "KOI8-R"

    aput-object v4, v2, v3

    const/16 v3, 0x17

    .line 77
    const-string/jumbo v4, "x-MacCyrillic"

    aput-object v4, v2, v3

    const/16 v3, 0x18

    .line 78
    const-string/jumbo v4, "SHIFT_JIS"

    aput-object v4, v2, v3

    const/16 v3, 0x19

    .line 79
    const-string/jumbo v4, "UTF-8"

    aput-object v4, v2, v3

    const/16 v3, 0x1a

    const-string/jumbo v4, "UTF-16BE"

    aput-object v4, v2, v3

    const/16 v3, 0x1b

    const-string/jumbo v4, "UTF-16LE"

    aput-object v4, v2, v3

    const/16 v3, 0x1c

    .line 80
    const-string/jumbo v4, "windows-1251"

    aput-object v4, v2, v3

    const/16 v3, 0x1d

    const-string/jumbo v4, "windows-1252"

    aput-object v4, v2, v3

    const/16 v3, 0x1e

    const-string/jumbo v4, "windows-1253"

    aput-object v4, v2, v3

    const/16 v3, 0x1f

    const-string/jumbo v4, "windows-1255"

    aput-object v4, v2, v3

    .line 67
    invoke-static {v2}, Lorg/apache/tika/utils/CharsetUtils;->initCommonCharsets([Ljava/lang/String;)Ljava/util/Map;

    .line 83
    sget-object v3, Lorg/apache/tika/utils/CharsetUtils;->COMMON_CHARSETS:Ljava/util/Map;

    const-string/jumbo v4, "iso-8851-1"

    sget-object v2, Lorg/apache/tika/utils/CharsetUtils;->COMMON_CHARSETS:Ljava/util/Map;

    const-string/jumbo v5, "iso-8859-1"

    invoke-interface {v2, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/nio/charset/Charset;

    invoke-interface {v3, v4, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 84
    sget-object v3, Lorg/apache/tika/utils/CharsetUtils;->COMMON_CHARSETS:Ljava/util/Map;

    const-string/jumbo v4, "windows"

    sget-object v2, Lorg/apache/tika/utils/CharsetUtils;->COMMON_CHARSETS:Ljava/util/Map;

    const-string/jumbo v5, "windows-1252"

    invoke-interface {v2, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/nio/charset/Charset;

    invoke-interface {v3, v4, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 85
    sget-object v3, Lorg/apache/tika/utils/CharsetUtils;->COMMON_CHARSETS:Ljava/util/Map;

    const-string/jumbo v4, "koi8r"

    sget-object v2, Lorg/apache/tika/utils/CharsetUtils;->COMMON_CHARSETS:Ljava/util/Map;

    const-string/jumbo v5, "koi8-r"

    invoke-interface {v2, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/nio/charset/Charset;

    invoke-interface {v3, v4, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 88
    const/4 v0, 0x0

    .line 90
    .local v0, "icuCharset":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :try_start_0
    const-class v2, Lorg/apache/tika/utils/CharsetUtils;

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    .line 91
    const-string/jumbo v3, "com.ibm.icu.charset.CharsetICU"

    .line 90
    invoke-virtual {v2, v3}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v0

    .line 94
    :goto_0
    if-eqz v0, :cond_0

    .line 96
    :try_start_1
    const-string/jumbo v2, "forNameICU"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Class;

    const/4 v4, 0x0

    const-class v5, Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-virtual {v0, v2, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    sput-object v2, Lorg/apache/tika/utils/CharsetUtils;->getCharsetICU:Ljava/lang/reflect/Method;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    .line 101
    :try_start_2
    const-string/jumbo v2, "isSupported"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Class;

    const/4 v4, 0x0

    const-class v5, Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-virtual {v0, v2, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    sput-object v2, Lorg/apache/tika/utils/CharsetUtils;->isSupportedICU:Ljava/lang/reflect/Method;
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    .line 107
    :cond_0
    :goto_1
    return-void

    .line 97
    :catch_0
    move-exception v1

    .line 98
    .local v1, "t":Ljava/lang/Throwable;
    new-instance v2, Ljava/lang/RuntimeException;

    invoke-direct {v2, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v2

    .line 102
    .end local v1    # "t":Ljava/lang/Throwable;
    :catch_1
    move-exception v2

    goto :goto_1

    .line 92
    :catch_2
    move-exception v2

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static clean(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "charsetName"    # Ljava/lang/String;

    .prologue
    .line 141
    :try_start_0
    invoke-static {p0}, Lorg/apache/tika/utils/CharsetUtils;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v1

    invoke-virtual {v1}, Ljava/nio/charset/Charset;->name()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 143
    :goto_0
    return-object v1

    .line 142
    :catch_0
    move-exception v0

    .line 143
    .local v0, "e":Ljava/lang/Exception;
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static forName(Ljava/lang/String;)Ljava/nio/charset/Charset;
    .locals 11
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    const/4 v9, 0x1

    .line 152
    if-nez p0, :cond_0

    .line 153
    new-instance v7, Ljava/lang/IllegalArgumentException;

    invoke-direct {v7}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v7

    .line 157
    :cond_0
    sget-object v7, Lorg/apache/tika/utils/CharsetUtils;->CHARSET_NAME_PATTERN:Ljava/util/regex/Pattern;

    invoke-virtual {v7, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v5

    .line 158
    .local v5, "m":Ljava/util/regex/Matcher;
    invoke-virtual {v5}, Ljava/util/regex/Matcher;->matches()Z

    move-result v7

    if-nez v7, :cond_1

    .line 159
    new-instance v7, Ljava/nio/charset/IllegalCharsetNameException;

    invoke-direct {v7, p0}, Ljava/nio/charset/IllegalCharsetNameException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 161
    :cond_1
    invoke-virtual {v5, v9}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object p0

    .line 163
    sget-object v7, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {p0, v7}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v4

    .line 164
    .local v4, "lower":Ljava/lang/String;
    sget-object v7, Lorg/apache/tika/utils/CharsetUtils;->COMMON_CHARSETS:Ljava/util/Map;

    invoke-interface {v7, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/nio/charset/Charset;

    .line 165
    .local v0, "charset":Ljava/nio/charset/Charset;
    if-eqz v0, :cond_3

    move-object v2, v0

    .line 202
    :cond_2
    :goto_0
    return-object v2

    .line 167
    :cond_3
    const-string/jumbo v7, "none"

    invoke-virtual {v7, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_4

    const-string/jumbo v7, "no"

    invoke-virtual {v7, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 168
    :cond_4
    new-instance v7, Ljava/nio/charset/IllegalCharsetNameException;

    invoke-direct {v7, p0}, Ljava/nio/charset/IllegalCharsetNameException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 170
    :cond_5
    sget-object v7, Lorg/apache/tika/utils/CharsetUtils;->ISO_NAME_PATTERN:Ljava/util/regex/Pattern;

    invoke-virtual {v7, v4}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v3

    .line 171
    .local v3, "iso":Ljava/util/regex/Matcher;
    sget-object v7, Lorg/apache/tika/utils/CharsetUtils;->CP_NAME_PATTERN:Ljava/util/regex/Pattern;

    invoke-virtual {v7, v4}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    .line 172
    .local v1, "cp":Ljava/util/regex/Matcher;
    sget-object v7, Lorg/apache/tika/utils/CharsetUtils;->WIN_NAME_PATTERN:Ljava/util/regex/Pattern;

    invoke-virtual {v7, v4}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v6

    .line 173
    .local v6, "win":Ljava/util/regex/Matcher;
    invoke-virtual {v3}, Ljava/util/regex/Matcher;->matches()Z

    move-result v7

    if-eqz v7, :cond_7

    .line 175
    new-instance v7, Ljava/lang/StringBuilder;

    const-string/jumbo v8, "iso-8859-"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v9}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 176
    sget-object v7, Lorg/apache/tika/utils/CharsetUtils;->COMMON_CHARSETS:Ljava/util/Map;

    invoke-interface {v7, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "charset":Ljava/nio/charset/Charset;
    check-cast v0, Ljava/nio/charset/Charset;

    .line 186
    .restart local v0    # "charset":Ljava/nio/charset/Charset;
    :cond_6
    :goto_1
    if-eqz v0, :cond_9

    move-object v2, v0

    .line 187
    goto :goto_0

    .line 177
    :cond_7
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->matches()Z

    move-result v7

    if-eqz v7, :cond_8

    .line 179
    new-instance v7, Ljava/lang/StringBuilder;

    const-string/jumbo v8, "cp"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v9}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 180
    sget-object v7, Lorg/apache/tika/utils/CharsetUtils;->COMMON_CHARSETS:Ljava/util/Map;

    invoke-interface {v7, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "charset":Ljava/nio/charset/Charset;
    check-cast v0, Ljava/nio/charset/Charset;

    .line 181
    .restart local v0    # "charset":Ljava/nio/charset/Charset;
    goto :goto_1

    :cond_8
    invoke-virtual {v6}, Ljava/util/regex/Matcher;->matches()Z

    move-result v7

    if-eqz v7, :cond_6

    .line 183
    new-instance v7, Ljava/lang/StringBuilder;

    const-string/jumbo v8, "windows-"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v9}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 184
    sget-object v7, Lorg/apache/tika/utils/CharsetUtils;->COMMON_CHARSETS:Ljava/util/Map;

    invoke-interface {v7, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "charset":Ljava/nio/charset/Charset;
    check-cast v0, Ljava/nio/charset/Charset;

    .restart local v0    # "charset":Ljava/nio/charset/Charset;
    goto :goto_1

    .line 191
    :cond_9
    sget-object v7, Lorg/apache/tika/utils/CharsetUtils;->getCharsetICU:Ljava/lang/reflect/Method;

    if-eqz v7, :cond_a

    .line 193
    :try_start_0
    sget-object v7, Lorg/apache/tika/utils/CharsetUtils;->getCharsetICU:Ljava/lang/reflect/Method;

    const/4 v8, 0x0

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    aput-object p0, v9, v10

    invoke-virtual {v7, v8, v9}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/nio/charset/Charset;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 194
    .local v2, "cs":Ljava/nio/charset/Charset;
    if-nez v2, :cond_2

    .line 202
    .end local v2    # "cs":Ljava/nio/charset/Charset;
    :cond_a
    :goto_2
    invoke-static {p0}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v2

    goto/16 :goto_0

    .line 197
    :catch_0
    move-exception v7

    goto :goto_2
.end method

.method private static varargs initCommonCharsets([Ljava/lang/String;)Ljava/util/Map;
    .locals 9
    .param p0, "names"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/nio/charset/Charset;",
            ">;"
        }
    .end annotation

    .prologue
    .line 51
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 52
    .local v2, "charsets":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/nio/charset/Charset;>;"
    array-length v5, p0

    const/4 v4, 0x0

    :goto_0
    if-lt v4, v5, :cond_0

    .line 63
    return-object v2

    .line 52
    :cond_0
    aget-object v3, p0, v4

    .line 54
    .local v3, "name":Ljava/lang/String;
    :try_start_0
    invoke-static {v3}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v1

    .line 55
    .local v1, "charset":Ljava/nio/charset/Charset;
    sget-object v6, Lorg/apache/tika/utils/CharsetUtils;->COMMON_CHARSETS:Ljava/util/Map;

    sget-object v7, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v3, v7}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v7

    invoke-interface {v6, v7, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 56
    invoke-virtual {v1}, Ljava/nio/charset/Charset;->aliases()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-nez v7, :cond_1

    .line 52
    .end local v1    # "charset":Ljava/nio/charset/Charset;
    :goto_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 56
    .restart local v1    # "charset":Ljava/nio/charset/Charset;
    :cond_1
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 57
    .local v0, "alias":Ljava/lang/String;
    sget-object v7, Lorg/apache/tika/utils/CharsetUtils;->COMMON_CHARSETS:Ljava/util/Map;

    sget-object v8, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v0, v8}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v8

    invoke-interface {v7, v8, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 59
    .end local v0    # "alias":Ljava/lang/String;
    .end local v1    # "charset":Ljava/nio/charset/Charset;
    :catch_0
    move-exception v6

    goto :goto_2
.end method

.method public static isSupported(Ljava/lang/String;)Z
    .locals 7
    .param p0, "charsetName"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 117
    :try_start_0
    sget-object v1, Lorg/apache/tika/utils/CharsetUtils;->isSupportedICU:Ljava/lang/reflect/Method;

    if-eqz v1, :cond_0

    sget-object v1, Lorg/apache/tika/utils/CharsetUtils;->isSupportedICU:Ljava/lang/reflect/Method;

    const/4 v4, 0x0

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object p0, v5, v6

    invoke-virtual {v1, v4, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    move v1, v2

    .line 128
    :goto_0
    return v1

    .line 120
    :cond_0
    invoke-static {p0}, Ljava/nio/charset/Charset;->isSupported(Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/nio/charset/IllegalCharsetNameException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    move-result v1

    goto :goto_0

    .line 121
    :catch_0
    move-exception v0

    .local v0, "e":Ljava/nio/charset/IllegalCharsetNameException;
    move v1, v3

    .line 122
    goto :goto_0

    .line 123
    .end local v0    # "e":Ljava/nio/charset/IllegalCharsetNameException;
    :catch_1
    move-exception v0

    .local v0, "e":Ljava/lang/IllegalArgumentException;
    move v1, v3

    .line 125
    goto :goto_0

    .line 126
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_2
    move-exception v0

    .local v0, "e":Ljava/lang/Exception;
    move v1, v3

    .line 128
    goto :goto_0
.end method
