.class public Lorg/apache/tika/io/TemporaryResources;
.super Ljava/lang/Object;
.source "TemporaryResources.java"

# interfaces
.implements Ljava/io/Closeable;


# instance fields
.field private final resources:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Ljava/io/Closeable;",
            ">;"
        }
    .end annotation
.end field

.field private tmp:Ljava/io/File;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lorg/apache/tika/io/TemporaryResources;->resources:Ljava/util/LinkedList;

    .line 45
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/tika/io/TemporaryResources;->tmp:Ljava/io/File;

    .line 35
    return-void
.end method


# virtual methods
.method public addResource(Ljava/io/Closeable;)V
    .locals 1
    .param p1, "resource"    # Ljava/io/Closeable;

    .prologue
    .line 86
    iget-object v0, p0, Lorg/apache/tika/io/TemporaryResources;->resources:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    .line 87
    return-void
.end method

.method public close()V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 118
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    .line 119
    .local v1, "exceptions":Ljava/util/List;, "Ljava/util/List<Ljava/io/IOException;>;"
    iget-object v3, p0, Lorg/apache/tika/io/TemporaryResources;->resources:Ljava/util/LinkedList;

    invoke-virtual {v3}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_0

    .line 126
    iget-object v3, p0, Lorg/apache/tika/io/TemporaryResources;->resources:Ljava/util/LinkedList;

    invoke-virtual {v3}, Ljava/util/LinkedList;->clear()V

    .line 129
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_2

    .line 130
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_1

    .line 131
    invoke-interface {v1, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/io/IOException;

    throw v3

    .line 119
    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/io/Closeable;

    .line 121
    .local v2, "resource":Ljava/io/Closeable;
    :try_start_0
    invoke-interface {v2}, Ljava/io/Closeable;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 122
    :catch_0
    move-exception v0

    .line 123
    .local v0, "e":Ljava/io/IOException;
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 133
    .end local v0    # "e":Ljava/io/IOException;
    .end local v2    # "resource":Ljava/io/Closeable;
    :cond_1
    new-instance v4, Lorg/apache/tika/io/IOExceptionWithCause;

    .line 134
    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "Multiple IOExceptions"

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 135
    invoke-interface {v1, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Throwable;

    .line 133
    invoke-direct {v4, v5, v3}, Lorg/apache/tika/io/IOExceptionWithCause;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v4

    .line 138
    :cond_2
    return-void
.end method

.method public createTemporaryFile()Ljava/io/File;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 66
    const-string/jumbo v1, "apache-tika-"

    const-string/jumbo v2, ".tmp"

    iget-object v3, p0, Lorg/apache/tika/io/TemporaryResources;->tmp:Ljava/io/File;

    invoke-static {v1, v2, v3}, Ljava/io/File;->createTempFile(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Ljava/io/File;

    move-result-object v0

    .line 67
    .local v0, "file":Ljava/io/File;
    new-instance v1, Lorg/apache/tika/io/TemporaryResources$1;

    invoke-direct {v1, p0, v0}, Lorg/apache/tika/io/TemporaryResources$1;-><init>(Lorg/apache/tika/io/TemporaryResources;Ljava/io/File;)V

    invoke-virtual {p0, v1}, Lorg/apache/tika/io/TemporaryResources;->addResource(Ljava/io/Closeable;)V

    .line 76
    return-object v0
.end method

.method public dispose()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tika/exception/TikaException;
        }
    .end annotation

    .prologue
    .line 150
    :try_start_0
    invoke-virtual {p0}, Lorg/apache/tika/io/TemporaryResources;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 154
    return-void

    .line 151
    :catch_0
    move-exception v0

    .line 152
    .local v0, "e":Ljava/io/IOException;
    new-instance v1, Lorg/apache/tika/exception/TikaException;

    const-string/jumbo v2, "Failed to close temporary resources"

    invoke-direct {v1, v2, v0}, Lorg/apache/tika/exception/TikaException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public getResource(Ljava/lang/Class;)Ljava/io/Closeable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Ljava/io/Closeable;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 98
    .local p1, "klass":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    iget-object v1, p0, Lorg/apache/tika/io/TemporaryResources;->resources:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_1

    .line 103
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 98
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/Closeable;

    .line 99
    .local v0, "resource":Ljava/io/Closeable;
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v2

    if-eqz v2, :cond_0

    goto :goto_0
.end method

.method public setTemporaryFileDirectory(Ljava/io/File;)V
    .locals 0
    .param p1, "tmp"    # Ljava/io/File;

    .prologue
    .line 55
    iput-object p1, p0, Lorg/apache/tika/io/TemporaryResources;->tmp:Ljava/io/File;

    .line 56
    return-void
.end method
