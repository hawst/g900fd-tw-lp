.class public Lorg/apache/tika/io/TikaInputStream;
.super Lorg/apache/tika/io/TaggedInputStream;
.source "TikaInputStream.java"


# static fields
.field private static final BLOB_SIZE_THRESHOLD:I = 0x100000


# instance fields
.field private file:Ljava/io/File;

.field private length:J

.field private mark:J

.field private openContainer:Ljava/lang/Object;

.field private position:J

.field private final tmp:Lorg/apache/tika/io/TemporaryResources;


# direct methods
.method private constructor <init>(Ljava/io/File;)V
    .locals 2
    .param p1, "file"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .prologue
    .line 446
    new-instance v0, Ljava/io/BufferedInputStream;

    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, p1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v0, v1}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    invoke-direct {p0, v0}, Lorg/apache/tika/io/TaggedInputStream;-><init>(Ljava/io/InputStream;)V

    .line 424
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lorg/apache/tika/io/TikaInputStream;->position:J

    .line 429
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lorg/apache/tika/io/TikaInputStream;->mark:J

    .line 447
    iput-object p1, p0, Lorg/apache/tika/io/TikaInputStream;->file:Ljava/io/File;

    .line 448
    new-instance v0, Lorg/apache/tika/io/TemporaryResources;

    invoke-direct {v0}, Lorg/apache/tika/io/TemporaryResources;-><init>()V

    iput-object v0, p0, Lorg/apache/tika/io/TikaInputStream;->tmp:Lorg/apache/tika/io/TemporaryResources;

    .line 449
    invoke-virtual {p1}, Ljava/io/File;->length()J

    move-result-wide v0

    iput-wide v0, p0, Lorg/apache/tika/io/TikaInputStream;->length:J

    .line 450
    return-void
.end method

.method private constructor <init>(Ljava/io/InputStream;Lorg/apache/tika/io/TemporaryResources;J)V
    .locals 3
    .param p1, "stream"    # Ljava/io/InputStream;
    .param p2, "tmp"    # Lorg/apache/tika/io/TemporaryResources;
    .param p3, "length"    # J

    .prologue
    .line 466
    invoke-direct {p0, p1}, Lorg/apache/tika/io/TaggedInputStream;-><init>(Ljava/io/InputStream;)V

    .line 424
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lorg/apache/tika/io/TikaInputStream;->position:J

    .line 429
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lorg/apache/tika/io/TikaInputStream;->mark:J

    .line 467
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/tika/io/TikaInputStream;->file:Ljava/io/File;

    .line 468
    iput-object p2, p0, Lorg/apache/tika/io/TikaInputStream;->tmp:Lorg/apache/tika/io/TemporaryResources;

    .line 469
    iput-wide p3, p0, Lorg/apache/tika/io/TikaInputStream;->length:J

    .line 470
    return-void
.end method

.method public static cast(Ljava/io/InputStream;)Lorg/apache/tika/io/TikaInputStream;
    .locals 1
    .param p0, "stream"    # Ljava/io/InputStream;

    .prologue
    .line 164
    instance-of v0, p0, Lorg/apache/tika/io/TikaInputStream;

    if-eqz v0, :cond_0

    .line 165
    check-cast p0, Lorg/apache/tika/io/TikaInputStream;

    .line 167
    .end local p0    # "stream":Ljava/io/InputStream;
    :goto_0
    return-object p0

    .restart local p0    # "stream":Ljava/io/InputStream;
    :cond_0
    const/4 p0, 0x0

    goto :goto_0
.end method

.method public static bridge synthetic get(Ljava/io/InputStream;)Lorg/apache/tika/io/TaggedInputStream;
    .locals 1

    .prologue
    .line 1
    invoke-static {p0}, Lorg/apache/tika/io/TikaInputStream;->get(Ljava/io/InputStream;)Lorg/apache/tika/io/TikaInputStream;

    move-result-object v0

    return-object v0
.end method

.method public static get(Ljava/io/File;)Lorg/apache/tika/io/TikaInputStream;
    .locals 1
    .param p0, "file"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .prologue
    .line 214
    new-instance v0, Lorg/apache/tika/metadata/Metadata;

    invoke-direct {v0}, Lorg/apache/tika/metadata/Metadata;-><init>()V

    invoke-static {p0, v0}, Lorg/apache/tika/io/TikaInputStream;->get(Ljava/io/File;Lorg/apache/tika/metadata/Metadata;)Lorg/apache/tika/io/TikaInputStream;

    move-result-object v0

    return-object v0
.end method

.method public static get(Ljava/io/File;Lorg/apache/tika/metadata/Metadata;)Lorg/apache/tika/io/TikaInputStream;
    .locals 4
    .param p0, "file"    # Ljava/io/File;
    .param p1, "metadata"    # Lorg/apache/tika/metadata/Metadata;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .prologue
    .line 231
    const-string/jumbo v0, "resourceName"

    invoke-virtual {p0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lorg/apache/tika/metadata/Metadata;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 232
    const-string/jumbo v0, "Content-Length"

    invoke-virtual {p0}, Ljava/io/File;->length()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lorg/apache/tika/metadata/Metadata;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 233
    new-instance v0, Lorg/apache/tika/io/TikaInputStream;

    invoke-direct {v0, p0}, Lorg/apache/tika/io/TikaInputStream;-><init>(Ljava/io/File;)V

    return-object v0
.end method

.method public static get(Ljava/io/InputStream;)Lorg/apache/tika/io/TikaInputStream;
    .locals 1
    .param p0, "stream"    # Ljava/io/InputStream;

    .prologue
    .line 152
    new-instance v0, Lorg/apache/tika/io/TemporaryResources;

    invoke-direct {v0}, Lorg/apache/tika/io/TemporaryResources;-><init>()V

    invoke-static {p0, v0}, Lorg/apache/tika/io/TikaInputStream;->get(Ljava/io/InputStream;Lorg/apache/tika/io/TemporaryResources;)Lorg/apache/tika/io/TikaInputStream;

    move-result-object v0

    return-object v0
.end method

.method public static get(Ljava/io/InputStream;Lorg/apache/tika/io/TemporaryResources;)Lorg/apache/tika/io/TikaInputStream;
    .locals 4
    .param p0, "stream"    # Ljava/io/InputStream;
    .param p1, "tmp"    # Lorg/apache/tika/io/TemporaryResources;

    .prologue
    .line 110
    if-nez p0, :cond_0

    .line 111
    new-instance v1, Ljava/lang/NullPointerException;

    const-string/jumbo v2, "The Stream must not be null"

    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 113
    :cond_0
    instance-of v1, p0, Lorg/apache/tika/io/TikaInputStream;

    if-eqz v1, :cond_1

    .line 114
    check-cast p0, Lorg/apache/tika/io/TikaInputStream;

    .line 122
    .end local p0    # "stream":Ljava/io/InputStream;
    :goto_0
    return-object p0

    .line 118
    .restart local p0    # "stream":Ljava/io/InputStream;
    :cond_1
    instance-of v1, p0, Ljava/io/BufferedInputStream;

    if-nez v1, :cond_2

    .line 119
    instance-of v1, p0, Ljava/io/ByteArrayInputStream;

    if-nez v1, :cond_2

    .line 120
    new-instance v0, Ljava/io/BufferedInputStream;

    invoke-direct {v0, p0}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    .end local p0    # "stream":Ljava/io/InputStream;
    .local v0, "stream":Ljava/io/InputStream;
    move-object p0, v0

    .line 122
    .end local v0    # "stream":Ljava/io/InputStream;
    .restart local p0    # "stream":Ljava/io/InputStream;
    :cond_2
    new-instance v1, Lorg/apache/tika/io/TikaInputStream;

    const-wide/16 v2, -0x1

    invoke-direct {v1, p0, p1, v2, v3}, Lorg/apache/tika/io/TikaInputStream;-><init>(Ljava/io/InputStream;Lorg/apache/tika/io/TemporaryResources;J)V

    move-object p0, v1

    goto :goto_0
.end method

.method public static get(Ljava/net/URI;)Lorg/apache/tika/io/TikaInputStream;
    .locals 1
    .param p0, "uri"    # Ljava/net/URI;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 306
    new-instance v0, Lorg/apache/tika/metadata/Metadata;

    invoke-direct {v0}, Lorg/apache/tika/metadata/Metadata;-><init>()V

    invoke-static {p0, v0}, Lorg/apache/tika/io/TikaInputStream;->get(Ljava/net/URI;Lorg/apache/tika/metadata/Metadata;)Lorg/apache/tika/io/TikaInputStream;

    move-result-object v0

    return-object v0
.end method

.method public static get(Ljava/net/URI;Lorg/apache/tika/metadata/Metadata;)Lorg/apache/tika/io/TikaInputStream;
    .locals 3
    .param p0, "uri"    # Ljava/net/URI;
    .param p1, "metadata"    # Lorg/apache/tika/metadata/Metadata;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 324
    const-string/jumbo v1, "file"

    invoke-virtual {p0}, Ljava/net/URI;->getScheme()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 325
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/net/URI;)V

    .line 326
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->isFile()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 327
    invoke-static {v0, p1}, Lorg/apache/tika/io/TikaInputStream;->get(Ljava/io/File;Lorg/apache/tika/metadata/Metadata;)Lorg/apache/tika/io/TikaInputStream;

    move-result-object v1

    .line 331
    .end local v0    # "file":Ljava/io/File;
    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {p0}, Ljava/net/URI;->toURL()Ljava/net/URL;

    move-result-object v1

    invoke-static {v1, p1}, Lorg/apache/tika/io/TikaInputStream;->get(Ljava/net/URL;Lorg/apache/tika/metadata/Metadata;)Lorg/apache/tika/io/TikaInputStream;

    move-result-object v1

    goto :goto_0
.end method

.method public static get(Ljava/net/URL;)Lorg/apache/tika/io/TikaInputStream;
    .locals 1
    .param p0, "url"    # Ljava/net/URL;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 345
    new-instance v0, Lorg/apache/tika/metadata/Metadata;

    invoke-direct {v0}, Lorg/apache/tika/metadata/Metadata;-><init>()V

    invoke-static {p0, v0}, Lorg/apache/tika/io/TikaInputStream;->get(Ljava/net/URL;Lorg/apache/tika/metadata/Metadata;)Lorg/apache/tika/io/TikaInputStream;

    move-result-object v0

    return-object v0
.end method

.method public static get(Ljava/net/URL;Lorg/apache/tika/metadata/Metadata;)Lorg/apache/tika/io/TikaInputStream;
    .locals 12
    .param p0, "url"    # Ljava/net/URL;
    .param p1, "metadata"    # Lorg/apache/tika/metadata/Metadata;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 363
    const-string/jumbo v7, "file"

    invoke-virtual {p0}, Ljava/net/URL;->getProtocol()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 365
    :try_start_0
    new-instance v2, Ljava/io/File;

    invoke-virtual {p0}, Ljava/net/URL;->toURI()Ljava/net/URI;

    move-result-object v7

    invoke-direct {v2, v7}, Ljava/io/File;-><init>(Ljava/net/URI;)V

    .line 366
    .local v2, "file":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->isFile()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 367
    invoke-static {v2, p1}, Lorg/apache/tika/io/TikaInputStream;->get(Ljava/io/File;Lorg/apache/tika/metadata/Metadata;)Lorg/apache/tika/io/TikaInputStream;
    :try_end_0
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v7

    .line 397
    .end local v2    # "file":Ljava/io/File;
    :goto_0
    return-object v7

    .line 369
    :catch_0
    move-exception v7

    .line 374
    :cond_0
    invoke-virtual {p0}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v0

    .line 376
    .local v0, "connection":Ljava/net/URLConnection;
    invoke-virtual {p0}, Ljava/net/URL;->getPath()Ljava/lang/String;

    move-result-object v4

    .line 377
    .local v4, "path":Ljava/lang/String;
    const/16 v7, 0x2f

    invoke-virtual {v4, v7}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v5

    .line 378
    .local v5, "slash":I
    add-int/lit8 v7, v5, 0x1

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v8

    if-ge v7, v8, :cond_1

    .line 379
    const-string/jumbo v7, "resourceName"

    add-int/lit8 v8, v5, 0x1

    invoke-virtual {v4, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v7, v8}, Lorg/apache/tika/metadata/Metadata;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 382
    :cond_1
    invoke-virtual {v0}, Ljava/net/URLConnection;->getContentType()Ljava/lang/String;

    move-result-object v6

    .line 383
    .local v6, "type":Ljava/lang/String;
    if-eqz v6, :cond_2

    .line 384
    const-string/jumbo v7, "Content-Type"

    invoke-virtual {p1, v7, v6}, Lorg/apache/tika/metadata/Metadata;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 387
    :cond_2
    invoke-virtual {v0}, Ljava/net/URLConnection;->getContentEncoding()Ljava/lang/String;

    move-result-object v1

    .line 388
    .local v1, "encoding":Ljava/lang/String;
    if-eqz v1, :cond_3

    .line 389
    const-string/jumbo v7, "Content-Encoding"

    invoke-virtual {p1, v7, v1}, Lorg/apache/tika/metadata/Metadata;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 392
    :cond_3
    invoke-virtual {v0}, Ljava/net/URLConnection;->getContentLength()I

    move-result v3

    .line 393
    .local v3, "length":I
    if-ltz v3, :cond_4

    .line 394
    const-string/jumbo v7, "Content-Length"

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v7, v8}, Lorg/apache/tika/metadata/Metadata;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 397
    :cond_4
    new-instance v7, Lorg/apache/tika/io/TikaInputStream;

    .line 398
    new-instance v8, Ljava/io/BufferedInputStream;

    invoke-virtual {v0}, Ljava/net/URLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    .line 399
    new-instance v9, Lorg/apache/tika/io/TemporaryResources;

    invoke-direct {v9}, Lorg/apache/tika/io/TemporaryResources;-><init>()V

    int-to-long v10, v3

    .line 397
    invoke-direct {v7, v8, v9, v10, v11}, Lorg/apache/tika/io/TikaInputStream;-><init>(Ljava/io/InputStream;Lorg/apache/tika/io/TemporaryResources;J)V

    goto :goto_0
.end method

.method public static get(Ljava/sql/Blob;)Lorg/apache/tika/io/TikaInputStream;
    .locals 1
    .param p0, "blob"    # Ljava/sql/Blob;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/sql/SQLException;
        }
    .end annotation

    .prologue
    .line 249
    new-instance v0, Lorg/apache/tika/metadata/Metadata;

    invoke-direct {v0}, Lorg/apache/tika/metadata/Metadata;-><init>()V

    invoke-static {p0, v0}, Lorg/apache/tika/io/TikaInputStream;->get(Ljava/sql/Blob;Lorg/apache/tika/metadata/Metadata;)Lorg/apache/tika/io/TikaInputStream;

    move-result-object v0

    return-object v0
.end method

.method public static get(Ljava/sql/Blob;Lorg/apache/tika/metadata/Metadata;)Lorg/apache/tika/io/TikaInputStream;
    .locals 5
    .param p0, "blob"    # Ljava/sql/Blob;
    .param p1, "metadata"    # Lorg/apache/tika/metadata/Metadata;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/sql/SQLException;
        }
    .end annotation

    .prologue
    .line 276
    const-wide/16 v0, -0x1

    .line 278
    .local v0, "length":J
    :try_start_0
    invoke-interface {p0}, Ljava/sql/Blob;->length()J

    move-result-wide v0

    .line 279
    const-string/jumbo v2, "Content-Length"

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lorg/apache/tika/metadata/Metadata;->set(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/sql/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    .line 285
    :goto_0
    const-wide/16 v2, 0x0

    cmp-long v2, v2, v0

    if-gtz v2, :cond_0

    const-wide/32 v2, 0x100000

    cmp-long v2, v0, v2

    if-gtz v2, :cond_0

    .line 287
    const-wide/16 v2, 0x1

    long-to-int v4, v0

    invoke-interface {p0, v2, v3, v4}, Ljava/sql/Blob;->getBytes(JI)[B

    move-result-object v2

    invoke-static {v2, p1}, Lorg/apache/tika/io/TikaInputStream;->get([BLorg/apache/tika/metadata/Metadata;)Lorg/apache/tika/io/TikaInputStream;

    move-result-object v2

    .line 289
    :goto_1
    return-object v2

    :cond_0
    new-instance v2, Lorg/apache/tika/io/TikaInputStream;

    .line 290
    new-instance v3, Ljava/io/BufferedInputStream;

    invoke-interface {p0}, Ljava/sql/Blob;->getBinaryStream()Ljava/io/InputStream;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    .line 291
    new-instance v4, Lorg/apache/tika/io/TemporaryResources;

    invoke-direct {v4}, Lorg/apache/tika/io/TemporaryResources;-><init>()V

    .line 289
    invoke-direct {v2, v3, v4, v0, v1}, Lorg/apache/tika/io/TikaInputStream;-><init>(Ljava/io/InputStream;Lorg/apache/tika/io/TemporaryResources;J)V

    goto :goto_1

    .line 280
    :catch_0
    move-exception v2

    goto :goto_0
.end method

.method public static get([B)Lorg/apache/tika/io/TikaInputStream;
    .locals 1
    .param p0, "data"    # [B

    .prologue
    .line 181
    new-instance v0, Lorg/apache/tika/metadata/Metadata;

    invoke-direct {v0}, Lorg/apache/tika/metadata/Metadata;-><init>()V

    invoke-static {p0, v0}, Lorg/apache/tika/io/TikaInputStream;->get([BLorg/apache/tika/metadata/Metadata;)Lorg/apache/tika/io/TikaInputStream;

    move-result-object v0

    return-object v0
.end method

.method public static get([BLorg/apache/tika/metadata/Metadata;)Lorg/apache/tika/io/TikaInputStream;
    .locals 6
    .param p0, "data"    # [B
    .param p1, "metadata"    # Lorg/apache/tika/metadata/Metadata;

    .prologue
    .line 197
    const-string/jumbo v0, "Content-Length"

    array-length v1, p0

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lorg/apache/tika/metadata/Metadata;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 198
    new-instance v0, Lorg/apache/tika/io/TikaInputStream;

    .line 199
    new-instance v1, Ljava/io/ByteArrayInputStream;

    invoke-direct {v1, p0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 200
    new-instance v2, Lorg/apache/tika/io/TemporaryResources;

    invoke-direct {v2}, Lorg/apache/tika/io/TemporaryResources;-><init>()V

    array-length v3, p0

    int-to-long v4, v3

    .line 198
    invoke-direct {v0, v1, v2, v4, v5}, Lorg/apache/tika/io/TikaInputStream;-><init>(Ljava/io/InputStream;Lorg/apache/tika/io/TemporaryResources;J)V

    return-object v0
.end method

.method public static isTikaInputStream(Ljava/io/InputStream;)Z
    .locals 1
    .param p0, "stream"    # Ljava/io/InputStream;

    .prologue
    .line 75
    instance-of v0, p0, Lorg/apache/tika/io/TikaInputStream;

    return v0
.end method


# virtual methods
.method protected afterRead(I)V
    .locals 4
    .param p1, "n"    # I

    .prologue
    .line 644
    const/4 v0, -0x1

    if-eq p1, v0, :cond_0

    .line 645
    iget-wide v0, p0, Lorg/apache/tika/io/TikaInputStream;->position:J

    int-to-long v2, p1

    add-long/2addr v0, v2

    iput-wide v0, p0, Lorg/apache/tika/io/TikaInputStream;->position:J

    .line 647
    :cond_0
    return-void
.end method

.method public close()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 630
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/tika/io/TikaInputStream;->file:Ljava/io/File;

    .line 631
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lorg/apache/tika/io/TikaInputStream;->mark:J

    .line 638
    iget-object v0, p0, Lorg/apache/tika/io/TikaInputStream;->tmp:Lorg/apache/tika/io/TemporaryResources;

    iget-object v1, p0, Lorg/apache/tika/io/TikaInputStream;->in:Ljava/io/InputStream;

    invoke-virtual {v0, v1}, Lorg/apache/tika/io/TemporaryResources;->addResource(Ljava/io/Closeable;)V

    .line 639
    iget-object v0, p0, Lorg/apache/tika/io/TikaInputStream;->tmp:Lorg/apache/tika/io/TemporaryResources;

    invoke-virtual {v0}, Lorg/apache/tika/io/TemporaryResources;->close()V

    .line 640
    return-void
.end method

.method public getFile()Ljava/io/File;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 530
    iget-object v3, p0, Lorg/apache/tika/io/TikaInputStream;->file:Ljava/io/File;

    if-nez v3, :cond_1

    .line 531
    iget-wide v4, p0, Lorg/apache/tika/io/TikaInputStream;->position:J

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-lez v3, :cond_0

    .line 532
    new-instance v3, Ljava/io/IOException;

    const-string/jumbo v4, "Stream is already being read"

    invoke-direct {v3, v4}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 535
    :cond_0
    iget-object v3, p0, Lorg/apache/tika/io/TikaInputStream;->tmp:Lorg/apache/tika/io/TemporaryResources;

    invoke-virtual {v3}, Lorg/apache/tika/io/TemporaryResources;->createTemporaryFile()Ljava/io/File;

    move-result-object v3

    iput-object v3, p0, Lorg/apache/tika/io/TikaInputStream;->file:Ljava/io/File;

    .line 536
    new-instance v2, Ljava/io/FileOutputStream;

    iget-object v3, p0, Lorg/apache/tika/io/TikaInputStream;->file:Ljava/io/File;

    invoke-direct {v2, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 538
    .local v2, "out":Ljava/io/OutputStream;
    :try_start_0
    iget-object v3, p0, Lorg/apache/tika/io/TikaInputStream;->in:Ljava/io/InputStream;

    invoke-static {v3, v2}, Lorg/apache/tika/io/IOUtils;->copy(Ljava/io/InputStream;Ljava/io/OutputStream;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 540
    invoke-virtual {v2}, Ljava/io/OutputStream;->close()V

    .line 544
    new-instance v0, Ljava/io/FileInputStream;

    iget-object v3, p0, Lorg/apache/tika/io/TikaInputStream;->file:Ljava/io/File;

    invoke-direct {v0, v3}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 545
    .local v0, "newStream":Ljava/io/FileInputStream;
    iget-object v3, p0, Lorg/apache/tika/io/TikaInputStream;->tmp:Lorg/apache/tika/io/TemporaryResources;

    invoke-virtual {v3, v0}, Lorg/apache/tika/io/TemporaryResources;->addResource(Ljava/io/Closeable;)V

    .line 551
    iget-object v1, p0, Lorg/apache/tika/io/TikaInputStream;->in:Ljava/io/InputStream;

    .line 552
    .local v1, "oldStream":Ljava/io/InputStream;
    new-instance v3, Lorg/apache/tika/io/TikaInputStream$1;

    invoke-direct {v3, p0, v0, v1}, Lorg/apache/tika/io/TikaInputStream$1;-><init>(Lorg/apache/tika/io/TikaInputStream;Ljava/io/InputStream;Ljava/io/InputStream;)V

    iput-object v3, p0, Lorg/apache/tika/io/TikaInputStream;->in:Ljava/io/InputStream;

    .line 559
    iget-object v3, p0, Lorg/apache/tika/io/TikaInputStream;->file:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->length()J

    move-result-wide v4

    iput-wide v4, p0, Lorg/apache/tika/io/TikaInputStream;->length:J

    .line 562
    .end local v0    # "newStream":Ljava/io/FileInputStream;
    .end local v1    # "oldStream":Ljava/io/InputStream;
    .end local v2    # "out":Ljava/io/OutputStream;
    :cond_1
    iget-object v3, p0, Lorg/apache/tika/io/TikaInputStream;->file:Ljava/io/File;

    return-object v3

    .line 539
    .restart local v2    # "out":Ljava/io/OutputStream;
    :catchall_0
    move-exception v3

    .line 540
    invoke-virtual {v2}, Ljava/io/OutputStream;->close()V

    .line 541
    throw v3
.end method

.method public getFileChannel()Ljava/nio/channels/FileChannel;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 566
    new-instance v1, Ljava/io/FileInputStream;

    invoke-virtual {p0}, Lorg/apache/tika/io/TikaInputStream;->getFile()Ljava/io/File;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 567
    .local v1, "fis":Ljava/io/FileInputStream;
    iget-object v2, p0, Lorg/apache/tika/io/TikaInputStream;->tmp:Lorg/apache/tika/io/TemporaryResources;

    invoke-virtual {v2, v1}, Lorg/apache/tika/io/TemporaryResources;->addResource(Ljava/io/Closeable;)V

    .line 568
    invoke-virtual {v1}, Ljava/io/FileInputStream;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v0

    .line 569
    .local v0, "channel":Ljava/nio/channels/FileChannel;
    iget-object v2, p0, Lorg/apache/tika/io/TikaInputStream;->tmp:Lorg/apache/tika/io/TemporaryResources;

    invoke-virtual {v2, v0}, Lorg/apache/tika/io/TemporaryResources;->addResource(Ljava/io/Closeable;)V

    .line 570
    return-object v0
.end method

.method public getLength()J
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 588
    iget-wide v0, p0, Lorg/apache/tika/io/TikaInputStream;->length:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 589
    invoke-virtual {p0}, Lorg/apache/tika/io/TikaInputStream;->getFile()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v0

    iput-wide v0, p0, Lorg/apache/tika/io/TikaInputStream;->length:J

    .line 591
    :cond_0
    iget-wide v0, p0, Lorg/apache/tika/io/TikaInputStream;->length:J

    return-wide v0
.end method

.method public getOpenContainer()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 509
    iget-object v0, p0, Lorg/apache/tika/io/TikaInputStream;->openContainer:Ljava/lang/Object;

    return-object v0
.end method

.method public getPosition()J
    .locals 2

    .prologue
    .line 600
    iget-wide v0, p0, Lorg/apache/tika/io/TikaInputStream;->position:J

    return-wide v0
.end method

.method public hasFile()Z
    .locals 1

    .prologue
    .line 526
    iget-object v0, p0, Lorg/apache/tika/io/TikaInputStream;->file:Ljava/io/File;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasLength()Z
    .locals 4

    .prologue
    .line 574
    iget-wide v0, p0, Lorg/apache/tika/io/TikaInputStream;->length:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public mark(I)V
    .locals 2
    .param p1, "readlimit"    # I

    .prologue
    .line 612
    invoke-super {p0, p1}, Lorg/apache/tika/io/TaggedInputStream;->mark(I)V

    .line 613
    iget-wide v0, p0, Lorg/apache/tika/io/TikaInputStream;->position:J

    iput-wide v0, p0, Lorg/apache/tika/io/TikaInputStream;->mark:J

    .line 614
    return-void
.end method

.method public markSupported()Z
    .locals 1

    .prologue
    .line 618
    const/4 v0, 0x1

    return v0
.end method

.method public peek([B)I
    .locals 3
    .param p1, "buffer"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 483
    const/4 v1, 0x0

    .line 485
    .local v1, "n":I
    array-length v2, p1

    invoke-virtual {p0, v2}, Lorg/apache/tika/io/TikaInputStream;->mark(I)V

    .line 487
    invoke-virtual {p0, p1}, Lorg/apache/tika/io/TikaInputStream;->read([B)I

    move-result v0

    .line 488
    .local v0, "m":I
    :goto_0
    const/4 v2, -0x1

    if-ne v0, v2, :cond_0

    .line 497
    invoke-virtual {p0}, Lorg/apache/tika/io/TikaInputStream;->reset()V

    .line 499
    return v1

    .line 489
    :cond_0
    add-int/2addr v1, v0

    .line 490
    array-length v2, p1

    if-ge v1, v2, :cond_1

    .line 491
    array-length v2, p1

    sub-int/2addr v2, v1

    invoke-virtual {p0, p1, v1, v2}, Lorg/apache/tika/io/TikaInputStream;->read([BII)I

    move-result v0

    .line 492
    goto :goto_0

    .line 493
    :cond_1
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public reset()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 623
    invoke-super {p0}, Lorg/apache/tika/io/TaggedInputStream;->reset()V

    .line 624
    iget-wide v0, p0, Lorg/apache/tika/io/TikaInputStream;->mark:J

    iput-wide v0, p0, Lorg/apache/tika/io/TikaInputStream;->position:J

    .line 625
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lorg/apache/tika/io/TikaInputStream;->mark:J

    .line 626
    return-void
.end method

.method public setOpenContainer(Ljava/lang/Object;)V
    .locals 1
    .param p1, "container"    # Ljava/lang/Object;

    .prologue
    .line 519
    iput-object p1, p0, Lorg/apache/tika/io/TikaInputStream;->openContainer:Ljava/lang/Object;

    .line 520
    instance-of v0, p1, Ljava/io/Closeable;

    if-eqz v0, :cond_0

    .line 521
    iget-object v0, p0, Lorg/apache/tika/io/TikaInputStream;->tmp:Lorg/apache/tika/io/TemporaryResources;

    check-cast p1, Ljava/io/Closeable;

    .end local p1    # "container":Ljava/lang/Object;
    invoke-virtual {v0, p1}, Lorg/apache/tika/io/TemporaryResources;->addResource(Ljava/io/Closeable;)V

    .line 523
    :cond_0
    return-void
.end method

.method public skip(J)J
    .locals 5
    .param p1, "ln"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 605
    invoke-super {p0, p1, p2}, Lorg/apache/tika/io/TaggedInputStream;->skip(J)J

    move-result-wide v0

    .line 606
    .local v0, "n":J
    iget-wide v2, p0, Lorg/apache/tika/io/TikaInputStream;->position:J

    add-long/2addr v2, v0

    iput-wide v2, p0, Lorg/apache/tika/io/TikaInputStream;->position:J

    .line 607
    return-wide v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 650
    const-string/jumbo v0, "TikaInputStream of "

    .line 651
    .local v0, "str":Ljava/lang/String;
    invoke-virtual {p0}, Lorg/apache/tika/io/TikaInputStream;->hasFile()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 652
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lorg/apache/tika/io/TikaInputStream;->file:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 656
    :goto_0
    iget-object v1, p0, Lorg/apache/tika/io/TikaInputStream;->openContainer:Ljava/lang/Object;

    if-eqz v1, :cond_0

    .line 657
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v2, " (in "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/tika/io/TikaInputStream;->openContainer:Ljava/lang/Object;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 659
    :cond_0
    return-object v0

    .line 654
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lorg/apache/tika/io/TikaInputStream;->in:Ljava/io/InputStream;

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
