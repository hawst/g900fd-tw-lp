.class public Lorg/apache/tika/io/TaggedInputStream;
.super Lorg/apache/tika/io/ProxyInputStream;
.source "TaggedInputStream.java"


# instance fields
.field private final tag:Ljava/io/Serializable;


# direct methods
.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 1
    .param p1, "proxy"    # Ljava/io/InputStream;

    .prologue
    .line 74
    invoke-direct {p0, p1}, Lorg/apache/tika/io/ProxyInputStream;-><init>(Ljava/io/InputStream;)V

    .line 66
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/tika/io/TaggedInputStream;->tag:Ljava/io/Serializable;

    .line 75
    return-void
.end method

.method public static get(Ljava/io/InputStream;)Lorg/apache/tika/io/TaggedInputStream;
    .locals 1
    .param p0, "proxy"    # Ljava/io/InputStream;

    .prologue
    .line 84
    instance-of v0, p0, Lorg/apache/tika/io/TaggedInputStream;

    if-eqz v0, :cond_0

    .line 85
    check-cast p0, Lorg/apache/tika/io/TaggedInputStream;

    .line 87
    .end local p0    # "proxy":Ljava/io/InputStream;
    :goto_0
    return-object p0

    .restart local p0    # "proxy":Ljava/io/InputStream;
    :cond_0
    new-instance v0, Lorg/apache/tika/io/TaggedInputStream;

    invoke-direct {v0, p0}, Lorg/apache/tika/io/TaggedInputStream;-><init>(Ljava/io/InputStream;)V

    move-object p0, v0

    goto :goto_0
.end method


# virtual methods
.method protected handleIOException(Ljava/io/IOException;)V
    .locals 2
    .param p1, "e"    # Ljava/io/IOException;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 133
    new-instance v0, Lorg/apache/tika/io/TaggedIOException;

    iget-object v1, p0, Lorg/apache/tika/io/TaggedInputStream;->tag:Ljava/io/Serializable;

    invoke-direct {v0, p1, v1}, Lorg/apache/tika/io/TaggedIOException;-><init>(Ljava/io/IOException;Ljava/lang/Object;)V

    throw v0
.end method

.method public isCauseOf(Ljava/io/IOException;)Z
    .locals 3
    .param p1, "exception"    # Ljava/io/IOException;

    .prologue
    .line 98
    instance-of v1, p1, Lorg/apache/tika/io/TaggedIOException;

    if-eqz v1, :cond_0

    move-object v0, p1

    .line 99
    check-cast v0, Lorg/apache/tika/io/TaggedIOException;

    .line 100
    .local v0, "tagged":Lorg/apache/tika/io/TaggedIOException;
    iget-object v1, p0, Lorg/apache/tika/io/TaggedInputStream;->tag:Ljava/io/Serializable;

    invoke-virtual {v0}, Lorg/apache/tika/io/TaggedIOException;->getTag()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    .line 102
    .end local v0    # "tagged":Lorg/apache/tika/io/TaggedIOException;
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public throwIfCauseOf(Ljava/lang/Exception;)V
    .locals 3
    .param p1, "exception"    # Ljava/lang/Exception;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 117
    instance-of v1, p1, Lorg/apache/tika/io/TaggedIOException;

    if-eqz v1, :cond_0

    move-object v0, p1

    .line 118
    check-cast v0, Lorg/apache/tika/io/TaggedIOException;

    .line 119
    .local v0, "tagged":Lorg/apache/tika/io/TaggedIOException;
    iget-object v1, p0, Lorg/apache/tika/io/TaggedInputStream;->tag:Ljava/io/Serializable;

    invoke-virtual {v0}, Lorg/apache/tika/io/TaggedIOException;->getTag()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 120
    invoke-virtual {v0}, Lorg/apache/tika/io/TaggedIOException;->getCause()Ljava/io/IOException;

    move-result-object v1

    throw v1

    .line 123
    .end local v0    # "tagged":Lorg/apache/tika/io/TaggedIOException;
    :cond_0
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 137
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "Tika Tagged InputStream wrapping "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lorg/apache/tika/io/TaggedInputStream;->in:Ljava/io/InputStream;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
