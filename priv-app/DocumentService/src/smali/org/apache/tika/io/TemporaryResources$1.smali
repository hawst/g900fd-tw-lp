.class Lorg/apache/tika/io/TemporaryResources$1;
.super Ljava/lang/Object;
.source "TemporaryResources.java"

# interfaces
.implements Ljava/io/Closeable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/tika/io/TemporaryResources;->createTemporaryFile()Ljava/io/File;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/apache/tika/io/TemporaryResources;

.field private final synthetic val$file:Ljava/io/File;


# direct methods
.method constructor <init>(Lorg/apache/tika/io/TemporaryResources;Ljava/io/File;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lorg/apache/tika/io/TemporaryResources$1;->this$0:Lorg/apache/tika/io/TemporaryResources;

    iput-object p2, p0, Lorg/apache/tika/io/TemporaryResources$1;->val$file:Ljava/io/File;

    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public close()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 69
    iget-object v0, p0, Lorg/apache/tika/io/TemporaryResources$1;->val$file:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    move-result v0

    if-nez v0, :cond_0

    .line 70
    new-instance v0, Ljava/io/IOException;

    .line 71
    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Could not delete temporary file "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 72
    iget-object v2, p0, Lorg/apache/tika/io/TemporaryResources$1;->val$file:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 71
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 70
    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 74
    :cond_0
    return-void
.end method
