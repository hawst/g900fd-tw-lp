.class public interface abstract Lorg/apache/tika/metadata/Office;
.super Ljava/lang/Object;
.source "Office.java"


# static fields
.field public static final AUTHOR:Lorg/apache/tika/metadata/Property;

.field public static final CHARACTER_COUNT:Lorg/apache/tika/metadata/Property;

.field public static final CHARACTER_COUNT_WITH_SPACES:Lorg/apache/tika/metadata/Property;

.field public static final CREATION_DATE:Lorg/apache/tika/metadata/Property;

.field public static final IMAGE_COUNT:Lorg/apache/tika/metadata/Property;

.field public static final INITIAL_AUTHOR:Lorg/apache/tika/metadata/Property;

.field public static final KEYWORDS:Lorg/apache/tika/metadata/Property;

.field public static final LAST_AUTHOR:Lorg/apache/tika/metadata/Property;

.field public static final LINE_COUNT:Lorg/apache/tika/metadata/Property;

.field public static final NAMESPACE_URI_DOC_META:Ljava/lang/String; = "urn:oasis:names:tc:opendocument:xmlns:meta:1.0"

.field public static final OBJECT_COUNT:Lorg/apache/tika/metadata/Property;

.field public static final PAGE_COUNT:Lorg/apache/tika/metadata/Property;

.field public static final PARAGRAPH_COUNT:Lorg/apache/tika/metadata/Property;

.field public static final PREFIX_DOC_META:Ljava/lang/String; = "meta"

.field public static final PRINT_DATE:Lorg/apache/tika/metadata/Property;

.field public static final SAVE_DATE:Lorg/apache/tika/metadata/Property;

.field public static final SLIDE_COUNT:Lorg/apache/tika/metadata/Property;

.field public static final TABLE_COUNT:Lorg/apache/tika/metadata/Property;

.field public static final USER_DEFINED_METADATA_NAME_PREFIX:Ljava/lang/String; = "custom:"

.field public static final WORD_COUNT:Lorg/apache/tika/metadata/Property;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 48
    const-string/jumbo v0, "meta:keyword"

    .line 47
    invoke-static {v0}, Lorg/apache/tika/metadata/Property;->internalTextBag(Ljava/lang/String;)Lorg/apache/tika/metadata/Property;

    move-result-object v0

    sput-object v0, Lorg/apache/tika/metadata/Office;->KEYWORDS:Lorg/apache/tika/metadata/Property;

    .line 54
    const-string/jumbo v0, "meta:initial-author"

    .line 53
    invoke-static {v0}, Lorg/apache/tika/metadata/Property;->internalText(Ljava/lang/String;)Lorg/apache/tika/metadata/Property;

    move-result-object v0

    sput-object v0, Lorg/apache/tika/metadata/Office;->INITIAL_AUTHOR:Lorg/apache/tika/metadata/Property;

    .line 60
    const-string/jumbo v0, "meta:last-author"

    .line 59
    invoke-static {v0}, Lorg/apache/tika/metadata/Property;->internalText(Ljava/lang/String;)Lorg/apache/tika/metadata/Property;

    move-result-object v0

    sput-object v0, Lorg/apache/tika/metadata/Office;->LAST_AUTHOR:Lorg/apache/tika/metadata/Property;

    .line 66
    const-string/jumbo v0, "meta:author"

    .line 65
    invoke-static {v0}, Lorg/apache/tika/metadata/Property;->internalTextBag(Ljava/lang/String;)Lorg/apache/tika/metadata/Property;

    move-result-object v0

    sput-object v0, Lorg/apache/tika/metadata/Office;->AUTHOR:Lorg/apache/tika/metadata/Property;

    .line 71
    const-string/jumbo v0, "meta:creation-date"

    .line 70
    invoke-static {v0}, Lorg/apache/tika/metadata/Property;->internalDate(Ljava/lang/String;)Lorg/apache/tika/metadata/Property;

    move-result-object v0

    sput-object v0, Lorg/apache/tika/metadata/Office;->CREATION_DATE:Lorg/apache/tika/metadata/Property;

    .line 75
    const-string/jumbo v0, "meta:save-date"

    .line 74
    invoke-static {v0}, Lorg/apache/tika/metadata/Property;->internalDate(Ljava/lang/String;)Lorg/apache/tika/metadata/Property;

    move-result-object v0

    sput-object v0, Lorg/apache/tika/metadata/Office;->SAVE_DATE:Lorg/apache/tika/metadata/Property;

    .line 79
    const-string/jumbo v0, "meta:print-date"

    .line 78
    invoke-static {v0}, Lorg/apache/tika/metadata/Property;->internalDate(Ljava/lang/String;)Lorg/apache/tika/metadata/Property;

    move-result-object v0

    sput-object v0, Lorg/apache/tika/metadata/Office;->PRINT_DATE:Lorg/apache/tika/metadata/Property;

    .line 85
    const-string/jumbo v0, "meta:slide-count"

    .line 84
    invoke-static {v0}, Lorg/apache/tika/metadata/Property;->internalInteger(Ljava/lang/String;)Lorg/apache/tika/metadata/Property;

    move-result-object v0

    sput-object v0, Lorg/apache/tika/metadata/Office;->SLIDE_COUNT:Lorg/apache/tika/metadata/Property;

    .line 89
    const-string/jumbo v0, "meta:page-count"

    .line 88
    invoke-static {v0}, Lorg/apache/tika/metadata/Property;->internalInteger(Ljava/lang/String;)Lorg/apache/tika/metadata/Property;

    move-result-object v0

    sput-object v0, Lorg/apache/tika/metadata/Office;->PAGE_COUNT:Lorg/apache/tika/metadata/Property;

    .line 93
    const-string/jumbo v0, "meta:paragraph-count"

    .line 92
    invoke-static {v0}, Lorg/apache/tika/metadata/Property;->internalInteger(Ljava/lang/String;)Lorg/apache/tika/metadata/Property;

    move-result-object v0

    sput-object v0, Lorg/apache/tika/metadata/Office;->PARAGRAPH_COUNT:Lorg/apache/tika/metadata/Property;

    .line 97
    const-string/jumbo v0, "meta:line-count"

    .line 96
    invoke-static {v0}, Lorg/apache/tika/metadata/Property;->internalInteger(Ljava/lang/String;)Lorg/apache/tika/metadata/Property;

    move-result-object v0

    sput-object v0, Lorg/apache/tika/metadata/Office;->LINE_COUNT:Lorg/apache/tika/metadata/Property;

    .line 101
    const-string/jumbo v0, "meta:word-count"

    .line 100
    invoke-static {v0}, Lorg/apache/tika/metadata/Property;->internalInteger(Ljava/lang/String;)Lorg/apache/tika/metadata/Property;

    move-result-object v0

    sput-object v0, Lorg/apache/tika/metadata/Office;->WORD_COUNT:Lorg/apache/tika/metadata/Property;

    .line 105
    const-string/jumbo v0, "meta:character-count"

    .line 104
    invoke-static {v0}, Lorg/apache/tika/metadata/Property;->internalInteger(Ljava/lang/String;)Lorg/apache/tika/metadata/Property;

    move-result-object v0

    sput-object v0, Lorg/apache/tika/metadata/Office;->CHARACTER_COUNT:Lorg/apache/tika/metadata/Property;

    .line 109
    const-string/jumbo v0, "meta:character-count-with-spaces"

    .line 108
    invoke-static {v0}, Lorg/apache/tika/metadata/Property;->internalInteger(Ljava/lang/String;)Lorg/apache/tika/metadata/Property;

    move-result-object v0

    sput-object v0, Lorg/apache/tika/metadata/Office;->CHARACTER_COUNT_WITH_SPACES:Lorg/apache/tika/metadata/Property;

    .line 113
    const-string/jumbo v0, "meta:table-count"

    .line 112
    invoke-static {v0}, Lorg/apache/tika/metadata/Property;->internalInteger(Ljava/lang/String;)Lorg/apache/tika/metadata/Property;

    move-result-object v0

    sput-object v0, Lorg/apache/tika/metadata/Office;->TABLE_COUNT:Lorg/apache/tika/metadata/Property;

    .line 117
    const-string/jumbo v0, "meta:image-count"

    .line 116
    invoke-static {v0}, Lorg/apache/tika/metadata/Property;->internalInteger(Ljava/lang/String;)Lorg/apache/tika/metadata/Property;

    move-result-object v0

    sput-object v0, Lorg/apache/tika/metadata/Office;->IMAGE_COUNT:Lorg/apache/tika/metadata/Property;

    .line 124
    const-string/jumbo v0, "meta:object-count"

    .line 123
    invoke-static {v0}, Lorg/apache/tika/metadata/Property;->internalInteger(Ljava/lang/String;)Lorg/apache/tika/metadata/Property;

    move-result-object v0

    sput-object v0, Lorg/apache/tika/metadata/Office;->OBJECT_COUNT:Lorg/apache/tika/metadata/Property;

    .line 124
    return-void
.end method
