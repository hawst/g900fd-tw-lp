.class public interface abstract Lorg/apache/tika/metadata/OfficeOpenXMLExtended;
.super Ljava/lang/Object;
.source "OfficeOpenXMLExtended.java"


# static fields
.field public static final APPLICATION:Lorg/apache/tika/metadata/Property;

.field public static final APP_VERSION:Lorg/apache/tika/metadata/Property;

.field public static final COMMENTS:Lorg/apache/tika/metadata/Property;

.field public static final COMPANY:Lorg/apache/tika/metadata/Property;

.field public static final DOC_SECURITY:Lorg/apache/tika/metadata/Property;

.field public static final HIDDEN_SLIDES:Lorg/apache/tika/metadata/Property;

.field public static final MANAGER:Lorg/apache/tika/metadata/Property;

.field public static final NAMESPACE_URI:Ljava/lang/String; = "http://schemas.openxmlformats.org/officeDocument/2006/extended-properties/"

.field public static final NOTES:Lorg/apache/tika/metadata/Property;

.field public static final PREFIX:Ljava/lang/String; = "extended-properties"

.field public static final PRESENTATION_FORMAT:Lorg/apache/tika/metadata/Property;

.field public static final TEMPLATE:Lorg/apache/tika/metadata/Property;

.field public static final TOTAL_TIME:Lorg/apache/tika/metadata/Property;

.field public static final WORD_PROCESSING_NAMESPACE_URI:Ljava/lang/String; = "http://schemas.openxmlformats.org/wordprocessingml/2006/main"

.field public static final WORD_PROCESSING_PREFIX:Ljava/lang/String; = "w"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    const-string/jumbo v0, "extended-properties:Template"

    .line 38
    invoke-static {v0}, Lorg/apache/tika/metadata/Property;->externalText(Ljava/lang/String;)Lorg/apache/tika/metadata/Property;

    move-result-object v0

    sput-object v0, Lorg/apache/tika/metadata/OfficeOpenXMLExtended;->TEMPLATE:Lorg/apache/tika/metadata/Property;

    .line 42
    const-string/jumbo v0, "extended-properties:Manager"

    .line 41
    invoke-static {v0}, Lorg/apache/tika/metadata/Property;->externalText(Ljava/lang/String;)Lorg/apache/tika/metadata/Property;

    move-result-object v0

    sput-object v0, Lorg/apache/tika/metadata/OfficeOpenXMLExtended;->MANAGER:Lorg/apache/tika/metadata/Property;

    .line 45
    const-string/jumbo v0, "extended-properties:Company"

    .line 44
    invoke-static {v0}, Lorg/apache/tika/metadata/Property;->externalText(Ljava/lang/String;)Lorg/apache/tika/metadata/Property;

    move-result-object v0

    sput-object v0, Lorg/apache/tika/metadata/OfficeOpenXMLExtended;->COMPANY:Lorg/apache/tika/metadata/Property;

    .line 48
    const-string/jumbo v0, "extended-properties:PresentationFormat"

    .line 47
    invoke-static {v0}, Lorg/apache/tika/metadata/Property;->externalText(Ljava/lang/String;)Lorg/apache/tika/metadata/Property;

    move-result-object v0

    sput-object v0, Lorg/apache/tika/metadata/OfficeOpenXMLExtended;->PRESENTATION_FORMAT:Lorg/apache/tika/metadata/Property;

    .line 51
    const-string/jumbo v0, "extended-properties:Notes"

    .line 50
    invoke-static {v0}, Lorg/apache/tika/metadata/Property;->externalInteger(Ljava/lang/String;)Lorg/apache/tika/metadata/Property;

    move-result-object v0

    sput-object v0, Lorg/apache/tika/metadata/OfficeOpenXMLExtended;->NOTES:Lorg/apache/tika/metadata/Property;

    .line 54
    const-string/jumbo v0, "extended-properties:TotalTime"

    .line 53
    invoke-static {v0}, Lorg/apache/tika/metadata/Property;->externalInteger(Ljava/lang/String;)Lorg/apache/tika/metadata/Property;

    move-result-object v0

    sput-object v0, Lorg/apache/tika/metadata/OfficeOpenXMLExtended;->TOTAL_TIME:Lorg/apache/tika/metadata/Property;

    .line 57
    const-string/jumbo v0, "extended-properties:HiddedSlides"

    .line 56
    invoke-static {v0}, Lorg/apache/tika/metadata/Property;->externalInteger(Ljava/lang/String;)Lorg/apache/tika/metadata/Property;

    move-result-object v0

    sput-object v0, Lorg/apache/tika/metadata/OfficeOpenXMLExtended;->HIDDEN_SLIDES:Lorg/apache/tika/metadata/Property;

    .line 60
    const-string/jumbo v0, "extended-properties:Application"

    .line 59
    invoke-static {v0}, Lorg/apache/tika/metadata/Property;->externalText(Ljava/lang/String;)Lorg/apache/tika/metadata/Property;

    move-result-object v0

    sput-object v0, Lorg/apache/tika/metadata/OfficeOpenXMLExtended;->APPLICATION:Lorg/apache/tika/metadata/Property;

    .line 63
    const-string/jumbo v0, "extended-properties:AppVersion"

    .line 62
    invoke-static {v0}, Lorg/apache/tika/metadata/Property;->externalText(Ljava/lang/String;)Lorg/apache/tika/metadata/Property;

    move-result-object v0

    sput-object v0, Lorg/apache/tika/metadata/OfficeOpenXMLExtended;->APP_VERSION:Lorg/apache/tika/metadata/Property;

    .line 66
    const-string/jumbo v0, "extended-properties:DocSecurity"

    .line 65
    invoke-static {v0}, Lorg/apache/tika/metadata/Property;->externalInteger(Ljava/lang/String;)Lorg/apache/tika/metadata/Property;

    move-result-object v0

    sput-object v0, Lorg/apache/tika/metadata/OfficeOpenXMLExtended;->DOC_SECURITY:Lorg/apache/tika/metadata/Property;

    .line 69
    const-string/jumbo v0, "w:comments"

    .line 68
    invoke-static {v0}, Lorg/apache/tika/metadata/Property;->externalTextBag(Ljava/lang/String;)Lorg/apache/tika/metadata/Property;

    move-result-object v0

    sput-object v0, Lorg/apache/tika/metadata/OfficeOpenXMLExtended;->COMMENTS:Lorg/apache/tika/metadata/Property;

    .line 69
    return-void
.end method
