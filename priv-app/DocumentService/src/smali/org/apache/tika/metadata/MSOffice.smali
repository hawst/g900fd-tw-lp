.class public interface abstract Lorg/apache/tika/metadata/MSOffice;
.super Ljava/lang/Object;
.source "MSOffice.java"


# static fields
.field public static final APPLICATION_NAME:Ljava/lang/String; = "Application-Name"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final APPLICATION_VERSION:Ljava/lang/String; = "Application-Version"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final AUTHOR:Ljava/lang/String; = "Author"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final CATEGORY:Ljava/lang/String; = "Category"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final CHARACTER_COUNT:Lorg/apache/tika/metadata/Property;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final CHARACTER_COUNT_WITH_SPACES:Lorg/apache/tika/metadata/Property;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final COMMENTS:Ljava/lang/String; = "Comments"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final COMPANY:Ljava/lang/String; = "Company"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final CONTENT_STATUS:Ljava/lang/String; = "Content-Status"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final CREATION_DATE:Lorg/apache/tika/metadata/Property;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final EDIT_TIME:Ljava/lang/String; = "Edit-Time"

.field public static final IMAGE_COUNT:Lorg/apache/tika/metadata/Property;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final KEYWORDS:Ljava/lang/String; = "Keywords"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final LAST_AUTHOR:Ljava/lang/String; = "Last-Author"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final LAST_PRINTED:Lorg/apache/tika/metadata/Property;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final LAST_SAVED:Lorg/apache/tika/metadata/Property;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final LINE_COUNT:Lorg/apache/tika/metadata/Property;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final MANAGER:Ljava/lang/String; = "Manager"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final NOTES:Ljava/lang/String; = "Notes"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final OBJECT_COUNT:Lorg/apache/tika/metadata/Property;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final PAGE_COUNT:Lorg/apache/tika/metadata/Property;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final PARAGRAPH_COUNT:Lorg/apache/tika/metadata/Property;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final PRESENTATION_FORMAT:Ljava/lang/String; = "Presentation-Format"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final REVISION_NUMBER:Ljava/lang/String; = "Revision-Number"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final SECURITY:Ljava/lang/String; = "Security"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final SLIDE_COUNT:Lorg/apache/tika/metadata/Property;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final TABLE_COUNT:Lorg/apache/tika/metadata/Property;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final TEMPLATE:Ljava/lang/String; = "Template"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final TOTAL_TIME:Ljava/lang/String; = "Total-Time"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final USER_DEFINED_METADATA_NAME_PREFIX:Ljava/lang/String; = "custom:"

.field public static final VERSION:Ljava/lang/String; = "Version"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final WORD_COUNT:Lorg/apache/tika/metadata/Property;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 64
    const-string/jumbo v0, "Slide-Count"

    invoke-static {v0}, Lorg/apache/tika/metadata/Property;->internalInteger(Ljava/lang/String;)Lorg/apache/tika/metadata/Property;

    move-result-object v0

    .line 63
    sput-object v0, Lorg/apache/tika/metadata/MSOffice;->SLIDE_COUNT:Lorg/apache/tika/metadata/Property;

    .line 68
    const-string/jumbo v0, "Page-Count"

    invoke-static {v0}, Lorg/apache/tika/metadata/Property;->internalInteger(Ljava/lang/String;)Lorg/apache/tika/metadata/Property;

    move-result-object v0

    .line 67
    sput-object v0, Lorg/apache/tika/metadata/MSOffice;->PAGE_COUNT:Lorg/apache/tika/metadata/Property;

    .line 72
    const-string/jumbo v0, "Paragraph-Count"

    invoke-static {v0}, Lorg/apache/tika/metadata/Property;->internalInteger(Ljava/lang/String;)Lorg/apache/tika/metadata/Property;

    move-result-object v0

    .line 71
    sput-object v0, Lorg/apache/tika/metadata/MSOffice;->PARAGRAPH_COUNT:Lorg/apache/tika/metadata/Property;

    .line 76
    const-string/jumbo v0, "Line-Count"

    invoke-static {v0}, Lorg/apache/tika/metadata/Property;->internalInteger(Ljava/lang/String;)Lorg/apache/tika/metadata/Property;

    move-result-object v0

    .line 75
    sput-object v0, Lorg/apache/tika/metadata/MSOffice;->LINE_COUNT:Lorg/apache/tika/metadata/Property;

    .line 80
    const-string/jumbo v0, "Word-Count"

    invoke-static {v0}, Lorg/apache/tika/metadata/Property;->internalInteger(Ljava/lang/String;)Lorg/apache/tika/metadata/Property;

    move-result-object v0

    .line 79
    sput-object v0, Lorg/apache/tika/metadata/MSOffice;->WORD_COUNT:Lorg/apache/tika/metadata/Property;

    .line 84
    const-string/jumbo v0, "Character Count"

    invoke-static {v0}, Lorg/apache/tika/metadata/Property;->internalInteger(Ljava/lang/String;)Lorg/apache/tika/metadata/Property;

    move-result-object v0

    .line 83
    sput-object v0, Lorg/apache/tika/metadata/MSOffice;->CHARACTER_COUNT:Lorg/apache/tika/metadata/Property;

    .line 88
    const-string/jumbo v0, "Character-Count-With-Spaces"

    invoke-static {v0}, Lorg/apache/tika/metadata/Property;->internalInteger(Ljava/lang/String;)Lorg/apache/tika/metadata/Property;

    move-result-object v0

    .line 87
    sput-object v0, Lorg/apache/tika/metadata/MSOffice;->CHARACTER_COUNT_WITH_SPACES:Lorg/apache/tika/metadata/Property;

    .line 92
    const-string/jumbo v0, "Table-Count"

    invoke-static {v0}, Lorg/apache/tika/metadata/Property;->internalInteger(Ljava/lang/String;)Lorg/apache/tika/metadata/Property;

    move-result-object v0

    .line 91
    sput-object v0, Lorg/apache/tika/metadata/MSOffice;->TABLE_COUNT:Lorg/apache/tika/metadata/Property;

    .line 96
    const-string/jumbo v0, "Image-Count"

    invoke-static {v0}, Lorg/apache/tika/metadata/Property;->internalInteger(Ljava/lang/String;)Lorg/apache/tika/metadata/Property;

    move-result-object v0

    .line 95
    sput-object v0, Lorg/apache/tika/metadata/MSOffice;->IMAGE_COUNT:Lorg/apache/tika/metadata/Property;

    .line 104
    const-string/jumbo v0, "Object-Count"

    invoke-static {v0}, Lorg/apache/tika/metadata/Property;->internalInteger(Ljava/lang/String;)Lorg/apache/tika/metadata/Property;

    move-result-object v0

    .line 103
    sput-object v0, Lorg/apache/tika/metadata/MSOffice;->OBJECT_COUNT:Lorg/apache/tika/metadata/Property;

    .line 112
    const-string/jumbo v0, "Creation-Date"

    invoke-static {v0}, Lorg/apache/tika/metadata/Property;->internalDate(Ljava/lang/String;)Lorg/apache/tika/metadata/Property;

    move-result-object v0

    .line 111
    sput-object v0, Lorg/apache/tika/metadata/MSOffice;->CREATION_DATE:Lorg/apache/tika/metadata/Property;

    .line 116
    const-string/jumbo v0, "Last-Save-Date"

    invoke-static {v0}, Lorg/apache/tika/metadata/Property;->internalDate(Ljava/lang/String;)Lorg/apache/tika/metadata/Property;

    move-result-object v0

    .line 115
    sput-object v0, Lorg/apache/tika/metadata/MSOffice;->LAST_SAVED:Lorg/apache/tika/metadata/Property;

    .line 120
    const-string/jumbo v0, "Last-Printed"

    invoke-static {v0}, Lorg/apache/tika/metadata/Property;->internalDate(Ljava/lang/String;)Lorg/apache/tika/metadata/Property;

    move-result-object v0

    .line 119
    sput-object v0, Lorg/apache/tika/metadata/MSOffice;->LAST_PRINTED:Lorg/apache/tika/metadata/Property;

    .line 127
    return-void
.end method
