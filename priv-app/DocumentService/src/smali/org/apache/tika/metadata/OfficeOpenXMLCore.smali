.class public interface abstract Lorg/apache/tika/metadata/OfficeOpenXMLCore;
.super Ljava/lang/Object;
.source "OfficeOpenXMLCore.java"


# static fields
.field public static final CATEGORY:Lorg/apache/tika/metadata/Property;

.field public static final CONTENT_STATUS:Lorg/apache/tika/metadata/Property;

.field public static final LAST_MODIFIED_BY:Lorg/apache/tika/metadata/Property;

.field public static final LAST_PRINTED:Lorg/apache/tika/metadata/Property;

.field public static final NAMESPACE_URI:Ljava/lang/String; = "http://schemas.openxmlformats.org/package/2006/metadata/core-properties/"

.field public static final PREFIX:Ljava/lang/String; = "cp"

.field public static final REVISION:Lorg/apache/tika/metadata/Property;

.field public static final SUBJECT:Lorg/apache/tika/metadata/Property;

.field public static final VERSION:Lorg/apache/tika/metadata/Property;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    const-string/jumbo v0, "cp:category"

    .line 38
    invoke-static {v0}, Lorg/apache/tika/metadata/Property;->externalText(Ljava/lang/String;)Lorg/apache/tika/metadata/Property;

    move-result-object v0

    sput-object v0, Lorg/apache/tika/metadata/OfficeOpenXMLCore;->CATEGORY:Lorg/apache/tika/metadata/Property;

    .line 45
    const-string/jumbo v0, "cp:contentStatus"

    .line 44
    invoke-static {v0}, Lorg/apache/tika/metadata/Property;->externalText(Ljava/lang/String;)Lorg/apache/tika/metadata/Property;

    move-result-object v0

    sput-object v0, Lorg/apache/tika/metadata/OfficeOpenXMLCore;->CONTENT_STATUS:Lorg/apache/tika/metadata/Property;

    .line 51
    const-string/jumbo v0, "cp:lastModifiedBy"

    .line 50
    invoke-static {v0}, Lorg/apache/tika/metadata/Property;->externalText(Ljava/lang/String;)Lorg/apache/tika/metadata/Property;

    move-result-object v0

    sput-object v0, Lorg/apache/tika/metadata/OfficeOpenXMLCore;->LAST_MODIFIED_BY:Lorg/apache/tika/metadata/Property;

    .line 57
    const-string/jumbo v0, "cp:lastPrinted"

    .line 56
    invoke-static {v0}, Lorg/apache/tika/metadata/Property;->externalDate(Ljava/lang/String;)Lorg/apache/tika/metadata/Property;

    move-result-object v0

    sput-object v0, Lorg/apache/tika/metadata/OfficeOpenXMLCore;->LAST_PRINTED:Lorg/apache/tika/metadata/Property;

    .line 63
    const-string/jumbo v0, "cp:revision"

    .line 62
    invoke-static {v0}, Lorg/apache/tika/metadata/Property;->externalText(Ljava/lang/String;)Lorg/apache/tika/metadata/Property;

    move-result-object v0

    sput-object v0, Lorg/apache/tika/metadata/OfficeOpenXMLCore;->REVISION:Lorg/apache/tika/metadata/Property;

    .line 69
    const-string/jumbo v0, "cp:version"

    .line 68
    invoke-static {v0}, Lorg/apache/tika/metadata/Property;->externalText(Ljava/lang/String;)Lorg/apache/tika/metadata/Property;

    move-result-object v0

    sput-object v0, Lorg/apache/tika/metadata/OfficeOpenXMLCore;->VERSION:Lorg/apache/tika/metadata/Property;

    .line 75
    const-string/jumbo v0, "cp:subject"

    .line 74
    invoke-static {v0}, Lorg/apache/tika/metadata/Property;->externalText(Ljava/lang/String;)Lorg/apache/tika/metadata/Property;

    move-result-object v0

    sput-object v0, Lorg/apache/tika/metadata/OfficeOpenXMLCore;->SUBJECT:Lorg/apache/tika/metadata/Property;

    .line 75
    return-void
.end method
