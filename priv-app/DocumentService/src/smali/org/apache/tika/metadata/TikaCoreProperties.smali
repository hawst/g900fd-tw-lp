.class public interface abstract Lorg/apache/tika/metadata/TikaCoreProperties;
.super Ljava/lang/Object;
.source "TikaCoreProperties.java"


# static fields
.field public static final ALTITUDE:Lorg/apache/tika/metadata/Property;

.field public static final COMMENTS:Lorg/apache/tika/metadata/Property;

.field public static final CONTRIBUTOR:Lorg/apache/tika/metadata/Property;

.field public static final COVERAGE:Lorg/apache/tika/metadata/Property;

.field public static final CREATED:Lorg/apache/tika/metadata/Property;

.field public static final CREATOR:Lorg/apache/tika/metadata/Property;

.field public static final CREATOR_TOOL:Lorg/apache/tika/metadata/Property;

.field public static final DESCRIPTION:Lorg/apache/tika/metadata/Property;

.field public static final FORMAT:Lorg/apache/tika/metadata/Property;

.field public static final IDENTIFIER:Lorg/apache/tika/metadata/Property;

.field public static final KEYWORDS:Lorg/apache/tika/metadata/Property;

.field public static final LANGUAGE:Lorg/apache/tika/metadata/Property;

.field public static final LATITUDE:Lorg/apache/tika/metadata/Property;

.field public static final LONGITUDE:Lorg/apache/tika/metadata/Property;

.field public static final METADATA_DATE:Lorg/apache/tika/metadata/Property;

.field public static final MODIFIED:Lorg/apache/tika/metadata/Property;

.field public static final MODIFIER:Lorg/apache/tika/metadata/Property;

.field public static final PRINT_DATE:Lorg/apache/tika/metadata/Property;

.field public static final PUBLISHER:Lorg/apache/tika/metadata/Property;

.field public static final RATING:Lorg/apache/tika/metadata/Property;

.field public static final RELATION:Lorg/apache/tika/metadata/Property;

.field public static final RIGHTS:Lorg/apache/tika/metadata/Property;

.field public static final SOURCE:Lorg/apache/tika/metadata/Property;

.field public static final TITLE:Lorg/apache/tika/metadata/Property;

.field public static final TRANSITION_KEYWORDS_TO_DC_SUBJECT:Lorg/apache/tika/metadata/Property;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final TRANSITION_SUBJECT_TO_DC_DESCRIPTION:Lorg/apache/tika/metadata/Property;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final TRANSITION_SUBJECT_TO_DC_TITLE:Lorg/apache/tika/metadata/Property;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final TRANSITION_SUBJECT_TO_OO_SUBJECT:Lorg/apache/tika/metadata/Property;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final TYPE:Lorg/apache/tika/metadata/Property;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v3, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 42
    sget-object v0, Lorg/apache/tika/metadata/DublinCore;->FORMAT:Lorg/apache/tika/metadata/Property;

    .line 43
    new-array v1, v5, [Lorg/apache/tika/metadata/Property;

    const-string/jumbo v2, "format"

    invoke-static {v2}, Lorg/apache/tika/metadata/Property;->internalText(Ljava/lang/String;)Lorg/apache/tika/metadata/Property;

    move-result-object v2

    aput-object v2, v1, v4

    .line 42
    invoke-static {v0, v1}, Lorg/apache/tika/metadata/Property;->composite(Lorg/apache/tika/metadata/Property;[Lorg/apache/tika/metadata/Property;)Lorg/apache/tika/metadata/Property;

    move-result-object v0

    sput-object v0, Lorg/apache/tika/metadata/TikaCoreProperties;->FORMAT:Lorg/apache/tika/metadata/Property;

    .line 48
    sget-object v0, Lorg/apache/tika/metadata/DublinCore;->IDENTIFIER:Lorg/apache/tika/metadata/Property;

    .line 49
    new-array v1, v5, [Lorg/apache/tika/metadata/Property;

    const-string/jumbo v2, "identifier"

    invoke-static {v2}, Lorg/apache/tika/metadata/Property;->internalText(Ljava/lang/String;)Lorg/apache/tika/metadata/Property;

    move-result-object v2

    aput-object v2, v1, v4

    .line 48
    invoke-static {v0, v1}, Lorg/apache/tika/metadata/Property;->composite(Lorg/apache/tika/metadata/Property;[Lorg/apache/tika/metadata/Property;)Lorg/apache/tika/metadata/Property;

    move-result-object v0

    sput-object v0, Lorg/apache/tika/metadata/TikaCoreProperties;->IDENTIFIER:Lorg/apache/tika/metadata/Property;

    .line 54
    sget-object v0, Lorg/apache/tika/metadata/DublinCore;->CONTRIBUTOR:Lorg/apache/tika/metadata/Property;

    .line 55
    new-array v1, v5, [Lorg/apache/tika/metadata/Property;

    const-string/jumbo v2, "contributor"

    invoke-static {v2}, Lorg/apache/tika/metadata/Property;->internalText(Ljava/lang/String;)Lorg/apache/tika/metadata/Property;

    move-result-object v2

    aput-object v2, v1, v4

    .line 54
    invoke-static {v0, v1}, Lorg/apache/tika/metadata/Property;->composite(Lorg/apache/tika/metadata/Property;[Lorg/apache/tika/metadata/Property;)Lorg/apache/tika/metadata/Property;

    move-result-object v0

    sput-object v0, Lorg/apache/tika/metadata/TikaCoreProperties;->CONTRIBUTOR:Lorg/apache/tika/metadata/Property;

    .line 60
    sget-object v0, Lorg/apache/tika/metadata/DublinCore;->COVERAGE:Lorg/apache/tika/metadata/Property;

    .line 61
    new-array v1, v5, [Lorg/apache/tika/metadata/Property;

    const-string/jumbo v2, "coverage"

    invoke-static {v2}, Lorg/apache/tika/metadata/Property;->internalText(Ljava/lang/String;)Lorg/apache/tika/metadata/Property;

    move-result-object v2

    aput-object v2, v1, v4

    .line 60
    invoke-static {v0, v1}, Lorg/apache/tika/metadata/Property;->composite(Lorg/apache/tika/metadata/Property;[Lorg/apache/tika/metadata/Property;)Lorg/apache/tika/metadata/Property;

    move-result-object v0

    sput-object v0, Lorg/apache/tika/metadata/TikaCoreProperties;->COVERAGE:Lorg/apache/tika/metadata/Property;

    .line 66
    sget-object v0, Lorg/apache/tika/metadata/DublinCore;->CREATOR:Lorg/apache/tika/metadata/Property;

    .line 67
    new-array v1, v3, [Lorg/apache/tika/metadata/Property;

    .line 68
    sget-object v2, Lorg/apache/tika/metadata/Office;->AUTHOR:Lorg/apache/tika/metadata/Property;

    aput-object v2, v1, v4

    .line 69
    const-string/jumbo v2, "creator"

    invoke-static {v2}, Lorg/apache/tika/metadata/Property;->internalTextBag(Ljava/lang/String;)Lorg/apache/tika/metadata/Property;

    move-result-object v2

    aput-object v2, v1, v5

    .line 70
    const-string/jumbo v2, "Author"

    invoke-static {v2}, Lorg/apache/tika/metadata/Property;->internalTextBag(Ljava/lang/String;)Lorg/apache/tika/metadata/Property;

    move-result-object v2

    aput-object v2, v1, v6

    .line 66
    invoke-static {v0, v1}, Lorg/apache/tika/metadata/Property;->composite(Lorg/apache/tika/metadata/Property;[Lorg/apache/tika/metadata/Property;)Lorg/apache/tika/metadata/Property;

    move-result-object v0

    sput-object v0, Lorg/apache/tika/metadata/TikaCoreProperties;->CREATOR:Lorg/apache/tika/metadata/Property;

    .line 76
    sget-object v0, Lorg/apache/tika/metadata/Office;->LAST_AUTHOR:Lorg/apache/tika/metadata/Property;

    .line 77
    new-array v1, v5, [Lorg/apache/tika/metadata/Property;

    const-string/jumbo v2, "Last-Author"

    invoke-static {v2}, Lorg/apache/tika/metadata/Property;->internalText(Ljava/lang/String;)Lorg/apache/tika/metadata/Property;

    move-result-object v2

    aput-object v2, v1, v4

    .line 76
    invoke-static {v0, v1}, Lorg/apache/tika/metadata/Property;->composite(Lorg/apache/tika/metadata/Property;[Lorg/apache/tika/metadata/Property;)Lorg/apache/tika/metadata/Property;

    move-result-object v0

    sput-object v0, Lorg/apache/tika/metadata/TikaCoreProperties;->MODIFIER:Lorg/apache/tika/metadata/Property;

    .line 82
    sget-object v0, Lorg/apache/tika/metadata/XMP;->CREATOR_TOOL:Lorg/apache/tika/metadata/Property;

    sput-object v0, Lorg/apache/tika/metadata/TikaCoreProperties;->CREATOR_TOOL:Lorg/apache/tika/metadata/Property;

    .line 87
    sget-object v0, Lorg/apache/tika/metadata/DublinCore;->LANGUAGE:Lorg/apache/tika/metadata/Property;

    .line 88
    new-array v1, v5, [Lorg/apache/tika/metadata/Property;

    const-string/jumbo v2, "language"

    invoke-static {v2}, Lorg/apache/tika/metadata/Property;->internalText(Ljava/lang/String;)Lorg/apache/tika/metadata/Property;

    move-result-object v2

    aput-object v2, v1, v4

    .line 87
    invoke-static {v0, v1}, Lorg/apache/tika/metadata/Property;->composite(Lorg/apache/tika/metadata/Property;[Lorg/apache/tika/metadata/Property;)Lorg/apache/tika/metadata/Property;

    move-result-object v0

    sput-object v0, Lorg/apache/tika/metadata/TikaCoreProperties;->LANGUAGE:Lorg/apache/tika/metadata/Property;

    .line 93
    sget-object v0, Lorg/apache/tika/metadata/DublinCore;->PUBLISHER:Lorg/apache/tika/metadata/Property;

    .line 94
    new-array v1, v5, [Lorg/apache/tika/metadata/Property;

    const-string/jumbo v2, "publisher"

    invoke-static {v2}, Lorg/apache/tika/metadata/Property;->internalText(Ljava/lang/String;)Lorg/apache/tika/metadata/Property;

    move-result-object v2

    aput-object v2, v1, v4

    .line 93
    invoke-static {v0, v1}, Lorg/apache/tika/metadata/Property;->composite(Lorg/apache/tika/metadata/Property;[Lorg/apache/tika/metadata/Property;)Lorg/apache/tika/metadata/Property;

    move-result-object v0

    sput-object v0, Lorg/apache/tika/metadata/TikaCoreProperties;->PUBLISHER:Lorg/apache/tika/metadata/Property;

    .line 99
    sget-object v0, Lorg/apache/tika/metadata/DublinCore;->RELATION:Lorg/apache/tika/metadata/Property;

    .line 100
    new-array v1, v5, [Lorg/apache/tika/metadata/Property;

    const-string/jumbo v2, "relation"

    invoke-static {v2}, Lorg/apache/tika/metadata/Property;->internalText(Ljava/lang/String;)Lorg/apache/tika/metadata/Property;

    move-result-object v2

    aput-object v2, v1, v4

    .line 99
    invoke-static {v0, v1}, Lorg/apache/tika/metadata/Property;->composite(Lorg/apache/tika/metadata/Property;[Lorg/apache/tika/metadata/Property;)Lorg/apache/tika/metadata/Property;

    move-result-object v0

    sput-object v0, Lorg/apache/tika/metadata/TikaCoreProperties;->RELATION:Lorg/apache/tika/metadata/Property;

    .line 105
    sget-object v0, Lorg/apache/tika/metadata/DublinCore;->RIGHTS:Lorg/apache/tika/metadata/Property;

    .line 106
    new-array v1, v5, [Lorg/apache/tika/metadata/Property;

    const-string/jumbo v2, "rights"

    invoke-static {v2}, Lorg/apache/tika/metadata/Property;->internalText(Ljava/lang/String;)Lorg/apache/tika/metadata/Property;

    move-result-object v2

    aput-object v2, v1, v4

    .line 105
    invoke-static {v0, v1}, Lorg/apache/tika/metadata/Property;->composite(Lorg/apache/tika/metadata/Property;[Lorg/apache/tika/metadata/Property;)Lorg/apache/tika/metadata/Property;

    move-result-object v0

    sput-object v0, Lorg/apache/tika/metadata/TikaCoreProperties;->RIGHTS:Lorg/apache/tika/metadata/Property;

    .line 111
    sget-object v0, Lorg/apache/tika/metadata/DublinCore;->SOURCE:Lorg/apache/tika/metadata/Property;

    .line 112
    new-array v1, v5, [Lorg/apache/tika/metadata/Property;

    const-string/jumbo v2, "source"

    invoke-static {v2}, Lorg/apache/tika/metadata/Property;->internalText(Ljava/lang/String;)Lorg/apache/tika/metadata/Property;

    move-result-object v2

    aput-object v2, v1, v4

    .line 111
    invoke-static {v0, v1}, Lorg/apache/tika/metadata/Property;->composite(Lorg/apache/tika/metadata/Property;[Lorg/apache/tika/metadata/Property;)Lorg/apache/tika/metadata/Property;

    move-result-object v0

    sput-object v0, Lorg/apache/tika/metadata/TikaCoreProperties;->SOURCE:Lorg/apache/tika/metadata/Property;

    .line 117
    sget-object v0, Lorg/apache/tika/metadata/DublinCore;->TYPE:Lorg/apache/tika/metadata/Property;

    .line 118
    new-array v1, v5, [Lorg/apache/tika/metadata/Property;

    const-string/jumbo v2, "type"

    invoke-static {v2}, Lorg/apache/tika/metadata/Property;->internalText(Ljava/lang/String;)Lorg/apache/tika/metadata/Property;

    move-result-object v2

    aput-object v2, v1, v4

    .line 117
    invoke-static {v0, v1}, Lorg/apache/tika/metadata/Property;->composite(Lorg/apache/tika/metadata/Property;[Lorg/apache/tika/metadata/Property;)Lorg/apache/tika/metadata/Property;

    move-result-object v0

    sput-object v0, Lorg/apache/tika/metadata/TikaCoreProperties;->TYPE:Lorg/apache/tika/metadata/Property;

    .line 126
    sget-object v0, Lorg/apache/tika/metadata/DublinCore;->TITLE:Lorg/apache/tika/metadata/Property;

    .line 127
    new-array v1, v5, [Lorg/apache/tika/metadata/Property;

    const-string/jumbo v2, "title"

    invoke-static {v2}, Lorg/apache/tika/metadata/Property;->internalText(Ljava/lang/String;)Lorg/apache/tika/metadata/Property;

    move-result-object v2

    aput-object v2, v1, v4

    .line 126
    invoke-static {v0, v1}, Lorg/apache/tika/metadata/Property;->composite(Lorg/apache/tika/metadata/Property;[Lorg/apache/tika/metadata/Property;)Lorg/apache/tika/metadata/Property;

    move-result-object v0

    sput-object v0, Lorg/apache/tika/metadata/TikaCoreProperties;->TITLE:Lorg/apache/tika/metadata/Property;

    .line 132
    sget-object v0, Lorg/apache/tika/metadata/DublinCore;->DESCRIPTION:Lorg/apache/tika/metadata/Property;

    .line 133
    new-array v1, v5, [Lorg/apache/tika/metadata/Property;

    const-string/jumbo v2, "description"

    invoke-static {v2}, Lorg/apache/tika/metadata/Property;->internalText(Ljava/lang/String;)Lorg/apache/tika/metadata/Property;

    move-result-object v2

    aput-object v2, v1, v4

    .line 132
    invoke-static {v0, v1}, Lorg/apache/tika/metadata/Property;->composite(Lorg/apache/tika/metadata/Property;[Lorg/apache/tika/metadata/Property;)Lorg/apache/tika/metadata/Property;

    move-result-object v0

    sput-object v0, Lorg/apache/tika/metadata/TikaCoreProperties;->DESCRIPTION:Lorg/apache/tika/metadata/Property;

    .line 139
    sget-object v0, Lorg/apache/tika/metadata/DublinCore;->SUBJECT:Lorg/apache/tika/metadata/Property;

    .line 140
    new-array v1, v3, [Lorg/apache/tika/metadata/Property;

    .line 141
    sget-object v2, Lorg/apache/tika/metadata/Office;->KEYWORDS:Lorg/apache/tika/metadata/Property;

    aput-object v2, v1, v4

    .line 142
    const-string/jumbo v2, "Keywords"

    invoke-static {v2}, Lorg/apache/tika/metadata/Property;->internalTextBag(Ljava/lang/String;)Lorg/apache/tika/metadata/Property;

    move-result-object v2

    aput-object v2, v1, v5

    .line 143
    const-string/jumbo v2, "subject"

    invoke-static {v2}, Lorg/apache/tika/metadata/Property;->internalTextBag(Ljava/lang/String;)Lorg/apache/tika/metadata/Property;

    move-result-object v2

    aput-object v2, v1, v6

    .line 139
    invoke-static {v0, v1}, Lorg/apache/tika/metadata/Property;->composite(Lorg/apache/tika/metadata/Property;[Lorg/apache/tika/metadata/Property;)Lorg/apache/tika/metadata/Property;

    move-result-object v0

    sput-object v0, Lorg/apache/tika/metadata/TikaCoreProperties;->KEYWORDS:Lorg/apache/tika/metadata/Property;

    .line 152
    sget-object v0, Lorg/apache/tika/metadata/DublinCore;->CREATED:Lorg/apache/tika/metadata/Property;

    .line 153
    new-array v1, v6, [Lorg/apache/tika/metadata/Property;

    .line 154
    sget-object v2, Lorg/apache/tika/metadata/Office;->CREATION_DATE:Lorg/apache/tika/metadata/Property;

    aput-object v2, v1, v4

    .line 155
    sget-object v2, Lorg/apache/tika/metadata/MSOffice;->CREATION_DATE:Lorg/apache/tika/metadata/Property;

    aput-object v2, v1, v5

    .line 152
    invoke-static {v0, v1}, Lorg/apache/tika/metadata/Property;->composite(Lorg/apache/tika/metadata/Property;[Lorg/apache/tika/metadata/Property;)Lorg/apache/tika/metadata/Property;

    move-result-object v0

    sput-object v0, Lorg/apache/tika/metadata/TikaCoreProperties;->CREATED:Lorg/apache/tika/metadata/Property;

    .line 163
    sget-object v0, Lorg/apache/tika/metadata/DublinCore;->MODIFIED:Lorg/apache/tika/metadata/Property;

    .line 164
    const/4 v1, 0x5

    new-array v1, v1, [Lorg/apache/tika/metadata/Property;

    .line 165
    sget-object v2, Lorg/apache/tika/metadata/Metadata;->DATE:Lorg/apache/tika/metadata/Property;

    aput-object v2, v1, v4

    .line 166
    sget-object v2, Lorg/apache/tika/metadata/Office;->SAVE_DATE:Lorg/apache/tika/metadata/Property;

    aput-object v2, v1, v5

    .line 167
    sget-object v2, Lorg/apache/tika/metadata/MSOffice;->LAST_SAVED:Lorg/apache/tika/metadata/Property;

    aput-object v2, v1, v6

    .line 168
    const-string/jumbo v2, "modified"

    invoke-static {v2}, Lorg/apache/tika/metadata/Property;->internalText(Ljava/lang/String;)Lorg/apache/tika/metadata/Property;

    move-result-object v2

    aput-object v2, v1, v3

    const/4 v2, 0x4

    .line 169
    const-string/jumbo v3, "Last-Modified"

    invoke-static {v3}, Lorg/apache/tika/metadata/Property;->internalText(Ljava/lang/String;)Lorg/apache/tika/metadata/Property;

    move-result-object v3

    aput-object v3, v1, v2

    .line 163
    invoke-static {v0, v1}, Lorg/apache/tika/metadata/Property;->composite(Lorg/apache/tika/metadata/Property;[Lorg/apache/tika/metadata/Property;)Lorg/apache/tika/metadata/Property;

    move-result-object v0

    sput-object v0, Lorg/apache/tika/metadata/TikaCoreProperties;->MODIFIED:Lorg/apache/tika/metadata/Property;

    .line 173
    sget-object v0, Lorg/apache/tika/metadata/Office;->PRINT_DATE:Lorg/apache/tika/metadata/Property;

    .line 174
    new-array v1, v5, [Lorg/apache/tika/metadata/Property;

    sget-object v2, Lorg/apache/tika/metadata/MSOffice;->LAST_PRINTED:Lorg/apache/tika/metadata/Property;

    aput-object v2, v1, v4

    .line 173
    invoke-static {v0, v1}, Lorg/apache/tika/metadata/Property;->composite(Lorg/apache/tika/metadata/Property;[Lorg/apache/tika/metadata/Property;)Lorg/apache/tika/metadata/Property;

    move-result-object v0

    sput-object v0, Lorg/apache/tika/metadata/TikaCoreProperties;->PRINT_DATE:Lorg/apache/tika/metadata/Property;

    .line 179
    sget-object v0, Lorg/apache/tika/metadata/XMP;->METADATA_DATE:Lorg/apache/tika/metadata/Property;

    sput-object v0, Lorg/apache/tika/metadata/TikaCoreProperties;->METADATA_DATE:Lorg/apache/tika/metadata/Property;

    .line 187
    sget-object v0, Lorg/apache/tika/metadata/Geographic;->LATITUDE:Lorg/apache/tika/metadata/Property;

    sput-object v0, Lorg/apache/tika/metadata/TikaCoreProperties;->LATITUDE:Lorg/apache/tika/metadata/Property;

    .line 192
    sget-object v0, Lorg/apache/tika/metadata/Geographic;->LONGITUDE:Lorg/apache/tika/metadata/Property;

    sput-object v0, Lorg/apache/tika/metadata/TikaCoreProperties;->LONGITUDE:Lorg/apache/tika/metadata/Property;

    .line 197
    sget-object v0, Lorg/apache/tika/metadata/Geographic;->ALTITUDE:Lorg/apache/tika/metadata/Property;

    sput-object v0, Lorg/apache/tika/metadata/TikaCoreProperties;->ALTITUDE:Lorg/apache/tika/metadata/Property;

    .line 205
    sget-object v0, Lorg/apache/tika/metadata/XMP;->RATING:Lorg/apache/tika/metadata/Property;

    sput-object v0, Lorg/apache/tika/metadata/TikaCoreProperties;->RATING:Lorg/apache/tika/metadata/Property;

    .line 210
    sget-object v0, Lorg/apache/tika/metadata/OfficeOpenXMLExtended;->COMMENTS:Lorg/apache/tika/metadata/Property;

    .line 211
    new-array v1, v6, [Lorg/apache/tika/metadata/Property;

    .line 212
    const-string/jumbo v2, "comment"

    invoke-static {v2}, Lorg/apache/tika/metadata/Property;->internalTextBag(Ljava/lang/String;)Lorg/apache/tika/metadata/Property;

    move-result-object v2

    aput-object v2, v1, v4

    .line 213
    const-string/jumbo v2, "Comments"

    invoke-static {v2}, Lorg/apache/tika/metadata/Property;->internalTextBag(Ljava/lang/String;)Lorg/apache/tika/metadata/Property;

    move-result-object v2

    aput-object v2, v1, v5

    .line 210
    invoke-static {v0, v1}, Lorg/apache/tika/metadata/Property;->composite(Lorg/apache/tika/metadata/Property;[Lorg/apache/tika/metadata/Property;)Lorg/apache/tika/metadata/Property;

    move-result-object v0

    sput-object v0, Lorg/apache/tika/metadata/TikaCoreProperties;->COMMENTS:Lorg/apache/tika/metadata/Property;

    .line 223
    sget-object v0, Lorg/apache/tika/metadata/DublinCore;->SUBJECT:Lorg/apache/tika/metadata/Property;

    .line 224
    new-array v1, v5, [Lorg/apache/tika/metadata/Property;

    const-string/jumbo v2, "Keywords"

    invoke-static {v2}, Lorg/apache/tika/metadata/Property;->internalTextBag(Ljava/lang/String;)Lorg/apache/tika/metadata/Property;

    move-result-object v2

    aput-object v2, v1, v4

    .line 223
    invoke-static {v0, v1}, Lorg/apache/tika/metadata/Property;->composite(Lorg/apache/tika/metadata/Property;[Lorg/apache/tika/metadata/Property;)Lorg/apache/tika/metadata/Property;

    move-result-object v0

    sput-object v0, Lorg/apache/tika/metadata/TikaCoreProperties;->TRANSITION_KEYWORDS_TO_DC_SUBJECT:Lorg/apache/tika/metadata/Property;

    .line 231
    sget-object v0, Lorg/apache/tika/metadata/DublinCore;->DESCRIPTION:Lorg/apache/tika/metadata/Property;

    .line 232
    new-array v1, v5, [Lorg/apache/tika/metadata/Property;

    const-string/jumbo v2, "subject"

    invoke-static {v2}, Lorg/apache/tika/metadata/Property;->internalText(Ljava/lang/String;)Lorg/apache/tika/metadata/Property;

    move-result-object v2

    aput-object v2, v1, v4

    .line 231
    invoke-static {v0, v1}, Lorg/apache/tika/metadata/Property;->composite(Lorg/apache/tika/metadata/Property;[Lorg/apache/tika/metadata/Property;)Lorg/apache/tika/metadata/Property;

    move-result-object v0

    sput-object v0, Lorg/apache/tika/metadata/TikaCoreProperties;->TRANSITION_SUBJECT_TO_DC_DESCRIPTION:Lorg/apache/tika/metadata/Property;

    .line 239
    sget-object v0, Lorg/apache/tika/metadata/DublinCore;->TITLE:Lorg/apache/tika/metadata/Property;

    .line 240
    new-array v1, v5, [Lorg/apache/tika/metadata/Property;

    const-string/jumbo v2, "subject"

    invoke-static {v2}, Lorg/apache/tika/metadata/Property;->internalText(Ljava/lang/String;)Lorg/apache/tika/metadata/Property;

    move-result-object v2

    aput-object v2, v1, v4

    .line 239
    invoke-static {v0, v1}, Lorg/apache/tika/metadata/Property;->composite(Lorg/apache/tika/metadata/Property;[Lorg/apache/tika/metadata/Property;)Lorg/apache/tika/metadata/Property;

    move-result-object v0

    sput-object v0, Lorg/apache/tika/metadata/TikaCoreProperties;->TRANSITION_SUBJECT_TO_DC_TITLE:Lorg/apache/tika/metadata/Property;

    .line 247
    sget-object v0, Lorg/apache/tika/metadata/OfficeOpenXMLCore;->SUBJECT:Lorg/apache/tika/metadata/Property;

    .line 248
    new-array v1, v5, [Lorg/apache/tika/metadata/Property;

    const-string/jumbo v2, "subject"

    invoke-static {v2}, Lorg/apache/tika/metadata/Property;->internalText(Ljava/lang/String;)Lorg/apache/tika/metadata/Property;

    move-result-object v2

    aput-object v2, v1, v4

    .line 247
    invoke-static {v0, v1}, Lorg/apache/tika/metadata/Property;->composite(Lorg/apache/tika/metadata/Property;[Lorg/apache/tika/metadata/Property;)Lorg/apache/tika/metadata/Property;

    move-result-object v0

    sput-object v0, Lorg/apache/tika/metadata/TikaCoreProperties;->TRANSITION_SUBJECT_TO_OO_SUBJECT:Lorg/apache/tika/metadata/Property;

    .line 248
    return-void
.end method
