.class public Lorg/apache/tika/metadata/Metadata;
.super Ljava/lang/Object;
.source "Metadata.java"

# interfaces
.implements Ljava/io/Serializable;
.implements Lorg/apache/tika/metadata/ClimateForcast;
.implements Lorg/apache/tika/metadata/CreativeCommons;
.implements Lorg/apache/tika/metadata/Geographic;
.implements Lorg/apache/tika/metadata/HttpHeaders;
.implements Lorg/apache/tika/metadata/MSOffice;
.implements Lorg/apache/tika/metadata/Message;
.implements Lorg/apache/tika/metadata/TIFF;
.implements Lorg/apache/tika/metadata/TikaMetadataKeys;
.implements Lorg/apache/tika/metadata/TikaMimeKeys;


# static fields
.field public static final CONTRIBUTOR:Ljava/lang/String; = "contributor"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final COVERAGE:Ljava/lang/String; = "coverage"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final CREATOR:Ljava/lang/String; = "creator"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final DATE:Lorg/apache/tika/metadata/Property;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final DESCRIPTION:Ljava/lang/String; = "description"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final FORMAT:Ljava/lang/String; = "format"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final IDENTIFIER:Ljava/lang/String; = "identifier"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final LANGUAGE:Ljava/lang/String; = "language"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private static final MIDDAY:Ljava/util/TimeZone;

.field public static final MODIFIED:Ljava/lang/String; = "modified"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final NAMESPACE_PREFIX_DELIMITER:Ljava/lang/String; = ":"

.field public static final PUBLISHER:Ljava/lang/String; = "publisher"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final RELATION:Ljava/lang/String; = "relation"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final RIGHTS:Ljava/lang/String; = "rights"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final SOURCE:Ljava/lang/String; = "source"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final SUBJECT:Ljava/lang/String; = "subject"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final TITLE:Ljava/lang/String; = "title"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final TYPE:Ljava/lang/String; = "type"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private static final UTC:Ljava/util/TimeZone;

.field private static final iso8601InputFormats:[Ljava/text/DateFormat;

.field private static final serialVersionUID:J = 0x4e0c33657eee179eL


# instance fields
.field private metadata:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 63
    const-string/jumbo v0, "date"

    invoke-static {v0}, Lorg/apache/tika/metadata/Property;->internalDate(Ljava/lang/String;)Lorg/apache/tika/metadata/Property;

    move-result-object v0

    sput-object v0, Lorg/apache/tika/metadata/Metadata;->DATE:Lorg/apache/tika/metadata/Property;

    .line 79
    const-string/jumbo v0, "UTC"

    invoke-static {v0}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v0

    sput-object v0, Lorg/apache/tika/metadata/Metadata;->UTC:Ljava/util/TimeZone;

    .line 90
    const-string/jumbo v0, "GMT-12:00"

    invoke-static {v0}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v0

    sput-object v0, Lorg/apache/tika/metadata/Metadata;->MIDDAY:Ljava/util/TimeZone;

    .line 100
    const/16 v0, 0x8

    new-array v0, v0, [Ljava/text/DateFormat;

    const/4 v1, 0x0

    .line 102
    const-string/jumbo v2, "yyyy-MM-dd\'T\'HH:mm:ss\'Z\'"

    sget-object v3, Lorg/apache/tika/metadata/Metadata;->UTC:Ljava/util/TimeZone;

    invoke-static {v2, v3}, Lorg/apache/tika/metadata/Metadata;->createDateFormat(Ljava/lang/String;Ljava/util/TimeZone;)Ljava/text/DateFormat;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    .line 103
    const-string/jumbo v2, "yyyy-MM-dd\'T\'HH:mm:ssZ"

    invoke-static {v2, v4}, Lorg/apache/tika/metadata/Metadata;->createDateFormat(Ljava/lang/String;Ljava/util/TimeZone;)Ljava/text/DateFormat;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    .line 104
    const-string/jumbo v2, "yyyy-MM-dd\'T\'HH:mm:ss"

    invoke-static {v2, v4}, Lorg/apache/tika/metadata/Metadata;->createDateFormat(Ljava/lang/String;Ljava/util/TimeZone;)Ljava/text/DateFormat;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    .line 106
    const-string/jumbo v2, "yyyy-MM-dd\' \'HH:mm:ss\'Z\'"

    sget-object v3, Lorg/apache/tika/metadata/Metadata;->UTC:Ljava/util/TimeZone;

    invoke-static {v2, v3}, Lorg/apache/tika/metadata/Metadata;->createDateFormat(Ljava/lang/String;Ljava/util/TimeZone;)Ljava/text/DateFormat;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    .line 107
    const-string/jumbo v2, "yyyy-MM-dd\' \'HH:mm:ssZ"

    invoke-static {v2, v4}, Lorg/apache/tika/metadata/Metadata;->createDateFormat(Ljava/lang/String;Ljava/util/TimeZone;)Ljava/text/DateFormat;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x5

    .line 108
    const-string/jumbo v2, "yyyy-MM-dd\' \'HH:mm:ss"

    invoke-static {v2, v4}, Lorg/apache/tika/metadata/Metadata;->createDateFormat(Ljava/lang/String;Ljava/util/TimeZone;)Ljava/text/DateFormat;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x6

    .line 110
    const-string/jumbo v2, "yyyy-MM-dd"

    sget-object v3, Lorg/apache/tika/metadata/Metadata;->MIDDAY:Ljava/util/TimeZone;

    invoke-static {v2, v3}, Lorg/apache/tika/metadata/Metadata;->createDateFormat(Ljava/lang/String;Ljava/util/TimeZone;)Ljava/text/DateFormat;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x7

    .line 111
    const-string/jumbo v2, "yyyy:MM:dd"

    sget-object v3, Lorg/apache/tika/metadata/Metadata;->MIDDAY:Ljava/util/TimeZone;

    invoke-static {v2, v3}, Lorg/apache/tika/metadata/Metadata;->createDateFormat(Ljava/lang/String;Ljava/util/TimeZone;)Ljava/text/DateFormat;

    move-result-object v2

    aput-object v2, v0, v1

    .line 100
    sput-object v0, Lorg/apache/tika/metadata/Metadata;->iso8601InputFormats:[Ljava/text/DateFormat;

    .line 112
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 174
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/tika/metadata/Metadata;->metadata:Ljava/util/Map;

    .line 175
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/tika/metadata/Metadata;->metadata:Ljava/util/Map;

    .line 176
    return-void
.end method

.method private _getValues(Ljava/lang/String;)[Ljava/lang/String;
    .locals 2
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 309
    iget-object v1, p0, Lorg/apache/tika/metadata/Metadata;->metadata:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    .line 310
    .local v0, "values":[Ljava/lang/String;
    if-nez v0, :cond_0

    .line 311
    const/4 v1, 0x0

    new-array v0, v1, [Ljava/lang/String;

    .line 313
    :cond_0
    return-object v0
.end method

.method private appendedValues([Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;
    .locals 3
    .param p1, "values"    # [Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 317
    array-length v1, p1

    add-int/lit8 v1, v1, 0x1

    new-array v0, v1, [Ljava/lang/String;

    .line 318
    .local v0, "newValues":[Ljava/lang/String;
    array-length v1, p1

    invoke-static {p1, v2, v0, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 319
    array-length v1, v0

    add-int/lit8 v1, v1, -0x1

    aput-object p2, v0, v1

    .line 320
    return-object v0
.end method

.method private static createDateFormat(Ljava/lang/String;Ljava/util/TimeZone;)Ljava/text/DateFormat;
    .locals 3
    .param p0, "format"    # Ljava/lang/String;
    .param p1, "timezone"    # Ljava/util/TimeZone;

    .prologue
    .line 116
    new-instance v0, Ljava/text/SimpleDateFormat;

    new-instance v1, Ljava/text/DateFormatSymbols;

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v1, v2}, Ljava/text/DateFormatSymbols;-><init>(Ljava/util/Locale;)V

    invoke-direct {v0, p0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/text/DateFormatSymbols;)V

    .line 117
    .local v0, "sdf":Ljava/text/SimpleDateFormat;
    if-eqz p1, :cond_0

    .line 118
    invoke-virtual {v0, p1}, Ljava/text/SimpleDateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 120
    :cond_0
    return-object v0
.end method

.method private static formatDate(Ljava/util/Date;)Ljava/lang/String;
    .locals 8
    .param p0, "date"    # Ljava/util/Date;

    .prologue
    const/4 v7, 0x5

    const/4 v6, 0x2

    const/4 v5, 0x1

    .line 159
    sget-object v1, Lorg/apache/tika/metadata/Metadata;->UTC:Ljava/util/TimeZone;

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-static {v1, v2}, Ljava/util/Calendar;->getInstance(Ljava/util/TimeZone;Ljava/util/Locale;)Ljava/util/Calendar;

    move-result-object v0

    .line 160
    .local v0, "calendar":Ljava/util/Calendar;
    invoke-virtual {v0, p0}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 162
    const-string/jumbo v1, "%04d-%02d-%02dT%02d:%02d:%02dZ"

    const/4 v2, 0x6

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 163
    invoke-virtual {v0, v5}, Ljava/util/Calendar;->get(I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    .line 164
    invoke-virtual {v0, v6}, Ljava/util/Calendar;->get(I)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    .line 165
    invoke-virtual {v0, v7}, Ljava/util/Calendar;->get(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v6

    const/4 v3, 0x3

    .line 166
    const/16 v4, 0xb

    invoke-virtual {v0, v4}, Ljava/util/Calendar;->get(I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x4

    .line 167
    const/16 v4, 0xc

    invoke-virtual {v0, v4}, Ljava/util/Calendar;->get(I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    .line 168
    const/16 v3, 0xd

    invoke-virtual {v0, v3}, Ljava/util/Calendar;->get(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v7

    .line 161
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private static declared-synchronized parseDate(Ljava/lang/String;)Ljava/util/Date;
    .locals 7
    .param p0, "date"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 134
    const-class v3, Lorg/apache/tika/metadata/Metadata;

    monitor-enter v3

    :try_start_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    .line 135
    .local v1, "n":I
    add-int/lit8 v4, v1, -0x3

    invoke-virtual {p0, v4}, Ljava/lang/String;->charAt(I)C

    move-result v4

    const/16 v5, 0x3a

    if-ne v4, v5, :cond_1

    .line 136
    add-int/lit8 v4, v1, -0x6

    invoke-virtual {p0, v4}, Ljava/lang/String;->charAt(I)C

    move-result v4

    const/16 v5, 0x2b

    if-eq v4, v5, :cond_0

    add-int/lit8 v4, v1, -0x6

    invoke-virtual {p0, v4}, Ljava/lang/String;->charAt(I)C

    move-result v4

    const/16 v5, 0x2d

    if-ne v4, v5, :cond_1

    .line 137
    :cond_0
    new-instance v4, Ljava/lang/StringBuilder;

    const/4 v5, 0x0

    add-int/lit8 v6, v1, -0x3

    invoke-virtual {p0, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    add-int/lit8 v5, v1, -0x2

    invoke-virtual {p0, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 141
    :cond_1
    sget-object v4, Lorg/apache/tika/metadata/Metadata;->iso8601InputFormats:[Ljava/text/DateFormat;

    array-length v5, v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    if-lt v2, v5, :cond_2

    .line 147
    const/4 v2, 0x0

    :goto_1
    monitor-exit v3

    return-object v2

    .line 141
    :cond_2
    :try_start_1
    aget-object v0, v4, v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 143
    .local v0, "format":Ljava/text/DateFormat;
    :try_start_2
    invoke-virtual {v0, p0}, Ljava/text/DateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;
    :try_end_2
    .catch Ljava/text/ParseException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v2

    goto :goto_1

    .line 144
    :catch_0
    move-exception v6

    .line 141
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 134
    .end local v0    # "format":Ljava/text/DateFormat;
    .end local v1    # "n":I
    :catchall_0
    move-exception v2

    monitor-exit v3

    throw v2
.end method


# virtual methods
.method public add(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 333
    iget-object v1, p0, Lorg/apache/tika/metadata/Metadata;->metadata:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    .line 334
    .local v0, "values":[Ljava/lang/String;
    if-nez v0, :cond_0

    .line 335
    invoke-virtual {p0, p1, p2}, Lorg/apache/tika/metadata/Metadata;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 339
    :goto_0
    return-void

    .line 337
    :cond_0
    iget-object v1, p0, Lorg/apache/tika/metadata/Metadata;->metadata:Ljava/util/Map;

    invoke-direct {p0, v0, p2}, Lorg/apache/tika/metadata/Metadata;->appendedValues([Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, p1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public add(Lorg/apache/tika/metadata/Property;Ljava/lang/String;)V
    .locals 3
    .param p1, "property"    # Lorg/apache/tika/metadata/Property;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 351
    iget-object v1, p0, Lorg/apache/tika/metadata/Metadata;->metadata:Ljava/util/Map;

    invoke-virtual {p1}, Lorg/apache/tika/metadata/Property;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    .line 352
    .local v0, "values":[Ljava/lang/String;
    if-nez v0, :cond_0

    .line 353
    invoke-virtual {p0, p1, p2}, Lorg/apache/tika/metadata/Metadata;->set(Lorg/apache/tika/metadata/Property;Ljava/lang/String;)V

    .line 361
    :goto_0
    return-void

    .line 355
    :cond_0
    invoke-virtual {p1}, Lorg/apache/tika/metadata/Property;->isMultiValuePermitted()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 356
    invoke-direct {p0, v0, p2}, Lorg/apache/tika/metadata/Metadata;->appendedValues([Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, p1, v1}, Lorg/apache/tika/metadata/Metadata;->set(Lorg/apache/tika/metadata/Property;[Ljava/lang/String;)V

    goto :goto_0

    .line 358
    :cond_1
    new-instance v1, Lorg/apache/tika/metadata/PropertyTypeException;

    invoke-virtual {p1}, Lorg/apache/tika/metadata/Property;->getPropertyType()Lorg/apache/tika/metadata/Property$PropertyType;

    move-result-object v2

    invoke-direct {v1, v2}, Lorg/apache/tika/metadata/PropertyTypeException;-><init>(Lorg/apache/tika/metadata/Property$PropertyType;)V

    throw v1
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 11
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v8, 0x0

    .line 519
    if-nez p1, :cond_1

    .line 547
    :cond_0
    :goto_0
    return v8

    .line 523
    :cond_1
    const/4 v5, 0x0

    .line 525
    .local v5, "other":Lorg/apache/tika/metadata/Metadata;
    :try_start_0
    move-object v0, p1

    check-cast v0, Lorg/apache/tika/metadata/Metadata;

    move-object v5, v0
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 530
    invoke-virtual {v5}, Lorg/apache/tika/metadata/Metadata;->size()I

    move-result v9

    invoke-virtual {p0}, Lorg/apache/tika/metadata/Metadata;->size()I

    move-result v10

    if-ne v9, v10, :cond_0

    .line 534
    invoke-virtual {p0}, Lorg/apache/tika/metadata/Metadata;->names()[Ljava/lang/String;

    move-result-object v4

    .line 535
    .local v4, "names":[Ljava/lang/String;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    array-length v9, v4

    if-lt v2, v9, :cond_2

    .line 547
    const/4 v8, 0x1

    goto :goto_0

    .line 526
    .end local v2    # "i":I
    .end local v4    # "names":[Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 527
    .local v1, "cce":Ljava/lang/ClassCastException;
    goto :goto_0

    .line 536
    .end local v1    # "cce":Ljava/lang/ClassCastException;
    .restart local v2    # "i":I
    .restart local v4    # "names":[Ljava/lang/String;
    :cond_2
    aget-object v9, v4, v2

    invoke-direct {v5, v9}, Lorg/apache/tika/metadata/Metadata;->_getValues(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    .line 537
    .local v6, "otherValues":[Ljava/lang/String;
    aget-object v9, v4, v2

    invoke-direct {p0, v9}, Lorg/apache/tika/metadata/Metadata;->_getValues(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    .line 538
    .local v7, "thisValues":[Ljava/lang/String;
    array-length v9, v6

    array-length v10, v7

    if-ne v9, v10, :cond_0

    .line 541
    const/4 v3, 0x0

    .local v3, "j":I
    :goto_2
    array-length v9, v6

    if-lt v3, v9, :cond_3

    .line 535
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 542
    :cond_3
    aget-object v9, v6, v3

    aget-object v10, v7, v3

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 541
    add-int/lit8 v3, v3, 0x1

    goto :goto_2
.end method

.method public get(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 218
    iget-object v1, p0, Lorg/apache/tika/metadata/Metadata;->metadata:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    .line 219
    .local v0, "values":[Ljava/lang/String;
    if-nez v0, :cond_0

    .line 220
    const/4 v1, 0x0

    .line 222
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    aget-object v1, v0, v1

    goto :goto_0
.end method

.method public get(Lorg/apache/tika/metadata/Property;)Ljava/lang/String;
    .locals 1
    .param p1, "property"    # Lorg/apache/tika/metadata/Property;

    .prologue
    .line 234
    invoke-virtual {p1}, Lorg/apache/tika/metadata/Property;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/apache/tika/metadata/Metadata;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDate(Lorg/apache/tika/metadata/Property;)Ljava/util/Date;
    .locals 4
    .param p1, "property"    # Lorg/apache/tika/metadata/Property;

    .prologue
    const/4 v1, 0x0

    .line 271
    invoke-virtual {p1}, Lorg/apache/tika/metadata/Property;->getPrimaryProperty()Lorg/apache/tika/metadata/Property;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/tika/metadata/Property;->getPropertyType()Lorg/apache/tika/metadata/Property$PropertyType;

    move-result-object v2

    sget-object v3, Lorg/apache/tika/metadata/Property$PropertyType;->SIMPLE:Lorg/apache/tika/metadata/Property$PropertyType;

    if-eq v2, v3, :cond_1

    .line 282
    :cond_0
    :goto_0
    return-object v1

    .line 274
    :cond_1
    invoke-virtual {p1}, Lorg/apache/tika/metadata/Property;->getPrimaryProperty()Lorg/apache/tika/metadata/Property;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/tika/metadata/Property;->getValueType()Lorg/apache/tika/metadata/Property$ValueType;

    move-result-object v2

    sget-object v3, Lorg/apache/tika/metadata/Property$ValueType;->DATE:Lorg/apache/tika/metadata/Property$ValueType;

    if-ne v2, v3, :cond_0

    .line 278
    invoke-virtual {p0, p1}, Lorg/apache/tika/metadata/Metadata;->get(Lorg/apache/tika/metadata/Property;)Ljava/lang/String;

    move-result-object v0

    .line 279
    .local v0, "v":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 280
    invoke-static {v0}, Lorg/apache/tika/metadata/Metadata;->parseDate(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v1

    goto :goto_0
.end method

.method public getInt(Lorg/apache/tika/metadata/Property;)Ljava/lang/Integer;
    .locals 5
    .param p1, "property"    # Lorg/apache/tika/metadata/Property;

    .prologue
    const/4 v2, 0x0

    .line 245
    invoke-virtual {p1}, Lorg/apache/tika/metadata/Property;->getPrimaryProperty()Lorg/apache/tika/metadata/Property;

    move-result-object v3

    invoke-virtual {v3}, Lorg/apache/tika/metadata/Property;->getPropertyType()Lorg/apache/tika/metadata/Property$PropertyType;

    move-result-object v3

    sget-object v4, Lorg/apache/tika/metadata/Property$PropertyType;->SIMPLE:Lorg/apache/tika/metadata/Property$PropertyType;

    if-eq v3, v4, :cond_1

    .line 259
    :cond_0
    :goto_0
    return-object v2

    .line 248
    :cond_1
    invoke-virtual {p1}, Lorg/apache/tika/metadata/Property;->getPrimaryProperty()Lorg/apache/tika/metadata/Property;

    move-result-object v3

    invoke-virtual {v3}, Lorg/apache/tika/metadata/Property;->getValueType()Lorg/apache/tika/metadata/Property$ValueType;

    move-result-object v3

    sget-object v4, Lorg/apache/tika/metadata/Property$ValueType;->INTEGER:Lorg/apache/tika/metadata/Property$ValueType;

    if-ne v3, v4, :cond_0

    .line 252
    invoke-virtual {p0, p1}, Lorg/apache/tika/metadata/Metadata;->get(Lorg/apache/tika/metadata/Property;)Ljava/lang/String;

    move-result-object v1

    .line 253
    .local v1, "v":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 257
    :try_start_0
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    goto :goto_0

    .line 258
    :catch_0
    move-exception v0

    .line 259
    .local v0, "e":Ljava/lang/NumberFormatException;
    goto :goto_0
.end method

.method public getValues(Ljava/lang/String;)[Ljava/lang/String;
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 305
    invoke-direct {p0, p1}, Lorg/apache/tika/metadata/Metadata;->_getValues(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getValues(Lorg/apache/tika/metadata/Property;)[Ljava/lang/String;
    .locals 1
    .param p1, "property"    # Lorg/apache/tika/metadata/Property;

    .prologue
    .line 294
    invoke-virtual {p1}, Lorg/apache/tika/metadata/Property;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/apache/tika/metadata/Metadata;->_getValues(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public isMultiValued(Ljava/lang/String;)Z
    .locals 2
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x1

    .line 197
    iget-object v0, p0, Lorg/apache/tika/metadata/Metadata;->metadata:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/tika/metadata/Metadata;->metadata:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    array-length v0, v0

    if-le v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isMultiValued(Lorg/apache/tika/metadata/Property;)Z
    .locals 3
    .param p1, "property"    # Lorg/apache/tika/metadata/Property;

    .prologue
    const/4 v1, 0x1

    .line 186
    iget-object v0, p0, Lorg/apache/tika/metadata/Metadata;->metadata:Ljava/util/Map;

    invoke-virtual {p1}, Lorg/apache/tika/metadata/Property;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/tika/metadata/Metadata;->metadata:Ljava/util/Map;

    invoke-virtual {p1}, Lorg/apache/tika/metadata/Property;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    array-length v0, v0

    if-le v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public names()[Ljava/lang/String;
    .locals 2

    .prologue
    .line 206
    iget-object v0, p0, Lorg/apache/tika/metadata/Metadata;->metadata:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/tika/metadata/Metadata;->metadata:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v1

    new-array v1, v1, [Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    return-object v0
.end method

.method public remove(Ljava/lang/String;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 505
    iget-object v0, p0, Lorg/apache/tika/metadata/Metadata;->metadata:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 506
    return-void
.end method

.method public set(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 389
    if-eqz p2, :cond_0

    .line 390
    iget-object v0, p0, Lorg/apache/tika/metadata/Metadata;->metadata:Ljava/util/Map;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object p2, v1, v2

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 394
    :goto_0
    return-void

    .line 392
    :cond_0
    iget-object v0, p0, Lorg/apache/tika/metadata/Metadata;->metadata:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public set(Lorg/apache/tika/metadata/Property;D)V
    .locals 4
    .param p1, "property"    # Lorg/apache/tika/metadata/Property;
    .param p2, "value"    # D

    .prologue
    .line 467
    invoke-virtual {p1}, Lorg/apache/tika/metadata/Property;->getPrimaryProperty()Lorg/apache/tika/metadata/Property;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/tika/metadata/Property;->getPropertyType()Lorg/apache/tika/metadata/Property$PropertyType;

    move-result-object v0

    sget-object v1, Lorg/apache/tika/metadata/Property$PropertyType;->SIMPLE:Lorg/apache/tika/metadata/Property$PropertyType;

    if-eq v0, v1, :cond_0

    .line 468
    new-instance v0, Lorg/apache/tika/metadata/PropertyTypeException;

    sget-object v1, Lorg/apache/tika/metadata/Property$PropertyType;->SIMPLE:Lorg/apache/tika/metadata/Property$PropertyType;

    invoke-virtual {p1}, Lorg/apache/tika/metadata/Property;->getPrimaryProperty()Lorg/apache/tika/metadata/Property;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/tika/metadata/Property;->getPropertyType()Lorg/apache/tika/metadata/Property$PropertyType;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/tika/metadata/PropertyTypeException;-><init>(Lorg/apache/tika/metadata/Property$PropertyType;Lorg/apache/tika/metadata/Property$PropertyType;)V

    throw v0

    .line 470
    :cond_0
    invoke-virtual {p1}, Lorg/apache/tika/metadata/Property;->getPrimaryProperty()Lorg/apache/tika/metadata/Property;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/tika/metadata/Property;->getValueType()Lorg/apache/tika/metadata/Property$ValueType;

    move-result-object v0

    sget-object v1, Lorg/apache/tika/metadata/Property$ValueType;->REAL:Lorg/apache/tika/metadata/Property$ValueType;

    if-eq v0, v1, :cond_1

    .line 471
    invoke-virtual {p1}, Lorg/apache/tika/metadata/Property;->getPrimaryProperty()Lorg/apache/tika/metadata/Property;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/tika/metadata/Property;->getValueType()Lorg/apache/tika/metadata/Property$ValueType;

    move-result-object v0

    sget-object v1, Lorg/apache/tika/metadata/Property$ValueType;->RATIONAL:Lorg/apache/tika/metadata/Property$ValueType;

    if-eq v0, v1, :cond_1

    .line 472
    new-instance v0, Lorg/apache/tika/metadata/PropertyTypeException;

    sget-object v1, Lorg/apache/tika/metadata/Property$ValueType;->REAL:Lorg/apache/tika/metadata/Property$ValueType;

    invoke-virtual {p1}, Lorg/apache/tika/metadata/Property;->getPrimaryProperty()Lorg/apache/tika/metadata/Property;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/tika/metadata/Property;->getValueType()Lorg/apache/tika/metadata/Property$ValueType;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/tika/metadata/PropertyTypeException;-><init>(Lorg/apache/tika/metadata/Property$ValueType;Lorg/apache/tika/metadata/Property$ValueType;)V

    throw v0

    .line 474
    :cond_1
    invoke-static {p2, p3}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lorg/apache/tika/metadata/Metadata;->set(Lorg/apache/tika/metadata/Property;Ljava/lang/String;)V

    .line 475
    return-void
.end method

.method public set(Lorg/apache/tika/metadata/Property;I)V
    .locals 3
    .param p1, "property"    # Lorg/apache/tika/metadata/Property;
    .param p2, "value"    # I

    .prologue
    .line 450
    invoke-virtual {p1}, Lorg/apache/tika/metadata/Property;->getPrimaryProperty()Lorg/apache/tika/metadata/Property;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/tika/metadata/Property;->getPropertyType()Lorg/apache/tika/metadata/Property$PropertyType;

    move-result-object v0

    sget-object v1, Lorg/apache/tika/metadata/Property$PropertyType;->SIMPLE:Lorg/apache/tika/metadata/Property$PropertyType;

    if-eq v0, v1, :cond_0

    .line 451
    new-instance v0, Lorg/apache/tika/metadata/PropertyTypeException;

    sget-object v1, Lorg/apache/tika/metadata/Property$PropertyType;->SIMPLE:Lorg/apache/tika/metadata/Property$PropertyType;

    invoke-virtual {p1}, Lorg/apache/tika/metadata/Property;->getPrimaryProperty()Lorg/apache/tika/metadata/Property;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/tika/metadata/Property;->getPropertyType()Lorg/apache/tika/metadata/Property$PropertyType;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/tika/metadata/PropertyTypeException;-><init>(Lorg/apache/tika/metadata/Property$PropertyType;Lorg/apache/tika/metadata/Property$PropertyType;)V

    throw v0

    .line 453
    :cond_0
    invoke-virtual {p1}, Lorg/apache/tika/metadata/Property;->getPrimaryProperty()Lorg/apache/tika/metadata/Property;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/tika/metadata/Property;->getValueType()Lorg/apache/tika/metadata/Property$ValueType;

    move-result-object v0

    sget-object v1, Lorg/apache/tika/metadata/Property$ValueType;->INTEGER:Lorg/apache/tika/metadata/Property$ValueType;

    if-eq v0, v1, :cond_1

    .line 454
    new-instance v0, Lorg/apache/tika/metadata/PropertyTypeException;

    sget-object v1, Lorg/apache/tika/metadata/Property$ValueType;->INTEGER:Lorg/apache/tika/metadata/Property$ValueType;

    invoke-virtual {p1}, Lorg/apache/tika/metadata/Property;->getPrimaryProperty()Lorg/apache/tika/metadata/Property;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/tika/metadata/Property;->getValueType()Lorg/apache/tika/metadata/Property$ValueType;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/tika/metadata/PropertyTypeException;-><init>(Lorg/apache/tika/metadata/Property$ValueType;Lorg/apache/tika/metadata/Property$ValueType;)V

    throw v0

    .line 456
    :cond_1
    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lorg/apache/tika/metadata/Metadata;->set(Lorg/apache/tika/metadata/Property;Ljava/lang/String;)V

    .line 457
    return-void
.end method

.method public set(Lorg/apache/tika/metadata/Property;Ljava/lang/String;)V
    .locals 4
    .param p1, "property"    # Lorg/apache/tika/metadata/Property;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 404
    if-nez p1, :cond_0

    .line 405
    new-instance v1, Ljava/lang/NullPointerException;

    const-string/jumbo v2, "property must not be null"

    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 407
    :cond_0
    invoke-virtual {p1}, Lorg/apache/tika/metadata/Property;->getPropertyType()Lorg/apache/tika/metadata/Property$PropertyType;

    move-result-object v1

    sget-object v2, Lorg/apache/tika/metadata/Property$PropertyType;->COMPOSITE:Lorg/apache/tika/metadata/Property$PropertyType;

    if-ne v1, v2, :cond_3

    .line 408
    invoke-virtual {p1}, Lorg/apache/tika/metadata/Property;->getPrimaryProperty()Lorg/apache/tika/metadata/Property;

    move-result-object v1

    invoke-virtual {p0, v1, p2}, Lorg/apache/tika/metadata/Metadata;->set(Lorg/apache/tika/metadata/Property;Ljava/lang/String;)V

    .line 409
    invoke-virtual {p1}, Lorg/apache/tika/metadata/Property;->getSecondaryExtractProperties()[Lorg/apache/tika/metadata/Property;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 410
    invoke-virtual {p1}, Lorg/apache/tika/metadata/Property;->getSecondaryExtractProperties()[Lorg/apache/tika/metadata/Property;

    move-result-object v2

    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-lt v1, v3, :cond_2

    .line 417
    :cond_1
    :goto_1
    return-void

    .line 410
    :cond_2
    aget-object v0, v2, v1

    .line 411
    .local v0, "secondaryExtractProperty":Lorg/apache/tika/metadata/Property;
    invoke-virtual {p0, v0, p2}, Lorg/apache/tika/metadata/Metadata;->set(Lorg/apache/tika/metadata/Property;Ljava/lang/String;)V

    .line 410
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 415
    .end local v0    # "secondaryExtractProperty":Lorg/apache/tika/metadata/Property;
    :cond_3
    invoke-virtual {p1}, Lorg/apache/tika/metadata/Property;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1, p2}, Lorg/apache/tika/metadata/Metadata;->set(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public set(Lorg/apache/tika/metadata/Property;Ljava/util/Date;)V
    .locals 4
    .param p1, "property"    # Lorg/apache/tika/metadata/Property;
    .param p2, "date"    # Ljava/util/Date;

    .prologue
    .line 485
    invoke-virtual {p1}, Lorg/apache/tika/metadata/Property;->getPrimaryProperty()Lorg/apache/tika/metadata/Property;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/tika/metadata/Property;->getPropertyType()Lorg/apache/tika/metadata/Property$PropertyType;

    move-result-object v1

    sget-object v2, Lorg/apache/tika/metadata/Property$PropertyType;->SIMPLE:Lorg/apache/tika/metadata/Property$PropertyType;

    if-eq v1, v2, :cond_0

    .line 486
    new-instance v1, Lorg/apache/tika/metadata/PropertyTypeException;

    sget-object v2, Lorg/apache/tika/metadata/Property$PropertyType;->SIMPLE:Lorg/apache/tika/metadata/Property$PropertyType;

    invoke-virtual {p1}, Lorg/apache/tika/metadata/Property;->getPrimaryProperty()Lorg/apache/tika/metadata/Property;

    move-result-object v3

    invoke-virtual {v3}, Lorg/apache/tika/metadata/Property;->getPropertyType()Lorg/apache/tika/metadata/Property$PropertyType;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/tika/metadata/PropertyTypeException;-><init>(Lorg/apache/tika/metadata/Property$PropertyType;Lorg/apache/tika/metadata/Property$PropertyType;)V

    throw v1

    .line 488
    :cond_0
    invoke-virtual {p1}, Lorg/apache/tika/metadata/Property;->getPrimaryProperty()Lorg/apache/tika/metadata/Property;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/tika/metadata/Property;->getValueType()Lorg/apache/tika/metadata/Property$ValueType;

    move-result-object v1

    sget-object v2, Lorg/apache/tika/metadata/Property$ValueType;->DATE:Lorg/apache/tika/metadata/Property$ValueType;

    if-eq v1, v2, :cond_1

    .line 489
    new-instance v1, Lorg/apache/tika/metadata/PropertyTypeException;

    sget-object v2, Lorg/apache/tika/metadata/Property$ValueType;->DATE:Lorg/apache/tika/metadata/Property$ValueType;

    invoke-virtual {p1}, Lorg/apache/tika/metadata/Property;->getPrimaryProperty()Lorg/apache/tika/metadata/Property;

    move-result-object v3

    invoke-virtual {v3}, Lorg/apache/tika/metadata/Property;->getValueType()Lorg/apache/tika/metadata/Property$ValueType;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/tika/metadata/PropertyTypeException;-><init>(Lorg/apache/tika/metadata/Property$ValueType;Lorg/apache/tika/metadata/Property$ValueType;)V

    throw v1

    .line 491
    :cond_1
    const/4 v0, 0x0

    .line 492
    .local v0, "dateString":Ljava/lang/String;
    if-eqz p2, :cond_2

    .line 493
    invoke-static {p2}, Lorg/apache/tika/metadata/Metadata;->formatDate(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    .line 495
    :cond_2
    invoke-virtual {p0, p1, v0}, Lorg/apache/tika/metadata/Metadata;->set(Lorg/apache/tika/metadata/Property;Ljava/lang/String;)V

    .line 496
    return-void
.end method

.method public set(Lorg/apache/tika/metadata/Property;[Ljava/lang/String;)V
    .locals 4
    .param p1, "property"    # Lorg/apache/tika/metadata/Property;
    .param p2, "values"    # [Ljava/lang/String;

    .prologue
    .line 427
    if-nez p1, :cond_0

    .line 428
    new-instance v1, Ljava/lang/NullPointerException;

    const-string/jumbo v2, "property must not be null"

    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 430
    :cond_0
    invoke-virtual {p1}, Lorg/apache/tika/metadata/Property;->getPropertyType()Lorg/apache/tika/metadata/Property$PropertyType;

    move-result-object v1

    sget-object v2, Lorg/apache/tika/metadata/Property$PropertyType;->COMPOSITE:Lorg/apache/tika/metadata/Property$PropertyType;

    if-ne v1, v2, :cond_3

    .line 431
    invoke-virtual {p1}, Lorg/apache/tika/metadata/Property;->getPrimaryProperty()Lorg/apache/tika/metadata/Property;

    move-result-object v1

    invoke-virtual {p0, v1, p2}, Lorg/apache/tika/metadata/Metadata;->set(Lorg/apache/tika/metadata/Property;[Ljava/lang/String;)V

    .line 432
    invoke-virtual {p1}, Lorg/apache/tika/metadata/Property;->getSecondaryExtractProperties()[Lorg/apache/tika/metadata/Property;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 433
    invoke-virtual {p1}, Lorg/apache/tika/metadata/Property;->getSecondaryExtractProperties()[Lorg/apache/tika/metadata/Property;

    move-result-object v2

    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-lt v1, v3, :cond_2

    .line 440
    :cond_1
    :goto_1
    return-void

    .line 433
    :cond_2
    aget-object v0, v2, v1

    .line 434
    .local v0, "secondaryExtractProperty":Lorg/apache/tika/metadata/Property;
    invoke-virtual {p0, v0, p2}, Lorg/apache/tika/metadata/Metadata;->set(Lorg/apache/tika/metadata/Property;[Ljava/lang/String;)V

    .line 433
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 438
    .end local v0    # "secondaryExtractProperty":Lorg/apache/tika/metadata/Property;
    :cond_3
    iget-object v1, p0, Lorg/apache/tika/metadata/Metadata;->metadata:Ljava/util/Map;

    invoke-virtual {p1}, Lorg/apache/tika/metadata/Property;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1
.end method

.method public setAll(Ljava/util/Properties;)V
    .locals 6
    .param p1, "properties"    # Ljava/util/Properties;

    .prologue
    .line 372
    invoke-virtual {p1}, Ljava/util/Properties;->propertyNames()Ljava/util/Enumeration;

    move-result-object v1

    .line 373
    .local v1, "names":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/lang/String;>;"
    :goto_0
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v2

    if-nez v2, :cond_0

    .line 377
    return-void

    .line 374
    :cond_0
    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 375
    .local v0, "name":Ljava/lang/String;
    iget-object v2, p0, Lorg/apache/tika/metadata/Metadata;->metadata:Ljava/util/Map;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    invoke-virtual {p1, v0}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-interface {v2, v0, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 514
    iget-object v0, p0, Lorg/apache/tika/metadata/Metadata;->metadata:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 7

    .prologue
    .line 551
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 552
    .local v0, "buf":Ljava/lang/StringBuffer;
    invoke-virtual {p0}, Lorg/apache/tika/metadata/Metadata;->names()[Ljava/lang/String;

    move-result-object v3

    .line 553
    .local v3, "names":[Ljava/lang/String;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v5, v3

    if-lt v1, v5, :cond_0

    .line 559
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    return-object v5

    .line 554
    :cond_0
    aget-object v5, v3, v1

    invoke-direct {p0, v5}, Lorg/apache/tika/metadata/Metadata;->_getValues(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 555
    .local v4, "values":[Ljava/lang/String;
    const/4 v2, 0x0

    .local v2, "j":I
    :goto_1
    array-length v5, v4

    if-lt v2, v5, :cond_1

    .line 553
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 556
    :cond_1
    aget-object v5, v3, v1

    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    const-string/jumbo v6, "="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    aget-object v6, v4, v2

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    const-string/jumbo v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 555
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method
