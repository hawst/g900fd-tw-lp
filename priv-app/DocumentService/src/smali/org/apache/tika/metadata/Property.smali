.class public final Lorg/apache/tika/metadata/Property;
.super Ljava/lang/Object;
.source "Property.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/tika/metadata/Property$PropertyType;,
        Lorg/apache/tika/metadata/Property$ValueType;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lorg/apache/tika/metadata/Property;",
        ">;"
    }
.end annotation


# static fields
.field private static final properties:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lorg/apache/tika/metadata/Property;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final choices:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final internal:Z

.field private final name:Ljava/lang/String;

.field private final primaryProperty:Lorg/apache/tika/metadata/Property;

.field private final propertyType:Lorg/apache/tika/metadata/Property$PropertyType;

.field private final secondaryExtractProperties:[Lorg/apache/tika/metadata/Property;

.field private final valueType:Lorg/apache/tika/metadata/Property$ValueType;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 59
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 58
    sput-object v0, Lorg/apache/tika/metadata/Property;->properties:Ljava/util/Map;

    .line 59
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ZLorg/apache/tika/metadata/Property$PropertyType;Lorg/apache/tika/metadata/Property$ValueType;)V
    .locals 6
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "internal"    # Z
    .param p3, "propertyType"    # Lorg/apache/tika/metadata/Property$PropertyType;
    .param p4, "valueType"    # Lorg/apache/tika/metadata/Property$ValueType;

    .prologue
    .line 125
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lorg/apache/tika/metadata/Property;-><init>(Ljava/lang/String;ZLorg/apache/tika/metadata/Property$PropertyType;Lorg/apache/tika/metadata/Property$ValueType;[Ljava/lang/String;)V

    .line 126
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ZLorg/apache/tika/metadata/Property$PropertyType;Lorg/apache/tika/metadata/Property$ValueType;[Ljava/lang/String;)V
    .locals 8
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "internal"    # Z
    .param p3, "propertyType"    # Lorg/apache/tika/metadata/Property$PropertyType;
    .param p4, "valueType"    # Lorg/apache/tika/metadata/Property$ValueType;
    .param p5, "choices"    # [Ljava/lang/String;

    .prologue
    const/4 v6, 0x0

    .line 109
    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v7, v6

    invoke-direct/range {v0 .. v7}, Lorg/apache/tika/metadata/Property;-><init>(Ljava/lang/String;ZLorg/apache/tika/metadata/Property$PropertyType;Lorg/apache/tika/metadata/Property$ValueType;[Ljava/lang/String;Lorg/apache/tika/metadata/Property;[Lorg/apache/tika/metadata/Property;)V

    .line 110
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ZLorg/apache/tika/metadata/Property$PropertyType;Lorg/apache/tika/metadata/Property$ValueType;[Ljava/lang/String;Lorg/apache/tika/metadata/Property;[Lorg/apache/tika/metadata/Property;)V
    .locals 3
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "internal"    # Z
    .param p3, "propertyType"    # Lorg/apache/tika/metadata/Property$PropertyType;
    .param p4, "valueType"    # Lorg/apache/tika/metadata/Property$ValueType;
    .param p5, "choices"    # [Ljava/lang/String;
    .param p6, "primaryProperty"    # Lorg/apache/tika/metadata/Property;
    .param p7, "secondaryExtractProperties"    # [Lorg/apache/tika/metadata/Property;

    .prologue
    const/4 v2, 0x0

    .line 78
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 81
    iput-object p1, p0, Lorg/apache/tika/metadata/Property;->name:Ljava/lang/String;

    .line 82
    iput-boolean p2, p0, Lorg/apache/tika/metadata/Property;->internal:Z

    .line 83
    iput-object p3, p0, Lorg/apache/tika/metadata/Property;->propertyType:Lorg/apache/tika/metadata/Property$PropertyType;

    .line 84
    iput-object p4, p0, Lorg/apache/tika/metadata/Property;->valueType:Lorg/apache/tika/metadata/Property$ValueType;

    .line 85
    if-eqz p5, :cond_0

    .line 87
    new-instance v1, Ljava/util/HashSet;

    invoke-virtual {p5}, [Ljava/lang/String;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 86
    invoke-static {v1}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/tika/metadata/Property;->choices:Ljava/util/Set;

    .line 92
    :goto_0
    if-eqz p6, :cond_1

    .line 93
    iput-object p6, p0, Lorg/apache/tika/metadata/Property;->primaryProperty:Lorg/apache/tika/metadata/Property;

    .line 94
    iput-object p7, p0, Lorg/apache/tika/metadata/Property;->secondaryExtractProperties:[Lorg/apache/tika/metadata/Property;

    .line 104
    :goto_1
    return-void

    .line 89
    :cond_0
    iput-object v2, p0, Lorg/apache/tika/metadata/Property;->choices:Ljava/util/Set;

    goto :goto_0

    .line 96
    :cond_1
    iput-object p0, p0, Lorg/apache/tika/metadata/Property;->primaryProperty:Lorg/apache/tika/metadata/Property;

    .line 97
    iput-object v2, p0, Lorg/apache/tika/metadata/Property;->secondaryExtractProperties:[Lorg/apache/tika/metadata/Property;

    .line 100
    sget-object v1, Lorg/apache/tika/metadata/Property;->properties:Ljava/util/Map;

    monitor-enter v1

    .line 101
    :try_start_0
    sget-object v0, Lorg/apache/tika/metadata/Property;->properties:Ljava/util/Map;

    invoke-interface {v0, p1, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 100
    monitor-exit v1

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private constructor <init>(Ljava/lang/String;ZLorg/apache/tika/metadata/Property$ValueType;)V
    .locals 6
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "internal"    # Z
    .param p3, "valueType"    # Lorg/apache/tika/metadata/Property$ValueType;

    .prologue
    .line 119
    sget-object v3, Lorg/apache/tika/metadata/Property$PropertyType;->SIMPLE:Lorg/apache/tika/metadata/Property$PropertyType;

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v4, p3

    invoke-direct/range {v0 .. v5}, Lorg/apache/tika/metadata/Property;-><init>(Ljava/lang/String;ZLorg/apache/tika/metadata/Property$PropertyType;Lorg/apache/tika/metadata/Property$ValueType;[Ljava/lang/String;)V

    .line 120
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ZLorg/apache/tika/metadata/Property$ValueType;[Ljava/lang/String;)V
    .locals 6
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "internal"    # Z
    .param p3, "valueType"    # Lorg/apache/tika/metadata/Property$ValueType;
    .param p4, "choices"    # [Ljava/lang/String;

    .prologue
    .line 115
    sget-object v3, Lorg/apache/tika/metadata/Property$PropertyType;->SIMPLE:Lorg/apache/tika/metadata/Property$PropertyType;

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lorg/apache/tika/metadata/Property;-><init>(Ljava/lang/String;ZLorg/apache/tika/metadata/Property$PropertyType;Lorg/apache/tika/metadata/Property$ValueType;[Ljava/lang/String;)V

    .line 116
    return-void
.end method

.method public static composite(Lorg/apache/tika/metadata/Property;[Lorg/apache/tika/metadata/Property;)Lorg/apache/tika/metadata/Property;
    .locals 9
    .param p0, "primaryProperty"    # Lorg/apache/tika/metadata/Property;
    .param p1, "secondaryExtractProperties"    # [Lorg/apache/tika/metadata/Property;

    .prologue
    .line 317
    if-nez p0, :cond_0

    .line 318
    new-instance v0, Ljava/lang/NullPointerException;

    const-string/jumbo v1, "primaryProperty must not be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 320
    :cond_0
    invoke-virtual {p0}, Lorg/apache/tika/metadata/Property;->getPropertyType()Lorg/apache/tika/metadata/Property$PropertyType;

    move-result-object v0

    sget-object v1, Lorg/apache/tika/metadata/Property$PropertyType;->COMPOSITE:Lorg/apache/tika/metadata/Property$PropertyType;

    if-ne v0, v1, :cond_1

    .line 321
    new-instance v0, Lorg/apache/tika/metadata/PropertyTypeException;

    invoke-virtual {p0}, Lorg/apache/tika/metadata/Property;->getPropertyType()Lorg/apache/tika/metadata/Property$PropertyType;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/tika/metadata/PropertyTypeException;-><init>(Lorg/apache/tika/metadata/Property$PropertyType;)V

    throw v0

    .line 323
    :cond_1
    if-eqz p1, :cond_2

    .line 324
    array-length v1, p1

    const/4 v0, 0x0

    :goto_0
    if-lt v0, v1, :cond_4

    .line 330
    :cond_2
    const/4 v5, 0x0

    .line 331
    .local v5, "choices":[Ljava/lang/String;
    invoke-virtual {p0}, Lorg/apache/tika/metadata/Property;->getChoices()Ljava/util/Set;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 332
    invoke-virtual {p0}, Lorg/apache/tika/metadata/Property;->getChoices()Ljava/util/Set;

    move-result-object v0

    .line 333
    invoke-virtual {p0}, Lorg/apache/tika/metadata/Property;->getChoices()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v1

    new-array v1, v1, [Ljava/lang/String;

    .line 332
    invoke-interface {v0, v1}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v5

    .end local v5    # "choices":[Ljava/lang/String;
    check-cast v5, [Ljava/lang/String;

    .line 335
    .restart local v5    # "choices":[Ljava/lang/String;
    :cond_3
    new-instance v0, Lorg/apache/tika/metadata/Property;

    invoke-virtual {p0}, Lorg/apache/tika/metadata/Property;->getName()Ljava/lang/String;

    move-result-object v1

    .line 336
    invoke-virtual {p0}, Lorg/apache/tika/metadata/Property;->isInternal()Z

    move-result v2

    sget-object v3, Lorg/apache/tika/metadata/Property$PropertyType;->COMPOSITE:Lorg/apache/tika/metadata/Property$PropertyType;

    .line 337
    sget-object v4, Lorg/apache/tika/metadata/Property$ValueType;->PROPERTY:Lorg/apache/tika/metadata/Property$ValueType;

    move-object v6, p0

    move-object v7, p1

    .line 335
    invoke-direct/range {v0 .. v7}, Lorg/apache/tika/metadata/Property;-><init>(Ljava/lang/String;ZLorg/apache/tika/metadata/Property$PropertyType;Lorg/apache/tika/metadata/Property$ValueType;[Ljava/lang/String;Lorg/apache/tika/metadata/Property;[Lorg/apache/tika/metadata/Property;)V

    return-object v0

    .line 324
    .end local v5    # "choices":[Ljava/lang/String;
    :cond_4
    aget-object v8, p1, v0

    .line 325
    .local v8, "secondaryExtractProperty":Lorg/apache/tika/metadata/Property;
    invoke-virtual {v8}, Lorg/apache/tika/metadata/Property;->getPropertyType()Lorg/apache/tika/metadata/Property$PropertyType;

    move-result-object v2

    sget-object v3, Lorg/apache/tika/metadata/Property$PropertyType;->COMPOSITE:Lorg/apache/tika/metadata/Property$PropertyType;

    if-ne v2, v3, :cond_5

    .line 326
    new-instance v0, Lorg/apache/tika/metadata/PropertyTypeException;

    invoke-virtual {v8}, Lorg/apache/tika/metadata/Property;->getPropertyType()Lorg/apache/tika/metadata/Property$PropertyType;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/tika/metadata/PropertyTypeException;-><init>(Lorg/apache/tika/metadata/Property$PropertyType;)V

    throw v0

    .line 324
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public static externalBoolean(Ljava/lang/String;)Lorg/apache/tika/metadata/Property;
    .locals 3
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 295
    new-instance v0, Lorg/apache/tika/metadata/Property;

    const/4 v1, 0x0

    sget-object v2, Lorg/apache/tika/metadata/Property$ValueType;->BOOLEAN:Lorg/apache/tika/metadata/Property$ValueType;

    invoke-direct {v0, p0, v1, v2}, Lorg/apache/tika/metadata/Property;-><init>(Ljava/lang/String;ZLorg/apache/tika/metadata/Property$ValueType;)V

    return-object v0
.end method

.method public static varargs externalClosedChoise(Ljava/lang/String;[Ljava/lang/String;)Lorg/apache/tika/metadata/Property;
    .locals 3
    .param p0, "name"    # Ljava/lang/String;
    .param p1, "choices"    # [Ljava/lang/String;

    .prologue
    .line 274
    new-instance v0, Lorg/apache/tika/metadata/Property;

    const/4 v1, 0x0

    sget-object v2, Lorg/apache/tika/metadata/Property$ValueType;->CLOSED_CHOICE:Lorg/apache/tika/metadata/Property$ValueType;

    invoke-direct {v0, p0, v1, v2, p1}, Lorg/apache/tika/metadata/Property;-><init>(Ljava/lang/String;ZLorg/apache/tika/metadata/Property$ValueType;[Ljava/lang/String;)V

    return-object v0
.end method

.method public static externalDate(Ljava/lang/String;)Lorg/apache/tika/metadata/Property;
    .locals 3
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 283
    new-instance v0, Lorg/apache/tika/metadata/Property;

    const/4 v1, 0x0

    sget-object v2, Lorg/apache/tika/metadata/Property$ValueType;->DATE:Lorg/apache/tika/metadata/Property$ValueType;

    invoke-direct {v0, p0, v1, v2}, Lorg/apache/tika/metadata/Property;-><init>(Ljava/lang/String;ZLorg/apache/tika/metadata/Property$ValueType;)V

    return-object v0
.end method

.method public static externalInteger(Ljava/lang/String;)Lorg/apache/tika/metadata/Property;
    .locals 3
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 291
    new-instance v0, Lorg/apache/tika/metadata/Property;

    const/4 v1, 0x0

    sget-object v2, Lorg/apache/tika/metadata/Property$ValueType;->INTEGER:Lorg/apache/tika/metadata/Property$ValueType;

    invoke-direct {v0, p0, v1, v2}, Lorg/apache/tika/metadata/Property;-><init>(Ljava/lang/String;ZLorg/apache/tika/metadata/Property$ValueType;)V

    return-object v0
.end method

.method public static varargs externalOpenChoise(Ljava/lang/String;[Ljava/lang/String;)Lorg/apache/tika/metadata/Property;
    .locals 3
    .param p0, "name"    # Ljava/lang/String;
    .param p1, "choices"    # [Ljava/lang/String;

    .prologue
    .line 279
    new-instance v0, Lorg/apache/tika/metadata/Property;

    const/4 v1, 0x0

    sget-object v2, Lorg/apache/tika/metadata/Property$ValueType;->OPEN_CHOICE:Lorg/apache/tika/metadata/Property$ValueType;

    invoke-direct {v0, p0, v1, v2, p1}, Lorg/apache/tika/metadata/Property;-><init>(Ljava/lang/String;ZLorg/apache/tika/metadata/Property$ValueType;[Ljava/lang/String;)V

    return-object v0
.end method

.method public static externalReal(Ljava/lang/String;)Lorg/apache/tika/metadata/Property;
    .locals 3
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 287
    new-instance v0, Lorg/apache/tika/metadata/Property;

    const/4 v1, 0x0

    sget-object v2, Lorg/apache/tika/metadata/Property$ValueType;->REAL:Lorg/apache/tika/metadata/Property$ValueType;

    invoke-direct {v0, p0, v1, v2}, Lorg/apache/tika/metadata/Property;-><init>(Ljava/lang/String;ZLorg/apache/tika/metadata/Property$ValueType;)V

    return-object v0
.end method

.method public static externalText(Ljava/lang/String;)Lorg/apache/tika/metadata/Property;
    .locals 3
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 299
    new-instance v0, Lorg/apache/tika/metadata/Property;

    const/4 v1, 0x0

    sget-object v2, Lorg/apache/tika/metadata/Property$ValueType;->TEXT:Lorg/apache/tika/metadata/Property$ValueType;

    invoke-direct {v0, p0, v1, v2}, Lorg/apache/tika/metadata/Property;-><init>(Ljava/lang/String;ZLorg/apache/tika/metadata/Property$ValueType;)V

    return-object v0
.end method

.method public static externalTextBag(Ljava/lang/String;)Lorg/apache/tika/metadata/Property;
    .locals 4
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 303
    new-instance v0, Lorg/apache/tika/metadata/Property;

    const/4 v1, 0x0

    sget-object v2, Lorg/apache/tika/metadata/Property$PropertyType;->BAG:Lorg/apache/tika/metadata/Property$PropertyType;

    sget-object v3, Lorg/apache/tika/metadata/Property$ValueType;->TEXT:Lorg/apache/tika/metadata/Property$ValueType;

    invoke-direct {v0, p0, v1, v2, v3}, Lorg/apache/tika/metadata/Property;-><init>(Ljava/lang/String;ZLorg/apache/tika/metadata/Property$PropertyType;Lorg/apache/tika/metadata/Property$ValueType;)V

    return-object v0
.end method

.method public static get(Ljava/lang/String;)Lorg/apache/tika/metadata/Property;
    .locals 1
    .param p0, "key"    # Ljava/lang/String;

    .prologue
    .line 174
    sget-object v0, Lorg/apache/tika/metadata/Property;->properties:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/tika/metadata/Property;

    return-object v0
.end method

.method public static getProperties(Ljava/lang/String;)Ljava/util/SortedSet;
    .locals 6
    .param p0, "prefix"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/SortedSet",
            "<",
            "Lorg/apache/tika/metadata/Property;",
            ">;"
        }
    .end annotation

    .prologue
    .line 215
    new-instance v2, Ljava/util/TreeSet;

    invoke-direct {v2}, Ljava/util/TreeSet;-><init>()V

    .line 216
    .local v2, "set":Ljava/util/SortedSet;, "Ljava/util/SortedSet<Lorg/apache/tika/metadata/Property;>;"
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 217
    .local v1, "p":Ljava/lang/String;
    sget-object v4, Lorg/apache/tika/metadata/Property;->properties:Ljava/util/Map;

    monitor-enter v4

    .line 218
    :try_start_0
    sget-object v3, Lorg/apache/tika/metadata/Property;->properties:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_1

    .line 217
    monitor-exit v4

    .line 224
    return-object v2

    .line 218
    :cond_1
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 219
    .local v0, "name":Ljava/lang/String;
    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 220
    sget-object v3, Lorg/apache/tika/metadata/Property;->properties:Ljava/util/Map;

    invoke-interface {v3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/tika/metadata/Property;

    invoke-interface {v2, v3}, Ljava/util/SortedSet;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 217
    .end local v0    # "name":Ljava/lang/String;
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3
.end method

.method public static getPropertyType(Ljava/lang/String;)Lorg/apache/tika/metadata/Property$PropertyType;
    .locals 3
    .param p0, "key"    # Ljava/lang/String;

    .prologue
    .line 160
    const/4 v1, 0x0

    .line 161
    .local v1, "type":Lorg/apache/tika/metadata/Property$PropertyType;
    sget-object v2, Lorg/apache/tika/metadata/Property;->properties:Ljava/util/Map;

    invoke-interface {v2, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/tika/metadata/Property;

    .line 162
    .local v0, "prop":Lorg/apache/tika/metadata/Property;
    if-eqz v0, :cond_0

    .line 163
    invoke-virtual {v0}, Lorg/apache/tika/metadata/Property;->getPropertyType()Lorg/apache/tika/metadata/Property$PropertyType;

    move-result-object v1

    .line 165
    :cond_0
    return-object v1
.end method

.method public static internalBoolean(Ljava/lang/String;)Lorg/apache/tika/metadata/Property;
    .locals 3
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 228
    new-instance v0, Lorg/apache/tika/metadata/Property;

    const/4 v1, 0x1

    sget-object v2, Lorg/apache/tika/metadata/Property$ValueType;->BOOLEAN:Lorg/apache/tika/metadata/Property$ValueType;

    invoke-direct {v0, p0, v1, v2}, Lorg/apache/tika/metadata/Property;-><init>(Ljava/lang/String;ZLorg/apache/tika/metadata/Property$ValueType;)V

    return-object v0
.end method

.method public static varargs internalClosedChoise(Ljava/lang/String;[Ljava/lang/String;)Lorg/apache/tika/metadata/Property;
    .locals 3
    .param p0, "name"    # Ljava/lang/String;
    .param p1, "choices"    # [Ljava/lang/String;

    .prologue
    .line 233
    new-instance v0, Lorg/apache/tika/metadata/Property;

    const/4 v1, 0x1

    sget-object v2, Lorg/apache/tika/metadata/Property$ValueType;->CLOSED_CHOICE:Lorg/apache/tika/metadata/Property$ValueType;

    invoke-direct {v0, p0, v1, v2, p1}, Lorg/apache/tika/metadata/Property;-><init>(Ljava/lang/String;ZLorg/apache/tika/metadata/Property$ValueType;[Ljava/lang/String;)V

    return-object v0
.end method

.method public static internalDate(Ljava/lang/String;)Lorg/apache/tika/metadata/Property;
    .locals 3
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 237
    new-instance v0, Lorg/apache/tika/metadata/Property;

    const/4 v1, 0x1

    sget-object v2, Lorg/apache/tika/metadata/Property$ValueType;->DATE:Lorg/apache/tika/metadata/Property$ValueType;

    invoke-direct {v0, p0, v1, v2}, Lorg/apache/tika/metadata/Property;-><init>(Ljava/lang/String;ZLorg/apache/tika/metadata/Property$ValueType;)V

    return-object v0
.end method

.method public static internalInteger(Ljava/lang/String;)Lorg/apache/tika/metadata/Property;
    .locals 3
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 241
    new-instance v0, Lorg/apache/tika/metadata/Property;

    const/4 v1, 0x1

    sget-object v2, Lorg/apache/tika/metadata/Property$ValueType;->INTEGER:Lorg/apache/tika/metadata/Property$ValueType;

    invoke-direct {v0, p0, v1, v2}, Lorg/apache/tika/metadata/Property;-><init>(Ljava/lang/String;ZLorg/apache/tika/metadata/Property$ValueType;)V

    return-object v0
.end method

.method public static internalIntegerSequence(Ljava/lang/String;)Lorg/apache/tika/metadata/Property;
    .locals 4
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 245
    new-instance v0, Lorg/apache/tika/metadata/Property;

    const/4 v1, 0x1

    sget-object v2, Lorg/apache/tika/metadata/Property$PropertyType;->SEQ:Lorg/apache/tika/metadata/Property$PropertyType;

    sget-object v3, Lorg/apache/tika/metadata/Property$ValueType;->INTEGER:Lorg/apache/tika/metadata/Property$ValueType;

    invoke-direct {v0, p0, v1, v2, v3}, Lorg/apache/tika/metadata/Property;-><init>(Ljava/lang/String;ZLorg/apache/tika/metadata/Property$PropertyType;Lorg/apache/tika/metadata/Property$ValueType;)V

    return-object v0
.end method

.method public static varargs internalOpenChoise(Ljava/lang/String;[Ljava/lang/String;)Lorg/apache/tika/metadata/Property;
    .locals 3
    .param p0, "name"    # Ljava/lang/String;
    .param p1, "choices"    # [Ljava/lang/String;

    .prologue
    .line 254
    new-instance v0, Lorg/apache/tika/metadata/Property;

    const/4 v1, 0x1

    sget-object v2, Lorg/apache/tika/metadata/Property$ValueType;->OPEN_CHOICE:Lorg/apache/tika/metadata/Property$ValueType;

    invoke-direct {v0, p0, v1, v2, p1}, Lorg/apache/tika/metadata/Property;-><init>(Ljava/lang/String;ZLorg/apache/tika/metadata/Property$ValueType;[Ljava/lang/String;)V

    return-object v0
.end method

.method public static internalRational(Ljava/lang/String;)Lorg/apache/tika/metadata/Property;
    .locals 3
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 249
    new-instance v0, Lorg/apache/tika/metadata/Property;

    const/4 v1, 0x1

    sget-object v2, Lorg/apache/tika/metadata/Property$ValueType;->RATIONAL:Lorg/apache/tika/metadata/Property$ValueType;

    invoke-direct {v0, p0, v1, v2}, Lorg/apache/tika/metadata/Property;-><init>(Ljava/lang/String;ZLorg/apache/tika/metadata/Property$ValueType;)V

    return-object v0
.end method

.method public static internalReal(Ljava/lang/String;)Lorg/apache/tika/metadata/Property;
    .locals 3
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 257
    new-instance v0, Lorg/apache/tika/metadata/Property;

    const/4 v1, 0x1

    sget-object v2, Lorg/apache/tika/metadata/Property$ValueType;->REAL:Lorg/apache/tika/metadata/Property$ValueType;

    invoke-direct {v0, p0, v1, v2}, Lorg/apache/tika/metadata/Property;-><init>(Ljava/lang/String;ZLorg/apache/tika/metadata/Property$ValueType;)V

    return-object v0
.end method

.method public static internalText(Ljava/lang/String;)Lorg/apache/tika/metadata/Property;
    .locals 3
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 261
    new-instance v0, Lorg/apache/tika/metadata/Property;

    const/4 v1, 0x1

    sget-object v2, Lorg/apache/tika/metadata/Property$ValueType;->TEXT:Lorg/apache/tika/metadata/Property$ValueType;

    invoke-direct {v0, p0, v1, v2}, Lorg/apache/tika/metadata/Property;-><init>(Ljava/lang/String;ZLorg/apache/tika/metadata/Property$ValueType;)V

    return-object v0
.end method

.method public static internalTextBag(Ljava/lang/String;)Lorg/apache/tika/metadata/Property;
    .locals 4
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 265
    new-instance v0, Lorg/apache/tika/metadata/Property;

    const/4 v1, 0x1

    sget-object v2, Lorg/apache/tika/metadata/Property$PropertyType;->BAG:Lorg/apache/tika/metadata/Property$PropertyType;

    sget-object v3, Lorg/apache/tika/metadata/Property$ValueType;->TEXT:Lorg/apache/tika/metadata/Property$ValueType;

    invoke-direct {v0, p0, v1, v2, v3}, Lorg/apache/tika/metadata/Property;-><init>(Ljava/lang/String;ZLorg/apache/tika/metadata/Property$PropertyType;Lorg/apache/tika/metadata/Property$ValueType;)V

    return-object v0
.end method

.method public static internalURI(Ljava/lang/String;)Lorg/apache/tika/metadata/Property;
    .locals 3
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 269
    new-instance v0, Lorg/apache/tika/metadata/Property;

    const/4 v1, 0x1

    sget-object v2, Lorg/apache/tika/metadata/Property$ValueType;->URI:Lorg/apache/tika/metadata/Property$ValueType;

    invoke-direct {v0, p0, v1, v2}, Lorg/apache/tika/metadata/Property;-><init>(Ljava/lang/String;ZLorg/apache/tika/metadata/Property$ValueType;)V

    return-object v0
.end method


# virtual methods
.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 1
    check-cast p1, Lorg/apache/tika/metadata/Property;

    invoke-virtual {p0, p1}, Lorg/apache/tika/metadata/Property;->compareTo(Lorg/apache/tika/metadata/Property;)I

    move-result v0

    return v0
.end method

.method public compareTo(Lorg/apache/tika/metadata/Property;)I
    .locals 2
    .param p1, "o"    # Lorg/apache/tika/metadata/Property;

    .prologue
    .line 344
    iget-object v0, p0, Lorg/apache/tika/metadata/Property;->name:Ljava/lang/String;

    iget-object v1, p1, Lorg/apache/tika/metadata/Property;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 350
    instance-of v0, p1, Lorg/apache/tika/metadata/Property;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/tika/metadata/Property;->name:Ljava/lang/String;

    check-cast p1, Lorg/apache/tika/metadata/Property;

    .end local p1    # "o":Ljava/lang/Object;
    iget-object v1, p1, Lorg/apache/tika/metadata/Property;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getChoices()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 193
    iget-object v0, p0, Lorg/apache/tika/metadata/Property;->choices:Ljava/util/Set;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lorg/apache/tika/metadata/Property;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getPrimaryProperty()Lorg/apache/tika/metadata/Property;
    .locals 1

    .prologue
    .line 202
    iget-object v0, p0, Lorg/apache/tika/metadata/Property;->primaryProperty:Lorg/apache/tika/metadata/Property;

    return-object v0
.end method

.method public getPropertyType()Lorg/apache/tika/metadata/Property$PropertyType;
    .locals 1

    .prologue
    .line 178
    iget-object v0, p0, Lorg/apache/tika/metadata/Property;->propertyType:Lorg/apache/tika/metadata/Property$PropertyType;

    return-object v0
.end method

.method public getSecondaryExtractProperties()[Lorg/apache/tika/metadata/Property;
    .locals 1

    .prologue
    .line 211
    iget-object v0, p0, Lorg/apache/tika/metadata/Property;->secondaryExtractProperties:[Lorg/apache/tika/metadata/Property;

    return-object v0
.end method

.method public getValueType()Lorg/apache/tika/metadata/Property$ValueType;
    .locals 1

    .prologue
    .line 182
    iget-object v0, p0, Lorg/apache/tika/metadata/Property;->valueType:Lorg/apache/tika/metadata/Property$ValueType;

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 354
    iget-object v0, p0, Lorg/apache/tika/metadata/Property;->name:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public isExternal()Z
    .locals 1

    .prologue
    .line 137
    iget-boolean v0, p0, Lorg/apache/tika/metadata/Property;->internal:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isInternal()Z
    .locals 1

    .prologue
    .line 133
    iget-boolean v0, p0, Lorg/apache/tika/metadata/Property;->internal:Z

    return v0
.end method

.method public isMultiValuePermitted()Z
    .locals 2

    .prologue
    .line 144
    iget-object v0, p0, Lorg/apache/tika/metadata/Property;->propertyType:Lorg/apache/tika/metadata/Property$PropertyType;

    sget-object v1, Lorg/apache/tika/metadata/Property$PropertyType;->BAG:Lorg/apache/tika/metadata/Property$PropertyType;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lorg/apache/tika/metadata/Property;->propertyType:Lorg/apache/tika/metadata/Property$PropertyType;

    sget-object v1, Lorg/apache/tika/metadata/Property$PropertyType;->SEQ:Lorg/apache/tika/metadata/Property$PropertyType;

    if-eq v0, v1, :cond_0

    .line 145
    iget-object v0, p0, Lorg/apache/tika/metadata/Property;->propertyType:Lorg/apache/tika/metadata/Property$PropertyType;

    sget-object v1, Lorg/apache/tika/metadata/Property$PropertyType;->ALT:Lorg/apache/tika/metadata/Property$PropertyType;

    if-ne v0, v1, :cond_1

    .line 146
    :cond_0
    const/4 v0, 0x1

    .line 151
    :goto_0
    return v0

    .line 147
    :cond_1
    iget-object v0, p0, Lorg/apache/tika/metadata/Property;->propertyType:Lorg/apache/tika/metadata/Property$PropertyType;

    sget-object v1, Lorg/apache/tika/metadata/Property$PropertyType;->COMPOSITE:Lorg/apache/tika/metadata/Property$PropertyType;

    if-ne v0, v1, :cond_2

    .line 149
    iget-object v0, p0, Lorg/apache/tika/metadata/Property;->primaryProperty:Lorg/apache/tika/metadata/Property;

    invoke-virtual {v0}, Lorg/apache/tika/metadata/Property;->isMultiValuePermitted()Z

    move-result v0

    goto :goto_0

    .line 151
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method
