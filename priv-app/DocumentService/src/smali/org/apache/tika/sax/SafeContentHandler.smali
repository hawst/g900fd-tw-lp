.class public Lorg/apache/tika/sax/SafeContentHandler;
.super Lorg/apache/tika/sax/ContentHandlerDecorator;
.source "SafeContentHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/tika/sax/SafeContentHandler$Output;,
        Lorg/apache/tika/sax/SafeContentHandler$StringOutput;
    }
.end annotation


# static fields
.field private static final REPLACEMENT:[C


# instance fields
.field private final charactersOutput:Lorg/apache/tika/sax/SafeContentHandler$Output;

.field private final ignorableWhitespaceOutput:Lorg/apache/tika/sax/SafeContentHandler$Output;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 51
    const/4 v0, 0x1

    new-array v0, v0, [C

    const/4 v1, 0x0

    const v2, 0xfffd

    aput-char v2, v0, v1

    sput-object v0, Lorg/apache/tika/sax/SafeContentHandler;->REPLACEMENT:[C

    return-void
.end method

.method public constructor <init>(Lorg/xml/sax/ContentHandler;)V
    .locals 1
    .param p1, "handler"    # Lorg/xml/sax/ContentHandler;

    .prologue
    .line 99
    invoke-direct {p0, p1}, Lorg/apache/tika/sax/ContentHandlerDecorator;-><init>(Lorg/xml/sax/ContentHandler;)V

    .line 79
    new-instance v0, Lorg/apache/tika/sax/SafeContentHandler$1;

    invoke-direct {v0, p0}, Lorg/apache/tika/sax/SafeContentHandler$1;-><init>(Lorg/apache/tika/sax/SafeContentHandler;)V

    iput-object v0, p0, Lorg/apache/tika/sax/SafeContentHandler;->charactersOutput:Lorg/apache/tika/sax/SafeContentHandler$Output;

    .line 91
    new-instance v0, Lorg/apache/tika/sax/SafeContentHandler$2;

    invoke-direct {v0, p0}, Lorg/apache/tika/sax/SafeContentHandler$2;-><init>(Lorg/apache/tika/sax/SafeContentHandler;)V

    iput-object v0, p0, Lorg/apache/tika/sax/SafeContentHandler;->ignorableWhitespaceOutput:Lorg/apache/tika/sax/SafeContentHandler$Output;

    .line 100
    return-void
.end method

.method static synthetic access$0(Lorg/apache/tika/sax/SafeContentHandler;[CII)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .prologue
    .line 1
    invoke-super {p0, p1, p2, p3}, Lorg/apache/tika/sax/ContentHandlerDecorator;->characters([CII)V

    return-void
.end method

.method static synthetic access$1(Lorg/apache/tika/sax/SafeContentHandler;[CII)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .prologue
    .line 1
    invoke-super {p0, p1, p2, p3}, Lorg/apache/tika/sax/ContentHandlerDecorator;->ignorableWhitespace([CII)V

    return-void
.end method

.method private filter([CIILorg/apache/tika/sax/SafeContentHandler$Output;)V
    .locals 5
    .param p1, "ch"    # [C
    .param p2, "start"    # I
    .param p3, "length"    # I
    .param p4, "output"    # Lorg/apache/tika/sax/SafeContentHandler$Output;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .prologue
    .line 116
    add-int v1, p2, p3

    .line 118
    .local v1, "end":I
    move v2, p2

    .line 119
    .local v2, "i":I
    :goto_0
    if-lt v2, v1, :cond_0

    .line 140
    sub-int v4, v1, p2

    invoke-interface {p4, p1, p2, v4}, Lorg/apache/tika/sax/SafeContentHandler$Output;->write([CII)V

    .line 141
    return-void

    .line 120
    :cond_0
    invoke-static {p1, v2, v1}, Ljava/lang/Character;->codePointAt([CII)I

    move-result v0

    .line 121
    .local v0, "c":I
    invoke-static {v0}, Ljava/lang/Character;->charCount(I)I

    move-result v4

    add-int v3, v2, v4

    .line 123
    .local v3, "j":I
    invoke-virtual {p0, v0}, Lorg/apache/tika/sax/SafeContentHandler;->isInvalid(I)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 125
    if-le v2, p2, :cond_1

    .line 126
    sub-int v4, v2, p2

    invoke-interface {p4, p1, p2, v4}, Lorg/apache/tika/sax/SafeContentHandler$Output;->write([CII)V

    .line 130
    :cond_1
    invoke-virtual {p0, p4}, Lorg/apache/tika/sax/SafeContentHandler;->writeReplacement(Lorg/apache/tika/sax/SafeContentHandler$Output;)V

    .line 133
    move p2, v3

    .line 136
    :cond_2
    move v2, v3

    goto :goto_0
.end method

.method private isInvalid(Ljava/lang/String;)Z
    .locals 4
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 151
    invoke-virtual {p1}, Ljava/lang/String;->toCharArray()[C

    move-result-object v1

    .line 153
    .local v1, "ch":[C
    const/4 v2, 0x0

    .line 154
    .local v2, "i":I
    :goto_0
    array-length v3, v1

    if-lt v2, v3, :cond_0

    .line 162
    const/4 v3, 0x0

    :goto_1
    return v3

    .line 155
    :cond_0
    invoke-static {v1, v2}, Ljava/lang/Character;->codePointAt([CI)I

    move-result v0

    .line 156
    .local v0, "c":I
    invoke-virtual {p0, v0}, Lorg/apache/tika/sax/SafeContentHandler;->isInvalid(I)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 157
    const/4 v3, 0x1

    goto :goto_1

    .line 159
    :cond_1
    invoke-static {v0}, Ljava/lang/Character;->charCount(I)I

    move-result v3

    add-int/2addr v2, v3

    goto :goto_0
.end method


# virtual methods
.method public characters([CII)V
    .locals 1
    .param p1, "ch"    # [C
    .param p2, "start"    # I
    .param p3, "length"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .prologue
    .line 287
    iget-object v0, p0, Lorg/apache/tika/sax/SafeContentHandler;->charactersOutput:Lorg/apache/tika/sax/SafeContentHandler$Output;

    invoke-direct {p0, p1, p2, p3, v0}, Lorg/apache/tika/sax/SafeContentHandler;->filter([CIILorg/apache/tika/sax/SafeContentHandler$Output;)V

    .line 288
    return-void
.end method

.method public endDocument()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .prologue
    .line 281
    invoke-super {p0}, Lorg/apache/tika/sax/ContentHandlerDecorator;->endDocument()V

    .line 282
    return-void
.end method

.method public endElement(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "uri"    # Ljava/lang/String;
    .param p2, "localName"    # Ljava/lang/String;
    .param p3, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .prologue
    .line 273
    invoke-super {p0, p1, p2, p3}, Lorg/apache/tika/sax/ContentHandlerDecorator;->endElement(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 274
    return-void
.end method

.method public ignorableWhitespace([CII)V
    .locals 1
    .param p1, "ch"    # [C
    .param p2, "start"    # I
    .param p3, "length"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .prologue
    .line 293
    iget-object v0, p0, Lorg/apache/tika/sax/SafeContentHandler;->ignorableWhitespaceOutput:Lorg/apache/tika/sax/SafeContentHandler$Output;

    invoke-direct {p0, p1, p2, p3, v0}, Lorg/apache/tika/sax/SafeContentHandler;->filter([CIILorg/apache/tika/sax/SafeContentHandler$Output;)V

    .line 294
    return-void
.end method

.method protected isInvalid(I)Z
    .locals 3
    .param p1, "ch"    # I

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 179
    const/16 v2, 0x20

    if-ge p1, v2, :cond_2

    .line 180
    const/16 v2, 0x9

    if-eq p1, v2, :cond_1

    const/16 v2, 0xa

    if-eq p1, v2, :cond_1

    const/16 v2, 0xd

    if-eq p1, v2, :cond_1

    .line 186
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    .line 180
    goto :goto_0

    .line 181
    :cond_2
    const v2, 0xe000

    if-ge p1, v2, :cond_3

    .line 182
    const v2, 0xd7ff

    if-gt p1, v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 183
    :cond_3
    const/high16 v2, 0x10000

    if-ge p1, v2, :cond_4

    .line 184
    const v2, 0xfffd

    if-gt p1, v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 186
    :cond_4
    const v2, 0x10ffff

    if-gt p1, v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public startElement(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 9
    .param p1, "uri"    # Ljava/lang/String;
    .param p2, "localName"    # Ljava/lang/String;
    .param p3, "name"    # Ljava/lang/String;
    .param p4, "atts"    # Lorg/xml/sax/Attributes;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .prologue
    .line 244
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_0
    invoke-interface {p4}, Lorg/xml/sax/Attributes;->getLength()I

    move-result v1

    if-lt v7, v1, :cond_0

    .line 264
    :goto_1
    invoke-super {p0, p1, p2, p3, p4}, Lorg/apache/tika/sax/ContentHandlerDecorator;->startElement(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 265
    return-void

    .line 245
    :cond_0
    invoke-interface {p4, v7}, Lorg/xml/sax/Attributes;->getValue(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lorg/apache/tika/sax/SafeContentHandler;->isInvalid(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 247
    new-instance v0, Lorg/xml/sax/helpers/AttributesImpl;

    invoke-direct {v0}, Lorg/xml/sax/helpers/AttributesImpl;-><init>()V

    .line 248
    .local v0, "filtered":Lorg/xml/sax/helpers/AttributesImpl;
    const/4 v8, 0x0

    .local v8, "j":I
    :goto_2
    invoke-interface {p4}, Lorg/xml/sax/Attributes;->getLength()I

    move-result v1

    if-lt v8, v1, :cond_1

    .line 260
    move-object p4, v0

    .line 261
    goto :goto_1

    .line 249
    :cond_1
    invoke-interface {p4, v8}, Lorg/xml/sax/Attributes;->getValue(I)Ljava/lang/String;

    move-result-object v5

    .line 250
    .local v5, "value":Ljava/lang/String;
    if-lt v8, v7, :cond_2

    invoke-direct {p0, v5}, Lorg/apache/tika/sax/SafeContentHandler;->isInvalid(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 252
    new-instance v6, Lorg/apache/tika/sax/SafeContentHandler$StringOutput;

    const/4 v1, 0x0

    invoke-direct {v6, v1}, Lorg/apache/tika/sax/SafeContentHandler$StringOutput;-><init>(Lorg/apache/tika/sax/SafeContentHandler$StringOutput;)V

    .line 253
    .local v6, "buffer":Lorg/apache/tika/sax/SafeContentHandler$Output;
    invoke-virtual {v5}, Ljava/lang/String;->toCharArray()[C

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v3

    invoke-direct {p0, v1, v2, v3, v6}, Lorg/apache/tika/sax/SafeContentHandler;->filter([CIILorg/apache/tika/sax/SafeContentHandler$Output;)V

    .line 254
    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    .line 257
    .end local v6    # "buffer":Lorg/apache/tika/sax/SafeContentHandler$Output;
    :cond_2
    invoke-interface {p4, v8}, Lorg/xml/sax/Attributes;->getURI(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p4, v8}, Lorg/xml/sax/Attributes;->getLocalName(I)Ljava/lang/String;

    move-result-object v2

    .line 258
    invoke-interface {p4, v8}, Lorg/xml/sax/Attributes;->getQName(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {p4, v8}, Lorg/xml/sax/Attributes;->getType(I)Ljava/lang/String;

    move-result-object v4

    .line 256
    invoke-virtual/range {v0 .. v5}, Lorg/xml/sax/helpers/AttributesImpl;->addAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 248
    add-int/lit8 v8, v8, 0x1

    goto :goto_2

    .line 244
    .end local v0    # "filtered":Lorg/xml/sax/helpers/AttributesImpl;
    .end local v5    # "value":Ljava/lang/String;
    .end local v8    # "j":I
    :cond_3
    add-int/lit8 v7, v7, 0x1

    goto :goto_0
.end method

.method protected writeReplacement(Lorg/apache/tika/sax/SafeContentHandler$Output;)V
    .locals 3
    .param p1, "output"    # Lorg/apache/tika/sax/SafeContentHandler$Output;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .prologue
    .line 198
    sget-object v0, Lorg/apache/tika/sax/SafeContentHandler;->REPLACEMENT:[C

    const/4 v1, 0x0

    sget-object v2, Lorg/apache/tika/sax/SafeContentHandler;->REPLACEMENT:[C

    array-length v2, v2

    invoke-interface {p1, v0, v1, v2}, Lorg/apache/tika/sax/SafeContentHandler$Output;->write([CII)V

    .line 199
    return-void
.end method
