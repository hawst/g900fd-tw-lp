.class public Lorg/apache/tika/sax/ToTextContentHandler;
.super Lorg/xml/sax/helpers/DefaultHandler;
.source "ToTextContentHandler.java"


# static fields
.field private static final MAX_CHAR_COUNT:I = 0x4c4b40


# instance fields
.field private final indexWriter:Lcom/samsung/index/ITextContentObs;

.field private final isLuceneSearch:Z

.field private final luceneBuffer:Ljava/lang/StringBuffer;

.field private final writer:Ljava/io/Writer;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 124
    new-instance v0, Ljava/io/StringWriter;

    invoke-direct {v0}, Ljava/io/StringWriter;-><init>()V

    invoke-direct {p0, v0}, Lorg/apache/tika/sax/ToTextContentHandler;-><init>(Ljava/io/Writer;)V

    .line 125
    return-void
.end method

.method public constructor <init>(Ljava/io/OutputStream;)V
    .locals 1
    .param p1, "stream"    # Ljava/io/OutputStream;

    .prologue
    .line 102
    new-instance v0, Ljava/io/OutputStreamWriter;

    invoke-direct {v0, p1}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;)V

    invoke-direct {p0, v0}, Lorg/apache/tika/sax/ToTextContentHandler;-><init>(Ljava/io/Writer;)V

    .line 103
    return-void
.end method

.method public constructor <init>(Ljava/io/OutputStream;Ljava/lang/String;)V
    .locals 1
    .param p1, "stream"    # Ljava/io/OutputStream;
    .param p2, "encoding"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/UnsupportedEncodingException;
        }
    .end annotation

    .prologue
    .line 115
    new-instance v0, Ljava/io/OutputStreamWriter;

    invoke-direct {v0, p1, p2}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lorg/apache/tika/sax/ToTextContentHandler;-><init>(Ljava/io/Writer;)V

    .line 116
    return-void
.end method

.method public constructor <init>(Ljava/io/Writer;)V
    .locals 2
    .param p1, "writer"    # Ljava/io/Writer;

    .prologue
    const/4 v1, 0x0

    .line 73
    invoke-direct {p0}, Lorg/xml/sax/helpers/DefaultHandler;-><init>()V

    .line 74
    iput-object p1, p0, Lorg/apache/tika/sax/ToTextContentHandler;->writer:Ljava/io/Writer;

    .line 75
    iput-object v1, p0, Lorg/apache/tika/sax/ToTextContentHandler;->luceneBuffer:Ljava/lang/StringBuffer;

    .line 76
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/tika/sax/ToTextContentHandler;->isLuceneSearch:Z

    .line 77
    iput-object v1, p0, Lorg/apache/tika/sax/ToTextContentHandler;->indexWriter:Lcom/samsung/index/ITextContentObs;

    .line 78
    return-void
.end method

.method public constructor <init>(Ljava/lang/StringBuffer;Lcom/samsung/index/ITextContentObs;)V
    .locals 1
    .param p1, "luceneBuffer"    # Ljava/lang/StringBuffer;
    .param p2, "indexWriter"    # Lcom/samsung/index/ITextContentObs;

    .prologue
    .line 87
    invoke-direct {p0}, Lorg/xml/sax/helpers/DefaultHandler;-><init>()V

    .line 88
    iput-object p1, p0, Lorg/apache/tika/sax/ToTextContentHandler;->luceneBuffer:Ljava/lang/StringBuffer;

    .line 89
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/tika/sax/ToTextContentHandler;->writer:Ljava/io/Writer;

    .line 90
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/tika/sax/ToTextContentHandler;->isLuceneSearch:Z

    .line 91
    iput-object p2, p0, Lorg/apache/tika/sax/ToTextContentHandler;->indexWriter:Lcom/samsung/index/ITextContentObs;

    .line 92
    return-void
.end method


# virtual methods
.method public characters([CII)V
    .locals 4
    .param p1, "ch"    # [C
    .param p2, "start"    # I
    .param p3, "length"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .prologue
    .line 134
    iget-boolean v1, p0, Lorg/apache/tika/sax/ToTextContentHandler;->isLuceneSearch:Z

    if-nez v1, :cond_1

    .line 136
    :try_start_0
    iget-object v1, p0, Lorg/apache/tika/sax/ToTextContentHandler;->writer:Ljava/io/Writer;

    invoke-virtual {v1, p1, p2, p3}, Ljava/io/Writer;->write([CII)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 149
    :cond_0
    :goto_0
    return-void

    .line 137
    :catch_0
    move-exception v0

    .line 138
    .local v0, "e":Ljava/io/IOException;
    new-instance v1, Lorg/xml/sax/SAXException;

    .line 139
    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Error writing: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v3, Ljava/lang/String;

    invoke-direct {v3, p1, p2, p3}, Ljava/lang/String;-><init>([CII)V

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 138
    invoke-direct {v1, v2, v0}, Lorg/xml/sax/SAXException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v1

    .line 142
    .end local v0    # "e":Ljava/io/IOException;
    :cond_1
    iget-object v1, p0, Lorg/apache/tika/sax/ToTextContentHandler;->luceneBuffer:Ljava/lang/StringBuffer;

    invoke-virtual {v1, p1, p2, p3}, Ljava/lang/StringBuffer;->append([CII)Ljava/lang/StringBuffer;

    .line 143
    iget-object v1, p0, Lorg/apache/tika/sax/ToTextContentHandler;->luceneBuffer:Ljava/lang/StringBuffer;

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->length()I

    move-result v1

    const v2, 0x4c4b40

    if-lt v1, v2, :cond_0

    .line 144
    iget-object v1, p0, Lorg/apache/tika/sax/ToTextContentHandler;->indexWriter:Lcom/samsung/index/ITextContentObs;

    invoke-virtual {p0}, Lorg/apache/tika/sax/ToTextContentHandler;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/samsung/index/ITextContentObs;->updateText(Ljava/lang/String;)V

    .line 145
    iget-object v1, p0, Lorg/apache/tika/sax/ToTextContentHandler;->luceneBuffer:Ljava/lang/StringBuffer;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->setLength(I)V

    goto :goto_0
.end method

.method public endDocument()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .prologue
    .line 172
    iget-boolean v1, p0, Lorg/apache/tika/sax/ToTextContentHandler;->isLuceneSearch:Z

    if-nez v1, :cond_0

    .line 174
    :try_start_0
    iget-object v1, p0, Lorg/apache/tika/sax/ToTextContentHandler;->writer:Ljava/io/Writer;

    invoke-virtual {v1}, Ljava/io/Writer;->flush()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 179
    :cond_0
    return-void

    .line 175
    :catch_0
    move-exception v0

    .line 176
    .local v0, "e":Ljava/io/IOException;
    new-instance v1, Lorg/xml/sax/SAXException;

    const-string/jumbo v2, "Error flushing character output"

    invoke-direct {v1, v2, v0}, Lorg/xml/sax/SAXException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v1
.end method

.method public ignorableWhitespace([CII)V
    .locals 0
    .param p1, "ch"    # [C
    .param p2, "start"    # I
    .param p3, "length"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .prologue
    .line 160
    invoke-virtual {p0, p1, p2, p3}, Lorg/apache/tika/sax/ToTextContentHandler;->characters([CII)V

    .line 161
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 190
    iget-boolean v0, p0, Lorg/apache/tika/sax/ToTextContentHandler;->isLuceneSearch:Z

    if-nez v0, :cond_0

    .line 191
    iget-object v0, p0, Lorg/apache/tika/sax/ToTextContentHandler;->writer:Ljava/io/Writer;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 193
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lorg/apache/tika/sax/ToTextContentHandler;->luceneBuffer:Ljava/lang/StringBuffer;

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
