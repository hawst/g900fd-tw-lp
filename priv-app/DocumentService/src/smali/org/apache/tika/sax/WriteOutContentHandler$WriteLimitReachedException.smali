.class Lorg/apache/tika/sax/WriteOutContentHandler$WriteLimitReachedException;
.super Lorg/xml/sax/SAXException;
.source "WriteOutContentHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/tika/sax/WriteOutContentHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "WriteLimitReachedException"
.end annotation


# static fields
.field private static final serialVersionUID:J = -0x19ae96797edc5237L


# instance fields
.field private final tag:Ljava/io/Serializable;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/io/Serializable;)V
    .locals 0
    .param p1, "message"    # Ljava/lang/String;
    .param p2, "tag"    # Ljava/io/Serializable;

    .prologue
    .line 209
    invoke-direct {p0, p1}, Lorg/xml/sax/SAXException;-><init>(Ljava/lang/String;)V

    .line 210
    iput-object p2, p0, Lorg/apache/tika/sax/WriteOutContentHandler$WriteLimitReachedException;->tag:Ljava/io/Serializable;

    .line 211
    return-void
.end method

.method static synthetic access$0(Lorg/apache/tika/sax/WriteOutContentHandler$WriteLimitReachedException;)Ljava/io/Serializable;
    .locals 1

    .prologue
    .line 206
    iget-object v0, p0, Lorg/apache/tika/sax/WriteOutContentHandler$WriteLimitReachedException;->tag:Ljava/io/Serializable;

    return-object v0
.end method
