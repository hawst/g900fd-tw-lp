.class public Lorg/apache/tika/sax/TextContentHandler;
.super Lorg/xml/sax/helpers/DefaultHandler;
.source "TextContentHandler.java"


# static fields
.field private static final SPACE:[C


# instance fields
.field private final addSpaceBetweenElements:Z

.field private final delegate:Lorg/xml/sax/ContentHandler;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 33
    const/4 v0, 0x1

    new-array v0, v0, [C

    const/4 v1, 0x0

    const/16 v2, 0x20

    aput-char v2, v0, v1

    sput-object v0, Lorg/apache/tika/sax/TextContentHandler;->SPACE:[C

    return-void
.end method

.method public constructor <init>(Lorg/xml/sax/ContentHandler;)V
    .locals 1
    .param p1, "delegate"    # Lorg/xml/sax/ContentHandler;

    .prologue
    .line 39
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lorg/apache/tika/sax/TextContentHandler;-><init>(Lorg/xml/sax/ContentHandler;Z)V

    .line 40
    return-void
.end method

.method public constructor <init>(Lorg/xml/sax/ContentHandler;Z)V
    .locals 0
    .param p1, "delegate"    # Lorg/xml/sax/ContentHandler;
    .param p2, "addSpaceBetweenElements"    # Z

    .prologue
    .line 42
    invoke-direct {p0}, Lorg/xml/sax/helpers/DefaultHandler;-><init>()V

    .line 43
    iput-object p1, p0, Lorg/apache/tika/sax/TextContentHandler;->delegate:Lorg/xml/sax/ContentHandler;

    .line 44
    iput-boolean p2, p0, Lorg/apache/tika/sax/TextContentHandler;->addSpaceBetweenElements:Z

    .line 45
    return-void
.end method


# virtual methods
.method public characters([CII)V
    .locals 1
    .param p1, "ch"    # [C
    .param p2, "start"    # I
    .param p3, "length"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .prologue
    .line 50
    iget-object v0, p0, Lorg/apache/tika/sax/TextContentHandler;->delegate:Lorg/xml/sax/ContentHandler;

    invoke-interface {v0, p1, p2, p3}, Lorg/xml/sax/ContentHandler;->characters([CII)V

    .line 51
    return-void
.end method

.method public endDocument()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .prologue
    .line 74
    iget-object v0, p0, Lorg/apache/tika/sax/TextContentHandler;->delegate:Lorg/xml/sax/ContentHandler;

    invoke-interface {v0}, Lorg/xml/sax/ContentHandler;->endDocument()V

    .line 75
    return-void
.end method

.method public ignorableWhitespace([CII)V
    .locals 1
    .param p1, "ch"    # [C
    .param p2, "start"    # I
    .param p3, "length"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .prologue
    .line 56
    iget-object v0, p0, Lorg/apache/tika/sax/TextContentHandler;->delegate:Lorg/xml/sax/ContentHandler;

    invoke-interface {v0, p1, p2, p3}, Lorg/xml/sax/ContentHandler;->ignorableWhitespace([CII)V

    .line 57
    return-void
.end method

.method public startDocument()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .prologue
    .line 69
    iget-object v0, p0, Lorg/apache/tika/sax/TextContentHandler;->delegate:Lorg/xml/sax/ContentHandler;

    invoke-interface {v0}, Lorg/xml/sax/ContentHandler;->startDocument()V

    .line 70
    return-void
.end method

.method public startElement(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 4
    .param p1, "uri"    # Ljava/lang/String;
    .param p2, "localName"    # Ljava/lang/String;
    .param p3, "qName"    # Ljava/lang/String;
    .param p4, "attributes"    # Lorg/xml/sax/Attributes;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .prologue
    .line 62
    iget-boolean v0, p0, Lorg/apache/tika/sax/TextContentHandler;->addSpaceBetweenElements:Z

    if-eqz v0, :cond_0

    .line 63
    iget-object v0, p0, Lorg/apache/tika/sax/TextContentHandler;->delegate:Lorg/xml/sax/ContentHandler;

    sget-object v1, Lorg/apache/tika/sax/TextContentHandler;->SPACE:[C

    const/4 v2, 0x0

    sget-object v3, Lorg/apache/tika/sax/TextContentHandler;->SPACE:[C

    array-length v3, v3

    invoke-interface {v0, v1, v2, v3}, Lorg/xml/sax/ContentHandler;->characters([CII)V

    .line 65
    :cond_0
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lorg/apache/tika/sax/TextContentHandler;->delegate:Lorg/xml/sax/ContentHandler;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
