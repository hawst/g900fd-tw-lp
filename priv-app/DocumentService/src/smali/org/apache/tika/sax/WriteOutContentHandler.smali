.class public Lorg/apache/tika/sax/WriteOutContentHandler;
.super Lorg/apache/tika/sax/ContentHandlerDecorator;
.source "WriteOutContentHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/tika/sax/WriteOutContentHandler$WriteLimitReachedException;
    }
.end annotation


# instance fields
.field private final tag:Ljava/io/Serializable;

.field private writeCount:I

.field private final writeLimit:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 138
    const v0, 0x186a0

    invoke-direct {p0, v0}, Lorg/apache/tika/sax/WriteOutContentHandler;-><init>(I)V

    .line 139
    return-void
.end method

.method public constructor <init>(I)V
    .locals 1
    .param p1, "writeLimit"    # I

    .prologue
    .line 124
    new-instance v0, Ljava/io/StringWriter;

    invoke-direct {v0}, Ljava/io/StringWriter;-><init>()V

    invoke-direct {p0, v0, p1}, Lorg/apache/tika/sax/WriteOutContentHandler;-><init>(Ljava/io/Writer;I)V

    .line 125
    return-void
.end method

.method public constructor <init>(Ljava/io/OutputStream;)V
    .locals 1
    .param p1, "stream"    # Ljava/io/OutputStream;

    .prologue
    .line 106
    new-instance v0, Ljava/io/OutputStreamWriter;

    invoke-direct {v0, p1}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;)V

    invoke-direct {p0, v0}, Lorg/apache/tika/sax/WriteOutContentHandler;-><init>(Ljava/io/Writer;)V

    .line 107
    return-void
.end method

.method public constructor <init>(Ljava/io/Writer;)V
    .locals 1
    .param p1, "writer"    # Ljava/io/Writer;

    .prologue
    .line 96
    const/4 v0, -0x1

    invoke-direct {p0, p1, v0}, Lorg/apache/tika/sax/WriteOutContentHandler;-><init>(Ljava/io/Writer;I)V

    .line 97
    return-void
.end method

.method public constructor <init>(Ljava/io/Writer;I)V
    .locals 1
    .param p1, "writer"    # Ljava/io/Writer;
    .param p2, "writeLimit"    # I

    .prologue
    .line 86
    new-instance v0, Lorg/apache/tika/sax/ToTextContentHandler;

    invoke-direct {v0, p1}, Lorg/apache/tika/sax/ToTextContentHandler;-><init>(Ljava/io/Writer;)V

    invoke-direct {p0, v0, p2}, Lorg/apache/tika/sax/WriteOutContentHandler;-><init>(Lorg/xml/sax/ContentHandler;I)V

    .line 87
    return-void
.end method

.method public constructor <init>(Lorg/apache/tika/sax/ToTextContentHandler;)V
    .locals 1
    .param p1, "toTextContentHandler"    # Lorg/apache/tika/sax/ToTextContentHandler;

    .prologue
    .line 72
    invoke-direct {p0, p1}, Lorg/apache/tika/sax/ContentHandlerDecorator;-><init>(Lorg/xml/sax/ContentHandler;)V

    .line 38
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/tika/sax/WriteOutContentHandler;->tag:Ljava/io/Serializable;

    .line 49
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/tika/sax/WriteOutContentHandler;->writeCount:I

    .line 73
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/tika/sax/WriteOutContentHandler;->writeLimit:I

    .line 74
    return-void
.end method

.method public constructor <init>(Lorg/xml/sax/ContentHandler;I)V
    .locals 1
    .param p1, "handler"    # Lorg/xml/sax/ContentHandler;
    .param p2, "writeLimit"    # I

    .prologue
    .line 60
    invoke-direct {p0, p1}, Lorg/apache/tika/sax/ContentHandlerDecorator;-><init>(Lorg/xml/sax/ContentHandler;)V

    .line 38
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/tika/sax/WriteOutContentHandler;->tag:Ljava/io/Serializable;

    .line 49
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/tika/sax/WriteOutContentHandler;->writeCount:I

    .line 61
    iput p2, p0, Lorg/apache/tika/sax/WriteOutContentHandler;->writeLimit:I

    .line 62
    return-void
.end method


# virtual methods
.method public characters([CII)V
    .locals 3
    .param p1, "ch"    # [C
    .param p2, "start"    # I
    .param p3, "length"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .prologue
    .line 147
    iget v0, p0, Lorg/apache/tika/sax/WriteOutContentHandler;->writeLimit:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget v0, p0, Lorg/apache/tika/sax/WriteOutContentHandler;->writeCount:I

    add-int/2addr v0, p3

    iget v1, p0, Lorg/apache/tika/sax/WriteOutContentHandler;->writeLimit:I

    if-gt v0, v1, :cond_1

    .line 148
    :cond_0
    invoke-super {p0, p1, p2, p3}, Lorg/apache/tika/sax/ContentHandlerDecorator;->characters([CII)V

    .line 149
    iget v0, p0, Lorg/apache/tika/sax/WriteOutContentHandler;->writeCount:I

    add-int/2addr v0, p3

    iput v0, p0, Lorg/apache/tika/sax/WriteOutContentHandler;->writeCount:I

    .line 160
    return-void

    .line 151
    :cond_1
    iget v0, p0, Lorg/apache/tika/sax/WriteOutContentHandler;->writeLimit:I

    iget v1, p0, Lorg/apache/tika/sax/WriteOutContentHandler;->writeCount:I

    sub-int/2addr v0, v1

    invoke-super {p0, p1, p2, v0}, Lorg/apache/tika/sax/ContentHandlerDecorator;->characters([CII)V

    .line 152
    iget v0, p0, Lorg/apache/tika/sax/WriteOutContentHandler;->writeLimit:I

    iput v0, p0, Lorg/apache/tika/sax/WriteOutContentHandler;->writeCount:I

    .line 153
    new-instance v0, Lorg/apache/tika/sax/WriteOutContentHandler$WriteLimitReachedException;

    .line 154
    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Your document contained more than "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lorg/apache/tika/sax/WriteOutContentHandler;->writeLimit:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 155
    const-string/jumbo v2, " characters, and so your requested limit has been"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 156
    const-string/jumbo v2, " reached. To receive the full text of the document,"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 157
    const-string/jumbo v2, " increase your limit. (Text up to the limit is"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 158
    const-string/jumbo v2, " however available)."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 154
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 158
    iget-object v2, p0, Lorg/apache/tika/sax/WriteOutContentHandler;->tag:Ljava/io/Serializable;

    .line 153
    invoke-direct {v0, v1, v2}, Lorg/apache/tika/sax/WriteOutContentHandler$WriteLimitReachedException;-><init>(Ljava/lang/String;Ljava/io/Serializable;)V

    throw v0
.end method

.method public ignorableWhitespace([CII)V
    .locals 3
    .param p1, "ch"    # [C
    .param p2, "start"    # I
    .param p3, "length"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .prologue
    .line 165
    iget v0, p0, Lorg/apache/tika/sax/WriteOutContentHandler;->writeLimit:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget v0, p0, Lorg/apache/tika/sax/WriteOutContentHandler;->writeCount:I

    add-int/2addr v0, p3

    iget v1, p0, Lorg/apache/tika/sax/WriteOutContentHandler;->writeLimit:I

    if-gt v0, v1, :cond_1

    .line 166
    :cond_0
    invoke-super {p0, p1, p2, p3}, Lorg/apache/tika/sax/ContentHandlerDecorator;->ignorableWhitespace([CII)V

    .line 167
    iget v0, p0, Lorg/apache/tika/sax/WriteOutContentHandler;->writeCount:I

    add-int/2addr v0, p3

    iput v0, p0, Lorg/apache/tika/sax/WriteOutContentHandler;->writeCount:I

    .line 178
    return-void

    .line 169
    :cond_1
    iget v0, p0, Lorg/apache/tika/sax/WriteOutContentHandler;->writeLimit:I

    iget v1, p0, Lorg/apache/tika/sax/WriteOutContentHandler;->writeCount:I

    sub-int/2addr v0, v1

    invoke-super {p0, p1, p2, v0}, Lorg/apache/tika/sax/ContentHandlerDecorator;->ignorableWhitespace([CII)V

    .line 170
    iget v0, p0, Lorg/apache/tika/sax/WriteOutContentHandler;->writeLimit:I

    iput v0, p0, Lorg/apache/tika/sax/WriteOutContentHandler;->writeCount:I

    .line 171
    new-instance v0, Lorg/apache/tika/sax/WriteOutContentHandler$WriteLimitReachedException;

    .line 172
    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Your document contained more than "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lorg/apache/tika/sax/WriteOutContentHandler;->writeLimit:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 173
    const-string/jumbo v2, " characters, and so your requested limit has been"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 174
    const-string/jumbo v2, " reached. To receive the full text of the document,"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 175
    const-string/jumbo v2, " increase your limit. (Text up to the limit is"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 176
    const-string/jumbo v2, " however available)."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 172
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 176
    iget-object v2, p0, Lorg/apache/tika/sax/WriteOutContentHandler;->tag:Ljava/io/Serializable;

    .line 171
    invoke-direct {v0, v1, v2}, Lorg/apache/tika/sax/WriteOutContentHandler$WriteLimitReachedException;-><init>(Ljava/lang/String;Ljava/io/Serializable;)V

    throw v0
.end method

.method public isWriteLimitReached(Ljava/lang/Throwable;)Z
    .locals 2
    .param p1, "t"    # Ljava/lang/Throwable;

    .prologue
    .line 190
    instance-of v0, p1, Lorg/apache/tika/sax/WriteOutContentHandler$WriteLimitReachedException;

    if-eqz v0, :cond_0

    .line 191
    iget-object v0, p0, Lorg/apache/tika/sax/WriteOutContentHandler;->tag:Ljava/io/Serializable;

    check-cast p1, Lorg/apache/tika/sax/WriteOutContentHandler$WriteLimitReachedException;

    .end local p1    # "t":Ljava/lang/Throwable;
    # getter for: Lorg/apache/tika/sax/WriteOutContentHandler$WriteLimitReachedException;->tag:Ljava/io/Serializable;
    invoke-static {p1}, Lorg/apache/tika/sax/WriteOutContentHandler$WriteLimitReachedException;->access$0(Lorg/apache/tika/sax/WriteOutContentHandler$WriteLimitReachedException;)Ljava/io/Serializable;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 193
    :goto_0
    return v0

    .restart local p1    # "t":Ljava/lang/Throwable;
    :cond_0
    invoke-virtual {p1}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/apache/tika/sax/WriteOutContentHandler;->isWriteLimitReached(Ljava/lang/Throwable;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
