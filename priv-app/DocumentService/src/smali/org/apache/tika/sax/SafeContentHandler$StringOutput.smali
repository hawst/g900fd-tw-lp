.class Lorg/apache/tika/sax/SafeContentHandler$StringOutput;
.super Ljava/lang/Object;
.source "SafeContentHandler.java"

# interfaces
.implements Lorg/apache/tika/sax/SafeContentHandler$Output;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/tika/sax/SafeContentHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "StringOutput"
.end annotation


# instance fields
.field private final builder:Ljava/lang/StringBuilder;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lorg/apache/tika/sax/SafeContentHandler$StringOutput;->builder:Ljava/lang/StringBuilder;

    .line 61
    return-void
.end method

.method synthetic constructor <init>(Lorg/apache/tika/sax/SafeContentHandler$StringOutput;)V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0}, Lorg/apache/tika/sax/SafeContentHandler$StringOutput;-><init>()V

    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lorg/apache/tika/sax/SafeContentHandler$StringOutput;->builder:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public write([CII)V
    .locals 1
    .param p1, "ch"    # [C
    .param p2, "start"    # I
    .param p3, "length"    # I

    .prologue
    .line 66
    iget-object v0, p0, Lorg/apache/tika/sax/SafeContentHandler$StringOutput;->builder:Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1, p2, p3}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;

    .line 67
    return-void
.end method
