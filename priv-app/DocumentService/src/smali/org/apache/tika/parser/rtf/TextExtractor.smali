.class final Lorg/apache/tika/parser/rtf/TextExtractor;
.super Ljava/lang/Object;
.source "TextExtractor.java"


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final ANSICPG_MAP:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/nio/charset/Charset;",
            ">;"
        }
    .end annotation
.end field

.field private static final ASCII:Ljava/nio/charset/Charset;

.field private static final BIG5:Ljava/nio/charset/Charset;

.field private static final CP1250:Ljava/nio/charset/Charset;

.field private static final CP12502:Ljava/nio/charset/Charset;

.field private static final CP1251:Ljava/nio/charset/Charset;

.field private static final CP12512:Ljava/nio/charset/Charset;

.field private static final CP1252:Ljava/nio/charset/Charset;

.field private static final CP1253:Ljava/nio/charset/Charset;

.field private static final CP12532:Ljava/nio/charset/Charset;

.field private static final CP1254:Ljava/nio/charset/Charset;

.field private static final CP12542:Ljava/nio/charset/Charset;

.field private static final CP1255:Ljava/nio/charset/Charset;

.field private static final CP12552:Ljava/nio/charset/Charset;

.field private static final CP1256:Ljava/nio/charset/Charset;

.field private static final CP12562:Ljava/nio/charset/Charset;

.field private static final CP1257:Ljava/nio/charset/Charset;

.field private static final CP12572:Ljava/nio/charset/Charset;

.field private static final CP1258:Ljava/nio/charset/Charset;

.field private static final CP12582:Ljava/nio/charset/Charset;

.field private static final CP437:Ljava/nio/charset/Charset;

.field private static final CP4372:Ljava/nio/charset/Charset;

.field private static final CP819:Ljava/nio/charset/Charset;

.field private static final CP850:Ljava/nio/charset/Charset;

.field private static final CP8502:Ljava/nio/charset/Charset;

.field private static final CP852:Ljava/nio/charset/Charset;

.field private static final CP860:Ljava/nio/charset/Charset;

.field private static final CP862:Ljava/nio/charset/Charset;

.field private static final CP863:Ljava/nio/charset/Charset;

.field private static final CP864:Ljava/nio/charset/Charset;

.field private static final CP865:Ljava/nio/charset/Charset;

.field private static final CP866:Ljava/nio/charset/Charset;

.field private static final CP949:Ljava/nio/charset/Charset;

.field private static final CP950:Ljava/nio/charset/Charset;

.field private static final FCHARSET_MAP:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/nio/charset/Charset;",
            ">;"
        }
    .end annotation
.end field

.field private static final GB2312:Ljava/nio/charset/Charset;

.field private static final ISO_8859_6:Ljava/nio/charset/Charset;

.field private static final JOHAB:Ljava/nio/charset/Charset;

.field private static final MAC_ARABIC:Ljava/nio/charset/Charset;

.field private static final MAC_CYRILLIC:Ljava/nio/charset/Charset;

.field private static final MAC_GREEK:Ljava/nio/charset/Charset;

.field private static final MAC_HEBREW:Ljava/nio/charset/Charset;

.field private static final MAC_ROMAN:Ljava/nio/charset/Charset;

.field private static final MAC_THAI:Ljava/nio/charset/Charset;

.field private static final MAC_TURKISH:Ljava/nio/charset/Charset;

.field private static final MS1361:Ljava/nio/charset/Charset;

.field private static final MS874:Ljava/nio/charset/Charset;

.field private static final MS8742:Ljava/nio/charset/Charset;

.field private static final MS932:Ljava/nio/charset/Charset;

.field private static final MS936:Ljava/nio/charset/Charset;

.field private static final MS9362:Ljava/nio/charset/Charset;

.field private static final MS949:Ljava/nio/charset/Charset;

.field private static final MS950:Ljava/nio/charset/Charset;

.field private static final SHIFT_JIS:Ljava/nio/charset/Charset;

.field private static final WINDOWS_1252:Ljava/nio/charset/Charset;

.field private static final WINDOWS_57003:Ljava/nio/charset/Charset;

.field private static final WINDOWS_57004:Ljava/nio/charset/Charset;

.field private static final WINDOWS_57005:Ljava/nio/charset/Charset;

.field private static final WINDOWS_57006:Ljava/nio/charset/Charset;

.field private static final WINDOWS_57007:Ljava/nio/charset/Charset;

.field private static final WINDOWS_57008:Ljava/nio/charset/Charset;

.field private static final WINDOWS_57009:Ljava/nio/charset/Charset;

.field private static final WINDOWS_57010:Ljava/nio/charset/Charset;

.field private static final WINDOWS_57011:Ljava/nio/charset/Charset;

.field private static final WINDOWS_709:Ljava/nio/charset/Charset;

.field private static final WINDOWS_710:Ljava/nio/charset/Charset;

.field private static final WINDOWS_711:Ljava/nio/charset/Charset;

.field private static final WINDOWS_720:Ljava/nio/charset/Charset;

.field private static final X_ISCII91:Ljava/nio/charset/Charset;

.field private static final X_JOHAB:Ljava/nio/charset/Charset;

.field private static final X_MAC_CENTRAL_EUROPE:Ljava/nio/charset/Charset;


# instance fields
.field ansiSkip:I

.field private curFontID:I

.field private day:I

.field private decoder:Ljava/nio/charset/CharsetDecoder;

.field private fieldState:I

.field private fontTableDepth:I

.field private fontTableState:I

.field private final fontToCharset:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/nio/charset/Charset;",
            ">;"
        }
    .end annotation
.end field

.field private globalCharset:Ljava/nio/charset/Charset;

.field private globalDefaultFont:I

.field private groupState:Lorg/apache/tika/parser/rtf/GroupState;

.field private final groupStates:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lorg/apache/tika/parser/rtf/GroupState;",
            ">;"
        }
    .end annotation
.end field

.field private hour:I

.field private inHeader:Z

.field private inParagraph:Z

.field private lastCharset:Ljava/nio/charset/Charset;

.field private final metadata:Lorg/apache/tika/metadata/Metadata;

.field private minute:I

.field private month:I

.field private nextMetaData:Lorg/apache/tika/metadata/Property;

.field private final out:Lorg/apache/tika/sax/XHTMLContentHandler;

.field private final outputArray:[C

.field private final outputBuffer:Ljava/nio/CharBuffer;

.field private final pendingBuffer:Ljava/lang/StringBuilder;

.field private pendingByteBuffer:Ljava/nio/ByteBuffer;

.field private pendingByteCount:I

.field private pendingBytes:[B

.field private pendingCharCount:I

.field private pendingChars:[C

.field private pendingControl:[B

.field private pendingControlCount:I

.field private pendingURL:Ljava/lang/String;

.field private uprState:I

.field private year:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/16 v4, 0x333

    const/16 v3, 0x2c6

    .line 54
    const-class v0, Lorg/apache/tika/parser/rtf/TextExtractor;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/tika/parser/rtf/TextExtractor;->$assertionsDisabled:Z

    .line 56
    const-string/jumbo v0, "US-ASCII"

    invoke-static {v0}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v0

    sput-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->ASCII:Ljava/nio/charset/Charset;

    .line 66
    const-string/jumbo v0, "WINDOWS-1252"

    invoke-static {v0}, Lorg/apache/tika/parser/rtf/TextExtractor;->getCharset(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v0

    sput-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->WINDOWS_1252:Ljava/nio/charset/Charset;

    .line 67
    const-string/jumbo v0, "MacRoman"

    invoke-static {v0}, Lorg/apache/tika/parser/rtf/TextExtractor;->getCharset(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v0

    sput-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->MAC_ROMAN:Ljava/nio/charset/Charset;

    .line 68
    const-string/jumbo v0, "Shift_JIS"

    invoke-static {v0}, Lorg/apache/tika/parser/rtf/TextExtractor;->getCharset(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v0

    sput-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->SHIFT_JIS:Ljava/nio/charset/Charset;

    .line 69
    const-string/jumbo v0, "windows-57011"

    invoke-static {v0}, Lorg/apache/tika/parser/rtf/TextExtractor;->getCharset(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v0

    sput-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->WINDOWS_57011:Ljava/nio/charset/Charset;

    .line 70
    const-string/jumbo v0, "windows-57010"

    invoke-static {v0}, Lorg/apache/tika/parser/rtf/TextExtractor;->getCharset(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v0

    sput-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->WINDOWS_57010:Ljava/nio/charset/Charset;

    .line 71
    const-string/jumbo v0, "windows-57009"

    invoke-static {v0}, Lorg/apache/tika/parser/rtf/TextExtractor;->getCharset(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v0

    sput-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->WINDOWS_57009:Ljava/nio/charset/Charset;

    .line 72
    const-string/jumbo v0, "windows-57008"

    invoke-static {v0}, Lorg/apache/tika/parser/rtf/TextExtractor;->getCharset(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v0

    sput-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->WINDOWS_57008:Ljava/nio/charset/Charset;

    .line 73
    const-string/jumbo v0, "windows-57007"

    invoke-static {v0}, Lorg/apache/tika/parser/rtf/TextExtractor;->getCharset(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v0

    sput-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->WINDOWS_57007:Ljava/nio/charset/Charset;

    .line 74
    const-string/jumbo v0, "windows-57006"

    invoke-static {v0}, Lorg/apache/tika/parser/rtf/TextExtractor;->getCharset(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v0

    sput-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->WINDOWS_57006:Ljava/nio/charset/Charset;

    .line 75
    const-string/jumbo v0, "windows-57005"

    invoke-static {v0}, Lorg/apache/tika/parser/rtf/TextExtractor;->getCharset(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v0

    sput-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->WINDOWS_57005:Ljava/nio/charset/Charset;

    .line 76
    const-string/jumbo v0, "windows-57004"

    invoke-static {v0}, Lorg/apache/tika/parser/rtf/TextExtractor;->getCharset(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v0

    sput-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->WINDOWS_57004:Ljava/nio/charset/Charset;

    .line 77
    const-string/jumbo v0, "windows-57003"

    invoke-static {v0}, Lorg/apache/tika/parser/rtf/TextExtractor;->getCharset(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v0

    sput-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->WINDOWS_57003:Ljava/nio/charset/Charset;

    .line 78
    const-string/jumbo v0, "x-ISCII91"

    invoke-static {v0}, Lorg/apache/tika/parser/rtf/TextExtractor;->getCharset(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v0

    sput-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->X_ISCII91:Ljava/nio/charset/Charset;

    .line 79
    const-string/jumbo v0, "x-MacCentralEurope"

    invoke-static {v0}, Lorg/apache/tika/parser/rtf/TextExtractor;->getCharset(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v0

    sput-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->X_MAC_CENTRAL_EUROPE:Ljava/nio/charset/Charset;

    .line 80
    const-string/jumbo v0, "MacCyrillic"

    invoke-static {v0}, Lorg/apache/tika/parser/rtf/TextExtractor;->getCharset(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v0

    sput-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->MAC_CYRILLIC:Ljava/nio/charset/Charset;

    .line 81
    const-string/jumbo v0, "x-Johab"

    invoke-static {v0}, Lorg/apache/tika/parser/rtf/TextExtractor;->getCharset(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v0

    sput-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->X_JOHAB:Ljava/nio/charset/Charset;

    .line 82
    const-string/jumbo v0, "CP1258"

    invoke-static {v0}, Lorg/apache/tika/parser/rtf/TextExtractor;->getCharset(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v0

    sput-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->CP12582:Ljava/nio/charset/Charset;

    .line 83
    const-string/jumbo v0, "CP1257"

    invoke-static {v0}, Lorg/apache/tika/parser/rtf/TextExtractor;->getCharset(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v0

    sput-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->CP12572:Ljava/nio/charset/Charset;

    .line 84
    const-string/jumbo v0, "CP1256"

    invoke-static {v0}, Lorg/apache/tika/parser/rtf/TextExtractor;->getCharset(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v0

    sput-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->CP12562:Ljava/nio/charset/Charset;

    .line 85
    const-string/jumbo v0, "CP1255"

    invoke-static {v0}, Lorg/apache/tika/parser/rtf/TextExtractor;->getCharset(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v0

    sput-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->CP12552:Ljava/nio/charset/Charset;

    .line 86
    const-string/jumbo v0, "CP1254"

    invoke-static {v0}, Lorg/apache/tika/parser/rtf/TextExtractor;->getCharset(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v0

    sput-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->CP12542:Ljava/nio/charset/Charset;

    .line 87
    const-string/jumbo v0, "CP1253"

    invoke-static {v0}, Lorg/apache/tika/parser/rtf/TextExtractor;->getCharset(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v0

    sput-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->CP12532:Ljava/nio/charset/Charset;

    .line 88
    const-string/jumbo v0, "CP1252"

    invoke-static {v0}, Lorg/apache/tika/parser/rtf/TextExtractor;->getCharset(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v0

    sput-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->CP1252:Ljava/nio/charset/Charset;

    .line 89
    const-string/jumbo v0, "CP1251"

    invoke-static {v0}, Lorg/apache/tika/parser/rtf/TextExtractor;->getCharset(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v0

    sput-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->CP12512:Ljava/nio/charset/Charset;

    .line 90
    const-string/jumbo v0, "CP1250"

    invoke-static {v0}, Lorg/apache/tika/parser/rtf/TextExtractor;->getCharset(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v0

    sput-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->CP12502:Ljava/nio/charset/Charset;

    .line 91
    const-string/jumbo v0, "CP950"

    invoke-static {v0}, Lorg/apache/tika/parser/rtf/TextExtractor;->getCharset(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v0

    sput-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->CP950:Ljava/nio/charset/Charset;

    .line 92
    const-string/jumbo v0, "CP949"

    invoke-static {v0}, Lorg/apache/tika/parser/rtf/TextExtractor;->getCharset(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v0

    sput-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->CP949:Ljava/nio/charset/Charset;

    .line 93
    const-string/jumbo v0, "MS936"

    invoke-static {v0}, Lorg/apache/tika/parser/rtf/TextExtractor;->getCharset(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v0

    sput-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->MS9362:Ljava/nio/charset/Charset;

    .line 94
    const-string/jumbo v0, "MS874"

    invoke-static {v0}, Lorg/apache/tika/parser/rtf/TextExtractor;->getCharset(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v0

    sput-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->MS8742:Ljava/nio/charset/Charset;

    .line 95
    const-string/jumbo v0, "CP866"

    invoke-static {v0}, Lorg/apache/tika/parser/rtf/TextExtractor;->getCharset(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v0

    sput-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->CP866:Ljava/nio/charset/Charset;

    .line 96
    const-string/jumbo v0, "CP865"

    invoke-static {v0}, Lorg/apache/tika/parser/rtf/TextExtractor;->getCharset(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v0

    sput-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->CP865:Ljava/nio/charset/Charset;

    .line 97
    const-string/jumbo v0, "CP864"

    invoke-static {v0}, Lorg/apache/tika/parser/rtf/TextExtractor;->getCharset(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v0

    sput-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->CP864:Ljava/nio/charset/Charset;

    .line 98
    const-string/jumbo v0, "CP863"

    invoke-static {v0}, Lorg/apache/tika/parser/rtf/TextExtractor;->getCharset(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v0

    sput-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->CP863:Ljava/nio/charset/Charset;

    .line 99
    const-string/jumbo v0, "CP862"

    invoke-static {v0}, Lorg/apache/tika/parser/rtf/TextExtractor;->getCharset(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v0

    sput-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->CP862:Ljava/nio/charset/Charset;

    .line 100
    const-string/jumbo v0, "CP860"

    invoke-static {v0}, Lorg/apache/tika/parser/rtf/TextExtractor;->getCharset(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v0

    sput-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->CP860:Ljava/nio/charset/Charset;

    .line 101
    const-string/jumbo v0, "CP852"

    invoke-static {v0}, Lorg/apache/tika/parser/rtf/TextExtractor;->getCharset(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v0

    sput-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->CP852:Ljava/nio/charset/Charset;

    .line 102
    const-string/jumbo v0, "CP850"

    invoke-static {v0}, Lorg/apache/tika/parser/rtf/TextExtractor;->getCharset(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v0

    sput-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->CP8502:Ljava/nio/charset/Charset;

    .line 103
    const-string/jumbo v0, "CP819"

    invoke-static {v0}, Lorg/apache/tika/parser/rtf/TextExtractor;->getCharset(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v0

    sput-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->CP819:Ljava/nio/charset/Charset;

    .line 104
    const-string/jumbo v0, "windows-720"

    invoke-static {v0}, Lorg/apache/tika/parser/rtf/TextExtractor;->getCharset(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v0

    sput-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->WINDOWS_720:Ljava/nio/charset/Charset;

    .line 105
    const-string/jumbo v0, "windows-711"

    invoke-static {v0}, Lorg/apache/tika/parser/rtf/TextExtractor;->getCharset(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v0

    sput-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->WINDOWS_711:Ljava/nio/charset/Charset;

    .line 106
    const-string/jumbo v0, "windows-710"

    invoke-static {v0}, Lorg/apache/tika/parser/rtf/TextExtractor;->getCharset(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v0

    sput-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->WINDOWS_710:Ljava/nio/charset/Charset;

    .line 107
    const-string/jumbo v0, "windows-709"

    invoke-static {v0}, Lorg/apache/tika/parser/rtf/TextExtractor;->getCharset(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v0

    sput-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->WINDOWS_709:Ljava/nio/charset/Charset;

    .line 108
    const-string/jumbo v0, "ISO-8859-6"

    invoke-static {v0}, Lorg/apache/tika/parser/rtf/TextExtractor;->getCharset(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v0

    sput-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->ISO_8859_6:Ljava/nio/charset/Charset;

    .line 109
    const-string/jumbo v0, "CP437"

    invoke-static {v0}, Lorg/apache/tika/parser/rtf/TextExtractor;->getCharset(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v0

    sput-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->CP4372:Ljava/nio/charset/Charset;

    .line 110
    const-string/jumbo v0, "cp850"

    invoke-static {v0}, Lorg/apache/tika/parser/rtf/TextExtractor;->getCharset(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v0

    sput-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->CP850:Ljava/nio/charset/Charset;

    .line 111
    const-string/jumbo v0, "cp437"

    invoke-static {v0}, Lorg/apache/tika/parser/rtf/TextExtractor;->getCharset(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v0

    sput-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->CP437:Ljava/nio/charset/Charset;

    .line 112
    const-string/jumbo v0, "ms874"

    invoke-static {v0}, Lorg/apache/tika/parser/rtf/TextExtractor;->getCharset(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v0

    sput-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->MS874:Ljava/nio/charset/Charset;

    .line 113
    const-string/jumbo v0, "cp1257"

    invoke-static {v0}, Lorg/apache/tika/parser/rtf/TextExtractor;->getCharset(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v0

    sput-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->CP1257:Ljava/nio/charset/Charset;

    .line 114
    const-string/jumbo v0, "cp1256"

    invoke-static {v0}, Lorg/apache/tika/parser/rtf/TextExtractor;->getCharset(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v0

    sput-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->CP1256:Ljava/nio/charset/Charset;

    .line 115
    const-string/jumbo v0, "cp1255"

    invoke-static {v0}, Lorg/apache/tika/parser/rtf/TextExtractor;->getCharset(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v0

    sput-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->CP1255:Ljava/nio/charset/Charset;

    .line 116
    const-string/jumbo v0, "cp1258"

    invoke-static {v0}, Lorg/apache/tika/parser/rtf/TextExtractor;->getCharset(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v0

    sput-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->CP1258:Ljava/nio/charset/Charset;

    .line 117
    const-string/jumbo v0, "cp1254"

    invoke-static {v0}, Lorg/apache/tika/parser/rtf/TextExtractor;->getCharset(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v0

    sput-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->CP1254:Ljava/nio/charset/Charset;

    .line 118
    const-string/jumbo v0, "cp1253"

    invoke-static {v0}, Lorg/apache/tika/parser/rtf/TextExtractor;->getCharset(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v0

    sput-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->CP1253:Ljava/nio/charset/Charset;

    .line 119
    const-string/jumbo v0, "ms950"

    invoke-static {v0}, Lorg/apache/tika/parser/rtf/TextExtractor;->getCharset(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v0

    sput-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->MS950:Ljava/nio/charset/Charset;

    .line 120
    const-string/jumbo v0, "ms936"

    invoke-static {v0}, Lorg/apache/tika/parser/rtf/TextExtractor;->getCharset(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v0

    sput-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->MS936:Ljava/nio/charset/Charset;

    .line 121
    const-string/jumbo v0, "ms1361"

    invoke-static {v0}, Lorg/apache/tika/parser/rtf/TextExtractor;->getCharset(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v0

    sput-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->MS1361:Ljava/nio/charset/Charset;

    .line 122
    const-string/jumbo v0, "MS932"

    invoke-static {v0}, Lorg/apache/tika/parser/rtf/TextExtractor;->getCharset(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v0

    sput-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->MS932:Ljava/nio/charset/Charset;

    .line 123
    const-string/jumbo v0, "cp1251"

    invoke-static {v0}, Lorg/apache/tika/parser/rtf/TextExtractor;->getCharset(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v0

    sput-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->CP1251:Ljava/nio/charset/Charset;

    .line 124
    const-string/jumbo v0, "cp1250"

    invoke-static {v0}, Lorg/apache/tika/parser/rtf/TextExtractor;->getCharset(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v0

    sput-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->CP1250:Ljava/nio/charset/Charset;

    .line 125
    const-string/jumbo v0, "MacThai"

    invoke-static {v0}, Lorg/apache/tika/parser/rtf/TextExtractor;->getCharset(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v0

    sput-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->MAC_THAI:Ljava/nio/charset/Charset;

    .line 126
    const-string/jumbo v0, "MacTurkish"

    invoke-static {v0}, Lorg/apache/tika/parser/rtf/TextExtractor;->getCharset(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v0

    sput-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->MAC_TURKISH:Ljava/nio/charset/Charset;

    .line 127
    const-string/jumbo v0, "MacGreek"

    invoke-static {v0}, Lorg/apache/tika/parser/rtf/TextExtractor;->getCharset(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v0

    sput-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->MAC_GREEK:Ljava/nio/charset/Charset;

    .line 128
    const-string/jumbo v0, "MacArabic"

    invoke-static {v0}, Lorg/apache/tika/parser/rtf/TextExtractor;->getCharset(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v0

    sput-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->MAC_ARABIC:Ljava/nio/charset/Charset;

    .line 129
    const-string/jumbo v0, "MacHebrew"

    invoke-static {v0}, Lorg/apache/tika/parser/rtf/TextExtractor;->getCharset(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v0

    sput-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->MAC_HEBREW:Ljava/nio/charset/Charset;

    .line 130
    const-string/jumbo v0, "johab"

    invoke-static {v0}, Lorg/apache/tika/parser/rtf/TextExtractor;->getCharset(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v0

    sput-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->JOHAB:Ljava/nio/charset/Charset;

    .line 131
    const-string/jumbo v0, "Big5"

    invoke-static {v0}, Lorg/apache/tika/parser/rtf/TextExtractor;->getCharset(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v0

    sput-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->BIG5:Ljava/nio/charset/Charset;

    .line 132
    const-string/jumbo v0, "GB2312"

    invoke-static {v0}, Lorg/apache/tika/parser/rtf/TextExtractor;->getCharset(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v0

    sput-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->GB2312:Ljava/nio/charset/Charset;

    .line 133
    const-string/jumbo v0, "ms949"

    invoke-static {v0}, Lorg/apache/tika/parser/rtf/TextExtractor;->getCharset(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v0

    sput-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->MS949:Ljava/nio/charset/Charset;

    .line 216
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 215
    sput-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->FCHARSET_MAP:Ljava/util/Map;

    .line 219
    sget-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->FCHARSET_MAP:Ljava/util/Map;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lorg/apache/tika/parser/rtf/TextExtractor;->WINDOWS_1252:Ljava/nio/charset/Charset;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 223
    sget-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->FCHARSET_MAP:Ljava/util/Map;

    const/16 v1, 0x4d

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lorg/apache/tika/parser/rtf/TextExtractor;->MAC_ROMAN:Ljava/nio/charset/Charset;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 224
    sget-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->FCHARSET_MAP:Ljava/util/Map;

    const/16 v1, 0x4e

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lorg/apache/tika/parser/rtf/TextExtractor;->SHIFT_JIS:Ljava/nio/charset/Charset;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 225
    sget-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->FCHARSET_MAP:Ljava/util/Map;

    const/16 v1, 0x4f

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lorg/apache/tika/parser/rtf/TextExtractor;->MS949:Ljava/nio/charset/Charset;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 226
    sget-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->FCHARSET_MAP:Ljava/util/Map;

    const/16 v1, 0x50

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lorg/apache/tika/parser/rtf/TextExtractor;->GB2312:Ljava/nio/charset/Charset;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 227
    sget-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->FCHARSET_MAP:Ljava/util/Map;

    const/16 v1, 0x51

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lorg/apache/tika/parser/rtf/TextExtractor;->BIG5:Ljava/nio/charset/Charset;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 228
    sget-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->FCHARSET_MAP:Ljava/util/Map;

    const/16 v1, 0x52

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lorg/apache/tika/parser/rtf/TextExtractor;->JOHAB:Ljava/nio/charset/Charset;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 229
    sget-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->FCHARSET_MAP:Ljava/util/Map;

    const/16 v1, 0x53

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lorg/apache/tika/parser/rtf/TextExtractor;->MAC_HEBREW:Ljava/nio/charset/Charset;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 230
    sget-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->FCHARSET_MAP:Ljava/util/Map;

    const/16 v1, 0x54

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lorg/apache/tika/parser/rtf/TextExtractor;->MAC_ARABIC:Ljava/nio/charset/Charset;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 231
    sget-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->FCHARSET_MAP:Ljava/util/Map;

    const/16 v1, 0x55

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lorg/apache/tika/parser/rtf/TextExtractor;->MAC_GREEK:Ljava/nio/charset/Charset;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 232
    sget-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->FCHARSET_MAP:Ljava/util/Map;

    const/16 v1, 0x56

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lorg/apache/tika/parser/rtf/TextExtractor;->MAC_TURKISH:Ljava/nio/charset/Charset;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 233
    sget-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->FCHARSET_MAP:Ljava/util/Map;

    const/16 v1, 0x57

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lorg/apache/tika/parser/rtf/TextExtractor;->MAC_THAI:Ljava/nio/charset/Charset;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 234
    sget-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->FCHARSET_MAP:Ljava/util/Map;

    const/16 v1, 0x58

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lorg/apache/tika/parser/rtf/TextExtractor;->CP1250:Ljava/nio/charset/Charset;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 235
    sget-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->FCHARSET_MAP:Ljava/util/Map;

    const/16 v1, 0x59

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lorg/apache/tika/parser/rtf/TextExtractor;->CP1251:Ljava/nio/charset/Charset;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 237
    sget-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->FCHARSET_MAP:Ljava/util/Map;

    const/16 v1, 0x80

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lorg/apache/tika/parser/rtf/TextExtractor;->MS932:Ljava/nio/charset/Charset;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 238
    sget-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->FCHARSET_MAP:Ljava/util/Map;

    const/16 v1, 0x81

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lorg/apache/tika/parser/rtf/TextExtractor;->MS949:Ljava/nio/charset/Charset;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 239
    sget-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->FCHARSET_MAP:Ljava/util/Map;

    const/16 v1, 0x82

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lorg/apache/tika/parser/rtf/TextExtractor;->MS1361:Ljava/nio/charset/Charset;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 240
    sget-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->FCHARSET_MAP:Ljava/util/Map;

    const/16 v1, 0x86

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lorg/apache/tika/parser/rtf/TextExtractor;->MS936:Ljava/nio/charset/Charset;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 241
    sget-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->FCHARSET_MAP:Ljava/util/Map;

    const/16 v1, 0x88

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lorg/apache/tika/parser/rtf/TextExtractor;->MS950:Ljava/nio/charset/Charset;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 242
    sget-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->FCHARSET_MAP:Ljava/util/Map;

    const/16 v1, 0xa1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lorg/apache/tika/parser/rtf/TextExtractor;->CP1253:Ljava/nio/charset/Charset;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 243
    sget-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->FCHARSET_MAP:Ljava/util/Map;

    const/16 v1, 0xa2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lorg/apache/tika/parser/rtf/TextExtractor;->CP1254:Ljava/nio/charset/Charset;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 244
    sget-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->FCHARSET_MAP:Ljava/util/Map;

    const/16 v1, 0xa3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lorg/apache/tika/parser/rtf/TextExtractor;->CP1258:Ljava/nio/charset/Charset;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 245
    sget-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->FCHARSET_MAP:Ljava/util/Map;

    const/16 v1, 0xb1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lorg/apache/tika/parser/rtf/TextExtractor;->CP1255:Ljava/nio/charset/Charset;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 246
    sget-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->FCHARSET_MAP:Ljava/util/Map;

    const/16 v1, 0xb2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lorg/apache/tika/parser/rtf/TextExtractor;->CP1256:Ljava/nio/charset/Charset;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 250
    sget-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->FCHARSET_MAP:Ljava/util/Map;

    const/16 v1, 0xba

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lorg/apache/tika/parser/rtf/TextExtractor;->CP1257:Ljava/nio/charset/Charset;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 252
    sget-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->FCHARSET_MAP:Ljava/util/Map;

    const/16 v1, 0xcc

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lorg/apache/tika/parser/rtf/TextExtractor;->CP1251:Ljava/nio/charset/Charset;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 253
    sget-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->FCHARSET_MAP:Ljava/util/Map;

    const/16 v1, 0xde

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lorg/apache/tika/parser/rtf/TextExtractor;->MS874:Ljava/nio/charset/Charset;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 254
    sget-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->FCHARSET_MAP:Ljava/util/Map;

    const/16 v1, 0xee

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lorg/apache/tika/parser/rtf/TextExtractor;->CP1250:Ljava/nio/charset/Charset;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 255
    sget-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->FCHARSET_MAP:Ljava/util/Map;

    const/16 v1, 0xfe

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lorg/apache/tika/parser/rtf/TextExtractor;->CP437:Ljava/nio/charset/Charset;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 256
    sget-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->FCHARSET_MAP:Ljava/util/Map;

    const/16 v1, 0xff

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lorg/apache/tika/parser/rtf/TextExtractor;->CP850:Ljava/nio/charset/Charset;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 263
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 262
    sput-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->ANSICPG_MAP:Ljava/util/Map;

    .line 265
    sget-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->ANSICPG_MAP:Ljava/util/Map;

    const/16 v1, 0x1b5

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lorg/apache/tika/parser/rtf/TextExtractor;->CP4372:Ljava/nio/charset/Charset;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 266
    sget-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->ANSICPG_MAP:Ljava/util/Map;

    const/16 v1, 0x2c4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lorg/apache/tika/parser/rtf/TextExtractor;->ISO_8859_6:Ljava/nio/charset/Charset;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 268
    sget-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->ANSICPG_MAP:Ljava/util/Map;

    const/16 v1, 0x2c5

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lorg/apache/tika/parser/rtf/TextExtractor;->WINDOWS_709:Ljava/nio/charset/Charset;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 269
    sget-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->ANSICPG_MAP:Ljava/util/Map;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lorg/apache/tika/parser/rtf/TextExtractor;->WINDOWS_710:Ljava/nio/charset/Charset;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 270
    sget-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->ANSICPG_MAP:Ljava/util/Map;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lorg/apache/tika/parser/rtf/TextExtractor;->WINDOWS_711:Ljava/nio/charset/Charset;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 271
    sget-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->ANSICPG_MAP:Ljava/util/Map;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lorg/apache/tika/parser/rtf/TextExtractor;->WINDOWS_720:Ljava/nio/charset/Charset;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 272
    sget-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->ANSICPG_MAP:Ljava/util/Map;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lorg/apache/tika/parser/rtf/TextExtractor;->CP819:Ljava/nio/charset/Charset;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 273
    sget-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->ANSICPG_MAP:Ljava/util/Map;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lorg/apache/tika/parser/rtf/TextExtractor;->CP819:Ljava/nio/charset/Charset;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 275
    sget-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->ANSICPG_MAP:Ljava/util/Map;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lorg/apache/tika/parser/rtf/TextExtractor;->CP819:Ljava/nio/charset/Charset;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 276
    sget-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->ANSICPG_MAP:Ljava/util/Map;

    const/16 v1, 0x352

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lorg/apache/tika/parser/rtf/TextExtractor;->CP8502:Ljava/nio/charset/Charset;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 277
    sget-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->ANSICPG_MAP:Ljava/util/Map;

    const/16 v1, 0x354

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lorg/apache/tika/parser/rtf/TextExtractor;->CP852:Ljava/nio/charset/Charset;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 278
    sget-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->ANSICPG_MAP:Ljava/util/Map;

    const/16 v1, 0x35c

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lorg/apache/tika/parser/rtf/TextExtractor;->CP860:Ljava/nio/charset/Charset;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 279
    sget-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->ANSICPG_MAP:Ljava/util/Map;

    const/16 v1, 0x35e

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lorg/apache/tika/parser/rtf/TextExtractor;->CP862:Ljava/nio/charset/Charset;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 280
    sget-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->ANSICPG_MAP:Ljava/util/Map;

    const/16 v1, 0x35f

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lorg/apache/tika/parser/rtf/TextExtractor;->CP863:Ljava/nio/charset/Charset;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 281
    sget-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->ANSICPG_MAP:Ljava/util/Map;

    const/16 v1, 0x360

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lorg/apache/tika/parser/rtf/TextExtractor;->CP864:Ljava/nio/charset/Charset;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 282
    sget-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->ANSICPG_MAP:Ljava/util/Map;

    const/16 v1, 0x361

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lorg/apache/tika/parser/rtf/TextExtractor;->CP865:Ljava/nio/charset/Charset;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 283
    sget-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->ANSICPG_MAP:Ljava/util/Map;

    const/16 v1, 0x362

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lorg/apache/tika/parser/rtf/TextExtractor;->CP866:Ljava/nio/charset/Charset;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 284
    sget-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->ANSICPG_MAP:Ljava/util/Map;

    const/16 v1, 0x36a

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lorg/apache/tika/parser/rtf/TextExtractor;->MS8742:Ljava/nio/charset/Charset;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 285
    sget-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->ANSICPG_MAP:Ljava/util/Map;

    const/16 v1, 0x3a4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lorg/apache/tika/parser/rtf/TextExtractor;->MS932:Ljava/nio/charset/Charset;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 286
    sget-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->ANSICPG_MAP:Ljava/util/Map;

    const/16 v1, 0x3a8

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lorg/apache/tika/parser/rtf/TextExtractor;->MS9362:Ljava/nio/charset/Charset;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 287
    sget-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->ANSICPG_MAP:Ljava/util/Map;

    const/16 v1, 0x3b5

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lorg/apache/tika/parser/rtf/TextExtractor;->CP949:Ljava/nio/charset/Charset;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 288
    sget-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->ANSICPG_MAP:Ljava/util/Map;

    const/16 v1, 0x3b6

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lorg/apache/tika/parser/rtf/TextExtractor;->CP950:Ljava/nio/charset/Charset;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 289
    sget-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->ANSICPG_MAP:Ljava/util/Map;

    const/16 v1, 0x4e2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lorg/apache/tika/parser/rtf/TextExtractor;->CP12502:Ljava/nio/charset/Charset;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 290
    sget-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->ANSICPG_MAP:Ljava/util/Map;

    const/16 v1, 0x4e3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lorg/apache/tika/parser/rtf/TextExtractor;->CP12512:Ljava/nio/charset/Charset;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 291
    sget-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->ANSICPG_MAP:Ljava/util/Map;

    const/16 v1, 0x4e4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lorg/apache/tika/parser/rtf/TextExtractor;->CP1252:Ljava/nio/charset/Charset;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 292
    sget-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->ANSICPG_MAP:Ljava/util/Map;

    const/16 v1, 0x4e5

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lorg/apache/tika/parser/rtf/TextExtractor;->CP12532:Ljava/nio/charset/Charset;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 293
    sget-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->ANSICPG_MAP:Ljava/util/Map;

    const/16 v1, 0x4e6

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lorg/apache/tika/parser/rtf/TextExtractor;->CP12542:Ljava/nio/charset/Charset;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 294
    sget-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->ANSICPG_MAP:Ljava/util/Map;

    const/16 v1, 0x4e7

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lorg/apache/tika/parser/rtf/TextExtractor;->CP12552:Ljava/nio/charset/Charset;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 295
    sget-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->ANSICPG_MAP:Ljava/util/Map;

    const/16 v1, 0x4e8

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lorg/apache/tika/parser/rtf/TextExtractor;->CP12562:Ljava/nio/charset/Charset;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 296
    sget-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->ANSICPG_MAP:Ljava/util/Map;

    const/16 v1, 0x4e9

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lorg/apache/tika/parser/rtf/TextExtractor;->CP12572:Ljava/nio/charset/Charset;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 297
    sget-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->ANSICPG_MAP:Ljava/util/Map;

    const/16 v1, 0x4ea

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lorg/apache/tika/parser/rtf/TextExtractor;->CP12582:Ljava/nio/charset/Charset;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 298
    sget-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->ANSICPG_MAP:Ljava/util/Map;

    const/16 v1, 0x551

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lorg/apache/tika/parser/rtf/TextExtractor;->X_JOHAB:Ljava/nio/charset/Charset;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 299
    sget-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->ANSICPG_MAP:Ljava/util/Map;

    const/16 v1, 0x2710

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lorg/apache/tika/parser/rtf/TextExtractor;->MAC_ROMAN:Ljava/nio/charset/Charset;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 300
    sget-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->ANSICPG_MAP:Ljava/util/Map;

    const/16 v1, 0x2711

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lorg/apache/tika/parser/rtf/TextExtractor;->SHIFT_JIS:Ljava/nio/charset/Charset;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 301
    sget-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->ANSICPG_MAP:Ljava/util/Map;

    const/16 v1, 0x2714

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lorg/apache/tika/parser/rtf/TextExtractor;->MAC_ARABIC:Ljava/nio/charset/Charset;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 302
    sget-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->ANSICPG_MAP:Ljava/util/Map;

    const/16 v1, 0x2715

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lorg/apache/tika/parser/rtf/TextExtractor;->MAC_HEBREW:Ljava/nio/charset/Charset;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 303
    sget-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->ANSICPG_MAP:Ljava/util/Map;

    const/16 v1, 0x2716

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lorg/apache/tika/parser/rtf/TextExtractor;->MAC_GREEK:Ljava/nio/charset/Charset;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 304
    sget-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->ANSICPG_MAP:Ljava/util/Map;

    const/16 v1, 0x2717

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lorg/apache/tika/parser/rtf/TextExtractor;->MAC_CYRILLIC:Ljava/nio/charset/Charset;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 305
    sget-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->ANSICPG_MAP:Ljava/util/Map;

    const/16 v1, 0x272d

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lorg/apache/tika/parser/rtf/TextExtractor;->X_MAC_CENTRAL_EUROPE:Ljava/nio/charset/Charset;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 306
    sget-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->ANSICPG_MAP:Ljava/util/Map;

    const/16 v1, 0x2761

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lorg/apache/tika/parser/rtf/TextExtractor;->MAC_TURKISH:Ljava/nio/charset/Charset;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 307
    sget-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->ANSICPG_MAP:Ljava/util/Map;

    const v1, 0xdeaa

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lorg/apache/tika/parser/rtf/TextExtractor;->X_ISCII91:Ljava/nio/charset/Charset;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 312
    sget-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->ANSICPG_MAP:Ljava/util/Map;

    const v1, 0xdeab

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lorg/apache/tika/parser/rtf/TextExtractor;->WINDOWS_57003:Ljava/nio/charset/Charset;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 313
    sget-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->ANSICPG_MAP:Ljava/util/Map;

    const v1, 0xdeac

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lorg/apache/tika/parser/rtf/TextExtractor;->WINDOWS_57004:Ljava/nio/charset/Charset;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 314
    sget-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->ANSICPG_MAP:Ljava/util/Map;

    const v1, 0xdead

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lorg/apache/tika/parser/rtf/TextExtractor;->WINDOWS_57005:Ljava/nio/charset/Charset;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 315
    sget-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->ANSICPG_MAP:Ljava/util/Map;

    const v1, 0xdeae

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lorg/apache/tika/parser/rtf/TextExtractor;->WINDOWS_57006:Ljava/nio/charset/Charset;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 316
    sget-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->ANSICPG_MAP:Ljava/util/Map;

    const v1, 0xdeaf

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lorg/apache/tika/parser/rtf/TextExtractor;->WINDOWS_57007:Ljava/nio/charset/Charset;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 317
    sget-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->ANSICPG_MAP:Ljava/util/Map;

    const v1, 0xdeb0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lorg/apache/tika/parser/rtf/TextExtractor;->WINDOWS_57008:Ljava/nio/charset/Charset;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 318
    sget-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->ANSICPG_MAP:Ljava/util/Map;

    const v1, 0xdeb1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lorg/apache/tika/parser/rtf/TextExtractor;->WINDOWS_57009:Ljava/nio/charset/Charset;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 319
    sget-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->ANSICPG_MAP:Ljava/util/Map;

    const v1, 0xdeb2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lorg/apache/tika/parser/rtf/TextExtractor;->WINDOWS_57010:Ljava/nio/charset/Charset;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 320
    sget-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->ANSICPG_MAP:Ljava/util/Map;

    const v1, 0xdeb3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lorg/apache/tika/parser/rtf/TextExtractor;->WINDOWS_57011:Ljava/nio/charset/Charset;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 321
    return-void

    :cond_0
    move v0, v1

    .line 54
    goto/16 :goto_0
.end method

.method public constructor <init>(Lorg/apache/tika/sax/XHTMLContentHandler;Lorg/apache/tika/metadata/Metadata;)V
    .locals 3
    .param p1, "out"    # Lorg/apache/tika/sax/XHTMLContentHandler;
    .param p2, "metadata"    # Lorg/apache/tika/metadata/Metadata;

    .prologue
    const/16 v2, 0xa

    const/4 v1, -0x1

    .line 323
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 137
    const/16 v0, 0x10

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->pendingBytes:[B

    .line 139
    iget-object v0, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->pendingBytes:[B

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->pendingByteBuffer:Ljava/nio/ByteBuffer;

    .line 142
    new-array v0, v2, [C

    iput-object v0, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->pendingChars:[C

    .line 146
    new-array v0, v2, [B

    iput-object v0, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->pendingControl:[B

    .line 150
    const/16 v0, 0x80

    new-array v0, v0, [C

    iput-object v0, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->outputArray:[C

    .line 151
    iget-object v0, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->outputArray:[C

    invoke-static {v0}, Ljava/nio/CharBuffer;->wrap([C)Ljava/nio/CharBuffer;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->outputBuffer:Ljava/nio/CharBuffer;

    .line 157
    sget-object v0, Lorg/apache/tika/parser/rtf/TextExtractor;->WINDOWS_1252:Ljava/nio/charset/Charset;

    iput-object v0, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->globalCharset:Ljava/nio/charset/Charset;

    .line 158
    iput v1, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->globalDefaultFont:I

    .line 159
    iput v1, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->curFontID:I

    .line 165
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->fontToCharset:Ljava/util/Map;

    .line 170
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->groupStates:Ljava/util/LinkedList;

    .line 175
    new-instance v0, Lorg/apache/tika/parser/rtf/GroupState;

    invoke-direct {v0}, Lorg/apache/tika/parser/rtf/GroupState;-><init>()V

    iput-object v0, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->groupState:Lorg/apache/tika/parser/rtf/GroupState;

    .line 177
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->inHeader:Z

    .line 193
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->pendingBuffer:Ljava/lang/StringBuilder;

    .line 197
    iput v1, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->uprState:I

    .line 209
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->ansiSkip:I

    .line 324
    iput-object p2, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->metadata:Lorg/apache/tika/metadata/Metadata;

    .line 325
    iput-object p1, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->out:Lorg/apache/tika/sax/XHTMLContentHandler;

    .line 326
    return-void
.end method

.method private addControl(I)V
    .locals 7
    .param p1, "b"    # I

    .prologue
    const/4 v6, 0x0

    .line 386
    sget-boolean v1, Lorg/apache/tika/parser/rtf/TextExtractor;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    invoke-static {p1}, Lorg/apache/tika/parser/rtf/TextExtractor;->isAlpha(I)Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 388
    :cond_0
    iget v1, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->pendingControlCount:I

    iget-object v2, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->pendingControl:[B

    array-length v2, v2

    if-ne v1, v2, :cond_1

    .line 390
    iget-object v1, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->pendingControl:[B

    array-length v1, v1

    int-to-double v2, v1

    const-wide/high16 v4, 0x3ff4000000000000L    # 1.25

    mul-double/2addr v2, v4

    double-to-int v1, v2

    new-array v0, v1, [B

    .line 391
    .local v0, "newArray":[B
    iget-object v1, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->pendingControl:[B

    iget-object v2, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->pendingControl:[B

    array-length v2, v2

    invoke-static {v1, v6, v0, v6, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 392
    iput-object v0, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->pendingControl:[B

    .line 394
    .end local v0    # "newArray":[B
    :cond_1
    iget-object v1, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->pendingControl:[B

    iget v2, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->pendingControlCount:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->pendingControlCount:I

    int-to-byte v3, p1

    aput-byte v3, v1, v2

    .line 395
    return-void
.end method

.method private addOutputByte(I)V
    .locals 7
    .param p1, "b"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/xml/sax/SAXException;,
            Lorg/apache/tika/exception/TikaException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 367
    sget-boolean v1, Lorg/apache/tika/parser/rtf/TextExtractor;->$assertionsDisabled:Z

    if-nez v1, :cond_1

    if-ltz p1, :cond_0

    const/16 v1, 0x100

    if-lt p1, v1, :cond_1

    :cond_0
    new-instance v1, Ljava/lang/AssertionError;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "byte value out of range: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1

    .line 369
    :cond_1
    iget v1, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->pendingCharCount:I

    if-eqz v1, :cond_2

    .line 370
    invoke-direct {p0}, Lorg/apache/tika/parser/rtf/TextExtractor;->pushChars()V

    .line 374
    :cond_2
    iget v1, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->pendingByteCount:I

    iget-object v2, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->pendingBytes:[B

    array-length v2, v2

    if-ne v1, v2, :cond_3

    .line 376
    iget-object v1, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->pendingBytes:[B

    array-length v1, v1

    int-to-double v2, v1

    const-wide/high16 v4, 0x3ff4000000000000L    # 1.25

    mul-double/2addr v2, v4

    double-to-int v1, v2

    new-array v0, v1, [B

    .line 377
    .local v0, "newArray":[B
    iget-object v1, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->pendingBytes:[B

    iget-object v2, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->pendingBytes:[B

    array-length v2, v2

    invoke-static {v1, v6, v0, v6, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 378
    iput-object v0, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->pendingBytes:[B

    .line 379
    iget-object v1, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->pendingBytes:[B

    invoke-static {v1}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->pendingByteBuffer:Ljava/nio/ByteBuffer;

    .line 381
    .end local v0    # "newArray":[B
    :cond_3
    iget-object v1, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->pendingBytes:[B

    iget v2, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->pendingByteCount:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->pendingByteCount:I

    int-to-byte v3, p1

    aput-byte v3, v1, v2

    .line 382
    return-void
.end method

.method private addOutputChar(C)V
    .locals 7
    .param p1, "ch"    # C
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/xml/sax/SAXException;,
            Lorg/apache/tika/exception/TikaException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 399
    iget v1, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->pendingByteCount:I

    if-eqz v1, :cond_0

    .line 400
    invoke-direct {p0}, Lorg/apache/tika/parser/rtf/TextExtractor;->pushBytes()V

    .line 403
    :cond_0
    iget-boolean v1, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->inHeader:Z

    if-nez v1, :cond_1

    iget v1, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->fieldState:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_2

    .line 404
    :cond_1
    iget-object v1, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->pendingBuffer:Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 414
    :goto_0
    return-void

    .line 406
    :cond_2
    iget v1, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->pendingCharCount:I

    iget-object v2, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->pendingChars:[C

    array-length v2, v2

    if-ne v1, v2, :cond_3

    .line 408
    iget-object v1, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->pendingChars:[C

    array-length v1, v1

    int-to-double v2, v1

    const-wide/high16 v4, 0x3ff4000000000000L    # 1.25

    mul-double/2addr v2, v4

    double-to-int v1, v2

    new-array v0, v1, [C

    .line 409
    .local v0, "newArray":[C
    iget-object v1, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->pendingChars:[C

    iget-object v2, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->pendingChars:[C

    array-length v2, v2

    invoke-static {v1, v6, v0, v6, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 410
    iput-object v0, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->pendingChars:[C

    .line 412
    .end local v0    # "newArray":[C
    :cond_3
    iget-object v1, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->pendingChars:[C

    iget v2, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->pendingCharCount:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->pendingCharCount:I

    aput-char p1, v1, v2

    goto :goto_0
.end method

.method private end(Ljava/lang/String;)V
    .locals 1
    .param p1, "tag"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/xml/sax/SAXException;,
            Lorg/apache/tika/exception/TikaException;
        }
    .end annotation

    .prologue
    .line 906
    iget-object v0, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->out:Lorg/apache/tika/sax/XHTMLContentHandler;

    invoke-virtual {v0, p1}, Lorg/apache/tika/sax/XHTMLContentHandler;->endElement(Ljava/lang/String;)V

    .line 907
    return-void
.end method

.method private endParagraph(Z)V
    .locals 2
    .param p1, "preserveStyles"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/xml/sax/SAXException;,
            Lorg/apache/tika/exception/TikaException;
        }
    .end annotation

    .prologue
    .line 583
    invoke-direct {p0}, Lorg/apache/tika/parser/rtf/TextExtractor;->pushText()V

    .line 584
    iget-boolean v0, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->inParagraph:Z

    if-eqz v0, :cond_5

    .line 585
    iget-object v0, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->groupState:Lorg/apache/tika/parser/rtf/GroupState;

    iget-boolean v0, v0, Lorg/apache/tika/parser/rtf/GroupState;->italic:Z

    if-eqz v0, :cond_0

    .line 586
    const-string/jumbo v0, "i"

    invoke-direct {p0, v0}, Lorg/apache/tika/parser/rtf/TextExtractor;->end(Ljava/lang/String;)V

    .line 587
    iget-object v0, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->groupState:Lorg/apache/tika/parser/rtf/GroupState;

    iput-boolean p1, v0, Lorg/apache/tika/parser/rtf/GroupState;->italic:Z

    .line 589
    :cond_0
    iget-object v0, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->groupState:Lorg/apache/tika/parser/rtf/GroupState;

    iget-boolean v0, v0, Lorg/apache/tika/parser/rtf/GroupState;->bold:Z

    if-eqz v0, :cond_1

    .line 590
    const-string/jumbo v0, "b"

    invoke-direct {p0, v0}, Lorg/apache/tika/parser/rtf/TextExtractor;->end(Ljava/lang/String;)V

    .line 591
    iget-object v0, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->groupState:Lorg/apache/tika/parser/rtf/GroupState;

    iput-boolean p1, v0, Lorg/apache/tika/parser/rtf/GroupState;->bold:Z

    .line 593
    :cond_1
    iget-object v0, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->out:Lorg/apache/tika/sax/XHTMLContentHandler;

    const-string/jumbo v1, "p"

    invoke-virtual {v0, v1}, Lorg/apache/tika/sax/XHTMLContentHandler;->endElement(Ljava/lang/String;)V

    .line 594
    if-eqz p1, :cond_6

    iget-object v0, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->groupState:Lorg/apache/tika/parser/rtf/GroupState;

    iget-boolean v0, v0, Lorg/apache/tika/parser/rtf/GroupState;->bold:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->groupState:Lorg/apache/tika/parser/rtf/GroupState;

    iget-boolean v0, v0, Lorg/apache/tika/parser/rtf/GroupState;->italic:Z

    if-eqz v0, :cond_6

    .line 595
    :cond_2
    const-string/jumbo v0, "p"

    invoke-direct {p0, v0}, Lorg/apache/tika/parser/rtf/TextExtractor;->start(Ljava/lang/String;)V

    .line 596
    iget-object v0, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->groupState:Lorg/apache/tika/parser/rtf/GroupState;

    iget-boolean v0, v0, Lorg/apache/tika/parser/rtf/GroupState;->bold:Z

    if-eqz v0, :cond_3

    .line 597
    const-string/jumbo v0, "b"

    invoke-direct {p0, v0}, Lorg/apache/tika/parser/rtf/TextExtractor;->start(Ljava/lang/String;)V

    .line 599
    :cond_3
    iget-object v0, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->groupState:Lorg/apache/tika/parser/rtf/GroupState;

    iget-boolean v0, v0, Lorg/apache/tika/parser/rtf/GroupState;->italic:Z

    if-eqz v0, :cond_4

    .line 600
    const-string/jumbo v0, "i"

    invoke-direct {p0, v0}, Lorg/apache/tika/parser/rtf/TextExtractor;->start(Ljava/lang/String;)V

    .line 602
    :cond_4
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->inParagraph:Z

    .line 607
    :cond_5
    :goto_0
    return-void

    .line 604
    :cond_6
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->inParagraph:Z

    goto :goto_0
.end method

.method private equals(Ljava/lang/String;)Z
    .locals 4
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 682
    iget v2, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->pendingControlCount:I

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    if-eq v2, v3, :cond_1

    .line 691
    :cond_0
    :goto_0
    return v1

    .line 685
    :cond_1
    const/4 v0, 0x0

    .local v0, "idx":I
    :goto_1
    iget v2, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->pendingControlCount:I

    if-lt v0, v2, :cond_2

    .line 691
    const/4 v1, 0x1

    goto :goto_0

    .line 686
    :cond_2
    sget-boolean v2, Lorg/apache/tika/parser/rtf/TextExtractor;->$assertionsDisabled:Z

    if-nez v2, :cond_3

    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-static {v2}, Lorg/apache/tika/parser/rtf/TextExtractor;->isAlpha(I)Z

    move-result v2

    if-nez v2, :cond_3

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 687
    :cond_3
    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v2

    int-to-byte v2, v2

    iget-object v3, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->pendingControl:[B

    aget-byte v3, v3, v0

    if-ne v2, v3, :cond_0

    .line 685
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method private extract(Ljava/io/PushbackInputStream;)V
    .locals 3
    .param p1, "in"    # Ljava/io/PushbackInputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/xml/sax/SAXException;,
            Lorg/apache/tika/exception/TikaException;
        }
    .end annotation

    .prologue
    .line 437
    iget-object v1, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->out:Lorg/apache/tika/sax/XHTMLContentHandler;

    invoke-virtual {v1}, Lorg/apache/tika/sax/XHTMLContentHandler;->startDocument()V

    .line 440
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljava/io/PushbackInputStream;->read()I

    move-result v0

    .line 441
    .local v0, "b":I
    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    .line 468
    :goto_1
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lorg/apache/tika/parser/rtf/TextExtractor;->endParagraph(Z)V

    .line 469
    iget-object v1, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->out:Lorg/apache/tika/sax/XHTMLContentHandler;

    invoke-virtual {v1}, Lorg/apache/tika/sax/XHTMLContentHandler;->endDocument()V

    .line 470
    return-void

    .line 443
    :cond_1
    const/16 v1, 0x5c

    if-ne v0, v1, :cond_2

    .line 444
    invoke-direct {p0, p1}, Lorg/apache/tika/parser/rtf/TextExtractor;->parseControlToken(Ljava/io/PushbackInputStream;)V

    goto :goto_0

    .line 445
    :cond_2
    const/16 v1, 0x7b

    if-ne v0, v1, :cond_3

    .line 446
    invoke-direct {p0}, Lorg/apache/tika/parser/rtf/TextExtractor;->pushText()V

    .line 447
    invoke-direct {p0, p1}, Lorg/apache/tika/parser/rtf/TextExtractor;->processGroupStart(Ljava/io/PushbackInputStream;)V

    goto :goto_0

    .line 448
    :cond_3
    const/16 v1, 0x7d

    if-ne v0, v1, :cond_4

    .line 449
    invoke-direct {p0}, Lorg/apache/tika/parser/rtf/TextExtractor;->pushText()V

    .line 450
    invoke-direct {p0}, Lorg/apache/tika/parser/rtf/TextExtractor;->processGroupEnd()V

    .line 451
    iget-object v1, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->groupStates:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_1

    .line 455
    :cond_4
    const/16 v1, 0x3c

    if-ne v0, v1, :cond_5

    .line 456
    new-instance v1, Lcom/samsung/index/GeneralEncryptedDocException;

    const-string/jumbo v2, "Document is encrypted"

    invoke-direct {v1, v2}, Lcom/samsung/index/GeneralEncryptedDocException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 457
    :cond_5
    const/16 v1, 0xd

    if-eq v0, v1, :cond_0

    const/16 v1, 0xa

    if-eq v0, v1, :cond_0

    iget-object v1, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->groupState:Lorg/apache/tika/parser/rtf/GroupState;

    iget-boolean v1, v1, Lorg/apache/tika/parser/rtf/GroupState;->ignore:Z

    if-eqz v1, :cond_6

    iget-object v1, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->nextMetaData:Lorg/apache/tika/metadata/Property;

    if-eqz v1, :cond_0

    .line 460
    :cond_6
    iget v1, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->ansiSkip:I

    if-eqz v1, :cond_7

    .line 461
    iget v1, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->ansiSkip:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->ansiSkip:I

    goto :goto_0

    .line 463
    :cond_7
    invoke-direct {p0, v0}, Lorg/apache/tika/parser/rtf/TextExtractor;->addOutputByte(I)V

    goto :goto_0
.end method

.method private getCharset()Ljava/nio/charset/Charset;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tika/exception/TikaException;
        }
    .end annotation

    .prologue
    .line 736
    iget-object v1, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->groupState:Lorg/apache/tika/parser/rtf/GroupState;

    iget-object v1, v1, Lorg/apache/tika/parser/rtf/GroupState;->fontCharset:Ljava/nio/charset/Charset;

    if-eqz v1, :cond_1

    .line 737
    iget-object v1, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->groupState:Lorg/apache/tika/parser/rtf/GroupState;

    iget-object v0, v1, Lorg/apache/tika/parser/rtf/GroupState;->fontCharset:Ljava/nio/charset/Charset;

    .line 753
    :cond_0
    :goto_0
    return-object v0

    .line 741
    :cond_1
    iget v1, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->globalDefaultFont:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_2

    iget-boolean v1, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->inHeader:Z

    if-nez v1, :cond_2

    .line 742
    iget-object v1, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->fontToCharset:Ljava/util/Map;

    iget v2, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->globalDefaultFont:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/nio/charset/Charset;

    .line 743
    .local v0, "cs":Ljava/nio/charset/Charset;
    if-nez v0, :cond_0

    .line 749
    .end local v0    # "cs":Ljava/nio/charset/Charset;
    :cond_2
    iget-object v1, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->globalCharset:Ljava/nio/charset/Charset;

    if-nez v1, :cond_3

    .line 750
    new-instance v1, Lorg/apache/tika/exception/TikaException;

    const-string/jumbo v2, "unable to determine charset"

    invoke-direct {v1, v2}, Lorg/apache/tika/exception/TikaException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 753
    :cond_3
    iget-object v0, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->globalCharset:Ljava/nio/charset/Charset;

    goto :goto_0
.end method

.method private static getCharset(Ljava/lang/String;)Ljava/nio/charset/Charset;
    .locals 2
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 60
    :try_start_0
    invoke-static {p0}, Lorg/apache/tika/utils/CharsetUtils;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 62
    :goto_0
    return-object v1

    .line 61
    :catch_0
    move-exception v0

    .line 62
    .local v0, "e":Ljava/lang/Exception;
    sget-object v1, Lorg/apache/tika/parser/rtf/TextExtractor;->ASCII:Ljava/nio/charset/Charset;

    goto :goto_0
.end method

.method private getDecoder()Ljava/nio/charset/CharsetDecoder;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tika/exception/TikaException;
        }
    .end annotation

    .prologue
    .line 719
    invoke-direct {p0}, Lorg/apache/tika/parser/rtf/TextExtractor;->getCharset()Ljava/nio/charset/Charset;

    move-result-object v0

    .line 723
    .local v0, "charset":Ljava/nio/charset/Charset;
    iget-object v1, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->lastCharset:Ljava/nio/charset/Charset;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->lastCharset:Ljava/nio/charset/Charset;

    invoke-virtual {v0, v1}, Ljava/nio/charset/Charset;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 724
    :cond_0
    invoke-virtual {v0}, Ljava/nio/charset/Charset;->newDecoder()Ljava/nio/charset/CharsetDecoder;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->decoder:Ljava/nio/charset/CharsetDecoder;

    .line 725
    iget-object v1, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->decoder:Ljava/nio/charset/CharsetDecoder;

    sget-object v2, Ljava/nio/charset/CodingErrorAction;->REPLACE:Ljava/nio/charset/CodingErrorAction;

    invoke-virtual {v1, v2}, Ljava/nio/charset/CharsetDecoder;->onMalformedInput(Ljava/nio/charset/CodingErrorAction;)Ljava/nio/charset/CharsetDecoder;

    .line 726
    iget-object v1, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->decoder:Ljava/nio/charset/CharsetDecoder;

    sget-object v2, Ljava/nio/charset/CodingErrorAction;->REPLACE:Ljava/nio/charset/CodingErrorAction;

    invoke-virtual {v1, v2}, Ljava/nio/charset/CharsetDecoder;->onUnmappableCharacter(Ljava/nio/charset/CodingErrorAction;)Ljava/nio/charset/CharsetDecoder;

    .line 727
    iput-object v0, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->lastCharset:Ljava/nio/charset/Charset;

    .line 730
    :cond_1
    iget-object v1, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->decoder:Ljava/nio/charset/CharsetDecoder;

    return-object v1
.end method

.method private static hexValue(I)I
    .locals 1
    .param p0, "ch"    # I

    .prologue
    .line 344
    const/16 v0, 0x30

    if-lt p0, v0, :cond_0

    const/16 v0, 0x39

    if-gt p0, v0, :cond_0

    .line 345
    add-int/lit8 v0, p0, -0x30

    .line 350
    :goto_0
    return v0

    .line 346
    :cond_0
    const/16 v0, 0x61

    if-lt p0, v0, :cond_1

    const/16 v0, 0x7a

    if-gt p0, v0, :cond_1

    .line 347
    add-int/lit8 v0, p0, -0x61

    add-int/lit8 v0, v0, 0xa

    goto :goto_0

    .line 349
    :cond_1
    sget-boolean v0, Lorg/apache/tika/parser/rtf/TextExtractor;->$assertionsDisabled:Z

    if-nez v0, :cond_3

    const/16 v0, 0x41

    if-lt p0, v0, :cond_2

    const/16 v0, 0x5a

    if-le p0, v0, :cond_3

    :cond_2
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 350
    :cond_3
    add-int/lit8 v0, p0, -0x41

    add-int/lit8 v0, v0, 0xa

    goto :goto_0
.end method

.method private static isAlpha(I)Z
    .locals 1
    .param p0, "ch"    # I

    .prologue
    .line 335
    const/16 v0, 0x61

    if-lt p0, v0, :cond_0

    const/16 v0, 0x7a

    if-le p0, v0, :cond_2

    .line 336
    :cond_0
    const/16 v0, 0x41

    if-lt p0, v0, :cond_1

    const/16 v0, 0x5a

    .line 335
    if-le p0, v0, :cond_2

    :cond_1
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private static isDigit(I)Z
    .locals 1
    .param p0, "ch"    # I

    .prologue
    .line 340
    const/16 v0, 0x30

    if-lt p0, v0, :cond_0

    const/16 v0, 0x39

    if-gt p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static isHexChar(I)Z
    .locals 1
    .param p0, "ch"    # I

    .prologue
    .line 329
    const/16 v0, 0x30

    if-lt p0, v0, :cond_0

    const/16 v0, 0x39

    if-le p0, v0, :cond_3

    .line 330
    :cond_0
    const/16 v0, 0x61

    if-lt p0, v0, :cond_1

    const/16 v0, 0x66

    if-le p0, v0, :cond_3

    .line 331
    :cond_1
    const/16 v0, 0x41

    if-lt p0, v0, :cond_2

    const/16 v0, 0x46

    .line 329
    if-le p0, v0, :cond_3

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private lazyStartParagraph()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/xml/sax/SAXException;,
            Lorg/apache/tika/exception/TikaException;
        }
    .end annotation

    .prologue
    .line 562
    iget-boolean v0, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->inParagraph:Z

    if-nez v0, :cond_4

    .line 564
    iget-object v0, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->groupState:Lorg/apache/tika/parser/rtf/GroupState;

    iget-boolean v0, v0, Lorg/apache/tika/parser/rtf/GroupState;->italic:Z

    if-eqz v0, :cond_0

    .line 565
    const-string/jumbo v0, "i"

    invoke-direct {p0, v0}, Lorg/apache/tika/parser/rtf/TextExtractor;->end(Ljava/lang/String;)V

    .line 567
    :cond_0
    iget-object v0, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->groupState:Lorg/apache/tika/parser/rtf/GroupState;

    iget-boolean v0, v0, Lorg/apache/tika/parser/rtf/GroupState;->bold:Z

    if-eqz v0, :cond_1

    .line 568
    const-string/jumbo v0, "b"

    invoke-direct {p0, v0}, Lorg/apache/tika/parser/rtf/TextExtractor;->end(Ljava/lang/String;)V

    .line 570
    :cond_1
    iget-object v0, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->out:Lorg/apache/tika/sax/XHTMLContentHandler;

    const-string/jumbo v1, "p"

    invoke-virtual {v0, v1}, Lorg/apache/tika/sax/XHTMLContentHandler;->startElement(Ljava/lang/String;)V

    .line 572
    iget-object v0, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->groupState:Lorg/apache/tika/parser/rtf/GroupState;

    iget-boolean v0, v0, Lorg/apache/tika/parser/rtf/GroupState;->bold:Z

    if-eqz v0, :cond_2

    .line 573
    const-string/jumbo v0, "b"

    invoke-direct {p0, v0}, Lorg/apache/tika/parser/rtf/TextExtractor;->start(Ljava/lang/String;)V

    .line 575
    :cond_2
    iget-object v0, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->groupState:Lorg/apache/tika/parser/rtf/GroupState;

    iget-boolean v0, v0, Lorg/apache/tika/parser/rtf/GroupState;->italic:Z

    if-eqz v0, :cond_3

    .line 576
    const-string/jumbo v0, "i"

    invoke-direct {p0, v0}, Lorg/apache/tika/parser/rtf/TextExtractor;->start(Ljava/lang/String;)V

    .line 578
    :cond_3
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->inParagraph:Z

    .line 580
    :cond_4
    return-void
.end method

.method private parseControlToken(Ljava/io/PushbackInputStream;)V
    .locals 2
    .param p1, "in"    # Ljava/io/PushbackInputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/xml/sax/SAXException;,
            Lorg/apache/tika/exception/TikaException;
        }
    .end annotation

    .prologue
    .line 473
    invoke-virtual {p1}, Ljava/io/PushbackInputStream;->read()I

    move-result v0

    .line 474
    .local v0, "b":I
    const/16 v1, 0x27

    if-ne v0, v1, :cond_1

    .line 476
    invoke-direct {p0, p1}, Lorg/apache/tika/parser/rtf/TextExtractor;->parseHexChar(Ljava/io/PushbackInputStream;)V

    .line 487
    :cond_0
    :goto_0
    return-void

    .line 477
    :cond_1
    invoke-static {v0}, Lorg/apache/tika/parser/rtf/TextExtractor;->isAlpha(I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 479
    int-to-char v1, v0

    invoke-direct {p0, v1, p1}, Lorg/apache/tika/parser/rtf/TextExtractor;->parseControlWord(ILjava/io/PushbackInputStream;)V

    goto :goto_0

    .line 480
    :cond_2
    const/16 v1, 0x7b

    if-eq v0, v1, :cond_3

    const/16 v1, 0x7d

    if-eq v0, v1, :cond_3

    const/16 v1, 0x5c

    if-eq v0, v1, :cond_3

    const/16 v1, 0xd

    if-eq v0, v1, :cond_3

    const/16 v1, 0xa

    if-ne v0, v1, :cond_4

    .line 482
    :cond_3
    invoke-direct {p0, v0}, Lorg/apache/tika/parser/rtf/TextExtractor;->addOutputByte(I)V

    goto :goto_0

    .line 483
    :cond_4
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 485
    int-to-char v1, v0

    invoke-direct {p0, v1}, Lorg/apache/tika/parser/rtf/TextExtractor;->processControlSymbol(C)V

    goto :goto_0
.end method

.method private parseControlWord(ILjava/io/PushbackInputStream;)V
    .locals 5
    .param p1, "firstChar"    # I
    .param p2, "in"    # Ljava/io/PushbackInputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/xml/sax/SAXException;,
            Lorg/apache/tika/exception/TikaException;
        }
    .end annotation

    .prologue
    .line 518
    invoke-direct {p0, p1}, Lorg/apache/tika/parser/rtf/TextExtractor;->addControl(I)V

    .line 520
    invoke-virtual {p2}, Ljava/io/PushbackInputStream;->read()I

    move-result v0

    .line 521
    .local v0, "b":I
    :goto_0
    invoke-static {v0}, Lorg/apache/tika/parser/rtf/TextExtractor;->isAlpha(I)Z

    move-result v4

    if-nez v4, :cond_3

    .line 526
    const/4 v1, 0x0

    .line 527
    .local v1, "hasParam":Z
    const/4 v2, 0x0

    .line 528
    .local v2, "negParam":Z
    const/16 v4, 0x2d

    if-ne v0, v4, :cond_0

    .line 529
    const/4 v2, 0x1

    .line 530
    const/4 v1, 0x1

    .line 531
    invoke-virtual {p2}, Ljava/io/PushbackInputStream;->read()I

    move-result v0

    .line 534
    :cond_0
    const/4 v3, 0x0

    .line 535
    .local v3, "param":I
    :goto_1
    invoke-static {v0}, Lorg/apache/tika/parser/rtf/TextExtractor;->isDigit(I)Z

    move-result v4

    if-nez v4, :cond_4

    .line 545
    const/16 v4, 0x20

    if-eq v0, v4, :cond_1

    .line 546
    invoke-virtual {p2, v0}, Ljava/io/PushbackInputStream;->unread(I)V

    .line 549
    :cond_1
    if-eqz v1, :cond_5

    .line 550
    if-eqz v2, :cond_2

    .line 551
    neg-int v3, v3

    .line 553
    :cond_2
    invoke-direct {p0, v3, p2}, Lorg/apache/tika/parser/rtf/TextExtractor;->processControlWord(ILjava/io/PushbackInputStream;)V

    .line 558
    :goto_2
    const/4 v4, 0x0

    iput v4, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->pendingControlCount:I

    .line 559
    return-void

    .line 522
    .end local v1    # "hasParam":Z
    .end local v2    # "negParam":Z
    .end local v3    # "param":I
    :cond_3
    invoke-direct {p0, v0}, Lorg/apache/tika/parser/rtf/TextExtractor;->addControl(I)V

    .line 523
    invoke-virtual {p2}, Ljava/io/PushbackInputStream;->read()I

    move-result v0

    goto :goto_0

    .line 536
    .restart local v1    # "hasParam":Z
    .restart local v2    # "negParam":Z
    .restart local v3    # "param":I
    :cond_4
    mul-int/lit8 v3, v3, 0xa

    .line 537
    add-int/lit8 v4, v0, -0x30

    add-int/2addr v3, v4

    .line 538
    const/4 v1, 0x1

    .line 539
    invoke-virtual {p2}, Ljava/io/PushbackInputStream;->read()I

    move-result v0

    goto :goto_1

    .line 555
    :cond_5
    invoke-direct {p0}, Lorg/apache/tika/parser/rtf/TextExtractor;->processControlWord()V

    goto :goto_2
.end method

.method private parseHexChar(Ljava/io/PushbackInputStream;)V
    .locals 4
    .param p1, "in"    # Ljava/io/PushbackInputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/xml/sax/SAXException;,
            Lorg/apache/tika/exception/TikaException;
        }
    .end annotation

    .prologue
    .line 490
    invoke-virtual {p1}, Ljava/io/PushbackInputStream;->read()I

    move-result v0

    .line 491
    .local v0, "hex1":I
    invoke-static {v0}, Lorg/apache/tika/parser/rtf/TextExtractor;->isHexChar(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 493
    invoke-virtual {p1, v0}, Ljava/io/PushbackInputStream;->unread(I)V

    .line 515
    :goto_0
    return-void

    .line 497
    :cond_0
    invoke-virtual {p1}, Ljava/io/PushbackInputStream;->read()I

    move-result v1

    .line 498
    .local v1, "hex2":I
    invoke-static {v1}, Lorg/apache/tika/parser/rtf/TextExtractor;->isHexChar(I)Z

    move-result v2

    if-nez v2, :cond_1

    .line 502
    invoke-virtual {p1, v1}, Ljava/io/PushbackInputStream;->unread(I)V

    goto :goto_0

    .line 506
    :cond_1
    iget v2, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->ansiSkip:I

    if-eqz v2, :cond_2

    .line 510
    iget v2, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->ansiSkip:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->ansiSkip:I

    goto :goto_0

    .line 513
    :cond_2
    invoke-static {v0}, Lorg/apache/tika/parser/rtf/TextExtractor;->hexValue(I)I

    move-result v2

    mul-int/lit8 v2, v2, 0x10

    invoke-static {v1}, Lorg/apache/tika/parser/rtf/TextExtractor;->hexValue(I)I

    move-result v3

    add-int/2addr v2, v3

    invoke-direct {p0, v2}, Lorg/apache/tika/parser/rtf/TextExtractor;->addOutputByte(I)V

    goto :goto_0
.end method

.method private processControlSymbol(C)V
    .locals 1
    .param p1, "ch"    # C
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/xml/sax/SAXException;,
            Lorg/apache/tika/exception/TikaException;
        }
    .end annotation

    .prologue
    .line 695
    sparse-switch p1, :sswitch_data_0

    .line 716
    :goto_0
    :sswitch_0
    return-void

    .line 698
    :sswitch_1
    const/16 v0, 0xa0

    invoke-direct {p0, v0}, Lorg/apache/tika/parser/rtf/TextExtractor;->addOutputChar(C)V

    goto :goto_0

    .line 707
    :sswitch_2
    const/16 v0, 0xad

    invoke-direct {p0, v0}, Lorg/apache/tika/parser/rtf/TextExtractor;->addOutputChar(C)V

    goto :goto_0

    .line 711
    :sswitch_3
    const/16 v0, 0x2011

    invoke-direct {p0, v0}, Lorg/apache/tika/parser/rtf/TextExtractor;->addOutputChar(C)V

    goto :goto_0

    .line 695
    :sswitch_data_0
    .sparse-switch
        0x2a -> :sswitch_0
        0x2d -> :sswitch_2
        0x5f -> :sswitch_3
        0x7e -> :sswitch_1
    .end sparse-switch
.end method

.method private processControlWord()V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/xml/sax/SAXException;,
            Lorg/apache/tika/exception/TikaException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x2

    const/4 v6, -0x1

    const/16 v4, 0xa

    const/4 v3, 0x1

    const/4 v5, 0x0

    .line 915
    iget-boolean v1, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->inHeader:Z

    if-eqz v1, :cond_16

    .line 916
    const-string/jumbo v1, "ansi"

    invoke-direct {p0, v1}, Lorg/apache/tika/parser/rtf/TextExtractor;->equals(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 917
    sget-object v1, Lorg/apache/tika/parser/rtf/TextExtractor;->WINDOWS_1252:Ljava/nio/charset/Charset;

    iput-object v1, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->globalCharset:Ljava/nio/charset/Charset;

    .line 926
    :cond_0
    :goto_0
    const-string/jumbo v1, "colortbl"

    invoke-direct {p0, v1}, Lorg/apache/tika/parser/rtf/TextExtractor;->equals(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string/jumbo v1, "stylesheet"

    invoke-direct {p0, v1}, Lorg/apache/tika/parser/rtf/TextExtractor;->equals(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string/jumbo v1, "fonttbl"

    invoke-direct {p0, v1}, Lorg/apache/tika/parser/rtf/TextExtractor;->equals(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 927
    :cond_1
    iget-object v1, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->groupState:Lorg/apache/tika/parser/rtf/GroupState;

    iput-boolean v3, v1, Lorg/apache/tika/parser/rtf/GroupState;->ignore:Z

    .line 930
    :cond_2
    iget v1, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->uprState:I

    if-ne v1, v6, :cond_3

    .line 933
    const-string/jumbo v1, "author"

    invoke-direct {p0, v1}, Lorg/apache/tika/parser/rtf/TextExtractor;->equals(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 934
    sget-object v1, Lorg/apache/tika/metadata/TikaCoreProperties;->CREATOR:Lorg/apache/tika/metadata/Property;

    iput-object v1, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->nextMetaData:Lorg/apache/tika/metadata/Property;

    .line 957
    :cond_3
    :goto_1
    iget v1, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->fontTableState:I

    if-nez v1, :cond_15

    .line 959
    const-string/jumbo v1, "fonttbl"

    invoke-direct {p0, v1}, Lorg/apache/tika/parser/rtf/TextExtractor;->equals(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 960
    iput v3, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->fontTableState:I

    .line 961
    iget-object v1, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->groupState:Lorg/apache/tika/parser/rtf/GroupState;

    iget v1, v1, Lorg/apache/tika/parser/rtf/GroupState;->depth:I

    iput v1, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->fontTableDepth:I

    .line 970
    :cond_4
    :goto_2
    iget-object v1, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->groupState:Lorg/apache/tika/parser/rtf/GroupState;

    iget-boolean v1, v1, Lorg/apache/tika/parser/rtf/GroupState;->ignore:Z

    if-nez v1, :cond_6

    const-string/jumbo v1, "par"

    invoke-direct {p0, v1}, Lorg/apache/tika/parser/rtf/TextExtractor;->equals(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_5

    const-string/jumbo v1, "pard"

    invoke-direct {p0, v1}, Lorg/apache/tika/parser/rtf/TextExtractor;->equals(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_5

    const-string/jumbo v1, "sect"

    invoke-direct {p0, v1}, Lorg/apache/tika/parser/rtf/TextExtractor;->equals(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_5

    const-string/jumbo v1, "sectd"

    invoke-direct {p0, v1}, Lorg/apache/tika/parser/rtf/TextExtractor;->equals(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_5

    const-string/jumbo v1, "plain"

    invoke-direct {p0, v1}, Lorg/apache/tika/parser/rtf/TextExtractor;->equals(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_5

    const-string/jumbo v1, "ltrch"

    invoke-direct {p0, v1}, Lorg/apache/tika/parser/rtf/TextExtractor;->equals(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_5

    const-string/jumbo v1, "rtlch"

    invoke-direct {p0, v1}, Lorg/apache/tika/parser/rtf/TextExtractor;->equals(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 971
    :cond_5
    iput-boolean v5, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->inHeader:Z

    .line 998
    :cond_6
    :goto_3
    iget-object v1, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->groupState:Lorg/apache/tika/parser/rtf/GroupState;

    iget-boolean v0, v1, Lorg/apache/tika/parser/rtf/GroupState;->ignore:Z

    .line 1000
    .local v0, "ignored":Z
    const-string/jumbo v1, "pard"

    invoke-direct {p0, v1}, Lorg/apache/tika/parser/rtf/TextExtractor;->equals(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_19

    .line 1002
    invoke-direct {p0}, Lorg/apache/tika/parser/rtf/TextExtractor;->pushText()V

    .line 1003
    iget-object v1, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->groupState:Lorg/apache/tika/parser/rtf/GroupState;

    iget-boolean v1, v1, Lorg/apache/tika/parser/rtf/GroupState;->italic:Z

    if-eqz v1, :cond_7

    .line 1004
    const-string/jumbo v1, "i"

    invoke-direct {p0, v1}, Lorg/apache/tika/parser/rtf/TextExtractor;->end(Ljava/lang/String;)V

    .line 1005
    iget-object v1, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->groupState:Lorg/apache/tika/parser/rtf/GroupState;

    iput-boolean v5, v1, Lorg/apache/tika/parser/rtf/GroupState;->italic:Z

    .line 1007
    :cond_7
    iget-object v1, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->groupState:Lorg/apache/tika/parser/rtf/GroupState;

    iget-boolean v1, v1, Lorg/apache/tika/parser/rtf/GroupState;->bold:Z

    if-eqz v1, :cond_8

    .line 1008
    const-string/jumbo v1, "b"

    invoke-direct {p0, v1}, Lorg/apache/tika/parser/rtf/TextExtractor;->end(Ljava/lang/String;)V

    .line 1009
    iget-object v1, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->groupState:Lorg/apache/tika/parser/rtf/GroupState;

    iput-boolean v5, v1, Lorg/apache/tika/parser/rtf/GroupState;->bold:Z

    .line 1137
    :cond_8
    :goto_4
    return-void

    .line 918
    .end local v0    # "ignored":Z
    :cond_9
    const-string/jumbo v1, "pca"

    invoke-direct {p0, v1}, Lorg/apache/tika/parser/rtf/TextExtractor;->equals(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 919
    sget-object v1, Lorg/apache/tika/parser/rtf/TextExtractor;->CP850:Ljava/nio/charset/Charset;

    iput-object v1, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->globalCharset:Ljava/nio/charset/Charset;

    goto/16 :goto_0

    .line 920
    :cond_a
    const-string/jumbo v1, "pc"

    invoke-direct {p0, v1}, Lorg/apache/tika/parser/rtf/TextExtractor;->equals(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 921
    sget-object v1, Lorg/apache/tika/parser/rtf/TextExtractor;->CP437:Ljava/nio/charset/Charset;

    iput-object v1, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->globalCharset:Ljava/nio/charset/Charset;

    goto/16 :goto_0

    .line 922
    :cond_b
    const-string/jumbo v1, "mac"

    invoke-direct {p0, v1}, Lorg/apache/tika/parser/rtf/TextExtractor;->equals(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 923
    sget-object v1, Lorg/apache/tika/parser/rtf/TextExtractor;->MAC_ROMAN:Ljava/nio/charset/Charset;

    iput-object v1, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->globalCharset:Ljava/nio/charset/Charset;

    goto/16 :goto_0

    .line 935
    :cond_c
    const-string/jumbo v1, "title"

    invoke-direct {p0, v1}, Lorg/apache/tika/parser/rtf/TextExtractor;->equals(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_d

    .line 936
    sget-object v1, Lorg/apache/tika/metadata/TikaCoreProperties;->TITLE:Lorg/apache/tika/metadata/Property;

    iput-object v1, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->nextMetaData:Lorg/apache/tika/metadata/Property;

    goto/16 :goto_1

    .line 937
    :cond_d
    const-string/jumbo v1, "subject"

    invoke-direct {p0, v1}, Lorg/apache/tika/parser/rtf/TextExtractor;->equals(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_e

    .line 939
    sget-object v1, Lorg/apache/tika/metadata/TikaCoreProperties;->TRANSITION_SUBJECT_TO_OO_SUBJECT:Lorg/apache/tika/metadata/Property;

    iput-object v1, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->nextMetaData:Lorg/apache/tika/metadata/Property;

    goto/16 :goto_1

    .line 940
    :cond_e
    const-string/jumbo v1, "keywords"

    invoke-direct {p0, v1}, Lorg/apache/tika/parser/rtf/TextExtractor;->equals(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_f

    .line 941
    sget-object v1, Lorg/apache/tika/metadata/TikaCoreProperties;->TRANSITION_KEYWORDS_TO_DC_SUBJECT:Lorg/apache/tika/metadata/Property;

    iput-object v1, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->nextMetaData:Lorg/apache/tika/metadata/Property;

    goto/16 :goto_1

    .line 942
    :cond_f
    const-string/jumbo v1, "category"

    invoke-direct {p0, v1}, Lorg/apache/tika/parser/rtf/TextExtractor;->equals(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_10

    .line 943
    sget-object v1, Lorg/apache/tika/metadata/OfficeOpenXMLCore;->CATEGORY:Lorg/apache/tika/metadata/Property;

    iput-object v1, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->nextMetaData:Lorg/apache/tika/metadata/Property;

    goto/16 :goto_1

    .line 944
    :cond_10
    const-string/jumbo v1, "comment"

    invoke-direct {p0, v1}, Lorg/apache/tika/parser/rtf/TextExtractor;->equals(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_11

    .line 945
    sget-object v1, Lorg/apache/tika/metadata/TikaCoreProperties;->COMMENTS:Lorg/apache/tika/metadata/Property;

    iput-object v1, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->nextMetaData:Lorg/apache/tika/metadata/Property;

    goto/16 :goto_1

    .line 946
    :cond_11
    const-string/jumbo v1, "company"

    invoke-direct {p0, v1}, Lorg/apache/tika/parser/rtf/TextExtractor;->equals(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_12

    .line 947
    sget-object v1, Lorg/apache/tika/metadata/OfficeOpenXMLExtended;->COMPANY:Lorg/apache/tika/metadata/Property;

    iput-object v1, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->nextMetaData:Lorg/apache/tika/metadata/Property;

    goto/16 :goto_1

    .line 948
    :cond_12
    const-string/jumbo v1, "manager"

    invoke-direct {p0, v1}, Lorg/apache/tika/parser/rtf/TextExtractor;->equals(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_13

    .line 949
    sget-object v1, Lorg/apache/tika/metadata/OfficeOpenXMLExtended;->MANAGER:Lorg/apache/tika/metadata/Property;

    iput-object v1, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->nextMetaData:Lorg/apache/tika/metadata/Property;

    goto/16 :goto_1

    .line 950
    :cond_13
    const-string/jumbo v1, "template"

    invoke-direct {p0, v1}, Lorg/apache/tika/parser/rtf/TextExtractor;->equals(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_14

    .line 951
    sget-object v1, Lorg/apache/tika/metadata/OfficeOpenXMLExtended;->TEMPLATE:Lorg/apache/tika/metadata/Property;

    iput-object v1, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->nextMetaData:Lorg/apache/tika/metadata/Property;

    goto/16 :goto_1

    .line 952
    :cond_14
    const-string/jumbo v1, "creatim"

    invoke-direct {p0, v1}, Lorg/apache/tika/parser/rtf/TextExtractor;->equals(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 953
    sget-object v1, Lorg/apache/tika/metadata/TikaCoreProperties;->CREATED:Lorg/apache/tika/metadata/Property;

    iput-object v1, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->nextMetaData:Lorg/apache/tika/metadata/Property;

    goto/16 :goto_1

    .line 963
    :cond_15
    iget v1, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->fontTableState:I

    if-ne v1, v3, :cond_4

    .line 965
    iget-object v1, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->groupState:Lorg/apache/tika/parser/rtf/GroupState;

    iget v1, v1, Lorg/apache/tika/parser/rtf/GroupState;->depth:I

    iget v2, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->fontTableDepth:I

    if-ge v1, v2, :cond_4

    .line 966
    iput v7, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->fontTableState:I

    goto/16 :goto_2

    .line 974
    :cond_16
    const-string/jumbo v1, "b"

    invoke-direct {p0, v1}, Lorg/apache/tika/parser/rtf/TextExtractor;->equals(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_18

    .line 975
    iget-object v1, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->groupState:Lorg/apache/tika/parser/rtf/GroupState;

    iget-boolean v1, v1, Lorg/apache/tika/parser/rtf/GroupState;->bold:Z

    if-nez v1, :cond_6

    .line 976
    invoke-direct {p0}, Lorg/apache/tika/parser/rtf/TextExtractor;->pushText()V

    .line 977
    invoke-direct {p0}, Lorg/apache/tika/parser/rtf/TextExtractor;->lazyStartParagraph()V

    .line 978
    iget-object v1, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->groupState:Lorg/apache/tika/parser/rtf/GroupState;

    iget-boolean v1, v1, Lorg/apache/tika/parser/rtf/GroupState;->italic:Z

    if-eqz v1, :cond_17

    .line 980
    const-string/jumbo v1, "i"

    invoke-direct {p0, v1}, Lorg/apache/tika/parser/rtf/TextExtractor;->end(Ljava/lang/String;)V

    .line 982
    :cond_17
    iget-object v1, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->groupState:Lorg/apache/tika/parser/rtf/GroupState;

    iput-boolean v3, v1, Lorg/apache/tika/parser/rtf/GroupState;->bold:Z

    .line 983
    const-string/jumbo v1, "b"

    invoke-direct {p0, v1}, Lorg/apache/tika/parser/rtf/TextExtractor;->start(Ljava/lang/String;)V

    .line 984
    iget-object v1, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->groupState:Lorg/apache/tika/parser/rtf/GroupState;

    iget-boolean v1, v1, Lorg/apache/tika/parser/rtf/GroupState;->italic:Z

    if-eqz v1, :cond_6

    .line 985
    const-string/jumbo v1, "i"

    invoke-direct {p0, v1}, Lorg/apache/tika/parser/rtf/TextExtractor;->start(Ljava/lang/String;)V

    goto/16 :goto_3

    .line 988
    :cond_18
    const-string/jumbo v1, "i"

    invoke-direct {p0, v1}, Lorg/apache/tika/parser/rtf/TextExtractor;->equals(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 989
    iget-object v1, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->groupState:Lorg/apache/tika/parser/rtf/GroupState;

    iget-boolean v1, v1, Lorg/apache/tika/parser/rtf/GroupState;->italic:Z

    if-nez v1, :cond_6

    .line 990
    invoke-direct {p0}, Lorg/apache/tika/parser/rtf/TextExtractor;->pushText()V

    .line 991
    invoke-direct {p0}, Lorg/apache/tika/parser/rtf/TextExtractor;->lazyStartParagraph()V

    .line 992
    iget-object v1, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->groupState:Lorg/apache/tika/parser/rtf/GroupState;

    iput-boolean v3, v1, Lorg/apache/tika/parser/rtf/GroupState;->italic:Z

    .line 993
    const-string/jumbo v1, "i"

    invoke-direct {p0, v1}, Lorg/apache/tika/parser/rtf/TextExtractor;->start(Ljava/lang/String;)V

    goto/16 :goto_3

    .line 1011
    .restart local v0    # "ignored":Z
    :cond_19
    const-string/jumbo v1, "par"

    invoke-direct {p0, v1}, Lorg/apache/tika/parser/rtf/TextExtractor;->equals(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1a

    .line 1012
    if-nez v0, :cond_8

    .line 1013
    invoke-direct {p0, v3}, Lorg/apache/tika/parser/rtf/TextExtractor;->endParagraph(Z)V

    goto/16 :goto_4

    .line 1015
    :cond_1a
    const-string/jumbo v1, "shptxt"

    invoke-direct {p0, v1}, Lorg/apache/tika/parser/rtf/TextExtractor;->equals(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1b

    .line 1016
    invoke-direct {p0}, Lorg/apache/tika/parser/rtf/TextExtractor;->pushText()V

    .line 1018
    iget-object v1, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->groupState:Lorg/apache/tika/parser/rtf/GroupState;

    iput-boolean v5, v1, Lorg/apache/tika/parser/rtf/GroupState;->ignore:Z

    goto/16 :goto_4

    .line 1019
    :cond_1b
    const-string/jumbo v1, "atnid"

    invoke-direct {p0, v1}, Lorg/apache/tika/parser/rtf/TextExtractor;->equals(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1c

    .line 1020
    invoke-direct {p0}, Lorg/apache/tika/parser/rtf/TextExtractor;->pushText()V

    .line 1022
    iget-object v1, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->groupState:Lorg/apache/tika/parser/rtf/GroupState;

    iput-boolean v5, v1, Lorg/apache/tika/parser/rtf/GroupState;->ignore:Z

    goto/16 :goto_4

    .line 1023
    :cond_1c
    const-string/jumbo v1, "atnauthor"

    invoke-direct {p0, v1}, Lorg/apache/tika/parser/rtf/TextExtractor;->equals(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1d

    .line 1024
    invoke-direct {p0}, Lorg/apache/tika/parser/rtf/TextExtractor;->pushText()V

    .line 1026
    iget-object v1, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->groupState:Lorg/apache/tika/parser/rtf/GroupState;

    iput-boolean v5, v1, Lorg/apache/tika/parser/rtf/GroupState;->ignore:Z

    goto/16 :goto_4

    .line 1027
    :cond_1d
    const-string/jumbo v1, "annotation"

    invoke-direct {p0, v1}, Lorg/apache/tika/parser/rtf/TextExtractor;->equals(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1e

    .line 1028
    invoke-direct {p0}, Lorg/apache/tika/parser/rtf/TextExtractor;->pushText()V

    .line 1030
    iget-object v1, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->groupState:Lorg/apache/tika/parser/rtf/GroupState;

    iput-boolean v5, v1, Lorg/apache/tika/parser/rtf/GroupState;->ignore:Z

    goto/16 :goto_4

    .line 1031
    :cond_1e
    const-string/jumbo v1, "cell"

    invoke-direct {p0, v1}, Lorg/apache/tika/parser/rtf/TextExtractor;->equals(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1f

    .line 1034
    invoke-direct {p0, v3}, Lorg/apache/tika/parser/rtf/TextExtractor;->endParagraph(Z)V

    goto/16 :goto_4

    .line 1035
    :cond_1f
    const-string/jumbo v1, "pict"

    invoke-direct {p0, v1}, Lorg/apache/tika/parser/rtf/TextExtractor;->equals(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_20

    .line 1036
    invoke-direct {p0}, Lorg/apache/tika/parser/rtf/TextExtractor;->pushText()V

    .line 1039
    iget-object v1, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->groupState:Lorg/apache/tika/parser/rtf/GroupState;

    iput-boolean v3, v1, Lorg/apache/tika/parser/rtf/GroupState;->ignore:Z

    goto/16 :goto_4

    .line 1040
    :cond_20
    const-string/jumbo v1, "line"

    invoke-direct {p0, v1}, Lorg/apache/tika/parser/rtf/TextExtractor;->equals(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_21

    .line 1041
    if-nez v0, :cond_8

    .line 1042
    invoke-direct {p0, v4}, Lorg/apache/tika/parser/rtf/TextExtractor;->addOutputChar(C)V

    goto/16 :goto_4

    .line 1044
    :cond_21
    const-string/jumbo v1, "column"

    invoke-direct {p0, v1}, Lorg/apache/tika/parser/rtf/TextExtractor;->equals(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_22

    .line 1045
    if-nez v0, :cond_8

    .line 1046
    const/16 v1, 0x20

    invoke-direct {p0, v1}, Lorg/apache/tika/parser/rtf/TextExtractor;->addOutputChar(C)V

    goto/16 :goto_4

    .line 1048
    :cond_22
    const-string/jumbo v1, "page"

    invoke-direct {p0, v1}, Lorg/apache/tika/parser/rtf/TextExtractor;->equals(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_23

    .line 1049
    if-nez v0, :cond_8

    .line 1050
    invoke-direct {p0, v4}, Lorg/apache/tika/parser/rtf/TextExtractor;->addOutputChar(C)V

    goto/16 :goto_4

    .line 1052
    :cond_23
    const-string/jumbo v1, "softline"

    invoke-direct {p0, v1}, Lorg/apache/tika/parser/rtf/TextExtractor;->equals(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_24

    .line 1053
    if-nez v0, :cond_8

    .line 1054
    invoke-direct {p0, v4}, Lorg/apache/tika/parser/rtf/TextExtractor;->addOutputChar(C)V

    goto/16 :goto_4

    .line 1056
    :cond_24
    const-string/jumbo v1, "softcolumn"

    invoke-direct {p0, v1}, Lorg/apache/tika/parser/rtf/TextExtractor;->equals(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_25

    .line 1057
    if-nez v0, :cond_8

    .line 1058
    const/16 v1, 0x20

    invoke-direct {p0, v1}, Lorg/apache/tika/parser/rtf/TextExtractor;->addOutputChar(C)V

    goto/16 :goto_4

    .line 1060
    :cond_25
    const-string/jumbo v1, "softpage"

    invoke-direct {p0, v1}, Lorg/apache/tika/parser/rtf/TextExtractor;->equals(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_26

    .line 1061
    if-nez v0, :cond_8

    .line 1062
    invoke-direct {p0, v4}, Lorg/apache/tika/parser/rtf/TextExtractor;->addOutputChar(C)V

    goto/16 :goto_4

    .line 1064
    :cond_26
    const-string/jumbo v1, "tab"

    invoke-direct {p0, v1}, Lorg/apache/tika/parser/rtf/TextExtractor;->equals(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_27

    .line 1065
    if-nez v0, :cond_8

    .line 1066
    const/16 v1, 0x9

    invoke-direct {p0, v1}, Lorg/apache/tika/parser/rtf/TextExtractor;->addOutputChar(C)V

    goto/16 :goto_4

    .line 1068
    :cond_27
    const-string/jumbo v1, "upr"

    invoke-direct {p0, v1}, Lorg/apache/tika/parser/rtf/TextExtractor;->equals(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_28

    .line 1069
    iput v5, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->uprState:I

    goto/16 :goto_4

    .line 1070
    :cond_28
    const-string/jumbo v1, "ud"

    invoke-direct {p0, v1}, Lorg/apache/tika/parser/rtf/TextExtractor;->equals(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_29

    iget v1, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->uprState:I

    if-ne v1, v3, :cond_29

    .line 1071
    iput v6, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->uprState:I

    .line 1075
    iget-object v1, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->groupState:Lorg/apache/tika/parser/rtf/GroupState;

    iput-boolean v5, v1, Lorg/apache/tika/parser/rtf/GroupState;->ignore:Z

    goto/16 :goto_4

    .line 1076
    :cond_29
    const-string/jumbo v1, "bullet"

    invoke-direct {p0, v1}, Lorg/apache/tika/parser/rtf/TextExtractor;->equals(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2a

    .line 1077
    if-nez v0, :cond_8

    .line 1079
    const/16 v1, 0x2022

    invoke-direct {p0, v1}, Lorg/apache/tika/parser/rtf/TextExtractor;->addOutputChar(C)V

    goto/16 :goto_4

    .line 1081
    :cond_2a
    const-string/jumbo v1, "endash"

    invoke-direct {p0, v1}, Lorg/apache/tika/parser/rtf/TextExtractor;->equals(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2b

    .line 1082
    if-nez v0, :cond_8

    .line 1084
    const/16 v1, 0x2013

    invoke-direct {p0, v1}, Lorg/apache/tika/parser/rtf/TextExtractor;->addOutputChar(C)V

    goto/16 :goto_4

    .line 1086
    :cond_2b
    const-string/jumbo v1, "emdash"

    invoke-direct {p0, v1}, Lorg/apache/tika/parser/rtf/TextExtractor;->equals(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2c

    .line 1087
    if-nez v0, :cond_8

    .line 1089
    const/16 v1, 0x2014

    invoke-direct {p0, v1}, Lorg/apache/tika/parser/rtf/TextExtractor;->addOutputChar(C)V

    goto/16 :goto_4

    .line 1091
    :cond_2c
    const-string/jumbo v1, "enspace"

    invoke-direct {p0, v1}, Lorg/apache/tika/parser/rtf/TextExtractor;->equals(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2d

    .line 1092
    if-nez v0, :cond_8

    .line 1094
    const/16 v1, 0x2002

    invoke-direct {p0, v1}, Lorg/apache/tika/parser/rtf/TextExtractor;->addOutputChar(C)V

    goto/16 :goto_4

    .line 1096
    :cond_2d
    const-string/jumbo v1, "qmspace"

    invoke-direct {p0, v1}, Lorg/apache/tika/parser/rtf/TextExtractor;->equals(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2e

    .line 1097
    if-nez v0, :cond_8

    .line 1099
    const/16 v1, 0x2005

    invoke-direct {p0, v1}, Lorg/apache/tika/parser/rtf/TextExtractor;->addOutputChar(C)V

    goto/16 :goto_4

    .line 1101
    :cond_2e
    const-string/jumbo v1, "emspace"

    invoke-direct {p0, v1}, Lorg/apache/tika/parser/rtf/TextExtractor;->equals(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2f

    .line 1102
    if-nez v0, :cond_8

    .line 1104
    const/16 v1, 0x2003

    invoke-direct {p0, v1}, Lorg/apache/tika/parser/rtf/TextExtractor;->addOutputChar(C)V

    goto/16 :goto_4

    .line 1106
    :cond_2f
    const-string/jumbo v1, "lquote"

    invoke-direct {p0, v1}, Lorg/apache/tika/parser/rtf/TextExtractor;->equals(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_30

    .line 1107
    if-nez v0, :cond_8

    .line 1109
    const/16 v1, 0x2018

    invoke-direct {p0, v1}, Lorg/apache/tika/parser/rtf/TextExtractor;->addOutputChar(C)V

    goto/16 :goto_4

    .line 1111
    :cond_30
    const-string/jumbo v1, "rquote"

    invoke-direct {p0, v1}, Lorg/apache/tika/parser/rtf/TextExtractor;->equals(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_31

    .line 1112
    if-nez v0, :cond_8

    .line 1114
    const/16 v1, 0x2019

    invoke-direct {p0, v1}, Lorg/apache/tika/parser/rtf/TextExtractor;->addOutputChar(C)V

    goto/16 :goto_4

    .line 1116
    :cond_31
    const-string/jumbo v1, "ldblquote"

    invoke-direct {p0, v1}, Lorg/apache/tika/parser/rtf/TextExtractor;->equals(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_32

    .line 1117
    if-nez v0, :cond_8

    .line 1119
    const/16 v1, 0x201c

    invoke-direct {p0, v1}, Lorg/apache/tika/parser/rtf/TextExtractor;->addOutputChar(C)V

    goto/16 :goto_4

    .line 1121
    :cond_32
    const-string/jumbo v1, "rdblquote"

    invoke-direct {p0, v1}, Lorg/apache/tika/parser/rtf/TextExtractor;->equals(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_33

    .line 1122
    if-nez v0, :cond_8

    .line 1124
    const/16 v1, 0x201d

    invoke-direct {p0, v1}, Lorg/apache/tika/parser/rtf/TextExtractor;->addOutputChar(C)V

    goto/16 :goto_4

    .line 1126
    :cond_33
    const-string/jumbo v1, "fldinst"

    invoke-direct {p0, v1}, Lorg/apache/tika/parser/rtf/TextExtractor;->equals(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_34

    .line 1127
    iput v3, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->fieldState:I

    .line 1128
    iget-object v1, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->groupState:Lorg/apache/tika/parser/rtf/GroupState;

    iput-boolean v5, v1, Lorg/apache/tika/parser/rtf/GroupState;->ignore:Z

    goto/16 :goto_4

    .line 1129
    :cond_34
    const-string/jumbo v1, "fldrslt"

    invoke-direct {p0, v1}, Lorg/apache/tika/parser/rtf/TextExtractor;->equals(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_8

    iget v1, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->fieldState:I

    if-ne v1, v7, :cond_8

    .line 1130
    sget-boolean v1, Lorg/apache/tika/parser/rtf/TextExtractor;->$assertionsDisabled:Z

    if-nez v1, :cond_35

    iget-object v1, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->pendingURL:Ljava/lang/String;

    if-nez v1, :cond_35

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 1131
    :cond_35
    invoke-direct {p0}, Lorg/apache/tika/parser/rtf/TextExtractor;->lazyStartParagraph()V

    .line 1132
    iget-object v1, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->out:Lorg/apache/tika/sax/XHTMLContentHandler;

    const-string/jumbo v2, "a"

    const-string/jumbo v3, "href"

    iget-object v4, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->pendingURL:Ljava/lang/String;

    invoke-virtual {v1, v2, v3, v4}, Lorg/apache/tika/sax/XHTMLContentHandler;->startElement(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1133
    const/4 v1, 0x0

    iput-object v1, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->pendingURL:Ljava/lang/String;

    .line 1134
    const/4 v1, 0x3

    iput v1, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->fieldState:I

    .line 1135
    iget-object v1, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->groupState:Lorg/apache/tika/parser/rtf/GroupState;

    iput-boolean v5, v1, Lorg/apache/tika/parser/rtf/GroupState;->ignore:Z

    goto/16 :goto_4
.end method

.method private processControlWord(ILjava/io/PushbackInputStream;)V
    .locals 10
    .param p1, "param"    # I
    .param p2, "in"    # Ljava/io/PushbackInputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/xml/sax/SAXException;,
            Lorg/apache/tika/exception/TikaException;
        }
    .end annotation

    .prologue
    const/4 v9, 0x0

    .line 783
    iget-boolean v6, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->inHeader:Z

    if-eqz v6, :cond_f

    .line 784
    const-string/jumbo v6, "ansicpg"

    invoke-direct {p0, v6}, Lorg/apache/tika/parser/rtf/TextExtractor;->equals(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 786
    sget-object v6, Lorg/apache/tika/parser/rtf/TextExtractor;->ANSICPG_MAP:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/nio/charset/Charset;

    .line 787
    .local v1, "cs":Ljava/nio/charset/Charset;
    if-eqz v1, :cond_0

    .line 788
    iput-object v1, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->globalCharset:Ljava/nio/charset/Charset;

    .line 811
    .end local v1    # "cs":Ljava/nio/charset/Charset;
    :cond_0
    :goto_0
    iget v6, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->fontTableState:I

    const/4 v7, 0x1

    if-ne v6, v7, :cond_1

    .line 814
    iget-object v6, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->groupState:Lorg/apache/tika/parser/rtf/GroupState;

    iget v6, v6, Lorg/apache/tika/parser/rtf/GroupState;->depth:I

    iget v7, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->fontTableDepth:I

    if-ge v6, v7, :cond_d

    .line 815
    const/4 v6, 0x2

    iput v6, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->fontTableState:I

    .line 874
    :cond_1
    :goto_1
    const-string/jumbo v6, "u"

    invoke-direct {p0, v6}, Lorg/apache/tika/parser/rtf/TextExtractor;->equals(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_17

    .line 876
    iget-object v6, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->groupState:Lorg/apache/tika/parser/rtf/GroupState;

    iget-boolean v6, v6, Lorg/apache/tika/parser/rtf/GroupState;->ignore:Z

    if-nez v6, :cond_2

    .line 877
    const v6, 0xffff

    and-int/2addr v6, p1

    int-to-char v5, v6

    .line 878
    .local v5, "utf16CodeUnit":C
    invoke-direct {p0, v5}, Lorg/apache/tika/parser/rtf/TextExtractor;->addOutputChar(C)V

    .line 884
    .end local v5    # "utf16CodeUnit":C
    :cond_2
    iget-object v6, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->groupState:Lorg/apache/tika/parser/rtf/GroupState;

    iget v6, v6, Lorg/apache/tika/parser/rtf/GroupState;->ucSkip:I

    iput v6, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->ansiSkip:I

    .line 903
    :cond_3
    :goto_2
    return-void

    .line 790
    :cond_4
    const-string/jumbo v6, "deff"

    invoke-direct {p0, v6}, Lorg/apache/tika/parser/rtf/TextExtractor;->equals(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 792
    iput p1, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->globalDefaultFont:I

    goto :goto_0

    .line 793
    :cond_5
    const-string/jumbo v6, "nofpages"

    invoke-direct {p0, v6}, Lorg/apache/tika/parser/rtf/TextExtractor;->equals(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 794
    iget-object v6, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->metadata:Lorg/apache/tika/metadata/Metadata;

    sget-object v7, Lorg/apache/tika/metadata/Office;->PAGE_COUNT:Lorg/apache/tika/metadata/Property;

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Lorg/apache/tika/metadata/Metadata;->add(Lorg/apache/tika/metadata/Property;Ljava/lang/String;)V

    goto :goto_0

    .line 795
    :cond_6
    const-string/jumbo v6, "nofwords"

    invoke-direct {p0, v6}, Lorg/apache/tika/parser/rtf/TextExtractor;->equals(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 796
    iget-object v6, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->metadata:Lorg/apache/tika/metadata/Metadata;

    sget-object v7, Lorg/apache/tika/metadata/Office;->WORD_COUNT:Lorg/apache/tika/metadata/Property;

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Lorg/apache/tika/metadata/Metadata;->add(Lorg/apache/tika/metadata/Property;Ljava/lang/String;)V

    goto :goto_0

    .line 797
    :cond_7
    const-string/jumbo v6, "nofchars"

    invoke-direct {p0, v6}, Lorg/apache/tika/parser/rtf/TextExtractor;->equals(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 798
    iget-object v6, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->metadata:Lorg/apache/tika/metadata/Metadata;

    sget-object v7, Lorg/apache/tika/metadata/Office;->CHARACTER_COUNT:Lorg/apache/tika/metadata/Property;

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Lorg/apache/tika/metadata/Metadata;->add(Lorg/apache/tika/metadata/Property;Ljava/lang/String;)V

    goto :goto_0

    .line 799
    :cond_8
    const-string/jumbo v6, "yr"

    invoke-direct {p0, v6}, Lorg/apache/tika/parser/rtf/TextExtractor;->equals(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_9

    .line 800
    iput p1, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->year:I

    goto/16 :goto_0

    .line 801
    :cond_9
    const-string/jumbo v6, "mo"

    invoke-direct {p0, v6}, Lorg/apache/tika/parser/rtf/TextExtractor;->equals(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_a

    .line 802
    iput p1, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->month:I

    goto/16 :goto_0

    .line 803
    :cond_a
    const-string/jumbo v6, "dy"

    invoke-direct {p0, v6}, Lorg/apache/tika/parser/rtf/TextExtractor;->equals(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_b

    .line 804
    iput p1, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->day:I

    goto/16 :goto_0

    .line 805
    :cond_b
    const-string/jumbo v6, "hr"

    invoke-direct {p0, v6}, Lorg/apache/tika/parser/rtf/TextExtractor;->equals(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_c

    .line 806
    iput p1, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->hour:I

    goto/16 :goto_0

    .line 807
    :cond_c
    const-string/jumbo v6, "min"

    invoke-direct {p0, v6}, Lorg/apache/tika/parser/rtf/TextExtractor;->equals(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 808
    iput p1, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->minute:I

    goto/16 :goto_0

    .line 817
    :cond_d
    const-string/jumbo v6, "f"

    invoke-direct {p0, v6}, Lorg/apache/tika/parser/rtf/TextExtractor;->equals(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_e

    .line 819
    iput p1, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->curFontID:I

    goto/16 :goto_1

    .line 820
    :cond_e
    const-string/jumbo v6, "fcharset"

    invoke-direct {p0, v6}, Lorg/apache/tika/parser/rtf/TextExtractor;->equals(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 821
    sget-object v6, Lorg/apache/tika/parser/rtf/TextExtractor;->FCHARSET_MAP:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/nio/charset/Charset;

    .line 822
    .restart local v1    # "cs":Ljava/nio/charset/Charset;
    if-eqz v1, :cond_1

    .line 823
    iget-object v6, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->fontToCharset:Ljava/util/Map;

    iget v7, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->curFontID:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v6, v7, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 830
    .end local v1    # "cs":Ljava/nio/charset/Charset;
    :cond_f
    const-string/jumbo v6, "b"

    invoke-direct {p0, v6}, Lorg/apache/tika/parser/rtf/TextExtractor;->equals(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_13

    .line 832
    sget-boolean v6, Lorg/apache/tika/parser/rtf/TextExtractor;->$assertionsDisabled:Z

    if-nez v6, :cond_10

    if-eqz p1, :cond_10

    new-instance v6, Ljava/lang/AssertionError;

    invoke-direct {v6}, Ljava/lang/AssertionError;-><init>()V

    throw v6

    .line 833
    :cond_10
    iget-object v6, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->groupState:Lorg/apache/tika/parser/rtf/GroupState;

    iget-boolean v6, v6, Lorg/apache/tika/parser/rtf/GroupState;->bold:Z

    if-eqz v6, :cond_1

    .line 834
    invoke-direct {p0}, Lorg/apache/tika/parser/rtf/TextExtractor;->pushText()V

    .line 835
    iget-object v6, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->groupState:Lorg/apache/tika/parser/rtf/GroupState;

    iget-boolean v6, v6, Lorg/apache/tika/parser/rtf/GroupState;->italic:Z

    if-eqz v6, :cond_11

    .line 836
    const-string/jumbo v6, "i"

    invoke-direct {p0, v6}, Lorg/apache/tika/parser/rtf/TextExtractor;->end(Ljava/lang/String;)V

    .line 838
    :cond_11
    const-string/jumbo v6, "b"

    invoke-direct {p0, v6}, Lorg/apache/tika/parser/rtf/TextExtractor;->end(Ljava/lang/String;)V

    .line 839
    iget-object v6, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->groupState:Lorg/apache/tika/parser/rtf/GroupState;

    iget-boolean v6, v6, Lorg/apache/tika/parser/rtf/GroupState;->italic:Z

    if-eqz v6, :cond_12

    .line 840
    const-string/jumbo v6, "i"

    invoke-direct {p0, v6}, Lorg/apache/tika/parser/rtf/TextExtractor;->start(Ljava/lang/String;)V

    .line 842
    :cond_12
    iget-object v6, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->groupState:Lorg/apache/tika/parser/rtf/GroupState;

    iput-boolean v9, v6, Lorg/apache/tika/parser/rtf/GroupState;->bold:Z

    goto/16 :goto_1

    .line 844
    :cond_13
    const-string/jumbo v6, "i"

    invoke-direct {p0, v6}, Lorg/apache/tika/parser/rtf/TextExtractor;->equals(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_15

    .line 846
    sget-boolean v6, Lorg/apache/tika/parser/rtf/TextExtractor;->$assertionsDisabled:Z

    if-nez v6, :cond_14

    if-eqz p1, :cond_14

    new-instance v6, Ljava/lang/AssertionError;

    invoke-direct {v6}, Ljava/lang/AssertionError;-><init>()V

    throw v6

    .line 847
    :cond_14
    iget-object v6, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->groupState:Lorg/apache/tika/parser/rtf/GroupState;

    iget-boolean v6, v6, Lorg/apache/tika/parser/rtf/GroupState;->italic:Z

    if-eqz v6, :cond_1

    .line 848
    invoke-direct {p0}, Lorg/apache/tika/parser/rtf/TextExtractor;->pushText()V

    .line 849
    const-string/jumbo v6, "i"

    invoke-direct {p0, v6}, Lorg/apache/tika/parser/rtf/TextExtractor;->end(Ljava/lang/String;)V

    .line 850
    iget-object v6, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->groupState:Lorg/apache/tika/parser/rtf/GroupState;

    iput-boolean v9, v6, Lorg/apache/tika/parser/rtf/GroupState;->italic:Z

    goto/16 :goto_1

    .line 852
    :cond_15
    const-string/jumbo v6, "f"

    invoke-direct {p0, v6}, Lorg/apache/tika/parser/rtf/TextExtractor;->equals(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 854
    iget-object v6, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->fontToCharset:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/nio/charset/Charset;

    .line 858
    .local v2, "fontCharset":Ljava/nio/charset/Charset;
    invoke-direct {p0}, Lorg/apache/tika/parser/rtf/TextExtractor;->pushText()V

    .line 860
    if-eqz v2, :cond_16

    .line 861
    iget-object v6, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->groupState:Lorg/apache/tika/parser/rtf/GroupState;

    iput-object v2, v6, Lorg/apache/tika/parser/rtf/GroupState;->fontCharset:Ljava/nio/charset/Charset;

    goto/16 :goto_1

    .line 866
    :cond_16
    iget-object v6, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->groupState:Lorg/apache/tika/parser/rtf/GroupState;

    const/4 v7, 0x0

    iput-object v7, v6, Lorg/apache/tika/parser/rtf/GroupState;->fontCharset:Ljava/nio/charset/Charset;

    goto/16 :goto_1

    .line 885
    .end local v2    # "fontCharset":Ljava/nio/charset/Charset;
    :cond_17
    const-string/jumbo v6, "uc"

    invoke-direct {p0, v6}, Lorg/apache/tika/parser/rtf/TextExtractor;->equals(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_18

    .line 887
    iget-object v6, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->groupState:Lorg/apache/tika/parser/rtf/GroupState;

    iput p1, v6, Lorg/apache/tika/parser/rtf/GroupState;->ucSkip:I

    goto/16 :goto_2

    .line 888
    :cond_18
    const-string/jumbo v6, "bin"

    invoke-direct {p0, v6}, Lorg/apache/tika/parser/rtf/TextExtractor;->equals(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 889
    if-ltz p1, :cond_3

    .line 890
    move v0, p1

    .line 891
    .local v0, "bytesToRead":I
    const/16 v6, 0x400

    invoke-static {v6, v0}, Ljava/lang/Math;->min(II)I

    move-result v6

    new-array v4, v6, [B

    .line 892
    .local v4, "tmpArray":[B
    :goto_3
    if-lez v0, :cond_3

    .line 893
    array-length v6, v4

    invoke-static {v0, v6}, Ljava/lang/Math;->min(II)I

    move-result v6

    invoke-virtual {p2, v4, v9, v6}, Ljava/io/PushbackInputStream;->read([BII)I

    move-result v3

    .line 894
    .local v3, "r":I
    if-gez v3, :cond_19

    .line 895
    new-instance v6, Lorg/apache/tika/exception/TikaException;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string/jumbo v8, "unexpected end of file: need "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, " bytes of binary data, found "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sub-int v8, p1, v0

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Lorg/apache/tika/exception/TikaException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 897
    :cond_19
    sub-int/2addr v0, v3

    goto :goto_3
.end method

.method private processGroupEnd()V
    .locals 14
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/xml/sax/SAXException;,
            Lorg/apache/tika/exception/TikaException;
        }
    .end annotation

    .prologue
    const/16 v13, 0x22

    const/4 v9, 0x1

    const/4 v12, -0x1

    const/4 v6, 0x0

    .line 1171
    iget-boolean v1, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->inHeader:Z

    if-eqz v1, :cond_1

    .line 1172
    iget-object v1, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->nextMetaData:Lorg/apache/tika/metadata/Property;

    if-eqz v1, :cond_0

    .line 1173
    iget-object v1, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->nextMetaData:Lorg/apache/tika/metadata/Property;

    sget-object v2, Lorg/apache/tika/metadata/TikaCoreProperties;->CREATED:Lorg/apache/tika/metadata/Property;

    if-ne v1, v2, :cond_2

    .line 1174
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 1175
    .local v0, "cal":Ljava/util/Calendar;
    iget v1, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->year:I

    iget v2, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->month:I

    add-int/lit8 v2, v2, -0x1

    iget v3, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->day:I

    iget v4, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->hour:I

    iget v5, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->minute:I

    invoke-virtual/range {v0 .. v6}, Ljava/util/Calendar;->set(IIIIII)V

    .line 1176
    iget-object v1, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->metadata:Lorg/apache/tika/metadata/Metadata;

    iget-object v2, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->nextMetaData:Lorg/apache/tika/metadata/Property;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lorg/apache/tika/metadata/Metadata;->set(Lorg/apache/tika/metadata/Property;Ljava/util/Date;)V

    .line 1182
    .end local v0    # "cal":Ljava/util/Calendar;
    :goto_0
    const/4 v1, 0x0

    iput-object v1, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->nextMetaData:Lorg/apache/tika/metadata/Property;

    .line 1184
    :cond_0
    iget-object v1, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->pendingBuffer:Ljava/lang/StringBuilder;

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 1187
    :cond_1
    sget-boolean v1, Lorg/apache/tika/parser/rtf/TextExtractor;->$assertionsDisabled:Z

    if-nez v1, :cond_4

    iget-object v1, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->groupState:Lorg/apache/tika/parser/rtf/GroupState;

    iget v1, v1, Lorg/apache/tika/parser/rtf/GroupState;->depth:I

    if-gtz v1, :cond_4

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 1177
    :cond_2
    iget-object v1, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->nextMetaData:Lorg/apache/tika/metadata/Property;

    invoke-virtual {v1}, Lorg/apache/tika/metadata/Property;->isMultiValuePermitted()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1178
    iget-object v1, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->metadata:Lorg/apache/tika/metadata/Metadata;

    iget-object v2, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->nextMetaData:Lorg/apache/tika/metadata/Property;

    iget-object v3, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->pendingBuffer:Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lorg/apache/tika/metadata/Metadata;->add(Lorg/apache/tika/metadata/Property;Ljava/lang/String;)V

    goto :goto_0

    .line 1180
    :cond_3
    iget-object v1, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->metadata:Lorg/apache/tika/metadata/Metadata;

    iget-object v2, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->nextMetaData:Lorg/apache/tika/metadata/Property;

    iget-object v3, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->pendingBuffer:Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lorg/apache/tika/metadata/Metadata;->set(Lorg/apache/tika/metadata/Property;Ljava/lang/String;)V

    goto :goto_0

    .line 1188
    :cond_4
    iput v6, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->ansiSkip:I

    .line 1193
    iget-object v1, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->groupStates:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->size()I

    move-result v1

    if-lez v1, :cond_a

    .line 1195
    iget-object v1, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->groupStates:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->removeLast()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lorg/apache/tika/parser/rtf/GroupState;

    .line 1199
    .local v10, "outerGroupState":Lorg/apache/tika/parser/rtf/GroupState;
    iget-object v1, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->groupState:Lorg/apache/tika/parser/rtf/GroupState;

    iget-boolean v1, v1, Lorg/apache/tika/parser/rtf/GroupState;->italic:Z

    if-eqz v1, :cond_6

    .line 1200
    iget-boolean v1, v10, Lorg/apache/tika/parser/rtf/GroupState;->italic:Z

    if-eqz v1, :cond_5

    .line 1201
    iget-object v1, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->groupState:Lorg/apache/tika/parser/rtf/GroupState;

    iget-boolean v1, v1, Lorg/apache/tika/parser/rtf/GroupState;->bold:Z

    iget-boolean v2, v10, Lorg/apache/tika/parser/rtf/GroupState;->bold:Z

    if-eq v1, v2, :cond_6

    .line 1202
    :cond_5
    const-string/jumbo v1, "i"

    invoke-direct {p0, v1}, Lorg/apache/tika/parser/rtf/TextExtractor;->end(Ljava/lang/String;)V

    .line 1203
    iget-object v1, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->groupState:Lorg/apache/tika/parser/rtf/GroupState;

    iput-boolean v6, v1, Lorg/apache/tika/parser/rtf/GroupState;->italic:Z

    .line 1208
    :cond_6
    iget-object v1, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->groupState:Lorg/apache/tika/parser/rtf/GroupState;

    iget-boolean v1, v1, Lorg/apache/tika/parser/rtf/GroupState;->bold:Z

    if-eqz v1, :cond_7

    iget-boolean v1, v10, Lorg/apache/tika/parser/rtf/GroupState;->bold:Z

    if-nez v1, :cond_7

    .line 1209
    const-string/jumbo v1, "b"

    invoke-direct {p0, v1}, Lorg/apache/tika/parser/rtf/TextExtractor;->end(Ljava/lang/String;)V

    .line 1213
    :cond_7
    iget-object v1, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->groupState:Lorg/apache/tika/parser/rtf/GroupState;

    iget-boolean v1, v1, Lorg/apache/tika/parser/rtf/GroupState;->bold:Z

    if-nez v1, :cond_8

    iget-boolean v1, v10, Lorg/apache/tika/parser/rtf/GroupState;->bold:Z

    if-eqz v1, :cond_8

    .line 1214
    const-string/jumbo v1, "b"

    invoke-direct {p0, v1}, Lorg/apache/tika/parser/rtf/TextExtractor;->start(Ljava/lang/String;)V

    .line 1218
    :cond_8
    iget-object v1, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->groupState:Lorg/apache/tika/parser/rtf/GroupState;

    iget-boolean v1, v1, Lorg/apache/tika/parser/rtf/GroupState;->italic:Z

    if-nez v1, :cond_9

    iget-boolean v1, v10, Lorg/apache/tika/parser/rtf/GroupState;->italic:Z

    if-eqz v1, :cond_9

    .line 1219
    const-string/jumbo v1, "i"

    invoke-direct {p0, v1}, Lorg/apache/tika/parser/rtf/TextExtractor;->start(Ljava/lang/String;)V

    .line 1221
    :cond_9
    iput-object v10, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->groupState:Lorg/apache/tika/parser/rtf/GroupState;

    .line 1223
    .end local v10    # "outerGroupState":Lorg/apache/tika/parser/rtf/GroupState;
    :cond_a
    sget-boolean v1, Lorg/apache/tika/parser/rtf/TextExtractor;->$assertionsDisabled:Z

    if-nez v1, :cond_b

    iget-object v1, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->groupStates:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->size()I

    move-result v1

    iget-object v2, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->groupState:Lorg/apache/tika/parser/rtf/GroupState;

    iget v2, v2, Lorg/apache/tika/parser/rtf/GroupState;->depth:I

    if-eq v1, v2, :cond_b

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 1225
    :cond_b
    iget v1, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->fieldState:I

    if-ne v1, v9, :cond_11

    .line 1226
    iget-object v1, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->pendingBuffer:Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v11

    .line 1227
    .local v11, "s":Ljava/lang/String;
    iget-object v1, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->pendingBuffer:Ljava/lang/StringBuilder;

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 1228
    const-string/jumbo v1, "HYPERLINK"

    invoke-virtual {v11, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_10

    .line 1229
    const/16 v1, 0x9

    invoke-virtual {v11, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v11

    .line 1232
    const-string/jumbo v1, "\\l "

    invoke-virtual {v11, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    if-eq v1, v12, :cond_e

    .line 1233
    .local v9, "isLocalLink":Z
    :goto_1
    invoke-virtual {v11, v13}, Ljava/lang/String;->indexOf(I)I

    move-result v7

    .line 1234
    .local v7, "idx":I
    if-eq v7, v12, :cond_c

    .line 1235
    add-int/lit8 v1, v7, 0x1

    invoke-virtual {v11, v13, v1}, Ljava/lang/String;->indexOf(II)I

    move-result v8

    .line 1236
    .local v8, "idx2":I
    if-eq v8, v12, :cond_c

    .line 1237
    add-int/lit8 v1, v7, 0x1

    invoke-virtual {v11, v1, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    .line 1240
    .end local v8    # "idx2":I
    :cond_c
    new-instance v2, Ljava/lang/StringBuilder;

    if-eqz v9, :cond_f

    const-string/jumbo v1, "#"

    :goto_2
    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->pendingURL:Ljava/lang/String;

    .line 1241
    const/4 v1, 0x2

    iput v1, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->fieldState:I

    .line 1254
    .end local v7    # "idx":I
    .end local v9    # "isLocalLink":Z
    .end local v11    # "s":Ljava/lang/String;
    :cond_d
    :goto_3
    return-void

    .restart local v11    # "s":Ljava/lang/String;
    :cond_e
    move v9, v6

    .line 1232
    goto :goto_1

    .line 1240
    .restart local v7    # "idx":I
    .restart local v9    # "isLocalLink":Z
    :cond_f
    const-string/jumbo v1, ""

    goto :goto_2

    .line 1243
    .end local v7    # "idx":I
    .end local v9    # "isLocalLink":Z
    :cond_10
    iput v6, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->fieldState:I

    goto :goto_3

    .line 1250
    .end local v11    # "s":Ljava/lang/String;
    :cond_11
    iget v1, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->fieldState:I

    const/4 v2, 0x3

    if-ne v1, v2, :cond_d

    .line 1251
    iget-object v1, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->out:Lorg/apache/tika/sax/XHTMLContentHandler;

    const-string/jumbo v2, "a"

    invoke-virtual {v1, v2}, Lorg/apache/tika/sax/XHTMLContentHandler;->endElement(Ljava/lang/String;)V

    .line 1252
    iput v6, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->fieldState:I

    goto :goto_3
.end method

.method private processGroupStart(Ljava/io/PushbackInputStream;)V
    .locals 5
    .param p1, "in"    # Ljava/io/PushbackInputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 1141
    const/4 v2, 0x0

    iput v2, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->ansiSkip:I

    .line 1143
    iget-object v2, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->groupStates:Ljava/util/LinkedList;

    iget-object v3, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->groupState:Lorg/apache/tika/parser/rtf/GroupState;

    invoke-virtual {v2, v3}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 1146
    new-instance v2, Lorg/apache/tika/parser/rtf/GroupState;

    iget-object v3, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->groupState:Lorg/apache/tika/parser/rtf/GroupState;

    invoke-direct {v2, v3}, Lorg/apache/tika/parser/rtf/GroupState;-><init>(Lorg/apache/tika/parser/rtf/GroupState;)V

    iput-object v2, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->groupState:Lorg/apache/tika/parser/rtf/GroupState;

    .line 1147
    sget-boolean v2, Lorg/apache/tika/parser/rtf/TextExtractor;->$assertionsDisabled:Z

    if-nez v2, :cond_0

    iget-object v2, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->groupStates:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->size()I

    move-result v2

    iget-object v3, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->groupState:Lorg/apache/tika/parser/rtf/GroupState;

    iget v3, v3, Lorg/apache/tika/parser/rtf/GroupState;->depth:I

    if-eq v2, v3, :cond_0

    new-instance v2, Ljava/lang/AssertionError;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "size="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->groupStates:Ljava/util/LinkedList;

    invoke-virtual {v4}, Ljava/util/LinkedList;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " depth="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->groupState:Lorg/apache/tika/parser/rtf/GroupState;

    iget v4, v4, Lorg/apache/tika/parser/rtf/GroupState;->depth:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2

    .line 1149
    :cond_0
    iget v2, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->uprState:I

    if-nez v2, :cond_1

    .line 1150
    iput v4, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->uprState:I

    .line 1151
    iget-object v2, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->groupState:Lorg/apache/tika/parser/rtf/GroupState;

    iput-boolean v4, v2, Lorg/apache/tika/parser/rtf/GroupState;->ignore:Z

    .line 1157
    :cond_1
    invoke-virtual {p1}, Ljava/io/PushbackInputStream;->read()I

    move-result v0

    .line 1158
    .local v0, "b2":I
    const/16 v2, 0x5c

    if-ne v0, v2, :cond_3

    .line 1159
    invoke-virtual {p1}, Ljava/io/PushbackInputStream;->read()I

    move-result v1

    .line 1160
    .local v1, "b3":I
    const/16 v2, 0x2a

    if-ne v1, v2, :cond_2

    .line 1161
    iget-object v2, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->groupState:Lorg/apache/tika/parser/rtf/GroupState;

    iput-boolean v4, v2, Lorg/apache/tika/parser/rtf/GroupState;->ignore:Z

    .line 1163
    :cond_2
    invoke-virtual {p1, v1}, Ljava/io/PushbackInputStream;->unread(I)V

    .line 1165
    .end local v1    # "b3":I
    :cond_3
    invoke-virtual {p1, v0}, Ljava/io/PushbackInputStream;->unread(I)V

    .line 1166
    return-void
.end method

.method private pushBytes()V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/xml/sax/SAXException;,
            Lorg/apache/tika/exception/TikaException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 623
    iget v3, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->pendingByteCount:I

    if-lez v3, :cond_8

    iget-object v3, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->groupState:Lorg/apache/tika/parser/rtf/GroupState;

    iget-boolean v3, v3, Lorg/apache/tika/parser/rtf/GroupState;->ignore:Z

    if-eqz v3, :cond_0

    iget-object v3, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->nextMetaData:Lorg/apache/tika/metadata/Property;

    if-eqz v3, :cond_8

    .line 625
    :cond_0
    invoke-direct {p0}, Lorg/apache/tika/parser/rtf/TextExtractor;->getDecoder()Ljava/nio/charset/CharsetDecoder;

    move-result-object v0

    .line 626
    .local v0, "decoder":Ljava/nio/charset/CharsetDecoder;
    iget-object v3, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->pendingByteBuffer:Ljava/nio/ByteBuffer;

    iget v4, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->pendingByteCount:I

    invoke-virtual {v3, v4}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    .line 627
    sget-boolean v3, Lorg/apache/tika/parser/rtf/TextExtractor;->$assertionsDisabled:Z

    if-nez v3, :cond_1

    iget-object v3, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->pendingByteBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->position()I

    move-result v3

    if-eqz v3, :cond_1

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 628
    :cond_1
    sget-boolean v3, Lorg/apache/tika/parser/rtf/TextExtractor;->$assertionsDisabled:Z

    if-nez v3, :cond_2

    iget-object v3, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->outputBuffer:Ljava/nio/CharBuffer;

    invoke-virtual {v3}, Ljava/nio/CharBuffer;->position()I

    move-result v3

    if-eqz v3, :cond_2

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 635
    :cond_2
    iget-object v3, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->pendingByteBuffer:Ljava/nio/ByteBuffer;

    iget-object v4, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->outputBuffer:Ljava/nio/CharBuffer;

    invoke-virtual {v0, v3, v4, v6}, Ljava/nio/charset/CharsetDecoder;->decode(Ljava/nio/ByteBuffer;Ljava/nio/CharBuffer;Z)Ljava/nio/charset/CoderResult;

    move-result-object v2

    .line 637
    .local v2, "result":Ljava/nio/charset/CoderResult;
    iget-object v3, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->outputBuffer:Ljava/nio/CharBuffer;

    invoke-virtual {v3}, Ljava/nio/CharBuffer;->position()I

    move-result v1

    .line 638
    .local v1, "pos":I
    if-lez v1, :cond_4

    .line 639
    iget-boolean v3, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->inHeader:Z

    if-nez v3, :cond_3

    iget v3, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->fieldState:I

    if-ne v3, v6, :cond_9

    .line 640
    :cond_3
    iget-object v3, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->pendingBuffer:Ljava/lang/StringBuilder;

    iget-object v4, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->outputArray:[C

    invoke-virtual {v3, v4, v5, v1}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;

    .line 645
    :goto_0
    iget-object v3, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->outputBuffer:Ljava/nio/CharBuffer;

    invoke-virtual {v3, v5}, Ljava/nio/CharBuffer;->position(I)Ljava/nio/Buffer;

    .line 648
    :cond_4
    sget-object v3, Ljava/nio/charset/CoderResult;->UNDERFLOW:Ljava/nio/charset/CoderResult;

    if-ne v2, v3, :cond_2

    .line 654
    :cond_5
    iget-object v3, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->outputBuffer:Ljava/nio/CharBuffer;

    invoke-virtual {v0, v3}, Ljava/nio/charset/CharsetDecoder;->flush(Ljava/nio/CharBuffer;)Ljava/nio/charset/CoderResult;

    move-result-object v2

    .line 656
    iget-object v3, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->outputBuffer:Ljava/nio/CharBuffer;

    invoke-virtual {v3}, Ljava/nio/CharBuffer;->position()I

    move-result v1

    .line 657
    if-lez v1, :cond_7

    .line 658
    iget-boolean v3, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->inHeader:Z

    if-nez v3, :cond_6

    iget v3, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->fieldState:I

    if-ne v3, v6, :cond_a

    .line 659
    :cond_6
    iget-object v3, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->pendingBuffer:Ljava/lang/StringBuilder;

    iget-object v4, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->outputArray:[C

    invoke-virtual {v3, v4, v5, v1}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;

    .line 664
    :goto_1
    iget-object v3, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->outputBuffer:Ljava/nio/CharBuffer;

    invoke-virtual {v3, v5}, Ljava/nio/CharBuffer;->position(I)Ljava/nio/Buffer;

    .line 667
    :cond_7
    sget-object v3, Ljava/nio/charset/CoderResult;->UNDERFLOW:Ljava/nio/charset/CoderResult;

    if-ne v2, v3, :cond_5

    .line 673
    invoke-virtual {v0}, Ljava/nio/charset/CharsetDecoder;->reset()Ljava/nio/charset/CharsetDecoder;

    .line 674
    iget-object v3, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->pendingByteBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v3, v5}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 677
    .end local v0    # "decoder":Ljava/nio/charset/CharsetDecoder;
    .end local v1    # "pos":I
    .end local v2    # "result":Ljava/nio/charset/CoderResult;
    :cond_8
    iput v5, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->pendingByteCount:I

    .line 678
    return-void

    .line 642
    .restart local v0    # "decoder":Ljava/nio/charset/CharsetDecoder;
    .restart local v1    # "pos":I
    .restart local v2    # "result":Ljava/nio/charset/CoderResult;
    :cond_9
    invoke-direct {p0}, Lorg/apache/tika/parser/rtf/TextExtractor;->lazyStartParagraph()V

    .line 643
    iget-object v3, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->out:Lorg/apache/tika/sax/XHTMLContentHandler;

    iget-object v4, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->outputArray:[C

    invoke-virtual {v3, v4, v5, v1}, Lorg/apache/tika/sax/XHTMLContentHandler;->characters([CII)V

    goto :goto_0

    .line 661
    :cond_a
    invoke-direct {p0}, Lorg/apache/tika/parser/rtf/TextExtractor;->lazyStartParagraph()V

    .line 662
    iget-object v3, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->out:Lorg/apache/tika/sax/XHTMLContentHandler;

    iget-object v4, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->outputArray:[C

    invoke-virtual {v3, v4, v5, v1}, Lorg/apache/tika/sax/XHTMLContentHandler;->characters([CII)V

    goto :goto_1
.end method

.method private pushChars()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/xml/sax/SAXException;,
            Lorg/apache/tika/exception/TikaException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 611
    iget v0, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->pendingCharCount:I

    if-eqz v0, :cond_0

    .line 612
    invoke-direct {p0}, Lorg/apache/tika/parser/rtf/TextExtractor;->lazyStartParagraph()V

    .line 613
    iget-object v0, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->out:Lorg/apache/tika/sax/XHTMLContentHandler;

    iget-object v1, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->pendingChars:[C

    iget v2, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->pendingCharCount:I

    invoke-virtual {v0, v1, v3, v2}, Lorg/apache/tika/sax/XHTMLContentHandler;->characters([CII)V

    .line 614
    iput v3, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->pendingCharCount:I

    .line 616
    :cond_0
    return-void
.end method

.method private pushText()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/xml/sax/SAXException;,
            Lorg/apache/tika/exception/TikaException;
        }
    .end annotation

    .prologue
    .line 356
    iget v0, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->pendingByteCount:I

    if-eqz v0, :cond_1

    .line 357
    sget-boolean v0, Lorg/apache/tika/parser/rtf/TextExtractor;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget v0, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->pendingCharCount:I

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 358
    :cond_0
    invoke-direct {p0}, Lorg/apache/tika/parser/rtf/TextExtractor;->pushBytes()V

    .line 362
    :goto_0
    return-void

    .line 360
    :cond_1
    invoke-direct {p0}, Lorg/apache/tika/parser/rtf/TextExtractor;->pushChars()V

    goto :goto_0
.end method

.method private start(Ljava/lang/String;)V
    .locals 1
    .param p1, "tag"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/xml/sax/SAXException;,
            Lorg/apache/tika/exception/TikaException;
        }
    .end annotation

    .prologue
    .line 910
    iget-object v0, p0, Lorg/apache/tika/parser/rtf/TextExtractor;->out:Lorg/apache/tika/sax/XHTMLContentHandler;

    invoke-virtual {v0, p1}, Lorg/apache/tika/sax/XHTMLContentHandler;->startElement(Ljava/lang/String;)V

    .line 911
    return-void
.end method


# virtual methods
.method public extract(Ljava/io/InputStream;)V
    .locals 2
    .param p1, "in"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/xml/sax/SAXException;,
            Lorg/apache/tika/exception/TikaException;
        }
    .end annotation

    .prologue
    .line 433
    new-instance v0, Ljava/io/PushbackInputStream;

    const/4 v1, 0x2

    invoke-direct {v0, p1, v1}, Ljava/io/PushbackInputStream;-><init>(Ljava/io/InputStream;I)V

    invoke-direct {p0, v0}, Lorg/apache/tika/parser/rtf/TextExtractor;->extract(Ljava/io/PushbackInputStream;)V

    .line 434
    return-void
.end method
