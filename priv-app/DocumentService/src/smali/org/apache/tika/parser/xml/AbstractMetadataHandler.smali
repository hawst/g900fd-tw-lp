.class Lorg/apache/tika/parser/xml/AbstractMetadataHandler;
.super Lorg/xml/sax/helpers/DefaultHandler;
.source "AbstractMetadataHandler.java"


# instance fields
.field private final metadata:Lorg/apache/tika/metadata/Metadata;

.field private final name:Ljava/lang/String;

.field private final property:Lorg/apache/tika/metadata/Property;


# direct methods
.method protected constructor <init>(Lorg/apache/tika/metadata/Metadata;Ljava/lang/String;)V
    .locals 1
    .param p1, "metadata"    # Lorg/apache/tika/metadata/Metadata;
    .param p2, "name"    # Ljava/lang/String;

    .prologue
    .line 37
    invoke-direct {p0}, Lorg/xml/sax/helpers/DefaultHandler;-><init>()V

    .line 38
    iput-object p1, p0, Lorg/apache/tika/parser/xml/AbstractMetadataHandler;->metadata:Lorg/apache/tika/metadata/Metadata;

    .line 39
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/tika/parser/xml/AbstractMetadataHandler;->property:Lorg/apache/tika/metadata/Property;

    .line 40
    iput-object p2, p0, Lorg/apache/tika/parser/xml/AbstractMetadataHandler;->name:Ljava/lang/String;

    .line 41
    return-void
.end method

.method protected constructor <init>(Lorg/apache/tika/metadata/Metadata;Lorg/apache/tika/metadata/Property;)V
    .locals 1
    .param p1, "metadata"    # Lorg/apache/tika/metadata/Metadata;
    .param p2, "property"    # Lorg/apache/tika/metadata/Property;

    .prologue
    .line 42
    invoke-direct {p0}, Lorg/xml/sax/helpers/DefaultHandler;-><init>()V

    .line 43
    iput-object p1, p0, Lorg/apache/tika/parser/xml/AbstractMetadataHandler;->metadata:Lorg/apache/tika/metadata/Metadata;

    .line 44
    iput-object p2, p0, Lorg/apache/tika/parser/xml/AbstractMetadataHandler;->property:Lorg/apache/tika/metadata/Property;

    .line 45
    invoke-virtual {p2}, Lorg/apache/tika/metadata/Property;->getName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/tika/parser/xml/AbstractMetadataHandler;->name:Ljava/lang/String;

    .line 46
    return-void
.end method


# virtual methods
.method protected addMetadata(Ljava/lang/String;)V
    .locals 4
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 56
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_0

    .line 57
    iget-object v2, p0, Lorg/apache/tika/parser/xml/AbstractMetadataHandler;->metadata:Lorg/apache/tika/metadata/Metadata;

    iget-object v3, p0, Lorg/apache/tika/parser/xml/AbstractMetadataHandler;->name:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lorg/apache/tika/metadata/Metadata;->isMultiValued(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 59
    iget-object v2, p0, Lorg/apache/tika/parser/xml/AbstractMetadataHandler;->metadata:Lorg/apache/tika/metadata/Metadata;

    iget-object v3, p0, Lorg/apache/tika/parser/xml/AbstractMetadataHandler;->name:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lorg/apache/tika/metadata/Metadata;->getValues(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    .line 60
    .local v1, "previous":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v1, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 61
    iget-object v2, p0, Lorg/apache/tika/parser/xml/AbstractMetadataHandler;->property:Lorg/apache/tika/metadata/Property;

    if-eqz v2, :cond_1

    .line 62
    iget-object v2, p0, Lorg/apache/tika/parser/xml/AbstractMetadataHandler;->metadata:Lorg/apache/tika/metadata/Metadata;

    iget-object v3, p0, Lorg/apache/tika/parser/xml/AbstractMetadataHandler;->property:Lorg/apache/tika/metadata/Property;

    invoke-virtual {v2, v3, p1}, Lorg/apache/tika/metadata/Metadata;->add(Lorg/apache/tika/metadata/Property;Ljava/lang/String;)V

    .line 92
    .end local v1    # "previous":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_0
    :goto_0
    return-void

    .line 64
    .restart local v1    # "previous":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_1
    iget-object v2, p0, Lorg/apache/tika/parser/xml/AbstractMetadataHandler;->metadata:Lorg/apache/tika/metadata/Metadata;

    iget-object v3, p0, Lorg/apache/tika/parser/xml/AbstractMetadataHandler;->name:Ljava/lang/String;

    invoke-virtual {v2, v3, p1}, Lorg/apache/tika/metadata/Metadata;->add(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 69
    .end local v1    # "previous":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_2
    iget-object v2, p0, Lorg/apache/tika/parser/xml/AbstractMetadataHandler;->metadata:Lorg/apache/tika/metadata/Metadata;

    iget-object v3, p0, Lorg/apache/tika/parser/xml/AbstractMetadataHandler;->name:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lorg/apache/tika/metadata/Metadata;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 70
    .local v0, "previous":Ljava/lang/String;
    if-eqz v0, :cond_5

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_5

    .line 71
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 72
    iget-object v2, p0, Lorg/apache/tika/parser/xml/AbstractMetadataHandler;->property:Lorg/apache/tika/metadata/Property;

    if-eqz v2, :cond_4

    .line 73
    iget-object v2, p0, Lorg/apache/tika/parser/xml/AbstractMetadataHandler;->property:Lorg/apache/tika/metadata/Property;

    invoke-virtual {v2}, Lorg/apache/tika/metadata/Property;->isMultiValuePermitted()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 74
    iget-object v2, p0, Lorg/apache/tika/parser/xml/AbstractMetadataHandler;->metadata:Lorg/apache/tika/metadata/Metadata;

    iget-object v3, p0, Lorg/apache/tika/parser/xml/AbstractMetadataHandler;->property:Lorg/apache/tika/metadata/Property;

    invoke-virtual {v2, v3, p1}, Lorg/apache/tika/metadata/Metadata;->add(Lorg/apache/tika/metadata/Property;Ljava/lang/String;)V

    goto :goto_0

    .line 77
    :cond_3
    iget-object v2, p0, Lorg/apache/tika/parser/xml/AbstractMetadataHandler;->metadata:Lorg/apache/tika/metadata/Metadata;

    iget-object v3, p0, Lorg/apache/tika/parser/xml/AbstractMetadataHandler;->property:Lorg/apache/tika/metadata/Property;

    invoke-virtual {v2, v3, p1}, Lorg/apache/tika/metadata/Metadata;->set(Lorg/apache/tika/metadata/Property;Ljava/lang/String;)V

    goto :goto_0

    .line 80
    :cond_4
    iget-object v2, p0, Lorg/apache/tika/parser/xml/AbstractMetadataHandler;->metadata:Lorg/apache/tika/metadata/Metadata;

    iget-object v3, p0, Lorg/apache/tika/parser/xml/AbstractMetadataHandler;->name:Ljava/lang/String;

    invoke-virtual {v2, v3, p1}, Lorg/apache/tika/metadata/Metadata;->add(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 84
    :cond_5
    iget-object v2, p0, Lorg/apache/tika/parser/xml/AbstractMetadataHandler;->property:Lorg/apache/tika/metadata/Property;

    if-eqz v2, :cond_6

    .line 85
    iget-object v2, p0, Lorg/apache/tika/parser/xml/AbstractMetadataHandler;->metadata:Lorg/apache/tika/metadata/Metadata;

    iget-object v3, p0, Lorg/apache/tika/parser/xml/AbstractMetadataHandler;->property:Lorg/apache/tika/metadata/Property;

    invoke-virtual {v2, v3, p1}, Lorg/apache/tika/metadata/Metadata;->set(Lorg/apache/tika/metadata/Property;Ljava/lang/String;)V

    goto :goto_0

    .line 87
    :cond_6
    iget-object v2, p0, Lorg/apache/tika/parser/xml/AbstractMetadataHandler;->metadata:Lorg/apache/tika/metadata/Metadata;

    iget-object v3, p0, Lorg/apache/tika/parser/xml/AbstractMetadataHandler;->name:Ljava/lang/String;

    invoke-virtual {v2, v3, p1}, Lorg/apache/tika/metadata/Metadata;->set(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
