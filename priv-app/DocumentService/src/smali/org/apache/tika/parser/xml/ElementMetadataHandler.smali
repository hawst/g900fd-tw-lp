.class public Lorg/apache/tika/parser/xml/ElementMetadataHandler;
.super Lorg/apache/tika/parser/xml/AbstractMetadataHandler;
.source "ElementMetadataHandler.java"


# static fields
.field private static final LOCAL_NAME_RDF_BAG:Ljava/lang/String; = "Bag"

.field private static final LOCAL_NAME_RDF_LI:Ljava/lang/String; = "li"

.field private static final URI_RDF:Ljava/lang/String; = "http://www.w3.org/1999/02/22-rdf-syntax-ns#"


# instance fields
.field private final bufferBagged:Ljava/lang/StringBuilder;

.field private final bufferBagless:Ljava/lang/StringBuilder;

.field private isBagless:Z

.field private final localName:Ljava/lang/String;

.field private matchLevel:I

.field private final metadata:Lorg/apache/tika/metadata/Metadata;

.field private final name:Ljava/lang/String;

.field private parentMatchLevel:I

.field private targetProperty:Lorg/apache/tika/metadata/Property;

.field private final uri:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lorg/apache/tika/metadata/Metadata;Ljava/lang/String;)V
    .locals 2
    .param p1, "uri"    # Ljava/lang/String;
    .param p2, "localName"    # Ljava/lang/String;
    .param p3, "metadata"    # Lorg/apache/tika/metadata/Metadata;
    .param p4, "name"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 74
    invoke-direct {p0, p3, p4}, Lorg/apache/tika/parser/xml/AbstractMetadataHandler;-><init>(Lorg/apache/tika/metadata/Metadata;Ljava/lang/String;)V

    .line 57
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lorg/apache/tika/parser/xml/ElementMetadataHandler;->bufferBagged:Ljava/lang/StringBuilder;

    .line 62
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lorg/apache/tika/parser/xml/ElementMetadataHandler;->bufferBagless:Ljava/lang/StringBuilder;

    .line 67
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/tika/parser/xml/ElementMetadataHandler;->isBagless:Z

    .line 69
    iput v1, p0, Lorg/apache/tika/parser/xml/ElementMetadataHandler;->matchLevel:I

    .line 70
    iput v1, p0, Lorg/apache/tika/parser/xml/ElementMetadataHandler;->parentMatchLevel:I

    .line 75
    iput-object p1, p0, Lorg/apache/tika/parser/xml/ElementMetadataHandler;->uri:Ljava/lang/String;

    .line 76
    iput-object p2, p0, Lorg/apache/tika/parser/xml/ElementMetadataHandler;->localName:Ljava/lang/String;

    .line 77
    iput-object p3, p0, Lorg/apache/tika/parser/xml/ElementMetadataHandler;->metadata:Lorg/apache/tika/metadata/Metadata;

    .line 78
    iput-object p4, p0, Lorg/apache/tika/parser/xml/ElementMetadataHandler;->name:Ljava/lang/String;

    .line 83
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lorg/apache/tika/metadata/Metadata;Lorg/apache/tika/metadata/Property;)V
    .locals 2
    .param p1, "uri"    # Ljava/lang/String;
    .param p2, "localName"    # Ljava/lang/String;
    .param p3, "metadata"    # Lorg/apache/tika/metadata/Metadata;
    .param p4, "targetProperty"    # Lorg/apache/tika/metadata/Property;

    .prologue
    const/4 v1, 0x0

    .line 87
    invoke-direct {p0, p3, p4}, Lorg/apache/tika/parser/xml/AbstractMetadataHandler;-><init>(Lorg/apache/tika/metadata/Metadata;Lorg/apache/tika/metadata/Property;)V

    .line 57
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lorg/apache/tika/parser/xml/ElementMetadataHandler;->bufferBagged:Ljava/lang/StringBuilder;

    .line 62
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lorg/apache/tika/parser/xml/ElementMetadataHandler;->bufferBagless:Ljava/lang/StringBuilder;

    .line 67
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/tika/parser/xml/ElementMetadataHandler;->isBagless:Z

    .line 69
    iput v1, p0, Lorg/apache/tika/parser/xml/ElementMetadataHandler;->matchLevel:I

    .line 70
    iput v1, p0, Lorg/apache/tika/parser/xml/ElementMetadataHandler;->parentMatchLevel:I

    .line 88
    iput-object p1, p0, Lorg/apache/tika/parser/xml/ElementMetadataHandler;->uri:Ljava/lang/String;

    .line 89
    iput-object p2, p0, Lorg/apache/tika/parser/xml/ElementMetadataHandler;->localName:Ljava/lang/String;

    .line 90
    iput-object p3, p0, Lorg/apache/tika/parser/xml/ElementMetadataHandler;->metadata:Lorg/apache/tika/metadata/Metadata;

    .line 91
    iput-object p4, p0, Lorg/apache/tika/parser/xml/ElementMetadataHandler;->targetProperty:Lorg/apache/tika/metadata/Property;

    .line 92
    invoke-virtual {p4}, Lorg/apache/tika/metadata/Property;->getName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/tika/parser/xml/ElementMetadataHandler;->name:Ljava/lang/String;

    .line 97
    return-void
.end method


# virtual methods
.method protected addMetadata(Ljava/lang/String;)V
    .locals 3
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 171
    iget-object v1, p0, Lorg/apache/tika/parser/xml/ElementMetadataHandler;->targetProperty:Lorg/apache/tika/metadata/Property;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lorg/apache/tika/parser/xml/ElementMetadataHandler;->targetProperty:Lorg/apache/tika/metadata/Property;

    invoke-virtual {v1}, Lorg/apache/tika/metadata/Property;->isMultiValuePermitted()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 172
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_1

    .line 173
    iget-object v1, p0, Lorg/apache/tika/parser/xml/ElementMetadataHandler;->metadata:Lorg/apache/tika/metadata/Metadata;

    iget-object v2, p0, Lorg/apache/tika/parser/xml/ElementMetadataHandler;->name:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lorg/apache/tika/metadata/Metadata;->getValues(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 174
    .local v0, "previous":[Ljava/lang/String;
    if-eqz v0, :cond_0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 175
    :cond_0
    iget-object v1, p0, Lorg/apache/tika/parser/xml/ElementMetadataHandler;->metadata:Lorg/apache/tika/metadata/Metadata;

    iget-object v2, p0, Lorg/apache/tika/parser/xml/ElementMetadataHandler;->targetProperty:Lorg/apache/tika/metadata/Property;

    invoke-virtual {v1, v2, p1}, Lorg/apache/tika/metadata/Metadata;->add(Lorg/apache/tika/metadata/Property;Ljava/lang/String;)V

    .line 181
    .end local v0    # "previous":[Ljava/lang/String;
    :cond_1
    :goto_0
    return-void

    .line 179
    :cond_2
    invoke-super {p0, p1}, Lorg/apache/tika/parser/xml/AbstractMetadataHandler;->addMetadata(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public characters([CII)V
    .locals 2
    .param p1, "ch"    # [C
    .param p2, "start"    # I
    .param p3, "length"    # I

    .prologue
    .line 152
    iget v0, p0, Lorg/apache/tika/parser/xml/ElementMetadataHandler;->parentMatchLevel:I

    if-lez v0, :cond_0

    iget v0, p0, Lorg/apache/tika/parser/xml/ElementMetadataHandler;->matchLevel:I

    const/4 v1, 0x2

    if-le v0, v1, :cond_0

    .line 153
    iget-object v0, p0, Lorg/apache/tika/parser/xml/ElementMetadataHandler;->bufferBagged:Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1, p2, p3}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;

    .line 155
    :cond_0
    iget v0, p0, Lorg/apache/tika/parser/xml/ElementMetadataHandler;->parentMatchLevel:I

    if-lez v0, :cond_1

    iget v0, p0, Lorg/apache/tika/parser/xml/ElementMetadataHandler;->matchLevel:I

    if-lez v0, :cond_1

    .line 156
    iget-object v0, p0, Lorg/apache/tika/parser/xml/ElementMetadataHandler;->bufferBagless:Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1, p2, p3}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;

    .line 158
    :cond_1
    return-void
.end method

.method public endElement(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "uri"    # Ljava/lang/String;
    .param p2, "localName"    # Ljava/lang/String;
    .param p3, "name"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 126
    invoke-virtual {p0, p1, p2}, Lorg/apache/tika/parser/xml/ElementMetadataHandler;->isMatchingParentElement(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 127
    iget v1, p0, Lorg/apache/tika/parser/xml/ElementMetadataHandler;->parentMatchLevel:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lorg/apache/tika/parser/xml/ElementMetadataHandler;->parentMatchLevel:I

    .line 129
    :cond_0
    invoke-virtual {p0, p1, p2}, Lorg/apache/tika/parser/xml/ElementMetadataHandler;->isMatchingElement(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 130
    iget v1, p0, Lorg/apache/tika/parser/xml/ElementMetadataHandler;->matchLevel:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lorg/apache/tika/parser/xml/ElementMetadataHandler;->matchLevel:I

    .line 131
    iget v1, p0, Lorg/apache/tika/parser/xml/ElementMetadataHandler;->matchLevel:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    .line 133
    iget-object v1, p0, Lorg/apache/tika/parser/xml/ElementMetadataHandler;->bufferBagged:Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lorg/apache/tika/parser/xml/ElementMetadataHandler;->addMetadata(Ljava/lang/String;)V

    .line 134
    iget-object v1, p0, Lorg/apache/tika/parser/xml/ElementMetadataHandler;->bufferBagged:Ljava/lang/StringBuilder;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 135
    iput-boolean v3, p0, Lorg/apache/tika/parser/xml/ElementMetadataHandler;->isBagless:Z

    .line 137
    :cond_1
    iget v1, p0, Lorg/apache/tika/parser/xml/ElementMetadataHandler;->matchLevel:I

    if-nez v1, :cond_3

    iget-boolean v1, p0, Lorg/apache/tika/parser/xml/ElementMetadataHandler;->isBagless:Z

    if-eqz v1, :cond_3

    .line 138
    iget-object v1, p0, Lorg/apache/tika/parser/xml/ElementMetadataHandler;->bufferBagless:Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 139
    .local v0, "valueBagless":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_2

    const-string/jumbo v1, "Bag"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 141
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lorg/apache/tika/parser/xml/ElementMetadataHandler;->addMetadata(Ljava/lang/String;)V

    .line 142
    iget-object v1, p0, Lorg/apache/tika/parser/xml/ElementMetadataHandler;->bufferBagless:Ljava/lang/StringBuilder;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 144
    :cond_2
    const/4 v1, 0x1

    iput-boolean v1, p0, Lorg/apache/tika/parser/xml/ElementMetadataHandler;->isBagless:Z

    .line 147
    .end local v0    # "valueBagless":Ljava/lang/String;
    :cond_3
    return-void
.end method

.method public ignorableWhitespace([CII)V
    .locals 0
    .param p1, "ch"    # [C
    .param p2, "start"    # I
    .param p3, "length"    # I

    .prologue
    .line 162
    invoke-virtual {p0, p1, p2, p3}, Lorg/apache/tika/parser/xml/ElementMetadataHandler;->characters([CII)V

    .line 163
    return-void
.end method

.method protected isMatchingElement(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1
    .param p1, "uri"    # Ljava/lang/String;
    .param p2, "localName"    # Ljava/lang/String;

    .prologue
    .line 105
    iget-object v0, p0, Lorg/apache/tika/parser/xml/ElementMetadataHandler;->uri:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/tika/parser/xml/ElementMetadataHandler;->localName:Ljava/lang/String;

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 106
    :cond_0
    iget v0, p0, Lorg/apache/tika/parser/xml/ElementMetadataHandler;->parentMatchLevel:I

    if-lez v0, :cond_2

    .line 107
    const-string/jumbo v0, "http://www.w3.org/1999/02/22-rdf-syntax-ns#"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string/jumbo v0, "Bag"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 108
    :cond_1
    const-string/jumbo v0, "http://www.w3.org/1999/02/22-rdf-syntax-ns#"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string/jumbo v0, "li"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 105
    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected isMatchingParentElement(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1
    .param p1, "uri"    # Ljava/lang/String;
    .param p2, "localName"    # Ljava/lang/String;

    .prologue
    .line 100
    iget-object v0, p0, Lorg/apache/tika/parser/xml/ElementMetadataHandler;->uri:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/tika/parser/xml/ElementMetadataHandler;->localName:Ljava/lang/String;

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public startElement(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 1
    .param p1, "uri"    # Ljava/lang/String;
    .param p2, "localName"    # Ljava/lang/String;
    .param p3, "name"    # Ljava/lang/String;
    .param p4, "attributes"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 116
    invoke-virtual {p0, p1, p2}, Lorg/apache/tika/parser/xml/ElementMetadataHandler;->isMatchingElement(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 117
    iget v0, p0, Lorg/apache/tika/parser/xml/ElementMetadataHandler;->matchLevel:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/tika/parser/xml/ElementMetadataHandler;->matchLevel:I

    .line 119
    :cond_0
    invoke-virtual {p0, p1, p2}, Lorg/apache/tika/parser/xml/ElementMetadataHandler;->isMatchingParentElement(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 120
    iget v0, p0, Lorg/apache/tika/parser/xml/ElementMetadataHandler;->parentMatchLevel:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/tika/parser/xml/ElementMetadataHandler;->parentMatchLevel:I

    .line 122
    :cond_1
    return-void
.end method
