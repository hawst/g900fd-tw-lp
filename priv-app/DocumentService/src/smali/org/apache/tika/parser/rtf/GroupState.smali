.class Lorg/apache/tika/parser/rtf/GroupState;
.super Ljava/lang/Object;
.source "GroupState.java"


# instance fields
.field public bold:Z

.field public depth:I

.field public fontCharset:Ljava/nio/charset/Charset;

.field public ignore:Z

.field public italic:Z

.field public ucSkip:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    const/4 v0, 0x1

    iput v0, p0, Lorg/apache/tika/parser/rtf/GroupState;->ucSkip:I

    .line 37
    return-void
.end method

.method public constructor <init>(Lorg/apache/tika/parser/rtf/GroupState;)V
    .locals 1
    .param p1, "other"    # Lorg/apache/tika/parser/rtf/GroupState;

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    const/4 v0, 0x1

    iput v0, p0, Lorg/apache/tika/parser/rtf/GroupState;->ucSkip:I

    .line 41
    iget-boolean v0, p1, Lorg/apache/tika/parser/rtf/GroupState;->bold:Z

    iput-boolean v0, p0, Lorg/apache/tika/parser/rtf/GroupState;->bold:Z

    .line 42
    iget-boolean v0, p1, Lorg/apache/tika/parser/rtf/GroupState;->italic:Z

    iput-boolean v0, p0, Lorg/apache/tika/parser/rtf/GroupState;->italic:Z

    .line 43
    iget-boolean v0, p1, Lorg/apache/tika/parser/rtf/GroupState;->ignore:Z

    iput-boolean v0, p0, Lorg/apache/tika/parser/rtf/GroupState;->ignore:Z

    .line 44
    iget v0, p1, Lorg/apache/tika/parser/rtf/GroupState;->ucSkip:I

    iput v0, p0, Lorg/apache/tika/parser/rtf/GroupState;->ucSkip:I

    .line 45
    iget-object v0, p1, Lorg/apache/tika/parser/rtf/GroupState;->fontCharset:Ljava/nio/charset/Charset;

    iput-object v0, p0, Lorg/apache/tika/parser/rtf/GroupState;->fontCharset:Ljava/nio/charset/Charset;

    .line 46
    iget v0, p1, Lorg/apache/tika/parser/rtf/GroupState;->depth:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/tika/parser/rtf/GroupState;->depth:I

    .line 47
    return-void
.end method
