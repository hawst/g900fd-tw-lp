.class public Lorg/apache/tika/parser/xml/DcXMLParser;
.super Lorg/apache/tika/parser/xml/XMLParser;
.source "DcXMLParser.java"


# static fields
.field private static final serialVersionUID:J = 0x4413317dc9db8073L


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Lorg/apache/tika/parser/xml/XMLParser;-><init>()V

    return-void
.end method

.method private static getDublinCoreHandler(Lorg/apache/tika/metadata/Metadata;Lorg/apache/tika/metadata/Property;Ljava/lang/String;)Lorg/xml/sax/ContentHandler;
    .locals 2
    .param p0, "metadata"    # Lorg/apache/tika/metadata/Metadata;
    .param p1, "property"    # Lorg/apache/tika/metadata/Property;
    .param p2, "element"    # Ljava/lang/String;

    .prologue
    .line 37
    new-instance v0, Lorg/apache/tika/parser/xml/ElementMetadataHandler;

    .line 38
    const-string/jumbo v1, "http://purl.org/dc/elements/1.1/"

    .line 37
    invoke-direct {v0, v1, p2, p0, p1}, Lorg/apache/tika/parser/xml/ElementMetadataHandler;-><init>(Ljava/lang/String;Ljava/lang/String;Lorg/apache/tika/metadata/Metadata;Lorg/apache/tika/metadata/Property;)V

    return-object v0
.end method


# virtual methods
.method protected getContentHandler(Lorg/xml/sax/ContentHandler;Lorg/apache/tika/metadata/Metadata;Lorg/apache/tika/parser/ParseContext;)Lorg/xml/sax/ContentHandler;
    .locals 5
    .param p1, "handler"    # Lorg/xml/sax/ContentHandler;
    .param p2, "metadata"    # Lorg/apache/tika/metadata/Metadata;
    .param p3, "context"    # Lorg/apache/tika/parser/ParseContext;

    .prologue
    .line 44
    new-instance v0, Lorg/apache/tika/sax/TeeContentHandler;

    const/16 v1, 0xd

    new-array v1, v1, [Lorg/xml/sax/ContentHandler;

    const/4 v2, 0x0

    .line 45
    invoke-super {p0, p1, p2, p3}, Lorg/apache/tika/parser/xml/XMLParser;->getContentHandler(Lorg/xml/sax/ContentHandler;Lorg/apache/tika/metadata/Metadata;Lorg/apache/tika/parser/ParseContext;)Lorg/xml/sax/ContentHandler;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    .line 46
    sget-object v3, Lorg/apache/tika/metadata/TikaCoreProperties;->TITLE:Lorg/apache/tika/metadata/Property;

    const-string/jumbo v4, "title"

    invoke-static {p2, v3, v4}, Lorg/apache/tika/parser/xml/DcXMLParser;->getDublinCoreHandler(Lorg/apache/tika/metadata/Metadata;Lorg/apache/tika/metadata/Property;Ljava/lang/String;)Lorg/xml/sax/ContentHandler;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    .line 47
    sget-object v3, Lorg/apache/tika/metadata/TikaCoreProperties;->KEYWORDS:Lorg/apache/tika/metadata/Property;

    const-string/jumbo v4, "subject"

    invoke-static {p2, v3, v4}, Lorg/apache/tika/parser/xml/DcXMLParser;->getDublinCoreHandler(Lorg/apache/tika/metadata/Metadata;Lorg/apache/tika/metadata/Property;Ljava/lang/String;)Lorg/xml/sax/ContentHandler;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    .line 48
    sget-object v3, Lorg/apache/tika/metadata/TikaCoreProperties;->CREATOR:Lorg/apache/tika/metadata/Property;

    const-string/jumbo v4, "creator"

    invoke-static {p2, v3, v4}, Lorg/apache/tika/parser/xml/DcXMLParser;->getDublinCoreHandler(Lorg/apache/tika/metadata/Metadata;Lorg/apache/tika/metadata/Property;Ljava/lang/String;)Lorg/xml/sax/ContentHandler;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x4

    .line 49
    sget-object v3, Lorg/apache/tika/metadata/TikaCoreProperties;->DESCRIPTION:Lorg/apache/tika/metadata/Property;

    const-string/jumbo v4, "description"

    invoke-static {p2, v3, v4}, Lorg/apache/tika/parser/xml/DcXMLParser;->getDublinCoreHandler(Lorg/apache/tika/metadata/Metadata;Lorg/apache/tika/metadata/Property;Ljava/lang/String;)Lorg/xml/sax/ContentHandler;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x5

    .line 50
    sget-object v3, Lorg/apache/tika/metadata/TikaCoreProperties;->PUBLISHER:Lorg/apache/tika/metadata/Property;

    const-string/jumbo v4, "publisher"

    invoke-static {p2, v3, v4}, Lorg/apache/tika/parser/xml/DcXMLParser;->getDublinCoreHandler(Lorg/apache/tika/metadata/Metadata;Lorg/apache/tika/metadata/Property;Ljava/lang/String;)Lorg/xml/sax/ContentHandler;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x6

    .line 51
    sget-object v3, Lorg/apache/tika/metadata/TikaCoreProperties;->CONTRIBUTOR:Lorg/apache/tika/metadata/Property;

    const-string/jumbo v4, "contributor"

    invoke-static {p2, v3, v4}, Lorg/apache/tika/parser/xml/DcXMLParser;->getDublinCoreHandler(Lorg/apache/tika/metadata/Metadata;Lorg/apache/tika/metadata/Property;Ljava/lang/String;)Lorg/xml/sax/ContentHandler;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x7

    .line 52
    sget-object v3, Lorg/apache/tika/metadata/TikaCoreProperties;->CREATED:Lorg/apache/tika/metadata/Property;

    const-string/jumbo v4, "date"

    invoke-static {p2, v3, v4}, Lorg/apache/tika/parser/xml/DcXMLParser;->getDublinCoreHandler(Lorg/apache/tika/metadata/Metadata;Lorg/apache/tika/metadata/Property;Ljava/lang/String;)Lorg/xml/sax/ContentHandler;

    move-result-object v3

    aput-object v3, v1, v2

    const/16 v2, 0x8

    .line 53
    sget-object v3, Lorg/apache/tika/metadata/TikaCoreProperties;->TYPE:Lorg/apache/tika/metadata/Property;

    const-string/jumbo v4, "type"

    invoke-static {p2, v3, v4}, Lorg/apache/tika/parser/xml/DcXMLParser;->getDublinCoreHandler(Lorg/apache/tika/metadata/Metadata;Lorg/apache/tika/metadata/Property;Ljava/lang/String;)Lorg/xml/sax/ContentHandler;

    move-result-object v3

    aput-object v3, v1, v2

    const/16 v2, 0x9

    .line 54
    sget-object v3, Lorg/apache/tika/metadata/TikaCoreProperties;->FORMAT:Lorg/apache/tika/metadata/Property;

    const-string/jumbo v4, "format"

    invoke-static {p2, v3, v4}, Lorg/apache/tika/parser/xml/DcXMLParser;->getDublinCoreHandler(Lorg/apache/tika/metadata/Metadata;Lorg/apache/tika/metadata/Property;Ljava/lang/String;)Lorg/xml/sax/ContentHandler;

    move-result-object v3

    aput-object v3, v1, v2

    const/16 v2, 0xa

    .line 55
    sget-object v3, Lorg/apache/tika/metadata/TikaCoreProperties;->IDENTIFIER:Lorg/apache/tika/metadata/Property;

    const-string/jumbo v4, "identifier"

    invoke-static {p2, v3, v4}, Lorg/apache/tika/parser/xml/DcXMLParser;->getDublinCoreHandler(Lorg/apache/tika/metadata/Metadata;Lorg/apache/tika/metadata/Property;Ljava/lang/String;)Lorg/xml/sax/ContentHandler;

    move-result-object v3

    aput-object v3, v1, v2

    const/16 v2, 0xb

    .line 56
    sget-object v3, Lorg/apache/tika/metadata/TikaCoreProperties;->LANGUAGE:Lorg/apache/tika/metadata/Property;

    const-string/jumbo v4, "language"

    invoke-static {p2, v3, v4}, Lorg/apache/tika/parser/xml/DcXMLParser;->getDublinCoreHandler(Lorg/apache/tika/metadata/Metadata;Lorg/apache/tika/metadata/Property;Ljava/lang/String;)Lorg/xml/sax/ContentHandler;

    move-result-object v3

    aput-object v3, v1, v2

    const/16 v2, 0xc

    .line 57
    sget-object v3, Lorg/apache/tika/metadata/TikaCoreProperties;->RIGHTS:Lorg/apache/tika/metadata/Property;

    const-string/jumbo v4, "rights"

    invoke-static {p2, v3, v4}, Lorg/apache/tika/parser/xml/DcXMLParser;->getDublinCoreHandler(Lorg/apache/tika/metadata/Metadata;Lorg/apache/tika/metadata/Property;Ljava/lang/String;)Lorg/xml/sax/ContentHandler;

    move-result-object v3

    .line 44
    aput-object v3, v1, v2

    invoke-direct {v0, v1}, Lorg/apache/tika/sax/TeeContentHandler;-><init>([Lorg/xml/sax/ContentHandler;)V

    return-object v0
.end method
