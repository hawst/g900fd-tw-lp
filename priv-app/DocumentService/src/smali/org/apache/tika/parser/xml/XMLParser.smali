.class public Lorg/apache/tika/parser/xml/XMLParser;
.super Lorg/apache/tika/parser/AbstractParser;
.source "XMLParser.java"


# static fields
.field private static final SUPPORTED_TYPES:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lorg/apache/tika/mime/MediaType;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J = -0x53aabb0f3b280765L


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 50
    new-instance v0, Ljava/util/HashSet;

    const/4 v1, 0x2

    new-array v1, v1, [Lorg/apache/tika/mime/MediaType;

    const/4 v2, 0x0

    .line 51
    const-string/jumbo v3, "xml"

    invoke-static {v3}, Lorg/apache/tika/mime/MediaType;->application(Ljava/lang/String;)Lorg/apache/tika/mime/MediaType;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    .line 52
    const-string/jumbo v3, "svg+xml"

    invoke-static {v3}, Lorg/apache/tika/mime/MediaType;->image(Ljava/lang/String;)Lorg/apache/tika/mime/MediaType;

    move-result-object v3

    aput-object v3, v1, v2

    .line 50
    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    .line 49
    sput-object v0, Lorg/apache/tika/parser/xml/XMLParser;->SUPPORTED_TYPES:Ljava/util/Set;

    .line 52
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0}, Lorg/apache/tika/parser/AbstractParser;-><init>()V

    return-void
.end method


# virtual methods
.method protected getContentHandler(Lorg/xml/sax/ContentHandler;Lorg/apache/tika/metadata/Metadata;Lorg/apache/tika/parser/ParseContext;)Lorg/xml/sax/ContentHandler;
    .locals 2
    .param p1, "handler"    # Lorg/xml/sax/ContentHandler;
    .param p2, "metadata"    # Lorg/apache/tika/metadata/Metadata;
    .param p3, "context"    # Lorg/apache/tika/parser/ParseContext;

    .prologue
    .line 88
    new-instance v0, Lorg/apache/tika/sax/TextContentHandler;

    const/4 v1, 0x1

    invoke-direct {v0, p1, v1}, Lorg/apache/tika/sax/TextContentHandler;-><init>(Lorg/xml/sax/ContentHandler;Z)V

    return-object v0
.end method

.method public getSupportedTypes(Lorg/apache/tika/parser/ParseContext;)Ljava/util/Set;
    .locals 1
    .param p1, "context"    # Lorg/apache/tika/parser/ParseContext;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/tika/parser/ParseContext;",
            ")",
            "Ljava/util/Set",
            "<",
            "Lorg/apache/tika/mime/MediaType;",
            ">;"
        }
    .end annotation

    .prologue
    .line 55
    sget-object v0, Lorg/apache/tika/parser/xml/XMLParser;->SUPPORTED_TYPES:Ljava/util/Set;

    return-object v0
.end method

.method public parse(Ljava/io/InputStream;Lorg/xml/sax/ContentHandler;Lorg/apache/tika/metadata/Metadata;Lorg/apache/tika/parser/ParseContext;)V
    .locals 8
    .param p1, "stream"    # Ljava/io/InputStream;
    .param p2, "handler"    # Lorg/xml/sax/ContentHandler;
    .param p3, "metadata"    # Lorg/apache/tika/metadata/Metadata;
    .param p4, "context"    # Lorg/apache/tika/parser/ParseContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/xml/sax/SAXException;,
            Lorg/apache/tika/exception/TikaException;
        }
    .end annotation

    .prologue
    .line 62
    const-string/jumbo v3, "Content-Type"

    invoke-virtual {p3, v3}, Lorg/apache/tika/metadata/Metadata;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_0

    .line 63
    const-string/jumbo v3, "Content-Type"

    const-string/jumbo v4, "application/xml"

    invoke-virtual {p3, v3, v4}, Lorg/apache/tika/metadata/Metadata;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 67
    :cond_0
    new-instance v2, Lorg/apache/tika/sax/XHTMLContentHandler;

    invoke-direct {v2, p2, p3}, Lorg/apache/tika/sax/XHTMLContentHandler;-><init>(Lorg/xml/sax/ContentHandler;Lorg/apache/tika/metadata/Metadata;)V

    .line 68
    .local v2, "xhtml":Lorg/apache/tika/sax/XHTMLContentHandler;
    invoke-virtual {v2}, Lorg/apache/tika/sax/XHTMLContentHandler;->startDocument()V

    .line 69
    const-string/jumbo v3, "p"

    invoke-virtual {v2, v3}, Lorg/apache/tika/sax/XHTMLContentHandler;->startElement(Ljava/lang/String;)V

    .line 71
    new-instance v1, Lorg/apache/tika/sax/TaggedContentHandler;

    invoke-direct {v1, p2}, Lorg/apache/tika/sax/TaggedContentHandler;-><init>(Lorg/xml/sax/ContentHandler;)V

    .line 73
    .local v1, "tagged":Lorg/apache/tika/sax/TaggedContentHandler;
    :try_start_0
    invoke-virtual {p4}, Lorg/apache/tika/parser/ParseContext;->getSAXParser()Ljavax/xml/parsers/SAXParser;

    move-result-object v3

    .line 74
    new-instance v4, Lorg/apache/tika/io/CloseShieldInputStream;

    invoke-direct {v4, p1}, Lorg/apache/tika/io/CloseShieldInputStream;-><init>(Ljava/io/InputStream;)V

    .line 75
    new-instance v5, Lorg/apache/tika/sax/OfflineContentHandler;

    new-instance v6, Lorg/apache/tika/sax/EmbeddedContentHandler;

    .line 76
    invoke-virtual {p0, v1, p3, p4}, Lorg/apache/tika/parser/xml/XMLParser;->getContentHandler(Lorg/xml/sax/ContentHandler;Lorg/apache/tika/metadata/Metadata;Lorg/apache/tika/parser/ParseContext;)Lorg/xml/sax/ContentHandler;

    move-result-object v7

    invoke-direct {v6, v7}, Lorg/apache/tika/sax/EmbeddedContentHandler;-><init>(Lorg/xml/sax/ContentHandler;)V

    .line 75
    invoke-direct {v5, v6}, Lorg/apache/tika/sax/OfflineContentHandler;-><init>(Lorg/xml/sax/ContentHandler;)V

    .line 73
    invoke-virtual {v3, v4, v5}, Ljavax/xml/parsers/SAXParser;->parse(Ljava/io/InputStream;Lorg/xml/sax/helpers/DefaultHandler;)V
    :try_end_0
    .catch Lorg/xml/sax/SAXException; {:try_start_0 .. :try_end_0} :catch_0

    .line 82
    const-string/jumbo v3, "p"

    invoke-virtual {v2, v3}, Lorg/apache/tika/sax/XHTMLContentHandler;->endElement(Ljava/lang/String;)V

    .line 83
    invoke-virtual {v2}, Lorg/apache/tika/sax/XHTMLContentHandler;->endDocument()V

    .line 84
    return-void

    .line 77
    :catch_0
    move-exception v0

    .line 78
    .local v0, "e":Lorg/xml/sax/SAXException;
    invoke-virtual {v1, v0}, Lorg/apache/tika/sax/TaggedContentHandler;->throwIfCauseOf(Ljava/lang/Exception;)V

    .line 79
    new-instance v3, Lorg/apache/tika/exception/TikaException;

    const-string/jumbo v4, "XML parse error"

    invoke-direct {v3, v4, v0}, Lorg/apache/tika/exception/TikaException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v3
.end method
