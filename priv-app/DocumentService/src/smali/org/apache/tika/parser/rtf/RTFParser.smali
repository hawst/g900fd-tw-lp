.class public Lorg/apache/tika/parser/rtf/RTFParser;
.super Lorg/apache/tika/parser/AbstractParser;
.source "RTFParser.java"


# static fields
.field private static final SUPPORTED_TYPES:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lorg/apache/tika/mime/MediaType;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J = -0x39cd4ca212dad239L


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 44
    const-string/jumbo v0, "rtf"

    invoke-static {v0}, Lorg/apache/tika/mime/MediaType;->application(Ljava/lang/String;)Lorg/apache/tika/mime/MediaType;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    .line 43
    sput-object v0, Lorg/apache/tika/parser/rtf/RTFParser;->SUPPORTED_TYPES:Ljava/util/Set;

    .line 44
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Lorg/apache/tika/parser/AbstractParser;-><init>()V

    return-void
.end method


# virtual methods
.method public getSupportedTypes(Lorg/apache/tika/parser/ParseContext;)Ljava/util/Set;
    .locals 1
    .param p1, "context"    # Lorg/apache/tika/parser/ParseContext;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/tika/parser/ParseContext;",
            ")",
            "Ljava/util/Set",
            "<",
            "Lorg/apache/tika/mime/MediaType;",
            ">;"
        }
    .end annotation

    .prologue
    .line 47
    sget-object v0, Lorg/apache/tika/parser/rtf/RTFParser;->SUPPORTED_TYPES:Ljava/util/Set;

    return-object v0
.end method

.method public parse(Ljava/io/InputStream;Lorg/xml/sax/ContentHandler;Lorg/apache/tika/metadata/Metadata;Lorg/apache/tika/parser/ParseContext;)V
    .locals 5
    .param p1, "stream"    # Ljava/io/InputStream;
    .param p2, "handler"    # Lorg/xml/sax/ContentHandler;
    .param p3, "metadata"    # Lorg/apache/tika/metadata/Metadata;
    .param p4, "context"    # Lorg/apache/tika/parser/ParseContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/xml/sax/SAXException;,
            Lorg/apache/tika/exception/TikaException;
        }
    .end annotation

    .prologue
    .line 54
    new-instance v2, Lorg/apache/tika/io/TaggedInputStream;

    invoke-direct {v2, p1}, Lorg/apache/tika/io/TaggedInputStream;-><init>(Ljava/io/InputStream;)V

    .line 56
    .local v2, "tagged":Lorg/apache/tika/io/TaggedInputStream;
    :try_start_0
    new-instance v1, Lorg/apache/tika/parser/rtf/TextExtractor;

    new-instance v3, Lorg/apache/tika/sax/XHTMLContentHandler;

    invoke-direct {v3, p2, p3}, Lorg/apache/tika/sax/XHTMLContentHandler;-><init>(Lorg/xml/sax/ContentHandler;Lorg/apache/tika/metadata/Metadata;)V

    invoke-direct {v1, v3, p3}, Lorg/apache/tika/parser/rtf/TextExtractor;-><init>(Lorg/apache/tika/sax/XHTMLContentHandler;Lorg/apache/tika/metadata/Metadata;)V

    .line 57
    .local v1, "ert":Lorg/apache/tika/parser/rtf/TextExtractor;
    invoke-virtual {v1, p1}, Lorg/apache/tika/parser/rtf/TextExtractor;->extract(Ljava/io/InputStream;)V

    .line 58
    const-string/jumbo v3, "Content-Type"

    const-string/jumbo v4, "application/rtf"

    invoke-virtual {p3, v3, v4}, Lorg/apache/tika/metadata/Metadata;->add(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 63
    return-void

    .line 59
    .end local v1    # "ert":Lorg/apache/tika/parser/rtf/TextExtractor;
    :catch_0
    move-exception v0

    .line 60
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v2, v0}, Lorg/apache/tika/io/TaggedInputStream;->throwIfCauseOf(Ljava/lang/Exception;)V

    .line 61
    new-instance v3, Lorg/apache/tika/exception/TikaException;

    const-string/jumbo v4, "Error parsing an RTF document"

    invoke-direct {v3, v4, v0}, Lorg/apache/tika/exception/TikaException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v3
.end method
