.class Lorg/apache/index/poi/hpsf/MutableSection$1;
.super Ljava/lang/Object;
.source "MutableSection.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/index/poi/hpsf/MutableSection;->write(Ljava/io/OutputStream;)I
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lorg/apache/index/poi/hpsf/Property;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lorg/apache/index/poi/hpsf/MutableSection;


# direct methods
.method constructor <init>(Lorg/apache/index/poi/hpsf/MutableSection;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lorg/apache/index/poi/hpsf/MutableSection$1;->this$0:Lorg/apache/index/poi/hpsf/MutableSection;

    .line 426
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 1
    check-cast p1, Lorg/apache/index/poi/hpsf/Property;

    check-cast p2, Lorg/apache/index/poi/hpsf/Property;

    invoke-virtual {p0, p1, p2}, Lorg/apache/index/poi/hpsf/MutableSection$1;->compare(Lorg/apache/index/poi/hpsf/Property;Lorg/apache/index/poi/hpsf/Property;)I

    move-result v0

    return v0
.end method

.method public compare(Lorg/apache/index/poi/hpsf/Property;Lorg/apache/index/poi/hpsf/Property;)I
    .locals 4
    .param p1, "p1"    # Lorg/apache/index/poi/hpsf/Property;
    .param p2, "p2"    # Lorg/apache/index/poi/hpsf/Property;

    .prologue
    .line 430
    invoke-virtual {p1}, Lorg/apache/index/poi/hpsf/Property;->getID()J

    move-result-wide v0

    invoke-virtual {p2}, Lorg/apache/index/poi/hpsf/Property;->getID()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    .line 431
    const/4 v0, -0x1

    .line 435
    :goto_0
    return v0

    .line 432
    :cond_0
    invoke-virtual {p1}, Lorg/apache/index/poi/hpsf/Property;->getID()J

    move-result-wide v0

    invoke-virtual {p2}, Lorg/apache/index/poi/hpsf/Property;->getID()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 433
    const/4 v0, 0x0

    goto :goto_0

    .line 435
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method
