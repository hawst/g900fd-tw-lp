.class public Lorg/apache/index/poi/hwpf/model/PAPBinTable;
.super Ljava/lang/Object;
.source "PAPBinTable.java"


# instance fields
.field _dataStream:[B

.field protected _paragraphs:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lorg/apache/index/poi/hwpf/model/PAPX;",
            ">;"
        }
    .end annotation
.end field

.field private tpt:Lorg/apache/index/poi/hwpf/model/TextPieceTable;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/apache/index/poi/hwpf/model/PAPBinTable;->_paragraphs:Ljava/util/ArrayList;

    .line 47
    return-void
.end method

.method public constructor <init>([B[B[BIIILorg/apache/index/poi/hwpf/model/TextPieceTable;)V
    .locals 17
    .param p1, "documentStream"    # [B
    .param p2, "tableStream"    # [B
    .param p3, "dataStream"    # [B
    .param p4, "offset"    # I
    .param p5, "size"    # I
    .param p6, "fcMin"    # I
    .param p7, "tpt"    # Lorg/apache/index/poi/hwpf/model/TextPieceTable;

    .prologue
    .line 49
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    .line 39
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iput-object v4, v0, Lorg/apache/index/poi/hwpf/model/PAPBinTable;->_paragraphs:Ljava/util/ArrayList;

    .line 52
    new-instance v9, Lorg/apache/index/poi/hwpf/model/PlexOfCps;

    const/4 v4, 0x4

    move-object/from16 v0, p2

    move/from16 v1, p4

    move/from16 v2, p5

    invoke-direct {v9, v0, v1, v2, v4}, Lorg/apache/index/poi/hwpf/model/PlexOfCps;-><init>([BIII)V

    .line 53
    .local v9, "binTable":Lorg/apache/index/poi/hwpf/model/PlexOfCps;
    move-object/from16 v0, p7

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/apache/index/poi/hwpf/model/PAPBinTable;->tpt:Lorg/apache/index/poi/hwpf/model/TextPieceTable;

    .line 55
    invoke-virtual {v9}, Lorg/apache/index/poi/hwpf/model/PlexOfCps;->length()I

    move-result v11

    .line 56
    .local v11, "length":I
    const/4 v15, 0x0

    .local v15, "x":I
    :goto_0
    if-lt v15, v11, :cond_0

    .line 74
    move-object/from16 v0, p3

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/apache/index/poi/hwpf/model/PAPBinTable;->_dataStream:[B

    .line 75
    return-void

    .line 58
    :cond_0
    invoke-virtual {v9, v15}, Lorg/apache/index/poi/hwpf/model/PlexOfCps;->getProperty(I)Lorg/apache/index/poi/hwpf/model/GenericPropertyNode;

    move-result-object v12

    .line 60
    .local v12, "node":Lorg/apache/index/poi/hwpf/model/GenericPropertyNode;
    invoke-virtual {v12}, Lorg/apache/index/poi/hwpf/model/GenericPropertyNode;->getBytes()[B

    move-result-object v4

    invoke-static {v4}, Lorg/apache/index/poi/util/LittleEndian;->getInt([B)I

    move-result v13

    .line 61
    .local v13, "pageNum":I
    mul-int/lit16 v6, v13, 0x200

    .line 63
    .local v6, "pageOffset":I
    new-instance v3, Lorg/apache/index/poi/hwpf/model/PAPFormattedDiskPage;

    move-object/from16 v4, p1

    move-object/from16 v5, p3

    move/from16 v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v3 .. v8}, Lorg/apache/index/poi/hwpf/model/PAPFormattedDiskPage;-><init>([B[BIILorg/apache/index/poi/hwpf/model/TextPieceTable;)V

    .line 66
    .local v3, "pfkp":Lorg/apache/index/poi/hwpf/model/PAPFormattedDiskPage;
    invoke-virtual {v3}, Lorg/apache/index/poi/hwpf/model/PAPFormattedDiskPage;->size()I

    move-result v10

    .line 68
    .local v10, "fkpSize":I
    const/16 v16, 0x0

    .local v16, "y":I
    :goto_1
    move/from16 v0, v16

    if-lt v0, v10, :cond_1

    .line 56
    add-int/lit8 v15, v15, 0x1

    goto :goto_0

    .line 70
    :cond_1
    move/from16 v0, v16

    invoke-virtual {v3, v0}, Lorg/apache/index/poi/hwpf/model/PAPFormattedDiskPage;->getPAPX(I)Lorg/apache/index/poi/hwpf/model/PAPX;

    move-result-object v14

    .line 71
    .local v14, "papx":Lorg/apache/index/poi/hwpf/model/PAPX;
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/index/poi/hwpf/model/PAPBinTable;->_paragraphs:Ljava/util/ArrayList;

    invoke-virtual {v4, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 68
    add-int/lit8 v16, v16, 0x1

    goto :goto_1
.end method


# virtual methods
.method public adjustForDelete(III)V
    .locals 6
    .param p1, "listIndex"    # I
    .param p2, "offset"    # I
    .param p3, "length"    # I

    .prologue
    .line 130
    iget-object v5, p0, Lorg/apache/index/poi/hwpf/model/PAPBinTable;->_paragraphs:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 131
    .local v3, "size":I
    add-int v1, p2, p3

    .line 132
    .local v1, "endMark":I
    move v0, p1

    .line 134
    .local v0, "endIndex":I
    iget-object v5, p0, Lorg/apache/index/poi/hwpf/model/PAPBinTable;->_paragraphs:Ljava/util/ArrayList;

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/index/poi/hwpf/model/PAPX;

    .line 135
    .local v2, "papx":Lorg/apache/index/poi/hwpf/model/PAPX;
    :goto_0
    invoke-virtual {v2}, Lorg/apache/index/poi/hwpf/model/PAPX;->getEnd()I

    move-result v5

    if-lt v5, v1, :cond_0

    .line 139
    if-ne p1, v0, :cond_1

    .line 141
    iget-object v5, p0, Lorg/apache/index/poi/hwpf/model/PAPBinTable;->_paragraphs:Ljava/util/ArrayList;

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "papx":Lorg/apache/index/poi/hwpf/model/PAPX;
    check-cast v2, Lorg/apache/index/poi/hwpf/model/PAPX;

    .line 142
    .restart local v2    # "papx":Lorg/apache/index/poi/hwpf/model/PAPX;
    invoke-virtual {v2}, Lorg/apache/index/poi/hwpf/model/PAPX;->getEnd()I

    move-result v5

    sub-int/2addr v5, v1

    add-int/2addr v5, p2

    invoke-virtual {v2, v5}, Lorg/apache/index/poi/hwpf/model/PAPX;->setEnd(I)V

    .line 158
    :goto_1
    add-int/lit8 v4, v0, 0x1

    .local v4, "x":I
    :goto_2
    if-lt v4, v3, :cond_3

    .line 164
    return-void

    .line 137
    .end local v4    # "x":I
    :cond_0
    iget-object v5, p0, Lorg/apache/index/poi/hwpf/model/PAPBinTable;->_paragraphs:Ljava/util/ArrayList;

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "papx":Lorg/apache/index/poi/hwpf/model/PAPX;
    check-cast v2, Lorg/apache/index/poi/hwpf/model/PAPX;

    .restart local v2    # "papx":Lorg/apache/index/poi/hwpf/model/PAPX;
    goto :goto_0

    .line 146
    :cond_1
    iget-object v5, p0, Lorg/apache/index/poi/hwpf/model/PAPBinTable;->_paragraphs:Ljava/util/ArrayList;

    invoke-virtual {v5, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "papx":Lorg/apache/index/poi/hwpf/model/PAPX;
    check-cast v2, Lorg/apache/index/poi/hwpf/model/PAPX;

    .line 147
    .restart local v2    # "papx":Lorg/apache/index/poi/hwpf/model/PAPX;
    invoke-virtual {v2, p2}, Lorg/apache/index/poi/hwpf/model/PAPX;->setEnd(I)V

    .line 148
    add-int/lit8 v4, p1, 0x1

    .restart local v4    # "x":I
    :goto_3
    if-lt v4, v0, :cond_2

    .line 154
    iget-object v5, p0, Lorg/apache/index/poi/hwpf/model/PAPBinTable;->_paragraphs:Ljava/util/ArrayList;

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "papx":Lorg/apache/index/poi/hwpf/model/PAPX;
    check-cast v2, Lorg/apache/index/poi/hwpf/model/PAPX;

    .line 155
    .restart local v2    # "papx":Lorg/apache/index/poi/hwpf/model/PAPX;
    invoke-virtual {v2}, Lorg/apache/index/poi/hwpf/model/PAPX;->getEnd()I

    move-result v5

    sub-int/2addr v5, v1

    add-int/2addr v5, p2

    invoke-virtual {v2, v5}, Lorg/apache/index/poi/hwpf/model/PAPX;->setEnd(I)V

    goto :goto_1

    .line 150
    :cond_2
    iget-object v5, p0, Lorg/apache/index/poi/hwpf/model/PAPBinTable;->_paragraphs:Ljava/util/ArrayList;

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "papx":Lorg/apache/index/poi/hwpf/model/PAPX;
    check-cast v2, Lorg/apache/index/poi/hwpf/model/PAPX;

    .line 151
    .restart local v2    # "papx":Lorg/apache/index/poi/hwpf/model/PAPX;
    invoke-virtual {v2, p2}, Lorg/apache/index/poi/hwpf/model/PAPX;->setStart(I)V

    .line 152
    invoke-virtual {v2, p2}, Lorg/apache/index/poi/hwpf/model/PAPX;->setEnd(I)V

    .line 148
    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    .line 160
    :cond_3
    iget-object v5, p0, Lorg/apache/index/poi/hwpf/model/PAPBinTable;->_paragraphs:Ljava/util/ArrayList;

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "papx":Lorg/apache/index/poi/hwpf/model/PAPX;
    check-cast v2, Lorg/apache/index/poi/hwpf/model/PAPX;

    .line 161
    .restart local v2    # "papx":Lorg/apache/index/poi/hwpf/model/PAPX;
    invoke-virtual {v2}, Lorg/apache/index/poi/hwpf/model/PAPX;->getStart()I

    move-result v5

    sub-int/2addr v5, p3

    invoke-virtual {v2, v5}, Lorg/apache/index/poi/hwpf/model/PAPX;->setStart(I)V

    .line 162
    invoke-virtual {v2}, Lorg/apache/index/poi/hwpf/model/PAPX;->getEnd()I

    move-result v5

    sub-int/2addr v5, p3

    invoke-virtual {v2, v5}, Lorg/apache/index/poi/hwpf/model/PAPX;->setEnd(I)V

    .line 158
    add-int/lit8 v4, v4, 0x1

    goto :goto_2
.end method

.method public adjustForInsert(II)V
    .locals 4
    .param p1, "listIndex"    # I
    .param p2, "length"    # I

    .prologue
    .line 169
    iget-object v3, p0, Lorg/apache/index/poi/hwpf/model/PAPBinTable;->_paragraphs:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 170
    .local v1, "size":I
    iget-object v3, p0, Lorg/apache/index/poi/hwpf/model/PAPBinTable;->_paragraphs:Ljava/util/ArrayList;

    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/index/poi/hwpf/model/PAPX;

    .line 171
    .local v0, "papx":Lorg/apache/index/poi/hwpf/model/PAPX;
    invoke-virtual {v0}, Lorg/apache/index/poi/hwpf/model/PAPX;->getEnd()I

    move-result v3

    add-int/2addr v3, p2

    invoke-virtual {v0, v3}, Lorg/apache/index/poi/hwpf/model/PAPX;->setEnd(I)V

    .line 173
    add-int/lit8 v2, p1, 0x1

    .local v2, "x":I
    :goto_0
    if-lt v2, v1, :cond_0

    .line 179
    return-void

    .line 175
    :cond_0
    iget-object v3, p0, Lorg/apache/index/poi/hwpf/model/PAPBinTable;->_paragraphs:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "papx":Lorg/apache/index/poi/hwpf/model/PAPX;
    check-cast v0, Lorg/apache/index/poi/hwpf/model/PAPX;

    .line 176
    .restart local v0    # "papx":Lorg/apache/index/poi/hwpf/model/PAPX;
    invoke-virtual {v0}, Lorg/apache/index/poi/hwpf/model/PAPX;->getStart()I

    move-result v3

    add-int/2addr v3, p2

    invoke-virtual {v0, v3}, Lorg/apache/index/poi/hwpf/model/PAPX;->setStart(I)V

    .line 177
    invoke-virtual {v0}, Lorg/apache/index/poi/hwpf/model/PAPX;->getEnd()I

    move-result v3

    add-int/2addr v3, p2

    invoke-virtual {v0, v3}, Lorg/apache/index/poi/hwpf/model/PAPX;->setEnd(I)V

    .line 173
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public getParagraphs()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lorg/apache/index/poi/hwpf/model/PAPX;",
            ">;"
        }
    .end annotation

    .prologue
    .line 184
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/PAPBinTable;->_paragraphs:Ljava/util/ArrayList;

    return-object v0
.end method

.method public insert(IILorg/apache/index/poi/hwpf/sprm/SprmBuffer;)V
    .locals 11
    .param p1, "listIndex"    # I
    .param p2, "cpStart"    # I
    .param p3, "buf"    # Lorg/apache/index/poi/hwpf/sprm/SprmBuffer;

    .prologue
    const/4 v2, 0x0

    .line 80
    new-instance v1, Lorg/apache/index/poi/hwpf/model/PAPX;

    iget-object v4, p0, Lorg/apache/index/poi/hwpf/model/PAPBinTable;->tpt:Lorg/apache/index/poi/hwpf/model/TextPieceTable;

    iget-object v6, p0, Lorg/apache/index/poi/hwpf/model/PAPBinTable;->_dataStream:[B

    move v3, v2

    move-object v5, p3

    invoke-direct/range {v1 .. v6}, Lorg/apache/index/poi/hwpf/model/PAPX;-><init>(IILorg/apache/index/poi/hwpf/model/CharIndexTranslator;Lorg/apache/index/poi/hwpf/sprm/SprmBuffer;[B)V

    .line 83
    .local v1, "forInsert":Lorg/apache/index/poi/hwpf/model/PAPX;
    invoke-virtual {v1, p2}, Lorg/apache/index/poi/hwpf/model/PAPX;->setStart(I)V

    .line 84
    invoke-virtual {v1, p2}, Lorg/apache/index/poi/hwpf/model/PAPX;->setEnd(I)V

    .line 86
    iget-object v4, p0, Lorg/apache/index/poi/hwpf/model/PAPBinTable;->_paragraphs:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ne p1, v4, :cond_0

    .line 88
    iget-object v2, p0, Lorg/apache/index/poi/hwpf/model/PAPBinTable;->_paragraphs:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 126
    :goto_0
    return-void

    .line 92
    :cond_0
    iget-object v4, p0, Lorg/apache/index/poi/hwpf/model/PAPBinTable;->_paragraphs:Ljava/util/ArrayList;

    invoke-virtual {v4, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lorg/apache/index/poi/hwpf/model/PAPX;

    .line 93
    .local v9, "currentPap":Lorg/apache/index/poi/hwpf/model/PAPX;
    if-eqz v9, :cond_1

    invoke-virtual {v9}, Lorg/apache/index/poi/hwpf/model/PAPX;->getStart()I

    move-result v4

    if-ge v4, p2, :cond_1

    .line 95
    const/4 v7, 0x0

    .line 98
    .local v7, "clonedBuf":Lorg/apache/index/poi/hwpf/sprm/SprmBuffer;
    :try_start_0
    invoke-virtual {v9}, Lorg/apache/index/poi/hwpf/model/PAPX;->getSprmBuf()Lorg/apache/index/poi/hwpf/sprm/SprmBuffer;

    move-result-object v4

    invoke-virtual {v4}, Lorg/apache/index/poi/hwpf/sprm/SprmBuffer;->clone()Ljava/lang/Object;

    move-result-object v4

    move-object v0, v4

    check-cast v0, Lorg/apache/index/poi/hwpf/sprm/SprmBuffer;

    move-object v7, v0
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 110
    :goto_1
    new-instance v3, Lorg/apache/index/poi/hwpf/model/PAPX;

    iget-object v6, p0, Lorg/apache/index/poi/hwpf/model/PAPBinTable;->tpt:Lorg/apache/index/poi/hwpf/model/TextPieceTable;

    iget-object v8, p0, Lorg/apache/index/poi/hwpf/model/PAPBinTable;->_dataStream:[B

    move v4, v2

    move v5, v2

    invoke-direct/range {v3 .. v8}, Lorg/apache/index/poi/hwpf/model/PAPX;-><init>(IILorg/apache/index/poi/hwpf/model/CharIndexTranslator;Lorg/apache/index/poi/hwpf/sprm/SprmBuffer;[B)V

    .line 112
    .local v3, "clone":Lorg/apache/index/poi/hwpf/model/PAPX;
    invoke-virtual {v3, p2}, Lorg/apache/index/poi/hwpf/model/PAPX;->setStart(I)V

    .line 113
    invoke-virtual {v9}, Lorg/apache/index/poi/hwpf/model/PAPX;->getEnd()I

    move-result v2

    invoke-virtual {v3, v2}, Lorg/apache/index/poi/hwpf/model/PAPX;->setEnd(I)V

    .line 115
    invoke-virtual {v9, p2}, Lorg/apache/index/poi/hwpf/model/PAPX;->setEnd(I)V

    .line 117
    iget-object v2, p0, Lorg/apache/index/poi/hwpf/model/PAPBinTable;->_paragraphs:Ljava/util/ArrayList;

    add-int/lit8 v4, p1, 0x1

    invoke-virtual {v2, v4, v1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 118
    iget-object v2, p0, Lorg/apache/index/poi/hwpf/model/PAPBinTable;->_paragraphs:Ljava/util/ArrayList;

    add-int/lit8 v4, p1, 0x2

    invoke-virtual {v2, v4, v3}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    goto :goto_0

    .line 100
    .end local v3    # "clone":Lorg/apache/index/poi/hwpf/model/PAPX;
    :catch_0
    move-exception v10

    .line 102
    .local v10, "exc":Ljava/lang/CloneNotSupportedException;
    const-string/jumbo v4, "DocumentService"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "Exception :"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10}, Ljava/lang/CloneNotSupportedException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 122
    .end local v7    # "clonedBuf":Lorg/apache/index/poi/hwpf/sprm/SprmBuffer;
    .end local v10    # "exc":Ljava/lang/CloneNotSupportedException;
    :cond_1
    iget-object v2, p0, Lorg/apache/index/poi/hwpf/model/PAPBinTable;->_paragraphs:Ljava/util/ArrayList;

    invoke-virtual {v2, p1, v1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    goto :goto_0
.end method
