.class public interface abstract Lorg/apache/index/poi/hwpf/model/CharIndexTranslator;
.super Ljava/lang/Object;
.source "CharIndexTranslator.java"


# virtual methods
.method public abstract getCharIndex(I)I
.end method

.method public abstract getCharIndex(II)I
.end method

.method public abstract isIndexInTable(I)Z
.end method

.method public abstract lookIndexBackward(I)I
.end method

.method public abstract lookIndexForward(I)I
.end method
