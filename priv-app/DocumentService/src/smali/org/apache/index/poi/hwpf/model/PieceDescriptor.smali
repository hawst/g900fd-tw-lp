.class public final Lorg/apache/index/poi/hwpf/model/PieceDescriptor;
.super Ljava/lang/Object;
.source "PieceDescriptor.java"


# instance fields
.field descriptor:S

.field fc:I

.field prm:S

.field unicode:Z


# direct methods
.method public constructor <init>([BI)V
    .locals 2
    .param p1, "buf"    # [B
    .param p2, "offset"    # I

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    invoke-static {p1, p2}, Lorg/apache/index/poi/util/LittleEndian;->getShort([BI)S

    move-result v0

    iput-short v0, p0, Lorg/apache/index/poi/hwpf/model/PieceDescriptor;->descriptor:S

    .line 40
    add-int/lit8 p2, p2, 0x2

    .line 41
    invoke-static {p1, p2}, Lorg/apache/index/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    iput v0, p0, Lorg/apache/index/poi/hwpf/model/PieceDescriptor;->fc:I

    .line 42
    add-int/lit8 p2, p2, 0x4

    .line 43
    invoke-static {p1, p2}, Lorg/apache/index/poi/util/LittleEndian;->getShort([BI)S

    move-result v0

    iput-short v0, p0, Lorg/apache/index/poi/hwpf/model/PieceDescriptor;->prm:S

    .line 46
    iget v0, p0, Lorg/apache/index/poi/hwpf/model/PieceDescriptor;->fc:I

    const/high16 v1, 0x40000000    # 2.0f

    and-int/2addr v0, v1

    if-nez v0, :cond_0

    .line 48
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/index/poi/hwpf/model/PieceDescriptor;->unicode:Z

    .line 57
    :goto_0
    return-void

    .line 52
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/index/poi/hwpf/model/PieceDescriptor;->unicode:Z

    .line 53
    iget v0, p0, Lorg/apache/index/poi/hwpf/model/PieceDescriptor;->fc:I

    const v1, -0x40000001    # -1.9999999f

    and-int/2addr v0, v1

    iput v0, p0, Lorg/apache/index/poi/hwpf/model/PieceDescriptor;->fc:I

    .line 54
    iget v0, p0, Lorg/apache/index/poi/hwpf/model/PieceDescriptor;->fc:I

    div-int/lit8 v0, v0, 0x2

    iput v0, p0, Lorg/apache/index/poi/hwpf/model/PieceDescriptor;->fc:I

    goto :goto_0
.end method

.method public static getSizeInBytes()I
    .locals 1

    .prologue
    .line 98
    const/16 v0, 0x8

    return v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 103
    if-nez p1, :cond_1

    .line 113
    :cond_0
    :goto_0
    return v1

    .line 108
    :cond_1
    instance-of v2, p1, Lorg/apache/index/poi/hwpf/model/PieceDescriptor;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 109
    check-cast v0, Lorg/apache/index/poi/hwpf/model/PieceDescriptor;

    .line 111
    .local v0, "pd":Lorg/apache/index/poi/hwpf/model/PieceDescriptor;
    iget-short v2, p0, Lorg/apache/index/poi/hwpf/model/PieceDescriptor;->descriptor:S

    iget-short v3, v0, Lorg/apache/index/poi/hwpf/model/PieceDescriptor;->descriptor:S

    if-ne v2, v3, :cond_0

    iget-short v2, p0, Lorg/apache/index/poi/hwpf/model/PieceDescriptor;->prm:S

    iget-short v3, v0, Lorg/apache/index/poi/hwpf/model/PieceDescriptor;->prm:S

    if-ne v2, v3, :cond_0

    iget-boolean v2, p0, Lorg/apache/index/poi/hwpf/model/PieceDescriptor;->unicode:Z

    iget-boolean v3, v0, Lorg/apache/index/poi/hwpf/model/PieceDescriptor;->unicode:Z

    if-ne v2, v3, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method public getFilePosition()I
    .locals 1

    .prologue
    .line 61
    iget v0, p0, Lorg/apache/index/poi/hwpf/model/PieceDescriptor;->fc:I

    return v0
.end method

.method public isUnicode()Z
    .locals 1

    .prologue
    .line 71
    iget-boolean v0, p0, Lorg/apache/index/poi/hwpf/model/PieceDescriptor;->unicode:Z

    return v0
.end method

.method public setFilePosition(I)V
    .locals 0
    .param p1, "pos"    # I

    .prologue
    .line 66
    iput p1, p0, Lorg/apache/index/poi/hwpf/model/PieceDescriptor;->fc:I

    .line 67
    return-void
.end method

.method protected toByteArray()[B
    .locals 4

    .prologue
    .line 77
    iget v2, p0, Lorg/apache/index/poi/hwpf/model/PieceDescriptor;->fc:I

    .line 78
    .local v2, "tempFc":I
    iget-boolean v3, p0, Lorg/apache/index/poi/hwpf/model/PieceDescriptor;->unicode:Z

    if-nez v3, :cond_0

    .line 80
    mul-int/lit8 v2, v2, 0x2

    .line 81
    const/high16 v3, 0x40000000    # 2.0f

    or-int/2addr v2, v3

    .line 84
    :cond_0
    const/4 v1, 0x0

    .line 85
    .local v1, "offset":I
    const/16 v3, 0x8

    new-array v0, v3, [B

    .line 86
    .local v0, "buf":[B
    iget-short v3, p0, Lorg/apache/index/poi/hwpf/model/PieceDescriptor;->descriptor:S

    invoke-static {v0, v1, v3}, Lorg/apache/index/poi/util/LittleEndian;->putShort([BIS)V

    .line 87
    add-int/lit8 v1, v1, 0x2

    .line 88
    invoke-static {v0, v1, v2}, Lorg/apache/index/poi/util/LittleEndian;->putInt([BII)V

    .line 89
    add-int/lit8 v1, v1, 0x4

    .line 90
    iget-short v3, p0, Lorg/apache/index/poi/hwpf/model/PieceDescriptor;->prm:S

    invoke-static {v0, v1, v3}, Lorg/apache/index/poi/util/LittleEndian;->putShort([BIS)V

    .line 92
    return-object v0
.end method
