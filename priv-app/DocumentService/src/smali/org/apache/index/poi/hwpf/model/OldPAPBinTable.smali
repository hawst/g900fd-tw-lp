.class public final Lorg/apache/index/poi/hwpf/model/OldPAPBinTable;
.super Lorg/apache/index/poi/hwpf/model/PAPBinTable;
.source "OldPAPBinTable.java"


# direct methods
.method public constructor <init>([BIIILorg/apache/index/poi/hwpf/model/TextPieceTable;)V
    .locals 17
    .param p1, "documentStream"    # [B
    .param p2, "offset"    # I
    .param p3, "size"    # I
    .param p4, "fcMin"    # I
    .param p5, "tpt"    # Lorg/apache/index/poi/hwpf/model/TextPieceTable;

    .prologue
    .line 33
    invoke-direct/range {p0 .. p0}, Lorg/apache/index/poi/hwpf/model/PAPBinTable;-><init>()V

    .line 36
    new-instance v9, Lorg/apache/index/poi/hwpf/model/PlexOfCps;

    const/4 v4, 0x2

    move-object/from16 v0, p1

    move/from16 v1, p2

    move/from16 v2, p3

    invoke-direct {v9, v0, v1, v2, v4}, Lorg/apache/index/poi/hwpf/model/PlexOfCps;-><init>([BIII)V

    .line 38
    .local v9, "binTable":Lorg/apache/index/poi/hwpf/model/PlexOfCps;
    invoke-virtual {v9}, Lorg/apache/index/poi/hwpf/model/PlexOfCps;->length()I

    move-result v11

    .line 39
    .local v11, "length":I
    const/4 v15, 0x0

    .local v15, "x":I
    :goto_0
    if-lt v15, v11, :cond_0

    .line 57
    return-void

    .line 41
    :cond_0
    invoke-virtual {v9, v15}, Lorg/apache/index/poi/hwpf/model/PlexOfCps;->getProperty(I)Lorg/apache/index/poi/hwpf/model/GenericPropertyNode;

    move-result-object v12

    .line 43
    .local v12, "node":Lorg/apache/index/poi/hwpf/model/GenericPropertyNode;
    invoke-virtual {v12}, Lorg/apache/index/poi/hwpf/model/GenericPropertyNode;->getBytes()[B

    move-result-object v4

    invoke-static {v4}, Lorg/apache/index/poi/util/LittleEndian;->getShort([B)S

    move-result v13

    .line 44
    .local v13, "pageNum":I
    mul-int/lit16 v6, v13, 0x200

    .line 46
    .local v6, "pageOffset":I
    new-instance v3, Lorg/apache/index/poi/hwpf/model/PAPFormattedDiskPage;

    move-object/from16 v4, p1

    move-object/from16 v5, p1

    move/from16 v7, p4

    move-object/from16 v8, p5

    invoke-direct/range {v3 .. v8}, Lorg/apache/index/poi/hwpf/model/PAPFormattedDiskPage;-><init>([B[BIILorg/apache/index/poi/hwpf/model/TextPieceTable;)V

    .line 49
    .local v3, "pfkp":Lorg/apache/index/poi/hwpf/model/PAPFormattedDiskPage;
    invoke-virtual {v3}, Lorg/apache/index/poi/hwpf/model/PAPFormattedDiskPage;->size()I

    move-result v10

    .line 51
    .local v10, "fkpSize":I
    const/16 v16, 0x0

    .local v16, "y":I
    :goto_1
    move/from16 v0, v16

    if-lt v0, v10, :cond_1

    .line 39
    add-int/lit8 v15, v15, 0x1

    goto :goto_0

    .line 53
    :cond_1
    move/from16 v0, v16

    invoke-virtual {v3, v0}, Lorg/apache/index/poi/hwpf/model/PAPFormattedDiskPage;->getPAPX(I)Lorg/apache/index/poi/hwpf/model/PAPX;

    move-result-object v14

    .line 54
    .local v14, "papx":Lorg/apache/index/poi/hwpf/model/PAPX;
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/index/poi/hwpf/model/OldPAPBinTable;->_paragraphs:Ljava/util/ArrayList;

    invoke-virtual {v4, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 51
    add-int/lit8 v16, v16, 0x1

    goto :goto_1
.end method
