.class public final Lorg/apache/index/poi/hwpf/model/FIBLongHandler;
.super Ljava/lang/Object;
.source "FIBLongHandler.java"


# static fields
.field public static final CBMAC:I = 0x0

.field public static final CCPATN:I = 0x7

.field public static final CCPEDN:I = 0x8

.field public static final CCPFTN:I = 0x4

.field public static final CCPHDD:I = 0x5

.field public static final CCPHDRTXBX:I = 0xa

.field public static final CCPMCR:I = 0x6

.field public static final CCPTEXT:I = 0x3

.field public static final CCPTXBX:I = 0x9

.field public static final CPNBTECHP:I = 0xd

.field public static final CPNBTELVC:I = 0x13

.field public static final CPNBTEPAP:I = 0x10

.field public static final FCISLANDFIRST:I = 0x14

.field public static final FCISLANDLIM:I = 0x15

.field public static final PNCHPFIRST:I = 0xc

.field public static final PNFBPCHPFIRST:I = 0xb

.field public static final PNFBPLVCFIRST:I = 0x11

.field public static final PNFBPPAPFIRST:I = 0xe

.field public static final PNLVCFIRST:I = 0x12

.field public static final PNPAPFIRST:I = 0xf

.field public static final PRODUCTCREATED:I = 0x1

.field public static final PRODUCTREVISED:I = 0x2


# instance fields
.field _longs:[I


# direct methods
.method public constructor <init>([BI)V
    .locals 4
    .param p1, "mainStream"    # [B
    .param p2, "offset"    # I

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    invoke-static {p1, p2}, Lorg/apache/index/poi/util/LittleEndian;->getShort([BI)S

    move-result v0

    .line 56
    .local v0, "longCount":I
    add-int/lit8 p2, p2, 0x2

    .line 57
    new-array v2, v0, [I

    iput-object v2, p0, Lorg/apache/index/poi/hwpf/model/FIBLongHandler;->_longs:[I

    .line 59
    const/4 v1, 0x0

    .local v1, "x":I
    :goto_0
    if-lt v1, v0, :cond_0

    .line 63
    return-void

    .line 61
    :cond_0
    iget-object v2, p0, Lorg/apache/index/poi/hwpf/model/FIBLongHandler;->_longs:[I

    mul-int/lit8 v3, v1, 0x4

    add-int/2addr v3, p2

    invoke-static {p1, v3}, Lorg/apache/index/poi/util/LittleEndian;->getInt([BI)I

    move-result v3

    aput v3, v2, v1

    .line 59
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method


# virtual methods
.method public getLong(I)I
    .locals 1
    .param p1, "longCode"    # I

    .prologue
    .line 72
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/FIBLongHandler;->_longs:[I

    aget v0, v0, p1

    return v0
.end method

.method serialize([BI)V
    .locals 2
    .param p1, "mainStream"    # [B
    .param p2, "offset"    # I

    .prologue
    .line 81
    iget-object v1, p0, Lorg/apache/index/poi/hwpf/model/FIBLongHandler;->_longs:[I

    array-length v1, v1

    int-to-short v1, v1

    invoke-static {p1, p2, v1}, Lorg/apache/index/poi/util/LittleEndian;->putShort([BIS)V

    .line 82
    add-int/lit8 p2, p2, 0x2

    .line 84
    const/4 v0, 0x0

    .local v0, "x":I
    :goto_0
    iget-object v1, p0, Lorg/apache/index/poi/hwpf/model/FIBLongHandler;->_longs:[I

    array-length v1, v1

    if-lt v0, v1, :cond_0

    .line 89
    return-void

    .line 86
    :cond_0
    iget-object v1, p0, Lorg/apache/index/poi/hwpf/model/FIBLongHandler;->_longs:[I

    aget v1, v1, v0

    invoke-static {p1, p2, v1}, Lorg/apache/index/poi/util/LittleEndian;->putInt([BII)V

    .line 87
    add-int/lit8 p2, p2, 0x4

    .line 84
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public setLong(II)V
    .locals 1
    .param p1, "longCode"    # I
    .param p2, "value"    # I

    .prologue
    .line 77
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/FIBLongHandler;->_longs:[I

    aput p2, v0, p1

    .line 78
    return-void
.end method

.method sizeInBytes()I
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/FIBLongHandler;->_longs:[I

    array-length v0, v0

    mul-int/lit8 v0, v0, 0x4

    add-int/lit8 v0, v0, 0x2

    return v0
.end method
