.class public Lorg/apache/index/poi/hwpf/model/CHPBinTable;
.super Ljava/lang/Object;
.source "CHPBinTable.java"


# instance fields
.field protected _textRuns:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lorg/apache/index/poi/hwpf/model/CHPX;",
            ">;"
        }
    .end annotation
.end field

.field private tpt:Lorg/apache/index/poi/hwpf/model/TextPieceTable;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/apache/index/poi/hwpf/model/CHPBinTable;->_textRuns:Ljava/util/ArrayList;

    .line 42
    return-void
.end method

.method public constructor <init>([B[BIIILorg/apache/index/poi/hwpf/model/TextPieceTable;)V
    .locals 13
    .param p1, "documentStream"    # [B
    .param p2, "tableStream"    # [B
    .param p3, "offset"    # I
    .param p4, "size"    # I
    .param p5, "fcMin"    # I
    .param p6, "tpt"    # Lorg/apache/index/poi/hwpf/model/TextPieceTable;

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    iput-object v11, p0, Lorg/apache/index/poi/hwpf/model/CHPBinTable;->_textRuns:Ljava/util/ArrayList;

    .line 56
    new-instance v2, Lorg/apache/index/poi/hwpf/model/PlexOfCps;

    const/4 v11, 0x4

    move/from16 v0, p3

    move/from16 v1, p4

    invoke-direct {v2, p2, v0, v1, v11}, Lorg/apache/index/poi/hwpf/model/PlexOfCps;-><init>([BIII)V

    .line 57
    .local v2, "binTable":Lorg/apache/index/poi/hwpf/model/PlexOfCps;
    move-object/from16 v0, p6

    iput-object v0, p0, Lorg/apache/index/poi/hwpf/model/CHPBinTable;->tpt:Lorg/apache/index/poi/hwpf/model/TextPieceTable;

    .line 59
    invoke-virtual {v2}, Lorg/apache/index/poi/hwpf/model/PlexOfCps;->length()I

    move-result v5

    .line 60
    .local v5, "length":I
    const/4 v9, 0x0

    .local v9, "x":I
    :goto_0
    if-lt v9, v5, :cond_0

    .line 77
    return-void

    .line 62
    :cond_0
    invoke-virtual {v2, v9}, Lorg/apache/index/poi/hwpf/model/PlexOfCps;->getProperty(I)Lorg/apache/index/poi/hwpf/model/GenericPropertyNode;

    move-result-object v6

    .line 64
    .local v6, "node":Lorg/apache/index/poi/hwpf/model/GenericPropertyNode;
    invoke-virtual {v6}, Lorg/apache/index/poi/hwpf/model/GenericPropertyNode;->getBytes()[B

    move-result-object v11

    invoke-static {v11}, Lorg/apache/index/poi/util/LittleEndian;->getInt([B)I

    move-result v7

    .line 65
    .local v7, "pageNum":I
    mul-int/lit16 v8, v7, 0x200

    .line 67
    .local v8, "pageOffset":I
    new-instance v3, Lorg/apache/index/poi/hwpf/model/CHPFormattedDiskPage;

    move/from16 v0, p5

    move-object/from16 v1, p6

    invoke-direct {v3, p1, v8, v0, v1}, Lorg/apache/index/poi/hwpf/model/CHPFormattedDiskPage;-><init>([BIILorg/apache/index/poi/hwpf/model/TextPieceTable;)V

    .line 70
    .local v3, "cfkp":Lorg/apache/index/poi/hwpf/model/CHPFormattedDiskPage;
    invoke-virtual {v3}, Lorg/apache/index/poi/hwpf/model/CHPFormattedDiskPage;->size()I

    move-result v4

    .line 72
    .local v4, "fkpSize":I
    const/4 v10, 0x0

    .local v10, "y":I
    :goto_1
    if-lt v10, v4, :cond_1

    .line 60
    add-int/lit8 v9, v9, 0x1

    goto :goto_0

    .line 74
    :cond_1
    iget-object v11, p0, Lorg/apache/index/poi/hwpf/model/CHPBinTable;->_textRuns:Ljava/util/ArrayList;

    invoke-virtual {v3, v10}, Lorg/apache/index/poi/hwpf/model/CHPFormattedDiskPage;->getCHPX(I)Lorg/apache/index/poi/hwpf/model/CHPX;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 72
    add-int/lit8 v10, v10, 0x1

    goto :goto_1
.end method


# virtual methods
.method public adjustForDelete(III)V
    .locals 6
    .param p1, "listIndex"    # I
    .param p2, "offset"    # I
    .param p3, "length"    # I

    .prologue
    .line 81
    iget-object v5, p0, Lorg/apache/index/poi/hwpf/model/CHPBinTable;->_textRuns:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 82
    .local v3, "size":I
    add-int v2, p2, p3

    .line 83
    .local v2, "endMark":I
    move v1, p1

    .line 85
    .local v1, "endIndex":I
    iget-object v5, p0, Lorg/apache/index/poi/hwpf/model/CHPBinTable;->_textRuns:Ljava/util/ArrayList;

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/index/poi/hwpf/model/CHPX;

    .line 86
    .local v0, "chpx":Lorg/apache/index/poi/hwpf/model/CHPX;
    :goto_0
    invoke-virtual {v0}, Lorg/apache/index/poi/hwpf/model/CHPX;->getEnd()I

    move-result v5

    if-lt v5, v2, :cond_0

    .line 90
    if-ne p1, v1, :cond_1

    .line 92
    iget-object v5, p0, Lorg/apache/index/poi/hwpf/model/CHPBinTable;->_textRuns:Ljava/util/ArrayList;

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "chpx":Lorg/apache/index/poi/hwpf/model/CHPX;
    check-cast v0, Lorg/apache/index/poi/hwpf/model/CHPX;

    .line 93
    .restart local v0    # "chpx":Lorg/apache/index/poi/hwpf/model/CHPX;
    invoke-virtual {v0}, Lorg/apache/index/poi/hwpf/model/CHPX;->getEnd()I

    move-result v5

    sub-int/2addr v5, v2

    add-int/2addr v5, p2

    invoke-virtual {v0, v5}, Lorg/apache/index/poi/hwpf/model/CHPX;->setEnd(I)V

    .line 109
    :goto_1
    add-int/lit8 v4, v1, 0x1

    .local v4, "x":I
    :goto_2
    if-lt v4, v3, :cond_3

    .line 115
    return-void

    .line 88
    .end local v4    # "x":I
    :cond_0
    iget-object v5, p0, Lorg/apache/index/poi/hwpf/model/CHPBinTable;->_textRuns:Ljava/util/ArrayList;

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "chpx":Lorg/apache/index/poi/hwpf/model/CHPX;
    check-cast v0, Lorg/apache/index/poi/hwpf/model/CHPX;

    .restart local v0    # "chpx":Lorg/apache/index/poi/hwpf/model/CHPX;
    goto :goto_0

    .line 97
    :cond_1
    iget-object v5, p0, Lorg/apache/index/poi/hwpf/model/CHPBinTable;->_textRuns:Ljava/util/ArrayList;

    invoke-virtual {v5, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "chpx":Lorg/apache/index/poi/hwpf/model/CHPX;
    check-cast v0, Lorg/apache/index/poi/hwpf/model/CHPX;

    .line 98
    .restart local v0    # "chpx":Lorg/apache/index/poi/hwpf/model/CHPX;
    invoke-virtual {v0, p2}, Lorg/apache/index/poi/hwpf/model/CHPX;->setEnd(I)V

    .line 99
    add-int/lit8 v4, p1, 0x1

    .restart local v4    # "x":I
    :goto_3
    if-lt v4, v1, :cond_2

    .line 105
    iget-object v5, p0, Lorg/apache/index/poi/hwpf/model/CHPBinTable;->_textRuns:Ljava/util/ArrayList;

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "chpx":Lorg/apache/index/poi/hwpf/model/CHPX;
    check-cast v0, Lorg/apache/index/poi/hwpf/model/CHPX;

    .line 106
    .restart local v0    # "chpx":Lorg/apache/index/poi/hwpf/model/CHPX;
    invoke-virtual {v0}, Lorg/apache/index/poi/hwpf/model/CHPX;->getEnd()I

    move-result v5

    sub-int/2addr v5, v2

    add-int/2addr v5, p2

    invoke-virtual {v0, v5}, Lorg/apache/index/poi/hwpf/model/CHPX;->setEnd(I)V

    goto :goto_1

    .line 101
    :cond_2
    iget-object v5, p0, Lorg/apache/index/poi/hwpf/model/CHPBinTable;->_textRuns:Ljava/util/ArrayList;

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "chpx":Lorg/apache/index/poi/hwpf/model/CHPX;
    check-cast v0, Lorg/apache/index/poi/hwpf/model/CHPX;

    .line 102
    .restart local v0    # "chpx":Lorg/apache/index/poi/hwpf/model/CHPX;
    invoke-virtual {v0, p2}, Lorg/apache/index/poi/hwpf/model/CHPX;->setStart(I)V

    .line 103
    invoke-virtual {v0, p2}, Lorg/apache/index/poi/hwpf/model/CHPX;->setEnd(I)V

    .line 99
    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    .line 111
    :cond_3
    iget-object v5, p0, Lorg/apache/index/poi/hwpf/model/CHPBinTable;->_textRuns:Ljava/util/ArrayList;

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "chpx":Lorg/apache/index/poi/hwpf/model/CHPX;
    check-cast v0, Lorg/apache/index/poi/hwpf/model/CHPX;

    .line 112
    .restart local v0    # "chpx":Lorg/apache/index/poi/hwpf/model/CHPX;
    invoke-virtual {v0}, Lorg/apache/index/poi/hwpf/model/CHPX;->getStart()I

    move-result v5

    sub-int/2addr v5, p3

    invoke-virtual {v0, v5}, Lorg/apache/index/poi/hwpf/model/CHPX;->setStart(I)V

    .line 113
    invoke-virtual {v0}, Lorg/apache/index/poi/hwpf/model/CHPX;->getEnd()I

    move-result v5

    sub-int/2addr v5, p3

    invoke-virtual {v0, v5}, Lorg/apache/index/poi/hwpf/model/CHPX;->setEnd(I)V

    .line 109
    add-int/lit8 v4, v4, 0x1

    goto :goto_2
.end method

.method public adjustForInsert(II)V
    .locals 4
    .param p1, "listIndex"    # I
    .param p2, "length"    # I

    .prologue
    .line 159
    iget-object v3, p0, Lorg/apache/index/poi/hwpf/model/CHPBinTable;->_textRuns:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 160
    .local v1, "size":I
    iget-object v3, p0, Lorg/apache/index/poi/hwpf/model/CHPBinTable;->_textRuns:Ljava/util/ArrayList;

    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/index/poi/hwpf/model/CHPX;

    .line 161
    .local v0, "chpx":Lorg/apache/index/poi/hwpf/model/CHPX;
    invoke-virtual {v0}, Lorg/apache/index/poi/hwpf/model/CHPX;->getEnd()I

    move-result v3

    add-int/2addr v3, p2

    invoke-virtual {v0, v3}, Lorg/apache/index/poi/hwpf/model/CHPX;->setEnd(I)V

    .line 163
    add-int/lit8 v2, p1, 0x1

    .local v2, "x":I
    :goto_0
    if-lt v2, v1, :cond_0

    .line 169
    return-void

    .line 165
    :cond_0
    iget-object v3, p0, Lorg/apache/index/poi/hwpf/model/CHPBinTable;->_textRuns:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "chpx":Lorg/apache/index/poi/hwpf/model/CHPX;
    check-cast v0, Lorg/apache/index/poi/hwpf/model/CHPX;

    .line 166
    .restart local v0    # "chpx":Lorg/apache/index/poi/hwpf/model/CHPX;
    invoke-virtual {v0}, Lorg/apache/index/poi/hwpf/model/CHPX;->getStart()I

    move-result v3

    add-int/2addr v3, p2

    invoke-virtual {v0, v3}, Lorg/apache/index/poi/hwpf/model/CHPX;->setStart(I)V

    .line 167
    invoke-virtual {v0}, Lorg/apache/index/poi/hwpf/model/CHPX;->getEnd()I

    move-result v3

    add-int/2addr v3, p2

    invoke-virtual {v0, v3}, Lorg/apache/index/poi/hwpf/model/CHPX;->setEnd(I)V

    .line 163
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public getTextRuns()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lorg/apache/index/poi/hwpf/model/CHPX;",
            ">;"
        }
    .end annotation

    .prologue
    .line 173
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/CHPBinTable;->_textRuns:Ljava/util/ArrayList;

    return-object v0
.end method

.method public insert(IILorg/apache/index/poi/hwpf/sprm/SprmBuffer;)V
    .locals 6
    .param p1, "listIndex"    # I
    .param p2, "cpStart"    # I
    .param p3, "buf"    # Lorg/apache/index/poi/hwpf/sprm/SprmBuffer;

    .prologue
    const/4 v5, 0x0

    .line 120
    new-instance v2, Lorg/apache/index/poi/hwpf/model/CHPX;

    iget-object v3, p0, Lorg/apache/index/poi/hwpf/model/CHPBinTable;->tpt:Lorg/apache/index/poi/hwpf/model/TextPieceTable;

    invoke-direct {v2, v5, v5, v3, p3}, Lorg/apache/index/poi/hwpf/model/CHPX;-><init>(IILorg/apache/index/poi/hwpf/model/CharIndexTranslator;Lorg/apache/index/poi/hwpf/sprm/SprmBuffer;)V

    .line 123
    .local v2, "insertChpx":Lorg/apache/index/poi/hwpf/model/CHPX;
    invoke-virtual {v2, p2}, Lorg/apache/index/poi/hwpf/model/CHPX;->setStart(I)V

    .line 124
    invoke-virtual {v2, p2}, Lorg/apache/index/poi/hwpf/model/CHPX;->setEnd(I)V

    .line 126
    iget-object v3, p0, Lorg/apache/index/poi/hwpf/model/CHPBinTable;->_textRuns:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ne p1, v3, :cond_0

    .line 128
    iget-object v3, p0, Lorg/apache/index/poi/hwpf/model/CHPBinTable;->_textRuns:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 155
    :goto_0
    return-void

    .line 132
    :cond_0
    iget-object v3, p0, Lorg/apache/index/poi/hwpf/model/CHPBinTable;->_textRuns:Ljava/util/ArrayList;

    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/index/poi/hwpf/model/CHPX;

    .line 133
    .local v0, "chpx":Lorg/apache/index/poi/hwpf/model/CHPX;
    invoke-virtual {v0}, Lorg/apache/index/poi/hwpf/model/CHPX;->getStart()I

    move-result v3

    if-ge v3, p2, :cond_1

    .line 140
    new-instance v1, Lorg/apache/index/poi/hwpf/model/CHPX;

    iget-object v3, p0, Lorg/apache/index/poi/hwpf/model/CHPBinTable;->tpt:Lorg/apache/index/poi/hwpf/model/TextPieceTable;

    invoke-virtual {v0}, Lorg/apache/index/poi/hwpf/model/CHPX;->getSprmBuf()Lorg/apache/index/poi/hwpf/sprm/SprmBuffer;

    move-result-object v4

    invoke-direct {v1, v5, v5, v3, v4}, Lorg/apache/index/poi/hwpf/model/CHPX;-><init>(IILorg/apache/index/poi/hwpf/model/CharIndexTranslator;Lorg/apache/index/poi/hwpf/sprm/SprmBuffer;)V

    .line 142
    .local v1, "clone":Lorg/apache/index/poi/hwpf/model/CHPX;
    invoke-virtual {v1, p2}, Lorg/apache/index/poi/hwpf/model/CHPX;->setStart(I)V

    .line 143
    invoke-virtual {v0}, Lorg/apache/index/poi/hwpf/model/CHPX;->getEnd()I

    move-result v3

    invoke-virtual {v1, v3}, Lorg/apache/index/poi/hwpf/model/CHPX;->setEnd(I)V

    .line 145
    invoke-virtual {v0, p2}, Lorg/apache/index/poi/hwpf/model/CHPX;->setEnd(I)V

    .line 147
    iget-object v3, p0, Lorg/apache/index/poi/hwpf/model/CHPBinTable;->_textRuns:Ljava/util/ArrayList;

    add-int/lit8 v4, p1, 0x1

    invoke-virtual {v3, v4, v2}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 148
    iget-object v3, p0, Lorg/apache/index/poi/hwpf/model/CHPBinTable;->_textRuns:Ljava/util/ArrayList;

    add-int/lit8 v4, p1, 0x2

    invoke-virtual {v3, v4, v1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    goto :goto_0

    .line 152
    .end local v1    # "clone":Lorg/apache/index/poi/hwpf/model/CHPX;
    :cond_1
    iget-object v3, p0, Lorg/apache/index/poi/hwpf/model/CHPBinTable;->_textRuns:Ljava/util/ArrayList;

    invoke-virtual {v3, p1, v2}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    goto :goto_0
.end method
