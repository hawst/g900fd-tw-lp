.class public Lorg/apache/index/poi/hpsf/MutableProperty;
.super Lorg/apache/index/poi/hpsf/Property;
.source "MutableProperty.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Lorg/apache/index/poi/hpsf/Property;-><init>()V

    .line 40
    return-void
.end method

.method public constructor <init>(Lorg/apache/index/poi/hpsf/Property;)V
    .locals 2
    .param p1, "p"    # Lorg/apache/index/poi/hpsf/Property;

    .prologue
    .line 50
    invoke-direct {p0}, Lorg/apache/index/poi/hpsf/Property;-><init>()V

    .line 52
    invoke-virtual {p1}, Lorg/apache/index/poi/hpsf/Property;->getID()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lorg/apache/index/poi/hpsf/MutableProperty;->setID(J)V

    .line 53
    invoke-virtual {p1}, Lorg/apache/index/poi/hpsf/Property;->getType()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lorg/apache/index/poi/hpsf/MutableProperty;->setType(J)V

    .line 54
    invoke-virtual {p1}, Lorg/apache/index/poi/hpsf/Property;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/apache/index/poi/hpsf/MutableProperty;->setValue(Ljava/lang/Object;)V

    .line 55
    return-void
.end method


# virtual methods
.method public setID(J)V
    .locals 1
    .param p1, "id"    # J

    .prologue
    .line 65
    iput-wide p1, p0, Lorg/apache/index/poi/hpsf/MutableProperty;->id:J

    .line 66
    return-void
.end method

.method public setType(J)V
    .locals 1
    .param p1, "type"    # J

    .prologue
    .line 77
    iput-wide p1, p0, Lorg/apache/index/poi/hpsf/MutableProperty;->type:J

    .line 78
    return-void
.end method

.method public setValue(Ljava/lang/Object;)V
    .locals 0
    .param p1, "value"    # Ljava/lang/Object;

    .prologue
    .line 89
    iput-object p1, p0, Lorg/apache/index/poi/hpsf/MutableProperty;->value:Ljava/lang/Object;

    .line 90
    return-void
.end method

.method public write(Ljava/io/OutputStream;I)I
    .locals 6
    .param p1, "out"    # Ljava/io/OutputStream;
    .param p2, "codepage"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/apache/index/poi/hpsf/WritingNotSupportedException;
        }
    .end annotation

    .prologue
    .line 108
    const/4 v0, 0x0

    .line 109
    .local v0, "length":I
    invoke-virtual {p0}, Lorg/apache/index/poi/hpsf/MutableProperty;->getType()J

    move-result-wide v2

    .line 112
    .local v2, "variantType":J
    const/16 v1, 0x4b0

    if-ne p2, v1, :cond_0

    const-wide/16 v4, 0x1e

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    .line 113
    const-wide/16 v2, 0x1f

    .line 115
    :cond_0
    invoke-static {p1, v2, v3}, Lorg/apache/index/poi/hpsf/TypeWriter;->writeUIntToStream(Ljava/io/OutputStream;J)I

    move-result v1

    add-int/2addr v0, v1

    .line 116
    invoke-virtual {p0}, Lorg/apache/index/poi/hpsf/MutableProperty;->getValue()Ljava/lang/Object;

    move-result-object v1

    invoke-static {p1, v2, v3, v1, p2}, Lorg/apache/index/poi/hpsf/VariantSupport;->write(Ljava/io/OutputStream;JLjava/lang/Object;I)I

    move-result v1

    add-int/2addr v0, v1

    .line 117
    return v0
.end method
