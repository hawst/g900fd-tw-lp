.class public Lorg/apache/index/poi/hssf/record/common/UnicodeString$FormatRun;
.super Ljava/lang/Object;
.source "UnicodeString.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/index/poi/hssf/record/common/UnicodeString;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "FormatRun"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lorg/apache/index/poi/hssf/record/common/UnicodeString$FormatRun;",
        ">;"
    }
.end annotation


# instance fields
.field final _character:S

.field _fontIndex:S


# direct methods
.method public constructor <init>(Lorg/apache/index/poi/util/LittleEndianInput;)V
    .locals 2
    .param p1, "in"    # Lorg/apache/index/poi/util/LittleEndianInput;

    .prologue
    .line 63
    invoke-interface {p1}, Lorg/apache/index/poi/util/LittleEndianInput;->readShort()S

    move-result v0

    invoke-interface {p1}, Lorg/apache/index/poi/util/LittleEndianInput;->readShort()S

    move-result v1

    invoke-direct {p0, v0, v1}, Lorg/apache/index/poi/hssf/record/common/UnicodeString$FormatRun;-><init>(SS)V

    .line 64
    return-void
.end method

.method public constructor <init>(SS)V
    .locals 0
    .param p1, "character"    # S
    .param p2, "fontIndex"    # S

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    iput-short p1, p0, Lorg/apache/index/poi/hssf/record/common/UnicodeString$FormatRun;->_character:S

    .line 59
    iput-short p2, p0, Lorg/apache/index/poi/hssf/record/common/UnicodeString$FormatRun;->_fontIndex:S

    .line 60
    return-void
.end method


# virtual methods
.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 1
    check-cast p1, Lorg/apache/index/poi/hssf/record/common/UnicodeString$FormatRun;

    invoke-virtual {p0, p1}, Lorg/apache/index/poi/hssf/record/common/UnicodeString$FormatRun;->compareTo(Lorg/apache/index/poi/hssf/record/common/UnicodeString$FormatRun;)I

    move-result v0

    return v0
.end method

.method public compareTo(Lorg/apache/index/poi/hssf/record/common/UnicodeString$FormatRun;)I
    .locals 2
    .param p1, "r"    # Lorg/apache/index/poi/hssf/record/common/UnicodeString$FormatRun;

    .prologue
    .line 84
    iget-short v0, p0, Lorg/apache/index/poi/hssf/record/common/UnicodeString$FormatRun;->_character:S

    iget-short v1, p1, Lorg/apache/index/poi/hssf/record/common/UnicodeString$FormatRun;->_character:S

    if-ne v0, v1, :cond_0

    iget-short v0, p0, Lorg/apache/index/poi/hssf/record/common/UnicodeString$FormatRun;->_fontIndex:S

    iget-short v1, p1, Lorg/apache/index/poi/hssf/record/common/UnicodeString$FormatRun;->_fontIndex:S

    if-ne v0, v1, :cond_0

    .line 85
    const/4 v0, 0x0

    .line 90
    :goto_0
    return v0

    .line 87
    :cond_0
    iget-short v0, p0, Lorg/apache/index/poi/hssf/record/common/UnicodeString$FormatRun;->_character:S

    iget-short v1, p1, Lorg/apache/index/poi/hssf/record/common/UnicodeString$FormatRun;->_character:S

    if-ne v0, v1, :cond_1

    .line 88
    iget-short v0, p0, Lorg/apache/index/poi/hssf/record/common/UnicodeString$FormatRun;->_fontIndex:S

    iget-short v1, p1, Lorg/apache/index/poi/hssf/record/common/UnicodeString$FormatRun;->_fontIndex:S

    sub-int/2addr v0, v1

    goto :goto_0

    .line 90
    :cond_1
    iget-short v0, p0, Lorg/apache/index/poi/hssf/record/common/UnicodeString$FormatRun;->_character:S

    iget-short v1, p1, Lorg/apache/index/poi/hssf/record/common/UnicodeString$FormatRun;->_character:S

    sub-int/2addr v0, v1

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 75
    instance-of v2, p1, Lorg/apache/index/poi/hssf/record/common/UnicodeString$FormatRun;

    if-nez v2, :cond_1

    .line 80
    :cond_0
    :goto_0
    return v1

    :cond_1
    move-object v0, p1

    .line 78
    check-cast v0, Lorg/apache/index/poi/hssf/record/common/UnicodeString$FormatRun;

    .line 80
    .local v0, "other":Lorg/apache/index/poi/hssf/record/common/UnicodeString$FormatRun;
    iget-short v2, p0, Lorg/apache/index/poi/hssf/record/common/UnicodeString$FormatRun;->_character:S

    iget-short v3, v0, Lorg/apache/index/poi/hssf/record/common/UnicodeString$FormatRun;->_character:S

    if-ne v2, v3, :cond_0

    iget-short v2, p0, Lorg/apache/index/poi/hssf/record/common/UnicodeString$FormatRun;->_fontIndex:S

    iget-short v3, v0, Lorg/apache/index/poi/hssf/record/common/UnicodeString$FormatRun;->_fontIndex:S

    if-ne v2, v3, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method public getCharacterPos()S
    .locals 1

    .prologue
    .line 67
    iget-short v0, p0, Lorg/apache/index/poi/hssf/record/common/UnicodeString$FormatRun;->_character:S

    return v0
.end method

.method public getFontIndex()S
    .locals 1

    .prologue
    .line 71
    iget-short v0, p0, Lorg/apache/index/poi/hssf/record/common/UnicodeString$FormatRun;->_fontIndex:S

    return v0
.end method

.method public serialize(Lorg/apache/index/poi/util/LittleEndianOutput;)V
    .locals 1
    .param p1, "out"    # Lorg/apache/index/poi/util/LittleEndianOutput;

    .prologue
    .line 98
    iget-short v0, p0, Lorg/apache/index/poi/hssf/record/common/UnicodeString$FormatRun;->_character:S

    invoke-interface {p1, v0}, Lorg/apache/index/poi/util/LittleEndianOutput;->writeShort(I)V

    .line 99
    iget-short v0, p0, Lorg/apache/index/poi/hssf/record/common/UnicodeString$FormatRun;->_fontIndex:S

    invoke-interface {p1, v0}, Lorg/apache/index/poi/util/LittleEndianOutput;->writeShort(I)V

    .line 100
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 94
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "character="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-short v1, p0, Lorg/apache/index/poi/hssf/record/common/UnicodeString$FormatRun;->_character:S

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ",fontIndex="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-short v1, p0, Lorg/apache/index/poi/hssf/record/common/UnicodeString$FormatRun;->_fontIndex:S

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
