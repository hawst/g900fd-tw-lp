.class public Lorg/apache/index/poi/util/POILogFactory;
.super Ljava/lang/Object;
.source "POILogFactory.java"


# static fields
.field private static _loggerClassName:Ljava/lang/String;

.field private static _loggers:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lorg/apache/index/poi/util/POILogger;",
            ">;"
        }
    .end annotation
.end field

.field private static _nullLogger:Lorg/apache/index/poi/util/POILogger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 43
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lorg/apache/index/poi/util/POILogFactory;->_loggers:Ljava/util/Map;

    .line 49
    new-instance v0, Lorg/apache/index/poi/util/NullLogger;

    invoke-direct {v0}, Lorg/apache/index/poi/util/NullLogger;-><init>()V

    sput-object v0, Lorg/apache/index/poi/util/POILogFactory;->_nullLogger:Lorg/apache/index/poi/util/POILogger;

    .line 54
    const/4 v0, 0x0

    sput-object v0, Lorg/apache/index/poi/util/POILogFactory;->_loggerClassName:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    return-void
.end method

.method public static getLogger(Ljava/lang/Class;)Lorg/apache/index/poi/util/POILogger;
    .locals 1
    .param p0, "theclass"    # Ljava/lang/Class;

    .prologue
    .line 73
    invoke-virtual {p0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/index/poi/util/POILogFactory;->getLogger(Ljava/lang/String;)Lorg/apache/index/poi/util/POILogger;

    move-result-object v0

    return-object v0
.end method

.method public static getLogger(Ljava/lang/String;)Lorg/apache/index/poi/util/POILogger;
    .locals 7
    .param p0, "cat"    # Ljava/lang/String;

    .prologue
    .line 86
    const/4 v2, 0x0

    .line 93
    .local v2, "logger":Lorg/apache/index/poi/util/POILogger;
    sget-object v4, Lorg/apache/index/poi/util/POILogFactory;->_loggerClassName:Ljava/lang/String;

    if-nez v4, :cond_0

    .line 95
    :try_start_0
    const-string/jumbo v4, "org.apache.poi.util.POILogger"

    invoke-static {v4}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    sput-object v4, Lorg/apache/index/poi/util/POILogFactory;->_loggerClassName:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 102
    :goto_0
    sget-object v4, Lorg/apache/index/poi/util/POILogFactory;->_loggerClassName:Ljava/lang/String;

    if-nez v4, :cond_0

    .line 103
    sget-object v4, Lorg/apache/index/poi/util/POILogFactory;->_nullLogger:Lorg/apache/index/poi/util/POILogger;

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    sput-object v4, Lorg/apache/index/poi/util/POILogFactory;->_loggerClassName:Ljava/lang/String;

    .line 109
    :cond_0
    sget-object v4, Lorg/apache/index/poi/util/POILogFactory;->_loggerClassName:Ljava/lang/String;

    sget-object v5, Lorg/apache/index/poi/util/POILogFactory;->_nullLogger:Lorg/apache/index/poi/util/POILogger;

    invoke-virtual {v5}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 110
    sget-object v4, Lorg/apache/index/poi/util/POILogFactory;->_nullLogger:Lorg/apache/index/poi/util/POILogger;

    .line 132
    :goto_1
    return-object v4

    .line 96
    :catch_0
    move-exception v1

    .line 97
    .local v1, "e":Ljava/lang/Exception;
    const-string/jumbo v4, "DocumentService"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "Exception :"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 116
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_1
    sget-object v4, Lorg/apache/index/poi/util/POILogFactory;->_loggers:Ljava/util/Map;

    invoke-interface {v4, p0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 117
    sget-object v4, Lorg/apache/index/poi/util/POILogFactory;->_loggers:Ljava/util/Map;

    invoke-interface {v4, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "logger":Lorg/apache/index/poi/util/POILogger;
    check-cast v2, Lorg/apache/index/poi/util/POILogger;

    .restart local v2    # "logger":Lorg/apache/index/poi/util/POILogger;
    :goto_2
    move-object v4, v2

    .line 132
    goto :goto_1

    .line 121
    :cond_2
    :try_start_1
    sget-object v4, Lorg/apache/index/poi/util/POILogFactory;->_loggerClassName:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v3

    .line 122
    .local v3, "loggerClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lorg/apache/index/poi/util/POILogger;>;"
    invoke-virtual {v3}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v4

    move-object v0, v4

    check-cast v0, Lorg/apache/index/poi/util/POILogger;

    move-object v2, v0

    .line 123
    invoke-virtual {v2, p0}, Lorg/apache/index/poi/util/POILogger;->initialize(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 130
    .end local v3    # "loggerClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lorg/apache/index/poi/util/POILogger;>;"
    :goto_3
    sget-object v4, Lorg/apache/index/poi/util/POILogFactory;->_loggers:Ljava/util/Map;

    invoke-interface {v4, p0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 124
    :catch_1
    move-exception v1

    .line 126
    .restart local v1    # "e":Ljava/lang/Exception;
    sget-object v2, Lorg/apache/index/poi/util/POILogFactory;->_nullLogger:Lorg/apache/index/poi/util/POILogger;

    goto :goto_3
.end method
