.class public Lorg/apache/index/poi/hslf/record/SlideListWithText$SlideAtomsSet;
.super Ljava/lang/Object;
.source "SlideListWithText.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/index/poi/hslf/record/SlideListWithText;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SlideAtomsSet"
.end annotation


# instance fields
.field private slidePersistAtom:Lorg/apache/index/poi/hslf/record/SlidePersistAtom;

.field private slideRecords:[Lorg/apache/index/poi/hslf/record/Record;


# direct methods
.method public constructor <init>(Lorg/apache/index/poi/hslf/record/SlidePersistAtom;[Lorg/apache/index/poi/hslf/record/Record;)V
    .locals 0
    .param p1, "s"    # Lorg/apache/index/poi/hslf/record/SlidePersistAtom;
    .param p2, "r"    # [Lorg/apache/index/poi/hslf/record/Record;

    .prologue
    .line 185
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 186
    iput-object p1, p0, Lorg/apache/index/poi/hslf/record/SlideListWithText$SlideAtomsSet;->slidePersistAtom:Lorg/apache/index/poi/hslf/record/SlidePersistAtom;

    .line 187
    iput-object p2, p0, Lorg/apache/index/poi/hslf/record/SlideListWithText$SlideAtomsSet;->slideRecords:[Lorg/apache/index/poi/hslf/record/Record;

    .line 188
    return-void
.end method


# virtual methods
.method public getSlidePersistAtom()Lorg/apache/index/poi/hslf/record/SlidePersistAtom;
    .locals 1

    .prologue
    .line 180
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/SlideListWithText$SlideAtomsSet;->slidePersistAtom:Lorg/apache/index/poi/hslf/record/SlidePersistAtom;

    return-object v0
.end method

.method public getSlideRecords()[Lorg/apache/index/poi/hslf/record/Record;
    .locals 1

    .prologue
    .line 182
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/SlideListWithText$SlideAtomsSet;->slideRecords:[Lorg/apache/index/poi/hslf/record/Record;

    return-object v0
.end method
