.class public Lorg/apache/index/poi/hwpf/model/SectionTable;
.super Ljava/lang/Object;
.source "SectionTable.java"


# static fields
.field private static final SED_SIZE:I = 0xc


# instance fields
.field protected _sections:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lorg/apache/index/poi/hwpf/model/SEPX;",
            ">;"
        }
    .end annotation
.end field

.field protected _text:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/apache/index/poi/hwpf/model/TextPiece;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/apache/index/poi/hwpf/model/SectionTable;->_sections:Ljava/util/ArrayList;

    .line 38
    return-void
.end method

.method public constructor <init>([B[BIIILorg/apache/index/poi/hwpf/model/TextPieceTable;Lorg/apache/index/poi/hwpf/model/CPSplitCalculator;)V
    .locals 22
    .param p1, "documentStream"    # [B
    .param p2, "tableStream"    # [B
    .param p3, "offset"    # I
    .param p4, "size"    # I
    .param p5, "fcMin"    # I
    .param p6, "tpt"    # Lorg/apache/index/poi/hwpf/model/TextPieceTable;
    .param p7, "cps"    # Lorg/apache/index/poi/hwpf/model/CPSplitCalculator;

    .prologue
    .line 41
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    .line 33
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iput-object v4, v0, Lorg/apache/index/poi/hwpf/model/SectionTable;->_sections:Ljava/util/ArrayList;

    .line 45
    new-instance v18, Lorg/apache/index/poi/hwpf/model/PlexOfCps;

    const/16 v4, 0xc

    move-object/from16 v0, v18

    move-object/from16 v1, p2

    move/from16 v2, p3

    move/from16 v3, p4

    invoke-direct {v0, v1, v2, v3, v4}, Lorg/apache/index/poi/hwpf/model/PlexOfCps;-><init>([BIII)V

    .line 46
    .local v18, "sedPlex":Lorg/apache/index/poi/hwpf/model/PlexOfCps;
    invoke-virtual/range {p6 .. p6}, Lorg/apache/index/poi/hwpf/model/TextPieceTable;->getTextPieces()Ljava/util/List;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lorg/apache/index/poi/hwpf/model/SectionTable;->_text:Ljava/util/List;

    .line 48
    invoke-virtual/range {v18 .. v18}, Lorg/apache/index/poi/hwpf/model/PlexOfCps;->length()I

    move-result v12

    .line 50
    .local v12, "length":I
    const/16 v20, 0x0

    .local v20, "x":I
    :goto_0
    move/from16 v0, v20

    if-lt v0, v12, :cond_1

    .line 78
    invoke-virtual/range {p7 .. p7}, Lorg/apache/index/poi/hwpf/model/CPSplitCalculator;->getMainDocumentEnd()I

    move-result v13

    .line 79
    .local v13, "mainEndsAt":I
    const/4 v14, 0x0

    .line 80
    .local v14, "matchAt":Z
    const/4 v15, 0x0

    .line 81
    .local v15, "matchHalf":Z
    const/4 v11, 0x0

    .local v11, "i":I
    :goto_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/index/poi/hwpf/model/SectionTable;->_sections:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lt v11, v4, :cond_3

    .line 89
    if-nez v14, :cond_0

    if-eqz v15, :cond_0

    .line 90
    sget-object v4, Ljava/lang/System;->err:Ljava/io/PrintStream;

    const-string/jumbo v8, "Your document seemed to be mostly unicode, but the section definition was in bytes! Trying anyway, but things may well go wrong!"

    invoke-virtual {v4, v8}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 91
    const/4 v11, 0x0

    :goto_2
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/index/poi/hwpf/model/SectionTable;->_sections:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lt v11, v4, :cond_7

    .line 99
    :cond_0
    return-void

    .line 52
    .end local v11    # "i":I
    .end local v13    # "mainEndsAt":I
    .end local v14    # "matchAt":Z
    .end local v15    # "matchHalf":Z
    :cond_1
    move-object/from16 v0, v18

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lorg/apache/index/poi/hwpf/model/PlexOfCps;->getProperty(I)Lorg/apache/index/poi/hwpf/model/GenericPropertyNode;

    move-result-object v16

    .line 53
    .local v16, "node":Lorg/apache/index/poi/hwpf/model/GenericPropertyNode;
    new-instance v5, Lorg/apache/index/poi/hwpf/model/SectionDescriptor;

    invoke-virtual/range {v16 .. v16}, Lorg/apache/index/poi/hwpf/model/GenericPropertyNode;->getBytes()[B

    move-result-object v4

    const/4 v8, 0x0

    invoke-direct {v5, v4, v8}, Lorg/apache/index/poi/hwpf/model/SectionDescriptor;-><init>([BI)V

    .line 55
    .local v5, "sed":Lorg/apache/index/poi/hwpf/model/SectionDescriptor;
    invoke-virtual {v5}, Lorg/apache/index/poi/hwpf/model/SectionDescriptor;->getFc()I

    move-result v10

    .line 56
    .local v10, "fileOffset":I
    invoke-virtual/range {v16 .. v16}, Lorg/apache/index/poi/hwpf/model/GenericPropertyNode;->getStart()I

    move-result v4

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lorg/apache/index/poi/hwpf/model/SectionTable;->CPtoFC(I)I

    move-result v6

    .line 57
    .local v6, "startAt":I
    invoke-virtual/range {v16 .. v16}, Lorg/apache/index/poi/hwpf/model/GenericPropertyNode;->getEnd()I

    move-result v4

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lorg/apache/index/poi/hwpf/model/SectionTable;->CPtoFC(I)I

    move-result v7

    .line 60
    .local v7, "endAt":I
    const/4 v4, -0x1

    if-ne v10, v4, :cond_2

    .line 62
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/index/poi/hwpf/model/SectionTable;->_sections:Ljava/util/ArrayList;

    move-object/from16 v21, v0

    new-instance v4, Lorg/apache/index/poi/hwpf/model/SEPX;

    const/4 v8, 0x0

    new-array v9, v8, [B

    move-object/from16 v8, p6

    invoke-direct/range {v4 .. v9}, Lorg/apache/index/poi/hwpf/model/SEPX;-><init>(Lorg/apache/index/poi/hwpf/model/SectionDescriptor;IILorg/apache/index/poi/hwpf/model/CharIndexTranslator;[B)V

    move-object/from16 v0, v21

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 50
    :goto_3
    add-int/lit8 v20, v20, 0x1

    goto :goto_0

    .line 67
    :cond_2
    move-object/from16 v0, p1

    invoke-static {v0, v10}, Lorg/apache/index/poi/util/LittleEndian;->getShort([BI)S

    move-result v19

    .line 68
    .local v19, "sepxSize":I
    move/from16 v0, v19

    new-array v9, v0, [B

    .line 69
    .local v9, "buf":[B
    add-int/lit8 v10, v10, 0x2

    .line 70
    const/4 v4, 0x0

    array-length v8, v9

    move-object/from16 v0, p1

    invoke-static {v0, v10, v9, v4, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 71
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/index/poi/hwpf/model/SectionTable;->_sections:Ljava/util/ArrayList;

    move-object/from16 v21, v0

    new-instance v4, Lorg/apache/index/poi/hwpf/model/SEPX;

    move-object/from16 v8, p6

    invoke-direct/range {v4 .. v9}, Lorg/apache/index/poi/hwpf/model/SEPX;-><init>(Lorg/apache/index/poi/hwpf/model/SectionDescriptor;IILorg/apache/index/poi/hwpf/model/CharIndexTranslator;[B)V

    move-object/from16 v0, v21

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 82
    .end local v5    # "sed":Lorg/apache/index/poi/hwpf/model/SectionDescriptor;
    .end local v6    # "startAt":I
    .end local v7    # "endAt":I
    .end local v9    # "buf":[B
    .end local v10    # "fileOffset":I
    .end local v16    # "node":Lorg/apache/index/poi/hwpf/model/GenericPropertyNode;
    .end local v19    # "sepxSize":I
    .restart local v11    # "i":I
    .restart local v13    # "mainEndsAt":I
    .restart local v14    # "matchAt":Z
    .restart local v15    # "matchHalf":Z
    :cond_3
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/index/poi/hwpf/model/SectionTable;->_sections:Ljava/util/ArrayList;

    invoke-virtual {v4, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lorg/apache/index/poi/hwpf/model/SEPX;

    .line 83
    .local v17, "s":Lorg/apache/index/poi/hwpf/model/SEPX;
    invoke-virtual/range {v17 .. v17}, Lorg/apache/index/poi/hwpf/model/SEPX;->getEnd()I

    move-result v4

    if-ne v4, v13, :cond_5

    .line 84
    const/4 v14, 0x1

    .line 81
    :cond_4
    :goto_4
    add-int/lit8 v11, v11, 0x1

    goto/16 :goto_1

    .line 85
    :cond_5
    invoke-virtual/range {v17 .. v17}, Lorg/apache/index/poi/hwpf/model/SEPX;->getEndBytes()I

    move-result v4

    if-eq v4, v13, :cond_6

    invoke-virtual/range {v17 .. v17}, Lorg/apache/index/poi/hwpf/model/SEPX;->getEndBytes()I

    move-result v4

    add-int/lit8 v8, v13, -0x1

    if-ne v4, v8, :cond_4

    .line 86
    :cond_6
    const/4 v15, 0x1

    goto :goto_4

    .line 92
    .end local v17    # "s":Lorg/apache/index/poi/hwpf/model/SEPX;
    :cond_7
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/index/poi/hwpf/model/SectionTable;->_sections:Ljava/util/ArrayList;

    invoke-virtual {v4, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lorg/apache/index/poi/hwpf/model/SEPX;

    .line 93
    .restart local v17    # "s":Lorg/apache/index/poi/hwpf/model/SEPX;
    move-object/from16 v0, v18

    invoke-virtual {v0, v11}, Lorg/apache/index/poi/hwpf/model/PlexOfCps;->getProperty(I)Lorg/apache/index/poi/hwpf/model/GenericPropertyNode;

    move-result-object v16

    .line 95
    .restart local v16    # "node":Lorg/apache/index/poi/hwpf/model/GenericPropertyNode;
    invoke-virtual/range {v16 .. v16}, Lorg/apache/index/poi/hwpf/model/GenericPropertyNode;->getStart()I

    move-result v4

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lorg/apache/index/poi/hwpf/model/SectionTable;->CPtoFC(I)I

    move-result v4

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Lorg/apache/index/poi/hwpf/model/SEPX;->setStart(I)V

    .line 96
    invoke-virtual/range {v16 .. v16}, Lorg/apache/index/poi/hwpf/model/GenericPropertyNode;->getEnd()I

    move-result v4

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lorg/apache/index/poi/hwpf/model/SectionTable;->CPtoFC(I)I

    move-result v4

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Lorg/apache/index/poi/hwpf/model/SEPX;->setEnd(I)V

    .line 91
    add-int/lit8 v11, v11, 0x1

    goto/16 :goto_2
.end method

.method private CPtoFC(I)I
    .locals 5
    .param p1, "CP"    # I

    .prologue
    .line 123
    const/4 v1, 0x0

    .line 125
    .local v1, "TP":Lorg/apache/index/poi/hwpf/model/TextPiece;
    iget-object v4, p0, Lorg/apache/index/poi/hwpf/model/SectionTable;->_text:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    add-int/lit8 v2, v4, -0x1

    .local v2, "i":I
    :goto_0
    const/4 v4, -0x1

    if-gt v2, v4, :cond_1

    .line 131
    :cond_0
    if-nez v1, :cond_2

    .line 132
    const/4 v0, 0x0

    .line 140
    :goto_1
    return v0

    .line 127
    :cond_1
    iget-object v4, p0, Lorg/apache/index/poi/hwpf/model/SectionTable;->_text:Ljava/util/List;

    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "TP":Lorg/apache/index/poi/hwpf/model/TextPiece;
    check-cast v1, Lorg/apache/index/poi/hwpf/model/TextPiece;

    .line 129
    .restart local v1    # "TP":Lorg/apache/index/poi/hwpf/model/TextPiece;
    invoke-virtual {v1}, Lorg/apache/index/poi/hwpf/model/TextPiece;->getCP()I

    move-result v4

    if-ge p1, v4, :cond_0

    .line 125
    add-int/lit8 v2, v2, -0x1

    goto :goto_0

    .line 134
    :cond_2
    invoke-virtual {v1}, Lorg/apache/index/poi/hwpf/model/TextPiece;->getPieceDescriptor()Lorg/apache/index/poi/hwpf/model/PieceDescriptor;

    move-result-object v4

    invoke-virtual {v4}, Lorg/apache/index/poi/hwpf/model/PieceDescriptor;->getFilePosition()I

    move-result v0

    .line 135
    .local v0, "FC":I
    invoke-virtual {v1}, Lorg/apache/index/poi/hwpf/model/TextPiece;->getCP()I

    move-result v4

    sub-int v3, p1, v4

    .line 136
    .local v3, "offset":I
    invoke-virtual {v1}, Lorg/apache/index/poi/hwpf/model/TextPiece;->isUnicode()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 137
    mul-int/lit8 v3, v3, 0x2

    .line 139
    :cond_3
    add-int/2addr v0, v3

    .line 140
    goto :goto_1
.end method


# virtual methods
.method public adjustForInsert(II)V
    .locals 4
    .param p1, "listIndex"    # I
    .param p2, "length"    # I

    .prologue
    .line 103
    iget-object v3, p0, Lorg/apache/index/poi/hwpf/model/SectionTable;->_sections:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 104
    .local v1, "size":I
    iget-object v3, p0, Lorg/apache/index/poi/hwpf/model/SectionTable;->_sections:Ljava/util/ArrayList;

    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/index/poi/hwpf/model/SEPX;

    .line 105
    .local v0, "sepx":Lorg/apache/index/poi/hwpf/model/SEPX;
    invoke-virtual {v0}, Lorg/apache/index/poi/hwpf/model/SEPX;->getEnd()I

    move-result v3

    add-int/2addr v3, p2

    invoke-virtual {v0, v3}, Lorg/apache/index/poi/hwpf/model/SEPX;->setEnd(I)V

    .line 107
    add-int/lit8 v2, p1, 0x1

    .local v2, "x":I
    :goto_0
    if-lt v2, v1, :cond_0

    .line 113
    return-void

    .line 109
    :cond_0
    iget-object v3, p0, Lorg/apache/index/poi/hwpf/model/SectionTable;->_sections:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "sepx":Lorg/apache/index/poi/hwpf/model/SEPX;
    check-cast v0, Lorg/apache/index/poi/hwpf/model/SEPX;

    .line 110
    .restart local v0    # "sepx":Lorg/apache/index/poi/hwpf/model/SEPX;
    invoke-virtual {v0}, Lorg/apache/index/poi/hwpf/model/SEPX;->getStart()I

    move-result v3

    add-int/2addr v3, p2

    invoke-virtual {v0, v3}, Lorg/apache/index/poi/hwpf/model/SEPX;->setStart(I)V

    .line 111
    invoke-virtual {v0}, Lorg/apache/index/poi/hwpf/model/SEPX;->getEnd()I

    move-result v3

    add-int/2addr v3, p2

    invoke-virtual {v0, v3}, Lorg/apache/index/poi/hwpf/model/SEPX;->setEnd(I)V

    .line 107
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public getSections()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lorg/apache/index/poi/hwpf/model/SEPX;",
            ">;"
        }
    .end annotation

    .prologue
    .line 145
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/SectionTable;->_sections:Ljava/util/ArrayList;

    return-object v0
.end method
