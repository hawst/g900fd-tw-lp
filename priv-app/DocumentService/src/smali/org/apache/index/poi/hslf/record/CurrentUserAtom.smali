.class public Lorg/apache/index/poi/hslf/record/CurrentUserAtom;
.super Ljava/lang/Object;
.source "CurrentUserAtom.java"


# static fields
.field public static final atomHeader:[B

.field public static final encHeaderToken:[B

.field public static final headerToken:[B

.field public static final ppt97FileVer:[B


# instance fields
.field private _contents:[B

.field private currentEditOffset:J

.field private docFinalVersion:I

.field private docMajorNo:B

.field private docMinorNo:B

.field private lastEditUser:Ljava/lang/String;

.field private releaseVersion:J


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x4

    const/4 v3, 0x3

    .line 44
    new-array v0, v4, [B

    const/16 v1, -0xa

    aput-byte v1, v0, v5

    const/16 v1, 0xf

    aput-byte v1, v0, v3

    sput-object v0, Lorg/apache/index/poi/hslf/record/CurrentUserAtom;->atomHeader:[B

    .line 46
    new-array v0, v4, [B

    fill-array-data v0, :array_0

    sput-object v0, Lorg/apache/index/poi/hslf/record/CurrentUserAtom;->headerToken:[B

    .line 48
    new-array v0, v4, [B

    fill-array-data v0, :array_1

    sput-object v0, Lorg/apache/index/poi/hslf/record/CurrentUserAtom;->encHeaderToken:[B

    .line 50
    const/4 v0, 0x6

    new-array v0, v0, [B

    const/4 v1, 0x0

    const/16 v2, 0x8

    aput-byte v2, v0, v1

    const/16 v1, -0xd

    aput-byte v1, v0, v5

    aput-byte v3, v0, v3

    aput-byte v3, v0, v4

    sput-object v0, Lorg/apache/index/poi/hslf/record/CurrentUserAtom;->ppt97FileVer:[B

    return-void

    .line 46
    :array_0
    .array-data 1
        0x5ft
        -0x40t
        -0x6ft
        -0x1dt
    .end array-data

    .line 48
    :array_1
    .array-data 1
        -0x21t
        -0x3ct
        -0x2ft
        -0xdt
    .end array-data
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 90
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 91
    new-array v0, v1, [B

    iput-object v0, p0, Lorg/apache/index/poi/hslf/record/CurrentUserAtom;->_contents:[B

    .line 94
    const/16 v0, 0x3f4

    iput v0, p0, Lorg/apache/index/poi/hslf/record/CurrentUserAtom;->docFinalVersion:I

    .line 95
    const/4 v0, 0x3

    iput-byte v0, p0, Lorg/apache/index/poi/hslf/record/CurrentUserAtom;->docMajorNo:B

    .line 96
    iput-byte v1, p0, Lorg/apache/index/poi/hslf/record/CurrentUserAtom;->docMinorNo:B

    .line 97
    const-wide/16 v0, 0x8

    iput-wide v0, p0, Lorg/apache/index/poi/hslf/record/CurrentUserAtom;->releaseVersion:J

    .line 98
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lorg/apache/index/poi/hslf/record/CurrentUserAtom;->currentEditOffset:J

    .line 99
    const-string/jumbo v0, "Apache POI"

    iput-object v0, p0, Lorg/apache/index/poi/hslf/record/CurrentUserAtom;->lastEditUser:Ljava/lang/String;

    .line 100
    return-void
.end method

.method public constructor <init>(Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;)V
    .locals 7
    .param p1, "dir"    # Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 111
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 114
    const-string/jumbo v4, "Current User"

    invoke-virtual {p1, v4}, Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;->getEntry(Ljava/lang/String;)Lorg/apache/index/poi/poifs/filesystem/Entry;

    move-result-object v0

    check-cast v0, Lorg/apache/index/poi/poifs/filesystem/DocumentEntry;

    .line 117
    .local v0, "docProps":Lorg/apache/index/poi/poifs/filesystem/DocumentEntry;
    invoke-interface {v0}, Lorg/apache/index/poi/poifs/filesystem/DocumentEntry;->getSize()I

    move-result v4

    const/high16 v5, 0x20000

    if-le v4, v5, :cond_0

    .line 118
    new-instance v4, Lorg/apache/index/poi/hslf/exceptions/CorruptPowerPointFileException;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "The Current User stream is implausably long. It\'s normally 28-200 bytes long, but was "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v0}, Lorg/apache/index/poi/poifs/filesystem/DocumentEntry;->getSize()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, " bytes"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Lorg/apache/index/poi/hslf/exceptions/CorruptPowerPointFileException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 122
    :cond_0
    invoke-interface {v0}, Lorg/apache/index/poi/poifs/filesystem/DocumentEntry;->getSize()I

    move-result v4

    new-array v4, v4, [B

    iput-object v4, p0, Lorg/apache/index/poi/hslf/record/CurrentUserAtom;->_contents:[B

    .line 123
    const/4 v1, 0x0

    .line 125
    .local v1, "in":Ljava/io/InputStream;
    :try_start_0
    const-string/jumbo v4, "Current User"

    invoke-virtual {p1, v4}, Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;->createDocumentInputStream(Ljava/lang/String;)Lorg/apache/index/poi/poifs/filesystem/DocumentInputStream;

    move-result-object v1

    .line 126
    iget-object v4, p0, Lorg/apache/index/poi/hslf/record/CurrentUserAtom;->_contents:[B

    invoke-virtual {v1, v4}, Ljava/io/InputStream;->read([B)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    .line 127
    .local v2, "read":I
    if-gez v2, :cond_3

    .line 131
    if-eqz v1, :cond_1

    .line 132
    :try_start_1
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 154
    :cond_1
    :goto_0
    return-void

    .line 129
    .end local v2    # "read":I
    :catchall_0
    move-exception v4

    .line 131
    if-eqz v1, :cond_2

    .line 132
    :try_start_2
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 136
    :cond_2
    :goto_1
    throw v4

    .line 131
    .restart local v2    # "read":I
    :cond_3
    if-eqz v1, :cond_4

    .line 132
    :try_start_3
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    .line 140
    :cond_4
    :goto_2
    iget-object v4, p0, Lorg/apache/index/poi/hslf/record/CurrentUserAtom;->_contents:[B

    array-length v4, v4

    const/16 v5, 0x1c

    if-ge v4, v5, :cond_6

    .line 141
    iget-object v4, p0, Lorg/apache/index/poi/hslf/record/CurrentUserAtom;->_contents:[B

    array-length v4, v4

    const/4 v5, 0x4

    if-lt v4, v5, :cond_5

    .line 143
    iget-object v4, p0, Lorg/apache/index/poi/hslf/record/CurrentUserAtom;->_contents:[B

    invoke-static {v4}, Lorg/apache/index/poi/util/LittleEndian;->getInt([B)I

    move-result v3

    .line 144
    .local v3, "size":I
    sget-object v4, Ljava/lang/System;->err:Ljava/io/PrintStream;

    invoke-virtual {v4, v3}, Ljava/io/PrintStream;->println(I)V

    .line 145
    add-int/lit8 v4, v3, 0x4

    iget-object v5, p0, Lorg/apache/index/poi/hslf/record/CurrentUserAtom;->_contents:[B

    array-length v5, v5

    if-ne v4, v5, :cond_5

    .line 146
    new-instance v4, Lorg/apache/index/poi/hslf/exceptions/OldPowerPointFormatException;

    const-string/jumbo v5, "Based on the Current User stream, you seem to have supplied a PowerPoint95 file, which isn\'t supported"

    invoke-direct {v4, v5}, Lorg/apache/index/poi/hslf/exceptions/OldPowerPointFormatException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 149
    .end local v3    # "size":I
    :cond_5
    new-instance v4, Lorg/apache/index/poi/hslf/exceptions/CorruptPowerPointFileException;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "The Current User stream must be at least 28 bytes long, but was only "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, p0, Lorg/apache/index/poi/hslf/record/CurrentUserAtom;->_contents:[B

    array-length v6, v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Lorg/apache/index/poi/hslf/exceptions/CorruptPowerPointFileException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 153
    :cond_6
    invoke-direct {p0}, Lorg/apache/index/poi/hslf/record/CurrentUserAtom;->init()V

    goto :goto_0

    .line 134
    :catch_0
    move-exception v4

    goto :goto_0

    .end local v2    # "read":I
    :catch_1
    move-exception v5

    goto :goto_1

    .restart local v2    # "read":I
    :catch_2
    move-exception v4

    goto :goto_2
.end method

.method public constructor <init>(Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;)V
    .locals 1
    .param p1, "fs"    # Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 106
    invoke-virtual {p1}, Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;->getRoot()Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/apache/index/poi/hslf/record/CurrentUserAtom;-><init>(Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;)V

    .line 107
    return-void
.end method

.method public constructor <init>([B)V
    .locals 0
    .param p1, "b"    # [B

    .prologue
    .line 159
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 160
    iput-object p1, p0, Lorg/apache/index/poi/hslf/record/CurrentUserAtom;->_contents:[B

    .line 161
    invoke-direct {p0}, Lorg/apache/index/poi/hslf/record/CurrentUserAtom;->init()V

    .line 162
    return-void
.end method

.method private init()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 170
    iget-object v3, p0, Lorg/apache/index/poi/hslf/record/CurrentUserAtom;->_contents:[B

    const/16 v6, 0xc

    aget-byte v3, v3, v6

    sget-object v6, Lorg/apache/index/poi/hslf/record/CurrentUserAtom;->encHeaderToken:[B

    aget-byte v6, v6, v8

    if-ne v3, v6, :cond_0

    .line 171
    iget-object v3, p0, Lorg/apache/index/poi/hslf/record/CurrentUserAtom;->_contents:[B

    const/16 v6, 0xd

    aget-byte v3, v3, v6

    sget-object v6, Lorg/apache/index/poi/hslf/record/CurrentUserAtom;->encHeaderToken:[B

    const/4 v7, 0x1

    aget-byte v6, v6, v7

    if-ne v3, v6, :cond_0

    .line 172
    iget-object v3, p0, Lorg/apache/index/poi/hslf/record/CurrentUserAtom;->_contents:[B

    const/16 v6, 0xe

    aget-byte v3, v3, v6

    sget-object v6, Lorg/apache/index/poi/hslf/record/CurrentUserAtom;->encHeaderToken:[B

    const/4 v7, 0x2

    aget-byte v6, v6, v7

    if-ne v3, v6, :cond_0

    .line 173
    iget-object v3, p0, Lorg/apache/index/poi/hslf/record/CurrentUserAtom;->_contents:[B

    const/16 v6, 0xf

    aget-byte v3, v3, v6

    sget-object v6, Lorg/apache/index/poi/hslf/record/CurrentUserAtom;->encHeaderToken:[B

    const/4 v7, 0x3

    aget-byte v6, v6, v7

    if-ne v3, v6, :cond_0

    .line 174
    new-instance v3, Lorg/apache/index/poi/hslf/exceptions/EncryptedPowerPointFileException;

    const-string/jumbo v6, "The CurrentUserAtom specifies that the document is encrypted"

    invoke-direct {v3, v6}, Lorg/apache/index/poi/hslf/exceptions/EncryptedPowerPointFileException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 178
    :cond_0
    iget-object v3, p0, Lorg/apache/index/poi/hslf/record/CurrentUserAtom;->_contents:[B

    const/16 v6, 0x10

    invoke-static {v3, v6}, Lorg/apache/index/poi/util/LittleEndian;->getUInt([BI)J

    move-result-wide v6

    iput-wide v6, p0, Lorg/apache/index/poi/hslf/record/CurrentUserAtom;->currentEditOffset:J

    .line 181
    iget-object v3, p0, Lorg/apache/index/poi/hslf/record/CurrentUserAtom;->_contents:[B

    const/16 v6, 0x16

    invoke-static {v3, v6}, Lorg/apache/index/poi/util/LittleEndian;->getUShort([BI)I

    move-result v3

    iput v3, p0, Lorg/apache/index/poi/hslf/record/CurrentUserAtom;->docFinalVersion:I

    .line 182
    iget-object v3, p0, Lorg/apache/index/poi/hslf/record/CurrentUserAtom;->_contents:[B

    const/16 v6, 0x18

    aget-byte v3, v3, v6

    iput-byte v3, p0, Lorg/apache/index/poi/hslf/record/CurrentUserAtom;->docMajorNo:B

    .line 183
    iget-object v3, p0, Lorg/apache/index/poi/hslf/record/CurrentUserAtom;->_contents:[B

    const/16 v6, 0x19

    aget-byte v3, v3, v6

    iput-byte v3, p0, Lorg/apache/index/poi/hslf/record/CurrentUserAtom;->docMinorNo:B

    .line 186
    iget-object v3, p0, Lorg/apache/index/poi/hslf/record/CurrentUserAtom;->_contents:[B

    const/16 v6, 0x14

    invoke-static {v3, v6}, Lorg/apache/index/poi/util/LittleEndian;->getUShort([BI)I

    move-result v3

    int-to-long v4, v3

    .line 187
    .local v4, "usernameLen":J
    const-wide/16 v6, 0x200

    cmp-long v3, v4, v6

    if-lez v3, :cond_1

    .line 189
    sget-object v3, Ljava/lang/System;->err:Ljava/io/PrintStream;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string/jumbo v7, "Warning - invalid username length "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, " found, treating as if there was no username set"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 190
    const-wide/16 v4, 0x0

    .line 195
    :cond_1
    iget-object v3, p0, Lorg/apache/index/poi/hslf/record/CurrentUserAtom;->_contents:[B

    array-length v3, v3

    long-to-int v6, v4

    add-int/lit8 v6, v6, 0x1c

    add-int/lit8 v6, v6, 0x4

    if-lt v3, v6, :cond_2

    .line 196
    iget-object v3, p0, Lorg/apache/index/poi/hslf/record/CurrentUserAtom;->_contents:[B

    long-to-int v6, v4

    add-int/lit8 v6, v6, 0x1c

    invoke-static {v3, v6}, Lorg/apache/index/poi/util/LittleEndian;->getUInt([BI)J

    move-result-wide v6

    iput-wide v6, p0, Lorg/apache/index/poi/hslf/record/CurrentUserAtom;->releaseVersion:J

    .line 203
    :goto_0
    long-to-int v3, v4

    add-int/lit8 v3, v3, 0x1c

    add-int/lit8 v1, v3, 0x4

    .line 204
    .local v1, "start":I
    long-to-int v3, v4

    mul-int/lit8 v0, v3, 0x2

    .line 206
    .local v0, "len":I
    iget-object v3, p0, Lorg/apache/index/poi/hslf/record/CurrentUserAtom;->_contents:[B

    array-length v3, v3

    add-int v6, v1, v0

    if-lt v3, v6, :cond_3

    .line 207
    new-array v2, v0, [B

    .line 208
    .local v2, "textBytes":[B
    iget-object v3, p0, Lorg/apache/index/poi/hslf/record/CurrentUserAtom;->_contents:[B

    invoke-static {v3, v1, v2, v8, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 209
    invoke-static {v2}, Lorg/apache/index/poi/util/StringUtil;->getFromUnicodeLE([B)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lorg/apache/index/poi/hslf/record/CurrentUserAtom;->lastEditUser:Ljava/lang/String;

    .line 216
    :goto_1
    return-void

    .line 199
    .end local v0    # "len":I
    .end local v1    # "start":I
    .end local v2    # "textBytes":[B
    :cond_2
    const-wide/16 v6, 0x0

    iput-wide v6, p0, Lorg/apache/index/poi/hslf/record/CurrentUserAtom;->releaseVersion:J

    goto :goto_0

    .line 212
    .restart local v0    # "len":I
    .restart local v1    # "start":I
    :cond_3
    long-to-int v3, v4

    new-array v2, v3, [B

    .line 213
    .restart local v2    # "textBytes":[B
    iget-object v3, p0, Lorg/apache/index/poi/hslf/record/CurrentUserAtom;->_contents:[B

    const/16 v6, 0x1c

    long-to-int v7, v4

    invoke-static {v3, v6, v2, v8, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 214
    long-to-int v3, v4

    invoke-static {v2, v8, v3}, Lorg/apache/index/poi/util/StringUtil;->getFromCompressedUnicode([BII)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lorg/apache/index/poi/hslf/record/CurrentUserAtom;->lastEditUser:Ljava/lang/String;

    goto :goto_1
.end method


# virtual methods
.method public getCurrentEditOffset()J
    .locals 2

    .prologue
    .line 78
    iget-wide v0, p0, Lorg/apache/index/poi/hslf/record/CurrentUserAtom;->currentEditOffset:J

    return-wide v0
.end method

.method public getDocFinalVersion()I
    .locals 1

    .prologue
    .line 70
    iget v0, p0, Lorg/apache/index/poi/hslf/record/CurrentUserAtom;->docFinalVersion:I

    return v0
.end method

.method public getDocMajorNo()B
    .locals 1

    .prologue
    .line 71
    iget-byte v0, p0, Lorg/apache/index/poi/hslf/record/CurrentUserAtom;->docMajorNo:B

    return v0
.end method

.method public getDocMinorNo()B
    .locals 1

    .prologue
    .line 72
    iget-byte v0, p0, Lorg/apache/index/poi/hslf/record/CurrentUserAtom;->docMinorNo:B

    return v0
.end method

.method public getLastEditUsername()Ljava/lang/String;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/CurrentUserAtom;->lastEditUser:Ljava/lang/String;

    return-object v0
.end method

.method public getReleaseVersion()J
    .locals 2

    .prologue
    .line 74
    iget-wide v0, p0, Lorg/apache/index/poi/hslf/record/CurrentUserAtom;->releaseVersion:J

    return-wide v0
.end method

.method public setCurrentEditOffset(J)V
    .locals 1
    .param p1, "id"    # J

    .prologue
    .line 79
    iput-wide p1, p0, Lorg/apache/index/poi/hslf/record/CurrentUserAtom;->currentEditOffset:J

    return-void
.end method

.method public setLastEditUsername(Ljava/lang/String;)V
    .locals 0
    .param p1, "u"    # Ljava/lang/String;

    .prologue
    .line 82
    iput-object p1, p0, Lorg/apache/index/poi/hslf/record/CurrentUserAtom;->lastEditUser:Ljava/lang/String;

    return-void
.end method

.method public setReleaseVersion(J)V
    .locals 1
    .param p1, "rv"    # J

    .prologue
    .line 75
    iput-wide p1, p0, Lorg/apache/index/poi/hslf/record/CurrentUserAtom;->releaseVersion:J

    return-void
.end method

.method public writeOut(Ljava/io/OutputStream;)V
    .locals 10
    .param p1, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v9, 0x14

    const/4 v7, 0x4

    const/4 v8, 0x0

    .line 228
    iget-object v4, p0, Lorg/apache/index/poi/hslf/record/CurrentUserAtom;->lastEditUser:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    mul-int/lit8 v4, v4, 0x3

    add-int/lit8 v2, v4, 0x20

    .line 229
    .local v2, "size":I
    new-array v4, v2, [B

    iput-object v4, p0, Lorg/apache/index/poi/hslf/record/CurrentUserAtom;->_contents:[B

    .line 232
    sget-object v4, Lorg/apache/index/poi/hslf/record/CurrentUserAtom;->atomHeader:[B

    iget-object v5, p0, Lorg/apache/index/poi/hslf/record/CurrentUserAtom;->_contents:[B

    invoke-static {v4, v8, v5, v8, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 234
    iget-object v4, p0, Lorg/apache/index/poi/hslf/record/CurrentUserAtom;->lastEditUser:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v1, v4, 0x18

    .line 235
    .local v1, "atomSize":I
    iget-object v4, p0, Lorg/apache/index/poi/hslf/record/CurrentUserAtom;->_contents:[B

    invoke-static {v4, v7, v1}, Lorg/apache/index/poi/util/LittleEndian;->putInt([BII)V

    .line 238
    iget-object v4, p0, Lorg/apache/index/poi/hslf/record/CurrentUserAtom;->_contents:[B

    const/16 v5, 0x8

    invoke-static {v4, v5, v9}, Lorg/apache/index/poi/util/LittleEndian;->putInt([BII)V

    .line 241
    sget-object v4, Lorg/apache/index/poi/hslf/record/CurrentUserAtom;->headerToken:[B

    iget-object v5, p0, Lorg/apache/index/poi/hslf/record/CurrentUserAtom;->_contents:[B

    const/16 v6, 0xc

    invoke-static {v4, v8, v5, v6, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 244
    iget-object v4, p0, Lorg/apache/index/poi/hslf/record/CurrentUserAtom;->_contents:[B

    const/16 v5, 0x10

    iget-wide v6, p0, Lorg/apache/index/poi/hslf/record/CurrentUserAtom;->currentEditOffset:J

    long-to-int v6, v6

    invoke-static {v4, v5, v6}, Lorg/apache/index/poi/util/LittleEndian;->putInt([BII)V

    .line 248
    iget-object v4, p0, Lorg/apache/index/poi/hslf/record/CurrentUserAtom;->lastEditUser:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    new-array v0, v4, [B

    .line 249
    .local v0, "asciiUN":[B
    iget-object v4, p0, Lorg/apache/index/poi/hslf/record/CurrentUserAtom;->lastEditUser:Ljava/lang/String;

    invoke-static {v4, v0, v8}, Lorg/apache/index/poi/util/StringUtil;->putCompressedUnicode(Ljava/lang/String;[BI)V

    .line 252
    iget-object v4, p0, Lorg/apache/index/poi/hslf/record/CurrentUserAtom;->_contents:[B

    array-length v5, v0

    int-to-short v5, v5

    invoke-static {v4, v9, v5}, Lorg/apache/index/poi/util/LittleEndian;->putShort([BIS)V

    .line 255
    iget-object v4, p0, Lorg/apache/index/poi/hslf/record/CurrentUserAtom;->_contents:[B

    const/16 v5, 0x16

    iget v6, p0, Lorg/apache/index/poi/hslf/record/CurrentUserAtom;->docFinalVersion:I

    int-to-short v6, v6

    invoke-static {v4, v5, v6}, Lorg/apache/index/poi/util/LittleEndian;->putShort([BIS)V

    .line 256
    iget-object v4, p0, Lorg/apache/index/poi/hslf/record/CurrentUserAtom;->_contents:[B

    const/16 v5, 0x18

    iget-byte v6, p0, Lorg/apache/index/poi/hslf/record/CurrentUserAtom;->docMajorNo:B

    aput-byte v6, v4, v5

    .line 257
    iget-object v4, p0, Lorg/apache/index/poi/hslf/record/CurrentUserAtom;->_contents:[B

    const/16 v5, 0x19

    iget-byte v6, p0, Lorg/apache/index/poi/hslf/record/CurrentUserAtom;->docMinorNo:B

    aput-byte v6, v4, v5

    .line 260
    iget-object v4, p0, Lorg/apache/index/poi/hslf/record/CurrentUserAtom;->_contents:[B

    const/16 v5, 0x1a

    aput-byte v8, v4, v5

    .line 261
    iget-object v4, p0, Lorg/apache/index/poi/hslf/record/CurrentUserAtom;->_contents:[B

    const/16 v5, 0x1b

    aput-byte v8, v4, v5

    .line 264
    iget-object v4, p0, Lorg/apache/index/poi/hslf/record/CurrentUserAtom;->_contents:[B

    const/16 v5, 0x1c

    array-length v6, v0

    invoke-static {v0, v8, v4, v5, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 267
    iget-object v4, p0, Lorg/apache/index/poi/hslf/record/CurrentUserAtom;->_contents:[B

    array-length v5, v0

    add-int/lit8 v5, v5, 0x1c

    iget-wide v6, p0, Lorg/apache/index/poi/hslf/record/CurrentUserAtom;->releaseVersion:J

    long-to-int v6, v6

    invoke-static {v4, v5, v6}, Lorg/apache/index/poi/util/LittleEndian;->putInt([BII)V

    .line 270
    iget-object v4, p0, Lorg/apache/index/poi/hslf/record/CurrentUserAtom;->lastEditUser:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    mul-int/lit8 v4, v4, 0x2

    new-array v3, v4, [B

    .line 271
    .local v3, "ucUN":[B
    iget-object v4, p0, Lorg/apache/index/poi/hslf/record/CurrentUserAtom;->lastEditUser:Ljava/lang/String;

    invoke-static {v4, v3, v8}, Lorg/apache/index/poi/util/StringUtil;->putUnicodeLE(Ljava/lang/String;[BI)V

    .line 272
    iget-object v4, p0, Lorg/apache/index/poi/hslf/record/CurrentUserAtom;->_contents:[B

    array-length v5, v0

    add-int/lit8 v5, v5, 0x1c

    add-int/lit8 v5, v5, 0x4

    array-length v6, v3

    invoke-static {v3, v8, v4, v5, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 275
    iget-object v4, p0, Lorg/apache/index/poi/hslf/record/CurrentUserAtom;->_contents:[B

    invoke-virtual {p1, v4}, Ljava/io/OutputStream;->write([B)V

    .line 276
    return-void
.end method

.method public writeToFS(Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;)V
    .locals 3
    .param p1, "fs"    # Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 283
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 284
    .local v1, "baos":Ljava/io/ByteArrayOutputStream;
    invoke-virtual {p0, v1}, Lorg/apache/index/poi/hslf/record/CurrentUserAtom;->writeOut(Ljava/io/OutputStream;)V

    .line 286
    new-instance v0, Ljava/io/ByteArrayInputStream;

    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 289
    .local v0, "bais":Ljava/io/ByteArrayInputStream;
    const-string/jumbo v2, "Current User"

    invoke-virtual {p1, v0, v2}, Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;->createDocument(Ljava/io/InputStream;Ljava/lang/String;)Lorg/apache/index/poi/poifs/filesystem/DocumentEntry;

    .line 290
    return-void
.end method
