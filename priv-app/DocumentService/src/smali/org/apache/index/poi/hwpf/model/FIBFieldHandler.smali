.class public final Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;
.super Ljava/lang/Object;
.source "FIBFieldHandler.java"


# static fields
.field public static final AUTOSAVESOURCE:I = 0x23

.field public static final BKDEDN:I = 0x44

.field public static final BKDFTN:I = 0x42

.field public static final BKDMOTHER:I = 0x40

.field public static final CLX:I = 0x21

.field public static final CMDS:I = 0x18

.field public static final DGGINFO:I = 0x32

.field public static final DOCUNDO:I = 0x4d

.field public static final DOP:I = 0x1f

.field private static final FIELD_SIZE:I = 0x8

.field public static final FORMFLDSTTBS:I = 0x2d

.field public static final GRPXSTATNOWNERS:I = 0x24

.field public static final MODIFIED:I = 0x57

.field public static final PGDEDN:I = 0x43

.field public static final PGDFTN:I = 0x41

.field public static final PGDMOTHER:I = 0x3f

.field public static final PLCASUMY:I = 0x59

.field public static final PLCDOAHDR:I = 0x27

.field public static final PLCFANDREF:I = 0x4

.field public static final PLCFANDTXT:I = 0x5

.field public static final PLCFATNBKF:I = 0x2a

.field public static final PLCFATNBKL:I = 0x2b

.field public static final PLCFBKF:I = 0x16

.field public static final PLCFBKL:I = 0x17

.field public static final PLCFBTECHPX:I = 0xc

.field public static final PLCFBTELVC:I = 0x56

.field public static final PLCFBTEPAPX:I = 0xd

.field public static final PLCFDOAMOM:I = 0x26

.field public static final PLCFENDREF:I = 0x2e

.field public static final PLCFENDTXT:I = 0x2f

.field public static final PLCFFLDATN:I = 0x13

.field public static final PLCFFLDEDN:I = 0x30

.field public static final PLCFFLDFTN:I = 0x12

.field public static final PLCFFLDHDR:I = 0x11

.field public static final PLCFFLDHDRTXBX:I = 0x3b

.field public static final PLCFFLDMCR:I = 0x14

.field public static final PLCFFLDMOM:I = 0x10

.field public static final PLCFFLDTXBX:I = 0x39

.field public static final PLCFFNDREF:I = 0x2

.field public static final PLCFFNDTXT:I = 0x3

.field public static final PLCFGLSY:I = 0xa

.field public static final PLCFGRAM:I = 0x5a

.field public static final PLCFHDD:I = 0xb

.field public static final PLCFHDRTXBXTXT:I = 0x3a

.field public static final PLCFLST:I = 0x49

.field public static final PLCFLVC:I = 0x58

.field public static final PLCFPAD:I = 0x7

.field public static final PLCFPGDEDN:I = 0x31

.field public static final PLCFPGDFTN:I = 0x22

.field public static final PLCFPHE:I = 0x8

.field public static final PLCFSEA:I = 0xe

.field public static final PLCFSED:I = 0x6

.field public static final PLCFSPL:I = 0x37

.field public static final PLCFTXBXBKD:I = 0x4b

.field public static final PLCFTXBXHDRBKD:I = 0x4c

.field public static final PLCFTXBXTXT:I = 0x38

.field public static final PLCFWKB:I = 0x36

.field public static final PLCMCR:I = 0x19

.field public static final PLCOCX:I = 0x55

.field public static final PLCSPAHDR:I = 0x29

.field public static final PLCSPAMOM:I = 0x28

.field public static final PLCUPCRGBUSE:I = 0x51

.field public static final PLCUPCUSP:I = 0x52

.field public static final PLFLFO:I = 0x4a

.field public static final PLGOSL:I = 0x54

.field public static final PMS:I = 0x2c

.field public static final PRDRVR:I = 0x1b

.field public static final PRENVLAND:I = 0x1d

.field public static final PRENVPORT:I = 0x1c

.field public static final RGBUSE:I = 0x4e

.field public static final ROUTESLIP:I = 0x46

.field public static final STSHF:I = 0x1

.field public static final STSHFORIG:I = 0x0

.field public static final STTBAUTOCAPTION:I = 0x35

.field public static final STTBCAPTION:I = 0x34

.field public static final STTBFASSOC:I = 0x20

.field public static final STTBFATNBKMK:I = 0x25

.field public static final STTBFBKMK:I = 0x15

.field public static final STTBFFFN:I = 0xf

.field public static final STTBFINTFLD:I = 0x45

.field public static final STTBFMCR:I = 0x1a

.field public static final STTBFNM:I = 0x48

.field public static final STTBFRMARK:I = 0x33

.field public static final STTBFUSSR:I = 0x5c

.field public static final STTBGLSY:I = 0x9

.field public static final STTBGLSYSTYLE:I = 0x53

.field public static final STTBLISTNAMES:I = 0x5b

.field public static final STTBSAVEDBY:I = 0x47

.field public static final STTBTTMBD:I = 0x3d

.field public static final STWUSER:I = 0x3c

.field public static final UNUSED:I = 0x3e

.field public static final USKF:I = 0x50

.field public static final USP:I = 0x4f

.field public static final WSS:I = 0x1e

.field private static log:Lorg/apache/index/poi/util/POILogger;


# instance fields
.field private _fields:[I

.field private _unknownMap:Ljava/util/HashMap;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 125
    const-class v0, Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;

    invoke-static {v0}, Lorg/apache/index/poi/util/POILogFactory;->getLogger(Ljava/lang/Class;)Lorg/apache/index/poi/util/POILogger;

    move-result-object v0

    sput-object v0, Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;->log:Lorg/apache/index/poi/util/POILogger;

    .line 127
    return-void
.end method

.method public constructor <init>([BI[BLjava/util/HashSet;Z)V
    .locals 10
    .param p1, "mainStream"    # [B
    .param p2, "offset"    # I
    .param p3, "tableStream"    # [B
    .param p4, "offsetList"    # Ljava/util/HashSet;
    .param p5, "areKnown"    # Z

    .prologue
    .line 133
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 129
    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    iput-object v6, p0, Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;->_unknownMap:Ljava/util/HashMap;

    .line 136
    invoke-static {p1, p2}, Lorg/apache/index/poi/util/LittleEndian;->getShort([BI)S

    move-result v3

    .line 137
    .local v3, "numFields":I
    add-int/lit8 p2, p2, 0x2

    .line 138
    mul-int/lit8 v6, v3, 0x2

    new-array v6, v6, [I

    iput-object v6, p0, Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;->_fields:[I

    .line 140
    const/4 v5, 0x0

    .local v5, "x":I
    :goto_0
    if-lt v5, v3, :cond_0

    .line 168
    return-void

    .line 142
    :cond_0
    mul-int/lit8 v6, v5, 0x8

    add-int v2, v6, p2

    .line 143
    .local v2, "fieldOffset":I
    invoke-static {p1, v2}, Lorg/apache/index/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    .line 144
    .local v0, "dsOffset":I
    add-int/lit8 v2, v2, 0x4

    .line 145
    invoke-static {p1, v2}, Lorg/apache/index/poi/util/LittleEndian;->getInt([BI)I

    move-result v1

    .line 147
    .local v1, "dsSize":I
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {p4, v6}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v6

    xor-int/2addr v6, p5

    if-eqz v6, :cond_1

    .line 149
    if-lez v1, :cond_1

    .line 151
    add-int v6, v0, v1

    array-length v7, p3

    if-le v6, v7, :cond_2

    .line 153
    sget-object v6, Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;->log:Lorg/apache/index/poi/util/POILogger;

    sget v7, Lorg/apache/index/poi/util/POILogger;->WARN:I

    new-instance v8, Ljava/lang/StringBuilder;

    const-string/jumbo v9, "Unhandled data structure points to outside the buffer. offset = "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 154
    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, ", length = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    .line 155
    const-string/jumbo v9, ", buffer length = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    array-length v9, p3

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 153
    invoke-virtual {v6, v7, v8}, Lorg/apache/index/poi/util/POILogger;->log(ILjava/lang/Object;)V

    .line 165
    :cond_1
    :goto_1
    iget-object v6, p0, Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;->_fields:[I

    mul-int/lit8 v7, v5, 0x2

    aput v0, v6, v7

    .line 166
    iget-object v6, p0, Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;->_fields:[I

    mul-int/lit8 v7, v5, 0x2

    add-int/lit8 v7, v7, 0x1

    aput v1, v6, v7

    .line 140
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 159
    :cond_2
    new-instance v4, Lorg/apache/index/poi/hwpf/model/UnhandledDataStructure;

    invoke-direct {v4, p3, v0, v1}, Lorg/apache/index/poi/hwpf/model/UnhandledDataStructure;-><init>([BII)V

    .line 161
    .local v4, "unhandled":Lorg/apache/index/poi/hwpf/model/UnhandledDataStructure;
    iget-object v6, p0, Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;->_unknownMap:Ljava/util/HashMap;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v6, v7, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1
.end method


# virtual methods
.method public clearFields()V
    .locals 2

    .prologue
    .line 172
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;->_fields:[I

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([II)V

    .line 173
    return-void
.end method

.method public getFieldOffset(I)I
    .locals 2
    .param p1, "field"    # I

    .prologue
    .line 177
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;->_fields:[I

    mul-int/lit8 v1, p1, 0x2

    aget v0, v0, v1

    return v0
.end method

.method public getFieldSize(I)I
    .locals 2
    .param p1, "field"    # I

    .prologue
    .line 182
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;->_fields:[I

    mul-int/lit8 v1, p1, 0x2

    add-int/lit8 v1, v1, 0x1

    aget v0, v0, v1

    return v0
.end method

.method public setFieldOffset(II)V
    .locals 2
    .param p1, "field"    # I
    .param p2, "offset"    # I

    .prologue
    .line 187
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;->_fields:[I

    mul-int/lit8 v1, p1, 0x2

    aput p2, v0, v1

    .line 188
    return-void
.end method

.method public setFieldSize(II)V
    .locals 2
    .param p1, "field"    # I
    .param p2, "size"    # I

    .prologue
    .line 192
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;->_fields:[I

    mul-int/lit8 v1, p1, 0x2

    add-int/lit8 v1, v1, 0x1

    aput p2, v0, v1

    .line 193
    return-void
.end method

.method public sizeInBytes()I
    .locals 1

    .prologue
    .line 197
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;->_fields:[I

    array-length v0, v0

    mul-int/lit8 v0, v0, 0x4

    add-int/lit8 v0, v0, 0x2

    return v0
.end method
