.class public Lorg/apache/index/poi/ddf/EscherBoolProperty;
.super Lorg/apache/index/poi/ddf/EscherSimpleProperty;
.source "EscherBoolProperty.java"


# direct methods
.method public constructor <init>(SI)V
    .locals 0
    .param p1, "propertyNumber"    # S
    .param p2, "value"    # I

    .prologue
    .line 41
    invoke-direct {p0, p1, p2}, Lorg/apache/index/poi/ddf/EscherSimpleProperty;-><init>(SI)V

    .line 42
    return-void
.end method


# virtual methods
.method public isFalse()Z
    .locals 1

    .prologue
    .line 57
    iget v0, p0, Lorg/apache/index/poi/ddf/EscherBoolProperty;->propertyValue:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isTrue()Z
    .locals 1

    .prologue
    .line 49
    iget v0, p0, Lorg/apache/index/poi/ddf/EscherBoolProperty;->propertyValue:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
