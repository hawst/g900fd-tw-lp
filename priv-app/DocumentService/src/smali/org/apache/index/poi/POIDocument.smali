.class public abstract Lorg/apache/index/poi/POIDocument;
.super Ljava/lang/Object;
.source "POIDocument.java"


# static fields
.field private static final logger:Lorg/apache/index/poi/util/POILogger;


# instance fields
.field protected directory:Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;

.field private dsInf:Lorg/apache/index/poi/hpsf/DocumentSummaryInformation;

.field private initialized:Z

.field private sInf:Lorg/apache/index/poi/hpsf/SummaryInformation;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 53
    const-class v0, Lorg/apache/index/poi/POIDocument;

    invoke-static {v0}, Lorg/apache/index/poi/util/POILogFactory;->getLogger(Ljava/lang/Class;)Lorg/apache/index/poi/util/POILogger;

    move-result-object v0

    sput-object v0, Lorg/apache/index/poi/POIDocument;->logger:Lorg/apache/index/poi/util/POILogger;

    return-void
.end method

.method protected constructor <init>(Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;)V
    .locals 1
    .param p1, "dir"    # Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/index/poi/POIDocument;->initialized:Z

    .line 60
    iput-object p1, p0, Lorg/apache/index/poi/POIDocument;->directory:Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;

    .line 61
    return-void
.end method

.method protected constructor <init>(Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;)V
    .locals 1
    .param p1, "dir"    # Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;
    .param p2, "fs"    # Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/index/poi/POIDocument;->initialized:Z

    .line 67
    iput-object p1, p0, Lorg/apache/index/poi/POIDocument;->directory:Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;

    .line 68
    return-void
.end method

.method protected constructor <init>(Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;)V
    .locals 1
    .param p1, "fs"    # Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;

    .prologue
    .line 70
    invoke-virtual {p1}, Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;->getRoot()Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/apache/index/poi/POIDocument;-><init>(Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;)V

    .line 71
    return-void
.end method

.method private copyNodeRecursively(Lorg/apache/index/poi/poifs/filesystem/Entry;Lorg/apache/index/poi/poifs/filesystem/DirectoryEntry;)V
    .locals 6
    .param p1, "entry"    # Lorg/apache/index/poi/poifs/filesystem/Entry;
    .param p2, "target"    # Lorg/apache/index/poi/poifs/filesystem/DirectoryEntry;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 205
    const/4 v4, 0x0

    .line 206
    .local v4, "newTarget":Lorg/apache/index/poi/poifs/filesystem/DirectoryEntry;
    invoke-interface {p1}, Lorg/apache/index/poi/poifs/filesystem/Entry;->isDirectoryEntry()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 207
    invoke-interface {p1}, Lorg/apache/index/poi/poifs/filesystem/Entry;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-interface {p2, v5}, Lorg/apache/index/poi/poifs/filesystem/DirectoryEntry;->createDirectory(Ljava/lang/String;)Lorg/apache/index/poi/poifs/filesystem/DirectoryEntry;

    move-result-object v4

    .line 208
    check-cast p1, Lorg/apache/index/poi/poifs/filesystem/DirectoryEntry;

    .end local p1    # "entry":Lorg/apache/index/poi/poifs/filesystem/Entry;
    invoke-interface {p1}, Lorg/apache/index/poi/poifs/filesystem/DirectoryEntry;->getEntries()Ljava/util/Iterator;

    move-result-object v3

    .line 210
    .local v3, "entries":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/index/poi/poifs/filesystem/Entry;>;"
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_1

    .line 225
    .end local v3    # "entries":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/index/poi/poifs/filesystem/Entry;>;"
    :cond_0
    :goto_1
    return-void

    .line 211
    .restart local v3    # "entries":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/index/poi/poifs/filesystem/Entry;>;"
    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/apache/index/poi/poifs/filesystem/Entry;

    invoke-direct {p0, v5, v4}, Lorg/apache/index/poi/POIDocument;->copyNodeRecursively(Lorg/apache/index/poi/poifs/filesystem/Entry;Lorg/apache/index/poi/poifs/filesystem/DirectoryEntry;)V

    goto :goto_0

    .end local v3    # "entries":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/index/poi/poifs/filesystem/Entry;>;"
    .restart local p1    # "entry":Lorg/apache/index/poi/poifs/filesystem/Entry;
    :cond_2
    move-object v0, p1

    .line 214
    check-cast v0, Lorg/apache/index/poi/poifs/filesystem/DocumentEntry;

    .line 215
    .local v0, "dentry":Lorg/apache/index/poi/poifs/filesystem/DocumentEntry;
    const/4 v1, 0x0

    .line 217
    .local v1, "dstream":Lorg/apache/index/poi/poifs/filesystem/DocumentInputStream;
    :try_start_0
    new-instance v2, Lorg/apache/index/poi/poifs/filesystem/DocumentInputStream;

    invoke-direct {v2, v0}, Lorg/apache/index/poi/poifs/filesystem/DocumentInputStream;-><init>(Lorg/apache/index/poi/poifs/filesystem/DocumentEntry;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 218
    .end local v1    # "dstream":Lorg/apache/index/poi/poifs/filesystem/DocumentInputStream;
    .local v2, "dstream":Lorg/apache/index/poi/poifs/filesystem/DocumentInputStream;
    :try_start_1
    invoke-interface {v0}, Lorg/apache/index/poi/poifs/filesystem/DocumentEntry;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-interface {p2, v5, v2}, Lorg/apache/index/poi/poifs/filesystem/DirectoryEntry;->createDocument(Ljava/lang/String;Ljava/io/InputStream;)Lorg/apache/index/poi/poifs/filesystem/DocumentEntry;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 220
    if-eqz v2, :cond_0

    .line 221
    invoke-virtual {v2}, Lorg/apache/index/poi/poifs/filesystem/DocumentInputStream;->close()V

    goto :goto_1

    .line 219
    .end local v2    # "dstream":Lorg/apache/index/poi/poifs/filesystem/DocumentInputStream;
    .restart local v1    # "dstream":Lorg/apache/index/poi/poifs/filesystem/DocumentInputStream;
    :catchall_0
    move-exception v5

    .line 220
    :goto_2
    if-eqz v1, :cond_3

    .line 221
    invoke-virtual {v1}, Lorg/apache/index/poi/poifs/filesystem/DocumentInputStream;->close()V

    .line 223
    :cond_3
    throw v5

    .line 219
    .end local v1    # "dstream":Lorg/apache/index/poi/poifs/filesystem/DocumentInputStream;
    .restart local v2    # "dstream":Lorg/apache/index/poi/poifs/filesystem/DocumentInputStream;
    :catchall_1
    move-exception v5

    move-object v1, v2

    .end local v2    # "dstream":Lorg/apache/index/poi/poifs/filesystem/DocumentInputStream;
    .restart local v1    # "dstream":Lorg/apache/index/poi/poifs/filesystem/DocumentInputStream;
    goto :goto_2
.end method


# virtual methods
.method protected copyNodes(Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;Ljava/util/List;)V
    .locals 5
    .param p1, "source"    # Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;
    .param p2, "target"    # Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;",
            "Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 186
    .local p3, "excepts":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p1}, Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;->getRoot()Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;

    move-result-object v3

    .line 187
    .local v3, "root":Lorg/apache/index/poi/poifs/filesystem/DirectoryEntry;
    invoke-virtual {p2}, Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;->getRoot()Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;

    move-result-object v2

    .line 189
    .local v2, "newRoot":Lorg/apache/index/poi/poifs/filesystem/DirectoryEntry;
    invoke-interface {v3}, Lorg/apache/index/poi/poifs/filesystem/DirectoryEntry;->getEntries()Ljava/util/Iterator;

    move-result-object v0

    .line 190
    .local v0, "entries":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/index/poi/poifs/filesystem/Entry;>;"
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_1

    .line 196
    return-void

    .line 191
    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/index/poi/poifs/filesystem/Entry;

    .line 192
    .local v1, "entry":Lorg/apache/index/poi/poifs/filesystem/Entry;
    invoke-interface {v1}, Lorg/apache/index/poi/poifs/filesystem/Entry;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-interface {p3, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 193
    invoke-direct {p0, v1, v2}, Lorg/apache/index/poi/POIDocument;->copyNodeRecursively(Lorg/apache/index/poi/poifs/filesystem/Entry;Lorg/apache/index/poi/poifs/filesystem/DirectoryEntry;)V

    goto :goto_0
.end method

.method public createInformationProperties()V
    .locals 1

    .prologue
    .line 102
    iget-boolean v0, p0, Lorg/apache/index/poi/POIDocument;->initialized:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/index/poi/POIDocument;->readProperties()V

    .line 103
    :cond_0
    iget-object v0, p0, Lorg/apache/index/poi/POIDocument;->sInf:Lorg/apache/index/poi/hpsf/SummaryInformation;

    if-nez v0, :cond_1

    .line 104
    invoke-static {}, Lorg/apache/index/poi/hpsf/PropertySetFactory;->newSummaryInformation()Lorg/apache/index/poi/hpsf/SummaryInformation;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/index/poi/POIDocument;->sInf:Lorg/apache/index/poi/hpsf/SummaryInformation;

    .line 106
    :cond_1
    iget-object v0, p0, Lorg/apache/index/poi/POIDocument;->dsInf:Lorg/apache/index/poi/hpsf/DocumentSummaryInformation;

    if-nez v0, :cond_2

    .line 107
    invoke-static {}, Lorg/apache/index/poi/hpsf/PropertySetFactory;->newDocumentSummaryInformation()Lorg/apache/index/poi/hpsf/DocumentSummaryInformation;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/index/poi/POIDocument;->dsInf:Lorg/apache/index/poi/hpsf/DocumentSummaryInformation;

    .line 109
    :cond_2
    return-void
.end method

.method public getDocumentSummaryInformation()Lorg/apache/index/poi/hpsf/DocumentSummaryInformation;
    .locals 1

    .prologue
    .line 80
    iget-boolean v0, p0, Lorg/apache/index/poi/POIDocument;->initialized:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/index/poi/POIDocument;->readProperties()V

    .line 81
    :cond_0
    iget-object v0, p0, Lorg/apache/index/poi/POIDocument;->dsInf:Lorg/apache/index/poi/hpsf/DocumentSummaryInformation;

    return-object v0
.end method

.method protected getPropertySet(Ljava/lang/String;)Lorg/apache/index/poi/hpsf/PropertySet;
    .locals 8
    .param p1, "setName"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 146
    iget-object v4, p0, Lorg/apache/index/poi/POIDocument;->directory:Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;

    if-nez v4, :cond_1

    .line 173
    :cond_0
    :goto_0
    return-object v3

    .line 151
    :cond_1
    :try_start_0
    iget-object v4, p0, Lorg/apache/index/poi/POIDocument;->directory:Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;

    iget-object v5, p0, Lorg/apache/index/poi/POIDocument;->directory:Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;

    invoke-virtual {v5, p1}, Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;->getEntry(Ljava/lang/String;)Lorg/apache/index/poi/poifs/filesystem/Entry;

    move-result-object v5

    invoke-virtual {v4, v5}, Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;->createDocumentInputStream(Lorg/apache/index/poi/poifs/filesystem/Entry;)Lorg/apache/index/poi/poifs/filesystem/DocumentInputStream;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 160
    .local v0, "dis":Lorg/apache/index/poi/poifs/filesystem/DocumentInputStream;
    :try_start_1
    invoke-static {v0}, Lorg/apache/index/poi/hpsf/PropertySetFactory;->create(Ljava/io/InputStream;)Lorg/apache/index/poi/hpsf/PropertySet;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lorg/apache/index/poi/hpsf/HPSFException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v3

    .line 169
    .local v3, "set":Lorg/apache/index/poi/hpsf/PropertySet;
    if-eqz v0, :cond_0

    .line 170
    invoke-virtual {v0}, Lorg/apache/index/poi/poifs/filesystem/DocumentInputStream;->close()V

    goto :goto_0

    .line 152
    .end local v0    # "dis":Lorg/apache/index/poi/poifs/filesystem/DocumentInputStream;
    .end local v3    # "set":Lorg/apache/index/poi/hpsf/PropertySet;
    :catch_0
    move-exception v2

    .line 154
    .local v2, "ie":Ljava/io/IOException;
    sget-object v4, Lorg/apache/index/poi/POIDocument;->logger:Lorg/apache/index/poi/util/POILogger;

    sget v5, Lorg/apache/index/poi/util/POILogger;->WARN:I

    new-instance v6, Ljava/lang/StringBuilder;

    const-string/jumbo v7, "Error getting property set with name "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "\n"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Lorg/apache/index/poi/util/POILogger;->log(ILjava/lang/Object;)V

    goto :goto_0

    .line 162
    .end local v2    # "ie":Ljava/io/IOException;
    .restart local v0    # "dis":Lorg/apache/index/poi/poifs/filesystem/DocumentInputStream;
    :catch_1
    move-exception v2

    .line 164
    .restart local v2    # "ie":Ljava/io/IOException;
    :try_start_2
    sget-object v4, Lorg/apache/index/poi/POIDocument;->logger:Lorg/apache/index/poi/util/POILogger;

    sget v5, Lorg/apache/index/poi/util/POILogger;->WARN:I

    new-instance v6, Ljava/lang/StringBuilder;

    const-string/jumbo v7, "Error creating property set with name "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "\n"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Lorg/apache/index/poi/util/POILogger;->log(ILjava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 169
    if-eqz v0, :cond_0

    .line 170
    invoke-virtual {v0}, Lorg/apache/index/poi/poifs/filesystem/DocumentInputStream;->close()V

    goto :goto_0

    .line 165
    .end local v2    # "ie":Ljava/io/IOException;
    :catch_2
    move-exception v1

    .line 167
    .local v1, "he":Lorg/apache/index/poi/hpsf/HPSFException;
    :try_start_3
    sget-object v4, Lorg/apache/index/poi/POIDocument;->logger:Lorg/apache/index/poi/util/POILogger;

    sget v5, Lorg/apache/index/poi/util/POILogger;->WARN:I

    new-instance v6, Ljava/lang/StringBuilder;

    const-string/jumbo v7, "Error creating property set with name "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "\n"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Lorg/apache/index/poi/util/POILogger;->log(ILjava/lang/Object;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 169
    if-eqz v0, :cond_0

    .line 170
    invoke-virtual {v0}, Lorg/apache/index/poi/poifs/filesystem/DocumentInputStream;->close()V

    goto/16 :goto_0

    .line 168
    .end local v1    # "he":Lorg/apache/index/poi/hpsf/HPSFException;
    :catchall_0
    move-exception v4

    .line 169
    if-eqz v0, :cond_2

    .line 170
    invoke-virtual {v0}, Lorg/apache/index/poi/poifs/filesystem/DocumentInputStream;->close()V

    .line 172
    :cond_2
    throw v4
.end method

.method public getSummaryInformation()Lorg/apache/index/poi/hpsf/SummaryInformation;
    .locals 1

    .prologue
    .line 88
    iget-boolean v0, p0, Lorg/apache/index/poi/POIDocument;->initialized:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/index/poi/POIDocument;->readProperties()V

    .line 89
    :cond_0
    iget-object v0, p0, Lorg/apache/index/poi/POIDocument;->sInf:Lorg/apache/index/poi/hpsf/SummaryInformation;

    return-object v0
.end method

.method protected readProperties()V
    .locals 5

    .prologue
    .line 121
    const-string/jumbo v1, "\u0005DocumentSummaryInformation"

    invoke-virtual {p0, v1}, Lorg/apache/index/poi/POIDocument;->getPropertySet(Ljava/lang/String;)Lorg/apache/index/poi/hpsf/PropertySet;

    move-result-object v0

    .line 122
    .local v0, "ps":Lorg/apache/index/poi/hpsf/PropertySet;
    if-eqz v0, :cond_2

    instance-of v1, v0, Lorg/apache/index/poi/hpsf/DocumentSummaryInformation;

    if-eqz v1, :cond_2

    .line 123
    check-cast v0, Lorg/apache/index/poi/hpsf/DocumentSummaryInformation;

    .end local v0    # "ps":Lorg/apache/index/poi/hpsf/PropertySet;
    iput-object v0, p0, Lorg/apache/index/poi/POIDocument;->dsInf:Lorg/apache/index/poi/hpsf/DocumentSummaryInformation;

    .line 129
    :cond_0
    :goto_0
    const-string/jumbo v1, "\u0005SummaryInformation"

    invoke-virtual {p0, v1}, Lorg/apache/index/poi/POIDocument;->getPropertySet(Ljava/lang/String;)Lorg/apache/index/poi/hpsf/PropertySet;

    move-result-object v0

    .line 130
    .restart local v0    # "ps":Lorg/apache/index/poi/hpsf/PropertySet;
    instance-of v1, v0, Lorg/apache/index/poi/hpsf/SummaryInformation;

    if-eqz v1, :cond_3

    .line 131
    check-cast v0, Lorg/apache/index/poi/hpsf/SummaryInformation;

    .end local v0    # "ps":Lorg/apache/index/poi/hpsf/PropertySet;
    iput-object v0, p0, Lorg/apache/index/poi/POIDocument;->sInf:Lorg/apache/index/poi/hpsf/SummaryInformation;

    .line 137
    :cond_1
    :goto_1
    const/4 v1, 0x1

    iput-boolean v1, p0, Lorg/apache/index/poi/POIDocument;->initialized:Z

    .line 138
    return-void

    .line 124
    .restart local v0    # "ps":Lorg/apache/index/poi/hpsf/PropertySet;
    :cond_2
    if-eqz v0, :cond_0

    .line 125
    sget-object v1, Lorg/apache/index/poi/POIDocument;->logger:Lorg/apache/index/poi/util/POILogger;

    sget v2, Lorg/apache/index/poi/util/POILogger;->WARN:I

    const-string/jumbo v3, "DocumentSummaryInformation property set came back with wrong class - "

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v4}, Lorg/apache/index/poi/util/POILogger;->log(ILjava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    .line 132
    :cond_3
    if-eqz v0, :cond_1

    .line 133
    sget-object v1, Lorg/apache/index/poi/POIDocument;->logger:Lorg/apache/index/poi/util/POILogger;

    sget v2, Lorg/apache/index/poi/util/POILogger;->WARN:I

    const-string/jumbo v3, "SummaryInformation property set came back with wrong class - "

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v4}, Lorg/apache/index/poi/util/POILogger;->log(ILjava/lang/Object;Ljava/lang/Object;)V

    goto :goto_1
.end method
