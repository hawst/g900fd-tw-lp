.class public final Lorg/apache/index/poi/hwpf/model/Ffn;
.super Ljava/lang/Object;
.source "Ffn.java"


# instance fields
.field private _cbFfnM1:I

.field private _chs:B

.field private _fontSig:[B

.field private _info:B

.field private _ixchSzAlt:B

.field private _panose:[B

.field private _wWeight:S

.field private _xszFfn:[C

.field private _xszFfnLength:I


# direct methods
.method public constructor <init>([BI)V
    .locals 5
    .param p1, "buf"    # [B
    .param p2, "offset"    # I

    .prologue
    const/4 v4, 0x0

    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    const/16 v2, 0xa

    new-array v2, v2, [B

    iput-object v2, p0, Lorg/apache/index/poi/hwpf/model/Ffn;->_panose:[B

    .line 48
    const/16 v2, 0x18

    new-array v2, v2, [B

    iput-object v2, p0, Lorg/apache/index/poi/hwpf/model/Ffn;->_fontSig:[B

    .line 59
    move v1, p2

    .line 61
    .local v1, "offsetTmp":I
    invoke-static {p1, p2}, Lorg/apache/index/poi/util/LittleEndian;->getUnsignedByte([BI)I

    move-result v2

    iput v2, p0, Lorg/apache/index/poi/hwpf/model/Ffn;->_cbFfnM1:I

    .line 62
    add-int/lit8 p2, p2, 0x1

    .line 63
    aget-byte v2, p1, p2

    iput-byte v2, p0, Lorg/apache/index/poi/hwpf/model/Ffn;->_info:B

    .line 64
    add-int/lit8 p2, p2, 0x1

    .line 65
    invoke-static {p1, p2}, Lorg/apache/index/poi/util/LittleEndian;->getShort([BI)S

    move-result v2

    iput-short v2, p0, Lorg/apache/index/poi/hwpf/model/Ffn;->_wWeight:S

    .line 66
    add-int/lit8 p2, p2, 0x2

    .line 67
    aget-byte v2, p1, p2

    iput-byte v2, p0, Lorg/apache/index/poi/hwpf/model/Ffn;->_chs:B

    .line 68
    add-int/lit8 p2, p2, 0x1

    .line 69
    aget-byte v2, p1, p2

    iput-byte v2, p0, Lorg/apache/index/poi/hwpf/model/Ffn;->_ixchSzAlt:B

    .line 70
    add-int/lit8 p2, p2, 0x1

    .line 73
    iget-object v2, p0, Lorg/apache/index/poi/hwpf/model/Ffn;->_panose:[B

    iget-object v3, p0, Lorg/apache/index/poi/hwpf/model/Ffn;->_panose:[B

    array-length v3, v3

    invoke-static {p1, p2, v2, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 74
    iget-object v2, p0, Lorg/apache/index/poi/hwpf/model/Ffn;->_panose:[B

    array-length v2, v2

    add-int/2addr p2, v2

    .line 75
    iget-object v2, p0, Lorg/apache/index/poi/hwpf/model/Ffn;->_fontSig:[B

    iget-object v3, p0, Lorg/apache/index/poi/hwpf/model/Ffn;->_fontSig:[B

    array-length v3, v3

    invoke-static {p1, p2, v2, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 76
    iget-object v2, p0, Lorg/apache/index/poi/hwpf/model/Ffn;->_fontSig:[B

    array-length v2, v2

    add-int/2addr p2, v2

    .line 78
    sub-int v1, p2, v1

    .line 79
    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/model/Ffn;->getSize()I

    move-result v2

    sub-int/2addr v2, v1

    div-int/lit8 v2, v2, 0x2

    iput v2, p0, Lorg/apache/index/poi/hwpf/model/Ffn;->_xszFfnLength:I

    .line 80
    iget v2, p0, Lorg/apache/index/poi/hwpf/model/Ffn;->_xszFfnLength:I

    new-array v2, v2, [C

    iput-object v2, p0, Lorg/apache/index/poi/hwpf/model/Ffn;->_xszFfn:[C

    .line 82
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v2, p0, Lorg/apache/index/poi/hwpf/model/Ffn;->_xszFfnLength:I

    if-lt v0, v2, :cond_0

    .line 89
    return-void

    .line 84
    :cond_0
    iget-object v2, p0, Lorg/apache/index/poi/hwpf/model/Ffn;->_xszFfn:[C

    invoke-static {p1, p2}, Lorg/apache/index/poi/util/LittleEndian;->getShort([BI)S

    move-result v3

    int-to-char v3, v3

    aput-char v3, v2, v0

    .line 85
    add-int/lit8 p2, p2, 0x2

    .line 82
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 187
    const/4 v0, 0x1

    .line 190
    .local v0, "retVal":Z
    if-eqz p1, :cond_7

    instance-of v1, p1, Lorg/apache/index/poi/hwpf/model/Ffn;

    if-eqz v1, :cond_7

    move-object v1, p1

    check-cast v1, Lorg/apache/index/poi/hwpf/model/Ffn;

    invoke-virtual {v1}, Lorg/apache/index/poi/hwpf/model/Ffn;->get_cbFfnM1()I

    move-result v1

    iget v2, p0, Lorg/apache/index/poi/hwpf/model/Ffn;->_cbFfnM1:I

    if-ne v1, v2, :cond_7

    move-object v1, p1

    .line 192
    check-cast v1, Lorg/apache/index/poi/hwpf/model/Ffn;

    iget-byte v1, v1, Lorg/apache/index/poi/hwpf/model/Ffn;->_info:B

    iget-byte v2, p0, Lorg/apache/index/poi/hwpf/model/Ffn;->_info:B

    if-ne v1, v2, :cond_6

    move-object v1, p1

    .line 194
    check-cast v1, Lorg/apache/index/poi/hwpf/model/Ffn;

    iget-short v1, v1, Lorg/apache/index/poi/hwpf/model/Ffn;->_wWeight:S

    iget-short v2, p0, Lorg/apache/index/poi/hwpf/model/Ffn;->_wWeight:S

    if-ne v1, v2, :cond_5

    move-object v1, p1

    .line 196
    check-cast v1, Lorg/apache/index/poi/hwpf/model/Ffn;

    iget-byte v1, v1, Lorg/apache/index/poi/hwpf/model/Ffn;->_chs:B

    iget-byte v2, p0, Lorg/apache/index/poi/hwpf/model/Ffn;->_chs:B

    if-ne v1, v2, :cond_4

    move-object v1, p1

    .line 198
    check-cast v1, Lorg/apache/index/poi/hwpf/model/Ffn;

    iget-byte v1, v1, Lorg/apache/index/poi/hwpf/model/Ffn;->_ixchSzAlt:B

    iget-byte v2, p0, Lorg/apache/index/poi/hwpf/model/Ffn;->_ixchSzAlt:B

    if-ne v1, v2, :cond_3

    move-object v1, p1

    .line 200
    check-cast v1, Lorg/apache/index/poi/hwpf/model/Ffn;

    iget-object v1, v1, Lorg/apache/index/poi/hwpf/model/Ffn;->_panose:[B

    iget-object v2, p0, Lorg/apache/index/poi/hwpf/model/Ffn;->_panose:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-eqz v1, :cond_2

    move-object v1, p1

    .line 202
    check-cast v1, Lorg/apache/index/poi/hwpf/model/Ffn;

    iget-object v1, v1, Lorg/apache/index/poi/hwpf/model/Ffn;->_fontSig:[B

    iget-object v2, p0, Lorg/apache/index/poi/hwpf/model/Ffn;->_fontSig:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 204
    check-cast p1, Lorg/apache/index/poi/hwpf/model/Ffn;

    .end local p1    # "o":Ljava/lang/Object;
    iget-object v1, p1, Lorg/apache/index/poi/hwpf/model/Ffn;->_xszFfn:[C

    iget-object v2, p0, Lorg/apache/index/poi/hwpf/model/Ffn;->_xszFfn:[C

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([C[C)Z

    move-result v1

    if-nez v1, :cond_0

    .line 205
    const/4 v0, 0x0

    .line 228
    :cond_0
    :goto_0
    return v0

    .line 208
    .restart local p1    # "o":Ljava/lang/Object;
    :cond_1
    const/4 v0, 0x0

    .line 209
    goto :goto_0

    .line 211
    :cond_2
    const/4 v0, 0x0

    .line 212
    goto :goto_0

    .line 214
    :cond_3
    const/4 v0, 0x0

    .line 215
    goto :goto_0

    .line 217
    :cond_4
    const/4 v0, 0x0

    .line 218
    goto :goto_0

    .line 220
    :cond_5
    const/4 v0, 0x0

    .line 221
    goto :goto_0

    .line 223
    :cond_6
    const/4 v0, 0x0

    .line 224
    goto :goto_0

    .line 226
    :cond_7
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getAltFontName()Ljava/lang/String;
    .locals 4

    .prologue
    .line 136
    iget-byte v0, p0, Lorg/apache/index/poi/hwpf/model/Ffn;->_ixchSzAlt:B

    .line 137
    .local v0, "index":I
    :goto_0
    iget v1, p0, Lorg/apache/index/poi/hwpf/model/Ffn;->_xszFfnLength:I

    if-lt v0, v1, :cond_1

    .line 144
    :cond_0
    new-instance v1, Ljava/lang/String;

    iget-object v2, p0, Lorg/apache/index/poi/hwpf/model/Ffn;->_xszFfn:[C

    iget-byte v3, p0, Lorg/apache/index/poi/hwpf/model/Ffn;->_ixchSzAlt:B

    invoke-direct {v1, v2, v3, v0}, Ljava/lang/String;-><init>([CII)V

    return-object v1

    .line 139
    :cond_1
    iget-object v1, p0, Lorg/apache/index/poi/hwpf/model/Ffn;->_xszFfn:[C

    aget-char v1, v1, v0

    if-eqz v1, :cond_0

    .line 137
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public getChs()B
    .locals 1

    .prologue
    .line 103
    iget-byte v0, p0, Lorg/apache/index/poi/hwpf/model/Ffn;->_chs:B

    return v0
.end method

.method public getFontSig()[B
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/Ffn;->_fontSig:[B

    return-object v0
.end method

.method public getMainFontName()Ljava/lang/String;
    .locals 4

    .prologue
    .line 123
    const/4 v0, 0x0

    .line 124
    .local v0, "index":I
    :goto_0
    iget v1, p0, Lorg/apache/index/poi/hwpf/model/Ffn;->_xszFfnLength:I

    if-lt v0, v1, :cond_1

    .line 131
    :cond_0
    new-instance v1, Ljava/lang/String;

    iget-object v2, p0, Lorg/apache/index/poi/hwpf/model/Ffn;->_xszFfn:[C

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3, v0}, Ljava/lang/String;-><init>([CII)V

    return-object v1

    .line 126
    :cond_1
    iget-object v1, p0, Lorg/apache/index/poi/hwpf/model/Ffn;->_xszFfn:[C

    aget-char v1, v1, v0

    if-eqz v1, :cond_0

    .line 124
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public getPanose()[B
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/Ffn;->_panose:[B

    return-object v0
.end method

.method public getSize()I
    .locals 1

    .prologue
    .line 118
    iget v0, p0, Lorg/apache/index/poi/hwpf/model/Ffn;->_cbFfnM1:I

    add-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public getWeight()S
    .locals 1

    .prologue
    .line 98
    iget-short v0, p0, Lorg/apache/index/poi/hwpf/model/Ffn;->_wWeight:S

    return v0
.end method

.method public get_cbFfnM1()I
    .locals 1

    .prologue
    .line 93
    iget v0, p0, Lorg/apache/index/poi/hwpf/model/Ffn;->_cbFfnM1:I

    return v0
.end method

.method public set_cbFfnM1(I)V
    .locals 0
    .param p1, "_cbFfnM1"    # I

    .prologue
    .line 150
    iput p1, p0, Lorg/apache/index/poi/hwpf/model/Ffn;->_cbFfnM1:I

    .line 151
    return-void
.end method

.method public toByteArray()[B
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 156
    const/4 v2, 0x0

    .line 157
    .local v2, "offset":I
    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/model/Ffn;->getSize()I

    move-result v3

    new-array v0, v3, [B

    .line 159
    .local v0, "buf":[B
    iget v3, p0, Lorg/apache/index/poi/hwpf/model/Ffn;->_cbFfnM1:I

    int-to-byte v3, v3

    aput-byte v3, v0, v2

    .line 160
    add-int/lit8 v2, v2, 0x1

    .line 161
    iget-byte v3, p0, Lorg/apache/index/poi/hwpf/model/Ffn;->_info:B

    aput-byte v3, v0, v2

    .line 162
    add-int/lit8 v2, v2, 0x1

    .line 163
    iget-short v3, p0, Lorg/apache/index/poi/hwpf/model/Ffn;->_wWeight:S

    invoke-static {v0, v2, v3}, Lorg/apache/index/poi/util/LittleEndian;->putShort([BIS)V

    .line 164
    add-int/lit8 v2, v2, 0x2

    .line 165
    iget-byte v3, p0, Lorg/apache/index/poi/hwpf/model/Ffn;->_chs:B

    aput-byte v3, v0, v2

    .line 166
    add-int/lit8 v2, v2, 0x1

    .line 167
    iget-byte v3, p0, Lorg/apache/index/poi/hwpf/model/Ffn;->_ixchSzAlt:B

    aput-byte v3, v0, v2

    .line 168
    add-int/lit8 v2, v2, 0x1

    .line 170
    iget-object v3, p0, Lorg/apache/index/poi/hwpf/model/Ffn;->_panose:[B

    iget-object v4, p0, Lorg/apache/index/poi/hwpf/model/Ffn;->_panose:[B

    array-length v4, v4

    invoke-static {v3, v5, v0, v2, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 171
    iget-object v3, p0, Lorg/apache/index/poi/hwpf/model/Ffn;->_panose:[B

    array-length v3, v3

    add-int/lit8 v2, v3, 0x6

    .line 172
    iget-object v3, p0, Lorg/apache/index/poi/hwpf/model/Ffn;->_fontSig:[B

    iget-object v4, p0, Lorg/apache/index/poi/hwpf/model/Ffn;->_fontSig:[B

    array-length v4, v4

    invoke-static {v3, v5, v0, v2, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 173
    iget-object v3, p0, Lorg/apache/index/poi/hwpf/model/Ffn;->_fontSig:[B

    array-length v3, v3

    add-int/2addr v2, v3

    .line 175
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lorg/apache/index/poi/hwpf/model/Ffn;->_xszFfn:[C

    array-length v3, v3

    if-lt v1, v3, :cond_0

    .line 181
    return-object v0

    .line 177
    :cond_0
    iget-object v3, p0, Lorg/apache/index/poi/hwpf/model/Ffn;->_xszFfn:[C

    aget-char v3, v3, v1

    int-to-short v3, v3

    invoke-static {v0, v2, v3}, Lorg/apache/index/poi/util/LittleEndian;->putShort([BIS)V

    .line 178
    add-int/lit8 v2, v2, 0x2

    .line 175
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method
