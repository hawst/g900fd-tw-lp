.class public final Lorg/apache/index/poi/hwpf/model/ComplexFileTable;
.super Ljava/lang/Object;
.source "ComplexFileTable.java"


# static fields
.field private static final GRPPRL_TYPE:B = 0x1t

.field private static final TEXT_PIECE_TABLE_TYPE:B = 0x2t


# instance fields
.field protected _tpt:Lorg/apache/index/poi/hwpf/model/TextPieceTable;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    new-instance v0, Lorg/apache/index/poi/hwpf/model/TextPieceTable;

    invoke-direct {v0}, Lorg/apache/index/poi/hwpf/model/TextPieceTable;-><init>()V

    iput-object v0, p0, Lorg/apache/index/poi/hwpf/model/ComplexFileTable;->_tpt:Lorg/apache/index/poi/hwpf/model/TextPieceTable;

    .line 39
    return-void
.end method

.method public constructor <init>(Lcom/samsung/index/ITextContentObs;Lorg/apache/index/poi/poifs/filesystem/DocumentInputStream;[B[BII)V
    .locals 9
    .param p1, "indexwriterIn"    # Lcom/samsung/index/ITextContentObs;
    .param p2, "documentFileStream"    # Lorg/apache/index/poi/poifs/filesystem/DocumentInputStream;
    .param p3, "documentStream"    # [B
    .param p4, "tableStream"    # [B
    .param p5, "offset"    # I
    .param p6, "fcMin"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    :goto_0
    aget-byte v0, p4, p5

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    .line 57
    aget-byte v0, p4, p5

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    .line 59
    new-instance v0, Ljava/io/IOException;

    const-string/jumbo v1, "The text piece table is corrupted"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 53
    :cond_0
    add-int/lit8 p5, p5, 0x1

    .line 54
    invoke-static {p4, p5}, Lorg/apache/index/poi/util/LittleEndian;->getShort([BI)S

    move-result v8

    .line 55
    .local v8, "skip":I
    add-int/lit8 v0, v8, 0x2

    add-int/2addr p5, v0

    goto :goto_0

    .line 61
    .end local v8    # "skip":I
    :cond_1
    add-int/lit8 p5, p5, 0x1

    invoke-static {p4, p5}, Lorg/apache/index/poi/util/LittleEndian;->getInt([BI)I

    move-result v6

    .line 62
    .local v6, "pieceTableSize":I
    add-int/lit8 p5, p5, 0x4

    .line 63
    new-instance v0, Lorg/apache/index/poi/hwpf/model/TextPieceTable;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    move v7, p6

    invoke-direct/range {v0 .. v7}, Lorg/apache/index/poi/hwpf/model/TextPieceTable;-><init>(Lcom/samsung/index/ITextContentObs;Lorg/apache/index/poi/poifs/filesystem/DocumentInputStream;[B[BIII)V

    iput-object v0, p0, Lorg/apache/index/poi/hwpf/model/ComplexFileTable;->_tpt:Lorg/apache/index/poi/hwpf/model/TextPieceTable;

    .line 65
    return-void
.end method

.method public constructor <init>(Lorg/apache/index/poi/poifs/filesystem/DocumentInputStream;[B[BII)V
    .locals 7
    .param p1, "documentFileStream"    # Lorg/apache/index/poi/poifs/filesystem/DocumentInputStream;
    .param p2, "documentStream"    # [B
    .param p3, "tableStream"    # [B
    .param p4, "offset"    # I
    .param p5, "fcMin"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 43
    const/4 v1, 0x0

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    move v6, p5

    invoke-direct/range {v0 .. v6}, Lorg/apache/index/poi/hwpf/model/ComplexFileTable;-><init>(Lcom/samsung/index/ITextContentObs;Lorg/apache/index/poi/poifs/filesystem/DocumentInputStream;[B[BII)V

    .line 44
    return-void
.end method


# virtual methods
.method public getTextPieceTable()Lorg/apache/index/poi/hwpf/model/TextPieceTable;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/ComplexFileTable;->_tpt:Lorg/apache/index/poi/hwpf/model/TextPieceTable;

    return-object v0
.end method
