.class public final Lorg/apache/index/poi/hwpf/sprm/SprmBuffer;
.super Ljava/lang/Object;
.source "SprmBuffer.java"

# interfaces
.implements Ljava/lang/Cloneable;


# instance fields
.field _buf:[B

.field _istd:Z

.field _offset:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    const/4 v0, 0x4

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/apache/index/poi/hwpf/sprm/SprmBuffer;->_buf:[B

    .line 45
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/index/poi/hwpf/sprm/SprmBuffer;->_offset:I

    .line 46
    return-void
.end method

.method public constructor <init>([B)V
    .locals 1
    .param p1, "buf"    # [B

    .prologue
    .line 40
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lorg/apache/index/poi/hwpf/sprm/SprmBuffer;-><init>([BZ)V

    .line 41
    return-void
.end method

.method public constructor <init>([BZ)V
    .locals 1
    .param p1, "buf"    # [B
    .param p2, "istd"    # Z

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    array-length v0, p1

    iput v0, p0, Lorg/apache/index/poi/hwpf/sprm/SprmBuffer;->_offset:I

    .line 35
    iput-object p1, p0, Lorg/apache/index/poi/hwpf/sprm/SprmBuffer;->_buf:[B

    .line 36
    iput-boolean p2, p0, Lorg/apache/index/poi/hwpf/sprm/SprmBuffer;->_istd:Z

    .line 37
    return-void
.end method

.method private ensureCapacity(I)V
    .locals 4
    .param p1, "addition"    # I

    .prologue
    const/4 v3, 0x0

    .line 167
    iget v1, p0, Lorg/apache/index/poi/hwpf/sprm/SprmBuffer;->_offset:I

    add-int/2addr v1, p1

    iget-object v2, p0, Lorg/apache/index/poi/hwpf/sprm/SprmBuffer;->_buf:[B

    array-length v2, v2

    if-lt v1, v2, :cond_0

    .line 170
    iget v1, p0, Lorg/apache/index/poi/hwpf/sprm/SprmBuffer;->_offset:I

    add-int/2addr v1, p1

    add-int/lit8 v1, v1, 0x6

    new-array v0, v1, [B

    .line 171
    .local v0, "newBuf":[B
    iget-object v1, p0, Lorg/apache/index/poi/hwpf/sprm/SprmBuffer;->_buf:[B

    iget-object v2, p0, Lorg/apache/index/poi/hwpf/sprm/SprmBuffer;->_buf:[B

    array-length v2, v2

    invoke-static {v1, v3, v0, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 172
    iput-object v0, p0, Lorg/apache/index/poi/hwpf/sprm/SprmBuffer;->_buf:[B

    .line 174
    .end local v0    # "newBuf":[B
    :cond_0
    return-void
.end method

.method private findSprm(S)I
    .locals 6
    .param p1, "opcode"    # S

    .prologue
    .line 50
    invoke-static {p1}, Lorg/apache/index/poi/hwpf/sprm/SprmOperation;->getOperationFromOpcode(S)I

    move-result v1

    .line 51
    .local v1, "operation":I
    invoke-static {p1}, Lorg/apache/index/poi/hwpf/sprm/SprmOperation;->getTypeFromOpcode(S)I

    move-result v3

    .line 53
    .local v3, "type":I
    new-instance v2, Lorg/apache/index/poi/hwpf/sprm/SprmIterator;

    iget-object v4, p0, Lorg/apache/index/poi/hwpf/sprm/SprmBuffer;->_buf:[B

    const/4 v5, 0x2

    invoke-direct {v2, v4, v5}, Lorg/apache/index/poi/hwpf/sprm/SprmIterator;-><init>([BI)V

    .line 54
    .local v2, "si":Lorg/apache/index/poi/hwpf/sprm/SprmIterator;
    :cond_0
    invoke-virtual {v2}, Lorg/apache/index/poi/hwpf/sprm/SprmIterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_1

    .line 60
    const/4 v4, -0x1

    :goto_0
    return v4

    .line 56
    :cond_1
    invoke-virtual {v2}, Lorg/apache/index/poi/hwpf/sprm/SprmIterator;->next()Lorg/apache/index/poi/hwpf/sprm/SprmOperation;

    move-result-object v0

    .line 57
    .local v0, "i":Lorg/apache/index/poi/hwpf/sprm/SprmOperation;
    invoke-virtual {v0}, Lorg/apache/index/poi/hwpf/sprm/SprmOperation;->getOperation()I

    move-result v4

    if-ne v4, v1, :cond_0

    invoke-virtual {v0}, Lorg/apache/index/poi/hwpf/sprm/SprmOperation;->getType()I

    move-result v4

    if-ne v4, v3, :cond_0

    .line 58
    invoke-virtual {v0}, Lorg/apache/index/poi/hwpf/sprm/SprmOperation;->getGrpprlOffset()I

    move-result v4

    goto :goto_0
.end method


# virtual methods
.method public addSprm(SB)V
    .locals 4
    .param p1, "opcode"    # S
    .param p2, "operand"    # B

    .prologue
    .line 98
    const/4 v0, 0x3

    .line 99
    .local v0, "addition":I
    invoke-direct {p0, v0}, Lorg/apache/index/poi/hwpf/sprm/SprmBuffer;->ensureCapacity(I)V

    .line 100
    iget-object v1, p0, Lorg/apache/index/poi/hwpf/sprm/SprmBuffer;->_buf:[B

    iget v2, p0, Lorg/apache/index/poi/hwpf/sprm/SprmBuffer;->_offset:I

    invoke-static {v1, v2, p1}, Lorg/apache/index/poi/util/LittleEndian;->putShort([BIS)V

    .line 101
    iget v1, p0, Lorg/apache/index/poi/hwpf/sprm/SprmBuffer;->_offset:I

    add-int/lit8 v1, v1, 0x2

    iput v1, p0, Lorg/apache/index/poi/hwpf/sprm/SprmBuffer;->_offset:I

    .line 102
    iget-object v1, p0, Lorg/apache/index/poi/hwpf/sprm/SprmBuffer;->_buf:[B

    iget v2, p0, Lorg/apache/index/poi/hwpf/sprm/SprmBuffer;->_offset:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lorg/apache/index/poi/hwpf/sprm/SprmBuffer;->_offset:I

    aput-byte p2, v1, v2

    .line 103
    return-void
.end method

.method public addSprm(SI)V
    .locals 3
    .param p1, "opcode"    # S
    .param p2, "operand"    # I

    .prologue
    .line 115
    const/4 v0, 0x6

    .line 116
    .local v0, "addition":I
    invoke-direct {p0, v0}, Lorg/apache/index/poi/hwpf/sprm/SprmBuffer;->ensureCapacity(I)V

    .line 117
    iget-object v1, p0, Lorg/apache/index/poi/hwpf/sprm/SprmBuffer;->_buf:[B

    iget v2, p0, Lorg/apache/index/poi/hwpf/sprm/SprmBuffer;->_offset:I

    invoke-static {v1, v2, p1}, Lorg/apache/index/poi/util/LittleEndian;->putShort([BIS)V

    .line 118
    iget v1, p0, Lorg/apache/index/poi/hwpf/sprm/SprmBuffer;->_offset:I

    add-int/lit8 v1, v1, 0x2

    iput v1, p0, Lorg/apache/index/poi/hwpf/sprm/SprmBuffer;->_offset:I

    .line 119
    iget-object v1, p0, Lorg/apache/index/poi/hwpf/sprm/SprmBuffer;->_buf:[B

    iget v2, p0, Lorg/apache/index/poi/hwpf/sprm/SprmBuffer;->_offset:I

    invoke-static {v1, v2, p2}, Lorg/apache/index/poi/util/LittleEndian;->putInt([BII)V

    .line 120
    iget v1, p0, Lorg/apache/index/poi/hwpf/sprm/SprmBuffer;->_offset:I

    add-int/lit8 v1, v1, 0x4

    iput v1, p0, Lorg/apache/index/poi/hwpf/sprm/SprmBuffer;->_offset:I

    .line 121
    return-void
.end method

.method public addSprm(SS)V
    .locals 3
    .param p1, "opcode"    # S
    .param p2, "operand"    # S

    .prologue
    .line 106
    const/4 v0, 0x4

    .line 107
    .local v0, "addition":I
    invoke-direct {p0, v0}, Lorg/apache/index/poi/hwpf/sprm/SprmBuffer;->ensureCapacity(I)V

    .line 108
    iget-object v1, p0, Lorg/apache/index/poi/hwpf/sprm/SprmBuffer;->_buf:[B

    iget v2, p0, Lorg/apache/index/poi/hwpf/sprm/SprmBuffer;->_offset:I

    invoke-static {v1, v2, p1}, Lorg/apache/index/poi/util/LittleEndian;->putShort([BIS)V

    .line 109
    iget v1, p0, Lorg/apache/index/poi/hwpf/sprm/SprmBuffer;->_offset:I

    add-int/lit8 v1, v1, 0x2

    iput v1, p0, Lorg/apache/index/poi/hwpf/sprm/SprmBuffer;->_offset:I

    .line 110
    iget-object v1, p0, Lorg/apache/index/poi/hwpf/sprm/SprmBuffer;->_buf:[B

    iget v2, p0, Lorg/apache/index/poi/hwpf/sprm/SprmBuffer;->_offset:I

    invoke-static {v1, v2, p2}, Lorg/apache/index/poi/util/LittleEndian;->putShort([BIS)V

    .line 111
    iget v1, p0, Lorg/apache/index/poi/hwpf/sprm/SprmBuffer;->_offset:I

    add-int/lit8 v1, v1, 0x2

    iput v1, p0, Lorg/apache/index/poi/hwpf/sprm/SprmBuffer;->_offset:I

    .line 112
    return-void
.end method

.method public addSprm(S[B)V
    .locals 5
    .param p1, "opcode"    # S
    .param p2, "operand"    # [B

    .prologue
    .line 124
    array-length v1, p2

    add-int/lit8 v0, v1, 0x3

    .line 125
    .local v0, "addition":I
    invoke-direct {p0, v0}, Lorg/apache/index/poi/hwpf/sprm/SprmBuffer;->ensureCapacity(I)V

    .line 126
    iget-object v1, p0, Lorg/apache/index/poi/hwpf/sprm/SprmBuffer;->_buf:[B

    iget v2, p0, Lorg/apache/index/poi/hwpf/sprm/SprmBuffer;->_offset:I

    invoke-static {v1, v2, p1}, Lorg/apache/index/poi/util/LittleEndian;->putShort([BIS)V

    .line 127
    iget v1, p0, Lorg/apache/index/poi/hwpf/sprm/SprmBuffer;->_offset:I

    add-int/lit8 v1, v1, 0x2

    iput v1, p0, Lorg/apache/index/poi/hwpf/sprm/SprmBuffer;->_offset:I

    .line 128
    iget-object v1, p0, Lorg/apache/index/poi/hwpf/sprm/SprmBuffer;->_buf:[B

    iget v2, p0, Lorg/apache/index/poi/hwpf/sprm/SprmBuffer;->_offset:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lorg/apache/index/poi/hwpf/sprm/SprmBuffer;->_offset:I

    array-length v3, p2

    int-to-byte v3, v3

    aput-byte v3, v1, v2

    .line 129
    const/4 v1, 0x0

    iget-object v2, p0, Lorg/apache/index/poi/hwpf/sprm/SprmBuffer;->_buf:[B

    iget v3, p0, Lorg/apache/index/poi/hwpf/sprm/SprmBuffer;->_offset:I

    array-length v4, p2

    invoke-static {p2, v1, v2, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 130
    return-void
.end method

.method public append([B)V
    .locals 4
    .param p1, "grpprl"    # [B

    .prologue
    .line 152
    array-length v0, p1

    invoke-direct {p0, v0}, Lorg/apache/index/poi/hwpf/sprm/SprmBuffer;->ensureCapacity(I)V

    .line 153
    const/4 v0, 0x0

    iget-object v1, p0, Lorg/apache/index/poi/hwpf/sprm/SprmBuffer;->_buf:[B

    iget v2, p0, Lorg/apache/index/poi/hwpf/sprm/SprmBuffer;->_offset:I

    array-length v3, p1

    invoke-static {p1, v0, v1, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 154
    return-void
.end method

.method public clone()Ljava/lang/Object;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 159
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/index/poi/hwpf/sprm/SprmBuffer;

    .line 160
    .local v0, "retVal":Lorg/apache/index/poi/hwpf/sprm/SprmBuffer;
    iget-object v1, p0, Lorg/apache/index/poi/hwpf/sprm/SprmBuffer;->_buf:[B

    array-length v1, v1

    new-array v1, v1, [B

    iput-object v1, v0, Lorg/apache/index/poi/hwpf/sprm/SprmBuffer;->_buf:[B

    .line 161
    iget-object v1, p0, Lorg/apache/index/poi/hwpf/sprm/SprmBuffer;->_buf:[B

    iget-object v2, v0, Lorg/apache/index/poi/hwpf/sprm/SprmBuffer;->_buf:[B

    iget-object v3, p0, Lorg/apache/index/poi/hwpf/sprm/SprmBuffer;->_buf:[B

    array-length v3, v3

    invoke-static {v1, v4, v2, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 162
    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 139
    if-nez p1, :cond_1

    .line 147
    :cond_0
    :goto_0
    return v1

    .line 143
    :cond_1
    instance-of v2, p1, Lorg/apache/index/poi/hwpf/sprm/SprmBuffer;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 144
    check-cast v0, Lorg/apache/index/poi/hwpf/sprm/SprmBuffer;

    .line 145
    .local v0, "sprmBuf":Lorg/apache/index/poi/hwpf/sprm/SprmBuffer;
    iget-object v1, p0, Lorg/apache/index/poi/hwpf/sprm/SprmBuffer;->_buf:[B

    iget-object v2, v0, Lorg/apache/index/poi/hwpf/sprm/SprmBuffer;->_buf:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    goto :goto_0
.end method

.method public toByteArray()[B
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/sprm/SprmBuffer;->_buf:[B

    return-object v0
.end method

.method public updateSprm(SB)V
    .locals 2
    .param p1, "opcode"    # S
    .param p2, "operand"    # B

    .prologue
    .line 65
    invoke-direct {p0, p1}, Lorg/apache/index/poi/hwpf/sprm/SprmBuffer;->findSprm(S)I

    move-result v0

    .line 66
    .local v0, "grpprlOffset":I
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 68
    iget-object v1, p0, Lorg/apache/index/poi/hwpf/sprm/SprmBuffer;->_buf:[B

    aput-byte p2, v1, v0

    .line 72
    :goto_0
    return-void

    .line 71
    :cond_0
    invoke-virtual {p0, p1, p2}, Lorg/apache/index/poi/hwpf/sprm/SprmBuffer;->addSprm(SB)V

    goto :goto_0
.end method

.method public updateSprm(SI)V
    .locals 2
    .param p1, "opcode"    # S
    .param p2, "operand"    # I

    .prologue
    .line 87
    invoke-direct {p0, p1}, Lorg/apache/index/poi/hwpf/sprm/SprmBuffer;->findSprm(S)I

    move-result v0

    .line 88
    .local v0, "grpprlOffset":I
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 90
    iget-object v1, p0, Lorg/apache/index/poi/hwpf/sprm/SprmBuffer;->_buf:[B

    invoke-static {v1, v0, p2}, Lorg/apache/index/poi/util/LittleEndian;->putInt([BII)V

    .line 94
    :goto_0
    return-void

    .line 93
    :cond_0
    invoke-virtual {p0, p1, p2}, Lorg/apache/index/poi/hwpf/sprm/SprmBuffer;->addSprm(SI)V

    goto :goto_0
.end method

.method public updateSprm(SS)V
    .locals 2
    .param p1, "opcode"    # S
    .param p2, "operand"    # S

    .prologue
    .line 76
    invoke-direct {p0, p1}, Lorg/apache/index/poi/hwpf/sprm/SprmBuffer;->findSprm(S)I

    move-result v0

    .line 77
    .local v0, "grpprlOffset":I
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 79
    iget-object v1, p0, Lorg/apache/index/poi/hwpf/sprm/SprmBuffer;->_buf:[B

    invoke-static {v1, v0, p2}, Lorg/apache/index/poi/util/LittleEndian;->putShort([BIS)V

    .line 83
    :goto_0
    return-void

    .line 82
    :cond_0
    invoke-virtual {p0, p1, p2}, Lorg/apache/index/poi/hwpf/sprm/SprmBuffer;->addSprm(SS)V

    goto :goto_0
.end method
