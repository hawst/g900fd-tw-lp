.class public Lorg/apache/index/poi/ddf/EscherSpRecord;
.super Lorg/apache/index/poi/ddf/EscherRecord;
.source "EscherSpRecord.java"


# static fields
.field public static final FLAG_BACKGROUND:I = 0x400

.field public static final FLAG_CHILD:I = 0x2

.field public static final FLAG_CONNECTOR:I = 0x100

.field public static final FLAG_DELETED:I = 0x8

.field public static final FLAG_FLIPHORIZ:I = 0x40

.field public static final FLAG_FLIPVERT:I = 0x80

.field public static final FLAG_GROUP:I = 0x1

.field public static final FLAG_HASSHAPETYPE:I = 0x800

.field public static final FLAG_HAVEANCHOR:I = 0x200

.field public static final FLAG_HAVEMASTER:I = 0x20

.field public static final FLAG_OLESHAPE:I = 0x10

.field public static final FLAG_PATRIARCH:I = 0x4

.field public static final RECORD_DESCRIPTION:Ljava/lang/String; = "MsofbtSp"

.field public static final RECORD_ID:S = -0xff6s


# instance fields
.field private field_1_shapeId:I

.field private field_2_flags:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Lorg/apache/index/poi/ddf/EscherRecord;-><init>()V

    return-void
.end method

.method private decodeFlags(I)Ljava/lang/String;
    .locals 2
    .param p1, "flags"    # I

    .prologue
    .line 123
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 124
    .local v0, "result":Ljava/lang/StringBuffer;
    and-int/lit8 v1, p1, 0x1

    if-eqz v1, :cond_1

    const-string/jumbo v1, "|GROUP"

    :goto_0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 125
    and-int/lit8 v1, p1, 0x2

    if-eqz v1, :cond_2

    const-string/jumbo v1, "|CHILD"

    :goto_1
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 126
    and-int/lit8 v1, p1, 0x4

    if-eqz v1, :cond_3

    const-string/jumbo v1, "|PATRIARCH"

    :goto_2
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 127
    and-int/lit8 v1, p1, 0x8

    if-eqz v1, :cond_4

    const-string/jumbo v1, "|DELETED"

    :goto_3
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 128
    and-int/lit8 v1, p1, 0x10

    if-eqz v1, :cond_5

    const-string/jumbo v1, "|OLESHAPE"

    :goto_4
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 129
    and-int/lit8 v1, p1, 0x20

    if-eqz v1, :cond_6

    const-string/jumbo v1, "|HAVEMASTER"

    :goto_5
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 130
    and-int/lit8 v1, p1, 0x40

    if-eqz v1, :cond_7

    const-string/jumbo v1, "|FLIPHORIZ"

    :goto_6
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 131
    and-int/lit16 v1, p1, 0x80

    if-eqz v1, :cond_8

    const-string/jumbo v1, "|FLIPVERT"

    :goto_7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 132
    and-int/lit16 v1, p1, 0x100

    if-eqz v1, :cond_9

    const-string/jumbo v1, "|CONNECTOR"

    :goto_8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 133
    and-int/lit16 v1, p1, 0x200

    if-eqz v1, :cond_a

    const-string/jumbo v1, "|HAVEANCHOR"

    :goto_9
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 134
    and-int/lit16 v1, p1, 0x400

    if-eqz v1, :cond_b

    const-string/jumbo v1, "|BACKGROUND"

    :goto_a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 135
    and-int/lit16 v1, p1, 0x800

    if-eqz v1, :cond_c

    const-string/jumbo v1, "|HASSHAPETYPE"

    :goto_b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 138
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->length()I

    move-result v1

    if-lez v1, :cond_0

    .line 139
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->deleteCharAt(I)Ljava/lang/StringBuffer;

    .line 141
    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 124
    :cond_1
    const-string/jumbo v1, ""

    goto/16 :goto_0

    .line 125
    :cond_2
    const-string/jumbo v1, ""

    goto :goto_1

    .line 126
    :cond_3
    const-string/jumbo v1, ""

    goto :goto_2

    .line 127
    :cond_4
    const-string/jumbo v1, ""

    goto :goto_3

    .line 128
    :cond_5
    const-string/jumbo v1, ""

    goto :goto_4

    .line 129
    :cond_6
    const-string/jumbo v1, ""

    goto :goto_5

    .line 130
    :cond_7
    const-string/jumbo v1, ""

    goto :goto_6

    .line 131
    :cond_8
    const-string/jumbo v1, ""

    goto :goto_7

    .line 132
    :cond_9
    const-string/jumbo v1, ""

    goto :goto_8

    .line 133
    :cond_a
    const-string/jumbo v1, ""

    goto :goto_9

    .line 134
    :cond_b
    const-string/jumbo v1, ""

    goto :goto_a

    .line 135
    :cond_c
    const-string/jumbo v1, ""

    goto :goto_b
.end method


# virtual methods
.method public fillFields([BILorg/apache/index/poi/ddf/EscherRecordFactory;)I
    .locals 3
    .param p1, "data"    # [B
    .param p2, "offset"    # I
    .param p3, "recordFactory"    # Lorg/apache/index/poi/ddf/EscherRecordFactory;

    .prologue
    .line 52
    invoke-virtual {p0, p1, p2}, Lorg/apache/index/poi/ddf/EscherSpRecord;->readHeader([BI)I

    .line 53
    add-int/lit8 v0, p2, 0x8

    .line 54
    .local v0, "pos":I
    const/4 v1, 0x0

    .line 55
    .local v1, "size":I
    add-int v2, v0, v1

    invoke-static {p1, v2}, Lorg/apache/index/poi/util/LittleEndian;->getInt([BI)I

    move-result v2

    iput v2, p0, Lorg/apache/index/poi/ddf/EscherSpRecord;->field_1_shapeId:I

    add-int/lit8 v1, v1, 0x4

    .line 56
    add-int/lit8 v2, v0, 0x4

    invoke-static {p1, v2}, Lorg/apache/index/poi/util/LittleEndian;->getInt([BI)I

    move-result v2

    iput v2, p0, Lorg/apache/index/poi/ddf/EscherSpRecord;->field_2_flags:I

    .line 61
    invoke-virtual {p0}, Lorg/apache/index/poi/ddf/EscherSpRecord;->getRecordSize()I

    move-result v2

    return v2
.end method

.method public getFlags()I
    .locals 1

    .prologue
    .line 178
    iget v0, p0, Lorg/apache/index/poi/ddf/EscherSpRecord;->field_2_flags:I

    return v0
.end method

.method public getRecordId()S
    .locals 1

    .prologue
    .line 95
    const/16 v0, -0xff6

    return v0
.end method

.method public getRecordName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 99
    const-string/jumbo v0, "Sp"

    return-object v0
.end method

.method public getRecordSize()I
    .locals 1

    .prologue
    .line 91
    const/16 v0, 0x10

    return v0
.end method

.method public getShapeId()I
    .locals 1

    .prologue
    .line 149
    iget v0, p0, Lorg/apache/index/poi/ddf/EscherSpRecord;->field_1_shapeId:I

    return v0
.end method

.method public serialize(I[BLorg/apache/index/poi/ddf/EscherSerializationListener;)I
    .locals 4
    .param p1, "offset"    # I
    .param p2, "data"    # [B
    .param p3, "listener"    # Lorg/apache/index/poi/ddf/EscherSerializationListener;

    .prologue
    .line 76
    invoke-virtual {p0}, Lorg/apache/index/poi/ddf/EscherSpRecord;->getRecordId()S

    move-result v1

    invoke-interface {p3, p1, v1, p0}, Lorg/apache/index/poi/ddf/EscherSerializationListener;->beforeRecordSerialize(ISLorg/apache/index/poi/ddf/EscherRecord;)V

    .line 77
    invoke-virtual {p0}, Lorg/apache/index/poi/ddf/EscherSpRecord;->getOptions()S

    move-result v1

    invoke-static {p2, p1, v1}, Lorg/apache/index/poi/util/LittleEndian;->putShort([BIS)V

    .line 78
    add-int/lit8 v1, p1, 0x2

    invoke-virtual {p0}, Lorg/apache/index/poi/ddf/EscherSpRecord;->getRecordId()S

    move-result v2

    invoke-static {p2, v1, v2}, Lorg/apache/index/poi/util/LittleEndian;->putShort([BIS)V

    .line 79
    const/16 v0, 0x8

    .line 80
    .local v0, "remainingBytes":I
    add-int/lit8 v1, p1, 0x4

    invoke-static {p2, v1, v0}, Lorg/apache/index/poi/util/LittleEndian;->putInt([BII)V

    .line 81
    add-int/lit8 v1, p1, 0x8

    iget v2, p0, Lorg/apache/index/poi/ddf/EscherSpRecord;->field_1_shapeId:I

    invoke-static {p2, v1, v2}, Lorg/apache/index/poi/util/LittleEndian;->putInt([BII)V

    .line 82
    add-int/lit8 v1, p1, 0xc

    iget v2, p0, Lorg/apache/index/poi/ddf/EscherSpRecord;->field_2_flags:I

    invoke-static {p2, v1, v2}, Lorg/apache/index/poi/util/LittleEndian;->putInt([BII)V

    .line 85
    invoke-virtual {p0}, Lorg/apache/index/poi/ddf/EscherSpRecord;->getRecordSize()I

    move-result v1

    add-int/2addr v1, p1

    invoke-virtual {p0}, Lorg/apache/index/poi/ddf/EscherSpRecord;->getRecordId()S

    move-result v2

    invoke-virtual {p0}, Lorg/apache/index/poi/ddf/EscherSpRecord;->getRecordSize()I

    move-result v3

    invoke-interface {p3, v1, v2, v3, p0}, Lorg/apache/index/poi/ddf/EscherSerializationListener;->afterRecordSerialize(ISILorg/apache/index/poi/ddf/EscherRecord;)V

    .line 86
    const/16 v1, 0x10

    return v1
.end method

.method public setFlags(I)V
    .locals 0
    .param p1, "field_2_flags"    # I

    .prologue
    .line 199
    iput p1, p0, Lorg/apache/index/poi/ddf/EscherSpRecord;->field_2_flags:I

    .line 200
    return-void
.end method

.method public setShapeId(I)V
    .locals 0
    .param p1, "field_1_shapeId"    # I

    .prologue
    .line 157
    iput p1, p0, Lorg/apache/index/poi/ddf/EscherSpRecord;->field_1_shapeId:I

    .line 158
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 108
    const-string/jumbo v1, "line.separator"

    invoke-static {v1}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 110
    .local v0, "nl":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 111
    const-string/jumbo v2, "  RecordId: 0x"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v2, -0xff6

    invoke-static {v2}, Lorg/apache/index/poi/util/HexDump;->toHex(S)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 112
    const-string/jumbo v2, "  Options: 0x"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/index/poi/ddf/EscherSpRecord;->getOptions()S

    move-result v2

    invoke-static {v2}, Lorg/apache/index/poi/util/HexDump;->toHex(S)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 113
    const-string/jumbo v2, "  ShapeId: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lorg/apache/index/poi/ddf/EscherSpRecord;->field_1_shapeId:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 114
    const-string/jumbo v2, "  Flags: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lorg/apache/index/poi/ddf/EscherSpRecord;->field_2_flags:I

    invoke-direct {p0, v2}, Lorg/apache/index/poi/ddf/EscherSpRecord;->decodeFlags(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " (0x"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lorg/apache/index/poi/ddf/EscherSpRecord;->field_2_flags:I

    invoke-static {v2}, Lorg/apache/index/poi/util/HexDump;->toHex(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 110
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
