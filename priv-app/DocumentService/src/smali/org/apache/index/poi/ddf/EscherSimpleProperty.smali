.class public Lorg/apache/index/poi/ddf/EscherSimpleProperty;
.super Lorg/apache/index/poi/ddf/EscherProperty;
.source "EscherSimpleProperty.java"


# instance fields
.field protected propertyValue:I


# direct methods
.method public constructor <init>(SI)V
    .locals 0
    .param p1, "id"    # S
    .param p2, "propertyValue"    # I

    .prologue
    .line 40
    invoke-direct {p0, p1}, Lorg/apache/index/poi/ddf/EscherProperty;-><init>(S)V

    .line 41
    iput p2, p0, Lorg/apache/index/poi/ddf/EscherSimpleProperty;->propertyValue:I

    .line 42
    return-void
.end method

.method public constructor <init>(SZZI)V
    .locals 0
    .param p1, "propertyNumber"    # S
    .param p2, "isComplex"    # Z
    .param p3, "isBlipId"    # Z
    .param p4, "propertyValue"    # I

    .prologue
    .line 50
    invoke-direct {p0, p1, p2, p3}, Lorg/apache/index/poi/ddf/EscherProperty;-><init>(SZZ)V

    .line 51
    iput p4, p0, Lorg/apache/index/poi/ddf/EscherSimpleProperty;->propertyValue:I

    .line 52
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 88
    if-ne p0, p1, :cond_1

    .line 96
    :cond_0
    :goto_0
    return v1

    .line 89
    :cond_1
    instance-of v3, p1, Lorg/apache/index/poi/ddf/EscherSimpleProperty;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 91
    check-cast v0, Lorg/apache/index/poi/ddf/EscherSimpleProperty;

    .line 93
    .local v0, "escherSimpleProperty":Lorg/apache/index/poi/ddf/EscherSimpleProperty;
    iget v3, p0, Lorg/apache/index/poi/ddf/EscherSimpleProperty;->propertyValue:I

    iget v4, v0, Lorg/apache/index/poi/ddf/EscherSimpleProperty;->propertyValue:I

    if-eq v3, v4, :cond_3

    move v1, v2

    goto :goto_0

    .line 94
    :cond_3
    invoke-virtual {p0}, Lorg/apache/index/poi/ddf/EscherSimpleProperty;->getId()S

    move-result v3

    invoke-virtual {v0}, Lorg/apache/index/poi/ddf/EscherSimpleProperty;->getId()S

    move-result v4

    if-eq v3, v4, :cond_0

    move v1, v2

    goto :goto_0
.end method

.method public getPropertyValue()I
    .locals 1

    .prologue
    .line 80
    iget v0, p0, Lorg/apache/index/poi/ddf/EscherSimpleProperty;->propertyValue:I

    return v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 105
    iget v0, p0, Lorg/apache/index/poi/ddf/EscherSimpleProperty;->propertyValue:I

    return v0
.end method

.method public serializeComplexPart([BI)I
    .locals 1
    .param p1, "data"    # [B
    .param p2, "pos"    # I

    .prologue
    .line 72
    const/4 v0, 0x0

    return v0
.end method

.method public serializeSimplePart([BI)I
    .locals 2
    .param p1, "data"    # [B
    .param p2, "offset"    # I

    .prologue
    .line 61
    invoke-virtual {p0}, Lorg/apache/index/poi/ddf/EscherSimpleProperty;->getId()S

    move-result v0

    invoke-static {p1, p2, v0}, Lorg/apache/index/poi/util/LittleEndian;->putShort([BIS)V

    .line 62
    add-int/lit8 v0, p2, 0x2

    iget v1, p0, Lorg/apache/index/poi/ddf/EscherSimpleProperty;->propertyValue:I

    invoke-static {p1, v0, v1}, Lorg/apache/index/poi/util/LittleEndian;->putInt([BII)V

    .line 63
    const/4 v0, 0x6

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 113
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "propNum: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lorg/apache/index/poi/ddf/EscherSimpleProperty;->getPropertyNumber()S

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 114
    const-string/jumbo v1, ", RAW: 0x"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lorg/apache/index/poi/ddf/EscherSimpleProperty;->getId()S

    move-result v1

    invoke-static {v1}, Lorg/apache/index/poi/util/HexDump;->toHex(S)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 115
    const-string/jumbo v1, ", propName: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lorg/apache/index/poi/ddf/EscherSimpleProperty;->getPropertyNumber()S

    move-result v1

    invoke-static {v1}, Lorg/apache/index/poi/ddf/EscherProperties;->getPropertyName(S)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 116
    const-string/jumbo v1, ", complex: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lorg/apache/index/poi/ddf/EscherSimpleProperty;->isComplex()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 117
    const-string/jumbo v1, ", blipId: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lorg/apache/index/poi/ddf/EscherSimpleProperty;->isBlipId()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 118
    const-string/jumbo v1, ", value: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lorg/apache/index/poi/ddf/EscherSimpleProperty;->propertyValue:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " (0x"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lorg/apache/index/poi/ddf/EscherSimpleProperty;->propertyValue:I

    invoke-static {v1}, Lorg/apache/index/poi/util/HexDump;->toHex(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 113
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
