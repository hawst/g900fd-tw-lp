.class public final Lorg/apache/index/poi/hwpf/model/CachedPropertyNode;
.super Lorg/apache/index/poi/hwpf/model/PropertyNode;
.source "CachedPropertyNode.java"


# instance fields
.field protected _propCache:Ljava/lang/ref/SoftReference;


# direct methods
.method public constructor <init>(IILorg/apache/index/poi/hwpf/sprm/SprmBuffer;)V
    .locals 0
    .param p1, "start"    # I
    .param p2, "end"    # I
    .param p3, "buf"    # Lorg/apache/index/poi/hwpf/sprm/SprmBuffer;

    .prologue
    .line 31
    invoke-direct {p0, p1, p2, p3}, Lorg/apache/index/poi/hwpf/model/PropertyNode;-><init>(IILjava/lang/Object;)V

    .line 32
    return-void
.end method


# virtual methods
.method protected fillCache(Ljava/lang/Object;)V
    .locals 1
    .param p1, "ref"    # Ljava/lang/Object;

    .prologue
    .line 36
    new-instance v0, Ljava/lang/ref/SoftReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/SoftReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lorg/apache/index/poi/hwpf/model/CachedPropertyNode;->_propCache:Ljava/lang/ref/SoftReference;

    .line 37
    return-void
.end method

.method protected getCacheContents()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/CachedPropertyNode;->_propCache:Ljava/lang/ref/SoftReference;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/CachedPropertyNode;->_propCache:Ljava/lang/ref/SoftReference;

    invoke-virtual {v0}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public getSprmBuf()Lorg/apache/index/poi/hwpf/sprm/SprmBuffer;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/CachedPropertyNode;->_buf:Ljava/lang/Object;

    check-cast v0, Lorg/apache/index/poi/hwpf/sprm/SprmBuffer;

    return-object v0
.end method
