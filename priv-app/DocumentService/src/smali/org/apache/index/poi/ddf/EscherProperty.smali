.class public abstract Lorg/apache/index/poi/ddf/EscherProperty;
.super Ljava/lang/Object;
.source "EscherProperty.java"


# instance fields
.field private _id:S


# direct methods
.method public constructor <init>(S)V
    .locals 0
    .param p1, "id"    # S

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-short p1, p0, Lorg/apache/index/poi/ddf/EscherProperty;->_id:S

    .line 36
    return-void
.end method

.method public constructor <init>(SZZ)V
    .locals 2
    .param p1, "propertyNumber"    # S
    .param p2, "isComplex"    # Z
    .param p3, "isBlipId"    # Z

    .prologue
    const/4 v0, 0x0

    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    if-eqz p2, :cond_1

    const v1, 0x8000

    :goto_0
    add-int/2addr v1, p1

    .line 45
    if-eqz p3, :cond_0

    const/16 v0, 0x4000

    :cond_0
    add-int/2addr v0, v1

    int-to-short v0, v0

    .line 43
    iput-short v0, p0, Lorg/apache/index/poi/ddf/EscherProperty;->_id:S

    .line 46
    return-void

    :cond_1
    move v1, v0

    .line 44
    goto :goto_0
.end method


# virtual methods
.method public getId()S
    .locals 1

    .prologue
    .line 49
    iget-short v0, p0, Lorg/apache/index/poi/ddf/EscherProperty;->_id:S

    return v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 65
    iget-short v0, p0, Lorg/apache/index/poi/ddf/EscherProperty;->_id:S

    invoke-static {v0}, Lorg/apache/index/poi/ddf/EscherProperties;->getPropertyName(S)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPropertyNumber()S
    .locals 1

    .prologue
    .line 53
    iget-short v0, p0, Lorg/apache/index/poi/ddf/EscherProperty;->_id:S

    and-int/lit16 v0, v0, 0x3fff

    int-to-short v0, v0

    return v0
.end method

.method public getPropertySize()I
    .locals 1

    .prologue
    .line 73
    const/4 v0, 0x6

    return v0
.end method

.method public isBlipId()Z
    .locals 1

    .prologue
    .line 61
    iget-short v0, p0, Lorg/apache/index/poi/ddf/EscherProperty;->_id:S

    and-int/lit16 v0, v0, 0x4000

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isComplex()Z
    .locals 1

    .prologue
    .line 57
    iget-short v0, p0, Lorg/apache/index/poi/ddf/EscherProperty;->_id:S

    and-int/lit16 v0, v0, -0x8000

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public abstract serializeComplexPart([BI)I
.end method

.method public abstract serializeSimplePart([BI)I
.end method
