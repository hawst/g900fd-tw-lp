.class public interface abstract Lorg/apache/index/poi/poifs/common/POIFSConstants;
.super Ljava/lang/Object;
.source "POIFSConstants.java"


# static fields
.field public static final BIG_BLOCK_MINIMUM_DOCUMENT_SIZE:I = 0x1000

.field public static final DIFAT_SECTOR_BLOCK:I = -0x4

.field public static final END_OF_CHAIN:I = -0x2

.field public static final FAT_SECTOR_BLOCK:I = -0x3

.field public static final LARGER_BIG_BLOCK_SIZE:I = 0x1000

.field public static final LARGER_BIG_BLOCK_SIZE_DETAILS:Lorg/apache/index/poi/poifs/common/POIFSBigBlockSize;

.field public static final LARGEST_REGULAR_SECTOR_NUMBER:I = -0x5

.field public static final OOXML_FILE_HEADER:[B

.field public static final PROPERTY_SIZE:I = 0x80

.field public static final SMALLER_BIG_BLOCK_SIZE:I = 0x200

.field public static final SMALLER_BIG_BLOCK_SIZE_DETAILS:Lorg/apache/index/poi/poifs/common/POIFSBigBlockSize;

.field public static final SMALL_BLOCK_SIZE:I = 0x40

.field public static final UNUSED_BLOCK:I = -0x1


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 30
    new-instance v0, Lorg/apache/index/poi/poifs/common/POIFSBigBlockSize;

    const/16 v1, 0x200

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lorg/apache/index/poi/poifs/common/POIFSBigBlockSize;-><init>(IS)V

    .line 29
    sput-object v0, Lorg/apache/index/poi/poifs/common/POIFSConstants;->SMALLER_BIG_BLOCK_SIZE_DETAILS:Lorg/apache/index/poi/poifs/common/POIFSBigBlockSize;

    .line 34
    new-instance v0, Lorg/apache/index/poi/poifs/common/POIFSBigBlockSize;

    const/16 v1, 0x1000

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lorg/apache/index/poi/poifs/common/POIFSBigBlockSize;-><init>(IS)V

    .line 33
    sput-object v0, Lorg/apache/index/poi/poifs/common/POIFSConstants;->LARGER_BIG_BLOCK_SIZE_DETAILS:Lorg/apache/index/poi/poifs/common/POIFSBigBlockSize;

    .line 63
    const/4 v0, 0x4

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    .line 62
    sput-object v0, Lorg/apache/index/poi/poifs/common/POIFSConstants;->OOXML_FILE_HEADER:[B

    .line 63
    return-void

    nop

    :array_0
    .array-data 1
        0x50t
        0x4bt
        0x3t
        0x4t
    .end array-data
.end method
