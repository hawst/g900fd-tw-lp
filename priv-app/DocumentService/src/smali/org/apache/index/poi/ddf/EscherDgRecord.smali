.class public Lorg/apache/index/poi/ddf/EscherDgRecord;
.super Lorg/apache/index/poi/ddf/EscherRecord;
.source "EscherDgRecord.java"


# static fields
.field public static final RECORD_DESCRIPTION:Ljava/lang/String; = "MsofbtDg"

.field public static final RECORD_ID:S = -0xff8s


# instance fields
.field private field_1_numShapes:I

.field private field_2_lastMSOSPID:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Lorg/apache/index/poi/ddf/EscherRecord;-><init>()V

    return-void
.end method


# virtual methods
.method public fillFields([BILorg/apache/index/poi/ddf/EscherRecordFactory;)I
    .locals 3
    .param p1, "data"    # [B
    .param p2, "offset"    # I
    .param p3, "recordFactory"    # Lorg/apache/index/poi/ddf/EscherRecordFactory;

    .prologue
    .line 40
    invoke-virtual {p0, p1, p2}, Lorg/apache/index/poi/ddf/EscherDgRecord;->readHeader([BI)I

    .line 41
    add-int/lit8 v0, p2, 0x8

    .line 42
    .local v0, "pos":I
    const/4 v1, 0x0

    .line 43
    .local v1, "size":I
    add-int v2, v0, v1

    invoke-static {p1, v2}, Lorg/apache/index/poi/util/LittleEndian;->getInt([BI)I

    move-result v2

    iput v2, p0, Lorg/apache/index/poi/ddf/EscherDgRecord;->field_1_numShapes:I

    add-int/lit8 v1, v1, 0x4

    .line 44
    add-int/lit8 v2, v0, 0x4

    invoke-static {p1, v2}, Lorg/apache/index/poi/util/LittleEndian;->getInt([BI)I

    move-result v2

    iput v2, p0, Lorg/apache/index/poi/ddf/EscherDgRecord;->field_2_lastMSOSPID:I

    .line 49
    invoke-virtual {p0}, Lorg/apache/index/poi/ddf/EscherDgRecord;->getRecordSize()I

    move-result v2

    return v2
.end method

.method public getDrawingGroupId()S
    .locals 1

    .prologue
    .line 138
    invoke-virtual {p0}, Lorg/apache/index/poi/ddf/EscherDgRecord;->getOptions()S

    move-result v0

    shr-int/lit8 v0, v0, 0x4

    int-to-short v0, v0

    return v0
.end method

.method public getLastMSOSPID()I
    .locals 1

    .prologue
    .line 119
    iget v0, p0, Lorg/apache/index/poi/ddf/EscherDgRecord;->field_2_lastMSOSPID:I

    return v0
.end method

.method public getNumShapes()I
    .locals 1

    .prologue
    .line 103
    iget v0, p0, Lorg/apache/index/poi/ddf/EscherDgRecord;->field_1_numShapes:I

    return v0
.end method

.method public getRecordId()S
    .locals 1

    .prologue
    .line 79
    const/16 v0, -0xff8

    return v0
.end method

.method public getRecordName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 83
    const-string/jumbo v0, "Dg"

    return-object v0
.end method

.method public getRecordSize()I
    .locals 1

    .prologue
    .line 75
    const/16 v0, 0x10

    return v0
.end method

.method public incrementShapeCount()V
    .locals 1

    .prologue
    .line 143
    iget v0, p0, Lorg/apache/index/poi/ddf/EscherDgRecord;->field_1_numShapes:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/index/poi/ddf/EscherDgRecord;->field_1_numShapes:I

    .line 144
    return-void
.end method

.method public serialize(I[BLorg/apache/index/poi/ddf/EscherSerializationListener;)I
    .locals 3
    .param p1, "offset"    # I
    .param p2, "data"    # [B
    .param p3, "listener"    # Lorg/apache/index/poi/ddf/EscherSerializationListener;

    .prologue
    .line 54
    invoke-virtual {p0}, Lorg/apache/index/poi/ddf/EscherDgRecord;->getRecordId()S

    move-result v0

    invoke-interface {p3, p1, v0, p0}, Lorg/apache/index/poi/ddf/EscherSerializationListener;->beforeRecordSerialize(ISLorg/apache/index/poi/ddf/EscherRecord;)V

    .line 56
    invoke-virtual {p0}, Lorg/apache/index/poi/ddf/EscherDgRecord;->getOptions()S

    move-result v0

    invoke-static {p2, p1, v0}, Lorg/apache/index/poi/util/LittleEndian;->putShort([BIS)V

    .line 57
    add-int/lit8 v0, p1, 0x2

    invoke-virtual {p0}, Lorg/apache/index/poi/ddf/EscherDgRecord;->getRecordId()S

    move-result v1

    invoke-static {p2, v0, v1}, Lorg/apache/index/poi/util/LittleEndian;->putShort([BIS)V

    .line 58
    add-int/lit8 v0, p1, 0x4

    const/16 v1, 0x8

    invoke-static {p2, v0, v1}, Lorg/apache/index/poi/util/LittleEndian;->putInt([BII)V

    .line 59
    add-int/lit8 v0, p1, 0x8

    iget v1, p0, Lorg/apache/index/poi/ddf/EscherDgRecord;->field_1_numShapes:I

    invoke-static {p2, v0, v1}, Lorg/apache/index/poi/util/LittleEndian;->putInt([BII)V

    .line 60
    add-int/lit8 v0, p1, 0xc

    iget v1, p0, Lorg/apache/index/poi/ddf/EscherDgRecord;->field_2_lastMSOSPID:I

    invoke-static {p2, v0, v1}, Lorg/apache/index/poi/util/LittleEndian;->putInt([BII)V

    .line 64
    add-int/lit8 v0, p1, 0x10

    invoke-virtual {p0}, Lorg/apache/index/poi/ddf/EscherDgRecord;->getRecordId()S

    move-result v1

    invoke-virtual {p0}, Lorg/apache/index/poi/ddf/EscherDgRecord;->getRecordSize()I

    move-result v2

    invoke-interface {p3, v0, v1, v2, p0}, Lorg/apache/index/poi/ddf/EscherSerializationListener;->afterRecordSerialize(ISILorg/apache/index/poi/ddf/EscherRecord;)V

    .line 65
    invoke-virtual {p0}, Lorg/apache/index/poi/ddf/EscherDgRecord;->getRecordSize()I

    move-result v0

    return v0
.end method

.method public setLastMSOSPID(I)V
    .locals 0
    .param p1, "field_2_lastMSOSPID"    # I

    .prologue
    .line 127
    iput p1, p0, Lorg/apache/index/poi/ddf/EscherDgRecord;->field_2_lastMSOSPID:I

    .line 128
    return-void
.end method

.method public setNumShapes(I)V
    .locals 0
    .param p1, "field_1_numShapes"    # I

    .prologue
    .line 111
    iput p1, p0, Lorg/apache/index/poi/ddf/EscherDgRecord;->field_1_numShapes:I

    .line 112
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    const/16 v2, 0xa

    .line 90
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 91
    const-string/jumbo v1, "  RecordId: 0x"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, -0xff8

    invoke-static {v1}, Lorg/apache/index/poi/util/HexDump;->toHex(S)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 92
    const-string/jumbo v1, "  Options: 0x"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lorg/apache/index/poi/ddf/EscherDgRecord;->getOptions()S

    move-result v1

    invoke-static {v1}, Lorg/apache/index/poi/util/HexDump;->toHex(S)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 93
    const-string/jumbo v1, "  NumShapes: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lorg/apache/index/poi/ddf/EscherDgRecord;->field_1_numShapes:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 94
    const-string/jumbo v1, "  LastMSOSPID: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lorg/apache/index/poi/ddf/EscherDgRecord;->field_2_lastMSOSPID:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 90
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
