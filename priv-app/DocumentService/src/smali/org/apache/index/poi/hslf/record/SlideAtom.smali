.class public final Lorg/apache/index/poi/hslf/record/SlideAtom;
.super Lorg/apache/index/poi/hslf/record/RecordAtom;
.source "SlideAtom.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/index/poi/hslf/record/SlideAtom$SSlideLayoutAtom;
    }
.end annotation


# static fields
.field public static final MASTER_SLIDE_ID:I = 0x0

.field public static final USES_MASTER_SLIDE_ID:I = -0x80000000

.field private static _type:J


# instance fields
.field private _header:[B

.field private followMasterBackground:Z

.field private followMasterObjects:Z

.field private followMasterScheme:Z

.field private layoutAtom:Lorg/apache/index/poi/hslf/record/SlideAtom$SSlideLayoutAtom;

.field private masterID:I

.field private notesID:I

.field private reserved:[B


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 36
    const-wide/16 v0, 0x3ef

    sput-wide v0, Lorg/apache/index/poi/hslf/record/SlideAtom;->_type:J

    .line 38
    return-void
.end method

.method public constructor <init>()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 120
    invoke-direct {p0}, Lorg/apache/index/poi/hslf/record/RecordAtom;-><init>()V

    .line 121
    const/16 v1, 0x8

    new-array v1, v1, [B

    iput-object v1, p0, Lorg/apache/index/poi/hslf/record/SlideAtom;->_header:[B

    .line 122
    iget-object v1, p0, Lorg/apache/index/poi/hslf/record/SlideAtom;->_header:[B

    invoke-static {v1, v6, v5}, Lorg/apache/index/poi/util/LittleEndian;->putUShort([BII)V

    .line 123
    iget-object v1, p0, Lorg/apache/index/poi/hslf/record/SlideAtom;->_header:[B

    sget-wide v2, Lorg/apache/index/poi/hslf/record/SlideAtom;->_type:J

    long-to-int v2, v2

    invoke-static {v1, v5, v2}, Lorg/apache/index/poi/util/LittleEndian;->putUShort([BII)V

    .line 124
    iget-object v1, p0, Lorg/apache/index/poi/hslf/record/SlideAtom;->_header:[B

    const/4 v2, 0x4

    const/16 v3, 0x18

    invoke-static {v1, v2, v3}, Lorg/apache/index/poi/util/LittleEndian;->putInt([BII)V

    .line 126
    const/16 v1, 0xc

    new-array v0, v1, [B

    .line 127
    .local v0, "ssdate":[B
    new-instance v1, Lorg/apache/index/poi/hslf/record/SlideAtom$SSlideLayoutAtom;

    invoke-direct {v1, v0}, Lorg/apache/index/poi/hslf/record/SlideAtom$SSlideLayoutAtom;-><init>([B)V

    iput-object v1, p0, Lorg/apache/index/poi/hslf/record/SlideAtom;->layoutAtom:Lorg/apache/index/poi/hslf/record/SlideAtom$SSlideLayoutAtom;

    .line 128
    iget-object v1, p0, Lorg/apache/index/poi/hslf/record/SlideAtom;->layoutAtom:Lorg/apache/index/poi/hslf/record/SlideAtom$SSlideLayoutAtom;

    const/16 v2, 0x10

    invoke-virtual {v1, v2}, Lorg/apache/index/poi/hslf/record/SlideAtom$SSlideLayoutAtom;->setGeometryType(I)V

    .line 130
    iput-boolean v4, p0, Lorg/apache/index/poi/hslf/record/SlideAtom;->followMasterObjects:Z

    .line 131
    iput-boolean v4, p0, Lorg/apache/index/poi/hslf/record/SlideAtom;->followMasterScheme:Z

    .line 132
    iput-boolean v4, p0, Lorg/apache/index/poi/hslf/record/SlideAtom;->followMasterBackground:Z

    .line 133
    const/high16 v1, -0x80000000

    iput v1, p0, Lorg/apache/index/poi/hslf/record/SlideAtom;->masterID:I

    .line 134
    iput v6, p0, Lorg/apache/index/poi/hslf/record/SlideAtom;->notesID:I

    .line 135
    new-array v1, v5, [B

    iput-object v1, p0, Lorg/apache/index/poi/hslf/record/SlideAtom;->reserved:[B

    .line 136
    return-void
.end method

.method protected constructor <init>([BII)V
    .locals 7
    .param p1, "source"    # [B
    .param p2, "start"    # I
    .param p3, "len"    # I

    .prologue
    const/16 v6, 0xc

    const/16 v3, 0x8

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 75
    invoke-direct {p0}, Lorg/apache/index/poi/hslf/record/RecordAtom;-><init>()V

    .line 77
    const/16 v2, 0x1e

    if-ge p3, v2, :cond_0

    const/16 p3, 0x1e

    .line 80
    :cond_0
    new-array v2, v3, [B

    iput-object v2, p0, Lorg/apache/index/poi/hslf/record/SlideAtom;->_header:[B

    .line 81
    iget-object v2, p0, Lorg/apache/index/poi/hslf/record/SlideAtom;->_header:[B

    invoke-static {p1, p2, v2, v5, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 84
    new-array v0, v6, [B

    .line 85
    .local v0, "SSlideLayoutAtomData":[B
    add-int/lit8 v2, p2, 0x8

    invoke-static {p1, v2, v0, v5, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 87
    new-instance v2, Lorg/apache/index/poi/hslf/record/SlideAtom$SSlideLayoutAtom;

    invoke-direct {v2, v0}, Lorg/apache/index/poi/hslf/record/SlideAtom$SSlideLayoutAtom;-><init>([B)V

    iput-object v2, p0, Lorg/apache/index/poi/hslf/record/SlideAtom;->layoutAtom:Lorg/apache/index/poi/hslf/record/SlideAtom$SSlideLayoutAtom;

    .line 90
    add-int/lit8 v2, p2, 0xc

    add-int/lit8 v2, v2, 0x8

    invoke-static {p1, v2}, Lorg/apache/index/poi/util/LittleEndian;->getInt([BI)I

    move-result v2

    iput v2, p0, Lorg/apache/index/poi/hslf/record/SlideAtom;->masterID:I

    .line 91
    add-int/lit8 v2, p2, 0x10

    add-int/lit8 v2, v2, 0x8

    invoke-static {p1, v2}, Lorg/apache/index/poi/util/LittleEndian;->getInt([BI)I

    move-result v2

    iput v2, p0, Lorg/apache/index/poi/hslf/record/SlideAtom;->notesID:I

    .line 94
    add-int/lit8 v2, p2, 0x14

    add-int/lit8 v2, v2, 0x8

    invoke-static {p1, v2}, Lorg/apache/index/poi/util/LittleEndian;->getUShort([BI)I

    move-result v1

    .line 95
    .local v1, "flags":I
    and-int/lit8 v2, v1, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_1

    .line 96
    iput-boolean v4, p0, Lorg/apache/index/poi/hslf/record/SlideAtom;->followMasterBackground:Z

    .line 100
    :goto_0
    and-int/lit8 v2, v1, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_2

    .line 101
    iput-boolean v4, p0, Lorg/apache/index/poi/hslf/record/SlideAtom;->followMasterScheme:Z

    .line 105
    :goto_1
    and-int/lit8 v2, v1, 0x1

    if-ne v2, v4, :cond_3

    .line 106
    iput-boolean v4, p0, Lorg/apache/index/poi/hslf/record/SlideAtom;->followMasterObjects:Z

    .line 113
    :goto_2
    add-int/lit8 v2, p3, -0x1e

    new-array v2, v2, [B

    iput-object v2, p0, Lorg/apache/index/poi/hslf/record/SlideAtom;->reserved:[B

    .line 114
    add-int/lit8 v2, p2, 0x1e

    iget-object v3, p0, Lorg/apache/index/poi/hslf/record/SlideAtom;->reserved:[B

    iget-object v4, p0, Lorg/apache/index/poi/hslf/record/SlideAtom;->reserved:[B

    array-length v4, v4

    invoke-static {p1, v2, v3, v5, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 115
    return-void

    .line 98
    :cond_1
    iput-boolean v5, p0, Lorg/apache/index/poi/hslf/record/SlideAtom;->followMasterBackground:Z

    goto :goto_0

    .line 103
    :cond_2
    iput-boolean v5, p0, Lorg/apache/index/poi/hslf/record/SlideAtom;->followMasterScheme:Z

    goto :goto_1

    .line 108
    :cond_3
    iput-boolean v5, p0, Lorg/apache/index/poi/hslf/record/SlideAtom;->followMasterObjects:Z

    goto :goto_2
.end method


# virtual methods
.method public getFollowMasterBackground()Z
    .locals 1

    .prologue
    .line 64
    iget-boolean v0, p0, Lorg/apache/index/poi/hslf/record/SlideAtom;->followMasterBackground:Z

    return v0
.end method

.method public getFollowMasterObjects()Z
    .locals 1

    .prologue
    .line 62
    iget-boolean v0, p0, Lorg/apache/index/poi/hslf/record/SlideAtom;->followMasterObjects:Z

    return v0
.end method

.method public getFollowMasterScheme()Z
    .locals 1

    .prologue
    .line 63
    iget-boolean v0, p0, Lorg/apache/index/poi/hslf/record/SlideAtom;->followMasterScheme:Z

    return v0
.end method

.method public getMasterID()I
    .locals 1

    .prologue
    .line 51
    iget v0, p0, Lorg/apache/index/poi/hslf/record/SlideAtom;->masterID:I

    return v0
.end method

.method public getNotesID()I
    .locals 1

    .prologue
    .line 55
    iget v0, p0, Lorg/apache/index/poi/hslf/record/SlideAtom;->notesID:I

    return v0
.end method

.method public getRecordType()J
    .locals 2

    .prologue
    .line 141
    sget-wide v0, Lorg/apache/index/poi/hslf/record/SlideAtom;->_type:J

    return-wide v0
.end method

.method public getSSlideLayoutAtom()Lorg/apache/index/poi/hslf/record/SlideAtom$SSlideLayoutAtom;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/SlideAtom;->layoutAtom:Lorg/apache/index/poi/hslf/record/SlideAtom$SSlideLayoutAtom;

    return-object v0
.end method

.method public setFollowMasterBackground(Z)V
    .locals 0
    .param p1, "flag"    # Z

    .prologue
    .line 67
    iput-boolean p1, p0, Lorg/apache/index/poi/hslf/record/SlideAtom;->followMasterBackground:Z

    return-void
.end method

.method public setFollowMasterObjects(Z)V
    .locals 0
    .param p1, "flag"    # Z

    .prologue
    .line 65
    iput-boolean p1, p0, Lorg/apache/index/poi/hslf/record/SlideAtom;->followMasterObjects:Z

    return-void
.end method

.method public setFollowMasterScheme(Z)V
    .locals 0
    .param p1, "flag"    # Z

    .prologue
    .line 66
    iput-boolean p1, p0, Lorg/apache/index/poi/hslf/record/SlideAtom;->followMasterScheme:Z

    return-void
.end method

.method public setMasterID(I)V
    .locals 0
    .param p1, "id"    # I

    .prologue
    .line 53
    iput p1, p0, Lorg/apache/index/poi/hslf/record/SlideAtom;->masterID:I

    return-void
.end method

.method public setNotesID(I)V
    .locals 0
    .param p1, "id"    # I

    .prologue
    .line 60
    iput p1, p0, Lorg/apache/index/poi/hslf/record/SlideAtom;->notesID:I

    return-void
.end method

.method public writeOut(Ljava/io/OutputStream;)V
    .locals 2
    .param p1, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 149
    iget-object v1, p0, Lorg/apache/index/poi/hslf/record/SlideAtom;->_header:[B

    invoke-virtual {p1, v1}, Ljava/io/OutputStream;->write([B)V

    .line 152
    iget-object v1, p0, Lorg/apache/index/poi/hslf/record/SlideAtom;->layoutAtom:Lorg/apache/index/poi/hslf/record/SlideAtom$SSlideLayoutAtom;

    invoke-virtual {v1, p1}, Lorg/apache/index/poi/hslf/record/SlideAtom$SSlideLayoutAtom;->writeOut(Ljava/io/OutputStream;)V

    .line 155
    iget v1, p0, Lorg/apache/index/poi/hslf/record/SlideAtom;->masterID:I

    invoke-static {v1, p1}, Lorg/apache/index/poi/hslf/record/SlideAtom;->writeLittleEndian(ILjava/io/OutputStream;)V

    .line 156
    iget v1, p0, Lorg/apache/index/poi/hslf/record/SlideAtom;->notesID:I

    invoke-static {v1, p1}, Lorg/apache/index/poi/hslf/record/SlideAtom;->writeLittleEndian(ILjava/io/OutputStream;)V

    .line 159
    const/4 v0, 0x0

    .line 160
    .local v0, "flags":S
    iget-boolean v1, p0, Lorg/apache/index/poi/hslf/record/SlideAtom;->followMasterObjects:Z

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    int-to-short v0, v1

    .line 161
    :cond_0
    iget-boolean v1, p0, Lorg/apache/index/poi/hslf/record/SlideAtom;->followMasterScheme:Z

    if-eqz v1, :cond_1

    add-int/lit8 v1, v0, 0x2

    int-to-short v0, v1

    .line 162
    :cond_1
    iget-boolean v1, p0, Lorg/apache/index/poi/hslf/record/SlideAtom;->followMasterBackground:Z

    if-eqz v1, :cond_2

    add-int/lit8 v1, v0, 0x4

    int-to-short v0, v1

    .line 163
    :cond_2
    invoke-static {v0, p1}, Lorg/apache/index/poi/hslf/record/SlideAtom;->writeLittleEndian(SLjava/io/OutputStream;)V

    .line 166
    iget-object v1, p0, Lorg/apache/index/poi/hslf/record/SlideAtom;->reserved:[B

    invoke-virtual {p1, v1}, Ljava/io/OutputStream;->write([B)V

    .line 167
    return-void
.end method
