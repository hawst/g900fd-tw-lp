.class public Lorg/apache/index/poi/hwpf/usermodel/Paragraph;
.super Lorg/apache/index/poi/hwpf/usermodel/Range;
.source "Paragraph.java"

# interfaces
.implements Ljava/lang/Cloneable;


# static fields
.field public static final SPRM_ANLD:S = -0x39c2s

.field public static final SPRM_AUTOSPACEDE:S = 0x2437s

.field public static final SPRM_AUTOSPACEDN:S = 0x2438s

.field public static final SPRM_BRCBAR:S = 0x6629s

.field public static final SPRM_BRCBOTTOM:S = 0x6426s

.field public static final SPRM_BRCL:S = 0x2408s

.field public static final SPRM_BRCLEFT:S = 0x6425s

.field public static final SPRM_BRCP:S = 0x2409s

.field public static final SPRM_BRCRIGHT:S = 0x6427s

.field public static final SPRM_BRCTOP:S = 0x6424s

.field public static final SPRM_CHGTABS:S = -0x39ebs

.field public static final SPRM_CHGTABSPAPX:S = -0x39f3s

.field public static final SPRM_CRLF:S = 0x2444s

.field public static final SPRM_DCS:S = 0x442cs

.field public static final SPRM_DXAABS:S = -0x7be8s

.field public static final SPRM_DXAFROMTEXT:S = -0x7bd1s

.field public static final SPRM_DXALEFT:S = -0x7bf1s

.field public static final SPRM_DXALEFT1:S = -0x7befs

.field public static final SPRM_DXARIGHT:S = -0x7bf2s

.field public static final SPRM_DXAWIDTH:S = -0x7be6s

.field public static final SPRM_DYAABS:S = -0x7be7s

.field public static final SPRM_DYAAFTER:S = -0x5becs

.field public static final SPRM_DYABEFORE:S = -0x5beds

.field public static final SPRM_DYAFROMTEXT:S = -0x7bd2s

.field public static final SPRM_DYALINE:S = 0x6412s

.field public static final SPRM_FADJUSTRIGHT:S = 0x2448s

.field public static final SPRM_FBIDI:S = 0x2441s

.field public static final SPRM_FINTABLE:S = 0x2416s

.field public static final SPRM_FKEEP:S = 0x2405s

.field public static final SPRM_FKEEPFOLLOW:S = 0x2406s

.field public static final SPRM_FKINSOKU:S = 0x2433s

.field public static final SPRM_FLOCKED:S = 0x2430s

.field public static final SPRM_FNOAUTOHYPH:S = 0x242as

.field public static final SPRM_FNOLINENUMB:S = 0x240cs

.field public static final SPRM_FNUMRMLNS:S = 0x2443s

.field public static final SPRM_FOVERFLOWPUNCT:S = 0x2435s

.field public static final SPRM_FPAGEBREAKBEFORE:S = 0x2407s

.field public static final SPRM_FRAMETEXTFLOW:S = 0x443as

.field public static final SPRM_FSIDEBYSIDE:S = 0x2404s

.field public static final SPRM_FTOPLINEPUNCT:S = 0x2436s

.field public static final SPRM_FTTP:S = 0x2417s

.field public static final SPRM_FWIDOWCONTROL:S = 0x2431s

.field public static final SPRM_FWORDWRAP:S = 0x2434s

.field public static final SPRM_ILFO:S = 0x460bs

.field public static final SPRM_ILVL:S = 0x260as

.field public static final SPRM_JC:S = 0x2403s

.field public static final SPRM_NUMRM:S = -0x39bbs

.field public static final SPRM_OUTLVL:S = 0x2640s

.field public static final SPRM_PC:S = 0x261bs

.field public static final SPRM_PROPRMARK:S = -0x39c1s

.field public static final SPRM_RULER:S = -0x39ces

.field public static final SPRM_SHD:S = 0x442ds

.field public static final SPRM_USEPGSUSETTINGS:S = 0x2447s

.field public static final SPRM_WALIGNFONT:S = 0x4439s

.field public static final SPRM_WHEIGHTABS:S = 0x442bs

.field public static final SPRM_WR:S = 0x2423s


# instance fields
.field protected _istd:S

.field protected _papx:Lorg/apache/index/poi/hwpf/sprm/SprmBuffer;


# direct methods
.method protected constructor <init>(Lorg/apache/index/poi/hwpf/model/PAPX;Lorg/apache/index/poi/hwpf/usermodel/Range;)V
    .locals 3
    .param p1, "papx"    # Lorg/apache/index/poi/hwpf/model/PAPX;
    .param p2, "parent"    # Lorg/apache/index/poi/hwpf/usermodel/Range;

    .prologue
    .line 96
    iget v0, p2, Lorg/apache/index/poi/hwpf/usermodel/Range;->_start:I

    invoke-virtual {p1}, Lorg/apache/index/poi/hwpf/model/PAPX;->getStart()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iget v1, p2, Lorg/apache/index/poi/hwpf/usermodel/Range;->_end:I

    invoke-virtual {p1}, Lorg/apache/index/poi/hwpf/model/PAPX;->getEnd()I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-direct {p0, v0, v1, p2}, Lorg/apache/index/poi/hwpf/usermodel/Range;-><init>(IILorg/apache/index/poi/hwpf/usermodel/Range;)V

    .line 97
    invoke-virtual {p1}, Lorg/apache/index/poi/hwpf/model/PAPX;->getSprmBuf()Lorg/apache/index/poi/hwpf/sprm/SprmBuffer;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/index/poi/hwpf/usermodel/Paragraph;->_papx:Lorg/apache/index/poi/hwpf/sprm/SprmBuffer;

    .line 98
    invoke-virtual {p1}, Lorg/apache/index/poi/hwpf/model/PAPX;->getIstd()S

    move-result v0

    iput-short v0, p0, Lorg/apache/index/poi/hwpf/usermodel/Paragraph;->_istd:S

    .line 99
    return-void
.end method

.method protected constructor <init>(Lorg/apache/index/poi/hwpf/model/PAPX;Lorg/apache/index/poi/hwpf/usermodel/Range;I)V
    .locals 3
    .param p1, "papx"    # Lorg/apache/index/poi/hwpf/model/PAPX;
    .param p2, "parent"    # Lorg/apache/index/poi/hwpf/usermodel/Range;
    .param p3, "start"    # I

    .prologue
    .line 103
    iget v0, p2, Lorg/apache/index/poi/hwpf/usermodel/Range;->_start:I

    invoke-static {v0, p3}, Ljava/lang/Math;->max(II)I

    move-result v0

    iget v1, p2, Lorg/apache/index/poi/hwpf/usermodel/Range;->_end:I

    invoke-virtual {p1}, Lorg/apache/index/poi/hwpf/model/PAPX;->getEnd()I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-direct {p0, v0, v1, p2}, Lorg/apache/index/poi/hwpf/usermodel/Range;-><init>(IILorg/apache/index/poi/hwpf/usermodel/Range;)V

    .line 104
    invoke-virtual {p1}, Lorg/apache/index/poi/hwpf/model/PAPX;->getSprmBuf()Lorg/apache/index/poi/hwpf/sprm/SprmBuffer;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/index/poi/hwpf/usermodel/Paragraph;->_papx:Lorg/apache/index/poi/hwpf/sprm/SprmBuffer;

    .line 105
    invoke-virtual {p1}, Lorg/apache/index/poi/hwpf/model/PAPX;->getIstd()S

    move-result v0

    iput-short v0, p0, Lorg/apache/index/poi/hwpf/usermodel/Paragraph;->_istd:S

    .line 106
    return-void
.end method


# virtual methods
.method protected clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 120
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getStyleIndex()S
    .locals 1

    .prologue
    .line 110
    iget-short v0, p0, Lorg/apache/index/poi/hwpf/usermodel/Paragraph;->_istd:S

    return v0
.end method

.method public type()I
    .locals 1

    .prologue
    .line 115
    const/4 v0, 0x0

    return v0
.end method
