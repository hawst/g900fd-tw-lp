.class Lorg/apache/index/poi/hwpf/model/OldSectionTable$CharIsBytes;
.super Ljava/lang/Object;
.source "OldSectionTable.java"

# interfaces
.implements Lorg/apache/index/poi/hwpf/model/CharIndexTranslator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/index/poi/hwpf/model/OldSectionTable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "CharIsBytes"
.end annotation


# instance fields
.field private tpt:Lorg/apache/index/poi/hwpf/model/TextPieceTable;


# direct methods
.method private constructor <init>(Lorg/apache/index/poi/hwpf/model/TextPieceTable;)V
    .locals 0
    .param p1, "tpt"    # Lorg/apache/index/poi/hwpf/model/TextPieceTable;

    .prologue
    .line 74
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 75
    iput-object p1, p0, Lorg/apache/index/poi/hwpf/model/OldSectionTable$CharIsBytes;->tpt:Lorg/apache/index/poi/hwpf/model/TextPieceTable;

    .line 76
    return-void
.end method

.method synthetic constructor <init>(Lorg/apache/index/poi/hwpf/model/TextPieceTable;Lorg/apache/index/poi/hwpf/model/OldSectionTable$CharIsBytes;)V
    .locals 0

    .prologue
    .line 74
    invoke-direct {p0, p1}, Lorg/apache/index/poi/hwpf/model/OldSectionTable$CharIsBytes;-><init>(Lorg/apache/index/poi/hwpf/model/TextPieceTable;)V

    return-void
.end method


# virtual methods
.method public getCharIndex(I)I
    .locals 0
    .param p1, "bytePos"    # I

    .prologue
    .line 82
    return p1
.end method

.method public getCharIndex(II)I
    .locals 0
    .param p1, "bytePos"    # I
    .param p2, "startCP"    # I

    .prologue
    .line 79
    return p1
.end method

.method public isIndexInTable(I)Z
    .locals 1
    .param p1, "bytePos"    # I

    .prologue
    .line 86
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/OldSectionTable$CharIsBytes;->tpt:Lorg/apache/index/poi/hwpf/model/TextPieceTable;

    invoke-virtual {v0, p1}, Lorg/apache/index/poi/hwpf/model/TextPieceTable;->isIndexInTable(I)Z

    move-result v0

    return v0
.end method

.method public lookIndexBackward(I)I
    .locals 1
    .param p1, "bytePos"    # I

    .prologue
    .line 89
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/OldSectionTable$CharIsBytes;->tpt:Lorg/apache/index/poi/hwpf/model/TextPieceTable;

    invoke-virtual {v0, p1}, Lorg/apache/index/poi/hwpf/model/TextPieceTable;->lookIndexBackward(I)I

    move-result v0

    return v0
.end method

.method public lookIndexForward(I)I
    .locals 1
    .param p1, "bytePos"    # I

    .prologue
    .line 92
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/OldSectionTable$CharIsBytes;->tpt:Lorg/apache/index/poi/hwpf/model/TextPieceTable;

    invoke-virtual {v0, p1}, Lorg/apache/index/poi/hwpf/model/TextPieceTable;->lookIndexForward(I)I

    move-result v0

    return v0
.end method
