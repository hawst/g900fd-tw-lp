.class Lorg/apache/index/poi/ddf/EscherOptRecord$1;
.super Ljava/lang/Object;
.source "EscherOptRecord.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/index/poi/ddf/EscherOptRecord;->sortProperties()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/apache/index/poi/ddf/EscherOptRecord;


# direct methods
.method constructor <init>(Lorg/apache/index/poi/ddf/EscherOptRecord;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lorg/apache/index/poi/ddf/EscherOptRecord$1;->this$0:Lorg/apache/index/poi/ddf/EscherOptRecord;

    .line 155
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 4
    .param p1, "o1"    # Ljava/lang/Object;
    .param p2, "o2"    # Ljava/lang/Object;

    .prologue
    .line 159
    move-object v0, p1

    check-cast v0, Lorg/apache/index/poi/ddf/EscherProperty;

    .local v0, "p1":Lorg/apache/index/poi/ddf/EscherProperty;
    move-object v1, p2

    .line 160
    check-cast v1, Lorg/apache/index/poi/ddf/EscherProperty;

    .line 161
    .local v1, "p2":Lorg/apache/index/poi/ddf/EscherProperty;
    invoke-virtual {v0}, Lorg/apache/index/poi/ddf/EscherProperty;->getPropertyNumber()S

    move-result v2

    invoke-static {v2}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v2

    invoke-virtual {v1}, Lorg/apache/index/poi/ddf/EscherProperty;->getPropertyNumber()S

    move-result v3

    invoke-static {v3}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Short;->compareTo(Ljava/lang/Short;)I

    move-result v2

    return v2
.end method
