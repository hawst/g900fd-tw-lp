.class public final Lorg/apache/index/poi/hslf/record/Comment2000Atom;
.super Lorg/apache/index/poi/hslf/record/RecordAtom;
.source "Comment2000Atom.java"


# instance fields
.field private _data:[B

.field private _header:[B


# direct methods
.method protected constructor <init>()V
    .locals 4

    .prologue
    .line 48
    invoke-direct {p0}, Lorg/apache/index/poi/hslf/record/RecordAtom;-><init>()V

    .line 49
    const/16 v0, 0x8

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/apache/index/poi/hslf/record/Comment2000Atom;->_header:[B

    .line 50
    const/16 v0, 0x1c

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/apache/index/poi/hslf/record/Comment2000Atom;->_data:[B

    .line 52
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/Comment2000Atom;->_header:[B

    const/4 v1, 0x2

    invoke-virtual {p0}, Lorg/apache/index/poi/hslf/record/Comment2000Atom;->getRecordType()J

    move-result-wide v2

    long-to-int v2, v2

    int-to-short v2, v2

    invoke-static {v0, v1, v2}, Lorg/apache/index/poi/util/LittleEndian;->putShort([BIS)V

    .line 53
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/Comment2000Atom;->_header:[B

    const/4 v1, 0x4

    iget-object v2, p0, Lorg/apache/index/poi/hslf/record/Comment2000Atom;->_data:[B

    array-length v2, v2

    invoke-static {v0, v1, v2}, Lorg/apache/index/poi/util/LittleEndian;->putInt([BII)V

    .line 56
    return-void
.end method

.method protected constructor <init>([BII)V
    .locals 4
    .param p1, "source"    # [B
    .param p2, "start"    # I
    .param p3, "len"    # I

    .prologue
    const/16 v1, 0x8

    const/4 v3, 0x0

    .line 65
    invoke-direct {p0}, Lorg/apache/index/poi/hslf/record/RecordAtom;-><init>()V

    .line 67
    new-array v0, v1, [B

    iput-object v0, p0, Lorg/apache/index/poi/hslf/record/Comment2000Atom;->_header:[B

    .line 68
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/Comment2000Atom;->_header:[B

    invoke-static {p1, p2, v0, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 71
    add-int/lit8 v0, p3, -0x8

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/apache/index/poi/hslf/record/Comment2000Atom;->_data:[B

    .line 72
    add-int/lit8 v0, p2, 0x8

    iget-object v1, p0, Lorg/apache/index/poi/hslf/record/Comment2000Atom;->_data:[B

    add-int/lit8 v2, p3, -0x8

    invoke-static {p1, v0, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 73
    return-void
.end method


# virtual methods
.method public getDate()Ljava/util/Date;
    .locals 2

    .prologue
    .line 96
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/Comment2000Atom;->_data:[B

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lorg/apache/index/poi/hslf/util/SystemTimeUtils;->getDate([BI)Ljava/util/Date;

    move-result-object v0

    return-object v0
.end method

.method public getNumber()I
    .locals 2

    .prologue
    .line 80
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/Comment2000Atom;->_data:[B

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lorg/apache/index/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    return v0
.end method

.method public getRecordType()J
    .locals 2

    .prologue
    .line 143
    sget-object v0, Lorg/apache/index/poi/hslf/record/RecordTypes;->Comment2000Atom:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    iget v0, v0, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;->typeID:I

    int-to-long v0, v0

    return-wide v0
.end method

.method public getXOffset()I
    .locals 2

    .prologue
    .line 112
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/Comment2000Atom;->_data:[B

    const/16 v1, 0x14

    invoke-static {v0, v1}, Lorg/apache/index/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    return v0
.end method

.method public getYOffset()I
    .locals 2

    .prologue
    .line 128
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/Comment2000Atom;->_data:[B

    const/16 v1, 0x18

    invoke-static {v0, v1}, Lorg/apache/index/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    return v0
.end method

.method public setDate(Ljava/util/Date;)V
    .locals 2
    .param p1, "date"    # Ljava/util/Date;

    .prologue
    .line 104
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/Comment2000Atom;->_data:[B

    const/4 v1, 0x4

    invoke-static {p1, v0, v1}, Lorg/apache/index/poi/hslf/util/SystemTimeUtils;->storeDate(Ljava/util/Date;[BI)V

    .line 105
    return-void
.end method

.method public setNumber(I)V
    .locals 2
    .param p1, "number"    # I

    .prologue
    .line 88
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/Comment2000Atom;->_data:[B

    const/4 v1, 0x0

    invoke-static {v0, v1, p1}, Lorg/apache/index/poi/util/LittleEndian;->putInt([BII)V

    .line 89
    return-void
.end method

.method public setXOffset(I)V
    .locals 2
    .param p1, "xOffset"    # I

    .prologue
    .line 120
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/Comment2000Atom;->_data:[B

    const/16 v1, 0x14

    invoke-static {v0, v1, p1}, Lorg/apache/index/poi/util/LittleEndian;->putInt([BII)V

    .line 121
    return-void
.end method

.method public setYOffset(I)V
    .locals 2
    .param p1, "yOffset"    # I

    .prologue
    .line 136
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/Comment2000Atom;->_data:[B

    const/16 v1, 0x18

    invoke-static {v0, v1, p1}, Lorg/apache/index/poi/util/LittleEndian;->putInt([BII)V

    .line 137
    return-void
.end method

.method public writeOut(Ljava/io/OutputStream;)V
    .locals 1
    .param p1, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 153
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/Comment2000Atom;->_header:[B

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 154
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/Comment2000Atom;->_data:[B

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 155
    return-void
.end method
