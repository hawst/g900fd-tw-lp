.class public Lorg/apache/index/poi/hpsf/VariantSupport;
.super Lorg/apache/index/poi/hpsf/Variant;
.source "VariantSupport.java"


# static fields
.field public static final SUPPORTED_TYPES:[I

.field private static logUnsupportedTypes:Z

.field protected static unsupportedMessage:Ljava/util/List;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x5

    const/4 v3, 0x3

    const/4 v2, 0x2

    .line 52
    const/4 v0, 0x0

    sput-boolean v0, Lorg/apache/index/poi/hpsf/VariantSupport;->logUnsupportedTypes:Z

    .line 113
    const/16 v0, 0xa

    new-array v0, v0, [I

    const/4 v1, 0x1

    .line 114
    aput v2, v0, v1

    aput v3, v0, v2

    const/16 v1, 0x14

    aput v1, v0, v3

    const/4 v1, 0x4

    aput v4, v0, v1

    .line 115
    const/16 v1, 0x40

    aput v1, v0, v4

    const/4 v1, 0x6

    const/16 v2, 0x1e

    aput v2, v0, v1

    const/4 v1, 0x7

    const/16 v2, 0x1f

    aput v2, v0, v1

    const/16 v1, 0x8

    .line 116
    const/16 v2, 0x47

    aput v2, v0, v1

    const/16 v1, 0x9

    const/16 v2, 0xb

    aput v2, v0, v1

    .line 113
    sput-object v0, Lorg/apache/index/poi/hpsf/VariantSupport;->SUPPORTED_TYPES:[I

    .line 116
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0}, Lorg/apache/index/poi/hpsf/Variant;-><init>()V

    return-void
.end method

.method public static codepageToEncoding(I)Ljava/lang/String;
    .locals 3
    .param p0, "codepage"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/UnsupportedEncodingException;
        }
    .end annotation

    .prologue
    .line 337
    if-gtz p0, :cond_0

    .line 338
    new-instance v0, Ljava/io/UnsupportedEncodingException;

    .line 339
    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Codepage number may not be "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 338
    invoke-direct {v0, v1}, Ljava/io/UnsupportedEncodingException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 340
    :cond_0
    sparse-switch p0, :sswitch_data_0

    .line 446
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "cp"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 343
    :sswitch_0
    const-string/jumbo v0, "UTF-16"

    goto :goto_0

    .line 345
    :sswitch_1
    const-string/jumbo v0, "UTF-16BE"

    goto :goto_0

    .line 347
    :sswitch_2
    const-string/jumbo v0, "UTF-8"

    goto :goto_0

    .line 349
    :sswitch_3
    const-string/jumbo v0, "cp037"

    goto :goto_0

    .line 351
    :sswitch_4
    const-string/jumbo v0, "GBK"

    goto :goto_0

    .line 353
    :sswitch_5
    const-string/jumbo v0, "ms949"

    goto :goto_0

    .line 355
    :sswitch_6
    const-string/jumbo v0, "windows-1250"

    goto :goto_0

    .line 357
    :sswitch_7
    const-string/jumbo v0, "windows-1251"

    goto :goto_0

    .line 359
    :sswitch_8
    const-string/jumbo v0, "windows-1252"

    goto :goto_0

    .line 361
    :sswitch_9
    const-string/jumbo v0, "windows-1253"

    goto :goto_0

    .line 363
    :sswitch_a
    const-string/jumbo v0, "windows-1254"

    goto :goto_0

    .line 365
    :sswitch_b
    const-string/jumbo v0, "windows-1255"

    goto :goto_0

    .line 367
    :sswitch_c
    const-string/jumbo v0, "windows-1256"

    goto :goto_0

    .line 369
    :sswitch_d
    const-string/jumbo v0, "windows-1257"

    goto :goto_0

    .line 371
    :sswitch_e
    const-string/jumbo v0, "windows-1258"

    goto :goto_0

    .line 373
    :sswitch_f
    const-string/jumbo v0, "johab"

    goto :goto_0

    .line 375
    :sswitch_10
    const-string/jumbo v0, "MacRoman"

    goto :goto_0

    .line 377
    :sswitch_11
    const-string/jumbo v0, "SJIS"

    goto :goto_0

    .line 379
    :sswitch_12
    const-string/jumbo v0, "Big5"

    goto :goto_0

    .line 381
    :sswitch_13
    const-string/jumbo v0, "EUC-KR"

    goto :goto_0

    .line 383
    :sswitch_14
    const-string/jumbo v0, "MacArabic"

    goto :goto_0

    .line 385
    :sswitch_15
    const-string/jumbo v0, "MacHebrew"

    goto :goto_0

    .line 387
    :sswitch_16
    const-string/jumbo v0, "MacGreek"

    goto :goto_0

    .line 389
    :sswitch_17
    const-string/jumbo v0, "MacCyrillic"

    goto :goto_0

    .line 391
    :sswitch_18
    const-string/jumbo v0, "EUC_CN"

    goto :goto_0

    .line 393
    :sswitch_19
    const-string/jumbo v0, "MacRomania"

    goto :goto_0

    .line 395
    :sswitch_1a
    const-string/jumbo v0, "MacUkraine"

    goto :goto_0

    .line 397
    :sswitch_1b
    const-string/jumbo v0, "MacThai"

    goto :goto_0

    .line 399
    :sswitch_1c
    const-string/jumbo v0, "MacCentralEurope"

    goto :goto_0

    .line 401
    :sswitch_1d
    const-string/jumbo v0, "MacIceland"

    goto :goto_0

    .line 403
    :sswitch_1e
    const-string/jumbo v0, "MacTurkish"

    goto :goto_0

    .line 405
    :sswitch_1f
    const-string/jumbo v0, "MacCroatian"

    goto :goto_0

    .line 408
    :sswitch_20
    const-string/jumbo v0, "US-ASCII"

    goto/16 :goto_0

    .line 410
    :sswitch_21
    const-string/jumbo v0, "KOI8-R"

    goto/16 :goto_0

    .line 412
    :sswitch_22
    const-string/jumbo v0, "ISO-8859-1"

    goto/16 :goto_0

    .line 414
    :sswitch_23
    const-string/jumbo v0, "ISO-8859-2"

    goto/16 :goto_0

    .line 416
    :sswitch_24
    const-string/jumbo v0, "ISO-8859-3"

    goto/16 :goto_0

    .line 418
    :sswitch_25
    const-string/jumbo v0, "ISO-8859-4"

    goto/16 :goto_0

    .line 420
    :sswitch_26
    const-string/jumbo v0, "ISO-8859-5"

    goto/16 :goto_0

    .line 422
    :sswitch_27
    const-string/jumbo v0, "ISO-8859-6"

    goto/16 :goto_0

    .line 424
    :sswitch_28
    const-string/jumbo v0, "ISO-8859-7"

    goto/16 :goto_0

    .line 426
    :sswitch_29
    const-string/jumbo v0, "ISO-8859-8"

    goto/16 :goto_0

    .line 428
    :sswitch_2a
    const-string/jumbo v0, "ISO-8859-9"

    goto/16 :goto_0

    .line 432
    :sswitch_2b
    const-string/jumbo v0, "ISO-2022-JP"

    goto/16 :goto_0

    .line 434
    :sswitch_2c
    const-string/jumbo v0, "ISO-2022-KR"

    goto/16 :goto_0

    .line 436
    :sswitch_2d
    const-string/jumbo v0, "EUC-JP"

    goto/16 :goto_0

    .line 438
    :sswitch_2e
    const-string/jumbo v0, "EUC-KR"

    goto/16 :goto_0

    .line 440
    :sswitch_2f
    const-string/jumbo v0, "GB2312"

    goto/16 :goto_0

    .line 442
    :sswitch_30
    const-string/jumbo v0, "GB18030"

    goto/16 :goto_0

    .line 444
    :sswitch_31
    const-string/jumbo v0, "SJIS"

    goto/16 :goto_0

    .line 340
    :sswitch_data_0
    .sparse-switch
        0x25 -> :sswitch_3
        0x3a4 -> :sswitch_31
        0x3a8 -> :sswitch_4
        0x3b5 -> :sswitch_5
        0x4b0 -> :sswitch_0
        0x4b1 -> :sswitch_1
        0x4e2 -> :sswitch_6
        0x4e3 -> :sswitch_7
        0x4e4 -> :sswitch_8
        0x4e5 -> :sswitch_9
        0x4e6 -> :sswitch_a
        0x4e7 -> :sswitch_b
        0x4e8 -> :sswitch_c
        0x4e9 -> :sswitch_d
        0x4ea -> :sswitch_e
        0x551 -> :sswitch_f
        0x2710 -> :sswitch_10
        0x2711 -> :sswitch_11
        0x2712 -> :sswitch_12
        0x2713 -> :sswitch_13
        0x2714 -> :sswitch_14
        0x2715 -> :sswitch_15
        0x2716 -> :sswitch_16
        0x2717 -> :sswitch_17
        0x2718 -> :sswitch_18
        0x271a -> :sswitch_19
        0x2721 -> :sswitch_1a
        0x2725 -> :sswitch_1b
        0x272d -> :sswitch_1c
        0x275f -> :sswitch_1d
        0x2761 -> :sswitch_1e
        0x2762 -> :sswitch_1f
        0x4e9f -> :sswitch_20
        0x5182 -> :sswitch_21
        0x6faf -> :sswitch_22
        0x6fb0 -> :sswitch_23
        0x6fb1 -> :sswitch_24
        0x6fb2 -> :sswitch_25
        0x6fb3 -> :sswitch_26
        0x6fb4 -> :sswitch_27
        0x6fb5 -> :sswitch_28
        0x6fb6 -> :sswitch_29
        0x6fb7 -> :sswitch_2a
        0xc42c -> :sswitch_2b
        0xc42d -> :sswitch_2b
        0xc42e -> :sswitch_2b
        0xc431 -> :sswitch_2c
        0xcadc -> :sswitch_2d
        0xcaed -> :sswitch_2e
        0xcec8 -> :sswitch_2f
        0xd698 -> :sswitch_30
        0xfde8 -> :sswitch_20
        0xfde9 -> :sswitch_2
    .end sparse-switch
.end method

.method public static isLogUnsupportedTypes()Z
    .locals 1

    .prologue
    .line 75
    sget-boolean v0, Lorg/apache/index/poi/hpsf/VariantSupport;->logUnsupportedTypes:Z

    return v0
.end method

.method public static read([BIIJI)Ljava/lang/Object;
    .locals 33
    .param p0, "src"    # [B
    .param p1, "offset"    # I
    .param p2, "length"    # I
    .param p3, "type"    # J
    .param p5, "codepage"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/index/poi/hpsf/ReadingNotSupportedException;,
            Ljava/io/UnsupportedEncodingException;
        }
    .end annotation

    .prologue
    .line 163
    move/from16 v19, p1

    .line 164
    .local v19, "o1":I
    add-int/lit8 v18, p2, -0x4

    .line 165
    .local v18, "l1":I
    move-wide/from16 v20, p3

    .line 169
    .local v20, "lType":J
    const/16 v28, 0x4b0

    move/from16 v0, p5

    move/from16 v1, v28

    if-ne v0, v1, :cond_0

    const-wide/16 v28, 0x1e

    cmp-long v28, p3, v28

    if-nez v28, :cond_0

    .line 170
    const-wide/16 v20, 0x1f

    .line 172
    :cond_0
    move-wide/from16 v0, v20

    long-to-int v0, v0

    move/from16 v28, v0

    sparse-switch v28, :sswitch_data_0

    .line 309
    move/from16 v0, v18

    new-array v0, v0, [B

    move-object/from16 v26, v0

    .line 310
    .local v26, "v":[B
    const/4 v11, 0x0

    .local v11, "i":I
    :goto_0
    move/from16 v0, v18

    if-lt v11, v0, :cond_9

    .line 312
    new-instance v28, Lorg/apache/index/poi/hpsf/ReadingNotSupportedException;

    move-object/from16 v0, v28

    move-wide/from16 v1, p3

    move-object/from16 v3, v26

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/index/poi/hpsf/ReadingNotSupportedException;-><init>(JLjava/lang/Object;)V

    throw v28

    .line 176
    .end local v11    # "i":I
    .end local v26    # "v":[B
    :sswitch_0
    const/16 v27, 0x0

    .line 315
    :goto_1
    return-object v27

    .line 185
    :sswitch_1
    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-static {v0, v1}, Lorg/apache/index/poi/util/LittleEndian;->getShort([BI)S

    move-result v28

    invoke-static/range {v28 .. v28}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v27

    .line 186
    .local v27, "value":Ljava/lang/Integer;
    goto :goto_1

    .line 194
    .end local v27    # "value":Ljava/lang/Integer;
    :sswitch_2
    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-static {v0, v1}, Lorg/apache/index/poi/util/LittleEndian;->getInt([BI)I

    move-result v28

    invoke-static/range {v28 .. v28}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v27

    .line 195
    .restart local v27    # "value":Ljava/lang/Integer;
    goto :goto_1

    .line 203
    .end local v27    # "value":Ljava/lang/Integer;
    :sswitch_3
    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-static {v0, v1}, Lorg/apache/index/poi/util/LittleEndian;->getLong([BI)J

    move-result-wide v28

    invoke-static/range {v28 .. v29}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v27

    .line 204
    .local v27, "value":Ljava/lang/Long;
    goto :goto_1

    .line 212
    .end local v27    # "value":Ljava/lang/Long;
    :sswitch_4
    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-static {v0, v1}, Lorg/apache/index/poi/util/LittleEndian;->getDouble([BI)D

    move-result-wide v28

    invoke-static/range {v28 .. v29}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v27

    .line 213
    .local v27, "value":Ljava/lang/Double;
    goto :goto_1

    .line 221
    .end local v27    # "value":Ljava/lang/Double;
    :sswitch_5
    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-static {v0, v1}, Lorg/apache/index/poi/util/LittleEndian;->getUInt([BI)J

    move-result-wide v24

    .line 222
    .local v24, "low":J
    add-int/lit8 v19, v19, 0x4

    .line 223
    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-static {v0, v1}, Lorg/apache/index/poi/util/LittleEndian;->getUInt([BI)J

    move-result-wide v12

    .line 224
    .local v12, "high":J
    long-to-int v0, v12

    move/from16 v28, v0

    move-wide/from16 v0, v24

    long-to-int v0, v0

    move/from16 v29, v0

    invoke-static/range {v28 .. v29}, Lorg/apache/index/poi/hpsf/Util;->filetimeToDate(II)Ljava/util/Date;

    move-result-object v27

    .line 225
    .local v27, "value":Ljava/util/Date;
    goto :goto_1

    .line 234
    .end local v12    # "high":J
    .end local v24    # "low":J
    .end local v27    # "value":Ljava/util/Date;
    :sswitch_6
    add-int/lit8 v10, v19, 0x4

    .line 235
    .local v10, "first":I
    int-to-long v0, v10

    move-wide/from16 v28, v0

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-static {v0, v1}, Lorg/apache/index/poi/util/LittleEndian;->getUInt([BI)J

    move-result-wide v30

    add-long v28, v28, v30

    const-wide/16 v30, 0x1

    sub-long v22, v28, v30

    .line 236
    .local v22, "last":J
    add-int/lit8 v19, v19, 0x4

    .line 237
    :goto_2
    move-wide/from16 v0, v22

    long-to-int v0, v0

    move/from16 v28, v0

    aget-byte v28, p0, v28

    if-nez v28, :cond_1

    int-to-long v0, v10

    move-wide/from16 v28, v0

    cmp-long v28, v28, v22

    if-lez v28, :cond_2

    .line 239
    :cond_1
    int-to-long v0, v10

    move-wide/from16 v28, v0

    sub-long v28, v22, v28

    const-wide/16 v30, 0x1

    add-long v28, v28, v30

    move-wide/from16 v0, v28

    long-to-int v0, v0

    move/from16 v16, v0

    .line 240
    .local v16, "l":I
    const/16 v28, -0x1

    move/from16 v0, p5

    move/from16 v1, v28

    if-eq v0, v1, :cond_3

    .line 241
    new-instance v27, Ljava/lang/String;

    .line 242
    invoke-static/range {p5 .. p5}, Lorg/apache/index/poi/hpsf/VariantSupport;->codepageToEncoding(I)Ljava/lang/String;

    move-result-object v28

    .line 241
    move-object/from16 v0, v27

    move-object/from16 v1, p0

    move/from16 v2, v16

    move-object/from16 v3, v28

    invoke-direct {v0, v1, v10, v2, v3}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V

    .line 244
    .local v27, "value":Ljava/lang/String;
    :goto_3
    goto/16 :goto_1

    .line 238
    .end local v16    # "l":I
    .end local v27    # "value":Ljava/lang/String;
    :cond_2
    const-wide/16 v28, 0x1

    sub-long v22, v22, v28

    goto :goto_2

    .line 243
    .restart local v16    # "l":I
    :cond_3
    new-instance v27, Ljava/lang/String;

    move-object/from16 v0, v27

    move-object/from16 v1, p0

    move/from16 v2, v16

    invoke-direct {v0, v1, v10, v2}, Ljava/lang/String;-><init>([BII)V

    goto :goto_3

    .line 253
    .end local v10    # "first":I
    .end local v16    # "l":I
    .end local v22    # "last":J
    :sswitch_7
    add-int/lit8 v10, v19, 0x4

    .line 254
    .restart local v10    # "first":I
    int-to-long v0, v10

    move-wide/from16 v28, v0

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-static {v0, v1}, Lorg/apache/index/poi/util/LittleEndian;->getUInt([BI)J

    move-result-wide v30

    add-long v28, v28, v30

    const-wide/16 v30, 0x1

    sub-long v22, v28, v30

    .line 255
    .restart local v22    # "last":J
    int-to-long v0, v10

    move-wide/from16 v28, v0

    sub-long v16, v22, v28

    .line 256
    .local v16, "l":J
    add-int/lit8 v19, v19, 0x4

    .line 257
    new-instance v6, Ljava/lang/StringBuffer;

    int-to-long v0, v10

    move-wide/from16 v28, v0

    sub-long v28, v22, v28

    move-wide/from16 v0, v28

    long-to-int v0, v0

    move/from16 v28, v0

    move/from16 v0, v28

    invoke-direct {v6, v0}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 258
    .local v6, "b":Ljava/lang/StringBuffer;
    const/4 v11, 0x0

    .restart local v11    # "i":I
    :goto_4
    int-to-long v0, v11

    move-wide/from16 v28, v0

    cmp-long v28, v28, v16

    if-lez v28, :cond_5

    .line 268
    :goto_5
    invoke-virtual {v6}, Ljava/lang/StringBuffer;->length()I

    move-result v28

    if-lez v28, :cond_4

    invoke-virtual {v6}, Ljava/lang/StringBuffer;->length()I

    move-result v28

    add-int/lit8 v28, v28, -0x1

    move/from16 v0, v28

    invoke-virtual {v6, v0}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v28

    if-eqz v28, :cond_6

    .line 270
    :cond_4
    invoke-virtual {v6}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v27

    .line 271
    .restart local v27    # "value":Ljava/lang/String;
    goto/16 :goto_1

    .line 260
    .end local v27    # "value":Ljava/lang/String;
    :cond_5
    mul-int/lit8 v28, v11, 0x2

    add-int v14, v19, v28

    .line 261
    .local v14, "i1":I
    add-int/lit8 v15, v14, 0x1

    .line 262
    .local v15, "i2":I
    aget-byte v28, p0, v15

    shl-int/lit8 v12, v28, 0x8

    .line 263
    .local v12, "high":I
    aget-byte v28, p0, v14

    move/from16 v0, v28

    and-int/lit16 v0, v0, 0xff

    move/from16 v24, v0

    .line 264
    .local v24, "low":I
    or-int v28, v12, v24

    move/from16 v0, v28

    int-to-char v7, v0

    .line 265
    .local v7, "c":C
    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 258
    add-int/lit8 v11, v11, 0x1

    goto :goto_4

    .line 269
    .end local v7    # "c":C
    .end local v12    # "high":I
    .end local v14    # "i1":I
    .end local v15    # "i2":I
    .end local v24    # "low":I
    :cond_6
    invoke-virtual {v6}, Ljava/lang/StringBuffer;->length()I

    move-result v28

    add-int/lit8 v28, v28, -0x1

    move/from16 v0, v28

    invoke-virtual {v6, v0}, Ljava/lang/StringBuffer;->setLength(I)V

    goto :goto_5

    .line 275
    .end local v6    # "b":Ljava/lang/StringBuffer;
    .end local v10    # "first":I
    .end local v11    # "i":I
    .end local v16    # "l":J
    .end local v22    # "last":J
    :sswitch_8
    if-gez v18, :cond_7

    .line 285
    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-static {v0, v1}, Lorg/apache/index/poi/util/LittleEndian;->getInt([BI)I

    move-result v18

    add-int/lit8 v19, v19, 0x4

    .line 287
    :cond_7
    move/from16 v0, v18

    new-array v0, v0, [B

    move-object/from16 v26, v0

    .line 288
    .restart local v26    # "v":[B
    const/16 v28, 0x0

    move-object/from16 v0, v26

    array-length v0, v0

    move/from16 v29, v0

    move-object/from16 v0, p0

    move/from16 v1, v19

    move-object/from16 v2, v26

    move/from16 v3, v28

    move/from16 v4, v29

    invoke-static {v0, v1, v2, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 289
    move-object/from16 v27, v26

    .line 290
    .local v27, "value":[B
    goto/16 :goto_1

    .line 300
    .end local v26    # "v":[B
    .end local v27    # "value":[B
    :sswitch_9
    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-static {v0, v1}, Lorg/apache/index/poi/util/LittleEndian;->getUInt([BI)J

    move-result-wide v8

    .line 301
    .local v8, "bool":J
    const-wide/16 v28, 0x0

    cmp-long v28, v8, v28

    if-eqz v28, :cond_8

    .line 302
    sget-object v27, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    .local v27, "value":Ljava/lang/Boolean;
    goto/16 :goto_1

    .line 304
    .end local v27    # "value":Ljava/lang/Boolean;
    :cond_8
    sget-object v27, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    .line 305
    .restart local v27    # "value":Ljava/lang/Boolean;
    goto/16 :goto_1

    .line 311
    .end local v8    # "bool":J
    .end local v27    # "value":Ljava/lang/Boolean;
    .restart local v11    # "i":I
    .restart local v26    # "v":[B
    :cond_9
    add-int v28, v19, v11

    aget-byte v28, p0, v28

    aput-byte v28, v26, v11

    .line 310
    add-int/lit8 v11, v11, 0x1

    goto/16 :goto_0

    .line 172
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x2 -> :sswitch_1
        0x3 -> :sswitch_2
        0x5 -> :sswitch_4
        0xb -> :sswitch_9
        0x14 -> :sswitch_3
        0x1e -> :sswitch_6
        0x1f -> :sswitch_7
        0x40 -> :sswitch_5
        0x47 -> :sswitch_8
    .end sparse-switch
.end method

.method public static setLogUnsupportedTypes(Z)V
    .locals 0
    .param p0, "logUnsupportedTypes"    # Z

    .prologue
    .line 63
    sput-boolean p0, Lorg/apache/index/poi/hpsf/VariantSupport;->logUnsupportedTypes:Z

    .line 64
    return-void
.end method

.method public static write(Ljava/io/OutputStream;JLjava/lang/Object;I)I
    .locals 23
    .param p0, "out"    # Ljava/io/OutputStream;
    .param p1, "type"    # J
    .param p3, "value"    # Ljava/lang/Object;
    .param p4, "codepage"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/apache/index/poi/hpsf/WritingNotSupportedException;
        }
    .end annotation

    .prologue
    .line 474
    const/4 v11, 0x0

    .line 475
    .local v11, "length":I
    move-wide/from16 v0, p1

    long-to-int v0, v0

    move/from16 v17, v0

    sparse-switch v17, :sswitch_data_0

    .line 580
    move-object/from16 v0, p3

    instance-of v0, v0, [B

    move/from16 v17, v0

    if-eqz v17, :cond_4

    move-object/from16 v4, p3

    .line 582
    check-cast v4, [B

    .line 583
    .local v4, "b":[B
    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Ljava/io/OutputStream;->write([B)V

    .line 584
    array-length v11, v4

    .line 586
    new-instance v17, Lorg/apache/index/poi/hpsf/WritingNotSupportedException;

    move-object/from16 v0, v17

    move-wide/from16 v1, p1

    move-object/from16 v3, p3

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/index/poi/hpsf/WritingNotSupportedException;-><init>(JLjava/lang/Object;)V

    .line 585
    invoke-static/range {v17 .. v17}, Lorg/apache/index/poi/hpsf/VariantSupport;->writeUnsupportedTypeMessage(Lorg/apache/index/poi/hpsf/UnsupportedVariantTypeException;)V

    .line 594
    .end local v4    # "b":[B
    .end local p3    # "value":Ljava/lang/Object;
    :goto_0
    return v11

    .line 480
    .restart local p3    # "value":Ljava/lang/Object;
    :sswitch_0
    check-cast p3, Ljava/lang/Boolean;

    .end local p3    # "value":Ljava/lang/Object;
    invoke-virtual/range {p3 .. p3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v17

    if-eqz v17, :cond_0

    .line 481
    const/16 v16, 0x1

    .line 484
    .local v16, "trueOrFalse":I
    :goto_1
    move/from16 v0, v16

    int-to-long v0, v0

    move-wide/from16 v18, v0

    move-object/from16 v0, p0

    move-wide/from16 v1, v18

    invoke-static {v0, v1, v2}, Lorg/apache/index/poi/hpsf/TypeWriter;->writeUIntToStream(Ljava/io/OutputStream;J)I

    move-result v11

    .line 485
    goto :goto_0

    .line 483
    .end local v16    # "trueOrFalse":I
    :cond_0
    const/16 v16, 0x0

    .restart local v16    # "trueOrFalse":I
    goto :goto_1

    .line 490
    .end local v16    # "trueOrFalse":I
    .restart local p3    # "value":Ljava/lang/Object;
    :sswitch_1
    const/16 v17, -0x1

    move/from16 v0, p4

    move/from16 v1, v17

    if-ne v0, v1, :cond_1

    .line 491
    check-cast p3, Ljava/lang/String;

    .end local p3    # "value":Ljava/lang/Object;
    invoke-virtual/range {p3 .. p3}, Ljava/lang/String;->getBytes()[B

    move-result-object v5

    .line 493
    .local v5, "bytes":[B
    :goto_2
    array-length v0, v5

    move/from16 v17, v0

    add-int/lit8 v17, v17, 0x1

    move/from16 v0, v17

    int-to-long v0, v0

    move-wide/from16 v18, v0

    move-object/from16 v0, p0

    move-wide/from16 v1, v18

    invoke-static {v0, v1, v2}, Lorg/apache/index/poi/hpsf/TypeWriter;->writeUIntToStream(Ljava/io/OutputStream;J)I

    move-result v11

    .line 494
    array-length v0, v5

    move/from16 v17, v0

    add-int/lit8 v17, v17, 0x1

    move/from16 v0, v17

    new-array v4, v0, [B

    .line 495
    .restart local v4    # "b":[B
    const/16 v17, 0x0

    const/16 v18, 0x0

    array-length v0, v5

    move/from16 v19, v0

    move/from16 v0, v17

    move/from16 v1, v18

    move/from16 v2, v19

    invoke-static {v5, v0, v4, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 496
    array-length v0, v4

    move/from16 v17, v0

    add-int/lit8 v17, v17, -0x1

    const/16 v18, 0x0

    aput-byte v18, v4, v17

    .line 497
    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Ljava/io/OutputStream;->write([B)V

    .line 498
    array-length v0, v4

    move/from16 v17, v0

    add-int v11, v11, v17

    .line 499
    goto :goto_0

    .line 492
    .end local v4    # "b":[B
    .end local v5    # "bytes":[B
    .restart local p3    # "value":Ljava/lang/Object;
    :cond_1
    check-cast p3, Ljava/lang/String;

    .end local p3    # "value":Ljava/lang/Object;
    invoke-static/range {p4 .. p4}, Lorg/apache/index/poi/hpsf/VariantSupport;->codepageToEncoding(I)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, p3

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v5

    goto :goto_2

    .restart local p3    # "value":Ljava/lang/Object;
    :sswitch_2
    move-object/from16 v17, p3

    .line 503
    check-cast v17, Ljava/lang/String;

    invoke-virtual/range {v17 .. v17}, Ljava/lang/String;->length()I

    move-result v17

    add-int/lit8 v14, v17, 0x1

    .line 504
    .local v14, "nrOfChars":I
    int-to-long v0, v14

    move-wide/from16 v18, v0

    move-object/from16 v0, p0

    move-wide/from16 v1, v18

    invoke-static {v0, v1, v2}, Lorg/apache/index/poi/hpsf/TypeWriter;->writeUIntToStream(Ljava/io/OutputStream;J)I

    move-result v17

    add-int v11, v11, v17

    .line 505
    check-cast p3, Ljava/lang/String;

    .end local p3    # "value":Ljava/lang/Object;
    invoke-static/range {p3 .. p3}, Lorg/apache/index/poi/hpsf/Util;->pad4(Ljava/lang/String;)[C

    move-result-object v15

    .line 506
    .local v15, "s":[C
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_3
    array-length v0, v15

    move/from16 v17, v0

    move/from16 v0, v17

    if-lt v10, v0, :cond_2

    .line 516
    const/16 v17, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/io/OutputStream;->write(I)V

    .line 517
    const/16 v17, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/io/OutputStream;->write(I)V

    .line 518
    add-int/lit8 v11, v11, 0x2

    .line 519
    goto/16 :goto_0

    .line 508
    :cond_2
    aget-char v17, v15, v10

    const v18, 0xff00

    and-int v17, v17, v18

    shr-int/lit8 v8, v17, 0x8

    .line 509
    .local v8, "high":I
    aget-char v17, v15, v10

    move/from16 v0, v17

    and-int/lit16 v12, v0, 0xff

    .line 510
    .local v12, "low":I
    int-to-byte v9, v8

    .line 511
    .local v9, "highb":B
    int-to-byte v13, v12

    .line 512
    .local v13, "lowb":B
    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Ljava/io/OutputStream;->write(I)V

    .line 513
    move-object/from16 v0, p0

    invoke-virtual {v0, v9}, Ljava/io/OutputStream;->write(I)V

    .line 514
    add-int/lit8 v11, v11, 0x2

    .line 506
    add-int/lit8 v10, v10, 0x1

    goto :goto_3

    .end local v8    # "high":I
    .end local v9    # "highb":B
    .end local v10    # "i":I
    .end local v12    # "low":I
    .end local v13    # "lowb":B
    .end local v14    # "nrOfChars":I
    .end local v15    # "s":[C
    .restart local p3    # "value":Ljava/lang/Object;
    :sswitch_3
    move-object/from16 v4, p3

    .line 523
    check-cast v4, [B

    .line 524
    .restart local v4    # "b":[B
    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Ljava/io/OutputStream;->write([B)V

    .line 525
    array-length v11, v4

    .line 526
    goto/16 :goto_0

    .line 530
    .end local v4    # "b":[B
    :sswitch_4
    const-wide/16 v18, 0x0

    move-object/from16 v0, p0

    move-wide/from16 v1, v18

    invoke-static {v0, v1, v2}, Lorg/apache/index/poi/hpsf/TypeWriter;->writeUIntToStream(Ljava/io/OutputStream;J)I

    .line 531
    const/4 v11, 0x4

    .line 532
    goto/16 :goto_0

    .line 536
    :sswitch_5
    check-cast p3, Ljava/lang/Integer;

    .end local p3    # "value":Ljava/lang/Object;
    invoke-virtual/range {p3 .. p3}, Ljava/lang/Integer;->shortValue()S

    move-result v17

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-static {v0, v1}, Lorg/apache/index/poi/hpsf/TypeWriter;->writeToStream(Ljava/io/OutputStream;S)I

    .line 537
    const/4 v11, 0x2

    .line 538
    goto/16 :goto_0

    .line 542
    .restart local p3    # "value":Ljava/lang/Object;
    :sswitch_6
    move-object/from16 v0, p3

    instance-of v0, v0, Ljava/lang/Integer;

    move/from16 v17, v0

    if-nez v17, :cond_3

    .line 544
    new-instance v17, Ljava/lang/ClassCastException;

    new-instance v18, Ljava/lang/StringBuilder;

    const-string/jumbo v19, "Could not cast an object to "

    invoke-direct/range {v18 .. v19}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 545
    const-class v19, Ljava/lang/Integer;

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string/jumbo v19, ": "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    .line 546
    invoke-virtual/range {p3 .. p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string/jumbo v19, ", "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    .line 547
    invoke-virtual/range {p3 .. p3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    .line 544
    invoke-direct/range {v17 .. v18}, Ljava/lang/ClassCastException;-><init>(Ljava/lang/String;)V

    throw v17

    .line 550
    :cond_3
    check-cast p3, Ljava/lang/Integer;

    .end local p3    # "value":Ljava/lang/Object;
    invoke-virtual/range {p3 .. p3}, Ljava/lang/Integer;->intValue()I

    move-result v17

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-static {v0, v1}, Lorg/apache/index/poi/hpsf/TypeWriter;->writeToStream(Ljava/io/OutputStream;I)I

    move-result v17

    add-int v11, v11, v17

    .line 551
    goto/16 :goto_0

    .line 555
    .restart local p3    # "value":Ljava/lang/Object;
    :sswitch_7
    check-cast p3, Ljava/lang/Long;

    .end local p3    # "value":Ljava/lang/Object;
    invoke-virtual/range {p3 .. p3}, Ljava/lang/Long;->longValue()J

    move-result-wide v18

    move-object/from16 v0, p0

    move-wide/from16 v1, v18

    invoke-static {v0, v1, v2}, Lorg/apache/index/poi/hpsf/TypeWriter;->writeToStream(Ljava/io/OutputStream;J)I

    .line 556
    const/16 v11, 0x8

    .line 557
    goto/16 :goto_0

    .line 562
    .restart local p3    # "value":Ljava/lang/Object;
    :sswitch_8
    check-cast p3, Ljava/lang/Double;

    .end local p3    # "value":Ljava/lang/Object;
    invoke-virtual/range {p3 .. p3}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v18

    move-object/from16 v0, p0

    move-wide/from16 v1, v18

    invoke-static {v0, v1, v2}, Lorg/apache/index/poi/hpsf/TypeWriter;->writeToStream(Ljava/io/OutputStream;D)I

    move-result v17

    add-int v11, v11, v17

    .line 563
    goto/16 :goto_0

    .line 567
    .restart local p3    # "value":Ljava/lang/Object;
    :sswitch_9
    check-cast p3, Ljava/util/Date;

    .end local p3    # "value":Ljava/lang/Object;
    invoke-static/range {p3 .. p3}, Lorg/apache/index/poi/hpsf/Util;->dateToFileTime(Ljava/util/Date;)J

    move-result-wide v6

    .line 568
    .local v6, "filetime":J
    const/16 v17, 0x20

    shr-long v18, v6, v17

    const-wide v20, 0xffffffffL

    and-long v18, v18, v20

    move-wide/from16 v0, v18

    long-to-int v8, v0

    .line 569
    .restart local v8    # "high":I
    const-wide v18, 0xffffffffL

    and-long v18, v18, v6

    move-wide/from16 v0, v18

    long-to-int v12, v0

    .line 571
    .restart local v12    # "low":I
    const-wide v18, 0xffffffffL

    int-to-long v0, v12

    move-wide/from16 v20, v0

    and-long v18, v18, v20

    move-object/from16 v0, p0

    move-wide/from16 v1, v18

    invoke-static {v0, v1, v2}, Lorg/apache/index/poi/hpsf/TypeWriter;->writeUIntToStream(Ljava/io/OutputStream;J)I

    move-result v17

    add-int v11, v11, v17

    .line 573
    const-wide v18, 0xffffffffL

    int-to-long v0, v8

    move-wide/from16 v20, v0

    and-long v18, v18, v20

    move-object/from16 v0, p0

    move-wide/from16 v1, v18

    invoke-static {v0, v1, v2}, Lorg/apache/index/poi/hpsf/TypeWriter;->writeUIntToStream(Ljava/io/OutputStream;J)I

    move-result v17

    add-int v11, v11, v17

    .line 574
    goto/16 :goto_0

    .line 589
    .end local v6    # "filetime":J
    .end local v8    # "high":I
    .end local v12    # "low":I
    .restart local p3    # "value":Ljava/lang/Object;
    :cond_4
    new-instance v17, Lorg/apache/index/poi/hpsf/WritingNotSupportedException;

    move-object/from16 v0, v17

    move-wide/from16 v1, p1

    move-object/from16 v3, p3

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/index/poi/hpsf/WritingNotSupportedException;-><init>(JLjava/lang/Object;)V

    throw v17

    .line 475
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_4
        0x2 -> :sswitch_5
        0x3 -> :sswitch_6
        0x5 -> :sswitch_8
        0xb -> :sswitch_0
        0x14 -> :sswitch_7
        0x1e -> :sswitch_1
        0x1f -> :sswitch_2
        0x40 -> :sswitch_9
        0x47 -> :sswitch_3
    .end sparse-switch
.end method

.method protected static writeUnsupportedTypeMessage(Lorg/apache/index/poi/hpsf/UnsupportedVariantTypeException;)V
    .locals 4
    .param p0, "ex"    # Lorg/apache/index/poi/hpsf/UnsupportedVariantTypeException;

    .prologue
    .line 96
    invoke-static {}, Lorg/apache/index/poi/hpsf/VariantSupport;->isLogUnsupportedTypes()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 98
    sget-object v1, Lorg/apache/index/poi/hpsf/VariantSupport;->unsupportedMessage:Ljava/util/List;

    if-nez v1, :cond_0

    .line 99
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    sput-object v1, Lorg/apache/index/poi/hpsf/VariantSupport;->unsupportedMessage:Ljava/util/List;

    .line 100
    :cond_0
    invoke-virtual {p0}, Lorg/apache/index/poi/hpsf/UnsupportedVariantTypeException;->getVariantType()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 101
    .local v0, "vt":Ljava/lang/Long;
    sget-object v1, Lorg/apache/index/poi/hpsf/VariantSupport;->unsupportedMessage:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 103
    sget-object v1, Ljava/lang/System;->err:Ljava/io/PrintStream;

    invoke-virtual {p0}, Lorg/apache/index/poi/hpsf/UnsupportedVariantTypeException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 104
    sget-object v1, Lorg/apache/index/poi/hpsf/VariantSupport;->unsupportedMessage:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 107
    .end local v0    # "vt":Ljava/lang/Long;
    :cond_1
    return-void
.end method


# virtual methods
.method public isSupportedType(I)Z
    .locals 2
    .param p1, "variantType"    # I

    .prologue
    .line 132
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    sget-object v1, Lorg/apache/index/poi/hpsf/VariantSupport;->SUPPORTED_TYPES:[I

    array-length v1, v1

    if-lt v0, v1, :cond_0

    .line 135
    const/4 v1, 0x0

    :goto_1
    return v1

    .line 133
    :cond_0
    sget-object v1, Lorg/apache/index/poi/hpsf/VariantSupport;->SUPPORTED_TYPES:[I

    aget v1, v1, v0

    if-ne p1, v1, :cond_1

    .line 134
    const/4 v1, 0x1

    goto :goto_1

    .line 132
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
