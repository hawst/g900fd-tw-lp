.class Lorg/apache/index/poi/hssf/record/SSTDeserializer;
.super Ljava/lang/Object;
.source "SSTDeserializer.java"


# static fields
.field private static final MAX_CHAR_COUNT:I = 0x100000


# instance fields
.field private g_strBuilderhssf:Ljava/lang/StringBuilder;

.field private indexWriterhssf:Lcom/samsung/index/ITextContentObs;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private indexString(Lorg/apache/index/poi/hssf/record/common/UnicodeString;)V
    .locals 2
    .param p1, "string"    # Lorg/apache/index/poi/hssf/record/common/UnicodeString;

    .prologue
    .line 81
    if-eqz p1, :cond_0

    iget-object v0, p0, Lorg/apache/index/poi/hssf/record/SSTDeserializer;->g_strBuilderhssf:Ljava/lang/StringBuilder;

    if-eqz v0, :cond_0

    .line 82
    iget-object v0, p0, Lorg/apache/index/poi/hssf/record/SSTDeserializer;->indexWriterhssf:Lcom/samsung/index/ITextContentObs;

    if-eqz v0, :cond_0

    .line 83
    iget-object v0, p0, Lorg/apache/index/poi/hssf/record/SSTDeserializer;->g_strBuilderhssf:Ljava/lang/StringBuilder;

    const-string/jumbo v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 84
    iget-object v0, p0, Lorg/apache/index/poi/hssf/record/SSTDeserializer;->g_strBuilderhssf:Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 88
    iget-object v0, p0, Lorg/apache/index/poi/hssf/record/SSTDeserializer;->g_strBuilderhssf:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    const/high16 v1, 0x100000

    if-lt v0, v1, :cond_0

    .line 89
    iget-object v0, p0, Lorg/apache/index/poi/hssf/record/SSTDeserializer;->indexWriterhssf:Lcom/samsung/index/ITextContentObs;

    iget-object v1, p0, Lorg/apache/index/poi/hssf/record/SSTDeserializer;->g_strBuilderhssf:Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/samsung/index/ITextContentObs;->updateText(Ljava/lang/String;)V

    .line 91
    iget-object v0, p0, Lorg/apache/index/poi/hssf/record/SSTDeserializer;->g_strBuilderhssf:Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 94
    :cond_0
    return-void
.end method


# virtual methods
.method public manufactureStrings(ILorg/apache/index/poi/hssf/record/RecordInputStream;)V
    .locals 5
    .param p1, "stringCount"    # I
    .param p2, "in"    # Lorg/apache/index/poi/hssf/record/RecordInputStream;

    .prologue
    .line 50
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v2, p0, Lorg/apache/index/poi/hssf/record/SSTDeserializer;->g_strBuilderhssf:Ljava/lang/StringBuilder;

    .line 51
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-lt v0, p1, :cond_1

    .line 65
    iget-object v2, p0, Lorg/apache/index/poi/hssf/record/SSTDeserializer;->g_strBuilderhssf:Ljava/lang/StringBuilder;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lorg/apache/index/poi/hssf/record/SSTDeserializer;->indexWriterhssf:Lcom/samsung/index/ITextContentObs;

    if-eqz v2, :cond_0

    .line 66
    iget-object v2, p0, Lorg/apache/index/poi/hssf/record/SSTDeserializer;->indexWriterhssf:Lcom/samsung/index/ITextContentObs;

    iget-object v3, p0, Lorg/apache/index/poi/hssf/record/SSTDeserializer;->g_strBuilderhssf:Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/samsung/index/ITextContentObs;->updateText(Ljava/lang/String;)V

    .line 68
    iget-object v2, p0, Lorg/apache/index/poi/hssf/record/SSTDeserializer;->g_strBuilderhssf:Ljava/lang/StringBuilder;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 71
    :cond_0
    return-void

    .line 54
    :cond_1
    invoke-virtual {p2}, Lorg/apache/index/poi/hssf/record/RecordInputStream;->available()I

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {p2}, Lorg/apache/index/poi/hssf/record/RecordInputStream;->hasNextRecord()Z

    move-result v2

    if-nez v2, :cond_2

    .line 55
    sget-object v2, Ljava/lang/System;->err:Ljava/io/PrintStream;

    .line 56
    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "Ran out of data before creating all the strings! String at index "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 57
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 56
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 58
    new-instance v1, Lorg/apache/index/poi/hssf/record/common/UnicodeString;

    const-string/jumbo v2, ""

    invoke-direct {v1, v2}, Lorg/apache/index/poi/hssf/record/common/UnicodeString;-><init>(Ljava/lang/String;)V

    .line 62
    .local v1, "str":Lorg/apache/index/poi/hssf/record/common/UnicodeString;
    :goto_1
    invoke-direct {p0, v1}, Lorg/apache/index/poi/hssf/record/SSTDeserializer;->indexString(Lorg/apache/index/poi/hssf/record/common/UnicodeString;)V

    .line 51
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 60
    .end local v1    # "str":Lorg/apache/index/poi/hssf/record/common/UnicodeString;
    :cond_2
    new-instance v1, Lorg/apache/index/poi/hssf/record/common/UnicodeString;

    invoke-direct {v1, p2}, Lorg/apache/index/poi/hssf/record/common/UnicodeString;-><init>(Lorg/apache/index/poi/hssf/record/RecordInputStream;)V

    .restart local v1    # "str":Lorg/apache/index/poi/hssf/record/common/UnicodeString;
    goto :goto_1
.end method

.method public setIndexWriterhssf(Lcom/samsung/index/ITextContentObs;)V
    .locals 0
    .param p1, "indexWriterhssf"    # Lcom/samsung/index/ITextContentObs;

    .prologue
    .line 97
    iput-object p1, p0, Lorg/apache/index/poi/hssf/record/SSTDeserializer;->indexWriterhssf:Lcom/samsung/index/ITextContentObs;

    .line 98
    return-void
.end method
