.class public Lorg/apache/index/poi/ddf/EscherSpgrRecord;
.super Lorg/apache/index/poi/ddf/EscherRecord;
.source "EscherSpgrRecord.java"


# static fields
.field public static final RECORD_DESCRIPTION:Ljava/lang/String; = "MsofbtSpgr"

.field public static final RECORD_ID:S = -0xff7s


# instance fields
.field private field_1_rectX1:I

.field private field_2_rectY1:I

.field private field_3_rectX2:I

.field private field_4_rectY2:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Lorg/apache/index/poi/ddf/EscherRecord;-><init>()V

    return-void
.end method


# virtual methods
.method public fillFields([BILorg/apache/index/poi/ddf/EscherRecordFactory;)I
    .locals 6
    .param p1, "data"    # [B
    .param p2, "offset"    # I
    .param p3, "recordFactory"    # Lorg/apache/index/poi/ddf/EscherRecordFactory;

    .prologue
    .line 42
    invoke-virtual {p0, p1, p2}, Lorg/apache/index/poi/ddf/EscherSpgrRecord;->readHeader([BI)I

    move-result v0

    .line 43
    .local v0, "bytesRemaining":I
    add-int/lit8 v1, p2, 0x8

    .line 44
    .local v1, "pos":I
    const/4 v2, 0x0

    .line 45
    .local v2, "size":I
    add-int v3, v1, v2

    invoke-static {p1, v3}, Lorg/apache/index/poi/util/LittleEndian;->getInt([BI)I

    move-result v3

    iput v3, p0, Lorg/apache/index/poi/ddf/EscherSpgrRecord;->field_1_rectX1:I

    add-int/lit8 v2, v2, 0x4

    .line 46
    add-int/lit8 v3, v1, 0x4

    invoke-static {p1, v3}, Lorg/apache/index/poi/util/LittleEndian;->getInt([BI)I

    move-result v3

    iput v3, p0, Lorg/apache/index/poi/ddf/EscherSpgrRecord;->field_2_rectY1:I

    add-int/lit8 v2, v2, 0x4

    .line 47
    add-int/lit8 v3, v1, 0x8

    invoke-static {p1, v3}, Lorg/apache/index/poi/util/LittleEndian;->getInt([BI)I

    move-result v3

    iput v3, p0, Lorg/apache/index/poi/ddf/EscherSpgrRecord;->field_3_rectX2:I

    add-int/lit8 v2, v2, 0x4

    .line 48
    add-int/lit8 v3, v1, 0xc

    invoke-static {p1, v3}, Lorg/apache/index/poi/util/LittleEndian;->getInt([BI)I

    move-result v3

    iput v3, p0, Lorg/apache/index/poi/ddf/EscherSpgrRecord;->field_4_rectY2:I

    add-int/lit8 v2, v2, 0x4

    .line 49
    add-int/lit8 v0, v0, -0x10

    .line 50
    if-eqz v0, :cond_0

    new-instance v3, Lorg/apache/index/poi/util/RecordFormatException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "Expected no remaining bytes but got "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Lorg/apache/index/poi/util/RecordFormatException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 53
    :cond_0
    add-int/lit8 v3, v0, 0x18

    return v3
.end method

.method public getRecordId()S
    .locals 1

    .prologue
    .line 79
    const/16 v0, -0xff7

    return v0
.end method

.method public getRecordName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 83
    const-string/jumbo v0, "Spgr"

    return-object v0
.end method

.method public getRecordSize()I
    .locals 1

    .prologue
    .line 75
    const/16 v0, 0x18

    return v0
.end method

.method public getRectX1()I
    .locals 1

    .prologue
    .line 104
    iget v0, p0, Lorg/apache/index/poi/ddf/EscherSpgrRecord;->field_1_rectX1:I

    return v0
.end method

.method public getRectX2()I
    .locals 1

    .prologue
    .line 136
    iget v0, p0, Lorg/apache/index/poi/ddf/EscherSpgrRecord;->field_3_rectX2:I

    return v0
.end method

.method public getRectY1()I
    .locals 1

    .prologue
    .line 120
    iget v0, p0, Lorg/apache/index/poi/ddf/EscherSpgrRecord;->field_2_rectY1:I

    return v0
.end method

.method public getRectY2()I
    .locals 1

    .prologue
    .line 152
    iget v0, p0, Lorg/apache/index/poi/ddf/EscherSpgrRecord;->field_4_rectY2:I

    return v0
.end method

.method public serialize(I[BLorg/apache/index/poi/ddf/EscherSerializationListener;)I
    .locals 4
    .param p1, "offset"    # I
    .param p2, "data"    # [B
    .param p3, "listener"    # Lorg/apache/index/poi/ddf/EscherSerializationListener;

    .prologue
    .line 58
    invoke-virtual {p0}, Lorg/apache/index/poi/ddf/EscherSpgrRecord;->getRecordId()S

    move-result v1

    invoke-interface {p3, p1, v1, p0}, Lorg/apache/index/poi/ddf/EscherSerializationListener;->beforeRecordSerialize(ISLorg/apache/index/poi/ddf/EscherRecord;)V

    .line 59
    invoke-virtual {p0}, Lorg/apache/index/poi/ddf/EscherSpgrRecord;->getOptions()S

    move-result v1

    invoke-static {p2, p1, v1}, Lorg/apache/index/poi/util/LittleEndian;->putShort([BIS)V

    .line 60
    add-int/lit8 v1, p1, 0x2

    invoke-virtual {p0}, Lorg/apache/index/poi/ddf/EscherSpgrRecord;->getRecordId()S

    move-result v2

    invoke-static {p2, v1, v2}, Lorg/apache/index/poi/util/LittleEndian;->putShort([BIS)V

    .line 61
    const/16 v0, 0x10

    .line 62
    .local v0, "remainingBytes":I
    add-int/lit8 v1, p1, 0x4

    invoke-static {p2, v1, v0}, Lorg/apache/index/poi/util/LittleEndian;->putInt([BII)V

    .line 63
    add-int/lit8 v1, p1, 0x8

    iget v2, p0, Lorg/apache/index/poi/ddf/EscherSpgrRecord;->field_1_rectX1:I

    invoke-static {p2, v1, v2}, Lorg/apache/index/poi/util/LittleEndian;->putInt([BII)V

    .line 64
    add-int/lit8 v1, p1, 0xc

    iget v2, p0, Lorg/apache/index/poi/ddf/EscherSpgrRecord;->field_2_rectY1:I

    invoke-static {p2, v1, v2}, Lorg/apache/index/poi/util/LittleEndian;->putInt([BII)V

    .line 65
    add-int/lit8 v1, p1, 0x10

    iget v2, p0, Lorg/apache/index/poi/ddf/EscherSpgrRecord;->field_3_rectX2:I

    invoke-static {p2, v1, v2}, Lorg/apache/index/poi/util/LittleEndian;->putInt([BII)V

    .line 66
    add-int/lit8 v1, p1, 0x14

    iget v2, p0, Lorg/apache/index/poi/ddf/EscherSpgrRecord;->field_4_rectY2:I

    invoke-static {p2, v1, v2}, Lorg/apache/index/poi/util/LittleEndian;->putInt([BII)V

    .line 69
    invoke-virtual {p0}, Lorg/apache/index/poi/ddf/EscherSpgrRecord;->getRecordSize()I

    move-result v1

    add-int/2addr v1, p1

    invoke-virtual {p0}, Lorg/apache/index/poi/ddf/EscherSpgrRecord;->getRecordId()S

    move-result v2

    invoke-virtual {p0}, Lorg/apache/index/poi/ddf/EscherSpgrRecord;->getRecordSize()I

    move-result v3

    add-int/2addr v3, p1

    invoke-interface {p3, v1, v2, v3, p0}, Lorg/apache/index/poi/ddf/EscherSerializationListener;->afterRecordSerialize(ISILorg/apache/index/poi/ddf/EscherRecord;)V

    .line 70
    const/16 v1, 0x18

    return v1
.end method

.method public setRectX1(I)V
    .locals 0
    .param p1, "x1"    # I

    .prologue
    .line 112
    iput p1, p0, Lorg/apache/index/poi/ddf/EscherSpgrRecord;->field_1_rectX1:I

    .line 113
    return-void
.end method

.method public setRectX2(I)V
    .locals 0
    .param p1, "x2"    # I

    .prologue
    .line 144
    iput p1, p0, Lorg/apache/index/poi/ddf/EscherSpgrRecord;->field_3_rectX2:I

    .line 145
    return-void
.end method

.method public setRectY1(I)V
    .locals 0
    .param p1, "y1"    # I

    .prologue
    .line 128
    iput p1, p0, Lorg/apache/index/poi/ddf/EscherSpgrRecord;->field_2_rectY1:I

    .line 129
    return-void
.end method

.method public setRectY2(I)V
    .locals 0
    .param p1, "rectY2"    # I

    .prologue
    .line 159
    iput p1, p0, Lorg/apache/index/poi/ddf/EscherSpgrRecord;->field_4_rectY2:I

    .line 160
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    const/16 v2, 0xa

    .line 90
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 91
    const-string/jumbo v1, "  RecordId: 0x"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, -0xff7

    invoke-static {v1}, Lorg/apache/index/poi/util/HexDump;->toHex(S)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 92
    const-string/jumbo v1, "  Options: 0x"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lorg/apache/index/poi/ddf/EscherSpgrRecord;->getOptions()S

    move-result v1

    invoke-static {v1}, Lorg/apache/index/poi/util/HexDump;->toHex(S)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 93
    const-string/jumbo v1, "  RectX: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lorg/apache/index/poi/ddf/EscherSpgrRecord;->field_1_rectX1:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 94
    const-string/jumbo v1, "  RectY: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lorg/apache/index/poi/ddf/EscherSpgrRecord;->field_2_rectY1:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 95
    const-string/jumbo v1, "  RectWidth: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lorg/apache/index/poi/ddf/EscherSpgrRecord;->field_3_rectX2:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 96
    const-string/jumbo v1, "  RectHeight: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lorg/apache/index/poi/ddf/EscherSpgrRecord;->field_4_rectY2:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 90
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
