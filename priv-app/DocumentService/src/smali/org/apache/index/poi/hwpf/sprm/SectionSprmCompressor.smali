.class public final Lorg/apache/index/poi/hwpf/sprm/SectionSprmCompressor;
.super Ljava/lang/Object;
.source "SectionSprmCompressor.java"


# static fields
.field private static final DEFAULT_SEP:Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    new-instance v0, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;

    invoke-direct {v0}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;-><init>()V

    sput-object v0, Lorg/apache/index/poi/hwpf/sprm/SectionSprmCompressor;->DEFAULT_SEP:Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    return-void
.end method

.method public static compressSectionProperty(Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;)[B
    .locals 9
    .param p0, "newSEP"    # Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v8, 0x0

    .line 36
    const/4 v2, 0x0

    .line 37
    .local v2, "size":I
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 39
    .local v3, "sprmList":Ljava/util/ArrayList;
    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getCnsPgn()B

    move-result v4

    sget-object v7, Lorg/apache/index/poi/hwpf/sprm/SectionSprmCompressor;->DEFAULT_SEP:Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;

    invoke-virtual {v7}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getCnsPgn()B

    move-result v7

    if-eq v4, v7, :cond_0

    .line 41
    const/16 v4, 0x3000

    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getCnsPgn()B

    move-result v7

    invoke-static {v4, v7, v8, v3}, Lorg/apache/index/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v4

    add-int/2addr v2, v4

    .line 43
    :cond_0
    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getIHeadingPgn()B

    move-result v4

    sget-object v7, Lorg/apache/index/poi/hwpf/sprm/SectionSprmCompressor;->DEFAULT_SEP:Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;

    invoke-virtual {v7}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getIHeadingPgn()B

    move-result v7

    if-eq v4, v7, :cond_1

    .line 45
    const/16 v4, 0x3001

    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getIHeadingPgn()B

    move-result v7

    invoke-static {v4, v7, v8, v3}, Lorg/apache/index/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v4

    add-int/2addr v2, v4

    .line 47
    :cond_1
    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getOlstAnm()[B

    move-result-object v4

    sget-object v7, Lorg/apache/index/poi/hwpf/sprm/SectionSprmCompressor;->DEFAULT_SEP:Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;

    invoke-virtual {v7}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getOlstAnm()[B

    move-result-object v7

    invoke-static {v4, v7}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v4

    if-nez v4, :cond_2

    .line 49
    const/16 v4, -0x2dfe

    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getOlstAnm()[B

    move-result-object v7

    invoke-static {v4, v6, v7, v3}, Lorg/apache/index/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v4

    add-int/2addr v2, v4

    .line 51
    :cond_2
    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getFEvenlySpaced()Z

    move-result v4

    sget-object v7, Lorg/apache/index/poi/hwpf/sprm/SectionSprmCompressor;->DEFAULT_SEP:Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;

    invoke-virtual {v7}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getFEvenlySpaced()Z

    move-result v7

    if-eq v4, v7, :cond_3

    .line 53
    const/16 v7, 0x3005

    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getFEvenlySpaced()Z

    move-result v4

    if-eqz v4, :cond_2f

    move v4, v5

    :goto_0
    invoke-static {v7, v4, v8, v3}, Lorg/apache/index/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v4

    add-int/2addr v2, v4

    .line 55
    :cond_3
    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getFUnlocked()Z

    move-result v4

    sget-object v7, Lorg/apache/index/poi/hwpf/sprm/SectionSprmCompressor;->DEFAULT_SEP:Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;

    invoke-virtual {v7}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getFUnlocked()Z

    move-result v7

    if-eq v4, v7, :cond_4

    .line 57
    const/16 v7, 0x3006

    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getFUnlocked()Z

    move-result v4

    if-eqz v4, :cond_30

    move v4, v5

    :goto_1
    invoke-static {v7, v4, v8, v3}, Lorg/apache/index/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v4

    add-int/2addr v2, v4

    .line 59
    :cond_4
    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getDmBinFirst()I

    move-result v4

    sget-object v7, Lorg/apache/index/poi/hwpf/sprm/SectionSprmCompressor;->DEFAULT_SEP:Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;

    invoke-virtual {v7}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getDmBinFirst()I

    move-result v7

    if-eq v4, v7, :cond_5

    .line 61
    const/16 v4, 0x5007

    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getDmBinFirst()I

    move-result v7

    invoke-static {v4, v7, v8, v3}, Lorg/apache/index/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v4

    add-int/2addr v2, v4

    .line 63
    :cond_5
    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getDmBinOther()I

    move-result v4

    sget-object v7, Lorg/apache/index/poi/hwpf/sprm/SectionSprmCompressor;->DEFAULT_SEP:Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;

    invoke-virtual {v7}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getDmBinOther()I

    move-result v7

    if-eq v4, v7, :cond_6

    .line 65
    const/16 v4, 0x5008

    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getDmBinOther()I

    move-result v7

    invoke-static {v4, v7, v8, v3}, Lorg/apache/index/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v4

    add-int/2addr v2, v4

    .line 67
    :cond_6
    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getBkc()B

    move-result v4

    sget-object v7, Lorg/apache/index/poi/hwpf/sprm/SectionSprmCompressor;->DEFAULT_SEP:Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;

    invoke-virtual {v7}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getBkc()B

    move-result v7

    if-eq v4, v7, :cond_7

    .line 69
    const/16 v4, 0x3009

    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getBkc()B

    move-result v7

    invoke-static {v4, v7, v8, v3}, Lorg/apache/index/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v4

    add-int/2addr v2, v4

    .line 71
    :cond_7
    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getFTitlePage()Z

    move-result v4

    sget-object v7, Lorg/apache/index/poi/hwpf/sprm/SectionSprmCompressor;->DEFAULT_SEP:Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;

    invoke-virtual {v7}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getFTitlePage()Z

    move-result v7

    if-eq v4, v7, :cond_8

    .line 73
    const/16 v7, 0x300a

    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getFTitlePage()Z

    move-result v4

    if-eqz v4, :cond_31

    move v4, v5

    :goto_2
    invoke-static {v7, v4, v8, v3}, Lorg/apache/index/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v4

    add-int/2addr v2, v4

    .line 75
    :cond_8
    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getCcolM1()I

    move-result v4

    sget-object v7, Lorg/apache/index/poi/hwpf/sprm/SectionSprmCompressor;->DEFAULT_SEP:Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;

    invoke-virtual {v7}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getCcolM1()I

    move-result v7

    if-eq v4, v7, :cond_9

    .line 77
    const/16 v4, 0x500b

    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getCcolM1()I

    move-result v7

    invoke-static {v4, v7, v8, v3}, Lorg/apache/index/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v4

    add-int/2addr v2, v4

    .line 79
    :cond_9
    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getDxaColumns()I

    move-result v4

    sget-object v7, Lorg/apache/index/poi/hwpf/sprm/SectionSprmCompressor;->DEFAULT_SEP:Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;

    invoke-virtual {v7}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getDxaColumns()I

    move-result v7

    if-eq v4, v7, :cond_a

    .line 81
    const/16 v4, -0x6ff4

    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getDxaColumns()I

    move-result v7

    invoke-static {v4, v7, v8, v3}, Lorg/apache/index/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v4

    add-int/2addr v2, v4

    .line 83
    :cond_a
    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getFAutoPgn()Z

    move-result v4

    sget-object v7, Lorg/apache/index/poi/hwpf/sprm/SectionSprmCompressor;->DEFAULT_SEP:Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;

    invoke-virtual {v7}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getFAutoPgn()Z

    move-result v7

    if-eq v4, v7, :cond_b

    .line 85
    const/16 v7, 0x300d

    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getFAutoPgn()Z

    move-result v4

    if-eqz v4, :cond_32

    move v4, v5

    :goto_3
    invoke-static {v7, v4, v8, v3}, Lorg/apache/index/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v4

    add-int/2addr v2, v4

    .line 87
    :cond_b
    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getNfcPgn()B

    move-result v4

    sget-object v7, Lorg/apache/index/poi/hwpf/sprm/SectionSprmCompressor;->DEFAULT_SEP:Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;

    invoke-virtual {v7}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getNfcPgn()B

    move-result v7

    if-eq v4, v7, :cond_c

    .line 89
    const/16 v4, 0x300e

    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getNfcPgn()B

    move-result v7

    invoke-static {v4, v7, v8, v3}, Lorg/apache/index/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v4

    add-int/2addr v2, v4

    .line 91
    :cond_c
    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getDyaPgn()I

    move-result v4

    sget-object v7, Lorg/apache/index/poi/hwpf/sprm/SectionSprmCompressor;->DEFAULT_SEP:Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;

    invoke-virtual {v7}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getDyaPgn()I

    move-result v7

    if-eq v4, v7, :cond_d

    .line 93
    const/16 v4, -0x4ff1

    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getDyaPgn()I

    move-result v7

    invoke-static {v4, v7, v8, v3}, Lorg/apache/index/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v4

    add-int/2addr v2, v4

    .line 95
    :cond_d
    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getDxaPgn()I

    move-result v4

    sget-object v7, Lorg/apache/index/poi/hwpf/sprm/SectionSprmCompressor;->DEFAULT_SEP:Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;

    invoke-virtual {v7}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getDxaPgn()I

    move-result v7

    if-eq v4, v7, :cond_e

    .line 97
    const/16 v4, -0x4ff0

    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getDxaPgn()I

    move-result v7

    invoke-static {v4, v7, v8, v3}, Lorg/apache/index/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v4

    add-int/2addr v2, v4

    .line 99
    :cond_e
    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getFPgnRestart()Z

    move-result v4

    sget-object v7, Lorg/apache/index/poi/hwpf/sprm/SectionSprmCompressor;->DEFAULT_SEP:Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;

    invoke-virtual {v7}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getFPgnRestart()Z

    move-result v7

    if-eq v4, v7, :cond_f

    .line 101
    const/16 v7, 0x3011

    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getFPgnRestart()Z

    move-result v4

    if-eqz v4, :cond_33

    move v4, v5

    :goto_4
    invoke-static {v7, v4, v8, v3}, Lorg/apache/index/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v4

    add-int/2addr v2, v4

    .line 103
    :cond_f
    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getFEndNote()Z

    move-result v4

    sget-object v7, Lorg/apache/index/poi/hwpf/sprm/SectionSprmCompressor;->DEFAULT_SEP:Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;

    invoke-virtual {v7}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getFEndNote()Z

    move-result v7

    if-eq v4, v7, :cond_10

    .line 105
    const/16 v7, 0x3012

    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getFEndNote()Z

    move-result v4

    if-eqz v4, :cond_34

    move v4, v5

    :goto_5
    invoke-static {v7, v4, v8, v3}, Lorg/apache/index/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v4

    add-int/2addr v2, v4

    .line 107
    :cond_10
    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getLnc()B

    move-result v4

    sget-object v7, Lorg/apache/index/poi/hwpf/sprm/SectionSprmCompressor;->DEFAULT_SEP:Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;

    invoke-virtual {v7}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getLnc()B

    move-result v7

    if-eq v4, v7, :cond_11

    .line 109
    const/16 v4, 0x3013

    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getLnc()B

    move-result v7

    invoke-static {v4, v7, v8, v3}, Lorg/apache/index/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v4

    add-int/2addr v2, v4

    .line 111
    :cond_11
    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getGrpfIhdt()B

    move-result v4

    sget-object v7, Lorg/apache/index/poi/hwpf/sprm/SectionSprmCompressor;->DEFAULT_SEP:Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;

    invoke-virtual {v7}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getGrpfIhdt()B

    move-result v7

    if-eq v4, v7, :cond_12

    .line 113
    const/16 v4, 0x3014

    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getGrpfIhdt()B

    move-result v7

    invoke-static {v4, v7, v8, v3}, Lorg/apache/index/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v4

    add-int/2addr v2, v4

    .line 115
    :cond_12
    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getNLnnMod()I

    move-result v4

    sget-object v7, Lorg/apache/index/poi/hwpf/sprm/SectionSprmCompressor;->DEFAULT_SEP:Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;

    invoke-virtual {v7}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getNLnnMod()I

    move-result v7

    if-eq v4, v7, :cond_13

    .line 117
    const/16 v4, 0x5015

    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getNLnnMod()I

    move-result v7

    invoke-static {v4, v7, v8, v3}, Lorg/apache/index/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v4

    add-int/2addr v2, v4

    .line 119
    :cond_13
    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getDxaLnn()I

    move-result v4

    sget-object v7, Lorg/apache/index/poi/hwpf/sprm/SectionSprmCompressor;->DEFAULT_SEP:Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;

    invoke-virtual {v7}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getDxaLnn()I

    move-result v7

    if-eq v4, v7, :cond_14

    .line 121
    const/16 v4, -0x6fea

    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getDxaLnn()I

    move-result v7

    invoke-static {v4, v7, v8, v3}, Lorg/apache/index/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v4

    add-int/2addr v2, v4

    .line 123
    :cond_14
    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getDyaHdrTop()I

    move-result v4

    sget-object v7, Lorg/apache/index/poi/hwpf/sprm/SectionSprmCompressor;->DEFAULT_SEP:Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;

    invoke-virtual {v7}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getDyaHdrTop()I

    move-result v7

    if-eq v4, v7, :cond_15

    .line 125
    const/16 v4, -0x4fe9

    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getDyaHdrTop()I

    move-result v7

    invoke-static {v4, v7, v8, v3}, Lorg/apache/index/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v4

    add-int/2addr v2, v4

    .line 127
    :cond_15
    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getDyaHdrBottom()I

    move-result v4

    sget-object v7, Lorg/apache/index/poi/hwpf/sprm/SectionSprmCompressor;->DEFAULT_SEP:Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;

    invoke-virtual {v7}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getDyaHdrBottom()I

    move-result v7

    if-eq v4, v7, :cond_16

    .line 129
    const/16 v4, -0x4fe8

    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getDyaHdrBottom()I

    move-result v7

    invoke-static {v4, v7, v8, v3}, Lorg/apache/index/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v4

    add-int/2addr v2, v4

    .line 131
    :cond_16
    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getFLBetween()Z

    move-result v4

    sget-object v7, Lorg/apache/index/poi/hwpf/sprm/SectionSprmCompressor;->DEFAULT_SEP:Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;

    invoke-virtual {v7}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getFLBetween()Z

    move-result v7

    if-eq v4, v7, :cond_17

    .line 133
    const/16 v7, 0x3019

    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getFLBetween()Z

    move-result v4

    if-eqz v4, :cond_35

    move v4, v5

    :goto_6
    invoke-static {v7, v4, v8, v3}, Lorg/apache/index/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v4

    add-int/2addr v2, v4

    .line 135
    :cond_17
    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getVjc()B

    move-result v4

    sget-object v7, Lorg/apache/index/poi/hwpf/sprm/SectionSprmCompressor;->DEFAULT_SEP:Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;

    invoke-virtual {v7}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getVjc()B

    move-result v7

    if-eq v4, v7, :cond_18

    .line 137
    const/16 v4, 0x301a

    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getVjc()B

    move-result v7

    invoke-static {v4, v7, v8, v3}, Lorg/apache/index/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v4

    add-int/2addr v2, v4

    .line 139
    :cond_18
    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getLnnMin()I

    move-result v4

    sget-object v7, Lorg/apache/index/poi/hwpf/sprm/SectionSprmCompressor;->DEFAULT_SEP:Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;

    invoke-virtual {v7}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getLnnMin()I

    move-result v7

    if-eq v4, v7, :cond_19

    .line 141
    const/16 v4, 0x501b

    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getLnnMin()I

    move-result v7

    invoke-static {v4, v7, v8, v3}, Lorg/apache/index/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v4

    add-int/2addr v2, v4

    .line 143
    :cond_19
    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getPgnStart()I

    move-result v4

    sget-object v7, Lorg/apache/index/poi/hwpf/sprm/SectionSprmCompressor;->DEFAULT_SEP:Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;

    invoke-virtual {v7}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getPgnStart()I

    move-result v7

    if-eq v4, v7, :cond_1a

    .line 145
    const/16 v4, 0x501c

    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getPgnStart()I

    move-result v7

    invoke-static {v4, v7, v8, v3}, Lorg/apache/index/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v4

    add-int/2addr v2, v4

    .line 147
    :cond_1a
    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getDmOrientPage()B

    move-result v4

    sget-object v7, Lorg/apache/index/poi/hwpf/sprm/SectionSprmCompressor;->DEFAULT_SEP:Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;

    invoke-virtual {v7}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getDmOrientPage()B

    move-result v7

    if-eq v4, v7, :cond_1b

    .line 149
    const/16 v4, 0x301d

    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getDmOrientPage()B

    move-result v7

    invoke-static {v4, v7, v8, v3}, Lorg/apache/index/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v4

    add-int/2addr v2, v4

    .line 151
    :cond_1b
    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getXaPage()I

    move-result v4

    sget-object v7, Lorg/apache/index/poi/hwpf/sprm/SectionSprmCompressor;->DEFAULT_SEP:Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;

    invoke-virtual {v7}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getXaPage()I

    move-result v7

    if-eq v4, v7, :cond_1c

    .line 153
    const/16 v4, -0x4fe1

    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getXaPage()I

    move-result v7

    invoke-static {v4, v7, v8, v3}, Lorg/apache/index/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v4

    add-int/2addr v2, v4

    .line 155
    :cond_1c
    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getYaPage()I

    move-result v4

    sget-object v7, Lorg/apache/index/poi/hwpf/sprm/SectionSprmCompressor;->DEFAULT_SEP:Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;

    invoke-virtual {v7}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getYaPage()I

    move-result v7

    if-eq v4, v7, :cond_1d

    .line 157
    const/16 v4, -0x4fe0

    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getYaPage()I

    move-result v7

    invoke-static {v4, v7, v8, v3}, Lorg/apache/index/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v4

    add-int/2addr v2, v4

    .line 159
    :cond_1d
    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getDxaLeft()I

    move-result v4

    sget-object v7, Lorg/apache/index/poi/hwpf/sprm/SectionSprmCompressor;->DEFAULT_SEP:Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;

    invoke-virtual {v7}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getDxaLeft()I

    move-result v7

    if-eq v4, v7, :cond_1e

    .line 161
    const/16 v4, -0x4fdf

    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getDxaLeft()I

    move-result v7

    invoke-static {v4, v7, v8, v3}, Lorg/apache/index/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v4

    add-int/2addr v2, v4

    .line 163
    :cond_1e
    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getDxaRight()I

    move-result v4

    sget-object v7, Lorg/apache/index/poi/hwpf/sprm/SectionSprmCompressor;->DEFAULT_SEP:Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;

    invoke-virtual {v7}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getDxaRight()I

    move-result v7

    if-eq v4, v7, :cond_1f

    .line 165
    const/16 v4, -0x4fde

    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getDxaRight()I

    move-result v7

    invoke-static {v4, v7, v8, v3}, Lorg/apache/index/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v4

    add-int/2addr v2, v4

    .line 167
    :cond_1f
    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getDyaTop()I

    move-result v4

    sget-object v7, Lorg/apache/index/poi/hwpf/sprm/SectionSprmCompressor;->DEFAULT_SEP:Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;

    invoke-virtual {v7}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getDyaTop()I

    move-result v7

    if-eq v4, v7, :cond_20

    .line 169
    const/16 v4, -0x6fdd

    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getDyaTop()I

    move-result v7

    invoke-static {v4, v7, v8, v3}, Lorg/apache/index/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v4

    add-int/2addr v2, v4

    .line 171
    :cond_20
    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getDyaBottom()I

    move-result v4

    sget-object v7, Lorg/apache/index/poi/hwpf/sprm/SectionSprmCompressor;->DEFAULT_SEP:Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;

    invoke-virtual {v7}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getDyaBottom()I

    move-result v7

    if-eq v4, v7, :cond_21

    .line 173
    const/16 v4, -0x6fdc

    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getDyaBottom()I

    move-result v7

    invoke-static {v4, v7, v8, v3}, Lorg/apache/index/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v4

    add-int/2addr v2, v4

    .line 175
    :cond_21
    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getDzaGutter()I

    move-result v4

    sget-object v7, Lorg/apache/index/poi/hwpf/sprm/SectionSprmCompressor;->DEFAULT_SEP:Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;

    invoke-virtual {v7}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getDzaGutter()I

    move-result v7

    if-eq v4, v7, :cond_22

    .line 177
    const/16 v4, -0x4fdb

    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getDzaGutter()I

    move-result v7

    invoke-static {v4, v7, v8, v3}, Lorg/apache/index/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v4

    add-int/2addr v2, v4

    .line 179
    :cond_22
    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getDmPaperReq()I

    move-result v4

    sget-object v7, Lorg/apache/index/poi/hwpf/sprm/SectionSprmCompressor;->DEFAULT_SEP:Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;

    invoke-virtual {v7}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getDmPaperReq()I

    move-result v7

    if-eq v4, v7, :cond_23

    .line 181
    const/16 v4, 0x5026

    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getDmPaperReq()I

    move-result v7

    invoke-static {v4, v7, v8, v3}, Lorg/apache/index/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v4

    add-int/2addr v2, v4

    .line 183
    :cond_23
    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getFPropMark()Z

    move-result v4

    sget-object v7, Lorg/apache/index/poi/hwpf/sprm/SectionSprmCompressor;->DEFAULT_SEP:Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;

    invoke-virtual {v7}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getFPropMark()Z

    move-result v7

    if-ne v4, v7, :cond_24

    .line 184
    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getIbstPropRMark()I

    move-result v4

    sget-object v7, Lorg/apache/index/poi/hwpf/sprm/SectionSprmCompressor;->DEFAULT_SEP:Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;

    invoke-virtual {v7}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getIbstPropRMark()I

    move-result v7

    if-ne v4, v7, :cond_24

    .line 185
    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getDttmPropRMark()Lorg/apache/index/poi/hwpf/usermodel/DateAndTime;

    move-result-object v4

    sget-object v7, Lorg/apache/index/poi/hwpf/sprm/SectionSprmCompressor;->DEFAULT_SEP:Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;

    invoke-virtual {v7}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getDttmPropRMark()Lorg/apache/index/poi/hwpf/usermodel/DateAndTime;

    move-result-object v7

    invoke-virtual {v4, v7}, Lorg/apache/index/poi/hwpf/usermodel/DateAndTime;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_25

    .line 187
    :cond_24
    const/4 v4, 0x7

    new-array v0, v4, [B

    .line 188
    .local v0, "buf":[B
    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getFPropMark()Z

    move-result v4

    if-eqz v4, :cond_36

    :goto_7
    int-to-byte v4, v5

    aput-byte v4, v0, v6

    .line 189
    const/4 v1, 0x1

    .line 190
    .local v1, "offset":I
    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getIbstPropRMark()I

    move-result v4

    int-to-short v4, v4

    invoke-static {v0, v4}, Lorg/apache/index/poi/util/LittleEndian;->putShort([BS)V

    .line 191
    add-int/lit8 v1, v1, 0x2

    .line 192
    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getDttmPropRMark()Lorg/apache/index/poi/hwpf/usermodel/DateAndTime;

    move-result-object v4

    invoke-virtual {v4, v0, v1}, Lorg/apache/index/poi/hwpf/usermodel/DateAndTime;->serialize([BI)V

    .line 193
    const/16 v4, -0x2dd9

    const/4 v5, -0x1

    invoke-static {v4, v5, v0, v3}, Lorg/apache/index/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v4

    add-int/2addr v2, v4

    .line 195
    .end local v0    # "buf":[B
    .end local v1    # "offset":I
    :cond_25
    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getBrcTop()Lorg/apache/index/poi/hwpf/usermodel/BorderCode;

    move-result-object v4

    sget-object v5, Lorg/apache/index/poi/hwpf/sprm/SectionSprmCompressor;->DEFAULT_SEP:Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;

    invoke-virtual {v5}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getBrcTop()Lorg/apache/index/poi/hwpf/usermodel/BorderCode;

    move-result-object v5

    invoke-virtual {v4, v5}, Lorg/apache/index/poi/hwpf/usermodel/BorderCode;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_26

    .line 197
    const/16 v4, 0x702b

    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getBrcTop()Lorg/apache/index/poi/hwpf/usermodel/BorderCode;

    move-result-object v5

    invoke-virtual {v5}, Lorg/apache/index/poi/hwpf/usermodel/BorderCode;->toInt()I

    move-result v5

    invoke-static {v4, v5, v8, v3}, Lorg/apache/index/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v4

    add-int/2addr v2, v4

    .line 199
    :cond_26
    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getBrcLeft()Lorg/apache/index/poi/hwpf/usermodel/BorderCode;

    move-result-object v4

    sget-object v5, Lorg/apache/index/poi/hwpf/sprm/SectionSprmCompressor;->DEFAULT_SEP:Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;

    invoke-virtual {v5}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getBrcLeft()Lorg/apache/index/poi/hwpf/usermodel/BorderCode;

    move-result-object v5

    invoke-virtual {v4, v5}, Lorg/apache/index/poi/hwpf/usermodel/BorderCode;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_27

    .line 201
    const/16 v4, 0x702c

    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getBrcLeft()Lorg/apache/index/poi/hwpf/usermodel/BorderCode;

    move-result-object v5

    invoke-virtual {v5}, Lorg/apache/index/poi/hwpf/usermodel/BorderCode;->toInt()I

    move-result v5

    invoke-static {v4, v5, v8, v3}, Lorg/apache/index/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v4

    add-int/2addr v2, v4

    .line 203
    :cond_27
    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getBrcBottom()Lorg/apache/index/poi/hwpf/usermodel/BorderCode;

    move-result-object v4

    sget-object v5, Lorg/apache/index/poi/hwpf/sprm/SectionSprmCompressor;->DEFAULT_SEP:Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;

    invoke-virtual {v5}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getBrcBottom()Lorg/apache/index/poi/hwpf/usermodel/BorderCode;

    move-result-object v5

    invoke-virtual {v4, v5}, Lorg/apache/index/poi/hwpf/usermodel/BorderCode;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_28

    .line 205
    const/16 v4, 0x702d

    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getBrcBottom()Lorg/apache/index/poi/hwpf/usermodel/BorderCode;

    move-result-object v5

    invoke-virtual {v5}, Lorg/apache/index/poi/hwpf/usermodel/BorderCode;->toInt()I

    move-result v5

    invoke-static {v4, v5, v8, v3}, Lorg/apache/index/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v4

    add-int/2addr v2, v4

    .line 207
    :cond_28
    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getBrcRight()Lorg/apache/index/poi/hwpf/usermodel/BorderCode;

    move-result-object v4

    sget-object v5, Lorg/apache/index/poi/hwpf/sprm/SectionSprmCompressor;->DEFAULT_SEP:Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;

    invoke-virtual {v5}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getBrcRight()Lorg/apache/index/poi/hwpf/usermodel/BorderCode;

    move-result-object v5

    invoke-virtual {v4, v5}, Lorg/apache/index/poi/hwpf/usermodel/BorderCode;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_29

    .line 209
    const/16 v4, 0x702e

    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getBrcRight()Lorg/apache/index/poi/hwpf/usermodel/BorderCode;

    move-result-object v5

    invoke-virtual {v5}, Lorg/apache/index/poi/hwpf/usermodel/BorderCode;->toInt()I

    move-result v5

    invoke-static {v4, v5, v8, v3}, Lorg/apache/index/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v4

    add-int/2addr v2, v4

    .line 211
    :cond_29
    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getPgbProp()I

    move-result v4

    sget-object v5, Lorg/apache/index/poi/hwpf/sprm/SectionSprmCompressor;->DEFAULT_SEP:Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;

    invoke-virtual {v5}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getPgbProp()I

    move-result v5

    if-eq v4, v5, :cond_2a

    .line 213
    const/16 v4, 0x522f

    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getPgbProp()I

    move-result v5

    invoke-static {v4, v5, v8, v3}, Lorg/apache/index/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v4

    add-int/2addr v2, v4

    .line 215
    :cond_2a
    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getDxtCharSpace()I

    move-result v4

    sget-object v5, Lorg/apache/index/poi/hwpf/sprm/SectionSprmCompressor;->DEFAULT_SEP:Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;

    invoke-virtual {v5}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getDxtCharSpace()I

    move-result v5

    if-eq v4, v5, :cond_2b

    .line 217
    const/16 v4, 0x7030

    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getDxtCharSpace()I

    move-result v5

    invoke-static {v4, v5, v8, v3}, Lorg/apache/index/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v4

    add-int/2addr v2, v4

    .line 219
    :cond_2b
    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getDyaLinePitch()I

    move-result v4

    sget-object v5, Lorg/apache/index/poi/hwpf/sprm/SectionSprmCompressor;->DEFAULT_SEP:Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;

    invoke-virtual {v5}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getDyaLinePitch()I

    move-result v5

    if-eq v4, v5, :cond_2c

    .line 221
    const/16 v4, -0x6fcf

    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getDyaLinePitch()I

    move-result v5

    invoke-static {v4, v5, v8, v3}, Lorg/apache/index/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v4

    add-int/2addr v2, v4

    .line 223
    :cond_2c
    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getClm()I

    move-result v4

    sget-object v5, Lorg/apache/index/poi/hwpf/sprm/SectionSprmCompressor;->DEFAULT_SEP:Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;

    invoke-virtual {v5}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getClm()I

    move-result v5

    if-eq v4, v5, :cond_2d

    .line 225
    const/16 v4, 0x5032

    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getClm()I

    move-result v5

    invoke-static {v4, v5, v8, v3}, Lorg/apache/index/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v4

    add-int/2addr v2, v4

    .line 227
    :cond_2d
    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getWTextFlow()I

    move-result v4

    sget-object v5, Lorg/apache/index/poi/hwpf/sprm/SectionSprmCompressor;->DEFAULT_SEP:Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;

    invoke-virtual {v5}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getWTextFlow()I

    move-result v5

    if-eq v4, v5, :cond_2e

    .line 229
    const/16 v4, 0x5033

    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getWTextFlow()I

    move-result v5

    invoke-static {v4, v5, v8, v3}, Lorg/apache/index/poi/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v4

    add-int/2addr v2, v4

    .line 232
    :cond_2e
    invoke-static {v3, v2}, Lorg/apache/index/poi/hwpf/sprm/SprmUtils;->getGrpprl(Ljava/util/List;I)[B

    move-result-object v4

    return-object v4

    :cond_2f
    move v4, v6

    .line 53
    goto/16 :goto_0

    :cond_30
    move v4, v6

    .line 57
    goto/16 :goto_1

    :cond_31
    move v4, v6

    .line 73
    goto/16 :goto_2

    :cond_32
    move v4, v6

    .line 85
    goto/16 :goto_3

    :cond_33
    move v4, v6

    .line 101
    goto/16 :goto_4

    :cond_34
    move v4, v6

    .line 105
    goto/16 :goto_5

    :cond_35
    move v4, v6

    .line 133
    goto/16 :goto_6

    .restart local v0    # "buf":[B
    :cond_36
    move v5, v6

    .line 188
    goto/16 :goto_7
.end method
