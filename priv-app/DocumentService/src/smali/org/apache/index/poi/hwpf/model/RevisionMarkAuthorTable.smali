.class public final Lorg/apache/index/poi/hwpf/model/RevisionMarkAuthorTable;
.super Ljava/lang/Object;
.source "RevisionMarkAuthorTable.java"


# instance fields
.field private cData:S

.field private cbExtra:S

.field private entries:[Ljava/lang/String;

.field private fExtend:S


# direct methods
.method public constructor <init>([BII)V
    .locals 5
    .param p1, "tableStream"    # [B
    .param p2, "offset"    # I
    .param p3, "size"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    const/4 v3, -0x1

    iput-short v3, p0, Lorg/apache/index/poi/hwpf/model/RevisionMarkAuthorTable;->fExtend:S

    .line 43
    iput-short v4, p0, Lorg/apache/index/poi/hwpf/model/RevisionMarkAuthorTable;->cData:S

    .line 48
    iput-short v4, p0, Lorg/apache/index/poi/hwpf/model/RevisionMarkAuthorTable;->cbExtra:S

    .line 64
    invoke-static {p1, p2}, Lorg/apache/index/poi/util/LittleEndian;->getShort([BI)S

    move-result v3

    iput-short v3, p0, Lorg/apache/index/poi/hwpf/model/RevisionMarkAuthorTable;->fExtend:S

    .line 68
    add-int/lit8 p2, p2, 0x2

    .line 71
    invoke-static {p1, p2}, Lorg/apache/index/poi/util/LittleEndian;->getShort([BI)S

    move-result v3

    iput-short v3, p0, Lorg/apache/index/poi/hwpf/model/RevisionMarkAuthorTable;->cData:S

    .line 72
    add-int/lit8 p2, p2, 0x2

    .line 75
    invoke-static {p1, p2}, Lorg/apache/index/poi/util/LittleEndian;->getShort([BI)S

    move-result v3

    iput-short v3, p0, Lorg/apache/index/poi/hwpf/model/RevisionMarkAuthorTable;->cbExtra:S

    .line 79
    add-int/lit8 p2, p2, 0x2

    .line 81
    iget-short v3, p0, Lorg/apache/index/poi/hwpf/model/RevisionMarkAuthorTable;->cData:S

    new-array v3, v3, [Ljava/lang/String;

    iput-object v3, p0, Lorg/apache/index/poi/hwpf/model/RevisionMarkAuthorTable;->entries:[Ljava/lang/String;

    .line 82
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-short v3, p0, Lorg/apache/index/poi/hwpf/model/RevisionMarkAuthorTable;->cData:S

    if-lt v0, v3, :cond_0

    .line 91
    return-void

    .line 83
    :cond_0
    invoke-static {p1, p2}, Lorg/apache/index/poi/util/LittleEndian;->getShort([BI)S

    move-result v1

    .line 84
    .local v1, "len":I
    add-int/lit8 p2, p2, 0x2

    .line 86
    invoke-static {p1, p2, v1}, Lorg/apache/index/poi/util/StringUtil;->getFromUnicodeLE([BII)Ljava/lang/String;

    move-result-object v2

    .line 87
    .local v2, "name":Ljava/lang/String;
    mul-int/lit8 v3, v1, 0x2

    add-int/2addr p2, v3

    .line 89
    iget-object v3, p0, Lorg/apache/index/poi/hwpf/model/RevisionMarkAuthorTable;->entries:[Ljava/lang/String;

    aput-object v2, v3, v0

    .line 82
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public getAuthor(I)Ljava/lang/String;
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 108
    const/4 v0, 0x0

    .line 109
    .local v0, "auth":Ljava/lang/String;
    if-ltz p1, :cond_0

    iget-object v1, p0, Lorg/apache/index/poi/hwpf/model/RevisionMarkAuthorTable;->entries:[Ljava/lang/String;

    array-length v1, v1

    if-ge p1, v1, :cond_0

    .line 110
    iget-object v1, p0, Lorg/apache/index/poi/hwpf/model/RevisionMarkAuthorTable;->entries:[Ljava/lang/String;

    aget-object v0, v1, p1

    .line 112
    :cond_0
    return-object v0
.end method

.method public getEntries()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 99
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/RevisionMarkAuthorTable;->entries:[Ljava/lang/String;

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getSize()I
    .locals 1

    .prologue
    .line 121
    iget-short v0, p0, Lorg/apache/index/poi/hwpf/model/RevisionMarkAuthorTable;->cData:S

    return v0
.end method
