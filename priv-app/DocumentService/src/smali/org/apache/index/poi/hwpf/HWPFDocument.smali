.class public final Lorg/apache/index/poi/hwpf/HWPFDocument;
.super Lorg/apache/index/poi/hwpf/HWPFDocumentCore;
.source "HWPFDocument.java"


# instance fields
.field protected _cft:Lorg/apache/index/poi/hwpf/model/ComplexFileTable;

.field protected _cpSplit:Lorg/apache/index/poi/hwpf/model/CPSplitCalculator;

.field protected _dataStream:[B

.field protected _rmat:Lorg/apache/index/poi/hwpf/model/RevisionMarkAuthorTable;

.field protected _tableStream:[B

.field protected _tpt:Lorg/apache/index/poi/hwpf/model/TextPieceTable;

.field indexWriter:Lcom/samsung/index/ITextContentObs;


# direct methods
.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 86
    invoke-direct {p0}, Lorg/apache/index/poi/hwpf/HWPFDocumentCore;-><init>()V

    .line 87
    return-void
.end method

.method public constructor <init>(Lcom/samsung/index/ITextContentObs;Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;)V
    .locals 10
    .param p1, "indexwriterIn"    # Lcom/samsung/index/ITextContentObs;
    .param p2, "directory"    # Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 153
    invoke-direct {p0, p2}, Lorg/apache/index/poi/hwpf/HWPFDocumentCore;-><init>(Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;)V

    .line 155
    iput-object p1, p0, Lorg/apache/index/poi/hwpf/HWPFDocument;->indexWriter:Lcom/samsung/index/ITextContentObs;

    .line 158
    new-instance v0, Lorg/apache/index/poi/hwpf/model/CPSplitCalculator;

    iget-object v1, p0, Lorg/apache/index/poi/hwpf/HWPFDocument;->_fib:Lorg/apache/index/poi/hwpf/model/FileInformationBlock;

    invoke-direct {v0, v1}, Lorg/apache/index/poi/hwpf/model/CPSplitCalculator;-><init>(Lorg/apache/index/poi/hwpf/model/FileInformationBlock;)V

    iput-object v0, p0, Lorg/apache/index/poi/hwpf/HWPFDocument;->_cpSplit:Lorg/apache/index/poi/hwpf/model/CPSplitCalculator;

    .line 161
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/HWPFDocument;->_fib:Lorg/apache/index/poi/hwpf/model/FileInformationBlock;

    invoke-virtual {v0}, Lorg/apache/index/poi/hwpf/model/FileInformationBlock;->getNFib()I

    move-result v0

    const/16 v1, 0x6a

    if-ge v0, v1, :cond_0

    .line 162
    new-instance v0, Lorg/apache/index/poi/hwpf/OldWordFileFormatException;

    const-string/jumbo v1, "The document is too old - Word 95 or older. Try HWPFOldDocument instead?"

    invoke-direct {v0, v1}, Lorg/apache/index/poi/hwpf/OldWordFileFormatException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 166
    :cond_0
    const-string/jumbo v8, "0Table"

    .line 167
    .local v8, "name":Ljava/lang/String;
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/HWPFDocument;->_fib:Lorg/apache/index/poi/hwpf/model/FileInformationBlock;

    invoke-virtual {v0}, Lorg/apache/index/poi/hwpf/model/FileInformationBlock;->isFWhichTblStm()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 169
    const-string/jumbo v8, "1Table"

    .line 176
    :cond_1
    :try_start_0
    invoke-virtual {p2, v8}, Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;->getEntry(Ljava/lang/String;)Lorg/apache/index/poi/poifs/filesystem/Entry;

    move-result-object v9

    check-cast v9, Lorg/apache/index/poi/poifs/filesystem/DocumentEntry;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 185
    .local v9, "tableProps":Lorg/apache/index/poi/poifs/filesystem/DocumentEntry;
    invoke-interface {v9}, Lorg/apache/index/poi/poifs/filesystem/DocumentEntry;->getSize()I

    move-result v0

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/apache/index/poi/hwpf/HWPFDocument;->_tableStream:[B

    .line 186
    invoke-virtual {p2, v8}, Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;->createDocumentInputStream(Ljava/lang/String;)Lorg/apache/index/poi/poifs/filesystem/DocumentInputStream;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/index/poi/hwpf/HWPFDocument;->_tableStream:[B

    invoke-virtual {v0, v1}, Lorg/apache/index/poi/poifs/filesystem/DocumentInputStream;->read([B)I

    .line 188
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/HWPFDocument;->_fib:Lorg/apache/index/poi/hwpf/model/FileInformationBlock;

    iget-object v1, p0, Lorg/apache/index/poi/hwpf/HWPFDocument;->_mainStream:[B

    iget-object v2, p0, Lorg/apache/index/poi/hwpf/HWPFDocument;->_tableStream:[B

    invoke-virtual {v0, v1, v2}, Lorg/apache/index/poi/hwpf/model/FileInformationBlock;->fillVariableFields([B[B)V

    .line 208
    const/4 v6, 0x0

    .line 213
    .local v6, "fcMin":I
    new-instance v0, Lorg/apache/index/poi/hwpf/model/ComplexFileTable;

    iget-object v2, p0, Lorg/apache/index/poi/hwpf/HWPFDocument;->_mainStreamDoc:Lorg/apache/index/poi/poifs/filesystem/DocumentInputStream;

    iget-object v3, p0, Lorg/apache/index/poi/hwpf/HWPFDocument;->_mainStream:[B

    iget-object v4, p0, Lorg/apache/index/poi/hwpf/HWPFDocument;->_tableStream:[B

    iget-object v1, p0, Lorg/apache/index/poi/hwpf/HWPFDocument;->_fib:Lorg/apache/index/poi/hwpf/model/FileInformationBlock;

    invoke-virtual {v1}, Lorg/apache/index/poi/hwpf/model/FileInformationBlock;->getFcClx()I

    move-result v5

    move-object v1, p1

    invoke-direct/range {v0 .. v6}, Lorg/apache/index/poi/hwpf/model/ComplexFileTable;-><init>(Lcom/samsung/index/ITextContentObs;Lorg/apache/index/poi/poifs/filesystem/DocumentInputStream;[B[BII)V

    iput-object v0, p0, Lorg/apache/index/poi/hwpf/HWPFDocument;->_cft:Lorg/apache/index/poi/hwpf/model/ComplexFileTable;

    .line 218
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/HWPFDocument;->_cft:Lorg/apache/index/poi/hwpf/model/ComplexFileTable;

    invoke-virtual {v0}, Lorg/apache/index/poi/hwpf/model/ComplexFileTable;->getTextPieceTable()Lorg/apache/index/poi/hwpf/model/TextPieceTable;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/index/poi/hwpf/HWPFDocument;->_tpt:Lorg/apache/index/poi/hwpf/model/TextPieceTable;

    .line 273
    return-void

    .line 177
    .end local v6    # "fcMin":I
    .end local v9    # "tableProps":Lorg/apache/index/poi/poifs/filesystem/DocumentEntry;
    :catch_0
    move-exception v7

    .line 178
    .local v7, "fnfe":Ljava/io/FileNotFoundException;
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Table Stream \'"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\' wasn\'t found - Either the document is corrupt, or is Word95 (or earlier)"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public constructor <init>(Lcom/samsung/index/ITextContentObs;Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;)V
    .locals 1
    .param p1, "indexwriterIn"    # Lcom/samsung/index/ITextContentObs;
    .param p2, "pfilesystem"    # Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 111
    invoke-virtual {p2}, Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;->getRoot()Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lorg/apache/index/poi/hwpf/HWPFDocument;-><init>(Lcom/samsung/index/ITextContentObs;Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;)V

    .line 112
    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 1
    .param p1, "istream"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 99
    invoke-static {p1}, Lorg/apache/index/poi/hwpf/HWPFDocument;->verifyAndBuildPOIFS(Ljava/io/InputStream;)Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/apache/index/poi/hwpf/HWPFDocument;-><init>(Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;)V

    .line 100
    return-void
.end method

.method public constructor <init>(Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;)V
    .locals 1
    .param p1, "directory"    # Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 146
    const/4 v0, 0x0

    invoke-direct {p0, v0, p1}, Lorg/apache/index/poi/hwpf/HWPFDocument;-><init>(Lcom/samsung/index/ITextContentObs;Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;)V

    .line 147
    return-void
.end method

.method public constructor <init>(Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;)V
    .locals 0
    .param p1, "directory"    # Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;
    .param p2, "pfilesystem"    # Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 131
    invoke-direct {p0, p1}, Lorg/apache/index/poi/hwpf/HWPFDocument;-><init>(Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;)V

    .line 132
    return-void
.end method

.method public constructor <init>(Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;)V
    .locals 1
    .param p1, "pfilesystem"    # Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 116
    invoke-virtual {p1}, Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;->getRoot()Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/apache/index/poi/hwpf/HWPFDocument;-><init>(Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;)V

    .line 117
    return-void
.end method


# virtual methods
.method public characterLength()I
    .locals 5

    .prologue
    .line 375
    iget-object v4, p0, Lorg/apache/index/poi/hwpf/HWPFDocument;->_tpt:Lorg/apache/index/poi/hwpf/model/TextPieceTable;

    invoke-virtual {v4}, Lorg/apache/index/poi/hwpf/model/TextPieceTable;->getTextPieces()Ljava/util/List;

    move-result-object v2

    .line 376
    .local v2, "textPieces":Ljava/util/List;, "Ljava/util/List<Lorg/apache/index/poi/hwpf/model/TextPiece;>;"
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 378
    .local v1, "textIt":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/index/poi/hwpf/model/TextPiece;>;"
    const/4 v0, 0x0

    .line 379
    .local v0, "length":I
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_0

    .line 384
    return v0

    .line 381
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/index/poi/hwpf/model/TextPiece;

    .line 382
    .local v3, "tp":Lorg/apache/index/poi/hwpf/model/TextPiece;
    invoke-virtual {v3}, Lorg/apache/index/poi/hwpf/model/TextPiece;->characterLength()I

    move-result v4

    add-int/2addr v0, v4

    goto :goto_0
.end method

.method public getCPSplitCalculator()Lorg/apache/index/poi/hwpf/model/CPSplitCalculator;
    .locals 1

    .prologue
    .line 291
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/HWPFDocument;->_cpSplit:Lorg/apache/index/poi/hwpf/model/CPSplitCalculator;

    return-object v0
.end method

.method public getCommentsRange()Lorg/apache/index/poi/hwpf/usermodel/Range;
    .locals 3

    .prologue
    .line 349
    new-instance v0, Lorg/apache/index/poi/hwpf/usermodel/Range;

    .line 350
    iget-object v1, p0, Lorg/apache/index/poi/hwpf/HWPFDocument;->_cpSplit:Lorg/apache/index/poi/hwpf/model/CPSplitCalculator;

    invoke-virtual {v1}, Lorg/apache/index/poi/hwpf/model/CPSplitCalculator;->getCommentsStart()I

    move-result v1

    .line 351
    iget-object v2, p0, Lorg/apache/index/poi/hwpf/HWPFDocument;->_cpSplit:Lorg/apache/index/poi/hwpf/model/CPSplitCalculator;

    invoke-virtual {v2}, Lorg/apache/index/poi/hwpf/model/CPSplitCalculator;->getCommentsEnd()I

    move-result v2

    .line 349
    invoke-direct {v0, v1, v2, p0}, Lorg/apache/index/poi/hwpf/usermodel/Range;-><init>(IILorg/apache/index/poi/hwpf/HWPFDocumentCore;)V

    return-object v0
.end method

.method public getDataStream()[B
    .locals 1

    .prologue
    .line 416
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/HWPFDocument;->_dataStream:[B

    return-object v0
.end method

.method public getEndnoteRange()Lorg/apache/index/poi/hwpf/usermodel/Range;
    .locals 3

    .prologue
    .line 338
    new-instance v0, Lorg/apache/index/poi/hwpf/usermodel/Range;

    .line 339
    iget-object v1, p0, Lorg/apache/index/poi/hwpf/HWPFDocument;->_cpSplit:Lorg/apache/index/poi/hwpf/model/CPSplitCalculator;

    invoke-virtual {v1}, Lorg/apache/index/poi/hwpf/model/CPSplitCalculator;->getEndNoteStart()I

    move-result v1

    .line 340
    iget-object v2, p0, Lorg/apache/index/poi/hwpf/HWPFDocument;->_cpSplit:Lorg/apache/index/poi/hwpf/model/CPSplitCalculator;

    invoke-virtual {v2}, Lorg/apache/index/poi/hwpf/model/CPSplitCalculator;->getEndNoteEnd()I

    move-result v2

    .line 338
    invoke-direct {v0, v1, v2, p0}, Lorg/apache/index/poi/hwpf/usermodel/Range;-><init>(IILorg/apache/index/poi/hwpf/HWPFDocumentCore;)V

    return-object v0
.end method

.method public getFootnoteRange()Lorg/apache/index/poi/hwpf/usermodel/Range;
    .locals 3

    .prologue
    .line 327
    new-instance v0, Lorg/apache/index/poi/hwpf/usermodel/Range;

    .line 328
    iget-object v1, p0, Lorg/apache/index/poi/hwpf/HWPFDocument;->_cpSplit:Lorg/apache/index/poi/hwpf/model/CPSplitCalculator;

    invoke-virtual {v1}, Lorg/apache/index/poi/hwpf/model/CPSplitCalculator;->getFootnoteStart()I

    move-result v1

    .line 329
    iget-object v2, p0, Lorg/apache/index/poi/hwpf/HWPFDocument;->_cpSplit:Lorg/apache/index/poi/hwpf/model/CPSplitCalculator;

    invoke-virtual {v2}, Lorg/apache/index/poi/hwpf/model/CPSplitCalculator;->getFootnoteEnd()I

    move-result v2

    .line 327
    invoke-direct {v0, v1, v2, p0}, Lorg/apache/index/poi/hwpf/usermodel/Range;-><init>(IILorg/apache/index/poi/hwpf/HWPFDocumentCore;)V

    return-object v0
.end method

.method public getHeaderStoryRange()Lorg/apache/index/poi/hwpf/usermodel/Range;
    .locals 3

    .prologue
    .line 362
    new-instance v0, Lorg/apache/index/poi/hwpf/usermodel/Range;

    .line 363
    iget-object v1, p0, Lorg/apache/index/poi/hwpf/HWPFDocument;->_cpSplit:Lorg/apache/index/poi/hwpf/model/CPSplitCalculator;

    invoke-virtual {v1}, Lorg/apache/index/poi/hwpf/model/CPSplitCalculator;->getHeaderStoryStart()I

    move-result v1

    .line 364
    iget-object v2, p0, Lorg/apache/index/poi/hwpf/HWPFDocument;->_cpSplit:Lorg/apache/index/poi/hwpf/model/CPSplitCalculator;

    invoke-virtual {v2}, Lorg/apache/index/poi/hwpf/model/CPSplitCalculator;->getHeaderStoryEnd()I

    move-result v2

    .line 362
    invoke-direct {v0, v1, v2, p0}, Lorg/apache/index/poi/hwpf/usermodel/Range;-><init>(IILorg/apache/index/poi/hwpf/HWPFDocumentCore;)V

    return-object v0
.end method

.method public getOverallRange()Lorg/apache/index/poi/hwpf/usermodel/Range;
    .locals 4

    .prologue
    .line 301
    iget-object v1, p0, Lorg/apache/index/poi/hwpf/HWPFDocument;->_tpt:Lorg/apache/index/poi/hwpf/model/TextPieceTable;

    invoke-virtual {v1}, Lorg/apache/index/poi/hwpf/model/TextPieceTable;->getTextPieces()Ljava/util/List;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/index/poi/hwpf/HWPFDocument;->_tpt:Lorg/apache/index/poi/hwpf/model/TextPieceTable;

    invoke-virtual {v2}, Lorg/apache/index/poi/hwpf/model/TextPieceTable;->getTextPieces()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/index/poi/hwpf/model/PropertyNode;

    .line 303
    .local v0, "p":Lorg/apache/index/poi/hwpf/model/PropertyNode;
    new-instance v1, Lorg/apache/index/poi/hwpf/usermodel/Range;

    const/4 v2, 0x0

    invoke-virtual {v0}, Lorg/apache/index/poi/hwpf/model/PropertyNode;->getEnd()I

    move-result v3

    invoke-direct {v1, v2, v3, p0}, Lorg/apache/index/poi/hwpf/usermodel/Range;-><init>(IILorg/apache/index/poi/hwpf/HWPFDocumentCore;)V

    return-object v1
.end method

.method public getRange()Lorg/apache/index/poi/hwpf/usermodel/Range;
    .locals 3

    .prologue
    .line 313
    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/HWPFDocument;->getOverallRange()Lorg/apache/index/poi/hwpf/usermodel/Range;

    .line 316
    new-instance v0, Lorg/apache/index/poi/hwpf/usermodel/Range;

    .line 317
    iget-object v1, p0, Lorg/apache/index/poi/hwpf/HWPFDocument;->_cpSplit:Lorg/apache/index/poi/hwpf/model/CPSplitCalculator;

    invoke-virtual {v1}, Lorg/apache/index/poi/hwpf/model/CPSplitCalculator;->getMainDocumentStart()I

    move-result v1

    .line 318
    iget-object v2, p0, Lorg/apache/index/poi/hwpf/HWPFDocument;->_cpSplit:Lorg/apache/index/poi/hwpf/model/CPSplitCalculator;

    invoke-virtual {v2}, Lorg/apache/index/poi/hwpf/model/CPSplitCalculator;->getMainDocumentEnd()I

    move-result v2

    .line 316
    invoke-direct {v0, v1, v2, p0}, Lorg/apache/index/poi/hwpf/usermodel/Range;-><init>(IILorg/apache/index/poi/hwpf/HWPFDocumentCore;)V

    return-object v0
.end method

.method public getRevisionMarkAuthorTable()Lorg/apache/index/poi/hwpf/model/RevisionMarkAuthorTable;
    .locals 1

    .prologue
    .line 404
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/HWPFDocument;->_rmat:Lorg/apache/index/poi/hwpf/model/RevisionMarkAuthorTable;

    return-object v0
.end method

.method public getTableStream()[B
    .locals 1

    .prologue
    .line 420
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/HWPFDocument;->_tableStream:[B

    return-object v0
.end method

.method public getText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 282
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/HWPFDocument;->_tpt:Lorg/apache/index/poi/hwpf/model/TextPieceTable;

    if-eqz v0, :cond_0

    .line 283
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/HWPFDocument;->_tpt:Lorg/apache/index/poi/hwpf/model/TextPieceTable;

    invoke-virtual {v0}, Lorg/apache/index/poi/hwpf/model/TextPieceTable;->getText()Ljava/lang/String;

    move-result-object v0

    .line 285
    :goto_0
    return-object v0

    :cond_0
    const-string/jumbo v0, ""

    goto :goto_0
.end method

.method public getTextTable()Lorg/apache/index/poi/hwpf/model/TextPieceTable;
    .locals 1

    .prologue
    .line 278
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/HWPFDocument;->_cft:Lorg/apache/index/poi/hwpf/model/ComplexFileTable;

    invoke-virtual {v0}, Lorg/apache/index/poi/hwpf/model/ComplexFileTable;->getTextPieceTable()Lorg/apache/index/poi/hwpf/model/TextPieceTable;

    move-result-object v0

    return-object v0
.end method
