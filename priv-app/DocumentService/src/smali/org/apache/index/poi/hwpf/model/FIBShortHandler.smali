.class public final Lorg/apache/index/poi/hwpf/model/FIBShortHandler;
.super Ljava/lang/Object;
.source "FIBShortHandler.java"


# static fields
.field public static final LIDFE:I = 0xd

.field public static final MAGICCREATED:I = 0x0

.field public static final MAGICCREATEDPRIVATE:I = 0x2

.field public static final MAGICREVISED:I = 0x1

.field public static final MAGICREVISEDPRIVATE:I = 0x3

.field static final START:I = 0x20


# instance fields
.field _shorts:[S


# direct methods
.method public constructor <init>([B)V
    .locals 5
    .param p1, "mainStream"    # [B

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    const/16 v0, 0x20

    .line 41
    .local v0, "offset":I
    invoke-static {p1, v0}, Lorg/apache/index/poi/util/LittleEndian;->getShort([BI)S

    move-result v1

    .line 42
    .local v1, "shortCount":I
    add-int/lit8 v0, v0, 0x2

    .line 43
    new-array v3, v1, [S

    iput-object v3, p0, Lorg/apache/index/poi/hwpf/model/FIBShortHandler;->_shorts:[S

    .line 45
    const/4 v2, 0x0

    .local v2, "x":I
    :goto_0
    if-lt v2, v1, :cond_0

    .line 50
    return-void

    .line 47
    :cond_0
    iget-object v3, p0, Lorg/apache/index/poi/hwpf/model/FIBShortHandler;->_shorts:[S

    invoke-static {p1, v0}, Lorg/apache/index/poi/util/LittleEndian;->getShort([BI)S

    move-result v4

    aput-short v4, v3, v2

    .line 48
    add-int/lit8 v0, v0, 0x2

    .line 45
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method


# virtual methods
.method public getShort(I)S
    .locals 1
    .param p1, "shortCode"    # I

    .prologue
    .line 54
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/FIBShortHandler;->_shorts:[S

    aget-short v0, v0, p1

    return v0
.end method

.method serialize([B)V
    .locals 3
    .param p1, "mainStream"    # [B

    .prologue
    .line 63
    const/16 v0, 0x20

    .line 64
    .local v0, "offset":I
    iget-object v2, p0, Lorg/apache/index/poi/hwpf/model/FIBShortHandler;->_shorts:[S

    array-length v2, v2

    int-to-short v2, v2

    invoke-static {p1, v0, v2}, Lorg/apache/index/poi/util/LittleEndian;->putShort([BIS)V

    .line 65
    add-int/lit8 v0, v0, 0x2

    .line 68
    const/4 v1, 0x0

    .local v1, "x":I
    :goto_0
    iget-object v2, p0, Lorg/apache/index/poi/hwpf/model/FIBShortHandler;->_shorts:[S

    array-length v2, v2

    if-lt v1, v2, :cond_0

    .line 73
    return-void

    .line 70
    :cond_0
    iget-object v2, p0, Lorg/apache/index/poi/hwpf/model/FIBShortHandler;->_shorts:[S

    aget-short v2, v2, v1

    invoke-static {p1, v0, v2}, Lorg/apache/index/poi/util/LittleEndian;->putShort([BIS)V

    .line 71
    add-int/lit8 v0, v0, 0x2

    .line 68
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method sizeInBytes()I
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/FIBShortHandler;->_shorts:[S

    array-length v0, v0

    mul-int/lit8 v0, v0, 0x2

    add-int/lit8 v0, v0, 0x2

    return v0
.end method
