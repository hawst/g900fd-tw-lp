.class public final Lorg/apache/index/poi/hslf/record/TextRulerAtom;
.super Lorg/apache/index/poi/hslf/record/RecordAtom;
.source "TextRulerAtom.java"


# instance fields
.field private _data:[B

.field private _header:[B

.field private bulletOffsets:[I

.field private defaultTabSize:I

.field private numLevels:I

.field private tabStops:[I

.field private textOffsets:[I


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v1, 0x5

    .line 53
    invoke-direct {p0}, Lorg/apache/index/poi/hslf/record/RecordAtom;-><init>()V

    .line 47
    new-array v0, v1, [I

    iput-object v0, p0, Lorg/apache/index/poi/hslf/record/TextRulerAtom;->bulletOffsets:[I

    .line 48
    new-array v0, v1, [I

    iput-object v0, p0, Lorg/apache/index/poi/hslf/record/TextRulerAtom;->textOffsets:[I

    .line 54
    const/16 v0, 0x8

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/apache/index/poi/hslf/record/TextRulerAtom;->_header:[B

    .line 55
    const/4 v0, 0x0

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/apache/index/poi/hslf/record/TextRulerAtom;->_data:[B

    .line 57
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/TextRulerAtom;->_header:[B

    const/4 v1, 0x2

    invoke-virtual {p0}, Lorg/apache/index/poi/hslf/record/TextRulerAtom;->getRecordType()J

    move-result-wide v2

    long-to-int v2, v2

    int-to-short v2, v2

    invoke-static {v0, v1, v2}, Lorg/apache/index/poi/util/LittleEndian;->putShort([BIS)V

    .line 58
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/TextRulerAtom;->_header:[B

    const/4 v1, 0x4

    iget-object v2, p0, Lorg/apache/index/poi/hslf/record/TextRulerAtom;->_data:[B

    array-length v2, v2

    invoke-static {v0, v1, v2}, Lorg/apache/index/poi/util/LittleEndian;->putInt([BII)V

    .line 59
    return-void
.end method

.method protected constructor <init>([BII)V
    .locals 5
    .param p1, "source"    # [B
    .param p2, "start"    # I
    .param p3, "len"    # I

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x5

    const/4 v4, 0x0

    .line 69
    invoke-direct {p0}, Lorg/apache/index/poi/hslf/record/RecordAtom;-><init>()V

    .line 47
    new-array v1, v2, [I

    iput-object v1, p0, Lorg/apache/index/poi/hslf/record/TextRulerAtom;->bulletOffsets:[I

    .line 48
    new-array v1, v2, [I

    iput-object v1, p0, Lorg/apache/index/poi/hslf/record/TextRulerAtom;->textOffsets:[I

    .line 71
    new-array v1, v3, [B

    iput-object v1, p0, Lorg/apache/index/poi/hslf/record/TextRulerAtom;->_header:[B

    .line 72
    iget-object v1, p0, Lorg/apache/index/poi/hslf/record/TextRulerAtom;->_header:[B

    invoke-static {p1, p2, v1, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 75
    add-int/lit8 v1, p3, -0x8

    new-array v1, v1, [B

    iput-object v1, p0, Lorg/apache/index/poi/hslf/record/TextRulerAtom;->_data:[B

    .line 76
    add-int/lit8 v1, p2, 0x8

    iget-object v2, p0, Lorg/apache/index/poi/hslf/record/TextRulerAtom;->_data:[B

    add-int/lit8 v3, p3, -0x8

    invoke-static {p1, v1, v2, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 79
    :try_start_0
    invoke-direct {p0}, Lorg/apache/index/poi/hslf/record/TextRulerAtom;->read()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 83
    :goto_0
    return-void

    .line 80
    :catch_0
    move-exception v0

    .line 81
    .local v0, "e":Ljava/lang/Exception;
    iget-object v1, p0, Lorg/apache/index/poi/hslf/record/TextRulerAtom;->logger:Lorg/apache/index/poi/util/POILogger;

    sget v2, Lorg/apache/index/poi/util/POILogger;->ERROR:I

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "Failed to parse TextRulerAtom: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lorg/apache/index/poi/util/POILogger;->log(ILjava/lang/Object;)V

    goto :goto_0
.end method

.method public static getParagraphInstance()Lorg/apache/index/poi/hslf/record/TextRulerAtom;
    .locals 9

    .prologue
    const/16 v8, 0x41

    const/16 v7, 0x10

    const/16 v6, 0xf

    const/4 v5, 0x3

    const/4 v4, 0x1

    .line 192
    const/16 v2, 0x12

    new-array v0, v2, [B

    const/4 v2, 0x2

    .line 193
    const/16 v3, -0x5a

    aput-byte v3, v0, v2

    aput-byte v6, v0, v5

    const/4 v2, 0x4

    const/16 v3, 0xa

    aput-byte v3, v0, v2

    const/16 v2, 0x8

    .line 194
    aput-byte v7, v0, v2

    const/16 v2, 0x9

    aput-byte v5, v0, v2

    const/16 v2, 0xc

    const/4 v3, -0x7

    aput-byte v3, v0, v2

    const/16 v2, 0xe

    aput-byte v8, v0, v2

    aput-byte v4, v0, v6

    aput-byte v8, v0, v7

    const/16 v2, 0x11

    aput-byte v4, v0, v2

    .line 196
    .local v0, "data":[B
    new-instance v1, Lorg/apache/index/poi/hslf/record/TextRulerAtom;

    const/4 v2, 0x0

    array-length v3, v0

    invoke-direct {v1, v0, v2, v3}, Lorg/apache/index/poi/hslf/record/TextRulerAtom;-><init>([BII)V

    .line 197
    .local v1, "ruler":Lorg/apache/index/poi/hslf/record/TextRulerAtom;
    return-object v1
.end method

.method private read()V
    .locals 12

    .prologue
    const/4 v11, 0x5

    const/4 v10, 0x4

    const/4 v9, 0x3

    const/4 v7, 0x2

    const/4 v8, 0x1

    .line 110
    const/4 v4, 0x0

    .line 111
    .local v4, "pos":I
    iget-object v6, p0, Lorg/apache/index/poi/hslf/record/TextRulerAtom;->_data:[B

    invoke-static {v6}, Lorg/apache/index/poi/util/LittleEndian;->getShort([B)S

    move-result v3

    .local v3, "mask":S
    add-int/lit8 v4, v4, 0x4

    .line 113
    const/16 v6, 0xd

    new-array v0, v6, [I

    const/4 v6, 0x0

    aput v8, v0, v6

    aput v7, v0, v7

    aput v9, v0, v9

    const/16 v6, 0x8

    aput v6, v0, v10

    aput v10, v0, v11

    const/4 v6, 0x6

    const/16 v7, 0x9

    aput v7, v0, v6

    const/4 v6, 0x7

    aput v11, v0, v6

    const/16 v6, 0x8

    const/16 v7, 0xa

    aput v7, v0, v6

    const/16 v6, 0x9

    const/4 v7, 0x6

    aput v7, v0, v6

    const/16 v6, 0xa

    const/16 v7, 0xb

    aput v7, v0, v6

    const/16 v6, 0xb

    const/4 v7, 0x7

    aput v7, v0, v6

    const/16 v6, 0xc

    const/16 v7, 0xc

    aput v7, v0, v6

    .line 114
    .local v0, "bits":[I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v6, v0

    if-lt v1, v6, :cond_0

    .line 154
    return-void

    .line 115
    :cond_0
    aget v6, v0, v1

    shl-int v6, v8, v6

    and-int/2addr v6, v3

    if-eqz v6, :cond_1

    .line 116
    aget v6, v0, v1

    packed-switch v6, :pswitch_data_0

    .line 114
    :cond_1
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 119
    :pswitch_0
    iget-object v6, p0, Lorg/apache/index/poi/hslf/record/TextRulerAtom;->_data:[B

    invoke-static {v6, v4}, Lorg/apache/index/poi/util/LittleEndian;->getShort([BI)S

    move-result v6

    iput v6, p0, Lorg/apache/index/poi/hslf/record/TextRulerAtom;->defaultTabSize:I

    add-int/lit8 v4, v4, 0x2

    .line 120
    goto :goto_1

    .line 123
    :pswitch_1
    iget-object v6, p0, Lorg/apache/index/poi/hslf/record/TextRulerAtom;->_data:[B

    invoke-static {v6, v4}, Lorg/apache/index/poi/util/LittleEndian;->getShort([BI)S

    move-result v6

    iput v6, p0, Lorg/apache/index/poi/hslf/record/TextRulerAtom;->numLevels:I

    add-int/lit8 v4, v4, 0x2

    .line 124
    goto :goto_1

    .line 127
    :pswitch_2
    iget-object v6, p0, Lorg/apache/index/poi/hslf/record/TextRulerAtom;->_data:[B

    invoke-static {v6, v4}, Lorg/apache/index/poi/util/LittleEndian;->getShort([BI)S

    move-result v5

    .local v5, "val":S
    add-int/lit8 v4, v4, 0x2

    .line 128
    mul-int/lit8 v6, v5, 0x2

    new-array v6, v6, [I

    iput-object v6, p0, Lorg/apache/index/poi/hslf/record/TextRulerAtom;->tabStops:[I

    .line 129
    const/4 v2, 0x0

    .local v2, "j":I
    :goto_2
    iget-object v6, p0, Lorg/apache/index/poi/hslf/record/TextRulerAtom;->tabStops:[I

    array-length v6, v6

    if-ge v2, v6, :cond_1

    .line 130
    iget-object v6, p0, Lorg/apache/index/poi/hslf/record/TextRulerAtom;->tabStops:[I

    iget-object v7, p0, Lorg/apache/index/poi/hslf/record/TextRulerAtom;->_data:[B

    invoke-static {v7, v4}, Lorg/apache/index/poi/util/LittleEndian;->getUShort([BI)I

    move-result v7

    aput v7, v6, v2

    add-int/lit8 v4, v4, 0x2

    .line 129
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 139
    .end local v2    # "j":I
    .end local v5    # "val":S
    :pswitch_3
    iget-object v6, p0, Lorg/apache/index/poi/hslf/record/TextRulerAtom;->_data:[B

    invoke-static {v6, v4}, Lorg/apache/index/poi/util/LittleEndian;->getShort([BI)S

    move-result v5

    .restart local v5    # "val":S
    add-int/lit8 v4, v4, 0x2

    .line 140
    iget-object v6, p0, Lorg/apache/index/poi/hslf/record/TextRulerAtom;->bulletOffsets:[I

    aget v7, v0, v1

    add-int/lit8 v7, v7, -0x3

    aput v5, v6, v7

    goto :goto_1

    .line 148
    .end local v5    # "val":S
    :pswitch_4
    iget-object v6, p0, Lorg/apache/index/poi/hslf/record/TextRulerAtom;->_data:[B

    invoke-static {v6, v4}, Lorg/apache/index/poi/util/LittleEndian;->getShort([BI)S

    move-result v5

    .restart local v5    # "val":S
    add-int/lit8 v4, v4, 0x2

    .line 149
    iget-object v6, p0, Lorg/apache/index/poi/hslf/record/TextRulerAtom;->textOffsets:[I

    aget v7, v0, v1

    add-int/lit8 v7, v7, -0x8

    aput v5, v6, v7

    goto :goto_1

    .line 116
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
    .end packed-switch
.end method


# virtual methods
.method public getBulletOffsets()[I
    .locals 1

    .prologue
    .line 188
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/TextRulerAtom;->bulletOffsets:[I

    return-object v0
.end method

.method public getDefaultTabSize()I
    .locals 1

    .prologue
    .line 160
    iget v0, p0, Lorg/apache/index/poi/hslf/record/TextRulerAtom;->defaultTabSize:I

    return v0
.end method

.method public getNumberOfLevels()I
    .locals 1

    .prologue
    .line 167
    iget v0, p0, Lorg/apache/index/poi/hslf/record/TextRulerAtom;->numLevels:I

    return v0
.end method

.method public getRecordType()J
    .locals 2

    .prologue
    .line 91
    sget-object v0, Lorg/apache/index/poi/hslf/record/RecordTypes;->TextRulerAtom:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    iget v0, v0, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;->typeID:I

    int-to-long v0, v0

    return-wide v0
.end method

.method public getTabStops()[I
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/TextRulerAtom;->tabStops:[I

    return-object v0
.end method

.method public getTextOffsets()[I
    .locals 1

    .prologue
    .line 181
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/TextRulerAtom;->textOffsets:[I

    return-object v0
.end method

.method public setParagraphIndent(SS)V
    .locals 2
    .param p1, "tetxOffset"    # S
    .param p2, "bulletOffset"    # S

    .prologue
    .line 201
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/TextRulerAtom;->_data:[B

    const/4 v1, 0x4

    invoke-static {v0, v1, p1}, Lorg/apache/index/poi/util/LittleEndian;->putShort([BIS)V

    .line 202
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/TextRulerAtom;->_data:[B

    const/4 v1, 0x6

    invoke-static {v0, v1, p2}, Lorg/apache/index/poi/util/LittleEndian;->putShort([BIS)V

    .line 203
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/TextRulerAtom;->_data:[B

    const/16 v1, 0x8

    invoke-static {v0, v1, p2}, Lorg/apache/index/poi/util/LittleEndian;->putShort([BIS)V

    .line 204
    return-void
.end method

.method public writeOut(Ljava/io/OutputStream;)V
    .locals 1
    .param p1, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 102
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/TextRulerAtom;->_header:[B

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 103
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/TextRulerAtom;->_data:[B

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 104
    return-void
.end method
