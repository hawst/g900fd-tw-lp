.class public final Lorg/apache/index/poi/hssf/usermodel/HSSFWorkbook;
.super Lorg/apache/index/poi/POIDocument;
.source "HSSFWorkbook.java"


# static fields
.field public static final INITIAL_CAPACITY:I = 0x3

.field public static PARSE_TEXT_INSIDE_SHAPE:Z = false

.field private static final TAG:Ljava/lang/String; = "HSSFWorkbook"

.field private static final WORKBOOK_DIR_ENTRY_NAMES:[Ljava/lang/String;


# instance fields
.field private indexWriterhssf:Lcom/samsung/index/ITextContentObs;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 55
    sput-boolean v2, Lorg/apache/index/poi/hssf/usermodel/HSSFWorkbook;->PARSE_TEXT_INSIDE_SHAPE:Z

    .line 163
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "Workbook"

    aput-object v1, v0, v2

    const/4 v1, 0x1

    .line 167
    const-string/jumbo v2, "WORKBOOK"

    aput-object v2, v0, v1

    .line 163
    sput-object v0, Lorg/apache/index/poi/hssf/usermodel/HSSFWorkbook;->WORKBOOK_DIR_ENTRY_NAMES:[Ljava/lang/String;

    .line 167
    return-void
.end method

.method public constructor <init>(Lcom/samsung/index/ITextContentObs;Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;Z)V
    .locals 0
    .param p1, "inwriter"    # Lcom/samsung/index/ITextContentObs;
    .param p2, "directory"    # Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;
    .param p3, "fs"    # Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;
    .param p4, "preserveNodes"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 218
    invoke-direct {p0, p1, p2, p4}, Lorg/apache/index/poi/hssf/usermodel/HSSFWorkbook;-><init>(Lcom/samsung/index/ITextContentObs;Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;Z)V

    .line 219
    return-void
.end method

.method public constructor <init>(Lcom/samsung/index/ITextContentObs;Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;Z)V
    .locals 3
    .param p1, "inwriter"    # Lcom/samsung/index/ITextContentObs;
    .param p2, "directory"    # Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;
    .param p3, "preserveNodes"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 250
    invoke-direct {p0, p2}, Lorg/apache/index/poi/POIDocument;-><init>(Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;)V

    .line 252
    iput-object p1, p0, Lorg/apache/index/poi/hssf/usermodel/HSSFWorkbook;->indexWriterhssf:Lcom/samsung/index/ITextContentObs;

    .line 255
    invoke-static {p2}, Lorg/apache/index/poi/hssf/usermodel/HSSFWorkbook;->getWorkbookDirEntryName(Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;)Ljava/lang/String;

    move-result-object v1

    .line 259
    .local v1, "workbookName":Ljava/lang/String;
    if-nez p3, :cond_0

    .line 260
    const/4 v2, 0x0

    iput-object v2, p0, Lorg/apache/index/poi/hssf/usermodel/HSSFWorkbook;->directory:Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;

    .line 263
    :cond_0
    invoke-virtual {p2, v1}, Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;->createDocumentInputStream(Ljava/lang/String;)Lorg/apache/index/poi/poifs/filesystem/DocumentInputStream;

    move-result-object v0

    .line 265
    .local v0, "documentInputStream":Lorg/apache/index/poi/poifs/filesystem/DocumentInputStream;
    iget-object v2, p0, Lorg/apache/index/poi/hssf/usermodel/HSSFWorkbook;->indexWriterhssf:Lcom/samsung/index/ITextContentObs;

    .line 264
    invoke-static {v0, v2}, Lorg/apache/index/poi/hssf/record/RecordFactory;->createRecords(Ljava/io/InputStream;Lcom/samsung/index/ITextContentObs;)Ljava/util/List;

    .line 266
    const/4 v0, 0x0

    .line 312
    return-void
.end method

.method public constructor <init>(Lcom/samsung/index/ITextContentObs;Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;)V
    .locals 1
    .param p1, "inwriter"    # Lcom/samsung/index/ITextContentObs;
    .param p2, "fs"    # Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 127
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/index/poi/hssf/usermodel/HSSFWorkbook;-><init>(Lcom/samsung/index/ITextContentObs;Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;Z)V

    .line 128
    return-void
.end method

.method public constructor <init>(Lcom/samsung/index/ITextContentObs;Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;Z)V
    .locals 1
    .param p1, "inwriter"    # Lcom/samsung/index/ITextContentObs;
    .param p2, "fs"    # Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;
    .param p3, "preserveNodes"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 151
    invoke-virtual {p2}, Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;->getRoot()Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2, p3}, Lorg/apache/index/poi/hssf/usermodel/HSSFWorkbook;-><init>(Lcom/samsung/index/ITextContentObs;Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;Z)V

    .line 152
    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 1
    .param p1, "s"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 315
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lorg/apache/index/poi/hssf/usermodel/HSSFWorkbook;-><init>(Ljava/io/InputStream;Z)V

    .line 316
    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;Z)V
    .locals 2
    .param p1, "s"    # Ljava/io/InputStream;
    .param p2, "preserveNodes"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 353
    new-instance v0, Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;-><init>(Ljava/io/InputStream;Ljava/io/File;)V

    invoke-direct {p0, v0, p2}, Lorg/apache/index/poi/hssf/usermodel/HSSFWorkbook;-><init>(Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;Z)V

    .line 354
    return-void
.end method

.method public constructor <init>(Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;Z)V
    .locals 0
    .param p1, "directory"    # Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;
    .param p2, "fs"    # Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;
    .param p3, "preserveNodes"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 223
    invoke-direct {p0, p1, p3}, Lorg/apache/index/poi/hssf/usermodel/HSSFWorkbook;-><init>(Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;Z)V

    .line 224
    return-void
.end method

.method public constructor <init>(Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;Z)V
    .locals 1
    .param p1, "directory"    # Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;
    .param p2, "preserveNodes"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 245
    const/4 v0, 0x0

    invoke-direct {p0, v0, p1, p2}, Lorg/apache/index/poi/hssf/usermodel/HSSFWorkbook;-><init>(Lcom/samsung/index/ITextContentObs;Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;Z)V

    .line 246
    return-void
.end method

.method public constructor <init>(Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;)V
    .locals 1
    .param p1, "fs"    # Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 131
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lorg/apache/index/poi/hssf/usermodel/HSSFWorkbook;-><init>(Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;Z)V

    .line 132
    return-void
.end method

.method public constructor <init>(Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;Z)V
    .locals 1
    .param p1, "fs"    # Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;
    .param p2, "preserveNodes"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 156
    invoke-virtual {p1}, Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;->getRoot()Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;

    move-result-object v0

    invoke-direct {p0, v0, p1, p2}, Lorg/apache/index/poi/hssf/usermodel/HSSFWorkbook;-><init>(Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;Z)V

    .line 157
    return-void
.end method

.method private static getWorkbookDirEntryName(Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;)Ljava/lang/String;
    .locals 7
    .param p0, "directory"    # Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;

    .prologue
    .line 171
    sget-object v2, Lorg/apache/index/poi/hssf/usermodel/HSSFWorkbook;->WORKBOOK_DIR_ENTRY_NAMES:[Ljava/lang/String;

    .line 172
    .local v2, "potentialNames":[Ljava/lang/String;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v4, v2

    if-lt v1, v4, :cond_0

    .line 185
    :try_start_0
    const-string/jumbo v4, "Book"

    invoke-virtual {p0, v4}, Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;->getEntry(Ljava/lang/String;)Lorg/apache/index/poi/poifs/filesystem/Entry;

    .line 186
    new-instance v4, Lorg/apache/index/poi/hssf/OldExcelFormatException;

    .line 187
    const-string/jumbo v5, "The supplied spreadsheet seems to be Excel 5.0/7.0 (BIFF5) format. POI only supports BIFF8 format (from Excel versions 97/2000/XP/2003)"

    .line 186
    invoke-direct {v4, v5}, Lorg/apache/index/poi/hssf/OldExcelFormatException;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 189
    :catch_0
    move-exception v0

    .line 190
    .local v0, "e":Ljava/io/FileNotFoundException;
    const-string/jumbo v4, "HSSFWorkbook"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "FileNotFoundException :"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 194
    new-instance v4, Ljava/lang/IllegalArgumentException;

    .line 195
    const-string/jumbo v5, "The supplied POIFSFileSystem does not contain a BIFF8 \'Workbook\' entry. Is it really an excel file?"

    .line 194
    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 173
    .end local v0    # "e":Ljava/io/FileNotFoundException;
    :cond_0
    aget-object v3, v2, v1

    .line 175
    .local v3, "wbName":Ljava/lang/String;
    :try_start_1
    invoke-virtual {p0, v3}, Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;->getEntry(Ljava/lang/String;)Lorg/apache/index/poi/poifs/filesystem/Entry;
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    .line 176
    return-object v3

    .line 177
    :catch_1
    move-exception v0

    .line 178
    .restart local v0    # "e":Ljava/io/FileNotFoundException;
    const-string/jumbo v4, "HSSFWorkbook"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "FileNotFoundException :"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 172
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method
