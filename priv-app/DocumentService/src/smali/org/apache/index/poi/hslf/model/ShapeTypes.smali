.class public final Lorg/apache/index/poi/hslf/model/ShapeTypes;
.super Ljava/lang/Object;
.source "ShapeTypes.java"

# interfaces
.implements Lorg/apache/index/poi/sl/usermodel/ShapeTypes;


# static fields
.field public static types:Ljava/util/HashMap;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    .line 43
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    sput-object v4, Lorg/apache/index/poi/hslf/model/ShapeTypes;->types:Ljava/util/HashMap;

    .line 45
    :try_start_0
    const-class v4, Lorg/apache/index/poi/sl/usermodel/ShapeTypes;

    invoke-virtual {v4}, Ljava/lang/Class;->getFields()[Ljava/lang/reflect/Field;

    move-result-object v1

    .line 46
    .local v1, "f":[Ljava/lang/reflect/Field;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v4, v1

    if-lt v2, v4, :cond_0

    .line 55
    return-void

    .line 47
    :cond_0
    aget-object v4, v1, v2

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    .line 48
    .local v3, "val":Ljava/lang/Object;
    instance-of v4, v3, Ljava/lang/Integer;

    if-eqz v4, :cond_1

    .line 49
    sget-object v4, Lorg/apache/index/poi/hslf/model/ShapeTypes;->types:Ljava/util/HashMap;

    aget-object v5, v1, v2

    invoke-virtual {v5}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v3, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0

    .line 46
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 52
    .end local v3    # "val":Ljava/lang/Object;
    :catch_0
    move-exception v0

    .line 53
    .local v0, "e":Ljava/lang/IllegalAccessException;
    new-instance v4, Lorg/apache/index/poi/hslf/exceptions/HSLFException;

    const-string/jumbo v5, "Failed to initialize shape types"

    invoke-direct {v4, v5}, Lorg/apache/index/poi/hslf/exceptions/HSLFException;-><init>(Ljava/lang/String;)V

    throw v4
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static typeName(I)Ljava/lang/String;
    .locals 3
    .param p0, "type"    # I

    .prologue
    .line 37
    sget-object v1, Lorg/apache/index/poi/hslf/model/ShapeTypes;->types:Ljava/util/HashMap;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 38
    .local v0, "name":Ljava/lang/String;
    return-object v0
.end method
