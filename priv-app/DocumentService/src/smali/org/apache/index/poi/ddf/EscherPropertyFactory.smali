.class public final Lorg/apache/index/poi/ddf/EscherPropertyFactory;
.super Ljava/lang/Object;
.source "EscherPropertyFactory.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createProperties([BIS)Ljava/util/List;
    .locals 14
    .param p1, "data"    # [B
    .param p2, "offset"    # I
    .param p3, "numProperties"    # S
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([BIS)",
            "Ljava/util/List",
            "<",
            "Lorg/apache/index/poi/ddf/EscherProperty;",
            ">;"
        }
    .end annotation

    .prologue
    .line 40
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 42
    .local v11, "results":Ljava/util/List;, "Ljava/util/List<Lorg/apache/index/poi/ddf/EscherProperty;>;"
    move/from16 v6, p2

    .line 45
    .local v6, "pos":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    move/from16 v0, p3

    if-lt v2, v0, :cond_1

    .line 76
    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/index/poi/ddf/EscherProperty;>;"
    :cond_0
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-nez v12, :cond_7

    .line 88
    return-object v11

    .line 48
    .end local v4    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/index/poi/ddf/EscherProperty;>;"
    :cond_1
    invoke-static {p1, v6}, Lorg/apache/index/poi/util/LittleEndian;->getShort([BI)S

    move-result v8

    .line 49
    .local v8, "propId":S
    add-int/lit8 v12, v6, 0x2

    invoke-static {p1, v12}, Lorg/apache/index/poi/util/LittleEndian;->getInt([BI)I

    move-result v7

    .line 50
    .local v7, "propData":I
    and-int/lit16 v12, v8, 0x3fff

    int-to-short v9, v12

    .line 51
    .local v9, "propNumber":S
    and-int/lit16 v12, v8, -0x8000

    if-eqz v12, :cond_2

    const/4 v3, 0x1

    .line 54
    .local v3, "isComplex":Z
    :goto_2
    invoke-static {v9}, Lorg/apache/index/poi/ddf/EscherProperties;->getPropertyType(S)B

    move-result v10

    .line 55
    .local v10, "propertyType":B
    const/4 v12, 0x1

    if-ne v10, v12, :cond_3

    .line 56
    new-instance v12, Lorg/apache/index/poi/ddf/EscherBoolProperty;

    invoke-direct {v12, v8, v7}, Lorg/apache/index/poi/ddf/EscherBoolProperty;-><init>(SI)V

    invoke-interface {v11, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 71
    :goto_3
    add-int/lit8 v6, v6, 0x6

    .line 45
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 51
    .end local v3    # "isComplex":Z
    .end local v10    # "propertyType":B
    :cond_2
    const/4 v3, 0x0

    goto :goto_2

    .line 57
    .restart local v3    # "isComplex":Z
    .restart local v10    # "propertyType":B
    :cond_3
    const/4 v12, 0x3

    if-ne v10, v12, :cond_4

    .line 58
    new-instance v12, Lorg/apache/index/poi/ddf/EscherShapePathProperty;

    invoke-direct {v12, v8, v7}, Lorg/apache/index/poi/ddf/EscherShapePathProperty;-><init>(SI)V

    invoke-interface {v11, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 61
    :cond_4
    if-nez v3, :cond_5

    .line 62
    new-instance v12, Lorg/apache/index/poi/ddf/EscherSimpleProperty;

    invoke-direct {v12, v8, v7}, Lorg/apache/index/poi/ddf/EscherSimpleProperty;-><init>(SI)V

    invoke-interface {v11, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 65
    :cond_5
    const/4 v12, 0x5

    if-ne v10, v12, :cond_6

    .line 66
    new-instance v12, Lorg/apache/index/poi/ddf/EscherArrayProperty;

    new-array v13, v7, [B

    invoke-direct {v12, v8, v13}, Lorg/apache/index/poi/ddf/EscherArrayProperty;-><init>(S[B)V

    invoke-interface {v11, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 68
    :cond_6
    new-instance v12, Lorg/apache/index/poi/ddf/EscherComplexProperty;

    new-array v13, v7, [B

    invoke-direct {v12, v8, v13}, Lorg/apache/index/poi/ddf/EscherComplexProperty;-><init>(S[B)V

    invoke-interface {v11, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 77
    .end local v3    # "isComplex":Z
    .end local v7    # "propData":I
    .end local v8    # "propId":S
    .end local v9    # "propNumber":S
    .end local v10    # "propertyType":B
    .restart local v4    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/index/poi/ddf/EscherProperty;>;"
    :cond_7
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/apache/index/poi/ddf/EscherProperty;

    .line 78
    .local v5, "p":Lorg/apache/index/poi/ddf/EscherProperty;
    instance-of v12, v5, Lorg/apache/index/poi/ddf/EscherComplexProperty;

    if-eqz v12, :cond_0

    .line 79
    instance-of v12, v5, Lorg/apache/index/poi/ddf/EscherArrayProperty;

    if-eqz v12, :cond_8

    .line 80
    check-cast v5, Lorg/apache/index/poi/ddf/EscherArrayProperty;

    .end local v5    # "p":Lorg/apache/index/poi/ddf/EscherProperty;
    invoke-virtual {v5, p1, v6}, Lorg/apache/index/poi/ddf/EscherArrayProperty;->setArrayData([BI)I

    move-result v12

    add-int/2addr v6, v12

    .line 81
    goto :goto_1

    .line 82
    .restart local v5    # "p":Lorg/apache/index/poi/ddf/EscherProperty;
    :cond_8
    check-cast v5, Lorg/apache/index/poi/ddf/EscherComplexProperty;

    .end local v5    # "p":Lorg/apache/index/poi/ddf/EscherProperty;
    invoke-virtual {v5}, Lorg/apache/index/poi/ddf/EscherComplexProperty;->getComplexData()[B

    move-result-object v1

    .line 83
    .local v1, "complexData":[B
    const/4 v12, 0x0

    array-length v13, v1

    invoke-static {p1, v6, v1, v12, v13}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 84
    array-length v12, v1

    add-int/2addr v6, v12

    goto/16 :goto_1
.end method
