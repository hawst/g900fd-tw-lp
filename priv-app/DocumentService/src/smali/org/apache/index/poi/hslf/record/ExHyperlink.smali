.class public Lorg/apache/index/poi/hslf/record/ExHyperlink;
.super Lorg/apache/index/poi/hslf/record/RecordContainer;
.source "ExHyperlink.java"


# static fields
.field private static _type:J


# instance fields
.field private _header:[B

.field private linkAtom:Lorg/apache/index/poi/hslf/record/ExHyperlinkAtom;

.field private linkDetailsA:Lorg/apache/index/poi/hslf/record/CString;

.field private linkDetailsB:Lorg/apache/index/poi/hslf/record/CString;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 28
    const-wide/16 v0, 0xfd7

    sput-wide v0, Lorg/apache/index/poi/hslf/record/ExHyperlink;->_type:J

    return-void
.end method

.method public constructor <init>()V
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x0

    .line 127
    invoke-direct {p0}, Lorg/apache/index/poi/hslf/record/RecordContainer;-><init>()V

    .line 128
    const/16 v2, 0x8

    new-array v2, v2, [B

    iput-object v2, p0, Lorg/apache/index/poi/hslf/record/ExHyperlink;->_header:[B

    .line 129
    const/4 v2, 0x3

    new-array v2, v2, [Lorg/apache/index/poi/hslf/record/Record;

    iput-object v2, p0, Lorg/apache/index/poi/hslf/record/ExHyperlink;->_children:[Lorg/apache/index/poi/hslf/record/Record;

    .line 132
    iget-object v2, p0, Lorg/apache/index/poi/hslf/record/ExHyperlink;->_header:[B

    const/16 v3, 0xf

    aput-byte v3, v2, v6

    .line 133
    iget-object v2, p0, Lorg/apache/index/poi/hslf/record/ExHyperlink;->_header:[B

    sget-wide v4, Lorg/apache/index/poi/hslf/record/ExHyperlink;->_type:J

    long-to-int v3, v4

    int-to-short v3, v3

    invoke-static {v2, v7, v3}, Lorg/apache/index/poi/util/LittleEndian;->putShort([BIS)V

    .line 136
    new-instance v0, Lorg/apache/index/poi/hslf/record/CString;

    invoke-direct {v0}, Lorg/apache/index/poi/hslf/record/CString;-><init>()V

    .line 137
    .local v0, "csa":Lorg/apache/index/poi/hslf/record/CString;
    new-instance v1, Lorg/apache/index/poi/hslf/record/CString;

    invoke-direct {v1}, Lorg/apache/index/poi/hslf/record/CString;-><init>()V

    .line 138
    .local v1, "csb":Lorg/apache/index/poi/hslf/record/CString;
    invoke-virtual {v0, v6}, Lorg/apache/index/poi/hslf/record/CString;->setOptions(I)V

    .line 139
    const/16 v2, 0x10

    invoke-virtual {v1, v2}, Lorg/apache/index/poi/hslf/record/CString;->setOptions(I)V

    .line 140
    iget-object v2, p0, Lorg/apache/index/poi/hslf/record/ExHyperlink;->_children:[Lorg/apache/index/poi/hslf/record/Record;

    new-instance v3, Lorg/apache/index/poi/hslf/record/ExHyperlinkAtom;

    invoke-direct {v3}, Lorg/apache/index/poi/hslf/record/ExHyperlinkAtom;-><init>()V

    aput-object v3, v2, v6

    .line 141
    iget-object v2, p0, Lorg/apache/index/poi/hslf/record/ExHyperlink;->_children:[Lorg/apache/index/poi/hslf/record/Record;

    const/4 v3, 0x1

    aput-object v0, v2, v3

    .line 142
    iget-object v2, p0, Lorg/apache/index/poi/hslf/record/ExHyperlink;->_children:[Lorg/apache/index/poi/hslf/record/Record;

    aput-object v1, v2, v7

    .line 143
    invoke-direct {p0}, Lorg/apache/index/poi/hslf/record/ExHyperlink;->findInterestingChildren()V

    .line 144
    return-void
.end method

.method protected constructor <init>([BII)V
    .locals 3
    .param p1, "source"    # [B
    .param p2, "start"    # I
    .param p3, "len"    # I

    .prologue
    const/16 v2, 0x8

    .line 89
    invoke-direct {p0}, Lorg/apache/index/poi/hslf/record/RecordContainer;-><init>()V

    .line 91
    new-array v0, v2, [B

    iput-object v0, p0, Lorg/apache/index/poi/hslf/record/ExHyperlink;->_header:[B

    .line 92
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/ExHyperlink;->_header:[B

    const/4 v1, 0x0

    invoke-static {p1, p2, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 95
    add-int/lit8 v0, p2, 0x8

    add-int/lit8 v1, p3, -0x8

    invoke-static {p1, v0, v1}, Lorg/apache/index/poi/hslf/record/Record;->findChildRecords([BII)[Lorg/apache/index/poi/hslf/record/Record;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/index/poi/hslf/record/ExHyperlink;->_children:[Lorg/apache/index/poi/hslf/record/Record;

    .line 96
    invoke-direct {p0}, Lorg/apache/index/poi/hslf/record/ExHyperlink;->findInterestingChildren()V

    .line 97
    return-void
.end method

.method private findInterestingChildren()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 107
    iget-object v1, p0, Lorg/apache/index/poi/hslf/record/ExHyperlink;->_children:[Lorg/apache/index/poi/hslf/record/Record;

    aget-object v1, v1, v5

    instance-of v1, v1, Lorg/apache/index/poi/hslf/record/ExHyperlinkAtom;

    if-eqz v1, :cond_0

    .line 108
    iget-object v1, p0, Lorg/apache/index/poi/hslf/record/ExHyperlink;->_children:[Lorg/apache/index/poi/hslf/record/Record;

    aget-object v1, v1, v5

    check-cast v1, Lorg/apache/index/poi/hslf/record/ExHyperlinkAtom;

    iput-object v1, p0, Lorg/apache/index/poi/hslf/record/ExHyperlink;->linkAtom:Lorg/apache/index/poi/hslf/record/ExHyperlinkAtom;

    .line 113
    :goto_0
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_1
    iget-object v1, p0, Lorg/apache/index/poi/hslf/record/ExHyperlink;->_children:[Lorg/apache/index/poi/hslf/record/Record;

    array-length v1, v1

    if-lt v0, v1, :cond_1

    .line 122
    return-void

    .line 110
    .end local v0    # "i":I
    :cond_0
    iget-object v1, p0, Lorg/apache/index/poi/hslf/record/ExHyperlink;->logger:Lorg/apache/index/poi/util/POILogger;

    sget v2, Lorg/apache/index/poi/util/POILogger;->ERROR:I

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "First child record wasn\'t a ExHyperlinkAtom, was of type "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lorg/apache/index/poi/hslf/record/ExHyperlink;->_children:[Lorg/apache/index/poi/hslf/record/Record;

    aget-object v4, v4, v5

    invoke-virtual {v4}, Lorg/apache/index/poi/hslf/record/Record;->getRecordType()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lorg/apache/index/poi/util/POILogger;->log(ILjava/lang/Object;)V

    goto :goto_0

    .line 114
    .restart local v0    # "i":I
    :cond_1
    iget-object v1, p0, Lorg/apache/index/poi/hslf/record/ExHyperlink;->_children:[Lorg/apache/index/poi/hslf/record/Record;

    aget-object v1, v1, v0

    instance-of v1, v1, Lorg/apache/index/poi/hslf/record/CString;

    if-eqz v1, :cond_3

    .line 115
    iget-object v1, p0, Lorg/apache/index/poi/hslf/record/ExHyperlink;->linkDetailsA:Lorg/apache/index/poi/hslf/record/CString;

    if-nez v1, :cond_2

    iget-object v1, p0, Lorg/apache/index/poi/hslf/record/ExHyperlink;->_children:[Lorg/apache/index/poi/hslf/record/Record;

    aget-object v1, v1, v0

    check-cast v1, Lorg/apache/index/poi/hslf/record/CString;

    iput-object v1, p0, Lorg/apache/index/poi/hslf/record/ExHyperlink;->linkDetailsA:Lorg/apache/index/poi/hslf/record/CString;

    .line 113
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 116
    :cond_2
    iget-object v1, p0, Lorg/apache/index/poi/hslf/record/ExHyperlink;->_children:[Lorg/apache/index/poi/hslf/record/Record;

    aget-object v1, v1, v0

    check-cast v1, Lorg/apache/index/poi/hslf/record/CString;

    iput-object v1, p0, Lorg/apache/index/poi/hslf/record/ExHyperlink;->linkDetailsB:Lorg/apache/index/poi/hslf/record/CString;

    goto :goto_2

    .line 118
    :cond_3
    iget-object v1, p0, Lorg/apache/index/poi/hslf/record/ExHyperlink;->logger:Lorg/apache/index/poi/util/POILogger;

    sget v2, Lorg/apache/index/poi/util/POILogger;->ERROR:I

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "Record after ExHyperlinkAtom wasn\'t a CString, was of type "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lorg/apache/index/poi/hslf/record/ExHyperlink;->_children:[Lorg/apache/index/poi/hslf/record/Record;

    const/4 v5, 0x1

    aget-object v4, v4, v5

    invoke-virtual {v4}, Lorg/apache/index/poi/hslf/record/Record;->getRecordType()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lorg/apache/index/poi/util/POILogger;->log(ILjava/lang/Object;)V

    goto :goto_2
.end method


# virtual methods
.method public _getDetailsA()Ljava/lang/String;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/ExHyperlink;->linkDetailsA:Lorg/apache/index/poi/hslf/record/CString;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/ExHyperlink;->linkDetailsA:Lorg/apache/index/poi/hslf/record/CString;

    invoke-virtual {v0}, Lorg/apache/index/poi/hslf/record/CString;->getText()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public _getDetailsB()Ljava/lang/String;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/ExHyperlink;->linkDetailsB:Lorg/apache/index/poi/hslf/record/CString;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/ExHyperlink;->linkDetailsB:Lorg/apache/index/poi/hslf/record/CString;

    invoke-virtual {v0}, Lorg/apache/index/poi/hslf/record/CString;->getText()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getExHyperlinkAtom()Lorg/apache/index/poi/hslf/record/ExHyperlinkAtom;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/ExHyperlink;->linkAtom:Lorg/apache/index/poi/hslf/record/ExHyperlinkAtom;

    return-object v0
.end method

.method public getLinkTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/ExHyperlink;->linkDetailsA:Lorg/apache/index/poi/hslf/record/CString;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/ExHyperlink;->linkDetailsA:Lorg/apache/index/poi/hslf/record/CString;

    invoke-virtual {v0}, Lorg/apache/index/poi/hslf/record/CString;->getText()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getLinkURL()Ljava/lang/String;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/ExHyperlink;->linkDetailsB:Lorg/apache/index/poi/hslf/record/CString;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/ExHyperlink;->linkDetailsB:Lorg/apache/index/poi/hslf/record/CString;

    invoke-virtual {v0}, Lorg/apache/index/poi/hslf/record/CString;->getText()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getRecordType()J
    .locals 2

    .prologue
    .line 149
    sget-wide v0, Lorg/apache/index/poi/hslf/record/ExHyperlink;->_type:J

    return-wide v0
.end method

.method public setLinkTitle(Ljava/lang/String;)V
    .locals 1
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 68
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/ExHyperlink;->linkDetailsA:Lorg/apache/index/poi/hslf/record/CString;

    if-eqz v0, :cond_0

    .line 69
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/ExHyperlink;->linkDetailsA:Lorg/apache/index/poi/hslf/record/CString;

    invoke-virtual {v0, p1}, Lorg/apache/index/poi/hslf/record/CString;->setText(Ljava/lang/String;)V

    .line 71
    :cond_0
    return-void
.end method

.method public setLinkURL(Ljava/lang/String;)V
    .locals 1
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 63
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/ExHyperlink;->linkDetailsB:Lorg/apache/index/poi/hslf/record/CString;

    if-eqz v0, :cond_0

    .line 64
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/ExHyperlink;->linkDetailsB:Lorg/apache/index/poi/hslf/record/CString;

    invoke-virtual {v0, p1}, Lorg/apache/index/poi/hslf/record/CString;->setText(Ljava/lang/String;)V

    .line 66
    :cond_0
    return-void
.end method
