.class public final Lorg/apache/index/poi/hslf/record/TxInteractiveInfoAtom;
.super Lorg/apache/index/poi/hslf/record/RecordAtom;
.source "TxInteractiveInfoAtom.java"


# instance fields
.field private _data:[B

.field private _header:[B


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/16 v1, 0x8

    .line 44
    invoke-direct {p0}, Lorg/apache/index/poi/hslf/record/RecordAtom;-><init>()V

    .line 45
    new-array v0, v1, [B

    iput-object v0, p0, Lorg/apache/index/poi/hslf/record/TxInteractiveInfoAtom;->_header:[B

    .line 46
    new-array v0, v1, [B

    iput-object v0, p0, Lorg/apache/index/poi/hslf/record/TxInteractiveInfoAtom;->_data:[B

    .line 48
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/TxInteractiveInfoAtom;->_header:[B

    const/4 v1, 0x2

    invoke-virtual {p0}, Lorg/apache/index/poi/hslf/record/TxInteractiveInfoAtom;->getRecordType()J

    move-result-wide v2

    long-to-int v2, v2

    int-to-short v2, v2

    invoke-static {v0, v1, v2}, Lorg/apache/index/poi/util/LittleEndian;->putShort([BIS)V

    .line 49
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/TxInteractiveInfoAtom;->_header:[B

    const/4 v1, 0x4

    iget-object v2, p0, Lorg/apache/index/poi/hslf/record/TxInteractiveInfoAtom;->_data:[B

    array-length v2, v2

    invoke-static {v0, v1, v2}, Lorg/apache/index/poi/util/LittleEndian;->putInt([BII)V

    .line 50
    return-void
.end method

.method protected constructor <init>([BII)V
    .locals 4
    .param p1, "source"    # [B
    .param p2, "start"    # I
    .param p3, "len"    # I

    .prologue
    const/16 v1, 0x8

    const/4 v3, 0x0

    .line 60
    invoke-direct {p0}, Lorg/apache/index/poi/hslf/record/RecordAtom;-><init>()V

    .line 62
    new-array v0, v1, [B

    iput-object v0, p0, Lorg/apache/index/poi/hslf/record/TxInteractiveInfoAtom;->_header:[B

    .line 63
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/TxInteractiveInfoAtom;->_header:[B

    invoke-static {p1, p2, v0, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 66
    add-int/lit8 v0, p3, -0x8

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/apache/index/poi/hslf/record/TxInteractiveInfoAtom;->_data:[B

    .line 67
    add-int/lit8 v0, p2, 0x8

    iget-object v1, p0, Lorg/apache/index/poi/hslf/record/TxInteractiveInfoAtom;->_data:[B

    add-int/lit8 v2, p3, -0x8

    invoke-static {p1, v0, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 69
    return-void
.end method


# virtual methods
.method public getEndIndex()I
    .locals 2

    .prologue
    .line 94
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/TxInteractiveInfoAtom;->_data:[B

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lorg/apache/index/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    return v0
.end method

.method public getRecordType()J
    .locals 2

    .prologue
    .line 110
    sget-object v0, Lorg/apache/index/poi/hslf/record/RecordTypes;->TxInteractiveInfoAtom:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    iget v0, v0, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;->typeID:I

    int-to-long v0, v0

    return-wide v0
.end method

.method public getStartIndex()I
    .locals 2

    .prologue
    .line 77
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/TxInteractiveInfoAtom;->_data:[B

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lorg/apache/index/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    return v0
.end method

.method public setEndIndex(I)V
    .locals 2
    .param p1, "idx"    # I

    .prologue
    .line 103
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/TxInteractiveInfoAtom;->_data:[B

    const/4 v1, 0x4

    invoke-static {v0, v1, p1}, Lorg/apache/index/poi/util/LittleEndian;->putInt([BII)V

    .line 104
    return-void
.end method

.method public setStartIndex(I)V
    .locals 2
    .param p1, "idx"    # I

    .prologue
    .line 85
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/TxInteractiveInfoAtom;->_data:[B

    const/4 v1, 0x0

    invoke-static {v0, v1, p1}, Lorg/apache/index/poi/util/LittleEndian;->putInt([BII)V

    .line 86
    return-void
.end method

.method public writeOut(Ljava/io/OutputStream;)V
    .locals 1
    .param p1, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 120
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/TxInteractiveInfoAtom;->_header:[B

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 121
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/TxInteractiveInfoAtom;->_data:[B

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 122
    return-void
.end method
