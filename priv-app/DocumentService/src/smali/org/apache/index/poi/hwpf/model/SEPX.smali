.class public final Lorg/apache/index/poi/hwpf/model/SEPX;
.super Lorg/apache/index/poi/hwpf/model/BytePropertyNode;
.source "SEPX.java"


# instance fields
.field _sed:Lorg/apache/index/poi/hwpf/model/SectionDescriptor;


# direct methods
.method public constructor <init>(Lorg/apache/index/poi/hwpf/model/SectionDescriptor;IILorg/apache/index/poi/hwpf/model/CharIndexTranslator;[B)V
    .locals 1
    .param p1, "sed"    # Lorg/apache/index/poi/hwpf/model/SectionDescriptor;
    .param p2, "start"    # I
    .param p3, "end"    # I
    .param p4, "translator"    # Lorg/apache/index/poi/hwpf/model/CharIndexTranslator;
    .param p5, "grpprl"    # [B

    .prologue
    .line 33
    const/4 v0, 0x0

    invoke-static {p5, v0}, Lorg/apache/index/poi/hwpf/sprm/SectionSprmUncompressor;->uncompressSEP([BI)Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;

    move-result-object v0

    invoke-direct {p0, p2, p3, p4, v0}, Lorg/apache/index/poi/hwpf/model/BytePropertyNode;-><init>(IILorg/apache/index/poi/hwpf/model/CharIndexTranslator;Ljava/lang/Object;)V

    .line 34
    iput-object p1, p0, Lorg/apache/index/poi/hwpf/model/SEPX;->_sed:Lorg/apache/index/poi/hwpf/model/SectionDescriptor;

    .line 35
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 55
    instance-of v1, p1, Lorg/apache/index/poi/hwpf/model/SEPX;

    if-eqz v1, :cond_0

    move-object v0, p1

    .line 56
    check-cast v0, Lorg/apache/index/poi/hwpf/model/SEPX;

    .line 57
    .local v0, "sepx":Lorg/apache/index/poi/hwpf/model/SEPX;
    invoke-super {p0, p1}, Lorg/apache/index/poi/hwpf/model/BytePropertyNode;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 59
    iget-object v1, v0, Lorg/apache/index/poi/hwpf/model/SEPX;->_sed:Lorg/apache/index/poi/hwpf/model/SectionDescriptor;

    iget-object v2, p0, Lorg/apache/index/poi/hwpf/model/SEPX;->_sed:Lorg/apache/index/poi/hwpf/model/SectionDescriptor;

    invoke-virtual {v1, v2}, Lorg/apache/index/poi/hwpf/model/SectionDescriptor;->equals(Ljava/lang/Object;)Z

    move-result v1

    .line 63
    .end local v0    # "sepx":Lorg/apache/index/poi/hwpf/model/SEPX;
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getGrpprl()[B
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/SEPX;->_buf:Ljava/lang/Object;

    check-cast v0, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;

    invoke-static {v0}, Lorg/apache/index/poi/hwpf/sprm/SectionSprmCompressor;->compressSectionProperty(Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;)[B

    move-result-object v0

    return-object v0
.end method

.method public getSectionDescriptor()Lorg/apache/index/poi/hwpf/model/SectionDescriptor;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/SEPX;->_sed:Lorg/apache/index/poi/hwpf/model/SectionDescriptor;

    return-object v0
.end method

.method public getSectionProperties()Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/SEPX;->_buf:Ljava/lang/Object;

    check-cast v0, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;

    return-object v0
.end method
