.class public final Lorg/apache/index/poi/ddf/EscherArrayProperty;
.super Lorg/apache/index/poi/ddf/EscherComplexProperty;
.source "EscherArrayProperty.java"


# static fields
.field private static final FIXED_SIZE:I = 0x6


# instance fields
.field private emptyComplexPart:Z

.field private sizeIncludesHeaderSize:Z


# direct methods
.method public constructor <init>(SZ[B)V
    .locals 1
    .param p1, "propertyNumber"    # S
    .param p2, "isBlipId"    # Z
    .param p3, "complexData"    # [B

    .prologue
    .line 53
    invoke-static {p3}, Lorg/apache/index/poi/ddf/EscherArrayProperty;->checkComplexData([B)[B

    move-result-object v0

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/index/poi/ddf/EscherComplexProperty;-><init>(SZ[B)V

    .line 40
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/index/poi/ddf/EscherArrayProperty;->sizeIncludesHeaderSize:Z

    .line 45
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/index/poi/ddf/EscherArrayProperty;->emptyComplexPart:Z

    .line 54
    return-void
.end method

.method public constructor <init>(S[B)V
    .locals 3
    .param p1, "id"    # S
    .param p2, "complexData"    # [B

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 48
    invoke-static {p2}, Lorg/apache/index/poi/ddf/EscherArrayProperty;->checkComplexData([B)[B

    move-result-object v2

    invoke-direct {p0, p1, v2}, Lorg/apache/index/poi/ddf/EscherComplexProperty;-><init>(S[B)V

    .line 40
    iput-boolean v0, p0, Lorg/apache/index/poi/ddf/EscherArrayProperty;->sizeIncludesHeaderSize:Z

    .line 45
    iput-boolean v1, p0, Lorg/apache/index/poi/ddf/EscherArrayProperty;->emptyComplexPart:Z

    .line 49
    array-length v2, p2

    if-nez v2, :cond_0

    :goto_0
    iput-boolean v0, p0, Lorg/apache/index/poi/ddf/EscherArrayProperty;->emptyComplexPart:Z

    .line 50
    return-void

    :cond_0
    move v0, v1

    .line 49
    goto :goto_0
.end method

.method private static checkComplexData([B)[B
    .locals 1
    .param p0, "complexData"    # [B

    .prologue
    .line 57
    if-eqz p0, :cond_0

    array-length v0, p0

    if-nez v0, :cond_1

    .line 58
    :cond_0
    const/4 v0, 0x6

    new-array p0, v0, [B

    .line 61
    .end local p0    # "complexData":[B
    :cond_1
    return-object p0
.end method

.method public static getActualSizeOfElements(S)I
    .locals 1
    .param p0, "sizeOfElements"    # S

    .prologue
    .line 188
    if-gez p0, :cond_0

    .line 189
    neg-int v0, p0

    shr-int/lit8 v0, v0, 0x2

    int-to-short p0, v0

    .line 191
    .end local p0    # "sizeOfElements":S
    :cond_0
    return p0
.end method


# virtual methods
.method public getElement(I)[B
    .locals 6
    .param p1, "index"    # I

    .prologue
    .line 109
    invoke-virtual {p0}, Lorg/apache/index/poi/ddf/EscherArrayProperty;->getSizeOfElements()S

    move-result v2

    invoke-static {v2}, Lorg/apache/index/poi/ddf/EscherArrayProperty;->getActualSizeOfElements(S)I

    move-result v0

    .line 110
    .local v0, "actualSize":I
    new-array v1, v0, [B

    .line 111
    .local v1, "result":[B
    iget-object v2, p0, Lorg/apache/index/poi/ddf/EscherArrayProperty;->_complexData:[B

    mul-int v3, p1, v0

    add-int/lit8 v3, v3, 0x6

    const/4 v4, 0x0

    array-length v5, v1

    invoke-static {v2, v3, v1, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 112
    return-object v1
.end method

.method public getNumberOfElementsInArray()I
    .locals 2

    .prologue
    .line 65
    iget-object v0, p0, Lorg/apache/index/poi/ddf/EscherArrayProperty;->_complexData:[B

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lorg/apache/index/poi/util/LittleEndian;->getUShort([BI)I

    move-result v0

    return v0
.end method

.method public getNumberOfElementsInMemory()I
    .locals 2

    .prologue
    .line 79
    iget-object v0, p0, Lorg/apache/index/poi/ddf/EscherArrayProperty;->_complexData:[B

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lorg/apache/index/poi/util/LittleEndian;->getUShort([BI)I

    move-result v0

    return v0
.end method

.method public getSizeOfElements()S
    .locals 2

    .prologue
    .line 93
    iget-object v0, p0, Lorg/apache/index/poi/ddf/EscherArrayProperty;->_complexData:[B

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lorg/apache/index/poi/util/LittleEndian;->getShort([BI)S

    move-result v0

    return v0
.end method

.method public serializeSimplePart([BI)I
    .locals 2
    .param p1, "data"    # [B
    .param p2, "pos"    # I

    .prologue
    .line 174
    invoke-virtual {p0}, Lorg/apache/index/poi/ddf/EscherArrayProperty;->getId()S

    move-result v1

    invoke-static {p1, p2, v1}, Lorg/apache/index/poi/util/LittleEndian;->putShort([BIS)V

    .line 175
    iget-object v1, p0, Lorg/apache/index/poi/ddf/EscherArrayProperty;->_complexData:[B

    array-length v0, v1

    .line 176
    .local v0, "recordSize":I
    iget-boolean v1, p0, Lorg/apache/index/poi/ddf/EscherArrayProperty;->sizeIncludesHeaderSize:Z

    if-nez v1, :cond_0

    .line 177
    add-int/lit8 v0, v0, -0x6

    .line 179
    :cond_0
    add-int/lit8 v1, p2, 0x2

    invoke-static {p1, v1, v0}, Lorg/apache/index/poi/util/LittleEndian;->putInt([BII)V

    .line 180
    const/4 v1, 0x6

    return v1
.end method

.method public setArrayData([BI)I
    .locals 6
    .param p1, "data"    # [B
    .param p2, "offset"    # I

    .prologue
    const/4 v5, 0x0

    .line 149
    iget-boolean v3, p0, Lorg/apache/index/poi/ddf/EscherArrayProperty;->emptyComplexPart:Z

    if-eqz v3, :cond_0

    .line 150
    new-array v3, v5, [B

    iput-object v3, p0, Lorg/apache/index/poi/ddf/EscherArrayProperty;->_complexData:[B

    .line 164
    :goto_0
    iget-object v3, p0, Lorg/apache/index/poi/ddf/EscherArrayProperty;->_complexData:[B

    array-length v3, v3

    return v3

    .line 152
    :cond_0
    invoke-static {p1, p2}, Lorg/apache/index/poi/util/LittleEndian;->getShort([BI)S

    move-result v1

    .line 153
    .local v1, "numElements":S
    add-int/lit8 v3, p2, 0x2

    invoke-static {p1, v3}, Lorg/apache/index/poi/util/LittleEndian;->getShort([BI)S

    .line 154
    add-int/lit8 v3, p2, 0x4

    invoke-static {p1, v3}, Lorg/apache/index/poi/util/LittleEndian;->getShort([BI)S

    move-result v2

    .line 156
    .local v2, "sizeOfElements":S
    invoke-static {v2}, Lorg/apache/index/poi/ddf/EscherArrayProperty;->getActualSizeOfElements(S)I

    move-result v3

    mul-int v0, v3, v1

    .line 157
    .local v0, "arraySize":I
    iget-object v3, p0, Lorg/apache/index/poi/ddf/EscherArrayProperty;->_complexData:[B

    array-length v3, v3

    if-ne v0, v3, :cond_1

    .line 159
    add-int/lit8 v3, v0, 0x6

    new-array v3, v3, [B

    iput-object v3, p0, Lorg/apache/index/poi/ddf/EscherArrayProperty;->_complexData:[B

    .line 160
    iput-boolean v5, p0, Lorg/apache/index/poi/ddf/EscherArrayProperty;->sizeIncludesHeaderSize:Z

    .line 162
    :cond_1
    iget-object v3, p0, Lorg/apache/index/poi/ddf/EscherArrayProperty;->_complexData:[B

    iget-object v4, p0, Lorg/apache/index/poi/ddf/EscherArrayProperty;->_complexData:[B

    array-length v4, v4

    invoke-static {p1, p2, v3, v5, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_0
.end method

.method public setElement(I[B)V
    .locals 4
    .param p1, "index"    # I
    .param p2, "element"    # [B

    .prologue
    .line 116
    invoke-virtual {p0}, Lorg/apache/index/poi/ddf/EscherArrayProperty;->getSizeOfElements()S

    move-result v1

    invoke-static {v1}, Lorg/apache/index/poi/ddf/EscherArrayProperty;->getActualSizeOfElements(S)I

    move-result v0

    .line 117
    .local v0, "actualSize":I
    const/4 v1, 0x0

    iget-object v2, p0, Lorg/apache/index/poi/ddf/EscherArrayProperty;->_complexData:[B

    mul-int v3, p1, v0

    add-int/lit8 v3, v3, 0x6

    invoke-static {p2, v1, v2, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 118
    return-void
.end method

.method public setNumberOfElementsInArray(I)V
    .locals 5
    .param p1, "numberOfElements"    # I

    .prologue
    const/4 v4, 0x0

    .line 69
    invoke-virtual {p0}, Lorg/apache/index/poi/ddf/EscherArrayProperty;->getSizeOfElements()S

    move-result v2

    invoke-static {v2}, Lorg/apache/index/poi/ddf/EscherArrayProperty;->getActualSizeOfElements(S)I

    move-result v2

    mul-int/2addr v2, p1

    add-int/lit8 v0, v2, 0x6

    .line 70
    .local v0, "expectedArraySize":I
    iget-object v2, p0, Lorg/apache/index/poi/ddf/EscherArrayProperty;->_complexData:[B

    array-length v2, v2

    if-eq v0, v2, :cond_0

    .line 71
    new-array v1, v0, [B

    .line 72
    .local v1, "newArray":[B
    iget-object v2, p0, Lorg/apache/index/poi/ddf/EscherArrayProperty;->_complexData:[B

    iget-object v3, p0, Lorg/apache/index/poi/ddf/EscherArrayProperty;->_complexData:[B

    array-length v3, v3

    invoke-static {v2, v4, v1, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 73
    iput-object v1, p0, Lorg/apache/index/poi/ddf/EscherArrayProperty;->_complexData:[B

    .line 75
    .end local v1    # "newArray":[B
    :cond_0
    iget-object v2, p0, Lorg/apache/index/poi/ddf/EscherArrayProperty;->_complexData:[B

    int-to-short v3, p1

    invoke-static {v2, v4, v3}, Lorg/apache/index/poi/util/LittleEndian;->putShort([BIS)V

    .line 76
    return-void
.end method

.method public setNumberOfElementsInMemory(I)V
    .locals 5
    .param p1, "numberOfElements"    # I

    .prologue
    const/4 v3, 0x0

    .line 83
    invoke-virtual {p0}, Lorg/apache/index/poi/ddf/EscherArrayProperty;->getSizeOfElements()S

    move-result v2

    invoke-static {v2}, Lorg/apache/index/poi/ddf/EscherArrayProperty;->getActualSizeOfElements(S)I

    move-result v2

    mul-int/2addr v2, p1

    add-int/lit8 v0, v2, 0x6

    .line 84
    .local v0, "expectedArraySize":I
    iget-object v2, p0, Lorg/apache/index/poi/ddf/EscherArrayProperty;->_complexData:[B

    array-length v2, v2

    if-eq v0, v2, :cond_0

    .line 85
    new-array v1, v0, [B

    .line 86
    .local v1, "newArray":[B
    iget-object v2, p0, Lorg/apache/index/poi/ddf/EscherArrayProperty;->_complexData:[B

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 87
    iput-object v1, p0, Lorg/apache/index/poi/ddf/EscherArrayProperty;->_complexData:[B

    .line 89
    .end local v1    # "newArray":[B
    :cond_0
    iget-object v2, p0, Lorg/apache/index/poi/ddf/EscherArrayProperty;->_complexData:[B

    const/4 v3, 0x2

    int-to-short v4, p1

    invoke-static {v2, v3, v4}, Lorg/apache/index/poi/util/LittleEndian;->putShort([BIS)V

    .line 90
    return-void
.end method

.method public setSizeOfElements(I)V
    .locals 6
    .param p1, "sizeOfElements"    # I

    .prologue
    const/4 v5, 0x0

    .line 97
    iget-object v2, p0, Lorg/apache/index/poi/ddf/EscherArrayProperty;->_complexData:[B

    const/4 v3, 0x4

    int-to-short v4, p1

    invoke-static {v2, v3, v4}, Lorg/apache/index/poi/util/LittleEndian;->putShort([BIS)V

    .line 99
    invoke-virtual {p0}, Lorg/apache/index/poi/ddf/EscherArrayProperty;->getNumberOfElementsInArray()I

    move-result v2

    invoke-virtual {p0}, Lorg/apache/index/poi/ddf/EscherArrayProperty;->getSizeOfElements()S

    move-result v3

    invoke-static {v3}, Lorg/apache/index/poi/ddf/EscherArrayProperty;->getActualSizeOfElements(S)I

    move-result v3

    mul-int/2addr v2, v3

    add-int/lit8 v0, v2, 0x6

    .line 100
    .local v0, "expectedArraySize":I
    iget-object v2, p0, Lorg/apache/index/poi/ddf/EscherArrayProperty;->_complexData:[B

    array-length v2, v2

    if-eq v0, v2, :cond_0

    .line 102
    new-array v1, v0, [B

    .line 103
    .local v1, "newArray":[B
    iget-object v2, p0, Lorg/apache/index/poi/ddf/EscherArrayProperty;->_complexData:[B

    const/4 v3, 0x6

    invoke-static {v2, v5, v1, v5, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 104
    iput-object v1, p0, Lorg/apache/index/poi/ddf/EscherArrayProperty;->_complexData:[B

    .line 106
    .end local v1    # "newArray":[B
    :cond_0
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    const/16 v4, 0xa

    .line 121
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 122
    .local v1, "results":Ljava/lang/StringBuffer;
    const-string/jumbo v2, "    {EscherArrayProperty:\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 123
    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "     Num Elements: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lorg/apache/index/poi/ddf/EscherArrayProperty;->getNumberOfElementsInArray()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 124
    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "     Num Elements In Memory: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lorg/apache/index/poi/ddf/EscherArrayProperty;->getNumberOfElementsInMemory()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 125
    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "     Size of elements: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lorg/apache/index/poi/ddf/EscherArrayProperty;->getSizeOfElements()S

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 126
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p0}, Lorg/apache/index/poi/ddf/EscherArrayProperty;->getNumberOfElementsInArray()I

    move-result v2

    if-lt v0, v2, :cond_0

    .line 129
    const-string/jumbo v2, "}\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 131
    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "propNum: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lorg/apache/index/poi/ddf/EscherArrayProperty;->getPropertyNumber()S

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 132
    const-string/jumbo v3, ", propName: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lorg/apache/index/poi/ddf/EscherArrayProperty;->getPropertyNumber()S

    move-result v3

    invoke-static {v3}, Lorg/apache/index/poi/ddf/EscherProperties;->getPropertyName(S)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 133
    const-string/jumbo v3, ", complex: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lorg/apache/index/poi/ddf/EscherArrayProperty;->isComplex()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 134
    const-string/jumbo v3, ", blipId: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lorg/apache/index/poi/ddf/EscherArrayProperty;->isBlipId()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 135
    const-string/jumbo v3, ", data: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 131
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2

    .line 127
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "     Element "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ": "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0, v0}, Lorg/apache/index/poi/ddf/EscherArrayProperty;->getElement(I)[B

    move-result-object v3

    invoke-static {v3}, Lorg/apache/index/poi/util/HexDump;->toHex([B)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 126
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_0
.end method
