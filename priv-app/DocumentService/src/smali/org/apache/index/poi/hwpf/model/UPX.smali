.class public final Lorg/apache/index/poi/hwpf/model/UPX;
.super Ljava/lang/Object;
.source "UPX.java"


# instance fields
.field private _upx:[B


# direct methods
.method public constructor <init>([B)V
    .locals 0
    .param p1, "upx"    # [B

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lorg/apache/index/poi/hwpf/model/UPX;->_upx:[B

    .line 29
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 42
    if-nez p1, :cond_1

    .line 51
    :cond_0
    :goto_0
    return v1

    .line 47
    :cond_1
    instance-of v2, p1, Lorg/apache/index/poi/hwpf/model/UPX;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 48
    check-cast v0, Lorg/apache/index/poi/hwpf/model/UPX;

    .line 49
    .local v0, "upx":Lorg/apache/index/poi/hwpf/model/UPX;
    iget-object v1, p0, Lorg/apache/index/poi/hwpf/model/UPX;->_upx:[B

    iget-object v2, v0, Lorg/apache/index/poi/hwpf/model/UPX;->_upx:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    goto :goto_0
.end method

.method public getUPX()[B
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/UPX;->_upx:[B

    return-object v0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/UPX;->_upx:[B

    array-length v0, v0

    return v0
.end method
