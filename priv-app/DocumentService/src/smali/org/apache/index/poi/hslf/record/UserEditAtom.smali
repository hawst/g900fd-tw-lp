.class public final Lorg/apache/index/poi/hslf/record/UserEditAtom;
.super Lorg/apache/index/poi/hslf/record/PositionDependentRecordAtom;
.source "UserEditAtom.java"


# static fields
.field public static final LAST_VIEW_NONE:I = 0x0

.field public static final LAST_VIEW_NOTES:I = 0x3

.field public static final LAST_VIEW_OUTLINE_VIEW:I = 0x2

.field public static final LAST_VIEW_SLIDE_VIEW:I = 0x1

.field private static _type:J


# instance fields
.field private _header:[B

.field private docPersistRef:I

.field private lastUserEditAtomOffset:I

.field private lastViewType:S

.field private lastViewedSlideID:I

.field private maxPersistWritten:I

.field private persistPointersOffset:I

.field private pptVersion:I

.field private reserved:[B


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 45
    const-wide/16 v0, 0xff5

    sput-wide v0, Lorg/apache/index/poi/hslf/record/UserEditAtom;->_type:J

    return-void
.end method

.method protected constructor <init>([BII)V
    .locals 4
    .param p1, "source"    # [B
    .param p2, "start"    # I
    .param p3, "len"    # I

    .prologue
    const/16 v1, 0x8

    const/4 v3, 0x0

    .line 77
    invoke-direct {p0}, Lorg/apache/index/poi/hslf/record/PositionDependentRecordAtom;-><init>()V

    .line 79
    const/16 v0, 0x22

    if-ge p3, v0, :cond_0

    const/16 p3, 0x22

    .line 82
    :cond_0
    new-array v0, v1, [B

    iput-object v0, p0, Lorg/apache/index/poi/hslf/record/UserEditAtom;->_header:[B

    .line 83
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/UserEditAtom;->_header:[B

    invoke-static {p1, p2, v0, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 86
    add-int/lit8 v0, p2, 0x0

    add-int/lit8 v0, v0, 0x8

    invoke-static {p1, v0}, Lorg/apache/index/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    iput v0, p0, Lorg/apache/index/poi/hslf/record/UserEditAtom;->lastViewedSlideID:I

    .line 89
    add-int/lit8 v0, p2, 0x4

    add-int/lit8 v0, v0, 0x8

    invoke-static {p1, v0}, Lorg/apache/index/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    iput v0, p0, Lorg/apache/index/poi/hslf/record/UserEditAtom;->pptVersion:I

    .line 94
    add-int/lit8 v0, p2, 0x8

    add-int/lit8 v0, v0, 0x8

    invoke-static {p1, v0}, Lorg/apache/index/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    iput v0, p0, Lorg/apache/index/poi/hslf/record/UserEditAtom;->lastUserEditAtomOffset:I

    .line 99
    add-int/lit8 v0, p2, 0xc

    add-int/lit8 v0, v0, 0x8

    invoke-static {p1, v0}, Lorg/apache/index/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    iput v0, p0, Lorg/apache/index/poi/hslf/record/UserEditAtom;->persistPointersOffset:I

    .line 103
    add-int/lit8 v0, p2, 0x10

    add-int/lit8 v0, v0, 0x8

    invoke-static {p1, v0}, Lorg/apache/index/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    iput v0, p0, Lorg/apache/index/poi/hslf/record/UserEditAtom;->docPersistRef:I

    .line 106
    add-int/lit8 v0, p2, 0x14

    add-int/lit8 v0, v0, 0x8

    invoke-static {p1, v0}, Lorg/apache/index/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    iput v0, p0, Lorg/apache/index/poi/hslf/record/UserEditAtom;->maxPersistWritten:I

    .line 109
    add-int/lit8 v0, p2, 0x18

    add-int/lit8 v0, v0, 0x8

    invoke-static {p1, v0}, Lorg/apache/index/poi/util/LittleEndian;->getShort([BI)S

    move-result v0

    iput-short v0, p0, Lorg/apache/index/poi/hslf/record/UserEditAtom;->lastViewType:S

    .line 112
    add-int/lit8 v0, p3, -0x1a

    add-int/lit8 v0, v0, -0x8

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/apache/index/poi/hslf/record/UserEditAtom;->reserved:[B

    .line 113
    add-int/lit8 v0, p2, 0x1a

    add-int/lit8 v0, v0, 0x8

    iget-object v1, p0, Lorg/apache/index/poi/hslf/record/UserEditAtom;->reserved:[B

    iget-object v2, p0, Lorg/apache/index/poi/hslf/record/UserEditAtom;->reserved:[B

    array-length v2, v2

    invoke-static {p1, v0, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 114
    return-void
.end method


# virtual methods
.method public getDocPersistRef()I
    .locals 1

    .prologue
    .line 63
    iget v0, p0, Lorg/apache/index/poi/hslf/record/UserEditAtom;->docPersistRef:I

    return v0
.end method

.method public getLastUserEditAtomOffset()I
    .locals 1

    .prologue
    .line 61
    iget v0, p0, Lorg/apache/index/poi/hslf/record/UserEditAtom;->lastUserEditAtomOffset:I

    return v0
.end method

.method public getLastViewType()S
    .locals 1

    .prologue
    .line 58
    iget-short v0, p0, Lorg/apache/index/poi/hslf/record/UserEditAtom;->lastViewType:S

    return v0
.end method

.method public getLastViewedSlideID()I
    .locals 1

    .prologue
    .line 57
    iget v0, p0, Lorg/apache/index/poi/hslf/record/UserEditAtom;->lastViewedSlideID:I

    return v0
.end method

.method public getMaxPersistWritten()I
    .locals 1

    .prologue
    .line 64
    iget v0, p0, Lorg/apache/index/poi/hslf/record/UserEditAtom;->maxPersistWritten:I

    return v0
.end method

.method public getPersistPointersOffset()I
    .locals 1

    .prologue
    .line 62
    iget v0, p0, Lorg/apache/index/poi/hslf/record/UserEditAtom;->persistPointersOffset:I

    return v0
.end method

.method public getRecordType()J
    .locals 2

    .prologue
    .line 119
    sget-wide v0, Lorg/apache/index/poi/hslf/record/UserEditAtom;->_type:J

    return-wide v0
.end method

.method public setLastUserEditAtomOffset(I)V
    .locals 0
    .param p1, "offset"    # I

    .prologue
    .line 67
    iput p1, p0, Lorg/apache/index/poi/hslf/record/UserEditAtom;->lastUserEditAtomOffset:I

    return-void
.end method

.method public setLastViewType(S)V
    .locals 0
    .param p1, "type"    # S

    .prologue
    .line 69
    iput-short p1, p0, Lorg/apache/index/poi/hslf/record/UserEditAtom;->lastViewType:S

    return-void
.end method

.method public setMaxPersistWritten(I)V
    .locals 0
    .param p1, "max"    # I

    .prologue
    .line 70
    iput p1, p0, Lorg/apache/index/poi/hslf/record/UserEditAtom;->maxPersistWritten:I

    return-void
.end method

.method public setPersistPointersOffset(I)V
    .locals 0
    .param p1, "offset"    # I

    .prologue
    .line 68
    iput p1, p0, Lorg/apache/index/poi/hslf/record/UserEditAtom;->persistPointersOffset:I

    return-void
.end method

.method public updateOtherRecordReferences(Ljava/util/Hashtable;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 127
    .local p1, "oldToNewReferencesLookup":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    iget v1, p0, Lorg/apache/index/poi/hslf/record/UserEditAtom;->lastUserEditAtomOffset:I

    if-eqz v1, :cond_1

    .line 128
    iget v1, p0, Lorg/apache/index/poi/hslf/record/UserEditAtom;->lastUserEditAtomOffset:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 129
    .local v0, "newLocation":Ljava/lang/Integer;
    if-nez v0, :cond_0

    .line 130
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Couldn\'t find the new location of the UserEditAtom that used to be at "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lorg/apache/index/poi/hslf/record/UserEditAtom;->lastUserEditAtomOffset:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 132
    :cond_0
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iput v1, p0, Lorg/apache/index/poi/hslf/record/UserEditAtom;->lastUserEditAtomOffset:I

    .line 136
    .end local v0    # "newLocation":Ljava/lang/Integer;
    :cond_1
    iget v1, p0, Lorg/apache/index/poi/hslf/record/UserEditAtom;->persistPointersOffset:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 137
    .restart local v0    # "newLocation":Ljava/lang/Integer;
    if-nez v0, :cond_2

    .line 138
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Couldn\'t find the new location of the PersistPtr that used to be at "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lorg/apache/index/poi/hslf/record/UserEditAtom;->persistPointersOffset:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 140
    :cond_2
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iput v1, p0, Lorg/apache/index/poi/hslf/record/UserEditAtom;->persistPointersOffset:I

    .line 141
    return-void
.end method

.method public writeOut(Ljava/io/OutputStream;)V
    .locals 1
    .param p1, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 149
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/UserEditAtom;->_header:[B

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 152
    iget v0, p0, Lorg/apache/index/poi/hslf/record/UserEditAtom;->lastViewedSlideID:I

    invoke-static {v0, p1}, Lorg/apache/index/poi/hslf/record/UserEditAtom;->writeLittleEndian(ILjava/io/OutputStream;)V

    .line 153
    iget v0, p0, Lorg/apache/index/poi/hslf/record/UserEditAtom;->pptVersion:I

    invoke-static {v0, p1}, Lorg/apache/index/poi/hslf/record/UserEditAtom;->writeLittleEndian(ILjava/io/OutputStream;)V

    .line 154
    iget v0, p0, Lorg/apache/index/poi/hslf/record/UserEditAtom;->lastUserEditAtomOffset:I

    invoke-static {v0, p1}, Lorg/apache/index/poi/hslf/record/UserEditAtom;->writeLittleEndian(ILjava/io/OutputStream;)V

    .line 155
    iget v0, p0, Lorg/apache/index/poi/hslf/record/UserEditAtom;->persistPointersOffset:I

    invoke-static {v0, p1}, Lorg/apache/index/poi/hslf/record/UserEditAtom;->writeLittleEndian(ILjava/io/OutputStream;)V

    .line 156
    iget v0, p0, Lorg/apache/index/poi/hslf/record/UserEditAtom;->docPersistRef:I

    invoke-static {v0, p1}, Lorg/apache/index/poi/hslf/record/UserEditAtom;->writeLittleEndian(ILjava/io/OutputStream;)V

    .line 157
    iget v0, p0, Lorg/apache/index/poi/hslf/record/UserEditAtom;->maxPersistWritten:I

    invoke-static {v0, p1}, Lorg/apache/index/poi/hslf/record/UserEditAtom;->writeLittleEndian(ILjava/io/OutputStream;)V

    .line 158
    iget-short v0, p0, Lorg/apache/index/poi/hslf/record/UserEditAtom;->lastViewType:S

    invoke-static {v0, p1}, Lorg/apache/index/poi/hslf/record/UserEditAtom;->writeLittleEndian(SLjava/io/OutputStream;)V

    .line 161
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/UserEditAtom;->reserved:[B

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 162
    return-void
.end method
