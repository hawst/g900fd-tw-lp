.class public Lorg/apache/index/poi/hwpf/model/PlexOfField;
.super Ljava/lang/Object;
.source "PlexOfField.java"


# instance fields
.field private fcEnd:I

.field private fcStart:I

.field private fld:Lorg/apache/index/poi/hwpf/model/FieldDescriptor;


# direct methods
.method public constructor <init>(II[B)V
    .locals 1
    .param p1, "fcStart"    # I
    .param p2, "fcEnd"    # I
    .param p3, "data"    # [B

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput p1, p0, Lorg/apache/index/poi/hwpf/model/PlexOfField;->fcStart:I

    .line 38
    iput p2, p0, Lorg/apache/index/poi/hwpf/model/PlexOfField;->fcEnd:I

    .line 40
    new-instance v0, Lorg/apache/index/poi/hwpf/model/FieldDescriptor;

    invoke-direct {v0, p3}, Lorg/apache/index/poi/hwpf/model/FieldDescriptor;-><init>([B)V

    iput-object v0, p0, Lorg/apache/index/poi/hwpf/model/PlexOfField;->fld:Lorg/apache/index/poi/hwpf/model/FieldDescriptor;

    .line 41
    return-void
.end method


# virtual methods
.method public getFcEnd()I
    .locals 1

    .prologue
    .line 48
    iget v0, p0, Lorg/apache/index/poi/hwpf/model/PlexOfField;->fcEnd:I

    return v0
.end method

.method public getFcStart()I
    .locals 1

    .prologue
    .line 44
    iget v0, p0, Lorg/apache/index/poi/hwpf/model/PlexOfField;->fcStart:I

    return v0
.end method

.method public getFld()Lorg/apache/index/poi/hwpf/model/FieldDescriptor;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/PlexOfField;->fld:Lorg/apache/index/poi/hwpf/model/FieldDescriptor;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 56
    const-string/jumbo v0, "[{0}, {1}) - {2}"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    .line 57
    iget v3, p0, Lorg/apache/index/poi/hwpf/model/PlexOfField;->fcStart:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget v3, p0, Lorg/apache/index/poi/hwpf/model/PlexOfField;->fcEnd:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, Lorg/apache/index/poi/hwpf/model/PlexOfField;->fld:Lorg/apache/index/poi/hwpf/model/FieldDescriptor;

    invoke-virtual {v3}, Lorg/apache/index/poi/hwpf/model/FieldDescriptor;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    .line 56
    invoke-static {v0, v1}, Ljava/text/MessageFormat;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
