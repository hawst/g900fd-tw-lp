.class public Lorg/apache/index/poi/poifs/property/DirectoryProperty;
.super Lorg/apache/index/poi/poifs/property/Property;
.source "DirectoryProperty.java"

# interfaces
.implements Lorg/apache/index/poi/poifs/property/Parent;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/index/poi/poifs/property/DirectoryProperty$PropertyComparator;
    }
.end annotation


# instance fields
.field private _children:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/apache/index/poi/poifs/property/Property;",
            ">;"
        }
    .end annotation
.end field

.field private _children_names:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method protected constructor <init>(I[BI)V
    .locals 1
    .param p1, "index"    # I
    .param p2, "array"    # [B
    .param p3, "offset"    # I

    .prologue
    .line 63
    invoke-direct {p0, p1, p2, p3}, Lorg/apache/index/poi/poifs/property/Property;-><init>(I[BI)V

    .line 64
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/apache/index/poi/poifs/property/DirectoryProperty;->_children:Ljava/util/List;

    .line 65
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lorg/apache/index/poi/poifs/property/DirectoryProperty;->_children_names:Ljava/util/Set;

    .line 66
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 3
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 43
    invoke-direct {p0}, Lorg/apache/index/poi/poifs/property/Property;-><init>()V

    .line 44
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/apache/index/poi/poifs/property/DirectoryProperty;->_children:Ljava/util/List;

    .line 45
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lorg/apache/index/poi/poifs/property/DirectoryProperty;->_children_names:Ljava/util/Set;

    .line 46
    invoke-virtual {p0, p1}, Lorg/apache/index/poi/poifs/property/DirectoryProperty;->setName(Ljava/lang/String;)V

    .line 47
    invoke-virtual {p0, v1}, Lorg/apache/index/poi/poifs/property/DirectoryProperty;->setSize(I)V

    .line 48
    invoke-virtual {p0, v2}, Lorg/apache/index/poi/poifs/property/DirectoryProperty;->setPropertyType(B)V

    .line 49
    invoke-virtual {p0, v1}, Lorg/apache/index/poi/poifs/property/DirectoryProperty;->setStartBlock(I)V

    .line 50
    invoke-virtual {p0, v2}, Lorg/apache/index/poi/poifs/property/DirectoryProperty;->setNodeColor(B)V

    .line 51
    return-void
.end method


# virtual methods
.method public addChild(Lorg/apache/index/poi/poifs/property/Property;)V
    .locals 4
    .param p1, "property"    # Lorg/apache/index/poi/poifs/property/Property;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 262
    invoke-virtual {p1}, Lorg/apache/index/poi/poifs/property/Property;->getName()Ljava/lang/String;

    move-result-object v0

    .line 264
    .local v0, "name":Ljava/lang/String;
    iget-object v1, p0, Lorg/apache/index/poi/poifs/property/DirectoryProperty;->_children_names:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 266
    new-instance v1, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Duplicate name \""

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "\""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 268
    :cond_0
    iget-object v1, p0, Lorg/apache/index/poi/poifs/property/DirectoryProperty;->_children_names:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 269
    iget-object v1, p0, Lorg/apache/index/poi/poifs/property/DirectoryProperty;->_children:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 270
    return-void
.end method

.method public changeName(Lorg/apache/index/poi/poifs/property/Property;Ljava/lang/String;)Z
    .locals 4
    .param p1, "property"    # Lorg/apache/index/poi/poifs/property/Property;
    .param p2, "newName"    # Ljava/lang/String;

    .prologue
    .line 79
    invoke-virtual {p1}, Lorg/apache/index/poi/poifs/property/Property;->getName()Ljava/lang/String;

    move-result-object v1

    .line 81
    .local v1, "oldName":Ljava/lang/String;
    invoke-virtual {p1, p2}, Lorg/apache/index/poi/poifs/property/Property;->setName(Ljava/lang/String;)V

    .line 82
    invoke-virtual {p1}, Lorg/apache/index/poi/poifs/property/Property;->getName()Ljava/lang/String;

    move-result-object v0

    .line 84
    .local v0, "cleanNewName":Ljava/lang/String;
    iget-object v3, p0, Lorg/apache/index/poi/poifs/property/DirectoryProperty;->_children_names:Ljava/util/Set;

    invoke-interface {v3, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 88
    invoke-virtual {p1, v1}, Lorg/apache/index/poi/poifs/property/Property;->setName(Ljava/lang/String;)V

    .line 89
    const/4 v2, 0x0

    .line 97
    .local v2, "result":Z
    :goto_0
    return v2

    .line 93
    .end local v2    # "result":Z
    :cond_0
    iget-object v3, p0, Lorg/apache/index/poi/poifs/property/DirectoryProperty;->_children_names:Ljava/util/Set;

    invoke-interface {v3, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 94
    iget-object v3, p0, Lorg/apache/index/poi/poifs/property/DirectoryProperty;->_children_names:Ljava/util/Set;

    invoke-interface {v3, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 95
    const/4 v2, 0x1

    .restart local v2    # "result":Z
    goto :goto_0
.end method

.method public deleteChild(Lorg/apache/index/poi/poifs/property/Property;)Z
    .locals 3
    .param p1, "property"    # Lorg/apache/index/poi/poifs/property/Property;

    .prologue
    .line 109
    iget-object v1, p0, Lorg/apache/index/poi/poifs/property/DirectoryProperty;->_children:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    move-result v0

    .line 111
    .local v0, "result":Z
    if-eqz v0, :cond_0

    .line 113
    iget-object v1, p0, Lorg/apache/index/poi/poifs/property/DirectoryProperty;->_children_names:Ljava/util/Set;

    invoke-virtual {p1}, Lorg/apache/index/poi/poifs/property/Property;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 115
    :cond_0
    return v0
.end method

.method public getChildren()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Lorg/apache/index/poi/poifs/property/Property;",
            ">;"
        }
    .end annotation

    .prologue
    .line 248
    iget-object v0, p0, Lorg/apache/index/poi/poifs/property/DirectoryProperty;->_children:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public isDirectory()Z
    .locals 1

    .prologue
    .line 193
    const/4 v0, 0x1

    return v0
.end method

.method protected preWrite()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 202
    iget-object v3, p0, Lorg/apache/index/poi/poifs/property/DirectoryProperty;->_children:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_1

    .line 204
    iget-object v3, p0, Lorg/apache/index/poi/poifs/property/DirectoryProperty;->_children:Ljava/util/List;

    new-array v4, v6, [Lorg/apache/index/poi/poifs/property/Property;

    invoke-interface {v3, v4}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lorg/apache/index/poi/poifs/property/Property;

    .line 206
    .local v0, "children":[Lorg/apache/index/poi/poifs/property/Property;
    new-instance v3, Lorg/apache/index/poi/poifs/property/DirectoryProperty$PropertyComparator;

    invoke-direct {v3}, Lorg/apache/index/poi/poifs/property/DirectoryProperty$PropertyComparator;-><init>()V

    invoke-static {v0, v3}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    .line 207
    array-length v3, v0

    div-int/lit8 v2, v3, 0x2

    .line 209
    .local v2, "midpoint":I
    aget-object v3, v0, v2

    invoke-virtual {v3}, Lorg/apache/index/poi/poifs/property/Property;->getIndex()I

    move-result v3

    invoke-virtual {p0, v3}, Lorg/apache/index/poi/poifs/property/DirectoryProperty;->setChildProperty(I)V

    .line 210
    aget-object v3, v0, v6

    invoke-virtual {v3, v5}, Lorg/apache/index/poi/poifs/property/Property;->setPreviousChild(Lorg/apache/index/poi/poifs/property/Child;)V

    .line 211
    aget-object v3, v0, v6

    invoke-virtual {v3, v5}, Lorg/apache/index/poi/poifs/property/Property;->setNextChild(Lorg/apache/index/poi/poifs/property/Child;)V

    .line 212
    const/4 v1, 0x1

    .local v1, "j":I
    :goto_0
    if-lt v1, v2, :cond_2

    .line 217
    if-eqz v2, :cond_0

    .line 219
    aget-object v3, v0, v2

    .line 220
    add-int/lit8 v4, v2, -0x1

    aget-object v4, v0, v4

    invoke-virtual {v3, v4}, Lorg/apache/index/poi/poifs/property/Property;->setPreviousChild(Lorg/apache/index/poi/poifs/property/Child;)V

    .line 222
    :cond_0
    array-length v3, v0

    add-int/lit8 v3, v3, -0x1

    if-eq v2, v3, :cond_4

    .line 224
    aget-object v3, v0, v2

    add-int/lit8 v4, v2, 0x1

    aget-object v4, v0, v4

    invoke-virtual {v3, v4}, Lorg/apache/index/poi/poifs/property/Property;->setNextChild(Lorg/apache/index/poi/poifs/property/Child;)V

    .line 225
    add-int/lit8 v1, v2, 0x1

    :goto_1
    array-length v3, v0

    add-int/lit8 v3, v3, -0x1

    if-lt v1, v3, :cond_3

    .line 230
    array-length v3, v0

    add-int/lit8 v3, v3, -0x1

    aget-object v3, v0, v3

    invoke-virtual {v3, v5}, Lorg/apache/index/poi/poifs/property/Property;->setPreviousChild(Lorg/apache/index/poi/poifs/property/Child;)V

    .line 231
    array-length v3, v0

    add-int/lit8 v3, v3, -0x1

    aget-object v3, v0, v3

    invoke-virtual {v3, v5}, Lorg/apache/index/poi/poifs/property/Property;->setNextChild(Lorg/apache/index/poi/poifs/property/Child;)V

    .line 238
    .end local v0    # "children":[Lorg/apache/index/poi/poifs/property/Property;
    .end local v1    # "j":I
    .end local v2    # "midpoint":I
    :cond_1
    :goto_2
    return-void

    .line 214
    .restart local v0    # "children":[Lorg/apache/index/poi/poifs/property/Property;
    .restart local v1    # "j":I
    .restart local v2    # "midpoint":I
    :cond_2
    aget-object v3, v0, v1

    add-int/lit8 v4, v1, -0x1

    aget-object v4, v0, v4

    invoke-virtual {v3, v4}, Lorg/apache/index/poi/poifs/property/Property;->setPreviousChild(Lorg/apache/index/poi/poifs/property/Child;)V

    .line 215
    aget-object v3, v0, v1

    invoke-virtual {v3, v5}, Lorg/apache/index/poi/poifs/property/Property;->setNextChild(Lorg/apache/index/poi/poifs/property/Child;)V

    .line 212
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 227
    :cond_3
    aget-object v3, v0, v1

    invoke-virtual {v3, v5}, Lorg/apache/index/poi/poifs/property/Property;->setPreviousChild(Lorg/apache/index/poi/poifs/property/Child;)V

    .line 228
    aget-object v3, v0, v1

    add-int/lit8 v4, v1, 0x1

    aget-object v4, v0, v4

    invoke-virtual {v3, v4}, Lorg/apache/index/poi/poifs/property/Property;->setNextChild(Lorg/apache/index/poi/poifs/property/Child;)V

    .line 225
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 235
    :cond_4
    aget-object v3, v0, v2

    invoke-virtual {v3, v5}, Lorg/apache/index/poi/poifs/property/Property;->setNextChild(Lorg/apache/index/poi/poifs/property/Child;)V

    goto :goto_2
.end method
