.class public Lorg/apache/index/poi/hpsf/Section;
.super Ljava/lang/Object;
.source "Section.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/index/poi/hpsf/Section$PropertyListEntry;
    }
.end annotation


# instance fields
.field protected dictionary:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected formatID:Lorg/apache/index/poi/hpsf/ClassID;

.field protected offset:J

.field protected properties:[Lorg/apache/index/poi/hpsf/Property;

.field protected size:I

.field private wasNull:Z


# direct methods
.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 139
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 140
    return-void
.end method

.method public constructor <init>([BI)V
    .locals 26
    .param p1, "src"    # [B
    .param p2, "offset"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/UnsupportedEncodingException;
        }
    .end annotation

    .prologue
    .line 154
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    .line 157
    move/from16 v15, p2

    .line 162
    .local v15, "o1":I
    new-instance v4, Lorg/apache/index/poi/hpsf/ClassID;

    move-object/from16 v0, p1

    invoke-direct {v4, v0, v15}, Lorg/apache/index/poi/hpsf/ClassID;-><init>([BI)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lorg/apache/index/poi/hpsf/Section;->formatID:Lorg/apache/index/poi/hpsf/ClassID;

    .line 163
    add-int/lit8 v15, v15, 0x10

    .line 169
    move-object/from16 v0, p1

    invoke-static {v0, v15}, Lorg/apache/index/poi/util/LittleEndian;->getUInt([BI)J

    move-result-wide v4

    move-object/from16 v0, p0

    iput-wide v4, v0, Lorg/apache/index/poi/hpsf/Section;->offset:J

    .line 170
    move-object/from16 v0, p0

    iget-wide v4, v0, Lorg/apache/index/poi/hpsf/Section;->offset:J

    long-to-int v15, v4

    .line 175
    move-object/from16 v0, p1

    invoke-static {v0, v15}, Lorg/apache/index/poi/util/LittleEndian;->getUInt([BI)J

    move-result-wide v4

    long-to-int v4, v4

    move-object/from16 v0, p0

    iput v4, v0, Lorg/apache/index/poi/hpsf/Section;->size:I

    .line 176
    add-int/lit8 v15, v15, 0x4

    .line 181
    move-object/from16 v0, p1

    invoke-static {v0, v15}, Lorg/apache/index/poi/util/LittleEndian;->getUInt([BI)J

    move-result-wide v4

    long-to-int v0, v4

    move/from16 v20, v0

    .line 182
    .local v20, "propertyCount":I
    add-int/lit8 v15, v15, 0x4

    .line 208
    move/from16 v0, v20

    new-array v4, v0, [Lorg/apache/index/poi/hpsf/Property;

    move-object/from16 v0, p0

    iput-object v4, v0, Lorg/apache/index/poi/hpsf/Section;->properties:[Lorg/apache/index/poi/hpsf/Property;

    .line 211
    move/from16 v16, v15

    .line 212
    .local v16, "pass1Offset":I
    new-instance v21, Ljava/util/ArrayList;

    move-object/from16 v0, v21

    move/from16 v1, v20

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 214
    .local v21, "propertyList":Ljava/util/List;, "Ljava/util/List<Lorg/apache/index/poi/hpsf/Section$PropertyListEntry;>;"
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/index/poi/hpsf/Section;->properties:[Lorg/apache/index/poi/hpsf/Property;

    array-length v4, v4

    if-lt v10, v4, :cond_3

    .line 231
    invoke-static/range {v21 .. v21}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 234
    const/4 v10, 0x0

    :goto_1
    add-int/lit8 v4, v20, -0x1

    if-lt v10, v4, :cond_4

    .line 240
    if-lez v20, :cond_0

    .line 242
    add-int/lit8 v4, v20, -0x1

    move-object/from16 v0, v21

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lorg/apache/index/poi/hpsf/Section$PropertyListEntry;

    .line 243
    .local v17, "ple":Lorg/apache/index/poi/hpsf/Section$PropertyListEntry;
    move-object/from16 v0, p0

    iget v4, v0, Lorg/apache/index/poi/hpsf/Section;->size:I

    move-object/from16 v0, v17

    iget v5, v0, Lorg/apache/index/poi/hpsf/Section$PropertyListEntry;->offset:I

    sub-int/2addr v4, v5

    move-object/from16 v0, v17

    iput v4, v0, Lorg/apache/index/poi/hpsf/Section$PropertyListEntry;->length:I

    .line 247
    .end local v17    # "ple":Lorg/apache/index/poi/hpsf/Section$PropertyListEntry;
    :cond_0
    const/4 v9, -0x1

    .line 248
    .local v9, "codepage":I
    invoke-interface/range {v21 .. v21}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    .line 249
    .local v11, "i":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/index/poi/hpsf/Section$PropertyListEntry;>;"
    :cond_1
    :goto_2
    const/4 v4, -0x1

    if-ne v9, v4, :cond_2

    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_5

    .line 274
    :cond_2
    const/4 v12, 0x0

    .line 275
    .local v12, "i1":I
    invoke-interface/range {v21 .. v21}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :goto_3
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_7

    .line 289
    const-wide/16 v4, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v5}, Lorg/apache/index/poi/hpsf/Section;->getProperty(J)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Map;

    move-object/from16 v0, p0

    iput-object v4, v0, Lorg/apache/index/poi/hpsf/Section;->dictionary:Ljava/util/Map;

    .line 290
    return-void

    .line 216
    .end local v9    # "codepage":I
    .end local v11    # "i":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/index/poi/hpsf/Section$PropertyListEntry;>;"
    .end local v12    # "i1":I
    :cond_3
    new-instance v17, Lorg/apache/index/poi/hpsf/Section$PropertyListEntry;

    invoke-direct/range {v17 .. v17}, Lorg/apache/index/poi/hpsf/Section$PropertyListEntry;-><init>()V

    .line 219
    .restart local v17    # "ple":Lorg/apache/index/poi/hpsf/Section$PropertyListEntry;
    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-static {v0, v1}, Lorg/apache/index/poi/util/LittleEndian;->getUInt([BI)J

    move-result-wide v4

    long-to-int v4, v4

    move-object/from16 v0, v17

    iput v4, v0, Lorg/apache/index/poi/hpsf/Section$PropertyListEntry;->id:I

    .line 220
    add-int/lit8 v16, v16, 0x4

    .line 223
    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-static {v0, v1}, Lorg/apache/index/poi/util/LittleEndian;->getUInt([BI)J

    move-result-wide v4

    long-to-int v4, v4

    move-object/from16 v0, v17

    iput v4, v0, Lorg/apache/index/poi/hpsf/Section$PropertyListEntry;->offset:I

    .line 224
    add-int/lit8 v16, v16, 0x4

    .line 227
    move-object/from16 v0, v21

    move-object/from16 v1, v17

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 214
    add-int/lit8 v10, v10, 0x1

    goto :goto_0

    .line 236
    .end local v17    # "ple":Lorg/apache/index/poi/hpsf/Section$PropertyListEntry;
    :cond_4
    move-object/from16 v0, v21

    invoke-interface {v0, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lorg/apache/index/poi/hpsf/Section$PropertyListEntry;

    .line 237
    .local v18, "ple1":Lorg/apache/index/poi/hpsf/Section$PropertyListEntry;
    add-int/lit8 v4, v10, 0x1

    move-object/from16 v0, v21

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lorg/apache/index/poi/hpsf/Section$PropertyListEntry;

    .line 238
    .local v19, "ple2":Lorg/apache/index/poi/hpsf/Section$PropertyListEntry;
    move-object/from16 v0, v19

    iget v4, v0, Lorg/apache/index/poi/hpsf/Section$PropertyListEntry;->offset:I

    move-object/from16 v0, v18

    iget v5, v0, Lorg/apache/index/poi/hpsf/Section$PropertyListEntry;->offset:I

    sub-int/2addr v4, v5

    move-object/from16 v0, v18

    iput v4, v0, Lorg/apache/index/poi/hpsf/Section$PropertyListEntry;->length:I

    .line 234
    add-int/lit8 v10, v10, 0x1

    goto/16 :goto_1

    .line 251
    .end local v18    # "ple1":Lorg/apache/index/poi/hpsf/Section$PropertyListEntry;
    .end local v19    # "ple2":Lorg/apache/index/poi/hpsf/Section$PropertyListEntry;
    .restart local v9    # "codepage":I
    .restart local v11    # "i":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/index/poi/hpsf/Section$PropertyListEntry;>;"
    :cond_5
    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lorg/apache/index/poi/hpsf/Section$PropertyListEntry;

    .line 254
    .restart local v17    # "ple":Lorg/apache/index/poi/hpsf/Section$PropertyListEntry;
    move-object/from16 v0, v17

    iget v4, v0, Lorg/apache/index/poi/hpsf/Section$PropertyListEntry;->id:I

    const/4 v5, 0x1

    if-ne v4, v5, :cond_1

    .line 258
    move-object/from16 v0, p0

    iget-wide v4, v0, Lorg/apache/index/poi/hpsf/Section;->offset:J

    move-object/from16 v0, v17

    iget v6, v0, Lorg/apache/index/poi/hpsf/Section$PropertyListEntry;->offset:I

    int-to-long v6, v6

    add-long/2addr v4, v6

    long-to-int v14, v4

    .line 259
    .local v14, "o":I
    move-object/from16 v0, p1

    invoke-static {v0, v14}, Lorg/apache/index/poi/util/LittleEndian;->getUInt([BI)J

    move-result-wide v22

    .line 260
    .local v22, "type":J
    add-int/lit8 v14, v14, 0x4

    .line 262
    const-wide/16 v4, 0x2

    cmp-long v4, v22, v4

    if-eqz v4, :cond_6

    .line 263
    new-instance v4, Lorg/apache/index/poi/hpsf/HPSFRuntimeException;

    .line 264
    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "Value type of property ID 1 is not VT_I2 but "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 265
    move-wide/from16 v0, v22

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, "."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 264
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 263
    invoke-direct {v4, v5}, Lorg/apache/index/poi/hpsf/HPSFRuntimeException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 268
    :cond_6
    move-object/from16 v0, p1

    invoke-static {v0, v14}, Lorg/apache/index/poi/util/LittleEndian;->getUShort([BI)I

    move-result v9

    goto/16 :goto_2

    .line 277
    .end local v14    # "o":I
    .end local v17    # "ple":Lorg/apache/index/poi/hpsf/Section$PropertyListEntry;
    .end local v22    # "type":J
    .restart local v12    # "i1":I
    :cond_7
    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lorg/apache/index/poi/hpsf/Section$PropertyListEntry;

    .line 278
    .restart local v17    # "ple":Lorg/apache/index/poi/hpsf/Section$PropertyListEntry;
    new-instance v2, Lorg/apache/index/poi/hpsf/Property;

    move-object/from16 v0, v17

    iget v4, v0, Lorg/apache/index/poi/hpsf/Section$PropertyListEntry;->id:I

    int-to-long v3, v4

    .line 279
    move-object/from16 v0, p0

    iget-wide v6, v0, Lorg/apache/index/poi/hpsf/Section;->offset:J

    move-object/from16 v0, v17

    iget v5, v0, Lorg/apache/index/poi/hpsf/Section$PropertyListEntry;->offset:I

    int-to-long v0, v5

    move-wide/from16 v24, v0

    add-long v6, v6, v24

    .line 280
    move-object/from16 v0, v17

    iget v8, v0, Lorg/apache/index/poi/hpsf/Section$PropertyListEntry;->length:I

    move-object/from16 v5, p1

    .line 278
    invoke-direct/range {v2 .. v9}, Lorg/apache/index/poi/hpsf/Property;-><init>(J[BJII)V

    .line 281
    .local v2, "p":Lorg/apache/index/poi/hpsf/Property;
    invoke-virtual {v2}, Lorg/apache/index/poi/hpsf/Property;->getID()J

    move-result-wide v4

    const-wide/16 v6, 0x1

    cmp-long v4, v4, v6

    if-nez v4, :cond_8

    .line 282
    new-instance v3, Lorg/apache/index/poi/hpsf/Property;

    invoke-virtual {v2}, Lorg/apache/index/poi/hpsf/Property;->getID()J

    move-result-wide v4

    invoke-virtual {v2}, Lorg/apache/index/poi/hpsf/Property;->getType()J

    move-result-wide v6

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-direct/range {v3 .. v8}, Lorg/apache/index/poi/hpsf/Property;-><init>(JJLjava/lang/Object;)V

    .end local v2    # "p":Lorg/apache/index/poi/hpsf/Property;
    .local v3, "p":Lorg/apache/index/poi/hpsf/Property;
    move-object v2, v3

    .line 283
    .end local v3    # "p":Lorg/apache/index/poi/hpsf/Property;
    .restart local v2    # "p":Lorg/apache/index/poi/hpsf/Property;
    :cond_8
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/index/poi/hpsf/Section;->properties:[Lorg/apache/index/poi/hpsf/Property;

    add-int/lit8 v13, v12, 0x1

    .end local v12    # "i1":I
    .local v13, "i1":I
    aput-object v2, v4, v12

    move v12, v13

    .end local v13    # "i1":I
    .restart local v12    # "i1":I
    goto/16 :goto_3
.end method

.method private remove([Lorg/apache/index/poi/hpsf/Property;I)[Lorg/apache/index/poi/hpsf/Property;
    .locals 3
    .param p1, "pa"    # [Lorg/apache/index/poi/hpsf/Property;
    .param p2, "i"    # I

    .prologue
    const/4 v2, 0x0

    .line 567
    array-length v1, p1

    add-int/lit8 v1, v1, -0x1

    new-array v0, v1, [Lorg/apache/index/poi/hpsf/Property;

    .line 568
    .local v0, "h":[Lorg/apache/index/poi/hpsf/Property;
    if-lez p2, :cond_0

    .line 569
    invoke-static {p1, v2, v0, v2, p2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 570
    :cond_0
    add-int/lit8 v1, p2, 0x1

    array-length v2, v0

    sub-int/2addr v2, p2

    invoke-static {p1, v1, v0, p2, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 571
    return-object v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 13
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 488
    if-eqz p1, :cond_0

    instance-of v9, p1, Lorg/apache/index/poi/hpsf/Section;

    if-nez v9, :cond_1

    .line 489
    :cond_0
    const/4 v9, 0x0

    .line 552
    :goto_0
    return v9

    :cond_1
    move-object v8, p1

    .line 490
    check-cast v8, Lorg/apache/index/poi/hpsf/Section;

    .line 491
    .local v8, "s":Lorg/apache/index/poi/hpsf/Section;
    invoke-virtual {v8}, Lorg/apache/index/poi/hpsf/Section;->getFormatID()Lorg/apache/index/poi/hpsf/ClassID;

    move-result-object v9

    invoke-virtual {p0}, Lorg/apache/index/poi/hpsf/Section;->getFormatID()Lorg/apache/index/poi/hpsf/ClassID;

    move-result-object v10

    invoke-virtual {v9, v10}, Lorg/apache/index/poi/hpsf/ClassID;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_2

    .line 492
    const/4 v9, 0x0

    goto :goto_0

    .line 496
    :cond_2
    invoke-virtual {p0}, Lorg/apache/index/poi/hpsf/Section;->getProperties()[Lorg/apache/index/poi/hpsf/Property;

    move-result-object v9

    array-length v9, v9

    new-array v6, v9, [Lorg/apache/index/poi/hpsf/Property;

    .line 497
    .local v6, "pa1":[Lorg/apache/index/poi/hpsf/Property;
    invoke-virtual {v8}, Lorg/apache/index/poi/hpsf/Section;->getProperties()[Lorg/apache/index/poi/hpsf/Property;

    move-result-object v9

    array-length v9, v9

    new-array v7, v9, [Lorg/apache/index/poi/hpsf/Property;

    .line 498
    .local v7, "pa2":[Lorg/apache/index/poi/hpsf/Property;
    invoke-virtual {p0}, Lorg/apache/index/poi/hpsf/Section;->getProperties()[Lorg/apache/index/poi/hpsf/Property;

    move-result-object v9

    const/4 v10, 0x0

    const/4 v11, 0x0

    array-length v12, v6

    invoke-static {v9, v10, v6, v11, v12}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 499
    invoke-virtual {v8}, Lorg/apache/index/poi/hpsf/Section;->getProperties()[Lorg/apache/index/poi/hpsf/Property;

    move-result-object v9

    const/4 v10, 0x0

    const/4 v11, 0x0

    array-length v12, v7

    invoke-static {v9, v10, v7, v11, v12}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 503
    const/4 v4, 0x0

    .line 504
    .local v4, "p10":Lorg/apache/index/poi/hpsf/Property;
    const/4 v5, 0x0

    .line 505
    .local v5, "p20":Lorg/apache/index/poi/hpsf/Property;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    array-length v9, v6

    if-lt v1, v9, :cond_3

    .line 521
    const/4 v1, 0x0

    :goto_2
    array-length v9, v7

    if-lt v1, v9, :cond_6

    .line 540
    array-length v9, v6

    array-length v10, v7

    if-eq v9, v10, :cond_9

    .line 541
    const/4 v9, 0x0

    goto :goto_0

    .line 507
    :cond_3
    aget-object v9, v6, v1

    invoke-virtual {v9}, Lorg/apache/index/poi/hpsf/Property;->getID()J

    move-result-wide v2

    .line 508
    .local v2, "id":J
    const-wide/16 v10, 0x0

    cmp-long v9, v2, v10

    if-nez v9, :cond_4

    .line 510
    aget-object v4, v6, v1

    .line 511
    invoke-direct {p0, v6, v1}, Lorg/apache/index/poi/hpsf/Section;->remove([Lorg/apache/index/poi/hpsf/Property;I)[Lorg/apache/index/poi/hpsf/Property;

    move-result-object v6

    .line 512
    add-int/lit8 v1, v1, -0x1

    .line 514
    :cond_4
    const-wide/16 v10, 0x1

    cmp-long v9, v2, v10

    if-nez v9, :cond_5

    .line 517
    invoke-direct {p0, v6, v1}, Lorg/apache/index/poi/hpsf/Section;->remove([Lorg/apache/index/poi/hpsf/Property;I)[Lorg/apache/index/poi/hpsf/Property;

    move-result-object v6

    .line 518
    add-int/lit8 v1, v1, -0x1

    .line 505
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 523
    .end local v2    # "id":J
    :cond_6
    aget-object v9, v7, v1

    invoke-virtual {v9}, Lorg/apache/index/poi/hpsf/Property;->getID()J

    move-result-wide v2

    .line 524
    .restart local v2    # "id":J
    const-wide/16 v10, 0x0

    cmp-long v9, v2, v10

    if-nez v9, :cond_7

    .line 526
    aget-object v5, v7, v1

    .line 527
    invoke-direct {p0, v7, v1}, Lorg/apache/index/poi/hpsf/Section;->remove([Lorg/apache/index/poi/hpsf/Property;I)[Lorg/apache/index/poi/hpsf/Property;

    move-result-object v7

    .line 528
    add-int/lit8 v1, v1, -0x1

    .line 530
    :cond_7
    const-wide/16 v10, 0x1

    cmp-long v9, v2, v10

    if-nez v9, :cond_8

    .line 533
    invoke-direct {p0, v7, v1}, Lorg/apache/index/poi/hpsf/Section;->remove([Lorg/apache/index/poi/hpsf/Property;I)[Lorg/apache/index/poi/hpsf/Property;

    move-result-object v7

    .line 534
    add-int/lit8 v1, v1, -0x1

    .line 521
    :cond_8
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 544
    .end local v2    # "id":J
    :cond_9
    const/4 v0, 0x1

    .line 545
    .local v0, "dictionaryEqual":Z
    if-eqz v4, :cond_b

    if-eqz v5, :cond_b

    .line 546
    invoke-virtual {v4}, Lorg/apache/index/poi/hpsf/Property;->getValue()Ljava/lang/Object;

    move-result-object v9

    invoke-virtual {v5}, Lorg/apache/index/poi/hpsf/Property;->getValue()Ljava/lang/Object;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 549
    :cond_a
    :goto_3
    if-eqz v0, :cond_d

    .line 550
    invoke-static {v6, v7}, Lorg/apache/index/poi/hpsf/Util;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v9

    goto/16 :goto_0

    .line 547
    :cond_b
    if-nez v4, :cond_c

    if-eqz v5, :cond_a

    .line 548
    :cond_c
    const/4 v0, 0x0

    goto :goto_3

    .line 552
    :cond_d
    const/4 v9, 0x0

    goto/16 :goto_0
.end method

.method public getCodepage()I
    .locals 4

    .prologue
    .line 647
    const-wide/16 v2, 0x1

    invoke-virtual {p0, v2, v3}, Lorg/apache/index/poi/hpsf/Section;->getProperty(J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 648
    .local v0, "codepage":Ljava/lang/Integer;
    if-nez v0, :cond_0

    .line 649
    const/4 v1, -0x1

    .line 651
    :goto_0
    return v1

    .line 650
    :cond_0
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 651
    .local v1, "cp":I
    goto :goto_0
.end method

.method public getDictionary()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 634
    iget-object v0, p0, Lorg/apache/index/poi/hpsf/Section;->dictionary:Ljava/util/Map;

    return-object v0
.end method

.method public getFormatID()Lorg/apache/index/poi/hpsf/ClassID;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lorg/apache/index/poi/hpsf/Section;->formatID:Lorg/apache/index/poi/hpsf/ClassID;

    return-object v0
.end method

.method public getOffset()J
    .locals 2

    .prologue
    .line 83
    iget-wide v0, p0, Lorg/apache/index/poi/hpsf/Section;->offset:J

    return-wide v0
.end method

.method public getPIDString(J)Ljava/lang/String;
    .locals 3
    .param p1, "pid"    # J

    .prologue
    .line 450
    const/4 v0, 0x0

    .line 451
    .local v0, "s":Ljava/lang/String;
    iget-object v1, p0, Lorg/apache/index/poi/hpsf/Section;->dictionary:Ljava/util/Map;

    if-eqz v1, :cond_0

    .line 452
    iget-object v1, p0, Lorg/apache/index/poi/hpsf/Section;->dictionary:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "s":Ljava/lang/String;
    check-cast v0, Ljava/lang/String;

    .line 453
    .restart local v0    # "s":Ljava/lang/String;
    :cond_0
    if-nez v0, :cond_1

    .line 454
    invoke-virtual {p0}, Lorg/apache/index/poi/hpsf/Section;->getFormatID()Lorg/apache/index/poi/hpsf/ClassID;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/index/poi/hpsf/ClassID;->getBytes()[B

    move-result-object v1

    invoke-static {v1, p1, p2}, Lorg/apache/index/poi/hpsf/wellknown/SectionIDMap;->getPIDString([BJ)Ljava/lang/String;

    move-result-object v0

    .line 455
    :cond_1
    if-nez v0, :cond_2

    .line 456
    const-string/jumbo v0, "[undefined]"

    .line 457
    :cond_2
    return-object v0
.end method

.method public getProperties()[Lorg/apache/index/poi/hpsf/Property;
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, Lorg/apache/index/poi/hpsf/Section;->properties:[Lorg/apache/index/poi/hpsf/Property;

    return-object v0
.end method

.method public getProperty(J)Ljava/lang/Object;
    .locals 5
    .param p1, "id"    # J

    .prologue
    .line 351
    const/4 v1, 0x0

    iput-boolean v1, p0, Lorg/apache/index/poi/hpsf/Section;->wasNull:Z

    .line 352
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lorg/apache/index/poi/hpsf/Section;->properties:[Lorg/apache/index/poi/hpsf/Property;

    array-length v1, v1

    if-lt v0, v1, :cond_0

    .line 355
    const/4 v1, 0x1

    iput-boolean v1, p0, Lorg/apache/index/poi/hpsf/Section;->wasNull:Z

    .line 356
    const/4 v1, 0x0

    :goto_1
    return-object v1

    .line 353
    :cond_0
    iget-object v1, p0, Lorg/apache/index/poi/hpsf/Section;->properties:[Lorg/apache/index/poi/hpsf/Property;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lorg/apache/index/poi/hpsf/Property;->getID()J

    move-result-wide v2

    cmp-long v1, p1, v2

    if-nez v1, :cond_1

    .line 354
    iget-object v1, p0, Lorg/apache/index/poi/hpsf/Section;->properties:[Lorg/apache/index/poi/hpsf/Property;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lorg/apache/index/poi/hpsf/Property;->getValue()Ljava/lang/Object;

    move-result-object v1

    goto :goto_1

    .line 352
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method protected getPropertyBooleanValue(I)Z
    .locals 4
    .param p1, "id"    # I

    .prologue
    .line 401
    int-to-long v2, p1

    invoke-virtual {p0, v2, v3}, Lorg/apache/index/poi/hpsf/Section;->getProperty(J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    .line 402
    .local v0, "b":Ljava/lang/Boolean;
    if-nez v0, :cond_0

    .line 403
    const/4 v1, 0x0

    .line 405
    :goto_0
    return v1

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    goto :goto_0
.end method

.method public getPropertyCount()I
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lorg/apache/index/poi/hpsf/Section;->properties:[Lorg/apache/index/poi/hpsf/Property;

    array-length v0, v0

    return v0
.end method

.method protected getPropertyIntValue(J)I
    .locals 5
    .param p1, "id"    # J

    .prologue
    .line 375
    invoke-virtual {p0, p1, p2}, Lorg/apache/index/poi/hpsf/Section;->getProperty(J)Ljava/lang/Object;

    move-result-object v1

    .line 376
    .local v1, "o":Ljava/lang/Object;
    if-nez v1, :cond_0

    .line 377
    const/4 v2, 0x0

    .line 383
    :goto_0
    return v2

    .line 378
    :cond_0
    instance-of v2, v1, Ljava/lang/Long;

    if-nez v2, :cond_1

    instance-of v2, v1, Ljava/lang/Integer;

    if-nez v2, :cond_1

    .line 379
    new-instance v2, Lorg/apache/index/poi/hpsf/HPSFRuntimeException;

    .line 380
    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "This property is not an integer type, but "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 381
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 380
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 379
    invoke-direct {v2, v3}, Lorg/apache/index/poi/hpsf/HPSFRuntimeException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_1
    move-object v0, v1

    .line 382
    check-cast v0, Ljava/lang/Number;

    .line 383
    .local v0, "i":Ljava/lang/Number;
    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v2

    goto :goto_0
.end method

.method public getSize()I
    .locals 1

    .prologue
    .line 101
    iget v0, p0, Lorg/apache/index/poi/hpsf/Section;->size:I

    return v0
.end method

.method public hashCode()I
    .locals 8

    .prologue
    .line 581
    const-wide/16 v0, 0x0

    .line 582
    .local v0, "hashCode":J
    invoke-virtual {p0}, Lorg/apache/index/poi/hpsf/Section;->getFormatID()Lorg/apache/index/poi/hpsf/ClassID;

    move-result-object v5

    invoke-virtual {v5}, Lorg/apache/index/poi/hpsf/ClassID;->hashCode()I

    move-result v5

    int-to-long v6, v5

    add-long/2addr v0, v6

    .line 583
    invoke-virtual {p0}, Lorg/apache/index/poi/hpsf/Section;->getProperties()[Lorg/apache/index/poi/hpsf/Property;

    move-result-object v3

    .line 584
    .local v3, "pa":[Lorg/apache/index/poi/hpsf/Property;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v5, v3

    if-lt v2, v5, :cond_0

    .line 586
    const-wide v6, 0xffffffffL

    and-long/2addr v6, v0

    long-to-int v4, v6

    .line 587
    .local v4, "returnHashCode":I
    return v4

    .line 585
    .end local v4    # "returnHashCode":I
    :cond_0
    aget-object v5, v3, v2

    invoke-virtual {v5}, Lorg/apache/index/poi/hpsf/Property;->hashCode()I

    move-result v5

    int-to-long v6, v5

    add-long/2addr v0, v6

    .line 584
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 7

    .prologue
    const/16 v6, 0x5d

    .line 597
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 598
    .local v0, "b":Ljava/lang/StringBuffer;
    invoke-virtual {p0}, Lorg/apache/index/poi/hpsf/Section;->getProperties()[Lorg/apache/index/poi/hpsf/Property;

    move-result-object v2

    .line 599
    .local v2, "pa":[Lorg/apache/index/poi/hpsf/Property;
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 600
    const/16 v3, 0x5b

    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 601
    const-string/jumbo v3, "formatID: "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 602
    invoke-virtual {p0}, Lorg/apache/index/poi/hpsf/Section;->getFormatID()Lorg/apache/index/poi/hpsf/ClassID;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    .line 603
    const-string/jumbo v3, ", offset: "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 604
    invoke-virtual {p0}, Lorg/apache/index/poi/hpsf/Section;->getOffset()J

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    .line 605
    const-string/jumbo v3, ", propertyCount: "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 606
    invoke-virtual {p0}, Lorg/apache/index/poi/hpsf/Section;->getPropertyCount()I

    move-result v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 607
    const-string/jumbo v3, ", size: "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 608
    invoke-virtual {p0}, Lorg/apache/index/poi/hpsf/Section;->getSize()I

    move-result v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 609
    const-string/jumbo v3, ", properties: [\n"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 610
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v3, v2

    if-lt v1, v3, :cond_0

    .line 615
    invoke-virtual {v0, v6}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 616
    invoke-virtual {v0, v6}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 617
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3

    .line 612
    :cond_0
    aget-object v3, v2, v1

    invoke-virtual {v3}, Lorg/apache/index/poi/hpsf/Property;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 613
    const-string/jumbo v3, ",\n"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 610
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public wasNull()Z
    .locals 1

    .prologue
    .line 433
    iget-boolean v0, p0, Lorg/apache/index/poi/hpsf/Section;->wasNull:Z

    return v0
.end method
