.class public final Lorg/apache/index/poi/hwpf/usermodel/BorderCode;
.super Ljava/lang/Object;
.source "BorderCode.java"

# interfaces
.implements Ljava/lang/Cloneable;


# static fields
.field public static final SIZE:I = 0x4

.field private static final _brcType:Lorg/apache/index/poi/util/BitField;

.field private static final _dptLineWidth:Lorg/apache/index/poi/util/BitField;

.field private static final _dptSpace:Lorg/apache/index/poi/util/BitField;

.field private static final _fFrame:Lorg/apache/index/poi/util/BitField;

.field private static final _fShadow:Lorg/apache/index/poi/util/BitField;

.field private static final _ico:Lorg/apache/index/poi/util/BitField;


# instance fields
.field private _info:S

.field private _info2:S


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/16 v1, 0xff

    .line 35
    invoke-static {v1}, Lorg/apache/index/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/index/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/index/poi/hwpf/usermodel/BorderCode;->_dptLineWidth:Lorg/apache/index/poi/util/BitField;

    .line 36
    const v0, 0xff00

    invoke-static {v0}, Lorg/apache/index/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/index/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/index/poi/hwpf/usermodel/BorderCode;->_brcType:Lorg/apache/index/poi/util/BitField;

    .line 39
    invoke-static {v1}, Lorg/apache/index/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/index/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/index/poi/hwpf/usermodel/BorderCode;->_ico:Lorg/apache/index/poi/util/BitField;

    .line 40
    const/16 v0, 0x1f00

    invoke-static {v0}, Lorg/apache/index/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/index/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/index/poi/hwpf/usermodel/BorderCode;->_dptSpace:Lorg/apache/index/poi/util/BitField;

    .line 41
    const/16 v0, 0x2000

    invoke-static {v0}, Lorg/apache/index/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/index/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/index/poi/hwpf/usermodel/BorderCode;->_fShadow:Lorg/apache/index/poi/util/BitField;

    .line 42
    const/16 v0, 0x4000

    invoke-static {v0}, Lorg/apache/index/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/index/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/index/poi/hwpf/usermodel/BorderCode;->_fFrame:Lorg/apache/index/poi/util/BitField;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    return-void
.end method

.method public constructor <init>([BI)V
    .locals 1
    .param p1, "buf"    # [B
    .param p2, "offset"    # I

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    invoke-static {p1, p2}, Lorg/apache/index/poi/util/LittleEndian;->getShort([BI)S

    move-result v0

    iput-short v0, p0, Lorg/apache/index/poi/hwpf/usermodel/BorderCode;->_info:S

    .line 51
    add-int/lit8 v0, p2, 0x2

    invoke-static {p1, v0}, Lorg/apache/index/poi/util/LittleEndian;->getShort([BI)S

    move-result v0

    iput-short v0, p0, Lorg/apache/index/poi/hwpf/usermodel/BorderCode;->_info2:S

    .line 52
    return-void
.end method


# virtual methods
.method public clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 90
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 74
    if-nez p1, :cond_1

    .line 83
    :cond_0
    :goto_0
    return v1

    .line 79
    :cond_1
    instance-of v2, p1, Lorg/apache/index/poi/hwpf/usermodel/BorderCode;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 80
    check-cast v0, Lorg/apache/index/poi/hwpf/usermodel/BorderCode;

    .line 81
    .local v0, "brc":Lorg/apache/index/poi/hwpf/usermodel/BorderCode;
    iget-short v2, p0, Lorg/apache/index/poi/hwpf/usermodel/BorderCode;->_info:S

    iget-short v3, v0, Lorg/apache/index/poi/hwpf/usermodel/BorderCode;->_info:S

    if-ne v2, v3, :cond_0

    iget-short v2, p0, Lorg/apache/index/poi/hwpf/usermodel/BorderCode;->_info2:S

    iget-short v3, v0, Lorg/apache/index/poi/hwpf/usermodel/BorderCode;->_info2:S

    if-ne v2, v3, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method public getBorderType()I
    .locals 2

    .prologue
    .line 134
    sget-object v0, Lorg/apache/index/poi/hwpf/usermodel/BorderCode;->_brcType:Lorg/apache/index/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/index/poi/hwpf/usermodel/BorderCode;->_info:S

    invoke-virtual {v0, v1}, Lorg/apache/index/poi/util/BitField;->getShortValue(S)S

    move-result v0

    return v0
.end method

.method public getColor()S
    .locals 2

    .prologue
    .line 162
    sget-object v0, Lorg/apache/index/poi/hwpf/usermodel/BorderCode;->_ico:Lorg/apache/index/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/index/poi/hwpf/usermodel/BorderCode;->_info2:S

    invoke-virtual {v0, v1}, Lorg/apache/index/poi/util/BitField;->getShortValue(S)S

    move-result v0

    return v0
.end method

.method public getLineWidth()I
    .locals 2

    .prologue
    .line 97
    sget-object v0, Lorg/apache/index/poi/hwpf/usermodel/BorderCode;->_dptLineWidth:Lorg/apache/index/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/index/poi/hwpf/usermodel/BorderCode;->_info:S

    invoke-virtual {v0, v1}, Lorg/apache/index/poi/util/BitField;->getShortValue(S)S

    move-result v0

    return v0
.end method

.method public getSpace()I
    .locals 2

    .prologue
    .line 177
    sget-object v0, Lorg/apache/index/poi/hwpf/usermodel/BorderCode;->_dptSpace:Lorg/apache/index/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/index/poi/hwpf/usermodel/BorderCode;->_info2:S

    invoke-virtual {v0, v1}, Lorg/apache/index/poi/util/BitField;->getShortValue(S)S

    move-result v0

    return v0
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 69
    iget-short v0, p0, Lorg/apache/index/poi/hwpf/usermodel/BorderCode;->_info:S

    if-nez v0, :cond_0

    iget-short v0, p0, Lorg/apache/index/poi/hwpf/usermodel/BorderCode;->_info2:S

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isFrame()Z
    .locals 2

    .prologue
    .line 200
    sget-object v0, Lorg/apache/index/poi/hwpf/usermodel/BorderCode;->_fFrame:Lorg/apache/index/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/index/poi/hwpf/usermodel/BorderCode;->_info2:S

    invoke-virtual {v0, v1}, Lorg/apache/index/poi/util/BitField;->getValue(I)I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isShadow()Z
    .locals 2

    .prologue
    .line 189
    sget-object v0, Lorg/apache/index/poi/hwpf/usermodel/BorderCode;->_fShadow:Lorg/apache/index/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/index/poi/hwpf/usermodel/BorderCode;->_info2:S

    invoke-virtual {v0, v1}, Lorg/apache/index/poi/util/BitField;->getValue(I)I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public serialize([BI)V
    .locals 2
    .param p1, "buf"    # [B
    .param p2, "offset"    # I

    .prologue
    .line 56
    iget-short v0, p0, Lorg/apache/index/poi/hwpf/usermodel/BorderCode;->_info:S

    invoke-static {p1, p2, v0}, Lorg/apache/index/poi/util/LittleEndian;->putShort([BIS)V

    .line 57
    add-int/lit8 v0, p2, 0x2

    iget-short v1, p0, Lorg/apache/index/poi/hwpf/usermodel/BorderCode;->_info2:S

    invoke-static {p1, v0, v1}, Lorg/apache/index/poi/util/LittleEndian;->putShort([BIS)V

    .line 58
    return-void
.end method

.method public setBorderType(I)V
    .locals 2
    .param p1, "borderType"    # I

    .prologue
    .line 138
    sget-object v0, Lorg/apache/index/poi/hwpf/usermodel/BorderCode;->_brcType:Lorg/apache/index/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/index/poi/hwpf/usermodel/BorderCode;->_info:S

    invoke-virtual {v0, v1, p1}, Lorg/apache/index/poi/util/BitField;->setValue(II)I

    .line 139
    return-void
.end method

.method public setColor(S)V
    .locals 2
    .param p1, "color"    # S

    .prologue
    .line 166
    sget-object v0, Lorg/apache/index/poi/hwpf/usermodel/BorderCode;->_ico:Lorg/apache/index/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/index/poi/hwpf/usermodel/BorderCode;->_info2:S

    invoke-virtual {v0, v1, p1}, Lorg/apache/index/poi/util/BitField;->setValue(II)I

    .line 167
    return-void
.end method

.method public setFrame(Z)V
    .locals 3
    .param p1, "frame"    # Z

    .prologue
    .line 204
    sget-object v1, Lorg/apache/index/poi/hwpf/usermodel/BorderCode;->_fFrame:Lorg/apache/index/poi/util/BitField;

    iget-short v2, p0, Lorg/apache/index/poi/hwpf/usermodel/BorderCode;->_info2:S

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v2, v0}, Lorg/apache/index/poi/util/BitField;->setValue(II)I

    .line 205
    return-void

    .line 204
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setLineWidth(I)V
    .locals 2
    .param p1, "lineWidth"    # I

    .prologue
    .line 101
    sget-object v0, Lorg/apache/index/poi/hwpf/usermodel/BorderCode;->_dptLineWidth:Lorg/apache/index/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/index/poi/hwpf/usermodel/BorderCode;->_info:S

    invoke-virtual {v0, v1, p1}, Lorg/apache/index/poi/util/BitField;->setValue(II)I

    .line 102
    return-void
.end method

.method public setShadow(Z)V
    .locals 3
    .param p1, "shadow"    # Z

    .prologue
    .line 193
    sget-object v1, Lorg/apache/index/poi/hwpf/usermodel/BorderCode;->_fShadow:Lorg/apache/index/poi/util/BitField;

    iget-short v2, p0, Lorg/apache/index/poi/hwpf/usermodel/BorderCode;->_info2:S

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v2, v0}, Lorg/apache/index/poi/util/BitField;->setValue(II)I

    .line 194
    return-void

    .line 193
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setSpace(I)V
    .locals 2
    .param p1, "space"    # I

    .prologue
    .line 181
    sget-object v0, Lorg/apache/index/poi/hwpf/usermodel/BorderCode;->_dptSpace:Lorg/apache/index/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/index/poi/hwpf/usermodel/BorderCode;->_info2:S

    invoke-virtual {v0, v1, p1}, Lorg/apache/index/poi/util/BitField;->setValue(II)I

    .line 182
    return-void
.end method

.method public toInt()I
    .locals 2

    .prologue
    .line 62
    const/4 v1, 0x4

    new-array v0, v1, [B

    .line 63
    .local v0, "buf":[B
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lorg/apache/index/poi/hwpf/usermodel/BorderCode;->serialize([BI)V

    .line 64
    invoke-static {v0}, Lorg/apache/index/poi/util/LittleEndian;->getInt([B)I

    move-result v1

    return v1
.end method
