.class public Lorg/apache/index/poi/poifs/storage/RawDataBlock;
.super Ljava/lang/Object;
.source "RawDataBlock.java"

# interfaces
.implements Lorg/apache/index/poi/poifs/storage/ListManagedBlock;


# static fields
.field private static log:Lorg/apache/index/poi/util/POILogger;


# instance fields
.field private _blockLength:I

.field private _data:[B

.field private _eof:Z

.field private _hasData:Z

.field private _isDatainStream:Z

.field private _startIndex:I

.field private mfileHandler:Lorg/apache/index/poi/poifs/filesystem/POIFSFileHandler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 51
    const-class v0, Lorg/apache/index/poi/poifs/storage/RawDataBlock;

    invoke-static {v0}, Lorg/apache/index/poi/util/POILogFactory;->getLogger(Ljava/lang/Class;)Lorg/apache/index/poi/util/POILogger;

    move-result-object v0

    sput-object v0, Lorg/apache/index/poi/poifs/storage/RawDataBlock;->log:Lorg/apache/index/poi/util/POILogger;

    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 1
    .param p1, "stream"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 64
    const/16 v0, 0x200

    invoke-direct {p0, p1, v0}, Lorg/apache/index/poi/poifs/storage/RawDataBlock;-><init>(Ljava/io/InputStream;I)V

    .line 65
    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;I)V
    .locals 2
    .param p1, "stream"    # Ljava/io/InputStream;
    .param p2, "blockSize"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 80
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, v0, p1, p2, v1}, Lorg/apache/index/poi/poifs/storage/RawDataBlock;-><init>(Lorg/apache/index/poi/poifs/filesystem/POIFSFileHandler;Ljava/io/InputStream;II)V

    .line 81
    return-void
.end method

.method public constructor <init>(Lorg/apache/index/poi/poifs/filesystem/POIFSFileHandler;Ljava/io/InputStream;II)V
    .locals 6
    .param p1, "fileHandler"    # Lorg/apache/index/poi/poifs/filesystem/POIFSFileHandler;
    .param p2, "stream"    # Ljava/io/InputStream;
    .param p3, "blockSize"    # I
    .param p4, "startIndex"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 83
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 85
    new-array v2, p3, [B

    iput-object v2, p0, Lorg/apache/index/poi/poifs/storage/RawDataBlock;->_data:[B

    .line 86
    iget-object v2, p0, Lorg/apache/index/poi/poifs/storage/RawDataBlock;->_data:[B

    invoke-static {p2, v2}, Lorg/apache/index/poi/util/IOUtils;->readFully(Ljava/io/InputStream;[B)I

    move-result v0

    .line 88
    .local v0, "count":I
    iput-object p1, p0, Lorg/apache/index/poi/poifs/storage/RawDataBlock;->mfileHandler:Lorg/apache/index/poi/poifs/filesystem/POIFSFileHandler;

    .line 90
    if-lez v0, :cond_1

    move v2, v3

    :goto_0
    iput-boolean v2, p0, Lorg/apache/index/poi/poifs/storage/RawDataBlock;->_hasData:Z

    .line 92
    iput p4, p0, Lorg/apache/index/poi/poifs/storage/RawDataBlock;->_startIndex:I

    .line 93
    iput v0, p0, Lorg/apache/index/poi/poifs/storage/RawDataBlock;->_blockLength:I

    .line 97
    iget-object v2, p0, Lorg/apache/index/poi/poifs/storage/RawDataBlock;->mfileHandler:Lorg/apache/index/poi/poifs/filesystem/POIFSFileHandler;

    if-eqz v2, :cond_0

    .line 99
    const/high16 v2, 0x100000

    if-le p4, v2, :cond_0

    .line 101
    iput-boolean v3, p0, Lorg/apache/index/poi/poifs/storage/RawDataBlock;->_isDatainStream:Z

    .line 102
    const/4 v2, 0x0

    iput-object v2, p0, Lorg/apache/index/poi/poifs/storage/RawDataBlock;->_data:[B

    .line 107
    :cond_0
    const/4 v2, -0x1

    if-ne v0, v2, :cond_2

    .line 108
    iput-boolean v3, p0, Lorg/apache/index/poi/poifs/storage/RawDataBlock;->_eof:Z

    .line 129
    :goto_1
    return-void

    :cond_1
    move v2, v4

    .line 90
    goto :goto_0

    .line 110
    :cond_2
    if-eq v0, p3, :cond_4

    .line 114
    iput-boolean v3, p0, Lorg/apache/index/poi/poifs/storage/RawDataBlock;->_eof:Z

    .line 115
    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v2, " byte"

    invoke-direct {v4, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-ne v0, v3, :cond_3

    const-string/jumbo v2, ""

    .line 116
    :goto_2
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 115
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 118
    .local v1, "type":Ljava/lang/String;
    sget-object v2, Lorg/apache/index/poi/poifs/storage/RawDataBlock;->log:Lorg/apache/index/poi/util/POILogger;

    sget v3, Lorg/apache/index/poi/util/POILogger;->ERROR:I

    .line 119
    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "Unable to read entire block; "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 120
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " read before EOF; expected "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 121
    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " bytes. Your document "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 122
    const-string/jumbo v5, "was either written by software that "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 123
    const-string/jumbo v5, "ignores the spec, or has been truncated!"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 119
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 118
    invoke-virtual {v2, v3, v4}, Lorg/apache/index/poi/util/POILogger;->log(ILjava/lang/Object;)V

    goto :goto_1

    .line 116
    .end local v1    # "type":Ljava/lang/String;
    :cond_3
    const-string/jumbo v2, "s"

    goto :goto_2

    .line 127
    :cond_4
    iput-boolean v4, p0, Lorg/apache/index/poi/poifs/storage/RawDataBlock;->_eof:Z

    goto :goto_1
.end method

.method private fillBuffer()V
    .locals 4

    .prologue
    .line 222
    iget-boolean v0, p0, Lorg/apache/index/poi/poifs/storage/RawDataBlock;->_isDatainStream:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/index/poi/poifs/storage/RawDataBlock;->mfileHandler:Lorg/apache/index/poi/poifs/filesystem/POIFSFileHandler;

    if-eqz v0, :cond_0

    .line 225
    iget v0, p0, Lorg/apache/index/poi/poifs/storage/RawDataBlock;->_blockLength:I

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/apache/index/poi/poifs/storage/RawDataBlock;->_data:[B

    .line 226
    iget-object v0, p0, Lorg/apache/index/poi/poifs/storage/RawDataBlock;->mfileHandler:Lorg/apache/index/poi/poifs/filesystem/POIFSFileHandler;

    iget-object v1, p0, Lorg/apache/index/poi/poifs/storage/RawDataBlock;->_data:[B

    iget v2, p0, Lorg/apache/index/poi/poifs/storage/RawDataBlock;->_startIndex:I

    iget v3, p0, Lorg/apache/index/poi/poifs/storage/RawDataBlock;->_blockLength:I

    invoke-virtual {v0, v1, v2, v3}, Lorg/apache/index/poi/poifs/filesystem/POIFSFileHandler;->getBlockData([BII)I

    .line 227
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/index/poi/poifs/storage/RawDataBlock;->_isDatainStream:Z

    .line 229
    :cond_0
    return-void
.end method


# virtual methods
.method public eof()Z
    .locals 1

    .prologue
    .line 139
    iget-boolean v0, p0, Lorg/apache/index/poi/poifs/storage/RawDataBlock;->_eof:Z

    return v0
.end method

.method public getBigBlockSize()I
    .locals 1

    .prologue
    .line 208
    iget-boolean v0, p0, Lorg/apache/index/poi/poifs/storage/RawDataBlock;->_isDatainStream:Z

    if-nez v0, :cond_0

    .line 210
    iget-object v0, p0, Lorg/apache/index/poi/poifs/storage/RawDataBlock;->_data:[B

    array-length v0, v0

    .line 214
    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lorg/apache/index/poi/poifs/storage/RawDataBlock;->_blockLength:I

    goto :goto_0
.end method

.method public getBlockLength()I
    .locals 1

    .prologue
    .line 187
    iget v0, p0, Lorg/apache/index/poi/poifs/storage/RawDataBlock;->_blockLength:I

    return v0
.end method

.method public getCurrentStreamIndex()I
    .locals 2

    .prologue
    .line 151
    iget v0, p0, Lorg/apache/index/poi/poifs/storage/RawDataBlock;->_startIndex:I

    iget v1, p0, Lorg/apache/index/poi/poifs/storage/RawDataBlock;->_blockLength:I

    add-int/2addr v0, v1

    return v0
.end method

.method public getData()[B
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 170
    invoke-virtual {p0}, Lorg/apache/index/poi/poifs/storage/RawDataBlock;->hasData()Z

    move-result v0

    if-nez v0, :cond_0

    .line 172
    new-instance v0, Ljava/io/IOException;

    const-string/jumbo v1, "Cannot return empty data"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 174
    :cond_0
    iget-object v0, p0, Lorg/apache/index/poi/poifs/storage/RawDataBlock;->_data:[B

    return-object v0
.end method

.method public getFileHandler()Lorg/apache/index/poi/poifs/filesystem/POIFSFileHandler;
    .locals 1

    .prologue
    .line 199
    iget-object v0, p0, Lorg/apache/index/poi/poifs/storage/RawDataBlock;->mfileHandler:Lorg/apache/index/poi/poifs/filesystem/POIFSFileHandler;

    return-object v0
.end method

.method public getIsDatainStream()Z
    .locals 1

    .prologue
    .line 193
    iget-boolean v0, p0, Lorg/apache/index/poi/poifs/storage/RawDataBlock;->_isDatainStream:Z

    return v0
.end method

.method public getStartIndex()I
    .locals 1

    .prologue
    .line 181
    iget v0, p0, Lorg/apache/index/poi/poifs/storage/RawDataBlock;->_startIndex:I

    return v0
.end method

.method public getXBATData()[B
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 235
    invoke-virtual {p0}, Lorg/apache/index/poi/poifs/storage/RawDataBlock;->hasData()Z

    move-result v0

    if-nez v0, :cond_0

    .line 237
    new-instance v0, Ljava/io/IOException;

    const-string/jumbo v1, "Cannot return empty data"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 240
    :cond_0
    iget-boolean v0, p0, Lorg/apache/index/poi/poifs/storage/RawDataBlock;->_isDatainStream:Z

    if-eqz v0, :cond_1

    .line 242
    invoke-direct {p0}, Lorg/apache/index/poi/poifs/storage/RawDataBlock;->fillBuffer()V

    .line 244
    iget-object v0, p0, Lorg/apache/index/poi/poifs/storage/RawDataBlock;->_data:[B

    .line 248
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lorg/apache/index/poi/poifs/storage/RawDataBlock;->_data:[B

    goto :goto_0
.end method

.method public hasData()Z
    .locals 1

    .prologue
    .line 147
    iget-boolean v0, p0, Lorg/apache/index/poi/poifs/storage/RawDataBlock;->_hasData:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 155
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "RawDataBlock of size "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lorg/apache/index/poi/poifs/storage/RawDataBlock;->_data:[B

    array-length v1, v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
