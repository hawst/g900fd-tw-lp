.class public final Lorg/apache/index/poi/hwpf/model/FileInformationBlock;
.super Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;
.source "FileInformationBlock.java"

# interfaces
.implements Ljava/lang/Cloneable;


# instance fields
.field _fieldHandler:Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;

.field _longHandler:Lorg/apache/index/poi/hwpf/model/FIBLongHandler;

.field _shortHandler:Lorg/apache/index/poi/hwpf/model/FIBShortHandler;


# direct methods
.method public constructor <init>([B)V
    .locals 1
    .param p1, "mainDocument"    # [B

    .prologue
    .line 49
    invoke-direct {p0}, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;-><init>()V

    .line 51
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lorg/apache/index/poi/hwpf/model/FileInformationBlock;->fillFields([BI)V

    .line 52
    return-void
.end method


# virtual methods
.method public clearOffsetsSizes()V
    .locals 1

    .prologue
    .line 458
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;

    invoke-virtual {v0}, Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;->clearFields()V

    .line 459
    return-void
.end method

.method protected clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 554
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public fillVariableFields([B[B)V
    .locals 6
    .param p1, "mainDocument"    # [B
    .param p2, "tableStream"    # [B

    .prologue
    const/4 v5, 0x1

    .line 56
    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    .line 57
    .local v4, "fieldSet":Ljava/util/HashSet;
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 58
    const/16 v0, 0x21

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 59
    const/16 v0, 0x1f

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 60
    const/16 v0, 0xc

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 61
    const/16 v0, 0xd

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 62
    const/4 v0, 0x6

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 63
    const/16 v0, 0x49

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 64
    const/16 v0, 0x4a

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 65
    const/16 v0, 0x13

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 66
    const/16 v0, 0x30

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 67
    const/16 v0, 0x12

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 68
    const/16 v0, 0x11

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 69
    const/16 v0, 0x3b

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 70
    const/16 v0, 0x10

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 71
    const/16 v0, 0x39

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 72
    const/16 v0, 0xf

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 73
    const/16 v0, 0x33

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 74
    const/16 v0, 0x47

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 75
    const/16 v0, 0x57

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 78
    new-instance v0, Lorg/apache/index/poi/hwpf/model/FIBShortHandler;

    invoke-direct {v0, p1}, Lorg/apache/index/poi/hwpf/model/FIBShortHandler;-><init>([B)V

    iput-object v0, p0, Lorg/apache/index/poi/hwpf/model/FileInformationBlock;->_shortHandler:Lorg/apache/index/poi/hwpf/model/FIBShortHandler;

    .line 79
    new-instance v0, Lorg/apache/index/poi/hwpf/model/FIBLongHandler;

    iget-object v1, p0, Lorg/apache/index/poi/hwpf/model/FileInformationBlock;->_shortHandler:Lorg/apache/index/poi/hwpf/model/FIBShortHandler;

    invoke-virtual {v1}, Lorg/apache/index/poi/hwpf/model/FIBShortHandler;->sizeInBytes()I

    move-result v1

    add-int/lit8 v1, v1, 0x20

    invoke-direct {v0, p1, v1}, Lorg/apache/index/poi/hwpf/model/FIBLongHandler;-><init>([BI)V

    iput-object v0, p0, Lorg/apache/index/poi/hwpf/model/FileInformationBlock;->_longHandler:Lorg/apache/index/poi/hwpf/model/FIBLongHandler;

    .line 80
    new-instance v0, Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;

    .line 81
    iget-object v1, p0, Lorg/apache/index/poi/hwpf/model/FileInformationBlock;->_shortHandler:Lorg/apache/index/poi/hwpf/model/FIBShortHandler;

    invoke-virtual {v1}, Lorg/apache/index/poi/hwpf/model/FIBShortHandler;->sizeInBytes()I

    move-result v1

    add-int/lit8 v1, v1, 0x20

    iget-object v2, p0, Lorg/apache/index/poi/hwpf/model/FileInformationBlock;->_longHandler:Lorg/apache/index/poi/hwpf/model/FIBLongHandler;

    invoke-virtual {v2}, Lorg/apache/index/poi/hwpf/model/FIBLongHandler;->sizeInBytes()I

    move-result v2

    add-int/2addr v2, v1

    move-object v1, p1

    move-object v3, p2

    .line 82
    invoke-direct/range {v0 .. v5}, Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;-><init>([BI[BLjava/util/HashSet;Z)V

    .line 80
    iput-object v0, p0, Lorg/apache/index/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;

    .line 83
    return-void
.end method

.method public getCbMac()I
    .locals 2

    .prologue
    .line 351
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/FileInformationBlock;->_longHandler:Lorg/apache/index/poi/hwpf/model/FIBLongHandler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lorg/apache/index/poi/hwpf/model/FIBLongHandler;->getLong(I)I

    move-result v0

    return v0
.end method

.method public getCcpAtn()I
    .locals 2

    .prologue
    .line 404
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/FileInformationBlock;->_longHandler:Lorg/apache/index/poi/hwpf/model/FIBLongHandler;

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Lorg/apache/index/poi/hwpf/model/FIBLongHandler;->getLong(I)I

    move-result v0

    return v0
.end method

.method public getCcpCommentAtn()I
    .locals 1

    .prologue
    .line 407
    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/model/FileInformationBlock;->getCcpAtn()I

    move-result v0

    return v0
.end method

.method public getCcpEdn()I
    .locals 2

    .prologue
    .line 420
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/FileInformationBlock;->_longHandler:Lorg/apache/index/poi/hwpf/model/FIBLongHandler;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lorg/apache/index/poi/hwpf/model/FIBLongHandler;->getLong(I)I

    move-result v0

    return v0
.end method

.method public getCcpFtn()I
    .locals 2

    .prologue
    .line 378
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/FileInformationBlock;->_longHandler:Lorg/apache/index/poi/hwpf/model/FIBLongHandler;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lorg/apache/index/poi/hwpf/model/FIBLongHandler;->getLong(I)I

    move-result v0

    return v0
.end method

.method public getCcpHdd()I
    .locals 2

    .prologue
    .line 391
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/FileInformationBlock;->_longHandler:Lorg/apache/index/poi/hwpf/model/FIBLongHandler;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lorg/apache/index/poi/hwpf/model/FIBLongHandler;->getLong(I)I

    move-result v0

    return v0
.end method

.method public getCcpHdrTxtBx()I
    .locals 2

    .prologue
    .line 446
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/FileInformationBlock;->_longHandler:Lorg/apache/index/poi/hwpf/model/FIBLongHandler;

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Lorg/apache/index/poi/hwpf/model/FIBLongHandler;->getLong(I)I

    move-result v0

    return v0
.end method

.method public getCcpText()I
    .locals 2

    .prologue
    .line 365
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/FileInformationBlock;->_longHandler:Lorg/apache/index/poi/hwpf/model/FIBLongHandler;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lorg/apache/index/poi/hwpf/model/FIBLongHandler;->getLong(I)I

    move-result v0

    return v0
.end method

.method public getCcpTxtBx()I
    .locals 2

    .prologue
    .line 433
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/FileInformationBlock;->_longHandler:Lorg/apache/index/poi/hwpf/model/FIBLongHandler;

    const/16 v1, 0x9

    invoke-virtual {v0, v1}, Lorg/apache/index/poi/hwpf/model/FIBLongHandler;->getLong(I)I

    move-result v0

    return v0
.end method

.method public getFcClx()I
    .locals 2

    .prologue
    .line 127
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0x21

    invoke-virtual {v0, v1}, Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;->getFieldOffset(I)I

    move-result v0

    return v0
.end method

.method public getFcDggInfo()I
    .locals 2

    .prologue
    .line 543
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0x32

    invoke-virtual {v0, v1}, Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;->getFieldOffset(I)I

    move-result v0

    return v0
.end method

.method public getFcDop()I
    .locals 2

    .prologue
    .line 87
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0x1f

    invoke-virtual {v0, v1}, Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;->getFieldOffset(I)I

    move-result v0

    return v0
.end method

.method public getFcPlcfLst()I
    .locals 2

    .prologue
    .line 207
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0x49

    invoke-virtual {v0, v1}, Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;->getFieldOffset(I)I

    move-result v0

    return v0
.end method

.method public getFcPlcfbteChpx()I
    .locals 2

    .prologue
    .line 147
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0xc

    invoke-virtual {v0, v1}, Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;->getFieldOffset(I)I

    move-result v0

    return v0
.end method

.method public getFcPlcfbtePapx()I
    .locals 2

    .prologue
    .line 167
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0xd

    invoke-virtual {v0, v1}, Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;->getFieldOffset(I)I

    move-result v0

    return v0
.end method

.method public getFcPlcffldAtn()I
    .locals 2

    .prologue
    .line 463
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0x13

    invoke-virtual {v0, v1}, Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;->getFieldOffset(I)I

    move-result v0

    return v0
.end method

.method public getFcPlcffldEdn()I
    .locals 2

    .prologue
    .line 473
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0x30

    invoke-virtual {v0, v1}, Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;->getFieldOffset(I)I

    move-result v0

    return v0
.end method

.method public getFcPlcffldFtn()I
    .locals 2

    .prologue
    .line 483
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0x12

    invoke-virtual {v0, v1}, Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;->getFieldOffset(I)I

    move-result v0

    return v0
.end method

.method public getFcPlcffldHdr()I
    .locals 2

    .prologue
    .line 493
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0x11

    invoke-virtual {v0, v1}, Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;->getFieldOffset(I)I

    move-result v0

    return v0
.end method

.method public getFcPlcffldHdrtxbx()I
    .locals 2

    .prologue
    .line 503
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0x3b

    invoke-virtual {v0, v1}, Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;->getFieldOffset(I)I

    move-result v0

    return v0
.end method

.method public getFcPlcffldMom()I
    .locals 2

    .prologue
    .line 513
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0x10

    invoke-virtual {v0, v1}, Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;->getFieldOffset(I)I

    move-result v0

    return v0
.end method

.method public getFcPlcffldTxbx()I
    .locals 2

    .prologue
    .line 523
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0x39

    invoke-virtual {v0, v1}, Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;->getFieldOffset(I)I

    move-result v0

    return v0
.end method

.method public getFcPlcfsed()I
    .locals 2

    .prologue
    .line 187
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;->getFieldOffset(I)I

    move-result v0

    return v0
.end method

.method public getFcPlcspaMom()I
    .locals 2

    .prologue
    .line 533
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0x28

    invoke-virtual {v0, v1}, Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;->getFieldOffset(I)I

    move-result v0

    return v0
.end method

.method public getFcPlfLfo()I
    .locals 2

    .prologue
    .line 227
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0x4a

    invoke-virtual {v0, v1}, Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;->getFieldOffset(I)I

    move-result v0

    return v0
.end method

.method public getFcStshf()I
    .locals 2

    .prologue
    .line 107
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;->getFieldOffset(I)I

    move-result v0

    return v0
.end method

.method public getFcSttbSavedBy()I
    .locals 2

    .prologue
    .line 308
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0x47

    invoke-virtual {v0, v1}, Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;->getFieldOffset(I)I

    move-result v0

    return v0
.end method

.method public getFcSttbfRMark()I
    .locals 2

    .prologue
    .line 267
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0x33

    invoke-virtual {v0, v1}, Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;->getFieldOffset(I)I

    move-result v0

    return v0
.end method

.method public getFcSttbfffn()I
    .locals 2

    .prologue
    .line 247
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0xf

    invoke-virtual {v0, v1}, Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;->getFieldOffset(I)I

    move-result v0

    return v0
.end method

.method public getLcbClx()I
    .locals 2

    .prologue
    .line 132
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0x21

    invoke-virtual {v0, v1}, Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;->getFieldSize(I)I

    move-result v0

    return v0
.end method

.method public getLcbDggInfo()I
    .locals 2

    .prologue
    .line 548
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0x32

    invoke-virtual {v0, v1}, Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;->getFieldSize(I)I

    move-result v0

    return v0
.end method

.method public getLcbDop()I
    .locals 2

    .prologue
    .line 97
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0x1f

    invoke-virtual {v0, v1}, Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;->getFieldSize(I)I

    move-result v0

    return v0
.end method

.method public getLcbPlcfLst()I
    .locals 2

    .prologue
    .line 212
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0x49

    invoke-virtual {v0, v1}, Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;->getFieldSize(I)I

    move-result v0

    return v0
.end method

.method public getLcbPlcfbteChpx()I
    .locals 2

    .prologue
    .line 152
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0xc

    invoke-virtual {v0, v1}, Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;->getFieldSize(I)I

    move-result v0

    return v0
.end method

.method public getLcbPlcfbtePapx()I
    .locals 2

    .prologue
    .line 172
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0xd

    invoke-virtual {v0, v1}, Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;->getFieldSize(I)I

    move-result v0

    return v0
.end method

.method public getLcbPlcffldAtn()I
    .locals 2

    .prologue
    .line 468
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0x13

    invoke-virtual {v0, v1}, Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;->getFieldSize(I)I

    move-result v0

    return v0
.end method

.method public getLcbPlcffldEdn()I
    .locals 2

    .prologue
    .line 478
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0x30

    invoke-virtual {v0, v1}, Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;->getFieldSize(I)I

    move-result v0

    return v0
.end method

.method public getLcbPlcffldFtn()I
    .locals 2

    .prologue
    .line 488
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0x12

    invoke-virtual {v0, v1}, Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;->getFieldSize(I)I

    move-result v0

    return v0
.end method

.method public getLcbPlcffldHdr()I
    .locals 2

    .prologue
    .line 498
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0x11

    invoke-virtual {v0, v1}, Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;->getFieldSize(I)I

    move-result v0

    return v0
.end method

.method public getLcbPlcffldHdrtxbx()I
    .locals 2

    .prologue
    .line 508
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0x3b

    invoke-virtual {v0, v1}, Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;->getFieldSize(I)I

    move-result v0

    return v0
.end method

.method public getLcbPlcffldMom()I
    .locals 2

    .prologue
    .line 518
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0x10

    invoke-virtual {v0, v1}, Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;->getFieldSize(I)I

    move-result v0

    return v0
.end method

.method public getLcbPlcffldTxbx()I
    .locals 2

    .prologue
    .line 528
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0x39

    invoke-virtual {v0, v1}, Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;->getFieldSize(I)I

    move-result v0

    return v0
.end method

.method public getLcbPlcfsed()I
    .locals 2

    .prologue
    .line 192
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;->getFieldSize(I)I

    move-result v0

    return v0
.end method

.method public getLcbPlcspaMom()I
    .locals 2

    .prologue
    .line 538
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0x28

    invoke-virtual {v0, v1}, Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;->getFieldSize(I)I

    move-result v0

    return v0
.end method

.method public getLcbPlfLfo()I
    .locals 2

    .prologue
    .line 232
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0x4a

    invoke-virtual {v0, v1}, Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;->getFieldSize(I)I

    move-result v0

    return v0
.end method

.method public getLcbStshf()I
    .locals 2

    .prologue
    .line 112
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;->getFieldSize(I)I

    move-result v0

    return v0
.end method

.method public getLcbSttbSavedBy()I
    .locals 2

    .prologue
    .line 313
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0x47

    invoke-virtual {v0, v1}, Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;->getFieldSize(I)I

    move-result v0

    return v0
.end method

.method public getLcbSttbfRMark()I
    .locals 2

    .prologue
    .line 272
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0x33

    invoke-virtual {v0, v1}, Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;->getFieldSize(I)I

    move-result v0

    return v0
.end method

.method public getLcbSttbfffn()I
    .locals 2

    .prologue
    .line 252
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0xf

    invoke-virtual {v0, v1}, Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;->getFieldSize(I)I

    move-result v0

    return v0
.end method

.method public getModifiedHigh()I
    .locals 2

    .prologue
    .line 333
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0x4a

    invoke-virtual {v0, v1}, Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;->getFieldSize(I)I

    move-result v0

    return v0
.end method

.method public getModifiedLow()I
    .locals 2

    .prologue
    .line 328
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0x4a

    invoke-virtual {v0, v1}, Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;->getFieldOffset(I)I

    move-result v0

    return v0
.end method

.method public getPlcfHddOffset()I
    .locals 2

    .prologue
    .line 290
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;->getFieldOffset(I)I

    move-result v0

    return v0
.end method

.method public getPlcfHddSize()I
    .locals 2

    .prologue
    .line 297
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;->getFieldSize(I)I

    move-result v0

    return v0
.end method

.method public setCbMac(I)V
    .locals 2
    .param p1, "cbMac"    # I

    .prologue
    .line 358
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/FileInformationBlock;->_longHandler:Lorg/apache/index/poi/hwpf/model/FIBLongHandler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p1}, Lorg/apache/index/poi/hwpf/model/FIBLongHandler;->setLong(II)V

    .line 359
    return-void
.end method

.method public setCcpAtn(I)V
    .locals 2
    .param p1, "ccpAtn"    # I

    .prologue
    .line 413
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/FileInformationBlock;->_longHandler:Lorg/apache/index/poi/hwpf/model/FIBLongHandler;

    const/4 v1, 0x7

    invoke-virtual {v0, v1, p1}, Lorg/apache/index/poi/hwpf/model/FIBLongHandler;->setLong(II)V

    .line 414
    return-void
.end method

.method public setCcpEdn(I)V
    .locals 2
    .param p1, "ccpEdn"    # I

    .prologue
    .line 426
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/FileInformationBlock;->_longHandler:Lorg/apache/index/poi/hwpf/model/FIBLongHandler;

    const/16 v1, 0x8

    invoke-virtual {v0, v1, p1}, Lorg/apache/index/poi/hwpf/model/FIBLongHandler;->setLong(II)V

    .line 427
    return-void
.end method

.method public setCcpFtn(I)V
    .locals 2
    .param p1, "ccpFtn"    # I

    .prologue
    .line 384
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/FileInformationBlock;->_longHandler:Lorg/apache/index/poi/hwpf/model/FIBLongHandler;

    const/4 v1, 0x4

    invoke-virtual {v0, v1, p1}, Lorg/apache/index/poi/hwpf/model/FIBLongHandler;->setLong(II)V

    .line 385
    return-void
.end method

.method public setCcpHdd(I)V
    .locals 2
    .param p1, "ccpHdd"    # I

    .prologue
    .line 397
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/FileInformationBlock;->_longHandler:Lorg/apache/index/poi/hwpf/model/FIBLongHandler;

    const/4 v1, 0x5

    invoke-virtual {v0, v1, p1}, Lorg/apache/index/poi/hwpf/model/FIBLongHandler;->setLong(II)V

    .line 398
    return-void
.end method

.method public setCcpHdrTxtBx(I)V
    .locals 2
    .param p1, "ccpTxtBx"    # I

    .prologue
    .line 452
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/FileInformationBlock;->_longHandler:Lorg/apache/index/poi/hwpf/model/FIBLongHandler;

    const/16 v1, 0xa

    invoke-virtual {v0, v1, p1}, Lorg/apache/index/poi/hwpf/model/FIBLongHandler;->setLong(II)V

    .line 453
    return-void
.end method

.method public setCcpText(I)V
    .locals 2
    .param p1, "ccpText"    # I

    .prologue
    .line 371
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/FileInformationBlock;->_longHandler:Lorg/apache/index/poi/hwpf/model/FIBLongHandler;

    const/4 v1, 0x3

    invoke-virtual {v0, v1, p1}, Lorg/apache/index/poi/hwpf/model/FIBLongHandler;->setLong(II)V

    .line 372
    return-void
.end method

.method public setCcpTxtBx(I)V
    .locals 2
    .param p1, "ccpTxtBx"    # I

    .prologue
    .line 439
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/FileInformationBlock;->_longHandler:Lorg/apache/index/poi/hwpf/model/FIBLongHandler;

    const/16 v1, 0x9

    invoke-virtual {v0, v1, p1}, Lorg/apache/index/poi/hwpf/model/FIBLongHandler;->setLong(II)V

    .line 440
    return-void
.end method

.method public setFcClx(I)V
    .locals 2
    .param p1, "fcClx"    # I

    .prologue
    .line 137
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0x21

    invoke-virtual {v0, v1, p1}, Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;->setFieldOffset(II)V

    .line 138
    return-void
.end method

.method public setFcDop(I)V
    .locals 2
    .param p1, "fcDop"    # I

    .prologue
    .line 92
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0x1f

    invoke-virtual {v0, v1, p1}, Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;->setFieldOffset(II)V

    .line 93
    return-void
.end method

.method public setFcPlcfLst(I)V
    .locals 2
    .param p1, "fcPlcfLst"    # I

    .prologue
    .line 217
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0x49

    invoke-virtual {v0, v1, p1}, Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;->setFieldOffset(II)V

    .line 218
    return-void
.end method

.method public setFcPlcfbteChpx(I)V
    .locals 2
    .param p1, "fcPlcfBteChpx"    # I

    .prologue
    .line 157
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0xc

    invoke-virtual {v0, v1, p1}, Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;->setFieldOffset(II)V

    .line 158
    return-void
.end method

.method public setFcPlcfbtePapx(I)V
    .locals 2
    .param p1, "fcPlcfBtePapx"    # I

    .prologue
    .line 177
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0xd

    invoke-virtual {v0, v1, p1}, Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;->setFieldOffset(II)V

    .line 178
    return-void
.end method

.method public setFcPlcfsed(I)V
    .locals 2
    .param p1, "fcPlcfSed"    # I

    .prologue
    .line 197
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;

    const/4 v1, 0x6

    invoke-virtual {v0, v1, p1}, Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;->setFieldOffset(II)V

    .line 198
    return-void
.end method

.method public setFcPlfLfo(I)V
    .locals 2
    .param p1, "fcPlfLfo"    # I

    .prologue
    .line 237
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0x4a

    invoke-virtual {v0, v1, p1}, Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;->setFieldOffset(II)V

    .line 238
    return-void
.end method

.method public setFcStshf(I)V
    .locals 2
    .param p1, "fcStshf"    # I

    .prologue
    .line 117
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1, p1}, Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;->setFieldOffset(II)V

    .line 118
    return-void
.end method

.method public setFcSttbSavedBy(I)V
    .locals 2
    .param p1, "fcSttbSavedBy"    # I

    .prologue
    .line 318
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0x47

    invoke-virtual {v0, v1, p1}, Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;->setFieldOffset(II)V

    .line 319
    return-void
.end method

.method public setFcSttbfRMark(I)V
    .locals 2
    .param p1, "fcSttbfRMark"    # I

    .prologue
    .line 277
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0x33

    invoke-virtual {v0, v1, p1}, Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;->setFieldOffset(II)V

    .line 278
    return-void
.end method

.method public setFcSttbfffn(I)V
    .locals 2
    .param p1, "fcSttbFffn"    # I

    .prologue
    .line 257
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0xf

    invoke-virtual {v0, v1, p1}, Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;->setFieldOffset(II)V

    .line 258
    return-void
.end method

.method public setLcbClx(I)V
    .locals 2
    .param p1, "lcbClx"    # I

    .prologue
    .line 142
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0x21

    invoke-virtual {v0, v1, p1}, Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;->setFieldSize(II)V

    .line 143
    return-void
.end method

.method public setLcbDop(I)V
    .locals 2
    .param p1, "lcbDop"    # I

    .prologue
    .line 102
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0x1f

    invoke-virtual {v0, v1, p1}, Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;->setFieldSize(II)V

    .line 103
    return-void
.end method

.method public setLcbPlcfLst(I)V
    .locals 2
    .param p1, "lcbPlcfLst"    # I

    .prologue
    .line 222
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0x49

    invoke-virtual {v0, v1, p1}, Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;->setFieldSize(II)V

    .line 223
    return-void
.end method

.method public setLcbPlcfbteChpx(I)V
    .locals 2
    .param p1, "lcbPlcfBteChpx"    # I

    .prologue
    .line 162
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0xc

    invoke-virtual {v0, v1, p1}, Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;->setFieldSize(II)V

    .line 163
    return-void
.end method

.method public setLcbPlcfbtePapx(I)V
    .locals 2
    .param p1, "lcbPlcfBtePapx"    # I

    .prologue
    .line 182
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0xd

    invoke-virtual {v0, v1, p1}, Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;->setFieldSize(II)V

    .line 183
    return-void
.end method

.method public setLcbPlcfsed(I)V
    .locals 2
    .param p1, "lcbPlcfSed"    # I

    .prologue
    .line 202
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;

    const/4 v1, 0x6

    invoke-virtual {v0, v1, p1}, Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;->setFieldSize(II)V

    .line 203
    return-void
.end method

.method public setLcbPlfLfo(I)V
    .locals 2
    .param p1, "lcbPlfLfo"    # I

    .prologue
    .line 242
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0x4a

    invoke-virtual {v0, v1, p1}, Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;->setFieldSize(II)V

    .line 243
    return-void
.end method

.method public setLcbStshf(I)V
    .locals 2
    .param p1, "lcbStshf"    # I

    .prologue
    .line 122
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1, p1}, Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;->setFieldSize(II)V

    .line 123
    return-void
.end method

.method public setLcbSttbSavedBy(I)V
    .locals 2
    .param p1, "fcSttbSavedBy"    # I

    .prologue
    .line 323
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0x47

    invoke-virtual {v0, v1, p1}, Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;->setFieldSize(II)V

    .line 324
    return-void
.end method

.method public setLcbSttbfRMark(I)V
    .locals 2
    .param p1, "lcbSttbfRMark"    # I

    .prologue
    .line 282
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0x33

    invoke-virtual {v0, v1, p1}, Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;->setFieldSize(II)V

    .line 283
    return-void
.end method

.method public setLcbSttbfffn(I)V
    .locals 2
    .param p1, "lcbSttbFffn"    # I

    .prologue
    .line 262
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0xf

    invoke-virtual {v0, v1, p1}, Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;->setFieldSize(II)V

    .line 263
    return-void
.end method

.method public setModifiedHigh(I)V
    .locals 2
    .param p1, "modifiedHigh"    # I

    .prologue
    .line 343
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0x4a

    invoke-virtual {v0, v1, p1}, Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;->setFieldSize(II)V

    .line 344
    return-void
.end method

.method public setModifiedLow(I)V
    .locals 2
    .param p1, "modifiedLow"    # I

    .prologue
    .line 338
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0x4a

    invoke-virtual {v0, v1, p1}, Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;->setFieldOffset(II)V

    .line 339
    return-void
.end method

.method public setPlcfHddOffset(I)V
    .locals 2
    .param p1, "fcPlcfHdd"    # I

    .prologue
    .line 300
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0xb

    invoke-virtual {v0, v1, p1}, Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;->setFieldOffset(II)V

    .line 301
    return-void
.end method

.method public setPlcfHddSize(I)V
    .locals 2
    .param p1, "lcbPlcfHdd"    # I

    .prologue
    .line 303
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/FileInformationBlock;->_fieldHandler:Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;

    const/16 v1, 0xb

    invoke-virtual {v0, v1, p1}, Lorg/apache/index/poi/hwpf/model/FIBFieldHandler;->setFieldSize(II)V

    .line 304
    return-void
.end method
