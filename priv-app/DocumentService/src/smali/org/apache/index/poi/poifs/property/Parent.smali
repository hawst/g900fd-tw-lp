.class public interface abstract Lorg/apache/index/poi/poifs/property/Parent;
.super Ljava/lang/Object;
.source "Parent.java"

# interfaces
.implements Lorg/apache/index/poi/poifs/property/Child;


# virtual methods
.method public abstract addChild(Lorg/apache/index/poi/poifs/property/Property;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract getChildren()Ljava/util/Iterator;
.end method

.method public abstract setNextChild(Lorg/apache/index/poi/poifs/property/Child;)V
.end method

.method public abstract setPreviousChild(Lorg/apache/index/poi/poifs/property/Child;)V
.end method
