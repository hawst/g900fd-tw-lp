.class public final Lorg/apache/index/poi/poifs/storage/BlockAllocationTableReader;
.super Ljava/lang/Object;
.source "BlockAllocationTableReader.java"


# static fields
.field private static final MAX_BLOCK_COUNT:I = 0xffff


# instance fields
.field private final _entries:Lorg/apache/index/poi/util/IntList;

.field private bigBlockSize:Lorg/apache/index/poi/poifs/common/POIFSBigBlockSize;


# direct methods
.method constructor <init>(Lorg/apache/index/poi/poifs/common/POIFSBigBlockSize;)V
    .locals 1
    .param p1, "bigBlockSize"    # Lorg/apache/index/poi/poifs/common/POIFSBigBlockSize;

    .prologue
    .line 176
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 177
    iput-object p1, p0, Lorg/apache/index/poi/poifs/storage/BlockAllocationTableReader;->bigBlockSize:Lorg/apache/index/poi/poifs/common/POIFSBigBlockSize;

    .line 178
    new-instance v0, Lorg/apache/index/poi/util/IntList;

    invoke-direct {v0}, Lorg/apache/index/poi/util/IntList;-><init>()V

    iput-object v0, p0, Lorg/apache/index/poi/poifs/storage/BlockAllocationTableReader;->_entries:Lorg/apache/index/poi/util/IntList;

    .line 179
    return-void
.end method

.method public constructor <init>(Lorg/apache/index/poi/poifs/common/POIFSBigBlockSize;I[IIILorg/apache/index/poi/poifs/storage/BlockList;)V
    .locals 17
    .param p1, "bigBlockSize"    # Lorg/apache/index/poi/poifs/common/POIFSBigBlockSize;
    .param p2, "block_count"    # I
    .param p3, "block_array"    # [I
    .param p4, "xbat_count"    # I
    .param p5, "xbat_index"    # I
    .param p6, "raw_block_list"    # Lorg/apache/index/poi/poifs/storage/BlockList;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 82
    invoke-direct/range {p0 .. p1}, Lorg/apache/index/poi/poifs/storage/BlockAllocationTableReader;-><init>(Lorg/apache/index/poi/poifs/common/POIFSBigBlockSize;)V

    .line 84
    invoke-static/range {p2 .. p2}, Lorg/apache/index/poi/poifs/storage/BlockAllocationTableReader;->sanityCheckBlockCount(I)V

    .line 92
    move-object/from16 v0, p3

    array-length v14, v0

    move/from16 v0, p2

    invoke-static {v0, v14}, Ljava/lang/Math;->min(II)I

    move-result v10

    .line 96
    .local v10, "limit":I
    move/from16 v0, p2

    new-array v4, v0, [Lorg/apache/index/poi/poifs/storage/RawDataBlock;

    .line 99
    .local v4, "blocks":[Lorg/apache/index/poi/poifs/storage/RawDataBlock;
    const/4 v2, 0x0

    .local v2, "block_index":I
    :goto_0
    if-lt v2, v10, :cond_0

    .line 115
    move/from16 v0, p2

    if-ge v2, v0, :cond_3

    .line 119
    if-gez p5, :cond_2

    .line 121
    new-instance v14, Ljava/io/IOException;

    .line 122
    const-string/jumbo v15, "BAT count exceeds limit, yet XBAT index indicates no valid entries"

    .line 121
    invoke-direct {v14, v15}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v14

    .line 102
    :cond_0
    aget v12, p3, v2

    .line 103
    .local v12, "nextOffset":I
    invoke-interface/range {p6 .. p6}, Lorg/apache/index/poi/poifs/storage/BlockList;->blockCount()I

    move-result v14

    if-le v12, v14, :cond_1

    .line 104
    new-instance v14, Ljava/io/IOException;

    new-instance v15, Ljava/lang/StringBuilder;

    const-string/jumbo v16, "Your file contains "

    invoke-direct/range {v15 .. v16}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface/range {p6 .. p6}, Lorg/apache/index/poi/poifs/storage/BlockList;->blockCount()I

    move-result v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    .line 105
    const-string/jumbo v16, " sectors, but the initial DIFAT array at index "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    .line 106
    const-string/jumbo v16, " referenced block # "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string/jumbo v16, ". This isn\'t allowed and "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    .line 107
    const-string/jumbo v16, " your file is corrupt"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    .line 104
    invoke-direct {v14, v15}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v14

    .line 111
    :cond_1
    move-object/from16 v0, p6

    invoke-interface {v0, v12}, Lorg/apache/index/poi/poifs/storage/BlockList;->remove(I)Lorg/apache/index/poi/poifs/storage/ListManagedBlock;

    move-result-object v14

    check-cast v14, Lorg/apache/index/poi/poifs/storage/RawDataBlock;

    .line 110
    aput-object v14, v4, v2

    .line 99
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 124
    .end local v12    # "nextOffset":I
    :cond_2
    move/from16 v5, p5

    .line 125
    .local v5, "chain_index":I
    invoke-virtual/range {p1 .. p1}, Lorg/apache/index/poi/poifs/common/POIFSBigBlockSize;->getXBATEntriesPerBlock()I

    move-result v11

    .line 126
    .local v11, "max_entries_per_block":I
    invoke-virtual/range {p1 .. p1}, Lorg/apache/index/poi/poifs/common/POIFSBigBlockSize;->getNextXBATChainOffset()I

    move-result v6

    .line 131
    .local v6, "chain_index_offset":I
    const/4 v8, 0x0

    .local v8, "j":I
    :goto_1
    move/from16 v0, p4

    if-lt v8, v0, :cond_4

    .line 152
    .end local v5    # "chain_index":I
    .end local v6    # "chain_index_offset":I
    .end local v8    # "j":I
    .end local v11    # "max_entries_per_block":I
    :cond_3
    :goto_2
    move/from16 v0, p2

    if-eq v2, v0, :cond_7

    .line 154
    new-instance v14, Ljava/io/IOException;

    const-string/jumbo v15, "Could not find all blocks"

    invoke-direct {v14, v15}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v14

    .line 133
    .restart local v5    # "chain_index":I
    .restart local v6    # "chain_index_offset":I
    .restart local v8    # "j":I
    .restart local v11    # "max_entries_per_block":I
    :cond_4
    sub-int v14, p2, v2

    invoke-static {v14, v11}, Ljava/lang/Math;->min(II)I

    move-result v10

    .line 135
    move-object/from16 v0, p6

    invoke-interface {v0, v5}, Lorg/apache/index/poi/poifs/storage/BlockList;->remove(I)Lorg/apache/index/poi/poifs/storage/ListManagedBlock;

    move-result-object v14

    invoke-interface {v14}, Lorg/apache/index/poi/poifs/storage/ListManagedBlock;->getXBATData()[B

    move-result-object v7

    .line 136
    .local v7, "data":[B
    const/4 v13, 0x0

    .line 138
    .local v13, "offset":I
    const/4 v9, 0x0

    .local v9, "k":I
    move v3, v2

    .end local v2    # "block_index":I
    .local v3, "block_index":I
    :goto_3
    if-lt v9, v10, :cond_5

    .line 145
    invoke-static {v7, v6}, Lorg/apache/index/poi/util/LittleEndian;->getInt([BI)I

    move-result v5

    .line 146
    const/4 v14, -0x2

    if-ne v5, v14, :cond_6

    move v2, v3

    .line 148
    .end local v3    # "block_index":I
    .restart local v2    # "block_index":I
    goto :goto_2

    .line 140
    .end local v2    # "block_index":I
    .restart local v3    # "block_index":I
    :cond_5
    add-int/lit8 v2, v3, 0x1

    .line 142
    .end local v3    # "block_index":I
    .restart local v2    # "block_index":I
    invoke-static {v7, v13}, Lorg/apache/index/poi/util/LittleEndian;->getInt([BI)I

    move-result v14

    move-object/from16 v0, p6

    invoke-interface {v0, v14}, Lorg/apache/index/poi/poifs/storage/BlockList;->remove(I)Lorg/apache/index/poi/poifs/storage/ListManagedBlock;

    move-result-object v14

    .line 141
    check-cast v14, Lorg/apache/index/poi/poifs/storage/RawDataBlock;

    .line 140
    aput-object v14, v4, v3

    .line 143
    add-int/lit8 v13, v13, 0x4

    .line 138
    add-int/lit8 v9, v9, 0x1

    move v3, v2

    .end local v2    # "block_index":I
    .restart local v3    # "block_index":I
    goto :goto_3

    .line 131
    :cond_6
    add-int/lit8 v8, v8, 0x1

    move v2, v3

    .end local v3    # "block_index":I
    .restart local v2    # "block_index":I
    goto :goto_1

    .line 159
    .end local v5    # "chain_index":I
    .end local v6    # "chain_index_offset":I
    .end local v7    # "data":[B
    .end local v8    # "j":I
    .end local v9    # "k":I
    .end local v11    # "max_entries_per_block":I
    .end local v13    # "offset":I
    :cond_7
    move-object/from16 v0, p0

    move-object/from16 v1, p6

    invoke-direct {v0, v4, v1}, Lorg/apache/index/poi/poifs/storage/BlockAllocationTableReader;->setEntries([Lorg/apache/index/poi/poifs/storage/ListManagedBlock;Lorg/apache/index/poi/poifs/storage/BlockList;)V

    .line 160
    return-void
.end method

.method constructor <init>(Lorg/apache/index/poi/poifs/common/POIFSBigBlockSize;[Lorg/apache/index/poi/poifs/storage/ListManagedBlock;Lorg/apache/index/poi/poifs/storage/BlockList;)V
    .locals 0
    .param p1, "bigBlockSize"    # Lorg/apache/index/poi/poifs/common/POIFSBigBlockSize;
    .param p2, "blocks"    # [Lorg/apache/index/poi/poifs/storage/ListManagedBlock;
    .param p3, "raw_block_list"    # Lorg/apache/index/poi/poifs/storage/BlockList;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 172
    invoke-direct {p0, p1}, Lorg/apache/index/poi/poifs/storage/BlockAllocationTableReader;-><init>(Lorg/apache/index/poi/poifs/common/POIFSBigBlockSize;)V

    .line 173
    invoke-direct {p0, p2, p3}, Lorg/apache/index/poi/poifs/storage/BlockAllocationTableReader;->setEntries([Lorg/apache/index/poi/poifs/storage/ListManagedBlock;Lorg/apache/index/poi/poifs/storage/BlockList;)V

    .line 174
    return-void
.end method

.method public static sanityCheckBlockCount(I)V
    .locals 4
    .param p0, "block_count"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const v3, 0xffff

    .line 182
    if-gtz p0, :cond_0

    .line 183
    new-instance v0, Ljava/io/IOException;

    .line 184
    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Illegal block count; minimum count is 1, got "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 185
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " instead"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 184
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 183
    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 188
    :cond_0
    if-le p0, v3, :cond_1

    .line 189
    new-instance v0, Ljava/io/IOException;

    .line 190
    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Block count "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 191
    const-string/jumbo v2, " is too high. POI maximum is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 190
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 189
    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 194
    :cond_1
    return-void
.end method

.method private setEntries([Lorg/apache/index/poi/poifs/storage/ListManagedBlock;Lorg/apache/index/poi/poifs/storage/BlockList;)V
    .locals 7
    .param p1, "blocks"    # [Lorg/apache/index/poi/poifs/storage/ListManagedBlock;
    .param p2, "raw_blocks"    # Lorg/apache/index/poi/poifs/storage/BlockList;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 292
    iget-object v6, p0, Lorg/apache/index/poi/poifs/storage/BlockAllocationTableReader;->bigBlockSize:Lorg/apache/index/poi/poifs/common/POIFSBigBlockSize;

    invoke-virtual {v6}, Lorg/apache/index/poi/poifs/common/POIFSBigBlockSize;->getBATEntriesPerBlock()I

    move-result v4

    .line 294
    .local v4, "limit":I
    const/4 v0, 0x0

    .local v0, "block_index":I
    :goto_0
    array-length v6, p1

    if-lt v0, v6, :cond_0

    .line 314
    invoke-interface {p2, p0}, Lorg/apache/index/poi/poifs/storage/BlockList;->setBAT(Lorg/apache/index/poi/poifs/storage/BlockAllocationTableReader;)V

    .line 315
    return-void

    .line 296
    :cond_0
    aget-object v6, p1, v0

    invoke-interface {v6}, Lorg/apache/index/poi/poifs/storage/ListManagedBlock;->getXBATData()[B

    move-result-object v1

    .line 297
    .local v1, "data":[B
    const/4 v5, 0x0

    .line 299
    .local v5, "offset":I
    const/4 v3, 0x0

    .local v3, "k":I
    :goto_1
    if-lt v3, v4, :cond_1

    .line 312
    const/4 v6, 0x0

    aput-object v6, p1, v0

    .line 294
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 301
    :cond_1
    invoke-static {v1, v5}, Lorg/apache/index/poi/util/LittleEndian;->getInt([BI)I

    move-result v2

    .line 303
    .local v2, "entry":I
    const/4 v6, -0x1

    if-ne v2, v6, :cond_2

    .line 305
    iget-object v6, p0, Lorg/apache/index/poi/poifs/storage/BlockAllocationTableReader;->_entries:Lorg/apache/index/poi/util/IntList;

    invoke-virtual {v6}, Lorg/apache/index/poi/util/IntList;->size()I

    move-result v6

    invoke-interface {p2, v6}, Lorg/apache/index/poi/poifs/storage/BlockList;->zap(I)V

    .line 307
    :cond_2
    iget-object v6, p0, Lorg/apache/index/poi/poifs/storage/BlockAllocationTableReader;->_entries:Lorg/apache/index/poi/util/IntList;

    invoke-virtual {v6, v2}, Lorg/apache/index/poi/util/IntList;->add(I)Z

    .line 308
    add-int/lit8 v5, v5, 0x4

    .line 299
    add-int/lit8 v3, v3, 0x1

    goto :goto_1
.end method


# virtual methods
.method fetchBlocks(IILorg/apache/index/poi/poifs/storage/BlockList;)[Lorg/apache/index/poi/poifs/storage/ListManagedBlock;
    .locals 7
    .param p1, "startBlock"    # I
    .param p2, "headerPropertiesStartBlock"    # I
    .param p3, "blockList"    # Lorg/apache/index/poi/poifs/storage/BlockList;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 210
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 211
    .local v0, "blocks":Ljava/util/List;, "Ljava/util/List<Lorg/apache/index/poi/poifs/storage/ListManagedBlock;>;"
    move v1, p1

    .line 212
    .local v1, "currentBlock":I
    const/4 v4, 0x1

    .line 213
    .local v4, "firstPass":Z
    const/4 v2, 0x0

    .line 219
    .local v2, "dataBlock":Lorg/apache/index/poi/poifs/storage/ListManagedBlock;
    :goto_0
    const/4 v5, -0x2

    if-ne v1, v5, :cond_0

    .line 244
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v5

    new-array v5, v5, [Lorg/apache/index/poi/poifs/storage/ListManagedBlock;

    invoke-interface {v0, v5}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [Lorg/apache/index/poi/poifs/storage/ListManagedBlock;

    return-object v5

    .line 222
    :cond_0
    :try_start_0
    invoke-interface {p3, v1}, Lorg/apache/index/poi/poifs/storage/BlockList;->remove(I)Lorg/apache/index/poi/poifs/storage/ListManagedBlock;

    move-result-object v2

    .line 223
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 225
    iget-object v5, p0, Lorg/apache/index/poi/poifs/storage/BlockAllocationTableReader;->_entries:Lorg/apache/index/poi/util/IntList;

    invoke-virtual {v5, v1}, Lorg/apache/index/poi/util/IntList;->get(I)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 226
    const/4 v4, 0x0

    goto :goto_0

    .line 227
    :catch_0
    move-exception v3

    .line 228
    .local v3, "e":Ljava/io/IOException;
    if-ne v1, p2, :cond_1

    .line 230
    sget-object v5, Ljava/lang/System;->err:Ljava/io/PrintStream;

    const-string/jumbo v6, "Warning, header block comes after data blocks in POIFS block listing"

    invoke-virtual {v5, v6}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 231
    const/4 v1, -0x2

    .line 232
    goto :goto_0

    :cond_1
    if-nez v1, :cond_2

    if-eqz v4, :cond_2

    .line 235
    sget-object v5, Ljava/lang/System;->err:Ljava/io/PrintStream;

    const-string/jumbo v6, "Warning, incorrectly terminated empty data blocks in POIFS block listing (should end at -2, ended at 0)"

    invoke-virtual {v5, v6}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 236
    const/4 v1, -0x2

    .line 237
    goto :goto_0

    .line 239
    :cond_2
    throw v3
.end method

.method getNextBlockIndex(I)I
    .locals 3
    .param p1, "index"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 278
    invoke-virtual {p0, p1}, Lorg/apache/index/poi/poifs/storage/BlockAllocationTableReader;->isUsed(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 279
    iget-object v0, p0, Lorg/apache/index/poi/poifs/storage/BlockAllocationTableReader;->_entries:Lorg/apache/index/poi/util/IntList;

    invoke-virtual {v0, p1}, Lorg/apache/index/poi/util/IntList;->get(I)I

    move-result v0

    return v0

    .line 281
    :cond_0
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "index "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " is unused"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method isUsed(I)Z
    .locals 4
    .param p1, "index"    # I

    .prologue
    const/4 v1, 0x0

    .line 259
    :try_start_0
    iget-object v2, p0, Lorg/apache/index/poi/poifs/storage/BlockAllocationTableReader;->_entries:Lorg/apache/index/poi/util/IntList;

    invoke-virtual {v2, p1}, Lorg/apache/index/poi/util/IntList;->get(I)I
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    const/4 v3, -0x1

    if-eq v2, v3, :cond_0

    const/4 v1, 0x1

    .line 262
    :cond_0
    :goto_0
    return v1

    .line 260
    :catch_0
    move-exception v0

    .line 262
    .local v0, "e":Ljava/lang/IndexOutOfBoundsException;
    goto :goto_0
.end method
