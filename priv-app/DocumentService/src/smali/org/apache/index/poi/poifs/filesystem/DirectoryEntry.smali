.class public interface abstract Lorg/apache/index/poi/poifs/filesystem/DirectoryEntry;
.super Ljava/lang/Object;
.source "DirectoryEntry.java"

# interfaces
.implements Ljava/lang/Iterable;
.implements Lorg/apache/index/poi/poifs/filesystem/Entry;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lorg/apache/index/poi/poifs/filesystem/Entry;",
        "Ljava/lang/Iterable",
        "<",
        "Lorg/apache/index/poi/poifs/filesystem/Entry;",
        ">;"
    }
.end annotation


# virtual methods
.method public abstract createDirectory(Ljava/lang/String;)Lorg/apache/index/poi/poifs/filesystem/DirectoryEntry;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract createDocument(Ljava/lang/String;Ljava/io/InputStream;)Lorg/apache/index/poi/poifs/filesystem/DocumentEntry;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract getEntries()Ljava/util/Iterator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Lorg/apache/index/poi/poifs/filesystem/Entry;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getEntry(Ljava/lang/String;)Lorg/apache/index/poi/poifs/filesystem/Entry;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation
.end method

.method public abstract getEntryCount()I
.end method

.method public abstract getStorageClsid()Lorg/apache/index/poi/hpsf/ClassID;
.end method

.method public abstract isEmpty()Z
.end method

.method public abstract setStorageClsid(Lorg/apache/index/poi/hpsf/ClassID;)V
.end method
