.class public Lorg/apache/index/poi/hpsf/TypeWriter;
.super Ljava/lang/Object;
.source "TypeWriter.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static writeToStream(Ljava/io/OutputStream;D)I
    .locals 5
    .param p0, "out"    # Ljava/io/OutputStream;
    .param p1, "n"    # D
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/16 v2, 0x8

    .line 207
    const/16 v1, 0x8

    .line 208
    .local v1, "l":I
    new-array v0, v2, [B

    .line 209
    .local v0, "buffer":[B
    invoke-static {v0, v3, p1, p2}, Lorg/apache/index/poi/util/LittleEndian;->putDouble([BID)V

    .line 210
    invoke-virtual {p0, v0, v3, v2}, Ljava/io/OutputStream;->write([BII)V

    .line 211
    return v2
.end method

.method public static writeToStream(Ljava/io/OutputStream;I)I
    .locals 4
    .param p0, "out"    # Ljava/io/OutputStream;
    .param p1, "n"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x4

    .line 66
    const/4 v1, 0x4

    .line 67
    .local v1, "l":I
    new-array v0, v2, [B

    .line 68
    .local v0, "buffer":[B
    invoke-static {v0, v3, p1}, Lorg/apache/index/poi/util/LittleEndian;->putInt([BII)V

    .line 69
    invoke-virtual {p0, v0, v3, v2}, Ljava/io/OutputStream;->write([BII)V

    .line 70
    return v2
.end method

.method public static writeToStream(Ljava/io/OutputStream;J)I
    .locals 5
    .param p0, "out"    # Ljava/io/OutputStream;
    .param p1, "n"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/16 v2, 0x8

    .line 87
    const/16 v1, 0x8

    .line 88
    .local v1, "l":I
    new-array v0, v2, [B

    .line 89
    .local v0, "buffer":[B
    invoke-static {v0, v3, p1, p2}, Lorg/apache/index/poi/util/LittleEndian;->putLong([BIJ)V

    .line 90
    invoke-virtual {p0, v0, v3, v2}, Ljava/io/OutputStream;->write([BII)V

    .line 91
    return v2
.end method

.method public static writeToStream(Ljava/io/OutputStream;Lorg/apache/index/poi/hpsf/ClassID;)I
    .locals 3
    .param p0, "out"    # Ljava/io/OutputStream;
    .param p1, "n"    # Lorg/apache/index/poi/hpsf/ClassID;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 147
    const/16 v1, 0x10

    new-array v0, v1, [B

    .line 148
    .local v0, "b":[B
    invoke-virtual {p1, v0, v2}, Lorg/apache/index/poi/hpsf/ClassID;->write([BI)V

    .line 149
    array-length v1, v0

    invoke-virtual {p0, v0, v2, v1}, Ljava/io/OutputStream;->write([BII)V

    .line 150
    array-length v1, v0

    return v1
.end method

.method public static writeToStream(Ljava/io/OutputStream;S)I
    .locals 4
    .param p0, "out"    # Ljava/io/OutputStream;
    .param p1, "n"    # S
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x2

    .line 46
    const/4 v1, 0x2

    .line 47
    .local v1, "length":I
    new-array v0, v2, [B

    .line 48
    .local v0, "buffer":[B
    invoke-static {v0, v3, p1}, Lorg/apache/index/poi/util/LittleEndian;->putShort([BIS)V

    .line 49
    invoke-virtual {p0, v0, v3, v2}, Ljava/io/OutputStream;->write([BII)V

    .line 50
    return v2
.end method

.method public static writeToStream(Ljava/io/OutputStream;[Lorg/apache/index/poi/hpsf/Property;I)V
    .locals 7
    .param p0, "out"    # Ljava/io/OutputStream;
    .param p1, "properties"    # [Lorg/apache/index/poi/hpsf/Property;
    .param p2, "codepage"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/apache/index/poi/hpsf/UnsupportedVariantTypeException;
        }
    .end annotation

    .prologue
    .line 172
    if-nez p1, :cond_1

    .line 192
    :cond_0
    return-void

    .line 177
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v4, p1

    if-lt v0, v4, :cond_2

    .line 185
    const/4 v0, 0x0

    :goto_1
    array-length v4, p1

    if-ge v0, v4, :cond_0

    .line 187
    aget-object v1, p1, v0

    .line 188
    .local v1, "p":Lorg/apache/index/poi/hpsf/Property;
    invoke-virtual {v1}, Lorg/apache/index/poi/hpsf/Property;->getType()J

    move-result-wide v2

    .line 189
    .local v2, "type":J
    invoke-static {p0, v2, v3}, Lorg/apache/index/poi/hpsf/TypeWriter;->writeUIntToStream(Ljava/io/OutputStream;J)I

    .line 190
    long-to-int v4, v2

    int-to-long v4, v4

    invoke-virtual {v1}, Lorg/apache/index/poi/hpsf/Property;->getValue()Ljava/lang/Object;

    move-result-object v6

    invoke-static {p0, v4, v5, v6, p2}, Lorg/apache/index/poi/hpsf/VariantSupport;->write(Ljava/io/OutputStream;JLjava/lang/Object;I)I

    .line 185
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 179
    .end local v1    # "p":Lorg/apache/index/poi/hpsf/Property;
    .end local v2    # "type":J
    :cond_2
    aget-object v1, p1, v0

    .line 180
    .restart local v1    # "p":Lorg/apache/index/poi/hpsf/Property;
    invoke-virtual {v1}, Lorg/apache/index/poi/hpsf/Property;->getID()J

    move-result-wide v4

    invoke-static {p0, v4, v5}, Lorg/apache/index/poi/hpsf/TypeWriter;->writeUIntToStream(Ljava/io/OutputStream;J)I

    .line 181
    invoke-virtual {v1}, Lorg/apache/index/poi/hpsf/Property;->getSize()I

    move-result v4

    int-to-long v4, v4

    invoke-static {p0, v4, v5}, Lorg/apache/index/poi/hpsf/TypeWriter;->writeUIntToStream(Ljava/io/OutputStream;J)I

    .line 177
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public static writeUIntToStream(Ljava/io/OutputStream;J)I
    .locals 7
    .param p0, "out"    # Ljava/io/OutputStream;
    .param p1, "n"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide v4, -0x100000000L

    .line 127
    and-long v0, p1, v4

    .line 128
    .local v0, "high":J
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-eqz v2, :cond_0

    cmp-long v2, v0, v4

    if-eqz v2, :cond_0

    .line 129
    new-instance v2, Lorg/apache/index/poi/hpsf/IllegalPropertySetDataException;

    .line 130
    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "Value "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " cannot be represented by 4 bytes."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 129
    invoke-direct {v2, v3}, Lorg/apache/index/poi/hpsf/IllegalPropertySetDataException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 131
    :cond_0
    long-to-int v2, p1

    invoke-static {p0, v2}, Lorg/apache/index/poi/hpsf/TypeWriter;->writeToStream(Ljava/io/OutputStream;I)I

    move-result v2

    return v2
.end method

.method public static writeUShortToStream(Ljava/io/OutputStream;I)V
    .locals 4
    .param p0, "out"    # Ljava/io/OutputStream;
    .param p1, "n"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 107
    const/high16 v1, -0x10000

    and-int v0, p1, v1

    .line 108
    .local v0, "high":I
    if-eqz v0, :cond_0

    .line 109
    new-instance v1, Lorg/apache/index/poi/hpsf/IllegalPropertySetDataException;

    .line 110
    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Value "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " cannot be represented by 2 bytes."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 109
    invoke-direct {v1, v2}, Lorg/apache/index/poi/hpsf/IllegalPropertySetDataException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 111
    :cond_0
    int-to-short v1, p1

    invoke-static {p0, v1}, Lorg/apache/index/poi/hpsf/TypeWriter;->writeToStream(Ljava/io/OutputStream;S)I

    .line 112
    return-void
.end method
