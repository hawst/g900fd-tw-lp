.class public final Lorg/apache/index/poi/hslf/record/EscherTextboxWrapper;
.super Lorg/apache/index/poi/hslf/record/RecordContainer;
.source "EscherTextboxWrapper.java"


# instance fields
.field private _escherRecord:Lorg/apache/index/poi/ddf/EscherTextboxRecord;

.field private _type:J

.field private shapeId:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 56
    invoke-direct {p0}, Lorg/apache/index/poi/hslf/record/RecordContainer;-><init>()V

    .line 57
    new-instance v0, Lorg/apache/index/poi/ddf/EscherTextboxRecord;

    invoke-direct {v0}, Lorg/apache/index/poi/ddf/EscherTextboxRecord;-><init>()V

    iput-object v0, p0, Lorg/apache/index/poi/hslf/record/EscherTextboxWrapper;->_escherRecord:Lorg/apache/index/poi/ddf/EscherTextboxRecord;

    .line 58
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/EscherTextboxWrapper;->_escherRecord:Lorg/apache/index/poi/ddf/EscherTextboxRecord;

    const/16 v1, -0xff3

    invoke-virtual {v0, v1}, Lorg/apache/index/poi/ddf/EscherTextboxRecord;->setRecordId(S)V

    .line 59
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/EscherTextboxWrapper;->_escherRecord:Lorg/apache/index/poi/ddf/EscherTextboxRecord;

    const/16 v1, 0xf

    invoke-virtual {v0, v1}, Lorg/apache/index/poi/ddf/EscherTextboxRecord;->setOptions(S)V

    .line 61
    const/4 v0, 0x0

    new-array v0, v0, [Lorg/apache/index/poi/hslf/record/Record;

    iput-object v0, p0, Lorg/apache/index/poi/hslf/record/EscherTextboxWrapper;->_children:[Lorg/apache/index/poi/hslf/record/Record;

    .line 62
    return-void
.end method

.method public constructor <init>(Lorg/apache/index/poi/ddf/EscherTextboxRecord;)V
    .locals 4
    .param p1, "textbox"    # Lorg/apache/index/poi/ddf/EscherTextboxRecord;

    .prologue
    .line 44
    invoke-direct {p0}, Lorg/apache/index/poi/hslf/record/RecordContainer;-><init>()V

    .line 45
    iput-object p1, p0, Lorg/apache/index/poi/hslf/record/EscherTextboxWrapper;->_escherRecord:Lorg/apache/index/poi/ddf/EscherTextboxRecord;

    .line 46
    iget-object v1, p0, Lorg/apache/index/poi/hslf/record/EscherTextboxWrapper;->_escherRecord:Lorg/apache/index/poi/ddf/EscherTextboxRecord;

    invoke-virtual {v1}, Lorg/apache/index/poi/ddf/EscherTextboxRecord;->getRecordId()S

    move-result v1

    int-to-long v2, v1

    iput-wide v2, p0, Lorg/apache/index/poi/hslf/record/EscherTextboxWrapper;->_type:J

    .line 49
    iget-object v1, p0, Lorg/apache/index/poi/hslf/record/EscherTextboxWrapper;->_escherRecord:Lorg/apache/index/poi/ddf/EscherTextboxRecord;

    invoke-virtual {v1}, Lorg/apache/index/poi/ddf/EscherTextboxRecord;->getData()[B

    move-result-object v0

    .line 50
    .local v0, "data":[B
    const/4 v1, 0x0

    array-length v2, v0

    invoke-static {v0, v1, v2}, Lorg/apache/index/poi/hslf/record/Record;->findChildRecords([BII)[Lorg/apache/index/poi/hslf/record/Record;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/index/poi/hslf/record/EscherTextboxWrapper;->_children:[Lorg/apache/index/poi/hslf/record/Record;

    .line 51
    return-void
.end method


# virtual methods
.method public getEscherRecord()Lorg/apache/index/poi/ddf/EscherTextboxRecord;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/EscherTextboxWrapper;->_escherRecord:Lorg/apache/index/poi/ddf/EscherTextboxRecord;

    return-object v0
.end method

.method public getRecordType()J
    .locals 2

    .prologue
    .line 68
    iget-wide v0, p0, Lorg/apache/index/poi/hslf/record/EscherTextboxWrapper;->_type:J

    return-wide v0
.end method

.method public getShapeId()I
    .locals 1

    .prologue
    .line 74
    iget v0, p0, Lorg/apache/index/poi/hslf/record/EscherTextboxWrapper;->shapeId:I

    return v0
.end method

.method public setShapeId(I)V
    .locals 0
    .param p1, "id"    # I

    .prologue
    .line 81
    iput p1, p0, Lorg/apache/index/poi/hslf/record/EscherTextboxWrapper;->shapeId:I

    .line 82
    return-void
.end method
