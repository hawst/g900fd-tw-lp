.class public final Lorg/apache/index/poi/hwpf/model/CPSplitCalculator;
.super Ljava/lang/Object;
.source "CPSplitCalculator.java"


# instance fields
.field private fib:Lorg/apache/index/poi/hwpf/model/FileInformationBlock;


# direct methods
.method public constructor <init>(Lorg/apache/index/poi/hwpf/model/FileInformationBlock;)V
    .locals 0
    .param p1, "fib"    # Lorg/apache/index/poi/hwpf/model/FileInformationBlock;

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lorg/apache/index/poi/hwpf/model/CPSplitCalculator;->fib:Lorg/apache/index/poi/hwpf/model/FileInformationBlock;

    .line 31
    return-void
.end method


# virtual methods
.method public getCommentsEnd()I
    .locals 2

    .prologue
    .line 91
    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/model/CPSplitCalculator;->getCommentsStart()I

    move-result v0

    .line 92
    iget-object v1, p0, Lorg/apache/index/poi/hwpf/model/CPSplitCalculator;->fib:Lorg/apache/index/poi/hwpf/model/FileInformationBlock;

    invoke-virtual {v1}, Lorg/apache/index/poi/hwpf/model/FileInformationBlock;->getCcpCommentAtn()I

    move-result v1

    .line 91
    add-int/2addr v0, v1

    return v0
.end method

.method public getCommentsStart()I
    .locals 1

    .prologue
    .line 84
    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/model/CPSplitCalculator;->getHeaderStoryEnd()I

    move-result v0

    return v0
.end method

.method public getEndNoteEnd()I
    .locals 2

    .prologue
    .line 107
    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/model/CPSplitCalculator;->getEndNoteStart()I

    move-result v0

    .line 108
    iget-object v1, p0, Lorg/apache/index/poi/hwpf/model/CPSplitCalculator;->fib:Lorg/apache/index/poi/hwpf/model/FileInformationBlock;

    invoke-virtual {v1}, Lorg/apache/index/poi/hwpf/model/FileInformationBlock;->getCcpEdn()I

    move-result v1

    .line 107
    add-int/2addr v0, v1

    return v0
.end method

.method public getEndNoteStart()I
    .locals 1

    .prologue
    .line 100
    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/model/CPSplitCalculator;->getCommentsEnd()I

    move-result v0

    return v0
.end method

.method public getFootnoteEnd()I
    .locals 2

    .prologue
    .line 59
    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/model/CPSplitCalculator;->getFootnoteStart()I

    move-result v0

    .line 60
    iget-object v1, p0, Lorg/apache/index/poi/hwpf/model/CPSplitCalculator;->fib:Lorg/apache/index/poi/hwpf/model/FileInformationBlock;

    invoke-virtual {v1}, Lorg/apache/index/poi/hwpf/model/FileInformationBlock;->getCcpFtn()I

    move-result v1

    .line 59
    add-int/2addr v0, v1

    return v0
.end method

.method public getFootnoteStart()I
    .locals 1

    .prologue
    .line 52
    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/model/CPSplitCalculator;->getMainDocumentEnd()I

    move-result v0

    return v0
.end method

.method public getHeaderStoryEnd()I
    .locals 2

    .prologue
    .line 75
    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/model/CPSplitCalculator;->getHeaderStoryStart()I

    move-result v0

    .line 76
    iget-object v1, p0, Lorg/apache/index/poi/hwpf/model/CPSplitCalculator;->fib:Lorg/apache/index/poi/hwpf/model/FileInformationBlock;

    invoke-virtual {v1}, Lorg/apache/index/poi/hwpf/model/FileInformationBlock;->getCcpHdd()I

    move-result v1

    .line 75
    add-int/2addr v0, v1

    return v0
.end method

.method public getHeaderStoryStart()I
    .locals 1

    .prologue
    .line 68
    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/model/CPSplitCalculator;->getFootnoteEnd()I

    move-result v0

    return v0
.end method

.method public getHeaderTextboxEnd()I
    .locals 2

    .prologue
    .line 139
    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/model/CPSplitCalculator;->getHeaderTextboxStart()I

    move-result v0

    .line 140
    iget-object v1, p0, Lorg/apache/index/poi/hwpf/model/CPSplitCalculator;->fib:Lorg/apache/index/poi/hwpf/model/FileInformationBlock;

    invoke-virtual {v1}, Lorg/apache/index/poi/hwpf/model/FileInformationBlock;->getCcpHdrTxtBx()I

    move-result v1

    .line 139
    add-int/2addr v0, v1

    return v0
.end method

.method public getHeaderTextboxStart()I
    .locals 1

    .prologue
    .line 132
    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/model/CPSplitCalculator;->getMainTextboxEnd()I

    move-result v0

    return v0
.end method

.method public getMainDocumentEnd()I
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/CPSplitCalculator;->fib:Lorg/apache/index/poi/hwpf/model/FileInformationBlock;

    invoke-virtual {v0}, Lorg/apache/index/poi/hwpf/model/FileInformationBlock;->getCcpText()I

    move-result v0

    return v0
.end method

.method public getMainDocumentStart()I
    .locals 1

    .prologue
    .line 37
    const/4 v0, 0x0

    return v0
.end method

.method public getMainTextboxEnd()I
    .locals 2

    .prologue
    .line 123
    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/model/CPSplitCalculator;->getMainTextboxStart()I

    move-result v0

    .line 124
    iget-object v1, p0, Lorg/apache/index/poi/hwpf/model/CPSplitCalculator;->fib:Lorg/apache/index/poi/hwpf/model/FileInformationBlock;

    invoke-virtual {v1}, Lorg/apache/index/poi/hwpf/model/FileInformationBlock;->getCcpTxtBx()I

    move-result v1

    .line 123
    add-int/2addr v0, v1

    return v0
.end method

.method public getMainTextboxStart()I
    .locals 1

    .prologue
    .line 116
    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/model/CPSplitCalculator;->getEndNoteEnd()I

    move-result v0

    return v0
.end method
