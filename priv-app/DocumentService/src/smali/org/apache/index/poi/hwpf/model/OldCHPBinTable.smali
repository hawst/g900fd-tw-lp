.class public final Lorg/apache/index/poi/hwpf/model/OldCHPBinTable;
.super Lorg/apache/index/poi/hwpf/model/CHPBinTable;
.source "OldCHPBinTable.java"


# direct methods
.method public constructor <init>([BIIILorg/apache/index/poi/hwpf/model/TextPieceTable;)V
    .locals 13
    .param p1, "documentStream"    # [B
    .param p2, "offset"    # I
    .param p3, "size"    # I
    .param p4, "fcMin"    # I
    .param p5, "tpt"    # Lorg/apache/index/poi/hwpf/model/TextPieceTable;

    .prologue
    .line 42
    invoke-direct {p0}, Lorg/apache/index/poi/hwpf/model/CHPBinTable;-><init>()V

    .line 45
    new-instance v2, Lorg/apache/index/poi/hwpf/model/PlexOfCps;

    const/4 v11, 0x2

    move/from16 v0, p3

    invoke-direct {v2, p1, p2, v0, v11}, Lorg/apache/index/poi/hwpf/model/PlexOfCps;-><init>([BIII)V

    .line 47
    .local v2, "binTable":Lorg/apache/index/poi/hwpf/model/PlexOfCps;
    invoke-virtual {v2}, Lorg/apache/index/poi/hwpf/model/PlexOfCps;->length()I

    move-result v5

    .line 48
    .local v5, "length":I
    const/4 v9, 0x0

    .local v9, "x":I
    :goto_0
    if-lt v9, v5, :cond_0

    .line 65
    return-void

    .line 50
    :cond_0
    invoke-virtual {v2, v9}, Lorg/apache/index/poi/hwpf/model/PlexOfCps;->getProperty(I)Lorg/apache/index/poi/hwpf/model/GenericPropertyNode;

    move-result-object v6

    .line 52
    .local v6, "node":Lorg/apache/index/poi/hwpf/model/GenericPropertyNode;
    invoke-virtual {v6}, Lorg/apache/index/poi/hwpf/model/GenericPropertyNode;->getBytes()[B

    move-result-object v11

    invoke-static {v11}, Lorg/apache/index/poi/util/LittleEndian;->getShort([B)S

    move-result v7

    .line 53
    .local v7, "pageNum":I
    mul-int/lit16 v8, v7, 0x200

    .line 55
    .local v8, "pageOffset":I
    new-instance v3, Lorg/apache/index/poi/hwpf/model/CHPFormattedDiskPage;

    move/from16 v0, p4

    move-object/from16 v1, p5

    invoke-direct {v3, p1, v8, v0, v1}, Lorg/apache/index/poi/hwpf/model/CHPFormattedDiskPage;-><init>([BIILorg/apache/index/poi/hwpf/model/TextPieceTable;)V

    .line 58
    .local v3, "cfkp":Lorg/apache/index/poi/hwpf/model/CHPFormattedDiskPage;
    invoke-virtual {v3}, Lorg/apache/index/poi/hwpf/model/CHPFormattedDiskPage;->size()I

    move-result v4

    .line 60
    .local v4, "fkpSize":I
    const/4 v10, 0x0

    .local v10, "y":I
    :goto_1
    if-lt v10, v4, :cond_1

    .line 48
    add-int/lit8 v9, v9, 0x1

    goto :goto_0

    .line 62
    :cond_1
    iget-object v11, p0, Lorg/apache/index/poi/hwpf/model/OldCHPBinTable;->_textRuns:Ljava/util/ArrayList;

    invoke-virtual {v3, v10}, Lorg/apache/index/poi/hwpf/model/CHPFormattedDiskPage;->getCHPX(I)Lorg/apache/index/poi/hwpf/model/CHPX;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 60
    add-int/lit8 v10, v10, 0x1

    goto :goto_1
.end method
