.class final Lorg/apache/index/poi/poifs/filesystem/POIFSDocument$SmallBlockStore;
.super Ljava/lang/Object;
.source "POIFSDocument.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/index/poi/poifs/filesystem/POIFSDocument;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "SmallBlockStore"
.end annotation


# instance fields
.field private _smallBlocks:[Lorg/apache/index/poi/poifs/storage/SmallDocumentBlock;


# direct methods
.method constructor <init>(Lorg/apache/index/poi/poifs/common/POIFSBigBlockSize;[Lorg/apache/index/poi/poifs/storage/SmallDocumentBlock;)V
    .locals 1
    .param p1, "bigBlockSize"    # Lorg/apache/index/poi/poifs/common/POIFSBigBlockSize;
    .param p2, "blocks"    # [Lorg/apache/index/poi/poifs/storage/SmallDocumentBlock;

    .prologue
    .line 402
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 403
    invoke-virtual {p2}, [Lorg/apache/index/poi/poifs/storage/SmallDocumentBlock;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lorg/apache/index/poi/poifs/storage/SmallDocumentBlock;

    iput-object v0, p0, Lorg/apache/index/poi/poifs/filesystem/POIFSDocument$SmallBlockStore;->_smallBlocks:[Lorg/apache/index/poi/poifs/storage/SmallDocumentBlock;

    .line 404
    return-void
.end method


# virtual methods
.method getBlocks()[Lorg/apache/index/poi/poifs/storage/SmallDocumentBlock;
    .locals 1

    .prologue
    .line 418
    iget-object v0, p0, Lorg/apache/index/poi/poifs/filesystem/POIFSDocument$SmallBlockStore;->_smallBlocks:[Lorg/apache/index/poi/poifs/storage/SmallDocumentBlock;

    return-object v0
.end method

.method isValid()Z
    .locals 1

    .prologue
    .line 411
    iget-object v0, p0, Lorg/apache/index/poi/poifs/filesystem/POIFSDocument$SmallBlockStore;->_smallBlocks:[Lorg/apache/index/poi/poifs/storage/SmallDocumentBlock;

    array-length v0, v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
