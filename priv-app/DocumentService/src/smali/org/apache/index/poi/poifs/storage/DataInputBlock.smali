.class public final Lorg/apache/index/poi/poifs/storage/DataInputBlock;
.super Ljava/lang/Object;
.source "DataInputBlock.java"


# instance fields
.field private _blockLength:I

.field private _buf:[B

.field private _isDatainStream:Z

.field private _maxIndex:I

.field private _readIndex:I

.field private _startIndex:I

.field private mDFileHandler:Lorg/apache/index/poi/poifs/filesystem/POIFSFileHandler;


# direct methods
.method constructor <init>([BIIIZLorg/apache/index/poi/poifs/filesystem/POIFSFileHandler;)V
    .locals 1
    .param p1, "data"    # [B
    .param p2, "startOffset"    # I
    .param p3, "startIndex"    # I
    .param p4, "blockLength"    # I
    .param p5, "isDatainStream"    # Z
    .param p6, "infileHandler"    # Lorg/apache/index/poi/poifs/filesystem/POIFSFileHandler;

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    iput-object p1, p0, Lorg/apache/index/poi/poifs/storage/DataInputBlock;->_buf:[B

    .line 62
    iput p2, p0, Lorg/apache/index/poi/poifs/storage/DataInputBlock;->_readIndex:I

    .line 64
    iput-object p6, p0, Lorg/apache/index/poi/poifs/storage/DataInputBlock;->mDFileHandler:Lorg/apache/index/poi/poifs/filesystem/POIFSFileHandler;

    .line 66
    iget-object v0, p0, Lorg/apache/index/poi/poifs/storage/DataInputBlock;->_buf:[B

    if-eqz v0, :cond_0

    .line 68
    iget-object v0, p0, Lorg/apache/index/poi/poifs/storage/DataInputBlock;->_buf:[B

    array-length v0, v0

    iput v0, p0, Lorg/apache/index/poi/poifs/storage/DataInputBlock;->_maxIndex:I

    .line 75
    :goto_0
    iput p3, p0, Lorg/apache/index/poi/poifs/storage/DataInputBlock;->_startIndex:I

    .line 76
    iput p4, p0, Lorg/apache/index/poi/poifs/storage/DataInputBlock;->_blockLength:I

    .line 77
    iput-boolean p5, p0, Lorg/apache/index/poi/poifs/storage/DataInputBlock;->_isDatainStream:Z

    .line 79
    return-void

    .line 72
    :cond_0
    iput p4, p0, Lorg/apache/index/poi/poifs/storage/DataInputBlock;->_maxIndex:I

    goto :goto_0
.end method

.method private fillBuffer()V
    .locals 4

    .prologue
    .line 88
    iget-boolean v0, p0, Lorg/apache/index/poi/poifs/storage/DataInputBlock;->_isDatainStream:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/index/poi/poifs/storage/DataInputBlock;->mDFileHandler:Lorg/apache/index/poi/poifs/filesystem/POIFSFileHandler;

    if-eqz v0, :cond_0

    .line 90
    iget v0, p0, Lorg/apache/index/poi/poifs/storage/DataInputBlock;->_blockLength:I

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/apache/index/poi/poifs/storage/DataInputBlock;->_buf:[B

    .line 91
    iget-object v0, p0, Lorg/apache/index/poi/poifs/storage/DataInputBlock;->mDFileHandler:Lorg/apache/index/poi/poifs/filesystem/POIFSFileHandler;

    iget-object v1, p0, Lorg/apache/index/poi/poifs/storage/DataInputBlock;->_buf:[B

    iget v2, p0, Lorg/apache/index/poi/poifs/storage/DataInputBlock;->_startIndex:I

    iget v3, p0, Lorg/apache/index/poi/poifs/storage/DataInputBlock;->_blockLength:I

    invoke-virtual {v0, v1, v2, v3}, Lorg/apache/index/poi/poifs/filesystem/POIFSFileHandler;->getBlockData([BII)I

    .line 93
    :cond_0
    return-void
.end method

.method private fillPrevBuffer(Lorg/apache/index/poi/poifs/storage/DataInputBlock;)V
    .locals 4
    .param p1, "prevBlock"    # Lorg/apache/index/poi/poifs/storage/DataInputBlock;

    .prologue
    .line 105
    iget-boolean v0, p1, Lorg/apache/index/poi/poifs/storage/DataInputBlock;->_isDatainStream:Z

    if-eqz v0, :cond_0

    .line 107
    iget v0, p1, Lorg/apache/index/poi/poifs/storage/DataInputBlock;->_blockLength:I

    new-array v0, v0, [B

    iput-object v0, p1, Lorg/apache/index/poi/poifs/storage/DataInputBlock;->_buf:[B

    .line 108
    iget-object v0, p0, Lorg/apache/index/poi/poifs/storage/DataInputBlock;->mDFileHandler:Lorg/apache/index/poi/poifs/filesystem/POIFSFileHandler;

    iget-object v1, p1, Lorg/apache/index/poi/poifs/storage/DataInputBlock;->_buf:[B

    iget v2, p1, Lorg/apache/index/poi/poifs/storage/DataInputBlock;->_startIndex:I

    iget v3, p1, Lorg/apache/index/poi/poifs/storage/DataInputBlock;->_blockLength:I

    invoke-virtual {v0, v1, v2, v3}, Lorg/apache/index/poi/poifs/filesystem/POIFSFileHandler;->getBlockData([BII)I

    .line 110
    :cond_0
    return-void
.end method

.method private freeBuffer()V
    .locals 1

    .prologue
    .line 97
    iget-boolean v0, p0, Lorg/apache/index/poi/poifs/storage/DataInputBlock;->_isDatainStream:Z

    if-eqz v0, :cond_0

    .line 99
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/index/poi/poifs/storage/DataInputBlock;->_buf:[B

    .line 101
    :cond_0
    return-void
.end method

.method private freePrevBuffer(Lorg/apache/index/poi/poifs/storage/DataInputBlock;)V
    .locals 1
    .param p1, "prevBlock"    # Lorg/apache/index/poi/poifs/storage/DataInputBlock;

    .prologue
    .line 114
    iget-boolean v0, p1, Lorg/apache/index/poi/poifs/storage/DataInputBlock;->_isDatainStream:Z

    if-eqz v0, :cond_0

    .line 116
    const/4 v0, 0x0

    iput-object v0, p1, Lorg/apache/index/poi/poifs/storage/DataInputBlock;->_buf:[B

    .line 118
    :cond_0
    return-void
.end method

.method private readSpanning(Lorg/apache/index/poi/poifs/storage/DataInputBlock;I[B)V
    .locals 4
    .param p1, "prevBlock"    # Lorg/apache/index/poi/poifs/storage/DataInputBlock;
    .param p2, "prevBlockAvailable"    # I
    .param p3, "buf"    # [B

    .prologue
    const/4 v3, 0x0

    .line 284
    iget-object v1, p1, Lorg/apache/index/poi/poifs/storage/DataInputBlock;->_buf:[B

    iget v2, p1, Lorg/apache/index/poi/poifs/storage/DataInputBlock;->_readIndex:I

    invoke-static {v1, v2, p3, v3, p2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 285
    array-length v1, p3

    sub-int v0, v1, p2

    .line 286
    .local v0, "secondReadLen":I
    iget-object v1, p0, Lorg/apache/index/poi/poifs/storage/DataInputBlock;->_buf:[B

    invoke-static {v1, v3, p3, p2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 287
    iput v0, p0, Lorg/apache/index/poi/poifs/storage/DataInputBlock;->_readIndex:I

    .line 288
    return-void
.end method


# virtual methods
.method public available()I
    .locals 2

    .prologue
    .line 81
    iget v0, p0, Lorg/apache/index/poi/poifs/storage/DataInputBlock;->_maxIndex:I

    iget v1, p0, Lorg/apache/index/poi/poifs/storage/DataInputBlock;->_readIndex:I

    sub-int/2addr v0, v1

    return v0
.end method

.method public readFully([BII)V
    .locals 2
    .param p1, "buf"    # [B
    .param p2, "off"    # I
    .param p3, "len"    # I

    .prologue
    .line 295
    invoke-direct {p0}, Lorg/apache/index/poi/poifs/storage/DataInputBlock;->fillBuffer()V

    .line 297
    iget-object v0, p0, Lorg/apache/index/poi/poifs/storage/DataInputBlock;->_buf:[B

    iget v1, p0, Lorg/apache/index/poi/poifs/storage/DataInputBlock;->_readIndex:I

    invoke-static {v0, v1, p1, p2, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 298
    iget v0, p0, Lorg/apache/index/poi/poifs/storage/DataInputBlock;->_readIndex:I

    add-int/2addr v0, p3

    iput v0, p0, Lorg/apache/index/poi/poifs/storage/DataInputBlock;->_readIndex:I

    .line 300
    invoke-direct {p0}, Lorg/apache/index/poi/poifs/storage/DataInputBlock;->freeBuffer()V

    .line 302
    return-void
.end method

.method public readIntLE()I
    .locals 9

    .prologue
    .line 171
    iget v4, p0, Lorg/apache/index/poi/poifs/storage/DataInputBlock;->_readIndex:I

    .line 172
    .local v4, "i":I
    invoke-direct {p0}, Lorg/apache/index/poi/poifs/storage/DataInputBlock;->fillBuffer()V

    .line 173
    iget-object v7, p0, Lorg/apache/index/poi/poifs/storage/DataInputBlock;->_buf:[B

    add-int/lit8 v5, v4, 0x1

    .end local v4    # "i":I
    .local v5, "i":I
    aget-byte v7, v7, v4

    and-int/lit16 v0, v7, 0xff

    .line 174
    .local v0, "b0":I
    iget-object v7, p0, Lorg/apache/index/poi/poifs/storage/DataInputBlock;->_buf:[B

    add-int/lit8 v4, v5, 0x1

    .end local v5    # "i":I
    .restart local v4    # "i":I
    aget-byte v7, v7, v5

    and-int/lit16 v1, v7, 0xff

    .line 175
    .local v1, "b1":I
    iget-object v7, p0, Lorg/apache/index/poi/poifs/storage/DataInputBlock;->_buf:[B

    add-int/lit8 v5, v4, 0x1

    .end local v4    # "i":I
    .restart local v5    # "i":I
    aget-byte v7, v7, v4

    and-int/lit16 v2, v7, 0xff

    .line 176
    .local v2, "b2":I
    iget-object v7, p0, Lorg/apache/index/poi/poifs/storage/DataInputBlock;->_buf:[B

    add-int/lit8 v4, v5, 0x1

    .end local v5    # "i":I
    .restart local v4    # "i":I
    aget-byte v7, v7, v5

    and-int/lit16 v3, v7, 0xff

    .line 177
    .local v3, "b3":I
    iput v4, p0, Lorg/apache/index/poi/poifs/storage/DataInputBlock;->_readIndex:I

    .line 179
    shl-int/lit8 v7, v3, 0x18

    shl-int/lit8 v8, v2, 0x10

    add-int/2addr v7, v8

    shl-int/lit8 v8, v1, 0x8

    add-int/2addr v7, v8

    shl-int/lit8 v8, v0, 0x0

    add-int v6, v7, v8

    .line 181
    .local v6, "retValue":I
    invoke-direct {p0}, Lorg/apache/index/poi/poifs/storage/DataInputBlock;->freeBuffer()V

    .line 183
    return v6
.end method

.method public readIntLE(Lorg/apache/index/poi/poifs/storage/DataInputBlock;I)I
    .locals 8
    .param p1, "prevBlock"    # Lorg/apache/index/poi/poifs/storage/DataInputBlock;
    .param p2, "prevBlockAvailable"    # I

    .prologue
    .line 190
    const/4 v6, 0x4

    new-array v4, v6, [B

    .line 192
    .local v4, "buf":[B
    invoke-direct {p0, p1}, Lorg/apache/index/poi/poifs/storage/DataInputBlock;->fillPrevBuffer(Lorg/apache/index/poi/poifs/storage/DataInputBlock;)V

    .line 193
    invoke-direct {p0}, Lorg/apache/index/poi/poifs/storage/DataInputBlock;->fillBuffer()V

    .line 195
    invoke-direct {p0, p1, p2, v4}, Lorg/apache/index/poi/poifs/storage/DataInputBlock;->readSpanning(Lorg/apache/index/poi/poifs/storage/DataInputBlock;I[B)V

    .line 196
    const/4 v6, 0x0

    aget-byte v6, v4, v6

    and-int/lit16 v0, v6, 0xff

    .line 197
    .local v0, "b0":I
    const/4 v6, 0x1

    aget-byte v6, v4, v6

    and-int/lit16 v1, v6, 0xff

    .line 198
    .local v1, "b1":I
    const/4 v6, 0x2

    aget-byte v6, v4, v6

    and-int/lit16 v2, v6, 0xff

    .line 199
    .local v2, "b2":I
    const/4 v6, 0x3

    aget-byte v6, v4, v6

    and-int/lit16 v3, v6, 0xff

    .line 201
    .local v3, "b3":I
    shl-int/lit8 v6, v3, 0x18

    shl-int/lit8 v7, v2, 0x10

    add-int/2addr v6, v7

    shl-int/lit8 v7, v1, 0x8

    add-int/2addr v6, v7

    shl-int/lit8 v7, v0, 0x0

    add-int v5, v6, v7

    .line 203
    .local v5, "retValue":I
    invoke-direct {p0}, Lorg/apache/index/poi/poifs/storage/DataInputBlock;->freeBuffer()V

    .line 204
    invoke-direct {p0, p1}, Lorg/apache/index/poi/poifs/storage/DataInputBlock;->freePrevBuffer(Lorg/apache/index/poi/poifs/storage/DataInputBlock;)V

    .line 206
    return v5
.end method

.method public readLongLE()J
    .locals 19

    .prologue
    .line 213
    move-object/from16 v0, p0

    iget v10, v0, Lorg/apache/index/poi/poifs/storage/DataInputBlock;->_readIndex:I

    .line 215
    .local v10, "i":I
    invoke-direct/range {p0 .. p0}, Lorg/apache/index/poi/poifs/storage/DataInputBlock;->fillBuffer()V

    .line 217
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/index/poi/poifs/storage/DataInputBlock;->_buf:[B

    add-int/lit8 v11, v10, 0x1

    .end local v10    # "i":I
    .local v11, "i":I
    aget-byte v14, v14, v10

    and-int/lit16 v2, v14, 0xff

    .line 218
    .local v2, "b0":I
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/index/poi/poifs/storage/DataInputBlock;->_buf:[B

    add-int/lit8 v10, v11, 0x1

    .end local v11    # "i":I
    .restart local v10    # "i":I
    aget-byte v14, v14, v11

    and-int/lit16 v3, v14, 0xff

    .line 219
    .local v3, "b1":I
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/index/poi/poifs/storage/DataInputBlock;->_buf:[B

    add-int/lit8 v11, v10, 0x1

    .end local v10    # "i":I
    .restart local v11    # "i":I
    aget-byte v14, v14, v10

    and-int/lit16 v4, v14, 0xff

    .line 220
    .local v4, "b2":I
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/index/poi/poifs/storage/DataInputBlock;->_buf:[B

    add-int/lit8 v10, v11, 0x1

    .end local v11    # "i":I
    .restart local v10    # "i":I
    aget-byte v14, v14, v11

    and-int/lit16 v5, v14, 0xff

    .line 221
    .local v5, "b3":I
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/index/poi/poifs/storage/DataInputBlock;->_buf:[B

    add-int/lit8 v11, v10, 0x1

    .end local v10    # "i":I
    .restart local v11    # "i":I
    aget-byte v14, v14, v10

    and-int/lit16 v6, v14, 0xff

    .line 222
    .local v6, "b4":I
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/index/poi/poifs/storage/DataInputBlock;->_buf:[B

    add-int/lit8 v10, v11, 0x1

    .end local v11    # "i":I
    .restart local v10    # "i":I
    aget-byte v14, v14, v11

    and-int/lit16 v7, v14, 0xff

    .line 223
    .local v7, "b5":I
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/index/poi/poifs/storage/DataInputBlock;->_buf:[B

    add-int/lit8 v11, v10, 0x1

    .end local v10    # "i":I
    .restart local v11    # "i":I
    aget-byte v14, v14, v10

    and-int/lit16 v8, v14, 0xff

    .line 224
    .local v8, "b6":I
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/index/poi/poifs/storage/DataInputBlock;->_buf:[B

    add-int/lit8 v10, v11, 0x1

    .end local v11    # "i":I
    .restart local v10    # "i":I
    aget-byte v14, v14, v11

    and-int/lit16 v9, v14, 0xff

    .line 225
    .local v9, "b7":I
    move-object/from16 v0, p0

    iput v10, v0, Lorg/apache/index/poi/poifs/storage/DataInputBlock;->_readIndex:I

    .line 227
    int-to-long v14, v9

    const/16 v16, 0x38

    shl-long v14, v14, v16

    .line 228
    int-to-long v0, v8

    move-wide/from16 v16, v0

    const/16 v18, 0x30

    shl-long v16, v16, v18

    .line 227
    add-long v14, v14, v16

    .line 229
    int-to-long v0, v7

    move-wide/from16 v16, v0

    const/16 v18, 0x28

    shl-long v16, v16, v18

    .line 227
    add-long v14, v14, v16

    .line 230
    int-to-long v0, v6

    move-wide/from16 v16, v0

    const/16 v18, 0x20

    shl-long v16, v16, v18

    .line 227
    add-long v14, v14, v16

    .line 231
    int-to-long v0, v5

    move-wide/from16 v16, v0

    const/16 v18, 0x18

    shl-long v16, v16, v18

    .line 227
    add-long v14, v14, v16

    .line 232
    shl-int/lit8 v16, v4, 0x10

    move/from16 v0, v16

    int-to-long v0, v0

    move-wide/from16 v16, v0

    .line 227
    add-long v14, v14, v16

    .line 233
    shl-int/lit8 v16, v3, 0x8

    move/from16 v0, v16

    int-to-long v0, v0

    move-wide/from16 v16, v0

    .line 227
    add-long v14, v14, v16

    .line 234
    shl-int/lit8 v16, v2, 0x0

    move/from16 v0, v16

    int-to-long v0, v0

    move-wide/from16 v16, v0

    .line 227
    add-long v12, v14, v16

    .line 236
    .local v12, "retValue":J
    invoke-direct/range {p0 .. p0}, Lorg/apache/index/poi/poifs/storage/DataInputBlock;->freeBuffer()V

    .line 238
    return-wide v12
.end method

.method public readLongLE(Lorg/apache/index/poi/poifs/storage/DataInputBlock;I)J
    .locals 20
    .param p1, "prevBlock"    # Lorg/apache/index/poi/poifs/storage/DataInputBlock;
    .param p2, "prevBlockAvailable"    # I

    .prologue
    .line 245
    const/16 v13, 0x8

    new-array v12, v13, [B

    .line 248
    .local v12, "buf":[B
    invoke-direct/range {p0 .. p1}, Lorg/apache/index/poi/poifs/storage/DataInputBlock;->fillPrevBuffer(Lorg/apache/index/poi/poifs/storage/DataInputBlock;)V

    .line 249
    invoke-direct/range {p0 .. p0}, Lorg/apache/index/poi/poifs/storage/DataInputBlock;->fillBuffer()V

    .line 251
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p2

    invoke-direct {v0, v1, v2, v12}, Lorg/apache/index/poi/poifs/storage/DataInputBlock;->readSpanning(Lorg/apache/index/poi/poifs/storage/DataInputBlock;I[B)V

    .line 253
    const/4 v13, 0x0

    aget-byte v13, v12, v13

    and-int/lit16 v4, v13, 0xff

    .line 254
    .local v4, "b0":I
    const/4 v13, 0x1

    aget-byte v13, v12, v13

    and-int/lit16 v5, v13, 0xff

    .line 255
    .local v5, "b1":I
    const/4 v13, 0x2

    aget-byte v13, v12, v13

    and-int/lit16 v6, v13, 0xff

    .line 256
    .local v6, "b2":I
    const/4 v13, 0x3

    aget-byte v13, v12, v13

    and-int/lit16 v7, v13, 0xff

    .line 257
    .local v7, "b3":I
    const/4 v13, 0x4

    aget-byte v13, v12, v13

    and-int/lit16 v8, v13, 0xff

    .line 258
    .local v8, "b4":I
    const/4 v13, 0x5

    aget-byte v13, v12, v13

    and-int/lit16 v9, v13, 0xff

    .line 259
    .local v9, "b5":I
    const/4 v13, 0x6

    aget-byte v13, v12, v13

    and-int/lit16 v10, v13, 0xff

    .line 260
    .local v10, "b6":I
    const/4 v13, 0x7

    aget-byte v13, v12, v13

    and-int/lit16 v11, v13, 0xff

    .line 262
    .local v11, "b7":I
    int-to-long v0, v11

    move-wide/from16 v16, v0

    const/16 v13, 0x38

    shl-long v16, v16, v13

    .line 263
    int-to-long v0, v10

    move-wide/from16 v18, v0

    const/16 v13, 0x30

    shl-long v18, v18, v13

    .line 262
    add-long v16, v16, v18

    .line 264
    int-to-long v0, v9

    move-wide/from16 v18, v0

    const/16 v13, 0x28

    shl-long v18, v18, v13

    .line 262
    add-long v16, v16, v18

    .line 265
    int-to-long v0, v8

    move-wide/from16 v18, v0

    const/16 v13, 0x20

    shl-long v18, v18, v13

    .line 262
    add-long v16, v16, v18

    .line 266
    int-to-long v0, v7

    move-wide/from16 v18, v0

    const/16 v13, 0x18

    shl-long v18, v18, v13

    .line 262
    add-long v16, v16, v18

    .line 267
    shl-int/lit8 v13, v6, 0x10

    int-to-long v0, v13

    move-wide/from16 v18, v0

    .line 262
    add-long v16, v16, v18

    .line 268
    shl-int/lit8 v13, v5, 0x8

    int-to-long v0, v13

    move-wide/from16 v18, v0

    .line 262
    add-long v16, v16, v18

    .line 269
    shl-int/lit8 v13, v4, 0x0

    int-to-long v0, v13

    move-wide/from16 v18, v0

    .line 262
    add-long v14, v16, v18

    .line 271
    .local v14, "retValue":J
    invoke-direct/range {p0 .. p0}, Lorg/apache/index/poi/poifs/storage/DataInputBlock;->freeBuffer()V

    .line 272
    invoke-direct/range {p0 .. p1}, Lorg/apache/index/poi/poifs/storage/DataInputBlock;->freePrevBuffer(Lorg/apache/index/poi/poifs/storage/DataInputBlock;)V

    .line 274
    return-wide v14
.end method

.method public readUByte()I
    .locals 4

    .prologue
    .line 122
    invoke-direct {p0}, Lorg/apache/index/poi/poifs/storage/DataInputBlock;->fillBuffer()V

    .line 123
    iget-object v1, p0, Lorg/apache/index/poi/poifs/storage/DataInputBlock;->_buf:[B

    iget v2, p0, Lorg/apache/index/poi/poifs/storage/DataInputBlock;->_readIndex:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lorg/apache/index/poi/poifs/storage/DataInputBlock;->_readIndex:I

    aget-byte v1, v1, v2

    and-int/lit16 v0, v1, 0xff

    .line 124
    .local v0, "retValue":I
    invoke-direct {p0}, Lorg/apache/index/poi/poifs/storage/DataInputBlock;->freeBuffer()V

    .line 125
    return v0
.end method

.method public readUShortLE()I
    .locals 7

    .prologue
    .line 132
    iget v2, p0, Lorg/apache/index/poi/poifs/storage/DataInputBlock;->_readIndex:I

    .line 134
    .local v2, "i":I
    invoke-direct {p0}, Lorg/apache/index/poi/poifs/storage/DataInputBlock;->fillBuffer()V

    .line 136
    iget-object v5, p0, Lorg/apache/index/poi/poifs/storage/DataInputBlock;->_buf:[B

    add-int/lit8 v3, v2, 0x1

    .end local v2    # "i":I
    .local v3, "i":I
    aget-byte v5, v5, v2

    and-int/lit16 v0, v5, 0xff

    .line 137
    .local v0, "b0":I
    iget-object v5, p0, Lorg/apache/index/poi/poifs/storage/DataInputBlock;->_buf:[B

    add-int/lit8 v2, v3, 0x1

    .end local v3    # "i":I
    .restart local v2    # "i":I
    aget-byte v5, v5, v3

    and-int/lit16 v1, v5, 0xff

    .line 138
    .local v1, "b1":I
    iput v2, p0, Lorg/apache/index/poi/poifs/storage/DataInputBlock;->_readIndex:I

    .line 140
    shl-int/lit8 v5, v1, 0x8

    shl-int/lit8 v6, v0, 0x0

    add-int v4, v5, v6

    .line 142
    .local v4, "retValue":I
    invoke-direct {p0}, Lorg/apache/index/poi/poifs/storage/DataInputBlock;->freeBuffer()V

    .line 144
    return v4
.end method

.method public readUShortLE(Lorg/apache/index/poi/poifs/storage/DataInputBlock;)I
    .locals 8
    .param p1, "prevBlock"    # Lorg/apache/index/poi/poifs/storage/DataInputBlock;

    .prologue
    .line 153
    invoke-direct {p0, p1}, Lorg/apache/index/poi/poifs/storage/DataInputBlock;->fillPrevBuffer(Lorg/apache/index/poi/poifs/storage/DataInputBlock;)V

    .line 154
    invoke-direct {p0}, Lorg/apache/index/poi/poifs/storage/DataInputBlock;->fillBuffer()V

    .line 156
    iget-object v5, p1, Lorg/apache/index/poi/poifs/storage/DataInputBlock;->_buf:[B

    array-length v5, v5

    add-int/lit8 v2, v5, -0x1

    .line 158
    .local v2, "i":I
    iget-object v5, p1, Lorg/apache/index/poi/poifs/storage/DataInputBlock;->_buf:[B

    add-int/lit8 v3, v2, 0x1

    .end local v2    # "i":I
    .local v3, "i":I
    aget-byte v5, v5, v2

    and-int/lit16 v0, v5, 0xff

    .line 159
    .local v0, "b0":I
    iget-object v5, p0, Lorg/apache/index/poi/poifs/storage/DataInputBlock;->_buf:[B

    iget v6, p0, Lorg/apache/index/poi/poifs/storage/DataInputBlock;->_readIndex:I

    add-int/lit8 v7, v6, 0x1

    iput v7, p0, Lorg/apache/index/poi/poifs/storage/DataInputBlock;->_readIndex:I

    aget-byte v5, v5, v6

    and-int/lit16 v1, v5, 0xff

    .line 160
    .local v1, "b1":I
    shl-int/lit8 v5, v1, 0x8

    shl-int/lit8 v6, v0, 0x0

    add-int v4, v5, v6

    .line 162
    .local v4, "retValue":I
    invoke-direct {p0}, Lorg/apache/index/poi/poifs/storage/DataInputBlock;->freeBuffer()V

    .line 163
    invoke-direct {p0, p1}, Lorg/apache/index/poi/poifs/storage/DataInputBlock;->freePrevBuffer(Lorg/apache/index/poi/poifs/storage/DataInputBlock;)V

    .line 164
    return v4
.end method
