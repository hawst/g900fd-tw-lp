.class public final Lorg/apache/index/poi/hwpf/usermodel/DateAndTime;
.super Ljava/lang/Object;
.source "DateAndTime.java"

# interfaces
.implements Ljava/lang/Cloneable;


# static fields
.field public static final SIZE:I = 0x4

.field private static final _dom:Lorg/apache/index/poi/util/BitField;

.field private static final _hours:Lorg/apache/index/poi/util/BitField;

.field private static final _minutes:Lorg/apache/index/poi/util/BitField;

.field private static final _months:Lorg/apache/index/poi/util/BitField;

.field private static final _years:Lorg/apache/index/poi/util/BitField;


# instance fields
.field private _info:S

.field private _info2:S


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 37
    const/16 v0, 0x3f

    invoke-static {v0}, Lorg/apache/index/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/index/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/index/poi/hwpf/usermodel/DateAndTime;->_minutes:Lorg/apache/index/poi/util/BitField;

    .line 38
    const/16 v0, 0x7c0

    invoke-static {v0}, Lorg/apache/index/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/index/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/index/poi/hwpf/usermodel/DateAndTime;->_hours:Lorg/apache/index/poi/util/BitField;

    .line 39
    const v0, 0xf800

    invoke-static {v0}, Lorg/apache/index/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/index/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/index/poi/hwpf/usermodel/DateAndTime;->_dom:Lorg/apache/index/poi/util/BitField;

    .line 41
    const/16 v0, 0xf

    invoke-static {v0}, Lorg/apache/index/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/index/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/index/poi/hwpf/usermodel/DateAndTime;->_months:Lorg/apache/index/poi/util/BitField;

    .line 42
    const/16 v0, 0x1ff0

    invoke-static {v0}, Lorg/apache/index/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/index/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/index/poi/hwpf/usermodel/DateAndTime;->_years:Lorg/apache/index/poi/util/BitField;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    return-void
.end method

.method public constructor <init>([BI)V
    .locals 1
    .param p1, "buf"    # [B
    .param p2, "offset"    # I

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    invoke-static {p1, p2}, Lorg/apache/index/poi/util/LittleEndian;->getShort([BI)S

    move-result v0

    iput-short v0, p0, Lorg/apache/index/poi/hwpf/usermodel/DateAndTime;->_info:S

    .line 54
    add-int/lit8 v0, p2, 0x2

    invoke-static {p1, v0}, Lorg/apache/index/poi/util/LittleEndian;->getShort([BI)S

    move-result v0

    iput-short v0, p0, Lorg/apache/index/poi/hwpf/usermodel/DateAndTime;->_info2:S

    .line 55
    return-void
.end method


# virtual methods
.method public clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 96
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 80
    if-nez p1, :cond_1

    .line 89
    :cond_0
    :goto_0
    return v1

    .line 85
    :cond_1
    instance-of v2, p1, Lorg/apache/index/poi/hwpf/usermodel/DateAndTime;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 86
    check-cast v0, Lorg/apache/index/poi/hwpf/usermodel/DateAndTime;

    .line 87
    .local v0, "dttm":Lorg/apache/index/poi/hwpf/usermodel/DateAndTime;
    iget-short v2, p0, Lorg/apache/index/poi/hwpf/usermodel/DateAndTime;->_info:S

    iget-short v3, v0, Lorg/apache/index/poi/hwpf/usermodel/DateAndTime;->_info:S

    if-ne v2, v3, :cond_0

    iget-short v2, p0, Lorg/apache/index/poi/hwpf/usermodel/DateAndTime;->_info2:S

    iget-short v3, v0, Lorg/apache/index/poi/hwpf/usermodel/DateAndTime;->_info2:S

    if-ne v2, v3, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method public getDate()Ljava/util/Calendar;
    .locals 8

    .prologue
    const/4 v6, 0x0

    .line 59
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 61
    .local v0, "cal":Ljava/util/Calendar;
    sget-object v1, Lorg/apache/index/poi/hwpf/usermodel/DateAndTime;->_years:Lorg/apache/index/poi/util/BitField;

    iget-short v2, p0, Lorg/apache/index/poi/hwpf/usermodel/DateAndTime;->_info2:S

    invoke-virtual {v1, v2}, Lorg/apache/index/poi/util/BitField;->getValue(I)I

    move-result v1

    add-int/lit16 v1, v1, 0x76c

    .line 62
    sget-object v2, Lorg/apache/index/poi/hwpf/usermodel/DateAndTime;->_months:Lorg/apache/index/poi/util/BitField;

    iget-short v3, p0, Lorg/apache/index/poi/hwpf/usermodel/DateAndTime;->_info2:S

    invoke-virtual {v2, v3}, Lorg/apache/index/poi/util/BitField;->getValue(I)I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    .line 63
    sget-object v3, Lorg/apache/index/poi/hwpf/usermodel/DateAndTime;->_dom:Lorg/apache/index/poi/util/BitField;

    iget-short v4, p0, Lorg/apache/index/poi/hwpf/usermodel/DateAndTime;->_info:S

    invoke-virtual {v3, v4}, Lorg/apache/index/poi/util/BitField;->getValue(I)I

    move-result v3

    .line 64
    sget-object v4, Lorg/apache/index/poi/hwpf/usermodel/DateAndTime;->_hours:Lorg/apache/index/poi/util/BitField;

    iget-short v5, p0, Lorg/apache/index/poi/hwpf/usermodel/DateAndTime;->_info:S

    invoke-virtual {v4, v5}, Lorg/apache/index/poi/util/BitField;->getValue(I)I

    move-result v4

    .line 65
    sget-object v5, Lorg/apache/index/poi/hwpf/usermodel/DateAndTime;->_minutes:Lorg/apache/index/poi/util/BitField;

    iget-short v7, p0, Lorg/apache/index/poi/hwpf/usermodel/DateAndTime;->_info:S

    invoke-virtual {v5, v7}, Lorg/apache/index/poi/util/BitField;->getValue(I)I

    move-result v5

    .line 60
    invoke-virtual/range {v0 .. v6}, Ljava/util/Calendar;->set(IIIIII)V

    .line 68
    const/16 v1, 0xe

    invoke-virtual {v0, v1, v6}, Ljava/util/Calendar;->set(II)V

    .line 69
    return-object v0
.end method

.method public serialize([BI)V
    .locals 2
    .param p1, "buf"    # [B
    .param p2, "offset"    # I

    .prologue
    .line 74
    iget-short v0, p0, Lorg/apache/index/poi/hwpf/usermodel/DateAndTime;->_info:S

    invoke-static {p1, p2, v0}, Lorg/apache/index/poi/util/LittleEndian;->putShort([BIS)V

    .line 75
    add-int/lit8 v0, p2, 0x2

    iget-short v1, p0, Lorg/apache/index/poi/hwpf/usermodel/DateAndTime;->_info2:S

    invoke-static {p1, v0, v1}, Lorg/apache/index/poi/util/LittleEndian;->putShort([BIS)V

    .line 76
    return-void
.end method
