.class public final Lorg/apache/index/poi/hslf/record/Notes;
.super Lorg/apache/index/poi/hslf/record/SheetContainer;
.source "Notes.java"


# static fields
.field private static _type:J


# instance fields
.field private _header:[B

.field private notesAtom:Lorg/apache/index/poi/hslf/record/NotesAtom;

.field private ppDrawing:Lorg/apache/index/poi/hslf/record/PPDrawing;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 31
    const-wide/16 v0, 0x3f0

    sput-wide v0, Lorg/apache/index/poi/hslf/record/Notes;->_type:J

    return-void
.end method

.method protected constructor <init>([BII)V
    .locals 4
    .param p1, "source"    # [B
    .param p2, "start"    # I
    .param p3, "len"    # I

    .prologue
    const/16 v3, 0x8

    .line 51
    invoke-direct {p0}, Lorg/apache/index/poi/hslf/record/SheetContainer;-><init>()V

    .line 53
    new-array v1, v3, [B

    iput-object v1, p0, Lorg/apache/index/poi/hslf/record/Notes;->_header:[B

    .line 54
    iget-object v1, p0, Lorg/apache/index/poi/hslf/record/Notes;->_header:[B

    const/4 v2, 0x0

    invoke-static {p1, p2, v1, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 57
    add-int/lit8 v1, p2, 0x8

    add-int/lit8 v2, p3, -0x8

    invoke-static {p1, v1, v2}, Lorg/apache/index/poi/hslf/record/Record;->findChildRecords([BII)[Lorg/apache/index/poi/hslf/record/Record;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/index/poi/hslf/record/Notes;->_children:[Lorg/apache/index/poi/hslf/record/Record;

    .line 60
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lorg/apache/index/poi/hslf/record/Notes;->_children:[Lorg/apache/index/poi/hslf/record/Record;

    array-length v1, v1

    if-lt v0, v1, :cond_0

    .line 69
    return-void

    .line 61
    :cond_0
    iget-object v1, p0, Lorg/apache/index/poi/hslf/record/Notes;->_children:[Lorg/apache/index/poi/hslf/record/Record;

    aget-object v1, v1, v0

    instance-of v1, v1, Lorg/apache/index/poi/hslf/record/NotesAtom;

    if-eqz v1, :cond_1

    .line 62
    iget-object v1, p0, Lorg/apache/index/poi/hslf/record/Notes;->_children:[Lorg/apache/index/poi/hslf/record/Record;

    aget-object v1, v1, v0

    check-cast v1, Lorg/apache/index/poi/hslf/record/NotesAtom;

    iput-object v1, p0, Lorg/apache/index/poi/hslf/record/Notes;->notesAtom:Lorg/apache/index/poi/hslf/record/NotesAtom;

    .line 65
    :cond_1
    iget-object v1, p0, Lorg/apache/index/poi/hslf/record/Notes;->_children:[Lorg/apache/index/poi/hslf/record/Record;

    aget-object v1, v1, v0

    instance-of v1, v1, Lorg/apache/index/poi/hslf/record/PPDrawing;

    if-eqz v1, :cond_2

    .line 66
    iget-object v1, p0, Lorg/apache/index/poi/hslf/record/Notes;->_children:[Lorg/apache/index/poi/hslf/record/Record;

    aget-object v1, v1, v0

    check-cast v1, Lorg/apache/index/poi/hslf/record/PPDrawing;

    iput-object v1, p0, Lorg/apache/index/poi/hslf/record/Notes;->ppDrawing:Lorg/apache/index/poi/hslf/record/PPDrawing;

    .line 60
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public getNotesAtom()Lorg/apache/index/poi/hslf/record/NotesAtom;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/Notes;->notesAtom:Lorg/apache/index/poi/hslf/record/NotesAtom;

    return-object v0
.end method

.method public getPPDrawing()Lorg/apache/index/poi/hslf/record/PPDrawing;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/Notes;->ppDrawing:Lorg/apache/index/poi/hslf/record/PPDrawing;

    return-object v0
.end method

.method public getRecordType()J
    .locals 2

    .prologue
    .line 75
    sget-wide v0, Lorg/apache/index/poi/hslf/record/Notes;->_type:J

    return-wide v0
.end method
