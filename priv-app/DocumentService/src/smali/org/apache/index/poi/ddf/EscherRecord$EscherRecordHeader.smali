.class Lorg/apache/index/poi/ddf/EscherRecord$EscherRecordHeader;
.super Ljava/lang/Object;
.source "EscherRecord.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/index/poi/ddf/EscherRecord;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "EscherRecordHeader"
.end annotation


# instance fields
.field private options:S

.field private recordId:S

.field private remainingBytes:I


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 243
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 245
    return-void
.end method

.method public static readHeader([BI)Lorg/apache/index/poi/ddf/EscherRecord$EscherRecordHeader;
    .locals 2
    .param p0, "data"    # [B
    .param p1, "offset"    # I

    .prologue
    .line 249
    new-instance v0, Lorg/apache/index/poi/ddf/EscherRecord$EscherRecordHeader;

    invoke-direct {v0}, Lorg/apache/index/poi/ddf/EscherRecord$EscherRecordHeader;-><init>()V

    .line 250
    .local v0, "header":Lorg/apache/index/poi/ddf/EscherRecord$EscherRecordHeader;
    invoke-static {p0, p1}, Lorg/apache/index/poi/util/LittleEndian;->getShort([BI)S

    move-result v1

    iput-short v1, v0, Lorg/apache/index/poi/ddf/EscherRecord$EscherRecordHeader;->options:S

    .line 251
    add-int/lit8 v1, p1, 0x2

    invoke-static {p0, v1}, Lorg/apache/index/poi/util/LittleEndian;->getShort([BI)S

    move-result v1

    iput-short v1, v0, Lorg/apache/index/poi/ddf/EscherRecord$EscherRecordHeader;->recordId:S

    .line 252
    add-int/lit8 v1, p1, 0x4

    invoke-static {p0, v1}, Lorg/apache/index/poi/util/LittleEndian;->getInt([BI)I

    move-result v1

    iput v1, v0, Lorg/apache/index/poi/ddf/EscherRecord$EscherRecordHeader;->remainingBytes:I

    .line 253
    return-object v0
.end method


# virtual methods
.method public getOptions()S
    .locals 1

    .prologue
    .line 259
    iget-short v0, p0, Lorg/apache/index/poi/ddf/EscherRecord$EscherRecordHeader;->options:S

    return v0
.end method

.method public getRecordId()S
    .locals 1

    .prologue
    .line 264
    iget-short v0, p0, Lorg/apache/index/poi/ddf/EscherRecord$EscherRecordHeader;->recordId:S

    return v0
.end method

.method public getRemainingBytes()I
    .locals 1

    .prologue
    .line 269
    iget v0, p0, Lorg/apache/index/poi/ddf/EscherRecord$EscherRecordHeader;->remainingBytes:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 274
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "EscherRecordHeader{options="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 275
    iget-short v1, p0, Lorg/apache/index/poi/ddf/EscherRecord$EscherRecordHeader;->options:S

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 276
    const-string/jumbo v1, ", recordId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-short v1, p0, Lorg/apache/index/poi/ddf/EscherRecord$EscherRecordHeader;->recordId:S

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 277
    const-string/jumbo v1, ", remainingBytes="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lorg/apache/index/poi/ddf/EscherRecord$EscherRecordHeader;->remainingBytes:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 278
    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 274
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
