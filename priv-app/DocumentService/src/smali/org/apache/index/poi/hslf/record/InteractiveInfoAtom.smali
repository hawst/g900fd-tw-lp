.class public Lorg/apache/index/poi/hslf/record/InteractiveInfoAtom;
.super Lorg/apache/index/poi/hslf/record/RecordAtom;
.source "InteractiveInfoAtom.java"


# static fields
.field public static final ACTION_CUSTOMSHOW:B = 0x7t

.field public static final ACTION_HYPERLINK:B = 0x4t

.field public static final ACTION_JUMP:B = 0x3t

.field public static final ACTION_MACRO:B = 0x1t

.field public static final ACTION_MEDIA:B = 0x6t

.field public static final ACTION_NONE:B = 0x0t

.field public static final ACTION_OLE:B = 0x5t

.field public static final ACTION_RUNPROGRAM:B = 0x2t

.field public static final JUMP_ENDSHOW:B = 0x6t

.field public static final JUMP_FIRSTSLIDE:B = 0x3t

.field public static final JUMP_LASTSLIDE:B = 0x4t

.field public static final JUMP_LASTSLIDEVIEWED:B = 0x5t

.field public static final JUMP_NEXTSLIDE:B = 0x1t

.field public static final JUMP_NONE:B = 0x0t

.field public static final JUMP_PREVIOUSSLIDE:B = 0x2t

.field public static final LINK_CustomShow:B = 0x6t

.field public static final LINK_FirstSlide:B = 0x2t

.field public static final LINK_LastSlide:B = 0x3t

.field public static final LINK_NULL:B = -0x1t

.field public static final LINK_NextSlide:B = 0x0t

.field public static final LINK_OtherFile:B = 0xat

.field public static final LINK_OtherPresentation:B = 0x9t

.field public static final LINK_PreviousSlide:B = 0x1t

.field public static final LINK_SlideNumber:B = 0x7t

.field public static final LINK_Url:B = 0x8t


# instance fields
.field private _data:[B

.field private _header:[B


# direct methods
.method protected constructor <init>()V
    .locals 4

    .prologue
    .line 84
    invoke-direct {p0}, Lorg/apache/index/poi/hslf/record/RecordAtom;-><init>()V

    .line 85
    const/16 v0, 0x8

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/apache/index/poi/hslf/record/InteractiveInfoAtom;->_header:[B

    .line 86
    const/16 v0, 0x10

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/apache/index/poi/hslf/record/InteractiveInfoAtom;->_data:[B

    .line 88
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/InteractiveInfoAtom;->_header:[B

    const/4 v1, 0x2

    invoke-virtual {p0}, Lorg/apache/index/poi/hslf/record/InteractiveInfoAtom;->getRecordType()J

    move-result-wide v2

    long-to-int v2, v2

    int-to-short v2, v2

    invoke-static {v0, v1, v2}, Lorg/apache/index/poi/util/LittleEndian;->putShort([BIS)V

    .line 89
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/InteractiveInfoAtom;->_header:[B

    const/4 v1, 0x4

    iget-object v2, p0, Lorg/apache/index/poi/hslf/record/InteractiveInfoAtom;->_data:[B

    array-length v2, v2

    invoke-static {v0, v1, v2}, Lorg/apache/index/poi/util/LittleEndian;->putInt([BII)V

    .line 92
    return-void
.end method

.method protected constructor <init>([BII)V
    .locals 4
    .param p1, "source"    # [B
    .param p2, "start"    # I
    .param p3, "len"    # I

    .prologue
    const/16 v1, 0x8

    const/4 v3, 0x0

    .line 102
    invoke-direct {p0}, Lorg/apache/index/poi/hslf/record/RecordAtom;-><init>()V

    .line 104
    new-array v0, v1, [B

    iput-object v0, p0, Lorg/apache/index/poi/hslf/record/InteractiveInfoAtom;->_header:[B

    .line 105
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/InteractiveInfoAtom;->_header:[B

    invoke-static {p1, p2, v0, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 108
    add-int/lit8 v0, p3, -0x8

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/apache/index/poi/hslf/record/InteractiveInfoAtom;->_data:[B

    .line 109
    add-int/lit8 v0, p2, 0x8

    iget-object v1, p0, Lorg/apache/index/poi/hslf/record/InteractiveInfoAtom;->_data:[B

    add-int/lit8 v2, p3, -0x8

    invoke-static {p1, v0, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 112
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/InteractiveInfoAtom;->_data:[B

    array-length v0, v0

    const/16 v1, 0x10

    if-ge v0, v1, :cond_0

    .line 113
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "The length of the data for a InteractiveInfoAtom must be at least 16 bytes, but was only "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lorg/apache/index/poi/hslf/record/InteractiveInfoAtom;->_data:[B

    array-length v2, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 120
    :cond_0
    return-void
.end method


# virtual methods
.method public getAction()B
    .locals 2

    .prologue
    .line 164
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/InteractiveInfoAtom;->_data:[B

    const/16 v1, 0x8

    aget-byte v0, v0, v1

    return v0
.end method

.method public getFlags()B
    .locals 2

    .prologue
    .line 227
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/InteractiveInfoAtom;->_data:[B

    const/16 v1, 0xb

    aget-byte v0, v0, v1

    return v0
.end method

.method public getHyperlinkID()I
    .locals 2

    .prologue
    .line 128
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/InteractiveInfoAtom;->_data:[B

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lorg/apache/index/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    return v0
.end method

.method public getHyperlinkType()B
    .locals 2

    .prologue
    .line 249
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/InteractiveInfoAtom;->_data:[B

    const/16 v1, 0xc

    aget-byte v0, v0, v1

    return v0
.end method

.method public getJump()B
    .locals 2

    .prologue
    .line 202
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/InteractiveInfoAtom;->_data:[B

    const/16 v1, 0xa

    aget-byte v0, v0, v1

    return v0
.end method

.method public getOleVerb()B
    .locals 2

    .prologue
    .line 183
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/InteractiveInfoAtom;->_data:[B

    const/16 v1, 0x9

    aget-byte v0, v0, v1

    return v0
.end method

.method public getRecordType()J
    .locals 2

    .prologue
    .line 265
    sget-object v0, Lorg/apache/index/poi/hslf/record/RecordTypes;->InteractiveInfoAtom:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    iget v0, v0, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;->typeID:I

    int-to-long v0, v0

    return-wide v0
.end method

.method public getSoundRef()I
    .locals 2

    .prologue
    .line 144
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/InteractiveInfoAtom;->_data:[B

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lorg/apache/index/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    return v0
.end method

.method public setAction(B)V
    .locals 2
    .param p1, "val"    # B

    .prologue
    .line 176
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/InteractiveInfoAtom;->_data:[B

    const/16 v1, 0x8

    aput-byte p1, v0, v1

    .line 177
    return-void
.end method

.method public setFlags(B)V
    .locals 2
    .param p1, "val"    # B

    .prologue
    .line 240
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/InteractiveInfoAtom;->_data:[B

    const/16 v1, 0xb

    aput-byte p1, v0, v1

    .line 241
    return-void
.end method

.method public setHyperlinkID(I)V
    .locals 2
    .param p1, "number"    # I

    .prologue
    .line 137
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/InteractiveInfoAtom;->_data:[B

    const/4 v1, 0x4

    invoke-static {v0, v1, p1}, Lorg/apache/index/poi/util/LittleEndian;->putInt([BII)V

    .line 138
    return-void
.end method

.method public setHyperlinkType(B)V
    .locals 2
    .param p1, "val"    # B

    .prologue
    .line 258
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/InteractiveInfoAtom;->_data:[B

    const/16 v1, 0xc

    aput-byte p1, v0, v1

    .line 259
    return-void
.end method

.method public setJump(B)V
    .locals 2
    .param p1, "val"    # B

    .prologue
    .line 214
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/InteractiveInfoAtom;->_data:[B

    const/16 v1, 0xa

    aput-byte p1, v0, v1

    .line 215
    return-void
.end method

.method public setOleVerb(B)V
    .locals 2
    .param p1, "val"    # B

    .prologue
    .line 190
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/InteractiveInfoAtom;->_data:[B

    const/16 v1, 0x9

    aput-byte p1, v0, v1

    .line 191
    return-void
.end method

.method public setSoundRef(I)V
    .locals 2
    .param p1, "val"    # I

    .prologue
    .line 152
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/InteractiveInfoAtom;->_data:[B

    const/4 v1, 0x0

    invoke-static {v0, v1, p1}, Lorg/apache/index/poi/util/LittleEndian;->putInt([BII)V

    .line 153
    return-void
.end method

.method public writeOut(Ljava/io/OutputStream;)V
    .locals 1
    .param p1, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 275
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/InteractiveInfoAtom;->_header:[B

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 276
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/InteractiveInfoAtom;->_data:[B

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 277
    return-void
.end method
