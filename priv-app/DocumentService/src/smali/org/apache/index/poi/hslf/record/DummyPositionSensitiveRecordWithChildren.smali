.class public final Lorg/apache/index/poi/hslf/record/DummyPositionSensitiveRecordWithChildren;
.super Lorg/apache/index/poi/hslf/record/PositionDependentRecordContainer;
.source "DummyPositionSensitiveRecordWithChildren.java"


# instance fields
.field private _header:[B

.field private _type:J


# direct methods
.method protected constructor <init>([BII)V
    .locals 3
    .param p1, "source"    # [B
    .param p2, "start"    # I
    .param p3, "len"    # I

    .prologue
    const/16 v2, 0x8

    .line 43
    invoke-direct {p0}, Lorg/apache/index/poi/hslf/record/PositionDependentRecordContainer;-><init>()V

    .line 45
    new-array v0, v2, [B

    iput-object v0, p0, Lorg/apache/index/poi/hslf/record/DummyPositionSensitiveRecordWithChildren;->_header:[B

    .line 46
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/DummyPositionSensitiveRecordWithChildren;->_header:[B

    const/4 v1, 0x0

    invoke-static {p1, p2, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 47
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/DummyPositionSensitiveRecordWithChildren;->_header:[B

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lorg/apache/index/poi/util/LittleEndian;->getUShort([BI)I

    move-result v0

    int-to-long v0, v0

    iput-wide v0, p0, Lorg/apache/index/poi/hslf/record/DummyPositionSensitiveRecordWithChildren;->_type:J

    .line 50
    add-int/lit8 v0, p2, 0x8

    add-int/lit8 v1, p3, -0x8

    invoke-static {p1, v0, v1}, Lorg/apache/index/poi/hslf/record/Record;->findChildRecords([BII)[Lorg/apache/index/poi/hslf/record/Record;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/index/poi/hslf/record/DummyPositionSensitiveRecordWithChildren;->_children:[Lorg/apache/index/poi/hslf/record/Record;

    .line 51
    return-void
.end method


# virtual methods
.method public getRecordType()J
    .locals 2

    .prologue
    .line 56
    iget-wide v0, p0, Lorg/apache/index/poi/hslf/record/DummyPositionSensitiveRecordWithChildren;->_type:J

    return-wide v0
.end method
