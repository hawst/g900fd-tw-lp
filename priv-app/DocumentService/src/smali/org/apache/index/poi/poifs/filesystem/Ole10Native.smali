.class public Lorg/apache/index/poi/poifs/filesystem/Ole10Native;
.super Ljava/lang/Object;
.source "Ole10Native.java"


# static fields
.field public static final OLE10_NATIVE:Ljava/lang/String; = "\u0001Ole10Native"


# instance fields
.field private final command:Ljava/lang/String;

.field private final dataBuffer:[B

.field private final dataSize:I

.field private final fileName:Ljava/lang/String;

.field private flags1:S

.field private flags2:S

.field private flags3:S

.field private final label:Ljava/lang/String;

.field private final totalSize:I

.field private unknown1:[B

.field private unknown2:[B


# direct methods
.method public constructor <init>([BI)V
    .locals 1
    .param p1, "data"    # [B
    .param p2, "offset"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/index/poi/poifs/filesystem/Ole10NativeException;
        }
    .end annotation

    .prologue
    .line 97
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/index/poi/poifs/filesystem/Ole10Native;-><init>([BIZ)V

    .line 98
    return-void
.end method

.method public constructor <init>([BIZ)V
    .locals 8
    .param p1, "data"    # [B
    .param p2, "offset"    # I
    .param p3, "plain"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/index/poi/poifs/filesystem/Ole10NativeException;
        }
    .end annotation

    .prologue
    const/16 v7, 0x8

    const/4 v6, 0x4

    const/4 v5, 0x0

    .line 107
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 108
    move v1, p2

    .line 110
    .local v1, "ofs":I
    array-length v3, p1

    add-int/lit8 v4, p2, 0x2

    if-ge v3, v4, :cond_0

    .line 111
    new-instance v3, Lorg/apache/index/poi/poifs/filesystem/Ole10NativeException;

    const-string/jumbo v4, "data is too small"

    invoke-direct {v3, v4}, Lorg/apache/index/poi/poifs/filesystem/Ole10NativeException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 114
    :cond_0
    invoke-static {p1, v1}, Lorg/apache/index/poi/util/LittleEndian;->getInt([BI)I

    move-result v3

    iput v3, p0, Lorg/apache/index/poi/poifs/filesystem/Ole10Native;->totalSize:I

    .line 115
    add-int/lit8 v1, v1, 0x4

    .line 117
    if-eqz p3, :cond_1

    .line 118
    iget v3, p0, Lorg/apache/index/poi/poifs/filesystem/Ole10Native;->totalSize:I

    add-int/lit8 v3, v3, -0x4

    new-array v3, v3, [B

    iput-object v3, p0, Lorg/apache/index/poi/poifs/filesystem/Ole10Native;->dataBuffer:[B

    .line 119
    iget-object v3, p0, Lorg/apache/index/poi/poifs/filesystem/Ole10Native;->dataBuffer:[B

    iget-object v4, p0, Lorg/apache/index/poi/poifs/filesystem/Ole10Native;->dataBuffer:[B

    array-length v4, v4

    invoke-static {p1, v6, v3, v5, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 120
    iget v3, p0, Lorg/apache/index/poi/poifs/filesystem/Ole10Native;->totalSize:I

    add-int/lit8 v3, v3, -0x4

    iput v3, p0, Lorg/apache/index/poi/poifs/filesystem/Ole10Native;->dataSize:I

    .line 122
    new-array v2, v7, [B

    .line 123
    .local v2, "oleLabel":[B
    iget-object v3, p0, Lorg/apache/index/poi/poifs/filesystem/Ole10Native;->dataBuffer:[B

    iget-object v4, p0, Lorg/apache/index/poi/poifs/filesystem/Ole10Native;->dataBuffer:[B

    array-length v4, v4

    invoke-static {v4, v7}, Ljava/lang/Math;->min(II)I

    move-result v4

    invoke-static {v3, v5, v2, v5, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 124
    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "ole-"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v2}, Lorg/apache/index/poi/util/HexDump;->toHex([B)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lorg/apache/index/poi/poifs/filesystem/Ole10Native;->label:Ljava/lang/String;

    .line 125
    iget-object v3, p0, Lorg/apache/index/poi/poifs/filesystem/Ole10Native;->label:Ljava/lang/String;

    iput-object v3, p0, Lorg/apache/index/poi/poifs/filesystem/Ole10Native;->fileName:Ljava/lang/String;

    .line 126
    iget-object v3, p0, Lorg/apache/index/poi/poifs/filesystem/Ole10Native;->label:Ljava/lang/String;

    iput-object v3, p0, Lorg/apache/index/poi/poifs/filesystem/Ole10Native;->command:Ljava/lang/String;

    .line 170
    .end local v2    # "oleLabel":[B
    :goto_0
    return-void

    .line 128
    :cond_1
    invoke-static {p1, v1}, Lorg/apache/index/poi/util/LittleEndian;->getShort([BI)S

    move-result v3

    iput-short v3, p0, Lorg/apache/index/poi/poifs/filesystem/Ole10Native;->flags1:S

    .line 129
    add-int/lit8 v1, v1, 0x2

    .line 130
    invoke-static {p1, v1}, Lorg/apache/index/poi/poifs/filesystem/Ole10Native;->getStringLength([BI)I

    move-result v0

    .line 131
    .local v0, "len":I
    add-int/lit8 v3, v0, -0x1

    invoke-static {p1, v1, v3}, Lorg/apache/index/poi/util/StringUtil;->getFromCompressedUnicode([BII)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lorg/apache/index/poi/poifs/filesystem/Ole10Native;->label:Ljava/lang/String;

    .line 132
    add-int/2addr v1, v0

    .line 133
    invoke-static {p1, v1}, Lorg/apache/index/poi/poifs/filesystem/Ole10Native;->getStringLength([BI)I

    move-result v0

    .line 134
    add-int/lit8 v3, v0, -0x1

    invoke-static {p1, v1, v3}, Lorg/apache/index/poi/util/StringUtil;->getFromCompressedUnicode([BII)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lorg/apache/index/poi/poifs/filesystem/Ole10Native;->fileName:Ljava/lang/String;

    .line 135
    add-int/2addr v1, v0

    .line 136
    invoke-static {p1, v1}, Lorg/apache/index/poi/util/LittleEndian;->getShort([BI)S

    move-result v3

    iput-short v3, p0, Lorg/apache/index/poi/poifs/filesystem/Ole10Native;->flags2:S

    .line 137
    add-int/lit8 v1, v1, 0x2

    .line 138
    invoke-static {p1, v1}, Lorg/apache/index/poi/util/LittleEndian;->getUnsignedByte([BI)I

    move-result v0

    .line 139
    new-array v3, v0, [B

    iput-object v3, p0, Lorg/apache/index/poi/poifs/filesystem/Ole10Native;->unknown1:[B

    .line 140
    add-int/2addr v1, v0

    .line 141
    const/4 v0, 0x3

    .line 142
    new-array v3, v0, [B

    iput-object v3, p0, Lorg/apache/index/poi/poifs/filesystem/Ole10Native;->unknown2:[B

    .line 143
    add-int/2addr v1, v0

    .line 144
    invoke-static {p1, v1}, Lorg/apache/index/poi/poifs/filesystem/Ole10Native;->getStringLength([BI)I

    move-result v0

    .line 145
    add-int/lit8 v3, v0, -0x1

    invoke-static {p1, v1, v3}, Lorg/apache/index/poi/util/StringUtil;->getFromCompressedUnicode([BII)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lorg/apache/index/poi/poifs/filesystem/Ole10Native;->command:Ljava/lang/String;

    .line 146
    add-int/2addr v1, v0

    .line 148
    iget v3, p0, Lorg/apache/index/poi/poifs/filesystem/Ole10Native;->totalSize:I

    add-int/lit8 v3, v3, 0x4

    sub-int/2addr v3, v1

    if-le v3, v6, :cond_5

    .line 149
    invoke-static {p1, v1}, Lorg/apache/index/poi/util/LittleEndian;->getInt([BI)I

    move-result v3

    iput v3, p0, Lorg/apache/index/poi/poifs/filesystem/Ole10Native;->dataSize:I

    .line 150
    add-int/lit8 v1, v1, 0x4

    .line 152
    iget v3, p0, Lorg/apache/index/poi/poifs/filesystem/Ole10Native;->dataSize:I

    iget v4, p0, Lorg/apache/index/poi/poifs/filesystem/Ole10Native;->totalSize:I

    if-gt v3, v4, :cond_2

    iget v3, p0, Lorg/apache/index/poi/poifs/filesystem/Ole10Native;->dataSize:I

    if-gez v3, :cond_3

    .line 153
    :cond_2
    new-instance v3, Lorg/apache/index/poi/poifs/filesystem/Ole10NativeException;

    const-string/jumbo v4, "Invalid Ole10Native"

    invoke-direct {v3, v4}, Lorg/apache/index/poi/poifs/filesystem/Ole10NativeException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 156
    :cond_3
    iget v3, p0, Lorg/apache/index/poi/poifs/filesystem/Ole10Native;->dataSize:I

    new-array v3, v3, [B

    iput-object v3, p0, Lorg/apache/index/poi/poifs/filesystem/Ole10Native;->dataBuffer:[B

    .line 157
    iget-object v3, p0, Lorg/apache/index/poi/poifs/filesystem/Ole10Native;->dataBuffer:[B

    iget v4, p0, Lorg/apache/index/poi/poifs/filesystem/Ole10Native;->dataSize:I

    invoke-static {p1, v1, v3, v5, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 158
    iget v3, p0, Lorg/apache/index/poi/poifs/filesystem/Ole10Native;->dataSize:I

    add-int/2addr v1, v3

    .line 160
    iget-object v3, p0, Lorg/apache/index/poi/poifs/filesystem/Ole10Native;->unknown1:[B

    array-length v3, v3

    if-lez v3, :cond_4

    .line 161
    invoke-static {p1, v1}, Lorg/apache/index/poi/util/LittleEndian;->getShort([BI)S

    move-result v3

    iput-short v3, p0, Lorg/apache/index/poi/poifs/filesystem/Ole10Native;->flags3:S

    .line 162
    add-int/lit8 v1, v1, 0x2

    .line 163
    goto/16 :goto_0

    .line 164
    :cond_4
    iput-short v5, p0, Lorg/apache/index/poi/poifs/filesystem/Ole10Native;->flags3:S

    goto/16 :goto_0

    .line 167
    :cond_5
    new-instance v3, Lorg/apache/index/poi/poifs/filesystem/Ole10NativeException;

    const-string/jumbo v4, "Invalid Ole10Native"

    invoke-direct {v3, v4}, Lorg/apache/index/poi/poifs/filesystem/Ole10NativeException;-><init>(Ljava/lang/String;)V

    throw v3
.end method

.method public static createFromEmbeddedOleObject(Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;)Lorg/apache/index/poi/poifs/filesystem/Ole10Native;
    .locals 8
    .param p0, "poifs"    # Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/apache/index/poi/poifs/filesystem/Ole10NativeException;
        }
    .end annotation

    .prologue
    .line 59
    const/4 v5, 0x0

    .line 62
    .local v5, "plain":Z
    :try_start_0
    invoke-virtual {p0}, Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;->getRoot()Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;

    move-result-object v6

    const-string/jumbo v7, "\u0001Ole10ItemName"

    invoke-virtual {v6, v7}, Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;->getEntry(Ljava/lang/String;)Lorg/apache/index/poi/poifs/filesystem/Entry;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 63
    const/4 v5, 0x1

    .line 67
    :goto_0
    const/4 v3, 0x0

    .line 68
    .local v3, "dis":Lorg/apache/index/poi/poifs/filesystem/DocumentInputStream;
    const/4 v0, 0x0

    .line 70
    .local v0, "bos":Ljava/io/ByteArrayOutputStream;
    :try_start_1
    const-string/jumbo v6, "\u0001Ole10Native"

    invoke-virtual {p0, v6}, Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;->createDocumentInputStream(Ljava/lang/String;)Lorg/apache/index/poi/poifs/filesystem/DocumentInputStream;

    move-result-object v3

    .line 71
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 72
    .end local v0    # "bos":Ljava/io/ByteArrayOutputStream;
    .local v1, "bos":Ljava/io/ByteArrayOutputStream;
    :try_start_2
    invoke-static {v3, v1}, Lorg/apache/index/poi/util/IOUtils;->copy(Ljava/io/InputStream;Ljava/io/OutputStream;)V

    .line 73
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v2

    .line 74
    .local v2, "data":[B
    new-instance v6, Lorg/apache/index/poi/poifs/filesystem/Ole10Native;

    const/4 v7, 0x0

    invoke-direct {v6, v2, v7, v5}, Lorg/apache/index/poi/poifs/filesystem/Ole10Native;-><init>([BIZ)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 76
    if-eqz v3, :cond_0

    .line 77
    invoke-virtual {v3}, Lorg/apache/index/poi/poifs/filesystem/DocumentInputStream;->close()V

    .line 79
    :cond_0
    if-eqz v1, :cond_1

    .line 81
    :try_start_3
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    .line 74
    :cond_1
    :goto_1
    return-object v6

    .line 64
    .end local v1    # "bos":Ljava/io/ByteArrayOutputStream;
    .end local v2    # "data":[B
    .end local v3    # "dis":Lorg/apache/index/poi/poifs/filesystem/DocumentInputStream;
    :catch_0
    move-exception v4

    .line 65
    .local v4, "ex":Ljava/io/FileNotFoundException;
    const/4 v5, 0x0

    goto :goto_0

    .line 75
    .end local v4    # "ex":Ljava/io/FileNotFoundException;
    .restart local v0    # "bos":Ljava/io/ByteArrayOutputStream;
    .restart local v3    # "dis":Lorg/apache/index/poi/poifs/filesystem/DocumentInputStream;
    :catchall_0
    move-exception v6

    .line 76
    :goto_2
    if-eqz v3, :cond_2

    .line 77
    invoke-virtual {v3}, Lorg/apache/index/poi/poifs/filesystem/DocumentInputStream;->close()V

    .line 79
    :cond_2
    if-eqz v0, :cond_3

    .line 81
    :try_start_4
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    .line 86
    :cond_3
    :goto_3
    throw v6

    .line 82
    .end local v0    # "bos":Ljava/io/ByteArrayOutputStream;
    .restart local v1    # "bos":Ljava/io/ByteArrayOutputStream;
    .restart local v2    # "data":[B
    :catch_1
    move-exception v7

    goto :goto_1

    .end local v1    # "bos":Ljava/io/ByteArrayOutputStream;
    .end local v2    # "data":[B
    .restart local v0    # "bos":Ljava/io/ByteArrayOutputStream;
    :catch_2
    move-exception v7

    goto :goto_3

    .line 75
    .end local v0    # "bos":Ljava/io/ByteArrayOutputStream;
    .restart local v1    # "bos":Ljava/io/ByteArrayOutputStream;
    :catchall_1
    move-exception v6

    move-object v0, v1

    .end local v1    # "bos":Ljava/io/ByteArrayOutputStream;
    .restart local v0    # "bos":Ljava/io/ByteArrayOutputStream;
    goto :goto_2
.end method

.method private static getStringLength([BI)I
    .locals 3
    .param p0, "data"    # [B
    .param p1, "ofs"    # I

    .prologue
    .line 176
    const/4 v0, 0x0

    .line 177
    .local v0, "len":I
    :goto_0
    add-int v1, v0, p1

    array-length v2, p0

    if-ge v1, v2, :cond_0

    add-int v1, p1, v0

    aget-byte v1, p0, v1

    if-nez v1, :cond_1

    .line 180
    :cond_0
    add-int/lit8 v0, v0, 0x1

    .line 181
    return v0

    .line 178
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public getCommand()Ljava/lang/String;
    .locals 1

    .prologue
    .line 257
    iget-object v0, p0, Lorg/apache/index/poi/poifs/filesystem/Ole10Native;->command:Ljava/lang/String;

    return-object v0
.end method

.method public getDataBuffer()[B
    .locals 1

    .prologue
    .line 280
    iget-object v0, p0, Lorg/apache/index/poi/poifs/filesystem/Ole10Native;->dataBuffer:[B

    return-object v0
.end method

.method public getDataSize()I
    .locals 1

    .prologue
    .line 268
    iget v0, p0, Lorg/apache/index/poi/poifs/filesystem/Ole10Native;->dataSize:I

    return v0
.end method

.method public getFileName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 220
    iget-object v0, p0, Lorg/apache/index/poi/poifs/filesystem/Ole10Native;->fileName:Ljava/lang/String;

    return-object v0
.end method

.method public getFlags1()S
    .locals 1

    .prologue
    .line 200
    iget-short v0, p0, Lorg/apache/index/poi/poifs/filesystem/Ole10Native;->flags1:S

    return v0
.end method

.method public getFlags2()S
    .locals 1

    .prologue
    .line 229
    iget-short v0, p0, Lorg/apache/index/poi/poifs/filesystem/Ole10Native;->flags2:S

    return v0
.end method

.method public getFlags3()S
    .locals 1

    .prologue
    .line 289
    iget-short v0, p0, Lorg/apache/index/poi/poifs/filesystem/Ole10Native;->flags3:S

    return v0
.end method

.method public getLabel()Ljava/lang/String;
    .locals 1

    .prologue
    .line 210
    iget-object v0, p0, Lorg/apache/index/poi/poifs/filesystem/Ole10Native;->label:Ljava/lang/String;

    return-object v0
.end method

.method public getTotalSize()I
    .locals 1

    .prologue
    .line 191
    iget v0, p0, Lorg/apache/index/poi/poifs/filesystem/Ole10Native;->totalSize:I

    return v0
.end method

.method public getUnknown1()[B
    .locals 1

    .prologue
    .line 238
    iget-object v0, p0, Lorg/apache/index/poi/poifs/filesystem/Ole10Native;->unknown1:[B

    return-object v0
.end method

.method public getUnknown2()[B
    .locals 1

    .prologue
    .line 247
    iget-object v0, p0, Lorg/apache/index/poi/poifs/filesystem/Ole10Native;->unknown2:[B

    return-object v0
.end method
