.class public Lorg/apache/index/poi/hpsf/PropertySetFactory;
.super Ljava/lang/Object;
.source "PropertySetFactory.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static create(Ljava/io/InputStream;)Lorg/apache/index/poi/hpsf/PropertySet;
    .locals 3
    .param p0, "stream"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/index/poi/hpsf/NoPropertySetStreamException;,
            Lorg/apache/index/poi/hpsf/MarkUnsupportedException;,
            Ljava/io/UnsupportedEncodingException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 58
    new-instance v1, Lorg/apache/index/poi/hpsf/PropertySet;

    invoke-direct {v1, p0}, Lorg/apache/index/poi/hpsf/PropertySet;-><init>(Ljava/io/InputStream;)V

    .line 61
    .local v1, "ps":Lorg/apache/index/poi/hpsf/PropertySet;
    :try_start_0
    invoke-virtual {v1}, Lorg/apache/index/poi/hpsf/PropertySet;->isSummaryInformation()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 62
    new-instance v2, Lorg/apache/index/poi/hpsf/SummaryInformation;

    invoke-direct {v2, v1}, Lorg/apache/index/poi/hpsf/SummaryInformation;-><init>(Lorg/apache/index/poi/hpsf/PropertySet;)V

    move-object v1, v2

    .line 73
    .end local v1    # "ps":Lorg/apache/index/poi/hpsf/PropertySet;
    :cond_0
    :goto_0
    return-object v1

    .line 63
    .restart local v1    # "ps":Lorg/apache/index/poi/hpsf/PropertySet;
    :cond_1
    invoke-virtual {v1}, Lorg/apache/index/poi/hpsf/PropertySet;->isDocumentSummaryInformation()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 64
    new-instance v2, Lorg/apache/index/poi/hpsf/DocumentSummaryInformation;

    invoke-direct {v2, v1}, Lorg/apache/index/poi/hpsf/DocumentSummaryInformation;-><init>(Lorg/apache/index/poi/hpsf/PropertySet;)V
    :try_end_0
    .catch Lorg/apache/index/poi/hpsf/UnexpectedPropertySetTypeException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v1, v2

    goto :goto_0

    .line 68
    :catch_0
    move-exception v0

    .line 73
    .local v0, "ex":Lorg/apache/index/poi/hpsf/UnexpectedPropertySetTypeException;
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static newDocumentSummaryInformation()Lorg/apache/index/poi/hpsf/DocumentSummaryInformation;
    .locals 5

    .prologue
    .line 109
    new-instance v1, Lorg/apache/index/poi/hpsf/MutablePropertySet;

    invoke-direct {v1}, Lorg/apache/index/poi/hpsf/MutablePropertySet;-><init>()V

    .line 110
    .local v1, "ps":Lorg/apache/index/poi/hpsf/MutablePropertySet;
    invoke-virtual {v1}, Lorg/apache/index/poi/hpsf/MutablePropertySet;->getFirstSection()Lorg/apache/index/poi/hpsf/Section;

    move-result-object v2

    check-cast v2, Lorg/apache/index/poi/hpsf/MutableSection;

    .line 111
    .local v2, "s":Lorg/apache/index/poi/hpsf/MutableSection;
    sget-object v3, Lorg/apache/index/poi/hpsf/wellknown/SectionIDMap;->DOCUMENT_SUMMARY_INFORMATION_ID:[[B

    const/4 v4, 0x0

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Lorg/apache/index/poi/hpsf/MutableSection;->setFormatID([B)V

    .line 114
    :try_start_0
    new-instance v3, Lorg/apache/index/poi/hpsf/DocumentSummaryInformation;

    invoke-direct {v3, v1}, Lorg/apache/index/poi/hpsf/DocumentSummaryInformation;-><init>(Lorg/apache/index/poi/hpsf/PropertySet;)V
    :try_end_0
    .catch Lorg/apache/index/poi/hpsf/UnexpectedPropertySetTypeException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v3

    .line 116
    :catch_0
    move-exception v0

    .line 119
    .local v0, "ex":Lorg/apache/index/poi/hpsf/UnexpectedPropertySetTypeException;
    new-instance v3, Lorg/apache/index/poi/hpsf/HPSFRuntimeException;

    invoke-direct {v3, v0}, Lorg/apache/index/poi/hpsf/HPSFRuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v3
.end method

.method public static newSummaryInformation()Lorg/apache/index/poi/hpsf/SummaryInformation;
    .locals 4

    .prologue
    .line 86
    new-instance v1, Lorg/apache/index/poi/hpsf/MutablePropertySet;

    invoke-direct {v1}, Lorg/apache/index/poi/hpsf/MutablePropertySet;-><init>()V

    .line 87
    .local v1, "ps":Lorg/apache/index/poi/hpsf/MutablePropertySet;
    invoke-virtual {v1}, Lorg/apache/index/poi/hpsf/MutablePropertySet;->getFirstSection()Lorg/apache/index/poi/hpsf/Section;

    move-result-object v2

    check-cast v2, Lorg/apache/index/poi/hpsf/MutableSection;

    .line 88
    .local v2, "s":Lorg/apache/index/poi/hpsf/MutableSection;
    sget-object v3, Lorg/apache/index/poi/hpsf/wellknown/SectionIDMap;->SUMMARY_INFORMATION_ID:[B

    invoke-virtual {v2, v3}, Lorg/apache/index/poi/hpsf/MutableSection;->setFormatID([B)V

    .line 91
    :try_start_0
    new-instance v3, Lorg/apache/index/poi/hpsf/SummaryInformation;

    invoke-direct {v3, v1}, Lorg/apache/index/poi/hpsf/SummaryInformation;-><init>(Lorg/apache/index/poi/hpsf/PropertySet;)V
    :try_end_0
    .catch Lorg/apache/index/poi/hpsf/UnexpectedPropertySetTypeException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v3

    .line 93
    :catch_0
    move-exception v0

    .line 96
    .local v0, "ex":Lorg/apache/index/poi/hpsf/UnexpectedPropertySetTypeException;
    new-instance v3, Lorg/apache/index/poi/hpsf/HPSFRuntimeException;

    invoke-direct {v3, v0}, Lorg/apache/index/poi/hpsf/HPSFRuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v3
.end method
