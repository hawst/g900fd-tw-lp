.class public Lorg/apache/index/poi/hslf/record/SlideAtom$SSlideLayoutAtom;
.super Ljava/lang/Object;
.source "SlideAtom.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/index/poi/hslf/record/SlideAtom;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SSlideLayoutAtom"
.end annotation


# static fields
.field public static final BIG_OBJECT:I = 0xf

.field public static final BLANK_SLIDE:I = 0x10

.field public static final FOUR_OBJECTS:I = 0xe

.field public static final HANDOUT:I = 0x6

.field public static final MASTER_NOTES:I = 0x4

.field public static final MASTER_SLIDE:I = 0x3

.field public static final NOTES_TITLE_BODY:I = 0x5

.field public static final TITLE_2_COLUMN_BODY:I = 0x8

.field public static final TITLE_2_COLUNM_LEFT_2_ROW_BODY:I = 0xb

.field public static final TITLE_2_COLUNM_RIGHT_2_ROW_BODY:I = 0xa

.field public static final TITLE_2_ROW_BODY:I = 0x9

.field public static final TITLE_2_ROW_BOTTOM_2_COLUMN_BODY:I = 0xc

.field public static final TITLE_2_ROW_TOP_2_COLUMN_BODY:I = 0xd

.field public static final TITLE_BODY_SLIDE:I = 0x1

.field public static final TITLE_MASTER_SLIDE:I = 0x2

.field public static final TITLE_ONLY:I = 0x7

.field public static final TITLE_SLIDE:I = 0x0

.field public static final VERTICAL_TITLE_2_ROW_BODY_LEFT:I = 0x11

.field public static final VERTICAL_TITLE_BODY_LEFT:I = 0x11


# instance fields
.field private geometry:I

.field private placeholderIDs:[B


# direct methods
.method public constructor <init>([B)V
    .locals 4
    .param p1, "data"    # [B

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 212
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 213
    array-length v0, p1

    const/16 v1, 0xc

    if-eq v0, v1, :cond_0

    .line 214
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "SSlideLayoutAtom created with byte array not 12 bytes long - was "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    array-length v2, p1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " bytes in size"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 218
    :cond_0
    invoke-static {p1, v2}, Lorg/apache/index/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    iput v0, p0, Lorg/apache/index/poi/hslf/record/SlideAtom$SSlideLayoutAtom;->geometry:I

    .line 219
    new-array v0, v3, [B

    iput-object v0, p0, Lorg/apache/index/poi/hslf/record/SlideAtom$SSlideLayoutAtom;->placeholderIDs:[B

    .line 220
    const/4 v0, 0x4

    iget-object v1, p0, Lorg/apache/index/poi/hslf/record/SlideAtom$SSlideLayoutAtom;->placeholderIDs:[B

    invoke-static {p1, v0, v1, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 221
    return-void
.end method


# virtual methods
.method public getGeometryType()I
    .locals 1

    .prologue
    .line 205
    iget v0, p0, Lorg/apache/index/poi/hslf/record/SlideAtom$SSlideLayoutAtom;->geometry:I

    return v0
.end method

.method public setGeometryType(I)V
    .locals 0
    .param p1, "geom"    # I

    .prologue
    .line 207
    iput p1, p0, Lorg/apache/index/poi/hslf/record/SlideAtom$SSlideLayoutAtom;->geometry:I

    return-void
.end method

.method public writeOut(Ljava/io/OutputStream;)V
    .locals 1
    .param p1, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 229
    iget v0, p0, Lorg/apache/index/poi/hslf/record/SlideAtom$SSlideLayoutAtom;->geometry:I

    invoke-static {v0, p1}, Lorg/apache/index/poi/hslf/record/SlideAtom;->writeLittleEndian(ILjava/io/OutputStream;)V

    .line 231
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/SlideAtom$SSlideLayoutAtom;->placeholderIDs:[B

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 232
    return-void
.end method
