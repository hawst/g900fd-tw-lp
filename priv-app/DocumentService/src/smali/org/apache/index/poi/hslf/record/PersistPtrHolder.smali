.class public final Lorg/apache/index/poi/hslf/record/PersistPtrHolder;
.super Lorg/apache/index/poi/hslf/record/PositionDependentRecordAtom;
.source "PersistPtrHolder.java"


# instance fields
.field private _header:[B

.field private _ptrData:[B

.field private _slideLocations:Ljava/util/Hashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private _slideOffsetDataLocation:Ljava/util/Hashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private _type:J

.field private sSyncObj:Ljava/lang/Object;


# direct methods
.method protected constructor <init>([BII)V
    .locals 15
    .param p1, "source"    # [B
    .param p2, "start"    # I
    .param p3, "len"    # I

    .prologue
    .line 128
    invoke-direct {p0}, Lorg/apache/index/poi/hslf/record/PositionDependentRecordAtom;-><init>()V

    .line 45
    new-instance v9, Ljava/lang/Object;

    invoke-direct {v9}, Ljava/lang/Object;-><init>()V

    iput-object v9, p0, Lorg/apache/index/poi/hslf/record/PersistPtrHolder;->sSyncObj:Ljava/lang/Object;

    .line 131
    const/16 v9, 0x8

    move/from16 v0, p3

    if-ge v0, v9, :cond_0

    const/16 p3, 0x8

    .line 134
    :cond_0
    const/16 v9, 0x8

    new-array v9, v9, [B

    iput-object v9, p0, Lorg/apache/index/poi/hslf/record/PersistPtrHolder;->_header:[B

    .line 135
    iget-object v9, p0, Lorg/apache/index/poi/hslf/record/PersistPtrHolder;->_header:[B

    const/4 v12, 0x0

    const/16 v13, 0x8

    move-object/from16 v0, p1

    move/from16 v1, p2

    invoke-static {v0, v1, v9, v12, v13}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 136
    iget-object v9, p0, Lorg/apache/index/poi/hslf/record/PersistPtrHolder;->_header:[B

    const/4 v12, 0x2

    invoke-static {v9, v12}, Lorg/apache/index/poi/util/LittleEndian;->getUShort([BI)I

    move-result v9

    int-to-long v12, v9

    iput-wide v12, p0, Lorg/apache/index/poi/hslf/record/PersistPtrHolder;->_type:J

    .line 145
    new-instance v9, Ljava/util/Hashtable;

    invoke-direct {v9}, Ljava/util/Hashtable;-><init>()V

    iput-object v9, p0, Lorg/apache/index/poi/hslf/record/PersistPtrHolder;->_slideLocations:Ljava/util/Hashtable;

    .line 146
    new-instance v9, Ljava/util/Hashtable;

    invoke-direct {v9}, Ljava/util/Hashtable;-><init>()V

    iput-object v9, p0, Lorg/apache/index/poi/hslf/record/PersistPtrHolder;->_slideOffsetDataLocation:Ljava/util/Hashtable;

    .line 147
    add-int/lit8 v9, p3, -0x8

    new-array v9, v9, [B

    iput-object v9, p0, Lorg/apache/index/poi/hslf/record/PersistPtrHolder;->_ptrData:[B

    .line 148
    add-int/lit8 v9, p2, 0x8

    iget-object v12, p0, Lorg/apache/index/poi/hslf/record/PersistPtrHolder;->_ptrData:[B

    const/4 v13, 0x0

    iget-object v14, p0, Lorg/apache/index/poi/hslf/record/PersistPtrHolder;->_ptrData:[B

    array-length v14, v14

    move-object/from16 v0, p1

    invoke-static {v0, v9, v12, v13, v14}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 150
    const/4 v7, 0x0

    .line 151
    .local v7, "pos":I
    :cond_1
    iget-object v9, p0, Lorg/apache/index/poi/hslf/record/PersistPtrHolder;->_ptrData:[B

    array-length v9, v9

    if-lt v7, v9, :cond_2

    .line 176
    return-void

    .line 153
    :cond_2
    iget-object v9, p0, Lorg/apache/index/poi/hslf/record/PersistPtrHolder;->_ptrData:[B

    invoke-static {v9, v7}, Lorg/apache/index/poi/util/LittleEndian;->getUInt([BI)J

    move-result-wide v4

    .line 157
    .local v4, "info":J
    const/16 v9, 0x14

    shr-long v12, v4, v9

    long-to-int v3, v12

    .line 158
    .local v3, "offset_count":I
    shl-int/lit8 v9, v3, 0x14

    int-to-long v12, v9

    sub-long v12, v4, v12

    long-to-int v6, v12

    .line 162
    .local v6, "offset_no":I
    add-int/lit8 v7, v7, 0x4

    .line 166
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v3, :cond_1

    iget-object v9, p0, Lorg/apache/index/poi/hslf/record/PersistPtrHolder;->_ptrData:[B

    array-length v9, v9

    if-ge v7, v9, :cond_1

    .line 167
    add-int v8, v6, v2

    .line 168
    .local v8, "sheet_no":I
    iget-object v9, p0, Lorg/apache/index/poi/hslf/record/PersistPtrHolder;->_ptrData:[B

    invoke-static {v9, v7}, Lorg/apache/index/poi/util/LittleEndian;->getUInt([BI)J

    move-result-wide v10

    .line 169
    .local v10, "sheet_offset":J
    iget-object v9, p0, Lorg/apache/index/poi/hslf/record/PersistPtrHolder;->_slideLocations:Ljava/util/Hashtable;

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    long-to-int v13, v10

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    invoke-virtual {v9, v12, v13}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 170
    iget-object v9, p0, Lorg/apache/index/poi/hslf/record/PersistPtrHolder;->_slideOffsetDataLocation:Ljava/util/Hashtable;

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    invoke-virtual {v9, v12, v13}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 173
    add-int/lit8 v7, v7, 0x4

    .line 166
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method


# virtual methods
.method public addSlideLookup(II)V
    .locals 5
    .param p1, "slideID"    # I
    .param p2, "posOnDisk"    # I

    .prologue
    const/4 v4, 0x0

    .line 99
    iget-object v2, p0, Lorg/apache/index/poi/hslf/record/PersistPtrHolder;->_ptrData:[B

    array-length v2, v2

    add-int/lit8 v2, v2, 0x8

    new-array v1, v2, [B

    .line 100
    .local v1, "newPtrData":[B
    iget-object v2, p0, Lorg/apache/index/poi/hslf/record/PersistPtrHolder;->_ptrData:[B

    iget-object v3, p0, Lorg/apache/index/poi/hslf/record/PersistPtrHolder;->_ptrData:[B

    array-length v3, v3

    invoke-static {v2, v4, v1, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 103
    iget-object v2, p0, Lorg/apache/index/poi/hslf/record/PersistPtrHolder;->_slideLocations:Ljava/util/Hashtable;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 105
    iget-object v2, p0, Lorg/apache/index/poi/hslf/record/PersistPtrHolder;->_slideOffsetDataLocation:Ljava/util/Hashtable;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    .line 106
    iget-object v4, p0, Lorg/apache/index/poi/hslf/record/PersistPtrHolder;->_ptrData:[B

    array-length v4, v4

    add-int/lit8 v4, v4, 0x4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    .line 105
    invoke-virtual {v2, v3, v4}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 111
    move v0, p1

    .line 112
    .local v0, "infoBlock":I
    const/high16 v2, 0x100000

    add-int/2addr v0, v2

    .line 115
    array-length v2, v1

    add-int/lit8 v2, v2, -0x8

    invoke-static {v1, v2, v0}, Lorg/apache/index/poi/util/LittleEndian;->putInt([BII)V

    .line 116
    array-length v2, v1

    add-int/lit8 v2, v2, -0x4

    invoke-static {v1, v2, p2}, Lorg/apache/index/poi/util/LittleEndian;->putInt([BII)V

    .line 119
    iput-object v1, p0, Lorg/apache/index/poi/hslf/record/PersistPtrHolder;->_ptrData:[B

    .line 122
    iget-object v2, p0, Lorg/apache/index/poi/hslf/record/PersistPtrHolder;->_header:[B

    const/4 v3, 0x4

    array-length v4, v1

    invoke-static {v2, v3, v4}, Lorg/apache/index/poi/util/LittleEndian;->putInt([BII)V

    .line 123
    return-void
.end method

.method public getKnownSlideIDs()[I
    .locals 5

    .prologue
    .line 67
    iget-object v4, p0, Lorg/apache/index/poi/hslf/record/PersistPtrHolder;->_slideLocations:Ljava/util/Hashtable;

    invoke-virtual {v4}, Ljava/util/Hashtable;->size()I

    move-result v4

    new-array v3, v4, [I

    .line 68
    .local v3, "ids":[I
    iget-object v4, p0, Lorg/apache/index/poi/hslf/record/PersistPtrHolder;->_slideLocations:Ljava/util/Hashtable;

    invoke-virtual {v4}, Ljava/util/Hashtable;->keys()Ljava/util/Enumeration;

    move-result-object v0

    .line 69
    .local v0, "e":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/lang/Integer;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v4, v3

    if-lt v1, v4, :cond_0

    .line 73
    return-object v3

    .line 70
    :cond_0
    invoke-interface {v0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    .line 71
    .local v2, "id":Ljava/lang/Integer;
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v4

    aput v4, v3, v1

    .line 69
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public getRecordType()J
    .locals 2

    .prologue
    .line 181
    iget-wide v0, p0, Lorg/apache/index/poi/hslf/record/PersistPtrHolder;->_type:J

    return-wide v0
.end method

.method public getSlideLocationsLookup()Ljava/util/Hashtable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 81
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/PersistPtrHolder;->_slideLocations:Ljava/util/Hashtable;

    return-object v0
.end method

.method public getSlideOffsetDataLocationsLookup()Ljava/util/Hashtable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 88
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/PersistPtrHolder;->_slideOffsetDataLocation:Ljava/util/Hashtable;

    return-object v0
.end method

.method public declared-synchronized updateOtherRecordReferences(Ljava/util/Hashtable;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 189
    .local p1, "oldToNewReferencesLookup":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lorg/apache/index/poi/hslf/record/PersistPtrHolder;->getKnownSlideIDs()[I

    move-result-object v5

    .line 194
    .local v5, "slideIDs":[I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v6, v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    if-lt v1, v6, :cond_0

    .line 220
    monitor-exit p0

    return-void

    .line 195
    :cond_0
    :try_start_1
    aget v6, v5, v1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 196
    .local v2, "id":Ljava/lang/Integer;
    iget-object v6, p0, Lorg/apache/index/poi/hslf/record/PersistPtrHolder;->_slideLocations:Ljava/util/Hashtable;

    invoke-virtual {v6, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    .line 198
    .local v4, "oldPos":Ljava/lang/Integer;
    iget-object v7, p0, Lorg/apache/index/poi/hslf/record/PersistPtrHolder;->sSyncObj:Ljava/lang/Object;

    monitor-enter v7
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 200
    :try_start_2
    invoke-virtual {p1, v4}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    .line 202
    .local v3, "newPos":Ljava/lang/Integer;
    if-nez v3, :cond_1

    .line 203
    iget-object v6, p0, Lorg/apache/index/poi/hslf/record/PersistPtrHolder;->logger:Lorg/apache/index/poi/util/POILogger;

    sget v8, Lorg/apache/index/poi/util/POILogger;->WARN:I

    new-instance v9, Ljava/lang/StringBuilder;

    const-string/jumbo v10, "Couldn\'t find the new location of the \"slide\" with id "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, " that used to be at "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v6, v8, v9}, Lorg/apache/index/poi/util/POILogger;->log(ILjava/lang/Object;)V

    .line 204
    iget-object v6, p0, Lorg/apache/index/poi/hslf/record/PersistPtrHolder;->logger:Lorg/apache/index/poi/util/POILogger;

    sget v8, Lorg/apache/index/poi/util/POILogger;->WARN:I

    const-string/jumbo v9, "Not updating the position of it, you probably won\'t be able to find it any more (if you ever could!)"

    invoke-virtual {v6, v8, v9}, Lorg/apache/index/poi/util/POILogger;->log(ILjava/lang/Object;)V

    .line 205
    move-object v3, v4

    .line 209
    :cond_1
    iget-object v6, p0, Lorg/apache/index/poi/hslf/record/PersistPtrHolder;->_slideOffsetDataLocation:Ljava/util/Hashtable;

    invoke-virtual {v6, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 210
    .local v0, "dataOffset":Ljava/lang/Integer;
    if-eqz v0, :cond_2

    .line 212
    iget-object v6, p0, Lorg/apache/index/poi/hslf/record/PersistPtrHolder;->_ptrData:[B

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v8

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v9

    invoke-static {v6, v8, v9}, Lorg/apache/index/poi/util/LittleEndian;->putInt([BII)V

    .line 216
    :cond_2
    iget-object v6, p0, Lorg/apache/index/poi/hslf/record/PersistPtrHolder;->_slideLocations:Ljava/util/Hashtable;

    invoke-virtual {v6, v2}, Ljava/util/Hashtable;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 217
    iget-object v6, p0, Lorg/apache/index/poi/hslf/record/PersistPtrHolder;->_slideLocations:Ljava/util/Hashtable;

    invoke-virtual {v6, v2, v3}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 198
    monitor-exit v7

    .line 194
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 198
    .end local v0    # "dataOffset":Ljava/lang/Integer;
    .end local v3    # "newPos":Ljava/lang/Integer;
    :catchall_0
    move-exception v6

    monitor-exit v7
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v6
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 189
    .end local v1    # "i":I
    .end local v2    # "id":Ljava/lang/Integer;
    .end local v4    # "oldPos":Ljava/lang/Integer;
    .end local v5    # "slideIDs":[I
    :catchall_1
    move-exception v6

    monitor-exit p0

    throw v6
.end method

.method public writeOut(Ljava/io/OutputStream;)V
    .locals 1
    .param p1, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 227
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/PersistPtrHolder;->_header:[B

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 228
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/PersistPtrHolder;->_ptrData:[B

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 229
    return-void
.end method
