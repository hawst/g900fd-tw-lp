.class public Lorg/apache/index/poi/hwpf/usermodel/Range;
.super Ljava/lang/Object;
.source "Range.java"


# static fields
.field public static final TYPE_CHARACTER:I = 0x1

.field public static final TYPE_LISTENTRY:I = 0x4

.field public static final TYPE_PARAGRAPH:I = 0x0

.field public static final TYPE_SECTION:I = 0x2

.field public static final TYPE_TABLE:I = 0x5

.field public static final TYPE_TEXT:I = 0x3

.field public static final TYPE_UNDEFINED:I = 0x6


# instance fields
.field protected _charEnd:I

.field protected _charRangeFound:Z

.field protected _charStart:I

.field protected _characters:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/apache/index/poi/hwpf/model/CHPX;",
            ">;"
        }
    .end annotation
.end field

.field protected _doc:Lorg/apache/index/poi/hwpf/HWPFDocumentCore;

.field protected _end:I

.field protected _parEnd:I

.field protected _parRangeFound:Z

.field protected _parStart:I

.field protected _paragraphs:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/apache/index/poi/hwpf/model/PAPX;",
            ">;"
        }
    .end annotation
.end field

.field private _parent:Ljava/lang/ref/WeakReference;

.field protected _sectionEnd:I

.field _sectionRangeFound:Z

.field protected _sectionStart:I

.field protected _sections:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/apache/index/poi/hwpf/model/SEPX;",
            ">;"
        }
    .end annotation
.end field

.field protected _start:I

.field protected _text:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/apache/index/poi/hwpf/model/TextPiece;",
            ">;"
        }
    .end annotation
.end field

.field protected _textEnd:I

.field protected _textRangeFound:Z

.field protected _textStart:I


# direct methods
.method protected constructor <init>(IIILorg/apache/index/poi/hwpf/usermodel/Range;)V
    .locals 3
    .param p1, "startIdx"    # I
    .param p2, "endIdx"    # I
    .param p3, "idxType"    # I
    .param p4, "parent"    # Lorg/apache/index/poi/hwpf/usermodel/Range;

    .prologue
    const/4 v2, 0x1

    .line 179
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 180
    iget-object v0, p4, Lorg/apache/index/poi/hwpf/usermodel/Range;->_doc:Lorg/apache/index/poi/hwpf/HWPFDocumentCore;

    iput-object v0, p0, Lorg/apache/index/poi/hwpf/usermodel/Range;->_doc:Lorg/apache/index/poi/hwpf/HWPFDocumentCore;

    .line 181
    iget-object v0, p4, Lorg/apache/index/poi/hwpf/usermodel/Range;->_sections:Ljava/util/List;

    iput-object v0, p0, Lorg/apache/index/poi/hwpf/usermodel/Range;->_sections:Ljava/util/List;

    .line 182
    iget-object v0, p4, Lorg/apache/index/poi/hwpf/usermodel/Range;->_paragraphs:Ljava/util/List;

    iput-object v0, p0, Lorg/apache/index/poi/hwpf/usermodel/Range;->_paragraphs:Ljava/util/List;

    .line 183
    iget-object v0, p4, Lorg/apache/index/poi/hwpf/usermodel/Range;->_characters:Ljava/util/List;

    iput-object v0, p0, Lorg/apache/index/poi/hwpf/usermodel/Range;->_characters:Ljava/util/List;

    .line 184
    iget-object v0, p4, Lorg/apache/index/poi/hwpf/usermodel/Range;->_text:Ljava/util/List;

    iput-object v0, p0, Lorg/apache/index/poi/hwpf/usermodel/Range;->_text:Ljava/util/List;

    .line 185
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p4}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lorg/apache/index/poi/hwpf/usermodel/Range;->_parent:Ljava/lang/ref/WeakReference;

    .line 187
    packed-switch p3, :pswitch_data_0

    .line 218
    :goto_0
    invoke-direct {p0}, Lorg/apache/index/poi/hwpf/usermodel/Range;->sanityCheckStartEnd()V

    .line 219
    return-void

    .line 189
    :pswitch_0
    iget v0, p4, Lorg/apache/index/poi/hwpf/usermodel/Range;->_parStart:I

    add-int/2addr v0, p1

    iput v0, p0, Lorg/apache/index/poi/hwpf/usermodel/Range;->_parStart:I

    .line 190
    iget v0, p4, Lorg/apache/index/poi/hwpf/usermodel/Range;->_parStart:I

    add-int/2addr v0, p2

    iput v0, p0, Lorg/apache/index/poi/hwpf/usermodel/Range;->_parEnd:I

    .line 191
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/usermodel/Range;->_paragraphs:Ljava/util/List;

    iget v1, p0, Lorg/apache/index/poi/hwpf/usermodel/Range;->_parStart:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/index/poi/hwpf/model/PAPX;

    invoke-virtual {v0}, Lorg/apache/index/poi/hwpf/model/PAPX;->getStart()I

    move-result v0

    iput v0, p0, Lorg/apache/index/poi/hwpf/usermodel/Range;->_start:I

    .line 192
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/usermodel/Range;->_paragraphs:Ljava/util/List;

    iget v1, p0, Lorg/apache/index/poi/hwpf/usermodel/Range;->_parEnd:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/index/poi/hwpf/model/PAPX;

    invoke-virtual {v0}, Lorg/apache/index/poi/hwpf/model/PAPX;->getEnd()I

    move-result v0

    iput v0, p0, Lorg/apache/index/poi/hwpf/usermodel/Range;->_end:I

    .line 193
    iput-boolean v2, p0, Lorg/apache/index/poi/hwpf/usermodel/Range;->_parRangeFound:Z

    goto :goto_0

    .line 196
    :pswitch_1
    iget v0, p4, Lorg/apache/index/poi/hwpf/usermodel/Range;->_charStart:I

    add-int/2addr v0, p1

    iput v0, p0, Lorg/apache/index/poi/hwpf/usermodel/Range;->_charStart:I

    .line 197
    iget v0, p4, Lorg/apache/index/poi/hwpf/usermodel/Range;->_charStart:I

    add-int/2addr v0, p2

    iput v0, p0, Lorg/apache/index/poi/hwpf/usermodel/Range;->_charEnd:I

    .line 198
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/usermodel/Range;->_characters:Ljava/util/List;

    iget v1, p0, Lorg/apache/index/poi/hwpf/usermodel/Range;->_charStart:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/index/poi/hwpf/model/CHPX;

    invoke-virtual {v0}, Lorg/apache/index/poi/hwpf/model/CHPX;->getStart()I

    move-result v0

    iput v0, p0, Lorg/apache/index/poi/hwpf/usermodel/Range;->_start:I

    .line 199
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/usermodel/Range;->_characters:Ljava/util/List;

    iget v1, p0, Lorg/apache/index/poi/hwpf/usermodel/Range;->_charEnd:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/index/poi/hwpf/model/CHPX;

    invoke-virtual {v0}, Lorg/apache/index/poi/hwpf/model/CHPX;->getEnd()I

    move-result v0

    iput v0, p0, Lorg/apache/index/poi/hwpf/usermodel/Range;->_end:I

    .line 200
    iput-boolean v2, p0, Lorg/apache/index/poi/hwpf/usermodel/Range;->_charRangeFound:Z

    goto :goto_0

    .line 203
    :pswitch_2
    iget v0, p4, Lorg/apache/index/poi/hwpf/usermodel/Range;->_sectionStart:I

    add-int/2addr v0, p1

    iput v0, p0, Lorg/apache/index/poi/hwpf/usermodel/Range;->_sectionStart:I

    .line 204
    iget v0, p4, Lorg/apache/index/poi/hwpf/usermodel/Range;->_sectionStart:I

    add-int/2addr v0, p2

    iput v0, p0, Lorg/apache/index/poi/hwpf/usermodel/Range;->_sectionEnd:I

    .line 205
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/usermodel/Range;->_sections:Ljava/util/List;

    iget v1, p0, Lorg/apache/index/poi/hwpf/usermodel/Range;->_sectionStart:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/index/poi/hwpf/model/SEPX;

    invoke-virtual {v0}, Lorg/apache/index/poi/hwpf/model/SEPX;->getStart()I

    move-result v0

    iput v0, p0, Lorg/apache/index/poi/hwpf/usermodel/Range;->_start:I

    .line 206
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/usermodel/Range;->_sections:Ljava/util/List;

    iget v1, p0, Lorg/apache/index/poi/hwpf/usermodel/Range;->_sectionEnd:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/index/poi/hwpf/model/SEPX;

    invoke-virtual {v0}, Lorg/apache/index/poi/hwpf/model/SEPX;->getEnd()I

    move-result v0

    iput v0, p0, Lorg/apache/index/poi/hwpf/usermodel/Range;->_end:I

    .line 207
    iput-boolean v2, p0, Lorg/apache/index/poi/hwpf/usermodel/Range;->_sectionRangeFound:Z

    goto/16 :goto_0

    .line 210
    :pswitch_3
    iget v0, p4, Lorg/apache/index/poi/hwpf/usermodel/Range;->_textStart:I

    add-int/2addr v0, p1

    iput v0, p0, Lorg/apache/index/poi/hwpf/usermodel/Range;->_textStart:I

    .line 211
    iget v0, p4, Lorg/apache/index/poi/hwpf/usermodel/Range;->_textStart:I

    add-int/2addr v0, p2

    iput v0, p0, Lorg/apache/index/poi/hwpf/usermodel/Range;->_textEnd:I

    .line 212
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/usermodel/Range;->_text:Ljava/util/List;

    iget v1, p0, Lorg/apache/index/poi/hwpf/usermodel/Range;->_textStart:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/index/poi/hwpf/model/TextPiece;

    invoke-virtual {v0}, Lorg/apache/index/poi/hwpf/model/TextPiece;->getStart()I

    move-result v0

    iput v0, p0, Lorg/apache/index/poi/hwpf/usermodel/Range;->_start:I

    .line 213
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/usermodel/Range;->_text:Ljava/util/List;

    iget v1, p0, Lorg/apache/index/poi/hwpf/usermodel/Range;->_textEnd:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/index/poi/hwpf/model/TextPiece;

    invoke-virtual {v0}, Lorg/apache/index/poi/hwpf/model/TextPiece;->getEnd()I

    move-result v0

    iput v0, p0, Lorg/apache/index/poi/hwpf/usermodel/Range;->_end:I

    .line 214
    iput-boolean v2, p0, Lorg/apache/index/poi/hwpf/usermodel/Range;->_textRangeFound:Z

    goto/16 :goto_0

    .line 187
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public constructor <init>(IILorg/apache/index/poi/hwpf/HWPFDocumentCore;)V
    .locals 2
    .param p1, "start"    # I
    .param p2, "end"    # I
    .param p3, "doc"    # Lorg/apache/index/poi/hwpf/HWPFDocumentCore;

    .prologue
    .line 130
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 131
    iput p1, p0, Lorg/apache/index/poi/hwpf/usermodel/Range;->_start:I

    .line 132
    iput p2, p0, Lorg/apache/index/poi/hwpf/usermodel/Range;->_end:I

    .line 133
    iput-object p3, p0, Lorg/apache/index/poi/hwpf/usermodel/Range;->_doc:Lorg/apache/index/poi/hwpf/HWPFDocumentCore;

    .line 134
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/usermodel/Range;->_doc:Lorg/apache/index/poi/hwpf/HWPFDocumentCore;

    invoke-virtual {v0}, Lorg/apache/index/poi/hwpf/HWPFDocumentCore;->getSectionTable()Lorg/apache/index/poi/hwpf/model/SectionTable;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/index/poi/hwpf/model/SectionTable;->getSections()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/index/poi/hwpf/usermodel/Range;->_sections:Ljava/util/List;

    .line 135
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/usermodel/Range;->_doc:Lorg/apache/index/poi/hwpf/HWPFDocumentCore;

    invoke-virtual {v0}, Lorg/apache/index/poi/hwpf/HWPFDocumentCore;->getParagraphTable()Lorg/apache/index/poi/hwpf/model/PAPBinTable;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/index/poi/hwpf/model/PAPBinTable;->getParagraphs()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/index/poi/hwpf/usermodel/Range;->_paragraphs:Ljava/util/List;

    .line 136
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/usermodel/Range;->_doc:Lorg/apache/index/poi/hwpf/HWPFDocumentCore;

    invoke-virtual {v0}, Lorg/apache/index/poi/hwpf/HWPFDocumentCore;->getCharacterTable()Lorg/apache/index/poi/hwpf/model/CHPBinTable;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/index/poi/hwpf/model/CHPBinTable;->getTextRuns()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/index/poi/hwpf/usermodel/Range;->_characters:Ljava/util/List;

    .line 137
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/usermodel/Range;->_doc:Lorg/apache/index/poi/hwpf/HWPFDocumentCore;

    invoke-virtual {v0}, Lorg/apache/index/poi/hwpf/HWPFDocumentCore;->getTextTable()Lorg/apache/index/poi/hwpf/model/TextPieceTable;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/index/poi/hwpf/model/TextPieceTable;->getTextPieces()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/index/poi/hwpf/usermodel/Range;->_text:Ljava/util/List;

    .line 138
    new-instance v0, Ljava/lang/ref/WeakReference;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lorg/apache/index/poi/hwpf/usermodel/Range;->_parent:Ljava/lang/ref/WeakReference;

    .line 140
    invoke-direct {p0}, Lorg/apache/index/poi/hwpf/usermodel/Range;->sanityCheckStartEnd()V

    .line 141
    return-void
.end method

.method protected constructor <init>(IILorg/apache/index/poi/hwpf/usermodel/Range;)V
    .locals 1
    .param p1, "start"    # I
    .param p2, "end"    # I
    .param p3, "parent"    # Lorg/apache/index/poi/hwpf/usermodel/Range;

    .prologue
    .line 153
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 154
    iput p1, p0, Lorg/apache/index/poi/hwpf/usermodel/Range;->_start:I

    .line 155
    iput p2, p0, Lorg/apache/index/poi/hwpf/usermodel/Range;->_end:I

    .line 156
    iget-object v0, p3, Lorg/apache/index/poi/hwpf/usermodel/Range;->_doc:Lorg/apache/index/poi/hwpf/HWPFDocumentCore;

    iput-object v0, p0, Lorg/apache/index/poi/hwpf/usermodel/Range;->_doc:Lorg/apache/index/poi/hwpf/HWPFDocumentCore;

    .line 157
    iget-object v0, p3, Lorg/apache/index/poi/hwpf/usermodel/Range;->_sections:Ljava/util/List;

    iput-object v0, p0, Lorg/apache/index/poi/hwpf/usermodel/Range;->_sections:Ljava/util/List;

    .line 158
    iget-object v0, p3, Lorg/apache/index/poi/hwpf/usermodel/Range;->_paragraphs:Ljava/util/List;

    iput-object v0, p0, Lorg/apache/index/poi/hwpf/usermodel/Range;->_paragraphs:Ljava/util/List;

    .line 159
    iget-object v0, p3, Lorg/apache/index/poi/hwpf/usermodel/Range;->_characters:Ljava/util/List;

    iput-object v0, p0, Lorg/apache/index/poi/hwpf/usermodel/Range;->_characters:Ljava/util/List;

    .line 160
    iget-object v0, p3, Lorg/apache/index/poi/hwpf/usermodel/Range;->_text:Ljava/util/List;

    iput-object v0, p0, Lorg/apache/index/poi/hwpf/usermodel/Range;->_text:Ljava/util/List;

    .line 161
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p3}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lorg/apache/index/poi/hwpf/usermodel/Range;->_parent:Ljava/lang/ref/WeakReference;

    .line 163
    invoke-direct {p0}, Lorg/apache/index/poi/hwpf/usermodel/Range;->sanityCheckStartEnd()V

    .line 164
    return-void
.end method

.method private findRange(Ljava/util/List;III)[I
    .locals 7
    .param p2, "min"    # I
    .param p3, "start"    # I
    .param p4, "end"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "Lorg/apache/index/poi/hwpf/model/PropertyNode;",
            ">;III)[I"
        }
    .end annotation

    .prologue
    .local p1, "rpl":Ljava/util/List;, "Ljava/util/List<+Lorg/apache/index/poi/hwpf/model/PropertyNode;>;"
    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x2

    .line 521
    move v1, p2

    .line 522
    .local v1, "x":I
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/index/poi/hwpf/model/PropertyNode;

    .line 524
    .local v0, "node":Lorg/apache/index/poi/hwpf/model/PropertyNode;
    :goto_0
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lorg/apache/index/poi/hwpf/model/PropertyNode;->getEnd()I

    move-result v3

    if-gt v3, p3, :cond_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-lt v1, v3, :cond_1

    .line 534
    :cond_0
    invoke-virtual {v0}, Lorg/apache/index/poi/hwpf/model/PropertyNode;->getStart()I

    move-result v3

    if-le v3, p4, :cond_3

    .line 535
    new-array v3, v4, [I

    .line 548
    :goto_1
    return-object v3

    .line 525
    :cond_1
    add-int/lit8 v1, v1, 0x1

    .line 527
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    if-lt v1, v3, :cond_2

    .line 528
    new-array v3, v4, [I

    goto :goto_1

    .line 531
    :cond_2
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "node":Lorg/apache/index/poi/hwpf/model/PropertyNode;
    check-cast v0, Lorg/apache/index/poi/hwpf/model/PropertyNode;

    .restart local v0    # "node":Lorg/apache/index/poi/hwpf/model/PropertyNode;
    goto :goto_0

    .line 538
    :cond_3
    invoke-virtual {v0}, Lorg/apache/index/poi/hwpf/model/PropertyNode;->getEnd()I

    move-result v3

    if-gt v3, p3, :cond_4

    .line 539
    new-array v3, v4, [I

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    aput v4, v3, v5

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    aput v4, v3, v6

    goto :goto_1

    .line 542
    :cond_4
    move v2, v1

    .line 543
    .local v2, "y":I
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "node":Lorg/apache/index/poi/hwpf/model/PropertyNode;
    check-cast v0, Lorg/apache/index/poi/hwpf/model/PropertyNode;

    .line 544
    .restart local v0    # "node":Lorg/apache/index/poi/hwpf/model/PropertyNode;
    :goto_2
    if-eqz v0, :cond_6

    invoke-virtual {v0}, Lorg/apache/index/poi/hwpf/model/PropertyNode;->getEnd()I

    move-result v3

    if-ge v3, p4, :cond_5

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-lt v2, v3, :cond_6

    .line 548
    :cond_5
    new-array v3, v4, [I

    aput v1, v3, v5

    add-int/lit8 v4, v2, 0x1

    aput v4, v3, v6

    goto :goto_1

    .line 545
    :cond_6
    add-int/lit8 v2, v2, 0x1

    .line 546
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "node":Lorg/apache/index/poi/hwpf/model/PropertyNode;
    check-cast v0, Lorg/apache/index/poi/hwpf/model/PropertyNode;

    .restart local v0    # "node":Lorg/apache/index/poi/hwpf/model/PropertyNode;
    goto :goto_2
.end method

.method private initCharacterRuns()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 474
    iget-boolean v1, p0, Lorg/apache/index/poi/hwpf/usermodel/Range;->_charRangeFound:Z

    if-nez v1, :cond_0

    .line 475
    iget-object v1, p0, Lorg/apache/index/poi/hwpf/usermodel/Range;->_characters:Ljava/util/List;

    iget v2, p0, Lorg/apache/index/poi/hwpf/usermodel/Range;->_charStart:I

    iget v3, p0, Lorg/apache/index/poi/hwpf/usermodel/Range;->_start:I

    iget v4, p0, Lorg/apache/index/poi/hwpf/usermodel/Range;->_end:I

    invoke-direct {p0, v1, v2, v3, v4}, Lorg/apache/index/poi/hwpf/usermodel/Range;->findRange(Ljava/util/List;III)[I

    move-result-object v0

    .line 476
    .local v0, "point":[I
    const/4 v1, 0x0

    aget v1, v0, v1

    iput v1, p0, Lorg/apache/index/poi/hwpf/usermodel/Range;->_charStart:I

    .line 477
    aget v1, v0, v5

    iput v1, p0, Lorg/apache/index/poi/hwpf/usermodel/Range;->_charEnd:I

    .line 478
    iput-boolean v5, p0, Lorg/apache/index/poi/hwpf/usermodel/Range;->_charRangeFound:Z

    .line 480
    .end local v0    # "point":[I
    :cond_0
    return-void
.end method

.method private initParagraphs()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 462
    iget-boolean v1, p0, Lorg/apache/index/poi/hwpf/usermodel/Range;->_parRangeFound:Z

    if-nez v1, :cond_0

    .line 463
    iget-object v1, p0, Lorg/apache/index/poi/hwpf/usermodel/Range;->_paragraphs:Ljava/util/List;

    iget v2, p0, Lorg/apache/index/poi/hwpf/usermodel/Range;->_parStart:I

    iget v3, p0, Lorg/apache/index/poi/hwpf/usermodel/Range;->_start:I

    iget v4, p0, Lorg/apache/index/poi/hwpf/usermodel/Range;->_end:I

    invoke-direct {p0, v1, v2, v3, v4}, Lorg/apache/index/poi/hwpf/usermodel/Range;->findRange(Ljava/util/List;III)[I

    move-result-object v0

    .line 464
    .local v0, "point":[I
    const/4 v1, 0x0

    aget v1, v0, v1

    iput v1, p0, Lorg/apache/index/poi/hwpf/usermodel/Range;->_parStart:I

    .line 465
    aget v1, v0, v5

    iput v1, p0, Lorg/apache/index/poi/hwpf/usermodel/Range;->_parEnd:I

    .line 466
    iput-boolean v5, p0, Lorg/apache/index/poi/hwpf/usermodel/Range;->_parRangeFound:Z

    .line 468
    .end local v0    # "point":[I
    :cond_0
    return-void
.end method

.method private initSections()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 498
    iget-boolean v1, p0, Lorg/apache/index/poi/hwpf/usermodel/Range;->_sectionRangeFound:Z

    if-nez v1, :cond_0

    .line 499
    iget-object v1, p0, Lorg/apache/index/poi/hwpf/usermodel/Range;->_sections:Ljava/util/List;

    iget v2, p0, Lorg/apache/index/poi/hwpf/usermodel/Range;->_sectionStart:I

    iget v3, p0, Lorg/apache/index/poi/hwpf/usermodel/Range;->_start:I

    iget v4, p0, Lorg/apache/index/poi/hwpf/usermodel/Range;->_end:I

    invoke-direct {p0, v1, v2, v3, v4}, Lorg/apache/index/poi/hwpf/usermodel/Range;->findRange(Ljava/util/List;III)[I

    move-result-object v0

    .line 500
    .local v0, "point":[I
    const/4 v1, 0x0

    aget v1, v0, v1

    iput v1, p0, Lorg/apache/index/poi/hwpf/usermodel/Range;->_sectionStart:I

    .line 501
    aget v1, v0, v5

    iput v1, p0, Lorg/apache/index/poi/hwpf/usermodel/Range;->_sectionEnd:I

    .line 502
    iput-boolean v5, p0, Lorg/apache/index/poi/hwpf/usermodel/Range;->_sectionRangeFound:Z

    .line 504
    .end local v0    # "point":[I
    :cond_0
    return-void
.end method

.method private initText()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 486
    iget-boolean v1, p0, Lorg/apache/index/poi/hwpf/usermodel/Range;->_textRangeFound:Z

    if-nez v1, :cond_0

    .line 487
    iget-object v1, p0, Lorg/apache/index/poi/hwpf/usermodel/Range;->_text:Ljava/util/List;

    iget v2, p0, Lorg/apache/index/poi/hwpf/usermodel/Range;->_textStart:I

    iget v3, p0, Lorg/apache/index/poi/hwpf/usermodel/Range;->_start:I

    iget v4, p0, Lorg/apache/index/poi/hwpf/usermodel/Range;->_end:I

    invoke-direct {p0, v1, v2, v3, v4}, Lorg/apache/index/poi/hwpf/usermodel/Range;->findRange(Ljava/util/List;III)[I

    move-result-object v0

    .line 488
    .local v0, "point":[I
    const/4 v1, 0x0

    aget v1, v0, v1

    iput v1, p0, Lorg/apache/index/poi/hwpf/usermodel/Range;->_textStart:I

    .line 489
    aget v1, v0, v5

    iput v1, p0, Lorg/apache/index/poi/hwpf/usermodel/Range;->_textEnd:I

    .line 490
    iput-boolean v5, p0, Lorg/apache/index/poi/hwpf/usermodel/Range;->_textRangeFound:Z

    .line 492
    .end local v0    # "point":[I
    :cond_0
    return-void
.end method

.method private sanityCheckStartEnd()V
    .locals 3

    .prologue
    .line 226
    iget v0, p0, Lorg/apache/index/poi/hwpf/usermodel/Range;->_start:I

    if-gez v0, :cond_0

    .line 227
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Range start must not be negative. Given "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lorg/apache/index/poi/hwpf/usermodel/Range;->_start:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 229
    :cond_0
    iget v0, p0, Lorg/apache/index/poi/hwpf/usermodel/Range;->_end:I

    iget v1, p0, Lorg/apache/index/poi/hwpf/usermodel/Range;->_start:I

    if-ge v0, v1, :cond_1

    .line 230
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "The end ("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lorg/apache/index/poi/hwpf/usermodel/Range;->_end:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 231
    const-string/jumbo v2, ") must not be before the start ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lorg/apache/index/poi/hwpf/usermodel/Range;->_start:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 230
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 233
    :cond_1
    return-void
.end method

.method public static stripFields(Ljava/lang/String;)Ljava/lang/String;
    .locals 10
    .param p0, "text"    # Ljava/lang/String;

    .prologue
    const/16 v9, 0x15

    const/4 v8, 0x0

    const/16 v7, 0x13

    const/4 v6, -0x1

    .line 295
    invoke-virtual {p0, v7}, Ljava/lang/String;->indexOf(I)I

    move-result v4

    if-ne v4, v6, :cond_4

    .line 333
    :cond_0
    :goto_0
    return-object p0

    .line 301
    :cond_1
    invoke-virtual {p0, v7}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    .line 302
    .local v0, "first13":I
    add-int/lit8 v4, v0, 0x1

    invoke-virtual {p0, v7, v4}, Ljava/lang/String;->indexOf(II)I

    move-result v3

    .line 303
    .local v3, "next13":I
    const/16 v4, 0x14

    add-int/lit8 v5, v0, 0x1

    invoke-virtual {p0, v4, v5}, Ljava/lang/String;->indexOf(II)I

    move-result v1

    .line 304
    .local v1, "first14":I
    invoke-virtual {p0, v9}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v2

    .line 307
    .local v2, "last15":I
    if-lt v2, v0, :cond_0

    .line 312
    if-ne v3, v6, :cond_2

    if-ne v1, v6, :cond_2

    .line 313
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-virtual {p0, v8, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    add-int/lit8 v5, v2, 0x1

    invoke-virtual {p0, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 314
    goto :goto_0

    .line 320
    :cond_2
    if-eq v1, v6, :cond_5

    if-lt v1, v3, :cond_3

    if-ne v3, v6, :cond_5

    .line 321
    :cond_3
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-virtual {p0, v8, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    add-int/lit8 v5, v1, 0x1

    invoke-virtual {p0, v5, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 322
    add-int/lit8 v5, v2, 0x1

    invoke-virtual {p0, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 321
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 300
    .end local v0    # "first13":I
    .end local v1    # "first14":I
    .end local v2    # "last15":I
    .end local v3    # "next13":I
    :cond_4
    :goto_1
    invoke-virtual {p0, v7}, Ljava/lang/String;->indexOf(I)I

    move-result v4

    if-le v4, v6, :cond_0

    invoke-virtual {p0, v9}, Ljava/lang/String;->indexOf(I)I

    move-result v4

    if-gt v4, v6, :cond_1

    goto :goto_0

    .line 329
    .restart local v0    # "first13":I
    .restart local v1    # "first14":I
    .restart local v2    # "last15":I
    .restart local v3    # "next13":I
    :cond_5
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-virtual {p0, v8, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    add-int/lit8 v5, v2, 0x1

    invoke-virtual {p0, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    goto :goto_1
.end method


# virtual methods
.method public getCharacterRun(I)Lorg/apache/index/poi/hwpf/usermodel/CharacterRun;
    .locals 10
    .param p1, "index"    # I

    .prologue
    const/4 v0, 0x0

    const/4 v9, 0x0

    .line 377
    invoke-direct {p0}, Lorg/apache/index/poi/hwpf/usermodel/Range;->initCharacterRuns()V

    .line 378
    iget-object v5, p0, Lorg/apache/index/poi/hwpf/usermodel/Range;->_characters:Ljava/util/List;

    iget v6, p0, Lorg/apache/index/poi/hwpf/usermodel/Range;->_charStart:I

    add-int/2addr v6, p1

    invoke-interface {v5, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/index/poi/hwpf/model/CHPX;

    .line 380
    .local v1, "chpx":Lorg/apache/index/poi/hwpf/model/CHPX;
    if-nez v1, :cond_1

    .line 396
    :cond_0
    :goto_0
    return-object v0

    .line 384
    :cond_1
    iget-object v5, p0, Lorg/apache/index/poi/hwpf/usermodel/Range;->_paragraphs:Ljava/util/List;

    iget v6, p0, Lorg/apache/index/poi/hwpf/usermodel/Range;->_parStart:I

    invoke-virtual {v1}, Lorg/apache/index/poi/hwpf/model/CHPX;->getStart()I

    move-result v7

    iget v8, p0, Lorg/apache/index/poi/hwpf/usermodel/Range;->_start:I

    invoke-static {v7, v8}, Ljava/lang/Math;->max(II)I

    move-result v7

    .line 385
    invoke-virtual {v1}, Lorg/apache/index/poi/hwpf/model/CHPX;->getEnd()I

    move-result v8

    .line 384
    invoke-direct {p0, v5, v6, v7, v8}, Lorg/apache/index/poi/hwpf/usermodel/Range;->findRange(Ljava/util/List;III)[I

    move-result-object v4

    .line 387
    .local v4, "point":[I
    aget v5, v4, v9

    iget-object v6, p0, Lorg/apache/index/poi/hwpf/usermodel/Range;->_paragraphs:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    if-ge v5, v6, :cond_0

    .line 391
    iget-object v5, p0, Lorg/apache/index/poi/hwpf/usermodel/Range;->_paragraphs:Ljava/util/List;

    aget v6, v4, v9

    invoke-interface {v5, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/index/poi/hwpf/model/PAPX;

    .line 392
    .local v3, "papx":Lorg/apache/index/poi/hwpf/model/PAPX;
    invoke-virtual {v3}, Lorg/apache/index/poi/hwpf/model/PAPX;->getIstd()S

    move-result v2

    .line 394
    .local v2, "istd":S
    new-instance v0, Lorg/apache/index/poi/hwpf/usermodel/CharacterRun;

    invoke-direct {v0, v1, v2, p0}, Lorg/apache/index/poi/hwpf/usermodel/CharacterRun;-><init>(Lorg/apache/index/poi/hwpf/model/CHPX;SLorg/apache/index/poi/hwpf/usermodel/Range;)V

    .line 396
    .local v0, "chp":Lorg/apache/index/poi/hwpf/usermodel/CharacterRun;
    goto :goto_0
.end method

.method protected getDocument()Lorg/apache/index/poi/hwpf/HWPFDocumentCore;
    .locals 1

    .prologue
    .line 570
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/usermodel/Range;->_doc:Lorg/apache/index/poi/hwpf/HWPFDocumentCore;

    return-object v0
.end method

.method public getEndOffset()I
    .locals 1

    .prologue
    .line 566
    iget v0, p0, Lorg/apache/index/poi/hwpf/usermodel/Range;->_end:I

    return v0
.end method

.method public getParagraph(I)Lorg/apache/index/poi/hwpf/usermodel/Paragraph;
    .locals 4
    .param p1, "index"    # I

    .prologue
    .line 422
    invoke-direct {p0}, Lorg/apache/index/poi/hwpf/usermodel/Range;->initParagraphs()V

    .line 423
    iget-object v2, p0, Lorg/apache/index/poi/hwpf/usermodel/Range;->_paragraphs:Ljava/util/List;

    iget v3, p0, Lorg/apache/index/poi/hwpf/usermodel/Range;->_parStart:I

    add-int/2addr v3, p1

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/index/poi/hwpf/model/PAPX;

    .line 426
    .local v1, "papx":Lorg/apache/index/poi/hwpf/model/PAPX;
    const/4 v0, 0x0

    .line 428
    .local v0, "pap":Lorg/apache/index/poi/hwpf/usermodel/Paragraph;
    iget v2, p0, Lorg/apache/index/poi/hwpf/usermodel/Range;->_parStart:I

    add-int/2addr v2, p1

    if-nez v2, :cond_0

    invoke-virtual {v1}, Lorg/apache/index/poi/hwpf/model/PAPX;->getStart()I

    move-result v2

    if-lez v2, :cond_0

    .line 429
    new-instance v0, Lorg/apache/index/poi/hwpf/usermodel/Paragraph;

    .end local v0    # "pap":Lorg/apache/index/poi/hwpf/usermodel/Paragraph;
    const/4 v2, 0x0

    invoke-direct {v0, v1, p0, v2}, Lorg/apache/index/poi/hwpf/usermodel/Paragraph;-><init>(Lorg/apache/index/poi/hwpf/model/PAPX;Lorg/apache/index/poi/hwpf/usermodel/Range;I)V

    .line 435
    .restart local v0    # "pap":Lorg/apache/index/poi/hwpf/usermodel/Paragraph;
    :goto_0
    return-object v0

    .line 431
    :cond_0
    new-instance v0, Lorg/apache/index/poi/hwpf/usermodel/Paragraph;

    .end local v0    # "pap":Lorg/apache/index/poi/hwpf/usermodel/Paragraph;
    invoke-direct {v0, v1, p0}, Lorg/apache/index/poi/hwpf/usermodel/Paragraph;-><init>(Lorg/apache/index/poi/hwpf/model/PAPX;Lorg/apache/index/poi/hwpf/usermodel/Range;)V

    .restart local v0    # "pap":Lorg/apache/index/poi/hwpf/usermodel/Paragraph;
    goto :goto_0
.end method

.method public getSection(I)Lorg/apache/index/poi/hwpf/usermodel/Section;
    .locals 4
    .param p1, "index"    # I

    .prologue
    .line 407
    invoke-direct {p0}, Lorg/apache/index/poi/hwpf/usermodel/Range;->initSections()V

    .line 408
    iget-object v2, p0, Lorg/apache/index/poi/hwpf/usermodel/Range;->_sections:Ljava/util/List;

    iget v3, p0, Lorg/apache/index/poi/hwpf/usermodel/Range;->_sectionStart:I

    add-int/2addr v3, p1

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/index/poi/hwpf/model/SEPX;

    .line 409
    .local v1, "sepx":Lorg/apache/index/poi/hwpf/model/SEPX;
    new-instance v0, Lorg/apache/index/poi/hwpf/usermodel/Section;

    invoke-direct {v0, v1, p0}, Lorg/apache/index/poi/hwpf/usermodel/Section;-><init>(Lorg/apache/index/poi/hwpf/model/SEPX;Lorg/apache/index/poi/hwpf/usermodel/Range;)V

    .line 410
    .local v0, "sep":Lorg/apache/index/poi/hwpf/usermodel/Section;
    return-object v0
.end method

.method public getStartOffset()I
    .locals 1

    .prologue
    .line 562
    iget v0, p0, Lorg/apache/index/poi/hwpf/usermodel/Range;->_start:I

    return v0
.end method

.method protected initAll()V
    .locals 0

    .prologue
    .line 452
    invoke-direct {p0}, Lorg/apache/index/poi/hwpf/usermodel/Range;->initText()V

    .line 453
    invoke-direct {p0}, Lorg/apache/index/poi/hwpf/usermodel/Range;->initCharacterRuns()V

    .line 454
    invoke-direct {p0}, Lorg/apache/index/poi/hwpf/usermodel/Range;->initParagraphs()V

    .line 455
    invoke-direct {p0}, Lorg/apache/index/poi/hwpf/usermodel/Range;->initSections()V

    .line 456
    return-void
.end method

.method public numCharacterRuns()I
    .locals 2

    .prologue
    .line 365
    invoke-direct {p0}, Lorg/apache/index/poi/hwpf/usermodel/Range;->initCharacterRuns()V

    .line 366
    iget v0, p0, Lorg/apache/index/poi/hwpf/usermodel/Range;->_charEnd:I

    iget v1, p0, Lorg/apache/index/poi/hwpf/usermodel/Range;->_charStart:I

    sub-int/2addr v0, v1

    return v0
.end method

.method public numParagraphs()I
    .locals 2

    .prologue
    .line 355
    invoke-direct {p0}, Lorg/apache/index/poi/hwpf/usermodel/Range;->initParagraphs()V

    .line 356
    iget v0, p0, Lorg/apache/index/poi/hwpf/usermodel/Range;->_parEnd:I

    iget v1, p0, Lorg/apache/index/poi/hwpf/usermodel/Range;->_parStart:I

    sub-int/2addr v0, v1

    return v0
.end method

.method public numSections()I
    .locals 2

    .prologue
    .line 343
    invoke-direct {p0}, Lorg/apache/index/poi/hwpf/usermodel/Range;->initSections()V

    .line 344
    iget v0, p0, Lorg/apache/index/poi/hwpf/usermodel/Range;->_sectionEnd:I

    iget v1, p0, Lorg/apache/index/poi/hwpf/usermodel/Range;->_sectionStart:I

    sub-int/2addr v0, v1

    return v0
.end method

.method public text()Ljava/lang/String;
    .locals 7

    .prologue
    .line 259
    invoke-direct {p0}, Lorg/apache/index/poi/hwpf/usermodel/Range;->initText()V

    .line 261
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    .line 263
    .local v3, "sb":Ljava/lang/StringBuffer;
    iget v4, p0, Lorg/apache/index/poi/hwpf/usermodel/Range;->_textStart:I

    .local v4, "x":I
    :goto_0
    iget v5, p0, Lorg/apache/index/poi/hwpf/usermodel/Range;->_textEnd:I

    if-lt v4, v5, :cond_0

    .line 281
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    return-object v5

    .line 264
    :cond_0
    iget-object v5, p0, Lorg/apache/index/poi/hwpf/usermodel/Range;->_text:Ljava/util/List;

    invoke-interface {v5, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/index/poi/hwpf/model/TextPiece;

    .line 268
    .local v0, "piece":Lorg/apache/index/poi/hwpf/model/TextPiece;
    const/4 v2, 0x0

    .line 269
    .local v2, "rStart":I
    invoke-virtual {v0}, Lorg/apache/index/poi/hwpf/model/TextPiece;->characterLength()I

    move-result v1

    .line 270
    .local v1, "rEnd":I
    iget v5, p0, Lorg/apache/index/poi/hwpf/usermodel/Range;->_start:I

    invoke-virtual {v0}, Lorg/apache/index/poi/hwpf/model/TextPiece;->getStart()I

    move-result v6

    if-le v5, v6, :cond_1

    .line 271
    iget v5, p0, Lorg/apache/index/poi/hwpf/usermodel/Range;->_start:I

    invoke-virtual {v0}, Lorg/apache/index/poi/hwpf/model/TextPiece;->getStart()I

    move-result v6

    sub-int v2, v5, v6

    .line 273
    :cond_1
    iget v5, p0, Lorg/apache/index/poi/hwpf/usermodel/Range;->_end:I

    invoke-virtual {v0}, Lorg/apache/index/poi/hwpf/model/TextPiece;->getEnd()I

    move-result v6

    if-ge v5, v6, :cond_2

    .line 274
    invoke-virtual {v0}, Lorg/apache/index/poi/hwpf/model/TextPiece;->getEnd()I

    move-result v5

    iget v6, p0, Lorg/apache/index/poi/hwpf/usermodel/Range;->_end:I

    sub-int/2addr v5, v6

    sub-int/2addr v1, v5

    .line 279
    :cond_2
    invoke-virtual {v0, v2, v1}, Lorg/apache/index/poi/hwpf/model/TextPiece;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 263
    add-int/lit8 v4, v4, 0x1

    goto :goto_0
.end method

.method public type()I
    .locals 1

    .prologue
    .line 445
    const/4 v0, 0x6

    return v0
.end method

.method public usesUnicode()Z
    .locals 3

    .prologue
    .line 242
    invoke-direct {p0}, Lorg/apache/index/poi/hwpf/usermodel/Range;->initText()V

    .line 244
    iget v0, p0, Lorg/apache/index/poi/hwpf/usermodel/Range;->_textStart:I

    .local v0, "i":I
    :goto_0
    iget v2, p0, Lorg/apache/index/poi/hwpf/usermodel/Range;->_textEnd:I

    if-lt v0, v2, :cond_0

    .line 250
    const/4 v2, 0x0

    :goto_1
    return v2

    .line 245
    :cond_0
    iget-object v2, p0, Lorg/apache/index/poi/hwpf/usermodel/Range;->_text:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/index/poi/hwpf/model/TextPiece;

    .line 246
    .local v1, "piece":Lorg/apache/index/poi/hwpf/model/TextPiece;
    invoke-virtual {v1}, Lorg/apache/index/poi/hwpf/model/TextPiece;->isUnicode()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 247
    const/4 v2, 0x1

    goto :goto_1

    .line 244
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
