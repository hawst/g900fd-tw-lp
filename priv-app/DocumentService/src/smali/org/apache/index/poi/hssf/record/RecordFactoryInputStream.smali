.class public final Lorg/apache/index/poi/hssf/record/RecordFactoryInputStream;
.super Ljava/lang/Object;
.source "RecordFactoryInputStream.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/index/poi/hssf/record/RecordFactoryInputStream$StreamEncryptionInfo;
    }
.end annotation


# instance fields
.field private _bofDepth:I

.field private _lastRecord:Lorg/apache/index/poi/hssf/record/Record;

.field private _lastRecordWasEOFLevelZero:Z

.field private final _recStream:Lorg/apache/index/poi/hssf/record/RecordInputStream;

.field private final _shouldIncludeContinueRecords:Z

.field private _unreadRecordBuffer:[Lorg/apache/index/poi/hssf/record/Record;

.field private _unreadRecordIndex:I


# direct methods
.method public constructor <init>(Ljava/io/InputStream;ZLcom/samsung/index/ITextContentObs;)V
    .locals 5
    .param p1, "in"    # Ljava/io/InputStream;
    .param p2, "shouldIncludeContinueRecords"    # Z
    .param p3, "indexWriterhssf"    # Lcom/samsung/index/ITextContentObs;

    .prologue
    const/4 v4, 0x0

    .line 143
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 122
    const/4 v3, -0x1

    iput v3, p0, Lorg/apache/index/poi/hssf/record/RecordFactoryInputStream;->_unreadRecordIndex:I

    .line 127
    const/4 v3, 0x0

    iput-object v3, p0, Lorg/apache/index/poi/hssf/record/RecordFactoryInputStream;->_lastRecord:Lorg/apache/index/poi/hssf/record/Record;

    .line 144
    new-instance v1, Lorg/apache/index/poi/hssf/record/RecordInputStream;

    invoke-direct {v1, p1, p3}, Lorg/apache/index/poi/hssf/record/RecordInputStream;-><init>(Ljava/io/InputStream;Lcom/samsung/index/ITextContentObs;)V

    .line 147
    .local v1, "rs":Lorg/apache/index/poi/hssf/record/RecordInputStream;
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 148
    .local v0, "records":Ljava/util/List;, "Ljava/util/List<Lorg/apache/index/poi/hssf/record/Record;>;"
    new-instance v2, Lorg/apache/index/poi/hssf/record/RecordFactoryInputStream$StreamEncryptionInfo;

    invoke-direct {v2, v1, v0, p3}, Lorg/apache/index/poi/hssf/record/RecordFactoryInputStream$StreamEncryptionInfo;-><init>(Lorg/apache/index/poi/hssf/record/RecordInputStream;Ljava/util/List;Lcom/samsung/index/ITextContentObs;)V

    .line 155
    .local v2, "sei":Lorg/apache/index/poi/hssf/record/RecordFactoryInputStream$StreamEncryptionInfo;
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 156
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    new-array v3, v3, [Lorg/apache/index/poi/hssf/record/Record;

    iput-object v3, p0, Lorg/apache/index/poi/hssf/record/RecordFactoryInputStream;->_unreadRecordBuffer:[Lorg/apache/index/poi/hssf/record/Record;

    .line 157
    iget-object v3, p0, Lorg/apache/index/poi/hssf/record/RecordFactoryInputStream;->_unreadRecordBuffer:[Lorg/apache/index/poi/hssf/record/Record;

    invoke-interface {v0, v3}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 158
    iput v4, p0, Lorg/apache/index/poi/hssf/record/RecordFactoryInputStream;->_unreadRecordIndex:I

    .line 160
    :cond_0
    iput-object v1, p0, Lorg/apache/index/poi/hssf/record/RecordFactoryInputStream;->_recStream:Lorg/apache/index/poi/hssf/record/RecordInputStream;

    .line 161
    iput-boolean p2, p0, Lorg/apache/index/poi/hssf/record/RecordFactoryInputStream;->_shouldIncludeContinueRecords:Z

    .line 162
    invoke-virtual {v2}, Lorg/apache/index/poi/hssf/record/RecordFactoryInputStream$StreamEncryptionInfo;->getLastRecord()Lorg/apache/index/poi/hssf/record/Record;

    move-result-object v3

    iput-object v3, p0, Lorg/apache/index/poi/hssf/record/RecordFactoryInputStream;->_lastRecord:Lorg/apache/index/poi/hssf/record/Record;

    .line 181
    invoke-virtual {v2}, Lorg/apache/index/poi/hssf/record/RecordFactoryInputStream$StreamEncryptionInfo;->hasBOFRecord()Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v3, 0x1

    :goto_0
    iput v3, p0, Lorg/apache/index/poi/hssf/record/RecordFactoryInputStream;->_bofDepth:I

    .line 182
    iput-boolean v4, p0, Lorg/apache/index/poi/hssf/record/RecordFactoryInputStream;->_lastRecordWasEOFLevelZero:Z

    .line 183
    return-void

    :cond_1
    move v3, v4

    .line 181
    goto :goto_0
.end method

.method private getNextUnreadRecord()Lorg/apache/index/poi/hssf/record/Record;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 231
    iget-object v2, p0, Lorg/apache/index/poi/hssf/record/RecordFactoryInputStream;->_unreadRecordBuffer:[Lorg/apache/index/poi/hssf/record/Record;

    if-eqz v2, :cond_0

    .line 232
    iget v0, p0, Lorg/apache/index/poi/hssf/record/RecordFactoryInputStream;->_unreadRecordIndex:I

    .line 233
    .local v0, "ix":I
    iget-object v2, p0, Lorg/apache/index/poi/hssf/record/RecordFactoryInputStream;->_unreadRecordBuffer:[Lorg/apache/index/poi/hssf/record/Record;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 234
    iget-object v2, p0, Lorg/apache/index/poi/hssf/record/RecordFactoryInputStream;->_unreadRecordBuffer:[Lorg/apache/index/poi/hssf/record/Record;

    aget-object v1, v2, v0

    .line 235
    .local v1, "result":Lorg/apache/index/poi/hssf/record/Record;
    add-int/lit8 v2, v0, 0x1

    iput v2, p0, Lorg/apache/index/poi/hssf/record/RecordFactoryInputStream;->_unreadRecordIndex:I

    .line 241
    .end local v0    # "ix":I
    .end local v1    # "result":Lorg/apache/index/poi/hssf/record/Record;
    :cond_0
    :goto_0
    return-object v1

    .line 238
    .restart local v0    # "ix":I
    :cond_1
    const/4 v2, -0x1

    iput v2, p0, Lorg/apache/index/poi/hssf/record/RecordFactoryInputStream;->_unreadRecordIndex:I

    .line 239
    iput-object v1, p0, Lorg/apache/index/poi/hssf/record/RecordFactoryInputStream;->_unreadRecordBuffer:[Lorg/apache/index/poi/hssf/record/Record;

    goto :goto_0
.end method

.method private readNextRecord(Lcom/samsung/index/ITextContentObs;)Lorg/apache/index/poi/hssf/record/Record;
    .locals 3
    .param p1, "indexWriterhssf"    # Lcom/samsung/index/ITextContentObs;

    .prologue
    const/4 v2, 0x1

    .line 251
    iget-object v1, p0, Lorg/apache/index/poi/hssf/record/RecordFactoryInputStream;->_recStream:Lorg/apache/index/poi/hssf/record/RecordInputStream;

    invoke-static {v1, p1}, Lorg/apache/index/poi/hssf/record/RecordFactory;->createSingleRecord(Lorg/apache/index/poi/hssf/record/RecordInputStream;Lcom/samsung/index/ITextContentObs;)Lorg/apache/index/poi/hssf/record/Record;

    move-result-object v0

    .line 252
    .local v0, "record":Lorg/apache/index/poi/hssf/record/Record;
    const/4 v1, 0x0

    iput-boolean v1, p0, Lorg/apache/index/poi/hssf/record/RecordFactoryInputStream;->_lastRecordWasEOFLevelZero:Z

    .line 254
    instance-of v1, v0, Lorg/apache/index/poi/hssf/record/BOFRecord;

    if-eqz v1, :cond_1

    .line 255
    iget v1, p0, Lorg/apache/index/poi/hssf/record/RecordFactoryInputStream;->_bofDepth:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/apache/index/poi/hssf/record/RecordFactoryInputStream;->_bofDepth:I

    .line 329
    :cond_0
    :goto_0
    return-object v0

    .line 259
    :cond_1
    instance-of v1, v0, Lorg/apache/index/poi/hssf/record/EOFRecord;

    if-eqz v1, :cond_2

    .line 260
    iget v1, p0, Lorg/apache/index/poi/hssf/record/RecordFactoryInputStream;->_bofDepth:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lorg/apache/index/poi/hssf/record/RecordFactoryInputStream;->_bofDepth:I

    .line 261
    iget v1, p0, Lorg/apache/index/poi/hssf/record/RecordFactoryInputStream;->_bofDepth:I

    if-ge v1, v2, :cond_0

    .line 262
    iput-boolean v2, p0, Lorg/apache/index/poi/hssf/record/RecordFactoryInputStream;->_lastRecordWasEOFLevelZero:Z

    goto :goto_0

    .line 325
    :cond_2
    iput-object v0, p0, Lorg/apache/index/poi/hssf/record/RecordFactoryInputStream;->_lastRecord:Lorg/apache/index/poi/hssf/record/Record;

    goto :goto_0
.end method


# virtual methods
.method public nextRecord(Lcom/samsung/index/ITextContentObs;)Lorg/apache/index/poi/hssf/record/Record;
    .locals 4
    .param p1, "indexWriterhssf"    # Lcom/samsung/index/ITextContentObs;

    .prologue
    const/4 v1, 0x0

    .line 191
    invoke-direct {p0}, Lorg/apache/index/poi/hssf/record/RecordFactoryInputStream;->getNextUnreadRecord()Lorg/apache/index/poi/hssf/record/Record;

    move-result-object v0

    .line 192
    .local v0, "r":Lorg/apache/index/poi/hssf/record/Record;
    if-eqz v0, :cond_2

    move-object v1, v0

    .line 222
    :cond_0
    :goto_0
    return-object v1

    .line 215
    :cond_1
    iget-object v2, p0, Lorg/apache/index/poi/hssf/record/RecordFactoryInputStream;->_recStream:Lorg/apache/index/poi/hssf/record/RecordInputStream;

    invoke-virtual {v2}, Lorg/apache/index/poi/hssf/record/RecordInputStream;->nextRecord()V

    .line 217
    invoke-direct {p0, p1}, Lorg/apache/index/poi/hssf/record/RecordFactoryInputStream;->readNextRecord(Lcom/samsung/index/ITextContentObs;)Lorg/apache/index/poi/hssf/record/Record;

    move-result-object v0

    .line 218
    if-nez v0, :cond_3

    .line 197
    :cond_2
    iget-object v2, p0, Lorg/apache/index/poi/hssf/record/RecordFactoryInputStream;->_recStream:Lorg/apache/index/poi/hssf/record/RecordInputStream;

    invoke-virtual {v2}, Lorg/apache/index/poi/hssf/record/RecordInputStream;->hasNextRecord()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 202
    iget-boolean v2, p0, Lorg/apache/index/poi/hssf/record/RecordFactoryInputStream;->_lastRecordWasEOFLevelZero:Z

    if-eqz v2, :cond_1

    .line 208
    iget-object v2, p0, Lorg/apache/index/poi/hssf/record/RecordFactoryInputStream;->_recStream:Lorg/apache/index/poi/hssf/record/RecordInputStream;

    invoke-virtual {v2}, Lorg/apache/index/poi/hssf/record/RecordInputStream;->getNextSid()I

    move-result v2

    const/16 v3, 0x809

    if-eq v2, v3, :cond_1

    goto :goto_0

    :cond_3
    move-object v1, v0

    .line 222
    goto :goto_0
.end method
