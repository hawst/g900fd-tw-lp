.class public interface abstract Lorg/apache/index/poi/poifs/filesystem/Entry;
.super Ljava/lang/Object;
.source "Entry.java"


# virtual methods
.method public abstract delete()Z
.end method

.method public abstract getName()Ljava/lang/String;
.end method

.method public abstract getParent()Lorg/apache/index/poi/poifs/filesystem/DirectoryEntry;
.end method

.method public abstract isDirectoryEntry()Z
.end method

.method public abstract isDocumentEntry()Z
.end method

.method public abstract renameTo(Ljava/lang/String;)Z
.end method
