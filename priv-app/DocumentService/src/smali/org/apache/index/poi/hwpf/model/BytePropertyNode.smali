.class public abstract Lorg/apache/index/poi/hwpf/model/BytePropertyNode;
.super Lorg/apache/index/poi/hwpf/model/PropertyNode;
.source "BytePropertyNode.java"


# instance fields
.field private final endBytes:I

.field private final startBytes:I


# direct methods
.method public constructor <init>(IILorg/apache/index/poi/hwpf/model/CharIndexTranslator;Ljava/lang/Object;)V
    .locals 2
    .param p1, "fcStart"    # I
    .param p2, "fcEnd"    # I
    .param p3, "translator"    # Lorg/apache/index/poi/hwpf/model/CharIndexTranslator;
    .param p4, "buf"    # Ljava/lang/Object;

    .prologue
    .line 36
    .line 37
    invoke-interface {p3, p1}, Lorg/apache/index/poi/hwpf/model/CharIndexTranslator;->getCharIndex(I)I

    move-result v0

    .line 38
    invoke-interface {p3, p1}, Lorg/apache/index/poi/hwpf/model/CharIndexTranslator;->getCharIndex(I)I

    move-result v1

    invoke-interface {p3, p2, v1}, Lorg/apache/index/poi/hwpf/model/CharIndexTranslator;->getCharIndex(II)I

    move-result v1

    .line 39
    invoke-direct {p0, v0, v1, p4}, Lorg/apache/index/poi/hwpf/model/PropertyNode;-><init>(IILjava/lang/Object;)V

    .line 41
    iput p1, p0, Lorg/apache/index/poi/hwpf/model/BytePropertyNode;->startBytes:I

    .line 42
    iput p2, p0, Lorg/apache/index/poi/hwpf/model/BytePropertyNode;->endBytes:I

    .line 43
    return-void
.end method


# virtual methods
.method public getEndBytes()I
    .locals 1

    .prologue
    .line 50
    iget v0, p0, Lorg/apache/index/poi/hwpf/model/BytePropertyNode;->endBytes:I

    return v0
.end method

.method public getStartBytes()I
    .locals 1

    .prologue
    .line 46
    iget v0, p0, Lorg/apache/index/poi/hwpf/model/BytePropertyNode;->startBytes:I

    return v0
.end method
