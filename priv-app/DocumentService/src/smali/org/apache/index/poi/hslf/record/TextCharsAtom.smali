.class public final Lorg/apache/index/poi/hslf/record/TextCharsAtom;
.super Lorg/apache/index/poi/hslf/record/RecordAtom;
.source "TextCharsAtom.java"


# static fields
.field private static _type:J


# instance fields
.field private _NormalText:Ljava/lang/String;

.field private _header:[B

.field private _text:[B


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 37
    const-wide/16 v0, 0xfa0

    sput-wide v0, Lorg/apache/index/poi/hslf/record/TextCharsAtom;->_type:J

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    .line 95
    invoke-direct {p0}, Lorg/apache/index/poi/hslf/record/RecordAtom;-><init>()V

    .line 97
    const/16 v0, 0x8

    new-array v0, v0, [B

    const/4 v1, 0x2

    const/16 v2, -0x60

    aput-byte v2, v0, v1

    const/4 v1, 0x3

    const/16 v2, 0xf

    aput-byte v2, v0, v1

    iput-object v0, p0, Lorg/apache/index/poi/hslf/record/TextCharsAtom;->_header:[B

    .line 99
    const/4 v0, 0x0

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/apache/index/poi/hslf/record/TextCharsAtom;->_text:[B

    .line 100
    return-void
.end method

.method protected constructor <init>([BII)V
    .locals 5
    .param p1, "source"    # [B
    .param p2, "start"    # I
    .param p3, "len"    # I

    .prologue
    const/4 v4, 0x0

    const/16 v2, 0x8

    .line 74
    invoke-direct {p0}, Lorg/apache/index/poi/hslf/record/RecordAtom;-><init>()V

    .line 76
    if-ge p3, v2, :cond_0

    const/16 p3, 0x8

    .line 79
    :cond_0
    new-array v1, v2, [B

    iput-object v1, p0, Lorg/apache/index/poi/hslf/record/TextCharsAtom;->_header:[B

    .line 80
    iget-object v1, p0, Lorg/apache/index/poi/hslf/record/TextCharsAtom;->_header:[B

    invoke-static {p1, p2, v1, v4, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 83
    add-int/lit8 v1, p3, -0x8

    new-array v1, v1, [B

    iput-object v1, p0, Lorg/apache/index/poi/hslf/record/TextCharsAtom;->_text:[B

    .line 84
    add-int/lit8 v1, p2, 0x8

    iget-object v2, p0, Lorg/apache/index/poi/hslf/record/TextCharsAtom;->_text:[B

    add-int/lit8 v3, p3, -0x8

    invoke-static {p1, v1, v2, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 86
    invoke-virtual {p0}, Lorg/apache/index/poi/hslf/record/TextCharsAtom;->getText()Ljava/lang/String;

    move-result-object v0

    .line 87
    .local v0, "localText":Ljava/lang/String;
    invoke-static {v0}, Lorg/apache/index/poi/hslf/record/Record;->setTextToUser(Ljava/lang/String;)V

    .line 88
    iput-object v0, p0, Lorg/apache/index/poi/hslf/record/TextCharsAtom;->_NormalText:Ljava/lang/String;

    .line 89
    const/4 v1, 0x0

    iput-object v1, p0, Lorg/apache/index/poi/hslf/record/TextCharsAtom;->_text:[B

    .line 91
    return-void
.end method


# virtual methods
.method public getNormalText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/TextCharsAtom;->_NormalText:Ljava/lang/String;

    return-object v0
.end method

.method public getRecordType()J
    .locals 2

    .prologue
    .line 105
    sget-wide v0, Lorg/apache/index/poi/hslf/record/TextCharsAtom;->_type:J

    return-wide v0
.end method

.method public getText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/TextCharsAtom;->_text:[B

    invoke-static {v0}, Lorg/apache/index/poi/util/StringUtil;->getFromUnicodeLE([B)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public setNormalTextNull()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 65
    iput-object v0, p0, Lorg/apache/index/poi/hslf/record/TextCharsAtom;->_NormalText:Ljava/lang/String;

    .line 66
    iput-object v0, p0, Lorg/apache/index/poi/hslf/record/TextCharsAtom;->_text:[B

    .line 67
    return-void
.end method

.method public setText(Ljava/lang/String;)V
    .locals 3
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 51
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    mul-int/lit8 v0, v0, 0x2

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/apache/index/poi/hslf/record/TextCharsAtom;->_text:[B

    .line 52
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/TextCharsAtom;->_text:[B

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lorg/apache/index/poi/util/StringUtil;->putUnicodeLE(Ljava/lang/String;[BI)V

    .line 55
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/TextCharsAtom;->_header:[B

    const/4 v1, 0x4

    iget-object v2, p0, Lorg/apache/index/poi/hslf/record/TextCharsAtom;->_text:[B

    array-length v2, v2

    invoke-static {v0, v1, v2}, Lorg/apache/index/poi/util/LittleEndian;->putInt([BII)V

    .line 56
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 124
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 125
    .local v0, "out":Ljava/lang/StringBuffer;
    const-string/jumbo v1, "TextCharsAtom:\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 126
    iget-object v1, p0, Lorg/apache/index/poi/hslf/record/TextCharsAtom;->_text:[B

    const-wide/16 v2, 0x0

    const/4 v4, 0x0

    invoke-static {v1, v2, v3, v4}, Lorg/apache/index/poi/util/HexDump;->dump([BJI)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 127
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public writeOut(Ljava/io/OutputStream;)V
    .locals 1
    .param p1, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 113
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/TextCharsAtom;->_header:[B

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 116
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/TextCharsAtom;->_text:[B

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 117
    return-void
.end method
