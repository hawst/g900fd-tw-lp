.class public Lorg/apache/index/poi/hpsf/HPSFRuntimeException;
.super Ljava/lang/RuntimeException;
.source "HPSFRuntimeException.java"


# instance fields
.field private reason:Ljava/lang/Throwable;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/RuntimeException;-><init>()V

    .line 46
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 58
    invoke-direct {p0, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    .line 59
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 0
    .param p1, "msg"    # Ljava/lang/String;
    .param p2, "reason"    # Ljava/lang/Throwable;

    .prologue
    .line 88
    invoke-direct {p0, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    .line 89
    iput-object p2, p0, Lorg/apache/index/poi/hpsf/HPSFRuntimeException;->reason:Ljava/lang/Throwable;

    .line 90
    return-void
.end method

.method public constructor <init>(Ljava/lang/Throwable;)V
    .locals 0
    .param p1, "reason"    # Ljava/lang/Throwable;

    .prologue
    .line 72
    invoke-direct {p0}, Ljava/lang/RuntimeException;-><init>()V

    .line 73
    iput-object p1, p0, Lorg/apache/index/poi/hpsf/HPSFRuntimeException;->reason:Ljava/lang/Throwable;

    .line 74
    return-void
.end method


# virtual methods
.method public getReason()Ljava/lang/Throwable;
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lorg/apache/index/poi/hpsf/HPSFRuntimeException;->reason:Ljava/lang/Throwable;

    return-object v0
.end method

.method public printStackTrace()V
    .locals 1

    .prologue
    .line 113
    sget-object v0, Ljava/lang/System;->err:Ljava/io/PrintStream;

    invoke-virtual {p0, v0}, Lorg/apache/index/poi/hpsf/HPSFRuntimeException;->printStackTrace(Ljava/io/PrintStream;)V

    .line 114
    return-void
.end method

.method public printStackTrace(Ljava/io/PrintStream;)V
    .locals 2
    .param p1, "p"    # Ljava/io/PrintStream;

    .prologue
    .line 123
    invoke-virtual {p0}, Lorg/apache/index/poi/hpsf/HPSFRuntimeException;->getReason()Ljava/lang/Throwable;

    move-result-object v0

    .line 124
    .local v0, "reason":Ljava/lang/Throwable;
    invoke-super {p0, p1}, Ljava/lang/RuntimeException;->printStackTrace(Ljava/io/PrintStream;)V

    .line 125
    if-eqz v0, :cond_0

    .line 127
    const-string/jumbo v1, "Caused by:"

    invoke-virtual {p1, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 128
    invoke-virtual {v0, p1}, Ljava/lang/Throwable;->printStackTrace(Ljava/io/PrintStream;)V

    .line 130
    :cond_0
    return-void
.end method

.method public printStackTrace(Ljava/io/PrintWriter;)V
    .locals 2
    .param p1, "p"    # Ljava/io/PrintWriter;

    .prologue
    .line 139
    invoke-virtual {p0}, Lorg/apache/index/poi/hpsf/HPSFRuntimeException;->getReason()Ljava/lang/Throwable;

    move-result-object v0

    .line 140
    .local v0, "reason":Ljava/lang/Throwable;
    invoke-super {p0, p1}, Ljava/lang/RuntimeException;->printStackTrace(Ljava/io/PrintWriter;)V

    .line 141
    if-eqz v0, :cond_0

    .line 143
    const-string/jumbo v1, "Caused by:"

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 144
    invoke-virtual {v0, p1}, Ljava/lang/Throwable;->printStackTrace(Ljava/io/PrintWriter;)V

    .line 146
    :cond_0
    return-void
.end method
