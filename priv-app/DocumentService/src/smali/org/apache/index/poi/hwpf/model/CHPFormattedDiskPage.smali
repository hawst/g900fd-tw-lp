.class public final Lorg/apache/index/poi/hwpf/model/CHPFormattedDiskPage;
.super Lorg/apache/index/poi/hwpf/model/FormattedDiskPage;
.source "CHPFormattedDiskPage.java"


# static fields
.field private static final FC_SIZE:I = 0x4


# instance fields
.field private _chpxList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lorg/apache/index/poi/hwpf/model/CHPX;",
            ">;"
        }
    .end annotation
.end field

.field private _overFlow:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lorg/apache/index/poi/hwpf/model/CHPX;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 49
    invoke-direct {p0}, Lorg/apache/index/poi/hwpf/model/FormattedDiskPage;-><init>()V

    .line 45
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/apache/index/poi/hwpf/model/CHPFormattedDiskPage;->_chpxList:Ljava/util/ArrayList;

    .line 51
    return-void
.end method

.method public constructor <init>([BIILorg/apache/index/poi/hwpf/model/TextPieceTable;)V
    .locals 6
    .param p1, "documentStream"    # [B
    .param p2, "offset"    # I
    .param p3, "fcMin"    # I
    .param p4, "tpt"    # Lorg/apache/index/poi/hwpf/model/TextPieceTable;

    .prologue
    .line 59
    invoke-direct {p0, p1, p2}, Lorg/apache/index/poi/hwpf/model/FormattedDiskPage;-><init>([BI)V

    .line 45
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lorg/apache/index/poi/hwpf/model/CHPFormattedDiskPage;->_chpxList:Ljava/util/ArrayList;

    .line 61
    const/4 v2, 0x0

    .local v2, "x":I
    :goto_0
    iget v3, p0, Lorg/apache/index/poi/hwpf/model/CHPFormattedDiskPage;->_crun:I

    if-lt v2, v3, :cond_0

    .line 72
    return-void

    .line 63
    :cond_0
    invoke-virtual {p0, v2}, Lorg/apache/index/poi/hwpf/model/CHPFormattedDiskPage;->getStart(I)I

    move-result v1

    .line 64
    .local v1, "startAt":I
    invoke-virtual {p0, v2}, Lorg/apache/index/poi/hwpf/model/CHPFormattedDiskPage;->getEnd(I)I

    move-result v0

    .line 66
    .local v0, "endAt":I
    invoke-virtual {p4, v1}, Lorg/apache/index/poi/hwpf/model/TextPieceTable;->isIndexInTable(I)Z

    move-result v3

    if-nez v3, :cond_1

    invoke-virtual {p4, v0}, Lorg/apache/index/poi/hwpf/model/TextPieceTable;->isIndexInTable(I)Z

    move-result v3

    if-nez v3, :cond_1

    .line 67
    iget-object v3, p0, Lorg/apache/index/poi/hwpf/model/CHPFormattedDiskPage;->_chpxList:Ljava/util/ArrayList;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 61
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 69
    :cond_1
    iget-object v3, p0, Lorg/apache/index/poi/hwpf/model/CHPFormattedDiskPage;->_chpxList:Ljava/util/ArrayList;

    new-instance v4, Lorg/apache/index/poi/hwpf/model/CHPX;

    invoke-virtual {p0, v2}, Lorg/apache/index/poi/hwpf/model/CHPFormattedDiskPage;->getGrpprl(I)[B

    move-result-object v5

    invoke-direct {v4, v1, v0, p4, v5}, Lorg/apache/index/poi/hwpf/model/CHPX;-><init>(IILorg/apache/index/poi/hwpf/model/CharIndexTranslator;[B)V

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method


# virtual methods
.method public fill(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/apache/index/poi/hwpf/model/CHPX;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 81
    .local p1, "filler":Ljava/util/List;, "Ljava/util/List<Lorg/apache/index/poi/hwpf/model/CHPX;>;"
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/CHPFormattedDiskPage;->_chpxList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 82
    return-void
.end method

.method public getCHPX(I)Lorg/apache/index/poi/hwpf/model/CHPX;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 76
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/CHPFormattedDiskPage;->_chpxList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/index/poi/hwpf/model/CHPX;

    return-object v0
.end method

.method protected getGrpprl(I)[B
    .locals 7
    .param p1, "index"    # I

    .prologue
    const/4 v6, 0x0

    .line 97
    iget-object v3, p0, Lorg/apache/index/poi/hwpf/model/CHPFormattedDiskPage;->_fkp:[B

    iget v4, p0, Lorg/apache/index/poi/hwpf/model/CHPFormattedDiskPage;->_offset:I

    iget v5, p0, Lorg/apache/index/poi/hwpf/model/CHPFormattedDiskPage;->_crun:I

    add-int/lit8 v5, v5, 0x1

    mul-int/lit8 v5, v5, 0x4

    add-int/2addr v5, p1

    add-int/2addr v4, v5

    invoke-static {v3, v4}, Lorg/apache/index/poi/util/LittleEndian;->getUnsignedByte([BI)I

    move-result v3

    mul-int/lit8 v1, v3, 0x2

    .line 100
    .local v1, "chpxOffset":I
    if-nez v1, :cond_0

    .line 102
    new-array v0, v6, [B

    .line 110
    :goto_0
    return-object v0

    .line 105
    :cond_0
    iget-object v3, p0, Lorg/apache/index/poi/hwpf/model/CHPFormattedDiskPage;->_fkp:[B

    iget v4, p0, Lorg/apache/index/poi/hwpf/model/CHPFormattedDiskPage;->_offset:I

    add-int/2addr v4, v1

    invoke-static {v3, v4}, Lorg/apache/index/poi/util/LittleEndian;->getUnsignedByte([BI)I

    move-result v2

    .line 107
    .local v2, "size":I
    new-array v0, v2, [B

    .line 109
    .local v0, "chpx":[B
    iget-object v3, p0, Lorg/apache/index/poi/hwpf/model/CHPFormattedDiskPage;->_fkp:[B

    iget v4, p0, Lorg/apache/index/poi/hwpf/model/CHPFormattedDiskPage;->_offset:I

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v4, v1

    invoke-static {v3, v4, v0, v6, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_0
.end method

.method public getOverflow()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lorg/apache/index/poi/hwpf/model/CHPX;",
            ">;"
        }
    .end annotation

    .prologue
    .line 86
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/CHPFormattedDiskPage;->_overFlow:Ljava/util/ArrayList;

    return-object v0
.end method

.method protected toByteArray(I)[B
    .locals 14
    .param p1, "fcMin"    # I

    .prologue
    .line 115
    const/16 v11, 0x200

    new-array v0, v11, [B

    .line 116
    .local v0, "buf":[B
    iget-object v11, p0, Lorg/apache/index/poi/hwpf/model/CHPFormattedDiskPage;->_chpxList:Ljava/util/ArrayList;

    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v8

    .line 117
    .local v8, "size":I
    const/16 v5, 0x1ff

    .line 118
    .local v5, "grpprlOffset":I
    const/4 v7, 0x0

    .line 119
    .local v7, "offsetOffset":I
    const/4 v2, 0x0

    .line 122
    .local v2, "fcOffset":I
    const/4 v9, 0x6

    .line 124
    .local v9, "totalSize":I
    const/4 v6, 0x0

    .line 125
    .local v6, "index":I
    :goto_0
    if-lt v6, v8, :cond_2

    .line 148
    :goto_1
    if-eq v6, v8, :cond_0

    .line 150
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    iput-object v11, p0, Lorg/apache/index/poi/hwpf/model/CHPFormattedDiskPage;->_overFlow:Ljava/util/ArrayList;

    .line 151
    iget-object v11, p0, Lorg/apache/index/poi/hwpf/model/CHPFormattedDiskPage;->_overFlow:Ljava/util/ArrayList;

    iget-object v12, p0, Lorg/apache/index/poi/hwpf/model/CHPFormattedDiskPage;->_chpxList:Ljava/util/ArrayList;

    invoke-virtual {v12, v6, v8}, Ljava/util/ArrayList;->subList(II)Ljava/util/List;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 155
    :cond_0
    const/16 v11, 0x1ff

    int-to-byte v12, v6

    aput-byte v12, v0, v11

    .line 157
    mul-int/lit8 v11, v6, 0x4

    add-int/lit8 v7, v11, 0x4

    .line 160
    const/4 v1, 0x0

    .line 161
    .local v1, "chpx":Lorg/apache/index/poi/hwpf/model/CHPX;
    const/4 v10, 0x0

    .local v10, "x":I
    :goto_2
    if-lt v10, v6, :cond_5

    .line 177
    if-eqz v1, :cond_1

    .line 178
    invoke-virtual {v1}, Lorg/apache/index/poi/hwpf/model/CHPX;->getEndBytes()I

    move-result v11

    add-int/2addr v11, p1

    invoke-static {v0, v2, v11}, Lorg/apache/index/poi/util/LittleEndian;->putInt([BII)V

    .line 180
    :cond_1
    return-object v0

    .line 127
    .end local v1    # "chpx":Lorg/apache/index/poi/hwpf/model/CHPX;
    .end local v10    # "x":I
    :cond_2
    iget-object v11, p0, Lorg/apache/index/poi/hwpf/model/CHPFormattedDiskPage;->_chpxList:Ljava/util/ArrayList;

    invoke-virtual {v11, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lorg/apache/index/poi/hwpf/model/CHPX;

    invoke-virtual {v11}, Lorg/apache/index/poi/hwpf/model/CHPX;->getGrpprl()[B

    move-result-object v11

    array-length v4, v11

    .line 131
    .local v4, "grpprlLength":I
    add-int/lit8 v11, v4, 0x6

    add-int/2addr v9, v11

    .line 134
    rem-int/lit8 v11, v6, 0x2

    add-int/lit16 v11, v11, 0x1ff

    if-le v9, v11, :cond_3

    .line 136
    add-int/lit8 v11, v4, 0x6

    sub-int/2addr v9, v11

    .line 137
    goto :goto_1

    .line 141
    :cond_3
    add-int/lit8 v11, v4, 0x1

    rem-int/lit8 v11, v11, 0x2

    if-lez v11, :cond_4

    .line 143
    add-int/lit8 v9, v9, 0x1

    .line 125
    :cond_4
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 163
    .end local v4    # "grpprlLength":I
    .restart local v1    # "chpx":Lorg/apache/index/poi/hwpf/model/CHPX;
    .restart local v10    # "x":I
    :cond_5
    iget-object v11, p0, Lorg/apache/index/poi/hwpf/model/CHPFormattedDiskPage;->_chpxList:Ljava/util/ArrayList;

    invoke-virtual {v11, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "chpx":Lorg/apache/index/poi/hwpf/model/CHPX;
    check-cast v1, Lorg/apache/index/poi/hwpf/model/CHPX;

    .line 164
    .restart local v1    # "chpx":Lorg/apache/index/poi/hwpf/model/CHPX;
    invoke-virtual {v1}, Lorg/apache/index/poi/hwpf/model/CHPX;->getGrpprl()[B

    move-result-object v3

    .line 166
    .local v3, "grpprl":[B
    invoke-virtual {v1}, Lorg/apache/index/poi/hwpf/model/CHPX;->getStartBytes()I

    move-result v11

    add-int/2addr v11, p1

    invoke-static {v0, v2, v11}, Lorg/apache/index/poi/util/LittleEndian;->putInt([BII)V

    .line 167
    array-length v11, v3

    add-int/lit8 v11, v11, 0x1

    sub-int/2addr v5, v11

    .line 168
    rem-int/lit8 v11, v5, 0x2

    sub-int/2addr v5, v11

    .line 169
    div-int/lit8 v11, v5, 0x2

    int-to-byte v11, v11

    aput-byte v11, v0, v7

    .line 170
    array-length v11, v3

    int-to-byte v11, v11

    aput-byte v11, v0, v5

    .line 171
    const/4 v11, 0x0

    add-int/lit8 v12, v5, 0x1

    array-length v13, v3

    invoke-static {v3, v11, v0, v12, v13}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 173
    add-int/lit8 v7, v7, 0x1

    .line 174
    add-int/lit8 v2, v2, 0x4

    .line 161
    add-int/lit8 v10, v10, 0x1

    goto :goto_2
.end method
