.class public final Lorg/apache/index/poi/hslf/record/Comment2000;
.super Lorg/apache/index/poi/hslf/record/RecordContainer;
.source "Comment2000.java"


# static fields
.field private static _type:J


# instance fields
.field private _header:[B

.field private authorInitialsRecord:Lorg/apache/index/poi/hslf/record/CString;

.field private authorRecord:Lorg/apache/index/poi/hslf/record/CString;

.field private commentAtom:Lorg/apache/index/poi/hslf/record/Comment2000Atom;

.field private commentRecord:Lorg/apache/index/poi/hslf/record/CString;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 30
    const-wide/16 v0, 0x2ee0

    sput-wide v0, Lorg/apache/index/poi/hslf/record/Comment2000;->_type:J

    return-void
.end method

.method public constructor <init>()V
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x0

    .line 137
    invoke-direct {p0}, Lorg/apache/index/poi/hslf/record/RecordContainer;-><init>()V

    .line 138
    const/16 v3, 0x8

    new-array v3, v3, [B

    iput-object v3, p0, Lorg/apache/index/poi/hslf/record/Comment2000;->_header:[B

    .line 139
    const/4 v3, 0x4

    new-array v3, v3, [Lorg/apache/index/poi/hslf/record/Record;

    iput-object v3, p0, Lorg/apache/index/poi/hslf/record/Comment2000;->_children:[Lorg/apache/index/poi/hslf/record/Record;

    .line 142
    iget-object v3, p0, Lorg/apache/index/poi/hslf/record/Comment2000;->_header:[B

    const/16 v4, 0xf

    aput-byte v4, v3, v6

    .line 143
    iget-object v3, p0, Lorg/apache/index/poi/hslf/record/Comment2000;->_header:[B

    sget-wide v4, Lorg/apache/index/poi/hslf/record/Comment2000;->_type:J

    long-to-int v4, v4

    int-to-short v4, v4

    invoke-static {v3, v7, v4}, Lorg/apache/index/poi/util/LittleEndian;->putShort([BIS)V

    .line 146
    new-instance v0, Lorg/apache/index/poi/hslf/record/CString;

    invoke-direct {v0}, Lorg/apache/index/poi/hslf/record/CString;-><init>()V

    .line 147
    .local v0, "csa":Lorg/apache/index/poi/hslf/record/CString;
    new-instance v1, Lorg/apache/index/poi/hslf/record/CString;

    invoke-direct {v1}, Lorg/apache/index/poi/hslf/record/CString;-><init>()V

    .line 148
    .local v1, "csb":Lorg/apache/index/poi/hslf/record/CString;
    new-instance v2, Lorg/apache/index/poi/hslf/record/CString;

    invoke-direct {v2}, Lorg/apache/index/poi/hslf/record/CString;-><init>()V

    .line 149
    .local v2, "csc":Lorg/apache/index/poi/hslf/record/CString;
    invoke-virtual {v0, v6}, Lorg/apache/index/poi/hslf/record/CString;->setOptions(I)V

    .line 150
    const/16 v3, 0x10

    invoke-virtual {v1, v3}, Lorg/apache/index/poi/hslf/record/CString;->setOptions(I)V

    .line 151
    const/16 v3, 0x20

    invoke-virtual {v2, v3}, Lorg/apache/index/poi/hslf/record/CString;->setOptions(I)V

    .line 152
    iget-object v3, p0, Lorg/apache/index/poi/hslf/record/Comment2000;->_children:[Lorg/apache/index/poi/hslf/record/Record;

    aput-object v0, v3, v6

    .line 153
    iget-object v3, p0, Lorg/apache/index/poi/hslf/record/Comment2000;->_children:[Lorg/apache/index/poi/hslf/record/Record;

    const/4 v4, 0x1

    aput-object v1, v3, v4

    .line 154
    iget-object v3, p0, Lorg/apache/index/poi/hslf/record/Comment2000;->_children:[Lorg/apache/index/poi/hslf/record/Record;

    aput-object v2, v3, v7

    .line 155
    iget-object v3, p0, Lorg/apache/index/poi/hslf/record/Comment2000;->_children:[Lorg/apache/index/poi/hslf/record/Record;

    const/4 v4, 0x3

    new-instance v5, Lorg/apache/index/poi/hslf/record/Comment2000Atom;

    invoke-direct {v5}, Lorg/apache/index/poi/hslf/record/Comment2000Atom;-><init>()V

    aput-object v5, v3, v4

    .line 156
    invoke-direct {p0}, Lorg/apache/index/poi/hslf/record/Comment2000;->findInterestingChildren()V

    .line 157
    return-void
.end method

.method protected constructor <init>([BII)V
    .locals 3
    .param p1, "source"    # [B
    .param p2, "start"    # I
    .param p3, "len"    # I

    .prologue
    const/16 v2, 0x8

    .line 99
    invoke-direct {p0}, Lorg/apache/index/poi/hslf/record/RecordContainer;-><init>()V

    .line 101
    new-array v0, v2, [B

    iput-object v0, p0, Lorg/apache/index/poi/hslf/record/Comment2000;->_header:[B

    .line 102
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/Comment2000;->_header:[B

    const/4 v1, 0x0

    invoke-static {p1, p2, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 105
    add-int/lit8 v0, p2, 0x8

    add-int/lit8 v1, p3, -0x8

    invoke-static {p1, v0, v1}, Lorg/apache/index/poi/hslf/record/Record;->findChildRecords([BII)[Lorg/apache/index/poi/hslf/record/Record;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/index/poi/hslf/record/Comment2000;->_children:[Lorg/apache/index/poi/hslf/record/Record;

    .line 106
    invoke-direct {p0}, Lorg/apache/index/poi/hslf/record/Comment2000;->findInterestingChildren()V

    .line 107
    return-void
.end method

.method private findInterestingChildren()V
    .locals 12

    .prologue
    .line 116
    iget-object v4, p0, Lorg/apache/index/poi/hslf/record/Comment2000;->_children:[Lorg/apache/index/poi/hslf/record/Record;

    array-length v5, v4

    const/4 v3, 0x0

    :goto_0
    if-lt v3, v5, :cond_0

    .line 132
    return-void

    .line 116
    :cond_0
    aget-object v1, v4, v3

    .line 117
    .local v1, "r":Lorg/apache/index/poi/hslf/record/Record;
    instance-of v6, v1, Lorg/apache/index/poi/hslf/record/CString;

    if-eqz v6, :cond_1

    move-object v0, v1

    .line 118
    check-cast v0, Lorg/apache/index/poi/hslf/record/CString;

    .line 119
    .local v0, "cs":Lorg/apache/index/poi/hslf/record/CString;
    invoke-virtual {v0}, Lorg/apache/index/poi/hslf/record/CString;->getOptions()I

    move-result v6

    shr-int/lit8 v2, v6, 0x4

    .line 120
    .local v2, "recInstance":I
    packed-switch v2, :pswitch_data_0

    .line 116
    .end local v0    # "cs":Lorg/apache/index/poi/hslf/record/CString;
    .end local v1    # "r":Lorg/apache/index/poi/hslf/record/Record;
    .end local v2    # "recInstance":I
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 121
    .restart local v0    # "cs":Lorg/apache/index/poi/hslf/record/CString;
    .restart local v1    # "r":Lorg/apache/index/poi/hslf/record/Record;
    .restart local v2    # "recInstance":I
    :pswitch_0
    iput-object v0, p0, Lorg/apache/index/poi/hslf/record/Comment2000;->authorRecord:Lorg/apache/index/poi/hslf/record/CString;

    goto :goto_1

    .line 122
    :pswitch_1
    iput-object v0, p0, Lorg/apache/index/poi/hslf/record/Comment2000;->commentRecord:Lorg/apache/index/poi/hslf/record/CString;

    goto :goto_1

    .line 123
    :pswitch_2
    iput-object v0, p0, Lorg/apache/index/poi/hslf/record/Comment2000;->authorInitialsRecord:Lorg/apache/index/poi/hslf/record/CString;

    goto :goto_1

    .line 125
    .end local v0    # "cs":Lorg/apache/index/poi/hslf/record/CString;
    .end local v2    # "recInstance":I
    :cond_1
    instance-of v6, v1, Lorg/apache/index/poi/hslf/record/Comment2000Atom;

    if-eqz v6, :cond_2

    .line 126
    check-cast v1, Lorg/apache/index/poi/hslf/record/Comment2000Atom;

    .end local v1    # "r":Lorg/apache/index/poi/hslf/record/Record;
    iput-object v1, p0, Lorg/apache/index/poi/hslf/record/Comment2000;->commentAtom:Lorg/apache/index/poi/hslf/record/Comment2000Atom;

    goto :goto_1

    .line 128
    .restart local v1    # "r":Lorg/apache/index/poi/hslf/record/Record;
    :cond_2
    iget-object v6, p0, Lorg/apache/index/poi/hslf/record/Comment2000;->logger:Lorg/apache/index/poi/util/POILogger;

    sget v7, Lorg/apache/index/poi/util/POILogger;->WARN:I

    new-instance v8, Ljava/lang/StringBuilder;

    const-string/jumbo v9, "Unexpected record with type="

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lorg/apache/index/poi/hslf/record/Record;->getRecordType()J

    move-result-wide v10

    invoke-virtual {v8, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, " in Comment2000: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Lorg/apache/index/poi/util/POILogger;->log(ILjava/lang/Object;)V

    goto :goto_1

    .line 120
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public getAuthor()Ljava/lang/String;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/Comment2000;->authorRecord:Lorg/apache/index/poi/hslf/record/CString;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/Comment2000;->authorRecord:Lorg/apache/index/poi/hslf/record/CString;

    invoke-virtual {v0}, Lorg/apache/index/poi/hslf/record/CString;->getText()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getAuthorInitials()Ljava/lang/String;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/Comment2000;->authorInitialsRecord:Lorg/apache/index/poi/hslf/record/CString;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/Comment2000;->authorInitialsRecord:Lorg/apache/index/poi/hslf/record/CString;

    invoke-virtual {v0}, Lorg/apache/index/poi/hslf/record/CString;->getText()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getComment2000Atom()Lorg/apache/index/poi/hslf/record/Comment2000Atom;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/Comment2000;->commentAtom:Lorg/apache/index/poi/hslf/record/Comment2000Atom;

    return-object v0
.end method

.method public getRecordType()J
    .locals 2

    .prologue
    .line 162
    sget-wide v0, Lorg/apache/index/poi/hslf/record/Comment2000;->_type:J

    return-wide v0
.end method

.method public getText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/Comment2000;->commentRecord:Lorg/apache/index/poi/hslf/record/CString;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/Comment2000;->commentRecord:Lorg/apache/index/poi/hslf/record/CString;

    invoke-virtual {v0}, Lorg/apache/index/poi/hslf/record/CString;->getText()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public setAuthor(Ljava/lang/String;)V
    .locals 1
    .param p1, "author"    # Ljava/lang/String;

    .prologue
    .line 67
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/Comment2000;->authorRecord:Lorg/apache/index/poi/hslf/record/CString;

    invoke-virtual {v0, p1}, Lorg/apache/index/poi/hslf/record/CString;->setText(Ljava/lang/String;)V

    .line 68
    return-void
.end method

.method public setAuthorInitials(Ljava/lang/String;)V
    .locals 1
    .param p1, "initials"    # Ljava/lang/String;

    .prologue
    .line 80
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/Comment2000;->authorInitialsRecord:Lorg/apache/index/poi/hslf/record/CString;

    invoke-virtual {v0, p1}, Lorg/apache/index/poi/hslf/record/CString;->setText(Ljava/lang/String;)V

    .line 81
    return-void
.end method

.method public setText(Ljava/lang/String;)V
    .locals 1
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 93
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/Comment2000;->commentRecord:Lorg/apache/index/poi/hslf/record/CString;

    invoke-virtual {v0, p1}, Lorg/apache/index/poi/hslf/record/CString;->setText(Ljava/lang/String;)V

    .line 94
    return-void
.end method
