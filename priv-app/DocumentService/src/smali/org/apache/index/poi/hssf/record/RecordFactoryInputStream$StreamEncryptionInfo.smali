.class final Lorg/apache/index/poi/hssf/record/RecordFactoryInputStream$StreamEncryptionInfo;
.super Ljava/lang/Object;
.source "RecordFactoryInputStream.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/index/poi/hssf/record/RecordFactoryInputStream;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "StreamEncryptionInfo"
.end annotation


# instance fields
.field private final _hasBOFRecord:Z

.field private final _lastRecord:Lorg/apache/index/poi/hssf/record/Record;


# direct methods
.method public constructor <init>(Lorg/apache/index/poi/hssf/record/RecordInputStream;Ljava/util/List;Lcom/samsung/index/ITextContentObs;)V
    .locals 2
    .param p1, "rs"    # Lorg/apache/index/poi/hssf/record/RecordInputStream;
    .param p3, "indexWriterhssf"    # Lcom/samsung/index/ITextContentObs;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/index/poi/hssf/record/RecordInputStream;",
            "Ljava/util/List",
            "<",
            "Lorg/apache/index/poi/hssf/record/Record;",
            ">;",
            "Lcom/samsung/index/ITextContentObs;",
            ")V"
        }
    .end annotation

    .prologue
    .line 47
    .local p2, "outputRecs":Ljava/util/List;, "Ljava/util/List<Lorg/apache/index/poi/hssf/record/Record;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    invoke-virtual {p1}, Lorg/apache/index/poi/hssf/record/RecordInputStream;->nextRecord()V

    .line 53
    invoke-static {p1, p3}, Lorg/apache/index/poi/hssf/record/RecordFactory;->createSingleRecord(Lorg/apache/index/poi/hssf/record/RecordInputStream;Lcom/samsung/index/ITextContentObs;)Lorg/apache/index/poi/hssf/record/Record;

    move-result-object v0

    .line 54
    .local v0, "rec":Lorg/apache/index/poi/hssf/record/Record;
    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 56
    instance-of v1, v0, Lorg/apache/index/poi/hssf/record/BOFRecord;

    if-eqz v1, :cond_1

    .line 57
    const/4 v1, 0x1

    iput-boolean v1, p0, Lorg/apache/index/poi/hssf/record/RecordFactoryInputStream$StreamEncryptionInfo;->_hasBOFRecord:Z

    .line 58
    invoke-virtual {p1}, Lorg/apache/index/poi/hssf/record/RecordInputStream;->hasNextRecord()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 59
    invoke-virtual {p1}, Lorg/apache/index/poi/hssf/record/RecordInputStream;->nextRecord()V

    .line 60
    invoke-static {p1, p3}, Lorg/apache/index/poi/hssf/record/RecordFactory;->createSingleRecord(Lorg/apache/index/poi/hssf/record/RecordInputStream;Lcom/samsung/index/ITextContentObs;)Lorg/apache/index/poi/hssf/record/Record;

    move-result-object v0

    .line 62
    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 84
    :cond_0
    :goto_0
    iput-object v0, p0, Lorg/apache/index/poi/hssf/record/RecordFactoryInputStream$StreamEncryptionInfo;->_lastRecord:Lorg/apache/index/poi/hssf/record/Record;

    .line 85
    return-void

    .line 81
    :cond_1
    const/4 v1, 0x0

    iput-boolean v1, p0, Lorg/apache/index/poi/hssf/record/RecordFactoryInputStream$StreamEncryptionInfo;->_hasBOFRecord:Z

    goto :goto_0
.end method


# virtual methods
.method public getLastRecord()Lorg/apache/index/poi/hssf/record/Record;
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lorg/apache/index/poi/hssf/record/RecordFactoryInputStream$StreamEncryptionInfo;->_lastRecord:Lorg/apache/index/poi/hssf/record/Record;

    return-object v0
.end method

.method public hasBOFRecord()Z
    .locals 1

    .prologue
    .line 104
    iget-boolean v0, p0, Lorg/apache/index/poi/hssf/record/RecordFactoryInputStream$StreamEncryptionInfo;->_hasBOFRecord:Z

    return v0
.end method
