.class public abstract Lorg/apache/index/poi/hpsf/SpecialPropertySet;
.super Lorg/apache/index/poi/hpsf/MutablePropertySet;
.source "SpecialPropertySet.java"


# instance fields
.field private delegate:Lorg/apache/index/poi/hpsf/MutablePropertySet;


# direct methods
.method public constructor <init>(Lorg/apache/index/poi/hpsf/MutablePropertySet;)V
    .locals 0
    .param p1, "ps"    # Lorg/apache/index/poi/hpsf/MutablePropertySet;

    .prologue
    .line 92
    invoke-direct {p0}, Lorg/apache/index/poi/hpsf/MutablePropertySet;-><init>()V

    .line 94
    iput-object p1, p0, Lorg/apache/index/poi/hpsf/SpecialPropertySet;->delegate:Lorg/apache/index/poi/hpsf/MutablePropertySet;

    .line 95
    return-void
.end method

.method public constructor <init>(Lorg/apache/index/poi/hpsf/PropertySet;)V
    .locals 1
    .param p1, "ps"    # Lorg/apache/index/poi/hpsf/PropertySet;

    .prologue
    .line 79
    invoke-direct {p0}, Lorg/apache/index/poi/hpsf/MutablePropertySet;-><init>()V

    .line 81
    new-instance v0, Lorg/apache/index/poi/hpsf/MutablePropertySet;

    invoke-direct {v0, p1}, Lorg/apache/index/poi/hpsf/MutablePropertySet;-><init>(Lorg/apache/index/poi/hpsf/PropertySet;)V

    iput-object v0, p0, Lorg/apache/index/poi/hpsf/SpecialPropertySet;->delegate:Lorg/apache/index/poi/hpsf/MutablePropertySet;

    .line 82
    return-void
.end method


# virtual methods
.method public addSection(Lorg/apache/index/poi/hpsf/Section;)V
    .locals 1
    .param p1, "section"    # Lorg/apache/index/poi/hpsf/Section;

    .prologue
    .line 193
    iget-object v0, p0, Lorg/apache/index/poi/hpsf/SpecialPropertySet;->delegate:Lorg/apache/index/poi/hpsf/MutablePropertySet;

    invoke-virtual {v0, p1}, Lorg/apache/index/poi/hpsf/MutablePropertySet;->addSection(Lorg/apache/index/poi/hpsf/Section;)V

    .line 194
    return-void
.end method

.method public clearSections()V
    .locals 1

    .prologue
    .line 203
    iget-object v0, p0, Lorg/apache/index/poi/hpsf/SpecialPropertySet;->delegate:Lorg/apache/index/poi/hpsf/MutablePropertySet;

    invoke-virtual {v0}, Lorg/apache/index/poi/hpsf/MutablePropertySet;->clearSections()V

    .line 204
    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 283
    iget-object v0, p0, Lorg/apache/index/poi/hpsf/SpecialPropertySet;->delegate:Lorg/apache/index/poi/hpsf/MutablePropertySet;

    invoke-virtual {v0, p1}, Lorg/apache/index/poi/hpsf/MutablePropertySet;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public getByteOrder()I
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lorg/apache/index/poi/hpsf/SpecialPropertySet;->delegate:Lorg/apache/index/poi/hpsf/MutablePropertySet;

    invoke-virtual {v0}, Lorg/apache/index/poi/hpsf/MutablePropertySet;->getByteOrder()I

    move-result v0

    return v0
.end method

.method public getClassID()Lorg/apache/index/poi/hpsf/ClassID;
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lorg/apache/index/poi/hpsf/SpecialPropertySet;->delegate:Lorg/apache/index/poi/hpsf/MutablePropertySet;

    invoke-virtual {v0}, Lorg/apache/index/poi/hpsf/MutablePropertySet;->getClassID()Lorg/apache/index/poi/hpsf/ClassID;

    move-result-object v0

    return-object v0
.end method

.method public getFirstSection()Lorg/apache/index/poi/hpsf/Section;
    .locals 1

    .prologue
    .line 184
    iget-object v0, p0, Lorg/apache/index/poi/hpsf/SpecialPropertySet;->delegate:Lorg/apache/index/poi/hpsf/MutablePropertySet;

    invoke-virtual {v0}, Lorg/apache/index/poi/hpsf/MutablePropertySet;->getFirstSection()Lorg/apache/index/poi/hpsf/Section;

    move-result-object v0

    return-object v0
.end method

.method public getFormat()I
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, Lorg/apache/index/poi/hpsf/SpecialPropertySet;->delegate:Lorg/apache/index/poi/hpsf/MutablePropertySet;

    invoke-virtual {v0}, Lorg/apache/index/poi/hpsf/MutablePropertySet;->getFormat()I

    move-result v0

    return v0
.end method

.method public getOSVersion()I
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Lorg/apache/index/poi/hpsf/SpecialPropertySet;->delegate:Lorg/apache/index/poi/hpsf/MutablePropertySet;

    invoke-virtual {v0}, Lorg/apache/index/poi/hpsf/MutablePropertySet;->getOSVersion()I

    move-result v0

    return v0
.end method

.method public getProperties()[Lorg/apache/index/poi/hpsf/Property;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/index/poi/hpsf/NoSingleSectionException;
        }
    .end annotation

    .prologue
    .line 293
    iget-object v0, p0, Lorg/apache/index/poi/hpsf/SpecialPropertySet;->delegate:Lorg/apache/index/poi/hpsf/MutablePropertySet;

    invoke-virtual {v0}, Lorg/apache/index/poi/hpsf/MutablePropertySet;->getProperties()[Lorg/apache/index/poi/hpsf/Property;

    move-result-object v0

    return-object v0
.end method

.method protected getProperty(I)Ljava/lang/Object;
    .locals 1
    .param p1, "id"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/index/poi/hpsf/NoSingleSectionException;
        }
    .end annotation

    .prologue
    .line 303
    iget-object v0, p0, Lorg/apache/index/poi/hpsf/SpecialPropertySet;->delegate:Lorg/apache/index/poi/hpsf/MutablePropertySet;

    invoke-virtual {v0, p1}, Lorg/apache/index/poi/hpsf/MutablePropertySet;->getProperty(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method protected getPropertyBooleanValue(I)Z
    .locals 1
    .param p1, "id"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/index/poi/hpsf/NoSingleSectionException;
        }
    .end annotation

    .prologue
    .line 313
    iget-object v0, p0, Lorg/apache/index/poi/hpsf/SpecialPropertySet;->delegate:Lorg/apache/index/poi/hpsf/MutablePropertySet;

    invoke-virtual {v0, p1}, Lorg/apache/index/poi/hpsf/MutablePropertySet;->getPropertyBooleanValue(I)Z

    move-result v0

    return v0
.end method

.method protected getPropertyIntValue(I)I
    .locals 1
    .param p1, "id"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/index/poi/hpsf/NoSingleSectionException;
        }
    .end annotation

    .prologue
    .line 323
    iget-object v0, p0, Lorg/apache/index/poi/hpsf/SpecialPropertySet;->delegate:Lorg/apache/index/poi/hpsf/MutablePropertySet;

    invoke-virtual {v0, p1}, Lorg/apache/index/poi/hpsf/MutablePropertySet;->getPropertyIntValue(I)I

    move-result v0

    return v0
.end method

.method public abstract getPropertySetIDMap()Lorg/apache/index/poi/hpsf/wellknown/PropertyIDMap;
.end method

.method public getSectionCount()I
    .locals 1

    .prologue
    .line 144
    iget-object v0, p0, Lorg/apache/index/poi/hpsf/SpecialPropertySet;->delegate:Lorg/apache/index/poi/hpsf/MutablePropertySet;

    invoke-virtual {v0}, Lorg/apache/index/poi/hpsf/MutablePropertySet;->getSectionCount()I

    move-result v0

    return v0
.end method

.method public getSections()Ljava/util/List;
    .locals 1

    .prologue
    .line 154
    iget-object v0, p0, Lorg/apache/index/poi/hpsf/SpecialPropertySet;->delegate:Lorg/apache/index/poi/hpsf/MutablePropertySet;

    invoke-virtual {v0}, Lorg/apache/index/poi/hpsf/MutablePropertySet;->getSections()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 333
    iget-object v0, p0, Lorg/apache/index/poi/hpsf/SpecialPropertySet;->delegate:Lorg/apache/index/poi/hpsf/MutablePropertySet;

    invoke-virtual {v0}, Lorg/apache/index/poi/hpsf/MutablePropertySet;->hashCode()I

    move-result v0

    return v0
.end method

.method public isDocumentSummaryInformation()Z
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lorg/apache/index/poi/hpsf/SpecialPropertySet;->delegate:Lorg/apache/index/poi/hpsf/MutablePropertySet;

    invoke-virtual {v0}, Lorg/apache/index/poi/hpsf/MutablePropertySet;->isDocumentSummaryInformation()Z

    move-result v0

    return v0
.end method

.method public isSummaryInformation()Z
    .locals 1

    .prologue
    .line 164
    iget-object v0, p0, Lorg/apache/index/poi/hpsf/SpecialPropertySet;->delegate:Lorg/apache/index/poi/hpsf/MutablePropertySet;

    invoke-virtual {v0}, Lorg/apache/index/poi/hpsf/MutablePropertySet;->isSummaryInformation()Z

    move-result v0

    return v0
.end method

.method public setByteOrder(I)V
    .locals 1
    .param p1, "byteOrder"    # I

    .prologue
    .line 213
    iget-object v0, p0, Lorg/apache/index/poi/hpsf/SpecialPropertySet;->delegate:Lorg/apache/index/poi/hpsf/MutablePropertySet;

    invoke-virtual {v0, p1}, Lorg/apache/index/poi/hpsf/MutablePropertySet;->setByteOrder(I)V

    .line 214
    return-void
.end method

.method public setClassID(Lorg/apache/index/poi/hpsf/ClassID;)V
    .locals 1
    .param p1, "classID"    # Lorg/apache/index/poi/hpsf/ClassID;

    .prologue
    .line 223
    iget-object v0, p0, Lorg/apache/index/poi/hpsf/SpecialPropertySet;->delegate:Lorg/apache/index/poi/hpsf/MutablePropertySet;

    invoke-virtual {v0, p1}, Lorg/apache/index/poi/hpsf/MutablePropertySet;->setClassID(Lorg/apache/index/poi/hpsf/ClassID;)V

    .line 224
    return-void
.end method

.method public setFormat(I)V
    .locals 1
    .param p1, "format"    # I

    .prologue
    .line 233
    iget-object v0, p0, Lorg/apache/index/poi/hpsf/SpecialPropertySet;->delegate:Lorg/apache/index/poi/hpsf/MutablePropertySet;

    invoke-virtual {v0, p1}, Lorg/apache/index/poi/hpsf/MutablePropertySet;->setFormat(I)V

    .line 234
    return-void
.end method

.method public setOSVersion(I)V
    .locals 1
    .param p1, "osVersion"    # I

    .prologue
    .line 243
    iget-object v0, p0, Lorg/apache/index/poi/hpsf/SpecialPropertySet;->delegate:Lorg/apache/index/poi/hpsf/MutablePropertySet;

    invoke-virtual {v0, p1}, Lorg/apache/index/poi/hpsf/MutablePropertySet;->setOSVersion(I)V

    .line 244
    return-void
.end method

.method public toInputStream()Ljava/io/InputStream;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/apache/index/poi/hpsf/WritingNotSupportedException;
        }
    .end annotation

    .prologue
    .line 253
    iget-object v0, p0, Lorg/apache/index/poi/hpsf/SpecialPropertySet;->delegate:Lorg/apache/index/poi/hpsf/MutablePropertySet;

    invoke-virtual {v0}, Lorg/apache/index/poi/hpsf/MutablePropertySet;->toInputStream()Ljava/io/InputStream;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 343
    iget-object v0, p0, Lorg/apache/index/poi/hpsf/SpecialPropertySet;->delegate:Lorg/apache/index/poi/hpsf/MutablePropertySet;

    invoke-virtual {v0}, Lorg/apache/index/poi/hpsf/MutablePropertySet;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public wasNull()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/index/poi/hpsf/NoSingleSectionException;
        }
    .end annotation

    .prologue
    .line 353
    iget-object v0, p0, Lorg/apache/index/poi/hpsf/SpecialPropertySet;->delegate:Lorg/apache/index/poi/hpsf/MutablePropertySet;

    invoke-virtual {v0}, Lorg/apache/index/poi/hpsf/MutablePropertySet;->wasNull()Z

    move-result v0

    return v0
.end method

.method public write(Ljava/io/OutputStream;)V
    .locals 1
    .param p1, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/index/poi/hpsf/WritingNotSupportedException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 273
    iget-object v0, p0, Lorg/apache/index/poi/hpsf/SpecialPropertySet;->delegate:Lorg/apache/index/poi/hpsf/MutablePropertySet;

    invoke-virtual {v0, p1}, Lorg/apache/index/poi/hpsf/MutablePropertySet;->write(Ljava/io/OutputStream;)V

    .line 274
    return-void
.end method

.method public write(Lorg/apache/index/poi/poifs/filesystem/DirectoryEntry;Ljava/lang/String;)V
    .locals 1
    .param p1, "dir"    # Lorg/apache/index/poi/poifs/filesystem/DirectoryEntry;
    .param p2, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/index/poi/hpsf/WritingNotSupportedException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 263
    iget-object v0, p0, Lorg/apache/index/poi/hpsf/SpecialPropertySet;->delegate:Lorg/apache/index/poi/hpsf/MutablePropertySet;

    invoke-virtual {v0, p1, p2}, Lorg/apache/index/poi/hpsf/MutablePropertySet;->write(Lorg/apache/index/poi/poifs/filesystem/DirectoryEntry;Ljava/lang/String;)V

    .line 264
    return-void
.end method
