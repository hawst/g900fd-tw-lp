.class public final Lorg/apache/index/poi/hslf/record/UnknownRecordPlaceholder;
.super Lorg/apache/index/poi/hslf/record/RecordAtom;
.source "UnknownRecordPlaceholder.java"


# instance fields
.field private _contents:[B

.field private _type:J


# direct methods
.method protected constructor <init>([BII)V
    .locals 2
    .param p1, "source"    # [B
    .param p2, "start"    # I
    .param p3, "len"    # I

    .prologue
    .line 41
    invoke-direct {p0}, Lorg/apache/index/poi/hslf/record/RecordAtom;-><init>()V

    .line 44
    if-gez p3, :cond_0

    const/4 p3, 0x0

    .line 47
    :cond_0
    new-array v0, p3, [B

    iput-object v0, p0, Lorg/apache/index/poi/hslf/record/UnknownRecordPlaceholder;->_contents:[B

    .line 48
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/UnknownRecordPlaceholder;->_contents:[B

    const/4 v1, 0x0

    invoke-static {p1, p2, v0, v1, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 49
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/UnknownRecordPlaceholder;->_contents:[B

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lorg/apache/index/poi/util/LittleEndian;->getUShort([BI)I

    move-result v0

    int-to-long v0, v0

    iput-wide v0, p0, Lorg/apache/index/poi/hslf/record/UnknownRecordPlaceholder;->_type:J

    .line 50
    return-void
.end method


# virtual methods
.method public getRecordType()J
    .locals 2

    .prologue
    .line 55
    iget-wide v0, p0, Lorg/apache/index/poi/hslf/record/UnknownRecordPlaceholder;->_type:J

    return-wide v0
.end method

.method public writeOut(Ljava/io/OutputStream;)V
    .locals 1
    .param p1, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 62
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/UnknownRecordPlaceholder;->_contents:[B

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 63
    return-void
.end method
