.class public final Lorg/apache/index/poi/hslf/record/Slide;
.super Lorg/apache/index/poi/hslf/record/SheetContainer;
.source "Slide.java"


# static fields
.field private static _type:J


# instance fields
.field private _header:[B

.field private ppDrawing:Lorg/apache/index/poi/hslf/record/PPDrawing;

.field private slideAtom:Lorg/apache/index/poi/hslf/record/SlideAtom;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 32
    const-wide/16 v0, 0x3ee

    sput-wide v0, Lorg/apache/index/poi/hslf/record/Slide;->_type:J

    return-void
.end method

.method public constructor <init>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x0

    .line 76
    invoke-direct {p0}, Lorg/apache/index/poi/hslf/record/SheetContainer;-><init>()V

    .line 77
    const/16 v0, 0x8

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/apache/index/poi/hslf/record/Slide;->_header:[B

    .line 78
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/Slide;->_header:[B

    const/16 v1, 0xf

    invoke-static {v0, v4, v1}, Lorg/apache/index/poi/util/LittleEndian;->putUShort([BII)V

    .line 79
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/Slide;->_header:[B

    sget-wide v2, Lorg/apache/index/poi/hslf/record/Slide;->_type:J

    long-to-int v1, v2

    invoke-static {v0, v5, v1}, Lorg/apache/index/poi/util/LittleEndian;->putUShort([BII)V

    .line 80
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/Slide;->_header:[B

    const/4 v1, 0x4

    invoke-static {v0, v1, v4}, Lorg/apache/index/poi/util/LittleEndian;->putInt([BII)V

    .line 82
    new-instance v0, Lorg/apache/index/poi/hslf/record/SlideAtom;

    invoke-direct {v0}, Lorg/apache/index/poi/hslf/record/SlideAtom;-><init>()V

    iput-object v0, p0, Lorg/apache/index/poi/hslf/record/Slide;->slideAtom:Lorg/apache/index/poi/hslf/record/SlideAtom;

    .line 83
    new-instance v0, Lorg/apache/index/poi/hslf/record/PPDrawing;

    invoke-direct {v0}, Lorg/apache/index/poi/hslf/record/PPDrawing;-><init>()V

    iput-object v0, p0, Lorg/apache/index/poi/hslf/record/Slide;->ppDrawing:Lorg/apache/index/poi/hslf/record/PPDrawing;

    .line 85
    new-array v0, v5, [Lorg/apache/index/poi/hslf/record/Record;

    .line 86
    iget-object v1, p0, Lorg/apache/index/poi/hslf/record/Slide;->slideAtom:Lorg/apache/index/poi/hslf/record/SlideAtom;

    aput-object v1, v0, v4

    const/4 v1, 0x1

    .line 87
    iget-object v2, p0, Lorg/apache/index/poi/hslf/record/Slide;->ppDrawing:Lorg/apache/index/poi/hslf/record/PPDrawing;

    aput-object v2, v0, v1

    .line 85
    iput-object v0, p0, Lorg/apache/index/poi/hslf/record/Slide;->_children:[Lorg/apache/index/poi/hslf/record/Record;

    .line 89
    return-void
.end method

.method protected constructor <init>([BII)V
    .locals 4
    .param p1, "source"    # [B
    .param p2, "start"    # I
    .param p3, "len"    # I

    .prologue
    const/16 v3, 0x8

    .line 53
    invoke-direct {p0}, Lorg/apache/index/poi/hslf/record/SheetContainer;-><init>()V

    .line 55
    new-array v1, v3, [B

    iput-object v1, p0, Lorg/apache/index/poi/hslf/record/Slide;->_header:[B

    .line 56
    iget-object v1, p0, Lorg/apache/index/poi/hslf/record/Slide;->_header:[B

    const/4 v2, 0x0

    invoke-static {p1, p2, v1, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 59
    add-int/lit8 v1, p2, 0x8

    add-int/lit8 v2, p3, -0x8

    invoke-static {p1, v1, v2}, Lorg/apache/index/poi/hslf/record/Record;->findChildRecords([BII)[Lorg/apache/index/poi/hslf/record/Record;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/index/poi/hslf/record/Slide;->_children:[Lorg/apache/index/poi/hslf/record/Record;

    .line 62
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lorg/apache/index/poi/hslf/record/Slide;->_children:[Lorg/apache/index/poi/hslf/record/Record;

    array-length v1, v1

    if-lt v0, v1, :cond_0

    .line 70
    return-void

    .line 63
    :cond_0
    iget-object v1, p0, Lorg/apache/index/poi/hslf/record/Slide;->_children:[Lorg/apache/index/poi/hslf/record/Record;

    aget-object v1, v1, v0

    instance-of v1, v1, Lorg/apache/index/poi/hslf/record/SlideAtom;

    if-eqz v1, :cond_2

    .line 64
    iget-object v1, p0, Lorg/apache/index/poi/hslf/record/Slide;->_children:[Lorg/apache/index/poi/hslf/record/Record;

    aget-object v1, v1, v0

    check-cast v1, Lorg/apache/index/poi/hslf/record/SlideAtom;

    iput-object v1, p0, Lorg/apache/index/poi/hslf/record/Slide;->slideAtom:Lorg/apache/index/poi/hslf/record/SlideAtom;

    .line 62
    :cond_1
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 66
    :cond_2
    iget-object v1, p0, Lorg/apache/index/poi/hslf/record/Slide;->_children:[Lorg/apache/index/poi/hslf/record/Record;

    aget-object v1, v1, v0

    instance-of v1, v1, Lorg/apache/index/poi/hslf/record/PPDrawing;

    if-eqz v1, :cond_1

    .line 67
    iget-object v1, p0, Lorg/apache/index/poi/hslf/record/Slide;->_children:[Lorg/apache/index/poi/hslf/record/Record;

    aget-object v1, v1, v0

    check-cast v1, Lorg/apache/index/poi/hslf/record/PPDrawing;

    iput-object v1, p0, Lorg/apache/index/poi/hslf/record/Slide;->ppDrawing:Lorg/apache/index/poi/hslf/record/PPDrawing;

    goto :goto_1
.end method


# virtual methods
.method public getPPDrawing()Lorg/apache/index/poi/hslf/record/PPDrawing;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/Slide;->ppDrawing:Lorg/apache/index/poi/hslf/record/PPDrawing;

    return-object v0
.end method

.method public getRecordType()J
    .locals 2

    .prologue
    .line 94
    sget-wide v0, Lorg/apache/index/poi/hslf/record/Slide;->_type:J

    return-wide v0
.end method

.method public getSlideAtom()Lorg/apache/index/poi/hslf/record/SlideAtom;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/Slide;->slideAtom:Lorg/apache/index/poi/hslf/record/SlideAtom;

    return-object v0
.end method
