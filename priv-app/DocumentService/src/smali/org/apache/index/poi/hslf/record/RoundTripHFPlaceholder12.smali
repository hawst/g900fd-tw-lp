.class public final Lorg/apache/index/poi/hslf/record/RoundTripHFPlaceholder12;
.super Lorg/apache/index/poi/hslf/record/RecordAtom;
.source "RoundTripHFPlaceholder12.java"


# instance fields
.field private _header:[B

.field private _placeholderId:B


# direct methods
.method protected constructor <init>([BII)V
    .locals 3
    .param p1, "source"    # [B
    .param p2, "start"    # I
    .param p3, "len"    # I

    .prologue
    const/16 v2, 0x8

    .line 50
    invoke-direct {p0}, Lorg/apache/index/poi/hslf/record/RecordAtom;-><init>()V

    .line 52
    new-array v0, v2, [B

    iput-object v0, p0, Lorg/apache/index/poi/hslf/record/RoundTripHFPlaceholder12;->_header:[B

    .line 53
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/RoundTripHFPlaceholder12;->_header:[B

    const/4 v1, 0x0

    invoke-static {p1, p2, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 56
    add-int/lit8 v0, p2, 0x8

    aget-byte v0, p1, v0

    iput-byte v0, p0, Lorg/apache/index/poi/hslf/record/RoundTripHFPlaceholder12;->_placeholderId:B

    .line 57
    return-void
.end method


# virtual methods
.method public getPlaceholderId()I
    .locals 1

    .prologue
    .line 64
    iget-byte v0, p0, Lorg/apache/index/poi/hslf/record/RoundTripHFPlaceholder12;->_placeholderId:B

    return v0
.end method

.method public getRecordType()J
    .locals 2

    .prologue
    .line 79
    sget-object v0, Lorg/apache/index/poi/hslf/record/RecordTypes;->RoundTripHFPlaceholder12:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    iget v0, v0, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;->typeID:I

    int-to-long v0, v0

    return-wide v0
.end method

.method public setPlaceholderId(I)V
    .locals 1
    .param p1, "number"    # I

    .prologue
    .line 72
    int-to-byte v0, p1

    iput-byte v0, p0, Lorg/apache/index/poi/hslf/record/RoundTripHFPlaceholder12;->_placeholderId:B

    .line 73
    return-void
.end method

.method public writeOut(Ljava/io/OutputStream;)V
    .locals 1
    .param p1, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 89
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/RoundTripHFPlaceholder12;->_header:[B

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 90
    iget-byte v0, p0, Lorg/apache/index/poi/hslf/record/RoundTripHFPlaceholder12;->_placeholderId:B

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write(I)V

    .line 91
    return-void
.end method
