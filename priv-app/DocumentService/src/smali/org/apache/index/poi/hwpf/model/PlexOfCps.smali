.class public final Lorg/apache/index/poi/hwpf/model/PlexOfCps;
.super Ljava/lang/Object;
.source "PlexOfCps.java"


# instance fields
.field private _count:I

.field private _props:Ljava/util/ArrayList;

.field private _sizeOfStruct:I


# direct methods
.method public constructor <init>(I)V
    .locals 1
    .param p1, "sizeOfStruct"    # I

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/apache/index/poi/hwpf/model/PlexOfCps;->_props:Ljava/util/ArrayList;

    .line 44
    iput p1, p0, Lorg/apache/index/poi/hwpf/model/PlexOfCps;->_sizeOfStruct:I

    .line 45
    return-void
.end method

.method public constructor <init>([BIII)V
    .locals 3
    .param p1, "buf"    # [B
    .param p2, "start"    # I
    .param p3, "size"    # I
    .param p4, "sizeOfStruct"    # I

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    add-int/lit8 v1, p3, -0x4

    add-int/lit8 v2, p4, 0x4

    div-int/2addr v1, v2

    iput v1, p0, Lorg/apache/index/poi/hwpf/model/PlexOfCps;->_count:I

    .line 59
    iput p4, p0, Lorg/apache/index/poi/hwpf/model/PlexOfCps;->_sizeOfStruct:I

    .line 60
    new-instance v1, Ljava/util/ArrayList;

    iget v2, p0, Lorg/apache/index/poi/hwpf/model/PlexOfCps;->_count:I

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v1, p0, Lorg/apache/index/poi/hwpf/model/PlexOfCps;->_props:Ljava/util/ArrayList;

    .line 62
    const/4 v0, 0x0

    .local v0, "x":I
    :goto_0
    iget v1, p0, Lorg/apache/index/poi/hwpf/model/PlexOfCps;->_count:I

    if-lt v0, v1, :cond_0

    .line 66
    return-void

    .line 64
    :cond_0
    iget-object v1, p0, Lorg/apache/index/poi/hwpf/model/PlexOfCps;->_props:Ljava/util/ArrayList;

    invoke-direct {p0, v0, p1, p2}, Lorg/apache/index/poi/hwpf/model/PlexOfCps;->getProperty(I[BI)Lorg/apache/index/poi/hwpf/model/GenericPropertyNode;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 62
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private getIntOffset(I)I
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 122
    mul-int/lit8 v0, p1, 0x4

    return v0
.end method

.method private getProperty(I[BI)Lorg/apache/index/poi/hwpf/model/GenericPropertyNode;
    .locals 6
    .param p1, "index"    # I
    .param p2, "buf"    # [B
    .param p3, "offset"    # I

    .prologue
    .line 111
    invoke-direct {p0, p1}, Lorg/apache/index/poi/hwpf/model/PlexOfCps;->getIntOffset(I)I

    move-result v3

    add-int/2addr v3, p3

    invoke-static {p2, v3}, Lorg/apache/index/poi/util/LittleEndian;->getInt([BI)I

    move-result v1

    .line 112
    .local v1, "start":I
    add-int/lit8 v3, p1, 0x1

    invoke-direct {p0, v3}, Lorg/apache/index/poi/hwpf/model/PlexOfCps;->getIntOffset(I)I

    move-result v3

    add-int/2addr v3, p3

    invoke-static {p2, v3}, Lorg/apache/index/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    .line 114
    .local v0, "end":I
    iget v3, p0, Lorg/apache/index/poi/hwpf/model/PlexOfCps;->_sizeOfStruct:I

    new-array v2, v3, [B

    .line 115
    .local v2, "struct":[B
    invoke-direct {p0, p1}, Lorg/apache/index/poi/hwpf/model/PlexOfCps;->getStructOffset(I)I

    move-result v3

    add-int/2addr v3, p3

    const/4 v4, 0x0

    iget v5, p0, Lorg/apache/index/poi/hwpf/model/PlexOfCps;->_sizeOfStruct:I

    invoke-static {p2, v3, v2, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 117
    new-instance v3, Lorg/apache/index/poi/hwpf/model/GenericPropertyNode;

    invoke-direct {v3, v1, v0, v2}, Lorg/apache/index/poi/hwpf/model/GenericPropertyNode;-><init>(II[B)V

    return-object v3
.end method

.method private getStructOffset(I)I
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 146
    iget v0, p0, Lorg/apache/index/poi/hwpf/model/PlexOfCps;->_count:I

    add-int/lit8 v0, v0, 0x1

    mul-int/lit8 v0, v0, 0x4

    iget v1, p0, Lorg/apache/index/poi/hwpf/model/PlexOfCps;->_sizeOfStruct:I

    mul-int/2addr v1, p1

    add-int/2addr v0, v1

    return v0
.end method


# virtual methods
.method public addProperty(Lorg/apache/index/poi/hwpf/model/GenericPropertyNode;)V
    .locals 1
    .param p1, "node"    # Lorg/apache/index/poi/hwpf/model/GenericPropertyNode;

    .prologue
    .line 75
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/PlexOfCps;->_props:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 76
    return-void
.end method

.method public getProperty(I)Lorg/apache/index/poi/hwpf/model/GenericPropertyNode;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 70
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/PlexOfCps;->_props:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/index/poi/hwpf/model/GenericPropertyNode;

    return-object v0
.end method

.method public length()I
    .locals 1

    .prologue
    .line 132
    iget v0, p0, Lorg/apache/index/poi/hwpf/model/PlexOfCps;->_count:I

    return v0
.end method

.method public toByteArray()[B
    .locals 11

    .prologue
    .line 80
    iget-object v7, p0, Lorg/apache/index/poi/hwpf/model/PlexOfCps;->_props:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v4

    .line 81
    .local v4, "size":I
    add-int/lit8 v7, v4, 0x1

    mul-int/lit8 v2, v7, 0x4

    .line 82
    .local v2, "cpBufSize":I
    iget v7, p0, Lorg/apache/index/poi/hwpf/model/PlexOfCps;->_sizeOfStruct:I

    mul-int v5, v7, v4

    .line 83
    .local v5, "structBufSize":I
    add-int v1, v2, v5

    .line 85
    .local v1, "bufSize":I
    new-array v0, v1, [B

    .line 87
    .local v0, "buf":[B
    const/4 v3, 0x0

    .line 88
    .local v3, "node":Lorg/apache/index/poi/hwpf/model/GenericPropertyNode;
    const/4 v6, 0x0

    .local v6, "x":I
    :goto_0
    if-lt v6, v4, :cond_1

    .line 100
    if-eqz v3, :cond_0

    .line 102
    mul-int/lit8 v7, v4, 0x4

    invoke-virtual {v3}, Lorg/apache/index/poi/hwpf/model/GenericPropertyNode;->getEnd()I

    move-result v8

    invoke-static {v0, v7, v8}, Lorg/apache/index/poi/util/LittleEndian;->putInt([BII)V

    .line 105
    :cond_0
    return-object v0

    .line 90
    :cond_1
    iget-object v7, p0, Lorg/apache/index/poi/hwpf/model/PlexOfCps;->_props:Ljava/util/ArrayList;

    invoke-virtual {v7, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "node":Lorg/apache/index/poi/hwpf/model/GenericPropertyNode;
    check-cast v3, Lorg/apache/index/poi/hwpf/model/GenericPropertyNode;

    .line 93
    .restart local v3    # "node":Lorg/apache/index/poi/hwpf/model/GenericPropertyNode;
    mul-int/lit8 v7, v6, 0x4

    invoke-virtual {v3}, Lorg/apache/index/poi/hwpf/model/GenericPropertyNode;->getStart()I

    move-result v8

    invoke-static {v0, v7, v8}, Lorg/apache/index/poi/util/LittleEndian;->putInt([BII)V

    .line 96
    invoke-virtual {v3}, Lorg/apache/index/poi/hwpf/model/GenericPropertyNode;->getBytes()[B

    move-result-object v7

    const/4 v8, 0x0

    iget v9, p0, Lorg/apache/index/poi/hwpf/model/PlexOfCps;->_sizeOfStruct:I

    mul-int/2addr v9, v6

    add-int/2addr v9, v2

    .line 97
    iget v10, p0, Lorg/apache/index/poi/hwpf/model/PlexOfCps;->_sizeOfStruct:I

    .line 96
    invoke-static {v7, v8, v0, v9, v10}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 88
    add-int/lit8 v6, v6, 0x1

    goto :goto_0
.end method
