.class public Lorg/apache/index/poi/ddf/EscherOptRecord;
.super Lorg/apache/index/poi/ddf/EscherRecord;
.source "EscherOptRecord.java"


# static fields
.field public static final RECORD_DESCRIPTION:Ljava/lang/String; = "msofbtOPT"

.field public static final RECORD_ID:S = -0xff5s


# instance fields
.field private properties:Ljava/util/List;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0}, Lorg/apache/index/poi/ddf/EscherRecord;-><init>()V

    .line 43
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/apache/index/poi/ddf/EscherOptRecord;->properties:Ljava/util/List;

    .line 37
    return-void
.end method

.method private getPropertiesSize()I
    .locals 4

    .prologue
    .line 96
    const/4 v2, 0x0

    .line 97
    .local v2, "totalSize":I
    iget-object v3, p0, Lorg/apache/index/poi/ddf/EscherOptRecord;->properties:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "iterator":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_0

    .line 102
    return v2

    .line 99
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/index/poi/ddf/EscherProperty;

    .line 100
    .local v0, "escherProperty":Lorg/apache/index/poi/ddf/EscherProperty;
    invoke-virtual {v0}, Lorg/apache/index/poi/ddf/EscherProperty;->getPropertySize()I

    move-result v3

    add-int/2addr v2, v3

    goto :goto_0
.end method


# virtual methods
.method public addEscherProperty(Lorg/apache/index/poi/ddf/EscherProperty;)V
    .locals 1
    .param p1, "prop"    # Lorg/apache/index/poi/ddf/EscherProperty;

    .prologue
    .line 147
    iget-object v0, p0, Lorg/apache/index/poi/ddf/EscherOptRecord;->properties:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 148
    return-void
.end method

.method public fillFields([BILorg/apache/index/poi/ddf/EscherRecordFactory;)I
    .locals 4
    .param p1, "data"    # [B
    .param p2, "offset"    # I
    .param p3, "recordFactory"    # Lorg/apache/index/poi/ddf/EscherRecordFactory;

    .prologue
    .line 46
    invoke-virtual {p0, p1, p2}, Lorg/apache/index/poi/ddf/EscherOptRecord;->readHeader([BI)I

    move-result v0

    .line 47
    .local v0, "bytesRemaining":I
    add-int/lit8 v2, p2, 0x8

    .line 49
    .local v2, "pos":I
    new-instance v1, Lorg/apache/index/poi/ddf/EscherPropertyFactory;

    invoke-direct {v1}, Lorg/apache/index/poi/ddf/EscherPropertyFactory;-><init>()V

    .line 50
    .local v1, "f":Lorg/apache/index/poi/ddf/EscherPropertyFactory;
    invoke-virtual {p0}, Lorg/apache/index/poi/ddf/EscherOptRecord;->getInstance()S

    move-result v3

    invoke-virtual {v1, p1, v2, v3}, Lorg/apache/index/poi/ddf/EscherPropertyFactory;->createProperties([BIS)Ljava/util/List;

    move-result-object v3

    iput-object v3, p0, Lorg/apache/index/poi/ddf/EscherOptRecord;->properties:Ljava/util/List;

    .line 51
    add-int/lit8 v3, v0, 0x8

    return v3
.end method

.method public getEscherProperties()Ljava/util/List;
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, Lorg/apache/index/poi/ddf/EscherOptRecord;->properties:Ljava/util/List;

    return-object v0
.end method

.method public getEscherProperty(I)Lorg/apache/index/poi/ddf/EscherProperty;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 139
    iget-object v0, p0, Lorg/apache/index/poi/ddf/EscherOptRecord;->properties:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/index/poi/ddf/EscherProperty;

    return-object v0
.end method

.method public getOptions()S
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lorg/apache/index/poi/ddf/EscherOptRecord;->properties:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    shl-int/lit8 v0, v0, 0x4

    or-int/lit8 v0, v0, 0x3

    int-to-short v0, v0

    invoke-virtual {p0, v0}, Lorg/apache/index/poi/ddf/EscherOptRecord;->setOptions(S)V

    .line 87
    invoke-super {p0}, Lorg/apache/index/poi/ddf/EscherRecord;->getOptions()S

    move-result v0

    return v0
.end method

.method public getRecordName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 91
    const-string/jumbo v0, "Opt"

    return-object v0
.end method

.method public getRecordSize()I
    .locals 1

    .prologue
    .line 78
    invoke-direct {p0}, Lorg/apache/index/poi/ddf/EscherOptRecord;->getPropertiesSize()I

    move-result v0

    add-int/lit8 v0, v0, 0x8

    return v0
.end method

.method public serialize(I[BLorg/apache/index/poi/ddf/EscherSerializationListener;)I
    .locals 5
    .param p1, "offset"    # I
    .param p2, "data"    # [B
    .param p3, "listener"    # Lorg/apache/index/poi/ddf/EscherSerializationListener;

    .prologue
    .line 56
    invoke-virtual {p0}, Lorg/apache/index/poi/ddf/EscherOptRecord;->getRecordId()S

    move-result v3

    invoke-interface {p3, p1, v3, p0}, Lorg/apache/index/poi/ddf/EscherSerializationListener;->beforeRecordSerialize(ISLorg/apache/index/poi/ddf/EscherRecord;)V

    .line 58
    invoke-virtual {p0}, Lorg/apache/index/poi/ddf/EscherOptRecord;->getOptions()S

    move-result v3

    invoke-static {p2, p1, v3}, Lorg/apache/index/poi/util/LittleEndian;->putShort([BIS)V

    .line 59
    add-int/lit8 v3, p1, 0x2

    invoke-virtual {p0}, Lorg/apache/index/poi/ddf/EscherOptRecord;->getRecordId()S

    move-result v4

    invoke-static {p2, v3, v4}, Lorg/apache/index/poi/util/LittleEndian;->putShort([BIS)V

    .line 60
    add-int/lit8 v3, p1, 0x4

    invoke-direct {p0}, Lorg/apache/index/poi/ddf/EscherOptRecord;->getPropertiesSize()I

    move-result v4

    invoke-static {p2, v3, v4}, Lorg/apache/index/poi/util/LittleEndian;->putInt([BII)V

    .line 61
    add-int/lit8 v2, p1, 0x8

    .line 62
    .local v2, "pos":I
    iget-object v3, p0, Lorg/apache/index/poi/ddf/EscherOptRecord;->properties:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "iterator":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_0

    .line 67
    iget-object v3, p0, Lorg/apache/index/poi/ddf/EscherOptRecord;->properties:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_1

    .line 72
    invoke-virtual {p0}, Lorg/apache/index/poi/ddf/EscherOptRecord;->getRecordId()S

    move-result v3

    sub-int v4, v2, p1

    invoke-interface {p3, v2, v3, v4, p0}, Lorg/apache/index/poi/ddf/EscherSerializationListener;->afterRecordSerialize(ISILorg/apache/index/poi/ddf/EscherRecord;)V

    .line 73
    sub-int v3, v2, p1

    return v3

    .line 64
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/index/poi/ddf/EscherProperty;

    .line 65
    .local v0, "escherProperty":Lorg/apache/index/poi/ddf/EscherProperty;
    invoke-virtual {v0, p2, v2}, Lorg/apache/index/poi/ddf/EscherProperty;->serializeSimplePart([BI)I

    move-result v3

    add-int/2addr v2, v3

    goto :goto_0

    .line 69
    .end local v0    # "escherProperty":Lorg/apache/index/poi/ddf/EscherProperty;
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/index/poi/ddf/EscherProperty;

    .line 70
    .restart local v0    # "escherProperty":Lorg/apache/index/poi/ddf/EscherProperty;
    invoke-virtual {v0, p2, v2}, Lorg/apache/index/poi/ddf/EscherProperty;->serializeComplexPart([BI)I

    move-result v3

    add-int/2addr v2, v3

    goto :goto_1
.end method

.method public sortProperties()V
    .locals 2

    .prologue
    .line 155
    iget-object v0, p0, Lorg/apache/index/poi/ddf/EscherOptRecord;->properties:Ljava/util/List;

    new-instance v1, Lorg/apache/index/poi/ddf/EscherOptRecord$1;

    invoke-direct {v1, p0}, Lorg/apache/index/poi/ddf/EscherOptRecord$1;-><init>(Lorg/apache/index/poi/ddf/EscherOptRecord;)V

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 164
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 110
    const-string/jumbo v3, "line.separator"

    invoke-static {v3}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 111
    .local v1, "nl":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    .line 112
    .local v2, "propertiesBuf":Ljava/lang/StringBuffer;
    iget-object v3, p0, Lorg/apache/index/poi/ddf/EscherOptRecord;->properties:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "iterator":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_0

    .line 117
    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "org.apache.poi.ddf.EscherOptRecord:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 118
    const-string/jumbo v4, "  isContainer: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lorg/apache/index/poi/ddf/EscherOptRecord;->isContainerRecord()Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 119
    const-string/jumbo v4, "  options: 0x"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lorg/apache/index/poi/ddf/EscherOptRecord;->getOptions()S

    move-result v4

    invoke-static {v4}, Lorg/apache/index/poi/util/HexDump;->toHex(S)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 120
    const-string/jumbo v4, "  recordId: 0x"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lorg/apache/index/poi/ddf/EscherOptRecord;->getRecordId()S

    move-result v4

    invoke-static {v4}, Lorg/apache/index/poi/util/HexDump;->toHex(S)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 121
    const-string/jumbo v4, "  numchildren: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lorg/apache/index/poi/ddf/EscherOptRecord;->getChildRecords()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 122
    const-string/jumbo v4, "  properties:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 123
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 117
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3

    .line 113
    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "    "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 114
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 115
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 113
    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto/16 :goto_0
.end method
