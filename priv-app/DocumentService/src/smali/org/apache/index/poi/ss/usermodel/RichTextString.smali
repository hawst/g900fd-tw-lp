.class public interface abstract Lorg/apache/index/poi/ss/usermodel/RichTextString;
.super Ljava/lang/Object;
.source "RichTextString.java"


# virtual methods
.method public abstract clearFormatting()V
.end method

.method public abstract getIndexOfFormattingRun(I)I
.end method

.method public abstract getString()Ljava/lang/String;
.end method

.method public abstract length()I
.end method

.method public abstract numFormattingRuns()I
.end method
