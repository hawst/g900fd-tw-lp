.class public final Lorg/apache/index/poi/hwpf/model/FieldDescriptor;
.super Ljava/lang/Object;
.source "FieldDescriptor.java"


# static fields
.field private static final BOUNDARY_MASK:S = 0x1fs

.field public static final FIELD_BEGIN_MARK:I = 0x13

.field public static final FIELD_END_MARK:I = 0x15

.field public static final FIELD_SEPARATOR_MARK:I = 0x14

.field private static final HAS_SEP_MASK:S = 0x80s

.field private static final LOCKED_MASK:S = 0x10s

.field private static final NESTED_MASK:S = 0x40s

.field private static final PRIVATE_RESULT_MASK:S = 0x20s

.field private static final RESULT_DIRTY_MASK:S = 0x4s

.field private static final RESULT_EDITED_MASK:S = 0x8s

.field private static final TYPE_MASK:S = 0xffs

.field private static final ZOMBIE_EMBED_MASK:S = 0x2s


# instance fields
.field _fieldBoundaryType:B

.field _info:B

.field private fieldType:I

.field private hasSep:Z

.field private locked:Z

.field private nested:Z

.field private privateResult:Z

.field private resultDirty:Z

.field private resultEdited:Z

.field private zombieEmbed:Z


# direct methods
.method public constructor <init>([B)V
    .locals 4
    .param p1, "data"    # [B

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    aget-byte v0, p1, v2

    and-int/lit8 v0, v0, 0x1f

    int-to-byte v0, v0

    iput-byte v0, p0, Lorg/apache/index/poi/hwpf/model/FieldDescriptor;->_fieldBoundaryType:B

    .line 61
    aget-byte v0, p1, v1

    iput-byte v0, p0, Lorg/apache/index/poi/hwpf/model/FieldDescriptor;->_info:B

    .line 63
    iget-byte v0, p0, Lorg/apache/index/poi/hwpf/model/FieldDescriptor;->_fieldBoundaryType:B

    const/16 v3, 0x13

    if-ne v0, v3, :cond_1

    .line 65
    iget-byte v0, p0, Lorg/apache/index/poi/hwpf/model/FieldDescriptor;->_info:B

    and-int/lit16 v0, v0, 0xff

    iput v0, p0, Lorg/apache/index/poi/hwpf/model/FieldDescriptor;->fieldType:I

    .line 76
    :cond_0
    :goto_0
    return-void

    .line 66
    :cond_1
    iget-byte v0, p0, Lorg/apache/index/poi/hwpf/model/FieldDescriptor;->_fieldBoundaryType:B

    const/16 v3, 0x15

    if-ne v0, v3, :cond_0

    .line 68
    iget-byte v0, p0, Lorg/apache/index/poi/hwpf/model/FieldDescriptor;->_info:B

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lorg/apache/index/poi/hwpf/model/FieldDescriptor;->zombieEmbed:Z

    .line 69
    iget-byte v0, p0, Lorg/apache/index/poi/hwpf/model/FieldDescriptor;->_info:B

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v1, :cond_3

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lorg/apache/index/poi/hwpf/model/FieldDescriptor;->resultDirty:Z

    .line 70
    iget-byte v0, p0, Lorg/apache/index/poi/hwpf/model/FieldDescriptor;->_info:B

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v1, :cond_4

    move v0, v1

    :goto_3
    iput-boolean v0, p0, Lorg/apache/index/poi/hwpf/model/FieldDescriptor;->resultEdited:Z

    .line 71
    iget-byte v0, p0, Lorg/apache/index/poi/hwpf/model/FieldDescriptor;->_info:B

    and-int/lit8 v0, v0, 0x10

    if-ne v0, v1, :cond_5

    move v0, v1

    :goto_4
    iput-boolean v0, p0, Lorg/apache/index/poi/hwpf/model/FieldDescriptor;->locked:Z

    .line 72
    iget-byte v0, p0, Lorg/apache/index/poi/hwpf/model/FieldDescriptor;->_info:B

    and-int/lit8 v0, v0, 0x20

    if-ne v0, v1, :cond_6

    move v0, v1

    :goto_5
    iput-boolean v0, p0, Lorg/apache/index/poi/hwpf/model/FieldDescriptor;->privateResult:Z

    .line 73
    iget-byte v0, p0, Lorg/apache/index/poi/hwpf/model/FieldDescriptor;->_info:B

    and-int/lit8 v0, v0, 0x40

    if-ne v0, v1, :cond_7

    move v0, v1

    :goto_6
    iput-boolean v0, p0, Lorg/apache/index/poi/hwpf/model/FieldDescriptor;->nested:Z

    .line 74
    iget-byte v0, p0, Lorg/apache/index/poi/hwpf/model/FieldDescriptor;->_info:B

    and-int/lit16 v0, v0, 0x80

    if-ne v0, v1, :cond_8

    :goto_7
    iput-boolean v1, p0, Lorg/apache/index/poi/hwpf/model/FieldDescriptor;->hasSep:Z

    goto :goto_0

    :cond_2
    move v0, v2

    .line 68
    goto :goto_1

    :cond_3
    move v0, v2

    .line 69
    goto :goto_2

    :cond_4
    move v0, v2

    .line 70
    goto :goto_3

    :cond_5
    move v0, v2

    .line 71
    goto :goto_4

    :cond_6
    move v0, v2

    .line 72
    goto :goto_5

    :cond_7
    move v0, v2

    .line 73
    goto :goto_6

    :cond_8
    move v1, v2

    .line 74
    goto :goto_7
.end method


# virtual methods
.method public getBoundaryType()I
    .locals 1

    .prologue
    .line 80
    iget-byte v0, p0, Lorg/apache/index/poi/hwpf/model/FieldDescriptor;->_fieldBoundaryType:B

    return v0
.end method

.method public getFieldType()I
    .locals 2

    .prologue
    .line 85
    iget-byte v0, p0, Lorg/apache/index/poi/hwpf/model/FieldDescriptor;->_fieldBoundaryType:B

    const/16 v1, 0x13

    if-eq v0, v1, :cond_0

    .line 86
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    .line 87
    const-string/jumbo v1, "This field is only defined for begin marks."

    .line 86
    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 88
    :cond_0
    iget v0, p0, Lorg/apache/index/poi/hwpf/model/FieldDescriptor;->fieldType:I

    return v0
.end method

.method public isHasSep()Z
    .locals 2

    .prologue
    .line 141
    iget-byte v0, p0, Lorg/apache/index/poi/hwpf/model/FieldDescriptor;->_fieldBoundaryType:B

    const/16 v1, 0x15

    if-eq v0, v1, :cond_0

    .line 142
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    .line 143
    const-string/jumbo v1, "This field is only defined for end marks."

    .line 142
    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 144
    :cond_0
    iget-boolean v0, p0, Lorg/apache/index/poi/hwpf/model/FieldDescriptor;->hasSep:Z

    return v0
.end method

.method public isLocked()Z
    .locals 2

    .prologue
    .line 117
    iget-byte v0, p0, Lorg/apache/index/poi/hwpf/model/FieldDescriptor;->_fieldBoundaryType:B

    const/16 v1, 0x15

    if-eq v0, v1, :cond_0

    .line 118
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    .line 119
    const-string/jumbo v1, "This field is only defined for end marks."

    .line 118
    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 120
    :cond_0
    iget-boolean v0, p0, Lorg/apache/index/poi/hwpf/model/FieldDescriptor;->locked:Z

    return v0
.end method

.method public isNested()Z
    .locals 2

    .prologue
    .line 133
    iget-byte v0, p0, Lorg/apache/index/poi/hwpf/model/FieldDescriptor;->_fieldBoundaryType:B

    const/16 v1, 0x15

    if-eq v0, v1, :cond_0

    .line 134
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    .line 135
    const-string/jumbo v1, "This field is only defined for end marks."

    .line 134
    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 136
    :cond_0
    iget-boolean v0, p0, Lorg/apache/index/poi/hwpf/model/FieldDescriptor;->nested:Z

    return v0
.end method

.method public isPrivateResult()Z
    .locals 2

    .prologue
    .line 125
    iget-byte v0, p0, Lorg/apache/index/poi/hwpf/model/FieldDescriptor;->_fieldBoundaryType:B

    const/16 v1, 0x15

    if-eq v0, v1, :cond_0

    .line 126
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    .line 127
    const-string/jumbo v1, "This field is only defined for end marks."

    .line 126
    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 128
    :cond_0
    iget-boolean v0, p0, Lorg/apache/index/poi/hwpf/model/FieldDescriptor;->privateResult:Z

    return v0
.end method

.method public isResultDirty()Z
    .locals 2

    .prologue
    .line 101
    iget-byte v0, p0, Lorg/apache/index/poi/hwpf/model/FieldDescriptor;->_fieldBoundaryType:B

    const/16 v1, 0x15

    if-eq v0, v1, :cond_0

    .line 102
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    .line 103
    const-string/jumbo v1, "This field is only defined for end marks."

    .line 102
    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 104
    :cond_0
    iget-boolean v0, p0, Lorg/apache/index/poi/hwpf/model/FieldDescriptor;->resultDirty:Z

    return v0
.end method

.method public isResultEdited()Z
    .locals 2

    .prologue
    .line 109
    iget-byte v0, p0, Lorg/apache/index/poi/hwpf/model/FieldDescriptor;->_fieldBoundaryType:B

    const/16 v1, 0x15

    if-eq v0, v1, :cond_0

    .line 110
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    .line 111
    const-string/jumbo v1, "This field is only defined for end marks."

    .line 110
    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 112
    :cond_0
    iget-boolean v0, p0, Lorg/apache/index/poi/hwpf/model/FieldDescriptor;->resultEdited:Z

    return v0
.end method

.method public isZombieEmbed()Z
    .locals 2

    .prologue
    .line 93
    iget-byte v0, p0, Lorg/apache/index/poi/hwpf/model/FieldDescriptor;->_fieldBoundaryType:B

    const/16 v1, 0x15

    if-eq v0, v1, :cond_0

    .line 94
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    .line 95
    const-string/jumbo v1, "This field is only defined for end marks."

    .line 94
    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 96
    :cond_0
    iget-boolean v0, p0, Lorg/apache/index/poi/hwpf/model/FieldDescriptor;->zombieEmbed:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 149
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 152
    .local v0, "details":Ljava/lang/StringBuffer;
    iget-byte v1, p0, Lorg/apache/index/poi/hwpf/model/FieldDescriptor;->_fieldBoundaryType:B

    const/16 v2, 0x13

    if-ne v1, v2, :cond_1

    .line 154
    const-string/jumbo v1, " type: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 155
    iget v1, p0, Lorg/apache/index/poi/hwpf/model/FieldDescriptor;->fieldType:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 163
    :cond_0
    :goto_0
    const-string/jumbo v1, "FLD - 0x{0}{1}"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 164
    iget-byte v4, p0, Lorg/apache/index/poi/hwpf/model/FieldDescriptor;->_fieldBoundaryType:B

    invoke-static {v4}, Lorg/apache/index/poi/util/HexDump;->toHex(B)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    .line 163
    invoke-static {v1, v2}, Ljava/text/MessageFormat;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 157
    :cond_1
    iget-byte v1, p0, Lorg/apache/index/poi/hwpf/model/FieldDescriptor;->_fieldBoundaryType:B

    const/16 v2, 0x15

    if-ne v1, v2, :cond_0

    .line 159
    const-string/jumbo v1, " flags: 0x"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 160
    iget-byte v1, p0, Lorg/apache/index/poi/hwpf/model/FieldDescriptor;->_info:B

    invoke-static {v1}, Lorg/apache/index/poi/util/HexDump;->toHex(B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0
.end method
