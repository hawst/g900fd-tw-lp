.class public Lorg/apache/index/poi/hssf/record/common/UnicodeString;
.super Ljava/lang/Object;
.source "UnicodeString.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/index/poi/hssf/record/common/UnicodeString$ExtRst;,
        Lorg/apache/index/poi/hssf/record/common/UnicodeString$FormatRun;,
        Lorg/apache/index/poi/hssf/record/common/UnicodeString$PhRun;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lorg/apache/index/poi/hssf/record/common/UnicodeString;",
        ">;"
    }
.end annotation


# static fields
.field private static final extBit:Lorg/apache/index/poi/util/BitField;

.field private static final highByte:Lorg/apache/index/poi/util/BitField;

.field private static final richText:Lorg/apache/index/poi/util/BitField;


# instance fields
.field private field_1_charCount:S

.field private field_2_optionflags:B

.field private field_3_string:Ljava/lang/String;

.field private field_4_format_runs:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/apache/index/poi/hssf/record/common/UnicodeString$FormatRun;",
            ">;"
        }
    .end annotation
.end field

.field private field_5_ext_rst:Lorg/apache/index/poi/hssf/record/common/UnicodeString$ExtRst;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 48
    const/4 v0, 0x1

    invoke-static {v0}, Lorg/apache/index/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/index/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->highByte:Lorg/apache/index/poi/util/BitField;

    .line 50
    const/4 v0, 0x4

    invoke-static {v0}, Lorg/apache/index/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/index/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->extBit:Lorg/apache/index/poi/util/BitField;

    .line 51
    const/16 v0, 0x8

    invoke-static {v0}, Lorg/apache/index/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/index/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->richText:Lorg/apache/index/poi/util/BitField;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 322
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 324
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    .line 326
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 328
    invoke-virtual {p0, p1}, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->setString(Ljava/lang/String;)V

    .line 329
    return-void
.end method

.method public constructor <init>(Lorg/apache/index/poi/hssf/record/RecordInputStream;)V
    .locals 7
    .param p1, "in"    # Lorg/apache/index/poi/hssf/record/RecordInputStream;

    .prologue
    .line 411
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 412
    invoke-virtual {p1}, Lorg/apache/index/poi/hssf/record/RecordInputStream;->readShort()S

    move-result v4

    iput-short v4, p0, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->field_1_charCount:S

    .line 413
    invoke-virtual {p1}, Lorg/apache/index/poi/hssf/record/RecordInputStream;->readByte()B

    move-result v4

    iput-byte v4, p0, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->field_2_optionflags:B

    .line 415
    const/4 v3, 0x0

    .line 416
    .local v3, "runCount":I
    const/4 v0, 0x0

    .line 418
    .local v0, "extensionLength":I
    invoke-direct {p0}, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->isRichText()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 420
    invoke-virtual {p1}, Lorg/apache/index/poi/hssf/record/RecordInputStream;->readShort()S

    move-result v3

    .line 423
    :cond_0
    invoke-direct {p0}, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->isExtendedText()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 425
    invoke-virtual {p1}, Lorg/apache/index/poi/hssf/record/RecordInputStream;->readInt()I

    move-result v0

    .line 428
    :cond_1
    iget-byte v4, p0, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->field_2_optionflags:B

    and-int/lit8 v4, v4, 0x1

    if-nez v4, :cond_4

    const/4 v2, 0x1

    .line 429
    .local v2, "isCompressed":Z
    :goto_0
    if-eqz v2, :cond_5

    .line 430
    invoke-virtual {p0}, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->getCharCount()I

    move-result v4

    invoke-virtual {p1, v4}, Lorg/apache/index/poi/hssf/record/RecordInputStream;->readCompressedUnicode(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->field_3_string:Ljava/lang/String;

    .line 436
    :goto_1
    invoke-direct {p0}, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->isRichText()Z

    move-result v4

    if-eqz v4, :cond_2

    if-lez v3, :cond_2

    .line 437
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4, v3}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v4, p0, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->field_4_format_runs:Ljava/util/List;

    .line 438
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_2
    if-lt v1, v3, :cond_6

    .line 443
    .end local v1    # "i":I
    :cond_2
    invoke-direct {p0}, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->isExtendedText()Z

    move-result v4

    if-eqz v4, :cond_3

    if-lez v0, :cond_3

    .line 444
    new-instance v4, Lorg/apache/index/poi/hssf/record/common/UnicodeString$ExtRst;

    new-instance v5, Lorg/apache/index/poi/hssf/record/cont/ContinuableRecordInput;

    invoke-direct {v5, p1}, Lorg/apache/index/poi/hssf/record/cont/ContinuableRecordInput;-><init>(Lorg/apache/index/poi/hssf/record/RecordInputStream;)V

    invoke-direct {v4, v5, v0}, Lorg/apache/index/poi/hssf/record/common/UnicodeString$ExtRst;-><init>(Lorg/apache/index/poi/util/LittleEndianInput;I)V

    iput-object v4, p0, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->field_5_ext_rst:Lorg/apache/index/poi/hssf/record/common/UnicodeString$ExtRst;

    .line 445
    iget-object v4, p0, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->field_5_ext_rst:Lorg/apache/index/poi/hssf/record/common/UnicodeString$ExtRst;

    invoke-virtual {v4}, Lorg/apache/index/poi/hssf/record/common/UnicodeString$ExtRst;->getDataSize()I

    move-result v4

    add-int/lit8 v4, v4, 0x4

    if-eq v4, v0, :cond_3

    .line 446
    sget-object v4, Ljava/lang/System;->err:Ljava/io/PrintStream;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "ExtRst was supposed to be "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, " bytes long, but seems to actually be "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->field_5_ext_rst:Lorg/apache/index/poi/hssf/record/common/UnicodeString$ExtRst;

    invoke-virtual {v6}, Lorg/apache/index/poi/hssf/record/common/UnicodeString$ExtRst;->getDataSize()I

    move-result v6

    add-int/lit8 v6, v6, 0x4

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 449
    :cond_3
    return-void

    .line 428
    .end local v2    # "isCompressed":Z
    :cond_4
    const/4 v2, 0x0

    goto :goto_0

    .line 432
    .restart local v2    # "isCompressed":Z
    :cond_5
    invoke-virtual {p0}, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->getCharCount()I

    move-result v4

    invoke-virtual {p1, v4}, Lorg/apache/index/poi/hssf/record/RecordInputStream;->readUnicodeLEString(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->field_3_string:Ljava/lang/String;

    goto :goto_1

    .line 439
    .restart local v1    # "i":I
    :cond_6
    iget-object v4, p0, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->field_4_format_runs:Ljava/util/List;

    new-instance v5, Lorg/apache/index/poi/hssf/record/common/UnicodeString$FormatRun;

    invoke-direct {v5, p1}, Lorg/apache/index/poi/hssf/record/common/UnicodeString$FormatRun;-><init>(Lorg/apache/index/poi/util/LittleEndianInput;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 438
    add-int/lit8 v1, v1, 0x1

    goto :goto_2
.end method

.method private findFormatRunAt(I)I
    .locals 5
    .param p1, "characterPos"    # I

    .prologue
    const/4 v3, -0x1

    .line 566
    iget-object v4, p0, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->field_4_format_runs:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v2

    .line 567
    .local v2, "size":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-lt v0, v2, :cond_1

    move v0, v3

    .line 574
    .end local v0    # "i":I
    :cond_0
    :goto_1
    return v0

    .line 568
    .restart local v0    # "i":I
    :cond_1
    iget-object v4, p0, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->field_4_format_runs:Ljava/util/List;

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/index/poi/hssf/record/common/UnicodeString$FormatRun;

    .line 569
    .local v1, "r":Lorg/apache/index/poi/hssf/record/common/UnicodeString$FormatRun;
    iget-short v4, v1, Lorg/apache/index/poi/hssf/record/common/UnicodeString$FormatRun;->_character:S

    if-eq v4, p1, :cond_0

    .line 571
    iget-short v4, v1, Lorg/apache/index/poi/hssf/record/common/UnicodeString$FormatRun;->_character:S

    if-le v4, p1, :cond_2

    move v0, v3

    .line 572
    goto :goto_1

    .line 567
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private isExtendedText()Z
    .locals 2

    .prologue
    .line 790
    sget-object v0, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->extBit:Lorg/apache/index/poi/util/BitField;

    invoke-virtual {p0}, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->getOptionFlags()B

    move-result v1

    invoke-virtual {v0, v1}, Lorg/apache/index/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method private isRichText()Z
    .locals 2

    .prologue
    .line 785
    sget-object v0, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->richText:Lorg/apache/index/poi/util/BitField;

    invoke-virtual {p0}, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->getOptionFlags()B

    move-result v1

    invoke-virtual {v0, v1}, Lorg/apache/index/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public addFormatRun(Lorg/apache/index/poi/hssf/record/common/UnicodeString$FormatRun;)V
    .locals 3
    .param p1, "r"    # Lorg/apache/index/poi/hssf/record/common/UnicodeString$FormatRun;

    .prologue
    .line 583
    iget-object v1, p0, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->field_4_format_runs:Ljava/util/List;

    if-nez v1, :cond_0

    .line 584
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->field_4_format_runs:Ljava/util/List;

    .line 587
    :cond_0
    iget-short v1, p1, Lorg/apache/index/poi/hssf/record/common/UnicodeString$FormatRun;->_character:S

    invoke-direct {p0, v1}, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->findFormatRunAt(I)I

    move-result v0

    .line 588
    .local v0, "index":I
    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    .line 589
    iget-object v1, p0, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->field_4_format_runs:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 591
    :cond_1
    iget-object v1, p0, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->field_4_format_runs:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 594
    iget-object v1, p0, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->field_4_format_runs:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 597
    sget-object v1, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->richText:Lorg/apache/index/poi/util/BitField;

    iget-byte v2, p0, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->field_2_optionflags:B

    invoke-virtual {v1, v2}, Lorg/apache/index/poi/util/BitField;->setByte(B)B

    move-result v1

    iput-byte v1, p0, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->field_2_optionflags:B

    .line 598
    return-void
.end method

.method public clearFormatting()V
    .locals 2

    .prologue
    .line 616
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->field_4_format_runs:Ljava/util/List;

    .line 617
    sget-object v0, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->richText:Lorg/apache/index/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->field_2_optionflags:B

    invoke-virtual {v0, v1}, Lorg/apache/index/poi/util/BitField;->clearByte(B)B

    move-result v0

    iput-byte v0, p0, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->field_2_optionflags:B

    .line 618
    return-void
.end method

.method public clone()Ljava/lang/Object;
    .locals 7

    .prologue
    .line 795
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 799
    :goto_0
    new-instance v1, Lorg/apache/index/poi/hssf/record/common/UnicodeString;

    invoke-direct {v1}, Lorg/apache/index/poi/hssf/record/common/UnicodeString;-><init>()V

    .line 800
    .local v1, "str":Lorg/apache/index/poi/hssf/record/common/UnicodeString;
    iget-short v2, p0, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->field_1_charCount:S

    iput-short v2, v1, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->field_1_charCount:S

    .line 801
    iget-byte v2, p0, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->field_2_optionflags:B

    iput-byte v2, v1, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->field_2_optionflags:B

    .line 802
    iget-object v2, p0, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->field_3_string:Ljava/lang/String;

    iput-object v2, v1, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->field_3_string:Ljava/lang/String;

    .line 803
    iget-object v2, p0, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->field_4_format_runs:Ljava/util/List;

    if-eqz v2, :cond_0

    .line 804
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, v1, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->field_4_format_runs:Ljava/util/List;

    .line 805
    iget-object v2, p0, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->field_4_format_runs:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_2

    .line 809
    :cond_0
    iget-object v2, p0, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->field_5_ext_rst:Lorg/apache/index/poi/hssf/record/common/UnicodeString$ExtRst;

    if-eqz v2, :cond_1

    .line 810
    iget-object v2, p0, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->field_5_ext_rst:Lorg/apache/index/poi/hssf/record/common/UnicodeString$ExtRst;

    invoke-virtual {v2}, Lorg/apache/index/poi/hssf/record/common/UnicodeString$ExtRst;->clone()Lorg/apache/index/poi/hssf/record/common/UnicodeString$ExtRst;

    move-result-object v2

    iput-object v2, v1, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->field_5_ext_rst:Lorg/apache/index/poi/hssf/record/common/UnicodeString$ExtRst;

    .line 813
    :cond_1
    return-object v1

    .line 805
    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/index/poi/hssf/record/common/UnicodeString$FormatRun;

    .line 806
    .local v0, "r":Lorg/apache/index/poi/hssf/record/common/UnicodeString$FormatRun;
    iget-object v3, v1, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->field_4_format_runs:Ljava/util/List;

    new-instance v4, Lorg/apache/index/poi/hssf/record/common/UnicodeString$FormatRun;

    iget-short v5, v0, Lorg/apache/index/poi/hssf/record/common/UnicodeString$FormatRun;->_character:S

    iget-short v6, v0, Lorg/apache/index/poi/hssf/record/common/UnicodeString$FormatRun;->_fontIndex:S

    invoke-direct {v4, v5, v6}, Lorg/apache/index/poi/hssf/record/common/UnicodeString$FormatRun;-><init>(SS)V

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 796
    .end local v0    # "r":Lorg/apache/index/poi/hssf/record/common/UnicodeString$FormatRun;
    .end local v1    # "str":Lorg/apache/index/poi/hssf/record/common/UnicodeString;
    :catch_0
    move-exception v2

    goto :goto_0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 1
    check-cast p1, Lorg/apache/index/poi/hssf/record/common/UnicodeString;

    invoke-virtual {p0, p1}, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->compareTo(Lorg/apache/index/poi/hssf/record/common/UnicodeString;)I

    move-result v0

    return v0
.end method

.method public compareTo(Lorg/apache/index/poi/hssf/record/common/UnicodeString;)I
    .locals 10
    .param p1, "str"    # Lorg/apache/index/poi/hssf/record/common/UnicodeString;

    .prologue
    const/4 v6, 0x1

    const/4 v7, -0x1

    const/4 v5, 0x0

    .line 728
    invoke-virtual {p0}, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->getString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1}, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->getString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    .line 731
    .local v1, "result":I
    if-eqz v1, :cond_1

    move v5, v1

    .line 780
    :cond_0
    :goto_0
    return v5

    .line 735
    :cond_1
    iget-object v8, p0, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->field_4_format_runs:Ljava/util/List;

    if-nez v8, :cond_2

    iget-object v8, p1, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->field_4_format_runs:Ljava/util/List;

    if-eqz v8, :cond_0

    .line 739
    :cond_2
    iget-object v8, p0, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->field_4_format_runs:Ljava/util/List;

    if-nez v8, :cond_3

    iget-object v8, p1, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->field_4_format_runs:Ljava/util/List;

    if-eqz v8, :cond_3

    move v5, v6

    .line 741
    goto :goto_0

    .line 742
    :cond_3
    iget-object v8, p0, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->field_4_format_runs:Ljava/util/List;

    if-eqz v8, :cond_4

    iget-object v8, p1, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->field_4_format_runs:Ljava/util/List;

    if-nez v8, :cond_4

    move v5, v7

    .line 744
    goto :goto_0

    .line 748
    :cond_4
    iget-object v8, p0, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->field_4_format_runs:Ljava/util/List;

    if-eqz v8, :cond_6

    .line 750
    iget-object v8, p0, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->field_4_format_runs:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v4

    .line 751
    .local v4, "size":I
    iget-object v8, p1, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->field_4_format_runs:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v8

    if-eq v4, v8, :cond_5

    .line 752
    iget-object v5, p1, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->field_4_format_runs:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    sub-int v5, v4, v5

    goto :goto_0

    .line 754
    :cond_5
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    if-lt v0, v4, :cond_8

    .line 765
    .end local v0    # "i":I
    .end local v4    # "size":I
    :cond_6
    iget-object v8, p0, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->field_5_ext_rst:Lorg/apache/index/poi/hssf/record/common/UnicodeString$ExtRst;

    if-nez v8, :cond_7

    iget-object v8, p1, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->field_5_ext_rst:Lorg/apache/index/poi/hssf/record/common/UnicodeString$ExtRst;

    if-eqz v8, :cond_0

    .line 767
    :cond_7
    iget-object v8, p0, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->field_5_ext_rst:Lorg/apache/index/poi/hssf/record/common/UnicodeString$ExtRst;

    if-nez v8, :cond_a

    iget-object v8, p1, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->field_5_ext_rst:Lorg/apache/index/poi/hssf/record/common/UnicodeString$ExtRst;

    if-eqz v8, :cond_a

    move v5, v6

    .line 768
    goto :goto_0

    .line 755
    .restart local v0    # "i":I
    .restart local v4    # "size":I
    :cond_8
    iget-object v8, p0, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->field_4_format_runs:Ljava/util/List;

    invoke-interface {v8, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/index/poi/hssf/record/common/UnicodeString$FormatRun;

    .line 756
    .local v2, "run1":Lorg/apache/index/poi/hssf/record/common/UnicodeString$FormatRun;
    iget-object v8, p1, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->field_4_format_runs:Ljava/util/List;

    invoke-interface {v8, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/index/poi/hssf/record/common/UnicodeString$FormatRun;

    .line 758
    .local v3, "run2":Lorg/apache/index/poi/hssf/record/common/UnicodeString$FormatRun;
    invoke-virtual {v2, v3}, Lorg/apache/index/poi/hssf/record/common/UnicodeString$FormatRun;->compareTo(Lorg/apache/index/poi/hssf/record/common/UnicodeString$FormatRun;)I

    move-result v1

    .line 759
    if-eqz v1, :cond_9

    move v5, v1

    .line 760
    goto :goto_0

    .line 754
    :cond_9
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 769
    .end local v0    # "i":I
    .end local v2    # "run1":Lorg/apache/index/poi/hssf/record/common/UnicodeString$FormatRun;
    .end local v3    # "run2":Lorg/apache/index/poi/hssf/record/common/UnicodeString$FormatRun;
    .end local v4    # "size":I
    :cond_a
    iget-object v6, p0, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->field_5_ext_rst:Lorg/apache/index/poi/hssf/record/common/UnicodeString$ExtRst;

    if-eqz v6, :cond_b

    iget-object v6, p1, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->field_5_ext_rst:Lorg/apache/index/poi/hssf/record/common/UnicodeString$ExtRst;

    if-nez v6, :cond_b

    move v5, v7

    .line 770
    goto :goto_0

    .line 772
    :cond_b
    iget-object v6, p0, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->field_5_ext_rst:Lorg/apache/index/poi/hssf/record/common/UnicodeString$ExtRst;

    if-eqz v6, :cond_0

    .line 774
    iget-object v6, p0, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->field_5_ext_rst:Lorg/apache/index/poi/hssf/record/common/UnicodeString$ExtRst;

    iget-object v7, p1, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->field_5_ext_rst:Lorg/apache/index/poi/hssf/record/common/UnicodeString$ExtRst;

    invoke-virtual {v6, v7}, Lorg/apache/index/poi/hssf/record/common/UnicodeString$ExtRst;->compareTo(Lorg/apache/index/poi/hssf/record/common/UnicodeString$ExtRst;)I

    move-result v1

    .line 775
    if-eqz v1, :cond_0

    move v5, v1

    .line 776
    goto/16 :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 11
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 350
    instance-of v9, p1, Lorg/apache/index/poi/hssf/record/common/UnicodeString;

    if-nez v9, :cond_1

    .line 404
    :cond_0
    :goto_0
    return v7

    :cond_1
    move-object v3, p1

    .line 353
    check-cast v3, Lorg/apache/index/poi/hssf/record/common/UnicodeString;

    .line 356
    .local v3, "other":Lorg/apache/index/poi/hssf/record/common/UnicodeString;
    iget-short v9, p0, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->field_1_charCount:S

    iget-short v10, v3, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->field_1_charCount:S

    if-ne v9, v10, :cond_2

    .line 357
    iget-byte v9, p0, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->field_2_optionflags:B

    iget-byte v10, v3, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->field_2_optionflags:B

    if-ne v9, v10, :cond_2

    .line 358
    iget-object v9, p0, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->field_3_string:Ljava/lang/String;

    iget-object v10, v3, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->field_3_string:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    move v0, v8

    .line 359
    .local v0, "eq":Z
    :goto_1
    if-eqz v0, :cond_0

    .line 362
    iget-object v9, p0, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->field_4_format_runs:Ljava/util/List;

    if-nez v9, :cond_3

    iget-object v9, v3, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->field_4_format_runs:Ljava/util/List;

    if-nez v9, :cond_3

    move v7, v8

    .line 364
    goto :goto_0

    .end local v0    # "eq":Z
    :cond_2
    move v0, v7

    .line 356
    goto :goto_1

    .line 365
    .restart local v0    # "eq":Z
    :cond_3
    iget-object v9, p0, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->field_4_format_runs:Ljava/util/List;

    if-nez v9, :cond_4

    iget-object v9, v3, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->field_4_format_runs:Ljava/util/List;

    if-nez v9, :cond_0

    .line 366
    :cond_4
    iget-object v9, p0, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->field_4_format_runs:Ljava/util/List;

    if-eqz v9, :cond_5

    iget-object v9, v3, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->field_4_format_runs:Ljava/util/List;

    if-eqz v9, :cond_0

    .line 372
    :cond_5
    iget-object v9, p0, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->field_4_format_runs:Ljava/util/List;

    if-eqz v9, :cond_6

    .line 374
    iget-object v9, p0, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->field_4_format_runs:Ljava/util/List;

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v6

    .line 375
    .local v6, "size":I
    iget-object v9, v3, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->field_4_format_runs:Ljava/util/List;

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v9

    if-ne v6, v9, :cond_0

    .line 378
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_2
    if-lt v2, v6, :cond_9

    .line 389
    .end local v2    # "i":I
    .end local v6    # "size":I
    :cond_6
    iget-object v9, p0, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->field_5_ext_rst:Lorg/apache/index/poi/hssf/record/common/UnicodeString$ExtRst;

    if-nez v9, :cond_7

    iget-object v9, v3, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->field_5_ext_rst:Lorg/apache/index/poi/hssf/record/common/UnicodeString$ExtRst;

    if-eqz v9, :cond_8

    .line 391
    :cond_7
    iget-object v9, p0, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->field_5_ext_rst:Lorg/apache/index/poi/hssf/record/common/UnicodeString$ExtRst;

    if-eqz v9, :cond_0

    iget-object v9, v3, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->field_5_ext_rst:Lorg/apache/index/poi/hssf/record/common/UnicodeString$ExtRst;

    if-eqz v9, :cond_0

    .line 392
    iget-object v9, p0, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->field_5_ext_rst:Lorg/apache/index/poi/hssf/record/common/UnicodeString$ExtRst;

    iget-object v10, v3, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->field_5_ext_rst:Lorg/apache/index/poi/hssf/record/common/UnicodeString$ExtRst;

    invoke-virtual {v9, v10}, Lorg/apache/index/poi/hssf/record/common/UnicodeString$ExtRst;->compareTo(Lorg/apache/index/poi/hssf/record/common/UnicodeString$ExtRst;)I

    move-result v1

    .line 393
    .local v1, "extCmp":I
    if-nez v1, :cond_0

    .end local v1    # "extCmp":I
    :cond_8
    move v7, v8

    .line 404
    goto :goto_0

    .line 379
    .restart local v2    # "i":I
    .restart local v6    # "size":I
    :cond_9
    iget-object v9, p0, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->field_4_format_runs:Ljava/util/List;

    invoke-interface {v9, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/index/poi/hssf/record/common/UnicodeString$FormatRun;

    .line 380
    .local v4, "run1":Lorg/apache/index/poi/hssf/record/common/UnicodeString$FormatRun;
    iget-object v9, v3, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->field_4_format_runs:Ljava/util/List;

    invoke-interface {v9, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/apache/index/poi/hssf/record/common/UnicodeString$FormatRun;

    .line 382
    .local v5, "run2":Lorg/apache/index/poi/hssf/record/common/UnicodeString$FormatRun;
    invoke-virtual {v4, v5}, Lorg/apache/index/poi/hssf/record/common/UnicodeString$FormatRun;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 378
    add-int/lit8 v2, v2, 0x1

    goto :goto_2
.end method

.method public formatIterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Lorg/apache/index/poi/hssf/record/common/UnicodeString$FormatRun;",
            ">;"
        }
    .end annotation

    .prologue
    .line 601
    iget-object v0, p0, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->field_4_format_runs:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 602
    iget-object v0, p0, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->field_4_format_runs:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 604
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getCharCount()I
    .locals 2

    .prologue
    .line 460
    iget-short v0, p0, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->field_1_charCount:S

    if-gez v0, :cond_0

    .line 461
    iget-short v0, p0, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->field_1_charCount:S

    const/high16 v1, 0x10000

    add-int/2addr v0, v1

    .line 463
    :goto_0
    return v0

    :cond_0
    iget-short v0, p0, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->field_1_charCount:S

    goto :goto_0
.end method

.method public getCharCountShort()S
    .locals 1

    .prologue
    .line 473
    iget-short v0, p0, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->field_1_charCount:S

    return v0
.end method

.method public getDebugInfo()Ljava/lang/String;
    .locals 5

    .prologue
    .line 668
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 670
    .local v0, "buffer":Ljava/lang/StringBuffer;
    const-string/jumbo v3, "[UNICODESTRING]\n"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 671
    const-string/jumbo v3, "    .charcount       = "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    .line 672
    invoke-virtual {p0}, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->getCharCount()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    const-string/jumbo v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 673
    const-string/jumbo v3, "    .optionflags     = "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    .line 674
    invoke-virtual {p0}, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->getOptionFlags()B

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    const-string/jumbo v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 675
    const-string/jumbo v3, "    .string          = "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {p0}, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->getString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    const-string/jumbo v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 676
    iget-object v3, p0, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->field_4_format_runs:Ljava/util/List;

    if-eqz v3, :cond_0

    .line 677
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->field_4_format_runs:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-lt v1, v3, :cond_2

    .line 682
    .end local v1    # "i":I
    :cond_0
    iget-object v3, p0, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->field_5_ext_rst:Lorg/apache/index/poi/hssf/record/common/UnicodeString$ExtRst;

    if-eqz v3, :cond_1

    .line 683
    const-string/jumbo v3, "    .field_5_ext_rst          = "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    const-string/jumbo v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 684
    iget-object v3, p0, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->field_5_ext_rst:Lorg/apache/index/poi/hssf/record/common/UnicodeString$ExtRst;

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    const-string/jumbo v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 686
    :cond_1
    const-string/jumbo v3, "[/UNICODESTRING]\n"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 687
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3

    .line 678
    .restart local v1    # "i":I
    :cond_2
    iget-object v3, p0, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->field_4_format_runs:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/index/poi/hssf/record/common/UnicodeString$FormatRun;

    .line 679
    .local v2, "r":Lorg/apache/index/poi/hssf/record/common/UnicodeString$FormatRun;
    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "      .format_run"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "          = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v2}, Lorg/apache/index/poi/hssf/record/common/UnicodeString$FormatRun;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    const-string/jumbo v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 677
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public getExtendedRst()Lorg/apache/index/poi/hssf/record/common/UnicodeString$ExtRst;
    .locals 1

    .prologue
    .line 622
    iget-object v0, p0, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->field_5_ext_rst:Lorg/apache/index/poi/hssf/record/common/UnicodeString$ExtRst;

    return-object v0
.end method

.method public getFormatRun(I)Lorg/apache/index/poi/hssf/record/common/UnicodeString$FormatRun;
    .locals 2
    .param p1, "index"    # I

    .prologue
    const/4 v0, 0x0

    .line 556
    iget-object v1, p0, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->field_4_format_runs:Ljava/util/List;

    if-nez v1, :cond_1

    .line 562
    :cond_0
    :goto_0
    return-object v0

    .line 559
    :cond_1
    if-ltz p1, :cond_0

    iget-object v1, p0, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->field_4_format_runs:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge p1, v1, :cond_0

    .line 562
    iget-object v0, p0, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->field_4_format_runs:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/index/poi/hssf/record/common/UnicodeString$FormatRun;

    goto :goto_0
.end method

.method public getFormatRunCount()I
    .locals 1

    .prologue
    .line 550
    iget-object v0, p0, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->field_4_format_runs:Ljava/util/List;

    if-nez v0, :cond_0

    .line 551
    const/4 v0, 0x0

    .line 552
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->field_4_format_runs:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_0
.end method

.method public getOptionFlags()B
    .locals 1

    .prologue
    .line 496
    iget-byte v0, p0, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->field_2_optionflags:B

    return v0
.end method

.method public getString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 517
    iget-object v0, p0, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->field_3_string:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 335
    const/4 v0, 0x0

    .line 336
    .local v0, "stringHash":I
    iget-object v1, p0, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->field_3_string:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 337
    iget-object v1, p0, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->field_3_string:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 338
    :cond_0
    iget-short v1, p0, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->field_1_charCount:S

    add-int/2addr v1, v0

    return v1
.end method

.method public removeFormatRun(Lorg/apache/index/poi/hssf/record/common/UnicodeString$FormatRun;)V
    .locals 2
    .param p1, "r"    # Lorg/apache/index/poi/hssf/record/common/UnicodeString$FormatRun;

    .prologue
    .line 608
    iget-object v0, p0, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->field_4_format_runs:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 609
    iget-object v0, p0, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->field_4_format_runs:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 610
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->field_4_format_runs:Ljava/util/List;

    .line 611
    sget-object v0, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->richText:Lorg/apache/index/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->field_2_optionflags:B

    invoke-virtual {v0, v1}, Lorg/apache/index/poi/util/BitField;->clearByte(B)B

    move-result v0

    iput-byte v0, p0, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->field_2_optionflags:B

    .line 613
    :cond_0
    return-void
.end method

.method public serialize(Lorg/apache/index/poi/hssf/record/cont/ContinuableRecordOutput;)V
    .locals 6
    .param p1, "out"    # Lorg/apache/index/poi/hssf/record/cont/ContinuableRecordOutput;

    .prologue
    .line 696
    const/4 v2, 0x0

    .line 697
    .local v2, "numberOfRichTextRuns":I
    const/4 v0, 0x0

    .line 698
    .local v0, "extendedDataSize":I
    invoke-direct {p0}, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->isRichText()Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->field_4_format_runs:Ljava/util/List;

    if-eqz v4, :cond_0

    .line 699
    iget-object v4, p0, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->field_4_format_runs:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v2

    .line 701
    :cond_0
    invoke-direct {p0}, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->isExtendedText()Z

    move-result v4

    if-eqz v4, :cond_1

    iget-object v4, p0, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->field_5_ext_rst:Lorg/apache/index/poi/hssf/record/common/UnicodeString$ExtRst;

    if-eqz v4, :cond_1

    .line 702
    iget-object v4, p0, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->field_5_ext_rst:Lorg/apache/index/poi/hssf/record/common/UnicodeString$ExtRst;

    invoke-virtual {v4}, Lorg/apache/index/poi/hssf/record/common/UnicodeString$ExtRst;->getDataSize()I

    move-result v4

    add-int/lit8 v0, v4, 0x4

    .line 707
    :cond_1
    iget-object v4, p0, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->field_3_string:Ljava/lang/String;

    invoke-virtual {p1, v4, v2, v0}, Lorg/apache/index/poi/hssf/record/cont/ContinuableRecordOutput;->writeString(Ljava/lang/String;II)V

    .line 709
    if-lez v2, :cond_2

    .line 712
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-lt v1, v2, :cond_4

    .line 721
    .end local v1    # "i":I
    :cond_2
    if-lez v0, :cond_3

    .line 722
    iget-object v4, p0, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->field_5_ext_rst:Lorg/apache/index/poi/hssf/record/common/UnicodeString$ExtRst;

    invoke-virtual {v4, p1}, Lorg/apache/index/poi/hssf/record/common/UnicodeString$ExtRst;->serialize(Lorg/apache/index/poi/hssf/record/cont/ContinuableRecordOutput;)V

    .line 724
    :cond_3
    return-void

    .line 713
    .restart local v1    # "i":I
    :cond_4
    invoke-virtual {p1}, Lorg/apache/index/poi/hssf/record/cont/ContinuableRecordOutput;->getAvailableSpace()I

    move-result v4

    const/4 v5, 0x4

    if-ge v4, v5, :cond_5

    .line 714
    invoke-virtual {p1}, Lorg/apache/index/poi/hssf/record/cont/ContinuableRecordOutput;->writeContinue()V

    .line 716
    :cond_5
    iget-object v4, p0, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->field_4_format_runs:Ljava/util/List;

    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/index/poi/hssf/record/common/UnicodeString$FormatRun;

    .line 717
    .local v3, "r":Lorg/apache/index/poi/hssf/record/common/UnicodeString$FormatRun;
    invoke-virtual {v3, p1}, Lorg/apache/index/poi/hssf/record/common/UnicodeString$FormatRun;->serialize(Lorg/apache/index/poi/util/LittleEndianOutput;)V

    .line 712
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public setCharCount(S)V
    .locals 0
    .param p1, "cc"    # S

    .prologue
    .line 483
    iput-short p1, p0, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->field_1_charCount:S

    .line 484
    return-void
.end method

.method setExtendedRst(Lorg/apache/index/poi/hssf/record/common/UnicodeString$ExtRst;)V
    .locals 2
    .param p1, "ext_rst"    # Lorg/apache/index/poi/hssf/record/common/UnicodeString$ExtRst;

    .prologue
    .line 625
    if-eqz p1, :cond_0

    .line 626
    sget-object v0, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->extBit:Lorg/apache/index/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->field_2_optionflags:B

    invoke-virtual {v0, v1}, Lorg/apache/index/poi/util/BitField;->setByte(B)B

    move-result v0

    iput-byte v0, p0, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->field_2_optionflags:B

    .line 630
    :goto_0
    iput-object p1, p0, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->field_5_ext_rst:Lorg/apache/index/poi/hssf/record/common/UnicodeString$ExtRst;

    .line 631
    return-void

    .line 628
    :cond_0
    sget-object v0, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->extBit:Lorg/apache/index/poi/util/BitField;

    iget-byte v1, p0, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->field_2_optionflags:B

    invoke-virtual {v0, v1}, Lorg/apache/index/poi/util/BitField;->clearByte(B)B

    move-result v0

    iput-byte v0, p0, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->field_2_optionflags:B

    goto :goto_0
.end method

.method public setOptionFlags(B)V
    .locals 0
    .param p1, "of"    # B

    .prologue
    .line 509
    iput-byte p1, p0, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->field_2_optionflags:B

    .line 510
    return-void
.end method

.method public setString(Ljava/lang/String;)V
    .locals 5
    .param p1, "string"    # Ljava/lang/String;

    .prologue
    .line 527
    iput-object p1, p0, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->field_3_string:Ljava/lang/String;

    .line 528
    iget-object v3, p0, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->field_3_string:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    int-to-short v3, v3

    invoke-virtual {p0, v3}, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->setCharCount(S)V

    .line 532
    const/4 v2, 0x0

    .line 533
    .local v2, "useUTF16":Z
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    .line 535
    .local v1, "strlen":I
    const/4 v0, 0x0

    .local v0, "j":I
    :goto_0
    if-lt v0, v1, :cond_0

    .line 543
    :goto_1
    if-eqz v2, :cond_2

    .line 545
    sget-object v3, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->highByte:Lorg/apache/index/poi/util/BitField;

    iget-byte v4, p0, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->field_2_optionflags:B

    invoke-virtual {v3, v4}, Lorg/apache/index/poi/util/BitField;->setByte(B)B

    move-result v3

    iput-byte v3, p0, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->field_2_optionflags:B

    .line 547
    :goto_2
    return-void

    .line 537
    :cond_0
    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v3

    const/16 v4, 0xff

    if-le v3, v4, :cond_1

    .line 539
    const/4 v2, 0x1

    .line 540
    goto :goto_1

    .line 535
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 546
    :cond_2
    sget-object v3, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->highByte:Lorg/apache/index/poi/util/BitField;

    iget-byte v4, p0, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->field_2_optionflags:B

    invoke-virtual {v3, v4}, Lorg/apache/index/poi/util/BitField;->clearByte(B)B

    move-result v3

    iput-byte v3, p0, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->field_2_optionflags:B

    goto :goto_2
.end method

.method public swapFontUse(SS)V
    .locals 3
    .param p1, "oldFontIndex"    # S
    .param p2, "newFontIndex"    # S

    .prologue
    .line 641
    iget-object v1, p0, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->field_4_format_runs:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_1

    .line 646
    return-void

    .line 641
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/index/poi/hssf/record/common/UnicodeString$FormatRun;

    .line 642
    .local v0, "run":Lorg/apache/index/poi/hssf/record/common/UnicodeString$FormatRun;
    iget-short v2, v0, Lorg/apache/index/poi/hssf/record/common/UnicodeString$FormatRun;->_fontIndex:S

    if-ne v2, p1, :cond_0

    .line 643
    iput-short p2, v0, Lorg/apache/index/poi/hssf/record/common/UnicodeString$FormatRun;->_fontIndex:S

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 656
    invoke-virtual {p0}, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->getString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
