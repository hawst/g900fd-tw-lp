.class public final Lorg/apache/index/poi/hslf/record/MainMaster;
.super Lorg/apache/index/poi/hslf/record/SheetContainer;
.source "MainMaster.java"


# static fields
.field private static _type:J


# instance fields
.field private _header:[B

.field private ppDrawing:Lorg/apache/index/poi/hslf/record/PPDrawing;

.field private slideAtom:Lorg/apache/index/poi/hslf/record/SlideAtom;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 28
    const-wide/16 v0, 0x3f8

    sput-wide v0, Lorg/apache/index/poi/hslf/record/MainMaster;->_type:J

    return-void
.end method

.method protected constructor <init>([BII)V
    .locals 4
    .param p1, "source"    # [B
    .param p2, "start"    # I
    .param p3, "len"    # I

    .prologue
    const/16 v3, 0x8

    .line 48
    invoke-direct {p0}, Lorg/apache/index/poi/hslf/record/SheetContainer;-><init>()V

    .line 50
    new-array v1, v3, [B

    iput-object v1, p0, Lorg/apache/index/poi/hslf/record/MainMaster;->_header:[B

    .line 51
    iget-object v1, p0, Lorg/apache/index/poi/hslf/record/MainMaster;->_header:[B

    const/4 v2, 0x0

    invoke-static {p1, p2, v1, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 54
    add-int/lit8 v1, p2, 0x8

    add-int/lit8 v2, p3, -0x8

    invoke-static {p1, v1, v2}, Lorg/apache/index/poi/hslf/record/Record;->findChildRecords([BII)[Lorg/apache/index/poi/hslf/record/Record;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/index/poi/hslf/record/MainMaster;->_children:[Lorg/apache/index/poi/hslf/record/Record;

    .line 57
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lorg/apache/index/poi/hslf/record/MainMaster;->_children:[Lorg/apache/index/poi/hslf/record/Record;

    array-length v1, v1

    if-lt v0, v1, :cond_0

    .line 64
    return-void

    .line 58
    :cond_0
    iget-object v1, p0, Lorg/apache/index/poi/hslf/record/MainMaster;->_children:[Lorg/apache/index/poi/hslf/record/Record;

    aget-object v1, v1, v0

    instance-of v1, v1, Lorg/apache/index/poi/hslf/record/SlideAtom;

    if-eqz v1, :cond_2

    .line 59
    iget-object v1, p0, Lorg/apache/index/poi/hslf/record/MainMaster;->_children:[Lorg/apache/index/poi/hslf/record/Record;

    aget-object v1, v1, v0

    check-cast v1, Lorg/apache/index/poi/hslf/record/SlideAtom;

    iput-object v1, p0, Lorg/apache/index/poi/hslf/record/MainMaster;->slideAtom:Lorg/apache/index/poi/hslf/record/SlideAtom;

    .line 57
    :cond_1
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 60
    :cond_2
    iget-object v1, p0, Lorg/apache/index/poi/hslf/record/MainMaster;->_children:[Lorg/apache/index/poi/hslf/record/Record;

    aget-object v1, v1, v0

    instance-of v1, v1, Lorg/apache/index/poi/hslf/record/PPDrawing;

    if-eqz v1, :cond_1

    .line 61
    iget-object v1, p0, Lorg/apache/index/poi/hslf/record/MainMaster;->_children:[Lorg/apache/index/poi/hslf/record/Record;

    aget-object v1, v1, v0

    check-cast v1, Lorg/apache/index/poi/hslf/record/PPDrawing;

    iput-object v1, p0, Lorg/apache/index/poi/hslf/record/MainMaster;->ppDrawing:Lorg/apache/index/poi/hslf/record/PPDrawing;

    goto :goto_1
.end method


# virtual methods
.method public getPPDrawing()Lorg/apache/index/poi/hslf/record/PPDrawing;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/MainMaster;->ppDrawing:Lorg/apache/index/poi/hslf/record/PPDrawing;

    return-object v0
.end method

.method public getRecordType()J
    .locals 2

    .prologue
    .line 69
    sget-wide v0, Lorg/apache/index/poi/hslf/record/MainMaster;->_type:J

    return-wide v0
.end method

.method public getSlideAtom()Lorg/apache/index/poi/hslf/record/SlideAtom;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/MainMaster;->slideAtom:Lorg/apache/index/poi/hslf/record/SlideAtom;

    return-object v0
.end method
