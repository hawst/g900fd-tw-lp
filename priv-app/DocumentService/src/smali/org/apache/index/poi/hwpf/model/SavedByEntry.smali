.class public final Lorg/apache/index/poi/hwpf/model/SavedByEntry;
.super Ljava/lang/Object;
.source "SavedByEntry.java"


# instance fields
.field private saveLocation:Ljava/lang/String;

.field private userName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "userName"    # Ljava/lang/String;
    .param p2, "saveLocation"    # Ljava/lang/String;

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p1, p0, Lorg/apache/index/poi/hwpf/model/SavedByEntry;->userName:Ljava/lang/String;

    .line 34
    iput-object p2, p0, Lorg/apache/index/poi/hwpf/model/SavedByEntry;->saveLocation:Ljava/lang/String;

    .line 35
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 55
    if-ne p1, p0, :cond_1

    .line 58
    :cond_0
    :goto_0
    return v1

    .line 56
    :cond_1
    instance-of v3, p1, Lorg/apache/index/poi/hwpf/model/SavedByEntry;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 57
    check-cast v0, Lorg/apache/index/poi/hwpf/model/SavedByEntry;

    .line 58
    .local v0, "that":Lorg/apache/index/poi/hwpf/model/SavedByEntry;
    iget-object v3, v0, Lorg/apache/index/poi/hwpf/model/SavedByEntry;->userName:Ljava/lang/String;

    iget-object v4, p0, Lorg/apache/index/poi/hwpf/model/SavedByEntry;->userName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 59
    iget-object v3, v0, Lorg/apache/index/poi/hwpf/model/SavedByEntry;->saveLocation:Ljava/lang/String;

    iget-object v4, p0, Lorg/apache/index/poi/hwpf/model/SavedByEntry;->saveLocation:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    .line 58
    goto :goto_0
.end method

.method public getSaveLocation()Ljava/lang/String;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/SavedByEntry;->saveLocation:Ljava/lang/String;

    return-object v0
.end method

.method public getUserName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/SavedByEntry;->userName:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 69
    const/16 v0, 0x1d

    .line 70
    .local v0, "hash":I
    iget-object v1, p0, Lorg/apache/index/poi/hwpf/model/SavedByEntry;->userName:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/lit16 v0, v1, 0x179

    .line 71
    mul-int/lit8 v1, v0, 0xd

    iget-object v2, p0, Lorg/apache/index/poi/hwpf/model/SavedByEntry;->saveLocation:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    add-int v0, v1, v2

    .line 72
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 82
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "SavedByEntry[userName="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/model/SavedByEntry;->getUserName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 83
    const-string/jumbo v1, ",saveLocation="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/model/SavedByEntry;->getSaveLocation()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 82
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
