.class public final Lorg/apache/index/poi/hslf/record/CString;
.super Lorg/apache/index/poi/hslf/record/RecordAtom;
.source "CString.java"


# static fields
.field private static _type:J


# instance fields
.field private _NormalText:Ljava/lang/String;

.field private _header:[B

.field private _text:[B


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 36
    const-wide/16 v0, 0xfba

    sput-wide v0, Lorg/apache/index/poi/hslf/record/CString;->_type:J

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    .line 110
    invoke-direct {p0}, Lorg/apache/index/poi/hslf/record/RecordAtom;-><init>()V

    .line 112
    const/16 v0, 0x8

    new-array v0, v0, [B

    const/4 v1, 0x2

    const/16 v2, -0x46

    aput-byte v2, v0, v1

    const/4 v1, 0x3

    const/16 v2, 0xf

    aput-byte v2, v0, v1

    iput-object v0, p0, Lorg/apache/index/poi/hslf/record/CString;->_header:[B

    .line 114
    const/4 v0, 0x0

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/apache/index/poi/hslf/record/CString;->_text:[B

    .line 115
    return-void
.end method

.method protected constructor <init>([BII)V
    .locals 5
    .param p1, "source"    # [B
    .param p2, "start"    # I
    .param p3, "len"    # I

    .prologue
    const/4 v4, 0x0

    const/16 v2, 0x8

    .line 88
    invoke-direct {p0}, Lorg/apache/index/poi/hslf/record/RecordAtom;-><init>()V

    .line 90
    if-ge p3, v2, :cond_0

    const/16 p3, 0x8

    .line 93
    :cond_0
    new-array v1, v2, [B

    iput-object v1, p0, Lorg/apache/index/poi/hslf/record/CString;->_header:[B

    .line 94
    iget-object v1, p0, Lorg/apache/index/poi/hslf/record/CString;->_header:[B

    invoke-static {p1, p2, v1, v4, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 97
    add-int/lit8 v1, p3, -0x8

    new-array v1, v1, [B

    iput-object v1, p0, Lorg/apache/index/poi/hslf/record/CString;->_text:[B

    .line 98
    add-int/lit8 v1, p2, 0x8

    iget-object v2, p0, Lorg/apache/index/poi/hslf/record/CString;->_text:[B

    add-int/lit8 v3, p3, -0x8

    invoke-static {p1, v1, v2, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 101
    invoke-virtual {p0}, Lorg/apache/index/poi/hslf/record/CString;->getText()Ljava/lang/String;

    move-result-object v0

    .line 102
    .local v0, "localText":Ljava/lang/String;
    invoke-static {v0}, Lorg/apache/index/poi/hslf/record/Record;->setTextToUser(Ljava/lang/String;)V

    .line 103
    iput-object v0, p0, Lorg/apache/index/poi/hslf/record/CString;->_NormalText:Ljava/lang/String;

    .line 104
    const/4 v1, 0x0

    iput-object v1, p0, Lorg/apache/index/poi/hslf/record/CString;->_text:[B

    .line 106
    return-void
.end method


# virtual methods
.method public getNormalText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/CString;->_NormalText:Ljava/lang/String;

    return-object v0
.end method

.method public getOptions()I
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/CString;->_header:[B

    invoke-static {v0}, Lorg/apache/index/poi/util/LittleEndian;->getShort([B)S

    move-result v0

    return v0
.end method

.method public getRecordType()J
    .locals 2

    .prologue
    .line 120
    sget-wide v0, Lorg/apache/index/poi/hslf/record/CString;->_type:J

    return-wide v0
.end method

.method public getText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/CString;->_text:[B

    invoke-static {v0}, Lorg/apache/index/poi/util/StringUtil;->getFromUnicodeLE([B)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public setNormalTextNull()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 78
    iput-object v0, p0, Lorg/apache/index/poi/hslf/record/CString;->_NormalText:Ljava/lang/String;

    .line 79
    iput-object v0, p0, Lorg/apache/index/poi/hslf/record/CString;->_text:[B

    .line 80
    return-void
.end method

.method public setOptions(I)V
    .locals 2
    .param p1, "count"    # I

    .prologue
    .line 70
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/CString;->_header:[B

    int-to-short v1, p1

    invoke-static {v0, v1}, Lorg/apache/index/poi/util/LittleEndian;->putShort([BS)V

    .line 71
    return-void
.end method

.method public setText(Ljava/lang/String;)V
    .locals 3
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 50
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    mul-int/lit8 v0, v0, 0x2

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/apache/index/poi/hslf/record/CString;->_text:[B

    .line 51
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/CString;->_text:[B

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lorg/apache/index/poi/util/StringUtil;->putUnicodeLE(Ljava/lang/String;[BI)V

    .line 54
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/CString;->_header:[B

    const/4 v1, 0x4

    iget-object v2, p0, Lorg/apache/index/poi/hslf/record/CString;->_text:[B

    array-length v2, v2

    invoke-static {v0, v1, v2}, Lorg/apache/index/poi/util/LittleEndian;->putInt([BII)V

    .line 55
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 139
    invoke-virtual {p0}, Lorg/apache/index/poi/hslf/record/CString;->getText()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeOut(Ljava/io/OutputStream;)V
    .locals 1
    .param p1, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 128
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/CString;->_header:[B

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 131
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/CString;->_text:[B

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 132
    return-void
.end method
