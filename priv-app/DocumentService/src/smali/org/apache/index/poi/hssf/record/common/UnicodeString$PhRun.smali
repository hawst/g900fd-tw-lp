.class public Lorg/apache/index/poi/hssf/record/common/UnicodeString$PhRun;
.super Ljava/lang/Object;
.source "UnicodeString.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/index/poi/hssf/record/common/UnicodeString;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "PhRun"
.end annotation


# instance fields
.field private phoneticTextFirstCharacterOffset:I

.field private realTextFirstCharacterOffset:I

.field private realTextLength:I


# direct methods
.method public constructor <init>(III)V
    .locals 0
    .param p1, "phoneticTextFirstCharacterOffset"    # I
    .param p2, "realTextFirstCharacterOffset"    # I
    .param p3, "realTextLength"    # I

    .prologue
    .line 303
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 305
    iput p1, p0, Lorg/apache/index/poi/hssf/record/common/UnicodeString$PhRun;->phoneticTextFirstCharacterOffset:I

    .line 306
    iput p2, p0, Lorg/apache/index/poi/hssf/record/common/UnicodeString$PhRun;->realTextFirstCharacterOffset:I

    .line 307
    iput p3, p0, Lorg/apache/index/poi/hssf/record/common/UnicodeString$PhRun;->realTextLength:I

    .line 308
    return-void
.end method

.method private constructor <init>(Lorg/apache/index/poi/util/LittleEndianInput;)V
    .locals 1
    .param p1, "in"    # Lorg/apache/index/poi/util/LittleEndianInput;

    .prologue
    .line 309
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 310
    invoke-interface {p1}, Lorg/apache/index/poi/util/LittleEndianInput;->readUShort()I

    move-result v0

    iput v0, p0, Lorg/apache/index/poi/hssf/record/common/UnicodeString$PhRun;->phoneticTextFirstCharacterOffset:I

    .line 311
    invoke-interface {p1}, Lorg/apache/index/poi/util/LittleEndianInput;->readUShort()I

    move-result v0

    iput v0, p0, Lorg/apache/index/poi/hssf/record/common/UnicodeString$PhRun;->realTextFirstCharacterOffset:I

    .line 312
    invoke-interface {p1}, Lorg/apache/index/poi/util/LittleEndianInput;->readUShort()I

    move-result v0

    iput v0, p0, Lorg/apache/index/poi/hssf/record/common/UnicodeString$PhRun;->realTextLength:I

    .line 313
    return-void
.end method

.method synthetic constructor <init>(Lorg/apache/index/poi/util/LittleEndianInput;Lorg/apache/index/poi/hssf/record/common/UnicodeString$PhRun;)V
    .locals 0

    .prologue
    .line 309
    invoke-direct {p0, p1}, Lorg/apache/index/poi/hssf/record/common/UnicodeString$PhRun;-><init>(Lorg/apache/index/poi/util/LittleEndianInput;)V

    return-void
.end method

.method static synthetic access$1(Lorg/apache/index/poi/hssf/record/common/UnicodeString$PhRun;Lorg/apache/index/poi/hssf/record/cont/ContinuableRecordOutput;)V
    .locals 0

    .prologue
    .line 314
    invoke-direct {p0, p1}, Lorg/apache/index/poi/hssf/record/common/UnicodeString$PhRun;->serialize(Lorg/apache/index/poi/hssf/record/cont/ContinuableRecordOutput;)V

    return-void
.end method

.method static synthetic access$2(Lorg/apache/index/poi/hssf/record/common/UnicodeString$PhRun;)I
    .locals 1

    .prologue
    .line 299
    iget v0, p0, Lorg/apache/index/poi/hssf/record/common/UnicodeString$PhRun;->phoneticTextFirstCharacterOffset:I

    return v0
.end method

.method static synthetic access$3(Lorg/apache/index/poi/hssf/record/common/UnicodeString$PhRun;)I
    .locals 1

    .prologue
    .line 300
    iget v0, p0, Lorg/apache/index/poi/hssf/record/common/UnicodeString$PhRun;->realTextFirstCharacterOffset:I

    return v0
.end method

.method static synthetic access$4(Lorg/apache/index/poi/hssf/record/common/UnicodeString$PhRun;)I
    .locals 1

    .prologue
    .line 301
    iget v0, p0, Lorg/apache/index/poi/hssf/record/common/UnicodeString$PhRun;->realTextLength:I

    return v0
.end method

.method private serialize(Lorg/apache/index/poi/hssf/record/cont/ContinuableRecordOutput;)V
    .locals 1
    .param p1, "out"    # Lorg/apache/index/poi/hssf/record/cont/ContinuableRecordOutput;

    .prologue
    .line 315
    const/4 v0, 0x6

    invoke-virtual {p1, v0}, Lorg/apache/index/poi/hssf/record/cont/ContinuableRecordOutput;->writeContinueIfRequired(I)V

    .line 316
    iget v0, p0, Lorg/apache/index/poi/hssf/record/common/UnicodeString$PhRun;->phoneticTextFirstCharacterOffset:I

    invoke-virtual {p1, v0}, Lorg/apache/index/poi/hssf/record/cont/ContinuableRecordOutput;->writeShort(I)V

    .line 317
    iget v0, p0, Lorg/apache/index/poi/hssf/record/common/UnicodeString$PhRun;->realTextFirstCharacterOffset:I

    invoke-virtual {p1, v0}, Lorg/apache/index/poi/hssf/record/cont/ContinuableRecordOutput;->writeShort(I)V

    .line 318
    iget v0, p0, Lorg/apache/index/poi/hssf/record/common/UnicodeString$PhRun;->realTextLength:I

    invoke-virtual {p1, v0}, Lorg/apache/index/poi/hssf/record/cont/ContinuableRecordOutput;->writeShort(I)V

    .line 319
    return-void
.end method
