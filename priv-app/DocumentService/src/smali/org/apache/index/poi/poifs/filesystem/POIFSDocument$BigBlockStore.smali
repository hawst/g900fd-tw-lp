.class final Lorg/apache/index/poi/poifs/filesystem/POIFSDocument$BigBlockStore;
.super Ljava/lang/Object;
.source "POIFSDocument.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/index/poi/poifs/filesystem/POIFSDocument;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "BigBlockStore"
.end annotation


# instance fields
.field private bigBlocks:[Lorg/apache/index/poi/poifs/storage/DocumentBlock;


# direct methods
.method constructor <init>(Lorg/apache/index/poi/poifs/common/POIFSBigBlockSize;Lorg/apache/index/poi/poifs/filesystem/POIFSDocumentPath;Ljava/lang/String;I)V
    .locals 1
    .param p1, "bigBlockSize"    # Lorg/apache/index/poi/poifs/common/POIFSBigBlockSize;
    .param p2, "path"    # Lorg/apache/index/poi/poifs/filesystem/POIFSDocumentPath;
    .param p3, "name"    # Ljava/lang/String;
    .param p4, "size"    # I

    .prologue
    .line 441
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 443
    const/4 v0, 0x0

    new-array v0, v0, [Lorg/apache/index/poi/poifs/storage/DocumentBlock;

    iput-object v0, p0, Lorg/apache/index/poi/poifs/filesystem/POIFSDocument$BigBlockStore;->bigBlocks:[Lorg/apache/index/poi/poifs/storage/DocumentBlock;

    .line 444
    return-void
.end method

.method constructor <init>(Lorg/apache/index/poi/poifs/common/POIFSBigBlockSize;[Lorg/apache/index/poi/poifs/storage/DocumentBlock;)V
    .locals 1
    .param p1, "bigBlockSize"    # Lorg/apache/index/poi/poifs/common/POIFSBigBlockSize;
    .param p2, "blocks"    # [Lorg/apache/index/poi/poifs/storage/DocumentBlock;

    .prologue
    .line 429
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 430
    invoke-virtual {p2}, [Lorg/apache/index/poi/poifs/storage/DocumentBlock;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lorg/apache/index/poi/poifs/storage/DocumentBlock;

    iput-object v0, p0, Lorg/apache/index/poi/poifs/filesystem/POIFSDocument$BigBlockStore;->bigBlocks:[Lorg/apache/index/poi/poifs/storage/DocumentBlock;

    .line 431
    return-void
.end method


# virtual methods
.method countBlocks()I
    .locals 1

    .prologue
    .line 474
    const/4 v0, 0x0

    return v0
.end method

.method getBlocks()[Lorg/apache/index/poi/poifs/storage/DocumentBlock;
    .locals 1

    .prologue
    .line 457
    iget-object v0, p0, Lorg/apache/index/poi/poifs/filesystem/POIFSDocument$BigBlockStore;->bigBlocks:[Lorg/apache/index/poi/poifs/storage/DocumentBlock;

    return-object v0
.end method

.method isValid()Z
    .locals 1

    .prologue
    .line 450
    iget-object v0, p0, Lorg/apache/index/poi/poifs/filesystem/POIFSDocument$BigBlockStore;->bigBlocks:[Lorg/apache/index/poi/poifs/storage/DocumentBlock;

    array-length v0, v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method writeBlocks(Ljava/io/OutputStream;)V
    .locals 0
    .param p1, "stream"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 468
    return-void
.end method
