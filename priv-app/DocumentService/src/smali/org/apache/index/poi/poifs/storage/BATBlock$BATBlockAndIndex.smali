.class public Lorg/apache/index/poi/poifs/storage/BATBlock$BATBlockAndIndex;
.super Ljava/lang/Object;
.source "BATBlock.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/index/poi/poifs/storage/BATBlock;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "BATBlockAndIndex"
.end annotation


# instance fields
.field private final block:Lorg/apache/index/poi/poifs/storage/BATBlock;

.field private final index:I


# direct methods
.method private constructor <init>(ILorg/apache/index/poi/poifs/storage/BATBlock;)V
    .locals 0
    .param p1, "index"    # I
    .param p2, "block"    # Lorg/apache/index/poi/poifs/storage/BATBlock;

    .prologue
    .line 390
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 391
    iput p1, p0, Lorg/apache/index/poi/poifs/storage/BATBlock$BATBlockAndIndex;->index:I

    .line 392
    iput-object p2, p0, Lorg/apache/index/poi/poifs/storage/BATBlock$BATBlockAndIndex;->block:Lorg/apache/index/poi/poifs/storage/BATBlock;

    .line 393
    return-void
.end method

.method synthetic constructor <init>(ILorg/apache/index/poi/poifs/storage/BATBlock;Lorg/apache/index/poi/poifs/storage/BATBlock$BATBlockAndIndex;)V
    .locals 0

    .prologue
    .line 390
    invoke-direct {p0, p1, p2}, Lorg/apache/index/poi/poifs/storage/BATBlock$BATBlockAndIndex;-><init>(ILorg/apache/index/poi/poifs/storage/BATBlock;)V

    return-void
.end method


# virtual methods
.method public getBlock()Lorg/apache/index/poi/poifs/storage/BATBlock;
    .locals 1

    .prologue
    .line 398
    iget-object v0, p0, Lorg/apache/index/poi/poifs/storage/BATBlock$BATBlockAndIndex;->block:Lorg/apache/index/poi/poifs/storage/BATBlock;

    return-object v0
.end method

.method public getIndex()I
    .locals 1

    .prologue
    .line 395
    iget v0, p0, Lorg/apache/index/poi/poifs/storage/BATBlock$BATBlockAndIndex;->index:I

    return v0
.end method
