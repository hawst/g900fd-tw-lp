.class public Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;
.super Lorg/apache/index/poi/poifs/filesystem/EntryNode;
.source "DirectoryNode.java"

# interfaces
.implements Ljava/lang/Iterable;
.implements Lorg/apache/index/poi/poifs/filesystem/DirectoryEntry;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/apache/index/poi/poifs/filesystem/EntryNode;",
        "Lorg/apache/index/poi/poifs/filesystem/DirectoryEntry;",
        "Ljava/lang/Iterable",
        "<",
        "Lorg/apache/index/poi/poifs/filesystem/Entry;",
        ">;"
    }
.end annotation


# instance fields
.field private _byname:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lorg/apache/index/poi/poifs/filesystem/Entry;",
            ">;"
        }
    .end annotation
.end field

.field private _entries:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lorg/apache/index/poi/poifs/filesystem/Entry;",
            ">;"
        }
    .end annotation
.end field

.field private _ofilesystem:Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;

.field private _path:Lorg/apache/index/poi/poifs/filesystem/POIFSDocumentPath;


# direct methods
.method private constructor <init>(Lorg/apache/index/poi/poifs/property/DirectoryProperty;Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;)V
    .locals 9
    .param p1, "property"    # Lorg/apache/index/poi/poifs/property/DirectoryProperty;
    .param p2, "parent"    # Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;
    .param p3, "ofilesystem"    # Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;

    .prologue
    .line 95
    invoke-direct {p0, p1, p2}, Lorg/apache/index/poi/poifs/filesystem/EntryNode;-><init>(Lorg/apache/index/poi/poifs/property/Property;Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;)V

    .line 96
    iput-object p3, p0, Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;->_ofilesystem:Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;

    .line 99
    if-nez p2, :cond_1

    .line 101
    new-instance v4, Lorg/apache/index/poi/poifs/filesystem/POIFSDocumentPath;

    invoke-direct {v4}, Lorg/apache/index/poi/poifs/filesystem/POIFSDocumentPath;-><init>()V

    iput-object v4, p0, Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;->_path:Lorg/apache/index/poi/poifs/filesystem/POIFSDocumentPath;

    .line 110
    :goto_0
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    iput-object v4, p0, Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;->_byname:Ljava/util/Map;

    .line 111
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;->_entries:Ljava/util/ArrayList;

    .line 112
    invoke-virtual {p1}, Lorg/apache/index/poi/poifs/property/DirectoryProperty;->getChildren()Ljava/util/Iterator;

    move-result-object v3

    .line 114
    .local v3, "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/index/poi/poifs/property/Property;>;"
    :cond_0
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_2

    .line 139
    return-void

    .line 105
    .end local v3    # "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/index/poi/poifs/property/Property;>;"
    :cond_1
    new-instance v4, Lorg/apache/index/poi/poifs/filesystem/POIFSDocumentPath;

    iget-object v5, p2, Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;->_path:Lorg/apache/index/poi/poifs/filesystem/POIFSDocumentPath;

    .line 106
    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    .line 107
    invoke-virtual {p1}, Lorg/apache/index/poi/poifs/property/DirectoryProperty;->getName()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-direct {v4, v5, v6}, Lorg/apache/index/poi/poifs/filesystem/POIFSDocumentPath;-><init>(Lorg/apache/index/poi/poifs/filesystem/POIFSDocumentPath;[Ljava/lang/String;)V

    .line 105
    iput-object v4, p0, Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;->_path:Lorg/apache/index/poi/poifs/filesystem/POIFSDocumentPath;

    goto :goto_0

    .line 116
    .restart local v3    # "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/index/poi/poifs/property/Property;>;"
    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/index/poi/poifs/property/Property;

    .line 117
    .local v0, "child":Lorg/apache/index/poi/poifs/property/Property;
    const/4 v2, 0x0

    .line 119
    .local v2, "childNode":Lorg/apache/index/poi/poifs/filesystem/Entry;
    invoke-virtual {v0}, Lorg/apache/index/poi/poifs/property/Property;->isDirectory()Z

    move-result v4

    if-eqz v4, :cond_4

    move-object v1, v0

    .line 121
    check-cast v1, Lorg/apache/index/poi/poifs/property/DirectoryProperty;

    .line 122
    .local v1, "childDir":Lorg/apache/index/poi/poifs/property/DirectoryProperty;
    iget-object v4, p0, Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;->_ofilesystem:Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;

    if-eqz v4, :cond_3

    .line 123
    new-instance v2, Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;

    .end local v2    # "childNode":Lorg/apache/index/poi/poifs/filesystem/Entry;
    iget-object v4, p0, Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;->_ofilesystem:Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;

    invoke-direct {v2, v1, v4, p0}, Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;-><init>(Lorg/apache/index/poi/poifs/property/DirectoryProperty;Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;)V

    .line 134
    .end local v0    # "child":Lorg/apache/index/poi/poifs/property/Property;
    .end local v1    # "childDir":Lorg/apache/index/poi/poifs/property/DirectoryProperty;
    .restart local v2    # "childNode":Lorg/apache/index/poi/poifs/filesystem/Entry;
    :cond_3
    :goto_2
    iget-object v4, p0, Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;->_entries:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 135
    if-eqz v2, :cond_0

    .line 136
    iget-object v4, p0, Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;->_byname:Ljava/util/Map;

    invoke-interface {v2}, Lorg/apache/index/poi/poifs/filesystem/Entry;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 132
    .restart local v0    # "child":Lorg/apache/index/poi/poifs/property/Property;
    :cond_4
    new-instance v2, Lorg/apache/index/poi/poifs/filesystem/DocumentNode;

    .end local v2    # "childNode":Lorg/apache/index/poi/poifs/filesystem/Entry;
    check-cast v0, Lorg/apache/index/poi/poifs/property/DocumentProperty;

    .end local v0    # "child":Lorg/apache/index/poi/poifs/property/Property;
    invoke-direct {v2, v0, p0}, Lorg/apache/index/poi/poifs/filesystem/DocumentNode;-><init>(Lorg/apache/index/poi/poifs/property/DocumentProperty;Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;)V

    .restart local v2    # "childNode":Lorg/apache/index/poi/poifs/filesystem/Entry;
    goto :goto_2
.end method

.method constructor <init>(Lorg/apache/index/poi/poifs/property/DirectoryProperty;Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;)V
    .locals 0
    .param p1, "property"    # Lorg/apache/index/poi/poifs/property/DirectoryProperty;
    .param p2, "filesystem"    # Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;
    .param p3, "parent"    # Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;

    .prologue
    .line 72
    invoke-direct {p0, p1, p3, p2}, Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;-><init>(Lorg/apache/index/poi/poifs/property/DirectoryProperty;Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;)V

    .line 73
    return-void
.end method


# virtual methods
.method changeName(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 4
    .param p1, "oldName"    # Ljava/lang/String;
    .param p2, "newName"    # Ljava/lang/String;

    .prologue
    .line 262
    const/4 v1, 0x0

    .line 263
    .local v1, "rval":Z
    iget-object v2, p0, Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;->_byname:Ljava/util/Map;

    invoke-interface {v2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/index/poi/poifs/filesystem/EntryNode;

    .line 265
    .local v0, "child":Lorg/apache/index/poi/poifs/filesystem/EntryNode;
    if-eqz v0, :cond_0

    .line 267
    invoke-virtual {p0}, Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;->getProperty()Lorg/apache/index/poi/poifs/property/Property;

    move-result-object v2

    check-cast v2, Lorg/apache/index/poi/poifs/property/DirectoryProperty;

    .line 268
    invoke-virtual {v0}, Lorg/apache/index/poi/poifs/filesystem/EntryNode;->getProperty()Lorg/apache/index/poi/poifs/property/Property;

    move-result-object v3

    invoke-virtual {v2, v3, p2}, Lorg/apache/index/poi/poifs/property/DirectoryProperty;->changeName(Lorg/apache/index/poi/poifs/property/Property;Ljava/lang/String;)Z

    move-result v1

    .line 269
    if-eqz v1, :cond_0

    .line 271
    iget-object v2, p0, Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;->_byname:Ljava/util/Map;

    invoke-interface {v2, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 272
    iget-object v2, p0, Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;->_byname:Ljava/util/Map;

    invoke-virtual {v0}, Lorg/apache/index/poi/poifs/filesystem/EntryNode;->getProperty()Lorg/apache/index/poi/poifs/property/Property;

    move-result-object v3

    invoke-virtual {v3}, Lorg/apache/index/poi/poifs/property/Property;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 275
    :cond_0
    return v1
.end method

.method public createDirectory(Ljava/lang/String;)Lorg/apache/index/poi/poifs/filesystem/DirectoryEntry;
    .locals 3
    .param p1, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 412
    const/4 v1, 0x0

    .line 413
    .local v1, "rval":Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;
    new-instance v0, Lorg/apache/index/poi/poifs/property/DirectoryProperty;

    invoke-direct {v0, p1}, Lorg/apache/index/poi/poifs/property/DirectoryProperty;-><init>(Ljava/lang/String;)V

    .line 415
    .local v0, "property":Lorg/apache/index/poi/poifs/property/DirectoryProperty;
    iget-object v2, p0, Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;->_ofilesystem:Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;

    if-eqz v2, :cond_0

    .line 416
    new-instance v1, Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;

    .end local v1    # "rval":Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;
    iget-object v2, p0, Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;->_ofilesystem:Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;

    invoke-direct {v1, v0, v2, p0}, Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;-><init>(Lorg/apache/index/poi/poifs/property/DirectoryProperty;Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;)V

    .line 417
    .restart local v1    # "rval":Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;
    iget-object v2, p0, Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;->_ofilesystem:Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;

    invoke-virtual {v2, v0}, Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;->addDirectory(Lorg/apache/index/poi/poifs/property/DirectoryProperty;)V

    .line 425
    :cond_0
    invoke-virtual {p0}, Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;->getProperty()Lorg/apache/index/poi/poifs/property/Property;

    move-result-object v2

    check-cast v2, Lorg/apache/index/poi/poifs/property/DirectoryProperty;

    invoke-virtual {v2, v0}, Lorg/apache/index/poi/poifs/property/DirectoryProperty;->addChild(Lorg/apache/index/poi/poifs/property/Property;)V

    .line 426
    iget-object v2, p0, Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;->_entries:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 427
    iget-object v2, p0, Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;->_byname:Ljava/util/Map;

    invoke-interface {v2, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 428
    return-object v1
.end method

.method public createDocument(Ljava/lang/String;Ljava/io/InputStream;)Lorg/apache/index/poi/poifs/filesystem/DocumentEntry;
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "stream"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 396
    new-instance v0, Lorg/apache/index/poi/poifs/filesystem/POIFSDocument;

    invoke-direct {v0, p1, p2}, Lorg/apache/index/poi/poifs/filesystem/POIFSDocument;-><init>(Ljava/lang/String;Ljava/io/InputStream;)V

    invoke-virtual {p0, v0}, Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;->createDocument(Lorg/apache/index/poi/poifs/filesystem/POIFSDocument;)Lorg/apache/index/poi/poifs/filesystem/DocumentEntry;

    move-result-object v0

    return-object v0
.end method

.method createDocument(Lorg/apache/index/poi/poifs/filesystem/POIFSDocument;)Lorg/apache/index/poi/poifs/filesystem/DocumentEntry;
    .locals 4
    .param p1, "document"    # Lorg/apache/index/poi/poifs/filesystem/POIFSDocument;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 218
    invoke-virtual {p1}, Lorg/apache/index/poi/poifs/filesystem/POIFSDocument;->getDocumentProperty()Lorg/apache/index/poi/poifs/property/DocumentProperty;

    move-result-object v0

    .line 219
    .local v0, "property":Lorg/apache/index/poi/poifs/property/DocumentProperty;
    new-instance v1, Lorg/apache/index/poi/poifs/filesystem/DocumentNode;

    invoke-direct {v1, v0, p0}, Lorg/apache/index/poi/poifs/filesystem/DocumentNode;-><init>(Lorg/apache/index/poi/poifs/property/DocumentProperty;Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;)V

    .line 221
    .local v1, "rval":Lorg/apache/index/poi/poifs/filesystem/DocumentNode;
    invoke-virtual {p0}, Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;->getProperty()Lorg/apache/index/poi/poifs/property/Property;

    move-result-object v2

    check-cast v2, Lorg/apache/index/poi/poifs/property/DirectoryProperty;

    invoke-virtual {v2, v0}, Lorg/apache/index/poi/poifs/property/DirectoryProperty;->addChild(Lorg/apache/index/poi/poifs/property/Property;)V

    .line 222
    iget-object v2, p0, Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;->_ofilesystem:Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;

    invoke-virtual {v2, p1}, Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;->addDocument(Lorg/apache/index/poi/poifs/filesystem/POIFSDocument;)V

    .line 224
    iget-object v2, p0, Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;->_entries:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 225
    iget-object v2, p0, Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;->_byname:Ljava/util/Map;

    invoke-virtual {v0}, Lorg/apache/index/poi/poifs/property/DocumentProperty;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 226
    return-object v1
.end method

.method public createDocumentInputStream(Ljava/lang/String;)Lorg/apache/index/poi/poifs/filesystem/DocumentInputStream;
    .locals 1
    .param p1, "documentName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 180
    invoke-virtual {p0, p1}, Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;->getEntry(Ljava/lang/String;)Lorg/apache/index/poi/poifs/filesystem/Entry;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;->createDocumentInputStream(Lorg/apache/index/poi/poifs/filesystem/Entry;)Lorg/apache/index/poi/poifs/filesystem/DocumentInputStream;

    move-result-object v0

    return-object v0
.end method

.method public createDocumentInputStream(Lorg/apache/index/poi/poifs/filesystem/Entry;)Lorg/apache/index/poi/poifs/filesystem/DocumentInputStream;
    .locals 4
    .param p1, "document"    # Lorg/apache/index/poi/poifs/filesystem/Entry;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 197
    invoke-interface {p1}, Lorg/apache/index/poi/poifs/filesystem/Entry;->isDocumentEntry()Z

    move-result v1

    if-nez v1, :cond_0

    .line 198
    new-instance v1, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Entry \'"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p1}, Lorg/apache/index/poi/poifs/filesystem/Entry;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 199
    const-string/jumbo v3, "\' is not a DocumentEntry"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 198
    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    move-object v0, p1

    .line 202
    check-cast v0, Lorg/apache/index/poi/poifs/filesystem/DocumentEntry;

    .line 203
    .local v0, "entry":Lorg/apache/index/poi/poifs/filesystem/DocumentEntry;
    new-instance v1, Lorg/apache/index/poi/poifs/filesystem/DocumentInputStream;

    invoke-direct {v1, v0}, Lorg/apache/index/poi/poifs/filesystem/DocumentInputStream;-><init>(Lorg/apache/index/poi/poifs/filesystem/DocumentEntry;)V

    return-object v1
.end method

.method deleteEntry(Lorg/apache/index/poi/poifs/filesystem/EntryNode;)Z
    .locals 3
    .param p1, "entry"    # Lorg/apache/index/poi/poifs/filesystem/EntryNode;

    .prologue
    .line 289
    invoke-virtual {p0}, Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;->getProperty()Lorg/apache/index/poi/poifs/property/Property;

    move-result-object v1

    check-cast v1, Lorg/apache/index/poi/poifs/property/DirectoryProperty;

    .line 290
    invoke-virtual {p1}, Lorg/apache/index/poi/poifs/filesystem/EntryNode;->getProperty()Lorg/apache/index/poi/poifs/property/Property;

    move-result-object v2

    invoke-virtual {v1, v2}, Lorg/apache/index/poi/poifs/property/DirectoryProperty;->deleteChild(Lorg/apache/index/poi/poifs/property/Property;)Z

    move-result v0

    .line 292
    .local v0, "rval":Z
    if-eqz v0, :cond_0

    .line 294
    iget-object v1, p0, Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;->_entries:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 295
    iget-object v1, p0, Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;->_byname:Ljava/util/Map;

    invoke-virtual {p1}, Lorg/apache/index/poi/poifs/filesystem/EntryNode;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 297
    iget-object v1, p0, Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;->_ofilesystem:Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;

    if-eqz v1, :cond_0

    .line 298
    iget-object v1, p0, Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;->_ofilesystem:Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;

    invoke-virtual {v1, p1}, Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;->remove(Lorg/apache/index/poi/poifs/filesystem/EntryNode;)V

    .line 304
    :cond_0
    return v0
.end method

.method public getEntries()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Lorg/apache/index/poi/poifs/filesystem/Entry;",
            ">;"
        }
    .end annotation

    .prologue
    .line 322
    iget-object v0, p0, Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;->_entries:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public getEntry(Ljava/lang/String;)Lorg/apache/index/poi/poifs/filesystem/Entry;
    .locals 4
    .param p1, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .prologue
    .line 364
    const/4 v0, 0x0

    .line 366
    .local v0, "rval":Lorg/apache/index/poi/poifs/filesystem/Entry;
    if-eqz p1, :cond_0

    .line 368
    iget-object v1, p0, Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;->_byname:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "rval":Lorg/apache/index/poi/poifs/filesystem/Entry;
    check-cast v0, Lorg/apache/index/poi/poifs/filesystem/Entry;

    .line 370
    .restart local v0    # "rval":Lorg/apache/index/poi/poifs/filesystem/Entry;
    :cond_0
    if-nez v0, :cond_1

    .line 374
    new-instance v1, Ljava/io/FileNotFoundException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "no such entry: \""

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 375
    const-string/jumbo v3, "\""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 374
    invoke-direct {v1, v2}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 377
    :cond_1
    return-object v0
.end method

.method public getEntryCount()I
    .locals 1

    .prologue
    .line 346
    iget-object v0, p0, Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;->_entries:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getFileSystem()Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;
    .locals 1

    .prologue
    .line 155
    iget-object v0, p0, Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;->_ofilesystem:Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;

    return-object v0
.end method

.method public getPath()Lorg/apache/index/poi/poifs/filesystem/POIFSDocumentPath;
    .locals 1

    .prologue
    .line 147
    iget-object v0, p0, Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;->_path:Lorg/apache/index/poi/poifs/filesystem/POIFSDocumentPath;

    return-object v0
.end method

.method public getShortDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 541
    invoke-virtual {p0}, Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getStorageClsid()Lorg/apache/index/poi/hpsf/ClassID;
    .locals 1

    .prologue
    .line 438
    invoke-virtual {p0}, Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;->getProperty()Lorg/apache/index/poi/poifs/property/Property;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/index/poi/poifs/property/Property;->getStorageClsid()Lorg/apache/index/poi/hpsf/ClassID;

    move-result-object v0

    return-object v0
.end method

.method public getViewableArray()[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 495
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    return-object v0
.end method

.method public getViewableIterator()Ljava/util/Iterator;
    .locals 3

    .prologue
    .line 508
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 510
    .local v0, "components":Ljava/util/List;
    invoke-virtual {p0}, Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;->getProperty()Lorg/apache/index/poi/poifs/property/Property;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 511
    iget-object v2, p0, Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;->_entries:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 512
    .local v1, "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/index/poi/poifs/filesystem/Entry;>;"
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 516
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    return-object v2

    .line 514
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method protected isDeleteOK()Z
    .locals 1

    .prologue
    .line 480
    invoke-virtual {p0}, Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public isDirectoryEntry()Z
    .locals 1

    .prologue
    .line 462
    const/4 v0, 0x1

    return v0
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 333
    iget-object v0, p0, Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;->_entries:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Lorg/apache/index/poi/poifs/filesystem/Entry;",
            ">;"
        }
    .end annotation

    .prologue
    .line 548
    invoke-virtual {p0}, Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;->getEntries()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public preferArray()Z
    .locals 1

    .prologue
    .line 529
    const/4 v0, 0x0

    return v0
.end method

.method public setStorageClsid(Lorg/apache/index/poi/hpsf/ClassID;)V
    .locals 1
    .param p1, "clsidStorage"    # Lorg/apache/index/poi/hpsf/ClassID;

    .prologue
    .line 448
    invoke-virtual {p0}, Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;->getProperty()Lorg/apache/index/poi/poifs/property/Property;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/index/poi/poifs/property/Property;->setStorageClsid(Lorg/apache/index/poi/hpsf/ClassID;)V

    .line 449
    return-void
.end method
