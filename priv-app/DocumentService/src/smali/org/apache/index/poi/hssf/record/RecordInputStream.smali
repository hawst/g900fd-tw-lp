.class public final Lorg/apache/index/poi/hssf/record/RecordInputStream;
.super Ljava/lang/Object;
.source "RecordInputStream.java"

# interfaces
.implements Lorg/apache/index/poi/util/LittleEndianInput;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/index/poi/hssf/record/RecordInputStream$LeftoverDataException;,
        Lorg/apache/index/poi/hssf/record/RecordInputStream$SimpleHeaderInput;
    }
.end annotation


# static fields
.field private static final DATA_LEN_NEEDS_TO_BE_READ:I = -0x1

.field private static final EMPTY_BYTE_ARRAY:[B

.field private static final INVALID_SID_VALUE:I = -0x1

.field public static final MAX_RECORD_DATA_SIZE:S = 0x2020s


# instance fields
.field private final _bhi:Lorg/apache/index/poi/hssf/record/BiffHeaderInput;

.field private _currentDataLength:I

.field private _currentDataOffset:I

.field private _currentSid:I

.field private final _dataInput:Lorg/apache/index/poi/util/LittleEndianInput;

.field private _nextSid:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 45
    const/4 v0, 0x0

    new-array v0, v0, [B

    sput-object v0, Lorg/apache/index/poi/hssf/record/RecordInputStream;->EMPTY_BYTE_ARRAY:[B

    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;I)V
    .locals 1
    .param p1, "in"    # Ljava/io/InputStream;
    .param p2, "initialOffset"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/index/poi/hssf/record/RecordFormatException;
        }
    .end annotation

    .prologue
    .line 106
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 107
    invoke-static {p1}, Lorg/apache/index/poi/hssf/record/RecordInputStream;->getLEI(Ljava/io/InputStream;)Lorg/apache/index/poi/util/LittleEndianInput;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/index/poi/hssf/record/RecordInputStream;->_dataInput:Lorg/apache/index/poi/util/LittleEndianInput;

    .line 108
    new-instance v0, Lorg/apache/index/poi/hssf/record/RecordInputStream$SimpleHeaderInput;

    invoke-direct {v0, p1}, Lorg/apache/index/poi/hssf/record/RecordInputStream$SimpleHeaderInput;-><init>(Ljava/io/InputStream;)V

    iput-object v0, p0, Lorg/apache/index/poi/hssf/record/RecordInputStream;->_bhi:Lorg/apache/index/poi/hssf/record/BiffHeaderInput;

    .line 110
    invoke-direct {p0}, Lorg/apache/index/poi/hssf/record/RecordInputStream;->readNextSid()I

    move-result v0

    iput v0, p0, Lorg/apache/index/poi/hssf/record/RecordInputStream;->_nextSid:I

    .line 111
    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;Lcom/samsung/index/ITextContentObs;)V
    .locals 1
    .param p1, "in"    # Ljava/io/InputStream;
    .param p2, "indexWriterhssf"    # Lcom/samsung/index/ITextContentObs;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/index/poi/hssf/record/RecordFormatException;
        }
    .end annotation

    .prologue
    .line 103
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lorg/apache/index/poi/hssf/record/RecordInputStream;-><init>(Ljava/io/InputStream;I)V

    .line 104
    return-void
.end method

.method private checkRecordPosition(I)V
    .locals 4
    .param p1, "requiredByteCount"    # I

    .prologue
    .line 204
    invoke-virtual {p0}, Lorg/apache/index/poi/hssf/record/RecordInputStream;->remaining()I

    move-result v0

    .line 205
    .local v0, "nAvailable":I
    if-lt v0, p1, :cond_0

    .line 211
    :goto_0
    return-void

    .line 209
    :cond_0
    if-nez v0, :cond_1

    invoke-direct {p0}, Lorg/apache/index/poi/hssf/record/RecordInputStream;->isContinueNext()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 210
    invoke-virtual {p0}, Lorg/apache/index/poi/hssf/record/RecordInputStream;->nextRecord()V

    goto :goto_0

    .line 213
    :cond_1
    new-instance v1, Lorg/apache/index/poi/hssf/record/RecordFormatException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Not enough data ("

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 214
    const-string/jumbo v3, ") to read requested ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ") bytes"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 213
    invoke-direct {v1, v2}, Lorg/apache/index/poi/hssf/record/RecordFormatException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method static getLEI(Ljava/io/InputStream;)Lorg/apache/index/poi/util/LittleEndianInput;
    .locals 1
    .param p0, "is"    # Ljava/io/InputStream;

    .prologue
    .line 114
    instance-of v0, p0, Lorg/apache/index/poi/util/LittleEndianInput;

    if-eqz v0, :cond_0

    .line 116
    check-cast p0, Lorg/apache/index/poi/util/LittleEndianInput;

    .line 119
    .end local p0    # "is":Ljava/io/InputStream;
    :goto_0
    return-object p0

    .restart local p0    # "is":Ljava/io/InputStream;
    :cond_0
    new-instance v0, Lorg/apache/index/poi/util/LittleEndianInputStream;

    invoke-direct {v0, p0}, Lorg/apache/index/poi/util/LittleEndianInputStream;-><init>(Ljava/io/InputStream;)V

    move-object p0, v0

    goto :goto_0
.end method

.method private isContinueNext()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 419
    iget v1, p0, Lorg/apache/index/poi/hssf/record/RecordInputStream;->_currentDataLength:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    iget v1, p0, Lorg/apache/index/poi/hssf/record/RecordInputStream;->_currentDataOffset:I

    iget v2, p0, Lorg/apache/index/poi/hssf/record/RecordInputStream;->_currentDataLength:I

    if-eq v1, v2, :cond_0

    .line 420
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Should never be called before end of current record"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 422
    :cond_0
    invoke-virtual {p0}, Lorg/apache/index/poi/hssf/record/RecordInputStream;->hasNextRecord()Z

    move-result v1

    if-nez v1, :cond_2

    .line 430
    :cond_1
    :goto_0
    return v0

    :cond_2
    iget v1, p0, Lorg/apache/index/poi/hssf/record/RecordInputStream;->_nextSid:I

    const/16 v2, 0x3c

    if-ne v1, v2, :cond_1

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private readNextSid()I
    .locals 5

    .prologue
    const/4 v2, -0x1

    .line 165
    iget-object v3, p0, Lorg/apache/index/poi/hssf/record/RecordInputStream;->_bhi:Lorg/apache/index/poi/hssf/record/BiffHeaderInput;

    invoke-interface {v3}, Lorg/apache/index/poi/hssf/record/BiffHeaderInput;->available()I

    move-result v0

    .line 166
    .local v0, "nAvailable":I
    const/4 v3, 0x4

    if-ge v0, v3, :cond_0

    move v1, v2

    .line 179
    :goto_0
    return v1

    .line 174
    :cond_0
    iget-object v3, p0, Lorg/apache/index/poi/hssf/record/RecordInputStream;->_bhi:Lorg/apache/index/poi/hssf/record/BiffHeaderInput;

    invoke-interface {v3}, Lorg/apache/index/poi/hssf/record/BiffHeaderInput;->readRecordSID()I

    move-result v1

    .line 175
    .local v1, "result":I
    if-ne v1, v2, :cond_1

    .line 176
    new-instance v2, Lorg/apache/index/poi/hssf/record/RecordFormatException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "Found invalid sid ("

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lorg/apache/index/poi/hssf/record/RecordFormatException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 178
    :cond_1
    iput v2, p0, Lorg/apache/index/poi/hssf/record/RecordInputStream;->_currentDataLength:I

    goto :goto_0
.end method

.method private readStringCommon(IZ)Ljava/lang/String;
    .locals 9
    .param p1, "requestedLength"    # I
    .param p2, "pIsCompressedEncoding"    # Z

    .prologue
    .line 317
    if-ltz p1, :cond_0

    const/high16 v6, 0x100000

    if-le p1, v6, :cond_1

    .line 318
    :cond_0
    new-instance v6, Ljava/lang/IllegalArgumentException;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string/jumbo v8, "Bad requested string length ("

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, ")"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 320
    :cond_1
    new-array v1, p1, [C

    .line 321
    .local v1, "buf":[C
    move v5, p2

    .line 322
    .local v5, "isCompressedEncoding":Z
    const/4 v4, 0x0

    .line 324
    .local v4, "curLen":I
    :goto_0
    if-eqz v5, :cond_2

    invoke-virtual {p0}, Lorg/apache/index/poi/hssf/record/RecordInputStream;->remaining()I

    move-result v0

    .line 325
    .local v0, "availableChars":I
    :goto_1
    sub-int v6, p1, v4

    if-gt v6, v0, :cond_6

    .line 327
    :goto_2
    if-lt v4, p1, :cond_3

    .line 337
    new-instance v6, Ljava/lang/String;

    invoke-direct {v6, v1}, Ljava/lang/String;-><init>([C)V

    return-object v6

    .line 324
    .end local v0    # "availableChars":I
    :cond_2
    invoke-virtual {p0}, Lorg/apache/index/poi/hssf/record/RecordInputStream;->remaining()I

    move-result v6

    div-int/lit8 v0, v6, 0x2

    goto :goto_1

    .line 329
    .restart local v0    # "availableChars":I
    :cond_3
    if-eqz v5, :cond_4

    .line 330
    invoke-virtual {p0}, Lorg/apache/index/poi/hssf/record/RecordInputStream;->readUByte()I

    move-result v6

    int-to-char v2, v6

    .line 334
    .local v2, "ch":C
    :goto_3
    aput-char v2, v1, v4

    .line 335
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 332
    .end local v2    # "ch":C
    :cond_4
    invoke-virtual {p0}, Lorg/apache/index/poi/hssf/record/RecordInputStream;->readShort()S

    move-result v6

    int-to-char v2, v6

    .restart local v2    # "ch":C
    goto :goto_3

    .line 343
    .end local v2    # "ch":C
    :cond_5
    if-eqz v5, :cond_7

    .line 344
    invoke-virtual {p0}, Lorg/apache/index/poi/hssf/record/RecordInputStream;->readUByte()I

    move-result v6

    int-to-char v2, v6

    .line 348
    .restart local v2    # "ch":C
    :goto_4
    aput-char v2, v1, v4

    .line 349
    add-int/lit8 v4, v4, 0x1

    .line 350
    add-int/lit8 v0, v0, -0x1

    .line 341
    .end local v2    # "ch":C
    :cond_6
    if-gtz v0, :cond_5

    .line 352
    invoke-direct {p0}, Lorg/apache/index/poi/hssf/record/RecordInputStream;->isContinueNext()Z

    move-result v6

    if-nez v6, :cond_8

    .line 353
    new-instance v6, Lorg/apache/index/poi/hssf/record/RecordFormatException;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string/jumbo v8, "Expected to find a ContinueRecord in order to read remaining "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 354
    sub-int v8, p1, v4

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, " of "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, " chars"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 353
    invoke-direct {v6, v7}, Lorg/apache/index/poi/hssf/record/RecordFormatException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 346
    :cond_7
    invoke-virtual {p0}, Lorg/apache/index/poi/hssf/record/RecordInputStream;->readShort()S

    move-result v6

    int-to-char v2, v6

    .restart local v2    # "ch":C
    goto :goto_4

    .line 356
    .end local v2    # "ch":C
    :cond_8
    invoke-virtual {p0}, Lorg/apache/index/poi/hssf/record/RecordInputStream;->remaining()I

    move-result v6

    if-eqz v6, :cond_9

    .line 357
    new-instance v6, Lorg/apache/index/poi/hssf/record/RecordFormatException;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string/jumbo v8, "Odd number of bytes("

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lorg/apache/index/poi/hssf/record/RecordInputStream;->remaining()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, ") left behind"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Lorg/apache/index/poi/hssf/record/RecordFormatException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 359
    :cond_9
    invoke-virtual {p0}, Lorg/apache/index/poi/hssf/record/RecordInputStream;->nextRecord()V

    .line 361
    invoke-virtual {p0}, Lorg/apache/index/poi/hssf/record/RecordInputStream;->readByte()B

    move-result v3

    .line 362
    .local v3, "compressFlag":B
    if-nez v3, :cond_a

    const/4 v5, 0x1

    .line 323
    :goto_5
    goto/16 :goto_0

    .line 362
    :cond_a
    const/4 v5, 0x0

    goto :goto_5
.end method


# virtual methods
.method public available()I
    .locals 1

    .prologue
    .line 127
    invoke-virtual {p0}, Lorg/apache/index/poi/hssf/record/RecordInputStream;->remaining()I

    move-result v0

    return v0
.end method

.method public getNextSid()I
    .locals 1

    .prologue
    .line 437
    iget v0, p0, Lorg/apache/index/poi/hssf/record/RecordInputStream;->_nextSid:I

    return v0
.end method

.method public getSid()S
    .locals 1

    .prologue
    .line 140
    iget v0, p0, Lorg/apache/index/poi/hssf/record/RecordInputStream;->_currentSid:I

    int-to-short v0, v0

    return v0
.end method

.method public hasNextRecord()Z
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/index/poi/hssf/record/RecordInputStream$LeftoverDataException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    const/4 v3, -0x1

    .line 150
    iget v1, p0, Lorg/apache/index/poi/hssf/record/RecordInputStream;->_currentDataLength:I

    if-eq v1, v3, :cond_1

    iget v1, p0, Lorg/apache/index/poi/hssf/record/RecordInputStream;->_currentDataLength:I

    iget v2, p0, Lorg/apache/index/poi/hssf/record/RecordInputStream;->_currentDataOffset:I

    if-eq v1, v2, :cond_1

    .line 158
    :cond_0
    :goto_0
    return v0

    .line 155
    :cond_1
    iget v1, p0, Lorg/apache/index/poi/hssf/record/RecordInputStream;->_currentDataLength:I

    if-eq v1, v3, :cond_2

    .line 156
    invoke-direct {p0}, Lorg/apache/index/poi/hssf/record/RecordInputStream;->readNextSid()I

    move-result v1

    iput v1, p0, Lorg/apache/index/poi/hssf/record/RecordInputStream;->_nextSid:I

    .line 158
    :cond_2
    iget v1, p0, Lorg/apache/index/poi/hssf/record/RecordInputStream;->_nextSid:I

    if-eq v1, v3, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public nextRecord()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/index/poi/hssf/record/RecordFormatException;
        }
    .end annotation

    .prologue
    const/4 v1, -0x1

    .line 187
    iget v0, p0, Lorg/apache/index/poi/hssf/record/RecordInputStream;->_nextSid:I

    if-ne v0, v1, :cond_0

    .line 188
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "EOF - next record not available"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 190
    :cond_0
    iget v0, p0, Lorg/apache/index/poi/hssf/record/RecordInputStream;->_currentDataLength:I

    if-eq v0, v1, :cond_1

    .line 191
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Cannot call nextRecord() without checking hasNextRecord() first"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 193
    :cond_1
    iget v0, p0, Lorg/apache/index/poi/hssf/record/RecordInputStream;->_nextSid:I

    iput v0, p0, Lorg/apache/index/poi/hssf/record/RecordInputStream;->_currentSid:I

    .line 194
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/index/poi/hssf/record/RecordInputStream;->_currentDataOffset:I

    .line 195
    iget-object v0, p0, Lorg/apache/index/poi/hssf/record/RecordInputStream;->_bhi:Lorg/apache/index/poi/hssf/record/BiffHeaderInput;

    invoke-interface {v0}, Lorg/apache/index/poi/hssf/record/BiffHeaderInput;->readDataSize()I

    move-result v0

    iput v0, p0, Lorg/apache/index/poi/hssf/record/RecordInputStream;->_currentDataLength:I

    .line 196
    iget v0, p0, Lorg/apache/index/poi/hssf/record/RecordInputStream;->_currentDataLength:I

    const/16 v1, 0x2020

    if-le v0, v1, :cond_2

    .line 197
    new-instance v0, Lorg/apache/index/poi/hssf/record/RecordFormatException;

    const-string/jumbo v1, "The content of an excel record cannot exceed 8224 bytes"

    invoke-direct {v0, v1}, Lorg/apache/index/poi/hssf/record/RecordFormatException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 200
    :cond_2
    return-void
.end method

.method public read([BII)I
    .locals 2
    .param p1, "b"    # [B
    .param p2, "off"    # I
    .param p3, "len"    # I

    .prologue
    .line 131
    invoke-virtual {p0}, Lorg/apache/index/poi/hssf/record/RecordInputStream;->remaining()I

    move-result v1

    invoke-static {p3, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 132
    .local v0, "limit":I
    if-nez v0, :cond_0

    .line 133
    const/4 v0, 0x0

    .line 136
    .end local v0    # "limit":I
    :goto_0
    return v0

    .line 135
    .restart local v0    # "limit":I
    :cond_0
    invoke-virtual {p0, p1, p2, v0}, Lorg/apache/index/poi/hssf/record/RecordInputStream;->readFully([BII)V

    goto :goto_0
.end method

.method public readAllContinuedRemainder()[B
    .locals 4

    .prologue
    .line 389
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    const/16 v2, 0x4040

    invoke-direct {v1, v2}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    .line 392
    .local v1, "out":Ljava/io/ByteArrayOutputStream;
    :goto_0
    invoke-virtual {p0}, Lorg/apache/index/poi/hssf/record/RecordInputStream;->readRemainder()[B

    move-result-object v0

    .line 393
    .local v0, "b":[B
    const/4 v2, 0x0

    array-length v3, v0

    invoke-virtual {v1, v0, v2, v3}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    .line 394
    invoke-direct {p0}, Lorg/apache/index/poi/hssf/record/RecordInputStream;->isContinueNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 399
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v2

    return-object v2

    .line 397
    :cond_0
    invoke-virtual {p0}, Lorg/apache/index/poi/hssf/record/RecordInputStream;->nextRecord()V

    goto :goto_0
.end method

.method public readByte()B
    .locals 1

    .prologue
    .line 221
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lorg/apache/index/poi/hssf/record/RecordInputStream;->checkRecordPosition(I)V

    .line 222
    iget v0, p0, Lorg/apache/index/poi/hssf/record/RecordInputStream;->_currentDataOffset:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/index/poi/hssf/record/RecordInputStream;->_currentDataOffset:I

    .line 223
    iget-object v0, p0, Lorg/apache/index/poi/hssf/record/RecordInputStream;->_dataInput:Lorg/apache/index/poi/util/LittleEndianInput;

    invoke-interface {v0}, Lorg/apache/index/poi/util/LittleEndianInput;->readByte()B

    move-result v0

    return v0
.end method

.method public readCompressedUnicode(I)Ljava/lang/String;
    .locals 1
    .param p1, "requestedLength"    # I

    .prologue
    .line 312
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lorg/apache/index/poi/hssf/record/RecordInputStream;->readStringCommon(IZ)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public readDouble()D
    .locals 4

    .prologue
    .line 270
    invoke-virtual {p0}, Lorg/apache/index/poi/hssf/record/RecordInputStream;->readLong()J

    move-result-wide v2

    .line 271
    .local v2, "valueLongBits":J
    invoke-static {v2, v3}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v0

    .line 278
    .local v0, "result":D
    return-wide v0
.end method

.method public readFully([B)V
    .locals 2
    .param p1, "buf"    # [B

    .prologue
    .line 281
    const/4 v0, 0x0

    array-length v1, p1

    invoke-virtual {p0, p1, v0, v1}, Lorg/apache/index/poi/hssf/record/RecordInputStream;->readFully([BII)V

    .line 282
    return-void
.end method

.method public readFully([BII)V
    .locals 1
    .param p1, "buf"    # [B
    .param p2, "off"    # I
    .param p3, "len"    # I

    .prologue
    .line 285
    invoke-direct {p0, p3}, Lorg/apache/index/poi/hssf/record/RecordInputStream;->checkRecordPosition(I)V

    .line 286
    iget-object v0, p0, Lorg/apache/index/poi/hssf/record/RecordInputStream;->_dataInput:Lorg/apache/index/poi/util/LittleEndianInput;

    invoke-interface {v0, p1, p2, p3}, Lorg/apache/index/poi/util/LittleEndianInput;->readFully([BII)V

    .line 287
    iget v0, p0, Lorg/apache/index/poi/hssf/record/RecordInputStream;->_currentDataOffset:I

    add-int/2addr v0, p3

    iput v0, p0, Lorg/apache/index/poi/hssf/record/RecordInputStream;->_currentDataOffset:I

    .line 288
    return-void
.end method

.method public readInt()I
    .locals 1

    .prologue
    .line 239
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lorg/apache/index/poi/hssf/record/RecordInputStream;->checkRecordPosition(I)V

    .line 240
    iget v0, p0, Lorg/apache/index/poi/hssf/record/RecordInputStream;->_currentDataOffset:I

    add-int/lit8 v0, v0, 0x4

    iput v0, p0, Lorg/apache/index/poi/hssf/record/RecordInputStream;->_currentDataOffset:I

    .line 241
    iget-object v0, p0, Lorg/apache/index/poi/hssf/record/RecordInputStream;->_dataInput:Lorg/apache/index/poi/util/LittleEndianInput;

    invoke-interface {v0}, Lorg/apache/index/poi/util/LittleEndianInput;->readInt()I

    move-result v0

    return v0
.end method

.method public readLong()J
    .locals 2

    .prologue
    .line 248
    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lorg/apache/index/poi/hssf/record/RecordInputStream;->checkRecordPosition(I)V

    .line 249
    iget v0, p0, Lorg/apache/index/poi/hssf/record/RecordInputStream;->_currentDataOffset:I

    add-int/lit8 v0, v0, 0x8

    iput v0, p0, Lorg/apache/index/poi/hssf/record/RecordInputStream;->_currentDataOffset:I

    .line 250
    iget-object v0, p0, Lorg/apache/index/poi/hssf/record/RecordInputStream;->_dataInput:Lorg/apache/index/poi/util/LittleEndianInput;

    invoke-interface {v0}, Lorg/apache/index/poi/util/LittleEndianInput;->readLong()J

    move-result-wide v0

    return-wide v0
.end method

.method public readRemainder()[B
    .locals 2

    .prologue
    .line 371
    invoke-virtual {p0}, Lorg/apache/index/poi/hssf/record/RecordInputStream;->remaining()I

    move-result v1

    .line 372
    .local v1, "size":I
    if-nez v1, :cond_0

    .line 373
    sget-object v0, Lorg/apache/index/poi/hssf/record/RecordInputStream;->EMPTY_BYTE_ARRAY:[B

    .line 377
    :goto_0
    return-object v0

    .line 375
    :cond_0
    new-array v0, v1, [B

    .line 376
    .local v0, "result":[B
    invoke-virtual {p0, v0}, Lorg/apache/index/poi/hssf/record/RecordInputStream;->readFully([B)V

    goto :goto_0
.end method

.method public readShort()S
    .locals 1

    .prologue
    .line 230
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lorg/apache/index/poi/hssf/record/RecordInputStream;->checkRecordPosition(I)V

    .line 231
    iget v0, p0, Lorg/apache/index/poi/hssf/record/RecordInputStream;->_currentDataOffset:I

    add-int/lit8 v0, v0, 0x2

    iput v0, p0, Lorg/apache/index/poi/hssf/record/RecordInputStream;->_currentDataOffset:I

    .line 232
    iget-object v0, p0, Lorg/apache/index/poi/hssf/record/RecordInputStream;->_dataInput:Lorg/apache/index/poi/util/LittleEndianInput;

    invoke-interface {v0}, Lorg/apache/index/poi/util/LittleEndianInput;->readShort()S

    move-result v0

    return v0
.end method

.method public readString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 291
    invoke-virtual {p0}, Lorg/apache/index/poi/hssf/record/RecordInputStream;->readUShort()I

    move-result v1

    .line 292
    .local v1, "requestedLength":I
    invoke-virtual {p0}, Lorg/apache/index/poi/hssf/record/RecordInputStream;->readByte()B

    move-result v0

    .line 293
    .local v0, "compressFlag":B
    if-nez v0, :cond_0

    const/4 v2, 0x1

    :goto_0
    invoke-direct {p0, v1, v2}, Lorg/apache/index/poi/hssf/record/RecordInputStream;->readStringCommon(IZ)Ljava/lang/String;

    move-result-object v2

    return-object v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public readUByte()I
    .locals 1

    .prologue
    .line 257
    invoke-virtual {p0}, Lorg/apache/index/poi/hssf/record/RecordInputStream;->readByte()B

    move-result v0

    and-int/lit16 v0, v0, 0xff

    return v0
.end method

.method public readUShort()I
    .locals 1

    .prologue
    .line 264
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lorg/apache/index/poi/hssf/record/RecordInputStream;->checkRecordPosition(I)V

    .line 265
    iget v0, p0, Lorg/apache/index/poi/hssf/record/RecordInputStream;->_currentDataOffset:I

    add-int/lit8 v0, v0, 0x2

    iput v0, p0, Lorg/apache/index/poi/hssf/record/RecordInputStream;->_currentDataOffset:I

    .line 266
    iget-object v0, p0, Lorg/apache/index/poi/hssf/record/RecordInputStream;->_dataInput:Lorg/apache/index/poi/util/LittleEndianInput;

    invoke-interface {v0}, Lorg/apache/index/poi/util/LittleEndianInput;->readUShort()I

    move-result v0

    return v0
.end method

.method public readUnicodeLEString(I)Ljava/lang/String;
    .locals 1
    .param p1, "requestedLength"    # I

    .prologue
    .line 308
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lorg/apache/index/poi/hssf/record/RecordInputStream;->readStringCommon(IZ)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public remaining()I
    .locals 2

    .prologue
    .line 407
    iget v0, p0, Lorg/apache/index/poi/hssf/record/RecordInputStream;->_currentDataLength:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 409
    const/4 v0, 0x0

    .line 411
    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lorg/apache/index/poi/hssf/record/RecordInputStream;->_currentDataLength:I

    iget v1, p0, Lorg/apache/index/poi/hssf/record/RecordInputStream;->_currentDataOffset:I

    sub-int/2addr v0, v1

    goto :goto_0
.end method
