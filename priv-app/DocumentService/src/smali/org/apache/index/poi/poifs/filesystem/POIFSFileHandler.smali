.class public Lorg/apache/index/poi/poifs/filesystem/POIFSFileHandler;
.super Ljava/lang/Object;
.source "POIFSFileHandler.java"


# static fields
.field private static final _logger:Lorg/apache/index/poi/util/POILogger;


# instance fields
.field private poiFile_Currentindex:I

.field private poiFile_Stream:Ljava/io/FileInputStream;

.field private poiFile__File:Ljava/io/File;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 45
    const-class v0, Lorg/apache/index/poi/poifs/filesystem/POIFSFileHandler;

    invoke-static {v0}, Lorg/apache/index/poi/util/POILogFactory;->getLogger(Ljava/lang/Class;)Lorg/apache/index/poi/util/POILogger;

    move-result-object v0

    .line 44
    sput-object v0, Lorg/apache/index/poi/poifs/filesystem/POIFSFileHandler;->_logger:Lorg/apache/index/poi/util/POILogger;

    .line 45
    return-void
.end method

.method public constructor <init>(Ljava/io/File;)V
    .locals 2
    .param p1, "inFile"    # Ljava/io/File;

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    iput-object v1, p0, Lorg/apache/index/poi/poifs/filesystem/POIFSFileHandler;->poiFile_Stream:Ljava/io/FileInputStream;

    .line 49
    iput v0, p0, Lorg/apache/index/poi/poifs/filesystem/POIFSFileHandler;->poiFile_Currentindex:I

    .line 50
    iput-object v1, p0, Lorg/apache/index/poi/poifs/filesystem/POIFSFileHandler;->poiFile__File:Ljava/io/File;

    .line 57
    iput-object p1, p0, Lorg/apache/index/poi/poifs/filesystem/POIFSFileHandler;->poiFile__File:Ljava/io/File;

    .line 58
    iput v0, p0, Lorg/apache/index/poi/poifs/filesystem/POIFSFileHandler;->poiFile_Currentindex:I

    .line 59
    return-void
.end method


# virtual methods
.method public close()V
    .locals 4

    .prologue
    .line 121
    :try_start_0
    iget-object v1, p0, Lorg/apache/index/poi/poifs/filesystem/POIFSFileHandler;->poiFile_Stream:Ljava/io/FileInputStream;

    if-eqz v1, :cond_0

    .line 123
    iget-object v1, p0, Lorg/apache/index/poi/poifs/filesystem/POIFSFileHandler;->poiFile_Stream:Ljava/io/FileInputStream;

    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V

    .line 124
    const/4 v1, 0x0

    iput-object v1, p0, Lorg/apache/index/poi/poifs/filesystem/POIFSFileHandler;->poiFile_Stream:Ljava/io/FileInputStream;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 131
    :cond_0
    :goto_0
    return-void

    .line 127
    :catch_0
    move-exception v0

    .line 129
    .local v0, "e":Ljava/io/IOException;
    const-string/jumbo v1, "POIFSFileHandler"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "close IOException :"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public getBlockData([BII)I
    .locals 6
    .param p1, "data"    # [B
    .param p2, "startIndex"    # I
    .param p3, "blockLength"    # I

    .prologue
    .line 64
    const/4 v2, 0x0

    .line 67
    .local v2, "retVale":I
    :try_start_0
    iget-object v3, p0, Lorg/apache/index/poi/poifs/filesystem/POIFSFileHandler;->poiFile_Stream:Ljava/io/FileInputStream;

    if-nez v3, :cond_0

    .line 69
    new-instance v3, Ljava/io/FileInputStream;

    iget-object v4, p0, Lorg/apache/index/poi/poifs/filesystem/POIFSFileHandler;->poiFile__File:Ljava/io/File;

    invoke-direct {v3, v4}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    iput-object v3, p0, Lorg/apache/index/poi/poifs/filesystem/POIFSFileHandler;->poiFile_Stream:Ljava/io/FileInputStream;

    .line 70
    const/4 v3, 0x0

    iput v3, p0, Lorg/apache/index/poi/poifs/filesystem/POIFSFileHandler;->poiFile_Currentindex:I

    .line 74
    :cond_0
    iget v3, p0, Lorg/apache/index/poi/poifs/filesystem/POIFSFileHandler;->poiFile_Currentindex:I

    if-le v3, p2, :cond_2

    .line 77
    const/4 v3, 0x0

    iput v3, p0, Lorg/apache/index/poi/poifs/filesystem/POIFSFileHandler;->poiFile_Currentindex:I

    .line 79
    iget-object v3, p0, Lorg/apache/index/poi/poifs/filesystem/POIFSFileHandler;->poiFile_Stream:Ljava/io/FileInputStream;

    if-eqz v3, :cond_1

    .line 81
    iget-object v3, p0, Lorg/apache/index/poi/poifs/filesystem/POIFSFileHandler;->poiFile_Stream:Ljava/io/FileInputStream;

    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V

    .line 82
    const/4 v3, 0x0

    iput-object v3, p0, Lorg/apache/index/poi/poifs/filesystem/POIFSFileHandler;->poiFile_Stream:Ljava/io/FileInputStream;

    .line 85
    :cond_1
    new-instance v3, Ljava/io/FileInputStream;

    iget-object v4, p0, Lorg/apache/index/poi/poifs/filesystem/POIFSFileHandler;->poiFile__File:Ljava/io/File;

    invoke-direct {v3, v4}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    iput-object v3, p0, Lorg/apache/index/poi/poifs/filesystem/POIFSFileHandler;->poiFile_Stream:Ljava/io/FileInputStream;

    .line 88
    iget-object v3, p0, Lorg/apache/index/poi/poifs/filesystem/POIFSFileHandler;->poiFile_Stream:Ljava/io/FileInputStream;

    int-to-long v4, p2

    invoke-virtual {v3, v4, v5}, Ljava/io/FileInputStream;->skip(J)J

    .line 90
    iget-object v3, p0, Lorg/apache/index/poi/poifs/filesystem/POIFSFileHandler;->poiFile_Stream:Ljava/io/FileInputStream;

    const/4 v4, 0x0

    invoke-virtual {v3, p1, v4, p3}, Ljava/io/FileInputStream;->read([BII)I

    move-result v2

    .line 92
    add-int v3, p2, v2

    iput v3, p0, Lorg/apache/index/poi/poifs/filesystem/POIFSFileHandler;->poiFile_Currentindex:I

    .line 113
    :goto_0
    return v2

    .line 97
    :cond_2
    iget v3, p0, Lorg/apache/index/poi/poifs/filesystem/POIFSFileHandler;->poiFile_Currentindex:I

    sub-int v1, p2, v3

    .line 99
    .local v1, "indextoSkip":I
    iget-object v3, p0, Lorg/apache/index/poi/poifs/filesystem/POIFSFileHandler;->poiFile_Stream:Ljava/io/FileInputStream;

    int-to-long v4, v1

    invoke-virtual {v3, v4, v5}, Ljava/io/FileInputStream;->skip(J)J

    .line 101
    iget-object v3, p0, Lorg/apache/index/poi/poifs/filesystem/POIFSFileHandler;->poiFile_Stream:Ljava/io/FileInputStream;

    const/4 v4, 0x0

    invoke-virtual {v3, p1, v4, p3}, Ljava/io/FileInputStream;->read([BII)I

    move-result v2

    .line 103
    add-int v3, p2, v2

    iput v3, p0, Lorg/apache/index/poi/poifs/filesystem/POIFSFileHandler;->poiFile_Currentindex:I
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 106
    .end local v1    # "indextoSkip":I
    :catch_0
    move-exception v0

    .line 108
    .local v0, "e":Ljava/io/FileNotFoundException;
    new-instance v3, Ljava/lang/RuntimeException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "POIFSFileHandler FileNotFoundException :"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 109
    .end local v0    # "e":Ljava/io/FileNotFoundException;
    :catch_1
    move-exception v0

    .line 110
    .local v0, "e":Ljava/io/IOException;
    const-string/jumbo v3, "POIFSFileHandler"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "IOException :"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
