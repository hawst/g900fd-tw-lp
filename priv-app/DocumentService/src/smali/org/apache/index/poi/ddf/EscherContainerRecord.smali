.class public final Lorg/apache/index/poi/ddf/EscherContainerRecord;
.super Lorg/apache/index/poi/ddf/EscherRecord;
.source "EscherContainerRecord.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/index/poi/ddf/EscherContainerRecord$ReadOnlyIterator;
    }
.end annotation


# static fields
.field public static final BSTORE_CONTAINER:S = -0xfffs

.field public static final DGG_CONTAINER:S = -0x1000s

.field public static final DG_CONTAINER:S = -0xffes

.field public static final SOLVER_CONTAINER:S = -0xffbs

.field public static final SPGR_CONTAINER:S = -0xffds

.field public static final SP_CONTAINER:S = -0xffcs


# instance fields
.field private final _childRecords:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/apache/index/poi/ddf/EscherRecord;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0}, Lorg/apache/index/poi/ddf/EscherRecord;-><init>()V

    .line 45
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/apache/index/poi/ddf/EscherContainerRecord;->_childRecords:Ljava/util/List;

    .line 37
    return-void
.end method


# virtual methods
.method public addChildBefore(Lorg/apache/index/poi/ddf/EscherRecord;I)V
    .locals 4
    .param p1, "record"    # Lorg/apache/index/poi/ddf/EscherRecord;
    .param p2, "insertBeforeRecordId"    # I

    .prologue
    .line 216
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v3, p0, Lorg/apache/index/poi/ddf/EscherContainerRecord;->_childRecords:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-lt v0, v3, :cond_0

    .line 223
    return-void

    .line 217
    :cond_0
    iget-object v3, p0, Lorg/apache/index/poi/ddf/EscherContainerRecord;->_childRecords:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/index/poi/ddf/EscherRecord;

    .line 218
    .local v2, "rec":Lorg/apache/index/poi/ddf/EscherRecord;
    invoke-virtual {v2}, Lorg/apache/index/poi/ddf/EscherRecord;->getRecordId()S

    move-result v3

    if-ne v3, p2, :cond_1

    .line 219
    iget-object v3, p0, Lorg/apache/index/poi/ddf/EscherContainerRecord;->_childRecords:Ljava/util/List;

    add-int/lit8 v1, v0, 0x1

    .end local v0    # "i":I
    .local v1, "i":I
    invoke-interface {v3, v0, p1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    move v0, v1

    .line 216
    .end local v1    # "i":I
    .restart local v0    # "i":I
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public addChildRecord(Lorg/apache/index/poi/ddf/EscherRecord;)V
    .locals 1
    .param p1, "record"    # Lorg/apache/index/poi/ddf/EscherRecord;

    .prologue
    .line 212
    iget-object v0, p0, Lorg/apache/index/poi/ddf/EscherContainerRecord;->_childRecords:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 213
    return-void
.end method

.method public display(Ljava/io/PrintWriter;I)V
    .locals 3
    .param p1, "w"    # Ljava/io/PrintWriter;
    .param p2, "indent"    # I

    .prologue
    .line 203
    invoke-super {p0, p1, p2}, Lorg/apache/index/poi/ddf/EscherRecord;->display(Ljava/io/PrintWriter;I)V

    .line 204
    iget-object v2, p0, Lorg/apache/index/poi/ddf/EscherContainerRecord;->_childRecords:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/index/poi/ddf/EscherRecord;>;"
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 209
    return-void

    .line 206
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/index/poi/ddf/EscherRecord;

    .line 207
    .local v0, "escherRecord":Lorg/apache/index/poi/ddf/EscherRecord;
    add-int/lit8 v2, p2, 0x1

    invoke-virtual {v0, p1, v2}, Lorg/apache/index/poi/ddf/EscherRecord;->display(Ljava/io/PrintWriter;I)V

    goto :goto_0
.end method

.method public fillFields([BILorg/apache/index/poi/ddf/EscherRecordFactory;)I
    .locals 8
    .param p1, "data"    # [B
    .param p2, "pOffset"    # I
    .param p3, "recordFactory"    # Lorg/apache/index/poi/ddf/EscherRecordFactory;

    .prologue
    .line 48
    invoke-virtual {p0, p1, p2}, Lorg/apache/index/poi/ddf/EscherContainerRecord;->readHeader([BI)I

    move-result v0

    .line 49
    .local v0, "bytesRemaining":I
    const/16 v1, 0x8

    .line 50
    .local v1, "bytesWritten":I
    add-int/lit8 v4, p2, 0x8

    .line 51
    .local v4, "offset":I
    :cond_0
    :goto_0
    if-lez v0, :cond_1

    array-length v5, p1

    if-lt v4, v5, :cond_2

    .line 62
    :cond_1
    return v1

    .line 52
    :cond_2
    invoke-interface {p3, p1, v4}, Lorg/apache/index/poi/ddf/EscherRecordFactory;->createRecord([BI)Lorg/apache/index/poi/ddf/EscherRecord;

    move-result-object v2

    .line 53
    .local v2, "child":Lorg/apache/index/poi/ddf/EscherRecord;
    invoke-virtual {v2, p1, v4, p3}, Lorg/apache/index/poi/ddf/EscherRecord;->fillFields([BILorg/apache/index/poi/ddf/EscherRecordFactory;)I

    move-result v3

    .line 54
    .local v3, "childBytesWritten":I
    add-int/2addr v1, v3

    .line 55
    add-int/2addr v4, v3

    .line 56
    sub-int/2addr v0, v3

    .line 57
    invoke-virtual {p0, v2}, Lorg/apache/index/poi/ddf/EscherContainerRecord;->addChildRecord(Lorg/apache/index/poi/ddf/EscherRecord;)V

    .line 58
    array-length v5, p1

    if-lt v4, v5, :cond_0

    if-lez v0, :cond_0

    .line 59
    sget-object v5, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string/jumbo v7, "WARNING: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, " bytes remaining but no space left"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public getChild(I)Lorg/apache/index/poi/ddf/EscherRecord;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 114
    iget-object v0, p0, Lorg/apache/index/poi/ddf/EscherContainerRecord;->_childRecords:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/index/poi/ddf/EscherRecord;

    return-object v0
.end method

.method public getChildById(S)Lorg/apache/index/poi/ddf/EscherSpRecord;
    .locals 3
    .param p1, "recordId"    # S

    .prologue
    .line 266
    iget-object v2, p0, Lorg/apache/index/poi/ddf/EscherContainerRecord;->_childRecords:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 267
    .local v0, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/index/poi/ddf/EscherRecord;>;"
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_1

    .line 272
    const/4 v1, 0x0

    :goto_0
    return-object v1

    .line 268
    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/index/poi/ddf/EscherRecord;

    .line 269
    .local v1, "r":Lorg/apache/index/poi/ddf/EscherRecord;
    invoke-virtual {v1}, Lorg/apache/index/poi/ddf/EscherRecord;->getRecordId()S

    move-result v2

    if-ne v2, p1, :cond_0

    .line 270
    check-cast v1, Lorg/apache/index/poi/ddf/EscherSpRecord;

    goto :goto_0
.end method

.method public getChildContainers()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lorg/apache/index/poi/ddf/EscherContainerRecord;",
            ">;"
        }
    .end annotation

    .prologue
    .line 172
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 173
    .local v0, "containers":Ljava/util/List;, "Ljava/util/List<Lorg/apache/index/poi/ddf/EscherContainerRecord;>;"
    iget-object v3, p0, Lorg/apache/index/poi/ddf/EscherContainerRecord;->_childRecords:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 174
    .local v1, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/index/poi/ddf/EscherRecord;>;"
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_1

    .line 180
    return-object v0

    .line 175
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/index/poi/ddf/EscherRecord;

    .line 176
    .local v2, "r":Lorg/apache/index/poi/ddf/EscherRecord;
    instance-of v3, v2, Lorg/apache/index/poi/ddf/EscherContainerRecord;

    if-eqz v3, :cond_0

    .line 177
    check-cast v2, Lorg/apache/index/poi/ddf/EscherContainerRecord;

    .end local v2    # "r":Lorg/apache/index/poi/ddf/EscherRecord;
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public getChildIterator()Ljava/util/Iterator;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Lorg/apache/index/poi/ddf/EscherRecord;",
            ">;"
        }
    .end annotation

    .prologue
    .line 125
    new-instance v0, Lorg/apache/index/poi/ddf/EscherContainerRecord$ReadOnlyIterator;

    iget-object v1, p0, Lorg/apache/index/poi/ddf/EscherContainerRecord;->_childRecords:Ljava/util/List;

    invoke-direct {v0, v1}, Lorg/apache/index/poi/ddf/EscherContainerRecord$ReadOnlyIterator;-><init>(Ljava/util/List;)V

    return-object v0
.end method

.method public getChildRecords()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lorg/apache/index/poi/ddf/EscherRecord;",
            ">;"
        }
    .end annotation

    .prologue
    .line 121
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lorg/apache/index/poi/ddf/EscherContainerRecord;->_childRecords:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method public getRecordName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 184
    invoke-virtual {p0}, Lorg/apache/index/poi/ddf/EscherContainerRecord;->getRecordId()S

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 198
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "Container 0x"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lorg/apache/index/poi/ddf/EscherContainerRecord;->getRecordId()S

    move-result v1

    invoke-static {v1}, Lorg/apache/index/poi/util/HexDump;->toHex(S)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 186
    :pswitch_0
    const-string/jumbo v0, "DggContainer"

    goto :goto_0

    .line 188
    :pswitch_1
    const-string/jumbo v0, "BStoreContainer"

    goto :goto_0

    .line 190
    :pswitch_2
    const-string/jumbo v0, "DgContainer"

    goto :goto_0

    .line 192
    :pswitch_3
    const-string/jumbo v0, "SpgrContainer"

    goto :goto_0

    .line 194
    :pswitch_4
    const-string/jumbo v0, "SpContainer"

    goto :goto_0

    .line 196
    :pswitch_5
    const-string/jumbo v0, "SolverContainer"

    goto :goto_0

    .line 184
    :pswitch_data_0
    .packed-switch -0x1000
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public getRecordSize()I
    .locals 4

    .prologue
    .line 90
    const/4 v0, 0x0

    .line 91
    .local v0, "childRecordsSize":I
    iget-object v3, p0, Lorg/apache/index/poi/ddf/EscherContainerRecord;->_childRecords:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 92
    .local v1, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/index/poi/ddf/EscherRecord;>;"
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_0

    .line 96
    add-int/lit8 v3, v0, 0x8

    return v3

    .line 93
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/index/poi/ddf/EscherRecord;

    .line 94
    .local v2, "r":Lorg/apache/index/poi/ddf/EscherRecord;
    invoke-virtual {v2}, Lorg/apache/index/poi/ddf/EscherRecord;->getRecordSize()I

    move-result v3

    add-int/2addr v0, v3

    goto :goto_0
.end method

.method public getRecordsById(SLjava/util/List;)V
    .locals 4
    .param p1, "recordId"    # S
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(S",
            "Ljava/util/List",
            "<",
            "Lorg/apache/index/poi/ddf/EscherRecord;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 281
    .local p2, "out":Ljava/util/List;, "Ljava/util/List<Lorg/apache/index/poi/ddf/EscherRecord;>;"
    iget-object v3, p0, Lorg/apache/index/poi/ddf/EscherContainerRecord;->_childRecords:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 282
    .local v1, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/index/poi/ddf/EscherRecord;>;"
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_1

    .line 291
    return-void

    .line 283
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/index/poi/ddf/EscherRecord;

    .line 284
    .local v2, "r":Lorg/apache/index/poi/ddf/EscherRecord;
    instance-of v3, v2, Lorg/apache/index/poi/ddf/EscherContainerRecord;

    if-eqz v3, :cond_2

    move-object v0, v2

    .line 285
    check-cast v0, Lorg/apache/index/poi/ddf/EscherContainerRecord;

    .line 286
    .local v0, "c":Lorg/apache/index/poi/ddf/EscherContainerRecord;
    invoke-virtual {v0, p1, p2}, Lorg/apache/index/poi/ddf/EscherContainerRecord;->getRecordsById(SLjava/util/List;)V

    goto :goto_0

    .line 287
    .end local v0    # "c":Lorg/apache/index/poi/ddf/EscherContainerRecord;
    :cond_2
    invoke-virtual {v2}, Lorg/apache/index/poi/ddf/EscherRecord;->getRecordId()S

    move-result v3

    if-ne v3, p1, :cond_0

    .line 288
    invoke-interface {p2, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public hasChildOfType(S)Z
    .locals 3
    .param p1, "recordId"    # S

    .prologue
    .line 104
    iget-object v2, p0, Lorg/apache/index/poi/ddf/EscherContainerRecord;->_childRecords:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 105
    .local v0, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/index/poi/ddf/EscherRecord;>;"
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_1

    .line 111
    const/4 v2, 0x0

    :goto_0
    return v2

    .line 106
    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/index/poi/ddf/EscherRecord;

    .line 107
    .local v1, "r":Lorg/apache/index/poi/ddf/EscherRecord;
    invoke-virtual {v1}, Lorg/apache/index/poi/ddf/EscherRecord;->getRecordId()S

    move-result v2

    if-ne v2, p1, :cond_0

    .line 108
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public removeChildRecord(Lorg/apache/index/poi/ddf/EscherRecord;)Z
    .locals 1
    .param p1, "toBeRemoved"    # Lorg/apache/index/poi/ddf/EscherRecord;

    .prologue
    .line 161
    iget-object v0, p0, Lorg/apache/index/poi/ddf/EscherContainerRecord;->_childRecords:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public serialize(I[BLorg/apache/index/poi/ddf/EscherSerializationListener;)I
    .locals 6
    .param p1, "offset"    # I
    .param p2, "data"    # [B
    .param p3, "listener"    # Lorg/apache/index/poi/ddf/EscherSerializationListener;

    .prologue
    .line 67
    invoke-virtual {p0}, Lorg/apache/index/poi/ddf/EscherContainerRecord;->getRecordId()S

    move-result v4

    invoke-interface {p3, p1, v4, p0}, Lorg/apache/index/poi/ddf/EscherSerializationListener;->beforeRecordSerialize(ISLorg/apache/index/poi/ddf/EscherRecord;)V

    .line 69
    invoke-virtual {p0}, Lorg/apache/index/poi/ddf/EscherContainerRecord;->getOptions()S

    move-result v4

    invoke-static {p2, p1, v4}, Lorg/apache/index/poi/util/LittleEndian;->putShort([BIS)V

    .line 70
    add-int/lit8 v4, p1, 0x2

    invoke-virtual {p0}, Lorg/apache/index/poi/ddf/EscherContainerRecord;->getRecordId()S

    move-result v5

    invoke-static {p2, v4, v5}, Lorg/apache/index/poi/util/LittleEndian;->putShort([BIS)V

    .line 71
    const/4 v3, 0x0

    .line 72
    .local v3, "remainingBytes":I
    iget-object v4, p0, Lorg/apache/index/poi/ddf/EscherContainerRecord;->_childRecords:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 73
    .local v0, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/index/poi/ddf/EscherRecord;>;"
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_0

    .line 77
    add-int/lit8 v4, p1, 0x4

    invoke-static {p2, v4, v3}, Lorg/apache/index/poi/util/LittleEndian;->putInt([BII)V

    .line 78
    add-int/lit8 v1, p1, 0x8

    .line 79
    .local v1, "pos":I
    iget-object v4, p0, Lorg/apache/index/poi/ddf/EscherContainerRecord;->_childRecords:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 80
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_1

    .line 85
    invoke-virtual {p0}, Lorg/apache/index/poi/ddf/EscherContainerRecord;->getRecordId()S

    move-result v4

    sub-int v5, v1, p1

    invoke-interface {p3, v1, v4, v5, p0}, Lorg/apache/index/poi/ddf/EscherSerializationListener;->afterRecordSerialize(ISILorg/apache/index/poi/ddf/EscherRecord;)V

    .line 86
    sub-int v4, v1, p1

    return v4

    .line 74
    .end local v1    # "pos":I
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/index/poi/ddf/EscherRecord;

    .line 75
    .local v2, "r":Lorg/apache/index/poi/ddf/EscherRecord;
    invoke-virtual {v2}, Lorg/apache/index/poi/ddf/EscherRecord;->getRecordSize()I

    move-result v4

    add-int/2addr v3, v4

    goto :goto_0

    .line 81
    .end local v2    # "r":Lorg/apache/index/poi/ddf/EscherRecord;
    .restart local v1    # "pos":I
    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/index/poi/ddf/EscherRecord;

    .line 82
    .restart local v2    # "r":Lorg/apache/index/poi/ddf/EscherRecord;
    invoke-virtual {v2, v1, p2, p3}, Lorg/apache/index/poi/ddf/EscherRecord;->serialize(I[BLorg/apache/index/poi/ddf/EscherSerializationListener;)I

    move-result v4

    add-int/2addr v1, v4

    goto :goto_1
.end method

.method public setChildRecords(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/apache/index/poi/ddf/EscherRecord;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 153
    .local p1, "childRecords":Ljava/util/List;, "Ljava/util/List<Lorg/apache/index/poi/ddf/EscherRecord;>;"
    iget-object v0, p0, Lorg/apache/index/poi/ddf/EscherContainerRecord;->_childRecords:Ljava/util/List;

    if-ne p1, v0, :cond_0

    .line 154
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Child records private data member has escaped"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 156
    :cond_0
    iget-object v0, p0, Lorg/apache/index/poi/ddf/EscherContainerRecord;->_childRecords:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 157
    iget-object v0, p0, Lorg/apache/index/poi/ddf/EscherContainerRecord;->_childRecords:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 158
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 227
    const-string/jumbo v0, ""

    invoke-virtual {p0, v0}, Lorg/apache/index/poi/ddf/EscherContainerRecord;->toString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public toString(Ljava/lang/String;)Ljava/lang/String;
    .locals 9
    .param p1, "indent"    # Ljava/lang/String;

    .prologue
    .line 231
    const-string/jumbo v7, "line.separator"

    invoke-static {v7}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 233
    .local v5, "nl":Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 234
    .local v0, "children":Ljava/lang/StringBuffer;
    iget-object v7, p0, Lorg/apache/index/poi/ddf/EscherContainerRecord;->_childRecords:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v7

    if-lez v7, :cond_0

    .line 235
    new-instance v7, Ljava/lang/StringBuilder;

    const-string/jumbo v8, "  children: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 237
    const/4 v1, 0x0

    .line 238
    .local v1, "count":I
    iget-object v7, p0, Lorg/apache/index/poi/ddf/EscherContainerRecord;->_childRecords:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/index/poi/ddf/EscherRecord;>;"
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-nez v7, :cond_1

    .line 256
    .end local v1    # "count":I
    .end local v3    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/index/poi/ddf/EscherRecord;>;"
    :cond_0
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, " ("

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p0}, Lorg/apache/index/poi/ddf/EscherContainerRecord;->getRecordName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, "):"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 257
    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, "  isContainer: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p0}, Lorg/apache/index/poi/ddf/EscherContainerRecord;->isContainerRecord()Z

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 258
    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, "  options: 0x"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p0}, Lorg/apache/index/poi/ddf/EscherContainerRecord;->getOptions()S

    move-result v8

    invoke-static {v8}, Lorg/apache/index/poi/util/HexDump;->toHex(S)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 259
    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, "  recordId: 0x"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p0}, Lorg/apache/index/poi/ddf/EscherContainerRecord;->getRecordId()S

    move-result v8

    invoke-static {v8}, Lorg/apache/index/poi/util/HexDump;->toHex(S)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 260
    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, "  numchildren: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lorg/apache/index/poi/ddf/EscherContainerRecord;->_childRecords:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 261
    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 256
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 255
    return-object v7

    .line 240
    .restart local v1    # "count":I
    .restart local v3    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/index/poi/ddf/EscherRecord;>;"
    :cond_1
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v8, "   "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 242
    .local v4, "newIndent":Ljava/lang/String;
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lorg/apache/index/poi/ddf/EscherRecord;

    .line 243
    .local v6, "record":Lorg/apache/index/poi/ddf/EscherRecord;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v8, "Child "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, ":"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 245
    instance-of v7, v6, Lorg/apache/index/poi/ddf/EscherContainerRecord;

    if-eqz v7, :cond_2

    move-object v2, v6

    .line 246
    check-cast v2, Lorg/apache/index/poi/ddf/EscherContainerRecord;

    .line 247
    .local v2, "ecr":Lorg/apache/index/poi/ddf/EscherContainerRecord;
    invoke-virtual {v2, v4}, Lorg/apache/index/poi/ddf/EscherContainerRecord;->toString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 251
    .end local v2    # "ecr":Lorg/apache/index/poi/ddf/EscherContainerRecord;
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_0

    .line 249
    :cond_2
    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_1
.end method
