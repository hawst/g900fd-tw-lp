.class public Lorg/apache/index/poi/poifs/filesystem/DocumentNode;
.super Lorg/apache/index/poi/poifs/filesystem/EntryNode;
.source "DocumentNode.java"

# interfaces
.implements Lorg/apache/index/poi/poifs/filesystem/DocumentEntry;


# instance fields
.field private _document:Lorg/apache/index/poi/poifs/filesystem/POIFSDocument;


# direct methods
.method constructor <init>(Lorg/apache/index/poi/poifs/property/DocumentProperty;Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;)V
    .locals 1
    .param p1, "property"    # Lorg/apache/index/poi/poifs/property/DocumentProperty;
    .param p2, "parent"    # Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;

    .prologue
    .line 51
    invoke-direct {p0, p1, p2}, Lorg/apache/index/poi/poifs/filesystem/EntryNode;-><init>(Lorg/apache/index/poi/poifs/property/Property;Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;)V

    .line 52
    invoke-virtual {p1}, Lorg/apache/index/poi/poifs/property/DocumentProperty;->getDocument()Lorg/apache/index/poi/poifs/filesystem/POIFSDocument;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/index/poi/poifs/filesystem/DocumentNode;->_document:Lorg/apache/index/poi/poifs/filesystem/POIFSDocument;

    .line 53
    return-void
.end method


# virtual methods
.method getDocument()Lorg/apache/index/poi/poifs/filesystem/POIFSDocument;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lorg/apache/index/poi/poifs/filesystem/DocumentNode;->_document:Lorg/apache/index/poi/poifs/filesystem/POIFSDocument;

    return-object v0
.end method

.method public getShortDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 163
    invoke-virtual {p0}, Lorg/apache/index/poi/poifs/filesystem/DocumentNode;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSize()I
    .locals 1

    .prologue
    .line 76
    invoke-virtual {p0}, Lorg/apache/index/poi/poifs/filesystem/DocumentNode;->getProperty()Lorg/apache/index/poi/poifs/property/Property;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/index/poi/poifs/property/Property;->getSize()I

    move-result v0

    return v0
.end method

.method public getViewableArray()[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 121
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    return-object v0
.end method

.method public getViewableIterator()Ljava/util/Iterator;
    .locals 2

    .prologue
    .line 134
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 136
    .local v0, "components":Ljava/util/List;
    invoke-virtual {p0}, Lorg/apache/index/poi/poifs/filesystem/DocumentNode;->getProperty()Lorg/apache/index/poi/poifs/property/Property;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 137
    iget-object v1, p0, Lorg/apache/index/poi/poifs/filesystem/DocumentNode;->_document:Lorg/apache/index/poi/poifs/filesystem/POIFSDocument;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 138
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    return-object v1
.end method

.method protected isDeleteOK()Z
    .locals 1

    .prologue
    .line 106
    const/4 v0, 0x1

    return v0
.end method

.method public isDocumentEntry()Z
    .locals 1

    .prologue
    .line 90
    const/4 v0, 0x1

    return v0
.end method

.method public preferArray()Z
    .locals 1

    .prologue
    .line 151
    const/4 v0, 0x0

    return v0
.end method
