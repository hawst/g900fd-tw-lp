.class public Lorg/apache/index/poi/util/StringUtil;
.super Ljava/lang/Object;
.source "StringUtil.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/index/poi/util/StringUtil$StringsIterator;
    }
.end annotation


# static fields
.field private static final ENCODING_ISO_8859_1:Ljava/lang/String; = "ISO-8859-1"


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    return-void
.end method

.method public static format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    .locals 7
    .param p0, "message"    # Ljava/lang/String;
    .param p1, "params"    # [Ljava/lang/Object;

    .prologue
    const/16 v6, 0x25

    .line 289
    const/4 v0, 0x0

    .line 290
    .local v0, "currentParamNumber":I
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    .line 291
    .local v2, "formattedMessage":Ljava/lang/StringBuffer;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v4

    if-lt v3, v4, :cond_0

    .line 318
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4

    .line 292
    :cond_0
    invoke-virtual {p0, v3}, Ljava/lang/String;->charAt(I)C

    move-result v4

    if-ne v4, v6, :cond_3

    .line 293
    array-length v4, p1

    if-lt v0, v4, :cond_1

    .line 294
    const-string/jumbo v4, "?missing data?"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 291
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 296
    :cond_1
    aget-object v4, p1, v0

    instance-of v4, v4, Ljava/lang/Number;

    if-eqz v4, :cond_2

    .line 297
    add-int/lit8 v4, v3, 0x1

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v5

    if-ge v4, v5, :cond_2

    .line 300
    add-int/lit8 v1, v0, 0x1

    .end local v0    # "currentParamNumber":I
    .local v1, "currentParamNumber":I
    aget-object v4, p1, v0

    check-cast v4, Ljava/lang/Number;

    .line 301
    add-int/lit8 v5, v3, 0x1

    invoke-virtual {p0, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    .line 299
    invoke-static {v4, v5, v2}, Lorg/apache/index/poi/util/StringUtil;->matchOptionalFormatting(Ljava/lang/Number;Ljava/lang/String;Ljava/lang/StringBuffer;)I

    move-result v4

    add-int/2addr v3, v4

    move v0, v1

    .line 303
    .end local v1    # "currentParamNumber":I
    .restart local v0    # "currentParamNumber":I
    goto :goto_1

    .line 305
    :cond_2
    add-int/lit8 v1, v0, 0x1

    .end local v0    # "currentParamNumber":I
    .restart local v1    # "currentParamNumber":I
    aget-object v4, p1, v0

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    .line 304
    invoke-virtual {v2, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move v0, v1

    .line 307
    .end local v1    # "currentParamNumber":I
    .restart local v0    # "currentParamNumber":I
    goto :goto_1

    .line 308
    :cond_3
    invoke-virtual {p0, v3}, Ljava/lang/String;->charAt(I)C

    move-result v4

    const/16 v5, 0x5c

    if-ne v4, v5, :cond_4

    .line 309
    add-int/lit8 v4, v3, 0x1

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v5

    if-ge v4, v5, :cond_4

    .line 310
    add-int/lit8 v4, v3, 0x1

    invoke-virtual {p0, v4}, Ljava/lang/String;->charAt(I)C

    move-result v4

    if-ne v4, v6, :cond_4

    .line 311
    invoke-virtual {v2, v6}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 312
    add-int/lit8 v3, v3, 0x1

    .line 313
    goto :goto_1

    .line 314
    :cond_4
    invoke-virtual {p0, v3}, Ljava/lang/String;->charAt(I)C

    move-result v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_1
.end method

.method public static getEncodedSize(Ljava/lang/String;)I
    .locals 3
    .param p0, "value"    # Ljava/lang/String;

    .prologue
    .line 210
    const/4 v0, 0x3

    .line 211
    .local v0, "result":I
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    invoke-static {p0}, Lorg/apache/index/poi/util/StringUtil;->hasMultibyte(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x2

    :goto_0
    mul-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 212
    return v0

    .line 211
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public static getFromCompressedUnicode([BII)Ljava/lang/String;
    .locals 4
    .param p0, "string"    # [B
    .param p1, "offset"    # I
    .param p2, "len"    # I

    .prologue
    .line 111
    :try_start_0
    array-length v2, p0

    sub-int/2addr v2, p1

    invoke-static {p2, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 112
    .local v1, "len_to_use":I
    new-instance v2, Ljava/lang/String;

    const-string/jumbo v3, "ISO-8859-1"

    invoke-direct {v2, p0, p1, v1, v3}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v2

    .line 113
    .end local v1    # "len_to_use":I
    :catch_0
    move-exception v0

    .line 114
    .local v0, "e":Ljava/io/UnsupportedEncodingException;
    new-instance v2, Ljava/lang/RuntimeException;

    invoke-direct {v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v2
.end method

.method public static getFromUnicodeLE([B)Ljava/lang/String;
    .locals 2
    .param p0, "string"    # [B

    .prologue
    .line 92
    array-length v0, p0

    if-nez v0, :cond_0

    const-string/jumbo v0, ""

    .line 93
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    array-length v1, p0

    div-int/lit8 v1, v1, 0x2

    invoke-static {p0, v0, v1}, Lorg/apache/index/poi/util/StringUtil;->getFromUnicodeLE([BII)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static getFromUnicodeLE([BII)Ljava/lang/String;
    .locals 4
    .param p0, "string"    # [B
    .param p1, "offset"    # I
    .param p2, "len"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/ArrayIndexOutOfBoundsException;,
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 67
    if-ltz p1, :cond_0

    array-length v1, p0

    if-lt p1, v1, :cond_1

    .line 68
    :cond_0
    new-instance v1, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Illegal offset "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " (String data is of length "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    array-length v3, p0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 70
    :cond_1
    if-ltz p2, :cond_2

    array-length v1, p0

    sub-int/2addr v1, p1

    div-int/lit8 v1, v1, 0x2

    if-ge v1, p2, :cond_3

    .line 71
    :cond_2
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Illegal length "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 75
    :cond_3
    :try_start_0
    new-instance v1, Ljava/lang/String;

    mul-int/lit8 v2, p2, 0x2

    const-string/jumbo v3, "UTF-16LE"

    invoke-direct {v1, p0, p1, v2, v3}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    .line 76
    :catch_0
    move-exception v0

    .line 77
    .local v0, "e":Ljava/io/UnsupportedEncodingException;
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public static getPreferredEncoding()Ljava/lang/String;
    .locals 1

    .prologue
    .line 359
    const-string/jumbo v0, "ISO-8859-1"

    return-object v0
.end method

.method public static hasMultibyte(Ljava/lang/String;)Z
    .locals 4
    .param p0, "value"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 369
    if-nez p0, :cond_1

    .line 377
    :cond_0
    :goto_0
    return v2

    .line 371
    :cond_1
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    if-ge v1, v3, :cond_0

    .line 372
    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 373
    .local v0, "c":C
    const/16 v3, 0xff

    if-le v0, v3, :cond_2

    .line 374
    const/4 v2, 0x1

    goto :goto_0

    .line 371
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method public static isUnicodeString(Ljava/lang/String;)Z
    .locals 5
    .param p0, "value"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x1

    .line 388
    :try_start_0
    new-instance v2, Ljava/lang/String;

    const-string/jumbo v3, "ISO-8859-1"

    invoke-virtual {p0, v3}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v3

    .line 389
    const-string/jumbo v4, "ISO-8859-1"

    invoke-direct {v2, v3, v4}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    .line 388
    invoke-virtual {p0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 389
    if-eqz v2, :cond_0

    .line 388
    const/4 v1, 0x0

    .line 391
    :cond_0
    :goto_0
    return v1

    .line 390
    :catch_0
    move-exception v0

    .line 391
    .local v0, "e":Ljava/io/UnsupportedEncodingException;
    goto :goto_0
.end method

.method private static matchOptionalFormatting(Ljava/lang/Number;Ljava/lang/String;Ljava/lang/StringBuffer;)I
    .locals 7
    .param p0, "number"    # Ljava/lang/Number;
    .param p1, "formatting"    # Ljava/lang/String;
    .param p2, "outputTo"    # Ljava/lang/StringBuffer;

    .prologue
    const/16 v6, 0x2e

    const/4 v2, 0x2

    const/4 v1, 0x1

    const/4 v5, 0x0

    .line 326
    invoke-static {}, Ljava/text/NumberFormat;->getInstance()Ljava/text/NumberFormat;

    move-result-object v0

    .line 327
    .local v0, "numberFormat":Ljava/text/NumberFormat;
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_1

    .line 328
    invoke-virtual {p1, v5}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-static {v3}, Ljava/lang/Character;->isDigit(C)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 330
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-virtual {p1, v5}, Ljava/lang/String;->charAt(I)C

    move-result v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    .line 329
    invoke-virtual {v0, v3}, Ljava/text/NumberFormat;->setMinimumIntegerDigits(I)V

    .line 331
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    if-ge v2, v3, :cond_0

    .line 332
    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    move-result v3

    if-ne v3, v6, :cond_0

    .line 333
    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-static {v3}, Ljava/lang/Character;->isDigit(C)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 335
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 334
    invoke-virtual {v0, v1}, Ljava/text/NumberFormat;->setMaximumFractionDigits(I)V

    .line 336
    new-instance v1, Ljava/text/FieldPosition;

    invoke-direct {v1, v5}, Ljava/text/FieldPosition;-><init>(I)V

    invoke-virtual {v0, p0, p2, v1}, Ljava/text/NumberFormat;->format(Ljava/lang/Object;Ljava/lang/StringBuffer;Ljava/text/FieldPosition;)Ljava/lang/StringBuffer;

    .line 337
    const/4 v1, 0x3

    .line 352
    :goto_0
    return v1

    .line 339
    :cond_0
    new-instance v2, Ljava/text/FieldPosition;

    invoke-direct {v2, v5}, Ljava/text/FieldPosition;-><init>(I)V

    invoke-virtual {v0, p0, p2, v2}, Ljava/text/NumberFormat;->format(Ljava/lang/Object;Ljava/lang/StringBuffer;Ljava/text/FieldPosition;)Ljava/lang/StringBuffer;

    goto :goto_0

    .line 342
    :cond_1
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_2

    invoke-virtual {p1, v5}, Ljava/lang/String;->charAt(I)C

    move-result v3

    if-ne v3, v6, :cond_2

    .line 343
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    if-ge v1, v3, :cond_2

    .line 344
    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-static {v3}, Ljava/lang/Character;->isDigit(C)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 346
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v3, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 345
    invoke-virtual {v0, v1}, Ljava/text/NumberFormat;->setMaximumFractionDigits(I)V

    .line 347
    new-instance v1, Ljava/text/FieldPosition;

    invoke-direct {v1, v5}, Ljava/text/FieldPosition;-><init>(I)V

    invoke-virtual {v0, p0, p2, v1}, Ljava/text/NumberFormat;->format(Ljava/lang/Object;Ljava/lang/StringBuffer;Ljava/text/FieldPosition;)Ljava/lang/StringBuffer;

    move v1, v2

    .line 348
    goto :goto_0

    .line 351
    :cond_2
    new-instance v2, Ljava/text/FieldPosition;

    invoke-direct {v2, v5}, Ljava/text/FieldPosition;-><init>(I)V

    invoke-virtual {v0, p0, p2, v2}, Ljava/text/NumberFormat;->format(Ljava/lang/Object;Ljava/lang/StringBuffer;Ljava/text/FieldPosition;)Ljava/lang/StringBuffer;

    goto :goto_0
.end method

.method public static putCompressedUnicode(Ljava/lang/String;Lorg/apache/index/poi/util/LittleEndianOutput;)V
    .locals 3
    .param p0, "input"    # Ljava/lang/String;
    .param p1, "out"    # Lorg/apache/index/poi/util/LittleEndianOutput;

    .prologue
    .line 237
    :try_start_0
    const-string/jumbo v2, "ISO-8859-1"

    invoke-virtual {p0, v2}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 241
    .local v0, "bytes":[B
    invoke-interface {p1, v0}, Lorg/apache/index/poi/util/LittleEndianOutput;->write([B)V

    .line 242
    return-void

    .line 238
    .end local v0    # "bytes":[B
    :catch_0
    move-exception v1

    .line 239
    .local v1, "e":Ljava/io/UnsupportedEncodingException;
    new-instance v2, Ljava/lang/RuntimeException;

    invoke-direct {v2, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v2
.end method

.method public static putCompressedUnicode(Ljava/lang/String;[BI)V
    .locals 4
    .param p0, "input"    # Ljava/lang/String;
    .param p1, "output"    # [B
    .param p2, "offset"    # I

    .prologue
    .line 228
    :try_start_0
    const-string/jumbo v2, "ISO-8859-1"

    invoke-virtual {p0, v2}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 232
    .local v0, "bytes":[B
    const/4 v2, 0x0

    array-length v3, v0

    invoke-static {v0, v2, p1, p2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 233
    return-void

    .line 229
    .end local v0    # "bytes":[B
    :catch_0
    move-exception v1

    .line 230
    .local v1, "e":Ljava/io/UnsupportedEncodingException;
    new-instance v2, Ljava/lang/RuntimeException;

    invoke-direct {v2, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v2
.end method

.method public static putUnicodeLE(Ljava/lang/String;Lorg/apache/index/poi/util/LittleEndianOutput;)V
    .locals 3
    .param p0, "input"    # Ljava/lang/String;
    .param p1, "out"    # Lorg/apache/index/poi/util/LittleEndianOutput;

    .prologue
    .line 265
    :try_start_0
    const-string/jumbo v2, "UTF-16LE"

    invoke-virtual {p0, v2}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 269
    .local v0, "bytes":[B
    invoke-interface {p1, v0}, Lorg/apache/index/poi/util/LittleEndianOutput;->write([B)V

    .line 270
    return-void

    .line 266
    .end local v0    # "bytes":[B
    :catch_0
    move-exception v1

    .line 267
    .local v1, "e":Ljava/io/UnsupportedEncodingException;
    new-instance v2, Ljava/lang/RuntimeException;

    invoke-direct {v2, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v2
.end method

.method public static putUnicodeLE(Ljava/lang/String;[BI)V
    .locals 4
    .param p0, "input"    # Ljava/lang/String;
    .param p1, "output"    # [B
    .param p2, "offset"    # I

    .prologue
    .line 256
    :try_start_0
    const-string/jumbo v2, "UTF-16LE"

    invoke-virtual {p0, v2}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 260
    .local v0, "bytes":[B
    const/4 v2, 0x0

    array-length v3, v0

    invoke-static {v0, v2, p1, p2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 261
    return-void

    .line 257
    .end local v0    # "bytes":[B
    :catch_0
    move-exception v1

    .line 258
    .local v1, "e":Ljava/io/UnsupportedEncodingException;
    new-instance v2, Ljava/lang/RuntimeException;

    invoke-direct {v2, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v2
.end method

.method public static readCompressedUnicode(Lorg/apache/index/poi/util/LittleEndianInput;I)Ljava/lang/String;
    .locals 3
    .param p0, "in"    # Lorg/apache/index/poi/util/LittleEndianInput;
    .param p1, "nChars"    # I

    .prologue
    .line 118
    new-array v0, p1, [C

    .line 119
    .local v0, "buf":[C
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v2, v0

    if-lt v1, v2, :cond_0

    .line 122
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v0}, Ljava/lang/String;-><init>([C)V

    return-object v2

    .line 120
    :cond_0
    invoke-interface {p0}, Lorg/apache/index/poi/util/LittleEndianInput;->readUByte()I

    move-result v2

    int-to-char v2, v2

    aput-char v2, v0, v1

    .line 119
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public static readUnicodeLE(Lorg/apache/index/poi/util/LittleEndianInput;I)Ljava/lang/String;
    .locals 3
    .param p0, "in"    # Lorg/apache/index/poi/util/LittleEndianInput;
    .param p1, "nChars"    # I

    .prologue
    .line 273
    new-array v0, p1, [C

    .line 274
    .local v0, "buf":[C
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v2, v0

    if-lt v1, v2, :cond_0

    .line 277
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v0}, Ljava/lang/String;-><init>([C)V

    return-object v2

    .line 275
    :cond_0
    invoke-interface {p0}, Lorg/apache/index/poi/util/LittleEndianInput;->readUShort()I

    move-result v2

    int-to-char v2, v2

    aput-char v2, v0, v1

    .line 274
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public static writeUnicodeString(Lorg/apache/index/poi/util/LittleEndianOutput;Ljava/lang/String;)V
    .locals 3
    .param p0, "out"    # Lorg/apache/index/poi/util/LittleEndianOutput;
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 174
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    .line 175
    .local v1, "nChars":I
    invoke-interface {p0, v1}, Lorg/apache/index/poi/util/LittleEndianOutput;->writeShort(I)V

    .line 176
    invoke-static {p1}, Lorg/apache/index/poi/util/StringUtil;->hasMultibyte(Ljava/lang/String;)Z

    move-result v0

    .line 177
    .local v0, "is16Bit":Z
    if-eqz v0, :cond_0

    const/4 v2, 0x1

    :goto_0
    invoke-interface {p0, v2}, Lorg/apache/index/poi/util/LittleEndianOutput;->writeByte(I)V

    .line 178
    if-eqz v0, :cond_1

    .line 179
    invoke-static {p1, p0}, Lorg/apache/index/poi/util/StringUtil;->putUnicodeLE(Ljava/lang/String;Lorg/apache/index/poi/util/LittleEndianOutput;)V

    .line 183
    :goto_1
    return-void

    .line 177
    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    .line 181
    :cond_1
    invoke-static {p1, p0}, Lorg/apache/index/poi/util/StringUtil;->putCompressedUnicode(Ljava/lang/String;Lorg/apache/index/poi/util/LittleEndianOutput;)V

    goto :goto_1
.end method

.method public static writeUnicodeStringFlagAndData(Lorg/apache/index/poi/util/LittleEndianOutput;Ljava/lang/String;)V
    .locals 2
    .param p0, "out"    # Lorg/apache/index/poi/util/LittleEndianOutput;
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 197
    invoke-static {p1}, Lorg/apache/index/poi/util/StringUtil;->hasMultibyte(Ljava/lang/String;)Z

    move-result v0

    .line 198
    .local v0, "is16Bit":Z
    if-eqz v0, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-interface {p0, v1}, Lorg/apache/index/poi/util/LittleEndianOutput;->writeByte(I)V

    .line 199
    if-eqz v0, :cond_1

    .line 200
    invoke-static {p1, p0}, Lorg/apache/index/poi/util/StringUtil;->putUnicodeLE(Ljava/lang/String;Lorg/apache/index/poi/util/LittleEndianOutput;)V

    .line 204
    :goto_1
    return-void

    .line 198
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 202
    :cond_1
    invoke-static {p1, p0}, Lorg/apache/index/poi/util/StringUtil;->putCompressedUnicode(Ljava/lang/String;Lorg/apache/index/poi/util/LittleEndianOutput;)V

    goto :goto_1
.end method
