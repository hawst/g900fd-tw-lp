.class public Lorg/apache/index/poi/poifs/filesystem/POIFSDocumentPath;
.super Ljava/lang/Object;
.source "POIFSDocumentPath.java"


# static fields
.field private static final log:Lorg/apache/index/poi/util/POILogger;


# instance fields
.field private components:[Ljava/lang/String;

.field private hashcode:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    const-class v0, Lorg/apache/index/poi/poifs/filesystem/POIFSDocumentPath;

    invoke-static {v0}, Lorg/apache/index/poi/util/POILogFactory;->getLogger(Ljava/lang/Class;)Lorg/apache/index/poi/util/POILogger;

    move-result-object v0

    sput-object v0, Lorg/apache/index/poi/poifs/filesystem/POIFSDocumentPath;->log:Lorg/apache/index/poi/util/POILogger;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 95
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput v0, p0, Lorg/apache/index/poi/poifs/filesystem/POIFSDocumentPath;->hashcode:I

    .line 97
    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lorg/apache/index/poi/poifs/filesystem/POIFSDocumentPath;->components:[Ljava/lang/String;

    .line 98
    return-void
.end method

.method public constructor <init>(Lorg/apache/index/poi/poifs/filesystem/POIFSDocumentPath;[Ljava/lang/String;)V
    .locals 5
    .param p1, "path"    # Lorg/apache/index/poi/poifs/filesystem/POIFSDocumentPath;
    .param p2, "components"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 112
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    const/4 v1, 0x0

    iput v1, p0, Lorg/apache/index/poi/poifs/filesystem/POIFSDocumentPath;->hashcode:I

    .line 116
    if-nez p2, :cond_1

    .line 118
    iget-object v1, p1, Lorg/apache/index/poi/poifs/filesystem/POIFSDocumentPath;->components:[Ljava/lang/String;

    array-length v1, v1

    new-array v1, v1, [Ljava/lang/String;

    iput-object v1, p0, Lorg/apache/index/poi/poifs/filesystem/POIFSDocumentPath;->components:[Ljava/lang/String;

    .line 125
    :goto_0
    const/4 v0, 0x0

    .local v0, "j":I
    :goto_1
    iget-object v1, p1, Lorg/apache/index/poi/poifs/filesystem/POIFSDocumentPath;->components:[Ljava/lang/String;

    array-length v1, v1

    if-lt v0, v1, :cond_2

    .line 129
    if-eqz p2, :cond_0

    .line 131
    const/4 v0, 0x0

    :goto_2
    array-length v1, p2

    if-lt v0, v1, :cond_3

    .line 148
    :cond_0
    return-void

    .line 123
    .end local v0    # "j":I
    :cond_1
    iget-object v1, p1, Lorg/apache/index/poi/poifs/filesystem/POIFSDocumentPath;->components:[Ljava/lang/String;

    array-length v1, v1

    array-length v2, p2

    add-int/2addr v1, v2

    new-array v1, v1, [Ljava/lang/String;

    .line 122
    iput-object v1, p0, Lorg/apache/index/poi/poifs/filesystem/POIFSDocumentPath;->components:[Ljava/lang/String;

    goto :goto_0

    .line 127
    .restart local v0    # "j":I
    :cond_2
    iget-object v1, p0, Lorg/apache/index/poi/poifs/filesystem/POIFSDocumentPath;->components:[Ljava/lang/String;

    iget-object v2, p1, Lorg/apache/index/poi/poifs/filesystem/POIFSDocumentPath;->components:[Ljava/lang/String;

    aget-object v2, v2, v0

    aput-object v2, v1, v0

    .line 125
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 133
    :cond_3
    aget-object v1, p2, v0

    if-nez v1, :cond_4

    .line 135
    new-instance v1, Ljava/lang/IllegalArgumentException;

    .line 136
    const-string/jumbo v2, "components cannot contain null"

    .line 135
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 138
    :cond_4
    aget-object v1, p2, v0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_5

    .line 140
    sget-object v1, Lorg/apache/index/poi/poifs/filesystem/POIFSDocumentPath;->log:Lorg/apache/index/poi/util/POILogger;

    sget v2, Lorg/apache/index/poi/util/POILogger;->WARN:I

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "Directory under "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " has an empty name, "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 141
    const-string/jumbo v4, "not all OLE2 readers will handle this file correctly!"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 140
    invoke-virtual {v1, v2, v3}, Lorg/apache/index/poi/util/POILogger;->log(ILjava/lang/Object;)V

    .line 144
    :cond_5
    iget-object v1, p0, Lorg/apache/index/poi/poifs/filesystem/POIFSDocumentPath;->components:[Ljava/lang/String;

    iget-object v2, p1, Lorg/apache/index/poi/poifs/filesystem/POIFSDocumentPath;->components:[Ljava/lang/String;

    array-length v2, v2

    add-int/2addr v2, v0

    .line 145
    aget-object v3, p2, v0

    .line 144
    aput-object v3, v1, v2

    .line 131
    add-int/lit8 v0, v0, 0x1

    goto :goto_2
.end method

.method public constructor <init>([Ljava/lang/String;)V
    .locals 3
    .param p1, "components"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput v1, p0, Lorg/apache/index/poi/poifs/filesystem/POIFSDocumentPath;->hashcode:I

    .line 68
    if-nez p1, :cond_1

    .line 70
    new-array v1, v1, [Ljava/lang/String;

    iput-object v1, p0, Lorg/apache/index/poi/poifs/filesystem/POIFSDocumentPath;->components:[Ljava/lang/String;

    .line 86
    :cond_0
    return-void

    .line 74
    :cond_1
    array-length v1, p1

    new-array v1, v1, [Ljava/lang/String;

    iput-object v1, p0, Lorg/apache/index/poi/poifs/filesystem/POIFSDocumentPath;->components:[Ljava/lang/String;

    .line 75
    const/4 v0, 0x0

    .local v0, "j":I
    :goto_0
    array-length v1, p1

    if-ge v0, v1, :cond_0

    .line 77
    aget-object v1, p1, v0

    if-eqz v1, :cond_2

    .line 78
    aget-object v1, p1, v0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_3

    .line 80
    :cond_2
    new-instance v1, Ljava/lang/IllegalArgumentException;

    .line 81
    const-string/jumbo v2, "components cannot contain null or empty strings"

    .line 80
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 83
    :cond_3
    iget-object v1, p0, Lorg/apache/index/poi/poifs/filesystem/POIFSDocumentPath;->components:[Ljava/lang/String;

    aget-object v2, p1, v0

    aput-object v2, v1, v0

    .line 75
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 162
    const/4 v2, 0x0

    .line 164
    .local v2, "rval":Z
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-ne v3, v4, :cond_0

    .line 166
    if-ne p0, p1, :cond_1

    .line 168
    const/4 v2, 0x1

    .line 189
    :cond_0
    :goto_0
    return v2

    :cond_1
    move-object v1, p1

    .line 172
    check-cast v1, Lorg/apache/index/poi/poifs/filesystem/POIFSDocumentPath;

    .line 174
    .local v1, "path":Lorg/apache/index/poi/poifs/filesystem/POIFSDocumentPath;
    iget-object v3, v1, Lorg/apache/index/poi/poifs/filesystem/POIFSDocumentPath;->components:[Ljava/lang/String;

    array-length v3, v3

    iget-object v4, p0, Lorg/apache/index/poi/poifs/filesystem/POIFSDocumentPath;->components:[Ljava/lang/String;

    array-length v4, v4

    if-ne v3, v4, :cond_0

    .line 176
    const/4 v2, 0x1

    .line 177
    const/4 v0, 0x0

    .local v0, "j":I
    :goto_1
    iget-object v3, p0, Lorg/apache/index/poi/poifs/filesystem/POIFSDocumentPath;->components:[Ljava/lang/String;

    array-length v3, v3

    if-ge v0, v3, :cond_0

    .line 179
    iget-object v3, v1, Lorg/apache/index/poi/poifs/filesystem/POIFSDocumentPath;->components:[Ljava/lang/String;

    aget-object v3, v3, v0

    .line 180
    iget-object v4, p0, Lorg/apache/index/poi/poifs/filesystem/POIFSDocumentPath;->components:[Ljava/lang/String;

    aget-object v4, v4, v0

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 182
    const/4 v2, 0x0

    .line 183
    goto :goto_0

    .line 177
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public getComponent(I)Ljava/lang/String;
    .locals 1
    .param p1, "n"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/ArrayIndexOutOfBoundsException;
        }
    .end annotation

    .prologue
    .line 233
    iget-object v0, p0, Lorg/apache/index/poi/poifs/filesystem/POIFSDocumentPath;->components:[Ljava/lang/String;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public getParent()Lorg/apache/index/poi/poifs/filesystem/POIFSDocumentPath;
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v4, 0x0

    .line 246
    iget-object v3, p0, Lorg/apache/index/poi/poifs/filesystem/POIFSDocumentPath;->components:[Ljava/lang/String;

    array-length v3, v3

    add-int/lit8 v0, v3, -0x1

    .line 248
    .local v0, "length":I
    if-gez v0, :cond_0

    move-object v1, v2

    .line 256
    :goto_0
    return-object v1

    .line 252
    :cond_0
    new-instance v1, Lorg/apache/index/poi/poifs/filesystem/POIFSDocumentPath;

    invoke-direct {v1, v2}, Lorg/apache/index/poi/poifs/filesystem/POIFSDocumentPath;-><init>([Ljava/lang/String;)V

    .line 254
    .local v1, "parent":Lorg/apache/index/poi/poifs/filesystem/POIFSDocumentPath;
    new-array v2, v0, [Ljava/lang/String;

    iput-object v2, v1, Lorg/apache/index/poi/poifs/filesystem/POIFSDocumentPath;->components:[Ljava/lang/String;

    .line 255
    iget-object v2, p0, Lorg/apache/index/poi/poifs/filesystem/POIFSDocumentPath;->components:[Ljava/lang/String;

    iget-object v3, v1, Lorg/apache/index/poi/poifs/filesystem/POIFSDocumentPath;->components:[Ljava/lang/String;

    invoke-static {v2, v4, v3, v4, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 200
    iget v1, p0, Lorg/apache/index/poi/poifs/filesystem/POIFSDocumentPath;->hashcode:I

    if-nez v1, :cond_0

    .line 202
    const/4 v0, 0x0

    .local v0, "j":I
    :goto_0
    iget-object v1, p0, Lorg/apache/index/poi/poifs/filesystem/POIFSDocumentPath;->components:[Ljava/lang/String;

    array-length v1, v1

    if-lt v0, v1, :cond_1

    .line 207
    .end local v0    # "j":I
    :cond_0
    iget v1, p0, Lorg/apache/index/poi/poifs/filesystem/POIFSDocumentPath;->hashcode:I

    return v1

    .line 204
    .restart local v0    # "j":I
    :cond_1
    iget v1, p0, Lorg/apache/index/poi/poifs/filesystem/POIFSDocumentPath;->hashcode:I

    iget-object v2, p0, Lorg/apache/index/poi/poifs/filesystem/POIFSDocumentPath;->components:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    add-int/2addr v1, v2

    iput v1, p0, Lorg/apache/index/poi/poifs/filesystem/POIFSDocumentPath;->hashcode:I

    .line 202
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public length()I
    .locals 1

    .prologue
    .line 216
    iget-object v0, p0, Lorg/apache/index/poi/poifs/filesystem/POIFSDocumentPath;->components:[Ljava/lang/String;

    array-length v0, v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 270
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 271
    .local v0, "b":Ljava/lang/StringBuffer;
    invoke-virtual {p0}, Lorg/apache/index/poi/poifs/filesystem/POIFSDocumentPath;->length()I

    move-result v2

    .line 273
    .local v2, "l":I
    sget-char v3, Ljava/io/File;->separatorChar:C

    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 274
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-lt v1, v2, :cond_0

    .line 282
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3

    .line 276
    :cond_0
    invoke-virtual {p0, v1}, Lorg/apache/index/poi/poifs/filesystem/POIFSDocumentPath;->getComponent(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 277
    add-int/lit8 v3, v2, -0x1

    if-ge v1, v3, :cond_1

    .line 279
    sget-char v3, Ljava/io/File;->separatorChar:C

    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 274
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method
