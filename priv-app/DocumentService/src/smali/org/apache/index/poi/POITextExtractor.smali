.class public abstract Lorg/apache/index/poi/POITextExtractor;
.super Ljava/lang/Object;
.source "POITextExtractor.java"


# instance fields
.field protected document:Lorg/apache/index/poi/POIDocument;


# direct methods
.method public constructor <init>(Lorg/apache/index/poi/POIDocument;)V
    .locals 0
    .param p1, "document"    # Lorg/apache/index/poi/POIDocument;

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-object p1, p0, Lorg/apache/index/poi/POITextExtractor;->document:Lorg/apache/index/poi/POIDocument;

    .line 39
    return-void
.end method

.method protected constructor <init>(Lorg/apache/index/poi/POITextExtractor;)V
    .locals 1
    .param p1, "otherExtractor"    # Lorg/apache/index/poi/POITextExtractor;

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    iget-object v0, p1, Lorg/apache/index/poi/POITextExtractor;->document:Lorg/apache/index/poi/POIDocument;

    iput-object v0, p0, Lorg/apache/index/poi/POITextExtractor;->document:Lorg/apache/index/poi/POIDocument;

    .line 47
    return-void
.end method


# virtual methods
.method public abstract getMetadataTextExtractor()Lorg/apache/index/poi/POITextExtractor;
.end method

.method public abstract getText()Ljava/lang/String;
.end method
