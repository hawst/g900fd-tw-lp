.class public Lorg/apache/index/poi/poifs/filesystem/DocumentDescriptor;
.super Ljava/lang/Object;
.source "DocumentDescriptor.java"


# instance fields
.field private hashcode:I

.field private name:Ljava/lang/String;

.field private path:Lorg/apache/index/poi/poifs/filesystem/POIFSDocumentPath;


# direct methods
.method public constructor <init>(Lorg/apache/index/poi/poifs/filesystem/POIFSDocumentPath;Ljava/lang/String;)V
    .locals 2
    .param p1, "path"    # Lorg/apache/index/poi/poifs/filesystem/POIFSDocumentPath;
    .param p2, "name"    # Ljava/lang/String;

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/index/poi/poifs/filesystem/DocumentDescriptor;->hashcode:I

    .line 44
    if-nez p1, :cond_0

    .line 46
    new-instance v0, Ljava/lang/NullPointerException;

    const-string/jumbo v1, "path must not be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 48
    :cond_0
    if-nez p2, :cond_1

    .line 50
    new-instance v0, Ljava/lang/NullPointerException;

    const-string/jumbo v1, "name must not be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 52
    :cond_1
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_2

    .line 54
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "name cannot be empty"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 56
    :cond_2
    iput-object p1, p0, Lorg/apache/index/poi/poifs/filesystem/DocumentDescriptor;->path:Lorg/apache/index/poi/poifs/filesystem/POIFSDocumentPath;

    .line 57
    iput-object p2, p0, Lorg/apache/index/poi/poifs/filesystem/DocumentDescriptor;->name:Ljava/lang/String;

    .line 58
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 71
    const/4 v1, 0x0

    .line 73
    .local v1, "rval":Z
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-ne v2, v3, :cond_0

    .line 75
    if-ne p0, p1, :cond_1

    .line 77
    const/4 v1, 0x1

    .line 87
    :cond_0
    :goto_0
    return v1

    :cond_1
    move-object v0, p1

    .line 81
    check-cast v0, Lorg/apache/index/poi/poifs/filesystem/DocumentDescriptor;

    .line 83
    .local v0, "descriptor":Lorg/apache/index/poi/poifs/filesystem/DocumentDescriptor;
    iget-object v2, p0, Lorg/apache/index/poi/poifs/filesystem/DocumentDescriptor;->path:Lorg/apache/index/poi/poifs/filesystem/POIFSDocumentPath;

    iget-object v3, v0, Lorg/apache/index/poi/poifs/filesystem/DocumentDescriptor;->path:Lorg/apache/index/poi/poifs/filesystem/POIFSDocumentPath;

    invoke-virtual {v2, v3}, Lorg/apache/index/poi/poifs/filesystem/POIFSDocumentPath;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 84
    iget-object v2, p0, Lorg/apache/index/poi/poifs/filesystem/DocumentDescriptor;->name:Ljava/lang/String;

    iget-object v3, v0, Lorg/apache/index/poi/poifs/filesystem/DocumentDescriptor;->name:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 83
    const/4 v1, 0x1

    :goto_1
    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 98
    iget v0, p0, Lorg/apache/index/poi/poifs/filesystem/DocumentDescriptor;->hashcode:I

    if-nez v0, :cond_0

    .line 100
    iget-object v0, p0, Lorg/apache/index/poi/poifs/filesystem/DocumentDescriptor;->path:Lorg/apache/index/poi/poifs/filesystem/POIFSDocumentPath;

    invoke-virtual {v0}, Lorg/apache/index/poi/poifs/filesystem/POIFSDocumentPath;->hashCode()I

    move-result v0

    iget-object v1, p0, Lorg/apache/index/poi/poifs/filesystem/DocumentDescriptor;->name:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    iput v0, p0, Lorg/apache/index/poi/poifs/filesystem/DocumentDescriptor;->hashcode:I

    .line 102
    :cond_0
    iget v0, p0, Lorg/apache/index/poi/poifs/filesystem/DocumentDescriptor;->hashcode:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 107
    new-instance v0, Ljava/lang/StringBuffer;

    iget-object v2, p0, Lorg/apache/index/poi/poifs/filesystem/DocumentDescriptor;->path:Lorg/apache/index/poi/poifs/filesystem/POIFSDocumentPath;

    invoke-virtual {v2}, Lorg/apache/index/poi/poifs/filesystem/POIFSDocumentPath;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    mul-int/lit8 v2, v2, 0x28

    invoke-direct {v0, v2}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 109
    .local v0, "buffer":Ljava/lang/StringBuffer;
    const/4 v1, 0x0

    .local v1, "j":I
    :goto_0
    iget-object v2, p0, Lorg/apache/index/poi/poifs/filesystem/DocumentDescriptor;->path:Lorg/apache/index/poi/poifs/filesystem/POIFSDocumentPath;

    invoke-virtual {v2}, Lorg/apache/index/poi/poifs/filesystem/POIFSDocumentPath;->length()I

    move-result v2

    if-lt v1, v2, :cond_0

    .line 113
    iget-object v2, p0, Lorg/apache/index/poi/poifs/filesystem/DocumentDescriptor;->name:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 114
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2

    .line 111
    :cond_0
    iget-object v2, p0, Lorg/apache/index/poi/poifs/filesystem/DocumentDescriptor;->path:Lorg/apache/index/poi/poifs/filesystem/POIFSDocumentPath;

    invoke-virtual {v2, v1}, Lorg/apache/index/poi/poifs/filesystem/POIFSDocumentPath;->getComponent(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string/jumbo v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 109
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method
