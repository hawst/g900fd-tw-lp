.class public final Lorg/apache/index/poi/hwpf/model/PAPX;
.super Lorg/apache/index/poi/hwpf/model/BytePropertyNode;
.source "PAPX.java"


# instance fields
.field private _hugeGrpprlOffset:I


# direct methods
.method public constructor <init>(IILorg/apache/index/poi/hwpf/model/CharIndexTranslator;Lorg/apache/index/poi/hwpf/sprm/SprmBuffer;[B)V
    .locals 1
    .param p1, "fcStart"    # I
    .param p2, "fcEnd"    # I
    .param p3, "translator"    # Lorg/apache/index/poi/hwpf/model/CharIndexTranslator;
    .param p4, "buf"    # Lorg/apache/index/poi/hwpf/sprm/SprmBuffer;
    .param p5, "dataStream"    # [B

    .prologue
    .line 49
    invoke-direct {p0, p1, p2, p3, p4}, Lorg/apache/index/poi/hwpf/model/BytePropertyNode;-><init>(IILorg/apache/index/poi/hwpf/model/CharIndexTranslator;Ljava/lang/Object;)V

    .line 37
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/index/poi/hwpf/model/PAPX;->_hugeGrpprlOffset:I

    .line 50
    invoke-direct {p0, p4, p5}, Lorg/apache/index/poi/hwpf/model/PAPX;->findHuge(Lorg/apache/index/poi/hwpf/sprm/SprmBuffer;[B)Lorg/apache/index/poi/hwpf/sprm/SprmBuffer;

    move-result-object p4

    .line 51
    if-eqz p4, :cond_0

    .line 52
    iput-object p4, p0, Lorg/apache/index/poi/hwpf/model/PAPX;->_buf:Ljava/lang/Object;

    .line 53
    :cond_0
    return-void
.end method

.method public constructor <init>(IILorg/apache/index/poi/hwpf/model/CharIndexTranslator;[B[B)V
    .locals 2
    .param p1, "fcStart"    # I
    .param p2, "fcEnd"    # I
    .param p3, "translator"    # Lorg/apache/index/poi/hwpf/model/CharIndexTranslator;
    .param p4, "papx"    # [B
    .param p5, "dataStream"    # [B

    .prologue
    .line 41
    new-instance v1, Lorg/apache/index/poi/hwpf/sprm/SprmBuffer;

    invoke-direct {v1, p4}, Lorg/apache/index/poi/hwpf/sprm/SprmBuffer;-><init>([B)V

    invoke-direct {p0, p1, p2, p3, v1}, Lorg/apache/index/poi/hwpf/model/BytePropertyNode;-><init>(IILorg/apache/index/poi/hwpf/model/CharIndexTranslator;Ljava/lang/Object;)V

    .line 37
    const/4 v1, -0x1

    iput v1, p0, Lorg/apache/index/poi/hwpf/model/PAPX;->_hugeGrpprlOffset:I

    .line 42
    new-instance v1, Lorg/apache/index/poi/hwpf/sprm/SprmBuffer;

    invoke-direct {v1, p4}, Lorg/apache/index/poi/hwpf/sprm/SprmBuffer;-><init>([B)V

    invoke-direct {p0, v1, p5}, Lorg/apache/index/poi/hwpf/model/PAPX;->findHuge(Lorg/apache/index/poi/hwpf/sprm/SprmBuffer;[B)Lorg/apache/index/poi/hwpf/sprm/SprmBuffer;

    move-result-object v0

    .line 43
    .local v0, "buf":Lorg/apache/index/poi/hwpf/sprm/SprmBuffer;
    if-eqz v0, :cond_0

    .line 44
    iput-object v0, p0, Lorg/apache/index/poi/hwpf/model/PAPX;->_buf:Ljava/lang/Object;

    .line 45
    :cond_0
    return-void
.end method

.method private findHuge(Lorg/apache/index/poi/hwpf/sprm/SprmBuffer;[B)Lorg/apache/index/poi/hwpf/sprm/SprmBuffer;
    .locals 10
    .param p1, "buf"    # Lorg/apache/index/poi/hwpf/sprm/SprmBuffer;
    .param p2, "datastream"    # [B

    .prologue
    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 57
    invoke-virtual {p1}, Lorg/apache/index/poi/hwpf/sprm/SprmBuffer;->toByteArray()[B

    move-result-object v0

    .line 58
    .local v0, "grpprl":[B
    array-length v5, v0

    const/16 v6, 0x8

    if-ne v5, v6, :cond_1

    if-eqz p2, :cond_1

    .line 60
    new-instance v4, Lorg/apache/index/poi/hwpf/sprm/SprmOperation;

    invoke-direct {v4, v0, v9}, Lorg/apache/index/poi/hwpf/sprm/SprmOperation;-><init>([BI)V

    .line 61
    .local v4, "sprm":Lorg/apache/index/poi/hwpf/sprm/SprmOperation;
    invoke-virtual {v4}, Lorg/apache/index/poi/hwpf/sprm/SprmOperation;->getOperation()I

    move-result v5

    const/16 v6, 0x45

    if-eq v5, v6, :cond_0

    invoke-virtual {v4}, Lorg/apache/index/poi/hwpf/sprm/SprmOperation;->getOperation()I

    move-result v5

    const/16 v6, 0x46

    if-ne v5, v6, :cond_1

    .line 62
    :cond_0
    invoke-virtual {v4}, Lorg/apache/index/poi/hwpf/sprm/SprmOperation;->getSizeCode()I

    move-result v5

    const/4 v6, 0x3

    if-ne v5, v6, :cond_1

    .line 64
    invoke-virtual {v4}, Lorg/apache/index/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v3

    .line 65
    .local v3, "hugeGrpprlOffset":I
    add-int/lit8 v5, v3, 0x1

    array-length v6, p2

    if-ge v5, v6, :cond_1

    .line 67
    invoke-static {p2, v3}, Lorg/apache/index/poi/util/LittleEndian;->getShort([BI)S

    move-result v1

    .line 68
    .local v1, "grpprlSize":I
    add-int v5, v3, v1

    array-length v6, p2

    if-ge v5, v6, :cond_1

    .line 70
    add-int/lit8 v5, v1, 0x2

    new-array v2, v5, [B

    .line 72
    .local v2, "hugeGrpprl":[B
    aget-byte v5, v0, v7

    aput-byte v5, v2, v7

    aget-byte v5, v0, v8

    aput-byte v5, v2, v8

    .line 74
    add-int/lit8 v5, v3, 0x2

    invoke-static {p2, v5, v2, v9, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 77
    iput v3, p0, Lorg/apache/index/poi/hwpf/model/PAPX;->_hugeGrpprlOffset:I

    .line 78
    new-instance v5, Lorg/apache/index/poi/hwpf/sprm/SprmBuffer;

    invoke-direct {v5, v2}, Lorg/apache/index/poi/hwpf/sprm/SprmBuffer;-><init>([B)V

    .line 83
    .end local v1    # "grpprlSize":I
    .end local v2    # "hugeGrpprl":[B
    .end local v3    # "hugeGrpprlOffset":I
    .end local v4    # "sprm":Lorg/apache/index/poi/hwpf/sprm/SprmOperation;
    :goto_0
    return-object v5

    :cond_1
    const/4 v5, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getGrpprl()[B
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/PAPX;->_buf:Ljava/lang/Object;

    check-cast v0, Lorg/apache/index/poi/hwpf/sprm/SprmBuffer;

    invoke-virtual {v0}, Lorg/apache/index/poi/hwpf/sprm/SprmBuffer;->toByteArray()[B

    move-result-object v0

    return-object v0
.end method

.method public getHugeGrpprlOffset()I
    .locals 1

    .prologue
    .line 94
    iget v0, p0, Lorg/apache/index/poi/hwpf/model/PAPX;->_hugeGrpprlOffset:I

    return v0
.end method

.method public getIstd()S
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 99
    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/model/PAPX;->getGrpprl()[B

    move-result-object v0

    .line 100
    .local v0, "buf":[B
    array-length v2, v0

    if-nez v2, :cond_0

    .line 108
    :goto_0
    return v1

    .line 104
    :cond_0
    array-length v2, v0

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    .line 106
    invoke-static {v0, v1}, Lorg/apache/index/poi/util/LittleEndian;->getUnsignedByte([BI)I

    move-result v1

    int-to-short v1, v1

    goto :goto_0

    .line 108
    :cond_1
    invoke-static {v0}, Lorg/apache/index/poi/util/LittleEndian;->getShort([B)S

    move-result v1

    goto :goto_0
.end method

.method public getSprmBuf()Lorg/apache/index/poi/hwpf/sprm/SprmBuffer;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/PAPX;->_buf:Ljava/lang/Object;

    check-cast v0, Lorg/apache/index/poi/hwpf/sprm/SprmBuffer;

    return-object v0
.end method
