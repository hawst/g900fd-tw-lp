.class public abstract Lorg/apache/index/poi/hwpf/model/PropertyNode;
.super Ljava/lang/Object;
.source "PropertyNode.java"

# interfaces
.implements Ljava/lang/Cloneable;
.implements Ljava/lang/Comparable;


# instance fields
.field protected _buf:Ljava/lang/Object;

.field private _cpEnd:I

.field private _cpStart:I


# direct methods
.method protected constructor <init>(IILjava/lang/Object;)V
    .locals 3
    .param p1, "fcStart"    # I
    .param p2, "fcEnd"    # I
    .param p3, "buf"    # Ljava/lang/Object;

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    iput p1, p0, Lorg/apache/index/poi/hwpf/model/PropertyNode;->_cpStart:I

    .line 48
    iput p2, p0, Lorg/apache/index/poi/hwpf/model/PropertyNode;->_cpEnd:I

    .line 49
    iput-object p3, p0, Lorg/apache/index/poi/hwpf/model/PropertyNode;->_buf:Ljava/lang/Object;

    .line 51
    iget v0, p0, Lorg/apache/index/poi/hwpf/model/PropertyNode;->_cpStart:I

    if-gez v0, :cond_0

    .line 52
    sget-object v0, Ljava/lang/System;->err:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "A property claimed to start before zero, at "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lorg/apache/index/poi/hwpf/model/PropertyNode;->_cpStart:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "! Resetting it to zero, and hoping for the best"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 53
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/index/poi/hwpf/model/PropertyNode;->_cpStart:I

    .line 55
    :cond_0
    return-void
.end method


# virtual methods
.method public adjustForDelete(II)V
    .locals 2
    .param p1, "start"    # I
    .param p2, "length"    # I

    .prologue
    .line 90
    add-int v0, p1, p2

    .line 92
    .local v0, "end":I
    iget v1, p0, Lorg/apache/index/poi/hwpf/model/PropertyNode;->_cpEnd:I

    if-le v1, p1, :cond_0

    .line 95
    iget v1, p0, Lorg/apache/index/poi/hwpf/model/PropertyNode;->_cpStart:I

    if-ge v1, v0, :cond_2

    .line 97
    iget v1, p0, Lorg/apache/index/poi/hwpf/model/PropertyNode;->_cpEnd:I

    if-lt v0, v1, :cond_1

    move v1, p1

    :goto_0
    iput v1, p0, Lorg/apache/index/poi/hwpf/model/PropertyNode;->_cpEnd:I

    .line 98
    iget v1, p0, Lorg/apache/index/poi/hwpf/model/PropertyNode;->_cpStart:I

    invoke-static {p1, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    iput v1, p0, Lorg/apache/index/poi/hwpf/model/PropertyNode;->_cpStart:I

    .line 105
    :cond_0
    :goto_1
    return-void

    .line 97
    :cond_1
    iget v1, p0, Lorg/apache/index/poi/hwpf/model/PropertyNode;->_cpEnd:I

    sub-int/2addr v1, p2

    goto :goto_0

    .line 101
    :cond_2
    iget v1, p0, Lorg/apache/index/poi/hwpf/model/PropertyNode;->_cpEnd:I

    sub-int/2addr v1, p2

    iput v1, p0, Lorg/apache/index/poi/hwpf/model/PropertyNode;->_cpEnd:I

    .line 102
    iget v1, p0, Lorg/apache/index/poi/hwpf/model/PropertyNode;->_cpStart:I

    sub-int/2addr v1, p2

    iput v1, p0, Lorg/apache/index/poi/hwpf/model/PropertyNode;->_cpStart:I

    goto :goto_1
.end method

.method public clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 139
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public compareTo(Ljava/lang/Object;)I
    .locals 2
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 147
    check-cast p1, Lorg/apache/index/poi/hwpf/model/PropertyNode;

    .end local p1    # "o":Ljava/lang/Object;
    invoke-virtual {p1}, Lorg/apache/index/poi/hwpf/model/PropertyNode;->getEnd()I

    move-result v0

    .line 148
    .local v0, "cpEnd":I
    iget v1, p0, Lorg/apache/index/poi/hwpf/model/PropertyNode;->_cpEnd:I

    if-ne v1, v0, :cond_0

    .line 150
    const/4 v1, 0x0

    .line 158
    :goto_0
    return v1

    .line 152
    :cond_0
    iget v1, p0, Lorg/apache/index/poi/hwpf/model/PropertyNode;->_cpEnd:I

    if-ge v1, v0, :cond_1

    .line 154
    const/4 v1, -0x1

    goto :goto_0

    .line 158
    :cond_1
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 116
    if-nez p1, :cond_1

    .line 132
    .end local p1    # "o":Ljava/lang/Object;
    :cond_0
    :goto_0
    return v1

    .line 121
    .restart local p1    # "o":Ljava/lang/Object;
    :cond_1
    instance-of v2, p1, Lorg/apache/index/poi/hwpf/model/PropertyNode;

    if-eqz v2, :cond_0

    .line 122
    invoke-virtual {p0, p1}, Lorg/apache/index/poi/hwpf/model/PropertyNode;->limitsAreEqual(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 124
    check-cast p1, Lorg/apache/index/poi/hwpf/model/PropertyNode;

    .end local p1    # "o":Ljava/lang/Object;
    iget-object v0, p1, Lorg/apache/index/poi/hwpf/model/PropertyNode;->_buf:Ljava/lang/Object;

    .line 125
    .local v0, "testBuf":Ljava/lang/Object;
    instance-of v1, v0, [B

    if-eqz v1, :cond_2

    iget-object v1, p0, Lorg/apache/index/poi/hwpf/model/PropertyNode;->_buf:Ljava/lang/Object;

    instance-of v1, v1, [B

    if-eqz v1, :cond_2

    .line 127
    check-cast v0, [B

    .end local v0    # "testBuf":Ljava/lang/Object;
    iget-object v1, p0, Lorg/apache/index/poi/hwpf/model/PropertyNode;->_buf:Ljava/lang/Object;

    check-cast v1, [B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    goto :goto_0

    .line 129
    .restart local v0    # "testBuf":Ljava/lang/Object;
    :cond_2
    iget-object v1, p0, Lorg/apache/index/poi/hwpf/model/PropertyNode;->_buf:Ljava/lang/Object;

    invoke-virtual {v1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0
.end method

.method public getEnd()I
    .locals 1

    .prologue
    .line 75
    iget v0, p0, Lorg/apache/index/poi/hwpf/model/PropertyNode;->_cpEnd:I

    return v0
.end method

.method public getStart()I
    .locals 1

    .prologue
    .line 62
    iget v0, p0, Lorg/apache/index/poi/hwpf/model/PropertyNode;->_cpStart:I

    return v0
.end method

.method protected limitsAreEqual(Ljava/lang/Object;)Z
    .locals 2
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 109
    move-object v0, p1

    check-cast v0, Lorg/apache/index/poi/hwpf/model/PropertyNode;

    invoke-virtual {v0}, Lorg/apache/index/poi/hwpf/model/PropertyNode;->getStart()I

    move-result v0

    iget v1, p0, Lorg/apache/index/poi/hwpf/model/PropertyNode;->_cpStart:I

    if-ne v0, v1, :cond_0

    .line 110
    check-cast p1, Lorg/apache/index/poi/hwpf/model/PropertyNode;

    .end local p1    # "o":Ljava/lang/Object;
    invoke-virtual {p1}, Lorg/apache/index/poi/hwpf/model/PropertyNode;->getEnd()I

    move-result v0

    iget v1, p0, Lorg/apache/index/poi/hwpf/model/PropertyNode;->_cpEnd:I

    if-ne v0, v1, :cond_0

    .line 109
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setEnd(I)V
    .locals 0
    .param p1, "end"    # I

    .prologue
    .line 80
    iput p1, p0, Lorg/apache/index/poi/hwpf/model/PropertyNode;->_cpEnd:I

    .line 81
    return-void
.end method

.method public setStart(I)V
    .locals 0
    .param p1, "start"    # I

    .prologue
    .line 67
    iput p1, p0, Lorg/apache/index/poi/hwpf/model/PropertyNode;->_cpStart:I

    .line 68
    return-void
.end method
