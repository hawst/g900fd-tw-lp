.class public final Lorg/apache/index/poi/hslf/record/TextBytesAtom;
.super Lorg/apache/index/poi/hslf/record/RecordAtom;
.source "TextBytesAtom.java"


# static fields
.field private static _type:J


# instance fields
.field private _NormalText:Ljava/lang/String;

.field private _header:[B

.field private _text:[B


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 39
    const-wide/16 v0, 0xfa8

    sput-wide v0, Lorg/apache/index/poi/hslf/record/TextBytesAtom;->_type:J

    return-void
.end method

.method public constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 94
    invoke-direct {p0}, Lorg/apache/index/poi/hslf/record/RecordAtom;-><init>()V

    .line 95
    const/16 v0, 0x8

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/apache/index/poi/hslf/record/TextBytesAtom;->_header:[B

    .line 96
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/TextBytesAtom;->_header:[B

    invoke-static {v0, v4, v4}, Lorg/apache/index/poi/util/LittleEndian;->putUShort([BII)V

    .line 97
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/TextBytesAtom;->_header:[B

    const/4 v1, 0x2

    sget-wide v2, Lorg/apache/index/poi/hslf/record/TextBytesAtom;->_type:J

    long-to-int v2, v2

    invoke-static {v0, v1, v2}, Lorg/apache/index/poi/util/LittleEndian;->putUShort([BII)V

    .line 98
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/TextBytesAtom;->_header:[B

    const/4 v1, 0x4

    invoke-static {v0, v1, v4}, Lorg/apache/index/poi/util/LittleEndian;->putInt([BII)V

    .line 100
    new-array v0, v4, [B

    iput-object v0, p0, Lorg/apache/index/poi/hslf/record/TextBytesAtom;->_text:[B

    .line 101
    return-void
.end method

.method protected constructor <init>([BII)V
    .locals 5
    .param p1, "source"    # [B
    .param p2, "start"    # I
    .param p3, "len"    # I

    .prologue
    const/4 v4, 0x0

    const/16 v2, 0x8

    .line 73
    invoke-direct {p0}, Lorg/apache/index/poi/hslf/record/RecordAtom;-><init>()V

    .line 75
    if-ge p3, v2, :cond_0

    const/16 p3, 0x8

    .line 78
    :cond_0
    new-array v1, v2, [B

    iput-object v1, p0, Lorg/apache/index/poi/hslf/record/TextBytesAtom;->_header:[B

    .line 79
    iget-object v1, p0, Lorg/apache/index/poi/hslf/record/TextBytesAtom;->_header:[B

    invoke-static {p1, p2, v1, v4, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 82
    add-int/lit8 v1, p3, -0x8

    new-array v1, v1, [B

    iput-object v1, p0, Lorg/apache/index/poi/hslf/record/TextBytesAtom;->_text:[B

    .line 83
    add-int/lit8 v1, p2, 0x8

    iget-object v2, p0, Lorg/apache/index/poi/hslf/record/TextBytesAtom;->_text:[B

    add-int/lit8 v3, p3, -0x8

    invoke-static {p1, v1, v2, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 85
    invoke-virtual {p0}, Lorg/apache/index/poi/hslf/record/TextBytesAtom;->getText()Ljava/lang/String;

    move-result-object v0

    .line 86
    .local v0, "localText":Ljava/lang/String;
    invoke-static {v0}, Lorg/apache/index/poi/hslf/record/Record;->setTextToUser(Ljava/lang/String;)V

    .line 87
    iput-object v0, p0, Lorg/apache/index/poi/hslf/record/TextBytesAtom;->_NormalText:Ljava/lang/String;

    .line 88
    const/4 v1, 0x0

    iput-object v1, p0, Lorg/apache/index/poi/hslf/record/TextBytesAtom;->_text:[B

    .line 89
    return-void
.end method


# virtual methods
.method public getNormalText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/TextBytesAtom;->_NormalText:Ljava/lang/String;

    return-object v0
.end method

.method public getRecordType()J
    .locals 2

    .prologue
    .line 106
    sget-wide v0, Lorg/apache/index/poi/hslf/record/TextBytesAtom;->_type:J

    return-wide v0
.end method

.method public getText()Ljava/lang/String;
    .locals 3

    .prologue
    .line 47
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/TextBytesAtom;->_text:[B

    const/4 v1, 0x0

    iget-object v2, p0, Lorg/apache/index/poi/hslf/record/TextBytesAtom;->_text:[B

    array-length v2, v2

    invoke-static {v0, v1, v2}, Lorg/apache/index/poi/util/StringUtil;->getFromCompressedUnicode([BII)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public setNormalTextNull()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 55
    iput-object v0, p0, Lorg/apache/index/poi/hslf/record/TextBytesAtom;->_NormalText:Ljava/lang/String;

    .line 56
    iput-object v0, p0, Lorg/apache/index/poi/hslf/record/TextBytesAtom;->_text:[B

    .line 57
    return-void
.end method

.method public setText([B)V
    .locals 3
    .param p1, "b"    # [B

    .prologue
    .line 62
    iput-object p1, p0, Lorg/apache/index/poi/hslf/record/TextBytesAtom;->_text:[B

    .line 65
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/TextBytesAtom;->_header:[B

    const/4 v1, 0x4

    iget-object v2, p0, Lorg/apache/index/poi/hslf/record/TextBytesAtom;->_text:[B

    array-length v2, v2

    invoke-static {v0, v1, v2}, Lorg/apache/index/poi/util/LittleEndian;->putInt([BII)V

    .line 66
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 125
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 126
    .local v0, "out":Ljava/lang/StringBuffer;
    const-string/jumbo v1, "TextBytesAtom:\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 127
    iget-object v1, p0, Lorg/apache/index/poi/hslf/record/TextBytesAtom;->_text:[B

    const-wide/16 v2, 0x0

    const/4 v4, 0x0

    invoke-static {v1, v2, v3, v4}, Lorg/apache/index/poi/util/HexDump;->dump([BJI)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 128
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public writeOut(Ljava/io/OutputStream;)V
    .locals 1
    .param p1, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 114
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/TextBytesAtom;->_header:[B

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 117
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/TextBytesAtom;->_text:[B

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 118
    return-void
.end method
