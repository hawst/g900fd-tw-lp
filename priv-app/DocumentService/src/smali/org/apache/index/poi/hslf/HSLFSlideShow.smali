.class public final Lorg/apache/index/poi/hslf/HSLFSlideShow;
.super Lorg/apache/index/poi/POIDocument;
.source "HSLFSlideShow.java"


# static fields
.field private static final MAX_CHAR_COUNT:I = 0x100000


# instance fields
.field private _docstreamLength:I

.field protected _mainStreamPPTDoc:Lorg/apache/index/poi/poifs/filesystem/DocumentInputStream;

.field private _records:[Lorg/apache/index/poi/hslf/record/Record;

.field private currentUser:Lorg/apache/index/poi/hslf/record/CurrentUserAtom;

.field private indexWriterhslfL:Lcom/samsung/index/ITextContentObs;

.field private l_strBuilder:Ljava/lang/StringBuilder;

.field private logger:Lorg/apache/index/poi/util/POILogger;


# direct methods
.method public constructor <init>(Lcom/samsung/index/ITextContentObs;Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;)V
    .locals 3
    .param p1, "inwriter"    # Lcom/samsung/index/ITextContentObs;
    .param p2, "dir"    # Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 191
    invoke-direct {p0, p2}, Lorg/apache/index/poi/POIDocument;-><init>(Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;)V

    .line 64
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-static {v1}, Lorg/apache/index/poi/util/POILogFactory;->getLogger(Ljava/lang/Class;)Lorg/apache/index/poi/util/POILogger;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/index/poi/hslf/HSLFSlideShow;->logger:Lorg/apache/index/poi/util/POILogger;

    .line 72
    const/4 v1, 0x0

    iput-object v1, p0, Lorg/apache/index/poi/hslf/HSLFSlideShow;->_mainStreamPPTDoc:Lorg/apache/index/poi/poifs/filesystem/DocumentInputStream;

    .line 194
    iput-object p1, p0, Lorg/apache/index/poi/hslf/HSLFSlideShow;->indexWriterhslfL:Lcom/samsung/index/ITextContentObs;

    .line 196
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v1, p0, Lorg/apache/index/poi/hslf/HSLFSlideShow;->l_strBuilder:Ljava/lang/StringBuilder;

    .line 200
    invoke-direct {p0}, Lorg/apache/index/poi/hslf/HSLFSlideShow;->readCurrentUserStream()V

    .line 204
    invoke-direct {p0}, Lorg/apache/index/poi/hslf/HSLFSlideShow;->readPowerPointStream()V

    .line 208
    invoke-static {p0}, Lorg/apache/index/poi/hslf/EncryptedSlideShow;->checkIfEncrypted(Lorg/apache/index/poi/hslf/HSLFSlideShow;)Z

    move-result v0

    .line 209
    .local v0, "encrypted":Z
    if-eqz v0, :cond_0

    .line 210
    new-instance v1, Lorg/apache/index/poi/hslf/exceptions/EncryptedPowerPointFileException;

    const-string/jumbo v2, "Encrypted PowerPoint files are not supported"

    invoke-direct {v1, v2}, Lorg/apache/index/poi/hslf/exceptions/EncryptedPowerPointFileException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 214
    :cond_0
    invoke-direct {p0}, Lorg/apache/index/poi/hslf/HSLFSlideShow;->buildRecords()V

    .line 216
    invoke-static {}, Lorg/apache/index/poi/hslf/HSLFSlideShow;->commitString()V

    .line 220
    invoke-direct {p0}, Lorg/apache/index/poi/hslf/HSLFSlideShow;->readOtherStreams()V

    .line 221
    return-void
.end method

.method public constructor <init>(Lcom/samsung/index/ITextContentObs;Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;)V
    .locals 1
    .param p1, "inwriter"    # Lcom/samsung/index/ITextContentObs;
    .param p2, "filesystem"    # Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 137
    invoke-virtual {p2}, Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;->getRoot()Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lorg/apache/index/poi/hslf/HSLFSlideShow;-><init>(Lcom/samsung/index/ITextContentObs;Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;)V

    .line 138
    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 2
    .param p1, "inputStream"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 124
    new-instance v0, Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;-><init>(Ljava/io/InputStream;Ljava/io/File;)V

    invoke-direct {p0, v0}, Lorg/apache/index/poi/hslf/HSLFSlideShow;-><init>(Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;)V

    .line 125
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "fileName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 112
    new-instance v0, Ljava/io/FileInputStream;

    invoke-direct {v0, p1}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lorg/apache/index/poi/hslf/HSLFSlideShow;-><init>(Ljava/io/InputStream;)V

    .line 113
    return-void
.end method

.method public constructor <init>(Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;)V
    .locals 1
    .param p1, "dir"    # Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 185
    const/4 v0, 0x0

    invoke-direct {p0, v0, p1}, Lorg/apache/index/poi/hslf/HSLFSlideShow;-><init>(Lcom/samsung/index/ITextContentObs;Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;)V

    .line 186
    return-void
.end method

.method public constructor <init>(Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;)V
    .locals 0
    .param p1, "dir"    # Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;
    .param p2, "filesystem"    # Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 171
    invoke-direct {p0, p1}, Lorg/apache/index/poi/hslf/HSLFSlideShow;-><init>(Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;)V

    .line 172
    return-void
.end method

.method public constructor <init>(Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;)V
    .locals 1
    .param p1, "filesystem"    # Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 143
    invoke-virtual {p1}, Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;->getRoot()Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/apache/index/poi/hslf/HSLFSlideShow;-><init>(Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;)V

    .line 144
    return-void
.end method

.method public static appendAndCommitString(Ljava/lang/String;)V
    .locals 0
    .param p0, "inString"    # Ljava/lang/String;

    .prologue
    .line 236
    return-void
.end method

.method private buildRecords()V
    .locals 4

    .prologue
    .line 360
    iget-object v0, p0, Lorg/apache/index/poi/hslf/HSLFSlideShow;->_mainStreamPPTDoc:Lorg/apache/index/poi/poifs/filesystem/DocumentInputStream;

    iget-object v1, p0, Lorg/apache/index/poi/hslf/HSLFSlideShow;->currentUser:Lorg/apache/index/poi/hslf/record/CurrentUserAtom;

    invoke-virtual {v1}, Lorg/apache/index/poi/hslf/record/CurrentUserAtom;->getCurrentEditOffset()J

    move-result-wide v2

    long-to-int v1, v2

    invoke-direct {p0, v0, v1}, Lorg/apache/index/poi/hslf/HSLFSlideShow;->read(Lorg/apache/index/poi/poifs/filesystem/DocumentInputStream;I)[Lorg/apache/index/poi/hslf/record/Record;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/index/poi/hslf/HSLFSlideShow;->_records:[Lorg/apache/index/poi/hslf/record/Record;

    .line 361
    return-void
.end method

.method public static commitString()V
    .locals 0

    .prologue
    .line 246
    return-void
.end method

.method public static final create()Lorg/apache/index/poi/hslf/HSLFSlideShow;
    .locals 4

    .prologue
    .line 292
    const-class v2, Lorg/apache/index/poi/hslf/HSLFSlideShow;

    const-string/jumbo v3, "data/empty.ppt"

    invoke-virtual {v2, v3}, Ljava/lang/Class;->getResourceAsStream(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v1

    .line 293
    .local v1, "is":Ljava/io/InputStream;
    if-nez v1, :cond_0

    .line 294
    new-instance v2, Ljava/lang/RuntimeException;

    const-string/jumbo v3, "Missing resource \'empty.ppt\'"

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 297
    :cond_0
    :try_start_0
    new-instance v2, Lorg/apache/index/poi/hslf/HSLFSlideShow;

    invoke-direct {v2, v1}, Lorg/apache/index/poi/hslf/HSLFSlideShow;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v2

    .line 298
    :catch_0
    move-exception v0

    .line 299
    .local v0, "e":Ljava/io/IOException;
    new-instance v2, Ljava/lang/RuntimeException;

    invoke-direct {v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v2
.end method

.method private extractText(Lorg/apache/index/poi/hslf/record/Record;)V
    .locals 2
    .param p1, "textbox"    # Lorg/apache/index/poi/hslf/record/Record;

    .prologue
    .line 366
    const/4 v0, 0x0

    .line 367
    .local v0, "mine":Ljava/lang/String;
    instance-of v1, p1, Lorg/apache/index/poi/hslf/record/TextBytesAtom;

    if-eqz v1, :cond_2

    move-object v1, p1

    .line 369
    check-cast v1, Lorg/apache/index/poi/hslf/record/TextBytesAtom;

    invoke-virtual {v1}, Lorg/apache/index/poi/hslf/record/TextBytesAtom;->getNormalText()Ljava/lang/String;

    move-result-object v0

    .line 370
    check-cast p1, Lorg/apache/index/poi/hslf/record/TextBytesAtom;

    .end local p1    # "textbox":Lorg/apache/index/poi/hslf/record/Record;
    invoke-virtual {p1}, Lorg/apache/index/poi/hslf/record/TextBytesAtom;->setNormalTextNull()V

    .line 383
    :cond_0
    :goto_0
    if-eqz v0, :cond_1

    .line 385
    invoke-virtual {p0, v0}, Lorg/apache/index/poi/hslf/HSLFSlideShow;->appendAndCommitStringL(Ljava/lang/String;)V

    .line 387
    :cond_1
    return-void

    .line 372
    .restart local p1    # "textbox":Lorg/apache/index/poi/hslf/record/Record;
    :cond_2
    instance-of v1, p1, Lorg/apache/index/poi/hslf/record/TextCharsAtom;

    if-eqz v1, :cond_3

    move-object v1, p1

    .line 374
    check-cast v1, Lorg/apache/index/poi/hslf/record/TextCharsAtom;

    invoke-virtual {v1}, Lorg/apache/index/poi/hslf/record/TextCharsAtom;->getNormalText()Ljava/lang/String;

    move-result-object v0

    .line 375
    check-cast p1, Lorg/apache/index/poi/hslf/record/TextCharsAtom;

    .end local p1    # "textbox":Lorg/apache/index/poi/hslf/record/Record;
    invoke-virtual {p1}, Lorg/apache/index/poi/hslf/record/TextCharsAtom;->setNormalTextNull()V

    goto :goto_0

    .line 377
    .restart local p1    # "textbox":Lorg/apache/index/poi/hslf/record/Record;
    :cond_3
    instance-of v1, p1, Lorg/apache/index/poi/hslf/record/CString;

    if-eqz v1, :cond_0

    move-object v1, p1

    .line 379
    check-cast v1, Lorg/apache/index/poi/hslf/record/CString;

    invoke-virtual {v1}, Lorg/apache/index/poi/hslf/record/CString;->getNormalText()Ljava/lang/String;

    move-result-object v0

    .line 380
    check-cast p1, Lorg/apache/index/poi/hslf/record/CString;

    .end local p1    # "textbox":Lorg/apache/index/poi/hslf/record/Record;
    invoke-virtual {p1}, Lorg/apache/index/poi/hslf/record/CString;->setNormalTextNull()V

    goto :goto_0
.end method

.method private getTextFromDrawAndDummyRecord(Lorg/apache/index/poi/hslf/record/PPDrawing;[Lorg/apache/index/poi/hslf/record/Record;)V
    .locals 10
    .param p1, "ppDraw"    # Lorg/apache/index/poi/hslf/record/PPDrawing;
    .param p2, "_childrenSlide"    # [Lorg/apache/index/poi/hslf/record/Record;

    .prologue
    const/4 v5, 0x0

    .line 391
    if-eqz p1, :cond_0

    .line 393
    invoke-virtual {p1}, Lorg/apache/index/poi/hslf/record/PPDrawing;->getTextboxWrappers()[Lorg/apache/index/poi/hslf/record/EscherTextboxWrapper;

    move-result-object v4

    .line 395
    .local v4, "textboxWrappers":[Lorg/apache/index/poi/hslf/record/EscherTextboxWrapper;
    if-eqz v4, :cond_0

    .line 397
    array-length v8, v4

    move v7, v5

    :goto_0
    if-lt v7, v8, :cond_2

    .line 413
    .end local v4    # "textboxWrappers":[Lorg/apache/index/poi/hslf/record/EscherTextboxWrapper;
    :cond_0
    if-eqz p2, :cond_1

    .line 415
    array-length v6, p2

    :goto_1
    if-lt v5, v6, :cond_5

    .line 431
    :cond_1
    return-void

    .line 397
    .restart local v4    # "textboxWrappers":[Lorg/apache/index/poi/hslf/record/EscherTextboxWrapper;
    :cond_2
    aget-object v3, v4, v7

    .line 399
    .local v3, "textboxWrapper":Lorg/apache/index/poi/hslf/record/EscherTextboxWrapper;
    invoke-virtual {v3}, Lorg/apache/index/poi/hslf/record/EscherTextboxWrapper;->getChildRecords()[Lorg/apache/index/poi/hslf/record/Record;

    move-result-object v0

    .line 401
    .local v0, "_childrenText":[Lorg/apache/index/poi/hslf/record/Record;
    if-nez v0, :cond_4

    .line 397
    :cond_3
    add-int/lit8 v6, v7, 0x1

    move v7, v6

    goto :goto_0

    .line 404
    :cond_4
    array-length v9, v0

    move v6, v5

    :goto_2
    if-ge v6, v9, :cond_3

    aget-object v2, v0, v6

    .line 406
    .local v2, "textbox":Lorg/apache/index/poi/hslf/record/Record;
    invoke-direct {p0, v2}, Lorg/apache/index/poi/hslf/HSLFSlideShow;->extractText(Lorg/apache/index/poi/hslf/record/Record;)V

    .line 404
    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    .line 415
    .end local v0    # "_childrenText":[Lorg/apache/index/poi/hslf/record/Record;
    .end local v2    # "textbox":Lorg/apache/index/poi/hslf/record/Record;
    .end local v3    # "textboxWrapper":Lorg/apache/index/poi/hslf/record/EscherTextboxWrapper;
    .end local v4    # "textboxWrappers":[Lorg/apache/index/poi/hslf/record/EscherTextboxWrapper;
    :cond_5
    aget-object v1, p2, v5

    .line 417
    .local v1, "slideChild":Lorg/apache/index/poi/hslf/record/Record;
    instance-of v7, v1, Lorg/apache/index/poi/hslf/record/DummyPositionSensitiveRecordWithChildren;

    if-eqz v7, :cond_6

    .line 420
    invoke-virtual {v1}, Lorg/apache/index/poi/hslf/record/Record;->getChildRecords()[Lorg/apache/index/poi/hslf/record/Record;

    move-result-object v0

    .line 422
    .restart local v0    # "_childrenText":[Lorg/apache/index/poi/hslf/record/Record;
    if-nez v0, :cond_7

    .line 415
    .end local v0    # "_childrenText":[Lorg/apache/index/poi/hslf/record/Record;
    :cond_6
    :goto_3
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 426
    .restart local v0    # "_childrenText":[Lorg/apache/index/poi/hslf/record/Record;
    :cond_7
    invoke-direct {p0, v0}, Lorg/apache/index/poi/hslf/HSLFSlideShow;->getTextFromDummyPositionSensitiveRecordWithChildren([Lorg/apache/index/poi/hslf/record/Record;)V

    goto :goto_3
.end method

.method private getTextFromDummyPositionSensitiveRecordWithChildren([Lorg/apache/index/poi/hslf/record/Record;)V
    .locals 5
    .param p1, "_DummyChildren"    # [Lorg/apache/index/poi/hslf/record/Record;

    .prologue
    .line 436
    array-length v3, p1

    const/4 v2, 0x0

    :goto_0
    if-lt v2, v3, :cond_0

    .line 453
    return-void

    .line 436
    :cond_0
    aget-object v0, p1, v2

    .line 438
    .local v0, "_DummyChild":Lorg/apache/index/poi/hslf/record/Record;
    instance-of v4, v0, Lorg/apache/index/poi/hslf/record/DummyPositionSensitiveRecordWithChildren;

    if-eqz v4, :cond_2

    .line 442
    invoke-virtual {v0}, Lorg/apache/index/poi/hslf/record/Record;->getChildRecords()[Lorg/apache/index/poi/hslf/record/Record;

    move-result-object v1

    .line 443
    .local v1, "_DummyChildsChildren":[Lorg/apache/index/poi/hslf/record/Record;
    if-eqz v1, :cond_1

    .line 445
    invoke-direct {p0, v1}, Lorg/apache/index/poi/hslf/HSLFSlideShow;->getTextFromDummyPositionSensitiveRecordWithChildren([Lorg/apache/index/poi/hslf/record/Record;)V

    .line 436
    .end local v1    # "_DummyChildsChildren":[Lorg/apache/index/poi/hslf/record/Record;
    :cond_1
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 450
    :cond_2
    invoke-direct {p0, v0}, Lorg/apache/index/poi/hslf/HSLFSlideShow;->extractText(Lorg/apache/index/poi/hslf/record/Record;)V

    goto :goto_1
.end method

.method private read(Lorg/apache/index/poi/poifs/filesystem/DocumentInputStream;I)[Lorg/apache/index/poi/hslf/record/Record;
    .locals 24
    .param p1, "docstream"    # Lorg/apache/index/poi/poifs/filesystem/DocumentInputStream;
    .param p2, "usrOffset"    # I

    .prologue
    .line 457
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 458
    .local v9, "lst":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    new-instance v11, Ljava/util/HashMap;

    invoke-direct {v11}, Ljava/util/HashMap;-><init>()V

    .line 459
    .local v11, "offset2id":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    :goto_0
    if-nez p2, :cond_1

    .line 488
    :cond_0
    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v22

    move/from16 v0, v22

    new-array v0, v0, [Ljava/lang/Integer;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Ljava/lang/Integer;

    .line 489
    .local v4, "a":[Ljava/lang/Integer;
    invoke-static {v4}, Ljava/util/Arrays;->sort([Ljava/lang/Object;)V

    .line 492
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_1
    array-length v0, v4

    move/from16 v22, v0

    move/from16 v0, v22

    if-lt v6, v0, :cond_4

    .line 543
    const/16 v22, 0x0

    return-object v22

    .line 460
    .end local v4    # "a":[Ljava/lang/Integer;
    .end local v6    # "i":I
    :cond_1
    invoke-static/range {p1 .. p2}, Lorg/apache/index/poi/hslf/record/Record;->buildRecordAtOffset(Lorg/apache/index/poi/poifs/filesystem/DocumentInputStream;I)Lorg/apache/index/poi/hslf/record/Record;

    move-result-object v20

    check-cast v20, Lorg/apache/index/poi/hslf/record/UserEditAtom;

    .line 462
    .local v20, "usr":Lorg/apache/index/poi/hslf/record/UserEditAtom;
    if-eqz v20, :cond_0

    .line 464
    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 465
    invoke-virtual/range {v20 .. v20}, Lorg/apache/index/poi/hslf/record/UserEditAtom;->getPersistPointersOffset()I

    move-result v14

    .line 468
    .local v14, "psrOffset":I
    move-object/from16 v0, p1

    invoke-static {v0, v14}, Lorg/apache/index/poi/hslf/record/Record;->buildRecordAtOffset(Lorg/apache/index/poi/poifs/filesystem/DocumentInputStream;I)Lorg/apache/index/poi/hslf/record/Record;

    move-result-object v15

    .line 467
    check-cast v15, Lorg/apache/index/poi/hslf/record/PersistPtrHolder;

    .line 469
    .local v15, "ptr":Lorg/apache/index/poi/hslf/record/PersistPtrHolder;
    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 471
    if-eqz v15, :cond_2

    .line 474
    invoke-virtual {v15}, Lorg/apache/index/poi/hslf/record/PersistPtrHolder;->getSlideLocationsLookup()Ljava/util/Hashtable;

    move-result-object v5

    .line 476
    .local v5, "entries":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    invoke-virtual {v5}, Ljava/util/Hashtable;->entrySet()Ljava/util/Set;

    move-result-object v22

    invoke-interface/range {v22 .. v22}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v23

    :goto_2
    invoke-interface/range {v23 .. v23}, Ljava/util/Iterator;->hasNext()Z

    move-result v22

    if-nez v22, :cond_3

    .line 483
    .end local v5    # "entries":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    :cond_2
    invoke-virtual/range {v20 .. v20}, Lorg/apache/index/poi/hslf/record/UserEditAtom;->getLastUserEditAtomOffset()I

    move-result p2

    goto :goto_0

    .line 476
    .restart local v5    # "entries":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    :cond_3
    invoke-interface/range {v23 .. v23}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/util/Map$Entry;

    .line 477
    .local v8, "id":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    invoke-interface {v8}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Integer;

    .line 478
    .local v10, "offset":Ljava/lang/Integer;
    invoke-virtual {v9, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 479
    invoke-interface {v8}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Ljava/lang/Integer;

    move-object/from16 v0, v22

    invoke-virtual {v11, v10, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 493
    .end local v5    # "entries":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    .end local v8    # "id":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    .end local v10    # "offset":Ljava/lang/Integer;
    .end local v14    # "psrOffset":I
    .end local v15    # "ptr":Lorg/apache/index/poi/hslf/record/PersistPtrHolder;
    .end local v20    # "usr":Lorg/apache/index/poi/hslf/record/UserEditAtom;
    .restart local v4    # "a":[Ljava/lang/Integer;
    .restart local v6    # "i":I
    :cond_4
    aget-object v10, v4, v6

    .line 495
    .restart local v10    # "offset":Ljava/lang/Integer;
    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v22

    .line 494
    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-static {v0, v1}, Lorg/apache/index/poi/hslf/record/Record;->buildRecordAtOffset(Lorg/apache/index/poi/poifs/filesystem/DocumentInputStream;I)Lorg/apache/index/poi/hslf/record/Record;

    move-result-object v16

    .line 497
    .local v16, "recSingle":Lorg/apache/index/poi/hslf/record/Record;
    move-object/from16 v0, v16

    instance-of v0, v0, Lorg/apache/index/poi/hslf/record/Slide;

    move/from16 v22, v0

    if-eqz v22, :cond_7

    move-object/from16 v17, v16

    .line 498
    check-cast v17, Lorg/apache/index/poi/hslf/record/Slide;

    .line 500
    .local v17, "slide":Lorg/apache/index/poi/hslf/record/Slide;
    invoke-virtual/range {v17 .. v17}, Lorg/apache/index/poi/hslf/record/Slide;->getPPDrawing()Lorg/apache/index/poi/hslf/record/PPDrawing;

    move-result-object v12

    .line 501
    .local v12, "ppDraw":Lorg/apache/index/poi/hslf/record/PPDrawing;
    invoke-virtual/range {v17 .. v17}, Lorg/apache/index/poi/hslf/record/Slide;->getChildRecords()[Lorg/apache/index/poi/hslf/record/Record;

    move-result-object v2

    .line 502
    .local v2, "_childrenSlide":[Lorg/apache/index/poi/hslf/record/Record;
    move-object/from16 v0, p0

    invoke-direct {v0, v12, v2}, Lorg/apache/index/poi/hslf/HSLFSlideShow;->getTextFromDrawAndDummyRecord(Lorg/apache/index/poi/hslf/record/PPDrawing;[Lorg/apache/index/poi/hslf/record/Record;)V

    .line 536
    .end local v2    # "_childrenSlide":[Lorg/apache/index/poi/hslf/record/Record;
    .end local v12    # "ppDraw":Lorg/apache/index/poi/hslf/record/PPDrawing;
    .end local v17    # "slide":Lorg/apache/index/poi/hslf/record/Slide;
    :cond_5
    :goto_3
    move-object/from16 v0, v16

    instance-of v0, v0, Lorg/apache/index/poi/hslf/record/PersistRecord;

    move/from16 v22, v0

    if-eqz v22, :cond_6

    move-object/from16 v13, v16

    .line 537
    check-cast v13, Lorg/apache/index/poi/hslf/record/PersistRecord;

    .line 538
    .local v13, "psr":Lorg/apache/index/poi/hslf/record/PersistRecord;
    invoke-virtual {v11, v10}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    .line 539
    .local v7, "id":Ljava/lang/Integer;
    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v22

    move/from16 v0, v22

    invoke-interface {v13, v0}, Lorg/apache/index/poi/hslf/record/PersistRecord;->setPersistId(I)V

    .line 492
    .end local v7    # "id":Ljava/lang/Integer;
    .end local v13    # "psr":Lorg/apache/index/poi/hslf/record/PersistRecord;
    :cond_6
    add-int/lit8 v6, v6, 0x1

    goto/16 :goto_1

    .line 503
    :cond_7
    move-object/from16 v0, v16

    instance-of v0, v0, Lorg/apache/index/poi/hslf/record/Notes;

    move/from16 v22, v0

    if-eqz v22, :cond_8

    move-object/from16 v17, v16

    .line 504
    check-cast v17, Lorg/apache/index/poi/hslf/record/Notes;

    .line 506
    .local v17, "slide":Lorg/apache/index/poi/hslf/record/Notes;
    invoke-virtual/range {v17 .. v17}, Lorg/apache/index/poi/hslf/record/Notes;->getPPDrawing()Lorg/apache/index/poi/hslf/record/PPDrawing;

    move-result-object v12

    .line 507
    .restart local v12    # "ppDraw":Lorg/apache/index/poi/hslf/record/PPDrawing;
    invoke-virtual/range {v17 .. v17}, Lorg/apache/index/poi/hslf/record/Notes;->getChildRecords()[Lorg/apache/index/poi/hslf/record/Record;

    move-result-object v2

    .line 508
    .restart local v2    # "_childrenSlide":[Lorg/apache/index/poi/hslf/record/Record;
    move-object/from16 v0, p0

    invoke-direct {v0, v12, v2}, Lorg/apache/index/poi/hslf/HSLFSlideShow;->getTextFromDrawAndDummyRecord(Lorg/apache/index/poi/hslf/record/PPDrawing;[Lorg/apache/index/poi/hslf/record/Record;)V

    goto :goto_3

    .line 518
    .end local v2    # "_childrenSlide":[Lorg/apache/index/poi/hslf/record/Record;
    .end local v12    # "ppDraw":Lorg/apache/index/poi/hslf/record/PPDrawing;
    .end local v17    # "slide":Lorg/apache/index/poi/hslf/record/Notes;
    :cond_8
    move-object/from16 v0, v16

    instance-of v0, v0, Lorg/apache/index/poi/hslf/record/Document;

    move/from16 v22, v0

    if-eqz v22, :cond_5

    move-object/from16 v17, v16

    .line 519
    check-cast v17, Lorg/apache/index/poi/hslf/record/Document;

    .line 521
    .local v17, "slide":Lorg/apache/index/poi/hslf/record/Document;
    invoke-virtual/range {v17 .. v17}, Lorg/apache/index/poi/hslf/record/Document;->getSlideListWithTexts()[Lorg/apache/index/poi/hslf/record/SlideListWithText;

    move-result-object v18

    .line 523
    .local v18, "slwts":[Lorg/apache/index/poi/hslf/record/SlideListWithText;
    if-eqz v18, :cond_5

    .line 524
    const/16 v21, 0x0

    .local v21, "x":I
    :goto_4
    move-object/from16 v0, v18

    array-length v0, v0

    move/from16 v22, v0

    move/from16 v0, v21

    move/from16 v1, v22

    if-ge v0, v1, :cond_5

    .line 525
    aget-object v22, v18, v21

    invoke-virtual/range {v22 .. v22}, Lorg/apache/index/poi/hslf/record/SlideListWithText;->getChildRecords()[Lorg/apache/index/poi/hslf/record/Record;

    move-result-object v3

    .line 526
    .local v3, "_childrenText":[Lorg/apache/index/poi/hslf/record/Record;
    if-nez v3, :cond_a

    .line 524
    :cond_9
    add-int/lit8 v21, v21, 0x1

    goto :goto_4

    .line 529
    :cond_a
    array-length v0, v3

    move/from16 v23, v0

    const/16 v22, 0x0

    :goto_5
    move/from16 v0, v22

    move/from16 v1, v23

    if-ge v0, v1, :cond_9

    aget-object v19, v3, v22

    .line 530
    .local v19, "textbox":Lorg/apache/index/poi/hslf/record/Record;
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v0, v1}, Lorg/apache/index/poi/hslf/HSLFSlideShow;->extractText(Lorg/apache/index/poi/hslf/record/Record;)V

    .line 529
    add-int/lit8 v22, v22, 0x1

    goto :goto_5
.end method

.method private readCurrentUserStream()V
    .locals 5

    .prologue
    .line 557
    :try_start_0
    new-instance v1, Lorg/apache/index/poi/hslf/record/CurrentUserAtom;

    iget-object v2, p0, Lorg/apache/index/poi/hslf/HSLFSlideShow;->directory:Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;

    invoke-direct {v1, v2}, Lorg/apache/index/poi/hslf/record/CurrentUserAtom;-><init>(Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;)V

    iput-object v1, p0, Lorg/apache/index/poi/hslf/HSLFSlideShow;->currentUser:Lorg/apache/index/poi/hslf/record/CurrentUserAtom;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 562
    :goto_0
    return-void

    .line 558
    :catch_0
    move-exception v0

    .line 559
    .local v0, "ie":Ljava/io/IOException;
    iget-object v1, p0, Lorg/apache/index/poi/hslf/HSLFSlideShow;->logger:Lorg/apache/index/poi/util/POILogger;

    sget v2, Lorg/apache/index/poi/util/POILogger;->ERROR:I

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "Error finding Current User Atom:\n"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lorg/apache/index/poi/util/POILogger;->log(ILjava/lang/Object;)V

    .line 560
    new-instance v1, Lorg/apache/index/poi/hslf/record/CurrentUserAtom;

    invoke-direct {v1}, Lorg/apache/index/poi/hslf/record/CurrentUserAtom;-><init>()V

    iput-object v1, p0, Lorg/apache/index/poi/hslf/HSLFSlideShow;->currentUser:Lorg/apache/index/poi/hslf/record/CurrentUserAtom;

    goto :goto_0
.end method

.method private readOtherStreams()V
    .locals 0

    .prologue
    .line 569
    return-void
.end method

.method private readPowerPointStream()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 313
    iget-object v1, p0, Lorg/apache/index/poi/hslf/HSLFSlideShow;->directory:Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;

    const-string/jumbo v2, "PowerPoint Document"

    invoke-virtual {v1, v2}, Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;->getEntry(Ljava/lang/String;)Lorg/apache/index/poi/poifs/filesystem/Entry;

    move-result-object v0

    check-cast v0, Lorg/apache/index/poi/poifs/filesystem/DocumentEntry;

    .line 316
    .local v0, "docProps":Lorg/apache/index/poi/poifs/filesystem/DocumentEntry;
    invoke-interface {v0}, Lorg/apache/index/poi/poifs/filesystem/DocumentEntry;->getSize()I

    move-result v1

    iput v1, p0, Lorg/apache/index/poi/hslf/HSLFSlideShow;->_docstreamLength:I

    .line 317
    iget-object v1, p0, Lorg/apache/index/poi/hslf/HSLFSlideShow;->directory:Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;

    const-string/jumbo v2, "PowerPoint Document"

    invoke-virtual {v1, v2}, Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;->createDocumentInputStream(Ljava/lang/String;)Lorg/apache/index/poi/poifs/filesystem/DocumentInputStream;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/index/poi/hslf/HSLFSlideShow;->_mainStreamPPTDoc:Lorg/apache/index/poi/poifs/filesystem/DocumentInputStream;

    .line 319
    return-void
.end method


# virtual methods
.method public appendAndCommitStringL(Ljava/lang/String;)V
    .locals 2
    .param p1, "inString"    # Ljava/lang/String;

    .prologue
    .line 251
    if-eqz p1, :cond_0

    iget-object v0, p0, Lorg/apache/index/poi/hslf/HSLFSlideShow;->l_strBuilder:Ljava/lang/StringBuilder;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/index/poi/hslf/HSLFSlideShow;->indexWriterhslfL:Lcom/samsung/index/ITextContentObs;

    if-eqz v0, :cond_0

    .line 253
    iget-object v0, p0, Lorg/apache/index/poi/hslf/HSLFSlideShow;->l_strBuilder:Ljava/lang/StringBuilder;

    const-string/jumbo v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 254
    iget-object v0, p0, Lorg/apache/index/poi/hslf/HSLFSlideShow;->l_strBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 256
    iget-object v0, p0, Lorg/apache/index/poi/hslf/HSLFSlideShow;->l_strBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    const/high16 v1, 0x100000

    if-lt v0, v1, :cond_0

    .line 257
    iget-object v0, p0, Lorg/apache/index/poi/hslf/HSLFSlideShow;->indexWriterhslfL:Lcom/samsung/index/ITextContentObs;

    iget-object v1, p0, Lorg/apache/index/poi/hslf/HSLFSlideShow;->l_strBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/samsung/index/ITextContentObs;->updateText(Ljava/lang/String;)V

    .line 258
    iget-object v0, p0, Lorg/apache/index/poi/hslf/HSLFSlideShow;->l_strBuilder:Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 261
    :cond_0
    return-void
.end method

.method public declared-synchronized appendRootLevelRecord(Lorg/apache/index/poi/hslf/record/Record;)I
    .locals 6
    .param p1, "newRecord"    # Lorg/apache/index/poi/hslf/record/Record;

    .prologue
    .line 578
    monitor-enter p0

    const/4 v1, -0x1

    .line 579
    .local v1, "addedAt":I
    :try_start_0
    iget-object v4, p0, Lorg/apache/index/poi/hslf/HSLFSlideShow;->_records:[Lorg/apache/index/poi/hslf/record/Record;

    array-length v4, v4

    add-int/lit8 v4, v4, 0x1

    new-array v3, v4, [Lorg/apache/index/poi/hslf/record/Record;

    .line 580
    .local v3, "r":[Lorg/apache/index/poi/hslf/record/Record;
    const/4 v0, 0x0

    .line 581
    .local v0, "added":Z
    iget-object v4, p0, Lorg/apache/index/poi/hslf/HSLFSlideShow;->_records:[Lorg/apache/index/poi/hslf/record/Record;

    array-length v4, v4

    add-int/lit8 v2, v4, -0x1

    .local v2, "i":I
    :goto_0
    if-gez v2, :cond_0

    .line 594
    iput-object v3, p0, Lorg/apache/index/poi/hslf/HSLFSlideShow;->_records:[Lorg/apache/index/poi/hslf/record/Record;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 595
    monitor-exit p0

    return v1

    .line 582
    :cond_0
    if-eqz v0, :cond_2

    .line 584
    :try_start_1
    iget-object v4, p0, Lorg/apache/index/poi/hslf/HSLFSlideShow;->_records:[Lorg/apache/index/poi/hslf/record/Record;

    aget-object v4, v4, v2

    aput-object v4, v3, v2

    .line 581
    :cond_1
    :goto_1
    add-int/lit8 v2, v2, -0x1

    goto :goto_0

    .line 586
    :cond_2
    add-int/lit8 v4, v2, 0x1

    iget-object v5, p0, Lorg/apache/index/poi/hslf/HSLFSlideShow;->_records:[Lorg/apache/index/poi/hslf/record/Record;

    aget-object v5, v5, v2

    aput-object v5, v3, v4

    .line 587
    iget-object v4, p0, Lorg/apache/index/poi/hslf/HSLFSlideShow;->_records:[Lorg/apache/index/poi/hslf/record/Record;

    aget-object v4, v4, v2

    instance-of v4, v4, Lorg/apache/index/poi/hslf/record/PersistPtrHolder;

    if-eqz v4, :cond_1

    .line 588
    aput-object p1, v3, v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 589
    const/4 v0, 0x1

    .line 590
    move v1, v2

    goto :goto_1

    .line 578
    .end local v0    # "added":Z
    .end local v2    # "i":I
    .end local v3    # "r":[Lorg/apache/index/poi/hslf/record/Record;
    :catchall_0
    move-exception v4

    monitor-exit p0

    throw v4
.end method

.method public commitStringL()V
    .locals 2

    .prologue
    .line 265
    iget-object v0, p0, Lorg/apache/index/poi/hslf/HSLFSlideShow;->l_strBuilder:Ljava/lang/StringBuilder;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/index/poi/hslf/HSLFSlideShow;->indexWriterhslfL:Lcom/samsung/index/ITextContentObs;

    if-eqz v0, :cond_0

    .line 267
    iget-object v0, p0, Lorg/apache/index/poi/hslf/HSLFSlideShow;->l_strBuilder:Ljava/lang/StringBuilder;

    const-string/jumbo v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 268
    iget-object v0, p0, Lorg/apache/index/poi/hslf/HSLFSlideShow;->indexWriterhslfL:Lcom/samsung/index/ITextContentObs;

    iget-object v1, p0, Lorg/apache/index/poi/hslf/HSLFSlideShow;->l_strBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/samsung/index/ITextContentObs;->updateText(Ljava/lang/String;)V

    .line 269
    iget-object v0, p0, Lorg/apache/index/poi/hslf/HSLFSlideShow;->l_strBuilder:Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 271
    :cond_0
    return-void
.end method

.method public getCurrentUserAtom()Lorg/apache/index/poi/hslf/record/CurrentUserAtom;
    .locals 1

    .prologue
    .line 622
    iget-object v0, p0, Lorg/apache/index/poi/hslf/HSLFSlideShow;->currentUser:Lorg/apache/index/poi/hslf/record/CurrentUserAtom;

    return-object v0
.end method

.method protected getPOIFSDirectory()Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lorg/apache/index/poi/hslf/HSLFSlideShow;->directory:Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;

    return-object v0
.end method

.method protected getPOIFSFileSystem()Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lorg/apache/index/poi/hslf/HSLFSlideShow;->directory:Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;

    invoke-virtual {v0}, Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;->getFileSystem()Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;

    move-result-object v0

    return-object v0
.end method

.method public declared-synchronized getRecords()[Lorg/apache/index/poi/hslf/record/Record;
    .locals 1

    .prologue
    .line 604
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/index/poi/hslf/HSLFSlideShow;->_records:[Lorg/apache/index/poi/hslf/record/Record;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getText()Ljava/lang/String;
    .locals 3

    .prologue
    .line 274
    const-string/jumbo v0, ""

    .line 275
    .local v0, "val":Ljava/lang/String;
    iget-object v1, p0, Lorg/apache/index/poi/hslf/HSLFSlideShow;->l_strBuilder:Ljava/lang/StringBuilder;

    if-eqz v1, :cond_0

    .line 276
    iget-object v1, p0, Lorg/apache/index/poi/hslf/HSLFSlideShow;->l_strBuilder:Ljava/lang/StringBuilder;

    const-string/jumbo v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 279
    iget-object v1, p0, Lorg/apache/index/poi/hslf/HSLFSlideShow;->l_strBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 280
    iget-object v1, p0, Lorg/apache/index/poi/hslf/HSLFSlideShow;->l_strBuilder:Ljava/lang/StringBuilder;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 282
    :cond_0
    return-object v0
.end method

.method public getUnderlyingBytesLength()I
    .locals 1

    .prologue
    .line 611
    iget v0, p0, Lorg/apache/index/poi/hslf/HSLFSlideShow;->_docstreamLength:I

    return v0
.end method

.method public getUnderlyingStream()Lorg/apache/index/poi/poifs/filesystem/DocumentInputStream;
    .locals 1

    .prologue
    .line 617
    iget-object v0, p0, Lorg/apache/index/poi/hslf/HSLFSlideShow;->_mainStreamPPTDoc:Lorg/apache/index/poi/poifs/filesystem/DocumentInputStream;

    return-object v0
.end method
