.class public final Lorg/apache/index/poi/hwpf/sprm/SprmUtils;
.super Ljava/lang/Object;
.source "SprmUtils.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    return-void
.end method

.method public static addSpecialSprm(S[BLjava/util/List;)I
    .locals 4
    .param p0, "instruction"    # S
    .param p1, "varParam"    # [B
    .param p2, "list"    # Ljava/util/List;

    .prologue
    .line 46
    array-length v1, p1

    add-int/lit8 v1, v1, 0x4

    new-array v0, v1, [B

    .line 47
    .local v0, "sprm":[B
    const/4 v1, 0x0

    const/4 v2, 0x4

    array-length v3, p1

    invoke-static {p1, v1, v0, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 48
    invoke-static {v0, p0}, Lorg/apache/index/poi/util/LittleEndian;->putShort([BS)V

    .line 49
    const/4 v1, 0x2

    array-length v2, p1

    add-int/lit8 v2, v2, 0x1

    int-to-short v2, v2

    invoke-static {v0, v1, v2}, Lorg/apache/index/poi/util/LittleEndian;->putShort([BIS)V

    .line 50
    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 51
    array-length v1, v0

    return v1
.end method

.method public static addSprm(SI[BLjava/util/List;)I
    .locals 8
    .param p0, "instruction"    # S
    .param p1, "param"    # I
    .param p2, "varParam"    # [B
    .param p3, "list"    # Ljava/util/List;

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v3, 0x0

    const/4 v5, 0x2

    .line 56
    const v4, 0xe000

    and-int/2addr v4, p0

    shr-int/lit8 v2, v4, 0xd

    .line 58
    .local v2, "type":I
    const/4 v0, 0x0

    .line 59
    .local v0, "sprm":[B
    packed-switch v2, :pswitch_data_0

    .line 98
    :cond_0
    :goto_0
    if-eqz v0, :cond_1

    .line 99
    invoke-static {v0, v3, p0}, Lorg/apache/index/poi/util/LittleEndian;->putShort([BIS)V

    .line 100
    invoke-interface {p3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 101
    array-length v3, v0

    .line 103
    :cond_1
    return v3

    .line 63
    :pswitch_0
    new-array v0, v6, [B

    .line 64
    int-to-byte v4, p1

    aput-byte v4, v0, v5

    goto :goto_0

    .line 67
    :pswitch_1
    new-array v0, v7, [B

    .line 68
    int-to-short v4, p1

    invoke-static {v0, v5, v4}, Lorg/apache/index/poi/util/LittleEndian;->putShort([BIS)V

    goto :goto_0

    .line 71
    :pswitch_2
    const/4 v4, 0x6

    new-array v0, v4, [B

    .line 72
    invoke-static {v0, v5, p1}, Lorg/apache/index/poi/util/LittleEndian;->putInt([BII)V

    goto :goto_0

    .line 76
    :pswitch_3
    new-array v0, v7, [B

    .line 77
    int-to-short v4, p1

    invoke-static {v0, v5, v4}, Lorg/apache/index/poi/util/LittleEndian;->putShort([BIS)V

    goto :goto_0

    .line 81
    :pswitch_4
    if-eqz p2, :cond_0

    .line 82
    array-length v4, p2

    add-int/lit8 v4, v4, 0x3

    new-array v0, v4, [B

    .line 83
    array-length v4, p2

    int-to-byte v4, v4

    aput-byte v4, v0, v5

    .line 84
    array-length v4, p2

    invoke-static {p2, v3, v0, v6, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_0

    .line 88
    :pswitch_5
    const/4 v4, 0x5

    new-array v0, v4, [B

    .line 90
    new-array v1, v7, [B

    .line 91
    .local v1, "temp":[B
    invoke-static {v1, v3, p1}, Lorg/apache/index/poi/util/LittleEndian;->putInt([BII)V

    .line 92
    invoke-static {v1, v3, v0, v5, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_0

    .line 59
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public static convertBrcToInt([S)I
    .locals 3
    .param p0, "brc"    # [S

    .prologue
    .line 126
    const/4 v1, 0x4

    new-array v0, v1, [B

    .line 127
    .local v0, "buf":[B
    const/4 v1, 0x0

    aget-short v1, p0, v1

    invoke-static {v0, v1}, Lorg/apache/index/poi/util/LittleEndian;->putShort([BS)V

    .line 128
    const/4 v1, 0x2

    const/4 v2, 0x1

    aget-short v2, p0, v2

    invoke-static {v0, v1, v2}, Lorg/apache/index/poi/util/LittleEndian;->putShort([BIS)V

    .line 129
    invoke-static {v0}, Lorg/apache/index/poi/util/LittleEndian;->getInt([B)I

    move-result v1

    return v1
.end method

.method public static getGrpprl(Ljava/util/List;I)[B
    .locals 6
    .param p0, "sprmList"    # Ljava/util/List;
    .param p1, "size"    # I

    .prologue
    const/4 v5, 0x0

    .line 110
    new-array v0, p1, [B

    .line 111
    .local v0, "grpprl":[B
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v4

    add-int/lit8 v2, v4, -0x1

    .line 112
    .local v2, "listSize":I
    const/4 v1, 0x0

    .line 113
    .local v1, "index":I
    :goto_0
    if-gez v2, :cond_0

    .line 120
    return-object v0

    .line 115
    :cond_0
    invoke-interface {p0, v5}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [B

    .line 116
    .local v3, "sprm":[B
    array-length v4, v3

    invoke-static {v3, v5, v0, v1, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 117
    array-length v4, v3

    add-int/2addr v1, v4

    .line 113
    add-int/lit8 v2, v2, -0x1

    goto :goto_0
.end method

.method public static shortArrayToByteArray([S)[B
    .locals 4
    .param p0, "convert"    # [S

    .prologue
    .line 34
    array-length v2, p0

    mul-int/lit8 v2, v2, 0x2

    new-array v0, v2, [B

    .line 36
    .local v0, "buf":[B
    const/4 v1, 0x0

    .local v1, "x":I
    :goto_0
    array-length v2, p0

    if-lt v1, v2, :cond_0

    .line 41
    return-object v0

    .line 38
    :cond_0
    mul-int/lit8 v2, v1, 0x2

    aget-short v3, p0, v1

    invoke-static {v0, v2, v3}, Lorg/apache/index/poi/util/LittleEndian;->putShort([BIS)V

    .line 36
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method
