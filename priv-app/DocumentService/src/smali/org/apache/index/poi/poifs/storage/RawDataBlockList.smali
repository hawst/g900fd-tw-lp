.class public Lorg/apache/index/poi/poifs/storage/RawDataBlockList;
.super Lorg/apache/index/poi/poifs/storage/BlockListImpl;
.source "RawDataBlockList.java"


# direct methods
.method public constructor <init>(Lorg/apache/index/poi/poifs/filesystem/POIFSFileHandler;Ljava/io/InputStream;Lorg/apache/index/poi/poifs/common/POIFSBigBlockSize;)V
    .locals 4
    .param p1, "fileHandler"    # Lorg/apache/index/poi/poifs/filesystem/POIFSFileHandler;
    .param p2, "stream"    # Ljava/io/InputStream;
    .param p3, "bigBlockSize"    # Lorg/apache/index/poi/poifs/common/POIFSBigBlockSize;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 50
    invoke-direct {p0}, Lorg/apache/index/poi/poifs/storage/BlockListImpl;-><init>()V

    .line 53
    const/16 v2, 0x200

    .line 54
    .local v2, "startIndex":I
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 58
    .local v1, "blocks":Ljava/util/List;, "Ljava/util/List<Lorg/apache/index/poi/poifs/storage/RawDataBlock;>;"
    :goto_0
    new-instance v0, Lorg/apache/index/poi/poifs/storage/RawDataBlock;

    invoke-virtual {p3}, Lorg/apache/index/poi/poifs/common/POIFSBigBlockSize;->getBigBlockSize()I

    move-result v3

    invoke-direct {v0, p1, p2, v3, v2}, Lorg/apache/index/poi/poifs/storage/RawDataBlock;-><init>(Lorg/apache/index/poi/poifs/filesystem/POIFSFileHandler;Ljava/io/InputStream;II)V

    .line 61
    .local v0, "block":Lorg/apache/index/poi/poifs/storage/RawDataBlock;
    invoke-virtual {v0}, Lorg/apache/index/poi/poifs/storage/RawDataBlock;->hasData()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 62
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 66
    :cond_0
    invoke-virtual {v0}, Lorg/apache/index/poi/poifs/storage/RawDataBlock;->eof()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 75
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    new-array v3, v3, [Lorg/apache/index/poi/poifs/storage/RawDataBlock;

    invoke-interface {v1, v3}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Lorg/apache/index/poi/poifs/storage/ListManagedBlock;

    invoke-virtual {p0, v3}, Lorg/apache/index/poi/poifs/storage/RawDataBlockList;->setBlocks([Lorg/apache/index/poi/poifs/storage/ListManagedBlock;)V

    .line 76
    return-void

    .line 71
    :cond_1
    invoke-virtual {v0}, Lorg/apache/index/poi/poifs/storage/RawDataBlock;->getCurrentStreamIndex()I

    move-result v2

    .line 56
    goto :goto_0
.end method


# virtual methods
.method public bridge synthetic blockCount()I
    .locals 1

    .prologue
    .line 1
    invoke-super {p0}, Lorg/apache/index/poi/poifs/storage/BlockListImpl;->blockCount()I

    move-result v0

    return v0
.end method

.method public bridge synthetic fetchBlocks(II)[Lorg/apache/index/poi/poifs/storage/ListManagedBlock;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1
    invoke-super {p0, p1, p2}, Lorg/apache/index/poi/poifs/storage/BlockListImpl;->fetchBlocks(II)[Lorg/apache/index/poi/poifs/storage/ListManagedBlock;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic remove(I)Lorg/apache/index/poi/poifs/storage/ListManagedBlock;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1
    invoke-super {p0, p1}, Lorg/apache/index/poi/poifs/storage/BlockListImpl;->remove(I)Lorg/apache/index/poi/poifs/storage/ListManagedBlock;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic setBAT(Lorg/apache/index/poi/poifs/storage/BlockAllocationTableReader;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1
    invoke-super {p0, p1}, Lorg/apache/index/poi/poifs/storage/BlockListImpl;->setBAT(Lorg/apache/index/poi/poifs/storage/BlockAllocationTableReader;)V

    return-void
.end method

.method public bridge synthetic zap(I)V
    .locals 0

    .prologue
    .line 1
    invoke-super {p0, p1}, Lorg/apache/index/poi/poifs/storage/BlockListImpl;->zap(I)V

    return-void
.end method
