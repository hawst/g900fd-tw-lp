.class public abstract Lorg/apache/index/poi/hslf/record/Record;
.super Ljava/lang/Object;
.source "Record.java"


# instance fields
.field protected logger:Lorg/apache/index/poi/util/POILogger;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/index/poi/util/POILogFactory;->getLogger(Ljava/lang/Class;)Lorg/apache/index/poi/util/POILogger;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/index/poi/hslf/record/Record;->logger:Lorg/apache/index/poi/util/POILogger;

    .line 40
    return-void
.end method

.method public static buildRecordAtOffset(Lorg/apache/index/poi/poifs/filesystem/DocumentInputStream;I)Lorg/apache/index/poi/hslf/record/Record;
    .locals 13
    .param p0, "docStream"    # Lorg/apache/index/poi/poifs/filesystem/DocumentInputStream;
    .param p1, "offset"    # I

    .prologue
    .line 93
    const/16 v3, 0x8

    new-array v6, v3, [B

    .line 97
    .local v6, "btemp":[B
    const-wide/16 v8, 0x0

    .line 98
    .local v8, "numberOfBytesSkipped":J
    :try_start_0
    invoke-virtual {p0}, Lorg/apache/index/poi/poifs/filesystem/DocumentInputStream;->reset()V

    .line 100
    int-to-long v4, p1

    invoke-virtual {p0, v4, v5}, Lorg/apache/index/poi/poifs/filesystem/DocumentInputStream;->skip(J)J

    move-result-wide v8

    .line 101
    int-to-long v4, p1

    cmp-long v3, v8, v4

    if-eqz v3, :cond_0

    .line 103
    sget-object v3, Ljava/lang/System;->err:Ljava/io/PrintStream;

    const-string/jumbo v4, "Error: Skipping of docStream failed in buildRecordAtOffset"

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 104
    const/4 v3, 0x0

    .line 137
    :goto_0
    return-object v3

    .line 108
    :cond_0
    invoke-virtual {p0, v6}, Lorg/apache/index/poi/poifs/filesystem/DocumentInputStream;->read([B)I

    .line 111
    const/4 v3, 0x2

    invoke-static {v6, v3}, Lorg/apache/index/poi/util/LittleEndian;->getUShort([BI)I

    move-result v3

    int-to-long v0, v3

    .line 112
    .local v0, "type":J
    const/4 v3, 0x4

    invoke-static {v6, v3}, Lorg/apache/index/poi/util/LittleEndian;->getUInt([BI)J

    move-result-wide v10

    .line 115
    .local v10, "rlen":J
    long-to-int v12, v10

    .line 116
    .local v12, "rleni":I
    if-gez v12, :cond_1

    const/4 v12, 0x0

    .line 119
    :cond_1
    add-int/lit8 v3, v12, 0x8

    new-array v2, v3, [B

    .line 120
    .local v2, "b":[B
    const/4 v3, 0x0

    const/4 v4, 0x0

    aget-byte v4, v6, v4

    aput-byte v4, v2, v3

    .line 121
    const/4 v3, 0x1

    const/4 v4, 0x1

    aget-byte v4, v6, v4

    aput-byte v4, v2, v3

    .line 122
    const/4 v3, 0x2

    const/4 v4, 0x2

    aget-byte v4, v6, v4

    aput-byte v4, v2, v3

    .line 123
    const/4 v3, 0x3

    const/4 v4, 0x3

    aget-byte v4, v6, v4

    aput-byte v4, v2, v3

    .line 124
    const/4 v3, 0x4

    const/4 v4, 0x4

    aget-byte v4, v6, v4

    aput-byte v4, v2, v3

    .line 125
    const/4 v3, 0x5

    const/4 v4, 0x5

    aget-byte v4, v6, v4

    aput-byte v4, v2, v3

    .line 126
    const/4 v3, 0x6

    const/4 v4, 0x6

    aget-byte v4, v6, v4

    aput-byte v4, v2, v3

    .line 127
    const/4 v3, 0x7

    const/4 v4, 0x7

    aget-byte v4, v6, v4

    aput-byte v4, v2, v3

    .line 129
    const/16 v3, 0x8

    invoke-virtual {p0, v2, v3, v12}, Lorg/apache/index/poi/poifs/filesystem/DocumentInputStream;->read([BII)I

    .line 132
    const/4 v3, 0x0

    add-int/lit8 v4, v12, 0x8

    move v5, p1

    invoke-static/range {v0 .. v5}, Lorg/apache/index/poi/hslf/record/Record;->createRecordForType(J[BIII)Lorg/apache/index/poi/hslf/record/Record;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    goto :goto_0

    .line 134
    .end local v0    # "type":J
    .end local v2    # "b":[B
    .end local v10    # "rlen":J
    .end local v12    # "rleni":I
    :catch_0
    move-exception v7

    .line 136
    .local v7, "e":Ljava/io/IOException;
    sget-object v3, Ljava/lang/System;->err:Ljava/io/PrintStream;

    const-string/jumbo v4, "Error: Skipping/Reading the PPT Stream has failed "

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 137
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public static createRecordForType(J[BIII)Lorg/apache/index/poi/hslf/record/Record;
    .locals 12
    .param p0, "type"    # J
    .param p2, "b"    # [B
    .param p3, "start"    # I
    .param p4, "len"    # I
    .param p5, "actualStart"    # I

    .prologue
    .line 201
    const/4 v8, 0x0

    .line 205
    .local v8, "toReturn":Lorg/apache/index/poi/hslf/record/Record;
    add-int v9, p3, p4

    array-length v10, p2

    if-le v9, v10, :cond_0

    .line 206
    sget-object v9, Ljava/lang/System;->err:Ljava/io/PrintStream;

    new-instance v10, Ljava/lang/StringBuilder;

    const-string/jumbo v11, "Warning: Skipping record of type "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string/jumbo v11, " at position "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string/jumbo v11, " which claims to be longer than the file! ("

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move/from16 v0, p4

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string/jumbo v11, " vs "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    array-length v11, p2

    sub-int/2addr v11, p3

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string/jumbo v11, ")"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 207
    const/4 v9, 0x0

    .line 249
    :goto_0
    return-object v9

    .line 215
    :cond_0
    const/4 v1, 0x0

    .line 217
    .local v1, "c":Ljava/lang/Class;, "Ljava/lang/Class<+Lorg/apache/index/poi/hslf/record/Record;>;"
    long-to-int v9, p0

    :try_start_0
    invoke-static {v9}, Lorg/apache/index/poi/hslf/record/RecordTypes;->recordHandlingClass(I)Ljava/lang/Class;

    move-result-object v1

    .line 218
    if-nez v1, :cond_1

    .line 223
    sget-object v9, Lorg/apache/index/poi/hslf/record/RecordTypes;->Unknown:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    iget v9, v9, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;->typeID:I

    invoke-static {v9}, Lorg/apache/index/poi/hslf/record/RecordTypes;->recordHandlingClass(I)Ljava/lang/Class;

    move-result-object v1

    .line 227
    :cond_1
    const/4 v9, 0x3

    new-array v9, v9, [Ljava/lang/Class;

    const/4 v10, 0x0

    const-class v11, [B

    aput-object v11, v9, v10

    const/4 v10, 0x1

    sget-object v11, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v11, v9, v10

    const/4 v10, 0x2

    sget-object v11, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v11, v9, v10

    invoke-virtual {v1, v9}, Ljava/lang/Class;->getDeclaredConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v2

    .line 229
    .local v2, "con":Ljava/lang/reflect/Constructor;, "Ljava/lang/reflect/Constructor<+Lorg/apache/index/poi/hslf/record/Record;>;"
    const/4 v9, 0x3

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    aput-object p2, v9, v10

    const/4 v10, 0x1

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x2

    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-virtual {v2, v9}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    .end local v8    # "toReturn":Lorg/apache/index/poi/hslf/record/Record;
    check-cast v8, Lorg/apache/index/poi/hslf/record/Record;
    :try_end_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_3

    .line 243
    .restart local v8    # "toReturn":Lorg/apache/index/poi/hslf/record/Record;
    instance-of v9, v8, Lorg/apache/index/poi/hslf/record/PositionDependentRecord;

    if-eqz v9, :cond_2

    move-object v7, v8

    .line 244
    check-cast v7, Lorg/apache/index/poi/hslf/record/PositionDependentRecord;

    .line 245
    .local v7, "pdr":Lorg/apache/index/poi/hslf/record/PositionDependentRecord;
    move/from16 v0, p5

    invoke-interface {v7, v0}, Lorg/apache/index/poi/hslf/record/PositionDependentRecord;->setLastOnDiskOffset(I)V

    .end local v7    # "pdr":Lorg/apache/index/poi/hslf/record/PositionDependentRecord;
    :cond_2
    move-object v9, v8

    .line 249
    goto :goto_0

    .line 230
    .end local v2    # "con":Ljava/lang/reflect/Constructor;, "Ljava/lang/reflect/Constructor<+Lorg/apache/index/poi/hslf/record/Record;>;"
    .end local v8    # "toReturn":Lorg/apache/index/poi/hslf/record/Record;
    :catch_0
    move-exception v4

    .line 231
    .local v4, "ie":Ljava/lang/InstantiationException;
    new-instance v9, Ljava/lang/RuntimeException;

    new-instance v10, Ljava/lang/StringBuilder;

    const-string/jumbo v11, "Couldn\'t instantiate the class for type with id "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string/jumbo v11, " on class "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string/jumbo v11, " : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v9

    .line 232
    .end local v4    # "ie":Ljava/lang/InstantiationException;
    :catch_1
    move-exception v5

    .line 233
    .local v5, "ite":Ljava/lang/reflect/InvocationTargetException;
    new-instance v9, Ljava/lang/RuntimeException;

    new-instance v10, Ljava/lang/StringBuilder;

    const-string/jumbo v11, "Couldn\'t instantiate the class for type with id "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string/jumbo v11, " on class "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string/jumbo v11, " : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string/jumbo v11, "\nCause was : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v5}, Ljava/lang/reflect/InvocationTargetException;->getCause()Ljava/lang/Throwable;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10, v5}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v9

    .line 234
    .end local v5    # "ite":Ljava/lang/reflect/InvocationTargetException;
    :catch_2
    move-exception v3

    .line 235
    .local v3, "iae":Ljava/lang/IllegalAccessException;
    new-instance v9, Ljava/lang/RuntimeException;

    new-instance v10, Ljava/lang/StringBuilder;

    const-string/jumbo v11, "Couldn\'t access the constructor for type with id "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string/jumbo v11, " on class "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string/jumbo v11, " : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v9

    .line 236
    .end local v3    # "iae":Ljava/lang/IllegalAccessException;
    :catch_3
    move-exception v6

    .line 237
    .local v6, "nsme":Ljava/lang/NoSuchMethodException;
    new-instance v9, Ljava/lang/RuntimeException;

    new-instance v10, Ljava/lang/StringBuilder;

    const-string/jumbo v11, "Couldn\'t access the constructor for type with id "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string/jumbo v11, " on class "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string/jumbo v11, " : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10, v6}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v9
.end method

.method public static findChildRecords([BII)[Lorg/apache/index/poi/hslf/record/Record;
    .locals 12
    .param p0, "b"    # [B
    .param p1, "start"    # I
    .param p2, "len"    # I

    .prologue
    .line 147
    new-instance v7, Ljava/util/ArrayList;

    const/4 v2, 0x5

    invoke-direct {v7, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 150
    .local v7, "children":Ljava/util/List;, "Ljava/util/List<Lorg/apache/index/poi/hslf/record/Record;>;"
    move v3, p1

    .line 151
    .local v3, "pos":I
    :goto_0
    add-int v2, p1, p2

    add-int/lit8 v2, v2, -0x8

    if-le v3, v2, :cond_0

    .line 176
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v2

    new-array v2, v2, [Lorg/apache/index/poi/hslf/record/Record;

    invoke-interface {v7, v2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v6

    check-cast v6, [Lorg/apache/index/poi/hslf/record/Record;

    .line 177
    .local v6, "cRecords":[Lorg/apache/index/poi/hslf/record/Record;
    return-object v6

    .line 152
    .end local v6    # "cRecords":[Lorg/apache/index/poi/hslf/record/Record;
    :cond_0
    add-int/lit8 v2, v3, 0x2

    invoke-static {p0, v2}, Lorg/apache/index/poi/util/LittleEndian;->getUShort([BI)I

    move-result v2

    int-to-long v0, v2

    .line 153
    .local v0, "type":J
    add-int/lit8 v2, v3, 0x4

    invoke-static {p0, v2}, Lorg/apache/index/poi/util/LittleEndian;->getUInt([BI)J

    move-result-wide v10

    .line 156
    .local v10, "rlen":J
    long-to-int v9, v10

    .line 157
    .local v9, "rleni":I
    if-gez v9, :cond_1

    const/4 v9, 0x0

    .line 161
    :cond_1
    if-nez v3, :cond_2

    const-wide/16 v4, 0x0

    cmp-long v2, v0, v4

    if-nez v2, :cond_2

    const v2, 0xffff

    if-ne v9, v2, :cond_2

    .line 162
    new-instance v2, Lorg/apache/index/poi/hslf/exceptions/CorruptPowerPointFileException;

    const-string/jumbo v4, "Corrupt document - starts with record of type 0000 and length 0xFFFF"

    invoke-direct {v2, v4}, Lorg/apache/index/poi/hslf/exceptions/CorruptPowerPointFileException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 165
    :cond_2
    add-int/lit8 v4, v9, 0x8

    move-object v2, p0

    move v5, v3

    invoke-static/range {v0 .. v5}, Lorg/apache/index/poi/hslf/record/Record;->createRecordForType(J[BIII)Lorg/apache/index/poi/hslf/record/Record;

    move-result-object v8

    .line 166
    .local v8, "r":Lorg/apache/index/poi/hslf/record/Record;
    if-eqz v8, :cond_3

    .line 167
    invoke-interface {v7, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 171
    :cond_3
    add-int/lit8 v3, v3, 0x8

    .line 172
    add-int/2addr v3, v9

    goto :goto_0
.end method

.method public static setTextToUser(Ljava/lang/String;)V
    .locals 0
    .param p0, "inStr"    # Ljava/lang/String;

    .prologue
    .line 183
    invoke-static {p0}, Lorg/apache/index/poi/hslf/HSLFSlideShow;->appendAndCommitString(Ljava/lang/String;)V

    .line 184
    return-void
.end method

.method public static writeLittleEndian(ILjava/io/OutputStream;)V
    .locals 2
    .param p0, "i"    # I
    .param p1, "o"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 72
    const/4 v1, 0x4

    new-array v0, v1, [B

    .line 73
    .local v0, "bi":[B
    invoke-static {v0, p0}, Lorg/apache/index/poi/util/LittleEndian;->putInt([BI)V

    .line 74
    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 75
    return-void
.end method

.method public static writeLittleEndian(SLjava/io/OutputStream;)V
    .locals 2
    .param p0, "s"    # S
    .param p1, "o"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 80
    const/4 v1, 0x2

    new-array v0, v1, [B

    .line 81
    .local v0, "bs":[B
    invoke-static {v0, p0}, Lorg/apache/index/poi/util/LittleEndian;->putShort([BS)V

    .line 82
    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 83
    return-void
.end method


# virtual methods
.method public abstract getChildRecords()[Lorg/apache/index/poi/hslf/record/Record;
.end method

.method public abstract getRecordType()J
.end method

.method public abstract isAnAtom()Z
.end method
