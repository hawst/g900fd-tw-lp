.class public final Lorg/apache/index/poi/hwpf/model/FSPA;
.super Ljava/lang/Object;
.source "FSPA.java"


# static fields
.field public static final FSPA_SIZE:I = 0x1a

.field private static bx:Lorg/apache/index/poi/util/BitField;

.field private static by:Lorg/apache/index/poi/util/BitField;

.field private static fAnchorLock:Lorg/apache/index/poi/util/BitField;

.field private static fBelowText:Lorg/apache/index/poi/util/BitField;

.field private static fHdr:Lorg/apache/index/poi/util/BitField;

.field private static fRcaSimple:Lorg/apache/index/poi/util/BitField;

.field private static wr:Lorg/apache/index/poi/util/BitField;

.field private static wrk:Lorg/apache/index/poi/util/BitField;


# instance fields
.field private cTxbx:I

.field private options:S

.field private spid:I

.field private xaLeft:I

.field private xaRight:I

.field private yaBottom:I

.field private yaTop:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    const/4 v0, 0x1

    invoke-static {v0}, Lorg/apache/index/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/index/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/index/poi/hwpf/model/FSPA;->fHdr:Lorg/apache/index/poi/util/BitField;

    .line 40
    const/4 v0, 0x6

    invoke-static {v0}, Lorg/apache/index/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/index/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/index/poi/hwpf/model/FSPA;->bx:Lorg/apache/index/poi/util/BitField;

    .line 41
    const/16 v0, 0x18

    invoke-static {v0}, Lorg/apache/index/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/index/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/index/poi/hwpf/model/FSPA;->by:Lorg/apache/index/poi/util/BitField;

    .line 42
    const/16 v0, 0x1e0

    invoke-static {v0}, Lorg/apache/index/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/index/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/index/poi/hwpf/model/FSPA;->wr:Lorg/apache/index/poi/util/BitField;

    .line 43
    const/16 v0, 0x1e00

    invoke-static {v0}, Lorg/apache/index/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/index/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/index/poi/hwpf/model/FSPA;->wrk:Lorg/apache/index/poi/util/BitField;

    .line 44
    const/16 v0, 0x2000

    invoke-static {v0}, Lorg/apache/index/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/index/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/index/poi/hwpf/model/FSPA;->fRcaSimple:Lorg/apache/index/poi/util/BitField;

    .line 45
    const/16 v0, 0x4000

    invoke-static {v0}, Lorg/apache/index/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/index/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/index/poi/hwpf/model/FSPA;->fBelowText:Lorg/apache/index/poi/util/BitField;

    .line 46
    const v0, 0x8000

    invoke-static {v0}, Lorg/apache/index/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/index/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/index/poi/hwpf/model/FSPA;->fAnchorLock:Lorg/apache/index/poi/util/BitField;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    return-void
.end method

.method public constructor <init>([BI)V
    .locals 1
    .param p1, "bytes"    # [B
    .param p2, "offset"    # I

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    invoke-static {p1, p2}, Lorg/apache/index/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    iput v0, p0, Lorg/apache/index/poi/hwpf/model/FSPA;->spid:I

    .line 56
    add-int/lit8 p2, p2, 0x4

    .line 57
    invoke-static {p1, p2}, Lorg/apache/index/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    iput v0, p0, Lorg/apache/index/poi/hwpf/model/FSPA;->xaLeft:I

    .line 58
    add-int/lit8 p2, p2, 0x4

    .line 59
    invoke-static {p1, p2}, Lorg/apache/index/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    iput v0, p0, Lorg/apache/index/poi/hwpf/model/FSPA;->yaTop:I

    .line 60
    add-int/lit8 p2, p2, 0x4

    .line 61
    invoke-static {p1, p2}, Lorg/apache/index/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    iput v0, p0, Lorg/apache/index/poi/hwpf/model/FSPA;->xaRight:I

    .line 62
    add-int/lit8 p2, p2, 0x4

    .line 63
    invoke-static {p1, p2}, Lorg/apache/index/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    iput v0, p0, Lorg/apache/index/poi/hwpf/model/FSPA;->yaBottom:I

    .line 64
    add-int/lit8 p2, p2, 0x4

    .line 65
    invoke-static {p1, p2}, Lorg/apache/index/poi/util/LittleEndian;->getShort([BI)S

    move-result v0

    iput-short v0, p0, Lorg/apache/index/poi/hwpf/model/FSPA;->options:S

    .line 66
    add-int/lit8 p2, p2, 0x2

    .line 67
    invoke-static {p1, p2}, Lorg/apache/index/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    iput v0, p0, Lorg/apache/index/poi/hwpf/model/FSPA;->cTxbx:I

    .line 68
    return-void
.end method


# virtual methods
.method public getBx()S
    .locals 2

    .prologue
    .line 102
    sget-object v0, Lorg/apache/index/poi/hwpf/model/FSPA;->bx:Lorg/apache/index/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/index/poi/hwpf/model/FSPA;->options:S

    invoke-virtual {v0, v1}, Lorg/apache/index/poi/util/BitField;->getShortValue(S)S

    move-result v0

    return v0
.end method

.method public getBy()S
    .locals 2

    .prologue
    .line 107
    sget-object v0, Lorg/apache/index/poi/hwpf/model/FSPA;->by:Lorg/apache/index/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/index/poi/hwpf/model/FSPA;->options:S

    invoke-virtual {v0, v1}, Lorg/apache/index/poi/util/BitField;->getShortValue(S)S

    move-result v0

    return v0
.end method

.method public getCTxbx()I
    .locals 1

    .prologue
    .line 137
    iget v0, p0, Lorg/apache/index/poi/hwpf/model/FSPA;->cTxbx:I

    return v0
.end method

.method public getSpid()I
    .locals 1

    .prologue
    .line 72
    iget v0, p0, Lorg/apache/index/poi/hwpf/model/FSPA;->spid:I

    return v0
.end method

.method public getWr()S
    .locals 2

    .prologue
    .line 112
    sget-object v0, Lorg/apache/index/poi/hwpf/model/FSPA;->wr:Lorg/apache/index/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/index/poi/hwpf/model/FSPA;->options:S

    invoke-virtual {v0, v1}, Lorg/apache/index/poi/util/BitField;->getShortValue(S)S

    move-result v0

    return v0
.end method

.method public getWrk()S
    .locals 2

    .prologue
    .line 117
    sget-object v0, Lorg/apache/index/poi/hwpf/model/FSPA;->wrk:Lorg/apache/index/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/index/poi/hwpf/model/FSPA;->options:S

    invoke-virtual {v0, v1}, Lorg/apache/index/poi/util/BitField;->getShortValue(S)S

    move-result v0

    return v0
.end method

.method public getXaLeft()I
    .locals 1

    .prologue
    .line 77
    iget v0, p0, Lorg/apache/index/poi/hwpf/model/FSPA;->xaLeft:I

    return v0
.end method

.method public getXaRight()I
    .locals 1

    .prologue
    .line 87
    iget v0, p0, Lorg/apache/index/poi/hwpf/model/FSPA;->xaRight:I

    return v0
.end method

.method public getYaBottom()I
    .locals 1

    .prologue
    .line 92
    iget v0, p0, Lorg/apache/index/poi/hwpf/model/FSPA;->yaBottom:I

    return v0
.end method

.method public getYaTop()I
    .locals 1

    .prologue
    .line 82
    iget v0, p0, Lorg/apache/index/poi/hwpf/model/FSPA;->yaTop:I

    return v0
.end method

.method public isFAnchorLock()Z
    .locals 2

    .prologue
    .line 132
    sget-object v0, Lorg/apache/index/poi/hwpf/model/FSPA;->fAnchorLock:Lorg/apache/index/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/index/poi/hwpf/model/FSPA;->options:S

    invoke-virtual {v0, v1}, Lorg/apache/index/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFBelowText()Z
    .locals 2

    .prologue
    .line 127
    sget-object v0, Lorg/apache/index/poi/hwpf/model/FSPA;->fBelowText:Lorg/apache/index/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/index/poi/hwpf/model/FSPA;->options:S

    invoke-virtual {v0, v1}, Lorg/apache/index/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFHdr()Z
    .locals 2

    .prologue
    .line 97
    sget-object v0, Lorg/apache/index/poi/hwpf/model/FSPA;->fHdr:Lorg/apache/index/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/index/poi/hwpf/model/FSPA;->options:S

    invoke-virtual {v0, v1}, Lorg/apache/index/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFRcaSimple()Z
    .locals 2

    .prologue
    .line 122
    sget-object v0, Lorg/apache/index/poi/hwpf/model/FSPA;->fRcaSimple:Lorg/apache/index/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/index/poi/hwpf/model/FSPA;->options:S

    invoke-virtual {v0, v1}, Lorg/apache/index/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public toByteArray()[B
    .locals 3

    .prologue
    .line 142
    const/4 v1, 0x0

    .line 143
    .local v1, "offset":I
    const/16 v2, 0x1a

    new-array v0, v2, [B

    .line 145
    .local v0, "buf":[B
    iget v2, p0, Lorg/apache/index/poi/hwpf/model/FSPA;->spid:I

    invoke-static {v0, v1, v2}, Lorg/apache/index/poi/util/LittleEndian;->putInt([BII)V

    .line 146
    add-int/lit8 v1, v1, 0x4

    .line 147
    iget v2, p0, Lorg/apache/index/poi/hwpf/model/FSPA;->xaLeft:I

    invoke-static {v0, v1, v2}, Lorg/apache/index/poi/util/LittleEndian;->putInt([BII)V

    .line 148
    add-int/lit8 v1, v1, 0x4

    .line 149
    iget v2, p0, Lorg/apache/index/poi/hwpf/model/FSPA;->yaTop:I

    invoke-static {v0, v1, v2}, Lorg/apache/index/poi/util/LittleEndian;->putInt([BII)V

    .line 150
    add-int/lit8 v1, v1, 0x4

    .line 151
    iget v2, p0, Lorg/apache/index/poi/hwpf/model/FSPA;->xaRight:I

    invoke-static {v0, v1, v2}, Lorg/apache/index/poi/util/LittleEndian;->putInt([BII)V

    .line 152
    add-int/lit8 v1, v1, 0x4

    .line 153
    iget v2, p0, Lorg/apache/index/poi/hwpf/model/FSPA;->yaBottom:I

    invoke-static {v0, v1, v2}, Lorg/apache/index/poi/util/LittleEndian;->putInt([BII)V

    .line 154
    add-int/lit8 v1, v1, 0x4

    .line 155
    iget-short v2, p0, Lorg/apache/index/poi/hwpf/model/FSPA;->options:S

    invoke-static {v0, v1, v2}, Lorg/apache/index/poi/util/LittleEndian;->putShort([BIS)V

    .line 156
    add-int/lit8 v1, v1, 0x2

    .line 157
    iget v2, p0, Lorg/apache/index/poi/hwpf/model/FSPA;->cTxbx:I

    invoke-static {v0, v1, v2}, Lorg/apache/index/poi/util/LittleEndian;->putInt([BII)V

    .line 160
    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 165
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 166
    .local v0, "buf":Ljava/lang/StringBuffer;
    const-string/jumbo v1, "spid: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget v2, p0, Lorg/apache/index/poi/hwpf/model/FSPA;->spid:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 167
    const-string/jumbo v1, ", xaLeft: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget v2, p0, Lorg/apache/index/poi/hwpf/model/FSPA;->xaLeft:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 168
    const-string/jumbo v1, ", yaTop: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget v2, p0, Lorg/apache/index/poi/hwpf/model/FSPA;->yaTop:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 169
    const-string/jumbo v1, ", xaRight: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget v2, p0, Lorg/apache/index/poi/hwpf/model/FSPA;->xaRight:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 170
    const-string/jumbo v1, ", yaBottom: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget v2, p0, Lorg/apache/index/poi/hwpf/model/FSPA;->yaBottom:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 171
    const-string/jumbo v1, ", options: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-short v2, p0, Lorg/apache/index/poi/hwpf/model/FSPA;->options:S

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 172
    const-string/jumbo v1, " (fHdr: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/model/FSPA;->isFHdr()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    .line 173
    const-string/jumbo v1, ", bx: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/model/FSPA;->getBx()S

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 174
    const-string/jumbo v1, ", by: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/model/FSPA;->getBy()S

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 175
    const-string/jumbo v1, ", wr: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/model/FSPA;->getWr()S

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 176
    const-string/jumbo v1, ", wrk: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/model/FSPA;->getWrk()S

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 177
    const-string/jumbo v1, ", fRcaSimple: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/model/FSPA;->isFRcaSimple()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    .line 178
    const-string/jumbo v1, ", fBelowText: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/model/FSPA;->isFBelowText()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    .line 179
    const-string/jumbo v1, ", fAnchorLock: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/model/FSPA;->isFAnchorLock()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    .line 180
    const-string/jumbo v1, "), cTxbx: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget v2, p0, Lorg/apache/index/poi/hwpf/model/FSPA;->cTxbx:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 181
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
