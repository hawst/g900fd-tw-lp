.class public Lorg/apache/index/poi/hwpf/model/TextPieceTable;
.super Ljava/lang/Object;
.source "TextPieceTable.java"

# interfaces
.implements Lorg/apache/index/poi/hwpf/model/CharIndexTranslator;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/index/poi/hwpf/model/TextPieceTable$FCComparator;
    }
.end annotation


# static fields
.field private static final EMPTY:Ljava/lang/String; = ""

.field private static final END_FIELD:C = '\u0015'

.field private static final HYPERLINK:Ljava/lang/String; = "HYPERLINK"

.field private static final MAX_CHAR_COUNT:I = 0x100000

.field private static final PAGE:Ljava/lang/String; = "PAGE"

.field private static final PAGEREF:Ljava/lang/String; = "PAGEREF"

.field private static final PAGE_REF_HYPER_LINK:Ljava/lang/String; = "\\h"

.field private static final SEQ:Ljava/lang/String; = "SEQ"

.field private static final SPACE:Ljava/lang/String; = " "

.field private static final SPECIAL_FIELD:C = '\u0014'

.field private static final START_FIELD:C = '\u0013'

.field private static final TOC:Ljava/lang/String; = "TOC"


# instance fields
.field _cpMin:I

.field protected _textPieces:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lorg/apache/index/poi/hwpf/model/TextPiece;",
            ">;"
        }
    .end annotation
.end field

.field protected _textPiecesFCOrder:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lorg/apache/index/poi/hwpf/model/TextPiece;",
            ">;"
        }
    .end annotation
.end field

.field private strBuilder:Ljava/lang/StringBuilder;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/apache/index/poi/hwpf/model/TextPieceTable;->_textPieces:Ljava/util/ArrayList;

    .line 60
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/apache/index/poi/hwpf/model/TextPieceTable;->_textPiecesFCOrder:Ljava/util/ArrayList;

    .line 62
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lorg/apache/index/poi/hwpf/model/TextPieceTable;->strBuilder:Ljava/lang/StringBuilder;

    .line 68
    return-void
.end method

.method public constructor <init>(Lcom/samsung/index/ITextContentObs;Lorg/apache/index/poi/poifs/filesystem/DocumentInputStream;[B[BIII)V
    .locals 26
    .param p1, "indexwriterIn"    # Lcom/samsung/index/ITextContentObs;
    .param p2, "documentFileStream"    # Lorg/apache/index/poi/poifs/filesystem/DocumentInputStream;
    .param p3, "documentStream"    # [B
    .param p4, "tableStream"    # [B
    .param p5, "offset"    # I
    .param p6, "size"    # I
    .param p7, "fcMin"    # I

    .prologue
    .line 88
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    .line 59
    new-instance v23, Ljava/util/ArrayList;

    invoke-direct/range {v23 .. v23}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, v23

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/apache/index/poi/hwpf/model/TextPieceTable;->_textPieces:Ljava/util/ArrayList;

    .line 60
    new-instance v23, Ljava/util/ArrayList;

    invoke-direct/range {v23 .. v23}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, v23

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/apache/index/poi/hwpf/model/TextPieceTable;->_textPiecesFCOrder:Ljava/util/ArrayList;

    .line 62
    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v23

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/apache/index/poi/hwpf/model/TextPieceTable;->strBuilder:Ljava/lang/StringBuilder;

    .line 95
    new-instance v11, Lorg/apache/index/poi/hwpf/model/PlexOfCps;

    .line 96
    invoke-static {}, Lorg/apache/index/poi/hwpf/model/PieceDescriptor;->getSizeInBytes()I

    move-result v23

    .line 95
    move-object/from16 v0, p4

    move/from16 v1, p5

    move/from16 v2, p6

    move/from16 v3, v23

    invoke-direct {v11, v0, v1, v2, v3}, Lorg/apache/index/poi/hwpf/model/PlexOfCps;-><init>([BIII)V

    .line 98
    .local v11, "pieceTable":Lorg/apache/index/poi/hwpf/model/PlexOfCps;
    invoke-virtual {v11}, Lorg/apache/index/poi/hwpf/model/PlexOfCps;->length()I

    move-result v6

    .line 99
    .local v6, "length":I
    new-array v12, v6, [Lorg/apache/index/poi/hwpf/model/PieceDescriptor;

    .line 103
    .local v12, "pieces":[Lorg/apache/index/poi/hwpf/model/PieceDescriptor;
    const/16 v22, 0x0

    .local v22, "x":I
    :goto_0
    move/from16 v0, v22

    if-lt v0, v6, :cond_0

    .line 110
    const/16 v23, 0x0

    aget-object v23, v12, v23

    invoke-virtual/range {v23 .. v23}, Lorg/apache/index/poi/hwpf/model/PieceDescriptor;->getFilePosition()I

    move-result v23

    sub-int v23, v23, p7

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/index/poi/hwpf/model/TextPieceTable;->_cpMin:I

    .line 111
    const/16 v22, 0x0

    :goto_1
    array-length v0, v12

    move/from16 v23, v0

    move/from16 v0, v22

    move/from16 v1, v23

    if-lt v0, v1, :cond_1

    .line 120
    const/16 v16, 0x0

    .line 122
    .local v16, "streamStartPos":I
    const/16 v22, 0x0

    :goto_2
    array-length v0, v12

    move/from16 v23, v0

    move/from16 v0, v22

    move/from16 v1, v23

    if-lt v0, v1, :cond_3

    .line 242
    return-void

    .line 104
    .end local v16    # "streamStartPos":I
    :cond_0
    move/from16 v0, v22

    invoke-virtual {v11, v0}, Lorg/apache/index/poi/hwpf/model/PlexOfCps;->getProperty(I)Lorg/apache/index/poi/hwpf/model/GenericPropertyNode;

    move-result-object v8

    .line 105
    .local v8, "node":Lorg/apache/index/poi/hwpf/model/GenericPropertyNode;
    new-instance v23, Lorg/apache/index/poi/hwpf/model/PieceDescriptor;

    invoke-virtual {v8}, Lorg/apache/index/poi/hwpf/model/GenericPropertyNode;->getBytes()[B

    move-result-object v24

    const/16 v25, 0x0

    invoke-direct/range {v23 .. v25}, Lorg/apache/index/poi/hwpf/model/PieceDescriptor;-><init>([BI)V

    aput-object v23, v12, v22

    .line 103
    add-int/lit8 v22, v22, 0x1

    goto :goto_0

    .line 112
    .end local v8    # "node":Lorg/apache/index/poi/hwpf/model/GenericPropertyNode;
    :cond_1
    aget-object v23, v12, v22

    invoke-virtual/range {v23 .. v23}, Lorg/apache/index/poi/hwpf/model/PieceDescriptor;->getFilePosition()I

    move-result v23

    sub-int v15, v23, p7

    .line 113
    .local v15, "start":I
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/index/poi/hwpf/model/TextPieceTable;->_cpMin:I

    move/from16 v23, v0

    move/from16 v0, v23

    if-ge v15, v0, :cond_2

    .line 114
    move-object/from16 v0, p0

    iput v15, v0, Lorg/apache/index/poi/hwpf/model/TextPieceTable;->_cpMin:I

    .line 111
    :cond_2
    add-int/lit8 v22, v22, 0x1

    goto :goto_1

    .line 123
    .end local v15    # "start":I
    .restart local v16    # "streamStartPos":I
    :cond_3
    aget-object v23, v12, v22

    invoke-virtual/range {v23 .. v23}, Lorg/apache/index/poi/hwpf/model/PieceDescriptor;->getFilePosition()I

    move-result v15

    .line 124
    .restart local v15    # "start":I
    move/from16 v0, v22

    invoke-virtual {v11, v0}, Lorg/apache/index/poi/hwpf/model/PlexOfCps;->getProperty(I)Lorg/apache/index/poi/hwpf/model/GenericPropertyNode;

    move-result-object v8

    .line 127
    .local v8, "node":Lorg/apache/index/poi/hwpf/model/PropertyNode;
    invoke-virtual {v8}, Lorg/apache/index/poi/hwpf/model/PropertyNode;->getStart()I

    move-result v10

    .line 128
    .local v10, "nodeStartChars":I
    invoke-virtual {v8}, Lorg/apache/index/poi/hwpf/model/PropertyNode;->getEnd()I

    move-result v9

    .line 131
    .local v9, "nodeEndChars":I
    aget-object v23, v12, v22

    invoke-virtual/range {v23 .. v23}, Lorg/apache/index/poi/hwpf/model/PieceDescriptor;->isUnicode()Z

    move-result v21

    .line 132
    .local v21, "unicode":Z
    const/4 v7, 0x1

    .line 133
    .local v7, "multiple":I
    if-eqz v21, :cond_4

    .line 134
    const/4 v7, 0x2

    .line 138
    :cond_4
    sub-int v19, v9, v10

    .line 139
    .local v19, "textSizeChars":I
    mul-int v18, v19, v7

    .line 143
    .local v18, "textSizeBytes":I
    if-nez p2, :cond_9

    .line 144
    move/from16 v0, v18

    new-array v4, v0, [B

    .line 145
    .local v4, "buf":[B
    const/16 v23, 0x0

    move-object/from16 v0, p3

    move/from16 v1, v23

    move/from16 v2, v18

    invoke-static {v0, v15, v4, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 151
    aget-object v23, v12, v22

    move-object/from16 v0, v23

    invoke-static {v4, v0}, Lorg/apache/index/poi/hwpf/model/TextPieceTable;->buildInitSBInTable([BLorg/apache/index/poi/hwpf/model/PieceDescriptor;)Ljava/lang/String;

    move-result-object v17

    .line 153
    .local v17, "strlocal":Ljava/lang/String;
    if-eqz v17, :cond_5

    .line 154
    invoke-static/range {v17 .. v17}, Lorg/apache/index/poi/hwpf/model/TextPieceTable;->stripFields(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    .line 159
    :cond_5
    if-eqz p1, :cond_6

    .line 160
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/index/poi/hwpf/model/TextPieceTable;->strBuilder:Ljava/lang/StringBuilder;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->length()I

    move-result v23

    const/high16 v24, 0x100000

    move/from16 v0, v23

    move/from16 v1, v24

    if-lt v0, v1, :cond_7

    .line 161
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/index/poi/hwpf/model/TextPieceTable;->strBuilder:Ljava/lang/StringBuilder;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    invoke-interface {v0, v1}, Lcom/samsung/index/ITextContentObs;->updateText(Ljava/lang/String;)V

    .line 162
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/index/poi/hwpf/model/TextPieceTable;->strBuilder:Ljava/lang/StringBuilder;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 163
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/index/poi/hwpf/model/TextPieceTable;->strBuilder:Ljava/lang/StringBuilder;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 122
    .end local v4    # "buf":[B
    .end local v17    # "strlocal":Ljava/lang/String;
    :cond_6
    :goto_3
    add-int/lit8 v22, v22, 0x1

    goto/16 :goto_2

    .line 164
    .restart local v4    # "buf":[B
    .restart local v17    # "strlocal":Ljava/lang/String;
    :cond_7
    invoke-virtual/range {v17 .. v17}, Ljava/lang/String;->length()I

    move-result v23

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/index/poi/hwpf/model/TextPieceTable;->strBuilder:Ljava/lang/StringBuilder;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->length()I

    move-result v24

    add-int v23, v23, v24

    const/high16 v24, 0x100000

    move/from16 v0, v23

    move/from16 v1, v24

    if-lt v0, v1, :cond_8

    .line 166
    new-instance v23, Ljava/lang/StringBuilder;

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/index/poi/hwpf/model/TextPieceTable;->strBuilder:Ljava/lang/StringBuilder;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v24 .. v24}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v24

    invoke-direct/range {v23 .. v24}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 167
    invoke-virtual/range {v17 .. v17}, Ljava/lang/String;->length()I

    move-result v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v23

    .line 166
    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    invoke-interface {v0, v1}, Lcom/samsung/index/ITextContentObs;->updateText(Ljava/lang/String;)V

    .line 168
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/index/poi/hwpf/model/TextPieceTable;->strBuilder:Ljava/lang/StringBuilder;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->setLength(I)V

    goto :goto_3

    .line 170
    :cond_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/index/poi/hwpf/model/TextPieceTable;->strBuilder:Ljava/lang/StringBuilder;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 179
    .end local v4    # "buf":[B
    .end local v17    # "strlocal":Ljava/lang/String;
    :cond_9
    sub-int v14, v15, v16

    .line 180
    .local v14, "sizeToSkip":I
    int-to-long v0, v14

    move-wide/from16 v24, v0

    :try_start_0
    move-object/from16 v0, p2

    move-wide/from16 v1, v24

    invoke-virtual {v0, v1, v2}, Lorg/apache/index/poi/poifs/filesystem/DocumentInputStream;->skip(J)J

    .line 182
    move/from16 v20, v18

    .line 184
    .local v20, "totalSize":I
    const/4 v13, 0x0

    .line 185
    .local v13, "sizeToRead":I
    const/high16 v23, 0x100000

    move/from16 v0, v20

    move/from16 v1, v23

    if-le v0, v1, :cond_a

    .line 186
    const/high16 v13, 0x100000

    .line 191
    :goto_4
    new-array v4, v13, [B

    .line 192
    .restart local v4    # "buf":[B
    :goto_5
    if-gtz v13, :cond_b

    .line 228
    add-int v16, v15, v18

    goto :goto_3

    .line 188
    .end local v4    # "buf":[B
    :cond_a
    move/from16 v13, v20

    goto :goto_4

    .line 196
    .restart local v4    # "buf":[B
    :cond_b
    const/16 v23, 0x0

    move-object/from16 v0, p2

    move/from16 v1, v23

    invoke-virtual {v0, v4, v1, v13}, Lorg/apache/index/poi/poifs/filesystem/DocumentInputStream;->readFully([BII)V

    .line 202
    aget-object v23, v12, v22

    move-object/from16 v0, v23

    invoke-static {v4, v0}, Lorg/apache/index/poi/hwpf/model/TextPieceTable;->buildInitSBInTable([BLorg/apache/index/poi/hwpf/model/PieceDescriptor;)Ljava/lang/String;

    move-result-object v17

    .line 204
    .restart local v17    # "strlocal":Ljava/lang/String;
    if-eqz v17, :cond_c

    .line 205
    invoke-static/range {v17 .. v17}, Lorg/apache/index/poi/hwpf/model/TextPieceTable;->stripFields(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    .line 211
    :cond_c
    if-eqz p1, :cond_d

    .line 212
    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-interface {v0, v1}, Lcom/samsung/index/ITextContentObs;->updateText(Ljava/lang/String;)V

    .line 216
    :cond_d
    sub-int v20, v20, v13

    .line 218
    const/high16 v23, 0x100000

    move/from16 v0, v20

    move/from16 v1, v23

    if-le v0, v1, :cond_e

    .line 219
    const/high16 v13, 0x100000

    .line 220
    goto :goto_5

    .line 221
    :cond_e
    move/from16 v13, v20

    .line 222
    new-array v4, v13, [B
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_5

    .line 230
    .end local v4    # "buf":[B
    .end local v13    # "sizeToRead":I
    .end local v17    # "strlocal":Ljava/lang/String;
    .end local v20    # "totalSize":I
    :catch_0
    move-exception v5

    .line 231
    .local v5, "e":Ljava/io/IOException;
    const-string/jumbo v23, "DocumentService"

    new-instance v24, Ljava/lang/StringBuilder;

    const-string/jumbo v25, "Exception :"

    invoke-direct/range {v24 .. v25}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v23 .. v24}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3
.end method

.method private static buildInitSBInTable([BLorg/apache/index/poi/hwpf/model/PieceDescriptor;)Ljava/lang/String;
    .locals 4
    .param p0, "text"    # [B
    .param p1, "pd"    # Lorg/apache/index/poi/hwpf/model/PieceDescriptor;

    .prologue
    .line 73
    const/4 v1, 0x0

    .line 75
    .local v1, "str":Ljava/lang/String;
    :try_start_0
    invoke-virtual {p1}, Lorg/apache/index/poi/hwpf/model/PieceDescriptor;->isUnicode()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 76
    new-instance v1, Ljava/lang/String;

    .end local v1    # "str":Ljava/lang/String;
    const-string/jumbo v2, "UTF-16LE"

    invoke-direct {v1, p0, v2}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    .line 85
    .restart local v1    # "str":Ljava/lang/String;
    :goto_0
    return-object v1

    .line 78
    :cond_0
    new-instance v1, Ljava/lang/String;

    .end local v1    # "str":Ljava/lang/String;
    const-string/jumbo v2, "Cp1252"

    invoke-direct {v1, p0, v2}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .restart local v1    # "str":Ljava/lang/String;
    goto :goto_0

    .line 81
    .end local v1    # "str":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 82
    .local v0, "e":Ljava/io/UnsupportedEncodingException;
    new-instance v2, Ljava/lang/RuntimeException;

    const-string/jumbo v3, "Your Java is broken! It doesn\'t know about basic, required character encodings!"

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method private static stripFields(Ljava/lang/String;)Ljava/lang/String;
    .locals 12
    .param p0, "text"    # Ljava/lang/String;

    .prologue
    .line 253
    const/16 v9, 0x13

    invoke-virtual {p0, v9}, Ljava/lang/String;->indexOf(I)I

    move-result v2

    .line 256
    .local v2, "index":I
    const/4 v9, -0x1

    if-ne v2, v9, :cond_0

    .line 313
    .end local p0    # "text":Ljava/lang/String;
    :goto_0
    return-object p0

    .line 259
    .restart local p0    # "text":Ljava/lang/String;
    :cond_0
    new-instance v7, Ljava/lang/StringBuffer;

    invoke-direct {v7}, Ljava/lang/StringBuffer;-><init>()V

    .line 263
    .local v7, "strBuf":Ljava/lang/StringBuffer;
    :goto_1
    const/4 v9, -0x1

    if-gt v2, v9, :cond_1

    .line 313
    invoke-virtual {v7}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 264
    :cond_1
    if-eqz v2, :cond_2

    .line 265
    const/4 v9, 0x0

    invoke-virtual {p0, v9, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 267
    :cond_2
    add-int/lit8 v9, v2, 0x1

    invoke-virtual {p0, v9}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v9

    const-string/jumbo v10, " "

    const-string/jumbo v11, ""

    invoke-virtual {v9, v10, v11}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 268
    const-string/jumbo v9, "PAGEREF"

    invoke-virtual {p0, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_3

    .line 269
    const-string/jumbo v9, "SEQ"

    invoke-virtual {p0, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_3

    const-string/jumbo v9, "PAGE"

    invoke-virtual {p0, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 270
    :cond_3
    const/16 v9, 0x15

    invoke-virtual {p0, v9}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    .line 271
    .local v0, "endIndex":I
    const/4 v9, -0x1

    if-eq v0, v9, :cond_4

    .line 272
    add-int/lit8 v9, v0, 0x1

    invoke-virtual {p0, v9}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p0

    .line 301
    .end local v0    # "endIndex":I
    :cond_4
    :goto_2
    const/16 v9, 0x13

    invoke-virtual {p0, v9}, Ljava/lang/String;->indexOf(I)I

    move-result v2

    .line 303
    const/4 v9, -0x1

    if-le v2, v9, :cond_8

    .line 304
    const/4 v9, 0x0

    invoke-virtual {p0, v9, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 305
    invoke-virtual {p0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p0

    .line 306
    const/16 v9, 0x13

    invoke-virtual {p0, v9}, Ljava/lang/String;->indexOf(I)I

    move-result v2

    .line 307
    goto :goto_1

    .line 274
    :cond_5
    const-string/jumbo v9, "HYPERLINK"

    invoke-virtual {p0, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_7

    .line 275
    const/16 v9, 0x15

    invoke-virtual {p0, v9}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    .line 276
    .restart local v0    # "endIndex":I
    const/16 v9, 0x14

    invoke-virtual {p0, v9}, Ljava/lang/String;->indexOf(I)I

    move-result v4

    .line 278
    .local v4, "last":I
    const/4 v9, -0x1

    if-eq v0, v9, :cond_4

    const/4 v9, -0x1

    if-eq v4, v9, :cond_4

    .line 279
    add-int/lit8 v9, v4, 0x1

    invoke-virtual {p0, v9, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    .line 282
    .local v8, "urlText":Ljava/lang/String;
    const/16 v9, 0x13

    invoke-virtual {p0, v9}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    .line 283
    .local v1, "ind":I
    const-string/jumbo v9, "\\h"

    invoke-virtual {v8, v9}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v5

    .line 284
    .local v5, "pageref":I
    const-string/jumbo v9, "PAGEREF"

    invoke-virtual {v8, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_6

    const/4 v9, -0x1

    if-eq v1, v9, :cond_6

    const/4 v9, -0x1

    if-eq v5, v9, :cond_6

    .line 285
    add-int/lit8 v9, v5, 0x2

    invoke-virtual {v8, v9}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v6

    .line 287
    .local v6, "refNo":Ljava/lang/String;
    add-int/lit8 v9, v4, 0x1

    invoke-virtual {p0, v9, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    .line 288
    invoke-virtual {v8, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 290
    .end local v6    # "refNo":Ljava/lang/String;
    :cond_6
    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 291
    add-int/lit8 v9, v0, 0x1

    invoke-virtual {p0, v9}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p0

    .line 293
    goto :goto_2

    .end local v0    # "endIndex":I
    .end local v1    # "ind":I
    .end local v4    # "last":I
    .end local v5    # "pageref":I
    .end local v8    # "urlText":Ljava/lang/String;
    :cond_7
    const-string/jumbo v9, "TOC"

    invoke-virtual {p0, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 296
    const/16 v9, 0x13

    invoke-virtual {p0, v9}, Ljava/lang/String;->indexOf(I)I

    move-result v3

    .line 297
    .local v3, "index1":I
    const/4 v9, -0x1

    if-eq v3, v9, :cond_4

    .line 298
    invoke-virtual {p0, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p0

    goto/16 :goto_2

    .line 308
    .end local v3    # "index1":I
    :cond_8
    invoke-virtual {v7, p0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto/16 :goto_1
.end method


# virtual methods
.method public add(Lorg/apache/index/poi/hwpf/model/TextPiece;)V
    .locals 3
    .param p1, "piece"    # Lorg/apache/index/poi/hwpf/model/TextPiece;

    .prologue
    .line 356
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/TextPieceTable;->_textPieces:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 357
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/TextPieceTable;->_textPiecesFCOrder:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 358
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/TextPieceTable;->_textPieces:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 359
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/TextPieceTable;->_textPiecesFCOrder:Ljava/util/ArrayList;

    new-instance v1, Lorg/apache/index/poi/hwpf/model/TextPieceTable$FCComparator;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lorg/apache/index/poi/hwpf/model/TextPieceTable$FCComparator;-><init>(Lorg/apache/index/poi/hwpf/model/TextPieceTable$FCComparator;)V

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 360
    return-void
.end method

.method public adjustForInsert(II)I
    .locals 4
    .param p1, "listIndex"    # I
    .param p2, "length"    # I

    .prologue
    .line 371
    iget-object v3, p0, Lorg/apache/index/poi/hwpf/model/TextPieceTable;->_textPieces:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 373
    .local v0, "size":I
    iget-object v3, p0, Lorg/apache/index/poi/hwpf/model/TextPieceTable;->_textPieces:Ljava/util/ArrayList;

    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/index/poi/hwpf/model/TextPiece;

    .line 376
    .local v1, "tp":Lorg/apache/index/poi/hwpf/model/TextPiece;
    invoke-virtual {v1}, Lorg/apache/index/poi/hwpf/model/TextPiece;->getEnd()I

    move-result v3

    add-int/2addr v3, p2

    invoke-virtual {v1, v3}, Lorg/apache/index/poi/hwpf/model/TextPiece;->setEnd(I)V

    .line 379
    add-int/lit8 v2, p1, 0x1

    .local v2, "x":I
    :goto_0
    if-lt v2, v0, :cond_0

    .line 386
    return p2

    .line 380
    :cond_0
    iget-object v3, p0, Lorg/apache/index/poi/hwpf/model/TextPieceTable;->_textPieces:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "tp":Lorg/apache/index/poi/hwpf/model/TextPiece;
    check-cast v1, Lorg/apache/index/poi/hwpf/model/TextPiece;

    .line 381
    .restart local v1    # "tp":Lorg/apache/index/poi/hwpf/model/TextPiece;
    invoke-virtual {v1}, Lorg/apache/index/poi/hwpf/model/TextPiece;->getStart()I

    move-result v3

    add-int/2addr v3, p2

    invoke-virtual {v1, v3}, Lorg/apache/index/poi/hwpf/model/TextPiece;->setStart(I)V

    .line 382
    invoke-virtual {v1}, Lorg/apache/index/poi/hwpf/model/TextPiece;->getEnd()I

    move-result v3

    add-int/2addr v3, p2

    invoke-virtual {v1, v3}, Lorg/apache/index/poi/hwpf/model/TextPiece;->setEnd(I)V

    .line 379
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v4, 0x0

    .line 390
    if-nez p1, :cond_0

    move v3, v4

    .line 410
    :goto_0
    return v3

    .line 395
    :cond_0
    instance-of v3, p1, Lorg/apache/index/poi/hwpf/model/TextPieceTable;

    if-eqz v3, :cond_3

    move-object v1, p1

    .line 397
    check-cast v1, Lorg/apache/index/poi/hwpf/model/TextPieceTable;

    .line 399
    .local v1, "tpt":Lorg/apache/index/poi/hwpf/model/TextPieceTable;
    iget-object v3, v1, Lorg/apache/index/poi/hwpf/model/TextPieceTable;->_textPieces:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 400
    .local v0, "size":I
    iget-object v3, p0, Lorg/apache/index/poi/hwpf/model/TextPieceTable;->_textPieces:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ne v0, v3, :cond_3

    .line 401
    const/4 v2, 0x0

    .local v2, "x":I
    :goto_1
    if-lt v2, v0, :cond_1

    .line 406
    const/4 v3, 0x1

    goto :goto_0

    .line 402
    :cond_1
    iget-object v3, v1, Lorg/apache/index/poi/hwpf/model/TextPieceTable;->_textPieces:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/index/poi/hwpf/model/TextPiece;

    iget-object v5, p0, Lorg/apache/index/poi/hwpf/model/TextPieceTable;->_textPieces:Ljava/util/ArrayList;

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v3, v5}, Lorg/apache/index/poi/hwpf/model/TextPiece;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    move v3, v4

    .line 403
    goto :goto_0

    .line 401
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .end local v0    # "size":I
    .end local v1    # "tpt":Lorg/apache/index/poi/hwpf/model/TextPieceTable;
    .end local v2    # "x":I
    :cond_3
    move v3, v4

    .line 410
    goto :goto_0
.end method

.method public getCharIndex(I)I
    .locals 1
    .param p1, "bytePos"    # I

    .prologue
    .line 414
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lorg/apache/index/poi/hwpf/model/TextPieceTable;->getCharIndex(II)I

    move-result v0

    return v0
.end method

.method public getCharIndex(II)I
    .locals 8
    .param p1, "bytePos"    # I
    .param p2, "startCP"    # I

    .prologue
    .line 418
    const/4 v1, 0x0

    .line 420
    .local v1, "charCount":I
    invoke-virtual {p0, p1}, Lorg/apache/index/poi/hwpf/model/TextPieceTable;->lookIndexForward(I)I

    move-result p1

    .line 422
    iget-object v6, p0, Lorg/apache/index/poi/hwpf/model/TextPieceTable;->_textPieces:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-nez v7, :cond_1

    .line 449
    :goto_0
    return v1

    .line 422
    :cond_1
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/apache/index/poi/hwpf/model/TextPiece;

    .line 423
    .local v5, "tp":Lorg/apache/index/poi/hwpf/model/TextPiece;
    invoke-virtual {v5}, Lorg/apache/index/poi/hwpf/model/TextPiece;->getPieceDescriptor()Lorg/apache/index/poi/hwpf/model/PieceDescriptor;

    move-result-object v7

    invoke-virtual {v7}, Lorg/apache/index/poi/hwpf/model/PieceDescriptor;->getFilePosition()I

    move-result v3

    .line 425
    .local v3, "pieceStart":I
    invoke-virtual {v5}, Lorg/apache/index/poi/hwpf/model/TextPiece;->bytesLength()I

    move-result v0

    .line 426
    .local v0, "bytesLength":I
    add-int v2, v3, v0

    .line 430
    .local v2, "pieceEnd":I
    if-lt p1, v3, :cond_2

    if-le p1, v2, :cond_3

    .line 431
    :cond_2
    move v4, v0

    .line 438
    .local v4, "toAdd":I
    :goto_1
    invoke-virtual {v5}, Lorg/apache/index/poi/hwpf/model/TextPiece;->isUnicode()Z

    move-result v7

    if-eqz v7, :cond_5

    .line 439
    div-int/lit8 v7, v4, 0x2

    add-int/2addr v1, v7

    .line 444
    :goto_2
    if-lt p1, v3, :cond_0

    if-gt p1, v2, :cond_0

    if-lt v1, p2, :cond_0

    goto :goto_0

    .line 432
    .end local v4    # "toAdd":I
    :cond_3
    if-le p1, v3, :cond_4

    if-ge p1, v2, :cond_4

    .line 433
    sub-int v4, p1, v3

    .line 434
    .restart local v4    # "toAdd":I
    goto :goto_1

    .line 435
    .end local v4    # "toAdd":I
    :cond_4
    sub-int v7, v2, p1

    sub-int v4, v0, v7

    .restart local v4    # "toAdd":I
    goto :goto_1

    .line 441
    :cond_5
    add-int/2addr v1, v4

    goto :goto_2
.end method

.method public getCpMin()I
    .locals 1

    .prologue
    .line 344
    iget v0, p0, Lorg/apache/index/poi/hwpf/model/TextPieceTable;->_cpMin:I

    return v0
.end method

.method public getText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 352
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/TextPieceTable;->strBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getTextPieces()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lorg/apache/index/poi/hwpf/model/TextPiece;",
            ">;"
        }
    .end annotation

    .prologue
    .line 348
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/TextPieceTable;->_textPieces:Ljava/util/ArrayList;

    return-object v0
.end method

.method public isIndexInTable(I)Z
    .locals 5
    .param p1, "bytePos"    # I

    .prologue
    const/4 v2, 0x0

    .line 491
    iget-object v3, p0, Lorg/apache/index/poi/hwpf/model/TextPieceTable;->_textPiecesFCOrder:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_2

    .line 505
    :cond_1
    :goto_0
    return v2

    .line 491
    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/index/poi/hwpf/model/TextPiece;

    .line 492
    .local v1, "tp":Lorg/apache/index/poi/hwpf/model/TextPiece;
    invoke-virtual {v1}, Lorg/apache/index/poi/hwpf/model/TextPiece;->getPieceDescriptor()Lorg/apache/index/poi/hwpf/model/PieceDescriptor;

    move-result-object v4

    invoke-virtual {v4}, Lorg/apache/index/poi/hwpf/model/PieceDescriptor;->getFilePosition()I

    move-result v0

    .line 494
    .local v0, "pieceStart":I
    invoke-virtual {v1}, Lorg/apache/index/poi/hwpf/model/TextPiece;->bytesLength()I

    move-result v4

    add-int/2addr v4, v0

    if-gt p1, v4, :cond_0

    .line 498
    if-gt v0, p1, :cond_1

    .line 502
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public lookIndexBackward(I)I
    .locals 5
    .param p1, "bytePos"    # I

    .prologue
    .line 470
    const/4 v0, 0x0

    .line 472
    .local v0, "lastEnd":I
    iget-object v3, p0, Lorg/apache/index/poi/hwpf/model/TextPieceTable;->_textPiecesFCOrder:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_1

    .line 487
    :cond_0
    :goto_1
    return p1

    .line 472
    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/index/poi/hwpf/model/TextPiece;

    .line 473
    .local v2, "tp":Lorg/apache/index/poi/hwpf/model/TextPiece;
    invoke-virtual {v2}, Lorg/apache/index/poi/hwpf/model/TextPiece;->getPieceDescriptor()Lorg/apache/index/poi/hwpf/model/PieceDescriptor;

    move-result-object v4

    invoke-virtual {v4}, Lorg/apache/index/poi/hwpf/model/PieceDescriptor;->getFilePosition()I

    move-result v1

    .line 475
    .local v1, "pieceStart":I
    invoke-virtual {v2}, Lorg/apache/index/poi/hwpf/model/TextPiece;->bytesLength()I

    move-result v4

    add-int/2addr v4, v1

    if-le p1, v4, :cond_2

    .line 476
    invoke-virtual {v2}, Lorg/apache/index/poi/hwpf/model/TextPiece;->bytesLength()I

    move-result v4

    add-int v0, v1, v4

    .line 477
    goto :goto_0

    .line 480
    :cond_2
    if-le v1, p1, :cond_0

    .line 481
    move p1, v0

    .line 484
    goto :goto_1
.end method

.method public lookIndexForward(I)I
    .locals 4
    .param p1, "bytePos"    # I

    .prologue
    .line 453
    iget-object v2, p0, Lorg/apache/index/poi/hwpf/model/TextPieceTable;->_textPiecesFCOrder:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_2

    .line 466
    :cond_1
    :goto_0
    return p1

    .line 453
    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/index/poi/hwpf/model/TextPiece;

    .line 454
    .local v1, "tp":Lorg/apache/index/poi/hwpf/model/TextPiece;
    invoke-virtual {v1}, Lorg/apache/index/poi/hwpf/model/TextPiece;->getPieceDescriptor()Lorg/apache/index/poi/hwpf/model/PieceDescriptor;

    move-result-object v3

    invoke-virtual {v3}, Lorg/apache/index/poi/hwpf/model/PieceDescriptor;->getFilePosition()I

    move-result v0

    .line 456
    .local v0, "pieceStart":I
    invoke-virtual {v1}, Lorg/apache/index/poi/hwpf/model/TextPiece;->bytesLength()I

    move-result v3

    add-int/2addr v3, v0

    if-gt p1, v3, :cond_0

    .line 460
    if-le v0, p1, :cond_1

    .line 461
    move p1, v0

    .line 464
    goto :goto_0
.end method
