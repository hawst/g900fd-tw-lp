.class public abstract Lorg/apache/index/poi/hwpf/model/types/SEPAbstractType;
.super Ljava/lang/Object;
.source "SEPAbstractType.java"

# interfaces
.implements Lorg/apache/index/poi/hwpf/model/HDFType;


# instance fields
.field protected field_10_grpfIhdt:B

.field protected field_11_nLnnMod:I

.field protected field_12_dxaLnn:I

.field protected field_13_dxaPgn:I

.field protected field_14_dyaPgn:I

.field protected field_15_fLBetween:Z

.field protected field_16_vjc:B

.field protected field_17_dmBinFirst:I

.field protected field_18_dmBinOther:I

.field protected field_19_dmPaperReq:I

.field protected field_1_bkc:B

.field protected field_20_brcTop:Lorg/apache/index/poi/hwpf/usermodel/BorderCode;

.field protected field_21_brcLeft:Lorg/apache/index/poi/hwpf/usermodel/BorderCode;

.field protected field_22_brcBottom:Lorg/apache/index/poi/hwpf/usermodel/BorderCode;

.field protected field_23_brcRight:Lorg/apache/index/poi/hwpf/usermodel/BorderCode;

.field protected field_24_fPropMark:Z

.field protected field_25_ibstPropRMark:I

.field protected field_26_dttmPropRMark:Lorg/apache/index/poi/hwpf/usermodel/DateAndTime;

.field protected field_27_dxtCharSpace:I

.field protected field_28_dyaLinePitch:I

.field protected field_29_clm:I

.field protected field_2_fTitlePage:Z

.field protected field_30_unused2:I

.field protected field_31_dmOrientPage:B

.field protected field_32_iHeadingPgn:B

.field protected field_33_pgnStart:I

.field protected field_34_lnnMin:I

.field protected field_35_wTextFlow:I

.field protected field_36_unused3:S

.field protected field_37_pgbProp:I

.field protected field_38_unused4:S

.field protected field_39_xaPage:I

.field protected field_3_fAutoPgn:Z

.field protected field_40_yaPage:I

.field protected field_41_xaPageNUp:I

.field protected field_42_yaPageNUp:I

.field protected field_43_dxaLeft:I

.field protected field_44_dxaRight:I

.field protected field_45_dyaTop:I

.field protected field_46_dyaBottom:I

.field protected field_47_dzaGutter:I

.field protected field_48_dyaHdrTop:I

.field protected field_49_dyaHdrBottom:I

.field protected field_4_nfcPgn:B

.field protected field_50_ccolM1:I

.field protected field_51_fEvenlySpaced:Z

.field protected field_52_unused5:B

.field protected field_53_dxaColumns:I

.field protected field_54_rgdxaColumn:[I

.field protected field_55_dxaColumnWidth:I

.field protected field_56_dmOrientFirst:B

.field protected field_57_fLayout:B

.field protected field_58_unused6:S

.field protected field_59_olstAnm:[B

.field protected field_5_fUnlocked:Z

.field protected field_6_cnsPgn:B

.field protected field_7_fPgnRestart:Z

.field protected field_8_fEndNote:Z

.field protected field_9_lnc:B


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 95
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 98
    return-void
.end method


# virtual methods
.method public getBkc()B
    .locals 1

    .prologue
    .line 115
    iget-byte v0, p0, Lorg/apache/index/poi/hwpf/model/types/SEPAbstractType;->field_1_bkc:B

    return v0
.end method

.method public getBrcBottom()Lorg/apache/index/poi/hwpf/usermodel/BorderCode;
    .locals 1

    .prologue
    .line 451
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/types/SEPAbstractType;->field_22_brcBottom:Lorg/apache/index/poi/hwpf/usermodel/BorderCode;

    return-object v0
.end method

.method public getBrcLeft()Lorg/apache/index/poi/hwpf/usermodel/BorderCode;
    .locals 1

    .prologue
    .line 435
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/types/SEPAbstractType;->field_21_brcLeft:Lorg/apache/index/poi/hwpf/usermodel/BorderCode;

    return-object v0
.end method

.method public getBrcRight()Lorg/apache/index/poi/hwpf/usermodel/BorderCode;
    .locals 1

    .prologue
    .line 467
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/types/SEPAbstractType;->field_23_brcRight:Lorg/apache/index/poi/hwpf/usermodel/BorderCode;

    return-object v0
.end method

.method public getBrcTop()Lorg/apache/index/poi/hwpf/usermodel/BorderCode;
    .locals 1

    .prologue
    .line 419
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/types/SEPAbstractType;->field_20_brcTop:Lorg/apache/index/poi/hwpf/usermodel/BorderCode;

    return-object v0
.end method

.method public getCcolM1()I
    .locals 1

    .prologue
    .line 899
    iget v0, p0, Lorg/apache/index/poi/hwpf/model/types/SEPAbstractType;->field_50_ccolM1:I

    return v0
.end method

.method public getClm()I
    .locals 1

    .prologue
    .line 563
    iget v0, p0, Lorg/apache/index/poi/hwpf/model/types/SEPAbstractType;->field_29_clm:I

    return v0
.end method

.method public getCnsPgn()B
    .locals 1

    .prologue
    .line 195
    iget-byte v0, p0, Lorg/apache/index/poi/hwpf/model/types/SEPAbstractType;->field_6_cnsPgn:B

    return v0
.end method

.method public getDmBinFirst()I
    .locals 1

    .prologue
    .line 371
    iget v0, p0, Lorg/apache/index/poi/hwpf/model/types/SEPAbstractType;->field_17_dmBinFirst:I

    return v0
.end method

.method public getDmBinOther()I
    .locals 1

    .prologue
    .line 387
    iget v0, p0, Lorg/apache/index/poi/hwpf/model/types/SEPAbstractType;->field_18_dmBinOther:I

    return v0
.end method

.method public getDmOrientFirst()B
    .locals 1

    .prologue
    .line 995
    iget-byte v0, p0, Lorg/apache/index/poi/hwpf/model/types/SEPAbstractType;->field_56_dmOrientFirst:B

    return v0
.end method

.method public getDmOrientPage()B
    .locals 1

    .prologue
    .line 595
    iget-byte v0, p0, Lorg/apache/index/poi/hwpf/model/types/SEPAbstractType;->field_31_dmOrientPage:B

    return v0
.end method

.method public getDmPaperReq()I
    .locals 1

    .prologue
    .line 403
    iget v0, p0, Lorg/apache/index/poi/hwpf/model/types/SEPAbstractType;->field_19_dmPaperReq:I

    return v0
.end method

.method public getDttmPropRMark()Lorg/apache/index/poi/hwpf/usermodel/DateAndTime;
    .locals 1

    .prologue
    .line 515
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/types/SEPAbstractType;->field_26_dttmPropRMark:Lorg/apache/index/poi/hwpf/usermodel/DateAndTime;

    return-object v0
.end method

.method public getDxaColumnWidth()I
    .locals 1

    .prologue
    .line 979
    iget v0, p0, Lorg/apache/index/poi/hwpf/model/types/SEPAbstractType;->field_55_dxaColumnWidth:I

    return v0
.end method

.method public getDxaColumns()I
    .locals 1

    .prologue
    .line 947
    iget v0, p0, Lorg/apache/index/poi/hwpf/model/types/SEPAbstractType;->field_53_dxaColumns:I

    return v0
.end method

.method public getDxaLeft()I
    .locals 1

    .prologue
    .line 787
    iget v0, p0, Lorg/apache/index/poi/hwpf/model/types/SEPAbstractType;->field_43_dxaLeft:I

    return v0
.end method

.method public getDxaLnn()I
    .locals 1

    .prologue
    .line 291
    iget v0, p0, Lorg/apache/index/poi/hwpf/model/types/SEPAbstractType;->field_12_dxaLnn:I

    return v0
.end method

.method public getDxaPgn()I
    .locals 1

    .prologue
    .line 307
    iget v0, p0, Lorg/apache/index/poi/hwpf/model/types/SEPAbstractType;->field_13_dxaPgn:I

    return v0
.end method

.method public getDxaRight()I
    .locals 1

    .prologue
    .line 803
    iget v0, p0, Lorg/apache/index/poi/hwpf/model/types/SEPAbstractType;->field_44_dxaRight:I

    return v0
.end method

.method public getDxtCharSpace()I
    .locals 1

    .prologue
    .line 531
    iget v0, p0, Lorg/apache/index/poi/hwpf/model/types/SEPAbstractType;->field_27_dxtCharSpace:I

    return v0
.end method

.method public getDyaBottom()I
    .locals 1

    .prologue
    .line 835
    iget v0, p0, Lorg/apache/index/poi/hwpf/model/types/SEPAbstractType;->field_46_dyaBottom:I

    return v0
.end method

.method public getDyaHdrBottom()I
    .locals 1

    .prologue
    .line 883
    iget v0, p0, Lorg/apache/index/poi/hwpf/model/types/SEPAbstractType;->field_49_dyaHdrBottom:I

    return v0
.end method

.method public getDyaHdrTop()I
    .locals 1

    .prologue
    .line 867
    iget v0, p0, Lorg/apache/index/poi/hwpf/model/types/SEPAbstractType;->field_48_dyaHdrTop:I

    return v0
.end method

.method public getDyaLinePitch()I
    .locals 1

    .prologue
    .line 547
    iget v0, p0, Lorg/apache/index/poi/hwpf/model/types/SEPAbstractType;->field_28_dyaLinePitch:I

    return v0
.end method

.method public getDyaPgn()I
    .locals 1

    .prologue
    .line 323
    iget v0, p0, Lorg/apache/index/poi/hwpf/model/types/SEPAbstractType;->field_14_dyaPgn:I

    return v0
.end method

.method public getDyaTop()I
    .locals 1

    .prologue
    .line 819
    iget v0, p0, Lorg/apache/index/poi/hwpf/model/types/SEPAbstractType;->field_45_dyaTop:I

    return v0
.end method

.method public getDzaGutter()I
    .locals 1

    .prologue
    .line 851
    iget v0, p0, Lorg/apache/index/poi/hwpf/model/types/SEPAbstractType;->field_47_dzaGutter:I

    return v0
.end method

.method public getFAutoPgn()Z
    .locals 1

    .prologue
    .line 147
    iget-boolean v0, p0, Lorg/apache/index/poi/hwpf/model/types/SEPAbstractType;->field_3_fAutoPgn:Z

    return v0
.end method

.method public getFEndNote()Z
    .locals 1

    .prologue
    .line 227
    iget-boolean v0, p0, Lorg/apache/index/poi/hwpf/model/types/SEPAbstractType;->field_8_fEndNote:Z

    return v0
.end method

.method public getFEvenlySpaced()Z
    .locals 1

    .prologue
    .line 915
    iget-boolean v0, p0, Lorg/apache/index/poi/hwpf/model/types/SEPAbstractType;->field_51_fEvenlySpaced:Z

    return v0
.end method

.method public getFLBetween()Z
    .locals 1

    .prologue
    .line 339
    iget-boolean v0, p0, Lorg/apache/index/poi/hwpf/model/types/SEPAbstractType;->field_15_fLBetween:Z

    return v0
.end method

.method public getFLayout()B
    .locals 1

    .prologue
    .line 1011
    iget-byte v0, p0, Lorg/apache/index/poi/hwpf/model/types/SEPAbstractType;->field_57_fLayout:B

    return v0
.end method

.method public getFPgnRestart()Z
    .locals 1

    .prologue
    .line 211
    iget-boolean v0, p0, Lorg/apache/index/poi/hwpf/model/types/SEPAbstractType;->field_7_fPgnRestart:Z

    return v0
.end method

.method public getFPropMark()Z
    .locals 1

    .prologue
    .line 483
    iget-boolean v0, p0, Lorg/apache/index/poi/hwpf/model/types/SEPAbstractType;->field_24_fPropMark:Z

    return v0
.end method

.method public getFTitlePage()Z
    .locals 1

    .prologue
    .line 131
    iget-boolean v0, p0, Lorg/apache/index/poi/hwpf/model/types/SEPAbstractType;->field_2_fTitlePage:Z

    return v0
.end method

.method public getFUnlocked()Z
    .locals 1

    .prologue
    .line 179
    iget-boolean v0, p0, Lorg/apache/index/poi/hwpf/model/types/SEPAbstractType;->field_5_fUnlocked:Z

    return v0
.end method

.method public getGrpfIhdt()B
    .locals 1

    .prologue
    .line 259
    iget-byte v0, p0, Lorg/apache/index/poi/hwpf/model/types/SEPAbstractType;->field_10_grpfIhdt:B

    return v0
.end method

.method public getIHeadingPgn()B
    .locals 1

    .prologue
    .line 611
    iget-byte v0, p0, Lorg/apache/index/poi/hwpf/model/types/SEPAbstractType;->field_32_iHeadingPgn:B

    return v0
.end method

.method public getIbstPropRMark()I
    .locals 1

    .prologue
    .line 499
    iget v0, p0, Lorg/apache/index/poi/hwpf/model/types/SEPAbstractType;->field_25_ibstPropRMark:I

    return v0
.end method

.method public getLnc()B
    .locals 1

    .prologue
    .line 243
    iget-byte v0, p0, Lorg/apache/index/poi/hwpf/model/types/SEPAbstractType;->field_9_lnc:B

    return v0
.end method

.method public getLnnMin()I
    .locals 1

    .prologue
    .line 643
    iget v0, p0, Lorg/apache/index/poi/hwpf/model/types/SEPAbstractType;->field_34_lnnMin:I

    return v0
.end method

.method public getNLnnMod()I
    .locals 1

    .prologue
    .line 275
    iget v0, p0, Lorg/apache/index/poi/hwpf/model/types/SEPAbstractType;->field_11_nLnnMod:I

    return v0
.end method

.method public getNfcPgn()B
    .locals 1

    .prologue
    .line 163
    iget-byte v0, p0, Lorg/apache/index/poi/hwpf/model/types/SEPAbstractType;->field_4_nfcPgn:B

    return v0
.end method

.method public getOlstAnm()[B
    .locals 1

    .prologue
    .line 1043
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/types/SEPAbstractType;->field_59_olstAnm:[B

    return-object v0
.end method

.method public getPgbProp()I
    .locals 1

    .prologue
    .line 691
    iget v0, p0, Lorg/apache/index/poi/hwpf/model/types/SEPAbstractType;->field_37_pgbProp:I

    return v0
.end method

.method public getPgnStart()I
    .locals 1

    .prologue
    .line 627
    iget v0, p0, Lorg/apache/index/poi/hwpf/model/types/SEPAbstractType;->field_33_pgnStart:I

    return v0
.end method

.method public getRgdxaColumn()[I
    .locals 1

    .prologue
    .line 963
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/types/SEPAbstractType;->field_54_rgdxaColumn:[I

    return-object v0
.end method

.method public getSize()I
    .locals 1

    .prologue
    .line 105
    const/16 v0, 0x2bd

    return v0
.end method

.method public getUnused2()I
    .locals 1

    .prologue
    .line 579
    iget v0, p0, Lorg/apache/index/poi/hwpf/model/types/SEPAbstractType;->field_30_unused2:I

    return v0
.end method

.method public getUnused3()S
    .locals 1

    .prologue
    .line 675
    iget-short v0, p0, Lorg/apache/index/poi/hwpf/model/types/SEPAbstractType;->field_36_unused3:S

    return v0
.end method

.method public getUnused4()S
    .locals 1

    .prologue
    .line 707
    iget-short v0, p0, Lorg/apache/index/poi/hwpf/model/types/SEPAbstractType;->field_38_unused4:S

    return v0
.end method

.method public getUnused5()B
    .locals 1

    .prologue
    .line 931
    iget-byte v0, p0, Lorg/apache/index/poi/hwpf/model/types/SEPAbstractType;->field_52_unused5:B

    return v0
.end method

.method public getUnused6()S
    .locals 1

    .prologue
    .line 1027
    iget-short v0, p0, Lorg/apache/index/poi/hwpf/model/types/SEPAbstractType;->field_58_unused6:S

    return v0
.end method

.method public getVjc()B
    .locals 1

    .prologue
    .line 355
    iget-byte v0, p0, Lorg/apache/index/poi/hwpf/model/types/SEPAbstractType;->field_16_vjc:B

    return v0
.end method

.method public getWTextFlow()I
    .locals 1

    .prologue
    .line 659
    iget v0, p0, Lorg/apache/index/poi/hwpf/model/types/SEPAbstractType;->field_35_wTextFlow:I

    return v0
.end method

.method public getXaPage()I
    .locals 1

    .prologue
    .line 723
    iget v0, p0, Lorg/apache/index/poi/hwpf/model/types/SEPAbstractType;->field_39_xaPage:I

    return v0
.end method

.method public getXaPageNUp()I
    .locals 1

    .prologue
    .line 755
    iget v0, p0, Lorg/apache/index/poi/hwpf/model/types/SEPAbstractType;->field_41_xaPageNUp:I

    return v0
.end method

.method public getYaPage()I
    .locals 1

    .prologue
    .line 739
    iget v0, p0, Lorg/apache/index/poi/hwpf/model/types/SEPAbstractType;->field_40_yaPage:I

    return v0
.end method

.method public getYaPageNUp()I
    .locals 1

    .prologue
    .line 771
    iget v0, p0, Lorg/apache/index/poi/hwpf/model/types/SEPAbstractType;->field_42_yaPageNUp:I

    return v0
.end method

.method public setBkc(B)V
    .locals 0
    .param p1, "field_1_bkc"    # B

    .prologue
    .line 123
    iput-byte p1, p0, Lorg/apache/index/poi/hwpf/model/types/SEPAbstractType;->field_1_bkc:B

    .line 124
    return-void
.end method

.method public setBrcBottom(Lorg/apache/index/poi/hwpf/usermodel/BorderCode;)V
    .locals 0
    .param p1, "field_22_brcBottom"    # Lorg/apache/index/poi/hwpf/usermodel/BorderCode;

    .prologue
    .line 459
    iput-object p1, p0, Lorg/apache/index/poi/hwpf/model/types/SEPAbstractType;->field_22_brcBottom:Lorg/apache/index/poi/hwpf/usermodel/BorderCode;

    .line 460
    return-void
.end method

.method public setBrcLeft(Lorg/apache/index/poi/hwpf/usermodel/BorderCode;)V
    .locals 0
    .param p1, "field_21_brcLeft"    # Lorg/apache/index/poi/hwpf/usermodel/BorderCode;

    .prologue
    .line 443
    iput-object p1, p0, Lorg/apache/index/poi/hwpf/model/types/SEPAbstractType;->field_21_brcLeft:Lorg/apache/index/poi/hwpf/usermodel/BorderCode;

    .line 444
    return-void
.end method

.method public setBrcRight(Lorg/apache/index/poi/hwpf/usermodel/BorderCode;)V
    .locals 0
    .param p1, "field_23_brcRight"    # Lorg/apache/index/poi/hwpf/usermodel/BorderCode;

    .prologue
    .line 475
    iput-object p1, p0, Lorg/apache/index/poi/hwpf/model/types/SEPAbstractType;->field_23_brcRight:Lorg/apache/index/poi/hwpf/usermodel/BorderCode;

    .line 476
    return-void
.end method

.method public setBrcTop(Lorg/apache/index/poi/hwpf/usermodel/BorderCode;)V
    .locals 0
    .param p1, "field_20_brcTop"    # Lorg/apache/index/poi/hwpf/usermodel/BorderCode;

    .prologue
    .line 427
    iput-object p1, p0, Lorg/apache/index/poi/hwpf/model/types/SEPAbstractType;->field_20_brcTop:Lorg/apache/index/poi/hwpf/usermodel/BorderCode;

    .line 428
    return-void
.end method

.method public setCcolM1(I)V
    .locals 0
    .param p1, "field_50_ccolM1"    # I

    .prologue
    .line 907
    iput p1, p0, Lorg/apache/index/poi/hwpf/model/types/SEPAbstractType;->field_50_ccolM1:I

    .line 908
    return-void
.end method

.method public setClm(I)V
    .locals 0
    .param p1, "field_29_clm"    # I

    .prologue
    .line 571
    iput p1, p0, Lorg/apache/index/poi/hwpf/model/types/SEPAbstractType;->field_29_clm:I

    .line 572
    return-void
.end method

.method public setCnsPgn(B)V
    .locals 0
    .param p1, "field_6_cnsPgn"    # B

    .prologue
    .line 203
    iput-byte p1, p0, Lorg/apache/index/poi/hwpf/model/types/SEPAbstractType;->field_6_cnsPgn:B

    .line 204
    return-void
.end method

.method public setDmBinFirst(I)V
    .locals 0
    .param p1, "field_17_dmBinFirst"    # I

    .prologue
    .line 379
    iput p1, p0, Lorg/apache/index/poi/hwpf/model/types/SEPAbstractType;->field_17_dmBinFirst:I

    .line 380
    return-void
.end method

.method public setDmBinOther(I)V
    .locals 0
    .param p1, "field_18_dmBinOther"    # I

    .prologue
    .line 395
    iput p1, p0, Lorg/apache/index/poi/hwpf/model/types/SEPAbstractType;->field_18_dmBinOther:I

    .line 396
    return-void
.end method

.method public setDmOrientFirst(B)V
    .locals 0
    .param p1, "field_56_dmOrientFirst"    # B

    .prologue
    .line 1003
    iput-byte p1, p0, Lorg/apache/index/poi/hwpf/model/types/SEPAbstractType;->field_56_dmOrientFirst:B

    .line 1004
    return-void
.end method

.method public setDmOrientPage(B)V
    .locals 0
    .param p1, "field_31_dmOrientPage"    # B

    .prologue
    .line 603
    iput-byte p1, p0, Lorg/apache/index/poi/hwpf/model/types/SEPAbstractType;->field_31_dmOrientPage:B

    .line 604
    return-void
.end method

.method public setDmPaperReq(I)V
    .locals 0
    .param p1, "field_19_dmPaperReq"    # I

    .prologue
    .line 411
    iput p1, p0, Lorg/apache/index/poi/hwpf/model/types/SEPAbstractType;->field_19_dmPaperReq:I

    .line 412
    return-void
.end method

.method public setDttmPropRMark(Lorg/apache/index/poi/hwpf/usermodel/DateAndTime;)V
    .locals 0
    .param p1, "field_26_dttmPropRMark"    # Lorg/apache/index/poi/hwpf/usermodel/DateAndTime;

    .prologue
    .line 523
    iput-object p1, p0, Lorg/apache/index/poi/hwpf/model/types/SEPAbstractType;->field_26_dttmPropRMark:Lorg/apache/index/poi/hwpf/usermodel/DateAndTime;

    .line 524
    return-void
.end method

.method public setDxaColumnWidth(I)V
    .locals 0
    .param p1, "field_55_dxaColumnWidth"    # I

    .prologue
    .line 987
    iput p1, p0, Lorg/apache/index/poi/hwpf/model/types/SEPAbstractType;->field_55_dxaColumnWidth:I

    .line 988
    return-void
.end method

.method public setDxaColumns(I)V
    .locals 0
    .param p1, "field_53_dxaColumns"    # I

    .prologue
    .line 955
    iput p1, p0, Lorg/apache/index/poi/hwpf/model/types/SEPAbstractType;->field_53_dxaColumns:I

    .line 956
    return-void
.end method

.method public setDxaLeft(I)V
    .locals 0
    .param p1, "field_43_dxaLeft"    # I

    .prologue
    .line 795
    iput p1, p0, Lorg/apache/index/poi/hwpf/model/types/SEPAbstractType;->field_43_dxaLeft:I

    .line 796
    return-void
.end method

.method public setDxaLnn(I)V
    .locals 0
    .param p1, "field_12_dxaLnn"    # I

    .prologue
    .line 299
    iput p1, p0, Lorg/apache/index/poi/hwpf/model/types/SEPAbstractType;->field_12_dxaLnn:I

    .line 300
    return-void
.end method

.method public setDxaPgn(I)V
    .locals 0
    .param p1, "field_13_dxaPgn"    # I

    .prologue
    .line 315
    iput p1, p0, Lorg/apache/index/poi/hwpf/model/types/SEPAbstractType;->field_13_dxaPgn:I

    .line 316
    return-void
.end method

.method public setDxaRight(I)V
    .locals 0
    .param p1, "field_44_dxaRight"    # I

    .prologue
    .line 811
    iput p1, p0, Lorg/apache/index/poi/hwpf/model/types/SEPAbstractType;->field_44_dxaRight:I

    .line 812
    return-void
.end method

.method public setDxtCharSpace(I)V
    .locals 0
    .param p1, "field_27_dxtCharSpace"    # I

    .prologue
    .line 539
    iput p1, p0, Lorg/apache/index/poi/hwpf/model/types/SEPAbstractType;->field_27_dxtCharSpace:I

    .line 540
    return-void
.end method

.method public setDyaBottom(I)V
    .locals 0
    .param p1, "field_46_dyaBottom"    # I

    .prologue
    .line 843
    iput p1, p0, Lorg/apache/index/poi/hwpf/model/types/SEPAbstractType;->field_46_dyaBottom:I

    .line 844
    return-void
.end method

.method public setDyaHdrBottom(I)V
    .locals 0
    .param p1, "field_49_dyaHdrBottom"    # I

    .prologue
    .line 891
    iput p1, p0, Lorg/apache/index/poi/hwpf/model/types/SEPAbstractType;->field_49_dyaHdrBottom:I

    .line 892
    return-void
.end method

.method public setDyaHdrTop(I)V
    .locals 0
    .param p1, "field_48_dyaHdrTop"    # I

    .prologue
    .line 875
    iput p1, p0, Lorg/apache/index/poi/hwpf/model/types/SEPAbstractType;->field_48_dyaHdrTop:I

    .line 876
    return-void
.end method

.method public setDyaLinePitch(I)V
    .locals 0
    .param p1, "field_28_dyaLinePitch"    # I

    .prologue
    .line 555
    iput p1, p0, Lorg/apache/index/poi/hwpf/model/types/SEPAbstractType;->field_28_dyaLinePitch:I

    .line 556
    return-void
.end method

.method public setDyaPgn(I)V
    .locals 0
    .param p1, "field_14_dyaPgn"    # I

    .prologue
    .line 331
    iput p1, p0, Lorg/apache/index/poi/hwpf/model/types/SEPAbstractType;->field_14_dyaPgn:I

    .line 332
    return-void
.end method

.method public setDyaTop(I)V
    .locals 0
    .param p1, "field_45_dyaTop"    # I

    .prologue
    .line 827
    iput p1, p0, Lorg/apache/index/poi/hwpf/model/types/SEPAbstractType;->field_45_dyaTop:I

    .line 828
    return-void
.end method

.method public setDzaGutter(I)V
    .locals 0
    .param p1, "field_47_dzaGutter"    # I

    .prologue
    .line 859
    iput p1, p0, Lorg/apache/index/poi/hwpf/model/types/SEPAbstractType;->field_47_dzaGutter:I

    .line 860
    return-void
.end method

.method public setFAutoPgn(Z)V
    .locals 0
    .param p1, "field_3_fAutoPgn"    # Z

    .prologue
    .line 155
    iput-boolean p1, p0, Lorg/apache/index/poi/hwpf/model/types/SEPAbstractType;->field_3_fAutoPgn:Z

    .line 156
    return-void
.end method

.method public setFEndNote(Z)V
    .locals 0
    .param p1, "field_8_fEndNote"    # Z

    .prologue
    .line 235
    iput-boolean p1, p0, Lorg/apache/index/poi/hwpf/model/types/SEPAbstractType;->field_8_fEndNote:Z

    .line 236
    return-void
.end method

.method public setFEvenlySpaced(Z)V
    .locals 0
    .param p1, "field_51_fEvenlySpaced"    # Z

    .prologue
    .line 923
    iput-boolean p1, p0, Lorg/apache/index/poi/hwpf/model/types/SEPAbstractType;->field_51_fEvenlySpaced:Z

    .line 924
    return-void
.end method

.method public setFLBetween(Z)V
    .locals 0
    .param p1, "field_15_fLBetween"    # Z

    .prologue
    .line 347
    iput-boolean p1, p0, Lorg/apache/index/poi/hwpf/model/types/SEPAbstractType;->field_15_fLBetween:Z

    .line 348
    return-void
.end method

.method public setFLayout(B)V
    .locals 0
    .param p1, "field_57_fLayout"    # B

    .prologue
    .line 1019
    iput-byte p1, p0, Lorg/apache/index/poi/hwpf/model/types/SEPAbstractType;->field_57_fLayout:B

    .line 1020
    return-void
.end method

.method public setFPgnRestart(Z)V
    .locals 0
    .param p1, "field_7_fPgnRestart"    # Z

    .prologue
    .line 219
    iput-boolean p1, p0, Lorg/apache/index/poi/hwpf/model/types/SEPAbstractType;->field_7_fPgnRestart:Z

    .line 220
    return-void
.end method

.method public setFPropMark(Z)V
    .locals 0
    .param p1, "field_24_fPropMark"    # Z

    .prologue
    .line 491
    iput-boolean p1, p0, Lorg/apache/index/poi/hwpf/model/types/SEPAbstractType;->field_24_fPropMark:Z

    .line 492
    return-void
.end method

.method public setFTitlePage(Z)V
    .locals 0
    .param p1, "field_2_fTitlePage"    # Z

    .prologue
    .line 139
    iput-boolean p1, p0, Lorg/apache/index/poi/hwpf/model/types/SEPAbstractType;->field_2_fTitlePage:Z

    .line 140
    return-void
.end method

.method public setFUnlocked(Z)V
    .locals 0
    .param p1, "field_5_fUnlocked"    # Z

    .prologue
    .line 187
    iput-boolean p1, p0, Lorg/apache/index/poi/hwpf/model/types/SEPAbstractType;->field_5_fUnlocked:Z

    .line 188
    return-void
.end method

.method public setGrpfIhdt(B)V
    .locals 0
    .param p1, "field_10_grpfIhdt"    # B

    .prologue
    .line 267
    iput-byte p1, p0, Lorg/apache/index/poi/hwpf/model/types/SEPAbstractType;->field_10_grpfIhdt:B

    .line 268
    return-void
.end method

.method public setIHeadingPgn(B)V
    .locals 0
    .param p1, "field_32_iHeadingPgn"    # B

    .prologue
    .line 619
    iput-byte p1, p0, Lorg/apache/index/poi/hwpf/model/types/SEPAbstractType;->field_32_iHeadingPgn:B

    .line 620
    return-void
.end method

.method public setIbstPropRMark(I)V
    .locals 0
    .param p1, "field_25_ibstPropRMark"    # I

    .prologue
    .line 507
    iput p1, p0, Lorg/apache/index/poi/hwpf/model/types/SEPAbstractType;->field_25_ibstPropRMark:I

    .line 508
    return-void
.end method

.method public setLnc(B)V
    .locals 0
    .param p1, "field_9_lnc"    # B

    .prologue
    .line 251
    iput-byte p1, p0, Lorg/apache/index/poi/hwpf/model/types/SEPAbstractType;->field_9_lnc:B

    .line 252
    return-void
.end method

.method public setLnnMin(I)V
    .locals 0
    .param p1, "field_34_lnnMin"    # I

    .prologue
    .line 651
    iput p1, p0, Lorg/apache/index/poi/hwpf/model/types/SEPAbstractType;->field_34_lnnMin:I

    .line 652
    return-void
.end method

.method public setNLnnMod(I)V
    .locals 0
    .param p1, "field_11_nLnnMod"    # I

    .prologue
    .line 283
    iput p1, p0, Lorg/apache/index/poi/hwpf/model/types/SEPAbstractType;->field_11_nLnnMod:I

    .line 284
    return-void
.end method

.method public setNfcPgn(B)V
    .locals 0
    .param p1, "field_4_nfcPgn"    # B

    .prologue
    .line 171
    iput-byte p1, p0, Lorg/apache/index/poi/hwpf/model/types/SEPAbstractType;->field_4_nfcPgn:B

    .line 172
    return-void
.end method

.method public setOlstAnm([B)V
    .locals 0
    .param p1, "field_59_olstAnm"    # [B

    .prologue
    .line 1051
    iput-object p1, p0, Lorg/apache/index/poi/hwpf/model/types/SEPAbstractType;->field_59_olstAnm:[B

    .line 1052
    return-void
.end method

.method public setPgbProp(I)V
    .locals 0
    .param p1, "field_37_pgbProp"    # I

    .prologue
    .line 699
    iput p1, p0, Lorg/apache/index/poi/hwpf/model/types/SEPAbstractType;->field_37_pgbProp:I

    .line 700
    return-void
.end method

.method public setPgnStart(I)V
    .locals 0
    .param p1, "field_33_pgnStart"    # I

    .prologue
    .line 635
    iput p1, p0, Lorg/apache/index/poi/hwpf/model/types/SEPAbstractType;->field_33_pgnStart:I

    .line 636
    return-void
.end method

.method public setRgdxaColumn([I)V
    .locals 0
    .param p1, "field_54_rgdxaColumn"    # [I

    .prologue
    .line 971
    iput-object p1, p0, Lorg/apache/index/poi/hwpf/model/types/SEPAbstractType;->field_54_rgdxaColumn:[I

    .line 972
    return-void
.end method

.method public setUnused2(I)V
    .locals 0
    .param p1, "field_30_unused2"    # I

    .prologue
    .line 587
    iput p1, p0, Lorg/apache/index/poi/hwpf/model/types/SEPAbstractType;->field_30_unused2:I

    .line 588
    return-void
.end method

.method public setUnused3(S)V
    .locals 0
    .param p1, "field_36_unused3"    # S

    .prologue
    .line 683
    iput-short p1, p0, Lorg/apache/index/poi/hwpf/model/types/SEPAbstractType;->field_36_unused3:S

    .line 684
    return-void
.end method

.method public setUnused4(S)V
    .locals 0
    .param p1, "field_38_unused4"    # S

    .prologue
    .line 715
    iput-short p1, p0, Lorg/apache/index/poi/hwpf/model/types/SEPAbstractType;->field_38_unused4:S

    .line 716
    return-void
.end method

.method public setUnused5(B)V
    .locals 0
    .param p1, "field_52_unused5"    # B

    .prologue
    .line 939
    iput-byte p1, p0, Lorg/apache/index/poi/hwpf/model/types/SEPAbstractType;->field_52_unused5:B

    .line 940
    return-void
.end method

.method public setUnused6(S)V
    .locals 0
    .param p1, "field_58_unused6"    # S

    .prologue
    .line 1035
    iput-short p1, p0, Lorg/apache/index/poi/hwpf/model/types/SEPAbstractType;->field_58_unused6:S

    .line 1036
    return-void
.end method

.method public setVjc(B)V
    .locals 0
    .param p1, "field_16_vjc"    # B

    .prologue
    .line 363
    iput-byte p1, p0, Lorg/apache/index/poi/hwpf/model/types/SEPAbstractType;->field_16_vjc:B

    .line 364
    return-void
.end method

.method public setWTextFlow(I)V
    .locals 0
    .param p1, "field_35_wTextFlow"    # I

    .prologue
    .line 667
    iput p1, p0, Lorg/apache/index/poi/hwpf/model/types/SEPAbstractType;->field_35_wTextFlow:I

    .line 668
    return-void
.end method

.method public setXaPage(I)V
    .locals 0
    .param p1, "field_39_xaPage"    # I

    .prologue
    .line 731
    iput p1, p0, Lorg/apache/index/poi/hwpf/model/types/SEPAbstractType;->field_39_xaPage:I

    .line 732
    return-void
.end method

.method public setXaPageNUp(I)V
    .locals 0
    .param p1, "field_41_xaPageNUp"    # I

    .prologue
    .line 763
    iput p1, p0, Lorg/apache/index/poi/hwpf/model/types/SEPAbstractType;->field_41_xaPageNUp:I

    .line 764
    return-void
.end method

.method public setYaPage(I)V
    .locals 0
    .param p1, "field_40_yaPage"    # I

    .prologue
    .line 747
    iput p1, p0, Lorg/apache/index/poi/hwpf/model/types/SEPAbstractType;->field_40_yaPage:I

    .line 748
    return-void
.end method

.method public setYaPageNUp(I)V
    .locals 0
    .param p1, "field_42_yaPageNUp"    # I

    .prologue
    .line 779
    iput p1, p0, Lorg/apache/index/poi/hwpf/model/types/SEPAbstractType;->field_42_yaPageNUp:I

    .line 780
    return-void
.end method
