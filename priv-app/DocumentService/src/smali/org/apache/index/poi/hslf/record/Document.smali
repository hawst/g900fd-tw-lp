.class public final Lorg/apache/index/poi/hslf/record/Document;
.super Lorg/apache/index/poi/hslf/record/PositionDependentRecordContainer;
.source "Document.java"


# static fields
.field private static _type:J


# instance fields
.field private _header:[B

.field private documentAtom:Lorg/apache/index/poi/hslf/record/DocumentAtom;

.field private environment:Lorg/apache/index/poi/hslf/record/Environment;

.field private ppDrawing:Lorg/apache/index/poi/hslf/record/PPDrawingGroup;

.field private slwts:[Lorg/apache/index/poi/hslf/record/SlideListWithText;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 32
    const-wide/16 v0, 0x3e8

    sput-wide v0, Lorg/apache/index/poi/hslf/record/Document;->_type:J

    return-void
.end method

.method protected constructor <init>([BII)V
    .locals 6
    .param p1, "source"    # [B
    .param p2, "start"    # I
    .param p3, "len"    # I

    .prologue
    const/16 v3, 0x8

    const/4 v4, 0x0

    .line 105
    invoke-direct {p0}, Lorg/apache/index/poi/hslf/record/PositionDependentRecordContainer;-><init>()V

    .line 107
    new-array v2, v3, [B

    iput-object v2, p0, Lorg/apache/index/poi/hslf/record/Document;->_header:[B

    .line 108
    iget-object v2, p0, Lorg/apache/index/poi/hslf/record/Document;->_header:[B

    invoke-static {p1, p2, v2, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 111
    add-int/lit8 v2, p2, 0x8

    add-int/lit8 v3, p3, -0x8

    invoke-static {p1, v2, v3}, Lorg/apache/index/poi/hslf/record/Record;->findChildRecords([BII)[Lorg/apache/index/poi/hslf/record/Record;

    move-result-object v2

    iput-object v2, p0, Lorg/apache/index/poi/hslf/record/Document;->_children:[Lorg/apache/index/poi/hslf/record/Record;

    .line 114
    iget-object v2, p0, Lorg/apache/index/poi/hslf/record/Document;->_children:[Lorg/apache/index/poi/hslf/record/Record;

    aget-object v2, v2, v4

    instance-of v2, v2, Lorg/apache/index/poi/hslf/record/DocumentAtom;

    if-nez v2, :cond_0

    .line 115
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string/jumbo v3, "The first child of a Document must be a DocumentAtom"

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 117
    :cond_0
    iget-object v2, p0, Lorg/apache/index/poi/hslf/record/Document;->_children:[Lorg/apache/index/poi/hslf/record/Record;

    aget-object v2, v2, v4

    check-cast v2, Lorg/apache/index/poi/hslf/record/DocumentAtom;

    iput-object v2, p0, Lorg/apache/index/poi/hslf/record/Document;->documentAtom:Lorg/apache/index/poi/hslf/record/DocumentAtom;

    .line 122
    const/4 v1, 0x0

    .line 123
    .local v1, "slwtcount":I
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lorg/apache/index/poi/hslf/record/Document;->_children:[Lorg/apache/index/poi/hslf/record/Record;

    array-length v2, v2

    if-lt v0, v2, :cond_3

    .line 138
    if-nez v1, :cond_1

    .line 139
    iget-object v2, p0, Lorg/apache/index/poi/hslf/record/Document;->logger:Lorg/apache/index/poi/util/POILogger;

    sget v3, Lorg/apache/index/poi/util/POILogger;->WARN:I

    const-string/jumbo v4, "No SlideListWithText\'s found - there should normally be at least one!"

    invoke-virtual {v2, v3, v4}, Lorg/apache/index/poi/util/POILogger;->log(ILjava/lang/Object;)V

    .line 141
    :cond_1
    const/4 v2, 0x3

    if-le v1, v2, :cond_2

    .line 142
    iget-object v2, p0, Lorg/apache/index/poi/hslf/record/Document;->logger:Lorg/apache/index/poi/util/POILogger;

    sget v3, Lorg/apache/index/poi/util/POILogger;->WARN:I

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "Found "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " SlideListWithTexts - normally there should only be three!"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lorg/apache/index/poi/util/POILogger;->log(ILjava/lang/Object;)V

    .line 146
    :cond_2
    new-array v2, v1, [Lorg/apache/index/poi/hslf/record/SlideListWithText;

    iput-object v2, p0, Lorg/apache/index/poi/hslf/record/Document;->slwts:[Lorg/apache/index/poi/hslf/record/SlideListWithText;

    .line 147
    const/4 v1, 0x0

    .line 148
    const/4 v0, 0x1

    :goto_1
    iget-object v2, p0, Lorg/apache/index/poi/hslf/record/Document;->_children:[Lorg/apache/index/poi/hslf/record/Record;

    array-length v2, v2

    if-lt v0, v2, :cond_7

    .line 154
    return-void

    .line 124
    :cond_3
    iget-object v2, p0, Lorg/apache/index/poi/hslf/record/Document;->_children:[Lorg/apache/index/poi/hslf/record/Record;

    aget-object v2, v2, v0

    instance-of v2, v2, Lorg/apache/index/poi/hslf/record/SlideListWithText;

    if-eqz v2, :cond_4

    .line 125
    add-int/lit8 v1, v1, 0x1

    .line 127
    :cond_4
    iget-object v2, p0, Lorg/apache/index/poi/hslf/record/Document;->_children:[Lorg/apache/index/poi/hslf/record/Record;

    aget-object v2, v2, v0

    instance-of v2, v2, Lorg/apache/index/poi/hslf/record/Environment;

    if-eqz v2, :cond_5

    .line 128
    iget-object v2, p0, Lorg/apache/index/poi/hslf/record/Document;->_children:[Lorg/apache/index/poi/hslf/record/Record;

    aget-object v2, v2, v0

    check-cast v2, Lorg/apache/index/poi/hslf/record/Environment;

    iput-object v2, p0, Lorg/apache/index/poi/hslf/record/Document;->environment:Lorg/apache/index/poi/hslf/record/Environment;

    .line 130
    :cond_5
    iget-object v2, p0, Lorg/apache/index/poi/hslf/record/Document;->_children:[Lorg/apache/index/poi/hslf/record/Record;

    aget-object v2, v2, v0

    instance-of v2, v2, Lorg/apache/index/poi/hslf/record/PPDrawingGroup;

    if-eqz v2, :cond_6

    .line 131
    iget-object v2, p0, Lorg/apache/index/poi/hslf/record/Document;->_children:[Lorg/apache/index/poi/hslf/record/Record;

    aget-object v2, v2, v0

    check-cast v2, Lorg/apache/index/poi/hslf/record/PPDrawingGroup;

    iput-object v2, p0, Lorg/apache/index/poi/hslf/record/Document;->ppDrawing:Lorg/apache/index/poi/hslf/record/PPDrawingGroup;

    .line 123
    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 149
    :cond_7
    iget-object v2, p0, Lorg/apache/index/poi/hslf/record/Document;->_children:[Lorg/apache/index/poi/hslf/record/Record;

    aget-object v2, v2, v0

    instance-of v2, v2, Lorg/apache/index/poi/hslf/record/SlideListWithText;

    if-eqz v2, :cond_8

    .line 150
    iget-object v3, p0, Lorg/apache/index/poi/hslf/record/Document;->slwts:[Lorg/apache/index/poi/hslf/record/SlideListWithText;

    iget-object v2, p0, Lorg/apache/index/poi/hslf/record/Document;->_children:[Lorg/apache/index/poi/hslf/record/Record;

    aget-object v2, v2, v0

    check-cast v2, Lorg/apache/index/poi/hslf/record/SlideListWithText;

    aput-object v2, v3, v1

    .line 151
    add-int/lit8 v1, v1, 0x1

    .line 148
    :cond_8
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method


# virtual methods
.method public getDocumentAtom()Lorg/apache/index/poi/hslf/record/DocumentAtom;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/Document;->documentAtom:Lorg/apache/index/poi/hslf/record/DocumentAtom;

    return-object v0
.end method

.method public getEnvironment()Lorg/apache/index/poi/hslf/record/Environment;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/Document;->environment:Lorg/apache/index/poi/hslf/record/Environment;

    return-object v0
.end method

.method public getMasterSlideListWithText()Lorg/apache/index/poi/hslf/record/SlideListWithText;
    .locals 3

    .prologue
    .line 68
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lorg/apache/index/poi/hslf/record/Document;->slwts:[Lorg/apache/index/poi/hslf/record/SlideListWithText;

    array-length v1, v1

    if-lt v0, v1, :cond_0

    .line 73
    const/4 v1, 0x0

    :goto_1
    return-object v1

    .line 69
    :cond_0
    iget-object v1, p0, Lorg/apache/index/poi/hslf/record/Document;->slwts:[Lorg/apache/index/poi/hslf/record/SlideListWithText;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lorg/apache/index/poi/hslf/record/SlideListWithText;->getInstance()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 70
    iget-object v1, p0, Lorg/apache/index/poi/hslf/record/Document;->slwts:[Lorg/apache/index/poi/hslf/record/SlideListWithText;

    aget-object v1, v1, v0

    goto :goto_1

    .line 68
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public getNotesSlideListWithText()Lorg/apache/index/poi/hslf/record/SlideListWithText;
    .locals 3

    .prologue
    .line 93
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lorg/apache/index/poi/hslf/record/Document;->slwts:[Lorg/apache/index/poi/hslf/record/SlideListWithText;

    array-length v1, v1

    if-lt v0, v1, :cond_0

    .line 98
    const/4 v1, 0x0

    :goto_1
    return-object v1

    .line 94
    :cond_0
    iget-object v1, p0, Lorg/apache/index/poi/hslf/record/Document;->slwts:[Lorg/apache/index/poi/hslf/record/SlideListWithText;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lorg/apache/index/poi/hslf/record/SlideListWithText;->getInstance()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    .line 95
    iget-object v1, p0, Lorg/apache/index/poi/hslf/record/Document;->slwts:[Lorg/apache/index/poi/hslf/record/SlideListWithText;

    aget-object v1, v1, v0

    goto :goto_1

    .line 93
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public getPPDrawingGroup()Lorg/apache/index/poi/hslf/record/PPDrawingGroup;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/Document;->ppDrawing:Lorg/apache/index/poi/hslf/record/PPDrawingGroup;

    return-object v0
.end method

.method public getRecordType()J
    .locals 2

    .prologue
    .line 158
    sget-wide v0, Lorg/apache/index/poi/hslf/record/Document;->_type:J

    return-wide v0
.end method

.method public getSlideListWithTexts()[Lorg/apache/index/poi/hslf/record/SlideListWithText;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/Document;->slwts:[Lorg/apache/index/poi/hslf/record/SlideListWithText;

    return-object v0
.end method

.method public getSlideSlideListWithText()Lorg/apache/index/poi/hslf/record/SlideListWithText;
    .locals 2

    .prologue
    .line 81
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lorg/apache/index/poi/hslf/record/Document;->slwts:[Lorg/apache/index/poi/hslf/record/SlideListWithText;

    array-length v1, v1

    if-lt v0, v1, :cond_0

    .line 86
    const/4 v1, 0x0

    :goto_1
    return-object v1

    .line 82
    :cond_0
    iget-object v1, p0, Lorg/apache/index/poi/hslf/record/Document;->slwts:[Lorg/apache/index/poi/hslf/record/SlideListWithText;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lorg/apache/index/poi/hslf/record/SlideListWithText;->getInstance()I

    move-result v1

    if-nez v1, :cond_1

    .line 83
    iget-object v1, p0, Lorg/apache/index/poi/hslf/record/Document;->slwts:[Lorg/apache/index/poi/hslf/record/SlideListWithText;

    aget-object v1, v1, v0

    goto :goto_1

    .line 81
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
