.class public final Lorg/apache/index/poi/hslf/EncryptedSlideShow;
.super Ljava/lang/Object;
.source "EncryptedSlideShow.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static checkIfEncrypted(Lorg/apache/index/poi/hslf/HSLFSlideShow;)Z
    .locals 4
    .param p0, "hss"    # Lorg/apache/index/poi/hslf/HSLFSlideShow;

    .prologue
    const/4 v1, 0x1

    .line 55
    :try_start_0
    invoke-virtual {p0}, Lorg/apache/index/poi/hslf/HSLFSlideShow;->getPOIFSDirectory()Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;

    move-result-object v2

    const-string/jumbo v3, "EncryptedSummary"

    invoke-virtual {v2, v3}, Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;->getEntry(Ljava/lang/String;)Lorg/apache/index/poi/poifs/filesystem/Entry;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 70
    :cond_0
    :goto_0
    return v1

    .line 57
    :catch_0
    move-exception v2

    .line 66
    invoke-static {p0}, Lorg/apache/index/poi/hslf/EncryptedSlideShow;->fetchDocumentEncryptionAtom(Lorg/apache/index/poi/hslf/HSLFSlideShow;)Lorg/apache/index/poi/hslf/record/DocumentEncryptionAtom;

    move-result-object v0

    .line 67
    .local v0, "dea":Lorg/apache/index/poi/hslf/record/DocumentEncryptionAtom;
    if-nez v0, :cond_0

    .line 70
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static fetchDocumentEncryptionAtom(Lorg/apache/index/poi/hslf/HSLFSlideShow;)Lorg/apache/index/poi/hslf/record/DocumentEncryptionAtom;
    .locals 16
    .param p0, "hss"    # Lorg/apache/index/poi/hslf/HSLFSlideShow;

    .prologue
    .line 82
    invoke-virtual/range {p0 .. p0}, Lorg/apache/index/poi/hslf/HSLFSlideShow;->getCurrentUserAtom()Lorg/apache/index/poi/hslf/record/CurrentUserAtom;

    move-result-object v0

    .line 83
    .local v0, "cua":Lorg/apache/index/poi/hslf/record/CurrentUserAtom;
    invoke-virtual {v0}, Lorg/apache/index/poi/hslf/record/CurrentUserAtom;->getCurrentEditOffset()J

    move-result-wide v12

    const-wide/16 v14, 0x0

    cmp-long v11, v12, v14

    if-eqz v11, :cond_7

    .line 85
    invoke-virtual {v0}, Lorg/apache/index/poi/hslf/record/CurrentUserAtom;->getCurrentEditOffset()J

    move-result-wide v12

    invoke-virtual/range {p0 .. p0}, Lorg/apache/index/poi/hslf/HSLFSlideShow;->getUnderlyingBytesLength()I

    move-result v11

    int-to-long v14, v11

    cmp-long v11, v12, v14

    if-lez v11, :cond_0

    .line 86
    new-instance v11, Lorg/apache/index/poi/hslf/exceptions/CorruptPowerPointFileException;

    const-string/jumbo v12, "The CurrentUserAtom claims that the offset of last edit details are past the end of the file"

    invoke-direct {v11, v12}, Lorg/apache/index/poi/hslf/exceptions/CorruptPowerPointFileException;-><init>(Ljava/lang/String;)V

    throw v11

    .line 91
    :cond_0
    const/4 v6, 0x0

    .line 94
    .local v6, "r":Lorg/apache/index/poi/hslf/record/Record;
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lorg/apache/index/poi/hslf/HSLFSlideShow;->getUnderlyingStream()Lorg/apache/index/poi/poifs/filesystem/DocumentInputStream;

    move-result-object v11

    .line 95
    invoke-virtual {v0}, Lorg/apache/index/poi/hslf/record/CurrentUserAtom;->getCurrentEditOffset()J

    move-result-wide v12

    long-to-int v12, v12

    .line 93
    invoke-static {v11, v12}, Lorg/apache/index/poi/hslf/record/Record;->buildRecordAtOffset(Lorg/apache/index/poi/poifs/filesystem/DocumentInputStream;I)Lorg/apache/index/poi/hslf/record/Record;
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    .line 100
    if-nez v6, :cond_1

    const/4 v8, 0x0

    .line 134
    .end local v6    # "r":Lorg/apache/index/poi/hslf/record/Record;
    :goto_0
    return-object v8

    .line 97
    .restart local v6    # "r":Lorg/apache/index/poi/hslf/record/Record;
    :catch_0
    move-exception v1

    .line 98
    .local v1, "e":Ljava/lang/ArrayIndexOutOfBoundsException;
    const/4 v8, 0x0

    goto :goto_0

    .line 101
    .end local v1    # "e":Ljava/lang/ArrayIndexOutOfBoundsException;
    :cond_1
    instance-of v11, v6, Lorg/apache/index/poi/hslf/record/UserEditAtom;

    if-nez v11, :cond_2

    const/4 v8, 0x0

    goto :goto_0

    :cond_2
    move-object v10, v6

    .line 102
    check-cast v10, Lorg/apache/index/poi/hslf/record/UserEditAtom;

    .line 106
    .local v10, "uea":Lorg/apache/index/poi/hslf/record/UserEditAtom;
    invoke-virtual/range {p0 .. p0}, Lorg/apache/index/poi/hslf/HSLFSlideShow;->getUnderlyingStream()Lorg/apache/index/poi/poifs/filesystem/DocumentInputStream;

    move-result-object v11

    .line 107
    invoke-virtual {v10}, Lorg/apache/index/poi/hslf/record/UserEditAtom;->getPersistPointersOffset()I

    move-result v12

    .line 105
    invoke-static {v11, v12}, Lorg/apache/index/poi/hslf/record/Record;->buildRecordAtOffset(Lorg/apache/index/poi/poifs/filesystem/DocumentInputStream;I)Lorg/apache/index/poi/hslf/record/Record;

    move-result-object v7

    .line 109
    .local v7, "r2":Lorg/apache/index/poi/hslf/record/Record;
    instance-of v11, v7, Lorg/apache/index/poi/hslf/record/PersistPtrHolder;

    if-nez v11, :cond_3

    const/4 v8, 0x0

    goto :goto_0

    :cond_3
    move-object v5, v7

    .line 110
    check-cast v5, Lorg/apache/index/poi/hslf/record/PersistPtrHolder;

    .line 113
    .local v5, "pph":Lorg/apache/index/poi/hslf/record/PersistPtrHolder;
    invoke-virtual {v5}, Lorg/apache/index/poi/hslf/record/PersistPtrHolder;->getKnownSlideIDs()[I

    move-result-object v9

    .line 114
    .local v9, "slideIds":[I
    const/4 v3, -0x1

    .line 115
    .local v3, "maxSlideId":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    array-length v11, v9

    if-lt v2, v11, :cond_4

    .line 118
    const/4 v11, -0x1

    if-ne v3, v11, :cond_6

    const/4 v8, 0x0

    goto :goto_0

    .line 116
    :cond_4
    aget v11, v9, v2

    if-le v11, v3, :cond_5

    aget v3, v9, v2

    .line 115
    :cond_5
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 120
    :cond_6
    invoke-virtual {v5}, Lorg/apache/index/poi/hslf/record/PersistPtrHolder;->getSlideLocationsLookup()Ljava/util/Hashtable;

    move-result-object v11

    .line 121
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    .line 120
    invoke-virtual {v11, v12}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/Integer;

    .line 122
    invoke-virtual {v11}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 124
    .local v4, "offset":I
    invoke-virtual/range {p0 .. p0}, Lorg/apache/index/poi/hslf/HSLFSlideShow;->getUnderlyingStream()Lorg/apache/index/poi/poifs/filesystem/DocumentInputStream;

    move-result-object v11

    .line 123
    invoke-static {v11, v4}, Lorg/apache/index/poi/hslf/record/Record;->buildRecordAtOffset(Lorg/apache/index/poi/poifs/filesystem/DocumentInputStream;I)Lorg/apache/index/poi/hslf/record/Record;

    move-result-object v8

    .line 129
    .local v8, "r3":Lorg/apache/index/poi/hslf/record/Record;
    instance-of v11, v8, Lorg/apache/index/poi/hslf/record/DocumentEncryptionAtom;

    if-eqz v11, :cond_7

    .line 130
    check-cast v8, Lorg/apache/index/poi/hslf/record/DocumentEncryptionAtom;

    goto :goto_0

    .line 134
    .end local v2    # "i":I
    .end local v3    # "maxSlideId":I
    .end local v4    # "offset":I
    .end local v5    # "pph":Lorg/apache/index/poi/hslf/record/PersistPtrHolder;
    .end local v6    # "r":Lorg/apache/index/poi/hslf/record/Record;
    .end local v7    # "r2":Lorg/apache/index/poi/hslf/record/Record;
    .end local v8    # "r3":Lorg/apache/index/poi/hslf/record/Record;
    .end local v9    # "slideIds":[I
    .end local v10    # "uea":Lorg/apache/index/poi/hslf/record/UserEditAtom;
    :cond_7
    const/4 v8, 0x0

    goto :goto_0
.end method
