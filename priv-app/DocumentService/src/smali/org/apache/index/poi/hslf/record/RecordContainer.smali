.class public abstract Lorg/apache/index/poi/hslf/record/RecordContainer;
.super Lorg/apache/index/poi/hslf/record/Record;
.source "RecordContainer.java"


# instance fields
.field protected _children:[Lorg/apache/index/poi/hslf/record/Record;

.field private changingChildRecordsLock:Ljava/lang/Object;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0}, Lorg/apache/index/poi/hslf/record/Record;-><init>()V

    .line 36
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lorg/apache/index/poi/hslf/record/RecordContainer;->changingChildRecordsLock:Ljava/lang/Object;

    .line 31
    return-void
.end method

.method private addChildAt(Lorg/apache/index/poi/hslf/record/Record;I)V
    .locals 3
    .param p1, "newChild"    # Lorg/apache/index/poi/hslf/record/Record;
    .param p2, "position"    # I

    .prologue
    .line 92
    iget-object v1, p0, Lorg/apache/index/poi/hslf/record/RecordContainer;->changingChildRecordsLock:Ljava/lang/Object;

    monitor-enter v1

    .line 94
    :try_start_0
    invoke-direct {p0, p1}, Lorg/apache/index/poi/hslf/record/RecordContainer;->appendChild(Lorg/apache/index/poi/hslf/record/Record;)V

    .line 97
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/RecordContainer;->_children:[Lorg/apache/index/poi/hslf/record/Record;

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    const/4 v2, 0x1

    invoke-direct {p0, v0, p2, v2}, Lorg/apache/index/poi/hslf/record/RecordContainer;->moveChildRecords(III)V

    .line 92
    monitor-exit v1

    .line 99
    return-void

    .line 92
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private appendChild(Lorg/apache/index/poi/hslf/record/Record;)V
    .locals 6
    .param p1, "newChild"    # Lorg/apache/index/poi/hslf/record/Record;

    .prologue
    .line 75
    iget-object v2, p0, Lorg/apache/index/poi/hslf/record/RecordContainer;->changingChildRecordsLock:Ljava/lang/Object;

    monitor-enter v2

    .line 77
    :try_start_0
    iget-object v1, p0, Lorg/apache/index/poi/hslf/record/RecordContainer;->_children:[Lorg/apache/index/poi/hslf/record/Record;

    array-length v1, v1

    add-int/lit8 v1, v1, 0x1

    new-array v0, v1, [Lorg/apache/index/poi/hslf/record/Record;

    .line 78
    .local v0, "nc":[Lorg/apache/index/poi/hslf/record/Record;
    iget-object v1, p0, Lorg/apache/index/poi/hslf/record/RecordContainer;->_children:[Lorg/apache/index/poi/hslf/record/Record;

    const/4 v3, 0x0

    const/4 v4, 0x0

    iget-object v5, p0, Lorg/apache/index/poi/hslf/record/RecordContainer;->_children:[Lorg/apache/index/poi/hslf/record/Record;

    array-length v5, v5

    invoke-static {v1, v3, v0, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 80
    iget-object v1, p0, Lorg/apache/index/poi/hslf/record/RecordContainer;->_children:[Lorg/apache/index/poi/hslf/record/Record;

    array-length v1, v1

    aput-object p1, v0, v1

    .line 81
    iput-object v0, p0, Lorg/apache/index/poi/hslf/record/RecordContainer;->_children:[Lorg/apache/index/poi/hslf/record/Record;

    .line 75
    monitor-exit v2

    .line 83
    return-void

    .line 75
    .end local v0    # "nc":[Lorg/apache/index/poi/hslf/record/Record;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private findChildLocation(Lorg/apache/index/poi/hslf/record/Record;)I
    .locals 3
    .param p1, "child"    # Lorg/apache/index/poi/hslf/record/Record;

    .prologue
    .line 60
    iget-object v2, p0, Lorg/apache/index/poi/hslf/record/RecordContainer;->changingChildRecordsLock:Ljava/lang/Object;

    monitor-enter v2

    .line 61
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    :try_start_0
    iget-object v1, p0, Lorg/apache/index/poi/hslf/record/RecordContainer;->_children:[Lorg/apache/index/poi/hslf/record/Record;

    array-length v1, v1

    if-lt v0, v1, :cond_0

    .line 60
    monitor-exit v2

    .line 67
    const/4 v0, -0x1

    .end local v0    # "i":I
    :goto_1
    return v0

    .line 62
    .restart local v0    # "i":I
    :cond_0
    iget-object v1, p0, Lorg/apache/index/poi/hslf/record/RecordContainer;->_children:[Lorg/apache/index/poi/hslf/record/Record;

    aget-object v1, v1, v0

    invoke-virtual {v1, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 63
    monitor-exit v2

    goto :goto_1

    .line 60
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 61
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public static handleParentAwareRecords(Lorg/apache/index/poi/hslf/record/RecordContainer;)V
    .locals 5
    .param p0, "br"    # Lorg/apache/index/poi/hslf/record/RecordContainer;

    .prologue
    .line 274
    invoke-virtual {p0}, Lorg/apache/index/poi/hslf/record/RecordContainer;->getChildRecords()[Lorg/apache/index/poi/hslf/record/Record;

    move-result-object v3

    array-length v4, v3

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-lt v2, v4, :cond_0

    .line 284
    return-void

    .line 274
    :cond_0
    aget-object v0, v3, v2

    .line 276
    .local v0, "record":Lorg/apache/index/poi/hslf/record/Record;
    instance-of v1, v0, Lorg/apache/index/poi/hslf/record/ParentAwareRecord;

    if-eqz v1, :cond_1

    move-object v1, v0

    .line 277
    check-cast v1, Lorg/apache/index/poi/hslf/record/ParentAwareRecord;

    invoke-interface {v1, p0}, Lorg/apache/index/poi/hslf/record/ParentAwareRecord;->setParentRecord(Lorg/apache/index/poi/hslf/record/RecordContainer;)V

    .line 280
    :cond_1
    instance-of v1, v0, Lorg/apache/index/poi/hslf/record/RecordContainer;

    if-eqz v1, :cond_2

    .line 281
    check-cast v0, Lorg/apache/index/poi/hslf/record/RecordContainer;

    .end local v0    # "record":Lorg/apache/index/poi/hslf/record/Record;
    invoke-static {v0}, Lorg/apache/index/poi/hslf/record/RecordContainer;->handleParentAwareRecords(Lorg/apache/index/poi/hslf/record/RecordContainer;)V

    .line 274
    :cond_2
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0
.end method

.method private moveChildRecords(III)V
    .locals 2
    .param p1, "oldLoc"    # I
    .param p2, "newLoc"    # I
    .param p3, "number"    # I

    .prologue
    .line 109
    if-ne p1, p2, :cond_1

    .line 119
    :cond_0
    :goto_0
    return-void

    .line 110
    :cond_1
    if-eqz p3, :cond_0

    .line 113
    add-int v0, p1, p3

    iget-object v1, p0, Lorg/apache/index/poi/hslf/record/RecordContainer;->_children:[Lorg/apache/index/poi/hslf/record/Record;

    array-length v1, v1

    if-le v0, v1, :cond_2

    .line 114
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "Asked to move more records than there are!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 118
    :cond_2
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/RecordContainer;->_children:[Lorg/apache/index/poi/hslf/record/Record;

    invoke-static {v0, p1, p2, p3}, Lorg/apache/index/poi/util/ArrayUtil;->arrayMoveWithin([Ljava/lang/Object;III)V

    goto :goto_0
.end method


# virtual methods
.method public addChildAfter(Lorg/apache/index/poi/hslf/record/Record;Lorg/apache/index/poi/hslf/record/Record;)V
    .locals 4
    .param p1, "newChild"    # Lorg/apache/index/poi/hslf/record/Record;
    .param p2, "after"    # Lorg/apache/index/poi/hslf/record/Record;

    .prologue
    .line 173
    iget-object v2, p0, Lorg/apache/index/poi/hslf/record/RecordContainer;->changingChildRecordsLock:Ljava/lang/Object;

    monitor-enter v2

    .line 175
    :try_start_0
    invoke-direct {p0, p2}, Lorg/apache/index/poi/hslf/record/RecordContainer;->findChildLocation(Lorg/apache/index/poi/hslf/record/Record;)I

    move-result v0

    .line 176
    .local v0, "loc":I
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 177
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v3, "Asked to add a new child after another record, but that record wasn\'t one of our children!"

    invoke-direct {v1, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 173
    .end local v0    # "loc":I
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 181
    .restart local v0    # "loc":I
    :cond_0
    add-int/lit8 v1, v0, 0x1

    :try_start_1
    invoke-direct {p0, p1, v1}, Lorg/apache/index/poi/hslf/record/RecordContainer;->addChildAt(Lorg/apache/index/poi/hslf/record/Record;I)V

    .line 173
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 183
    return-void
.end method

.method public addChildBefore(Lorg/apache/index/poi/hslf/record/Record;Lorg/apache/index/poi/hslf/record/Record;)V
    .locals 4
    .param p1, "newChild"    # Lorg/apache/index/poi/hslf/record/Record;
    .param p2, "before"    # Lorg/apache/index/poi/hslf/record/Record;

    .prologue
    .line 191
    iget-object v2, p0, Lorg/apache/index/poi/hslf/record/RecordContainer;->changingChildRecordsLock:Ljava/lang/Object;

    monitor-enter v2

    .line 193
    :try_start_0
    invoke-direct {p0, p2}, Lorg/apache/index/poi/hslf/record/RecordContainer;->findChildLocation(Lorg/apache/index/poi/hslf/record/Record;)I

    move-result v0

    .line 194
    .local v0, "loc":I
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 195
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v3, "Asked to add a new child before another record, but that record wasn\'t one of our children!"

    invoke-direct {v1, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 191
    .end local v0    # "loc":I
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 199
    .restart local v0    # "loc":I
    :cond_0
    :try_start_1
    invoke-direct {p0, p1, v0}, Lorg/apache/index/poi/hslf/record/RecordContainer;->addChildAt(Lorg/apache/index/poi/hslf/record/Record;I)V

    .line 191
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 201
    return-void
.end method

.method public appendChildRecord(Lorg/apache/index/poi/hslf/record/Record;)V
    .locals 2
    .param p1, "newChild"    # Lorg/apache/index/poi/hslf/record/Record;

    .prologue
    .line 162
    iget-object v1, p0, Lorg/apache/index/poi/hslf/record/RecordContainer;->changingChildRecordsLock:Ljava/lang/Object;

    monitor-enter v1

    .line 163
    :try_start_0
    invoke-direct {p0, p1}, Lorg/apache/index/poi/hslf/record/RecordContainer;->appendChild(Lorg/apache/index/poi/hslf/record/Record;)V

    .line 162
    monitor-exit v1

    .line 165
    return-void

    .line 162
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public findFirstOfType(J)Lorg/apache/index/poi/hslf/record/Record;
    .locals 5
    .param p1, "type"    # J

    .prologue
    .line 128
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lorg/apache/index/poi/hslf/record/RecordContainer;->_children:[Lorg/apache/index/poi/hslf/record/Record;

    array-length v1, v1

    if-lt v0, v1, :cond_0

    .line 133
    const/4 v1, 0x0

    :goto_1
    return-object v1

    .line 129
    :cond_0
    iget-object v1, p0, Lorg/apache/index/poi/hslf/record/RecordContainer;->_children:[Lorg/apache/index/poi/hslf/record/Record;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lorg/apache/index/poi/hslf/record/Record;->getRecordType()J

    move-result-wide v2

    cmp-long v1, v2, p1

    if-nez v1, :cond_1

    .line 130
    iget-object v1, p0, Lorg/apache/index/poi/hslf/record/RecordContainer;->_children:[Lorg/apache/index/poi/hslf/record/Record;

    aget-object v1, v1, v0

    goto :goto_1

    .line 128
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public getChildRecords()[Lorg/apache/index/poi/hslf/record/Record;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/RecordContainer;->_children:[Lorg/apache/index/poi/hslf/record/Record;

    return-object v0
.end method

.method public isAnAtom()Z
    .locals 1

    .prologue
    .line 46
    const/4 v0, 0x0

    return v0
.end method

.method public moveChildBefore(Lorg/apache/index/poi/hslf/record/Record;Lorg/apache/index/poi/hslf/record/Record;)V
    .locals 1
    .param p1, "child"    # Lorg/apache/index/poi/hslf/record/Record;
    .param p2, "before"    # Lorg/apache/index/poi/hslf/record/Record;

    .prologue
    .line 207
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, p2}, Lorg/apache/index/poi/hslf/record/RecordContainer;->moveChildrenBefore(Lorg/apache/index/poi/hslf/record/Record;ILorg/apache/index/poi/hslf/record/Record;)V

    .line 208
    return-void
.end method

.method public moveChildrenAfter(Lorg/apache/index/poi/hslf/record/Record;ILorg/apache/index/poi/hslf/record/Record;)V
    .locals 5
    .param p1, "firstChild"    # Lorg/apache/index/poi/hslf/record/Record;
    .param p2, "number"    # I
    .param p3, "after"    # Lorg/apache/index/poi/hslf/record/Record;

    .prologue
    const/4 v4, -0x1

    .line 238
    const/4 v2, 0x1

    if-ge p2, v2, :cond_0

    .line 258
    :goto_0
    return-void

    .line 240
    :cond_0
    iget-object v3, p0, Lorg/apache/index/poi/hslf/record/RecordContainer;->changingChildRecordsLock:Ljava/lang/Object;

    monitor-enter v3

    .line 242
    :try_start_0
    invoke-direct {p0, p3}, Lorg/apache/index/poi/hslf/record/RecordContainer;->findChildLocation(Lorg/apache/index/poi/hslf/record/Record;)I

    move-result v0

    .line 243
    .local v0, "newLoc":I
    if-ne v0, v4, :cond_1

    .line 244
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v4, "Asked to move children before another record, but that record wasn\'t one of our children!"

    invoke-direct {v2, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 240
    .end local v0    # "newLoc":I
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 247
    .restart local v0    # "newLoc":I
    :cond_1
    add-int/lit8 v0, v0, 0x1

    .line 250
    :try_start_1
    invoke-direct {p0, p1}, Lorg/apache/index/poi/hslf/record/RecordContainer;->findChildLocation(Lorg/apache/index/poi/hslf/record/Record;)I

    move-result v1

    .line 251
    .local v1, "oldLoc":I
    if-ne v1, v4, :cond_2

    .line 252
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v4, "Asked to move a record that wasn\'t a child!"

    invoke-direct {v2, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 256
    :cond_2
    invoke-direct {p0, v1, v0, p2}, Lorg/apache/index/poi/hslf/record/RecordContainer;->moveChildRecords(III)V

    .line 240
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public moveChildrenBefore(Lorg/apache/index/poi/hslf/record/Record;ILorg/apache/index/poi/hslf/record/Record;)V
    .locals 5
    .param p1, "firstChild"    # Lorg/apache/index/poi/hslf/record/Record;
    .param p2, "number"    # I
    .param p3, "before"    # Lorg/apache/index/poi/hslf/record/Record;

    .prologue
    const/4 v4, -0x1

    .line 214
    const/4 v2, 0x1

    if-ge p2, v2, :cond_0

    .line 232
    :goto_0
    return-void

    .line 216
    :cond_0
    iget-object v3, p0, Lorg/apache/index/poi/hslf/record/RecordContainer;->changingChildRecordsLock:Ljava/lang/Object;

    monitor-enter v3

    .line 218
    :try_start_0
    invoke-direct {p0, p3}, Lorg/apache/index/poi/hslf/record/RecordContainer;->findChildLocation(Lorg/apache/index/poi/hslf/record/Record;)I

    move-result v0

    .line 219
    .local v0, "newLoc":I
    if-ne v0, v4, :cond_1

    .line 220
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v4, "Asked to move children before another record, but that record wasn\'t one of our children!"

    invoke-direct {v2, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 216
    .end local v0    # "newLoc":I
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 224
    .restart local v0    # "newLoc":I
    :cond_1
    :try_start_1
    invoke-direct {p0, p1}, Lorg/apache/index/poi/hslf/record/RecordContainer;->findChildLocation(Lorg/apache/index/poi/hslf/record/Record;)I

    move-result v1

    .line 225
    .local v1, "oldLoc":I
    if-ne v1, v4, :cond_2

    .line 226
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v4, "Asked to move a record that wasn\'t a child!"

    invoke-direct {v2, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 230
    :cond_2
    invoke-direct {p0, v1, v0, p2}, Lorg/apache/index/poi/hslf/record/RecordContainer;->moveChildRecords(III)V

    .line 216
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public removeChild(Lorg/apache/index/poi/hslf/record/Record;)Lorg/apache/index/poi/hslf/record/Record;
    .locals 6
    .param p1, "ch"    # Lorg/apache/index/poi/hslf/record/Record;

    .prologue
    .line 143
    const/4 v2, 0x0

    .line 144
    .local v2, "rm":Lorg/apache/index/poi/hslf/record/Record;
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 145
    .local v0, "lst":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/index/poi/hslf/record/Record;>;"
    iget-object v4, p0, Lorg/apache/index/poi/hslf/record/RecordContainer;->_children:[Lorg/apache/index/poi/hslf/record/Record;

    array-length v5, v4

    const/4 v3, 0x0

    :goto_0
    if-lt v3, v5, :cond_0

    .line 149
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    new-array v3, v3, [Lorg/apache/index/poi/hslf/record/Record;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Lorg/apache/index/poi/hslf/record/Record;

    iput-object v3, p0, Lorg/apache/index/poi/hslf/record/RecordContainer;->_children:[Lorg/apache/index/poi/hslf/record/Record;

    .line 150
    return-object v2

    .line 145
    :cond_0
    aget-object v1, v4, v3

    .line 146
    .local v1, "r":Lorg/apache/index/poi/hslf/record/Record;
    if-eq v1, p1, :cond_1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 145
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 147
    :cond_1
    move-object v2, v1

    goto :goto_1
.end method

.method public setChildRecord([Lorg/apache/index/poi/hslf/record/Record;)V
    .locals 0
    .param p1, "records"    # [Lorg/apache/index/poi/hslf/record/Record;

    .prologue
    .line 266
    iput-object p1, p0, Lorg/apache/index/poi/hslf/record/RecordContainer;->_children:[Lorg/apache/index/poi/hslf/record/Record;

    .line 267
    return-void
.end method
