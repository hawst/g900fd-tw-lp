.class public final Lorg/apache/index/poi/hslf/record/SlideListWithText;
.super Lorg/apache/index/poi/hslf/record/RecordContainer;
.source "SlideListWithText.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/index/poi/hslf/record/SlideListWithText$SlideAtomsSet;
    }
.end annotation


# static fields
.field public static final MASTER:I = 0x1

.field public static final NOTES:I = 0x2

.field public static final SLIDES:I

.field private static _type:J


# instance fields
.field private _header:[B

.field private slideAtomsSets:[Lorg/apache/index/poi/hslf/record/SlideListWithText$SlideAtomsSet;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 65
    const-wide/16 v0, 0xff0

    sput-wide v0, Lorg/apache/index/poi/hslf/record/SlideListWithText;->_type:J

    return-void
.end method

.method public constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 115
    invoke-direct {p0}, Lorg/apache/index/poi/hslf/record/RecordContainer;-><init>()V

    .line 116
    const/16 v0, 0x8

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/apache/index/poi/hslf/record/SlideListWithText;->_header:[B

    .line 117
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/SlideListWithText;->_header:[B

    const/16 v1, 0xf

    invoke-static {v0, v4, v1}, Lorg/apache/index/poi/util/LittleEndian;->putUShort([BII)V

    .line 118
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/SlideListWithText;->_header:[B

    const/4 v1, 0x2

    sget-wide v2, Lorg/apache/index/poi/hslf/record/SlideListWithText;->_type:J

    long-to-int v2, v2

    invoke-static {v0, v1, v2}, Lorg/apache/index/poi/util/LittleEndian;->putUShort([BII)V

    .line 119
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/SlideListWithText;->_header:[B

    const/4 v1, 0x4

    invoke-static {v0, v1, v4}, Lorg/apache/index/poi/util/LittleEndian;->putInt([BII)V

    .line 122
    new-array v0, v4, [Lorg/apache/index/poi/hslf/record/Record;

    iput-object v0, p0, Lorg/apache/index/poi/hslf/record/SlideListWithText;->_children:[Lorg/apache/index/poi/hslf/record/Record;

    .line 123
    new-array v0, v4, [Lorg/apache/index/poi/hslf/record/SlideListWithText$SlideAtomsSet;

    iput-object v0, p0, Lorg/apache/index/poi/hslf/record/SlideListWithText;->slideAtomsSets:[Lorg/apache/index/poi/hslf/record/SlideListWithText$SlideAtomsSet;

    .line 124
    return-void
.end method

.method protected constructor <init>([BII)V
    .locals 9
    .param p1, "source"    # [B
    .param p2, "start"    # I
    .param p3, "len"    # I

    .prologue
    const/16 v7, 0x8

    const/4 v8, 0x0

    .line 72
    invoke-direct {p0}, Lorg/apache/index/poi/hslf/record/RecordContainer;-><init>()V

    .line 74
    new-array v6, v7, [B

    iput-object v6, p0, Lorg/apache/index/poi/hslf/record/SlideListWithText;->_header:[B

    .line 75
    iget-object v6, p0, Lorg/apache/index/poi/hslf/record/SlideListWithText;->_header:[B

    invoke-static {p1, p2, v6, v8, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 78
    add-int/lit8 v6, p2, 0x8

    add-int/lit8 v7, p3, -0x8

    invoke-static {p1, v6, v7}, Lorg/apache/index/poi/hslf/record/Record;->findChildRecords([BII)[Lorg/apache/index/poi/hslf/record/Record;

    move-result-object v6

    iput-object v6, p0, Lorg/apache/index/poi/hslf/record/SlideListWithText;->_children:[Lorg/apache/index/poi/hslf/record/Record;

    .line 83
    new-instance v4, Ljava/util/Vector;

    invoke-direct {v4}, Ljava/util/Vector;-><init>()V

    .line 84
    .local v4, "sets":Ljava/util/Vector;, "Ljava/util/Vector<Lorg/apache/index/poi/hslf/record/SlideListWithText$SlideAtomsSet;>;"
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v6, p0, Lorg/apache/index/poi/hslf/record/SlideListWithText;->_children:[Lorg/apache/index/poi/hslf/record/Record;

    array-length v6, v6

    if-lt v2, v6, :cond_0

    .line 109
    invoke-virtual {v4}, Ljava/util/Vector;->size()I

    move-result v6

    new-array v6, v6, [Lorg/apache/index/poi/hslf/record/SlideListWithText$SlideAtomsSet;

    invoke-virtual {v4, v6}, Ljava/util/Vector;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v6

    check-cast v6, [Lorg/apache/index/poi/hslf/record/SlideListWithText$SlideAtomsSet;

    iput-object v6, p0, Lorg/apache/index/poi/hslf/record/SlideListWithText;->slideAtomsSets:[Lorg/apache/index/poi/hslf/record/SlideListWithText$SlideAtomsSet;

    .line 110
    return-void

    .line 85
    :cond_0
    iget-object v6, p0, Lorg/apache/index/poi/hslf/record/SlideListWithText;->_children:[Lorg/apache/index/poi/hslf/record/Record;

    aget-object v6, v6, v2

    instance-of v6, v6, Lorg/apache/index/poi/hslf/record/SlidePersistAtom;

    if-eqz v6, :cond_2

    .line 87
    add-int/lit8 v1, v2, 0x1

    .line 88
    .local v1, "endPos":I
    :goto_1
    iget-object v6, p0, Lorg/apache/index/poi/hslf/record/SlideListWithText;->_children:[Lorg/apache/index/poi/hslf/record/Record;

    array-length v6, v6

    if-ge v1, v6, :cond_1

    iget-object v6, p0, Lorg/apache/index/poi/hslf/record/SlideListWithText;->_children:[Lorg/apache/index/poi/hslf/record/Record;

    aget-object v6, v6, v1

    instance-of v6, v6, Lorg/apache/index/poi/hslf/record/SlidePersistAtom;

    if-eqz v6, :cond_3

    .line 92
    :cond_1
    sub-int v6, v1, v2

    add-int/lit8 v0, v6, -0x1

    .line 98
    .local v0, "clen":I
    new-array v5, v0, [Lorg/apache/index/poi/hslf/record/Record;

    .line 99
    .local v5, "spaChildren":[Lorg/apache/index/poi/hslf/record/Record;
    iget-object v6, p0, Lorg/apache/index/poi/hslf/record/SlideListWithText;->_children:[Lorg/apache/index/poi/hslf/record/Record;

    add-int/lit8 v7, v2, 0x1

    invoke-static {v6, v7, v5, v8, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 100
    new-instance v3, Lorg/apache/index/poi/hslf/record/SlideListWithText$SlideAtomsSet;

    iget-object v6, p0, Lorg/apache/index/poi/hslf/record/SlideListWithText;->_children:[Lorg/apache/index/poi/hslf/record/Record;

    aget-object v6, v6, v2

    check-cast v6, Lorg/apache/index/poi/hslf/record/SlidePersistAtom;

    invoke-direct {v3, v6, v5}, Lorg/apache/index/poi/hslf/record/SlideListWithText$SlideAtomsSet;-><init>(Lorg/apache/index/poi/hslf/record/SlidePersistAtom;[Lorg/apache/index/poi/hslf/record/Record;)V

    .line 101
    .local v3, "set":Lorg/apache/index/poi/hslf/record/SlideListWithText$SlideAtomsSet;
    invoke-virtual {v4, v3}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 104
    add-int/2addr v2, v0

    .line 84
    .end local v0    # "clen":I
    .end local v1    # "endPos":I
    .end local v3    # "set":Lorg/apache/index/poi/hslf/record/SlideListWithText$SlideAtomsSet;
    .end local v5    # "spaChildren":[Lorg/apache/index/poi/hslf/record/Record;
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 89
    .restart local v1    # "endPos":I
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method


# virtual methods
.method public addSlidePersistAtom(Lorg/apache/index/poi/hslf/record/SlidePersistAtom;)V
    .locals 5
    .param p1, "spa"    # Lorg/apache/index/poi/hslf/record/SlidePersistAtom;

    .prologue
    const/4 v4, 0x0

    .line 133
    invoke-virtual {p0, p1}, Lorg/apache/index/poi/hslf/record/SlideListWithText;->appendChildRecord(Lorg/apache/index/poi/hslf/record/Record;)V

    .line 135
    new-instance v0, Lorg/apache/index/poi/hslf/record/SlideListWithText$SlideAtomsSet;

    new-array v2, v4, [Lorg/apache/index/poi/hslf/record/Record;

    invoke-direct {v0, p1, v2}, Lorg/apache/index/poi/hslf/record/SlideListWithText$SlideAtomsSet;-><init>(Lorg/apache/index/poi/hslf/record/SlidePersistAtom;[Lorg/apache/index/poi/hslf/record/Record;)V

    .line 138
    .local v0, "newSAS":Lorg/apache/index/poi/hslf/record/SlideListWithText$SlideAtomsSet;
    iget-object v2, p0, Lorg/apache/index/poi/hslf/record/SlideListWithText;->slideAtomsSets:[Lorg/apache/index/poi/hslf/record/SlideListWithText$SlideAtomsSet;

    array-length v2, v2

    add-int/lit8 v2, v2, 0x1

    new-array v1, v2, [Lorg/apache/index/poi/hslf/record/SlideListWithText$SlideAtomsSet;

    .line 139
    .local v1, "sas":[Lorg/apache/index/poi/hslf/record/SlideListWithText$SlideAtomsSet;
    iget-object v2, p0, Lorg/apache/index/poi/hslf/record/SlideListWithText;->slideAtomsSets:[Lorg/apache/index/poi/hslf/record/SlideListWithText$SlideAtomsSet;

    iget-object v3, p0, Lorg/apache/index/poi/hslf/record/SlideListWithText;->slideAtomsSets:[Lorg/apache/index/poi/hslf/record/SlideListWithText$SlideAtomsSet;

    array-length v3, v3

    invoke-static {v2, v4, v1, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 140
    array-length v2, v1

    add-int/lit8 v2, v2, -0x1

    aput-object v0, v1, v2

    .line 141
    iput-object v1, p0, Lorg/apache/index/poi/hslf/record/SlideListWithText;->slideAtomsSets:[Lorg/apache/index/poi/hslf/record/SlideListWithText$SlideAtomsSet;

    .line 142
    return-void
.end method

.method public getInstance()I
    .locals 2

    .prologue
    .line 145
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/SlideListWithText;->_header:[B

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lorg/apache/index/poi/util/LittleEndian;->getShort([BI)S

    move-result v0

    shr-int/lit8 v0, v0, 0x4

    return v0
.end method

.method public getRecordType()J
    .locals 2

    .prologue
    .line 165
    sget-wide v0, Lorg/apache/index/poi/hslf/record/SlideListWithText;->_type:J

    return-wide v0
.end method

.method public getSlideAtomsSets()[Lorg/apache/index/poi/hslf/record/SlideListWithText$SlideAtomsSet;
    .locals 1

    .prologue
    .line 155
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/SlideListWithText;->slideAtomsSets:[Lorg/apache/index/poi/hslf/record/SlideListWithText$SlideAtomsSet;

    return-object v0
.end method

.method public setInstance(I)V
    .locals 2
    .param p1, "inst"    # I

    .prologue
    .line 149
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/SlideListWithText;->_header:[B

    shl-int/lit8 v1, p1, 0x4

    or-int/lit8 v1, v1, 0xf

    int-to-short v1, v1

    invoke-static {v0, v1}, Lorg/apache/index/poi/util/LittleEndian;->putShort([BS)V

    .line 150
    return-void
.end method

.method public setSlideAtomsSets([Lorg/apache/index/poi/hslf/record/SlideListWithText$SlideAtomsSet;)V
    .locals 0
    .param p1, "sas"    # [Lorg/apache/index/poi/hslf/record/SlideListWithText$SlideAtomsSet;

    .prologue
    .line 160
    iput-object p1, p0, Lorg/apache/index/poi/hslf/record/SlideListWithText;->slideAtomsSets:[Lorg/apache/index/poi/hslf/record/SlideListWithText$SlideAtomsSet;

    return-void
.end method
