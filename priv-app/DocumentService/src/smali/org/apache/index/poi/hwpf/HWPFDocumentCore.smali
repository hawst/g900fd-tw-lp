.class public abstract Lorg/apache/index/poi/hwpf/HWPFDocumentCore;
.super Lorg/apache/index/poi/POIDocument;
.source "HWPFDocumentCore.java"


# static fields
.field private static final MAX_DOCHEADER_SIZE:J = 0x19000L


# instance fields
.field protected _cbt:Lorg/apache/index/poi/hwpf/model/CHPBinTable;

.field protected _fib:Lorg/apache/index/poi/hwpf/model/FileInformationBlock;

.field protected _mainStream:[B

.field protected _mainStreamDoc:Lorg/apache/index/poi/poifs/filesystem/DocumentInputStream;

.field protected _pbt:Lorg/apache/index/poi/hwpf/model/PAPBinTable;

.field protected _st:Lorg/apache/index/poi/hwpf/model/SectionTable;


# direct methods
.method protected constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 68
    invoke-direct {p0, v0}, Lorg/apache/index/poi/POIDocument;-><init>(Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;)V

    .line 49
    iput-object v0, p0, Lorg/apache/index/poi/hwpf/HWPFDocumentCore;->_mainStreamDoc:Lorg/apache/index/poi/poifs/filesystem/DocumentInputStream;

    .line 69
    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 1
    .param p1, "istream"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 106
    invoke-static {p1}, Lorg/apache/index/poi/hwpf/HWPFDocumentCore;->verifyAndBuildPOIFS(Ljava/io/InputStream;)Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/apache/index/poi/hwpf/HWPFDocumentCore;-><init>(Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;)V

    .line 107
    return-void
.end method

.method public constructor <init>(Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;)V
    .locals 10
    .param p1, "directory"    # Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/32 v8, 0x19000

    const/4 v6, 0x0

    .line 133
    invoke-direct {p0, p1}, Lorg/apache/index/poi/POIDocument;-><init>(Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;)V

    .line 49
    iput-object v6, p0, Lorg/apache/index/poi/hwpf/HWPFDocumentCore;->_mainStreamDoc:Lorg/apache/index/poi/poifs/filesystem/DocumentInputStream;

    .line 137
    const-string/jumbo v3, "WordDocument"

    invoke-virtual {p1, v3}, Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;->getEntry(Ljava/lang/String;)Lorg/apache/index/poi/poifs/filesystem/Entry;

    move-result-object v1

    .line 136
    check-cast v1, Lorg/apache/index/poi/poifs/filesystem/DocumentEntry;

    .line 139
    .local v1, "documentProps":Lorg/apache/index/poi/poifs/filesystem/DocumentEntry;
    invoke-interface {v1}, Lorg/apache/index/poi/poifs/filesystem/DocumentEntry;->getSize()I

    move-result v0

    .line 146
    .local v0, "docsize":I
    int-to-long v4, v0

    cmp-long v3, v4, v8

    if-gez v3, :cond_0

    .line 148
    new-array v3, v0, [B

    iput-object v3, p0, Lorg/apache/index/poi/hwpf/HWPFDocumentCore;->_mainStream:[B

    .line 155
    :goto_0
    const-string/jumbo v3, "WordDocument"

    invoke-virtual {p1, v3}, Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;->createDocumentInputStream(Ljava/lang/String;)Lorg/apache/index/poi/poifs/filesystem/DocumentInputStream;

    move-result-object v3

    iput-object v3, p0, Lorg/apache/index/poi/hwpf/HWPFDocumentCore;->_mainStreamDoc:Lorg/apache/index/poi/poifs/filesystem/DocumentInputStream;

    .line 157
    iget-object v3, p0, Lorg/apache/index/poi/hwpf/HWPFDocumentCore;->_mainStreamDoc:Lorg/apache/index/poi/poifs/filesystem/DocumentInputStream;

    iget-object v4, p0, Lorg/apache/index/poi/hwpf/HWPFDocumentCore;->_mainStream:[B

    invoke-virtual {v3, v4}, Lorg/apache/index/poi/poifs/filesystem/DocumentInputStream;->read([B)I

    move-result v2

    .line 158
    .local v2, "read":I
    if-gez v2, :cond_1

    .line 159
    new-instance v3, Ljava/io/IOException;

    invoke-direct {v3}, Ljava/io/IOException;-><init>()V

    throw v3

    .line 152
    .end local v2    # "read":I
    :cond_0
    const v3, 0x19000

    new-array v3, v3, [B

    iput-object v3, p0, Lorg/apache/index/poi/hwpf/HWPFDocumentCore;->_mainStream:[B

    goto :goto_0

    .line 160
    .restart local v2    # "read":I
    :cond_1
    iget-object v3, p0, Lorg/apache/index/poi/hwpf/HWPFDocumentCore;->_mainStreamDoc:Lorg/apache/index/poi/poifs/filesystem/DocumentInputStream;

    invoke-virtual {v3}, Lorg/apache/index/poi/poifs/filesystem/DocumentInputStream;->reset()V

    .line 162
    int-to-long v4, v0

    cmp-long v3, v4, v8

    if-gez v3, :cond_2

    .line 164
    iput-object v6, p0, Lorg/apache/index/poi/hwpf/HWPFDocumentCore;->_mainStreamDoc:Lorg/apache/index/poi/poifs/filesystem/DocumentInputStream;

    .line 169
    :cond_2
    new-instance v3, Lorg/apache/index/poi/hwpf/model/FileInformationBlock;

    iget-object v4, p0, Lorg/apache/index/poi/hwpf/HWPFDocumentCore;->_mainStream:[B

    invoke-direct {v3, v4}, Lorg/apache/index/poi/hwpf/model/FileInformationBlock;-><init>([B)V

    iput-object v3, p0, Lorg/apache/index/poi/hwpf/HWPFDocumentCore;->_fib:Lorg/apache/index/poi/hwpf/model/FileInformationBlock;

    .line 170
    iget-object v3, p0, Lorg/apache/index/poi/hwpf/HWPFDocumentCore;->_fib:Lorg/apache/index/poi/hwpf/model/FileInformationBlock;

    invoke-virtual {v3}, Lorg/apache/index/poi/hwpf/model/FileInformationBlock;->isFEncrypted()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 171
    new-instance v3, Lorg/apache/index/poi/EncryptedDocumentException;

    const-string/jumbo v4, "Cannot process encrypted word files!"

    invoke-direct {v3, v4}, Lorg/apache/index/poi/EncryptedDocumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 173
    :cond_3
    return-void
.end method

.method public constructor <init>(Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;)V
    .locals 1
    .param p1, "pfilesystem"    # Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 118
    invoke-virtual {p1}, Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;->getRoot()Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/apache/index/poi/hwpf/HWPFDocumentCore;-><init>(Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;)V

    .line 119
    return-void
.end method

.method public static verifyAndBuildPOIFS(Ljava/io/InputStream;)Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;
    .locals 5
    .param p0, "istream"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x6

    .line 77
    new-instance v2, Ljava/io/PushbackInputStream;

    invoke-direct {v2, p0, v3}, Ljava/io/PushbackInputStream;-><init>(Ljava/io/InputStream;I)V

    .line 78
    .local v2, "pis":Ljava/io/PushbackInputStream;
    new-array v0, v3, [B

    .line 79
    .local v0, "first6":[B
    invoke-virtual {v2, v0}, Ljava/io/PushbackInputStream;->read([B)I

    move-result v1

    .line 80
    .local v1, "len":I
    if-gez v1, :cond_0

    .line 81
    new-instance v3, Ljava/io/IOException;

    invoke-direct {v3}, Ljava/io/IOException;-><init>()V

    throw v3

    .line 85
    :cond_0
    const/4 v3, 0x0

    aget-byte v3, v0, v3

    const/16 v4, 0x7b

    if-ne v3, v4, :cond_1

    const/4 v3, 0x1

    aget-byte v3, v0, v3

    const/16 v4, 0x5c

    if-ne v3, v4, :cond_1

    const/4 v3, 0x2

    aget-byte v3, v0, v3

    const/16 v4, 0x72

    if-ne v3, v4, :cond_1

    .line 86
    const/4 v3, 0x3

    aget-byte v3, v0, v3

    const/16 v4, 0x74

    if-ne v3, v4, :cond_1

    const/4 v3, 0x4

    aget-byte v3, v0, v3

    const/16 v4, 0x66

    if-ne v3, v4, :cond_1

    .line 87
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v4, "The document is really a RTF file"

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 92
    :cond_1
    invoke-virtual {v2, v0}, Ljava/io/PushbackInputStream;->unread([B)V

    .line 93
    new-instance v3, Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;

    const/4 v4, 0x0

    invoke-direct {v3, v2, v4}, Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;-><init>(Ljava/io/InputStream;Ljava/io/File;)V

    return-object v3
.end method


# virtual methods
.method public getCharacterTable()Lorg/apache/index/poi/hwpf/model/CHPBinTable;
    .locals 1

    .prologue
    .line 185
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/HWPFDocumentCore;->_cbt:Lorg/apache/index/poi/hwpf/model/CHPBinTable;

    return-object v0
.end method

.method public getFileInformationBlock()Lorg/apache/index/poi/hwpf/model/FileInformationBlock;
    .locals 1

    .prologue
    .line 200
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/HWPFDocumentCore;->_fib:Lorg/apache/index/poi/hwpf/model/FileInformationBlock;

    return-object v0
.end method

.method public getParagraphTable()Lorg/apache/index/poi/hwpf/model/PAPBinTable;
    .locals 1

    .prologue
    .line 190
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/HWPFDocumentCore;->_pbt:Lorg/apache/index/poi/hwpf/model/PAPBinTable;

    return-object v0
.end method

.method public abstract getRange()Lorg/apache/index/poi/hwpf/usermodel/Range;
.end method

.method public getSectionTable()Lorg/apache/index/poi/hwpf/model/SectionTable;
    .locals 1

    .prologue
    .line 195
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/HWPFDocumentCore;->_st:Lorg/apache/index/poi/hwpf/model/SectionTable;

    return-object v0
.end method

.method public abstract getTextTable()Lorg/apache/index/poi/hwpf/model/TextPieceTable;
.end method
