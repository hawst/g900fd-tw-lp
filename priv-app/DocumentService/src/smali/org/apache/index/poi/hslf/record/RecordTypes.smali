.class public final Lorg/apache/index/poi/hslf/record/RecordTypes;
.super Ljava/lang/Object;
.source "RecordTypes.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/index/poi/hslf/record/RecordTypes$Type;
    }
.end annotation


# static fields
.field public static final BaseTextPropAtom:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

.field public static final BinaryTagData:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

.field public static final BookmarkCollection:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

.field public static final BookmarkEntityAtom:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

.field public static final BookmarkSeedAtom:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

.field public static final CString:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

.field public static final CharFormatAtom:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

.field public static final Comment2000:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

.field public static final Comment2000Atom:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

.field public static final Comment2000Summary:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

.field public static final Comment2000SummaryAtom:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

.field public static final CompositeMasterId:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

.field public static final CurrentUserAtom:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

.field public static final DateTimeMCAtom:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

.field public static final DefaultRulerAtom:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

.field public static final DocRoutingSlip:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

.field public static final Document:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

.field public static final DocumentAtom:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

.field public static final DocumentEncryptionAtom:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

.field public static final EndDocument:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

.field public static final Environment:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

.field public static final EscherAlignRule:I = 0xf013

.field public static final EscherAnchor:I = 0xf00e

.field public static final EscherArcRule:I = 0xf014

.field public static final EscherBSE:I = 0xf007

.field public static final EscherBStoreContainer:I = 0xf001

.field public static final EscherBlip_END:I = 0xf117

.field public static final EscherBlip_START:I = 0xf018

.field public static final EscherCLSID:I = 0xf016

.field public static final EscherCalloutRule:I = 0xf017

.field public static final EscherChildAnchor:I = 0xf00f

.field public static final EscherClientAnchor:I = 0xf010

.field public static final EscherClientData:I = 0xf011

.field public static final EscherClientRule:I = 0xf015

.field public static final EscherClientTextbox:I = 0xf00d

.field public static final EscherColorMRU:I = 0xf11a

.field public static final EscherColorScheme:I = 0xf120

.field public static final EscherConnectorRule:I = 0xf012

.field public static final EscherDeletedPspl:I = 0xf11d

.field public static final EscherDg:I = 0xf008

.field public static final EscherDgContainer:I = 0xf002

.field public static final EscherDgg:I = 0xf006

.field public static final EscherDggContainer:I = 0xf000

.field public static final EscherOPT:I = 0xf00b

.field public static final EscherOleObject:I = 0xf11f

.field public static final EscherRegroupItems:I = 0xf118

.field public static final EscherSelection:I = 0xf119

.field public static final EscherSolverContainer:I = 0xf005

.field public static final EscherSp:I = 0xf00a

.field public static final EscherSpContainer:I = 0xf004

.field public static final EscherSpgr:I = 0xf009

.field public static final EscherSpgrContainer:I = 0xf003

.field public static final EscherSplitMenuColors:I = 0xf11e

.field public static final EscherTextbox:I = 0xf00c

.field public static final EscherUserDefined:I = 0xf122

.field public static final ExCDAudio:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

.field public static final ExCDAudioAtom:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

.field public static final ExHyperlink:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

.field public static final ExHyperlinkAtom:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

.field public static final ExLink:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

.field public static final ExLinkAtom:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

.field public static final ExMIDIAudio:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

.field public static final ExObjRefAtom:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

.field public static final ExQuickTimeMovie:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

.field public static final ExQuickTimeMovieData:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

.field public static final ExWAVAudioEmbedded:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

.field public static final ExWAVAudioEmbeddedAtom:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

.field public static final ExWAVAudioLink:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

.field public static final FontEmbeddedData:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

.field public static final FooterMCAtom:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

.field public static final GPopublicintAtom:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

.field public static final GRColorAtom:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

.field public static final GRatioAtom:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

.field public static final GScalingAtom:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

.field public static final GenericDateMCAtom:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

.field public static final GuideAtom:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

.field public static final HandOut:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

.field public static final HeadersFooters:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

.field public static final HeadersFootersAtom:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

.field public static final InteractiveInfo:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

.field public static final InteractiveInfoAtom:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

.field public static final List:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

.field public static final MainMaster:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

.field public static final MetaFile:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

.field public static final NamedShow:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

.field public static final NamedShowSlides:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

.field public static final NamedShows:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

.field public static final Notes:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

.field public static final NotesAtom:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

.field public static final OEPlaceholderAtom:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

.field public static final OEShapeAtom:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

.field public static final OriginalMainMasterId:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

.field public static final OutlineTextRefAtom:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

.field public static final OutlineViewInfo:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

.field public static final PPDrawing:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

.field public static final PPDrawingGroup:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

.field public static final ParaFormatAtom:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

.field public static final PersistPtrFullBlock:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

.field public static final PersistPtrIncrementalBlock:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

.field public static final ProgBinaryTag:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

.field public static final ProgStringTag:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

.field public static final ProgTags:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

.field public static final PrpublicintOptions:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

.field public static final RTFDateTimeMCAtom:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

.field public static final RecolorInfoAtom:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

.field public static final RoundTripContentMasterId:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

.field public static final RoundTripContentMasterInfo12:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

.field public static final RoundTripCustomTableStyles12:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

.field public static final RoundTripHFPlaceholder12:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

.field public static final RoundTripNotesMasterTextStyles12:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

.field public static final RoundTripOArtTextStyles12:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

.field public static final RoundTripShapeCheckSumForCustomLayouts12:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

.field public static final RoundTripShapeId12:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

.field public static final SSDocInfoAtom:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

.field public static final SSSlideInfoAtom:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

.field public static final SSlideLayoutAtom:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

.field public static final SheetProperties:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

.field public static final Slide:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

.field public static final SlideAtom:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

.field public static final SlideListWithText:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

.field public static final SlideNumberMCAtom:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

.field public static final SlidePersistAtom:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

.field public static final SlideViewInfo:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

.field public static final SlideViewInfoAtom:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

.field public static final SorterViewInfo:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

.field public static final SoundCollAtom:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

.field public static final SrKinsoku:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

.field public static final SrKinsokuAtom:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

.field public static final Summary:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

.field public static final TextBookmarkAtom:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

.field public static final TextBytesAtom:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

.field public static final TextCharsAtom:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

.field public static final TextHeaderAtom:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

.field public static final TextRulerAtom:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

.field public static final TextSpecInfoAtom:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

.field public static final TxCFStyleAtom:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

.field public static final TxInteractiveInfoAtom:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

.field public static final TxPFStyleAtom:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

.field public static final TxSIStyleAtom:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

.field public static final Unknown:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

.field public static final UserEditAtom:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

.field public static final VBAInfo:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

.field public static final VBAInfoAtom:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

.field public static final ViewInfo:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

.field public static final ViewInfoAtom:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

.field public static typeToClass:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Class",
            "<+",
            "Lorg/apache/index/poi/hslf/record/Record;",
            ">;>;"
        }
    .end annotation
.end field

.field public static typeToName:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 14

    .prologue
    const/16 v13, 0xbc1

    const/16 v12, 0x41c

    const/4 v11, 0x0

    .line 37
    new-instance v8, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    const/4 v9, 0x0

    invoke-direct {v8, v9, v11}, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    sput-object v8, Lorg/apache/index/poi/hslf/record/RecordTypes;->Unknown:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    .line 38
    new-instance v8, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    const/16 v9, 0x3e8

    const-class v10, Lorg/apache/index/poi/hslf/record/Document;

    invoke-direct {v8, v9, v10}, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    sput-object v8, Lorg/apache/index/poi/hslf/record/RecordTypes;->Document:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    .line 39
    new-instance v8, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    const/16 v9, 0x3e9

    const-class v10, Lorg/apache/index/poi/hslf/record/DocumentAtom;

    invoke-direct {v8, v9, v10}, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    sput-object v8, Lorg/apache/index/poi/hslf/record/RecordTypes;->DocumentAtom:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    .line 40
    new-instance v8, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    const/16 v9, 0x3ea

    invoke-direct {v8, v9, v11}, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    sput-object v8, Lorg/apache/index/poi/hslf/record/RecordTypes;->EndDocument:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    .line 41
    new-instance v8, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    const/16 v9, 0x3ee

    const-class v10, Lorg/apache/index/poi/hslf/record/Slide;

    invoke-direct {v8, v9, v10}, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    sput-object v8, Lorg/apache/index/poi/hslf/record/RecordTypes;->Slide:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    .line 42
    new-instance v8, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    const/16 v9, 0x3ef

    const-class v10, Lorg/apache/index/poi/hslf/record/SlideAtom;

    invoke-direct {v8, v9, v10}, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    sput-object v8, Lorg/apache/index/poi/hslf/record/RecordTypes;->SlideAtom:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    .line 43
    new-instance v8, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    const/16 v9, 0x3f0

    const-class v10, Lorg/apache/index/poi/hslf/record/Notes;

    invoke-direct {v8, v9, v10}, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    sput-object v8, Lorg/apache/index/poi/hslf/record/RecordTypes;->Notes:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    .line 44
    new-instance v8, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    const/16 v9, 0x3f1

    const-class v10, Lorg/apache/index/poi/hslf/record/NotesAtom;

    invoke-direct {v8, v9, v10}, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    sput-object v8, Lorg/apache/index/poi/hslf/record/RecordTypes;->NotesAtom:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    .line 45
    new-instance v8, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    const/16 v9, 0x3f2

    const-class v10, Lorg/apache/index/poi/hslf/record/Environment;

    invoke-direct {v8, v9, v10}, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    sput-object v8, Lorg/apache/index/poi/hslf/record/RecordTypes;->Environment:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    .line 46
    new-instance v8, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    const/16 v9, 0x3f3

    const-class v10, Lorg/apache/index/poi/hslf/record/SlidePersistAtom;

    invoke-direct {v8, v9, v10}, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    sput-object v8, Lorg/apache/index/poi/hslf/record/RecordTypes;->SlidePersistAtom:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    .line 47
    new-instance v8, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    const/16 v9, 0x3f7

    invoke-direct {v8, v9, v11}, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    sput-object v8, Lorg/apache/index/poi/hslf/record/RecordTypes;->SSlideLayoutAtom:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    .line 48
    new-instance v8, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    const/16 v9, 0x3f8

    const-class v10, Lorg/apache/index/poi/hslf/record/MainMaster;

    invoke-direct {v8, v9, v10}, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    sput-object v8, Lorg/apache/index/poi/hslf/record/RecordTypes;->MainMaster:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    .line 49
    new-instance v8, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    const/16 v9, 0x3f9

    invoke-direct {v8, v9, v11}, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    sput-object v8, Lorg/apache/index/poi/hslf/record/RecordTypes;->SSSlideInfoAtom:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    .line 50
    new-instance v8, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    const/16 v9, 0x3fa

    invoke-direct {v8, v9, v11}, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    sput-object v8, Lorg/apache/index/poi/hslf/record/RecordTypes;->SlideViewInfo:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    .line 51
    new-instance v8, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    const/16 v9, 0x3fb

    invoke-direct {v8, v9, v11}, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    sput-object v8, Lorg/apache/index/poi/hslf/record/RecordTypes;->GuideAtom:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    .line 52
    new-instance v8, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    const/16 v9, 0x3fc

    invoke-direct {v8, v9, v11}, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    sput-object v8, Lorg/apache/index/poi/hslf/record/RecordTypes;->ViewInfo:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    .line 53
    new-instance v8, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    const/16 v9, 0x3fd

    invoke-direct {v8, v9, v11}, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    sput-object v8, Lorg/apache/index/poi/hslf/record/RecordTypes;->ViewInfoAtom:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    .line 54
    new-instance v8, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    const/16 v9, 0x3fe

    invoke-direct {v8, v9, v11}, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    sput-object v8, Lorg/apache/index/poi/hslf/record/RecordTypes;->SlideViewInfoAtom:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    .line 55
    new-instance v8, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    const/16 v9, 0x3ff

    invoke-direct {v8, v9, v11}, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    sput-object v8, Lorg/apache/index/poi/hslf/record/RecordTypes;->VBAInfo:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    .line 56
    new-instance v8, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    const/16 v9, 0x400

    invoke-direct {v8, v9, v11}, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    sput-object v8, Lorg/apache/index/poi/hslf/record/RecordTypes;->VBAInfoAtom:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    .line 57
    new-instance v8, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    const/16 v9, 0x401

    invoke-direct {v8, v9, v11}, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    sput-object v8, Lorg/apache/index/poi/hslf/record/RecordTypes;->SSDocInfoAtom:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    .line 58
    new-instance v8, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    const/16 v9, 0x402

    invoke-direct {v8, v9, v11}, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    sput-object v8, Lorg/apache/index/poi/hslf/record/RecordTypes;->Summary:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    .line 59
    new-instance v8, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    const/16 v9, 0x406

    invoke-direct {v8, v9, v11}, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    sput-object v8, Lorg/apache/index/poi/hslf/record/RecordTypes;->DocRoutingSlip:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    .line 60
    new-instance v8, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    const/16 v9, 0x407

    invoke-direct {v8, v9, v11}, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    sput-object v8, Lorg/apache/index/poi/hslf/record/RecordTypes;->OutlineViewInfo:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    .line 61
    new-instance v8, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    const/16 v9, 0x408

    invoke-direct {v8, v9, v11}, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    sput-object v8, Lorg/apache/index/poi/hslf/record/RecordTypes;->SorterViewInfo:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    .line 62
    new-instance v8, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    const/16 v9, 0x40b

    const-class v10, Lorg/apache/index/poi/hslf/record/PPDrawingGroup;

    invoke-direct {v8, v9, v10}, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    sput-object v8, Lorg/apache/index/poi/hslf/record/RecordTypes;->PPDrawingGroup:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    .line 63
    new-instance v8, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    const/16 v9, 0x40c

    const-class v10, Lorg/apache/index/poi/hslf/record/PPDrawing;

    invoke-direct {v8, v9, v10}, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    sput-object v8, Lorg/apache/index/poi/hslf/record/RecordTypes;->PPDrawing:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    .line 64
    new-instance v8, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    const/16 v9, 0x410

    invoke-direct {v8, v9, v11}, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    sput-object v8, Lorg/apache/index/poi/hslf/record/RecordTypes;->NamedShows:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    .line 65
    new-instance v8, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    const/16 v9, 0x411

    invoke-direct {v8, v9, v11}, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    sput-object v8, Lorg/apache/index/poi/hslf/record/RecordTypes;->NamedShow:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    .line 66
    new-instance v8, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    const/16 v9, 0x412

    invoke-direct {v8, v9, v11}, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    sput-object v8, Lorg/apache/index/poi/hslf/record/RecordTypes;->NamedShowSlides:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    .line 67
    new-instance v8, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    const/16 v9, 0x414

    invoke-direct {v8, v9, v11}, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    sput-object v8, Lorg/apache/index/poi/hslf/record/RecordTypes;->SheetProperties:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    .line 68
    new-instance v8, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    const/16 v9, 0x7d0

    invoke-direct {v8, v9, v11}, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    sput-object v8, Lorg/apache/index/poi/hslf/record/RecordTypes;->List:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    .line 69
    new-instance v8, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    const/16 v9, 0x7e3

    invoke-direct {v8, v9, v11}, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    sput-object v8, Lorg/apache/index/poi/hslf/record/RecordTypes;->BookmarkCollection:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    .line 70
    new-instance v8, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    const/16 v9, 0x7e5

    invoke-direct {v8, v9, v11}, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    sput-object v8, Lorg/apache/index/poi/hslf/record/RecordTypes;->SoundCollAtom:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    .line 71
    new-instance v8, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    const/16 v9, 0x7e9

    invoke-direct {v8, v9, v11}, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    sput-object v8, Lorg/apache/index/poi/hslf/record/RecordTypes;->BookmarkSeedAtom:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    .line 72
    new-instance v8, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    invoke-direct {v8, v13, v11}, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    sput-object v8, Lorg/apache/index/poi/hslf/record/RecordTypes;->ExObjRefAtom:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    .line 73
    new-instance v8, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    const-class v9, Lorg/apache/index/poi/hslf/record/OEShapeAtom;

    invoke-direct {v8, v13, v9}, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    sput-object v8, Lorg/apache/index/poi/hslf/record/RecordTypes;->OEShapeAtom:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    .line 74
    new-instance v8, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    const/16 v9, 0xbc3

    const-class v10, Lorg/apache/index/poi/hslf/record/OEPlaceholderAtom;

    invoke-direct {v8, v9, v10}, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    sput-object v8, Lorg/apache/index/poi/hslf/record/RecordTypes;->OEPlaceholderAtom:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    .line 75
    new-instance v8, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    const/16 v9, 0xbd0

    invoke-direct {v8, v9, v11}, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    sput-object v8, Lorg/apache/index/poi/hslf/record/RecordTypes;->GPopublicintAtom:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    .line 76
    new-instance v8, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    const/16 v9, 0xbd7

    invoke-direct {v8, v9, v11}, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    sput-object v8, Lorg/apache/index/poi/hslf/record/RecordTypes;->GRatioAtom:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    .line 77
    new-instance v8, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    const/16 v9, 0xf9e

    const-class v10, Lorg/apache/index/poi/hslf/record/OutlineTextRefAtom;

    invoke-direct {v8, v9, v10}, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    sput-object v8, Lorg/apache/index/poi/hslf/record/RecordTypes;->OutlineTextRefAtom:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    .line 78
    new-instance v8, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    const/16 v9, 0xf9f

    const-class v10, Lorg/apache/index/poi/hslf/record/TextHeaderAtom;

    invoke-direct {v8, v9, v10}, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    sput-object v8, Lorg/apache/index/poi/hslf/record/RecordTypes;->TextHeaderAtom:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    .line 79
    new-instance v8, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    const/16 v9, 0xfa0

    const-class v10, Lorg/apache/index/poi/hslf/record/TextCharsAtom;

    invoke-direct {v8, v9, v10}, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    sput-object v8, Lorg/apache/index/poi/hslf/record/RecordTypes;->TextCharsAtom:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    .line 80
    new-instance v8, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    const/16 v9, 0xfa2

    invoke-direct {v8, v9, v11}, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    sput-object v8, Lorg/apache/index/poi/hslf/record/RecordTypes;->BaseTextPropAtom:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    .line 81
    new-instance v8, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    const/16 v9, 0xfa4

    invoke-direct {v8, v9, v11}, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    sput-object v8, Lorg/apache/index/poi/hslf/record/RecordTypes;->TxCFStyleAtom:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    .line 82
    new-instance v8, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    const/16 v9, 0xfa5

    invoke-direct {v8, v9, v11}, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    sput-object v8, Lorg/apache/index/poi/hslf/record/RecordTypes;->TxPFStyleAtom:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    .line 83
    new-instance v8, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    const/16 v9, 0xfa6

    const-class v10, Lorg/apache/index/poi/hslf/record/TextRulerAtom;

    invoke-direct {v8, v9, v10}, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    sput-object v8, Lorg/apache/index/poi/hslf/record/RecordTypes;->TextRulerAtom:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    .line 84
    new-instance v8, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    const/16 v9, 0xfa7

    invoke-direct {v8, v9, v11}, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    sput-object v8, Lorg/apache/index/poi/hslf/record/RecordTypes;->TextBookmarkAtom:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    .line 85
    new-instance v8, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    const/16 v9, 0xfa8

    const-class v10, Lorg/apache/index/poi/hslf/record/TextBytesAtom;

    invoke-direct {v8, v9, v10}, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    sput-object v8, Lorg/apache/index/poi/hslf/record/RecordTypes;->TextBytesAtom:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    .line 86
    new-instance v8, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    const/16 v9, 0xfa9

    invoke-direct {v8, v9, v11}, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    sput-object v8, Lorg/apache/index/poi/hslf/record/RecordTypes;->TxSIStyleAtom:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    .line 87
    new-instance v8, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    const/16 v9, 0xfaa

    const-class v10, Lorg/apache/index/poi/hslf/record/TextSpecInfoAtom;

    invoke-direct {v8, v9, v10}, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    sput-object v8, Lorg/apache/index/poi/hslf/record/RecordTypes;->TextSpecInfoAtom:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    .line 88
    new-instance v8, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    const/16 v9, 0xfab

    invoke-direct {v8, v9, v11}, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    sput-object v8, Lorg/apache/index/poi/hslf/record/RecordTypes;->DefaultRulerAtom:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    .line 89
    new-instance v8, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    const/16 v9, 0xfb8

    invoke-direct {v8, v9, v11}, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    sput-object v8, Lorg/apache/index/poi/hslf/record/RecordTypes;->FontEmbeddedData:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    .line 90
    new-instance v8, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    const/16 v9, 0xfba

    const-class v10, Lorg/apache/index/poi/hslf/record/CString;

    invoke-direct {v8, v9, v10}, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    sput-object v8, Lorg/apache/index/poi/hslf/record/RecordTypes;->CString:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    .line 91
    new-instance v8, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    const/16 v9, 0xfc1

    invoke-direct {v8, v9, v11}, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    sput-object v8, Lorg/apache/index/poi/hslf/record/RecordTypes;->MetaFile:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    .line 92
    new-instance v8, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    const/16 v9, 0xfc8

    invoke-direct {v8, v9, v11}, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    sput-object v8, Lorg/apache/index/poi/hslf/record/RecordTypes;->SrKinsoku:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    .line 93
    new-instance v8, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    const/16 v9, 0xfc9

    const-class v10, Lorg/apache/index/poi/hslf/record/DummyPositionSensitiveRecordWithChildren;

    invoke-direct {v8, v9, v10}, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    sput-object v8, Lorg/apache/index/poi/hslf/record/RecordTypes;->HandOut:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    .line 94
    new-instance v8, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    const/16 v9, 0xfce

    invoke-direct {v8, v9, v11}, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    sput-object v8, Lorg/apache/index/poi/hslf/record/RecordTypes;->ExLink:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    .line 95
    new-instance v8, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    const/16 v9, 0xfd0

    invoke-direct {v8, v9, v11}, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    sput-object v8, Lorg/apache/index/poi/hslf/record/RecordTypes;->BookmarkEntityAtom:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    .line 96
    new-instance v8, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    const/16 v9, 0xfd1

    invoke-direct {v8, v9, v11}, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    sput-object v8, Lorg/apache/index/poi/hslf/record/RecordTypes;->ExLinkAtom:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    .line 97
    new-instance v8, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    const/16 v9, 0xfd2

    invoke-direct {v8, v9, v11}, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    sput-object v8, Lorg/apache/index/poi/hslf/record/RecordTypes;->SrKinsokuAtom:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    .line 98
    new-instance v8, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    const/16 v9, 0xfd3

    const-class v10, Lorg/apache/index/poi/hslf/record/ExHyperlinkAtom;

    invoke-direct {v8, v9, v10}, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    sput-object v8, Lorg/apache/index/poi/hslf/record/RecordTypes;->ExHyperlinkAtom:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    .line 99
    new-instance v8, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    const/16 v9, 0xfd7

    const-class v10, Lorg/apache/index/poi/hslf/record/ExHyperlink;

    invoke-direct {v8, v9, v10}, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    sput-object v8, Lorg/apache/index/poi/hslf/record/RecordTypes;->ExHyperlink:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    .line 100
    new-instance v8, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    const/16 v9, 0xfd8

    invoke-direct {v8, v9, v11}, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    sput-object v8, Lorg/apache/index/poi/hslf/record/RecordTypes;->SlideNumberMCAtom:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    .line 101
    new-instance v8, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    const/16 v9, 0xfd9

    const-class v10, Lorg/apache/index/poi/hslf/record/HeadersFootersContainer;

    invoke-direct {v8, v9, v10}, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    sput-object v8, Lorg/apache/index/poi/hslf/record/RecordTypes;->HeadersFooters:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    .line 102
    new-instance v8, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    const/16 v9, 0xfda

    const-class v10, Lorg/apache/index/poi/hslf/record/HeadersFootersAtom;

    invoke-direct {v8, v9, v10}, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    sput-object v8, Lorg/apache/index/poi/hslf/record/RecordTypes;->HeadersFootersAtom:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    .line 103
    new-instance v8, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    const/16 v9, 0xfdf

    const-class v10, Lorg/apache/index/poi/hslf/record/TxInteractiveInfoAtom;

    invoke-direct {v8, v9, v10}, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    sput-object v8, Lorg/apache/index/poi/hslf/record/RecordTypes;->TxInteractiveInfoAtom:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    .line 104
    new-instance v8, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    const/16 v9, 0xfe2

    invoke-direct {v8, v9, v11}, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    sput-object v8, Lorg/apache/index/poi/hslf/record/RecordTypes;->CharFormatAtom:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    .line 105
    new-instance v8, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    const/16 v9, 0xfe3

    invoke-direct {v8, v9, v11}, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    sput-object v8, Lorg/apache/index/poi/hslf/record/RecordTypes;->ParaFormatAtom:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    .line 106
    new-instance v8, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    const/16 v9, 0xfe7

    invoke-direct {v8, v9, v11}, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    sput-object v8, Lorg/apache/index/poi/hslf/record/RecordTypes;->RecolorInfoAtom:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    .line 107
    new-instance v8, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    const/16 v9, 0xfea

    invoke-direct {v8, v9, v11}, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    sput-object v8, Lorg/apache/index/poi/hslf/record/RecordTypes;->ExQuickTimeMovie:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    .line 108
    new-instance v8, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    const/16 v9, 0xfeb

    invoke-direct {v8, v9, v11}, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    sput-object v8, Lorg/apache/index/poi/hslf/record/RecordTypes;->ExQuickTimeMovieData:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    .line 109
    new-instance v8, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    const/16 v9, 0xff0

    const-class v10, Lorg/apache/index/poi/hslf/record/SlideListWithText;

    invoke-direct {v8, v9, v10}, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    sput-object v8, Lorg/apache/index/poi/hslf/record/RecordTypes;->SlideListWithText:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    .line 110
    new-instance v8, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    const/16 v9, 0xff2

    const-class v10, Lorg/apache/index/poi/hslf/record/InteractiveInfo;

    invoke-direct {v8, v9, v10}, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    sput-object v8, Lorg/apache/index/poi/hslf/record/RecordTypes;->InteractiveInfo:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    .line 111
    new-instance v8, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    const/16 v9, 0xff3

    const-class v10, Lorg/apache/index/poi/hslf/record/InteractiveInfoAtom;

    invoke-direct {v8, v9, v10}, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    sput-object v8, Lorg/apache/index/poi/hslf/record/RecordTypes;->InteractiveInfoAtom:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    .line 112
    new-instance v8, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    const/16 v9, 0xff5

    const-class v10, Lorg/apache/index/poi/hslf/record/UserEditAtom;

    invoke-direct {v8, v9, v10}, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    sput-object v8, Lorg/apache/index/poi/hslf/record/RecordTypes;->UserEditAtom:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    .line 113
    new-instance v8, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    const/16 v9, 0xff6

    invoke-direct {v8, v9, v11}, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    sput-object v8, Lorg/apache/index/poi/hslf/record/RecordTypes;->CurrentUserAtom:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    .line 114
    new-instance v8, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    const/16 v9, 0xff7

    invoke-direct {v8, v9, v11}, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    sput-object v8, Lorg/apache/index/poi/hslf/record/RecordTypes;->DateTimeMCAtom:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    .line 115
    new-instance v8, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    const/16 v9, 0xff8

    invoke-direct {v8, v9, v11}, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    sput-object v8, Lorg/apache/index/poi/hslf/record/RecordTypes;->GenericDateMCAtom:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    .line 116
    new-instance v8, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    const/16 v9, 0xffa

    invoke-direct {v8, v9, v11}, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    sput-object v8, Lorg/apache/index/poi/hslf/record/RecordTypes;->FooterMCAtom:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    .line 117
    new-instance v8, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    const/16 v9, 0x100d

    invoke-direct {v8, v9, v11}, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    sput-object v8, Lorg/apache/index/poi/hslf/record/RecordTypes;->ExMIDIAudio:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    .line 118
    new-instance v8, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    const/16 v9, 0x100e

    invoke-direct {v8, v9, v11}, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    sput-object v8, Lorg/apache/index/poi/hslf/record/RecordTypes;->ExCDAudio:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    .line 119
    new-instance v8, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    const/16 v9, 0x100f

    invoke-direct {v8, v9, v11}, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    sput-object v8, Lorg/apache/index/poi/hslf/record/RecordTypes;->ExWAVAudioEmbedded:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    .line 120
    new-instance v8, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    const/16 v9, 0x1010

    invoke-direct {v8, v9, v11}, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    sput-object v8, Lorg/apache/index/poi/hslf/record/RecordTypes;->ExWAVAudioLink:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    .line 121
    new-instance v8, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    const/16 v9, 0x1012

    invoke-direct {v8, v9, v11}, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    sput-object v8, Lorg/apache/index/poi/hslf/record/RecordTypes;->ExCDAudioAtom:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    .line 122
    new-instance v8, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    const/16 v9, 0x1013

    invoke-direct {v8, v9, v11}, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    sput-object v8, Lorg/apache/index/poi/hslf/record/RecordTypes;->ExWAVAudioEmbeddedAtom:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    .line 123
    new-instance v8, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    const/16 v9, 0x1015

    invoke-direct {v8, v9, v11}, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    sput-object v8, Lorg/apache/index/poi/hslf/record/RecordTypes;->RTFDateTimeMCAtom:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    .line 124
    new-instance v8, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    const/16 v9, 0x1388

    const-class v10, Lorg/apache/index/poi/hslf/record/DummyPositionSensitiveRecordWithChildren;

    invoke-direct {v8, v9, v10}, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    sput-object v8, Lorg/apache/index/poi/hslf/record/RecordTypes;->ProgTags:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    .line 125
    new-instance v8, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    const/16 v9, 0x1389

    invoke-direct {v8, v9, v11}, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    sput-object v8, Lorg/apache/index/poi/hslf/record/RecordTypes;->ProgStringTag:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    .line 126
    new-instance v8, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    const/16 v9, 0x138a

    const-class v10, Lorg/apache/index/poi/hslf/record/DummyPositionSensitiveRecordWithChildren;

    invoke-direct {v8, v9, v10}, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    sput-object v8, Lorg/apache/index/poi/hslf/record/RecordTypes;->ProgBinaryTag:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    .line 127
    new-instance v8, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    const/16 v9, 0x138b

    const-class v10, Lorg/apache/index/poi/hslf/record/DummyPositionSensitiveRecordWithChildren;

    invoke-direct {v8, v9, v10}, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    sput-object v8, Lorg/apache/index/poi/hslf/record/RecordTypes;->BinaryTagData:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    .line 128
    new-instance v8, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    const/16 v9, 0x1770

    invoke-direct {v8, v9, v11}, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    sput-object v8, Lorg/apache/index/poi/hslf/record/RecordTypes;->PrpublicintOptions:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    .line 129
    new-instance v8, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    const/16 v9, 0x1771

    const-class v10, Lorg/apache/index/poi/hslf/record/PersistPtrHolder;

    invoke-direct {v8, v9, v10}, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    sput-object v8, Lorg/apache/index/poi/hslf/record/RecordTypes;->PersistPtrFullBlock:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    .line 130
    new-instance v8, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    const/16 v9, 0x1772

    const-class v10, Lorg/apache/index/poi/hslf/record/PersistPtrHolder;

    invoke-direct {v8, v9, v10}, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    sput-object v8, Lorg/apache/index/poi/hslf/record/RecordTypes;->PersistPtrIncrementalBlock:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    .line 131
    new-instance v8, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    const/16 v9, 0x2711

    invoke-direct {v8, v9, v11}, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    sput-object v8, Lorg/apache/index/poi/hslf/record/RecordTypes;->GScalingAtom:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    .line 132
    new-instance v8, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    const/16 v9, 0x2712

    invoke-direct {v8, v9, v11}, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    sput-object v8, Lorg/apache/index/poi/hslf/record/RecordTypes;->GRColorAtom:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    .line 136
    new-instance v8, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    const/16 v9, 0x2ee0

    const-class v10, Lorg/apache/index/poi/hslf/record/Comment2000;

    invoke-direct {v8, v9, v10}, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    sput-object v8, Lorg/apache/index/poi/hslf/record/RecordTypes;->Comment2000:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    .line 137
    new-instance v8, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    const/16 v9, 0x2ee1

    const-class v10, Lorg/apache/index/poi/hslf/record/Comment2000Atom;

    invoke-direct {v8, v9, v10}, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    sput-object v8, Lorg/apache/index/poi/hslf/record/RecordTypes;->Comment2000Atom:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    .line 138
    new-instance v8, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    const/16 v9, 0x2ee4

    invoke-direct {v8, v9, v11}, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    sput-object v8, Lorg/apache/index/poi/hslf/record/RecordTypes;->Comment2000Summary:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    .line 139
    new-instance v8, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    const/16 v9, 0x2ee5

    invoke-direct {v8, v9, v11}, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    sput-object v8, Lorg/apache/index/poi/hslf/record/RecordTypes;->Comment2000SummaryAtom:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    .line 142
    new-instance v8, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    const/16 v9, 0x2f14

    const-class v10, Lorg/apache/index/poi/hslf/record/DocumentEncryptionAtom;

    invoke-direct {v8, v9, v10}, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    sput-object v8, Lorg/apache/index/poi/hslf/record/RecordTypes;->DocumentEncryptionAtom:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    .line 144
    new-instance v8, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    invoke-direct {v8, v12, v11}, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    sput-object v8, Lorg/apache/index/poi/hslf/record/RecordTypes;->OriginalMainMasterId:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    .line 145
    new-instance v8, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    invoke-direct {v8, v12, v11}, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    sput-object v8, Lorg/apache/index/poi/hslf/record/RecordTypes;->CompositeMasterId:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    .line 146
    new-instance v8, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    const/16 v9, 0x41e

    invoke-direct {v8, v9, v11}, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    sput-object v8, Lorg/apache/index/poi/hslf/record/RecordTypes;->RoundTripContentMasterInfo12:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    .line 147
    new-instance v8, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    const/16 v9, 0x41f

    invoke-direct {v8, v9, v11}, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    sput-object v8, Lorg/apache/index/poi/hslf/record/RecordTypes;->RoundTripShapeId12:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    .line 148
    new-instance v8, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    const/16 v9, 0x420

    const-class v10, Lorg/apache/index/poi/hslf/record/RoundTripHFPlaceholder12;

    invoke-direct {v8, v9, v10}, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    sput-object v8, Lorg/apache/index/poi/hslf/record/RecordTypes;->RoundTripHFPlaceholder12:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    .line 149
    new-instance v8, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    const/16 v9, 0x422

    invoke-direct {v8, v9, v11}, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    sput-object v8, Lorg/apache/index/poi/hslf/record/RecordTypes;->RoundTripContentMasterId:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    .line 150
    new-instance v8, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    const/16 v9, 0x423

    invoke-direct {v8, v9, v11}, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    sput-object v8, Lorg/apache/index/poi/hslf/record/RecordTypes;->RoundTripOArtTextStyles12:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    .line 151
    new-instance v8, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    const/16 v9, 0x426

    invoke-direct {v8, v9, v11}, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    sput-object v8, Lorg/apache/index/poi/hslf/record/RecordTypes;->RoundTripShapeCheckSumForCustomLayouts12:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    .line 152
    new-instance v8, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    const/16 v9, 0x427

    invoke-direct {v8, v9, v11}, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    sput-object v8, Lorg/apache/index/poi/hslf/record/RecordTypes;->RoundTripNotesMasterTextStyles12:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    .line 153
    new-instance v8, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    const/16 v9, 0x428

    invoke-direct {v8, v9, v11}, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;-><init>(ILjava/lang/Class;)V

    sput-object v8, Lorg/apache/index/poi/hslf/record/RecordTypes;->RoundTripCustomTableStyles12:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    .line 218
    new-instance v8, Ljava/util/HashMap;

    invoke-direct {v8}, Ljava/util/HashMap;-><init>()V

    sput-object v8, Lorg/apache/index/poi/hslf/record/RecordTypes;->typeToName:Ljava/util/HashMap;

    .line 219
    new-instance v8, Ljava/util/HashMap;

    invoke-direct {v8}, Ljava/util/HashMap;-><init>()V

    sput-object v8, Lorg/apache/index/poi/hslf/record/RecordTypes;->typeToClass:Ljava/util/HashMap;

    .line 221
    :try_start_0
    const-class v8, Lorg/apache/index/poi/hslf/record/RecordTypes;

    invoke-virtual {v8}, Ljava/lang/Class;->getFields()[Ljava/lang/reflect/Field;

    move-result-object v3

    .line 222
    .local v3, "f":[Ljava/lang/reflect/Field;
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    array-length v8, v3

    if-lt v4, v8, :cond_0

    .line 243
    return-void

    .line 223
    :cond_0
    aget-object v8, v3, v4

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    .line 226
    .local v7, "val":Ljava/lang/Object;
    instance-of v8, v7, Ljava/lang/Integer;

    if-eqz v8, :cond_1

    .line 227
    sget-object v9, Lorg/apache/index/poi/hslf/record/RecordTypes;->typeToName:Ljava/util/HashMap;

    move-object v0, v7

    nop

    nop

    move-object v8, v0

    aget-object v10, v3, v4

    invoke-virtual {v10}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v8, v10}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 230
    :cond_1
    instance-of v8, v7, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    if-eqz v8, :cond_3

    .line 231
    move-object v0, v7

    nop

    nop

    move-object v6, v0

    .line 232
    .local v6, "t":Lorg/apache/index/poi/hslf/record/RecordTypes$Type;
    iget-object v1, v6, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;->handlingClass:Ljava/lang/Class;

    .line 233
    .local v1, "c":Ljava/lang/Class;, "Ljava/lang/Class<+Lorg/apache/index/poi/hslf/record/Record;>;"
    iget v8, v6, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;->typeID:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    .line 234
    .local v5, "id":Ljava/lang/Integer;
    if-nez v1, :cond_2

    const-class v1, Lorg/apache/index/poi/hslf/record/UnknownRecordPlaceholder;

    .line 236
    :cond_2
    sget-object v8, Lorg/apache/index/poi/hslf/record/RecordTypes;->typeToName:Ljava/util/HashMap;

    aget-object v9, v3, v4

    invoke-virtual {v9}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v5, v9}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 237
    sget-object v8, Lorg/apache/index/poi/hslf/record/RecordTypes;->typeToClass:Ljava/util/HashMap;

    invoke-virtual {v8, v5, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0

    .line 222
    .end local v1    # "c":Ljava/lang/Class;, "Ljava/lang/Class<+Lorg/apache/index/poi/hslf/record/Record;>;"
    .end local v5    # "id":Ljava/lang/Integer;
    .end local v6    # "t":Lorg/apache/index/poi/hslf/record/RecordTypes$Type;
    :cond_3
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 240
    .end local v7    # "val":Ljava/lang/Object;
    :catch_0
    move-exception v2

    .line 241
    .local v2, "e":Ljava/lang/IllegalAccessException;
    new-instance v8, Ljava/lang/RuntimeException;

    const-string/jumbo v9, "Failed to initialize records types"

    invoke-direct {v8, v9}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v8
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static recordHandlingClass(I)Ljava/lang/Class;
    .locals 3
    .param p0, "type"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/lang/Class",
            "<+",
            "Lorg/apache/index/poi/hslf/record/Record;",
            ">;"
        }
    .end annotation

    .prologue
    .line 213
    sget-object v1, Lorg/apache/index/poi/hslf/record/RecordTypes;->typeToClass:Ljava/util/HashMap;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    .line 214
    .local v0, "c":Ljava/lang/Class;, "Ljava/lang/Class<+Lorg/apache/index/poi/hslf/record/Record;>;"
    return-object v0
.end method

.method public static recordName(I)Ljava/lang/String;
    .locals 3
    .param p0, "type"    # I

    .prologue
    .line 198
    sget-object v1, Lorg/apache/index/poi/hslf/record/RecordTypes;->typeToName:Ljava/util/HashMap;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 199
    .local v0, "name":Ljava/lang/String;
    if-nez v0, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Unknown"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 200
    :cond_0
    return-object v0
.end method
