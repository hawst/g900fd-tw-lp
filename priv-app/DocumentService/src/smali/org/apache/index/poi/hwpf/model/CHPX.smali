.class public final Lorg/apache/index/poi/hwpf/model/CHPX;
.super Lorg/apache/index/poi/hwpf/model/BytePropertyNode;
.source "CHPX.java"


# direct methods
.method public constructor <init>(IILorg/apache/index/poi/hwpf/model/CharIndexTranslator;Lorg/apache/index/poi/hwpf/sprm/SprmBuffer;)V
    .locals 1
    .param p1, "fcStart"    # I
    .param p2, "fcEnd"    # I
    .param p3, "translator"    # Lorg/apache/index/poi/hwpf/model/CharIndexTranslator;
    .param p4, "buf"    # Lorg/apache/index/poi/hwpf/sprm/SprmBuffer;

    .prologue
    .line 42
    invoke-interface {p3, p2}, Lorg/apache/index/poi/hwpf/model/CharIndexTranslator;->lookIndexBackward(I)I

    move-result v0

    invoke-direct {p0, p1, v0, p3, p4}, Lorg/apache/index/poi/hwpf/model/BytePropertyNode;-><init>(IILorg/apache/index/poi/hwpf/model/CharIndexTranslator;Ljava/lang/Object;)V

    .line 43
    return-void
.end method

.method public constructor <init>(IILorg/apache/index/poi/hwpf/model/CharIndexTranslator;[B)V
    .locals 2
    .param p1, "fcStart"    # I
    .param p2, "fcEnd"    # I
    .param p3, "translator"    # Lorg/apache/index/poi/hwpf/model/CharIndexTranslator;
    .param p4, "grpprl"    # [B

    .prologue
    .line 37
    invoke-interface {p3, p2}, Lorg/apache/index/poi/hwpf/model/CharIndexTranslator;->lookIndexBackward(I)I

    move-result v0

    new-instance v1, Lorg/apache/index/poi/hwpf/sprm/SprmBuffer;

    invoke-direct {v1, p4}, Lorg/apache/index/poi/hwpf/sprm/SprmBuffer;-><init>([B)V

    invoke-direct {p0, p1, v0, p3, v1}, Lorg/apache/index/poi/hwpf/model/BytePropertyNode;-><init>(IILorg/apache/index/poi/hwpf/model/CharIndexTranslator;Ljava/lang/Object;)V

    .line 38
    return-void
.end method


# virtual methods
.method public getGrpprl()[B
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/CHPX;->_buf:Ljava/lang/Object;

    check-cast v0, Lorg/apache/index/poi/hwpf/sprm/SprmBuffer;

    invoke-virtual {v0}, Lorg/apache/index/poi/hwpf/sprm/SprmBuffer;->toByteArray()[B

    move-result-object v0

    return-object v0
.end method

.method public getSprmBuf()Lorg/apache/index/poi/hwpf/sprm/SprmBuffer;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/CHPX;->_buf:Ljava/lang/Object;

    check-cast v0, Lorg/apache/index/poi/hwpf/sprm/SprmBuffer;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 57
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "CHPX from "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/model/CHPX;->getStart()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " to "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/model/CHPX;->getEnd()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 58
    const-string/jumbo v1, " (in bytes "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/model/CHPX;->getStartBytes()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " to "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/model/CHPX;->getEndBytes()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 57
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
