.class public Lorg/apache/index/poi/hpsf/MutablePropertySet;
.super Lorg/apache/index/poi/hpsf/PropertySet;
.source "MutablePropertySet.java"


# instance fields
.field private final OFFSET_HEADER:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 57
    invoke-direct {p0}, Lorg/apache/index/poi/hpsf/PropertySet;-><init>()V

    .line 110
    sget-object v0, Lorg/apache/index/poi/hpsf/MutablePropertySet;->BYTE_ORDER_ASSERTION:[B

    array-length v0, v0

    .line 111
    sget-object v1, Lorg/apache/index/poi/hpsf/MutablePropertySet;->FORMAT_ASSERTION:[B

    array-length v1, v1

    .line 110
    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x4

    add-int/lit8 v0, v0, 0x10

    add-int/lit8 v0, v0, 0x4

    iput v0, p0, Lorg/apache/index/poi/hpsf/MutablePropertySet;->OFFSET_HEADER:I

    .line 60
    sget-object v0, Lorg/apache/index/poi/hpsf/MutablePropertySet;->BYTE_ORDER_ASSERTION:[B

    invoke-static {v0}, Lorg/apache/index/poi/util/LittleEndian;->getUShort([B)I

    move-result v0

    iput v0, p0, Lorg/apache/index/poi/hpsf/MutablePropertySet;->byteOrder:I

    .line 63
    sget-object v0, Lorg/apache/index/poi/hpsf/MutablePropertySet;->FORMAT_ASSERTION:[B

    invoke-static {v0}, Lorg/apache/index/poi/util/LittleEndian;->getUShort([B)I

    move-result v0

    iput v0, p0, Lorg/apache/index/poi/hpsf/MutablePropertySet;->format:I

    .line 67
    const v0, 0x20a04

    iput v0, p0, Lorg/apache/index/poi/hpsf/MutablePropertySet;->osVersion:I

    .line 70
    new-instance v0, Lorg/apache/index/poi/hpsf/ClassID;

    invoke-direct {v0}, Lorg/apache/index/poi/hpsf/ClassID;-><init>()V

    iput-object v0, p0, Lorg/apache/index/poi/hpsf/MutablePropertySet;->classID:Lorg/apache/index/poi/hpsf/ClassID;

    .line 74
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lorg/apache/index/poi/hpsf/MutablePropertySet;->sections:Ljava/util/List;

    .line 75
    iget-object v0, p0, Lorg/apache/index/poi/hpsf/MutablePropertySet;->sections:Ljava/util/List;

    new-instance v1, Lorg/apache/index/poi/hpsf/MutableSection;

    invoke-direct {v1}, Lorg/apache/index/poi/hpsf/MutableSection;-><init>()V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 76
    return-void
.end method

.method public constructor <init>(Lorg/apache/index/poi/hpsf/PropertySet;)V
    .locals 4
    .param p1, "ps"    # Lorg/apache/index/poi/hpsf/PropertySet;

    .prologue
    .line 88
    invoke-direct {p0}, Lorg/apache/index/poi/hpsf/PropertySet;-><init>()V

    .line 110
    sget-object v2, Lorg/apache/index/poi/hpsf/MutablePropertySet;->BYTE_ORDER_ASSERTION:[B

    array-length v2, v2

    .line 111
    sget-object v3, Lorg/apache/index/poi/hpsf/MutablePropertySet;->FORMAT_ASSERTION:[B

    array-length v3, v3

    .line 110
    add-int/2addr v2, v3

    add-int/lit8 v2, v2, 0x4

    add-int/lit8 v2, v2, 0x10

    add-int/lit8 v2, v2, 0x4

    iput v2, p0, Lorg/apache/index/poi/hpsf/MutablePropertySet;->OFFSET_HEADER:I

    .line 90
    invoke-virtual {p1}, Lorg/apache/index/poi/hpsf/PropertySet;->getByteOrder()I

    move-result v2

    iput v2, p0, Lorg/apache/index/poi/hpsf/MutablePropertySet;->byteOrder:I

    .line 91
    invoke-virtual {p1}, Lorg/apache/index/poi/hpsf/PropertySet;->getFormat()I

    move-result v2

    iput v2, p0, Lorg/apache/index/poi/hpsf/MutablePropertySet;->format:I

    .line 92
    invoke-virtual {p1}, Lorg/apache/index/poi/hpsf/PropertySet;->getOSVersion()I

    move-result v2

    iput v2, p0, Lorg/apache/index/poi/hpsf/MutablePropertySet;->osVersion:I

    .line 93
    invoke-virtual {p1}, Lorg/apache/index/poi/hpsf/PropertySet;->getClassID()Lorg/apache/index/poi/hpsf/ClassID;

    move-result-object v2

    invoke-virtual {p0, v2}, Lorg/apache/index/poi/hpsf/MutablePropertySet;->setClassID(Lorg/apache/index/poi/hpsf/ClassID;)V

    .line 94
    invoke-virtual {p0}, Lorg/apache/index/poi/hpsf/MutablePropertySet;->clearSections()V

    .line 95
    iget-object v2, p0, Lorg/apache/index/poi/hpsf/MutablePropertySet;->sections:Ljava/util/List;

    if-nez v2, :cond_0

    .line 96
    new-instance v2, Ljava/util/LinkedList;

    invoke-direct {v2}, Ljava/util/LinkedList;-><init>()V

    iput-object v2, p0, Lorg/apache/index/poi/hpsf/MutablePropertySet;->sections:Ljava/util/List;

    .line 97
    :cond_0
    invoke-virtual {p1}, Lorg/apache/index/poi/hpsf/PropertySet;->getSections()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_1

    .line 102
    return-void

    .line 99
    :cond_1
    new-instance v1, Lorg/apache/index/poi/hpsf/MutableSection;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/index/poi/hpsf/Section;

    invoke-direct {v1, v2}, Lorg/apache/index/poi/hpsf/MutableSection;-><init>(Lorg/apache/index/poi/hpsf/Section;)V

    .line 100
    .local v1, "s":Lorg/apache/index/poi/hpsf/MutableSection;
    invoke-virtual {p0, v1}, Lorg/apache/index/poi/hpsf/MutablePropertySet;->addSection(Lorg/apache/index/poi/hpsf/Section;)V

    goto :goto_0
.end method


# virtual methods
.method public addSection(Lorg/apache/index/poi/hpsf/Section;)V
    .locals 1
    .param p1, "section"    # Lorg/apache/index/poi/hpsf/Section;

    .prologue
    .line 188
    iget-object v0, p0, Lorg/apache/index/poi/hpsf/MutablePropertySet;->sections:Ljava/util/List;

    if-nez v0, :cond_0

    .line 189
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lorg/apache/index/poi/hpsf/MutablePropertySet;->sections:Ljava/util/List;

    .line 190
    :cond_0
    iget-object v0, p0, Lorg/apache/index/poi/hpsf/MutablePropertySet;->sections:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 191
    return-void
.end method

.method public clearSections()V
    .locals 1

    .prologue
    .line 174
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/index/poi/hpsf/MutablePropertySet;->sections:Ljava/util/List;

    .line 175
    return-void
.end method

.method public setByteOrder(I)V
    .locals 0
    .param p1, "byteOrder"    # I

    .prologue
    .line 125
    iput p1, p0, Lorg/apache/index/poi/hpsf/MutablePropertySet;->byteOrder:I

    .line 126
    return-void
.end method

.method public setClassID(Lorg/apache/index/poi/hpsf/ClassID;)V
    .locals 0
    .param p1, "classID"    # Lorg/apache/index/poi/hpsf/ClassID;

    .prologue
    .line 164
    iput-object p1, p0, Lorg/apache/index/poi/hpsf/MutablePropertySet;->classID:Lorg/apache/index/poi/hpsf/ClassID;

    .line 165
    return-void
.end method

.method public setFormat(I)V
    .locals 0
    .param p1, "format"    # I

    .prologue
    .line 137
    iput p1, p0, Lorg/apache/index/poi/hpsf/MutablePropertySet;->format:I

    .line 138
    return-void
.end method

.method public setOSVersion(I)V
    .locals 0
    .param p1, "osVersion"    # I

    .prologue
    .line 149
    iput p1, p0, Lorg/apache/index/poi/hpsf/MutablePropertySet;->osVersion:I

    .line 150
    return-void
.end method

.method public toInputStream()Ljava/io/InputStream;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/apache/index/poi/hpsf/WritingNotSupportedException;
        }
    .end annotation

    .prologue
    .line 271
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 272
    .local v0, "psStream":Ljava/io/ByteArrayOutputStream;
    invoke-virtual {p0, v0}, Lorg/apache/index/poi/hpsf/MutablePropertySet;->write(Ljava/io/OutputStream;)V

    .line 273
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->close()V

    .line 274
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    .line 275
    .local v1, "streamData":[B
    new-instance v2, Ljava/io/ByteArrayInputStream;

    invoke-direct {v2, v1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    return-object v2
.end method

.method public write(Ljava/io/OutputStream;)V
    .locals 10
    .param p1, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/index/poi/hpsf/WritingNotSupportedException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 208
    iget-object v8, p0, Lorg/apache/index/poi/hpsf/MutablePropertySet;->sections:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v4

    .line 209
    .local v4, "nrSections":I
    invoke-virtual {p0}, Lorg/apache/index/poi/hpsf/MutablePropertySet;->getByteOrder()I

    move-result v8

    int-to-short v8, v8

    invoke-static {p1, v8}, Lorg/apache/index/poi/hpsf/TypeWriter;->writeToStream(Ljava/io/OutputStream;S)I

    .line 210
    invoke-virtual {p0}, Lorg/apache/index/poi/hpsf/MutablePropertySet;->getFormat()I

    move-result v8

    int-to-short v8, v8

    invoke-static {p1, v8}, Lorg/apache/index/poi/hpsf/TypeWriter;->writeToStream(Ljava/io/OutputStream;S)I

    .line 211
    invoke-virtual {p0}, Lorg/apache/index/poi/hpsf/MutablePropertySet;->getOSVersion()I

    move-result v8

    invoke-static {p1, v8}, Lorg/apache/index/poi/hpsf/TypeWriter;->writeToStream(Ljava/io/OutputStream;I)I

    .line 212
    invoke-virtual {p0}, Lorg/apache/index/poi/hpsf/MutablePropertySet;->getClassID()Lorg/apache/index/poi/hpsf/ClassID;

    move-result-object v8

    invoke-static {p1, v8}, Lorg/apache/index/poi/hpsf/TypeWriter;->writeToStream(Ljava/io/OutputStream;Lorg/apache/index/poi/hpsf/ClassID;)I

    .line 213
    invoke-static {p1, v4}, Lorg/apache/index/poi/hpsf/TypeWriter;->writeToStream(Ljava/io/OutputStream;I)I

    .line 214
    iget v5, p0, Lorg/apache/index/poi/hpsf/MutablePropertySet;->OFFSET_HEADER:I

    .line 219
    .local v5, "offset":I
    mul-int/lit8 v8, v4, 0x14

    add-int/2addr v5, v8

    .line 220
    move v7, v5

    .line 221
    .local v7, "sectionsBegin":I
    iget-object v8, p0, Lorg/apache/index/poi/hpsf/MutablePropertySet;->sections:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->listIterator()Ljava/util/ListIterator;

    move-result-object v3

    .local v3, "i":Ljava/util/ListIterator;
    :goto_0
    invoke-interface {v3}, Ljava/util/ListIterator;->hasNext()Z

    move-result v8

    if-nez v8, :cond_0

    .line 244
    move v5, v7

    .line 245
    iget-object v8, p0, Lorg/apache/index/poi/hpsf/MutablePropertySet;->sections:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->listIterator()Ljava/util/ListIterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/ListIterator;->hasNext()Z

    move-result v8

    if-nez v8, :cond_3

    .line 250
    return-void

    .line 223
    :cond_0
    invoke-interface {v3}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lorg/apache/index/poi/hpsf/MutableSection;

    .line 224
    .local v6, "s":Lorg/apache/index/poi/hpsf/MutableSection;
    invoke-virtual {v6}, Lorg/apache/index/poi/hpsf/MutableSection;->getFormatID()Lorg/apache/index/poi/hpsf/ClassID;

    move-result-object v2

    .line 225
    .local v2, "formatID":Lorg/apache/index/poi/hpsf/ClassID;
    if-nez v2, :cond_1

    .line 226
    new-instance v8, Lorg/apache/index/poi/hpsf/NoFormatIDException;

    invoke-direct {v8}, Lorg/apache/index/poi/hpsf/NoFormatIDException;-><init>()V

    throw v8

    .line 227
    :cond_1
    invoke-virtual {v6}, Lorg/apache/index/poi/hpsf/MutableSection;->getFormatID()Lorg/apache/index/poi/hpsf/ClassID;

    move-result-object v8

    invoke-static {p1, v8}, Lorg/apache/index/poi/hpsf/TypeWriter;->writeToStream(Ljava/io/OutputStream;Lorg/apache/index/poi/hpsf/ClassID;)I

    .line 228
    int-to-long v8, v5

    invoke-static {p1, v8, v9}, Lorg/apache/index/poi/hpsf/TypeWriter;->writeUIntToStream(Ljava/io/OutputStream;J)I

    .line 231
    :try_start_0
    invoke-virtual {v6}, Lorg/apache/index/poi/hpsf/MutableSection;->getSize()I
    :try_end_0
    .catch Lorg/apache/index/poi/hpsf/HPSFRuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v8

    add-int/2addr v5, v8

    goto :goto_0

    .line 233
    :catch_0
    move-exception v1

    .line 235
    .local v1, "ex":Lorg/apache/index/poi/hpsf/HPSFRuntimeException;
    invoke-virtual {v1}, Lorg/apache/index/poi/hpsf/HPSFRuntimeException;->getReason()Ljava/lang/Throwable;

    move-result-object v0

    .line 236
    .local v0, "cause":Ljava/lang/Throwable;
    instance-of v8, v0, Ljava/io/UnsupportedEncodingException;

    if-eqz v8, :cond_2

    .line 237
    new-instance v8, Lorg/apache/index/poi/hpsf/IllegalPropertySetDataException;

    invoke-direct {v8, v0}, Lorg/apache/index/poi/hpsf/IllegalPropertySetDataException;-><init>(Ljava/lang/Throwable;)V

    throw v8

    .line 239
    :cond_2
    throw v1

    .line 247
    .end local v0    # "cause":Ljava/lang/Throwable;
    .end local v1    # "ex":Lorg/apache/index/poi/hpsf/HPSFRuntimeException;
    .end local v2    # "formatID":Lorg/apache/index/poi/hpsf/ClassID;
    .end local v6    # "s":Lorg/apache/index/poi/hpsf/MutableSection;
    :cond_3
    invoke-interface {v3}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lorg/apache/index/poi/hpsf/MutableSection;

    .line 248
    .restart local v6    # "s":Lorg/apache/index/poi/hpsf/MutableSection;
    invoke-virtual {v6, p1}, Lorg/apache/index/poi/hpsf/MutableSection;->write(Ljava/io/OutputStream;)I

    move-result v8

    add-int/2addr v5, v8

    goto :goto_1
.end method

.method public write(Lorg/apache/index/poi/poifs/filesystem/DirectoryEntry;Ljava/lang/String;)V
    .locals 5
    .param p1, "dir"    # Lorg/apache/index/poi/poifs/filesystem/DirectoryEntry;
    .param p2, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/index/poi/hpsf/WritingNotSupportedException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 294
    :try_start_0
    invoke-interface {p1, p2}, Lorg/apache/index/poi/poifs/filesystem/DirectoryEntry;->getEntry(Ljava/lang/String;)Lorg/apache/index/poi/poifs/filesystem/Entry;

    move-result-object v0

    .line 295
    .local v0, "e":Lorg/apache/index/poi/poifs/filesystem/Entry;
    invoke-interface {v0}, Lorg/apache/index/poi/poifs/filesystem/Entry;->delete()Z
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 302
    .end local v0    # "e":Lorg/apache/index/poi/poifs/filesystem/Entry;
    :goto_0
    invoke-virtual {p0}, Lorg/apache/index/poi/hpsf/MutablePropertySet;->toInputStream()Ljava/io/InputStream;

    move-result-object v2

    invoke-interface {p1, p2, v2}, Lorg/apache/index/poi/poifs/filesystem/DirectoryEntry;->createDocument(Ljava/lang/String;Ljava/io/InputStream;)Lorg/apache/index/poi/poifs/filesystem/DocumentEntry;

    .line 303
    return-void

    .line 297
    :catch_0
    move-exception v1

    .line 298
    .local v1, "ex":Ljava/io/FileNotFoundException;
    const-string/jumbo v2, "DocumentService"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "Exception :"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/FileNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
