.class public final Lorg/apache/index/poi/hslf/record/Environment;
.super Lorg/apache/index/poi/hslf/record/PositionDependentRecordContainer;
.source "Environment.java"


# static fields
.field private static _type:J


# instance fields
.field private _header:[B


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 30
    const-wide/16 v0, 0x3f2

    sput-wide v0, Lorg/apache/index/poi/hslf/record/Environment;->_type:J

    return-void
.end method

.method protected constructor <init>([BII)V
    .locals 3
    .param p1, "source"    # [B
    .param p2, "start"    # I
    .param p3, "len"    # I

    .prologue
    const/16 v2, 0x8

    .line 35
    invoke-direct {p0}, Lorg/apache/index/poi/hslf/record/PositionDependentRecordContainer;-><init>()V

    .line 37
    new-array v0, v2, [B

    iput-object v0, p0, Lorg/apache/index/poi/hslf/record/Environment;->_header:[B

    .line 38
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/Environment;->_header:[B

    const/4 v1, 0x0

    invoke-static {p1, p2, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 41
    add-int/lit8 v0, p2, 0x8

    add-int/lit8 v1, p3, -0x8

    invoke-static {p1, v0, v1}, Lorg/apache/index/poi/hslf/record/Record;->findChildRecords([BII)[Lorg/apache/index/poi/hslf/record/Record;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/index/poi/hslf/record/Environment;->_children:[Lorg/apache/index/poi/hslf/record/Record;

    .line 42
    return-void
.end method


# virtual methods
.method public getRecordType()J
    .locals 2

    .prologue
    .line 47
    sget-wide v0, Lorg/apache/index/poi/hslf/record/Environment;->_type:J

    return-wide v0
.end method
