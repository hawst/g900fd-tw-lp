.class public final Lorg/apache/index/poi/hslf/record/PPDrawingGroup;
.super Lorg/apache/index/poi/hslf/record/RecordAtom;
.source "PPDrawingGroup.java"


# instance fields
.field private _header:[B

.field private dgg:Lorg/apache/index/poi/ddf/EscherDggRecord;

.field private dggContainer:Lorg/apache/index/poi/ddf/EscherContainerRecord;


# direct methods
.method protected constructor <init>([BII)V
    .locals 6
    .param p1, "source"    # [B
    .param p2, "start"    # I
    .param p3, "len"    # I

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x0

    .line 41
    invoke-direct {p0}, Lorg/apache/index/poi/hslf/record/RecordAtom;-><init>()V

    .line 43
    new-array v3, v5, [B

    iput-object v3, p0, Lorg/apache/index/poi/hslf/record/PPDrawingGroup;->_header:[B

    .line 44
    iget-object v3, p0, Lorg/apache/index/poi/hslf/record/PPDrawingGroup;->_header:[B

    invoke-static {p1, p2, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 47
    new-array v1, p3, [B

    .line 48
    .local v1, "contents":[B
    invoke-static {p1, p2, v1, v4, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 50
    new-instance v2, Lorg/apache/index/poi/ddf/DefaultEscherRecordFactory;

    invoke-direct {v2}, Lorg/apache/index/poi/ddf/DefaultEscherRecordFactory;-><init>()V

    .line 51
    .local v2, "erf":Lorg/apache/index/poi/ddf/DefaultEscherRecordFactory;
    invoke-virtual {v2, v1, v4}, Lorg/apache/index/poi/ddf/DefaultEscherRecordFactory;->createRecord([BI)Lorg/apache/index/poi/ddf/EscherRecord;

    move-result-object v0

    .line 52
    .local v0, "child":Lorg/apache/index/poi/ddf/EscherRecord;
    invoke-virtual {v0, v1, v4, v2}, Lorg/apache/index/poi/ddf/EscherRecord;->fillFields([BILorg/apache/index/poi/ddf/EscherRecordFactory;)I

    .line 53
    invoke-virtual {v0, v4}, Lorg/apache/index/poi/ddf/EscherRecord;->getChild(I)Lorg/apache/index/poi/ddf/EscherRecord;

    move-result-object v3

    check-cast v3, Lorg/apache/index/poi/ddf/EscherContainerRecord;

    iput-object v3, p0, Lorg/apache/index/poi/hslf/record/PPDrawingGroup;->dggContainer:Lorg/apache/index/poi/ddf/EscherContainerRecord;

    .line 54
    return-void
.end method


# virtual methods
.method public getChildRecords()[Lorg/apache/index/poi/hslf/record/Record;
    .locals 1

    .prologue
    .line 67
    const/4 v0, 0x0

    return-object v0
.end method

.method public getDggContainer()Lorg/apache/index/poi/ddf/EscherContainerRecord;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/PPDrawingGroup;->dggContainer:Lorg/apache/index/poi/ddf/EscherContainerRecord;

    return-object v0
.end method

.method public getEscherDggRecord()Lorg/apache/index/poi/ddf/EscherDggRecord;
    .locals 3

    .prologue
    .line 79
    iget-object v2, p0, Lorg/apache/index/poi/hslf/record/PPDrawingGroup;->dgg:Lorg/apache/index/poi/ddf/EscherDggRecord;

    if-nez v2, :cond_1

    .line 80
    iget-object v2, p0, Lorg/apache/index/poi/hslf/record/PPDrawingGroup;->dggContainer:Lorg/apache/index/poi/ddf/EscherContainerRecord;

    invoke-virtual {v2}, Lorg/apache/index/poi/ddf/EscherContainerRecord;->getChildIterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/index/poi/ddf/EscherRecord;>;"
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_2

    .line 88
    .end local v0    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/index/poi/ddf/EscherRecord;>;"
    :cond_1
    :goto_0
    iget-object v2, p0, Lorg/apache/index/poi/hslf/record/PPDrawingGroup;->dgg:Lorg/apache/index/poi/ddf/EscherDggRecord;

    return-object v2

    .line 81
    .restart local v0    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/index/poi/ddf/EscherRecord;>;"
    :cond_2
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/index/poi/ddf/EscherRecord;

    .line 82
    .local v1, "r":Lorg/apache/index/poi/ddf/EscherRecord;
    instance-of v2, v1, Lorg/apache/index/poi/ddf/EscherDggRecord;

    if-eqz v2, :cond_0

    .line 83
    check-cast v1, Lorg/apache/index/poi/ddf/EscherDggRecord;

    .end local v1    # "r":Lorg/apache/index/poi/ddf/EscherRecord;
    iput-object v1, p0, Lorg/apache/index/poi/hslf/record/PPDrawingGroup;->dgg:Lorg/apache/index/poi/ddf/EscherDggRecord;

    goto :goto_0
.end method

.method public getRecordType()J
    .locals 2

    .prologue
    .line 60
    sget-object v0, Lorg/apache/index/poi/hslf/record/RecordTypes;->PPDrawingGroup:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    iget v0, v0, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;->typeID:I

    int-to-long v0, v0

    return-wide v0
.end method

.method public writeOut(Ljava/io/OutputStream;)V
    .locals 0
    .param p1, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 72
    return-void
.end method
