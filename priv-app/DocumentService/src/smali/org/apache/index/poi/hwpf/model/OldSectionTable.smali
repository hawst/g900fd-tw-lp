.class public final Lorg/apache/index/poi/hwpf/model/OldSectionTable;
.super Lorg/apache/index/poi/hwpf/model/SectionTable;
.source "OldSectionTable.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/index/poi/hwpf/model/OldSectionTable$CharIsBytes;
    }
.end annotation


# direct methods
.method public constructor <init>([BIIILorg/apache/index/poi/hwpf/model/TextPieceTable;)V
    .locals 17
    .param p1, "documentStream"    # [B
    .param p2, "offset"    # I
    .param p3, "size"    # I
    .param p4, "fcMin"    # I
    .param p5, "tpt"    # Lorg/apache/index/poi/hwpf/model/TextPieceTable;

    .prologue
    .line 33
    invoke-direct/range {p0 .. p0}, Lorg/apache/index/poi/hwpf/model/SectionTable;-><init>()V

    .line 37
    new-instance v12, Lorg/apache/index/poi/hwpf/model/PlexOfCps;

    const/16 v3, 0xc

    move-object/from16 v0, p1

    move/from16 v1, p2

    move/from16 v2, p3

    invoke-direct {v12, v0, v1, v2, v3}, Lorg/apache/index/poi/hwpf/model/PlexOfCps;-><init>([BIII)V

    .line 38
    .local v12, "sedPlex":Lorg/apache/index/poi/hwpf/model/PlexOfCps;
    new-instance v7, Lorg/apache/index/poi/hwpf/model/OldSectionTable$CharIsBytes;

    const/4 v3, 0x0

    move-object/from16 v0, p5

    invoke-direct {v7, v0, v3}, Lorg/apache/index/poi/hwpf/model/OldSectionTable$CharIsBytes;-><init>(Lorg/apache/index/poi/hwpf/model/TextPieceTable;Lorg/apache/index/poi/hwpf/model/OldSectionTable$CharIsBytes;)V

    .line 40
    .local v7, "charConv":Lorg/apache/index/poi/hwpf/model/OldSectionTable$CharIsBytes;
    invoke-virtual {v12}, Lorg/apache/index/poi/hwpf/model/PlexOfCps;->length()I

    move-result v10

    .line 42
    .local v10, "length":I
    const/4 v14, 0x0

    .local v14, "x":I
    :goto_0
    if-lt v14, v10, :cond_0

    .line 70
    return-void

    .line 44
    :cond_0
    invoke-virtual {v12, v14}, Lorg/apache/index/poi/hwpf/model/PlexOfCps;->getProperty(I)Lorg/apache/index/poi/hwpf/model/GenericPropertyNode;

    move-result-object v11

    .line 45
    .local v11, "node":Lorg/apache/index/poi/hwpf/model/GenericPropertyNode;
    new-instance v4, Lorg/apache/index/poi/hwpf/model/SectionDescriptor;

    invoke-virtual {v11}, Lorg/apache/index/poi/hwpf/model/GenericPropertyNode;->getBytes()[B

    move-result-object v3

    const/4 v15, 0x0

    invoke-direct {v4, v3, v15}, Lorg/apache/index/poi/hwpf/model/SectionDescriptor;-><init>([BI)V

    .line 47
    .local v4, "sed":Lorg/apache/index/poi/hwpf/model/SectionDescriptor;
    invoke-virtual {v4}, Lorg/apache/index/poi/hwpf/model/SectionDescriptor;->getFc()I

    move-result v9

    .line 48
    .local v9, "fileOffset":I
    invoke-virtual {v11}, Lorg/apache/index/poi/hwpf/model/GenericPropertyNode;->getStart()I

    move-result v5

    .line 49
    .local v5, "startAt":I
    invoke-virtual {v11}, Lorg/apache/index/poi/hwpf/model/GenericPropertyNode;->getEnd()I

    move-result v6

    .line 52
    .local v6, "endAt":I
    const/4 v3, -0x1

    if-ne v9, v3, :cond_1

    .line 54
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/index/poi/hwpf/model/OldSectionTable;->_sections:Ljava/util/ArrayList;

    new-instance v3, Lorg/apache/index/poi/hwpf/model/SEPX;

    const/16 v16, 0x0

    move/from16 v0, v16

    new-array v8, v0, [B

    invoke-direct/range {v3 .. v8}, Lorg/apache/index/poi/hwpf/model/SEPX;-><init>(Lorg/apache/index/poi/hwpf/model/SectionDescriptor;IILorg/apache/index/poi/hwpf/model/CharIndexTranslator;[B)V

    invoke-virtual {v15, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 42
    :goto_1
    add-int/lit8 v14, v14, 0x1

    goto :goto_0

    .line 59
    :cond_1
    move-object/from16 v0, p1

    invoke-static {v0, v9}, Lorg/apache/index/poi/util/LittleEndian;->getShort([BI)S

    move-result v13

    .line 64
    .local v13, "sepxSize":I
    add-int/lit8 v3, v13, 0x2

    new-array v8, v3, [B

    .line 65
    .local v8, "buf":[B
    add-int/lit8 v9, v9, 0x2

    .line 66
    const/4 v3, 0x0

    array-length v15, v8

    move-object/from16 v0, p1

    invoke-static {v0, v9, v8, v3, v15}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 67
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/index/poi/hwpf/model/OldSectionTable;->_sections:Ljava/util/ArrayList;

    new-instance v3, Lorg/apache/index/poi/hwpf/model/SEPX;

    invoke-direct/range {v3 .. v8}, Lorg/apache/index/poi/hwpf/model/SEPX;-><init>(Lorg/apache/index/poi/hwpf/model/SectionDescriptor;IILorg/apache/index/poi/hwpf/model/CharIndexTranslator;[B)V

    invoke-virtual {v15, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method
