.class public abstract Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;
.super Ljava/lang/Object;
.source "FIBAbstractType.java"

# interfaces
.implements Lorg/apache/index/poi/hwpf/model/HDFType;


# static fields
.field private static cQuickSaves:Lorg/apache/index/poi/util/BitField;

.field private static fComplex:Lorg/apache/index/poi/util/BitField;

.field private static fCrypto:Lorg/apache/index/poi/util/BitField;

.field private static fDot:Lorg/apache/index/poi/util/BitField;

.field private static fEmptySpecial:Lorg/apache/index/poi/util/BitField;

.field private static fEncrypted:Lorg/apache/index/poi/util/BitField;

.field private static fExtChar:Lorg/apache/index/poi/util/BitField;

.field private static fFarEast:Lorg/apache/index/poi/util/BitField;

.field private static fFutureSavedUndo:Lorg/apache/index/poi/util/BitField;

.field private static fGlsy:Lorg/apache/index/poi/util/BitField;

.field private static fHasPic:Lorg/apache/index/poi/util/BitField;

.field private static fLoadOverride:Lorg/apache/index/poi/util/BitField;

.field private static fLoadOverridePage:Lorg/apache/index/poi/util/BitField;

.field private static fMac:Lorg/apache/index/poi/util/BitField;

.field private static fReadOnlyRecommended:Lorg/apache/index/poi/util/BitField;

.field private static fSpare0:Lorg/apache/index/poi/util/BitField;

.field private static fWhichTblStm:Lorg/apache/index/poi/util/BitField;

.field private static fWord97Saved:Lorg/apache/index/poi/util/BitField;

.field private static fWriteReservation:Lorg/apache/index/poi/util/BitField;


# instance fields
.field protected field_10_history:S

.field protected field_11_chs:I

.field protected field_12_chsTables:I

.field protected field_13_fcMin:I

.field protected field_14_fcMac:I

.field protected field_1_wIdent:I

.field protected field_2_nFib:I

.field protected field_3_nProduct:I

.field protected field_4_lid:I

.field protected field_5_pnNext:I

.field protected field_6_options:S

.field protected field_7_nFibBack:I

.field protected field_8_lKey:I

.field protected field_9_envr:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 41
    invoke-static {v1}, Lorg/apache/index/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/index/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->fDot:Lorg/apache/index/poi/util/BitField;

    .line 42
    invoke-static {v2}, Lorg/apache/index/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/index/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->fGlsy:Lorg/apache/index/poi/util/BitField;

    .line 43
    invoke-static {v3}, Lorg/apache/index/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/index/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->fComplex:Lorg/apache/index/poi/util/BitField;

    .line 44
    invoke-static {v4}, Lorg/apache/index/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/index/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->fHasPic:Lorg/apache/index/poi/util/BitField;

    .line 45
    const/16 v0, 0xf0

    invoke-static {v0}, Lorg/apache/index/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/index/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->cQuickSaves:Lorg/apache/index/poi/util/BitField;

    .line 46
    const/16 v0, 0x100

    invoke-static {v0}, Lorg/apache/index/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/index/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->fEncrypted:Lorg/apache/index/poi/util/BitField;

    .line 47
    const/16 v0, 0x200

    invoke-static {v0}, Lorg/apache/index/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/index/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->fWhichTblStm:Lorg/apache/index/poi/util/BitField;

    .line 48
    const/16 v0, 0x400

    invoke-static {v0}, Lorg/apache/index/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/index/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->fReadOnlyRecommended:Lorg/apache/index/poi/util/BitField;

    .line 49
    const/16 v0, 0x800

    invoke-static {v0}, Lorg/apache/index/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/index/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->fWriteReservation:Lorg/apache/index/poi/util/BitField;

    .line 50
    const/16 v0, 0x1000

    invoke-static {v0}, Lorg/apache/index/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/index/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->fExtChar:Lorg/apache/index/poi/util/BitField;

    .line 51
    const/16 v0, 0x2000

    invoke-static {v0}, Lorg/apache/index/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/index/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->fLoadOverride:Lorg/apache/index/poi/util/BitField;

    .line 52
    const/16 v0, 0x4000

    invoke-static {v0}, Lorg/apache/index/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/index/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->fFarEast:Lorg/apache/index/poi/util/BitField;

    .line 53
    const v0, 0x8000

    invoke-static {v0}, Lorg/apache/index/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/index/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->fCrypto:Lorg/apache/index/poi/util/BitField;

    .line 58
    invoke-static {v1}, Lorg/apache/index/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/index/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->fMac:Lorg/apache/index/poi/util/BitField;

    .line 59
    invoke-static {v2}, Lorg/apache/index/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/index/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->fEmptySpecial:Lorg/apache/index/poi/util/BitField;

    .line 60
    invoke-static {v3}, Lorg/apache/index/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/index/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->fLoadOverridePage:Lorg/apache/index/poi/util/BitField;

    .line 61
    invoke-static {v4}, Lorg/apache/index/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/index/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->fFutureSavedUndo:Lorg/apache/index/poi/util/BitField;

    .line 62
    const/16 v0, 0x10

    invoke-static {v0}, Lorg/apache/index/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/index/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->fWord97Saved:Lorg/apache/index/poi/util/BitField;

    .line 63
    const/16 v0, 0xfe

    invoke-static {v0}, Lorg/apache/index/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/index/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->fSpare0:Lorg/apache/index/poi/util/BitField;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 73
    return-void
.end method


# virtual methods
.method protected fillFields([BI)V
    .locals 1
    .param p1, "data"    # [B
    .param p2, "offset"    # I

    .prologue
    .line 77
    add-int/lit8 v0, p2, 0x0

    invoke-static {p1, v0}, Lorg/apache/index/poi/util/LittleEndian;->getShort([BI)S

    move-result v0

    iput v0, p0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->field_1_wIdent:I

    .line 78
    add-int/lit8 v0, p2, 0x2

    invoke-static {p1, v0}, Lorg/apache/index/poi/util/LittleEndian;->getShort([BI)S

    move-result v0

    iput v0, p0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->field_2_nFib:I

    .line 79
    add-int/lit8 v0, p2, 0x4

    invoke-static {p1, v0}, Lorg/apache/index/poi/util/LittleEndian;->getShort([BI)S

    move-result v0

    iput v0, p0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->field_3_nProduct:I

    .line 80
    add-int/lit8 v0, p2, 0x6

    invoke-static {p1, v0}, Lorg/apache/index/poi/util/LittleEndian;->getShort([BI)S

    move-result v0

    iput v0, p0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->field_4_lid:I

    .line 81
    add-int/lit8 v0, p2, 0x8

    invoke-static {p1, v0}, Lorg/apache/index/poi/util/LittleEndian;->getShort([BI)S

    move-result v0

    iput v0, p0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->field_5_pnNext:I

    .line 82
    add-int/lit8 v0, p2, 0xa

    invoke-static {p1, v0}, Lorg/apache/index/poi/util/LittleEndian;->getShort([BI)S

    move-result v0

    iput-short v0, p0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->field_6_options:S

    .line 83
    add-int/lit8 v0, p2, 0xc

    invoke-static {p1, v0}, Lorg/apache/index/poi/util/LittleEndian;->getShort([BI)S

    move-result v0

    iput v0, p0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->field_7_nFibBack:I

    .line 84
    add-int/lit8 v0, p2, 0xe

    invoke-static {p1, v0}, Lorg/apache/index/poi/util/LittleEndian;->getShort([BI)S

    move-result v0

    iput v0, p0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->field_8_lKey:I

    .line 85
    add-int/lit8 v0, p2, 0x10

    invoke-static {p1, v0}, Lorg/apache/index/poi/util/LittleEndian;->getShort([BI)S

    move-result v0

    iput v0, p0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->field_9_envr:I

    .line 86
    add-int/lit8 v0, p2, 0x12

    invoke-static {p1, v0}, Lorg/apache/index/poi/util/LittleEndian;->getShort([BI)S

    move-result v0

    iput-short v0, p0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->field_10_history:S

    .line 87
    add-int/lit8 v0, p2, 0x14

    invoke-static {p1, v0}, Lorg/apache/index/poi/util/LittleEndian;->getShort([BI)S

    move-result v0

    iput v0, p0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->field_11_chs:I

    .line 88
    add-int/lit8 v0, p2, 0x16

    invoke-static {p1, v0}, Lorg/apache/index/poi/util/LittleEndian;->getShort([BI)S

    move-result v0

    iput v0, p0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->field_12_chsTables:I

    .line 89
    add-int/lit8 v0, p2, 0x18

    invoke-static {p1, v0}, Lorg/apache/index/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    iput v0, p0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->field_13_fcMin:I

    .line 90
    add-int/lit8 v0, p2, 0x1c

    invoke-static {p1, v0}, Lorg/apache/index/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    iput v0, p0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->field_14_fcMac:I

    .line 91
    return-void
.end method

.method public getCQuickSaves()B
    .locals 2

    .prologue
    .line 503
    sget-object v0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->cQuickSaves:Lorg/apache/index/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->field_6_options:S

    invoke-virtual {v0, v1}, Lorg/apache/index/poi/util/BitField;->getValue(I)I

    move-result v0

    int-to-byte v0, v0

    return v0
.end method

.method public getChs()I
    .locals 1

    .prologue
    .line 357
    iget v0, p0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->field_11_chs:I

    return v0
.end method

.method public getChsTables()I
    .locals 1

    .prologue
    .line 373
    iget v0, p0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->field_12_chsTables:I

    return v0
.end method

.method public getEnvr()I
    .locals 1

    .prologue
    .line 325
    iget v0, p0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->field_9_envr:I

    return v0
.end method

.method public getFSpare0()B
    .locals 2

    .prologue
    .line 755
    sget-object v0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->fSpare0:Lorg/apache/index/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->field_10_history:S

    invoke-virtual {v0, v1}, Lorg/apache/index/poi/util/BitField;->getValue(I)I

    move-result v0

    int-to-byte v0, v0

    return v0
.end method

.method public getFcMac()I
    .locals 1

    .prologue
    .line 405
    iget v0, p0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->field_14_fcMac:I

    return v0
.end method

.method public getFcMin()I
    .locals 1

    .prologue
    .line 389
    iget v0, p0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->field_13_fcMin:I

    return v0
.end method

.method public getHistory()S
    .locals 1

    .prologue
    .line 341
    iget-short v0, p0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->field_10_history:S

    return v0
.end method

.method public getLKey()I
    .locals 1

    .prologue
    .line 309
    iget v0, p0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->field_8_lKey:I

    return v0
.end method

.method public getLid()I
    .locals 1

    .prologue
    .line 245
    iget v0, p0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->field_4_lid:I

    return v0
.end method

.method public getNFib()I
    .locals 1

    .prologue
    .line 213
    iget v0, p0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->field_2_nFib:I

    return v0
.end method

.method public getNFibBack()I
    .locals 1

    .prologue
    .line 293
    iget v0, p0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->field_7_nFibBack:I

    return v0
.end method

.method public getNProduct()I
    .locals 1

    .prologue
    .line 229
    iget v0, p0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->field_3_nProduct:I

    return v0
.end method

.method public getOptions()S
    .locals 1

    .prologue
    .line 277
    iget-short v0, p0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->field_6_options:S

    return v0
.end method

.method public getPnNext()I
    .locals 1

    .prologue
    .line 261
    iget v0, p0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->field_5_pnNext:I

    return v0
.end method

.method public getSize()I
    .locals 1

    .prologue
    .line 187
    const/16 v0, 0x20

    return v0
.end method

.method public getWIdent()I
    .locals 1

    .prologue
    .line 197
    iget v0, p0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->field_1_wIdent:I

    return v0
.end method

.method public isFComplex()Z
    .locals 2

    .prologue
    .line 467
    sget-object v0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->fComplex:Lorg/apache/index/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->field_6_options:S

    invoke-virtual {v0, v1}, Lorg/apache/index/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFCrypto()Z
    .locals 2

    .prologue
    .line 647
    sget-object v0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->fCrypto:Lorg/apache/index/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->field_6_options:S

    invoke-virtual {v0, v1}, Lorg/apache/index/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFDot()Z
    .locals 2

    .prologue
    .line 431
    sget-object v0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->fDot:Lorg/apache/index/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->field_6_options:S

    invoke-virtual {v0, v1}, Lorg/apache/index/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFEmptySpecial()Z
    .locals 2

    .prologue
    .line 683
    sget-object v0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->fEmptySpecial:Lorg/apache/index/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->field_10_history:S

    invoke-virtual {v0, v1}, Lorg/apache/index/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFEncrypted()Z
    .locals 2

    .prologue
    .line 521
    sget-object v0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->fEncrypted:Lorg/apache/index/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->field_6_options:S

    invoke-virtual {v0, v1}, Lorg/apache/index/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFExtChar()Z
    .locals 2

    .prologue
    .line 593
    sget-object v0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->fExtChar:Lorg/apache/index/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->field_6_options:S

    invoke-virtual {v0, v1}, Lorg/apache/index/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFFarEast()Z
    .locals 2

    .prologue
    .line 629
    sget-object v0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->fFarEast:Lorg/apache/index/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->field_6_options:S

    invoke-virtual {v0, v1}, Lorg/apache/index/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFFutureSavedUndo()Z
    .locals 2

    .prologue
    .line 719
    sget-object v0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->fFutureSavedUndo:Lorg/apache/index/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->field_10_history:S

    invoke-virtual {v0, v1}, Lorg/apache/index/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFGlsy()Z
    .locals 2

    .prologue
    .line 449
    sget-object v0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->fGlsy:Lorg/apache/index/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->field_6_options:S

    invoke-virtual {v0, v1}, Lorg/apache/index/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFHasPic()Z
    .locals 2

    .prologue
    .line 485
    sget-object v0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->fHasPic:Lorg/apache/index/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->field_6_options:S

    invoke-virtual {v0, v1}, Lorg/apache/index/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFLoadOverride()Z
    .locals 2

    .prologue
    .line 611
    sget-object v0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->fLoadOverride:Lorg/apache/index/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->field_6_options:S

    invoke-virtual {v0, v1}, Lorg/apache/index/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFLoadOverridePage()Z
    .locals 2

    .prologue
    .line 701
    sget-object v0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->fLoadOverridePage:Lorg/apache/index/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->field_10_history:S

    invoke-virtual {v0, v1}, Lorg/apache/index/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFMac()Z
    .locals 2

    .prologue
    .line 665
    sget-object v0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->fMac:Lorg/apache/index/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->field_10_history:S

    invoke-virtual {v0, v1}, Lorg/apache/index/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFReadOnlyRecommended()Z
    .locals 2

    .prologue
    .line 557
    sget-object v0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->fReadOnlyRecommended:Lorg/apache/index/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->field_6_options:S

    invoke-virtual {v0, v1}, Lorg/apache/index/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFWhichTblStm()Z
    .locals 2

    .prologue
    .line 539
    sget-object v0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->fWhichTblStm:Lorg/apache/index/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->field_6_options:S

    invoke-virtual {v0, v1}, Lorg/apache/index/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFWord97Saved()Z
    .locals 2

    .prologue
    .line 737
    sget-object v0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->fWord97Saved:Lorg/apache/index/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->field_10_history:S

    invoke-virtual {v0, v1}, Lorg/apache/index/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public isFWriteReservation()Z
    .locals 2

    .prologue
    .line 575
    sget-object v0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->fWriteReservation:Lorg/apache/index/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->field_6_options:S

    invoke-virtual {v0, v1}, Lorg/apache/index/poi/util/BitField;->isSet(I)Z

    move-result v0

    return v0
.end method

.method public serialize([BI)V
    .locals 2
    .param p1, "data"    # [B
    .param p2, "offset"    # I

    .prologue
    .line 95
    add-int/lit8 v0, p2, 0x0

    iget v1, p0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->field_1_wIdent:I

    int-to-short v1, v1

    invoke-static {p1, v0, v1}, Lorg/apache/index/poi/util/LittleEndian;->putShort([BIS)V

    .line 96
    add-int/lit8 v0, p2, 0x2

    iget v1, p0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->field_2_nFib:I

    int-to-short v1, v1

    invoke-static {p1, v0, v1}, Lorg/apache/index/poi/util/LittleEndian;->putShort([BIS)V

    .line 97
    add-int/lit8 v0, p2, 0x4

    iget v1, p0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->field_3_nProduct:I

    int-to-short v1, v1

    invoke-static {p1, v0, v1}, Lorg/apache/index/poi/util/LittleEndian;->putShort([BIS)V

    .line 98
    add-int/lit8 v0, p2, 0x6

    iget v1, p0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->field_4_lid:I

    int-to-short v1, v1

    invoke-static {p1, v0, v1}, Lorg/apache/index/poi/util/LittleEndian;->putShort([BIS)V

    .line 99
    add-int/lit8 v0, p2, 0x8

    iget v1, p0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->field_5_pnNext:I

    int-to-short v1, v1

    invoke-static {p1, v0, v1}, Lorg/apache/index/poi/util/LittleEndian;->putShort([BIS)V

    .line 100
    add-int/lit8 v0, p2, 0xa

    iget-short v1, p0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->field_6_options:S

    invoke-static {p1, v0, v1}, Lorg/apache/index/poi/util/LittleEndian;->putShort([BIS)V

    .line 101
    add-int/lit8 v0, p2, 0xc

    iget v1, p0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->field_7_nFibBack:I

    int-to-short v1, v1

    invoke-static {p1, v0, v1}, Lorg/apache/index/poi/util/LittleEndian;->putShort([BIS)V

    .line 102
    add-int/lit8 v0, p2, 0xe

    iget v1, p0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->field_8_lKey:I

    int-to-short v1, v1

    invoke-static {p1, v0, v1}, Lorg/apache/index/poi/util/LittleEndian;->putShort([BIS)V

    .line 103
    add-int/lit8 v0, p2, 0x10

    iget v1, p0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->field_9_envr:I

    int-to-short v1, v1

    invoke-static {p1, v0, v1}, Lorg/apache/index/poi/util/LittleEndian;->putShort([BIS)V

    .line 104
    add-int/lit8 v0, p2, 0x12

    iget-short v1, p0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->field_10_history:S

    invoke-static {p1, v0, v1}, Lorg/apache/index/poi/util/LittleEndian;->putShort([BIS)V

    .line 105
    add-int/lit8 v0, p2, 0x14

    iget v1, p0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->field_11_chs:I

    int-to-short v1, v1

    invoke-static {p1, v0, v1}, Lorg/apache/index/poi/util/LittleEndian;->putShort([BIS)V

    .line 106
    add-int/lit8 v0, p2, 0x16

    iget v1, p0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->field_12_chsTables:I

    int-to-short v1, v1

    invoke-static {p1, v0, v1}, Lorg/apache/index/poi/util/LittleEndian;->putShort([BIS)V

    .line 107
    add-int/lit8 v0, p2, 0x18

    iget v1, p0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->field_13_fcMin:I

    invoke-static {p1, v0, v1}, Lorg/apache/index/poi/util/LittleEndian;->putInt([BII)V

    .line 108
    add-int/lit8 v0, p2, 0x1c

    iget v1, p0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->field_14_fcMac:I

    invoke-static {p1, v0, v1}, Lorg/apache/index/poi/util/LittleEndian;->putInt([BII)V

    .line 109
    return-void
.end method

.method public setCQuickSaves(B)V
    .locals 2
    .param p1, "value"    # B

    .prologue
    .line 494
    sget-object v0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->cQuickSaves:Lorg/apache/index/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->field_6_options:S

    invoke-virtual {v0, v1, p1}, Lorg/apache/index/poi/util/BitField;->setValue(II)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->field_6_options:S

    .line 495
    return-void
.end method

.method public setChs(I)V
    .locals 0
    .param p1, "field_11_chs"    # I

    .prologue
    .line 365
    iput p1, p0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->field_11_chs:I

    .line 366
    return-void
.end method

.method public setChsTables(I)V
    .locals 0
    .param p1, "field_12_chsTables"    # I

    .prologue
    .line 381
    iput p1, p0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->field_12_chsTables:I

    .line 382
    return-void
.end method

.method public setEnvr(I)V
    .locals 0
    .param p1, "field_9_envr"    # I

    .prologue
    .line 333
    iput p1, p0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->field_9_envr:I

    .line 334
    return-void
.end method

.method public setFComplex(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 458
    sget-object v0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->fComplex:Lorg/apache/index/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->field_6_options:S

    invoke-virtual {v0, v1, p1}, Lorg/apache/index/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->field_6_options:S

    .line 459
    return-void
.end method

.method public setFCrypto(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 638
    sget-object v0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->fCrypto:Lorg/apache/index/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->field_6_options:S

    invoke-virtual {v0, v1, p1}, Lorg/apache/index/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->field_6_options:S

    .line 639
    return-void
.end method

.method public setFDot(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 422
    sget-object v0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->fDot:Lorg/apache/index/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->field_6_options:S

    invoke-virtual {v0, v1, p1}, Lorg/apache/index/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->field_6_options:S

    .line 423
    return-void
.end method

.method public setFEmptySpecial(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 674
    sget-object v0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->fEmptySpecial:Lorg/apache/index/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->field_10_history:S

    invoke-virtual {v0, v1, p1}, Lorg/apache/index/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->field_10_history:S

    .line 675
    return-void
.end method

.method public setFEncrypted(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 512
    sget-object v0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->fEncrypted:Lorg/apache/index/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->field_6_options:S

    invoke-virtual {v0, v1, p1}, Lorg/apache/index/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->field_6_options:S

    .line 513
    return-void
.end method

.method public setFExtChar(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 584
    sget-object v0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->fExtChar:Lorg/apache/index/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->field_6_options:S

    invoke-virtual {v0, v1, p1}, Lorg/apache/index/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->field_6_options:S

    .line 585
    return-void
.end method

.method public setFFarEast(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 620
    sget-object v0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->fFarEast:Lorg/apache/index/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->field_6_options:S

    invoke-virtual {v0, v1, p1}, Lorg/apache/index/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->field_6_options:S

    .line 621
    return-void
.end method

.method public setFFutureSavedUndo(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 710
    sget-object v0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->fFutureSavedUndo:Lorg/apache/index/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->field_10_history:S

    invoke-virtual {v0, v1, p1}, Lorg/apache/index/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->field_10_history:S

    .line 711
    return-void
.end method

.method public setFGlsy(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 440
    sget-object v0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->fGlsy:Lorg/apache/index/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->field_6_options:S

    invoke-virtual {v0, v1, p1}, Lorg/apache/index/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->field_6_options:S

    .line 441
    return-void
.end method

.method public setFHasPic(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 476
    sget-object v0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->fHasPic:Lorg/apache/index/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->field_6_options:S

    invoke-virtual {v0, v1, p1}, Lorg/apache/index/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->field_6_options:S

    .line 477
    return-void
.end method

.method public setFLoadOverride(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 602
    sget-object v0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->fLoadOverride:Lorg/apache/index/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->field_6_options:S

    invoke-virtual {v0, v1, p1}, Lorg/apache/index/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->field_6_options:S

    .line 603
    return-void
.end method

.method public setFLoadOverridePage(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 692
    sget-object v0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->fLoadOverridePage:Lorg/apache/index/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->field_10_history:S

    invoke-virtual {v0, v1, p1}, Lorg/apache/index/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->field_10_history:S

    .line 693
    return-void
.end method

.method public setFMac(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 656
    sget-object v0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->fMac:Lorg/apache/index/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->field_10_history:S

    invoke-virtual {v0, v1, p1}, Lorg/apache/index/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->field_10_history:S

    .line 657
    return-void
.end method

.method public setFReadOnlyRecommended(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 548
    sget-object v0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->fReadOnlyRecommended:Lorg/apache/index/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->field_6_options:S

    invoke-virtual {v0, v1, p1}, Lorg/apache/index/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->field_6_options:S

    .line 549
    return-void
.end method

.method public setFSpare0(B)V
    .locals 2
    .param p1, "value"    # B

    .prologue
    .line 746
    sget-object v0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->fSpare0:Lorg/apache/index/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->field_10_history:S

    invoke-virtual {v0, v1, p1}, Lorg/apache/index/poi/util/BitField;->setValue(II)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->field_10_history:S

    .line 747
    return-void
.end method

.method public setFWhichTblStm(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 530
    sget-object v0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->fWhichTblStm:Lorg/apache/index/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->field_6_options:S

    invoke-virtual {v0, v1, p1}, Lorg/apache/index/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->field_6_options:S

    .line 531
    return-void
.end method

.method public setFWord97Saved(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 728
    sget-object v0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->fWord97Saved:Lorg/apache/index/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->field_10_history:S

    invoke-virtual {v0, v1, p1}, Lorg/apache/index/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->field_10_history:S

    .line 729
    return-void
.end method

.method public setFWriteReservation(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 566
    sget-object v0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->fWriteReservation:Lorg/apache/index/poi/util/BitField;

    iget-short v1, p0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->field_6_options:S

    invoke-virtual {v0, v1, p1}, Lorg/apache/index/poi/util/BitField;->setBoolean(IZ)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->field_6_options:S

    .line 567
    return-void
.end method

.method public setFcMac(I)V
    .locals 0
    .param p1, "field_14_fcMac"    # I

    .prologue
    .line 413
    iput p1, p0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->field_14_fcMac:I

    .line 414
    return-void
.end method

.method public setFcMin(I)V
    .locals 0
    .param p1, "field_13_fcMin"    # I

    .prologue
    .line 397
    iput p1, p0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->field_13_fcMin:I

    .line 398
    return-void
.end method

.method public setHistory(S)V
    .locals 0
    .param p1, "field_10_history"    # S

    .prologue
    .line 349
    iput-short p1, p0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->field_10_history:S

    .line 350
    return-void
.end method

.method public setLKey(I)V
    .locals 0
    .param p1, "field_8_lKey"    # I

    .prologue
    .line 317
    iput p1, p0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->field_8_lKey:I

    .line 318
    return-void
.end method

.method public setLid(I)V
    .locals 0
    .param p1, "field_4_lid"    # I

    .prologue
    .line 253
    iput p1, p0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->field_4_lid:I

    .line 254
    return-void
.end method

.method public setNFib(I)V
    .locals 0
    .param p1, "field_2_nFib"    # I

    .prologue
    .line 221
    iput p1, p0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->field_2_nFib:I

    .line 222
    return-void
.end method

.method public setNFibBack(I)V
    .locals 0
    .param p1, "field_7_nFibBack"    # I

    .prologue
    .line 301
    iput p1, p0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->field_7_nFibBack:I

    .line 302
    return-void
.end method

.method public setNProduct(I)V
    .locals 0
    .param p1, "field_3_nProduct"    # I

    .prologue
    .line 237
    iput p1, p0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->field_3_nProduct:I

    .line 238
    return-void
.end method

.method public setOptions(S)V
    .locals 0
    .param p1, "field_6_options"    # S

    .prologue
    .line 285
    iput-short p1, p0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->field_6_options:S

    .line 286
    return-void
.end method

.method public setPnNext(I)V
    .locals 0
    .param p1, "field_5_pnNext"    # I

    .prologue
    .line 269
    iput p1, p0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->field_5_pnNext:I

    .line 270
    return-void
.end method

.method public setWIdent(I)V
    .locals 0
    .param p1, "field_1_wIdent"    # I

    .prologue
    .line 205
    iput p1, p0, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->field_1_wIdent:I

    .line 206
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    const/16 v3, 0xa

    .line 113
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 115
    .local v0, "buffer":Ljava/lang/StringBuffer;
    const-string/jumbo v1, "[FIB]\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 117
    const-string/jumbo v1, "    .wIdent               = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 118
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->getWIdent()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 120
    const-string/jumbo v1, "    .nFib                 = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 121
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->getNFib()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 123
    const-string/jumbo v1, "    .nProduct             = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 124
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->getNProduct()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 126
    const-string/jumbo v1, "    .lid                  = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 127
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->getLid()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 129
    const-string/jumbo v1, "    .pnNext               = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 130
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->getPnNext()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 132
    const-string/jumbo v1, "    .options              = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 133
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->getOptions()S

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 134
    const-string/jumbo v1, "         .fDot                     = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->isFDot()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 135
    const-string/jumbo v1, "         .fGlsy                    = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->isFGlsy()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 136
    const-string/jumbo v1, "         .fComplex                 = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->isFComplex()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 137
    const-string/jumbo v1, "         .fHasPic                  = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->isFHasPic()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 138
    const-string/jumbo v1, "         .cQuickSaves              = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->getCQuickSaves()B

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 139
    const-string/jumbo v1, "         .fEncrypted               = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->isFEncrypted()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 140
    const-string/jumbo v1, "         .fWhichTblStm             = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->isFWhichTblStm()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 141
    const-string/jumbo v1, "         .fReadOnlyRecommended     = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->isFReadOnlyRecommended()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 142
    const-string/jumbo v1, "         .fWriteReservation        = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->isFWriteReservation()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 143
    const-string/jumbo v1, "         .fExtChar                 = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->isFExtChar()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 144
    const-string/jumbo v1, "         .fLoadOverride            = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->isFLoadOverride()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 145
    const-string/jumbo v1, "         .fFarEast                 = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->isFFarEast()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 146
    const-string/jumbo v1, "         .fCrypto                  = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->isFCrypto()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 148
    const-string/jumbo v1, "    .nFibBack             = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 149
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->getNFibBack()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 151
    const-string/jumbo v1, "    .lKey                 = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 152
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->getLKey()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 154
    const-string/jumbo v1, "    .envr                 = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 155
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->getEnvr()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 157
    const-string/jumbo v1, "    .history              = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 158
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->getHistory()S

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 159
    const-string/jumbo v1, "         .fMac                     = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->isFMac()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 160
    const-string/jumbo v1, "         .fEmptySpecial            = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->isFEmptySpecial()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 161
    const-string/jumbo v1, "         .fLoadOverridePage        = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->isFLoadOverridePage()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 162
    const-string/jumbo v1, "         .fFutureSavedUndo         = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->isFFutureSavedUndo()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 163
    const-string/jumbo v1, "         .fWord97Saved             = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->isFWord97Saved()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 164
    const-string/jumbo v1, "         .fSpare0                  = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->getFSpare0()B

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 166
    const-string/jumbo v1, "    .chs                  = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 167
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->getChs()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 169
    const-string/jumbo v1, "    .chsTables            = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 170
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->getChsTables()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 172
    const-string/jumbo v1, "    .fcMin                = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 173
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->getFcMin()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 175
    const-string/jumbo v1, "    .fcMac                = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 176
    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/model/types/FIBAbstractType;->getFcMac()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, " )\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 178
    const-string/jumbo v1, "[/FIB]\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 179
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
