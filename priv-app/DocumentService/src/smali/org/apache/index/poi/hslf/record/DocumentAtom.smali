.class public final Lorg/apache/index/poi/hslf/record/DocumentAtom;
.super Lorg/apache/index/poi/hslf/record/RecordAtom;
.source "DocumentAtom.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/index/poi/hslf/record/DocumentAtom$SlideSize;
    }
.end annotation


# static fields
.field private static _type:J


# instance fields
.field private _header:[B

.field private firstSlideNum:I

.field private handoutMasterPersist:J

.field private notesMasterPersist:J

.field private notesSizeX:J

.field private notesSizeY:J

.field private omitTitlePlace:B

.field private reserved:[B

.field private rightToLeft:B

.field private saveWithFonts:B

.field private serverZoomFrom:J

.field private serverZoomTo:J

.field private showComments:B

.field private slideSizeType:I

.field private slideSizeX:J

.field private slideSizeY:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 35
    const-wide/16 v0, 0x3e9

    sput-wide v0, Lorg/apache/index/poi/hslf/record/DocumentAtom;->_type:J

    return-void
.end method

.method protected constructor <init>([BII)V
    .locals 4
    .param p1, "source"    # [B
    .param p2, "start"    # I
    .param p3, "len"    # I

    .prologue
    const/16 v1, 0x8

    const/4 v3, 0x0

    .line 108
    invoke-direct {p0}, Lorg/apache/index/poi/hslf/record/RecordAtom;-><init>()V

    .line 110
    const/16 v0, 0x30

    if-ge p3, v0, :cond_0

    const/16 p3, 0x30

    .line 113
    :cond_0
    new-array v0, v1, [B

    iput-object v0, p0, Lorg/apache/index/poi/hslf/record/DocumentAtom;->_header:[B

    .line 114
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/DocumentAtom;->_header:[B

    invoke-static {p1, p2, v0, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 117
    add-int/lit8 v0, p2, 0x0

    add-int/lit8 v0, v0, 0x8

    invoke-static {p1, v0}, Lorg/apache/index/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    int-to-long v0, v0

    iput-wide v0, p0, Lorg/apache/index/poi/hslf/record/DocumentAtom;->slideSizeX:J

    .line 118
    add-int/lit8 v0, p2, 0x4

    add-int/lit8 v0, v0, 0x8

    invoke-static {p1, v0}, Lorg/apache/index/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    int-to-long v0, v0

    iput-wide v0, p0, Lorg/apache/index/poi/hslf/record/DocumentAtom;->slideSizeY:J

    .line 119
    add-int/lit8 v0, p2, 0x8

    add-int/lit8 v0, v0, 0x8

    invoke-static {p1, v0}, Lorg/apache/index/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    int-to-long v0, v0

    iput-wide v0, p0, Lorg/apache/index/poi/hslf/record/DocumentAtom;->notesSizeX:J

    .line 120
    add-int/lit8 v0, p2, 0xc

    add-int/lit8 v0, v0, 0x8

    invoke-static {p1, v0}, Lorg/apache/index/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    int-to-long v0, v0

    iput-wide v0, p0, Lorg/apache/index/poi/hslf/record/DocumentAtom;->notesSizeY:J

    .line 121
    add-int/lit8 v0, p2, 0x10

    add-int/lit8 v0, v0, 0x8

    invoke-static {p1, v0}, Lorg/apache/index/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    int-to-long v0, v0

    iput-wide v0, p0, Lorg/apache/index/poi/hslf/record/DocumentAtom;->serverZoomFrom:J

    .line 122
    add-int/lit8 v0, p2, 0x14

    add-int/lit8 v0, v0, 0x8

    invoke-static {p1, v0}, Lorg/apache/index/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    int-to-long v0, v0

    iput-wide v0, p0, Lorg/apache/index/poi/hslf/record/DocumentAtom;->serverZoomTo:J

    .line 125
    add-int/lit8 v0, p2, 0x18

    add-int/lit8 v0, v0, 0x8

    invoke-static {p1, v0}, Lorg/apache/index/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    int-to-long v0, v0

    iput-wide v0, p0, Lorg/apache/index/poi/hslf/record/DocumentAtom;->notesMasterPersist:J

    .line 126
    add-int/lit8 v0, p2, 0x1c

    add-int/lit8 v0, v0, 0x8

    invoke-static {p1, v0}, Lorg/apache/index/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    int-to-long v0, v0

    iput-wide v0, p0, Lorg/apache/index/poi/hslf/record/DocumentAtom;->handoutMasterPersist:J

    .line 129
    add-int/lit8 v0, p2, 0x20

    add-int/lit8 v0, v0, 0x8

    invoke-static {p1, v0}, Lorg/apache/index/poi/util/LittleEndian;->getShort([BI)S

    move-result v0

    iput v0, p0, Lorg/apache/index/poi/hslf/record/DocumentAtom;->firstSlideNum:I

    .line 132
    add-int/lit8 v0, p2, 0x22

    add-int/lit8 v0, v0, 0x8

    invoke-static {p1, v0}, Lorg/apache/index/poi/util/LittleEndian;->getShort([BI)S

    move-result v0

    iput v0, p0, Lorg/apache/index/poi/hslf/record/DocumentAtom;->slideSizeType:I

    .line 135
    add-int/lit8 v0, p2, 0x24

    add-int/lit8 v0, v0, 0x8

    aget-byte v0, p1, v0

    iput-byte v0, p0, Lorg/apache/index/poi/hslf/record/DocumentAtom;->saveWithFonts:B

    .line 136
    add-int/lit8 v0, p2, 0x25

    add-int/lit8 v0, v0, 0x8

    aget-byte v0, p1, v0

    iput-byte v0, p0, Lorg/apache/index/poi/hslf/record/DocumentAtom;->omitTitlePlace:B

    .line 137
    add-int/lit8 v0, p2, 0x26

    add-int/lit8 v0, v0, 0x8

    aget-byte v0, p1, v0

    iput-byte v0, p0, Lorg/apache/index/poi/hslf/record/DocumentAtom;->rightToLeft:B

    .line 138
    add-int/lit8 v0, p2, 0x27

    add-int/lit8 v0, v0, 0x8

    aget-byte v0, p1, v0

    iput-byte v0, p0, Lorg/apache/index/poi/hslf/record/DocumentAtom;->showComments:B

    .line 141
    add-int/lit8 v0, p3, -0x28

    add-int/lit8 v0, v0, -0x8

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/apache/index/poi/hslf/record/DocumentAtom;->reserved:[B

    .line 142
    add-int/lit8 v0, p2, 0x30

    iget-object v1, p0, Lorg/apache/index/poi/hslf/record/DocumentAtom;->reserved:[B

    iget-object v2, p0, Lorg/apache/index/poi/hslf/record/DocumentAtom;->reserved:[B

    array-length v2, v2

    invoke-static {p1, v0, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 143
    return-void
.end method


# virtual methods
.method public getFirstSlideNum()I
    .locals 1

    .prologue
    .line 77
    iget v0, p0, Lorg/apache/index/poi/hslf/record/DocumentAtom;->firstSlideNum:I

    return v0
.end method

.method public getHandoutMasterPersist()J
    .locals 2

    .prologue
    .line 75
    iget-wide v0, p0, Lorg/apache/index/poi/hslf/record/DocumentAtom;->handoutMasterPersist:J

    return-wide v0
.end method

.method public getNotesMasterPersist()J
    .locals 2

    .prologue
    .line 73
    iget-wide v0, p0, Lorg/apache/index/poi/hslf/record/DocumentAtom;->notesMasterPersist:J

    return-wide v0
.end method

.method public getNotesSizeX()J
    .locals 2

    .prologue
    .line 60
    iget-wide v0, p0, Lorg/apache/index/poi/hslf/record/DocumentAtom;->notesSizeX:J

    return-wide v0
.end method

.method public getNotesSizeY()J
    .locals 2

    .prologue
    .line 61
    iget-wide v0, p0, Lorg/apache/index/poi/hslf/record/DocumentAtom;->notesSizeY:J

    return-wide v0
.end method

.method public getOmitTitlePlace()Z
    .locals 1

    .prologue
    .line 89
    iget-byte v0, p0, Lorg/apache/index/poi/hslf/record/DocumentAtom;->omitTitlePlace:B

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getRecordType()J
    .locals 2

    .prologue
    .line 148
    sget-wide v0, Lorg/apache/index/poi/hslf/record/DocumentAtom;->_type:J

    return-wide v0
.end method

.method public getRightToLeft()Z
    .locals 1

    .prologue
    .line 94
    iget-byte v0, p0, Lorg/apache/index/poi/hslf/record/DocumentAtom;->rightToLeft:B

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getSaveWithFonts()Z
    .locals 1

    .prologue
    .line 84
    iget-byte v0, p0, Lorg/apache/index/poi/hslf/record/DocumentAtom;->saveWithFonts:B

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getServerZoomFrom()J
    .locals 2

    .prologue
    .line 67
    iget-wide v0, p0, Lorg/apache/index/poi/hslf/record/DocumentAtom;->serverZoomFrom:J

    return-wide v0
.end method

.method public getServerZoomTo()J
    .locals 2

    .prologue
    .line 68
    iget-wide v0, p0, Lorg/apache/index/poi/hslf/record/DocumentAtom;->serverZoomTo:J

    return-wide v0
.end method

.method public getShowComments()Z
    .locals 1

    .prologue
    .line 99
    iget-byte v0, p0, Lorg/apache/index/poi/hslf/record/DocumentAtom;->showComments:B

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getSlideSizeType()I
    .locals 1

    .prologue
    .line 80
    iget v0, p0, Lorg/apache/index/poi/hslf/record/DocumentAtom;->slideSizeType:I

    return v0
.end method

.method public getSlideSizeX()J
    .locals 2

    .prologue
    .line 58
    iget-wide v0, p0, Lorg/apache/index/poi/hslf/record/DocumentAtom;->slideSizeX:J

    return-wide v0
.end method

.method public getSlideSizeY()J
    .locals 2

    .prologue
    .line 59
    iget-wide v0, p0, Lorg/apache/index/poi/hslf/record/DocumentAtom;->slideSizeY:J

    return-wide v0
.end method

.method public setNotesSizeX(J)V
    .locals 1
    .param p1, "x"    # J

    .prologue
    .line 64
    iput-wide p1, p0, Lorg/apache/index/poi/hslf/record/DocumentAtom;->notesSizeX:J

    return-void
.end method

.method public setNotesSizeY(J)V
    .locals 1
    .param p1, "y"    # J

    .prologue
    .line 65
    iput-wide p1, p0, Lorg/apache/index/poi/hslf/record/DocumentAtom;->notesSizeY:J

    return-void
.end method

.method public setServerZoomFrom(J)V
    .locals 1
    .param p1, "zoom"    # J

    .prologue
    .line 69
    iput-wide p1, p0, Lorg/apache/index/poi/hslf/record/DocumentAtom;->serverZoomFrom:J

    return-void
.end method

.method public setServerZoomTo(J)V
    .locals 1
    .param p1, "zoom"    # J

    .prologue
    .line 70
    iput-wide p1, p0, Lorg/apache/index/poi/hslf/record/DocumentAtom;->serverZoomTo:J

    return-void
.end method

.method public setSlideSizeX(J)V
    .locals 1
    .param p1, "x"    # J

    .prologue
    .line 62
    iput-wide p1, p0, Lorg/apache/index/poi/hslf/record/DocumentAtom;->slideSizeX:J

    return-void
.end method

.method public setSlideSizeY(J)V
    .locals 1
    .param p1, "y"    # J

    .prologue
    .line 63
    iput-wide p1, p0, Lorg/apache/index/poi/hslf/record/DocumentAtom;->slideSizeY:J

    return-void
.end method

.method public writeOut(Ljava/io/OutputStream;)V
    .locals 2
    .param p1, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 156
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/DocumentAtom;->_header:[B

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 159
    iget-wide v0, p0, Lorg/apache/index/poi/hslf/record/DocumentAtom;->slideSizeX:J

    long-to-int v0, v0

    invoke-static {v0, p1}, Lorg/apache/index/poi/hslf/record/DocumentAtom;->writeLittleEndian(ILjava/io/OutputStream;)V

    .line 160
    iget-wide v0, p0, Lorg/apache/index/poi/hslf/record/DocumentAtom;->slideSizeY:J

    long-to-int v0, v0

    invoke-static {v0, p1}, Lorg/apache/index/poi/hslf/record/DocumentAtom;->writeLittleEndian(ILjava/io/OutputStream;)V

    .line 161
    iget-wide v0, p0, Lorg/apache/index/poi/hslf/record/DocumentAtom;->notesSizeX:J

    long-to-int v0, v0

    invoke-static {v0, p1}, Lorg/apache/index/poi/hslf/record/DocumentAtom;->writeLittleEndian(ILjava/io/OutputStream;)V

    .line 162
    iget-wide v0, p0, Lorg/apache/index/poi/hslf/record/DocumentAtom;->notesSizeY:J

    long-to-int v0, v0

    invoke-static {v0, p1}, Lorg/apache/index/poi/hslf/record/DocumentAtom;->writeLittleEndian(ILjava/io/OutputStream;)V

    .line 163
    iget-wide v0, p0, Lorg/apache/index/poi/hslf/record/DocumentAtom;->serverZoomFrom:J

    long-to-int v0, v0

    invoke-static {v0, p1}, Lorg/apache/index/poi/hslf/record/DocumentAtom;->writeLittleEndian(ILjava/io/OutputStream;)V

    .line 164
    iget-wide v0, p0, Lorg/apache/index/poi/hslf/record/DocumentAtom;->serverZoomTo:J

    long-to-int v0, v0

    invoke-static {v0, p1}, Lorg/apache/index/poi/hslf/record/DocumentAtom;->writeLittleEndian(ILjava/io/OutputStream;)V

    .line 167
    iget-wide v0, p0, Lorg/apache/index/poi/hslf/record/DocumentAtom;->notesMasterPersist:J

    long-to-int v0, v0

    invoke-static {v0, p1}, Lorg/apache/index/poi/hslf/record/DocumentAtom;->writeLittleEndian(ILjava/io/OutputStream;)V

    .line 168
    iget-wide v0, p0, Lorg/apache/index/poi/hslf/record/DocumentAtom;->handoutMasterPersist:J

    long-to-int v0, v0

    invoke-static {v0, p1}, Lorg/apache/index/poi/hslf/record/DocumentAtom;->writeLittleEndian(ILjava/io/OutputStream;)V

    .line 171
    iget v0, p0, Lorg/apache/index/poi/hslf/record/DocumentAtom;->firstSlideNum:I

    int-to-short v0, v0

    invoke-static {v0, p1}, Lorg/apache/index/poi/hslf/record/DocumentAtom;->writeLittleEndian(SLjava/io/OutputStream;)V

    .line 174
    iget v0, p0, Lorg/apache/index/poi/hslf/record/DocumentAtom;->slideSizeType:I

    int-to-short v0, v0

    invoke-static {v0, p1}, Lorg/apache/index/poi/hslf/record/DocumentAtom;->writeLittleEndian(SLjava/io/OutputStream;)V

    .line 177
    iget-byte v0, p0, Lorg/apache/index/poi/hslf/record/DocumentAtom;->saveWithFonts:B

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write(I)V

    .line 178
    iget-byte v0, p0, Lorg/apache/index/poi/hslf/record/DocumentAtom;->omitTitlePlace:B

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write(I)V

    .line 179
    iget-byte v0, p0, Lorg/apache/index/poi/hslf/record/DocumentAtom;->rightToLeft:B

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write(I)V

    .line 180
    iget-byte v0, p0, Lorg/apache/index/poi/hslf/record/DocumentAtom;->showComments:B

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write(I)V

    .line 183
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/DocumentAtom;->reserved:[B

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 184
    return-void
.end method
