.class public final Lorg/apache/index/poi/poifs/storage/DocumentBlock;
.super Lorg/apache/index/poi/poifs/storage/BigBlock;
.source "DocumentBlock.java"


# static fields
.field private static final _default_value:B = -0x1t


# instance fields
.field private _blockLength:I

.field private _bytes_read:I

.field private _data:[B

.field private _isDatainStream:Z

.field private _startIndex:I

.field private mfileHandler:Lorg/apache/index/poi/poifs/filesystem/POIFSFileHandler;


# direct methods
.method public constructor <init>(Ljava/io/InputStream;Lorg/apache/index/poi/poifs/common/POIFSBigBlockSize;)V
    .locals 2
    .param p1, "stream"    # Ljava/io/InputStream;
    .param p2, "bigBlockSize"    # Lorg/apache/index/poi/poifs/common/POIFSBigBlockSize;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 92
    invoke-direct {p0, p2}, Lorg/apache/index/poi/poifs/storage/DocumentBlock;-><init>(Lorg/apache/index/poi/poifs/common/POIFSBigBlockSize;)V

    .line 93
    iget-object v1, p0, Lorg/apache/index/poi/poifs/storage/DocumentBlock;->_data:[B

    invoke-static {p1, v1}, Lorg/apache/index/poi/util/IOUtils;->readFully(Ljava/io/InputStream;[B)I

    move-result v0

    .line 95
    .local v0, "count":I
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    .end local v0    # "count":I
    :cond_0
    iput v0, p0, Lorg/apache/index/poi/poifs/storage/DocumentBlock;->_bytes_read:I

    .line 97
    return-void
.end method

.method private constructor <init>(Lorg/apache/index/poi/poifs/common/POIFSBigBlockSize;)V
    .locals 2
    .param p1, "bigBlockSize"    # Lorg/apache/index/poi/poifs/common/POIFSBigBlockSize;

    .prologue
    .line 105
    invoke-direct {p0, p1}, Lorg/apache/index/poi/poifs/storage/BigBlock;-><init>(Lorg/apache/index/poi/poifs/common/POIFSBigBlockSize;)V

    .line 106
    invoke-virtual {p1}, Lorg/apache/index/poi/poifs/common/POIFSBigBlockSize;->getBigBlockSize()I

    move-result v0

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/apache/index/poi/poifs/storage/DocumentBlock;->_data:[B

    .line 107
    iget-object v0, p0, Lorg/apache/index/poi/poifs/storage/DocumentBlock;->_data:[B

    const/4 v1, -0x1

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([BB)V

    .line 108
    return-void
.end method

.method public constructor <init>(Lorg/apache/index/poi/poifs/storage/RawDataBlock;)V
    .locals 2
    .param p1, "block"    # Lorg/apache/index/poi/poifs/storage/RawDataBlock;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 56
    .line 57
    invoke-virtual {p1}, Lorg/apache/index/poi/poifs/storage/RawDataBlock;->getBigBlockSize()I

    move-result v0

    const/16 v1, 0x200

    if-ne v0, v1, :cond_1

    .line 58
    sget-object v0, Lorg/apache/index/poi/poifs/common/POIFSConstants;->SMALLER_BIG_BLOCK_SIZE_DETAILS:Lorg/apache/index/poi/poifs/common/POIFSBigBlockSize;

    .line 59
    :goto_0
    invoke-direct {p0, v0}, Lorg/apache/index/poi/poifs/storage/BigBlock;-><init>(Lorg/apache/index/poi/poifs/common/POIFSBigBlockSize;)V

    .line 61
    invoke-virtual {p1}, Lorg/apache/index/poi/poifs/storage/RawDataBlock;->getData()[B

    move-result-object v0

    iput-object v0, p0, Lorg/apache/index/poi/poifs/storage/DocumentBlock;->_data:[B

    .line 63
    invoke-virtual {p1}, Lorg/apache/index/poi/poifs/storage/RawDataBlock;->getIsDatainStream()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 65
    invoke-virtual {p1}, Lorg/apache/index/poi/poifs/storage/RawDataBlock;->getStartIndex()I

    move-result v0

    iput v0, p0, Lorg/apache/index/poi/poifs/storage/DocumentBlock;->_startIndex:I

    .line 66
    invoke-virtual {p1}, Lorg/apache/index/poi/poifs/storage/RawDataBlock;->getBlockLength()I

    move-result v0

    iput v0, p0, Lorg/apache/index/poi/poifs/storage/DocumentBlock;->_blockLength:I

    .line 67
    invoke-virtual {p1}, Lorg/apache/index/poi/poifs/storage/RawDataBlock;->getIsDatainStream()Z

    move-result v0

    iput-boolean v0, p0, Lorg/apache/index/poi/poifs/storage/DocumentBlock;->_isDatainStream:Z

    .line 68
    invoke-virtual {p1}, Lorg/apache/index/poi/poifs/storage/RawDataBlock;->getFileHandler()Lorg/apache/index/poi/poifs/filesystem/POIFSFileHandler;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/index/poi/poifs/storage/DocumentBlock;->mfileHandler:Lorg/apache/index/poi/poifs/filesystem/POIFSFileHandler;

    .line 71
    :cond_0
    invoke-virtual {p1}, Lorg/apache/index/poi/poifs/storage/RawDataBlock;->getIsDatainStream()Z

    move-result v0

    if-nez v0, :cond_2

    .line 73
    iget-object v0, p0, Lorg/apache/index/poi/poifs/storage/DocumentBlock;->_data:[B

    array-length v0, v0

    iput v0, p0, Lorg/apache/index/poi/poifs/storage/DocumentBlock;->_bytes_read:I

    .line 79
    :goto_1
    return-void

    .line 59
    :cond_1
    sget-object v0, Lorg/apache/index/poi/poifs/common/POIFSConstants;->LARGER_BIG_BLOCK_SIZE_DETAILS:Lorg/apache/index/poi/poifs/common/POIFSBigBlockSize;

    goto :goto_0

    .line 77
    :cond_2
    iget v0, p0, Lorg/apache/index/poi/poifs/storage/DocumentBlock;->_blockLength:I

    iput v0, p0, Lorg/apache/index/poi/poifs/storage/DocumentBlock;->_bytes_read:I

    goto :goto_1
.end method

.method public static convert(Lorg/apache/index/poi/poifs/common/POIFSBigBlockSize;[BI)[Lorg/apache/index/poi/poifs/storage/DocumentBlock;
    .locals 7
    .param p0, "bigBlockSize"    # Lorg/apache/index/poi/poifs/common/POIFSBigBlockSize;
    .param p1, "array"    # [B
    .param p2, "size"    # I

    .prologue
    const/4 v6, -0x1

    .line 157
    invoke-virtual {p0}, Lorg/apache/index/poi/poifs/common/POIFSBigBlockSize;->getBigBlockSize()I

    move-result v4

    add-int/2addr v4, p2

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {p0}, Lorg/apache/index/poi/poifs/common/POIFSBigBlockSize;->getBigBlockSize()I

    move-result v5

    div-int/2addr v4, v5

    new-array v3, v4, [Lorg/apache/index/poi/poifs/storage/DocumentBlock;

    .line 158
    .local v3, "rval":[Lorg/apache/index/poi/poifs/storage/DocumentBlock;
    const/4 v2, 0x0

    .line 160
    .local v2, "offset":I
    const/4 v0, 0x0

    .local v0, "k":I
    :goto_0
    array-length v4, v3

    if-lt v0, v4, :cond_0

    .line 182
    return-object v3

    .line 162
    :cond_0
    new-instance v4, Lorg/apache/index/poi/poifs/storage/DocumentBlock;

    invoke-direct {v4, p0}, Lorg/apache/index/poi/poifs/storage/DocumentBlock;-><init>(Lorg/apache/index/poi/poifs/common/POIFSBigBlockSize;)V

    aput-object v4, v3, v0

    .line 163
    array-length v4, p1

    if-ge v2, v4, :cond_2

    .line 165
    invoke-virtual {p0}, Lorg/apache/index/poi/poifs/common/POIFSBigBlockSize;->getBigBlockSize()I

    move-result v4

    .line 166
    array-length v5, p1

    sub-int/2addr v5, v2

    .line 165
    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 168
    .local v1, "length":I
    aget-object v4, v3, v0

    iget-object v4, v4, Lorg/apache/index/poi/poifs/storage/DocumentBlock;->_data:[B

    const/4 v5, 0x0

    invoke-static {p1, v2, v4, v5, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 169
    invoke-virtual {p0}, Lorg/apache/index/poi/poifs/common/POIFSBigBlockSize;->getBigBlockSize()I

    move-result v4

    if-eq v1, v4, :cond_1

    .line 171
    aget-object v4, v3, v0

    iget-object v4, v4, Lorg/apache/index/poi/poifs/storage/DocumentBlock;->_data:[B

    .line 172
    invoke-virtual {p0}, Lorg/apache/index/poi/poifs/common/POIFSBigBlockSize;->getBigBlockSize()I

    move-result v5

    .line 171
    invoke-static {v4, v1, v5, v6}, Ljava/util/Arrays;->fill([BIIB)V

    .line 180
    .end local v1    # "length":I
    :cond_1
    :goto_1
    invoke-virtual {p0}, Lorg/apache/index/poi/poifs/common/POIFSBigBlockSize;->getBigBlockSize()I

    move-result v4

    add-int/2addr v2, v4

    .line 160
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 178
    :cond_2
    aget-object v4, v3, v0

    iget-object v4, v4, Lorg/apache/index/poi/poifs/storage/DocumentBlock;->_data:[B

    invoke-static {v4, v6}, Ljava/util/Arrays;->fill([BB)V

    goto :goto_1
.end method

.method public static getDataInputBlock([Lorg/apache/index/poi/poifs/storage/DocumentBlock;I)Lorg/apache/index/poi/poifs/storage/DataInputBlock;
    .locals 12
    .param p0, "blocks"    # [Lorg/apache/index/poi/poifs/storage/DocumentBlock;
    .param p1, "offset"    # I

    .prologue
    .line 186
    if-eqz p0, :cond_0

    array-length v0, p0

    if-nez v0, :cond_1

    .line 187
    :cond_0
    const/4 v0, 0x0

    .line 199
    :goto_0
    return-object v0

    .line 191
    :cond_1
    const/4 v0, 0x0

    aget-object v0, p0, v0

    iget-object v10, v0, Lorg/apache/index/poi/poifs/storage/DocumentBlock;->bigBlockSize:Lorg/apache/index/poi/poifs/common/POIFSBigBlockSize;

    .line 192
    .local v10, "bigBlockSize":Lorg/apache/index/poi/poifs/common/POIFSBigBlockSize;
    invoke-virtual {v10}, Lorg/apache/index/poi/poifs/common/POIFSBigBlockSize;->getHeaderValue()S

    move-result v8

    .line 193
    .local v8, "BLOCK_SHIFT":I
    invoke-virtual {v10}, Lorg/apache/index/poi/poifs/common/POIFSBigBlockSize;->getBigBlockSize()I

    move-result v9

    .line 194
    .local v9, "BLOCK_SIZE":I
    add-int/lit8 v7, v9, -0x1

    .line 197
    .local v7, "BLOCK_MASK":I
    shr-int v11, p1, v8

    .line 198
    .local v11, "firstBlockIndex":I
    and-int v2, p1, v7

    .line 199
    .local v2, "firstBlockOffset":I
    new-instance v0, Lorg/apache/index/poi/poifs/storage/DataInputBlock;

    aget-object v1, p0, v11

    iget-object v1, v1, Lorg/apache/index/poi/poifs/storage/DocumentBlock;->_data:[B

    aget-object v3, p0, v11

    iget v3, v3, Lorg/apache/index/poi/poifs/storage/DocumentBlock;->_startIndex:I

    .line 200
    aget-object v4, p0, v11

    iget v4, v4, Lorg/apache/index/poi/poifs/storage/DocumentBlock;->_blockLength:I

    aget-object v5, p0, v11

    iget-boolean v5, v5, Lorg/apache/index/poi/poifs/storage/DocumentBlock;->_isDatainStream:Z

    aget-object v6, p0, v11

    iget-object v6, v6, Lorg/apache/index/poi/poifs/storage/DocumentBlock;->mfileHandler:Lorg/apache/index/poi/poifs/filesystem/POIFSFileHandler;

    .line 199
    invoke-direct/range {v0 .. v6}, Lorg/apache/index/poi/poifs/storage/DataInputBlock;-><init>([BIIIZLorg/apache/index/poi/poifs/filesystem/POIFSFileHandler;)V

    goto :goto_0
.end method

.method public static getFillByte()B
    .locals 1

    .prologue
    .line 138
    const/4 v0, -0x1

    return v0
.end method


# virtual methods
.method public partiallyRead()Z
    .locals 2

    .prologue
    .line 129
    iget v0, p0, Lorg/apache/index/poi/poifs/storage/DocumentBlock;->_bytes_read:I

    iget-object v1, p0, Lorg/apache/index/poi/poifs/storage/DocumentBlock;->bigBlockSize:Lorg/apache/index/poi/poifs/common/POIFSBigBlockSize;

    invoke-virtual {v1}, Lorg/apache/index/poi/poifs/common/POIFSBigBlockSize;->getBigBlockSize()I

    move-result v1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 118
    iget v0, p0, Lorg/apache/index/poi/poifs/storage/DocumentBlock;->_bytes_read:I

    return v0
.end method

.method public bridge synthetic writeBlocks(Ljava/io/OutputStream;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1
    invoke-super {p0, p1}, Lorg/apache/index/poi/poifs/storage/BigBlock;->writeBlocks(Ljava/io/OutputStream;)V

    return-void
.end method

.method writeData(Ljava/io/OutputStream;)V
    .locals 1
    .param p1, "stream"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 218
    iget-object v0, p0, Lorg/apache/index/poi/poifs/storage/DocumentBlock;->_data:[B

    invoke-virtual {p0, p1, v0}, Lorg/apache/index/poi/poifs/storage/DocumentBlock;->doWriteData(Ljava/io/OutputStream;[B)V

    .line 219
    return-void
.end method
