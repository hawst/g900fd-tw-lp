.class public final Lorg/apache/index/poi/hwpf/sprm/SectionSprmUncompressor;
.super Lorg/apache/index/poi/hwpf/sprm/SprmUncompressor;
.source "SectionSprmUncompressor.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Lorg/apache/index/poi/hwpf/sprm/SprmUncompressor;-><init>()V

    .line 27
    return-void
.end method

.method static unCompressSEPOperation(Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;Lorg/apache/index/poi/hwpf/sprm/SprmOperation;)V
    .locals 5
    .param p0, "newSEP"    # Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;
    .param p1, "sprm"    # Lorg/apache/index/poi/hwpf/sprm/SprmOperation;

    .prologue
    .line 54
    invoke-virtual {p1}, Lorg/apache/index/poi/hwpf/sprm/SprmOperation;->getOperation()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 213
    :goto_0
    :pswitch_0
    return-void

    .line 57
    :pswitch_1
    invoke-virtual {p1}, Lorg/apache/index/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v1

    int-to-byte v1, v1

    invoke-virtual {p0, v1}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->setCnsPgn(B)V

    goto :goto_0

    .line 60
    :pswitch_2
    invoke-virtual {p1}, Lorg/apache/index/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v1

    int-to-byte v1, v1

    invoke-virtual {p0, v1}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->setIHeadingPgn(B)V

    goto :goto_0

    .line 63
    :pswitch_3
    invoke-virtual {p1}, Lorg/apache/index/poi/hwpf/sprm/SprmOperation;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x3

    new-array v0, v1, [B

    .line 64
    .local v0, "buf":[B
    invoke-virtual {p1}, Lorg/apache/index/poi/hwpf/sprm/SprmOperation;->getGrpprl()[B

    move-result-object v1

    invoke-virtual {p1}, Lorg/apache/index/poi/hwpf/sprm/SprmOperation;->getGrpprlOffset()I

    move-result v2

    const/4 v3, 0x0

    array-length v4, v0

    invoke-static {v1, v2, v0, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 65
    invoke-virtual {p0, v0}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->setOlstAnm([B)V

    goto :goto_0

    .line 74
    .end local v0    # "buf":[B
    :pswitch_4
    invoke-virtual {p1}, Lorg/apache/index/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v1

    invoke-static {v1}, Lorg/apache/index/poi/hwpf/sprm/SectionSprmUncompressor;->getFlag(I)Z

    move-result v1

    invoke-virtual {p0, v1}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->setFEvenlySpaced(Z)V

    goto :goto_0

    .line 77
    :pswitch_5
    invoke-virtual {p1}, Lorg/apache/index/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v1

    invoke-static {v1}, Lorg/apache/index/poi/hwpf/sprm/SectionSprmUncompressor;->getFlag(I)Z

    move-result v1

    invoke-virtual {p0, v1}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->setFUnlocked(Z)V

    goto :goto_0

    .line 80
    :pswitch_6
    invoke-virtual {p1}, Lorg/apache/index/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v1

    int-to-short v1, v1

    invoke-virtual {p0, v1}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->setDmBinFirst(I)V

    goto :goto_0

    .line 83
    :pswitch_7
    invoke-virtual {p1}, Lorg/apache/index/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v1

    int-to-short v1, v1

    invoke-virtual {p0, v1}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->setDmBinOther(I)V

    goto :goto_0

    .line 86
    :pswitch_8
    invoke-virtual {p1}, Lorg/apache/index/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v1

    int-to-byte v1, v1

    invoke-virtual {p0, v1}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->setBkc(B)V

    goto :goto_0

    .line 89
    :pswitch_9
    invoke-virtual {p1}, Lorg/apache/index/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v1

    invoke-static {v1}, Lorg/apache/index/poi/hwpf/sprm/SectionSprmUncompressor;->getFlag(I)Z

    move-result v1

    invoke-virtual {p0, v1}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->setFTitlePage(Z)V

    goto :goto_0

    .line 92
    :pswitch_a
    invoke-virtual {p1}, Lorg/apache/index/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v1

    int-to-short v1, v1

    invoke-virtual {p0, v1}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->setCcolM1(I)V

    goto :goto_0

    .line 95
    :pswitch_b
    invoke-virtual {p1}, Lorg/apache/index/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v1

    invoke-virtual {p0, v1}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->setDxaColumns(I)V

    goto :goto_0

    .line 98
    :pswitch_c
    invoke-virtual {p1}, Lorg/apache/index/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v1

    invoke-static {v1}, Lorg/apache/index/poi/hwpf/sprm/SectionSprmUncompressor;->getFlag(I)Z

    move-result v1

    invoke-virtual {p0, v1}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->setFAutoPgn(Z)V

    goto/16 :goto_0

    .line 101
    :pswitch_d
    invoke-virtual {p1}, Lorg/apache/index/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v1

    int-to-byte v1, v1

    invoke-virtual {p0, v1}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->setNfcPgn(B)V

    goto/16 :goto_0

    .line 104
    :pswitch_e
    invoke-virtual {p1}, Lorg/apache/index/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v1

    int-to-short v1, v1

    invoke-virtual {p0, v1}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->setDyaPgn(I)V

    goto/16 :goto_0

    .line 107
    :pswitch_f
    invoke-virtual {p1}, Lorg/apache/index/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v1

    int-to-short v1, v1

    invoke-virtual {p0, v1}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->setDxaPgn(I)V

    goto/16 :goto_0

    .line 110
    :pswitch_10
    invoke-virtual {p1}, Lorg/apache/index/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v1

    invoke-static {v1}, Lorg/apache/index/poi/hwpf/sprm/SectionSprmUncompressor;->getFlag(I)Z

    move-result v1

    invoke-virtual {p0, v1}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->setFPgnRestart(Z)V

    goto/16 :goto_0

    .line 113
    :pswitch_11
    invoke-virtual {p1}, Lorg/apache/index/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v1

    invoke-static {v1}, Lorg/apache/index/poi/hwpf/sprm/SectionSprmUncompressor;->getFlag(I)Z

    move-result v1

    invoke-virtual {p0, v1}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->setFEndNote(Z)V

    goto/16 :goto_0

    .line 116
    :pswitch_12
    invoke-virtual {p1}, Lorg/apache/index/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v1

    int-to-byte v1, v1

    invoke-virtual {p0, v1}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->setLnc(B)V

    goto/16 :goto_0

    .line 119
    :pswitch_13
    invoke-virtual {p1}, Lorg/apache/index/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v1

    int-to-byte v1, v1

    invoke-virtual {p0, v1}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->setGrpfIhdt(B)V

    goto/16 :goto_0

    .line 122
    :pswitch_14
    invoke-virtual {p1}, Lorg/apache/index/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v1

    int-to-short v1, v1

    invoke-virtual {p0, v1}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->setNLnnMod(I)V

    goto/16 :goto_0

    .line 125
    :pswitch_15
    invoke-virtual {p1}, Lorg/apache/index/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v1

    invoke-virtual {p0, v1}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->setDxaLnn(I)V

    goto/16 :goto_0

    .line 128
    :pswitch_16
    invoke-virtual {p1}, Lorg/apache/index/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v1

    invoke-virtual {p0, v1}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->setDyaHdrTop(I)V

    goto/16 :goto_0

    .line 131
    :pswitch_17
    invoke-virtual {p1}, Lorg/apache/index/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v1

    invoke-virtual {p0, v1}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->setDyaHdrBottom(I)V

    goto/16 :goto_0

    .line 134
    :pswitch_18
    invoke-virtual {p1}, Lorg/apache/index/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v1

    invoke-static {v1}, Lorg/apache/index/poi/hwpf/sprm/SectionSprmUncompressor;->getFlag(I)Z

    move-result v1

    invoke-virtual {p0, v1}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->setFLBetween(Z)V

    goto/16 :goto_0

    .line 137
    :pswitch_19
    invoke-virtual {p1}, Lorg/apache/index/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v1

    int-to-byte v1, v1

    invoke-virtual {p0, v1}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->setVjc(B)V

    goto/16 :goto_0

    .line 140
    :pswitch_1a
    invoke-virtual {p1}, Lorg/apache/index/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v1

    int-to-short v1, v1

    invoke-virtual {p0, v1}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->setLnnMin(I)V

    goto/16 :goto_0

    .line 143
    :pswitch_1b
    invoke-virtual {p1}, Lorg/apache/index/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v1

    int-to-short v1, v1

    invoke-virtual {p0, v1}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->setPgnStart(I)V

    goto/16 :goto_0

    .line 146
    :pswitch_1c
    invoke-virtual {p1}, Lorg/apache/index/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v1

    int-to-byte v1, v1

    invoke-virtual {p0, v1}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->setDmOrientPage(B)V

    goto/16 :goto_0

    .line 153
    :pswitch_1d
    invoke-virtual {p1}, Lorg/apache/index/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v1

    invoke-virtual {p0, v1}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->setXaPage(I)V

    goto/16 :goto_0

    .line 156
    :pswitch_1e
    invoke-virtual {p1}, Lorg/apache/index/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v1

    invoke-virtual {p0, v1}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->setYaPage(I)V

    goto/16 :goto_0

    .line 159
    :pswitch_1f
    invoke-virtual {p1}, Lorg/apache/index/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v1

    invoke-virtual {p0, v1}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->setDxaLeft(I)V

    goto/16 :goto_0

    .line 162
    :pswitch_20
    invoke-virtual {p1}, Lorg/apache/index/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v1

    invoke-virtual {p0, v1}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->setDxaRight(I)V

    goto/16 :goto_0

    .line 165
    :pswitch_21
    invoke-virtual {p1}, Lorg/apache/index/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v1

    invoke-virtual {p0, v1}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->setDyaTop(I)V

    goto/16 :goto_0

    .line 168
    :pswitch_22
    invoke-virtual {p1}, Lorg/apache/index/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v1

    invoke-virtual {p0, v1}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->setDyaBottom(I)V

    goto/16 :goto_0

    .line 171
    :pswitch_23
    invoke-virtual {p1}, Lorg/apache/index/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v1

    invoke-virtual {p0, v1}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->setDzaGutter(I)V

    goto/16 :goto_0

    .line 174
    :pswitch_24
    invoke-virtual {p1}, Lorg/apache/index/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v1

    int-to-short v1, v1

    invoke-virtual {p0, v1}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->setDmPaperReq(I)V

    goto/16 :goto_0

    .line 177
    :pswitch_25
    invoke-virtual {p1}, Lorg/apache/index/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v1

    invoke-static {v1}, Lorg/apache/index/poi/hwpf/sprm/SectionSprmUncompressor;->getFlag(I)Z

    move-result v1

    invoke-virtual {p0, v1}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->setFPropMark(Z)V

    goto/16 :goto_0

    .line 186
    :pswitch_26
    new-instance v1, Lorg/apache/index/poi/hwpf/usermodel/BorderCode;

    invoke-virtual {p1}, Lorg/apache/index/poi/hwpf/sprm/SprmOperation;->getGrpprl()[B

    move-result-object v2

    invoke-virtual {p1}, Lorg/apache/index/poi/hwpf/sprm/SprmOperation;->getGrpprlOffset()I

    move-result v3

    invoke-direct {v1, v2, v3}, Lorg/apache/index/poi/hwpf/usermodel/BorderCode;-><init>([BI)V

    invoke-virtual {p0, v1}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->setBrcTop(Lorg/apache/index/poi/hwpf/usermodel/BorderCode;)V

    goto/16 :goto_0

    .line 189
    :pswitch_27
    new-instance v1, Lorg/apache/index/poi/hwpf/usermodel/BorderCode;

    invoke-virtual {p1}, Lorg/apache/index/poi/hwpf/sprm/SprmOperation;->getGrpprl()[B

    move-result-object v2

    invoke-virtual {p1}, Lorg/apache/index/poi/hwpf/sprm/SprmOperation;->getGrpprlOffset()I

    move-result v3

    invoke-direct {v1, v2, v3}, Lorg/apache/index/poi/hwpf/usermodel/BorderCode;-><init>([BI)V

    invoke-virtual {p0, v1}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->setBrcLeft(Lorg/apache/index/poi/hwpf/usermodel/BorderCode;)V

    goto/16 :goto_0

    .line 192
    :pswitch_28
    new-instance v1, Lorg/apache/index/poi/hwpf/usermodel/BorderCode;

    invoke-virtual {p1}, Lorg/apache/index/poi/hwpf/sprm/SprmOperation;->getGrpprl()[B

    move-result-object v2

    invoke-virtual {p1}, Lorg/apache/index/poi/hwpf/sprm/SprmOperation;->getGrpprlOffset()I

    move-result v3

    invoke-direct {v1, v2, v3}, Lorg/apache/index/poi/hwpf/usermodel/BorderCode;-><init>([BI)V

    invoke-virtual {p0, v1}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->setBrcBottom(Lorg/apache/index/poi/hwpf/usermodel/BorderCode;)V

    goto/16 :goto_0

    .line 195
    :pswitch_29
    new-instance v1, Lorg/apache/index/poi/hwpf/usermodel/BorderCode;

    invoke-virtual {p1}, Lorg/apache/index/poi/hwpf/sprm/SprmOperation;->getGrpprl()[B

    move-result-object v2

    invoke-virtual {p1}, Lorg/apache/index/poi/hwpf/sprm/SprmOperation;->getGrpprlOffset()I

    move-result v3

    invoke-direct {v1, v2, v3}, Lorg/apache/index/poi/hwpf/usermodel/BorderCode;-><init>([BI)V

    invoke-virtual {p0, v1}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->setBrcRight(Lorg/apache/index/poi/hwpf/usermodel/BorderCode;)V

    goto/16 :goto_0

    .line 198
    :pswitch_2a
    invoke-virtual {p1}, Lorg/apache/index/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v1

    invoke-virtual {p0, v1}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->setPgbProp(I)V

    goto/16 :goto_0

    .line 201
    :pswitch_2b
    invoke-virtual {p1}, Lorg/apache/index/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v1

    invoke-virtual {p0, v1}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->setDxtCharSpace(I)V

    goto/16 :goto_0

    .line 204
    :pswitch_2c
    invoke-virtual {p1}, Lorg/apache/index/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v1

    invoke-virtual {p0, v1}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->setDyaLinePitch(I)V

    goto/16 :goto_0

    .line 207
    :pswitch_2d
    invoke-virtual {p1}, Lorg/apache/index/poi/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v1

    int-to-short v1, v1

    invoke-virtual {p0, v1}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->setWTextFlow(I)V

    goto/16 :goto_0

    .line 54
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_19
        :pswitch_1a
        :pswitch_1b
        :pswitch_1c
        :pswitch_0
        :pswitch_1d
        :pswitch_1e
        :pswitch_1f
        :pswitch_20
        :pswitch_21
        :pswitch_22
        :pswitch_23
        :pswitch_24
        :pswitch_25
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_26
        :pswitch_27
        :pswitch_28
        :pswitch_29
        :pswitch_2a
        :pswitch_2b
        :pswitch_2c
        :pswitch_0
        :pswitch_2d
    .end packed-switch
.end method

.method public static uncompressSEP([BI)Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;
    .locals 4
    .param p0, "grpprl"    # [B
    .param p1, "offset"    # I

    .prologue
    .line 30
    new-instance v0, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;

    invoke-direct {v0}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;-><init>()V

    .line 32
    .local v0, "newProperties":Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;
    new-instance v2, Lorg/apache/index/poi/hwpf/sprm/SprmIterator;

    invoke-direct {v2, p0, p1}, Lorg/apache/index/poi/hwpf/sprm/SprmIterator;-><init>([BI)V

    .line 34
    .local v2, "sprmIt":Lorg/apache/index/poi/hwpf/sprm/SprmIterator;
    :goto_0
    invoke-virtual {v2}, Lorg/apache/index/poi/hwpf/sprm/SprmIterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_0

    .line 40
    return-object v0

    .line 36
    :cond_0
    invoke-virtual {v2}, Lorg/apache/index/poi/hwpf/sprm/SprmIterator;->next()Lorg/apache/index/poi/hwpf/sprm/SprmOperation;

    move-result-object v1

    .line 37
    .local v1, "sprm":Lorg/apache/index/poi/hwpf/sprm/SprmOperation;
    invoke-static {v0, v1}, Lorg/apache/index/poi/hwpf/sprm/SectionSprmUncompressor;->unCompressSEPOperation(Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;Lorg/apache/index/poi/hwpf/sprm/SprmOperation;)V

    goto :goto_0
.end method
