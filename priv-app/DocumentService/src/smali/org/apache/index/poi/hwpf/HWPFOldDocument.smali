.class public Lorg/apache/index/poi/hwpf/HWPFOldDocument;
.super Lorg/apache/index/poi/hwpf/HWPFDocumentCore;
.source "HWPFOldDocument.java"


# instance fields
.field private tpt:Lorg/apache/index/poi/hwpf/model/TextPieceTable;


# direct methods
.method public constructor <init>(Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;)V
    .locals 22
    .param p1, "directory"    # Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 50
    invoke-direct/range {p0 .. p1}, Lorg/apache/index/poi/hwpf/HWPFDocumentCore;-><init>(Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;)V

    .line 53
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/index/poi/hwpf/HWPFOldDocument;->_mainStream:[B

    const/16 v3, 0x88

    invoke-static {v2, v3}, Lorg/apache/index/poi/util/LittleEndian;->getInt([BI)I

    move-result v16

    .line 54
    .local v16, "sedTableOffset":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/index/poi/hwpf/HWPFOldDocument;->_mainStream:[B

    const/16 v3, 0x8c

    invoke-static {v2, v3}, Lorg/apache/index/poi/util/LittleEndian;->getInt([BI)I

    move-result v17

    .line 55
    .local v17, "sedTableSize":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/index/poi/hwpf/HWPFOldDocument;->_mainStream:[B

    const/16 v3, 0xb8

    invoke-static {v2, v3}, Lorg/apache/index/poi/util/LittleEndian;->getInt([BI)I

    move-result v8

    .line 56
    .local v8, "chpTableOffset":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/index/poi/hwpf/HWPFOldDocument;->_mainStream:[B

    const/16 v3, 0xbc

    invoke-static {v2, v3}, Lorg/apache/index/poi/util/LittleEndian;->getInt([BI)I

    move-result v9

    .line 57
    .local v9, "chpTableSize":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/index/poi/hwpf/HWPFOldDocument;->_mainStream:[B

    const/16 v3, 0xc0

    invoke-static {v2, v3}, Lorg/apache/index/poi/util/LittleEndian;->getInt([BI)I

    move-result v12

    .line 58
    .local v12, "papTableOffset":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/index/poi/hwpf/HWPFOldDocument;->_mainStream:[B

    const/16 v3, 0xc4

    invoke-static {v2, v3}, Lorg/apache/index/poi/util/LittleEndian;->getInt([BI)I

    move-result v13

    .line 61
    .local v13, "papTableSize":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/index/poi/hwpf/HWPFOldDocument;->_mainStream:[B

    const/16 v3, 0x160

    invoke-static {v2, v3}, Lorg/apache/index/poi/util/LittleEndian;->getInt([BI)I

    .line 65
    new-instance v21, Ljava/lang/StringBuffer;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuffer;-><init>()V

    .line 66
    .local v21, "text":Ljava/lang/StringBuffer;
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/index/poi/hwpf/HWPFOldDocument;->_fib:Lorg/apache/index/poi/hwpf/model/FileInformationBlock;

    invoke-virtual {v2}, Lorg/apache/index/poi/hwpf/model/FileInformationBlock;->isFComplex()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 67
    const/16 v20, 0x0

    .line 71
    .local v20, "cft":Lorg/apache/index/poi/hwpf/model/ComplexFileTable;
    invoke-virtual/range {v20 .. v20}, Lorg/apache/index/poi/hwpf/model/ComplexFileTable;->getTextPieceTable()Lorg/apache/index/poi/hwpf/model/TextPieceTable;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lorg/apache/index/poi/hwpf/HWPFOldDocument;->tpt:Lorg/apache/index/poi/hwpf/model/TextPieceTable;

    .line 73
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/index/poi/hwpf/HWPFOldDocument;->tpt:Lorg/apache/index/poi/hwpf/model/TextPieceTable;

    invoke-virtual {v2}, Lorg/apache/index/poi/hwpf/model/TextPieceTable;->getTextPieces()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_0

    .line 98
    .end local v20    # "cft":Lorg/apache/index/poi/hwpf/model/ComplexFileTable;
    :goto_1
    new-instance v6, Lorg/apache/index/poi/hwpf/model/OldCHPBinTable;

    .line 99
    move-object/from16 v0, p0

    iget-object v7, v0, Lorg/apache/index/poi/hwpf/HWPFOldDocument;->_mainStream:[B

    .line 100
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/index/poi/hwpf/HWPFOldDocument;->_fib:Lorg/apache/index/poi/hwpf/model/FileInformationBlock;

    invoke-virtual {v2}, Lorg/apache/index/poi/hwpf/model/FileInformationBlock;->getFcMin()I

    move-result v10

    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/apache/index/poi/hwpf/HWPFOldDocument;->tpt:Lorg/apache/index/poi/hwpf/model/TextPieceTable;

    invoke-direct/range {v6 .. v11}, Lorg/apache/index/poi/hwpf/model/OldCHPBinTable;-><init>([BIIILorg/apache/index/poi/hwpf/model/TextPieceTable;)V

    .line 98
    move-object/from16 v0, p0

    iput-object v6, v0, Lorg/apache/index/poi/hwpf/HWPFOldDocument;->_cbt:Lorg/apache/index/poi/hwpf/model/CHPBinTable;

    .line 102
    new-instance v10, Lorg/apache/index/poi/hwpf/model/OldPAPBinTable;

    .line 103
    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/apache/index/poi/hwpf/HWPFOldDocument;->_mainStream:[B

    .line 104
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/index/poi/hwpf/HWPFOldDocument;->_fib:Lorg/apache/index/poi/hwpf/model/FileInformationBlock;

    invoke-virtual {v2}, Lorg/apache/index/poi/hwpf/model/FileInformationBlock;->getFcMin()I

    move-result v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/index/poi/hwpf/HWPFOldDocument;->tpt:Lorg/apache/index/poi/hwpf/model/TextPieceTable;

    invoke-direct/range {v10 .. v15}, Lorg/apache/index/poi/hwpf/model/OldPAPBinTable;-><init>([BIIILorg/apache/index/poi/hwpf/model/TextPieceTable;)V

    .line 102
    move-object/from16 v0, p0

    iput-object v10, v0, Lorg/apache/index/poi/hwpf/HWPFOldDocument;->_pbt:Lorg/apache/index/poi/hwpf/model/PAPBinTable;

    .line 106
    new-instance v14, Lorg/apache/index/poi/hwpf/model/OldSectionTable;

    .line 107
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/index/poi/hwpf/HWPFOldDocument;->_mainStream:[B

    .line 108
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/index/poi/hwpf/HWPFOldDocument;->_fib:Lorg/apache/index/poi/hwpf/model/FileInformationBlock;

    invoke-virtual {v2}, Lorg/apache/index/poi/hwpf/model/FileInformationBlock;->getFcMin()I

    move-result v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/index/poi/hwpf/HWPFOldDocument;->tpt:Lorg/apache/index/poi/hwpf/model/TextPieceTable;

    move-object/from16 v19, v0

    invoke-direct/range {v14 .. v19}, Lorg/apache/index/poi/hwpf/model/OldSectionTable;-><init>([BIIILorg/apache/index/poi/hwpf/model/TextPieceTable;)V

    .line 106
    move-object/from16 v0, p0

    iput-object v14, v0, Lorg/apache/index/poi/hwpf/HWPFOldDocument;->_st:Lorg/apache/index/poi/hwpf/model/SectionTable;

    .line 110
    return-void

    .line 73
    .restart local v20    # "cft":Lorg/apache/index/poi/hwpf/model/ComplexFileTable;
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/index/poi/hwpf/model/TextPiece;

    .line 74
    .local v1, "tp":Lorg/apache/index/poi/hwpf/model/TextPiece;
    invoke-virtual {v1}, Lorg/apache/index/poi/hwpf/model/TextPiece;->getStringBuffer()Ljava/lang/StringBuffer;

    move-result-object v3

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;

    goto :goto_0

    .line 81
    .end local v1    # "tp":Lorg/apache/index/poi/hwpf/model/TextPiece;
    .end local v20    # "cft":Lorg/apache/index/poi/hwpf/model/ComplexFileTable;
    :cond_1
    new-instance v5, Lorg/apache/index/poi/hwpf/model/PieceDescriptor;

    const/16 v2, 0x8

    new-array v2, v2, [B

    const/4 v3, 0x5

    const/16 v6, 0x7f

    aput-byte v6, v2, v3

    const/4 v3, 0x0

    invoke-direct {v5, v2, v3}, Lorg/apache/index/poi/hwpf/model/PieceDescriptor;-><init>([BI)V

    .line 82
    .local v5, "pd":Lorg/apache/index/poi/hwpf/model/PieceDescriptor;
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/index/poi/hwpf/HWPFOldDocument;->_fib:Lorg/apache/index/poi/hwpf/model/FileInformationBlock;

    invoke-virtual {v2}, Lorg/apache/index/poi/hwpf/model/FileInformationBlock;->getFcMin()I

    move-result v2

    invoke-virtual {v5, v2}, Lorg/apache/index/poi/hwpf/model/PieceDescriptor;->setFilePosition(I)V

    .line 86
    new-instance v2, Lorg/apache/index/poi/hwpf/model/TextPieceTable;

    invoke-direct {v2}, Lorg/apache/index/poi/hwpf/model/TextPieceTable;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lorg/apache/index/poi/hwpf/HWPFOldDocument;->tpt:Lorg/apache/index/poi/hwpf/model/TextPieceTable;

    .line 87
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/index/poi/hwpf/HWPFOldDocument;->_fib:Lorg/apache/index/poi/hwpf/model/FileInformationBlock;

    invoke-virtual {v2}, Lorg/apache/index/poi/hwpf/model/FileInformationBlock;->getFcMac()I

    move-result v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/index/poi/hwpf/HWPFOldDocument;->_fib:Lorg/apache/index/poi/hwpf/model/FileInformationBlock;

    invoke-virtual {v3}, Lorg/apache/index/poi/hwpf/model/FileInformationBlock;->getFcMin()I

    move-result v3

    sub-int/2addr v2, v3

    new-array v4, v2, [B

    .line 88
    .local v4, "textData":[B
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/index/poi/hwpf/HWPFOldDocument;->_mainStream:[B

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/index/poi/hwpf/HWPFOldDocument;->_fib:Lorg/apache/index/poi/hwpf/model/FileInformationBlock;

    invoke-virtual {v3}, Lorg/apache/index/poi/hwpf/model/FileInformationBlock;->getFcMin()I

    move-result v3

    const/4 v6, 0x0

    array-length v7, v4

    invoke-static {v2, v3, v4, v6, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 89
    new-instance v1, Lorg/apache/index/poi/hwpf/model/TextPiece;

    .line 90
    const/4 v2, 0x0

    array-length v3, v4

    const/4 v6, 0x0

    .line 89
    invoke-direct/range {v1 .. v6}, Lorg/apache/index/poi/hwpf/model/TextPiece;-><init>(II[BLorg/apache/index/poi/hwpf/model/PieceDescriptor;I)V

    .line 92
    .restart local v1    # "tp":Lorg/apache/index/poi/hwpf/model/TextPiece;
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/index/poi/hwpf/HWPFOldDocument;->tpt:Lorg/apache/index/poi/hwpf/model/TextPieceTable;

    invoke-virtual {v2, v1}, Lorg/apache/index/poi/hwpf/model/TextPieceTable;->add(Lorg/apache/index/poi/hwpf/model/TextPiece;)V

    .line 94
    invoke-virtual {v1}, Lorg/apache/index/poi/hwpf/model/TextPiece;->getStringBuffer()Ljava/lang/StringBuffer;

    move-result-object v2

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;

    goto/16 :goto_1
.end method

.method public constructor <init>(Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;)V
    .locals 0
    .param p1, "directory"    # Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;
    .param p2, "fs"    # Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 46
    invoke-direct {p0, p1}, Lorg/apache/index/poi/hwpf/HWPFOldDocument;-><init>(Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;)V

    .line 47
    return-void
.end method

.method public constructor <init>(Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;)V
    .locals 1
    .param p1, "fs"    # Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 41
    invoke-virtual {p1}, Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;->getRoot()Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/apache/index/poi/hwpf/HWPFOldDocument;-><init>(Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;)V

    .line 42
    return-void
.end method


# virtual methods
.method public getRange()Lorg/apache/index/poi/hwpf/usermodel/Range;
    .locals 4

    .prologue
    .line 114
    new-instance v0, Lorg/apache/index/poi/hwpf/usermodel/Range;

    .line 115
    const/4 v1, 0x0

    iget-object v2, p0, Lorg/apache/index/poi/hwpf/HWPFOldDocument;->_fib:Lorg/apache/index/poi/hwpf/model/FileInformationBlock;

    invoke-virtual {v2}, Lorg/apache/index/poi/hwpf/model/FileInformationBlock;->getFcMac()I

    move-result v2

    iget-object v3, p0, Lorg/apache/index/poi/hwpf/HWPFOldDocument;->_fib:Lorg/apache/index/poi/hwpf/model/FileInformationBlock;

    invoke-virtual {v3}, Lorg/apache/index/poi/hwpf/model/FileInformationBlock;->getFcMin()I

    move-result v3

    sub-int/2addr v2, v3

    .line 114
    invoke-direct {v0, v1, v2, p0}, Lorg/apache/index/poi/hwpf/usermodel/Range;-><init>(IILorg/apache/index/poi/hwpf/HWPFDocumentCore;)V

    return-object v0
.end method

.method public getTextTable()Lorg/apache/index/poi/hwpf/model/TextPieceTable;
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/HWPFOldDocument;->tpt:Lorg/apache/index/poi/hwpf/model/TextPieceTable;

    return-object v0
.end method
