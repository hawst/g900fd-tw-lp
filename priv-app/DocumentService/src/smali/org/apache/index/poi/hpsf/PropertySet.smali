.class public Lorg/apache/index/poi/hpsf/PropertySet;
.super Ljava/lang/Object;
.source "PropertySet.java"


# static fields
.field static final BYTE_ORDER_ASSERTION:[B

.field static final FORMAT_ASSERTION:[B

.field public static final OS_MACINTOSH:I = 0x1

.field public static final OS_WIN16:I = 0x0

.field public static final OS_WIN32:I = 0x2


# instance fields
.field protected byteOrder:I

.field protected classID:Lorg/apache/index/poi/hpsf/ClassID;

.field protected format:I

.field protected osVersion:I

.field protected sections:Ljava/util/List;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x2

    .line 69
    new-array v0, v1, [B

    fill-array-data v0, :array_0

    .line 68
    sput-object v0, Lorg/apache/index/poi/hpsf/PropertySet;->BYTE_ORDER_ASSERTION:[B

    .line 94
    new-array v0, v1, [B

    .line 93
    sput-object v0, Lorg/apache/index/poi/hpsf/PropertySet;->FORMAT_ASSERTION:[B

    .line 139
    return-void

    .line 69
    nop

    :array_0
    .array-data 1
        -0x2t
        -0x1t
    .end array-data
.end method

.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 212
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 213
    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 4
    .param p1, "stream"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/index/poi/hpsf/NoPropertySetStreamException;,
            Lorg/apache/index/poi/hpsf/MarkUnsupportedException;,
            Ljava/io/IOException;,
            Ljava/io/UnsupportedEncodingException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 239
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 243
    invoke-static {p1}, Lorg/apache/index/poi/hpsf/PropertySet;->isPropertySetStream(Ljava/io/InputStream;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 245
    invoke-virtual {p1}, Ljava/io/InputStream;->available()I

    move-result v0

    .line 246
    .local v0, "avail":I
    new-array v1, v0, [B

    .line 247
    .local v1, "buffer":[B
    array-length v2, v1

    invoke-virtual {p1, v1, v3, v2}, Ljava/io/InputStream;->read([BII)I

    .line 248
    array-length v2, v1

    invoke-direct {p0, v1, v3, v2}, Lorg/apache/index/poi/hpsf/PropertySet;->init([BII)V

    .line 252
    return-void

    .line 251
    .end local v0    # "avail":I
    .end local v1    # "buffer":[B
    :cond_0
    new-instance v2, Lorg/apache/index/poi/hpsf/NoPropertySetStreamException;

    invoke-direct {v2}, Lorg/apache/index/poi/hpsf/NoPropertySetStreamException;-><init>()V

    throw v2
.end method

.method public constructor <init>([B)V
    .locals 2
    .param p1, "stream"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/index/poi/hpsf/NoPropertySetStreamException;,
            Ljava/io/UnsupportedEncodingException;
        }
    .end annotation

    .prologue
    .line 297
    const/4 v0, 0x0

    array-length v1, p1

    invoke-direct {p0, p1, v0, v1}, Lorg/apache/index/poi/hpsf/PropertySet;-><init>([BII)V

    .line 298
    return-void
.end method

.method public constructor <init>([BII)V
    .locals 1
    .param p1, "stream"    # [B
    .param p2, "offset"    # I
    .param p3, "length"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/index/poi/hpsf/NoPropertySetStreamException;,
            Ljava/io/UnsupportedEncodingException;
        }
    .end annotation

    .prologue
    .line 271
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 274
    invoke-static {p1, p2, p3}, Lorg/apache/index/poi/hpsf/PropertySet;->isPropertySetStream([BII)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 275
    invoke-direct {p0, p1, p2, p3}, Lorg/apache/index/poi/hpsf/PropertySet;->init([BII)V

    .line 278
    return-void

    .line 277
    :cond_0
    new-instance v0, Lorg/apache/index/poi/hpsf/NoPropertySetStreamException;

    invoke-direct {v0}, Lorg/apache/index/poi/hpsf/NoPropertySetStreamException;-><init>()V

    throw v0
.end method

.method private init([BII)V
    .locals 7
    .param p1, "src"    # [B
    .param p2, "offset"    # I
    .param p3, "length"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/UnsupportedEncodingException;
        }
    .end annotation

    .prologue
    .line 417
    move v1, p2

    .line 418
    .local v1, "o":I
    invoke-static {p1, v1}, Lorg/apache/index/poi/util/LittleEndian;->getUShort([BI)I

    move-result v4

    iput v4, p0, Lorg/apache/index/poi/hpsf/PropertySet;->byteOrder:I

    .line 419
    add-int/lit8 v1, v1, 0x2

    .line 420
    invoke-static {p1, v1}, Lorg/apache/index/poi/util/LittleEndian;->getUShort([BI)I

    move-result v4

    iput v4, p0, Lorg/apache/index/poi/hpsf/PropertySet;->format:I

    .line 421
    add-int/lit8 v1, v1, 0x2

    .line 422
    invoke-static {p1, v1}, Lorg/apache/index/poi/util/LittleEndian;->getUInt([BI)J

    move-result-wide v4

    long-to-int v4, v4

    iput v4, p0, Lorg/apache/index/poi/hpsf/PropertySet;->osVersion:I

    .line 423
    add-int/lit8 v1, v1, 0x4

    .line 424
    new-instance v4, Lorg/apache/index/poi/hpsf/ClassID;

    invoke-direct {v4, p1, v1}, Lorg/apache/index/poi/hpsf/ClassID;-><init>([BI)V

    iput-object v4, p0, Lorg/apache/index/poi/hpsf/PropertySet;->classID:Lorg/apache/index/poi/hpsf/ClassID;

    .line 425
    add-int/lit8 v1, v1, 0x10

    .line 426
    invoke-static {p1, v1}, Lorg/apache/index/poi/util/LittleEndian;->getInt([BI)I

    move-result v3

    .line 427
    .local v3, "sectionCount":I
    add-int/lit8 v1, v1, 0x4

    .line 428
    if-gez v3, :cond_0

    .line 429
    new-instance v4, Lorg/apache/index/poi/hpsf/HPSFRuntimeException;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "Section count "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 430
    const-string/jumbo v6, " is negative."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 429
    invoke-direct {v4, v5}, Lorg/apache/index/poi/hpsf/HPSFRuntimeException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 444
    :cond_0
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4, v3}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v4, p0, Lorg/apache/index/poi/hpsf/PropertySet;->sections:Ljava/util/List;

    .line 451
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-lt v0, v3, :cond_1

    .line 457
    return-void

    .line 453
    :cond_1
    new-instance v2, Lorg/apache/index/poi/hpsf/Section;

    invoke-direct {v2, p1, v1}, Lorg/apache/index/poi/hpsf/Section;-><init>([BI)V

    .line 454
    .local v2, "s":Lorg/apache/index/poi/hpsf/Section;
    add-int/lit8 v1, v1, 0x14

    .line 455
    iget-object v4, p0, Lorg/apache/index/poi/hpsf/PropertySet;->sections:Ljava/util/List;

    invoke-interface {v4, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 451
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public static isPropertySetStream(Ljava/io/InputStream;)Z
    .locals 7
    .param p0, "stream"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/index/poi/hpsf/MarkUnsupportedException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v5, 0x32

    const/4 v6, 0x0

    .line 323
    const/16 v0, 0x32

    .line 330
    .local v0, "BUFFER_SIZE":I
    invoke-virtual {p0}, Ljava/io/InputStream;->markSupported()Z

    move-result v4

    if-nez v4, :cond_0

    .line 331
    new-instance v4, Lorg/apache/index/poi/hpsf/MarkUnsupportedException;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Lorg/apache/index/poi/hpsf/MarkUnsupportedException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 332
    :cond_0
    invoke-virtual {p0, v5}, Ljava/io/InputStream;->mark(I)V

    .line 337
    new-array v1, v5, [B

    .line 340
    .local v1, "buffer":[B
    array-length v4, v1

    invoke-virtual {p0}, Ljava/io/InputStream;->available()I

    move-result v5

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v4

    .line 339
    invoke-virtual {p0, v1, v6, v4}, Ljava/io/InputStream;->read([BII)I

    move-result v2

    .line 342
    .local v2, "bytes":I
    invoke-static {v1, v6, v2}, Lorg/apache/index/poi/hpsf/PropertySet;->isPropertySetStream([BII)Z

    move-result v3

    .line 343
    .local v3, "isPropertySetStream":Z
    invoke-virtual {p0}, Ljava/io/InputStream;->reset()V

    .line 344
    return v3
.end method

.method public static isPropertySetStream([BII)Z
    .locals 10
    .param p0, "src"    # [B
    .param p1, "offset"    # I
    .param p2, "length"    # I

    .prologue
    const/4 v8, 0x2

    const/4 v6, 0x0

    .line 370
    move v2, p1

    .line 371
    .local v2, "o":I
    invoke-static {p0, v2}, Lorg/apache/index/poi/util/LittleEndian;->getUShort([BI)I

    move-result v0

    .line 372
    .local v0, "byteOrder":I
    add-int/lit8 v2, v2, 0x2

    .line 373
    new-array v3, v8, [B

    .line 374
    .local v3, "temp":[B
    int-to-short v7, v0

    invoke-static {v3, v7}, Lorg/apache/index/poi/util/LittleEndian;->putShort([BS)V

    .line 375
    sget-object v7, Lorg/apache/index/poi/hpsf/PropertySet;->BYTE_ORDER_ASSERTION:[B

    invoke-static {v3, v7}, Lorg/apache/index/poi/hpsf/Util;->equal([B[B)Z

    move-result v7

    if-nez v7, :cond_1

    .line 391
    :cond_0
    :goto_0
    return v6

    .line 377
    :cond_1
    invoke-static {p0, v2}, Lorg/apache/index/poi/util/LittleEndian;->getUShort([BI)I

    move-result v1

    .line 378
    .local v1, "format":I
    add-int/lit8 v2, v2, 0x2

    .line 379
    new-array v3, v8, [B

    .line 380
    int-to-short v7, v1

    invoke-static {v3, v7}, Lorg/apache/index/poi/util/LittleEndian;->putShort([BS)V

    .line 381
    sget-object v7, Lorg/apache/index/poi/hpsf/PropertySet;->FORMAT_ASSERTION:[B

    invoke-static {v3, v7}, Lorg/apache/index/poi/hpsf/Util;->equal([B[B)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 384
    add-int/lit8 v2, v2, 0x4

    .line 386
    add-int/lit8 v2, v2, 0x10

    .line 387
    invoke-static {p0, v2}, Lorg/apache/index/poi/util/LittleEndian;->getUInt([BI)J

    move-result-wide v4

    .line 388
    .local v4, "sectionCount":J
    add-int/lit8 v2, v2, 0x4

    .line 389
    const-wide/16 v8, 0x0

    cmp-long v7, v4, v8

    if-ltz v7, :cond_0

    .line 391
    const/4 v6, 0x1

    goto :goto_0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 13
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v11, 0x0

    .line 638
    if-eqz p1, :cond_0

    instance-of v12, p1, Lorg/apache/index/poi/hpsf/PropertySet;

    if-nez v12, :cond_1

    .line 659
    :cond_0
    :goto_0
    return v11

    :cond_1
    move-object v8, p1

    .line 640
    check-cast v8, Lorg/apache/index/poi/hpsf/PropertySet;

    .line 641
    .local v8, "ps":Lorg/apache/index/poi/hpsf/PropertySet;
    invoke-virtual {v8}, Lorg/apache/index/poi/hpsf/PropertySet;->getByteOrder()I

    move-result v0

    .line 642
    .local v0, "byteOrder1":I
    invoke-virtual {p0}, Lorg/apache/index/poi/hpsf/PropertySet;->getByteOrder()I

    move-result v1

    .line 643
    .local v1, "byteOrder2":I
    invoke-virtual {v8}, Lorg/apache/index/poi/hpsf/PropertySet;->getClassID()Lorg/apache/index/poi/hpsf/ClassID;

    move-result-object v2

    .line 644
    .local v2, "classID1":Lorg/apache/index/poi/hpsf/ClassID;
    invoke-virtual {p0}, Lorg/apache/index/poi/hpsf/PropertySet;->getClassID()Lorg/apache/index/poi/hpsf/ClassID;

    move-result-object v3

    .line 645
    .local v3, "classID2":Lorg/apache/index/poi/hpsf/ClassID;
    invoke-virtual {v8}, Lorg/apache/index/poi/hpsf/PropertySet;->getFormat()I

    move-result v4

    .line 646
    .local v4, "format1":I
    invoke-virtual {p0}, Lorg/apache/index/poi/hpsf/PropertySet;->getFormat()I

    move-result v5

    .line 647
    .local v5, "format2":I
    invoke-virtual {v8}, Lorg/apache/index/poi/hpsf/PropertySet;->getOSVersion()I

    move-result v6

    .line 648
    .local v6, "osVersion1":I
    invoke-virtual {p0}, Lorg/apache/index/poi/hpsf/PropertySet;->getOSVersion()I

    move-result v7

    .line 649
    .local v7, "osVersion2":I
    invoke-virtual {v8}, Lorg/apache/index/poi/hpsf/PropertySet;->getSectionCount()I

    move-result v9

    .line 650
    .local v9, "sectionCount1":I
    invoke-virtual {p0}, Lorg/apache/index/poi/hpsf/PropertySet;->getSectionCount()I

    move-result v10

    .line 651
    .local v10, "sectionCount2":I
    if-ne v0, v1, :cond_0

    .line 652
    invoke-virtual {v2, v3}, Lorg/apache/index/poi/hpsf/ClassID;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_0

    .line 653
    if-ne v4, v5, :cond_0

    .line 654
    if-ne v6, v7, :cond_0

    .line 655
    if-ne v9, v10, :cond_0

    .line 659
    invoke-virtual {p0}, Lorg/apache/index/poi/hpsf/PropertySet;->getSections()Ljava/util/List;

    move-result-object v11

    invoke-virtual {v8}, Lorg/apache/index/poi/hpsf/PropertySet;->getSections()Ljava/util/List;

    move-result-object v12

    invoke-static {v11, v12}, Lorg/apache/index/poi/hpsf/Util;->equals(Ljava/util/Collection;Ljava/util/Collection;)Z

    move-result v11

    goto :goto_0
.end method

.method public getByteOrder()I
    .locals 1

    .prologue
    .line 85
    iget v0, p0, Lorg/apache/index/poi/hpsf/PropertySet;->byteOrder:I

    return v0
.end method

.method public getClassID()Lorg/apache/index/poi/hpsf/ClassID;
    .locals 1

    .prologue
    .line 168
    iget-object v0, p0, Lorg/apache/index/poi/hpsf/PropertySet;->classID:Lorg/apache/index/poi/hpsf/ClassID;

    return-object v0
.end method

.method public getFirstSection()Lorg/apache/index/poi/hpsf/Section;
    .locals 2

    .prologue
    .line 603
    invoke-virtual {p0}, Lorg/apache/index/poi/hpsf/PropertySet;->getSectionCount()I

    move-result v0

    const/4 v1, 0x1

    if-ge v0, v1, :cond_0

    .line 604
    new-instance v0, Lorg/apache/index/poi/hpsf/MissingSectionException;

    const-string/jumbo v1, "Property set does not contain any sections."

    invoke-direct {v0, v1}, Lorg/apache/index/poi/hpsf/MissingSectionException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 605
    :cond_0
    iget-object v0, p0, Lorg/apache/index/poi/hpsf/PropertySet;->sections:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/index/poi/hpsf/Section;

    return-object v0
.end method

.method public getFormat()I
    .locals 1

    .prologue
    .line 110
    iget v0, p0, Lorg/apache/index/poi/hpsf/PropertySet;->format:I

    return v0
.end method

.method public getOSVersion()I
    .locals 1

    .prologue
    .line 149
    iget v0, p0, Lorg/apache/index/poi/hpsf/PropertySet;->osVersion:I

    return v0
.end method

.method public getProperties()[Lorg/apache/index/poi/hpsf/Property;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/index/poi/hpsf/NoSingleSectionException;
        }
    .end annotation

    .prologue
    .line 510
    invoke-virtual {p0}, Lorg/apache/index/poi/hpsf/PropertySet;->getFirstSection()Lorg/apache/index/poi/hpsf/Section;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/index/poi/hpsf/Section;->getProperties()[Lorg/apache/index/poi/hpsf/Property;

    move-result-object v0

    return-object v0
.end method

.method protected getProperty(I)Ljava/lang/Object;
    .locals 4
    .param p1, "id"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/index/poi/hpsf/NoSingleSectionException;
        }
    .end annotation

    .prologue
    .line 528
    invoke-virtual {p0}, Lorg/apache/index/poi/hpsf/PropertySet;->getFirstSection()Lorg/apache/index/poi/hpsf/Section;

    move-result-object v0

    int-to-long v2, p1

    invoke-virtual {v0, v2, v3}, Lorg/apache/index/poi/hpsf/Section;->getProperty(J)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method protected getPropertyBooleanValue(I)Z
    .locals 1
    .param p1, "id"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/index/poi/hpsf/NoSingleSectionException;
        }
    .end annotation

    .prologue
    .line 549
    invoke-virtual {p0}, Lorg/apache/index/poi/hpsf/PropertySet;->getFirstSection()Lorg/apache/index/poi/hpsf/Section;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/index/poi/hpsf/Section;->getPropertyBooleanValue(I)Z

    move-result v0

    return v0
.end method

.method protected getPropertyIntValue(I)I
    .locals 4
    .param p1, "id"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/index/poi/hpsf/NoSingleSectionException;
        }
    .end annotation

    .prologue
    .line 569
    invoke-virtual {p0}, Lorg/apache/index/poi/hpsf/PropertySet;->getFirstSection()Lorg/apache/index/poi/hpsf/Section;

    move-result-object v0

    int-to-long v2, p1

    invoke-virtual {v0, v2, v3}, Lorg/apache/index/poi/hpsf/Section;->getPropertyIntValue(J)I

    move-result v0

    return v0
.end method

.method public getSectionCount()I
    .locals 1

    .prologue
    .line 181
    iget-object v0, p0, Lorg/apache/index/poi/hpsf/PropertySet;->sections:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getSections()Ljava/util/List;
    .locals 1

    .prologue
    .line 199
    iget-object v0, p0, Lorg/apache/index/poi/hpsf/PropertySet;->sections:Ljava/util/List;

    return-object v0
.end method

.method public getSingleSection()Lorg/apache/index/poi/hpsf/Section;
    .locals 4

    .prologue
    .line 618
    invoke-virtual {p0}, Lorg/apache/index/poi/hpsf/PropertySet;->getSectionCount()I

    move-result v0

    .line 619
    .local v0, "sectionCount":I
    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    .line 620
    new-instance v1, Lorg/apache/index/poi/hpsf/NoSingleSectionException;

    .line 621
    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Property set contains "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " sections."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 620
    invoke-direct {v1, v2}, Lorg/apache/index/poi/hpsf/NoSingleSectionException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 622
    :cond_0
    iget-object v1, p0, Lorg/apache/index/poi/hpsf/PropertySet;->sections:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/index/poi/hpsf/Section;

    return-object v1
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 669
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string/jumbo v1, "FIXME: Not yet implemented."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public isDocumentSummaryInformation()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 487
    iget-object v0, p0, Lorg/apache/index/poi/hpsf/PropertySet;->sections:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-gtz v0, :cond_0

    move v0, v1

    .line 489
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lorg/apache/index/poi/hpsf/PropertySet;->sections:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/index/poi/hpsf/Section;

    invoke-virtual {v0}, Lorg/apache/index/poi/hpsf/Section;->getFormatID()Lorg/apache/index/poi/hpsf/ClassID;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/index/poi/hpsf/ClassID;->getBytes()[B

    move-result-object v0

    .line 490
    sget-object v2, Lorg/apache/index/poi/hpsf/wellknown/SectionIDMap;->DOCUMENT_SUMMARY_INFORMATION_ID:[[B

    aget-object v1, v2, v1

    .line 489
    invoke-static {v0, v1}, Lorg/apache/index/poi/hpsf/Util;->equal([B[B)Z

    move-result v0

    goto :goto_0
.end method

.method public isSummaryInformation()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 470
    iget-object v1, p0, Lorg/apache/index/poi/hpsf/PropertySet;->sections:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-gtz v1, :cond_0

    .line 472
    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Lorg/apache/index/poi/hpsf/PropertySet;->sections:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/index/poi/hpsf/Section;

    invoke-virtual {v0}, Lorg/apache/index/poi/hpsf/Section;->getFormatID()Lorg/apache/index/poi/hpsf/ClassID;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/index/poi/hpsf/ClassID;->getBytes()[B

    move-result-object v0

    .line 473
    sget-object v1, Lorg/apache/index/poi/hpsf/wellknown/SectionIDMap;->SUMMARY_INFORMATION_ID:[B

    .line 472
    invoke-static {v0, v1}, Lorg/apache/index/poi/hpsf/Util;->equal([B[B)Z

    move-result v0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .prologue
    const/16 v5, 0x5d

    .line 679
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 680
    .local v0, "b":Ljava/lang/StringBuffer;
    invoke-virtual {p0}, Lorg/apache/index/poi/hpsf/PropertySet;->getSectionCount()I

    move-result v2

    .line 681
    .local v2, "sectionCount":I
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 682
    const/16 v4, 0x5b

    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 683
    const-string/jumbo v4, "byteOrder: "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 684
    invoke-virtual {p0}, Lorg/apache/index/poi/hpsf/PropertySet;->getByteOrder()I

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 685
    const-string/jumbo v4, ", classID: "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 686
    invoke-virtual {p0}, Lorg/apache/index/poi/hpsf/PropertySet;->getClassID()Lorg/apache/index/poi/hpsf/ClassID;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    .line 687
    const-string/jumbo v4, ", format: "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 688
    invoke-virtual {p0}, Lorg/apache/index/poi/hpsf/PropertySet;->getFormat()I

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 689
    const-string/jumbo v4, ", OSVersion: "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 690
    invoke-virtual {p0}, Lorg/apache/index/poi/hpsf/PropertySet;->getOSVersion()I

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 691
    const-string/jumbo v4, ", sectionCount: "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 692
    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 693
    const-string/jumbo v4, ", sections: [\n"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 694
    invoke-virtual {p0}, Lorg/apache/index/poi/hpsf/PropertySet;->getSections()Ljava/util/List;

    move-result-object v3

    .line 695
    .local v3, "sections":Ljava/util/List;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-lt v1, v2, :cond_0

    .line 697
    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 698
    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 699
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4

    .line 696
    :cond_0
    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/index/poi/hpsf/Section;

    invoke-virtual {v4}, Lorg/apache/index/poi/hpsf/Section;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 695
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public wasNull()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/index/poi/hpsf/NoSingleSectionException;
        }
    .end annotation

    .prologue
    .line 591
    invoke-virtual {p0}, Lorg/apache/index/poi/hpsf/PropertySet;->getFirstSection()Lorg/apache/index/poi/hpsf/Section;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/index/poi/hpsf/Section;->wasNull()Z

    move-result v0

    return v0
.end method
