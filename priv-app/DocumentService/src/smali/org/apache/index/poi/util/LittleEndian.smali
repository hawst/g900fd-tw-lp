.class public Lorg/apache/index/poi/util/LittleEndian;
.super Ljava/lang/Object;
.source "LittleEndian.java"

# interfaces
.implements Lorg/apache/index/poi/util/LittleEndianConsts;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/index/poi/util/LittleEndian$BufferUnderrunException;
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    return-void
.end method

.method public static getByteArray([BII)[B
    .locals 2
    .param p0, "data"    # [B
    .param p1, "offset"    # I
    .param p2, "size"    # I

    .prologue
    .line 397
    new-array v0, p2, [B

    .line 398
    .local v0, "copy":[B
    const/4 v1, 0x0

    invoke-static {p0, p1, v0, v1, p2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 400
    return-object v0
.end method

.method public static getDouble([BI)D
    .locals 2
    .param p0, "data"    # [B
    .param p1, "offset"    # I

    .prologue
    .line 161
    invoke-static {p0, p1}, Lorg/apache/index/poi/util/LittleEndian;->getLong([BI)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v0

    return-wide v0
.end method

.method public static getInt([B)I
    .locals 1
    .param p0, "data"    # [B

    .prologue
    .line 108
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lorg/apache/index/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    return v0
.end method

.method public static getInt([BI)I
    .locals 8
    .param p0, "data"    # [B
    .param p1, "offset"    # I

    .prologue
    .line 92
    move v4, p1

    .line 93
    .local v4, "i":I
    add-int/lit8 v5, v4, 0x1

    .end local v4    # "i":I
    .local v5, "i":I
    aget-byte v6, p0, v4

    and-int/lit16 v0, v6, 0xff

    .line 94
    .local v0, "b0":I
    add-int/lit8 v4, v5, 0x1

    .end local v5    # "i":I
    .restart local v4    # "i":I
    aget-byte v6, p0, v5

    and-int/lit16 v1, v6, 0xff

    .line 95
    .local v1, "b1":I
    add-int/lit8 v5, v4, 0x1

    .end local v4    # "i":I
    .restart local v5    # "i":I
    aget-byte v6, p0, v4

    and-int/lit16 v2, v6, 0xff

    .line 96
    .local v2, "b2":I
    add-int/lit8 v4, v5, 0x1

    .end local v5    # "i":I
    .restart local v4    # "i":I
    aget-byte v6, p0, v5

    and-int/lit16 v3, v6, 0xff

    .line 97
    .local v3, "b3":I
    shl-int/lit8 v6, v3, 0x18

    shl-int/lit8 v7, v2, 0x10

    add-int/2addr v6, v7

    shl-int/lit8 v7, v1, 0x8

    add-int/2addr v6, v7

    shl-int/lit8 v7, v0, 0x0

    add-int/2addr v6, v7

    return v6
.end method

.method public static getLong([BI)J
    .locals 6
    .param p0, "data"    # [B
    .param p1, "offset"    # I

    .prologue
    .line 142
    const-wide/16 v2, 0x0

    .line 144
    .local v2, "result":J
    add-int/lit8 v1, p1, 0x8

    add-int/lit8 v0, v1, -0x1

    .local v0, "j":I
    :goto_0
    if-ge v0, p1, :cond_0

    .line 148
    return-wide v2

    .line 145
    :cond_0
    const/16 v1, 0x8

    shl-long/2addr v2, v1

    .line 146
    aget-byte v1, p0, v0

    and-int/lit16 v1, v1, 0xff

    int-to-long v4, v1

    or-long/2addr v2, v4

    .line 144
    add-int/lit8 v0, v0, -0x1

    goto :goto_0
.end method

.method public static getShort([B)S
    .locals 1
    .param p0, "data"    # [B

    .prologue
    .line 71
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lorg/apache/index/poi/util/LittleEndian;->getShort([BI)S

    move-result v0

    return v0
.end method

.method public static getShort([BI)S
    .locals 4
    .param p0, "data"    # [B
    .param p1, "offset"    # I

    .prologue
    .line 45
    aget-byte v2, p0, p1

    and-int/lit16 v0, v2, 0xff

    .line 46
    .local v0, "b0":I
    add-int/lit8 v2, p1, 0x1

    aget-byte v2, p0, v2

    and-int/lit16 v1, v2, 0xff

    .line 47
    .local v1, "b1":I
    shl-int/lit8 v2, v1, 0x8

    shl-int/lit8 v3, v0, 0x0

    add-int/2addr v2, v3

    int-to-short v2, v2

    return v2
.end method

.method public static getUInt([B)J
    .locals 2
    .param p0, "data"    # [B

    .prologue
    .line 131
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lorg/apache/index/poi/util/LittleEndian;->getUInt([BI)J

    move-result-wide v0

    return-wide v0
.end method

.method public static getUInt([BI)J
    .locals 4
    .param p0, "data"    # [B
    .param p1, "offset"    # I

    .prologue
    .line 120
    invoke-static {p0, p1}, Lorg/apache/index/poi/util/LittleEndian;->getInt([BI)I

    move-result v2

    int-to-long v0, v2

    .line 121
    .local v0, "retNum":J
    const-wide/16 v2, -0x1

    and-long/2addr v2, v0

    return-wide v2
.end method

.method public static getUShort([B)I
    .locals 1
    .param p0, "data"    # [B

    .prologue
    .line 81
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lorg/apache/index/poi/util/LittleEndian;->getUShort([BI)I

    move-result v0

    return v0
.end method

.method public static getUShort([BI)I
    .locals 4
    .param p0, "data"    # [B
    .param p1, "offset"    # I

    .prologue
    .line 59
    aget-byte v2, p0, p1

    and-int/lit16 v0, v2, 0xff

    .line 60
    .local v0, "b0":I
    add-int/lit8 v2, p1, 0x1

    aget-byte v2, p0, v2

    and-int/lit16 v1, v2, 0xff

    .line 61
    .local v1, "b1":I
    shl-int/lit8 v2, v1, 0x8

    shl-int/lit8 v3, v0, 0x0

    add-int/2addr v2, v3

    return v2
.end method

.method public static getUnsignedByte([BI)I
    .locals 1
    .param p0, "data"    # [B
    .param p1, "offset"    # I

    .prologue
    .line 382
    aget-byte v0, p0, p1

    and-int/lit16 v0, v0, 0xff

    return v0
.end method

.method public static putByte([BII)V
    .locals 1
    .param p0, "data"    # [B
    .param p1, "offset"    # I
    .param p2, "value"    # I

    .prologue
    .line 185
    int-to-byte v0, p2

    aput-byte v0, p0, p1

    .line 186
    return-void
.end method

.method public static putDouble([BID)V
    .locals 2
    .param p0, "data"    # [B
    .param p1, "offset"    # I
    .param p2, "value"    # D

    .prologue
    .line 267
    invoke-static {p2, p3}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v0

    invoke-static {p0, p1, v0, v1}, Lorg/apache/index/poi/util/LittleEndian;->putLong([BIJ)V

    .line 268
    return-void
.end method

.method public static putInt([BI)V
    .locals 1
    .param p0, "data"    # [B
    .param p1, "value"    # I

    .prologue
    .line 237
    const/4 v0, 0x0

    invoke-static {p0, v0, p1}, Lorg/apache/index/poi/util/LittleEndian;->putInt([BII)V

    .line 238
    return-void
.end method

.method public static putInt([BII)V
    .locals 3
    .param p0, "data"    # [B
    .param p1, "offset"    # I
    .param p2, "value"    # I

    .prologue
    .line 222
    move v0, p1

    .line 223
    .local v0, "i":I
    add-int/lit8 v1, v0, 0x1

    .end local v0    # "i":I
    .local v1, "i":I
    ushr-int/lit8 v2, p2, 0x0

    and-int/lit16 v2, v2, 0xff

    int-to-byte v2, v2

    aput-byte v2, p0, v0

    .line 224
    add-int/lit8 v0, v1, 0x1

    .end local v1    # "i":I
    .restart local v0    # "i":I
    ushr-int/lit8 v2, p2, 0x8

    and-int/lit16 v2, v2, 0xff

    int-to-byte v2, v2

    aput-byte v2, p0, v1

    .line 225
    add-int/lit8 v1, v0, 0x1

    .end local v0    # "i":I
    .restart local v1    # "i":I
    ushr-int/lit8 v2, p2, 0x10

    and-int/lit16 v2, v2, 0xff

    int-to-byte v2, v2

    aput-byte v2, p0, v0

    .line 226
    add-int/lit8 v0, v1, 0x1

    .end local v1    # "i":I
    .restart local v0    # "i":I
    ushr-int/lit8 v2, p2, 0x18

    and-int/lit16 v2, v2, 0xff

    int-to-byte v2, v2

    aput-byte v2, p0, v1

    .line 227
    return-void
.end method

.method public static putLong([BIJ)V
    .locals 6
    .param p0, "data"    # [B
    .param p1, "offset"    # I
    .param p2, "value"    # J

    .prologue
    .line 249
    add-int/lit8 v1, p1, 0x8

    .line 250
    .local v1, "limit":I
    move-wide v2, p2

    .line 252
    .local v2, "v":J
    move v0, p1

    .local v0, "j":I
    :goto_0
    if-lt v0, v1, :cond_0

    .line 256
    return-void

    .line 253
    :cond_0
    const-wide/16 v4, 0xff

    and-long/2addr v4, v2

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, p0, v0

    .line 254
    const/16 v4, 0x8

    shr-long/2addr v2, v4

    .line 252
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public static putShort([BIS)V
    .locals 3
    .param p0, "data"    # [B
    .param p1, "offset"    # I
    .param p2, "value"    # S

    .prologue
    .line 172
    move v0, p1

    .line 173
    .local v0, "i":I
    add-int/lit8 v1, v0, 0x1

    .end local v0    # "i":I
    .local v1, "i":I
    ushr-int/lit8 v2, p2, 0x0

    and-int/lit16 v2, v2, 0xff

    int-to-byte v2, v2

    aput-byte v2, p0, v0

    .line 174
    add-int/lit8 v0, v1, 0x1

    .end local v1    # "i":I
    .restart local v0    # "i":I
    ushr-int/lit8 v2, p2, 0x8

    and-int/lit16 v2, v2, 0xff

    int-to-byte v2, v2

    aput-byte v2, p0, v1

    .line 175
    return-void
.end method

.method public static putShort([BS)V
    .locals 1
    .param p0, "data"    # [B
    .param p1, "value"    # S

    .prologue
    .line 210
    const/4 v0, 0x0

    invoke-static {p0, v0, p1}, Lorg/apache/index/poi/util/LittleEndian;->putShort([BIS)V

    .line 211
    return-void
.end method

.method public static putUShort([BII)V
    .locals 3
    .param p0, "data"    # [B
    .param p1, "offset"    # I
    .param p2, "value"    # I

    .prologue
    .line 198
    move v0, p1

    .line 199
    .local v0, "i":I
    add-int/lit8 v1, v0, 0x1

    .end local v0    # "i":I
    .local v1, "i":I
    ushr-int/lit8 v2, p2, 0x0

    and-int/lit16 v2, v2, 0xff

    int-to-byte v2, v2

    aput-byte v2, p0, v0

    .line 200
    add-int/lit8 v0, v1, 0x1

    .end local v1    # "i":I
    .restart local v0    # "i":I
    ushr-int/lit8 v2, p2, 0x8

    and-int/lit16 v2, v2, 0xff

    int-to-byte v2, v2

    aput-byte v2, p0, v1

    .line 201
    return-void
.end method

.method public static readInt(Ljava/io/InputStream;)I
    .locals 6
    .param p0, "stream"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/apache/index/poi/util/LittleEndian$BufferUnderrunException;
        }
    .end annotation

    .prologue
    .line 318
    invoke-virtual {p0}, Ljava/io/InputStream;->read()I

    move-result v0

    .line 319
    .local v0, "ch1":I
    invoke-virtual {p0}, Ljava/io/InputStream;->read()I

    move-result v1

    .line 320
    .local v1, "ch2":I
    invoke-virtual {p0}, Ljava/io/InputStream;->read()I

    move-result v2

    .line 321
    .local v2, "ch3":I
    invoke-virtual {p0}, Ljava/io/InputStream;->read()I

    move-result v3

    .line 322
    .local v3, "ch4":I
    or-int v4, v0, v1

    or-int/2addr v4, v2

    or-int/2addr v4, v3

    if-gez v4, :cond_0

    .line 323
    new-instance v4, Lorg/apache/index/poi/util/LittleEndian$BufferUnderrunException;

    invoke-direct {v4}, Lorg/apache/index/poi/util/LittleEndian$BufferUnderrunException;-><init>()V

    throw v4

    .line 325
    :cond_0
    shl-int/lit8 v4, v3, 0x18

    shl-int/lit8 v5, v2, 0x10

    add-int/2addr v4, v5

    shl-int/lit8 v5, v1, 0x8

    add-int/2addr v4, v5

    shl-int/lit8 v5, v0, 0x0

    add-int/2addr v4, v5

    return v4
.end method

.method public static readLong(Ljava/io/InputStream;)J
    .locals 13
    .param p0, "stream"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/apache/index/poi/util/LittleEndian$BufferUnderrunException;
        }
    .end annotation

    .prologue
    .line 339
    invoke-virtual {p0}, Ljava/io/InputStream;->read()I

    move-result v0

    .line 340
    .local v0, "ch1":I
    invoke-virtual {p0}, Ljava/io/InputStream;->read()I

    move-result v1

    .line 341
    .local v1, "ch2":I
    invoke-virtual {p0}, Ljava/io/InputStream;->read()I

    move-result v2

    .line 342
    .local v2, "ch3":I
    invoke-virtual {p0}, Ljava/io/InputStream;->read()I

    move-result v3

    .line 343
    .local v3, "ch4":I
    invoke-virtual {p0}, Ljava/io/InputStream;->read()I

    move-result v4

    .line 344
    .local v4, "ch5":I
    invoke-virtual {p0}, Ljava/io/InputStream;->read()I

    move-result v5

    .line 345
    .local v5, "ch6":I
    invoke-virtual {p0}, Ljava/io/InputStream;->read()I

    move-result v6

    .line 346
    .local v6, "ch7":I
    invoke-virtual {p0}, Ljava/io/InputStream;->read()I

    move-result v7

    .line 347
    .local v7, "ch8":I
    or-int v8, v0, v1

    or-int/2addr v8, v2

    or-int/2addr v8, v3

    or-int/2addr v8, v4

    or-int/2addr v8, v5

    or-int/2addr v8, v6

    or-int/2addr v8, v7

    if-gez v8, :cond_0

    .line 348
    new-instance v8, Lorg/apache/index/poi/util/LittleEndian$BufferUnderrunException;

    invoke-direct {v8}, Lorg/apache/index/poi/util/LittleEndian$BufferUnderrunException;-><init>()V

    throw v8

    .line 352
    :cond_0
    int-to-long v8, v7

    const/16 v10, 0x38

    shl-long/2addr v8, v10

    .line 353
    int-to-long v10, v6

    const/16 v12, 0x30

    shl-long/2addr v10, v12

    .line 352
    add-long/2addr v8, v10

    .line 354
    int-to-long v10, v5

    const/16 v12, 0x28

    shl-long/2addr v10, v12

    .line 352
    add-long/2addr v8, v10

    .line 355
    int-to-long v10, v4

    const/16 v12, 0x20

    shl-long/2addr v10, v12

    .line 352
    add-long/2addr v8, v10

    .line 356
    int-to-long v10, v3

    const/16 v12, 0x18

    shl-long/2addr v10, v12

    .line 352
    add-long/2addr v8, v10

    .line 357
    shl-int/lit8 v10, v2, 0x10

    int-to-long v10, v10

    .line 352
    add-long/2addr v8, v10

    .line 358
    shl-int/lit8 v10, v1, 0x8

    int-to-long v10, v10

    .line 352
    add-long/2addr v8, v10

    .line 359
    shl-int/lit8 v10, v0, 0x0

    int-to-long v10, v10

    .line 352
    add-long/2addr v8, v10

    .line 351
    return-wide v8
.end method

.method public static readShort(Ljava/io/InputStream;)S
    .locals 1
    .param p0, "stream"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/apache/index/poi/util/LittleEndian$BufferUnderrunException;
        }
    .end annotation

    .prologue
    .line 294
    invoke-static {p0}, Lorg/apache/index/poi/util/LittleEndian;->readUShort(Ljava/io/InputStream;)I

    move-result v0

    int-to-short v0, v0

    return v0
.end method

.method public static readUShort(Ljava/io/InputStream;)I
    .locals 4
    .param p0, "stream"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/apache/index/poi/util/LittleEndian$BufferUnderrunException;
        }
    .end annotation

    .prologue
    .line 299
    invoke-virtual {p0}, Ljava/io/InputStream;->read()I

    move-result v0

    .line 300
    .local v0, "ch1":I
    invoke-virtual {p0}, Ljava/io/InputStream;->read()I

    move-result v1

    .line 301
    .local v1, "ch2":I
    or-int v2, v0, v1

    if-gez v2, :cond_0

    .line 302
    new-instance v2, Lorg/apache/index/poi/util/LittleEndian$BufferUnderrunException;

    invoke-direct {v2}, Lorg/apache/index/poi/util/LittleEndian$BufferUnderrunException;-><init>()V

    throw v2

    .line 304
    :cond_0
    shl-int/lit8 v2, v1, 0x8

    shl-int/lit8 v3, v0, 0x0

    add-int/2addr v2, v3

    return v2
.end method

.method public static ubyteToInt(B)I
    .locals 1
    .param p0, "b"    # B

    .prologue
    .line 370
    and-int/lit16 v0, p0, 0xff

    return v0
.end method
