.class public final Lorg/apache/index/poi/hslf/record/HeadersFootersAtom;
.super Lorg/apache/index/poi/hslf/record/RecordAtom;
.source "HeadersFootersAtom.java"


# static fields
.field public static final fHasDate:I = 0x1

.field public static final fHasFooter:I = 0x20

.field public static final fHasHeader:I = 0x10

.field public static final fHasSlideNumber:I = 0x8

.field public static final fHasTodayDate:I = 0x2

.field public static final fHasUserDate:I = 0x4


# instance fields
.field private _header:[B

.field private _recdata:[B


# direct methods
.method public constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, 0x4

    .line 107
    invoke-direct {p0}, Lorg/apache/index/poi/hslf/record/RecordAtom;-><init>()V

    .line 108
    new-array v0, v4, [B

    iput-object v0, p0, Lorg/apache/index/poi/hslf/record/HeadersFootersAtom;->_recdata:[B

    .line 110
    const/16 v0, 0x8

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/apache/index/poi/hslf/record/HeadersFootersAtom;->_header:[B

    .line 111
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/HeadersFootersAtom;->_header:[B

    const/4 v1, 0x2

    invoke-virtual {p0}, Lorg/apache/index/poi/hslf/record/HeadersFootersAtom;->getRecordType()J

    move-result-wide v2

    long-to-int v2, v2

    int-to-short v2, v2

    invoke-static {v0, v1, v2}, Lorg/apache/index/poi/util/LittleEndian;->putShort([BIS)V

    .line 112
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/HeadersFootersAtom;->_header:[B

    iget-object v1, p0, Lorg/apache/index/poi/hslf/record/HeadersFootersAtom;->_recdata:[B

    array-length v1, v1

    invoke-static {v0, v4, v1}, Lorg/apache/index/poi/util/LittleEndian;->putInt([BII)V

    .line 113
    return-void
.end method

.method protected constructor <init>([BII)V
    .locals 4
    .param p1, "source"    # [B
    .param p2, "start"    # I
    .param p3, "len"    # I

    .prologue
    const/16 v1, 0x8

    const/4 v3, 0x0

    .line 94
    invoke-direct {p0}, Lorg/apache/index/poi/hslf/record/RecordAtom;-><init>()V

    .line 96
    new-array v0, v1, [B

    iput-object v0, p0, Lorg/apache/index/poi/hslf/record/HeadersFootersAtom;->_header:[B

    .line 97
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/HeadersFootersAtom;->_header:[B

    invoke-static {p1, p2, v0, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 100
    add-int/lit8 v0, p3, -0x8

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/apache/index/poi/hslf/record/HeadersFootersAtom;->_recdata:[B

    .line 101
    add-int/lit8 v0, p2, 0x8

    iget-object v1, p0, Lorg/apache/index/poi/hslf/record/HeadersFootersAtom;->_recdata:[B

    add-int/lit8 v2, p3, -0x8

    invoke-static {p1, v0, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 102
    return-void
.end method


# virtual methods
.method public getFlag(I)Z
    .locals 1
    .param p1, "bit"    # I

    .prologue
    .line 185
    invoke-virtual {p0}, Lorg/apache/index/poi/hslf/record/HeadersFootersAtom;->getMask()I

    move-result v0

    and-int/2addr v0, p1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getFormatId()I
    .locals 2

    .prologue
    .line 138
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/HeadersFootersAtom;->_recdata:[B

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lorg/apache/index/poi/util/LittleEndian;->getShort([BI)S

    move-result v0

    return v0
.end method

.method public getMask()I
    .locals 2

    .prologue
    .line 168
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/HeadersFootersAtom;->_recdata:[B

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lorg/apache/index/poi/util/LittleEndian;->getShort([BI)S

    move-result v0

    return v0
.end method

.method public getRecordType()J
    .locals 2

    .prologue
    .line 116
    sget-object v0, Lorg/apache/index/poi/hslf/record/RecordTypes;->HeadersFootersAtom:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    iget v0, v0, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;->typeID:I

    int-to-long v0, v0

    return-wide v0
.end method

.method public setFlag(IZ)V
    .locals 2
    .param p1, "bit"    # I
    .param p2, "value"    # Z

    .prologue
    .line 193
    invoke-virtual {p0}, Lorg/apache/index/poi/hslf/record/HeadersFootersAtom;->getMask()I

    move-result v0

    .line 194
    .local v0, "mask":I
    if-eqz p2, :cond_0

    or-int/2addr v0, p1

    .line 196
    :goto_0
    invoke-virtual {p0, v0}, Lorg/apache/index/poi/hslf/record/HeadersFootersAtom;->setMask(I)V

    .line 197
    return-void

    .line 195
    :cond_0
    xor-int/lit8 v1, p1, -0x1

    and-int/2addr v0, v1

    goto :goto_0
.end method

.method public setFormatId(I)V
    .locals 2
    .param p1, "formatId"    # I

    .prologue
    .line 147
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/HeadersFootersAtom;->_recdata:[B

    const/4 v1, 0x0

    invoke-static {v0, v1, p1}, Lorg/apache/index/poi/util/LittleEndian;->putUShort([BII)V

    .line 148
    return-void
.end method

.method public setMask(I)V
    .locals 2
    .param p1, "mask"    # I

    .prologue
    .line 177
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/HeadersFootersAtom;->_recdata:[B

    const/4 v1, 0x2

    invoke-static {v0, v1, p1}, Lorg/apache/index/poi/util/LittleEndian;->putUShort([BII)V

    .line 178
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 200
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 201
    .local v0, "buf":Ljava/lang/StringBuffer;
    const-string/jumbo v1, "HeadersFootersAtom\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 202
    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "\tFormatId: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lorg/apache/index/poi/hslf/record/HeadersFootersAtom;->getFormatId()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 203
    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "\tMask    : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lorg/apache/index/poi/hslf/record/HeadersFootersAtom;->getMask()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 204
    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "\t  fHasDate        : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lorg/apache/index/poi/hslf/record/HeadersFootersAtom;->getFlag(I)Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 205
    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "\t  fHasTodayDate   : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v2, 0x2

    invoke-virtual {p0, v2}, Lorg/apache/index/poi/hslf/record/HeadersFootersAtom;->getFlag(I)Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 206
    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "\t  fHasUserDate    : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v2, 0x4

    invoke-virtual {p0, v2}, Lorg/apache/index/poi/hslf/record/HeadersFootersAtom;->getFlag(I)Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 207
    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "\t  fHasSlideNumber : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/16 v2, 0x8

    invoke-virtual {p0, v2}, Lorg/apache/index/poi/hslf/record/HeadersFootersAtom;->getFlag(I)Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 208
    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "\t  fHasHeader      : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/16 v2, 0x10

    invoke-virtual {p0, v2}, Lorg/apache/index/poi/hslf/record/HeadersFootersAtom;->getFlag(I)Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 209
    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "\t  fHasFooter      : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/16 v2, 0x20

    invoke-virtual {p0, v2}, Lorg/apache/index/poi/hslf/record/HeadersFootersAtom;->getFlag(I)Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 210
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public writeOut(Ljava/io/OutputStream;)V
    .locals 1
    .param p1, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 123
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/HeadersFootersAtom;->_header:[B

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 124
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/HeadersFootersAtom;->_recdata:[B

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 125
    return-void
.end method
