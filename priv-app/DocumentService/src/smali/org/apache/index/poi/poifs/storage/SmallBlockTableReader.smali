.class public final Lorg/apache/index/poi/poifs/storage/SmallBlockTableReader;
.super Ljava/lang/Object;
.source "SmallBlockTableReader.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getSmallDocumentBlocks(Lorg/apache/index/poi/poifs/common/POIFSBigBlockSize;Lorg/apache/index/poi/poifs/storage/RawDataBlockList;Lorg/apache/index/poi/poifs/property/RootProperty;I)Lorg/apache/index/poi/poifs/storage/BlockList;
    .locals 4
    .param p0, "bigBlockSize"    # Lorg/apache/index/poi/poifs/common/POIFSBigBlockSize;
    .param p1, "blockList"    # Lorg/apache/index/poi/poifs/storage/RawDataBlockList;
    .param p2, "root"    # Lorg/apache/index/poi/poifs/property/RootProperty;
    .param p3, "sbatStart"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, -0x1

    .line 54
    invoke-virtual {p2}, Lorg/apache/index/poi/poifs/property/RootProperty;->getStartBlock()I

    move-result v2

    invoke-virtual {p1, v2, v3}, Lorg/apache/index/poi/poifs/storage/RawDataBlockList;->fetchBlocks(II)[Lorg/apache/index/poi/poifs/storage/ListManagedBlock;

    move-result-object v1

    .line 57
    .local v1, "smallBlockBlocks":[Lorg/apache/index/poi/poifs/storage/ListManagedBlock;
    new-instance v0, Lorg/apache/index/poi/poifs/storage/SmallDocumentBlockList;

    .line 58
    invoke-static {p0, v1}, Lorg/apache/index/poi/poifs/storage/SmallDocumentBlock;->extract(Lorg/apache/index/poi/poifs/common/POIFSBigBlockSize;[Lorg/apache/index/poi/poifs/storage/ListManagedBlock;)Ljava/util/List;

    move-result-object v2

    .line 57
    invoke-direct {v0, v2}, Lorg/apache/index/poi/poifs/storage/SmallDocumentBlockList;-><init>(Ljava/util/List;)V

    .line 61
    .local v0, "list":Lorg/apache/index/poi/poifs/storage/BlockList;
    new-instance v2, Lorg/apache/index/poi/poifs/storage/BlockAllocationTableReader;

    .line 62
    invoke-virtual {p1, p3, v3}, Lorg/apache/index/poi/poifs/storage/RawDataBlockList;->fetchBlocks(II)[Lorg/apache/index/poi/poifs/storage/ListManagedBlock;

    move-result-object v3

    .line 61
    invoke-direct {v2, p0, v3, v0}, Lorg/apache/index/poi/poifs/storage/BlockAllocationTableReader;-><init>(Lorg/apache/index/poi/poifs/common/POIFSBigBlockSize;[Lorg/apache/index/poi/poifs/storage/ListManagedBlock;Lorg/apache/index/poi/poifs/storage/BlockList;)V

    .line 64
    return-object v0
.end method
