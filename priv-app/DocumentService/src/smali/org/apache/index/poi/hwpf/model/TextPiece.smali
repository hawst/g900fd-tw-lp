.class public final Lorg/apache/index/poi/hwpf/model/TextPiece;
.super Lorg/apache/index/poi/hwpf/model/PropertyNode;
.source "TextPiece.java"

# interfaces
.implements Ljava/lang/Comparable;


# instance fields
.field private _pd:Lorg/apache/index/poi/hwpf/model/PieceDescriptor;

.field private _usesUnicode:Z


# direct methods
.method public constructor <init>(II[BLorg/apache/index/poi/hwpf/model/PieceDescriptor;I)V
    .locals 4
    .param p1, "start"    # I
    .param p2, "end"    # I
    .param p3, "text"    # [B
    .param p4, "pd"    # Lorg/apache/index/poi/hwpf/model/PieceDescriptor;
    .param p5, "cpStart"    # I

    .prologue
    .line 46
    invoke-static {p3, p4}, Lorg/apache/index/poi/hwpf/model/TextPiece;->buildInitSB([BLorg/apache/index/poi/hwpf/model/PieceDescriptor;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-direct {p0, p1, p2, v1}, Lorg/apache/index/poi/hwpf/model/PropertyNode;-><init>(IILjava/lang/Object;)V

    .line 47
    invoke-virtual {p4}, Lorg/apache/index/poi/hwpf/model/PieceDescriptor;->isUnicode()Z

    move-result v1

    iput-boolean v1, p0, Lorg/apache/index/poi/hwpf/model/TextPiece;->_usesUnicode:Z

    .line 48
    iput-object p4, p0, Lorg/apache/index/poi/hwpf/model/TextPiece;->_pd:Lorg/apache/index/poi/hwpf/model/PieceDescriptor;

    .line 51
    iget-object v1, p0, Lorg/apache/index/poi/hwpf/model/TextPiece;->_buf:Ljava/lang/Object;

    check-cast v1, Ljava/lang/StringBuffer;

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->length()I

    move-result v0

    .line 52
    .local v0, "textLength":I
    sub-int v1, p2, p1

    if-eq v1, v0, :cond_0

    .line 53
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Told we\'re for characters "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " -> "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ", but actually covers "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " characters!"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 55
    :cond_0
    if-ge p2, p1, :cond_1

    .line 56
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Told we\'re of negative size! start="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " end="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 58
    :cond_1
    return-void
.end method

.method private static buildInitSB([BLorg/apache/index/poi/hwpf/model/PieceDescriptor;)Ljava/lang/StringBuffer;
    .locals 4
    .param p0, "text"    # [B
    .param p1, "pd"    # Lorg/apache/index/poi/hwpf/model/PieceDescriptor;

    .prologue
    .line 66
    const/4 v1, 0x0

    .line 68
    .local v1, "str":Ljava/lang/String;
    :try_start_0
    invoke-virtual {p1}, Lorg/apache/index/poi/hwpf/model/PieceDescriptor;->isUnicode()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 69
    new-instance v1, Ljava/lang/String;

    .end local v1    # "str":Ljava/lang/String;
    const-string/jumbo v2, "UTF-16LE"

    invoke-direct {v1, p0, v2}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 77
    .restart local v1    # "str":Ljava/lang/String;
    :goto_0
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2, v1}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    return-object v2

    .line 71
    :cond_0
    :try_start_1
    new-instance v1, Ljava/lang/String;

    .end local v1    # "str":Ljava/lang/String;
    const-string/jumbo v2, "Cp1252"

    invoke-direct {v1, p0, v2}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_1
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_0

    .restart local v1    # "str":Ljava/lang/String;
    goto :goto_0

    .line 73
    .end local v1    # "str":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 74
    .local v0, "e":Ljava/io/UnsupportedEncodingException;
    new-instance v2, Ljava/lang/RuntimeException;

    const-string/jumbo v3, "Your Java is broken! It doesn\'t know about basic, required character encodings!"

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2
.end method


# virtual methods
.method public adjustForDelete(II)V
    .locals 7
    .param p1, "start"    # I
    .param p2, "length"    # I

    .prologue
    .line 138
    move v3, p2

    .line 140
    .local v3, "numChars":I
    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/model/TextPiece;->getStart()I

    move-result v2

    .line 141
    .local v2, "myStart":I
    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/model/TextPiece;->getEnd()I

    move-result v1

    .line 142
    .local v1, "myEnd":I
    add-int v0, p1, v3

    .line 145
    .local v0, "end":I
    if-gt p1, v1, :cond_0

    if-lt v0, v2, :cond_0

    .line 148
    invoke-static {v2, p1}, Ljava/lang/Math;->max(II)I

    move-result v5

    .line 149
    .local v5, "overlapStart":I
    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v4

    .line 150
    .local v4, "overlapEnd":I
    iget-object v6, p0, Lorg/apache/index/poi/hwpf/model/TextPiece;->_buf:Ljava/lang/Object;

    check-cast v6, Ljava/lang/StringBuffer;

    invoke-virtual {v6, v5, v4}, Ljava/lang/StringBuffer;->delete(II)Ljava/lang/StringBuffer;

    .line 158
    .end local v4    # "overlapEnd":I
    .end local v5    # "overlapStart":I
    :cond_0
    invoke-super {p0, p1, p2}, Lorg/apache/index/poi/hwpf/model/PropertyNode;->adjustForDelete(II)V

    .line 159
    return-void
.end method

.method public bytesLength()I
    .locals 2

    .prologue
    .line 172
    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/model/TextPiece;->getEnd()I

    move-result v0

    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/model/TextPiece;->getStart()I

    move-result v1

    sub-int v1, v0, v1

    iget-boolean v0, p0, Lorg/apache/index/poi/hwpf/model/TextPiece;->_usesUnicode:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    :goto_0
    mul-int/2addr v0, v1

    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public characterLength()I
    .locals 2

    .prologue
    .line 166
    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/model/TextPiece;->getEnd()I

    move-result v0

    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/model/TextPiece;->getStart()I

    move-result v1

    sub-int/2addr v0, v1

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 177
    if-nez p1, :cond_1

    .line 191
    :cond_0
    :goto_0
    return v1

    .line 182
    :cond_1
    instance-of v2, p1, Lorg/apache/index/poi/hwpf/model/TextPiece;

    if-eqz v2, :cond_0

    .line 183
    invoke-virtual {p0, p1}, Lorg/apache/index/poi/hwpf/model/TextPiece;->limitsAreEqual(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 185
    check-cast v0, Lorg/apache/index/poi/hwpf/model/TextPiece;

    .line 186
    .local v0, "tp":Lorg/apache/index/poi/hwpf/model/TextPiece;
    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/model/TextPiece;->getStringBuffer()Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lorg/apache/index/poi/hwpf/model/TextPiece;->getStringBuffer()Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 187
    iget-boolean v2, v0, Lorg/apache/index/poi/hwpf/model/TextPiece;->_usesUnicode:Z

    iget-boolean v3, p0, Lorg/apache/index/poi/hwpf/model/TextPiece;->_usesUnicode:Z

    if-ne v2, v3, :cond_0

    iget-object v2, p0, Lorg/apache/index/poi/hwpf/model/TextPiece;->_pd:Lorg/apache/index/poi/hwpf/model/PieceDescriptor;

    iget-object v3, v0, Lorg/apache/index/poi/hwpf/model/TextPiece;->_pd:Lorg/apache/index/poi/hwpf/model/PieceDescriptor;

    invoke-virtual {v2, v3}, Lorg/apache/index/poi/hwpf/model/PieceDescriptor;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 186
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public getCP()I
    .locals 1

    .prologue
    .line 200
    invoke-virtual {p0}, Lorg/apache/index/poi/hwpf/model/TextPiece;->getStart()I

    move-result v0

    return v0
.end method

.method public getPieceDescriptor()Lorg/apache/index/poi/hwpf/model/PieceDescriptor;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/TextPiece;->_pd:Lorg/apache/index/poi/hwpf/model/PieceDescriptor;

    return-object v0
.end method

.method public getRawBytes()[B
    .locals 3

    .prologue
    .line 101
    :try_start_0
    iget-object v1, p0, Lorg/apache/index/poi/hwpf/model/TextPiece;->_buf:Ljava/lang/Object;

    check-cast v1, Ljava/lang/StringBuffer;

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    iget-boolean v1, p0, Lorg/apache/index/poi/hwpf/model/TextPiece;->_usesUnicode:Z

    if-eqz v1, :cond_0

    .line 102
    const-string/jumbo v1, "UTF-16LE"

    .line 101
    :goto_0
    invoke-virtual {v2, v1}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v1

    return-object v1

    .line 102
    :cond_0
    const-string/jumbo v1, "Cp1252"
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 103
    :catch_0
    move-exception v0

    .line 104
    .local v0, "ignore":Ljava/io/UnsupportedEncodingException;
    new-instance v1, Ljava/lang/RuntimeException;

    const-string/jumbo v2, "Your Java is broken! It doesn\'t know about basic, required character encodings!"

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public getStringBuffer()Ljava/lang/StringBuffer;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/TextPiece;->_buf:Ljava/lang/Object;

    check-cast v0, Ljava/lang/StringBuffer;

    return-object v0
.end method

.method public isUnicode()Z
    .locals 1

    .prologue
    .line 85
    iget-boolean v0, p0, Lorg/apache/index/poi/hwpf/model/TextPiece;->_usesUnicode:Z

    return v0
.end method

.method public substring(II)Ljava/lang/String;
    .locals 4
    .param p1, "start"    # I
    .param p2, "end"    # I

    .prologue
    .line 116
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/TextPiece;->_buf:Ljava/lang/Object;

    check-cast v0, Ljava/lang/StringBuffer;

    .line 119
    .local v0, "buf":Ljava/lang/StringBuffer;
    if-gez p1, :cond_0

    .line 120
    new-instance v1, Ljava/lang/StringIndexOutOfBoundsException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Can\'t request a substring before 0 - asked for "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 122
    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->length()I

    move-result v1

    if-le p2, v1, :cond_1

    .line 123
    new-instance v1, Ljava/lang/StringIndexOutOfBoundsException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Index "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " out of range 0 -> "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->length()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 125
    :cond_1
    if-ge p2, p1, :cond_2

    .line 126
    new-instance v1, Ljava/lang/StringIndexOutOfBoundsException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Asked for text from "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " to "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ", which has an end before the start!"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 128
    :cond_2
    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuffer;->substring(II)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
