.class public final Lorg/apache/index/poi/hwpf/model/SectionDescriptor;
.super Ljava/lang/Object;
.source "SectionDescriptor.java"


# instance fields
.field private fc:I

.field private fcMpr:I

.field private fn:S

.field private fnMpr:S


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    return-void
.end method

.method public constructor <init>([BI)V
    .locals 1
    .param p1, "buf"    # [B
    .param p2, "offset"    # I

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    invoke-static {p1, p2}, Lorg/apache/index/poi/util/LittleEndian;->getShort([BI)S

    move-result v0

    iput-short v0, p0, Lorg/apache/index/poi/hwpf/model/SectionDescriptor;->fn:S

    .line 38
    add-int/lit8 p2, p2, 0x2

    .line 39
    invoke-static {p1, p2}, Lorg/apache/index/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    iput v0, p0, Lorg/apache/index/poi/hwpf/model/SectionDescriptor;->fc:I

    .line 40
    add-int/lit8 p2, p2, 0x4

    .line 41
    invoke-static {p1, p2}, Lorg/apache/index/poi/util/LittleEndian;->getShort([BI)S

    move-result v0

    iput-short v0, p0, Lorg/apache/index/poi/hwpf/model/SectionDescriptor;->fnMpr:S

    .line 42
    add-int/lit8 p2, p2, 0x2

    .line 43
    invoke-static {p1, p2}, Lorg/apache/index/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    iput v0, p0, Lorg/apache/index/poi/hwpf/model/SectionDescriptor;->fcMpr:I

    .line 44
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 58
    if-nez p1, :cond_1

    .line 67
    :cond_0
    :goto_0
    return v1

    .line 63
    :cond_1
    instance-of v2, p1, Lorg/apache/index/poi/hwpf/model/SectionDescriptor;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 64
    check-cast v0, Lorg/apache/index/poi/hwpf/model/SectionDescriptor;

    .line 65
    .local v0, "sed":Lorg/apache/index/poi/hwpf/model/SectionDescriptor;
    iget-short v2, v0, Lorg/apache/index/poi/hwpf/model/SectionDescriptor;->fn:S

    iget-short v3, p0, Lorg/apache/index/poi/hwpf/model/SectionDescriptor;->fn:S

    if-ne v2, v3, :cond_0

    iget-short v2, v0, Lorg/apache/index/poi/hwpf/model/SectionDescriptor;->fnMpr:S

    iget-short v3, p0, Lorg/apache/index/poi/hwpf/model/SectionDescriptor;->fnMpr:S

    if-ne v2, v3, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method public getFc()I
    .locals 1

    .prologue
    .line 48
    iget v0, p0, Lorg/apache/index/poi/hwpf/model/SectionDescriptor;->fc:I

    return v0
.end method

.method public setFc(I)V
    .locals 0
    .param p1, "fc"    # I

    .prologue
    .line 53
    iput p1, p0, Lorg/apache/index/poi/hwpf/model/SectionDescriptor;->fc:I

    .line 54
    return-void
.end method

.method public toByteArray()[B
    .locals 3

    .prologue
    .line 73
    const/4 v1, 0x0

    .line 74
    .local v1, "offset":I
    const/16 v2, 0xc

    new-array v0, v2, [B

    .line 76
    .local v0, "buf":[B
    iget-short v2, p0, Lorg/apache/index/poi/hwpf/model/SectionDescriptor;->fn:S

    invoke-static {v0, v1, v2}, Lorg/apache/index/poi/util/LittleEndian;->putShort([BIS)V

    .line 77
    add-int/lit8 v1, v1, 0x2

    .line 78
    iget v2, p0, Lorg/apache/index/poi/hwpf/model/SectionDescriptor;->fc:I

    invoke-static {v0, v1, v2}, Lorg/apache/index/poi/util/LittleEndian;->putInt([BII)V

    .line 79
    add-int/lit8 v1, v1, 0x4

    .line 80
    iget-short v2, p0, Lorg/apache/index/poi/hwpf/model/SectionDescriptor;->fnMpr:S

    invoke-static {v0, v1, v2}, Lorg/apache/index/poi/util/LittleEndian;->putShort([BIS)V

    .line 81
    add-int/lit8 v1, v1, 0x2

    .line 82
    iget v2, p0, Lorg/apache/index/poi/hwpf/model/SectionDescriptor;->fcMpr:I

    invoke-static {v0, v1, v2}, Lorg/apache/index/poi/util/LittleEndian;->putInt([BII)V

    .line 84
    return-object v0
.end method
