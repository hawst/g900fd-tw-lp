.class public abstract Lorg/apache/index/poi/ddf/EscherRecord;
.super Ljava/lang/Object;
.source "EscherRecord.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/index/poi/ddf/EscherRecord$EscherRecordHeader;
    }
.end annotation


# instance fields
.field private _options:S

.field private _recordId:S


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    return-void
.end method


# virtual methods
.method public clone()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 197
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "The class "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " needs to define a clone method"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public display(Ljava/io/PrintWriter;I)V
    .locals 2
    .param p1, "w"    # Ljava/io/PrintWriter;
    .param p2, "indent"    # I

    .prologue
    .line 216
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    mul-int/lit8 v1, p2, 0x4

    if-lt v0, v1, :cond_0

    .line 217
    invoke-virtual {p0}, Lorg/apache/index/poi/ddf/EscherRecord;->getRecordName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 218
    return-void

    .line 216
    :cond_0
    const/16 v1, 0x20

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(C)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public abstract fillFields([BILorg/apache/index/poi/ddf/EscherRecordFactory;)I
.end method

.method protected fillFields([BLorg/apache/index/poi/ddf/EscherRecordFactory;)I
    .locals 1
    .param p1, "data"    # [B
    .param p2, "f"    # Lorg/apache/index/poi/ddf/EscherRecordFactory;

    .prologue
    .line 51
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, p2}, Lorg/apache/index/poi/ddf/EscherRecord;->fillFields([BILorg/apache/index/poi/ddf/EscherRecordFactory;)I

    move-result v0

    return v0
.end method

.method public getChild(I)Lorg/apache/index/poi/ddf/EscherRecord;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 204
    invoke-virtual {p0}, Lorg/apache/index/poi/ddf/EscherRecord;->getChildRecords()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/index/poi/ddf/EscherRecord;

    return-object v0
.end method

.method public getChildRecords()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lorg/apache/index/poi/ddf/EscherRecord;",
            ">;"
        }
    .end annotation

    .prologue
    .line 180
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getInstance()S
    .locals 1

    .prologue
    .line 231
    iget-short v0, p0, Lorg/apache/index/poi/ddf/EscherRecord;->_options:S

    shr-int/lit8 v0, v0, 0x4

    int-to-short v0, v0

    return v0
.end method

.method public getOptions()S
    .locals 1

    .prologue
    .line 95
    iget-short v0, p0, Lorg/apache/index/poi/ddf/EscherRecord;->_options:S

    return v0
.end method

.method public getRecordId()S
    .locals 1

    .prologue
    .line 163
    iget-short v0, p0, Lorg/apache/index/poi/ddf/EscherRecord;->_recordId:S

    return v0
.end method

.method public abstract getRecordName()Ljava/lang/String;
.end method

.method public abstract getRecordSize()I
.end method

.method public isContainerRecord()Z
    .locals 2

    .prologue
    .line 88
    iget-short v0, p0, Lorg/apache/index/poi/ddf/EscherRecord;->_options:S

    and-int/lit8 v0, v0, 0xf

    const/16 v1, 0xf

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected readHeader([BI)I
    .locals 2
    .param p1, "data"    # [B
    .param p2, "offset"    # I

    .prologue
    .line 76
    invoke-static {p1, p2}, Lorg/apache/index/poi/ddf/EscherRecord$EscherRecordHeader;->readHeader([BI)Lorg/apache/index/poi/ddf/EscherRecord$EscherRecordHeader;

    move-result-object v0

    .line 77
    .local v0, "header":Lorg/apache/index/poi/ddf/EscherRecord$EscherRecordHeader;
    invoke-virtual {v0}, Lorg/apache/index/poi/ddf/EscherRecord$EscherRecordHeader;->getOptions()S

    move-result v1

    iput-short v1, p0, Lorg/apache/index/poi/ddf/EscherRecord;->_options:S

    .line 78
    invoke-virtual {v0}, Lorg/apache/index/poi/ddf/EscherRecord$EscherRecordHeader;->getRecordId()S

    move-result v1

    iput-short v1, p0, Lorg/apache/index/poi/ddf/EscherRecord;->_recordId:S

    .line 79
    invoke-virtual {v0}, Lorg/apache/index/poi/ddf/EscherRecord$EscherRecordHeader;->getRemainingBytes()I

    move-result v1

    return v1
.end method

.method public serialize(I[B)I
    .locals 1
    .param p1, "offset"    # I
    .param p2, "data"    # [B

    .prologue
    .line 133
    new-instance v0, Lorg/apache/index/poi/ddf/NullEscherSerializationListener;

    invoke-direct {v0}, Lorg/apache/index/poi/ddf/NullEscherSerializationListener;-><init>()V

    invoke-virtual {p0, p1, p2, v0}, Lorg/apache/index/poi/ddf/EscherRecord;->serialize(I[BLorg/apache/index/poi/ddf/EscherSerializationListener;)I

    move-result v0

    return v0
.end method

.method public abstract serialize(I[BLorg/apache/index/poi/ddf/EscherSerializationListener;)I
.end method

.method public serialize()[B
    .locals 2

    .prologue
    .line 115
    invoke-virtual {p0}, Lorg/apache/index/poi/ddf/EscherRecord;->getRecordSize()I

    move-result v1

    new-array v0, v1, [B

    .line 117
    .local v0, "retval":[B
    const/4 v1, 0x0

    invoke-virtual {p0, v1, v0}, Lorg/apache/index/poi/ddf/EscherRecord;->serialize(I[B)I

    .line 118
    return-object v0
.end method

.method public setChildRecords(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/apache/index/poi/ddf/EscherRecord;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 189
    .local p1, "childRecords":Ljava/util/List;, "Ljava/util/List<Lorg/apache/index/poi/ddf/EscherRecord;>;"
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string/jumbo v1, "This record does not support child records."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setOptions(S)V
    .locals 0
    .param p1, "options"    # S

    .prologue
    .line 103
    iput-short p1, p0, Lorg/apache/index/poi/ddf/EscherRecord;->_options:S

    .line 104
    return-void
.end method

.method public setRecordId(S)V
    .locals 0
    .param p1, "recordId"    # S

    .prologue
    .line 170
    iput-short p1, p0, Lorg/apache/index/poi/ddf/EscherRecord;->_recordId:S

    .line 171
    return-void
.end method
