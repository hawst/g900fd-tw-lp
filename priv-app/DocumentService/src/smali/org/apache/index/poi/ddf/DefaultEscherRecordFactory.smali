.class public Lorg/apache/index/poi/ddf/DefaultEscherRecordFactory;
.super Ljava/lang/Object;
.source "DefaultEscherRecordFactory.java"

# interfaces
.implements Lorg/apache/index/poi/ddf/EscherRecordFactory;


# static fields
.field private static escherRecordClasses:[Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static recordsMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Short;",
            "Ljava/lang/reflect/Constructor",
            "<+",
            "Lorg/apache/index/poi/ddf/EscherRecord;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 33
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/Class;

    const/4 v1, 0x0

    .line 34
    const-class v2, Lorg/apache/index/poi/ddf/EscherOptRecord;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-class v2, Lorg/apache/index/poi/ddf/EscherDgRecord;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    .line 35
    const-class v2, Lorg/apache/index/poi/ddf/EscherSpgrRecord;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-class v2, Lorg/apache/index/poi/ddf/EscherSpRecord;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-class v2, Lorg/apache/index/poi/ddf/EscherDggRecord;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    .line 36
    const-class v2, Lorg/apache/index/poi/ddf/EscherTextboxRecord;

    aput-object v2, v0, v1

    .line 33
    sput-object v0, Lorg/apache/index/poi/ddf/DefaultEscherRecordFactory;->escherRecordClasses:[Ljava/lang/Class;

    .line 38
    sget-object v0, Lorg/apache/index/poi/ddf/DefaultEscherRecordFactory;->escherRecordClasses:[Ljava/lang/Class;

    invoke-static {v0}, Lorg/apache/index/poi/ddf/DefaultEscherRecordFactory;->recordsToMap([Ljava/lang/Class;)Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lorg/apache/index/poi/ddf/DefaultEscherRecordFactory;->recordsMap:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    return-void
.end method

.method private static recordsToMap([Ljava/lang/Class;)Ljava/util/Map;
    .locals 9
    .param p0, "recClasses"    # [Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/Class",
            "<*>;)",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Short;",
            "Ljava/lang/reflect/Constructor",
            "<+",
            "Lorg/apache/index/poi/ddf/EscherRecord;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 96
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    .line 97
    .local v5, "result":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Short;Ljava/lang/reflect/Constructor<+Lorg/apache/index/poi/ddf/EscherRecord;>;>;"
    const/4 v7, 0x0

    new-array v0, v7, [Ljava/lang/Class;

    .line 99
    .local v0, "EMPTY_CLASS_ARRAY":[Ljava/lang/Class;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    array-length v7, p0

    if-lt v3, v7, :cond_0

    .line 120
    return-object v5

    .line 101
    :cond_0
    aget-object v4, p0, v3

    .line 104
    .local v4, "recCls":Ljava/lang/Class;, "Ljava/lang/Class<+Lorg/apache/index/poi/ddf/EscherRecord;>;"
    :try_start_0
    const-string/jumbo v7, "RECORD_ID"

    invoke-virtual {v4, v7}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Ljava/lang/reflect/Field;->getShort(Ljava/lang/Object;)S
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_2

    move-result v6

    .line 114
    .local v6, "sid":S
    :try_start_1
    invoke-virtual {v4, v0}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;
    :try_end_1
    .catch Ljava/lang/NoSuchMethodException; {:try_start_1 .. :try_end_1} :catch_3

    move-result-object v1

    .line 118
    .local v1, "constructor":Ljava/lang/reflect/Constructor;, "Ljava/lang/reflect/Constructor<+Lorg/apache/index/poi/ddf/EscherRecord;>;"
    invoke-static {v6}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v7

    invoke-interface {v5, v7, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 99
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 105
    .end local v1    # "constructor":Ljava/lang/reflect/Constructor;, "Ljava/lang/reflect/Constructor<+Lorg/apache/index/poi/ddf/EscherRecord;>;"
    .end local v6    # "sid":S
    :catch_0
    move-exception v2

    .line 106
    .local v2, "e":Ljava/lang/IllegalArgumentException;
    new-instance v7, Ljava/lang/RuntimeException;

    invoke-direct {v7, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v7

    .line 107
    .end local v2    # "e":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v2

    .line 108
    .local v2, "e":Ljava/lang/IllegalAccessException;
    new-instance v7, Ljava/lang/RuntimeException;

    invoke-direct {v7, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v7

    .line 109
    .end local v2    # "e":Ljava/lang/IllegalAccessException;
    :catch_2
    move-exception v2

    .line 110
    .local v2, "e":Ljava/lang/NoSuchFieldException;
    new-instance v7, Ljava/lang/RuntimeException;

    invoke-direct {v7, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v7

    .line 115
    .end local v2    # "e":Ljava/lang/NoSuchFieldException;
    .restart local v6    # "sid":S
    :catch_3
    move-exception v2

    .line 116
    .local v2, "e":Ljava/lang/NoSuchMethodException;
    new-instance v7, Ljava/lang/RuntimeException;

    invoke-direct {v7, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v7
.end method


# virtual methods
.method public createRecord([BI)Lorg/apache/index/poi/ddf/EscherRecord;
    .locals 7
    .param p1, "data"    # [B
    .param p2, "offset"    # I

    .prologue
    .line 56
    invoke-static {p1, p2}, Lorg/apache/index/poi/ddf/EscherRecord$EscherRecordHeader;->readHeader([BI)Lorg/apache/index/poi/ddf/EscherRecord$EscherRecordHeader;

    move-result-object v2

    .line 62
    .local v2, "header":Lorg/apache/index/poi/ddf/EscherRecord$EscherRecordHeader;
    invoke-virtual {v2}, Lorg/apache/index/poi/ddf/EscherRecord$EscherRecordHeader;->getOptions()S

    move-result v5

    and-int/lit8 v5, v5, 0xf

    const/16 v6, 0xf

    if-ne v5, v6, :cond_0

    .line 63
    invoke-virtual {v2}, Lorg/apache/index/poi/ddf/EscherRecord$EscherRecordHeader;->getRecordId()S

    move-result v5

    const/16 v6, -0xff3

    if-eq v5, v6, :cond_0

    .line 64
    new-instance v3, Lorg/apache/index/poi/ddf/EscherContainerRecord;

    invoke-direct {v3}, Lorg/apache/index/poi/ddf/EscherContainerRecord;-><init>()V

    .line 65
    .local v3, "r":Lorg/apache/index/poi/ddf/EscherContainerRecord;
    invoke-virtual {v2}, Lorg/apache/index/poi/ddf/EscherRecord$EscherRecordHeader;->getRecordId()S

    move-result v5

    invoke-virtual {v3, v5}, Lorg/apache/index/poi/ddf/EscherContainerRecord;->setRecordId(S)V

    .line 66
    invoke-virtual {v2}, Lorg/apache/index/poi/ddf/EscherRecord$EscherRecordHeader;->getOptions()S

    move-result v5

    invoke-virtual {v3, v5}, Lorg/apache/index/poi/ddf/EscherContainerRecord;->setOptions(S)V

    .line 84
    .end local v3    # "r":Lorg/apache/index/poi/ddf/EscherContainerRecord;
    :goto_0
    return-object v3

    .line 72
    :cond_0
    sget-object v5, Lorg/apache/index/poi/ddf/DefaultEscherRecordFactory;->recordsMap:Ljava/util/Map;

    invoke-virtual {v2}, Lorg/apache/index/poi/ddf/EscherRecord$EscherRecordHeader;->getRecordId()S

    move-result v6

    invoke-static {v6}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/reflect/Constructor;

    .line 73
    .local v4, "recordConstructor":Ljava/lang/reflect/Constructor;, "Ljava/lang/reflect/Constructor<+Lorg/apache/index/poi/ddf/EscherRecord;>;"
    const/4 v1, 0x0

    .line 74
    .local v1, "escherRecord":Lorg/apache/index/poi/ddf/EscherRecord;
    if-nez v4, :cond_1

    .line 75
    new-instance v3, Lorg/apache/index/poi/ddf/UnknownEscherRecord;

    invoke-direct {v3}, Lorg/apache/index/poi/ddf/UnknownEscherRecord;-><init>()V

    goto :goto_0

    .line 78
    :cond_1
    const/4 v5, 0x0

    :try_start_0
    new-array v5, v5, [Ljava/lang/Object;

    invoke-virtual {v4, v5}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "escherRecord":Lorg/apache/index/poi/ddf/EscherRecord;
    check-cast v1, Lorg/apache/index/poi/ddf/EscherRecord;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 82
    .restart local v1    # "escherRecord":Lorg/apache/index/poi/ddf/EscherRecord;
    invoke-virtual {v2}, Lorg/apache/index/poi/ddf/EscherRecord$EscherRecordHeader;->getRecordId()S

    move-result v5

    invoke-virtual {v1, v5}, Lorg/apache/index/poi/ddf/EscherRecord;->setRecordId(S)V

    .line 83
    invoke-virtual {v2}, Lorg/apache/index/poi/ddf/EscherRecord$EscherRecordHeader;->getOptions()S

    move-result v5

    invoke-virtual {v1, v5}, Lorg/apache/index/poi/ddf/EscherRecord;->setOptions(S)V

    move-object v3, v1

    .line 84
    goto :goto_0

    .line 79
    .end local v1    # "escherRecord":Lorg/apache/index/poi/ddf/EscherRecord;
    :catch_0
    move-exception v0

    .line 80
    .local v0, "e":Ljava/lang/Exception;
    new-instance v3, Lorg/apache/index/poi/ddf/UnknownEscherRecord;

    invoke-direct {v3}, Lorg/apache/index/poi/ddf/UnknownEscherRecord;-><init>()V

    goto :goto_0
.end method
