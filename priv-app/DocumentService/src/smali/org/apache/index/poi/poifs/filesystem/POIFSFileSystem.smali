.class public Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;
.super Ljava/lang/Object;
.source "POIFSFileSystem.java"


# static fields
.field public static final MAX_FILE_SIZE:I = 0x100000

.field private static final _logger:Lorg/apache/index/poi/util/POILogger;


# instance fields
.field private _documents:Ljava/util/List;

.field private _newFileHandler:Lorg/apache/index/poi/poifs/filesystem/POIFSFileHandler;

.field private _property_table:Lorg/apache/index/poi/poifs/property/PropertyTable;

.field private _root:Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;

.field private bigBlockSize:Lorg/apache/index/poi/poifs/common/POIFSBigBlockSize;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 68
    const-class v0, Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;

    invoke-static {v0}, Lorg/apache/index/poi/util/POILogFactory;->getLogger(Ljava/lang/Class;)Lorg/apache/index/poi/util/POILogger;

    move-result-object v0

    .line 67
    sput-object v0, Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;->_logger:Lorg/apache/index/poi/util/POILogger;

    .line 70
    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 89
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 77
    iput-object v2, p0, Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;->_newFileHandler:Lorg/apache/index/poi/poifs/filesystem/POIFSFileHandler;

    .line 84
    sget-object v1, Lorg/apache/index/poi/poifs/common/POIFSConstants;->SMALLER_BIG_BLOCK_SIZE_DETAILS:Lorg/apache/index/poi/poifs/common/POIFSBigBlockSize;

    iput-object v1, p0, Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;->bigBlockSize:Lorg/apache/index/poi/poifs/common/POIFSBigBlockSize;

    .line 91
    new-instance v0, Lorg/apache/index/poi/poifs/storage/HeaderBlock;

    iget-object v1, p0, Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;->bigBlockSize:Lorg/apache/index/poi/poifs/common/POIFSBigBlockSize;

    invoke-direct {v0, v1}, Lorg/apache/index/poi/poifs/storage/HeaderBlock;-><init>(Lorg/apache/index/poi/poifs/common/POIFSBigBlockSize;)V

    .line 92
    .local v0, "header_block":Lorg/apache/index/poi/poifs/storage/HeaderBlock;
    new-instance v1, Lorg/apache/index/poi/poifs/property/PropertyTable;

    invoke-direct {v1, v0}, Lorg/apache/index/poi/poifs/property/PropertyTable;-><init>(Lorg/apache/index/poi/poifs/storage/HeaderBlock;)V

    iput-object v1, p0, Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;->_property_table:Lorg/apache/index/poi/poifs/property/PropertyTable;

    .line 93
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;->_documents:Ljava/util/List;

    .line 94
    iput-object v2, p0, Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;->_root:Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;

    .line 95
    return-void
.end method

.method public constructor <init>(Ljava/io/File;)V
    .locals 1
    .param p1, "inFile"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 129
    new-instance v0, Ljava/io/FileInputStream;

    invoke-direct {v0, p1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-direct {p0, v0, p1}, Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;-><init>(Ljava/io/InputStream;Ljava/io/File;)V

    .line 130
    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 1
    .param p1, "stream"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 136
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;-><init>(Ljava/io/InputStream;Ljava/io/File;)V

    .line 137
    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;Ljava/io/File;)V
    .locals 13
    .param p1, "stream"    # Ljava/io/InputStream;
    .param p2, "inFile"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 143
    invoke-direct {p0}, Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;-><init>()V

    .line 144
    const/4 v12, 0x0

    .line 150
    .local v12, "success":Z
    :try_start_0
    new-instance v10, Lorg/apache/index/poi/poifs/storage/HeaderBlock;

    invoke-direct {v10, p1}, Lorg/apache/index/poi/poifs/storage/HeaderBlock;-><init>(Ljava/io/InputStream;)V

    .line 151
    .local v10, "header_block":Lorg/apache/index/poi/poifs/storage/HeaderBlock;
    invoke-virtual {v10}, Lorg/apache/index/poi/poifs/storage/HeaderBlock;->getBigBlockSize()Lorg/apache/index/poi/poifs/common/POIFSBigBlockSize;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;->bigBlockSize:Lorg/apache/index/poi/poifs/common/POIFSBigBlockSize;

    .line 155
    if-eqz p2, :cond_0

    .line 157
    new-instance v0, Lorg/apache/index/poi/poifs/filesystem/POIFSFileHandler;

    invoke-direct {v0, p2}, Lorg/apache/index/poi/poifs/filesystem/POIFSFileHandler;-><init>(Ljava/io/File;)V

    iput-object v0, p0, Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;->_newFileHandler:Lorg/apache/index/poi/poifs/filesystem/POIFSFileHandler;

    .line 161
    :cond_0
    new-instance v6, Lorg/apache/index/poi/poifs/storage/RawDataBlockList;

    iget-object v0, p0, Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;->_newFileHandler:Lorg/apache/index/poi/poifs/filesystem/POIFSFileHandler;

    iget-object v1, p0, Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;->bigBlockSize:Lorg/apache/index/poi/poifs/common/POIFSBigBlockSize;

    invoke-direct {v6, v0, p1, v1}, Lorg/apache/index/poi/poifs/storage/RawDataBlockList;-><init>(Lorg/apache/index/poi/poifs/filesystem/POIFSFileHandler;Ljava/io/InputStream;Lorg/apache/index/poi/poifs/common/POIFSBigBlockSize;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 162
    .local v6, "data_blocks":Lorg/apache/index/poi/poifs/storage/RawDataBlockList;
    const/4 v12, 0x1

    .line 164
    invoke-direct {p0, p1, v12}, Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;->closeInputStream(Ljava/io/InputStream;Z)V

    .line 170
    new-instance v0, Lorg/apache/index/poi/poifs/storage/BlockAllocationTableReader;

    invoke-virtual {v10}, Lorg/apache/index/poi/poifs/storage/HeaderBlock;->getBigBlockSize()Lorg/apache/index/poi/poifs/common/POIFSBigBlockSize;

    move-result-object v1

    .line 171
    invoke-virtual {v10}, Lorg/apache/index/poi/poifs/storage/HeaderBlock;->getBATCount()I

    move-result v2

    .line 172
    invoke-virtual {v10}, Lorg/apache/index/poi/poifs/storage/HeaderBlock;->getBATArray()[I

    move-result-object v3

    .line 173
    invoke-virtual {v10}, Lorg/apache/index/poi/poifs/storage/HeaderBlock;->getXBATCount()I

    move-result v4

    .line 174
    invoke-virtual {v10}, Lorg/apache/index/poi/poifs/storage/HeaderBlock;->getXBATIndex()I

    move-result v5

    .line 170
    invoke-direct/range {v0 .. v6}, Lorg/apache/index/poi/poifs/storage/BlockAllocationTableReader;-><init>(Lorg/apache/index/poi/poifs/common/POIFSBigBlockSize;I[IIILorg/apache/index/poi/poifs/storage/BlockList;)V

    .line 179
    new-instance v11, Lorg/apache/index/poi/poifs/property/PropertyTable;

    invoke-direct {v11, v10, v6}, Lorg/apache/index/poi/poifs/property/PropertyTable;-><init>(Lorg/apache/index/poi/poifs/storage/HeaderBlock;Lorg/apache/index/poi/poifs/storage/RawDataBlockList;)V

    .line 184
    .local v11, "properties":Lorg/apache/index/poi/poifs/property/PropertyTable;
    iget-object v0, p0, Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;->bigBlockSize:Lorg/apache/index/poi/poifs/common/POIFSBigBlockSize;

    invoke-virtual {v11}, Lorg/apache/index/poi/poifs/property/PropertyTable;->getRoot()Lorg/apache/index/poi/poifs/property/RootProperty;

    move-result-object v1

    .line 185
    invoke-virtual {v10}, Lorg/apache/index/poi/poifs/storage/HeaderBlock;->getSBATStart()I

    move-result v2

    .line 183
    invoke-static {v0, v6, v1, v2}, Lorg/apache/index/poi/poifs/storage/SmallBlockTableReader;->getSmallDocumentBlocks(Lorg/apache/index/poi/poifs/common/POIFSBigBlockSize;Lorg/apache/index/poi/poifs/storage/RawDataBlockList;Lorg/apache/index/poi/poifs/property/RootProperty;I)Lorg/apache/index/poi/poifs/storage/BlockList;

    move-result-object v5

    .line 188
    invoke-virtual {v11}, Lorg/apache/index/poi/poifs/property/PropertyTable;->getRoot()Lorg/apache/index/poi/poifs/property/RootProperty;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/index/poi/poifs/property/RootProperty;->getChildren()Ljava/util/Iterator;

    move-result-object v7

    .line 189
    const/4 v8, 0x0

    .line 190
    invoke-virtual {v10}, Lorg/apache/index/poi/poifs/storage/HeaderBlock;->getPropertyStart()I

    move-result v9

    move-object v4, p0

    .line 182
    invoke-direct/range {v4 .. v9}, Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;->processProperties(Lorg/apache/index/poi/poifs/storage/BlockList;Lorg/apache/index/poi/poifs/storage/BlockList;Ljava/util/Iterator;Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;I)V

    .line 194
    invoke-virtual {p0}, Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;->getRoot()Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;

    move-result-object v0

    invoke-virtual {v11}, Lorg/apache/index/poi/poifs/property/PropertyTable;->getRoot()Lorg/apache/index/poi/poifs/property/RootProperty;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/index/poi/poifs/property/RootProperty;->getStorageClsid()Lorg/apache/index/poi/hpsf/ClassID;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;->setStorageClsid(Lorg/apache/index/poi/hpsf/ClassID;)V

    .line 195
    return-void

    .line 163
    .end local v6    # "data_blocks":Lorg/apache/index/poi/poifs/storage/RawDataBlockList;
    .end local v10    # "header_block":Lorg/apache/index/poi/poifs/storage/HeaderBlock;
    .end local v11    # "properties":Lorg/apache/index/poi/poifs/property/PropertyTable;
    :catchall_0
    move-exception v0

    .line 164
    invoke-direct {p0, p1, v12}, Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;->closeInputStream(Ljava/io/InputStream;Z)V

    .line 165
    throw v0
.end method

.method private closeInputStream(Ljava/io/InputStream;Z)V
    .locals 5
    .param p1, "stream"    # Ljava/io/InputStream;
    .param p2, "success"    # Z

    .prologue
    .line 212
    invoke-virtual {p1}, Ljava/io/InputStream;->markSupported()Z

    move-result v2

    if-eqz v2, :cond_0

    instance-of v2, p1, Ljava/io/ByteArrayInputStream;

    if-nez v2, :cond_0

    .line 213
    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "POIFS is closing the supplied input stream of type ("

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 214
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ") which supports mark/reset.  "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 215
    const-string/jumbo v3, "This will be a problem for the caller if the stream will still be used.  "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 216
    const-string/jumbo v3, "If that is the case the caller should wrap the input stream to avoid this close logic.  "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 217
    const-string/jumbo v3, "This warning is only temporary and will not be present in future versions of POI."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 213
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 218
    .local v1, "msg":Ljava/lang/String;
    sget-object v2, Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;->_logger:Lorg/apache/index/poi/util/POILogger;

    sget v3, Lorg/apache/index/poi/util/POILogger;->WARN:I

    invoke-virtual {v2, v3, v1}, Lorg/apache/index/poi/util/POILogger;->log(ILjava/lang/Object;)V

    .line 221
    .end local v1    # "msg":Ljava/lang/String;
    :cond_0
    :try_start_0
    invoke-virtual {p1}, Ljava/io/InputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 230
    :cond_1
    return-void

    .line 222
    :catch_0
    move-exception v0

    .line 223
    .local v0, "e":Ljava/io/IOException;
    const-string/jumbo v2, "DocumentService"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "Exception :"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 224
    if-eqz p2, :cond_1

    .line 225
    new-instance v2, Ljava/lang/RuntimeException;

    invoke-direct {v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v2
.end method

.method public static hasPOIFSHeader(Ljava/io/InputStream;)Z
    .locals 8
    .param p0, "inp"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 243
    invoke-virtual {p0, v4}, Ljava/io/InputStream;->mark(I)V

    .line 245
    new-array v0, v4, [B

    .line 246
    .local v0, "header":[B
    invoke-static {p0, v0}, Lorg/apache/index/poi/util/IOUtils;->readFully(Ljava/io/InputStream;[B)I

    .line 247
    new-instance v2, Lorg/apache/index/poi/util/LongField;

    invoke-direct {v2, v3, v0}, Lorg/apache/index/poi/util/LongField;-><init>(I[B)V

    .line 250
    .local v2, "signature":Lorg/apache/index/poi/util/LongField;
    instance-of v4, p0, Ljava/io/PushbackInputStream;

    if-eqz v4, :cond_1

    move-object v1, p0

    .line 251
    check-cast v1, Ljava/io/PushbackInputStream;

    .line 252
    .local v1, "pin":Ljava/io/PushbackInputStream;
    invoke-virtual {v1, v0}, Ljava/io/PushbackInputStream;->unread([B)V

    .line 258
    .end local v1    # "pin":Ljava/io/PushbackInputStream;
    :goto_0
    invoke-virtual {v2}, Lorg/apache/index/poi/util/LongField;->get()J

    move-result-wide v4

    const-wide v6, -0x1ee54e5e1fee3030L    # -5.8639378995972355E159

    cmp-long v4, v4, v6

    if-nez v4, :cond_0

    const/4 v3, 0x1

    :cond_0
    return v3

    .line 254
    :cond_1
    invoke-virtual {p0}, Ljava/io/InputStream;->reset()V

    goto :goto_0
.end method

.method private processProperties(Lorg/apache/index/poi/poifs/storage/BlockList;Lorg/apache/index/poi/poifs/storage/BlockList;Ljava/util/Iterator;Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;I)V
    .locals 13
    .param p1, "small_blocks"    # Lorg/apache/index/poi/poifs/storage/BlockList;
    .param p2, "big_blocks"    # Lorg/apache/index/poi/poifs/storage/BlockList;
    .param p3, "properties"    # Ljava/util/Iterator;
    .param p4, "dir"    # Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;
    .param p5, "headerPropertiesStartAt"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 503
    :cond_0
    :goto_0
    invoke-interface/range {p3 .. p3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_1

    .line 549
    return-void

    .line 505
    :cond_1
    invoke-interface/range {p3 .. p3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lorg/apache/index/poi/poifs/property/Property;

    .line 506
    .local v10, "property":Lorg/apache/index/poi/poifs/property/Property;
    invoke-virtual {v10}, Lorg/apache/index/poi/poifs/property/Property;->getName()Ljava/lang/String;

    move-result-object v8

    .line 507
    .local v8, "name":Ljava/lang/String;
    if-nez p4, :cond_2

    .line 508
    invoke-virtual {p0}, Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;->getRoot()Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;

    move-result-object v9

    .line 511
    .local v9, "parent":Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;
    :goto_1
    invoke-virtual {v10}, Lorg/apache/index/poi/poifs/property/Property;->isDirectory()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 514
    invoke-virtual {v9, v8}, Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;->createDirectory(Ljava/lang/String;)Lorg/apache/index/poi/poifs/filesystem/DirectoryEntry;

    move-result-object v5

    check-cast v5, Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;

    .line 516
    .local v5, "new_dir":Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;
    if-eqz v5, :cond_0

    .line 518
    invoke-virtual {v10}, Lorg/apache/index/poi/poifs/property/Property;->getStorageClsid()Lorg/apache/index/poi/hpsf/ClassID;

    move-result-object v1

    invoke-virtual {v5, v1}, Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;->setStorageClsid(Lorg/apache/index/poi/hpsf/ClassID;)V

    .line 522
    check-cast v10, Lorg/apache/index/poi/poifs/property/DirectoryProperty;

    .end local v10    # "property":Lorg/apache/index/poi/poifs/property/Property;
    invoke-virtual {v10}, Lorg/apache/index/poi/poifs/property/DirectoryProperty;->getChildren()Ljava/util/Iterator;

    move-result-object v4

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move/from16 v6, p5

    .line 520
    invoke-direct/range {v1 .. v6}, Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;->processProperties(Lorg/apache/index/poi/poifs/storage/BlockList;Lorg/apache/index/poi/poifs/storage/BlockList;Ljava/util/Iterator;Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;I)V

    goto :goto_0

    .end local v5    # "new_dir":Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;
    .end local v9    # "parent":Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;
    .restart local v10    # "property":Lorg/apache/index/poi/poifs/property/Property;
    :cond_2
    move-object/from16 v9, p4

    .line 509
    goto :goto_1

    .line 528
    .restart local v9    # "parent":Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;
    :cond_3
    invoke-virtual {v10}, Lorg/apache/index/poi/poifs/property/Property;->getStartBlock()I

    move-result v12

    .line 529
    .local v12, "startBlock":I
    invoke-virtual {v10}, Lorg/apache/index/poi/poifs/property/Property;->getSize()I

    move-result v11

    .line 530
    .local v11, "size":I
    const/4 v7, 0x0

    .line 532
    .local v7, "document":Lorg/apache/index/poi/poifs/filesystem/POIFSDocument;
    invoke-virtual {v10}, Lorg/apache/index/poi/poifs/property/Property;->shouldUseSmallBlocks()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 535
    new-instance v7, Lorg/apache/index/poi/poifs/filesystem/POIFSDocument;

    .line 536
    .end local v7    # "document":Lorg/apache/index/poi/poifs/filesystem/POIFSDocument;
    move/from16 v0, p5

    invoke-interface {p1, v12, v0}, Lorg/apache/index/poi/poifs/storage/BlockList;->fetchBlocks(II)[Lorg/apache/index/poi/poifs/storage/ListManagedBlock;

    move-result-object v1

    .line 535
    invoke-direct {v7, v8, v1, v11}, Lorg/apache/index/poi/poifs/filesystem/POIFSDocument;-><init>(Ljava/lang/String;[Lorg/apache/index/poi/poifs/storage/ListManagedBlock;I)V

    .line 546
    .restart local v7    # "document":Lorg/apache/index/poi/poifs/filesystem/POIFSDocument;
    :goto_2
    invoke-virtual {v9, v7}, Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;->createDocument(Lorg/apache/index/poi/poifs/filesystem/POIFSDocument;)Lorg/apache/index/poi/poifs/filesystem/DocumentEntry;

    goto :goto_0

    .line 542
    :cond_4
    new-instance v7, Lorg/apache/index/poi/poifs/filesystem/POIFSDocument;

    .line 543
    .end local v7    # "document":Lorg/apache/index/poi/poifs/filesystem/POIFSDocument;
    move/from16 v0, p5

    invoke-interface {p2, v12, v0}, Lorg/apache/index/poi/poifs/storage/BlockList;->fetchBlocks(II)[Lorg/apache/index/poi/poifs/storage/ListManagedBlock;

    move-result-object v1

    .line 542
    invoke-direct {v7, v8, v1, v11}, Lorg/apache/index/poi/poifs/filesystem/POIFSDocument;-><init>(Ljava/lang/String;[Lorg/apache/index/poi/poifs/storage/ListManagedBlock;I)V

    .line 541
    .restart local v7    # "document":Lorg/apache/index/poi/poifs/filesystem/POIFSDocument;
    goto :goto_2
.end method


# virtual methods
.method addDirectory(Lorg/apache/index/poi/poifs/property/DirectoryProperty;)V
    .locals 1
    .param p1, "directory"    # Lorg/apache/index/poi/poifs/property/DirectoryProperty;

    .prologue
    .line 478
    iget-object v0, p0, Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;->_property_table:Lorg/apache/index/poi/poifs/property/PropertyTable;

    invoke-virtual {v0, p1}, Lorg/apache/index/poi/poifs/property/PropertyTable;->addProperty(Lorg/apache/index/poi/poifs/property/Property;)V

    .line 479
    return-void
.end method

.method addDocument(Lorg/apache/index/poi/poifs/filesystem/POIFSDocument;)V
    .locals 2
    .param p1, "document"    # Lorg/apache/index/poi/poifs/filesystem/POIFSDocument;

    .prologue
    .line 466
    iget-object v0, p0, Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;->_documents:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 467
    iget-object v0, p0, Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;->_property_table:Lorg/apache/index/poi/poifs/property/PropertyTable;

    invoke-virtual {p1}, Lorg/apache/index/poi/poifs/filesystem/POIFSDocument;->getDocumentProperty()Lorg/apache/index/poi/poifs/property/DocumentProperty;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/index/poi/poifs/property/PropertyTable;->addProperty(Lorg/apache/index/poi/poifs/property/Property;)V

    .line 468
    return-void
.end method

.method public closePOIFileHandler()V
    .locals 1

    .prologue
    .line 203
    iget-object v0, p0, Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;->_newFileHandler:Lorg/apache/index/poi/poifs/filesystem/POIFSFileHandler;

    invoke-virtual {v0}, Lorg/apache/index/poi/poifs/filesystem/POIFSFileHandler;->close()V

    .line 204
    return-void
.end method

.method public createDirectory(Ljava/lang/String;)Lorg/apache/index/poi/poifs/filesystem/DirectoryEntry;
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 294
    invoke-virtual {p0}, Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;->getRoot()Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;->createDirectory(Ljava/lang/String;)Lorg/apache/index/poi/poifs/filesystem/DirectoryEntry;

    move-result-object v0

    return-object v0
.end method

.method public createDocument(Ljava/io/InputStream;Ljava/lang/String;)Lorg/apache/index/poi/poifs/filesystem/DocumentEntry;
    .locals 1
    .param p1, "stream"    # Ljava/io/InputStream;
    .param p2, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 277
    invoke-virtual {p0}, Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;->getRoot()Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;

    move-result-object v0

    invoke-virtual {v0, p2, p1}, Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;->createDocument(Ljava/lang/String;Ljava/io/InputStream;)Lorg/apache/index/poi/poifs/filesystem/DocumentEntry;

    move-result-object v0

    return-object v0
.end method

.method public createDocumentInputStream(Ljava/lang/String;)Lorg/apache/index/poi/poifs/filesystem/DocumentInputStream;
    .locals 1
    .param p1, "documentName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 455
    invoke-virtual {p0}, Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;->getRoot()Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;->createDocumentInputStream(Ljava/lang/String;)Lorg/apache/index/poi/poifs/filesystem/DocumentInputStream;

    move-result-object v0

    return-object v0
.end method

.method public getBigBlockSize()I
    .locals 1

    .prologue
    .line 570
    iget-object v0, p0, Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;->bigBlockSize:Lorg/apache/index/poi/poifs/common/POIFSBigBlockSize;

    invoke-virtual {v0}, Lorg/apache/index/poi/poifs/common/POIFSBigBlockSize;->getBigBlockSize()I

    move-result v0

    return v0
.end method

.method public getBigBlockSizeDetails()Lorg/apache/index/poi/poifs/common/POIFSBigBlockSize;
    .locals 1

    .prologue
    .line 576
    iget-object v0, p0, Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;->bigBlockSize:Lorg/apache/index/poi/poifs/common/POIFSBigBlockSize;

    return-object v0
.end method

.method public getRoot()Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;
    .locals 3

    .prologue
    .line 433
    iget-object v0, p0, Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;->_root:Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;

    if-nez v0, :cond_0

    .line 435
    new-instance v0, Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;

    iget-object v1, p0, Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;->_property_table:Lorg/apache/index/poi/poifs/property/PropertyTable;

    invoke-virtual {v1}, Lorg/apache/index/poi/poifs/property/PropertyTable;->getRoot()Lorg/apache/index/poi/poifs/property/RootProperty;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, p0, v2}, Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;-><init>(Lorg/apache/index/poi/poifs/property/DirectoryProperty;Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;)V

    iput-object v0, p0, Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;->_root:Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;

    .line 437
    :cond_0
    iget-object v0, p0, Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;->_root:Lorg/apache/index/poi/poifs/filesystem/DirectoryNode;

    return-object v0
.end method

.method public getShortDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 563
    const-string/jumbo v0, "POIFS FileSystem"

    return-object v0
.end method

.method remove(Lorg/apache/index/poi/poifs/filesystem/EntryNode;)V
    .locals 2
    .param p1, "entry"    # Lorg/apache/index/poi/poifs/filesystem/EntryNode;

    .prologue
    .line 489
    iget-object v0, p0, Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;->_property_table:Lorg/apache/index/poi/poifs/property/PropertyTable;

    invoke-virtual {p1}, Lorg/apache/index/poi/poifs/filesystem/EntryNode;->getProperty()Lorg/apache/index/poi/poifs/property/Property;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/index/poi/poifs/property/PropertyTable;->removeProperty(Lorg/apache/index/poi/poifs/property/Property;)V

    .line 490
    invoke-virtual {p1}, Lorg/apache/index/poi/poifs/filesystem/EntryNode;->isDocumentEntry()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 492
    iget-object v0, p0, Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;->_documents:Ljava/util/List;

    check-cast p1, Lorg/apache/index/poi/poifs/filesystem/DocumentNode;

    .end local p1    # "entry":Lorg/apache/index/poi/poifs/filesystem/EntryNode;
    invoke-virtual {p1}, Lorg/apache/index/poi/poifs/filesystem/DocumentNode;->getDocument()Lorg/apache/index/poi/poifs/filesystem/POIFSDocument;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 494
    :cond_0
    return-void
.end method

.method public writeFilesystem(Ljava/io/OutputStream;)V
    .locals 16
    .param p1, "stream"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 311
    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;->_property_table:Lorg/apache/index/poi/poifs/property/PropertyTable;

    invoke-virtual {v13}, Lorg/apache/index/poi/poifs/property/PropertyTable;->preWrite()V

    .line 315
    new-instance v9, Lorg/apache/index/poi/poifs/storage/SmallBlockTableWriter;

    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;->bigBlockSize:Lorg/apache/index/poi/poifs/common/POIFSBigBlockSize;

    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;->_documents:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;->_property_table:Lorg/apache/index/poi/poifs/property/PropertyTable;

    invoke-virtual {v15}, Lorg/apache/index/poi/poifs/property/PropertyTable;->getRoot()Lorg/apache/index/poi/poifs/property/RootProperty;

    move-result-object v15

    invoke-direct {v9, v13, v14, v15}, Lorg/apache/index/poi/poifs/storage/SmallBlockTableWriter;-><init>(Lorg/apache/index/poi/poifs/common/POIFSBigBlockSize;Ljava/util/List;Lorg/apache/index/poi/poifs/property/RootProperty;)V

    .line 319
    .local v9, "sbtw":Lorg/apache/index/poi/poifs/storage/SmallBlockTableWriter;
    new-instance v1, Lorg/apache/index/poi/poifs/storage/BlockAllocationTableWriter;

    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;->bigBlockSize:Lorg/apache/index/poi/poifs/common/POIFSBigBlockSize;

    invoke-direct {v1, v13}, Lorg/apache/index/poi/poifs/storage/BlockAllocationTableWriter;-><init>(Lorg/apache/index/poi/poifs/common/POIFSBigBlockSize;)V

    .line 323
    .local v1, "bat":Lorg/apache/index/poi/poifs/storage/BlockAllocationTableWriter;
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 325
    .local v4, "bm_objects":Ljava/util/List;
    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;->_documents:Ljava/util/List;

    invoke-interface {v4, v13}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 326
    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;->_property_table:Lorg/apache/index/poi/poifs/property/PropertyTable;

    invoke-interface {v4, v13}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 327
    invoke-interface {v4, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 328
    invoke-virtual {v9}, Lorg/apache/index/poi/poifs/storage/SmallBlockTableWriter;->getSBAT()Lorg/apache/index/poi/poifs/storage/BlockAllocationTableWriter;

    move-result-object v13

    invoke-interface {v4, v13}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 332
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .line 334
    .local v7, "iter":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-nez v13, :cond_1

    .line 354
    invoke-virtual {v1}, Lorg/apache/index/poi/poifs/storage/BlockAllocationTableWriter;->createBlocks()I

    move-result v2

    .line 357
    .local v2, "batStartBlock":I
    new-instance v6, Lorg/apache/index/poi/poifs/storage/HeaderBlockWriter;

    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;->bigBlockSize:Lorg/apache/index/poi/poifs/common/POIFSBigBlockSize;

    invoke-direct {v6, v13}, Lorg/apache/index/poi/poifs/storage/HeaderBlockWriter;-><init>(Lorg/apache/index/poi/poifs/common/POIFSBigBlockSize;)V

    .line 359
    .local v6, "header_block_writer":Lorg/apache/index/poi/poifs/storage/HeaderBlockWriter;
    invoke-virtual {v1}, Lorg/apache/index/poi/poifs/storage/BlockAllocationTableWriter;->countBlocks()I

    move-result v13

    invoke-virtual {v6, v13, v2}, Lorg/apache/index/poi/poifs/storage/HeaderBlockWriter;->setBATBlocks(II)[Lorg/apache/index/poi/poifs/storage/BATBlock;

    move-result-object v12

    .line 363
    .local v12, "xbat_blocks":[Lorg/apache/index/poi/poifs/storage/BATBlock;
    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;->_property_table:Lorg/apache/index/poi/poifs/property/PropertyTable;

    invoke-virtual {v13}, Lorg/apache/index/poi/poifs/property/PropertyTable;->getStartBlock()I

    move-result v13

    invoke-virtual {v6, v13}, Lorg/apache/index/poi/poifs/storage/HeaderBlockWriter;->setPropertyStart(I)V

    .line 366
    invoke-virtual {v9}, Lorg/apache/index/poi/poifs/storage/SmallBlockTableWriter;->getSBAT()Lorg/apache/index/poi/poifs/storage/BlockAllocationTableWriter;

    move-result-object v13

    invoke-virtual {v13}, Lorg/apache/index/poi/poifs/storage/BlockAllocationTableWriter;->getStartBlock()I

    move-result v13

    invoke-virtual {v6, v13}, Lorg/apache/index/poi/poifs/storage/HeaderBlockWriter;->setSBATStart(I)V

    .line 369
    invoke-virtual {v9}, Lorg/apache/index/poi/poifs/storage/SmallBlockTableWriter;->getSBATBlockCount()I

    move-result v13

    invoke-virtual {v6, v13}, Lorg/apache/index/poi/poifs/storage/HeaderBlockWriter;->setSBATBlockCount(I)V

    .line 376
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 378
    .local v11, "writers":Ljava/util/List;
    invoke-interface {v11, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 379
    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;->_documents:Ljava/util/List;

    invoke-interface {v11, v13}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 380
    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;->_property_table:Lorg/apache/index/poi/poifs/property/PropertyTable;

    invoke-interface {v11, v13}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 381
    invoke-interface {v11, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 382
    invoke-virtual {v9}, Lorg/apache/index/poi/poifs/storage/SmallBlockTableWriter;->getSBAT()Lorg/apache/index/poi/poifs/storage/BlockAllocationTableWriter;

    move-result-object v13

    invoke-interface {v11, v13}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 383
    invoke-interface {v11, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 384
    const/4 v8, 0x0

    .local v8, "j":I
    :goto_1
    array-length v13, v12

    if-lt v8, v13, :cond_2

    .line 390
    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .line 391
    :goto_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-nez v13, :cond_3

    .line 397
    return-void

    .line 336
    .end local v2    # "batStartBlock":I
    .end local v6    # "header_block_writer":Lorg/apache/index/poi/poifs/storage/HeaderBlockWriter;
    .end local v8    # "j":I
    .end local v11    # "writers":Ljava/util/List;
    .end local v12    # "xbat_blocks":[Lorg/apache/index/poi/poifs/storage/BATBlock;
    :cond_1
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/apache/index/poi/poifs/filesystem/BATManaged;

    .line 337
    .local v5, "bmo":Lorg/apache/index/poi/poifs/filesystem/BATManaged;
    invoke-interface {v5}, Lorg/apache/index/poi/poifs/filesystem/BATManaged;->countBlocks()I

    move-result v3

    .line 339
    .local v3, "block_count":I
    if-eqz v3, :cond_0

    .line 341
    invoke-virtual {v1, v3}, Lorg/apache/index/poi/poifs/storage/BlockAllocationTableWriter;->allocateSpace(I)I

    move-result v13

    invoke-interface {v5, v13}, Lorg/apache/index/poi/poifs/filesystem/BATManaged;->setStartBlock(I)V

    goto :goto_0

    .line 386
    .end local v3    # "block_count":I
    .end local v5    # "bmo":Lorg/apache/index/poi/poifs/filesystem/BATManaged;
    .restart local v2    # "batStartBlock":I
    .restart local v6    # "header_block_writer":Lorg/apache/index/poi/poifs/storage/HeaderBlockWriter;
    .restart local v8    # "j":I
    .restart local v11    # "writers":Ljava/util/List;
    .restart local v12    # "xbat_blocks":[Lorg/apache/index/poi/poifs/storage/BATBlock;
    :cond_2
    aget-object v13, v12, v8

    invoke-interface {v11, v13}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 384
    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    .line 393
    :cond_3
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lorg/apache/index/poi/poifs/storage/BlockWritable;

    .line 395
    .local v10, "writer":Lorg/apache/index/poi/poifs/storage/BlockWritable;
    move-object/from16 v0, p1

    invoke-interface {v10, v0}, Lorg/apache/index/poi/poifs/storage/BlockWritable;->writeBlocks(Ljava/io/OutputStream;)V

    goto :goto_2
.end method
