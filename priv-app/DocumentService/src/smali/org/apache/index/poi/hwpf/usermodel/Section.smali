.class public final Lorg/apache/index/poi/hwpf/usermodel/Section;
.super Lorg/apache/index/poi/hwpf/usermodel/Range;
.source "Section.java"


# instance fields
.field private _props:Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;


# direct methods
.method public constructor <init>(Lorg/apache/index/poi/hwpf/model/SEPX;Lorg/apache/index/poi/hwpf/usermodel/Range;)V
    .locals 3
    .param p1, "sepx"    # Lorg/apache/index/poi/hwpf/model/SEPX;
    .param p2, "parent"    # Lorg/apache/index/poi/hwpf/usermodel/Range;

    .prologue
    .line 30
    iget v0, p2, Lorg/apache/index/poi/hwpf/usermodel/Range;->_start:I

    invoke-virtual {p1}, Lorg/apache/index/poi/hwpf/model/SEPX;->getStart()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iget v1, p2, Lorg/apache/index/poi/hwpf/usermodel/Range;->_end:I

    invoke-virtual {p1}, Lorg/apache/index/poi/hwpf/model/SEPX;->getEnd()I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-direct {p0, v0, v1, p2}, Lorg/apache/index/poi/hwpf/usermodel/Range;-><init>(IILorg/apache/index/poi/hwpf/usermodel/Range;)V

    .line 31
    invoke-virtual {p1}, Lorg/apache/index/poi/hwpf/model/SEPX;->getSectionProperties()Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/index/poi/hwpf/usermodel/Section;->_props:Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;

    .line 32
    return-void
.end method


# virtual methods
.method public clone()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 47
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/index/poi/hwpf/usermodel/Section;

    .line 48
    .local v0, "s":Lorg/apache/index/poi/hwpf/usermodel/Section;
    iget-object v1, p0, Lorg/apache/index/poi/hwpf/usermodel/Section;->_props:Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;

    invoke-virtual {v1}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;

    iput-object v1, v0, Lorg/apache/index/poi/hwpf/usermodel/Section;->_props:Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;

    .line 49
    return-object v0
.end method

.method public getNumColumns()I
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/usermodel/Section;->_props:Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;

    invoke-virtual {v0}, Lorg/apache/index/poi/hwpf/usermodel/SectionProperties;->getCcolM1()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public type()I
    .locals 1

    .prologue
    .line 36
    const/4 v0, 0x2

    return v0
.end method
