.class public final Lorg/apache/index/poi/hslf/record/NotesAtom;
.super Lorg/apache/index/poi/hslf/record/RecordAtom;
.source "NotesAtom.java"


# static fields
.field private static _type:J


# instance fields
.field private _header:[B

.field private followMasterBackground:Z

.field private followMasterObjects:Z

.field private followMasterScheme:Z

.field private reserved:[B

.field private slideID:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 35
    const-wide/16 v0, 0x3f1

    sput-wide v0, Lorg/apache/index/poi/hslf/record/NotesAtom;->_type:J

    return-void
.end method

.method protected constructor <init>([BII)V
    .locals 5
    .param p1, "source"    # [B
    .param p2, "start"    # I
    .param p3, "len"    # I

    .prologue
    const/16 v2, 0x8

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 60
    invoke-direct {p0}, Lorg/apache/index/poi/hslf/record/RecordAtom;-><init>()V

    .line 62
    if-ge p3, v2, :cond_0

    const/16 p3, 0x8

    .line 65
    :cond_0
    new-array v1, v2, [B

    iput-object v1, p0, Lorg/apache/index/poi/hslf/record/NotesAtom;->_header:[B

    .line 66
    iget-object v1, p0, Lorg/apache/index/poi/hslf/record/NotesAtom;->_header:[B

    invoke-static {p1, p2, v1, v4, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 69
    add-int/lit8 v1, p2, 0x8

    invoke-static {p1, v1}, Lorg/apache/index/poi/util/LittleEndian;->getInt([BI)I

    move-result v1

    iput v1, p0, Lorg/apache/index/poi/hslf/record/NotesAtom;->slideID:I

    .line 72
    add-int/lit8 v1, p2, 0xc

    invoke-static {p1, v1}, Lorg/apache/index/poi/util/LittleEndian;->getUShort([BI)I

    move-result v0

    .line 73
    .local v0, "flags":I
    and-int/lit8 v1, v0, 0x4

    const/4 v2, 0x4

    if-ne v1, v2, :cond_1

    .line 74
    iput-boolean v3, p0, Lorg/apache/index/poi/hslf/record/NotesAtom;->followMasterBackground:Z

    .line 78
    :goto_0
    and-int/lit8 v1, v0, 0x2

    const/4 v2, 0x2

    if-ne v1, v2, :cond_2

    .line 79
    iput-boolean v3, p0, Lorg/apache/index/poi/hslf/record/NotesAtom;->followMasterScheme:Z

    .line 83
    :goto_1
    and-int/lit8 v1, v0, 0x1

    if-ne v1, v3, :cond_3

    .line 84
    iput-boolean v3, p0, Lorg/apache/index/poi/hslf/record/NotesAtom;->followMasterObjects:Z

    .line 90
    :goto_2
    add-int/lit8 v1, p3, -0xe

    new-array v1, v1, [B

    iput-object v1, p0, Lorg/apache/index/poi/hslf/record/NotesAtom;->reserved:[B

    .line 91
    add-int/lit8 v1, p2, 0xe

    iget-object v2, p0, Lorg/apache/index/poi/hslf/record/NotesAtom;->reserved:[B

    iget-object v3, p0, Lorg/apache/index/poi/hslf/record/NotesAtom;->reserved:[B

    array-length v3, v3

    invoke-static {p1, v1, v2, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 92
    return-void

    .line 76
    :cond_1
    iput-boolean v4, p0, Lorg/apache/index/poi/hslf/record/NotesAtom;->followMasterBackground:Z

    goto :goto_0

    .line 81
    :cond_2
    iput-boolean v4, p0, Lorg/apache/index/poi/hslf/record/NotesAtom;->followMasterScheme:Z

    goto :goto_1

    .line 86
    :cond_3
    iput-boolean v4, p0, Lorg/apache/index/poi/hslf/record/NotesAtom;->followMasterObjects:Z

    goto :goto_2
.end method


# virtual methods
.method public getFollowMasterBackground()Z
    .locals 1

    .prologue
    .line 49
    iget-boolean v0, p0, Lorg/apache/index/poi/hslf/record/NotesAtom;->followMasterBackground:Z

    return v0
.end method

.method public getFollowMasterObjects()Z
    .locals 1

    .prologue
    .line 47
    iget-boolean v0, p0, Lorg/apache/index/poi/hslf/record/NotesAtom;->followMasterObjects:Z

    return v0
.end method

.method public getFollowMasterScheme()Z
    .locals 1

    .prologue
    .line 48
    iget-boolean v0, p0, Lorg/apache/index/poi/hslf/record/NotesAtom;->followMasterScheme:Z

    return v0
.end method

.method public getRecordType()J
    .locals 2

    .prologue
    .line 97
    sget-wide v0, Lorg/apache/index/poi/hslf/record/NotesAtom;->_type:J

    return-wide v0
.end method

.method public getSlideID()I
    .locals 1

    .prologue
    .line 44
    iget v0, p0, Lorg/apache/index/poi/hslf/record/NotesAtom;->slideID:I

    return v0
.end method

.method public setFollowMasterBackground(Z)V
    .locals 0
    .param p1, "flag"    # Z

    .prologue
    .line 52
    iput-boolean p1, p0, Lorg/apache/index/poi/hslf/record/NotesAtom;->followMasterBackground:Z

    return-void
.end method

.method public setFollowMasterObjects(Z)V
    .locals 0
    .param p1, "flag"    # Z

    .prologue
    .line 50
    iput-boolean p1, p0, Lorg/apache/index/poi/hslf/record/NotesAtom;->followMasterObjects:Z

    return-void
.end method

.method public setFollowMasterScheme(Z)V
    .locals 0
    .param p1, "flag"    # Z

    .prologue
    .line 51
    iput-boolean p1, p0, Lorg/apache/index/poi/hslf/record/NotesAtom;->followMasterScheme:Z

    return-void
.end method

.method public setSlideID(I)V
    .locals 0
    .param p1, "id"    # I

    .prologue
    .line 45
    iput p1, p0, Lorg/apache/index/poi/hslf/record/NotesAtom;->slideID:I

    return-void
.end method

.method public writeOut(Ljava/io/OutputStream;)V
    .locals 2
    .param p1, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 105
    iget-object v1, p0, Lorg/apache/index/poi/hslf/record/NotesAtom;->_header:[B

    invoke-virtual {p1, v1}, Ljava/io/OutputStream;->write([B)V

    .line 108
    iget v1, p0, Lorg/apache/index/poi/hslf/record/NotesAtom;->slideID:I

    invoke-static {v1, p1}, Lorg/apache/index/poi/hslf/record/NotesAtom;->writeLittleEndian(ILjava/io/OutputStream;)V

    .line 111
    const/4 v0, 0x0

    .line 112
    .local v0, "flags":S
    iget-boolean v1, p0, Lorg/apache/index/poi/hslf/record/NotesAtom;->followMasterObjects:Z

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    int-to-short v0, v1

    .line 113
    :cond_0
    iget-boolean v1, p0, Lorg/apache/index/poi/hslf/record/NotesAtom;->followMasterScheme:Z

    if-eqz v1, :cond_1

    add-int/lit8 v1, v0, 0x2

    int-to-short v0, v1

    .line 114
    :cond_1
    iget-boolean v1, p0, Lorg/apache/index/poi/hslf/record/NotesAtom;->followMasterBackground:Z

    if-eqz v1, :cond_2

    add-int/lit8 v1, v0, 0x4

    int-to-short v0, v1

    .line 115
    :cond_2
    invoke-static {v0, p1}, Lorg/apache/index/poi/hslf/record/NotesAtom;->writeLittleEndian(SLjava/io/OutputStream;)V

    .line 118
    iget-object v1, p0, Lorg/apache/index/poi/hslf/record/NotesAtom;->reserved:[B

    invoke-virtual {p1, v1}, Ljava/io/OutputStream;->write([B)V

    .line 119
    return-void
.end method
