.class public final Lorg/apache/index/poi/ddf/UnknownEscherRecord;
.super Lorg/apache/index/poi/ddf/EscherRecord;
.source "UnknownEscherRecord.java"


# static fields
.field private static final NO_BYTES:[B


# instance fields
.field private _childRecords:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/apache/index/poi/ddf/EscherRecord;",
            ">;"
        }
    .end annotation
.end field

.field private thedata:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    const/4 v0, 0x0

    new-array v0, v0, [B

    sput-object v0, Lorg/apache/index/poi/ddf/UnknownEscherRecord;->NO_BYTES:[B

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0}, Lorg/apache/index/poi/ddf/EscherRecord;-><init>()V

    .line 37
    sget-object v0, Lorg/apache/index/poi/ddf/UnknownEscherRecord;->NO_BYTES:[B

    iput-object v0, p0, Lorg/apache/index/poi/ddf/UnknownEscherRecord;->thedata:[B

    .line 41
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/apache/index/poi/ddf/UnknownEscherRecord;->_childRecords:Ljava/util/List;

    .line 42
    return-void
.end method


# virtual methods
.method public addChildRecord(Lorg/apache/index/poi/ddf/EscherRecord;)V
    .locals 1
    .param p1, "childRecord"    # Lorg/apache/index/poi/ddf/EscherRecord;

    .prologue
    .line 146
    invoke-virtual {p0}, Lorg/apache/index/poi/ddf/UnknownEscherRecord;->getChildRecords()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 147
    return-void
.end method

.method public clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 117
    invoke-super {p0}, Lorg/apache/index/poi/ddf/EscherRecord;->clone()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public fillFields([BILorg/apache/index/poi/ddf/EscherRecordFactory;)I
    .locals 8
    .param p1, "data"    # [B
    .param p2, "offset"    # I
    .param p3, "recordFactory"    # Lorg/apache/index/poi/ddf/EscherRecordFactory;

    .prologue
    const/4 v7, 0x0

    .line 45
    invoke-virtual {p0, p1, p2}, Lorg/apache/index/poi/ddf/UnknownEscherRecord;->readHeader([BI)I

    move-result v1

    .line 52
    .local v1, "bytesRemaining":I
    array-length v5, p1

    add-int/lit8 v6, p2, 0x8

    sub-int v0, v5, v6

    .line 53
    .local v0, "avaliable":I
    if-le v1, v0, :cond_0

    .line 54
    move v1, v0

    .line 57
    :cond_0
    invoke-virtual {p0}, Lorg/apache/index/poi/ddf/UnknownEscherRecord;->isContainerRecord()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 58
    const/4 v2, 0x0

    .line 59
    .local v2, "bytesWritten":I
    new-array v5, v7, [B

    iput-object v5, p0, Lorg/apache/index/poi/ddf/UnknownEscherRecord;->thedata:[B

    .line 60
    add-int/lit8 p2, p2, 0x8

    .line 61
    add-int/lit8 v2, v2, 0x8

    .line 62
    :goto_0
    if-gtz v1, :cond_1

    .line 76
    .end local v2    # "bytesWritten":I
    :goto_1
    return v2

    .line 64
    .restart local v2    # "bytesWritten":I
    :cond_1
    invoke-interface {p3, p1, p2}, Lorg/apache/index/poi/ddf/EscherRecordFactory;->createRecord([BI)Lorg/apache/index/poi/ddf/EscherRecord;

    move-result-object v3

    .line 65
    .local v3, "child":Lorg/apache/index/poi/ddf/EscherRecord;
    invoke-virtual {v3, p1, p2, p3}, Lorg/apache/index/poi/ddf/EscherRecord;->fillFields([BILorg/apache/index/poi/ddf/EscherRecordFactory;)I

    move-result v4

    .line 66
    .local v4, "childBytesWritten":I
    add-int/2addr v2, v4

    .line 67
    add-int/2addr p2, v4

    .line 68
    sub-int/2addr v1, v4

    .line 69
    invoke-virtual {p0}, Lorg/apache/index/poi/ddf/UnknownEscherRecord;->getChildRecords()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 74
    .end local v2    # "bytesWritten":I
    .end local v3    # "child":Lorg/apache/index/poi/ddf/EscherRecord;
    .end local v4    # "childBytesWritten":I
    :cond_2
    new-array v5, v1, [B

    iput-object v5, p0, Lorg/apache/index/poi/ddf/UnknownEscherRecord;->thedata:[B

    .line 75
    add-int/lit8 v5, p2, 0x8

    iget-object v6, p0, Lorg/apache/index/poi/ddf/UnknownEscherRecord;->thedata:[B

    invoke-static {p1, v5, v6, v7, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 76
    add-int/lit8 v2, v1, 0x8

    goto :goto_1
.end method

.method public getChildRecords()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lorg/apache/index/poi/ddf/EscherRecord;",
            ">;"
        }
    .end annotation

    .prologue
    .line 108
    iget-object v0, p0, Lorg/apache/index/poi/ddf/UnknownEscherRecord;->_childRecords:Ljava/util/List;

    return-object v0
.end method

.method public getData()[B
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lorg/apache/index/poi/ddf/UnknownEscherRecord;->thedata:[B

    return-object v0
.end method

.method public getRecordName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 121
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "Unknown 0x"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lorg/apache/index/poi/ddf/UnknownEscherRecord;->getRecordId()S

    move-result v1

    invoke-static {v1}, Lorg/apache/index/poi/util/HexDump;->toHex(S)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getRecordSize()I
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lorg/apache/index/poi/ddf/UnknownEscherRecord;->thedata:[B

    array-length v0, v0

    add-int/lit8 v0, v0, 0x8

    return v0
.end method

.method public serialize(I[BLorg/apache/index/poi/ddf/EscherSerializationListener;)I
    .locals 7
    .param p1, "offset"    # I
    .param p2, "data"    # [B
    .param p3, "listener"    # Lorg/apache/index/poi/ddf/EscherSerializationListener;

    .prologue
    .line 80
    invoke-virtual {p0}, Lorg/apache/index/poi/ddf/UnknownEscherRecord;->getRecordId()S

    move-result v3

    invoke-interface {p3, p1, v3, p0}, Lorg/apache/index/poi/ddf/EscherSerializationListener;->beforeRecordSerialize(ISLorg/apache/index/poi/ddf/EscherRecord;)V

    .line 82
    invoke-virtual {p0}, Lorg/apache/index/poi/ddf/UnknownEscherRecord;->getOptions()S

    move-result v3

    invoke-static {p2, p1, v3}, Lorg/apache/index/poi/util/LittleEndian;->putShort([BIS)V

    .line 83
    add-int/lit8 v3, p1, 0x2

    invoke-virtual {p0}, Lorg/apache/index/poi/ddf/UnknownEscherRecord;->getRecordId()S

    move-result v4

    invoke-static {p2, v3, v4}, Lorg/apache/index/poi/util/LittleEndian;->putShort([BIS)V

    .line 84
    iget-object v3, p0, Lorg/apache/index/poi/ddf/UnknownEscherRecord;->thedata:[B

    array-length v2, v3

    .line 85
    .local v2, "remainingBytes":I
    iget-object v3, p0, Lorg/apache/index/poi/ddf/UnknownEscherRecord;->_childRecords:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_0

    .line 88
    add-int/lit8 v3, p1, 0x4

    invoke-static {p2, v3, v2}, Lorg/apache/index/poi/util/LittleEndian;->putInt([BII)V

    .line 89
    iget-object v3, p0, Lorg/apache/index/poi/ddf/UnknownEscherRecord;->thedata:[B

    const/4 v4, 0x0

    add-int/lit8 v5, p1, 0x8

    iget-object v6, p0, Lorg/apache/index/poi/ddf/UnknownEscherRecord;->thedata:[B

    array-length v6, v6

    invoke-static {v3, v4, p2, v5, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 90
    add-int/lit8 v3, p1, 0x8

    iget-object v4, p0, Lorg/apache/index/poi/ddf/UnknownEscherRecord;->thedata:[B

    array-length v4, v4

    add-int v0, v3, v4

    .line 91
    .local v0, "pos":I
    iget-object v3, p0, Lorg/apache/index/poi/ddf/UnknownEscherRecord;->_childRecords:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_1

    .line 95
    invoke-virtual {p0}, Lorg/apache/index/poi/ddf/UnknownEscherRecord;->getRecordId()S

    move-result v3

    sub-int v4, v0, p1

    invoke-interface {p3, v0, v3, v4, p0}, Lorg/apache/index/poi/ddf/EscherSerializationListener;->afterRecordSerialize(ISILorg/apache/index/poi/ddf/EscherRecord;)V

    .line 96
    sub-int v3, v0, p1

    return v3

    .line 85
    .end local v0    # "pos":I
    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/index/poi/ddf/EscherRecord;

    .line 86
    .local v1, "r":Lorg/apache/index/poi/ddf/EscherRecord;
    invoke-virtual {v1}, Lorg/apache/index/poi/ddf/EscherRecord;->getRecordSize()I

    move-result v4

    add-int/2addr v2, v4

    goto :goto_0

    .line 91
    .end local v1    # "r":Lorg/apache/index/poi/ddf/EscherRecord;
    .restart local v0    # "pos":I
    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/index/poi/ddf/EscherRecord;

    .line 92
    .restart local v1    # "r":Lorg/apache/index/poi/ddf/EscherRecord;
    invoke-virtual {v1, v0, p2, p3}, Lorg/apache/index/poi/ddf/EscherRecord;->serialize(I[BLorg/apache/index/poi/ddf/EscherSerializationListener;)I

    move-result v4

    add-int/2addr v0, v4

    goto :goto_1
.end method

.method public setChildRecords(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/apache/index/poi/ddf/EscherRecord;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 112
    .local p1, "childRecords":Ljava/util/List;, "Ljava/util/List<Lorg/apache/index/poi/ddf/EscherRecord;>;"
    iput-object p1, p0, Lorg/apache/index/poi/ddf/UnknownEscherRecord;->_childRecords:Ljava/util/List;

    .line 113
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .prologue
    const/16 v5, 0xa

    .line 125
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 126
    .local v0, "children":Ljava/lang/StringBuffer;
    invoke-virtual {p0}, Lorg/apache/index/poi/ddf/UnknownEscherRecord;->getChildRecords()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_0

    .line 127
    const-string/jumbo v3, "  children: \n"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 128
    iget-object v3, p0, Lorg/apache/index/poi/ddf/UnknownEscherRecord;->_childRecords:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_1

    .line 134
    :cond_0
    iget-object v3, p0, Lorg/apache/index/poi/ddf/UnknownEscherRecord;->thedata:[B

    const/16 v4, 0x20

    invoke-static {v3, v4}, Lorg/apache/index/poi/util/HexDump;->toHex([BI)Ljava/lang/String;

    move-result-object v2

    .line 136
    .local v2, "theDumpHex":Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 137
    const-string/jumbo v4, "  isContainer: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lorg/apache/index/poi/ddf/UnknownEscherRecord;->isContainerRecord()Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 138
    const-string/jumbo v4, "  options: 0x"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lorg/apache/index/poi/ddf/UnknownEscherRecord;->getOptions()S

    move-result v4

    invoke-static {v4}, Lorg/apache/index/poi/util/HexDump;->toHex(S)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 139
    const-string/jumbo v4, "  recordId: 0x"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lorg/apache/index/poi/ddf/UnknownEscherRecord;->getRecordId()S

    move-result v4

    invoke-static {v4}, Lorg/apache/index/poi/util/HexDump;->toHex(S)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 140
    const-string/jumbo v4, "  numchildren: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lorg/apache/index/poi/ddf/UnknownEscherRecord;->getChildRecords()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 141
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 142
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 136
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3

    .line 128
    .end local v2    # "theDumpHex":Ljava/lang/String;
    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/index/poi/ddf/EscherRecord;

    .line 129
    .local v1, "record":Lorg/apache/index/poi/ddf/EscherRecord;
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 130
    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto/16 :goto_0
.end method
