.class public final Lorg/apache/index/poi/hssf/record/RecordFactory;
.super Ljava/lang/Object;
.source "RecordFactory.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/index/poi/hssf/record/RecordFactory$I_RecordCreator;,
        Lorg/apache/index/poi/hssf/record/RecordFactory$ReflectionConstructorRecordCreator;,
        Lorg/apache/index/poi/hssf/record/RecordFactory$ReflectionMethodRecordCreator;
    }
.end annotation


# static fields
.field private static final CONSTRUCTOR_ARGS:[Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final _recordCreatorsById:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lorg/apache/index/poi/hssf/record/RecordFactory$I_RecordCreator;",
            ">;"
        }
    .end annotation
.end field

.field private static final recordClasses:[Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Ljava/lang/Class",
            "<+",
            "Lorg/apache/index/poi/hssf/record/Record;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 106
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Class;

    const-class v1, Lorg/apache/index/poi/hssf/record/RecordInputStream;

    aput-object v1, v0, v2

    const-class v1, Lcom/samsung/index/ITextContentObs;

    aput-object v1, v0, v3

    sput-object v0, Lorg/apache/index/poi/hssf/record/RecordFactory;->CONSTRUCTOR_ARGS:[Ljava/lang/Class;

    .line 113
    new-array v0, v3, [Ljava/lang/Class;

    .line 205
    const-class v1, Lorg/apache/index/poi/hssf/record/SSTRecord;

    aput-object v1, v0, v2

    .line 113
    sput-object v0, Lorg/apache/index/poi/hssf/record/RecordFactory;->recordClasses:[Ljava/lang/Class;

    .line 242
    sget-object v0, Lorg/apache/index/poi/hssf/record/RecordFactory;->recordClasses:[Ljava/lang/Class;

    invoke-static {v0}, Lorg/apache/index/poi/hssf/record/RecordFactory;->recordsToMap([Ljava/lang/Class;)Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lorg/apache/index/poi/hssf/record/RecordFactory;->_recordCreatorsById:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static createRecords(Ljava/io/InputStream;Lcom/samsung/index/ITextContentObs;)Ljava/util/List;
    .locals 5
    .param p0, "in"    # Ljava/io/InputStream;
    .param p1, "indexWriterhssf"    # Lcom/samsung/index/ITextContentObs;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/InputStream;",
            "Lcom/samsung/index/ITextContentObs;",
            ")",
            "Ljava/util/List",
            "<",
            "Lorg/apache/index/poi/hssf/record/Record;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/index/poi/hssf/record/RecordFormatException;
        }
    .end annotation

    .prologue
    .line 429
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 430
    .local v2, "records":Ljava/util/List;, "Ljava/util/List<Lorg/apache/index/poi/hssf/record/Record;>;"
    new-instance v0, Lorg/apache/index/poi/hssf/record/RecordFactoryInputStream;

    const/4 v3, 0x0

    invoke-direct {v0, p0, v3, p1}, Lorg/apache/index/poi/hssf/record/RecordFactoryInputStream;-><init>(Ljava/io/InputStream;ZLcom/samsung/index/ITextContentObs;)V

    .line 433
    .local v0, "recStream":Lorg/apache/index/poi/hssf/record/RecordFactoryInputStream;
    :cond_0
    invoke-virtual {v0, p1}, Lorg/apache/index/poi/hssf/record/RecordFactoryInputStream;->nextRecord(Lcom/samsung/index/ITextContentObs;)Lorg/apache/index/poi/hssf/record/Record;

    move-result-object v1

    .local v1, "record":Lorg/apache/index/poi/hssf/record/Record;
    if-nez v1, :cond_1

    .line 450
    :goto_0
    return-object v2

    .line 435
    :cond_1
    invoke-virtual {v1}, Lorg/apache/index/poi/hssf/record/Record;->getSid()S

    move-result v3

    const/16 v4, 0xfc

    if-ne v3, v4, :cond_0

    .line 437
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public static createSingleRecord(Lorg/apache/index/poi/hssf/record/RecordInputStream;Lcom/samsung/index/ITextContentObs;)Lorg/apache/index/poi/hssf/record/Record;
    .locals 3
    .param p0, "in"    # Lorg/apache/index/poi/hssf/record/RecordInputStream;
    .param p1, "indexWriterhssf"    # Lcom/samsung/index/ITextContentObs;

    .prologue
    .line 283
    sget-object v1, Lorg/apache/index/poi/hssf/record/RecordFactory;->_recordCreatorsById:Ljava/util/Map;

    invoke-virtual {p0}, Lorg/apache/index/poi/hssf/record/RecordInputStream;->getSid()S

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/index/poi/hssf/record/RecordFactory$I_RecordCreator;

    .line 285
    .local v0, "constructor":Lorg/apache/index/poi/hssf/record/RecordFactory$I_RecordCreator;
    if-nez v0, :cond_0

    .line 286
    new-instance v1, Lorg/apache/index/poi/hssf/record/UnknownRecord;

    invoke-direct {v1, p0}, Lorg/apache/index/poi/hssf/record/UnknownRecord;-><init>(Lorg/apache/index/poi/hssf/record/RecordInputStream;)V

    .line 289
    :goto_0
    return-object v1

    :cond_0
    invoke-interface {v0, p0, p1}, Lorg/apache/index/poi/hssf/record/RecordFactory$I_RecordCreator;->create(Lorg/apache/index/poi/hssf/record/RecordInputStream;Lcom/samsung/index/ITextContentObs;)Lorg/apache/index/poi/hssf/record/Record;

    move-result-object v1

    goto :goto_0
.end method

.method public static getRecordClass(I)Ljava/lang/Class;
    .locals 3
    .param p0, "sid"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/lang/Class",
            "<+",
            "Lorg/apache/index/poi/hssf/record/Record;",
            ">;"
        }
    .end annotation

    .prologue
    .line 255
    sget-object v1, Lorg/apache/index/poi/hssf/record/RecordFactory;->_recordCreatorsById:Ljava/util/Map;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/index/poi/hssf/record/RecordFactory$I_RecordCreator;

    .line 256
    .local v0, "rc":Lorg/apache/index/poi/hssf/record/RecordFactory$I_RecordCreator;
    if-nez v0, :cond_0

    .line 257
    const/4 v1, 0x0

    .line 259
    :goto_0
    return-object v1

    :cond_0
    invoke-interface {v0}, Lorg/apache/index/poi/hssf/record/RecordFactory$I_RecordCreator;->getRecordClass()Ljava/lang/Class;

    move-result-object v1

    goto :goto_0
.end method

.method private static getRecordCreator(Ljava/lang/Class;)Lorg/apache/index/poi/hssf/record/RecordFactory$I_RecordCreator;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lorg/apache/index/poi/hssf/record/Record;",
            ">;)",
            "Lorg/apache/index/poi/hssf/record/RecordFactory$I_RecordCreator;"
        }
    .end annotation

    .prologue
    .line 403
    .local p0, "recClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lorg/apache/index/poi/hssf/record/Record;>;"
    :try_start_0
    sget-object v3, Lorg/apache/index/poi/hssf/record/RecordFactory;->CONSTRUCTOR_ARGS:[Ljava/lang/Class;

    invoke-virtual {p0, v3}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v0

    .line 404
    .local v0, "constructor":Ljava/lang/reflect/Constructor;, "Ljava/lang/reflect/Constructor<+Lorg/apache/index/poi/hssf/record/Record;>;"
    new-instance v3, Lorg/apache/index/poi/hssf/record/RecordFactory$ReflectionConstructorRecordCreator;

    invoke-direct {v3, v0}, Lorg/apache/index/poi/hssf/record/RecordFactory$ReflectionConstructorRecordCreator;-><init>(Ljava/lang/reflect/Constructor;)V
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0

    .line 411
    .end local v0    # "constructor":Ljava/lang/reflect/Constructor;, "Ljava/lang/reflect/Constructor<+Lorg/apache/index/poi/hssf/record/Record;>;"
    :goto_0
    return-object v3

    .line 405
    :catch_0
    move-exception v1

    .line 406
    .local v1, "e":Ljava/lang/NoSuchMethodException;
    const-string/jumbo v3, "DocumentService"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "Exception :"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/NoSuchMethodException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 410
    :try_start_1
    const-string/jumbo v3, "create"

    sget-object v4, Lorg/apache/index/poi/hssf/record/RecordFactory;->CONSTRUCTOR_ARGS:[Ljava/lang/Class;

    invoke-virtual {p0, v3, v4}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    .line 411
    .local v2, "m":Ljava/lang/reflect/Method;
    new-instance v3, Lorg/apache/index/poi/hssf/record/RecordFactory$ReflectionMethodRecordCreator;

    invoke-direct {v3, v2}, Lorg/apache/index/poi/hssf/record/RecordFactory$ReflectionMethodRecordCreator;-><init>(Ljava/lang/reflect/Method;)V
    :try_end_1
    .catch Ljava/lang/NoSuchMethodException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 412
    .end local v2    # "m":Ljava/lang/reflect/Method;
    :catch_1
    move-exception v1

    .line 413
    new-instance v3, Ljava/lang/RuntimeException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "Failed to find constructor or create method for ("

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, ")."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v3
.end method

.method private static recordsToMap([Ljava/lang/Class;)Ljava/util/Map;
    .locals 11
    .param p0, "records"    # [Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/Class",
            "<+",
            "Lorg/apache/index/poi/hssf/record/Record;",
            ">;)",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lorg/apache/index/poi/hssf/record/RecordFactory$I_RecordCreator;",
            ">;"
        }
    .end annotation

    .prologue
    .line 365
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    .line 366
    .local v5, "result":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Lorg/apache/index/poi/hssf/record/RecordFactory$I_RecordCreator;>;"
    new-instance v7, Ljava/util/HashSet;

    array-length v8, p0

    mul-int/lit8 v8, v8, 0x3

    div-int/lit8 v8, v8, 0x2

    invoke-direct {v7, v8}, Ljava/util/HashSet;-><init>(I)V

    .line 368
    .local v7, "uniqueRecClasses":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Class<*>;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v8, p0

    if-lt v0, v8, :cond_0

    .line 397
    return-object v5

    .line 370
    :cond_0
    aget-object v4, p0, v0

    .line 371
    .local v4, "recClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lorg/apache/index/poi/hssf/record/Record;>;"
    const-class v8, Lorg/apache/index/poi/hssf/record/Record;

    invoke-virtual {v8, v4}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v8

    if-nez v8, :cond_1

    .line 372
    new-instance v8, Ljava/lang/RuntimeException;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string/jumbo v10, "Invalid record sub-class ("

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, ")"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 374
    :cond_1
    invoke-virtual {v4}, Ljava/lang/Class;->getModifiers()I

    move-result v8

    invoke-static {v8}, Ljava/lang/reflect/Modifier;->isAbstract(I)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 375
    new-instance v8, Ljava/lang/RuntimeException;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string/jumbo v10, "Invalid record class ("

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, ") - must not be abstract"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 377
    :cond_2
    invoke-interface {v7, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_3

    .line 378
    new-instance v8, Ljava/lang/RuntimeException;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string/jumbo v10, "duplicate record class ("

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, ")"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 383
    :cond_3
    :try_start_0
    const-string/jumbo v8, "sid"

    invoke-virtual {v4, v8}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Ljava/lang/reflect/Field;->getShort(Ljava/lang/Object;)S
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v6

    .line 388
    .local v6, "sid":I
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 389
    .local v2, "key":Ljava/lang/Integer;
    invoke-interface {v5, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 390
    invoke-interface {v5, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lorg/apache/index/poi/hssf/record/RecordFactory$I_RecordCreator;

    invoke-interface {v8}, Lorg/apache/index/poi/hssf/record/RecordFactory$I_RecordCreator;->getRecordClass()Ljava/lang/Class;

    move-result-object v3

    .line 391
    .local v3, "prevClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    new-instance v8, Ljava/lang/RuntimeException;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string/jumbo v10, "duplicate record sid 0x"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v6}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    .line 392
    const-string/jumbo v10, " for classes ("

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, ") and ("

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, ")"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 391
    invoke-direct {v8, v9}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 384
    .end local v2    # "key":Ljava/lang/Integer;
    .end local v3    # "prevClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v6    # "sid":I
    :catch_0
    move-exception v1

    .line 385
    .local v1, "illegalArgumentException":Ljava/lang/Exception;
    new-instance v8, Lorg/apache/index/poi/hssf/record/RecordFormatException;

    .line 386
    const-string/jumbo v9, "Unable to determine record types"

    .line 385
    invoke-direct {v8, v9}, Lorg/apache/index/poi/hssf/record/RecordFormatException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 394
    .end local v1    # "illegalArgumentException":Ljava/lang/Exception;
    .restart local v2    # "key":Ljava/lang/Integer;
    .restart local v6    # "sid":I
    :cond_4
    invoke-static {v4}, Lorg/apache/index/poi/hssf/record/RecordFactory;->getRecordCreator(Ljava/lang/Class;)Lorg/apache/index/poi/hssf/record/RecordFactory$I_RecordCreator;

    move-result-object v8

    invoke-interface {v5, v2, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 368
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_0
.end method
