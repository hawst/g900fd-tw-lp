.class public final Lorg/apache/index/poi/hwpf/model/PAPFormattedDiskPage;
.super Lorg/apache/index/poi/hwpf/model/FormattedDiskPage;
.source "PAPFormattedDiskPage.java"


# static fields
.field private static final BX_SIZE:I = 0xd

.field private static final FC_SIZE:I = 0x4


# instance fields
.field private _dataStream:[B

.field private _overFlow:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lorg/apache/index/poi/hwpf/model/PAPX;",
            ">;"
        }
    .end annotation
.end field

.field private _papxList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lorg/apache/index/poi/hwpf/model/PAPX;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>([B)V
    .locals 1
    .param p1, "dataStream"    # [B

    .prologue
    .line 52
    invoke-direct {p0}, Lorg/apache/index/poi/hwpf/model/FormattedDiskPage;-><init>()V

    .line 47
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/apache/index/poi/hwpf/model/PAPFormattedDiskPage;->_papxList:Ljava/util/ArrayList;

    .line 54
    iput-object p1, p0, Lorg/apache/index/poi/hwpf/model/PAPFormattedDiskPage;->_dataStream:[B

    .line 55
    return-void
.end method

.method public constructor <init>([B[BIILorg/apache/index/poi/hwpf/model/TextPieceTable;)V
    .locals 8
    .param p1, "documentStream"    # [B
    .param p2, "dataStream"    # [B
    .param p3, "offset"    # I
    .param p4, "fcMin"    # I
    .param p5, "tpt"    # Lorg/apache/index/poi/hwpf/model/TextPieceTable;

    .prologue
    .line 62
    invoke-direct {p0, p1, p3}, Lorg/apache/index/poi/hwpf/model/FormattedDiskPage;-><init>([BI)V

    .line 47
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/apache/index/poi/hwpf/model/PAPFormattedDiskPage;->_papxList:Ljava/util/ArrayList;

    .line 63
    const/4 v6, 0x0

    .local v6, "x":I
    :goto_0
    iget v0, p0, Lorg/apache/index/poi/hwpf/model/PAPFormattedDiskPage;->_crun:I

    if-lt v6, v0, :cond_0

    .line 68
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/index/poi/hwpf/model/PAPFormattedDiskPage;->_fkp:[B

    .line 69
    iput-object p2, p0, Lorg/apache/index/poi/hwpf/model/PAPFormattedDiskPage;->_dataStream:[B

    .line 70
    return-void

    .line 64
    :cond_0
    invoke-virtual {p0, v6}, Lorg/apache/index/poi/hwpf/model/PAPFormattedDiskPage;->getStart(I)I

    move-result v1

    .line 65
    .local v1, "startAt":I
    invoke-virtual {p0, v6}, Lorg/apache/index/poi/hwpf/model/PAPFormattedDiskPage;->getEnd(I)I

    move-result v2

    .line 66
    .local v2, "endAt":I
    iget-object v7, p0, Lorg/apache/index/poi/hwpf/model/PAPFormattedDiskPage;->_papxList:Ljava/util/ArrayList;

    new-instance v0, Lorg/apache/index/poi/hwpf/model/PAPX;

    invoke-virtual {p0, v6}, Lorg/apache/index/poi/hwpf/model/PAPFormattedDiskPage;->getGrpprl(I)[B

    move-result-object v4

    move-object v3, p5

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lorg/apache/index/poi/hwpf/model/PAPX;-><init>(IILorg/apache/index/poi/hwpf/model/CharIndexTranslator;[B[B)V

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 63
    add-int/lit8 v6, v6, 0x1

    goto :goto_0
.end method


# virtual methods
.method public fill(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/apache/index/poi/hwpf/model/PAPX;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 79
    .local p1, "filler":Ljava/util/List;, "Ljava/util/List<Lorg/apache/index/poi/hwpf/model/PAPX;>;"
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/PAPFormattedDiskPage;->_papxList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 80
    return-void
.end method

.method protected getGrpprl(I)[B
    .locals 7
    .param p1, "index"    # I

    .prologue
    .line 117
    iget-object v3, p0, Lorg/apache/index/poi/hwpf/model/PAPFormattedDiskPage;->_fkp:[B

    iget v4, p0, Lorg/apache/index/poi/hwpf/model/PAPFormattedDiskPage;->_offset:I

    iget v5, p0, Lorg/apache/index/poi/hwpf/model/PAPFormattedDiskPage;->_crun:I

    add-int/lit8 v5, v5, 0x1

    mul-int/lit8 v5, v5, 0x4

    mul-int/lit8 v6, p1, 0xd

    add-int/2addr v5, v6

    add-int/2addr v4, v5

    invoke-static {v3, v4}, Lorg/apache/index/poi/util/LittleEndian;->getUnsignedByte([BI)I

    move-result v3

    mul-int/lit8 v1, v3, 0x2

    .line 118
    .local v1, "papxOffset":I
    iget-object v3, p0, Lorg/apache/index/poi/hwpf/model/PAPFormattedDiskPage;->_fkp:[B

    iget v4, p0, Lorg/apache/index/poi/hwpf/model/PAPFormattedDiskPage;->_offset:I

    add-int/2addr v4, v1

    invoke-static {v3, v4}, Lorg/apache/index/poi/util/LittleEndian;->getUnsignedByte([BI)I

    move-result v3

    mul-int/lit8 v2, v3, 0x2

    .line 119
    .local v2, "size":I
    if-nez v2, :cond_0

    .line 121
    iget-object v3, p0, Lorg/apache/index/poi/hwpf/model/PAPFormattedDiskPage;->_fkp:[B

    iget v4, p0, Lorg/apache/index/poi/hwpf/model/PAPFormattedDiskPage;->_offset:I

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v4, v1

    invoke-static {v3, v4}, Lorg/apache/index/poi/util/LittleEndian;->getUnsignedByte([BI)I

    move-result v3

    mul-int/lit8 v2, v3, 0x2

    .line 128
    :goto_0
    new-array v0, v2, [B

    .line 129
    .local v0, "papx":[B
    iget-object v3, p0, Lorg/apache/index/poi/hwpf/model/PAPFormattedDiskPage;->_fkp:[B

    iget v4, p0, Lorg/apache/index/poi/hwpf/model/PAPFormattedDiskPage;->_offset:I

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v4, v1

    const/4 v5, 0x0

    invoke-static {v3, v4, v0, v5, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 130
    return-object v0

    .line 125
    .end local v0    # "papx":[B
    :cond_0
    add-int/lit8 v2, v2, -0x1

    goto :goto_0
.end method

.method getOverflow()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lorg/apache/index/poi/hwpf/model/PAPX;",
            ">;"
        }
    .end annotation

    .prologue
    .line 96
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/PAPFormattedDiskPage;->_overFlow:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getPAPX(I)Lorg/apache/index/poi/hwpf/model/PAPX;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 106
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/model/PAPFormattedDiskPage;->_papxList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/index/poi/hwpf/model/PAPX;

    return-object v0
.end method

.method protected toByteArray(I)[B
    .locals 27
    .param p1, "fcMin"    # I

    .prologue
    .line 142
    const/16 v23, 0x200

    move/from16 v0, v23

    new-array v5, v0, [B

    .line 143
    .local v5, "buf":[B
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/index/poi/hwpf/model/PAPFormattedDiskPage;->_papxList:Ljava/util/ArrayList;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Ljava/util/ArrayList;->size()I

    move-result v20

    .line 144
    .local v20, "size":I
    const/4 v12, 0x0

    .line 145
    .local v12, "grpprlOffset":I
    const/4 v6, 0x0

    .line 146
    .local v6, "bxOffset":I
    const/4 v9, 0x0

    .line 147
    .local v9, "fcOffset":I
    const/16 v23, 0x0

    move/from16 v0, v23

    new-array v0, v0, [B

    move-object/from16 v16, v0

    .line 150
    .local v16, "lastGrpprl":[B
    const/16 v21, 0x4

    .line 152
    .local v21, "totalSize":I
    const/4 v14, 0x0

    .line 153
    .local v14, "index":I
    :goto_0
    move/from16 v0, v20

    if-lt v14, v0, :cond_2

    .line 199
    :goto_1
    move/from16 v0, v20

    if-eq v14, v0, :cond_0

    .line 201
    new-instance v23, Ljava/util/ArrayList;

    invoke-direct/range {v23 .. v23}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, v23

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/apache/index/poi/hwpf/model/PAPFormattedDiskPage;->_overFlow:Ljava/util/ArrayList;

    .line 202
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/index/poi/hwpf/model/PAPFormattedDiskPage;->_overFlow:Ljava/util/ArrayList;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/index/poi/hwpf/model/PAPFormattedDiskPage;->_papxList:Ljava/util/ArrayList;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    move/from16 v1, v20

    invoke-virtual {v0, v14, v1}, Ljava/util/ArrayList;->subList(II)Ljava/util/List;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 206
    :cond_0
    const/16 v23, 0x1ff

    int-to-byte v0, v14

    move/from16 v24, v0

    aput-byte v24, v5, v23

    .line 208
    mul-int/lit8 v23, v14, 0x4

    add-int/lit8 v6, v23, 0x4

    .line 209
    const/16 v12, 0x1ff

    .line 211
    const/16 v18, 0x0

    .line 212
    .local v18, "papx":Lorg/apache/index/poi/hwpf/model/PAPX;
    const/16 v23, 0x0

    move/from16 v0, v23

    new-array v0, v0, [B

    move-object/from16 v16, v0

    .line 213
    const/16 v22, 0x0

    .local v22, "x":I
    :goto_2
    move/from16 v0, v22

    if-lt v0, v14, :cond_7

    .line 285
    if-eqz v18, :cond_1

    .line 287
    invoke-virtual/range {v18 .. v18}, Lorg/apache/index/poi/hwpf/model/PAPX;->getEndBytes()I

    move-result v23

    add-int v23, v23, p1

    move/from16 v0, v23

    invoke-static {v5, v9, v0}, Lorg/apache/index/poi/util/LittleEndian;->putInt([BII)V

    .line 289
    :cond_1
    return-object v5

    .line 155
    .end local v18    # "papx":Lorg/apache/index/poi/hwpf/model/PAPX;
    .end local v22    # "x":I
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/index/poi/hwpf/model/PAPFormattedDiskPage;->_papxList:Ljava/util/ArrayList;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    invoke-virtual {v0, v14}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Lorg/apache/index/poi/hwpf/model/PAPX;

    invoke-virtual/range {v23 .. v23}, Lorg/apache/index/poi/hwpf/model/PAPX;->getGrpprl()[B

    move-result-object v10

    .line 156
    .local v10, "grpprl":[B
    array-length v11, v10

    .line 159
    .local v11, "grpprlLength":I
    const/16 v23, 0x1e8

    move/from16 v0, v23

    if-le v11, v0, :cond_3

    .line 161
    const/16 v11, 0x8

    .line 166
    :cond_3
    const/4 v4, 0x0

    .line 167
    .local v4, "addition":I
    move-object/from16 v0, v16

    invoke-static {v10, v0}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v23

    if-nez v23, :cond_4

    .line 169
    add-int/lit8 v23, v11, 0x11

    add-int/lit8 v4, v23, 0x1

    .line 176
    :goto_3
    add-int v21, v21, v4

    .line 180
    rem-int/lit8 v23, v14, 0x2

    move/from16 v0, v23

    add-int/lit16 v0, v0, 0x1ff

    move/from16 v23, v0

    move/from16 v0, v21

    move/from16 v1, v23

    if-le v0, v1, :cond_5

    .line 182
    sub-int v21, v21, v4

    .line 183
    goto/16 :goto_1

    .line 173
    :cond_4
    const/16 v4, 0x11

    goto :goto_3

    .line 187
    :cond_5
    rem-int/lit8 v23, v11, 0x2

    if-lez v23, :cond_6

    .line 189
    add-int/lit8 v21, v21, 0x1

    .line 195
    :goto_4
    move-object/from16 v16, v10

    .line 153
    add-int/lit8 v14, v14, 0x1

    goto/16 :goto_0

    .line 193
    :cond_6
    add-int/lit8 v21, v21, 0x2

    goto :goto_4

    .line 215
    .end local v4    # "addition":I
    .end local v10    # "grpprl":[B
    .end local v11    # "grpprlLength":I
    .restart local v18    # "papx":Lorg/apache/index/poi/hwpf/model/PAPX;
    .restart local v22    # "x":I
    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/index/poi/hwpf/model/PAPFormattedDiskPage;->_papxList:Ljava/util/ArrayList;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    .end local v18    # "papx":Lorg/apache/index/poi/hwpf/model/PAPX;
    check-cast v18, Lorg/apache/index/poi/hwpf/model/PAPX;

    .line 216
    .restart local v18    # "papx":Lorg/apache/index/poi/hwpf/model/PAPX;
    invoke-virtual/range {v18 .. v18}, Lorg/apache/index/poi/hwpf/model/PAPX;->getGrpprl()[B

    move-result-object v10

    .line 219
    .restart local v10    # "grpprl":[B
    array-length v0, v10

    move/from16 v23, v0

    const/16 v24, 0x1e8

    move/from16 v0, v23

    move/from16 v1, v24

    if-le v0, v1, :cond_a

    .line 222
    invoke-virtual/range {v18 .. v18}, Lorg/apache/index/poi/hwpf/model/PAPX;->getHugeGrpprlOffset()I

    move-result v13

    .line 223
    .local v13, "hugeGrpprlOffset":I
    const/16 v23, -0x1

    move/from16 v0, v23

    if-ne v13, v0, :cond_8

    .line 225
    new-instance v23, Ljava/lang/UnsupportedOperationException;

    .line 226
    const-string/jumbo v24, "This Paragraph has no dataStream storage."

    .line 225
    invoke-direct/range {v23 .. v24}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v23

    .line 231
    :cond_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/index/poi/hwpf/model/PAPFormattedDiskPage;->_dataStream:[B

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    invoke-static {v0, v13}, Lorg/apache/index/poi/util/LittleEndian;->getUShort([BI)I

    move-result v17

    .line 233
    .local v17, "maxHugeGrpprlSize":I
    array-length v0, v10

    move/from16 v23, v0

    add-int/lit8 v23, v23, -0x2

    move/from16 v0, v17

    move/from16 v1, v23

    if-ge v0, v1, :cond_9

    .line 234
    new-instance v23, Ljava/lang/UnsupportedOperationException;

    .line 235
    const-string/jumbo v24, "This Paragraph\'s dataStream storage is too small."

    .line 234
    invoke-direct/range {v23 .. v24}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v23

    .line 239
    :cond_9
    const/16 v23, 0x2

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/index/poi/hwpf/model/PAPFormattedDiskPage;->_dataStream:[B

    move-object/from16 v24, v0

    add-int/lit8 v25, v13, 0x2

    .line 240
    array-length v0, v10

    move/from16 v26, v0

    add-int/lit8 v26, v26, -0x2

    .line 239
    move/from16 v0, v23

    move-object/from16 v1, v24

    move/from16 v2, v25

    move/from16 v3, v26

    invoke-static {v10, v0, v1, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 241
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/index/poi/hwpf/model/PAPFormattedDiskPage;->_dataStream:[B

    move-object/from16 v23, v0

    array-length v0, v10

    move/from16 v24, v0

    add-int/lit8 v24, v24, -0x2

    move-object/from16 v0, v23

    move/from16 v1, v24

    invoke-static {v0, v13, v1}, Lorg/apache/index/poi/util/LittleEndian;->putUShort([BII)V

    .line 244
    const/16 v23, 0x0

    move/from16 v0, v23

    invoke-static {v10, v0}, Lorg/apache/index/poi/util/LittleEndian;->getUShort([BI)I

    move-result v15

    .line 245
    .local v15, "istd":I
    const/16 v23, 0x8

    move/from16 v0, v23

    new-array v10, v0, [B

    .line 246
    const/16 v23, 0x0

    move/from16 v0, v23

    invoke-static {v10, v0, v15}, Lorg/apache/index/poi/util/LittleEndian;->putUShort([BII)V

    .line 247
    const/16 v23, 0x2

    const/16 v24, 0x6646

    move/from16 v0, v23

    move/from16 v1, v24

    invoke-static {v10, v0, v1}, Lorg/apache/index/poi/util/LittleEndian;->putUShort([BII)V

    .line 248
    const/16 v23, 0x4

    move/from16 v0, v23

    invoke-static {v10, v0, v13}, Lorg/apache/index/poi/util/LittleEndian;->putInt([BII)V

    .line 251
    .end local v13    # "hugeGrpprlOffset":I
    .end local v15    # "istd":I
    .end local v17    # "maxHugeGrpprlSize":I
    :cond_a
    move-object/from16 v0, v16

    invoke-static {v0, v10}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v19

    .line 252
    .local v19, "same":Z
    if-nez v19, :cond_b

    .line 254
    array-length v0, v10

    move/from16 v23, v0

    array-length v0, v10

    move/from16 v24, v0

    rem-int/lit8 v24, v24, 0x2

    rsub-int/lit8 v24, v24, 0x2

    add-int v23, v23, v24

    sub-int v12, v12, v23

    .line 255
    rem-int/lit8 v23, v12, 0x2

    sub-int v12, v12, v23

    .line 257
    :cond_b
    invoke-virtual/range {v18 .. v18}, Lorg/apache/index/poi/hwpf/model/PAPX;->getStartBytes()I

    move-result v23

    add-int v23, v23, p1

    move/from16 v0, v23

    invoke-static {v5, v9, v0}, Lorg/apache/index/poi/util/LittleEndian;->putInt([BII)V

    .line 258
    div-int/lit8 v23, v12, 0x2

    move/from16 v0, v23

    int-to-byte v0, v0

    move/from16 v23, v0

    aput-byte v23, v5, v6

    .line 264
    if-nez v19, :cond_c

    .line 266
    move v7, v12

    .line 267
    .local v7, "copyOffset":I
    array-length v0, v10

    move/from16 v23, v0

    rem-int/lit8 v23, v23, 0x2

    if-lez v23, :cond_d

    .line 269
    add-int/lit8 v8, v7, 0x1

    .end local v7    # "copyOffset":I
    .local v8, "copyOffset":I
    array-length v0, v10

    move/from16 v23, v0

    add-int/lit8 v23, v23, 0x1

    div-int/lit8 v23, v23, 0x2

    move/from16 v0, v23

    int-to-byte v0, v0

    move/from16 v23, v0

    aput-byte v23, v5, v7

    move v7, v8

    .line 276
    .end local v8    # "copyOffset":I
    .restart local v7    # "copyOffset":I
    :goto_5
    const/16 v23, 0x0

    array-length v0, v10

    move/from16 v24, v0

    move/from16 v0, v23

    move/from16 v1, v24

    invoke-static {v10, v0, v5, v7, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 277
    move-object/from16 v16, v10

    .line 280
    .end local v7    # "copyOffset":I
    :cond_c
    add-int/lit8 v6, v6, 0xd

    .line 281
    add-int/lit8 v9, v9, 0x4

    .line 213
    add-int/lit8 v22, v22, 0x1

    goto/16 :goto_2

    .line 273
    .restart local v7    # "copyOffset":I
    :cond_d
    add-int/lit8 v7, v7, 0x1

    array-length v0, v10

    move/from16 v23, v0

    div-int/lit8 v23, v23, 0x2

    move/from16 v0, v23

    int-to-byte v0, v0

    move/from16 v23, v0

    aput-byte v23, v5, v7

    .line 274
    add-int/lit8 v7, v7, 0x1

    goto :goto_5
.end method
