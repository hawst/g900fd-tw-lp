.class public Lorg/apache/index/poi/ddf/EscherPropertyMetaData;
.super Ljava/lang/Object;
.source "EscherPropertyMetaData.java"


# static fields
.field public static final TYPE_ARRAY:B = 0x5t

.field public static final TYPE_BOOLEAN:B = 0x1t

.field public static final TYPE_RGB:B = 0x2t

.field public static final TYPE_SHAPEPATH:B = 0x3t

.field public static final TYPE_SIMPLE:B = 0x4t

.field public static final TYPE_UNKNOWN:B


# instance fields
.field private description:Ljava/lang/String;

.field private type:B


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "description"    # Ljava/lang/String;

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    iput-object p1, p0, Lorg/apache/index/poi/ddf/EscherPropertyMetaData;->description:Ljava/lang/String;

    .line 46
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;B)V
    .locals 0
    .param p1, "description"    # Ljava/lang/String;
    .param p2, "type"    # B

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    iput-object p1, p0, Lorg/apache/index/poi/ddf/EscherPropertyMetaData;->description:Ljava/lang/String;

    .line 56
    iput-byte p2, p0, Lorg/apache/index/poi/ddf/EscherPropertyMetaData;->type:B

    .line 57
    return-void
.end method


# virtual methods
.method public getDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lorg/apache/index/poi/ddf/EscherPropertyMetaData;->description:Ljava/lang/String;

    return-object v0
.end method

.method public getType()B
    .locals 1

    .prologue
    .line 66
    iget-byte v0, p0, Lorg/apache/index/poi/ddf/EscherPropertyMetaData;->type:B

    return v0
.end method
