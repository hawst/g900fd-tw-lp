.class public final Lorg/apache/index/poi/hssf/record/SSTRecord;
.super Lorg/apache/index/poi/hssf/record/cont/ContinuableRecord;
.source "SSTRecord.java"


# static fields
.field private static final EMPTY_STRING:Lorg/apache/index/poi/hssf/record/common/UnicodeString;

.field public static final sid:S = 0xfcs


# instance fields
.field bucketAbsoluteOffsets:[I

.field bucketRelativeOffsets:[I

.field private deserializer:Lorg/apache/index/poi/hssf/record/SSTDeserializer;

.field private field_1_num_strings:I

.field private field_2_num_unique_strings:I

.field private field_3_strings:Lorg/apache/index/poi/util/IntMapper;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/index/poi/util/IntMapper",
            "<",
            "Lorg/apache/index/poi/hssf/record/common/UnicodeString;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 48
    new-instance v0, Lorg/apache/index/poi/hssf/record/common/UnicodeString;

    const-string/jumbo v1, ""

    invoke-direct {v0, v1}, Lorg/apache/index/poi/hssf/record/common/UnicodeString;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/index/poi/hssf/record/SSTRecord;->EMPTY_STRING:Lorg/apache/index/poi/hssf/record/common/UnicodeString;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 74
    invoke-direct {p0}, Lorg/apache/index/poi/hssf/record/cont/ContinuableRecord;-><init>()V

    .line 79
    new-instance v0, Lorg/apache/index/poi/hssf/record/SSTDeserializer;

    invoke-direct {v0}, Lorg/apache/index/poi/hssf/record/SSTDeserializer;-><init>()V

    iput-object v0, p0, Lorg/apache/index/poi/hssf/record/SSTRecord;->deserializer:Lorg/apache/index/poi/hssf/record/SSTDeserializer;

    .line 80
    return-void
.end method

.method public constructor <init>(Lorg/apache/index/poi/hssf/record/RecordInputStream;Lcom/samsung/index/ITextContentObs;)V
    .locals 2
    .param p1, "in"    # Lorg/apache/index/poi/hssf/record/RecordInputStream;
    .param p2, "indexWriterhssf"    # Lcom/samsung/index/ITextContentObs;

    .prologue
    .line 252
    invoke-direct {p0}, Lorg/apache/index/poi/hssf/record/cont/ContinuableRecord;-><init>()V

    .line 256
    invoke-virtual {p1}, Lorg/apache/index/poi/hssf/record/RecordInputStream;->readInt()I

    move-result v0

    iput v0, p0, Lorg/apache/index/poi/hssf/record/SSTRecord;->field_1_num_strings:I

    .line 257
    invoke-virtual {p1}, Lorg/apache/index/poi/hssf/record/RecordInputStream;->readInt()I

    move-result v0

    iput v0, p0, Lorg/apache/index/poi/hssf/record/SSTRecord;->field_2_num_unique_strings:I

    .line 258
    new-instance v0, Lorg/apache/index/poi/util/IntMapper;

    invoke-direct {v0}, Lorg/apache/index/poi/util/IntMapper;-><init>()V

    iput-object v0, p0, Lorg/apache/index/poi/hssf/record/SSTRecord;->field_3_strings:Lorg/apache/index/poi/util/IntMapper;

    .line 259
    new-instance v0, Lorg/apache/index/poi/hssf/record/SSTDeserializer;

    invoke-direct {v0}, Lorg/apache/index/poi/hssf/record/SSTDeserializer;-><init>()V

    iput-object v0, p0, Lorg/apache/index/poi/hssf/record/SSTRecord;->deserializer:Lorg/apache/index/poi/hssf/record/SSTDeserializer;

    .line 260
    iget-object v0, p0, Lorg/apache/index/poi/hssf/record/SSTRecord;->deserializer:Lorg/apache/index/poi/hssf/record/SSTDeserializer;

    invoke-virtual {v0, p2}, Lorg/apache/index/poi/hssf/record/SSTDeserializer;->setIndexWriterhssf(Lcom/samsung/index/ITextContentObs;)V

    .line 261
    iget-object v0, p0, Lorg/apache/index/poi/hssf/record/SSTRecord;->deserializer:Lorg/apache/index/poi/hssf/record/SSTDeserializer;

    iget v1, p0, Lorg/apache/index/poi/hssf/record/SSTRecord;->field_2_num_unique_strings:I

    invoke-virtual {v0, v1, p1}, Lorg/apache/index/poi/hssf/record/SSTDeserializer;->manufactureStrings(ILorg/apache/index/poi/hssf/record/RecordInputStream;)V

    .line 262
    return-void
.end method


# virtual methods
.method public addString(Lorg/apache/index/poi/hssf/record/common/UnicodeString;)I
    .locals 4
    .param p1, "string"    # Lorg/apache/index/poi/hssf/record/common/UnicodeString;

    .prologue
    .line 91
    iget v3, p0, Lorg/apache/index/poi/hssf/record/SSTRecord;->field_1_num_strings:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lorg/apache/index/poi/hssf/record/SSTRecord;->field_1_num_strings:I

    .line 92
    if-nez p1, :cond_0

    sget-object v2, Lorg/apache/index/poi/hssf/record/SSTRecord;->EMPTY_STRING:Lorg/apache/index/poi/hssf/record/common/UnicodeString;

    .line 95
    .local v2, "ucs":Lorg/apache/index/poi/hssf/record/common/UnicodeString;
    :goto_0
    iget-object v3, p0, Lorg/apache/index/poi/hssf/record/SSTRecord;->field_3_strings:Lorg/apache/index/poi/util/IntMapper;

    invoke-virtual {v3, v2}, Lorg/apache/index/poi/util/IntMapper;->getIndex(Ljava/lang/Object;)I

    move-result v0

    .line 97
    .local v0, "index":I
    const/4 v3, -0x1

    if-eq v0, v3, :cond_1

    .line 98
    move v1, v0

    .line 107
    .local v1, "rval":I
    :goto_1
    return v1

    .end local v0    # "index":I
    .end local v1    # "rval":I
    .end local v2    # "ucs":Lorg/apache/index/poi/hssf/record/common/UnicodeString;
    :cond_0
    move-object v2, p1

    .line 93
    goto :goto_0

    .line 102
    .restart local v0    # "index":I
    .restart local v2    # "ucs":Lorg/apache/index/poi/hssf/record/common/UnicodeString;
    :cond_1
    iget-object v3, p0, Lorg/apache/index/poi/hssf/record/SSTRecord;->field_3_strings:Lorg/apache/index/poi/util/IntMapper;

    invoke-virtual {v3}, Lorg/apache/index/poi/util/IntMapper;->size()I

    move-result v1

    .line 103
    .restart local v1    # "rval":I
    iget v3, p0, Lorg/apache/index/poi/hssf/record/SSTRecord;->field_2_num_unique_strings:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lorg/apache/index/poi/hssf/record/SSTRecord;->field_2_num_unique_strings:I

    goto :goto_1
.end method

.method public calcExtSSTRecordSize()I
    .locals 1

    .prologue
    .line 327
    iget-object v0, p0, Lorg/apache/index/poi/hssf/record/SSTRecord;->field_3_strings:Lorg/apache/index/poi/util/IntMapper;

    invoke-virtual {v0}, Lorg/apache/index/poi/util/IntMapper;->size()I

    move-result v0

    invoke-static {v0}, Lorg/apache/index/poi/hssf/record/ExtSSTRecord;->getRecordSizeForStrings(I)I

    move-result v0

    return v0
.end method

.method countStrings()I
    .locals 1

    .prologue
    .line 278
    iget-object v0, p0, Lorg/apache/index/poi/hssf/record/SSTRecord;->field_3_strings:Lorg/apache/index/poi/util/IntMapper;

    invoke-virtual {v0}, Lorg/apache/index/poi/util/IntMapper;->size()I

    move-result v0

    return v0
.end method

.method getDeserializer()Lorg/apache/index/poi/hssf/record/SSTDeserializer;
    .locals 1

    .prologue
    .line 289
    iget-object v0, p0, Lorg/apache/index/poi/hssf/record/SSTRecord;->deserializer:Lorg/apache/index/poi/hssf/record/SSTDeserializer;

    return-object v0
.end method

.method public getNumStrings()I
    .locals 1

    .prologue
    .line 115
    iget v0, p0, Lorg/apache/index/poi/hssf/record/SSTRecord;->field_1_num_strings:I

    return v0
.end method

.method public getNumUniqueStrings()I
    .locals 1

    .prologue
    .line 123
    iget v0, p0, Lorg/apache/index/poi/hssf/record/SSTRecord;->field_2_num_unique_strings:I

    return v0
.end method

.method public getSid()S
    .locals 1

    .prologue
    .line 171
    const/16 v0, 0xfc

    return v0
.end method

.method public getString(I)Lorg/apache/index/poi/hssf/record/common/UnicodeString;
    .locals 1
    .param p1, "id"    # I

    .prologue
    .line 137
    iget-object v0, p0, Lorg/apache/index/poi/hssf/record/SSTRecord;->field_3_strings:Lorg/apache/index/poi/util/IntMapper;

    invoke-virtual {v0, p1}, Lorg/apache/index/poi/util/IntMapper;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/index/poi/hssf/record/common/UnicodeString;

    return-object v0
.end method

.method public getStringListSize()I
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, Lorg/apache/index/poi/hssf/record/SSTRecord;->field_3_strings:Lorg/apache/index/poi/util/IntMapper;

    invoke-virtual {v0}, Lorg/apache/index/poi/util/IntMapper;->size()I

    move-result v0

    return v0
.end method

.method getStrings()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Lorg/apache/index/poi/hssf/record/common/UnicodeString;",
            ">;"
        }
    .end annotation

    .prologue
    .line 271
    iget-object v0, p0, Lorg/apache/index/poi/hssf/record/SSTRecord;->field_3_strings:Lorg/apache/index/poi/util/IntMapper;

    invoke-virtual {v0}, Lorg/apache/index/poi/util/IntMapper;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method protected serialize(Lorg/apache/index/poi/hssf/record/cont/ContinuableRecordOutput;)V
    .locals 4
    .param p1, "out"    # Lorg/apache/index/poi/hssf/record/cont/ContinuableRecordOutput;

    .prologue
    .line 282
    new-instance v0, Lorg/apache/index/poi/hssf/record/SSTSerializer;

    iget-object v1, p0, Lorg/apache/index/poi/hssf/record/SSTRecord;->field_3_strings:Lorg/apache/index/poi/util/IntMapper;

    invoke-virtual {p0}, Lorg/apache/index/poi/hssf/record/SSTRecord;->getNumStrings()I

    move-result v2

    invoke-virtual {p0}, Lorg/apache/index/poi/hssf/record/SSTRecord;->getNumUniqueStrings()I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/index/poi/hssf/record/SSTSerializer;-><init>(Lorg/apache/index/poi/util/IntMapper;II)V

    .line 283
    .local v0, "serializer":Lorg/apache/index/poi/hssf/record/SSTSerializer;
    invoke-virtual {v0, p1}, Lorg/apache/index/poi/hssf/record/SSTSerializer;->serialize(Lorg/apache/index/poi/hssf/record/cont/ContinuableRecordOutput;)V

    .line 284
    invoke-virtual {v0}, Lorg/apache/index/poi/hssf/record/SSTSerializer;->getBucketAbsoluteOffsets()[I

    move-result-object v1

    iput-object v1, p0, Lorg/apache/index/poi/hssf/record/SSTRecord;->bucketAbsoluteOffsets:[I

    .line 285
    invoke-virtual {v0}, Lorg/apache/index/poi/hssf/record/SSTSerializer;->getBucketRelativeOffsets()[I

    move-result-object v1

    iput-object v1, p0, Lorg/apache/index/poi/hssf/record/SSTRecord;->bucketRelativeOffsets:[I

    .line 286
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 153
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 155
    .local v0, "buffer":Ljava/lang/StringBuffer;
    const-string/jumbo v3, "[SST]\n"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 156
    const-string/jumbo v3, "    .numstrings     = "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    .line 157
    invoke-virtual {p0}, Lorg/apache/index/poi/hssf/record/SSTRecord;->getNumStrings()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    const-string/jumbo v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 158
    const-string/jumbo v3, "    .uniquestrings  = "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    .line 159
    invoke-virtual {p0}, Lorg/apache/index/poi/hssf/record/SSTRecord;->getNumUniqueStrings()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    const-string/jumbo v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 160
    const/4 v1, 0x0

    .local v1, "k":I
    :goto_0
    iget-object v3, p0, Lorg/apache/index/poi/hssf/record/SSTRecord;->field_3_strings:Lorg/apache/index/poi/util/IntMapper;

    invoke-virtual {v3}, Lorg/apache/index/poi/util/IntMapper;->size()I

    move-result v3

    if-lt v1, v3, :cond_0

    .line 166
    const-string/jumbo v3, "[/SST]\n"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 167
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3

    .line 162
    :cond_0
    iget-object v3, p0, Lorg/apache/index/poi/hssf/record/SSTRecord;->field_3_strings:Lorg/apache/index/poi/util/IntMapper;

    invoke-virtual {v3, v1}, Lorg/apache/index/poi/util/IntMapper;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/index/poi/hssf/record/common/UnicodeString;

    .line 163
    .local v2, "s":Lorg/apache/index/poi/hssf/record/common/UnicodeString;
    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "    .string_"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "      = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    .line 164
    invoke-virtual {v2}, Lorg/apache/index/poi/hssf/record/common/UnicodeString;->getDebugInfo()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    const-string/jumbo v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 160
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method
