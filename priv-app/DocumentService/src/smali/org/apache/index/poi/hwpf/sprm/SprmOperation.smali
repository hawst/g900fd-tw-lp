.class public final Lorg/apache/index/poi/hwpf/sprm/SprmOperation;
.super Ljava/lang/Object;
.source "SprmOperation.java"


# static fields
.field private static final LONG_SPRM_PARAGRAPH:S = -0x39ebs

.field private static final LONG_SPRM_TABLE:S = -0x29f8s

.field private static final OP_BITFIELD:Lorg/apache/index/poi/util/BitField;

.field public static final PAP_TYPE:I = 0x1

.field private static final SIZECODE_BITFIELD:Lorg/apache/index/poi/util/BitField;

.field public static final TAP_TYPE:I = 0x5

.field private static final TYPE_BITFIELD:Lorg/apache/index/poi/util/BitField;


# instance fields
.field private _gOffset:I

.field private _grpprl:[B

.field private _operation:I

.field private _size:I

.field private _sizeCode:I

.field private _type:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    const/16 v0, 0x1ff

    invoke-static {v0}, Lorg/apache/index/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/index/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/index/poi/hwpf/sprm/SprmOperation;->OP_BITFIELD:Lorg/apache/index/poi/util/BitField;

    .line 36
    const/16 v0, 0x1c00

    invoke-static {v0}, Lorg/apache/index/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/index/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/index/poi/hwpf/sprm/SprmOperation;->TYPE_BITFIELD:Lorg/apache/index/poi/util/BitField;

    .line 37
    const v0, 0xe000

    invoke-static {v0}, Lorg/apache/index/poi/util/BitFieldFactory;->getInstance(I)Lorg/apache/index/poi/util/BitField;

    move-result-object v0

    sput-object v0, Lorg/apache/index/poi/hwpf/sprm/SprmOperation;->SIZECODE_BITFIELD:Lorg/apache/index/poi/util/BitField;

    .line 43
    return-void
.end method

.method public constructor <init>([BI)V
    .locals 2
    .param p1, "grpprl"    # [B
    .param p2, "offset"    # I

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    iput-object p1, p0, Lorg/apache/index/poi/hwpf/sprm/SprmOperation;->_grpprl:[B

    .line 56
    invoke-static {p1, p2}, Lorg/apache/index/poi/util/LittleEndian;->getShort([BI)S

    move-result v0

    .line 58
    .local v0, "sprmStart":S
    add-int/lit8 v1, p2, 0x2

    iput v1, p0, Lorg/apache/index/poi/hwpf/sprm/SprmOperation;->_gOffset:I

    .line 60
    sget-object v1, Lorg/apache/index/poi/hwpf/sprm/SprmOperation;->OP_BITFIELD:Lorg/apache/index/poi/util/BitField;

    invoke-virtual {v1, v0}, Lorg/apache/index/poi/util/BitField;->getValue(I)I

    move-result v1

    iput v1, p0, Lorg/apache/index/poi/hwpf/sprm/SprmOperation;->_operation:I

    .line 61
    sget-object v1, Lorg/apache/index/poi/hwpf/sprm/SprmOperation;->TYPE_BITFIELD:Lorg/apache/index/poi/util/BitField;

    invoke-virtual {v1, v0}, Lorg/apache/index/poi/util/BitField;->getValue(I)I

    move-result v1

    iput v1, p0, Lorg/apache/index/poi/hwpf/sprm/SprmOperation;->_type:I

    .line 62
    sget-object v1, Lorg/apache/index/poi/hwpf/sprm/SprmOperation;->SIZECODE_BITFIELD:Lorg/apache/index/poi/util/BitField;

    invoke-virtual {v1, v0}, Lorg/apache/index/poi/util/BitField;->getValue(I)I

    move-result v1

    iput v1, p0, Lorg/apache/index/poi/hwpf/sprm/SprmOperation;->_sizeCode:I

    .line 63
    invoke-direct {p0, v0}, Lorg/apache/index/poi/hwpf/sprm/SprmOperation;->initSize(S)I

    move-result v1

    iput v1, p0, Lorg/apache/index/poi/hwpf/sprm/SprmOperation;->_size:I

    .line 64
    return-void
.end method

.method public static getOperationFromOpcode(S)I
    .locals 1
    .param p0, "opcode"    # S

    .prologue
    .line 68
    sget-object v0, Lorg/apache/index/poi/hwpf/sprm/SprmOperation;->OP_BITFIELD:Lorg/apache/index/poi/util/BitField;

    invoke-virtual {v0, p0}, Lorg/apache/index/poi/util/BitField;->getValue(I)I

    move-result v0

    return v0
.end method

.method public static getTypeFromOpcode(S)I
    .locals 1
    .param p0, "opcode"    # S

    .prologue
    .line 73
    sget-object v0, Lorg/apache/index/poi/hwpf/sprm/SprmOperation;->TYPE_BITFIELD:Lorg/apache/index/poi/util/BitField;

    invoke-virtual {v0, p0}, Lorg/apache/index/poi/util/BitField;->getValue(I)I

    move-result v0

    return v0
.end method

.method private initSize(S)I
    .locals 4
    .param p1, "sprm"    # S

    .prologue
    .line 139
    iget v1, p0, Lorg/apache/index/poi/hwpf/sprm/SprmOperation;->_sizeCode:I

    packed-switch v1, :pswitch_data_0

    .line 161
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v2, "SPRM contains an invalid size code"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 143
    :pswitch_0
    const/4 v0, 0x3

    .line 159
    :goto_0
    return v0

    .line 147
    :pswitch_1
    const/4 v0, 0x4

    goto :goto_0

    .line 149
    :pswitch_2
    const/4 v0, 0x6

    goto :goto_0

    .line 151
    :pswitch_3
    const/16 v1, -0x29f8

    if-eq p1, v1, :cond_0

    const/16 v1, -0x39eb

    if-ne p1, v1, :cond_1

    .line 153
    :cond_0
    const v1, 0xffff

    iget-object v2, p0, Lorg/apache/index/poi/hwpf/sprm/SprmOperation;->_grpprl:[B

    iget v3, p0, Lorg/apache/index/poi/hwpf/sprm/SprmOperation;->_gOffset:I

    invoke-static {v2, v3}, Lorg/apache/index/poi/util/LittleEndian;->getShort([BI)S

    move-result v2

    and-int/2addr v1, v2

    add-int/lit8 v0, v1, 0x3

    .line 154
    .local v0, "retVal":I
    iget v1, p0, Lorg/apache/index/poi/hwpf/sprm/SprmOperation;->_gOffset:I

    add-int/lit8 v1, v1, 0x2

    iput v1, p0, Lorg/apache/index/poi/hwpf/sprm/SprmOperation;->_gOffset:I

    goto :goto_0

    .line 157
    .end local v0    # "retVal":I
    :cond_1
    iget-object v1, p0, Lorg/apache/index/poi/hwpf/sprm/SprmOperation;->_grpprl:[B

    iget v2, p0, Lorg/apache/index/poi/hwpf/sprm/SprmOperation;->_gOffset:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lorg/apache/index/poi/hwpf/sprm/SprmOperation;->_gOffset:I

    aget-byte v1, v1, v2

    and-int/lit16 v1, v1, 0xff

    add-int/lit8 v0, v1, 0x3

    goto :goto_0

    .line 159
    :pswitch_4
    const/4 v0, 0x5

    goto :goto_0

    .line 139
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method


# virtual methods
.method public getGrpprl()[B
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lorg/apache/index/poi/hwpf/sprm/SprmOperation;->_grpprl:[B

    return-object v0
.end method

.method public getGrpprlOffset()I
    .locals 1

    .prologue
    .line 88
    iget v0, p0, Lorg/apache/index/poi/hwpf/sprm/SprmOperation;->_gOffset:I

    return v0
.end method

.method public getOperand()I
    .locals 8

    .prologue
    const/4 v6, 0x4

    const/4 v7, 0x0

    .line 92
    iget v4, p0, Lorg/apache/index/poi/hwpf/sprm/SprmOperation;->_sizeCode:I

    packed-switch v4, :pswitch_data_0

    .line 120
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v5, "SPRM contains an invalid size code"

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 96
    :pswitch_0
    iget-object v4, p0, Lorg/apache/index/poi/hwpf/sprm/SprmOperation;->_grpprl:[B

    iget v5, p0, Lorg/apache/index/poi/hwpf/sprm/SprmOperation;->_gOffset:I

    aget-byte v4, v4, v5

    .line 118
    :goto_0
    return v4

    .line 100
    :pswitch_1
    iget-object v4, p0, Lorg/apache/index/poi/hwpf/sprm/SprmOperation;->_grpprl:[B

    iget v5, p0, Lorg/apache/index/poi/hwpf/sprm/SprmOperation;->_gOffset:I

    invoke-static {v4, v5}, Lorg/apache/index/poi/util/LittleEndian;->getShort([BI)S

    move-result v4

    goto :goto_0

    .line 102
    :pswitch_2
    iget-object v4, p0, Lorg/apache/index/poi/hwpf/sprm/SprmOperation;->_grpprl:[B

    iget v5, p0, Lorg/apache/index/poi/hwpf/sprm/SprmOperation;->_gOffset:I

    invoke-static {v4, v5}, Lorg/apache/index/poi/util/LittleEndian;->getInt([BI)I

    move-result v4

    goto :goto_0

    .line 104
    :pswitch_3
    iget-object v4, p0, Lorg/apache/index/poi/hwpf/sprm/SprmOperation;->_grpprl:[B

    iget v5, p0, Lorg/apache/index/poi/hwpf/sprm/SprmOperation;->_gOffset:I

    add-int/lit8 v5, v5, 0x1

    aget-byte v2, v4, v5

    .line 106
    .local v2, "operandLength":B
    new-array v0, v6, [B

    .line 107
    .local v0, "codeBytes":[B
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-lt v1, v2, :cond_0

    .line 111
    invoke-static {v0, v7}, Lorg/apache/index/poi/util/LittleEndian;->getInt([BI)I

    move-result v4

    goto :goto_0

    .line 108
    :cond_0
    iget v4, p0, Lorg/apache/index/poi/hwpf/sprm/SprmOperation;->_gOffset:I

    add-int/2addr v4, v1

    iget-object v5, p0, Lorg/apache/index/poi/hwpf/sprm/SprmOperation;->_grpprl:[B

    array-length v5, v5

    if-ge v4, v5, :cond_1

    .line 109
    iget-object v4, p0, Lorg/apache/index/poi/hwpf/sprm/SprmOperation;->_grpprl:[B

    iget v5, p0, Lorg/apache/index/poi/hwpf/sprm/SprmOperation;->_gOffset:I

    add-int/lit8 v5, v5, 0x1

    add-int/2addr v5, v1

    aget-byte v4, v4, v5

    aput-byte v4, v0, v1

    .line 107
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 113
    .end local v0    # "codeBytes":[B
    .end local v1    # "i":I
    .end local v2    # "operandLength":B
    :pswitch_4
    new-array v3, v6, [B

    .line 114
    .local v3, "threeByteInt":[B
    iget-object v4, p0, Lorg/apache/index/poi/hwpf/sprm/SprmOperation;->_grpprl:[B

    iget v5, p0, Lorg/apache/index/poi/hwpf/sprm/SprmOperation;->_gOffset:I

    aget-byte v4, v4, v5

    aput-byte v4, v3, v7

    .line 115
    const/4 v4, 0x1

    iget-object v5, p0, Lorg/apache/index/poi/hwpf/sprm/SprmOperation;->_grpprl:[B

    iget v6, p0, Lorg/apache/index/poi/hwpf/sprm/SprmOperation;->_gOffset:I

    add-int/lit8 v6, v6, 0x1

    aget-byte v5, v5, v6

    aput-byte v5, v3, v4

    .line 116
    const/4 v4, 0x2

    iget-object v5, p0, Lorg/apache/index/poi/hwpf/sprm/SprmOperation;->_grpprl:[B

    iget v6, p0, Lorg/apache/index/poi/hwpf/sprm/SprmOperation;->_gOffset:I

    add-int/lit8 v6, v6, 0x2

    aget-byte v5, v5, v6

    aput-byte v5, v3, v4

    .line 117
    const/4 v4, 0x3

    aput-byte v7, v3, v4

    .line 118
    invoke-static {v3, v7}, Lorg/apache/index/poi/util/LittleEndian;->getInt([BI)I

    move-result v4

    goto :goto_0

    .line 92
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public getOperation()I
    .locals 1

    .prologue
    .line 83
    iget v0, p0, Lorg/apache/index/poi/hwpf/sprm/SprmOperation;->_operation:I

    return v0
.end method

.method public getSizeCode()I
    .locals 1

    .prologue
    .line 125
    iget v0, p0, Lorg/apache/index/poi/hwpf/sprm/SprmOperation;->_sizeCode:I

    return v0
.end method

.method public getType()I
    .locals 1

    .prologue
    .line 78
    iget v0, p0, Lorg/apache/index/poi/hwpf/sprm/SprmOperation;->_type:I

    return v0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 130
    iget v0, p0, Lorg/apache/index/poi/hwpf/sprm/SprmOperation;->_size:I

    return v0
.end method
