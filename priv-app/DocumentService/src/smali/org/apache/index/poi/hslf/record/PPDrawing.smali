.class public final Lorg/apache/index/poi/hslf/record/PPDrawing;
.super Lorg/apache/index/poi/hslf/record/RecordAtom;
.source "PPDrawing.java"


# instance fields
.field private _header:[B

.field private _type:J

.field private childRecords:[Lorg/apache/index/poi/ddf/EscherRecord;

.field private dg:Lorg/apache/index/poi/ddf/EscherDgRecord;

.field private textboxWrappers:[Lorg/apache/index/poi/hslf/record/EscherTextboxWrapper;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 115
    invoke-direct {p0}, Lorg/apache/index/poi/hslf/record/RecordAtom;-><init>()V

    .line 116
    const/16 v0, 0x8

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/apache/index/poi/hslf/record/PPDrawing;->_header:[B

    .line 117
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/PPDrawing;->_header:[B

    const/16 v1, 0xf

    invoke-static {v0, v3, v1}, Lorg/apache/index/poi/util/LittleEndian;->putUShort([BII)V

    .line 118
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/PPDrawing;->_header:[B

    const/4 v1, 0x2

    sget-object v2, Lorg/apache/index/poi/hslf/record/RecordTypes;->PPDrawing:Lorg/apache/index/poi/hslf/record/RecordTypes$Type;

    iget v2, v2, Lorg/apache/index/poi/hslf/record/RecordTypes$Type;->typeID:I

    invoke-static {v0, v1, v2}, Lorg/apache/index/poi/util/LittleEndian;->putUShort([BII)V

    .line 119
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/PPDrawing;->_header:[B

    const/4 v1, 0x4

    invoke-static {v0, v1, v3}, Lorg/apache/index/poi/util/LittleEndian;->putInt([BII)V

    .line 121
    new-array v0, v3, [Lorg/apache/index/poi/hslf/record/EscherTextboxWrapper;

    iput-object v0, p0, Lorg/apache/index/poi/hslf/record/PPDrawing;->textboxWrappers:[Lorg/apache/index/poi/hslf/record/EscherTextboxWrapper;

    .line 122
    invoke-direct {p0}, Lorg/apache/index/poi/hslf/record/PPDrawing;->create()V

    .line 123
    return-void
.end method

.method protected constructor <init>([BII)V
    .locals 11
    .param p1, "source"    # [B
    .param p2, "start"    # I
    .param p3, "len"    # I

    .prologue
    const/4 v10, 0x0

    const/16 v3, 0x8

    .line 79
    invoke-direct {p0}, Lorg/apache/index/poi/hslf/record/RecordAtom;-><init>()V

    .line 81
    new-array v0, v3, [B

    iput-object v0, p0, Lorg/apache/index/poi/hslf/record/PPDrawing;->_header:[B

    .line 82
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/PPDrawing;->_header:[B

    invoke-static {p1, p2, v0, v10, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 85
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/PPDrawing;->_header:[B

    const/4 v4, 0x2

    invoke-static {v0, v4}, Lorg/apache/index/poi/util/LittleEndian;->getUShort([BI)I

    move-result v0

    int-to-long v8, v0

    iput-wide v8, p0, Lorg/apache/index/poi/hslf/record/PPDrawing;->_type:J

    .line 88
    new-array v2, p3, [B

    .line 89
    .local v2, "contents":[B
    invoke-static {p1, p2, v2, v10, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 93
    new-instance v1, Lorg/apache/index/poi/ddf/DefaultEscherRecordFactory;

    invoke-direct {v1}, Lorg/apache/index/poi/ddf/DefaultEscherRecordFactory;-><init>()V

    .line 94
    .local v1, "erf":Lorg/apache/index/poi/ddf/DefaultEscherRecordFactory;
    new-instance v5, Ljava/util/Vector;

    invoke-direct {v5}, Ljava/util/Vector;-><init>()V

    .line 95
    .local v5, "escherChildren":Ljava/util/Vector;
    add-int/lit8 v4, p3, -0x8

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lorg/apache/index/poi/hslf/record/PPDrawing;->findEscherChildren(Lorg/apache/index/poi/ddf/DefaultEscherRecordFactory;[BIILjava/util/Vector;)V

    .line 97
    invoke-virtual {v5}, Ljava/util/Vector;->size()I

    move-result v0

    new-array v0, v0, [Lorg/apache/index/poi/ddf/EscherRecord;

    iput-object v0, p0, Lorg/apache/index/poi/hslf/record/PPDrawing;->childRecords:[Lorg/apache/index/poi/ddf/EscherRecord;

    .line 98
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_0
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/PPDrawing;->childRecords:[Lorg/apache/index/poi/ddf/EscherRecord;

    array-length v0, v0

    if-lt v6, v0, :cond_0

    .line 103
    new-instance v7, Ljava/util/Vector;

    invoke-direct {v7}, Ljava/util/Vector;-><init>()V

    .line 104
    .local v7, "textboxes":Ljava/util/Vector;
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/PPDrawing;->childRecords:[Lorg/apache/index/poi/ddf/EscherRecord;

    invoke-direct {p0, v0, v7}, Lorg/apache/index/poi/hslf/record/PPDrawing;->findEscherTextboxRecord([Lorg/apache/index/poi/ddf/EscherRecord;Ljava/util/Vector;)V

    .line 105
    invoke-virtual {v7}, Ljava/util/Vector;->size()I

    move-result v0

    new-array v0, v0, [Lorg/apache/index/poi/hslf/record/EscherTextboxWrapper;

    iput-object v0, p0, Lorg/apache/index/poi/hslf/record/PPDrawing;->textboxWrappers:[Lorg/apache/index/poi/hslf/record/EscherTextboxWrapper;

    .line 106
    const/4 v6, 0x0

    :goto_1
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/PPDrawing;->textboxWrappers:[Lorg/apache/index/poi/hslf/record/EscherTextboxWrapper;

    array-length v0, v0

    if-lt v6, v0, :cond_1

    .line 109
    return-void

    .line 99
    .end local v7    # "textboxes":Ljava/util/Vector;
    :cond_0
    iget-object v3, p0, Lorg/apache/index/poi/hslf/record/PPDrawing;->childRecords:[Lorg/apache/index/poi/ddf/EscherRecord;

    invoke-virtual {v5, v6}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/index/poi/ddf/EscherRecord;

    aput-object v0, v3, v6

    .line 98
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 107
    .restart local v7    # "textboxes":Ljava/util/Vector;
    :cond_1
    iget-object v3, p0, Lorg/apache/index/poi/hslf/record/PPDrawing;->textboxWrappers:[Lorg/apache/index/poi/hslf/record/EscherTextboxWrapper;

    invoke-virtual {v7, v6}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/index/poi/hslf/record/EscherTextboxWrapper;

    aput-object v0, v3, v6

    .line 106
    add-int/lit8 v6, v6, 0x1

    goto :goto_1
.end method

.method private create()V
    .locals 11

    .prologue
    const/16 v9, -0xffc

    const/4 v10, 0x1

    const/16 v8, 0xf

    .line 204
    new-instance v1, Lorg/apache/index/poi/ddf/EscherContainerRecord;

    invoke-direct {v1}, Lorg/apache/index/poi/ddf/EscherContainerRecord;-><init>()V

    .line 205
    .local v1, "dgContainer":Lorg/apache/index/poi/ddf/EscherContainerRecord;
    const/16 v7, -0xffe

    invoke-virtual {v1, v7}, Lorg/apache/index/poi/ddf/EscherContainerRecord;->setRecordId(S)V

    .line 206
    invoke-virtual {v1, v8}, Lorg/apache/index/poi/ddf/EscherContainerRecord;->setOptions(S)V

    .line 208
    new-instance v0, Lorg/apache/index/poi/ddf/EscherDgRecord;

    invoke-direct {v0}, Lorg/apache/index/poi/ddf/EscherDgRecord;-><init>()V

    .line 209
    .local v0, "dg":Lorg/apache/index/poi/ddf/EscherDgRecord;
    const/16 v7, 0x10

    invoke-virtual {v0, v7}, Lorg/apache/index/poi/ddf/EscherDgRecord;->setOptions(S)V

    .line 210
    invoke-virtual {v0, v10}, Lorg/apache/index/poi/ddf/EscherDgRecord;->setNumShapes(I)V

    .line 211
    invoke-virtual {v1, v0}, Lorg/apache/index/poi/ddf/EscherContainerRecord;->addChildRecord(Lorg/apache/index/poi/ddf/EscherRecord;)V

    .line 213
    new-instance v6, Lorg/apache/index/poi/ddf/EscherContainerRecord;

    invoke-direct {v6}, Lorg/apache/index/poi/ddf/EscherContainerRecord;-><init>()V

    .line 214
    .local v6, "spgrContainer":Lorg/apache/index/poi/ddf/EscherContainerRecord;
    invoke-virtual {v6, v8}, Lorg/apache/index/poi/ddf/EscherContainerRecord;->setOptions(S)V

    .line 215
    const/16 v7, -0xffd

    invoke-virtual {v6, v7}, Lorg/apache/index/poi/ddf/EscherContainerRecord;->setRecordId(S)V

    .line 217
    new-instance v4, Lorg/apache/index/poi/ddf/EscherContainerRecord;

    invoke-direct {v4}, Lorg/apache/index/poi/ddf/EscherContainerRecord;-><init>()V

    .line 218
    .local v4, "spContainer":Lorg/apache/index/poi/ddf/EscherContainerRecord;
    invoke-virtual {v4, v8}, Lorg/apache/index/poi/ddf/EscherContainerRecord;->setOptions(S)V

    .line 219
    invoke-virtual {v4, v9}, Lorg/apache/index/poi/ddf/EscherContainerRecord;->setRecordId(S)V

    .line 221
    new-instance v5, Lorg/apache/index/poi/ddf/EscherSpgrRecord;

    invoke-direct {v5}, Lorg/apache/index/poi/ddf/EscherSpgrRecord;-><init>()V

    .line 222
    .local v5, "spgr":Lorg/apache/index/poi/ddf/EscherSpgrRecord;
    invoke-virtual {v5, v10}, Lorg/apache/index/poi/ddf/EscherSpgrRecord;->setOptions(S)V

    .line 223
    invoke-virtual {v4, v5}, Lorg/apache/index/poi/ddf/EscherContainerRecord;->addChildRecord(Lorg/apache/index/poi/ddf/EscherRecord;)V

    .line 225
    new-instance v3, Lorg/apache/index/poi/ddf/EscherSpRecord;

    invoke-direct {v3}, Lorg/apache/index/poi/ddf/EscherSpRecord;-><init>()V

    .line 226
    .local v3, "sp":Lorg/apache/index/poi/ddf/EscherSpRecord;
    const/4 v7, 0x2

    invoke-virtual {v3, v7}, Lorg/apache/index/poi/ddf/EscherSpRecord;->setOptions(S)V

    .line 227
    const/4 v7, 0x5

    invoke-virtual {v3, v7}, Lorg/apache/index/poi/ddf/EscherSpRecord;->setFlags(I)V

    .line 228
    invoke-virtual {v4, v3}, Lorg/apache/index/poi/ddf/EscherContainerRecord;->addChildRecord(Lorg/apache/index/poi/ddf/EscherRecord;)V

    .line 229
    invoke-virtual {v6, v4}, Lorg/apache/index/poi/ddf/EscherContainerRecord;->addChildRecord(Lorg/apache/index/poi/ddf/EscherRecord;)V

    .line 230
    invoke-virtual {v1, v6}, Lorg/apache/index/poi/ddf/EscherContainerRecord;->addChildRecord(Lorg/apache/index/poi/ddf/EscherRecord;)V

    .line 232
    new-instance v4, Lorg/apache/index/poi/ddf/EscherContainerRecord;

    .end local v4    # "spContainer":Lorg/apache/index/poi/ddf/EscherContainerRecord;
    invoke-direct {v4}, Lorg/apache/index/poi/ddf/EscherContainerRecord;-><init>()V

    .line 233
    .restart local v4    # "spContainer":Lorg/apache/index/poi/ddf/EscherContainerRecord;
    invoke-virtual {v4, v8}, Lorg/apache/index/poi/ddf/EscherContainerRecord;->setOptions(S)V

    .line 234
    invoke-virtual {v4, v9}, Lorg/apache/index/poi/ddf/EscherContainerRecord;->setRecordId(S)V

    .line 235
    new-instance v3, Lorg/apache/index/poi/ddf/EscherSpRecord;

    .end local v3    # "sp":Lorg/apache/index/poi/ddf/EscherSpRecord;
    invoke-direct {v3}, Lorg/apache/index/poi/ddf/EscherSpRecord;-><init>()V

    .line 236
    .restart local v3    # "sp":Lorg/apache/index/poi/ddf/EscherSpRecord;
    const/16 v7, 0x12

    invoke-virtual {v3, v7}, Lorg/apache/index/poi/ddf/EscherSpRecord;->setOptions(S)V

    .line 237
    const/16 v7, 0xc00

    invoke-virtual {v3, v7}, Lorg/apache/index/poi/ddf/EscherSpRecord;->setFlags(I)V

    .line 238
    invoke-virtual {v4, v3}, Lorg/apache/index/poi/ddf/EscherContainerRecord;->addChildRecord(Lorg/apache/index/poi/ddf/EscherRecord;)V

    .line 240
    new-instance v2, Lorg/apache/index/poi/ddf/EscherOptRecord;

    invoke-direct {v2}, Lorg/apache/index/poi/ddf/EscherOptRecord;-><init>()V

    .line 241
    .local v2, "opt":Lorg/apache/index/poi/ddf/EscherOptRecord;
    const/16 v7, -0xff5

    invoke-virtual {v2, v7}, Lorg/apache/index/poi/ddf/EscherOptRecord;->setRecordId(S)V

    .line 242
    new-instance v7, Lorg/apache/index/poi/ddf/EscherSimpleProperty;

    const/16 v8, 0x193

    const v9, 0x99936e

    invoke-direct {v7, v8, v9}, Lorg/apache/index/poi/ddf/EscherSimpleProperty;-><init>(SI)V

    invoke-virtual {v2, v7}, Lorg/apache/index/poi/ddf/EscherOptRecord;->addEscherProperty(Lorg/apache/index/poi/ddf/EscherProperty;)V

    .line 243
    new-instance v7, Lorg/apache/index/poi/ddf/EscherSimpleProperty;

    const/16 v8, 0x194

    const v9, 0x76b1be

    invoke-direct {v7, v8, v9}, Lorg/apache/index/poi/ddf/EscherSimpleProperty;-><init>(SI)V

    invoke-virtual {v2, v7}, Lorg/apache/index/poi/ddf/EscherOptRecord;->addEscherProperty(Lorg/apache/index/poi/ddf/EscherProperty;)V

    .line 244
    new-instance v7, Lorg/apache/index/poi/ddf/EscherBoolProperty;

    const/16 v8, 0x1bf

    const v9, 0x120012

    invoke-direct {v7, v8, v9}, Lorg/apache/index/poi/ddf/EscherBoolProperty;-><init>(SI)V

    invoke-virtual {v2, v7}, Lorg/apache/index/poi/ddf/EscherOptRecord;->addEscherProperty(Lorg/apache/index/poi/ddf/EscherProperty;)V

    .line 245
    new-instance v7, Lorg/apache/index/poi/ddf/EscherBoolProperty;

    const/16 v8, 0x1ff

    const/high16 v9, 0x80000

    invoke-direct {v7, v8, v9}, Lorg/apache/index/poi/ddf/EscherBoolProperty;-><init>(SI)V

    invoke-virtual {v2, v7}, Lorg/apache/index/poi/ddf/EscherOptRecord;->addEscherProperty(Lorg/apache/index/poi/ddf/EscherProperty;)V

    .line 246
    new-instance v7, Lorg/apache/index/poi/ddf/EscherSimpleProperty;

    const/16 v8, 0x304

    const/16 v9, 0x9

    invoke-direct {v7, v8, v9}, Lorg/apache/index/poi/ddf/EscherSimpleProperty;-><init>(SI)V

    invoke-virtual {v2, v7}, Lorg/apache/index/poi/ddf/EscherOptRecord;->addEscherProperty(Lorg/apache/index/poi/ddf/EscherProperty;)V

    .line 247
    new-instance v7, Lorg/apache/index/poi/ddf/EscherSimpleProperty;

    const/16 v8, 0x33f

    const v9, 0x10001

    invoke-direct {v7, v8, v9}, Lorg/apache/index/poi/ddf/EscherSimpleProperty;-><init>(SI)V

    invoke-virtual {v2, v7}, Lorg/apache/index/poi/ddf/EscherOptRecord;->addEscherProperty(Lorg/apache/index/poi/ddf/EscherProperty;)V

    .line 248
    invoke-virtual {v4, v2}, Lorg/apache/index/poi/ddf/EscherContainerRecord;->addChildRecord(Lorg/apache/index/poi/ddf/EscherRecord;)V

    .line 250
    invoke-virtual {v1, v4}, Lorg/apache/index/poi/ddf/EscherContainerRecord;->addChildRecord(Lorg/apache/index/poi/ddf/EscherRecord;)V

    .line 252
    new-array v7, v10, [Lorg/apache/index/poi/ddf/EscherRecord;

    const/4 v8, 0x0

    .line 253
    aput-object v1, v7, v8

    .line 252
    iput-object v7, p0, Lorg/apache/index/poi/hslf/record/PPDrawing;->childRecords:[Lorg/apache/index/poi/ddf/EscherRecord;

    .line 255
    return-void
.end method

.method private findEscherChildren(Lorg/apache/index/poi/ddf/DefaultEscherRecordFactory;[BIILjava/util/Vector;)V
    .locals 8
    .param p1, "erf"    # Lorg/apache/index/poi/ddf/DefaultEscherRecordFactory;
    .param p2, "source"    # [B
    .param p3, "startPos"    # I
    .param p4, "lenToGo"    # I
    .param p5, "found"    # Ljava/util/Vector;

    .prologue
    const/16 v7, 0x8

    .line 130
    add-int/lit8 v3, p3, 0x4

    invoke-static {p2, v3}, Lorg/apache/index/poi/util/LittleEndian;->getInt([BI)I

    move-result v3

    add-int/lit8 v0, v3, 0x8

    .line 133
    .local v0, "escherBytes":I
    invoke-virtual {p1, p2, p3}, Lorg/apache/index/poi/ddf/DefaultEscherRecordFactory;->createRecord([BI)Lorg/apache/index/poi/ddf/EscherRecord;

    move-result-object v1

    .line 135
    .local v1, "r":Lorg/apache/index/poi/ddf/EscherRecord;
    invoke-virtual {v1, p2, p3, p1}, Lorg/apache/index/poi/ddf/EscherRecord;->fillFields([BILorg/apache/index/poi/ddf/EscherRecordFactory;)I

    .line 137
    invoke-virtual {p5, v1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 140
    invoke-virtual {v1}, Lorg/apache/index/poi/ddf/EscherRecord;->getRecordSize()I

    move-result v2

    .line 141
    .local v2, "size":I
    if-ge v2, v7, :cond_0

    .line 142
    iget-object v3, p0, Lorg/apache/index/poi/hslf/record/PPDrawing;->logger:Lorg/apache/index/poi/util/POILogger;

    sget v4, Lorg/apache/index/poi/util/POILogger;->WARN:I

    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "Hit short DDF record at "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, " - "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lorg/apache/index/poi/util/POILogger;->log(ILjava/lang/Object;)V

    .line 151
    :cond_0
    if-eq v2, v0, :cond_1

    .line 152
    iget-object v3, p0, Lorg/apache/index/poi/hslf/record/PPDrawing;->logger:Lorg/apache/index/poi/util/POILogger;

    sget v4, Lorg/apache/index/poi/util/POILogger;->WARN:I

    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "Record length="

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, " but getRecordSize() returned "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Lorg/apache/index/poi/ddf/EscherRecord;->getRecordSize()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, "; record: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lorg/apache/index/poi/util/POILogger;->log(ILjava/lang/Object;)V

    .line 153
    move v2, v0

    .line 155
    :cond_1
    add-int/2addr p3, v2

    .line 156
    sub-int/2addr p4, v2

    .line 157
    if-lt p4, v7, :cond_2

    .line 158
    invoke-direct/range {p0 .. p5}, Lorg/apache/index/poi/hslf/record/PPDrawing;->findEscherChildren(Lorg/apache/index/poi/ddf/DefaultEscherRecordFactory;[BIILjava/util/Vector;)V

    .line 160
    :cond_2
    return-void
.end method

.method private findEscherTextboxRecord([Lorg/apache/index/poi/ddf/EscherRecord;Ljava/util/Vector;)V
    .locals 8
    .param p1, "toSearch"    # [Lorg/apache/index/poi/ddf/EscherRecord;
    .param p2, "found"    # Ljava/util/Vector;

    .prologue
    .line 166
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v7, p1

    if-lt v2, v7, :cond_0

    .line 188
    return-void

    .line 167
    :cond_0
    aget-object v7, p1, v2

    instance-of v7, v7, Lorg/apache/index/poi/ddf/EscherTextboxRecord;

    if-eqz v7, :cond_4

    .line 168
    aget-object v5, p1, v2

    check-cast v5, Lorg/apache/index/poi/ddf/EscherTextboxRecord;

    .line 169
    .local v5, "tbr":Lorg/apache/index/poi/ddf/EscherTextboxRecord;
    new-instance v6, Lorg/apache/index/poi/hslf/record/EscherTextboxWrapper;

    invoke-direct {v6, v5}, Lorg/apache/index/poi/hslf/record/EscherTextboxWrapper;-><init>(Lorg/apache/index/poi/ddf/EscherTextboxRecord;)V

    .line 170
    .local v6, "w":Lorg/apache/index/poi/hslf/record/EscherTextboxWrapper;
    invoke-virtual {p2, v6}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 171
    move v3, v2

    .local v3, "j":I
    :goto_1
    if-gez v3, :cond_2

    .line 166
    .end local v3    # "j":I
    .end local v5    # "tbr":Lorg/apache/index/poi/ddf/EscherTextboxRecord;
    .end local v6    # "w":Lorg/apache/index/poi/hslf/record/EscherTextboxWrapper;
    :cond_1
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 172
    .restart local v3    # "j":I
    .restart local v5    # "tbr":Lorg/apache/index/poi/ddf/EscherTextboxRecord;
    .restart local v6    # "w":Lorg/apache/index/poi/hslf/record/EscherTextboxWrapper;
    :cond_2
    aget-object v7, p1, v3

    instance-of v7, v7, Lorg/apache/index/poi/ddf/EscherSpRecord;

    if-eqz v7, :cond_3

    .line 173
    aget-object v4, p1, v3

    check-cast v4, Lorg/apache/index/poi/ddf/EscherSpRecord;

    .line 174
    .local v4, "sp":Lorg/apache/index/poi/ddf/EscherSpRecord;
    invoke-virtual {v4}, Lorg/apache/index/poi/ddf/EscherSpRecord;->getShapeId()I

    move-result v7

    invoke-virtual {v6, v7}, Lorg/apache/index/poi/hslf/record/EscherTextboxWrapper;->setShapeId(I)V

    goto :goto_2

    .line 171
    .end local v4    # "sp":Lorg/apache/index/poi/ddf/EscherSpRecord;
    :cond_3
    add-int/lit8 v3, v3, -0x1

    goto :goto_1

    .line 180
    .end local v3    # "j":I
    .end local v5    # "tbr":Lorg/apache/index/poi/ddf/EscherTextboxRecord;
    .end local v6    # "w":Lorg/apache/index/poi/hslf/record/EscherTextboxWrapper;
    :cond_4
    aget-object v7, p1, v2

    invoke-virtual {v7}, Lorg/apache/index/poi/ddf/EscherRecord;->isContainerRecord()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 181
    aget-object v7, p1, v2

    invoke-virtual {v7}, Lorg/apache/index/poi/ddf/EscherRecord;->getChildRecords()Ljava/util/List;

    move-result-object v1

    .line 182
    .local v1, "childrenL":Ljava/util/List;, "Ljava/util/List<Lorg/apache/index/poi/ddf/EscherRecord;>;"
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v7

    new-array v0, v7, [Lorg/apache/index/poi/ddf/EscherRecord;

    .line 183
    .local v0, "children":[Lorg/apache/index/poi/ddf/EscherRecord;
    invoke-interface {v1, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 184
    invoke-direct {p0, v0, p2}, Lorg/apache/index/poi/hslf/record/PPDrawing;->findEscherTextboxRecord([Lorg/apache/index/poi/ddf/EscherRecord;Ljava/util/Vector;)V

    goto :goto_2
.end method


# virtual methods
.method public addTextboxWrapper(Lorg/apache/index/poi/hslf/record/EscherTextboxWrapper;)V
    .locals 4
    .param p1, "txtbox"    # Lorg/apache/index/poi/hslf/record/EscherTextboxWrapper;

    .prologue
    const/4 v3, 0x0

    .line 261
    iget-object v1, p0, Lorg/apache/index/poi/hslf/record/PPDrawing;->textboxWrappers:[Lorg/apache/index/poi/hslf/record/EscherTextboxWrapper;

    array-length v1, v1

    add-int/lit8 v1, v1, 0x1

    new-array v0, v1, [Lorg/apache/index/poi/hslf/record/EscherTextboxWrapper;

    .line 262
    .local v0, "tw":[Lorg/apache/index/poi/hslf/record/EscherTextboxWrapper;
    iget-object v1, p0, Lorg/apache/index/poi/hslf/record/PPDrawing;->textboxWrappers:[Lorg/apache/index/poi/hslf/record/EscherTextboxWrapper;

    iget-object v2, p0, Lorg/apache/index/poi/hslf/record/PPDrawing;->textboxWrappers:[Lorg/apache/index/poi/hslf/record/EscherTextboxWrapper;

    array-length v2, v2

    invoke-static {v1, v3, v0, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 264
    iget-object v1, p0, Lorg/apache/index/poi/hslf/record/PPDrawing;->textboxWrappers:[Lorg/apache/index/poi/hslf/record/EscherTextboxWrapper;

    array-length v1, v1

    aput-object p1, v0, v1

    .line 265
    iput-object v0, p0, Lorg/apache/index/poi/hslf/record/PPDrawing;->textboxWrappers:[Lorg/apache/index/poi/hslf/record/EscherTextboxWrapper;

    .line 266
    return-void
.end method

.method public getChildRecords()[Lorg/apache/index/poi/hslf/record/Record;
    .locals 1

    .prologue
    .line 198
    const/4 v0, 0x0

    return-object v0
.end method

.method public getEscherDgRecord()Lorg/apache/index/poi/ddf/EscherDgRecord;
    .locals 5

    .prologue
    .line 274
    iget-object v3, p0, Lorg/apache/index/poi/hslf/record/PPDrawing;->dg:Lorg/apache/index/poi/ddf/EscherDgRecord;

    if-nez v3, :cond_1

    .line 275
    iget-object v3, p0, Lorg/apache/index/poi/hslf/record/PPDrawing;->childRecords:[Lorg/apache/index/poi/ddf/EscherRecord;

    const/4 v4, 0x0

    aget-object v0, v3, v4

    check-cast v0, Lorg/apache/index/poi/ddf/EscherContainerRecord;

    .line 276
    .local v0, "dgContainer":Lorg/apache/index/poi/ddf/EscherContainerRecord;
    invoke-virtual {v0}, Lorg/apache/index/poi/ddf/EscherContainerRecord;->getChildIterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/index/poi/ddf/EscherRecord;>;"
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_2

    .line 284
    .end local v0    # "dgContainer":Lorg/apache/index/poi/ddf/EscherContainerRecord;
    .end local v1    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/index/poi/ddf/EscherRecord;>;"
    :cond_1
    :goto_0
    iget-object v3, p0, Lorg/apache/index/poi/hslf/record/PPDrawing;->dg:Lorg/apache/index/poi/ddf/EscherDgRecord;

    return-object v3

    .line 277
    .restart local v0    # "dgContainer":Lorg/apache/index/poi/ddf/EscherContainerRecord;
    .restart local v1    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/index/poi/ddf/EscherRecord;>;"
    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/index/poi/ddf/EscherRecord;

    .line 278
    .local v2, "r":Lorg/apache/index/poi/ddf/EscherRecord;
    instance-of v3, v2, Lorg/apache/index/poi/ddf/EscherDgRecord;

    if-eqz v3, :cond_0

    .line 279
    check-cast v2, Lorg/apache/index/poi/ddf/EscherDgRecord;

    .end local v2    # "r":Lorg/apache/index/poi/ddf/EscherRecord;
    iput-object v2, p0, Lorg/apache/index/poi/hslf/record/PPDrawing;->dg:Lorg/apache/index/poi/ddf/EscherDgRecord;

    goto :goto_0
.end method

.method public getEscherRecords()[Lorg/apache/index/poi/ddf/EscherRecord;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/PPDrawing;->childRecords:[Lorg/apache/index/poi/ddf/EscherRecord;

    return-object v0
.end method

.method public getRecordType()J
    .locals 2

    .prologue
    .line 193
    iget-wide v0, p0, Lorg/apache/index/poi/hslf/record/PPDrawing;->_type:J

    return-wide v0
.end method

.method public getTextboxWrappers()[Lorg/apache/index/poi/hslf/record/EscherTextboxWrapper;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/PPDrawing;->textboxWrappers:[Lorg/apache/index/poi/hslf/record/EscherTextboxWrapper;

    return-object v0
.end method
