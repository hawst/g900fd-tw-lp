.class public final Lorg/apache/index/poi/hslf/record/SlidePersistAtom;
.super Lorg/apache/index/poi/hslf/record/RecordAtom;
.source "SlidePersistAtom.java"


# static fields
.field private static _type:J


# instance fields
.field private _header:[B

.field private hasShapesOtherThanPlaceholders:Z

.field private numPlaceholderTexts:I

.field private refID:I

.field private reservedFields:[B

.field private slideIdentifier:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 33
    const-wide/16 v0, 0x3f3

    sput-wide v0, Lorg/apache/index/poi/hslf/record/SlidePersistAtom;->_type:J

    return-void
.end method

.method public constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v1, 0x0

    .line 103
    invoke-direct {p0}, Lorg/apache/index/poi/hslf/record/RecordAtom;-><init>()V

    .line 104
    const/16 v0, 0x8

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/apache/index/poi/hslf/record/SlidePersistAtom;->_header:[B

    .line 105
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/SlidePersistAtom;->_header:[B

    invoke-static {v0, v1, v1}, Lorg/apache/index/poi/util/LittleEndian;->putUShort([BII)V

    .line 106
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/SlidePersistAtom;->_header:[B

    const/4 v1, 0x2

    sget-wide v2, Lorg/apache/index/poi/hslf/record/SlidePersistAtom;->_type:J

    long-to-int v2, v2

    invoke-static {v0, v1, v2}, Lorg/apache/index/poi/util/LittleEndian;->putUShort([BII)V

    .line 107
    iget-object v0, p0, Lorg/apache/index/poi/hslf/record/SlidePersistAtom;->_header:[B

    const/16 v1, 0x14

    invoke-static {v0, v4, v1}, Lorg/apache/index/poi/util/LittleEndian;->putInt([BII)V

    .line 109
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/index/poi/hslf/record/SlidePersistAtom;->hasShapesOtherThanPlaceholders:Z

    .line 110
    new-array v0, v4, [B

    iput-object v0, p0, Lorg/apache/index/poi/hslf/record/SlidePersistAtom;->reservedFields:[B

    .line 111
    return-void
.end method

.method protected constructor <init>([BII)V
    .locals 5
    .param p1, "source"    # [B
    .param p2, "start"    # I
    .param p3, "len"    # I

    .prologue
    const/16 v2, 0x8

    const/4 v4, 0x0

    .line 69
    invoke-direct {p0}, Lorg/apache/index/poi/hslf/record/RecordAtom;-><init>()V

    .line 71
    if-ge p3, v2, :cond_0

    const/16 p3, 0x8

    .line 74
    :cond_0
    new-array v1, v2, [B

    iput-object v1, p0, Lorg/apache/index/poi/hslf/record/SlidePersistAtom;->_header:[B

    .line 75
    iget-object v1, p0, Lorg/apache/index/poi/hslf/record/SlidePersistAtom;->_header:[B

    invoke-static {p1, p2, v1, v4, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 78
    add-int/lit8 v1, p2, 0x8

    invoke-static {p1, v1}, Lorg/apache/index/poi/util/LittleEndian;->getInt([BI)I

    move-result v1

    iput v1, p0, Lorg/apache/index/poi/hslf/record/SlidePersistAtom;->refID:I

    .line 81
    add-int/lit8 v1, p2, 0xc

    invoke-static {p1, v1}, Lorg/apache/index/poi/util/LittleEndian;->getInt([BI)I

    move-result v0

    .line 82
    .local v0, "flags":I
    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    .line 83
    const/4 v1, 0x1

    iput-boolean v1, p0, Lorg/apache/index/poi/hslf/record/SlidePersistAtom;->hasShapesOtherThanPlaceholders:Z

    .line 89
    :goto_0
    add-int/lit8 v1, p2, 0x10

    invoke-static {p1, v1}, Lorg/apache/index/poi/util/LittleEndian;->getInt([BI)I

    move-result v1

    iput v1, p0, Lorg/apache/index/poi/hslf/record/SlidePersistAtom;->numPlaceholderTexts:I

    .line 92
    add-int/lit8 v1, p2, 0x14

    invoke-static {p1, v1}, Lorg/apache/index/poi/util/LittleEndian;->getInt([BI)I

    move-result v1

    iput v1, p0, Lorg/apache/index/poi/hslf/record/SlidePersistAtom;->slideIdentifier:I

    .line 96
    add-int/lit8 v1, p3, -0x18

    new-array v1, v1, [B

    iput-object v1, p0, Lorg/apache/index/poi/hslf/record/SlidePersistAtom;->reservedFields:[B

    .line 97
    add-int/lit8 v1, p2, 0x18

    iget-object v2, p0, Lorg/apache/index/poi/hslf/record/SlidePersistAtom;->reservedFields:[B

    iget-object v3, p0, Lorg/apache/index/poi/hslf/record/SlidePersistAtom;->reservedFields:[B

    array-length v3, v3

    invoke-static {p1, v1, v2, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 98
    return-void

    .line 85
    :cond_1
    iput-boolean v4, p0, Lorg/apache/index/poi/hslf/record/SlidePersistAtom;->hasShapesOtherThanPlaceholders:Z

    goto :goto_0
.end method


# virtual methods
.method public getHasShapesOtherThanPlaceholders()Z
    .locals 1

    .prologue
    .line 54
    iget-boolean v0, p0, Lorg/apache/index/poi/hslf/record/SlidePersistAtom;->hasShapesOtherThanPlaceholders:Z

    return v0
.end method

.method public getNumPlaceholderTexts()I
    .locals 1

    .prologue
    .line 53
    iget v0, p0, Lorg/apache/index/poi/hslf/record/SlidePersistAtom;->numPlaceholderTexts:I

    return v0
.end method

.method public getRecordType()J
    .locals 2

    .prologue
    .line 116
    sget-wide v0, Lorg/apache/index/poi/hslf/record/SlidePersistAtom;->_type:J

    return-wide v0
.end method

.method public getRefID()I
    .locals 1

    .prologue
    .line 51
    iget v0, p0, Lorg/apache/index/poi/hslf/record/SlidePersistAtom;->refID:I

    return v0
.end method

.method public getSlideIdentifier()I
    .locals 1

    .prologue
    .line 52
    iget v0, p0, Lorg/apache/index/poi/hslf/record/SlidePersistAtom;->slideIdentifier:I

    return v0
.end method

.method public setRefID(I)V
    .locals 0
    .param p1, "id"    # I

    .prologue
    .line 58
    iput p1, p0, Lorg/apache/index/poi/hslf/record/SlidePersistAtom;->refID:I

    .line 59
    return-void
.end method

.method public setSlideIdentifier(I)V
    .locals 0
    .param p1, "id"    # I

    .prologue
    .line 61
    iput p1, p0, Lorg/apache/index/poi/hslf/record/SlidePersistAtom;->slideIdentifier:I

    .line 62
    return-void
.end method

.method public writeOut(Ljava/io/OutputStream;)V
    .locals 2
    .param p1, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 124
    iget-object v1, p0, Lorg/apache/index/poi/hslf/record/SlidePersistAtom;->_header:[B

    invoke-virtual {p1, v1}, Ljava/io/OutputStream;->write([B)V

    .line 127
    const/4 v0, 0x0

    .line 128
    .local v0, "flags":I
    iget-boolean v1, p0, Lorg/apache/index/poi/hslf/record/SlidePersistAtom;->hasShapesOtherThanPlaceholders:Z

    if-eqz v1, :cond_0

    .line 129
    const/4 v0, 0x4

    .line 133
    :cond_0
    iget v1, p0, Lorg/apache/index/poi/hslf/record/SlidePersistAtom;->refID:I

    invoke-static {v1, p1}, Lorg/apache/index/poi/hslf/record/SlidePersistAtom;->writeLittleEndian(ILjava/io/OutputStream;)V

    .line 134
    invoke-static {v0, p1}, Lorg/apache/index/poi/hslf/record/SlidePersistAtom;->writeLittleEndian(ILjava/io/OutputStream;)V

    .line 135
    iget v1, p0, Lorg/apache/index/poi/hslf/record/SlidePersistAtom;->numPlaceholderTexts:I

    invoke-static {v1, p1}, Lorg/apache/index/poi/hslf/record/SlidePersistAtom;->writeLittleEndian(ILjava/io/OutputStream;)V

    .line 136
    iget v1, p0, Lorg/apache/index/poi/hslf/record/SlidePersistAtom;->slideIdentifier:I

    invoke-static {v1, p1}, Lorg/apache/index/poi/hslf/record/SlidePersistAtom;->writeLittleEndian(ILjava/io/OutputStream;)V

    .line 137
    iget-object v1, p0, Lorg/apache/index/poi/hslf/record/SlidePersistAtom;->reservedFields:[B

    invoke-virtual {p1, v1}, Ljava/io/OutputStream;->write([B)V

    .line 138
    return-void
.end method
