.class public final Lorg/apache/index/poi/hpsf/SummaryInformation;
.super Lorg/apache/index/poi/hpsf/SpecialPropertySet;
.source "SummaryInformation.java"


# static fields
.field public static final DEFAULT_STREAM_NAME:Ljava/lang/String; = "\u0005SummaryInformation"


# direct methods
.method public constructor <init>(Lorg/apache/index/poi/hpsf/PropertySet;)V
    .locals 3
    .param p1, "ps"    # Lorg/apache/index/poi/hpsf/PropertySet;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/index/poi/hpsf/UnexpectedPropertySetTypeException;
        }
    .end annotation

    .prologue
    .line 57
    invoke-direct {p0, p1}, Lorg/apache/index/poi/hpsf/SpecialPropertySet;-><init>(Lorg/apache/index/poi/hpsf/PropertySet;)V

    .line 58
    invoke-virtual {p0}, Lorg/apache/index/poi/hpsf/SummaryInformation;->isSummaryInformation()Z

    move-result v0

    if-nez v0, :cond_0

    .line 59
    new-instance v0, Lorg/apache/index/poi/hpsf/UnexpectedPropertySetTypeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Not a "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 60
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 59
    invoke-direct {v0, v1}, Lorg/apache/index/poi/hpsf/UnexpectedPropertySetTypeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 61
    :cond_0
    return-void
.end method


# virtual methods
.method public getApplicationName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 671
    const/16 v0, 0x12

    invoke-virtual {p0, v0}, Lorg/apache/index/poi/hpsf/SummaryInformation;->getProperty(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getAuthor()Ljava/lang/String;
    .locals 1

    .prologue
    .line 144
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lorg/apache/index/poi/hpsf/SummaryInformation;->getProperty(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getCharCount()I
    .locals 1

    .prologue
    .line 591
    const/16 v0, 0x10

    invoke-virtual {p0, v0}, Lorg/apache/index/poi/hpsf/SummaryInformation;->getPropertyIntValue(I)I

    move-result v0

    return v0
.end method

.method public getComments()Ljava/lang/String;
    .locals 1

    .prologue
    .line 216
    const/4 v0, 0x6

    invoke-virtual {p0, v0}, Lorg/apache/index/poi/hpsf/SummaryInformation;->getProperty(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getCreateDateTime()Ljava/util/Date;
    .locals 1

    .prologue
    .line 440
    const/16 v0, 0xc

    invoke-virtual {p0, v0}, Lorg/apache/index/poi/hpsf/SummaryInformation;->getProperty(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Date;

    return-object v0
.end method

.method public getEditTime()J
    .locals 4

    .prologue
    .line 362
    const/16 v1, 0xa

    invoke-virtual {p0, v1}, Lorg/apache/index/poi/hpsf/SummaryInformation;->getProperty(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Date;

    .line 363
    .local v0, "d":Ljava/util/Date;
    if-nez v0, :cond_0

    .line 364
    const-wide/16 v2, 0x0

    .line 366
    :goto_0
    return-wide v2

    :cond_0
    invoke-static {v0}, Lorg/apache/index/poi/hpsf/Util;->dateToFileTime(Ljava/util/Date;)J

    move-result-wide v2

    goto :goto_0
.end method

.method public getKeywords()Ljava/lang/String;
    .locals 1

    .prologue
    .line 180
    const/4 v0, 0x5

    invoke-virtual {p0, v0}, Lorg/apache/index/poi/hpsf/SummaryInformation;->getProperty(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getLastAuthor()Ljava/lang/String;
    .locals 1

    .prologue
    .line 288
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lorg/apache/index/poi/hpsf/SummaryInformation;->getProperty(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getLastPrinted()Ljava/util/Date;
    .locals 1

    .prologue
    .line 403
    const/16 v0, 0xb

    invoke-virtual {p0, v0}, Lorg/apache/index/poi/hpsf/SummaryInformation;->getProperty(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Date;

    return-object v0
.end method

.method public getLastSaveDateTime()Ljava/util/Date;
    .locals 1

    .prologue
    .line 477
    const/16 v0, 0xd

    invoke-virtual {p0, v0}, Lorg/apache/index/poi/hpsf/SummaryInformation;->getProperty(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Date;

    return-object v0
.end method

.method public getPageCount()I
    .locals 1

    .prologue
    .line 517
    const/16 v0, 0xe

    invoke-virtual {p0, v0}, Lorg/apache/index/poi/hpsf/SummaryInformation;->getPropertyIntValue(I)I

    move-result v0

    return v0
.end method

.method public getPropertySetIDMap()Lorg/apache/index/poi/hpsf/wellknown/PropertyIDMap;
    .locals 1

    .prologue
    .line 41
    invoke-static {}, Lorg/apache/index/poi/hpsf/wellknown/PropertyIDMap;->getSummaryInformationProperties()Lorg/apache/index/poi/hpsf/wellknown/PropertyIDMap;

    move-result-object v0

    return-object v0
.end method

.method public getRevNumber()Ljava/lang/String;
    .locals 1

    .prologue
    .line 324
    const/16 v0, 0x9

    invoke-virtual {p0, v0}, Lorg/apache/index/poi/hpsf/SummaryInformation;->getProperty(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getSecurity()I
    .locals 1

    .prologue
    .line 724
    const/16 v0, 0x13

    invoke-virtual {p0, v0}, Lorg/apache/index/poi/hpsf/SummaryInformation;->getPropertyIntValue(I)I

    move-result v0

    return v0
.end method

.method public getSubject()Ljava/lang/String;
    .locals 1

    .prologue
    .line 108
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lorg/apache/index/poi/hpsf/SummaryInformation;->getProperty(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getTemplate()Ljava/lang/String;
    .locals 1

    .prologue
    .line 252
    const/4 v0, 0x7

    invoke-virtual {p0, v0}, Lorg/apache/index/poi/hpsf/SummaryInformation;->getProperty(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getThumbnail()[B
    .locals 1

    .prologue
    .line 634
    const/16 v0, 0x11

    invoke-virtual {p0, v0}, Lorg/apache/index/poi/hpsf/SummaryInformation;->getProperty(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 72
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lorg/apache/index/poi/hpsf/SummaryInformation;->getProperty(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getWordCount()I
    .locals 1

    .prologue
    .line 554
    const/16 v0, 0xf

    invoke-virtual {p0, v0}, Lorg/apache/index/poi/hpsf/SummaryInformation;->getPropertyIntValue(I)I

    move-result v0

    return v0
.end method

.method public removeApplicationName()V
    .locals 4

    .prologue
    .line 694
    invoke-virtual {p0}, Lorg/apache/index/poi/hpsf/SummaryInformation;->getFirstSection()Lorg/apache/index/poi/hpsf/Section;

    move-result-object v0

    check-cast v0, Lorg/apache/index/poi/hpsf/MutableSection;

    .line 695
    .local v0, "s":Lorg/apache/index/poi/hpsf/MutableSection;
    const-wide/16 v2, 0x12

    invoke-virtual {v0, v2, v3}, Lorg/apache/index/poi/hpsf/MutableSection;->removeProperty(J)V

    .line 696
    return-void
.end method

.method public removeAuthor()V
    .locals 4

    .prologue
    .line 167
    invoke-virtual {p0}, Lorg/apache/index/poi/hpsf/SummaryInformation;->getFirstSection()Lorg/apache/index/poi/hpsf/Section;

    move-result-object v0

    check-cast v0, Lorg/apache/index/poi/hpsf/MutableSection;

    .line 168
    .local v0, "s":Lorg/apache/index/poi/hpsf/MutableSection;
    const-wide/16 v2, 0x4

    invoke-virtual {v0, v2, v3}, Lorg/apache/index/poi/hpsf/MutableSection;->removeProperty(J)V

    .line 169
    return-void
.end method

.method public removeCharCount()V
    .locals 4

    .prologue
    .line 614
    invoke-virtual {p0}, Lorg/apache/index/poi/hpsf/SummaryInformation;->getFirstSection()Lorg/apache/index/poi/hpsf/Section;

    move-result-object v0

    check-cast v0, Lorg/apache/index/poi/hpsf/MutableSection;

    .line 615
    .local v0, "s":Lorg/apache/index/poi/hpsf/MutableSection;
    const-wide/16 v2, 0x10

    invoke-virtual {v0, v2, v3}, Lorg/apache/index/poi/hpsf/MutableSection;->removeProperty(J)V

    .line 616
    return-void
.end method

.method public removeComments()V
    .locals 4

    .prologue
    .line 239
    invoke-virtual {p0}, Lorg/apache/index/poi/hpsf/SummaryInformation;->getFirstSection()Lorg/apache/index/poi/hpsf/Section;

    move-result-object v0

    check-cast v0, Lorg/apache/index/poi/hpsf/MutableSection;

    .line 240
    .local v0, "s":Lorg/apache/index/poi/hpsf/MutableSection;
    const-wide/16 v2, 0x6

    invoke-virtual {v0, v2, v3}, Lorg/apache/index/poi/hpsf/MutableSection;->removeProperty(J)V

    .line 241
    return-void
.end method

.method public removeCreateDateTime()V
    .locals 4

    .prologue
    .line 464
    invoke-virtual {p0}, Lorg/apache/index/poi/hpsf/SummaryInformation;->getFirstSection()Lorg/apache/index/poi/hpsf/Section;

    move-result-object v0

    check-cast v0, Lorg/apache/index/poi/hpsf/MutableSection;

    .line 465
    .local v0, "s":Lorg/apache/index/poi/hpsf/MutableSection;
    const-wide/16 v2, 0xc

    invoke-virtual {v0, v2, v3}, Lorg/apache/index/poi/hpsf/MutableSection;->removeProperty(J)V

    .line 466
    return-void
.end method

.method public removeEditTime()V
    .locals 4

    .prologue
    .line 390
    invoke-virtual {p0}, Lorg/apache/index/poi/hpsf/SummaryInformation;->getFirstSection()Lorg/apache/index/poi/hpsf/Section;

    move-result-object v0

    check-cast v0, Lorg/apache/index/poi/hpsf/MutableSection;

    .line 391
    .local v0, "s":Lorg/apache/index/poi/hpsf/MutableSection;
    const-wide/16 v2, 0xa

    invoke-virtual {v0, v2, v3}, Lorg/apache/index/poi/hpsf/MutableSection;->removeProperty(J)V

    .line 392
    return-void
.end method

.method public removeKeywords()V
    .locals 4

    .prologue
    .line 203
    invoke-virtual {p0}, Lorg/apache/index/poi/hpsf/SummaryInformation;->getFirstSection()Lorg/apache/index/poi/hpsf/Section;

    move-result-object v0

    check-cast v0, Lorg/apache/index/poi/hpsf/MutableSection;

    .line 204
    .local v0, "s":Lorg/apache/index/poi/hpsf/MutableSection;
    const-wide/16 v2, 0x5

    invoke-virtual {v0, v2, v3}, Lorg/apache/index/poi/hpsf/MutableSection;->removeProperty(J)V

    .line 205
    return-void
.end method

.method public removeLastAuthor()V
    .locals 4

    .prologue
    .line 311
    invoke-virtual {p0}, Lorg/apache/index/poi/hpsf/SummaryInformation;->getFirstSection()Lorg/apache/index/poi/hpsf/Section;

    move-result-object v0

    check-cast v0, Lorg/apache/index/poi/hpsf/MutableSection;

    .line 312
    .local v0, "s":Lorg/apache/index/poi/hpsf/MutableSection;
    const-wide/16 v2, 0x8

    invoke-virtual {v0, v2, v3}, Lorg/apache/index/poi/hpsf/MutableSection;->removeProperty(J)V

    .line 313
    return-void
.end method

.method public removeLastPrinted()V
    .locals 4

    .prologue
    .line 427
    invoke-virtual {p0}, Lorg/apache/index/poi/hpsf/SummaryInformation;->getFirstSection()Lorg/apache/index/poi/hpsf/Section;

    move-result-object v0

    check-cast v0, Lorg/apache/index/poi/hpsf/MutableSection;

    .line 428
    .local v0, "s":Lorg/apache/index/poi/hpsf/MutableSection;
    const-wide/16 v2, 0xb

    invoke-virtual {v0, v2, v3}, Lorg/apache/index/poi/hpsf/MutableSection;->removeProperty(J)V

    .line 429
    return-void
.end method

.method public removeLastSaveDateTime()V
    .locals 4

    .prologue
    .line 502
    invoke-virtual {p0}, Lorg/apache/index/poi/hpsf/SummaryInformation;->getFirstSection()Lorg/apache/index/poi/hpsf/Section;

    move-result-object v0

    check-cast v0, Lorg/apache/index/poi/hpsf/MutableSection;

    .line 503
    .local v0, "s":Lorg/apache/index/poi/hpsf/MutableSection;
    const-wide/16 v2, 0xd

    invoke-virtual {v0, v2, v3}, Lorg/apache/index/poi/hpsf/MutableSection;->removeProperty(J)V

    .line 504
    return-void
.end method

.method public removePageCount()V
    .locals 4

    .prologue
    .line 540
    invoke-virtual {p0}, Lorg/apache/index/poi/hpsf/SummaryInformation;->getFirstSection()Lorg/apache/index/poi/hpsf/Section;

    move-result-object v0

    check-cast v0, Lorg/apache/index/poi/hpsf/MutableSection;

    .line 541
    .local v0, "s":Lorg/apache/index/poi/hpsf/MutableSection;
    const-wide/16 v2, 0xe

    invoke-virtual {v0, v2, v3}, Lorg/apache/index/poi/hpsf/MutableSection;->removeProperty(J)V

    .line 542
    return-void
.end method

.method public removeRevNumber()V
    .locals 4

    .prologue
    .line 347
    invoke-virtual {p0}, Lorg/apache/index/poi/hpsf/SummaryInformation;->getFirstSection()Lorg/apache/index/poi/hpsf/Section;

    move-result-object v0

    check-cast v0, Lorg/apache/index/poi/hpsf/MutableSection;

    .line 348
    .local v0, "s":Lorg/apache/index/poi/hpsf/MutableSection;
    const-wide/16 v2, 0x9

    invoke-virtual {v0, v2, v3}, Lorg/apache/index/poi/hpsf/MutableSection;->removeProperty(J)V

    .line 349
    return-void
.end method

.method public removeSecurity()V
    .locals 4

    .prologue
    .line 747
    invoke-virtual {p0}, Lorg/apache/index/poi/hpsf/SummaryInformation;->getFirstSection()Lorg/apache/index/poi/hpsf/Section;

    move-result-object v0

    check-cast v0, Lorg/apache/index/poi/hpsf/MutableSection;

    .line 748
    .local v0, "s":Lorg/apache/index/poi/hpsf/MutableSection;
    const-wide/16 v2, 0x13

    invoke-virtual {v0, v2, v3}, Lorg/apache/index/poi/hpsf/MutableSection;->removeProperty(J)V

    .line 749
    return-void
.end method

.method public removeSubject()V
    .locals 4

    .prologue
    .line 131
    invoke-virtual {p0}, Lorg/apache/index/poi/hpsf/SummaryInformation;->getFirstSection()Lorg/apache/index/poi/hpsf/Section;

    move-result-object v0

    check-cast v0, Lorg/apache/index/poi/hpsf/MutableSection;

    .line 132
    .local v0, "s":Lorg/apache/index/poi/hpsf/MutableSection;
    const-wide/16 v2, 0x3

    invoke-virtual {v0, v2, v3}, Lorg/apache/index/poi/hpsf/MutableSection;->removeProperty(J)V

    .line 133
    return-void
.end method

.method public removeTemplate()V
    .locals 4

    .prologue
    .line 275
    invoke-virtual {p0}, Lorg/apache/index/poi/hpsf/SummaryInformation;->getFirstSection()Lorg/apache/index/poi/hpsf/Section;

    move-result-object v0

    check-cast v0, Lorg/apache/index/poi/hpsf/MutableSection;

    .line 276
    .local v0, "s":Lorg/apache/index/poi/hpsf/MutableSection;
    const-wide/16 v2, 0x7

    invoke-virtual {v0, v2, v3}, Lorg/apache/index/poi/hpsf/MutableSection;->removeProperty(J)V

    .line 277
    return-void
.end method

.method public removeThumbnail()V
    .locals 4

    .prologue
    .line 658
    invoke-virtual {p0}, Lorg/apache/index/poi/hpsf/SummaryInformation;->getFirstSection()Lorg/apache/index/poi/hpsf/Section;

    move-result-object v0

    check-cast v0, Lorg/apache/index/poi/hpsf/MutableSection;

    .line 659
    .local v0, "s":Lorg/apache/index/poi/hpsf/MutableSection;
    const-wide/16 v2, 0x11

    invoke-virtual {v0, v2, v3}, Lorg/apache/index/poi/hpsf/MutableSection;->removeProperty(J)V

    .line 660
    return-void
.end method

.method public removeTitle()V
    .locals 4

    .prologue
    .line 95
    invoke-virtual {p0}, Lorg/apache/index/poi/hpsf/SummaryInformation;->getFirstSection()Lorg/apache/index/poi/hpsf/Section;

    move-result-object v0

    check-cast v0, Lorg/apache/index/poi/hpsf/MutableSection;

    .line 96
    .local v0, "s":Lorg/apache/index/poi/hpsf/MutableSection;
    const-wide/16 v2, 0x2

    invoke-virtual {v0, v2, v3}, Lorg/apache/index/poi/hpsf/MutableSection;->removeProperty(J)V

    .line 97
    return-void
.end method

.method public removeWordCount()V
    .locals 4

    .prologue
    .line 577
    invoke-virtual {p0}, Lorg/apache/index/poi/hpsf/SummaryInformation;->getFirstSection()Lorg/apache/index/poi/hpsf/Section;

    move-result-object v0

    check-cast v0, Lorg/apache/index/poi/hpsf/MutableSection;

    .line 578
    .local v0, "s":Lorg/apache/index/poi/hpsf/MutableSection;
    const-wide/16 v2, 0xf

    invoke-virtual {v0, v2, v3}, Lorg/apache/index/poi/hpsf/MutableSection;->removeProperty(J)V

    .line 579
    return-void
.end method

.method public setApplicationName(Ljava/lang/String;)V
    .locals 2
    .param p1, "applicationName"    # Ljava/lang/String;

    .prologue
    .line 683
    invoke-virtual {p0}, Lorg/apache/index/poi/hpsf/SummaryInformation;->getFirstSection()Lorg/apache/index/poi/hpsf/Section;

    move-result-object v0

    check-cast v0, Lorg/apache/index/poi/hpsf/MutableSection;

    .line 684
    .local v0, "s":Lorg/apache/index/poi/hpsf/MutableSection;
    const/16 v1, 0x12

    invoke-virtual {v0, v1, p1}, Lorg/apache/index/poi/hpsf/MutableSection;->setProperty(ILjava/lang/String;)V

    .line 685
    return-void
.end method

.method public setAuthor(Ljava/lang/String;)V
    .locals 2
    .param p1, "author"    # Ljava/lang/String;

    .prologue
    .line 156
    invoke-virtual {p0}, Lorg/apache/index/poi/hpsf/SummaryInformation;->getFirstSection()Lorg/apache/index/poi/hpsf/Section;

    move-result-object v0

    check-cast v0, Lorg/apache/index/poi/hpsf/MutableSection;

    .line 157
    .local v0, "s":Lorg/apache/index/poi/hpsf/MutableSection;
    const/4 v1, 0x4

    invoke-virtual {v0, v1, p1}, Lorg/apache/index/poi/hpsf/MutableSection;->setProperty(ILjava/lang/String;)V

    .line 158
    return-void
.end method

.method public setCharCount(I)V
    .locals 2
    .param p1, "charCount"    # I

    .prologue
    .line 603
    invoke-virtual {p0}, Lorg/apache/index/poi/hpsf/SummaryInformation;->getFirstSection()Lorg/apache/index/poi/hpsf/Section;

    move-result-object v0

    check-cast v0, Lorg/apache/index/poi/hpsf/MutableSection;

    .line 604
    .local v0, "s":Lorg/apache/index/poi/hpsf/MutableSection;
    const/16 v1, 0x10

    invoke-virtual {v0, v1, p1}, Lorg/apache/index/poi/hpsf/MutableSection;->setProperty(II)V

    .line 605
    return-void
.end method

.method public setComments(Ljava/lang/String;)V
    .locals 2
    .param p1, "comments"    # Ljava/lang/String;

    .prologue
    .line 228
    invoke-virtual {p0}, Lorg/apache/index/poi/hpsf/SummaryInformation;->getFirstSection()Lorg/apache/index/poi/hpsf/Section;

    move-result-object v0

    check-cast v0, Lorg/apache/index/poi/hpsf/MutableSection;

    .line 229
    .local v0, "s":Lorg/apache/index/poi/hpsf/MutableSection;
    const/4 v1, 0x6

    invoke-virtual {v0, v1, p1}, Lorg/apache/index/poi/hpsf/MutableSection;->setProperty(ILjava/lang/String;)V

    .line 230
    return-void
.end method

.method public setCreateDateTime(Ljava/util/Date;)V
    .locals 4
    .param p1, "createDateTime"    # Ljava/util/Date;

    .prologue
    .line 452
    invoke-virtual {p0}, Lorg/apache/index/poi/hpsf/SummaryInformation;->getFirstSection()Lorg/apache/index/poi/hpsf/Section;

    move-result-object v0

    check-cast v0, Lorg/apache/index/poi/hpsf/MutableSection;

    .line 453
    .local v0, "s":Lorg/apache/index/poi/hpsf/MutableSection;
    const/16 v1, 0xc

    const-wide/16 v2, 0x40

    invoke-virtual {v0, v1, v2, v3, p1}, Lorg/apache/index/poi/hpsf/MutableSection;->setProperty(IJLjava/lang/Object;)V

    .line 455
    return-void
.end method

.method public setEditTime(J)V
    .locals 7
    .param p1, "time"    # J

    .prologue
    .line 378
    invoke-static {p1, p2}, Lorg/apache/index/poi/hpsf/Util;->filetimeToDate(J)Ljava/util/Date;

    move-result-object v0

    .line 379
    .local v0, "d":Ljava/util/Date;
    invoke-virtual {p0}, Lorg/apache/index/poi/hpsf/SummaryInformation;->getFirstSection()Lorg/apache/index/poi/hpsf/Section;

    move-result-object v1

    check-cast v1, Lorg/apache/index/poi/hpsf/MutableSection;

    .line 380
    .local v1, "s":Lorg/apache/index/poi/hpsf/MutableSection;
    const/16 v2, 0xa

    const-wide/16 v4, 0x40

    invoke-virtual {v1, v2, v4, v5, v0}, Lorg/apache/index/poi/hpsf/MutableSection;->setProperty(IJLjava/lang/Object;)V

    .line 381
    return-void
.end method

.method public setKeywords(Ljava/lang/String;)V
    .locals 2
    .param p1, "keywords"    # Ljava/lang/String;

    .prologue
    .line 192
    invoke-virtual {p0}, Lorg/apache/index/poi/hpsf/SummaryInformation;->getFirstSection()Lorg/apache/index/poi/hpsf/Section;

    move-result-object v0

    check-cast v0, Lorg/apache/index/poi/hpsf/MutableSection;

    .line 193
    .local v0, "s":Lorg/apache/index/poi/hpsf/MutableSection;
    const/4 v1, 0x5

    invoke-virtual {v0, v1, p1}, Lorg/apache/index/poi/hpsf/MutableSection;->setProperty(ILjava/lang/String;)V

    .line 194
    return-void
.end method

.method public setLastAuthor(Ljava/lang/String;)V
    .locals 2
    .param p1, "lastAuthor"    # Ljava/lang/String;

    .prologue
    .line 300
    invoke-virtual {p0}, Lorg/apache/index/poi/hpsf/SummaryInformation;->getFirstSection()Lorg/apache/index/poi/hpsf/Section;

    move-result-object v0

    check-cast v0, Lorg/apache/index/poi/hpsf/MutableSection;

    .line 301
    .local v0, "s":Lorg/apache/index/poi/hpsf/MutableSection;
    const/16 v1, 0x8

    invoke-virtual {v0, v1, p1}, Lorg/apache/index/poi/hpsf/MutableSection;->setProperty(ILjava/lang/String;)V

    .line 302
    return-void
.end method

.method public setLastPrinted(Ljava/util/Date;)V
    .locals 4
    .param p1, "lastPrinted"    # Ljava/util/Date;

    .prologue
    .line 415
    invoke-virtual {p0}, Lorg/apache/index/poi/hpsf/SummaryInformation;->getFirstSection()Lorg/apache/index/poi/hpsf/Section;

    move-result-object v0

    check-cast v0, Lorg/apache/index/poi/hpsf/MutableSection;

    .line 416
    .local v0, "s":Lorg/apache/index/poi/hpsf/MutableSection;
    const/16 v1, 0xb

    const-wide/16 v2, 0x40

    invoke-virtual {v0, v1, v2, v3, p1}, Lorg/apache/index/poi/hpsf/MutableSection;->setProperty(IJLjava/lang/Object;)V

    .line 418
    return-void
.end method

.method public setLastSaveDateTime(Ljava/util/Date;)V
    .locals 4
    .param p1, "time"    # Ljava/util/Date;

    .prologue
    .line 489
    invoke-virtual {p0}, Lorg/apache/index/poi/hpsf/SummaryInformation;->getFirstSection()Lorg/apache/index/poi/hpsf/Section;

    move-result-object v0

    check-cast v0, Lorg/apache/index/poi/hpsf/MutableSection;

    .line 491
    .local v0, "s":Lorg/apache/index/poi/hpsf/MutableSection;
    const/16 v1, 0xd

    .line 492
    const-wide/16 v2, 0x40

    .line 491
    invoke-virtual {v0, v1, v2, v3, p1}, Lorg/apache/index/poi/hpsf/MutableSection;->setProperty(IJLjava/lang/Object;)V

    .line 493
    return-void
.end method

.method public setPageCount(I)V
    .locals 2
    .param p1, "pageCount"    # I

    .prologue
    .line 529
    invoke-virtual {p0}, Lorg/apache/index/poi/hpsf/SummaryInformation;->getFirstSection()Lorg/apache/index/poi/hpsf/Section;

    move-result-object v0

    check-cast v0, Lorg/apache/index/poi/hpsf/MutableSection;

    .line 530
    .local v0, "s":Lorg/apache/index/poi/hpsf/MutableSection;
    const/16 v1, 0xe

    invoke-virtual {v0, v1, p1}, Lorg/apache/index/poi/hpsf/MutableSection;->setProperty(II)V

    .line 531
    return-void
.end method

.method public setRevNumber(Ljava/lang/String;)V
    .locals 2
    .param p1, "revNumber"    # Ljava/lang/String;

    .prologue
    .line 336
    invoke-virtual {p0}, Lorg/apache/index/poi/hpsf/SummaryInformation;->getFirstSection()Lorg/apache/index/poi/hpsf/Section;

    move-result-object v0

    check-cast v0, Lorg/apache/index/poi/hpsf/MutableSection;

    .line 337
    .local v0, "s":Lorg/apache/index/poi/hpsf/MutableSection;
    const/16 v1, 0x9

    invoke-virtual {v0, v1, p1}, Lorg/apache/index/poi/hpsf/MutableSection;->setProperty(ILjava/lang/String;)V

    .line 338
    return-void
.end method

.method public setSecurity(I)V
    .locals 2
    .param p1, "security"    # I

    .prologue
    .line 736
    invoke-virtual {p0}, Lorg/apache/index/poi/hpsf/SummaryInformation;->getFirstSection()Lorg/apache/index/poi/hpsf/Section;

    move-result-object v0

    check-cast v0, Lorg/apache/index/poi/hpsf/MutableSection;

    .line 737
    .local v0, "s":Lorg/apache/index/poi/hpsf/MutableSection;
    const/16 v1, 0x13

    invoke-virtual {v0, v1, p1}, Lorg/apache/index/poi/hpsf/MutableSection;->setProperty(II)V

    .line 738
    return-void
.end method

.method public setSubject(Ljava/lang/String;)V
    .locals 2
    .param p1, "subject"    # Ljava/lang/String;

    .prologue
    .line 120
    invoke-virtual {p0}, Lorg/apache/index/poi/hpsf/SummaryInformation;->getFirstSection()Lorg/apache/index/poi/hpsf/Section;

    move-result-object v0

    check-cast v0, Lorg/apache/index/poi/hpsf/MutableSection;

    .line 121
    .local v0, "s":Lorg/apache/index/poi/hpsf/MutableSection;
    const/4 v1, 0x3

    invoke-virtual {v0, v1, p1}, Lorg/apache/index/poi/hpsf/MutableSection;->setProperty(ILjava/lang/String;)V

    .line 122
    return-void
.end method

.method public setTemplate(Ljava/lang/String;)V
    .locals 2
    .param p1, "template"    # Ljava/lang/String;

    .prologue
    .line 264
    invoke-virtual {p0}, Lorg/apache/index/poi/hpsf/SummaryInformation;->getFirstSection()Lorg/apache/index/poi/hpsf/Section;

    move-result-object v0

    check-cast v0, Lorg/apache/index/poi/hpsf/MutableSection;

    .line 265
    .local v0, "s":Lorg/apache/index/poi/hpsf/MutableSection;
    const/4 v1, 0x7

    invoke-virtual {v0, v1, p1}, Lorg/apache/index/poi/hpsf/MutableSection;->setProperty(ILjava/lang/String;)V

    .line 266
    return-void
.end method

.method public setThumbnail([B)V
    .locals 4
    .param p1, "thumbnail"    # [B

    .prologue
    .line 646
    invoke-virtual {p0}, Lorg/apache/index/poi/hpsf/SummaryInformation;->getFirstSection()Lorg/apache/index/poi/hpsf/Section;

    move-result-object v0

    check-cast v0, Lorg/apache/index/poi/hpsf/MutableSection;

    .line 647
    .local v0, "s":Lorg/apache/index/poi/hpsf/MutableSection;
    const/16 v1, 0x11

    .line 648
    const-wide/16 v2, 0x1e

    .line 647
    invoke-virtual {v0, v1, v2, v3, p1}, Lorg/apache/index/poi/hpsf/MutableSection;->setProperty(IJLjava/lang/Object;)V

    .line 649
    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 2
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 84
    invoke-virtual {p0}, Lorg/apache/index/poi/hpsf/SummaryInformation;->getFirstSection()Lorg/apache/index/poi/hpsf/Section;

    move-result-object v0

    check-cast v0, Lorg/apache/index/poi/hpsf/MutableSection;

    .line 85
    .local v0, "s":Lorg/apache/index/poi/hpsf/MutableSection;
    const/4 v1, 0x2

    invoke-virtual {v0, v1, p1}, Lorg/apache/index/poi/hpsf/MutableSection;->setProperty(ILjava/lang/String;)V

    .line 86
    return-void
.end method

.method public setWordCount(I)V
    .locals 2
    .param p1, "wordCount"    # I

    .prologue
    .line 566
    invoke-virtual {p0}, Lorg/apache/index/poi/hpsf/SummaryInformation;->getFirstSection()Lorg/apache/index/poi/hpsf/Section;

    move-result-object v0

    check-cast v0, Lorg/apache/index/poi/hpsf/MutableSection;

    .line 567
    .local v0, "s":Lorg/apache/index/poi/hpsf/MutableSection;
    const/16 v1, 0xf

    invoke-virtual {v0, v1, p1}, Lorg/apache/index/poi/hpsf/MutableSection;->setProperty(II)V

    .line 568
    return-void
.end method
