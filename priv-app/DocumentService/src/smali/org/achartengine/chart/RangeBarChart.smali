.class public Lorg/achartengine/chart/RangeBarChart;
.super Lorg/achartengine/chart/BarChart;
.source "RangeBarChart.java"


# static fields
.field public static final TYPE:Ljava/lang/String; = "RangeBar"


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Lorg/achartengine/chart/BarChart;-><init>()V

    .line 19
    return-void
.end method

.method constructor <init>(Lorg/achartengine/chart/BarChart$Type;)V
    .locals 0
    .param p1, "type"    # Lorg/achartengine/chart/BarChart$Type;

    .prologue
    .line 23
    invoke-direct {p0, p1}, Lorg/achartengine/chart/BarChart;-><init>(Lorg/achartengine/chart/BarChart$Type;)V

    .line 24
    return-void
.end method

.method public constructor <init>(Lorg/achartengine/model/XYMultipleSeriesDataset;Lorg/achartengine/renderer/XYMultipleSeriesRenderer;Lorg/achartengine/chart/BarChart$Type;)V
    .locals 0
    .param p1, "dataset"    # Lorg/achartengine/model/XYMultipleSeriesDataset;
    .param p2, "renderer"    # Lorg/achartengine/renderer/XYMultipleSeriesRenderer;
    .param p3, "type"    # Lorg/achartengine/chart/BarChart$Type;

    .prologue
    .line 28
    invoke-direct {p0, p1, p2, p3}, Lorg/achartengine/chart/BarChart;-><init>(Lorg/achartengine/model/XYMultipleSeriesDataset;Lorg/achartengine/renderer/XYMultipleSeriesRenderer;Lorg/achartengine/chart/BarChart$Type;)V

    .line 29
    return-void
.end method

.method private drawTriangle(Landroid/graphics/Canvas;Landroid/graphics/Paint;[FFFF)V
    .locals 4
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "paint"    # Landroid/graphics/Paint;
    .param p3, "path"    # [F
    .param p4, "x"    # F
    .param p5, "y"    # F
    .param p6, "size"    # F

    .prologue
    const/4 v3, 0x3

    const/4 v2, 0x1

    .line 62
    const/4 v0, 0x0

    aput p4, p3, v0

    .line 63
    sub-float v0, p5, p6

    const/high16 v1, 0x40000000    # 2.0f

    div-float v1, p6, v1

    sub-float/2addr v0, v1

    aput v0, p3, v2

    .line 64
    const/4 v0, 0x2

    sub-float v1, p4, p6

    aput v1, p3, v0

    .line 65
    add-float v0, p5, p6

    aput v0, p3, v3

    .line 66
    const/4 v0, 0x4

    add-float v1, p4, p6

    aput v1, p3, v0

    .line 67
    const/4 v0, 0x5

    aget v1, p3, v3

    aput v1, p3, v0

    .line 68
    invoke-virtual {p0, p1, p3, p2, v2}, Lorg/achartengine/chart/RangeBarChart;->drawPath(Landroid/graphics/Canvas;[FLandroid/graphics/Paint;Z)V

    .line 69
    return-void
.end method


# virtual methods
.method protected drawChartValuesText(Landroid/graphics/Canvas;Lorg/achartengine/model/XYSeries;Lorg/achartengine/renderer/SimpleSeriesRenderer;Landroid/graphics/Paint;Ljava/util/List;II)V
    .locals 14
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "series"    # Lorg/achartengine/model/XYSeries;
    .param p3, "renderer"    # Lorg/achartengine/renderer/SimpleSeriesRenderer;
    .param p4, "paint"    # Landroid/graphics/Paint;
    .param p6, "seriesIndex"    # I
    .param p7, "startIndex"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/graphics/Canvas;",
            "Lorg/achartengine/model/XYSeries;",
            "Lorg/achartengine/renderer/SimpleSeriesRenderer;",
            "Landroid/graphics/Paint;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Float;",
            ">;II)V"
        }
    .end annotation

    .prologue
    .line 73
    .local p5, "points":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Float;>;"
    iget-object v2, p0, Lorg/achartengine/chart/RangeBarChart;->mDataset:Lorg/achartengine/model/XYMultipleSeriesDataset;

    invoke-virtual {v2}, Lorg/achartengine/model/XYMultipleSeriesDataset;->getSeriesCount()I

    move-result v12

    .line 74
    .local v12, "seriesNr":I
    invoke-interface/range {p5 .. p5}, Ljava/util/List;->size()I

    move-result v2

    move-object/from16 v0, p5

    invoke-virtual {p0, v0, v2, v12}, Lorg/achartengine/chart/RangeBarChart;->getHalfDiffX(Ljava/util/List;II)F

    move-result v9

    .line 75
    .local v9, "halfDiffX":F
    const/4 v13, 0x0

    .line 76
    .local v13, "start":I
    if-lez p7, :cond_0

    .line 77
    const/4 v13, 0x2

    .line 79
    :cond_0
    move v10, v13

    .local v10, "i":I
    :goto_0
    invoke-interface/range {p5 .. p5}, Ljava/util/List;->size()I

    move-result v2

    if-ge v10, v2, :cond_5

    .line 80
    div-int/lit8 v2, v10, 0x2

    add-int v11, p7, v2

    .line 81
    .local v11, "index":I
    move-object/from16 v0, p5

    invoke-interface {v0, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v5

    .line 82
    .local v5, "x":F
    iget-object v2, p0, Lorg/achartengine/chart/RangeBarChart;->mType:Lorg/achartengine/chart/BarChart$Type;

    sget-object v3, Lorg/achartengine/chart/BarChart$Type;->DEFAULT:Lorg/achartengine/chart/BarChart$Type;

    if-ne v2, v3, :cond_1

    .line 83
    mul-int/lit8 v2, p6, 0x2

    int-to-float v2, v2

    mul-float/2addr v2, v9

    int-to-float v3, v12

    const/high16 v4, 0x3fc00000    # 1.5f

    sub-float/2addr v3, v4

    mul-float/2addr v3, v9

    sub-float/2addr v2, v3

    add-float/2addr v5, v2

    .line 86
    :cond_1
    add-int/lit8 v2, v11, 0x1

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Lorg/achartengine/model/XYSeries;->getY(I)D

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, Lorg/achartengine/chart/RangeBarChart;->isNullValue(D)Z

    move-result v2

    if-nez v2, :cond_2

    invoke-interface/range {p5 .. p5}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v3, v10, 0x5

    if-le v2, v3, :cond_2

    .line 88
    invoke-virtual/range {p3 .. p3}, Lorg/achartengine/renderer/SimpleSeriesRenderer;->getChartValuesFormat()Ljava/text/NumberFormat;

    move-result-object v2

    add-int/lit8 v3, v11, 0x1

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Lorg/achartengine/model/XYSeries;->getY(I)D

    move-result-wide v6

    invoke-virtual {p0, v2, v6, v7}, Lorg/achartengine/chart/RangeBarChart;->getLabel(Ljava/text/NumberFormat;D)Ljava/lang/String;

    move-result-object v4

    add-int/lit8 v2, v10, 0x5

    move-object/from16 v0, p5

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    invoke-virtual/range {p3 .. p3}, Lorg/achartengine/renderer/SimpleSeriesRenderer;->getChartValuesSpacing()F

    move-result v3

    sub-float v6, v2, v3

    const/4 v8, 0x0

    move-object v2, p0

    move-object v3, p1

    move-object/from16 v7, p4

    invoke-virtual/range {v2 .. v8}, Lorg/achartengine/chart/RangeBarChart;->drawText(Landroid/graphics/Canvas;Ljava/lang/String;FFLandroid/graphics/Paint;F)V

    .line 91
    :cond_2
    move-object/from16 v0, p2

    invoke-virtual {v0, v11}, Lorg/achartengine/model/XYSeries;->getY(I)D

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, Lorg/achartengine/chart/RangeBarChart;->isNullValue(D)Z

    move-result v2

    if-nez v2, :cond_3

    invoke-interface/range {p5 .. p5}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v3, v10, 0x1

    if-le v2, v3, :cond_3

    .line 93
    invoke-virtual/range {p3 .. p3}, Lorg/achartengine/renderer/SimpleSeriesRenderer;->getChartValuesFormat()Ljava/text/NumberFormat;

    move-result-object v2

    move-object/from16 v0, p2

    invoke-virtual {v0, v11}, Lorg/achartengine/model/XYSeries;->getY(I)D

    move-result-wide v6

    invoke-virtual {p0, v2, v6, v7}, Lorg/achartengine/chart/RangeBarChart;->getLabel(Ljava/text/NumberFormat;D)Ljava/lang/String;

    move-result-object v4

    add-int/lit8 v2, v10, 0x1

    move-object/from16 v0, p5

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    invoke-virtual/range {p3 .. p3}, Lorg/achartengine/renderer/SimpleSeriesRenderer;->getChartValuesTextSize()F

    move-result v3

    add-float/2addr v2, v3

    invoke-virtual/range {p3 .. p3}, Lorg/achartengine/renderer/SimpleSeriesRenderer;->getChartValuesSpacing()F

    move-result v3

    add-float/2addr v2, v3

    const/high16 v3, 0x40400000    # 3.0f

    sub-float v6, v2, v3

    const/4 v8, 0x0

    move-object v2, p0

    move-object v3, p1

    move-object/from16 v7, p4

    invoke-virtual/range {v2 .. v8}, Lorg/achartengine/chart/RangeBarChart;->drawText(Landroid/graphics/Canvas;Ljava/lang/String;FFLandroid/graphics/Paint;F)V

    .line 96
    :cond_3
    add-int/lit8 v2, v11, 0x2

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Lorg/achartengine/model/XYSeries;->getY(I)D

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, Lorg/achartengine/chart/RangeBarChart;->isNullValue(D)Z

    move-result v2

    if-nez v2, :cond_4

    invoke-interface/range {p5 .. p5}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v3, v10, 0x3

    if-le v2, v3, :cond_4

    .line 97
    invoke-virtual/range {p3 .. p3}, Lorg/achartengine/renderer/SimpleSeriesRenderer;->getChartValuesFormat()Ljava/text/NumberFormat;

    move-result-object v2

    add-int/lit8 v3, v11, 0x2

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Lorg/achartengine/model/XYSeries;->getY(I)D

    move-result-wide v6

    invoke-virtual {p0, v2, v6, v7}, Lorg/achartengine/chart/RangeBarChart;->getLabel(Ljava/text/NumberFormat;D)Ljava/lang/String;

    move-result-object v4

    add-int/lit8 v2, v10, 0x3

    move-object/from16 v0, p5

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v6

    const/4 v8, 0x0

    move-object v2, p0

    move-object v3, p1

    move-object/from16 v7, p4

    invoke-virtual/range {v2 .. v8}, Lorg/achartengine/chart/RangeBarChart;->drawText(Landroid/graphics/Canvas;Ljava/lang/String;FFLandroid/graphics/Paint;F)V

    .line 79
    :cond_4
    add-int/lit8 v10, v10, 0x6

    goto/16 :goto_0

    .line 99
    .end local v5    # "x":F
    .end local v11    # "index":I
    :cond_5
    return-void
.end method

.method public drawSeries(Landroid/graphics/Canvas;Landroid/graphics/Paint;Ljava/util/List;Lorg/achartengine/renderer/SimpleSeriesRenderer;FII)V
    .locals 21
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "paint"    # Landroid/graphics/Paint;
    .param p4, "seriesRenderer"    # Lorg/achartengine/renderer/SimpleSeriesRenderer;
    .param p5, "yAxisValue"    # F
    .param p6, "seriesIndex"    # I
    .param p7, "startIndex"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/graphics/Canvas;",
            "Landroid/graphics/Paint;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Float;",
            ">;",
            "Lorg/achartengine/renderer/SimpleSeriesRenderer;",
            "FII)V"
        }
    .end annotation

    .prologue
    .line 33
    .local p3, "points":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Float;>;"
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/achartengine/chart/RangeBarChart;->mDataset:Lorg/achartengine/model/XYMultipleSeriesDataset;

    invoke-virtual {v3}, Lorg/achartengine/model/XYMultipleSeriesDataset;->getSeriesCount()I

    move-result v10

    .line 34
    .local v10, "seriesNr":I
    invoke-interface/range {p3 .. p3}, Ljava/util/List;->size()I

    move-result v19

    .line 35
    .local v19, "length":I
    invoke-virtual/range {p4 .. p4}, Lorg/achartengine/renderer/SimpleSeriesRenderer;->getColor()I

    move-result v3

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 36
    sget-object v3, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 37
    move-object/from16 v0, p0

    move-object/from16 v1, p3

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2, v10}, Lorg/achartengine/chart/RangeBarChart;->getHalfDiffX(Ljava/util/List;II)F

    move-result v9

    .line 38
    .local v9, "halfDiffX":F
    const/16 v20, 0x0

    .line 39
    .local v20, "start":I
    if-lez p7, :cond_0

    .line 40
    const/16 v20, 0x2

    .line 42
    :cond_0
    move/from16 v18, v20

    .local v18, "i":I
    :goto_0
    move/from16 v0, v18

    move/from16 v1, v19

    if-ge v0, v1, :cond_2

    .line 43
    invoke-interface/range {p3 .. p3}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v4, v18, 0x5

    if-le v3, v4, :cond_1

    .line 44
    move-object/from16 v0, p3

    move/from16 v1, v18

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Float;

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v5

    .line 45
    .local v5, "xMin":F
    add-int/lit8 v3, v18, 0x1

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Float;

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v6

    .line 47
    .local v6, "yMin":F
    add-int/lit8 v3, v18, 0x4

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Float;

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v7

    .line 48
    .local v7, "xMax":F
    add-int/lit8 v3, v18, 0x5

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Float;

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v8

    .local v8, "yMax":F
    move-object/from16 v3, p0

    move-object/from16 v4, p1

    move/from16 v11, p6

    move-object/from16 v12, p2

    .line 49
    invoke-virtual/range {v3 .. v12}, Lorg/achartengine/chart/RangeBarChart;->drawBar(Landroid/graphics/Canvas;FFFFFIILandroid/graphics/Paint;)V

    .line 53
    const/4 v3, 0x6

    new-array v14, v3, [F

    add-int/lit8 v3, v18, 0x2

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Float;

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v15

    add-int/lit8 v3, v18, 0x3

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Float;

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v16

    const/high16 v17, 0x40400000    # 3.0f

    move-object/from16 v11, p0

    move-object/from16 v12, p1

    move-object/from16 v13, p2

    invoke-direct/range {v11 .. v17}, Lorg/achartengine/chart/RangeBarChart;->drawTriangle(Landroid/graphics/Canvas;Landroid/graphics/Paint;[FFFF)V

    .line 42
    .end local v5    # "xMin":F
    .end local v6    # "yMin":F
    .end local v7    # "xMax":F
    .end local v8    # "yMax":F
    :cond_1
    add-int/lit8 v18, v18, 0x6

    goto :goto_0

    .line 57
    :cond_2
    invoke-virtual/range {p4 .. p4}, Lorg/achartengine/renderer/SimpleSeriesRenderer;->getColor()I

    move-result v3

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 58
    return-void
.end method

.method public getChartType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 108
    const-string/jumbo v0, "RangeBar"

    return-object v0
.end method

.method protected getCoeficient()F
    .locals 1

    .prologue
    .line 103
    const/high16 v0, 0x3f000000    # 0.5f

    return v0
.end method
