.class public Lorg/achartengine/chart/DoughnutChart;
.super Lorg/achartengine/chart/RoundChart;
.source "DoughnutChart.java"


# instance fields
.field private mDataset:Lorg/achartengine/model/CategorySeries;

.field private mStep:I


# direct methods
.method public constructor <init>(Lorg/achartengine/model/CategorySeries;Lorg/achartengine/renderer/DefaultRenderer;)V
    .locals 1
    .param p1, "dataset"    # Lorg/achartengine/model/CategorySeries;
    .param p2, "renderer"    # Lorg/achartengine/renderer/DefaultRenderer;

    .prologue
    .line 30
    const/4 v0, 0x0

    invoke-direct {p0, v0, p2}, Lorg/achartengine/chart/RoundChart;-><init>(Lorg/achartengine/model/CategorySeries;Lorg/achartengine/renderer/DefaultRenderer;)V

    .line 31
    iput-object p1, p0, Lorg/achartengine/chart/DoughnutChart;->mDataset:Lorg/achartengine/model/CategorySeries;

    .line 32
    return-void
.end method

.method public constructor <init>(Lorg/achartengine/model/MultipleCategorySeries;Lorg/achartengine/renderer/DefaultRenderer;)V
    .locals 1
    .param p1, "dataset"    # Lorg/achartengine/model/MultipleCategorySeries;
    .param p2, "renderer"    # Lorg/achartengine/renderer/DefaultRenderer;

    .prologue
    .line 24
    const/4 v0, 0x0

    invoke-direct {p0, v0, p2}, Lorg/achartengine/chart/RoundChart;-><init>(Lorg/achartengine/model/CategorySeries;Lorg/achartengine/renderer/DefaultRenderer;)V

    .line 26
    return-void
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;IIIILandroid/graphics/Paint;)V
    .locals 56
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "x"    # I
    .param p3, "y"    # I
    .param p4, "width"    # I
    .param p5, "height"    # I
    .param p6, "paint"    # Landroid/graphics/Paint;

    .prologue
    .line 37
    move-object/from16 v46, p6

    .line 38
    .local v46, "p":Landroid/graphics/Paint;
    const/high16 v2, -0x1000000

    move-object/from16 v0, p6

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 39
    move/from16 v0, p2

    int-to-float v3, v0

    move/from16 v0, p3

    int-to-float v4, v0

    add-int v2, p2, p4

    int-to-float v5, v2

    move/from16 v0, p3

    int-to-float v6, v0

    move-object/from16 v2, p1

    move-object/from16 v7, p6

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 40
    move/from16 v0, p2

    int-to-float v3, v0

    move/from16 v0, p3

    int-to-float v4, v0

    move/from16 v0, p2

    int-to-float v5, v0

    add-int v2, p3, p5

    int-to-float v6, v2

    move-object/from16 v2, p1

    move-object/from16 v7, p6

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 41
    move/from16 v0, p2

    int-to-float v3, v0

    add-int v2, p3, p5

    int-to-float v4, v2

    add-int v2, p2, p4

    int-to-float v5, v2

    add-int v2, p3, p5

    int-to-float v6, v2

    move-object/from16 v2, p1

    move-object/from16 v7, p6

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 42
    add-int v2, p2, p4

    int-to-float v3, v2

    move/from16 v0, p3

    int-to-float v4, v0

    add-int v2, p2, p4

    int-to-float v5, v2

    add-int v2, p3, p5

    int-to-float v6, v2

    move-object/from16 v2, p1

    move-object/from16 v7, p6

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 43
    move-object/from16 p6, v46

    .line 44
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/achartengine/chart/DoughnutChart;->mRenderer:Lorg/achartengine/renderer/DefaultRenderer;

    invoke-virtual {v2}, Lorg/achartengine/renderer/DefaultRenderer;->isAntialiasing()Z

    move-result v2

    move-object/from16 v0, p6

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 45
    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    move-object/from16 v0, p6

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 46
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/achartengine/chart/DoughnutChart;->mRenderer:Lorg/achartengine/renderer/DefaultRenderer;

    invoke-virtual {v2}, Lorg/achartengine/renderer/DefaultRenderer;->getLabelsTextSize()F

    move-result v2

    move-object/from16 v0, p6

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 47
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/achartengine/chart/DoughnutChart;->mRenderer:Lorg/achartengine/renderer/DefaultRenderer;

    div-int/lit8 v3, p5, 0x5

    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3, v4}, Lorg/achartengine/chart/DoughnutChart;->getLegendSize(Lorg/achartengine/renderer/DefaultRenderer;IF)I

    move-result v11

    .line 48
    .local v11, "legendSize":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/achartengine/chart/DoughnutChart;->mRenderer:Lorg/achartengine/renderer/DefaultRenderer;

    invoke-virtual {v2}, Lorg/achartengine/renderer/DefaultRenderer;->getMargins()[I

    move-result-object v45

    .line 49
    .local v45, "margin":[I
    move/from16 v6, p2

    .line 50
    .local v6, "left":I
    move/from16 v52, p3

    .line 51
    .local v52, "top":I
    add-int v2, p2, p4

    const/4 v3, 0x3

    aget v3, v45, v3

    sub-int v7, v2, v3

    .line 52
    .local v7, "right":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/achartengine/chart/DoughnutChart;->mDataset:Lorg/achartengine/model/CategorySeries;

    invoke-virtual {v2}, Lorg/achartengine/model/CategorySeries;->getItemCount()I

    move-result v39

    .line 53
    .local v39, "cLength":I
    move/from16 v0, v39

    new-array v5, v0, [Ljava/lang/String;

    .line 54
    .local v5, "categories":[Ljava/lang/String;
    const/16 v40, 0x0

    .local v40, "category":I
    :goto_0
    move/from16 v0, v40

    move/from16 v1, v39

    if-ge v0, v1, :cond_0

    .line 55
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/achartengine/chart/DoughnutChart;->mDataset:Lorg/achartengine/model/CategorySeries;

    move/from16 v0, v40

    invoke-virtual {v2, v0}, Lorg/achartengine/model/CategorySeries;->getCategory(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v5, v40

    .line 54
    add-int/lit8 v40, v40, 0x1

    goto :goto_0

    .line 57
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/achartengine/chart/DoughnutChart;->mRenderer:Lorg/achartengine/renderer/DefaultRenderer;

    invoke-virtual {v2}, Lorg/achartengine/renderer/DefaultRenderer;->isFitLegend()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 58
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/achartengine/chart/DoughnutChart;->mRenderer:Lorg/achartengine/renderer/DefaultRenderer;

    const/4 v13, 0x1

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move/from16 v8, p3

    move/from16 v9, p4

    move/from16 v10, p5

    move-object/from16 v12, p6

    invoke-virtual/range {v2 .. v13}, Lorg/achartengine/chart/DoughnutChart;->drawLegend(Landroid/graphics/Canvas;Lorg/achartengine/renderer/DefaultRenderer;[Ljava/lang/String;IIIIIILandroid/graphics/Paint;Z)I

    move-result v11

    .line 61
    :cond_1
    add-int v2, p3, p5

    sub-int v38, v2, v11

    .line 62
    .local v38, "bottom":I
    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/achartengine/chart/DoughnutChart;->mRenderer:Lorg/achartengine/renderer/DefaultRenderer;

    const/16 v20, 0x0

    const/16 v21, 0x0

    move-object/from16 v12, p0

    move-object/from16 v14, p1

    move/from16 v15, p2

    move/from16 v16, p3

    move/from16 v17, p4

    move/from16 v18, p5

    move-object/from16 v19, p6

    invoke-virtual/range {v12 .. v21}, Lorg/achartengine/chart/DoughnutChart;->drawBackground(Lorg/achartengine/renderer/DefaultRenderer;Landroid/graphics/Canvas;IIIILandroid/graphics/Paint;ZI)V

    .line 63
    const/4 v2, 0x7

    move-object/from16 v0, p0

    iput v2, v0, Lorg/achartengine/chart/DoughnutChart;->mStep:I

    .line 65
    sub-int v2, v7, v6

    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v2

    sub-int v3, v38, v52

    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v44

    .line 66
    .local v44, "mRadius":I
    const-wide v2, 0x3fd6666666666666L    # 0.35

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/achartengine/chart/DoughnutChart;->mRenderer:Lorg/achartengine/renderer/DefaultRenderer;

    invoke-virtual {v4}, Lorg/achartengine/renderer/DefaultRenderer;->getScale()F

    move-result v4

    float-to-double v8, v4

    mul-double v48, v2, v8

    .line 67
    .local v48, "rCoef":D
    const-wide/high16 v2, 0x3fe8000000000000L    # 0.75

    move/from16 v0, v39

    int-to-double v8, v0

    div-double v42, v2, v8

    .line 68
    .local v42, "decCoef":D
    move/from16 v0, v44

    int-to-double v2, v0

    mul-double v2, v2, v48

    double-to-int v0, v2

    move/from16 v47, v0

    .line 69
    .local v47, "radius":I
    move-object/from16 v0, p0

    iget v2, v0, Lorg/achartengine/chart/DoughnutChart;->mCenterX:I

    const v3, 0x7fffffff

    if-ne v2, v3, :cond_2

    .line 70
    add-int v2, v6, v7

    div-int/lit8 v2, v2, 0x2

    move-object/from16 v0, p0

    iput v2, v0, Lorg/achartengine/chart/DoughnutChart;->mCenterX:I

    .line 72
    :cond_2
    move-object/from16 v0, p0

    iget v2, v0, Lorg/achartengine/chart/DoughnutChart;->mCenterY:I

    const v3, 0x7fffffff

    if-ne v2, v3, :cond_3

    .line 73
    add-int v2, v38, v52

    div-int/lit8 v2, v2, 0x2

    move-object/from16 v0, p0

    iput v2, v0, Lorg/achartengine/chart/DoughnutChart;->mCenterY:I

    .line 75
    :cond_3
    move/from16 v0, v47

    int-to-float v2, v0

    const v3, 0x3f666666    # 0.9f

    mul-float v23, v2, v3

    .line 76
    .local v23, "shortRadius":F
    move/from16 v0, v47

    int-to-float v2, v0

    const v3, 0x3f8ccccd    # 1.1f

    mul-float v24, v2, v3

    .line 77
    .local v24, "longRadius":F
    new-instance v20, Ljava/util/ArrayList;

    invoke-direct/range {v20 .. v20}, Ljava/util/ArrayList;-><init>()V

    .line 79
    .local v20, "prevLabelsBounds":Ljava/util/List;
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/achartengine/chart/DoughnutChart;->mDataset:Lorg/achartengine/model/CategorySeries;

    invoke-virtual {v2}, Lorg/achartengine/model/CategorySeries;->getItemCount()I

    move-result v50

    .line 80
    .local v50, "sLength":I
    const-wide/16 v54, 0x0

    .line 81
    .local v54, "total":D
    move/from16 v0, v50

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v51, v0

    .line 82
    .local v51, "titles":[Ljava/lang/String;
    const/16 v41, 0x0

    .local v41, "i":I
    :goto_1
    move/from16 v0, v41

    move/from16 v1, v50

    if-ge v0, v1, :cond_4

    .line 83
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/achartengine/chart/DoughnutChart;->mDataset:Lorg/achartengine/model/CategorySeries;

    move/from16 v0, v41

    invoke-virtual {v2, v0}, Lorg/achartengine/model/CategorySeries;->getValue(I)D

    move-result-wide v2

    add-double v54, v54, v2

    .line 84
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/achartengine/chart/DoughnutChart;->mDataset:Lorg/achartengine/model/CategorySeries;

    move/from16 v0, v41

    invoke-virtual {v2, v0}, Lorg/achartengine/model/CategorySeries;->getCategory(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v51, v41

    .line 82
    add-int/lit8 v41, v41, 0x1

    goto :goto_1

    .line 86
    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/achartengine/chart/DoughnutChart;->mRenderer:Lorg/achartengine/renderer/DefaultRenderer;

    invoke-virtual {v2}, Lorg/achartengine/renderer/DefaultRenderer;->getStartAngle()F

    move-result v25

    .line 87
    .local v25, "currentAngle":F
    new-instance v13, Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget v2, v0, Lorg/achartengine/chart/DoughnutChart;->mCenterX:I

    sub-int v2, v2, v47

    int-to-float v2, v2

    move-object/from16 v0, p0

    iget v3, v0, Lorg/achartengine/chart/DoughnutChart;->mCenterY:I

    sub-int v3, v3, v47

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget v4, v0, Lorg/achartengine/chart/DoughnutChart;->mCenterX:I

    add-int v4, v4, v47

    int-to-float v4, v4

    move-object/from16 v0, p0

    iget v8, v0, Lorg/achartengine/chart/DoughnutChart;->mCenterY:I

    add-int v8, v8, v47

    int-to-float v8, v8

    invoke-direct {v13, v2, v3, v4, v8}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 89
    .local v13, "oval":Landroid/graphics/RectF;
    const/16 v41, 0x0

    :goto_2
    move/from16 v0, v41

    move/from16 v1, v50

    if-ge v0, v1, :cond_5

    .line 90
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/achartengine/chart/DoughnutChart;->mRenderer:Lorg/achartengine/renderer/DefaultRenderer;

    move/from16 v0, v41

    invoke-virtual {v2, v0}, Lorg/achartengine/renderer/DefaultRenderer;->getSeriesRendererAt(I)Lorg/achartengine/renderer/SimpleSeriesRenderer;

    move-result-object v2

    invoke-virtual {v2}, Lorg/achartengine/renderer/SimpleSeriesRenderer;->getColor()I

    move-result v2

    move-object/from16 v0, p6

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 91
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/achartengine/chart/DoughnutChart;->mDataset:Lorg/achartengine/model/CategorySeries;

    move/from16 v0, v41

    invoke-virtual {v2, v0}, Lorg/achartengine/model/CategorySeries;->getValue(I)D

    move-result-wide v2

    double-to-float v0, v2

    move/from16 v53, v0

    .line 92
    .local v53, "value":F
    move/from16 v0, v53

    float-to-double v2, v0

    div-double v2, v2, v54

    const-wide v8, 0x4076800000000000L    # 360.0

    mul-double/2addr v2, v8

    double-to-float v15, v2

    .line 93
    .local v15, "angle":F
    const/high16 v2, 0x42b40000    # 90.0f

    sub-float v14, v25, v2

    const/16 v16, 0x1

    move-object/from16 v12, p1

    move-object/from16 v17, p6

    invoke-virtual/range {v12 .. v17}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    .line 95
    aget-object v18, v51, v41

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/achartengine/chart/DoughnutChart;->mRenderer:Lorg/achartengine/renderer/DefaultRenderer;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/achartengine/chart/DoughnutChart;->mCenterX:I

    move/from16 v21, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/achartengine/chart/DoughnutChart;->mCenterY:I

    move/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/achartengine/chart/DoughnutChart;->mRenderer:Lorg/achartengine/renderer/DefaultRenderer;

    invoke-virtual {v2}, Lorg/achartengine/renderer/DefaultRenderer;->getLabelsColor()I

    move-result v29

    const/16 v31, 0x1

    const/16 v32, 0x0

    move-object/from16 v16, p0

    move-object/from16 v17, p1

    move/from16 v26, v15

    move/from16 v27, v6

    move/from16 v28, v7

    move-object/from16 v30, p6

    invoke-virtual/range {v16 .. v32}, Lorg/achartengine/chart/DoughnutChart;->drawLabel(Landroid/graphics/Canvas;Ljava/lang/String;Lorg/achartengine/renderer/DefaultRenderer;Ljava/util/List;IIFFFFIIILandroid/graphics/Paint;ZZ)V

    .line 97
    add-float v25, v25, v15

    .line 89
    add-int/lit8 v41, v41, 0x1

    goto :goto_2

    .line 99
    .end local v15    # "angle":F
    .end local v53    # "value":F
    :cond_5
    move/from16 v0, v47

    int-to-double v2, v0

    move/from16 v0, v44

    int-to-double v8, v0

    mul-double v8, v8, v42

    sub-double/2addr v2, v8

    double-to-int v0, v2

    move/from16 v47, v0

    .line 100
    move/from16 v0, v23

    float-to-double v2, v0

    move/from16 v0, v44

    int-to-double v8, v0

    mul-double v8, v8, v42

    const-wide/high16 v16, 0x4000000000000000L    # 2.0

    sub-double v8, v8, v16

    sub-double/2addr v2, v8

    double-to-float v0, v2

    move/from16 v23, v0

    .line 101
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/achartengine/chart/DoughnutChart;->mRenderer:Lorg/achartengine/renderer/DefaultRenderer;

    invoke-virtual {v2}, Lorg/achartengine/renderer/DefaultRenderer;->getBackgroundColor()I

    move-result v2

    if-eqz v2, :cond_6

    .line 102
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/achartengine/chart/DoughnutChart;->mRenderer:Lorg/achartengine/renderer/DefaultRenderer;

    invoke-virtual {v2}, Lorg/achartengine/renderer/DefaultRenderer;->getBackgroundColor()I

    move-result v2

    move-object/from16 v0, p6

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 106
    :goto_3
    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    move-object/from16 v0, p6

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 107
    new-instance v13, Landroid/graphics/RectF;

    .end local v13    # "oval":Landroid/graphics/RectF;
    move-object/from16 v0, p0

    iget v2, v0, Lorg/achartengine/chart/DoughnutChart;->mCenterX:I

    sub-int v2, v2, v47

    int-to-float v2, v2

    move-object/from16 v0, p0

    iget v3, v0, Lorg/achartengine/chart/DoughnutChart;->mCenterY:I

    sub-int v3, v3, v47

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget v4, v0, Lorg/achartengine/chart/DoughnutChart;->mCenterX:I

    add-int v4, v4, v47

    int-to-float v4, v4

    move-object/from16 v0, p0

    iget v8, v0, Lorg/achartengine/chart/DoughnutChart;->mCenterY:I

    add-int v8, v8, v47

    int-to-float v8, v8

    invoke-direct {v13, v2, v3, v4, v8}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 108
    .restart local v13    # "oval":Landroid/graphics/RectF;
    const/16 v28, 0x0

    const/high16 v29, 0x43b40000    # 360.0f

    const/16 v30, 0x1

    move-object/from16 v26, p1

    move-object/from16 v27, v13

    move-object/from16 v31, p6

    invoke-virtual/range {v26 .. v31}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    .line 109
    add-int/lit8 v47, v47, -0x1

    .line 111
    invoke-interface/range {v20 .. v20}, Ljava/util/List;->clear()V

    .line 112
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/achartengine/chart/DoughnutChart;->mRenderer:Lorg/achartengine/renderer/DefaultRenderer;

    move-object/from16 v28, v0

    const/16 v37, 0x0

    move-object/from16 v26, p0

    move-object/from16 v27, p1

    move-object/from16 v29, v5

    move/from16 v30, v6

    move/from16 v31, v7

    move/from16 v32, p3

    move/from16 v33, p4

    move/from16 v34, p5

    move/from16 v35, v11

    move-object/from16 v36, p6

    invoke-virtual/range {v26 .. v37}, Lorg/achartengine/chart/DoughnutChart;->drawLegend(Landroid/graphics/Canvas;Lorg/achartengine/renderer/DefaultRenderer;[Ljava/lang/String;IIIIIILandroid/graphics/Paint;Z)I

    move-object/from16 v26, p0

    move-object/from16 v27, p1

    move/from16 v28, p2

    move/from16 v29, p3

    move/from16 v30, p4

    move-object/from16 v31, p6

    .line 114
    invoke-virtual/range {v26 .. v31}, Lorg/achartengine/chart/DoughnutChart;->drawTitle(Landroid/graphics/Canvas;IIILandroid/graphics/Paint;)V

    .line 115
    return-void

    .line 104
    :cond_6
    const/4 v2, -0x1

    move-object/from16 v0, p6

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    goto :goto_3
.end method

.method public drawLegendShape(Landroid/graphics/Canvas;Lorg/achartengine/renderer/SimpleSeriesRenderer;FFILandroid/graphics/Paint;)V
    .locals 2
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "renderer"    # Lorg/achartengine/renderer/SimpleSeriesRenderer;
    .param p3, "x"    # F
    .param p4, "y"    # F
    .param p5, "seriesIndex"    # I
    .param p6, "paint"    # Landroid/graphics/Paint;

    .prologue
    .line 124
    iget v0, p0, Lorg/achartengine/chart/DoughnutChart;->mStep:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lorg/achartengine/chart/DoughnutChart;->mStep:I

    .line 125
    const/high16 v0, 0x41200000    # 10.0f

    add-float/2addr v0, p3

    iget v1, p0, Lorg/achartengine/chart/DoughnutChart;->mStep:I

    int-to-float v1, v1

    sub-float/2addr v0, v1

    iget v1, p0, Lorg/achartengine/chart/DoughnutChart;->mStep:I

    int-to-float v1, v1

    invoke-virtual {p1, v0, p4, v1, p6}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 126
    return-void
.end method

.method public getLegendShapeWidth(I)I
    .locals 1
    .param p1, "seriesIndex"    # I

    .prologue
    .line 119
    const/16 v0, 0xa

    return v0
.end method
