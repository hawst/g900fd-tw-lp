.class public final enum Lorg/achartengine/chart/ColumnChart$Type;
.super Ljava/lang/Enum;
.source "ColumnChart.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/achartengine/chart/ColumnChart;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Type"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lorg/achartengine/chart/ColumnChart$Type;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lorg/achartengine/chart/ColumnChart$Type;

.field public static final enum DEFAULT:Lorg/achartengine/chart/ColumnChart$Type;

.field public static final enum STACKED:Lorg/achartengine/chart/ColumnChart$Type;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 212
    new-instance v0, Lorg/achartengine/chart/ColumnChart$Type;

    const-string/jumbo v1, "DEFAULT"

    invoke-direct {v0, v1, v2}, Lorg/achartengine/chart/ColumnChart$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/achartengine/chart/ColumnChart$Type;->DEFAULT:Lorg/achartengine/chart/ColumnChart$Type;

    new-instance v0, Lorg/achartengine/chart/ColumnChart$Type;

    const-string/jumbo v1, "STACKED"

    invoke-direct {v0, v1, v3}, Lorg/achartengine/chart/ColumnChart$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/achartengine/chart/ColumnChart$Type;->STACKED:Lorg/achartengine/chart/ColumnChart$Type;

    .line 210
    const/4 v0, 0x2

    new-array v0, v0, [Lorg/achartengine/chart/ColumnChart$Type;

    sget-object v1, Lorg/achartengine/chart/ColumnChart$Type;->DEFAULT:Lorg/achartengine/chart/ColumnChart$Type;

    aput-object v1, v0, v2

    sget-object v1, Lorg/achartengine/chart/ColumnChart$Type;->STACKED:Lorg/achartengine/chart/ColumnChart$Type;

    aput-object v1, v0, v3

    sput-object v0, Lorg/achartengine/chart/ColumnChart$Type;->$VALUES:[Lorg/achartengine/chart/ColumnChart$Type;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 210
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lorg/achartengine/chart/ColumnChart$Type;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 210
    const-class v0, Lorg/achartengine/chart/ColumnChart$Type;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lorg/achartengine/chart/ColumnChart$Type;

    return-object v0
.end method

.method public static values()[Lorg/achartengine/chart/ColumnChart$Type;
    .locals 1

    .prologue
    .line 210
    sget-object v0, Lorg/achartengine/chart/ColumnChart$Type;->$VALUES:[Lorg/achartengine/chart/ColumnChart$Type;

    invoke-virtual {v0}, [Lorg/achartengine/chart/ColumnChart$Type;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lorg/achartengine/chart/ColumnChart$Type;

    return-object v0
.end method
