.class public Lorg/achartengine/chart/PieChart;
.super Lorg/achartengine/chart/RoundChart;
.source "PieChart.java"


# instance fields
.field private mPieMapper:Lorg/achartengine/chart/PieMapper;


# direct methods
.method public constructor <init>(Lorg/achartengine/model/CategorySeries;Lorg/achartengine/renderer/DefaultRenderer;)V
    .locals 1
    .param p1, "dataset"    # Lorg/achartengine/model/CategorySeries;
    .param p2, "renderer"    # Lorg/achartengine/renderer/DefaultRenderer;

    .prologue
    .line 23
    invoke-direct {p0, p1, p2}, Lorg/achartengine/chart/RoundChart;-><init>(Lorg/achartengine/model/CategorySeries;Lorg/achartengine/renderer/DefaultRenderer;)V

    .line 24
    new-instance v0, Lorg/achartengine/chart/PieMapper;

    invoke-direct {v0}, Lorg/achartengine/chart/PieMapper;-><init>()V

    iput-object v0, p0, Lorg/achartengine/chart/PieChart;->mPieMapper:Lorg/achartengine/chart/PieMapper;

    .line 25
    return-void
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;IIIILandroid/graphics/Paint;)V
    .locals 61
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "x"    # I
    .param p3, "y"    # I
    .param p4, "width"    # I
    .param p5, "height"    # I
    .param p6, "paint"    # Landroid/graphics/Paint;

    .prologue
    .line 29
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/achartengine/chart/PieChart;->mRenderer:Lorg/achartengine/renderer/DefaultRenderer;

    invoke-virtual {v4}, Lorg/achartengine/renderer/DefaultRenderer;->isAntialiasing()Z

    move-result v4

    move-object/from16 v0, p6

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 30
    sget-object v4, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    move-object/from16 v0, p6

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 31
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/achartengine/chart/PieChart;->mRenderer:Lorg/achartengine/renderer/DefaultRenderer;

    invoke-virtual {v4}, Lorg/achartengine/renderer/DefaultRenderer;->getMargins()[I

    move-result-object v52

    .line 32
    .local v52, "margin":[I
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/achartengine/chart/PieChart;->mRenderer:Lorg/achartengine/renderer/DefaultRenderer;

    invoke-virtual {v4}, Lorg/achartengine/renderer/DefaultRenderer;->getLabelsTextSize()F

    move-result v4

    move-object/from16 v0, p6

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 33
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/achartengine/chart/PieChart;->mRenderer:Lorg/achartengine/renderer/DefaultRenderer;

    div-int/lit8 v5, p5, 0x5

    const/4 v6, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v5, v6}, Lorg/achartengine/chart/PieChart;->getLegendSize(Lorg/achartengine/renderer/DefaultRenderer;IF)I

    move-result v13

    .line 34
    .local v13, "legendSize":I
    move/from16 v29, p2

    .line 35
    .local v29, "left":I
    move/from16 v57, p3

    .line 36
    .local v57, "top":I
    add-int v4, p2, p4

    const/4 v5, 0x3

    aget v5, v52, v5

    sub-int v30, v4, v5

    .line 37
    .local v30, "right":I
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/achartengine/chart/PieChart;->mDataset:Lorg/achartengine/model/CategorySeries;

    invoke-virtual {v4}, Lorg/achartengine/model/CategorySeries;->getItemCount()I

    move-result v55

    .line 38
    .local v55, "sLength":I
    const-wide/16 v58, 0x0

    .line 40
    .local v58, "total":D
    move-object/from16 v53, p6

    .line 41
    .local v53, "p":Landroid/graphics/Paint;
    const/high16 v4, -0x1000000

    move-object/from16 v0, p6

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 42
    move/from16 v0, p2

    int-to-float v5, v0

    move/from16 v0, p3

    int-to-float v6, v0

    add-int v4, p2, p4

    int-to-float v7, v4

    move/from16 v0, p3

    int-to-float v8, v0

    move-object/from16 v4, p1

    move-object/from16 v9, p6

    invoke-virtual/range {v4 .. v9}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 43
    move/from16 v0, p2

    int-to-float v5, v0

    move/from16 v0, p3

    int-to-float v6, v0

    move/from16 v0, p2

    int-to-float v7, v0

    add-int v4, p3, p5

    int-to-float v8, v4

    move-object/from16 v4, p1

    move-object/from16 v9, p6

    invoke-virtual/range {v4 .. v9}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 44
    move/from16 v0, p2

    int-to-float v5, v0

    add-int v4, p3, p5

    int-to-float v6, v4

    add-int v4, p2, p4

    int-to-float v7, v4

    add-int v4, p3, p5

    int-to-float v8, v4

    move-object/from16 v4, p1

    move-object/from16 v9, p6

    invoke-virtual/range {v4 .. v9}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 45
    add-int v4, p2, p4

    int-to-float v5, v4

    move/from16 v0, p3

    int-to-float v6, v0

    add-int v4, p2, p4

    int-to-float v7, v4

    add-int v4, p3, p5

    int-to-float v8, v4

    move-object/from16 v4, p1

    move-object/from16 v9, p6

    invoke-virtual/range {v4 .. v9}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 46
    move-object/from16 p6, v53

    .line 48
    move/from16 v0, v55

    new-array v7, v0, [Ljava/lang/String;

    .line 49
    .local v7, "titles":[Ljava/lang/String;
    const/16 v49, 0x0

    .local v49, "i":I
    :goto_0
    move/from16 v0, v49

    move/from16 v1, v55

    if-ge v0, v1, :cond_0

    .line 50
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/achartengine/chart/PieChart;->mDataset:Lorg/achartengine/model/CategorySeries;

    move/from16 v0, v49

    invoke-virtual {v4, v0}, Lorg/achartengine/model/CategorySeries;->getValue(I)D

    move-result-wide v4

    add-double v58, v58, v4

    .line 51
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/achartengine/chart/PieChart;->mDataset:Lorg/achartengine/model/CategorySeries;

    move/from16 v0, v49

    invoke-virtual {v4, v0}, Lorg/achartengine/model/CategorySeries;->getCategory(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v7, v49

    .line 49
    add-int/lit8 v49, v49, 0x1

    goto :goto_0

    .line 53
    :cond_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/achartengine/chart/PieChart;->mRenderer:Lorg/achartengine/renderer/DefaultRenderer;

    invoke-virtual {v4}, Lorg/achartengine/renderer/DefaultRenderer;->isFitLegend()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 54
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/achartengine/chart/PieChart;->mRenderer:Lorg/achartengine/renderer/DefaultRenderer;

    const/4 v15, 0x1

    move-object/from16 v4, p0

    move-object/from16 v5, p1

    move/from16 v8, v29

    move/from16 v9, v30

    move/from16 v10, p3

    move/from16 v11, p4

    move/from16 v12, p5

    move-object/from16 v14, p6

    invoke-virtual/range {v4 .. v15}, Lorg/achartengine/chart/PieChart;->drawLegend(Landroid/graphics/Canvas;Lorg/achartengine/renderer/DefaultRenderer;[Ljava/lang/String;IIIIIILandroid/graphics/Paint;Z)I

    move-result v13

    .line 57
    :cond_1
    add-int v4, p3, p5

    sub-int v48, v4, v13

    .line 58
    .local v48, "bottom":I
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/achartengine/chart/PieChart;->mRenderer:Lorg/achartengine/renderer/DefaultRenderer;

    const/16 v22, 0x0

    const/16 v23, 0x0

    move-object/from16 v14, p0

    move-object/from16 v16, p1

    move/from16 v17, p2

    move/from16 v18, p3

    move/from16 v19, p4

    move/from16 v20, p5

    move-object/from16 v21, p6

    invoke-virtual/range {v14 .. v23}, Lorg/achartengine/chart/PieChart;->drawBackground(Lorg/achartengine/renderer/DefaultRenderer;Landroid/graphics/Canvas;IIIILandroid/graphics/Paint;ZI)V

    .line 60
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/achartengine/chart/PieChart;->mRenderer:Lorg/achartengine/renderer/DefaultRenderer;

    invoke-virtual {v4}, Lorg/achartengine/renderer/DefaultRenderer;->getStartAngle()F

    move-result v27

    .line 61
    .local v27, "currentAngle":F
    sub-int v4, v30, v29

    invoke-static {v4}, Ljava/lang/Math;->abs(I)I

    move-result v4

    sub-int v5, v48, v57

    invoke-static {v5}, Ljava/lang/Math;->abs(I)I

    move-result v5

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v51

    .line 62
    .local v51, "mRadius":I
    move/from16 v0, v51

    int-to-double v4, v0

    const-wide v8, 0x3fd6666666666666L    # 0.35

    mul-double/2addr v4, v8

    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/achartengine/chart/PieChart;->mRenderer:Lorg/achartengine/renderer/DefaultRenderer;

    invoke-virtual {v6}, Lorg/achartengine/renderer/DefaultRenderer;->getScale()F

    move-result v6

    float-to-double v8, v6

    mul-double/2addr v4, v8

    double-to-int v0, v4

    move/from16 v54, v0

    .line 64
    .local v54, "radius":I
    move-object/from16 v0, p0

    iget v4, v0, Lorg/achartengine/chart/PieChart;->mCenterX:I

    const v5, 0x7fffffff

    if-ne v4, v5, :cond_2

    .line 65
    add-int v4, v29, v30

    div-int/lit8 v4, v4, 0x2

    move-object/from16 v0, p0

    iput v4, v0, Lorg/achartengine/chart/PieChart;->mCenterX:I

    .line 67
    :cond_2
    move-object/from16 v0, p0

    iget v4, v0, Lorg/achartengine/chart/PieChart;->mCenterY:I

    const v5, 0x7fffffff

    if-ne v4, v5, :cond_3

    .line 68
    add-int v4, v48, v57

    div-int/lit8 v4, v4, 0x2

    move-object/from16 v0, p0

    iput v4, v0, Lorg/achartengine/chart/PieChart;->mCenterY:I

    .line 71
    :cond_3
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/achartengine/chart/PieChart;->mPieMapper:Lorg/achartengine/chart/PieMapper;

    move-object/from16 v0, p0

    iget v5, v0, Lorg/achartengine/chart/PieChart;->mCenterX:I

    move-object/from16 v0, p0

    iget v6, v0, Lorg/achartengine/chart/PieChart;->mCenterY:I

    move/from16 v0, v54

    invoke-virtual {v4, v0, v5, v6}, Lorg/achartengine/chart/PieMapper;->setDimensions(III)V

    .line 72
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/achartengine/chart/PieChart;->mPieMapper:Lorg/achartengine/chart/PieMapper;

    move/from16 v0, v55

    invoke-virtual {v4, v0}, Lorg/achartengine/chart/PieMapper;->areAllSegmentPresent(I)Z

    move-result v4

    if-nez v4, :cond_7

    const/16 v50, 0x1

    .line 73
    .local v50, "loadPieCfg":Z
    :goto_1
    if-eqz v50, :cond_4

    .line 74
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/achartengine/chart/PieChart;->mPieMapper:Lorg/achartengine/chart/PieMapper;

    invoke-virtual {v4}, Lorg/achartengine/chart/PieMapper;->clearPieSegments()V

    .line 77
    :cond_4
    move/from16 v0, v54

    int-to-float v4, v0

    const v5, 0x3f666666    # 0.9f

    mul-float v25, v4, v5

    .line 78
    .local v25, "shortRadius":F
    move/from16 v0, v54

    int-to-float v4, v0

    const v5, 0x3f8ccccd    # 1.1f

    mul-float v26, v4, v5

    .line 79
    .local v26, "longRadius":F
    new-instance v15, Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget v4, v0, Lorg/achartengine/chart/PieChart;->mCenterX:I

    sub-int v4, v4, v54

    int-to-float v4, v4

    move-object/from16 v0, p0

    iget v5, v0, Lorg/achartengine/chart/PieChart;->mCenterY:I

    sub-int v5, v5, v54

    int-to-float v5, v5

    move-object/from16 v0, p0

    iget v6, v0, Lorg/achartengine/chart/PieChart;->mCenterX:I

    add-int v6, v6, v54

    int-to-float v6, v6

    move-object/from16 v0, p0

    iget v8, v0, Lorg/achartengine/chart/PieChart;->mCenterY:I

    add-int v8, v8, v54

    int-to-float v8, v8

    invoke-direct {v15, v4, v5, v6, v8}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 81
    .local v15, "oval":Landroid/graphics/RectF;
    new-instance v22, Ljava/util/ArrayList;

    invoke-direct/range {v22 .. v22}, Ljava/util/ArrayList;-><init>()V

    .line 83
    .local v22, "prevLabelsBounds":Ljava/util/List;
    const/16 v49, 0x0

    :goto_2
    move/from16 v0, v49

    move/from16 v1, v55

    if-ge v0, v1, :cond_8

    .line 84
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/achartengine/chart/PieChart;->mRenderer:Lorg/achartengine/renderer/DefaultRenderer;

    move/from16 v0, v49

    invoke-virtual {v4, v0}, Lorg/achartengine/renderer/DefaultRenderer;->getSeriesRendererAt(I)Lorg/achartengine/renderer/SimpleSeriesRenderer;

    move-result-object v56

    .line 85
    .local v56, "seriesRenderer":Lorg/achartengine/renderer/SimpleSeriesRenderer;
    invoke-virtual/range {v56 .. v56}, Lorg/achartengine/renderer/SimpleSeriesRenderer;->getColor()I

    move-result v4

    move-object/from16 v0, p6

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 87
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/achartengine/chart/PieChart;->mDataset:Lorg/achartengine/model/CategorySeries;

    move/from16 v0, v49

    invoke-virtual {v4, v0}, Lorg/achartengine/model/CategorySeries;->getValue(I)D

    move-result-wide v4

    double-to-float v0, v4

    move/from16 v60, v0

    .line 88
    .local v60, "value":F
    move/from16 v0, v60

    float-to-double v4, v0

    div-double v4, v4, v58

    const-wide v8, 0x4076800000000000L    # 360.0

    mul-double/2addr v4, v8

    double-to-float v0, v4

    move/from16 v17, v0

    .line 89
    .local v17, "angle":F
    const/high16 v4, 0x42b40000    # 90.0f

    sub-float v16, v27, v4

    const/16 v18, 0x1

    move-object/from16 v14, p1

    move-object/from16 v19, p6

    invoke-virtual/range {v14 .. v19}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    .line 90
    invoke-virtual/range {v56 .. v56}, Lorg/achartengine/renderer/SimpleSeriesRenderer;->getColor()I

    move-result v4

    move-object/from16 v0, p6

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 91
    const/4 v4, 0x0

    move-object/from16 v0, p6

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    .line 92
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/achartengine/chart/PieChart;->mDataset:Lorg/achartengine/model/CategorySeries;

    move/from16 v0, v49

    invoke-virtual {v4, v0}, Lorg/achartengine/model/CategorySeries;->getCategory(I)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/achartengine/chart/PieChart;->mRenderer:Lorg/achartengine/renderer/DefaultRenderer;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/achartengine/chart/PieChart;->mCenterX:I

    move/from16 v23, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/achartengine/chart/PieChart;->mCenterY:I

    move/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/achartengine/chart/PieChart;->mRenderer:Lorg/achartengine/renderer/DefaultRenderer;

    invoke-virtual {v4}, Lorg/achartengine/renderer/DefaultRenderer;->getLabelsColor()I

    move-result v31

    const/16 v33, 0x1

    const/16 v34, 0x0

    move-object/from16 v18, p0

    move-object/from16 v19, p1

    move/from16 v28, v17

    move-object/from16 v32, p6

    invoke-virtual/range {v18 .. v34}, Lorg/achartengine/chart/PieChart;->drawLabel(Landroid/graphics/Canvas;Ljava/lang/String;Lorg/achartengine/renderer/DefaultRenderer;Ljava/util/List;IIFFFFIIILandroid/graphics/Paint;ZZ)V

    .line 94
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/achartengine/chart/PieChart;->mRenderer:Lorg/achartengine/renderer/DefaultRenderer;

    invoke-virtual {v4}, Lorg/achartengine/renderer/DefaultRenderer;->isDisplayValues()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 95
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/achartengine/chart/PieChart;->mRenderer:Lorg/achartengine/renderer/DefaultRenderer;

    move/from16 v0, v49

    invoke-virtual {v4, v0}, Lorg/achartengine/renderer/DefaultRenderer;->getSeriesRendererAt(I)Lorg/achartengine/renderer/SimpleSeriesRenderer;

    move-result-object v4

    invoke-virtual {v4}, Lorg/achartengine/renderer/SimpleSeriesRenderer;->getChartValuesFormat()Ljava/text/NumberFormat;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/achartengine/chart/PieChart;->mDataset:Lorg/achartengine/model/CategorySeries;

    move/from16 v0, v49

    invoke-virtual {v5, v0}, Lorg/achartengine/model/CategorySeries;->getValue(I)D

    move-result-wide v8

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v8, v9}, Lorg/achartengine/chart/PieChart;->getLabel(Ljava/text/NumberFormat;D)Ljava/lang/String;

    move-result-object v33

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/achartengine/chart/PieChart;->mRenderer:Lorg/achartengine/renderer/DefaultRenderer;

    move-object/from16 v34, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/achartengine/chart/PieChart;->mCenterX:I

    move/from16 v36, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/achartengine/chart/PieChart;->mCenterY:I

    move/from16 v37, v0

    const/high16 v4, 0x40000000    # 2.0f

    div-float v38, v25, v4

    const/high16 v4, 0x40000000    # 2.0f

    div-float v39, v26, v4

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/achartengine/chart/PieChart;->mRenderer:Lorg/achartengine/renderer/DefaultRenderer;

    invoke-virtual {v4}, Lorg/achartengine/renderer/DefaultRenderer;->getLabelsColor()I

    move-result v44

    const/16 v46, 0x0

    const/16 v47, 0x1

    move-object/from16 v31, p0

    move-object/from16 v32, p1

    move-object/from16 v35, v22

    move/from16 v40, v27

    move/from16 v41, v17

    move/from16 v42, v29

    move/from16 v43, v30

    move-object/from16 v45, p6

    invoke-virtual/range {v31 .. v47}, Lorg/achartengine/chart/PieChart;->drawLabel(Landroid/graphics/Canvas;Ljava/lang/String;Lorg/achartengine/renderer/DefaultRenderer;Ljava/util/List;IIFFFFIIILandroid/graphics/Paint;ZZ)V

    .line 98
    :cond_5
    if-eqz v50, :cond_6

    .line 99
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/achartengine/chart/PieChart;->mPieMapper:Lorg/achartengine/chart/PieMapper;

    move/from16 v0, v49

    move/from16 v1, v60

    move/from16 v2, v27

    move/from16 v3, v17

    invoke-virtual {v4, v0, v1, v2, v3}, Lorg/achartengine/chart/PieMapper;->addPieSegment(IFFF)V

    .line 101
    :cond_6
    add-float v27, v27, v17

    .line 83
    add-int/lit8 v49, v49, 0x1

    goto/16 :goto_2

    .line 72
    .end local v15    # "oval":Landroid/graphics/RectF;
    .end local v17    # "angle":F
    .end local v22    # "prevLabelsBounds":Ljava/util/List;
    .end local v25    # "shortRadius":F
    .end local v26    # "longRadius":F
    .end local v50    # "loadPieCfg":Z
    .end local v56    # "seriesRenderer":Lorg/achartengine/renderer/SimpleSeriesRenderer;
    .end local v60    # "value":F
    :cond_7
    const/16 v50, 0x0

    goto/16 :goto_1

    .line 103
    .restart local v15    # "oval":Landroid/graphics/RectF;
    .restart local v22    # "prevLabelsBounds":Ljava/util/List;
    .restart local v25    # "shortRadius":F
    .restart local v26    # "longRadius":F
    .restart local v50    # "loadPieCfg":Z
    :cond_8
    invoke-interface/range {v22 .. v22}, Ljava/util/List;->clear()V

    .line 104
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/achartengine/chart/PieChart;->mRenderer:Lorg/achartengine/renderer/DefaultRenderer;

    move-object/from16 v33, v0

    const/16 v42, 0x0

    move-object/from16 v31, p0

    move-object/from16 v32, p1

    move-object/from16 v34, v7

    move/from16 v35, v29

    move/from16 v36, v30

    move/from16 v37, p3

    move/from16 v38, p4

    move/from16 v39, p5

    move/from16 v40, v13

    move-object/from16 v41, p6

    invoke-virtual/range {v31 .. v42}, Lorg/achartengine/chart/PieChart;->drawLegend(Landroid/graphics/Canvas;Lorg/achartengine/renderer/DefaultRenderer;[Ljava/lang/String;IIIIIILandroid/graphics/Paint;Z)I

    move-object/from16 v31, p0

    move-object/from16 v32, p1

    move/from16 v33, p2

    move/from16 v34, p3

    move/from16 v35, p4

    move-object/from16 v36, p6

    .line 105
    invoke-virtual/range {v31 .. v36}, Lorg/achartengine/chart/PieChart;->drawTitle(Landroid/graphics/Canvas;IIILandroid/graphics/Paint;)V

    .line 106
    return-void
.end method

.method public getSeriesAndPointForScreenCoordinate(Lorg/achartengine/model/Point;)Lorg/achartengine/model/SeriesSelection;
    .locals 1
    .param p1, "screenPoint"    # Lorg/achartengine/model/Point;

    .prologue
    .line 109
    iget-object v0, p0, Lorg/achartengine/chart/PieChart;->mPieMapper:Lorg/achartengine/chart/PieMapper;

    invoke-virtual {v0, p1}, Lorg/achartengine/chart/PieMapper;->getSeriesAndPointForScreenCoordinate(Lorg/achartengine/model/Point;)Lorg/achartengine/model/SeriesSelection;

    move-result-object v0

    return-object v0
.end method
