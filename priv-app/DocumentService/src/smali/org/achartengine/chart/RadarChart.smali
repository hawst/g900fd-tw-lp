.class public Lorg/achartengine/chart/RadarChart;
.super Ljava/lang/Object;
.source "RadarChart.java"


# static fields
.field private static SIZE:D = 0.0

.field private static color:[I = null

.field private static final mWidthOfLableText:F = 7.0f


# instance fields
.field private SCALE_FACTOR:D

.field private angles:[D

.field private dataset:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<[D>;"
        }
    .end annotation
.end field

.field private gridPaint:Landroid/graphics/Paint;

.field private gridPath:Landroid/graphics/Path;

.field private height:I

.field private mLabelSet:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mSeriesText:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private maxlength:I

.field private midpoint:Landroid/graphics/PointF;

.field private point:Landroid/graphics/PointF;

.field private width:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const/16 v0, 0xa

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lorg/achartengine/chart/RadarChart;->color:[I

    return-void

    :array_0
    .array-data 4
        -0xffff01
        -0x10000
        -0xff0100
        -0x1000000
        -0xff0001
        -0x777778
        -0x100
        -0xbbbbbc
        -0xff01
        -0x333334
    .end array-data
.end method

.method public constructor <init>(Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<[D>;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 51
    .local p1, "dataset":Ljava/util/ArrayList;, "Ljava/util/ArrayList<[D>;"
    .local p2, "labelSet":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .local p3, "seriesText":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lorg/achartengine/chart/RadarChart;->gridPath:Landroid/graphics/Path;

    .line 52
    iput-object p1, p0, Lorg/achartengine/chart/RadarChart;->dataset:Ljava/util/ArrayList;

    .line 53
    iput-object p2, p0, Lorg/achartengine/chart/RadarChart;->mLabelSet:Ljava/util/ArrayList;

    .line 54
    iput-object p3, p0, Lorg/achartengine/chart/RadarChart;->mSeriesText:Ljava/util/ArrayList;

    .line 55
    return-void
.end method

.method private drawGrid(IDDLandroid/graphics/Canvas;)I
    .locals 18
    .param p1, "length"    # I
    .param p2, "scaleFactor"    # D
    .param p4, "gridMaxVal"    # D
    .param p6, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 157
    const/4 v13, 0x0

    .local v13, "i":I
    :goto_0
    move/from16 v0, p1

    if-ge v13, v0, :cond_0

    .line 158
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/achartengine/chart/RadarChart;->angles:[D

    const/16 v4, 0x168

    div-int v4, v4, p1

    mul-int/2addr v4, v13

    int-to-double v4, v4

    aput-wide v4, v3, v13

    .line 159
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/achartengine/chart/RadarChart;->angles:[D

    aget-wide v8, v3, v13

    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/achartengine/chart/RadarChart;->midpoint:Landroid/graphics/PointF;

    move-object/from16 v3, p0

    move-wide/from16 v4, p4

    move-wide/from16 v6, p2

    invoke-direct/range {v3 .. v10}, Lorg/achartengine/chart/RadarChart;->toXYPoint(DDDLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v2

    .line 161
    .local v2, "dest":Landroid/graphics/PointF;
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/achartengine/chart/RadarChart;->gridPath:Landroid/graphics/Path;

    iget v4, v2, Landroid/graphics/PointF;->x:F

    iget v5, v2, Landroid/graphics/PointF;->y:F

    invoke-virtual {v3, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 162
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/achartengine/chart/RadarChart;->gridPath:Landroid/graphics/Path;

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/achartengine/chart/RadarChart;->midpoint:Landroid/graphics/PointF;

    iget v4, v4, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/achartengine/chart/RadarChart;->midpoint:Landroid/graphics/PointF;

    iget v5, v5, Landroid/graphics/PointF;->y:F

    invoke-virtual {v3, v4, v5}, Landroid/graphics/Path;->moveTo(FF)V

    .line 157
    add-int/lit8 v13, v13, 0x1

    goto :goto_0

    .line 164
    .end local v2    # "dest":Landroid/graphics/PointF;
    :cond_0
    invoke-direct/range {p0 .. p0}, Lorg/achartengine/chart/RadarChart;->getInterval()D

    move-result-wide v14

    .line 165
    .local v14, "interval":D
    div-double v4, p4, v14

    double-to-int v11, v4

    .line 166
    .local v11, "gridNum":I
    move/from16 v0, p1

    new-array v12, v0, [D

    .line 167
    .local v12, "gridSeries":[D
    const/16 v16, 0x0

    .local v16, "j":I
    :goto_1
    move/from16 v0, v16

    if-ge v0, v11, :cond_2

    .line 168
    const/4 v13, 0x0

    :goto_2
    move/from16 v0, p1

    if-ge v13, v0, :cond_1

    .line 169
    aput-wide p4, v12, v13

    .line 168
    add-int/lit8 v13, v13, 0x1

    goto :goto_2

    .line 171
    :cond_1
    sub-double p4, p4, v14

    .line 172
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/achartengine/chart/RadarChart;->gridPath:Landroid/graphics/Path;

    const/4 v4, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v12, v3, v4}, Lorg/achartengine/chart/RadarChart;->plotSeries([DLandroid/graphics/Path;Z)V

    .line 167
    add-int/lit8 v16, v16, 0x1

    goto :goto_1

    .line 175
    :cond_2
    return v11
.end method

.method private drawSingleSeries([DLandroid/graphics/Canvas;Landroid/graphics/Paint;)V
    .locals 3
    .param p1, "series"    # [D
    .param p2, "canvas"    # Landroid/graphics/Canvas;
    .param p3, "plotPaint"    # Landroid/graphics/Paint;

    .prologue
    .line 139
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    .line 140
    .local v0, "plotPath":Landroid/graphics/Path;
    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, v1}, Lorg/achartengine/chart/RadarChart;->plotSeries([DLandroid/graphics/Path;Z)V

    .line 141
    sget-object v1, Lorg/achartengine/chart/RadarChart;->color:[I

    iget-object v2, p0, Lorg/achartengine/chart/RadarChart;->dataset:Ljava/util/ArrayList;

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v2

    aget v1, v1, v2

    invoke-virtual {p3, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 142
    invoke-virtual {p2, v0, p3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 143
    return-void
.end method

.method private getInterval()D
    .locals 6

    .prologue
    .line 241
    iget-object v2, p0, Lorg/achartengine/chart/RadarChart;->dataset:Ljava/util/ArrayList;

    invoke-direct {p0, v2}, Lorg/achartengine/chart/RadarChart;->getMaxValOfSeries(Ljava/util/ArrayList;)D

    move-result-wide v2

    iget v4, p0, Lorg/achartengine/chart/RadarChart;->maxlength:I

    int-to-double v4, v4

    div-double/2addr v2, v4

    invoke-static {v2, v3}, Lorg/achartengine/util/MathHelper;->roundUp(D)D

    move-result-wide v0

    .line 243
    .local v0, "interval":D
    return-wide v0
.end method

.method private getMaxSizeOfSeries(Ljava/util/ArrayList;)I
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<[D>;)I"
        }
    .end annotation

    .prologue
    .line 211
    .local p1, "dataset":Ljava/util/ArrayList;, "Ljava/util/ArrayList<[D>;"
    const/4 v1, 0x0

    .line 212
    .local v1, "maxSize":I
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [D

    .line 213
    .local v2, "series":[D
    array-length v3, v2

    if-le v3, v1, :cond_0

    .line 214
    array-length v1, v2

    goto :goto_0

    .line 216
    .end local v2    # "series":[D
    :cond_1
    return v1
.end method

.method private getMaxValOfSeries(Ljava/util/ArrayList;)D
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<[D>;)D"
        }
    .end annotation

    .prologue
    .line 225
    .local p1, "dataset":Ljava/util/ArrayList;, "Ljava/util/ArrayList<[D>;"
    const-wide/16 v4, 0x0

    .line 226
    .local v4, "maxNum":D
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, [D

    .line 227
    .local v6, "series":[D
    move-object v0, v6

    .local v0, "arr$":[D
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_0

    aget-wide v8, v0, v2

    .line 228
    .local v8, "val":D
    cmpg-double v7, v4, v8

    if-gez v7, :cond_1

    .line 229
    move-wide v4, v8

    .line 227
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 232
    .end local v0    # "arr$":[D
    .end local v2    # "i$":I
    .end local v3    # "len$":I
    .end local v6    # "series":[D
    .end local v8    # "val":D
    :cond_2
    invoke-static {v4, v5}, Lorg/achartengine/util/MathHelper;->roundUp(D)D

    move-result-wide v10

    return-wide v10
.end method

.method private getMidPoint()Landroid/graphics/PointF;
    .locals 6

    .prologue
    const/high16 v5, 0x40000000    # 2.0f

    .line 111
    iget-object v2, p0, Lorg/achartengine/chart/RadarChart;->point:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->x:F

    iget v3, p0, Lorg/achartengine/chart/RadarChart;->width:I

    int-to-float v3, v3

    const/high16 v4, 0x40400000    # 3.0f

    div-float/2addr v3, v4

    add-float v0, v2, v3

    .line 112
    .local v0, "x":F
    iget-object v2, p0, Lorg/achartengine/chart/RadarChart;->point:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->y:F

    mul-float/2addr v2, v5

    iget v3, p0, Lorg/achartengine/chart/RadarChart;->height:I

    int-to-float v3, v3

    add-float/2addr v2, v3

    div-float v1, v2, v5

    .line 113
    .local v1, "y":F
    new-instance v2, Landroid/graphics/PointF;

    invoke-direct {v2, v0, v1}, Landroid/graphics/PointF;-><init>(FF)V

    return-object v2
.end method

.method private labelGraph(Landroid/graphics/Canvas;I)V
    .locals 38
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "gridNum"    # I
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "UseValueOf"
        }
    .end annotation

    .prologue
    .line 272
    const-wide/16 v8, 0x0

    invoke-static {v8, v9}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v33

    .line 273
    .local v33, "val":Ljava/lang/Double;
    sget-wide v8, Lorg/achartengine/chart/RadarChart;->SIZE:D

    const-wide/high16 v10, 0x4069000000000000L    # 200.0

    div-double v26, v8, v10

    .line 274
    .local v26, "labelScaleFactor":D
    invoke-direct/range {p0 .. p0}, Lorg/achartengine/chart/RadarChart;->getInterval()D

    move-result-wide v22

    .line 275
    .local v22, "interval":D
    new-instance v24, Landroid/graphics/Paint;

    const/4 v5, 0x1

    move-object/from16 v0, v24

    invoke-direct {v0, v5}, Landroid/graphics/Paint;-><init>(I)V

    .line 276
    .local v24, "labelPaint":Landroid/graphics/Paint;
    const/high16 v5, -0x1000000

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Landroid/graphics/Paint;->setColor(I)V

    .line 277
    sget-object v5, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 278
    const-wide/high16 v8, 0x4039000000000000L    # 25.0

    mul-double v8, v8, v26

    double-to-float v5, v8

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 281
    const/16 v19, 0x0

    .local v19, "i":I
    :goto_0
    move/from16 v0, v19

    move/from16 v1, p2

    if-gt v0, v1, :cond_0

    .line 282
    invoke-virtual/range {v33 .. v33}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v6

    move-object/from16 v0, p0

    iget-wide v8, v0, Lorg/achartengine/chart/RadarChart;->SCALE_FACTOR:D

    const-wide/16 v10, 0x0

    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/achartengine/chart/RadarChart;->midpoint:Landroid/graphics/PointF;

    move-object/from16 v5, p0

    invoke-direct/range {v5 .. v12}, Lorg/achartengine/chart/RadarChart;->toXYPoint(DDDLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v25

    .line 283
    .local v25, "labelPoint":Landroid/graphics/PointF;
    invoke-virtual/range {v33 .. v33}, Ljava/lang/Double;->toString()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v25

    iget v8, v0, Landroid/graphics/PointF;->x:F

    const/high16 v9, 0x40a00000    # 5.0f

    add-float/2addr v8, v9

    move-object/from16 v0, v25

    iget v9, v0, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    invoke-virtual {v0, v5, v8, v9, v1}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 285
    invoke-virtual/range {v33 .. v33}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v8

    add-double v8, v8, v22

    invoke-static {v8, v9}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v33

    .line 286
    invoke-virtual/range {v33 .. v33}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v8

    const-wide/high16 v10, 0x4059000000000000L    # 100.0

    mul-double/2addr v8, v10

    invoke-static {v8, v9}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v8

    const-wide/high16 v10, 0x4059000000000000L    # 100.0

    div-double/2addr v8, v10

    invoke-static {v8, v9}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v33

    .line 281
    add-int/lit8 v19, v19, 0x1

    goto :goto_0

    .line 289
    .end local v25    # "labelPoint":Landroid/graphics/PointF;
    :cond_0
    invoke-virtual/range {v33 .. v33}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v8

    const-wide/high16 v10, 0x4014000000000000L    # 5.0

    mul-double v10, v10, v22

    add-double v6, v8, v10

    .line 291
    .local v6, "indexPointVal":D
    move-object/from16 v0, p0

    iget-wide v8, v0, Lorg/achartengine/chart/RadarChart;->SCALE_FACTOR:D

    const-wide v10, 0x4056800000000000L    # 90.0

    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/achartengine/chart/RadarChart;->midpoint:Landroid/graphics/PointF;

    move-object/from16 v5, p0

    invoke-direct/range {v5 .. v12}, Lorg/achartengine/chart/RadarChart;->toXYPoint(DDDLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v29

    .line 293
    .local v29, "pl":Landroid/graphics/PointF;
    move-object/from16 v0, v29

    iget v5, v0, Landroid/graphics/PointF;->y:F

    float-to-double v8, v5

    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/achartengine/chart/RadarChart;->dataset:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    mul-int/lit8 v5, v5, 0x1e

    int-to-double v10, v5

    mul-double v10, v10, v26

    sub-double/2addr v8, v10

    double-to-float v5, v8

    move-object/from16 v0, v29

    iput v5, v0, Landroid/graphics/PointF;->y:F

    .line 295
    const/16 v19, 0x0

    :goto_1
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/achartengine/chart/RadarChart;->dataset:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    move/from16 v0, v19

    if-ge v0, v5, :cond_1

    .line 296
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/achartengine/chart/RadarChart;->gridPaint:Landroid/graphics/Paint;

    sget-object v8, Lorg/achartengine/chart/RadarChart;->color:[I

    aget v8, v8, v19

    invoke-virtual {v5, v8}, Landroid/graphics/Paint;->setColor(I)V

    .line 299
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/achartengine/chart/RadarChart;->point:Landroid/graphics/PointF;

    iget v5, v5, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, p0

    iget v8, v0, Lorg/achartengine/chart/RadarChart;->width:I

    move-object/from16 v0, p0

    iget v9, v0, Lorg/achartengine/chart/RadarChart;->width:I

    div-int/lit8 v9, v9, 0x5

    sub-int/2addr v8, v9

    int-to-float v8, v8

    add-float/2addr v5, v8

    move-object/from16 v0, v29

    iput v5, v0, Landroid/graphics/PointF;->x:F

    .line 301
    move-object/from16 v0, v29

    iget v9, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v29

    iget v5, v0, Landroid/graphics/PointF;->y:F

    float-to-double v10, v5

    mul-int/lit8 v5, v19, 0x1e

    int-to-double v12, v5

    mul-double v12, v12, v26

    add-double/2addr v10, v12

    double-to-float v10, v10

    move-object/from16 v0, v29

    iget v5, v0, Landroid/graphics/PointF;->x:F

    float-to-double v12, v5

    const-wide/high16 v36, 0x4034000000000000L    # 20.0

    mul-double v36, v36, v26

    add-double v12, v12, v36

    double-to-float v11, v12

    move-object/from16 v0, v29

    iget v5, v0, Landroid/graphics/PointF;->y:F

    float-to-double v12, v5

    mul-int/lit8 v5, v19, 0x1e

    int-to-double v0, v5

    move-wide/from16 v36, v0

    mul-double v36, v36, v26

    add-double v12, v12, v36

    double-to-float v12, v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/achartengine/chart/RadarChart;->gridPaint:Landroid/graphics/Paint;

    move-object/from16 v8, p1

    invoke-virtual/range {v8 .. v13}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 304
    move-object/from16 v0, v29

    iget v5, v0, Landroid/graphics/PointF;->x:F

    float-to-double v8, v5

    const-wide/high16 v10, 0x4034000000000000L    # 20.0

    mul-double v10, v10, v26

    add-double/2addr v8, v10

    double-to-float v5, v8

    move-object/from16 v0, v29

    iget v8, v0, Landroid/graphics/PointF;->y:F

    float-to-double v8, v8

    mul-int/lit8 v10, v19, 0x1e

    int-to-double v10, v10

    mul-double v10, v10, v26

    add-double/2addr v8, v10

    double-to-float v8, v8

    const-wide/high16 v10, 0x4008000000000000L    # 3.0

    mul-double v10, v10, v26

    double-to-float v9, v10

    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/achartengine/chart/RadarChart;->gridPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v8, v9, v10}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 307
    move-object/from16 v0, v29

    iget v5, v0, Landroid/graphics/PointF;->x:F

    float-to-double v8, v5

    const-wide/high16 v10, 0x4034000000000000L    # 20.0

    mul-double v10, v10, v26

    add-double/2addr v8, v10

    double-to-float v9, v8

    move-object/from16 v0, v29

    iget v5, v0, Landroid/graphics/PointF;->y:F

    float-to-double v10, v5

    mul-int/lit8 v5, v19, 0x1e

    int-to-double v12, v5

    mul-double v12, v12, v26

    add-double/2addr v10, v12

    double-to-float v10, v10

    move-object/from16 v0, v29

    iget v5, v0, Landroid/graphics/PointF;->x:F

    float-to-double v12, v5

    const-wide/high16 v36, 0x4044000000000000L    # 40.0

    mul-double v36, v36, v26

    add-double v12, v12, v36

    double-to-float v11, v12

    move-object/from16 v0, v29

    iget v5, v0, Landroid/graphics/PointF;->y:F

    float-to-double v12, v5

    mul-int/lit8 v5, v19, 0x1e

    int-to-double v0, v5

    move-wide/from16 v36, v0

    mul-double v36, v36, v26

    add-double v12, v12, v36

    double-to-float v12, v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/achartengine/chart/RadarChart;->gridPaint:Landroid/graphics/Paint;

    move-object/from16 v8, p1

    invoke-virtual/range {v8 .. v13}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 311
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/achartengine/chart/RadarChart;->mSeriesText:Ljava/util/ArrayList;

    move/from16 v0, v19

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    move-object/from16 v0, v29

    iget v8, v0, Landroid/graphics/PointF;->x:F

    float-to-double v8, v8

    const-wide/high16 v10, 0x404e000000000000L    # 60.0

    mul-double v10, v10, v26

    add-double/2addr v8, v10

    double-to-float v8, v8

    move-object/from16 v0, v29

    iget v9, v0, Landroid/graphics/PointF;->y:F

    float-to-double v10, v9

    mul-int/lit8 v9, v19, 0x1e

    int-to-double v12, v9

    mul-double v12, v12, v26

    add-double/2addr v10, v12

    double-to-float v9, v10

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    invoke-virtual {v0, v5, v8, v9, v1}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 295
    add-int/lit8 v19, v19, 0x1

    goto/16 :goto_1

    .line 316
    :cond_1
    const/16 v19, 0x0

    .line 317
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/achartengine/chart/RadarChart;->angles:[D

    .local v4, "arr$":[D
    array-length v0, v4

    move/from16 v28, v0

    .local v28, "len$":I
    const/16 v20, 0x0

    .local v20, "i$":I
    move/from16 v21, v20

    .end local v20    # "i$":I
    .local v21, "i$":I
    :goto_2
    move/from16 v0, v21

    move/from16 v1, v28

    if-ge v0, v1, :cond_8

    aget-wide v14, v4, v21

    .line 318
    .local v14, "angle":D
    invoke-virtual/range {v33 .. v33}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v10

    move-object/from16 v0, p0

    iget-wide v12, v0, Lorg/achartengine/chart/RadarChart;->SCALE_FACTOR:D

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/achartengine/chart/RadarChart;->midpoint:Landroid/graphics/PointF;

    move-object/from16 v16, v0

    move-object/from16 v9, p0

    invoke-direct/range {v9 .. v16}, Lorg/achartengine/chart/RadarChart;->toXYPoint(DDDLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v18

    .line 320
    .local v18, "axisNameLabelPoint":Landroid/graphics/PointF;
    const-wide/16 v8, 0x0

    cmpl-double v5, v14, v8

    if-eqz v5, :cond_2

    const-wide v8, 0x4066800000000000L    # 180.0

    cmpl-double v5, v14, v8

    if-nez v5, :cond_5

    .line 321
    :cond_2
    new-instance v31, Landroid/graphics/PointF;

    move-object/from16 v0, v18

    iget v8, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/achartengine/chart/RadarChart;->mLabelSet:Ljava/util/ArrayList;

    move/from16 v0, v19

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    mul-int/lit8 v5, v5, 0x5

    int-to-float v5, v5

    sub-float v5, v8, v5

    move-object/from16 v0, v18

    iget v8, v0, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, v31

    invoke-direct {v0, v5, v8}, Landroid/graphics/PointF;-><init>(FF)V

    .line 323
    .local v31, "startPoint":Landroid/graphics/PointF;
    new-instance v30, Landroid/graphics/Rect;

    invoke-direct/range {v30 .. v30}, Landroid/graphics/Rect;-><init>()V

    .line 324
    .local v30, "r":Landroid/graphics/Rect;
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/achartengine/chart/RadarChart;->mLabelSet:Ljava/util/ArrayList;

    move/from16 v0, v19

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    const/4 v9, 0x0

    move-object/from16 v0, p0

    iget-object v8, v0, Lorg/achartengine/chart/RadarChart;->mLabelSet:Ljava/util/ArrayList;

    move/from16 v0, v19

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    move-object/from16 v0, v24

    move-object/from16 v1, v30

    invoke-virtual {v0, v5, v9, v8, v1}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 326
    const-wide v8, 0x4066800000000000L    # 180.0

    cmpl-double v5, v14, v8

    if-nez v5, :cond_4

    .line 327
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/achartengine/chart/RadarChart;->mLabelSet:Ljava/util/ArrayList;

    move/from16 v0, v19

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    move-object/from16 v0, v31

    iget v8, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v30

    iget v9, v0, Landroid/graphics/Rect;->right:I

    move-object/from16 v0, v30

    iget v10, v0, Landroid/graphics/Rect;->left:I

    sub-int/2addr v9, v10

    int-to-float v9, v9

    add-float/2addr v8, v9

    move-object/from16 v0, v31

    iget v9, v0, Landroid/graphics/PointF;->y:F

    const/high16 v10, 0x40a00000    # 5.0f

    add-float/2addr v9, v10

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    invoke-virtual {v0, v5, v8, v9, v1}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 363
    .end local v21    # "i$":I
    .end local v31    # "startPoint":Landroid/graphics/PointF;
    :cond_3
    :goto_3
    add-int/lit8 v19, v19, 0x1

    .line 317
    add-int/lit8 v20, v21, 0x1

    .restart local v20    # "i$":I
    move/from16 v21, v20

    .end local v20    # "i$":I
    .restart local v21    # "i$":I
    goto/16 :goto_2

    .line 331
    .restart local v31    # "startPoint":Landroid/graphics/PointF;
    :cond_4
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/achartengine/chart/RadarChart;->mLabelSet:Ljava/util/ArrayList;

    move/from16 v0, v19

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    move-object/from16 v0, v31

    iget v8, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v30

    iget v9, v0, Landroid/graphics/Rect;->right:I

    move-object/from16 v0, v30

    iget v10, v0, Landroid/graphics/Rect;->left:I

    sub-int/2addr v9, v10

    int-to-float v9, v9

    add-float/2addr v8, v9

    move-object/from16 v0, v31

    iget v9, v0, Landroid/graphics/PointF;->y:F

    const/high16 v10, 0x40a00000    # 5.0f

    sub-float/2addr v9, v10

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    invoke-virtual {v0, v5, v8, v9, v1}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    goto :goto_3

    .line 335
    .end local v30    # "r":Landroid/graphics/Rect;
    .end local v31    # "startPoint":Landroid/graphics/PointF;
    :cond_5
    const-wide v8, 0x4066800000000000L    # 180.0

    cmpl-double v5, v14, v8

    if-lez v5, :cond_7

    .line 336
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/achartengine/chart/RadarChart;->mLabelSet:Ljava/util/ArrayList;

    move/from16 v0, v19

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    move-object/from16 v0, p0

    move-object/from16 v1, v24

    invoke-virtual {v0, v1, v5}, Lorg/achartengine/chart/RadarChart;->getStringArray(Landroid/graphics/Paint;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v17

    .line 338
    .local v17, "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v30, Landroid/graphics/Rect;

    invoke-direct/range {v30 .. v30}, Landroid/graphics/Rect;-><init>()V

    .line 339
    .restart local v30    # "r":Landroid/graphics/Rect;
    const/16 v34, 0x0

    .line 340
    .local v34, "x":I
    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v20

    .end local v21    # "i$":I
    .local v20, "i$":Ljava/util/Iterator;
    :goto_4
    invoke-interface/range {v20 .. v20}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface/range {v20 .. v20}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v32

    check-cast v32, Ljava/lang/String;

    .line 342
    .local v32, "str":Ljava/lang/String;
    const/4 v5, 0x0

    invoke-virtual/range {v32 .. v32}, Ljava/lang/String;->length()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    move-object/from16 v0, v24

    move-object/from16 v1, v32

    move-object/from16 v2, v30

    invoke-virtual {v0, v1, v5, v8, v2}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 343
    if-nez v34, :cond_6

    .line 344
    move-object/from16 v0, v18

    iget v5, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v30

    iget v8, v0, Landroid/graphics/Rect;->right:I

    move-object/from16 v0, v30

    iget v9, v0, Landroid/graphics/Rect;->left:I

    sub-int/2addr v8, v9

    add-int/lit8 v8, v8, -0x2

    int-to-float v8, v8

    sub-float/2addr v5, v8

    move-object/from16 v0, v18

    iput v5, v0, Landroid/graphics/PointF;->x:F

    .line 345
    :cond_6
    move-object/from16 v0, v18

    iget v5, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v18

    iget v8, v0, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, p1

    move-object/from16 v1, v32

    move-object/from16 v2, v24

    invoke-virtual {v0, v1, v5, v8, v2}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 347
    move-object/from16 v0, v18

    iget v5, v0, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, v30

    iget v8, v0, Landroid/graphics/Rect;->bottom:I

    move-object/from16 v0, v30

    iget v9, v0, Landroid/graphics/Rect;->top:I

    sub-int/2addr v8, v9

    add-int/lit8 v8, v8, 0x2

    int-to-float v8, v8

    add-float/2addr v5, v8

    move-object/from16 v0, v18

    iput v5, v0, Landroid/graphics/PointF;->y:F

    .line 348
    add-int/lit8 v34, v34, 0x1

    .line 349
    goto :goto_4

    .line 351
    .end local v17    # "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v20    # "i$":Ljava/util/Iterator;
    .end local v30    # "r":Landroid/graphics/Rect;
    .end local v32    # "str":Ljava/lang/String;
    .end local v34    # "x":I
    .restart local v21    # "i$":I
    :cond_7
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/achartengine/chart/RadarChart;->mLabelSet:Ljava/util/ArrayList;

    move/from16 v0, v19

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    move-object/from16 v0, p0

    move-object/from16 v1, v24

    invoke-virtual {v0, v1, v5}, Lorg/achartengine/chart/RadarChart;->getStringArray(Landroid/graphics/Paint;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v17

    .line 353
    .restart local v17    # "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v30, Landroid/graphics/Rect;

    invoke-direct/range {v30 .. v30}, Landroid/graphics/Rect;-><init>()V

    .line 354
    .restart local v30    # "r":Landroid/graphics/Rect;
    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v20

    .end local v21    # "i$":I
    .restart local v20    # "i$":Ljava/util/Iterator;
    :goto_5
    invoke-interface/range {v20 .. v20}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface/range {v20 .. v20}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v32

    check-cast v32, Ljava/lang/String;

    .line 356
    .restart local v32    # "str":Ljava/lang/String;
    const/4 v5, 0x0

    invoke-virtual/range {v32 .. v32}, Ljava/lang/String;->length()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    move-object/from16 v0, v24

    move-object/from16 v1, v32

    move-object/from16 v2, v30

    invoke-virtual {v0, v1, v5, v8, v2}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 357
    move-object/from16 v0, v18

    iget v5, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v18

    iget v8, v0, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, p1

    move-object/from16 v1, v32

    move-object/from16 v2, v24

    invoke-virtual {v0, v1, v5, v8, v2}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 359
    move-object/from16 v0, v18

    iget v5, v0, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, v30

    iget v8, v0, Landroid/graphics/Rect;->bottom:I

    move-object/from16 v0, v30

    iget v9, v0, Landroid/graphics/Rect;->top:I

    sub-int/2addr v8, v9

    add-int/lit8 v8, v8, 0x2

    int-to-float v8, v8

    add-float/2addr v5, v8

    move-object/from16 v0, v18

    iput v5, v0, Landroid/graphics/PointF;->y:F

    goto :goto_5

    .line 365
    .end local v14    # "angle":D
    .end local v17    # "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v18    # "axisNameLabelPoint":Landroid/graphics/PointF;
    .end local v20    # "i$":Ljava/util/Iterator;
    .end local v30    # "r":Landroid/graphics/Rect;
    .end local v32    # "str":Ljava/lang/String;
    .restart local v21    # "i$":I
    :cond_8
    return-void
.end method

.method private setGraphSize(I)V
    .locals 4
    .param p1, "height"    # I

    .prologue
    .line 253
    int-to-double v0, p1

    const-wide v2, 0x400999999999999aL    # 3.2

    div-double/2addr v0, v2

    sput-wide v0, Lorg/achartengine/chart/RadarChart;->SIZE:D

    .line 254
    return-void
.end method

.method private setScaleFactor(D)V
    .locals 3
    .param p1, "maxVal"    # D

    .prologue
    .line 262
    sget-wide v0, Lorg/achartengine/chart/RadarChart;->SIZE:D

    div-double/2addr v0, p1

    iput-wide v0, p0, Lorg/achartengine/chart/RadarChart;->SCALE_FACTOR:D

    .line 263
    return-void
.end method

.method private toXYPoint(DDDLandroid/graphics/PointF;)Landroid/graphics/PointF;
    .locals 13
    .param p1, "radius"    # D
    .param p3, "scaleFactor"    # D
    .param p5, "theta"    # D
    .param p7, "origin"    # Landroid/graphics/PointF;

    .prologue
    .line 124
    move-object/from16 v0, p7

    iget v4, v0, Landroid/graphics/PointF;->x:F

    float-to-double v4, v4

    mul-double v6, p1, p3

    const-wide v8, 0x4076800000000000L    # 360.0

    sub-double v8, v8, p5

    const-wide v10, 0x400921fb54442d18L    # Math.PI

    mul-double/2addr v8, v10

    const-wide v10, 0x4066800000000000L    # 180.0

    div-double/2addr v8, v10

    invoke-static {v8, v9}, Ljava/lang/Math;->sin(D)D

    move-result-wide v8

    mul-double/2addr v6, v8

    sub-double/2addr v4, v6

    double-to-float v2, v4

    .line 126
    .local v2, "x":F
    move-object/from16 v0, p7

    iget v4, v0, Landroid/graphics/PointF;->y:F

    float-to-double v4, v4

    mul-double v6, p1, p3

    const-wide v8, 0x4076800000000000L    # 360.0

    sub-double v8, v8, p5

    const-wide v10, 0x400921fb54442d18L    # Math.PI

    mul-double/2addr v8, v10

    const-wide v10, 0x4066800000000000L    # 180.0

    div-double/2addr v8, v10

    invoke-static {v8, v9}, Ljava/lang/Math;->cos(D)D

    move-result-wide v8

    mul-double/2addr v6, v8

    sub-double/2addr v4, v6

    double-to-float v3, v4

    .line 128
    .local v3, "y":F
    new-instance v4, Landroid/graphics/PointF;

    invoke-direct {v4, v2, v3}, Landroid/graphics/PointF;-><init>(FF)V

    return-object v4
.end method


# virtual methods
.method public drawChart(Landroid/graphics/Canvas;IIIILandroid/graphics/Paint;)V
    .locals 14
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "x"    # I
    .param p3, "y"    # I
    .param p4, "chartWidth"    # I
    .param p5, "chartHeight"    # I
    .param p6, "paint"    # Landroid/graphics/Paint;

    .prologue
    .line 61
    move/from16 v0, p4

    iput v0, p0, Lorg/achartengine/chart/RadarChart;->width:I

    .line 62
    move/from16 v0, p5

    iput v0, p0, Lorg/achartengine/chart/RadarChart;->height:I

    .line 63
    move-object/from16 v0, p6

    iput-object v0, p0, Lorg/achartengine/chart/RadarChart;->gridPaint:Landroid/graphics/Paint;

    .line 64
    new-instance v2, Landroid/graphics/PointF;

    move/from16 v0, p2

    int-to-float v3, v0

    move/from16 v0, p3

    int-to-float v4, v0

    invoke-direct {v2, v3, v4}, Landroid/graphics/PointF;-><init>(FF)V

    iput-object v2, p0, Lorg/achartengine/chart/RadarChart;->point:Landroid/graphics/PointF;

    .line 65
    iget-object v2, p0, Lorg/achartengine/chart/RadarChart;->gridPaint:Landroid/graphics/Paint;

    sget-object v3, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 67
    iget v2, p0, Lorg/achartengine/chart/RadarChart;->height:I

    invoke-direct {p0, v2}, Lorg/achartengine/chart/RadarChart;->setGraphSize(I)V

    .line 70
    move/from16 v0, p2

    int-to-float v3, v0

    move/from16 v0, p3

    int-to-float v4, v0

    add-int v2, p2, p4

    int-to-float v5, v2

    move/from16 v0, p3

    int-to-float v6, v0

    iget-object v7, p0, Lorg/achartengine/chart/RadarChart;->gridPaint:Landroid/graphics/Paint;

    move-object v2, p1

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 71
    move/from16 v0, p2

    int-to-float v3, v0

    move/from16 v0, p3

    int-to-float v4, v0

    move/from16 v0, p2

    int-to-float v5, v0

    add-int v2, p3, p5

    int-to-float v6, v2

    iget-object v7, p0, Lorg/achartengine/chart/RadarChart;->gridPaint:Landroid/graphics/Paint;

    move-object v2, p1

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 72
    add-int v2, p2, p4

    int-to-float v3, v2

    move/from16 v0, p3

    int-to-float v4, v0

    add-int v2, p2, p4

    int-to-float v5, v2

    add-int v2, p3, p5

    int-to-float v6, v2

    iget-object v7, p0, Lorg/achartengine/chart/RadarChart;->gridPaint:Landroid/graphics/Paint;

    move-object v2, p1

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 74
    move/from16 v0, p2

    int-to-float v3, v0

    add-int v2, p3, p5

    int-to-float v4, v2

    add-int v2, p2, p4

    int-to-float v5, v2

    add-int v2, p3, p5

    int-to-float v6, v2

    iget-object v7, p0, Lorg/achartengine/chart/RadarChart;->gridPaint:Landroid/graphics/Paint;

    move-object v2, p1

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 77
    iget-object v2, p0, Lorg/achartengine/chart/RadarChart;->dataset:Ljava/util/ArrayList;

    invoke-direct {p0, v2}, Lorg/achartengine/chart/RadarChart;->getMaxSizeOfSeries(Ljava/util/ArrayList;)I

    move-result v2

    iput v2, p0, Lorg/achartengine/chart/RadarChart;->maxlength:I

    .line 78
    iget v2, p0, Lorg/achartengine/chart/RadarChart;->maxlength:I

    new-array v2, v2, [D

    iput-object v2, p0, Lorg/achartengine/chart/RadarChart;->angles:[D

    .line 80
    invoke-direct {p0}, Lorg/achartengine/chart/RadarChart;->getMidPoint()Landroid/graphics/PointF;

    move-result-object v2

    iput-object v2, p0, Lorg/achartengine/chart/RadarChart;->midpoint:Landroid/graphics/PointF;

    .line 81
    iget-object v2, p0, Lorg/achartengine/chart/RadarChart;->gridPath:Landroid/graphics/Path;

    iget-object v3, p0, Lorg/achartengine/chart/RadarChart;->midpoint:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->x:F

    iget-object v4, p0, Lorg/achartengine/chart/RadarChart;->midpoint:Landroid/graphics/PointF;

    iget v4, v4, Landroid/graphics/PointF;->y:F

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 83
    iget-object v2, p0, Lorg/achartengine/chart/RadarChart;->dataset:Ljava/util/ArrayList;

    invoke-direct {p0, v2}, Lorg/achartengine/chart/RadarChart;->getMaxValOfSeries(Ljava/util/ArrayList;)D

    move-result-wide v12

    .line 84
    .local v12, "maxVal":D
    invoke-direct {p0, v12, v13}, Lorg/achartengine/chart/RadarChart;->setScaleFactor(D)V

    .line 86
    invoke-static {v12, v13}, Lorg/achartengine/util/MathHelper;->roundUp(D)D

    move-result-wide v6

    .line 87
    .local v6, "gridMaxVal":D
    iget v3, p0, Lorg/achartengine/chart/RadarChart;->maxlength:I

    iget-wide v4, p0, Lorg/achartengine/chart/RadarChart;->SCALE_FACTOR:D

    move-object v2, p0

    move-object v8, p1

    invoke-direct/range {v2 .. v8}, Lorg/achartengine/chart/RadarChart;->drawGrid(IDDLandroid/graphics/Canvas;)I

    move-result v9

    .line 89
    .local v9, "gridNum":I
    iget-object v2, p0, Lorg/achartengine/chart/RadarChart;->gridPath:Landroid/graphics/Path;

    iget-object v3, p0, Lorg/achartengine/chart/RadarChart;->gridPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 92
    iget-object v2, p0, Lorg/achartengine/chart/RadarChart;->gridPaint:Landroid/graphics/Paint;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 93
    iget-object v2, p0, Lorg/achartengine/chart/RadarChart;->gridPaint:Landroid/graphics/Paint;

    sget-object v3, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 94
    iget-object v2, p0, Lorg/achartengine/chart/RadarChart;->gridPaint:Landroid/graphics/Paint;

    const/high16 v3, 0x3fc00000    # 1.5f

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 98
    iget-object v2, p0, Lorg/achartengine/chart/RadarChart;->dataset:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    .local v10, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, [D

    .line 99
    .local v11, "series":[D
    iget-object v2, p0, Lorg/achartengine/chart/RadarChart;->gridPaint:Landroid/graphics/Paint;

    invoke-direct {p0, v11, p1, v2}, Lorg/achartengine/chart/RadarChart;->drawSingleSeries([DLandroid/graphics/Canvas;Landroid/graphics/Paint;)V

    goto :goto_0

    .line 102
    .end local v11    # "series":[D
    :cond_0
    invoke-direct {p0, p1, v9}, Lorg/achartengine/chart/RadarChart;->labelGraph(Landroid/graphics/Canvas;I)V

    .line 103
    return-void
.end method

.method public getStringArray(Landroid/graphics/Paint;Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 13
    .param p1, "labelPaint"    # Landroid/graphics/Paint;
    .param p2, "longString"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/graphics/Paint;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/high16 v12, 0x40e00000    # 7.0f

    const/4 v11, 0x0

    .line 369
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 370
    .local v5, "stringArray":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 372
    .local v6, "stringBuilder":Ljava/lang/StringBuilder;
    new-instance v3, Landroid/graphics/Rect;

    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    .line 373
    .local v3, "r":Landroid/graphics/Rect;
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v9

    add-int/lit8 v9, v9, -0x1

    invoke-virtual {p1, p2, v11, v9, v3}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 375
    const-string/jumbo v9, " "

    invoke-virtual {p2, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    .line 377
    .local v7, "subStrings":[Ljava/lang/String;
    array-length v9, v7

    const/4 v10, 0x1

    if-ne v9, v10, :cond_4

    .line 379
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v9

    add-int/lit8 v9, v9, -0x1

    invoke-virtual {p1, p2, v11, v9, v3}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 380
    iget v9, v3, Landroid/graphics/Rect;->right:I

    iget v10, v3, Landroid/graphics/Rect;->left:I

    sub-int/2addr v9, v10

    int-to-float v9, v9

    iget v10, p0, Lorg/achartengine/chart/RadarChart;->width:I

    int-to-float v10, v10

    div-float/2addr v10, v12

    cmpl-float v9, v9, v10

    if-lez v9, :cond_3

    .line 381
    const/4 v8, 0x2

    .line 382
    .local v8, "x":I
    const/4 v1, 0x1

    .line 383
    .local v1, "breaking":Z
    move-object v0, p2

    .line 385
    .local v0, "actualString":Ljava/lang/String;
    :cond_0
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v9

    div-int/lit8 v9, v9, 0x2

    invoke-virtual {p2, v11, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p2

    .line 387
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v9

    add-int/lit8 v9, v9, -0x1

    invoke-virtual {p1, p2, v11, v9, v3}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 389
    iget v9, v3, Landroid/graphics/Rect;->right:I

    iget v10, v3, Landroid/graphics/Rect;->left:I

    sub-int/2addr v9, v10

    int-to-float v9, v9

    iget v10, p0, Lorg/achartengine/chart/RadarChart;->width:I

    int-to-float v10, v10

    div-float/2addr v10, v12

    cmpl-float v9, v9, v10

    if-lez v9, :cond_1

    .line 390
    add-int/lit8 v8, v8, 0x1

    .line 401
    :goto_0
    if-nez v1, :cond_0

    .line 429
    .end local v0    # "actualString":Ljava/lang/String;
    .end local v1    # "breaking":Z
    .end local v8    # "x":I
    :goto_1
    return-object v5

    .line 392
    .restart local v0    # "actualString":Ljava/lang/String;
    .restart local v1    # "breaking":Z
    .restart local v8    # "x":I
    :cond_1
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v9

    div-int v4, v9, v8

    .line 393
    .local v4, "strArrayLength":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_2
    if-ge v2, v8, :cond_2

    .line 394
    invoke-virtual {v0, v11, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v5, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 396
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v9

    invoke-virtual {v0, v4, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 393
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 399
    :cond_2
    const/4 v1, 0x0

    goto :goto_0

    .line 403
    .end local v0    # "actualString":Ljava/lang/String;
    .end local v1    # "breaking":Z
    .end local v2    # "i":I
    .end local v4    # "strArrayLength":I
    .end local v8    # "x":I
    :cond_3
    invoke-virtual {v5, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 409
    :cond_4
    iget v9, v3, Landroid/graphics/Rect;->right:I

    iget v10, v3, Landroid/graphics/Rect;->left:I

    sub-int/2addr v9, v10

    int-to-float v9, v9

    iget v10, p0, Lorg/achartengine/chart/RadarChart;->width:I

    int-to-float v10, v10

    div-float/2addr v10, v12

    cmpl-float v9, v9, v10

    if-lez v9, :cond_7

    .line 410
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_3
    array-length v9, v7

    if-ge v2, v9, :cond_6

    .line 411
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, " "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    aget-object v10, v7, v2

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 412
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v10

    add-int/lit8 v10, v10, -0x1

    invoke-virtual {p1, v9, v11, v10, v3}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 414
    iget v9, v3, Landroid/graphics/Rect;->right:I

    iget v10, v3, Landroid/graphics/Rect;->left:I

    sub-int/2addr v9, v10

    int-to-float v9, v9

    iget v10, p0, Lorg/achartengine/chart/RadarChart;->width:I

    int-to-float v10, v10

    div-float/2addr v10, v12

    cmpl-float v9, v9, v10

    if-lez v9, :cond_5

    .line 415
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->length()I

    move-result v9

    aget-object v10, v7, v2

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v10

    sub-int/2addr v9, v10

    invoke-virtual {v6, v11, v9}, Ljava/lang/StringBuilder;->substring(II)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v5, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 417
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->length()I

    move-result v9

    invoke-virtual {v6, v11, v9}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 418
    add-int/lit8 v2, v2, -0x1

    .line 410
    :cond_5
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 423
    :cond_6
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->length()I

    move-result v9

    invoke-virtual {v6, v11, v9}, Ljava/lang/StringBuilder;->substring(II)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v5, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 425
    .end local v2    # "i":I
    :cond_7
    invoke-virtual {v5, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1
.end method

.method public plotSeries([DLandroid/graphics/Path;Z)V
    .locals 16
    .param p1, "series"    # [D
    .param p2, "plotPath"    # Landroid/graphics/Path;
    .param p3, "isGrid"    # Z

    .prologue
    .line 186
    const/4 v11, 0x0

    .line 187
    .local v11, "init":Landroid/graphics/PointF;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    move-object/from16 v0, p1

    array-length v3, v0

    if-ge v2, v3, :cond_2

    .line 188
    aget-wide v4, p1, v2

    move-object/from16 v0, p0

    iget-wide v6, v0, Lorg/achartengine/chart/RadarChart;->SCALE_FACTOR:D

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/achartengine/chart/RadarChart;->angles:[D

    aget-wide v8, v3, v2

    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/achartengine/chart/RadarChart;->midpoint:Landroid/graphics/PointF;

    move-object/from16 v3, p0

    invoke-direct/range {v3 .. v10}, Lorg/achartengine/chart/RadarChart;->toXYPoint(DDDLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v12

    .line 189
    .local v12, "p":Landroid/graphics/PointF;
    if-nez v2, :cond_0

    .line 190
    iget v3, v12, Landroid/graphics/PointF;->x:F

    iget v4, v12, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, p2

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 191
    move-object v11, v12

    .line 192
    if-nez p3, :cond_0

    .line 193
    iget v3, v12, Landroid/graphics/PointF;->x:F

    iget v4, v12, Landroid/graphics/PointF;->y:F

    const/high16 v5, 0x40400000    # 3.0f

    sget-object v6, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    move-object/from16 v0, p2

    invoke-virtual {v0, v3, v4, v5, v6}, Landroid/graphics/Path;->addCircle(FFFLandroid/graphics/Path$Direction;)V

    .line 195
    :cond_0
    iget v3, v12, Landroid/graphics/PointF;->x:F

    iget v4, v12, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, p2

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 196
    if-nez p3, :cond_1

    .line 197
    iget v3, v12, Landroid/graphics/PointF;->x:F

    iget v4, v12, Landroid/graphics/PointF;->y:F

    const-wide/high16 v6, 0x4008000000000000L    # 3.0

    sget-wide v8, Lorg/achartengine/chart/RadarChart;->SIZE:D

    const-wide/high16 v14, 0x4069000000000000L    # 200.0

    div-double/2addr v8, v14

    mul-double/2addr v6, v8

    double-to-float v5, v6

    sget-object v6, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    move-object/from16 v0, p2

    invoke-virtual {v0, v3, v4, v5, v6}, Landroid/graphics/Path;->addCircle(FFFLandroid/graphics/Path$Direction;)V

    .line 187
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 201
    .end local v12    # "p":Landroid/graphics/PointF;
    :cond_2
    move-object/from16 v0, p1

    array-length v3, v0

    move-object/from16 v0, p0

    iget v4, v0, Lorg/achartengine/chart/RadarChart;->maxlength:I

    if-ne v3, v4, :cond_3

    if-eqz v11, :cond_3

    .line 202
    iget v3, v11, Landroid/graphics/PointF;->x:F

    iget v4, v11, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, p2

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 203
    :cond_3
    return-void
.end method
