.class public abstract Lorg/achartengine/chart/XYChart;
.super Lorg/achartengine/chart/AbstractChart;
.source "XYChart.java"


# instance fields
.field private clickableAreas:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/util/List",
            "<",
            "Lorg/achartengine/chart/ClickableArea;",
            ">;>;"
        }
    .end annotation
.end field

.field private final mCalcRange:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "[D>;"
        }
    .end annotation
.end field

.field private mCenter:Lorg/achartengine/model/Point;

.field protected mDataset:Lorg/achartengine/model/XYMultipleSeriesDataset;

.field protected mRenderer:Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

.field private mScale:F

.field private mScreenR:Landroid/graphics/Rect;

.field private mTranslate:F


# direct methods
.method protected constructor <init>()V
    .locals 1

    .prologue
    .line 45
    invoke-direct {p0}, Lorg/achartengine/chart/AbstractChart;-><init>()V

    .line 40
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lorg/achartengine/chart/XYChart;->mCalcRange:Ljava/util/Map;

    .line 42
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lorg/achartengine/chart/XYChart;->clickableAreas:Ljava/util/Map;

    .line 46
    return-void
.end method

.method public constructor <init>(Lorg/achartengine/model/XYMultipleSeriesDataset;Lorg/achartengine/renderer/XYMultipleSeriesRenderer;)V
    .locals 1
    .param p1, "dataset"    # Lorg/achartengine/model/XYMultipleSeriesDataset;
    .param p2, "renderer"    # Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    .prologue
    .line 49
    invoke-direct {p0}, Lorg/achartengine/chart/AbstractChart;-><init>()V

    .line 40
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lorg/achartengine/chart/XYChart;->mCalcRange:Ljava/util/Map;

    .line 42
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lorg/achartengine/chart/XYChart;->clickableAreas:Ljava/util/Map;

    .line 50
    iput-object p1, p0, Lorg/achartengine/chart/XYChart;->mDataset:Lorg/achartengine/model/XYMultipleSeriesDataset;

    .line 51
    iput-object p2, p0, Lorg/achartengine/chart/XYChart;->mRenderer:Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    .line 52
    return-void
.end method

.method private getLabelLinePos(Landroid/graphics/Paint$Align;)I
    .locals 2
    .param p1, "align"    # Landroid/graphics/Paint$Align;

    .prologue
    .line 724
    const/4 v0, 0x4

    .line 725
    .local v0, "pos":I
    sget-object v1, Landroid/graphics/Paint$Align;->LEFT:Landroid/graphics/Paint$Align;

    if-ne p1, v1, :cond_0

    .line 726
    neg-int v0, v0

    .line 728
    :cond_0
    return v0
.end method

.method private getValidLabels(Ljava/util/List;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Double;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Double;",
            ">;"
        }
    .end annotation

    .prologue
    .line 488
    .local p1, "labels":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Double;>;"
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 489
    .local v2, "result":Ljava/util/List;
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Double;

    .line 490
    .local v1, "label":Ljava/lang/Double;
    invoke-virtual {v1}, Ljava/lang/Double;->isNaN()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 491
    invoke-interface {v2, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    .line 494
    .end local v1    # "label":Ljava/lang/Double;
    :cond_1
    return-object v2
.end method

.method private setStroke(Landroid/graphics/Paint$Cap;Landroid/graphics/Paint$Join;FLandroid/graphics/Paint$Style;Landroid/graphics/PathEffect;Landroid/graphics/Paint;)V
    .locals 0
    .param p1, "cap"    # Landroid/graphics/Paint$Cap;
    .param p2, "join"    # Landroid/graphics/Paint$Join;
    .param p3, "miter"    # F
    .param p4, "style"    # Landroid/graphics/Paint$Style;
    .param p5, "pathEffect"    # Landroid/graphics/PathEffect;
    .param p6, "paint"    # Landroid/graphics/Paint;

    .prologue
    .line 538
    invoke-virtual {p6, p1}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    .line 539
    invoke-virtual {p6, p2}, Landroid/graphics/Paint;->setStrokeJoin(Landroid/graphics/Paint$Join;)V

    .line 540
    invoke-virtual {p6, p3}, Landroid/graphics/Paint;->setStrokeMiter(F)V

    .line 541
    invoke-virtual {p6, p5}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 542
    invoke-virtual {p6, p4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 543
    return-void
.end method

.method private transform(Landroid/graphics/Canvas;FZ)V
    .locals 3
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "angle"    # F
    .param p3, "inverse"    # Z

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    .line 594
    if-eqz p3, :cond_0

    .line 595
    iget v0, p0, Lorg/achartengine/chart/XYChart;->mScale:F

    div-float v0, v2, v0

    iget v1, p0, Lorg/achartengine/chart/XYChart;->mScale:F

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->scale(FF)V

    .line 596
    iget v0, p0, Lorg/achartengine/chart/XYChart;->mTranslate:F

    iget v1, p0, Lorg/achartengine/chart/XYChart;->mTranslate:F

    neg-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 597
    neg-float v0, p2

    iget-object v1, p0, Lorg/achartengine/chart/XYChart;->mCenter:Lorg/achartengine/model/Point;

    invoke-virtual {v1}, Lorg/achartengine/model/Point;->getX()F

    move-result v1

    iget-object v2, p0, Lorg/achartengine/chart/XYChart;->mCenter:Lorg/achartengine/model/Point;

    invoke-virtual {v2}, Lorg/achartengine/model/Point;->getY()F

    move-result v2

    invoke-virtual {p1, v0, v1, v2}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 603
    :goto_0
    return-void

    .line 599
    :cond_0
    iget-object v0, p0, Lorg/achartengine/chart/XYChart;->mCenter:Lorg/achartengine/model/Point;

    invoke-virtual {v0}, Lorg/achartengine/model/Point;->getX()F

    move-result v0

    iget-object v1, p0, Lorg/achartengine/chart/XYChart;->mCenter:Lorg/achartengine/model/Point;

    invoke-virtual {v1}, Lorg/achartengine/model/Point;->getY()F

    move-result v1

    invoke-virtual {p1, p2, v0, v1}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 600
    iget v0, p0, Lorg/achartengine/chart/XYChart;->mTranslate:F

    neg-float v0, v0

    iget v1, p0, Lorg/achartengine/chart/XYChart;->mTranslate:F

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 601
    iget v0, p0, Lorg/achartengine/chart/XYChart;->mScale:F

    iget v1, p0, Lorg/achartengine/chart/XYChart;->mScale:F

    div-float v1, v2, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->scale(FF)V

    goto :goto_0
.end method


# virtual methods
.method protected abstract clickableAreasForPoints(Ljava/util/List;Ljava/util/List;FII)[Lorg/achartengine/chart/ClickableArea;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Float;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Double;",
            ">;FII)[",
            "Lorg/achartengine/chart/ClickableArea;"
        }
    .end annotation
.end method

.method public draw(Landroid/graphics/Canvas;IIIILandroid/graphics/Paint;)V
    .locals 122
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "x"    # I
    .param p3, "y"    # I
    .param p4, "width"    # I
    .param p5, "height"    # I
    .param p6, "paint"    # Landroid/graphics/Paint;

    .prologue
    .line 62
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/achartengine/chart/XYChart;->mRenderer:Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    invoke-virtual {v4}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->isAntialiasing()Z

    move-result v4

    move-object/from16 v0, p6

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 63
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/achartengine/chart/XYChart;->mRenderer:Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    div-int/lit8 v5, p5, 0x5

    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/achartengine/chart/XYChart;->mRenderer:Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    invoke-virtual {v6}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->getAxisTitleTextSize()F

    move-result v6

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v5, v6}, Lorg/achartengine/chart/XYChart;->getLegendSize(Lorg/achartengine/renderer/DefaultRenderer;IF)I

    move-result v13

    .line 64
    .local v13, "legendSize":I
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/achartengine/chart/XYChart;->mRenderer:Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    invoke-virtual {v4}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->getMargins()[I

    move-result-object v84

    .line 65
    .local v84, "margins":[I
    const/4 v4, 0x1

    aget v4, v84, v4

    add-int v81, p2, v4

    .line 66
    .local v81, "left":I
    const/4 v4, 0x0

    aget v4, v84, v4

    add-int v111, p3, v4

    .line 67
    .local v111, "top":I
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/achartengine/chart/XYChart;->mDataset:Lorg/achartengine/model/XYMultipleSeriesDataset;

    invoke-virtual {v4}, Lorg/achartengine/model/XYMultipleSeriesDataset;->getSeriesCount()I

    move-result v103

    .line 68
    .local v103, "sLength":I
    move/from16 v0, v103

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v110, v0

    .line 69
    .local v110, "titles":[Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/achartengine/chart/XYChart;->mRenderer:Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    invoke-virtual {v4}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->getOrientation()Lorg/achartengine/renderer/XYMultipleSeriesRenderer$Orientation;

    move-result-object v93

    .line 71
    .local v93, "or":Lorg/achartengine/renderer/XYMultipleSeriesRenderer$Orientation;
    sget-object v4, Lorg/achartengine/renderer/XYMultipleSeriesRenderer$Orientation;->HORIZONTAL:Lorg/achartengine/renderer/XYMultipleSeriesRenderer$Orientation;

    move-object/from16 v0, v93

    if-ne v0, v4, :cond_0

    .line 72
    move-object/from16 v98, p6

    .line 73
    .local v98, "p":Landroid/graphics/Paint;
    const/high16 v4, -0x1000000

    move-object/from16 v0, p6

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 74
    move/from16 v0, p2

    int-to-float v5, v0

    move/from16 v0, p3

    int-to-float v6, v0

    add-int v4, p2, p4

    int-to-float v7, v4

    move/from16 v0, p3

    int-to-float v8, v0

    move-object/from16 v4, p1

    move-object/from16 v9, p6

    invoke-virtual/range {v4 .. v9}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 75
    move/from16 v0, p2

    int-to-float v5, v0

    move/from16 v0, p3

    int-to-float v6, v0

    move/from16 v0, p2

    int-to-float v7, v0

    add-int v4, p3, p5

    int-to-float v8, v4

    move-object/from16 v4, p1

    move-object/from16 v9, p6

    invoke-virtual/range {v4 .. v9}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 76
    move/from16 v0, p2

    int-to-float v5, v0

    add-int v4, p3, p5

    int-to-float v6, v4

    add-int v4, p2, p4

    int-to-float v7, v4

    add-int v4, p3, p5

    int-to-float v8, v4

    move-object/from16 v4, p1

    move-object/from16 v9, p6

    invoke-virtual/range {v4 .. v9}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 77
    add-int v4, p2, p4

    int-to-float v5, v4

    move/from16 v0, p3

    int-to-float v6, v0

    add-int v4, p2, p4

    int-to-float v7, v4

    add-int v4, p3, p5

    int-to-float v8, v4

    move-object/from16 v4, p1

    move-object/from16 v9, p6

    invoke-virtual/range {v4 .. v9}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 78
    move-object/from16 p6, v98

    .line 81
    .end local v98    # "p":Landroid/graphics/Paint;
    :cond_0
    const/16 v21, 0x0

    .local v21, "i":I
    :goto_0
    move/from16 v0, v21

    move/from16 v1, v103

    if-ge v0, v1, :cond_1

    .line 82
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/achartengine/chart/XYChart;->mDataset:Lorg/achartengine/model/XYMultipleSeriesDataset;

    move/from16 v0, v21

    invoke-virtual {v4, v0}, Lorg/achartengine/model/XYMultipleSeriesDataset;->getSeriesAt(I)Lorg/achartengine/model/XYSeries;

    move-result-object v4

    invoke-virtual {v4}, Lorg/achartengine/model/XYSeries;->getTitle()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v110, v21

    .line 81
    add-int/lit8 v21, v21, 0x1

    goto :goto_0

    .line 84
    :cond_1
    add-int v4, p2, p4

    const/4 v5, 0x3

    aget v5, v84, v5

    sub-int v9, v4, v5

    .line 85
    .local v9, "right":I
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/achartengine/chart/XYChart;->mRenderer:Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    invoke-virtual {v4}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->isFitLegend()Z

    move-result v4

    if-eqz v4, :cond_2

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/achartengine/chart/XYChart;->mRenderer:Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    invoke-virtual {v4}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->isShowLegend()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 86
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/achartengine/chart/XYChart;->mRenderer:Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    const/4 v15, 0x1

    move-object/from16 v4, p0

    move-object/from16 v5, p1

    move-object/from16 v7, v110

    move/from16 v8, v81

    move/from16 v10, p3

    move/from16 v11, p4

    move/from16 v12, p5

    move-object/from16 v14, p6

    invoke-virtual/range {v4 .. v15}, Lorg/achartengine/chart/XYChart;->drawLegend(Landroid/graphics/Canvas;Lorg/achartengine/renderer/DefaultRenderer;[Ljava/lang/String;IIIIIILandroid/graphics/Paint;Z)I

    move-result v13

    .line 88
    :cond_2
    sub-int/2addr v9, v13

    .line 89
    add-int v4, p3, p5

    const/4 v5, 0x2

    aget v5, v84, v5

    sub-int v33, v4, v5

    .line 90
    .local v33, "bottom":I
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/achartengine/chart/XYChart;->mScreenR:Landroid/graphics/Rect;

    if-nez v4, :cond_3

    .line 91
    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4}, Landroid/graphics/Rect;-><init>()V

    move-object/from16 v0, p0

    iput-object v4, v0, Lorg/achartengine/chart/XYChart;->mScreenR:Landroid/graphics/Rect;

    .line 93
    :cond_3
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/achartengine/chart/XYChart;->mScreenR:Landroid/graphics/Rect;

    move/from16 v0, v81

    move/from16 v1, v111

    move/from16 v2, v33

    invoke-virtual {v4, v0, v1, v9, v2}, Landroid/graphics/Rect;->set(IIII)V

    .line 94
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/achartengine/chart/XYChart;->mRenderer:Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    const/16 v22, 0x0

    const/16 v23, 0x0

    move-object/from16 v14, p0

    move-object/from16 v16, p1

    move/from16 v17, p2

    move/from16 v18, p3

    move/from16 v19, p4

    move/from16 v20, p5

    move-object/from16 v21, p6

    invoke-virtual/range {v14 .. v23}, Lorg/achartengine/chart/XYChart;->drawBackground(Lorg/achartengine/renderer/DefaultRenderer;Landroid/graphics/Canvas;IIIILandroid/graphics/Paint;ZI)V

    .line 96
    .end local v21    # "i":I
    invoke-virtual/range {p6 .. p6}, Landroid/graphics/Paint;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v4

    if-eqz v4, :cond_5

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/achartengine/chart/XYChart;->mRenderer:Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    invoke-virtual {v4}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->getTextTypeface()Landroid/graphics/Typeface;

    move-result-object v4

    if-eqz v4, :cond_4

    invoke-virtual/range {p6 .. p6}, Landroid/graphics/Paint;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/achartengine/chart/XYChart;->mRenderer:Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    invoke-virtual {v5}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->getTextTypeface()Landroid/graphics/Typeface;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/graphics/Typeface;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_5

    :cond_4
    invoke-virtual/range {p6 .. p6}, Landroid/graphics/Paint;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/achartengine/chart/XYChart;->mRenderer:Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    invoke-virtual {v5}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->getTextTypefaceName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-virtual/range {p6 .. p6}, Landroid/graphics/Paint;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v4

    invoke-virtual {v4}, Landroid/graphics/Typeface;->getStyle()I

    move-result v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/achartengine/chart/XYChart;->mRenderer:Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    invoke-virtual {v5}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->getTextTypefaceStyle()I

    move-result v5

    if-eq v4, v5, :cond_6

    .line 98
    :cond_5
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/achartengine/chart/XYChart;->mRenderer:Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    invoke-virtual {v4}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->getTextTypeface()Landroid/graphics/Typeface;

    move-result-object v4

    if-eqz v4, :cond_c

    .line 99
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/achartengine/chart/XYChart;->mRenderer:Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    invoke-virtual {v4}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->getTextTypeface()Landroid/graphics/Typeface;

    move-result-object v4

    move-object/from16 v0, p6

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 106
    :cond_6
    :goto_1
    sget-object v4, Lorg/achartengine/renderer/XYMultipleSeriesRenderer$Orientation;->VERTICAL:Lorg/achartengine/renderer/XYMultipleSeriesRenderer$Orientation;

    move-object/from16 v0, v93

    if-ne v0, v4, :cond_7

    .line 107
    sub-int/2addr v9, v13

    .line 108
    add-int/lit8 v4, v13, -0x14

    add-int v33, v33, v4

    .line 110
    :cond_7
    invoke-virtual/range {v93 .. v93}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer$Orientation;->getAngle()I

    move-result v66

    .line 111
    .local v66, "angle":I
    const/16 v4, 0x5a

    move/from16 v0, v66

    if-ne v0, v4, :cond_d

    const/16 v102, 0x1

    .line 112
    .local v102, "rotate":Z
    :goto_2
    move/from16 v0, p5

    move/from16 v1, p4

    if-gt v0, v1, :cond_e

    .line 113
    move/from16 v0, p4

    int-to-float v4, v0

    move/from16 v0, p5

    int-to-float v5, v0

    div-float/2addr v4, v5

    move-object/from16 v0, p0

    iput v4, v0, Lorg/achartengine/chart/XYChart;->mScale:F

    .line 116
    :goto_3
    sub-int v4, p4, p5

    invoke-static {v4}, Ljava/lang/Math;->abs(I)I

    move-result v4

    int-to-float v4, v4

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    move-object/from16 v0, p0

    iput v4, v0, Lorg/achartengine/chart/XYChart;->mTranslate:F

    .line 117
    sget-object v4, Lorg/achartengine/renderer/XYMultipleSeriesRenderer$Orientation;->VERTICAL:Lorg/achartengine/renderer/XYMultipleSeriesRenderer$Orientation;

    move-object/from16 v0, v93

    if-ne v0, v4, :cond_8

    .line 118
    sub-int v4, p4, p5

    invoke-static {v4}, Ljava/lang/Math;->abs(I)I

    move-result v4

    int-to-float v4, v4

    move-object/from16 v0, p0

    iput v4, v0, Lorg/achartengine/chart/XYChart;->mTranslate:F

    .line 119
    move-object/from16 v0, p0

    iget v4, v0, Lorg/achartengine/chart/XYChart;->mTranslate:F

    const/high16 v5, -0x40800000    # -1.0f

    mul-float/2addr v4, v5

    move-object/from16 v0, p0

    iput v4, v0, Lorg/achartengine/chart/XYChart;->mTranslate:F

    .line 121
    :cond_8
    move-object/from16 v0, p0

    iget v4, v0, Lorg/achartengine/chart/XYChart;->mScale:F

    const/high16 v5, 0x3f800000    # 1.0f

    cmpg-float v4, v4, v5

    if-gez v4, :cond_9

    .line 122
    move-object/from16 v0, p0

    iget v4, v0, Lorg/achartengine/chart/XYChart;->mTranslate:F

    const/high16 v5, -0x40800000    # -1.0f

    mul-float/2addr v4, v5

    move-object/from16 v0, p0

    iput v4, v0, Lorg/achartengine/chart/XYChart;->mTranslate:F

    .line 124
    :cond_9
    new-instance v4, Lorg/achartengine/model/Point;

    add-int v5, p2, p4

    int-to-float v5, v5

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v5, v6

    add-int v6, p3, p5

    int-to-float v6, v6

    const/high16 v7, 0x40000000    # 2.0f

    div-float/2addr v6, v7

    invoke-direct {v4, v5, v6}, Lorg/achartengine/model/Point;-><init>(FF)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lorg/achartengine/chart/XYChart;->mCenter:Lorg/achartengine/model/Point;

    .line 125
    if-eqz v102, :cond_a

    .line 126
    move/from16 v0, v66

    int-to-float v4, v0

    const/4 v5, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v4, v5}, Lorg/achartengine/chart/XYChart;->transform(Landroid/graphics/Canvas;FZ)V

    .line 129
    :cond_a
    sget-object v4, Lorg/achartengine/renderer/XYMultipleSeriesRenderer$Orientation;->VERTICAL:Lorg/achartengine/renderer/XYMultipleSeriesRenderer$Orientation;

    move-object/from16 v0, v93

    if-ne v0, v4, :cond_b

    .line 130
    move-object/from16 v98, p6

    .line 131
    .restart local v98    # "p":Landroid/graphics/Paint;
    const/high16 v4, -0x1000000

    move-object/from16 v0, p6

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 132
    move/from16 v0, p2

    int-to-float v15, v0

    move/from16 v0, p3

    int-to-float v0, v0

    move/from16 v16, v0

    add-int v4, p2, p4

    int-to-float v0, v4

    move/from16 v17, v0

    move/from16 v0, p3

    int-to-float v0, v0

    move/from16 v18, v0

    move-object/from16 v14, p1

    move-object/from16 v19, p6

    invoke-virtual/range {v14 .. v19}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 133
    move/from16 v0, p2

    int-to-float v15, v0

    move/from16 v0, p3

    int-to-float v0, v0

    move/from16 v16, v0

    move/from16 v0, p2

    int-to-float v0, v0

    move/from16 v17, v0

    add-int v4, p3, p5

    int-to-float v0, v4

    move/from16 v18, v0

    move-object/from16 v14, p1

    move-object/from16 v19, p6

    invoke-virtual/range {v14 .. v19}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 134
    move/from16 v0, p2

    int-to-float v15, v0

    add-int v4, p3, p5

    int-to-float v0, v4

    move/from16 v16, v0

    add-int v4, p2, p4

    int-to-float v0, v4

    move/from16 v17, v0

    add-int v4, p3, p5

    int-to-float v0, v4

    move/from16 v18, v0

    move-object/from16 v14, p1

    move-object/from16 v19, p6

    invoke-virtual/range {v14 .. v19}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 135
    add-int v4, p2, p4

    int-to-float v15, v4

    move/from16 v0, p3

    int-to-float v0, v0

    move/from16 v16, v0

    add-int v4, p2, p4

    int-to-float v0, v4

    move/from16 v17, v0

    add-int v4, p3, p5

    int-to-float v0, v4

    move/from16 v18, v0

    move-object/from16 v14, p1

    move-object/from16 v19, p6

    invoke-virtual/range {v14 .. v19}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 136
    move-object/from16 p6, v98

    .line 139
    .end local v98    # "p":Landroid/graphics/Paint;
    :cond_b
    const v85, -0x7fffffff

    .line 140
    .local v85, "maxScaleNumber":I
    const/16 v21, 0x0

    .restart local v21    # "i":I
    :goto_4
    move/from16 v0, v21

    move/from16 v1, v103

    if-ge v0, v1, :cond_f

    .line 141
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/achartengine/chart/XYChart;->mDataset:Lorg/achartengine/model/XYMultipleSeriesDataset;

    move/from16 v0, v21

    invoke-virtual {v4, v0}, Lorg/achartengine/model/XYMultipleSeriesDataset;->getSeriesAt(I)Lorg/achartengine/model/XYSeries;

    move-result-object v4

    invoke-virtual {v4}, Lorg/achartengine/model/XYSeries;->getScaleNumber()I

    move-result v4

    move/from16 v0, v85

    invoke-static {v0, v4}, Ljava/lang/Math;->max(II)I

    move-result v85

    .line 140
    add-int/lit8 v21, v21, 0x1

    goto :goto_4

    .line 101
    .end local v21    # "i":I
    .end local v66    # "angle":I
    .end local v85    # "maxScaleNumber":I
    .end local v102    # "rotate":Z
    :cond_c
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/achartengine/chart/XYChart;->mRenderer:Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    invoke-virtual {v4}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->getTextTypefaceName()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/achartengine/chart/XYChart;->mRenderer:Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    invoke-virtual {v5}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->getTextTypefaceStyle()I

    move-result v5

    invoke-static {v4, v5}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v4

    move-object/from16 v0, p6

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    goto/16 :goto_1

    .line 111
    .restart local v66    # "angle":I
    :cond_d
    const/16 v102, 0x0

    goto/16 :goto_2

    .line 115
    .restart local v102    # "rotate":Z
    :cond_e
    move/from16 v0, p5

    int-to-float v4, v0

    move/from16 v0, p4

    int-to-float v5, v0

    div-float/2addr v4, v5

    move-object/from16 v0, p0

    iput v4, v0, Lorg/achartengine/chart/XYChart;->mScale:F

    goto/16 :goto_3

    .line 143
    .restart local v21    # "i":I
    .restart local v85    # "maxScaleNumber":I
    :cond_f
    add-int/lit8 v85, v85, 0x1

    .line 144
    if-gez v85, :cond_11

    .line 463
    :cond_10
    :goto_5
    return-void

    .line 147
    :cond_11
    move/from16 v0, v85

    new-array v0, v0, [D

    move-object/from16 v92, v0

    .line 148
    .local v92, "minX":[D
    move/from16 v0, v85

    new-array v0, v0, [D

    move-object/from16 v86, v0

    .line 149
    .local v86, "maxX":[D
    move/from16 v0, v85

    new-array v0, v0, [D

    move-object/from16 v49, v0

    .line 150
    .local v49, "minY":[D
    move/from16 v0, v85

    new-array v0, v0, [D

    move-object/from16 v87, v0

    .line 151
    .local v87, "maxY":[D
    move/from16 v0, v85

    new-array v0, v0, [Z

    move-object/from16 v77, v0

    .line 152
    .local v77, "isMinXSet":[Z
    move/from16 v0, v85

    new-array v0, v0, [Z

    move-object/from16 v75, v0

    .line 153
    .local v75, "isMaxXSet":[Z
    move/from16 v0, v85

    new-array v0, v0, [Z

    move-object/from16 v78, v0

    .line 154
    .local v78, "isMinYSet":[Z
    move/from16 v0, v85

    new-array v0, v0, [Z

    move-object/from16 v76, v0

    .line 156
    .local v76, "isMaxYSet":[Z
    const/16 v21, 0x0

    :goto_6
    move/from16 v0, v21

    move/from16 v1, v85

    if-ge v0, v1, :cond_13

    .line 157
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/achartengine/chart/XYChart;->mRenderer:Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    move/from16 v0, v21

    invoke-virtual {v4, v0}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->getXAxisMin(I)D

    move-result-wide v4

    aput-wide v4, v92, v21

    .line 158
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/achartengine/chart/XYChart;->mRenderer:Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    move/from16 v0, v21

    invoke-virtual {v4, v0}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->getXAxisMax(I)D

    move-result-wide v4

    aput-wide v4, v86, v21

    .line 159
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/achartengine/chart/XYChart;->mRenderer:Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    move/from16 v0, v21

    invoke-virtual {v4, v0}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->getYAxisMin(I)D

    move-result-wide v4

    aput-wide v4, v49, v21

    .line 160
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/achartengine/chart/XYChart;->mRenderer:Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    move/from16 v0, v21

    invoke-virtual {v4, v0}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->getYAxisMax(I)D

    move-result-wide v4

    aput-wide v4, v87, v21

    .line 161
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/achartengine/chart/XYChart;->mRenderer:Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    move/from16 v0, v21

    invoke-virtual {v4, v0}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->isMinXSet(I)Z

    move-result v4

    aput-boolean v4, v77, v21

    .line 162
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/achartengine/chart/XYChart;->mRenderer:Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    move/from16 v0, v21

    invoke-virtual {v4, v0}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->isMaxXSet(I)Z

    move-result v4

    aput-boolean v4, v75, v21

    .line 163
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/achartengine/chart/XYChart;->mRenderer:Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    move/from16 v0, v21

    invoke-virtual {v4, v0}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->isMinYSet(I)Z

    move-result v4

    aput-boolean v4, v78, v21

    .line 164
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/achartengine/chart/XYChart;->mRenderer:Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    move/from16 v0, v21

    invoke-virtual {v4, v0}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->isMaxYSet(I)Z

    move-result v4

    aput-boolean v4, v76, v21

    .line 165
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/achartengine/chart/XYChart;->mCalcRange:Ljava/util/Map;

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    if-nez v4, :cond_12

    .line 166
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/achartengine/chart/XYChart;->mCalcRange:Ljava/util/Map;

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    const/4 v6, 0x4

    new-array v6, v6, [D

    invoke-interface {v4, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 156
    :cond_12
    add-int/lit8 v21, v21, 0x1

    goto/16 :goto_6

    .line 169
    :cond_13
    move/from16 v0, v85

    new-array v0, v0, [D

    move-object/from16 v114, v0

    .line 170
    .local v114, "xPixelsPerUnit":[D
    move/from16 v0, v85

    new-array v0, v0, [D

    move-object/from16 v48, v0

    .line 171
    .local v48, "yPixelsPerUnit":[D
    const/16 v21, 0x0

    :goto_7
    move/from16 v0, v21

    move/from16 v1, v103

    if-ge v0, v1, :cond_18

    .line 172
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/achartengine/chart/XYChart;->mDataset:Lorg/achartengine/model/XYMultipleSeriesDataset;

    move/from16 v0, v21

    invoke-virtual {v4, v0}, Lorg/achartengine/model/XYMultipleSeriesDataset;->getSeriesAt(I)Lorg/achartengine/model/XYSeries;

    move-result-object v15

    .line 173
    .local v15, "series":Lorg/achartengine/model/XYSeries;
    invoke-virtual {v15}, Lorg/achartengine/model/XYSeries;->getScaleNumber()I

    move-result v104

    .line 174
    .local v104, "scale":I
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/achartengine/chart/XYChart;->mCalcRange:Ljava/util/Map;

    invoke-static/range {v104 .. v104}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v112

    check-cast v112, [D

    .line 175
    .local v112, "value":[D
    invoke-virtual {v15}, Lorg/achartengine/model/XYSeries;->getItemCount()I

    move-result v4

    if-eqz v4, :cond_17

    .line 176
    if-eqz v112, :cond_17

    .line 177
    aget-boolean v4, v77, v104

    if-nez v4, :cond_14

    .line 178
    invoke-virtual {v15}, Lorg/achartengine/model/XYSeries;->getMinX()D

    move-result-wide v94

    .line 179
    .local v94, "minimumX":D
    aget-wide v4, v92, v104

    move-wide/from16 v0, v94

    invoke-static {v4, v5, v0, v1}, Ljava/lang/Math;->min(DD)D

    move-result-wide v4

    aput-wide v4, v92, v104

    move-object/from16 v4, v112

    .line 180
    check-cast v4, [D

    const/4 v5, 0x0

    aget-wide v6, v92, v104

    aput-wide v6, v4, v5

    .line 182
    .end local v94    # "minimumX":D
    :cond_14
    aget-boolean v4, v75, v104

    if-nez v4, :cond_15

    .line 183
    invoke-virtual {v15}, Lorg/achartengine/model/XYSeries;->getMaxX()D

    move-result-wide v88

    .line 184
    .local v88, "maximumX":D
    aget-wide v4, v86, v104

    move-wide/from16 v0, v88

    invoke-static {v4, v5, v0, v1}, Ljava/lang/Math;->max(DD)D

    move-result-wide v4

    aput-wide v4, v86, v104

    move-object/from16 v4, v112

    .line 185
    check-cast v4, [D

    const/4 v5, 0x1

    aget-wide v6, v86, v104

    aput-wide v6, v4, v5

    .line 187
    .end local v88    # "maximumX":D
    :cond_15
    aget-boolean v4, v78, v104

    if-nez v4, :cond_16

    .line 188
    invoke-virtual {v15}, Lorg/achartengine/model/XYSeries;->getMinY()D

    move-result-wide v96

    .line 189
    .local v96, "minimumY":D
    aget-wide v4, v49, v104

    move-wide/from16 v0, v96

    double-to-float v6, v0

    float-to-double v6, v6

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->min(DD)D

    move-result-wide v4

    aput-wide v4, v49, v104

    move-object/from16 v4, v112

    .line 190
    check-cast v4, [D

    const/4 v5, 0x2

    aget-wide v6, v49, v104

    aput-wide v6, v4, v5

    .line 192
    .end local v96    # "minimumY":D
    :cond_16
    aget-boolean v4, v76, v104

    if-nez v4, :cond_17

    .line 193
    invoke-virtual {v15}, Lorg/achartengine/model/XYSeries;->getMaxY()D

    move-result-wide v90

    .line 194
    .local v90, "maximumY":D
    aget-wide v4, v87, v104

    move-wide/from16 v0, v90

    double-to-float v6, v0

    float-to-double v6, v6

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->max(DD)D

    move-result-wide v4

    aput-wide v4, v87, v104

    .line 195
    check-cast v112, [D

    .end local v112    # "value":[D
    const/4 v4, 0x3

    aget-wide v6, v87, v104

    aput-wide v6, v112, v4

    .line 171
    .end local v90    # "maximumY":D
    :cond_17
    add-int/lit8 v21, v21, 0x1

    goto/16 :goto_7

    .line 200
    .end local v15    # "series":Lorg/achartengine/model/XYSeries;
    .end local v104    # "scale":I
    :cond_18
    const/16 v21, 0x0

    :goto_8
    move/from16 v0, v21

    move/from16 v1, v85

    if-ge v0, v1, :cond_1b

    .line 201
    aget-wide v4, v86, v21

    aget-wide v6, v92, v21

    sub-double/2addr v4, v6

    const-wide/16 v6, 0x0

    cmpl-double v4, v4, v6

    if-eqz v4, :cond_19

    .line 202
    sub-int v4, v9, v81

    int-to-double v4, v4

    aget-wide v6, v86, v21

    aget-wide v10, v92, v21

    sub-double/2addr v6, v10

    div-double/2addr v4, v6

    aput-wide v4, v114, v21

    .line 204
    :cond_19
    aget-wide v4, v87, v21

    aget-wide v6, v49, v21

    sub-double/2addr v4, v6

    const-wide/16 v6, 0x0

    cmpl-double v4, v4, v6

    if-eqz v4, :cond_1a

    .line 205
    sub-int v4, v33, v111

    int-to-double v4, v4

    aget-wide v6, v87, v21

    aget-wide v10, v49, v21

    sub-double/2addr v6, v10

    div-double/2addr v4, v6

    double-to-float v4, v4

    float-to-double v4, v4

    aput-wide v4, v48, v21

    .line 200
    :cond_1a
    add-int/lit8 v21, v21, 0x1

    goto :goto_8

    .line 209
    :cond_1b
    const/16 v73, 0x0

    .line 211
    .local v73, "hasValues":Z
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    move-object/from16 v0, p0

    iput-object v4, v0, Lorg/achartengine/chart/XYChart;->clickableAreas:Ljava/util/Map;

    .line 212
    const/16 v21, 0x0

    :goto_9
    move/from16 v0, v21

    move/from16 v1, v103

    if-ge v0, v1, :cond_26

    .line 213
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/achartengine/chart/XYChart;->mDataset:Lorg/achartengine/model/XYMultipleSeriesDataset;

    move/from16 v0, v21

    invoke-virtual {v4, v0}, Lorg/achartengine/model/XYMultipleSeriesDataset;->getSeriesAt(I)Lorg/achartengine/model/XYSeries;

    move-result-object v15

    .line 214
    .restart local v15    # "series":Lorg/achartengine/model/XYSeries;
    invoke-virtual {v15}, Lorg/achartengine/model/XYSeries;->getScaleNumber()I

    move-result v104

    .line 215
    .restart local v104    # "scale":I
    invoke-virtual {v15}, Lorg/achartengine/model/XYSeries;->getItemCount()I

    move-result v4

    if-eqz v4, :cond_25

    .line 217
    const/16 v73, 0x1

    .line 218
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/achartengine/chart/XYChart;->mRenderer:Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    move/from16 v0, v21

    invoke-virtual {v4, v0}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->getSeriesRendererAt(I)Lorg/achartengine/renderer/SimpleSeriesRenderer;

    move-result-object v105

    .line 220
    .local v105, "seriesRenderer":Lorg/achartengine/renderer/SimpleSeriesRenderer;
    new-instance v99, Ljava/util/ArrayList;

    invoke-direct/range {v99 .. v99}, Ljava/util/ArrayList;-><init>()V

    .line 221
    .local v99, "points":Ljava/util/List;
    new-instance v113, Ljava/util/ArrayList;

    invoke-direct/range {v113 .. v113}, Ljava/util/ArrayList;-><init>()V

    .line 222
    .local v113, "values":Ljava/util/List;
    move/from16 v0, v33

    int-to-float v4, v0

    move/from16 v0, v33

    int-to-double v6, v0

    aget-wide v10, v48, v104

    aget-wide v16, v49, v104

    mul-double v10, v10, v16

    add-double/2addr v6, v10

    double-to-float v5, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->min(FF)F

    move-result v115

    .line 223
    .local v115, "yAxisValue":F
    new-instance v70, Ljava/util/LinkedList;

    invoke-direct/range {v70 .. v70}, Ljava/util/LinkedList;-><init>()V

    .line 225
    .local v70, "clickableArea":Ljava/util/LinkedList;
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/achartengine/chart/XYChart;->clickableAreas:Ljava/util/Map;

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, v70

    invoke-interface {v4, v5, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 227
    monitor-enter v15

    .line 228
    :try_start_0
    aget-wide v16, v92, v104

    aget-wide v18, v86, v104

    invoke-virtual/range {v105 .. v105}, Lorg/achartengine/renderer/SimpleSeriesRenderer;->isDisplayBoundingPoints()Z

    move-result v20

    invoke-virtual/range {v15 .. v20}, Lorg/achartengine/model/XYSeries;->getRange(DDZ)Ljava/util/SortedMap;

    move-result-object v100

    .line 230
    .local v100, "range":Ljava/util/SortedMap;
    const/16 v23, -0x1

    .line 233
    .local v23, "startIndex":I
    invoke-interface/range {v100 .. v100}, Ljava/util/SortedMap;->entrySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v79

    .line 234
    .local v79, "it":Ljava/util/Iterator;
    :goto_a
    invoke-interface/range {v79 .. v79}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_21

    .line 235
    invoke-interface/range {v79 .. v79}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v112

    check-cast v112, Ljava/util/Map$Entry;

    .line 236
    .local v112, "value":Ljava/util/Map$Entry;
    invoke-interface/range {v112 .. v112}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Double;

    invoke-virtual {v4}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v116

    .line 237
    .local v116, "xValue":D
    invoke-interface/range {v112 .. v112}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Double;

    invoke-virtual {v4}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v120

    .line 238
    .local v120, "yValue":D
    if-gez v23, :cond_1d

    move-object/from16 v0, p0

    move-wide/from16 v1, v120

    invoke-virtual {v0, v1, v2}, Lorg/achartengine/chart/XYChart;->isNullValue(D)Z

    move-result v4

    if-eqz v4, :cond_1c

    invoke-virtual/range {p0 .. p0}, Lorg/achartengine/chart/XYChart;->isRenderNullValues()Z

    move-result v4

    if-eqz v4, :cond_1d

    .line 239
    :cond_1c
    move-wide/from16 v0, v116

    invoke-virtual {v15, v0, v1}, Lorg/achartengine/model/XYSeries;->getIndexForKey(D)I

    move-result v23

    .line 242
    :cond_1d
    invoke-interface/range {v112 .. v112}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    move-object/from16 v0, v113

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 243
    invoke-interface/range {v112 .. v112}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    move-object/from16 v0, v113

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 245
    move-object/from16 v0, p0

    move-wide/from16 v1, v120

    invoke-virtual {v0, v1, v2}, Lorg/achartengine/chart/XYChart;->isNullValue(D)Z

    move-result v4

    if-nez v4, :cond_1e

    .line 246
    move/from16 v0, v81

    int-to-double v4, v0

    aget-wide v6, v114, v104

    aget-wide v10, v92, v104

    sub-double v10, v116, v10

    mul-double/2addr v6, v10

    add-double/2addr v4, v6

    double-to-float v4, v4

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    move-object/from16 v0, v99

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 247
    move/from16 v0, v33

    int-to-double v4, v0

    aget-wide v6, v48, v104

    aget-wide v10, v49, v104

    sub-double v10, v120, v10

    mul-double/2addr v6, v10

    sub-double/2addr v4, v6

    double-to-float v4, v4

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    move-object/from16 v0, v99

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_a

    .line 288
    .end local v23    # "startIndex":I
    .end local v79    # "it":Ljava/util/Iterator;
    .end local v100    # "range":Ljava/util/SortedMap;
    .end local v112    # "value":Ljava/util/Map$Entry;
    .end local v116    # "xValue":D
    .end local v120    # "yValue":D
    :catchall_0
    move-exception v4

    monitor-exit v15
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4

    .line 248
    .restart local v23    # "startIndex":I
    .restart local v79    # "it":Ljava/util/Iterator;
    .restart local v100    # "range":Ljava/util/SortedMap;
    .restart local v112    # "value":Ljava/util/Map$Entry;
    .restart local v116    # "xValue":D
    .restart local v120    # "yValue":D
    :cond_1e
    :try_start_1
    invoke-virtual/range {p0 .. p0}, Lorg/achartengine/chart/XYChart;->isRenderNullValues()Z

    move-result v4

    if-eqz v4, :cond_1f

    .line 249
    move/from16 v0, v81

    int-to-double v4, v0

    aget-wide v6, v114, v104

    aget-wide v10, v92, v104

    sub-double v10, v116, v10

    mul-double/2addr v6, v10

    add-double/2addr v4, v6

    double-to-float v4, v4

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    move-object/from16 v0, v99

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 250
    move/from16 v0, v33

    int-to-double v4, v0

    aget-wide v6, v48, v104

    aget-wide v10, v49, v104

    neg-double v10, v10

    mul-double/2addr v6, v10

    sub-double/2addr v4, v6

    double-to-float v4, v4

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    move-object/from16 v0, v99

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_a

    .line 252
    :cond_1f
    invoke-interface/range {v99 .. v99}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_20

    move-object/from16 v14, p0

    move-object/from16 v16, p1

    move-object/from16 v17, p6

    move-object/from16 v18, v99

    move-object/from16 v19, v105

    move/from16 v20, v115

    move-object/from16 v22, v93

    .line 253
    invoke-virtual/range {v14 .. v23}, Lorg/achartengine/chart/XYChart;->drawSeries(Lorg/achartengine/model/XYSeries;Landroid/graphics/Canvas;Landroid/graphics/Paint;Ljava/util/List;Lorg/achartengine/renderer/SimpleSeriesRenderer;FILorg/achartengine/renderer/XYMultipleSeriesRenderer$Orientation;I)V

    move-object/from16 v17, p0

    move-object/from16 v18, v99

    move-object/from16 v19, v113

    move/from16 v20, v115

    move/from16 v22, v23

    .line 255
    invoke-virtual/range {v17 .. v22}, Lorg/achartengine/chart/XYChart;->clickableAreasForPoints(Ljava/util/List;Ljava/util/List;FII)[Lorg/achartengine/chart/ClickableArea;

    move-result-object v71

    .line 257
    .local v71, "clickableAreasForSubSeries":[Lorg/achartengine/chart/ClickableArea;
    invoke-static/range {v71 .. v71}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v4

    move-object/from16 v0, v70

    invoke-virtual {v0, v4}, Ljava/util/LinkedList;->addAll(Ljava/util/Collection;)Z

    .line 258
    invoke-interface/range {v99 .. v99}, Ljava/util/List;->clear()V

    .line 259
    invoke-interface/range {v113 .. v113}, Ljava/util/List;->clear()V

    .line 260
    const/16 v23, -0x1

    .line 262
    .end local v71    # "clickableAreasForSubSeries":[Lorg/achartengine/chart/ClickableArea;
    :cond_20
    const/4 v4, 0x0

    move-object/from16 v0, v70

    invoke-virtual {v0, v4}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_a

    .line 266
    .end local v112    # "value":Ljava/util/Map$Entry;
    .end local v116    # "xValue":D
    .end local v120    # "yValue":D
    :cond_21
    invoke-virtual {v15}, Lorg/achartengine/model/XYSeries;->getAnnotationCount()I

    move-result v72

    .line 267
    .local v72, "count":I
    if-lez v72, :cond_23

    .line 268
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/achartengine/chart/XYChart;->mRenderer:Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    invoke-virtual {v4}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->getLabelsColor()I

    move-result v4

    move-object/from16 v0, p6

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 269
    new-instance v69, Landroid/graphics/Rect;

    invoke-direct/range {v69 .. v69}, Landroid/graphics/Rect;-><init>()V

    .line 270
    .local v69, "bound":Landroid/graphics/Rect;
    const/16 v80, 0x0

    .local v80, "j":I
    :goto_b
    move/from16 v0, v80

    move/from16 v1, v72

    if-ge v0, v1, :cond_23

    .line 271
    move/from16 v0, v81

    int-to-double v4, v0

    aget-wide v6, v114, v104

    move/from16 v0, v80

    invoke-virtual {v15, v0}, Lorg/achartengine/model/XYSeries;->getAnnotationX(I)D

    move-result-wide v10

    aget-wide v16, v92, v104

    sub-double v10, v10, v16

    mul-double/2addr v6, v10

    add-double/2addr v4, v6

    double-to-float v0, v4

    move/from16 v27, v0

    .line 273
    .local v27, "xS":F
    move/from16 v0, v33

    int-to-double v4, v0

    aget-wide v6, v48, v104

    move/from16 v0, v80

    invoke-virtual {v15, v0}, Lorg/achartengine/model/XYSeries;->getAnnotationY(I)D

    move-result-wide v10

    aget-wide v16, v49, v104

    sub-double v10, v10, v16

    mul-double/2addr v6, v10

    sub-double/2addr v4, v6

    double-to-float v0, v4

    move/from16 v28, v0

    .line 275
    .local v28, "yS":F
    move/from16 v0, v80

    invoke-virtual {v15, v0}, Lorg/achartengine/model/XYSeries;->getAnnotationAt(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    move/from16 v0, v80

    invoke-virtual {v15, v0}, Lorg/achartengine/model/XYSeries;->getAnnotationAt(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    move-object/from16 v0, p6

    move-object/from16 v1, v69

    invoke-virtual {v0, v4, v5, v6, v1}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 277
    invoke-virtual/range {v69 .. v69}, Landroid/graphics/Rect;->width()I

    move-result v4

    int-to-float v4, v4

    add-float v4, v4, v27

    cmpg-float v4, v27, v4

    if-gez v4, :cond_22

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v4

    int-to-float v4, v4

    cmpg-float v4, v28, v4

    if-gez v4, :cond_22

    .line 278
    move/from16 v0, v80

    invoke-virtual {v15, v0}, Lorg/achartengine/model/XYSeries;->getAnnotationAt(I)Ljava/lang/String;

    move-result-object v26

    move-object/from16 v24, p0

    move-object/from16 v25, p1

    move-object/from16 v29, p6

    invoke-virtual/range {v24 .. v29}, Lorg/achartengine/chart/XYChart;->drawString(Landroid/graphics/Canvas;Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 270
    :cond_22
    add-int/lit8 v80, v80, 0x1

    goto :goto_b

    .line 283
    .end local v27    # "xS":F
    .end local v28    # "yS":F
    .end local v69    # "bound":Landroid/graphics/Rect;
    .end local v80    # "j":I
    :cond_23
    invoke-interface/range {v99 .. v99}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_24

    move-object/from16 v14, p0

    move-object/from16 v16, p1

    move-object/from16 v17, p6

    move-object/from16 v18, v99

    move-object/from16 v19, v105

    move/from16 v20, v115

    move-object/from16 v22, v93

    .line 284
    invoke-virtual/range {v14 .. v23}, Lorg/achartengine/chart/XYChart;->drawSeries(Lorg/achartengine/model/XYSeries;Landroid/graphics/Canvas;Landroid/graphics/Paint;Ljava/util/List;Lorg/achartengine/renderer/SimpleSeriesRenderer;FILorg/achartengine/renderer/XYMultipleSeriesRenderer$Orientation;I)V

    move-object/from16 v17, p0

    move-object/from16 v18, v99

    move-object/from16 v19, v113

    move/from16 v20, v115

    move/from16 v22, v23

    .line 285
    invoke-virtual/range {v17 .. v22}, Lorg/achartengine/chart/XYChart;->clickableAreasForPoints(Ljava/util/List;Ljava/util/List;FII)[Lorg/achartengine/chart/ClickableArea;

    move-result-object v71

    .line 286
    .restart local v71    # "clickableAreasForSubSeries":[Lorg/achartengine/chart/ClickableArea;
    invoke-static/range {v71 .. v71}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v4

    move-object/from16 v0, v70

    invoke-virtual {v0, v4}, Ljava/util/LinkedList;->addAll(Ljava/util/Collection;)Z

    .line 288
    .end local v71    # "clickableAreasForSubSeries":[Lorg/achartengine/chart/ClickableArea;
    :cond_24
    monitor-exit v15
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 212
    .end local v23    # "startIndex":I
    .end local v70    # "clickableArea":Ljava/util/LinkedList;
    .end local v72    # "count":I
    .end local v79    # "it":Ljava/util/Iterator;
    .end local v99    # "points":Ljava/util/List;
    .end local v100    # "range":Ljava/util/SortedMap;
    .end local v105    # "seriesRenderer":Lorg/achartengine/renderer/SimpleSeriesRenderer;
    .end local v113    # "values":Ljava/util/List;
    .end local v115    # "yAxisValue":F
    :cond_25
    add-int/lit8 v21, v21, 0x1

    goto/16 :goto_9

    .line 291
    .end local v15    # "series":Lorg/achartengine/model/XYSeries;
    .end local v104    # "scale":I
    :cond_26
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/achartengine/chart/XYChart;->mRenderer:Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    move-object/from16 v30, v0

    sub-int v35, p5, v33

    const/16 v37, 0x1

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/achartengine/chart/XYChart;->mRenderer:Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    invoke-virtual {v4}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->getMarginsColor()I

    move-result v38

    move-object/from16 v29, p0

    move-object/from16 v31, p1

    move/from16 v32, p2

    move/from16 v34, p4

    move-object/from16 v36, p6

    invoke-virtual/range {v29 .. v38}, Lorg/achartengine/chart/XYChart;->drawBackground(Lorg/achartengine/renderer/DefaultRenderer;Landroid/graphics/Canvas;IIIILandroid/graphics/Paint;ZI)V

    .line 293
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/achartengine/chart/XYChart;->mRenderer:Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    move-object/from16 v35, v0

    const/4 v4, 0x0

    aget v40, v84, v4

    const/16 v42, 0x1

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/achartengine/chart/XYChart;->mRenderer:Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    invoke-virtual {v4}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->getMarginsColor()I

    move-result v43

    move-object/from16 v34, p0

    move-object/from16 v36, p1

    move/from16 v37, p2

    move/from16 v38, p3

    move/from16 v39, p4

    move-object/from16 v41, p6

    invoke-virtual/range {v34 .. v43}, Lorg/achartengine/chart/XYChart;->drawBackground(Lorg/achartengine/renderer/DefaultRenderer;Landroid/graphics/Canvas;IIIILandroid/graphics/Paint;ZI)V

    .line 295
    sget-object v4, Lorg/achartengine/renderer/XYMultipleSeriesRenderer$Orientation;->HORIZONTAL:Lorg/achartengine/renderer/XYMultipleSeriesRenderer$Orientation;

    move-object/from16 v0, v93

    if-ne v0, v4, :cond_2b

    .line 296
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/achartengine/chart/XYChart;->mRenderer:Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    move-object/from16 v35, v0

    sub-int v39, v81, p2

    sub-int v40, p5, p3

    const/16 v42, 0x1

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/achartengine/chart/XYChart;->mRenderer:Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    invoke-virtual {v4}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->getMarginsColor()I

    move-result v43

    move-object/from16 v34, p0

    move-object/from16 v36, p1

    move/from16 v37, p2

    move/from16 v38, p3

    move-object/from16 v41, p6

    invoke-virtual/range {v34 .. v43}, Lorg/achartengine/chart/XYChart;->drawBackground(Lorg/achartengine/renderer/DefaultRenderer;Landroid/graphics/Canvas;IIIILandroid/graphics/Paint;ZI)V

    .line 298
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/achartengine/chart/XYChart;->mRenderer:Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    move-object/from16 v35, v0

    const/4 v4, 0x3

    aget v39, v84, v4

    sub-int v40, p5, p3

    const/16 v42, 0x1

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/achartengine/chart/XYChart;->mRenderer:Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    invoke-virtual {v4}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->getMarginsColor()I

    move-result v43

    move-object/from16 v34, p0

    move-object/from16 v36, p1

    move/from16 v37, v9

    move/from16 v38, p3

    move-object/from16 v41, p6

    invoke-virtual/range {v34 .. v43}, Lorg/achartengine/chart/XYChart;->drawBackground(Lorg/achartengine/renderer/DefaultRenderer;Landroid/graphics/Canvas;IIIILandroid/graphics/Paint;ZI)V

    .line 306
    :cond_27
    :goto_c
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/achartengine/chart/XYChart;->mRenderer:Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    invoke-virtual {v4}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->isShowLabels()Z

    move-result v4

    if-eqz v4, :cond_2c

    if-eqz v73, :cond_2c

    const/16 v108, 0x1

    .line 307
    .local v108, "showLabels":Z
    :goto_d
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/achartengine/chart/XYChart;->mRenderer:Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    invoke-virtual {v4}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->isShowGridX()Z

    move-result v107

    .line 308
    .local v107, "showGridX":Z
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/achartengine/chart/XYChart;->mRenderer:Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    invoke-virtual {v4}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->isShowCustomTextGrid()Z

    move-result v106

    .line 309
    .local v106, "showCustomTextGrid":Z
    if-nez v108, :cond_28

    if-eqz v107, :cond_33

    .line 310
    :cond_28
    const/4 v4, 0x0

    aget-wide v36, v92, v4

    const/4 v4, 0x0

    aget-wide v38, v86, v4

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/achartengine/chart/XYChart;->mRenderer:Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    invoke-virtual {v4}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->getXLabels()I

    move-result v40

    move-object/from16 v35, p0

    invoke-virtual/range {v35 .. v40}, Lorg/achartengine/chart/XYChart;->getXLabels(DDI)Ljava/util/List;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lorg/achartengine/chart/XYChart;->getValidLabels(Ljava/util/List;)Ljava/util/List;

    move-result-object v35

    .line 311
    .local v35, "xLabels":Ljava/util/List;
    move-object/from16 v0, p0

    move-object/from16 v1, v49

    move-object/from16 v2, v87

    move/from16 v3, v85

    invoke-virtual {v0, v1, v2, v3}, Lorg/achartengine/chart/XYChart;->getYLabels([D[DI)Ljava/util/Map;

    move-result-object v65

    .line 313
    .local v65, "allYLabels":Ljava/util/Map;
    move/from16 v39, v81

    .line 314
    .local v39, "xLabelsLeft":I
    if-eqz v108, :cond_29

    .line 315
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/achartengine/chart/XYChart;->mRenderer:Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    invoke-virtual {v4}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->getXLabelsColor()I

    move-result v4

    move-object/from16 v0, p6

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 316
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/achartengine/chart/XYChart;->mRenderer:Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    invoke-virtual {v4}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->getLabelsTextSize()F

    move-result v4

    move-object/from16 v0, p6

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 317
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/achartengine/chart/XYChart;->mRenderer:Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    invoke-virtual {v4}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->getXLabelsAlign()Landroid/graphics/Paint$Align;

    move-result-object v4

    move-object/from16 v0, p6

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 320
    :cond_29
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/achartengine/chart/XYChart;->mRenderer:Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    invoke-virtual {v4}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->getXTextLabelLocations()[Ljava/lang/Double;

    move-result-object v36

    const/4 v4, 0x0

    aget-wide v42, v114, v4

    const/4 v4, 0x0

    aget-wide v44, v92, v4

    const/4 v4, 0x0

    aget-wide v46, v86, v4

    move-object/from16 v34, p0

    move-object/from16 v37, p1

    move-object/from16 v38, p6

    move/from16 v40, v111

    move/from16 v41, v33

    invoke-virtual/range {v34 .. v47}, Lorg/achartengine/chart/XYChart;->drawXLabels(Ljava/util/List;[Ljava/lang/Double;Landroid/graphics/Canvas;Landroid/graphics/Paint;IIIDDD)V

    move-object/from16 v40, p0

    move-object/from16 v41, v65

    move-object/from16 v42, p1

    move-object/from16 v43, p6

    move/from16 v44, v85

    move/from16 v45, v81

    move/from16 v46, v9

    move/from16 v47, v33

    .line 322
    invoke-virtual/range {v40 .. v49}, Lorg/achartengine/chart/XYChart;->drawYLabels(Ljava/util/Map;Landroid/graphics/Canvas;Landroid/graphics/Paint;IIII[D[D)V

    .line 324
    if-eqz v108, :cond_30

    .line 325
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/achartengine/chart/XYChart;->mRenderer:Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    invoke-virtual {v4}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->getLabelsColor()I

    move-result v4

    move-object/from16 v0, p6

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 326
    const/16 v21, 0x0

    :goto_e
    move/from16 v0, v21

    move/from16 v1, v85

    if-ge v0, v1, :cond_30

    .line 327
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/achartengine/chart/XYChart;->mRenderer:Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    move/from16 v0, v21

    invoke-virtual {v4, v0}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->getYAxisAlign(I)Landroid/graphics/Paint$Align;

    move-result-object v68

    .line 328
    .local v68, "axisAlign":Landroid/graphics/Paint$Align;
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/achartengine/chart/XYChart;->mRenderer:Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    move/from16 v0, v21

    invoke-virtual {v4, v0}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->getYTextLabelLocations(I)[Ljava/lang/Double;

    move-result-object v118

    .line 329
    .local v118, "yTextLabelLocations":[Ljava/lang/Double;
    if-eqz v118, :cond_2f

    .line 330
    move-object/from16 v67, v118

    .local v67, "arr$":[Ljava/lang/Double;
    move-object/from16 v0, v67

    array-length v0, v0

    move/from16 v82, v0

    .local v82, "len$":I
    const/16 v74, 0x0

    .local v74, "i$":I
    :goto_f
    move/from16 v0, v74

    move/from16 v1, v82

    if-ge v0, v1, :cond_2f

    aget-object v83, v67, v74

    .line 331
    .local v83, "location":Ljava/lang/Double;
    aget-wide v4, v49, v21

    invoke-virtual/range {v83 .. v83}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v6

    cmpg-double v4, v4, v6

    if-gtz v4, :cond_2a

    invoke-virtual/range {v83 .. v83}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    aget-wide v6, v87, v21

    cmpg-double v4, v4, v6

    if-gtz v4, :cond_2a

    .line 333
    move/from16 v0, v33

    int-to-double v4, v0

    aget-wide v6, v48, v21

    invoke-virtual/range {v83 .. v83}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v10

    aget-wide v16, v49, v21

    sub-double v10, v10, v16

    mul-double/2addr v6, v10

    sub-double/2addr v4, v6

    double-to-float v0, v4

    move/from16 v42, v0

    .line 336
    .local v42, "yLabel":F
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/achartengine/chart/XYChart;->mRenderer:Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    move-object/from16 v0, v83

    move/from16 v1, v21

    invoke-virtual {v4, v0, v1}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->getYTextLabel(Ljava/lang/Double;I)Ljava/lang/String;

    move-result-object v52

    .line 338
    .local v52, "label":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/achartengine/chart/XYChart;->mRenderer:Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    move/from16 v0, v21

    invoke-virtual {v4, v0}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->getYLabelsColor(I)I

    move-result v4

    move-object/from16 v0, p6

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 340
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/achartengine/chart/XYChart;->mRenderer:Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    move/from16 v0, v21

    invoke-virtual {v4, v0}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->getYLabelsAlign(I)Landroid/graphics/Paint$Align;

    move-result-object v4

    move-object/from16 v0, p6

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 342
    sget-object v4, Lorg/achartengine/renderer/XYMultipleSeriesRenderer$Orientation;->HORIZONTAL:Lorg/achartengine/renderer/XYMultipleSeriesRenderer$Orientation;

    move-object/from16 v0, v93

    if-ne v0, v4, :cond_2e

    .line 343
    sget-object v4, Landroid/graphics/Paint$Align;->LEFT:Landroid/graphics/Paint$Align;

    move-object/from16 v0, v68

    if-ne v0, v4, :cond_2d

    .line 344
    move-object/from16 v0, p0

    move-object/from16 v1, v68

    invoke-direct {v0, v1}, Lorg/achartengine/chart/XYChart;->getLabelLinePos(Landroid/graphics/Paint$Align;)I

    move-result v4

    add-int v4, v4, v81

    int-to-float v0, v4

    move/from16 v41, v0

    move/from16 v0, v81

    int-to-float v0, v0

    move/from16 v43, v0

    move-object/from16 v40, p1

    move/from16 v44, v42

    move-object/from16 v45, p6

    invoke-virtual/range {v40 .. v45}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 347
    move/from16 v0, v81

    int-to-float v0, v0

    move/from16 v53, v0

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/achartengine/chart/XYChart;->mRenderer:Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    invoke-virtual {v4}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->getYLabelsVerticalPadding()F

    move-result v4

    sub-float v54, v42, v4

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/achartengine/chart/XYChart;->mRenderer:Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    invoke-virtual {v4}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->getYLabelsAngle()F

    move-result v56

    move-object/from16 v50, p0

    move-object/from16 v51, p1

    move-object/from16 v55, p6

    invoke-virtual/range {v50 .. v56}, Lorg/achartengine/chart/XYChart;->drawText(Landroid/graphics/Canvas;Ljava/lang/String;FFLandroid/graphics/Paint;F)V

    .line 371
    :goto_10
    if-eqz v106, :cond_2a

    .line 372
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/achartengine/chart/XYChart;->mRenderer:Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    invoke-virtual {v4}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->getGridColor()I

    move-result v4

    move-object/from16 v0, p6

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 374
    move/from16 v0, v81

    int-to-float v0, v0

    move/from16 v41, v0

    int-to-float v0, v9

    move/from16 v43, v0

    move-object/from16 v40, p1

    move/from16 v44, v42

    move-object/from16 v45, p6

    invoke-virtual/range {v40 .. v45}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 330
    .end local v42    # "yLabel":F
    .end local v52    # "label":Ljava/lang/String;
    :cond_2a
    :goto_11
    add-int/lit8 v74, v74, 0x1

    goto/16 :goto_f

    .line 300
    .end local v35    # "xLabels":Ljava/util/List;
    .end local v39    # "xLabelsLeft":I
    .end local v65    # "allYLabels":Ljava/util/Map;
    .end local v67    # "arr$":[Ljava/lang/Double;
    .end local v68    # "axisAlign":Landroid/graphics/Paint$Align;
    .end local v74    # "i$":I
    .end local v82    # "len$":I
    .end local v83    # "location":Ljava/lang/Double;
    .end local v106    # "showCustomTextGrid":Z
    .end local v107    # "showGridX":Z
    .end local v108    # "showLabels":Z
    .end local v118    # "yTextLabelLocations":[Ljava/lang/Double;
    :cond_2b
    sget-object v4, Lorg/achartengine/renderer/XYMultipleSeriesRenderer$Orientation;->VERTICAL:Lorg/achartengine/renderer/XYMultipleSeriesRenderer$Orientation;

    move-object/from16 v0, v93

    if-ne v0, v4, :cond_27

    .line 301
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/achartengine/chart/XYChart;->mRenderer:Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    move-object/from16 v35, v0

    sub-int v39, p4, v9

    sub-int v40, p5, p3

    const/16 v42, 0x1

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/achartengine/chart/XYChart;->mRenderer:Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    invoke-virtual {v4}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->getMarginsColor()I

    move-result v43

    move-object/from16 v34, p0

    move-object/from16 v36, p1

    move/from16 v37, v9

    move/from16 v38, p3

    move-object/from16 v41, p6

    invoke-virtual/range {v34 .. v43}, Lorg/achartengine/chart/XYChart;->drawBackground(Lorg/achartengine/renderer/DefaultRenderer;Landroid/graphics/Canvas;IIIILandroid/graphics/Paint;ZI)V

    .line 303
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/achartengine/chart/XYChart;->mRenderer:Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    move-object/from16 v35, v0

    sub-int v39, v81, p2

    sub-int v40, p5, p3

    const/16 v42, 0x1

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/achartengine/chart/XYChart;->mRenderer:Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    invoke-virtual {v4}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->getMarginsColor()I

    move-result v43

    move-object/from16 v34, p0

    move-object/from16 v36, p1

    move/from16 v37, p2

    move/from16 v38, p3

    move-object/from16 v41, p6

    invoke-virtual/range {v34 .. v43}, Lorg/achartengine/chart/XYChart;->drawBackground(Lorg/achartengine/renderer/DefaultRenderer;Landroid/graphics/Canvas;IIIILandroid/graphics/Paint;ZI)V

    goto/16 :goto_c

    .line 306
    :cond_2c
    const/16 v108, 0x0

    goto/16 :goto_d

    .line 357
    .restart local v35    # "xLabels":Ljava/util/List;
    .restart local v39    # "xLabelsLeft":I
    .restart local v42    # "yLabel":F
    .restart local v52    # "label":Ljava/lang/String;
    .restart local v65    # "allYLabels":Ljava/util/Map;
    .restart local v67    # "arr$":[Ljava/lang/Double;
    .restart local v68    # "axisAlign":Landroid/graphics/Paint$Align;
    .restart local v74    # "i$":I
    .restart local v82    # "len$":I
    .restart local v83    # "location":Ljava/lang/Double;
    .restart local v106    # "showCustomTextGrid":Z
    .restart local v107    # "showGridX":Z
    .restart local v108    # "showLabels":Z
    .restart local v118    # "yTextLabelLocations":[Ljava/lang/Double;
    :cond_2d
    int-to-float v0, v9

    move/from16 v41, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v68

    invoke-direct {v0, v1}, Lorg/achartengine/chart/XYChart;->getLabelLinePos(Landroid/graphics/Paint$Align;)I

    move-result v4

    add-int/2addr v4, v9

    int-to-float v0, v4

    move/from16 v43, v0

    move-object/from16 v40, p1

    move/from16 v44, v42

    move-object/from16 v45, p6

    invoke-virtual/range {v40 .. v45}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 360
    int-to-float v0, v9

    move/from16 v53, v0

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/achartengine/chart/XYChart;->mRenderer:Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    invoke-virtual {v4}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->getYLabelsVerticalPadding()F

    move-result v4

    sub-float v54, v42, v4

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/achartengine/chart/XYChart;->mRenderer:Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    invoke-virtual {v4}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->getYLabelsAngle()F

    move-result v56

    move-object/from16 v50, p0

    move-object/from16 v51, p1

    move-object/from16 v55, p6

    invoke-virtual/range {v50 .. v56}, Lorg/achartengine/chart/XYChart;->drawText(Landroid/graphics/Canvas;Ljava/lang/String;FFLandroid/graphics/Paint;F)V

    goto/16 :goto_10

    .line 378
    :cond_2e
    move-object/from16 v0, p0

    move-object/from16 v1, v68

    invoke-direct {v0, v1}, Lorg/achartengine/chart/XYChart;->getLabelLinePos(Landroid/graphics/Paint$Align;)I

    move-result v4

    sub-int v4, v9, v4

    int-to-float v0, v4

    move/from16 v41, v0

    int-to-float v0, v9

    move/from16 v43, v0

    move-object/from16 v40, p1

    move/from16 v44, v42

    move-object/from16 v45, p6

    invoke-virtual/range {v40 .. v45}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 381
    add-int/lit8 v4, v9, 0xa

    int-to-float v0, v4

    move/from16 v53, v0

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/achartengine/chart/XYChart;->mRenderer:Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    invoke-virtual {v4}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->getYLabelsVerticalPadding()F

    move-result v4

    sub-float v54, v42, v4

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/achartengine/chart/XYChart;->mRenderer:Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    invoke-virtual {v4}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->getYLabelsAngle()F

    move-result v56

    move-object/from16 v50, p0

    move-object/from16 v51, p1

    move-object/from16 v55, p6

    invoke-virtual/range {v50 .. v56}, Lorg/achartengine/chart/XYChart;->drawText(Landroid/graphics/Canvas;Ljava/lang/String;FFLandroid/graphics/Paint;F)V

    .line 390
    if-eqz v106, :cond_2a

    .line 391
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/achartengine/chart/XYChart;->mRenderer:Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    invoke-virtual {v4}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->getGridColor()I

    move-result v4

    move-object/from16 v0, p6

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 393
    int-to-float v0, v9

    move/from16 v41, v0

    move/from16 v0, v81

    int-to-float v0, v0

    move/from16 v43, v0

    move-object/from16 v40, p1

    move/from16 v44, v42

    move-object/from16 v45, p6

    invoke-virtual/range {v40 .. v45}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    goto/16 :goto_11

    .line 326
    .end local v42    # "yLabel":F
    .end local v52    # "label":Ljava/lang/String;
    .end local v67    # "arr$":[Ljava/lang/Double;
    .end local v74    # "i$":I
    .end local v82    # "len$":I
    .end local v83    # "location":Ljava/lang/Double;
    :cond_2f
    add-int/lit8 v21, v21, 0x1

    goto/16 :goto_e

    .line 403
    .end local v68    # "axisAlign":Landroid/graphics/Paint$Align;
    .end local v118    # "yTextLabelLocations":[Ljava/lang/Double;
    :cond_30
    if-eqz v108, :cond_33

    .line 404
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/achartengine/chart/XYChart;->mRenderer:Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    invoke-virtual {v4}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->getLabelsColor()I

    move-result v4

    move-object/from16 v0, p6

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 405
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/achartengine/chart/XYChart;->mRenderer:Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    invoke-virtual {v4}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->getAxisTitleTextSize()F

    move-result v109

    .line 406
    .local v109, "size":F
    move-object/from16 v0, p6

    move/from16 v1, v109

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 407
    sget-object v4, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    move-object/from16 v0, p6

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 408
    sget-object v4, Lorg/achartengine/renderer/XYMultipleSeriesRenderer$Orientation;->HORIZONTAL:Lorg/achartengine/renderer/XYMultipleSeriesRenderer$Orientation;

    move-object/from16 v0, v93

    if-ne v0, v4, :cond_36

    .line 409
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/achartengine/chart/XYChart;->mRenderer:Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    invoke-virtual {v4}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->getXTitle()Ljava/lang/String;

    move-result-object v55

    div-int/lit8 v4, p4, 0x2

    add-int v4, v4, p2

    int-to-float v0, v4

    move/from16 v56, v0

    move/from16 v0, v33

    int-to-float v4, v0

    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/achartengine/chart/XYChart;->mRenderer:Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    invoke-virtual {v5}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->getLabelsTextSize()F

    move-result v5

    const/high16 v6, 0x40800000    # 4.0f

    mul-float/2addr v5, v6

    const/high16 v6, 0x40400000    # 3.0f

    div-float/2addr v5, v6

    add-float/2addr v4, v5

    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/achartengine/chart/XYChart;->mRenderer:Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    invoke-virtual {v5}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->getXLabelsPadding()F

    move-result v5

    add-float/2addr v4, v5

    add-float v57, v4, v109

    const/16 v59, 0x0

    move-object/from16 v53, p0

    move-object/from16 v54, p1

    move-object/from16 v58, p6

    invoke-virtual/range {v53 .. v59}, Lorg/achartengine/chart/XYChart;->drawText(Landroid/graphics/Canvas;Ljava/lang/String;FFLandroid/graphics/Paint;F)V

    .line 411
    const/16 v21, 0x0

    :goto_12
    move/from16 v0, v21

    move/from16 v1, v85

    if-ge v0, v1, :cond_32

    .line 412
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/achartengine/chart/XYChart;->mRenderer:Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    move/from16 v0, v21

    invoke-virtual {v4, v0}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->getYAxisAlign(I)Landroid/graphics/Paint$Align;

    move-result-object v68

    .line 413
    .restart local v68    # "axisAlign":Landroid/graphics/Paint$Align;
    sget-object v4, Landroid/graphics/Paint$Align;->LEFT:Landroid/graphics/Paint$Align;

    move-object/from16 v0, v68

    if-ne v0, v4, :cond_31

    .line 414
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/achartengine/chart/XYChart;->mRenderer:Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    move/from16 v0, v21

    invoke-virtual {v4, v0}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->getYTitle(I)Ljava/lang/String;

    move-result-object v55

    move/from16 v0, p2

    int-to-float v4, v0

    add-float v56, v4, v109

    div-int/lit8 v4, p5, 0x2

    add-int v4, v4, p3

    int-to-float v0, v4

    move/from16 v57, v0

    const/high16 v59, -0x3d4c0000    # -90.0f

    move-object/from16 v53, p0

    move-object/from16 v54, p1

    move-object/from16 v58, p6

    invoke-virtual/range {v53 .. v59}, Lorg/achartengine/chart/XYChart;->drawText(Landroid/graphics/Canvas;Ljava/lang/String;FFLandroid/graphics/Paint;F)V

    .line 411
    :goto_13
    add-int/lit8 v21, v21, 0x1

    goto :goto_12

    .line 416
    :cond_31
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/achartengine/chart/XYChart;->mRenderer:Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    move/from16 v0, v21

    invoke-virtual {v4, v0}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->getYTitle(I)Ljava/lang/String;

    move-result-object v55

    add-int v4, p2, p4

    int-to-float v0, v4

    move/from16 v56, v0

    div-int/lit8 v4, p5, 0x2

    add-int v4, v4, p3

    int-to-float v0, v4

    move/from16 v57, v0

    const/high16 v59, -0x3d4c0000    # -90.0f

    move-object/from16 v53, p0

    move-object/from16 v54, p1

    move-object/from16 v58, p6

    invoke-virtual/range {v53 .. v59}, Lorg/achartengine/chart/XYChart;->drawText(Landroid/graphics/Canvas;Ljava/lang/String;FFLandroid/graphics/Paint;F)V

    goto :goto_13

    .line 419
    .end local v68    # "axisAlign":Landroid/graphics/Paint$Align;
    :cond_32
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/achartengine/chart/XYChart;->mRenderer:Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    invoke-virtual {v4}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->getChartTitleTextSize()F

    move-result v4

    move-object/from16 v0, p6

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 420
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/achartengine/chart/XYChart;->mRenderer:Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    invoke-virtual {v4}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->getChartTitle()Ljava/lang/String;

    move-result-object v55

    div-int/lit8 v4, p4, 0x2

    add-int v4, v4, p2

    int-to-float v0, v4

    move/from16 v56, v0

    move/from16 v0, p3

    int-to-float v4, v0

    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/achartengine/chart/XYChart;->mRenderer:Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    invoke-virtual {v5}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->getChartTitleTextSize()F

    move-result v5

    add-float v57, v4, v5

    const/16 v59, 0x0

    move-object/from16 v53, p0

    move-object/from16 v54, p1

    move-object/from16 v58, p6

    invoke-virtual/range {v53 .. v59}, Lorg/achartengine/chart/XYChart;->drawText(Landroid/graphics/Canvas;Ljava/lang/String;FFLandroid/graphics/Paint;F)V

    .line 431
    .end local v35    # "xLabels":Ljava/util/List;
    .end local v39    # "xLabelsLeft":I
    .end local v65    # "allYLabels":Ljava/util/Map;
    .end local v109    # "size":F
    :cond_33
    :goto_14
    sget-object v4, Lorg/achartengine/renderer/XYMultipleSeriesRenderer$Orientation;->HORIZONTAL:Lorg/achartengine/renderer/XYMultipleSeriesRenderer$Orientation;

    move-object/from16 v0, v93

    if-ne v0, v4, :cond_37

    .line 432
    const/4 v4, 0x3

    aget v4, v84, v4

    add-int/2addr v9, v4

    .line 433
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/achartengine/chart/XYChart;->mRenderer:Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    move-object/from16 v55, v0

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/achartengine/chart/XYChart;->mRenderer:Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    invoke-virtual {v4}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->getXLabelsPadding()F

    move-result v4

    float-to-int v4, v4

    add-int v59, p3, v4

    const/16 v64, 0x0

    move-object/from16 v53, p0

    move-object/from16 v54, p1

    move-object/from16 v56, v110

    move/from16 v57, v81

    move/from16 v58, v9

    move/from16 v60, p4

    move/from16 v61, p5

    move/from16 v62, v13

    move-object/from16 v63, p6

    invoke-virtual/range {v53 .. v64}, Lorg/achartengine/chart/XYChart;->drawLegend(Landroid/graphics/Canvas;Lorg/achartengine/renderer/DefaultRenderer;[Ljava/lang/String;IIIIIILandroid/graphics/Paint;Z)I

    .line 440
    :cond_34
    :goto_15
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/achartengine/chart/XYChart;->mRenderer:Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    invoke-virtual {v4}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->isShowAxes()Z

    move-result v4

    if-eqz v4, :cond_3a

    .line 441
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/achartengine/chart/XYChart;->mRenderer:Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    invoke-virtual {v4}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->getAxesColor()I

    move-result v4

    move-object/from16 v0, p6

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 442
    move/from16 v0, v81

    int-to-float v0, v0

    move/from16 v54, v0

    move/from16 v0, v33

    int-to-float v0, v0

    move/from16 v55, v0

    int-to-float v0, v9

    move/from16 v56, v0

    move/from16 v0, v33

    int-to-float v0, v0

    move/from16 v57, v0

    move-object/from16 v53, p1

    move-object/from16 v58, p6

    invoke-virtual/range {v53 .. v58}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 444
    sget-object v4, Lorg/achartengine/renderer/XYMultipleSeriesRenderer$Orientation;->HORIZONTAL:Lorg/achartengine/renderer/XYMultipleSeriesRenderer$Orientation;

    move-object/from16 v0, v93

    if-ne v0, v4, :cond_35

    .line 445
    move/from16 v0, v81

    int-to-float v0, v0

    move/from16 v54, v0

    move/from16 v0, v33

    int-to-float v0, v0

    move/from16 v55, v0

    int-to-float v0, v9

    move/from16 v56, v0

    move/from16 v0, v33

    int-to-float v0, v0

    move/from16 v57, v0

    move-object/from16 v53, p1

    move-object/from16 v58, p6

    invoke-virtual/range {v53 .. v58}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 447
    :cond_35
    const/16 v101, 0x0

    .line 448
    .local v101, "rightAxis":Z
    const/16 v21, 0x0

    :goto_16
    move/from16 v0, v21

    move/from16 v1, v85

    if-ge v0, v1, :cond_39

    if-nez v101, :cond_39

    .line 449
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/achartengine/chart/XYChart;->mRenderer:Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    move/from16 v0, v21

    invoke-virtual {v4, v0}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->getYAxisAlign(I)Landroid/graphics/Paint$Align;

    move-result-object v4

    sget-object v5, Landroid/graphics/Paint$Align;->RIGHT:Landroid/graphics/Paint$Align;

    if-ne v4, v5, :cond_38

    const/16 v101, 0x1

    .line 448
    :goto_17
    add-int/lit8 v21, v21, 0x1

    goto :goto_16

    .line 422
    .end local v101    # "rightAxis":Z
    .restart local v35    # "xLabels":Ljava/util/List;
    .restart local v39    # "xLabelsLeft":I
    .restart local v65    # "allYLabels":Ljava/util/Map;
    .restart local v109    # "size":F
    :cond_36
    sget-object v4, Lorg/achartengine/renderer/XYMultipleSeriesRenderer$Orientation;->VERTICAL:Lorg/achartengine/renderer/XYMultipleSeriesRenderer$Orientation;

    move-object/from16 v0, v93

    if-ne v0, v4, :cond_33

    .line 423
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/achartengine/chart/XYChart;->mRenderer:Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    invoke-virtual {v4}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->getXTitle()Ljava/lang/String;

    move-result-object v55

    div-int/lit8 v4, p4, 0x2

    add-int v4, v4, p2

    int-to-float v0, v4

    move/from16 v56, v0

    add-int v4, p3, p5

    int-to-float v4, v4

    sub-float v4, v4, v109

    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/achartengine/chart/XYChart;->mRenderer:Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    invoke-virtual {v5}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->getXLabelsPadding()F

    move-result v5

    add-float v57, v4, v5

    const/high16 v59, -0x3d4c0000    # -90.0f

    move-object/from16 v53, p0

    move-object/from16 v54, p1

    move-object/from16 v58, p6

    invoke-virtual/range {v53 .. v59}, Lorg/achartengine/chart/XYChart;->drawText(Landroid/graphics/Canvas;Ljava/lang/String;FFLandroid/graphics/Paint;F)V

    .line 425
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/achartengine/chart/XYChart;->mRenderer:Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    invoke-virtual {v4}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->getYTitle()Ljava/lang/String;

    move-result-object v55

    add-int/lit8 v4, v9, 0x14

    int-to-float v0, v4

    move/from16 v56, v0

    div-int/lit8 v4, p5, 0x2

    add-int v4, v4, p3

    int-to-float v0, v4

    move/from16 v57, v0

    const/16 v59, 0x0

    move-object/from16 v53, p0

    move-object/from16 v54, p1

    move-object/from16 v58, p6

    invoke-virtual/range {v53 .. v59}, Lorg/achartengine/chart/XYChart;->drawText(Landroid/graphics/Canvas;Ljava/lang/String;FFLandroid/graphics/Paint;F)V

    .line 426
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/achartengine/chart/XYChart;->mRenderer:Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    invoke-virtual {v4}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->getChartTitleTextSize()F

    move-result v4

    move-object/from16 v0, p6

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 427
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/achartengine/chart/XYChart;->mRenderer:Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    invoke-virtual {v4}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->getChartTitle()Ljava/lang/String;

    move-result-object v55

    move/from16 v0, p2

    int-to-float v4, v0

    add-float v56, v4, v109

    div-int/lit8 v4, p5, 0x2

    add-int v4, v4, v111

    int-to-float v0, v4

    move/from16 v57, v0

    const/16 v59, 0x0

    move-object/from16 v53, p0

    move-object/from16 v54, p1

    move-object/from16 v58, p6

    invoke-virtual/range {v53 .. v59}, Lorg/achartengine/chart/XYChart;->drawText(Landroid/graphics/Canvas;Ljava/lang/String;FFLandroid/graphics/Paint;F)V

    goto/16 :goto_14

    .line 435
    .end local v35    # "xLabels":Ljava/util/List;
    .end local v39    # "xLabelsLeft":I
    .end local v65    # "allYLabels":Ljava/util/Map;
    .end local v109    # "size":F
    :cond_37
    sget-object v4, Lorg/achartengine/renderer/XYMultipleSeriesRenderer$Orientation;->VERTICAL:Lorg/achartengine/renderer/XYMultipleSeriesRenderer$Orientation;

    move-object/from16 v0, v93

    if-ne v0, v4, :cond_34

    .line 436
    move/from16 v0, v66

    int-to-float v4, v0

    const/4 v5, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v4, v5}, Lorg/achartengine/chart/XYChart;->transform(Landroid/graphics/Canvas;FZ)V

    .line 437
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/achartengine/chart/XYChart;->mRenderer:Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    move-object/from16 v55, v0

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/achartengine/chart/XYChart;->mRenderer:Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    invoke-virtual {v4}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->getXLabelsPadding()F

    move-result v4

    float-to-int v4, v4

    add-int v59, p3, v4

    const/16 v64, 0x0

    move-object/from16 v53, p0

    move-object/from16 v54, p1

    move-object/from16 v56, v110

    move/from16 v57, v81

    move/from16 v58, v9

    move/from16 v60, p4

    move/from16 v61, p5

    move/from16 v62, v13

    move-object/from16 v63, p6

    invoke-virtual/range {v53 .. v64}, Lorg/achartengine/chart/XYChart;->drawLegend(Landroid/graphics/Canvas;Lorg/achartengine/renderer/DefaultRenderer;[Ljava/lang/String;IIIIIILandroid/graphics/Paint;Z)I

    .line 438
    move/from16 v0, v66

    int-to-float v4, v0

    const/4 v5, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v4, v5}, Lorg/achartengine/chart/XYChart;->transform(Landroid/graphics/Canvas;FZ)V

    goto/16 :goto_15

    .line 449
    .restart local v101    # "rightAxis":Z
    :cond_38
    const/16 v101, 0x0

    goto/16 :goto_17

    .line 451
    :cond_39
    sget-object v4, Lorg/achartengine/renderer/XYMultipleSeriesRenderer$Orientation;->HORIZONTAL:Lorg/achartengine/renderer/XYMultipleSeriesRenderer$Orientation;

    move-object/from16 v0, v93

    if-ne v0, v4, :cond_3b

    .line 452
    move/from16 v0, v81

    int-to-float v0, v0

    move/from16 v54, v0

    move/from16 v0, v111

    int-to-float v0, v0

    move/from16 v55, v0

    move/from16 v0, v81

    int-to-float v0, v0

    move/from16 v56, v0

    move/from16 v0, v33

    int-to-float v0, v0

    move/from16 v57, v0

    move-object/from16 v53, p1

    move-object/from16 v58, p6

    invoke-virtual/range {v53 .. v58}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 453
    if-eqz v101, :cond_3a

    .line 454
    int-to-float v0, v9

    move/from16 v54, v0

    move/from16 v0, v111

    int-to-float v0, v0

    move/from16 v55, v0

    int-to-float v0, v9

    move/from16 v56, v0

    move/from16 v0, v33

    int-to-float v0, v0

    move/from16 v57, v0

    move-object/from16 v53, p1

    move-object/from16 v58, p6

    invoke-virtual/range {v53 .. v58}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 461
    .end local v101    # "rightAxis":Z
    :cond_3a
    :goto_18
    if-eqz v102, :cond_10

    .line 462
    move/from16 v0, v66

    int-to-float v4, v0

    const/4 v5, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v4, v5}, Lorg/achartengine/chart/XYChart;->transform(Landroid/graphics/Canvas;FZ)V

    goto/16 :goto_5

    .line 456
    .restart local v101    # "rightAxis":Z
    :cond_3b
    sget-object v4, Lorg/achartengine/renderer/XYMultipleSeriesRenderer$Orientation;->VERTICAL:Lorg/achartengine/renderer/XYMultipleSeriesRenderer$Orientation;

    move-object/from16 v0, v93

    if-ne v0, v4, :cond_3a

    .line 457
    int-to-float v0, v9

    move/from16 v54, v0

    move/from16 v0, v111

    int-to-float v0, v0

    move/from16 v55, v0

    int-to-float v0, v9

    move/from16 v56, v0

    move/from16 v0, v33

    int-to-float v0, v0

    move/from16 v57, v0

    move-object/from16 v53, p1

    move-object/from16 v58, p6

    invoke-virtual/range {v53 .. v58}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    goto :goto_18
.end method

.method protected drawChartValuesText(Landroid/graphics/Canvas;Lorg/achartengine/model/XYSeries;Lorg/achartengine/renderer/SimpleSeriesRenderer;Landroid/graphics/Paint;Ljava/util/List;II)V
    .locals 12
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "series"    # Lorg/achartengine/model/XYSeries;
    .param p3, "renderer"    # Lorg/achartengine/renderer/SimpleSeriesRenderer;
    .param p4, "paint"    # Landroid/graphics/Paint;
    .param p6, "seriesIndex"    # I
    .param p7, "startIndex"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/graphics/Canvas;",
            "Lorg/achartengine/model/XYSeries;",
            "Lorg/achartengine/renderer/SimpleSeriesRenderer;",
            "Landroid/graphics/Paint;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Float;",
            ">;II)V"
        }
    .end annotation

    .prologue
    .line 547
    .local p5, "points":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Float;>;"
    invoke-interface/range {p5 .. p5}, Ljava/util/List;->size()I

    move-result v2

    const/4 v3, 0x1

    if-le v2, v3, :cond_4

    .line 549
    const/4 v2, 0x0

    move-object/from16 v0, p5

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v10

    .line 550
    .local v10, "previousPointX":F
    const/4 v2, 0x1

    move-object/from16 v0, p5

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v11

    .line 551
    .local v11, "previousPointY":F
    const/4 v9, 0x0

    .local v9, "k":I
    :goto_0
    invoke-interface/range {p5 .. p5}, Ljava/util/List;->size()I

    move-result v2

    if-ge v9, v2, :cond_5

    .line 552
    const/4 v2, 0x2

    if-ne v9, v2, :cond_2

    .line 554
    const/4 v2, 0x2

    move-object/from16 v0, p5

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v3

    const/4 v2, 0x0

    move-object/from16 v0, p5

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    sub-float v2, v3, v2

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    invoke-virtual {p3}, Lorg/achartengine/renderer/SimpleSeriesRenderer;->getDisplayChartValuesDistance()I

    move-result v3

    int-to-float v3, v3

    cmpl-float v2, v2, v3

    if-gtz v2, :cond_0

    const/4 v2, 0x3

    move-object/from16 v0, p5

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v3

    const/4 v2, 0x1

    move-object/from16 v0, p5

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    sub-float v2, v3, v2

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    invoke-virtual {p3}, Lorg/achartengine/renderer/SimpleSeriesRenderer;->getDisplayChartValuesDistance()I

    move-result v3

    int-to-float v3, v3

    cmpl-float v2, v2, v3

    if-lez v2, :cond_1

    .line 556
    :cond_0
    invoke-virtual {p3}, Lorg/achartengine/renderer/SimpleSeriesRenderer;->getChartValuesFormat()Ljava/text/NumberFormat;

    move-result-object v2

    move/from16 v0, p7

    invoke-virtual {p2, v0}, Lorg/achartengine/model/XYSeries;->getY(I)D

    move-result-wide v4

    invoke-virtual {p0, v2, v4, v5}, Lorg/achartengine/chart/XYChart;->getLabel(Ljava/text/NumberFormat;D)Ljava/lang/String;

    move-result-object v4

    const/4 v2, 0x0

    move-object/from16 v0, p5

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v5

    const/4 v2, 0x1

    move-object/from16 v0, p5

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    invoke-virtual {p3}, Lorg/achartengine/renderer/SimpleSeriesRenderer;->getChartValuesSpacing()F

    move-result v3

    sub-float v6, v2, v3

    const/4 v8, 0x0

    move-object v2, p0

    move-object v3, p1

    move-object/from16 v7, p4

    invoke-virtual/range {v2 .. v8}, Lorg/achartengine/chart/XYChart;->drawText(Landroid/graphics/Canvas;Ljava/lang/String;FFLandroid/graphics/Paint;F)V

    .line 558
    invoke-virtual {p3}, Lorg/achartengine/renderer/SimpleSeriesRenderer;->getChartValuesFormat()Ljava/text/NumberFormat;

    move-result-object v2

    add-int/lit8 v3, p7, 0x1

    invoke-virtual {p2, v3}, Lorg/achartengine/model/XYSeries;->getY(I)D

    move-result-wide v4

    invoke-virtual {p0, v2, v4, v5}, Lorg/achartengine/chart/XYChart;->getLabel(Ljava/text/NumberFormat;D)Ljava/lang/String;

    move-result-object v4

    const/4 v2, 0x2

    move-object/from16 v0, p5

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v5

    const/4 v2, 0x3

    move-object/from16 v0, p5

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    invoke-virtual {p3}, Lorg/achartengine/renderer/SimpleSeriesRenderer;->getChartValuesSpacing()F

    move-result v3

    sub-float v6, v2, v3

    const/4 v8, 0x0

    move-object v2, p0

    move-object v3, p1

    move-object/from16 v7, p4

    invoke-virtual/range {v2 .. v8}, Lorg/achartengine/chart/XYChart;->drawText(Landroid/graphics/Canvas;Ljava/lang/String;FFLandroid/graphics/Paint;F)V

    .line 560
    const/4 v2, 0x2

    move-object/from16 v0, p5

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v10

    .line 561
    const/4 v2, 0x3

    move-object/from16 v0, p5

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v11

    .line 551
    :cond_1
    :goto_1
    add-int/lit8 v9, v9, 0x2

    goto/16 :goto_0

    .line 563
    :cond_2
    const/4 v2, 0x2

    if-le v9, v2, :cond_1

    .line 565
    move-object/from16 v0, p5

    invoke-interface {v0, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    sub-float/2addr v2, v10

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    invoke-virtual {p3}, Lorg/achartengine/renderer/SimpleSeriesRenderer;->getDisplayChartValuesDistance()I

    move-result v3

    int-to-float v3, v3

    cmpl-float v2, v2, v3

    if-gtz v2, :cond_3

    add-int/lit8 v2, v9, 0x1

    move-object/from16 v0, p5

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    sub-float/2addr v2, v11

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    invoke-virtual {p3}, Lorg/achartengine/renderer/SimpleSeriesRenderer;->getDisplayChartValuesDistance()I

    move-result v3

    int-to-float v3, v3

    cmpl-float v2, v2, v3

    if-lez v2, :cond_1

    .line 567
    :cond_3
    invoke-virtual {p3}, Lorg/achartengine/renderer/SimpleSeriesRenderer;->getChartValuesFormat()Ljava/text/NumberFormat;

    move-result-object v2

    div-int/lit8 v3, v9, 0x2

    add-int v3, v3, p7

    invoke-virtual {p2, v3}, Lorg/achartengine/model/XYSeries;->getY(I)D

    move-result-wide v4

    invoke-virtual {p0, v2, v4, v5}, Lorg/achartengine/chart/XYChart;->getLabel(Ljava/text/NumberFormat;D)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p5

    invoke-interface {v0, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v5

    add-int/lit8 v2, v9, 0x1

    move-object/from16 v0, p5

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    invoke-virtual {p3}, Lorg/achartengine/renderer/SimpleSeriesRenderer;->getChartValuesSpacing()F

    move-result v3

    sub-float v6, v2, v3

    const/4 v8, 0x0

    move-object v2, p0

    move-object v3, p1

    move-object/from16 v7, p4

    invoke-virtual/range {v2 .. v8}, Lorg/achartengine/chart/XYChart;->drawText(Landroid/graphics/Canvas;Ljava/lang/String;FFLandroid/graphics/Paint;F)V

    .line 569
    move-object/from16 v0, p5

    invoke-interface {v0, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v10

    .line 570
    add-int/lit8 v2, v9, 0x1

    move-object/from16 v0, p5

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v11

    goto/16 :goto_1

    .line 575
    .end local v9    # "k":I
    .end local v10    # "previousPointX":F
    .end local v11    # "previousPointY":F
    :cond_4
    const/4 v9, 0x0

    .restart local v9    # "k":I
    :goto_2
    invoke-interface/range {p5 .. p5}, Ljava/util/List;->size()I

    move-result v2

    if-ge v9, v2, :cond_5

    .line 576
    invoke-virtual {p3}, Lorg/achartengine/renderer/SimpleSeriesRenderer;->getChartValuesFormat()Ljava/text/NumberFormat;

    move-result-object v2

    div-int/lit8 v3, v9, 0x2

    add-int v3, v3, p7

    invoke-virtual {p2, v3}, Lorg/achartengine/model/XYSeries;->getY(I)D

    move-result-wide v4

    invoke-virtual {p0, v2, v4, v5}, Lorg/achartengine/chart/XYChart;->getLabel(Ljava/text/NumberFormat;D)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p5

    invoke-interface {v0, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v5

    add-int/lit8 v2, v9, 0x1

    move-object/from16 v0, p5

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    invoke-virtual {p3}, Lorg/achartengine/renderer/SimpleSeriesRenderer;->getChartValuesSpacing()F

    move-result v3

    sub-float v6, v2, v3

    const/4 v8, 0x0

    move-object v2, p0

    move-object v3, p1

    move-object/from16 v7, p4

    invoke-virtual/range {v2 .. v8}, Lorg/achartengine/chart/XYChart;->drawText(Landroid/graphics/Canvas;Ljava/lang/String;FFLandroid/graphics/Paint;F)V

    .line 575
    add-int/lit8 v9, v9, 0x2

    goto :goto_2

    .line 578
    :cond_5
    return-void
.end method

.method public abstract drawSeries(Landroid/graphics/Canvas;Landroid/graphics/Paint;Ljava/util/List;Lorg/achartengine/renderer/SimpleSeriesRenderer;FII)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/graphics/Canvas;",
            "Landroid/graphics/Paint;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Float;",
            ">;",
            "Lorg/achartengine/renderer/SimpleSeriesRenderer;",
            "FII)V"
        }
    .end annotation
.end method

.method protected drawSeries(Lorg/achartengine/model/XYSeries;Landroid/graphics/Canvas;Landroid/graphics/Paint;Ljava/util/List;Lorg/achartengine/renderer/SimpleSeriesRenderer;FILorg/achartengine/renderer/XYMultipleSeriesRenderer$Orientation;I)V
    .locals 23
    .param p1, "series"    # Lorg/achartengine/model/XYSeries;
    .param p2, "canvas"    # Landroid/graphics/Canvas;
    .param p3, "paint"    # Landroid/graphics/Paint;
    .param p5, "seriesRenderer"    # Lorg/achartengine/renderer/SimpleSeriesRenderer;
    .param p6, "yAxisValue"    # F
    .param p7, "seriesIndex"    # I
    .param p8, "or"    # Lorg/achartengine/renderer/XYMultipleSeriesRenderer$Orientation;
    .param p9, "startIndex"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/achartengine/model/XYSeries;",
            "Landroid/graphics/Canvas;",
            "Landroid/graphics/Paint;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Float;",
            ">;",
            "Lorg/achartengine/renderer/SimpleSeriesRenderer;",
            "FI",
            "Lorg/achartengine/renderer/XYMultipleSeriesRenderer$Orientation;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 499
    .local p4, "pointsList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Float;>;"
    invoke-virtual/range {p5 .. p5}, Lorg/achartengine/renderer/SimpleSeriesRenderer;->getStroke()Lorg/achartengine/renderer/BasicStroke;

    move-result-object v21

    .line 500
    .local v21, "stroke":Lorg/achartengine/renderer/BasicStroke;
    invoke-virtual/range {p3 .. p3}, Landroid/graphics/Paint;->getStrokeCap()Landroid/graphics/Paint$Cap;

    move-result-object v17

    .line 501
    .local v17, "cap":Landroid/graphics/Paint$Cap;
    invoke-virtual/range {p3 .. p3}, Landroid/graphics/Paint;->getStrokeJoin()Landroid/graphics/Paint$Join;

    move-result-object v18

    .line 502
    .local v18, "join":Landroid/graphics/Paint$Join;
    invoke-virtual/range {p3 .. p3}, Landroid/graphics/Paint;->getStrokeMiter()F

    move-result v19

    .line 503
    .local v19, "miter":F
    invoke-virtual/range {p3 .. p3}, Landroid/graphics/Paint;->getPathEffect()Landroid/graphics/PathEffect;

    move-result-object v20

    .line 504
    .local v20, "pathEffect":Landroid/graphics/PathEffect;
    invoke-virtual/range {p3 .. p3}, Landroid/graphics/Paint;->getStyle()Landroid/graphics/Paint$Style;

    move-result-object v22

    .line 505
    .local v22, "style":Landroid/graphics/Paint$Style;
    if-eqz v21, :cond_1

    .line 506
    const/4 v7, 0x0

    .line 507
    .local v7, "effect":Landroid/graphics/PathEffect;
    invoke-virtual/range {v21 .. v21}, Lorg/achartengine/renderer/BasicStroke;->getIntervals()[F

    move-result-object v2

    if-eqz v2, :cond_0

    .line 508
    new-instance v7, Landroid/graphics/DashPathEffect;

    .end local v7    # "effect":Landroid/graphics/PathEffect;
    invoke-virtual/range {v21 .. v21}, Lorg/achartengine/renderer/BasicStroke;->getIntervals()[F

    move-result-object v2

    invoke-virtual/range {v21 .. v21}, Lorg/achartengine/renderer/BasicStroke;->getPhase()F

    move-result v3

    invoke-direct {v7, v2, v3}, Landroid/graphics/DashPathEffect;-><init>([FF)V

    .line 510
    .restart local v7    # "effect":Landroid/graphics/PathEffect;
    :cond_0
    invoke-virtual/range {v21 .. v21}, Lorg/achartengine/renderer/BasicStroke;->getCap()Landroid/graphics/Paint$Cap;

    move-result-object v3

    invoke-virtual/range {v21 .. v21}, Lorg/achartengine/renderer/BasicStroke;->getJoin()Landroid/graphics/Paint$Join;

    move-result-object v4

    invoke-virtual/range {v21 .. v21}, Lorg/achartengine/renderer/BasicStroke;->getMiter()F

    move-result v5

    sget-object v6, Landroid/graphics/Paint$Style;->FILL_AND_STROKE:Landroid/graphics/Paint$Style;

    move-object/from16 v2, p0

    move-object/from16 v8, p3

    invoke-direct/range {v2 .. v8}, Lorg/achartengine/chart/XYChart;->setStroke(Landroid/graphics/Paint$Cap;Landroid/graphics/Paint$Join;FLandroid/graphics/Paint$Style;Landroid/graphics/PathEffect;Landroid/graphics/Paint;)V

    .end local v7    # "effect":Landroid/graphics/PathEffect;
    :cond_1
    move-object/from16 v8, p0

    move-object/from16 v9, p2

    move-object/from16 v10, p3

    move-object/from16 v11, p4

    move-object/from16 v12, p5

    move/from16 v13, p6

    move/from16 v14, p7

    move/from16 v15, p9

    .line 513
    invoke-virtual/range {v8 .. v15}, Lorg/achartengine/chart/XYChart;->drawSeries(Landroid/graphics/Canvas;Landroid/graphics/Paint;Ljava/util/List;Lorg/achartengine/renderer/SimpleSeriesRenderer;FII)V

    .line 514
    move-object/from16 v0, p0

    move-object/from16 v1, p5

    invoke-virtual {v0, v1}, Lorg/achartengine/chart/XYChart;->isRenderPoints(Lorg/achartengine/renderer/SimpleSeriesRenderer;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 515
    invoke-virtual/range {p0 .. p0}, Lorg/achartengine/chart/XYChart;->getPointsChart()Lorg/achartengine/chart/ScatterChart;

    move-result-object v8

    .line 516
    .local v8, "pointsChart":Lorg/achartengine/chart/ScatterChart;
    if-eqz v8, :cond_2

    move-object/from16 v9, p2

    move-object/from16 v10, p3

    move-object/from16 v11, p4

    move-object/from16 v12, p5

    move/from16 v13, p6

    move/from16 v14, p7

    move/from16 v15, p9

    .line 517
    invoke-virtual/range {v8 .. v15}, Lorg/achartengine/chart/ScatterChart;->drawSeries(Landroid/graphics/Canvas;Landroid/graphics/Paint;Ljava/util/List;Lorg/achartengine/renderer/SimpleSeriesRenderer;FII)V

    .line 521
    .end local v8    # "pointsChart":Lorg/achartengine/chart/ScatterChart;
    :cond_2
    invoke-virtual/range {p5 .. p5}, Lorg/achartengine/renderer/SimpleSeriesRenderer;->getChartValuesTextSize()F

    move-result v2

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 522
    sget-object v2, Lorg/achartengine/renderer/XYMultipleSeriesRenderer$Orientation;->HORIZONTAL:Lorg/achartengine/renderer/XYMultipleSeriesRenderer$Orientation;

    move-object/from16 v0, p8

    if-ne v0, v2, :cond_5

    .line 523
    sget-object v2, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 527
    :goto_0
    invoke-virtual/range {p5 .. p5}, Lorg/achartengine/renderer/SimpleSeriesRenderer;->isDisplayChartValues()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 528
    invoke-virtual/range {p5 .. p5}, Lorg/achartengine/renderer/SimpleSeriesRenderer;->getChartValuesTextAlign()Landroid/graphics/Paint$Align;

    move-result-object v2

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    move-object/from16 v9, p0

    move-object/from16 v10, p2

    move-object/from16 v11, p1

    move-object/from16 v12, p5

    move-object/from16 v13, p3

    move-object/from16 v14, p4

    move/from16 v15, p7

    move/from16 v16, p9

    .line 529
    invoke-virtual/range {v9 .. v16}, Lorg/achartengine/chart/XYChart;->drawChartValuesText(Landroid/graphics/Canvas;Lorg/achartengine/model/XYSeries;Lorg/achartengine/renderer/SimpleSeriesRenderer;Landroid/graphics/Paint;Ljava/util/List;II)V

    .line 532
    :cond_3
    if-eqz v21, :cond_4

    move-object/from16 v9, p0

    move-object/from16 v10, v17

    move-object/from16 v11, v18

    move/from16 v12, v19

    move-object/from16 v13, v22

    move-object/from16 v14, v20

    move-object/from16 v15, p3

    .line 533
    invoke-direct/range {v9 .. v15}, Lorg/achartengine/chart/XYChart;->setStroke(Landroid/graphics/Paint$Cap;Landroid/graphics/Paint$Join;FLandroid/graphics/Paint$Style;Landroid/graphics/PathEffect;Landroid/graphics/Paint;)V

    .line 534
    :cond_4
    return-void

    .line 525
    :cond_5
    sget-object v2, Landroid/graphics/Paint$Align;->LEFT:Landroid/graphics/Paint$Align;

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    goto :goto_0
.end method

.method protected drawText(Landroid/graphics/Canvas;Ljava/lang/String;FFLandroid/graphics/Paint;F)V
    .locals 3
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "text"    # Ljava/lang/String;
    .param p3, "x"    # F
    .param p4, "y"    # F
    .param p5, "paint"    # Landroid/graphics/Paint;
    .param p6, "extraAngle"    # F

    .prologue
    const/4 v2, 0x0

    .line 582
    iget-object v1, p0, Lorg/achartengine/chart/XYChart;->mRenderer:Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    invoke-virtual {v1}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->getOrientation()Lorg/achartengine/renderer/XYMultipleSeriesRenderer$Orientation;

    move-result-object v1

    invoke-virtual {v1}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer$Orientation;->getAngle()I

    move-result v1

    neg-int v1, v1

    int-to-float v1, v1

    add-float v0, v1, p6

    .line 583
    .local v0, "angle":F
    cmpl-float v1, v0, v2

    if-eqz v1, :cond_0

    .line 584
    invoke-virtual {p1, v0, p3, p4}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 586
    :cond_0
    invoke-virtual/range {p0 .. p5}, Lorg/achartengine/chart/XYChart;->drawString(Landroid/graphics/Canvas;Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 587
    cmpl-float v1, v0, v2

    if-eqz v1, :cond_1

    .line 588
    neg-float v1, v0

    invoke-virtual {p1, v1, p3, p4}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 590
    :cond_1
    return-void
.end method

.method protected drawXLabels(Ljava/util/List;[Ljava/lang/Double;Landroid/graphics/Canvas;Landroid/graphics/Paint;IIIDDD)V
    .locals 26
    .param p2, "xTextLabelLocations"    # [Ljava/lang/Double;
    .param p3, "canvas"    # Landroid/graphics/Canvas;
    .param p4, "paint"    # Landroid/graphics/Paint;
    .param p5, "left"    # I
    .param p6, "top"    # I
    .param p7, "bottom"    # I
    .param p8, "xPixelsPerUnit"    # D
    .param p10, "minX"    # D
    .param p12, "maxX"    # D
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Double;",
            ">;[",
            "Ljava/lang/Double;",
            "Landroid/graphics/Canvas;",
            "Landroid/graphics/Paint;",
            "IIIDDD)V"
        }
    .end annotation

    .prologue
    .line 607
    .local p1, "xLabels":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Double;>;"
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v21

    .line 608
    .local v21, "length":I
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/achartengine/chart/XYChart;->mRenderer:Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    invoke-virtual {v4}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->isShowLabels()Z

    move-result v25

    .line 609
    .local v25, "showLabels":Z
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/achartengine/chart/XYChart;->mRenderer:Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    invoke-virtual {v4}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->isShowGridY()Z

    move-result v24

    .line 610
    .local v24, "showGridY":Z
    const/16 v20, 0x0

    .local v20, "i":I
    :goto_0
    move/from16 v0, v20

    move/from16 v1, v21

    if-ge v0, v1, :cond_2

    .line 611
    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Double;

    invoke-virtual {v4}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v22

    .line 612
    .local v22, "label":D
    move/from16 v0, p5

    int-to-double v6, v0

    sub-double v8, v22, p10

    mul-double v8, v8, p8

    add-double/2addr v6, v8

    double-to-float v5, v6

    .line 613
    .local v5, "xLabel":F
    if-eqz v25, :cond_0

    .line 614
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/achartengine/chart/XYChart;->mRenderer:Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    invoke-virtual {v4}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->getXLabelsColor()I

    move-result v4

    move-object/from16 v0, p4

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 615
    move/from16 v0, p7

    int-to-float v6, v0

    move/from16 v0, p7

    int-to-float v4, v0

    move-object/from16 v0, p0

    iget-object v7, v0, Lorg/achartengine/chart/XYChart;->mRenderer:Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    invoke-virtual {v7}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->getLabelsTextSize()F

    move-result v7

    const/high16 v8, 0x40400000    # 3.0f

    div-float/2addr v7, v8

    add-float v8, v4, v7

    move-object/from16 v4, p3

    move v7, v5

    move-object/from16 v9, p4

    invoke-virtual/range {v4 .. v9}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 616
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/achartengine/chart/XYChart;->mRenderer:Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    invoke-virtual {v4}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->getXTextLabelSize()I

    move-result v4

    if-nez v4, :cond_0

    .line 617
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/achartengine/chart/XYChart;->mRenderer:Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    invoke-virtual {v4}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->getLabelFormat()Ljava/text/NumberFormat;

    move-result-object v4

    move-object/from16 v0, p0

    move-wide/from16 v1, v22

    invoke-virtual {v0, v4, v1, v2}, Lorg/achartengine/chart/XYChart;->getLabel(Ljava/text/NumberFormat;D)Ljava/lang/String;

    move-result-object v8

    move/from16 v0, p7

    int-to-float v4, v0

    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/achartengine/chart/XYChart;->mRenderer:Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    invoke-virtual {v6}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->getLabelsTextSize()F

    move-result v6

    const/high16 v7, 0x40800000    # 4.0f

    mul-float/2addr v6, v7

    const/high16 v7, 0x40400000    # 3.0f

    div-float/2addr v6, v7

    add-float/2addr v4, v6

    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/achartengine/chart/XYChart;->mRenderer:Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    invoke-virtual {v6}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->getXLabelsPadding()F

    move-result v6

    add-float v10, v4, v6

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/achartengine/chart/XYChart;->mRenderer:Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    invoke-virtual {v4}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->getXLabelsAngle()F

    move-result v12

    move-object/from16 v6, p0

    move-object/from16 v7, p3

    move v9, v5

    move-object/from16 v11, p4

    invoke-virtual/range {v6 .. v12}, Lorg/achartengine/chart/XYChart;->drawText(Landroid/graphics/Canvas;Ljava/lang/String;FFLandroid/graphics/Paint;F)V

    .line 620
    :cond_0
    if-eqz v24, :cond_1

    .line 621
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/achartengine/chart/XYChart;->mRenderer:Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    invoke-virtual {v4}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->getGridColor()I

    move-result v4

    move-object/from16 v0, p4

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 622
    move/from16 v0, p7

    int-to-float v6, v0

    move/from16 v0, p6

    int-to-float v8, v0

    move-object/from16 v4, p3

    move v7, v5

    move-object/from16 v9, p4

    invoke-virtual/range {v4 .. v9}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 610
    :cond_1
    add-int/lit8 v20, v20, 0x1

    goto/16 :goto_0

    .end local v5    # "xLabel":F
    .end local v22    # "label":D
    :cond_2
    move-object/from16 v6, p0

    move-object/from16 v7, p2

    move-object/from16 v8, p3

    move-object/from16 v9, p4

    move/from16 v10, v25

    move/from16 v11, p5

    move/from16 v12, p6

    move/from16 v13, p7

    move-wide/from16 v14, p8

    move-wide/from16 v16, p10

    move-wide/from16 v18, p12

    .line 625
    invoke-virtual/range {v6 .. v19}, Lorg/achartengine/chart/XYChart;->drawXTextLabels([Ljava/lang/Double;Landroid/graphics/Canvas;Landroid/graphics/Paint;ZIIIDDD)V

    .line 626
    return-void
.end method

.method protected drawXTextLabels([Ljava/lang/Double;Landroid/graphics/Canvas;Landroid/graphics/Paint;ZIIIDDD)V
    .locals 16
    .param p1, "xTextLabelLocations"    # [Ljava/lang/Double;
    .param p2, "canvas"    # Landroid/graphics/Canvas;
    .param p3, "paint"    # Landroid/graphics/Paint;
    .param p4, "showLabels"    # Z
    .param p5, "left"    # I
    .param p6, "top"    # I
    .param p7, "bottom"    # I
    .param p8, "xPixelsPerUnit"    # D
    .param p10, "minX"    # D
    .param p12, "maxX"    # D

    .prologue
    .line 679
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/achartengine/chart/XYChart;->mRenderer:Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    invoke-virtual {v2}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->isShowCustomTextGrid()Z

    move-result v15

    .line 680
    .local v15, "showCustomTextGrid":Z
    if-eqz p4, :cond_1

    .line 681
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/achartengine/chart/XYChart;->mRenderer:Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    invoke-virtual {v2}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->getXLabelsColor()I

    move-result v2

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 682
    move-object/from16 v11, p1

    .local v11, "arr$":[Ljava/lang/Double;
    array-length v13, v11

    .local v13, "len$":I
    const/4 v12, 0x0

    .local v12, "i$":I
    :goto_0
    if-ge v12, v13, :cond_1

    aget-object v14, v11, v12

    .line 683
    .local v14, "location":Ljava/lang/Double;
    invoke-virtual {v14}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    cmpg-double v2, p10, v4

    if-gtz v2, :cond_0

    invoke-virtual {v14}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    cmpg-double v2, v4, p12

    if-gtz v2, :cond_0

    .line 684
    move/from16 v0, p5

    int-to-double v4, v0

    invoke-virtual {v14}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v6

    sub-double v6, v6, p10

    mul-double v6, v6, p8

    add-double/2addr v4, v6

    double-to-float v3, v4

    .line 685
    .local v3, "xLabel":F
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/achartengine/chart/XYChart;->mRenderer:Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    invoke-virtual {v2}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->getXLabelsColor()I

    move-result v2

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 686
    move/from16 v0, p7

    int-to-float v4, v0

    move/from16 v0, p7

    int-to-float v2, v0

    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/achartengine/chart/XYChart;->mRenderer:Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    invoke-virtual {v5}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->getLabelsTextSize()F

    move-result v5

    const/high16 v6, 0x40400000    # 3.0f

    div-float/2addr v5, v6

    add-float v6, v2, v5

    move-object/from16 v2, p2

    move v5, v3

    move-object/from16 v7, p3

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 688
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/achartengine/chart/XYChart;->mRenderer:Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    invoke-virtual {v2, v14}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->getXTextLabel(Ljava/lang/Double;)Ljava/lang/String;

    move-result-object v6

    move/from16 v0, p7

    int-to-float v2, v0

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/achartengine/chart/XYChart;->mRenderer:Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    invoke-virtual {v4}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->getLabelsTextSize()F

    move-result v4

    const/high16 v5, 0x40800000    # 4.0f

    mul-float/2addr v4, v5

    const/high16 v5, 0x40400000    # 3.0f

    div-float/2addr v4, v5

    add-float v8, v2, v4

    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/achartengine/chart/XYChart;->mRenderer:Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    invoke-virtual {v2}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->getXLabelsAngle()F

    move-result v10

    move-object/from16 v4, p0

    move-object/from16 v5, p2

    move v7, v3

    move-object/from16 v9, p3

    invoke-virtual/range {v4 .. v10}, Lorg/achartengine/chart/XYChart;->drawText(Landroid/graphics/Canvas;Ljava/lang/String;FFLandroid/graphics/Paint;F)V

    .line 690
    if-eqz v15, :cond_0

    .line 691
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/achartengine/chart/XYChart;->mRenderer:Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    invoke-virtual {v2}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->getGridColor()I

    move-result v2

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 692
    move/from16 v0, p7

    int-to-float v4, v0

    move/from16 v0, p6

    int-to-float v6, v0

    move-object/from16 v2, p2

    move v5, v3

    move-object/from16 v7, p3

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 682
    .end local v3    # "xLabel":F
    :cond_0
    add-int/lit8 v12, v12, 0x1

    goto/16 :goto_0

    .line 696
    .end local v11    # "arr$":[Ljava/lang/Double;
    .end local v12    # "i$":I
    .end local v13    # "len$":I
    .end local v14    # "location":Ljava/lang/Double;
    :cond_1
    return-void
.end method

.method protected drawYLabels(Ljava/util/Map;Landroid/graphics/Canvas;Landroid/graphics/Paint;IIII[D[D)V
    .locals 25
    .param p2, "canvas"    # Landroid/graphics/Canvas;
    .param p3, "paint"    # Landroid/graphics/Paint;
    .param p4, "maxScaleNumber"    # I
    .param p5, "left"    # I
    .param p6, "right"    # I
    .param p7, "bottom"    # I
    .param p8, "yPixelsPerUnit"    # [D
    .param p9, "minY"    # [D
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Double;",
            ">;>;",
            "Landroid/graphics/Canvas;",
            "Landroid/graphics/Paint;",
            "IIII[D[D)V"
        }
    .end annotation

    .prologue
    .line 630
    .local p1, "allYLabels":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Ljava/util/List<Ljava/lang/Double;>;>;"
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/achartengine/chart/XYChart;->mRenderer:Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    invoke-virtual {v4}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->getOrientation()Lorg/achartengine/renderer/XYMultipleSeriesRenderer$Orientation;

    move-result-object v20

    .line 631
    .local v20, "or":Lorg/achartengine/renderer/XYMultipleSeriesRenderer$Orientation;
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/achartengine/chart/XYChart;->mRenderer:Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    invoke-virtual {v4}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->isShowGridX()Z

    move-result v21

    .line 632
    .local v21, "showGridX":Z
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/achartengine/chart/XYChart;->mRenderer:Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    invoke-virtual {v4}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->isShowLabels()Z

    move-result v22

    .line 633
    .local v22, "showLabels":Z
    const/4 v15, 0x0

    .local v15, "i":I
    :goto_0
    move/from16 v0, p4

    if-ge v15, v0, :cond_7

    .line 634
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/achartengine/chart/XYChart;->mRenderer:Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    invoke-virtual {v4, v15}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->getYLabelsAlign(I)Landroid/graphics/Paint$Align;

    move-result-object v4

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 635
    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Ljava/util/List;

    .line 636
    .local v24, "yLabels":Ljava/util/List;
    invoke-interface/range {v24 .. v24}, Ljava/util/List;->size()I

    move-result v17

    .line 637
    .local v17, "length":I
    const/16 v16, 0x0

    .local v16, "j":I
    :goto_1
    move/from16 v0, v16

    move/from16 v1, v17

    if-ge v0, v1, :cond_6

    .line 638
    move-object/from16 v0, v24

    move/from16 v1, v16

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Double;

    invoke-virtual {v4}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v18

    .line 639
    .local v18, "label":D
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/achartengine/chart/XYChart;->mRenderer:Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    invoke-virtual {v4, v15}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->getYAxisAlign(I)Landroid/graphics/Paint$Align;

    move-result-object v14

    .line 640
    .local v14, "axisAlign":Landroid/graphics/Paint$Align;
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/achartengine/chart/XYChart;->mRenderer:Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    invoke-static/range {v18 .. v19}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    invoke-virtual {v4, v5, v15}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->getYTextLabel(Ljava/lang/Double;I)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_2

    const/16 v23, 0x1

    .line 641
    .local v23, "textLabel":Z
    :goto_2
    move/from16 v0, p7

    int-to-double v4, v0

    aget-wide v8, p8, v15

    aget-wide v10, p9, v15

    sub-double v10, v18, v10

    mul-double/2addr v8, v10

    sub-double/2addr v4, v8

    double-to-float v6, v4

    .line 642
    .local v6, "yLabel":F
    sget-object v4, Lorg/achartengine/renderer/XYMultipleSeriesRenderer$Orientation;->HORIZONTAL:Lorg/achartengine/renderer/XYMultipleSeriesRenderer$Orientation;

    move-object/from16 v0, v20

    if-ne v0, v4, :cond_4

    .line 643
    if-eqz v22, :cond_0

    if-nez v23, :cond_0

    .line 644
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/achartengine/chart/XYChart;->mRenderer:Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    invoke-virtual {v4, v15}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->getYLabelsColor(I)I

    move-result v4

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 645
    sget-object v4, Landroid/graphics/Paint$Align;->LEFT:Landroid/graphics/Paint$Align;

    if-ne v14, v4, :cond_3

    .line 646
    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lorg/achartengine/chart/XYChart;->getLabelLinePos(Landroid/graphics/Paint$Align;)I

    move-result v4

    add-int v4, v4, p5

    int-to-float v5, v4

    move/from16 v0, p5

    int-to-float v7, v0

    move-object/from16 v4, p2

    move v8, v6

    move-object/from16 v9, p3

    invoke-virtual/range {v4 .. v9}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 647
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/achartengine/chart/XYChart;->mRenderer:Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    invoke-virtual {v4}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->getLabelFormat()Ljava/text/NumberFormat;

    move-result-object v4

    move-object/from16 v0, p0

    move-wide/from16 v1, v18

    invoke-virtual {v0, v4, v1, v2}, Lorg/achartengine/chart/XYChart;->getLabel(Ljava/text/NumberFormat;D)Ljava/lang/String;

    move-result-object v9

    move/from16 v0, p5

    int-to-float v4, v0

    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/achartengine/chart/XYChart;->mRenderer:Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    invoke-virtual {v5}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->getYLabelsPadding()F

    move-result v5

    sub-float/2addr v4, v5

    const/high16 v5, 0x41700000    # 15.0f

    sub-float v10, v4, v5

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/achartengine/chart/XYChart;->mRenderer:Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    invoke-virtual {v4}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->getYLabelsVerticalPadding()F

    move-result v4

    add-float v11, v6, v4

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/achartengine/chart/XYChart;->mRenderer:Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    invoke-virtual {v4}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->getYLabelsAngle()F

    move-result v13

    move-object/from16 v7, p0

    move-object/from16 v8, p2

    move-object/from16 v12, p3

    invoke-virtual/range {v7 .. v13}, Lorg/achartengine/chart/XYChart;->drawText(Landroid/graphics/Canvas;Ljava/lang/String;FFLandroid/graphics/Paint;F)V

    .line 657
    :cond_0
    :goto_3
    if-eqz v21, :cond_1

    .line 658
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/achartengine/chart/XYChart;->mRenderer:Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    invoke-virtual {v4}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->getGridColor()I

    move-result v4

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 659
    move/from16 v0, p5

    int-to-float v5, v0

    move/from16 v0, p6

    int-to-float v7, v0

    move-object/from16 v4, p2

    move v8, v6

    move-object/from16 v9, p3

    invoke-virtual/range {v4 .. v9}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 637
    :cond_1
    :goto_4
    add-int/lit8 v16, v16, 0x1

    goto/16 :goto_1

    .line 640
    .end local v6    # "yLabel":F
    .end local v23    # "textLabel":Z
    :cond_2
    const/16 v23, 0x0

    goto/16 :goto_2

    .line 651
    .restart local v6    # "yLabel":F
    .restart local v23    # "textLabel":Z
    :cond_3
    move/from16 v0, p6

    int-to-float v5, v0

    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lorg/achartengine/chart/XYChart;->getLabelLinePos(Landroid/graphics/Paint$Align;)I

    move-result v4

    add-int v4, v4, p6

    int-to-float v7, v4

    move-object/from16 v4, p2

    move v8, v6

    move-object/from16 v9, p3

    invoke-virtual/range {v4 .. v9}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 652
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/achartengine/chart/XYChart;->mRenderer:Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    invoke-virtual {v4}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->getLabelFormat()Ljava/text/NumberFormat;

    move-result-object v4

    move-object/from16 v0, p0

    move-wide/from16 v1, v18

    invoke-virtual {v0, v4, v1, v2}, Lorg/achartengine/chart/XYChart;->getLabel(Ljava/text/NumberFormat;D)Ljava/lang/String;

    move-result-object v9

    move/from16 v0, p6

    int-to-float v4, v0

    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/achartengine/chart/XYChart;->mRenderer:Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    invoke-virtual {v5}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->getYLabelsPadding()F

    move-result v5

    add-float/2addr v4, v5

    const/high16 v5, 0x41700000    # 15.0f

    add-float v10, v4, v5

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/achartengine/chart/XYChart;->mRenderer:Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    invoke-virtual {v4}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->getYLabelsVerticalPadding()F

    move-result v4

    sub-float v11, v6, v4

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/achartengine/chart/XYChart;->mRenderer:Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    invoke-virtual {v4}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->getYLabelsAngle()F

    move-result v13

    move-object/from16 v7, p0

    move-object/from16 v8, p2

    move-object/from16 v12, p3

    invoke-virtual/range {v7 .. v13}, Lorg/achartengine/chart/XYChart;->drawText(Landroid/graphics/Canvas;Ljava/lang/String;FFLandroid/graphics/Paint;F)V

    goto :goto_3

    .line 661
    :cond_4
    sget-object v4, Lorg/achartengine/renderer/XYMultipleSeriesRenderer$Orientation;->VERTICAL:Lorg/achartengine/renderer/XYMultipleSeriesRenderer$Orientation;

    move-object/from16 v0, v20

    if-ne v0, v4, :cond_1

    .line 662
    if-eqz v22, :cond_5

    if-nez v23, :cond_5

    .line 663
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/achartengine/chart/XYChart;->mRenderer:Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    invoke-virtual {v4, v15}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->getYLabelsColor(I)I

    move-result v4

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 664
    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lorg/achartengine/chart/XYChart;->getLabelLinePos(Landroid/graphics/Paint$Align;)I

    move-result v4

    sub-int v4, p6, v4

    int-to-float v5, v4

    move/from16 v0, p6

    int-to-float v7, v0

    move-object/from16 v4, p2

    move v8, v6

    move-object/from16 v9, p3

    invoke-virtual/range {v4 .. v9}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 665
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/achartengine/chart/XYChart;->mRenderer:Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    invoke-virtual {v4}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->getLabelFormat()Ljava/text/NumberFormat;

    move-result-object v4

    move-object/from16 v0, p0

    move-wide/from16 v1, v18

    invoke-virtual {v0, v4, v1, v2}, Lorg/achartengine/chart/XYChart;->getLabel(Ljava/text/NumberFormat;D)Ljava/lang/String;

    move-result-object v9

    add-int/lit8 v4, p6, 0xa

    int-to-float v4, v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/achartengine/chart/XYChart;->mRenderer:Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    invoke-virtual {v5}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->getYLabelsPadding()F

    move-result v5

    add-float v10, v4, v5

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/achartengine/chart/XYChart;->mRenderer:Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    invoke-virtual {v4}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->getYLabelsVerticalPadding()F

    move-result v4

    sub-float v11, v6, v4

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/achartengine/chart/XYChart;->mRenderer:Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    invoke-virtual {v4}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->getYLabelsAngle()F

    move-result v13

    move-object/from16 v7, p0

    move-object/from16 v8, p2

    move-object/from16 v12, p3

    invoke-virtual/range {v7 .. v13}, Lorg/achartengine/chart/XYChart;->drawText(Landroid/graphics/Canvas;Ljava/lang/String;FFLandroid/graphics/Paint;F)V

    .line 668
    :cond_5
    if-eqz v21, :cond_1

    .line 669
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/achartengine/chart/XYChart;->mRenderer:Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    invoke-virtual {v4}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->getGridColor()I

    move-result v4

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 670
    move/from16 v0, p6

    int-to-float v5, v0

    move/from16 v0, p5

    int-to-float v7, v0

    move-object/from16 v4, p2

    move v8, v6

    move-object/from16 v9, p3

    invoke-virtual/range {v4 .. v9}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    goto/16 :goto_4

    .line 633
    .end local v6    # "yLabel":F
    .end local v14    # "axisAlign":Landroid/graphics/Paint$Align;
    .end local v18    # "label":D
    .end local v23    # "textLabel":Z
    :cond_6
    add-int/lit8 v15, v15, 0x1

    goto/16 :goto_0

    .line 675
    .end local v16    # "j":I
    .end local v17    # "length":I
    .end local v24    # "yLabels":Ljava/util/List;
    :cond_7
    return-void
.end method

.method public getCalcRange(I)[D
    .locals 2
    .param p1, "scale"    # I

    .prologue
    .line 708
    iget-object v0, p0, Lorg/achartengine/chart/XYChart;->mCalcRange:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [D

    check-cast v0, [D

    return-object v0
.end method

.method public abstract getChartType()Ljava/lang/String;
.end method

.method public getDataset()Lorg/achartengine/model/XYMultipleSeriesDataset;
    .locals 1

    .prologue
    .line 704
    iget-object v0, p0, Lorg/achartengine/chart/XYChart;->mDataset:Lorg/achartengine/model/XYMultipleSeriesDataset;

    return-object v0
.end method

.method public getDefaultMinimum()D
    .locals 2

    .prologue
    .line 808
    const-wide v0, 0x7fefffffffffffffL    # Double.MAX_VALUE

    return-wide v0
.end method

.method public getPointsChart()Lorg/achartengine/chart/ScatterChart;
    .locals 1

    .prologue
    .line 813
    const/4 v0, 0x0

    return-object v0
.end method

.method public getRenderer()Lorg/achartengine/renderer/XYMultipleSeriesRenderer;
    .locals 1

    .prologue
    .line 700
    iget-object v0, p0, Lorg/achartengine/chart/XYChart;->mRenderer:Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    return-object v0
.end method

.method protected getScreenR()Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 480
    iget-object v0, p0, Lorg/achartengine/chart/XYChart;->mScreenR:Landroid/graphics/Rect;

    return-object v0
.end method

.method public getSeriesAndPointForScreenCoordinate(Lorg/achartengine/model/Point;)Lorg/achartengine/model/SeriesSelection;
    .locals 10
    .param p1, "screenPoint"    # Lorg/achartengine/model/Point;

    .prologue
    .line 769
    iget-object v1, p0, Lorg/achartengine/chart/XYChart;->clickableAreas:Ljava/util/Map;

    if-eqz v1, :cond_2

    .line 772
    iget-object v1, p0, Lorg/achartengine/chart/XYChart;->clickableAreas:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v1

    add-int/lit8 v2, v1, -0x1

    .local v2, "seriesIndex":I
    :goto_0
    if-ltz v2, :cond_2

    .line 774
    const/4 v3, 0x0

    .line 775
    .local v3, "pointIndex":I
    iget-object v1, p0, Lorg/achartengine/chart/XYChart;->clickableAreas:Ljava/util/Map;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 777
    iget-object v1, p0, Lorg/achartengine/chart/XYChart;->clickableAreas:Ljava/util/Map;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .local v8, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/achartengine/chart/ClickableArea;

    .line 778
    .local v0, "area":Lorg/achartengine/chart/ClickableArea;
    if-eqz v0, :cond_0

    .line 779
    invoke-virtual {v0}, Lorg/achartengine/chart/ClickableArea;->getRect()Landroid/graphics/RectF;

    move-result-object v9

    .line 780
    .local v9, "rectangle":Landroid/graphics/RectF;
    if-eqz v9, :cond_0

    invoke-virtual {p1}, Lorg/achartengine/model/Point;->getX()F

    move-result v1

    invoke-virtual {p1}, Lorg/achartengine/model/Point;->getY()F

    move-result v4

    invoke-virtual {v9, v1, v4}, Landroid/graphics/RectF;->contains(FF)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 781
    new-instance v1, Lorg/achartengine/model/SeriesSelection;

    invoke-virtual {v0}, Lorg/achartengine/chart/ClickableArea;->getX()D

    move-result-wide v4

    invoke-virtual {v0}, Lorg/achartengine/chart/ClickableArea;->getY()D

    move-result-wide v6

    invoke-direct/range {v1 .. v7}, Lorg/achartengine/model/SeriesSelection;-><init>(IIDD)V

    .line 789
    .end local v0    # "area":Lorg/achartengine/chart/ClickableArea;
    .end local v2    # "seriesIndex":I
    .end local v3    # "pointIndex":I
    .end local v8    # "i$":Ljava/util/Iterator;
    .end local v9    # "rectangle":Landroid/graphics/RectF;
    :goto_2
    return-object v1

    .line 784
    .restart local v0    # "area":Lorg/achartengine/chart/ClickableArea;
    .restart local v2    # "seriesIndex":I
    .restart local v3    # "pointIndex":I
    .restart local v8    # "i$":Ljava/util/Iterator;
    :cond_0
    add-int/lit8 v3, v3, 0x1

    .line 785
    goto :goto_1

    .line 772
    .end local v0    # "area":Lorg/achartengine/chart/ClickableArea;
    .end local v8    # "i$":Ljava/util/Iterator;
    :cond_1
    add-int/lit8 v2, v2, -0x1

    goto :goto_0

    .line 789
    .end local v2    # "seriesIndex":I
    .end local v3    # "pointIndex":I
    :cond_2
    invoke-super {p0, p1}, Lorg/achartengine/chart/AbstractChart;->getSeriesAndPointForScreenCoordinate(Lorg/achartengine/model/Point;)Lorg/achartengine/model/SeriesSelection;

    move-result-object v1

    goto :goto_2
.end method

.method protected getXLabels(DDI)Ljava/util/List;
    .locals 1
    .param p1, "min"    # D
    .param p3, "max"    # D
    .param p5, "count"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(DDI)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Double;",
            ">;"
        }
    .end annotation

    .prologue
    .line 467
    invoke-static {p1, p2, p3, p4, p5}, Lorg/achartengine/util/MathHelper;->getLabels(DDI)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected getYLabels([D[DI)Ljava/util/Map;
    .locals 8
    .param p1, "minY"    # [D
    .param p2, "maxY"    # [D
    .param p3, "maxScaleNumber"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([D[DI)",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Double;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 471
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 472
    .local v0, "allYLabels":Ljava/util/Map;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, p3, :cond_0

    .line 473
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aget-wide v4, p1, v1

    aget-wide v6, p2, v1

    iget-object v3, p0, Lorg/achartengine/chart/XYChart;->mRenderer:Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    invoke-virtual {v3}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->getYLabels()I

    move-result v3

    invoke-static {v4, v5, v6, v7, v3}, Lorg/achartengine/util/MathHelper;->getLabels(DDI)Ljava/util/List;

    move-result-object v3

    invoke-direct {p0, v3}, Lorg/achartengine/chart/XYChart;->getValidLabels(Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 472
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 476
    :cond_0
    return-object v0
.end method

.method protected isRenderNullValues()Z
    .locals 1

    .prologue
    .line 798
    const/4 v0, 0x0

    return v0
.end method

.method public isRenderPoints(Lorg/achartengine/renderer/SimpleSeriesRenderer;)Z
    .locals 1
    .param p1, "renderer"    # Lorg/achartengine/renderer/SimpleSeriesRenderer;

    .prologue
    .line 803
    const/4 v0, 0x0

    return v0
.end method

.method public setCalcRange([DI)V
    .locals 2
    .param p1, "range"    # [D
    .param p2, "scale"    # I

    .prologue
    .line 712
    iget-object v0, p0, Lorg/achartengine/chart/XYChart;->mCalcRange:Ljava/util/Map;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 713
    return-void
.end method

.method protected setDatasetRenderer(Lorg/achartengine/model/XYMultipleSeriesDataset;Lorg/achartengine/renderer/XYMultipleSeriesRenderer;)V
    .locals 0
    .param p1, "dataset"    # Lorg/achartengine/model/XYMultipleSeriesDataset;
    .param p2, "renderer"    # Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    .prologue
    .line 56
    iput-object p1, p0, Lorg/achartengine/chart/XYChart;->mDataset:Lorg/achartengine/model/XYMultipleSeriesDataset;

    .line 57
    iput-object p2, p0, Lorg/achartengine/chart/XYChart;->mRenderer:Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    .line 58
    return-void
.end method

.method protected setScreenR(Landroid/graphics/Rect;)V
    .locals 0
    .param p1, "screenR"    # Landroid/graphics/Rect;

    .prologue
    .line 484
    iput-object p1, p0, Lorg/achartengine/chart/XYChart;->mScreenR:Landroid/graphics/Rect;

    .line 485
    return-void
.end method

.method public toRealPoint(FF)[D
    .locals 1
    .param p1, "screenX"    # F
    .param p2, "screenY"    # F

    .prologue
    .line 716
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lorg/achartengine/chart/XYChart;->toRealPoint(FFI)[D

    move-result-object v0

    return-object v0
.end method

.method public toRealPoint(FFI)[D
    .locals 16
    .param p1, "screenX"    # F
    .param p2, "screenY"    # F
    .param p3, "scale"    # I

    .prologue
    .line 733
    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/achartengine/chart/XYChart;->mRenderer:Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    move/from16 v0, p3

    invoke-virtual {v10, v0}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->getXAxisMin(I)D

    move-result-wide v6

    .line 734
    .local v6, "realMinX":D
    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/achartengine/chart/XYChart;->mRenderer:Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    move/from16 v0, p3

    invoke-virtual {v10, v0}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->getXAxisMax(I)D

    move-result-wide v2

    .line 735
    .local v2, "realMaxX":D
    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/achartengine/chart/XYChart;->mRenderer:Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    move/from16 v0, p3

    invoke-virtual {v10, v0}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->getYAxisMin(I)D

    move-result-wide v8

    .line 736
    .local v8, "realMinY":D
    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/achartengine/chart/XYChart;->mRenderer:Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    move/from16 v0, p3

    invoke-virtual {v10, v0}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->getYAxisMax(I)D

    move-result-wide v4

    .line 737
    .local v4, "realMaxY":D
    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/achartengine/chart/XYChart;->mScreenR:Landroid/graphics/Rect;

    if-eqz v10, :cond_0

    .line 738
    const/4 v10, 0x2

    new-array v10, v10, [D

    const/4 v11, 0x0

    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/achartengine/chart/XYChart;->mScreenR:Landroid/graphics/Rect;

    iget v12, v12, Landroid/graphics/Rect;->left:I

    int-to-float v12, v12

    sub-float v12, p1, v12

    float-to-double v12, v12

    sub-double v14, v2, v6

    mul-double/2addr v12, v14

    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/achartengine/chart/XYChart;->mScreenR:Landroid/graphics/Rect;

    invoke-virtual {v14}, Landroid/graphics/Rect;->width()I

    move-result v14

    int-to-double v14, v14

    div-double/2addr v12, v14

    add-double/2addr v12, v6

    aput-wide v12, v10, v11

    const/4 v11, 0x1

    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/achartengine/chart/XYChart;->mScreenR:Landroid/graphics/Rect;

    iget v12, v12, Landroid/graphics/Rect;->top:I

    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/achartengine/chart/XYChart;->mScreenR:Landroid/graphics/Rect;

    invoke-virtual {v13}, Landroid/graphics/Rect;->height()I

    move-result v13

    add-int/2addr v12, v13

    int-to-float v12, v12

    sub-float v12, v12, p2

    float-to-double v12, v12

    sub-double v14, v4, v8

    mul-double/2addr v12, v14

    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/achartengine/chart/XYChart;->mScreenR:Landroid/graphics/Rect;

    invoke-virtual {v14}, Landroid/graphics/Rect;->height()I

    move-result v14

    int-to-double v14, v14

    div-double/2addr v12, v14

    add-double/2addr v12, v8

    aput-wide v12, v10, v11

    .line 741
    :goto_0
    return-object v10

    :cond_0
    const/4 v10, 0x2

    new-array v10, v10, [D

    const/4 v11, 0x0

    move/from16 v0, p1

    float-to-double v12, v0

    aput-wide v12, v10, v11

    const/4 v11, 0x1

    move/from16 v0, p2

    float-to-double v12, v0

    aput-wide v12, v10, v11

    goto :goto_0
.end method

.method public toScreenPoint([D)[D
    .locals 1
    .param p1, "realPoint"    # [D

    .prologue
    .line 720
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lorg/achartengine/chart/XYChart;->toScreenPoint([DI)[D

    move-result-object v0

    return-object v0
.end method

.method public toScreenPoint([DI)[D
    .locals 18
    .param p1, "realPoint"    # [D
    .param p2, "scale"    # I

    .prologue
    .line 746
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/achartengine/chart/XYChart;->mRenderer:Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    move/from16 v0, p2

    invoke-virtual {v3, v0}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->getXAxisMin(I)D

    move-result-wide v8

    .line 747
    .local v8, "realMinX":D
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/achartengine/chart/XYChart;->mRenderer:Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    move/from16 v0, p2

    invoke-virtual {v3, v0}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->getXAxisMax(I)D

    move-result-wide v4

    .line 748
    .local v4, "realMaxX":D
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/achartengine/chart/XYChart;->mRenderer:Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    move/from16 v0, p2

    invoke-virtual {v3, v0}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->getYAxisMin(I)D

    move-result-wide v10

    .line 749
    .local v10, "realMinY":D
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/achartengine/chart/XYChart;->mRenderer:Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    move/from16 v0, p2

    invoke-virtual {v3, v0}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->getYAxisMax(I)D

    move-result-wide v6

    .line 750
    .local v6, "realMaxY":D
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/achartengine/chart/XYChart;->mRenderer:Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    move/from16 v0, p2

    invoke-virtual {v3, v0}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->isMinXSet(I)Z

    move-result v3

    if-eqz v3, :cond_0

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/achartengine/chart/XYChart;->mRenderer:Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    move/from16 v0, p2

    invoke-virtual {v3, v0}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->isMaxXSet(I)Z

    move-result v3

    if-eqz v3, :cond_0

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/achartengine/chart/XYChart;->mRenderer:Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    move/from16 v0, p2

    invoke-virtual {v3, v0}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->isMinXSet(I)Z

    move-result v3

    if-eqz v3, :cond_0

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/achartengine/chart/XYChart;->mRenderer:Lorg/achartengine/renderer/XYMultipleSeriesRenderer;

    move/from16 v0, p2

    invoke-virtual {v3, v0}, Lorg/achartengine/renderer/XYMultipleSeriesRenderer;->isMaxYSet(I)Z

    move-result v3

    if-nez v3, :cond_1

    .line 752
    :cond_0
    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Lorg/achartengine/chart/XYChart;->getCalcRange(I)[D

    move-result-object v2

    .line 753
    .local v2, "calcRange":[D
    if-eqz v2, :cond_1

    .line 754
    const/4 v3, 0x0

    aget-wide v8, v2, v3

    .line 755
    const/4 v3, 0x1

    aget-wide v4, v2, v3

    .line 756
    const/4 v3, 0x2

    aget-wide v10, v2, v3

    .line 757
    const/4 v3, 0x3

    aget-wide v6, v2, v3

    .line 760
    .end local v2    # "calcRange":[D
    :cond_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/achartengine/chart/XYChart;->mScreenR:Landroid/graphics/Rect;

    if-eqz v3, :cond_2

    .line 761
    const/4 v3, 0x2

    new-array v3, v3, [D

    const/4 v12, 0x0

    const/4 v13, 0x0

    aget-wide v14, p1, v13

    sub-double/2addr v14, v8

    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/achartengine/chart/XYChart;->mScreenR:Landroid/graphics/Rect;

    invoke-virtual {v13}, Landroid/graphics/Rect;->width()I

    move-result v13

    int-to-double v0, v13

    move-wide/from16 v16, v0

    mul-double v14, v14, v16

    sub-double v16, v4, v8

    div-double v14, v14, v16

    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/achartengine/chart/XYChart;->mScreenR:Landroid/graphics/Rect;

    iget v13, v13, Landroid/graphics/Rect;->left:I

    int-to-double v0, v13

    move-wide/from16 v16, v0

    add-double v14, v14, v16

    aput-wide v14, v3, v12

    const/4 v12, 0x1

    const/4 v13, 0x1

    aget-wide v14, p1, v13

    sub-double v14, v6, v14

    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/achartengine/chart/XYChart;->mScreenR:Landroid/graphics/Rect;

    invoke-virtual {v13}, Landroid/graphics/Rect;->height()I

    move-result v13

    int-to-double v0, v13

    move-wide/from16 v16, v0

    mul-double v14, v14, v16

    sub-double v16, v6, v10

    div-double v14, v14, v16

    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/achartengine/chart/XYChart;->mScreenR:Landroid/graphics/Rect;

    iget v13, v13, Landroid/graphics/Rect;->top:I

    int-to-double v0, v13

    move-wide/from16 v16, v0

    add-double v14, v14, v16

    aput-wide v14, v3, v12

    move-object/from16 p1, v3

    .line 764
    .end local p1    # "realPoint":[D
    :cond_2
    return-object p1
.end method
