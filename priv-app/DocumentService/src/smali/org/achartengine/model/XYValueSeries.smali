.class public Lorg/achartengine/model/XYValueSeries;
.super Lorg/achartengine/model/XYSeries;
.source "XYValueSeries.java"


# instance fields
.field private mMaxValue:D

.field private mMinValue:D

.field private mValue:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Double;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 2
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 16
    invoke-direct {p0, p1}, Lorg/achartengine/model/XYSeries;-><init>(Ljava/lang/String;)V

    .line 8
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/achartengine/model/XYValueSeries;->mValue:Ljava/util/List;

    .line 10
    const-wide v0, 0x7fefffffffffffffL    # Double.MAX_VALUE

    iput-wide v0, p0, Lorg/achartengine/model/XYValueSeries;->mMinValue:D

    .line 12
    const-wide v0, -0x10000000000005L

    iput-wide v0, p0, Lorg/achartengine/model/XYValueSeries;->mMaxValue:D

    .line 17
    return-void
.end method

.method private initRange()V
    .locals 4

    .prologue
    const-wide v2, 0x7fefffffffffffffL    # Double.MAX_VALUE

    .line 28
    iput-wide v2, p0, Lorg/achartengine/model/XYValueSeries;->mMinValue:D

    .line 29
    iput-wide v2, p0, Lorg/achartengine/model/XYValueSeries;->mMaxValue:D

    .line 30
    invoke-virtual {p0}, Lorg/achartengine/model/XYValueSeries;->getItemCount()I

    move-result v1

    .line 31
    .local v1, "length":I
    const/4 v0, 0x0

    .local v0, "k":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 32
    invoke-virtual {p0, v0}, Lorg/achartengine/model/XYValueSeries;->getValue(I)D

    move-result-wide v2

    invoke-direct {p0, v2, v3}, Lorg/achartengine/model/XYValueSeries;->updateRange(D)V

    .line 31
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 33
    :cond_0
    return-void
.end method

.method private updateRange(D)V
    .locals 3
    .param p1, "value"    # D

    .prologue
    .line 37
    iget-wide v0, p0, Lorg/achartengine/model/XYValueSeries;->mMinValue:D

    invoke-static {v0, v1, p1, p2}, Ljava/lang/Math;->min(DD)D

    move-result-wide v0

    iput-wide v0, p0, Lorg/achartengine/model/XYValueSeries;->mMinValue:D

    .line 38
    iget-wide v0, p0, Lorg/achartengine/model/XYValueSeries;->mMaxValue:D

    invoke-static {v0, v1, p1, p2}, Ljava/lang/Math;->max(DD)D

    move-result-wide v0

    iput-wide v0, p0, Lorg/achartengine/model/XYValueSeries;->mMaxValue:D

    .line 39
    return-void
.end method


# virtual methods
.method public declared-synchronized add(DD)V
    .locals 9
    .param p1, "x"    # D
    .param p3, "y"    # D

    .prologue
    .line 43
    monitor-enter p0

    const-wide/16 v6, 0x0

    move-object v1, p0

    move-wide v2, p1

    move-wide v4, p3

    :try_start_0
    invoke-virtual/range {v1 .. v7}, Lorg/achartengine/model/XYValueSeries;->add(DDD)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 44
    monitor-exit p0

    return-void

    .line 43
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized add(DDD)V
    .locals 3
    .param p1, "x"    # D
    .param p3, "y"    # D
    .param p5, "value"    # D

    .prologue
    .line 21
    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1, p2, p3, p4}, Lorg/achartengine/model/XYSeries;->add(DD)V

    .line 22
    iget-object v0, p0, Lorg/achartengine/model/XYValueSeries;->mValue:Ljava/util/List;

    invoke-static {p5, p6}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 23
    invoke-direct {p0, p5, p6}, Lorg/achartengine/model/XYValueSeries;->updateRange(D)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 24
    monitor-exit p0

    return-void

    .line 21
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized clear()V
    .locals 1

    .prologue
    .line 56
    monitor-enter p0

    :try_start_0
    invoke-super {p0}, Lorg/achartengine/model/XYSeries;->clear()V

    .line 57
    iget-object v0, p0, Lorg/achartengine/model/XYValueSeries;->mValue:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 58
    invoke-direct {p0}, Lorg/achartengine/model/XYValueSeries;->initRange()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 59
    monitor-exit p0

    return-void

    .line 56
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getMaxValue()D
    .locals 2

    .prologue
    .line 73
    iget-wide v0, p0, Lorg/achartengine/model/XYValueSeries;->mMaxValue:D

    return-wide v0
.end method

.method public getMinValue()D
    .locals 2

    .prologue
    .line 68
    iget-wide v0, p0, Lorg/achartengine/model/XYValueSeries;->mMinValue:D

    return-wide v0
.end method

.method public declared-synchronized getValue(I)D
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 63
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/achartengine/model/XYValueSeries;->mValue:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v0

    monitor-exit p0

    return-wide v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized remove(I)V
    .locals 4
    .param p1, "index"    # I

    .prologue
    .line 48
    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1}, Lorg/achartengine/model/XYSeries;->remove(I)V

    .line 49
    iget-object v2, p0, Lorg/achartengine/model/XYValueSeries;->mValue:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Double;

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    .line 50
    .local v0, "removedValue":D
    iget-wide v2, p0, Lorg/achartengine/model/XYValueSeries;->mMinValue:D

    cmpl-double v2, v0, v2

    if-eqz v2, :cond_0

    iget-wide v2, p0, Lorg/achartengine/model/XYValueSeries;->mMaxValue:D

    cmpl-double v2, v0, v2

    if-nez v2, :cond_1

    .line 51
    :cond_0
    invoke-direct {p0}, Lorg/achartengine/model/XYValueSeries;->initRange()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 52
    :cond_1
    monitor-exit p0

    return-void

    .line 48
    .end local v0    # "removedValue":D
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method
