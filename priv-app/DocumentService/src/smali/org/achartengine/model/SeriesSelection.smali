.class public Lorg/achartengine/model/SeriesSelection;
.super Ljava/lang/Object;
.source "SeriesSelection.java"


# instance fields
.field private mPointIndex:I

.field private mSeriesIndex:I

.field private mValue:D

.field private mXValue:D


# direct methods
.method public constructor <init>(IIDD)V
    .locals 1
    .param p1, "seriesIndex"    # I
    .param p2, "pointIndex"    # I
    .param p3, "xValue"    # D
    .param p5, "value"    # D

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    iput p1, p0, Lorg/achartengine/model/SeriesSelection;->mSeriesIndex:I

    .line 13
    iput p2, p0, Lorg/achartengine/model/SeriesSelection;->mPointIndex:I

    .line 14
    iput-wide p3, p0, Lorg/achartengine/model/SeriesSelection;->mXValue:D

    .line 15
    iput-wide p5, p0, Lorg/achartengine/model/SeriesSelection;->mValue:D

    .line 16
    return-void
.end method


# virtual methods
.method public getPointIndex()I
    .locals 1

    .prologue
    .line 23
    iget v0, p0, Lorg/achartengine/model/SeriesSelection;->mPointIndex:I

    return v0
.end method

.method public getSeriesIndex()I
    .locals 1

    .prologue
    .line 19
    iget v0, p0, Lorg/achartengine/model/SeriesSelection;->mSeriesIndex:I

    return v0
.end method

.method public getValue()D
    .locals 2

    .prologue
    .line 31
    iget-wide v0, p0, Lorg/achartengine/model/SeriesSelection;->mValue:D

    return-wide v0
.end method

.method public getXValue()D
    .locals 2

    .prologue
    .line 27
    iget-wide v0, p0, Lorg/achartengine/model/SeriesSelection;->mXValue:D

    return-wide v0
.end method
