.class public final Lorg/achartengine/model/Point;
.super Ljava/lang/Object;
.source "Point.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private mX:F

.field private mY:F


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    return-void
.end method

.method public constructor <init>(FF)V
    .locals 0
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput p1, p0, Lorg/achartengine/model/Point;->mX:F

    .line 18
    iput p2, p0, Lorg/achartengine/model/Point;->mY:F

    .line 19
    return-void
.end method


# virtual methods
.method public getX()F
    .locals 1

    .prologue
    .line 22
    iget v0, p0, Lorg/achartengine/model/Point;->mX:F

    return v0
.end method

.method public getY()F
    .locals 1

    .prologue
    .line 26
    iget v0, p0, Lorg/achartengine/model/Point;->mY:F

    return v0
.end method

.method public setX(F)V
    .locals 0
    .param p1, "x"    # F

    .prologue
    .line 30
    iput p1, p0, Lorg/achartengine/model/Point;->mX:F

    .line 31
    return-void
.end method

.method public setY(F)V
    .locals 0
    .param p1, "y"    # F

    .prologue
    .line 34
    iput p1, p0, Lorg/achartengine/model/Point;->mY:F

    .line 35
    return-void
.end method
