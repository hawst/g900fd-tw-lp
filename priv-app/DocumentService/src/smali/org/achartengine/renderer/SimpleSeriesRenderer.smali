.class public Lorg/achartengine/renderer/SimpleSeriesRenderer;
.super Ljava/lang/Object;
.source "SimpleSeriesRenderer.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private mChartValuesFormat:Ljava/text/NumberFormat;

.field private mChartValuesSpacing:F

.field private mChartValuesTextAlign:Landroid/graphics/Paint$Align;

.field private mChartValuesTextSize:F

.field private mColor:I

.field private mDisplayBoundingPoints:Z

.field private mDisplayChartValues:Z

.field private mDisplayChartValuesDistance:I

.field private mGradientEnabled:Z

.field private mGradientStartColor:I

.field private mGradientStartValue:D

.field private mGradientStopColor:I

.field private mGradientStopValue:D

.field private mHighlighted:Z

.field private mShowLegendItem:Z

.field private mStroke:Lorg/achartengine/renderer/BasicStroke;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    const v0, -0xffff01

    iput v0, p0, Lorg/achartengine/renderer/SimpleSeriesRenderer;->mColor:I

    .line 15
    const/16 v0, 0x64

    iput v0, p0, Lorg/achartengine/renderer/SimpleSeriesRenderer;->mDisplayChartValuesDistance:I

    .line 17
    const/high16 v0, 0x41200000    # 10.0f

    iput v0, p0, Lorg/achartengine/renderer/SimpleSeriesRenderer;->mChartValuesTextSize:F

    .line 19
    sget-object v0, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    iput-object v0, p0, Lorg/achartengine/renderer/SimpleSeriesRenderer;->mChartValuesTextAlign:Landroid/graphics/Paint$Align;

    .line 21
    const/high16 v0, 0x40a00000    # 5.0f

    iput v0, p0, Lorg/achartengine/renderer/SimpleSeriesRenderer;->mChartValuesSpacing:F

    .line 23
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/achartengine/renderer/SimpleSeriesRenderer;->mGradientEnabled:Z

    .line 28
    iput-boolean v1, p0, Lorg/achartengine/renderer/SimpleSeriesRenderer;->mShowLegendItem:Z

    .line 31
    iput-boolean v1, p0, Lorg/achartengine/renderer/SimpleSeriesRenderer;->mDisplayBoundingPoints:Z

    return-void
.end method


# virtual methods
.method public getChartValuesFormat()Ljava/text/NumberFormat;
    .locals 1

    .prologue
    .line 177
    iget-object v0, p0, Lorg/achartengine/renderer/SimpleSeriesRenderer;->mChartValuesFormat:Ljava/text/NumberFormat;

    return-object v0
.end method

.method public getChartValuesSpacing()F
    .locals 1

    .prologue
    .line 85
    iget v0, p0, Lorg/achartengine/renderer/SimpleSeriesRenderer;->mChartValuesSpacing:F

    return v0
.end method

.method public getChartValuesTextAlign()Landroid/graphics/Paint$Align;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lorg/achartengine/renderer/SimpleSeriesRenderer;->mChartValuesTextAlign:Landroid/graphics/Paint$Align;

    return-object v0
.end method

.method public getChartValuesTextSize()F
    .locals 1

    .prologue
    .line 65
    iget v0, p0, Lorg/achartengine/renderer/SimpleSeriesRenderer;->mChartValuesTextSize:F

    return v0
.end method

.method public getColor()I
    .locals 1

    .prologue
    .line 35
    iget v0, p0, Lorg/achartengine/renderer/SimpleSeriesRenderer;->mColor:I

    return v0
.end method

.method public getDisplayChartValuesDistance()I
    .locals 1

    .prologue
    .line 55
    iget v0, p0, Lorg/achartengine/renderer/SimpleSeriesRenderer;->mDisplayChartValuesDistance:I

    return v0
.end method

.method public getGradientStartColor()I
    .locals 1

    .prologue
    .line 120
    iget v0, p0, Lorg/achartengine/renderer/SimpleSeriesRenderer;->mGradientStartColor:I

    return v0
.end method

.method public getGradientStartValue()D
    .locals 2

    .prologue
    .line 115
    iget-wide v0, p0, Lorg/achartengine/renderer/SimpleSeriesRenderer;->mGradientStartValue:D

    return-wide v0
.end method

.method public getGradientStopColor()I
    .locals 1

    .prologue
    .line 136
    iget v0, p0, Lorg/achartengine/renderer/SimpleSeriesRenderer;->mGradientStopColor:I

    return v0
.end method

.method public getGradientStopValue()D
    .locals 2

    .prologue
    .line 131
    iget-wide v0, p0, Lorg/achartengine/renderer/SimpleSeriesRenderer;->mGradientStopValue:D

    return-wide v0
.end method

.method public getStroke()Lorg/achartengine/renderer/BasicStroke;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lorg/achartengine/renderer/SimpleSeriesRenderer;->mStroke:Lorg/achartengine/renderer/BasicStroke;

    return-object v0
.end method

.method public isDisplayBoundingPoints()Z
    .locals 1

    .prologue
    .line 167
    iget-boolean v0, p0, Lorg/achartengine/renderer/SimpleSeriesRenderer;->mDisplayBoundingPoints:Z

    return v0
.end method

.method public isDisplayChartValues()Z
    .locals 1

    .prologue
    .line 45
    iget-boolean v0, p0, Lorg/achartengine/renderer/SimpleSeriesRenderer;->mDisplayChartValues:Z

    return v0
.end method

.method public isGradientEnabled()Z
    .locals 1

    .prologue
    .line 105
    iget-boolean v0, p0, Lorg/achartengine/renderer/SimpleSeriesRenderer;->mGradientEnabled:Z

    return v0
.end method

.method public isHighlighted()Z
    .locals 1

    .prologue
    .line 157
    iget-boolean v0, p0, Lorg/achartengine/renderer/SimpleSeriesRenderer;->mHighlighted:Z

    return v0
.end method

.method public isShowLegendItem()Z
    .locals 1

    .prologue
    .line 147
    iget-boolean v0, p0, Lorg/achartengine/renderer/SimpleSeriesRenderer;->mShowLegendItem:Z

    return v0
.end method

.method public setChartValuesFormat(Ljava/text/NumberFormat;)V
    .locals 0
    .param p1, "format"    # Ljava/text/NumberFormat;

    .prologue
    .line 182
    iput-object p1, p0, Lorg/achartengine/renderer/SimpleSeriesRenderer;->mChartValuesFormat:Ljava/text/NumberFormat;

    .line 183
    return-void
.end method

.method public setChartValuesSpacing(F)V
    .locals 0
    .param p1, "spacing"    # F

    .prologue
    .line 90
    iput p1, p0, Lorg/achartengine/renderer/SimpleSeriesRenderer;->mChartValuesSpacing:F

    .line 91
    return-void
.end method

.method public setChartValuesTextAlign(Landroid/graphics/Paint$Align;)V
    .locals 0
    .param p1, "align"    # Landroid/graphics/Paint$Align;

    .prologue
    .line 80
    iput-object p1, p0, Lorg/achartengine/renderer/SimpleSeriesRenderer;->mChartValuesTextAlign:Landroid/graphics/Paint$Align;

    .line 81
    return-void
.end method

.method public setChartValuesTextSize(F)V
    .locals 0
    .param p1, "textSize"    # F

    .prologue
    .line 70
    iput p1, p0, Lorg/achartengine/renderer/SimpleSeriesRenderer;->mChartValuesTextSize:F

    .line 71
    return-void
.end method

.method public setColor(I)V
    .locals 0
    .param p1, "color"    # I

    .prologue
    .line 40
    iput p1, p0, Lorg/achartengine/renderer/SimpleSeriesRenderer;->mColor:I

    .line 41
    return-void
.end method

.method public setDisplayBoundingPoints(Z)V
    .locals 0
    .param p1, "display"    # Z

    .prologue
    .line 172
    iput-boolean p1, p0, Lorg/achartengine/renderer/SimpleSeriesRenderer;->mDisplayBoundingPoints:Z

    .line 173
    return-void
.end method

.method public setDisplayChartValues(Z)V
    .locals 0
    .param p1, "display"    # Z

    .prologue
    .line 50
    iput-boolean p1, p0, Lorg/achartengine/renderer/SimpleSeriesRenderer;->mDisplayChartValues:Z

    .line 51
    return-void
.end method

.method public setDisplayChartValuesDistance(I)V
    .locals 0
    .param p1, "distance"    # I

    .prologue
    .line 60
    iput p1, p0, Lorg/achartengine/renderer/SimpleSeriesRenderer;->mDisplayChartValuesDistance:I

    .line 61
    return-void
.end method

.method public setGradientEnabled(Z)V
    .locals 0
    .param p1, "enabled"    # Z

    .prologue
    .line 110
    iput-boolean p1, p0, Lorg/achartengine/renderer/SimpleSeriesRenderer;->mGradientEnabled:Z

    .line 111
    return-void
.end method

.method public setGradientStart(DI)V
    .locals 1
    .param p1, "start"    # D
    .param p3, "color"    # I

    .prologue
    .line 125
    iput-wide p1, p0, Lorg/achartengine/renderer/SimpleSeriesRenderer;->mGradientStartValue:D

    .line 126
    iput p3, p0, Lorg/achartengine/renderer/SimpleSeriesRenderer;->mGradientStartColor:I

    .line 127
    return-void
.end method

.method public setGradientStop(DI)V
    .locals 1
    .param p1, "start"    # D
    .param p3, "color"    # I

    .prologue
    .line 141
    iput-wide p1, p0, Lorg/achartengine/renderer/SimpleSeriesRenderer;->mGradientStopValue:D

    .line 142
    iput p3, p0, Lorg/achartengine/renderer/SimpleSeriesRenderer;->mGradientStopColor:I

    .line 143
    return-void
.end method

.method public setHighlighted(Z)V
    .locals 0
    .param p1, "highlighted"    # Z

    .prologue
    .line 162
    iput-boolean p1, p0, Lorg/achartengine/renderer/SimpleSeriesRenderer;->mHighlighted:Z

    .line 163
    return-void
.end method

.method public setShowLegendItem(Z)V
    .locals 0
    .param p1, "showLegend"    # Z

    .prologue
    .line 152
    iput-boolean p1, p0, Lorg/achartengine/renderer/SimpleSeriesRenderer;->mShowLegendItem:Z

    .line 153
    return-void
.end method

.method public setStroke(Lorg/achartengine/renderer/BasicStroke;)V
    .locals 0
    .param p1, "stroke"    # Lorg/achartengine/renderer/BasicStroke;

    .prologue
    .line 100
    iput-object p1, p0, Lorg/achartengine/renderer/SimpleSeriesRenderer;->mStroke:Lorg/achartengine/renderer/BasicStroke;

    .line 101
    return-void
.end method
