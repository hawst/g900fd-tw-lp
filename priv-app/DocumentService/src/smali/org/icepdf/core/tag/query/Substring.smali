.class public Lorg/icepdf/core/tag/query/Substring;
.super Lorg/icepdf/core/tag/query/Function;
.source "Substring.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Lorg/icepdf/core/tag/query/Function;-><init>()V

    return-void
.end method


# virtual methods
.method public getArgumentCount()I
    .locals 1

    .prologue
    .line 26
    const/4 v0, 0x1

    return v0
.end method

.method public matches(Lorg/icepdf/core/tag/TaggedDocument;Lorg/icepdf/core/tag/TaggedImage;Ljava/lang/String;)Z
    .locals 2
    .param p1, "td"    # Lorg/icepdf/core/tag/TaggedDocument;
    .param p2, "ti"    # Lorg/icepdf/core/tag/TaggedImage;
    .param p3, "tag"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 30
    iget-object v1, p0, Lorg/icepdf/core/tag/query/Substring;->arguments:[Ljava/lang/String;

    aget-object v1, v1, v0

    invoke-virtual {p3, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    if-ltz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method
