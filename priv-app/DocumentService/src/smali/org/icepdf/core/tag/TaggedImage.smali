.class public Lorg/icepdf/core/tag/TaggedImage;
.super Ljava/lang/Object;
.source "TaggedImage.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private inlineImage:Z

.field private pages:Ljava/util/List;

.field private ref:Lorg/icepdf/core/pobjects/Reference;

.field private tags:Ljava/util/List;


# direct methods
.method constructor <init>(Lorg/icepdf/core/pobjects/Reference;Z)V
    .locals 2
    .param p1, "ref"    # Lorg/icepdf/core/pobjects/Reference;
    .param p2, "inlineImage"    # Z

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object p1, p0, Lorg/icepdf/core/tag/TaggedImage;->ref:Lorg/icepdf/core/pobjects/Reference;

    .line 36
    iput-boolean p2, p0, Lorg/icepdf/core/tag/TaggedImage;->inlineImage:Z

    .line 37
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lorg/icepdf/core/tag/TaggedImage;->pages:Ljava/util/List;

    .line 38
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x6

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lorg/icepdf/core/tag/TaggedImage;->tags:Ljava/util/List;

    .line 39
    return-void
.end method


# virtual methods
.method addPage(I)V
    .locals 6
    .param p1, "pageIndex"    # I

    .prologue
    .line 91
    iget-object v3, p0, Lorg/icepdf/core/tag/TaggedImage;->pages:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v2

    .line 92
    .local v2, "size":I
    if-nez v2, :cond_1

    .line 93
    iget-object v3, p0, Lorg/icepdf/core/tag/TaggedImage;->pages:Ljava/util/List;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 120
    :cond_0
    :goto_0
    return-void

    .line 96
    :cond_1
    rem-int/lit8 v3, v2, 0x2

    if-nez v3, :cond_3

    .line 97
    iget-object v3, p0, Lorg/icepdf/core/tag/TaggedImage;->pages:Ljava/util/List;

    add-int/lit8 v4, v2, -0x1

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 99
    .local v1, "end":Ljava/lang/Integer;
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v3

    add-int/lit8 v4, p1, -0x1

    if-ne v3, v4, :cond_2

    .line 100
    iget-object v3, p0, Lorg/icepdf/core/tag/TaggedImage;->pages:Ljava/util/List;

    add-int/lit8 v4, v2, -0x1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 103
    :cond_2
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v3

    add-int/lit8 v4, p1, -0x1

    if-ge v3, v4, :cond_0

    .line 104
    iget-object v3, p0, Lorg/icepdf/core/tag/TaggedImage;->pages:Ljava/util/List;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 109
    .end local v1    # "end":Ljava/lang/Integer;
    :cond_3
    iget-object v3, p0, Lorg/icepdf/core/tag/TaggedImage;->pages:Ljava/util/List;

    add-int/lit8 v4, v2, -0x1

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 111
    .local v0, "begin":Ljava/lang/Integer;
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    add-int/lit8 v4, p1, -0x1

    if-ne v3, v4, :cond_4

    .line 112
    iget-object v3, p0, Lorg/icepdf/core/tag/TaggedImage;->pages:Ljava/util/List;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 115
    :cond_4
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    add-int/lit8 v4, p1, -0x1

    if-ge v3, v4, :cond_0

    .line 116
    iget-object v3, p0, Lorg/icepdf/core/tag/TaggedImage;->pages:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 117
    iget-object v3, p0, Lorg/icepdf/core/tag/TaggedImage;->pages:Ljava/util/List;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method describe()Ljava/lang/String;
    .locals 4

    .prologue
    .line 129
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v3, 0x1000

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 130
    .local v0, "sb":Ljava/lang/StringBuilder;
    iget-object v3, p0, Lorg/icepdf/core/tag/TaggedImage;->tags:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "tgs":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 131
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 132
    .local v1, "t":Ljava/lang/String;
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 133
    const-string/jumbo v3, "\n"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 135
    .end local v1    # "t":Ljava/lang/String;
    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method public describePages()Ljava/lang/String;
    .locals 9

    .prologue
    const/4 v5, 0x1

    .line 62
    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v6, 0x20

    invoke-direct {v3, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 63
    .local v3, "sb":Ljava/lang/StringBuilder;
    iget-object v6, p0, Lorg/icepdf/core/tag/TaggedImage;->pages:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v4

    .line 64
    .local v4, "sz":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v4, :cond_4

    .line 65
    rem-int/lit8 v6, v1, 0x2

    if-ne v6, v5, :cond_2

    move v2, v5

    .line 66
    .local v2, "oddIndex":Z
    :goto_1
    const/4 v0, 0x0

    .line 67
    .local v0, "closingRangeForSinglePage":Z
    if-eqz v2, :cond_3

    .line 68
    iget-object v6, p0, Lorg/icepdf/core/tag/TaggedImage;->pages:Ljava/util/List;

    invoke-interface {v6, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    iget-object v7, p0, Lorg/icepdf/core/tag/TaggedImage;->pages:Ljava/util/List;

    add-int/lit8 v8, v1, -0x1

    invoke-interface {v7, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 69
    if-nez v0, :cond_0

    .line 70
    const/16 v6, 0x2d

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 76
    :cond_0
    :goto_2
    if-nez v0, :cond_1

    .line 77
    iget-object v6, p0, Lorg/icepdf/core/tag/TaggedImage;->pages:Ljava/util/List;

    invoke-interface {v6, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 64
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 65
    .end local v0    # "closingRangeForSinglePage":Z
    .end local v2    # "oddIndex":Z
    :cond_2
    const/4 v2, 0x0

    goto :goto_1

    .line 73
    .restart local v0    # "closingRangeForSinglePage":Z
    .restart local v2    # "oddIndex":Z
    :cond_3
    if-lez v1, :cond_0

    .line 74
    const-string/jumbo v6, ", "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 80
    .end local v0    # "closingRangeForSinglePage":Z
    .end local v2    # "oddIndex":Z
    :cond_4
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    return-object v5
.end method

.method public getPages()Ljava/util/List;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lorg/icepdf/core/tag/TaggedImage;->pages:Ljava/util/List;

    return-object v0
.end method

.method public getReference()Lorg/icepdf/core/pobjects/Reference;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lorg/icepdf/core/tag/TaggedImage;->ref:Lorg/icepdf/core/pobjects/Reference;

    return-object v0
.end method

.method public getTags()Ljava/util/List;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lorg/icepdf/core/tag/TaggedImage;->tags:Ljava/util/List;

    return-object v0
.end method

.method public isInlineImage()Z
    .locals 1

    .prologue
    .line 46
    iget-boolean v0, p0, Lorg/icepdf/core/tag/TaggedImage;->inlineImage:Z

    return v0
.end method

.method tag(Ljava/lang/String;)V
    .locals 1
    .param p1, "tag"    # Ljava/lang/String;

    .prologue
    .line 123
    iget-object v0, p0, Lorg/icepdf/core/tag/TaggedImage;->tags:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 124
    iget-object v0, p0, Lorg/icepdf/core/tag/TaggedImage;->tags:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 126
    :cond_0
    return-void
.end method
