.class public Lorg/icepdf/core/tag/Tagger;
.super Ljava/lang/Object;
.source "Tagger.java"


# static fields
.field private static state:Lorg/icepdf/core/tag/TagState;

.field public static final tagging:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const-string/jumbo v0, "ice.tag.tagging"

    invoke-static {v0}, Lorg/icepdf/core/tag/Tagger;->property(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lorg/icepdf/core/tag/Tagger;->tagging:Z

    .line 40
    new-instance v0, Lorg/icepdf/core/tag/TagState;

    invoke-direct {v0}, Lorg/icepdf/core/tag/TagState;-><init>()V

    sput-object v0, Lorg/icepdf/core/tag/Tagger;->state:Lorg/icepdf/core/tag/TagState;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static beginImage(Lorg/icepdf/core/pobjects/Reference;Z)V
    .locals 1
    .param p0, "ref"    # Lorg/icepdf/core/pobjects/Reference;
    .param p1, "inlineImage"    # Z

    .prologue
    .line 53
    sget-object v0, Lorg/icepdf/core/tag/Tagger;->state:Lorg/icepdf/core/tag/TagState;

    invoke-virtual {v0, p0, p1}, Lorg/icepdf/core/tag/TagState;->beginImage(Lorg/icepdf/core/pobjects/Reference;Z)V

    .line 55
    return-void
.end method

.method public static describe()Ljava/lang/String;
    .locals 1

    .prologue
    .line 68
    sget-object v0, Lorg/icepdf/core/tag/Tagger;->state:Lorg/icepdf/core/tag/TagState;

    invoke-virtual {v0}, Lorg/icepdf/core/tag/TagState;->describe()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static endImage(Lorg/icepdf/core/pobjects/Reference;)V
    .locals 1
    .param p0, "ref"    # Lorg/icepdf/core/pobjects/Reference;

    .prologue
    .line 58
    sget-object v0, Lorg/icepdf/core/tag/Tagger;->state:Lorg/icepdf/core/tag/TagState;

    invoke-virtual {v0, p0}, Lorg/icepdf/core/tag/TagState;->endImage(Lorg/icepdf/core/pobjects/Reference;)V

    .line 60
    return-void
.end method

.method public static getTagState()Lorg/icepdf/core/tag/TagState;
    .locals 1

    .prologue
    .line 72
    sget-object v0, Lorg/icepdf/core/tag/Tagger;->state:Lorg/icepdf/core/tag/TagState;

    return-object v0
.end method

.method private static property(Ljava/lang/String;)Z
    .locals 3
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 30
    const/4 v0, 0x0

    .line 32
    .local v0, "value":Ljava/lang/String;
    :try_start_0
    invoke-static {p0}, Lorg/icepdf/core/util/Defs;->sysProperty(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 34
    :goto_0
    if-eqz v0, :cond_1

    .line 35
    const-string/jumbo v2, "yes"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string/jumbo v2, "true"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    const/4 v1, 0x1

    .line 37
    :cond_1
    return v1

    .line 33
    :catch_0
    move-exception v2

    goto :goto_0
.end method

.method public static setCurrentDocument(Lorg/icepdf/core/pobjects/Document;)V
    .locals 1
    .param p0, "doc"    # Lorg/icepdf/core/pobjects/Document;

    .prologue
    .line 43
    sget-object v0, Lorg/icepdf/core/tag/Tagger;->state:Lorg/icepdf/core/tag/TagState;

    invoke-virtual {v0, p0}, Lorg/icepdf/core/tag/TagState;->setCurrentDocument(Lorg/icepdf/core/pobjects/Document;)V

    .line 45
    return-void
.end method

.method public static setCurrentPageIndex(I)V
    .locals 1
    .param p0, "currentPageIndex"    # I

    .prologue
    .line 48
    sget-object v0, Lorg/icepdf/core/tag/Tagger;->state:Lorg/icepdf/core/tag/TagState;

    invoke-virtual {v0, p0}, Lorg/icepdf/core/tag/TagState;->setCurrentPageIndex(I)V

    .line 50
    return-void
.end method

.method public static tagImage(Ljava/lang/String;)V
    .locals 1
    .param p0, "tag"    # Ljava/lang/String;

    .prologue
    .line 63
    sget-object v0, Lorg/icepdf/core/tag/Tagger;->state:Lorg/icepdf/core/tag/TagState;

    invoke-virtual {v0, p0}, Lorg/icepdf/core/tag/TagState;->tagImage(Ljava/lang/String;)V

    .line 65
    return-void
.end method
