.class public abstract Lorg/icepdf/core/tag/query/Function;
.super Ljava/lang/Object;
.source "Function.java"

# interfaces
.implements Lorg/icepdf/core/tag/query/Expression;


# instance fields
.field protected arguments:[Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public describe(I)Ljava/lang/String;
    .locals 6
    .param p1, "indent"    # I

    .prologue
    const/16 v5, 0x27

    .line 29
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 30
    .local v3, "sb":Ljava/lang/StringBuilder;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, p1, :cond_0

    .line 31
    const-string/jumbo v4, "  "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 30
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 32
    :cond_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    .line 33
    .local v0, "className":Ljava/lang/String;
    const-string/jumbo v4, "."

    invoke-virtual {v0, v4}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    invoke-virtual {v0, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 34
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 35
    const-string/jumbo v4, " ( "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 36
    iget-object v4, p0, Lorg/icepdf/core/tag/query/Function;->arguments:[Ljava/lang/String;

    if-eqz v4, :cond_2

    iget-object v4, p0, Lorg/icepdf/core/tag/query/Function;->arguments:[Ljava/lang/String;

    array-length v2, v4

    .line 37
    .local v2, "num":I
    :goto_1
    const/4 v1, 0x0

    :goto_2
    if-ge v1, v2, :cond_3

    .line 38
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 39
    iget-object v4, p0, Lorg/icepdf/core/tag/query/Function;->arguments:[Ljava/lang/String;

    aget-object v4, v4, v1

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 40
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 41
    add-int/lit8 v4, v2, -0x1

    if-ge v1, v4, :cond_1

    .line 42
    const-string/jumbo v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 37
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 36
    .end local v2    # "num":I
    :cond_2
    const/4 v2, 0x0

    goto :goto_1

    .line 44
    .restart local v2    # "num":I
    :cond_3
    const-string/jumbo v4, " )\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 45
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4
.end method

.method public setArguments([Ljava/lang/String;)V
    .locals 0
    .param p1, "arguments"    # [Ljava/lang/String;

    .prologue
    .line 25
    iput-object p1, p0, Lorg/icepdf/core/tag/query/Function;->arguments:[Ljava/lang/String;

    .line 26
    return-void
.end method
