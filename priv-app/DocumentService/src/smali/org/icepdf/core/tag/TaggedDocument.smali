.class public Lorg/icepdf/core/tag/TaggedDocument;
.super Ljava/lang/Object;
.source "TaggedDocument.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private allImages:Ljava/util/List;

.field private transient currentImages:Ljava/util/List;

.field private currentPageIndex:I

.field private origin:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;)V
    .locals 2
    .param p1, "origin"    # Ljava/lang/String;

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object p1, p0, Lorg/icepdf/core/tag/TaggedDocument;->origin:Ljava/lang/String;

    .line 36
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0x20

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lorg/icepdf/core/tag/TaggedDocument;->allImages:Ljava/util/List;

    .line 37
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lorg/icepdf/core/tag/TaggedDocument;->currentImages:Ljava/util/List;

    .line 38
    return-void
.end method

.method private findImage(Lorg/icepdf/core/pobjects/Reference;)Lorg/icepdf/core/tag/TaggedImage;
    .locals 4
    .param p1, "ref"    # Lorg/icepdf/core/pobjects/Reference;

    .prologue
    const/4 v2, 0x0

    .line 63
    if-nez p1, :cond_1

    move-object v1, v2

    .line 70
    :cond_0
    :goto_0
    return-object v1

    .line 65
    :cond_1
    iget-object v3, p0, Lorg/icepdf/core/tag/TaggedDocument;->allImages:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v0, v3, -0x1

    .local v0, "i":I
    :goto_1
    if-ltz v0, :cond_3

    .line 66
    iget-object v3, p0, Lorg/icepdf/core/tag/TaggedDocument;->allImages:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/icepdf/core/tag/TaggedImage;

    .line 67
    .local v1, "ti":Lorg/icepdf/core/tag/TaggedImage;
    invoke-virtual {v1}, Lorg/icepdf/core/tag/TaggedImage;->getReference()Lorg/icepdf/core/pobjects/Reference;

    move-result-object v3

    if-eqz v3, :cond_2

    invoke-virtual {v1}, Lorg/icepdf/core/tag/TaggedImage;->getReference()Lorg/icepdf/core/pobjects/Reference;

    move-result-object v3

    invoke-virtual {v3, p1}, Lorg/icepdf/core/pobjects/Reference;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 65
    :cond_2
    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    .end local v1    # "ti":Lorg/icepdf/core/tag/TaggedImage;
    :cond_3
    move-object v1, v2

    .line 70
    goto :goto_0
.end method


# virtual methods
.method beginImage(Lorg/icepdf/core/pobjects/Reference;Z)V
    .locals 2
    .param p1, "ref"    # Lorg/icepdf/core/pobjects/Reference;
    .param p2, "inlineImage"    # Z

    .prologue
    .line 53
    invoke-direct {p0, p1}, Lorg/icepdf/core/tag/TaggedDocument;->findImage(Lorg/icepdf/core/pobjects/Reference;)Lorg/icepdf/core/tag/TaggedImage;

    move-result-object v0

    .line 54
    .local v0, "ti":Lorg/icepdf/core/tag/TaggedImage;
    if-nez v0, :cond_0

    .line 55
    new-instance v0, Lorg/icepdf/core/tag/TaggedImage;

    .end local v0    # "ti":Lorg/icepdf/core/tag/TaggedImage;
    invoke-direct {v0, p1, p2}, Lorg/icepdf/core/tag/TaggedImage;-><init>(Lorg/icepdf/core/pobjects/Reference;Z)V

    .line 56
    .restart local v0    # "ti":Lorg/icepdf/core/tag/TaggedImage;
    iget-object v1, p0, Lorg/icepdf/core/tag/TaggedDocument;->allImages:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 58
    :cond_0
    iget-object v1, p0, Lorg/icepdf/core/tag/TaggedDocument;->currentImages:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 59
    iget v1, p0, Lorg/icepdf/core/tag/TaggedDocument;->currentPageIndex:I

    invoke-virtual {v0, v1}, Lorg/icepdf/core/tag/TaggedImage;->addPage(I)V

    .line 60
    return-void
.end method

.method describe()Ljava/lang/String;
    .locals 4

    .prologue
    .line 83
    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v3, 0x1000

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 84
    .local v1, "sb":Ljava/lang/StringBuilder;
    const-string/jumbo v3, "ORIGIN: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 85
    iget-object v3, p0, Lorg/icepdf/core/tag/TaggedDocument;->origin:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 86
    const-string/jumbo v3, "\n"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 87
    iget-object v3, p0, Lorg/icepdf/core/tag/TaggedDocument;->allImages:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "imgs":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 88
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/icepdf/core/tag/TaggedImage;

    .line 89
    .local v2, "ti":Lorg/icepdf/core/tag/TaggedImage;
    invoke-virtual {v2}, Lorg/icepdf/core/tag/TaggedImage;->describe()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 90
    const-string/jumbo v3, "---------------------------------\n"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 92
    .end local v2    # "ti":Lorg/icepdf/core/tag/TaggedImage;
    :cond_0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method endImage(Lorg/icepdf/core/pobjects/Reference;)V
    .locals 2
    .param p1, "ref"    # Lorg/icepdf/core/pobjects/Reference;

    .prologue
    .line 74
    iget-object v0, p0, Lorg/icepdf/core/tag/TaggedDocument;->currentImages:Ljava/util/List;

    iget-object v1, p0, Lorg/icepdf/core/tag/TaggedDocument;->currentImages:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 75
    return-void
.end method

.method public getImages()Ljava/util/List;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lorg/icepdf/core/tag/TaggedDocument;->allImages:Ljava/util/List;

    return-object v0
.end method

.method public getOrigin()Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lorg/icepdf/core/tag/TaggedDocument;->origin:Ljava/lang/String;

    return-object v0
.end method

.method setCurrentPageIndex(I)V
    .locals 0
    .param p1, "currentPageIndex"    # I

    .prologue
    .line 49
    iput p1, p0, Lorg/icepdf/core/tag/TaggedDocument;->currentPageIndex:I

    .line 50
    return-void
.end method

.method tagImage(Ljava/lang/String;)V
    .locals 3
    .param p1, "tag"    # Ljava/lang/String;

    .prologue
    .line 78
    iget-object v1, p0, Lorg/icepdf/core/tag/TaggedDocument;->currentImages:Ljava/util/List;

    iget-object v2, p0, Lorg/icepdf/core/tag/TaggedDocument;->currentImages:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/icepdf/core/tag/TaggedImage;

    .line 79
    .local v0, "ti":Lorg/icepdf/core/tag/TaggedImage;
    invoke-virtual {v0, p1}, Lorg/icepdf/core/tag/TaggedImage;->tag(Ljava/lang/String;)V

    .line 80
    return-void
.end method
