.class public Lorg/icepdf/core/tag/TagUtil;
.super Ljava/lang/Object;
.source "TagUtil.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/icepdf/core/tag/TagUtil$DuplicateEntry;,
        Lorg/icepdf/core/tag/TagUtil$FileHash;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 683
    return-void
.end method

.method private static addFileIfIsPDF(Ljava/util/List;Ljava/io/File;)V
    .locals 2
    .param p1, "file"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/io/File;",
            ">;",
            "Ljava/io/File;",
            ")V"
        }
    .end annotation

    .prologue
    .line 614
    .local p0, "list":Ljava/util/List;, "Ljava/util/List<Ljava/io/File;>;"
    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, ".pdf"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 615
    invoke-interface {p0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 616
    :cond_0
    return-void
.end method

.method public static catalog(Ljava/lang/String;Ljava/lang/String;)V
    .locals 12
    .param p0, "contentRoot"    # Ljava/lang/String;
    .param p1, "catalogFile"    # Ljava/lang/String;

    .prologue
    .line 98
    const/4 v6, 0x0

    .line 99
    .local v6, "oos":Ljava/io/ObjectOutputStream;
    const/4 v4, 0x0

    .line 101
    .local v4, "fos":Ljava/io/FileOutputStream;
    :try_start_0
    new-instance v5, Ljava/io/FileOutputStream;

    invoke-direct {v5, p1}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 102
    .end local v4    # "fos":Ljava/io/FileOutputStream;
    .local v5, "fos":Ljava/io/FileOutputStream;
    :try_start_1
    new-instance v1, Ljava/io/BufferedOutputStream;

    const/16 v8, 0x4000

    invoke-direct {v1, v5, v8}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;I)V

    .line 103
    .local v1, "bos":Ljava/io/BufferedOutputStream;
    new-instance v7, Ljava/io/ObjectOutputStream;

    invoke-direct {v7, v1}, Ljava/io/ObjectOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_5
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 110
    .end local v6    # "oos":Ljava/io/ObjectOutputStream;
    .local v7, "oos":Ljava/io/ObjectOutputStream;
    if-eqz v5, :cond_0

    .line 111
    :try_start_2
    invoke-virtual {v5}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 117
    :cond_0
    :goto_0
    new-instance v0, Ljava/util/ArrayList;

    const/16 v8, 0x400

    invoke-direct {v0, v8}, Ljava/util/ArrayList;-><init>(I)V

    .line 120
    .local v0, "allFiles":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/io/File;>;"
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 121
    .local v2, "contentRootFile":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->isDirectory()Z

    move-result v8

    if-eqz v8, :cond_4

    .line 123
    invoke-static {v0, v2}, Lorg/icepdf/core/tag/TagUtil;->recursivelyCatalogPDFs(Ljava/util/ArrayList;Ljava/io/File;)V

    .line 128
    :cond_1
    :goto_1
    invoke-virtual {v0}, Ljava/util/ArrayList;->trimToSize()V

    .line 132
    :try_start_3
    invoke-virtual {v7, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 133
    invoke-virtual {v7}, Ljava/io/ObjectOutputStream;->flush()V

    .line 134
    invoke-virtual {v7}, Ljava/io/ObjectOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_4

    :goto_2
    move-object v4, v5

    .end local v5    # "fos":Ljava/io/FileOutputStream;
    .restart local v4    # "fos":Ljava/io/FileOutputStream;
    move-object v6, v7

    .line 138
    .end local v0    # "allFiles":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/io/File;>;"
    .end local v1    # "bos":Ljava/io/BufferedOutputStream;
    .end local v2    # "contentRootFile":Ljava/io/File;
    .end local v7    # "oos":Ljava/io/ObjectOutputStream;
    .restart local v6    # "oos":Ljava/io/ObjectOutputStream;
    :cond_2
    :goto_3
    return-void

    .line 112
    .end local v4    # "fos":Ljava/io/FileOutputStream;
    .end local v6    # "oos":Ljava/io/ObjectOutputStream;
    .restart local v1    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v5    # "fos":Ljava/io/FileOutputStream;
    .restart local v7    # "oos":Ljava/io/ObjectOutputStream;
    :catch_0
    move-exception v3

    .line 113
    .local v3, "e":Ljava/io/IOException;
    const-string/jumbo v8, "TagUtil"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "IOException :"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v3}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 104
    .end local v1    # "bos":Ljava/io/BufferedOutputStream;
    .end local v3    # "e":Ljava/io/IOException;
    .end local v5    # "fos":Ljava/io/FileOutputStream;
    .end local v7    # "oos":Ljava/io/ObjectOutputStream;
    .restart local v4    # "fos":Ljava/io/FileOutputStream;
    .restart local v6    # "oos":Ljava/io/ObjectOutputStream;
    :catch_1
    move-exception v3

    .line 106
    .restart local v3    # "e":Ljava/io/IOException;
    :goto_4
    :try_start_4
    const-string/jumbo v8, "TagUtil"

    invoke-virtual {v3}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 110
    if-eqz v4, :cond_2

    .line 111
    :try_start_5
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    goto :goto_3

    .line 112
    :catch_2
    move-exception v3

    .line 113
    const-string/jumbo v8, "TagUtil"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "IOException :"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v3}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 109
    .end local v3    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v8

    .line 110
    :goto_5
    if-eqz v4, :cond_3

    .line 111
    :try_start_6
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    .line 114
    :cond_3
    :goto_6
    throw v8

    .line 112
    :catch_3
    move-exception v3

    .line 113
    .restart local v3    # "e":Ljava/io/IOException;
    const-string/jumbo v9, "TagUtil"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "IOException :"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v3}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_6

    .line 124
    .end local v3    # "e":Ljava/io/IOException;
    .end local v4    # "fos":Ljava/io/FileOutputStream;
    .end local v6    # "oos":Ljava/io/ObjectOutputStream;
    .restart local v0    # "allFiles":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/io/File;>;"
    .restart local v1    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v2    # "contentRootFile":Ljava/io/File;
    .restart local v5    # "fos":Ljava/io/FileOutputStream;
    .restart local v7    # "oos":Ljava/io/ObjectOutputStream;
    :cond_4
    invoke-virtual {v2}, Ljava/io/File;->isFile()Z

    move-result v8

    if-eqz v8, :cond_1

    .line 126
    invoke-static {v0, v2}, Lorg/icepdf/core/tag/TagUtil;->addFileIfIsPDF(Ljava/util/List;Ljava/io/File;)V

    goto/16 :goto_1

    .line 135
    :catch_4
    move-exception v3

    .line 136
    .restart local v3    # "e":Ljava/io/IOException;
    sget-object v8, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "Problem saving catalog of PDF files: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 109
    .end local v0    # "allFiles":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/io/File;>;"
    .end local v1    # "bos":Ljava/io/BufferedOutputStream;
    .end local v2    # "contentRootFile":Ljava/io/File;
    .end local v3    # "e":Ljava/io/IOException;
    .end local v7    # "oos":Ljava/io/ObjectOutputStream;
    .restart local v6    # "oos":Ljava/io/ObjectOutputStream;
    :catchall_1
    move-exception v8

    move-object v4, v5

    .end local v5    # "fos":Ljava/io/FileOutputStream;
    .restart local v4    # "fos":Ljava/io/FileOutputStream;
    goto :goto_5

    .line 104
    .end local v4    # "fos":Ljava/io/FileOutputStream;
    .restart local v5    # "fos":Ljava/io/FileOutputStream;
    :catch_5
    move-exception v3

    move-object v4, v5

    .end local v5    # "fos":Ljava/io/FileOutputStream;
    .restart local v4    # "fos":Ljava/io/FileOutputStream;
    goto/16 :goto_4
.end method

.method public static main([Ljava/lang/String;)V
    .locals 8
    .param p0, "args"    # [Ljava/lang/String;

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 50
    sget-boolean v2, Lorg/icepdf/core/tag/Tagger;->tagging:Z

    if-nez v2, :cond_1

    .line 91
    :cond_0
    :goto_0
    return-void

    .line 55
    :cond_1
    array-length v2, p0

    if-lt v2, v4, :cond_2

    aget-object v2, p0, v5

    const-string/jumbo v3, "-catalog"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 56
    array-length v2, p0

    if-lt v2, v7, :cond_0

    .line 57
    aget-object v2, p0, v4

    aget-object v3, p0, v6

    invoke-static {v2, v3}, Lorg/icepdf/core/tag/TagUtil;->catalog(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 63
    :cond_2
    array-length v2, p0

    if-lt v2, v4, :cond_3

    aget-object v2, p0, v5

    const-string/jumbo v3, "-prune"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 64
    array-length v2, p0

    if-lt v2, v7, :cond_0

    .line 65
    aget-object v2, p0, v4

    aget-object v3, p0, v6

    invoke-static {v2, v3}, Lorg/icepdf/core/tag/TagUtil;->prune(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 71
    :cond_3
    array-length v2, p0

    if-lt v2, v4, :cond_4

    aget-object v2, p0, v5

    const-string/jumbo v3, "-tag"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 72
    array-length v2, p0

    if-lt v2, v7, :cond_0

    .line 73
    aget-object v2, p0, v4

    aget-object v3, p0, v6

    invoke-static {v2, v3}, Lorg/icepdf/core/tag/TagUtil;->tag(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 79
    :cond_4
    array-length v2, p0

    if-lt v2, v4, :cond_0

    aget-object v2, p0, v5

    const-string/jumbo v3, "-query"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 80
    array-length v2, p0

    if-lt v2, v7, :cond_0

    .line 81
    aget-object v1, p0, v4

    .line 82
    .local v1, "tagFile":Ljava/lang/String;
    array-length v2, p0

    add-int/lit8 v2, v2, -0x2

    new-array v0, v2, [Ljava/lang/String;

    .line 83
    .local v0, "rpnQuery":[Ljava/lang/String;
    array-length v2, p0

    add-int/lit8 v2, v2, -0x2

    invoke-static {p0, v6, v0, v5, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 84
    invoke-static {v1, v0}, Lorg/icepdf/core/tag/TagUtil;->query(Ljava/lang/String;[Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static prune(Ljava/lang/String;Ljava/lang/String;)V
    .locals 42
    .param p0, "oldCatalogFile"    # Ljava/lang/String;
    .param p1, "newCatalogFile"    # Ljava/lang/String;

    .prologue
    .line 141
    const/4 v11, 0x0

    .line 143
    .local v11, "digest":Ljava/security/MessageDigest;
    :try_start_0
    const-string/jumbo v38, "SHA-256"

    invoke-static/range {v38 .. v38}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v11

    .line 151
    const/16 v24, 0x0

    .line 152
    .local v24, "newOutput":Ljava/io/ObjectOutputStream;
    const/16 v19, 0x0

    .line 153
    .local v19, "fos":Ljava/io/FileOutputStream;
    const/4 v7, 0x0

    .line 155
    .local v7, "bos":Ljava/io/BufferedOutputStream;
    :try_start_1
    new-instance v20, Ljava/io/FileOutputStream;

    move-object/from16 v0, v20

    move-object/from16 v1, p1

    invoke-direct {v0, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 156
    .end local v19    # "fos":Ljava/io/FileOutputStream;
    .local v20, "fos":Ljava/io/FileOutputStream;
    :try_start_2
    new-instance v8, Ljava/io/BufferedOutputStream;

    const/16 v38, 0x4000

    move-object/from16 v0, v20

    move/from16 v1, v38

    invoke-direct {v8, v0, v1}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;I)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_25
    .catchall {:try_start_2 .. :try_end_2} :catchall_7

    .line 157
    .end local v7    # "bos":Ljava/io/BufferedOutputStream;
    .local v8, "bos":Ljava/io/BufferedOutputStream;
    :try_start_3
    new-instance v25, Ljava/io/ObjectOutputStream;

    move-object/from16 v0, v25

    invoke-direct {v0, v8}, Ljava/io/ObjectOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_26
    .catchall {:try_start_3 .. :try_end_3} :catchall_8

    .line 165
    .end local v24    # "newOutput":Ljava/io/ObjectOutputStream;
    .local v25, "newOutput":Ljava/io/ObjectOutputStream;
    if-eqz v25, :cond_0

    .line 166
    :try_start_4
    invoke-virtual/range {v25 .. v25}, Ljava/io/ObjectOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    .line 172
    :cond_0
    :goto_0
    if-eqz v20, :cond_1

    .line 173
    :try_start_5
    invoke-virtual/range {v20 .. v20}, Ljava/io/FileOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_14

    .line 180
    :cond_1
    :goto_1
    if-eqz v8, :cond_2

    .line 181
    :try_start_6
    invoke-virtual {v8}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2

    :cond_2
    move-object v7, v8

    .end local v8    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v7    # "bos":Ljava/io/BufferedOutputStream;
    move-object/from16 v19, v20

    .end local v20    # "fos":Ljava/io/FileOutputStream;
    .restart local v19    # "fos":Ljava/io/FileOutputStream;
    move-object/from16 v24, v25

    .line 189
    .end local v25    # "newOutput":Ljava/io/ObjectOutputStream;
    .restart local v24    # "newOutput":Ljava/io/ObjectOutputStream;
    :cond_3
    :goto_2
    const/16 v17, 0x0

    .line 190
    .local v17, "fis":Ljava/io/FileInputStream;
    const/4 v5, 0x0

    .line 191
    .local v5, "bis":Ljava/io/BufferedInputStream;
    const/16 v30, 0x0

    .line 192
    .local v30, "ois":Ljava/io/ObjectInputStream;
    const/4 v4, 0x0

    .line 194
    .local v4, "allFiles":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/io/File;>;"
    :try_start_7
    new-instance v18, Ljava/io/FileInputStream;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_8
    .catch Ljava/lang/ClassNotFoundException; {:try_start_7 .. :try_end_7} :catch_b
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 195
    .end local v17    # "fis":Ljava/io/FileInputStream;
    .local v18, "fis":Ljava/io/FileInputStream;
    :try_start_8
    new-instance v6, Ljava/io/BufferedInputStream;

    const/16 v38, 0x4000

    move-object/from16 v0, v18

    move/from16 v1, v38

    invoke-direct {v6, v0, v1}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;I)V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_22
    .catch Ljava/lang/ClassNotFoundException; {:try_start_8 .. :try_end_8} :catch_1f
    .catchall {:try_start_8 .. :try_end_8} :catchall_4

    .line 196
    .end local v5    # "bis":Ljava/io/BufferedInputStream;
    .local v6, "bis":Ljava/io/BufferedInputStream;
    :try_start_9
    new-instance v31, Ljava/io/ObjectInputStream;

    move-object/from16 v0, v31

    invoke-direct {v0, v6}, Ljava/io/ObjectInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_23
    .catch Ljava/lang/ClassNotFoundException; {:try_start_9 .. :try_end_9} :catch_20
    .catchall {:try_start_9 .. :try_end_9} :catchall_5

    .line 197
    .end local v30    # "ois":Ljava/io/ObjectInputStream;
    .local v31, "ois":Ljava/io/ObjectInputStream;
    :try_start_a
    invoke-virtual/range {v31 .. v31}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v4

    .end local v4    # "allFiles":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/io/File;>;"
    check-cast v4, Ljava/util/ArrayList;
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_24
    .catch Ljava/lang/ClassNotFoundException; {:try_start_a .. :try_end_a} :catch_21
    .catchall {:try_start_a .. :try_end_a} :catchall_6

    .line 211
    .restart local v4    # "allFiles":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/io/File;>;"
    if-eqz v18, :cond_4

    .line 212
    :try_start_b
    invoke-virtual/range {v18 .. v18}, Ljava/io/FileInputStream;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_7

    .line 218
    :cond_4
    :goto_3
    if-eqz v6, :cond_5

    .line 219
    :try_start_c
    invoke-virtual {v6}, Ljava/io/BufferedInputStream;->close()V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_18

    .line 227
    :cond_5
    :goto_4
    if-eqz v31, :cond_6

    .line 228
    :try_start_d
    invoke-virtual/range {v31 .. v31}, Ljava/io/ObjectInputStream;->close()V
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_19

    .line 238
    :cond_6
    :goto_5
    new-instance v34, Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v38

    const/16 v39, 0x1

    invoke-static/range {v38 .. v39}, Ljava/lang/Math;->max(II)I

    move-result v38

    move-object/from16 v0, v34

    move/from16 v1, v38

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 239
    .local v34, "prunedFiles":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/io/File;>;"
    new-instance v21, Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v38

    move-object/from16 v0, v21

    move/from16 v1, v38

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    .line 241
    .local v21, "hash2path":Ljava/util/HashMap;, "Ljava/util/HashMap<Lorg/icepdf/core/tag/TagUtil$FileHash;Ljava/lang/String;>;"
    new-instance v12, Ljava/util/HashMap;

    invoke-direct {v12}, Ljava/util/HashMap;-><init>()V

    .line 244
    .local v12, "duplicatePaths":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/util/ArrayList<Lorg/icepdf/core/tag/TagUtil$DuplicateEntry;>;>;"
    const/16 v38, 0x2000

    move/from16 v0, v38

    new-array v9, v0, [B

    .line 245
    .local v9, "buffer":[B
    const/16 v29, 0x0

    .line 246
    .local v29, "numMissing":I
    const/16 v28, 0x0

    .line 247
    .local v28, "numDuplicates":I
    const-wide/16 v36, 0x0

    .line 248
    .local v36, "then":J
    const/16 v22, 0x0

    .local v22, "i":I
    move-object/from16 v17, v18

    .end local v18    # "fis":Ljava/io/FileInputStream;
    .restart local v17    # "fis":Ljava/io/FileInputStream;
    :goto_6
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v38

    move/from16 v0, v22

    move/from16 v1, v38

    if-ge v0, v1, :cond_1c

    .line 249
    move/from16 v0, v22

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Ljava/io/File;

    .line 250
    .local v16, "file":Ljava/io/File;
    invoke-virtual/range {v16 .. v16}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v33

    .line 251
    .local v33, "path":Ljava/lang/String;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v26

    .line 252
    .local v26, "now":J
    sub-long v38, v26, v36

    const-wide/16 v40, 0x1388

    cmp-long v38, v38, v40

    if-ltz v38, :cond_7

    .line 253
    move-wide/from16 v36, v26

    .line 257
    :cond_7
    invoke-virtual/range {v16 .. v16}, Ljava/io/File;->exists()Z

    move-result v38

    if-nez v38, :cond_15

    .line 259
    add-int/lit8 v29, v29, 0x1

    .line 248
    :goto_7
    add-int/lit8 v22, v22, 0x1

    goto :goto_6

    .line 145
    .end local v4    # "allFiles":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/io/File;>;"
    .end local v6    # "bis":Ljava/io/BufferedInputStream;
    .end local v7    # "bos":Ljava/io/BufferedOutputStream;
    .end local v9    # "buffer":[B
    .end local v12    # "duplicatePaths":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/util/ArrayList<Lorg/icepdf/core/tag/TagUtil$DuplicateEntry;>;>;"
    .end local v16    # "file":Ljava/io/File;
    .end local v17    # "fis":Ljava/io/FileInputStream;
    .end local v19    # "fos":Ljava/io/FileOutputStream;
    .end local v21    # "hash2path":Ljava/util/HashMap;, "Ljava/util/HashMap<Lorg/icepdf/core/tag/TagUtil$FileHash;Ljava/lang/String;>;"
    .end local v22    # "i":I
    .end local v24    # "newOutput":Ljava/io/ObjectOutputStream;
    .end local v26    # "now":J
    .end local v28    # "numDuplicates":I
    .end local v29    # "numMissing":I
    .end local v31    # "ois":Ljava/io/ObjectInputStream;
    .end local v33    # "path":Ljava/lang/String;
    .end local v34    # "prunedFiles":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/io/File;>;"
    .end local v36    # "then":J
    :catch_0
    move-exception v14

    .line 146
    .local v14, "e":Ljava/security/NoSuchAlgorithmException;
    sget-object v38, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v39, Ljava/lang/StringBuilder;

    invoke-direct/range {v39 .. v39}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v40, "Problem getting SHA-256 digest: "

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    move-object/from16 v0, v39

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v39

    invoke-virtual/range {v39 .. v39}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v39

    invoke-virtual/range {v38 .. v39}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 337
    .end local v14    # "e":Ljava/security/NoSuchAlgorithmException;
    :cond_8
    :goto_8
    return-void

    .line 167
    .restart local v8    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v20    # "fos":Ljava/io/FileOutputStream;
    .restart local v25    # "newOutput":Ljava/io/ObjectOutputStream;
    :catch_1
    move-exception v14

    .line 168
    .local v14, "e":Ljava/io/IOException;
    const-string/jumbo v38, "DocumentService"

    new-instance v39, Ljava/lang/StringBuilder;

    invoke-direct/range {v39 .. v39}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v40, "Exception: "

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    invoke-virtual {v14}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v40

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    invoke-virtual/range {v39 .. v39}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v39

    invoke-static/range {v38 .. v39}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 183
    .end local v14    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v38

    move-object v7, v8

    .end local v8    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v7    # "bos":Ljava/io/BufferedOutputStream;
    move-object/from16 v19, v20

    .end local v20    # "fos":Ljava/io/FileOutputStream;
    .restart local v19    # "fos":Ljava/io/FileOutputStream;
    move-object/from16 v24, v25

    .line 186
    .end local v25    # "newOutput":Ljava/io/ObjectOutputStream;
    .restart local v24    # "newOutput":Ljava/io/ObjectOutputStream;
    goto/16 :goto_2

    .line 159
    :catch_3
    move-exception v14

    .line 160
    .restart local v14    # "e":Ljava/io/IOException;
    :goto_9
    :try_start_e
    sget-object v38, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v39, Ljava/lang/StringBuilder;

    invoke-direct/range {v39 .. v39}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v40, "Problem openning newCatalogFile: "

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    move-object/from16 v0, v39

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v39

    invoke-virtual/range {v39 .. v39}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v39

    invoke-virtual/range {v38 .. v39}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_0

    .line 165
    if-eqz v24, :cond_9

    .line 166
    :try_start_f
    invoke-virtual/range {v24 .. v24}, Ljava/io/ObjectOutputStream;->close()V
    :try_end_f
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_f} :catch_5

    .line 172
    :cond_9
    :goto_a
    if-eqz v19, :cond_a

    .line 173
    :try_start_10
    invoke-virtual/range {v19 .. v19}, Ljava/io/FileOutputStream;->close()V
    :try_end_10
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_10} :catch_15

    .line 180
    :cond_a
    :goto_b
    if-eqz v7, :cond_3

    .line 181
    :try_start_11
    invoke-virtual {v7}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_11
    .catch Ljava/io/IOException; {:try_start_11 .. :try_end_11} :catch_4

    goto/16 :goto_2

    .line 183
    :catch_4
    move-exception v38

    goto/16 :goto_2

    .line 167
    :catch_5
    move-exception v14

    .line 168
    const-string/jumbo v38, "DocumentService"

    new-instance v39, Ljava/lang/StringBuilder;

    invoke-direct/range {v39 .. v39}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v40, "Exception: "

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    invoke-virtual {v14}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v40

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    invoke-virtual/range {v39 .. v39}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v39

    invoke-static/range {v38 .. v39}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_a

    .line 164
    .end local v14    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v38

    .line 165
    :goto_c
    if-eqz v24, :cond_b

    .line 166
    :try_start_12
    invoke-virtual/range {v24 .. v24}, Ljava/io/ObjectOutputStream;->close()V
    :try_end_12
    .catch Ljava/io/IOException; {:try_start_12 .. :try_end_12} :catch_6

    .line 172
    :cond_b
    :goto_d
    if-eqz v19, :cond_c

    .line 173
    :try_start_13
    invoke-virtual/range {v19 .. v19}, Ljava/io/FileOutputStream;->close()V
    :try_end_13
    .catch Ljava/io/IOException; {:try_start_13 .. :try_end_13} :catch_16

    .line 180
    :cond_c
    :goto_e
    if-eqz v7, :cond_d

    .line 181
    :try_start_14
    invoke-virtual {v7}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_14
    .catch Ljava/io/IOException; {:try_start_14 .. :try_end_14} :catch_17

    .line 185
    :cond_d
    :goto_f
    throw v38

    .line 167
    :catch_6
    move-exception v14

    .line 168
    .restart local v14    # "e":Ljava/io/IOException;
    const-string/jumbo v39, "DocumentService"

    new-instance v40, Ljava/lang/StringBuilder;

    invoke-direct/range {v40 .. v40}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v41, "Exception: "

    invoke-virtual/range {v40 .. v41}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v40

    invoke-virtual {v14}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v41

    invoke-virtual/range {v40 .. v41}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v40

    invoke-virtual/range {v40 .. v40}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v40

    invoke-static/range {v39 .. v40}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_d

    .line 213
    .end local v14    # "e":Ljava/io/IOException;
    .restart local v4    # "allFiles":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/io/File;>;"
    .restart local v6    # "bis":Ljava/io/BufferedInputStream;
    .restart local v18    # "fis":Ljava/io/FileInputStream;
    .restart local v31    # "ois":Ljava/io/ObjectInputStream;
    :catch_7
    move-exception v14

    .line 214
    .restart local v14    # "e":Ljava/io/IOException;
    const-string/jumbo v38, "DocumentService"

    new-instance v39, Ljava/lang/StringBuilder;

    invoke-direct/range {v39 .. v39}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v40, "Exception: "

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    invoke-virtual {v14}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v40

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    invoke-virtual/range {v39 .. v39}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v39

    invoke-static/range {v38 .. v39}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    .line 200
    .end local v6    # "bis":Ljava/io/BufferedInputStream;
    .end local v14    # "e":Ljava/io/IOException;
    .end local v18    # "fis":Ljava/io/FileInputStream;
    .end local v31    # "ois":Ljava/io/ObjectInputStream;
    .restart local v5    # "bis":Ljava/io/BufferedInputStream;
    .restart local v17    # "fis":Ljava/io/FileInputStream;
    .restart local v30    # "ois":Ljava/io/ObjectInputStream;
    :catch_8
    move-exception v14

    .line 201
    .end local v4    # "allFiles":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/io/File;>;"
    .restart local v14    # "e":Ljava/io/IOException;
    :goto_10
    :try_start_15
    sget-object v38, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v39, Ljava/lang/StringBuilder;

    invoke-direct/range {v39 .. v39}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v40, "Problem openning oldCatalogFile: "

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    move-object/from16 v0, v39

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v39

    invoke-virtual/range {v39 .. v39}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v39

    invoke-virtual/range {v38 .. v39}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_15
    .catchall {:try_start_15 .. :try_end_15} :catchall_1

    .line 211
    if-eqz v17, :cond_e

    .line 212
    :try_start_16
    invoke-virtual/range {v17 .. v17}, Ljava/io/FileInputStream;->close()V
    :try_end_16
    .catch Ljava/io/IOException; {:try_start_16 .. :try_end_16} :catch_a

    .line 218
    :cond_e
    :goto_11
    if-eqz v5, :cond_f

    .line 219
    :try_start_17
    invoke-virtual {v5}, Ljava/io/BufferedInputStream;->close()V
    :try_end_17
    .catch Ljava/io/IOException; {:try_start_17 .. :try_end_17} :catch_1a

    .line 227
    :cond_f
    :goto_12
    if-eqz v30, :cond_8

    .line 228
    :try_start_18
    invoke-virtual/range {v30 .. v30}, Ljava/io/ObjectInputStream;->close()V
    :try_end_18
    .catch Ljava/io/IOException; {:try_start_18 .. :try_end_18} :catch_9

    goto/16 :goto_8

    .line 230
    :catch_9
    move-exception v38

    goto/16 :goto_8

    .line 213
    :catch_a
    move-exception v14

    .line 214
    const-string/jumbo v38, "DocumentService"

    new-instance v39, Ljava/lang/StringBuilder;

    invoke-direct/range {v39 .. v39}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v40, "Exception: "

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    invoke-virtual {v14}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v40

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    invoke-virtual/range {v39 .. v39}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v39

    invoke-static/range {v38 .. v39}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_11

    .line 204
    .end local v14    # "e":Ljava/io/IOException;
    .restart local v4    # "allFiles":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/io/File;>;"
    :catch_b
    move-exception v14

    .line 205
    .end local v4    # "allFiles":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/io/File;>;"
    .local v14, "e":Ljava/lang/ClassNotFoundException;
    :goto_13
    :try_start_19
    sget-object v38, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v39, Ljava/lang/StringBuilder;

    invoke-direct/range {v39 .. v39}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v40, "Problem reading catalog from ["

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    move-object/from16 v0, v39

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    const-string/jumbo v40, "]: "

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    move-object/from16 v0, v39

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v39

    invoke-virtual/range {v39 .. v39}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v39

    invoke-virtual/range {v38 .. v39}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_19
    .catchall {:try_start_19 .. :try_end_19} :catchall_1

    .line 211
    if-eqz v17, :cond_10

    .line 212
    :try_start_1a
    invoke-virtual/range {v17 .. v17}, Ljava/io/FileInputStream;->close()V
    :try_end_1a
    .catch Ljava/io/IOException; {:try_start_1a .. :try_end_1a} :catch_d

    .line 218
    .end local v14    # "e":Ljava/lang/ClassNotFoundException;
    :cond_10
    :goto_14
    if-eqz v5, :cond_11

    .line 219
    :try_start_1b
    invoke-virtual {v5}, Ljava/io/BufferedInputStream;->close()V
    :try_end_1b
    .catch Ljava/io/IOException; {:try_start_1b .. :try_end_1b} :catch_1b

    .line 227
    :cond_11
    :goto_15
    if-eqz v30, :cond_8

    .line 228
    :try_start_1c
    invoke-virtual/range {v30 .. v30}, Ljava/io/ObjectInputStream;->close()V
    :try_end_1c
    .catch Ljava/io/IOException; {:try_start_1c .. :try_end_1c} :catch_c

    goto/16 :goto_8

    .line 230
    :catch_c
    move-exception v38

    goto/16 :goto_8

    .line 213
    .restart local v14    # "e":Ljava/lang/ClassNotFoundException;
    :catch_d
    move-exception v14

    .line 214
    .local v14, "e":Ljava/io/IOException;
    const-string/jumbo v38, "DocumentService"

    new-instance v39, Ljava/lang/StringBuilder;

    invoke-direct/range {v39 .. v39}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v40, "Exception: "

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    invoke-virtual {v14}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v40

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    invoke-virtual/range {v39 .. v39}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v39

    invoke-static/range {v38 .. v39}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_14

    .line 210
    .end local v14    # "e":Ljava/io/IOException;
    :catchall_1
    move-exception v38

    .line 211
    :goto_16
    if-eqz v17, :cond_12

    .line 212
    :try_start_1d
    invoke-virtual/range {v17 .. v17}, Ljava/io/FileInputStream;->close()V
    :try_end_1d
    .catch Ljava/io/IOException; {:try_start_1d .. :try_end_1d} :catch_e

    .line 218
    :cond_12
    :goto_17
    if-eqz v5, :cond_13

    .line 219
    :try_start_1e
    invoke-virtual {v5}, Ljava/io/BufferedInputStream;->close()V
    :try_end_1e
    .catch Ljava/io/IOException; {:try_start_1e .. :try_end_1e} :catch_1c

    .line 227
    :cond_13
    :goto_18
    if-eqz v30, :cond_14

    .line 228
    :try_start_1f
    invoke-virtual/range {v30 .. v30}, Ljava/io/ObjectInputStream;->close()V
    :try_end_1f
    .catch Ljava/io/IOException; {:try_start_1f .. :try_end_1f} :catch_1d

    .line 232
    :cond_14
    :goto_19
    throw v38

    .line 213
    :catch_e
    move-exception v14

    .line 214
    .restart local v14    # "e":Ljava/io/IOException;
    const-string/jumbo v39, "DocumentService"

    new-instance v40, Ljava/lang/StringBuilder;

    invoke-direct/range {v40 .. v40}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v41, "Exception: "

    invoke-virtual/range {v40 .. v41}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v40

    invoke-virtual {v14}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v41

    invoke-virtual/range {v40 .. v41}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v40

    invoke-virtual/range {v40 .. v40}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v40

    invoke-static/range {v39 .. v40}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_17

    .line 264
    .end local v5    # "bis":Ljava/io/BufferedInputStream;
    .end local v14    # "e":Ljava/io/IOException;
    .end local v30    # "ois":Ljava/io/ObjectInputStream;
    .restart local v4    # "allFiles":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/io/File;>;"
    .restart local v6    # "bis":Ljava/io/BufferedInputStream;
    .restart local v9    # "buffer":[B
    .restart local v12    # "duplicatePaths":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/util/ArrayList<Lorg/icepdf/core/tag/TagUtil$DuplicateEntry;>;>;"
    .restart local v16    # "file":Ljava/io/File;
    .restart local v21    # "hash2path":Ljava/util/HashMap;, "Ljava/util/HashMap<Lorg/icepdf/core/tag/TagUtil$FileHash;Ljava/lang/String;>;"
    .restart local v22    # "i":I
    .restart local v26    # "now":J
    .restart local v28    # "numDuplicates":I
    .restart local v29    # "numMissing":I
    .restart local v31    # "ois":Ljava/io/ObjectInputStream;
    .restart local v33    # "path":Ljava/lang/String;
    .restart local v34    # "prunedFiles":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/io/File;>;"
    .restart local v36    # "then":J
    :cond_15
    const/16 v17, 0x0

    .line 266
    :try_start_20
    new-instance v18, Ljava/io/FileInputStream;

    move-object/from16 v0, v18

    move-object/from16 v1, v16

    invoke-direct {v0, v1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_20
    .catch Ljava/io/IOException; {:try_start_20 .. :try_end_20} :catch_1e
    .catchall {:try_start_20 .. :try_end_20} :catchall_2

    .line 267
    .end local v17    # "fis":Ljava/io/FileInputStream;
    .restart local v18    # "fis":Ljava/io/FileInputStream;
    :try_start_21
    invoke-virtual {v11}, Ljava/security/MessageDigest;->reset()V

    .line 269
    :goto_1a
    move-object/from16 v0, v18

    invoke-virtual {v0, v9}, Ljava/io/FileInputStream;->read([B)I
    :try_end_21
    .catch Ljava/io/IOException; {:try_start_21 .. :try_end_21} :catch_f
    .catchall {:try_start_21 .. :try_end_21} :catchall_3

    move-result v35

    .line 270
    .local v35, "read":I
    if-gez v35, :cond_18

    .line 282
    if-eqz v18, :cond_16

    .line 283
    :try_start_22
    invoke-virtual/range {v18 .. v18}, Ljava/io/FileInputStream;->close()V
    :try_end_22
    .catch Ljava/io/IOException; {:try_start_22 .. :try_end_22} :catch_11

    :cond_16
    move-object/from16 v17, v18

    .line 289
    .end local v18    # "fis":Ljava/io/FileInputStream;
    .end local v35    # "read":I
    .restart local v17    # "fis":Ljava/io/FileInputStream;
    :cond_17
    :goto_1b
    new-instance v15, Lorg/icepdf/core/tag/TagUtil$FileHash;

    invoke-virtual {v11}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v38

    invoke-virtual/range {v16 .. v16}, Ljava/io/File;->length()J

    move-result-wide v40

    move-object/from16 v0, v38

    move-wide/from16 v1, v40

    invoke-direct {v15, v0, v1, v2}, Lorg/icepdf/core/tag/TagUtil$FileHash;-><init>([BJ)V

    .line 290
    .local v15, "fh":Lorg/icepdf/core/tag/TagUtil$FileHash;
    move-object/from16 v0, v21

    invoke-virtual {v0, v15}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v32

    check-cast v32, Ljava/lang/String;

    .line 291
    .local v32, "originalPath":Ljava/lang/String;
    if-nez v32, :cond_1a

    .line 292
    move-object/from16 v0, v21

    move-object/from16 v1, v33

    invoke-virtual {v0, v15, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 293
    move-object/from16 v0, v34

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_7

    .line 272
    .end local v15    # "fh":Lorg/icepdf/core/tag/TagUtil$FileHash;
    .end local v17    # "fis":Ljava/io/FileInputStream;
    .end local v32    # "originalPath":Ljava/lang/String;
    .restart local v18    # "fis":Ljava/io/FileInputStream;
    .restart local v35    # "read":I
    :cond_18
    const/16 v38, 0x0

    :try_start_23
    move/from16 v0, v38

    move/from16 v1, v35

    invoke-virtual {v11, v9, v0, v1}, Ljava/security/MessageDigest;->update([BII)V
    :try_end_23
    .catch Ljava/io/IOException; {:try_start_23 .. :try_end_23} :catch_f
    .catchall {:try_start_23 .. :try_end_23} :catchall_3

    goto :goto_1a

    .line 276
    .end local v35    # "read":I
    :catch_f
    move-exception v14

    move-object/from16 v17, v18

    .line 277
    .end local v18    # "fis":Ljava/io/FileInputStream;
    .restart local v14    # "e":Ljava/io/IOException;
    .restart local v17    # "fis":Ljava/io/FileInputStream;
    :goto_1c
    :try_start_24
    sget-object v38, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v39, Ljava/lang/StringBuilder;

    invoke-direct/range {v39 .. v39}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v40, "Problem hashing \'"

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    move-object/from16 v0, v39

    move-object/from16 v1, v33

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    const-string/jumbo v40, "\' : "

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    move-object/from16 v0, v39

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v39

    invoke-virtual/range {v39 .. v39}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v39

    invoke-virtual/range {v38 .. v39}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_24
    .catchall {:try_start_24 .. :try_end_24} :catchall_2

    .line 282
    if-eqz v17, :cond_17

    .line 283
    :try_start_25
    invoke-virtual/range {v17 .. v17}, Ljava/io/FileInputStream;->close()V
    :try_end_25
    .catch Ljava/io/IOException; {:try_start_25 .. :try_end_25} :catch_10

    goto :goto_1b

    .line 284
    :catch_10
    move-exception v14

    .line 285
    const-string/jumbo v38, "DocumentService"

    new-instance v39, Ljava/lang/StringBuilder;

    invoke-direct/range {v39 .. v39}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v40, "Exception: "

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    invoke-virtual {v14}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v40

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    invoke-virtual/range {v39 .. v39}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v39

    invoke-static/range {v38 .. v39}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1b

    .line 284
    .end local v14    # "e":Ljava/io/IOException;
    .end local v17    # "fis":Ljava/io/FileInputStream;
    .restart local v18    # "fis":Ljava/io/FileInputStream;
    .restart local v35    # "read":I
    :catch_11
    move-exception v14

    .line 285
    .restart local v14    # "e":Ljava/io/IOException;
    const-string/jumbo v38, "DocumentService"

    new-instance v39, Ljava/lang/StringBuilder;

    invoke-direct/range {v39 .. v39}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v40, "Exception: "

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    invoke-virtual {v14}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v40

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    invoke-virtual/range {v39 .. v39}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v39

    invoke-static/range {v38 .. v39}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v17, v18

    .line 287
    .end local v18    # "fis":Ljava/io/FileInputStream;
    .restart local v17    # "fis":Ljava/io/FileInputStream;
    goto/16 :goto_1b

    .line 281
    .end local v14    # "e":Ljava/io/IOException;
    .end local v35    # "read":I
    :catchall_2
    move-exception v38

    .line 282
    :goto_1d
    if-eqz v17, :cond_19

    .line 283
    :try_start_26
    invoke-virtual/range {v17 .. v17}, Ljava/io/FileInputStream;->close()V
    :try_end_26
    .catch Ljava/io/IOException; {:try_start_26 .. :try_end_26} :catch_12

    .line 286
    :cond_19
    :goto_1e
    throw v38

    .line 284
    :catch_12
    move-exception v14

    .line 285
    .restart local v14    # "e":Ljava/io/IOException;
    const-string/jumbo v39, "DocumentService"

    new-instance v40, Ljava/lang/StringBuilder;

    invoke-direct/range {v40 .. v40}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v41, "Exception: "

    invoke-virtual/range {v40 .. v41}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v40

    invoke-virtual {v14}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v41

    invoke-virtual/range {v40 .. v41}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v40

    invoke-virtual/range {v40 .. v40}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v40

    invoke-static/range {v39 .. v40}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1e

    .line 296
    .end local v14    # "e":Ljava/io/IOException;
    .restart local v15    # "fh":Lorg/icepdf/core/tag/TagUtil$FileHash;
    .restart local v32    # "originalPath":Ljava/lang/String;
    :cond_1a
    move-object/from16 v0, v32

    invoke-virtual {v12, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/util/ArrayList;

    .line 297
    .local v13, "duplicates":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/icepdf/core/tag/TagUtil$DuplicateEntry;>;"
    if-nez v13, :cond_1b

    .line 298
    new-instance v13, Ljava/util/ArrayList;

    .end local v13    # "duplicates":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/icepdf/core/tag/TagUtil$DuplicateEntry;>;"
    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    .line 299
    .restart local v13    # "duplicates":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/icepdf/core/tag/TagUtil$DuplicateEntry;>;"
    move-object/from16 v0, v32

    invoke-virtual {v12, v0, v13}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 301
    :cond_1b
    new-instance v10, Lorg/icepdf/core/tag/TagUtil$DuplicateEntry;

    move-object/from16 v0, v32

    move-object/from16 v1, v33

    move/from16 v2, v22

    invoke-direct {v10, v0, v1, v2}, Lorg/icepdf/core/tag/TagUtil$DuplicateEntry;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    .line 302
    .local v10, "de":Lorg/icepdf/core/tag/TagUtil$DuplicateEntry;
    invoke-virtual {v13, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 303
    add-int/lit8 v28, v28, 0x1

    goto/16 :goto_7

    .line 308
    .end local v10    # "de":Lorg/icepdf/core/tag/TagUtil$DuplicateEntry;
    .end local v13    # "duplicates":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/icepdf/core/tag/TagUtil$DuplicateEntry;>;"
    .end local v15    # "fh":Lorg/icepdf/core/tag/TagUtil$FileHash;
    .end local v16    # "file":Ljava/io/File;
    .end local v26    # "now":J
    .end local v32    # "originalPath":Ljava/lang/String;
    .end local v33    # "path":Ljava/lang/String;
    :cond_1c
    invoke-virtual/range {v34 .. v34}, Ljava/util/ArrayList;->trimToSize()V

    .line 325
    invoke-virtual/range {v34 .. v34}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v23

    .local v23, "i$":Ljava/util/Iterator;
    :goto_1f
    invoke-interface/range {v23 .. v23}, Ljava/util/Iterator;->hasNext()Z

    move-result v38

    if-eqz v38, :cond_1d

    invoke-interface/range {v23 .. v23}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v38

    check-cast v38, Ljava/io/File;

    goto :goto_1f

    .line 330
    :cond_1d
    :try_start_27
    move-object/from16 v0, v24

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 331
    invoke-virtual/range {v24 .. v24}, Ljava/io/ObjectOutputStream;->flush()V

    .line 332
    invoke-virtual/range {v24 .. v24}, Ljava/io/ObjectOutputStream;->close()V
    :try_end_27
    .catch Ljava/io/IOException; {:try_start_27 .. :try_end_27} :catch_13

    goto/16 :goto_8

    .line 334
    :catch_13
    move-exception v14

    .line 335
    .restart local v14    # "e":Ljava/io/IOException;
    sget-object v38, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v39, Ljava/lang/StringBuilder;

    invoke-direct/range {v39 .. v39}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v40, "Problem saving newCatalogFile: "

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    move-object/from16 v0, v39

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v39

    invoke-virtual/range {v39 .. v39}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v39

    invoke-virtual/range {v38 .. v39}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_8

    .line 175
    .end local v4    # "allFiles":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/io/File;>;"
    .end local v6    # "bis":Ljava/io/BufferedInputStream;
    .end local v7    # "bos":Ljava/io/BufferedOutputStream;
    .end local v9    # "buffer":[B
    .end local v12    # "duplicatePaths":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/util/ArrayList<Lorg/icepdf/core/tag/TagUtil$DuplicateEntry;>;>;"
    .end local v14    # "e":Ljava/io/IOException;
    .end local v17    # "fis":Ljava/io/FileInputStream;
    .end local v19    # "fos":Ljava/io/FileOutputStream;
    .end local v21    # "hash2path":Ljava/util/HashMap;, "Ljava/util/HashMap<Lorg/icepdf/core/tag/TagUtil$FileHash;Ljava/lang/String;>;"
    .end local v22    # "i":I
    .end local v23    # "i$":Ljava/util/Iterator;
    .end local v24    # "newOutput":Ljava/io/ObjectOutputStream;
    .end local v28    # "numDuplicates":I
    .end local v29    # "numMissing":I
    .end local v31    # "ois":Ljava/io/ObjectInputStream;
    .end local v34    # "prunedFiles":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/io/File;>;"
    .end local v36    # "then":J
    .restart local v8    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v20    # "fos":Ljava/io/FileOutputStream;
    .restart local v25    # "newOutput":Ljava/io/ObjectOutputStream;
    :catch_14
    move-exception v38

    goto/16 :goto_1

    .end local v8    # "bos":Ljava/io/BufferedOutputStream;
    .end local v20    # "fos":Ljava/io/FileOutputStream;
    .end local v25    # "newOutput":Ljava/io/ObjectOutputStream;
    .restart local v7    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v14    # "e":Ljava/io/IOException;
    .restart local v19    # "fos":Ljava/io/FileOutputStream;
    .restart local v24    # "newOutput":Ljava/io/ObjectOutputStream;
    :catch_15
    move-exception v38

    goto/16 :goto_b

    .end local v14    # "e":Ljava/io/IOException;
    :catch_16
    move-exception v39

    goto/16 :goto_e

    .line 183
    :catch_17
    move-exception v39

    goto/16 :goto_f

    .line 221
    .restart local v4    # "allFiles":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/io/File;>;"
    .restart local v6    # "bis":Ljava/io/BufferedInputStream;
    .restart local v18    # "fis":Ljava/io/FileInputStream;
    .restart local v31    # "ois":Ljava/io/ObjectInputStream;
    :catch_18
    move-exception v38

    goto/16 :goto_4

    .line 230
    :catch_19
    move-exception v38

    goto/16 :goto_5

    .line 221
    .end local v4    # "allFiles":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/io/File;>;"
    .end local v6    # "bis":Ljava/io/BufferedInputStream;
    .end local v18    # "fis":Ljava/io/FileInputStream;
    .end local v31    # "ois":Ljava/io/ObjectInputStream;
    .restart local v5    # "bis":Ljava/io/BufferedInputStream;
    .restart local v14    # "e":Ljava/io/IOException;
    .restart local v17    # "fis":Ljava/io/FileInputStream;
    .restart local v30    # "ois":Ljava/io/ObjectInputStream;
    :catch_1a
    move-exception v38

    goto/16 :goto_12

    .end local v14    # "e":Ljava/io/IOException;
    :catch_1b
    move-exception v38

    goto/16 :goto_15

    :catch_1c
    move-exception v39

    goto/16 :goto_18

    .line 230
    :catch_1d
    move-exception v39

    goto/16 :goto_19

    .line 281
    .end local v5    # "bis":Ljava/io/BufferedInputStream;
    .end local v17    # "fis":Ljava/io/FileInputStream;
    .end local v30    # "ois":Ljava/io/ObjectInputStream;
    .restart local v4    # "allFiles":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/io/File;>;"
    .restart local v6    # "bis":Ljava/io/BufferedInputStream;
    .restart local v9    # "buffer":[B
    .restart local v12    # "duplicatePaths":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/util/ArrayList<Lorg/icepdf/core/tag/TagUtil$DuplicateEntry;>;>;"
    .restart local v16    # "file":Ljava/io/File;
    .restart local v18    # "fis":Ljava/io/FileInputStream;
    .restart local v21    # "hash2path":Ljava/util/HashMap;, "Ljava/util/HashMap<Lorg/icepdf/core/tag/TagUtil$FileHash;Ljava/lang/String;>;"
    .restart local v22    # "i":I
    .restart local v26    # "now":J
    .restart local v28    # "numDuplicates":I
    .restart local v29    # "numMissing":I
    .restart local v31    # "ois":Ljava/io/ObjectInputStream;
    .restart local v33    # "path":Ljava/lang/String;
    .restart local v34    # "prunedFiles":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/io/File;>;"
    .restart local v36    # "then":J
    :catchall_3
    move-exception v38

    move-object/from16 v17, v18

    .end local v18    # "fis":Ljava/io/FileInputStream;
    .restart local v17    # "fis":Ljava/io/FileInputStream;
    goto/16 :goto_1d

    .line 276
    :catch_1e
    move-exception v14

    goto/16 :goto_1c

    .line 210
    .end local v6    # "bis":Ljava/io/BufferedInputStream;
    .end local v9    # "buffer":[B
    .end local v12    # "duplicatePaths":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/util/ArrayList<Lorg/icepdf/core/tag/TagUtil$DuplicateEntry;>;>;"
    .end local v16    # "file":Ljava/io/File;
    .end local v17    # "fis":Ljava/io/FileInputStream;
    .end local v21    # "hash2path":Ljava/util/HashMap;, "Ljava/util/HashMap<Lorg/icepdf/core/tag/TagUtil$FileHash;Ljava/lang/String;>;"
    .end local v22    # "i":I
    .end local v26    # "now":J
    .end local v28    # "numDuplicates":I
    .end local v29    # "numMissing":I
    .end local v31    # "ois":Ljava/io/ObjectInputStream;
    .end local v33    # "path":Ljava/lang/String;
    .end local v34    # "prunedFiles":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/io/File;>;"
    .end local v36    # "then":J
    .restart local v5    # "bis":Ljava/io/BufferedInputStream;
    .restart local v18    # "fis":Ljava/io/FileInputStream;
    .restart local v30    # "ois":Ljava/io/ObjectInputStream;
    :catchall_4
    move-exception v38

    move-object/from16 v17, v18

    .end local v18    # "fis":Ljava/io/FileInputStream;
    .restart local v17    # "fis":Ljava/io/FileInputStream;
    goto/16 :goto_16

    .end local v5    # "bis":Ljava/io/BufferedInputStream;
    .end local v17    # "fis":Ljava/io/FileInputStream;
    .restart local v6    # "bis":Ljava/io/BufferedInputStream;
    .restart local v18    # "fis":Ljava/io/FileInputStream;
    :catchall_5
    move-exception v38

    move-object v5, v6

    .end local v6    # "bis":Ljava/io/BufferedInputStream;
    .restart local v5    # "bis":Ljava/io/BufferedInputStream;
    move-object/from16 v17, v18

    .end local v18    # "fis":Ljava/io/FileInputStream;
    .restart local v17    # "fis":Ljava/io/FileInputStream;
    goto/16 :goto_16

    .end local v4    # "allFiles":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/io/File;>;"
    .end local v5    # "bis":Ljava/io/BufferedInputStream;
    .end local v17    # "fis":Ljava/io/FileInputStream;
    .end local v30    # "ois":Ljava/io/ObjectInputStream;
    .restart local v6    # "bis":Ljava/io/BufferedInputStream;
    .restart local v18    # "fis":Ljava/io/FileInputStream;
    .restart local v31    # "ois":Ljava/io/ObjectInputStream;
    :catchall_6
    move-exception v38

    move-object/from16 v30, v31

    .end local v31    # "ois":Ljava/io/ObjectInputStream;
    .restart local v30    # "ois":Ljava/io/ObjectInputStream;
    move-object v5, v6

    .end local v6    # "bis":Ljava/io/BufferedInputStream;
    .restart local v5    # "bis":Ljava/io/BufferedInputStream;
    move-object/from16 v17, v18

    .end local v18    # "fis":Ljava/io/FileInputStream;
    .restart local v17    # "fis":Ljava/io/FileInputStream;
    goto/16 :goto_16

    .line 204
    .end local v17    # "fis":Ljava/io/FileInputStream;
    .restart local v4    # "allFiles":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/io/File;>;"
    .restart local v18    # "fis":Ljava/io/FileInputStream;
    :catch_1f
    move-exception v14

    move-object/from16 v17, v18

    .end local v18    # "fis":Ljava/io/FileInputStream;
    .restart local v17    # "fis":Ljava/io/FileInputStream;
    goto/16 :goto_13

    .end local v5    # "bis":Ljava/io/BufferedInputStream;
    .end local v17    # "fis":Ljava/io/FileInputStream;
    .restart local v6    # "bis":Ljava/io/BufferedInputStream;
    .restart local v18    # "fis":Ljava/io/FileInputStream;
    :catch_20
    move-exception v14

    move-object v5, v6

    .end local v6    # "bis":Ljava/io/BufferedInputStream;
    .restart local v5    # "bis":Ljava/io/BufferedInputStream;
    move-object/from16 v17, v18

    .end local v18    # "fis":Ljava/io/FileInputStream;
    .restart local v17    # "fis":Ljava/io/FileInputStream;
    goto/16 :goto_13

    .end local v4    # "allFiles":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/io/File;>;"
    .end local v5    # "bis":Ljava/io/BufferedInputStream;
    .end local v17    # "fis":Ljava/io/FileInputStream;
    .end local v30    # "ois":Ljava/io/ObjectInputStream;
    .restart local v6    # "bis":Ljava/io/BufferedInputStream;
    .restart local v18    # "fis":Ljava/io/FileInputStream;
    .restart local v31    # "ois":Ljava/io/ObjectInputStream;
    :catch_21
    move-exception v14

    move-object/from16 v30, v31

    .end local v31    # "ois":Ljava/io/ObjectInputStream;
    .restart local v30    # "ois":Ljava/io/ObjectInputStream;
    move-object v5, v6

    .end local v6    # "bis":Ljava/io/BufferedInputStream;
    .restart local v5    # "bis":Ljava/io/BufferedInputStream;
    move-object/from16 v17, v18

    .end local v18    # "fis":Ljava/io/FileInputStream;
    .restart local v17    # "fis":Ljava/io/FileInputStream;
    goto/16 :goto_13

    .line 200
    .end local v17    # "fis":Ljava/io/FileInputStream;
    .restart local v4    # "allFiles":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/io/File;>;"
    .restart local v18    # "fis":Ljava/io/FileInputStream;
    :catch_22
    move-exception v14

    move-object/from16 v17, v18

    .end local v18    # "fis":Ljava/io/FileInputStream;
    .restart local v17    # "fis":Ljava/io/FileInputStream;
    goto/16 :goto_10

    .end local v5    # "bis":Ljava/io/BufferedInputStream;
    .end local v17    # "fis":Ljava/io/FileInputStream;
    .restart local v6    # "bis":Ljava/io/BufferedInputStream;
    .restart local v18    # "fis":Ljava/io/FileInputStream;
    :catch_23
    move-exception v14

    move-object v5, v6

    .end local v6    # "bis":Ljava/io/BufferedInputStream;
    .restart local v5    # "bis":Ljava/io/BufferedInputStream;
    move-object/from16 v17, v18

    .end local v18    # "fis":Ljava/io/FileInputStream;
    .restart local v17    # "fis":Ljava/io/FileInputStream;
    goto/16 :goto_10

    .end local v4    # "allFiles":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/io/File;>;"
    .end local v5    # "bis":Ljava/io/BufferedInputStream;
    .end local v17    # "fis":Ljava/io/FileInputStream;
    .end local v30    # "ois":Ljava/io/ObjectInputStream;
    .restart local v6    # "bis":Ljava/io/BufferedInputStream;
    .restart local v18    # "fis":Ljava/io/FileInputStream;
    .restart local v31    # "ois":Ljava/io/ObjectInputStream;
    :catch_24
    move-exception v14

    move-object/from16 v30, v31

    .end local v31    # "ois":Ljava/io/ObjectInputStream;
    .restart local v30    # "ois":Ljava/io/ObjectInputStream;
    move-object v5, v6

    .end local v6    # "bis":Ljava/io/BufferedInputStream;
    .restart local v5    # "bis":Ljava/io/BufferedInputStream;
    move-object/from16 v17, v18

    .end local v18    # "fis":Ljava/io/FileInputStream;
    .restart local v17    # "fis":Ljava/io/FileInputStream;
    goto/16 :goto_10

    .line 164
    .end local v5    # "bis":Ljava/io/BufferedInputStream;
    .end local v17    # "fis":Ljava/io/FileInputStream;
    .end local v19    # "fos":Ljava/io/FileOutputStream;
    .end local v30    # "ois":Ljava/io/ObjectInputStream;
    .restart local v20    # "fos":Ljava/io/FileOutputStream;
    :catchall_7
    move-exception v38

    move-object/from16 v19, v20

    .end local v20    # "fos":Ljava/io/FileOutputStream;
    .restart local v19    # "fos":Ljava/io/FileOutputStream;
    goto/16 :goto_c

    .end local v7    # "bos":Ljava/io/BufferedOutputStream;
    .end local v19    # "fos":Ljava/io/FileOutputStream;
    .restart local v8    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v20    # "fos":Ljava/io/FileOutputStream;
    :catchall_8
    move-exception v38

    move-object v7, v8

    .end local v8    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v7    # "bos":Ljava/io/BufferedOutputStream;
    move-object/from16 v19, v20

    .end local v20    # "fos":Ljava/io/FileOutputStream;
    .restart local v19    # "fos":Ljava/io/FileOutputStream;
    goto/16 :goto_c

    .line 159
    .end local v19    # "fos":Ljava/io/FileOutputStream;
    .restart local v20    # "fos":Ljava/io/FileOutputStream;
    :catch_25
    move-exception v14

    move-object/from16 v19, v20

    .end local v20    # "fos":Ljava/io/FileOutputStream;
    .restart local v19    # "fos":Ljava/io/FileOutputStream;
    goto/16 :goto_9

    .end local v7    # "bos":Ljava/io/BufferedOutputStream;
    .end local v19    # "fos":Ljava/io/FileOutputStream;
    .restart local v8    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v20    # "fos":Ljava/io/FileOutputStream;
    :catch_26
    move-exception v14

    move-object v7, v8

    .end local v8    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v7    # "bos":Ljava/io/BufferedOutputStream;
    move-object/from16 v19, v20

    .end local v20    # "fos":Ljava/io/FileOutputStream;
    .restart local v19    # "fos":Ljava/io/FileOutputStream;
    goto/16 :goto_9
.end method

.method public static query(Ljava/lang/String;[Ljava/lang/String;)V
    .locals 19
    .param p0, "tagFile"    # Ljava/lang/String;
    .param p1, "rpnQuery"    # [Ljava/lang/String;

    .prologue
    .line 513
    const/4 v6, 0x0

    .line 515
    .local v6, "expression":Lorg/icepdf/core/tag/query/Expression;
    :try_start_0
    invoke-static/range {p1 .. p1}, Lorg/icepdf/core/tag/query/Querior;->parse([Ljava/lang/String;)Lorg/icepdf/core/tag/query/Expression;
    :try_end_0
    .catch Lorg/icepdf/core/tag/query/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    .line 521
    if-nez v6, :cond_1

    .line 585
    :cond_0
    :goto_0
    return-void

    .line 517
    :catch_0
    move-exception v5

    .line 518
    .local v5, "e":Lorg/icepdf/core/tag/query/ParseException;
    sget-object v15, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v17, "Problem parsing expression: "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0

    .line 523
    .end local v5    # "e":Lorg/icepdf/core/tag/query/ParseException;
    :cond_1
    const/4 v12, 0x0

    .line 525
    .local v12, "state":Lorg/icepdf/core/tag/TagState;
    const/4 v7, 0x0

    .line 526
    .local v7, "fis":Ljava/io/FileInputStream;
    const/4 v1, 0x0

    .line 527
    .local v1, "bis":Ljava/io/BufferedInputStream;
    const/4 v13, 0x0

    .line 531
    .local v13, "tagInput":Ljava/io/ObjectInputStream;
    :try_start_1
    new-instance v8, Ljava/io/FileInputStream;

    move-object/from16 v0, p0

    invoke-direct {v8, v0}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/ClassNotFoundException; {:try_start_1 .. :try_end_1} :catch_5
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 532
    .end local v7    # "fis":Ljava/io/FileInputStream;
    .local v8, "fis":Ljava/io/FileInputStream;
    :try_start_2
    new-instance v2, Ljava/io/BufferedInputStream;

    const/16 v15, 0x4000

    invoke-direct {v2, v8, v15}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;I)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_12
    .catch Ljava/lang/ClassNotFoundException; {:try_start_2 .. :try_end_2} :catch_f
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 533
    .end local v1    # "bis":Ljava/io/BufferedInputStream;
    .local v2, "bis":Ljava/io/BufferedInputStream;
    :try_start_3
    new-instance v14, Ljava/io/ObjectInputStream;

    invoke-direct {v14, v2}, Ljava/io/ObjectInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_13
    .catch Ljava/lang/ClassNotFoundException; {:try_start_3 .. :try_end_3} :catch_10
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 534
    .end local v13    # "tagInput":Ljava/io/ObjectInputStream;
    .local v14, "tagInput":Ljava/io/ObjectInputStream;
    :try_start_4
    invoke-virtual {v14}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v12

    .end local v12    # "state":Lorg/icepdf/core/tag/TagState;
    check-cast v12, Lorg/icepdf/core/tag/TagState;
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_14
    .catch Ljava/lang/ClassNotFoundException; {:try_start_4 .. :try_end_4} :catch_11
    .catchall {:try_start_4 .. :try_end_4} :catchall_3

    .line 547
    .restart local v12    # "state":Lorg/icepdf/core/tag/TagState;
    if-eqz v8, :cond_2

    .line 548
    :try_start_5
    invoke-virtual {v8}, Ljava/io/FileInputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1

    .line 554
    :cond_2
    :goto_1
    if-eqz v2, :cond_3

    .line 555
    :try_start_6
    invoke-virtual {v2}, Ljava/io/BufferedInputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_9

    .line 563
    :cond_3
    :goto_2
    if-eqz v14, :cond_4

    .line 564
    :try_start_7
    invoke-virtual {v14}, Ljava/io/ObjectInputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_a

    .line 570
    :cond_4
    :goto_3
    invoke-static {v12, v6}, Lorg/icepdf/core/tag/query/Querior;->search(Lorg/icepdf/core/tag/TagState;Lorg/icepdf/core/tag/query/Expression;)Ljava/util/List;

    move-result-object v4

    .line 571
    .local v4, "documentResults":Ljava/util/List;
    if-eqz v4, :cond_0

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v15

    if-eqz v15, :cond_0

    .line 575
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_4
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v15

    if-ge v9, v15, :cond_0

    .line 576
    invoke-interface {v4, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/icepdf/core/tag/query/DocumentResult;

    .line 578
    .local v3, "docRes":Lorg/icepdf/core/tag/query/DocumentResult;
    invoke-virtual {v3}, Lorg/icepdf/core/tag/query/DocumentResult;->getImages()Ljava/util/List;

    move-result-object v10

    .line 580
    .local v10, "images":Ljava/util/List;
    const/4 v11, 0x0

    .local v11, "j":I
    :goto_5
    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v15

    if-ge v11, v15, :cond_c

    .line 581
    invoke-interface {v10, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lorg/icepdf/core/tag/TaggedImage;

    .line 580
    add-int/lit8 v11, v11, 0x1

    goto :goto_5

    .line 549
    .end local v3    # "docRes":Lorg/icepdf/core/tag/query/DocumentResult;
    .end local v4    # "documentResults":Ljava/util/List;
    .end local v9    # "i":I
    .end local v10    # "images":Ljava/util/List;
    .end local v11    # "j":I
    :catch_1
    move-exception v5

    .line 550
    .local v5, "e":Ljava/io/IOException;
    const-string/jumbo v15, "DocumentService"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v17, "Exception: "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual {v5}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 537
    .end local v2    # "bis":Ljava/io/BufferedInputStream;
    .end local v5    # "e":Ljava/io/IOException;
    .end local v8    # "fis":Ljava/io/FileInputStream;
    .end local v14    # "tagInput":Ljava/io/ObjectInputStream;
    .restart local v1    # "bis":Ljava/io/BufferedInputStream;
    .restart local v7    # "fis":Ljava/io/FileInputStream;
    .restart local v13    # "tagInput":Ljava/io/ObjectInputStream;
    :catch_2
    move-exception v5

    .line 538
    .end local v12    # "state":Lorg/icepdf/core/tag/TagState;
    .restart local v5    # "e":Ljava/io/IOException;
    :goto_6
    :try_start_8
    sget-object v15, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v17, "Problem reading from tagFile: "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 547
    if-eqz v7, :cond_5

    .line 548
    :try_start_9
    invoke-virtual {v7}, Ljava/io/FileInputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_4

    .line 554
    :cond_5
    :goto_7
    if-eqz v1, :cond_6

    .line 555
    :try_start_a
    invoke-virtual {v1}, Ljava/io/BufferedInputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_b

    .line 563
    :cond_6
    :goto_8
    if-eqz v13, :cond_0

    .line 564
    :try_start_b
    invoke-virtual {v13}, Ljava/io/ObjectInputStream;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_3

    goto/16 :goto_0

    .line 566
    :catch_3
    move-exception v15

    goto/16 :goto_0

    .line 549
    :catch_4
    move-exception v5

    .line 550
    const-string/jumbo v15, "DocumentService"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v17, "Exception: "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual {v5}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_7

    .line 541
    .end local v5    # "e":Ljava/io/IOException;
    .restart local v12    # "state":Lorg/icepdf/core/tag/TagState;
    :catch_5
    move-exception v5

    .line 542
    .end local v12    # "state":Lorg/icepdf/core/tag/TagState;
    .local v5, "e":Ljava/lang/ClassNotFoundException;
    :goto_9
    :try_start_c
    sget-object v15, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v17, "Problem reading TagState from tagFile: "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    .line 547
    if-eqz v7, :cond_7

    .line 548
    :try_start_d
    invoke-virtual {v7}, Ljava/io/FileInputStream;->close()V
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_7

    .line 554
    .end local v5    # "e":Ljava/lang/ClassNotFoundException;
    :cond_7
    :goto_a
    if-eqz v1, :cond_8

    .line 555
    :try_start_e
    invoke-virtual {v1}, Ljava/io/BufferedInputStream;->close()V
    :try_end_e
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_c

    .line 563
    :cond_8
    :goto_b
    if-eqz v13, :cond_0

    .line 564
    :try_start_f
    invoke-virtual {v13}, Ljava/io/ObjectInputStream;->close()V
    :try_end_f
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_f} :catch_6

    goto/16 :goto_0

    .line 566
    :catch_6
    move-exception v15

    goto/16 :goto_0

    .line 549
    .restart local v5    # "e":Ljava/lang/ClassNotFoundException;
    :catch_7
    move-exception v5

    .line 550
    .local v5, "e":Ljava/io/IOException;
    const-string/jumbo v15, "DocumentService"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v17, "Exception: "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual {v5}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_a

    .line 546
    .end local v5    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v15

    .line 547
    :goto_c
    if-eqz v7, :cond_9

    .line 548
    :try_start_10
    invoke-virtual {v7}, Ljava/io/FileInputStream;->close()V
    :try_end_10
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_10} :catch_8

    .line 554
    :cond_9
    :goto_d
    if-eqz v1, :cond_a

    .line 555
    :try_start_11
    invoke-virtual {v1}, Ljava/io/BufferedInputStream;->close()V
    :try_end_11
    .catch Ljava/io/IOException; {:try_start_11 .. :try_end_11} :catch_d

    .line 563
    :cond_a
    :goto_e
    if-eqz v13, :cond_b

    .line 564
    :try_start_12
    invoke-virtual {v13}, Ljava/io/ObjectInputStream;->close()V
    :try_end_12
    .catch Ljava/io/IOException; {:try_start_12 .. :try_end_12} :catch_e

    .line 568
    :cond_b
    :goto_f
    throw v15

    .line 549
    :catch_8
    move-exception v5

    .line 550
    .restart local v5    # "e":Ljava/io/IOException;
    const-string/jumbo v16, "DocumentService"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v18, "Exception: "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual {v5}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_d

    .line 575
    .end local v1    # "bis":Ljava/io/BufferedInputStream;
    .end local v5    # "e":Ljava/io/IOException;
    .end local v7    # "fis":Ljava/io/FileInputStream;
    .end local v13    # "tagInput":Ljava/io/ObjectInputStream;
    .restart local v2    # "bis":Ljava/io/BufferedInputStream;
    .restart local v3    # "docRes":Lorg/icepdf/core/tag/query/DocumentResult;
    .restart local v4    # "documentResults":Ljava/util/List;
    .restart local v8    # "fis":Ljava/io/FileInputStream;
    .restart local v9    # "i":I
    .restart local v10    # "images":Ljava/util/List;
    .restart local v11    # "j":I
    .restart local v12    # "state":Lorg/icepdf/core/tag/TagState;
    .restart local v14    # "tagInput":Ljava/io/ObjectInputStream;
    :cond_c
    add-int/lit8 v9, v9, 0x1

    goto/16 :goto_4

    .line 557
    .end local v3    # "docRes":Lorg/icepdf/core/tag/query/DocumentResult;
    .end local v4    # "documentResults":Ljava/util/List;
    .end local v9    # "i":I
    .end local v10    # "images":Ljava/util/List;
    .end local v11    # "j":I
    :catch_9
    move-exception v15

    goto/16 :goto_2

    .line 566
    :catch_a
    move-exception v15

    goto/16 :goto_3

    .line 557
    .end local v2    # "bis":Ljava/io/BufferedInputStream;
    .end local v8    # "fis":Ljava/io/FileInputStream;
    .end local v12    # "state":Lorg/icepdf/core/tag/TagState;
    .end local v14    # "tagInput":Ljava/io/ObjectInputStream;
    .restart local v1    # "bis":Ljava/io/BufferedInputStream;
    .restart local v5    # "e":Ljava/io/IOException;
    .restart local v7    # "fis":Ljava/io/FileInputStream;
    .restart local v13    # "tagInput":Ljava/io/ObjectInputStream;
    :catch_b
    move-exception v15

    goto/16 :goto_8

    .end local v5    # "e":Ljava/io/IOException;
    :catch_c
    move-exception v15

    goto :goto_b

    :catch_d
    move-exception v16

    goto :goto_e

    .line 566
    :catch_e
    move-exception v16

    goto :goto_f

    .line 546
    .end local v7    # "fis":Ljava/io/FileInputStream;
    .restart local v8    # "fis":Ljava/io/FileInputStream;
    .restart local v12    # "state":Lorg/icepdf/core/tag/TagState;
    :catchall_1
    move-exception v15

    move-object v7, v8

    .end local v8    # "fis":Ljava/io/FileInputStream;
    .restart local v7    # "fis":Ljava/io/FileInputStream;
    goto :goto_c

    .end local v1    # "bis":Ljava/io/BufferedInputStream;
    .end local v7    # "fis":Ljava/io/FileInputStream;
    .restart local v2    # "bis":Ljava/io/BufferedInputStream;
    .restart local v8    # "fis":Ljava/io/FileInputStream;
    :catchall_2
    move-exception v15

    move-object v1, v2

    .end local v2    # "bis":Ljava/io/BufferedInputStream;
    .restart local v1    # "bis":Ljava/io/BufferedInputStream;
    move-object v7, v8

    .end local v8    # "fis":Ljava/io/FileInputStream;
    .restart local v7    # "fis":Ljava/io/FileInputStream;
    goto :goto_c

    .end local v1    # "bis":Ljava/io/BufferedInputStream;
    .end local v7    # "fis":Ljava/io/FileInputStream;
    .end local v12    # "state":Lorg/icepdf/core/tag/TagState;
    .end local v13    # "tagInput":Ljava/io/ObjectInputStream;
    .restart local v2    # "bis":Ljava/io/BufferedInputStream;
    .restart local v8    # "fis":Ljava/io/FileInputStream;
    .restart local v14    # "tagInput":Ljava/io/ObjectInputStream;
    :catchall_3
    move-exception v15

    move-object v13, v14

    .end local v14    # "tagInput":Ljava/io/ObjectInputStream;
    .restart local v13    # "tagInput":Ljava/io/ObjectInputStream;
    move-object v1, v2

    .end local v2    # "bis":Ljava/io/BufferedInputStream;
    .restart local v1    # "bis":Ljava/io/BufferedInputStream;
    move-object v7, v8

    .end local v8    # "fis":Ljava/io/FileInputStream;
    .restart local v7    # "fis":Ljava/io/FileInputStream;
    goto :goto_c

    .line 541
    .end local v7    # "fis":Ljava/io/FileInputStream;
    .restart local v8    # "fis":Ljava/io/FileInputStream;
    .restart local v12    # "state":Lorg/icepdf/core/tag/TagState;
    :catch_f
    move-exception v5

    move-object v7, v8

    .end local v8    # "fis":Ljava/io/FileInputStream;
    .restart local v7    # "fis":Ljava/io/FileInputStream;
    goto/16 :goto_9

    .end local v1    # "bis":Ljava/io/BufferedInputStream;
    .end local v7    # "fis":Ljava/io/FileInputStream;
    .restart local v2    # "bis":Ljava/io/BufferedInputStream;
    .restart local v8    # "fis":Ljava/io/FileInputStream;
    :catch_10
    move-exception v5

    move-object v1, v2

    .end local v2    # "bis":Ljava/io/BufferedInputStream;
    .restart local v1    # "bis":Ljava/io/BufferedInputStream;
    move-object v7, v8

    .end local v8    # "fis":Ljava/io/FileInputStream;
    .restart local v7    # "fis":Ljava/io/FileInputStream;
    goto/16 :goto_9

    .end local v1    # "bis":Ljava/io/BufferedInputStream;
    .end local v7    # "fis":Ljava/io/FileInputStream;
    .end local v12    # "state":Lorg/icepdf/core/tag/TagState;
    .end local v13    # "tagInput":Ljava/io/ObjectInputStream;
    .restart local v2    # "bis":Ljava/io/BufferedInputStream;
    .restart local v8    # "fis":Ljava/io/FileInputStream;
    .restart local v14    # "tagInput":Ljava/io/ObjectInputStream;
    :catch_11
    move-exception v5

    move-object v13, v14

    .end local v14    # "tagInput":Ljava/io/ObjectInputStream;
    .restart local v13    # "tagInput":Ljava/io/ObjectInputStream;
    move-object v1, v2

    .end local v2    # "bis":Ljava/io/BufferedInputStream;
    .restart local v1    # "bis":Ljava/io/BufferedInputStream;
    move-object v7, v8

    .end local v8    # "fis":Ljava/io/FileInputStream;
    .restart local v7    # "fis":Ljava/io/FileInputStream;
    goto/16 :goto_9

    .line 537
    .end local v7    # "fis":Ljava/io/FileInputStream;
    .restart local v8    # "fis":Ljava/io/FileInputStream;
    .restart local v12    # "state":Lorg/icepdf/core/tag/TagState;
    :catch_12
    move-exception v5

    move-object v7, v8

    .end local v8    # "fis":Ljava/io/FileInputStream;
    .restart local v7    # "fis":Ljava/io/FileInputStream;
    goto/16 :goto_6

    .end local v1    # "bis":Ljava/io/BufferedInputStream;
    .end local v7    # "fis":Ljava/io/FileInputStream;
    .restart local v2    # "bis":Ljava/io/BufferedInputStream;
    .restart local v8    # "fis":Ljava/io/FileInputStream;
    :catch_13
    move-exception v5

    move-object v1, v2

    .end local v2    # "bis":Ljava/io/BufferedInputStream;
    .restart local v1    # "bis":Ljava/io/BufferedInputStream;
    move-object v7, v8

    .end local v8    # "fis":Ljava/io/FileInputStream;
    .restart local v7    # "fis":Ljava/io/FileInputStream;
    goto/16 :goto_6

    .end local v1    # "bis":Ljava/io/BufferedInputStream;
    .end local v7    # "fis":Ljava/io/FileInputStream;
    .end local v12    # "state":Lorg/icepdf/core/tag/TagState;
    .end local v13    # "tagInput":Ljava/io/ObjectInputStream;
    .restart local v2    # "bis":Ljava/io/BufferedInputStream;
    .restart local v8    # "fis":Ljava/io/FileInputStream;
    .restart local v14    # "tagInput":Ljava/io/ObjectInputStream;
    :catch_14
    move-exception v5

    move-object v13, v14

    .end local v14    # "tagInput":Ljava/io/ObjectInputStream;
    .restart local v13    # "tagInput":Ljava/io/ObjectInputStream;
    move-object v1, v2

    .end local v2    # "bis":Ljava/io/BufferedInputStream;
    .restart local v1    # "bis":Ljava/io/BufferedInputStream;
    move-object v7, v8

    .end local v8    # "fis":Ljava/io/FileInputStream;
    .restart local v7    # "fis":Ljava/io/FileInputStream;
    goto/16 :goto_6
.end method

.method private static recursivelyCatalogPDFs(Ljava/util/ArrayList;Ljava/io/File;)V
    .locals 6
    .param p1, "directory"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/io/File;",
            ">;",
            "Ljava/io/File;",
            ")V"
        }
    .end annotation

    .prologue
    .line 588
    .local p0, "allFiles":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/io/File;>;"
    invoke-virtual {p1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v0

    .line 589
    .local v0, "children":[Ljava/io/File;
    if-eqz v0, :cond_4

    array-length v5, v0

    if-lez v5, :cond_4

    .line 590
    new-instance v1, Ljava/util/ArrayList;

    array-length v5, v0

    invoke-direct {v1, v5}, Ljava/util/ArrayList;-><init>(I)V

    .line 591
    .local v1, "directories":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/io/File;>;"
    new-instance v2, Ljava/util/ArrayList;

    array-length v5, v0

    invoke-direct {v2, v5}, Ljava/util/ArrayList;-><init>(I)V

    .line 592
    .local v2, "files":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/io/File;>;"
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    array-length v5, v0

    if-ge v3, v5, :cond_2

    .line 593
    aget-object v5, v0, v3

    invoke-virtual {v5}, Ljava/io/File;->isDirectory()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 594
    aget-object v5, v0, v3

    invoke-virtual {v5}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v4

    .line 595
    .local v4, "name":Ljava/lang/String;
    const-string/jumbo v5, "."

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 596
    aget-object v5, v0, v3

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 592
    .end local v4    # "name":Ljava/lang/String;
    :cond_0
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 599
    :cond_1
    aget-object v5, v0, v3

    invoke-virtual {v5}, Ljava/io/File;->isFile()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 600
    aget-object v5, v0, v3

    invoke-static {v2, v5}, Lorg/icepdf/core/tag/TagUtil;->addFileIfIsPDF(Ljava/util/List;Ljava/io/File;)V

    goto :goto_1

    .line 603
    :cond_2
    invoke-static {v1}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 604
    invoke-static {v2}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 605
    const/4 v3, 0x0

    :goto_2
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-ge v3, v5, :cond_3

    .line 607
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/io/File;

    invoke-static {p0, v5}, Lorg/icepdf/core/tag/TagUtil;->recursivelyCatalogPDFs(Ljava/util/ArrayList;Ljava/io/File;)V

    .line 605
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 609
    :cond_3
    invoke-virtual {p0, v2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 611
    .end local v1    # "directories":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/io/File;>;"
    .end local v2    # "files":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/io/File;>;"
    .end local v3    # "i":I
    :cond_4
    return-void
.end method

.method public static tag(Ljava/lang/String;Ljava/lang/String;)V
    .locals 30
    .param p0, "catalogFile"    # Ljava/lang/String;
    .param p1, "tagFile"    # Ljava/lang/String;

    .prologue
    .line 340
    const/16 v22, 0x0

    .line 342
    .local v22, "tagOutput":Ljava/io/ObjectOutputStream;
    const/4 v13, 0x0

    .line 343
    .local v13, "fos":Ljava/io/FileOutputStream;
    const/4 v5, 0x0

    .line 345
    .local v5, "bos":Ljava/io/BufferedOutputStream;
    :try_start_0
    new-instance v14, Ljava/io/FileOutputStream;

    move-object/from16 v0, p1

    invoke-direct {v14, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 346
    .end local v13    # "fos":Ljava/io/FileOutputStream;
    .local v14, "fos":Ljava/io/FileOutputStream;
    :try_start_1
    new-instance v6, Ljava/io/BufferedOutputStream;

    const/16 v26, 0x4000

    move/from16 v0, v26

    invoke-direct {v6, v14, v0}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;I)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_25

    .line 347
    .end local v5    # "bos":Ljava/io/BufferedOutputStream;
    .local v6, "bos":Ljava/io/BufferedOutputStream;
    :try_start_2
    new-instance v23, Ljava/io/ObjectOutputStream;

    move-object/from16 v0, v23

    invoke-direct {v0, v6}, Ljava/io/ObjectOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_26

    .end local v22    # "tagOutput":Ljava/io/ObjectOutputStream;
    .local v23, "tagOutput":Ljava/io/ObjectOutputStream;
    move-object v5, v6

    .end local v6    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v5    # "bos":Ljava/io/BufferedOutputStream;
    move-object v13, v14

    .end local v14    # "fos":Ljava/io/FileOutputStream;
    .restart local v13    # "fos":Ljava/io/FileOutputStream;
    move-object/from16 v22, v23

    .line 353
    .end local v23    # "tagOutput":Ljava/io/ObjectOutputStream;
    .restart local v22    # "tagOutput":Ljava/io/ObjectOutputStream;
    :goto_0
    const/4 v2, 0x0

    .line 355
    .local v2, "allFiles":Ljava/util/ArrayList;
    const/4 v11, 0x0

    .line 356
    .local v11, "fis":Ljava/io/FileInputStream;
    const/4 v3, 0x0

    .line 357
    .local v3, "bis":Ljava/io/BufferedInputStream;
    const/16 v18, 0x0

    .line 359
    .local v18, "ois":Ljava/io/ObjectInputStream;
    :try_start_3
    new-instance v12, Ljava/io/FileInputStream;

    move-object/from16 v0, p0

    invoke-direct {v12, v0}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_4
    .catch Ljava/lang/ClassNotFoundException; {:try_start_3 .. :try_end_3} :catch_b
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 360
    .end local v11    # "fis":Ljava/io/FileInputStream;
    .local v12, "fis":Ljava/io/FileInputStream;
    :try_start_4
    new-instance v4, Ljava/io/BufferedInputStream;

    const/16 v26, 0x4000

    move/from16 v0, v26

    invoke-direct {v4, v12, v0}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;I)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_22
    .catch Ljava/lang/ClassNotFoundException; {:try_start_4 .. :try_end_4} :catch_1f
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 361
    .end local v3    # "bis":Ljava/io/BufferedInputStream;
    .local v4, "bis":Ljava/io/BufferedInputStream;
    :try_start_5
    new-instance v19, Ljava/io/ObjectInputStream;

    move-object/from16 v0, v19

    invoke-direct {v0, v4}, Ljava/io/ObjectInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_23
    .catch Ljava/lang/ClassNotFoundException; {:try_start_5 .. :try_end_5} :catch_20
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    .line 362
    .end local v18    # "ois":Ljava/io/ObjectInputStream;
    .local v19, "ois":Ljava/io/ObjectInputStream;
    :try_start_6
    invoke-virtual/range {v19 .. v19}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v26

    move-object/from16 v0, v26

    check-cast v0, Ljava/util/ArrayList;

    move-object v2, v0

    .line 363
    invoke-virtual/range {v19 .. v19}, Ljava/io/ObjectInputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_24
    .catch Ljava/lang/ClassNotFoundException; {:try_start_6 .. :try_end_6} :catch_21
    .catchall {:try_start_6 .. :try_end_6} :catchall_4

    .line 429
    if-eqz v12, :cond_0

    .line 430
    :try_start_7
    invoke-virtual {v12}, Ljava/io/FileInputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_1

    .line 436
    :cond_0
    :goto_1
    if-eqz v4, :cond_1

    .line 437
    :try_start_8
    invoke-virtual {v4}, Ljava/io/BufferedInputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_2

    .line 442
    :cond_1
    :goto_2
    if-eqz v19, :cond_2

    .line 443
    :try_start_9
    invoke-virtual/range {v19 .. v19}, Ljava/io/ObjectInputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_3

    .line 451
    :cond_2
    :goto_3
    const-wide/16 v24, 0x0

    .line 452
    .local v24, "then":J
    const/4 v15, 0x0

    .local v15, "i":I
    :goto_4
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v26

    move/from16 v0, v26

    if-ge v15, v0, :cond_12

    .line 453
    invoke-virtual {v2, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/io/File;

    .line 454
    .local v10, "file":Ljava/io/File;
    invoke-virtual {v10}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v20

    .line 455
    .local v20, "path":Ljava/lang/String;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v16

    .line 456
    .local v16, "now":J
    sub-long v26, v16, v24

    const-wide/16 v28, 0x1388

    cmp-long v26, v26, v28

    if-ltz v26, :cond_3

    .line 457
    move-wide/from16 v24, v16

    .line 462
    :cond_3
    invoke-static/range {v20 .. v20}, Lorg/icepdf/core/tag/TagUtil;->tagPdf(Ljava/lang/String;)V

    .line 452
    add-int/lit8 v15, v15, 0x1

    goto :goto_4

    .line 349
    .end local v2    # "allFiles":Ljava/util/ArrayList;
    .end local v4    # "bis":Ljava/io/BufferedInputStream;
    .end local v10    # "file":Ljava/io/File;
    .end local v12    # "fis":Ljava/io/FileInputStream;
    .end local v15    # "i":I
    .end local v16    # "now":J
    .end local v19    # "ois":Ljava/io/ObjectInputStream;
    .end local v20    # "path":Ljava/lang/String;
    .end local v24    # "then":J
    :catch_0
    move-exception v7

    .line 350
    .local v7, "e":Ljava/io/IOException;
    :goto_5
    sget-object v26, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v28, "Problem openning tagFile: "

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, v27

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0

    .line 431
    .end local v7    # "e":Ljava/io/IOException;
    .restart local v2    # "allFiles":Ljava/util/ArrayList;
    .restart local v4    # "bis":Ljava/io/BufferedInputStream;
    .restart local v12    # "fis":Ljava/io/FileInputStream;
    .restart local v19    # "ois":Ljava/io/ObjectInputStream;
    :catch_1
    move-exception v7

    .line 432
    .restart local v7    # "e":Ljava/io/IOException;
    const-string/jumbo v26, "DocumentService"

    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v28, "Exception: "

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual {v7}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    invoke-static/range {v26 .. v27}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 438
    .end local v7    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v7

    .line 439
    .restart local v7    # "e":Ljava/io/IOException;
    const-string/jumbo v26, "DocumentService"

    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v28, "Exception: "

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual {v7}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    invoke-static/range {v26 .. v27}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    .line 444
    .end local v7    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v7

    .line 445
    .restart local v7    # "e":Ljava/io/IOException;
    const-string/jumbo v26, "DocumentService"

    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v28, "Exception: "

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual {v7}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    invoke-static/range {v26 .. v27}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    .line 365
    .end local v4    # "bis":Ljava/io/BufferedInputStream;
    .end local v7    # "e":Ljava/io/IOException;
    .end local v12    # "fis":Ljava/io/FileInputStream;
    .end local v19    # "ois":Ljava/io/ObjectInputStream;
    .restart local v3    # "bis":Ljava/io/BufferedInputStream;
    .restart local v11    # "fis":Ljava/io/FileInputStream;
    .restart local v18    # "ois":Ljava/io/ObjectInputStream;
    :catch_4
    move-exception v7

    .line 366
    .restart local v7    # "e":Ljava/io/IOException;
    :goto_6
    :try_start_a
    sget-object v26, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v28, "Problem openning catalogFile: "

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, v27

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    .line 367
    if-eqz v22, :cond_4

    .line 369
    :try_start_b
    invoke-virtual/range {v22 .. v22}, Ljava/io/ObjectOutputStream;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_5
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    .line 370
    const/16 v22, 0x0

    .line 378
    :cond_4
    :goto_7
    if-eqz v13, :cond_5

    .line 379
    :try_start_c
    invoke-virtual {v13}, Ljava/io/FileOutputStream;->close()V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_6
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    .line 380
    :cond_5
    const/4 v13, 0x0

    .line 387
    :goto_8
    if-eqz v5, :cond_6

    .line 388
    :try_start_d
    invoke-virtual {v5}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_7
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    .line 389
    :cond_6
    const/4 v5, 0x0

    .line 429
    :goto_9
    if-eqz v11, :cond_7

    .line 430
    :try_start_e
    invoke-virtual {v11}, Ljava/io/FileInputStream;->close()V
    :try_end_e
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_8

    .line 436
    :cond_7
    :goto_a
    if-eqz v3, :cond_8

    .line 437
    :try_start_f
    invoke-virtual {v3}, Ljava/io/BufferedInputStream;->close()V
    :try_end_f
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_f} :catch_9

    .line 442
    :cond_8
    :goto_b
    if-eqz v18, :cond_9

    .line 443
    :try_start_10
    invoke-virtual/range {v18 .. v18}, Ljava/io/ObjectInputStream;->close()V
    :try_end_10
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_10} :catch_a

    .line 510
    .end local v7    # "e":Ljava/io/IOException;
    :cond_9
    :goto_c
    return-void

    .line 371
    .restart local v7    # "e":Ljava/io/IOException;
    :catch_5
    move-exception v8

    .line 372
    .local v8, "e1":Ljava/io/IOException;
    :try_start_11
    const-string/jumbo v26, "DocumentService"

    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v28, "Exception: "

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual {v7}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    invoke-static/range {v26 .. v27}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_0

    goto :goto_7

    .line 428
    .end local v7    # "e":Ljava/io/IOException;
    .end local v8    # "e1":Ljava/io/IOException;
    :catchall_0
    move-exception v26

    .line 429
    :goto_d
    if-eqz v11, :cond_a

    .line 430
    :try_start_12
    invoke-virtual {v11}, Ljava/io/FileInputStream;->close()V
    :try_end_12
    .catch Ljava/io/IOException; {:try_start_12 .. :try_end_12} :catch_12

    .line 436
    :cond_a
    :goto_e
    if-eqz v3, :cond_b

    .line 437
    :try_start_13
    invoke-virtual {v3}, Ljava/io/BufferedInputStream;->close()V
    :try_end_13
    .catch Ljava/io/IOException; {:try_start_13 .. :try_end_13} :catch_13

    .line 442
    :cond_b
    :goto_f
    if-eqz v18, :cond_c

    .line 443
    :try_start_14
    invoke-virtual/range {v18 .. v18}, Ljava/io/ObjectInputStream;->close()V
    :try_end_14
    .catch Ljava/io/IOException; {:try_start_14 .. :try_end_14} :catch_14

    .line 446
    :cond_c
    :goto_10
    throw v26

    .line 382
    .restart local v7    # "e":Ljava/io/IOException;
    :catch_6
    move-exception v9

    .line 383
    .local v9, "ec":Ljava/io/IOException;
    :try_start_15
    const-string/jumbo v26, "DocumentService"

    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v28, "Exception: "

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual {v7}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    invoke-static/range {v26 .. v27}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_8

    .line 391
    .end local v9    # "ec":Ljava/io/IOException;
    :catch_7
    move-exception v9

    .line 392
    .restart local v9    # "ec":Ljava/io/IOException;
    const-string/jumbo v26, "DocumentService"

    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v28, "Exception: "

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual {v7}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    invoke-static/range {v26 .. v27}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_15
    .catchall {:try_start_15 .. :try_end_15} :catchall_0

    goto :goto_9

    .line 431
    .end local v9    # "ec":Ljava/io/IOException;
    :catch_8
    move-exception v7

    .line 432
    const-string/jumbo v26, "DocumentService"

    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v28, "Exception: "

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual {v7}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    invoke-static/range {v26 .. v27}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_a

    .line 438
    :catch_9
    move-exception v7

    .line 439
    const-string/jumbo v26, "DocumentService"

    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v28, "Exception: "

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual {v7}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    invoke-static/range {v26 .. v27}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_b

    .line 444
    :catch_a
    move-exception v7

    .line 445
    const-string/jumbo v26, "DocumentService"

    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v28, "Exception: "

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual {v7}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    invoke-static/range {v26 .. v27}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_c

    .line 396
    .end local v7    # "e":Ljava/io/IOException;
    :catch_b
    move-exception v7

    .line 397
    .local v7, "e":Ljava/lang/ClassNotFoundException;
    :goto_11
    :try_start_16
    sget-object v26, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v28, "Problem reading catalog from ["

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, v27

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    const-string/jumbo v28, "]: "

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, v27

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_16
    .catchall {:try_start_16 .. :try_end_16} :catchall_0

    .line 398
    if-eqz v22, :cond_d

    .line 400
    :try_start_17
    invoke-virtual/range {v22 .. v22}, Ljava/io/ObjectOutputStream;->close()V
    :try_end_17
    .catch Ljava/io/IOException; {:try_start_17 .. :try_end_17} :catch_d
    .catchall {:try_start_17 .. :try_end_17} :catchall_0

    .line 401
    const/16 v22, 0x0

    .line 408
    :cond_d
    :goto_12
    if-eqz v13, :cond_e

    .line 409
    :try_start_18
    invoke-virtual {v13}, Ljava/io/FileOutputStream;->close()V
    :try_end_18
    .catch Ljava/io/IOException; {:try_start_18 .. :try_end_18} :catch_e
    .catchall {:try_start_18 .. :try_end_18} :catchall_0

    .line 410
    :cond_e
    const/4 v13, 0x0

    .line 417
    :goto_13
    if-eqz v5, :cond_f

    .line 418
    :try_start_19
    invoke-virtual {v5}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_19
    .catch Ljava/io/IOException; {:try_start_19 .. :try_end_19} :catch_f
    .catchall {:try_start_19 .. :try_end_19} :catchall_0

    .line 419
    :cond_f
    const/4 v5, 0x0

    .line 429
    :goto_14
    if-eqz v11, :cond_10

    .line 430
    :try_start_1a
    invoke-virtual {v11}, Ljava/io/FileInputStream;->close()V
    :try_end_1a
    .catch Ljava/io/IOException; {:try_start_1a .. :try_end_1a} :catch_10

    .line 436
    .end local v7    # "e":Ljava/lang/ClassNotFoundException;
    :cond_10
    :goto_15
    if-eqz v3, :cond_11

    .line 437
    :try_start_1b
    invoke-virtual {v3}, Ljava/io/BufferedInputStream;->close()V
    :try_end_1b
    .catch Ljava/io/IOException; {:try_start_1b .. :try_end_1b} :catch_11

    .line 442
    :cond_11
    :goto_16
    if-eqz v18, :cond_9

    .line 443
    :try_start_1c
    invoke-virtual/range {v18 .. v18}, Ljava/io/ObjectInputStream;->close()V
    :try_end_1c
    .catch Ljava/io/IOException; {:try_start_1c .. :try_end_1c} :catch_c

    goto/16 :goto_c

    .line 444
    :catch_c
    move-exception v7

    .line 445
    .local v7, "e":Ljava/io/IOException;
    const-string/jumbo v26, "DocumentService"

    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v28, "Exception: "

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual {v7}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    invoke-static/range {v26 .. v27}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_c

    .line 402
    .local v7, "e":Ljava/lang/ClassNotFoundException;
    :catch_d
    move-exception v8

    .line 403
    .restart local v8    # "e1":Ljava/io/IOException;
    :try_start_1d
    const-string/jumbo v26, "DocumentService"

    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v28, "Exception: "

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual {v8}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    invoke-static/range {v26 .. v27}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_12

    .line 412
    .end local v8    # "e1":Ljava/io/IOException;
    :catch_e
    move-exception v9

    .line 413
    .restart local v9    # "ec":Ljava/io/IOException;
    const-string/jumbo v26, "DocumentService"

    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v28, "Exception: "

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual {v9}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    invoke-static/range {v26 .. v27}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_13

    .line 421
    .end local v9    # "ec":Ljava/io/IOException;
    :catch_f
    move-exception v9

    .line 422
    .restart local v9    # "ec":Ljava/io/IOException;
    const-string/jumbo v26, "DocumentService"

    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v28, "Exception: "

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual {v9}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    invoke-static/range {v26 .. v27}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1d
    .catchall {:try_start_1d .. :try_end_1d} :catchall_0

    goto/16 :goto_14

    .line 431
    .end local v9    # "ec":Ljava/io/IOException;
    :catch_10
    move-exception v7

    .line 432
    .local v7, "e":Ljava/io/IOException;
    const-string/jumbo v26, "DocumentService"

    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v28, "Exception: "

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual {v7}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    invoke-static/range {v26 .. v27}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_15

    .line 438
    .end local v7    # "e":Ljava/io/IOException;
    :catch_11
    move-exception v7

    .line 439
    .restart local v7    # "e":Ljava/io/IOException;
    const-string/jumbo v26, "DocumentService"

    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v28, "Exception: "

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual {v7}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    invoke-static/range {v26 .. v27}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_16

    .line 431
    .end local v7    # "e":Ljava/io/IOException;
    :catch_12
    move-exception v7

    .line 432
    .restart local v7    # "e":Ljava/io/IOException;
    const-string/jumbo v27, "DocumentService"

    new-instance v28, Ljava/lang/StringBuilder;

    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v29, "Exception: "

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual {v7}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v29

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    invoke-static/range {v27 .. v28}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_e

    .line 438
    .end local v7    # "e":Ljava/io/IOException;
    :catch_13
    move-exception v7

    .line 439
    .restart local v7    # "e":Ljava/io/IOException;
    const-string/jumbo v27, "DocumentService"

    new-instance v28, Ljava/lang/StringBuilder;

    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v29, "Exception: "

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual {v7}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v29

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    invoke-static/range {v27 .. v28}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_f

    .line 444
    .end local v7    # "e":Ljava/io/IOException;
    :catch_14
    move-exception v7

    .line 445
    .restart local v7    # "e":Ljava/io/IOException;
    const-string/jumbo v27, "DocumentService"

    new-instance v28, Ljava/lang/StringBuilder;

    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v29, "Exception: "

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual {v7}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v29

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    invoke-static/range {v27 .. v28}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_10

    .line 471
    .end local v3    # "bis":Ljava/io/BufferedInputStream;
    .end local v7    # "e":Ljava/io/IOException;
    .end local v11    # "fis":Ljava/io/FileInputStream;
    .end local v18    # "ois":Ljava/io/ObjectInputStream;
    .restart local v4    # "bis":Ljava/io/BufferedInputStream;
    .restart local v12    # "fis":Ljava/io/FileInputStream;
    .restart local v15    # "i":I
    .restart local v19    # "ois":Ljava/io/ObjectInputStream;
    .restart local v24    # "then":J
    :cond_12
    :try_start_1e
    invoke-static {}, Lorg/icepdf/core/tag/Tagger;->getTagState()Lorg/icepdf/core/tag/TagState;

    move-result-object v21

    .line 472
    .local v21, "state":Lorg/icepdf/core/tag/TagState;
    move-object/from16 v0, v22

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 473
    invoke-virtual/range {v22 .. v22}, Ljava/io/ObjectOutputStream;->flush()V
    :try_end_1e
    .catch Ljava/io/IOException; {:try_start_1e .. :try_end_1e} :catch_18
    .catchall {:try_start_1e .. :try_end_1e} :catchall_1

    .line 482
    if-eqz v22, :cond_13

    .line 483
    :try_start_1f
    invoke-virtual/range {v22 .. v22}, Ljava/io/ObjectOutputStream;->close()V
    :try_end_1f
    .catch Ljava/io/IOException; {:try_start_1f .. :try_end_1f} :catch_15

    .line 484
    const/16 v22, 0x0

    .line 492
    :cond_13
    :goto_17
    if-eqz v13, :cond_14

    .line 493
    :try_start_20
    invoke-virtual {v13}, Ljava/io/FileOutputStream;->close()V
    :try_end_20
    .catch Ljava/io/IOException; {:try_start_20 .. :try_end_20} :catch_16

    .line 494
    :cond_14
    const/4 v13, 0x0

    .line 501
    :goto_18
    if-eqz v5, :cond_15

    .line 502
    :try_start_21
    invoke-virtual {v5}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_21
    .catch Ljava/io/IOException; {:try_start_21 .. :try_end_21} :catch_17

    .line 503
    :cond_15
    const/4 v5, 0x0

    .end local v21    # "state":Lorg/icepdf/core/tag/TagState;
    :goto_19
    move-object/from16 v18, v19

    .end local v19    # "ois":Ljava/io/ObjectInputStream;
    .restart local v18    # "ois":Ljava/io/ObjectInputStream;
    move-object v3, v4

    .end local v4    # "bis":Ljava/io/BufferedInputStream;
    .restart local v3    # "bis":Ljava/io/BufferedInputStream;
    move-object v11, v12

    .line 510
    .end local v12    # "fis":Ljava/io/FileInputStream;
    .restart local v11    # "fis":Ljava/io/FileInputStream;
    goto/16 :goto_c

    .line 486
    .end local v3    # "bis":Ljava/io/BufferedInputStream;
    .end local v11    # "fis":Ljava/io/FileInputStream;
    .end local v18    # "ois":Ljava/io/ObjectInputStream;
    .restart local v4    # "bis":Ljava/io/BufferedInputStream;
    .restart local v12    # "fis":Ljava/io/FileInputStream;
    .restart local v19    # "ois":Ljava/io/ObjectInputStream;
    .restart local v21    # "state":Lorg/icepdf/core/tag/TagState;
    :catch_15
    move-exception v7

    .line 487
    .restart local v7    # "e":Ljava/io/IOException;
    const-string/jumbo v26, "DocumentService"

    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v28, "Exception: "

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual {v7}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    invoke-static/range {v26 .. v27}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_17

    .line 496
    .end local v7    # "e":Ljava/io/IOException;
    :catch_16
    move-exception v7

    .line 497
    .restart local v7    # "e":Ljava/io/IOException;
    const-string/jumbo v26, "DocumentService"

    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v28, "Exception: "

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual {v7}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    invoke-static/range {v26 .. v27}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_18

    .line 505
    .end local v7    # "e":Ljava/io/IOException;
    :catch_17
    move-exception v7

    .line 506
    .restart local v7    # "e":Ljava/io/IOException;
    const-string/jumbo v26, "DocumentService"

    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v28, "Exception: "

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual {v7}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    invoke-static/range {v26 .. v27}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_19

    .line 476
    .end local v7    # "e":Ljava/io/IOException;
    .end local v21    # "state":Lorg/icepdf/core/tag/TagState;
    :catch_18
    move-exception v7

    .line 477
    .restart local v7    # "e":Ljava/io/IOException;
    :try_start_22
    sget-object v26, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v28, "Problem saving tags: "

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, v27

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_22
    .catchall {:try_start_22 .. :try_end_22} :catchall_1

    .line 482
    if-eqz v22, :cond_16

    .line 483
    :try_start_23
    invoke-virtual/range {v22 .. v22}, Ljava/io/ObjectOutputStream;->close()V
    :try_end_23
    .catch Ljava/io/IOException; {:try_start_23 .. :try_end_23} :catch_19

    .line 484
    const/16 v22, 0x0

    .line 492
    :cond_16
    :goto_1a
    if-eqz v13, :cond_17

    .line 493
    :try_start_24
    invoke-virtual {v13}, Ljava/io/FileOutputStream;->close()V
    :try_end_24
    .catch Ljava/io/IOException; {:try_start_24 .. :try_end_24} :catch_1a

    .line 494
    :cond_17
    const/4 v13, 0x0

    .line 501
    :goto_1b
    if-eqz v5, :cond_18

    .line 502
    :try_start_25
    invoke-virtual {v5}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_25
    .catch Ljava/io/IOException; {:try_start_25 .. :try_end_25} :catch_1b

    .line 503
    :cond_18
    const/4 v5, 0x0

    goto/16 :goto_19

    .line 486
    :catch_19
    move-exception v7

    .line 487
    const-string/jumbo v26, "DocumentService"

    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v28, "Exception: "

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual {v7}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    invoke-static/range {v26 .. v27}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1a

    .line 496
    :catch_1a
    move-exception v7

    .line 497
    const-string/jumbo v26, "DocumentService"

    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v28, "Exception: "

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual {v7}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    invoke-static/range {v26 .. v27}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1b

    .line 505
    :catch_1b
    move-exception v7

    .line 506
    const-string/jumbo v26, "DocumentService"

    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v28, "Exception: "

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual {v7}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    invoke-static/range {v26 .. v27}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_19

    .line 481
    .end local v7    # "e":Ljava/io/IOException;
    :catchall_1
    move-exception v26

    .line 482
    if-eqz v22, :cond_19

    .line 483
    :try_start_26
    invoke-virtual/range {v22 .. v22}, Ljava/io/ObjectOutputStream;->close()V
    :try_end_26
    .catch Ljava/io/IOException; {:try_start_26 .. :try_end_26} :catch_1c

    .line 484
    const/16 v22, 0x0

    .line 492
    :cond_19
    :goto_1c
    if-eqz v13, :cond_1a

    .line 493
    :try_start_27
    invoke-virtual {v13}, Ljava/io/FileOutputStream;->close()V
    :try_end_27
    .catch Ljava/io/IOException; {:try_start_27 .. :try_end_27} :catch_1d

    .line 494
    :cond_1a
    const/4 v13, 0x0

    .line 501
    :goto_1d
    if-eqz v5, :cond_1b

    .line 502
    :try_start_28
    invoke-virtual {v5}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_28
    .catch Ljava/io/IOException; {:try_start_28 .. :try_end_28} :catch_1e

    .line 503
    :cond_1b
    const/4 v5, 0x0

    .line 507
    :goto_1e
    throw v26

    .line 486
    :catch_1c
    move-exception v7

    .line 487
    .restart local v7    # "e":Ljava/io/IOException;
    const-string/jumbo v27, "DocumentService"

    new-instance v28, Ljava/lang/StringBuilder;

    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v29, "Exception: "

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual {v7}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v29

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    invoke-static/range {v27 .. v28}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1c

    .line 496
    .end local v7    # "e":Ljava/io/IOException;
    :catch_1d
    move-exception v7

    .line 497
    .restart local v7    # "e":Ljava/io/IOException;
    const-string/jumbo v27, "DocumentService"

    new-instance v28, Ljava/lang/StringBuilder;

    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v29, "Exception: "

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual {v7}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v29

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    invoke-static/range {v27 .. v28}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1d

    .line 505
    .end local v7    # "e":Ljava/io/IOException;
    :catch_1e
    move-exception v7

    .line 506
    .restart local v7    # "e":Ljava/io/IOException;
    const-string/jumbo v27, "DocumentService"

    new-instance v28, Ljava/lang/StringBuilder;

    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v29, "Exception: "

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual {v7}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v29

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    invoke-static/range {v27 .. v28}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1e

    .line 428
    .end local v4    # "bis":Ljava/io/BufferedInputStream;
    .end local v7    # "e":Ljava/io/IOException;
    .end local v15    # "i":I
    .end local v19    # "ois":Ljava/io/ObjectInputStream;
    .end local v24    # "then":J
    .restart local v3    # "bis":Ljava/io/BufferedInputStream;
    .restart local v18    # "ois":Ljava/io/ObjectInputStream;
    :catchall_2
    move-exception v26

    move-object v11, v12

    .end local v12    # "fis":Ljava/io/FileInputStream;
    .restart local v11    # "fis":Ljava/io/FileInputStream;
    goto/16 :goto_d

    .end local v3    # "bis":Ljava/io/BufferedInputStream;
    .end local v11    # "fis":Ljava/io/FileInputStream;
    .restart local v4    # "bis":Ljava/io/BufferedInputStream;
    .restart local v12    # "fis":Ljava/io/FileInputStream;
    :catchall_3
    move-exception v26

    move-object v3, v4

    .end local v4    # "bis":Ljava/io/BufferedInputStream;
    .restart local v3    # "bis":Ljava/io/BufferedInputStream;
    move-object v11, v12

    .end local v12    # "fis":Ljava/io/FileInputStream;
    .restart local v11    # "fis":Ljava/io/FileInputStream;
    goto/16 :goto_d

    .end local v3    # "bis":Ljava/io/BufferedInputStream;
    .end local v11    # "fis":Ljava/io/FileInputStream;
    .end local v18    # "ois":Ljava/io/ObjectInputStream;
    .restart local v4    # "bis":Ljava/io/BufferedInputStream;
    .restart local v12    # "fis":Ljava/io/FileInputStream;
    .restart local v19    # "ois":Ljava/io/ObjectInputStream;
    :catchall_4
    move-exception v26

    move-object/from16 v18, v19

    .end local v19    # "ois":Ljava/io/ObjectInputStream;
    .restart local v18    # "ois":Ljava/io/ObjectInputStream;
    move-object v3, v4

    .end local v4    # "bis":Ljava/io/BufferedInputStream;
    .restart local v3    # "bis":Ljava/io/BufferedInputStream;
    move-object v11, v12

    .end local v12    # "fis":Ljava/io/FileInputStream;
    .restart local v11    # "fis":Ljava/io/FileInputStream;
    goto/16 :goto_d

    .line 396
    .end local v11    # "fis":Ljava/io/FileInputStream;
    .restart local v12    # "fis":Ljava/io/FileInputStream;
    :catch_1f
    move-exception v7

    move-object v11, v12

    .end local v12    # "fis":Ljava/io/FileInputStream;
    .restart local v11    # "fis":Ljava/io/FileInputStream;
    goto/16 :goto_11

    .end local v3    # "bis":Ljava/io/BufferedInputStream;
    .end local v11    # "fis":Ljava/io/FileInputStream;
    .restart local v4    # "bis":Ljava/io/BufferedInputStream;
    .restart local v12    # "fis":Ljava/io/FileInputStream;
    :catch_20
    move-exception v7

    move-object v3, v4

    .end local v4    # "bis":Ljava/io/BufferedInputStream;
    .restart local v3    # "bis":Ljava/io/BufferedInputStream;
    move-object v11, v12

    .end local v12    # "fis":Ljava/io/FileInputStream;
    .restart local v11    # "fis":Ljava/io/FileInputStream;
    goto/16 :goto_11

    .end local v3    # "bis":Ljava/io/BufferedInputStream;
    .end local v11    # "fis":Ljava/io/FileInputStream;
    .end local v18    # "ois":Ljava/io/ObjectInputStream;
    .restart local v4    # "bis":Ljava/io/BufferedInputStream;
    .restart local v12    # "fis":Ljava/io/FileInputStream;
    .restart local v19    # "ois":Ljava/io/ObjectInputStream;
    :catch_21
    move-exception v7

    move-object/from16 v18, v19

    .end local v19    # "ois":Ljava/io/ObjectInputStream;
    .restart local v18    # "ois":Ljava/io/ObjectInputStream;
    move-object v3, v4

    .end local v4    # "bis":Ljava/io/BufferedInputStream;
    .restart local v3    # "bis":Ljava/io/BufferedInputStream;
    move-object v11, v12

    .end local v12    # "fis":Ljava/io/FileInputStream;
    .restart local v11    # "fis":Ljava/io/FileInputStream;
    goto/16 :goto_11

    .line 365
    .end local v11    # "fis":Ljava/io/FileInputStream;
    .restart local v12    # "fis":Ljava/io/FileInputStream;
    :catch_22
    move-exception v7

    move-object v11, v12

    .end local v12    # "fis":Ljava/io/FileInputStream;
    .restart local v11    # "fis":Ljava/io/FileInputStream;
    goto/16 :goto_6

    .end local v3    # "bis":Ljava/io/BufferedInputStream;
    .end local v11    # "fis":Ljava/io/FileInputStream;
    .restart local v4    # "bis":Ljava/io/BufferedInputStream;
    .restart local v12    # "fis":Ljava/io/FileInputStream;
    :catch_23
    move-exception v7

    move-object v3, v4

    .end local v4    # "bis":Ljava/io/BufferedInputStream;
    .restart local v3    # "bis":Ljava/io/BufferedInputStream;
    move-object v11, v12

    .end local v12    # "fis":Ljava/io/FileInputStream;
    .restart local v11    # "fis":Ljava/io/FileInputStream;
    goto/16 :goto_6

    .end local v3    # "bis":Ljava/io/BufferedInputStream;
    .end local v11    # "fis":Ljava/io/FileInputStream;
    .end local v18    # "ois":Ljava/io/ObjectInputStream;
    .restart local v4    # "bis":Ljava/io/BufferedInputStream;
    .restart local v12    # "fis":Ljava/io/FileInputStream;
    .restart local v19    # "ois":Ljava/io/ObjectInputStream;
    :catch_24
    move-exception v7

    move-object/from16 v18, v19

    .end local v19    # "ois":Ljava/io/ObjectInputStream;
    .restart local v18    # "ois":Ljava/io/ObjectInputStream;
    move-object v3, v4

    .end local v4    # "bis":Ljava/io/BufferedInputStream;
    .restart local v3    # "bis":Ljava/io/BufferedInputStream;
    move-object v11, v12

    .end local v12    # "fis":Ljava/io/FileInputStream;
    .restart local v11    # "fis":Ljava/io/FileInputStream;
    goto/16 :goto_6

    .line 349
    .end local v2    # "allFiles":Ljava/util/ArrayList;
    .end local v3    # "bis":Ljava/io/BufferedInputStream;
    .end local v11    # "fis":Ljava/io/FileInputStream;
    .end local v13    # "fos":Ljava/io/FileOutputStream;
    .end local v18    # "ois":Ljava/io/ObjectInputStream;
    .restart local v14    # "fos":Ljava/io/FileOutputStream;
    :catch_25
    move-exception v7

    move-object v13, v14

    .end local v14    # "fos":Ljava/io/FileOutputStream;
    .restart local v13    # "fos":Ljava/io/FileOutputStream;
    goto/16 :goto_5

    .end local v5    # "bos":Ljava/io/BufferedOutputStream;
    .end local v13    # "fos":Ljava/io/FileOutputStream;
    .restart local v6    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v14    # "fos":Ljava/io/FileOutputStream;
    :catch_26
    move-exception v7

    move-object v5, v6

    .end local v6    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v5    # "bos":Ljava/io/BufferedOutputStream;
    move-object v13, v14

    .end local v14    # "fos":Ljava/io/FileOutputStream;
    .restart local v13    # "fos":Ljava/io/FileOutputStream;
    goto/16 :goto_5
.end method

.method private static tagPdf(Ljava/lang/String;)V
    .locals 8
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    .line 619
    new-instance v4, Lorg/icepdf/core/pobjects/Document;

    const/4 v5, 0x0

    invoke-direct {v4, v5}, Lorg/icepdf/core/pobjects/Document;-><init>(Landroid/content/Context;)V

    .line 621
    .local v4, "pdfDoc":Lorg/icepdf/core/pobjects/Document;
    :try_start_0
    invoke-virtual {v4, p0}, Lorg/icepdf/core/pobjects/Document;->setFile(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 626
    :goto_0
    sget-boolean v5, Lorg/icepdf/core/tag/Tagger;->tagging:Z

    if-eqz v5, :cond_0

    .line 627
    invoke-static {v4}, Lorg/icepdf/core/tag/Tagger;->setCurrentDocument(Lorg/icepdf/core/pobjects/Document;)V

    .line 629
    :cond_0
    invoke-virtual {v4}, Lorg/icepdf/core/pobjects/Document;->getNumberOfPages()I

    move-result v1

    .line 630
    .local v1, "numberOfPages":I
    const/4 v3, 0x0

    .local v3, "pageNumber":I
    :goto_1
    if-ge v3, v1, :cond_3

    .line 631
    sget-boolean v5, Lorg/icepdf/core/tag/Tagger;->tagging:Z

    if-eqz v5, :cond_1

    .line 632
    invoke-static {v3}, Lorg/icepdf/core/tag/Tagger;->setCurrentPageIndex(I)V

    .line 634
    :cond_1
    invoke-virtual {v4}, Lorg/icepdf/core/pobjects/Document;->getPageTree()Lorg/icepdf/core/pobjects/PageTree;

    move-result-object v5

    const-class v6, Lorg/icepdf/core/tag/TagUtil;

    invoke-virtual {v5, v3, v6}, Lorg/icepdf/core/pobjects/PageTree;->getPage(ILjava/lang/Object;)Lorg/icepdf/core/pobjects/Page;

    move-result-object v2

    .line 635
    .local v2, "page":Lorg/icepdf/core/pobjects/Page;
    if-eqz v2, :cond_2

    .line 637
    new-instance v5, Ljava/io/File;

    const-string/jumbo v6, ""

    invoke-direct {v5, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v5}, Lorg/icepdf/core/pobjects/Page;->init(Ljava/io/File;)V

    .line 638
    invoke-virtual {v4}, Lorg/icepdf/core/pobjects/Document;->getPageTree()Lorg/icepdf/core/pobjects/PageTree;

    move-result-object v5

    const-class v6, Lorg/icepdf/core/tag/TagUtil;

    invoke-virtual {v5, v2, v6}, Lorg/icepdf/core/pobjects/PageTree;->releasePage(Lorg/icepdf/core/pobjects/Page;Ljava/lang/Object;)V

    .line 640
    const-wide/16 v6, 0x14

    :try_start_1
    invoke-static {v6, v7}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1

    .line 630
    :cond_2
    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 622
    .end local v1    # "numberOfPages":I
    .end local v2    # "page":Lorg/icepdf/core/pobjects/Page;
    .end local v3    # "pageNumber":I
    :catch_0
    move-exception v0

    .line 623
    .local v0, "e":Ljava/lang/Exception;
    const-string/jumbo v5, "DocumentService"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "Exception: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 643
    .end local v0    # "e":Ljava/lang/Exception;
    .restart local v1    # "numberOfPages":I
    .restart local v3    # "pageNumber":I
    :cond_3
    invoke-virtual {v4}, Lorg/icepdf/core/pobjects/Document;->dispose()V

    .line 644
    return-void

    .line 640
    .restart local v2    # "page":Lorg/icepdf/core/pobjects/Page;
    :catch_1
    move-exception v5

    goto :goto_2
.end method
