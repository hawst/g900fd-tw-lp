.class public Lorg/icepdf/core/tag/query/DocumentResult;
.super Ljava/lang/Object;
.source "DocumentResult.java"


# instance fields
.field private document:Lorg/icepdf/core/tag/TaggedDocument;

.field private images:Ljava/util/List;


# direct methods
.method constructor <init>(Lorg/icepdf/core/tag/TaggedDocument;Lorg/icepdf/core/tag/TaggedImage;)V
    .locals 2
    .param p1, "doc"    # Lorg/icepdf/core/tag/TaggedDocument;
    .param p2, "initial"    # Lorg/icepdf/core/tag/TaggedImage;

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object p1, p0, Lorg/icepdf/core/tag/query/DocumentResult;->document:Lorg/icepdf/core/tag/TaggedDocument;

    .line 33
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0x8

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lorg/icepdf/core/tag/query/DocumentResult;->images:Ljava/util/List;

    .line 34
    invoke-virtual {p0, p2}, Lorg/icepdf/core/tag/query/DocumentResult;->addImage(Lorg/icepdf/core/tag/TaggedImage;)V

    .line 35
    return-void
.end method


# virtual methods
.method addImage(Lorg/icepdf/core/tag/TaggedImage;)V
    .locals 1
    .param p1, "ti"    # Lorg/icepdf/core/tag/TaggedImage;

    .prologue
    .line 38
    iget-object v0, p0, Lorg/icepdf/core/tag/query/DocumentResult;->images:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 39
    return-void
.end method

.method public getDocument()Lorg/icepdf/core/tag/TaggedDocument;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lorg/icepdf/core/tag/query/DocumentResult;->document:Lorg/icepdf/core/tag/TaggedDocument;

    return-object v0
.end method

.method public getImages()Ljava/util/List;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lorg/icepdf/core/tag/query/DocumentResult;->images:Ljava/util/List;

    return-object v0
.end method
