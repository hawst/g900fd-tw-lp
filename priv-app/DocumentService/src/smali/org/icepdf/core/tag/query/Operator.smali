.class public abstract Lorg/icepdf/core/tag/query/Operator;
.super Ljava/lang/Object;
.source "Operator.java"

# interfaces
.implements Lorg/icepdf/core/tag/query/Expression;


# static fields
.field public static SCOPE_IMAGE:I

.field private static final SCOPE_NAMES:[Ljava/lang/String;

.field public static SCOPE_TAG:I


# instance fields
.field protected childExpressions:[Lorg/icepdf/core/tag/query/Expression;

.field protected scope:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 22
    sput v3, Lorg/icepdf/core/tag/query/Operator;->SCOPE_TAG:I

    .line 23
    sput v4, Lorg/icepdf/core/tag/query/Operator;->SCOPE_IMAGE:I

    .line 24
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v2, ""

    aput-object v2, v0, v1

    const-string/jumbo v1, "TAG"

    aput-object v1, v0, v3

    const-string/jumbo v1, "IMAGE"

    aput-object v1, v0, v4

    sput-object v0, Lorg/icepdf/core/tag/query/Operator;->SCOPE_NAMES:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public describe(I)Ljava/lang/String;
    .locals 6
    .param p1, "indent"    # I

    .prologue
    .line 42
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 43
    .local v3, "sb":Ljava/lang/StringBuilder;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, p1, :cond_0

    .line 44
    const-string/jumbo v4, "  "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 43
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 45
    :cond_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    .line 46
    .local v0, "className":Ljava/lang/String;
    const-string/jumbo v4, "."

    invoke-virtual {v0, v4}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    invoke-virtual {v0, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 47
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 48
    const-string/jumbo v4, "  scope: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 49
    sget-object v4, Lorg/icepdf/core/tag/query/Operator;->SCOPE_NAMES:[Ljava/lang/String;

    iget v5, p0, Lorg/icepdf/core/tag/query/Operator;->scope:I

    aget-object v4, v4, v5

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 50
    const/16 v4, 0xa

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 51
    iget-object v4, p0, Lorg/icepdf/core/tag/query/Operator;->childExpressions:[Lorg/icepdf/core/tag/query/Expression;

    if-eqz v4, :cond_1

    iget-object v4, p0, Lorg/icepdf/core/tag/query/Operator;->childExpressions:[Lorg/icepdf/core/tag/query/Expression;

    array-length v2, v4

    .line 52
    .local v2, "num":I
    :goto_1
    const/4 v1, 0x0

    :goto_2
    if-ge v1, v2, :cond_2

    .line 53
    iget-object v4, p0, Lorg/icepdf/core/tag/query/Operator;->childExpressions:[Lorg/icepdf/core/tag/query/Expression;

    aget-object v4, v4, v1

    add-int/lit8 v5, p1, 0x1

    invoke-interface {v4, v5}, Lorg/icepdf/core/tag/query/Expression;->describe(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 52
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 51
    .end local v2    # "num":I
    :cond_1
    const/4 v2, 0x0

    goto :goto_1

    .line 54
    .restart local v2    # "num":I
    :cond_2
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4
.end method

.method public getScope()I
    .locals 1

    .prologue
    .line 30
    iget v0, p0, Lorg/icepdf/core/tag/query/Operator;->scope:I

    return v0
.end method

.method public setChildExpressions([Lorg/icepdf/core/tag/query/Expression;)V
    .locals 0
    .param p1, "children"    # [Lorg/icepdf/core/tag/query/Expression;

    .prologue
    .line 38
    iput-object p1, p0, Lorg/icepdf/core/tag/query/Operator;->childExpressions:[Lorg/icepdf/core/tag/query/Expression;

    .line 39
    return-void
.end method

.method public setScope(I)V
    .locals 0
    .param p1, "scope"    # I

    .prologue
    .line 34
    iput p1, p0, Lorg/icepdf/core/tag/query/Operator;->scope:I

    .line 35
    return-void
.end method
