.class public Lorg/icepdf/core/tag/query/Querior;
.super Ljava/lang/Object;
.source "Querior.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static parse([Ljava/lang/String;)Lorg/icepdf/core/tag/query/Expression;
    .locals 9
    .param p0, "unparsedQuery"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/icepdf/core/tag/query/ParseException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 31
    if-nez p0, :cond_0

    move v2, v7

    .line 32
    .local v2, "len":I
    :goto_0
    new-array v5, v2, [Ljava/lang/Object;

    .line 33
    .local v5, "parsedQuery":[Ljava/lang/Object;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-ge v1, v2, :cond_8

    .line 35
    aget-object v6, p0, v1

    const-string/jumbo v8, "img:and"

    invoke-virtual {v6, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 36
    new-instance v4, Lorg/icepdf/core/tag/query/And;

    invoke-direct {v4}, Lorg/icepdf/core/tag/query/And;-><init>()V

    .line 37
    .local v4, "op":Lorg/icepdf/core/tag/query/Operator;
    sget v6, Lorg/icepdf/core/tag/query/Operator;->SCOPE_IMAGE:I

    invoke-virtual {v4, v6}, Lorg/icepdf/core/tag/query/Operator;->setScope(I)V

    .line 38
    aput-object v4, v5, v1

    .line 33
    .end local v4    # "op":Lorg/icepdf/core/tag/query/Operator;
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 31
    .end local v1    # "i":I
    .end local v2    # "len":I
    .end local v5    # "parsedQuery":[Ljava/lang/Object;
    :cond_0
    array-length v2, p0

    goto :goto_0

    .line 40
    .restart local v1    # "i":I
    .restart local v2    # "len":I
    .restart local v5    # "parsedQuery":[Ljava/lang/Object;
    :cond_1
    aget-object v6, p0, v1

    const-string/jumbo v8, "tag:and"

    invoke-virtual {v6, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 41
    new-instance v4, Lorg/icepdf/core/tag/query/And;

    invoke-direct {v4}, Lorg/icepdf/core/tag/query/And;-><init>()V

    .line 42
    .restart local v4    # "op":Lorg/icepdf/core/tag/query/Operator;
    sget v6, Lorg/icepdf/core/tag/query/Operator;->SCOPE_TAG:I

    invoke-virtual {v4, v6}, Lorg/icepdf/core/tag/query/Operator;->setScope(I)V

    .line 43
    aput-object v4, v5, v1

    goto :goto_2

    .line 45
    .end local v4    # "op":Lorg/icepdf/core/tag/query/Operator;
    :cond_2
    aget-object v6, p0, v1

    const-string/jumbo v8, "img:or"

    invoke-virtual {v6, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 46
    new-instance v4, Lorg/icepdf/core/tag/query/Or;

    invoke-direct {v4}, Lorg/icepdf/core/tag/query/Or;-><init>()V

    .line 47
    .restart local v4    # "op":Lorg/icepdf/core/tag/query/Operator;
    sget v6, Lorg/icepdf/core/tag/query/Operator;->SCOPE_IMAGE:I

    invoke-virtual {v4, v6}, Lorg/icepdf/core/tag/query/Operator;->setScope(I)V

    .line 48
    aput-object v4, v5, v1

    goto :goto_2

    .line 50
    .end local v4    # "op":Lorg/icepdf/core/tag/query/Operator;
    :cond_3
    aget-object v6, p0, v1

    const-string/jumbo v8, "tag:or"

    invoke-virtual {v6, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 51
    new-instance v4, Lorg/icepdf/core/tag/query/Or;

    invoke-direct {v4}, Lorg/icepdf/core/tag/query/Or;-><init>()V

    .line 52
    .restart local v4    # "op":Lorg/icepdf/core/tag/query/Operator;
    sget v6, Lorg/icepdf/core/tag/query/Operator;->SCOPE_TAG:I

    invoke-virtual {v4, v6}, Lorg/icepdf/core/tag/query/Operator;->setScope(I)V

    .line 53
    aput-object v4, v5, v1

    goto :goto_2

    .line 55
    .end local v4    # "op":Lorg/icepdf/core/tag/query/Operator;
    :cond_4
    aget-object v6, p0, v1

    const-string/jumbo v8, "img:not"

    invoke-virtual {v6, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 56
    new-instance v4, Lorg/icepdf/core/tag/query/Not;

    invoke-direct {v4}, Lorg/icepdf/core/tag/query/Not;-><init>()V

    .line 57
    .restart local v4    # "op":Lorg/icepdf/core/tag/query/Operator;
    sget v6, Lorg/icepdf/core/tag/query/Operator;->SCOPE_IMAGE:I

    invoke-virtual {v4, v6}, Lorg/icepdf/core/tag/query/Operator;->setScope(I)V

    .line 58
    aput-object v4, v5, v1

    goto :goto_2

    .line 60
    .end local v4    # "op":Lorg/icepdf/core/tag/query/Operator;
    :cond_5
    aget-object v6, p0, v1

    const-string/jumbo v8, "tag:not"

    invoke-virtual {v6, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 61
    new-instance v4, Lorg/icepdf/core/tag/query/Not;

    invoke-direct {v4}, Lorg/icepdf/core/tag/query/Not;-><init>()V

    .line 62
    .restart local v4    # "op":Lorg/icepdf/core/tag/query/Operator;
    sget v6, Lorg/icepdf/core/tag/query/Operator;->SCOPE_TAG:I

    invoke-virtual {v4, v6}, Lorg/icepdf/core/tag/query/Operator;->setScope(I)V

    .line 63
    aput-object v4, v5, v1

    goto :goto_2

    .line 66
    .end local v4    # "op":Lorg/icepdf/core/tag/query/Operator;
    :cond_6
    aget-object v6, p0, v1

    const-string/jumbo v8, "substring"

    invoke-virtual {v6, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 67
    new-instance v6, Lorg/icepdf/core/tag/query/Substring;

    invoke-direct {v6}, Lorg/icepdf/core/tag/query/Substring;-><init>()V

    aput-object v6, v5, v1

    goto/16 :goto_2

    .line 70
    :cond_7
    aget-object v6, p0, v1

    aput-object v6, v5, v1

    goto/16 :goto_2

    .line 72
    :cond_8
    invoke-static {v5, v7}, Lorg/icepdf/core/tag/query/Querior;->rpnParse([Ljava/lang/Object;I)I

    move-result v3

    .line 73
    .local v3, "numTokensParsed":I
    array-length v6, v5

    if-eq v3, v6, :cond_9

    .line 74
    new-instance v6, Lorg/icepdf/core/tag/query/ParseException;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "Number of tokens given: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    array-length v8, v5

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, ", number of tokens parsed: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Lorg/icepdf/core/tag/query/ParseException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 75
    :cond_9
    aget-object v6, v5, v7

    check-cast v6, Lorg/icepdf/core/tag/query/Expression;

    invoke-interface {v6, v7}, Lorg/icepdf/core/tag/query/Expression;->describe(I)Ljava/lang/String;

    move-result-object v0

    .line 77
    .local v0, "desc":Ljava/lang/String;
    aget-object v6, v5, v7

    check-cast v6, Lorg/icepdf/core/tag/query/Expression;

    return-object v6
.end method

.method private static rpnParse([Ljava/lang/Object;I)I
    .locals 16
    .param p0, "parsedQuery"    # [Ljava/lang/Object;
    .param p1, "position"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/icepdf/core/tag/query/ParseException;
        }
    .end annotation

    .prologue
    .line 91
    aget-object v4, p0, p1

    .line 92
    .local v4, "curr":Ljava/lang/Object;
    const/4 v12, 0x1

    .line 93
    .local v12, "tokens":I
    instance-of v13, v4, Lorg/icepdf/core/tag/query/Expression;

    if-eqz v13, :cond_4

    move-object v13, v4

    .line 94
    check-cast v13, Lorg/icepdf/core/tag/query/Expression;

    invoke-interface {v13}, Lorg/icepdf/core/tag/query/Expression;->getArgumentCount()I

    move-result v9

    .line 95
    .local v9, "numArgs":I
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3, v9}, Ljava/util/ArrayList;-><init>(I)V

    .line 96
    .local v3, "args":Ljava/util/List;
    const/4 v1, 0x0

    .local v1, "a":I
    :goto_0
    if-ge v1, v9, :cond_1

    .line 97
    add-int v5, p1, v12

    .line 98
    .local v5, "currArgIndex":I
    move-object/from16 v0, p0

    array-length v13, v0

    if-lt v5, v13, :cond_0

    .line 99
    new-instance v13, Lorg/icepdf/core/tag/query/ParseException;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v15, "Token at position "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move/from16 v0, p1

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string/jumbo v15, " has argument "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    add-int/lit8 v15, v1, 0x1

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string/jumbo v15, " of "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string/jumbo v15, " which is calculated to be at position "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string/jumbo v15, ", which beyond the parsed list of query tokens"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-direct {v13, v14}, Lorg/icepdf/core/tag/query/ParseException;-><init>(Ljava/lang/String;)V

    throw v13

    .line 101
    :cond_0
    move-object/from16 v0, p0

    invoke-static {v0, v5}, Lorg/icepdf/core/tag/query/Querior;->rpnParse([Ljava/lang/Object;I)I

    move-result v13

    add-int/2addr v12, v13

    .line 102
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    invoke-interface {v3, v13}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 96
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 104
    .end local v5    # "currArgIndex":I
    :cond_1
    instance-of v13, v4, Lorg/icepdf/core/tag/query/Operator;

    if-eqz v13, :cond_5

    move-object v10, v4

    .line 105
    check-cast v10, Lorg/icepdf/core/tag/query/Operator;

    .line 106
    .local v10, "op":Lorg/icepdf/core/tag/query/Operator;
    new-array v6, v9, [Lorg/icepdf/core/tag/query/Expression;

    .line 107
    .local v6, "eargs":[Lorg/icepdf/core/tag/query/Expression;
    const/4 v1, 0x0

    :goto_1
    if-ge v1, v9, :cond_3

    .line 108
    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Integer;

    .line 109
    .local v8, "index":Ljava/lang/Integer;
    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v13

    aget-object v2, p0, v13

    .line 110
    .local v2, "arg":Ljava/lang/Object;
    instance-of v13, v2, Lorg/icepdf/core/tag/query/Expression;

    if-nez v13, :cond_2

    .line 111
    new-instance v13, Lorg/icepdf/core/tag/query/ParseException;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v15, "Token at position "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move/from16 v0, p1

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string/jumbo v15, " has argument "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    add-int/lit8 v15, v1, 0x1

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string/jumbo v15, " of "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string/jumbo v15, ", calculated to be at position "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string/jumbo v15, ", which is expected to be an expression token, but is instead a literal string: "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-direct {v13, v14}, Lorg/icepdf/core/tag/query/ParseException;-><init>(Ljava/lang/String;)V

    throw v13

    .line 112
    :cond_2
    check-cast v2, Lorg/icepdf/core/tag/query/Expression;

    .end local v2    # "arg":Ljava/lang/Object;
    aput-object v2, v6, v1

    .line 107
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 114
    .end local v8    # "index":Ljava/lang/Integer;
    :cond_3
    invoke-virtual {v10, v6}, Lorg/icepdf/core/tag/query/Operator;->setChildExpressions([Lorg/icepdf/core/tag/query/Expression;)V

    .line 129
    .end local v1    # "a":I
    .end local v3    # "args":Ljava/util/List;
    .end local v6    # "eargs":[Lorg/icepdf/core/tag/query/Expression;
    .end local v9    # "numArgs":I
    .end local v10    # "op":Lorg/icepdf/core/tag/query/Operator;
    :cond_4
    :goto_2
    return v12

    .line 116
    .restart local v1    # "a":I
    .restart local v3    # "args":Ljava/util/List;
    .restart local v9    # "numArgs":I
    :cond_5
    instance-of v13, v4, Lorg/icepdf/core/tag/query/Function;

    if-eqz v13, :cond_4

    move-object v7, v4

    .line 117
    check-cast v7, Lorg/icepdf/core/tag/query/Function;

    .line 118
    .local v7, "func":Lorg/icepdf/core/tag/query/Function;
    new-array v11, v9, [Ljava/lang/String;

    .line 119
    .local v11, "sargs":[Ljava/lang/String;
    const/4 v1, 0x0

    :goto_3
    if-ge v1, v9, :cond_7

    .line 120
    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Integer;

    .line 121
    .restart local v8    # "index":Ljava/lang/Integer;
    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v13

    aget-object v2, p0, v13

    .line 122
    .restart local v2    # "arg":Ljava/lang/Object;
    instance-of v13, v2, Lorg/icepdf/core/tag/query/Expression;

    if-eqz v13, :cond_6

    .line 123
    new-instance v13, Lorg/icepdf/core/tag/query/ParseException;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v15, "Token at position "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move/from16 v0, p1

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string/jumbo v15, " has argument "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    add-int/lit8 v15, v1, 0x1

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string/jumbo v15, " of "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string/jumbo v15, ", calculated to be at position "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string/jumbo v15, ", which is expected to be literal string, but is instead an expression token: "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-direct {v13, v14}, Lorg/icepdf/core/tag/query/ParseException;-><init>(Ljava/lang/String;)V

    throw v13

    .line 124
    :cond_6
    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v13

    aput-object v13, v11, v1

    .line 119
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 126
    .end local v2    # "arg":Ljava/lang/Object;
    .end local v8    # "index":Ljava/lang/Integer;
    :cond_7
    invoke-virtual {v7, v11}, Lorg/icepdf/core/tag/query/Function;->setArguments([Ljava/lang/String;)V

    goto :goto_2
.end method

.method public static search(Lorg/icepdf/core/tag/TagState;Lorg/icepdf/core/tag/query/Expression;)Ljava/util/List;
    .locals 17
    .param p0, "state"    # Lorg/icepdf/core/tag/TagState;
    .param p1, "queryRoot"    # Lorg/icepdf/core/tag/query/Expression;

    .prologue
    .line 133
    const/4 v1, 0x0

    .line 134
    .local v1, "docCount":I
    const/4 v8, 0x0

    .line 135
    .local v8, "imgCount":I
    const/4 v11, 0x0

    .line 137
    .local v11, "tagCount":I
    new-instance v9, Ljava/util/ArrayList;

    const/16 v16, 0x20

    move/from16 v0, v16

    invoke-direct {v9, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 139
    .local v9, "results":Ljava/util/List;
    invoke-virtual/range {p0 .. p0}, Lorg/icepdf/core/tag/TagState;->getDocuments()Ljava/util/List;

    move-result-object v4

    .line 140
    .local v4, "docs":Ljava/util/List;
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "docIt":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v16

    if-eqz v16, :cond_4

    .line 141
    add-int/lit8 v1, v1, 0x1

    .line 142
    const/4 v3, 0x0

    .line 144
    .local v3, "docRes":Lorg/icepdf/core/tag/query/DocumentResult;
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lorg/icepdf/core/tag/TaggedDocument;

    .line 145
    .local v14, "td":Lorg/icepdf/core/tag/TaggedDocument;
    invoke-virtual {v14}, Lorg/icepdf/core/tag/TaggedDocument;->getImages()Ljava/util/List;

    move-result-object v7

    .line 146
    .local v7, "images":Ljava/util/List;
    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .local v5, "imIt":Ljava/util/Iterator;
    :cond_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v16

    if-eqz v16, :cond_0

    .line 147
    add-int/lit8 v8, v8, 0x1

    .line 148
    const/4 v6, 0x0

    .line 150
    .local v6, "imageInResults":Z
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lorg/icepdf/core/tag/TaggedImage;

    .line 151
    .local v15, "ti":Lorg/icepdf/core/tag/TaggedImage;
    invoke-virtual {v15}, Lorg/icepdf/core/tag/TaggedImage;->getTags()Ljava/util/List;

    move-result-object v13

    .line 152
    .local v13, "tags":Ljava/util/List;
    invoke-interface {v13}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v12

    .local v12, "tagIt":Ljava/util/Iterator;
    :cond_2
    :goto_0
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v16

    if-eqz v16, :cond_1

    .line 153
    add-int/lit8 v11, v11, 0x1

    .line 154
    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    .line 155
    .local v10, "tag":Ljava/lang/String;
    move-object/from16 v0, p1

    invoke-interface {v0, v14, v15, v10}, Lorg/icepdf/core/tag/query/Expression;->matches(Lorg/icepdf/core/tag/TaggedDocument;Lorg/icepdf/core/tag/TaggedImage;Ljava/lang/String;)Z

    move-result v16

    if-eqz v16, :cond_2

    .line 156
    if-nez v3, :cond_3

    .line 157
    new-instance v3, Lorg/icepdf/core/tag/query/DocumentResult;

    .end local v3    # "docRes":Lorg/icepdf/core/tag/query/DocumentResult;
    invoke-direct {v3, v14, v15}, Lorg/icepdf/core/tag/query/DocumentResult;-><init>(Lorg/icepdf/core/tag/TaggedDocument;Lorg/icepdf/core/tag/TaggedImage;)V

    .line 158
    .restart local v3    # "docRes":Lorg/icepdf/core/tag/query/DocumentResult;
    const/4 v6, 0x1

    .line 159
    invoke-interface {v9, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 161
    :cond_3
    if-nez v6, :cond_2

    .line 162
    invoke-virtual {v3, v15}, Lorg/icepdf/core/tag/query/DocumentResult;->addImage(Lorg/icepdf/core/tag/TaggedImage;)V

    .line 163
    const/4 v6, 0x1

    goto :goto_0

    .line 170
    .end local v3    # "docRes":Lorg/icepdf/core/tag/query/DocumentResult;
    .end local v5    # "imIt":Ljava/util/Iterator;
    .end local v6    # "imageInResults":Z
    .end local v7    # "images":Ljava/util/List;
    .end local v10    # "tag":Ljava/lang/String;
    .end local v12    # "tagIt":Ljava/util/Iterator;
    .end local v13    # "tags":Ljava/util/List;
    .end local v14    # "td":Lorg/icepdf/core/tag/TaggedDocument;
    .end local v15    # "ti":Lorg/icepdf/core/tag/TaggedImage;
    :cond_4
    return-object v9
.end method
