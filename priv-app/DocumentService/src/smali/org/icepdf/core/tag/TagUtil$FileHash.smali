.class Lorg/icepdf/core/tag/TagUtil$FileHash;
.super Ljava/lang/Object;
.source "TagUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/icepdf/core/tag/TagUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "FileHash"
.end annotation


# instance fields
.field private digestBytes:[B

.field private fileLength:J

.field private hash:I


# direct methods
.method constructor <init>([BJ)V
    .locals 4
    .param p1, "digestBytes"    # [B
    .param p2, "fileLength"    # J

    .prologue
    .line 652
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 653
    iput-object p1, p0, Lorg/icepdf/core/tag/TagUtil$FileHash;->digestBytes:[B

    .line 654
    iput-wide p2, p0, Lorg/icepdf/core/tag/TagUtil$FileHash;->fileLength:J

    .line 656
    const/16 v1, 0x20

    shr-long v2, p2, v1

    xor-long/2addr v2, p2

    long-to-int v1, v2

    iput v1, p0, Lorg/icepdf/core/tag/TagUtil$FileHash;->hash:I

    .line 657
    array-length v1, p1

    add-int/lit8 v0, v1, -0x1

    .local v0, "i":I
    :goto_0
    if-ltz v0, :cond_0

    .line 658
    iget v1, p0, Lorg/icepdf/core/tag/TagUtil$FileHash;->hash:I

    mul-int/lit8 v1, v1, 0x1f

    aget-byte v2, p1, v0

    add-int/2addr v1, v2

    iput v1, p0, Lorg/icepdf/core/tag/TagUtil$FileHash;->hash:I

    .line 657
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 660
    :cond_0
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 8
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v2, 0x0

    .line 667
    instance-of v3, p1, Lorg/icepdf/core/tag/TagUtil$FileHash;

    if-eqz v3, :cond_0

    move-object v0, p1

    .line 668
    check-cast v0, Lorg/icepdf/core/tag/TagUtil$FileHash;

    .line 669
    .local v0, "fh":Lorg/icepdf/core/tag/TagUtil$FileHash;
    iget v3, p0, Lorg/icepdf/core/tag/TagUtil$FileHash;->hash:I

    iget v4, v0, Lorg/icepdf/core/tag/TagUtil$FileHash;->hash:I

    if-ne v3, v4, :cond_0

    iget-wide v4, p0, Lorg/icepdf/core/tag/TagUtil$FileHash;->fileLength:J

    iget-wide v6, v0, Lorg/icepdf/core/tag/TagUtil$FileHash;->fileLength:J

    cmp-long v3, v4, v6

    if-eqz v3, :cond_1

    .line 679
    .end local v0    # "fh":Lorg/icepdf/core/tag/TagUtil$FileHash;
    :cond_0
    :goto_0
    return v2

    .line 671
    .restart local v0    # "fh":Lorg/icepdf/core/tag/TagUtil$FileHash;
    :cond_1
    iget-object v3, p0, Lorg/icepdf/core/tag/TagUtil$FileHash;->digestBytes:[B

    array-length v3, v3

    iget-object v4, v0, Lorg/icepdf/core/tag/TagUtil$FileHash;->digestBytes:[B

    array-length v4, v4

    if-ne v3, v4, :cond_0

    .line 673
    iget-object v3, p0, Lorg/icepdf/core/tag/TagUtil$FileHash;->digestBytes:[B

    array-length v3, v3

    add-int/lit8 v1, v3, -0x1

    .local v1, "i":I
    :goto_1
    if-ltz v1, :cond_2

    .line 674
    iget-object v3, p0, Lorg/icepdf/core/tag/TagUtil$FileHash;->digestBytes:[B

    aget-byte v3, v3, v1

    iget-object v4, v0, Lorg/icepdf/core/tag/TagUtil$FileHash;->digestBytes:[B

    aget-byte v4, v4, v1

    if-ne v3, v4, :cond_0

    .line 673
    add-int/lit8 v1, v1, -0x1

    goto :goto_1

    .line 677
    :cond_2
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 663
    iget v0, p0, Lorg/icepdf/core/tag/TagUtil$FileHash;->hash:I

    return v0
.end method
