.class public Lorg/icepdf/core/tag/query/And;
.super Lorg/icepdf/core/tag/query/Operator;
.source "And.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Lorg/icepdf/core/tag/query/Operator;-><init>()V

    return-void
.end method


# virtual methods
.method public getArgumentCount()I
    .locals 1

    .prologue
    .line 26
    const/4 v0, 0x2

    return v0
.end method

.method public matches(Lorg/icepdf/core/tag/TaggedDocument;Lorg/icepdf/core/tag/TaggedImage;Ljava/lang/String;)Z
    .locals 3
    .param p1, "td"    # Lorg/icepdf/core/tag/TaggedDocument;
    .param p2, "ti"    # Lorg/icepdf/core/tag/TaggedImage;
    .param p3, "tag"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 30
    iget-object v2, p0, Lorg/icepdf/core/tag/query/And;->childExpressions:[Lorg/icepdf/core/tag/query/Expression;

    aget-object v2, v2, v1

    invoke-interface {v2, p1, p2, p3}, Lorg/icepdf/core/tag/query/Expression;->matches(Lorg/icepdf/core/tag/TaggedDocument;Lorg/icepdf/core/tag/TaggedImage;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lorg/icepdf/core/tag/query/And;->childExpressions:[Lorg/icepdf/core/tag/query/Expression;

    aget-object v2, v2, v0

    invoke-interface {v2, p1, p2, p3}, Lorg/icepdf/core/tag/query/Expression;->matches(Lorg/icepdf/core/tag/TaggedDocument;Lorg/icepdf/core/tag/TaggedImage;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method
