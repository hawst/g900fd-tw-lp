.class public Lorg/icepdf/core/tag/TagState;
.super Ljava/lang/Object;
.source "TagState.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x14f8397351ad9b66L


# instance fields
.field private transient currentDocument:Lorg/icepdf/core/tag/TaggedDocument;

.field private documents:Ljava/util/List;

.field private transient origin2document:Ljava/util/HashMap;


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    const/16 v1, 0x80

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lorg/icepdf/core/tag/TagState;->documents:Ljava/util/List;

    .line 39
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, Lorg/icepdf/core/tag/TagState;->origin2document:Ljava/util/HashMap;

    .line 40
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/icepdf/core/tag/TagState;->currentDocument:Lorg/icepdf/core/tag/TaggedDocument;

    .line 51
    return-void
.end method


# virtual methods
.method beginImage(Lorg/icepdf/core/pobjects/Reference;Z)V
    .locals 1
    .param p1, "ref"    # Lorg/icepdf/core/pobjects/Reference;
    .param p2, "inlineImage"    # Z

    .prologue
    .line 71
    iget-object v0, p0, Lorg/icepdf/core/tag/TagState;->currentDocument:Lorg/icepdf/core/tag/TaggedDocument;

    if-eqz v0, :cond_0

    .line 72
    iget-object v0, p0, Lorg/icepdf/core/tag/TagState;->currentDocument:Lorg/icepdf/core/tag/TaggedDocument;

    invoke-virtual {v0, p1, p2}, Lorg/icepdf/core/tag/TaggedDocument;->beginImage(Lorg/icepdf/core/pobjects/Reference;Z)V

    .line 73
    :cond_0
    return-void
.end method

.method describe()Ljava/lang/String;
    .locals 4

    .prologue
    .line 86
    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v3, 0x1000

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 87
    .local v1, "sb":Ljava/lang/StringBuilder;
    iget-object v3, p0, Lorg/icepdf/core/tag/TagState;->documents:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "docs":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 88
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/icepdf/core/tag/TaggedDocument;

    .line 89
    .local v2, "td":Lorg/icepdf/core/tag/TaggedDocument;
    invoke-virtual {v2}, Lorg/icepdf/core/tag/TaggedDocument;->describe()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 90
    const-string/jumbo v3, "=================================\n"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 92
    .end local v2    # "td":Lorg/icepdf/core/tag/TaggedDocument;
    :cond_0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method endImage(Lorg/icepdf/core/pobjects/Reference;)V
    .locals 1
    .param p1, "ref"    # Lorg/icepdf/core/pobjects/Reference;

    .prologue
    .line 76
    iget-object v0, p0, Lorg/icepdf/core/tag/TagState;->currentDocument:Lorg/icepdf/core/tag/TaggedDocument;

    if-eqz v0, :cond_0

    .line 77
    iget-object v0, p0, Lorg/icepdf/core/tag/TagState;->currentDocument:Lorg/icepdf/core/tag/TaggedDocument;

    invoke-virtual {v0, p1}, Lorg/icepdf/core/tag/TaggedDocument;->endImage(Lorg/icepdf/core/pobjects/Reference;)V

    .line 78
    :cond_0
    return-void
.end method

.method public getDocuments()Ljava/util/List;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lorg/icepdf/core/tag/TagState;->documents:Ljava/util/List;

    return-object v0
.end method

.method setCurrentDocument(Lorg/icepdf/core/pobjects/Document;)V
    .locals 4
    .param p1, "doc"    # Lorg/icepdf/core/pobjects/Document;

    .prologue
    .line 54
    invoke-virtual {p1}, Lorg/icepdf/core/pobjects/Document;->getDocumentOrigin()Ljava/lang/String;

    move-result-object v0

    .line 55
    .local v0, "origin":Ljava/lang/String;
    iget-object v2, p0, Lorg/icepdf/core/tag/TagState;->origin2document:Ljava/util/HashMap;

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/icepdf/core/tag/TaggedDocument;

    .line 56
    .local v1, "td":Lorg/icepdf/core/tag/TaggedDocument;
    if-eqz v1, :cond_0

    .line 57
    iput-object v1, p0, Lorg/icepdf/core/tag/TagState;->currentDocument:Lorg/icepdf/core/tag/TaggedDocument;

    .line 63
    :goto_0
    return-void

    .line 60
    :cond_0
    new-instance v2, Lorg/icepdf/core/tag/TaggedDocument;

    invoke-direct {v2, v0}, Lorg/icepdf/core/tag/TaggedDocument;-><init>(Ljava/lang/String;)V

    iput-object v2, p0, Lorg/icepdf/core/tag/TagState;->currentDocument:Lorg/icepdf/core/tag/TaggedDocument;

    .line 61
    iget-object v2, p0, Lorg/icepdf/core/tag/TagState;->documents:Ljava/util/List;

    iget-object v3, p0, Lorg/icepdf/core/tag/TagState;->currentDocument:Lorg/icepdf/core/tag/TaggedDocument;

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method setCurrentPageIndex(I)V
    .locals 1
    .param p1, "currentPageIndex"    # I

    .prologue
    .line 66
    iget-object v0, p0, Lorg/icepdf/core/tag/TagState;->currentDocument:Lorg/icepdf/core/tag/TaggedDocument;

    if-eqz v0, :cond_0

    .line 67
    iget-object v0, p0, Lorg/icepdf/core/tag/TagState;->currentDocument:Lorg/icepdf/core/tag/TaggedDocument;

    invoke-virtual {v0, p1}, Lorg/icepdf/core/tag/TaggedDocument;->setCurrentPageIndex(I)V

    .line 68
    :cond_0
    return-void
.end method

.method tagImage(Ljava/lang/String;)V
    .locals 1
    .param p1, "tag"    # Ljava/lang/String;

    .prologue
    .line 81
    iget-object v0, p0, Lorg/icepdf/core/tag/TagState;->currentDocument:Lorg/icepdf/core/tag/TaggedDocument;

    if-eqz v0, :cond_0

    .line 82
    iget-object v0, p0, Lorg/icepdf/core/tag/TagState;->currentDocument:Lorg/icepdf/core/tag/TaggedDocument;

    invoke-virtual {v0, p1}, Lorg/icepdf/core/tag/TaggedDocument;->tagImage(Ljava/lang/String;)V

    .line 83
    :cond_0
    return-void
.end method
