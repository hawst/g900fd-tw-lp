.class public Lorg/icepdf/core/tag/query/Not;
.super Lorg/icepdf/core/tag/query/Operator;
.source "Not.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Lorg/icepdf/core/tag/query/Operator;-><init>()V

    return-void
.end method


# virtual methods
.method public getArgumentCount()I
    .locals 1

    .prologue
    .line 26
    const/4 v0, 0x1

    return v0
.end method

.method public matches(Lorg/icepdf/core/tag/TaggedDocument;Lorg/icepdf/core/tag/TaggedImage;Ljava/lang/String;)Z
    .locals 2
    .param p1, "td"    # Lorg/icepdf/core/tag/TaggedDocument;
    .param p2, "ti"    # Lorg/icepdf/core/tag/TaggedImage;
    .param p3, "tag"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 30
    iget-object v1, p0, Lorg/icepdf/core/tag/query/Not;->childExpressions:[Lorg/icepdf/core/tag/query/Expression;

    aget-object v1, v1, v0

    invoke-interface {v1, p1, p2, p3}, Lorg/icepdf/core/tag/query/Expression;->matches(Lorg/icepdf/core/tag/TaggedDocument;Lorg/icepdf/core/tag/TaggedImage;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method
