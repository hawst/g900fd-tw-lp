.class public Lorg/icepdf/core/util/ImageCache;
.super Ljava/lang/Object;
.source "ImageCache.java"


# static fields
.field private static isCachingEnabled:Z

.field private static final logger:Ljava/util/logging/Logger;

.field private static scaleImages:Z


# instance fields
.field private cacheManager:Lorg/icepdf/core/util/CacheManager;

.field private imageStore:Landroid/graphics/Bitmap;

.field private isCached:Z

.field private isScaled:Z

.field private length:J

.field private tempFile:Ljava/io/File;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 50
    const-class v0, Lorg/icepdf/core/util/ImageCache;

    invoke-virtual {v0}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Lorg/icepdf/core/util/ImageCache;->logger:Ljava/util/logging/Logger;

    .line 83
    const-string/jumbo v0, "org.icepdf.core.imagecache.enabled"

    invoke-static {v0, v1}, Lorg/icepdf/core/util/Defs;->sysPropertyBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lorg/icepdf/core/util/ImageCache;->isCachingEnabled:Z

    .line 88
    const-string/jumbo v0, "org.icepdf.core.scaleImages"

    invoke-static {v0, v1}, Lorg/icepdf/core/util/Defs;->sysPropertyBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lorg/icepdf/core/util/ImageCache;->scaleImages:Z

    .line 92
    return-void
.end method

.method public constructor <init>(Lorg/icepdf/core/util/Library;)V
    .locals 4
    .param p1, "library"    # Lorg/icepdf/core/util/Library;

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 99
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    iput-object v3, p0, Lorg/icepdf/core/util/ImageCache;->tempFile:Ljava/io/File;

    .line 57
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lorg/icepdf/core/util/ImageCache;->length:J

    .line 60
    iput-boolean v2, p0, Lorg/icepdf/core/util/ImageCache;->isCached:Z

    .line 63
    iput-object v3, p0, Lorg/icepdf/core/util/ImageCache;->cacheManager:Lorg/icepdf/core/util/CacheManager;

    .line 66
    iput-boolean v2, p0, Lorg/icepdf/core/util/ImageCache;->isScaled:Z

    .line 101
    invoke-virtual {p1}, Lorg/icepdf/core/util/Library;->getCacheManager()Lorg/icepdf/core/util/CacheManager;

    move-result-object v0

    iput-object v0, p0, Lorg/icepdf/core/util/ImageCache;->cacheManager:Lorg/icepdf/core/util/CacheManager;

    .line 102
    return-void
.end method

.method public static scaleBufferedImage(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;
    .locals 10
    .param p0, "bim"    # Landroid/graphics/Bitmap;
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    const/16 v9, 0xbb8

    const/16 v8, 0x9c4

    const/16 v7, 0x7d0

    const/16 v6, 0x5dc

    const/16 v5, 0x3e8

    .line 260
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    .line 262
    .local v0, "imageScale":D
    const/16 v4, 0x1f4

    if-ge p1, v4, :cond_0

    const/16 v4, 0x1f4

    if-lt p2, v4, :cond_4

    :cond_0
    if-lt p1, v5, :cond_1

    if-ge p2, v5, :cond_4

    .line 264
    :cond_1
    const-wide v0, 0x3fe999999999999aL    # 0.8

    .line 281
    :cond_2
    :goto_0
    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    cmpg-double v4, v0, v4

    if-gez v4, :cond_3

    .line 282
    new-instance v3, Landroid/graphics/Matrix;

    invoke-direct {v3}, Landroid/graphics/Matrix;-><init>()V

    .line 283
    .local v3, "tx":Landroid/graphics/Matrix;
    double-to-float v4, v0

    double-to-float v5, v0

    invoke-virtual {v3, v4, v5}, Landroid/graphics/Matrix;->preScale(FF)Z

    .line 285
    double-to-int v4, v0

    double-to-int v5, v0

    const/4 v6, 0x0

    invoke-static {p0, v4, v5, v6}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 292
    .local v2, "sbim":Landroid/graphics/Bitmap;
    move-object p0, v2

    .line 295
    .end local v2    # "sbim":Landroid/graphics/Bitmap;
    .end local v3    # "tx":Landroid/graphics/Matrix;
    :cond_3
    return-object p0

    .line 265
    :cond_4
    if-ge p1, v5, :cond_5

    if-lt p2, v5, :cond_7

    :cond_5
    if-lt p1, v6, :cond_6

    if-ge p2, v6, :cond_7

    .line 267
    :cond_6
    const-wide v0, 0x3fe6666666666666L    # 0.7

    goto :goto_0

    .line 268
    :cond_7
    if-ge p1, v6, :cond_8

    if-lt p2, v6, :cond_a

    :cond_8
    if-lt p1, v7, :cond_9

    if-ge p2, v7, :cond_a

    .line 270
    :cond_9
    const-wide v0, 0x3fe3333333333333L    # 0.6

    goto :goto_0

    .line 271
    :cond_a
    if-ge p1, v7, :cond_b

    if-lt p2, v7, :cond_d

    :cond_b
    if-lt p1, v8, :cond_c

    if-ge p2, v8, :cond_d

    .line 273
    :cond_c
    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    goto :goto_0

    .line 274
    :cond_d
    if-ge p1, v8, :cond_e

    if-lt p2, v8, :cond_10

    :cond_e
    if-lt p1, v9, :cond_f

    if-ge p2, v9, :cond_10

    .line 276
    :cond_f
    const-wide v0, 0x3fd999999999999aL    # 0.4

    goto :goto_0

    .line 277
    :cond_10
    if-ge p1, v9, :cond_11

    if-lt p2, v9, :cond_2

    .line 278
    :cond_11
    const-wide v0, 0x3fd3333333333333L    # 0.3

    goto :goto_0
.end method

.method private setImage(Landroid/graphics/Bitmap;Z)V
    .locals 5
    .param p1, "image"    # Landroid/graphics/Bitmap;
    .param p2, "useCaching"    # Z

    .prologue
    .line 123
    if-eqz p2, :cond_0

    :try_start_0
    iget-boolean v2, p0, Lorg/icepdf/core/util/ImageCache;->isCached:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lorg/icepdf/core/util/ImageCache;->imageStore:Landroid/graphics/Bitmap;

    if-ne v2, p1, :cond_0

    .line 160
    :goto_0
    return-void

    .line 125
    :cond_0
    iget-object v2, p0, Lorg/icepdf/core/util/ImageCache;->imageStore:Landroid/graphics/Bitmap;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lorg/icepdf/core/util/ImageCache;->imageStore:Landroid/graphics/Bitmap;

    if-eq v2, p1, :cond_1

    .line 127
    const/4 v2, 0x0

    iput-object v2, p0, Lorg/icepdf/core/util/ImageCache;->imageStore:Landroid/graphics/Bitmap;

    .line 131
    :cond_1
    if-eqz p2, :cond_2

    .line 133
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "PDFImageStream"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->hashCode()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, ".tmp"

    invoke-static {v2, v3}, Ljava/io/File;->createTempFile(Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object v2

    iput-object v2, p0, Lorg/icepdf/core/util/ImageCache;->tempFile:Ljava/io/File;

    .line 136
    iget-object v2, p0, Lorg/icepdf/core/util/ImageCache;->cacheManager:Lorg/icepdf/core/util/CacheManager;

    iget-object v3, p0, Lorg/icepdf/core/util/ImageCache;->tempFile:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lorg/icepdf/core/util/CacheManager;->addCachedFile(Ljava/lang/String;)V

    .line 139
    iget-object v2, p0, Lorg/icepdf/core/util/ImageCache;->tempFile:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->deleteOnExit()V

    .line 141
    new-instance v1, Ljava/io/FileOutputStream;

    iget-object v2, p0, Lorg/icepdf/core/util/ImageCache;->tempFile:Ljava/io/File;

    invoke-direct {v1, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 143
    .local v1, "out":Ljava/io/FileOutputStream;
    sget-object v2, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v3, 0x64

    invoke-virtual {p1, v2, v3, v1}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 145
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V

    .line 148
    iget-object v2, p0, Lorg/icepdf/core/util/ImageCache;->tempFile:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->length()J

    move-result-wide v2

    iput-wide v2, p0, Lorg/icepdf/core/util/ImageCache;->length:J

    .line 150
    const/4 v2, 0x1

    iput-boolean v2, p0, Lorg/icepdf/core/util/ImageCache;->isCached:Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 156
    .end local v1    # "out":Ljava/io/FileOutputStream;
    :catch_0
    move-exception v0

    .line 157
    .local v0, "e":Ljava/io/IOException;
    sget-object v2, Lorg/icepdf/core/util/ImageCache;->logger:Ljava/util/logging/Logger;

    sget-object v3, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string/jumbo v4, "Error creating ImageCache temporary file."

    invoke-virtual {v2, v3, v4, v0}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 153
    .end local v0    # "e":Ljava/io/IOException;
    :cond_2
    :try_start_1
    iput-object p1, p0, Lorg/icepdf/core/util/ImageCache;->imageStore:Landroid/graphics/Bitmap;

    .line 154
    const/4 v2, 0x0

    iput-boolean v2, p0, Lorg/icepdf/core/util/ImageCache;->isCached:Z
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method


# virtual methods
.method public dispose(ZZ)V
    .locals 2
    .param p1, "cache"    # Z
    .param p2, "imageRecoverableElsewise"    # Z

    .prologue
    .line 200
    iget-object v0, p0, Lorg/icepdf/core/util/ImageCache;->imageStore:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_2

    .line 205
    if-eqz p1, :cond_0

    sget-boolean v0, Lorg/icepdf/core/util/ImageCache;->isCachingEnabled:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lorg/icepdf/core/util/ImageCache;->isCached:Z

    if-nez v0, :cond_0

    if-nez p2, :cond_0

    .line 206
    iget-object v0, p0, Lorg/icepdf/core/util/ImageCache;->imageStore:Landroid/graphics/Bitmap;

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lorg/icepdf/core/util/ImageCache;->setImage(Landroid/graphics/Bitmap;Z)V

    .line 209
    :cond_0
    if-eqz p1, :cond_1

    iget-boolean v0, p0, Lorg/icepdf/core/util/ImageCache;->isCached:Z

    if-nez v0, :cond_1

    if-eqz p2, :cond_2

    .line 211
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/icepdf/core/util/ImageCache;->imageStore:Landroid/graphics/Bitmap;

    .line 214
    :cond_2
    return-void
.end method

.method public getLength()J
    .locals 2

    .prologue
    .line 240
    iget-wide v0, p0, Lorg/icepdf/core/util/ImageCache;->length:J

    return-wide v0
.end method

.method public isCachedSomehow()Z
    .locals 1

    .prologue
    .line 248
    iget-boolean v0, p0, Lorg/icepdf/core/util/ImageCache;->isCached:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/icepdf/core/util/ImageCache;->imageStore:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isScaled()Z
    .locals 1

    .prologue
    .line 244
    iget-boolean v0, p0, Lorg/icepdf/core/util/ImageCache;->isScaled:Z

    return v0
.end method

.method public readImage()Landroid/graphics/Bitmap;
    .locals 5

    .prologue
    .line 168
    iget-object v2, p0, Lorg/icepdf/core/util/ImageCache;->imageStore:Landroid/graphics/Bitmap;

    if-eqz v2, :cond_0

    .line 169
    iget-object v1, p0, Lorg/icepdf/core/util/ImageCache;->imageStore:Landroid/graphics/Bitmap;

    .line 187
    :goto_0
    return-object v1

    .line 172
    :cond_0
    iget-boolean v2, p0, Lorg/icepdf/core/util/ImageCache;->isCached:Z

    if-eqz v2, :cond_1

    .line 173
    const/4 v1, 0x0

    .line 176
    .local v1, "image":Landroid/graphics/Bitmap;
    :try_start_0
    new-instance v2, Ljava/io/FileInputStream;

    iget-object v3, p0, Lorg/icepdf/core/util/ImageCache;->tempFile:Ljava/io/File;

    invoke-direct {v2, v3}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-static {v2}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 178
    iget-object v2, p0, Lorg/icepdf/core/util/ImageCache;->tempFile:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->length()J

    move-result-wide v2

    iput-wide v2, p0, Lorg/icepdf/core/util/ImageCache;->length:J
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 180
    :catch_0
    move-exception v0

    .line 181
    .local v0, "e":Ljava/io/IOException;
    sget-object v2, Lorg/icepdf/core/util/ImageCache;->logger:Ljava/util/logging/Logger;

    sget-object v3, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string/jumbo v4, "Error creating ImageCache temporary file "

    invoke-virtual {v2, v3, v4, v0}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 187
    .end local v0    # "e":Ljava/io/IOException;
    .end local v1    # "image":Landroid/graphics/Bitmap;
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public scaleImage(II)V
    .locals 1
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    .line 217
    sget-boolean v0, Lorg/icepdf/core/util/ImageCache;->scaleImages:Z

    if-eqz v0, :cond_0

    .line 223
    invoke-virtual {p0}, Lorg/icepdf/core/util/ImageCache;->readImage()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-static {v0, p1, p2}, Lorg/icepdf/core/util/ImageCache;->scaleBufferedImage(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/icepdf/core/util/ImageCache;->setImage(Landroid/graphics/Bitmap;)V

    .line 224
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/icepdf/core/util/ImageCache;->isScaled:Z

    .line 228
    :goto_0
    return-void

    .line 226
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/icepdf/core/util/ImageCache;->isScaled:Z

    goto :goto_0
.end method

.method public setImage(Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1, "image"    # Landroid/graphics/Bitmap;

    .prologue
    .line 111
    iget-boolean v0, p0, Lorg/icepdf/core/util/ImageCache;->isCached:Z

    invoke-direct {p0, p1, v0}, Lorg/icepdf/core/util/ImageCache;->setImage(Landroid/graphics/Bitmap;Z)V

    .line 112
    return-void
.end method

.method public setIsScaled(Z)V
    .locals 0
    .param p1, "flag"    # Z

    .prologue
    .line 231
    iput-boolean p1, p0, Lorg/icepdf/core/util/ImageCache;->isScaled:Z

    .line 232
    return-void
.end method
