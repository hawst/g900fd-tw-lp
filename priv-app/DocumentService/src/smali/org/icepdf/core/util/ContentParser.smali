.class public Lorg/icepdf/core/util/ContentParser;
.super Ljava/lang/Object;
.source "ContentParser.java"


# static fields
.field private static final MAX_OBJ_COUNT:I = 0x7a120

.field public static final OVERPAINT_ALPHA:F = 0.4f

.field private static final TAG:Ljava/lang/String; = "ContentParser"

.field private static disableTransparencyGroups:Z

.field private static final logger:Ljava/util/logging/Logger;


# instance fields
.field private graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

.field private inTextBlock:Z

.field private library:Lorg/icepdf/core/util/Library;

.field private resources:Lorg/icepdf/core/pobjects/Resources;

.field private textBlockBase:Landroid/graphics/Matrix;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 53
    const-class v0, Lorg/icepdf/core/util/ContentParser;

    invoke-virtual {v0}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Lorg/icepdf/core/util/ContentParser;->logger:Ljava/util/logging/Logger;

    .line 64
    const-string/jumbo v0, "org.icepdf.core.disableTransparencyGroup"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lorg/icepdf/core/util/Defs;->sysPropertyBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lorg/icepdf/core/util/ContentParser;->disableTransparencyGroups:Z

    .line 68
    return-void
.end method

.method public constructor <init>(Lorg/icepdf/core/util/Library;Lorg/icepdf/core/pobjects/Resources;)V
    .locals 0
    .param p1, "l"    # Lorg/icepdf/core/util/Library;
    .param p2, "r"    # Lorg/icepdf/core/pobjects/Resources;

    .prologue
    .line 89
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 90
    iput-object p1, p0, Lorg/icepdf/core/util/ContentParser;->library:Lorg/icepdf/core/util/Library;

    .line 91
    iput-object p2, p0, Lorg/icepdf/core/util/ContentParser;->resources:Lorg/icepdf/core/pobjects/Resources;

    .line 93
    return-void
.end method

.method private static applyTextScaling(Lorg/icepdf/core/pobjects/graphics/GraphicsState;)Landroid/graphics/Matrix;
    .locals 11
    .param p0, "graphicState"    # Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    .prologue
    .line 2812
    new-instance v7, Landroid/graphics/Matrix;

    invoke-virtual {p0}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->getCTM()Landroid/graphics/Matrix;

    move-result-object v1

    invoke-direct {v7, v1}, Landroid/graphics/Matrix;-><init>(Landroid/graphics/Matrix;)V

    .line 2816
    .local v7, "af":Landroid/graphics/Matrix;
    new-instance v10, Landroid/graphics/Matrix;

    invoke-virtual {p0}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->getCTM()Landroid/graphics/Matrix;

    move-result-object v1

    invoke-direct {v10, v1}, Landroid/graphics/Matrix;-><init>(Landroid/graphics/Matrix;)V

    .line 2817
    .local v10, "oldHScaling":Landroid/graphics/Matrix;
    invoke-virtual {p0}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->getTextState()Lorg/icepdf/core/pobjects/graphics/TextState;

    move-result-object v1

    iget v9, v1, Lorg/icepdf/core/pobjects/graphics/TextState;->hScalling:F

    .line 2818
    .local v9, "hScalling":F
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    .line 2820
    .local v0, "horizontalScalingTransform":Landroid/graphics/Matrix;
    const/16 v1, 0x9

    new-array v8, v1, [F

    .line 2821
    .local v8, "getvalues":[F
    invoke-virtual {v7, v8}, Landroid/graphics/Matrix;->getValues([F)V

    .line 2823
    const/4 v1, 0x0

    aget v1, v8, v1

    mul-float/2addr v1, v9

    const/4 v2, 0x3

    aget v2, v8, v2

    const/4 v3, 0x1

    aget v3, v8, v3

    const/4 v4, 0x4

    aget v4, v8, v4

    const/4 v5, 0x2

    aget v5, v8, v5

    const/4 v6, 0x5

    aget v6, v8, v6

    invoke-static/range {v0 .. v6}, Lcom/samsung/thumbnail/util/Utils;->setMatrixFromValues(Landroid/graphics/Matrix;FFFFFF)V

    .line 2832
    invoke-virtual {p0, v0}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->set(Landroid/graphics/Matrix;)V

    .line 2834
    return-object v10
.end method

.method private commonFill(Lorg/icepdf/core/pobjects/graphics/Shapes;Landroid/graphics/Path;)V
    .locals 9
    .param p1, "shapes"    # Lorg/icepdf/core/pobjects/graphics/Shapes;
    .param p2, "geometricPath"    # Landroid/graphics/Path;

    .prologue
    const/4 v8, 0x2

    const/high16 v7, 0x3f800000    # 1.0f

    .line 2717
    iget-object v5, p0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    invoke-virtual {v5}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->getFillColorSpace()Lorg/icepdf/core/pobjects/graphics/PColorSpace;

    move-result-object v5

    instance-of v5, v5, Lorg/icepdf/core/pobjects/graphics/PatternColor;

    if-eqz v5, :cond_5

    .line 2719
    iget-object v5, p0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    invoke-virtual {v5}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->getFillColorSpace()Lorg/icepdf/core/pobjects/graphics/PColorSpace;

    move-result-object v2

    check-cast v2, Lorg/icepdf/core/pobjects/graphics/PatternColor;

    .line 2721
    .local v2, "patternColor":Lorg/icepdf/core/pobjects/graphics/PatternColor;
    invoke-virtual {v2}, Lorg/icepdf/core/pobjects/graphics/PatternColor;->getPattern()Lorg/icepdf/core/pobjects/graphics/Pattern;

    move-result-object v1

    .line 2723
    .local v1, "pattern":Lorg/icepdf/core/pobjects/graphics/Pattern;
    if-eqz v1, :cond_4

    invoke-interface {v1}, Lorg/icepdf/core/pobjects/graphics/Pattern;->getPatternType()I

    move-result v5

    const/4 v6, 0x1

    if-ne v5, v6, :cond_4

    move-object v4, v1

    .line 2727
    check-cast v4, Lorg/icepdf/core/pobjects/graphics/TilingPattern;

    .line 2729
    .local v4, "tilingPattern":Lorg/icepdf/core/pobjects/graphics/TilingPattern;
    iget-object v5, p0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    invoke-virtual {v5}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->save()Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    move-result-object v5

    iput-object v5, p0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    .line 2731
    iget-object v5, p0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    invoke-virtual {v4, v5}, Lorg/icepdf/core/pobjects/graphics/TilingPattern;->setParentGraphicState(Lorg/icepdf/core/pobjects/graphics/GraphicsState;)V

    .line 2732
    invoke-virtual {v4}, Lorg/icepdf/core/pobjects/graphics/TilingPattern;->init()V

    .line 2734
    iget-object v5, p0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    invoke-virtual {v5}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->restore()Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    move-result-object v5

    iput-object v5, p0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    .line 2737
    invoke-virtual {v4}, Lorg/icepdf/core/pobjects/graphics/TilingPattern;->getbBoxMod()Landroid/graphics/RectF;

    move-result-object v5

    invoke-virtual {v5}, Landroid/graphics/RectF;->width()F

    move-result v5

    cmpl-float v5, v5, v7

    if-gez v5, :cond_0

    invoke-virtual {v4}, Lorg/icepdf/core/pobjects/graphics/TilingPattern;->getbBoxMod()Landroid/graphics/RectF;

    move-result-object v5

    invoke-virtual {v5}, Landroid/graphics/RectF;->height()F

    move-result v5

    cmpl-float v5, v5, v7

    if-ltz v5, :cond_2

    .line 2739
    :cond_0
    invoke-virtual {p1, v4}, Lorg/icepdf/core/pobjects/graphics/Shapes;->add(Ljava/lang/Object;)V

    .line 2749
    :goto_0
    invoke-virtual {p1, p2}, Lorg/icepdf/core/pobjects/graphics/Shapes;->add(Ljava/lang/Object;)V

    .line 2750
    invoke-virtual {p1}, Lorg/icepdf/core/pobjects/graphics/Shapes;->addFillCommand()V

    .line 2774
    .end local v1    # "pattern":Lorg/icepdf/core/pobjects/graphics/Pattern;
    .end local v2    # "patternColor":Lorg/icepdf/core/pobjects/graphics/PatternColor;
    .end local v4    # "tilingPattern":Lorg/icepdf/core/pobjects/graphics/TilingPattern;
    :cond_1
    :goto_1
    return-void

    .line 2742
    .restart local v1    # "pattern":Lorg/icepdf/core/pobjects/graphics/Pattern;
    .restart local v2    # "patternColor":Lorg/icepdf/core/pobjects/graphics/PatternColor;
    .restart local v4    # "tilingPattern":Lorg/icepdf/core/pobjects/graphics/TilingPattern;
    :cond_2
    invoke-virtual {v4}, Lorg/icepdf/core/pobjects/graphics/TilingPattern;->getPaintType()I

    move-result v5

    if-ne v5, v8, :cond_3

    .line 2744
    invoke-virtual {v4}, Lorg/icepdf/core/pobjects/graphics/TilingPattern;->getUnColored()Lorg/apache/poi/java/awt/Color;

    move-result-object v5

    invoke-virtual {p1, v5}, Lorg/icepdf/core/pobjects/graphics/Shapes;->add(Ljava/lang/Object;)V

    goto :goto_0

    .line 2746
    :cond_3
    invoke-virtual {v4}, Lorg/icepdf/core/pobjects/graphics/TilingPattern;->getFirstColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v5

    invoke-virtual {p1, v5}, Lorg/icepdf/core/pobjects/graphics/Shapes;->add(Ljava/lang/Object;)V

    goto :goto_0

    .line 2751
    .end local v4    # "tilingPattern":Lorg/icepdf/core/pobjects/graphics/TilingPattern;
    :cond_4
    if-eqz v1, :cond_1

    invoke-interface {v1}, Lorg/icepdf/core/pobjects/graphics/Pattern;->getPatternType()I

    move-result v5

    if-ne v5, v8, :cond_1

    .line 2753
    invoke-interface {v1}, Lorg/icepdf/core/pobjects/graphics/Pattern;->init()V

    .line 2754
    invoke-interface {v1}, Lorg/icepdf/core/pobjects/graphics/Pattern;->getPaint()Landroid/graphics/Paint;

    move-result-object v5

    invoke-virtual {p1, v5}, Lorg/icepdf/core/pobjects/graphics/Shapes;->add(Ljava/lang/Object;)V

    .line 2755
    invoke-virtual {p1, p2}, Lorg/icepdf/core/pobjects/graphics/Shapes;->add(Ljava/lang/Object;)V

    .line 2756
    invoke-virtual {p1}, Lorg/icepdf/core/pobjects/graphics/Shapes;->addFillCommand()V

    goto :goto_1

    .line 2762
    .end local v1    # "pattern":Lorg/icepdf/core/pobjects/graphics/Pattern;
    .end local v2    # "patternColor":Lorg/icepdf/core/pobjects/graphics/PatternColor;
    :cond_5
    iget-object v5, p0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    invoke-virtual {v5}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->getFillAlpha()F

    move-result v5

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    .line 2763
    .local v3, "requiredAlpha":Ljava/lang/Float;
    iget-object v5, p0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    invoke-virtual {v5}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->getFillColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v0

    .line 2764
    .local v0, "fillColor":Lorg/apache/poi/java/awt/Color;
    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v5

    invoke-virtual {v0, v5}, Lorg/apache/poi/java/awt/Color;->setFAlpha(F)V

    .line 2765
    invoke-virtual {p1, v0}, Lorg/icepdf/core/pobjects/graphics/Shapes;->add(Ljava/lang/Object;)V

    .line 2766
    invoke-virtual {p1, p2}, Lorg/icepdf/core/pobjects/graphics/Shapes;->add(Ljava/lang/Object;)V

    .line 2767
    invoke-virtual {p1}, Lorg/icepdf/core/pobjects/graphics/Shapes;->addFillCommand()V

    goto :goto_1
.end method

.method private static commonStroke(Lorg/icepdf/core/pobjects/graphics/GraphicsState;Lorg/icepdf/core/pobjects/graphics/Shapes;Landroid/graphics/Path;)V
    .locals 9
    .param p0, "graphicState"    # Lorg/icepdf/core/pobjects/graphics/GraphicsState;
    .param p1, "shapes"    # Lorg/icepdf/core/pobjects/graphics/Shapes;
    .param p2, "geometricPath"    # Landroid/graphics/Path;

    .prologue
    const/4 v8, 0x2

    const/high16 v7, 0x3f800000    # 1.0f

    .line 2610
    invoke-virtual {p0}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->getStrokeColorSpace()Lorg/icepdf/core/pobjects/graphics/PColorSpace;

    move-result-object v5

    instance-of v5, v5, Lorg/icepdf/core/pobjects/graphics/PatternColor;

    if-eqz v5, :cond_4

    .line 2612
    invoke-virtual {p0}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->getStrokeColorSpace()Lorg/icepdf/core/pobjects/graphics/PColorSpace;

    move-result-object v1

    check-cast v1, Lorg/icepdf/core/pobjects/graphics/PatternColor;

    .line 2614
    .local v1, "patternColor":Lorg/icepdf/core/pobjects/graphics/PatternColor;
    invoke-virtual {v1}, Lorg/icepdf/core/pobjects/graphics/PatternColor;->getPattern()Lorg/icepdf/core/pobjects/graphics/Pattern;

    move-result-object v0

    .line 2616
    .local v0, "pattern":Lorg/icepdf/core/pobjects/graphics/Pattern;
    if-eqz v0, :cond_3

    invoke-interface {v0}, Lorg/icepdf/core/pobjects/graphics/Pattern;->getPatternType()I

    move-result v5

    const/4 v6, 0x1

    if-ne v5, v6, :cond_3

    move-object v4, v0

    .line 2620
    check-cast v4, Lorg/icepdf/core/pobjects/graphics/TilingPattern;

    .line 2622
    .local v4, "tilingPattern":Lorg/icepdf/core/pobjects/graphics/TilingPattern;
    invoke-virtual {p0}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->save()Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    move-result-object p0

    .line 2624
    invoke-virtual {v4, p0}, Lorg/icepdf/core/pobjects/graphics/TilingPattern;->setParentGraphicState(Lorg/icepdf/core/pobjects/graphics/GraphicsState;)V

    .line 2625
    invoke-virtual {v4}, Lorg/icepdf/core/pobjects/graphics/TilingPattern;->init()V

    .line 2627
    invoke-virtual {p0}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->restore()Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    move-result-object p0

    .line 2630
    invoke-virtual {v4}, Lorg/icepdf/core/pobjects/graphics/TilingPattern;->getbBoxMod()Landroid/graphics/RectF;

    move-result-object v5

    invoke-virtual {v5}, Landroid/graphics/RectF;->width()F

    move-result v5

    cmpl-float v5, v5, v7

    if-lez v5, :cond_1

    invoke-virtual {v4}, Lorg/icepdf/core/pobjects/graphics/TilingPattern;->getbBoxMod()Landroid/graphics/RectF;

    move-result-object v5

    invoke-virtual {v5}, Landroid/graphics/RectF;->height()F

    move-result v5

    cmpl-float v5, v5, v7

    if-lez v5, :cond_1

    .line 2632
    invoke-virtual {p1, v4}, Lorg/icepdf/core/pobjects/graphics/Shapes;->add(Ljava/lang/Object;)V

    .line 2642
    :goto_0
    invoke-virtual {p1, p2}, Lorg/icepdf/core/pobjects/graphics/Shapes;->add(Ljava/lang/Object;)V

    .line 2643
    invoke-virtual {p1}, Lorg/icepdf/core/pobjects/graphics/Shapes;->addDrawCommand()V

    .line 2669
    .end local v0    # "pattern":Lorg/icepdf/core/pobjects/graphics/Pattern;
    .end local v1    # "patternColor":Lorg/icepdf/core/pobjects/graphics/PatternColor;
    .end local v4    # "tilingPattern":Lorg/icepdf/core/pobjects/graphics/TilingPattern;
    :cond_0
    :goto_1
    return-void

    .line 2635
    .restart local v0    # "pattern":Lorg/icepdf/core/pobjects/graphics/Pattern;
    .restart local v1    # "patternColor":Lorg/icepdf/core/pobjects/graphics/PatternColor;
    .restart local v4    # "tilingPattern":Lorg/icepdf/core/pobjects/graphics/TilingPattern;
    :cond_1
    invoke-virtual {v4}, Lorg/icepdf/core/pobjects/graphics/TilingPattern;->getPaintType()I

    move-result v5

    if-ne v5, v8, :cond_2

    .line 2637
    invoke-virtual {v4}, Lorg/icepdf/core/pobjects/graphics/TilingPattern;->getUnColored()Lorg/apache/poi/java/awt/Color;

    move-result-object v5

    invoke-virtual {p1, v5}, Lorg/icepdf/core/pobjects/graphics/Shapes;->add(Ljava/lang/Object;)V

    goto :goto_0

    .line 2639
    :cond_2
    invoke-virtual {v4}, Lorg/icepdf/core/pobjects/graphics/TilingPattern;->getFirstColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v5

    invoke-virtual {p1, v5}, Lorg/icepdf/core/pobjects/graphics/Shapes;->add(Ljava/lang/Object;)V

    goto :goto_0

    .line 2644
    .end local v4    # "tilingPattern":Lorg/icepdf/core/pobjects/graphics/TilingPattern;
    :cond_3
    if-eqz v0, :cond_0

    invoke-interface {v0}, Lorg/icepdf/core/pobjects/graphics/Pattern;->getPatternType()I

    move-result v5

    if-ne v5, v8, :cond_0

    .line 2646
    invoke-interface {v0}, Lorg/icepdf/core/pobjects/graphics/Pattern;->init()V

    .line 2647
    invoke-interface {v0}, Lorg/icepdf/core/pobjects/graphics/Pattern;->getPaint()Landroid/graphics/Paint;

    move-result-object v5

    invoke-virtual {p1, v5}, Lorg/icepdf/core/pobjects/graphics/Shapes;->add(Ljava/lang/Object;)V

    .line 2648
    invoke-virtual {p1, p2}, Lorg/icepdf/core/pobjects/graphics/Shapes;->add(Ljava/lang/Object;)V

    .line 2649
    invoke-virtual {p1}, Lorg/icepdf/core/pobjects/graphics/Shapes;->addDrawCommand()V

    goto :goto_1

    .line 2657
    .end local v0    # "pattern":Lorg/icepdf/core/pobjects/graphics/Pattern;
    .end local v1    # "patternColor":Lorg/icepdf/core/pobjects/graphics/PatternColor;
    :cond_4
    invoke-virtual {p0}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->getStrokeAlpha()F

    move-result v5

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    .line 2658
    .local v2, "requiredAlpha":Ljava/lang/Float;
    invoke-virtual {p0}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->getStrokeColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v3

    .line 2659
    .local v3, "strokeColor":Lorg/apache/poi/java/awt/Color;
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v5

    invoke-virtual {v3, v5}, Lorg/apache/poi/java/awt/Color;->setFAlpha(F)V

    .line 2660
    invoke-virtual {p1, v3}, Lorg/icepdf/core/pobjects/graphics/Shapes;->add(Ljava/lang/Object;)V

    .line 2661
    invoke-virtual {p1, p2}, Lorg/icepdf/core/pobjects/graphics/Shapes;->add(Ljava/lang/Object;)V

    .line 2662
    invoke-virtual {p1}, Lorg/icepdf/core/pobjects/graphics/Shapes;->addDrawCommand()V

    goto :goto_1
.end method

.method private static consume_CS(Lorg/icepdf/core/pobjects/graphics/GraphicsState;Ljava/util/Stack;Lorg/icepdf/core/pobjects/Resources;)V
    .locals 2
    .param p0, "graphicState"    # Lorg/icepdf/core/pobjects/graphics/GraphicsState;
    .param p1, "stack"    # Ljava/util/Stack;
    .param p2, "resources"    # Lorg/icepdf/core/pobjects/Resources;

    .prologue
    .line 1730
    invoke-virtual {p1}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/icepdf/core/pobjects/Name;

    .line 1733
    .local v0, "n":Lorg/icepdf/core/pobjects/Name;
    invoke-virtual {p2, v0}, Lorg/icepdf/core/pobjects/Resources;->getColorSpace(Ljava/lang/Object;)Lorg/icepdf/core/pobjects/graphics/PColorSpace;

    move-result-object v1

    invoke-virtual {p0, v1}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->setStrokeColorSpace(Lorg/icepdf/core/pobjects/graphics/PColorSpace;)V

    .line 1734
    return-void
.end method

.method private static consume_Do(Lorg/icepdf/core/pobjects/graphics/GraphicsState;Ljava/util/Stack;Lorg/icepdf/core/pobjects/graphics/Shapes;Lorg/icepdf/core/pobjects/Resources;Z)Lorg/icepdf/core/pobjects/graphics/GraphicsState;
    .locals 26
    .param p0, "graphicState"    # Lorg/icepdf/core/pobjects/graphics/GraphicsState;
    .param p1, "stack"    # Ljava/util/Stack;
    .param p2, "shapes"    # Lorg/icepdf/core/pobjects/graphics/Shapes;
    .param p3, "resources"    # Lorg/icepdf/core/pobjects/Resources;
    .param p4, "viewParse"    # Z

    .prologue
    .line 2012
    invoke-virtual/range {p1 .. p1}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Lorg/icepdf/core/pobjects/Name;

    check-cast v22, Lorg/icepdf/core/pobjects/Name;

    invoke-virtual/range {v22 .. v22}, Lorg/icepdf/core/pobjects/Name;->getName()Ljava/lang/String;

    move-result-object v21

    .line 2014
    .local v21, "xobjectName":Ljava/lang/String;
    move-object/from16 v0, p3

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lorg/icepdf/core/pobjects/Resources;->isForm(Ljava/lang/String;)Z

    move-result v22

    if-eqz v22, :cond_a

    .line 2017
    invoke-virtual/range {p0 .. p0}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->save()Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    move-result-object p0

    .line 2020
    move-object/from16 v0, p3

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lorg/icepdf/core/pobjects/Resources;->getForm(Ljava/lang/String;)Lorg/icepdf/core/pobjects/Form;

    move-result-object v12

    .line 2021
    .local v12, "formXObject":Lorg/icepdf/core/pobjects/Form;
    if-eqz v12, :cond_6

    .line 2023
    new-instance v20, Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;-><init>(Lorg/icepdf/core/pobjects/graphics/GraphicsState;)V

    .line 2025
    .local v20, "xformGraphicsState":Lorg/icepdf/core/pobjects/graphics/GraphicsState;
    move-object/from16 v0, v20

    invoke-virtual {v12, v0}, Lorg/icepdf/core/pobjects/Form;->setGraphicsState(Lorg/icepdf/core/pobjects/graphics/GraphicsState;)V

    .line 2026
    invoke-virtual {v12}, Lorg/icepdf/core/pobjects/Form;->isTransparencyGroup()Z

    move-result v22

    if-eqz v22, :cond_0

    .line 2029
    invoke-virtual {v12}, Lorg/icepdf/core/pobjects/Form;->isTransparencyGroup()Z

    move-result v22

    move-object/from16 v0, v20

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->setTransparencyGroup(Z)V

    .line 2030
    invoke-virtual {v12}, Lorg/icepdf/core/pobjects/Form;->isIsolated()Z

    move-result v22

    move-object/from16 v0, v20

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->setIsolated(Z)V

    .line 2031
    invoke-virtual {v12}, Lorg/icepdf/core/pobjects/Form;->isKnockOut()Z

    move-result v22

    move-object/from16 v0, v20

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->setKnockOut(Z)V

    .line 2036
    :cond_0
    move-object/from16 v0, p3

    invoke-virtual {v12, v0}, Lorg/icepdf/core/pobjects/Form;->setParentResources(Lorg/icepdf/core/pobjects/Resources;)V

    .line 2037
    invoke-virtual {v12}, Lorg/icepdf/core/pobjects/Form;->init()V

    .line 2039
    new-instance v6, Landroid/graphics/Matrix;

    invoke-virtual/range {p0 .. p0}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->getCTM()Landroid/graphics/Matrix;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-direct {v6, v0}, Landroid/graphics/Matrix;-><init>(Landroid/graphics/Matrix;)V

    .line 2041
    .local v6, "af":Landroid/graphics/Matrix;
    invoke-virtual {v12}, Lorg/icepdf/core/pobjects/Form;->getMatrix()Landroid/graphics/Matrix;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v6, v0}, Landroid/graphics/Matrix;->preConcat(Landroid/graphics/Matrix;)Z

    .line 2042
    move-object/from16 v0, p2

    invoke-virtual {v0, v6}, Lorg/icepdf/core/pobjects/graphics/Shapes;->add(Ljava/lang/Object;)V

    .line 2044
    invoke-virtual/range {p0 .. p0}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->getClip()Landroid/graphics/Path;

    move-result-object v22

    if-eqz v22, :cond_8

    .line 2045
    invoke-virtual {v12}, Lorg/icepdf/core/pobjects/Form;->getMatrix()Landroid/graphics/Matrix;

    move-result-object v16

    .line 2047
    .local v16, "matrix":Landroid/graphics/Matrix;
    invoke-virtual {v12}, Lorg/icepdf/core/pobjects/Form;->getBBox()Landroid/graphics/RectF;

    move-result-object v8

    .line 2048
    .local v8, "bboxRect":Landroid/graphics/RectF;
    new-instance v7, Landroid/graphics/Path;

    invoke-direct {v7}, Landroid/graphics/Path;-><init>()V

    .line 2049
    .local v7, "bboxPath":Landroid/graphics/Path;
    iget v0, v8, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    iget v0, v8, Landroid/graphics/RectF;->top:F

    move/from16 v23, v0

    move/from16 v0, v22

    move/from16 v1, v23

    invoke-virtual {v7, v0, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 2050
    iget v0, v8, Landroid/graphics/RectF;->right:F

    move/from16 v22, v0

    iget v0, v8, Landroid/graphics/RectF;->top:F

    move/from16 v23, v0

    move/from16 v0, v22

    move/from16 v1, v23

    invoke-virtual {v7, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 2051
    iget v0, v8, Landroid/graphics/RectF;->right:F

    move/from16 v22, v0

    iget v0, v8, Landroid/graphics/RectF;->bottom:F

    move/from16 v23, v0

    move/from16 v0, v22

    move/from16 v1, v23

    invoke-virtual {v7, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 2052
    iget v0, v8, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    iget v0, v8, Landroid/graphics/RectF;->bottom:F

    move/from16 v23, v0

    move/from16 v0, v22

    move/from16 v1, v23

    invoke-virtual {v7, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 2053
    invoke-virtual {v7}, Landroid/graphics/Path;->close()V

    .line 2055
    invoke-virtual/range {p0 .. p0}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->getClip()Landroid/graphics/Path;

    move-result-object v10

    .line 2059
    .local v10, "clipPath":Landroid/graphics/Path;
    new-instance v15, Landroid/graphics/Matrix;

    invoke-direct {v15}, Landroid/graphics/Matrix;-><init>()V

    .line 2061
    .local v15, "invertMatrix":Landroid/graphics/Matrix;
    move-object/from16 v0, v16

    invoke-virtual {v0, v15}, Landroid/graphics/Matrix;->invert(Landroid/graphics/Matrix;)Z

    move-result v22

    if-eqz v22, :cond_1

    .line 2063
    move-object/from16 v16, v15

    .line 2069
    :cond_1
    const/16 v17, 0x0

    .line 2070
    .local v17, "shape":Landroid/graphics/Path;
    if-eqz v10, :cond_3

    .line 2072
    new-instance v17, Landroid/graphics/Path;

    .end local v17    # "shape":Landroid/graphics/Path;
    move-object/from16 v0, v17

    invoke-direct {v0, v10}, Landroid/graphics/Path;-><init>(Landroid/graphics/Path;)V

    .line 2073
    .restart local v17    # "shape":Landroid/graphics/Path;
    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/graphics/Path;->transform(Landroid/graphics/Matrix;)V

    .line 2076
    new-instance v18, Landroid/graphics/RectF;

    invoke-direct/range {v18 .. v18}, Landroid/graphics/RectF;-><init>()V

    .line 2077
    .local v18, "shapeBounds":Landroid/graphics/RectF;
    const/16 v22, 0x1

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->computeBounds(Landroid/graphics/RectF;Z)V

    .line 2079
    new-instance v9, Landroid/graphics/Region;

    invoke-direct {v9}, Landroid/graphics/Region;-><init>()V

    .line 2080
    .local v9, "bboxreg":Landroid/graphics/Region;
    new-instance v19, Landroid/graphics/Region;

    invoke-direct/range {v19 .. v19}, Landroid/graphics/Region;-><init>()V

    .line 2083
    .local v19, "shapereg":Landroid/graphics/Region;
    new-instance v11, Landroid/graphics/Region;

    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    move/from16 v0, v22

    float-to-int v0, v0

    move/from16 v22, v0

    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/RectF;->top:F

    move/from16 v23, v0

    move/from16 v0, v23

    float-to-int v0, v0

    move/from16 v23, v0

    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/RectF;->right:F

    move/from16 v24, v0

    move/from16 v0, v24

    float-to-int v0, v0

    move/from16 v24, v0

    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    move/from16 v25, v0

    move/from16 v0, v25

    float-to-int v0, v0

    move/from16 v25, v0

    move/from16 v0, v22

    move/from16 v1, v23

    move/from16 v2, v24

    move/from16 v3, v25

    invoke-direct {v11, v0, v1, v2, v3}, Landroid/graphics/Region;-><init>(IIII)V

    .line 2085
    .local v11, "common":Landroid/graphics/Region;
    move-object/from16 v0, v19

    move-object/from16 v1, v17

    invoke-virtual {v0, v1, v11}, Landroid/graphics/Region;->setPath(Landroid/graphics/Path;Landroid/graphics/Region;)Z

    .line 2087
    new-instance v11, Landroid/graphics/Region;

    .end local v11    # "common":Landroid/graphics/Region;
    iget v0, v8, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    move/from16 v0, v22

    float-to-int v0, v0

    move/from16 v22, v0

    iget v0, v8, Landroid/graphics/RectF;->top:F

    move/from16 v23, v0

    move/from16 v0, v23

    float-to-int v0, v0

    move/from16 v23, v0

    iget v0, v8, Landroid/graphics/RectF;->right:F

    move/from16 v24, v0

    move/from16 v0, v24

    float-to-int v0, v0

    move/from16 v24, v0

    iget v0, v8, Landroid/graphics/RectF;->bottom:F

    move/from16 v25, v0

    move/from16 v0, v25

    float-to-int v0, v0

    move/from16 v25, v0

    move/from16 v0, v22

    move/from16 v1, v23

    move/from16 v2, v24

    move/from16 v3, v25

    invoke-direct {v11, v0, v1, v2, v3}, Landroid/graphics/Region;-><init>(IIII)V

    .line 2089
    .restart local v11    # "common":Landroid/graphics/Region;
    invoke-virtual {v9, v7, v11}, Landroid/graphics/Region;->setPath(Landroid/graphics/Path;Landroid/graphics/Region;)Z

    .line 2091
    const/4 v14, 0x0

    .line 2092
    .local v14, "intersection":Landroid/graphics/Path;
    sget-object v22, Landroid/graphics/Region$Op;->INTERSECT:Landroid/graphics/Region$Op;

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    invoke-virtual {v9, v0, v1}, Landroid/graphics/Region;->op(Landroid/graphics/Region;Landroid/graphics/Region$Op;)Z

    move-result v22

    if-eqz v22, :cond_2

    .line 2094
    invoke-virtual {v9}, Landroid/graphics/Region;->getBoundaryPath()Landroid/graphics/Path;

    move-result-object v14

    .line 2098
    :cond_2
    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Lorg/icepdf/core/pobjects/graphics/Shapes;->add(Ljava/lang/Object;)V

    .line 2103
    .end local v7    # "bboxPath":Landroid/graphics/Path;
    .end local v8    # "bboxRect":Landroid/graphics/RectF;
    .end local v9    # "bboxreg":Landroid/graphics/Region;
    .end local v10    # "clipPath":Landroid/graphics/Path;
    .end local v11    # "common":Landroid/graphics/Region;
    .end local v14    # "intersection":Landroid/graphics/Path;
    .end local v15    # "invertMatrix":Landroid/graphics/Matrix;
    .end local v16    # "matrix":Landroid/graphics/Matrix;
    .end local v17    # "shape":Landroid/graphics/Path;
    .end local v18    # "shapeBounds":Landroid/graphics/RectF;
    .end local v19    # "shapereg":Landroid/graphics/Region;
    :cond_3
    :goto_0
    invoke-virtual/range {p2 .. p2}, Lorg/icepdf/core/pobjects/graphics/Shapes;->addClipCommand()V

    .line 2112
    sget-boolean v22, Lorg/icepdf/core/util/ContentParser;->disableTransparencyGroups:Z

    if-nez v22, :cond_9

    invoke-virtual {v12}, Lorg/icepdf/core/pobjects/Form;->isTransparencyGroup()Z

    move-result v22

    if-eqz v22, :cond_9

    invoke-virtual/range {p0 .. p0}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->getFillAlpha()F

    move-result v22

    const/high16 v23, 0x3f800000    # 1.0f

    cmpg-float v22, v22, v23

    if-gez v22, :cond_9

    invoke-virtual {v12}, Lorg/icepdf/core/pobjects/Form;->getBBox()Landroid/graphics/RectF;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Landroid/graphics/RectF;->width()F

    move-result v22

    const v23, 0x46fffe00    # 32767.0f

    cmpg-float v22, v22, v23

    if-gez v22, :cond_9

    invoke-virtual {v12}, Lorg/icepdf/core/pobjects/Form;->getBBox()Landroid/graphics/RectF;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Landroid/graphics/RectF;->height()F

    move-result v22

    const v23, 0x46fffe00    # 32767.0f

    cmpg-float v22, v22, v23

    if-gez v22, :cond_9

    .line 2118
    move-object/from16 v0, p2

    invoke-virtual {v0, v12}, Lorg/icepdf/core/pobjects/graphics/Shapes;->add(Ljava/lang/Object;)V

    .line 2128
    :goto_1
    invoke-virtual {v12}, Lorg/icepdf/core/pobjects/Form;->getShapes()Lorg/icepdf/core/pobjects/graphics/Shapes;

    move-result-object v22

    if-eqz v22, :cond_4

    .line 2129
    invoke-virtual {v12}, Lorg/icepdf/core/pobjects/Form;->getShapes()Lorg/icepdf/core/pobjects/graphics/Shapes;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Lorg/icepdf/core/pobjects/graphics/Shapes;->getImages()Ljava/util/Vector;

    move-result-object v22

    move-object/from16 v0, p2

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lorg/icepdf/core/pobjects/graphics/Shapes;->add(Ljava/lang/Object;)V

    .line 2132
    :cond_4
    invoke-virtual {v12}, Lorg/icepdf/core/pobjects/Form;->getShapes()Lorg/icepdf/core/pobjects/graphics/Shapes;

    move-result-object v22

    if-eqz v22, :cond_5

    invoke-virtual {v12}, Lorg/icepdf/core/pobjects/Form;->getShapes()Lorg/icepdf/core/pobjects/graphics/Shapes;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Lorg/icepdf/core/pobjects/graphics/Shapes;->getPageText()Lorg/icepdf/core/pobjects/graphics/text/PageText;

    move-result-object v22

    if-eqz v22, :cond_5

    .line 2135
    invoke-virtual {v12}, Lorg/icepdf/core/pobjects/Form;->getShapes()Lorg/icepdf/core/pobjects/graphics/Shapes;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Lorg/icepdf/core/pobjects/graphics/Shapes;->getPageText()Lorg/icepdf/core/pobjects/graphics/text/PageText;

    move-result-object v22

    invoke-virtual/range {p0 .. p0}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->getCTM()Landroid/graphics/Matrix;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Lorg/icepdf/core/pobjects/graphics/text/PageText;->applyXObjectTransform(Landroid/graphics/Matrix;)V

    .line 2138
    :cond_5
    invoke-virtual/range {p2 .. p2}, Lorg/icepdf/core/pobjects/graphics/Shapes;->addNoClipCommand()V

    .line 2139
    invoke-virtual {v12}, Lorg/icepdf/core/pobjects/Form;->completed()V

    .line 2141
    const/16 v22, 0x1

    move/from16 v0, v22

    invoke-virtual {v12, v0}, Lorg/icepdf/core/pobjects/Form;->disposeResources(Z)V

    .line 2144
    .end local v6    # "af":Landroid/graphics/Matrix;
    .end local v20    # "xformGraphicsState":Lorg/icepdf/core/pobjects/graphics/GraphicsState;
    :cond_6
    invoke-virtual/range {p0 .. p0}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->restore()Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    move-result-object p0

    .line 2162
    .end local v12    # "formXObject":Lorg/icepdf/core/pobjects/Form;
    :cond_7
    :goto_2
    return-object p0

    .line 2101
    .restart local v6    # "af":Landroid/graphics/Matrix;
    .restart local v12    # "formXObject":Lorg/icepdf/core/pobjects/Form;
    .restart local v20    # "xformGraphicsState":Lorg/icepdf/core/pobjects/graphics/GraphicsState;
    :cond_8
    invoke-virtual {v12}, Lorg/icepdf/core/pobjects/Form;->getBBox()Landroid/graphics/RectF;

    move-result-object v22

    move-object/from16 v0, p2

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lorg/icepdf/core/pobjects/graphics/Shapes;->add(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 2125
    :cond_9
    invoke-virtual {v12}, Lorg/icepdf/core/pobjects/Form;->getShapes()Lorg/icepdf/core/pobjects/graphics/Shapes;

    move-result-object v22

    move-object/from16 v0, p2

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lorg/icepdf/core/pobjects/graphics/Shapes;->add(Ljava/lang/Object;)V

    goto :goto_1

    .line 2147
    .end local v6    # "af":Landroid/graphics/Matrix;
    .end local v12    # "formXObject":Lorg/icepdf/core/pobjects/Form;
    .end local v20    # "xformGraphicsState":Lorg/icepdf/core/pobjects/graphics/GraphicsState;
    :cond_a
    if-eqz p4, :cond_7

    .line 2150
    invoke-virtual/range {p0 .. p0}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->getFillColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v22

    move-object/from16 v0, p3

    move-object/from16 v1, v21

    move-object/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Lorg/icepdf/core/pobjects/Resources;->getImage(Ljava/lang/String;Lorg/apache/poi/java/awt/Color;)Landroid/graphics/Bitmap;

    move-result-object v13

    .line 2152
    .local v13, "im":Landroid/graphics/Bitmap;
    if-eqz v13, :cond_7

    .line 2153
    new-instance v6, Landroid/graphics/Matrix;

    invoke-virtual/range {p0 .. p0}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->getCTM()Landroid/graphics/Matrix;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-direct {v6, v0}, Landroid/graphics/Matrix;-><init>(Landroid/graphics/Matrix;)V

    .line 2155
    .restart local v6    # "af":Landroid/graphics/Matrix;
    const-wide/high16 v22, 0x3ff0000000000000L    # 1.0

    const-wide/high16 v24, -0x4010000000000000L    # -1.0

    move-object/from16 v0, p0

    move-wide/from16 v1, v22

    move-wide/from16 v3, v24

    invoke-virtual {v0, v1, v2, v3, v4}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->scale(DD)V

    .line 2156
    const-wide/16 v22, 0x0

    const-wide/high16 v24, -0x4010000000000000L    # -1.0

    move-object/from16 v0, p0

    move-wide/from16 v1, v22

    move-wide/from16 v3, v24

    invoke-virtual {v0, v1, v2, v3, v4}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->translate(DD)V

    .line 2158
    move-object/from16 v0, p2

    invoke-virtual {v0, v13}, Lorg/icepdf/core/pobjects/graphics/Shapes;->add(Ljava/lang/Object;)V

    .line 2159
    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->set(Landroid/graphics/Matrix;)V

    goto :goto_2
.end method

.method private static consume_G(Lorg/icepdf/core/pobjects/graphics/GraphicsState;Ljava/util/Stack;Lorg/icepdf/core/util/Library;)V
    .locals 3
    .param p0, "graphicState"    # Lorg/icepdf/core/pobjects/graphics/GraphicsState;
    .param p1, "stack"    # Ljava/util/Stack;
    .param p2, "library"    # Lorg/icepdf/core/util/Library;

    .prologue
    .line 1634
    invoke-virtual {p1}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Number;

    invoke-virtual {v1}, Ljava/lang/Number;->floatValue()F

    move-result v0

    .line 1636
    .local v0, "gray":F
    new-instance v1, Lorg/icepdf/core/pobjects/Name;

    const-string/jumbo v2, "DeviceGray"

    invoke-direct {v1, v2}, Lorg/icepdf/core/pobjects/Name;-><init>(Ljava/lang/String;)V

    invoke-static {p2, v1}, Lorg/icepdf/core/pobjects/graphics/PColorSpace;->getColorSpace(Lorg/icepdf/core/util/Library;Ljava/lang/Object;)Lorg/icepdf/core/pobjects/graphics/PColorSpace;

    move-result-object v1

    invoke-virtual {p0, v1}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->setStrokeColorSpace(Lorg/icepdf/core/pobjects/graphics/PColorSpace;)V

    .line 1638
    new-instance v1, Lorg/apache/poi/java/awt/Color;

    invoke-direct {v1, v0, v0, v0}, Lorg/apache/poi/java/awt/Color;-><init>(FFF)V

    invoke-virtual {p0, v1}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->setStrokeColor(Lorg/apache/poi/java/awt/Color;)V

    .line 1639
    return-void
.end method

.method private static consume_J(Lorg/icepdf/core/pobjects/graphics/GraphicsState;Ljava/util/Stack;Lorg/icepdf/core/pobjects/graphics/Shapes;)V
    .locals 1
    .param p0, "graphicState"    # Lorg/icepdf/core/pobjects/graphics/GraphicsState;
    .param p1, "stack"    # Ljava/util/Stack;
    .param p2, "shapes"    # Lorg/icepdf/core/pobjects/graphics/Shapes;

    .prologue
    .line 1972
    invoke-virtual {p1}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->floatValue()F

    move-result v0

    float-to-int v0, v0

    invoke-virtual {p0, v0}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->setLineCap(I)V

    .line 1995
    invoke-static {p2, p0}, Lorg/icepdf/core/util/ContentParser;->setStroke(Lorg/icepdf/core/pobjects/graphics/Shapes;Lorg/icepdf/core/pobjects/graphics/GraphicsState;)V

    .line 1996
    return-void
.end method

.method private static consume_K(Lorg/icepdf/core/pobjects/graphics/GraphicsState;Ljava/util/Stack;Lorg/icepdf/core/util/Library;)V
    .locals 7
    .param p0, "graphicState"    # Lorg/icepdf/core/pobjects/graphics/GraphicsState;
    .param p1, "stack"    # Ljava/util/Stack;
    .param p2, "library"    # Lorg/icepdf/core/util/Library;

    .prologue
    .line 1684
    invoke-virtual {p1}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Number;

    invoke-virtual {v5}, Ljava/lang/Number;->floatValue()F

    move-result v1

    .line 1685
    .local v1, "k":F
    invoke-virtual {p1}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Number;

    invoke-virtual {v5}, Ljava/lang/Number;->floatValue()F

    move-result v4

    .line 1686
    .local v4, "y":F
    invoke-virtual {p1}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Number;

    invoke-virtual {v5}, Ljava/lang/Number;->floatValue()F

    move-result v2

    .line 1687
    .local v2, "m":F
    invoke-virtual {p1}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Number;

    invoke-virtual {v5}, Ljava/lang/Number;->floatValue()F

    move-result v0

    .line 1696
    .local v0, "c":F
    new-instance v5, Lorg/icepdf/core/pobjects/Name;

    const-string/jumbo v6, "DeviceCMYK"

    invoke-direct {v5, v6}, Lorg/icepdf/core/pobjects/Name;-><init>(Ljava/lang/String;)V

    invoke-static {p2, v5}, Lorg/icepdf/core/pobjects/graphics/PColorSpace;->getColorSpace(Lorg/icepdf/core/util/Library;Ljava/lang/Object;)Lorg/icepdf/core/pobjects/graphics/PColorSpace;

    move-result-object v3

    .line 1699
    .local v3, "pColorSpace":Lorg/icepdf/core/pobjects/graphics/PColorSpace;
    invoke-virtual {p0, v3}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->setStrokeColorSpace(Lorg/icepdf/core/pobjects/graphics/PColorSpace;)V

    .line 1701
    const/4 v5, 0x4

    new-array v5, v5, [F

    const/4 v6, 0x0

    aput v0, v5, v6

    const/4 v6, 0x1

    aput v2, v5, v6

    const/4 v6, 0x2

    aput v4, v5, v6

    const/4 v6, 0x3

    aput v1, v5, v6

    invoke-static {v5}, Lorg/icepdf/core/pobjects/graphics/PColorSpace;->reverse([F)[F

    move-result-object v5

    invoke-virtual {v3, v5}, Lorg/icepdf/core/pobjects/graphics/PColorSpace;->getColor([F)Lorg/apache/poi/java/awt/Color;

    move-result-object v5

    invoke-virtual {p0, v5}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->setStrokeColor(Lorg/apache/poi/java/awt/Color;)V

    .line 1702
    return-void
.end method

.method private static consume_M(Lorg/icepdf/core/pobjects/graphics/GraphicsState;Ljava/util/Stack;Lorg/icepdf/core/pobjects/graphics/Shapes;)V
    .locals 1
    .param p0, "graphicState"    # Lorg/icepdf/core/pobjects/graphics/GraphicsState;
    .param p1, "stack"    # Ljava/util/Stack;
    .param p2, "shapes"    # Lorg/icepdf/core/pobjects/graphics/Shapes;

    .prologue
    .line 2238
    invoke-virtual {p1}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->floatValue()F

    move-result v0

    invoke-virtual {p0, v0}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->setMiterLimit(F)V

    .line 2239
    invoke-static {p2, p0}, Lorg/icepdf/core/util/ContentParser;->setStroke(Lorg/icepdf/core/pobjects/graphics/Shapes;Lorg/icepdf/core/pobjects/graphics/GraphicsState;)V

    .line 2240
    return-void
.end method

.method private static consume_Q(Lorg/icepdf/core/pobjects/graphics/GraphicsState;Lorg/icepdf/core/pobjects/graphics/Shapes;)Lorg/icepdf/core/pobjects/graphics/GraphicsState;
    .locals 2
    .param p0, "graphicState"    # Lorg/icepdf/core/pobjects/graphics/GraphicsState;
    .param p1, "shapes"    # Lorg/icepdf/core/pobjects/graphics/Shapes;

    .prologue
    .line 1917
    invoke-virtual {p0}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->restore()Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    move-result-object v0

    .line 1919
    .local v0, "gs1":Lorg/icepdf/core/pobjects/graphics/GraphicsState;
    if-eqz v0, :cond_0

    .line 1920
    move-object p0, v0

    .line 1929
    :goto_0
    return-object p0

    .line 1924
    :cond_0
    new-instance p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    .end local p0    # "graphicState":Lorg/icepdf/core/pobjects/graphics/GraphicsState;
    invoke-direct {p0, p1}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;-><init>(Lorg/icepdf/core/pobjects/graphics/Shapes;)V

    .line 1925
    .restart local p0    # "graphicState":Lorg/icepdf/core/pobjects/graphics/GraphicsState;
    new-instance v1, Landroid/graphics/Matrix;

    invoke-direct {v1}, Landroid/graphics/Matrix;-><init>()V

    invoke-virtual {p0, v1}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->set(Landroid/graphics/Matrix;)V

    .line 1926
    invoke-virtual {p1}, Lorg/icepdf/core/pobjects/graphics/Shapes;->addNoClipCommand()V

    goto :goto_0
.end method

.method private static consume_RG(Lorg/icepdf/core/pobjects/graphics/GraphicsState;Ljava/util/Stack;Lorg/icepdf/core/util/Library;)V
    .locals 6
    .param p0, "graphicState"    # Lorg/icepdf/core/pobjects/graphics/GraphicsState;
    .param p1, "stack"    # Ljava/util/Stack;
    .param p2, "library"    # Lorg/icepdf/core/util/Library;

    .prologue
    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v4, 0x0

    .line 1654
    invoke-virtual {p1}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Number;

    invoke-virtual {v3}, Ljava/lang/Number;->floatValue()F

    move-result v0

    .line 1655
    .local v0, "b":F
    invoke-virtual {p1}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Number;

    invoke-virtual {v3}, Ljava/lang/Number;->floatValue()F

    move-result v1

    .line 1656
    .local v1, "gg":F
    invoke-virtual {p1}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Number;

    invoke-virtual {v3}, Ljava/lang/Number;->floatValue()F

    move-result v2

    .line 1657
    .local v2, "r":F
    invoke-static {v5, v0}, Ljava/lang/Math;->min(FF)F

    move-result v3

    invoke-static {v4, v3}, Ljava/lang/Math;->max(FF)F

    move-result v0

    .line 1658
    invoke-static {v5, v1}, Ljava/lang/Math;->min(FF)F

    move-result v3

    invoke-static {v4, v3}, Ljava/lang/Math;->max(FF)F

    move-result v1

    .line 1659
    invoke-static {v5, v2}, Ljava/lang/Math;->min(FF)F

    move-result v3

    invoke-static {v4, v3}, Ljava/lang/Math;->max(FF)F

    move-result v2

    .line 1661
    new-instance v3, Lorg/icepdf/core/pobjects/Name;

    const-string/jumbo v4, "DeviceRGB"

    invoke-direct {v3, v4}, Lorg/icepdf/core/pobjects/Name;-><init>(Ljava/lang/String;)V

    invoke-static {p2, v3}, Lorg/icepdf/core/pobjects/graphics/PColorSpace;->getColorSpace(Lorg/icepdf/core/util/Library;Ljava/lang/Object;)Lorg/icepdf/core/pobjects/graphics/PColorSpace;

    move-result-object v3

    invoke-virtual {p0, v3}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->setStrokeColorSpace(Lorg/icepdf/core/pobjects/graphics/PColorSpace;)V

    .line 1663
    new-instance v3, Lorg/apache/poi/java/awt/Color;

    invoke-direct {v3, v2, v1, v0}, Lorg/apache/poi/java/awt/Color;-><init>(FFF)V

    invoke-virtual {p0, v3}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->setStrokeColor(Lorg/apache/poi/java/awt/Color;)V

    .line 1664
    return-void
.end method

.method private static consume_SC(Lorg/icepdf/core/pobjects/graphics/GraphicsState;Ljava/util/Stack;Lorg/icepdf/core/util/Library;Lorg/icepdf/core/pobjects/Resources;)V
    .locals 12
    .param p0, "graphicState"    # Lorg/icepdf/core/pobjects/graphics/GraphicsState;
    .param p1, "stack"    # Ljava/util/Stack;
    .param p2, "library"    # Lorg/icepdf/core/util/Library;
    .param p3, "resources"    # Lorg/icepdf/core/pobjects/Resources;

    .prologue
    .line 1748
    invoke-virtual {p1}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v5

    .line 1750
    .local v5, "o":Ljava/lang/Object;
    instance-of v10, v5, Lorg/icepdf/core/pobjects/Name;

    if-eqz v10, :cond_3

    .line 1751
    invoke-virtual {p1}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lorg/icepdf/core/pobjects/Name;

    .line 1752
    .local v7, "patternName":Lorg/icepdf/core/pobjects/Name;
    invoke-virtual {v7}, Lorg/icepdf/core/pobjects/Name;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p3, v10}, Lorg/icepdf/core/pobjects/Resources;->getPattern(Ljava/lang/String;)Lorg/icepdf/core/pobjects/graphics/Pattern;

    move-result-object v6

    .line 1756
    .local v6, "pattern":Lorg/icepdf/core/pobjects/graphics/Pattern;
    invoke-virtual {p0}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->getStrokeColorSpace()Lorg/icepdf/core/pobjects/graphics/PColorSpace;

    move-result-object v10

    instance-of v10, v10, Lorg/icepdf/core/pobjects/graphics/PatternColor;

    if-eqz v10, :cond_0

    .line 1757
    invoke-virtual {p0}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->getStrokeColorSpace()Lorg/icepdf/core/pobjects/graphics/PColorSpace;

    move-result-object v8

    check-cast v8, Lorg/icepdf/core/pobjects/graphics/PatternColor;

    .line 1758
    .local v8, "pc":Lorg/icepdf/core/pobjects/graphics/PatternColor;
    invoke-virtual {v8, v6}, Lorg/icepdf/core/pobjects/graphics/PatternColor;->setPattern(Lorg/icepdf/core/pobjects/graphics/Pattern;)V

    .line 1769
    :goto_0
    instance-of v10, v6, Lorg/icepdf/core/pobjects/graphics/TilingPattern;

    if-eqz v10, :cond_2

    move-object v9, v6

    .line 1770
    check-cast v9, Lorg/icepdf/core/pobjects/graphics/TilingPattern;

    .line 1771
    .local v9, "tilingPattern":Lorg/icepdf/core/pobjects/graphics/TilingPattern;
    invoke-virtual {v9}, Lorg/icepdf/core/pobjects/graphics/TilingPattern;->getPaintType()I

    move-result v10

    const/4 v11, 0x2

    if-ne v10, v11, :cond_2

    .line 1775
    invoke-virtual {p0}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->getStrokeColorSpace()Lorg/icepdf/core/pobjects/graphics/PColorSpace;

    move-result-object v10

    invoke-virtual {v10}, Lorg/icepdf/core/pobjects/graphics/PColorSpace;->getNumComponents()I

    move-result v2

    .line 1777
    .local v2, "compLength":I
    const/4 v4, 0x0

    .line 1779
    .local v4, "nCount":I
    new-array v1, v2, [F

    .line 1781
    .local v1, "colour":[F
    :goto_1
    invoke-virtual {p1}, Ljava/util/Stack;->isEmpty()Z

    move-result v10

    if-nez v10, :cond_1

    invoke-virtual {p1}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v10

    instance-of v10, v10, Ljava/lang/Number;

    if-eqz v10, :cond_1

    if-ge v4, v2, :cond_1

    .line 1783
    invoke-virtual {p1}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Number;

    invoke-virtual {v10}, Ljava/lang/Number;->floatValue()F

    move-result v10

    aput v10, v1, v4

    .line 1784
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 1760
    .end local v1    # "colour":[F
    .end local v2    # "compLength":I
    .end local v4    # "nCount":I
    .end local v8    # "pc":Lorg/icepdf/core/pobjects/graphics/PatternColor;
    .end local v9    # "tilingPattern":Lorg/icepdf/core/pobjects/graphics/TilingPattern;
    :cond_0
    new-instance v8, Lorg/icepdf/core/pobjects/graphics/PatternColor;

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-direct {v8, v10, v11}, Lorg/icepdf/core/pobjects/graphics/PatternColor;-><init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V

    .line 1761
    .restart local v8    # "pc":Lorg/icepdf/core/pobjects/graphics/PatternColor;
    invoke-virtual {v8, v6}, Lorg/icepdf/core/pobjects/graphics/PatternColor;->setPattern(Lorg/icepdf/core/pobjects/graphics/Pattern;)V

    .line 1762
    invoke-virtual {p0, v8}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->setStrokeColorSpace(Lorg/icepdf/core/pobjects/graphics/PColorSpace;)V

    goto :goto_0

    .line 1786
    .restart local v1    # "colour":[F
    .restart local v2    # "compLength":I
    .restart local v4    # "nCount":I
    .restart local v9    # "tilingPattern":Lorg/icepdf/core/pobjects/graphics/TilingPattern;
    :cond_1
    invoke-virtual {p0}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->getStrokeColorSpace()Lorg/icepdf/core/pobjects/graphics/PColorSpace;

    move-result-object v10

    invoke-virtual {v10, v1}, Lorg/icepdf/core/pobjects/graphics/PColorSpace;->getColor([F)Lorg/apache/poi/java/awt/Color;

    move-result-object v10

    invoke-virtual {p0, v10}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->setStrokeColor(Lorg/apache/poi/java/awt/Color;)V

    .line 1787
    invoke-virtual {p0}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->getStrokeColorSpace()Lorg/icepdf/core/pobjects/graphics/PColorSpace;

    move-result-object v10

    invoke-virtual {v10, v1}, Lorg/icepdf/core/pobjects/graphics/PColorSpace;->getColor([F)Lorg/apache/poi/java/awt/Color;

    move-result-object v10

    invoke-virtual {v9, v10}, Lorg/icepdf/core/pobjects/graphics/TilingPattern;->setUnColored(Lorg/apache/poi/java/awt/Color;)V

    .line 1824
    .end local v1    # "colour":[F
    .end local v2    # "compLength":I
    .end local v4    # "nCount":I
    .end local v6    # "pattern":Lorg/icepdf/core/pobjects/graphics/Pattern;
    .end local v7    # "patternName":Lorg/icepdf/core/pobjects/Name;
    .end local v8    # "pc":Lorg/icepdf/core/pobjects/graphics/PatternColor;
    .end local v9    # "tilingPattern":Lorg/icepdf/core/pobjects/graphics/TilingPattern;
    :cond_2
    :goto_2
    return-void

    .line 1791
    :cond_3
    instance-of v10, v5, Ljava/lang/Number;

    if-eqz v10, :cond_2

    .line 1799
    invoke-virtual {p0}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->getStrokeColorSpace()Lorg/icepdf/core/pobjects/graphics/PColorSpace;

    move-result-object v10

    invoke-virtual {v10}, Lorg/icepdf/core/pobjects/graphics/PColorSpace;->getNumComponents()I

    move-result v0

    .line 1802
    .local v0, "colorSpaceN":I
    const/4 v4, 0x0

    .line 1804
    .restart local v4    # "nCount":I
    const/4 v2, 0x4

    .line 1805
    .restart local v2    # "compLength":I
    new-array v1, v2, [F

    .line 1807
    .restart local v1    # "colour":[F
    :goto_3
    invoke-virtual {p1}, Ljava/util/Stack;->isEmpty()Z

    move-result v10

    if-nez v10, :cond_4

    invoke-virtual {p1}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v10

    instance-of v10, v10, Ljava/lang/Number;

    if-eqz v10, :cond_4

    if-ge v4, v2, :cond_4

    .line 1809
    invoke-virtual {p1}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Number;

    invoke-virtual {v10}, Ljava/lang/Number;->floatValue()F

    move-result v10

    aput v10, v1, v4

    .line 1810
    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    .line 1814
    :cond_4
    if-eq v4, v0, :cond_5

    .line 1816
    int-to-float v10, v4

    invoke-static {p2, v10}, Lorg/icepdf/core/pobjects/graphics/PColorSpace;->getColorSpace(Lorg/icepdf/core/util/Library;F)Lorg/icepdf/core/pobjects/graphics/PColorSpace;

    move-result-object v10

    invoke-virtual {p0, v10}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->setStrokeColorSpace(Lorg/icepdf/core/pobjects/graphics/PColorSpace;)V

    .line 1820
    :cond_5
    new-array v3, v4, [F

    .line 1821
    .local v3, "f":[F
    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-static {v1, v10, v3, v11, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1822
    invoke-virtual {p0}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->getStrokeColorSpace()Lorg/icepdf/core/pobjects/graphics/PColorSpace;

    move-result-object v10

    invoke-virtual {v10, v3}, Lorg/icepdf/core/pobjects/graphics/PColorSpace;->getColor([F)Lorg/apache/poi/java/awt/Color;

    move-result-object v10

    invoke-virtual {p0, v10}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->setStrokeColor(Lorg/apache/poi/java/awt/Color;)V

    goto :goto_2
.end method

.method private static consume_TL(Lorg/icepdf/core/pobjects/graphics/GraphicsState;Ljava/util/Stack;)V
    .locals 2
    .param p0, "graphicState"    # Lorg/icepdf/core/pobjects/graphics/GraphicsState;
    .param p1, "stack"    # Ljava/util/Stack;

    .prologue
    .line 2326
    invoke-virtual {p0}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->getTextState()Lorg/icepdf/core/pobjects/graphics/TextState;

    move-result-object v1

    invoke-virtual {p1}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->floatValue()F

    move-result v0

    iput v0, v1, Lorg/icepdf/core/pobjects/graphics/TextState;->leading:F

    .line 2327
    return-void
.end method

.method private static consume_Tc(Lorg/icepdf/core/pobjects/graphics/GraphicsState;Ljava/util/Stack;)V
    .locals 2
    .param p0, "graphicState"    # Lorg/icepdf/core/pobjects/graphics/GraphicsState;
    .param p1, "stack"    # Ljava/util/Stack;

    .prologue
    .line 2300
    invoke-virtual {p0}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->getTextState()Lorg/icepdf/core/pobjects/graphics/TextState;

    move-result-object v1

    invoke-virtual {p1}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->floatValue()F

    move-result v0

    iput v0, v1, Lorg/icepdf/core/pobjects/graphics/TextState;->cspace:F

    .line 2301
    return-void
.end method

.method private static consume_Tf(Lorg/icepdf/core/pobjects/graphics/GraphicsState;Ljava/util/Stack;Lorg/icepdf/core/pobjects/Resources;)V
    .locals 10
    .param p0, "graphicState"    # Lorg/icepdf/core/pobjects/graphics/GraphicsState;
    .param p1, "stack"    # Ljava/util/Stack;
    .param p2, "resources"    # Lorg/icepdf/core/pobjects/Resources;

    .prologue
    .line 2259
    invoke-virtual {p1}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Number;

    invoke-virtual {v7}, Ljava/lang/Number;->floatValue()F

    move-result v6

    .line 2260
    .local v6, "size":F
    invoke-virtual {p1}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/icepdf/core/pobjects/Name;

    .line 2262
    .local v3, "name2":Lorg/icepdf/core/pobjects/Name;
    invoke-virtual {p0}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->getTextState()Lorg/icepdf/core/pobjects/graphics/TextState;

    move-result-object v7

    invoke-virtual {v3}, Lorg/icepdf/core/pobjects/Name;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p2, v8}, Lorg/icepdf/core/pobjects/Resources;->getFont(Ljava/lang/String;)Lorg/icepdf/core/pobjects/fonts/Font;

    move-result-object v8

    iput-object v8, v7, Lorg/icepdf/core/pobjects/graphics/TextState;->font:Lorg/icepdf/core/pobjects/fonts/Font;

    .line 2265
    invoke-virtual {p0}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->getTextState()Lorg/icepdf/core/pobjects/graphics/TextState;

    move-result-object v7

    iget-object v7, v7, Lorg/icepdf/core/pobjects/graphics/TextState;->font:Lorg/icepdf/core/pobjects/fonts/Font;

    if-eqz v7, :cond_0

    invoke-virtual {p0}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->getTextState()Lorg/icepdf/core/pobjects/graphics/TextState;

    move-result-object v7

    iget-object v7, v7, Lorg/icepdf/core/pobjects/graphics/TextState;->font:Lorg/icepdf/core/pobjects/fonts/Font;

    invoke-virtual {v7}, Lorg/icepdf/core/pobjects/fonts/Font;->getFont()Lorg/icepdf/core/pobjects/fonts/FontFile;

    move-result-object v7

    if-nez v7, :cond_3

    .line 2268
    :cond_0
    invoke-static {}, Lorg/icepdf/core/pobjects/fonts/FontFactory;->getInstance()Lorg/icepdf/core/pobjects/fonts/FontFactory;

    move-result-object v1

    .line 2269
    .local v1, "fontFactory":Lorg/icepdf/core/pobjects/fonts/FontFactory;
    invoke-virtual {v1}, Lorg/icepdf/core/pobjects/fonts/FontFactory;->isAwtFontSubstitution()Z

    move-result v0

    .line 2270
    .local v0, "awtState":Z
    const/4 v7, 0x1

    invoke-virtual {v1, v7}, Lorg/icepdf/core/pobjects/fonts/FontFactory;->setAwtFontSubstitution(Z)V

    .line 2272
    invoke-virtual {p2}, Lorg/icepdf/core/pobjects/Resources;->getLibrary()Lorg/icepdf/core/util/Library;

    move-result-object v7

    invoke-virtual {v7}, Lorg/icepdf/core/util/Library;->getCatalog()Lorg/icepdf/core/pobjects/Catalog;

    move-result-object v7

    invoke-virtual {v7}, Lorg/icepdf/core/pobjects/Catalog;->getPageTree()Lorg/icepdf/core/pobjects/PageTree;

    move-result-object v7

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual {v7, v8, v9}, Lorg/icepdf/core/pobjects/PageTree;->getPage(ILjava/lang/Object;)Lorg/icepdf/core/pobjects/Page;

    move-result-object v7

    invoke-virtual {v7}, Lorg/icepdf/core/pobjects/Page;->getResources()Lorg/icepdf/core/pobjects/Resources;

    move-result-object v5

    .line 2274
    .local v5, "res":Lorg/icepdf/core/pobjects/Resources;
    invoke-virtual {v5}, Lorg/icepdf/core/pobjects/Resources;->getEntries()Ljava/util/Hashtable;

    move-result-object v7

    const-string/jumbo v8, "Font"

    invoke-virtual {v7, v8}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    .line 2276
    .local v4, "pageFonts":Ljava/lang/Object;
    if-nez v4, :cond_1

    .line 2278
    invoke-virtual {v5}, Lorg/icepdf/core/pobjects/Resources;->getEntries()Ljava/util/Hashtable;

    move-result-object v7

    new-instance v8, Lorg/icepdf/core/pobjects/Name;

    const-string/jumbo v9, "Font"

    invoke-direct {v8, v9}, Lorg/icepdf/core/pobjects/Name;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v8}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    .line 2281
    :cond_1
    instance-of v7, v4, Ljava/util/Hashtable;

    if-eqz v7, :cond_2

    .line 2283
    check-cast v4, Ljava/util/Hashtable;

    .end local v4    # "pageFonts":Ljava/lang/Object;
    invoke-virtual {v4, v3}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/icepdf/core/pobjects/Reference;

    .line 2284
    .local v2, "fontRef":Lorg/icepdf/core/pobjects/Reference;
    invoke-virtual {p0}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->getTextState()Lorg/icepdf/core/pobjects/graphics/TextState;

    move-result-object v8

    invoke-virtual {p2}, Lorg/icepdf/core/pobjects/Resources;->getLibrary()Lorg/icepdf/core/util/Library;

    move-result-object v7

    invoke-virtual {v7, v2}, Lorg/icepdf/core/util/Library;->getObject(Lorg/icepdf/core/pobjects/Reference;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lorg/icepdf/core/pobjects/fonts/Font;

    iput-object v7, v8, Lorg/icepdf/core/pobjects/graphics/TextState;->font:Lorg/icepdf/core/pobjects/fonts/Font;

    .line 2288
    invoke-virtual {p0}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->getTextState()Lorg/icepdf/core/pobjects/graphics/TextState;

    move-result-object v7

    iget-object v7, v7, Lorg/icepdf/core/pobjects/graphics/TextState;->font:Lorg/icepdf/core/pobjects/fonts/Font;

    invoke-virtual {v7}, Lorg/icepdf/core/pobjects/fonts/Font;->init()V

    .line 2291
    .end local v2    # "fontRef":Lorg/icepdf/core/pobjects/Reference;
    :cond_2
    invoke-virtual {v1, v0}, Lorg/icepdf/core/pobjects/fonts/FontFactory;->setAwtFontSubstitution(Z)V

    .line 2294
    .end local v0    # "awtState":Z
    .end local v1    # "fontFactory":Lorg/icepdf/core/pobjects/fonts/FontFactory;
    .end local v5    # "res":Lorg/icepdf/core/pobjects/Resources;
    :cond_3
    invoke-virtual {p0}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->getTextState()Lorg/icepdf/core/pobjects/graphics/TextState;

    move-result-object v7

    invoke-virtual {p0}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->getTextState()Lorg/icepdf/core/pobjects/graphics/TextState;

    move-result-object v8

    iget-object v8, v8, Lorg/icepdf/core/pobjects/graphics/TextState;->font:Lorg/icepdf/core/pobjects/fonts/Font;

    invoke-virtual {v8}, Lorg/icepdf/core/pobjects/fonts/Font;->getFont()Lorg/icepdf/core/pobjects/fonts/FontFile;

    move-result-object v8

    invoke-interface {v8, v6}, Lorg/icepdf/core/pobjects/fonts/FontFile;->deriveFont(F)Lorg/icepdf/core/pobjects/fonts/FontFile;

    move-result-object v8

    iput-object v8, v7, Lorg/icepdf/core/pobjects/graphics/TextState;->currentfont:Lorg/icepdf/core/pobjects/fonts/FontFile;

    .line 2296
    return-void
.end method

.method private static consume_Tr(Lorg/icepdf/core/pobjects/graphics/GraphicsState;Ljava/util/Stack;)V
    .locals 2
    .param p0, "graphicState"    # Lorg/icepdf/core/pobjects/graphics/GraphicsState;
    .param p1, "stack"    # Ljava/util/Stack;

    .prologue
    .line 2321
    invoke-virtual {p0}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->getTextState()Lorg/icepdf/core/pobjects/graphics/TextState;

    move-result-object v1

    invoke-virtual {p1}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->floatValue()F

    move-result v0

    float-to-int v0, v0

    iput v0, v1, Lorg/icepdf/core/pobjects/graphics/TextState;->rmode:I

    .line 2322
    return-void
.end method

.method private static consume_Ts(Lorg/icepdf/core/pobjects/graphics/GraphicsState;Ljava/util/Stack;)V
    .locals 2
    .param p0, "graphicState"    # Lorg/icepdf/core/pobjects/graphics/GraphicsState;
    .param p1, "stack"    # Ljava/util/Stack;

    .prologue
    .line 2331
    invoke-virtual {p0}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->getTextState()Lorg/icepdf/core/pobjects/graphics/TextState;

    move-result-object v1

    invoke-virtual {p1}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->floatValue()F

    move-result v0

    iput v0, v1, Lorg/icepdf/core/pobjects/graphics/TextState;->trise:F

    .line 2332
    return-void
.end method

.method private static consume_Tw(Lorg/icepdf/core/pobjects/graphics/GraphicsState;Ljava/util/Stack;)V
    .locals 2
    .param p0, "graphicState"    # Lorg/icepdf/core/pobjects/graphics/GraphicsState;
    .param p1, "stack"    # Ljava/util/Stack;

    .prologue
    .line 2316
    invoke-virtual {p0}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->getTextState()Lorg/icepdf/core/pobjects/graphics/TextState;

    move-result-object v1

    invoke-virtual {p1}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->floatValue()F

    move-result v0

    iput v0, v1, Lorg/icepdf/core/pobjects/graphics/TextState;->wspace:F

    .line 2317
    return-void
.end method

.method private static consume_Tz(Lorg/icepdf/core/pobjects/graphics/GraphicsState;Ljava/util/Stack;Z)V
    .locals 4
    .param p0, "graphicState"    # Lorg/icepdf/core/pobjects/graphics/GraphicsState;
    .param p1, "stack"    # Ljava/util/Stack;
    .param p2, "inTextBlock"    # Z

    .prologue
    .line 2306
    invoke-virtual {p1}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v1

    .line 2307
    .local v1, "ob":Ljava/lang/Object;
    instance-of v2, v1, Ljava/lang/Number;

    if-eqz v2, :cond_0

    .line 2308
    check-cast v1, Ljava/lang/Number;

    .end local v1    # "ob":Ljava/lang/Object;
    invoke-virtual {v1}, Ljava/lang/Number;->floatValue()F

    move-result v0

    .line 2310
    .local v0, "hScaling":F
    invoke-virtual {p0}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->getTextState()Lorg/icepdf/core/pobjects/graphics/TextState;

    move-result-object v2

    const/high16 v3, 0x42c80000    # 100.0f

    div-float v3, v0, v3

    iput v3, v2, Lorg/icepdf/core/pobjects/graphics/TextState;->hScalling:F

    .line 2312
    .end local v0    # "hScaling":F
    :cond_0
    return-void
.end method

.method private static consume_cm(Lorg/icepdf/core/pobjects/graphics/GraphicsState;Ljava/util/Stack;ZLandroid/graphics/Matrix;)V
    .locals 9
    .param p0, "graphicState"    # Lorg/icepdf/core/pobjects/graphics/GraphicsState;
    .param p1, "stack"    # Ljava/util/Stack;
    .param p2, "inTextBlock"    # Z
    .param p3, "textBlockBase"    # Landroid/graphics/Matrix;

    .prologue
    .line 1935
    invoke-virtual {p1}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Number;

    invoke-virtual {v7}, Ljava/lang/Number;->floatValue()F

    move-result v5

    .line 1936
    .local v5, "f":F
    invoke-virtual {p1}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Number;

    invoke-virtual {v7}, Ljava/lang/Number;->floatValue()F

    move-result v4

    .line 1937
    .local v4, "e":F
    invoke-virtual {p1}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Number;

    invoke-virtual {v7}, Ljava/lang/Number;->floatValue()F

    move-result v3

    .line 1938
    .local v3, "d":F
    invoke-virtual {p1}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Number;

    invoke-virtual {v7}, Ljava/lang/Number;->floatValue()F

    move-result v2

    .line 1939
    .local v2, "c":F
    invoke-virtual {p1}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Number;

    invoke-virtual {v7}, Ljava/lang/Number;->floatValue()F

    move-result v1

    .line 1940
    .local v1, "b":F
    invoke-virtual {p1}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Number;

    invoke-virtual {v7}, Ljava/lang/Number;->floatValue()F

    move-result v0

    .line 1942
    .local v0, "a":F
    new-instance v6, Landroid/graphics/Matrix;

    invoke-virtual {p0}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->getCTM()Landroid/graphics/Matrix;

    move-result-object v7

    invoke-direct {v6, v7}, Landroid/graphics/Matrix;-><init>(Landroid/graphics/Matrix;)V

    .line 1945
    .local v6, "af":Landroid/graphics/Matrix;
    invoke-static/range {v0 .. v5}, Lcom/samsung/thumbnail/util/Utils;->createMatrixFromValues(FFFFFF)Landroid/graphics/Matrix;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/graphics/Matrix;->preConcat(Landroid/graphics/Matrix;)Z

    .line 1947
    invoke-virtual {p0, v6}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->set(Landroid/graphics/Matrix;)V

    .line 1949
    invoke-static/range {v0 .. v5}, Lcom/samsung/thumbnail/util/Utils;->createMatrixFromValues(FFFFFF)Landroid/graphics/Matrix;

    move-result-object v7

    invoke-virtual {p0, v7}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->updateClipCM(Landroid/graphics/Matrix;)V

    .line 1951
    if-eqz p2, :cond_0

    .line 1953
    new-instance v6, Landroid/graphics/Matrix;

    .end local v6    # "af":Landroid/graphics/Matrix;
    invoke-direct {v6, p3}, Landroid/graphics/Matrix;-><init>(Landroid/graphics/Matrix;)V

    .line 1955
    .restart local v6    # "af":Landroid/graphics/Matrix;
    invoke-virtual {p0}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->getTextState()Lorg/icepdf/core/pobjects/graphics/TextState;

    move-result-object v7

    invoke-static/range {v0 .. v5}, Lcom/samsung/thumbnail/util/Utils;->createMatrixFromValues(FFFFFF)Landroid/graphics/Matrix;

    move-result-object v8

    iput-object v8, v7, Lorg/icepdf/core/pobjects/graphics/TextState;->tmatrix:Landroid/graphics/Matrix;

    .line 1956
    invoke-virtual {p0}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->getTextState()Lorg/icepdf/core/pobjects/graphics/TextState;

    move-result-object v7

    iget-object v7, v7, Lorg/icepdf/core/pobjects/graphics/TextState;->tmatrix:Landroid/graphics/Matrix;

    invoke-virtual {v6, v7}, Landroid/graphics/Matrix;->preConcat(Landroid/graphics/Matrix;)Z

    .line 1957
    invoke-virtual {p0, v6}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->set(Landroid/graphics/Matrix;)V

    .line 1960
    new-instance v7, Landroid/graphics/Matrix;

    invoke-virtual {p0}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->getCTM()Landroid/graphics/Matrix;

    move-result-object v8

    invoke-direct {v7, v8}, Landroid/graphics/Matrix;-><init>(Landroid/graphics/Matrix;)V

    invoke-virtual {p3, v7}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 1962
    :cond_0
    return-void
.end method

.method private static consume_cs(Lorg/icepdf/core/pobjects/graphics/GraphicsState;Ljava/util/Stack;Lorg/icepdf/core/pobjects/Resources;)V
    .locals 2
    .param p0, "graphicState"    # Lorg/icepdf/core/pobjects/graphics/GraphicsState;
    .param p1, "stack"    # Ljava/util/Stack;
    .param p2, "resources"    # Lorg/icepdf/core/pobjects/Resources;

    .prologue
    .line 1738
    invoke-virtual {p1}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/icepdf/core/pobjects/Name;

    .line 1741
    .local v0, "n":Lorg/icepdf/core/pobjects/Name;
    invoke-virtual {p2, v0}, Lorg/icepdf/core/pobjects/Resources;->getColorSpace(Ljava/lang/Object;)Lorg/icepdf/core/pobjects/graphics/PColorSpace;

    move-result-object v1

    invoke-virtual {p0, v1}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->setFillColorSpace(Lorg/icepdf/core/pobjects/graphics/PColorSpace;)V

    .line 1742
    return-void
.end method

.method private static consume_d(Lorg/icepdf/core/pobjects/graphics/GraphicsState;Ljava/util/Stack;Lorg/icepdf/core/pobjects/graphics/Shapes;)V
    .locals 9
    .param p0, "graphicState"    # Lorg/icepdf/core/pobjects/graphics/GraphicsState;
    .param p1, "stack"    # Ljava/util/Stack;
    .param p2, "shapes"    # Lorg/icepdf/core/pobjects/graphics/Shapes;

    .prologue
    .line 2171
    :try_start_0
    invoke-virtual {p1}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Number;

    invoke-virtual {v6}, Ljava/lang/Number;->floatValue()F

    move-result v6

    invoke-static {v6}, Ljava/lang/Math;->abs(F)F

    move-result v1

    .line 2173
    .local v1, "dashPhase":F
    invoke-virtual {p1}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Vector;

    .line 2176
    .local v2, "dashVector":Ljava/util/Vector;
    invoke-virtual {v2}, Ljava/util/Vector;->size()I

    move-result v6

    if-lez v6, :cond_0

    .line 2178
    invoke-virtual {v2}, Ljava/util/Vector;->size()I

    move-result v5

    .line 2179
    .local v5, "sz":I
    new-array v0, v5, [F

    .line 2180
    .local v0, "dashArray":[F
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    if-ge v4, v5, :cond_1

    .line 2181
    invoke-virtual {v2, v4}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Number;

    invoke-virtual {v6}, Ljava/lang/Number;->floatValue()F

    move-result v6

    invoke-static {v6}, Ljava/lang/Math;->abs(F)F

    move-result v6

    aput v6, v0, v4

    .line 2180
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 2186
    .end local v0    # "dashArray":[F
    .end local v4    # "i":I
    .end local v5    # "sz":I
    :cond_0
    const/4 v1, 0x0

    .line 2187
    const/4 v0, 0x0

    .line 2191
    .restart local v0    # "dashArray":[F
    :cond_1
    invoke-virtual {p0, v0}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->setDashArray([F)V

    .line 2192
    invoke-virtual {p0, v1}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->setDashPhase(F)V
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2197
    .end local v0    # "dashArray":[F
    .end local v1    # "dashPhase":F
    .end local v2    # "dashVector":Ljava/util/Vector;
    :goto_1
    invoke-static {p2, p0}, Lorg/icepdf/core/util/ContentParser;->setStroke(Lorg/icepdf/core/pobjects/graphics/Shapes;Lorg/icepdf/core/pobjects/graphics/GraphicsState;)V

    .line 2198
    return-void

    .line 2193
    :catch_0
    move-exception v3

    .line 2194
    .local v3, "e":Ljava/lang/ClassCastException;
    sget-object v6, Lorg/icepdf/core/util/ContentParser;->logger:Ljava/util/logging/Logger;

    sget-object v7, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string/jumbo v8, "Dash pattern syntax error: "

    invoke-virtual {v6, v7, v8, v3}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method private static consume_g(Lorg/icepdf/core/pobjects/graphics/GraphicsState;Ljava/util/Stack;Lorg/icepdf/core/util/Library;)V
    .locals 3
    .param p0, "graphicState"    # Lorg/icepdf/core/pobjects/graphics/GraphicsState;
    .param p1, "stack"    # Ljava/util/Stack;
    .param p2, "library"    # Lorg/icepdf/core/util/Library;

    .prologue
    .line 1644
    invoke-virtual {p1}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Number;

    invoke-virtual {v1}, Ljava/lang/Number;->floatValue()F

    move-result v0

    .line 1646
    .local v0, "gray":F
    new-instance v1, Lorg/icepdf/core/pobjects/Name;

    const-string/jumbo v2, "DeviceGray"

    invoke-direct {v1, v2}, Lorg/icepdf/core/pobjects/Name;-><init>(Ljava/lang/String;)V

    invoke-static {p2, v1}, Lorg/icepdf/core/pobjects/graphics/PColorSpace;->getColorSpace(Lorg/icepdf/core/util/Library;Ljava/lang/Object;)Lorg/icepdf/core/pobjects/graphics/PColorSpace;

    move-result-object v1

    invoke-virtual {p0, v1}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->setFillColorSpace(Lorg/icepdf/core/pobjects/graphics/PColorSpace;)V

    .line 1648
    new-instance v1, Lorg/apache/poi/java/awt/Color;

    invoke-direct {v1, v0, v0, v0}, Lorg/apache/poi/java/awt/Color;-><init>(FFF)V

    invoke-virtual {p0, v1}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->setFillColor(Lorg/apache/poi/java/awt/Color;)V

    .line 1649
    return-void
.end method

.method private static consume_gs(Lorg/icepdf/core/pobjects/graphics/GraphicsState;Ljava/util/Stack;Lorg/icepdf/core/pobjects/Resources;)V
    .locals 3
    .param p0, "graphicState"    # Lorg/icepdf/core/pobjects/graphics/GraphicsState;
    .param p1, "stack"    # Ljava/util/Stack;
    .param p2, "resources"    # Lorg/icepdf/core/pobjects/Resources;

    .prologue
    .line 2244
    invoke-virtual {p1}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v1

    .line 2245
    .local v1, "gs":Ljava/lang/Object;
    instance-of v2, v1, Lorg/icepdf/core/pobjects/Name;

    if-eqz v2, :cond_0

    .line 2247
    check-cast v1, Lorg/icepdf/core/pobjects/Name;

    .end local v1    # "gs":Ljava/lang/Object;
    invoke-virtual {v1}, Lorg/icepdf/core/pobjects/Name;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v2}, Lorg/icepdf/core/pobjects/Resources;->getExtGState(Ljava/lang/String;)Lorg/icepdf/core/pobjects/graphics/ExtGState;

    move-result-object v0

    .line 2249
    .local v0, "extGState":Lorg/icepdf/core/pobjects/graphics/ExtGState;
    if-eqz v0, :cond_0

    .line 2250
    invoke-virtual {p0, v0}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->concatenate(Lorg/icepdf/core/pobjects/graphics/ExtGState;)V

    .line 2253
    .end local v0    # "extGState":Lorg/icepdf/core/pobjects/graphics/ExtGState;
    :cond_0
    return-void
.end method

.method private static consume_i(Ljava/util/Stack;)V
    .locals 0
    .param p0, "stack"    # Ljava/util/Stack;

    .prologue
    .line 1966
    invoke-virtual {p0}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    .line 1967
    return-void
.end method

.method private static consume_j(Lorg/icepdf/core/pobjects/graphics/GraphicsState;Ljava/util/Stack;Lorg/icepdf/core/pobjects/graphics/Shapes;)V
    .locals 1
    .param p0, "graphicState"    # Lorg/icepdf/core/pobjects/graphics/GraphicsState;
    .param p1, "stack"    # Ljava/util/Stack;
    .param p2, "shapes"    # Lorg/icepdf/core/pobjects/graphics/Shapes;

    .prologue
    .line 2203
    invoke-virtual {p1}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->floatValue()F

    move-result v0

    float-to-int v0, v0

    invoke-virtual {p0, v0}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->setLineJoin(I)V

    .line 2226
    invoke-static {p2, p0}, Lorg/icepdf/core/util/ContentParser;->setStroke(Lorg/icepdf/core/pobjects/graphics/Shapes;Lorg/icepdf/core/pobjects/graphics/GraphicsState;)V

    .line 2227
    return-void
.end method

.method private static consume_k(Lorg/icepdf/core/pobjects/graphics/GraphicsState;Ljava/util/Stack;Lorg/icepdf/core/util/Library;)V
    .locals 7
    .param p0, "graphicState"    # Lorg/icepdf/core/pobjects/graphics/GraphicsState;
    .param p1, "stack"    # Ljava/util/Stack;
    .param p2, "library"    # Lorg/icepdf/core/util/Library;

    .prologue
    .line 1707
    invoke-virtual {p1}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Number;

    invoke-virtual {v5}, Ljava/lang/Number;->floatValue()F

    move-result v1

    .line 1708
    .local v1, "k":F
    invoke-virtual {p1}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Number;

    invoke-virtual {v5}, Ljava/lang/Number;->floatValue()F

    move-result v4

    .line 1709
    .local v4, "y":F
    invoke-virtual {p1}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Number;

    invoke-virtual {v5}, Ljava/lang/Number;->floatValue()F

    move-result v2

    .line 1710
    .local v2, "m":F
    invoke-virtual {p1}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Number;

    invoke-virtual {v5}, Ljava/lang/Number;->floatValue()F

    move-result v0

    .line 1720
    .local v0, "c":F
    new-instance v5, Lorg/icepdf/core/pobjects/Name;

    const-string/jumbo v6, "DeviceCMYK"

    invoke-direct {v5, v6}, Lorg/icepdf/core/pobjects/Name;-><init>(Ljava/lang/String;)V

    invoke-static {p2, v5}, Lorg/icepdf/core/pobjects/graphics/PColorSpace;->getColorSpace(Lorg/icepdf/core/util/Library;Ljava/lang/Object;)Lorg/icepdf/core/pobjects/graphics/PColorSpace;

    move-result-object v3

    .line 1723
    .local v3, "pColorSpace":Lorg/icepdf/core/pobjects/graphics/PColorSpace;
    invoke-virtual {p0, v3}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->setFillColorSpace(Lorg/icepdf/core/pobjects/graphics/PColorSpace;)V

    .line 1725
    const/4 v5, 0x4

    new-array v5, v5, [F

    const/4 v6, 0x0

    aput v0, v5, v6

    const/4 v6, 0x1

    aput v2, v5, v6

    const/4 v6, 0x2

    aput v4, v5, v6

    const/4 v6, 0x3

    aput v1, v5, v6

    invoke-static {v5}, Lorg/icepdf/core/pobjects/graphics/PColorSpace;->reverse([F)[F

    move-result-object v5

    invoke-virtual {v3, v5}, Lorg/icepdf/core/pobjects/graphics/PColorSpace;->getColor([F)Lorg/apache/poi/java/awt/Color;

    move-result-object v5

    invoke-virtual {p0, v5}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->setFillColor(Lorg/apache/poi/java/awt/Color;)V

    .line 1726
    return-void
.end method

.method private static consume_q(Lorg/icepdf/core/pobjects/graphics/GraphicsState;)Lorg/icepdf/core/pobjects/graphics/GraphicsState;
    .locals 1
    .param p0, "graphicState"    # Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    .prologue
    .line 1911
    invoke-virtual {p0}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->save()Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    move-result-object v0

    return-object v0
.end method

.method private static consume_rg(Lorg/icepdf/core/pobjects/graphics/GraphicsState;Ljava/util/Stack;Lorg/icepdf/core/util/Library;)V
    .locals 6
    .param p0, "graphicState"    # Lorg/icepdf/core/pobjects/graphics/GraphicsState;
    .param p1, "stack"    # Ljava/util/Stack;
    .param p2, "library"    # Lorg/icepdf/core/util/Library;

    .prologue
    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v4, 0x0

    .line 1669
    invoke-virtual {p1}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Number;

    invoke-virtual {v3}, Ljava/lang/Number;->floatValue()F

    move-result v0

    .line 1670
    .local v0, "b":F
    invoke-virtual {p1}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Number;

    invoke-virtual {v3}, Ljava/lang/Number;->floatValue()F

    move-result v1

    .line 1671
    .local v1, "gg":F
    invoke-virtual {p1}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Number;

    invoke-virtual {v3}, Ljava/lang/Number;->floatValue()F

    move-result v2

    .line 1672
    .local v2, "r":F
    invoke-static {v5, v0}, Ljava/lang/Math;->min(FF)F

    move-result v3

    invoke-static {v4, v3}, Ljava/lang/Math;->max(FF)F

    move-result v0

    .line 1673
    invoke-static {v5, v1}, Ljava/lang/Math;->min(FF)F

    move-result v3

    invoke-static {v4, v3}, Ljava/lang/Math;->max(FF)F

    move-result v1

    .line 1674
    invoke-static {v5, v2}, Ljava/lang/Math;->min(FF)F

    move-result v3

    invoke-static {v4, v3}, Ljava/lang/Math;->max(FF)F

    move-result v2

    .line 1676
    new-instance v3, Lorg/icepdf/core/pobjects/Name;

    const-string/jumbo v4, "DeviceRGB"

    invoke-direct {v3, v4}, Lorg/icepdf/core/pobjects/Name;-><init>(Ljava/lang/String;)V

    invoke-static {p2, v3}, Lorg/icepdf/core/pobjects/graphics/PColorSpace;->getColorSpace(Lorg/icepdf/core/util/Library;Ljava/lang/Object;)Lorg/icepdf/core/pobjects/graphics/PColorSpace;

    move-result-object v3

    invoke-virtual {p0, v3}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->setFillColorSpace(Lorg/icepdf/core/pobjects/graphics/PColorSpace;)V

    .line 1678
    new-instance v3, Lorg/apache/poi/java/awt/Color;

    invoke-direct {v3, v2, v1, v0}, Lorg/apache/poi/java/awt/Color;-><init>(FFF)V

    invoke-virtual {p0, v3}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->setFillColor(Lorg/apache/poi/java/awt/Color;)V

    .line 1679
    return-void
.end method

.method private static consume_sc(Lorg/icepdf/core/pobjects/graphics/GraphicsState;Ljava/util/Stack;Lorg/icepdf/core/util/Library;Lorg/icepdf/core/pobjects/Resources;)V
    .locals 12
    .param p0, "graphicState"    # Lorg/icepdf/core/pobjects/graphics/GraphicsState;
    .param p1, "stack"    # Ljava/util/Stack;
    .param p2, "library"    # Lorg/icepdf/core/util/Library;
    .param p3, "resources"    # Lorg/icepdf/core/pobjects/Resources;

    .prologue
    const/4 v11, 0x0

    .line 1831
    invoke-virtual {p1}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v5

    .line 1833
    .local v5, "o":Ljava/lang/Object;
    instance-of v10, v5, Lorg/icepdf/core/pobjects/Name;

    if-eqz v10, :cond_3

    .line 1834
    invoke-virtual {p1}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lorg/icepdf/core/pobjects/Name;

    .line 1835
    .local v7, "patternName":Lorg/icepdf/core/pobjects/Name;
    invoke-virtual {v7}, Lorg/icepdf/core/pobjects/Name;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p3, v10}, Lorg/icepdf/core/pobjects/Resources;->getPattern(Ljava/lang/String;)Lorg/icepdf/core/pobjects/graphics/Pattern;

    move-result-object v6

    .line 1839
    .local v6, "pattern":Lorg/icepdf/core/pobjects/graphics/Pattern;
    invoke-virtual {p0}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->getFillColorSpace()Lorg/icepdf/core/pobjects/graphics/PColorSpace;

    move-result-object v10

    instance-of v10, v10, Lorg/icepdf/core/pobjects/graphics/PatternColor;

    if-eqz v10, :cond_0

    .line 1840
    invoke-virtual {p0}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->getFillColorSpace()Lorg/icepdf/core/pobjects/graphics/PColorSpace;

    move-result-object v8

    check-cast v8, Lorg/icepdf/core/pobjects/graphics/PatternColor;

    .line 1841
    .local v8, "pc":Lorg/icepdf/core/pobjects/graphics/PatternColor;
    invoke-virtual {v8, v6}, Lorg/icepdf/core/pobjects/graphics/PatternColor;->setPattern(Lorg/icepdf/core/pobjects/graphics/Pattern;)V

    .line 1852
    :goto_0
    instance-of v10, v6, Lorg/icepdf/core/pobjects/graphics/TilingPattern;

    if-eqz v10, :cond_2

    move-object v9, v6

    .line 1853
    check-cast v9, Lorg/icepdf/core/pobjects/graphics/TilingPattern;

    .line 1854
    .local v9, "tilingPattern":Lorg/icepdf/core/pobjects/graphics/TilingPattern;
    invoke-virtual {v9}, Lorg/icepdf/core/pobjects/graphics/TilingPattern;->getPaintType()I

    move-result v10

    const/4 v11, 0x2

    if-ne v10, v11, :cond_2

    .line 1858
    invoke-virtual {p0}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->getFillColorSpace()Lorg/icepdf/core/pobjects/graphics/PColorSpace;

    move-result-object v10

    invoke-virtual {v10}, Lorg/icepdf/core/pobjects/graphics/PColorSpace;->getNumComponents()I

    move-result v2

    .line 1860
    .local v2, "compLength":I
    const/4 v4, 0x0

    .line 1862
    .local v4, "nCount":I
    new-array v1, v2, [F

    .line 1864
    .local v1, "colour":[F
    :goto_1
    invoke-virtual {p1}, Ljava/util/Stack;->isEmpty()Z

    move-result v10

    if-nez v10, :cond_1

    invoke-virtual {p1}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v10

    instance-of v10, v10, Ljava/lang/Number;

    if-eqz v10, :cond_1

    if-ge v4, v2, :cond_1

    .line 1866
    invoke-virtual {p1}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Number;

    invoke-virtual {v10}, Ljava/lang/Number;->floatValue()F

    move-result v10

    aput v10, v1, v4

    .line 1867
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 1843
    .end local v1    # "colour":[F
    .end local v2    # "compLength":I
    .end local v4    # "nCount":I
    .end local v8    # "pc":Lorg/icepdf/core/pobjects/graphics/PatternColor;
    .end local v9    # "tilingPattern":Lorg/icepdf/core/pobjects/graphics/TilingPattern;
    :cond_0
    new-instance v8, Lorg/icepdf/core/pobjects/graphics/PatternColor;

    const/4 v10, 0x0

    invoke-direct {v8, p2, v10}, Lorg/icepdf/core/pobjects/graphics/PatternColor;-><init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V

    .line 1844
    .restart local v8    # "pc":Lorg/icepdf/core/pobjects/graphics/PatternColor;
    invoke-virtual {v8, v6}, Lorg/icepdf/core/pobjects/graphics/PatternColor;->setPattern(Lorg/icepdf/core/pobjects/graphics/Pattern;)V

    .line 1845
    invoke-virtual {p0, v8}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->setFillColorSpace(Lorg/icepdf/core/pobjects/graphics/PColorSpace;)V

    goto :goto_0

    .line 1870
    .restart local v1    # "colour":[F
    .restart local v2    # "compLength":I
    .restart local v4    # "nCount":I
    .restart local v9    # "tilingPattern":Lorg/icepdf/core/pobjects/graphics/TilingPattern;
    :cond_1
    invoke-virtual {p0}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->getFillColorSpace()Lorg/icepdf/core/pobjects/graphics/PColorSpace;

    move-result-object v10

    invoke-virtual {v10, v1}, Lorg/icepdf/core/pobjects/graphics/PColorSpace;->getColor([F)Lorg/apache/poi/java/awt/Color;

    move-result-object v10

    invoke-virtual {p0, v10}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->setFillColor(Lorg/apache/poi/java/awt/Color;)V

    .line 1871
    invoke-virtual {p0}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->getFillColorSpace()Lorg/icepdf/core/pobjects/graphics/PColorSpace;

    move-result-object v10

    invoke-virtual {v10, v1}, Lorg/icepdf/core/pobjects/graphics/PColorSpace;->getColor([F)Lorg/apache/poi/java/awt/Color;

    move-result-object v10

    invoke-virtual {v9, v10}, Lorg/icepdf/core/pobjects/graphics/TilingPattern;->setUnColored(Lorg/apache/poi/java/awt/Color;)V

    .line 1907
    .end local v1    # "colour":[F
    .end local v2    # "compLength":I
    .end local v4    # "nCount":I
    .end local v6    # "pattern":Lorg/icepdf/core/pobjects/graphics/Pattern;
    .end local v7    # "patternName":Lorg/icepdf/core/pobjects/Name;
    .end local v8    # "pc":Lorg/icepdf/core/pobjects/graphics/PatternColor;
    .end local v9    # "tilingPattern":Lorg/icepdf/core/pobjects/graphics/TilingPattern;
    :cond_2
    :goto_2
    return-void

    .line 1875
    :cond_3
    instance-of v10, v5, Ljava/lang/Number;

    if-eqz v10, :cond_2

    .line 1882
    invoke-virtual {p0}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->getFillColorSpace()Lorg/icepdf/core/pobjects/graphics/PColorSpace;

    move-result-object v10

    invoke-virtual {v10}, Lorg/icepdf/core/pobjects/graphics/PColorSpace;->getNumComponents()I

    move-result v0

    .line 1885
    .local v0, "colorSpaceN":I
    const/4 v4, 0x0

    .line 1887
    .restart local v4    # "nCount":I
    const/4 v2, 0x4

    .line 1888
    .restart local v2    # "compLength":I
    new-array v1, v2, [F

    .line 1890
    .restart local v1    # "colour":[F
    :goto_3
    invoke-virtual {p1}, Ljava/util/Stack;->isEmpty()Z

    move-result v10

    if-nez v10, :cond_4

    invoke-virtual {p1}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v10

    instance-of v10, v10, Ljava/lang/Number;

    if-eqz v10, :cond_4

    if-ge v4, v2, :cond_4

    .line 1892
    invoke-virtual {p1}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Number;

    invoke-virtual {v10}, Ljava/lang/Number;->floatValue()F

    move-result v10

    aput v10, v1, v4

    .line 1893
    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    .line 1897
    :cond_4
    if-eq v4, v0, :cond_5

    .line 1899
    int-to-float v10, v4

    invoke-static {p2, v10}, Lorg/icepdf/core/pobjects/graphics/PColorSpace;->getColorSpace(Lorg/icepdf/core/util/Library;F)Lorg/icepdf/core/pobjects/graphics/PColorSpace;

    move-result-object v10

    invoke-virtual {p0, v10}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->setFillColorSpace(Lorg/icepdf/core/pobjects/graphics/PColorSpace;)V

    .line 1903
    :cond_5
    new-array v3, v4, [F

    .line 1904
    .local v3, "f":[F
    invoke-static {v1, v11, v3, v11, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1905
    invoke-virtual {p0}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->getFillColorSpace()Lorg/icepdf/core/pobjects/graphics/PColorSpace;

    move-result-object v10

    invoke-virtual {v10, v3}, Lorg/icepdf/core/pobjects/graphics/PColorSpace;->getColor([F)Lorg/apache/poi/java/awt/Color;

    move-result-object v10

    invoke-virtual {p0, v10}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->setFillColor(Lorg/apache/poi/java/awt/Color;)V

    goto :goto_2
.end method

.method private static consume_w(Lorg/icepdf/core/pobjects/graphics/GraphicsState;Ljava/util/Stack;Lorg/icepdf/core/pobjects/graphics/Shapes;)V
    .locals 1
    .param p0, "graphicState"    # Lorg/icepdf/core/pobjects/graphics/GraphicsState;
    .param p1, "stack"    # Ljava/util/Stack;
    .param p2, "shapes"    # Lorg/icepdf/core/pobjects/graphics/Shapes;

    .prologue
    .line 2232
    invoke-virtual {p1}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->floatValue()F

    move-result v0

    invoke-virtual {p0, v0}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->setLineWidth(F)V

    .line 2233
    invoke-static {p2, p0}, Lorg/icepdf/core/util/ContentParser;->setStroke(Lorg/icepdf/core/pobjects/graphics/Shapes;Lorg/icepdf/core/pobjects/graphics/GraphicsState;)V

    .line 2234
    return-void
.end method

.method private drawModeFill(Lorg/icepdf/core/pobjects/graphics/TextSprite;Lorg/icepdf/core/pobjects/graphics/Shapes;I)V
    .locals 3
    .param p1, "textSprites"    # Lorg/icepdf/core/pobjects/graphics/TextSprite;
    .param p2, "shapes"    # Lorg/icepdf/core/pobjects/graphics/Shapes;
    .param p3, "rmode"    # I

    .prologue
    .line 2507
    invoke-virtual {p1, p3}, Lorg/icepdf/core/pobjects/graphics/TextSprite;->setRMode(I)V

    .line 2510
    iget-object v2, p0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    invoke-virtual {v2}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->getFillAlpha()F

    move-result v2

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    .line 2511
    .local v1, "requiredAlpha":Ljava/lang/Float;
    iget-object v2, p0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    invoke-virtual {v2}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->getFillColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v0

    .line 2512
    .local v0, "fillColor":Lorg/apache/poi/java/awt/Color;
    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v2

    invoke-virtual {v0, v2}, Lorg/apache/poi/java/awt/Color;->setFAlpha(F)V

    .line 2513
    invoke-virtual {p2, v0}, Lorg/icepdf/core/pobjects/graphics/Shapes;->add(Ljava/lang/Object;)V

    .line 2514
    invoke-virtual {p2, p1}, Lorg/icepdf/core/pobjects/graphics/Shapes;->add(Ljava/lang/Object;)V

    .line 2515
    return-void
.end method

.method private drawModeFillStroke(Lorg/icepdf/core/pobjects/graphics/TextSprite;Lorg/icepdf/core/pobjects/graphics/TextState;Lorg/icepdf/core/pobjects/graphics/Shapes;I)V
    .locals 4
    .param p1, "textSprites"    # Lorg/icepdf/core/pobjects/graphics/TextSprite;
    .param p2, "textState"    # Lorg/icepdf/core/pobjects/graphics/TextState;
    .param p3, "shapes"    # Lorg/icepdf/core/pobjects/graphics/Shapes;
    .param p4, "rmode"    # I

    .prologue
    .line 2564
    invoke-virtual {p1, p4}, Lorg/icepdf/core/pobjects/graphics/TextSprite;->setRMode(I)V

    .line 2565
    iget-object v3, p0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    invoke-virtual {v3}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->getStrokeColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v3

    invoke-virtual {p1, v3}, Lorg/icepdf/core/pobjects/graphics/TextSprite;->setStrokeColor(Lorg/apache/poi/java/awt/Color;)V

    .line 2567
    iget-object v3, p0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    invoke-virtual {v3}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->getLineWidth()F

    move-result v1

    .line 2570
    .local v1, "old":F
    iget-object v3, p0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    invoke-virtual {v3}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->getLineWidth()F

    move-result v0

    .line 2572
    .local v0, "lineWidth":F
    const/16 v3, 0x9

    new-array v2, v3, [F

    .line 2573
    .local v2, "values1":[F
    iget-object v3, p2, Lorg/icepdf/core/pobjects/graphics/TextState;->tmatrix:Landroid/graphics/Matrix;

    invoke-virtual {v3, v2}, Landroid/graphics/Matrix;->getValues([F)V

    .line 2575
    const/4 v3, 0x0

    aget v3, v2, v3

    div-float/2addr v0, v3

    .line 2576
    iget-object v3, p0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    invoke-virtual {v3, v0}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->setLineWidth(F)V

    .line 2578
    iget-object v3, p0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    invoke-static {p3, v3}, Lorg/icepdf/core/util/ContentParser;->setStroke(Lorg/icepdf/core/pobjects/graphics/Shapes;Lorg/icepdf/core/pobjects/graphics/GraphicsState;)V

    .line 2579
    iget-object v3, p0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    invoke-virtual {v3}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->getFillColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v3

    invoke-virtual {p3, v3}, Lorg/icepdf/core/pobjects/graphics/Shapes;->add(Ljava/lang/Object;)V

    .line 2580
    invoke-virtual {p3, p1}, Lorg/icepdf/core/pobjects/graphics/Shapes;->add(Ljava/lang/Object;)V

    .line 2583
    iget-object v3, p0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    invoke-virtual {v3, v1}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->setLineWidth(F)V

    .line 2584
    iget-object v3, p0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    invoke-static {p3, v3}, Lorg/icepdf/core/util/ContentParser;->setStroke(Lorg/icepdf/core/pobjects/graphics/Shapes;Lorg/icepdf/core/pobjects/graphics/GraphicsState;)V

    .line 2585
    return-void
.end method

.method private drawModeStroke(Lorg/icepdf/core/pobjects/graphics/TextSprite;Lorg/icepdf/core/pobjects/graphics/TextState;Lorg/icepdf/core/pobjects/graphics/Shapes;I)V
    .locals 4
    .param p1, "textSprites"    # Lorg/icepdf/core/pobjects/graphics/TextSprite;
    .param p2, "textState"    # Lorg/icepdf/core/pobjects/graphics/TextState;
    .param p3, "shapes"    # Lorg/icepdf/core/pobjects/graphics/Shapes;
    .param p4, "rmode"    # I

    .prologue
    .line 2529
    invoke-virtual {p1, p4}, Lorg/icepdf/core/pobjects/graphics/TextSprite;->setRMode(I)V

    .line 2530
    iget-object v3, p0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    invoke-virtual {v3}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->getStrokeColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v3

    invoke-virtual {p1, v3}, Lorg/icepdf/core/pobjects/graphics/TextSprite;->setStrokeColor(Lorg/apache/poi/java/awt/Color;)V

    .line 2532
    iget-object v3, p0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    invoke-virtual {v3}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->getLineWidth()F

    move-result v1

    .line 2535
    .local v1, "old":F
    iget-object v3, p0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    invoke-virtual {v3}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->getLineWidth()F

    move-result v0

    .line 2537
    .local v0, "lineWidth":F
    const/16 v3, 0x9

    new-array v2, v3, [F

    .line 2538
    .local v2, "values1":[F
    iget-object v3, p2, Lorg/icepdf/core/pobjects/graphics/TextState;->tmatrix:Landroid/graphics/Matrix;

    invoke-virtual {v3, v2}, Landroid/graphics/Matrix;->getValues([F)V

    .line 2540
    const/4 v3, 0x3

    aget v3, v2, v3

    div-float/2addr v0, v3

    .line 2541
    iget-object v3, p0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    invoke-virtual {v3, v0}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->setLineWidth(F)V

    .line 2543
    iget-object v3, p0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    invoke-static {p3, v3}, Lorg/icepdf/core/util/ContentParser;->setStroke(Lorg/icepdf/core/pobjects/graphics/Shapes;Lorg/icepdf/core/pobjects/graphics/GraphicsState;)V

    .line 2544
    iget-object v3, p0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    invoke-virtual {v3}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->getStrokeColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v3

    invoke-virtual {p3, v3}, Lorg/icepdf/core/pobjects/graphics/Shapes;->add(Ljava/lang/Object;)V

    .line 2545
    invoke-virtual {p3, p1}, Lorg/icepdf/core/pobjects/graphics/Shapes;->add(Ljava/lang/Object;)V

    .line 2548
    iget-object v3, p0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    invoke-virtual {v3, v1}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->setLineWidth(F)V

    .line 2549
    iget-object v3, p0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    invoke-static {p3, v3}, Lorg/icepdf/core/util/ContentParser;->setStroke(Lorg/icepdf/core/pobjects/graphics/Shapes;Lorg/icepdf/core/pobjects/graphics/GraphicsState;)V

    .line 2550
    return-void
.end method

.method private drawString(Ljava/lang/StringBuilder;Landroid/graphics/PointF;FLorg/icepdf/core/pobjects/graphics/TextState;Lorg/icepdf/core/pobjects/graphics/Shapes;Lorg/icepdf/core/pobjects/graphics/GlyphOutlineClip;)Landroid/graphics/PointF;
    .locals 26
    .param p1, "displayText"    # Ljava/lang/StringBuilder;
    .param p2, "advance"    # Landroid/graphics/PointF;
    .param p3, "previousAdvance"    # F
    .param p4, "textState"    # Lorg/icepdf/core/pobjects/graphics/TextState;
    .param p5, "shapes"    # Lorg/icepdf/core/pobjects/graphics/Shapes;
    .param p6, "glyphOutlineClip"    # Lorg/icepdf/core/pobjects/graphics/GlyphOutlineClip;

    .prologue
    .line 2356
    move-object/from16 v0, p2

    iget v10, v0, Landroid/graphics/PointF;->x:F

    .line 2357
    .local v10, "advanceX":F
    move-object/from16 v0, p2

    iget v11, v0, Landroid/graphics/PointF;->y:F

    .line 2359
    .local v11, "advanceY":F
    invoke-virtual/range {p1 .. p1}, Ljava/lang/StringBuilder;->length()I

    move-result v5

    if-nez v5, :cond_0

    .line 2360
    new-instance v5, Landroid/graphics/PointF;

    const/4 v6, 0x0

    move/from16 v0, p3

    invoke-direct {v5, v0, v6}, Landroid/graphics/PointF;-><init>(FF)V

    .line 2495
    :goto_0
    return-object v5

    .line 2364
    :cond_0
    const/16 v19, 0x0

    .local v19, "lastx":F
    const/16 v20, 0x0

    .line 2368
    .local v20, "lasty":F
    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v13

    .line 2370
    .local v13, "currentChar":C
    move-object/from16 v0, p4

    iget-object v5, v0, Lorg/icepdf/core/pobjects/graphics/TextState;->currentfont:Lorg/icepdf/core/pobjects/fonts/FontFile;

    invoke-interface {v5, v13}, Lorg/icepdf/core/pobjects/fonts/FontFile;->echarAdvance(C)Landroid/graphics/PointF;

    move-result-object v5

    iget v15, v5, Landroid/graphics/PointF;->x:F

    .line 2372
    .local v15, "firstCharWidth":F
    add-float v5, v10, v15

    cmpg-float v5, v5, p3

    if-gez v5, :cond_1

    .line 2373
    move/from16 v10, p3

    .line 2377
    :cond_1
    move-object/from16 v0, p4

    iget-object v14, v0, Lorg/icepdf/core/pobjects/graphics/TextState;->currentfont:Lorg/icepdf/core/pobjects/fonts/FontFile;

    .line 2378
    .local v14, "currentFont":Lorg/icepdf/core/pobjects/fonts/FontFile;
    move-object/from16 v0, p4

    iget-object v5, v0, Lorg/icepdf/core/pobjects/graphics/TextState;->font:Lorg/icepdf/core/pobjects/fonts/Font;

    invoke-virtual {v5}, Lorg/icepdf/core/pobjects/fonts/Font;->isVerticalWriting()Z

    move-result v18

    .line 2382
    .local v18, "isVerticalWriting":Z
    move-object/from16 v0, p4

    iget v0, v0, Lorg/icepdf/core/pobjects/graphics/TextState;->trise:F

    move/from16 v24, v0

    .line 2383
    .local v24, "textRise":F
    move-object/from16 v0, p4

    iget v5, v0, Lorg/icepdf/core/pobjects/graphics/TextState;->cspace:F

    move-object/from16 v0, p4

    iget v6, v0, Lorg/icepdf/core/pobjects/graphics/TextState;->hScalling:F

    mul-float v12, v5, v6

    .line 2384
    .local v12, "charcterSpace":F
    move-object/from16 v0, p4

    iget v5, v0, Lorg/icepdf/core/pobjects/graphics/TextState;->wspace:F

    move-object/from16 v0, p4

    iget v6, v0, Lorg/icepdf/core/pobjects/graphics/TextState;->hScalling:F

    mul-float v25, v5, v6

    .line 2385
    .local v25, "whiteSpace":F
    invoke-virtual/range {p1 .. p1}, Ljava/lang/StringBuilder;->length()I

    move-result v23

    .line 2388
    .local v23, "textLength":I
    new-instance v4, Lorg/icepdf/core/pobjects/graphics/TextSprite;

    new-instance v5, Landroid/graphics/Matrix;

    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    invoke-virtual {v6}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->getCTM()Landroid/graphics/Matrix;

    move-result-object v6

    invoke-direct {v5, v6}, Landroid/graphics/Matrix;-><init>(Landroid/graphics/Matrix;)V

    move/from16 v0, v23

    invoke-direct {v4, v14, v0, v5}, Lorg/icepdf/core/pobjects/graphics/TextSprite;-><init>(Lorg/icepdf/core/pobjects/fonts/FontFile;ILandroid/graphics/Matrix;)V

    .line 2398
    .local v4, "textSprites":Lorg/icepdf/core/pobjects/graphics/TextSprite;
    const/16 v17, 0x0

    .local v17, "i":I
    :goto_1
    move/from16 v0, v17

    move/from16 v1, v23

    if-ge v0, v1, :cond_4

    .line 2399
    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v13

    .line 2403
    invoke-interface {v14, v13}, Lorg/icepdf/core/pobjects/fonts/FontFile;->echarAdvance(C)Landroid/graphics/PointF;

    move-result-object v5

    iget v9, v5, Landroid/graphics/PointF;->x:F

    .line 2407
    .local v9, "newAdvanceX":F
    move/from16 v21, v9

    .line 2408
    .local v21, "newAdvanceY":F
    if-nez v18, :cond_3

    .line 2410
    add-float v7, v10, v19

    .line 2411
    .local v7, "currentX":F
    sub-float v8, v20, v24

    .line 2412
    .local v8, "currentY":F
    add-float v19, v19, v9

    .line 2414
    add-float v19, v19, v12

    .line 2416
    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v5

    const/16 v6, 0x20

    if-ne v5, v6, :cond_2

    .line 2418
    add-float v19, v19, v25

    .line 2428
    :cond_2
    :goto_2
    invoke-static {v13}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p4

    iget-object v6, v0, Lorg/icepdf/core/pobjects/graphics/TextState;->currentfont:Lorg/icepdf/core/pobjects/fonts/FontFile;

    invoke-interface {v6, v13}, Lorg/icepdf/core/pobjects/fonts/FontFile;->toUnicode(C)Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {v4 .. v9}, Lorg/icepdf/core/pobjects/graphics/TextSprite;->addText(Ljava/lang/String;Ljava/lang/String;FFF)Lorg/icepdf/core/pobjects/graphics/text/GlyphText;

    move-result-object v16

    .line 2432
    .local v16, "glyphText":Lorg/icepdf/core/pobjects/graphics/text/GlyphText;
    invoke-virtual/range {p5 .. p5}, Lorg/icepdf/core/pobjects/graphics/Shapes;->getPageText()Lorg/icepdf/core/pobjects/graphics/text/PageText;

    move-result-object v5

    move-object/from16 v0, v16

    invoke-virtual {v5, v0}, Lorg/icepdf/core/pobjects/graphics/text/PageText;->addGlyph(Lorg/icepdf/core/pobjects/graphics/text/GlyphText;)V

    .line 2398
    add-int/lit8 v17, v17, 0x1

    goto :goto_1

    .line 2422
    .end local v7    # "currentX":F
    .end local v8    # "currentY":F
    .end local v16    # "glyphText":Lorg/icepdf/core/pobjects/graphics/text/GlyphText;
    :cond_3
    sub-float v5, v21, v24

    add-float v20, v20, v5

    .line 2423
    const/high16 v5, 0x40000000    # 2.0f

    div-float v5, v9, v5

    sub-float v7, v10, v5

    .line 2424
    .restart local v7    # "currentX":F
    add-float v8, v11, v20

    .restart local v8    # "currentY":F
    goto :goto_2

    .line 2436
    .end local v7    # "currentX":F
    .end local v8    # "currentY":F
    .end local v9    # "newAdvanceX":F
    .end local v21    # "newAdvanceY":F
    :cond_4
    add-float v10, v10, v19

    .line 2437
    add-float v11, v11, v20

    .line 2456
    move-object/from16 v0, p4

    iget v0, v0, Lorg/icepdf/core/pobjects/graphics/TextState;->rmode:I

    move/from16 v22, v0

    .line 2458
    .local v22, "rmode":I
    packed-switch v22, :pswitch_data_0

    .line 2495
    :goto_3
    :pswitch_0
    new-instance v5, Landroid/graphics/PointF;

    invoke-direct {v5, v10, v11}, Landroid/graphics/PointF;-><init>(FF)V

    goto/16 :goto_0

    .line 2461
    :pswitch_1
    move-object/from16 v0, p0

    move-object/from16 v1, p5

    move/from16 v2, v22

    invoke-direct {v0, v4, v1, v2}, Lorg/icepdf/core/util/ContentParser;->drawModeFill(Lorg/icepdf/core/pobjects/graphics/TextSprite;Lorg/icepdf/core/pobjects/graphics/Shapes;I)V

    goto :goto_3

    .line 2465
    :pswitch_2
    move-object/from16 v0, p0

    move-object/from16 v1, p4

    move-object/from16 v2, p5

    move/from16 v3, v22

    invoke-direct {v0, v4, v1, v2, v3}, Lorg/icepdf/core/util/ContentParser;->drawModeStroke(Lorg/icepdf/core/pobjects/graphics/TextSprite;Lorg/icepdf/core/pobjects/graphics/TextState;Lorg/icepdf/core/pobjects/graphics/Shapes;I)V

    goto :goto_3

    .line 2469
    :pswitch_3
    move-object/from16 v0, p0

    move-object/from16 v1, p4

    move-object/from16 v2, p5

    move/from16 v3, v22

    invoke-direct {v0, v4, v1, v2, v3}, Lorg/icepdf/core/util/ContentParser;->drawModeFillStroke(Lorg/icepdf/core/pobjects/graphics/TextSprite;Lorg/icepdf/core/pobjects/graphics/TextState;Lorg/icepdf/core/pobjects/graphics/Shapes;I)V

    goto :goto_3

    .line 2477
    :pswitch_4
    move-object/from16 v0, p0

    move-object/from16 v1, p5

    move/from16 v2, v22

    invoke-direct {v0, v4, v1, v2}, Lorg/icepdf/core/util/ContentParser;->drawModeFill(Lorg/icepdf/core/pobjects/graphics/TextSprite;Lorg/icepdf/core/pobjects/graphics/Shapes;I)V

    .line 2478
    move-object/from16 v0, p6

    invoke-virtual {v0, v4}, Lorg/icepdf/core/pobjects/graphics/GlyphOutlineClip;->addTextSprite(Lorg/icepdf/core/pobjects/graphics/TextSprite;)V

    goto :goto_3

    .line 2482
    :pswitch_5
    move-object/from16 v0, p0

    move-object/from16 v1, p4

    move-object/from16 v2, p5

    move/from16 v3, v22

    invoke-direct {v0, v4, v1, v2, v3}, Lorg/icepdf/core/util/ContentParser;->drawModeStroke(Lorg/icepdf/core/pobjects/graphics/TextSprite;Lorg/icepdf/core/pobjects/graphics/TextState;Lorg/icepdf/core/pobjects/graphics/Shapes;I)V

    .line 2483
    move-object/from16 v0, p6

    invoke-virtual {v0, v4}, Lorg/icepdf/core/pobjects/graphics/GlyphOutlineClip;->addTextSprite(Lorg/icepdf/core/pobjects/graphics/TextSprite;)V

    goto :goto_3

    .line 2487
    :pswitch_6
    move-object/from16 v0, p0

    move-object/from16 v1, p4

    move-object/from16 v2, p5

    move/from16 v3, v22

    invoke-direct {v0, v4, v1, v2, v3}, Lorg/icepdf/core/util/ContentParser;->drawModeFillStroke(Lorg/icepdf/core/pobjects/graphics/TextSprite;Lorg/icepdf/core/pobjects/graphics/TextState;Lorg/icepdf/core/pobjects/graphics/Shapes;I)V

    .line 2488
    move-object/from16 v0, p6

    invoke-virtual {v0, v4}, Lorg/icepdf/core/pobjects/graphics/GlyphOutlineClip;->addTextSprite(Lorg/icepdf/core/pobjects/graphics/TextSprite;)V

    goto :goto_3

    .line 2492
    :pswitch_7
    move-object/from16 v0, p6

    invoke-virtual {v0, v4}, Lorg/icepdf/core/pobjects/graphics/GlyphOutlineClip;->addTextSprite(Lorg/icepdf/core/pobjects/graphics/TextSprite;)V

    goto :goto_3

    .line 2458
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method static setStroke(Lorg/icepdf/core/pobjects/graphics/Shapes;Lorg/icepdf/core/pobjects/graphics/GraphicsState;)V
    .locals 7
    .param p0, "shapes"    # Lorg/icepdf/core/pobjects/graphics/Shapes;
    .param p1, "graphicState"    # Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    .prologue
    .line 2788
    invoke-virtual {p1}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->getLineWidth()F

    move-result v0

    invoke-virtual {p1}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->getLineCap()I

    move-result v1

    invoke-virtual {p1}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->getLineJoin()I

    move-result v2

    invoke-virtual {p1}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->getMiterLimit()F

    move-result v3

    invoke-virtual {p1}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->getDashArray()[F

    move-result-object v4

    invoke-virtual {p1}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->getDashPhase()F

    move-result v5

    invoke-static/range {v0 .. v5}, Lcom/samsung/thumbnail/util/Utils;->setPaintValues(FIIF[FF)Landroid/graphics/Paint;

    move-result-object v6

    .line 2795
    .local v6, "temp":Landroid/graphics/Paint;
    invoke-virtual {p0, v6}, Lorg/icepdf/core/pobjects/graphics/Shapes;->add(Ljava/lang/Object;)V

    .line 2796
    return-void
.end method


# virtual methods
.method public getGraphicsState()Lorg/icepdf/core/pobjects/graphics/GraphicsState;
    .locals 1

    .prologue
    .line 133
    iget-object v0, p0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    return-object v0
.end method

.method public parse(Ljava/io/InputStream;)Lorg/icepdf/core/pobjects/graphics/Shapes;
    .locals 37
    .param p1, "source"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 155
    new-instance v28, Lorg/icepdf/core/pobjects/graphics/Shapes;

    invoke-direct/range {v28 .. v28}, Lorg/icepdf/core/pobjects/graphics/Shapes;-><init>()V

    .line 157
    .local v28, "shapes":Lorg/icepdf/core/pobjects/graphics/Shapes;
    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    if-nez v11, :cond_3

    .line 158
    new-instance v11, Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    move-object/from16 v0, v28

    invoke-direct {v11, v0}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;-><init>(Lorg/icepdf/core/pobjects/graphics/Shapes;)V

    move-object/from16 v0, p0

    iput-object v11, v0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    .line 174
    :goto_0
    sget-object v11, Lorg/icepdf/core/util/ContentParser;->logger:Ljava/util/logging/Logger;

    sget-object v12, Ljava/util/logging/Level;->FINER:Ljava/util/logging/Level;

    invoke-virtual {v11, v12}, Ljava/util/logging/Logger;->isLoggable(Ljava/util/logging/Level;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 176
    move-object/from16 v0, p1

    instance-of v11, v0, Lorg/icepdf/core/io/SeekableInput;

    if-eqz v11, :cond_4

    move-object/from16 v11, p1

    .line 177
    check-cast v11, Lorg/icepdf/core/io/SeekableInput;

    const/4 v12, 0x0

    invoke-static {v11, v12}, Lorg/icepdf/core/util/Utils;->getContentFromSeekableInput(Lorg/icepdf/core/io/SeekableInput;Z)Ljava/lang/String;

    move-result-object v18

    .line 183
    .local v18, "content":Ljava/lang/String;
    :goto_1
    sget-object v11, Lorg/icepdf/core/util/ContentParser;->logger:Ljava/util/logging/Logger;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v13, "Content = "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move-object/from16 v0, v18

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/util/logging/Logger;->finer(Ljava/lang/String;)V

    .line 190
    .end local v18    # "content":Ljava/lang/String;
    :cond_0
    new-instance v25, Lorg/icepdf/core/util/Parser;

    move-object/from16 v0, v25

    move-object/from16 v1, p1

    invoke-direct {v0, v1}, Lorg/icepdf/core/util/Parser;-><init>(Ljava/io/InputStream;)V

    .line 193
    .local v25, "parser":Lorg/icepdf/core/util/Parser;
    new-instance v29, Ljava/util/Stack;

    invoke-direct/range {v29 .. v29}, Ljava/util/Stack;-><init>()V

    .line 196
    .local v29, "stack":Ljava/util/Stack;, "Ljava/util/Stack<Ljava/lang/Object;>;"
    const/16 v36, 0x0

    .line 203
    .local v36, "yBTstart":F
    const/4 v4, 0x0

    .line 205
    .local v4, "geometricPath":Landroid/graphics/Path;
    const/16 v24, 0x0

    .line 209
    .local v24, "objCount":I
    :cond_1
    :goto_2
    add-int/lit8 v24, v24, 0x1

    .line 210
    const v11, 0x7a120

    move/from16 v0, v24

    if-le v0, v11, :cond_5

    .line 211
    const-string/jumbo v11, "ContentParser"

    const-string/jumbo v12, "Object Count greater than 1000000... So breaking..."

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 886
    :cond_2
    :goto_3
    invoke-virtual/range {v29 .. v29}, Ljava/util/Stack;->isEmpty()Z

    move-result v11

    if-nez v11, :cond_4b

    .line 887
    invoke-virtual/range {v29 .. v29}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v31

    .line 888
    .local v31, "tmp":Ljava/lang/String;
    sget-object v11, Lorg/icepdf/core/util/ContentParser;->logger:Ljava/util/logging/Logger;

    sget-object v12, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    invoke-virtual {v11, v12}, Ljava/util/logging/Logger;->isLoggable(Ljava/util/logging/Level;)Z

    move-result v11

    if-eqz v11, :cond_2

    .line 889
    sget-object v11, Lorg/icepdf/core/util/ContentParser;->logger:Ljava/util/logging/Logger;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v13, "STACK="

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move-object/from16 v0, v31

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/util/logging/Logger;->fine(Ljava/lang/String;)V

    goto :goto_3

    .line 165
    .end local v4    # "geometricPath":Landroid/graphics/Path;
    .end local v24    # "objCount":I
    .end local v25    # "parser":Lorg/icepdf/core/util/Parser;
    .end local v29    # "stack":Ljava/util/Stack;, "Ljava/util/Stack<Ljava/lang/Object;>;"
    .end local v31    # "tmp":Ljava/lang/String;
    .end local v36    # "yBTstart":F
    :cond_3
    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    new-instance v12, Landroid/graphics/Matrix;

    invoke-direct {v12}, Landroid/graphics/Matrix;-><init>()V

    invoke-virtual {v11, v12}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->setCTM(Landroid/graphics/Matrix;)V

    .line 167
    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->setClip(Landroid/graphics/Path;)V

    .line 169
    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    move-object/from16 v0, v28

    invoke-static {v0, v11}, Lorg/icepdf/core/util/ContentParser;->setStroke(Lorg/icepdf/core/pobjects/graphics/Shapes;Lorg/icepdf/core/pobjects/graphics/GraphicsState;)V

    .line 171
    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    move-object/from16 v0, v28

    invoke-virtual {v11, v0}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->setShapes(Lorg/icepdf/core/pobjects/graphics/Shapes;)V

    goto/16 :goto_0

    .line 179
    :cond_4
    const/4 v11, 0x1

    new-array v0, v11, [Ljava/io/InputStream;

    move-object/from16 v22, v0

    const/4 v11, 0x0

    aput-object p1, v22, v11

    .line 180
    .local v22, "inArray":[Ljava/io/InputStream;
    const/4 v11, 0x0

    move-object/from16 v0, v22

    invoke-static {v0, v11}, Lorg/icepdf/core/util/Utils;->getContentAndReplaceInputStream([Ljava/io/InputStream;Z)Ljava/lang/String;

    move-result-object v18

    .line 181
    .restart local v18    # "content":Ljava/lang/String;
    const/4 v11, 0x0

    aget-object p1, v22, v11

    goto/16 :goto_1

    .line 216
    .end local v18    # "content":Ljava/lang/String;
    .end local v22    # "inArray":[Ljava/io/InputStream;
    .restart local v4    # "geometricPath":Landroid/graphics/Path;
    .restart local v24    # "objCount":I
    .restart local v25    # "parser":Lorg/icepdf/core/util/Parser;
    .restart local v29    # "stack":Ljava/util/Stack;, "Ljava/util/Stack<Ljava/lang/Object;>;"
    .restart local v36    # "yBTstart":F
    :cond_5
    :try_start_0
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    move-result v11

    if-eqz v11, :cond_6

    .line 217
    new-instance v11, Ljava/lang/InterruptedException;

    const-string/jumbo v12, "ContentParser thread interrupted"

    invoke-direct {v11, v12}, Ljava/lang/InterruptedException;-><init>(Ljava/lang/String;)V

    throw v11
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 865
    :catch_0
    move-exception v19

    .line 867
    .local v19, "e":Ljava/io/IOException;
    sget-object v11, Lorg/icepdf/core/util/ContentParser;->logger:Ljava/util/logging/Logger;

    const-string/jumbo v12, "End of Content Stream"

    invoke-virtual {v11, v12}, Ljava/util/logging/Logger;->finer(Ljava/lang/String;)V

    goto/16 :goto_3

    .line 220
    .end local v19    # "e":Ljava/io/IOException;
    :cond_6
    :try_start_1
    invoke-virtual/range {v25 .. v25}, Lorg/icepdf/core/util/Parser;->getStreamObject()Ljava/lang/Object;

    move-result-object v32

    .line 224
    .local v32, "tok":Ljava/lang/Object;
    move-object/from16 v0, v32

    instance-of v11, v0, Ljava/lang/String;

    if-nez v11, :cond_7

    .line 225
    move-object/from16 v0, v29

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_2

    .line 869
    .end local v32    # "tok":Ljava/lang/Object;
    :catch_1
    move-exception v19

    .line 872
    .local v19, "e":Ljava/lang/Exception;
    const-string/jumbo v11, "ContentParser"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v13, "Exception : "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    .line 230
    .end local v19    # "e":Ljava/lang/Exception;
    .restart local v32    # "tok":Ljava/lang/Object;
    :cond_7
    :try_start_2
    const-string/jumbo v11, "l"

    move-object/from16 v0, v32

    invoke-virtual {v0, v11}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_8

    .line 232
    invoke-virtual/range {v29 .. v29}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/Number;

    invoke-virtual {v11}, Ljava/lang/Number;->floatValue()F

    move-result v35

    .line 233
    .local v35, "y":F
    invoke-virtual/range {v29 .. v29}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/Number;

    invoke-virtual {v11}, Ljava/lang/Number;->floatValue()F

    move-result v34

    .line 234
    .local v34, "x":F
    if-eqz v4, :cond_1

    .line 235
    move/from16 v0, v34

    move/from16 v1, v35

    invoke-virtual {v4, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    goto/16 :goto_2

    .line 244
    .end local v34    # "x":F
    .end local v35    # "y":F
    :cond_8
    const-string/jumbo v11, "m"

    move-object/from16 v0, v32

    invoke-virtual {v0, v11}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_a

    .line 246
    if-nez v4, :cond_9

    .line 247
    new-instance v20, Landroid/graphics/Path;

    invoke-direct/range {v20 .. v20}, Landroid/graphics/Path;-><init>()V

    .end local v4    # "geometricPath":Landroid/graphics/Path;
    .local v20, "geometricPath":Landroid/graphics/Path;
    move-object/from16 v4, v20

    .line 249
    .end local v20    # "geometricPath":Landroid/graphics/Path;
    .restart local v4    # "geometricPath":Landroid/graphics/Path;
    :cond_9
    invoke-virtual/range {v29 .. v29}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/Number;

    invoke-virtual {v11}, Ljava/lang/Number;->floatValue()F

    move-result v35

    .line 250
    .restart local v35    # "y":F
    invoke-virtual/range {v29 .. v29}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/Number;

    invoke-virtual {v11}, Ljava/lang/Number;->floatValue()F

    move-result v34

    .line 251
    .restart local v34    # "x":F
    move/from16 v0, v34

    move/from16 v1, v35

    invoke-virtual {v4, v0, v1}, Landroid/graphics/Path;->moveTo(FF)V

    goto/16 :goto_2

    .line 258
    .end local v34    # "x":F
    .end local v35    # "y":F
    :cond_a
    const-string/jumbo v11, "c"

    move-object/from16 v0, v32

    invoke-virtual {v0, v11}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_b

    .line 260
    invoke-virtual/range {v29 .. v29}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/Number;

    invoke-virtual {v11}, Ljava/lang/Number;->floatValue()F

    move-result v10

    .line 261
    .local v10, "y3":F
    invoke-virtual/range {v29 .. v29}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/Number;

    invoke-virtual {v11}, Ljava/lang/Number;->floatValue()F

    move-result v9

    .line 262
    .local v9, "x3":F
    invoke-virtual/range {v29 .. v29}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/Number;

    invoke-virtual {v11}, Ljava/lang/Number;->floatValue()F

    move-result v8

    .line 263
    .local v8, "y2":F
    invoke-virtual/range {v29 .. v29}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/Number;

    invoke-virtual {v11}, Ljava/lang/Number;->floatValue()F

    move-result v7

    .line 264
    .local v7, "x2":F
    invoke-virtual/range {v29 .. v29}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/Number;

    invoke-virtual {v11}, Ljava/lang/Number;->floatValue()F

    move-result v6

    .line 265
    .local v6, "y1":F
    invoke-virtual/range {v29 .. v29}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/Number;

    invoke-virtual {v11}, Ljava/lang/Number;->floatValue()F

    move-result v5

    .line 267
    .local v5, "x1":F
    if-eqz v4, :cond_1

    .line 268
    invoke-virtual/range {v4 .. v10}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    goto/16 :goto_2

    .line 273
    .end local v5    # "x1":F
    .end local v6    # "y1":F
    .end local v7    # "x2":F
    .end local v8    # "y2":F
    .end local v9    # "x3":F
    .end local v10    # "y3":F
    :cond_b
    const-string/jumbo v11, "S"

    move-object/from16 v0, v32

    invoke-virtual {v0, v11}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_c

    .line 275
    if-eqz v4, :cond_1

    .line 276
    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    move-object/from16 v0, v28

    invoke-static {v11, v0, v4}, Lorg/icepdf/core/util/ContentParser;->commonStroke(Lorg/icepdf/core/pobjects/graphics/GraphicsState;Lorg/icepdf/core/pobjects/graphics/Shapes;Landroid/graphics/Path;)V

    .line 277
    const/4 v4, 0x0

    goto/16 :goto_2

    .line 282
    :cond_c
    const-string/jumbo v11, "Tf"

    move-object/from16 v0, v32

    invoke-virtual {v0, v11}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_d

    .line 283
    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/icepdf/core/util/ContentParser;->resources:Lorg/icepdf/core/pobjects/Resources;

    move-object/from16 v0, v29

    invoke-static {v11, v0, v12}, Lorg/icepdf/core/util/ContentParser;->consume_Tf(Lorg/icepdf/core/pobjects/graphics/GraphicsState;Ljava/util/Stack;Lorg/icepdf/core/pobjects/Resources;)V

    goto/16 :goto_2

    .line 290
    :cond_d
    const-string/jumbo v11, "BT"

    move-object/from16 v0, v32

    invoke-virtual {v0, v11}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_e

    .line 293
    move/from16 v0, v36

    float-to-double v12, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v25

    move-object/from16 v2, v28

    invoke-virtual {v0, v1, v2, v12, v13}, Lorg/icepdf/core/util/ContentParser;->parseText(Lorg/icepdf/core/util/Parser;Lorg/icepdf/core/pobjects/graphics/Shapes;D)F

    move-result v36

    goto/16 :goto_2

    .line 300
    :cond_e
    const-string/jumbo v11, "F"

    move-object/from16 v0, v32

    invoke-virtual {v0, v11}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_f

    const-string/jumbo v11, "f"

    move-object/from16 v0, v32

    invoke-virtual {v0, v11}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_11

    .line 304
    :cond_f
    if-eqz v4, :cond_10

    .line 305
    move-object/from16 v0, p0

    move-object/from16 v1, v28

    invoke-direct {v0, v1, v4}, Lorg/icepdf/core/util/ContentParser;->commonFill(Lorg/icepdf/core/pobjects/graphics/Shapes;Landroid/graphics/Path;)V

    .line 307
    :cond_10
    const/4 v4, 0x0

    goto/16 :goto_2

    .line 312
    :cond_11
    const-string/jumbo v11, "q"

    move-object/from16 v0, v32

    invoke-virtual {v0, v11}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_12

    .line 313
    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    invoke-static {v11}, Lorg/icepdf/core/util/ContentParser;->consume_q(Lorg/icepdf/core/pobjects/graphics/GraphicsState;)Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    move-result-object v11

    move-object/from16 v0, p0

    iput-object v11, v0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    goto/16 :goto_2

    .line 317
    :cond_12
    const-string/jumbo v11, "Q"

    move-object/from16 v0, v32

    invoke-virtual {v0, v11}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_13

    .line 318
    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    move-object/from16 v0, v28

    invoke-static {v11, v0}, Lorg/icepdf/core/util/ContentParser;->consume_Q(Lorg/icepdf/core/pobjects/graphics/GraphicsState;Lorg/icepdf/core/pobjects/graphics/Shapes;)Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    move-result-object v11

    move-object/from16 v0, p0

    iput-object v11, v0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    goto/16 :goto_2

    .line 329
    :cond_13
    const-string/jumbo v11, "re"

    move-object/from16 v0, v32

    invoke-virtual {v0, v11}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_15

    .line 331
    if-nez v4, :cond_14

    .line 332
    new-instance v20, Landroid/graphics/Path;

    invoke-direct/range {v20 .. v20}, Landroid/graphics/Path;-><init>()V

    .end local v4    # "geometricPath":Landroid/graphics/Path;
    .restart local v20    # "geometricPath":Landroid/graphics/Path;
    move-object/from16 v4, v20

    .line 334
    .end local v20    # "geometricPath":Landroid/graphics/Path;
    .restart local v4    # "geometricPath":Landroid/graphics/Path;
    :cond_14
    invoke-virtual/range {v29 .. v29}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/Number;

    invoke-virtual {v11}, Ljava/lang/Number;->floatValue()F

    move-result v21

    .line 335
    .local v21, "h":F
    invoke-virtual/range {v29 .. v29}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/Number;

    invoke-virtual {v11}, Ljava/lang/Number;->floatValue()F

    move-result v33

    .line 336
    .local v33, "w":F
    invoke-virtual/range {v29 .. v29}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/Number;

    invoke-virtual {v11}, Ljava/lang/Number;->floatValue()F

    move-result v35

    .line 337
    .restart local v35    # "y":F
    invoke-virtual/range {v29 .. v29}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/Number;

    invoke-virtual {v11}, Ljava/lang/Number;->floatValue()F

    move-result v34

    .line 338
    .restart local v34    # "x":F
    move/from16 v0, v34

    move/from16 v1, v35

    invoke-virtual {v4, v0, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 339
    add-float v11, v34, v33

    move/from16 v0, v35

    invoke-virtual {v4, v11, v0}, Landroid/graphics/Path;->lineTo(FF)V

    .line 340
    add-float v11, v34, v33

    add-float v12, v35, v21

    invoke-virtual {v4, v11, v12}, Landroid/graphics/Path;->lineTo(FF)V

    .line 341
    add-float v11, v35, v21

    move/from16 v0, v34

    invoke-virtual {v4, v0, v11}, Landroid/graphics/Path;->lineTo(FF)V

    .line 342
    move/from16 v0, v34

    move/from16 v1, v35

    invoke-virtual {v4, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    goto/16 :goto_2

    .line 347
    .end local v21    # "h":F
    .end local v33    # "w":F
    .end local v34    # "x":F
    .end local v35    # "y":F
    :cond_15
    const-string/jumbo v11, "cm"

    move-object/from16 v0, v32

    invoke-virtual {v0, v11}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_16

    .line 348
    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    move-object/from16 v0, p0

    iget-boolean v12, v0, Lorg/icepdf/core/util/ContentParser;->inTextBlock:Z

    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/icepdf/core/util/ContentParser;->textBlockBase:Landroid/graphics/Matrix;

    move-object/from16 v0, v29

    invoke-static {v11, v0, v12, v13}, Lorg/icepdf/core/util/ContentParser;->consume_cm(Lorg/icepdf/core/pobjects/graphics/GraphicsState;Ljava/util/Stack;ZLandroid/graphics/Matrix;)V

    goto/16 :goto_2

    .line 358
    :cond_16
    const-string/jumbo v11, "h"

    move-object/from16 v0, v32

    invoke-virtual {v0, v11}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_17

    .line 360
    if-eqz v4, :cond_1

    .line 361
    invoke-virtual {v4}, Landroid/graphics/Path;->close()V

    goto/16 :goto_2

    .line 371
    :cond_17
    const-string/jumbo v11, "BDC"

    move-object/from16 v0, v32

    invoke-virtual {v0, v11}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_18

    .line 373
    invoke-virtual/range {v29 .. v29}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    .line 374
    invoke-virtual/range {v29 .. v29}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    goto/16 :goto_2

    .line 407
    :cond_18
    const-string/jumbo v11, "Do"

    move-object/from16 v0, v32

    invoke-virtual {v0, v11}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_19

    .line 409
    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/icepdf/core/util/ContentParser;->resources:Lorg/icepdf/core/pobjects/Resources;

    const/4 v13, 0x1

    move-object/from16 v0, v29

    move-object/from16 v1, v28

    invoke-static {v11, v0, v1, v12, v13}, Lorg/icepdf/core/util/ContentParser;->consume_Do(Lorg/icepdf/core/pobjects/graphics/GraphicsState;Ljava/util/Stack;Lorg/icepdf/core/pobjects/graphics/Shapes;Lorg/icepdf/core/pobjects/Resources;Z)Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    move-result-object v11

    move-object/from16 v0, p0

    iput-object v11, v0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    goto/16 :goto_2

    .line 414
    :cond_19
    const-string/jumbo v11, "f*"

    move-object/from16 v0, v32

    invoke-virtual {v0, v11}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_1b

    .line 416
    if-eqz v4, :cond_1a

    .line 419
    move-object/from16 v0, p0

    move-object/from16 v1, v28

    invoke-direct {v0, v1, v4}, Lorg/icepdf/core/util/ContentParser;->commonFill(Lorg/icepdf/core/pobjects/graphics/Shapes;Landroid/graphics/Path;)V

    .line 421
    :cond_1a
    const/4 v4, 0x0

    goto/16 :goto_2

    .line 428
    :cond_1b
    const-string/jumbo v11, "gs"

    move-object/from16 v0, v32

    invoke-virtual {v0, v11}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_1c

    .line 429
    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/icepdf/core/util/ContentParser;->resources:Lorg/icepdf/core/pobjects/Resources;

    move-object/from16 v0, v29

    invoke-static {v11, v0, v12}, Lorg/icepdf/core/util/ContentParser;->consume_gs(Lorg/icepdf/core/pobjects/graphics/GraphicsState;Ljava/util/Stack;Lorg/icepdf/core/pobjects/Resources;)V

    goto/16 :goto_2

    .line 435
    :cond_1c
    const-string/jumbo v11, "n"

    move-object/from16 v0, v32

    invoke-virtual {v0, v11}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_1d

    .line 439
    const/4 v4, 0x0

    goto/16 :goto_2

    .line 443
    :cond_1d
    const-string/jumbo v11, "w"

    move-object/from16 v0, v32

    invoke-virtual {v0, v11}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_1e

    const-string/jumbo v11, "LW"

    move-object/from16 v0, v32

    invoke-virtual {v0, v11}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_1f

    .line 445
    :cond_1e
    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    move-object/from16 v0, v29

    move-object/from16 v1, v28

    invoke-static {v11, v0, v1}, Lorg/icepdf/core/util/ContentParser;->consume_w(Lorg/icepdf/core/pobjects/graphics/GraphicsState;Ljava/util/Stack;Lorg/icepdf/core/pobjects/graphics/Shapes;)V

    goto/16 :goto_2

    .line 451
    :cond_1f
    const-string/jumbo v11, "W"

    move-object/from16 v0, v32

    invoke-virtual {v0, v11}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_20

    .line 453
    if-eqz v4, :cond_1

    .line 455
    invoke-virtual {v4}, Landroid/graphics/Path;->close()V

    .line 456
    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    invoke-virtual {v11, v4}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->setClip(Landroid/graphics/Path;)V

    goto/16 :goto_2

    .line 461
    :cond_20
    const-string/jumbo v11, "sc"

    move-object/from16 v0, v32

    invoke-virtual {v0, v11}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_21

    const-string/jumbo v11, "scn"

    move-object/from16 v0, v32

    invoke-virtual {v0, v11}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_22

    .line 463
    :cond_21
    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/icepdf/core/util/ContentParser;->library:Lorg/icepdf/core/util/Library;

    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/icepdf/core/util/ContentParser;->resources:Lorg/icepdf/core/pobjects/Resources;

    move-object/from16 v0, v29

    invoke-static {v11, v0, v12, v13}, Lorg/icepdf/core/util/ContentParser;->consume_sc(Lorg/icepdf/core/pobjects/graphics/GraphicsState;Ljava/util/Stack;Lorg/icepdf/core/util/Library;Lorg/icepdf/core/pobjects/Resources;)V

    goto/16 :goto_2

    .line 470
    :cond_22
    const-string/jumbo v11, "b"

    move-object/from16 v0, v32

    invoke-virtual {v0, v11}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_24

    .line 472
    if-eqz v4, :cond_23

    .line 474
    invoke-virtual {v4}, Landroid/graphics/Path;->close()V

    .line 475
    move-object/from16 v0, p0

    move-object/from16 v1, v28

    invoke-direct {v0, v1, v4}, Lorg/icepdf/core/util/ContentParser;->commonFill(Lorg/icepdf/core/pobjects/graphics/Shapes;Landroid/graphics/Path;)V

    .line 476
    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    move-object/from16 v0, v28

    invoke-static {v11, v0, v4}, Lorg/icepdf/core/util/ContentParser;->commonStroke(Lorg/icepdf/core/pobjects/graphics/GraphicsState;Lorg/icepdf/core/pobjects/graphics/Shapes;Landroid/graphics/Path;)V

    .line 478
    :cond_23
    const/4 v4, 0x0

    goto/16 :goto_2

    .line 482
    :cond_24
    const-string/jumbo v11, "k"

    move-object/from16 v0, v32

    invoke-virtual {v0, v11}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_25

    .line 483
    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/icepdf/core/util/ContentParser;->library:Lorg/icepdf/core/util/Library;

    move-object/from16 v0, v29

    invoke-static {v11, v0, v12}, Lorg/icepdf/core/util/ContentParser;->consume_k(Lorg/icepdf/core/pobjects/graphics/GraphicsState;Ljava/util/Stack;Lorg/icepdf/core/util/Library;)V

    goto/16 :goto_2

    .line 487
    :cond_25
    const-string/jumbo v11, "g"

    move-object/from16 v0, v32

    invoke-virtual {v0, v11}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_26

    .line 488
    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/icepdf/core/util/ContentParser;->library:Lorg/icepdf/core/util/Library;

    move-object/from16 v0, v29

    invoke-static {v11, v0, v12}, Lorg/icepdf/core/util/ContentParser;->consume_g(Lorg/icepdf/core/pobjects/graphics/GraphicsState;Ljava/util/Stack;Lorg/icepdf/core/util/Library;)V

    goto/16 :goto_2

    .line 494
    :cond_26
    const-string/jumbo v11, "i"

    move-object/from16 v0, v32

    invoke-virtual {v0, v11}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_27

    .line 495
    invoke-static/range {v29 .. v29}, Lorg/icepdf/core/util/ContentParser;->consume_i(Ljava/util/Stack;)V

    goto/16 :goto_2

    .line 499
    :cond_27
    const-string/jumbo v11, "M"

    move-object/from16 v0, v32

    invoke-virtual {v0, v11}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_28

    .line 500
    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    move-object/from16 v0, v29

    move-object/from16 v1, v28

    invoke-static {v11, v0, v1}, Lorg/icepdf/core/util/ContentParser;->consume_M(Lorg/icepdf/core/pobjects/graphics/GraphicsState;Ljava/util/Stack;Lorg/icepdf/core/pobjects/graphics/Shapes;)V

    goto/16 :goto_2

    .line 505
    :cond_28
    const-string/jumbo v11, "J"

    move-object/from16 v0, v32

    invoke-virtual {v0, v11}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_29

    .line 506
    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    move-object/from16 v0, v29

    move-object/from16 v1, v28

    invoke-static {v11, v0, v1}, Lorg/icepdf/core/util/ContentParser;->consume_J(Lorg/icepdf/core/pobjects/graphics/GraphicsState;Ljava/util/Stack;Lorg/icepdf/core/pobjects/graphics/Shapes;)V

    goto/16 :goto_2

    .line 510
    :cond_29
    const-string/jumbo v11, "rg"

    move-object/from16 v0, v32

    invoke-virtual {v0, v11}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_2a

    .line 511
    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/icepdf/core/util/ContentParser;->library:Lorg/icepdf/core/util/Library;

    move-object/from16 v0, v29

    invoke-static {v11, v0, v12}, Lorg/icepdf/core/util/ContentParser;->consume_rg(Lorg/icepdf/core/pobjects/graphics/GraphicsState;Ljava/util/Stack;Lorg/icepdf/core/util/Library;)V

    goto/16 :goto_2

    .line 518
    :cond_2a
    const-string/jumbo v11, "d"

    move-object/from16 v0, v32

    invoke-virtual {v0, v11}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_2b

    .line 519
    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    move-object/from16 v0, v29

    move-object/from16 v1, v28

    invoke-static {v11, v0, v1}, Lorg/icepdf/core/util/ContentParser;->consume_d(Lorg/icepdf/core/pobjects/graphics/GraphicsState;Ljava/util/Stack;Lorg/icepdf/core/pobjects/graphics/Shapes;)V

    goto/16 :goto_2

    .line 526
    :cond_2b
    const-string/jumbo v11, "v"

    move-object/from16 v0, v32

    invoke-virtual {v0, v11}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_2c

    .line 528
    invoke-virtual/range {v29 .. v29}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/Number;

    invoke-virtual {v11}, Ljava/lang/Number;->floatValue()F

    move-result v10

    .line 529
    .restart local v10    # "y3":F
    invoke-virtual/range {v29 .. v29}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/Number;

    invoke-virtual {v11}, Ljava/lang/Number;->floatValue()F

    move-result v9

    .line 530
    .restart local v9    # "x3":F
    invoke-virtual/range {v29 .. v29}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/Number;

    invoke-virtual {v11}, Ljava/lang/Number;->floatValue()F

    move-result v8

    .line 531
    .restart local v8    # "y2":F
    invoke-virtual/range {v29 .. v29}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/Number;

    invoke-virtual {v11}, Ljava/lang/Number;->floatValue()F

    move-result v7

    .line 534
    .restart local v7    # "x2":F
    if-eqz v4, :cond_1

    move-object v11, v4

    move v12, v7

    move v13, v8

    move v14, v7

    move v15, v8

    move/from16 v16, v9

    move/from16 v17, v10

    .line 535
    invoke-virtual/range {v11 .. v17}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    goto/16 :goto_2

    .line 548
    .end local v7    # "x2":F
    .end local v8    # "y2":F
    .end local v9    # "x3":F
    .end local v10    # "y3":F
    :cond_2c
    const-string/jumbo v11, "j"

    move-object/from16 v0, v32

    invoke-virtual {v0, v11}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_2d

    .line 549
    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    move-object/from16 v0, v29

    move-object/from16 v1, v28

    invoke-static {v11, v0, v1}, Lorg/icepdf/core/util/ContentParser;->consume_j(Lorg/icepdf/core/pobjects/graphics/GraphicsState;Ljava/util/Stack;Lorg/icepdf/core/pobjects/graphics/Shapes;)V

    goto/16 :goto_2

    .line 556
    :cond_2d
    const-string/jumbo v11, "y"

    move-object/from16 v0, v32

    invoke-virtual {v0, v11}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_2e

    .line 558
    invoke-virtual/range {v29 .. v29}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/Number;

    invoke-virtual {v11}, Ljava/lang/Number;->floatValue()F

    move-result v10

    .line 559
    .restart local v10    # "y3":F
    invoke-virtual/range {v29 .. v29}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/Number;

    invoke-virtual {v11}, Ljava/lang/Number;->floatValue()F

    move-result v9

    .line 560
    .restart local v9    # "x3":F
    invoke-virtual/range {v29 .. v29}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/Number;

    invoke-virtual {v11}, Ljava/lang/Number;->floatValue()F

    move-result v6

    .line 561
    .restart local v6    # "y1":F
    invoke-virtual/range {v29 .. v29}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/Number;

    invoke-virtual {v11}, Ljava/lang/Number;->floatValue()F

    move-result v5

    .line 564
    .restart local v5    # "x1":F
    if-eqz v4, :cond_1

    move-object v11, v4

    move v12, v5

    move v13, v6

    move v14, v9

    move v15, v10

    move/from16 v16, v9

    move/from16 v17, v10

    .line 565
    invoke-virtual/range {v11 .. v17}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    goto/16 :goto_2

    .line 571
    .end local v5    # "x1":F
    .end local v6    # "y1":F
    .end local v9    # "x3":F
    .end local v10    # "y3":F
    :cond_2e
    const-string/jumbo v11, "cs"

    move-object/from16 v0, v32

    invoke-virtual {v0, v11}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_2f

    .line 572
    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/icepdf/core/util/ContentParser;->resources:Lorg/icepdf/core/pobjects/Resources;

    move-object/from16 v0, v29

    invoke-static {v11, v0, v12}, Lorg/icepdf/core/util/ContentParser;->consume_cs(Lorg/icepdf/core/pobjects/graphics/GraphicsState;Ljava/util/Stack;Lorg/icepdf/core/pobjects/Resources;)V

    goto/16 :goto_2

    .line 576
    :cond_2f
    const-string/jumbo v11, "ri"

    move-object/from16 v0, v32

    invoke-virtual {v0, v11}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_30

    .line 578
    invoke-virtual/range {v29 .. v29}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    goto/16 :goto_2

    .line 589
    :cond_30
    const-string/jumbo v11, "SC"

    move-object/from16 v0, v32

    invoke-virtual {v0, v11}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_31

    const-string/jumbo v11, "SCN"

    move-object/from16 v0, v32

    invoke-virtual {v0, v11}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_32

    .line 591
    :cond_31
    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/icepdf/core/util/ContentParser;->library:Lorg/icepdf/core/util/Library;

    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/icepdf/core/util/ContentParser;->resources:Lorg/icepdf/core/pobjects/Resources;

    move-object/from16 v0, v29

    invoke-static {v11, v0, v12, v13}, Lorg/icepdf/core/util/ContentParser;->consume_SC(Lorg/icepdf/core/pobjects/graphics/GraphicsState;Ljava/util/Stack;Lorg/icepdf/core/util/Library;Lorg/icepdf/core/pobjects/Resources;)V

    goto/16 :goto_2

    .line 601
    :cond_32
    const-string/jumbo v11, "B"

    move-object/from16 v0, v32

    invoke-virtual {v0, v11}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_34

    .line 603
    if-eqz v4, :cond_33

    .line 605
    move-object/from16 v0, p0

    move-object/from16 v1, v28

    invoke-direct {v0, v1, v4}, Lorg/icepdf/core/util/ContentParser;->commonFill(Lorg/icepdf/core/pobjects/graphics/Shapes;Landroid/graphics/Path;)V

    .line 606
    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    move-object/from16 v0, v28

    invoke-static {v11, v0, v4}, Lorg/icepdf/core/util/ContentParser;->commonStroke(Lorg/icepdf/core/pobjects/graphics/GraphicsState;Lorg/icepdf/core/pobjects/graphics/Shapes;Landroid/graphics/Path;)V

    .line 608
    :cond_33
    const/4 v4, 0x0

    goto/16 :goto_2

    .line 617
    :cond_34
    const-string/jumbo v11, "K"

    move-object/from16 v0, v32

    invoke-virtual {v0, v11}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_35

    .line 618
    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/icepdf/core/util/ContentParser;->library:Lorg/icepdf/core/util/Library;

    move-object/from16 v0, v29

    invoke-static {v11, v0, v12}, Lorg/icepdf/core/util/ContentParser;->consume_K(Lorg/icepdf/core/pobjects/graphics/GraphicsState;Ljava/util/Stack;Lorg/icepdf/core/util/Library;)V

    goto/16 :goto_2

    .line 624
    :cond_35
    const-string/jumbo v11, "d0"

    move-object/from16 v0, v32

    invoke-virtual {v0, v11}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_36

    .line 627
    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    invoke-virtual {v11}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->save()Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    move-result-object v11

    move-object/from16 v0, p0

    iput-object v11, v0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    .line 629
    invoke-virtual/range {v29 .. v29}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/Number;

    invoke-virtual {v11}, Ljava/lang/Number;->floatValue()F

    move-result v35

    .line 630
    .restart local v35    # "y":F
    invoke-virtual/range {v29 .. v29}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/Number;

    invoke-virtual {v11}, Ljava/lang/Number;->floatValue()F

    move-result v34

    .line 631
    .restart local v34    # "x":F
    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    invoke-virtual {v11}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->getTextState()Lorg/icepdf/core/pobjects/graphics/TextState;

    move-result-object v30

    .line 632
    .local v30, "textState":Lorg/icepdf/core/pobjects/graphics/TextState;
    new-instance v11, Landroid/graphics/PointF;

    move/from16 v0, v34

    move/from16 v1, v35

    invoke-direct {v11, v0, v1}, Landroid/graphics/PointF;-><init>(FF)V

    move-object/from16 v0, v30

    invoke-virtual {v0, v11}, Lorg/icepdf/core/pobjects/graphics/TextState;->setType3HorizontalDisplacement(Landroid/graphics/PointF;)V

    goto/16 :goto_2

    .line 637
    .end local v30    # "textState":Lorg/icepdf/core/pobjects/graphics/TextState;
    .end local v34    # "x":F
    .end local v35    # "y":F
    :cond_36
    const-string/jumbo v11, "s"

    move-object/from16 v0, v32

    invoke-virtual {v0, v11}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_37

    .line 639
    if-eqz v4, :cond_1

    .line 640
    invoke-virtual {v4}, Landroid/graphics/Path;->close()V

    .line 641
    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    move-object/from16 v0, v28

    invoke-static {v11, v0, v4}, Lorg/icepdf/core/util/ContentParser;->commonStroke(Lorg/icepdf/core/pobjects/graphics/GraphicsState;Lorg/icepdf/core/pobjects/graphics/Shapes;Landroid/graphics/Path;)V

    .line 642
    const/4 v4, 0x0

    goto/16 :goto_2

    .line 650
    :cond_37
    const-string/jumbo v11, "G"

    move-object/from16 v0, v32

    invoke-virtual {v0, v11}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_38

    .line 651
    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/icepdf/core/util/ContentParser;->library:Lorg/icepdf/core/util/Library;

    move-object/from16 v0, v29

    invoke-static {v11, v0, v12}, Lorg/icepdf/core/util/ContentParser;->consume_G(Lorg/icepdf/core/pobjects/graphics/GraphicsState;Ljava/util/Stack;Lorg/icepdf/core/util/Library;)V

    goto/16 :goto_2

    .line 658
    :cond_38
    const-string/jumbo v11, "b*"

    move-object/from16 v0, v32

    invoke-virtual {v0, v11}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3a

    .line 660
    if-eqz v4, :cond_39

    .line 662
    invoke-virtual {v4}, Landroid/graphics/Path;->close()V

    .line 663
    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    move-object/from16 v0, v28

    invoke-static {v11, v0, v4}, Lorg/icepdf/core/util/ContentParser;->commonStroke(Lorg/icepdf/core/pobjects/graphics/GraphicsState;Lorg/icepdf/core/pobjects/graphics/Shapes;Landroid/graphics/Path;)V

    .line 664
    move-object/from16 v0, p0

    move-object/from16 v1, v28

    invoke-direct {v0, v1, v4}, Lorg/icepdf/core/util/ContentParser;->commonFill(Lorg/icepdf/core/pobjects/graphics/Shapes;Landroid/graphics/Path;)V

    .line 666
    :cond_39
    const/4 v4, 0x0

    goto/16 :goto_2

    .line 673
    :cond_3a
    const-string/jumbo v11, "RG"

    move-object/from16 v0, v32

    invoke-virtual {v0, v11}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3b

    .line 674
    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/icepdf/core/util/ContentParser;->library:Lorg/icepdf/core/util/Library;

    move-object/from16 v0, v29

    invoke-static {v11, v0, v12}, Lorg/icepdf/core/util/ContentParser;->consume_RG(Lorg/icepdf/core/pobjects/graphics/GraphicsState;Ljava/util/Stack;Lorg/icepdf/core/util/Library;)V

    goto/16 :goto_2

    .line 703
    :cond_3b
    const-string/jumbo v11, "CS"

    move-object/from16 v0, v32

    invoke-virtual {v0, v11}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3c

    .line 704
    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/icepdf/core/util/ContentParser;->resources:Lorg/icepdf/core/pobjects/Resources;

    move-object/from16 v0, v29

    invoke-static {v11, v0, v12}, Lorg/icepdf/core/util/ContentParser;->consume_CS(Lorg/icepdf/core/pobjects/graphics/GraphicsState;Ljava/util/Stack;Lorg/icepdf/core/pobjects/Resources;)V

    goto/16 :goto_2

    .line 705
    :cond_3c
    const-string/jumbo v11, "d1"

    move-object/from16 v0, v32

    invoke-virtual {v0, v11}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3d

    .line 708
    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    invoke-virtual {v11}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->save()Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    move-result-object v11

    move-object/from16 v0, p0

    iput-object v11, v0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    .line 710
    invoke-virtual/range {v29 .. v29}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/Number;

    invoke-virtual {v11}, Ljava/lang/Number;->floatValue()F

    move-result v7

    .line 711
    .restart local v7    # "x2":F
    invoke-virtual/range {v29 .. v29}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/Number;

    invoke-virtual {v11}, Ljava/lang/Number;->floatValue()F

    move-result v8

    .line 712
    .restart local v8    # "y2":F
    invoke-virtual/range {v29 .. v29}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/Number;

    invoke-virtual {v11}, Ljava/lang/Number;->floatValue()F

    move-result v5

    .line 713
    .restart local v5    # "x1":F
    invoke-virtual/range {v29 .. v29}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/Number;

    invoke-virtual {v11}, Ljava/lang/Number;->floatValue()F

    move-result v6

    .line 714
    .restart local v6    # "y1":F
    invoke-virtual/range {v29 .. v29}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/Number;

    invoke-virtual {v11}, Ljava/lang/Number;->floatValue()F

    move-result v35

    .line 715
    .restart local v35    # "y":F
    invoke-virtual/range {v29 .. v29}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/Number;

    invoke-virtual {v11}, Ljava/lang/Number;->floatValue()F

    move-result v34

    .line 716
    .restart local v34    # "x":F
    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    invoke-virtual {v11}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->getTextState()Lorg/icepdf/core/pobjects/graphics/TextState;

    move-result-object v30

    .line 717
    .restart local v30    # "textState":Lorg/icepdf/core/pobjects/graphics/TextState;
    new-instance v11, Landroid/graphics/PointF;

    move/from16 v0, v34

    move/from16 v1, v35

    invoke-direct {v11, v0, v1}, Landroid/graphics/PointF;-><init>(FF)V

    move-object/from16 v0, v30

    invoke-virtual {v0, v11}, Lorg/icepdf/core/pobjects/graphics/TextState;->setType3HorizontalDisplacement(Landroid/graphics/PointF;)V

    .line 719
    new-instance v11, Lorg/icepdf/core/pobjects/PRectangle;

    new-instance v12, Landroid/graphics/PointF;

    invoke-direct {v12, v5, v6}, Landroid/graphics/PointF;-><init>(FF)V

    new-instance v13, Landroid/graphics/PointF;

    invoke-direct {v13, v7, v8}, Landroid/graphics/PointF;-><init>(FF)V

    invoke-direct {v11, v12, v13}, Lorg/icepdf/core/pobjects/PRectangle;-><init>(Landroid/graphics/PointF;Landroid/graphics/PointF;)V

    move-object/from16 v0, v30

    invoke-virtual {v0, v11}, Lorg/icepdf/core/pobjects/graphics/TextState;->setType3BBox(Lorg/icepdf/core/pobjects/PRectangle;)V

    goto/16 :goto_2

    .line 728
    .end local v5    # "x1":F
    .end local v6    # "y1":F
    .end local v7    # "x2":F
    .end local v8    # "y2":F
    .end local v30    # "textState":Lorg/icepdf/core/pobjects/graphics/TextState;
    .end local v34    # "x":F
    .end local v35    # "y":F
    :cond_3d
    const-string/jumbo v11, "B*"

    move-object/from16 v0, v32

    invoke-virtual {v0, v11}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3f

    .line 730
    if-eqz v4, :cond_3e

    .line 732
    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    move-object/from16 v0, v28

    invoke-static {v11, v0, v4}, Lorg/icepdf/core/util/ContentParser;->commonStroke(Lorg/icepdf/core/pobjects/graphics/GraphicsState;Lorg/icepdf/core/pobjects/graphics/Shapes;Landroid/graphics/Path;)V

    .line 733
    move-object/from16 v0, p0

    move-object/from16 v1, v28

    invoke-direct {v0, v1, v4}, Lorg/icepdf/core/util/ContentParser;->commonFill(Lorg/icepdf/core/pobjects/graphics/Shapes;Landroid/graphics/Path;)V

    .line 735
    :cond_3e
    const/4 v4, 0x0

    goto/16 :goto_2

    .line 741
    :cond_3f
    const-string/jumbo v11, "BMC"

    move-object/from16 v0, v32

    invoke-virtual {v0, v11}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_40

    .line 743
    invoke-virtual/range {v29 .. v29}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    goto/16 :goto_2

    .line 747
    :cond_40
    const-string/jumbo v11, "BI"

    move-object/from16 v0, v32

    invoke-virtual {v0, v11}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_41

    .line 753
    move-object/from16 v0, p0

    move-object/from16 v1, v25

    move-object/from16 v2, v28

    invoke-virtual {v0, v1, v2}, Lorg/icepdf/core/util/ContentParser;->parseInlineImage(Lorg/icepdf/core/util/Parser;Lorg/icepdf/core/pobjects/graphics/Shapes;)V

    goto/16 :goto_2

    .line 773
    :cond_41
    const-string/jumbo v11, "W*"

    move-object/from16 v0, v32

    invoke-virtual {v0, v11}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_42

    .line 774
    if-eqz v4, :cond_1

    .line 776
    invoke-virtual {v4}, Landroid/graphics/Path;->close()V

    .line 777
    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    invoke-virtual {v11, v4}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->setClip(Landroid/graphics/Path;)V

    goto/16 :goto_2

    .line 790
    :cond_42
    const-string/jumbo v11, "DP"

    move-object/from16 v0, v32

    invoke-virtual {v0, v11}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_43

    .line 792
    invoke-virtual/range {v29 .. v29}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    .line 793
    invoke-virtual/range {v29 .. v29}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    goto/16 :goto_2

    .line 797
    :cond_43
    const-string/jumbo v11, "MP"

    move-object/from16 v0, v32

    invoke-virtual {v0, v11}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_44

    .line 799
    invoke-virtual/range {v29 .. v29}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    goto/16 :goto_2

    .line 803
    :cond_44
    const-string/jumbo v11, "sh"

    move-object/from16 v0, v32

    invoke-virtual {v0, v11}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_45

    .line 805
    invoke-virtual/range {v29 .. v29}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v23

    .line 807
    .local v23, "o":Ljava/lang/Object;
    move-object/from16 v0, v23

    instance-of v11, v0, Lorg/icepdf/core/pobjects/Name;

    if-eqz v11, :cond_1

    .line 808
    invoke-virtual/range {v29 .. v29}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v27

    check-cast v27, Lorg/icepdf/core/pobjects/Name;

    .line 809
    .local v27, "patternName":Lorg/icepdf/core/pobjects/Name;
    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/icepdf/core/util/ContentParser;->resources:Lorg/icepdf/core/pobjects/Resources;

    invoke-virtual/range {v27 .. v27}, Lorg/icepdf/core/pobjects/Name;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Lorg/icepdf/core/pobjects/Resources;->getShading(Ljava/lang/String;)Lorg/icepdf/core/pobjects/graphics/ShadingPattern;

    move-result-object v26

    .line 810
    .local v26, "pattern":Lorg/icepdf/core/pobjects/graphics/Pattern;
    if-eqz v26, :cond_1

    .line 811
    invoke-interface/range {v26 .. v26}, Lorg/icepdf/core/pobjects/graphics/Pattern;->init()V

    .line 826
    invoke-interface/range {v26 .. v26}, Lorg/icepdf/core/pobjects/graphics/Pattern;->getPaint()Landroid/graphics/Paint;

    move-result-object v11

    move-object/from16 v0, v28

    invoke-virtual {v0, v11}, Lorg/icepdf/core/pobjects/graphics/Shapes;->add(Ljava/lang/Object;)V

    .line 827
    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    invoke-virtual {v11}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->getClip()Landroid/graphics/Path;

    move-result-object v11

    move-object/from16 v0, v28

    invoke-virtual {v0, v11}, Lorg/icepdf/core/pobjects/graphics/Shapes;->add(Ljava/lang/Object;)V

    .line 828
    invoke-virtual/range {v28 .. v28}, Lorg/icepdf/core/pobjects/graphics/Shapes;->addFillCommand()V

    goto/16 :goto_2

    .line 838
    .end local v23    # "o":Ljava/lang/Object;
    .end local v26    # "pattern":Lorg/icepdf/core/pobjects/graphics/Pattern;
    .end local v27    # "patternName":Lorg/icepdf/core/pobjects/Name;
    :cond_45
    const-string/jumbo v11, "Tc"

    move-object/from16 v0, v32

    invoke-virtual {v0, v11}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_46

    .line 839
    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    move-object/from16 v0, v29

    invoke-static {v11, v0}, Lorg/icepdf/core/util/ContentParser;->consume_Tc(Lorg/icepdf/core/pobjects/graphics/GraphicsState;Ljava/util/Stack;)V

    goto/16 :goto_2

    .line 842
    :cond_46
    const-string/jumbo v11, "Tw"

    move-object/from16 v0, v32

    invoke-virtual {v0, v11}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_47

    .line 843
    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    move-object/from16 v0, v29

    invoke-static {v11, v0}, Lorg/icepdf/core/util/ContentParser;->consume_Tw(Lorg/icepdf/core/pobjects/graphics/GraphicsState;Ljava/util/Stack;)V

    goto/16 :goto_2

    .line 846
    :cond_47
    const-string/jumbo v11, "TL"

    move-object/from16 v0, v32

    invoke-virtual {v0, v11}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_48

    .line 847
    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    move-object/from16 v0, v29

    invoke-static {v11, v0}, Lorg/icepdf/core/util/ContentParser;->consume_TL(Lorg/icepdf/core/pobjects/graphics/GraphicsState;Ljava/util/Stack;)V

    goto/16 :goto_2

    .line 850
    :cond_48
    const-string/jumbo v11, "Tr"

    move-object/from16 v0, v32

    invoke-virtual {v0, v11}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_49

    .line 851
    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    move-object/from16 v0, v29

    invoke-static {v11, v0}, Lorg/icepdf/core/util/ContentParser;->consume_Tr(Lorg/icepdf/core/pobjects/graphics/GraphicsState;Ljava/util/Stack;)V

    goto/16 :goto_2

    .line 854
    :cond_49
    const-string/jumbo v11, "Tz"

    move-object/from16 v0, v32

    invoke-virtual {v0, v11}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_4a

    .line 855
    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    move-object/from16 v0, p0

    iget-boolean v12, v0, Lorg/icepdf/core/util/ContentParser;->inTextBlock:Z

    move-object/from16 v0, v29

    invoke-static {v11, v0, v12}, Lorg/icepdf/core/util/ContentParser;->consume_Tz(Lorg/icepdf/core/pobjects/graphics/GraphicsState;Ljava/util/Stack;Z)V

    goto/16 :goto_2

    .line 858
    :cond_4a
    const-string/jumbo v11, "Ts"

    move-object/from16 v0, v32

    invoke-virtual {v0, v11}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_1

    .line 859
    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    move-object/from16 v0, v29

    invoke-static {v11, v0}, Lorg/icepdf/core/util/ContentParser;->consume_Ts(Lorg/icepdf/core/pobjects/graphics/GraphicsState;Ljava/util/Stack;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto/16 :goto_2

    .line 893
    .end local v32    # "tok":Ljava/lang/Object;
    :cond_4b
    invoke-virtual/range {v28 .. v28}, Lorg/icepdf/core/pobjects/graphics/Shapes;->contract()V

    .line 894
    return-object v28
.end method

.method parseInlineImage(Lorg/icepdf/core/util/Parser;Lorg/icepdf/core/pobjects/graphics/Shapes;)V
    .locals 22
    .param p1, "p"    # Lorg/icepdf/core/util/Parser;
    .param p2, "shapes"    # Lorg/icepdf/core/pobjects/graphics/Shapes;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1553
    :try_start_0
    new-instance v14, Ljava/util/Hashtable;

    invoke-direct {v14}, Ljava/util/Hashtable;-><init>()V

    .line 1554
    .local v14, "iih":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/Object;Ljava/lang/Object;>;"
    invoke-virtual/range {p1 .. p1}, Lorg/icepdf/core/util/Parser;->getStreamObject()Ljava/lang/Object;

    move-result-object v17

    .line 1555
    .local v17, "tok":Ljava/lang/Object;
    :goto_0
    const-string/jumbo v4, "ID"

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_9

    .line 1556
    const-string/jumbo v4, "BPC"

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1557
    new-instance v17, Lorg/icepdf/core/pobjects/Name;

    .end local v17    # "tok":Ljava/lang/Object;
    const-string/jumbo v4, "BitsPerComponent"

    move-object/from16 v0, v17

    invoke-direct {v0, v4}, Lorg/icepdf/core/pobjects/Name;-><init>(Ljava/lang/String;)V

    .line 1575
    :cond_0
    :goto_1
    invoke-virtual/range {p1 .. p1}, Lorg/icepdf/core/util/Parser;->getStreamObject()Ljava/lang/Object;

    move-result-object v18

    .line 1577
    .local v18, "tok1":Ljava/lang/Object;
    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v14, v0, v1}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1578
    invoke-virtual/range {p1 .. p1}, Lorg/icepdf/core/util/Parser;->getStreamObject()Ljava/lang/Object;

    move-result-object v17

    .line 1579
    .restart local v17    # "tok":Ljava/lang/Object;
    goto :goto_0

    .line 1558
    .end local v18    # "tok1":Ljava/lang/Object;
    :cond_1
    const-string/jumbo v4, "CS"

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1559
    new-instance v17, Lorg/icepdf/core/pobjects/Name;

    .end local v17    # "tok":Ljava/lang/Object;
    const-string/jumbo v4, "ColorSpace"

    move-object/from16 v0, v17

    invoke-direct {v0, v4}, Lorg/icepdf/core/pobjects/Name;-><init>(Ljava/lang/String;)V

    .local v17, "tok":Lorg/icepdf/core/pobjects/Name;
    goto :goto_1

    .line 1560
    .local v17, "tok":Ljava/lang/Object;
    :cond_2
    const-string/jumbo v4, "D"

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 1561
    new-instance v17, Lorg/icepdf/core/pobjects/Name;

    .end local v17    # "tok":Ljava/lang/Object;
    const-string/jumbo v4, "Decode"

    move-object/from16 v0, v17

    invoke-direct {v0, v4}, Lorg/icepdf/core/pobjects/Name;-><init>(Ljava/lang/String;)V

    .local v17, "tok":Lorg/icepdf/core/pobjects/Name;
    goto :goto_1

    .line 1562
    .local v17, "tok":Ljava/lang/Object;
    :cond_3
    const-string/jumbo v4, "DP"

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 1563
    new-instance v17, Lorg/icepdf/core/pobjects/Name;

    .end local v17    # "tok":Ljava/lang/Object;
    const-string/jumbo v4, "DecodeParms"

    move-object/from16 v0, v17

    invoke-direct {v0, v4}, Lorg/icepdf/core/pobjects/Name;-><init>(Ljava/lang/String;)V

    .local v17, "tok":Lorg/icepdf/core/pobjects/Name;
    goto :goto_1

    .line 1564
    .local v17, "tok":Ljava/lang/Object;
    :cond_4
    const-string/jumbo v4, "F"

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 1565
    new-instance v17, Lorg/icepdf/core/pobjects/Name;

    .end local v17    # "tok":Ljava/lang/Object;
    const-string/jumbo v4, "Filter"

    move-object/from16 v0, v17

    invoke-direct {v0, v4}, Lorg/icepdf/core/pobjects/Name;-><init>(Ljava/lang/String;)V

    .local v17, "tok":Lorg/icepdf/core/pobjects/Name;
    goto :goto_1

    .line 1566
    .local v17, "tok":Ljava/lang/Object;
    :cond_5
    const-string/jumbo v4, "H"

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 1567
    new-instance v17, Lorg/icepdf/core/pobjects/Name;

    .end local v17    # "tok":Ljava/lang/Object;
    const-string/jumbo v4, "Height"

    move-object/from16 v0, v17

    invoke-direct {v0, v4}, Lorg/icepdf/core/pobjects/Name;-><init>(Ljava/lang/String;)V

    .local v17, "tok":Lorg/icepdf/core/pobjects/Name;
    goto :goto_1

    .line 1568
    .local v17, "tok":Ljava/lang/Object;
    :cond_6
    const-string/jumbo v4, "IM"

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 1569
    new-instance v17, Lorg/icepdf/core/pobjects/Name;

    .end local v17    # "tok":Ljava/lang/Object;
    const-string/jumbo v4, "ImageMask"

    move-object/from16 v0, v17

    invoke-direct {v0, v4}, Lorg/icepdf/core/pobjects/Name;-><init>(Ljava/lang/String;)V

    .local v17, "tok":Lorg/icepdf/core/pobjects/Name;
    goto/16 :goto_1

    .line 1570
    .local v17, "tok":Ljava/lang/Object;
    :cond_7
    const-string/jumbo v4, "I"

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 1571
    new-instance v17, Lorg/icepdf/core/pobjects/Name;

    .end local v17    # "tok":Ljava/lang/Object;
    const-string/jumbo v4, "Indexed"

    move-object/from16 v0, v17

    invoke-direct {v0, v4}, Lorg/icepdf/core/pobjects/Name;-><init>(Ljava/lang/String;)V

    .local v17, "tok":Lorg/icepdf/core/pobjects/Name;
    goto/16 :goto_1

    .line 1572
    .local v17, "tok":Ljava/lang/Object;
    :cond_8
    const-string/jumbo v4, "W"

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1573
    new-instance v17, Lorg/icepdf/core/pobjects/Name;

    .end local v17    # "tok":Ljava/lang/Object;
    const-string/jumbo v4, "Width"

    move-object/from16 v0, v17

    invoke-direct {v0, v4}, Lorg/icepdf/core/pobjects/Name;-><init>(Ljava/lang/String;)V

    .local v17, "tok":Lorg/icepdf/core/pobjects/Name;
    goto/16 :goto_1

    .line 1589
    .local v17, "tok":Ljava/lang/Object;
    :cond_9
    new-instance v11, Ljava/io/ByteArrayOutputStream;

    const/16 v4, 0x1000

    invoke-direct {v11, v4}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    .line 1590
    .local v11, "buf":Ljava/io/ByteArrayOutputStream;
    invoke-virtual/range {p1 .. p1}, Lorg/icepdf/core/util/Parser;->peek2()Ljava/lang/String;

    move-result-object v17

    .line 1591
    .local v17, "tok":Ljava/lang/String;
    const/4 v10, 0x0

    .line 1592
    .local v10, "ateEI":Z
    :goto_2
    if-eqz v17, :cond_a

    const-string/jumbo v4, " EI"

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    move-result v4

    if-nez v4, :cond_a

    .line 1594
    :try_start_1
    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Lorg/icepdf/core/util/Parser;->readLineForInlineImage(Ljava/io/OutputStream;)Z
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    move-result v10

    .line 1599
    if-eqz v10, :cond_c

    .line 1603
    :cond_a
    :goto_3
    if-nez v10, :cond_b

    .line 1605
    :try_start_2
    invoke-virtual/range {p1 .. p1}, Lorg/icepdf/core/util/Parser;->getToken()Ljava/lang/Object;

    .line 1607
    :cond_b
    invoke-virtual {v11}, Ljava/io/ByteArrayOutputStream;->flush()V

    .line 1608
    invoke-virtual {v11}, Ljava/io/ByteArrayOutputStream;->close()V

    .line 1609
    invoke-virtual {v11}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v12

    .line 1610
    .local v12, "data":[B
    new-instance v3, Lorg/icepdf/core/io/SeekableByteArrayInputStream;

    invoke-direct {v3, v12}, Lorg/icepdf/core/io/SeekableByteArrayInputStream;-><init>([B)V

    .line 1612
    .local v3, "sbais":Lorg/icepdf/core/io/SeekableByteArrayInputStream;
    new-instance v2, Lorg/icepdf/core/io/SeekableInputConstrainedWrapper;

    const-wide/16 v4, 0x0

    array-length v6, v12

    int-to-long v6, v6

    const/4 v8, 0x1

    invoke-direct/range {v2 .. v8}, Lorg/icepdf/core/io/SeekableInputConstrainedWrapper;-><init>(Lorg/icepdf/core/io/SeekableInput;JJZ)V

    .line 1614
    .local v2, "streamInputWrapper":Lorg/icepdf/core/io/SeekableInputConstrainedWrapper;
    new-instance v16, Lorg/icepdf/core/pobjects/Stream;

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/icepdf/core/util/ContentParser;->library:Lorg/icepdf/core/util/Library;

    move-object/from16 v0, v16

    invoke-direct {v0, v4, v14, v2}, Lorg/icepdf/core/pobjects/Stream;-><init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;Lorg/icepdf/core/io/SeekableInputConstrainedWrapper;)V

    .line 1615
    .local v16, "st":Lorg/icepdf/core/pobjects/Stream;
    const/4 v4, 0x1

    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Lorg/icepdf/core/pobjects/Stream;->setInlineImage(Z)V

    .line 1617
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    invoke-virtual {v4}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->getFillColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/icepdf/core/util/ContentParser;->resources:Lorg/icepdf/core/pobjects/Resources;

    const/4 v6, 0x1

    const/4 v7, 0x0

    move-object/from16 v0, v16

    invoke-virtual {v0, v4, v5, v6, v7}, Lorg/icepdf/core/pobjects/Stream;->getImage(Lorg/apache/poi/java/awt/Color;Lorg/icepdf/core/pobjects/Resources;ZZ)Landroid/graphics/Bitmap;

    move-result-object v15

    .line 1618
    .local v15, "im":Landroid/graphics/Bitmap;
    const/4 v4, 0x0

    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Lorg/icepdf/core/pobjects/Stream;->dispose(Z)V

    .line 1619
    new-instance v9, Landroid/graphics/Matrix;

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    invoke-virtual {v4}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->getCTM()Landroid/graphics/Matrix;

    move-result-object v4

    invoke-direct {v9, v4}, Landroid/graphics/Matrix;-><init>(Landroid/graphics/Matrix;)V

    .line 1620
    .local v9, "af":Landroid/graphics/Matrix;
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    const-wide/high16 v20, -0x4010000000000000L    # -1.0

    move-wide/from16 v0, v20

    invoke-virtual {v4, v6, v7, v0, v1}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->scale(DD)V

    .line 1621
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    const-wide/16 v6, 0x0

    const-wide/high16 v20, -0x4010000000000000L    # -1.0

    move-wide/from16 v0, v20

    invoke-virtual {v4, v6, v7, v0, v1}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->translate(DD)V

    .line 1622
    move-object/from16 v0, p2

    invoke-virtual {v0, v15}, Lorg/icepdf/core/pobjects/graphics/Shapes;->add(Ljava/lang/Object;)V

    .line 1623
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    invoke-virtual {v4, v9}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->set(Landroid/graphics/Matrix;)V

    .line 1629
    .end local v2    # "streamInputWrapper":Lorg/icepdf/core/io/SeekableInputConstrainedWrapper;
    .end local v3    # "sbais":Lorg/icepdf/core/io/SeekableByteArrayInputStream;
    .end local v9    # "af":Landroid/graphics/Matrix;
    .end local v10    # "ateEI":Z
    .end local v11    # "buf":Ljava/io/ByteArrayOutputStream;
    .end local v12    # "data":[B
    .end local v14    # "iih":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/Object;Ljava/lang/Object;>;"
    .end local v15    # "im":Landroid/graphics/Bitmap;
    .end local v16    # "st":Lorg/icepdf/core/pobjects/Stream;
    .end local v17    # "tok":Ljava/lang/String;
    :goto_4
    return-void

    .line 1595
    .restart local v10    # "ateEI":Z
    .restart local v11    # "buf":Ljava/io/ByteArrayOutputStream;
    .restart local v14    # "iih":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/Object;Ljava/lang/Object;>;"
    .restart local v17    # "tok":Ljava/lang/String;
    :catch_0
    move-exception v13

    .line 1596
    .local v13, "e":Ljava/io/IOException;
    const-string/jumbo v4, "ContentParser"

    invoke-virtual {v13}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    goto/16 :goto_3

    .line 1624
    .end local v10    # "ateEI":Z
    .end local v11    # "buf":Ljava/io/ByteArrayOutputStream;
    .end local v13    # "e":Ljava/io/IOException;
    .end local v14    # "iih":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/Object;Ljava/lang/Object;>;"
    .end local v17    # "tok":Ljava/lang/String;
    :catch_1
    move-exception v13

    .line 1625
    .restart local v13    # "e":Ljava/io/IOException;
    throw v13

    .line 1601
    .end local v13    # "e":Ljava/io/IOException;
    .restart local v10    # "ateEI":Z
    .restart local v11    # "buf":Ljava/io/ByteArrayOutputStream;
    .restart local v14    # "iih":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/Object;Ljava/lang/Object;>;"
    .restart local v17    # "tok":Ljava/lang/String;
    :cond_c
    :try_start_3
    invoke-virtual/range {p1 .. p1}, Lorg/icepdf/core/util/Parser;->peek2()Ljava/lang/String;
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    move-result-object v17

    goto/16 :goto_2

    .line 1626
    .end local v10    # "ateEI":Z
    .end local v11    # "buf":Ljava/io/ByteArrayOutputStream;
    .end local v14    # "iih":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/Object;Ljava/lang/Object;>;"
    .end local v17    # "tok":Ljava/lang/String;
    :catch_2
    move-exception v13

    .line 1627
    .local v13, "e":Ljava/lang/Exception;
    sget-object v4, Lorg/icepdf/core/util/ContentParser;->logger:Ljava/util/logging/Logger;

    sget-object v5, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string/jumbo v6, "Error parsing inline image."

    invoke-virtual {v4, v5, v6, v13}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_4
.end method

.method parseText(Lorg/icepdf/core/util/Parser;Lorg/icepdf/core/pobjects/graphics/Shapes;D)F
    .locals 55
    .param p1, "parser"    # Lorg/icepdf/core/util/Parser;
    .param p2, "shapes"    # Lorg/icepdf/core/pobjects/graphics/Shapes;
    .param p3, "previousBTStart"    # D
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 973
    new-instance v40, Ljava/util/Stack;

    invoke-direct/range {v40 .. v40}, Ljava/util/Stack;-><init>()V

    .line 974
    .local v40, "stack":Ljava/util/Stack;, "Ljava/util/Stack<Ljava/lang/Object;>;"
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lorg/icepdf/core/util/ContentParser;->inTextBlock:Z

    .line 975
    const/16 v39, 0x0

    .line 979
    .local v39, "shift":F
    const/4 v5, 0x0

    .line 980
    .local v5, "previousAdvance":F
    new-instance v4, Landroid/graphics/PointF;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-direct {v4, v2, v3}, Landroid/graphics/PointF;-><init>(FF)V

    .line 981
    .local v4, "advance":Landroid/graphics/PointF;
    new-instance v2, Landroid/graphics/Matrix;

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    invoke-virtual {v3}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->getCTM()Landroid/graphics/Matrix;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/graphics/Matrix;-><init>(Landroid/graphics/Matrix;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lorg/icepdf/core/util/ContentParser;->textBlockBase:Landroid/graphics/Matrix;

    .line 984
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    invoke-virtual {v2}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->getTextState()Lorg/icepdf/core/pobjects/graphics/TextState;

    move-result-object v2

    new-instance v3, Landroid/graphics/Matrix;

    invoke-direct {v3}, Landroid/graphics/Matrix;-><init>()V

    iput-object v3, v2, Lorg/icepdf/core/pobjects/graphics/TextState;->tmatrix:Landroid/graphics/Matrix;

    .line 985
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    invoke-virtual {v2}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->getTextState()Lorg/icepdf/core/pobjects/graphics/TextState;

    move-result-object v2

    new-instance v3, Landroid/graphics/Matrix;

    invoke-direct {v3}, Landroid/graphics/Matrix;-><init>()V

    iput-object v3, v2, Lorg/icepdf/core/pobjects/graphics/TextState;->tlmatrix:Landroid/graphics/Matrix;

    .line 986
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    const-wide/high16 v10, -0x4010000000000000L    # -1.0

    invoke-virtual {v2, v6, v7, v10, v11}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->scale(DD)V

    .line 989
    invoke-virtual/range {p2 .. p2}, Lorg/icepdf/core/pobjects/graphics/Shapes;->getPageText()Lorg/icepdf/core/pobjects/graphics/text/PageText;

    move-result-object v38

    .line 991
    .local v38, "pageText":Lorg/icepdf/core/pobjects/graphics/text/PageText;
    const/16 v23, 0x1

    .line 992
    .local v23, "isYstart":Z
    const/16 v53, 0x0

    .line 995
    .local v53, "yBTStart":F
    new-instance v8, Lorg/icepdf/core/pobjects/graphics/GlyphOutlineClip;

    invoke-direct {v8}, Lorg/icepdf/core/pobjects/graphics/GlyphOutlineClip;-><init>()V

    .line 998
    .local v8, "glyphOutlineClip":Lorg/icepdf/core/pobjects/graphics/GlyphOutlineClip;
    invoke-virtual/range {p1 .. p1}, Lorg/icepdf/core/util/Parser;->getStreamObject()Ljava/lang/Object;

    move-result-object v31

    .line 999
    .local v31, "nextToken":Ljava/lang/Object;
    :goto_0
    const-string/jumbo v2, "ET"

    move-object/from16 v0, v31

    invoke-virtual {v0, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_33

    .line 1003
    move-object/from16 v0, v31

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_32

    .line 1006
    const-string/jumbo v2, "Tj"

    move-object/from16 v0, v31

    invoke-virtual {v0, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1008
    invoke-virtual/range {v40 .. v40}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v44

    .line 1011
    .local v44, "tjValue":Ljava/lang/Object;
    move-object/from16 v0, v44

    instance-of v2, v0, Lorg/icepdf/core/pobjects/StringObject;

    if-eqz v2, :cond_0

    move-object/from16 v41, v44

    .line 1012
    check-cast v41, Lorg/icepdf/core/pobjects/StringObject;

    .line 1013
    .local v41, "stringObject":Lorg/icepdf/core/pobjects/StringObject;
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    invoke-virtual {v2}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->getTextState()Lorg/icepdf/core/pobjects/graphics/TextState;

    move-result-object v43

    .line 1015
    .local v43, "textState":Lorg/icepdf/core/pobjects/graphics/TextState;
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    invoke-static {v2}, Lorg/icepdf/core/util/ContentParser;->applyTextScaling(Lorg/icepdf/core/pobjects/graphics/GraphicsState;)Landroid/graphics/Matrix;

    move-result-object v46

    .line 1020
    .local v46, "tmp":Landroid/graphics/Matrix;
    move-object/from16 v0, v43

    iget-object v2, v0, Lorg/icepdf/core/pobjects/graphics/TextState;->font:Lorg/icepdf/core/pobjects/fonts/Font;

    invoke-virtual {v2}, Lorg/icepdf/core/pobjects/fonts/Font;->getSubTypeFormat()I

    move-result v2

    move-object/from16 v0, v43

    iget-object v3, v0, Lorg/icepdf/core/pobjects/graphics/TextState;->font:Lorg/icepdf/core/pobjects/fonts/Font;

    invoke-virtual {v3}, Lorg/icepdf/core/pobjects/fonts/Font;->getFont()Lorg/icepdf/core/pobjects/fonts/FontFile;

    move-result-object v3

    move-object/from16 v0, v41

    invoke-interface {v0, v2, v3}, Lorg/icepdf/core/pobjects/StringObject;->getLiteralStringBuffer(ILorg/icepdf/core/pobjects/fonts/FontFile;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    invoke-virtual {v2}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->getTextState()Lorg/icepdf/core/pobjects/graphics/TextState;

    move-result-object v6

    move-object/from16 v2, p0

    move-object/from16 v7, p2

    invoke-direct/range {v2 .. v8}, Lorg/icepdf/core/util/ContentParser;->drawString(Ljava/lang/StringBuilder;Landroid/graphics/PointF;FLorg/icepdf/core/pobjects/graphics/TextState;Lorg/icepdf/core/pobjects/graphics/Shapes;Lorg/icepdf/core/pobjects/graphics/GlyphOutlineClip;)Landroid/graphics/PointF;

    move-result-object v18

    .line 1029
    .local v18, "d":Landroid/graphics/PointF;
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    move-object/from16 v0, v46

    invoke-virtual {v2, v0}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->set(Landroid/graphics/Matrix;)V

    .line 1030
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    move-object/from16 v0, v18

    iget v3, v0, Landroid/graphics/PointF;->x:F

    float-to-double v6, v3

    const-wide/16 v10, 0x0

    invoke-virtual {v2, v6, v7, v10, v11}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->translate(DD)V

    .line 1031
    move-object/from16 v0, v18

    iget v2, v0, Landroid/graphics/PointF;->x:F

    add-float v39, v39, v2

    .line 1032
    const/4 v5, 0x0

    .line 1033
    const/4 v2, 0x0

    iput v2, v4, Landroid/graphics/PointF;->x:F

    .line 1034
    const/4 v2, 0x0

    iput v2, v4, Landroid/graphics/PointF;->y:F

    .line 1518
    .end local v18    # "d":Landroid/graphics/PointF;
    .end local v41    # "stringObject":Lorg/icepdf/core/pobjects/StringObject;
    .end local v43    # "textState":Lorg/icepdf/core/pobjects/graphics/TextState;
    .end local v44    # "tjValue":Ljava/lang/Object;
    .end local v46    # "tmp":Landroid/graphics/Matrix;
    :cond_0
    :goto_1
    invoke-virtual/range {p1 .. p1}, Lorg/icepdf/core/util/Parser;->getStreamObject()Ljava/lang/Object;

    move-result-object v31

    goto/16 :goto_0

    .line 1039
    :cond_1
    const-string/jumbo v2, "Tc"

    move-object/from16 v0, v31

    invoke-virtual {v0, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1041
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    invoke-virtual {v2}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->getTextState()Lorg/icepdf/core/pobjects/graphics/TextState;

    move-result-object v3

    invoke-virtual/range {v40 .. v40}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Number;

    invoke-virtual {v2}, Ljava/lang/Number;->floatValue()F

    move-result v2

    iput v2, v3, Lorg/icepdf/core/pobjects/graphics/TextState;->cspace:F

    goto :goto_1

    .line 1045
    :cond_2
    const-string/jumbo v2, "Tw"

    move-object/from16 v0, v31

    invoke-virtual {v0, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1047
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    invoke-virtual {v2}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->getTextState()Lorg/icepdf/core/pobjects/graphics/TextState;

    move-result-object v3

    invoke-virtual/range {v40 .. v40}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Number;

    invoke-virtual {v2}, Ljava/lang/Number;->floatValue()F

    move-result v2

    iput v2, v3, Lorg/icepdf/core/pobjects/graphics/TextState;->wspace:F

    goto :goto_1

    .line 1052
    :cond_3
    const-string/jumbo v2, "Td"

    move-object/from16 v0, v31

    invoke-virtual {v0, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 1054
    invoke-virtual/range {v40 .. v40}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Number;

    invoke-virtual {v2}, Ljava/lang/Number;->floatValue()F

    move-result v52

    .line 1055
    .local v52, "y":F
    invoke-virtual/range {v40 .. v40}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Number;

    invoke-virtual {v2}, Ljava/lang/Number;->floatValue()F

    move-result v51

    .line 1057
    .local v51, "x":F
    const/16 v2, 0x9

    new-array v0, v2, [F

    move-object/from16 v48, v0

    .line 1058
    .local v48, "values1":[F
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    invoke-virtual {v2}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->getCTM()Landroid/graphics/Matrix;

    move-result-object v2

    move-object/from16 v0, v48

    invoke-virtual {v2, v0}, Landroid/graphics/Matrix;->getValues([F)V

    .line 1059
    const/4 v2, 0x5

    aget v2, v48, v2

    float-to-double v0, v2

    move-wide/from16 v36, v0

    .line 1062
    .local v36, "oldY":D
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    move/from16 v0, v39

    neg-float v3, v0

    float-to-double v6, v3

    const-wide/16 v10, 0x0

    invoke-virtual {v2, v6, v7, v10, v11}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->translate(DD)V

    .line 1063
    const/16 v39, 0x0

    .line 1064
    const/4 v5, 0x0

    .line 1065
    const/4 v2, 0x0

    iput v2, v4, Landroid/graphics/PointF;->x:F

    .line 1066
    const/4 v2, 0x0

    iput v2, v4, Landroid/graphics/PointF;->y:F

    .line 1069
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    move/from16 v0, v51

    float-to-double v6, v0

    move/from16 v0, v52

    neg-float v3, v0

    float-to-double v10, v3

    invoke-virtual {v2, v6, v7, v10, v11}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->translate(DD)V

    .line 1071
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    invoke-virtual {v2}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->getCTM()Landroid/graphics/Matrix;

    move-result-object v2

    move-object/from16 v0, v48

    invoke-virtual {v2, v0}, Landroid/graphics/Matrix;->getValues([F)V

    .line 1073
    const/4 v2, 0x5

    aget v25, v48, v2

    .line 1075
    .local v25, "newY":F
    if-eqz v23, :cond_4

    .line 1076
    move/from16 v53, v25

    .line 1077
    const/16 v23, 0x0

    .line 1078
    move/from16 v0, v53

    float-to-double v2, v0

    cmpl-double v2, p3, v2

    if-eqz v2, :cond_4

    .line 1079
    invoke-virtual/range {v38 .. v38}, Lorg/icepdf/core/pobjects/graphics/text/PageText;->newLine()V

    .line 1085
    :cond_4
    const/4 v2, 0x0

    cmpl-float v2, v52, v2

    if-eqz v2, :cond_0

    invoke-static/range {v25 .. v25}, Ljava/lang/Math;->round(F)I

    move-result v2

    int-to-long v2, v2

    invoke-static/range {v36 .. v37}, Ljava/lang/Math;->round(D)J

    move-result-wide v6

    cmp-long v2, v2, v6

    if-eqz v2, :cond_0

    .line 1086
    invoke-virtual/range {v38 .. v38}, Lorg/icepdf/core/pobjects/graphics/text/PageText;->newLine()V

    goto/16 :goto_1

    .line 1096
    .end local v25    # "newY":F
    .end local v36    # "oldY":D
    .end local v48    # "values1":[F
    .end local v51    # "x":F
    .end local v52    # "y":F
    :cond_5
    const-string/jumbo v2, "Tm"

    move-object/from16 v0, v31

    invoke-virtual {v0, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 1098
    const/16 v39, 0x0

    .line 1099
    const/4 v5, 0x0

    .line 1100
    const/4 v2, 0x0

    iput v2, v4, Landroid/graphics/PointF;->x:F

    .line 1101
    const/4 v2, 0x0

    iput v2, v4, Landroid/graphics/PointF;->y:F

    .line 1107
    const/4 v2, 0x6

    new-array v0, v2, [F

    move-object/from16 v45, v0

    fill-array-data v45, :array_0

    .line 1108
    .local v45, "tm":[F
    const/16 v21, 0x0

    .local v21, "i":I
    const/16 v20, 0x5

    .local v20, "hits":I
    invoke-virtual/range {v40 .. v40}, Ljava/util/Stack;->size()I

    move-result v24

    .local v24, "max":I
    :goto_2
    const/4 v2, -0x1

    move/from16 v0, v20

    if-eq v0, v2, :cond_7

    move/from16 v0, v21

    move/from16 v1, v24

    if-ge v0, v1, :cond_7

    .line 1109
    invoke-virtual/range {v40 .. v40}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v30

    .line 1110
    .local v30, "next":Ljava/lang/Object;
    move-object/from16 v0, v30

    instance-of v2, v0, Ljava/lang/Number;

    if-eqz v2, :cond_6

    .line 1111
    check-cast v30, Ljava/lang/Number;

    .end local v30    # "next":Ljava/lang/Object;
    invoke-virtual/range {v30 .. v30}, Ljava/lang/Number;->floatValue()F

    move-result v2

    aput v2, v45, v20

    .line 1112
    add-int/lit8 v20, v20, -0x1

    .line 1108
    :cond_6
    add-int/lit8 v21, v21, 0x1

    goto :goto_2

    .line 1115
    :cond_7
    new-instance v16, Landroid/graphics/Matrix;

    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/icepdf/core/util/ContentParser;->textBlockBase:Landroid/graphics/Matrix;

    move-object/from16 v0, v16

    invoke-direct {v0, v2}, Landroid/graphics/Matrix;-><init>(Landroid/graphics/Matrix;)V

    .line 1119
    .local v16, "af":Landroid/graphics/Matrix;
    const/16 v2, 0x9

    new-array v0, v2, [F

    move-object/from16 v49, v0

    .line 1120
    .local v49, "values2":[F
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    invoke-virtual {v2}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->getCTM()Landroid/graphics/Matrix;

    move-result-object v2

    move-object/from16 v0, v49

    invoke-virtual {v2, v0}, Landroid/graphics/Matrix;->getValues([F)V

    .line 1122
    const/4 v2, 0x5

    aget v2, v49, v2

    float-to-double v0, v2

    move-wide/from16 v34, v0

    .line 1123
    .local v34, "oldTransY":D
    const/4 v2, 0x4

    aget v2, v49, v2

    float-to-double v0, v2

    move-wide/from16 v32, v0

    .line 1128
    .local v32, "oldScaleY":D
    new-instance v42, Landroid/graphics/Matrix;

    invoke-direct/range {v42 .. v42}, Landroid/graphics/Matrix;-><init>()V

    .line 1130
    .local v42, "temp":Landroid/graphics/Matrix;
    move-object/from16 v0, v42

    move-object/from16 v1, v45

    invoke-static {v0, v1}, Lcom/samsung/thumbnail/util/Utils;->setMatrixFromArray(Landroid/graphics/Matrix;[F)V

    .line 1132
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    invoke-virtual {v2}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->getTextState()Lorg/icepdf/core/pobjects/graphics/TextState;

    move-result-object v2

    move-object/from16 v0, v42

    iput-object v0, v2, Lorg/icepdf/core/pobjects/graphics/TextState;->tmatrix:Landroid/graphics/Matrix;

    .line 1134
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    invoke-virtual {v2}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->getTextState()Lorg/icepdf/core/pobjects/graphics/TextState;

    move-result-object v2

    iget-object v2, v2, Lorg/icepdf/core/pobjects/graphics/TextState;->tmatrix:Landroid/graphics/Matrix;

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Landroid/graphics/Matrix;->preConcat(Landroid/graphics/Matrix;)Z

    .line 1136
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    move-object/from16 v0, v16

    invoke-virtual {v2, v0}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->set(Landroid/graphics/Matrix;)V

    .line 1137
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    const-wide/high16 v10, -0x4010000000000000L    # -1.0

    invoke-virtual {v2, v6, v7, v10, v11}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->scale(DD)V

    .line 1141
    if-eqz v23, :cond_8

    .line 1142
    const/4 v2, 0x5

    aget v53, v45, v2

    .line 1143
    const/16 v23, 0x0

    .line 1144
    move/from16 v0, v53

    float-to-double v2, v0

    cmpl-double v2, p3, v2

    if-eqz v2, :cond_8

    .line 1145
    invoke-virtual/range {v38 .. v38}, Lorg/icepdf/core/pobjects/graphics/text/PageText;->newLine()V

    .line 1150
    :cond_8
    const/16 v2, 0x9

    new-array v0, v2, [F

    move-object/from16 v50, v0

    .line 1151
    .local v50, "values3":[F
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    invoke-virtual {v2}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->getCTM()Landroid/graphics/Matrix;

    move-result-object v2

    move-object/from16 v0, v50

    invoke-virtual {v2, v0}, Landroid/graphics/Matrix;->getValues([F)V

    .line 1153
    const/4 v2, 0x5

    aget v2, v50, v2

    float-to-double v0, v2

    move-wide/from16 v28, v0

    .line 1154
    .local v28, "newTransY":D
    const/4 v2, 0x4

    aget v2, v50, v2

    float-to-double v0, v2

    move-wide/from16 v26, v0

    .line 1158
    .local v26, "newScaleY":D
    invoke-static/range {v34 .. v35}, Ljava/lang/Math;->round(D)J

    move-result-wide v2

    invoke-static/range {v28 .. v29}, Ljava/lang/Math;->round(D)J

    move-result-wide v6

    cmp-long v2, v2, v6

    if-eqz v2, :cond_9

    .line 1159
    invoke-virtual/range {v38 .. v38}, Lorg/icepdf/core/pobjects/graphics/text/PageText;->newLine()V

    goto/16 :goto_1

    .line 1160
    :cond_9
    invoke-static/range {v32 .. v33}, Ljava/lang/Math;->abs(D)D

    move-result-wide v2

    invoke-static/range {v26 .. v27}, Ljava/lang/Math;->abs(D)D

    move-result-wide v6

    cmpl-double v2, v2, v6

    if-eqz v2, :cond_0

    .line 1161
    invoke-virtual/range {v38 .. v38}, Lorg/icepdf/core/pobjects/graphics/text/PageText;->newLine()V

    goto/16 :goto_1

    .line 1167
    .end local v16    # "af":Landroid/graphics/Matrix;
    .end local v20    # "hits":I
    .end local v21    # "i":I
    .end local v24    # "max":I
    .end local v26    # "newScaleY":D
    .end local v28    # "newTransY":D
    .end local v32    # "oldScaleY":D
    .end local v34    # "oldTransY":D
    .end local v42    # "temp":Landroid/graphics/Matrix;
    .end local v45    # "tm":[F
    .end local v49    # "values2":[F
    .end local v50    # "values3":[F
    :cond_a
    const-string/jumbo v2, "Tf"

    move-object/from16 v0, v31

    invoke-virtual {v0, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 1168
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/icepdf/core/util/ContentParser;->resources:Lorg/icepdf/core/pobjects/Resources;

    move-object/from16 v0, v40

    invoke-static {v2, v0, v3}, Lorg/icepdf/core/util/ContentParser;->consume_Tf(Lorg/icepdf/core/pobjects/graphics/GraphicsState;Ljava/util/Stack;Lorg/icepdf/core/pobjects/Resources;)V

    goto/16 :goto_1

    .line 1172
    :cond_b
    const-string/jumbo v2, "TJ"

    move-object/from16 v0, v31

    invoke-virtual {v0, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_f

    .line 1175
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    invoke-static {v2}, Lorg/icepdf/core/util/ContentParser;->applyTextScaling(Lorg/icepdf/core/pobjects/graphics/GraphicsState;)Landroid/graphics/Matrix;

    move-result-object v46

    .line 1179
    .restart local v46    # "tmp":Landroid/graphics/Matrix;
    invoke-virtual/range {v40 .. v40}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v47

    check-cast v47, Ljava/util/Vector;

    .line 1184
    .local v47, "v":Ljava/util/Vector;
    invoke-virtual/range {v47 .. v47}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v22

    .local v22, "i$":Ljava/util/Iterator;
    :goto_3
    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_e

    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v17

    .line 1185
    .local v17, "currentObject":Ljava/lang/Object;
    move-object/from16 v0, v17

    instance-of v2, v0, Lorg/icepdf/core/pobjects/StringObject;

    if-eqz v2, :cond_d

    move-object/from16 v41, v17

    .line 1186
    check-cast v41, Lorg/icepdf/core/pobjects/StringObject;

    .line 1187
    .restart local v41    # "stringObject":Lorg/icepdf/core/pobjects/StringObject;
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    invoke-virtual {v2}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->getTextState()Lorg/icepdf/core/pobjects/graphics/TextState;

    move-result-object v43

    .line 1189
    .restart local v43    # "textState":Lorg/icepdf/core/pobjects/graphics/TextState;
    move-object/from16 v0, v43

    iget-object v2, v0, Lorg/icepdf/core/pobjects/graphics/TextState;->font:Lorg/icepdf/core/pobjects/fonts/Font;

    invoke-virtual {v2}, Lorg/icepdf/core/pobjects/fonts/Font;->getSubTypeFormat()I

    move-result v2

    move-object/from16 v0, v43

    iget-object v3, v0, Lorg/icepdf/core/pobjects/graphics/TextState;->font:Lorg/icepdf/core/pobjects/fonts/Font;

    invoke-virtual {v3}, Lorg/icepdf/core/pobjects/fonts/Font;->getFont()Lorg/icepdf/core/pobjects/fonts/FontFile;

    move-result-object v3

    move-object/from16 v0, v41

    invoke-interface {v0, v2, v3}, Lorg/icepdf/core/pobjects/StringObject;->getLiteralStringBuffer(ILorg/icepdf/core/pobjects/fonts/FontFile;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    invoke-virtual {v2}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->getTextState()Lorg/icepdf/core/pobjects/graphics/TextState;

    move-result-object v6

    move-object/from16 v2, p0

    move-object/from16 v7, p2

    invoke-direct/range {v2 .. v8}, Lorg/icepdf/core/util/ContentParser;->drawString(Ljava/lang/StringBuilder;Landroid/graphics/PointF;FLorg/icepdf/core/pobjects/graphics/TextState;Lorg/icepdf/core/pobjects/graphics/Shapes;Lorg/icepdf/core/pobjects/graphics/GlyphOutlineClip;)Landroid/graphics/PointF;

    move-result-object v4

    .line 1203
    .end local v41    # "stringObject":Lorg/icepdf/core/pobjects/StringObject;
    .end local v43    # "textState":Lorg/icepdf/core/pobjects/graphics/TextState;
    :cond_c
    :goto_4
    iget v5, v4, Landroid/graphics/PointF;->x:F

    .line 1204
    goto :goto_3

    .line 1197
    :cond_d
    move-object/from16 v0, v17

    instance-of v2, v0, Ljava/lang/Number;

    if-eqz v2, :cond_c

    move-object/from16 v19, v17

    .line 1198
    check-cast v19, Ljava/lang/Number;

    .line 1199
    .local v19, "f":Ljava/lang/Number;
    iget v2, v4, Landroid/graphics/PointF;->x:F

    float-to-double v2, v2

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Number;->floatValue()F

    move-result v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    invoke-virtual {v7}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->getTextState()Lorg/icepdf/core/pobjects/graphics/TextState;

    move-result-object v7

    iget-object v7, v7, Lorg/icepdf/core/pobjects/graphics/TextState;->currentfont:Lorg/icepdf/core/pobjects/fonts/FontFile;

    invoke-interface {v7}, Lorg/icepdf/core/pobjects/fonts/FontFile;->getSize()F

    move-result v7

    mul-float/2addr v6, v7

    float-to-double v6, v6

    const-wide v10, 0x408f400000000000L    # 1000.0

    div-double/2addr v6, v10

    sub-double/2addr v2, v6

    double-to-float v2, v2

    iput v2, v4, Landroid/graphics/PointF;->x:F

    goto :goto_4

    .line 1205
    .end local v17    # "currentObject":Ljava/lang/Object;
    .end local v19    # "f":Ljava/lang/Number;
    :cond_e
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    move-object/from16 v0, v46

    invoke-virtual {v2, v0}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->set(Landroid/graphics/Matrix;)V

    goto/16 :goto_1

    .line 1210
    .end local v22    # "i$":Ljava/util/Iterator;
    .end local v46    # "tmp":Landroid/graphics/Matrix;
    .end local v47    # "v":Ljava/util/Vector;
    :cond_f
    const-string/jumbo v2, "TD"

    move-object/from16 v0, v31

    invoke-virtual {v0, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_11

    .line 1212
    invoke-virtual/range {v40 .. v40}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Number;

    invoke-virtual {v2}, Ljava/lang/Number;->floatValue()F

    move-result v52

    .line 1213
    .restart local v52    # "y":F
    invoke-virtual/range {v40 .. v40}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Number;

    invoke-virtual {v2}, Ljava/lang/Number;->floatValue()F

    move-result v51

    .line 1214
    .restart local v51    # "x":F
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    move/from16 v0, v39

    neg-float v3, v0

    float-to-double v6, v3

    const-wide/16 v10, 0x0

    invoke-virtual {v2, v6, v7, v10, v11}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->translate(DD)V

    .line 1215
    const/16 v39, 0x0

    .line 1216
    const/4 v5, 0x0

    .line 1217
    const/4 v2, 0x0

    iput v2, v4, Landroid/graphics/PointF;->x:F

    .line 1218
    const/4 v2, 0x0

    iput v2, v4, Landroid/graphics/PointF;->y:F

    .line 1219
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    move/from16 v0, v51

    float-to-double v6, v0

    move/from16 v0, v52

    neg-float v3, v0

    float-to-double v10, v3

    invoke-virtual {v2, v6, v7, v10, v11}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->translate(DD)V

    .line 1220
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    invoke-virtual {v2}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->getTextState()Lorg/icepdf/core/pobjects/graphics/TextState;

    move-result-object v2

    move/from16 v0, v52

    neg-float v3, v0

    iput v3, v2, Lorg/icepdf/core/pobjects/graphics/TextState;->leading:F

    .line 1223
    if-eqz v23, :cond_10

    .line 1224
    move/from16 v53, v52

    .line 1225
    const/16 v23, 0x0

    .line 1229
    :cond_10
    const/4 v2, 0x0

    cmpl-float v2, v52, v2

    if-eqz v2, :cond_0

    .line 1230
    invoke-virtual/range {v38 .. v38}, Lorg/icepdf/core/pobjects/graphics/text/PageText;->newLine()V

    goto/16 :goto_1

    .line 1240
    .end local v51    # "x":F
    .end local v52    # "y":F
    :cond_11
    const-string/jumbo v2, "TL"

    move-object/from16 v0, v31

    invoke-virtual {v0, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_12

    .line 1242
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    invoke-virtual {v2}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->getTextState()Lorg/icepdf/core/pobjects/graphics/TextState;

    move-result-object v3

    invoke-virtual/range {v40 .. v40}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Number;

    invoke-virtual {v2}, Ljava/lang/Number;->floatValue()F

    move-result v2

    iput v2, v3, Lorg/icepdf/core/pobjects/graphics/TextState;->leading:F

    goto/16 :goto_1

    .line 1247
    :cond_12
    const-string/jumbo v2, "q"

    move-object/from16 v0, v31

    invoke-virtual {v0, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_13

    .line 1248
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    invoke-static {v2}, Lorg/icepdf/core/util/ContentParser;->consume_q(Lorg/icepdf/core/pobjects/graphics/GraphicsState;)Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    goto/16 :goto_1

    .line 1252
    :cond_13
    const-string/jumbo v2, "Q"

    move-object/from16 v0, v31

    invoke-virtual {v0, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_14

    .line 1253
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    move-object/from16 v0, p2

    invoke-static {v2, v0}, Lorg/icepdf/core/util/ContentParser;->consume_Q(Lorg/icepdf/core/pobjects/graphics/GraphicsState;Lorg/icepdf/core/pobjects/graphics/Shapes;)Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    goto/16 :goto_1

    .line 1258
    :cond_14
    const-string/jumbo v2, "cm"

    move-object/from16 v0, v31

    invoke-virtual {v0, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_15

    .line 1259
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lorg/icepdf/core/util/ContentParser;->inTextBlock:Z

    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/icepdf/core/util/ContentParser;->textBlockBase:Landroid/graphics/Matrix;

    move-object/from16 v0, v40

    invoke-static {v2, v0, v3, v6}, Lorg/icepdf/core/util/ContentParser;->consume_cm(Lorg/icepdf/core/pobjects/graphics/GraphicsState;Ljava/util/Stack;ZLandroid/graphics/Matrix;)V

    goto/16 :goto_1

    .line 1263
    :cond_15
    const-string/jumbo v2, "T*"

    move-object/from16 v0, v31

    invoke-virtual {v0, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_16

    .line 1265
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    move/from16 v0, v39

    neg-float v3, v0

    float-to-double v6, v3

    const-wide/16 v10, 0x0

    invoke-virtual {v2, v6, v7, v10, v11}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->translate(DD)V

    .line 1266
    const/16 v39, 0x0

    .line 1267
    const/4 v5, 0x0

    .line 1268
    const/4 v2, 0x0

    iput v2, v4, Landroid/graphics/PointF;->x:F

    .line 1269
    const/4 v2, 0x0

    iput v2, v4, Landroid/graphics/PointF;->y:F

    .line 1271
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    const-wide/16 v6, 0x0

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    invoke-virtual {v3}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->getTextState()Lorg/icepdf/core/pobjects/graphics/TextState;

    move-result-object v3

    iget v3, v3, Lorg/icepdf/core/pobjects/graphics/TextState;->leading:F

    float-to-double v10, v3

    invoke-virtual {v2, v6, v7, v10, v11}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->translate(DD)V

    .line 1273
    invoke-virtual/range {v38 .. v38}, Lorg/icepdf/core/pobjects/graphics/text/PageText;->newLine()V

    goto/16 :goto_1

    .line 1274
    :cond_16
    const-string/jumbo v2, "BDC"

    move-object/from16 v0, v31

    invoke-virtual {v0, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_17

    .line 1276
    invoke-virtual/range {v40 .. v40}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    .line 1277
    invoke-virtual/range {v40 .. v40}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    goto/16 :goto_1

    .line 1290
    :cond_17
    const-string/jumbo v2, "gs"

    move-object/from16 v0, v31

    invoke-virtual {v0, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_18

    .line 1291
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/icepdf/core/util/ContentParser;->resources:Lorg/icepdf/core/pobjects/Resources;

    move-object/from16 v0, v40

    invoke-static {v2, v0, v3}, Lorg/icepdf/core/util/ContentParser;->consume_gs(Lorg/icepdf/core/pobjects/graphics/GraphicsState;Ljava/util/Stack;Lorg/icepdf/core/pobjects/Resources;)V

    goto/16 :goto_1

    .line 1295
    :cond_18
    const-string/jumbo v2, "w"

    move-object/from16 v0, v31

    invoke-virtual {v0, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_19

    const-string/jumbo v2, "LW"

    move-object/from16 v0, v31

    invoke-virtual {v0, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1a

    .line 1297
    :cond_19
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    move-object/from16 v0, v40

    move-object/from16 v1, p2

    invoke-static {v2, v0, v1}, Lorg/icepdf/core/util/ContentParser;->consume_w(Lorg/icepdf/core/pobjects/graphics/GraphicsState;Ljava/util/Stack;Lorg/icepdf/core/pobjects/graphics/Shapes;)V

    goto/16 :goto_1

    .line 1301
    :cond_1a
    const-string/jumbo v2, "sc"

    move-object/from16 v0, v31

    invoke-virtual {v0, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1b

    const-string/jumbo v2, "scn"

    move-object/from16 v0, v31

    invoke-virtual {v0, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1c

    .line 1303
    :cond_1b
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/icepdf/core/util/ContentParser;->library:Lorg/icepdf/core/util/Library;

    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/icepdf/core/util/ContentParser;->resources:Lorg/icepdf/core/pobjects/Resources;

    move-object/from16 v0, v40

    invoke-static {v2, v0, v3, v6}, Lorg/icepdf/core/util/ContentParser;->consume_sc(Lorg/icepdf/core/pobjects/graphics/GraphicsState;Ljava/util/Stack;Lorg/icepdf/core/util/Library;Lorg/icepdf/core/pobjects/Resources;)V

    goto/16 :goto_1

    .line 1307
    :cond_1c
    const-string/jumbo v2, "k"

    move-object/from16 v0, v31

    invoke-virtual {v0, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1d

    .line 1308
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/icepdf/core/util/ContentParser;->library:Lorg/icepdf/core/util/Library;

    move-object/from16 v0, v40

    invoke-static {v2, v0, v3}, Lorg/icepdf/core/util/ContentParser;->consume_k(Lorg/icepdf/core/pobjects/graphics/GraphicsState;Ljava/util/Stack;Lorg/icepdf/core/util/Library;)V

    goto/16 :goto_1

    .line 1312
    :cond_1d
    const-string/jumbo v2, "g"

    move-object/from16 v0, v31

    invoke-virtual {v0, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1e

    .line 1313
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/icepdf/core/util/ContentParser;->library:Lorg/icepdf/core/util/Library;

    move-object/from16 v0, v40

    invoke-static {v2, v0, v3}, Lorg/icepdf/core/util/ContentParser;->consume_g(Lorg/icepdf/core/pobjects/graphics/GraphicsState;Ljava/util/Stack;Lorg/icepdf/core/util/Library;)V

    goto/16 :goto_1

    .line 1319
    :cond_1e
    const-string/jumbo v2, "i"

    move-object/from16 v0, v31

    invoke-virtual {v0, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1f

    .line 1320
    invoke-static/range {v40 .. v40}, Lorg/icepdf/core/util/ContentParser;->consume_i(Ljava/util/Stack;)V

    goto/16 :goto_1

    .line 1324
    :cond_1f
    const-string/jumbo v2, "M"

    move-object/from16 v0, v31

    invoke-virtual {v0, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_20

    .line 1325
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    move-object/from16 v0, v40

    move-object/from16 v1, p2

    invoke-static {v2, v0, v1}, Lorg/icepdf/core/util/ContentParser;->consume_M(Lorg/icepdf/core/pobjects/graphics/GraphicsState;Ljava/util/Stack;Lorg/icepdf/core/pobjects/graphics/Shapes;)V

    goto/16 :goto_1

    .line 1330
    :cond_20
    const-string/jumbo v2, "J"

    move-object/from16 v0, v31

    invoke-virtual {v0, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_21

    .line 1331
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    move-object/from16 v0, v40

    move-object/from16 v1, p2

    invoke-static {v2, v0, v1}, Lorg/icepdf/core/util/ContentParser;->consume_J(Lorg/icepdf/core/pobjects/graphics/GraphicsState;Ljava/util/Stack;Lorg/icepdf/core/pobjects/graphics/Shapes;)V

    goto/16 :goto_1

    .line 1335
    :cond_21
    const-string/jumbo v2, "rg"

    move-object/from16 v0, v31

    invoke-virtual {v0, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_22

    .line 1336
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/icepdf/core/util/ContentParser;->library:Lorg/icepdf/core/util/Library;

    move-object/from16 v0, v40

    invoke-static {v2, v0, v3}, Lorg/icepdf/core/util/ContentParser;->consume_rg(Lorg/icepdf/core/pobjects/graphics/GraphicsState;Ljava/util/Stack;Lorg/icepdf/core/util/Library;)V

    goto/16 :goto_1

    .line 1343
    :cond_22
    const-string/jumbo v2, "d"

    move-object/from16 v0, v31

    invoke-virtual {v0, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_23

    .line 1344
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    move-object/from16 v0, v40

    move-object/from16 v1, p2

    invoke-static {v2, v0, v1}, Lorg/icepdf/core/util/ContentParser;->consume_d(Lorg/icepdf/core/pobjects/graphics/GraphicsState;Ljava/util/Stack;Lorg/icepdf/core/pobjects/graphics/Shapes;)V

    goto/16 :goto_1

    .line 1351
    :cond_23
    const-string/jumbo v2, "d"

    move-object/from16 v0, v31

    invoke-virtual {v0, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_24

    .line 1352
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    move-object/from16 v0, v40

    move-object/from16 v1, p2

    invoke-static {v2, v0, v1}, Lorg/icepdf/core/util/ContentParser;->consume_d(Lorg/icepdf/core/pobjects/graphics/GraphicsState;Ljava/util/Stack;Lorg/icepdf/core/pobjects/graphics/Shapes;)V

    goto/16 :goto_1

    .line 1356
    :cond_24
    const-string/jumbo v2, "j"

    move-object/from16 v0, v31

    invoke-virtual {v0, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_25

    .line 1357
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    move-object/from16 v0, v40

    move-object/from16 v1, p2

    invoke-static {v2, v0, v1}, Lorg/icepdf/core/util/ContentParser;->consume_j(Lorg/icepdf/core/pobjects/graphics/GraphicsState;Ljava/util/Stack;Lorg/icepdf/core/pobjects/graphics/Shapes;)V

    goto/16 :goto_1

    .line 1361
    :cond_25
    const-string/jumbo v2, "cs"

    move-object/from16 v0, v31

    invoke-virtual {v0, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_26

    .line 1362
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/icepdf/core/util/ContentParser;->resources:Lorg/icepdf/core/pobjects/Resources;

    move-object/from16 v0, v40

    invoke-static {v2, v0, v3}, Lorg/icepdf/core/util/ContentParser;->consume_cs(Lorg/icepdf/core/pobjects/graphics/GraphicsState;Ljava/util/Stack;Lorg/icepdf/core/pobjects/Resources;)V

    goto/16 :goto_1

    .line 1366
    :cond_26
    const-string/jumbo v2, "ri"

    move-object/from16 v0, v31

    invoke-virtual {v0, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_27

    .line 1368
    invoke-virtual/range {v40 .. v40}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    goto/16 :goto_1

    .line 1379
    :cond_27
    const-string/jumbo v2, "SC"

    move-object/from16 v0, v31

    invoke-virtual {v0, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_28

    const-string/jumbo v2, "SCN"

    move-object/from16 v0, v31

    invoke-virtual {v0, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_29

    .line 1381
    :cond_28
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/icepdf/core/util/ContentParser;->library:Lorg/icepdf/core/util/Library;

    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/icepdf/core/util/ContentParser;->resources:Lorg/icepdf/core/pobjects/Resources;

    move-object/from16 v0, v40

    invoke-static {v2, v0, v3, v6}, Lorg/icepdf/core/util/ContentParser;->consume_SC(Lorg/icepdf/core/pobjects/graphics/GraphicsState;Ljava/util/Stack;Lorg/icepdf/core/util/Library;Lorg/icepdf/core/pobjects/Resources;)V

    goto/16 :goto_1

    .line 1390
    :cond_29
    const-string/jumbo v2, "K"

    move-object/from16 v0, v31

    invoke-virtual {v0, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2a

    .line 1391
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/icepdf/core/util/ContentParser;->library:Lorg/icepdf/core/util/Library;

    move-object/from16 v0, v40

    invoke-static {v2, v0, v3}, Lorg/icepdf/core/util/ContentParser;->consume_K(Lorg/icepdf/core/pobjects/graphics/GraphicsState;Ljava/util/Stack;Lorg/icepdf/core/util/Library;)V

    goto/16 :goto_1

    .line 1398
    :cond_2a
    const-string/jumbo v2, "G"

    move-object/from16 v0, v31

    invoke-virtual {v0, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2b

    .line 1399
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/icepdf/core/util/ContentParser;->library:Lorg/icepdf/core/util/Library;

    move-object/from16 v0, v40

    invoke-static {v2, v0, v3}, Lorg/icepdf/core/util/ContentParser;->consume_G(Lorg/icepdf/core/pobjects/graphics/GraphicsState;Ljava/util/Stack;Lorg/icepdf/core/util/Library;)V

    goto/16 :goto_1

    .line 1406
    :cond_2b
    const-string/jumbo v2, "RG"

    move-object/from16 v0, v31

    invoke-virtual {v0, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2c

    .line 1407
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/icepdf/core/util/ContentParser;->library:Lorg/icepdf/core/util/Library;

    move-object/from16 v0, v40

    invoke-static {v2, v0, v3}, Lorg/icepdf/core/util/ContentParser;->consume_RG(Lorg/icepdf/core/pobjects/graphics/GraphicsState;Ljava/util/Stack;Lorg/icepdf/core/util/Library;)V

    goto/16 :goto_1

    .line 1408
    :cond_2c
    const-string/jumbo v2, "CS"

    move-object/from16 v0, v31

    invoke-virtual {v0, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2d

    .line 1409
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/icepdf/core/util/ContentParser;->resources:Lorg/icepdf/core/pobjects/Resources;

    move-object/from16 v0, v40

    invoke-static {v2, v0, v3}, Lorg/icepdf/core/util/ContentParser;->consume_CS(Lorg/icepdf/core/pobjects/graphics/GraphicsState;Ljava/util/Stack;Lorg/icepdf/core/pobjects/Resources;)V

    goto/16 :goto_1

    .line 1413
    :cond_2d
    const-string/jumbo v2, "Tr"

    move-object/from16 v0, v31

    invoke-virtual {v0, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2e

    .line 1415
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    invoke-virtual {v2}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->getTextState()Lorg/icepdf/core/pobjects/graphics/TextState;

    move-result-object v3

    invoke-virtual/range {v40 .. v40}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Number;

    invoke-virtual {v2}, Ljava/lang/Number;->floatValue()F

    move-result v2

    float-to-int v2, v2

    iput v2, v3, Lorg/icepdf/core/pobjects/graphics/TextState;->rmode:I

    goto/16 :goto_1

    .line 1419
    :cond_2e
    const-string/jumbo v2, "Tz"

    move-object/from16 v0, v31

    invoke-virtual {v0, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2f

    .line 1421
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lorg/icepdf/core/util/ContentParser;->inTextBlock:Z

    move-object/from16 v0, v40

    invoke-static {v2, v0, v3}, Lorg/icepdf/core/util/ContentParser;->consume_Tz(Lorg/icepdf/core/pobjects/graphics/GraphicsState;Ljava/util/Stack;Z)V

    goto/16 :goto_1

    .line 1425
    :cond_2f
    const-string/jumbo v2, "Ts"

    move-object/from16 v0, v31

    invoke-virtual {v0, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_30

    .line 1427
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    invoke-virtual {v2}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->getTextState()Lorg/icepdf/core/pobjects/graphics/TextState;

    move-result-object v3

    invoke-virtual/range {v40 .. v40}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Number;

    invoke-virtual {v2}, Ljava/lang/Number;->floatValue()F

    move-result v2

    iput v2, v3, Lorg/icepdf/core/pobjects/graphics/TextState;->trise:F

    goto/16 :goto_1

    .line 1447
    :cond_30
    const-string/jumbo v2, "\'"

    move-object/from16 v0, v31

    invoke-virtual {v0, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_31

    .line 1449
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    move/from16 v0, v39

    neg-float v3, v0

    float-to-double v6, v3

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    invoke-virtual {v3}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->getTextState()Lorg/icepdf/core/pobjects/graphics/TextState;

    move-result-object v3

    iget v3, v3, Lorg/icepdf/core/pobjects/graphics/TextState;->leading:F

    float-to-double v10, v3

    invoke-virtual {v2, v6, v7, v10, v11}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->translate(DD)V

    .line 1455
    const/16 v39, 0x0

    .line 1456
    const/4 v5, 0x0

    .line 1457
    const/4 v2, 0x0

    iput v2, v4, Landroid/graphics/PointF;->x:F

    .line 1458
    const/4 v2, 0x0

    iput v2, v4, Landroid/graphics/PointF;->y:F

    .line 1460
    invoke-virtual/range {v40 .. v40}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v41

    check-cast v41, Lorg/icepdf/core/pobjects/StringObject;

    .line 1462
    .restart local v41    # "stringObject":Lorg/icepdf/core/pobjects/StringObject;
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    invoke-virtual {v2}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->getTextState()Lorg/icepdf/core/pobjects/graphics/TextState;

    move-result-object v43

    .line 1464
    .restart local v43    # "textState":Lorg/icepdf/core/pobjects/graphics/TextState;
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    invoke-static {v2}, Lorg/icepdf/core/util/ContentParser;->applyTextScaling(Lorg/icepdf/core/pobjects/graphics/GraphicsState;)Landroid/graphics/Matrix;

    move-result-object v46

    .line 1466
    .restart local v46    # "tmp":Landroid/graphics/Matrix;
    move-object/from16 v0, v43

    iget-object v2, v0, Lorg/icepdf/core/pobjects/graphics/TextState;->font:Lorg/icepdf/core/pobjects/fonts/Font;

    invoke-virtual {v2}, Lorg/icepdf/core/pobjects/fonts/Font;->getSubTypeFormat()I

    move-result v2

    move-object/from16 v0, v43

    iget-object v3, v0, Lorg/icepdf/core/pobjects/graphics/TextState;->font:Lorg/icepdf/core/pobjects/fonts/Font;

    invoke-virtual {v3}, Lorg/icepdf/core/pobjects/fonts/Font;->getFont()Lorg/icepdf/core/pobjects/fonts/FontFile;

    move-result-object v3

    move-object/from16 v0, v41

    invoke-interface {v0, v2, v3}, Lorg/icepdf/core/pobjects/StringObject;->getLiteralStringBuffer(ILorg/icepdf/core/pobjects/fonts/FontFile;)Ljava/lang/StringBuilder;

    move-result-object v10

    new-instance v11, Landroid/graphics/PointF;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-direct {v11, v2, v3}, Landroid/graphics/PointF;-><init>(FF)V

    const/4 v12, 0x0

    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    invoke-virtual {v2}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->getTextState()Lorg/icepdf/core/pobjects/graphics/TextState;

    move-result-object v13

    move-object/from16 v9, p0

    move-object/from16 v14, p2

    move-object v15, v8

    invoke-direct/range {v9 .. v15}, Lorg/icepdf/core/util/ContentParser;->drawString(Ljava/lang/StringBuilder;Landroid/graphics/PointF;FLorg/icepdf/core/pobjects/graphics/TextState;Lorg/icepdf/core/pobjects/graphics/Shapes;Lorg/icepdf/core/pobjects/graphics/GlyphOutlineClip;)Landroid/graphics/PointF;

    move-result-object v18

    .line 1472
    .restart local v18    # "d":Landroid/graphics/PointF;
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    move-object/from16 v0, v46

    invoke-virtual {v2, v0}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->set(Landroid/graphics/Matrix;)V

    .line 1473
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    move-object/from16 v0, v18

    iget v3, v0, Landroid/graphics/PointF;->x:F

    float-to-double v6, v3

    const-wide/16 v10, 0x0

    invoke-virtual {v2, v6, v7, v10, v11}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->translate(DD)V

    .line 1474
    move-object/from16 v0, v18

    iget v2, v0, Landroid/graphics/PointF;->x:F

    add-float v39, v39, v2

    .line 1475
    goto/16 :goto_1

    .line 1482
    .end local v18    # "d":Landroid/graphics/PointF;
    .end local v41    # "stringObject":Lorg/icepdf/core/pobjects/StringObject;
    .end local v43    # "textState":Lorg/icepdf/core/pobjects/graphics/TextState;
    .end local v46    # "tmp":Landroid/graphics/Matrix;
    :cond_31
    const-string/jumbo v2, "\""

    move-object/from16 v0, v31

    invoke-virtual {v0, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1484
    invoke-virtual/range {v40 .. v40}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v41

    check-cast v41, Lorg/icepdf/core/pobjects/StringObject;

    .line 1485
    .restart local v41    # "stringObject":Lorg/icepdf/core/pobjects/StringObject;
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    invoke-virtual {v2}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->getTextState()Lorg/icepdf/core/pobjects/graphics/TextState;

    move-result-object v3

    invoke-virtual/range {v40 .. v40}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Number;

    invoke-virtual {v2}, Ljava/lang/Number;->floatValue()F

    move-result v2

    iput v2, v3, Lorg/icepdf/core/pobjects/graphics/TextState;->cspace:F

    .line 1486
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    invoke-virtual {v2}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->getTextState()Lorg/icepdf/core/pobjects/graphics/TextState;

    move-result-object v3

    invoke-virtual/range {v40 .. v40}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Number;

    invoke-virtual {v2}, Ljava/lang/Number;->floatValue()F

    move-result v2

    iput v2, v3, Lorg/icepdf/core/pobjects/graphics/TextState;->wspace:F

    .line 1487
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    move/from16 v0, v39

    neg-float v3, v0

    float-to-double v6, v3

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    invoke-virtual {v3}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->getTextState()Lorg/icepdf/core/pobjects/graphics/TextState;

    move-result-object v3

    iget v3, v3, Lorg/icepdf/core/pobjects/graphics/TextState;->leading:F

    float-to-double v10, v3

    invoke-virtual {v2, v6, v7, v10, v11}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->translate(DD)V

    .line 1493
    const/16 v39, 0x0

    .line 1494
    const/4 v5, 0x0

    .line 1496
    const/4 v2, 0x0

    iput v2, v4, Landroid/graphics/PointF;->x:F

    .line 1497
    const/4 v2, 0x0

    iput v2, v4, Landroid/graphics/PointF;->y:F

    .line 1499
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    invoke-virtual {v2}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->getTextState()Lorg/icepdf/core/pobjects/graphics/TextState;

    move-result-object v43

    .line 1501
    .restart local v43    # "textState":Lorg/icepdf/core/pobjects/graphics/TextState;
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    invoke-static {v2}, Lorg/icepdf/core/util/ContentParser;->applyTextScaling(Lorg/icepdf/core/pobjects/graphics/GraphicsState;)Landroid/graphics/Matrix;

    move-result-object v46

    .line 1502
    .restart local v46    # "tmp":Landroid/graphics/Matrix;
    move-object/from16 v0, v43

    iget-object v2, v0, Lorg/icepdf/core/pobjects/graphics/TextState;->font:Lorg/icepdf/core/pobjects/fonts/Font;

    invoke-virtual {v2}, Lorg/icepdf/core/pobjects/fonts/Font;->getSubTypeFormat()I

    move-result v2

    move-object/from16 v0, v43

    iget-object v3, v0, Lorg/icepdf/core/pobjects/graphics/TextState;->font:Lorg/icepdf/core/pobjects/fonts/Font;

    invoke-virtual {v3}, Lorg/icepdf/core/pobjects/fonts/Font;->getFont()Lorg/icepdf/core/pobjects/fonts/FontFile;

    move-result-object v3

    move-object/from16 v0, v41

    invoke-interface {v0, v2, v3}, Lorg/icepdf/core/pobjects/StringObject;->getLiteralStringBuffer(ILorg/icepdf/core/pobjects/fonts/FontFile;)Ljava/lang/StringBuilder;

    move-result-object v10

    new-instance v11, Landroid/graphics/PointF;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-direct {v11, v2, v3}, Landroid/graphics/PointF;-><init>(FF)V

    const/4 v12, 0x0

    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    invoke-virtual {v2}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->getTextState()Lorg/icepdf/core/pobjects/graphics/TextState;

    move-result-object v13

    move-object/from16 v9, p0

    move-object/from16 v14, p2

    move-object v15, v8

    invoke-direct/range {v9 .. v15}, Lorg/icepdf/core/util/ContentParser;->drawString(Ljava/lang/StringBuilder;Landroid/graphics/PointF;FLorg/icepdf/core/pobjects/graphics/TextState;Lorg/icepdf/core/pobjects/graphics/Shapes;Lorg/icepdf/core/pobjects/graphics/GlyphOutlineClip;)Landroid/graphics/PointF;

    move-result-object v18

    .line 1508
    .restart local v18    # "d":Landroid/graphics/PointF;
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    move-object/from16 v0, v46

    invoke-virtual {v2, v0}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->set(Landroid/graphics/Matrix;)V

    .line 1509
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    move-object/from16 v0, v18

    iget v3, v0, Landroid/graphics/PointF;->x:F

    float-to-double v6, v3

    const-wide/16 v10, 0x0

    invoke-virtual {v2, v6, v7, v10, v11}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->translate(DD)V

    .line 1510
    move-object/from16 v0, v18

    iget v2, v0, Landroid/graphics/PointF;->x:F

    add-float v39, v39, v2

    .line 1511
    goto/16 :goto_1

    .line 1515
    .end local v18    # "d":Landroid/graphics/PointF;
    .end local v41    # "stringObject":Lorg/icepdf/core/pobjects/StringObject;
    .end local v43    # "textState":Lorg/icepdf/core/pobjects/graphics/TextState;
    .end local v46    # "tmp":Landroid/graphics/Matrix;
    :cond_32
    move-object/from16 v0, v40

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 1524
    :cond_33
    invoke-virtual {v8}, Lorg/icepdf/core/pobjects/graphics/GlyphOutlineClip;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_34

    .line 1527
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    invoke-virtual {v8}, Lorg/icepdf/core/pobjects/graphics/GlyphOutlineClip;->getGlyphOutlineClip()Landroid/graphics/Path;

    move-result-object v3

    invoke-virtual {v2, v3}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->setClip(Landroid/graphics/Path;)V

    .line 1530
    move-object/from16 v0, p2

    invoke-virtual {v0, v8}, Lorg/icepdf/core/pobjects/graphics/Shapes;->add(Ljava/lang/Object;)V

    .line 1534
    :cond_34
    :goto_5
    invoke-virtual/range {v40 .. v40}, Ljava/util/Stack;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_35

    .line 1535
    invoke-virtual/range {v40 .. v40}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v46

    .line 1536
    .local v46, "tmp":Ljava/lang/String;
    sget-object v2, Lorg/icepdf/core/util/ContentParser;->logger:Ljava/util/logging/Logger;

    sget-object v3, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    invoke-virtual {v2, v3}, Ljava/util/logging/Logger;->isLoggable(Ljava/util/logging/Level;)Z

    move-result v2

    if-eqz v2, :cond_34

    .line 1537
    sget-object v2, Lorg/icepdf/core/util/ContentParser;->logger:Ljava/util/logging/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "Text="

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v46

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/logging/Logger;->warning(Ljava/lang/String;)V

    goto :goto_5

    .line 1540
    .end local v46    # "tmp":Ljava/lang/String;
    :cond_35
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/icepdf/core/util/ContentParser;->textBlockBase:Landroid/graphics/Matrix;

    invoke-virtual {v2, v3}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->set(Landroid/graphics/Matrix;)V

    .line 1541
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lorg/icepdf/core/util/ContentParser;->inTextBlock:Z

    .line 1543
    return v53

    .line 1107
    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x0
        0x0
        0x3f800000    # 1.0f
        0x0
        0x0
    .end array-data
.end method

.method public parseTextBlocks(Ljava/io/InputStream;)Lorg/icepdf/core/pobjects/graphics/Shapes;
    .locals 10
    .param p1, "source"    # Ljava/io/InputStream;

    .prologue
    .line 906
    new-instance v1, Lorg/icepdf/core/util/Parser;

    invoke-direct {v1, p1}, Lorg/icepdf/core/util/Parser;-><init>(Ljava/io/InputStream;)V

    .line 907
    .local v1, "parser":Lorg/icepdf/core/util/Parser;
    new-instance v2, Lorg/icepdf/core/pobjects/graphics/Shapes;

    invoke-direct {v2}, Lorg/icepdf/core/pobjects/graphics/Shapes;-><init>()V

    .line 909
    .local v2, "shapes":Lorg/icepdf/core/pobjects/graphics/Shapes;
    iget-object v5, p0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    if-nez v5, :cond_0

    .line 910
    new-instance v5, Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    invoke-direct {v5, v2}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;-><init>(Lorg/icepdf/core/pobjects/graphics/Shapes;)V

    iput-object v5, p0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    .line 917
    :cond_0
    :try_start_0
    invoke-virtual {v1}, Lorg/icepdf/core/util/Parser;->getStreamObject()Ljava/lang/Object;

    move-result-object v4

    .line 918
    .local v4, "tok":Ljava/lang/Object;
    new-instance v3, Ljava/util/Stack;

    invoke-direct {v3}, Ljava/util/Stack;-><init>()V

    .line 919
    .local v3, "stack":Ljava/util/Stack;, "Ljava/util/Stack<Ljava/lang/Object;>;"
    const-wide/16 v6, 0x0

    .line 920
    .local v6, "yBTstart":D
    :goto_0
    if-eqz v4, :cond_5

    .line 924
    instance-of v5, v4, Ljava/lang/String;

    if-eqz v5, :cond_4

    .line 926
    const-string/jumbo v5, "BT"

    invoke-virtual {v4, v5}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 928
    invoke-virtual {p0, v1, v2, v6, v7}, Lorg/icepdf/core/util/ContentParser;->parseText(Lorg/icepdf/core/util/Parser;Lorg/icepdf/core/pobjects/graphics/Shapes;D)F

    move-result v5

    float-to-double v6, v5

    .line 931
    invoke-virtual {v3}, Ljava/util/Stack;->clear()V

    .line 947
    :cond_1
    :goto_1
    invoke-virtual {v1}, Lorg/icepdf/core/util/Parser;->getStreamObject()Ljava/lang/Object;

    move-result-object v4

    goto :goto_0

    .line 935
    :cond_2
    const-string/jumbo v5, "Tf"

    invoke-virtual {v4, v5}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 936
    iget-object v5, p0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    iget-object v8, p0, Lorg/icepdf/core/util/ContentParser;->resources:Lorg/icepdf/core/pobjects/Resources;

    invoke-static {v5, v3, v8}, Lorg/icepdf/core/util/ContentParser;->consume_Tf(Lorg/icepdf/core/pobjects/graphics/GraphicsState;Ljava/util/Stack;Lorg/icepdf/core/pobjects/Resources;)V

    .line 937
    invoke-virtual {v3}, Ljava/util/Stack;->clear()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 951
    .end local v3    # "stack":Ljava/util/Stack;, "Ljava/util/Stack<Ljava/lang/Object;>;"
    .end local v4    # "tok":Ljava/lang/Object;
    .end local v6    # "yBTstart":D
    :catch_0
    move-exception v0

    .line 953
    .local v0, "e":Ljava/io/IOException;
    sget-object v5, Lorg/icepdf/core/util/ContentParser;->logger:Ljava/util/logging/Logger;

    const-string/jumbo v8, "End of Content Stream"

    invoke-virtual {v5, v8}, Ljava/util/logging/Logger;->finer(Ljava/lang/String;)V

    .line 957
    .end local v0    # "e":Ljava/io/IOException;
    :goto_2
    invoke-virtual {v2}, Lorg/icepdf/core/pobjects/graphics/Shapes;->contract()V

    .line 958
    return-object v2

    .line 940
    .restart local v3    # "stack":Ljava/util/Stack;, "Ljava/util/Stack<Ljava/lang/Object;>;"
    .restart local v4    # "tok":Ljava/lang/Object;
    .restart local v6    # "yBTstart":D
    :cond_3
    :try_start_1
    const-string/jumbo v5, "Do"

    invoke-virtual {v4, v5}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 941
    iget-object v5, p0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    iget-object v8, p0, Lorg/icepdf/core/util/ContentParser;->resources:Lorg/icepdf/core/pobjects/Resources;

    const/4 v9, 0x0

    invoke-static {v5, v3, v2, v8, v9}, Lorg/icepdf/core/util/ContentParser;->consume_Do(Lorg/icepdf/core/pobjects/graphics/GraphicsState;Ljava/util/Stack;Lorg/icepdf/core/pobjects/graphics/Shapes;Lorg/icepdf/core/pobjects/Resources;Z)Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    .line 942
    invoke-virtual {v3}, Ljava/util/Stack;->clear()V

    goto :goto_1

    .line 945
    :cond_4
    invoke-virtual {v3, v4}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 950
    :cond_5
    invoke-virtual {v3}, Ljava/util/Stack;->clear()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2
.end method

.method public setGraphicsState(Lorg/icepdf/core/pobjects/graphics/GraphicsState;)V
    .locals 0
    .param p1, "graphicState"    # Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    .prologue
    .line 144
    iput-object p1, p0, Lorg/icepdf/core/util/ContentParser;->graphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    .line 145
    return-void
.end method
