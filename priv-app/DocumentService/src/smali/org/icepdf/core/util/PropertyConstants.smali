.class public Lorg/icepdf/core/util/PropertyConstants;
.super Ljava/lang/Object;
.source "PropertyConstants.java"


# static fields
.field public static final ANNOTATION_BOUNDS:Ljava/lang/String; = "annotationBounds"

.field public static final ANNOTATION_DELETED:Ljava/lang/String; = "annotationDeleted"

.field public static final ANNOTATION_DESELECTED:Ljava/lang/String; = "annotationDeselected"

.field public static final ANNOTATION_FOCUS_GAINED:Ljava/lang/String; = "annotationFocusGained"

.field public static final ANNOTATION_FOCUS_LOST:Ljava/lang/String; = "annotationFocusLost"

.field public static final ANNOTATION_NEW_LINK:Ljava/lang/String; = "annotationNewLink"

.field public static final ANNOTATION_SELECTED:Ljava/lang/String; = "annotationSelected"

.field public static final DOCUMENT_CURRENT_PAGE:Ljava/lang/String; = "documentCurrentPage"

.field public static final DOCUMENT_INITIALIZING_PAGE:Ljava/lang/String; = "documentPageInitialization"

.field public static final DOCUMENT_PAINTING_PAGE:Ljava/lang/String; = "documentPagePainting"

.field public static final DOCUMENT_TOOL_ANNOTATION_SELECTION:Ljava/lang/String; = "documentToolAnnotationSelect"

.field public static final DOCUMENT_TOOL_NONE:Ljava/lang/String; = "documentToolNone"

.field public static final DOCUMENT_TOOL_PAN:Ljava/lang/String; = "documentToolRotation"

.field public static final DOCUMENT_TOOL_TEXT_SELECTION:Ljava/lang/String; = "documentToolTextSelect"

.field public static final DOCUMENT_TOOL_ZOOM_IN:Ljava/lang/String; = "documentToolZoomIn"

.field public static final DOCUMENT_TOOL_ZOOM_OUT:Ljava/lang/String; = "documentToolZoomOut"

.field public static final TEXT_DESELECTED:Ljava/lang/String; = "textDeselected"

.field public static final TEXT_SELECTED:Ljava/lang/String; = "textSelected"

.field public static final TEXT_SELECT_ALL:Ljava/lang/String; = "textSelectAll"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
