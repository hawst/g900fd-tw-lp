.class public Lorg/icepdf/core/util/ByteCache;
.super Ljava/lang/Object;
.source "ByteCache.java"


# static fields
.field private static fileCachingFallbackSize:I

.field private static fileCachingSize:I

.field private static isCachingEnabled:Z

.field private static final logger:Ljava/util/logging/Logger;


# instance fields
.field private byteArrayInputStream:Ljava/io/ByteArrayInputStream;

.field private byteArrayOutputStream:Ljava/io/ByteArrayOutputStream;

.field private cacheManager:Lorg/icepdf/core/util/CacheManager;

.field private fileInputStream:Ljava/io/FileInputStream;

.field private fileOutputStream:Ljava/io/FileOutputStream;

.field private isCached:Z

.field private length:I

.field private tempFile:Ljava/io/File;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 48
    const-class v0, Lorg/icepdf/core/util/ByteCache;

    invoke-virtual {v0}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Lorg/icepdf/core/util/ByteCache;->logger:Ljava/util/logging/Logger;

    .line 66
    const/high16 v0, 0x80000

    sput v0, Lorg/icepdf/core/util/ByteCache;->fileCachingFallbackSize:I

    .line 88
    const-string/jumbo v0, "org.icepdf.core.streamcache.thresholdSize"

    const v1, 0xf4240

    invoke-static {v0, v1}, Lorg/icepdf/core/util/Defs;->sysPropertyInt(Ljava/lang/String;I)I

    move-result v0

    sput v0, Lorg/icepdf/core/util/ByteCache;->fileCachingSize:I

    .line 92
    sget v0, Lorg/icepdf/core/util/ByteCache;->fileCachingFallbackSize:I

    sget v1, Lorg/icepdf/core/util/ByteCache;->fileCachingSize:I

    if-le v0, v1, :cond_0

    .line 93
    sget v0, Lorg/icepdf/core/util/ByteCache;->fileCachingSize:I

    sput v0, Lorg/icepdf/core/util/ByteCache;->fileCachingFallbackSize:I

    .line 96
    :cond_0
    const-string/jumbo v0, "org.icepdf.core.streamcache.enabled"

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lorg/icepdf/core/util/Defs;->sysPropertyBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lorg/icepdf/core/util/ByteCache;->isCachingEnabled:Z

    .line 99
    return-void
.end method

.method public constructor <init>(ILorg/icepdf/core/util/Library;)V
    .locals 4
    .param p1, "numNewBytes"    # I
    .param p2, "library"    # Lorg/icepdf/core/util/Library;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 133
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    iput v2, p0, Lorg/icepdf/core/util/ByteCache;->length:I

    .line 55
    iput-object v1, p0, Lorg/icepdf/core/util/ByteCache;->tempFile:Ljava/io/File;

    .line 58
    iput-boolean v2, p0, Lorg/icepdf/core/util/ByteCache;->isCached:Z

    .line 72
    iput-object v1, p0, Lorg/icepdf/core/util/ByteCache;->fileOutputStream:Ljava/io/FileOutputStream;

    .line 75
    iput-object v1, p0, Lorg/icepdf/core/util/ByteCache;->fileInputStream:Ljava/io/FileInputStream;

    .line 78
    iput-object v1, p0, Lorg/icepdf/core/util/ByteCache;->byteArrayInputStream:Ljava/io/ByteArrayInputStream;

    .line 81
    iput-object v1, p0, Lorg/icepdf/core/util/ByteCache;->byteArrayOutputStream:Ljava/io/ByteArrayOutputStream;

    .line 84
    iput-object v1, p0, Lorg/icepdf/core/util/ByteCache;->cacheManager:Lorg/icepdf/core/util/CacheManager;

    .line 135
    invoke-virtual {p2}, Lorg/icepdf/core/util/Library;->getCacheManager()Lorg/icepdf/core/util/CacheManager;

    move-result-object v1

    iput-object v1, p0, Lorg/icepdf/core/util/ByteCache;->cacheManager:Lorg/icepdf/core/util/CacheManager;

    .line 137
    if-gtz p1, :cond_0

    .line 147
    :goto_0
    return-void

    .line 139
    :cond_0
    :try_start_0
    invoke-direct {p0, p1}, Lorg/icepdf/core/util/ByteCache;->calcIfFileCachingAndPotentiallyForce(I)V

    .line 141
    invoke-direct {p0, p1}, Lorg/icepdf/core/util/ByteCache;->getCorrectOutputStream(I)Ljava/io/OutputStream;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 143
    :catch_0
    move-exception v0

    .line 144
    .local v0, "e":Ljava/io/IOException;
    sget-object v1, Lorg/icepdf/core/util/ByteCache;->logger:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string/jumbo v3, "Error creating ByteCache temporary file."

    invoke-virtual {v1, v2, v3, v0}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public constructor <init>([BLorg/icepdf/core/util/Library;)V
    .locals 6
    .param p1, "bytes"    # [B
    .param p2, "library"    # Lorg/icepdf/core/util/Library;

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 107
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    iput v1, p0, Lorg/icepdf/core/util/ByteCache;->length:I

    .line 55
    iput-object v3, p0, Lorg/icepdf/core/util/ByteCache;->tempFile:Ljava/io/File;

    .line 58
    iput-boolean v1, p0, Lorg/icepdf/core/util/ByteCache;->isCached:Z

    .line 72
    iput-object v3, p0, Lorg/icepdf/core/util/ByteCache;->fileOutputStream:Ljava/io/FileOutputStream;

    .line 75
    iput-object v3, p0, Lorg/icepdf/core/util/ByteCache;->fileInputStream:Ljava/io/FileInputStream;

    .line 78
    iput-object v3, p0, Lorg/icepdf/core/util/ByteCache;->byteArrayInputStream:Ljava/io/ByteArrayInputStream;

    .line 81
    iput-object v3, p0, Lorg/icepdf/core/util/ByteCache;->byteArrayOutputStream:Ljava/io/ByteArrayOutputStream;

    .line 84
    iput-object v3, p0, Lorg/icepdf/core/util/ByteCache;->cacheManager:Lorg/icepdf/core/util/CacheManager;

    .line 109
    invoke-virtual {p2}, Lorg/icepdf/core/util/Library;->getCacheManager()Lorg/icepdf/core/util/CacheManager;

    move-result-object v3

    iput-object v3, p0, Lorg/icepdf/core/util/ByteCache;->cacheManager:Lorg/icepdf/core/util/CacheManager;

    .line 111
    if-nez p1, :cond_0

    .line 112
    .local v1, "numNewBytes":I
    :goto_0
    if-gtz v1, :cond_1

    .line 122
    .end local v1    # "numNewBytes":I
    :goto_1
    return-void

    .line 111
    :cond_0
    :try_start_0
    array-length v1, p1

    goto :goto_0

    .line 114
    .restart local v1    # "numNewBytes":I
    :cond_1
    invoke-direct {p0, v1}, Lorg/icepdf/core/util/ByteCache;->calcIfFileCachingAndPotentiallyForce(I)V

    .line 115
    invoke-direct {p0, v1}, Lorg/icepdf/core/util/ByteCache;->getCorrectOutputStream(I)Ljava/io/OutputStream;

    move-result-object v2

    .line 116
    .local v2, "out":Ljava/io/OutputStream;
    invoke-virtual {v2, p1}, Ljava/io/OutputStream;->write([B)V

    .line 117
    iput v1, p0, Lorg/icepdf/core/util/ByteCache;->length:I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 118
    .end local v1    # "numNewBytes":I
    .end local v2    # "out":Ljava/io/OutputStream;
    :catch_0
    move-exception v0

    .line 119
    .local v0, "e":Ljava/io/IOException;
    sget-object v3, Lorg/icepdf/core/util/ByteCache;->logger:Ljava/util/logging/Logger;

    sget-object v4, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string/jumbo v5, "Error creating ByteCache temporary file."

    invoke-virtual {v3, v4, v5, v0}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method private calcIfFileCachingAndPotentiallyForce(I)V
    .locals 2
    .param p1, "numNewBytes"    # I

    .prologue
    .line 533
    iget-boolean v0, p0, Lorg/icepdf/core/util/ByteCache;->isCached:Z

    if-nez v0, :cond_0

    iget v0, p0, Lorg/icepdf/core/util/ByteCache;->length:I

    add-int/2addr v0, p1

    sget v1, Lorg/icepdf/core/util/ByteCache;->fileCachingSize:I

    if-le v0, v1, :cond_0

    .line 534
    invoke-virtual {p0}, Lorg/icepdf/core/util/ByteCache;->forceByteCaching()V

    .line 535
    :cond_0
    return-void
.end method

.method private createTempFile()V
    .locals 4

    .prologue
    .line 517
    :try_start_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "PDFByteStream"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, ".tmp"

    invoke-static {v1, v2}, Ljava/io/File;->createTempFile(Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    iput-object v1, p0, Lorg/icepdf/core/util/ByteCache;->tempFile:Ljava/io/File;

    .line 522
    iget-object v1, p0, Lorg/icepdf/core/util/ByteCache;->tempFile:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->deleteOnExit()V

    .line 525
    iget-object v1, p0, Lorg/icepdf/core/util/ByteCache;->cacheManager:Lorg/icepdf/core/util/CacheManager;

    iget-object v2, p0, Lorg/icepdf/core/util/ByteCache;->tempFile:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lorg/icepdf/core/util/CacheManager;->addCachedFile(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 530
    :goto_0
    return-void

    .line 527
    :catch_0
    move-exception v0

    .line 528
    .local v0, "e":Ljava/io/IOException;
    sget-object v1, Lorg/icepdf/core/util/ByteCache;->logger:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string/jumbo v3, "Error creating byte cache tmp file"

    invoke-virtual {v1, v2, v3}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private getCorrectInputStream()Ljava/io/InputStream;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 560
    iget-boolean v0, p0, Lorg/icepdf/core/util/ByteCache;->isCached:Z

    if-eqz v0, :cond_1

    .line 561
    iget-object v0, p0, Lorg/icepdf/core/util/ByteCache;->tempFile:Ljava/io/File;

    if-eqz v0, :cond_3

    .line 563
    iget-object v0, p0, Lorg/icepdf/core/util/ByteCache;->fileInputStream:Ljava/io/FileInputStream;

    if-nez v0, :cond_0

    .line 564
    new-instance v0, Ljava/io/FileInputStream;

    iget-object v1, p0, Lorg/icepdf/core/util/ByteCache;->tempFile:Ljava/io/File;

    invoke-direct {v0, v1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    iput-object v0, p0, Lorg/icepdf/core/util/ByteCache;->fileInputStream:Ljava/io/FileInputStream;

    .line 566
    :cond_0
    iget-object v0, p0, Lorg/icepdf/core/util/ByteCache;->fileInputStream:Ljava/io/FileInputStream;

    .line 578
    :goto_0
    return-object v0

    .line 569
    :cond_1
    iget-object v0, p0, Lorg/icepdf/core/util/ByteCache;->byteArrayOutputStream:Ljava/io/ByteArrayOutputStream;

    if-eqz v0, :cond_3

    .line 570
    iget-object v0, p0, Lorg/icepdf/core/util/ByteCache;->byteArrayInputStream:Ljava/io/ByteArrayInputStream;

    if-nez v0, :cond_2

    .line 571
    new-instance v0, Ljava/io/ByteArrayInputStream;

    iget-object v1, p0, Lorg/icepdf/core/util/ByteCache;->byteArrayOutputStream:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    iput-object v0, p0, Lorg/icepdf/core/util/ByteCache;->byteArrayInputStream:Ljava/io/ByteArrayInputStream;

    .line 575
    :cond_2
    iget-object v0, p0, Lorg/icepdf/core/util/ByteCache;->byteArrayInputStream:Ljava/io/ByteArrayInputStream;

    goto :goto_0

    .line 578
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getCorrectOutputStream(I)Ljava/io/OutputStream;
    .locals 3
    .param p1, "optionalPresizing"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 538
    iget-boolean v0, p0, Lorg/icepdf/core/util/ByteCache;->isCached:Z

    if-eqz v0, :cond_2

    .line 540
    iget-object v0, p0, Lorg/icepdf/core/util/ByteCache;->tempFile:Ljava/io/File;

    if-nez v0, :cond_0

    .line 541
    invoke-direct {p0}, Lorg/icepdf/core/util/ByteCache;->createTempFile()V

    .line 544
    :cond_0
    iget-object v0, p0, Lorg/icepdf/core/util/ByteCache;->fileOutputStream:Ljava/io/FileOutputStream;

    if-nez v0, :cond_1

    iget-object v0, p0, Lorg/icepdf/core/util/ByteCache;->tempFile:Ljava/io/File;

    if-eqz v0, :cond_1

    .line 545
    new-instance v0, Ljava/io/FileOutputStream;

    iget-object v1, p0, Lorg/icepdf/core/util/ByteCache;->tempFile:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;Z)V

    iput-object v0, p0, Lorg/icepdf/core/util/ByteCache;->fileOutputStream:Ljava/io/FileOutputStream;

    .line 548
    :cond_1
    iget-object v0, p0, Lorg/icepdf/core/util/ByteCache;->fileOutputStream:Ljava/io/FileOutputStream;

    .line 554
    :goto_0
    return-object v0

    .line 551
    :cond_2
    iget-object v0, p0, Lorg/icepdf/core/util/ByteCache;->byteArrayOutputStream:Ljava/io/ByteArrayOutputStream;

    if-nez v0, :cond_3

    .line 552
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0, p1}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    iput-object v0, p0, Lorg/icepdf/core/util/ByteCache;->byteArrayOutputStream:Ljava/io/ByteArrayOutputStream;

    .line 554
    :cond_3
    iget-object v0, p0, Lorg/icepdf/core/util/ByteCache;->byteArrayOutputStream:Ljava/io/ByteArrayOutputStream;

    goto :goto_0
.end method


# virtual methods
.method public close()V
    .locals 4

    .prologue
    .line 448
    :try_start_0
    iget-object v1, p0, Lorg/icepdf/core/util/ByteCache;->fileOutputStream:Ljava/io/FileOutputStream;

    if-eqz v1, :cond_0

    .line 449
    iget-object v1, p0, Lorg/icepdf/core/util/ByteCache;->fileOutputStream:Ljava/io/FileOutputStream;

    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V

    .line 451
    :cond_0
    iget-object v1, p0, Lorg/icepdf/core/util/ByteCache;->fileInputStream:Ljava/io/FileInputStream;

    if-eqz v1, :cond_1

    .line 452
    iget-object v1, p0, Lorg/icepdf/core/util/ByteCache;->fileInputStream:Ljava/io/FileInputStream;

    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V

    .line 454
    :cond_1
    iget-object v1, p0, Lorg/icepdf/core/util/ByteCache;->byteArrayInputStream:Ljava/io/ByteArrayInputStream;

    if-eqz v1, :cond_2

    .line 455
    iget-object v1, p0, Lorg/icepdf/core/util/ByteCache;->byteArrayInputStream:Ljava/io/ByteArrayInputStream;

    invoke-virtual {v1}, Ljava/io/ByteArrayInputStream;->close()V

    .line 457
    :cond_2
    iget-object v1, p0, Lorg/icepdf/core/util/ByteCache;->byteArrayOutputStream:Ljava/io/ByteArrayOutputStream;

    if-eqz v1, :cond_3

    .line 458
    iget-object v1, p0, Lorg/icepdf/core/util/ByteCache;->byteArrayOutputStream:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 463
    :cond_3
    :goto_0
    return-void

    .line 460
    :catch_0
    move-exception v0

    .line 461
    .local v0, "e":Ljava/io/IOException;
    sget-object v1, Lorg/icepdf/core/util/ByteCache;->logger:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string/jumbo v3, "Error closing file streams."

    invoke-virtual {v1, v2, v3, v0}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public deleteFileCache()V
    .locals 1

    .prologue
    .line 437
    iget-object v0, p0, Lorg/icepdf/core/util/ByteCache;->tempFile:Ljava/io/File;

    if-eqz v0, :cond_0

    .line 438
    iget-object v0, p0, Lorg/icepdf/core/util/ByteCache;->tempFile:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 440
    :cond_0
    return-void
.end method

.method public dispose(Z)V
    .locals 4
    .param p1, "cache"    # Z

    .prologue
    .line 353
    :try_start_0
    iget-boolean v1, p0, Lorg/icepdf/core/util/ByteCache;->isCached:Z

    if-nez v1, :cond_0

    if-eqz p1, :cond_0

    iget v1, p0, Lorg/icepdf/core/util/ByteCache;->length:I

    sget v2, Lorg/icepdf/core/util/ByteCache;->fileCachingFallbackSize:I

    if-le v1, v2, :cond_0

    .line 354
    invoke-virtual {p0}, Lorg/icepdf/core/util/ByteCache;->forceByteCaching()V

    .line 357
    :cond_0
    iget-object v1, p0, Lorg/icepdf/core/util/ByteCache;->fileOutputStream:Ljava/io/FileOutputStream;

    if-eqz v1, :cond_1

    .line 358
    iget-object v1, p0, Lorg/icepdf/core/util/ByteCache;->fileOutputStream:Ljava/io/FileOutputStream;

    invoke-virtual {v1}, Ljava/io/FileOutputStream;->flush()V

    .line 359
    iget-object v1, p0, Lorg/icepdf/core/util/ByteCache;->fileOutputStream:Ljava/io/FileOutputStream;

    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V

    .line 360
    const/4 v1, 0x0

    iput-object v1, p0, Lorg/icepdf/core/util/ByteCache;->fileOutputStream:Ljava/io/FileOutputStream;

    .line 362
    :cond_1
    iget-object v1, p0, Lorg/icepdf/core/util/ByteCache;->fileInputStream:Ljava/io/FileInputStream;

    if-eqz v1, :cond_2

    .line 363
    iget-object v1, p0, Lorg/icepdf/core/util/ByteCache;->fileInputStream:Ljava/io/FileInputStream;

    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V

    .line 364
    const/4 v1, 0x0

    iput-object v1, p0, Lorg/icepdf/core/util/ByteCache;->fileInputStream:Ljava/io/FileInputStream;

    .line 366
    :cond_2
    iget-object v1, p0, Lorg/icepdf/core/util/ByteCache;->byteArrayInputStream:Ljava/io/ByteArrayInputStream;

    if-eqz v1, :cond_3

    .line 367
    iget-object v1, p0, Lorg/icepdf/core/util/ByteCache;->byteArrayInputStream:Ljava/io/ByteArrayInputStream;

    invoke-virtual {v1}, Ljava/io/ByteArrayInputStream;->close()V

    .line 368
    const/4 v1, 0x0

    iput-object v1, p0, Lorg/icepdf/core/util/ByteCache;->byteArrayInputStream:Ljava/io/ByteArrayInputStream;

    .line 370
    :cond_3
    iget-object v1, p0, Lorg/icepdf/core/util/ByteCache;->byteArrayOutputStream:Ljava/io/ByteArrayOutputStream;

    if-eqz v1, :cond_5

    .line 371
    iget-object v1, p0, Lorg/icepdf/core/util/ByteCache;->byteArrayOutputStream:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->flush()V

    .line 372
    iget-object v1, p0, Lorg/icepdf/core/util/ByteCache;->byteArrayOutputStream:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->close()V

    .line 375
    iget-boolean v1, p0, Lorg/icepdf/core/util/ByteCache;->isCached:Z

    if-nez v1, :cond_4

    if-nez p1, :cond_5

    .line 376
    :cond_4
    const/4 v1, 0x0

    iput-object v1, p0, Lorg/icepdf/core/util/ByteCache;->byteArrayOutputStream:Ljava/io/ByteArrayOutputStream;

    .line 380
    :cond_5
    if-nez p1, :cond_6

    .line 381
    invoke-virtual {p0}, Lorg/icepdf/core/util/ByteCache;->deleteFileCache()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 386
    :cond_6
    :goto_0
    return-void

    .line 383
    :catch_0
    move-exception v0

    .line 384
    .local v0, "e":Ljava/io/IOException;
    sget-object v1, Lorg/icepdf/core/util/ByteCache;->logger:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string/jumbo v3, "Error closing file streams "

    invoke-virtual {v1, v2, v3, v0}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public forceByteCaching()V
    .locals 4

    .prologue
    .line 320
    sget-boolean v1, Lorg/icepdf/core/util/ByteCache;->isCachingEnabled:Z

    if-eqz v1, :cond_3

    iget-boolean v1, p0, Lorg/icepdf/core/util/ByteCache;->isCached:Z

    if-nez v1, :cond_3

    .line 323
    :try_start_0
    iget-object v1, p0, Lorg/icepdf/core/util/ByteCache;->tempFile:Ljava/io/File;

    if-nez v1, :cond_0

    .line 324
    invoke-direct {p0}, Lorg/icepdf/core/util/ByteCache;->createTempFile()V

    .line 327
    :cond_0
    iget-object v1, p0, Lorg/icepdf/core/util/ByteCache;->fileOutputStream:Ljava/io/FileOutputStream;

    if-nez v1, :cond_1

    .line 328
    new-instance v1, Ljava/io/FileOutputStream;

    iget-object v2, p0, Lorg/icepdf/core/util/ByteCache;->tempFile:Ljava/io/File;

    invoke-direct {v1, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    iput-object v1, p0, Lorg/icepdf/core/util/ByteCache;->fileOutputStream:Ljava/io/FileOutputStream;

    .line 331
    :cond_1
    iget-object v1, p0, Lorg/icepdf/core/util/ByteCache;->byteArrayOutputStream:Ljava/io/ByteArrayOutputStream;

    if-eqz v1, :cond_2

    .line 332
    iget-object v1, p0, Lorg/icepdf/core/util/ByteCache;->fileOutputStream:Ljava/io/FileOutputStream;

    iget-object v2, p0, Lorg/icepdf/core/util/ByteCache;->byteArrayOutputStream:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/FileOutputStream;->write([B)V

    .line 334
    :cond_2
    const/4 v1, 0x1

    iput-boolean v1, p0, Lorg/icepdf/core/util/ByteCache;->isCached:Z

    .line 337
    const/4 v1, 0x0

    iput-object v1, p0, Lorg/icepdf/core/util/ByteCache;->byteArrayOutputStream:Ljava/io/ByteArrayOutputStream;

    .line 338
    const/4 v1, 0x0

    iput-object v1, p0, Lorg/icepdf/core/util/ByteCache;->byteArrayInputStream:Ljava/io/ByteArrayInputStream;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 343
    :cond_3
    :goto_0
    return-void

    .line 339
    :catch_0
    move-exception v0

    .line 340
    .local v0, "e":Ljava/io/IOException;
    sget-object v1, Lorg/icepdf/core/util/ByteCache;->logger:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string/jumbo v3, "Error creating the temp file."

    invoke-virtual {v1, v2, v3, v0}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public getLength()I
    .locals 1

    .prologue
    .line 501
    iget v0, p0, Lorg/icepdf/core/util/ByteCache;->length:I

    return v0
.end method

.method public inMemory()Z
    .locals 1

    .prologue
    .line 508
    iget-object v0, p0, Lorg/icepdf/core/util/ByteCache;->byteArrayOutputStream:Ljava/io/ByteArrayOutputStream;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isCached()Z
    .locals 1

    .prologue
    .line 492
    iget-boolean v0, p0, Lorg/icepdf/core/util/ByteCache;->isCached:Z

    return v0
.end method

.method public readByte()I
    .locals 6

    .prologue
    .line 301
    const/4 v2, -0x1

    .line 304
    .local v2, "returnValue":I
    :try_start_0
    invoke-direct {p0}, Lorg/icepdf/core/util/ByteCache;->getCorrectInputStream()Ljava/io/InputStream;

    move-result-object v1

    .line 305
    .local v1, "in":Ljava/io/InputStream;
    if-eqz v1, :cond_0

    .line 306
    invoke-virtual {v1}, Ljava/io/InputStream;->read()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 311
    .end local v1    # "in":Ljava/io/InputStream;
    :cond_0
    :goto_0
    return v2

    .line 308
    :catch_0
    move-exception v0

    .line 309
    .local v0, "e":Ljava/io/IOException;
    sget-object v3, Lorg/icepdf/core/util/ByteCache;->logger:Ljava/util/logging/Logger;

    sget-object v4, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string/jumbo v5, "Error reading from temporary file "

    invoke-virtual {v3, v4, v5, v0}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public readBytes([B)I
    .locals 6
    .param p1, "bytes"    # [B

    .prologue
    .line 280
    const/4 v2, -0x1

    .line 283
    .local v2, "returnValue":I
    :try_start_0
    invoke-direct {p0}, Lorg/icepdf/core/util/ByteCache;->getCorrectInputStream()Ljava/io/InputStream;

    move-result-object v1

    .line 284
    .local v1, "in":Ljava/io/InputStream;
    if-eqz v1, :cond_0

    .line 285
    invoke-virtual {v1, p1}, Ljava/io/InputStream;->read([B)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 290
    .end local v1    # "in":Ljava/io/InputStream;
    :cond_0
    :goto_0
    return v2

    .line 287
    :catch_0
    move-exception v0

    .line 288
    .local v0, "e":Ljava/io/IOException;
    sget-object v3, Lorg/icepdf/core/util/ByteCache;->logger:Ljava/util/logging/Logger;

    sget-object v4, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string/jumbo v5, "Error reading from temporary file "

    invoke-virtual {v3, v4, v5, v0}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public readBytes([BII)I
    .locals 6
    .param p1, "bytes"    # [B
    .param p2, "offset"    # I
    .param p3, "length"    # I

    .prologue
    .line 257
    const/4 v2, -0x1

    .line 259
    .local v2, "returnValue":I
    :try_start_0
    invoke-direct {p0}, Lorg/icepdf/core/util/ByteCache;->getCorrectInputStream()Ljava/io/InputStream;

    move-result-object v1

    .line 260
    .local v1, "in":Ljava/io/InputStream;
    if-eqz v1, :cond_0

    .line 261
    invoke-virtual {v1, p1, p2, p3}, Ljava/io/InputStream;->read([BII)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 266
    .end local v1    # "in":Ljava/io/InputStream;
    :cond_0
    :goto_0
    return v2

    .line 263
    :catch_0
    move-exception v0

    .line 264
    .local v0, "e":Ljava/io/IOException;
    sget-object v3, Lorg/icepdf/core/util/ByteCache;->logger:Ljava/util/logging/Logger;

    sget-object v4, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string/jumbo v5, "Error reading from temporary file "

    invoke-virtual {v3, v4, v5, v0}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public reset()V
    .locals 4

    .prologue
    .line 472
    :try_start_0
    iget-object v1, p0, Lorg/icepdf/core/util/ByteCache;->byteArrayInputStream:Ljava/io/ByteArrayInputStream;

    if-eqz v1, :cond_0

    .line 473
    iget-object v1, p0, Lorg/icepdf/core/util/ByteCache;->byteArrayInputStream:Ljava/io/ByteArrayInputStream;

    invoke-virtual {v1}, Ljava/io/ByteArrayInputStream;->reset()V

    .line 476
    :cond_0
    iget-object v1, p0, Lorg/icepdf/core/util/ByteCache;->fileInputStream:Ljava/io/FileInputStream;

    if-eqz v1, :cond_1

    .line 477
    iget-object v1, p0, Lorg/icepdf/core/util/ByteCache;->fileInputStream:Ljava/io/FileInputStream;

    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V

    .line 478
    const/4 v1, 0x0

    iput-object v1, p0, Lorg/icepdf/core/util/ByteCache;->fileInputStream:Ljava/io/FileInputStream;

    .line 479
    new-instance v1, Ljava/io/FileInputStream;

    iget-object v2, p0, Lorg/icepdf/core/util/ByteCache;->tempFile:Ljava/io/File;

    invoke-direct {v1, v2}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    iput-object v1, p0, Lorg/icepdf/core/util/ByteCache;->fileInputStream:Ljava/io/FileInputStream;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 484
    :cond_1
    :goto_0
    return-void

    .line 481
    :catch_0
    move-exception v0

    .line 482
    .local v0, "e":Ljava/io/IOException;
    sget-object v1, Lorg/icepdf/core/util/ByteCache;->logger:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string/jumbo v3, "Error closing file streams."

    invoke-virtual {v1, v2, v3, v0}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public toByteArray()[B
    .locals 8

    .prologue
    .line 396
    const/4 v3, 0x0

    .line 398
    .local v3, "returnValue":[B
    const/4 v1, 0x0

    .line 401
    .local v1, "fileInputStream":Ljava/io/FileInputStream;
    :try_start_0
    iget-boolean v4, p0, Lorg/icepdf/core/util/ByteCache;->isCached:Z

    if-eqz v4, :cond_2

    .line 402
    iget-object v4, p0, Lorg/icepdf/core/util/ByteCache;->tempFile:Ljava/io/File;

    if-eqz v4, :cond_0

    .line 405
    new-instance v2, Ljava/io/FileInputStream;

    iget-object v4, p0, Lorg/icepdf/core/util/ByteCache;->tempFile:Ljava/io/File;

    invoke-direct {v2, v4}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 406
    .end local v1    # "fileInputStream":Ljava/io/FileInputStream;
    .local v2, "fileInputStream":Ljava/io/FileInputStream;
    :try_start_1
    iget v4, p0, Lorg/icepdf/core/util/ByteCache;->length:I

    new-array v3, v4, [B

    .line 408
    invoke-virtual {v2, v3}, Ljava/io/FileInputStream;->read([B)I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-object v1, v2

    .line 423
    .end local v2    # "fileInputStream":Ljava/io/FileInputStream;
    .restart local v1    # "fileInputStream":Ljava/io/FileInputStream;
    :cond_0
    :goto_0
    if-eqz v1, :cond_1

    .line 425
    :try_start_2
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 430
    :cond_1
    :goto_1
    return-object v3

    .line 414
    :cond_2
    :try_start_3
    iget-object v4, p0, Lorg/icepdf/core/util/ByteCache;->byteArrayOutputStream:Ljava/io/ByteArrayOutputStream;

    if-eqz v4, :cond_0

    .line 415
    iget-object v4, p0, Lorg/icepdf/core/util/ByteCache;->byteArrayOutputStream:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v4}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v3

    goto :goto_0

    .line 426
    :catch_0
    move-exception v0

    .line 427
    .local v0, "e":Ljava/io/IOException;
    const-string/jumbo v4, "DocumentService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "Exception: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 418
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 419
    .restart local v0    # "e":Ljava/io/IOException;
    :goto_2
    :try_start_4
    sget-object v4, Lorg/icepdf/core/util/ByteCache;->logger:Ljava/util/logging/Logger;

    sget-object v5, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string/jumbo v6, "Error reading from temporary file."

    invoke-virtual {v4, v5, v6, v0}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 423
    if-eqz v1, :cond_1

    .line 425
    :try_start_5
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    goto :goto_1

    .line 426
    :catch_2
    move-exception v0

    .line 427
    const-string/jumbo v4, "DocumentService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "Exception: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 423
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v4

    :goto_3
    if-eqz v1, :cond_3

    .line 425
    :try_start_6
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    .line 428
    :cond_3
    :goto_4
    throw v4

    .line 426
    :catch_3
    move-exception v0

    .line 427
    .restart local v0    # "e":Ljava/io/IOException;
    const-string/jumbo v5, "DocumentService"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "Exception: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4

    .line 423
    .end local v0    # "e":Ljava/io/IOException;
    .end local v1    # "fileInputStream":Ljava/io/FileInputStream;
    .restart local v2    # "fileInputStream":Ljava/io/FileInputStream;
    :catchall_1
    move-exception v4

    move-object v1, v2

    .end local v2    # "fileInputStream":Ljava/io/FileInputStream;
    .restart local v1    # "fileInputStream":Ljava/io/FileInputStream;
    goto :goto_3

    .line 418
    .end local v1    # "fileInputStream":Ljava/io/FileInputStream;
    .restart local v2    # "fileInputStream":Ljava/io/FileInputStream;
    :catch_4
    move-exception v0

    move-object v1, v2

    .end local v2    # "fileInputStream":Ljava/io/FileInputStream;
    .restart local v1    # "fileInputStream":Ljava/io/FileInputStream;
    goto :goto_2
.end method

.method public writeBytes(I)V
    .locals 6
    .param p1, "bytes"    # I

    .prologue
    .line 232
    const/4 v1, 0x1

    .line 233
    .local v1, "numNewBytes":I
    :try_start_0
    invoke-direct {p0, v1}, Lorg/icepdf/core/util/ByteCache;->calcIfFileCachingAndPotentiallyForce(I)V

    .line 234
    const/16 v3, 0x100

    invoke-direct {p0, v3}, Lorg/icepdf/core/util/ByteCache;->getCorrectOutputStream(I)Ljava/io/OutputStream;

    move-result-object v2

    .line 235
    .local v2, "out":Ljava/io/OutputStream;
    invoke-virtual {v2, p1}, Ljava/io/OutputStream;->write(I)V

    .line 236
    iget v3, p0, Lorg/icepdf/core/util/ByteCache;->length:I

    add-int/2addr v3, v1

    iput v3, p0, Lorg/icepdf/core/util/ByteCache;->length:I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 241
    .end local v2    # "out":Ljava/io/OutputStream;
    :goto_0
    return-void

    .line 238
    :catch_0
    move-exception v0

    .line 239
    .local v0, "e":Ljava/io/IOException;
    sget-object v3, Lorg/icepdf/core/util/ByteCache;->logger:Ljava/util/logging/Logger;

    sget-object v4, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string/jumbo v5, "Error writing to temporary file "

    invoke-virtual {v3, v4, v5, v0}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public writeBytes(Ljava/io/InputStream;I)V
    .locals 9
    .param p1, "in"    # Ljava/io/InputStream;
    .param p2, "numNewBytes"    # I

    .prologue
    .line 158
    if-gtz p2, :cond_0

    .line 179
    :goto_0
    return-void

    .line 160
    :cond_0
    :try_start_0
    invoke-direct {p0, p2}, Lorg/icepdf/core/util/ByteCache;->calcIfFileCachingAndPotentiallyForce(I)V

    .line 161
    invoke-direct {p0, p2}, Lorg/icepdf/core/util/ByteCache;->getCorrectOutputStream(I)Ljava/io/OutputStream;

    move-result-object v4

    .line 163
    .local v4, "out":Ljava/io/OutputStream;
    const/16 v6, 0x1000

    invoke-static {p2, v6}, Ljava/lang/Math;->min(II)I

    move-result v6

    new-array v0, v6, [B

    .line 164
    .local v0, "buffer":[B
    const/4 v5, 0x0

    .line 165
    .local v5, "totalRead":I
    :goto_1
    if-ge v5, p2, :cond_1

    .line 166
    array-length v6, v0

    sub-int v7, p2, v5

    invoke-static {v6, v7}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 167
    .local v1, "currNumToRead":I
    const/4 v6, 0x0

    invoke-virtual {p1, v0, v6, v1}, Ljava/io/InputStream;->read([BII)I

    move-result v2

    .line 168
    .local v2, "currRead":I
    if-gtz v2, :cond_2

    .line 174
    .end local v1    # "currNumToRead":I
    .end local v2    # "currRead":I
    :cond_1
    iget v6, p0, Lorg/icepdf/core/util/ByteCache;->length:I

    add-int/2addr v6, v5

    iput v6, p0, Lorg/icepdf/core/util/ByteCache;->length:I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 176
    .end local v0    # "buffer":[B
    .end local v4    # "out":Ljava/io/OutputStream;
    .end local v5    # "totalRead":I
    :catch_0
    move-exception v3

    .line 177
    .local v3, "e":Ljava/io/IOException;
    sget-object v6, Lorg/icepdf/core/util/ByteCache;->logger:Ljava/util/logging/Logger;

    sget-object v7, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string/jumbo v8, "Error writing to temporary file "

    invoke-virtual {v6, v7, v8, v3}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 170
    .end local v3    # "e":Ljava/io/IOException;
    .restart local v0    # "buffer":[B
    .restart local v1    # "currNumToRead":I
    .restart local v2    # "currRead":I
    .restart local v4    # "out":Ljava/io/OutputStream;
    .restart local v5    # "totalRead":I
    :cond_2
    const/4 v6, 0x0

    :try_start_1
    invoke-virtual {v4, v0, v6, v2}, Ljava/io/OutputStream;->write([BII)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 171
    add-int/2addr v5, v2

    .line 172
    goto :goto_1
.end method

.method public writeBytes([B)V
    .locals 6
    .param p1, "bytes"    # [B

    .prologue
    .line 212
    if-nez p1, :cond_0

    const/4 v1, 0x0

    .line 213
    .local v1, "numNewBytes":I
    :goto_0
    if-gtz v1, :cond_1

    .line 223
    .end local v1    # "numNewBytes":I
    :goto_1
    return-void

    .line 212
    :cond_0
    :try_start_0
    array-length v1, p1

    goto :goto_0

    .line 215
    .restart local v1    # "numNewBytes":I
    :cond_1
    invoke-direct {p0, v1}, Lorg/icepdf/core/util/ByteCache;->calcIfFileCachingAndPotentiallyForce(I)V

    .line 216
    const/16 v3, 0x100

    invoke-static {v1, v3}, Ljava/lang/Math;->max(II)I

    move-result v3

    invoke-direct {p0, v3}, Lorg/icepdf/core/util/ByteCache;->getCorrectOutputStream(I)Ljava/io/OutputStream;

    move-result-object v2

    .line 217
    .local v2, "out":Ljava/io/OutputStream;
    invoke-virtual {v2, p1}, Ljava/io/OutputStream;->write([B)V

    .line 218
    iget v3, p0, Lorg/icepdf/core/util/ByteCache;->length:I

    add-int/2addr v3, v1

    iput v3, p0, Lorg/icepdf/core/util/ByteCache;->length:I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 220
    .end local v1    # "numNewBytes":I
    .end local v2    # "out":Ljava/io/OutputStream;
    :catch_0
    move-exception v0

    .line 221
    .local v0, "e":Ljava/io/IOException;
    sget-object v3, Lorg/icepdf/core/util/ByteCache;->logger:Ljava/util/logging/Logger;

    sget-object v4, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string/jumbo v5, "Error writing to temporary file "

    invoke-virtual {v3, v4, v5, v0}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method public writeBytes([BII)V
    .locals 5
    .param p1, "bytes"    # [B
    .param p2, "offset"    # I
    .param p3, "numNewBytes"    # I

    .prologue
    .line 192
    if-gtz p3, :cond_0

    .line 202
    :goto_0
    return-void

    .line 194
    :cond_0
    :try_start_0
    invoke-direct {p0, p3}, Lorg/icepdf/core/util/ByteCache;->calcIfFileCachingAndPotentiallyForce(I)V

    .line 195
    const/16 v2, 0x100

    invoke-static {p3, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    invoke-direct {p0, v2}, Lorg/icepdf/core/util/ByteCache;->getCorrectOutputStream(I)Ljava/io/OutputStream;

    move-result-object v1

    .line 196
    .local v1, "out":Ljava/io/OutputStream;
    invoke-virtual {v1, p1, p2, p3}, Ljava/io/OutputStream;->write([BII)V

    .line 197
    iget v2, p0, Lorg/icepdf/core/util/ByteCache;->length:I

    add-int/2addr v2, p3

    iput v2, p0, Lorg/icepdf/core/util/ByteCache;->length:I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 199
    .end local v1    # "out":Ljava/io/OutputStream;
    :catch_0
    move-exception v0

    .line 200
    .local v0, "e":Ljava/io/IOException;
    sget-object v2, Lorg/icepdf/core/util/ByteCache;->logger:Ljava/util/logging/Logger;

    sget-object v3, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string/jumbo v4, "Error writing to temporary file."

    invoke-virtual {v2, v3, v4, v0}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
