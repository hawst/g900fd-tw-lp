.class public Lorg/icepdf/core/util/Defs;
.super Ljava/lang/Object;
.source "Defs.java"


# static fields
.field private static final logger:Ljava/util/logging/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    const-class v0, Lorg/icepdf/core/util/Defs;

    invoke-virtual {v0}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Lorg/icepdf/core/util/Defs;->logger:Ljava/util/logging/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static booleanProperty(Ljava/lang/String;)Z
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 93
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lorg/icepdf/core/util/Defs;->booleanProperty(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static booleanProperty(Ljava/lang/String;Z)Z
    .locals 4
    .param p0, "name"    # Ljava/lang/String;
    .param p1, "defaultValue"    # Z

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 103
    invoke-static {p0}, Lorg/icepdf/core/util/Defs;->property(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 104
    .local v0, "value":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 105
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 120
    .end local p1    # "defaultValue":Z
    :cond_0
    :goto_0
    return p1

    .line 107
    .restart local p1    # "defaultValue":Z
    :pswitch_0
    const-string/jumbo v2, "no"

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v3}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move p1, v1

    goto :goto_0

    .line 110
    :pswitch_1
    const-string/jumbo v1, "yes"

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v3}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    move p1, v2

    goto :goto_0

    .line 113
    :pswitch_2
    const-string/jumbo v1, "true"

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v3}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    move p1, v2

    goto :goto_0

    .line 116
    :pswitch_3
    const-string/jumbo v2, "false"

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v3}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move p1, v1

    goto :goto_0

    .line 105
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static doubleProperty(Ljava/lang/String;D)D
    .locals 5
    .param p0, "name"    # Ljava/lang/String;
    .param p1, "defaultValue"    # D

    .prologue
    .line 76
    invoke-static {p0}, Lorg/icepdf/core/util/Defs;->property(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 77
    .local v1, "value":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 79
    :try_start_0
    invoke-static {v1}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide p1

    .line 85
    .end local p1    # "defaultValue":D
    :cond_0
    :goto_0
    return-wide p1

    .line 81
    .restart local p1    # "defaultValue":D
    :catch_0
    move-exception v0

    .line 82
    .local v0, "ex":Ljava/lang/NumberFormatException;
    sget-object v2, Lorg/icepdf/core/util/Defs;->logger:Ljava/util/logging/Logger;

    sget-object v3, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string/jumbo v4, "Failed to parse property."

    invoke-virtual {v2, v3, v4, v0}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public static intProperty(Ljava/lang/String;I)I
    .locals 5
    .param p0, "name"    # Ljava/lang/String;
    .param p1, "defaultValue"    # I

    .prologue
    .line 58
    invoke-static {p0}, Lorg/icepdf/core/util/Defs;->property(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 59
    .local v1, "value":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 61
    :try_start_0
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result p1

    .line 67
    .end local p1    # "defaultValue":I
    :cond_0
    :goto_0
    return p1

    .line 63
    .restart local p1    # "defaultValue":I
    :catch_0
    move-exception v0

    .line 64
    .local v0, "ex":Ljava/lang/NumberFormatException;
    sget-object v2, Lorg/icepdf/core/util/Defs;->logger:Ljava/util/logging/Logger;

    sget-object v3, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string/jumbo v4, "Failed to parse property."

    invoke-virtual {v2, v3, v4, v0}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public static property(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 32
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lorg/icepdf/core/util/Defs;->property(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static property(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0, "name"    # Ljava/lang/String;
    .param p1, "defaultValue"    # Ljava/lang/String;

    .prologue
    .line 42
    :try_start_0
    invoke-static {p0, p1}, Ljava/lang/System;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object p1

    .line 48
    .end local p1    # "defaultValue":Ljava/lang/String;
    :goto_0
    return-object p1

    .line 44
    .restart local p1    # "defaultValue":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 46
    .local v0, "ex":Ljava/lang/SecurityException;
    sget-object v1, Lorg/icepdf/core/util/Defs;->logger:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string/jumbo v3, "Security exception, property could not be set."

    invoke-virtual {v1, v2, v3, v0}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public static setProperty(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 5
    .param p0, "property"    # Ljava/lang/String;
    .param p1, "value"    # Ljava/lang/Object;

    .prologue
    .line 173
    :try_start_0
    invoke-static {}, Ljava/lang/System;->getProperties()Ljava/util/Properties;

    move-result-object v1

    .line 174
    .local v1, "prop":Ljava/util/Properties;
    if-eqz p1, :cond_0

    .line 175
    invoke-virtual {v1, p0, p1}, Ljava/util/Properties;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    .line 182
    .end local v1    # "prop":Ljava/util/Properties;
    :cond_0
    :goto_0
    return-void

    .line 178
    :catch_0
    move-exception v0

    .line 180
    .local v0, "ex":Ljava/lang/SecurityException;
    sget-object v2, Lorg/icepdf/core/util/Defs;->logger:Ljava/util/logging/Logger;

    sget-object v3, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string/jumbo v4, "Security exception, property could not be set."

    invoke-virtual {v2, v3, v4, v0}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public static setSystemProperty(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0, "name"    # Ljava/lang/String;
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 190
    invoke-static {p0, p1}, Lorg/icepdf/core/util/Defs;->setProperty(Ljava/lang/String;Ljava/lang/Object;)V

    .line 191
    return-void
.end method

.method public static sysProperty(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 127
    invoke-static {p0}, Lorg/icepdf/core/util/Defs;->property(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static sysProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;
    .param p1, "defaultValue"    # Ljava/lang/String;

    .prologue
    .line 134
    invoke-static {p0, p1}, Lorg/icepdf/core/util/Defs;->property(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static sysPropertyBoolean(Ljava/lang/String;)Z
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 155
    invoke-static {p0}, Lorg/icepdf/core/util/Defs;->booleanProperty(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static sysPropertyBoolean(Ljava/lang/String;Z)Z
    .locals 1
    .param p0, "name"    # Ljava/lang/String;
    .param p1, "defaultValue"    # Z

    .prologue
    .line 163
    invoke-static {p0, p1}, Lorg/icepdf/core/util/Defs;->booleanProperty(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static sysPropertyDouble(Ljava/lang/String;D)D
    .locals 3
    .param p0, "name"    # Ljava/lang/String;
    .param p1, "defaultValue"    # D

    .prologue
    .line 148
    invoke-static {p0, p1, p2}, Lorg/icepdf/core/util/Defs;->doubleProperty(Ljava/lang/String;D)D

    move-result-wide v0

    return-wide v0
.end method

.method public static sysPropertyInt(Ljava/lang/String;I)I
    .locals 1
    .param p0, "name"    # Ljava/lang/String;
    .param p1, "defaultValue"    # I

    .prologue
    .line 141
    invoke-static {p0, p1}, Lorg/icepdf/core/util/Defs;->intProperty(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method
