.class public Lorg/icepdf/core/util/LazyObjectLoader;
.super Ljava/lang/Object;
.source "LazyObjectLoader.java"

# interfaces
.implements Lorg/icepdf/core/util/MemoryManagerDelegate;


# static fields
.field private static final logger:Ljava/util/logging/Logger;


# instance fields
.field protected leastRecentlyUsed:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lorg/icepdf/core/pobjects/ObjectStream;",
            ">;"
        }
    .end annotation
.end field

.field private final leastRectlyUsedLock:Ljava/lang/Object;

.field private library:Lorg/icepdf/core/util/Library;

.field private m_CrossReference:Lorg/icepdf/core/pobjects/CrossReference;

.field private m_SeekableInput:Lorg/icepdf/core/io/SeekableInput;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    const-class v0, Lorg/icepdf/core/util/LazyObjectLoader;

    invoke-virtual {v0}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Lorg/icepdf/core/util/LazyObjectLoader;->logger:Ljava/util/logging/Logger;

    return-void
.end method

.method public constructor <init>(Lorg/icepdf/core/util/Library;Lorg/icepdf/core/io/SeekableInput;Lorg/icepdf/core/pobjects/CrossReference;)V
    .locals 2
    .param p1, "lib"    # Lorg/icepdf/core/util/Library;
    .param p2, "seekableInput"    # Lorg/icepdf/core/io/SeekableInput;
    .param p3, "xref"    # Lorg/icepdf/core/pobjects/CrossReference;

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lorg/icepdf/core/util/LazyObjectLoader;->leastRectlyUsedLock:Ljava/lang/Object;

    .line 40
    iput-object p1, p0, Lorg/icepdf/core/util/LazyObjectLoader;->library:Lorg/icepdf/core/util/Library;

    .line 41
    iput-object p2, p0, Lorg/icepdf/core/util/LazyObjectLoader;->m_SeekableInput:Lorg/icepdf/core/io/SeekableInput;

    .line 42
    iput-object p3, p0, Lorg/icepdf/core/util/LazyObjectLoader;->m_CrossReference:Lorg/icepdf/core/pobjects/CrossReference;

    .line 43
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0x100

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lorg/icepdf/core/util/LazyObjectLoader;->leastRecentlyUsed:Ljava/util/ArrayList;

    .line 44
    return-void
.end method


# virtual methods
.method public dispose()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 137
    iput-object v1, p0, Lorg/icepdf/core/util/LazyObjectLoader;->library:Lorg/icepdf/core/util/Library;

    .line 138
    iput-object v1, p0, Lorg/icepdf/core/util/LazyObjectLoader;->m_SeekableInput:Lorg/icepdf/core/io/SeekableInput;

    .line 139
    iput-object v1, p0, Lorg/icepdf/core/util/LazyObjectLoader;->m_CrossReference:Lorg/icepdf/core/pobjects/CrossReference;

    .line 140
    iget-object v0, p0, Lorg/icepdf/core/util/LazyObjectLoader;->leastRecentlyUsed:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 141
    iget-object v0, p0, Lorg/icepdf/core/util/LazyObjectLoader;->leastRecentlyUsed:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 142
    iput-object v1, p0, Lorg/icepdf/core/util/LazyObjectLoader;->leastRecentlyUsed:Ljava/util/ArrayList;

    .line 144
    :cond_0
    return-void
.end method

.method public getLibrary()Lorg/icepdf/core/util/Library;
    .locals 1

    .prologue
    .line 179
    iget-object v0, p0, Lorg/icepdf/core/util/LazyObjectLoader;->library:Lorg/icepdf/core/util/Library;

    return-object v0
.end method

.method public haveEntry(Lorg/icepdf/core/pobjects/Reference;)Z
    .locals 5
    .param p1, "reference"    # Lorg/icepdf/core/pobjects/Reference;

    .prologue
    const/4 v2, 0x0

    .line 101
    if-eqz p1, :cond_0

    iget-object v3, p0, Lorg/icepdf/core/util/LazyObjectLoader;->m_CrossReference:Lorg/icepdf/core/pobjects/CrossReference;

    if-nez v3, :cond_1

    .line 105
    :cond_0
    :goto_0
    return v2

    .line 103
    :cond_1
    invoke-virtual {p1}, Lorg/icepdf/core/pobjects/Reference;->getObjectNumber()I

    move-result v1

    .line 104
    .local v1, "objNum":I
    iget-object v3, p0, Lorg/icepdf/core/util/LazyObjectLoader;->m_CrossReference:Lorg/icepdf/core/pobjects/CrossReference;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Lorg/icepdf/core/pobjects/CrossReference;->getEntryForObject(Ljava/lang/Integer;)Lorg/icepdf/core/pobjects/CrossReference$Entry;

    move-result-object v0

    .line 105
    .local v0, "entry":Lorg/icepdf/core/pobjects/CrossReference$Entry;
    if-eqz v0, :cond_0

    const/4 v2, 0x1

    goto :goto_0
.end method

.method public loadObject(Lorg/icepdf/core/pobjects/Reference;)Z
    .locals 24
    .param p1, "reference"    # Lorg/icepdf/core/pobjects/Reference;

    .prologue
    .line 47
    if-eqz p1, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/core/util/LazyObjectLoader;->library:Lorg/icepdf/core/util/Library;

    move-object/from16 v20, v0

    if-eqz v20, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/core/util/LazyObjectLoader;->m_CrossReference:Lorg/icepdf/core/pobjects/CrossReference;

    move-object/from16 v20, v0

    if-nez v20, :cond_2

    .line 48
    :cond_0
    const/4 v7, 0x0

    .line 97
    :cond_1
    :goto_0
    return v7

    .line 49
    :cond_2
    invoke-virtual/range {p1 .. p1}, Lorg/icepdf/core/pobjects/Reference;->getObjectNumber()I

    move-result v9

    .line 50
    .local v9, "objNum":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/core/util/LazyObjectLoader;->m_CrossReference:Lorg/icepdf/core/pobjects/CrossReference;

    move-object/from16 v20, v0

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Lorg/icepdf/core/pobjects/CrossReference;->getEntryForObject(Ljava/lang/Integer;)Lorg/icepdf/core/pobjects/CrossReference$Entry;

    move-result-object v6

    .line 51
    .local v6, "entry":Lorg/icepdf/core/pobjects/CrossReference$Entry;
    if-nez v6, :cond_3

    .line 52
    const/4 v7, 0x0

    goto :goto_0

    .line 53
    :cond_3
    const/4 v7, 0x0

    .line 54
    .local v7, "gotSomething":Z
    instance-of v0, v6, Lorg/icepdf/core/pobjects/CrossReference$UsedEntry;

    move/from16 v20, v0

    if-eqz v20, :cond_7

    .line 56
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/core/util/LazyObjectLoader;->m_SeekableInput:Lorg/icepdf/core/io/SeekableInput;

    move-object/from16 v20, v0

    if-eqz v20, :cond_4

    .line 57
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/core/util/LazyObjectLoader;->m_SeekableInput:Lorg/icepdf/core/io/SeekableInput;

    move-object/from16 v20, v0

    invoke-interface/range {v20 .. v20}, Lorg/icepdf/core/io/SeekableInput;->beginThreadAccess()V

    .line 58
    move-object v0, v6

    check-cast v0, Lorg/icepdf/core/pobjects/CrossReference$UsedEntry;

    move-object v15, v0

    .line 59
    .local v15, "usedEntry":Lorg/icepdf/core/pobjects/CrossReference$UsedEntry;
    invoke-virtual {v15}, Lorg/icepdf/core/pobjects/CrossReference$UsedEntry;->getFilePositionOfObject()J

    move-result-wide v16

    .line 60
    .local v16, "position":J
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/core/util/LazyObjectLoader;->m_SeekableInput:Lorg/icepdf/core/io/SeekableInput;

    move-object/from16 v20, v0

    invoke-interface/range {v20 .. v20}, Lorg/icepdf/core/io/SeekableInput;->getAbsolutePosition()J

    move-result-wide v18

    .line 61
    .local v18, "savedPosition":J
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/core/util/LazyObjectLoader;->m_SeekableInput:Lorg/icepdf/core/io/SeekableInput;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    move-wide/from16 v1, v16

    invoke-interface {v0, v1, v2}, Lorg/icepdf/core/io/SeekableInput;->seekAbsolute(J)V

    .line 62
    new-instance v14, Lorg/icepdf/core/util/Parser;

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/core/util/LazyObjectLoader;->m_SeekableInput:Lorg/icepdf/core/io/SeekableInput;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-direct {v14, v0}, Lorg/icepdf/core/util/Parser;-><init>(Lorg/icepdf/core/io/SeekableInput;)V

    .line 63
    .local v14, "parser":Lorg/icepdf/core/util/Parser;
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/core/util/LazyObjectLoader;->library:Lorg/icepdf/core/util/Library;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v14, v0}, Lorg/icepdf/core/util/Parser;->getObject(Lorg/icepdf/core/util/Library;)Ljava/lang/Object;

    move-result-object v8

    .line 64
    .local v8, "ob":Ljava/lang/Object;
    if-eqz v8, :cond_5

    const/4 v7, 0x1

    .line 65
    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/core/util/LazyObjectLoader;->m_SeekableInput:Lorg/icepdf/core/io/SeekableInput;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    move-wide/from16 v1, v18

    invoke-interface {v0, v1, v2}, Lorg/icepdf/core/io/SeekableInput;->seekAbsolute(J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 73
    .end local v8    # "ob":Ljava/lang/Object;
    .end local v14    # "parser":Lorg/icepdf/core/util/Parser;
    .end local v15    # "usedEntry":Lorg/icepdf/core/pobjects/CrossReference$UsedEntry;
    .end local v16    # "position":J
    .end local v18    # "savedPosition":J
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/core/util/LazyObjectLoader;->m_SeekableInput:Lorg/icepdf/core/io/SeekableInput;

    move-object/from16 v20, v0

    if-eqz v20, :cond_1

    .line 74
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/core/util/LazyObjectLoader;->m_SeekableInput:Lorg/icepdf/core/io/SeekableInput;

    move-object/from16 v20, v0

    invoke-interface/range {v20 .. v20}, Lorg/icepdf/core/io/SeekableInput;->endThreadAccess()V

    goto/16 :goto_0

    .line 64
    .restart local v8    # "ob":Ljava/lang/Object;
    .restart local v14    # "parser":Lorg/icepdf/core/util/Parser;
    .restart local v15    # "usedEntry":Lorg/icepdf/core/pobjects/CrossReference$UsedEntry;
    .restart local v16    # "position":J
    .restart local v18    # "savedPosition":J
    :cond_5
    const/4 v7, 0x0

    goto :goto_1

    .line 68
    .end local v8    # "ob":Ljava/lang/Object;
    .end local v14    # "parser":Lorg/icepdf/core/util/Parser;
    .end local v15    # "usedEntry":Lorg/icepdf/core/pobjects/CrossReference$UsedEntry;
    .end local v16    # "position":J
    .end local v18    # "savedPosition":J
    :catch_0
    move-exception v5

    .line 69
    .local v5, "e":Ljava/lang/Exception;
    :try_start_1
    sget-object v20, Lorg/icepdf/core/util/LazyObjectLoader;->logger:Ljava/util/logging/Logger;

    sget-object v21, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v23, "Error loading object instance: "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {p1 .. p1}, Lorg/icepdf/core/pobjects/Reference;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    move-object/from16 v2, v22

    invoke-virtual {v0, v1, v2, v5}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 73
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/core/util/LazyObjectLoader;->m_SeekableInput:Lorg/icepdf/core/io/SeekableInput;

    move-object/from16 v20, v0

    if-eqz v20, :cond_1

    .line 74
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/core/util/LazyObjectLoader;->m_SeekableInput:Lorg/icepdf/core/io/SeekableInput;

    move-object/from16 v20, v0

    invoke-interface/range {v20 .. v20}, Lorg/icepdf/core/io/SeekableInput;->endThreadAccess()V

    goto/16 :goto_0

    .line 73
    .end local v5    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/core/util/LazyObjectLoader;->m_SeekableInput:Lorg/icepdf/core/io/SeekableInput;

    move-object/from16 v21, v0

    if-eqz v21, :cond_6

    .line 74
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/core/util/LazyObjectLoader;->m_SeekableInput:Lorg/icepdf/core/io/SeekableInput;

    move-object/from16 v21, v0

    invoke-interface/range {v21 .. v21}, Lorg/icepdf/core/io/SeekableInput;->endThreadAccess()V

    :cond_6
    throw v20

    .line 76
    :cond_7
    instance-of v0, v6, Lorg/icepdf/core/pobjects/CrossReference$CompressedEntry;

    move/from16 v20, v0

    if-eqz v20, :cond_1

    .line 78
    :try_start_2
    move-object v0, v6

    check-cast v0, Lorg/icepdf/core/pobjects/CrossReference$CompressedEntry;

    move-object v4, v0

    .line 79
    .local v4, "compressedEntry":Lorg/icepdf/core/pobjects/CrossReference$CompressedEntry;
    invoke-virtual {v4}, Lorg/icepdf/core/pobjects/CrossReference$CompressedEntry;->getObjectNumberOfContainingObjectStream()I

    move-result v13

    .line 80
    .local v13, "objectStreamsObjectNumber":I
    invoke-virtual {v4}, Lorg/icepdf/core/pobjects/CrossReference$CompressedEntry;->getIndexWithinObjectStream()I

    move-result v10

    .line 81
    .local v10, "objectIndex":I
    new-instance v12, Lorg/icepdf/core/pobjects/Reference;

    const/16 v20, 0x0

    move/from16 v0, v20

    invoke-direct {v12, v13, v0}, Lorg/icepdf/core/pobjects/Reference;-><init>(II)V

    .line 82
    .local v12, "objectStreamRef":Lorg/icepdf/core/pobjects/Reference;
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/core/util/LazyObjectLoader;->library:Lorg/icepdf/core/util/Library;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v12}, Lorg/icepdf/core/util/Library;->getObject(Lorg/icepdf/core/pobjects/Reference;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lorg/icepdf/core/pobjects/ObjectStream;

    .line 83
    .local v11, "objectStream":Lorg/icepdf/core/pobjects/ObjectStream;
    if-eqz v11, :cond_1

    .line 84
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/core/util/LazyObjectLoader;->leastRectlyUsedLock:Ljava/lang/Object;

    move-object/from16 v21, v0

    monitor-enter v21
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    .line 85
    :try_start_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/core/util/LazyObjectLoader;->leastRecentlyUsed:Ljava/util/ArrayList;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v11}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 86
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/core/util/LazyObjectLoader;->leastRecentlyUsed:Ljava/util/ArrayList;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 87
    monitor-exit v21
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 89
    :try_start_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/core/util/LazyObjectLoader;->library:Lorg/icepdf/core/util/Library;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v11, v0, v10}, Lorg/icepdf/core/pobjects/ObjectStream;->loadObject(Lorg/icepdf/core/util/Library;I)Z
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    move-result v7

    goto/16 :goto_0

    .line 87
    :catchall_1
    move-exception v20

    :try_start_5
    monitor-exit v21
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :try_start_6
    throw v20
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1

    .line 92
    .end local v4    # "compressedEntry":Lorg/icepdf/core/pobjects/CrossReference$CompressedEntry;
    .end local v10    # "objectIndex":I
    .end local v11    # "objectStream":Lorg/icepdf/core/pobjects/ObjectStream;
    .end local v12    # "objectStreamRef":Lorg/icepdf/core/pobjects/Reference;
    .end local v13    # "objectStreamsObjectNumber":I
    :catch_1
    move-exception v5

    .line 93
    .restart local v5    # "e":Ljava/lang/Exception;
    sget-object v20, Lorg/icepdf/core/util/LazyObjectLoader;->logger:Ljava/util/logging/Logger;

    sget-object v21, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v23, "Error loading object instance: "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {p1 .. p1}, Lorg/icepdf/core/pobjects/Reference;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    move-object/from16 v2, v22

    invoke-virtual {v0, v1, v2, v5}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0
.end method

.method public loadTrailer(J)Lorg/icepdf/core/pobjects/PTrailer;
    .locals 13
    .param p1, "position"    # J

    .prologue
    .line 109
    const/4 v5, 0x0

    .line 111
    .local v5, "trailer":Lorg/icepdf/core/pobjects/PTrailer;
    :try_start_0
    iget-object v8, p0, Lorg/icepdf/core/util/LazyObjectLoader;->m_SeekableInput:Lorg/icepdf/core/io/SeekableInput;

    if-eqz v8, :cond_2

    .line 112
    iget-object v8, p0, Lorg/icepdf/core/util/LazyObjectLoader;->m_SeekableInput:Lorg/icepdf/core/io/SeekableInput;

    invoke-interface {v8}, Lorg/icepdf/core/io/SeekableInput;->beginThreadAccess()V

    .line 113
    iget-object v8, p0, Lorg/icepdf/core/util/LazyObjectLoader;->m_SeekableInput:Lorg/icepdf/core/io/SeekableInput;

    invoke-interface {v8}, Lorg/icepdf/core/io/SeekableInput;->getAbsolutePosition()J

    move-result-wide v6

    .line 114
    .local v6, "savedPosition":J
    iget-object v8, p0, Lorg/icepdf/core/util/LazyObjectLoader;->m_SeekableInput:Lorg/icepdf/core/io/SeekableInput;

    invoke-interface {v8, p1, p2}, Lorg/icepdf/core/io/SeekableInput;->seekAbsolute(J)V

    .line 115
    new-instance v4, Lorg/icepdf/core/util/Parser;

    iget-object v8, p0, Lorg/icepdf/core/util/LazyObjectLoader;->m_SeekableInput:Lorg/icepdf/core/io/SeekableInput;

    invoke-direct {v4, v8}, Lorg/icepdf/core/util/Parser;-><init>(Lorg/icepdf/core/io/SeekableInput;)V

    .line 116
    .local v4, "parser":Lorg/icepdf/core/util/Parser;
    iget-object v8, p0, Lorg/icepdf/core/util/LazyObjectLoader;->library:Lorg/icepdf/core/util/Library;

    invoke-virtual {v4, v8}, Lorg/icepdf/core/util/Parser;->getObject(Lorg/icepdf/core/util/Library;)Ljava/lang/Object;

    move-result-object v3

    .line 117
    .local v3, "obj":Ljava/lang/Object;
    instance-of v8, v3, Lorg/icepdf/core/pobjects/PObject;

    if-eqz v8, :cond_0

    .line 118
    check-cast v3, Lorg/icepdf/core/pobjects/PObject;

    .end local v3    # "obj":Ljava/lang/Object;
    invoke-virtual {v3}, Lorg/icepdf/core/pobjects/PObject;->getObject()Ljava/lang/Object;

    move-result-object v3

    .line 119
    .restart local v3    # "obj":Ljava/lang/Object;
    :cond_0
    move-object v0, v3

    check-cast v0, Lorg/icepdf/core/pobjects/PTrailer;

    move-object v5, v0

    .line 120
    if-eqz v5, :cond_1

    .line 121
    invoke-virtual {v5, p1, p2}, Lorg/icepdf/core/pobjects/PTrailer;->setPosition(J)V

    .line 122
    :cond_1
    iget-object v8, p0, Lorg/icepdf/core/util/LazyObjectLoader;->m_SeekableInput:Lorg/icepdf/core/io/SeekableInput;

    invoke-interface {v8, v6, v7}, Lorg/icepdf/core/io/SeekableInput;->seekAbsolute(J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 130
    .end local v3    # "obj":Ljava/lang/Object;
    .end local v4    # "parser":Lorg/icepdf/core/util/Parser;
    .end local v6    # "savedPosition":J
    :cond_2
    iget-object v8, p0, Lorg/icepdf/core/util/LazyObjectLoader;->m_SeekableInput:Lorg/icepdf/core/io/SeekableInput;

    if-eqz v8, :cond_3

    .line 131
    iget-object v8, p0, Lorg/icepdf/core/util/LazyObjectLoader;->m_SeekableInput:Lorg/icepdf/core/io/SeekableInput;

    invoke-interface {v8}, Lorg/icepdf/core/io/SeekableInput;->endThreadAccess()V

    .line 133
    :cond_3
    :goto_0
    return-object v5

    .line 125
    :catch_0
    move-exception v2

    .line 126
    .local v2, "e":Ljava/lang/Exception;
    :try_start_1
    sget-object v8, Lorg/icepdf/core/util/LazyObjectLoader;->logger:Ljava/util/logging/Logger;

    sget-object v9, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "Error loading PTrailer instance: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v9, v10, v2}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 130
    iget-object v8, p0, Lorg/icepdf/core/util/LazyObjectLoader;->m_SeekableInput:Lorg/icepdf/core/io/SeekableInput;

    if-eqz v8, :cond_3

    .line 131
    iget-object v8, p0, Lorg/icepdf/core/util/LazyObjectLoader;->m_SeekableInput:Lorg/icepdf/core/io/SeekableInput;

    invoke-interface {v8}, Lorg/icepdf/core/io/SeekableInput;->endThreadAccess()V

    goto :goto_0

    .line 130
    .end local v2    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v8

    iget-object v9, p0, Lorg/icepdf/core/util/LazyObjectLoader;->m_SeekableInput:Lorg/icepdf/core/io/SeekableInput;

    if-eqz v9, :cond_4

    .line 131
    iget-object v9, p0, Lorg/icepdf/core/util/LazyObjectLoader;->m_SeekableInput:Lorg/icepdf/core/io/SeekableInput;

    invoke-interface {v9}, Lorg/icepdf/core/io/SeekableInput;->endThreadAccess()V

    :cond_4
    throw v8
.end method

.method public reduceMemory(I)Z
    .locals 9
    .param p1, "reductionPolicy"    # I

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 152
    const/4 v2, 0x0

    .line 153
    .local v2, "numToDo":I
    iget-object v6, p0, Lorg/icepdf/core/util/LazyObjectLoader;->leastRectlyUsedLock:Ljava/lang/Object;

    monitor-enter v6

    .line 154
    :try_start_0
    iget-object v7, p0, Lorg/icepdf/core/util/LazyObjectLoader;->leastRecentlyUsed:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 155
    .local v1, "lruSize":I
    if-ne p1, v4, :cond_2

    .line 156
    mul-int/lit8 v7, v1, 0x4b

    div-int/lit8 v2, v7, 0x64

    .line 164
    :cond_0
    :goto_0
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    if-ge v0, v2, :cond_4

    .line 165
    iget-object v7, p0, Lorg/icepdf/core/util/LazyObjectLoader;->leastRecentlyUsed:Ljava/util/ArrayList;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/icepdf/core/pobjects/ObjectStream;

    .line 166
    .local v3, "objStm":Lorg/icepdf/core/pobjects/ObjectStream;
    if-eqz v3, :cond_1

    .line 167
    const/4 v7, 0x1

    invoke-virtual {v3, v7}, Lorg/icepdf/core/pobjects/ObjectStream;->dispose(Z)V

    .line 164
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 157
    .end local v0    # "i":I
    .end local v3    # "objStm":Lorg/icepdf/core/pobjects/ObjectStream;
    :cond_2
    if-nez p1, :cond_0

    .line 158
    const/4 v7, 0x5

    if-le v1, v7, :cond_3

    .line 159
    mul-int/lit8 v7, v1, 0x32

    div-int/lit8 v2, v7, 0x64

    goto :goto_0

    .line 160
    :cond_3
    if-lez v1, :cond_0

    .line 161
    const/4 v2, 0x1

    goto :goto_0

    .line 169
    .restart local v0    # "i":I
    :cond_4
    monitor-exit v6

    .line 170
    if-lez v2, :cond_5

    :goto_2
    return v4

    .line 169
    .end local v0    # "i":I
    .end local v1    # "lruSize":I
    :catchall_0
    move-exception v4

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4

    .restart local v0    # "i":I
    .restart local v1    # "lruSize":I
    :cond_5
    move v4, v5

    .line 170
    goto :goto_2
.end method
