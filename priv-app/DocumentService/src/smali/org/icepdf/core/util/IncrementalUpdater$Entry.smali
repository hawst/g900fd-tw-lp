.class Lorg/icepdf/core/util/IncrementalUpdater$Entry;
.super Ljava/lang/Object;
.source "IncrementalUpdater.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/icepdf/core/util/IncrementalUpdater;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Entry"
.end annotation


# static fields
.field private static final POSITION_DELETED:J = -0x1L


# instance fields
.field private nextDeletedObjectNumber:I

.field private position:J

.field private reference:Lorg/icepdf/core/pobjects/Reference;


# direct methods
.method constructor <init>(Lorg/icepdf/core/pobjects/Reference;)V
    .locals 2
    .param p1, "ref"    # Lorg/icepdf/core/pobjects/Reference;

    .prologue
    .line 712
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 713
    iput-object p1, p0, Lorg/icepdf/core/util/IncrementalUpdater$Entry;->reference:Lorg/icepdf/core/pobjects/Reference;

    .line 714
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lorg/icepdf/core/util/IncrementalUpdater$Entry;->position:J

    .line 715
    return-void
.end method

.method constructor <init>(Lorg/icepdf/core/pobjects/Reference;J)V
    .locals 0
    .param p1, "ref"    # Lorg/icepdf/core/pobjects/Reference;
    .param p2, "pos"    # J

    .prologue
    .line 704
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 705
    iput-object p1, p0, Lorg/icepdf/core/util/IncrementalUpdater$Entry;->reference:Lorg/icepdf/core/pobjects/Reference;

    .line 706
    iput-wide p2, p0, Lorg/icepdf/core/util/IncrementalUpdater$Entry;->position:J

    .line 707
    return-void
.end method


# virtual methods
.method getNextDeletedObjectNumber()I
    .locals 1

    .prologue
    .line 734
    iget v0, p0, Lorg/icepdf/core/util/IncrementalUpdater$Entry;->nextDeletedObjectNumber:I

    return v0
.end method

.method getPosition()J
    .locals 2

    .prologue
    .line 726
    iget-wide v0, p0, Lorg/icepdf/core/util/IncrementalUpdater$Entry;->position:J

    return-wide v0
.end method

.method getReference()Lorg/icepdf/core/pobjects/Reference;
    .locals 1

    .prologue
    .line 718
    iget-object v0, p0, Lorg/icepdf/core/util/IncrementalUpdater$Entry;->reference:Lorg/icepdf/core/pobjects/Reference;

    return-object v0
.end method

.method isDeleted()Z
    .locals 4

    .prologue
    .line 722
    iget-wide v0, p0, Lorg/icepdf/core/util/IncrementalUpdater$Entry;->position:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method setNextDeletedObjectNumber(I)V
    .locals 0
    .param p1, "nextDelObjNum"    # I

    .prologue
    .line 730
    iput p1, p0, Lorg/icepdf/core/util/IncrementalUpdater$Entry;->nextDeletedObjectNumber:I

    .line 731
    return-void
.end method
