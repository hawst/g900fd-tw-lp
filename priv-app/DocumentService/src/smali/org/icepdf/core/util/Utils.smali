.class public Lorg/icepdf/core/util/Utils;
.super Ljava/lang/Object;
.source "Utils.java"


# static fields
.field private static final logger:Ljava/util/logging/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    const-class v0, Lorg/icepdf/core/util/Utils;

    invoke-virtual {v0}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Lorg/icepdf/core/util/Utils;->logger:Ljava/util/logging/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static convertByteArrayToByteString([B)Ljava/lang/String;
    .locals 5
    .param p0, "bytes"    # [B

    .prologue
    .line 341
    array-length v2, p0

    .line 342
    .local v2, "max":I
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 343
    .local v3, "sb":Ljava/lang/StringBuilder;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v2, :cond_0

    .line 344
    aget-byte v4, p0, v1

    and-int/lit16 v0, v4, 0xff

    .line 345
    .local v0, "b":I
    int-to-char v4, v0

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 343
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 347
    .end local v0    # "b":I
    :cond_0
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4
.end method

.method public static convertByteArrayToHexString([BIIZIC)Ljava/lang/String;
    .locals 10
    .param p0, "buffer"    # [B
    .param p1, "offset"    # I
    .param p2, "length"    # I
    .param p3, "addSpaceSeparator"    # Z
    .param p4, "addDelimiterEverNBytes"    # I
    .param p5, "delimiter"    # C

    .prologue
    const/4 v9, 0x2

    .line 114
    if-eqz p3, :cond_1

    const/4 v8, 0x3

    :goto_0
    mul-int v5, p2, v8

    .line 115
    .local v5, "presize":I
    if-lez p4, :cond_0

    .line 116
    div-int v8, p2, p4

    add-int/2addr v5, v8

    .line 117
    :cond_0
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 118
    .local v7, "sb":Ljava/lang/StringBuilder;
    const/4 v1, 0x0

    .line 119
    .local v1, "delimiterCount":I
    add-int v2, p1, p2

    .line 120
    .local v2, "end":I
    move v4, p1

    .local v4, "index":I
    :goto_1
    if-ge v4, v2, :cond_5

    .line 121
    const/4 v0, 0x0

    .line 122
    .local v0, "currValue":I
    aget-byte v8, p0, v4

    and-int/lit16 v8, v8, 0xff

    or-int/2addr v0, v8

    .line 123
    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v6

    .line 124
    .local v6, "s":Ljava/lang/String;
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v3

    .local v3, "i":I
    :goto_2
    if-ge v3, v9, :cond_2

    .line 125
    const/16 v8, 0x30

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 124
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .end local v0    # "currValue":I
    .end local v1    # "delimiterCount":I
    .end local v2    # "end":I
    .end local v3    # "i":I
    .end local v4    # "index":I
    .end local v5    # "presize":I
    .end local v6    # "s":Ljava/lang/String;
    .end local v7    # "sb":Ljava/lang/StringBuilder;
    :cond_1
    move v8, v9

    .line 114
    goto :goto_0

    .line 126
    .restart local v0    # "currValue":I
    .restart local v1    # "delimiterCount":I
    .restart local v2    # "end":I
    .restart local v3    # "i":I
    .restart local v4    # "index":I
    .restart local v5    # "presize":I
    .restart local v6    # "s":Ljava/lang/String;
    .restart local v7    # "sb":Ljava/lang/StringBuilder;
    :cond_2
    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 127
    if-eqz p3, :cond_3

    .line 128
    const/16 v8, 0x20

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 129
    :cond_3
    add-int/lit8 v1, v1, 0x1

    .line 130
    if-lez p4, :cond_4

    if-ne v1, p4, :cond_4

    .line 131
    const/4 v1, 0x0

    .line 132
    invoke-virtual {v7, p5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 120
    :cond_4
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 135
    .end local v0    # "currValue":I
    .end local v3    # "i":I
    .end local v6    # "s":Ljava/lang/String;
    :cond_5
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    return-object v8
.end method

.method public static convertByteArrayToHexString([BZ)Ljava/lang/String;
    .locals 6
    .param p0, "buffer"    # [B
    .param p1, "addSpaceSeparator"    # Z

    .prologue
    const/4 v1, 0x0

    .line 102
    array-length v2, p0

    const/4 v4, -0x1

    move-object v0, p0

    move v3, p1

    move v5, v1

    invoke-static/range {v0 .. v5}, Lorg/icepdf/core/util/Utils;->convertByteArrayToHexString([BIIZIC)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static convertByteArrayToHexString([BZIC)Ljava/lang/String;
    .locals 6
    .param p0, "buffer"    # [B
    .param p1, "addSpaceSeparator"    # Z
    .param p2, "addDelimiterEverNBytes"    # I
    .param p3, "delimiter"    # C

    .prologue
    .line 107
    const/4 v1, 0x0

    array-length v2, p0

    move-object v0, p0

    move v3, p1

    move v4, p2

    move v5, p3

    invoke-static/range {v0 .. v5}, Lorg/icepdf/core/util/Utils;->convertByteArrayToHexString([BIIZIC)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static convertByteCharSequenceToByteArray(Ljava/lang/CharSequence;)[B
    .locals 4
    .param p0, "string"    # Ljava/lang/CharSequence;

    .prologue
    .line 321
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v2

    .line 322
    .local v2, "max":I
    new-array v0, v2, [B

    .line 323
    .local v0, "bytes":[B
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v2, :cond_0

    .line 324
    invoke-interface {p0, v1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v3

    int-to-byte v3, v3

    aput-byte v3, v0, v1

    .line 323
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 326
    :cond_0
    return-object v0
.end method

.method public static getContentAndReplaceInputStream([Ljava/io/InputStream;Z)Ljava/lang/String;
    .locals 12
    .param p0, "inArray"    # [Ljava/io/InputStream;
    .param p1, "convertToHex"    # Z

    .prologue
    .line 167
    const/4 v1, 0x0

    .line 169
    .local v1, "content":Ljava/lang/String;
    :try_start_0
    new-instance v7, Ljava/io/ByteArrayOutputStream;

    const/16 v9, 0x400

    invoke-direct {v7, v9}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    .line 170
    .local v7, "out":Ljava/io/ByteArrayOutputStream;
    const/4 v9, 0x0

    aget-object v5, p0, v9

    .line 172
    .local v5, "in":Ljava/io/InputStream;
    const/16 v9, 0x400

    new-array v0, v9, [B

    .line 174
    .local v0, "buf":[B
    :goto_0
    const/4 v9, 0x0

    array-length v10, v0

    invoke-virtual {v5, v0, v9, v10}, Ljava/io/InputStream;->read([BII)I

    move-result v8

    .line 175
    .local v8, "read":I
    if-gez v8, :cond_1

    .line 187
    instance-of v9, v5, Lorg/icepdf/core/io/SeekableInput;

    if-nez v9, :cond_0

    .line 188
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V

    .line 189
    :cond_0
    invoke-virtual {v7}, Ljava/io/ByteArrayOutputStream;->flush()V

    .line 190
    invoke-virtual {v7}, Ljava/io/ByteArrayOutputStream;->close()V

    .line 191
    invoke-virtual {v7}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v3

    .line 192
    .local v3, "data":[B
    const/4 v7, 0x0

    .line 193
    const/4 v9, 0x0

    new-instance v10, Ljava/io/ByteArrayInputStream;

    invoke-direct {v10, v3}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    aput-object v10, p0, v9

    .line 194
    if-eqz p1, :cond_2

    .line 195
    const/4 v9, 0x1

    invoke-static {v3, v9}, Lorg/icepdf/core/util/Utils;->convertByteArrayToHexString([BZ)Ljava/lang/String;

    move-result-object v1

    .line 205
    .end local v0    # "buf":[B
    .end local v3    # "data":[B
    .end local v5    # "in":Ljava/io/InputStream;
    .end local v7    # "out":Ljava/io/ByteArrayOutputStream;
    .end local v8    # "read":I
    :goto_1
    return-object v1

    .line 177
    .restart local v0    # "buf":[B
    .restart local v5    # "in":Ljava/io/InputStream;
    .restart local v7    # "out":Ljava/io/ByteArrayOutputStream;
    .restart local v8    # "read":I
    :cond_1
    const/4 v9, 0x0

    invoke-virtual {v7, v0, v9, v8}, Ljava/io/ByteArrayOutputStream;->write([BII)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 199
    .end local v0    # "buf":[B
    .end local v5    # "in":Ljava/io/InputStream;
    .end local v7    # "out":Ljava/io/ByteArrayOutputStream;
    .end local v8    # "read":I
    :catch_0
    move-exception v6

    .line 200
    .local v6, "ioe":Ljava/io/IOException;
    sget-object v9, Lorg/icepdf/core/util/Utils;->logger:Ljava/util/logging/Logger;

    sget-object v10, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string/jumbo v11, "Problem getting debug string"

    invoke-virtual {v9, v10, v11}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    goto :goto_1

    .line 197
    .end local v6    # "ioe":Ljava/io/IOException;
    .restart local v0    # "buf":[B
    .restart local v3    # "data":[B
    .restart local v5    # "in":Ljava/io/InputStream;
    .restart local v7    # "out":Ljava/io/ByteArrayOutputStream;
    .restart local v8    # "read":I
    :cond_2
    :try_start_1
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v3}, Ljava/lang/String;-><init>([B)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    .end local v1    # "content":Ljava/lang/String;
    .local v2, "content":Ljava/lang/String;
    move-object v1, v2

    .end local v2    # "content":Ljava/lang/String;
    .restart local v1    # "content":Ljava/lang/String;
    goto :goto_1

    .line 202
    .end local v0    # "buf":[B
    .end local v3    # "data":[B
    .end local v5    # "in":Ljava/io/InputStream;
    .end local v7    # "out":Ljava/io/ByteArrayOutputStream;
    .end local v8    # "read":I
    :catch_1
    move-exception v4

    .line 203
    .local v4, "e":Ljava/lang/Throwable;
    sget-object v9, Lorg/icepdf/core/util/Utils;->logger:Ljava/util/logging/Logger;

    sget-object v10, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string/jumbo v11, "Problem getting content stream, skipping"

    invoke-virtual {v9, v10, v11}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public static getContentFromSeekableInput(Lorg/icepdf/core/io/SeekableInput;Z)Ljava/lang/String;
    .locals 11
    .param p0, "in"    # Lorg/icepdf/core/io/SeekableInput;
    .param p1, "convertToHex"    # Z

    .prologue
    .line 209
    const/4 v0, 0x0

    .line 211
    .local v0, "content":Ljava/lang/String;
    :try_start_0
    invoke-interface {p0}, Lorg/icepdf/core/io/SeekableInput;->beginThreadAccess()V

    .line 213
    invoke-interface {p0}, Lorg/icepdf/core/io/SeekableInput;->getAbsolutePosition()J

    move-result-wide v6

    .line 215
    .local v6, "position":J
    new-instance v4, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v4}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 228
    .local v4, "out":Ljava/io/ByteArrayOutputStream;
    :goto_0
    invoke-interface {p0}, Lorg/icepdf/core/io/SeekableInput;->getInputStream()Ljava/io/InputStream;

    move-result-object v8

    invoke-virtual {v8}, Ljava/io/InputStream;->read()I

    move-result v5

    .line 229
    .local v5, "read":I
    if-gez v5, :cond_0

    .line 234
    invoke-interface {p0, v6, v7}, Lorg/icepdf/core/io/SeekableInput;->seekAbsolute(J)V

    .line 236
    invoke-virtual {v4}, Ljava/io/ByteArrayOutputStream;->flush()V

    .line 237
    invoke-virtual {v4}, Ljava/io/ByteArrayOutputStream;->close()V

    .line 238
    invoke-virtual {v4}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v2

    .line 239
    .local v2, "data":[B
    if-eqz p1, :cond_1

    .line 240
    const/4 v8, 0x1

    invoke-static {v2, v8}, Lorg/icepdf/core/util/Utils;->convertByteArrayToHexString([BZ)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 248
    :goto_1
    invoke-interface {p0}, Lorg/icepdf/core/io/SeekableInput;->endThreadAccess()V

    .line 250
    .end local v2    # "data":[B
    .end local v4    # "out":Ljava/io/ByteArrayOutputStream;
    .end local v5    # "read":I
    .end local v6    # "position":J
    :goto_2
    return-object v0

    .line 231
    .restart local v4    # "out":Ljava/io/ByteArrayOutputStream;
    .restart local v5    # "read":I
    .restart local v6    # "position":J
    :cond_0
    :try_start_1
    invoke-virtual {v4, v5}, Ljava/io/ByteArrayOutputStream;->write(I)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 244
    .end local v4    # "out":Ljava/io/ByteArrayOutputStream;
    .end local v5    # "read":I
    .end local v6    # "position":J
    :catch_0
    move-exception v3

    .line 245
    .local v3, "ioe":Ljava/io/IOException;
    :try_start_2
    sget-object v8, Lorg/icepdf/core/util/Utils;->logger:Ljava/util/logging/Logger;

    sget-object v9, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string/jumbo v10, "Problem getting debug string"

    invoke-virtual {v8, v9, v10}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 248
    invoke-interface {p0}, Lorg/icepdf/core/io/SeekableInput;->endThreadAccess()V

    goto :goto_2

    .line 242
    .end local v3    # "ioe":Ljava/io/IOException;
    .restart local v2    # "data":[B
    .restart local v4    # "out":Ljava/io/ByteArrayOutputStream;
    .restart local v5    # "read":I
    .restart local v6    # "position":J
    :cond_1
    :try_start_3
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/lang/String;-><init>([B)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .end local v0    # "content":Ljava/lang/String;
    .local v1, "content":Ljava/lang/String;
    move-object v0, v1

    .end local v1    # "content":Ljava/lang/String;
    .restart local v0    # "content":Ljava/lang/String;
    goto :goto_1

    .line 248
    .end local v2    # "data":[B
    .end local v4    # "out":Ljava/io/ByteArrayOutputStream;
    .end local v5    # "read":I
    .end local v6    # "position":J
    :catchall_0
    move-exception v8

    invoke-interface {p0}, Lorg/icepdf/core/io/SeekableInput;->endThreadAccess()V

    throw v8
.end method

.method public static numBytesToHoldBits(I)I
    .locals 2
    .param p0, "numBits"    # I

    .prologue
    .line 303
    div-int/lit8 v0, p0, 0x8

    .line 304
    .local v0, "numBytes":I
    rem-int/lit8 v1, p0, 0x8

    if-lez v1, :cond_0

    .line 305
    add-int/lit8 v0, v0, 0x1

    .line 306
    :cond_0
    return v0
.end method

.method public static readIntWithVaryingBytesBE(Ljava/io/InputStream;I)I
    .locals 4
    .param p0, "in"    # Ljava/io/InputStream;
    .param p1, "numBytes"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 90
    const/4 v2, 0x0

    .line 91
    .local v2, "val":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, p1, :cond_1

    .line 92
    invoke-virtual {p0}, Ljava/io/InputStream;->read()I

    move-result v0

    .line 93
    .local v0, "curr":I
    if-gez v0, :cond_0

    .line 94
    new-instance v3, Ljava/io/EOFException;

    invoke-direct {v3}, Ljava/io/EOFException;-><init>()V

    throw v3

    .line 95
    :cond_0
    shl-int/lit8 v2, v2, 0x8

    .line 96
    and-int/lit16 v3, v0, 0xff

    or-int/2addr v2, v3

    .line 91
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 98
    .end local v0    # "curr":I
    :cond_1
    return v2
.end method

.method public static readLongWithVaryingBytesBE(Ljava/io/InputStream;I)J
    .locals 8
    .param p0, "in"    # Ljava/io/InputStream;
    .param p1, "numBytes"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 71
    const-wide/16 v2, 0x0

    .line 72
    .local v2, "val":J
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, p1, :cond_1

    .line 73
    invoke-virtual {p0}, Ljava/io/InputStream;->read()I

    move-result v0

    .line 74
    .local v0, "curr":I
    if-gez v0, :cond_0

    .line 75
    new-instance v4, Ljava/io/EOFException;

    invoke-direct {v4}, Ljava/io/EOFException;-><init>()V

    throw v4

    .line 76
    :cond_0
    const/16 v4, 0x8

    shl-long/2addr v2, v4

    .line 77
    int-to-long v4, v0

    const-wide/16 v6, 0xff

    and-long/2addr v4, v6

    or-long/2addr v2, v4

    .line 72
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 79
    .end local v0    # "curr":I
    :cond_1
    return-wide v2
.end method

.method public static reflectGraphicsEnvironmentISHeadlessInstance(Ljava/lang/Object;Z)Z
    .locals 7
    .param p0, "graphicsEnvironment"    # Ljava/lang/Object;
    .param p1, "defaultReturnIfNoMethod"    # Z

    .prologue
    .line 150
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    .line 151
    .local v0, "clazz":Ljava/lang/Class;
    const-string/jumbo v4, "isHeadlessInstance"

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Class;

    invoke-virtual {v0, v4, v5}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    .line 152
    .local v1, "isHeadlessInstanceMethod":Ljava/lang/reflect/Method;
    if-eqz v1, :cond_0

    .line 153
    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v1, p0, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .line 155
    .local v2, "ret":Ljava/lang/Object;
    instance-of v4, v2, Ljava/lang/Boolean;

    if-eqz v4, :cond_0

    .line 156
    check-cast v2, Ljava/lang/Boolean;

    .end local v2    # "ret":Ljava/lang/Object;
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result p1

    .line 163
    .end local v0    # "clazz":Ljava/lang/Class;
    .end local v1    # "isHeadlessInstanceMethod":Ljava/lang/reflect/Method;
    .end local p1    # "defaultReturnIfNoMethod":Z
    :cond_0
    :goto_0
    return p1

    .line 159
    .restart local p1    # "defaultReturnIfNoMethod":Z
    :catch_0
    move-exception v3

    .line 160
    .local v3, "t":Ljava/lang/Throwable;
    sget-object v4, Lorg/icepdf/core/util/Utils;->logger:Ljava/util/logging/Logger;

    sget-object v5, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string/jumbo v6, "ImageCache: Java 1.4 Headless support not found."

    invoke-virtual {v4, v5, v6}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static replaceInputStreamWithSeekableInput(Ljava/io/InputStream;)Lorg/icepdf/core/io/SeekableInput;
    .locals 9
    .param p0, "in"    # Ljava/io/InputStream;

    .prologue
    .line 254
    instance-of v6, p0, Lorg/icepdf/core/io/SeekableInput;

    if-eqz v6, :cond_0

    .line 255
    check-cast p0, Lorg/icepdf/core/io/SeekableInput;

    .line 287
    .end local p0    # "in":Ljava/io/InputStream;
    .local v4, "sin":Lorg/icepdf/core/io/SeekableInput;
    :goto_0
    return-object p0

    .line 257
    .end local v4    # "sin":Lorg/icepdf/core/io/SeekableInput;
    .restart local p0    # "in":Ljava/io/InputStream;
    :cond_0
    const/4 v4, 0x0

    .line 259
    .restart local v4    # "sin":Lorg/icepdf/core/io/SeekableInput;
    :try_start_0
    new-instance v2, Ljava/io/ByteArrayOutputStream;

    const/16 v6, 0x400

    invoke-direct {v2, v6}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    .line 272
    .local v2, "out":Ljava/io/ByteArrayOutputStream;
    :goto_1
    invoke-virtual {p0}, Ljava/io/InputStream;->read()I

    move-result v3

    .line 273
    .local v3, "read":I
    if-gez v3, :cond_1

    .line 278
    invoke-virtual {p0}, Ljava/io/InputStream;->close()V

    .line 279
    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->flush()V

    .line 280
    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->close()V

    .line 281
    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    .line 282
    .local v0, "data":[B
    new-instance v5, Lorg/icepdf/core/io/SeekableByteArrayInputStream;

    invoke-direct {v5, v0}, Lorg/icepdf/core/io/SeekableByteArrayInputStream;-><init>([B)V

    .end local v4    # "sin":Lorg/icepdf/core/io/SeekableInput;
    .local v5, "sin":Lorg/icepdf/core/io/SeekableInput;
    move-object v4, v5

    .end local v0    # "data":[B
    .end local v2    # "out":Ljava/io/ByteArrayOutputStream;
    .end local v3    # "read":I
    .end local v5    # "sin":Lorg/icepdf/core/io/SeekableInput;
    .restart local v4    # "sin":Lorg/icepdf/core/io/SeekableInput;
    :goto_2
    move-object p0, v4

    .line 287
    goto :goto_0

    .line 275
    .restart local v2    # "out":Ljava/io/ByteArrayOutputStream;
    .restart local v3    # "read":I
    :cond_1
    invoke-virtual {v2, v3}, Ljava/io/ByteArrayOutputStream;->write(I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 284
    .end local v2    # "out":Ljava/io/ByteArrayOutputStream;
    .end local v3    # "read":I
    :catch_0
    move-exception v1

    .line 285
    .local v1, "ioe":Ljava/io/IOException;
    sget-object v6, Lorg/icepdf/core/util/Utils;->logger:Ljava/util/logging/Logger;

    sget-object v7, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string/jumbo v8, "Problem getting debug string"

    invoke-virtual {v6, v7, v8}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    goto :goto_2
.end method

.method public static setIntIntoByteArrayBE(I[BI)V
    .locals 2
    .param p0, "value"    # I
    .param p1, "buffer"    # [B
    .param p2, "offset"    # I

    .prologue
    .line 44
    add-int/lit8 v0, p2, 0x0

    ushr-int/lit8 v1, p0, 0x18

    and-int/lit16 v1, v1, 0xff

    int-to-byte v1, v1

    aput-byte v1, p1, v0

    .line 45
    add-int/lit8 v0, p2, 0x1

    ushr-int/lit8 v1, p0, 0x10

    and-int/lit16 v1, v1, 0xff

    int-to-byte v1, v1

    aput-byte v1, p1, v0

    .line 46
    add-int/lit8 v0, p2, 0x2

    ushr-int/lit8 v1, p0, 0x8

    and-int/lit16 v1, v1, 0xff

    int-to-byte v1, v1

    aput-byte v1, p1, v0

    .line 47
    add-int/lit8 v0, p2, 0x3

    ushr-int/lit8 v1, p0, 0x0

    and-int/lit16 v1, v1, 0xff

    int-to-byte v1, v1

    aput-byte v1, p1, v0

    .line 48
    return-void
.end method

.method public static setShortIntoByteArrayBE(S[BI)V
    .locals 2
    .param p0, "value"    # S
    .param p1, "buffer"    # [B
    .param p2, "offset"    # I

    .prologue
    .line 59
    add-int/lit8 v0, p2, 0x0

    ushr-int/lit8 v1, p0, 0x8

    and-int/lit16 v1, v1, 0xff

    int-to-byte v1, v1

    aput-byte v1, p1, v0

    .line 60
    add-int/lit8 v0, p2, 0x1

    ushr-int/lit8 v1, p0, 0x0

    and-int/lit16 v1, v1, 0xff

    int-to-byte v1, v1

    aput-byte v1, p1, v0

    .line 61
    return-void
.end method
