.class public Lorg/icepdf/core/util/ColorUtil;
.super Ljava/lang/Object;
.source "ColorUtil.java"


# static fields
.field private static final colors:Ljava/util/HashMap;

.field private static final defaultColors:[I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const/16 v0, 0x93

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lorg/icepdf/core/util/ColorUtil;->defaultColors:[I

    .line 178
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lorg/icepdf/core/util/ColorUtil;->colors:Ljava/util/HashMap;

    return-void

    .line 28
    nop

    :array_0
    .array-data 4
        0xf0f8ff
        0xfaebd7
        0xffff
        0x7fffd4
        0xf0ffff
        0xf5f5dc
        0xffe4c4
        0x0
        0xffebcd
        0xff
        0x8a2be2
        0xa52a2a
        0xdeb887
        0x5f9ea0
        0x7fff00
        0xd2691e
        0xff7f50
        0x6495ed
        0xfff8dc
        0xdc143c
        0xffff
        0x8b
        0x8b8b
        0xb8860b
        0xa9a9a9
        0x6400
        0xa9a9a9
        0xbdb76b
        0x8b008b
        0x556b2f
        0xff8c00
        0x9932cc
        0x8b0000
        0xe9967a
        0x8fbc8f
        0x483d8b
        0x2f4f4f
        0x2f4f4f
        0xced1
        0x9400d3
        0xff1493
        0xbfff
        0x696969
        0x696969
        0x1e90ff
        0xb22222
        0xfffaf0
        0x228b22
        0xff00ff
        0xdcdcdc
        0xf8f8ff
        0xffd700
        0xdaa520
        0x808080
        0x808080
        0x8000
        0xadff2f
        0xf0fff0
        0xff69b4
        0xcd5c5c
        0x4b0082
        0xfffff0
        0xf0e68c
        0xe6e6fa
        0xfff0f5
        0x7cfc00
        0xfffacd
        0xadd8e6
        0xf08080
        0xe0ffff
        0xfafad2
        0xd3d3d3
        0x90ee90
        0xd3d3d3
        0xffb6c1
        0xffa07a
        0x20b2aa
        0x87cefa
        0x778899
        0x778899
        0xb0c4de
        0xffffe0
        0xff00
        0x32cd32
        0xfaf0e6
        0xff00ff
        0x800000
        0x66cdaa
        0xcd
        0xba55d3
        0x9370db
        0x3cb371
        0x7b68ee
        0xfa9a
        0x48d1cc
        0xc71585
        0x191970
        0xf5fffa
        0xffe4e1
        0xffe4b5    # 2.3500096E-38f
        0xffdead
        0x80
        0xfdf5e6
        0x808000
        0x6b8e23
        0xffa500
        0xff4500
        0xda70d6
        0xeee8aa
        0x98fb98
        0xafeeee
        0xdb7093
        0xffefd5
        0xffdab9
        0xcd853f
        0xffc0cb
        0xdda0dd
        0xb0e0e6
        0x800080
        0xff0000
        0xbc8f8f
        0x4169e1
        0x8b4513
        0xfa8072
        0xf4a460
        0x2e8b57
        0xfff5ee
        0xa0522d
        0xc0c0c0
        0x87ceeb
        0x6a5acd
        0x708090
        0x708090
        0xfffafa
        0xff7f
        0x4682b4
        0xd2b48c
        0x8080
        0xd8bfd8
        0xff6347
        0x40e0d0
        0xee82ee
        0xf5deb3
        0xffffff
        0xf5f5f5
        0xffff00
        0x9acd32
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static convertColor(Ljava/lang/String;)I
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 212
    :try_start_0
    const-string/jumbo v0, "#"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 213
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p0

    .line 215
    :cond_0
    const/16 v0, 0x10

    invoke-static {p0, v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 220
    :goto_0
    return v0

    .line 216
    :catch_0
    move-exception v0

    .line 220
    invoke-static {p0}, Lorg/icepdf/core/util/ColorUtil;->convertNamedColor(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method public static final convertColorNameToRGB(Ljava/lang/String;)Ljava/lang/String;
    .locals 8
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x7

    const/4 v6, 0x0

    .line 189
    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p0, v5}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lorg/icepdf/core/util/ColorUtil;->convertNamedColor(Ljava/lang/String;)I

    move-result v1

    .line 190
    .local v1, "c":I
    if-ltz v1, :cond_2

    .line 192
    new-array v0, v7, [C

    .line 193
    .local v0, "buf":[C
    const/16 v5, 0x23

    aput-char v5, v0, v6

    .line 194
    const/4 v3, 0x1

    .local v3, "pos":I
    const/16 v4, 0x14

    .local v4, "shift":I
    :goto_0
    if-ltz v4, :cond_1

    .line 195
    shr-int v5, v1, v4

    and-int/lit8 v2, v5, 0xf

    .line 196
    .local v2, "d":I
    const/16 v5, 0xa

    if-ge v2, v5, :cond_0

    add-int/lit8 v5, v2, 0x30

    :goto_1
    int-to-char v5, v5

    aput-char v5, v0, v3

    .line 194
    add-int/lit8 v3, v3, 0x1

    add-int/lit8 v4, v4, -0x4

    goto :goto_0

    .line 196
    :cond_0
    add-int/lit8 v5, v2, 0x41

    add-int/lit8 v5, v5, -0xa

    goto :goto_1

    .line 198
    .end local v2    # "d":I
    :cond_1
    new-instance p0, Ljava/lang/String;

    .end local p0    # "name":Ljava/lang/String;
    invoke-direct {p0, v0, v6, v7}, Ljava/lang/String;-><init>([CII)V

    .line 200
    .end local v0    # "buf":[C
    .end local v3    # "pos":I
    .end local v4    # "shift":I
    .restart local p0    # "name":Ljava/lang/String;
    :cond_2
    return-object p0
.end method

.method public static final convertNamedColor(Ljava/lang/String;)I
    .locals 3
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 233
    invoke-static {p0}, Lorg/icepdf/core/util/ColorUtil;->getDefaultColorIndex(Ljava/lang/String;)I

    move-result v1

    .line 235
    .local v1, "index":I
    if-ltz v1, :cond_0

    .line 236
    sget-object v2, Lorg/icepdf/core/util/ColorUtil;->defaultColors:[I

    aget v2, v2, v1

    .line 244
    :goto_0
    return v2

    .line 239
    :cond_0
    sget-object v2, Lorg/icepdf/core/util/ColorUtil;->colors:Ljava/util/HashMap;

    invoke-virtual {v2, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 240
    .local v0, "ii":Ljava/lang/Integer;
    if-eqz v0, :cond_1

    .line 241
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    goto :goto_0

    .line 244
    :cond_1
    const/4 v2, -0x1

    goto :goto_0
.end method

.method private static final getDefaultColorIndex(Ljava/lang/String;)I
    .locals 154
    .param p0, "s"    # Ljava/lang/String;

    .prologue
    .line 254
    const/4 v2, 0x1

    .line 255
    .local v2, "Id_aliceblue":I
    const/4 v3, 0x2

    .line 256
    .local v3, "Id_antiquewhite":I
    const/4 v4, 0x3

    .line 257
    .local v4, "Id_aqua":I
    const/4 v5, 0x4

    .line 258
    .local v5, "Id_aquamarine":I
    const/4 v6, 0x5

    .line 259
    .local v6, "Id_azure":I
    const/4 v7, 0x6

    .line 260
    .local v7, "Id_beige":I
    const/4 v8, 0x7

    .line 261
    .local v8, "Id_bisque":I
    const/16 v9, 0x8

    .line 262
    .local v9, "Id_black":I
    const/16 v10, 0x9

    .line 263
    .local v10, "Id_blanchedalmond":I
    const/16 v11, 0xa

    .line 264
    .local v11, "Id_blue":I
    const/16 v12, 0xb

    .line 265
    .local v12, "Id_blueviolet":I
    const/16 v13, 0xc

    .line 266
    .local v13, "Id_brown":I
    const/16 v14, 0xd

    .line 267
    .local v14, "Id_burlywood":I
    const/16 v15, 0xe

    .line 268
    .local v15, "Id_cadetblue":I
    const/16 v16, 0xf

    .line 269
    .local v16, "Id_chartreuse":I
    const/16 v17, 0x10

    .line 270
    .local v17, "Id_chocolate":I
    const/16 v18, 0x11

    .line 271
    .local v18, "Id_coral":I
    const/16 v19, 0x12

    .line 272
    .local v19, "Id_cornflowerblue":I
    const/16 v20, 0x13

    .line 273
    .local v20, "Id_cornsilk":I
    const/16 v21, 0x14

    .line 274
    .local v21, "Id_crimson":I
    const/16 v22, 0x15

    .line 275
    .local v22, "Id_cyan":I
    const/16 v23, 0x16

    .line 276
    .local v23, "Id_darkblue":I
    const/16 v24, 0x17

    .line 277
    .local v24, "Id_darkcyan":I
    const/16 v25, 0x18

    .line 278
    .local v25, "Id_darkgoldenrod":I
    const/16 v26, 0x19

    .line 279
    .local v26, "Id_darkgray":I
    const/16 v27, 0x1a

    .line 280
    .local v27, "Id_darkgreen":I
    const/16 v28, 0x1b

    .line 281
    .local v28, "Id_darkgrey":I
    const/16 v29, 0x1c

    .line 282
    .local v29, "Id_darkkhaki":I
    const/16 v30, 0x1d

    .line 283
    .local v30, "Id_darkmagenta":I
    const/16 v31, 0x1e

    .line 284
    .local v31, "Id_darkolivegreen":I
    const/16 v32, 0x1f

    .line 285
    .local v32, "Id_darkorange":I
    const/16 v33, 0x20

    .line 286
    .local v33, "Id_darkorchid":I
    const/16 v34, 0x21

    .line 287
    .local v34, "Id_darkred":I
    const/16 v35, 0x22

    .line 288
    .local v35, "Id_darksalmon":I
    const/16 v36, 0x23

    .line 289
    .local v36, "Id_darkseagreen":I
    const/16 v37, 0x24

    .line 290
    .local v37, "Id_darkslateblue":I
    const/16 v38, 0x25

    .line 291
    .local v38, "Id_darkslategray":I
    const/16 v39, 0x26

    .line 292
    .local v39, "Id_darkslategrey":I
    const/16 v40, 0x27

    .line 293
    .local v40, "Id_darkturquoise":I
    const/16 v41, 0x28

    .line 294
    .local v41, "Id_darkviolet":I
    const/16 v42, 0x29

    .line 295
    .local v42, "Id_deeppink":I
    const/16 v43, 0x2a

    .line 296
    .local v43, "Id_deepskyblue":I
    const/16 v44, 0x2b

    .line 297
    .local v44, "Id_dimgray":I
    const/16 v45, 0x2c

    .line 298
    .local v45, "Id_dimgrey":I
    const/16 v46, 0x2d

    .line 299
    .local v46, "Id_dodgerblue":I
    const/16 v47, 0x2e

    .line 300
    .local v47, "Id_firebrick":I
    const/16 v48, 0x2f

    .line 301
    .local v48, "Id_floralwhite":I
    const/16 v49, 0x30

    .line 302
    .local v49, "Id_forestgreen":I
    const/16 v50, 0x31

    .line 303
    .local v50, "Id_fuchsia":I
    const/16 v51, 0x32

    .line 304
    .local v51, "Id_gainsboro":I
    const/16 v52, 0x33

    .line 305
    .local v52, "Id_ghostwhite":I
    const/16 v53, 0x34

    .line 306
    .local v53, "Id_gold":I
    const/16 v54, 0x35

    .line 307
    .local v54, "Id_goldenrod":I
    const/16 v55, 0x36

    .line 308
    .local v55, "Id_gray":I
    const/16 v58, 0x37

    .line 309
    .local v58, "Id_grey":I
    const/16 v56, 0x38

    .line 310
    .local v56, "Id_green":I
    const/16 v57, 0x39

    .line 311
    .local v57, "Id_greenyellow":I
    const/16 v59, 0x3a

    .line 312
    .local v59, "Id_honeydew":I
    const/16 v60, 0x3b

    .line 313
    .local v60, "Id_hotpink":I
    const/16 v61, 0x3c

    .line 314
    .local v61, "Id_indianred":I
    const/16 v62, 0x3d

    .line 315
    .local v62, "Id_indigo":I
    const/16 v63, 0x3e

    .line 316
    .local v63, "Id_ivory":I
    const/16 v64, 0x3f

    .line 317
    .local v64, "Id_khaki":I
    const/16 v65, 0x40

    .line 318
    .local v65, "Id_lavender":I
    const/16 v66, 0x41

    .line 319
    .local v66, "Id_lavenderblush":I
    const/16 v67, 0x42

    .line 320
    .local v67, "Id_lawngreen":I
    const/16 v68, 0x43

    .line 321
    .local v68, "Id_lemonchiffon":I
    const/16 v69, 0x44

    .line 322
    .local v69, "Id_lightblue":I
    const/16 v70, 0x45

    .line 323
    .local v70, "Id_lightcoral":I
    const/16 v71, 0x46

    .line 324
    .local v71, "Id_lightcyan":I
    const/16 v72, 0x47

    .line 325
    .local v72, "Id_lightgoldenrodyellow":I
    const/16 v73, 0x48

    .line 326
    .local v73, "Id_lightgray":I
    const/16 v74, 0x49

    .line 327
    .local v74, "Id_lightgreen":I
    const/16 v75, 0x4a

    .line 328
    .local v75, "Id_lightgrey":I
    const/16 v76, 0x4b

    .line 329
    .local v76, "Id_lightpink":I
    const/16 v77, 0x4c

    .line 330
    .local v77, "Id_lightsalmon":I
    const/16 v78, 0x4d

    .line 331
    .local v78, "Id_lightseagreen":I
    const/16 v79, 0x4e

    .line 332
    .local v79, "Id_lightskyblue":I
    const/16 v80, 0x4f

    .line 333
    .local v80, "Id_lightslategray":I
    const/16 v81, 0x50

    .line 334
    .local v81, "Id_lightslategrey":I
    const/16 v82, 0x51

    .line 335
    .local v82, "Id_lightsteelblue":I
    const/16 v83, 0x52

    .line 336
    .local v83, "Id_lightyellow":I
    const/16 v84, 0x53

    .line 337
    .local v84, "Id_lime":I
    const/16 v85, 0x54

    .line 338
    .local v85, "Id_limegreen":I
    const/16 v86, 0x55

    .line 339
    .local v86, "Id_linen":I
    const/16 v87, 0x56

    .line 340
    .local v87, "Id_magenta":I
    const/16 v88, 0x57

    .line 341
    .local v88, "Id_maroon":I
    const/16 v89, 0x58

    .line 342
    .local v89, "Id_mediumaquamarine":I
    const/16 v90, 0x59

    .line 343
    .local v90, "Id_mediumblue":I
    const/16 v91, 0x5a

    .line 344
    .local v91, "Id_mediumorchid":I
    const/16 v92, 0x5b

    .line 345
    .local v92, "Id_mediumpurple":I
    const/16 v93, 0x5c

    .line 346
    .local v93, "Id_mediumseagreen":I
    const/16 v94, 0x5d

    .line 347
    .local v94, "Id_mediumslateblue":I
    const/16 v95, 0x5e

    .line 348
    .local v95, "Id_mediumspringgreen":I
    const/16 v96, 0x5f

    .line 349
    .local v96, "Id_mediumturquoise":I
    const/16 v97, 0x60

    .line 350
    .local v97, "Id_mediumvioletred":I
    const/16 v98, 0x61

    .line 351
    .local v98, "Id_midnightblue":I
    const/16 v99, 0x62

    .line 352
    .local v99, "Id_mintcream":I
    const/16 v100, 0x63

    .line 353
    .local v100, "Id_mistyrose":I
    const/16 v101, 0x64

    .line 354
    .local v101, "Id_moccasin":I
    const/16 v102, 0x65

    .line 355
    .local v102, "Id_navajowhite":I
    const/16 v103, 0x66

    .line 356
    .local v103, "Id_navy":I
    const/16 v104, 0x67

    .line 357
    .local v104, "Id_oldlace":I
    const/16 v105, 0x68

    .line 358
    .local v105, "Id_olive":I
    const/16 v106, 0x69

    .line 359
    .local v106, "Id_olivedrab":I
    const/16 v107, 0x6a

    .line 360
    .local v107, "Id_orange":I
    const/16 v108, 0x6b

    .line 361
    .local v108, "Id_orangered":I
    const/16 v109, 0x6c

    .line 362
    .local v109, "Id_orchid":I
    const/16 v110, 0x6d

    .line 363
    .local v110, "Id_palegoldenrod":I
    const/16 v111, 0x6e

    .line 364
    .local v111, "Id_palegreen":I
    const/16 v112, 0x6f

    .line 365
    .local v112, "Id_paleturquoise":I
    const/16 v113, 0x70

    .line 366
    .local v113, "Id_palevioletred":I
    const/16 v114, 0x71

    .line 367
    .local v114, "Id_papayawhip":I
    const/16 v115, 0x72

    .line 368
    .local v115, "Id_peachpuff":I
    const/16 v116, 0x73

    .line 369
    .local v116, "Id_peru":I
    const/16 v117, 0x74

    .line 370
    .local v117, "Id_pink":I
    const/16 v118, 0x75

    .line 371
    .local v118, "Id_plum":I
    const/16 v119, 0x76

    .line 372
    .local v119, "Id_powderblue":I
    const/16 v120, 0x77

    .line 373
    .local v120, "Id_purple":I
    const/16 v121, 0x78

    .line 374
    .local v121, "Id_red":I
    const/16 v122, 0x79

    .line 375
    .local v122, "Id_rosybrown":I
    const/16 v123, 0x7a

    .line 376
    .local v123, "Id_royalblue":I
    const/16 v124, 0x7b

    .line 377
    .local v124, "Id_saddlebrown":I
    const/16 v125, 0x7c

    .line 378
    .local v125, "Id_salmon":I
    const/16 v126, 0x7d

    .line 379
    .local v126, "Id_sandybrown":I
    const/16 v127, 0x7e

    .line 380
    .local v127, "Id_seagreen":I
    const/16 v128, 0x7f

    .line 381
    .local v128, "Id_seashell":I
    const/16 v129, 0x80

    .line 382
    .local v129, "Id_sienna":I
    const/16 v130, 0x81

    .line 383
    .local v130, "Id_silver":I
    const/16 v131, 0x82

    .line 384
    .local v131, "Id_skyblue":I
    const/16 v132, 0x83

    .line 385
    .local v132, "Id_slateblue":I
    const/16 v133, 0x84

    .line 386
    .local v133, "Id_slategray":I
    const/16 v134, 0x85

    .line 387
    .local v134, "Id_slategrey":I
    const/16 v135, 0x86

    .line 388
    .local v135, "Id_snow":I
    const/16 v136, 0x87

    .line 389
    .local v136, "Id_springgreen":I
    const/16 v137, 0x88

    .line 390
    .local v137, "Id_steelblue":I
    const/16 v138, 0x89

    .line 391
    .local v138, "Id_tan":I
    const/16 v139, 0x8a

    .line 392
    .local v139, "Id_teal":I
    const/16 v140, 0x8b

    .line 393
    .local v140, "Id_thistle":I
    const/16 v141, 0x8c

    .line 394
    .local v141, "Id_tomato":I
    const/16 v142, 0x8d

    .line 395
    .local v142, "Id_turquoise":I
    const/16 v143, 0x8e

    .line 396
    .local v143, "Id_violet":I
    const/16 v144, 0x8f

    .line 397
    .local v144, "Id_wheat":I
    const/16 v145, 0x90

    .line 398
    .local v145, "Id_white":I
    const/16 v146, 0x91

    .line 399
    .local v146, "Id_whitesmoke":I
    const/16 v147, 0x92

    .line 400
    .local v147, "Id_yellow":I
    const/16 v148, 0x93

    .line 409
    .local v148, "Id_yellowgreen":I
    const/16 v151, 0x0

    .line 410
    .local v151, "id":I
    const/16 v149, 0x0

    .line 413
    .local v149, "X":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Ljava/lang/String;->length()I

    move-result v152

    packed-switch v152, :pswitch_data_0

    .line 1117
    :cond_0
    :goto_0
    :pswitch_0
    if-eqz v149, :cond_1

    move-object/from16 v0, v149

    move-object/from16 v1, p0

    if-eq v0, v1, :cond_1

    move-object/from16 v0, v149

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v152

    if-nez v152, :cond_1

    const/16 v151, 0x0

    .line 1121
    :cond_1
    :goto_1
    add-int/lit8 v152, v151, -0x1

    return v152

    .line 415
    :pswitch_1
    const/16 v152, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v152

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v150

    .line 416
    .local v150, "c":I
    const/16 v152, 0x72

    move/from16 v0, v150

    move/from16 v1, v152

    if-ne v0, v1, :cond_2

    .line 417
    const/16 v152, 0x2

    move-object/from16 v0, p0

    move/from16 v1, v152

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v152

    const/16 v153, 0x64

    move/from16 v0, v152

    move/from16 v1, v153

    if-ne v0, v1, :cond_0

    const/16 v152, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v152

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v152

    const/16 v153, 0x65

    move/from16 v0, v152

    move/from16 v1, v153

    if-ne v0, v1, :cond_0

    .line 418
    const/16 v151, 0x78

    .line 419
    goto :goto_1

    .line 421
    :cond_2
    const/16 v152, 0x74

    move/from16 v0, v150

    move/from16 v1, v152

    if-ne v0, v1, :cond_0

    .line 422
    const/16 v152, 0x2

    move-object/from16 v0, p0

    move/from16 v1, v152

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v152

    const/16 v153, 0x6e

    move/from16 v0, v152

    move/from16 v1, v153

    if-ne v0, v1, :cond_0

    const/16 v152, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v152

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v152

    const/16 v153, 0x61

    move/from16 v0, v152

    move/from16 v1, v153

    if-ne v0, v1, :cond_0

    .line 423
    const/16 v151, 0x89

    .line 424
    goto :goto_1

    .line 429
    .end local v150    # "c":I
    :pswitch_2
    const/16 v152, 0x3

    move-object/from16 v0, p0

    move/from16 v1, v152

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v152

    sparse-switch v152, :sswitch_data_0

    goto/16 :goto_0

    .line 431
    :sswitch_0
    const-string/jumbo v149, "aqua"

    .line 432
    const/16 v151, 0x3

    .line 433
    goto/16 :goto_0

    .line 435
    :sswitch_1
    const-string/jumbo v149, "gold"

    .line 436
    const/16 v151, 0x34

    .line 437
    goto/16 :goto_0

    .line 439
    :sswitch_2
    const/16 v152, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v152

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v150

    .line 440
    .restart local v150    # "c":I
    const/16 v152, 0x62

    move/from16 v0, v150

    move/from16 v1, v152

    if-ne v0, v1, :cond_3

    .line 441
    const/16 v152, 0x2

    move-object/from16 v0, p0

    move/from16 v1, v152

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v152

    const/16 v153, 0x75

    move/from16 v0, v152

    move/from16 v1, v153

    if-ne v0, v1, :cond_0

    const/16 v152, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v152

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v152

    const/16 v153, 0x6c

    move/from16 v0, v152

    move/from16 v1, v153

    if-ne v0, v1, :cond_0

    .line 442
    const/16 v151, 0xa

    .line 443
    goto/16 :goto_1

    .line 445
    :cond_3
    const/16 v152, 0x6c

    move/from16 v0, v150

    move/from16 v1, v152

    if-ne v0, v1, :cond_0

    .line 446
    const/16 v152, 0x2

    move-object/from16 v0, p0

    move/from16 v1, v152

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v152

    const/16 v153, 0x6d

    move/from16 v0, v152

    move/from16 v1, v153

    if-ne v0, v1, :cond_0

    const/16 v152, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v152

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v152

    const/16 v153, 0x69

    move/from16 v0, v152

    move/from16 v1, v153

    if-ne v0, v1, :cond_0

    .line 447
    const/16 v151, 0x53

    .line 448
    goto/16 :goto_1

    .line 453
    .end local v150    # "c":I
    :sswitch_3
    const-string/jumbo v149, "pink"

    .line 454
    const/16 v151, 0x74

    .line 455
    goto/16 :goto_0

    .line 457
    :sswitch_4
    const-string/jumbo v149, "teal"

    .line 458
    const/16 v151, 0x8a

    .line 459
    goto/16 :goto_0

    .line 461
    :sswitch_5
    const-string/jumbo v149, "plum"

    .line 462
    const/16 v151, 0x75

    .line 463
    goto/16 :goto_0

    .line 465
    :sswitch_6
    const-string/jumbo v149, "cyan"

    .line 466
    const/16 v151, 0x15

    .line 467
    goto/16 :goto_0

    .line 469
    :sswitch_7
    const-string/jumbo v149, "peru"

    .line 470
    const/16 v151, 0x73

    .line 471
    goto/16 :goto_0

    .line 473
    :sswitch_8
    const-string/jumbo v149, "snow"

    .line 474
    const/16 v151, 0x86

    .line 475
    goto/16 :goto_0

    .line 477
    :sswitch_9
    const/16 v152, 0x2

    move-object/from16 v0, p0

    move/from16 v1, v152

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v150

    .line 478
    .restart local v150    # "c":I
    const/16 v152, 0x61

    move/from16 v0, v150

    move/from16 v1, v152

    if-ne v0, v1, :cond_4

    .line 479
    const/16 v152, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v152

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v152

    const/16 v153, 0x67

    move/from16 v0, v152

    move/from16 v1, v153

    if-ne v0, v1, :cond_0

    const/16 v152, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v152

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v152

    const/16 v153, 0x72

    move/from16 v0, v152

    move/from16 v1, v153

    if-ne v0, v1, :cond_0

    .line 480
    const/16 v151, 0x36

    .line 481
    goto/16 :goto_1

    .line 483
    :cond_4
    const/16 v152, 0x65

    move/from16 v0, v150

    move/from16 v1, v152

    if-ne v0, v1, :cond_5

    .line 484
    const/16 v152, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v152

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v152

    const/16 v153, 0x67

    move/from16 v0, v152

    move/from16 v1, v153

    if-ne v0, v1, :cond_0

    const/16 v152, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v152

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v152

    const/16 v153, 0x72

    move/from16 v0, v152

    move/from16 v1, v153

    if-ne v0, v1, :cond_0

    .line 485
    const/16 v151, 0x37

    .line 486
    goto/16 :goto_1

    .line 488
    :cond_5
    const/16 v152, 0x76

    move/from16 v0, v150

    move/from16 v1, v152

    if-ne v0, v1, :cond_0

    .line 489
    const/16 v152, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v152

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v152

    const/16 v153, 0x6e

    move/from16 v0, v152

    move/from16 v1, v153

    if-ne v0, v1, :cond_0

    const/16 v152, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v152

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v152

    const/16 v153, 0x61

    move/from16 v0, v152

    move/from16 v1, v153

    if-ne v0, v1, :cond_0

    .line 490
    const/16 v151, 0x66

    .line 491
    goto/16 :goto_1

    .line 498
    .end local v150    # "c":I
    :pswitch_3
    const/16 v152, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v152

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v152

    sparse-switch v152, :sswitch_data_1

    goto/16 :goto_0

    .line 500
    :sswitch_a
    const-string/jumbo v149, "azure"

    .line 501
    const/16 v151, 0x5

    .line 502
    goto/16 :goto_0

    .line 504
    :sswitch_b
    const/16 v152, 0x4

    move-object/from16 v0, p0

    move/from16 v1, v152

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v150

    .line 505
    .restart local v150    # "c":I
    const/16 v152, 0x65

    move/from16 v0, v150

    move/from16 v1, v152

    if-ne v0, v1, :cond_6

    .line 506
    const-string/jumbo v149, "beige"

    .line 507
    const/16 v151, 0x6

    goto/16 :goto_0

    .line 508
    :cond_6
    const/16 v152, 0x6b

    move/from16 v0, v150

    move/from16 v1, v152

    if-ne v0, v1, :cond_7

    .line 509
    const-string/jumbo v149, "black"

    .line 510
    const/16 v151, 0x8

    goto/16 :goto_0

    .line 511
    :cond_7
    const/16 v152, 0x6e

    move/from16 v0, v150

    move/from16 v1, v152

    if-ne v0, v1, :cond_0

    .line 512
    const-string/jumbo v149, "brown"

    .line 513
    const/16 v151, 0xc

    goto/16 :goto_0

    .line 517
    .end local v150    # "c":I
    :sswitch_c
    const-string/jumbo v149, "coral"

    .line 518
    const/16 v151, 0x11

    .line 519
    goto/16 :goto_0

    .line 521
    :sswitch_d
    const-string/jumbo v149, "green"

    .line 522
    const/16 v151, 0x38

    .line 523
    goto/16 :goto_0

    .line 525
    :sswitch_e
    const-string/jumbo v149, "ivory"

    .line 526
    const/16 v151, 0x3e

    .line 527
    goto/16 :goto_0

    .line 529
    :sswitch_f
    const-string/jumbo v149, "khaki"

    .line 530
    const/16 v151, 0x3f

    .line 531
    goto/16 :goto_0

    .line 533
    :sswitch_10
    const-string/jumbo v149, "linen"

    .line 534
    const/16 v151, 0x55

    .line 535
    goto/16 :goto_0

    .line 537
    :sswitch_11
    const-string/jumbo v149, "olive"

    .line 538
    const/16 v151, 0x68

    .line 539
    goto/16 :goto_0

    .line 541
    :sswitch_12
    const/16 v152, 0x4

    move-object/from16 v0, p0

    move/from16 v1, v152

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v150

    .line 542
    .restart local v150    # "c":I
    const/16 v152, 0x65

    move/from16 v0, v150

    move/from16 v1, v152

    if-ne v0, v1, :cond_8

    .line 543
    const-string/jumbo v149, "white"

    .line 544
    const/16 v151, 0x90

    goto/16 :goto_0

    .line 545
    :cond_8
    const/16 v152, 0x74

    move/from16 v0, v150

    move/from16 v1, v152

    if-ne v0, v1, :cond_0

    .line 546
    const-string/jumbo v149, "wheat"

    .line 547
    const/16 v151, 0x8f

    goto/16 :goto_0

    .line 553
    .end local v150    # "c":I
    :pswitch_4
    const/16 v152, 0x3

    move-object/from16 v0, p0

    move/from16 v1, v152

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v152

    packed-switch v152, :pswitch_data_1

    :pswitch_5
    goto/16 :goto_0

    .line 555
    :pswitch_6
    const-string/jumbo v149, "tomato"

    .line 556
    const/16 v151, 0x8c

    .line 557
    goto/16 :goto_0

    .line 559
    :pswitch_7
    const-string/jumbo v149, "orchid"

    .line 560
    const/16 v151, 0x6c

    .line 561
    goto/16 :goto_0

    .line 563
    :pswitch_8
    const-string/jumbo v149, "indigo"

    .line 564
    const/16 v151, 0x3d

    .line 565
    goto/16 :goto_0

    .line 567
    :pswitch_9
    const/16 v152, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v152

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v150

    .line 568
    .restart local v150    # "c":I
    const/16 v152, 0x76

    move/from16 v0, v150

    move/from16 v1, v152

    if-ne v0, v1, :cond_9

    .line 569
    const-string/jumbo v149, "violet"

    .line 570
    const/16 v151, 0x8e

    goto/16 :goto_0

    .line 571
    :cond_9
    const/16 v152, 0x79

    move/from16 v0, v150

    move/from16 v1, v152

    if-ne v0, v1, :cond_0

    .line 572
    const-string/jumbo v149, "yellow"

    .line 573
    const/16 v151, 0x92

    goto/16 :goto_0

    .line 577
    .end local v150    # "c":I
    :pswitch_a
    const-string/jumbo v149, "salmon"

    .line 578
    const/16 v151, 0x7c

    .line 579
    goto/16 :goto_0

    .line 581
    :pswitch_b
    const/16 v152, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v152

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v150

    .line 582
    .restart local v150    # "c":I
    const/16 v152, 0x6f

    move/from16 v0, v150

    move/from16 v1, v152

    if-ne v0, v1, :cond_a

    .line 583
    const-string/jumbo v149, "orange"

    .line 584
    const/16 v151, 0x6a

    goto/16 :goto_0

    .line 585
    :cond_a
    const/16 v152, 0x73

    move/from16 v0, v150

    move/from16 v1, v152

    if-ne v0, v1, :cond_0

    .line 586
    const-string/jumbo v149, "sienna"

    .line 587
    const/16 v151, 0x80

    goto/16 :goto_0

    .line 591
    .end local v150    # "c":I
    :pswitch_c
    const-string/jumbo v149, "maroon"

    .line 592
    const/16 v151, 0x57

    .line 593
    goto/16 :goto_0

    .line 595
    :pswitch_d
    const-string/jumbo v149, "purple"

    .line 596
    const/16 v151, 0x77

    .line 597
    goto/16 :goto_0

    .line 599
    :pswitch_e
    const-string/jumbo v149, "bisque"

    .line 600
    const/16 v151, 0x7

    .line 601
    goto/16 :goto_0

    .line 603
    :pswitch_f
    const-string/jumbo v149, "silver"

    .line 604
    const/16 v151, 0x81

    .line 605
    goto/16 :goto_0

    .line 609
    :pswitch_10
    const/16 v152, 0x3

    move-object/from16 v0, p0

    move/from16 v1, v152

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v152

    packed-switch v152, :pswitch_data_2

    :pswitch_11
    goto/16 :goto_0

    .line 611
    :pswitch_12
    const-string/jumbo v149, "skyblue"

    .line 612
    const/16 v151, 0x82

    .line 613
    goto/16 :goto_0

    .line 615
    :pswitch_13
    const-string/jumbo v149, "magenta"

    .line 616
    const/16 v151, 0x56

    .line 617
    goto/16 :goto_0

    .line 619
    :pswitch_14
    const/16 v152, 0x5

    move-object/from16 v0, p0

    move/from16 v1, v152

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v150

    .line 620
    .restart local v150    # "c":I
    const/16 v152, 0x61

    move/from16 v0, v150

    move/from16 v1, v152

    if-ne v0, v1, :cond_b

    .line 621
    const-string/jumbo v149, "dimgray"

    .line 622
    const/16 v151, 0x2b

    goto/16 :goto_0

    .line 623
    :cond_b
    const/16 v152, 0x65

    move/from16 v0, v150

    move/from16 v1, v152

    if-ne v0, v1, :cond_0

    .line 624
    const-string/jumbo v149, "dimgrey"

    .line 625
    const/16 v151, 0x2c

    goto/16 :goto_0

    .line 629
    .end local v150    # "c":I
    :pswitch_15
    const-string/jumbo v149, "fuchsia"

    .line 630
    const/16 v151, 0x31

    .line 631
    goto/16 :goto_0

    .line 633
    :pswitch_16
    const-string/jumbo v149, "darkred"

    .line 634
    const/16 v151, 0x21

    .line 635
    goto/16 :goto_0

    .line 637
    :pswitch_17
    const-string/jumbo v149, "oldlace"

    .line 638
    const/16 v151, 0x67

    .line 639
    goto/16 :goto_0

    .line 641
    :pswitch_18
    const-string/jumbo v149, "crimson"

    .line 642
    const/16 v151, 0x14

    .line 643
    goto/16 :goto_0

    .line 645
    :pswitch_19
    const-string/jumbo v149, "hotpink"

    .line 646
    const/16 v151, 0x3b

    .line 647
    goto/16 :goto_0

    .line 649
    :pswitch_1a
    const-string/jumbo v149, "thistle"

    .line 650
    const/16 v151, 0x8b

    .line 651
    goto/16 :goto_0

    .line 655
    :pswitch_1b
    const/16 v152, 0x4

    move-object/from16 v0, p0

    move/from16 v1, v152

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v152

    sparse-switch v152, :sswitch_data_2

    goto/16 :goto_0

    .line 657
    :sswitch_13
    const-string/jumbo v149, "moccasin"

    .line 658
    const/16 v151, 0x64

    .line 659
    goto/16 :goto_0

    .line 661
    :sswitch_14
    const-string/jumbo v149, "darkblue"

    .line 662
    const/16 v151, 0x16

    .line 663
    goto/16 :goto_0

    .line 665
    :sswitch_15
    const-string/jumbo v149, "darkcyan"

    .line 666
    const/16 v151, 0x17

    .line 667
    goto/16 :goto_0

    .line 669
    :sswitch_16
    const/16 v152, 0x6

    move-object/from16 v0, p0

    move/from16 v1, v152

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v150

    .line 670
    .restart local v150    # "c":I
    const/16 v152, 0x61

    move/from16 v0, v150

    move/from16 v1, v152

    if-ne v0, v1, :cond_c

    .line 671
    const-string/jumbo v149, "darkgray"

    .line 672
    const/16 v151, 0x19

    goto/16 :goto_0

    .line 673
    :cond_c
    const/16 v152, 0x65

    move/from16 v0, v150

    move/from16 v1, v152

    if-ne v0, v1, :cond_0

    .line 674
    const-string/jumbo v149, "darkgrey"

    .line 675
    const/16 v151, 0x1b

    goto/16 :goto_0

    .line 679
    .end local v150    # "c":I
    :sswitch_17
    const-string/jumbo v149, "seashell"

    .line 680
    const/16 v151, 0x7f

    .line 681
    goto/16 :goto_0

    .line 683
    :sswitch_18
    const-string/jumbo v149, "lavender"

    .line 684
    const/16 v151, 0x40

    .line 685
    goto/16 :goto_0

    .line 687
    :sswitch_19
    const-string/jumbo v149, "deeppink"

    .line 688
    const/16 v151, 0x29

    .line 689
    goto/16 :goto_0

    .line 691
    :sswitch_1a
    const-string/jumbo v149, "seagreen"

    .line 692
    const/16 v151, 0x7e

    .line 693
    goto/16 :goto_0

    .line 695
    :sswitch_1b
    const-string/jumbo v149, "cornsilk"

    .line 696
    const/16 v151, 0x13

    .line 697
    goto/16 :goto_0

    .line 699
    :sswitch_1c
    const-string/jumbo v149, "honeydew"

    .line 700
    const/16 v151, 0x3a

    .line 701
    goto/16 :goto_0

    .line 705
    :pswitch_1c
    const/16 v152, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v152

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v152

    packed-switch v152, :pswitch_data_3

    :pswitch_1d
    goto/16 :goto_0

    .line 707
    :pswitch_1e
    const-string/jumbo v149, "aliceblue"

    .line 708
    const/16 v151, 0x1

    .line 709
    goto/16 :goto_0

    .line 711
    :pswitch_1f
    const-string/jumbo v149, "burlywood"

    .line 712
    const/16 v151, 0xd

    .line 713
    goto/16 :goto_0

    .line 715
    :pswitch_20
    const/16 v152, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v152

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v150

    .line 716
    .restart local v150    # "c":I
    const/16 v152, 0x61

    move/from16 v0, v150

    move/from16 v1, v152

    if-ne v0, v1, :cond_d

    .line 717
    const-string/jumbo v149, "cadetblue"

    .line 718
    const/16 v151, 0xe

    goto/16 :goto_0

    .line 719
    :cond_d
    const/16 v152, 0x68

    move/from16 v0, v150

    move/from16 v1, v152

    if-ne v0, v1, :cond_0

    .line 720
    const-string/jumbo v149, "chocolate"

    .line 721
    const/16 v151, 0x10

    goto/16 :goto_0

    .line 725
    .end local v150    # "c":I
    :pswitch_21
    const/16 v152, 0x8

    move-object/from16 v0, p0

    move/from16 v1, v152

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v150

    .line 726
    .restart local v150    # "c":I
    const/16 v152, 0x69

    move/from16 v0, v150

    move/from16 v1, v152

    if-ne v0, v1, :cond_e

    .line 727
    const-string/jumbo v149, "darkkhaki"

    .line 728
    const/16 v151, 0x1c

    goto/16 :goto_0

    .line 729
    :cond_e
    const/16 v152, 0x6e

    move/from16 v0, v150

    move/from16 v1, v152

    if-ne v0, v1, :cond_0

    .line 730
    const-string/jumbo v149, "darkgreen"

    .line 731
    const/16 v151, 0x1a

    goto/16 :goto_0

    .line 735
    .end local v150    # "c":I
    :pswitch_22
    const-string/jumbo v149, "firebrick"

    .line 736
    const/16 v151, 0x2e

    .line 737
    goto/16 :goto_0

    .line 739
    :pswitch_23
    const/16 v152, 0x8

    move-object/from16 v0, p0

    move/from16 v1, v152

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v150

    .line 740
    .restart local v150    # "c":I
    const/16 v152, 0x64

    move/from16 v0, v150

    move/from16 v1, v152

    if-ne v0, v1, :cond_f

    .line 741
    const-string/jumbo v149, "goldenrod"

    .line 742
    const/16 v151, 0x35

    goto/16 :goto_0

    .line 743
    :cond_f
    const/16 v152, 0x6f

    move/from16 v0, v150

    move/from16 v1, v152

    if-ne v0, v1, :cond_0

    .line 744
    const-string/jumbo v149, "gainsboro"

    .line 745
    const/16 v151, 0x32

    goto/16 :goto_0

    .line 749
    .end local v150    # "c":I
    :pswitch_24
    const-string/jumbo v149, "indianred"

    .line 750
    const/16 v151, 0x3c

    .line 751
    goto/16 :goto_0

    .line 753
    :pswitch_25
    const/16 v152, 0x5

    move-object/from16 v0, p0

    move/from16 v1, v152

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v152

    sparse-switch v152, :sswitch_data_3

    goto/16 :goto_0

    .line 755
    :sswitch_1d
    const-string/jumbo v149, "lightblue"

    .line 756
    const/16 v151, 0x44

    .line 757
    goto/16 :goto_0

    .line 759
    :sswitch_1e
    const-string/jumbo v149, "lightcyan"

    .line 760
    const/16 v151, 0x46

    .line 761
    goto/16 :goto_0

    .line 763
    :sswitch_1f
    const/16 v152, 0x7

    move-object/from16 v0, p0

    move/from16 v1, v152

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v150

    .line 764
    .restart local v150    # "c":I
    const/16 v152, 0x61

    move/from16 v0, v150

    move/from16 v1, v152

    if-ne v0, v1, :cond_10

    .line 765
    const-string/jumbo v149, "lightgray"

    .line 766
    const/16 v151, 0x48

    goto/16 :goto_0

    .line 767
    :cond_10
    const/16 v152, 0x65

    move/from16 v0, v150

    move/from16 v1, v152

    if-ne v0, v1, :cond_0

    .line 768
    const-string/jumbo v149, "lightgrey"

    .line 769
    const/16 v151, 0x4a

    goto/16 :goto_0

    .line 773
    .end local v150    # "c":I
    :sswitch_20
    const-string/jumbo v149, "lightpink"

    .line 774
    const/16 v151, 0x4b

    .line 775
    goto/16 :goto_0

    .line 777
    :sswitch_21
    const/16 v152, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v152

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v150

    .line 778
    .restart local v150    # "c":I
    const/16 v152, 0x61

    move/from16 v0, v150

    move/from16 v1, v152

    if-ne v0, v1, :cond_11

    .line 779
    const-string/jumbo v149, "lawngreen"

    .line 780
    const/16 v151, 0x42

    goto/16 :goto_0

    .line 781
    :cond_11
    const/16 v152, 0x69

    move/from16 v0, v150

    move/from16 v1, v152

    if-ne v0, v1, :cond_0

    .line 782
    const-string/jumbo v149, "limegreen"

    .line 783
    const/16 v151, 0x54

    goto/16 :goto_0

    .line 789
    .end local v150    # "c":I
    :pswitch_26
    const/16 v152, 0x8

    move-object/from16 v0, p0

    move/from16 v1, v152

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v150

    .line 790
    .restart local v150    # "c":I
    const/16 v152, 0x65

    move/from16 v0, v150

    move/from16 v1, v152

    if-ne v0, v1, :cond_12

    .line 791
    const-string/jumbo v149, "mistyrose"

    .line 792
    const/16 v151, 0x63

    goto/16 :goto_0

    .line 793
    :cond_12
    const/16 v152, 0x6d

    move/from16 v0, v150

    move/from16 v1, v152

    if-ne v0, v1, :cond_0

    .line 794
    const-string/jumbo v149, "mintcream"

    .line 795
    const/16 v151, 0x62

    goto/16 :goto_0

    .line 799
    .end local v150    # "c":I
    :pswitch_27
    const/16 v152, 0x8

    move-object/from16 v0, p0

    move/from16 v1, v152

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v150

    .line 800
    .restart local v150    # "c":I
    const/16 v152, 0x62

    move/from16 v0, v150

    move/from16 v1, v152

    if-ne v0, v1, :cond_13

    .line 801
    const-string/jumbo v149, "olivedrab"

    .line 802
    const/16 v151, 0x69

    goto/16 :goto_0

    .line 803
    :cond_13
    const/16 v152, 0x64

    move/from16 v0, v150

    move/from16 v1, v152

    if-ne v0, v1, :cond_0

    .line 804
    const-string/jumbo v149, "orangered"

    .line 805
    const/16 v151, 0x6b

    goto/16 :goto_0

    .line 809
    .end local v150    # "c":I
    :pswitch_28
    const/16 v152, 0x8

    move-object/from16 v0, p0

    move/from16 v1, v152

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v150

    .line 810
    .restart local v150    # "c":I
    const/16 v152, 0x66

    move/from16 v0, v150

    move/from16 v1, v152

    if-ne v0, v1, :cond_14

    .line 811
    const-string/jumbo v149, "peachpuff"

    .line 812
    const/16 v151, 0x72

    goto/16 :goto_0

    .line 813
    :cond_14
    const/16 v152, 0x6e

    move/from16 v0, v150

    move/from16 v1, v152

    if-ne v0, v1, :cond_0

    .line 814
    const-string/jumbo v149, "palegreen"

    .line 815
    const/16 v151, 0x6e

    goto/16 :goto_0

    .line 819
    .end local v150    # "c":I
    :pswitch_29
    const/16 v152, 0x8

    move-object/from16 v0, p0

    move/from16 v1, v152

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v150

    .line 820
    .restart local v150    # "c":I
    const/16 v152, 0x65

    move/from16 v0, v150

    move/from16 v1, v152

    if-ne v0, v1, :cond_15

    .line 821
    const-string/jumbo v149, "royalblue"

    .line 822
    const/16 v151, 0x7a

    goto/16 :goto_0

    .line 823
    :cond_15
    const/16 v152, 0x6e

    move/from16 v0, v150

    move/from16 v1, v152

    if-ne v0, v1, :cond_0

    .line 824
    const-string/jumbo v149, "rosybrown"

    .line 825
    const/16 v151, 0x79

    goto/16 :goto_0

    .line 829
    .end local v150    # "c":I
    :pswitch_2a
    const/16 v152, 0x7

    move-object/from16 v0, p0

    move/from16 v1, v152

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v150

    .line 830
    .restart local v150    # "c":I
    const/16 v152, 0x61

    move/from16 v0, v150

    move/from16 v1, v152

    if-ne v0, v1, :cond_16

    .line 831
    const-string/jumbo v149, "slategray"

    .line 832
    const/16 v151, 0x84

    goto/16 :goto_0

    .line 833
    :cond_16
    const/16 v152, 0x65

    move/from16 v0, v150

    move/from16 v1, v152

    if-ne v0, v1, :cond_17

    .line 834
    const-string/jumbo v149, "slategrey"

    .line 835
    const/16 v151, 0x85

    goto/16 :goto_0

    .line 836
    :cond_17
    const/16 v152, 0x75

    move/from16 v0, v150

    move/from16 v1, v152

    if-ne v0, v1, :cond_0

    .line 837
    const/16 v152, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v152

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v150

    .line 838
    const/16 v152, 0x6c

    move/from16 v0, v150

    move/from16 v1, v152

    if-ne v0, v1, :cond_18

    .line 839
    const-string/jumbo v149, "slateblue"

    .line 840
    const/16 v151, 0x83

    goto/16 :goto_0

    .line 841
    :cond_18
    const/16 v152, 0x74

    move/from16 v0, v150

    move/from16 v1, v152

    if-ne v0, v1, :cond_0

    .line 842
    const-string/jumbo v149, "steelblue"

    .line 843
    const/16 v151, 0x88

    goto/16 :goto_0

    .line 848
    .end local v150    # "c":I
    :pswitch_2b
    const-string/jumbo v149, "turquoise"

    .line 849
    const/16 v151, 0x8d

    .line 850
    goto/16 :goto_0

    .line 854
    :pswitch_2c
    const/16 v152, 0x8

    move-object/from16 v0, p0

    move/from16 v1, v152

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v152

    packed-switch v152, :pswitch_data_4

    :pswitch_2d
    goto/16 :goto_0

    .line 856
    :pswitch_2e
    const-string/jumbo v149, "lightcoral"

    .line 857
    const/16 v151, 0x45

    .line 858
    goto/16 :goto_0

    .line 860
    :pswitch_2f
    const/16 v152, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v152

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v150

    .line 861
    .restart local v150    # "c":I
    const/16 v152, 0x62

    move/from16 v0, v150

    move/from16 v1, v152

    if-ne v0, v1, :cond_19

    .line 862
    const-string/jumbo v149, "blueviolet"

    .line 863
    const/16 v151, 0xb

    goto/16 :goto_0

    .line 864
    :cond_19
    const/16 v152, 0x64

    move/from16 v0, v150

    move/from16 v1, v152

    if-ne v0, v1, :cond_1a

    .line 865
    const-string/jumbo v149, "darkviolet"

    .line 866
    const/16 v151, 0x28

    goto/16 :goto_0

    .line 867
    :cond_1a
    const/16 v152, 0x6c

    move/from16 v0, v150

    move/from16 v1, v152

    if-ne v0, v1, :cond_0

    .line 868
    const-string/jumbo v149, "lightgreen"

    .line 869
    const/16 v151, 0x49

    goto/16 :goto_0

    .line 873
    .end local v150    # "c":I
    :pswitch_30
    const-string/jumbo v149, "darkorange"

    .line 874
    const/16 v151, 0x1f

    .line 875
    goto/16 :goto_0

    .line 877
    :pswitch_31
    const/16 v152, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v152

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v150

    .line 878
    .restart local v150    # "c":I
    const/16 v152, 0x64

    move/from16 v0, v150

    move/from16 v1, v152

    if-ne v0, v1, :cond_1b

    .line 879
    const-string/jumbo v149, "darkorchid"

    .line 880
    const/16 v151, 0x20

    goto/16 :goto_0

    .line 881
    :cond_1b
    const/16 v152, 0x70

    move/from16 v0, v150

    move/from16 v1, v152

    if-ne v0, v1, :cond_0

    .line 882
    const-string/jumbo v149, "papayawhip"

    .line 883
    const/16 v151, 0x71

    goto/16 :goto_0

    .line 887
    .end local v150    # "c":I
    :pswitch_32
    const-string/jumbo v149, "whitesmoke"

    .line 888
    const/16 v151, 0x91

    .line 889
    goto/16 :goto_0

    .line 891
    :pswitch_33
    const-string/jumbo v149, "aquamarine"

    .line 892
    const/16 v151, 0x4

    .line 893
    goto/16 :goto_0

    .line 895
    :pswitch_34
    const-string/jumbo v149, "darksalmon"

    .line 896
    const/16 v151, 0x22

    .line 897
    goto/16 :goto_0

    .line 899
    :pswitch_35
    const-string/jumbo v149, "chartreuse"

    .line 900
    const/16 v151, 0xf

    .line 901
    goto/16 :goto_0

    .line 903
    :pswitch_36
    const-string/jumbo v149, "ghostwhite"

    .line 904
    const/16 v151, 0x33

    .line 905
    goto/16 :goto_0

    .line 907
    :pswitch_37
    const/16 v152, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v152

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v150

    .line 908
    .restart local v150    # "c":I
    const/16 v152, 0x64

    move/from16 v0, v150

    move/from16 v1, v152

    if-ne v0, v1, :cond_1c

    .line 909
    const-string/jumbo v149, "dodgerblue"

    .line 910
    const/16 v151, 0x2d

    goto/16 :goto_0

    .line 911
    :cond_1c
    const/16 v152, 0x6d

    move/from16 v0, v150

    move/from16 v1, v152

    if-ne v0, v1, :cond_1d

    .line 912
    const-string/jumbo v149, "mediumblue"

    .line 913
    const/16 v151, 0x59

    goto/16 :goto_0

    .line 914
    :cond_1d
    const/16 v152, 0x70

    move/from16 v0, v150

    move/from16 v1, v152

    if-ne v0, v1, :cond_0

    .line 915
    const-string/jumbo v149, "powderblue"

    .line 916
    const/16 v151, 0x76

    goto/16 :goto_0

    .line 920
    .end local v150    # "c":I
    :pswitch_38
    const-string/jumbo v149, "sandybrown"

    .line 921
    const/16 v151, 0x7d

    .line 922
    goto/16 :goto_0

    .line 926
    :pswitch_39
    const/16 v152, 0x5

    move-object/from16 v0, p0

    move/from16 v1, v152

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v152

    sparse-switch v152, :sswitch_data_4

    goto/16 :goto_0

    .line 928
    :sswitch_22
    const-string/jumbo v149, "darkmagenta"

    .line 929
    const/16 v151, 0x1d

    .line 930
    goto/16 :goto_0

    .line 932
    :sswitch_23
    const-string/jumbo v149, "saddlebrown"

    .line 933
    const/16 v151, 0x7b

    .line 934
    goto/16 :goto_0

    .line 936
    :sswitch_24
    const-string/jumbo v149, "springgreen"

    .line 937
    const/16 v151, 0x87

    .line 938
    goto/16 :goto_0

    .line 940
    :sswitch_25
    const-string/jumbo v149, "deepskyblue"

    .line 941
    const/16 v151, 0x2a

    .line 942
    goto/16 :goto_0

    .line 944
    :sswitch_26
    const-string/jumbo v149, "floralwhite"

    .line 945
    const/16 v151, 0x2f

    .line 946
    goto/16 :goto_0

    .line 948
    :sswitch_27
    const-string/jumbo v149, "navajowhite"

    .line 949
    const/16 v151, 0x65

    .line 950
    goto/16 :goto_0

    .line 952
    :sswitch_28
    const-string/jumbo v149, "lightsalmon"

    .line 953
    const/16 v151, 0x4c

    .line 954
    goto/16 :goto_0

    .line 956
    :sswitch_29
    const-string/jumbo v149, "forestgreen"

    .line 957
    const/16 v151, 0x30

    .line 958
    goto/16 :goto_0

    .line 960
    :sswitch_2a
    const-string/jumbo v149, "yellowgreen"

    .line 961
    const/16 v151, 0x93

    .line 962
    goto/16 :goto_0

    .line 964
    :sswitch_2b
    const/16 v152, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v152

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v150

    .line 965
    .restart local v150    # "c":I
    const/16 v152, 0x67

    move/from16 v0, v150

    move/from16 v1, v152

    if-ne v0, v1, :cond_1e

    .line 966
    const-string/jumbo v149, "greenyellow"

    .line 967
    const/16 v151, 0x39

    goto/16 :goto_0

    .line 968
    :cond_1e
    const/16 v152, 0x6c

    move/from16 v0, v150

    move/from16 v1, v152

    if-ne v0, v1, :cond_0

    .line 969
    const-string/jumbo v149, "lightyellow"

    .line 970
    const/16 v151, 0x52

    goto/16 :goto_0

    .line 976
    .end local v150    # "c":I
    :pswitch_3a
    const/16 v152, 0x7

    move-object/from16 v0, p0

    move/from16 v1, v152

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v152

    sparse-switch v152, :sswitch_data_5

    goto/16 :goto_0

    .line 978
    :sswitch_2c
    const-string/jumbo v149, "darkseagreen"

    .line 979
    const/16 v151, 0x23

    .line 980
    goto/16 :goto_0

    .line 982
    :sswitch_2d
    const-string/jumbo v149, "lemonchiffon"

    .line 983
    const/16 v151, 0x43

    .line 984
    goto/16 :goto_0

    .line 986
    :sswitch_2e
    const-string/jumbo v149, "mediumorchid"

    .line 987
    const/16 v151, 0x5a

    .line 988
    goto/16 :goto_0

    .line 990
    :sswitch_2f
    const-string/jumbo v149, "midnightblue"

    .line 991
    const/16 v151, 0x61

    .line 992
    goto/16 :goto_0

    .line 994
    :sswitch_30
    const-string/jumbo v149, "mediumpurple"

    .line 995
    const/16 v151, 0x5b

    .line 996
    goto/16 :goto_0

    .line 998
    :sswitch_31
    const-string/jumbo v149, "antiquewhite"

    .line 999
    const/16 v151, 0x2

    .line 1000
    goto/16 :goto_0

    .line 1002
    :sswitch_32
    const-string/jumbo v149, "lightskyblue"

    .line 1003
    const/16 v151, 0x4e

    .line 1004
    goto/16 :goto_0

    .line 1008
    :pswitch_3b
    const/16 v152, 0x9

    move-object/from16 v0, p0

    move/from16 v1, v152

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v152

    sparse-switch v152, :sswitch_data_6

    goto/16 :goto_0

    .line 1010
    :sswitch_33
    const-string/jumbo v149, "darkslateblue"

    .line 1011
    const/16 v151, 0x24

    .line 1012
    goto/16 :goto_0

    .line 1014
    :sswitch_34
    const/16 v152, 0xb

    move-object/from16 v0, p0

    move/from16 v1, v152

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v150

    .line 1015
    .restart local v150    # "c":I
    const/16 v152, 0x61

    move/from16 v0, v150

    move/from16 v1, v152

    if-ne v0, v1, :cond_1f

    .line 1016
    const-string/jumbo v149, "darkslategray"

    .line 1017
    const/16 v151, 0x25

    goto/16 :goto_0

    .line 1018
    :cond_1f
    const/16 v152, 0x65

    move/from16 v0, v150

    move/from16 v1, v152

    if-ne v0, v1, :cond_0

    .line 1019
    const-string/jumbo v149, "darkslategrey"

    .line 1020
    const/16 v151, 0x26

    goto/16 :goto_0

    .line 1024
    .end local v150    # "c":I
    :sswitch_35
    const-string/jumbo v149, "lavenderblush"

    .line 1025
    const/16 v151, 0x41

    .line 1026
    goto/16 :goto_0

    .line 1028
    :sswitch_36
    const/16 v152, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v152

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v150

    .line 1029
    .restart local v150    # "c":I
    const/16 v152, 0x64

    move/from16 v0, v150

    move/from16 v1, v152

    if-ne v0, v1, :cond_20

    .line 1030
    const-string/jumbo v149, "darkgoldenrod"

    .line 1031
    const/16 v151, 0x18

    goto/16 :goto_0

    .line 1032
    :cond_20
    const/16 v152, 0x70

    move/from16 v0, v150

    move/from16 v1, v152

    if-ne v0, v1, :cond_0

    .line 1033
    const-string/jumbo v149, "palegoldenrod"

    .line 1034
    const/16 v151, 0x6d

    goto/16 :goto_0

    .line 1038
    .end local v150    # "c":I
    :sswitch_37
    const/16 v152, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v152

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v150

    .line 1039
    .restart local v150    # "c":I
    const/16 v152, 0x64

    move/from16 v0, v150

    move/from16 v1, v152

    if-ne v0, v1, :cond_21

    .line 1040
    const-string/jumbo v149, "darkturquoise"

    .line 1041
    const/16 v151, 0x27

    goto/16 :goto_0

    .line 1042
    :cond_21
    const/16 v152, 0x70

    move/from16 v0, v150

    move/from16 v1, v152

    if-ne v0, v1, :cond_0

    .line 1043
    const-string/jumbo v149, "paleturquoise"

    .line 1044
    const/16 v151, 0x6f

    goto/16 :goto_0

    .line 1048
    .end local v150    # "c":I
    :sswitch_38
    const-string/jumbo v149, "lightseagreen"

    .line 1049
    const/16 v151, 0x4d

    .line 1050
    goto/16 :goto_0

    .line 1052
    :sswitch_39
    const-string/jumbo v149, "palevioletred"

    .line 1053
    const/16 v151, 0x70

    .line 1054
    goto/16 :goto_0

    .line 1058
    :pswitch_3c
    const/16 v152, 0x6

    move-object/from16 v0, p0

    move/from16 v1, v152

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v152

    sparse-switch v152, :sswitch_data_7

    goto/16 :goto_0

    .line 1060
    :sswitch_3a
    const-string/jumbo v149, "blanchedalmond"

    .line 1061
    const/16 v151, 0x9

    .line 1062
    goto/16 :goto_0

    .line 1064
    :sswitch_3b
    const-string/jumbo v149, "darkolivegreen"

    .line 1065
    const/16 v151, 0x1e

    .line 1066
    goto/16 :goto_0

    .line 1068
    :sswitch_3c
    const/16 v152, 0xc

    move-object/from16 v0, p0

    move/from16 v1, v152

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v150

    .line 1069
    .restart local v150    # "c":I
    const/16 v152, 0x61

    move/from16 v0, v150

    move/from16 v1, v152

    if-ne v0, v1, :cond_22

    .line 1070
    const-string/jumbo v149, "lightslategray"

    .line 1071
    const/16 v151, 0x4f

    goto/16 :goto_0

    .line 1072
    :cond_22
    const/16 v152, 0x65

    move/from16 v0, v150

    move/from16 v1, v152

    if-ne v0, v1, :cond_0

    .line 1073
    const-string/jumbo v149, "lightslategrey"

    .line 1074
    const/16 v151, 0x50

    goto/16 :goto_0

    .line 1078
    .end local v150    # "c":I
    :sswitch_3d
    const-string/jumbo v149, "cornflowerblue"

    .line 1079
    const/16 v151, 0x12

    .line 1080
    goto/16 :goto_0

    .line 1082
    :sswitch_3e
    const-string/jumbo v149, "mediumseagreen"

    .line 1083
    const/16 v151, 0x5c

    .line 1084
    goto/16 :goto_0

    .line 1086
    :sswitch_3f
    const-string/jumbo v149, "lightsteelblue"

    .line 1087
    const/16 v151, 0x51

    .line 1088
    goto/16 :goto_0

    .line 1092
    :pswitch_3d
    const/16 v152, 0x6

    move-object/from16 v0, p0

    move/from16 v1, v152

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v150

    .line 1093
    .restart local v150    # "c":I
    const/16 v152, 0x73

    move/from16 v0, v150

    move/from16 v1, v152

    if-ne v0, v1, :cond_23

    .line 1094
    const-string/jumbo v149, "mediumslateblue"

    .line 1095
    const/16 v151, 0x5d

    goto/16 :goto_0

    .line 1096
    :cond_23
    const/16 v152, 0x74

    move/from16 v0, v150

    move/from16 v1, v152

    if-ne v0, v1, :cond_24

    .line 1097
    const-string/jumbo v149, "mediumturquoise"

    .line 1098
    const/16 v151, 0x5f

    goto/16 :goto_0

    .line 1099
    :cond_24
    const/16 v152, 0x76

    move/from16 v0, v150

    move/from16 v1, v152

    if-ne v0, v1, :cond_0

    .line 1100
    const-string/jumbo v149, "mediumvioletred"

    .line 1101
    const/16 v151, 0x60

    goto/16 :goto_0

    .line 1105
    .end local v150    # "c":I
    :pswitch_3e
    const-string/jumbo v149, "mediumaquamarine"

    .line 1106
    const/16 v151, 0x58

    .line 1107
    goto/16 :goto_0

    .line 1109
    :pswitch_3f
    const-string/jumbo v149, "mediumspringgreen"

    .line 1110
    const/16 v151, 0x5e

    .line 1111
    goto/16 :goto_0

    .line 1113
    :pswitch_40
    const-string/jumbo v149, "lightgoldenrodyellow"

    .line 1114
    const/16 v151, 0x47

    .line 1115
    goto/16 :goto_0

    .line 413
    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_10
        :pswitch_1b
        :pswitch_1c
        :pswitch_2c
        :pswitch_39
        :pswitch_3a
        :pswitch_3b
        :pswitch_3c
        :pswitch_3d
        :pswitch_3e
        :pswitch_3f
        :pswitch_0
        :pswitch_0
        :pswitch_40
    .end packed-switch

    .line 429
    :sswitch_data_0
    .sparse-switch
        0x61 -> :sswitch_0
        0x64 -> :sswitch_1
        0x65 -> :sswitch_2
        0x6b -> :sswitch_3
        0x6c -> :sswitch_4
        0x6d -> :sswitch_5
        0x6e -> :sswitch_6
        0x75 -> :sswitch_7
        0x77 -> :sswitch_8
        0x79 -> :sswitch_9
    .end sparse-switch

    .line 498
    :sswitch_data_1
    .sparse-switch
        0x61 -> :sswitch_a
        0x62 -> :sswitch_b
        0x63 -> :sswitch_c
        0x67 -> :sswitch_d
        0x69 -> :sswitch_e
        0x6b -> :sswitch_f
        0x6c -> :sswitch_10
        0x6f -> :sswitch_11
        0x77 -> :sswitch_12
    .end sparse-switch

    .line 553
    :pswitch_data_1
    .packed-switch 0x61
        :pswitch_6
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_7
        :pswitch_8
        :pswitch_5
        :pswitch_5
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_f
    .end packed-switch

    .line 609
    :pswitch_data_2
    .packed-switch 0x62
        :pswitch_12
        :pswitch_11
        :pswitch_11
        :pswitch_13
        :pswitch_11
        :pswitch_14
        :pswitch_15
        :pswitch_11
        :pswitch_11
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_11
        :pswitch_11
        :pswitch_19
        :pswitch_11
        :pswitch_11
        :pswitch_1a
    .end packed-switch

    .line 655
    :sswitch_data_2
    .sparse-switch
        0x61 -> :sswitch_13
        0x62 -> :sswitch_14
        0x63 -> :sswitch_15
        0x67 -> :sswitch_16
        0x68 -> :sswitch_17
        0x6e -> :sswitch_18
        0x70 -> :sswitch_19
        0x72 -> :sswitch_1a
        0x73 -> :sswitch_1b
        0x79 -> :sswitch_1c
    .end sparse-switch

    .line 705
    :pswitch_data_3
    .packed-switch 0x61
        :pswitch_1e
        :pswitch_1f
        :pswitch_20
        :pswitch_21
        :pswitch_1d
        :pswitch_22
        :pswitch_23
        :pswitch_1d
        :pswitch_24
        :pswitch_1d
        :pswitch_1d
        :pswitch_25
        :pswitch_26
        :pswitch_1d
        :pswitch_27
        :pswitch_28
        :pswitch_1d
        :pswitch_29
        :pswitch_2a
        :pswitch_2b
    .end packed-switch

    .line 753
    :sswitch_data_3
    .sparse-switch
        0x62 -> :sswitch_1d
        0x63 -> :sswitch_1e
        0x67 -> :sswitch_1f
        0x70 -> :sswitch_20
        0x72 -> :sswitch_21
    .end sparse-switch

    .line 854
    :pswitch_data_4
    .packed-switch 0x61
        :pswitch_2e
        :pswitch_2d
        :pswitch_2d
        :pswitch_2d
        :pswitch_2f
        :pswitch_2d
        :pswitch_30
        :pswitch_2d
        :pswitch_31
        :pswitch_2d
        :pswitch_32
        :pswitch_2d
        :pswitch_2d
        :pswitch_33
        :pswitch_34
        :pswitch_2d
        :pswitch_2d
        :pswitch_2d
        :pswitch_35
        :pswitch_36
        :pswitch_37
        :pswitch_2d
        :pswitch_38
    .end packed-switch

    .line 926
    :sswitch_data_4
    .sparse-switch
        0x61 -> :sswitch_22
        0x65 -> :sswitch_23
        0x67 -> :sswitch_24
        0x6b -> :sswitch_25
        0x6c -> :sswitch_26
        0x6f -> :sswitch_27
        0x73 -> :sswitch_28
        0x74 -> :sswitch_29
        0x77 -> :sswitch_2a
        0x79 -> :sswitch_2b
    .end sparse-switch

    .line 976
    :sswitch_data_5
    .sparse-switch
        0x67 -> :sswitch_2c
        0x69 -> :sswitch_2d
        0x72 -> :sswitch_2e
        0x74 -> :sswitch_2f
        0x75 -> :sswitch_30
        0x77 -> :sswitch_31
        0x79 -> :sswitch_32
    .end sparse-switch

    .line 1008
    :sswitch_data_6
    .sparse-switch
        0x62 -> :sswitch_33
        0x67 -> :sswitch_34
        0x6c -> :sswitch_35
        0x6e -> :sswitch_36
        0x6f -> :sswitch_37
        0x72 -> :sswitch_38
        0x74 -> :sswitch_39
    .end sparse-switch

    .line 1058
    :sswitch_data_7
    .sparse-switch
        0x65 -> :sswitch_3a
        0x69 -> :sswitch_3b
        0x6c -> :sswitch_3c
        0x6f -> :sswitch_3d
        0x73 -> :sswitch_3e
        0x74 -> :sswitch_3f
    .end sparse-switch
.end method
