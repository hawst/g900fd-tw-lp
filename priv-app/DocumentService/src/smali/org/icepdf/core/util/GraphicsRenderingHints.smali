.class public Lorg/icepdf/core/util/GraphicsRenderingHints;
.super Ljava/lang/Object;
.source "GraphicsRenderingHints.java"


# static fields
.field public static final PRINT:I = 0x2

.field public static final SCREEN:I = 0x1

.field private static singleton:Lorg/icepdf/core/util/GraphicsRenderingHints;


# instance fields
.field printBackground:Lorg/apache/poi/java/awt/Color;

.field screenBackground:Lorg/apache/poi/java/awt/Color;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 377
    sget-object v0, Lorg/apache/poi/java/awt/Color;->white:Lorg/apache/poi/java/awt/Color;

    iput-object v0, p0, Lorg/icepdf/core/util/GraphicsRenderingHints;->printBackground:Lorg/apache/poi/java/awt/Color;

    .line 434
    sget-object v0, Lorg/apache/poi/java/awt/Color;->white:Lorg/apache/poi/java/awt/Color;

    iput-object v0, p0, Lorg/icepdf/core/util/GraphicsRenderingHints;->screenBackground:Lorg/apache/poi/java/awt/Color;

    .line 63
    invoke-direct {p0}, Lorg/icepdf/core/util/GraphicsRenderingHints;->setFromProperties()V

    .line 64
    return-void
.end method

.method public static declared-synchronized getDefault()Lorg/icepdf/core/util/GraphicsRenderingHints;
    .locals 2

    .prologue
    .line 50
    const-class v1, Lorg/icepdf/core/util/GraphicsRenderingHints;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lorg/icepdf/core/util/GraphicsRenderingHints;->singleton:Lorg/icepdf/core/util/GraphicsRenderingHints;

    if-nez v0, :cond_0

    .line 51
    new-instance v0, Lorg/icepdf/core/util/GraphicsRenderingHints;

    invoke-direct {v0}, Lorg/icepdf/core/util/GraphicsRenderingHints;-><init>()V

    sput-object v0, Lorg/icepdf/core/util/GraphicsRenderingHints;->singleton:Lorg/icepdf/core/util/GraphicsRenderingHints;

    .line 53
    :cond_0
    sget-object v0, Lorg/icepdf/core/util/GraphicsRenderingHints;->singleton:Lorg/icepdf/core/util/GraphicsRenderingHints;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 50
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private setFromProperties()V
    .locals 0

    .prologue
    .line 323
    return-void
.end method


# virtual methods
.method public getPageBackgroundColor(I)Lorg/apache/poi/java/awt/Color;
    .locals 1
    .param p1, "hintType"    # I

    .prologue
    .line 83
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 84
    iget-object v0, p0, Lorg/icepdf/core/util/GraphicsRenderingHints;->screenBackground:Lorg/apache/poi/java/awt/Color;

    .line 86
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lorg/icepdf/core/util/GraphicsRenderingHints;->printBackground:Lorg/apache/poi/java/awt/Color;

    goto :goto_0
.end method

.method public declared-synchronized reset()V
    .locals 1

    .prologue
    .line 94
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lorg/icepdf/core/util/GraphicsRenderingHints;->setFromProperties()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 95
    monitor-exit p0

    return-void

    .line 94
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
