.class public Lorg/icepdf/core/util/Library;
.super Ljava/lang/Object;
.source "Library.java"


# static fields
.field private static final log:Ljava/util/logging/Logger;


# instance fields
.field public cacheManager:Lorg/icepdf/core/util/CacheManager;

.field private catalog:Lorg/icepdf/core/pobjects/Catalog;

.field private isEncrypted:Z

.field private isLinearTraversal:Z

.field private lookupReference2ICCBased:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Lorg/icepdf/core/pobjects/Reference;",
            "Lorg/icepdf/core/pobjects/graphics/ICCBased;",
            ">;"
        }
    .end annotation
.end field

.field private m_LazyObjectLoader:Lorg/icepdf/core/util/LazyObjectLoader;

.field public memoryManager:Lorg/icepdf/core/util/MemoryManager;

.field private refs:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Lorg/icepdf/core/pobjects/Reference;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public securityManager:Lorg/icepdf/core/pobjects/security/SecurityManager;

.field public stateManager:Lorg/icepdf/core/pobjects/StateManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 41
    const-class v0, Lorg/icepdf/core/util/Library;

    invoke-virtual {v0}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Lorg/icepdf/core/util/Library;->log:Ljava/util/logging/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 537
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    const/16 v1, 0x400

    invoke-direct {v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;-><init>(I)V

    iput-object v0, p0, Lorg/icepdf/core/util/Library;->refs:Ljava/util/concurrent/ConcurrentHashMap;

    .line 49
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    const/16 v1, 0x100

    invoke-direct {v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;-><init>(I)V

    iput-object v0, p0, Lorg/icepdf/core/util/Library;->lookupReference2ICCBased:Ljava/util/concurrent/ConcurrentHashMap;

    .line 539
    invoke-static {}, Lorg/icepdf/core/util/MemoryManager;->getInstance()Lorg/icepdf/core/util/MemoryManager;

    move-result-object v0

    iput-object v0, p0, Lorg/icepdf/core/util/Library;->memoryManager:Lorg/icepdf/core/util/MemoryManager;

    .line 540
    new-instance v0, Lorg/icepdf/core/util/CacheManager;

    invoke-direct {v0}, Lorg/icepdf/core/util/CacheManager;-><init>()V

    iput-object v0, p0, Lorg/icepdf/core/util/Library;->cacheManager:Lorg/icepdf/core/util/CacheManager;

    .line 541
    return-void
.end method

.method private printObjectDebug(Ljava/lang/Object;)V
    .locals 4
    .param p1, "ob"    # Ljava/lang/Object;

    .prologue
    .line 124
    if-nez p1, :cond_0

    .line 125
    sget-object v1, Lorg/icepdf/core/util/Library;->log:Ljava/util/logging/Logger;

    const-string/jumbo v2, "null object found"

    invoke-virtual {v1, v2}, Ljava/util/logging/Logger;->finer(Ljava/lang/String;)V

    .line 135
    :goto_0
    return-void

    .line 126
    :cond_0
    instance-of v1, p1, Lorg/icepdf/core/pobjects/PObject;

    if-eqz v1, :cond_1

    move-object v0, p1

    .line 127
    check-cast v0, Lorg/icepdf/core/pobjects/PObject;

    .line 128
    .local v0, "tmp":Lorg/icepdf/core/pobjects/PObject;
    sget-object v1, Lorg/icepdf/core/util/Library;->log:Ljava/util/logging/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Lorg/icepdf/core/pobjects/PObject;->getReference()Lorg/icepdf/core/pobjects/Reference;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lorg/icepdf/core/pobjects/PObject;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/logging/Logger;->finer(Ljava/lang/String;)V

    goto :goto_0

    .line 129
    .end local v0    # "tmp":Lorg/icepdf/core/pobjects/PObject;
    :cond_1
    instance-of v1, p1, Lorg/icepdf/core/pobjects/Dictionary;

    if-eqz v1, :cond_2

    move-object v0, p1

    .line 130
    check-cast v0, Lorg/icepdf/core/pobjects/Dictionary;

    .line 131
    .local v0, "tmp":Lorg/icepdf/core/pobjects/Dictionary;
    sget-object v1, Lorg/icepdf/core/util/Library;->log:Ljava/util/logging/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Lorg/icepdf/core/pobjects/Dictionary;->getPObjectReference()Lorg/icepdf/core/pobjects/Reference;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lorg/icepdf/core/pobjects/Dictionary;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/logging/Logger;->finer(Ljava/lang/String;)V

    goto :goto_0

    .line 133
    .end local v0    # "tmp":Lorg/icepdf/core/pobjects/Dictionary;
    :cond_2
    sget-object v1, Lorg/icepdf/core/util/Library;->log:Ljava/util/logging/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/logging/Logger;->finer(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public addObject(Ljava/lang/Object;Lorg/icepdf/core/pobjects/Reference;)V
    .locals 1
    .param p1, "object"    # Ljava/lang/Object;
    .param p2, "objectReference"    # Lorg/icepdf/core/pobjects/Reference;

    .prologue
    .line 520
    iget-object v0, p0, Lorg/icepdf/core/util/Library;->refs:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p2, p1}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 521
    return-void
.end method

.method public dispose()V
    .locals 1

    .prologue
    .line 672
    iget-object v0, p0, Lorg/icepdf/core/util/Library;->memoryManager:Lorg/icepdf/core/util/MemoryManager;

    if-eqz v0, :cond_0

    .line 673
    iget-object v0, p0, Lorg/icepdf/core/util/Library;->memoryManager:Lorg/icepdf/core/util/MemoryManager;

    invoke-virtual {v0, p0}, Lorg/icepdf/core/util/MemoryManager;->releaseAllByLibrary(Lorg/icepdf/core/util/Library;)V

    .line 675
    :cond_0
    iget-object v0, p0, Lorg/icepdf/core/util/Library;->cacheManager:Lorg/icepdf/core/util/CacheManager;

    if-eqz v0, :cond_1

    .line 676
    iget-object v0, p0, Lorg/icepdf/core/util/Library;->cacheManager:Lorg/icepdf/core/util/CacheManager;

    invoke-virtual {v0}, Lorg/icepdf/core/util/CacheManager;->dispose()V

    .line 678
    :cond_1
    iget-object v0, p0, Lorg/icepdf/core/util/Library;->refs:Ljava/util/concurrent/ConcurrentHashMap;

    if-eqz v0, :cond_2

    .line 679
    iget-object v0, p0, Lorg/icepdf/core/util/Library;->refs:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->clear()V

    .line 681
    :cond_2
    iget-object v0, p0, Lorg/icepdf/core/util/Library;->lookupReference2ICCBased:Ljava/util/concurrent/ConcurrentHashMap;

    if-eqz v0, :cond_3

    .line 682
    iget-object v0, p0, Lorg/icepdf/core/util/Library;->lookupReference2ICCBased:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->clear()V

    .line 683
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/icepdf/core/util/Library;->lookupReference2ICCBased:Ljava/util/concurrent/ConcurrentHashMap;

    .line 685
    :cond_3
    iget-object v0, p0, Lorg/icepdf/core/util/Library;->m_LazyObjectLoader:Lorg/icepdf/core/util/LazyObjectLoader;

    if-eqz v0, :cond_4

    .line 686
    iget-object v0, p0, Lorg/icepdf/core/util/Library;->m_LazyObjectLoader:Lorg/icepdf/core/util/LazyObjectLoader;

    invoke-virtual {v0}, Lorg/icepdf/core/util/LazyObjectLoader;->dispose()V

    .line 688
    :cond_4
    return-void
.end method

.method public disposeFontResources()V
    .locals 5

    .prologue
    .line 657
    iget-object v4, p0, Lorg/icepdf/core/util/Library;->refs:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v4}, Ljava/util/concurrent/ConcurrentHashMap;->keySet()Ljava/util/Set;

    move-result-object v2

    .line 659
    .local v2, "test":Ljava/util/Set;, "Ljava/util/Set<Lorg/icepdf/core/pobjects/Reference;>;"
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/icepdf/core/pobjects/Reference;

    .line 660
    .local v1, "ref":Lorg/icepdf/core/pobjects/Reference;
    iget-object v4, p0, Lorg/icepdf/core/util/Library;->refs:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v4, v1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    .line 661
    .local v3, "tmp":Ljava/lang/Object;
    instance-of v4, v3, Lorg/icepdf/core/pobjects/fonts/Font;

    if-nez v4, :cond_1

    instance-of v4, v3, Lorg/icepdf/core/pobjects/fonts/FontDescriptor;

    if-eqz v4, :cond_0

    .line 663
    :cond_1
    iget-object v4, p0, Lorg/icepdf/core/util/Library;->refs:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v4, v1}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 666
    .end local v1    # "ref":Lorg/icepdf/core/pobjects/Reference;
    .end local v3    # "tmp":Ljava/lang/Object;
    :cond_2
    return-void
.end method

.method public getBoolean(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Boolean;
    .locals 2
    .param p1, "dictionaryEntries"    # Ljava/util/Hashtable;
    .param p2, "key"    # Ljava/lang/String;

    .prologue
    .line 321
    invoke-virtual {p0, p1, p2}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 322
    .local v0, "o":Ljava/lang/Object;
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 323
    check-cast v0, Ljava/lang/String;

    .end local v0    # "o":Ljava/lang/Object;
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v1

    .line 324
    :goto_0
    return-object v1

    .restart local v0    # "o":Ljava/lang/Object;
    :cond_0
    instance-of v1, v0, Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    check-cast v0, Ljava/lang/Boolean;

    .end local v0    # "o":Ljava/lang/Object;
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    :goto_1
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public getCacheManager()Lorg/icepdf/core/util/CacheManager;
    .locals 1

    .prologue
    .line 630
    iget-object v0, p0, Lorg/icepdf/core/util/Library;->cacheManager:Lorg/icepdf/core/util/CacheManager;

    return-object v0
.end method

.method public getCatalog()Lorg/icepdf/core/pobjects/Catalog;
    .locals 1

    .prologue
    .line 639
    iget-object v0, p0, Lorg/icepdf/core/util/Library;->catalog:Lorg/icepdf/core/pobjects/Catalog;

    return-object v0
.end method

.method public getDictionary(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/util/Hashtable;
    .locals 6
    .param p1, "dictionaryEntries"    # Ljava/util/Hashtable;
    .param p2, "key"    # Ljava/lang/String;

    .prologue
    .line 403
    invoke-virtual {p0, p1, p2}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    .line 404
    .local v2, "o":Ljava/lang/Object;
    instance-of v5, v2, Ljava/util/Hashtable;

    if-eqz v5, :cond_0

    .line 405
    check-cast v2, Ljava/util/Hashtable;

    .line 417
    .end local v2    # "o":Ljava/lang/Object;
    :goto_0
    return-object v2

    .line 406
    .restart local v2    # "o":Ljava/lang/Object;
    :cond_0
    instance-of v5, v2, Ljava/util/Vector;

    if-eqz v5, :cond_3

    move-object v4, v2

    .line 407
    check-cast v4, Ljava/util/Vector;

    .line 408
    .local v4, "v":Ljava/util/Vector;
    new-instance v1, Ljava/util/Hashtable;

    invoke-direct {v1}, Ljava/util/Hashtable;-><init>()V

    .line 409
    .local v1, "h1":Ljava/util/Hashtable;
    invoke-virtual {v4}, Ljava/util/Vector;->elements()Ljava/util/Enumeration;

    move-result-object v0

    .local v0, "e":Ljava/util/Enumeration;
    :cond_1
    :goto_1
    invoke-interface {v0}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 410
    invoke-interface {v0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v3

    .line 411
    .local v3, "o1":Ljava/lang/Object;
    instance-of v5, v3, Ljava/util/Map;

    if-eqz v5, :cond_1

    .line 412
    check-cast v3, Ljava/util/Map;

    .end local v3    # "o1":Ljava/lang/Object;
    invoke-virtual {v1, v3}, Ljava/util/Hashtable;->putAll(Ljava/util/Map;)V

    goto :goto_1

    :cond_2
    move-object v2, v1

    .line 415
    goto :goto_0

    .line 417
    .end local v0    # "e":Ljava/util/Enumeration;
    .end local v1    # "h1":Ljava/util/Hashtable;
    .end local v4    # "v":Ljava/util/Vector;
    :cond_3
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public getFloat(Ljava/util/Hashtable;Ljava/lang/String;)F
    .locals 2
    .param p1, "dictionaryEntries"    # Ljava/util/Hashtable;
    .param p2, "key"    # Ljava/lang/String;

    .prologue
    .line 338
    invoke-virtual {p0, p1, p2}, Lorg/icepdf/core/util/Library;->getNumber(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Number;

    move-result-object v0

    .line 339
    .local v0, "n":Ljava/lang/Number;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Number;->floatValue()F

    move-result v1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getICCBased(Lorg/icepdf/core/pobjects/Reference;)Lorg/icepdf/core/pobjects/graphics/ICCBased;
    .locals 4
    .param p1, "ref"    # Lorg/icepdf/core/pobjects/Reference;

    .prologue
    .line 448
    iget-object v3, p0, Lorg/icepdf/core/util/Library;->lookupReference2ICCBased:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v3, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/icepdf/core/pobjects/graphics/ICCBased;

    .line 449
    .local v0, "cs":Lorg/icepdf/core/pobjects/graphics/ICCBased;
    if-nez v0, :cond_0

    .line 450
    invoke-virtual {p0, p1}, Lorg/icepdf/core/util/Library;->getObject(Lorg/icepdf/core/pobjects/Reference;)Ljava/lang/Object;

    move-result-object v1

    .line 451
    .local v1, "obj":Ljava/lang/Object;
    instance-of v3, v1, Lorg/icepdf/core/pobjects/Stream;

    if-eqz v3, :cond_0

    move-object v2, v1

    .line 452
    check-cast v2, Lorg/icepdf/core/pobjects/Stream;

    .line 453
    .local v2, "stream":Lorg/icepdf/core/pobjects/Stream;
    new-instance v0, Lorg/icepdf/core/pobjects/graphics/ICCBased;

    .end local v0    # "cs":Lorg/icepdf/core/pobjects/graphics/ICCBased;
    invoke-direct {v0, p0, v2}, Lorg/icepdf/core/pobjects/graphics/ICCBased;-><init>(Lorg/icepdf/core/util/Library;Lorg/icepdf/core/pobjects/Stream;)V

    .line 454
    .restart local v0    # "cs":Lorg/icepdf/core/pobjects/graphics/ICCBased;
    iget-object v3, p0, Lorg/icepdf/core/util/Library;->lookupReference2ICCBased:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v3, p1, v0}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 457
    .end local v1    # "obj":Ljava/lang/Object;
    .end local v2    # "stream":Lorg/icepdf/core/pobjects/Stream;
    :cond_0
    return-object v0
.end method

.method public getInt(Ljava/util/Hashtable;Ljava/lang/String;)I
    .locals 2
    .param p1, "dictionaryEntries"    # Ljava/util/Hashtable;
    .param p2, "key"    # Ljava/lang/String;

    .prologue
    .line 353
    invoke-virtual {p0, p1, p2}, Lorg/icepdf/core/util/Library;->getNumber(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Number;

    move-result-object v0

    .line 354
    .local v0, "n":Ljava/lang/Number;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getLong(Ljava/util/Hashtable;Ljava/lang/String;)J
    .locals 4
    .param p1, "dictionaryEntries"    # Ljava/util/Hashtable;
    .param p2, "key"    # Ljava/lang/String;

    .prologue
    .line 368
    invoke-virtual {p0, p1, p2}, Lorg/icepdf/core/util/Library;->getNumber(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Number;

    move-result-object v0

    .line 369
    .local v0, "n":Ljava/lang/Number;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Number;->longValue()J

    move-result-wide v2

    :goto_0
    return-wide v2

    :cond_0
    const-wide/16 v2, 0x0

    goto :goto_0
.end method

.method public getName(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "dictionaryEntries"    # Ljava/util/Hashtable;
    .param p2, "key"    # Ljava/lang/String;

    .prologue
    .line 383
    invoke-virtual {p0, p1, p2}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 384
    .local v0, "o":Ljava/lang/Object;
    if-eqz v0, :cond_0

    .line 385
    instance-of v1, v0, Lorg/icepdf/core/pobjects/Name;

    if-eqz v1, :cond_0

    .line 386
    check-cast v0, Lorg/icepdf/core/pobjects/Name;

    .end local v0    # "o":Ljava/lang/Object;
    invoke-virtual {v0}, Lorg/icepdf/core/pobjects/Name;->getName()Ljava/lang/String;

    move-result-object v1

    .line 389
    :goto_0
    return-object v1

    .restart local v0    # "o":Ljava/lang/Object;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getNumber(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Number;
    .locals 2
    .param p1, "dictionaryEntries"    # Ljava/util/Hashtable;
    .param p2, "key"    # Ljava/lang/String;

    .prologue
    .line 304
    invoke-virtual {p0, p1, p2}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 305
    .local v0, "o":Ljava/lang/Object;
    instance-of v1, v0, Ljava/lang/Number;

    if-eqz v1, :cond_0

    .line 306
    check-cast v0, Ljava/lang/Number;

    .line 307
    .end local v0    # "o":Ljava/lang/Object;
    :goto_0
    return-object v0

    .restart local v0    # "o":Ljava/lang/Object;
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getObject(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "referenceObject"    # Ljava/lang/Object;

    .prologue
    .line 251
    instance-of v0, p1, Lorg/icepdf/core/pobjects/Reference;

    if-eqz v0, :cond_0

    .line 252
    check-cast p1, Lorg/icepdf/core/pobjects/Reference;

    .end local p1    # "referenceObject":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lorg/icepdf/core/util/Library;->getObject(Lorg/icepdf/core/pobjects/Reference;)Ljava/lang/Object;

    move-result-object p1

    .line 254
    :cond_0
    return-object p1
.end method

.method public getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;
    .locals 5
    .param p1, "dictionaryEntries"    # Ljava/util/Hashtable;
    .param p2, "key"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 148
    if-nez p1, :cond_1

    move-object v0, v3

    .line 171
    :cond_0
    :goto_0
    return-object v0

    .line 151
    :cond_1
    invoke-virtual {p1, p2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .line 154
    .local v2, "oS":Ljava/lang/Object;
    new-instance v4, Lorg/icepdf/core/pobjects/Name;

    invoke-direct {v4, p2}, Lorg/icepdf/core/pobjects/Name;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v4}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 156
    .local v1, "oN":Ljava/lang/Object;
    const/4 v0, 0x0

    .line 157
    .local v0, "o":Ljava/lang/Object;
    if-eqz v2, :cond_2

    .line 159
    move-object v0, v2

    .line 167
    :goto_1
    if-nez v0, :cond_3

    move-object v0, v3

    .line 168
    goto :goto_0

    .line 163
    :cond_2
    move-object v0, v1

    goto :goto_1

    .line 169
    :cond_3
    instance-of v3, v0, Lorg/icepdf/core/pobjects/Reference;

    if-eqz v3, :cond_0

    .line 170
    check-cast v0, Lorg/icepdf/core/pobjects/Reference;

    .end local v0    # "o":Ljava/lang/Object;
    invoke-virtual {p0, v0}, Lorg/icepdf/core/util/Library;->getObject(Lorg/icepdf/core/pobjects/Reference;)Ljava/lang/Object;

    move-result-object v0

    .restart local v0    # "o":Ljava/lang/Object;
    goto :goto_0
.end method

.method public getObject(Lorg/icepdf/core/pobjects/Reference;)Ljava/lang/Object;
    .locals 2
    .param p1, "reference"    # Lorg/icepdf/core/pobjects/Reference;

    .prologue
    .line 101
    :goto_0
    iget-object v1, p0, Lorg/icepdf/core/util/Library;->refs:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 102
    .local v0, "ob":Ljava/lang/Object;
    if-nez v0, :cond_0

    iget-object v1, p0, Lorg/icepdf/core/util/Library;->m_LazyObjectLoader:Lorg/icepdf/core/util/LazyObjectLoader;

    if-eqz v1, :cond_0

    .line 103
    iget-object v1, p0, Lorg/icepdf/core/util/Library;->m_LazyObjectLoader:Lorg/icepdf/core/util/LazyObjectLoader;

    invoke-virtual {v1, p1}, Lorg/icepdf/core/util/LazyObjectLoader;->loadObject(Lorg/icepdf/core/pobjects/Reference;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 104
    iget-object v1, p0, Lorg/icepdf/core/util/Library;->refs:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 109
    :cond_0
    if-nez v0, :cond_2

    .line 115
    :cond_1
    return-object v0

    .line 111
    :cond_2
    instance-of v1, v0, Lorg/icepdf/core/pobjects/Reference;

    if-eqz v1, :cond_1

    move-object p1, v0

    .line 113
    check-cast p1, Lorg/icepdf/core/pobjects/Reference;

    goto :goto_0
.end method

.method public getObjectReference(Ljava/util/Hashtable;Ljava/lang/String;)Lorg/icepdf/core/pobjects/Reference;
    .locals 3
    .param p1, "dictionaryEntries"    # Ljava/util/Hashtable;
    .param p2, "key"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 555
    if-nez p1, :cond_1

    .line 572
    :cond_0
    return-object v0

    .line 558
    :cond_1
    invoke-virtual {p1, p2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 560
    .local v1, "o":Ljava/lang/Object;
    if-nez v1, :cond_2

    .line 562
    new-instance v2, Lorg/icepdf/core/pobjects/Name;

    invoke-direct {v2, p2}, Lorg/icepdf/core/pobjects/Name;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 565
    :cond_2
    if-eqz v1, :cond_0

    .line 567
    const/4 v0, 0x0

    .line 568
    .local v0, "currentRef":Lorg/icepdf/core/pobjects/Reference;
    :goto_0
    if-eqz v1, :cond_0

    instance-of v2, v1, Lorg/icepdf/core/pobjects/Reference;

    if-eqz v2, :cond_0

    move-object v0, v1

    .line 569
    check-cast v0, Lorg/icepdf/core/pobjects/Reference;

    .line 570
    invoke-virtual {p0, v0}, Lorg/icepdf/core/util/Library;->getObject(Lorg/icepdf/core/pobjects/Reference;)Ljava/lang/Object;

    move-result-object v1

    goto :goto_0
.end method

.method public getRectangle(Ljava/util/Hashtable;Ljava/lang/String;)Landroid/graphics/RectF;
    .locals 2
    .param p1, "dictionaryEntries"    # Ljava/util/Hashtable;
    .param p2, "key"    # Ljava/lang/String;

    .prologue
    .line 429
    invoke-virtual {p0, p1, p2}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Vector;

    .line 430
    .local v0, "v":Ljava/util/Vector;
    if-eqz v0, :cond_0

    .line 432
    new-instance v1, Lorg/icepdf/core/pobjects/PRectangle;

    invoke-direct {v1, v0}, Lorg/icepdf/core/pobjects/PRectangle;-><init>(Ljava/util/Vector;)V

    invoke-virtual {v1}, Lorg/icepdf/core/pobjects/PRectangle;->toJava2dCoordinates()Landroid/graphics/RectF;

    move-result-object v1

    .line 434
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getReference(Ljava/util/Hashtable;Ljava/lang/String;)Lorg/icepdf/core/pobjects/Reference;
    .locals 2
    .param p1, "dictionaryEntries"    # Ljava/util/Hashtable;
    .param p2, "key"    # Ljava/lang/String;

    .prologue
    .line 208
    invoke-virtual {p1, p2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 210
    .local v0, "ref":Ljava/lang/Object;
    if-nez v0, :cond_0

    .line 212
    new-instance v1, Lorg/icepdf/core/pobjects/Name;

    invoke-direct {v1, p2}, Lorg/icepdf/core/pobjects/Name;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 215
    :cond_0
    instance-of v1, v0, Lorg/icepdf/core/pobjects/Reference;

    if-eqz v1, :cond_1

    .line 216
    check-cast v0, Lorg/icepdf/core/pobjects/Reference;

    .line 219
    .end local v0    # "ref":Ljava/lang/Object;
    :goto_0
    return-object v0

    .restart local v0    # "ref":Ljava/lang/Object;
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getResources(Ljava/util/Hashtable;Ljava/lang/String;)Lorg/icepdf/core/pobjects/Resources;
    .locals 5
    .param p1, "dictionaryEntries"    # Ljava/util/Hashtable;
    .param p2, "key"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 461
    if-nez p1, :cond_0

    move-object v1, v3

    .line 484
    :goto_0
    return-object v1

    .line 463
    :cond_0
    invoke-virtual {p1, p2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 465
    .local v1, "ob":Ljava/lang/Object;
    if-nez v1, :cond_1

    .line 467
    new-instance v4, Lorg/icepdf/core/pobjects/Name;

    invoke-direct {v4, p2}, Lorg/icepdf/core/pobjects/Name;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v4}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 471
    :cond_1
    if-nez v1, :cond_2

    move-object v1, v3

    .line 472
    goto :goto_0

    .line 473
    :cond_2
    instance-of v4, v1, Lorg/icepdf/core/pobjects/Resources;

    if-eqz v4, :cond_3

    .line 474
    check-cast v1, Lorg/icepdf/core/pobjects/Resources;

    goto :goto_0

    .line 475
    :cond_3
    instance-of v4, v1, Lorg/icepdf/core/pobjects/Reference;

    if-eqz v4, :cond_4

    move-object v2, v1

    .line 476
    check-cast v2, Lorg/icepdf/core/pobjects/Reference;

    .line 477
    .local v2, "reference":Lorg/icepdf/core/pobjects/Reference;
    invoke-virtual {p0, v2}, Lorg/icepdf/core/util/Library;->getResources(Lorg/icepdf/core/pobjects/Reference;)Lorg/icepdf/core/pobjects/Resources;

    move-result-object v1

    goto :goto_0

    .line 478
    .end local v2    # "reference":Lorg/icepdf/core/pobjects/Reference;
    :cond_4
    instance-of v4, v1, Ljava/util/Hashtable;

    if-eqz v4, :cond_5

    move-object v0, v1

    .line 479
    check-cast v0, Ljava/util/Hashtable;

    .line 480
    .local v0, "ht":Ljava/util/Hashtable;
    new-instance v3, Lorg/icepdf/core/pobjects/Resources;

    invoke-direct {v3, p0, v0}, Lorg/icepdf/core/pobjects/Resources;-><init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V

    .line 481
    .local v3, "resources":Lorg/icepdf/core/pobjects/Resources;
    invoke-virtual {p1, p2, v3}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v1, v3

    .line 482
    goto :goto_0

    .end local v0    # "ht":Ljava/util/Hashtable;
    .end local v3    # "resources":Lorg/icepdf/core/pobjects/Resources;
    :cond_5
    move-object v1, v3

    .line 484
    goto :goto_0
.end method

.method public getResources(Lorg/icepdf/core/pobjects/Reference;)Lorg/icepdf/core/pobjects/Resources;
    .locals 4
    .param p1, "reference"    # Lorg/icepdf/core/pobjects/Reference;

    .prologue
    const/4 v2, 0x0

    .line 489
    :goto_0
    iget-object v3, p0, Lorg/icepdf/core/util/Library;->refs:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v3, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 490
    .local v1, "ob":Ljava/lang/Object;
    if-nez v1, :cond_0

    iget-object v3, p0, Lorg/icepdf/core/util/Library;->m_LazyObjectLoader:Lorg/icepdf/core/util/LazyObjectLoader;

    if-eqz v3, :cond_0

    .line 491
    iget-object v3, p0, Lorg/icepdf/core/util/Library;->m_LazyObjectLoader:Lorg/icepdf/core/util/LazyObjectLoader;

    invoke-virtual {v3, p1}, Lorg/icepdf/core/util/LazyObjectLoader;->loadObject(Lorg/icepdf/core/pobjects/Reference;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 492
    iget-object v3, p0, Lorg/icepdf/core/util/Library;->refs:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v3, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 495
    :cond_0
    if-nez v1, :cond_1

    move-object v1, v2

    .line 510
    .end local v1    # "ob":Ljava/lang/Object;
    :goto_1
    return-object v1

    .line 497
    .restart local v1    # "ob":Ljava/lang/Object;
    :cond_1
    instance-of v3, v1, Lorg/icepdf/core/pobjects/Resources;

    if-eqz v3, :cond_2

    .line 498
    check-cast v1, Lorg/icepdf/core/pobjects/Resources;

    goto :goto_1

    .line 499
    :cond_2
    instance-of v3, v1, Lorg/icepdf/core/pobjects/Reference;

    if-eqz v3, :cond_3

    move-object p1, v1

    .line 500
    check-cast p1, Lorg/icepdf/core/pobjects/Reference;

    .line 501
    goto :goto_0

    .line 502
    :cond_3
    instance-of v3, v1, Ljava/util/Hashtable;

    if-eqz v3, :cond_4

    move-object v0, v1

    .line 503
    check-cast v0, Ljava/util/Hashtable;

    .line 504
    .local v0, "ht":Ljava/util/Hashtable;
    new-instance v2, Lorg/icepdf/core/pobjects/Resources;

    invoke-direct {v2, p0, v0}, Lorg/icepdf/core/pobjects/Resources;-><init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V

    .line 505
    .local v2, "resources":Lorg/icepdf/core/pobjects/Resources;
    invoke-virtual {p0, v2, p1}, Lorg/icepdf/core/util/Library;->addObject(Ljava/lang/Object;Lorg/icepdf/core/pobjects/Reference;)V

    move-object v1, v2

    .line 506
    goto :goto_1

    .end local v0    # "ht":Ljava/util/Hashtable;
    .end local v2    # "resources":Lorg/icepdf/core/pobjects/Resources;
    :cond_4
    move-object v1, v2

    .line 510
    goto :goto_1
.end method

.method public getSecurityManager()Lorg/icepdf/core/pobjects/security/SecurityManager;
    .locals 1

    .prologue
    .line 591
    iget-object v0, p0, Lorg/icepdf/core/util/Library;->securityManager:Lorg/icepdf/core/pobjects/security/SecurityManager;

    return-object v0
.end method

.method public getStateManager()Lorg/icepdf/core/pobjects/StateManager;
    .locals 1

    .prologue
    .line 229
    iget-object v0, p0, Lorg/icepdf/core/util/Library;->stateManager:Lorg/icepdf/core/pobjects/StateManager;

    return-object v0
.end method

.method public getTrailerByFilePosition(J)Lorg/icepdf/core/pobjects/PTrailer;
    .locals 1
    .param p1, "position"    # J

    .prologue
    .line 86
    iget-object v0, p0, Lorg/icepdf/core/util/Library;->m_LazyObjectLoader:Lorg/icepdf/core/util/LazyObjectLoader;

    if-nez v0, :cond_0

    .line 87
    const/4 v0, 0x0

    .line 88
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lorg/icepdf/core/util/Library;->m_LazyObjectLoader:Lorg/icepdf/core/util/LazyObjectLoader;

    invoke-virtual {v0, p1, p2}, Lorg/icepdf/core/util/LazyObjectLoader;->loadTrailer(J)Lorg/icepdf/core/pobjects/PTrailer;

    move-result-object v0

    goto :goto_0
.end method

.method public isEncrypted()Z
    .locals 1

    .prologue
    .line 581
    iget-boolean v0, p0, Lorg/icepdf/core/util/Library;->isEncrypted:Z

    return v0
.end method

.method public isLinearTraversal()Z
    .locals 1

    .prologue
    .line 621
    iget-boolean v0, p0, Lorg/icepdf/core/util/Library;->isLinearTraversal:Z

    return v0
.end method

.method public isReference(Ljava/util/Hashtable;Ljava/lang/String;)Z
    .locals 3
    .param p1, "dictionaryEntries"    # Ljava/util/Hashtable;
    .param p2, "key"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 184
    if-nez p1, :cond_1

    .line 193
    :cond_0
    :goto_0
    return v1

    .line 187
    :cond_1
    invoke-virtual {p1, p2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 188
    .local v0, "o":Ljava/lang/Object;
    if-nez v0, :cond_2

    .line 190
    new-instance v2, Lorg/icepdf/core/pobjects/Name;

    invoke-direct {v2, p2}, Lorg/icepdf/core/pobjects/Name;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 193
    :cond_2
    if-eqz p1, :cond_0

    instance-of v2, v0, Lorg/icepdf/core/pobjects/Reference;

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isValidEntry(Ljava/util/Hashtable;Ljava/lang/String;)Z
    .locals 3
    .param p1, "dictionaryEntries"    # Ljava/util/Hashtable;
    .param p2, "key"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 267
    if-nez p1, :cond_1

    .line 277
    :cond_0
    :goto_0
    return v1

    .line 270
    :cond_1
    invoke-virtual {p1, p2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 272
    .local v0, "o":Ljava/lang/Object;
    if-nez v0, :cond_2

    .line 274
    new-instance v2, Lorg/icepdf/core/pobjects/Name;

    invoke-direct {v2, p2}, Lorg/icepdf/core/pobjects/Name;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 277
    :cond_2
    if-eqz v0, :cond_0

    instance-of v2, v0, Lorg/icepdf/core/pobjects/Reference;

    if-eqz v2, :cond_3

    check-cast v0, Lorg/icepdf/core/pobjects/Reference;

    .end local v0    # "o":Ljava/lang/Object;
    invoke-virtual {p0, v0}, Lorg/icepdf/core/util/Library;->isValidEntry(Lorg/icepdf/core/pobjects/Reference;)Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_3
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isValidEntry(Lorg/icepdf/core/pobjects/Reference;)Z
    .locals 2
    .param p1, "reference"    # Lorg/icepdf/core/pobjects/Reference;

    .prologue
    .line 287
    iget-object v1, p0, Lorg/icepdf/core/util/Library;->refs:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 288
    .local v0, "ob":Ljava/lang/Object;
    if-nez v0, :cond_0

    iget-object v1, p0, Lorg/icepdf/core/util/Library;->m_LazyObjectLoader:Lorg/icepdf/core/util/LazyObjectLoader;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lorg/icepdf/core/util/Library;->m_LazyObjectLoader:Lorg/icepdf/core/util/LazyObjectLoader;

    invoke-virtual {v1, p1}, Lorg/icepdf/core/util/LazyObjectLoader;->haveEntry(Lorg/icepdf/core/pobjects/Reference;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public removeObject(Lorg/icepdf/core/pobjects/Reference;)V
    .locals 1
    .param p1, "objetReference"    # Lorg/icepdf/core/pobjects/Reference;

    .prologue
    .line 529
    if-eqz p1, :cond_0

    .line 530
    iget-object v0, p0, Lorg/icepdf/core/util/Library;->refs:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 532
    :cond_0
    return-void
.end method

.method public setCatalog(Lorg/icepdf/core/pobjects/Catalog;)V
    .locals 0
    .param p1, "c"    # Lorg/icepdf/core/pobjects/Catalog;

    .prologue
    .line 648
    iput-object p1, p0, Lorg/icepdf/core/util/Library;->catalog:Lorg/icepdf/core/pobjects/Catalog;

    .line 649
    return-void
.end method

.method public setEncrypted(Z)V
    .locals 0
    .param p1, "flag"    # Z

    .prologue
    .line 600
    iput-boolean p1, p0, Lorg/icepdf/core/util/Library;->isEncrypted:Z

    .line 601
    return-void
.end method

.method public setLazyObjectLoader(Lorg/icepdf/core/util/LazyObjectLoader;)V
    .locals 2
    .param p1, "lol"    # Lorg/icepdf/core/util/LazyObjectLoader;

    .prologue
    .line 74
    iput-object p1, p0, Lorg/icepdf/core/util/Library;->m_LazyObjectLoader:Lorg/icepdf/core/util/LazyObjectLoader;

    .line 75
    iget-object v0, p0, Lorg/icepdf/core/util/Library;->m_LazyObjectLoader:Lorg/icepdf/core/util/LazyObjectLoader;

    if-eqz v0, :cond_0

    .line 76
    iget-object v0, p0, Lorg/icepdf/core/util/Library;->memoryManager:Lorg/icepdf/core/util/MemoryManager;

    iget-object v1, p0, Lorg/icepdf/core/util/Library;->m_LazyObjectLoader:Lorg/icepdf/core/util/LazyObjectLoader;

    invoke-virtual {v0, v1}, Lorg/icepdf/core/util/MemoryManager;->registerMemoryManagerDelegate(Lorg/icepdf/core/util/MemoryManagerDelegate;)V

    .line 77
    :cond_0
    return-void
.end method

.method public setLinearTraversal()V
    .locals 1

    .prologue
    .line 610
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/icepdf/core/util/Library;->isLinearTraversal:Z

    .line 611
    return-void
.end method

.method public setStateManager(Lorg/icepdf/core/pobjects/StateManager;)V
    .locals 0
    .param p1, "stateManager"    # Lorg/icepdf/core/pobjects/StateManager;

    .prologue
    .line 239
    iput-object p1, p0, Lorg/icepdf/core/util/Library;->stateManager:Lorg/icepdf/core/pobjects/StateManager;

    .line 240
    return-void
.end method
