.class public Lorg/icepdf/core/util/IncrementalUpdater;
.super Ljava/lang/Object;
.source "IncrementalUpdater.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/icepdf/core/util/IncrementalUpdater$Entry;
    }
.end annotation


# static fields
.field private static final BEGIN_ARRAY:[B

.field private static final BEGIN_DICTIONARY:[B

.field private static final BEGIN_HEX_STRING:[B

.field private static final BEGIN_LITERAL_STRING:[B

.field private static final BEGIN_OBJECT:[B

.field private static final COMMENT_EOF:[B

.field private static final END_ARRAY:[B

.field private static final END_DICTIONARY:[B

.field private static final END_HEX_STRING:[B

.field private static final END_LITERAL_STRING:[B

.field private static final END_OBJECT:[B

.field private static final FALSE:[B

.field private static final NAME:[B

.field private static final NEWLINE:[B

.field private static final PRETTY:Z

.field private static final REFERENCE:[B

.field private static final SPACE:[B

.field private static final STARTXREF:[B

.field private static final TRAILER:[B

.field private static final TRUE:[B

.field private static final XREF:[B

.field private static final logger:Ljava/util/logging/Logger;


# instance fields
.field private entries:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lorg/icepdf/core/util/IncrementalUpdater$Entry;",
            ">;"
        }
    .end annotation
.end field

.field private output:Lorg/icepdf/core/io/CountingOutputStream;

.field private startingPosition:J

.field private xrefPosition:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 40
    const-class v0, Lorg/icepdf/core/util/IncrementalUpdater;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Lorg/icepdf/core/util/IncrementalUpdater;->logger:Ljava/util/logging/Logger;

    .line 45
    const-string/jumbo v0, " "

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    sput-object v0, Lorg/icepdf/core/util/IncrementalUpdater;->SPACE:[B

    .line 46
    const-string/jumbo v0, "\r\n"

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    sput-object v0, Lorg/icepdf/core/util/IncrementalUpdater;->NEWLINE:[B

    .line 47
    const-string/jumbo v0, "true"

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    sput-object v0, Lorg/icepdf/core/util/IncrementalUpdater;->TRUE:[B

    .line 48
    const-string/jumbo v0, "false"

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    sput-object v0, Lorg/icepdf/core/util/IncrementalUpdater;->FALSE:[B

    .line 49
    const-string/jumbo v0, "/"

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    sput-object v0, Lorg/icepdf/core/util/IncrementalUpdater;->NAME:[B

    .line 50
    const-string/jumbo v0, "R"

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    sput-object v0, Lorg/icepdf/core/util/IncrementalUpdater;->REFERENCE:[B

    .line 53
    const-string/jumbo v0, "obj\r\n"

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    sput-object v0, Lorg/icepdf/core/util/IncrementalUpdater;->BEGIN_OBJECT:[B

    .line 54
    const-string/jumbo v0, "\r\nendobj\r\n"

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    sput-object v0, Lorg/icepdf/core/util/IncrementalUpdater;->END_OBJECT:[B

    .line 55
    const-string/jumbo v0, "<<"

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    sput-object v0, Lorg/icepdf/core/util/IncrementalUpdater;->BEGIN_DICTIONARY:[B

    .line 56
    const-string/jumbo v0, ">>"

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    sput-object v0, Lorg/icepdf/core/util/IncrementalUpdater;->END_DICTIONARY:[B

    .line 57
    const-string/jumbo v0, "["

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    sput-object v0, Lorg/icepdf/core/util/IncrementalUpdater;->BEGIN_ARRAY:[B

    .line 58
    const-string/jumbo v0, "]"

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    sput-object v0, Lorg/icepdf/core/util/IncrementalUpdater;->END_ARRAY:[B

    .line 59
    const-string/jumbo v0, "("

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    sput-object v0, Lorg/icepdf/core/util/IncrementalUpdater;->BEGIN_LITERAL_STRING:[B

    .line 60
    const-string/jumbo v0, ")"

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    sput-object v0, Lorg/icepdf/core/util/IncrementalUpdater;->END_LITERAL_STRING:[B

    .line 61
    const-string/jumbo v0, "<"

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    sput-object v0, Lorg/icepdf/core/util/IncrementalUpdater;->BEGIN_HEX_STRING:[B

    .line 62
    const-string/jumbo v0, ">"

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    sput-object v0, Lorg/icepdf/core/util/IncrementalUpdater;->END_HEX_STRING:[B

    .line 64
    const-string/jumbo v0, "xref\r\n"

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    sput-object v0, Lorg/icepdf/core/util/IncrementalUpdater;->XREF:[B

    .line 65
    const-string/jumbo v0, "trailer\r\n"

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    sput-object v0, Lorg/icepdf/core/util/IncrementalUpdater;->TRAILER:[B

    .line 66
    const-string/jumbo v0, "\r\n\r\nstartxref\r\n"

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    sput-object v0, Lorg/icepdf/core/util/IncrementalUpdater;->STARTXREF:[B

    .line 67
    const-string/jumbo v0, "\r\n%%EOF\r\n"

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    sput-object v0, Lorg/icepdf/core/util/IncrementalUpdater;->COMMENT_EOF:[B

    return-void
.end method

.method private constructor <init>(Ljava/io/OutputStream;J)V
    .locals 2
    .param p1, "out"    # Ljava/io/OutputStream;
    .param p2, "sp"    # J

    .prologue
    .line 124
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 125
    new-instance v0, Lorg/icepdf/core/io/CountingOutputStream;

    invoke-direct {v0, p1}, Lorg/icepdf/core/io/CountingOutputStream;-><init>(Ljava/io/OutputStream;)V

    iput-object v0, p0, Lorg/icepdf/core/util/IncrementalUpdater;->output:Lorg/icepdf/core/io/CountingOutputStream;

    .line 126
    iput-wide p2, p0, Lorg/icepdf/core/util/IncrementalUpdater;->startingPosition:J

    .line 127
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0x20

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lorg/icepdf/core/util/IncrementalUpdater;->entries:Ljava/util/ArrayList;

    .line 128
    return-void
.end method

.method private addEntry(Lorg/icepdf/core/util/IncrementalUpdater$Entry;)V
    .locals 7
    .param p1, "entry"    # Lorg/icepdf/core/util/IncrementalUpdater$Entry;

    .prologue
    .line 669
    invoke-virtual {p1}, Lorg/icepdf/core/util/IncrementalUpdater$Entry;->getReference()Lorg/icepdf/core/pobjects/Reference;

    move-result-object v4

    invoke-virtual {v4}, Lorg/icepdf/core/pobjects/Reference;->getObjectNumber()I

    move-result v0

    .line 670
    .local v0, "entryObjNum":I
    iget-object v4, p0, Lorg/icepdf/core/util/IncrementalUpdater;->entries:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 671
    .local v1, "index":I
    :goto_0
    if-lez v1, :cond_1

    .line 672
    iget-object v4, p0, Lorg/icepdf/core/util/IncrementalUpdater;->entries:Ljava/util/ArrayList;

    add-int/lit8 v5, v1, -0x1

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/icepdf/core/util/IncrementalUpdater$Entry;

    .line 673
    .local v2, "prev":Lorg/icepdf/core/util/IncrementalUpdater$Entry;
    invoke-virtual {v2}, Lorg/icepdf/core/util/IncrementalUpdater$Entry;->getReference()Lorg/icepdf/core/pobjects/Reference;

    move-result-object v4

    invoke-virtual {v4}, Lorg/icepdf/core/pobjects/Reference;->getObjectNumber()I

    move-result v3

    .line 674
    .local v3, "prevObjNum":I
    if-ne v3, v0, :cond_0

    .line 676
    new-instance v4, Ljava/lang/IllegalArgumentException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "Multiple entries with same object number: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 679
    :cond_0
    if-ge v3, v0, :cond_2

    .line 683
    .end local v2    # "prev":Lorg/icepdf/core/util/IncrementalUpdater$Entry;
    .end local v3    # "prevObjNum":I
    :cond_1
    iget-object v4, p0, Lorg/icepdf/core/util/IncrementalUpdater;->entries:Ljava/util/ArrayList;

    invoke-virtual {v4, v1, p1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 684
    return-void

    .line 681
    .restart local v2    # "prev":Lorg/icepdf/core/util/IncrementalUpdater$Entry;
    .restart local v3    # "prevObjNum":I
    :cond_2
    add-int/lit8 v1, v1, -0x1

    .line 682
    goto :goto_0
.end method

.method public static appendIncrementalUpdate(Lorg/icepdf/core/pobjects/Document;Ljava/io/OutputStream;J)J
    .locals 6
    .param p0, "document"    # Lorg/icepdf/core/pobjects/Document;
    .param p1, "out"    # Ljava/io/OutputStream;
    .param p2, "documentLength"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 92
    sget-object v3, Lorg/icepdf/core/util/IncrementalUpdater;->logger:Ljava/util/logging/Logger;

    sget-object v4, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    invoke-virtual {v3, v4}, Ljava/util/logging/Logger;->isLoggable(Ljava/util/logging/Level;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 93
    invoke-virtual {p0}, Lorg/icepdf/core/pobjects/Document;->getStateManager()Lorg/icepdf/core/pobjects/StateManager;

    move-result-object v3

    invoke-virtual {v3}, Lorg/icepdf/core/pobjects/StateManager;->isChanged()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 94
    sget-object v3, Lorg/icepdf/core/util/IncrementalUpdater;->logger:Ljava/util/logging/Logger;

    const-string/jumbo v4, "Have changes, will append incremental update"

    invoke-virtual {v3, v4}, Ljava/util/logging/Logger;->fine(Ljava/lang/String;)V

    .line 100
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lorg/icepdf/core/pobjects/Document;->getStateManager()Lorg/icepdf/core/pobjects/StateManager;

    move-result-object v3

    invoke-virtual {v3}, Lorg/icepdf/core/pobjects/StateManager;->isChanged()Z

    move-result v3

    if-nez v3, :cond_2

    .line 101
    const-wide/16 v4, 0x0

    .line 120
    :goto_1
    return-wide v4

    .line 96
    :cond_1
    sget-object v3, Lorg/icepdf/core/util/IncrementalUpdater;->logger:Ljava/util/logging/Logger;

    const-string/jumbo v4, "No changes, will not append incremental update"

    invoke-virtual {v3, v4}, Ljava/util/logging/Logger;->fine(Ljava/lang/String;)V

    goto :goto_0

    .line 103
    :cond_2
    new-instance v2, Lorg/icepdf/core/util/IncrementalUpdater;

    invoke-direct {v2, p1, p2, p3}, Lorg/icepdf/core/util/IncrementalUpdater;-><init>(Ljava/io/OutputStream;J)V

    .line 105
    .local v2, "updater":Lorg/icepdf/core/util/IncrementalUpdater;
    invoke-direct {v2}, Lorg/icepdf/core/util/IncrementalUpdater;->begin()V

    .line 107
    invoke-virtual {p0}, Lorg/icepdf/core/pobjects/Document;->getStateManager()Lorg/icepdf/core/pobjects/StateManager;

    move-result-object v3

    invoke-virtual {v3}, Lorg/icepdf/core/pobjects/StateManager;->iteratorSortedByObjectNumber()Ljava/util/Iterator;

    move-result-object v0

    .line 109
    .local v0, "changes":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/icepdf/core/pobjects/PObject;>;"
    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 110
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/icepdf/core/pobjects/PObject;

    .line 111
    .local v1, "pobject":Lorg/icepdf/core/pobjects/PObject;
    invoke-virtual {v1}, Lorg/icepdf/core/pobjects/PObject;->getReference()Lorg/icepdf/core/pobjects/Reference;

    move-result-object v3

    invoke-virtual {v1}, Lorg/icepdf/core/pobjects/PObject;->getObject()Ljava/lang/Object;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lorg/icepdf/core/util/IncrementalUpdater;->writeObject(Lorg/icepdf/core/pobjects/Reference;Ljava/lang/Object;)V

    goto :goto_2

    .line 115
    .end local v1    # "pobject":Lorg/icepdf/core/pobjects/PObject;
    :cond_3
    invoke-direct {v2}, Lorg/icepdf/core/util/IncrementalUpdater;->writeXRefTable()V

    .line 118
    invoke-virtual {p0}, Lorg/icepdf/core/pobjects/Document;->getStateManager()Lorg/icepdf/core/pobjects/StateManager;

    move-result-object v3

    invoke-virtual {v3}, Lorg/icepdf/core/pobjects/StateManager;->getTrailer()Lorg/icepdf/core/pobjects/PTrailer;

    move-result-object v3

    invoke-direct {v2, v3}, Lorg/icepdf/core/util/IncrementalUpdater;->writeTrailer(Lorg/icepdf/core/pobjects/PTrailer;)V

    .line 120
    invoke-direct {v2}, Lorg/icepdf/core/util/IncrementalUpdater;->getIncrementalUpdateLength()J

    move-result-wide v4

    goto :goto_1
.end method

.method private begin()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 138
    iget-object v0, p0, Lorg/icepdf/core/util/IncrementalUpdater;->output:Lorg/icepdf/core/io/CountingOutputStream;

    sget-object v1, Lorg/icepdf/core/util/IncrementalUpdater;->NEWLINE:[B

    invoke-virtual {v0, v1}, Lorg/icepdf/core/io/CountingOutputStream;->write([B)V

    .line 139
    return-void
.end method

.method private getGreatestObjectNumberWritten()I
    .locals 2

    .prologue
    .line 689
    iget-object v0, p0, Lorg/icepdf/core/util/IncrementalUpdater;->entries:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lorg/icepdf/core/util/IncrementalUpdater;->entries:Ljava/util/ArrayList;

    iget-object v1, p0, Lorg/icepdf/core/util/IncrementalUpdater;->entries:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/icepdf/core/util/IncrementalUpdater$Entry;

    invoke-virtual {v0}, Lorg/icepdf/core/util/IncrementalUpdater$Entry;->getReference()Lorg/icepdf/core/pobjects/Reference;

    move-result-object v0

    invoke-virtual {v0}, Lorg/icepdf/core/pobjects/Reference;->getObjectNumber()I

    move-result v0

    goto :goto_0
.end method

.method private getIncrementalUpdateLength()J
    .locals 2

    .prologue
    .line 289
    iget-object v0, p0, Lorg/icepdf/core/util/IncrementalUpdater;->output:Lorg/icepdf/core/io/CountingOutputStream;

    invoke-virtual {v0}, Lorg/icepdf/core/io/CountingOutputStream;->getCount()J

    move-result-wide v0

    return-wide v0
.end method

.method private writeArray(Ljava/util/Vector;)V
    .locals 4
    .param p1, "array"    # Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 495
    iget-object v2, p0, Lorg/icepdf/core/util/IncrementalUpdater;->output:Lorg/icepdf/core/io/CountingOutputStream;

    sget-object v3, Lorg/icepdf/core/util/IncrementalUpdater;->BEGIN_ARRAY:[B

    invoke-virtual {v2, v3}, Lorg/icepdf/core/io/CountingOutputStream;->write([B)V

    .line 496
    invoke-virtual {p1}, Ljava/util/Vector;->size()I

    move-result v1

    .line 497
    .local v1, "size":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_1

    .line 498
    invoke-virtual {p1, v0}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-direct {p0, v2}, Lorg/icepdf/core/util/IncrementalUpdater;->writeValue(Ljava/lang/Object;)V

    .line 499
    add-int/lit8 v2, v1, -0x1

    if-ge v0, v2, :cond_0

    .line 500
    iget-object v2, p0, Lorg/icepdf/core/util/IncrementalUpdater;->output:Lorg/icepdf/core/io/CountingOutputStream;

    sget-object v3, Lorg/icepdf/core/util/IncrementalUpdater;->SPACE:[B

    invoke-virtual {v2, v3}, Lorg/icepdf/core/io/CountingOutputStream;->write([B)V

    .line 497
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 502
    :cond_1
    iget-object v2, p0, Lorg/icepdf/core/util/IncrementalUpdater;->output:Lorg/icepdf/core/io/CountingOutputStream;

    sget-object v3, Lorg/icepdf/core/util/IncrementalUpdater;->END_ARRAY:[B

    invoke-virtual {v2, v3}, Lorg/icepdf/core/io/CountingOutputStream;->write([B)V

    .line 503
    return-void
.end method

.method private writeBoolean(Z)V
    .locals 2
    .param p1, "b"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 506
    if-eqz p1, :cond_0

    .line 507
    iget-object v0, p0, Lorg/icepdf/core/util/IncrementalUpdater;->output:Lorg/icepdf/core/io/CountingOutputStream;

    sget-object v1, Lorg/icepdf/core/util/IncrementalUpdater;->TRUE:[B

    invoke-virtual {v0, v1}, Lorg/icepdf/core/io/CountingOutputStream;->write([B)V

    .line 510
    :goto_0
    return-void

    .line 509
    :cond_0
    iget-object v0, p0, Lorg/icepdf/core/util/IncrementalUpdater;->output:Lorg/icepdf/core/io/CountingOutputStream;

    sget-object v1, Lorg/icepdf/core/util/IncrementalUpdater;->FALSE:[B

    invoke-virtual {v0, v1}, Lorg/icepdf/core/io/CountingOutputStream;->write([B)V

    goto :goto_0
.end method

.method private writeByteString(Ljava/lang/String;)V
    .locals 4
    .param p1, "str"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 645
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    .line 646
    .local v1, "len":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 647
    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v3

    and-int/lit16 v2, v3, 0xff

    .line 648
    .local v2, "val":I
    iget-object v3, p0, Lorg/icepdf/core/util/IncrementalUpdater;->output:Lorg/icepdf/core/io/CountingOutputStream;

    invoke-virtual {v3, v2}, Lorg/icepdf/core/io/CountingOutputStream;->write(I)V

    .line 646
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 650
    .end local v2    # "val":I
    :cond_0
    return-void
.end method

.method private writeDictionary(Ljava/util/Hashtable;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 374
    .local p1, "dictEntries":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/Object;Ljava/lang/Object;>;"
    sget-object v4, Lorg/icepdf/core/util/IncrementalUpdater;->logger:Ljava/util/logging/Logger;

    sget-object v5, Ljava/util/logging/Level;->FINER:Ljava/util/logging/Level;

    const-string/jumbo v6, "writeDictionary()  dictEntries: {0}"

    invoke-virtual {v4, v5, v6, p1}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Object;)V

    .line 375
    iget-object v4, p0, Lorg/icepdf/core/util/IncrementalUpdater;->output:Lorg/icepdf/core/io/CountingOutputStream;

    sget-object v5, Lorg/icepdf/core/util/IncrementalUpdater;->BEGIN_DICTIONARY:[B

    invoke-virtual {v4, v5}, Lorg/icepdf/core/io/CountingOutputStream;->write([B)V

    .line 378
    invoke-virtual {p1}, Ljava/util/Hashtable;->keys()Ljava/util/Enumeration;

    move-result-object v2

    .line 379
    .local v2, "keys":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/lang/Object;>;"
    :goto_0
    invoke-interface {v2}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 380
    invoke-interface {v2}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v1

    .line 381
    .local v1, "key":Ljava/lang/Object;
    invoke-virtual {p1, v1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    .line 382
    .local v3, "val":Ljava/lang/Object;
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, Lorg/icepdf/core/util/IncrementalUpdater;->writeName(Ljava/lang/String;)V

    .line 383
    iget-object v4, p0, Lorg/icepdf/core/util/IncrementalUpdater;->output:Lorg/icepdf/core/io/CountingOutputStream;

    sget-object v5, Lorg/icepdf/core/util/IncrementalUpdater;->SPACE:[B

    invoke-virtual {v4, v5}, Lorg/icepdf/core/io/CountingOutputStream;->write([B)V

    .line 385
    :try_start_0
    invoke-direct {p0, v3}, Lorg/icepdf/core/util/IncrementalUpdater;->writeValue(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 393
    iget-object v4, p0, Lorg/icepdf/core/util/IncrementalUpdater;->output:Lorg/icepdf/core/io/CountingOutputStream;

    sget-object v5, Lorg/icepdf/core/util/IncrementalUpdater;->SPACE:[B

    invoke-virtual {v4, v5}, Lorg/icepdf/core/io/CountingOutputStream;->write([B)V

    goto :goto_0

    .line 387
    :catch_0
    move-exception v0

    .line 388
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    new-instance v4, Ljava/lang/IllegalArgumentException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, " for key: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v4

    .line 395
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    .end local v1    # "key":Ljava/lang/Object;
    .end local v3    # "val":Ljava/lang/Object;
    :cond_0
    iget-object v4, p0, Lorg/icepdf/core/util/IncrementalUpdater;->output:Lorg/icepdf/core/io/CountingOutputStream;

    sget-object v5, Lorg/icepdf/core/util/IncrementalUpdater;->END_DICTIONARY:[B

    invoke-virtual {v4, v5}, Lorg/icepdf/core/io/CountingOutputStream;->write([B)V

    .line 396
    return-void
.end method

.method private writeDictionary(Lorg/icepdf/core/pobjects/Dictionary;)V
    .locals 5
    .param p1, "dict"    # Lorg/icepdf/core/pobjects/Dictionary;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 356
    sget-object v2, Lorg/icepdf/core/util/IncrementalUpdater;->logger:Ljava/util/logging/Logger;

    sget-object v3, Ljava/util/logging/Level;->FINER:Ljava/util/logging/Level;

    const-string/jumbo v4, "writeDictionary()  dict: {0}"

    invoke-virtual {v2, v3, v4, p1}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Object;)V

    .line 358
    :try_start_0
    invoke-virtual {p1}, Lorg/icepdf/core/pobjects/Dictionary;->getEntries()Ljava/util/Hashtable;

    move-result-object v2

    invoke-direct {p0, v2}, Lorg/icepdf/core/util/IncrementalUpdater;->writeDictionary(Ljava/util/Hashtable;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 366
    return-void

    .line 360
    :catch_0
    move-exception v1

    .line 361
    .local v1, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {p1}, Lorg/icepdf/core/pobjects/Dictionary;->getPObjectReference()Lorg/icepdf/core/pobjects/Reference;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {p1}, Lorg/icepdf/core/pobjects/Dictionary;->getPObjectReference()Lorg/icepdf/core/pobjects/Reference;

    move-result-object v2

    invoke-virtual {v2}, Lorg/icepdf/core/pobjects/Reference;->toString()Ljava/lang/String;

    move-result-object v0

    .line 363
    .local v0, "dictString":Ljava/lang/String;
    :goto_0
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " in dictionary: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2

    .line 361
    .end local v0    # "dictString":Ljava/lang/String;
    :cond_0
    invoke-virtual {p1}, Lorg/icepdf/core/pobjects/Dictionary;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private writeHexString(Lorg/icepdf/core/pobjects/HexStringObject;)V
    .locals 2
    .param p1, "hso"    # Lorg/icepdf/core/pobjects/HexStringObject;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 556
    iget-object v0, p0, Lorg/icepdf/core/util/IncrementalUpdater;->output:Lorg/icepdf/core/io/CountingOutputStream;

    sget-object v1, Lorg/icepdf/core/util/IncrementalUpdater;->BEGIN_HEX_STRING:[B

    invoke-virtual {v0, v1}, Lorg/icepdf/core/io/CountingOutputStream;->write([B)V

    .line 557
    invoke-virtual {p1}, Lorg/icepdf/core/pobjects/HexStringObject;->getHexString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/icepdf/core/util/IncrementalUpdater;->writeByteString(Ljava/lang/String;)V

    .line 558
    iget-object v0, p0, Lorg/icepdf/core/util/IncrementalUpdater;->output:Lorg/icepdf/core/io/CountingOutputStream;

    sget-object v1, Lorg/icepdf/core/util/IncrementalUpdater;->END_HEX_STRING:[B

    invoke-virtual {v0, v1}, Lorg/icepdf/core/io/CountingOutputStream;->write([B)V

    .line 559
    return-void
.end method

.method private writeInteger(I)V
    .locals 1
    .param p1, "i"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 513
    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    .line 514
    .local v0, "str":Ljava/lang/String;
    invoke-direct {p0, v0}, Lorg/icepdf/core/util/IncrementalUpdater;->writeByteString(Ljava/lang/String;)V

    .line 515
    return-void
.end method

.method private writeLiteralString(Lorg/icepdf/core/pobjects/LiteralStringObject;)V
    .locals 2
    .param p1, "lso"    # Lorg/icepdf/core/pobjects/LiteralStringObject;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 543
    iget-object v0, p0, Lorg/icepdf/core/util/IncrementalUpdater;->output:Lorg/icepdf/core/io/CountingOutputStream;

    sget-object v1, Lorg/icepdf/core/util/IncrementalUpdater;->BEGIN_LITERAL_STRING:[B

    invoke-virtual {v0, v1}, Lorg/icepdf/core/io/CountingOutputStream;->write([B)V

    .line 544
    invoke-virtual {p1}, Lorg/icepdf/core/pobjects/LiteralStringObject;->getLiteralString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/icepdf/core/util/IncrementalUpdater;->writeByteString(Ljava/lang/String;)V

    .line 545
    iget-object v0, p0, Lorg/icepdf/core/util/IncrementalUpdater;->output:Lorg/icepdf/core/io/CountingOutputStream;

    sget-object v1, Lorg/icepdf/core/util/IncrementalUpdater;->END_LITERAL_STRING:[B

    invoke-virtual {v0, v1}, Lorg/icepdf/core/io/CountingOutputStream;->write([B)V

    .line 546
    return-void
.end method

.method private writeLong(J)V
    .locals 1
    .param p1, "i"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 518
    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    .line 519
    .local v0, "str":Ljava/lang/String;
    invoke-direct {p0, v0}, Lorg/icepdf/core/util/IncrementalUpdater;->writeByteString(Ljava/lang/String;)V

    .line 520
    return-void
.end method

.method private writeName(Ljava/lang/String;)V
    .locals 14
    .param p1, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v9, 0x41

    const/16 v10, 0x30

    const/16 v13, 0x23

    const/16 v12, 0xa

    .line 458
    iget-object v8, p0, Lorg/icepdf/core/util/IncrementalUpdater;->output:Lorg/icepdf/core/io/CountingOutputStream;

    sget-object v11, Lorg/icepdf/core/util/IncrementalUpdater;->NAME:[B

    invoke-virtual {v8, v11}, Lorg/icepdf/core/io/CountingOutputStream;->write([B)V

    .line 467
    const/16 v7, 0x23

    .line 468
    .local v7, "pound":I
    const-string/jumbo v8, "UTF-8"

    invoke-virtual {p1, v8}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v2

    .line 469
    .local v2, "bytes":[B
    move-object v0, v2

    .local v0, "arr$":[B
    array-length v6, v0

    .local v6, "len$":I
    const/4 v5, 0x0

    .local v5, "i$":I
    :goto_0
    if-ge v5, v6, :cond_4

    aget-byte v1, v0, v5

    .line 470
    .local v1, "b":I
    and-int/lit16 v1, v1, 0xff

    .line 471
    if-eq v1, v13, :cond_0

    const/16 v8, 0x21

    if-lt v1, v8, :cond_0

    const/16 v8, 0x7e

    if-le v1, v8, :cond_3

    .line 472
    :cond_0
    iget-object v8, p0, Lorg/icepdf/core/util/IncrementalUpdater;->output:Lorg/icepdf/core/io/CountingOutputStream;

    invoke-virtual {v8, v13}, Lorg/icepdf/core/io/CountingOutputStream;->write(I)V

    .line 473
    shr-int/lit8 v8, v1, 0x4

    and-int/lit8 v4, v8, 0xf

    .line 474
    .local v4, "hexVal":I
    if-lt v4, v12, :cond_1

    move v8, v9

    :goto_1
    add-int v3, v4, v8

    .line 475
    .local v3, "hexDigit":I
    iget-object v8, p0, Lorg/icepdf/core/util/IncrementalUpdater;->output:Lorg/icepdf/core/io/CountingOutputStream;

    invoke-virtual {v8, v3}, Lorg/icepdf/core/io/CountingOutputStream;->write(I)V

    .line 476
    and-int/lit8 v4, v1, 0xf

    .line 477
    if-lt v4, v12, :cond_2

    move v8, v9

    :goto_2
    add-int v3, v4, v8

    .line 478
    iget-object v8, p0, Lorg/icepdf/core/util/IncrementalUpdater;->output:Lorg/icepdf/core/io/CountingOutputStream;

    invoke-virtual {v8, v3}, Lorg/icepdf/core/io/CountingOutputStream;->write(I)V

    .line 469
    .end local v3    # "hexDigit":I
    .end local v4    # "hexVal":I
    :goto_3
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .restart local v4    # "hexVal":I
    :cond_1
    move v8, v10

    .line 474
    goto :goto_1

    .restart local v3    # "hexDigit":I
    :cond_2
    move v8, v10

    .line 477
    goto :goto_2

    .line 481
    .end local v3    # "hexDigit":I
    .end local v4    # "hexVal":I
    :cond_3
    iget-object v8, p0, Lorg/icepdf/core/util/IncrementalUpdater;->output:Lorg/icepdf/core/io/CountingOutputStream;

    invoke-virtual {v8, v1}, Lorg/icepdf/core/io/CountingOutputStream;->write(I)V

    goto :goto_3

    .line 484
    .end local v1    # "b":I
    :cond_4
    return-void
.end method

.method private writeName(Lorg/icepdf/core/pobjects/Name;)V
    .locals 1
    .param p1, "name"    # Lorg/icepdf/core/pobjects/Name;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 454
    invoke-virtual {p1}, Lorg/icepdf/core/pobjects/Name;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/icepdf/core/util/IncrementalUpdater;->writeName(Ljava/lang/String;)V

    .line 455
    return-void
.end method

.method private writeObject(Lorg/icepdf/core/pobjects/Reference;Ljava/lang/Object;)V
    .locals 1
    .param p1, "ref"    # Lorg/icepdf/core/pobjects/Reference;
    .param p2, "obj"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 147
    instance-of v0, p2, Lorg/icepdf/core/pobjects/Dictionary;

    if-eqz v0, :cond_0

    .line 148
    check-cast p2, Lorg/icepdf/core/pobjects/Dictionary;

    .end local p2    # "obj":Ljava/lang/Object;
    invoke-direct {p0, p2}, Lorg/icepdf/core/util/IncrementalUpdater;->writeObjectDictionary(Lorg/icepdf/core/pobjects/Dictionary;)V

    .line 153
    :goto_0
    return-void

    .line 151
    .restart local p2    # "obj":Ljava/lang/Object;
    :cond_0
    invoke-direct {p0, p1, p2}, Lorg/icepdf/core/util/IncrementalUpdater;->writeObjectValue(Lorg/icepdf/core/pobjects/Reference;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private writeObjectDictionary(Lorg/icepdf/core/pobjects/Dictionary;)V
    .locals 6
    .param p1, "obj"    # Lorg/icepdf/core/pobjects/Dictionary;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 297
    sget-object v1, Lorg/icepdf/core/util/IncrementalUpdater;->logger:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->FINER:Ljava/util/logging/Level;

    const-string/jumbo v3, "writeObjectDictionary()  obj: {0}"

    invoke-virtual {v1, v2, v3, p1}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Object;)V

    .line 298
    if-nez p1, :cond_0

    .line 299
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v2, "Object must be non-null"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 300
    :cond_0
    invoke-virtual {p1}, Lorg/icepdf/core/pobjects/Dictionary;->getPObjectReference()Lorg/icepdf/core/pobjects/Reference;

    move-result-object v0

    .line 301
    .local v0, "ref":Lorg/icepdf/core/pobjects/Reference;
    sget-object v1, Lorg/icepdf/core/util/IncrementalUpdater;->logger:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->FINER:Ljava/util/logging/Level;

    const-string/jumbo v3, "writeObjectDictionary()  ref: {0}"

    invoke-virtual {v1, v2, v3, v0}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Object;)V

    .line 302
    if-nez v0, :cond_1

    .line 303
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Reference must be non-null for object: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 305
    :cond_1
    invoke-virtual {p1}, Lorg/icepdf/core/pobjects/Dictionary;->isDeleted()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 311
    new-instance v1, Lorg/icepdf/core/util/IncrementalUpdater$Entry;

    invoke-direct {v1, v0}, Lorg/icepdf/core/util/IncrementalUpdater$Entry;-><init>(Lorg/icepdf/core/pobjects/Reference;)V

    invoke-direct {p0, v1}, Lorg/icepdf/core/util/IncrementalUpdater;->addEntry(Lorg/icepdf/core/util/IncrementalUpdater$Entry;)V

    .line 323
    :goto_0
    return-void

    .line 314
    :cond_2
    new-instance v1, Lorg/icepdf/core/util/IncrementalUpdater$Entry;

    iget-wide v2, p0, Lorg/icepdf/core/util/IncrementalUpdater;->startingPosition:J

    iget-object v4, p0, Lorg/icepdf/core/util/IncrementalUpdater;->output:Lorg/icepdf/core/io/CountingOutputStream;

    invoke-virtual {v4}, Lorg/icepdf/core/io/CountingOutputStream;->getCount()J

    move-result-wide v4

    add-long/2addr v2, v4

    invoke-direct {v1, v0, v2, v3}, Lorg/icepdf/core/util/IncrementalUpdater$Entry;-><init>(Lorg/icepdf/core/pobjects/Reference;J)V

    invoke-direct {p0, v1}, Lorg/icepdf/core/util/IncrementalUpdater;->addEntry(Lorg/icepdf/core/util/IncrementalUpdater$Entry;)V

    .line 316
    invoke-virtual {v0}, Lorg/icepdf/core/pobjects/Reference;->getObjectNumber()I

    move-result v1

    invoke-direct {p0, v1}, Lorg/icepdf/core/util/IncrementalUpdater;->writeInteger(I)V

    .line 317
    iget-object v1, p0, Lorg/icepdf/core/util/IncrementalUpdater;->output:Lorg/icepdf/core/io/CountingOutputStream;

    sget-object v2, Lorg/icepdf/core/util/IncrementalUpdater;->SPACE:[B

    invoke-virtual {v1, v2}, Lorg/icepdf/core/io/CountingOutputStream;->write([B)V

    .line 318
    invoke-virtual {v0}, Lorg/icepdf/core/pobjects/Reference;->getGenerationNumber()I

    move-result v1

    invoke-direct {p0, v1}, Lorg/icepdf/core/util/IncrementalUpdater;->writeInteger(I)V

    .line 319
    iget-object v1, p0, Lorg/icepdf/core/util/IncrementalUpdater;->output:Lorg/icepdf/core/io/CountingOutputStream;

    sget-object v2, Lorg/icepdf/core/util/IncrementalUpdater;->SPACE:[B

    invoke-virtual {v1, v2}, Lorg/icepdf/core/io/CountingOutputStream;->write([B)V

    .line 320
    iget-object v1, p0, Lorg/icepdf/core/util/IncrementalUpdater;->output:Lorg/icepdf/core/io/CountingOutputStream;

    sget-object v2, Lorg/icepdf/core/util/IncrementalUpdater;->BEGIN_OBJECT:[B

    invoke-virtual {v1, v2}, Lorg/icepdf/core/io/CountingOutputStream;->write([B)V

    .line 321
    invoke-direct {p0, p1}, Lorg/icepdf/core/util/IncrementalUpdater;->writeDictionary(Lorg/icepdf/core/pobjects/Dictionary;)V

    .line 322
    iget-object v1, p0, Lorg/icepdf/core/util/IncrementalUpdater;->output:Lorg/icepdf/core/io/CountingOutputStream;

    sget-object v2, Lorg/icepdf/core/util/IncrementalUpdater;->END_OBJECT:[B

    invoke-virtual {v1, v2}, Lorg/icepdf/core/io/CountingOutputStream;->write([B)V

    goto :goto_0
.end method

.method private writeObjectValue(Lorg/icepdf/core/pobjects/Reference;Ljava/lang/Object;)V
    .locals 6
    .param p1, "ref"    # Lorg/icepdf/core/pobjects/Reference;
    .param p2, "obj"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 332
    sget-object v0, Lorg/icepdf/core/util/IncrementalUpdater;->logger:Ljava/util/logging/Logger;

    sget-object v1, Ljava/util/logging/Level;->FINER:Ljava/util/logging/Level;

    const-string/jumbo v2, "writeObjectValue()  obj: {0}"

    invoke-virtual {v0, v1, v2, p2}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Object;)V

    .line 333
    if-nez p1, :cond_0

    .line 334
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Reference must be non-null for object: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 335
    :cond_0
    if-nez p2, :cond_1

    .line 336
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "Object must be non-null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 337
    :cond_1
    new-instance v0, Lorg/icepdf/core/util/IncrementalUpdater$Entry;

    iget-wide v2, p0, Lorg/icepdf/core/util/IncrementalUpdater;->startingPosition:J

    iget-object v1, p0, Lorg/icepdf/core/util/IncrementalUpdater;->output:Lorg/icepdf/core/io/CountingOutputStream;

    invoke-virtual {v1}, Lorg/icepdf/core/io/CountingOutputStream;->getCount()J

    move-result-wide v4

    add-long/2addr v2, v4

    invoke-direct {v0, p1, v2, v3}, Lorg/icepdf/core/util/IncrementalUpdater$Entry;-><init>(Lorg/icepdf/core/pobjects/Reference;J)V

    invoke-direct {p0, v0}, Lorg/icepdf/core/util/IncrementalUpdater;->addEntry(Lorg/icepdf/core/util/IncrementalUpdater$Entry;)V

    .line 339
    invoke-virtual {p1}, Lorg/icepdf/core/pobjects/Reference;->getObjectNumber()I

    move-result v0

    invoke-direct {p0, v0}, Lorg/icepdf/core/util/IncrementalUpdater;->writeInteger(I)V

    .line 340
    iget-object v0, p0, Lorg/icepdf/core/util/IncrementalUpdater;->output:Lorg/icepdf/core/io/CountingOutputStream;

    sget-object v1, Lorg/icepdf/core/util/IncrementalUpdater;->SPACE:[B

    invoke-virtual {v0, v1}, Lorg/icepdf/core/io/CountingOutputStream;->write([B)V

    .line 341
    invoke-virtual {p1}, Lorg/icepdf/core/pobjects/Reference;->getGenerationNumber()I

    move-result v0

    invoke-direct {p0, v0}, Lorg/icepdf/core/util/IncrementalUpdater;->writeInteger(I)V

    .line 342
    iget-object v0, p0, Lorg/icepdf/core/util/IncrementalUpdater;->output:Lorg/icepdf/core/io/CountingOutputStream;

    sget-object v1, Lorg/icepdf/core/util/IncrementalUpdater;->SPACE:[B

    invoke-virtual {v0, v1}, Lorg/icepdf/core/io/CountingOutputStream;->write([B)V

    .line 343
    iget-object v0, p0, Lorg/icepdf/core/util/IncrementalUpdater;->output:Lorg/icepdf/core/io/CountingOutputStream;

    sget-object v1, Lorg/icepdf/core/util/IncrementalUpdater;->BEGIN_OBJECT:[B

    invoke-virtual {v0, v1}, Lorg/icepdf/core/io/CountingOutputStream;->write([B)V

    .line 344
    invoke-direct {p0, p2}, Lorg/icepdf/core/util/IncrementalUpdater;->writeValue(Ljava/lang/Object;)V

    .line 345
    iget-object v0, p0, Lorg/icepdf/core/util/IncrementalUpdater;->output:Lorg/icepdf/core/io/CountingOutputStream;

    sget-object v1, Lorg/icepdf/core/util/IncrementalUpdater;->END_OBJECT:[B

    invoke-virtual {v0, v1}, Lorg/icepdf/core/io/CountingOutputStream;->write([B)V

    .line 346
    return-void
.end method

.method private writeReal(Ljava/lang/Number;)V
    .locals 1
    .param p1, "r"    # Ljava/lang/Number;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 531
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 532
    .local v0, "str":Ljava/lang/String;
    invoke-direct {p0, v0}, Lorg/icepdf/core/util/IncrementalUpdater;->writeByteString(Ljava/lang/String;)V

    .line 533
    return-void
.end method

.method private writeReference(Lorg/icepdf/core/pobjects/Reference;)V
    .locals 2
    .param p1, "ref"    # Lorg/icepdf/core/pobjects/Reference;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 487
    invoke-virtual {p1}, Lorg/icepdf/core/pobjects/Reference;->getObjectNumber()I

    move-result v0

    invoke-direct {p0, v0}, Lorg/icepdf/core/util/IncrementalUpdater;->writeInteger(I)V

    .line 488
    iget-object v0, p0, Lorg/icepdf/core/util/IncrementalUpdater;->output:Lorg/icepdf/core/io/CountingOutputStream;

    sget-object v1, Lorg/icepdf/core/util/IncrementalUpdater;->SPACE:[B

    invoke-virtual {v0, v1}, Lorg/icepdf/core/io/CountingOutputStream;->write([B)V

    .line 489
    invoke-virtual {p1}, Lorg/icepdf/core/pobjects/Reference;->getGenerationNumber()I

    move-result v0

    invoke-direct {p0, v0}, Lorg/icepdf/core/util/IncrementalUpdater;->writeInteger(I)V

    .line 490
    iget-object v0, p0, Lorg/icepdf/core/util/IncrementalUpdater;->output:Lorg/icepdf/core/io/CountingOutputStream;

    sget-object v1, Lorg/icepdf/core/util/IncrementalUpdater;->SPACE:[B

    invoke-virtual {v0, v1}, Lorg/icepdf/core/io/CountingOutputStream;->write([B)V

    .line 491
    iget-object v0, p0, Lorg/icepdf/core/util/IncrementalUpdater;->output:Lorg/icepdf/core/io/CountingOutputStream;

    sget-object v1, Lorg/icepdf/core/util/IncrementalUpdater;->REFERENCE:[B

    invoke-virtual {v0, v1}, Lorg/icepdf/core/io/CountingOutputStream;->write([B)V

    .line 492
    return-void
.end method

.method private writeTrailer(Lorg/icepdf/core/pobjects/PTrailer;)V
    .locals 10
    .param p1, "prevTrailer"    # Lorg/icepdf/core/pobjects/PTrailer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 248
    invoke-virtual {p1}, Lorg/icepdf/core/pobjects/PTrailer;->getDictionary()Ljava/util/Hashtable;

    move-result-object v8

    invoke-virtual {v8}, Ljava/util/Hashtable;->clone()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Hashtable;

    .line 250
    .local v2, "newTrailer":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/Object;Ljava/lang/Object;>;"
    invoke-virtual {p1}, Lorg/icepdf/core/pobjects/PTrailer;->getNumberOfObjects()I

    move-result v3

    .line 251
    .local v3, "oldSize":I
    invoke-direct {p0}, Lorg/icepdf/core/util/IncrementalUpdater;->getGreatestObjectNumberWritten()I

    move-result v0

    .line 252
    .local v0, "greatestWritten":I
    add-int/lit8 v8, v0, 0x1

    invoke-static {v3, v8}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 253
    .local v1, "newSize":I
    new-instance v8, Lorg/icepdf/core/pobjects/Name;

    const-string/jumbo v9, "Size"

    invoke-direct {v8, v9}, Lorg/icepdf/core/pobjects/Name;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v2, v8, v9}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 254
    invoke-virtual {p1}, Lorg/icepdf/core/pobjects/PTrailer;->getPosition()J

    move-result-wide v4

    .line 255
    .local v4, "prevTrailerPos":J
    new-instance v8, Lorg/icepdf/core/pobjects/Name;

    const-string/jumbo v9, "Prev"

    invoke-direct {v8, v9}, Lorg/icepdf/core/pobjects/Name;-><init>(Ljava/lang/String;)V

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v2, v8, v9}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 256
    iget-wide v6, p0, Lorg/icepdf/core/util/IncrementalUpdater;->xrefPosition:J

    .line 265
    .local v6, "xrefPos":J
    const-wide/16 v8, 0x0

    cmp-long v8, v4, v8

    if-nez v8, :cond_0

    .line 266
    const-wide/16 v6, -0x1

    .line 269
    :cond_0
    iget-object v8, p0, Lorg/icepdf/core/util/IncrementalUpdater;->output:Lorg/icepdf/core/io/CountingOutputStream;

    sget-object v9, Lorg/icepdf/core/util/IncrementalUpdater;->TRAILER:[B

    invoke-virtual {v8, v9}, Lorg/icepdf/core/io/CountingOutputStream;->write([B)V

    .line 270
    invoke-direct {p0, v2}, Lorg/icepdf/core/util/IncrementalUpdater;->writeDictionary(Ljava/util/Hashtable;)V

    .line 271
    iget-object v8, p0, Lorg/icepdf/core/util/IncrementalUpdater;->output:Lorg/icepdf/core/io/CountingOutputStream;

    sget-object v9, Lorg/icepdf/core/util/IncrementalUpdater;->STARTXREF:[B

    invoke-virtual {v8, v9}, Lorg/icepdf/core/io/CountingOutputStream;->write([B)V

    .line 272
    invoke-direct {p0, v6, v7}, Lorg/icepdf/core/util/IncrementalUpdater;->writeLong(J)V

    .line 273
    iget-object v8, p0, Lorg/icepdf/core/util/IncrementalUpdater;->output:Lorg/icepdf/core/io/CountingOutputStream;

    sget-object v9, Lorg/icepdf/core/util/IncrementalUpdater;->NEWLINE:[B

    invoke-virtual {v8, v9}, Lorg/icepdf/core/io/CountingOutputStream;->write([B)V

    .line 274
    iget-object v8, p0, Lorg/icepdf/core/util/IncrementalUpdater;->output:Lorg/icepdf/core/io/CountingOutputStream;

    sget-object v9, Lorg/icepdf/core/util/IncrementalUpdater;->COMMENT_EOF:[B

    invoke-virtual {v8, v9}, Lorg/icepdf/core/io/CountingOutputStream;->write([B)V

    .line 275
    return-void
.end method

.method private writeValue(Ljava/lang/Object;)V
    .locals 3
    .param p1, "val"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 404
    if-nez p1, :cond_0

    .line 405
    const-string/jumbo v0, "null"

    invoke-direct {p0, v0}, Lorg/icepdf/core/util/IncrementalUpdater;->writeByteString(Ljava/lang/String;)V

    .line 451
    .end local p1    # "val":Ljava/lang/Object;
    :goto_0
    return-void

    .line 407
    .restart local p1    # "val":Ljava/lang/Object;
    :cond_0
    instance-of v0, p1, Lorg/icepdf/core/pobjects/Name;

    if-eqz v0, :cond_1

    .line 408
    check-cast p1, Lorg/icepdf/core/pobjects/Name;

    .end local p1    # "val":Ljava/lang/Object;
    invoke-direct {p0, p1}, Lorg/icepdf/core/util/IncrementalUpdater;->writeName(Lorg/icepdf/core/pobjects/Name;)V

    goto :goto_0

    .line 410
    .restart local p1    # "val":Ljava/lang/Object;
    :cond_1
    instance-of v0, p1, Lorg/icepdf/core/pobjects/Reference;

    if-eqz v0, :cond_2

    .line 411
    check-cast p1, Lorg/icepdf/core/pobjects/Reference;

    .end local p1    # "val":Ljava/lang/Object;
    invoke-direct {p0, p1}, Lorg/icepdf/core/util/IncrementalUpdater;->writeReference(Lorg/icepdf/core/pobjects/Reference;)V

    goto :goto_0

    .line 413
    .restart local p1    # "val":Ljava/lang/Object;
    :cond_2
    instance-of v0, p1, Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    .line 414
    check-cast p1, Ljava/lang/Boolean;

    .end local p1    # "val":Ljava/lang/Object;
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lorg/icepdf/core/util/IncrementalUpdater;->writeBoolean(Z)V

    goto :goto_0

    .line 416
    .restart local p1    # "val":Ljava/lang/Object;
    :cond_3
    instance-of v0, p1, Ljava/lang/Integer;

    if-eqz v0, :cond_4

    .line 417
    check-cast p1, Ljava/lang/Integer;

    .end local p1    # "val":Ljava/lang/Object;
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {p0, v0}, Lorg/icepdf/core/util/IncrementalUpdater;->writeInteger(I)V

    goto :goto_0

    .line 419
    .restart local p1    # "val":Ljava/lang/Object;
    :cond_4
    instance-of v0, p1, Ljava/lang/Long;

    if-eqz v0, :cond_5

    .line 420
    check-cast p1, Ljava/lang/Long;

    .end local p1    # "val":Ljava/lang/Object;
    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lorg/icepdf/core/util/IncrementalUpdater;->writeLong(J)V

    goto :goto_0

    .line 422
    .restart local p1    # "val":Ljava/lang/Object;
    :cond_5
    instance-of v0, p1, Ljava/lang/Number;

    if-eqz v0, :cond_6

    .line 423
    check-cast p1, Ljava/lang/Number;

    .end local p1    # "val":Ljava/lang/Object;
    invoke-direct {p0, p1}, Lorg/icepdf/core/util/IncrementalUpdater;->writeReal(Ljava/lang/Number;)V

    goto :goto_0

    .line 425
    .restart local p1    # "val":Ljava/lang/Object;
    :cond_6
    instance-of v0, p1, Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 426
    sget-object v0, Lorg/icepdf/core/util/IncrementalUpdater;->logger:Ljava/util/logging/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Found invalid java.lang.String being written out: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/logging/Logger;->severe(Ljava/lang/String;)V

    .line 428
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "invalid type of java.lang.String. Should use LiteralStringObject or HexStringObject"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 432
    :cond_7
    instance-of v0, p1, Lorg/icepdf/core/pobjects/LiteralStringObject;

    if-eqz v0, :cond_8

    .line 433
    check-cast p1, Lorg/icepdf/core/pobjects/LiteralStringObject;

    .end local p1    # "val":Ljava/lang/Object;
    invoke-direct {p0, p1}, Lorg/icepdf/core/util/IncrementalUpdater;->writeLiteralString(Lorg/icepdf/core/pobjects/LiteralStringObject;)V

    goto :goto_0

    .line 435
    .restart local p1    # "val":Ljava/lang/Object;
    :cond_8
    instance-of v0, p1, Lorg/icepdf/core/pobjects/HexStringObject;

    if-eqz v0, :cond_9

    .line 436
    check-cast p1, Lorg/icepdf/core/pobjects/HexStringObject;

    .end local p1    # "val":Ljava/lang/Object;
    invoke-direct {p0, p1}, Lorg/icepdf/core/util/IncrementalUpdater;->writeHexString(Lorg/icepdf/core/pobjects/HexStringObject;)V

    goto/16 :goto_0

    .line 438
    .restart local p1    # "val":Ljava/lang/Object;
    :cond_9
    instance-of v0, p1, Ljava/util/Vector;

    if-eqz v0, :cond_a

    .line 439
    check-cast p1, Ljava/util/Vector;

    .end local p1    # "val":Ljava/lang/Object;
    invoke-direct {p0, p1}, Lorg/icepdf/core/util/IncrementalUpdater;->writeArray(Ljava/util/Vector;)V

    goto/16 :goto_0

    .line 441
    .restart local p1    # "val":Ljava/lang/Object;
    :cond_a
    instance-of v0, p1, Lorg/icepdf/core/pobjects/Dictionary;

    if-eqz v0, :cond_b

    .line 442
    check-cast p1, Lorg/icepdf/core/pobjects/Dictionary;

    .end local p1    # "val":Ljava/lang/Object;
    invoke-direct {p0, p1}, Lorg/icepdf/core/util/IncrementalUpdater;->writeDictionary(Lorg/icepdf/core/pobjects/Dictionary;)V

    goto/16 :goto_0

    .line 444
    .restart local p1    # "val":Ljava/lang/Object;
    :cond_b
    instance-of v0, p1, Ljava/util/Hashtable;

    if-eqz v0, :cond_c

    .line 445
    check-cast p1, Ljava/util/Hashtable;

    .end local p1    # "val":Ljava/lang/Object;
    invoke-direct {p0, p1}, Lorg/icepdf/core/util/IncrementalUpdater;->writeDictionary(Ljava/util/Hashtable;)V

    goto/16 :goto_0

    .line 448
    .restart local p1    # "val":Ljava/lang/Object;
    :cond_c
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "unknown value type of: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private writeXRefTable()V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 165
    const/4 v2, 0x0

    .line 166
    .local v2, "nextDeletedObjectNumber":I
    iget-object v4, p0, Lorg/icepdf/core/util/IncrementalUpdater;->entries:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    add-int/lit8 v1, v4, -0x1

    .local v1, "i":I
    :goto_0
    if-ltz v1, :cond_1

    .line 167
    iget-object v4, p0, Lorg/icepdf/core/util/IncrementalUpdater;->entries:Ljava/util/ArrayList;

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/icepdf/core/util/IncrementalUpdater$Entry;

    .line 168
    .local v0, "entry":Lorg/icepdf/core/util/IncrementalUpdater$Entry;
    invoke-virtual {v0}, Lorg/icepdf/core/util/IncrementalUpdater$Entry;->isDeleted()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 169
    invoke-virtual {v0, v2}, Lorg/icepdf/core/util/IncrementalUpdater$Entry;->setNextDeletedObjectNumber(I)V

    .line 170
    invoke-virtual {v0}, Lorg/icepdf/core/util/IncrementalUpdater$Entry;->getReference()Lorg/icepdf/core/pobjects/Reference;

    move-result-object v4

    invoke-virtual {v4}, Lorg/icepdf/core/pobjects/Reference;->getObjectNumber()I

    move-result v2

    .line 166
    :cond_0
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    .line 177
    .end local v0    # "entry":Lorg/icepdf/core/util/IncrementalUpdater$Entry;
    :cond_1
    new-instance v3, Lorg/icepdf/core/util/IncrementalUpdater$Entry;

    new-instance v4, Lorg/icepdf/core/pobjects/Reference;

    const v5, 0xfffe

    invoke-direct {v4, v6, v5}, Lorg/icepdf/core/pobjects/Reference;-><init>(II)V

    invoke-direct {v3, v4}, Lorg/icepdf/core/util/IncrementalUpdater$Entry;-><init>(Lorg/icepdf/core/pobjects/Reference;)V

    .line 178
    .local v3, "zero":Lorg/icepdf/core/util/IncrementalUpdater$Entry;
    invoke-virtual {v3, v2}, Lorg/icepdf/core/util/IncrementalUpdater$Entry;->setNextDeletedObjectNumber(I)V

    .line 179
    iget-object v4, p0, Lorg/icepdf/core/util/IncrementalUpdater;->entries:Ljava/util/ArrayList;

    invoke-virtual {v4, v6, v3}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 181
    iget-object v4, p0, Lorg/icepdf/core/util/IncrementalUpdater;->output:Lorg/icepdf/core/io/CountingOutputStream;

    sget-object v5, Lorg/icepdf/core/util/IncrementalUpdater;->NEWLINE:[B

    invoke-virtual {v4, v5}, Lorg/icepdf/core/io/CountingOutputStream;->write([B)V

    .line 182
    iget-wide v4, p0, Lorg/icepdf/core/util/IncrementalUpdater;->startingPosition:J

    iget-object v6, p0, Lorg/icepdf/core/util/IncrementalUpdater;->output:Lorg/icepdf/core/io/CountingOutputStream;

    invoke-virtual {v6}, Lorg/icepdf/core/io/CountingOutputStream;->getCount()J

    move-result-wide v6

    add-long/2addr v4, v6

    iput-wide v4, p0, Lorg/icepdf/core/util/IncrementalUpdater;->xrefPosition:J

    .line 183
    iget-object v4, p0, Lorg/icepdf/core/util/IncrementalUpdater;->output:Lorg/icepdf/core/io/CountingOutputStream;

    sget-object v5, Lorg/icepdf/core/util/IncrementalUpdater;->XREF:[B

    invoke-virtual {v4, v5}, Lorg/icepdf/core/io/CountingOutputStream;->write([B)V

    .line 184
    const/4 v1, 0x0

    :goto_1
    iget-object v4, p0, Lorg/icepdf/core/util/IncrementalUpdater;->entries:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v1, v4, :cond_2

    .line 185
    invoke-direct {p0, v1}, Lorg/icepdf/core/util/IncrementalUpdater;->writeXrefSubSection(I)I

    move-result v4

    add-int/2addr v1, v4

    goto :goto_1

    .line 187
    :cond_2
    iget-object v4, p0, Lorg/icepdf/core/util/IncrementalUpdater;->output:Lorg/icepdf/core/io/CountingOutputStream;

    sget-object v5, Lorg/icepdf/core/util/IncrementalUpdater;->NEWLINE:[B

    invoke-virtual {v4, v5}, Lorg/icepdf/core/io/CountingOutputStream;->write([B)V

    .line 188
    return-void
.end method

.method private writeXrefSubSection(I)I
    .locals 12
    .param p1, "beginIndex"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v11, 0xd

    const/4 v10, 0x5

    const/16 v9, 0x20

    const/16 v8, 0xa

    .line 197
    iget-object v5, p0, Lorg/icepdf/core/util/IncrementalUpdater;->entries:Ljava/util/ArrayList;

    invoke-virtual {v5, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/icepdf/core/util/IncrementalUpdater$Entry;

    invoke-virtual {v5}, Lorg/icepdf/core/util/IncrementalUpdater$Entry;->getReference()Lorg/icepdf/core/pobjects/Reference;

    move-result-object v5

    invoke-virtual {v5}, Lorg/icepdf/core/pobjects/Reference;->getObjectNumber()I

    move-result v0

    .line 200
    .local v0, "beginObjNum":I
    add-int/lit8 v3, v0, 0x1

    .line 201
    .local v3, "nextContiguous":I
    add-int/lit8 v2, p1, 0x1

    .local v2, "i":I
    :goto_0
    iget-object v5, p0, Lorg/icepdf/core/util/IncrementalUpdater;->entries:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-ge v2, v5, :cond_0

    .line 202
    iget-object v5, p0, Lorg/icepdf/core/util/IncrementalUpdater;->entries:Ljava/util/ArrayList;

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/icepdf/core/util/IncrementalUpdater$Entry;

    invoke-virtual {v5}, Lorg/icepdf/core/util/IncrementalUpdater$Entry;->getReference()Lorg/icepdf/core/pobjects/Reference;

    move-result-object v5

    invoke-virtual {v5}, Lorg/icepdf/core/pobjects/Reference;->getObjectNumber()I

    move-result v5

    if-ne v5, v3, :cond_0

    .line 203
    add-int/lit8 v3, v3, 0x1

    .line 201
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 207
    :cond_0
    sub-int v4, v3, v0

    .line 210
    .local v4, "subSectionLength":I
    invoke-direct {p0, v0}, Lorg/icepdf/core/util/IncrementalUpdater;->writeInteger(I)V

    .line 211
    iget-object v5, p0, Lorg/icepdf/core/util/IncrementalUpdater;->output:Lorg/icepdf/core/io/CountingOutputStream;

    sget-object v6, Lorg/icepdf/core/util/IncrementalUpdater;->SPACE:[B

    invoke-virtual {v5, v6}, Lorg/icepdf/core/io/CountingOutputStream;->write([B)V

    .line 212
    invoke-direct {p0, v4}, Lorg/icepdf/core/util/IncrementalUpdater;->writeInteger(I)V

    .line 213
    iget-object v5, p0, Lorg/icepdf/core/util/IncrementalUpdater;->output:Lorg/icepdf/core/io/CountingOutputStream;

    sget-object v6, Lorg/icepdf/core/util/IncrementalUpdater;->NEWLINE:[B

    invoke-virtual {v5, v6}, Lorg/icepdf/core/io/CountingOutputStream;->write([B)V

    .line 215
    move v2, p1

    :goto_1
    add-int v5, p1, v4

    if-ge v2, v5, :cond_2

    .line 216
    iget-object v5, p0, Lorg/icepdf/core/util/IncrementalUpdater;->entries:Ljava/util/ArrayList;

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/icepdf/core/util/IncrementalUpdater$Entry;

    .line 217
    .local v1, "entry":Lorg/icepdf/core/util/IncrementalUpdater$Entry;
    invoke-virtual {v1}, Lorg/icepdf/core/util/IncrementalUpdater$Entry;->isDeleted()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 219
    invoke-virtual {v1}, Lorg/icepdf/core/util/IncrementalUpdater$Entry;->getNextDeletedObjectNumber()I

    move-result v5

    int-to-long v6, v5

    invoke-direct {p0, v6, v7, v8}, Lorg/icepdf/core/util/IncrementalUpdater;->writeZeroPaddedLong(JI)V

    .line 220
    iget-object v5, p0, Lorg/icepdf/core/util/IncrementalUpdater;->output:Lorg/icepdf/core/io/CountingOutputStream;

    invoke-virtual {v5, v9}, Lorg/icepdf/core/io/CountingOutputStream;->write(I)V

    .line 221
    invoke-virtual {v1}, Lorg/icepdf/core/util/IncrementalUpdater$Entry;->getReference()Lorg/icepdf/core/pobjects/Reference;

    move-result-object v5

    invoke-virtual {v5}, Lorg/icepdf/core/pobjects/Reference;->getGenerationNumber()I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    int-to-long v6, v5

    invoke-direct {p0, v6, v7, v10}, Lorg/icepdf/core/util/IncrementalUpdater;->writeZeroPaddedLong(JI)V

    .line 222
    iget-object v5, p0, Lorg/icepdf/core/util/IncrementalUpdater;->output:Lorg/icepdf/core/io/CountingOutputStream;

    invoke-virtual {v5, v9}, Lorg/icepdf/core/io/CountingOutputStream;->write(I)V

    .line 223
    iget-object v5, p0, Lorg/icepdf/core/util/IncrementalUpdater;->output:Lorg/icepdf/core/io/CountingOutputStream;

    const/16 v6, 0x66

    invoke-virtual {v5, v6}, Lorg/icepdf/core/io/CountingOutputStream;->write(I)V

    .line 224
    iget-object v5, p0, Lorg/icepdf/core/util/IncrementalUpdater;->output:Lorg/icepdf/core/io/CountingOutputStream;

    invoke-virtual {v5, v11}, Lorg/icepdf/core/io/CountingOutputStream;->write(I)V

    .line 225
    iget-object v5, p0, Lorg/icepdf/core/util/IncrementalUpdater;->output:Lorg/icepdf/core/io/CountingOutputStream;

    invoke-virtual {v5, v8}, Lorg/icepdf/core/io/CountingOutputStream;->write(I)V

    .line 215
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 229
    :cond_1
    invoke-virtual {v1}, Lorg/icepdf/core/util/IncrementalUpdater$Entry;->getPosition()J

    move-result-wide v6

    invoke-direct {p0, v6, v7, v8}, Lorg/icepdf/core/util/IncrementalUpdater;->writeZeroPaddedLong(JI)V

    .line 230
    iget-object v5, p0, Lorg/icepdf/core/util/IncrementalUpdater;->output:Lorg/icepdf/core/io/CountingOutputStream;

    invoke-virtual {v5, v9}, Lorg/icepdf/core/io/CountingOutputStream;->write(I)V

    .line 231
    invoke-virtual {v1}, Lorg/icepdf/core/util/IncrementalUpdater$Entry;->getReference()Lorg/icepdf/core/pobjects/Reference;

    move-result-object v5

    invoke-virtual {v5}, Lorg/icepdf/core/pobjects/Reference;->getGenerationNumber()I

    move-result v5

    int-to-long v6, v5

    invoke-direct {p0, v6, v7, v10}, Lorg/icepdf/core/util/IncrementalUpdater;->writeZeroPaddedLong(JI)V

    .line 232
    iget-object v5, p0, Lorg/icepdf/core/util/IncrementalUpdater;->output:Lorg/icepdf/core/io/CountingOutputStream;

    invoke-virtual {v5, v9}, Lorg/icepdf/core/io/CountingOutputStream;->write(I)V

    .line 233
    iget-object v5, p0, Lorg/icepdf/core/util/IncrementalUpdater;->output:Lorg/icepdf/core/io/CountingOutputStream;

    const/16 v6, 0x6e

    invoke-virtual {v5, v6}, Lorg/icepdf/core/io/CountingOutputStream;->write(I)V

    .line 234
    iget-object v5, p0, Lorg/icepdf/core/util/IncrementalUpdater;->output:Lorg/icepdf/core/io/CountingOutputStream;

    invoke-virtual {v5, v11}, Lorg/icepdf/core/io/CountingOutputStream;->write(I)V

    .line 235
    iget-object v5, p0, Lorg/icepdf/core/util/IncrementalUpdater;->output:Lorg/icepdf/core/io/CountingOutputStream;

    invoke-virtual {v5, v8}, Lorg/icepdf/core/io/CountingOutputStream;->write(I)V

    goto :goto_2

    .line 239
    .end local v1    # "entry":Lorg/icepdf/core/util/IncrementalUpdater$Entry;
    :cond_2
    return v4
.end method

.method private writeZeroPaddedLong(JI)V
    .locals 5
    .param p1, "val"    # J
    .param p3, "len"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 653
    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    .line 654
    .local v2, "str":Ljava/lang/String;
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-le v3, p3, :cond_0

    .line 655
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    sub-int/2addr v3, p3

    invoke-virtual {v2, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    .line 656
    :cond_0
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    sub-int v1, p3, v3

    .line 657
    .local v1, "padding":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_1

    .line 658
    iget-object v3, p0, Lorg/icepdf/core/util/IncrementalUpdater;->output:Lorg/icepdf/core/io/CountingOutputStream;

    const/16 v4, 0x30

    invoke-virtual {v3, v4}, Lorg/icepdf/core/io/CountingOutputStream;->write(I)V

    .line 657
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 659
    :cond_1
    invoke-direct {p0, v2}, Lorg/icepdf/core/util/IncrementalUpdater;->writeByteString(Ljava/lang/String;)V

    .line 660
    return-void
.end method
