.class public Lorg/icepdf/core/util/Parser;
.super Ljava/lang/Object;
.source "Parser.java"


# static fields
.field public static final PARSE_MODE_NORMAL:I = 0x0

.field public static final PARSE_MODE_OBJECT_STREAM:I = 0x1

.field private static final logger:Ljava/util/logging/Logger;


# instance fields
.field lastTokenHString:Z

.field private parseMode:I

.field private reader:Ljava/io/InputStream;

.field private stack:Ljava/util/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Stack",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 44
    const-class v0, Lorg/icepdf/core/util/Parser;

    invoke-virtual {v0}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Lorg/icepdf/core/util/Parser;->logger:Ljava/util/logging/Logger;

    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 1
    .param p1, "r"    # Ljava/io/InputStream;

    .prologue
    .line 67
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lorg/icepdf/core/util/Parser;-><init>(Ljava/io/InputStream;I)V

    .line 68
    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;I)V
    .locals 1
    .param p1, "r"    # Ljava/io/InputStream;
    .param p2, "pm"    # I

    .prologue
    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/icepdf/core/util/Parser;->lastTokenHString:Z

    .line 54
    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    iput-object v0, p0, Lorg/icepdf/core/util/Parser;->stack:Ljava/util/Stack;

    .line 71
    new-instance v0, Ljava/io/BufferedInputStream;

    invoke-direct {v0, p1}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    iput-object v0, p0, Lorg/icepdf/core/util/Parser;->reader:Ljava/io/InputStream;

    .line 72
    iput p2, p0, Lorg/icepdf/core/util/Parser;->parseMode:I

    .line 73
    return-void
.end method

.method public constructor <init>(Lorg/icepdf/core/io/SeekableInput;)V
    .locals 1
    .param p1, "r"    # Lorg/icepdf/core/io/SeekableInput;

    .prologue
    .line 58
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lorg/icepdf/core/util/Parser;-><init>(Lorg/icepdf/core/io/SeekableInput;I)V

    .line 59
    return-void
.end method

.method public constructor <init>(Lorg/icepdf/core/io/SeekableInput;I)V
    .locals 1
    .param p1, "r"    # Lorg/icepdf/core/io/SeekableInput;
    .param p2, "pm"    # I

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/icepdf/core/util/Parser;->lastTokenHString:Z

    .line 54
    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    iput-object v0, p0, Lorg/icepdf/core/util/Parser;->stack:Ljava/util/Stack;

    .line 62
    invoke-interface {p1}, Lorg/icepdf/core/io/SeekableInput;->getInputStream()Ljava/io/InputStream;

    move-result-object v0

    iput-object v0, p0, Lorg/icepdf/core/util/Parser;->reader:Ljava/io/InputStream;

    .line 63
    iput p2, p0, Lorg/icepdf/core/util/Parser;->parseMode:I

    .line 64
    return-void
.end method

.method private captureStreamData(Ljava/io/OutputStream;)J
    .locals 7
    .param p1, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v6, 0x65

    .line 1318
    const-wide/16 v2, 0x0

    .line 1321
    .local v2, "numBytes":J
    :goto_0
    iget-object v1, p0, Lorg/icepdf/core/util/Parser;->reader:Ljava/io/InputStream;

    invoke-virtual {v1}, Ljava/io/InputStream;->read()I

    move-result v0

    .line 1323
    .local v0, "nextByte":I
    if-ne v0, v6, :cond_3

    .line 1324
    iget-object v1, p0, Lorg/icepdf/core/util/Parser;->reader:Ljava/io/InputStream;

    const/16 v4, 0xa

    invoke-virtual {v1, v4}, Ljava/io/InputStream;->mark(I)V

    .line 1325
    iget-object v1, p0, Lorg/icepdf/core/util/Parser;->reader:Ljava/io/InputStream;

    invoke-virtual {v1}, Ljava/io/InputStream;->read()I

    move-result v1

    const/16 v4, 0x6e

    if-ne v1, v4, :cond_0

    iget-object v1, p0, Lorg/icepdf/core/util/Parser;->reader:Ljava/io/InputStream;

    invoke-virtual {v1}, Ljava/io/InputStream;->read()I

    move-result v1

    const/16 v4, 0x64

    if-ne v1, v4, :cond_0

    iget-object v1, p0, Lorg/icepdf/core/util/Parser;->reader:Ljava/io/InputStream;

    invoke-virtual {v1}, Ljava/io/InputStream;->read()I

    move-result v1

    const/16 v4, 0x73

    if-ne v1, v4, :cond_0

    iget-object v1, p0, Lorg/icepdf/core/util/Parser;->reader:Ljava/io/InputStream;

    invoke-virtual {v1}, Ljava/io/InputStream;->read()I

    move-result v1

    const/16 v4, 0x74

    if-ne v1, v4, :cond_0

    iget-object v1, p0, Lorg/icepdf/core/util/Parser;->reader:Ljava/io/InputStream;

    invoke-virtual {v1}, Ljava/io/InputStream;->read()I

    move-result v1

    const/16 v4, 0x72

    if-ne v1, v4, :cond_0

    iget-object v1, p0, Lorg/icepdf/core/util/Parser;->reader:Ljava/io/InputStream;

    invoke-virtual {v1}, Ljava/io/InputStream;->read()I

    move-result v1

    if-ne v1, v6, :cond_0

    iget-object v1, p0, Lorg/icepdf/core/util/Parser;->reader:Ljava/io/InputStream;

    invoke-virtual {v1}, Ljava/io/InputStream;->read()I

    move-result v1

    const/16 v4, 0x61

    if-ne v1, v4, :cond_0

    iget-object v1, p0, Lorg/icepdf/core/util/Parser;->reader:Ljava/io/InputStream;

    invoke-virtual {v1}, Ljava/io/InputStream;->read()I

    move-result v1

    const/16 v4, 0x6d

    if-ne v1, v4, :cond_0

    .line 1344
    :goto_1
    return-wide v2

    .line 1335
    :cond_0
    iget-object v1, p0, Lorg/icepdf/core/util/Parser;->reader:Ljava/io/InputStream;

    invoke-virtual {v1}, Ljava/io/InputStream;->reset()V

    .line 1340
    :cond_1
    if-eqz p1, :cond_2

    .line 1341
    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write(I)V

    .line 1342
    :cond_2
    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    .line 1343
    goto :goto_0

    .line 1337
    :cond_3
    if-gez v0, :cond_1

    goto :goto_1
.end method

.method private static final isDelimiter(C)Z
    .locals 1
    .param p0, "c"    # C

    .prologue
    .line 1240
    const/16 v0, 0x5b

    if-eq p0, v0, :cond_0

    const/16 v0, 0x5d

    if-eq p0, v0, :cond_0

    const/16 v0, 0x28

    if-eq p0, v0, :cond_0

    const/16 v0, 0x29

    if-eq p0, v0, :cond_0

    const/16 v0, 0x3c

    if-eq p0, v0, :cond_0

    const/16 v0, 0x3e

    if-eq p0, v0, :cond_0

    const/16 v0, 0x7b

    if-eq p0, v0, :cond_0

    const/16 v0, 0x7d

    if-eq p0, v0, :cond_0

    const/16 v0, 0x2f

    if-eq p0, v0, :cond_0

    const/16 v0, 0x25

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static isExpectedInContentStream(C)Z
    .locals 1
    .param p0, "c"    # C

    .prologue
    .line 1253
    const/16 v0, 0x61

    if-lt p0, v0, :cond_0

    const/16 v0, 0x7a

    if-le p0, v0, :cond_3

    :cond_0
    const/16 v0, 0x41

    if-lt p0, v0, :cond_1

    const/16 v0, 0x5a

    if-le p0, v0, :cond_3

    :cond_1
    const/16 v0, 0x30

    if-lt p0, v0, :cond_2

    const/16 v0, 0x39

    if-le p0, v0, :cond_3

    :cond_2
    invoke-static {p0}, Lorg/icepdf/core/util/Parser;->isWhitespace(C)Z

    move-result v0

    if-nez v0, :cond_3

    invoke-static {p0}, Lorg/icepdf/core/util/Parser;->isDelimiter(C)Z

    move-result v0

    if-nez v0, :cond_3

    const/16 v0, 0x5c

    if-eq p0, v0, :cond_3

    const/16 v0, 0x27

    if-eq p0, v0, :cond_3

    const/16 v0, 0x22

    if-eq p0, v0, :cond_3

    const/16 v0, 0x2a

    if-eq p0, v0, :cond_3

    const/16 v0, 0x2e

    if-ne p0, v0, :cond_4

    :cond_3
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static isStillInlineImageData(Ljava/io/InputStream;I)Z
    .locals 8
    .param p0, "reader"    # Ljava/io/InputStream;
    .param p1, "numBytesToCheck"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1272
    const/4 v2, 0x0

    .line 1273
    .local v2, "imageDataFound":Z
    const/4 v4, 0x1

    .line 1274
    .local v4, "onlyWhitespaceSoFar":Z
    invoke-virtual {p0, p1}, Ljava/io/InputStream;->mark(I)V

    .line 1275
    new-array v5, p1, [B

    .line 1276
    .local v5, "toCheck":[B
    invoke-virtual {p0, v5}, Ljava/io/InputStream;->read([B)I

    move-result v3

    .line 1277
    .local v3, "numReadToCheck":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v3, :cond_1

    .line 1278
    aget-byte v7, v5, v1

    and-int/lit16 v7, v7, 0xff

    int-to-char v0, v7

    .line 1281
    .local v0, "charToCheck":C
    const/16 v7, 0x51

    if-eq v0, v7, :cond_0

    const/16 v7, 0x71

    if-eq v0, v7, :cond_0

    const/16 v7, 0x53

    if-eq v0, v7, :cond_0

    const/16 v7, 0x73

    if-ne v0, v7, :cond_2

    :cond_0
    const/4 v6, 0x1

    .line 1284
    .local v6, "typicalTextTokenInContentStream":Z
    :goto_1
    if-eqz v4, :cond_3

    if-eqz v6, :cond_3

    add-int/lit8 v7, v1, 0x1

    if-ge v7, v3, :cond_3

    add-int/lit8 v7, v1, 0x1

    aget-byte v7, v5, v7

    and-int/lit16 v7, v7, 0xff

    int-to-char v7, v7

    invoke-static {v7}, Lorg/icepdf/core/util/Parser;->isWhitespace(C)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 1299
    .end local v0    # "charToCheck":C
    .end local v6    # "typicalTextTokenInContentStream":Z
    :cond_1
    :goto_2
    invoke-virtual {p0}, Ljava/io/InputStream;->reset()V

    .line 1300
    return v2

    .line 1281
    .restart local v0    # "charToCheck":C
    :cond_2
    const/4 v6, 0x0

    goto :goto_1

    .line 1290
    .restart local v6    # "typicalTextTokenInContentStream":Z
    :cond_3
    invoke-static {v0}, Lorg/icepdf/core/util/Parser;->isWhitespace(C)Z

    move-result v7

    if-nez v7, :cond_4

    .line 1291
    const/4 v4, 0x0

    .line 1294
    :cond_4
    invoke-static {v0}, Lorg/icepdf/core/util/Parser;->isExpectedInContentStream(C)Z

    move-result v7

    if-nez v7, :cond_5

    .line 1295
    const/4 v2, 0x1

    .line 1296
    goto :goto_2

    .line 1277
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public static final isWhitespace(C)Z
    .locals 1
    .param p0, "c"    # C

    .prologue
    .line 1235
    const/16 v0, 0x20

    if-eq p0, v0, :cond_0

    const/16 v0, 0x9

    if-eq p0, v0, :cond_0

    const/16 v0, 0xd

    if-eq p0, v0, :cond_0

    const/16 v0, 0xa

    if-eq p0, v0, :cond_0

    const/16 v0, 0xc

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private skipUntilEndstream(Ljava/io/OutputStream;)J
    .locals 8
    .param p1, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v7, 0x65

    const/16 v6, 0xa

    .line 1348
    const-wide/16 v2, 0x0

    .line 1350
    .local v2, "skipped":J
    :cond_0
    :goto_0
    iget-object v1, p0, Lorg/icepdf/core/util/Parser;->reader:Ljava/io/InputStream;

    invoke-virtual {v1, v6}, Ljava/io/InputStream;->mark(I)V

    .line 1352
    iget-object v1, p0, Lorg/icepdf/core/util/Parser;->reader:Ljava/io/InputStream;

    invoke-virtual {v1}, Ljava/io/InputStream;->read()I

    move-result v0

    .line 1353
    .local v0, "nextByte":I
    if-ne v0, v7, :cond_2

    iget-object v1, p0, Lorg/icepdf/core/util/Parser;->reader:Ljava/io/InputStream;

    invoke-virtual {v1}, Ljava/io/InputStream;->read()I

    move-result v1

    const/16 v4, 0x6e

    if-ne v1, v4, :cond_2

    iget-object v1, p0, Lorg/icepdf/core/util/Parser;->reader:Ljava/io/InputStream;

    invoke-virtual {v1}, Ljava/io/InputStream;->read()I

    move-result v1

    const/16 v4, 0x64

    if-ne v1, v4, :cond_2

    iget-object v1, p0, Lorg/icepdf/core/util/Parser;->reader:Ljava/io/InputStream;

    invoke-virtual {v1}, Ljava/io/InputStream;->read()I

    move-result v1

    const/16 v4, 0x73

    if-ne v1, v4, :cond_2

    iget-object v1, p0, Lorg/icepdf/core/util/Parser;->reader:Ljava/io/InputStream;

    invoke-virtual {v1}, Ljava/io/InputStream;->read()I

    move-result v1

    const/16 v4, 0x74

    if-ne v1, v4, :cond_2

    iget-object v1, p0, Lorg/icepdf/core/util/Parser;->reader:Ljava/io/InputStream;

    invoke-virtual {v1}, Ljava/io/InputStream;->read()I

    move-result v1

    const/16 v4, 0x72

    if-ne v1, v4, :cond_2

    iget-object v1, p0, Lorg/icepdf/core/util/Parser;->reader:Ljava/io/InputStream;

    invoke-virtual {v1}, Ljava/io/InputStream;->read()I

    move-result v1

    if-ne v1, v7, :cond_2

    iget-object v1, p0, Lorg/icepdf/core/util/Parser;->reader:Ljava/io/InputStream;

    invoke-virtual {v1}, Ljava/io/InputStream;->read()I

    move-result v1

    const/16 v4, 0x61

    if-ne v1, v4, :cond_2

    iget-object v1, p0, Lorg/icepdf/core/util/Parser;->reader:Ljava/io/InputStream;

    invoke-virtual {v1}, Ljava/io/InputStream;->read()I

    move-result v1

    const/16 v4, 0x6d

    if-ne v1, v4, :cond_2

    .line 1362
    iget-object v1, p0, Lorg/icepdf/core/util/Parser;->reader:Ljava/io/InputStream;

    invoke-virtual {v1}, Ljava/io/InputStream;->reset()V

    .line 1374
    :cond_1
    return-wide v2

    .line 1364
    :cond_2
    if-ltz v0, :cond_1

    .line 1367
    if-eq v0, v6, :cond_0

    const/16 v1, 0xd

    if-eq v0, v1, :cond_0

    const/16 v1, 0x20

    if-eq v0, v1, :cond_0

    .line 1369
    if-eqz p1, :cond_3

    .line 1370
    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write(I)V

    .line 1372
    :cond_3
    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    .line 1373
    goto :goto_0
.end method


# virtual methods
.method public addPObject(Lorg/icepdf/core/util/Library;Lorg/icepdf/core/pobjects/Reference;)Lorg/icepdf/core/pobjects/PObject;
    .locals 3
    .param p1, "library"    # Lorg/icepdf/core/util/Library;
    .param p2, "objectReference"    # Lorg/icepdf/core/pobjects/Reference;

    .prologue
    .line 569
    iget-object v2, p0, Lorg/icepdf/core/util/Parser;->stack:Ljava/util/Stack;

    invoke-virtual {v2}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v0

    .line 573
    .local v0, "o":Ljava/lang/Object;
    instance-of v2, v0, Lorg/icepdf/core/pobjects/Stream;

    if-eqz v2, :cond_1

    move-object v1, v0

    .line 574
    check-cast v1, Lorg/icepdf/core/pobjects/Stream;

    .line 575
    .local v1, "tmp":Lorg/icepdf/core/pobjects/Stream;
    invoke-virtual {v1, p2}, Lorg/icepdf/core/pobjects/Stream;->setPObjectReference(Lorg/icepdf/core/pobjects/Reference;)V

    .line 586
    .end local v1    # "tmp":Lorg/icepdf/core/pobjects/Stream;
    :cond_0
    :goto_0
    invoke-virtual {p1, v0, p2}, Lorg/icepdf/core/util/Library;->addObject(Ljava/lang/Object;Lorg/icepdf/core/pobjects/Reference;)V

    .line 588
    new-instance v2, Lorg/icepdf/core/pobjects/PObject;

    invoke-direct {v2, v0, p2}, Lorg/icepdf/core/pobjects/PObject;-><init>(Ljava/lang/Object;Lorg/icepdf/core/pobjects/Reference;)V

    return-object v2

    .line 580
    :cond_1
    instance-of v2, v0, Lorg/icepdf/core/pobjects/Dictionary;

    if-eqz v2, :cond_0

    move-object v1, v0

    .line 581
    check-cast v1, Lorg/icepdf/core/pobjects/Dictionary;

    .line 582
    .local v1, "tmp":Lorg/icepdf/core/pobjects/Dictionary;
    invoke-virtual {v1, p2}, Lorg/icepdf/core/pobjects/Dictionary;->setPObjectReference(Lorg/icepdf/core/pobjects/Reference;)V

    goto :goto_0
.end method

.method public getCharSurroundedByWhitespace()C
    .locals 7

    .prologue
    .line 1097
    const/4 v0, 0x0

    .line 1100
    .local v0, "alpha":C
    :cond_0
    :try_start_0
    iget-object v4, p0, Lorg/icepdf/core/util/Parser;->reader:Ljava/io/InputStream;

    invoke-virtual {v4}, Ljava/io/InputStream;->read()I

    move-result v2

    .line 1101
    .local v2, "curr":I
    if-gez v2, :cond_1

    .line 1113
    .end local v2    # "curr":I
    :goto_0
    return v0

    .line 1103
    .restart local v2    # "curr":I
    :cond_1
    int-to-char v1, v2

    .line 1104
    .local v1, "c":C
    invoke-static {v1}, Ljava/lang/Character;->isWhitespace(C)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    if-nez v4, :cond_0

    .line 1105
    move v0, v1

    .line 1106
    goto :goto_0

    .line 1110
    .end local v1    # "c":C
    .end local v2    # "curr":I
    :catch_0
    move-exception v3

    .line 1111
    .local v3, "e":Ljava/io/IOException;
    sget-object v4, Lorg/icepdf/core/util/Parser;->logger:Ljava/util/logging/Logger;

    sget-object v5, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string/jumbo v6, "Error detecting char."

    invoke-virtual {v4, v5, v6, v3}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public getIntSurroundedByWhitespace()I
    .locals 8

    .prologue
    .line 1037
    const/4 v3, 0x0

    .line 1038
    .local v3, "num":I
    const/4 v2, 0x0

    .line 1039
    .local v2, "makeNegative":Z
    const/4 v4, 0x0

    .line 1042
    .local v4, "readNonWhitespace":Z
    :cond_0
    :goto_0
    :try_start_0
    iget-object v5, p0, Lorg/icepdf/core/util/Parser;->reader:Ljava/io/InputStream;

    invoke-virtual {v5}, Ljava/io/InputStream;->read()I

    move-result v0

    .line 1043
    .local v0, "curr":I
    if-gez v0, :cond_2

    .line 1061
    .end local v0    # "curr":I
    :goto_1
    if-eqz v2, :cond_1

    .line 1062
    mul-int/lit8 v3, v3, -0x1

    .line 1063
    :cond_1
    return v3

    .line 1045
    .restart local v0    # "curr":I
    :cond_2
    int-to-char v5, v0

    invoke-static {v5}, Ljava/lang/Character;->isWhitespace(C)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v5

    if-eqz v5, :cond_3

    .line 1046
    if-eqz v4, :cond_0

    goto :goto_1

    .line 1048
    :cond_3
    const/16 v5, 0x2d

    if-ne v0, v5, :cond_4

    .line 1049
    const/4 v2, 0x1

    .line 1050
    const/4 v4, 0x1

    goto :goto_0

    .line 1051
    :cond_4
    const/16 v5, 0x30

    if-lt v0, v5, :cond_0

    const/16 v5, 0x39

    if-gt v0, v5, :cond_0

    .line 1052
    mul-int/lit8 v3, v3, 0xa

    .line 1053
    add-int/lit8 v5, v0, -0x30

    add-int/2addr v3, v5

    .line 1054
    const/4 v4, 0x1

    goto :goto_0

    .line 1058
    .end local v0    # "curr":I
    :catch_0
    move-exception v1

    .line 1059
    .local v1, "e":Ljava/io/IOException;
    sget-object v5, Lorg/icepdf/core/util/Parser;->logger:Ljava/util/logging/Logger;

    sget-object v6, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string/jumbo v7, "Error detecting int."

    invoke-virtual {v5, v6, v7, v1}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method public getLongSurroundedByWhitespace()J
    .locals 9

    .prologue
    .line 1067
    const-wide/16 v4, 0x0

    .line 1068
    .local v4, "num":J
    const/4 v2, 0x0

    .line 1069
    .local v2, "makeNegative":Z
    const/4 v3, 0x0

    .line 1072
    .local v3, "readNonWhitespace":Z
    :cond_0
    :goto_0
    :try_start_0
    iget-object v6, p0, Lorg/icepdf/core/util/Parser;->reader:Ljava/io/InputStream;

    invoke-virtual {v6}, Ljava/io/InputStream;->read()I

    move-result v0

    .line 1073
    .local v0, "curr":I
    if-gez v0, :cond_2

    .line 1091
    .end local v0    # "curr":I
    :goto_1
    if-eqz v2, :cond_1

    .line 1092
    const-wide/16 v6, -0x1

    mul-long/2addr v4, v6

    .line 1093
    :cond_1
    return-wide v4

    .line 1075
    .restart local v0    # "curr":I
    :cond_2
    int-to-char v6, v0

    invoke-static {v6}, Ljava/lang/Character;->isWhitespace(C)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v6

    if-eqz v6, :cond_3

    .line 1076
    if-eqz v3, :cond_0

    goto :goto_1

    .line 1078
    :cond_3
    const/16 v6, 0x2d

    if-ne v0, v6, :cond_4

    .line 1079
    const/4 v2, 0x1

    .line 1080
    const/4 v3, 0x1

    goto :goto_0

    .line 1081
    :cond_4
    const/16 v6, 0x30

    if-lt v0, v6, :cond_0

    const/16 v6, 0x39

    if-gt v0, v6, :cond_0

    .line 1082
    const-wide/16 v6, 0xa

    mul-long/2addr v4, v6

    .line 1083
    add-int/lit8 v6, v0, -0x30

    int-to-long v6, v6

    add-long/2addr v4, v6

    .line 1084
    const/4 v3, 0x1

    goto :goto_0

    .line 1088
    .end local v0    # "curr":I
    :catch_0
    move-exception v1

    .line 1089
    .local v1, "e":Ljava/io/IOException;
    sget-object v6, Lorg/icepdf/core/util/Parser;->logger:Ljava/util/logging/Logger;

    sget-object v7, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string/jumbo v8, "Error detecting long."

    invoke-virtual {v6, v7, v8, v1}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method public getNumberOrStringWithMark(I)Ljava/lang/Object;
    .locals 9
    .param p1, "maxLength"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 976
    iget-object v8, p0, Lorg/icepdf/core/util/Parser;->reader:Ljava/io/InputStream;

    invoke-virtual {v8, p1}, Ljava/io/InputStream;->mark(I)V

    .line 978
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7, p1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 979
    .local v7, "sb":Ljava/lang/StringBuilder;
    const/4 v6, 0x0

    .line 980
    .local v6, "readNonWhitespaceYet":Z
    const/4 v3, 0x0

    .line 981
    .local v3, "foundDigit":Z
    const/4 v2, 0x0

    .line 983
    .local v2, "foundDecimal":Z
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    if-ge v4, p1, :cond_0

    .line 984
    iget-object v8, p0, Lorg/icepdf/core/util/Parser;->reader:Ljava/io/InputStream;

    invoke-virtual {v8}, Ljava/io/InputStream;->read()I

    move-result v0

    .line 985
    .local v0, "curr":I
    if-gez v0, :cond_1

    .line 1014
    .end local v0    # "curr":I
    :cond_0
    :goto_1
    if-eqz v3, :cond_8

    .line 1016
    if-eqz v2, :cond_7

    .line 1017
    :try_start_0
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v8

    .line 1029
    :goto_2
    return-object v8

    .line 987
    .restart local v0    # "curr":I
    :cond_1
    int-to-char v1, v0

    .line 988
    .local v1, "currChar":C
    invoke-static {v1}, Lorg/icepdf/core/util/Parser;->isWhitespace(C)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 989
    if-nez v6, :cond_0

    .line 983
    :goto_3
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 991
    :cond_2
    invoke-static {v1}, Lorg/icepdf/core/util/Parser;->isDelimiter(C)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 995
    iget-object v8, p0, Lorg/icepdf/core/util/Parser;->reader:Ljava/io/InputStream;

    invoke-virtual {v8}, Ljava/io/InputStream;->reset()V

    .line 996
    iget-object v8, p0, Lorg/icepdf/core/util/Parser;->reader:Ljava/io/InputStream;

    invoke-virtual {v8, p1}, Ljava/io/InputStream;->mark(I)V

    .line 997
    const/4 v5, 0x0

    .local v5, "j":I
    :goto_4
    if-ge v5, v4, :cond_3

    .line 998
    iget-object v8, p0, Lorg/icepdf/core/util/Parser;->reader:Ljava/io/InputStream;

    invoke-virtual {v8}, Ljava/io/InputStream;->read()I

    .line 997
    add-int/lit8 v5, v5, 0x1

    goto :goto_4

    .line 1000
    :cond_3
    const/4 v6, 0x1

    .line 1001
    goto :goto_1

    .line 1003
    .end local v5    # "j":I
    :cond_4
    const/4 v6, 0x1

    .line 1004
    const/16 v8, 0x2e

    if-ne v1, v8, :cond_6

    .line 1005
    const/4 v2, 0x1

    .line 1008
    :cond_5
    :goto_5
    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 1006
    :cond_6
    const/16 v8, 0x30

    if-lt v1, v8, :cond_5

    const/16 v8, 0x39

    if-gt v0, v8, :cond_5

    .line 1007
    const/4 v3, 0x1

    goto :goto_5

    .line 1019
    .end local v0    # "curr":I
    .end local v1    # "currChar":C
    :cond_7
    :try_start_1
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v8

    goto :goto_2

    .line 1022
    :catch_0
    move-exception v8

    .line 1027
    :cond_8
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->length()I

    move-result v8

    if-lez v8, :cond_9

    .line 1028
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    goto :goto_2

    .line 1029
    :cond_9
    const/4 v8, 0x0

    goto :goto_2
.end method

.method public getObject(Lorg/icepdf/core/util/Library;)Ljava/lang/Object;
    .locals 50
    .param p1, "library"    # Lorg/icepdf/core/util/Library;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/icepdf/core/exceptions/PDFException;
        }
    .end annotation

    .prologue
    .line 85
    const/4 v15, 0x0

    .line 86
    .local v15, "deepnessCount":I
    const/16 v21, 0x0

    .line 87
    .local v21, "inObject":Z
    const/4 v12, 0x0

    .line 89
    .local v12, "complete":Z
    const/16 v27, 0x0

    .line 90
    .local v27, "objectReference":Lorg/icepdf/core/pobjects/Reference;
    const/16 v38, 0x0

    .local v38, "tokenCount":I
    move-object/from16 v28, v27

    .line 98
    .end local v27    # "objectReference":Lorg/icepdf/core/pobjects/Reference;
    .local v28, "objectReference":Lorg/icepdf/core/pobjects/Reference;
    :goto_0
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lorg/icepdf/core/util/Parser;->getToken()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v24

    .line 112
    .local v24, "nextToken":Ljava/lang/Object;
    :try_start_1
    move-object/from16 v0, v24

    instance-of v10, v0, Lorg/icepdf/core/pobjects/StringObject;

    if-nez v10, :cond_0

    move-object/from16 v0, v24

    instance-of v10, v0, Lorg/icepdf/core/pobjects/Name;

    if-nez v10, :cond_0

    move-object/from16 v0, v24

    instance-of v10, v0, Ljava/lang/Number;

    if-eqz v10, :cond_3

    .line 117
    :cond_0
    move-object/from16 v0, v24

    instance-of v10, v0, Lorg/icepdf/core/pobjects/StringObject;

    if-eqz v10, :cond_1

    .line 118
    move-object/from16 v0, v24

    check-cast v0, Lorg/icepdf/core/pobjects/StringObject;

    move-object/from16 v37, v0

    .line 119
    .local v37, "tmp":Lorg/icepdf/core/pobjects/StringObject;
    move-object/from16 v0, v37

    move-object/from16 v1, v28

    invoke-interface {v0, v1}, Lorg/icepdf/core/pobjects/StringObject;->setReference(Lorg/icepdf/core/pobjects/Reference;)V

    .line 124
    .end local v37    # "tmp":Lorg/icepdf/core/pobjects/StringObject;
    :cond_1
    move-object/from16 v0, v24

    instance-of v10, v0, Ljava/lang/Number;

    if-eqz v10, :cond_2

    .line 126
    add-int/lit8 v38, v38, 0x1

    .line 127
    const/16 v10, 0x3e8

    move/from16 v0, v38

    if-le v0, v10, :cond_2

    .line 128
    new-instance v10, Lorg/icepdf/core/exceptions/PDFException;

    const-string/jumbo v47, "Not a valid PDF file"

    move-object/from16 v0, v47

    invoke-direct {v10, v0}, Lorg/icepdf/core/exceptions/PDFException;-><init>(Ljava/lang/String;)V

    throw v10
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 551
    .end local v24    # "nextToken":Ljava/lang/Object;
    :catch_0
    move-exception v16

    move-object/from16 v27, v28

    .line 552
    .end local v28    # "objectReference":Lorg/icepdf/core/pobjects/Reference;
    .local v16, "e":Ljava/lang/Exception;
    .restart local v27    # "objectReference":Lorg/icepdf/core/pobjects/Reference;
    :goto_1
    sget-object v10, Lorg/icepdf/core/util/Parser;->logger:Ljava/util/logging/Logger;

    sget-object v47, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string/jumbo v48, "Fatal error parsing PDF file stream."

    move-object/from16 v0, v47

    move-object/from16 v1, v48

    move-object/from16 v2, v16

    invoke-virtual {v10, v0, v1, v2}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 553
    const/4 v10, 0x0

    .line 556
    .end local v16    # "e":Ljava/lang/Exception;
    :goto_2
    return-object v10

    .line 103
    .end local v27    # "objectReference":Lorg/icepdf/core/pobjects/Reference;
    .restart local v28    # "objectReference":Lorg/icepdf/core/pobjects/Reference;
    :catch_1
    move-exception v16

    .line 108
    .local v16, "e":Ljava/io/IOException;
    const/4 v10, 0x0

    move-object/from16 v27, v28

    .end local v28    # "objectReference":Lorg/icepdf/core/pobjects/Reference;
    .restart local v27    # "objectReference":Lorg/icepdf/core/pobjects/Reference;
    goto :goto_2

    .line 130
    .end local v16    # "e":Ljava/io/IOException;
    .end local v27    # "objectReference":Lorg/icepdf/core/pobjects/Reference;
    .restart local v24    # "nextToken":Ljava/lang/Object;
    .restart local v28    # "objectReference":Lorg/icepdf/core/pobjects/Reference;
    :cond_2
    :try_start_2
    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/icepdf/core/util/Parser;->stack:Ljava/util/Stack;

    move-object/from16 v0, v24

    invoke-virtual {v10, v0}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    move-object/from16 v27, v28

    .line 542
    .end local v28    # "objectReference":Lorg/icepdf/core/pobjects/Reference;
    .restart local v27    # "objectReference":Lorg/icepdf/core/pobjects/Reference;
    :goto_3
    :try_start_3
    move-object/from16 v0, p0

    iget v10, v0, Lorg/icepdf/core/util/Parser;->parseMode:I

    const/16 v47, 0x1

    move/from16 v0, v47

    if-ne v10, v0, :cond_34

    if-nez v15, :cond_34

    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/icepdf/core/util/Parser;->stack:Ljava/util/Stack;

    invoke-virtual {v10}, Ljava/util/Stack;->size()I

    move-result v10

    if-lez v10, :cond_34

    .line 543
    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/icepdf/core/util/Parser;->stack:Ljava/util/Stack;

    invoke-virtual {v10}, Ljava/util/Stack;->pop()Ljava/lang/Object;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_5

    move-result-object v10

    goto :goto_2

    .line 133
    .end local v27    # "objectReference":Lorg/icepdf/core/pobjects/Reference;
    .restart local v28    # "objectReference":Lorg/icepdf/core/pobjects/Reference;
    :cond_3
    :try_start_4
    const-string/jumbo v10, "obj"

    move-object/from16 v0, v24

    invoke-virtual {v0, v10}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_5

    .line 136
    if-eqz v21, :cond_4

    .line 138
    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/icepdf/core/util/Parser;->stack:Ljava/util/Stack;

    invoke-virtual {v10}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    .line 139
    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/icepdf/core/util/Parser;->stack:Ljava/util/Stack;

    invoke-virtual {v10}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    .line 141
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v28

    invoke-virtual {v0, v1, v2}, Lorg/icepdf/core/util/Parser;->addPObject(Lorg/icepdf/core/util/Library;Lorg/icepdf/core/pobjects/Reference;)Lorg/icepdf/core/pobjects/PObject;

    move-result-object v10

    move-object/from16 v27, v28

    .end local v28    # "objectReference":Lorg/icepdf/core/pobjects/Reference;
    .restart local v27    # "objectReference":Lorg/icepdf/core/pobjects/Reference;
    goto :goto_2

    .line 147
    .end local v27    # "objectReference":Lorg/icepdf/core/pobjects/Reference;
    .restart local v28    # "objectReference":Lorg/icepdf/core/pobjects/Reference;
    :cond_4
    const/4 v15, 0x1

    .line 148
    const/16 v21, 0x1

    .line 149
    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/icepdf/core/util/Parser;->stack:Ljava/util/Stack;

    invoke-virtual {v10}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Number;

    move-object v0, v10

    check-cast v0, Ljava/lang/Number;

    move-object/from16 v17, v0

    .line 150
    .local v17, "generationNumber":Ljava/lang/Number;
    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/icepdf/core/util/Parser;->stack:Ljava/util/Stack;

    invoke-virtual {v10}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Number;

    move-object v0, v10

    check-cast v0, Ljava/lang/Number;

    move-object/from16 v26, v0

    .line 151
    .local v26, "objectNumber":Ljava/lang/Number;
    new-instance v27, Lorg/icepdf/core/pobjects/Reference;

    move-object/from16 v0, v27

    move-object/from16 v1, v26

    move-object/from16 v2, v17

    invoke-direct {v0, v1, v2}, Lorg/icepdf/core/pobjects/Reference;-><init>(Ljava/lang/Number;Ljava/lang/Number;)V

    .line 153
    .end local v28    # "objectReference":Lorg/icepdf/core/pobjects/Reference;
    .restart local v27    # "objectReference":Lorg/icepdf/core/pobjects/Reference;
    goto :goto_3

    .line 155
    .end local v17    # "generationNumber":Ljava/lang/Number;
    .end local v26    # "objectNumber":Ljava/lang/Number;
    .end local v27    # "objectReference":Lorg/icepdf/core/pobjects/Reference;
    .restart local v28    # "objectReference":Lorg/icepdf/core/pobjects/Reference;
    :cond_5
    const-string/jumbo v10, "endobj"

    move-object/from16 v0, v24

    invoke-virtual {v0, v10}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_7

    .line 156
    add-int/lit8 v15, v15, -0x1

    .line 158
    if-eqz v21, :cond_6

    .line 160
    const/16 v21, 0x0

    .line 162
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v28

    invoke-virtual {v0, v1, v2}, Lorg/icepdf/core/util/Parser;->addPObject(Lorg/icepdf/core/util/Library;Lorg/icepdf/core/pobjects/Reference;)Lorg/icepdf/core/pobjects/PObject;

    move-result-object v10

    move-object/from16 v27, v28

    .end local v28    # "objectReference":Lorg/icepdf/core/pobjects/Reference;
    .restart local v27    # "objectReference":Lorg/icepdf/core/pobjects/Reference;
    goto/16 :goto_2

    .line 166
    .end local v27    # "objectReference":Lorg/icepdf/core/pobjects/Reference;
    .restart local v28    # "objectReference":Lorg/icepdf/core/pobjects/Reference;
    :cond_6
    const/4 v10, 0x0

    move-object/from16 v27, v28

    .end local v28    # "objectReference":Lorg/icepdf/core/pobjects/Reference;
    .restart local v27    # "objectReference":Lorg/icepdf/core/pobjects/Reference;
    goto/16 :goto_2

    .line 172
    .end local v27    # "objectReference":Lorg/icepdf/core/pobjects/Reference;
    .restart local v28    # "objectReference":Lorg/icepdf/core/pobjects/Reference;
    :cond_7
    const-string/jumbo v10, "endstream"

    move-object/from16 v0, v24

    invoke-virtual {v0, v10}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_8

    .line 173
    add-int/lit8 v15, v15, -0x1

    .line 175
    if-eqz v21, :cond_36

    .line 176
    const/16 v21, 0x0

    .line 178
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v28

    invoke-virtual {v0, v1, v2}, Lorg/icepdf/core/util/Parser;->addPObject(Lorg/icepdf/core/util/Library;Lorg/icepdf/core/pobjects/Reference;)Lorg/icepdf/core/pobjects/PObject;

    move-result-object v10

    move-object/from16 v27, v28

    .end local v28    # "objectReference":Lorg/icepdf/core/pobjects/Reference;
    .restart local v27    # "objectReference":Lorg/icepdf/core/pobjects/Reference;
    goto/16 :goto_2

    .line 185
    .end local v27    # "objectReference":Lorg/icepdf/core/pobjects/Reference;
    .restart local v28    # "objectReference":Lorg/icepdf/core/pobjects/Reference;
    :cond_8
    const-string/jumbo v10, "stream"

    move-object/from16 v0, v24

    invoke-virtual {v0, v10}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1a

    .line 187
    add-int/lit8 v15, v15, 0x1

    .line 189
    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/icepdf/core/util/Parser;->stack:Ljava/util/Stack;

    invoke-virtual {v10}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v34

    check-cast v34, Ljava/util/Hashtable;

    .line 192
    .local v34, "streamHash":Ljava/util/Hashtable;
    const-string/jumbo v10, "Length"

    move-object/from16 v0, p1

    move-object/from16 v1, v34

    invoke-virtual {v0, v1, v10}, Lorg/icepdf/core/util/Library;->getInt(Ljava/util/Hashtable;Ljava/lang/String;)I
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    move-result v35

    .line 196
    .local v35, "streamLength":I
    const/16 v29, 0x0

    .line 212
    .local v29, "out":Lorg/icepdf/core/io/ConservativeSizingByteArrayOutputStream;
    :try_start_5
    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/icepdf/core/util/Parser;->reader:Ljava/io/InputStream;

    const/16 v47, 0x2

    move/from16 v0, v47

    invoke-virtual {v10, v0}, Ljava/io/InputStream;->mark(I)V

    .line 215
    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/icepdf/core/util/Parser;->reader:Ljava/io/InputStream;

    invoke-virtual {v10}, Ljava/io/InputStream;->read()I

    move-result v13

    .line 216
    .local v13, "curChar":I
    const/16 v10, 0xd

    if-ne v13, v10, :cond_d

    .line 217
    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/icepdf/core/util/Parser;->reader:Ljava/io/InputStream;

    const/16 v47, 0x1

    move/from16 v0, v47

    invoke-virtual {v10, v0}, Ljava/io/InputStream;->mark(I)V

    .line 218
    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/icepdf/core/util/Parser;->reader:Ljava/io/InputStream;

    invoke-virtual {v10}, Ljava/io/InputStream;->read()I

    move-result v10

    const/16 v47, 0xa

    move/from16 v0, v47

    if-eq v10, v0, :cond_9

    .line 219
    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/icepdf/core/util/Parser;->reader:Ljava/io/InputStream;

    invoke-virtual {v10}, Ljava/io/InputStream;->reset()V

    .line 239
    :cond_9
    :goto_4
    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/icepdf/core/util/Parser;->reader:Ljava/io/InputStream;

    instance-of v10, v10, Lorg/icepdf/core/io/SeekableInput;

    if-eqz v10, :cond_10

    .line 240
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/icepdf/core/util/Parser;->reader:Ljava/io/InputStream;

    check-cast v5, Lorg/icepdf/core/io/SeekableInput;

    .line 241
    .local v5, "streamDataInput":Lorg/icepdf/core/io/SeekableInput;
    invoke-interface {v5}, Lorg/icepdf/core/io/SeekableInput;->getAbsolutePosition()J

    move-result-wide v6

    .line 245
    .local v6, "filePositionOfStreamData":J
    if-lez v35, :cond_f

    .line 246
    move/from16 v0, v35

    int-to-long v8, v0

    .line 247
    .local v8, "lengthOfStreamData":J
    move/from16 v0, v35

    int-to-long v0, v0

    move-wide/from16 v48, v0

    move-wide/from16 v0, v48

    invoke-interface {v5, v0, v1}, Lorg/icepdf/core/io/SeekableInput;->seekRelative(J)V

    .line 250
    const/4 v10, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v10}, Lorg/icepdf/core/util/Parser;->skipUntilEndstream(Ljava/io/OutputStream;)J

    move-result-wide v48

    add-long v8, v8, v48

    .line 254
    :goto_5
    new-instance v4, Lorg/icepdf/core/io/SeekableInputConstrainedWrapper;

    const/4 v10, 0x0

    invoke-direct/range {v4 .. v10}, Lorg/icepdf/core/io/SeekableInputConstrainedWrapper;-><init>(Lorg/icepdf/core/io/SeekableInput;JJZ)V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 311
    .local v4, "streamInputWrapper":Lorg/icepdf/core/io/SeekableInputConstrainedWrapper;
    :goto_6
    if-eqz v29, :cond_a

    .line 312
    :try_start_6
    invoke-virtual/range {v29 .. v29}, Lorg/icepdf/core/io/ConservativeSizingByteArrayOutputStream;->close()V

    .line 314
    :cond_a
    const/16 v40, 0x0

    .line 316
    .local v40, "trailer":Lorg/icepdf/core/pobjects/PTrailer;
    const/16 v33, 0x0

    .line 318
    .local v33, "stream":Lorg/icepdf/core/pobjects/Stream;
    const-string/jumbo v10, "Type"

    move-object/from16 v0, p1

    move-object/from16 v1, v34

    invoke-virtual {v0, v1, v10}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v43

    check-cast v43, Lorg/icepdf/core/pobjects/Name;

    .line 319
    .local v43, "type":Lorg/icepdf/core/pobjects/Name;
    const-string/jumbo v10, "Subtype"

    move-object/from16 v0, p1

    move-object/from16 v1, v34

    invoke-virtual {v0, v1, v10}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v36

    check-cast v36, Lorg/icepdf/core/pobjects/Name;

    .line 320
    .local v36, "subtype":Lorg/icepdf/core/pobjects/Name;
    if-eqz v43, :cond_b

    .line 322
    const-string/jumbo v10, "Pattern"

    move-object/from16 v0, v43

    invoke-virtual {v0, v10}, Lorg/icepdf/core/pobjects/Name;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_15

    .line 323
    new-instance v33, Lorg/icepdf/core/pobjects/graphics/TilingPattern;

    .end local v33    # "stream":Lorg/icepdf/core/pobjects/Stream;
    move-object/from16 v0, v33

    move-object/from16 v1, p1

    move-object/from16 v2, v34

    invoke-direct {v0, v1, v2, v4}, Lorg/icepdf/core/pobjects/graphics/TilingPattern;-><init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;Lorg/icepdf/core/io/SeekableInputConstrainedWrapper;)V

    .line 352
    .restart local v33    # "stream":Lorg/icepdf/core/pobjects/Stream;
    :cond_b
    :goto_7
    if-eqz v36, :cond_c

    .line 354
    const-string/jumbo v10, "Form"

    move-object/from16 v0, v36

    invoke-virtual {v0, v10}, Lorg/icepdf/core/pobjects/Name;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_c

    const-string/jumbo v10, "pattern"

    move-object/from16 v0, v43

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_c

    .line 355
    new-instance v33, Lorg/icepdf/core/pobjects/Form;

    .end local v33    # "stream":Lorg/icepdf/core/pobjects/Stream;
    move-object/from16 v0, v33

    move-object/from16 v1, p1

    move-object/from16 v2, v34

    invoke-direct {v0, v1, v2, v4}, Lorg/icepdf/core/pobjects/Form;-><init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;Lorg/icepdf/core/io/SeekableInputConstrainedWrapper;)V

    .line 358
    .restart local v33    # "stream":Lorg/icepdf/core/pobjects/Stream;
    :cond_c
    if-eqz v40, :cond_18

    .line 359
    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/icepdf/core/util/Parser;->stack:Ljava/util/Stack;

    move-object/from16 v0, v40

    invoke-virtual {v10, v0}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_0

    :goto_8
    move-object/from16 v27, v28

    .line 368
    .end local v28    # "objectReference":Lorg/icepdf/core/pobjects/Reference;
    .restart local v27    # "objectReference":Lorg/icepdf/core/pobjects/Reference;
    goto/16 :goto_3

    .line 223
    .end local v4    # "streamInputWrapper":Lorg/icepdf/core/io/SeekableInputConstrainedWrapper;
    .end local v5    # "streamDataInput":Lorg/icepdf/core/io/SeekableInput;
    .end local v6    # "filePositionOfStreamData":J
    .end local v8    # "lengthOfStreamData":J
    .end local v27    # "objectReference":Lorg/icepdf/core/pobjects/Reference;
    .end local v33    # "stream":Lorg/icepdf/core/pobjects/Stream;
    .end local v36    # "subtype":Lorg/icepdf/core/pobjects/Name;
    .end local v40    # "trailer":Lorg/icepdf/core/pobjects/PTrailer;
    .end local v43    # "type":Lorg/icepdf/core/pobjects/Name;
    .restart local v28    # "objectReference":Lorg/icepdf/core/pobjects/Reference;
    :cond_d
    const/16 v10, 0xa

    if-eq v13, v10, :cond_9

    .line 228
    :try_start_7
    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/icepdf/core/util/Parser;->reader:Ljava/io/InputStream;

    invoke-virtual {v10}, Ljava/io/InputStream;->reset()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_2
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto/16 :goto_4

    .line 307
    .end local v13    # "curChar":I
    :catch_2
    move-exception v16

    .line 308
    .restart local v16    # "e":Ljava/io/IOException;
    :goto_9
    const/4 v10, 0x0

    .line 311
    if-eqz v29, :cond_e

    .line 312
    :try_start_8
    invoke-virtual/range {v29 .. v29}, Lorg/icepdf/core/io/ConservativeSizingByteArrayOutputStream;->close()V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_0

    :cond_e
    move-object/from16 v27, v28

    .end local v28    # "objectReference":Lorg/icepdf/core/pobjects/Reference;
    .restart local v27    # "objectReference":Lorg/icepdf/core/pobjects/Reference;
    goto/16 :goto_2

    .line 252
    .end local v16    # "e":Ljava/io/IOException;
    .end local v27    # "objectReference":Lorg/icepdf/core/pobjects/Reference;
    .restart local v5    # "streamDataInput":Lorg/icepdf/core/io/SeekableInput;
    .restart local v6    # "filePositionOfStreamData":J
    .restart local v13    # "curChar":I
    .restart local v28    # "objectReference":Lorg/icepdf/core/pobjects/Reference;
    :cond_f
    const/4 v10, 0x0

    :try_start_9
    move-object/from16 v0, p0

    invoke-direct {v0, v10}, Lorg/icepdf/core/util/Parser;->captureStreamData(Ljava/io/OutputStream;)J

    move-result-wide v8

    .restart local v8    # "lengthOfStreamData":J
    goto/16 :goto_5

    .line 267
    .end local v5    # "streamDataInput":Lorg/icepdf/core/io/SeekableInput;
    .end local v6    # "filePositionOfStreamData":J
    .end local v8    # "lengthOfStreamData":J
    :cond_10
    invoke-virtual/range {p1 .. p1}, Lorg/icepdf/core/util/Library;->isLinearTraversal()Z

    move-result v10

    if-nez v10, :cond_13

    if-lez v35, :cond_13

    .line 268
    move/from16 v0, v35

    new-array v11, v0, [B

    .line 269
    .local v11, "buffer":[B
    const/16 v39, 0x0

    .line 270
    .local v39, "totalRead":I
    :goto_a
    array-length v10, v11

    move/from16 v0, v39

    if-ge v0, v10, :cond_11

    .line 271
    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/icepdf/core/util/Parser;->reader:Ljava/io/InputStream;

    array-length v0, v11

    move/from16 v47, v0

    sub-int v47, v47, v39

    move/from16 v0, v39

    move/from16 v1, v47

    invoke-virtual {v10, v11, v0, v1}, Ljava/io/InputStream;->read([BII)I

    move-result v14

    .line 275
    .local v14, "currRead":I
    if-gtz v14, :cond_12

    .line 280
    .end local v14    # "currRead":I
    :cond_11
    new-instance v30, Lorg/icepdf/core/io/ConservativeSizingByteArrayOutputStream;

    move-object/from16 v0, p1

    iget-object v10, v0, Lorg/icepdf/core/util/Library;->memoryManager:Lorg/icepdf/core/util/MemoryManager;

    move-object/from16 v0, v30

    invoke-direct {v0, v11, v10}, Lorg/icepdf/core/io/ConservativeSizingByteArrayOutputStream;-><init>([BLorg/icepdf/core/util/MemoryManager;)V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_2
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 284
    .end local v29    # "out":Lorg/icepdf/core/io/ConservativeSizingByteArrayOutputStream;
    .local v30, "out":Lorg/icepdf/core/io/ConservativeSizingByteArrayOutputStream;
    :try_start_a
    move-object/from16 v0, p0

    move-object/from16 v1, v30

    invoke-direct {v0, v1}, Lorg/icepdf/core/util/Parser;->skipUntilEndstream(Ljava/io/OutputStream;)J
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_6
    .catchall {:try_start_a .. :try_end_a} :catchall_2

    move-object/from16 v29, v30

    .line 296
    .end local v11    # "buffer":[B
    .end local v30    # "out":Lorg/icepdf/core/io/ConservativeSizingByteArrayOutputStream;
    .end local v39    # "totalRead":I
    .restart local v29    # "out":Lorg/icepdf/core/io/ConservativeSizingByteArrayOutputStream;
    :goto_b
    :try_start_b
    invoke-virtual/range {v29 .. v29}, Lorg/icepdf/core/io/ConservativeSizingByteArrayOutputStream;->size()I

    move-result v32

    .line 297
    .local v32, "size":I
    invoke-virtual/range {v29 .. v29}, Lorg/icepdf/core/io/ConservativeSizingByteArrayOutputStream;->trim()Z

    .line 298
    invoke-virtual/range {v29 .. v29}, Lorg/icepdf/core/io/ConservativeSizingByteArrayOutputStream;->relinquishByteArray()[B

    move-result-object v11

    .line 300
    .restart local v11    # "buffer":[B
    new-instance v5, Lorg/icepdf/core/io/SeekableByteArrayInputStream;

    invoke-direct {v5, v11}, Lorg/icepdf/core/io/SeekableByteArrayInputStream;-><init>([B)V

    .line 301
    .restart local v5    # "streamDataInput":Lorg/icepdf/core/io/SeekableInput;
    const-wide/16 v6, 0x0

    .line 302
    .restart local v6    # "filePositionOfStreamData":J
    move/from16 v0, v32

    int-to-long v8, v0

    .line 303
    .restart local v8    # "lengthOfStreamData":J
    new-instance v4, Lorg/icepdf/core/io/SeekableInputConstrainedWrapper;

    const/4 v10, 0x1

    invoke-direct/range {v4 .. v10}, Lorg/icepdf/core/io/SeekableInputConstrainedWrapper;-><init>(Lorg/icepdf/core/io/SeekableInput;JJZ)V

    .restart local v4    # "streamInputWrapper":Lorg/icepdf/core/io/SeekableInputConstrainedWrapper;
    goto/16 :goto_6

    .line 277
    .end local v4    # "streamInputWrapper":Lorg/icepdf/core/io/SeekableInputConstrainedWrapper;
    .end local v5    # "streamDataInput":Lorg/icepdf/core/io/SeekableInput;
    .end local v6    # "filePositionOfStreamData":J
    .end local v8    # "lengthOfStreamData":J
    .end local v32    # "size":I
    .restart local v14    # "currRead":I
    .restart local v39    # "totalRead":I
    :cond_12
    add-int v39, v39, v14

    .line 279
    goto :goto_a

    .line 290
    .end local v11    # "buffer":[B
    .end local v14    # "currRead":I
    .end local v39    # "totalRead":I
    :cond_13
    new-instance v30, Lorg/icepdf/core/io/ConservativeSizingByteArrayOutputStream;

    const/16 v10, 0x4000

    move-object/from16 v0, p1

    iget-object v0, v0, Lorg/icepdf/core/util/Library;->memoryManager:Lorg/icepdf/core/util/MemoryManager;

    move-object/from16 v47, v0

    move-object/from16 v0, v30

    move-object/from16 v1, v47

    invoke-direct {v0, v10, v1}, Lorg/icepdf/core/io/ConservativeSizingByteArrayOutputStream;-><init>(ILorg/icepdf/core/util/MemoryManager;)V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_2
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    .line 292
    .end local v29    # "out":Lorg/icepdf/core/io/ConservativeSizingByteArrayOutputStream;
    .restart local v30    # "out":Lorg/icepdf/core/io/ConservativeSizingByteArrayOutputStream;
    :try_start_c
    move-object/from16 v0, p0

    move-object/from16 v1, v30

    invoke-direct {v0, v1}, Lorg/icepdf/core/util/Parser;->captureStreamData(Ljava/io/OutputStream;)J
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_6
    .catchall {:try_start_c .. :try_end_c} :catchall_2

    move-object/from16 v29, v30

    .end local v30    # "out":Lorg/icepdf/core/io/ConservativeSizingByteArrayOutputStream;
    .restart local v29    # "out":Lorg/icepdf/core/io/ConservativeSizingByteArrayOutputStream;
    goto :goto_b

    .line 311
    .end local v13    # "curChar":I
    :catchall_0
    move-exception v10

    :goto_c
    if-eqz v29, :cond_14

    .line 312
    :try_start_d
    invoke-virtual/range {v29 .. v29}, Lorg/icepdf/core/io/ConservativeSizingByteArrayOutputStream;->close()V

    :cond_14
    throw v10

    .line 324
    .restart local v4    # "streamInputWrapper":Lorg/icepdf/core/io/SeekableInputConstrainedWrapper;
    .restart local v5    # "streamDataInput":Lorg/icepdf/core/io/SeekableInput;
    .restart local v6    # "filePositionOfStreamData":J
    .restart local v8    # "lengthOfStreamData":J
    .restart local v13    # "curChar":I
    .restart local v33    # "stream":Lorg/icepdf/core/pobjects/Stream;
    .restart local v36    # "subtype":Lorg/icepdf/core/pobjects/Name;
    .restart local v40    # "trailer":Lorg/icepdf/core/pobjects/PTrailer;
    .restart local v43    # "type":Lorg/icepdf/core/pobjects/Name;
    :cond_15
    const-string/jumbo v10, "XRef"

    move-object/from16 v0, v43

    invoke-virtual {v0, v10}, Lorg/icepdf/core/pobjects/Name;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_17

    .line 325
    new-instance v33, Lorg/icepdf/core/pobjects/Stream;

    .end local v33    # "stream":Lorg/icepdf/core/pobjects/Stream;
    move-object/from16 v0, v33

    move-object/from16 v1, p1

    move-object/from16 v2, v34

    invoke-direct {v0, v1, v2, v4}, Lorg/icepdf/core/pobjects/Stream;-><init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;Lorg/icepdf/core/io/SeekableInputConstrainedWrapper;)V

    .line 326
    .restart local v33    # "stream":Lorg/icepdf/core/pobjects/Stream;
    invoke-virtual/range {v33 .. v33}, Lorg/icepdf/core/pobjects/Stream;->init()V

    .line 327
    invoke-virtual/range {v33 .. v33}, Lorg/icepdf/core/pobjects/Stream;->getInputStreamForDecodedStreamBytes()Ljava/io/InputStream;

    move-result-object v20

    .line 328
    .local v20, "in":Ljava/io/InputStream;
    new-instance v45, Lorg/icepdf/core/pobjects/CrossReference;

    invoke-direct/range {v45 .. v45}, Lorg/icepdf/core/pobjects/CrossReference;-><init>()V
    :try_end_d
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_0

    .line 329
    .local v45, "xrefStream":Lorg/icepdf/core/pobjects/CrossReference;
    if-eqz v20, :cond_16

    .line 331
    :try_start_e
    move-object/from16 v0, v45

    move-object/from16 v1, p1

    move-object/from16 v2, v34

    move-object/from16 v3, v20

    invoke-virtual {v0, v1, v2, v3}, Lorg/icepdf/core/pobjects/CrossReference;->addXRefStreamEntries(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;Ljava/io/InputStream;)V
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_1

    .line 335
    :try_start_f
    invoke-virtual/range {v20 .. v20}, Ljava/io/InputStream;->close()V
    :try_end_f
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_f} :catch_3
    .catch Ljava/lang/Exception; {:try_start_f .. :try_end_f} :catch_0

    .line 342
    :cond_16
    :goto_d
    const/4 v10, 0x0

    :try_start_10
    move-object/from16 v0, v33

    invoke-virtual {v0, v10}, Lorg/icepdf/core/pobjects/Stream;->dispose(Z)V

    .line 346
    invoke-virtual/range {v34 .. v34}, Ljava/util/Hashtable;->clone()Ljava/lang/Object;

    move-result-object v42

    check-cast v42, Ljava/util/Hashtable;

    .line 347
    .local v42, "trailerHash":Ljava/util/Hashtable;
    new-instance v40, Lorg/icepdf/core/pobjects/PTrailer;

    .end local v40    # "trailer":Lorg/icepdf/core/pobjects/PTrailer;
    const/4 v10, 0x0

    move-object/from16 v0, v40

    move-object/from16 v1, p1

    move-object/from16 v2, v42

    move-object/from16 v3, v45

    invoke-direct {v0, v1, v2, v10, v3}, Lorg/icepdf/core/pobjects/PTrailer;-><init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;Lorg/icepdf/core/pobjects/CrossReference;Lorg/icepdf/core/pobjects/CrossReference;)V

    .line 348
    .restart local v40    # "trailer":Lorg/icepdf/core/pobjects/PTrailer;
    goto/16 :goto_7

    .line 337
    .end local v42    # "trailerHash":Ljava/util/Hashtable;
    :catch_3
    move-exception v16

    .line 338
    .restart local v16    # "e":Ljava/io/IOException;
    sget-object v10, Lorg/icepdf/core/util/Parser;->logger:Ljava/util/logging/Logger;

    sget-object v47, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string/jumbo v48, "Error appending stream entries."

    move-object/from16 v0, v47

    move-object/from16 v1, v48

    move-object/from16 v2, v16

    invoke-virtual {v10, v0, v1, v2}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_10
    .catch Ljava/lang/Exception; {:try_start_10 .. :try_end_10} :catch_0

    goto :goto_d

    .line 334
    .end local v16    # "e":Ljava/io/IOException;
    :catchall_1
    move-exception v10

    .line 335
    :try_start_11
    invoke-virtual/range {v20 .. v20}, Ljava/io/InputStream;->close()V
    :try_end_11
    .catch Ljava/io/IOException; {:try_start_11 .. :try_end_11} :catch_4
    .catch Ljava/lang/Exception; {:try_start_11 .. :try_end_11} :catch_0

    .line 339
    :goto_e
    :try_start_12
    throw v10

    .line 337
    :catch_4
    move-exception v16

    .line 338
    .restart local v16    # "e":Ljava/io/IOException;
    sget-object v47, Lorg/icepdf/core/util/Parser;->logger:Ljava/util/logging/Logger;

    sget-object v48, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string/jumbo v49, "Error appending stream entries."

    move-object/from16 v0, v47

    move-object/from16 v1, v48

    move-object/from16 v2, v49

    move-object/from16 v3, v16

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_e

    .line 348
    .end local v16    # "e":Ljava/io/IOException;
    .end local v20    # "in":Ljava/io/InputStream;
    .end local v45    # "xrefStream":Lorg/icepdf/core/pobjects/CrossReference;
    :cond_17
    const-string/jumbo v10, "ObjStm"

    move-object/from16 v0, v43

    invoke-virtual {v0, v10}, Lorg/icepdf/core/pobjects/Name;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_b

    .line 349
    new-instance v33, Lorg/icepdf/core/pobjects/ObjectStream;

    .end local v33    # "stream":Lorg/icepdf/core/pobjects/Stream;
    move-object/from16 v0, v33

    move-object/from16 v1, p1

    move-object/from16 v2, v34

    invoke-direct {v0, v1, v2, v4}, Lorg/icepdf/core/pobjects/ObjectStream;-><init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;Lorg/icepdf/core/io/SeekableInputConstrainedWrapper;)V

    .restart local v33    # "stream":Lorg/icepdf/core/pobjects/Stream;
    goto/16 :goto_7

    .line 363
    :cond_18
    if-nez v33, :cond_19

    .line 364
    new-instance v33, Lorg/icepdf/core/pobjects/Stream;

    .end local v33    # "stream":Lorg/icepdf/core/pobjects/Stream;
    move-object/from16 v0, v33

    move-object/from16 v1, p1

    move-object/from16 v2, v34

    invoke-direct {v0, v1, v2, v4}, Lorg/icepdf/core/pobjects/Stream;-><init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;Lorg/icepdf/core/io/SeekableInputConstrainedWrapper;)V

    .line 366
    .restart local v33    # "stream":Lorg/icepdf/core/pobjects/Stream;
    :cond_19
    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/icepdf/core/util/Parser;->stack:Ljava/util/Stack;

    move-object/from16 v0, v33

    invoke-virtual {v10, v0}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_8

    .line 372
    .end local v4    # "streamInputWrapper":Lorg/icepdf/core/io/SeekableInputConstrainedWrapper;
    .end local v5    # "streamDataInput":Lorg/icepdf/core/io/SeekableInput;
    .end local v6    # "filePositionOfStreamData":J
    .end local v8    # "lengthOfStreamData":J
    .end local v13    # "curChar":I
    .end local v29    # "out":Lorg/icepdf/core/io/ConservativeSizingByteArrayOutputStream;
    .end local v33    # "stream":Lorg/icepdf/core/pobjects/Stream;
    .end local v34    # "streamHash":Ljava/util/Hashtable;
    .end local v35    # "streamLength":I
    .end local v36    # "subtype":Lorg/icepdf/core/pobjects/Name;
    .end local v40    # "trailer":Lorg/icepdf/core/pobjects/PTrailer;
    .end local v43    # "type":Lorg/icepdf/core/pobjects/Name;
    :cond_1a
    const-string/jumbo v10, "true"

    move-object/from16 v0, v24

    invoke-virtual {v0, v10}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1b

    .line 373
    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/icepdf/core/util/Parser;->stack:Ljava/util/Stack;

    const/16 v47, 0x1

    invoke-static/range {v47 .. v47}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v47

    move-object/from16 v0, v47

    invoke-virtual {v10, v0}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    move-object/from16 v27, v28

    .end local v28    # "objectReference":Lorg/icepdf/core/pobjects/Reference;
    .restart local v27    # "objectReference":Lorg/icepdf/core/pobjects/Reference;
    goto/16 :goto_3

    .line 374
    .end local v27    # "objectReference":Lorg/icepdf/core/pobjects/Reference;
    .restart local v28    # "objectReference":Lorg/icepdf/core/pobjects/Reference;
    :cond_1b
    const-string/jumbo v10, "false"

    move-object/from16 v0, v24

    invoke-virtual {v0, v10}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1c

    .line 375
    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/icepdf/core/util/Parser;->stack:Ljava/util/Stack;

    const/16 v47, 0x0

    invoke-static/range {v47 .. v47}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v47

    move-object/from16 v0, v47

    invoke-virtual {v10, v0}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    move-object/from16 v27, v28

    .end local v28    # "objectReference":Lorg/icepdf/core/pobjects/Reference;
    .restart local v27    # "objectReference":Lorg/icepdf/core/pobjects/Reference;
    goto/16 :goto_3

    .line 378
    .end local v27    # "objectReference":Lorg/icepdf/core/pobjects/Reference;
    .restart local v28    # "objectReference":Lorg/icepdf/core/pobjects/Reference;
    :cond_1c
    const-string/jumbo v10, "R"

    move-object/from16 v0, v24

    invoke-virtual {v0, v10}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1d

    .line 380
    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/icepdf/core/util/Parser;->stack:Ljava/util/Stack;

    invoke-virtual {v10}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Number;

    move-object v0, v10

    check-cast v0, Ljava/lang/Number;

    move-object/from16 v17, v0

    .line 381
    .restart local v17    # "generationNumber":Ljava/lang/Number;
    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/icepdf/core/util/Parser;->stack:Ljava/util/Stack;

    invoke-virtual {v10}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Number;

    move-object v0, v10

    check-cast v0, Ljava/lang/Number;

    move-object/from16 v26, v0

    .line 382
    .restart local v26    # "objectNumber":Ljava/lang/Number;
    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/icepdf/core/util/Parser;->stack:Ljava/util/Stack;

    new-instance v47, Lorg/icepdf/core/pobjects/Reference;

    move-object/from16 v0, v47

    move-object/from16 v1, v26

    move-object/from16 v2, v17

    invoke-direct {v0, v1, v2}, Lorg/icepdf/core/pobjects/Reference;-><init>(Ljava/lang/Number;Ljava/lang/Number;)V

    move-object/from16 v0, v47

    invoke-virtual {v10, v0}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    move-object/from16 v27, v28

    .line 384
    .end local v28    # "objectReference":Lorg/icepdf/core/pobjects/Reference;
    .restart local v27    # "objectReference":Lorg/icepdf/core/pobjects/Reference;
    goto/16 :goto_3

    .end local v17    # "generationNumber":Ljava/lang/Number;
    .end local v26    # "objectNumber":Ljava/lang/Number;
    .end local v27    # "objectReference":Lorg/icepdf/core/pobjects/Reference;
    .restart local v28    # "objectReference":Lorg/icepdf/core/pobjects/Reference;
    :cond_1d
    const-string/jumbo v10, "["

    move-object/from16 v0, v24

    invoke-virtual {v0, v10}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1e

    .line 385
    add-int/lit8 v15, v15, 0x1

    .line 386
    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/icepdf/core/util/Parser;->stack:Ljava/util/Stack;

    move-object/from16 v0, v24

    invoke-virtual {v10, v0}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    move-object/from16 v27, v28

    .end local v28    # "objectReference":Lorg/icepdf/core/pobjects/Reference;
    .restart local v27    # "objectReference":Lorg/icepdf/core/pobjects/Reference;
    goto/16 :goto_3

    .line 389
    .end local v27    # "objectReference":Lorg/icepdf/core/pobjects/Reference;
    .restart local v28    # "objectReference":Lorg/icepdf/core/pobjects/Reference;
    :cond_1e
    const-string/jumbo v10, "]"

    move-object/from16 v0, v24

    invoke-virtual {v0, v10}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_23

    .line 390
    add-int/lit8 v15, v15, -0x1

    .line 391
    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/icepdf/core/util/Parser;->stack:Ljava/util/Stack;

    const-string/jumbo v47, "["

    move-object/from16 v0, v47

    invoke-virtual {v10, v0}, Ljava/util/Stack;->search(Ljava/lang/Object;)I

    move-result v31

    .line 392
    .local v31, "searchPosition":I
    add-int/lit8 v32, v31, -0x1

    .line 393
    .restart local v32    # "size":I
    new-instance v44, Ljava/util/Vector;

    if-lez v32, :cond_20

    move/from16 v10, v32

    :goto_f
    move-object/from16 v0, v44

    invoke-direct {v0, v10}, Ljava/util/Vector;-><init>(I)V

    .line 394
    .local v44, "v":Ljava/util/Vector;
    if-lez v32, :cond_1f

    .line 395
    move-object/from16 v0, v44

    move/from16 v1, v32

    invoke-virtual {v0, v1}, Ljava/util/Vector;->setSize(I)V

    .line 396
    :cond_1f
    if-lez v31, :cond_22

    .line 397
    add-int/lit8 v19, v32, -0x1

    .local v19, "i":I
    :goto_10
    if-ltz v19, :cond_21

    .line 398
    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/icepdf/core/util/Parser;->stack:Ljava/util/Stack;

    invoke-virtual {v10}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v25

    .line 399
    .local v25, "obj":Ljava/lang/Object;
    move-object/from16 v0, v44

    move/from16 v1, v19

    move-object/from16 v2, v25

    invoke-virtual {v0, v1, v2}, Ljava/util/Vector;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 397
    add-int/lit8 v19, v19, -0x1

    goto :goto_10

    .line 393
    .end local v19    # "i":I
    .end local v25    # "obj":Ljava/lang/Object;
    .end local v44    # "v":Ljava/util/Vector;
    :cond_20
    const/4 v10, 0x1

    goto :goto_f

    .line 401
    .restart local v19    # "i":I
    .restart local v44    # "v":Ljava/util/Vector;
    :cond_21
    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/icepdf/core/util/Parser;->stack:Ljava/util/Stack;

    invoke-virtual {v10}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    .line 406
    .end local v19    # "i":I
    :goto_11
    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/icepdf/core/util/Parser;->stack:Ljava/util/Stack;

    move-object/from16 v0, v44

    invoke-virtual {v10, v0}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    move-object/from16 v27, v28

    .line 407
    .end local v28    # "objectReference":Lorg/icepdf/core/pobjects/Reference;
    .restart local v27    # "objectReference":Lorg/icepdf/core/pobjects/Reference;
    goto/16 :goto_3

    .line 404
    .end local v27    # "objectReference":Lorg/icepdf/core/pobjects/Reference;
    .restart local v28    # "objectReference":Lorg/icepdf/core/pobjects/Reference;
    :cond_22
    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/icepdf/core/util/Parser;->stack:Ljava/util/Stack;

    invoke-virtual {v10}, Ljava/util/Stack;->clear()V

    goto :goto_11

    .line 407
    .end local v31    # "searchPosition":I
    .end local v32    # "size":I
    .end local v44    # "v":Ljava/util/Vector;
    :cond_23
    const-string/jumbo v10, "<<"

    move-object/from16 v0, v24

    invoke-virtual {v0, v10}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_24

    .line 409
    add-int/lit8 v15, v15, 0x1

    .line 410
    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/icepdf/core/util/Parser;->stack:Ljava/util/Stack;

    move-object/from16 v0, v24

    invoke-virtual {v10, v0}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    move-object/from16 v27, v28

    .end local v28    # "objectReference":Lorg/icepdf/core/pobjects/Reference;
    .restart local v27    # "objectReference":Lorg/icepdf/core/pobjects/Reference;
    goto/16 :goto_3

    .line 413
    .end local v27    # "objectReference":Lorg/icepdf/core/pobjects/Reference;
    .restart local v28    # "objectReference":Lorg/icepdf/core/pobjects/Reference;
    :cond_24
    const-string/jumbo v10, ">>"

    move-object/from16 v0, v24

    invoke-virtual {v0, v10}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2f

    .line 415
    add-int/lit8 v15, v15, -0x1

    .line 416
    new-instance v18, Ljava/util/Hashtable;

    invoke-direct/range {v18 .. v18}, Ljava/util/Hashtable;-><init>()V

    .line 418
    .local v18, "hashTable":Ljava/util/Hashtable;
    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/icepdf/core/util/Parser;->stack:Ljava/util/Stack;

    invoke-virtual {v10}, Ljava/util/Stack;->isEmpty()Z

    move-result v10

    if-nez v10, :cond_2e

    .line 419
    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/icepdf/core/util/Parser;->stack:Ljava/util/Stack;

    invoke-virtual {v10}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v25

    .line 423
    .restart local v25    # "obj":Ljava/lang/Object;
    :goto_12
    move-object/from16 v0, v25

    instance-of v10, v0, Ljava/lang/String;

    if-eqz v10, :cond_25

    const-string/jumbo v10, "<<"

    move-object/from16 v0, v25

    invoke-virtual {v0, v10}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_26

    :cond_25
    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/icepdf/core/util/Parser;->stack:Ljava/util/Stack;

    invoke-virtual {v10}, Ljava/util/Stack;->isEmpty()Z

    move-result v10

    if-nez v10, :cond_26

    .line 424
    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/icepdf/core/util/Parser;->stack:Ljava/util/Stack;

    invoke-virtual {v10}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v22

    .line 427
    .local v22, "key":Ljava/lang/Object;
    move-object/from16 v0, v18

    move-object/from16 v1, v22

    move-object/from16 v2, v25

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 428
    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/icepdf/core/util/Parser;->stack:Ljava/util/Stack;

    invoke-virtual {v10}, Ljava/util/Stack;->isEmpty()Z

    move-result v10

    if-nez v10, :cond_26

    .line 429
    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/icepdf/core/util/Parser;->stack:Ljava/util/Stack;

    invoke-virtual {v10}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v25

    .line 433
    goto :goto_12

    .line 435
    .end local v22    # "key":Ljava/lang/Object;
    :cond_26
    const-string/jumbo v10, "Type"

    move-object/from16 v0, v18

    invoke-virtual {v0, v10}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v25

    .line 437
    if-nez v25, :cond_27

    .line 439
    new-instance v10, Lorg/icepdf/core/pobjects/Name;

    const-string/jumbo v47, "Type"

    move-object/from16 v0, v47

    invoke-direct {v10, v0}, Lorg/icepdf/core/pobjects/Name;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v18

    invoke-virtual {v0, v10}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v25

    .line 444
    :cond_27
    if-eqz v25, :cond_2d

    move-object/from16 v0, v25

    instance-of v10, v0, Lorg/icepdf/core/pobjects/Name;

    if-eqz v10, :cond_2d

    .line 445
    move-object/from16 v0, v25

    check-cast v0, Lorg/icepdf/core/pobjects/Name;

    move-object/from16 v23, v0

    .line 447
    .local v23, "n":Lorg/icepdf/core/pobjects/Name;
    const-string/jumbo v10, "Catalog"

    move-object/from16 v0, v23

    invoke-virtual {v0, v10}, Lorg/icepdf/core/pobjects/Name;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_28

    .line 448
    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/icepdf/core/util/Parser;->stack:Ljava/util/Stack;

    new-instance v47, Lorg/icepdf/core/pobjects/Catalog;

    move-object/from16 v0, v47

    move-object/from16 v1, p1

    move-object/from16 v2, v18

    invoke-direct {v0, v1, v2}, Lorg/icepdf/core/pobjects/Catalog;-><init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V

    move-object/from16 v0, v47

    invoke-virtual {v10, v0}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 476
    .end local v23    # "n":Lorg/icepdf/core/pobjects/Name;
    :goto_13
    if-nez v15, :cond_2e

    .line 477
    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/icepdf/core/util/Parser;->stack:Ljava/util/Stack;

    invoke-virtual {v10}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v10

    move-object/from16 v27, v28

    .end local v28    # "objectReference":Lorg/icepdf/core/pobjects/Reference;
    .restart local v27    # "objectReference":Lorg/icepdf/core/pobjects/Reference;
    goto/16 :goto_2

    .line 449
    .end local v27    # "objectReference":Lorg/icepdf/core/pobjects/Reference;
    .restart local v23    # "n":Lorg/icepdf/core/pobjects/Name;
    .restart local v28    # "objectReference":Lorg/icepdf/core/pobjects/Reference;
    :cond_28
    const-string/jumbo v10, "Pages"

    move-object/from16 v0, v23

    invoke-virtual {v0, v10}, Lorg/icepdf/core/pobjects/Name;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_29

    .line 450
    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/icepdf/core/util/Parser;->stack:Ljava/util/Stack;

    new-instance v47, Lorg/icepdf/core/pobjects/PageTree;

    move-object/from16 v0, v47

    move-object/from16 v1, p1

    move-object/from16 v2, v18

    invoke-direct {v0, v1, v2}, Lorg/icepdf/core/pobjects/PageTree;-><init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V

    move-object/from16 v0, v47

    invoke-virtual {v10, v0}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_13

    .line 451
    :cond_29
    const-string/jumbo v10, "Page"

    move-object/from16 v0, v23

    invoke-virtual {v0, v10}, Lorg/icepdf/core/pobjects/Name;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2a

    .line 452
    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/icepdf/core/util/Parser;->stack:Ljava/util/Stack;

    new-instance v47, Lorg/icepdf/core/pobjects/Page;

    move-object/from16 v0, v47

    move-object/from16 v1, p1

    move-object/from16 v2, v18

    invoke-direct {v0, v1, v2}, Lorg/icepdf/core/pobjects/Page;-><init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V

    move-object/from16 v0, v47

    invoke-virtual {v10, v0}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_13

    .line 453
    :cond_2a
    const-string/jumbo v10, "Font"

    move-object/from16 v0, v23

    invoke-virtual {v0, v10}, Lorg/icepdf/core/pobjects/Name;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2b

    .line 454
    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/icepdf/core/util/Parser;->stack:Ljava/util/Stack;

    invoke-static {}, Lorg/icepdf/core/pobjects/fonts/FontFactory;->getInstance()Lorg/icepdf/core/pobjects/fonts/FontFactory;

    move-result-object v47

    move-object/from16 v0, v47

    move-object/from16 v1, p1

    move-object/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Lorg/icepdf/core/pobjects/fonts/FontFactory;->getFont(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)Lorg/icepdf/core/pobjects/fonts/Font;

    move-result-object v47

    move-object/from16 v0, v47

    invoke-virtual {v10, v0}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_13

    .line 456
    :cond_2b
    const-string/jumbo v10, "FontDescriptor"

    move-object/from16 v0, v23

    invoke-virtual {v0, v10}, Lorg/icepdf/core/pobjects/Name;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2c

    .line 457
    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/icepdf/core/util/Parser;->stack:Ljava/util/Stack;

    new-instance v47, Lorg/icepdf/core/pobjects/fonts/FontDescriptor;

    move-object/from16 v0, v47

    move-object/from16 v1, p1

    move-object/from16 v2, v18

    invoke-direct {v0, v1, v2}, Lorg/icepdf/core/pobjects/fonts/FontDescriptor;-><init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V

    move-object/from16 v0, v47

    invoke-virtual {v10, v0}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_13

    .line 467
    :cond_2c
    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/icepdf/core/util/Parser;->stack:Ljava/util/Stack;

    move-object/from16 v0, v18

    invoke-virtual {v10, v0}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_13

    .line 472
    .end local v23    # "n":Lorg/icepdf/core/pobjects/Name;
    :cond_2d
    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/icepdf/core/util/Parser;->stack:Ljava/util/Stack;

    move-object/from16 v0, v18

    invoke-virtual {v10, v0}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_13

    .end local v25    # "obj":Ljava/lang/Object;
    :cond_2e
    move-object/from16 v27, v28

    .line 479
    .end local v28    # "objectReference":Lorg/icepdf/core/pobjects/Reference;
    .restart local v27    # "objectReference":Lorg/icepdf/core/pobjects/Reference;
    goto/16 :goto_3

    .line 511
    .end local v18    # "hashTable":Ljava/util/Hashtable;
    .end local v27    # "objectReference":Lorg/icepdf/core/pobjects/Reference;
    .restart local v28    # "objectReference":Lorg/icepdf/core/pobjects/Reference;
    :cond_2f
    const-string/jumbo v10, "xref"

    move-object/from16 v0, v24

    invoke-virtual {v0, v10}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_30

    .line 513
    new-instance v46, Lorg/icepdf/core/pobjects/CrossReference;

    invoke-direct/range {v46 .. v46}, Lorg/icepdf/core/pobjects/CrossReference;-><init>()V

    .line 514
    .local v46, "xrefTable":Lorg/icepdf/core/pobjects/CrossReference;
    move-object/from16 v0, v46

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Lorg/icepdf/core/pobjects/CrossReference;->addXRefTableEntries(Lorg/icepdf/core/util/Parser;)V

    .line 515
    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/icepdf/core/util/Parser;->stack:Ljava/util/Stack;

    move-object/from16 v0, v46

    invoke-virtual {v10, v0}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    move-object/from16 v27, v28

    .line 516
    .end local v28    # "objectReference":Lorg/icepdf/core/pobjects/Reference;
    .restart local v27    # "objectReference":Lorg/icepdf/core/pobjects/Reference;
    goto/16 :goto_3

    .end local v27    # "objectReference":Lorg/icepdf/core/pobjects/Reference;
    .end local v46    # "xrefTable":Lorg/icepdf/core/pobjects/CrossReference;
    .restart local v28    # "objectReference":Lorg/icepdf/core/pobjects/Reference;
    :cond_30
    const-string/jumbo v10, "trailer"

    move-object/from16 v0, v24

    invoke-virtual {v0, v10}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_32

    .line 517
    const/16 v46, 0x0

    .line 518
    .restart local v46    # "xrefTable":Lorg/icepdf/core/pobjects/CrossReference;
    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/icepdf/core/util/Parser;->stack:Ljava/util/Stack;

    invoke-virtual {v10}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v10

    instance-of v10, v10, Lorg/icepdf/core/pobjects/CrossReference;

    if-eqz v10, :cond_31

    .line 519
    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/icepdf/core/util/Parser;->stack:Ljava/util/Stack;

    invoke-virtual {v10}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v46

    .end local v46    # "xrefTable":Lorg/icepdf/core/pobjects/CrossReference;
    check-cast v46, Lorg/icepdf/core/pobjects/CrossReference;

    .line 520
    .restart local v46    # "xrefTable":Lorg/icepdf/core/pobjects/CrossReference;
    :cond_31
    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/icepdf/core/util/Parser;->stack:Ljava/util/Stack;

    invoke-virtual {v10}, Ljava/util/Stack;->clear()V

    .line 521
    invoke-virtual/range {p0 .. p1}, Lorg/icepdf/core/util/Parser;->getObject(Lorg/icepdf/core/util/Library;)Ljava/lang/Object;

    move-result-object v41

    check-cast v41, Ljava/util/Hashtable;

    .line 525
    .local v41, "trailerDictionary":Ljava/util/Hashtable;
    new-instance v10, Lorg/icepdf/core/pobjects/PTrailer;

    const/16 v47, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v41

    move-object/from16 v2, v46

    move-object/from16 v3, v47

    invoke-direct {v10, v0, v1, v2, v3}, Lorg/icepdf/core/pobjects/PTrailer;-><init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;Lorg/icepdf/core/pobjects/CrossReference;Lorg/icepdf/core/pobjects/CrossReference;)V

    move-object/from16 v27, v28

    .end local v28    # "objectReference":Lorg/icepdf/core/pobjects/Reference;
    .restart local v27    # "objectReference":Lorg/icepdf/core/pobjects/Reference;
    goto/16 :goto_2

    .line 528
    .end local v27    # "objectReference":Lorg/icepdf/core/pobjects/Reference;
    .end local v41    # "trailerDictionary":Ljava/util/Hashtable;
    .end local v46    # "xrefTable":Lorg/icepdf/core/pobjects/CrossReference;
    .restart local v28    # "objectReference":Lorg/icepdf/core/pobjects/Reference;
    :cond_32
    move-object/from16 v0, v24

    instance-of v10, v0, Ljava/lang/String;

    if-eqz v10, :cond_33

    move-object/from16 v0, v24

    check-cast v0, Ljava/lang/String;

    move-object v10, v0

    const-string/jumbo v47, "%"

    move-object/from16 v0, v47

    invoke-virtual {v10, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_33

    move-object/from16 v27, v28

    .end local v28    # "objectReference":Lorg/icepdf/core/pobjects/Reference;
    .restart local v27    # "objectReference":Lorg/icepdf/core/pobjects/Reference;
    goto/16 :goto_3

    .line 534
    .end local v27    # "objectReference":Lorg/icepdf/core/pobjects/Reference;
    .restart local v28    # "objectReference":Lorg/icepdf/core/pobjects/Reference;
    :cond_33
    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/icepdf/core/util/Parser;->stack:Ljava/util/Stack;

    move-object/from16 v0, v24

    invoke-virtual {v10, v0}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 538
    add-int/lit8 v38, v38, 0x1

    .line 539
    const/16 v10, 0x3e8

    move/from16 v0, v38

    if-le v0, v10, :cond_36

    .line 540
    new-instance v10, Lorg/icepdf/core/exceptions/PDFException;

    const-string/jumbo v47, "Not a valid PDF file"

    move-object/from16 v0, v47

    invoke-direct {v10, v0}, Lorg/icepdf/core/exceptions/PDFException;-><init>(Ljava/lang/String;)V

    throw v10
    :try_end_12
    .catch Ljava/lang/Exception; {:try_start_12 .. :try_end_12} :catch_0

    .line 546
    .end local v28    # "objectReference":Lorg/icepdf/core/pobjects/Reference;
    .restart local v27    # "objectReference":Lorg/icepdf/core/pobjects/Reference;
    :cond_34
    if-eqz v12, :cond_35

    .line 556
    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/icepdf/core/util/Parser;->stack:Ljava/util/Stack;

    invoke-virtual {v10}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v10

    goto/16 :goto_2

    .line 551
    :catch_5
    move-exception v16

    goto/16 :goto_1

    .line 311
    .end local v27    # "objectReference":Lorg/icepdf/core/pobjects/Reference;
    .restart local v13    # "curChar":I
    .restart local v28    # "objectReference":Lorg/icepdf/core/pobjects/Reference;
    .restart local v30    # "out":Lorg/icepdf/core/io/ConservativeSizingByteArrayOutputStream;
    .restart local v34    # "streamHash":Ljava/util/Hashtable;
    .restart local v35    # "streamLength":I
    :catchall_2
    move-exception v10

    move-object/from16 v29, v30

    .end local v30    # "out":Lorg/icepdf/core/io/ConservativeSizingByteArrayOutputStream;
    .restart local v29    # "out":Lorg/icepdf/core/io/ConservativeSizingByteArrayOutputStream;
    goto/16 :goto_c

    .line 307
    .end local v29    # "out":Lorg/icepdf/core/io/ConservativeSizingByteArrayOutputStream;
    .restart local v30    # "out":Lorg/icepdf/core/io/ConservativeSizingByteArrayOutputStream;
    :catch_6
    move-exception v16

    move-object/from16 v29, v30

    .end local v30    # "out":Lorg/icepdf/core/io/ConservativeSizingByteArrayOutputStream;
    .restart local v29    # "out":Lorg/icepdf/core/io/ConservativeSizingByteArrayOutputStream;
    goto/16 :goto_9

    .end local v13    # "curChar":I
    .end local v28    # "objectReference":Lorg/icepdf/core/pobjects/Reference;
    .end local v29    # "out":Lorg/icepdf/core/io/ConservativeSizingByteArrayOutputStream;
    .end local v34    # "streamHash":Ljava/util/Hashtable;
    .end local v35    # "streamLength":I
    .restart local v27    # "objectReference":Lorg/icepdf/core/pobjects/Reference;
    :cond_35
    move-object/from16 v28, v27

    .end local v27    # "objectReference":Lorg/icepdf/core/pobjects/Reference;
    .restart local v28    # "objectReference":Lorg/icepdf/core/pobjects/Reference;
    goto/16 :goto_0

    :cond_36
    move-object/from16 v27, v28

    .end local v28    # "objectReference":Lorg/icepdf/core/pobjects/Reference;
    .restart local v27    # "objectReference":Lorg/icepdf/core/pobjects/Reference;
    goto/16 :goto_3
.end method

.method public getStreamObject()Ljava/lang/Object;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 600
    invoke-virtual {p0}, Lorg/icepdf/core/util/Parser;->getToken()Ljava/lang/Object;

    move-result-object v1

    .line 601
    .local v1, "o":Ljava/lang/Object;
    instance-of v4, v1, Ljava/lang/String;

    if-eqz v4, :cond_1

    .line 602
    const-string/jumbo v4, "<<"

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 603
    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    .line 604
    .local v0, "h":Ljava/util/Hashtable;
    invoke-virtual {p0}, Lorg/icepdf/core/util/Parser;->getStreamObject()Ljava/lang/Object;

    move-result-object v2

    .line 605
    .local v2, "o1":Ljava/lang/Object;
    :goto_0
    const-string/jumbo v4, ">>"

    invoke-virtual {v2, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 606
    invoke-virtual {p0}, Lorg/icepdf/core/util/Parser;->getStreamObject()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v0, v2, v4}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 607
    invoke-virtual {p0}, Lorg/icepdf/core/util/Parser;->getStreamObject()Ljava/lang/Object;

    move-result-object v2

    goto :goto_0

    .line 609
    :cond_0
    move-object v1, v0

    .line 625
    .end local v0    # "h":Ljava/util/Hashtable;
    .end local v1    # "o":Ljava/lang/Object;
    .end local v2    # "o1":Ljava/lang/Object;
    :cond_1
    :goto_1
    return-object v1

    .line 613
    .restart local v1    # "o":Ljava/lang/Object;
    :cond_2
    const-string/jumbo v4, "["

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 614
    new-instance v3, Ljava/util/Vector;

    invoke-direct {v3}, Ljava/util/Vector;-><init>()V

    .line 615
    .local v3, "v":Ljava/util/Vector;
    invoke-virtual {p0}, Lorg/icepdf/core/util/Parser;->getStreamObject()Ljava/lang/Object;

    move-result-object v2

    .line 616
    .restart local v2    # "o1":Ljava/lang/Object;
    :goto_2
    const-string/jumbo v4, "]"

    invoke-virtual {v2, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 617
    invoke-virtual {v3, v2}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    .line 618
    invoke-virtual {p0}, Lorg/icepdf/core/util/Parser;->getStreamObject()Ljava/lang/Object;

    move-result-object v2

    goto :goto_2

    .line 620
    :cond_3
    invoke-virtual {v3}, Ljava/util/Vector;->trimToSize()V

    .line 621
    move-object v1, v3

    .local v1, "o":Ljava/util/Vector;
    goto :goto_1
.end method

.method public getToken()Ljava/lang/Object;
    .locals 22
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 640
    const/4 v15, 0x0

    .line 641
    .local v15, "inString":Z
    const/4 v12, 0x0

    .line 642
    .local v12, "hexString":Z
    const/16 v19, 0x0

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lorg/icepdf/core/util/Parser;->lastTokenHString:Z

    .line 646
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/core/util/Parser;->reader:Ljava/io/InputStream;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Ljava/io/InputStream;->read()I

    move-result v6

    .line 648
    .local v6, "currentByte":I
    if-gez v6, :cond_1

    .line 649
    new-instance v19, Ljava/io/IOException;

    invoke-direct/range {v19 .. v19}, Ljava/io/IOException;-><init>()V

    throw v19

    .line 651
    :cond_1
    int-to-char v7, v6

    .line 653
    .local v7, "currentChar":C
    invoke-static {v7}, Lorg/icepdf/core/util/Parser;->isWhitespace(C)Z

    move-result v19

    if-nez v19, :cond_0

    .line 661
    const/16 v19, 0x28

    move/from16 v0, v19

    if-ne v7, v0, :cond_3

    .line 663
    const/4 v15, 0x1

    .line 691
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/core/util/Parser;->reader:Ljava/io/InputStream;

    move-object/from16 v19, v0

    const/16 v20, 0x1

    invoke-virtual/range {v19 .. v20}, Ljava/io/InputStream;->mark(I)V

    .line 694
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/core/util/Parser;->reader:Ljava/io/InputStream;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Ljava/io/InputStream;->read()I

    move-result v19

    move/from16 v0, v19

    int-to-char v0, v0

    move/from16 v16, v0

    .line 697
    .local v16, "nextChar":C
    const/16 v19, 0x3e

    move/from16 v0, v19

    if-ne v7, v0, :cond_a

    const/16 v19, 0x3e

    move/from16 v0, v16

    move/from16 v1, v19

    if-ne v0, v1, :cond_a

    .line 698
    const-string/jumbo v19, ">>"

    .line 972
    .end local v16    # "nextChar":C
    :goto_0
    return-object v19

    .line 664
    :cond_3
    const/16 v19, 0x5d

    move/from16 v0, v19

    if-ne v7, v0, :cond_4

    .line 666
    const-string/jumbo v19, "]"

    goto :goto_0

    .line 667
    :cond_4
    const/16 v19, 0x5b

    move/from16 v0, v19

    if-ne v7, v0, :cond_5

    .line 669
    const-string/jumbo v19, "["

    goto :goto_0

    .line 670
    :cond_5
    const/16 v19, 0x25

    move/from16 v0, v19

    if-ne v7, v0, :cond_2

    .line 673
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    .line 675
    .local v18, "stringBuffer":Ljava/lang/StringBuilder;
    :cond_6
    move-object/from16 v0, v18

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 676
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/core/util/Parser;->reader:Ljava/io/InputStream;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Ljava/io/InputStream;->read()I

    move-result v6

    .line 677
    if-gez v6, :cond_8

    .line 679
    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->length()I

    move-result v19

    if-lez v19, :cond_7

    .line 680
    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    goto :goto_0

    .line 681
    :cond_7
    new-instance v19, Ljava/io/IOException;

    invoke-direct/range {v19 .. v19}, Ljava/io/IOException;-><init>()V

    throw v19

    .line 683
    :cond_8
    int-to-char v7, v6

    .line 685
    const/16 v19, 0xd

    move/from16 v0, v19

    if-eq v7, v0, :cond_9

    const/16 v19, 0xa

    move/from16 v0, v19

    if-ne v7, v0, :cond_6

    .line 687
    :cond_9
    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    goto :goto_0

    .line 700
    .end local v18    # "stringBuffer":Ljava/lang/StringBuilder;
    .restart local v16    # "nextChar":C
    :cond_a
    const/16 v19, 0x3c

    move/from16 v0, v19

    if-ne v7, v0, :cond_c

    .line 702
    const/16 v19, 0x3c

    move/from16 v0, v16

    move/from16 v1, v19

    if-ne v0, v1, :cond_b

    .line 703
    const-string/jumbo v19, "<<"

    goto :goto_0

    .line 707
    :cond_b
    const/4 v15, 0x1

    .line 708
    const/4 v12, 0x1

    .line 713
    :cond_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/core/util/Parser;->reader:Ljava/io/InputStream;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Ljava/io/InputStream;->reset()V

    .line 716
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    .line 718
    .restart local v18    # "stringBuffer":Ljava/lang/StringBuilder;
    if-nez v15, :cond_f

    .line 719
    const/16 v19, 0x80

    move/from16 v0, v19

    if-ge v7, v0, :cond_d

    .line 720
    move-object/from16 v0, v18

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 723
    :cond_d
    const/16 v19, 0x51

    move/from16 v0, v19

    if-eq v7, v0, :cond_e

    const/16 v19, 0x71

    move/from16 v0, v19

    if-ne v7, v0, :cond_10

    .line 724
    :cond_e
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/core/util/Parser;->reader:Ljava/io/InputStream;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Ljava/io/InputStream;->read()I

    .line 725
    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    goto/16 :goto_0

    .line 728
    :cond_f
    move-object/from16 v0, v18

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 736
    :cond_10
    const/16 v17, 0x0

    .line 737
    .local v17, "parenthesisCount":I
    const/4 v4, 0x0

    .line 740
    .local v4, "complete":Z
    const/4 v14, 0x0

    .line 745
    .local v14, "ignoreChar":Z
    :cond_11
    if-nez v15, :cond_12

    .line 746
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/core/util/Parser;->reader:Ljava/io/InputStream;

    move-object/from16 v19, v0

    const/16 v20, 0x1

    invoke-virtual/range {v19 .. v20}, Ljava/io/InputStream;->mark(I)V

    .line 753
    :cond_12
    if-nez v15, :cond_14

    if-nez v12, :cond_14

    const/16 v19, 0x64

    move/from16 v0, v19

    if-eq v7, v0, :cond_14

    const/16 v19, 0x41

    move/from16 v0, v19

    if-le v7, v0, :cond_14

    const/16 v19, 0x30

    move/from16 v0, v16

    move/from16 v1, v19

    if-lt v0, v1, :cond_14

    const/16 v19, 0x39

    move/from16 v0, v16

    move/from16 v1, v19

    if-gt v0, v1, :cond_14

    const/16 v19, 0x0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v19

    const/16 v20, 0x2f

    move/from16 v0, v19

    move/from16 v1, v20

    if-eq v0, v1, :cond_14

    .line 756
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/core/util/Parser;->reader:Ljava/io/InputStream;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Ljava/io/InputStream;->reset()V

    .line 931
    :cond_13
    :goto_1
    if-eqz v12, :cond_2a

    .line 932
    const/16 v19, 0x1

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lorg/icepdf/core/util/Parser;->lastTokenHString:Z

    .line 933
    new-instance v19, Lorg/icepdf/core/pobjects/HexStringObject;

    move-object/from16 v0, v19

    move-object/from16 v1, v18

    invoke-direct {v0, v1}, Lorg/icepdf/core/pobjects/HexStringObject;-><init>(Ljava/lang/StringBuilder;)V

    goto/16 :goto_0

    .line 761
    :cond_14
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/core/util/Parser;->reader:Ljava/io/InputStream;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Ljava/io/InputStream;->read()I

    move-result v6

    .line 765
    if-ltz v6, :cond_15

    .line 766
    int-to-char v7, v6

    .line 772
    if-eqz v15, :cond_25

    .line 773
    if-eqz v12, :cond_16

    .line 775
    const/16 v19, 0x3e

    move/from16 v0, v19

    if-ne v7, v0, :cond_1c

    .line 776
    const/4 v4, 0x1

    .line 777
    move-object/from16 v0, v18

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 768
    :cond_15
    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    goto/16 :goto_0

    .line 782
    :cond_16
    const/16 v19, 0x28

    move/from16 v0, v19

    if-ne v7, v0, :cond_17

    .line 783
    add-int/lit8 v17, v17, 0x1

    .line 785
    :cond_17
    const/16 v19, 0x29

    move/from16 v0, v19

    if-ne v7, v0, :cond_19

    .line 786
    if-nez v17, :cond_18

    .line 787
    const/4 v4, 0x1

    .line 788
    move-object/from16 v0, v18

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 791
    :cond_18
    add-int/lit8 v17, v17, -0x1

    .line 810
    :cond_19
    const/16 v19, 0x5c

    move/from16 v0, v19

    if-ne v7, v0, :cond_1c

    .line 812
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/core/util/Parser;->reader:Ljava/io/InputStream;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Ljava/io/InputStream;->read()I

    move-result v19

    move/from16 v0, v19

    int-to-char v7, v0

    .line 816
    invoke-static {v7}, Ljava/lang/Character;->isDigit(C)Z

    move-result v19

    if-eqz v19, :cond_1e

    .line 818
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    .line 819
    .local v8, "digit":Ljava/lang/StringBuilder;
    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 822
    const/4 v13, 0x0

    .local v13, "i":I
    :goto_2
    const/16 v19, 0x2

    move/from16 v0, v19

    if-ge v13, v0, :cond_1b

    .line 825
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/core/util/Parser;->reader:Ljava/io/InputStream;

    move-object/from16 v19, v0

    const/16 v20, 0x1

    invoke-virtual/range {v19 .. v20}, Ljava/io/InputStream;->mark(I)V

    .line 827
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/core/util/Parser;->reader:Ljava/io/InputStream;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Ljava/io/InputStream;->read()I

    move-result v19

    move/from16 v0, v19

    int-to-char v7, v0

    .line 828
    invoke-static {v7}, Ljava/lang/Character;->isDigit(C)Z

    move-result v19

    if-eqz v19, :cond_1a

    .line 829
    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 822
    add-int/lit8 v13, v13, 0x1

    goto :goto_2

    .line 833
    :cond_1a
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/core/util/Parser;->reader:Ljava/io/InputStream;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Ljava/io/InputStream;->reset()V

    .line 839
    :cond_1b
    const/4 v3, 0x0

    .line 841
    .local v3, "charNumber":I
    :try_start_0
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    const/16 v20, 0x8

    invoke-static/range {v19 .. v20}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    .line 847
    :goto_3
    int-to-char v7, v3

    .line 911
    .end local v3    # "charNumber":I
    .end local v8    # "digit":Ljava/lang/StringBuilder;
    .end local v13    # "i":I
    :cond_1c
    :goto_4
    if-nez v14, :cond_29

    .line 912
    if-eqz v15, :cond_28

    .line 913
    move-object/from16 v0, v18

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 925
    :cond_1d
    :goto_5
    if-eqz v4, :cond_11

    goto/16 :goto_1

    .line 843
    .restart local v3    # "charNumber":I
    .restart local v8    # "digit":Ljava/lang/StringBuilder;
    .restart local v13    # "i":I
    :catch_0
    move-exception v9

    .line 844
    .local v9, "e":Ljava/lang/NumberFormatException;
    sget-object v19, Lorg/icepdf/core/util/Parser;->logger:Ljava/util/logging/Logger;

    sget-object v20, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string/jumbo v21, "Integer parse error "

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    move-object/from16 v2, v21

    invoke-virtual {v0, v1, v2, v9}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_3

    .line 850
    .end local v3    # "charNumber":I
    .end local v8    # "digit":Ljava/lang/StringBuilder;
    .end local v9    # "e":Ljava/lang/NumberFormatException;
    .end local v13    # "i":I
    :cond_1e
    const/16 v19, 0x28

    move/from16 v0, v19

    if-eq v7, v0, :cond_1c

    const/16 v19, 0x29

    move/from16 v0, v19

    if-eq v7, v0, :cond_1c

    const/16 v19, 0x5c

    move/from16 v0, v19

    if-eq v7, v0, :cond_1c

    .line 856
    const/16 v19, 0x74

    move/from16 v0, v19

    if-ne v7, v0, :cond_1f

    .line 857
    const/16 v7, 0x9

    goto :goto_4

    .line 860
    :cond_1f
    const/16 v19, 0x72

    move/from16 v0, v19

    if-ne v7, v0, :cond_20

    .line 861
    const/16 v7, 0xd

    goto :goto_4

    .line 864
    :cond_20
    const/16 v19, 0x6e

    move/from16 v0, v19

    if-ne v7, v0, :cond_21

    .line 865
    const/16 v7, 0xa

    goto :goto_4

    .line 868
    :cond_21
    const/16 v19, 0x62

    move/from16 v0, v19

    if-ne v7, v0, :cond_22

    .line 869
    const/16 v7, 0x8

    goto :goto_4

    .line 872
    :cond_22
    const/16 v19, 0x66

    move/from16 v0, v19

    if-ne v7, v0, :cond_23

    .line 873
    const/16 v7, 0xc

    goto :goto_4

    .line 876
    :cond_23
    const/16 v19, 0xd

    move/from16 v0, v19

    if-ne v7, v0, :cond_24

    .line 877
    const/4 v14, 0x1

    goto :goto_4

    .line 881
    :cond_24
    sget-object v19, Lorg/icepdf/core/util/Parser;->logger:Ljava/util/logging/Logger;

    sget-object v20, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    invoke-virtual/range {v19 .. v20}, Ljava/util/logging/Logger;->isLoggable(Ljava/util/logging/Level;)Z

    move-result v19

    if-eqz v19, :cond_1c

    .line 882
    sget-object v19, Lorg/icepdf/core/util/Parser;->logger:Ljava/util/logging/Logger;

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v21, "C="

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/util/logging/Logger;->warning(Ljava/lang/String;)V

    goto/16 :goto_4

    .line 891
    :cond_25
    invoke-static {v7}, Lorg/icepdf/core/util/Parser;->isWhitespace(C)Z

    move-result v19

    if-eqz v19, :cond_27

    .line 895
    const/16 v19, 0xd

    move/from16 v0, v19

    if-eq v6, v0, :cond_26

    const/16 v19, 0xa

    move/from16 v0, v19

    if-ne v6, v0, :cond_13

    .line 896
    :cond_26
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/core/util/Parser;->reader:Ljava/io/InputStream;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Ljava/io/InputStream;->reset()V

    goto/16 :goto_1

    .line 904
    :cond_27
    invoke-static {v7}, Lorg/icepdf/core/util/Parser;->isDelimiter(C)Z

    move-result v19

    if-eqz v19, :cond_1c

    .line 906
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/core/util/Parser;->reader:Ljava/io/InputStream;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Ljava/io/InputStream;->reset()V

    goto/16 :goto_1

    .line 916
    :cond_28
    const/16 v19, 0x80

    move/from16 v0, v19

    if-ge v7, v0, :cond_1d

    .line 917
    move-object/from16 v0, v18

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto/16 :goto_5

    .line 922
    :cond_29
    const/4 v14, 0x0

    goto/16 :goto_5

    .line 939
    :cond_2a
    if-eqz v15, :cond_2b

    .line 940
    new-instance v19, Lorg/icepdf/core/pobjects/LiteralStringObject;

    move-object/from16 v0, v19

    move-object/from16 v1, v18

    invoke-direct {v0, v1}, Lorg/icepdf/core/pobjects/LiteralStringObject;-><init>(Ljava/lang/StringBuilder;)V

    goto/16 :goto_0

    .line 943
    :cond_2b
    const/16 v19, 0x0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v19

    const/16 v20, 0x2f

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_2c

    .line 944
    new-instance v19, Lorg/icepdf/core/pobjects/Name;

    const/16 v20, 0x0

    move-object/from16 v0, v18

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->deleteCharAt(I)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-direct/range {v19 .. v20}, Lorg/icepdf/core/pobjects/Name;-><init>(Ljava/lang/StringBuilder;)V

    goto/16 :goto_0

    .line 948
    :cond_2c
    const/4 v11, 0x0

    .line 949
    .local v11, "foundDigit":Z
    const/4 v10, 0x0

    .line 950
    .local v10, "foundDecimal":Z
    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->length()I

    move-result v19

    add-int/lit8 v13, v19, -0x1

    .restart local v13    # "i":I
    :goto_6
    if-ltz v13, :cond_2f

    .line 951
    move-object/from16 v0, v18

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v5

    .line 952
    .local v5, "curr":C
    const/16 v19, 0x2e

    move/from16 v0, v19

    if-ne v5, v0, :cond_2e

    .line 953
    const/4 v10, 0x1

    .line 950
    :cond_2d
    :goto_7
    add-int/lit8 v13, v13, -0x1

    goto :goto_6

    .line 954
    :cond_2e
    const/16 v19, 0x30

    move/from16 v0, v19

    if-lt v5, v0, :cond_2d

    const/16 v19, 0x39

    move/from16 v0, v19

    if-gt v5, v0, :cond_2d

    .line 955
    const/4 v11, 0x1

    goto :goto_7

    .line 959
    .end local v5    # "curr":C
    :cond_2f
    if-eqz v11, :cond_31

    .line 961
    if-eqz v10, :cond_30

    .line 962
    :try_start_1
    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v19 .. v19}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v19

    goto/16 :goto_0

    .line 964
    :cond_30
    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v19

    goto/16 :goto_0

    .line 967
    :catch_1
    move-exception v19

    .line 972
    :cond_31
    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    goto/16 :goto_0
.end method

.method hexToInt(Ljava/lang/String;)I
    .locals 2
    .param p1, "hex"    # Ljava/lang/String;

    .prologue
    .line 1117
    const/4 v0, 0x1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object p1

    .line 1118
    const/16 v0, 0x10

    invoke-static {p1, v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method hexToString(Ljava/lang/String;)Ljava/lang/String;
    .locals 10
    .param p1, "hh"    # Ljava/lang/String;

    .prologue
    const/4 v9, 0x3

    const/16 v8, 0x46

    const/4 v7, 0x2

    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 1125
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {p1, v5, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object p1

    .line 1126
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 1127
    .local v2, "sb":Ljava/lang/StringBuilder;
    invoke-virtual {p1, v6}, Ljava/lang/String;->charAt(I)C

    move-result v3

    if-ne v3, v8, :cond_0

    invoke-virtual {p1, v5}, Ljava/lang/String;->charAt(I)C

    move-result v3

    const/16 v4, 0x45

    if-ne v3, v4, :cond_0

    invoke-virtual {p1, v7}, Ljava/lang/String;->charAt(I)C

    move-result v3

    if-ne v3, v8, :cond_0

    invoke-virtual {p1, v9}, Ljava/lang/String;->charAt(I)C

    move-result v3

    if-ne v3, v8, :cond_0

    .line 1131
    const/4 v3, 0x4

    new-array v0, v3, [B

    .line 1132
    .local v0, "b":[B
    const/4 v1, 0x1

    .local v1, "i":I
    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    div-int/lit8 v3, v3, 0x4

    if-ge v1, v3, :cond_1

    .line 1133
    mul-int/lit8 v3, v1, 0x4

    invoke-virtual {p1, v3}, Ljava/lang/String;->charAt(I)C

    move-result v3

    int-to-byte v3, v3

    aput-byte v3, v0, v6

    .line 1134
    mul-int/lit8 v3, v1, 0x4

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {p1, v3}, Ljava/lang/String;->charAt(I)C

    move-result v3

    int-to-byte v3, v3

    aput-byte v3, v0, v5

    .line 1135
    mul-int/lit8 v3, v1, 0x4

    add-int/lit8 v3, v3, 0x2

    invoke-virtual {p1, v3}, Ljava/lang/String;->charAt(I)C

    move-result v3

    int-to-byte v3, v3

    aput-byte v3, v0, v7

    .line 1136
    mul-int/lit8 v3, v1, 0x4

    add-int/lit8 v3, v3, 0x3

    invoke-virtual {p1, v3}, Ljava/lang/String;->charAt(I)C

    move-result v3

    int-to-byte v3, v3

    aput-byte v3, v0, v9

    .line 1137
    new-instance v3, Ljava/lang/String;

    invoke-direct {v3, v0}, Ljava/lang/String;-><init>([B)V

    const/16 v4, 0x10

    invoke-static {v3, v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v3

    int-to-char v3, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1132
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1140
    .end local v0    # "b":[B
    .end local v1    # "i":I
    :cond_0
    new-array v0, v7, [B

    .line 1141
    .restart local v0    # "b":[B
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_1
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    if-ge v1, v3, :cond_1

    .line 1143
    const/4 v3, 0x0

    mul-int/lit8 v4, v1, 0x2

    :try_start_0
    invoke-virtual {p1, v4}, Ljava/lang/String;->charAt(I)C

    move-result v4

    int-to-byte v4, v4

    aput-byte v4, v0, v3

    .line 1144
    const/4 v3, 0x1

    mul-int/lit8 v4, v1, 0x2

    add-int/lit8 v4, v4, 0x1

    invoke-virtual {p1, v4}, Ljava/lang/String;->charAt(I)C

    move-result v4

    int-to-byte v4, v4

    aput-byte v4, v0, v3

    .line 1145
    new-instance v3, Ljava/lang/String;

    invoke-direct {v3, v0}, Ljava/lang/String;-><init>([B)V

    const/16 v4, 0x10

    invoke-static {v3, v4}, Ljava/lang/Short;->parseShort(Ljava/lang/String;I)S

    move-result v3

    int-to-char v3, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1141
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1152
    :cond_1
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3

    .line 1147
    :catch_0
    move-exception v3

    goto :goto_2
.end method

.method peek2()Ljava/lang/String;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x2

    .line 1308
    iget-object v2, p0, Lorg/icepdf/core/util/Parser;->reader:Ljava/io/InputStream;

    invoke-virtual {v2, v3}, Ljava/io/InputStream;->mark(I)V

    .line 1309
    new-array v0, v3, [C

    .line 1310
    .local v0, "c":[C
    const/4 v2, 0x0

    iget-object v3, p0, Lorg/icepdf/core/util/Parser;->reader:Ljava/io/InputStream;

    invoke-virtual {v3}, Ljava/io/InputStream;->read()I

    move-result v3

    int-to-char v3, v3

    aput-char v3, v0, v2

    .line 1311
    const/4 v2, 0x1

    iget-object v3, p0, Lorg/icepdf/core/util/Parser;->reader:Ljava/io/InputStream;

    invoke-virtual {v3}, Ljava/io/InputStream;->read()I

    move-result v3

    int-to-char v3, v3

    aput-char v3, v0, v2

    .line 1312
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([C)V

    .line 1313
    .local v1, "s":Ljava/lang/String;
    iget-object v2, p0, Lorg/icepdf/core/util/Parser;->reader:Ljava/io/InputStream;

    invoke-virtual {v2}, Ljava/io/InputStream;->reset()V

    .line 1314
    return-object v1
.end method

.method readByte()B
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1226
    iget-object v0, p0, Lorg/icepdf/core/util/Parser;->reader:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->read()I

    move-result v0

    int-to-byte v0, v0

    return v0
.end method

.method readLineForInlineImage(Ljava/io/OutputStream;)Z
    .locals 13
    .param p1, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v12, 0xa

    const/4 v11, 0x2

    const/16 v10, 0x49

    const/16 v9, 0x45

    const/4 v6, 0x1

    .line 1162
    const/4 v0, 0x0

    .line 1163
    .local v0, "STATE_PRE_E":I
    const/4 v1, 0x1

    .line 1164
    .local v1, "STATE_PRE_I":I
    const/4 v2, 0x2

    .line 1165
    .local v2, "STATE_PRE_WHITESPACE":I
    const/4 v5, 0x0

    .line 1168
    .local v5, "state":I
    :cond_0
    :goto_0
    iget-object v7, p0, Lorg/icepdf/core/util/Parser;->reader:Ljava/io/InputStream;

    invoke-virtual {v7}, Ljava/io/InputStream;->read()I

    move-result v3

    .line 1169
    .local v3, "c":I
    if-gez v3, :cond_1

    .line 1170
    new-instance v6, Ljava/io/IOException;

    const-string/jumbo v7, "Error parsing Inline Image"

    invoke-direct {v6, v7}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 1171
    :cond_1
    if-nez v5, :cond_2

    if-ne v3, v9, :cond_2

    .line 1172
    add-int/lit8 v5, v5, 0x1

    .line 1173
    goto :goto_0

    .line 1174
    :cond_2
    if-ne v5, v6, :cond_3

    if-ne v3, v10, :cond_3

    .line 1175
    add-int/lit8 v5, v5, 0x1

    .line 1176
    goto :goto_0

    .line 1177
    :cond_3
    if-ne v5, v11, :cond_6

    and-int/lit16 v7, v3, 0xff

    int-to-char v7, v7

    invoke-static {v7}, Lorg/icepdf/core/util/Parser;->isWhitespace(C)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 1184
    iget-object v7, p0, Lorg/icepdf/core/util/Parser;->reader:Ljava/io/InputStream;

    const/16 v8, 0x20

    invoke-static {v7, v8}, Lorg/icepdf/core/util/Parser;->isStillInlineImageData(Ljava/io/InputStream;I)Z

    move-result v4

    .line 1185
    .local v4, "imageDataFound":Z
    if-eqz v4, :cond_5

    .line 1186
    invoke-virtual {p1, v9}, Ljava/io/OutputStream;->write(I)V

    .line 1187
    invoke-virtual {p1, v10}, Ljava/io/OutputStream;->write(I)V

    .line 1188
    invoke-virtual {p1, v3}, Ljava/io/OutputStream;->write(I)V

    .line 1189
    const/4 v5, 0x0

    .line 1191
    const/16 v7, 0xd

    if-eq v3, v7, :cond_4

    if-ne v3, v12, :cond_0

    .line 1215
    .end local v4    # "imageDataFound":Z
    :cond_4
    :goto_1
    if-nez v11, :cond_9

    .line 1217
    :cond_5
    :goto_2
    return v6

    .line 1201
    :cond_6
    if-lez v5, :cond_7

    .line 1202
    invoke-virtual {p1, v9}, Ljava/io/OutputStream;->write(I)V

    .line 1203
    :cond_7
    if-le v5, v6, :cond_8

    .line 1204
    invoke-virtual {p1, v10}, Ljava/io/OutputStream;->write(I)V

    .line 1205
    :cond_8
    const/4 v5, 0x0

    .line 1207
    int-to-byte v7, v3

    invoke-virtual {p1, v7}, Ljava/io/OutputStream;->write(I)V

    .line 1208
    const/16 v7, 0xd

    if-eq v3, v7, :cond_4

    if-ne v3, v12, :cond_0

    goto :goto_1

    .line 1217
    :cond_9
    const/4 v6, 0x0

    goto :goto_2
.end method

.method public ungetNumberOrStringWithReset()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1033
    iget-object v0, p0, Lorg/icepdf/core/util/Parser;->reader:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->reset()V

    .line 1034
    return-void
.end method
