.class public interface abstract Lorg/icepdf/core/util/MemoryManagerDelegate;
.super Ljava/lang/Object;
.source "MemoryManagerDelegate.java"


# static fields
.field public static final REDUCE_AGGRESSIVELY:I = 0x1

.field public static final REDUCE_SOMEWHAT:I


# virtual methods
.method public abstract getLibrary()Lorg/icepdf/core/util/Library;
.end method

.method public abstract reduceMemory(I)Z
.end method
