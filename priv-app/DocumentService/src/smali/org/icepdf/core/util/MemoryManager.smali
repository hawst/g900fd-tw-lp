.class public Lorg/icepdf/core/util/MemoryManager;
.super Ljava/lang/Object;
.source "MemoryManager.java"


# static fields
.field private static instance:Lorg/icepdf/core/util/MemoryManager;

.field private static final logger:Ljava/util/logging/Logger;


# instance fields
.field protected cumulativeDurationManagingMemory:J

.field protected cumulativeDurationNotManagingMemory:J

.field protected delegates:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lorg/icepdf/core/util/MemoryManagerDelegate;",
            ">;"
        }
    .end annotation
.end field

.field protected leastRecentlyUsed:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lorg/icepdf/core/util/MemoryManageable;",
            ">;"
        }
    .end annotation
.end field

.field protected locked:Ljava/util/WeakHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/WeakHashMap",
            "<",
            "Ljava/lang/Object;",
            "Ljava/util/HashSet",
            "<",
            "Lorg/icepdf/core/util/MemoryManageable;",
            ">;>;"
        }
    .end annotation
.end field

.field protected maxMemory:J

.field protected maxSize:I

.field protected minMemory:J

.field protected percentageDurationManagingMemory:I

.field protected previousTimestampManagedMemory:J

.field protected purgeSize:I

.field protected final runtime:Ljava/lang/Runtime;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    const-class v0, Lorg/icepdf/core/util/MemoryManager;

    invoke-virtual {v0}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Lorg/icepdf/core/util/MemoryManager;->logger:Ljava/util/logging/Logger;

    return-void
.end method

.method protected constructor <init>()V
    .locals 5

    .prologue
    .line 95
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v2

    iput-object v2, p0, Lorg/icepdf/core/util/MemoryManager;->runtime:Ljava/lang/Runtime;

    .line 48
    const-wide/32 v2, 0x493e0

    iput-wide v2, p0, Lorg/icepdf/core/util/MemoryManager;->minMemory:J

    .line 53
    iget-object v2, p0, Lorg/icepdf/core/util/MemoryManager;->runtime:Ljava/lang/Runtime;

    invoke-virtual {v2}, Ljava/lang/Runtime;->maxMemory()J

    move-result-wide v2

    iput-wide v2, p0, Lorg/icepdf/core/util/MemoryManager;->maxMemory:J

    .line 98
    :try_start_0
    const-string/jumbo v2, "org.icepdf.core.minMemory"

    invoke-static {v2}, Lorg/icepdf/core/util/MemoryManager;->parse(Ljava/lang/String;)I

    move-result v1

    .line 99
    .local v1, "t":I
    if-lez v1, :cond_0

    .line 100
    int-to-long v2, v1

    iput-wide v2, p0, Lorg/icepdf/core/util/MemoryManager;->minMemory:J
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 106
    .end local v1    # "t":I
    :cond_0
    :goto_0
    const-string/jumbo v2, "org.icepdf.core.purgeSize"

    const/4 v3, 0x5

    invoke-static {v2, v3}, Lorg/icepdf/core/util/Defs;->sysPropertyInt(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lorg/icepdf/core/util/MemoryManager;->purgeSize:I

    .line 108
    const-string/jumbo v2, "org.icepdf.core.maxSize"

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lorg/icepdf/core/util/Defs;->sysPropertyInt(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lorg/icepdf/core/util/MemoryManager;->maxSize:I

    .line 111
    new-instance v2, Ljava/util/WeakHashMap;

    invoke-direct {v2}, Ljava/util/WeakHashMap;-><init>()V

    iput-object v2, p0, Lorg/icepdf/core/util/MemoryManager;->locked:Ljava/util/WeakHashMap;

    .line 112
    new-instance v2, Ljava/util/ArrayList;

    const/16 v3, 0x100

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v2, p0, Lorg/icepdf/core/util/MemoryManager;->leastRecentlyUsed:Ljava/util/ArrayList;

    .line 113
    new-instance v2, Ljava/util/ArrayList;

    const/16 v3, 0x40

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v2, p0, Lorg/icepdf/core/util/MemoryManager;->delegates:Ljava/util/ArrayList;

    .line 114
    return-void

    .line 102
    :catch_0
    move-exception v0

    .line 103
    .local v0, "e":Ljava/lang/Throwable;
    sget-object v2, Lorg/icepdf/core/util/MemoryManager;->logger:Ljava/util/logging/Logger;

    sget-object v3, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string/jumbo v4, "Error setting org.icepdf.core.minMemory"

    invoke-virtual {v2, v3, v4}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private canAllocate(IZ)Z
    .locals 10
    .param p1, "bytes"    # I
    .param p2, "doGC"    # Z

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 388
    iget-object v6, p0, Lorg/icepdf/core/util/MemoryManager;->runtime:Ljava/lang/Runtime;

    invoke-virtual {v6}, Ljava/lang/Runtime;->freeMemory()J

    move-result-wide v0

    .line 391
    .local v0, "mem":J
    int-to-long v6, p1

    sub-long v6, v0, v6

    iget-wide v8, p0, Lorg/icepdf/core/util/MemoryManager;->minMemory:J

    cmp-long v6, v6, v8

    if-lez v6, :cond_1

    .line 435
    :cond_0
    :goto_0
    return v4

    .line 396
    :cond_1
    iget-object v6, p0, Lorg/icepdf/core/util/MemoryManager;->runtime:Ljava/lang/Runtime;

    invoke-virtual {v6}, Ljava/lang/Runtime;->totalMemory()J

    move-result-wide v2

    .line 398
    .local v2, "total":J
    iget-wide v6, p0, Lorg/icepdf/core/util/MemoryManager;->maxMemory:J

    cmp-long v6, v6, v2

    if-lez v6, :cond_2

    .line 399
    iget-wide v6, p0, Lorg/icepdf/core/util/MemoryManager;->maxMemory:J

    sub-long/2addr v6, v2

    add-long/2addr v0, v6

    .line 400
    int-to-long v6, p1

    sub-long v6, v0, v6

    iget-wide v8, p0, Lorg/icepdf/core/util/MemoryManager;->minMemory:J

    cmp-long v6, v6, v8

    if-gtz v6, :cond_0

    .line 408
    :cond_2
    if-nez p2, :cond_3

    move v4, v5

    .line 409
    goto :goto_0

    .line 412
    :cond_3
    invoke-virtual {p0}, Lorg/icepdf/core/util/MemoryManager;->reduceMemory()Z

    .line 415
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 416
    iget-object v6, p0, Lorg/icepdf/core/util/MemoryManager;->runtime:Ljava/lang/Runtime;

    invoke-virtual {v6}, Ljava/lang/Runtime;->freeMemory()J

    move-result-wide v0

    .line 419
    int-to-long v6, p1

    sub-long v6, v0, v6

    iget-wide v8, p0, Lorg/icepdf/core/util/MemoryManager;->minMemory:J

    cmp-long v6, v6, v8

    if-gtz v6, :cond_0

    .line 424
    invoke-static {}, Ljava/lang/System;->runFinalization()V

    .line 425
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 426
    iget-object v6, p0, Lorg/icepdf/core/util/MemoryManager;->runtime:Ljava/lang/Runtime;

    invoke-virtual {v6}, Ljava/lang/Runtime;->freeMemory()J

    move-result-wide v0

    .line 429
    int-to-long v6, p1

    sub-long v6, v0, v6

    iget-wide v8, p0, Lorg/icepdf/core/util/MemoryManager;->minMemory:J

    cmp-long v6, v6, v8

    if-gtz v6, :cond_0

    move v4, v5

    .line 435
    goto :goto_0
.end method

.method private finishedMemoryProcessing(J)V
    .locals 13
    .param p1, "beginTime"    # J

    .prologue
    const-wide/16 v10, 0x0

    .line 476
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 477
    .local v2, "endTime":J
    sub-long v0, v2, p1

    .line 478
    .local v0, "duration":J
    cmp-long v6, v0, v10

    if-lez v6, :cond_0

    .line 479
    iget-wide v6, p0, Lorg/icepdf/core/util/MemoryManager;->cumulativeDurationManagingMemory:J

    add-long/2addr v6, v0

    iput-wide v6, p0, Lorg/icepdf/core/util/MemoryManager;->cumulativeDurationManagingMemory:J

    .line 480
    :cond_0
    iget-wide v6, p0, Lorg/icepdf/core/util/MemoryManager;->previousTimestampManagedMemory:J

    cmp-long v6, v6, v10

    if-eqz v6, :cond_1

    .line 481
    iget-wide v6, p0, Lorg/icepdf/core/util/MemoryManager;->previousTimestampManagedMemory:J

    sub-long v0, p1, v6

    .line 482
    cmp-long v6, v0, v10

    if-lez v6, :cond_1

    .line 483
    iget-wide v6, p0, Lorg/icepdf/core/util/MemoryManager;->cumulativeDurationNotManagingMemory:J

    add-long/2addr v6, v0

    iput-wide v6, p0, Lorg/icepdf/core/util/MemoryManager;->cumulativeDurationNotManagingMemory:J

    .line 485
    :cond_1
    iput-wide v2, p0, Lorg/icepdf/core/util/MemoryManager;->previousTimestampManagedMemory:J

    .line 487
    iget-wide v6, p0, Lorg/icepdf/core/util/MemoryManager;->cumulativeDurationManagingMemory:J

    iget-wide v8, p0, Lorg/icepdf/core/util/MemoryManager;->cumulativeDurationNotManagingMemory:J

    add-long v4, v6, v8

    .line 488
    .local v4, "totalDuration":J
    cmp-long v6, v4, v10

    if-lez v6, :cond_2

    .line 489
    iget-wide v6, p0, Lorg/icepdf/core/util/MemoryManager;->cumulativeDurationManagingMemory:J

    const-wide/16 v8, 0x64

    mul-long/2addr v6, v8

    div-long/2addr v6, v4

    long-to-int v6, v6

    iput v6, p0, Lorg/icepdf/core/util/MemoryManager;->percentageDurationManagingMemory:I

    .line 492
    :cond_2
    return-void
.end method

.method public static getInstance()Lorg/icepdf/core/util/MemoryManager;
    .locals 1

    .prologue
    .line 86
    sget-object v0, Lorg/icepdf/core/util/MemoryManager;->instance:Lorg/icepdf/core/util/MemoryManager;

    if-nez v0, :cond_0

    .line 87
    new-instance v0, Lorg/icepdf/core/util/MemoryManager;

    invoke-direct {v0}, Lorg/icepdf/core/util/MemoryManager;-><init>()V

    sput-object v0, Lorg/icepdf/core/util/MemoryManager;->instance:Lorg/icepdf/core/util/MemoryManager;

    .line 89
    :cond_0
    sget-object v0, Lorg/icepdf/core/util/MemoryManager;->instance:Lorg/icepdf/core/util/MemoryManager;

    return-object v0
.end method

.method private static parse(Ljava/lang/String;)I
    .locals 5
    .param p0, "memoryValue"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 336
    invoke-static {p0}, Lorg/icepdf/core/util/Defs;->sysProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 337
    .local v2, "s":Ljava/lang/String;
    if-nez v2, :cond_0

    .line 338
    const/4 v3, -0x1

    .line 351
    :goto_0
    return v3

    .line 340
    :cond_0
    const/4 v1, 0x1

    .line 341
    .local v1, "mult":I
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v2, v3}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 342
    .local v0, "c":C
    const/16 v3, 0x6b

    if-eq v0, v3, :cond_1

    const/16 v3, 0x4b

    if-ne v0, v3, :cond_2

    .line 343
    :cond_1
    const/16 v1, 0x400

    .line 344
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v2, v4, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 346
    :cond_2
    const/16 v3, 0x6d

    if-eq v0, v3, :cond_3

    const/16 v3, 0x4d

    if-ne v0, v3, :cond_4

    .line 347
    :cond_3
    const/high16 v1, 0x100000

    .line 348
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v2, v4, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 351
    :cond_4
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    mul-int/2addr v3, v1

    goto :goto_0
.end method


# virtual methods
.method public checkMemory(I)Z
    .locals 7
    .param p1, "memoryNeeded"    # I

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 455
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 456
    .local v0, "beginTime":J
    const/4 v2, 0x0

    .line 458
    .local v2, "count":I
    :cond_0
    if-lez v2, :cond_1

    move v4, v5

    :goto_0
    invoke-direct {p0, p1, v4}, Lorg/icepdf/core/util/MemoryManager;->canAllocate(IZ)Z

    move-result v4

    if-nez v4, :cond_3

    .line 460
    invoke-virtual {p0}, Lorg/icepdf/core/util/MemoryManager;->reduceMemory()Z

    move-result v3

    .line 461
    .local v3, "reducedSomething":Z
    if-nez v3, :cond_2

    if-lez v2, :cond_2

    .line 462
    invoke-direct {p0, v0, v1}, Lorg/icepdf/core/util/MemoryManager;->finishedMemoryProcessing(J)V

    .line 472
    .end local v3    # "reducedSomething":Z
    :goto_1
    return v6

    :cond_1
    move v4, v6

    .line 458
    goto :goto_0

    .line 465
    .restart local v3    # "reducedSomething":Z
    :cond_2
    add-int/lit8 v2, v2, 0x1

    .line 466
    const/16 v4, 0xa

    if-le v2, v4, :cond_0

    .line 467
    invoke-direct {p0, v0, v1}, Lorg/icepdf/core/util/MemoryManager;->finishedMemoryProcessing(J)V

    goto :goto_1

    .line 471
    .end local v3    # "reducedSomething":Z
    :cond_3
    invoke-direct {p0, v0, v1}, Lorg/icepdf/core/util/MemoryManager;->finishedMemoryProcessing(J)V

    move v6, v5

    .line 472
    goto :goto_1
.end method

.method public getFreeMemory()J
    .locals 2

    .prologue
    .line 380
    iget-object v0, p0, Lorg/icepdf/core/util/MemoryManager;->runtime:Ljava/lang/Runtime;

    invoke-virtual {v0}, Ljava/lang/Runtime;->freeMemory()J

    move-result-wide v0

    return-wide v0
.end method

.method public getMinMemory()J
    .locals 2

    .prologue
    .line 371
    iget-wide v0, p0, Lorg/icepdf/core/util/MemoryManager;->minMemory:J

    return-wide v0
.end method

.method protected declared-synchronized isLocked(Lorg/icepdf/core/util/MemoryManageable;)Z
    .locals 7
    .param p1, "mm"    # Lorg/icepdf/core/util/MemoryManageable;

    .prologue
    .line 303
    monitor-enter p0

    :try_start_0
    iget-object v6, p0, Lorg/icepdf/core/util/MemoryManager;->locked:Ljava/util/WeakHashMap;

    invoke-virtual {v6}, Ljava/util/WeakHashMap;->entrySet()Ljava/util/Set;

    move-result-object v1

    .line 305
    .local v1, "entries":Ljava/util/Set;
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 306
    .local v3, "entry1":Ljava/lang/Object;
    move-object v0, v3

    check-cast v0, Ljava/util/Map$Entry;

    move-object v2, v0

    .line 307
    .local v2, "entry":Ljava/util/Map$Entry;
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/HashSet;

    .line 308
    .local v5, "inUse":Ljava/util/HashSet;
    if-eqz v5, :cond_0

    invoke-virtual {v5, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v6

    if-eqz v6, :cond_0

    .line 309
    const/4 v6, 0x1

    .line 311
    .end local v2    # "entry":Ljava/util/Map$Entry;
    .end local v3    # "entry1":Ljava/lang/Object;
    .end local v5    # "inUse":Ljava/util/HashSet;
    :goto_0
    monitor-exit p0

    return v6

    :cond_1
    const/4 v6, 0x0

    goto :goto_0

    .line 303
    .end local v1    # "entries":Ljava/util/Set;
    .end local v4    # "i$":Ljava/util/Iterator;
    :catchall_0
    move-exception v6

    monitor-exit p0

    throw v6
.end method

.method public isLowMemory()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 447
    invoke-direct {p0, v1, v0}, Lorg/icepdf/core/util/MemoryManager;->canAllocate(IZ)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public declared-synchronized lock(Ljava/lang/Object;Lorg/icepdf/core/util/MemoryManageable;)V
    .locals 5
    .param p1, "user"    # Ljava/lang/Object;
    .param p2, "mm"    # Lorg/icepdf/core/util/MemoryManageable;

    .prologue
    .line 117
    monitor-enter p0

    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 139
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 120
    :cond_1
    :try_start_0
    iget-object v4, p0, Lorg/icepdf/core/util/MemoryManager;->locked:Ljava/util/WeakHashMap;

    invoke-virtual {v4, p1}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashSet;

    .line 121
    .local v0, "inUse":Ljava/util/HashSet;, "Ljava/util/HashSet<Lorg/icepdf/core/util/MemoryManageable;>;"
    if-nez v0, :cond_2

    .line 122
    new-instance v0, Ljava/util/HashSet;

    .end local v0    # "inUse":Ljava/util/HashSet;, "Ljava/util/HashSet<Lorg/icepdf/core/util/MemoryManageable;>;"
    const/16 v4, 0x100

    invoke-direct {v0, v4}, Ljava/util/HashSet;-><init>(I)V

    .line 123
    .restart local v0    # "inUse":Ljava/util/HashSet;, "Ljava/util/HashSet<Lorg/icepdf/core/util/MemoryManageable;>;"
    iget-object v4, p0, Lorg/icepdf/core/util/MemoryManager;->locked:Ljava/util/WeakHashMap;

    invoke-virtual {v4, p1, v0}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 125
    :cond_2
    invoke-virtual {v0, p2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 127
    iget-object v4, p0, Lorg/icepdf/core/util/MemoryManager;->leastRecentlyUsed:Ljava/util/ArrayList;

    invoke-virtual {v4, p2}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 128
    iget-object v4, p0, Lorg/icepdf/core/util/MemoryManager;->leastRecentlyUsed:Ljava/util/ArrayList;

    invoke-virtual {v4, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 130
    iget v4, p0, Lorg/icepdf/core/util/MemoryManager;->maxSize:I

    if-lez v4, :cond_0

    .line 131
    iget-object v4, p0, Lorg/icepdf/core/util/MemoryManager;->leastRecentlyUsed:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 132
    .local v2, "numUsed":I
    iget v4, p0, Lorg/icepdf/core/util/MemoryManager;->maxSize:I

    sub-int v3, v2, v4

    .line 133
    .local v3, "numUsedMoreThanShould":I
    if-lez v3, :cond_0

    .line 135
    iget v4, p0, Lorg/icepdf/core/util/MemoryManager;->purgeSize:I

    invoke-static {v4, v3}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 136
    .local v1, "numToDo":I
    invoke-virtual {p0, v1}, Lorg/icepdf/core/util/MemoryManager;->reduceMemory(I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 117
    .end local v0    # "inUse":Ljava/util/HashSet;, "Ljava/util/HashSet<Lorg/icepdf/core/util/MemoryManageable;>;"
    .end local v1    # "numToDo":I
    .end local v2    # "numUsed":I
    .end local v3    # "numUsedMoreThanShould":I
    :catchall_0
    move-exception v4

    monitor-exit p0

    throw v4
.end method

.method protected reduceMemory(I)I
    .locals 7
    .param p1, "numToDo"    # I

    .prologue
    .line 281
    const/4 v3, 0x0

    .line 283
    .local v3, "numDone":I
    const/4 v1, 0x0

    .line 284
    .local v1, "leastRecentlyUsedIndex":I
    :goto_0
    if-ge v3, p1, :cond_0

    :try_start_0
    iget-object v4, p0, Lorg/icepdf/core/util/MemoryManager;->leastRecentlyUsed:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v1, v4, :cond_0

    .line 286
    iget-object v4, p0, Lorg/icepdf/core/util/MemoryManager;->leastRecentlyUsed:Ljava/util/ArrayList;

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/icepdf/core/util/MemoryManageable;

    .line 288
    .local v2, "mm":Lorg/icepdf/core/util/MemoryManageable;
    invoke-virtual {p0, v2}, Lorg/icepdf/core/util/MemoryManager;->isLocked(Lorg/icepdf/core/util/MemoryManageable;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 289
    invoke-interface {v2}, Lorg/icepdf/core/util/MemoryManageable;->reduceMemory()V

    .line 290
    add-int/lit8 v3, v3, 0x1

    .line 291
    iget-object v4, p0, Lorg/icepdf/core/util/MemoryManager;->leastRecentlyUsed:Ljava/util/ArrayList;

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 295
    .end local v2    # "mm":Lorg/icepdf/core/util/MemoryManageable;
    :catch_0
    move-exception v0

    .line 296
    .local v0, "e":Ljava/lang/Exception;
    sget-object v4, Lorg/icepdf/core/util/MemoryManager;->logger:Ljava/util/logging/Logger;

    sget-object v5, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string/jumbo v6, "Problem while reducing memory"

    invoke-virtual {v4, v5, v6, v0}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 299
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_0
    return v3

    .line 293
    .restart local v2    # "mm":Lorg/icepdf/core/util/MemoryManageable;
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method protected declared-synchronized reduceMemory()Z
    .locals 9

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 255
    monitor-enter p0

    :try_start_0
    iget v4, p0, Lorg/icepdf/core/util/MemoryManager;->purgeSize:I

    .line 257
    .local v4, "numToDo":I
    const/4 v0, 0x0

    .line 258
    .local v0, "aggressive":I
    iget-object v7, p0, Lorg/icepdf/core/util/MemoryManager;->leastRecentlyUsed:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 259
    .local v2, "lruSize":I
    iget v7, p0, Lorg/icepdf/core/util/MemoryManager;->percentageDurationManagingMemory:I

    const/16 v8, 0xf

    if-gt v7, v8, :cond_0

    const/16 v7, 0x64

    if-le v2, v7, :cond_6

    .line 260
    :cond_0
    mul-int/lit8 v7, v2, 0x3c

    div-int/lit8 v0, v7, 0x64

    .line 265
    :cond_1
    :goto_0
    if-le v0, v4, :cond_2

    .line 266
    move v4, v0

    .line 268
    :cond_2
    invoke-virtual {p0, v4}, Lorg/icepdf/core/util/MemoryManager;->reduceMemory(I)I

    move-result v3

    .line 270
    .local v3, "numDone":I
    const/4 v1, 0x0

    .line 271
    .local v1, "delegatesReduced":Z
    if-nez v3, :cond_8

    .line 272
    const/4 v7, 0x1

    invoke-virtual {p0, v7}, Lorg/icepdf/core/util/MemoryManager;->reduceMemoryWithDelegates(Z)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    .line 276
    :cond_3
    :goto_1
    if-gtz v3, :cond_4

    if-eqz v1, :cond_5

    :cond_4
    move v5, v6

    :cond_5
    monitor-exit p0

    return v5

    .line 261
    .end local v1    # "delegatesReduced":Z
    .end local v3    # "numDone":I
    :cond_6
    const/16 v7, 0x32

    if-le v2, v7, :cond_7

    .line 262
    mul-int/lit8 v7, v2, 0x32

    :try_start_1
    div-int/lit8 v0, v7, 0x64

    goto :goto_0

    .line 263
    :cond_7
    const/16 v7, 0x14

    if-le v2, v7, :cond_1

    .line 264
    mul-int/lit8 v7, v2, 0x28

    div-int/lit8 v0, v7, 0x64

    goto :goto_0

    .line 273
    .restart local v1    # "delegatesReduced":Z
    .restart local v3    # "numDone":I
    :cond_8
    if-ge v3, v4, :cond_3

    .line 274
    const/4 v7, 0x0

    invoke-virtual {p0, v7}, Lorg/icepdf/core/util/MemoryManager;->reduceMemoryWithDelegates(Z)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v1

    goto :goto_1

    .line 255
    .end local v0    # "aggressive":I
    .end local v1    # "delegatesReduced":Z
    .end local v2    # "lruSize":I
    .end local v3    # "numDone":I
    .end local v4    # "numToDo":I
    :catchall_0
    move-exception v5

    monitor-exit p0

    throw v5
.end method

.method protected declared-synchronized reduceMemoryWithDelegates(Z)Z
    .locals 6
    .param p1, "aggressively"    # Z

    .prologue
    .line 315
    monitor-enter p0

    if-eqz p1, :cond_1

    const/4 v4, 0x1

    .line 317
    .local v4, "reductionPolicy":I
    :goto_0
    const/4 v0, 0x0

    .line 318
    .local v0, "anyReduced":Z
    :try_start_0
    iget-object v5, p0, Lorg/icepdf/core/util/MemoryManager;->delegates:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/icepdf/core/util/MemoryManagerDelegate;

    .line 319
    .local v2, "mmd":Lorg/icepdf/core/util/MemoryManagerDelegate;
    if-eqz v2, :cond_0

    .line 321
    invoke-interface {v2, v4}, Lorg/icepdf/core/util/MemoryManagerDelegate;->reduceMemory(I)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    .line 322
    .local v3, "reduced":Z
    or-int/2addr v0, v3

    .line 323
    goto :goto_1

    .line 315
    .end local v0    # "anyReduced":Z
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "mmd":Lorg/icepdf/core/util/MemoryManagerDelegate;
    .end local v3    # "reduced":Z
    .end local v4    # "reductionPolicy":I
    :cond_1
    const/4 v4, 0x0

    goto :goto_0

    .line 324
    .restart local v0    # "anyReduced":Z
    .restart local v1    # "i$":Ljava/util/Iterator;
    .restart local v4    # "reductionPolicy":I
    :cond_2
    monitor-exit p0

    return v0

    .line 315
    .end local v1    # "i$":Ljava/util/Iterator;
    :catchall_0
    move-exception v5

    monitor-exit p0

    throw v5
.end method

.method public declared-synchronized registerMemoryManagerDelegate(Lorg/icepdf/core/util/MemoryManagerDelegate;)V
    .locals 1
    .param p1, "delegate"    # Lorg/icepdf/core/util/MemoryManagerDelegate;

    .prologue
    .line 158
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/icepdf/core/util/MemoryManager;->delegates:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 159
    iget-object v0, p0, Lorg/icepdf/core/util/MemoryManager;->delegates:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 160
    :cond_0
    monitor-exit p0

    return-void

    .line 158
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized release(Ljava/lang/Object;Lorg/icepdf/core/util/MemoryManageable;)V
    .locals 2
    .param p1, "user"    # Ljava/lang/Object;
    .param p2, "mm"    # Lorg/icepdf/core/util/MemoryManageable;

    .prologue
    .line 142
    monitor-enter p0

    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 155
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 145
    :cond_1
    :try_start_0
    iget-object v1, p0, Lorg/icepdf/core/util/MemoryManager;->locked:Ljava/util/WeakHashMap;

    invoke-virtual {v1, p1}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashSet;

    .line 146
    .local v0, "inUse":Ljava/util/HashSet;
    if-eqz v0, :cond_0

    .line 150
    invoke-virtual {v0}, Ljava/util/HashSet;->size()I

    move-result v1

    if-nez v1, :cond_0

    .line 151
    iget-object v1, p0, Lorg/icepdf/core/util/MemoryManager;->locked:Ljava/util/WeakHashMap;

    invoke-virtual {v1, p1}, Ljava/util/WeakHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 142
    .end local v0    # "inUse":Ljava/util/HashSet;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized releaseAllByLibrary(Lorg/icepdf/core/util/Library;)V
    .locals 20
    .param p1, "library"    # Lorg/icepdf/core/util/Library;

    .prologue
    .line 164
    monitor-enter p0

    if-nez p1, :cond_1

    .line 248
    :cond_0
    monitor-exit p0

    return-void

    .line 169
    :cond_1
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/core/util/MemoryManager;->leastRecentlyUsed:Ljava/util/ArrayList;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Ljava/util/ArrayList;->size()I

    move-result v19

    add-int/lit8 v8, v19, -0x1

    .local v8, "i":I
    :goto_0
    if-ltz v8, :cond_4

    .line 170
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/core/util/MemoryManager;->leastRecentlyUsed:Ljava/util/ArrayList;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lorg/icepdf/core/util/MemoryManageable;

    .line 172
    .local v13, "mm":Lorg/icepdf/core/util/MemoryManageable;
    invoke-interface {v13}, Lorg/icepdf/core/util/MemoryManageable;->getLibrary()Lorg/icepdf/core/util/Library;

    move-result-object v12

    .line 173
    .local v12, "lib":Lorg/icepdf/core/util/Library;
    if-nez v12, :cond_3

    .line 169
    :cond_2
    :goto_1
    add-int/lit8 v8, v8, -0x1

    goto :goto_0

    .line 178
    :cond_3
    move-object/from16 v0, p1

    invoke-virtual {v12, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_2

    .line 179
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/core/util/MemoryManager;->leastRecentlyUsed:Ljava/util/ArrayList;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 164
    .end local v8    # "i":I
    .end local v12    # "lib":Lorg/icepdf/core/util/Library;
    .end local v13    # "mm":Lorg/icepdf/core/util/MemoryManageable;
    :catchall_0
    move-exception v19

    monitor-exit p0

    throw v19

    .line 185
    .restart local v8    # "i":I
    :cond_4
    :try_start_1
    new-instance v18, Ljava/util/ArrayList;

    invoke-direct/range {v18 .. v18}, Ljava/util/ArrayList;-><init>()V

    .line 186
    .local v18, "usersToRemove":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/core/util/MemoryManager;->locked:Ljava/util/WeakHashMap;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Ljava/util/WeakHashMap;->entrySet()Ljava/util/Set;

    move-result-object v5

    .line 187
    .local v5, "entries":Ljava/util/Set;
    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_5
    :goto_2
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v19

    if-eqz v19, :cond_9

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    .line 188
    .local v7, "entry1":Ljava/lang/Object;
    move-object v0, v7

    check-cast v0, Ljava/util/Map$Entry;

    move-object v6, v0

    .line 189
    .local v6, "entry":Ljava/util/Map$Entry;
    invoke-interface {v6}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v17

    .line 191
    .local v17, "user":Ljava/lang/Object;
    invoke-interface {v6}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/util/HashSet;

    .line 192
    .local v11, "inUse":Ljava/util/HashSet;
    if-eqz v11, :cond_5

    .line 194
    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    .line 195
    .local v15, "mmsToRemove":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/icepdf/core/util/MemoryManageable;>;"
    invoke-virtual {v11}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v10

    .local v10, "i$":Ljava/util/Iterator;
    :cond_6
    :goto_3
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v19

    if-eqz v19, :cond_7

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 196
    .local v3, "anInUse":Ljava/lang/Object;
    move-object v0, v3

    check-cast v0, Lorg/icepdf/core/util/MemoryManageable;

    move-object v13, v0

    .line 198
    .restart local v13    # "mm":Lorg/icepdf/core/util/MemoryManageable;
    if-eqz v13, :cond_6

    .line 199
    invoke-interface {v13}, Lorg/icepdf/core/util/MemoryManageable;->getLibrary()Lorg/icepdf/core/util/Library;

    move-result-object v12

    .line 200
    .restart local v12    # "lib":Lorg/icepdf/core/util/Library;
    if-eqz v12, :cond_6

    .line 205
    move-object/from16 v0, p1

    invoke-virtual {v12, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_6

    .line 206
    invoke-virtual {v15, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 210
    .end local v3    # "anInUse":Ljava/lang/Object;
    .end local v12    # "lib":Lorg/icepdf/core/util/Library;
    .end local v13    # "mm":Lorg/icepdf/core/util/MemoryManageable;
    :cond_7
    invoke-virtual {v15}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_4
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v19

    if-eqz v19, :cond_8

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/icepdf/core/util/MemoryManageable;

    .line 211
    .local v2, "aMmsToRemove":Lorg/icepdf/core/util/MemoryManageable;
    invoke-virtual {v11, v2}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    goto :goto_4

    .line 213
    .end local v2    # "aMmsToRemove":Lorg/icepdf/core/util/MemoryManageable;
    :cond_8
    invoke-virtual {v15}, Ljava/util/ArrayList;->clear()V

    .line 220
    invoke-virtual {v11}, Ljava/util/HashSet;->size()I

    move-result v19

    if-nez v19, :cond_5

    .line 222
    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 226
    .end local v6    # "entry":Ljava/util/Map$Entry;
    .end local v7    # "entry1":Ljava/lang/Object;
    .end local v10    # "i$":Ljava/util/Iterator;
    .end local v11    # "inUse":Ljava/util/HashSet;
    .end local v15    # "mmsToRemove":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/icepdf/core/util/MemoryManageable;>;"
    .end local v17    # "user":Ljava/lang/Object;
    :cond_9
    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v9

    .local v9, "i$":Ljava/util/Iterator;
    :goto_5
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v19

    if-eqz v19, :cond_a

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    .line 227
    .local v4, "anUsersToRemove":Ljava/lang/Object;
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/core/util/MemoryManager;->locked:Ljava/util/WeakHashMap;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Ljava/util/WeakHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_5

    .line 229
    .end local v4    # "anUsersToRemove":Ljava/lang/Object;
    :cond_a
    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->clear()V

    .line 231
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/core/util/MemoryManager;->delegates:Ljava/util/ArrayList;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Ljava/util/ArrayList;->size()I

    move-result v19

    add-int/lit8 v8, v19, -0x1

    :goto_6
    if-ltz v8, :cond_0

    .line 232
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/core/util/MemoryManager;->delegates:Ljava/util/ArrayList;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lorg/icepdf/core/util/MemoryManagerDelegate;

    .line 233
    .local v14, "mmd":Lorg/icepdf/core/util/MemoryManagerDelegate;
    const/16 v16, 0x0

    .line 234
    .local v16, "shouldRemove":Z
    if-nez v14, :cond_d

    .line 235
    const/16 v16, 0x1

    .line 245
    :cond_b
    :goto_7
    if-eqz v16, :cond_c

    .line 246
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/core/util/MemoryManager;->delegates:Ljava/util/ArrayList;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 231
    :cond_c
    add-int/lit8 v8, v8, -0x1

    goto :goto_6

    .line 237
    :cond_d
    invoke-interface {v14}, Lorg/icepdf/core/util/MemoryManagerDelegate;->getLibrary()Lorg/icepdf/core/util/Library;

    move-result-object v12

    .line 238
    .restart local v12    # "lib":Lorg/icepdf/core/util/Library;
    if-nez v12, :cond_e

    .line 239
    const/16 v16, 0x1

    goto :goto_7

    .line 241
    :cond_e
    move-object/from16 v0, p1

    invoke-virtual {v12, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v19

    if-eqz v19, :cond_b

    .line 242
    const/16 v16, 0x1

    goto :goto_7
.end method

.method public setMinMemory(J)V
    .locals 1
    .param p1, "m"    # J

    .prologue
    .line 362
    iput-wide p1, p0, Lorg/icepdf/core/util/MemoryManager;->minMemory:J

    .line 363
    return-void
.end method
