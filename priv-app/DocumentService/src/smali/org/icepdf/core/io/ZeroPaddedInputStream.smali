.class public Lorg/icepdf/core/io/ZeroPaddedInputStream;
.super Ljava/io/InputStream;
.source "ZeroPaddedInputStream.java"


# instance fields
.field private in:Ljava/io/InputStream;


# direct methods
.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 0
    .param p1, "in"    # Ljava/io/InputStream;

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    .line 36
    iput-object p1, p0, Lorg/icepdf/core/io/ZeroPaddedInputStream;->in:Ljava/io/InputStream;

    .line 37
    return-void
.end method


# virtual methods
.method public available()I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 79
    iget-object v1, p0, Lorg/icepdf/core/io/ZeroPaddedInputStream;->in:Ljava/io/InputStream;

    invoke-virtual {v1}, Ljava/io/InputStream;->available()I

    move-result v0

    .line 81
    .local v0, "a":I
    if-gtz v0, :cond_0

    .line 82
    const/4 v0, 0x1

    .line 83
    :cond_0
    return v0
.end method

.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 75
    iget-object v0, p0, Lorg/icepdf/core/io/ZeroPaddedInputStream;->in:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    .line 76
    return-void
.end method

.method public mark(I)V
    .locals 1
    .param p1, "readLimit"    # I

    .prologue
    .line 87
    iget-object v0, p0, Lorg/icepdf/core/io/ZeroPaddedInputStream;->in:Ljava/io/InputStream;

    invoke-virtual {v0, p1}, Ljava/io/InputStream;->mark(I)V

    .line 88
    return-void
.end method

.method public markSupported()Z
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lorg/icepdf/core/io/ZeroPaddedInputStream;->in:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->markSupported()Z

    move-result v0

    return v0
.end method

.method public read()I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 43
    iget-object v1, p0, Lorg/icepdf/core/io/ZeroPaddedInputStream;->in:Ljava/io/InputStream;

    invoke-virtual {v1}, Ljava/io/InputStream;->read()I

    move-result v0

    .line 45
    .local v0, "r":I
    if-gez v0, :cond_0

    .line 46
    const/4 v0, 0x0

    .line 47
    .end local v0    # "r":I
    :cond_0
    return v0
.end method

.method public read([B)I
    .locals 2
    .param p1, "buffer"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 51
    const/4 v0, 0x0

    array-length v1, p1

    invoke-virtual {p0, p1, v0, v1}, Lorg/icepdf/core/io/ZeroPaddedInputStream;->read([BII)I

    move-result v0

    return v0
.end method

.method public read([BII)I
    .locals 5
    .param p1, "buffer"    # [B
    .param p2, "offset"    # I
    .param p3, "length"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 55
    iget-object v3, p0, Lorg/icepdf/core/io/ZeroPaddedInputStream;->in:Ljava/io/InputStream;

    invoke-virtual {v3, p1, p2, p3}, Ljava/io/InputStream;->read([BII)I

    move-result v2

    .line 57
    .local v2, "readIn":I
    if-ge v2, p3, :cond_2

    .line 62
    if-lez v2, :cond_3

    .line 63
    :goto_0
    add-int v3, p2, v2

    add-int/lit8 v3, v3, -0x1

    aget-byte v3, p1, v3

    const/16 v4, 0xa

    if-eq v3, v4, :cond_0

    add-int v3, p2, v2

    add-int/lit8 v3, v3, -0x1

    aget-byte v3, p1, v3

    const/16 v4, 0xd

    if-eq v3, v4, :cond_0

    add-int v3, p2, v2

    add-int/lit8 v3, v3, -0x1

    aget-byte v3, p1, v3

    const/16 v4, 0x20

    if-ne v3, v4, :cond_1

    :cond_0
    if-lez v2, :cond_1

    .line 64
    add-int/lit8 v2, v2, -0x1

    goto :goto_0

    :cond_1
    move p3, v2

    .line 71
    .end local p3    # "length":I
    :cond_2
    return p3

    .line 67
    .restart local p3    # "length":I
    :cond_3
    add-int v1, p2, p3

    .line 68
    .local v1, "end":I
    invoke-static {v4, v2}, Ljava/lang/Math;->max(II)I

    move-result v3

    add-int v0, p2, v3

    .local v0, "current":I
    :goto_1
    if-ge v0, v1, :cond_2

    .line 69
    aput-byte v4, p1, v0

    .line 68
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public reset()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 95
    iget-object v0, p0, Lorg/icepdf/core/io/ZeroPaddedInputStream;->in:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->reset()V

    .line 96
    return-void
.end method

.method public skip(J)J
    .locals 3
    .param p1, "n"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 99
    iget-object v2, p0, Lorg/icepdf/core/io/ZeroPaddedInputStream;->in:Ljava/io/InputStream;

    invoke-virtual {v2, p1, p2}, Ljava/io/InputStream;->skip(J)J

    move-result-wide v0

    .line 101
    .local v0, "s":J
    return-wide v0
.end method
