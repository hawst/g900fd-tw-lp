.class public Lorg/icepdf/core/io/BitStream;
.super Ljava/lang/Object;
.source "BitStream.java"


# static fields
.field private static final masks:[I


# instance fields
.field bits:I

.field bits_left:I

.field in:Ljava/io/InputStream;

.field out:Ljava/io/OutputStream;

.field readEOF:Z


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/16 v3, 0x20

    .line 44
    new-array v1, v3, [I

    sput-object v1, Lorg/icepdf/core/io/BitStream;->masks:[I

    .line 47
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v3, :cond_0

    .line 48
    sget-object v1, Lorg/icepdf/core/io/BitStream;->masks:[I

    const/4 v2, 0x1

    shl-int/2addr v2, v0

    add-int/lit8 v2, v2, -0x1

    aput v2, v1, v0

    .line 47
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 50
    :cond_0
    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 1
    .param p1, "i"    # Ljava/io/InputStream;

    .prologue
    const/4 v0, 0x0

    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    iput-object p1, p0, Lorg/icepdf/core/io/BitStream;->in:Ljava/io/InputStream;

    .line 60
    iput v0, p0, Lorg/icepdf/core/io/BitStream;->bits:I

    .line 61
    iput v0, p0, Lorg/icepdf/core/io/BitStream;->bits_left:I

    .line 62
    iput-boolean v0, p0, Lorg/icepdf/core/io/BitStream;->readEOF:Z

    .line 63
    return-void
.end method

.method public constructor <init>(Ljava/io/OutputStream;)V
    .locals 1
    .param p1, "o"    # Ljava/io/OutputStream;

    .prologue
    const/4 v0, 0x0

    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 72
    iput-object p1, p0, Lorg/icepdf/core/io/BitStream;->out:Ljava/io/OutputStream;

    .line 73
    iput v0, p0, Lorg/icepdf/core/io/BitStream;->bits:I

    .line 74
    iput v0, p0, Lorg/icepdf/core/io/BitStream;->bits_left:I

    .line 75
    iput-boolean v0, p0, Lorg/icepdf/core/io/BitStream;->readEOF:Z

    .line 76
    return-void
.end method


# virtual methods
.method public atEndOfFile()Z
    .locals 1

    .prologue
    .line 116
    iget-boolean v0, p0, Lorg/icepdf/core/io/BitStream;->readEOF:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lorg/icepdf/core/io/BitStream;->bits_left:I

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public available()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 159
    iget v0, p0, Lorg/icepdf/core/io/BitStream;->bits_left:I

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/icepdf/core/io/BitStream;->in:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->available()I

    move-result v0

    if-gtz v0, :cond_0

    .line 160
    const/4 v0, 0x0

    .line 161
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 86
    iget-object v0, p0, Lorg/icepdf/core/io/BitStream;->in:Ljava/io/InputStream;

    if-eqz v0, :cond_0

    .line 87
    iget-object v0, p0, Lorg/icepdf/core/io/BitStream;->in:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    .line 89
    :cond_0
    iget-object v0, p0, Lorg/icepdf/core/io/BitStream;->out:Ljava/io/OutputStream;

    if-eqz v0, :cond_1

    .line 90
    iget-object v0, p0, Lorg/icepdf/core/io/BitStream;->out:Ljava/io/OutputStream;

    invoke-virtual {v0}, Ljava/io/OutputStream;->flush()V

    .line 91
    iget-object v0, p0, Lorg/icepdf/core/io/BitStream;->out:Ljava/io/OutputStream;

    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V

    .line 93
    :cond_1
    return-void
.end method

.method public getBits(I)I
    .locals 3
    .param p1, "i"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 101
    :goto_0
    iget v1, p0, Lorg/icepdf/core/io/BitStream;->bits_left:I

    if-ge v1, p1, :cond_0

    .line 102
    iget-object v1, p0, Lorg/icepdf/core/io/BitStream;->in:Ljava/io/InputStream;

    invoke-virtual {v1}, Ljava/io/InputStream;->read()I

    move-result v0

    .line 103
    .local v0, "r":I
    if-gez v0, :cond_1

    .line 104
    const/4 v1, 0x1

    iput-boolean v1, p0, Lorg/icepdf/core/io/BitStream;->readEOF:Z

    .line 111
    .end local v0    # "r":I
    :cond_0
    iget v1, p0, Lorg/icepdf/core/io/BitStream;->bits_left:I

    sub-int/2addr v1, p1

    iput v1, p0, Lorg/icepdf/core/io/BitStream;->bits_left:I

    .line 112
    iget v1, p0, Lorg/icepdf/core/io/BitStream;->bits:I

    iget v2, p0, Lorg/icepdf/core/io/BitStream;->bits_left:I

    shr-int/2addr v1, v2

    sget-object v2, Lorg/icepdf/core/io/BitStream;->masks:[I

    aget v2, v2, p1

    and-int/2addr v1, v2

    return v1

    .line 107
    .restart local v0    # "r":I
    :cond_1
    iget v1, p0, Lorg/icepdf/core/io/BitStream;->bits:I

    shl-int/lit8 v1, v1, 0x8

    iput v1, p0, Lorg/icepdf/core/io/BitStream;->bits:I

    .line 108
    iget v1, p0, Lorg/icepdf/core/io/BitStream;->bits:I

    and-int/lit16 v2, v0, 0xff

    or-int/2addr v1, v2

    iput v1, p0, Lorg/icepdf/core/io/BitStream;->bits:I

    .line 109
    iget v1, p0, Lorg/icepdf/core/io/BitStream;->bits_left:I

    add-int/lit8 v1, v1, 0x8

    iput v1, p0, Lorg/icepdf/core/io/BitStream;->bits_left:I

    goto :goto_0
.end method

.method public putBit(I)V
    .locals 3
    .param p1, "i"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 124
    iget v0, p0, Lorg/icepdf/core/io/BitStream;->bits:I

    shl-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/icepdf/core/io/BitStream;->bits:I

    .line 125
    iget v0, p0, Lorg/icepdf/core/io/BitStream;->bits:I

    or-int/2addr v0, p1

    iput v0, p0, Lorg/icepdf/core/io/BitStream;->bits:I

    .line 126
    iget v0, p0, Lorg/icepdf/core/io/BitStream;->bits_left:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/icepdf/core/io/BitStream;->bits_left:I

    .line 127
    iget v0, p0, Lorg/icepdf/core/io/BitStream;->bits_left:I

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    .line 128
    iget-object v0, p0, Lorg/icepdf/core/io/BitStream;->out:Ljava/io/OutputStream;

    iget v1, p0, Lorg/icepdf/core/io/BitStream;->bits:I

    invoke-virtual {v0, v1}, Ljava/io/OutputStream;->write(I)V

    .line 129
    iput v2, p0, Lorg/icepdf/core/io/BitStream;->bits:I

    .line 130
    iput v2, p0, Lorg/icepdf/core/io/BitStream;->bits_left:I

    .line 132
    :cond_0
    return-void
.end method

.method public putRunBits(II)V
    .locals 3
    .param p1, "i"    # I
    .param p2, "len"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 140
    add-int/lit8 v0, p2, -0x1

    .local v0, "j":I
    :goto_0
    if-ltz v0, :cond_3

    .line 141
    iget v1, p0, Lorg/icepdf/core/io/BitStream;->bits_left:I

    if-nez v1, :cond_0

    const/16 v1, 0x8

    if-ge v0, v1, :cond_1

    .line 142
    :cond_0
    invoke-virtual {p0, p1}, Lorg/icepdf/core/io/BitStream;->putBit(I)V

    .line 143
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 145
    :cond_1
    if-nez p1, :cond_2

    .line 146
    iget-object v1, p0, Lorg/icepdf/core/io/BitStream;->out:Ljava/io/OutputStream;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/io/OutputStream;->write(I)V

    .line 149
    :goto_1
    add-int/lit8 v0, v0, -0x8

    goto :goto_0

    .line 148
    :cond_2
    iget-object v1, p0, Lorg/icepdf/core/io/BitStream;->out:Ljava/io/OutputStream;

    const/16 v2, 0xff

    invoke-virtual {v1, v2}, Ljava/io/OutputStream;->write(I)V

    goto :goto_1

    .line 152
    :cond_3
    return-void
.end method

.method public skipByte()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 168
    iput v0, p0, Lorg/icepdf/core/io/BitStream;->bits_left:I

    .line 169
    iput v0, p0, Lorg/icepdf/core/io/BitStream;->bits:I

    .line 170
    return-void
.end method
