.class public Lorg/icepdf/core/io/SeekableByteArrayInputStream;
.super Ljava/io/ByteArrayInputStream;
.source "SeekableByteArrayInputStream.java"

# interfaces
.implements Lorg/icepdf/core/io/SeekableInput;


# static fields
.field private static final log:Ljava/util/logging/Logger;


# instance fields
.field private m_iBeginningOffset:I

.field private m_oCurrentUser:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const-class v0, Lorg/icepdf/core/io/SeekableByteArrayInputStream;

    invoke-virtual {v0}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Lorg/icepdf/core/io/SeekableByteArrayInputStream;->log:Ljava/util/logging/Logger;

    return-void
.end method

.method public constructor <init>([B)V
    .locals 1
    .param p1, "buf"    # [B

    .prologue
    .line 36
    invoke-direct {p0, p1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 37
    const/4 v0, 0x0

    iput v0, p0, Lorg/icepdf/core/io/SeekableByteArrayInputStream;->m_iBeginningOffset:I

    .line 39
    return-void
.end method

.method public constructor <init>([BII)V
    .locals 0
    .param p1, "buf"    # [B
    .param p2, "offset"    # I
    .param p3, "length"    # I

    .prologue
    .line 42
    invoke-direct {p0, p1, p2, p3}, Ljava/io/ByteArrayInputStream;-><init>([BII)V

    .line 43
    iput p2, p0, Lorg/icepdf/core/io/SeekableByteArrayInputStream;->m_iBeginningOffset:I

    .line 45
    return-void
.end method


# virtual methods
.method public declared-synchronized beginThreadAccess()V
    .locals 4

    .prologue
    .line 86
    monitor-enter p0

    :try_start_0
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    .line 88
    .local v0, "requestingUser":Ljava/lang/Thread;
    :goto_0
    iget-object v1, p0, Lorg/icepdf/core/io/SeekableByteArrayInputStream;->m_oCurrentUser:Ljava/lang/Object;

    if-nez v1, :cond_1

    .line 89
    iput-object v0, p0, Lorg/icepdf/core/io/SeekableByteArrayInputStream;->m_oCurrentUser:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 101
    :cond_0
    monitor-exit p0

    return-void

    .line 91
    :cond_1
    :try_start_1
    iget-object v1, p0, Lorg/icepdf/core/io/SeekableByteArrayInputStream;->m_oCurrentUser:Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eq v1, v0, :cond_0

    .line 95
    const-wide/16 v2, 0x64

    :try_start_2
    invoke-virtual {p0, v2, v3}, Ljava/lang/Object;->wait(J)V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 97
    :catch_0
    move-exception v1

    goto :goto_0

    .line 86
    .end local v0    # "requestingUser":Ljava/lang/Thread;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized endThreadAccess()V
    .locals 4

    .prologue
    .line 104
    monitor-enter p0

    :try_start_0
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    .line 105
    .local v0, "requestingUser":Ljava/lang/Thread;
    iget-object v1, p0, Lorg/icepdf/core/io/SeekableByteArrayInputStream;->m_oCurrentUser:Ljava/lang/Object;

    if-nez v1, :cond_1

    .line 106
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 119
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 107
    :cond_1
    :try_start_1
    iget-object v1, p0, Lorg/icepdf/core/io/SeekableByteArrayInputStream;->m_oCurrentUser:Ljava/lang/Object;

    if-ne v1, v0, :cond_2

    .line 108
    const/4 v1, 0x0

    iput-object v1, p0, Lorg/icepdf/core/io/SeekableByteArrayInputStream;->m_oCurrentUser:Ljava/lang/Object;

    .line 109
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 104
    .end local v0    # "requestingUser":Ljava/lang/Thread;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 111
    .restart local v0    # "requestingUser":Ljava/lang/Thread;
    :cond_2
    :try_start_2
    sget-object v1, Lorg/icepdf/core/io/SeekableByteArrayInputStream;->log:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    invoke-virtual {v1, v2}, Ljava/util/logging/Logger;->isLoggable(Ljava/util/logging/Level;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 112
    sget-object v1, Lorg/icepdf/core/io/SeekableByteArrayInputStream;->log:Ljava/util/logging/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "ERROR:  Thread finished using SeekableInput, but it wasn\'t locked by that Thread\n        Thread: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "        Locking Thread: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lorg/icepdf/core/io/SeekableByteArrayInputStream;->m_oCurrentUser:Ljava/lang/Object;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "        SeekableInput: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/logging/Logger;->severe(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public getAbsolutePosition()J
    .locals 6

    .prologue
    .line 71
    iget v1, p0, Lorg/icepdf/core/io/SeekableByteArrayInputStream;->pos:I

    iget v2, p0, Lorg/icepdf/core/io/SeekableByteArrayInputStream;->m_iBeginningOffset:I

    sub-int v0, v1, v2

    .line 72
    .local v0, "absPos":I
    int-to-long v2, v0

    const-wide/16 v4, -0x1

    and-long/2addr v2, v4

    return-wide v2
.end method

.method public getInputStream()Ljava/io/InputStream;
    .locals 0

    .prologue
    .line 81
    return-object p0
.end method

.method public getLength()J
    .locals 6

    .prologue
    .line 76
    iget v1, p0, Lorg/icepdf/core/io/SeekableByteArrayInputStream;->count:I

    iget v2, p0, Lorg/icepdf/core/io/SeekableByteArrayInputStream;->m_iBeginningOffset:I

    sub-int v0, v1, v2

    .line 77
    .local v0, "len":I
    int-to-long v2, v0

    const-wide/16 v4, -0x1

    and-long/2addr v2, v4

    return-wide v2
.end method

.method public seekAbsolute(J)V
    .locals 5
    .param p1, "absolutePosition"    # J

    .prologue
    .line 54
    const-wide/16 v2, -0x1

    and-long/2addr v2, p1

    long-to-int v0, v2

    .line 55
    .local v0, "absPos":I
    iget v1, p0, Lorg/icepdf/core/io/SeekableByteArrayInputStream;->m_iBeginningOffset:I

    add-int/2addr v1, v0

    iput v1, p0, Lorg/icepdf/core/io/SeekableByteArrayInputStream;->pos:I

    .line 56
    return-void
.end method

.method public seekEnd()V
    .locals 2

    .prologue
    .line 67
    invoke-virtual {p0}, Lorg/icepdf/core/io/SeekableByteArrayInputStream;->getLength()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lorg/icepdf/core/io/SeekableByteArrayInputStream;->seekAbsolute(J)V

    .line 68
    return-void
.end method

.method public seekRelative(J)V
    .locals 5
    .param p1, "relativeOffset"    # J

    .prologue
    .line 59
    const-wide/16 v2, -0x1

    and-long/2addr v2, p1

    long-to-int v1, v2

    .line 60
    .local v1, "relOff":I
    iget v2, p0, Lorg/icepdf/core/io/SeekableByteArrayInputStream;->pos:I

    add-int v0, v2, v1

    .line 61
    .local v0, "currPos":I
    iget v2, p0, Lorg/icepdf/core/io/SeekableByteArrayInputStream;->m_iBeginningOffset:I

    if-ge v0, v2, :cond_0

    .line 62
    iget v0, p0, Lorg/icepdf/core/io/SeekableByteArrayInputStream;->m_iBeginningOffset:I

    .line 63
    :cond_0
    iput v0, p0, Lorg/icepdf/core/io/SeekableByteArrayInputStream;->pos:I

    .line 64
    return-void
.end method
