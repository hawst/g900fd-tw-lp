.class public Lorg/icepdf/core/io/RandomAccessFileInputStream;
.super Ljava/io/InputStream;
.source "RandomAccessFileInputStream.java"

# interfaces
.implements Lorg/icepdf/core/io/SeekableInput;


# static fields
.field private static final logger:Ljava/util/logging/Logger;


# instance fields
.field private m_RandomAccessFile:Ljava/io/RandomAccessFile;

.field private m_lMarkPosition:J

.field private m_oCurrentUser:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const-class v0, Lorg/icepdf/core/io/RandomAccessFileInputStream;

    invoke-virtual {v0}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Lorg/icepdf/core/io/RandomAccessFileInputStream;->logger:Ljava/util/logging/Logger;

    return-void
.end method

.method protected constructor <init>(Ljava/io/RandomAccessFile;)V
    .locals 2
    .param p1, "raf"    # Ljava/io/RandomAccessFile;

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    .line 42
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lorg/icepdf/core/io/RandomAccessFileInputStream;->m_lMarkPosition:J

    .line 43
    iput-object p1, p0, Lorg/icepdf/core/io/RandomAccessFileInputStream;->m_RandomAccessFile:Ljava/io/RandomAccessFile;

    .line 44
    return-void
.end method

.method public static build(Ljava/io/File;)Lorg/icepdf/core/io/RandomAccessFileInputStream;
    .locals 3
    .param p0, "file"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .prologue
    .line 35
    new-instance v0, Ljava/io/RandomAccessFile;

    const-string/jumbo v2, "r"

    invoke-direct {v0, p0, v2}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 36
    .local v0, "raf":Ljava/io/RandomAccessFile;
    new-instance v1, Lorg/icepdf/core/io/RandomAccessFileInputStream;

    invoke-direct {v1, v0}, Lorg/icepdf/core/io/RandomAccessFileInputStream;-><init>(Ljava/io/RandomAccessFile;)V

    .line 37
    .local v1, "rafis":Lorg/icepdf/core/io/RandomAccessFileInputStream;
    return-object v1
.end method


# virtual methods
.method public available()I
    .locals 1

    .prologue
    .line 68
    const/4 v0, 0x0

    return v0
.end method

.method public declared-synchronized beginThreadAccess()V
    .locals 4

    .prologue
    .line 130
    monitor-enter p0

    :try_start_0
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    .line 132
    .local v0, "requestingUser":Ljava/lang/Thread;
    :goto_0
    iget-object v1, p0, Lorg/icepdf/core/io/RandomAccessFileInputStream;->m_oCurrentUser:Ljava/lang/Object;

    if-nez v1, :cond_1

    .line 133
    iput-object v0, p0, Lorg/icepdf/core/io/RandomAccessFileInputStream;->m_oCurrentUser:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 145
    :cond_0
    monitor-exit p0

    return-void

    .line 135
    :cond_1
    :try_start_1
    iget-object v1, p0, Lorg/icepdf/core/io/RandomAccessFileInputStream;->m_oCurrentUser:Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eq v1, v0, :cond_0

    .line 139
    const-wide/16 v2, 0x64

    :try_start_2
    invoke-virtual {p0, v2, v3}, Ljava/lang/Object;->wait(J)V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 141
    :catch_0
    move-exception v1

    goto :goto_0

    .line 130
    .end local v0    # "requestingUser":Ljava/lang/Thread;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 64
    iget-object v0, p0, Lorg/icepdf/core/io/RandomAccessFileInputStream;->m_RandomAccessFile:Ljava/io/RandomAccessFile;

    invoke-virtual {v0}, Ljava/io/RandomAccessFile;->close()V

    .line 65
    return-void
.end method

.method public declared-synchronized endThreadAccess()V
    .locals 4

    .prologue
    .line 149
    monitor-enter p0

    :try_start_0
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    .line 150
    .local v0, "requestingUser":Ljava/lang/Thread;
    iget-object v1, p0, Lorg/icepdf/core/io/RandomAccessFileInputStream;->m_oCurrentUser:Ljava/lang/Object;

    if-nez v1, :cond_1

    .line 151
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 164
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 152
    :cond_1
    :try_start_1
    iget-object v1, p0, Lorg/icepdf/core/io/RandomAccessFileInputStream;->m_oCurrentUser:Ljava/lang/Object;

    if-ne v1, v0, :cond_2

    .line 153
    const/4 v1, 0x0

    iput-object v1, p0, Lorg/icepdf/core/io/RandomAccessFileInputStream;->m_oCurrentUser:Ljava/lang/Object;

    .line 154
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 149
    .end local v0    # "requestingUser":Ljava/lang/Thread;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 156
    .restart local v0    # "requestingUser":Ljava/lang/Thread;
    :cond_2
    :try_start_2
    sget-object v1, Lorg/icepdf/core/io/RandomAccessFileInputStream;->logger:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    invoke-virtual {v1, v2}, Ljava/util/logging/Logger;->isLoggable(Ljava/util/logging/Level;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 157
    sget-object v1, Lorg/icepdf/core/io/RandomAccessFileInputStream;->logger:Ljava/util/logging/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "ERROR:  Thread finished using SeekableInput, but it wasn\'t locked by that Thread\n        Thread: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "        Locking Thread: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lorg/icepdf/core/io/RandomAccessFileInputStream;->m_oCurrentUser:Ljava/lang/Object;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "        SeekableInput: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/logging/Logger;->severe(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public getAbsolutePosition()J
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 117
    iget-object v0, p0, Lorg/icepdf/core/io/RandomAccessFileInputStream;->m_RandomAccessFile:Ljava/io/RandomAccessFile;

    invoke-virtual {v0}, Ljava/io/RandomAccessFile;->getFilePointer()J

    move-result-wide v0

    return-wide v0
.end method

.method public getInputStream()Ljava/io/InputStream;
    .locals 0

    .prologue
    .line 125
    return-object p0
.end method

.method public getLength()J
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 121
    iget-object v0, p0, Lorg/icepdf/core/io/RandomAccessFileInputStream;->m_RandomAccessFile:Ljava/io/RandomAccessFile;

    invoke-virtual {v0}, Ljava/io/RandomAccessFile;->length()J

    move-result-wide v0

    return-wide v0
.end method

.method public mark(I)V
    .locals 4
    .param p1, "readLimit"    # I

    .prologue
    .line 73
    :try_start_0
    iget-object v1, p0, Lorg/icepdf/core/io/RandomAccessFileInputStream;->m_RandomAccessFile:Ljava/io/RandomAccessFile;

    invoke-virtual {v1}, Ljava/io/RandomAccessFile;->getFilePointer()J

    move-result-wide v2

    iput-wide v2, p0, Lorg/icepdf/core/io/RandomAccessFileInputStream;->m_lMarkPosition:J
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 78
    return-void

    .line 75
    :catch_0
    move-exception v0

    .line 76
    .local v0, "e":Ljava/io/IOException;
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public markSupported()Z
    .locals 1

    .prologue
    .line 81
    const/4 v0, 0x1

    return v0
.end method

.method public read()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 52
    iget-object v0, p0, Lorg/icepdf/core/io/RandomAccessFileInputStream;->m_RandomAccessFile:Ljava/io/RandomAccessFile;

    invoke-virtual {v0}, Ljava/io/RandomAccessFile;->read()I

    move-result v0

    return v0
.end method

.method public read([B)I
    .locals 1
    .param p1, "buffer"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 56
    iget-object v0, p0, Lorg/icepdf/core/io/RandomAccessFileInputStream;->m_RandomAccessFile:Ljava/io/RandomAccessFile;

    invoke-virtual {v0, p1}, Ljava/io/RandomAccessFile;->read([B)I

    move-result v0

    return v0
.end method

.method public read([BII)I
    .locals 1
    .param p1, "buffer"    # [B
    .param p2, "offset"    # I
    .param p3, "length"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 60
    iget-object v0, p0, Lorg/icepdf/core/io/RandomAccessFileInputStream;->m_RandomAccessFile:Ljava/io/RandomAccessFile;

    invoke-virtual {v0, p1, p2, p3}, Ljava/io/RandomAccessFile;->read([BII)I

    move-result v0

    return v0
.end method

.method public reset()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 85
    iget-object v0, p0, Lorg/icepdf/core/io/RandomAccessFileInputStream;->m_RandomAccessFile:Ljava/io/RandomAccessFile;

    iget-wide v2, p0, Lorg/icepdf/core/io/RandomAccessFileInputStream;->m_lMarkPosition:J

    invoke-virtual {v0, v2, v3}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 86
    return-void
.end method

.method public seekAbsolute(J)V
    .locals 1
    .param p1, "absolutePosition"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 100
    iget-object v0, p0, Lorg/icepdf/core/io/RandomAccessFileInputStream;->m_RandomAccessFile:Ljava/io/RandomAccessFile;

    invoke-virtual {v0, p1, p2}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 101
    return-void
.end method

.method public seekEnd()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 112
    iget-object v2, p0, Lorg/icepdf/core/io/RandomAccessFileInputStream;->m_RandomAccessFile:Ljava/io/RandomAccessFile;

    invoke-virtual {v2}, Ljava/io/RandomAccessFile;->length()J

    move-result-wide v0

    .line 113
    .local v0, "end":J
    invoke-virtual {p0, v0, v1}, Lorg/icepdf/core/io/RandomAccessFileInputStream;->seekAbsolute(J)V

    .line 114
    return-void
.end method

.method public seekRelative(J)V
    .locals 5
    .param p1, "relativeOffset"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 104
    iget-object v2, p0, Lorg/icepdf/core/io/RandomAccessFileInputStream;->m_RandomAccessFile:Ljava/io/RandomAccessFile;

    invoke-virtual {v2}, Ljava/io/RandomAccessFile;->getFilePointer()J

    move-result-wide v0

    .line 105
    .local v0, "pos":J
    add-long/2addr v0, p1

    .line 106
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-gez v2, :cond_0

    .line 107
    const-wide/16 v0, 0x0

    .line 108
    :cond_0
    iget-object v2, p0, Lorg/icepdf/core/io/RandomAccessFileInputStream;->m_RandomAccessFile:Ljava/io/RandomAccessFile;

    invoke-virtual {v2, v0, v1}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 109
    return-void
.end method

.method public skip(J)J
    .locals 5
    .param p1, "n"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 89
    const-wide/16 v2, -0x1

    and-long/2addr v2, p1

    long-to-int v0, v2

    .line 90
    .local v0, "nn":I
    iget-object v1, p0, Lorg/icepdf/core/io/RandomAccessFileInputStream;->m_RandomAccessFile:Ljava/io/RandomAccessFile;

    invoke-virtual {v1, v0}, Ljava/io/RandomAccessFile;->skipBytes(I)I

    move-result v1

    int-to-long v2, v1

    return-wide v2
.end method
