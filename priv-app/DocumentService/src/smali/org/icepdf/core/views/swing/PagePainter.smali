.class public Lorg/icepdf/core/views/swing/PagePainter;
.super Ljava/lang/Object;
.source "PagePainter.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private hasBeenQueued:Z

.field private isBufferyDirty:Z

.field private isLastPaintDirty:Z

.field private isRunning:Z

.field private final isRunningLock:Ljava/lang/Object;

.field private isStopRequested:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lorg/icepdf/core/views/swing/PagePainter;->isRunningLock:Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public hasBeenQueued()Z
    .locals 2

    .prologue
    .line 69
    iget-object v1, p0, Lorg/icepdf/core/views/swing/PagePainter;->isRunningLock:Ljava/lang/Object;

    monitor-enter v1

    .line 70
    :try_start_0
    iget-boolean v0, p0, Lorg/icepdf/core/views/swing/PagePainter;->hasBeenQueued:Z

    monitor-exit v1

    return v0

    .line 71
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public isBufferDirty()Z
    .locals 1

    .prologue
    .line 41
    iget-boolean v0, p0, Lorg/icepdf/core/views/swing/PagePainter;->isBufferyDirty:Z

    return v0
.end method

.method public declared-synchronized isLastPaintDirty()Z
    .locals 1

    .prologue
    .line 29
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lorg/icepdf/core/views/swing/PagePainter;->isLastPaintDirty:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public isRunning()Z
    .locals 2

    .prologue
    .line 79
    iget-object v1, p0, Lorg/icepdf/core/views/swing/PagePainter;->isRunningLock:Ljava/lang/Object;

    monitor-enter v1

    .line 80
    :try_start_0
    iget-boolean v0, p0, Lorg/icepdf/core/views/swing/PagePainter;->isRunning:Z

    monitor-exit v1

    return v0

    .line 81
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public isStopPaintingRequested()Z
    .locals 1

    .prologue
    .line 45
    iget-boolean v0, p0, Lorg/icepdf/core/views/swing/PagePainter;->isStopRequested:Z

    return v0
.end method

.method public run()V
    .locals 2

    .prologue
    .line 55
    iget-object v1, p0, Lorg/icepdf/core/views/swing/PagePainter;->isRunningLock:Ljava/lang/Object;

    monitor-enter v1

    .line 56
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lorg/icepdf/core/views/swing/PagePainter;->isRunning:Z

    .line 57
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/icepdf/core/views/swing/PagePainter;->hasBeenQueued:Z

    .line 58
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 62
    iget-object v1, p0, Lorg/icepdf/core/views/swing/PagePainter;->isRunningLock:Ljava/lang/Object;

    monitor-enter v1

    .line 63
    const/4 v0, 0x0

    :try_start_1
    iput-boolean v0, p0, Lorg/icepdf/core/views/swing/PagePainter;->isStopRequested:Z

    .line 64
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/icepdf/core/views/swing/PagePainter;->isRunning:Z

    .line 65
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 66
    return-void

    .line 58
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 65
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public setHasBeenQueued(Z)V
    .locals 0
    .param p1, "hasBeenQueued"    # Z

    .prologue
    .line 75
    iput-boolean p1, p0, Lorg/icepdf/core/views/swing/PagePainter;->hasBeenQueued:Z

    .line 76
    return-void
.end method

.method public setIsBufferDirty(Z)V
    .locals 0
    .param p1, "isDirty"    # Z

    .prologue
    .line 37
    iput-boolean p1, p0, Lorg/icepdf/core/views/swing/PagePainter;->isBufferyDirty:Z

    .line 38
    return-void
.end method

.method public setIsLastPaintDirty(Z)V
    .locals 0
    .param p1, "isDirty"    # Z

    .prologue
    .line 33
    iput-boolean p1, p0, Lorg/icepdf/core/views/swing/PagePainter;->isLastPaintDirty:Z

    .line 34
    return-void
.end method

.method public declared-synchronized stopPaintingPage()V
    .locals 1

    .prologue
    .line 50
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lorg/icepdf/core/views/swing/PagePainter;->isStopRequested:Z

    .line 51
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/icepdf/core/views/swing/PagePainter;->isLastPaintDirty:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 52
    monitor-exit p0

    return-void

    .line 50
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
