.class public Lorg/icepdf/core/pobjects/fonts/FontDescriptor;
.super Lorg/icepdf/core/pobjects/Dictionary;
.source "FontDescriptor.java"


# static fields
.field public static final ASCENT:Ljava/lang/String; = "Ascent"

.field public static final AVG_WIDTH:Ljava/lang/String; = "AvgWidth"

.field public static final CAP_HEIGHT:Ljava/lang/String; = "CapHeight"

.field public static final DESCENT:Ljava/lang/String; = "Descent"

.field public static final FLAGS:Ljava/lang/String; = "Flags"

.field public static final FONT_BBOX:Ljava/lang/String; = "FontBBox"

.field public static final FONT_FAMILY:Ljava/lang/String; = "FontFamily"

.field private static final FONT_FILE:Ljava/lang/String; = "FontFile"

.field private static final FONT_FILE_2:Ljava/lang/String; = "FontFile2"

.field private static final FONT_FILE_3:Ljava/lang/String; = "FontFile3"

.field public static final FONT_NAME:Ljava/lang/String; = "FontName"

.field public static final FONT_WEIGHT:Ljava/lang/String; = "FontWeight"

.field public static final ITALIC_ANGLE:Ljava/lang/String; = "ItalicAngle"

.field public static final LEADING:Ljava/lang/String; = "Leading"

.field public static final MAX_WIDTH:Ljava/lang/String; = "MaxWidth"

.field public static final MISSING_Stretch:Ljava/lang/String; = "FontStretch"

.field public static final MISSING_WIDTH:Ljava/lang/String; = "MissingWidth"

.field public static final STEM_H:Ljava/lang/String; = "StemH"

.field public static final STEM_V:Ljava/lang/String; = "StemV"

.field public static final WIDTH:Ljava/lang/String; = "Widths"

.field public static final X_HEIGHT:Ljava/lang/String; = "XHeight"


# instance fields
.field private font:Lorg/icepdf/core/pobjects/fonts/FontFile;


# direct methods
.method public constructor <init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V
    .locals 0
    .param p1, "l"    # Lorg/icepdf/core/util/Library;
    .param p2, "h"    # Ljava/util/Hashtable;

    .prologue
    .line 108
    invoke-direct {p0, p1, p2}, Lorg/icepdf/core/pobjects/Dictionary;-><init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V

    .line 109
    return-void
.end method

.method public static createDescriptor(Lorg/icepdf/core/util/Library;Lorg/icepdf/core/pobjects/fonts/DefaultFonts;)Lorg/icepdf/core/pobjects/fonts/FontDescriptor;
    .locals 3
    .param p0, "library"    # Lorg/icepdf/core/util/Library;
    .param p1, "defaultfont"    # Lorg/icepdf/core/pobjects/fonts/DefaultFonts;

    .prologue
    .line 121
    new-instance v0, Ljava/util/Hashtable;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Ljava/util/Hashtable;-><init>(I)V

    .line 122
    .local v0, "properties":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/String;Ljava/lang/Object;>;"
    const-string/jumbo v1, "FontName"

    invoke-virtual {p1}, Lorg/icepdf/core/pobjects/fonts/DefaultFonts;->getFontname()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 123
    const-string/jumbo v1, "Widths"

    invoke-virtual {p1}, Lorg/icepdf/core/pobjects/fonts/DefaultFonts;->getWidths()[F

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 124
    new-instance v1, Lorg/icepdf/core/pobjects/fonts/FontDescriptor;

    invoke-direct {v1, p0, v0}, Lorg/icepdf/core/pobjects/fonts/FontDescriptor;-><init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V

    return-object v1
.end method

.method private deleteTempFiles(Ljava/io/File;)Z
    .locals 8
    .param p1, "file"    # Ljava/io/File;

    .prologue
    .line 374
    const/4 v5, 0x0

    .line 375
    .local v5, "success":Z
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v7

    if-nez v7, :cond_1

    :cond_0
    move v6, v5

    .line 390
    .end local v5    # "success":Z
    .local v6, "success":I
    :goto_0
    return v6

    .line 378
    .end local v6    # "success":I
    .restart local v5    # "success":Z
    :cond_1
    invoke-virtual {p1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    .line 380
    .local v2, "filesLst":[Ljava/io/File;
    if-nez v2, :cond_2

    .line 381
    invoke-virtual {p1}, Ljava/io/File;->delete()Z

    move-result v5

    move v6, v5

    .line 382
    .restart local v6    # "success":I
    goto :goto_0

    .line 384
    .end local v6    # "success":I
    :cond_2
    move-object v0, v2

    .local v0, "arr$":[Ljava/io/File;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_1
    if-ge v3, v4, :cond_4

    aget-object v1, v0, v3

    .line 385
    .local v1, "currentFile":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->isFile()Z

    move-result v7

    if-eqz v7, :cond_3

    .line 386
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    move-result v5

    .line 384
    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 389
    .end local v1    # "currentFile":Ljava/io/File;
    :cond_4
    invoke-virtual {p1}, Ljava/io/File;->delete()Z

    move-result v5

    move v6, v5

    .line 390
    .restart local v6    # "success":I
    goto :goto_0
.end method


# virtual methods
.method public getAscent()F
    .locals 4

    .prologue
    .line 218
    iget-object v1, p0, Lorg/icepdf/core/pobjects/fonts/FontDescriptor;->library:Lorg/icepdf/core/util/Library;

    iget-object v2, p0, Lorg/icepdf/core/pobjects/fonts/FontDescriptor;->entries:Ljava/util/Hashtable;

    const-string/jumbo v3, "Ascent"

    invoke-virtual {v1, v2, v3}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 219
    .local v0, "value":Ljava/lang/Object;
    instance-of v1, v0, Ljava/lang/Number;

    if-eqz v1, :cond_0

    .line 220
    check-cast v0, Ljava/lang/Number;

    .end local v0    # "value":Ljava/lang/Object;
    invoke-virtual {v0}, Ljava/lang/Number;->floatValue()F

    move-result v1

    .line 222
    :goto_0
    return v1

    .restart local v0    # "value":Ljava/lang/Object;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getAverageWidth()F
    .locals 4

    .prologue
    .line 192
    iget-object v1, p0, Lorg/icepdf/core/pobjects/fonts/FontDescriptor;->library:Lorg/icepdf/core/util/Library;

    iget-object v2, p0, Lorg/icepdf/core/pobjects/fonts/FontDescriptor;->entries:Ljava/util/Hashtable;

    const-string/jumbo v3, "AvgWidth"

    invoke-virtual {v1, v2, v3}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 193
    .local v0, "value":Ljava/lang/Object;
    instance-of v1, v0, Ljava/lang/Number;

    if-eqz v1, :cond_0

    .line 194
    check-cast v0, Ljava/lang/Number;

    .end local v0    # "value":Ljava/lang/Object;
    invoke-virtual {v0}, Ljava/lang/Number;->floatValue()F

    move-result v1

    .line 196
    :goto_0
    return v1

    .restart local v0    # "value":Ljava/lang/Object;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getDescent()F
    .locals 4

    .prologue
    .line 231
    iget-object v1, p0, Lorg/icepdf/core/pobjects/fonts/FontDescriptor;->library:Lorg/icepdf/core/util/Library;

    iget-object v2, p0, Lorg/icepdf/core/pobjects/fonts/FontDescriptor;->entries:Ljava/util/Hashtable;

    const-string/jumbo v3, "Descent"

    invoke-virtual {v1, v2, v3}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 232
    .local v0, "value":Ljava/lang/Object;
    instance-of v1, v0, Ljava/lang/Number;

    if-eqz v1, :cond_0

    .line 233
    check-cast v0, Ljava/lang/Number;

    .end local v0    # "value":Ljava/lang/Object;
    invoke-virtual {v0}, Ljava/lang/Number;->floatValue()F

    move-result v1

    .line 235
    :goto_0
    return v1

    .restart local v0    # "value":Ljava/lang/Object;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getEmbeddedFont()Lorg/icepdf/core/pobjects/fonts/FontFile;
    .locals 1

    .prologue
    .line 244
    iget-object v0, p0, Lorg/icepdf/core/pobjects/fonts/FontDescriptor;->font:Lorg/icepdf/core/pobjects/fonts/FontFile;

    return-object v0
.end method

.method public getFlags()I
    .locals 4

    .prologue
    .line 269
    iget-object v1, p0, Lorg/icepdf/core/pobjects/fonts/FontDescriptor;->library:Lorg/icepdf/core/util/Library;

    iget-object v2, p0, Lorg/icepdf/core/pobjects/fonts/FontDescriptor;->entries:Ljava/util/Hashtable;

    const-string/jumbo v3, "Flags"

    invoke-virtual {v1, v2, v3}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 270
    .local v0, "value":Ljava/lang/Object;
    instance-of v1, v0, Ljava/lang/Number;

    if-eqz v1, :cond_0

    .line 271
    check-cast v0, Ljava/lang/Number;

    .end local v0    # "value":Ljava/lang/Object;
    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v1

    .line 273
    :goto_0
    return v1

    .restart local v0    # "value":Ljava/lang/Object;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getFontBBox()Lorg/icepdf/core/pobjects/PRectangle;
    .locals 5

    .prologue
    .line 253
    iget-object v2, p0, Lorg/icepdf/core/pobjects/fonts/FontDescriptor;->library:Lorg/icepdf/core/util/Library;

    iget-object v3, p0, Lorg/icepdf/core/pobjects/fonts/FontDescriptor;->entries:Ljava/util/Hashtable;

    const-string/jumbo v4, "FontBBox"

    invoke-virtual {v2, v3, v4}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    .line 254
    .local v1, "value":Ljava/lang/Object;
    instance-of v2, v1, Ljava/util/Vector;

    if-eqz v2, :cond_0

    move-object v0, v1

    .line 255
    check-cast v0, Ljava/util/Vector;

    .line 256
    .local v0, "rectangle":Ljava/util/Vector;
    new-instance v2, Lorg/icepdf/core/pobjects/PRectangle;

    invoke-direct {v2, v0}, Lorg/icepdf/core/pobjects/PRectangle;-><init>(Ljava/util/Vector;)V

    .line 258
    .end local v0    # "rectangle":Ljava/util/Vector;
    :goto_0
    return-object v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public getFontFamily()Ljava/lang/String;
    .locals 5

    .prologue
    .line 150
    iget-object v2, p0, Lorg/icepdf/core/pobjects/fonts/FontDescriptor;->library:Lorg/icepdf/core/util/Library;

    iget-object v3, p0, Lorg/icepdf/core/pobjects/fonts/FontDescriptor;->entries:Ljava/util/Hashtable;

    const-string/jumbo v4, "FontFamily"

    invoke-virtual {v2, v3, v4}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    .line 151
    .local v1, "value":Ljava/lang/Object;
    instance-of v2, v1, Lorg/icepdf/core/pobjects/StringObject;

    if-eqz v2, :cond_0

    move-object v0, v1

    .line 152
    check-cast v0, Lorg/icepdf/core/pobjects/StringObject;

    .line 153
    .local v0, "familyName":Lorg/icepdf/core/pobjects/StringObject;
    iget-object v2, p0, Lorg/icepdf/core/pobjects/fonts/FontDescriptor;->library:Lorg/icepdf/core/util/Library;

    invoke-virtual {v2}, Lorg/icepdf/core/util/Library;->getSecurityManager()Lorg/icepdf/core/pobjects/security/SecurityManager;

    move-result-object v2

    invoke-interface {v0, v2}, Lorg/icepdf/core/pobjects/StringObject;->getDecryptedLiteralString(Lorg/icepdf/core/pobjects/security/SecurityManager;)Ljava/lang/String;

    move-result-object v2

    .line 155
    .end local v0    # "familyName":Lorg/icepdf/core/pobjects/StringObject;
    :goto_0
    return-object v2

    :cond_0
    const-string/jumbo v2, "FontName"

    goto :goto_0
.end method

.method public getFontName()Ljava/lang/String;
    .locals 4

    .prologue
    .line 133
    iget-object v1, p0, Lorg/icepdf/core/pobjects/fonts/FontDescriptor;->library:Lorg/icepdf/core/util/Library;

    iget-object v2, p0, Lorg/icepdf/core/pobjects/fonts/FontDescriptor;->entries:Ljava/util/Hashtable;

    const-string/jumbo v3, "FontName"

    invoke-virtual {v1, v2, v3}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 134
    .local v0, "value":Ljava/lang/Object;
    instance-of v1, v0, Lorg/icepdf/core/pobjects/Name;

    if-eqz v1, :cond_0

    .line 135
    check-cast v0, Lorg/icepdf/core/pobjects/Name;

    .end local v0    # "value":Ljava/lang/Object;
    invoke-virtual {v0}, Lorg/icepdf/core/pobjects/Name;->getName()Ljava/lang/String;

    move-result-object v0

    .line 140
    :goto_0
    return-object v0

    .line 137
    .restart local v0    # "value":Ljava/lang/Object;
    :cond_0
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 138
    check-cast v0, Ljava/lang/String;

    goto :goto_0

    .line 140
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getFontWeight()F
    .locals 4

    .prologue
    .line 165
    iget-object v1, p0, Lorg/icepdf/core/pobjects/fonts/FontDescriptor;->library:Lorg/icepdf/core/util/Library;

    iget-object v2, p0, Lorg/icepdf/core/pobjects/fonts/FontDescriptor;->entries:Ljava/util/Hashtable;

    const-string/jumbo v3, "FontWeight"

    invoke-virtual {v1, v2, v3}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 166
    .local v0, "value":Ljava/lang/Object;
    instance-of v1, v0, Ljava/lang/Number;

    if-eqz v1, :cond_0

    .line 167
    check-cast v0, Ljava/lang/Number;

    .end local v0    # "value":Ljava/lang/Object;
    invoke-virtual {v0}, Ljava/lang/Number;->floatValue()F

    move-result v1

    .line 169
    :goto_0
    return v1

    .restart local v0    # "value":Ljava/lang/Object;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getMaxWidth()F
    .locals 4

    .prologue
    .line 205
    iget-object v1, p0, Lorg/icepdf/core/pobjects/fonts/FontDescriptor;->library:Lorg/icepdf/core/util/Library;

    iget-object v2, p0, Lorg/icepdf/core/pobjects/fonts/FontDescriptor;->entries:Ljava/util/Hashtable;

    const-string/jumbo v3, "MaxWidth"

    invoke-virtual {v1, v2, v3}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 206
    .local v0, "value":Ljava/lang/Object;
    instance-of v1, v0, Ljava/lang/Number;

    if-eqz v1, :cond_0

    .line 207
    check-cast v0, Ljava/lang/Number;

    .end local v0    # "value":Ljava/lang/Object;
    invoke-virtual {v0}, Ljava/lang/Number;->floatValue()F

    move-result v1

    .line 209
    :goto_0
    return v1

    .restart local v0    # "value":Ljava/lang/Object;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getMissingWidth()F
    .locals 4

    .prologue
    .line 179
    iget-object v1, p0, Lorg/icepdf/core/pobjects/fonts/FontDescriptor;->library:Lorg/icepdf/core/util/Library;

    iget-object v2, p0, Lorg/icepdf/core/pobjects/fonts/FontDescriptor;->entries:Ljava/util/Hashtable;

    const-string/jumbo v3, "MissingWidth"

    invoke-virtual {v1, v2, v3}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 180
    .local v0, "value":Ljava/lang/Object;
    instance-of v1, v0, Ljava/lang/Number;

    if-eqz v1, :cond_0

    .line 181
    check-cast v0, Ljava/lang/Number;

    .end local v0    # "value":Ljava/lang/Object;
    invoke-virtual {v0}, Ljava/lang/Number;->floatValue()F

    move-result v1

    .line 183
    :goto_0
    return v1

    .restart local v0    # "value":Ljava/lang/Object;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public init()V
    .locals 23

    .prologue
    .line 289
    const/4 v14, 0x0

    .line 291
    .local v14, "folderName":Ljava/io/File;
    const/4 v4, 0x0

    .line 292
    .local v4, "fontName":Ljava/lang/String;
    const/4 v15, 0x0

    .line 294
    .local v15, "fontStream":Lorg/icepdf/core/pobjects/Stream;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/icepdf/core/pobjects/fonts/FontDescriptor;->entries:Ljava/util/Hashtable;

    new-instance v5, Lorg/icepdf/core/pobjects/Name;

    const-string/jumbo v6, "FontFile2"

    invoke-direct {v5, v6}, Lorg/icepdf/core/pobjects/Name;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v5}, Ljava/util/Hashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 295
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/icepdf/core/pobjects/fonts/FontDescriptor;->library:Lorg/icepdf/core/util/Library;

    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/icepdf/core/pobjects/fonts/FontDescriptor;->entries:Ljava/util/Hashtable;

    const-string/jumbo v6, "FontName"

    invoke-virtual {v2, v5, v6}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/icepdf/core/pobjects/Name;

    invoke-virtual {v2}, Lorg/icepdf/core/pobjects/Name;->toString()Ljava/lang/String;

    move-result-object v4

    .line 297
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/icepdf/core/pobjects/fonts/FontDescriptor;->library:Lorg/icepdf/core/util/Library;

    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/icepdf/core/pobjects/fonts/FontDescriptor;->entries:Ljava/util/Hashtable;

    const-string/jumbo v6, "FontFile2"

    invoke-virtual {v2, v5, v6}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Lorg/icepdf/core/pobjects/Stream;

    move-object v15, v0

    .line 307
    :cond_0
    :goto_0
    if-eqz v15, :cond_9

    .line 308
    invoke-static {}, Lcom/samsung/index/parser/ThumbnailDocumentParser;->getContext()Landroid/content/Context;

    move-result-object v10

    .line 309
    .local v10, "context":Landroid/content/Context;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, ""

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-virtual {v2, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v5, 0x1

    invoke-virtual {v10, v2, v5}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v14

    .line 312
    invoke-virtual {v15}, Lorg/icepdf/core/pobjects/Stream;->getInputStreamForDecodedStreamBytes()Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v17

    .line 314
    .local v17, "in":Ljava/io/InputStream;
    const/16 v20, 0x0

    .line 316
    .local v20, "outputStream":Ljava/io/FileOutputStream;
    :try_start_1
    new-instance v21, Ljava/io/FileOutputStream;

    new-instance v2, Ljava/io/File;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, ".ttf"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v2, v14, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    move-object/from16 v0, v21

    invoke-direct {v0, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_4

    .line 318
    .end local v20    # "outputStream":Ljava/io/FileOutputStream;
    .local v21, "outputStream":Ljava/io/FileOutputStream;
    const/16 v22, 0x0

    .line 319
    .local v22, "read":I
    const/16 v2, 0x400

    :try_start_2
    new-array v9, v2, [B

    .line 321
    .local v9, "bytes":[B
    :goto_1
    move-object/from16 v0, v17

    invoke-virtual {v0, v9}, Ljava/io/InputStream;->read([B)I

    move-result v22

    const/4 v2, -0x1

    move/from16 v0, v22

    if-eq v0, v2, :cond_4

    .line 322
    const/4 v2, 0x0

    move-object/from16 v0, v21

    move/from16 v1, v22

    invoke-virtual {v0, v9, v2, v1}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 326
    .end local v9    # "bytes":[B
    :catchall_0
    move-exception v2

    move-object/from16 v20, v21

    .line 327
    .end local v21    # "outputStream":Ljava/io/FileOutputStream;
    .end local v22    # "read":I
    .restart local v20    # "outputStream":Ljava/io/FileOutputStream;
    :goto_2
    :try_start_3
    invoke-virtual/range {v17 .. v17}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_6
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    .line 332
    if-eqz v20, :cond_1

    .line 335
    :try_start_4
    invoke-virtual/range {v20 .. v20}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_5
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 339
    :cond_1
    :goto_3
    :try_start_5
    throw v2
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 364
    .end local v10    # "context":Landroid/content/Context;
    .end local v17    # "in":Ljava/io/InputStream;
    .end local v20    # "outputStream":Ljava/io/FileOutputStream;
    :catch_0
    move-exception v12

    .line 365
    .local v12, "e":Ljava/lang/Throwable;
    :try_start_6
    const-string/jumbo v2, "FontDescriptor"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "Error loading embedded fonts:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 367
    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lorg/icepdf/core/pobjects/fonts/FontDescriptor;->deleteTempFiles(Ljava/io/File;)Z

    .line 370
    .end local v12    # "e":Ljava/lang/Throwable;
    :goto_4
    return-void

    .line 298
    :cond_2
    :try_start_7
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/icepdf/core/pobjects/fonts/FontDescriptor;->entries:Ljava/util/Hashtable;

    new-instance v5, Lorg/icepdf/core/pobjects/Name;

    const-string/jumbo v6, "FontFile3"

    invoke-direct {v5, v6}, Lorg/icepdf/core/pobjects/Name;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v5}, Ljava/util/Hashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 299
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/icepdf/core/pobjects/fonts/FontDescriptor;->library:Lorg/icepdf/core/util/Library;

    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/icepdf/core/pobjects/fonts/FontDescriptor;->entries:Ljava/util/Hashtable;

    const-string/jumbo v6, "FontName"

    invoke-virtual {v2, v5, v6}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/icepdf/core/pobjects/Name;

    invoke-virtual {v2}, Lorg/icepdf/core/pobjects/Name;->toString()Ljava/lang/String;

    move-result-object v4

    .line 301
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/icepdf/core/pobjects/fonts/FontDescriptor;->library:Lorg/icepdf/core/util/Library;

    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/icepdf/core/pobjects/fonts/FontDescriptor;->entries:Ljava/util/Hashtable;

    const-string/jumbo v6, "FontFile3"

    invoke-virtual {v2, v5, v6}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Lorg/icepdf/core/pobjects/Stream;

    move-object v15, v0

    goto/16 :goto_0

    .line 302
    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/icepdf/core/pobjects/fonts/FontDescriptor;->entries:Ljava/util/Hashtable;

    new-instance v5, Lorg/icepdf/core/pobjects/Name;

    const-string/jumbo v6, "FontFile"

    invoke-direct {v5, v6}, Lorg/icepdf/core/pobjects/Name;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v5}, Ljava/util/Hashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 303
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/icepdf/core/pobjects/fonts/FontDescriptor;->library:Lorg/icepdf/core/util/Library;

    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/icepdf/core/pobjects/fonts/FontDescriptor;->entries:Ljava/util/Hashtable;

    const-string/jumbo v6, "FontName"

    invoke-virtual {v2, v5, v6}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/icepdf/core/pobjects/Name;

    invoke-virtual {v2}, Lorg/icepdf/core/pobjects/Name;->toString()Ljava/lang/String;

    move-result-object v4

    .line 305
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/icepdf/core/pobjects/fonts/FontDescriptor;->library:Lorg/icepdf/core/util/Library;

    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/icepdf/core/pobjects/fonts/FontDescriptor;->entries:Ljava/util/Hashtable;

    const-string/jumbo v6, "FontFile"

    invoke-virtual {v2, v5, v6}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Lorg/icepdf/core/pobjects/Stream;

    move-object v15, v0
    :try_end_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_0
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    goto/16 :goto_0

    .line 327
    .restart local v9    # "bytes":[B
    .restart local v10    # "context":Landroid/content/Context;
    .restart local v17    # "in":Ljava/io/InputStream;
    .restart local v21    # "outputStream":Ljava/io/FileOutputStream;
    .restart local v22    # "read":I
    :cond_4
    :try_start_8
    invoke-virtual/range {v17 .. v17}, Ljava/io/InputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_2
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    .line 332
    if-eqz v21, :cond_5

    .line 335
    :try_start_9
    invoke-virtual/range {v21 .. v21}, Ljava/io/FileOutputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_1
    .catch Ljava/lang/Throwable; {:try_start_9 .. :try_end_9} :catch_0
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    .line 343
    :cond_5
    :goto_5
    move-object v11, v14

    .line 345
    .local v11, "dir":Ljava/io/File;
    if-eqz v11, :cond_9

    :try_start_a
    invoke-virtual {v11}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    if-eqz v2, :cond_9

    .line 346
    invoke-virtual {v11}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v8

    .local v8, "arr$":[Ljava/io/File;
    array-length v0, v8

    move/from16 v18, v0

    .local v18, "len$":I
    const/16 v16, 0x0

    .local v16, "i$":I
    :goto_6
    move/from16 v0, v16

    move/from16 v1, v18

    if-ge v0, v1, :cond_9

    aget-object v13, v8, v16

    .line 347
    .local v13, "file":Ljava/io/File;
    invoke-virtual {v13}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v19

    .line 348
    .local v19, "name":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v5, ".ttf"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 350
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v14}, Ljava/io/File;->getCanonicalPath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v5, "/"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v19

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/graphics/Typeface;->createFromFile(Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v3

    .line 352
    .local v3, "typeface":Landroid/graphics/Typeface;
    new-instance v2, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v5, v4

    invoke-direct/range {v2 .. v7}, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;-><init>(Landroid/graphics/Typeface;Ljava/lang/String;Ljava/lang/String;FLjava/lang/String;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lorg/icepdf/core/pobjects/fonts/FontDescriptor;->font:Lorg/icepdf/core/pobjects/fonts/FontFile;

    .line 346
    .end local v3    # "typeface":Landroid/graphics/Typeface;
    :cond_6
    add-int/lit8 v16, v16, 0x1

    goto :goto_6

    .line 336
    .end local v8    # "arr$":[Ljava/io/File;
    .end local v11    # "dir":Ljava/io/File;
    .end local v13    # "file":Ljava/io/File;
    .end local v16    # "i$":I
    .end local v18    # "len$":I
    .end local v19    # "name":Ljava/lang/String;
    :catch_1
    move-exception v12

    .line 337
    .local v12, "e":Ljava/io/IOException;
    const-string/jumbo v2, "Page"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "IOException: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v12}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_a
    .catch Ljava/lang/Throwable; {:try_start_a .. :try_end_a} :catch_0
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    goto/16 :goto_5

    .line 367
    .end local v9    # "bytes":[B
    .end local v10    # "context":Landroid/content/Context;
    .end local v12    # "e":Ljava/io/IOException;
    .end local v17    # "in":Ljava/io/InputStream;
    .end local v21    # "outputStream":Ljava/io/FileOutputStream;
    .end local v22    # "read":I
    :catchall_1
    move-exception v2

    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lorg/icepdf/core/pobjects/fonts/FontDescriptor;->deleteTempFiles(Ljava/io/File;)Z

    throw v2

    .line 328
    .restart local v9    # "bytes":[B
    .restart local v10    # "context":Landroid/content/Context;
    .restart local v17    # "in":Ljava/io/InputStream;
    .restart local v21    # "outputStream":Ljava/io/FileOutputStream;
    .restart local v22    # "read":I
    :catch_2
    move-exception v12

    .line 329
    .restart local v12    # "e":Ljava/io/IOException;
    :try_start_b
    const-string/jumbo v2, "Page"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "Exception: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v12}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_2

    .line 332
    if-eqz v21, :cond_5

    .line 335
    :try_start_c
    invoke-virtual/range {v21 .. v21}, Ljava/io/FileOutputStream;->close()V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_3
    .catch Ljava/lang/Throwable; {:try_start_c .. :try_end_c} :catch_0
    .catchall {:try_start_c .. :try_end_c} :catchall_1

    goto/16 :goto_5

    .line 336
    :catch_3
    move-exception v12

    .line 337
    :try_start_d
    const-string/jumbo v2, "Page"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "IOException: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v12}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_d
    .catch Ljava/lang/Throwable; {:try_start_d .. :try_end_d} :catch_0
    .catchall {:try_start_d .. :try_end_d} :catchall_1

    goto/16 :goto_5

    .line 332
    .end local v12    # "e":Ljava/io/IOException;
    :catchall_2
    move-exception v2

    if-eqz v21, :cond_7

    .line 335
    :try_start_e
    invoke-virtual/range {v21 .. v21}, Ljava/io/FileOutputStream;->close()V
    :try_end_e
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_4
    .catch Ljava/lang/Throwable; {:try_start_e .. :try_end_e} :catch_0
    .catchall {:try_start_e .. :try_end_e} :catchall_1

    .line 339
    :cond_7
    :goto_7
    :try_start_f
    throw v2

    .line 336
    :catch_4
    move-exception v12

    .line 337
    .restart local v12    # "e":Ljava/io/IOException;
    const-string/jumbo v5, "Page"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "IOException: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v12}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_7

    .line 336
    .end local v9    # "bytes":[B
    .end local v12    # "e":Ljava/io/IOException;
    .end local v21    # "outputStream":Ljava/io/FileOutputStream;
    .end local v22    # "read":I
    .restart local v20    # "outputStream":Ljava/io/FileOutputStream;
    :catch_5
    move-exception v12

    .line 337
    .restart local v12    # "e":Ljava/io/IOException;
    const-string/jumbo v5, "Page"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "IOException: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v12}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_f
    .catch Ljava/lang/Throwable; {:try_start_f .. :try_end_f} :catch_0
    .catchall {:try_start_f .. :try_end_f} :catchall_1

    goto/16 :goto_3

    .line 328
    .end local v12    # "e":Ljava/io/IOException;
    :catch_6
    move-exception v12

    .line 329
    .restart local v12    # "e":Ljava/io/IOException;
    :try_start_10
    const-string/jumbo v5, "Page"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "Exception: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v12}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_3

    .line 332
    if-eqz v20, :cond_1

    .line 335
    :try_start_11
    invoke-virtual/range {v20 .. v20}, Ljava/io/FileOutputStream;->close()V
    :try_end_11
    .catch Ljava/io/IOException; {:try_start_11 .. :try_end_11} :catch_7
    .catch Ljava/lang/Throwable; {:try_start_11 .. :try_end_11} :catch_0
    .catchall {:try_start_11 .. :try_end_11} :catchall_1

    goto/16 :goto_3

    .line 336
    :catch_7
    move-exception v12

    .line 337
    :try_start_12
    const-string/jumbo v5, "Page"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "IOException: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v12}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_12
    .catch Ljava/lang/Throwable; {:try_start_12 .. :try_end_12} :catch_0
    .catchall {:try_start_12 .. :try_end_12} :catchall_1

    goto/16 :goto_3

    .line 332
    .end local v12    # "e":Ljava/io/IOException;
    :catchall_3
    move-exception v2

    if-eqz v20, :cond_8

    .line 335
    :try_start_13
    invoke-virtual/range {v20 .. v20}, Ljava/io/FileOutputStream;->close()V
    :try_end_13
    .catch Ljava/io/IOException; {:try_start_13 .. :try_end_13} :catch_8
    .catch Ljava/lang/Throwable; {:try_start_13 .. :try_end_13} :catch_0
    .catchall {:try_start_13 .. :try_end_13} :catchall_1

    .line 339
    :cond_8
    :goto_8
    :try_start_14
    throw v2

    .line 336
    :catch_8
    move-exception v12

    .line 337
    .restart local v12    # "e":Ljava/io/IOException;
    const-string/jumbo v5, "Page"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "IOException: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v12}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_14
    .catch Ljava/lang/Throwable; {:try_start_14 .. :try_end_14} :catch_0
    .catchall {:try_start_14 .. :try_end_14} :catchall_1

    goto :goto_8

    .line 367
    .end local v10    # "context":Landroid/content/Context;
    .end local v12    # "e":Ljava/io/IOException;
    .end local v17    # "in":Ljava/io/InputStream;
    .end local v20    # "outputStream":Ljava/io/FileOutputStream;
    :cond_9
    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lorg/icepdf/core/pobjects/fonts/FontDescriptor;->deleteTempFiles(Ljava/io/File;)Z

    goto/16 :goto_4

    .line 326
    .restart local v10    # "context":Landroid/content/Context;
    .restart local v17    # "in":Ljava/io/InputStream;
    .restart local v20    # "outputStream":Ljava/io/FileOutputStream;
    :catchall_4
    move-exception v2

    goto/16 :goto_2
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 400
    const/4 v0, 0x0

    .line 401
    .local v0, "name":Ljava/lang/String;
    iget-object v1, p0, Lorg/icepdf/core/pobjects/fonts/FontDescriptor;->font:Lorg/icepdf/core/pobjects/fonts/FontFile;

    if-eqz v1, :cond_0

    .line 402
    iget-object v1, p0, Lorg/icepdf/core/pobjects/fonts/FontDescriptor;->font:Lorg/icepdf/core/pobjects/fonts/FontFile;

    invoke-interface {v1}, Lorg/icepdf/core/pobjects/fonts/FontFile;->getName()Ljava/lang/String;

    move-result-object v0

    .line 403
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-super {p0}, Lorg/icepdf/core/pobjects/Dictionary;->getPObjectReference()Lorg/icepdf/core/pobjects/Reference;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " FONTDESCRIPTOR= "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/icepdf/core/pobjects/fonts/FontDescriptor;->entries:Ljava/util/Hashtable;

    invoke-virtual {v2}, Ljava/util/Hashtable;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
