.class public Lorg/icepdf/core/pobjects/Page;
.super Lorg/icepdf/core/pobjects/Dictionary;
.source "Page.java"

# interfaces
.implements Lorg/icepdf/core/util/MemoryManageable;


# static fields
.field public static final ANNOTS_KEY:Lorg/icepdf/core/pobjects/Name;

.field public static final BOUNDARY_ARTBOX:I = 0x5

.field public static final BOUNDARY_BLEEDBOX:I = 0x3

.field public static final BOUNDARY_CROPBOX:I = 0x2

.field public static final BOUNDARY_MEDIABOX:I = 0x1

.field public static final BOUNDARY_TRIMBOX:I = 0x4

.field private static final CHAR_COMMA:C = ','

.field private static final CHAR_HYPHEN:C = '-'

.field public static final CONTENTS_KEY:Lorg/icepdf/core/pobjects/Name;

.field private static final STRING_EMPTY:Ljava/lang/String; = " "

.field private static final TAG:Ljava/lang/String; = "DocumentParser"

.field private static final logger:Ljava/util/logging/Logger;


# instance fields
.field private artBox:Lorg/icepdf/core/pobjects/PRectangle;

.field private bleedBox:Lorg/icepdf/core/pobjects/PRectangle;

.field private contents:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<",
            "Lorg/icepdf/core/pobjects/Stream;",
            ">;"
        }
    .end annotation
.end field

.field private cropBox:Lorg/icepdf/core/pobjects/PRectangle;

.field private isInited:Z

.field private mediaBox:Lorg/icepdf/core/pobjects/PRectangle;

.field private pageRotation:F

.field private paintPageListeners:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<",
            "Lorg/icepdf/core/events/PaintPageListener;",
            ">;"
        }
    .end annotation
.end field

.field private resources:Lorg/icepdf/core/pobjects/Resources;

.field private shapes:Lorg/icepdf/core/pobjects/graphics/Shapes;

.field private trimBox:Lorg/icepdf/core/pobjects/PRectangle;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 84
    const-class v0, Lorg/icepdf/core/pobjects/Page;

    invoke-virtual {v0}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Lorg/icepdf/core/pobjects/Page;->logger:Ljava/util/logging/Logger;

    .line 87
    new-instance v0, Lorg/icepdf/core/pobjects/Name;

    const-string/jumbo v1, "Annots"

    invoke-direct {v0, v1}, Lorg/icepdf/core/pobjects/Name;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/icepdf/core/pobjects/Page;->ANNOTS_KEY:Lorg/icepdf/core/pobjects/Name;

    .line 88
    new-instance v0, Lorg/icepdf/core/pobjects/Name;

    const-string/jumbo v1, "Contents"

    invoke-direct {v0, v1}, Lorg/icepdf/core/pobjects/Name;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/icepdf/core/pobjects/Page;->CONTENTS_KEY:Lorg/icepdf/core/pobjects/Name;

    return-void
.end method

.method public constructor <init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V
    .locals 2
    .param p1, "l"    # Lorg/icepdf/core/util/Library;
    .param p2, "h"    # Ljava/util/Hashtable;

    .prologue
    .line 173
    invoke-direct {p0, p1, p2}, Lorg/icepdf/core/pobjects/Dictionary;-><init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V

    .line 130
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/icepdf/core/pobjects/Page;->isInited:Z

    .line 142
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/icepdf/core/pobjects/Page;->shapes:Lorg/icepdf/core/pobjects/graphics/Shapes;

    .line 145
    new-instance v0, Ljava/util/Vector;

    const/16 v1, 0x8

    invoke-direct {v0, v1}, Ljava/util/Vector;-><init>(I)V

    iput-object v0, p0, Lorg/icepdf/core/pobjects/Page;->paintPageListeners:Ljava/util/Vector;

    .line 162
    const/4 v0, 0x0

    iput v0, p0, Lorg/icepdf/core/pobjects/Page;->pageRotation:F

    .line 174
    return-void
.end method

.method private getPageRotation()F
    .locals 6

    .prologue
    const/high16 v5, 0x43b40000    # 360.0f

    .line 1418
    iget-object v2, p0, Lorg/icepdf/core/pobjects/Page;->library:Lorg/icepdf/core/util/Library;

    iget-object v3, p0, Lorg/icepdf/core/pobjects/Page;->entries:Ljava/util/Hashtable;

    const-string/jumbo v4, "Rotate"

    invoke-virtual {v2, v3, v4}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    .line 1419
    .local v1, "tmpRotation":Ljava/lang/Object;
    if-eqz v1, :cond_1

    .line 1420
    check-cast v1, Ljava/lang/Number;

    .end local v1    # "tmpRotation":Ljava/lang/Object;
    invoke-virtual {v1}, Ljava/lang/Number;->floatValue()F

    move-result v2

    iput v2, p0, Lorg/icepdf/core/pobjects/Page;->pageRotation:F

    .line 1436
    :cond_0
    :goto_0
    iget v2, p0, Lorg/icepdf/core/pobjects/Page;->pageRotation:F

    sub-float v2, v5, v2

    iput v2, p0, Lorg/icepdf/core/pobjects/Page;->pageRotation:F

    .line 1437
    iget v2, p0, Lorg/icepdf/core/pobjects/Page;->pageRotation:F

    rem-float/2addr v2, v5

    iput v2, p0, Lorg/icepdf/core/pobjects/Page;->pageRotation:F

    .line 1439
    iget v2, p0, Lorg/icepdf/core/pobjects/Page;->pageRotation:F

    return v2

    .line 1425
    .restart local v1    # "tmpRotation":Ljava/lang/Object;
    :cond_1
    invoke-virtual {p0}, Lorg/icepdf/core/pobjects/Page;->getParent()Lorg/icepdf/core/pobjects/PageTree;

    move-result-object v0

    .line 1426
    .local v0, "pageTree":Lorg/icepdf/core/pobjects/PageTree;
    :goto_1
    if-eqz v0, :cond_0

    .line 1427
    iget-boolean v2, v0, Lorg/icepdf/core/pobjects/PageTree;->isRotationFactor:Z

    if-eqz v2, :cond_2

    .line 1428
    iget v2, v0, Lorg/icepdf/core/pobjects/PageTree;->rotationFactor:F

    iput v2, p0, Lorg/icepdf/core/pobjects/Page;->pageRotation:F

    goto :goto_0

    .line 1431
    :cond_2
    invoke-virtual {v0}, Lorg/icepdf/core/pobjects/PageTree;->getParent()Lorg/icepdf/core/pobjects/PageTree;

    move-result-object v0

    goto :goto_1
.end method

.method private initPageContents()V
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    const/4 v8, 0x1

    .line 252
    iget-object v5, p0, Lorg/icepdf/core/pobjects/Page;->library:Lorg/icepdf/core/util/Library;

    iget-object v6, p0, Lorg/icepdf/core/pobjects/Page;->entries:Ljava/util/Hashtable;

    sget-object v7, Lorg/icepdf/core/pobjects/Page;->CONTENTS_KEY:Lorg/icepdf/core/pobjects/Name;

    invoke-virtual {v7}, Lorg/icepdf/core/pobjects/Name;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    .line 255
    .local v2, "pageContent":Ljava/lang/Object;
    instance-of v5, v2, Lorg/icepdf/core/pobjects/Stream;

    if-eqz v5, :cond_1

    .line 256
    new-instance v5, Ljava/util/Vector;

    invoke-direct {v5, v8}, Ljava/util/Vector;-><init>(I)V

    iput-object v5, p0, Lorg/icepdf/core/pobjects/Page;->contents:Ljava/util/Vector;

    move-object v4, v2

    .line 257
    check-cast v4, Lorg/icepdf/core/pobjects/Stream;

    .line 258
    .local v4, "tmpStream":Lorg/icepdf/core/pobjects/Stream;
    iget-object v5, p0, Lorg/icepdf/core/pobjects/Page;->library:Lorg/icepdf/core/util/Library;

    iget-object v6, p0, Lorg/icepdf/core/pobjects/Page;->entries:Ljava/util/Hashtable;

    sget-object v7, Lorg/icepdf/core/pobjects/Page;->CONTENTS_KEY:Lorg/icepdf/core/pobjects/Name;

    invoke-virtual {v7}, Lorg/icepdf/core/pobjects/Name;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Lorg/icepdf/core/util/Library;->getObjectReference(Ljava/util/Hashtable;Ljava/lang/String;)Lorg/icepdf/core/pobjects/Reference;

    move-result-object v5

    invoke-virtual {v4, v5}, Lorg/icepdf/core/pobjects/Stream;->setPObjectReference(Lorg/icepdf/core/pobjects/Reference;)V

    .line 260
    iget-object v5, p0, Lorg/icepdf/core/pobjects/Page;->contents:Ljava/util/Vector;

    invoke-virtual {v5, v4}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    .line 279
    .end local v4    # "tmpStream":Lorg/icepdf/core/pobjects/Stream;
    :cond_0
    return-void

    .line 263
    :cond_1
    instance-of v5, v2, Ljava/util/Vector;

    if-eqz v5, :cond_0

    move-object v0, v2

    .line 264
    check-cast v0, Ljava/util/Vector;

    .line 265
    .local v0, "conts":Ljava/util/Vector;
    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v3

    .line 266
    .local v3, "sz":I
    new-instance v5, Ljava/util/Vector;

    invoke-static {v3, v8}, Ljava/lang/Math;->max(II)I

    move-result v6

    invoke-direct {v5, v6}, Ljava/util/Vector;-><init>(I)V

    iput-object v5, p0, Lorg/icepdf/core/pobjects/Page;->contents:Ljava/util/Vector;

    .line 268
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v3, :cond_0

    .line 269
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 270
    new-instance v5, Ljava/lang/InterruptedException;

    const-string/jumbo v6, "Page Content initialization thread interrupted"

    invoke-direct {v5, v6}, Ljava/lang/InterruptedException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 272
    :cond_2
    iget-object v6, p0, Lorg/icepdf/core/pobjects/Page;->library:Lorg/icepdf/core/util/Library;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/icepdf/core/pobjects/Reference;

    invoke-virtual {v6, v5}, Lorg/icepdf/core/util/Library;->getObject(Lorg/icepdf/core/pobjects/Reference;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/icepdf/core/pobjects/Stream;

    .line 273
    .restart local v4    # "tmpStream":Lorg/icepdf/core/pobjects/Stream;
    if-eqz v4, :cond_3

    .line 274
    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/icepdf/core/pobjects/Reference;

    invoke-virtual {v4, v5}, Lorg/icepdf/core/pobjects/Stream;->setPObjectReference(Lorg/icepdf/core/pobjects/Reference;)V

    .line 275
    iget-object v5, p0, Lorg/icepdf/core/pobjects/Page;->contents:Ljava/util/Vector;

    invoke-virtual {v5, v4}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    .line 268
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private initPageResources()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 282
    iget-object v3, p0, Lorg/icepdf/core/pobjects/Page;->library:Lorg/icepdf/core/util/Library;

    iget-object v4, p0, Lorg/icepdf/core/pobjects/Page;->entries:Ljava/util/Hashtable;

    const-string/jumbo v5, "Resources"

    invoke-virtual {v3, v4, v5}, Lorg/icepdf/core/util/Library;->getResources(Ljava/util/Hashtable;Ljava/lang/String;)Lorg/icepdf/core/pobjects/Resources;

    move-result-object v2

    .line 283
    .local v2, "res":Lorg/icepdf/core/pobjects/Resources;
    if-nez v2, :cond_1

    .line 284
    invoke-virtual {p0}, Lorg/icepdf/core/pobjects/Page;->getParent()Lorg/icepdf/core/pobjects/PageTree;

    move-result-object v1

    .line 285
    .local v1, "pt":Lorg/icepdf/core/pobjects/PageTree;
    :goto_0
    if-eqz v1, :cond_1

    .line 286
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 287
    new-instance v3, Ljava/lang/InterruptedException;

    const-string/jumbo v4, "Page Resource initialization thread interrupted"

    invoke-direct {v3, v4}, Ljava/lang/InterruptedException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 289
    :cond_0
    invoke-virtual {v1}, Lorg/icepdf/core/pobjects/PageTree;->getResources()Lorg/icepdf/core/pobjects/Resources;

    move-result-object v0

    .line 290
    .local v0, "parentResources":Lorg/icepdf/core/pobjects/Resources;
    if-eqz v0, :cond_3

    .line 291
    move-object v2, v0

    .line 297
    .end local v0    # "parentResources":Lorg/icepdf/core/pobjects/Resources;
    .end local v1    # "pt":Lorg/icepdf/core/pobjects/PageTree;
    :cond_1
    iput-object v2, p0, Lorg/icepdf/core/pobjects/Page;->resources:Lorg/icepdf/core/pobjects/Resources;

    .line 298
    iget-object v3, p0, Lorg/icepdf/core/pobjects/Page;->resources:Lorg/icepdf/core/pobjects/Resources;

    if-eqz v3, :cond_2

    .line 299
    iget-object v3, p0, Lorg/icepdf/core/pobjects/Page;->resources:Lorg/icepdf/core/pobjects/Resources;

    invoke-virtual {v3, p0}, Lorg/icepdf/core/pobjects/Resources;->addReference(Ljava/lang/Object;)V

    .line 301
    :cond_2
    return-void

    .line 294
    .restart local v0    # "parentResources":Lorg/icepdf/core/pobjects/Resources;
    .restart local v1    # "pt":Lorg/icepdf/core/pobjects/PageTree;
    :cond_3
    invoke-virtual {v1}, Lorg/icepdf/core/pobjects/PageTree;->getParent()Lorg/icepdf/core/pobjects/PageTree;

    move-result-object v1

    .line 295
    goto :goto_0
.end method


# virtual methods
.method public addPaintPageListener(Lorg/icepdf/core/events/PaintPageListener;)V
    .locals 2
    .param p1, "listener"    # Lorg/icepdf/core/events/PaintPageListener;

    .prologue
    .line 1740
    iget-object v1, p0, Lorg/icepdf/core/pobjects/Page;->paintPageListeners:Ljava/util/Vector;

    monitor-enter v1

    .line 1741
    :try_start_0
    iget-object v0, p0, Lorg/icepdf/core/pobjects/Page;->paintPageListeners:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1742
    iget-object v0, p0, Lorg/icepdf/core/pobjects/Page;->paintPageListeners:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    .line 1744
    :cond_0
    monitor-exit v1

    .line 1745
    return-void

    .line 1744
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method protected declared-synchronized dispose(Z)V
    .locals 8
    .param p1, "cache"    # Z

    .prologue
    .line 187
    monitor-enter p0

    :try_start_0
    iget-boolean v6, p0, Lorg/icepdf/core/pobjects/Page;->isInited:Z

    if-eqz v6, :cond_6

    .line 189
    const/4 v6, 0x0

    iput-boolean v6, p0, Lorg/icepdf/core/pobjects/Page;->isInited:Z

    .line 197
    iget-object v6, p0, Lorg/icepdf/core/pobjects/Page;->contents:Ljava/util/Vector;

    if-eqz v6, :cond_1

    .line 199
    iget-object v6, p0, Lorg/icepdf/core/pobjects/Page;->contents:Ljava/util/Vector;

    invoke-virtual {v6}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/icepdf/core/pobjects/Stream;

    .line 200
    .local v4, "stream":Lorg/icepdf/core/pobjects/Stream;
    invoke-virtual {v4, p1}, Lorg/icepdf/core/pobjects/Stream;->dispose(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 187
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v4    # "stream":Lorg/icepdf/core/pobjects/Stream;
    :catchall_0
    move-exception v6

    monitor-exit p0

    throw v6

    .line 202
    .restart local v2    # "i$":Ljava/util/Iterator;
    :cond_0
    :try_start_1
    iget-object v6, p0, Lorg/icepdf/core/pobjects/Page;->contents:Ljava/util/Vector;

    invoke-virtual {v6}, Ljava/util/Vector;->clear()V

    .line 203
    iget-object v6, p0, Lorg/icepdf/core/pobjects/Page;->contents:Ljava/util/Vector;

    invoke-virtual {v6}, Ljava/util/Vector;->trimToSize()V

    .line 207
    .end local v2    # "i$":Ljava/util/Iterator;
    :cond_1
    iget-object v6, p0, Lorg/icepdf/core/pobjects/Page;->shapes:Lorg/icepdf/core/pobjects/graphics/Shapes;

    if-eqz v6, :cond_2

    .line 208
    iget-object v6, p0, Lorg/icepdf/core/pobjects/Page;->shapes:Lorg/icepdf/core/pobjects/graphics/Shapes;

    invoke-virtual {v6}, Lorg/icepdf/core/pobjects/graphics/Shapes;->dispose()V

    .line 209
    const/4 v6, 0x0

    iput-object v6, p0, Lorg/icepdf/core/pobjects/Page;->shapes:Lorg/icepdf/core/pobjects/graphics/Shapes;

    .line 213
    :cond_2
    iget-object v6, p0, Lorg/icepdf/core/pobjects/Page;->resources:Lorg/icepdf/core/pobjects/Resources;

    if-eqz v6, :cond_3

    .line 214
    iget-object v6, p0, Lorg/icepdf/core/pobjects/Page;->resources:Lorg/icepdf/core/pobjects/Resources;

    invoke-virtual {v6, p1, p0}, Lorg/icepdf/core/pobjects/Resources;->dispose(ZLorg/icepdf/core/pobjects/Dictionary;)Z

    .line 215
    const/4 v6, 0x0

    iput-object v6, p0, Lorg/icepdf/core/pobjects/Page;->resources:Lorg/icepdf/core/pobjects/Resources;

    .line 218
    :cond_3
    if-eqz p1, :cond_6

    .line 220
    iget-object v6, p0, Lorg/icepdf/core/pobjects/Page;->library:Lorg/icepdf/core/util/Library;

    invoke-virtual {p0}, Lorg/icepdf/core/pobjects/Page;->getPObjectReference()Lorg/icepdf/core/pobjects/Reference;

    move-result-object v7

    invoke-virtual {v6, v7}, Lorg/icepdf/core/util/Library;->removeObject(Lorg/icepdf/core/pobjects/Reference;)V

    .line 222
    iget-object v6, p0, Lorg/icepdf/core/pobjects/Page;->entries:Ljava/util/Hashtable;

    sget-object v7, Lorg/icepdf/core/pobjects/Page;->ANNOTS_KEY:Lorg/icepdf/core/pobjects/Name;

    invoke-virtual {v7}, Lorg/icepdf/core/pobjects/Name;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    .line 224
    .local v5, "tmp":Ljava/lang/Object;
    if-nez v5, :cond_4

    .line 226
    iget-object v6, p0, Lorg/icepdf/core/pobjects/Page;->entries:Ljava/util/Hashtable;

    sget-object v7, Lorg/icepdf/core/pobjects/Page;->ANNOTS_KEY:Lorg/icepdf/core/pobjects/Name;

    invoke-virtual {v6, v7}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    .line 229
    :cond_4
    if-eqz v5, :cond_6

    instance-of v6, v5, Ljava/util/Vector;

    if-eqz v6, :cond_6

    .line 230
    move-object v0, v5

    check-cast v0, Ljava/util/Vector;

    move-object v1, v0

    .line 231
    .local v1, "annots":Ljava/util/Vector;
    invoke-virtual {v1}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .restart local v2    # "i$":Ljava/util/Iterator;
    :cond_5
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_6

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 232
    .local v3, "ref":Ljava/lang/Object;
    instance-of v6, v3, Lorg/icepdf/core/pobjects/Reference;

    if-eqz v6, :cond_5

    .line 233
    iget-object v6, p0, Lorg/icepdf/core/pobjects/Page;->library:Lorg/icepdf/core/util/Library;

    check-cast v3, Lorg/icepdf/core/pobjects/Reference;

    .end local v3    # "ref":Ljava/lang/Object;
    invoke-virtual {v6, v3}, Lorg/icepdf/core/util/Library;->removeObject(Lorg/icepdf/core/pobjects/Reference;)V

    goto :goto_1

    .line 240
    .end local v1    # "annots":Ljava/util/Vector;
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v5    # "tmp":Ljava/lang/Object;
    :cond_6
    iget-object v6, p0, Lorg/icepdf/core/pobjects/Page;->paintPageListeners:Ljava/util/Vector;

    if-eqz v6, :cond_7

    .line 241
    iget-object v6, p0, Lorg/icepdf/core/pobjects/Page;->paintPageListeners:Ljava/util/Vector;

    invoke-virtual {v6}, Ljava/util/Vector;->clear()V

    .line 242
    iget-object v6, p0, Lorg/icepdf/core/pobjects/Page;->paintPageListeners:Ljava/util/Vector;

    invoke-virtual {v6}, Ljava/util/Vector;->trimToSize()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 244
    :cond_7
    monitor-exit p0

    return-void
.end method

.method public getArtBox()Landroid/graphics/RectF;
    .locals 4

    .prologue
    .line 1572
    iget-object v1, p0, Lorg/icepdf/core/pobjects/Page;->library:Lorg/icepdf/core/util/Library;

    iget-object v2, p0, Lorg/icepdf/core/pobjects/Page;->entries:Ljava/util/Hashtable;

    const-string/jumbo v3, "ArtBox"

    invoke-virtual {v1, v2, v3}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Vector;

    move-object v0, v1

    check-cast v0, Ljava/util/Vector;

    .line 1573
    .local v0, "boxDimensions":Ljava/util/Vector;
    if-eqz v0, :cond_0

    .line 1574
    new-instance v1, Lorg/icepdf/core/pobjects/PRectangle;

    invoke-direct {v1, v0}, Lorg/icepdf/core/pobjects/PRectangle;-><init>(Ljava/util/Vector;)V

    iput-object v1, p0, Lorg/icepdf/core/pobjects/Page;->artBox:Lorg/icepdf/core/pobjects/PRectangle;

    .line 1578
    :cond_0
    iget-object v1, p0, Lorg/icepdf/core/pobjects/Page;->artBox:Lorg/icepdf/core/pobjects/PRectangle;

    if-nez v1, :cond_1

    .line 1579
    invoke-virtual {p0}, Lorg/icepdf/core/pobjects/Page;->getCropBox()Landroid/graphics/RectF;

    move-result-object v1

    check-cast v1, Lorg/icepdf/core/pobjects/PRectangle;

    iput-object v1, p0, Lorg/icepdf/core/pobjects/Page;->artBox:Lorg/icepdf/core/pobjects/PRectangle;

    .line 1581
    :cond_1
    iget-object v1, p0, Lorg/icepdf/core/pobjects/Page;->artBox:Lorg/icepdf/core/pobjects/PRectangle;

    return-object v1
.end method

.method public getBleedBox()Landroid/graphics/RectF;
    .locals 4

    .prologue
    .line 1592
    iget-object v1, p0, Lorg/icepdf/core/pobjects/Page;->library:Lorg/icepdf/core/util/Library;

    iget-object v2, p0, Lorg/icepdf/core/pobjects/Page;->entries:Ljava/util/Hashtable;

    const-string/jumbo v3, "BleedBox"

    invoke-virtual {v1, v2, v3}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Vector;

    move-object v0, v1

    check-cast v0, Ljava/util/Vector;

    .line 1593
    .local v0, "boxDimensions":Ljava/util/Vector;
    if-eqz v0, :cond_0

    .line 1594
    new-instance v1, Lorg/icepdf/core/pobjects/PRectangle;

    invoke-direct {v1, v0}, Lorg/icepdf/core/pobjects/PRectangle;-><init>(Ljava/util/Vector;)V

    iput-object v1, p0, Lorg/icepdf/core/pobjects/Page;->bleedBox:Lorg/icepdf/core/pobjects/PRectangle;

    .line 1598
    :cond_0
    iget-object v1, p0, Lorg/icepdf/core/pobjects/Page;->bleedBox:Lorg/icepdf/core/pobjects/PRectangle;

    if-nez v1, :cond_1

    .line 1599
    invoke-virtual {p0}, Lorg/icepdf/core/pobjects/Page;->getCropBox()Landroid/graphics/RectF;

    move-result-object v1

    check-cast v1, Lorg/icepdf/core/pobjects/PRectangle;

    iput-object v1, p0, Lorg/icepdf/core/pobjects/Page;->bleedBox:Lorg/icepdf/core/pobjects/PRectangle;

    .line 1601
    :cond_1
    iget-object v1, p0, Lorg/icepdf/core/pobjects/Page;->bleedBox:Lorg/icepdf/core/pobjects/PRectangle;

    return-object v1
.end method

.method public getBoundingBox(F)Landroid/graphics/RectF;
    .locals 2
    .param p1, "userRotation"    # F

    .prologue
    .line 1274
    const/4 v0, 0x2

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {p0, v0, p1, v1}, Lorg/icepdf/core/pobjects/Page;->getBoundingBox(IFF)Landroid/graphics/RectF;

    move-result-object v0

    return-object v0
.end method

.method public getBoundingBox(FF)Landroid/graphics/RectF;
    .locals 1
    .param p1, "userRotation"    # F
    .param p2, "userZoom"    # F

    .prologue
    .line 1288
    const/4 v0, 0x2

    invoke-virtual {p0, v0, p1, p2}, Lorg/icepdf/core/pobjects/Page;->getBoundingBox(IFF)Landroid/graphics/RectF;

    move-result-object v0

    return-object v0
.end method

.method public getBoundingBox(IFF)Landroid/graphics/RectF;
    .locals 10
    .param p1, "boundary"    # I
    .param p2, "userRotation"    # F
    .param p3, "userZoom"    # F

    .prologue
    const/4 v9, 0x0

    .line 1304
    invoke-virtual {p0, p2}, Lorg/icepdf/core/pobjects/Page;->getTotalRotation(F)F

    move-result v5

    .line 1305
    .local v5, "totalRotation":F
    invoke-virtual {p0, p1}, Lorg/icepdf/core/pobjects/Page;->getPageBoundary(I)Lorg/icepdf/core/pobjects/PRectangle;

    move-result-object v3

    .line 1307
    .local v3, "pageBoundary":Lorg/icepdf/core/pobjects/PRectangle;
    iget v7, v3, Lorg/icepdf/core/pobjects/PRectangle;->right:F

    iget v8, v3, Lorg/icepdf/core/pobjects/PRectangle;->left:F

    sub-float/2addr v7, v8

    mul-float v6, v7, p3

    .line 1308
    .local v6, "width":F
    iget v7, v3, Lorg/icepdf/core/pobjects/PRectangle;->bottom:F

    iget v8, v3, Lorg/icepdf/core/pobjects/PRectangle;->top:F

    sub-float/2addr v7, v8

    mul-float v2, v7, p3

    .line 1310
    .local v2, "height":F
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    .line 1313
    .local v0, "at":Landroid/graphics/Matrix;
    invoke-virtual {v0, v5}, Landroid/graphics/Matrix;->preRotate(F)Z

    .line 1315
    new-instance v4, Landroid/graphics/Path;

    invoke-direct {v4}, Landroid/graphics/Path;-><init>()V

    .line 1316
    .local v4, "tempPath":Landroid/graphics/Path;
    invoke-virtual {v4, v9, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 1317
    invoke-virtual {v4, v6, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1318
    invoke-virtual {v4, v6, v9}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1319
    invoke-virtual {v4, v9, v9}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1320
    invoke-virtual {v4}, Landroid/graphics/Path;->close()V

    .line 1322
    invoke-virtual {v4, v0}, Landroid/graphics/Path;->transform(Landroid/graphics/Matrix;)V

    .line 1324
    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    .line 1325
    .local v1, "boundingBox":Landroid/graphics/RectF;
    const/4 v7, 0x1

    invoke-virtual {v4, v1, v7}, Landroid/graphics/Path;->computeBounds(Landroid/graphics/RectF;Z)V

    .line 1327
    return-object v1
.end method

.method public getCropBox()Landroid/graphics/RectF;
    .locals 6

    .prologue
    .line 1535
    iget-object v3, p0, Lorg/icepdf/core/pobjects/Page;->library:Lorg/icepdf/core/util/Library;

    iget-object v4, p0, Lorg/icepdf/core/pobjects/Page;->entries:Ljava/util/Hashtable;

    const-string/jumbo v5, "CropBox"

    invoke-virtual {v3, v4, v5}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Vector;

    move-object v0, v3

    check-cast v0, Ljava/util/Vector;

    .line 1536
    .local v0, "boxDimensions":Ljava/util/Vector;
    if-eqz v0, :cond_0

    .line 1537
    new-instance v3, Lorg/icepdf/core/pobjects/PRectangle;

    invoke-direct {v3, v0}, Lorg/icepdf/core/pobjects/PRectangle;-><init>(Ljava/util/Vector;)V

    iput-object v3, p0, Lorg/icepdf/core/pobjects/Page;->cropBox:Lorg/icepdf/core/pobjects/PRectangle;

    .line 1541
    :cond_0
    iget-object v3, p0, Lorg/icepdf/core/pobjects/Page;->cropBox:Lorg/icepdf/core/pobjects/PRectangle;

    if-nez v3, :cond_1

    .line 1542
    invoke-virtual {p0}, Lorg/icepdf/core/pobjects/Page;->getParent()Lorg/icepdf/core/pobjects/PageTree;

    move-result-object v2

    .line 1543
    .local v2, "pageTree":Lorg/icepdf/core/pobjects/PageTree;
    :goto_0
    if-eqz v2, :cond_1

    iget-object v3, p0, Lorg/icepdf/core/pobjects/Page;->cropBox:Lorg/icepdf/core/pobjects/PRectangle;

    if-nez v3, :cond_1

    .line 1544
    invoke-virtual {v2}, Lorg/icepdf/core/pobjects/PageTree;->getCropBox()Lorg/icepdf/core/pobjects/PRectangle;

    move-result-object v3

    if-nez v3, :cond_3

    .line 1552
    .end local v2    # "pageTree":Lorg/icepdf/core/pobjects/PageTree;
    :cond_1
    invoke-virtual {p0}, Lorg/icepdf/core/pobjects/Page;->getMediaBox()Landroid/graphics/RectF;

    move-result-object v1

    check-cast v1, Lorg/icepdf/core/pobjects/PRectangle;

    .line 1553
    .local v1, "mediaBox":Lorg/icepdf/core/pobjects/PRectangle;
    iget-object v3, p0, Lorg/icepdf/core/pobjects/Page;->cropBox:Lorg/icepdf/core/pobjects/PRectangle;

    if-nez v3, :cond_4

    if-eqz v1, :cond_4

    .line 1554
    new-instance v3, Lorg/icepdf/core/pobjects/PRectangle;

    invoke-direct {v3, v1}, Lorg/icepdf/core/pobjects/PRectangle;-><init>(Lorg/icepdf/core/pobjects/PRectangle;)V

    iput-object v3, p0, Lorg/icepdf/core/pobjects/Page;->cropBox:Lorg/icepdf/core/pobjects/PRectangle;

    .line 1561
    :cond_2
    :goto_1
    iget-object v3, p0, Lorg/icepdf/core/pobjects/Page;->cropBox:Lorg/icepdf/core/pobjects/PRectangle;

    return-object v3

    .line 1547
    .end local v1    # "mediaBox":Lorg/icepdf/core/pobjects/PRectangle;
    .restart local v2    # "pageTree":Lorg/icepdf/core/pobjects/PageTree;
    :cond_3
    invoke-virtual {v2}, Lorg/icepdf/core/pobjects/PageTree;->getCropBox()Lorg/icepdf/core/pobjects/PRectangle;

    move-result-object v3

    iput-object v3, p0, Lorg/icepdf/core/pobjects/Page;->cropBox:Lorg/icepdf/core/pobjects/PRectangle;

    .line 1548
    invoke-virtual {v2}, Lorg/icepdf/core/pobjects/PageTree;->getParent()Lorg/icepdf/core/pobjects/PageTree;

    move-result-object v2

    goto :goto_0

    .line 1555
    .end local v2    # "pageTree":Lorg/icepdf/core/pobjects/PageTree;
    .restart local v1    # "mediaBox":Lorg/icepdf/core/pobjects/PRectangle;
    :cond_4
    if-eqz v1, :cond_2

    .line 1559
    iget-object v3, p0, Lorg/icepdf/core/pobjects/Page;->cropBox:Lorg/icepdf/core/pobjects/PRectangle;

    invoke-virtual {v1, v3}, Lorg/icepdf/core/pobjects/PRectangle;->createCartesianIntersection(Lorg/icepdf/core/pobjects/PRectangle;)Lorg/icepdf/core/pobjects/PRectangle;

    move-result-object v3

    iput-object v3, p0, Lorg/icepdf/core/pobjects/Page;->cropBox:Lorg/icepdf/core/pobjects/PRectangle;

    goto :goto_1
.end method

.method public getDecodedContentSteam()[Ljava/lang/String;
    .locals 13

    .prologue
    const/4 v10, 0x0

    .line 1468
    :try_start_0
    invoke-direct {p0}, Lorg/icepdf/core/pobjects/Page;->initPageContents()V

    .line 1470
    iget-object v9, p0, Lorg/icepdf/core/pobjects/Page;->contents:Ljava/util/Vector;

    if-nez v9, :cond_1

    move-object v2, v10

    .line 1499
    :cond_0
    :goto_0
    return-object v2

    .line 1473
    :cond_1
    iget-object v9, p0, Lorg/icepdf/core/pobjects/Page;->contents:Ljava/util/Vector;

    invoke-virtual {v9}, Ljava/util/Vector;->size()I

    move-result v9

    new-array v2, v9, [Ljava/lang/String;

    .line 1474
    .local v2, "decodedContentStream":[Ljava/lang/String;
    const/4 v4, 0x0

    .line 1475
    .local v4, "i":I
    iget-object v9, p0, Lorg/icepdf/core/pobjects/Page;->contents:Ljava/util/Vector;

    invoke-virtual {v9}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .local v5, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lorg/icepdf/core/pobjects/Stream;

    .line 1476
    .local v8, "stream":Lorg/icepdf/core/pobjects/Stream;
    invoke-virtual {v8}, Lorg/icepdf/core/pobjects/Stream;->getInputStreamForDecodedStreamBytes()Ljava/io/InputStream;

    move-result-object v7

    .line 1478
    .local v7, "input":Ljava/io/InputStream;
    if-eqz v7, :cond_2

    .line 1481
    instance-of v9, v7, Lorg/icepdf/core/io/SeekableInput;

    if-eqz v9, :cond_3

    .line 1482
    move-object v0, v7

    check-cast v0, Lorg/icepdf/core/io/SeekableInput;

    move-object v9, v0

    const/4 v11, 0x0

    invoke-static {v9, v11}, Lorg/icepdf/core/util/Utils;->getContentFromSeekableInput(Lorg/icepdf/core/io/SeekableInput;Z)Ljava/lang/String;

    move-result-object v1

    .line 1489
    .local v1, "content":Ljava/lang/String;
    :goto_2
    aput-object v1, v2, v4

    .line 1490
    invoke-virtual {v7}, Ljava/io/InputStream;->close()V

    .line 1491
    add-int/lit8 v4, v4, 0x1

    .line 1492
    goto :goto_1

    .line 1485
    .end local v1    # "content":Ljava/lang/String;
    :cond_3
    const/4 v9, 0x1

    new-array v6, v9, [Ljava/io/InputStream;

    const/4 v9, 0x0

    aput-object v7, v6, v9

    .line 1486
    .local v6, "inArray":[Ljava/io/InputStream;
    const/4 v9, 0x0

    invoke-static {v6, v9}, Lorg/icepdf/core/util/Utils;->getContentAndReplaceInputStream([Ljava/io/InputStream;Z)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v1

    .restart local v1    # "content":Ljava/lang/String;
    goto :goto_2

    .line 1494
    .end local v1    # "content":Ljava/lang/String;
    .end local v2    # "decodedContentStream":[Ljava/lang/String;
    .end local v4    # "i":I
    .end local v5    # "i$":Ljava/util/Iterator;
    .end local v6    # "inArray":[Ljava/io/InputStream;
    .end local v7    # "input":Ljava/io/InputStream;
    .end local v8    # "stream":Lorg/icepdf/core/pobjects/Stream;
    :catch_0
    move-exception v3

    .line 1495
    .local v3, "e":Ljava/lang/InterruptedException;
    sget-object v9, Lorg/icepdf/core/pobjects/Page;->logger:Ljava/util/logging/Logger;

    sget-object v11, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    const-string/jumbo v12, "Error initializing page Contents."

    invoke-virtual {v9, v11, v12, v3}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    .end local v3    # "e":Ljava/lang/InterruptedException;
    :goto_3
    move-object v2, v10

    .line 1499
    goto :goto_0

    .line 1496
    :catch_1
    move-exception v3

    .line 1497
    .local v3, "e":Ljava/io/IOException;
    sget-object v9, Lorg/icepdf/core/pobjects/Page;->logger:Ljava/util/logging/Logger;

    sget-object v11, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    const-string/jumbo v12, "Error closing content stream"

    invoke-virtual {v9, v11, v12}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    goto :goto_3
.end method

.method public declared-synchronized getImages()Ljava/util/Vector;
    .locals 2

    .prologue
    .line 1720
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lorg/icepdf/core/pobjects/Page;->isInited:Z

    if-nez v0, :cond_0

    .line 1722
    new-instance v0, Ljava/io/File;

    const-string/jumbo v1, ""

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lorg/icepdf/core/pobjects/Page;->init(Ljava/io/File;)V

    .line 1724
    :cond_0
    iget-object v0, p0, Lorg/icepdf/core/pobjects/Page;->shapes:Lorg/icepdf/core/pobjects/graphics/Shapes;

    invoke-virtual {v0}, Lorg/icepdf/core/pobjects/graphics/Shapes;->getImages()Ljava/util/Vector;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    .line 1720
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getMediaBox()Landroid/graphics/RectF;
    .locals 5

    .prologue
    .line 1511
    iget-object v2, p0, Lorg/icepdf/core/pobjects/Page;->library:Lorg/icepdf/core/util/Library;

    iget-object v3, p0, Lorg/icepdf/core/pobjects/Page;->entries:Ljava/util/Hashtable;

    const-string/jumbo v4, "MediaBox"

    invoke-virtual {v2, v3, v4}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Vector;

    move-object v0, v2

    check-cast v0, Ljava/util/Vector;

    .line 1512
    .local v0, "boxDimensions":Ljava/util/Vector;
    if-eqz v0, :cond_0

    .line 1513
    new-instance v2, Lorg/icepdf/core/pobjects/PRectangle;

    invoke-direct {v2, v0}, Lorg/icepdf/core/pobjects/PRectangle;-><init>(Ljava/util/Vector;)V

    iput-object v2, p0, Lorg/icepdf/core/pobjects/Page;->mediaBox:Lorg/icepdf/core/pobjects/PRectangle;

    .line 1517
    :cond_0
    iget-object v2, p0, Lorg/icepdf/core/pobjects/Page;->mediaBox:Lorg/icepdf/core/pobjects/PRectangle;

    if-nez v2, :cond_1

    .line 1518
    invoke-virtual {p0}, Lorg/icepdf/core/pobjects/Page;->getParent()Lorg/icepdf/core/pobjects/PageTree;

    move-result-object v1

    .line 1519
    .local v1, "pageTree":Lorg/icepdf/core/pobjects/PageTree;
    :goto_0
    if-eqz v1, :cond_1

    iget-object v2, p0, Lorg/icepdf/core/pobjects/Page;->mediaBox:Lorg/icepdf/core/pobjects/PRectangle;

    if-nez v2, :cond_1

    .line 1520
    invoke-virtual {v1}, Lorg/icepdf/core/pobjects/PageTree;->getMediaBox()Lorg/icepdf/core/pobjects/PRectangle;

    move-result-object v2

    iput-object v2, p0, Lorg/icepdf/core/pobjects/Page;->mediaBox:Lorg/icepdf/core/pobjects/PRectangle;

    .line 1521
    invoke-virtual {v1}, Lorg/icepdf/core/pobjects/PageTree;->getParent()Lorg/icepdf/core/pobjects/PageTree;

    move-result-object v1

    goto :goto_0

    .line 1524
    .end local v1    # "pageTree":Lorg/icepdf/core/pobjects/PageTree;
    :cond_1
    iget-object v2, p0, Lorg/icepdf/core/pobjects/Page;->mediaBox:Lorg/icepdf/core/pobjects/PRectangle;

    return-object v2
.end method

.method public getPageBoundary(I)Lorg/icepdf/core/pobjects/PRectangle;
    .locals 2
    .param p1, "specifiedBox"    # I

    .prologue
    .line 1337
    const/4 v0, 0x0

    .line 1339
    .local v0, "userSpecifiedBox":Lorg/icepdf/core/pobjects/PRectangle;
    const/4 v1, 0x1

    if-ne p1, v1, :cond_2

    .line 1340
    invoke-virtual {p0}, Lorg/icepdf/core/pobjects/Page;->getMediaBox()Landroid/graphics/RectF;

    move-result-object v0

    .end local v0    # "userSpecifiedBox":Lorg/icepdf/core/pobjects/PRectangle;
    check-cast v0, Lorg/icepdf/core/pobjects/PRectangle;

    .line 1367
    .restart local v0    # "userSpecifiedBox":Lorg/icepdf/core/pobjects/PRectangle;
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 1368
    invoke-virtual {p0}, Lorg/icepdf/core/pobjects/Page;->getCropBox()Landroid/graphics/RectF;

    move-result-object v0

    .end local v0    # "userSpecifiedBox":Lorg/icepdf/core/pobjects/PRectangle;
    check-cast v0, Lorg/icepdf/core/pobjects/PRectangle;

    .line 1371
    .restart local v0    # "userSpecifiedBox":Lorg/icepdf/core/pobjects/PRectangle;
    :cond_1
    return-object v0

    .line 1343
    :cond_2
    const/4 v1, 0x2

    if-ne p1, v1, :cond_3

    .line 1344
    invoke-virtual {p0}, Lorg/icepdf/core/pobjects/Page;->getCropBox()Landroid/graphics/RectF;

    move-result-object v0

    .end local v0    # "userSpecifiedBox":Lorg/icepdf/core/pobjects/PRectangle;
    check-cast v0, Lorg/icepdf/core/pobjects/PRectangle;

    .restart local v0    # "userSpecifiedBox":Lorg/icepdf/core/pobjects/PRectangle;
    goto :goto_0

    .line 1347
    :cond_3
    const/4 v1, 0x3

    if-ne p1, v1, :cond_4

    .line 1348
    iget-object v1, p0, Lorg/icepdf/core/pobjects/Page;->bleedBox:Lorg/icepdf/core/pobjects/PRectangle;

    if-eqz v1, :cond_0

    .line 1349
    invoke-virtual {p0}, Lorg/icepdf/core/pobjects/Page;->getBleedBox()Landroid/graphics/RectF;

    move-result-object v0

    .end local v0    # "userSpecifiedBox":Lorg/icepdf/core/pobjects/PRectangle;
    check-cast v0, Lorg/icepdf/core/pobjects/PRectangle;

    .restart local v0    # "userSpecifiedBox":Lorg/icepdf/core/pobjects/PRectangle;
    goto :goto_0

    .line 1352
    :cond_4
    const/4 v1, 0x4

    if-ne p1, v1, :cond_5

    .line 1353
    iget-object v1, p0, Lorg/icepdf/core/pobjects/Page;->trimBox:Lorg/icepdf/core/pobjects/PRectangle;

    if-eqz v1, :cond_0

    .line 1354
    invoke-virtual {p0}, Lorg/icepdf/core/pobjects/Page;->getTrimBox()Landroid/graphics/RectF;

    move-result-object v0

    .end local v0    # "userSpecifiedBox":Lorg/icepdf/core/pobjects/PRectangle;
    check-cast v0, Lorg/icepdf/core/pobjects/PRectangle;

    .restart local v0    # "userSpecifiedBox":Lorg/icepdf/core/pobjects/PRectangle;
    goto :goto_0

    .line 1357
    :cond_5
    const/4 v1, 0x5

    if-ne p1, v1, :cond_6

    .line 1358
    iget-object v1, p0, Lorg/icepdf/core/pobjects/Page;->artBox:Lorg/icepdf/core/pobjects/PRectangle;

    if-eqz v1, :cond_0

    .line 1359
    invoke-virtual {p0}, Lorg/icepdf/core/pobjects/Page;->getArtBox()Landroid/graphics/RectF;

    move-result-object v0

    .end local v0    # "userSpecifiedBox":Lorg/icepdf/core/pobjects/PRectangle;
    check-cast v0, Lorg/icepdf/core/pobjects/PRectangle;

    .restart local v0    # "userSpecifiedBox":Lorg/icepdf/core/pobjects/PRectangle;
    goto :goto_0

    .line 1363
    :cond_6
    invoke-virtual {p0}, Lorg/icepdf/core/pobjects/Page;->getBleedBox()Landroid/graphics/RectF;

    move-result-object v0

    .end local v0    # "userSpecifiedBox":Lorg/icepdf/core/pobjects/PRectangle;
    check-cast v0, Lorg/icepdf/core/pobjects/PRectangle;

    .restart local v0    # "userSpecifiedBox":Lorg/icepdf/core/pobjects/PRectangle;
    goto :goto_0
.end method

.method public getPageTransform(IFF)Landroid/graphics/Matrix;
    .locals 22
    .param p1, "boundary"    # I
    .param p2, "userRotation"    # F
    .param p3, "userZoom"    # F

    .prologue
    .line 835
    new-instance v2, Landroid/graphics/Matrix;

    invoke-direct {v2}, Landroid/graphics/Matrix;-><init>()V

    .line 837
    .local v2, "at":Landroid/graphics/Matrix;
    invoke-virtual/range {p0 .. p3}, Lorg/icepdf/core/pobjects/Page;->getBoundingBox(IFF)Landroid/graphics/RectF;

    move-result-object v3

    .line 838
    .local v3, "boundingBox":Landroid/graphics/RectF;
    const/16 v18, 0x0

    invoke-virtual {v3}, Landroid/graphics/RectF;->height()F

    move-result v19

    move/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v2, v0, v1}, Landroid/graphics/Matrix;->preTranslate(FF)Z

    .line 841
    const/high16 v18, 0x3f800000    # 1.0f

    const/high16 v19, -0x40800000    # -1.0f

    move/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v2, v0, v1}, Landroid/graphics/Matrix;->preScale(FF)Z

    .line 843
    move/from16 v0, p3

    move/from16 v1, p3

    invoke-virtual {v2, v0, v1}, Landroid/graphics/Matrix;->preScale(FF)Z

    .line 845
    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Lorg/icepdf/core/pobjects/Page;->getTotalRotation(F)F

    move-result v7

    .line 846
    .local v7, "totalRotation":F
    invoke-virtual/range {p0 .. p1}, Lorg/icepdf/core/pobjects/Page;->getPageBoundary(I)Lorg/icepdf/core/pobjects/PRectangle;

    move-result-object v6

    .line 848
    .local v6, "pageBoundary":Lorg/icepdf/core/pobjects/PRectangle;
    const/16 v18, 0x0

    cmpl-float v18, v7, v18

    if-nez v18, :cond_1

    .line 880
    :cond_0
    :goto_0
    invoke-virtual {v2, v7}, Landroid/graphics/Matrix;->preRotate(F)Z

    .line 883
    const/16 v18, 0x0

    iget v0, v6, Lorg/icepdf/core/pobjects/PRectangle;->left:F

    move/from16 v19, v0

    sub-float v12, v18, v19

    .line 884
    .local v12, "x":F
    const/16 v18, 0x0

    iget v0, v6, Lorg/icepdf/core/pobjects/PRectangle;->top:F

    move/from16 v19, v0

    iget v0, v6, Lorg/icepdf/core/pobjects/PRectangle;->bottom:F

    move/from16 v20, v0

    iget v0, v6, Lorg/icepdf/core/pobjects/PRectangle;->top:F

    move/from16 v21, v0

    sub-float v20, v20, v21

    sub-float v19, v19, v20

    sub-float v13, v18, v19

    .line 885
    .local v13, "y":F
    invoke-virtual {v2, v12, v13}, Landroid/graphics/Matrix;->preTranslate(FF)Z

    .line 887
    return-object v2

    .line 849
    .end local v12    # "x":F
    .end local v13    # "y":F
    :cond_1
    const/high16 v18, 0x42b40000    # 90.0f

    cmpl-float v18, v7, v18

    if-nez v18, :cond_2

    .line 850
    iget v0, v6, Lorg/icepdf/core/pobjects/PRectangle;->bottom:F

    move/from16 v18, v0

    iget v0, v6, Lorg/icepdf/core/pobjects/PRectangle;->top:F

    move/from16 v19, v0

    sub-float v18, v18, v19

    const/16 v19, 0x0

    move/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v2, v0, v1}, Landroid/graphics/Matrix;->preTranslate(FF)Z

    goto :goto_0

    .line 851
    :cond_2
    const/high16 v18, 0x43340000    # 180.0f

    cmpl-float v18, v7, v18

    if-nez v18, :cond_3

    .line 852
    iget v0, v6, Lorg/icepdf/core/pobjects/PRectangle;->right:F

    move/from16 v18, v0

    iget v0, v6, Lorg/icepdf/core/pobjects/PRectangle;->left:F

    move/from16 v19, v0

    sub-float v18, v18, v19

    iget v0, v6, Lorg/icepdf/core/pobjects/PRectangle;->bottom:F

    move/from16 v19, v0

    iget v0, v6, Lorg/icepdf/core/pobjects/PRectangle;->top:F

    move/from16 v20, v0

    sub-float v19, v19, v20

    move/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v2, v0, v1}, Landroid/graphics/Matrix;->preTranslate(FF)Z

    goto :goto_0

    .line 853
    :cond_3
    const/high16 v18, 0x43870000    # 270.0f

    cmpl-float v18, v7, v18

    if-nez v18, :cond_4

    .line 854
    const/16 v18, 0x0

    iget v0, v6, Lorg/icepdf/core/pobjects/PRectangle;->right:F

    move/from16 v19, v0

    iget v0, v6, Lorg/icepdf/core/pobjects/PRectangle;->left:F

    move/from16 v20, v0

    sub-float v19, v19, v20

    move/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v2, v0, v1}, Landroid/graphics/Matrix;->preTranslate(FF)Z

    goto :goto_0

    .line 856
    :cond_4
    const/16 v18, 0x0

    cmpl-float v18, v7, v18

    if-lez v18, :cond_5

    const/high16 v18, 0x42b40000    # 90.0f

    cmpg-float v18, v7, v18

    if-gez v18, :cond_5

    .line 857
    iget v0, v6, Lorg/icepdf/core/pobjects/PRectangle;->bottom:F

    move/from16 v18, v0

    iget v0, v6, Lorg/icepdf/core/pobjects/PRectangle;->top:F

    move/from16 v19, v0

    sub-float v18, v18, v19

    move/from16 v0, v18

    float-to-double v0, v0

    move-wide/from16 v18, v0

    const/high16 v20, 0x42b40000    # 90.0f

    sub-float v20, v20, v7

    move/from16 v0, v20

    float-to-double v0, v0

    move-wide/from16 v20, v0

    invoke-static/range {v20 .. v21}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v20

    invoke-static/range {v20 .. v21}, Ljava/lang/Math;->cos(D)D

    move-result-wide v20

    mul-double v14, v18, v20

    .line 858
    .local v14, "xShift":D
    double-to-float v0, v14

    move/from16 v18, v0

    const/16 v19, 0x0

    move/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v2, v0, v1}, Landroid/graphics/Matrix;->preTranslate(FF)Z

    goto/16 :goto_0

    .line 859
    .end local v14    # "xShift":D
    :cond_5
    const/high16 v18, 0x42b40000    # 90.0f

    cmpl-float v18, v7, v18

    if-lez v18, :cond_6

    const/high16 v18, 0x43340000    # 180.0f

    cmpg-float v18, v7, v18

    if-gez v18, :cond_6

    .line 860
    const/high16 v18, 0x43340000    # 180.0f

    sub-float v18, v18, v7

    move/from16 v0, v18

    float-to-double v0, v0

    move-wide/from16 v18, v0

    invoke-static/range {v18 .. v19}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v8

    .line 861
    .local v8, "rad":D
    invoke-static {v8, v9}, Ljava/lang/Math;->cos(D)D

    move-result-wide v4

    .line 862
    .local v4, "cosRad":D
    invoke-static {v8, v9}, Ljava/lang/Math;->sin(D)D

    move-result-wide v10

    .line 863
    .local v10, "sinRad":D
    iget v0, v6, Lorg/icepdf/core/pobjects/PRectangle;->bottom:F

    move/from16 v18, v0

    iget v0, v6, Lorg/icepdf/core/pobjects/PRectangle;->top:F

    move/from16 v19, v0

    sub-float v18, v18, v19

    move/from16 v0, v18

    float-to-double v0, v0

    move-wide/from16 v18, v0

    mul-double v18, v18, v10

    iget v0, v6, Lorg/icepdf/core/pobjects/PRectangle;->right:F

    move/from16 v20, v0

    iget v0, v6, Lorg/icepdf/core/pobjects/PRectangle;->left:F

    move/from16 v21, v0

    sub-float v20, v20, v21

    move/from16 v0, v20

    float-to-double v0, v0

    move-wide/from16 v20, v0

    mul-double v20, v20, v4

    add-double v14, v18, v20

    .line 864
    .restart local v14    # "xShift":D
    iget v0, v6, Lorg/icepdf/core/pobjects/PRectangle;->bottom:F

    move/from16 v18, v0

    iget v0, v6, Lorg/icepdf/core/pobjects/PRectangle;->top:F

    move/from16 v19, v0

    sub-float v18, v18, v19

    move/from16 v0, v18

    float-to-double v0, v0

    move-wide/from16 v18, v0

    mul-double v16, v18, v4

    .line 865
    .local v16, "yShift":D
    double-to-float v0, v14

    move/from16 v18, v0

    move-wide/from16 v0, v16

    double-to-float v0, v0

    move/from16 v19, v0

    move/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v2, v0, v1}, Landroid/graphics/Matrix;->preTranslate(FF)Z

    goto/16 :goto_0

    .line 866
    .end local v4    # "cosRad":D
    .end local v8    # "rad":D
    .end local v10    # "sinRad":D
    .end local v14    # "xShift":D
    .end local v16    # "yShift":D
    :cond_6
    const/high16 v18, 0x43340000    # 180.0f

    cmpl-float v18, v7, v18

    if-lez v18, :cond_7

    const/high16 v18, 0x43870000    # 270.0f

    cmpg-float v18, v7, v18

    if-gez v18, :cond_7

    .line 867
    const/high16 v18, 0x43340000    # 180.0f

    sub-float v18, v7, v18

    move/from16 v0, v18

    float-to-double v0, v0

    move-wide/from16 v18, v0

    invoke-static/range {v18 .. v19}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v8

    .line 868
    .restart local v8    # "rad":D
    invoke-static {v8, v9}, Ljava/lang/Math;->cos(D)D

    move-result-wide v4

    .line 869
    .restart local v4    # "cosRad":D
    invoke-static {v8, v9}, Ljava/lang/Math;->sin(D)D

    move-result-wide v10

    .line 870
    .restart local v10    # "sinRad":D
    iget v0, v6, Lorg/icepdf/core/pobjects/PRectangle;->right:F

    move/from16 v18, v0

    iget v0, v6, Lorg/icepdf/core/pobjects/PRectangle;->left:F

    move/from16 v19, v0

    sub-float v18, v18, v19

    move/from16 v0, v18

    float-to-double v0, v0

    move-wide/from16 v18, v0

    mul-double v14, v18, v4

    .line 871
    .restart local v14    # "xShift":D
    iget v0, v6, Lorg/icepdf/core/pobjects/PRectangle;->right:F

    move/from16 v18, v0

    iget v0, v6, Lorg/icepdf/core/pobjects/PRectangle;->left:F

    move/from16 v19, v0

    sub-float v18, v18, v19

    move/from16 v0, v18

    float-to-double v0, v0

    move-wide/from16 v18, v0

    mul-double v18, v18, v10

    iget v0, v6, Lorg/icepdf/core/pobjects/PRectangle;->bottom:F

    move/from16 v20, v0

    iget v0, v6, Lorg/icepdf/core/pobjects/PRectangle;->top:F

    move/from16 v21, v0

    sub-float v20, v20, v21

    move/from16 v0, v20

    float-to-double v0, v0

    move-wide/from16 v20, v0

    mul-double v20, v20, v4

    add-double v16, v18, v20

    .line 872
    .restart local v16    # "yShift":D
    double-to-float v0, v14

    move/from16 v18, v0

    move-wide/from16 v0, v16

    double-to-float v0, v0

    move/from16 v19, v0

    move/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v2, v0, v1}, Landroid/graphics/Matrix;->preTranslate(FF)Z

    goto/16 :goto_0

    .line 873
    .end local v4    # "cosRad":D
    .end local v8    # "rad":D
    .end local v10    # "sinRad":D
    .end local v14    # "xShift":D
    .end local v16    # "yShift":D
    :cond_7
    const/high16 v18, 0x43870000    # 270.0f

    cmpl-float v18, v7, v18

    if-lez v18, :cond_0

    const/high16 v18, 0x43b40000    # 360.0f

    cmpg-float v18, v7, v18

    if-gez v18, :cond_0

    .line 874
    iget v0, v6, Lorg/icepdf/core/pobjects/PRectangle;->right:F

    move/from16 v18, v0

    iget v0, v6, Lorg/icepdf/core/pobjects/PRectangle;->left:F

    move/from16 v19, v0

    sub-float v18, v18, v19

    move/from16 v0, v18

    float-to-double v0, v0

    move-wide/from16 v18, v0

    const/high16 v20, 0x43870000    # 270.0f

    sub-float v20, v7, v20

    move/from16 v0, v20

    float-to-double v0, v0

    move-wide/from16 v20, v0

    invoke-static/range {v20 .. v21}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v20

    invoke-static/range {v20 .. v21}, Ljava/lang/Math;->cos(D)D

    move-result-wide v20

    mul-double v16, v18, v20

    .line 875
    .restart local v16    # "yShift":D
    const/16 v18, 0x0

    move-wide/from16 v0, v16

    double-to-float v0, v0

    move/from16 v19, v0

    move/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v2, v0, v1}, Landroid/graphics/Matrix;->preTranslate(FF)Z

    goto/16 :goto_0
.end method

.method public getParent()Lorg/icepdf/core/pobjects/PageTree;
    .locals 3

    .prologue
    .line 1175
    iget-object v0, p0, Lorg/icepdf/core/pobjects/Page;->library:Lorg/icepdf/core/util/Library;

    iget-object v1, p0, Lorg/icepdf/core/pobjects/Page;->entries:Ljava/util/Hashtable;

    const-string/jumbo v2, "Parent"

    invoke-virtual {v0, v1, v2}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/icepdf/core/pobjects/PageTree;

    return-object v0
.end method

.method protected getParentReference()Lorg/icepdf/core/pobjects/Reference;
    .locals 4

    .prologue
    .line 1159
    iget-object v1, p0, Lorg/icepdf/core/pobjects/Page;->entries:Ljava/util/Hashtable;

    const-string/jumbo v2, "Parent"

    invoke-virtual {v1, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 1160
    .local v0, "o":Ljava/lang/Object;
    if-nez v0, :cond_0

    .line 1162
    iget-object v1, p0, Lorg/icepdf/core/pobjects/Page;->entries:Ljava/util/Hashtable;

    new-instance v2, Lorg/icepdf/core/pobjects/Name;

    const-string/jumbo v3, "Parent"

    invoke-direct {v2, v3}, Lorg/icepdf/core/pobjects/Name;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 1165
    :cond_0
    check-cast v0, Lorg/icepdf/core/pobjects/Reference;

    .end local v0    # "o":Ljava/lang/Object;
    return-object v0
.end method

.method public getResources()Lorg/icepdf/core/pobjects/Resources;
    .locals 1

    .prologue
    .line 1728
    iget-object v0, p0, Lorg/icepdf/core/pobjects/Page;->resources:Lorg/icepdf/core/pobjects/Resources;

    return-object v0
.end method

.method public getSize(F)Lorg/icepdf/core/pobjects/PDimension;
    .locals 2
    .param p1, "userRotation"    # F

    .prologue
    .line 1190
    const/4 v0, 0x2

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {p0, v0, p1, v1}, Lorg/icepdf/core/pobjects/Page;->getSize(IFF)Lorg/icepdf/core/pobjects/PDimension;

    move-result-object v0

    return-object v0
.end method

.method public getSize(FF)Lorg/icepdf/core/pobjects/PDimension;
    .locals 1
    .param p1, "userRotation"    # F
    .param p2, "userZoom"    # F

    .prologue
    .line 1204
    const/4 v0, 0x2

    invoke-virtual {p0, v0, p1, p2}, Lorg/icepdf/core/pobjects/Page;->getSize(IFF)Lorg/icepdf/core/pobjects/PDimension;

    move-result-object v0

    return-object v0
.end method

.method public getSize(IFF)Lorg/icepdf/core/pobjects/PDimension;
    .locals 11
    .param p1, "boundary"    # I
    .param p2, "userRotation"    # F
    .param p3, "userZoom"    # F

    .prologue
    const/4 v10, 0x0

    .line 1220
    invoke-virtual {p0, p2}, Lorg/icepdf/core/pobjects/Page;->getTotalRotation(F)F

    move-result v6

    .line 1221
    .local v6, "totalRotation":F
    invoke-virtual {p0, p1}, Lorg/icepdf/core/pobjects/Page;->getPageBoundary(I)Lorg/icepdf/core/pobjects/PRectangle;

    move-result-object v3

    .line 1223
    .local v3, "pageBoundary":Lorg/icepdf/core/pobjects/PRectangle;
    iget v8, v3, Lorg/icepdf/core/pobjects/PRectangle;->right:F

    iget v9, v3, Lorg/icepdf/core/pobjects/PRectangle;->left:F

    sub-float/2addr v8, v9

    mul-float v7, v8, p3

    .line 1224
    .local v7, "width":F
    iget v8, v3, Lorg/icepdf/core/pobjects/PRectangle;->bottom:F

    iget v9, v3, Lorg/icepdf/core/pobjects/PRectangle;->top:F

    sub-float/2addr v8, v9

    mul-float v2, v8, p3

    .line 1226
    .local v2, "height":F
    cmpl-float v8, v6, v10

    if-eqz v8, :cond_0

    const/high16 v8, 0x43340000    # 180.0f

    cmpl-float v8, v6, v8

    if-nez v8, :cond_1

    .line 1259
    :cond_0
    :goto_0
    new-instance v8, Lorg/icepdf/core/pobjects/PDimension;

    invoke-direct {v8, v7, v2}, Lorg/icepdf/core/pobjects/PDimension;-><init>(FF)V

    return-object v8

    .line 1231
    :cond_1
    const/high16 v8, 0x42b40000    # 90.0f

    cmpl-float v8, v6, v8

    if-eqz v8, :cond_2

    const/high16 v8, 0x43870000    # 270.0f

    cmpl-float v8, v6, v8

    if-nez v8, :cond_3

    .line 1232
    :cond_2
    move v4, v7

    .line 1234
    .local v4, "temp":F
    move v7, v2

    .line 1235
    move v2, v4

    .line 1236
    goto :goto_0

    .line 1239
    .end local v4    # "temp":F
    :cond_3
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    .line 1242
    .local v0, "at":Landroid/graphics/Matrix;
    invoke-virtual {v0, v6}, Landroid/graphics/Matrix;->postRotate(F)Z

    .line 1244
    new-instance v5, Landroid/graphics/Path;

    invoke-direct {v5}, Landroid/graphics/Path;-><init>()V

    .line 1245
    .local v5, "tempPath":Landroid/graphics/Path;
    invoke-virtual {v5, v10, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 1246
    invoke-virtual {v5, v7, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1247
    invoke-virtual {v5, v7, v10}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1248
    invoke-virtual {v5, v10, v10}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1249
    invoke-virtual {v5}, Landroid/graphics/Path;->close()V

    .line 1251
    invoke-virtual {v5, v0}, Landroid/graphics/Path;->transform(Landroid/graphics/Matrix;)V

    .line 1253
    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    .line 1254
    .local v1, "boundingRect":Landroid/graphics/RectF;
    const/4 v8, 0x1

    invoke-virtual {v5, v1, v8}, Landroid/graphics/Path;->computeBounds(Landroid/graphics/RectF;Z)V

    .line 1255
    invoke-virtual {v1}, Landroid/graphics/RectF;->width()F

    move-result v7

    .line 1256
    invoke-virtual {v1}, Landroid/graphics/RectF;->height()F

    move-result v2

    goto :goto_0
.end method

.method public declared-synchronized getText()Lorg/icepdf/core/pobjects/graphics/text/PageText;
    .locals 13

    .prologue
    .line 1650
    monitor-enter p0

    :try_start_0
    iget-boolean v9, p0, Lorg/icepdf/core/pobjects/Page;->isInited:Z

    if-eqz v9, :cond_0

    .line 1651
    iget-object v9, p0, Lorg/icepdf/core/pobjects/Page;->shapes:Lorg/icepdf/core/pobjects/graphics/Shapes;

    if-eqz v9, :cond_0

    iget-object v9, p0, Lorg/icepdf/core/pobjects/Page;->shapes:Lorg/icepdf/core/pobjects/graphics/Shapes;

    invoke-virtual {v9}, Lorg/icepdf/core/pobjects/graphics/Shapes;->getPageText()Lorg/icepdf/core/pobjects/graphics/text/PageText;

    move-result-object v9

    if-eqz v9, :cond_0

    .line 1652
    iget-object v9, p0, Lorg/icepdf/core/pobjects/Page;->shapes:Lorg/icepdf/core/pobjects/graphics/Shapes;

    invoke-virtual {v9}, Lorg/icepdf/core/pobjects/graphics/Shapes;->getPageText()Lorg/icepdf/core/pobjects/graphics/text/PageText;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v9

    .line 1709
    :goto_0
    monitor-exit p0

    return-object v9

    .line 1656
    :cond_0
    const/4 v8, 0x0

    .line 1663
    .local v8, "textBlockShapes":Lorg/icepdf/core/pobjects/graphics/Shapes;
    :try_start_1
    iget-object v9, p0, Lorg/icepdf/core/pobjects/Page;->contents:Ljava/util/Vector;

    if-nez v9, :cond_1

    .line 1665
    invoke-direct {p0}, Lorg/icepdf/core/pobjects/Page;->initPageContents()V

    .line 1668
    :cond_1
    iget-object v9, p0, Lorg/icepdf/core/pobjects/Page;->resources:Lorg/icepdf/core/pobjects/Resources;

    if-nez v9, :cond_2

    .line 1670
    invoke-direct {p0}, Lorg/icepdf/core/pobjects/Page;->initPageResources()V

    .line 1673
    :cond_2
    iget-object v9, p0, Lorg/icepdf/core/pobjects/Page;->contents:Ljava/util/Vector;

    if-eqz v9, :cond_4

    .line 1674
    new-instance v3, Ljava/util/Vector;

    iget-object v9, p0, Lorg/icepdf/core/pobjects/Page;->contents:Ljava/util/Vector;

    invoke-virtual {v9}, Ljava/util/Vector;->size()I

    move-result v9

    invoke-direct {v3, v9}, Ljava/util/Vector;-><init>(I)V

    .line 1676
    .local v3, "inputStreamsVec":Ljava/util/Vector;, "Ljava/util/Vector<Ljava/io/InputStream;>;"
    const/4 v6, 0x0

    .local v6, "st":I
    iget-object v9, p0, Lorg/icepdf/core/pobjects/Page;->contents:Ljava/util/Vector;

    invoke-virtual {v9}, Ljava/util/Vector;->size()I

    move-result v4

    .local v4, "max":I
    :goto_1
    if-ge v6, v4, :cond_3

    .line 1677
    iget-object v9, p0, Lorg/icepdf/core/pobjects/Page;->contents:Ljava/util/Vector;

    invoke-virtual {v9, v6}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lorg/icepdf/core/pobjects/Stream;

    .line 1678
    .local v7, "stream":Lorg/icepdf/core/pobjects/Stream;
    invoke-virtual {v7}, Lorg/icepdf/core/pobjects/Stream;->getInputStreamForDecodedStreamBytes()Ljava/io/InputStream;

    move-result-object v2

    .line 1679
    .local v2, "input":Ljava/io/InputStream;
    invoke-virtual {v3, v2}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 1676
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 1681
    .end local v2    # "input":Ljava/io/InputStream;
    .end local v7    # "stream":Lorg/icepdf/core/pobjects/Stream;
    :cond_3
    new-instance v5, Lorg/icepdf/core/io/SequenceInputStream;

    invoke-virtual {v3}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v9

    invoke-direct {v5, v9}, Lorg/icepdf/core/io/SequenceInputStream;-><init>(Ljava/util/Iterator;)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1687
    .local v5, "sis":Lorg/icepdf/core/io/SequenceInputStream;
    :try_start_2
    new-instance v0, Lorg/icepdf/core/util/ContentParser;

    iget-object v9, p0, Lorg/icepdf/core/pobjects/Page;->library:Lorg/icepdf/core/util/Library;

    iget-object v10, p0, Lorg/icepdf/core/pobjects/Page;->resources:Lorg/icepdf/core/pobjects/Resources;

    invoke-direct {v0, v9, v10}, Lorg/icepdf/core/util/ContentParser;-><init>(Lorg/icepdf/core/util/Library;Lorg/icepdf/core/pobjects/Resources;)V

    .line 1689
    .local v0, "cp":Lorg/icepdf/core/util/ContentParser;
    invoke-virtual {v0, v5}, Lorg/icepdf/core/util/ContentParser;->parseTextBlocks(Ljava/io/InputStream;)Lorg/icepdf/core/pobjects/graphics/Shapes;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v8

    .line 1694
    :try_start_3
    invoke-virtual {v5}, Lorg/icepdf/core/io/SequenceInputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1706
    .end local v0    # "cp":Lorg/icepdf/core/util/ContentParser;
    .end local v3    # "inputStreamsVec":Ljava/util/Vector;, "Ljava/util/Vector<Ljava/io/InputStream;>;"
    .end local v4    # "max":I
    .end local v5    # "sis":Lorg/icepdf/core/io/SequenceInputStream;
    .end local v6    # "st":I
    :cond_4
    :goto_2
    if-eqz v8, :cond_5

    :try_start_4
    invoke-virtual {v8}, Lorg/icepdf/core/pobjects/graphics/Shapes;->getPageText()Lorg/icepdf/core/pobjects/graphics/text/PageText;

    move-result-object v9

    if-eqz v9, :cond_5

    .line 1707
    invoke-virtual {v8}, Lorg/icepdf/core/pobjects/graphics/Shapes;->getPageText()Lorg/icepdf/core/pobjects/graphics/text/PageText;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result-object v9

    goto :goto_0

    .line 1695
    .restart local v0    # "cp":Lorg/icepdf/core/util/ContentParser;
    .restart local v3    # "inputStreamsVec":Ljava/util/Vector;, "Ljava/util/Vector<Ljava/io/InputStream;>;"
    .restart local v4    # "max":I
    .restart local v5    # "sis":Lorg/icepdf/core/io/SequenceInputStream;
    .restart local v6    # "st":I
    :catch_0
    move-exception v1

    .line 1696
    .local v1, "e":Ljava/io/IOException;
    :try_start_5
    sget-object v9, Lorg/icepdf/core/pobjects/Page;->logger:Ljava/util/logging/Logger;

    sget-object v10, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string/jumbo v11, "Error closing page stream."

    invoke-virtual {v9, v10, v11, v1}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_5
    .catch Ljava/lang/InterruptedException; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_2

    .line 1700
    .end local v0    # "cp":Lorg/icepdf/core/util/ContentParser;
    .end local v1    # "e":Ljava/io/IOException;
    .end local v3    # "inputStreamsVec":Ljava/util/Vector;, "Ljava/util/Vector<Ljava/io/InputStream;>;"
    .end local v4    # "max":I
    .end local v5    # "sis":Lorg/icepdf/core/io/SequenceInputStream;
    .end local v6    # "st":I
    :catch_1
    move-exception v1

    .line 1703
    .local v1, "e":Ljava/lang/InterruptedException;
    const/4 v9, 0x0

    :try_start_6
    iput-boolean v9, p0, Lorg/icepdf/core/pobjects/Page;->isInited:Z

    .line 1704
    sget-object v9, Lorg/icepdf/core/pobjects/Page;->logger:Ljava/util/logging/Logger;

    sget-object v10, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    const-string/jumbo v11, "Page text extraction thread interrupted."

    invoke-virtual {v9, v10, v11, v1}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_2

    .line 1650
    .end local v1    # "e":Ljava/lang/InterruptedException;
    .end local v8    # "textBlockShapes":Lorg/icepdf/core/pobjects/graphics/Shapes;
    :catchall_0
    move-exception v9

    monitor-exit p0

    throw v9

    .line 1690
    .restart local v3    # "inputStreamsVec":Ljava/util/Vector;, "Ljava/util/Vector<Ljava/io/InputStream;>;"
    .restart local v4    # "max":I
    .restart local v5    # "sis":Lorg/icepdf/core/io/SequenceInputStream;
    .restart local v6    # "st":I
    .restart local v8    # "textBlockShapes":Lorg/icepdf/core/pobjects/graphics/Shapes;
    :catch_2
    move-exception v1

    .line 1691
    .local v1, "e":Ljava/lang/Exception;
    :try_start_7
    sget-object v9, Lorg/icepdf/core/pobjects/Page;->logger:Ljava/util/logging/Logger;

    sget-object v10, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string/jumbo v11, "Error getting page text."

    invoke-virtual {v9, v10, v11, v1}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 1694
    :try_start_8
    invoke-virtual {v5}, Lorg/icepdf/core/io/SequenceInputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_3
    .catch Ljava/lang/InterruptedException; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto :goto_2

    .line 1695
    :catch_3
    move-exception v1

    .line 1696
    .local v1, "e":Ljava/io/IOException;
    :try_start_9
    sget-object v9, Lorg/icepdf/core/pobjects/Page;->logger:Ljava/util/logging/Logger;

    sget-object v10, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string/jumbo v11, "Error closing page stream."

    invoke-virtual {v9, v10, v11, v1}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_9
    .catch Ljava/lang/InterruptedException; {:try_start_9 .. :try_end_9} :catch_1
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    goto :goto_2

    .line 1693
    .end local v1    # "e":Ljava/io/IOException;
    :catchall_1
    move-exception v9

    .line 1694
    :try_start_a
    invoke-virtual {v5}, Lorg/icepdf/core/io/SequenceInputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_4
    .catch Ljava/lang/InterruptedException; {:try_start_a .. :try_end_a} :catch_1
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    .line 1697
    :goto_3
    :try_start_b
    throw v9

    .line 1695
    :catch_4
    move-exception v1

    .line 1696
    .restart local v1    # "e":Ljava/io/IOException;
    sget-object v10, Lorg/icepdf/core/pobjects/Page;->logger:Ljava/util/logging/Logger;

    sget-object v11, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string/jumbo v12, "Error closing page stream."

    invoke-virtual {v10, v11, v12, v1}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_b
    .catch Ljava/lang/InterruptedException; {:try_start_b .. :try_end_b} :catch_1
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    goto :goto_3

    .line 1709
    .end local v1    # "e":Ljava/io/IOException;
    .end local v3    # "inputStreamsVec":Ljava/util/Vector;, "Ljava/util/Vector<Ljava/io/InputStream;>;"
    .end local v4    # "max":I
    .end local v5    # "sis":Lorg/icepdf/core/io/SequenceInputStream;
    .end local v6    # "st":I
    :cond_5
    const/4 v9, 0x0

    goto/16 :goto_0
.end method

.method public getThumbnail()Lorg/icepdf/core/pobjects/Thumbnail;
    .locals 4

    .prologue
    .line 462
    iget-object v1, p0, Lorg/icepdf/core/pobjects/Page;->library:Lorg/icepdf/core/util/Library;

    iget-object v2, p0, Lorg/icepdf/core/pobjects/Page;->entries:Ljava/util/Hashtable;

    const-string/jumbo v3, "Thumb"

    invoke-virtual {v1, v2, v3}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 463
    .local v0, "thumb":Ljava/lang/Object;
    if-eqz v0, :cond_0

    instance-of v1, v0, Lorg/icepdf/core/pobjects/Stream;

    if-eqz v1, :cond_0

    .line 464
    new-instance v1, Lorg/icepdf/core/pobjects/Thumbnail;

    iget-object v2, p0, Lorg/icepdf/core/pobjects/Page;->library:Lorg/icepdf/core/util/Library;

    iget-object v3, p0, Lorg/icepdf/core/pobjects/Page;->entries:Ljava/util/Hashtable;

    invoke-direct {v1, v2, v3}, Lorg/icepdf/core/pobjects/Thumbnail;-><init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V

    .line 466
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getTotalRotation(F)F
    .locals 4
    .param p1, "userRotation"    # F

    .prologue
    const/high16 v3, 0x43b40000    # 360.0f

    const/4 v1, 0x0

    .line 1393
    invoke-direct {p0}, Lorg/icepdf/core/pobjects/Page;->getPageRotation()F

    move-result v2

    add-float v0, v2, p1

    .line 1396
    .local v0, "totalRotation":F
    rem-float/2addr v0, v3

    .line 1398
    cmpg-float v2, v0, v1

    if-gez v2, :cond_0

    .line 1399
    add-float/2addr v0, v3

    .line 1403
    :cond_0
    const v2, -0x457ced91    # -0.001f

    cmpl-float v2, v0, v2

    if-ltz v2, :cond_2

    const v2, 0x3a83126f    # 0.001f

    cmpg-float v2, v0, v2

    if-gtz v2, :cond_2

    move v0, v1

    .line 1412
    .end local v0    # "totalRotation":F
    :cond_1
    :goto_0
    return v0

    .line 1405
    .restart local v0    # "totalRotation":F
    :cond_2
    const v1, 0x42b3fae1    # 89.99f

    cmpl-float v1, v0, v1

    if-ltz v1, :cond_3

    const v1, 0x42b40083    # 90.001f

    cmpg-float v1, v0, v1

    if-gtz v1, :cond_3

    .line 1406
    const/high16 v0, 0x42b40000    # 90.0f

    goto :goto_0

    .line 1407
    :cond_3
    const v1, 0x4333fd71    # 179.99f

    cmpl-float v1, v0, v1

    if-ltz v1, :cond_4

    const v1, 0x43340042    # 180.001f

    cmpg-float v1, v0, v1

    if-gtz v1, :cond_4

    .line 1408
    const/high16 v0, 0x43340000    # 180.0f

    goto :goto_0

    .line 1409
    :cond_4
    const v1, 0x4386feb8    # 269.99f

    cmpl-float v1, v0, v1

    if-ltz v1, :cond_1

    const v1, 0x43870021    # 270.001f

    cmpg-float v1, v0, v1

    if-gtz v1, :cond_1

    .line 1410
    const/high16 v0, 0x43870000    # 270.0f

    goto :goto_0
.end method

.method public getTrimBox()Landroid/graphics/RectF;
    .locals 4

    .prologue
    .line 1612
    iget-object v1, p0, Lorg/icepdf/core/pobjects/Page;->library:Lorg/icepdf/core/util/Library;

    iget-object v2, p0, Lorg/icepdf/core/pobjects/Page;->entries:Ljava/util/Hashtable;

    const-string/jumbo v3, "TrimBox"

    invoke-virtual {v1, v2, v3}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Vector;

    move-object v0, v1

    check-cast v0, Ljava/util/Vector;

    .line 1613
    .local v0, "boxDimensions":Ljava/util/Vector;
    if-eqz v0, :cond_0

    .line 1614
    new-instance v1, Lorg/icepdf/core/pobjects/PRectangle;

    invoke-direct {v1, v0}, Lorg/icepdf/core/pobjects/PRectangle;-><init>(Ljava/util/Vector;)V

    iput-object v1, p0, Lorg/icepdf/core/pobjects/Page;->trimBox:Lorg/icepdf/core/pobjects/PRectangle;

    .line 1618
    :cond_0
    iget-object v1, p0, Lorg/icepdf/core/pobjects/Page;->trimBox:Lorg/icepdf/core/pobjects/PRectangle;

    if-nez v1, :cond_1

    .line 1619
    invoke-virtual {p0}, Lorg/icepdf/core/pobjects/Page;->getCropBox()Landroid/graphics/RectF;

    move-result-object v1

    check-cast v1, Lorg/icepdf/core/pobjects/PRectangle;

    iput-object v1, p0, Lorg/icepdf/core/pobjects/Page;->trimBox:Lorg/icepdf/core/pobjects/PRectangle;

    .line 1621
    :cond_1
    iget-object v1, p0, Lorg/icepdf/core/pobjects/Page;->trimBox:Lorg/icepdf/core/pobjects/PRectangle;

    return-object v1
.end method

.method public declared-synchronized getViewText()Lorg/icepdf/core/pobjects/graphics/text/PageText;
    .locals 2

    .prologue
    .line 1633
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lorg/icepdf/core/pobjects/Page;->isInited:Z

    if-nez v0, :cond_0

    .line 1635
    new-instance v0, Ljava/io/File;

    const-string/jumbo v1, ""

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lorg/icepdf/core/pobjects/Page;->init(Ljava/io/File;)V

    .line 1637
    :cond_0
    iget-object v0, p0, Lorg/icepdf/core/pobjects/Page;->shapes:Lorg/icepdf/core/pobjects/graphics/Shapes;

    invoke-virtual {v0}, Lorg/icepdf/core/pobjects/graphics/Shapes;->getPageText()Lorg/icepdf/core/pobjects/graphics/text/PageText;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    .line 1633
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized init(Ljava/io/File;)V
    .locals 12
    .param p1, "folderPath"    # Ljava/io/File;

    .prologue
    .line 357
    monitor-enter p0

    :try_start_0
    iget-boolean v8, p0, Lorg/icepdf/core/pobjects/Page;->isInited:Z
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v8, :cond_0

    .line 452
    :goto_0
    monitor-exit p0

    return-void

    .line 363
    :cond_0
    :try_start_1
    invoke-static {}, Lorg/icepdf/core/util/MemoryManager;->getInstance()Lorg/icepdf/core/util/MemoryManager;

    move-result-object v8

    invoke-virtual {v8}, Lorg/icepdf/core/util/MemoryManager;->isLowMemory()Z

    move-result v5

    .line 364
    .local v5, "lowMemory":Z
    if-eqz v5, :cond_1

    sget-object v8, Lorg/icepdf/core/pobjects/Page;->logger:Ljava/util/logging/Logger;

    sget-object v9, Ljava/util/logging/Level;->FINER:Ljava/util/logging/Level;

    invoke-virtual {v8, v9}, Ljava/util/logging/Logger;->isLoggable(Ljava/util/logging/Level;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 365
    sget-object v8, Lorg/icepdf/core/pobjects/Page;->logger:Ljava/util/logging/Logger;

    const-string/jumbo v9, "Low memory conditions encountered, clearing page cache"

    invoke-virtual {v8, v9}, Ljava/util/logging/Logger;->finer(Ljava/lang/String;)V

    .line 369
    :cond_1
    invoke-direct {p0}, Lorg/icepdf/core/pobjects/Page;->initPageResources()V

    .line 376
    invoke-direct {p0}, Lorg/icepdf/core/pobjects/Page;->initPageContents()V

    .line 403
    iget-object v8, p0, Lorg/icepdf/core/pobjects/Page;->contents:Ljava/util/Vector;

    if-eqz v8, :cond_3

    .line 404
    new-instance v4, Ljava/util/Vector;

    iget-object v8, p0, Lorg/icepdf/core/pobjects/Page;->contents:Ljava/util/Vector;

    invoke-virtual {v8}, Ljava/util/Vector;->size()I

    move-result v8

    invoke-direct {v4, v8}, Ljava/util/Vector;-><init>(I)V

    .line 405
    .local v4, "inputStreamsVec":Ljava/util/Vector;, "Ljava/util/Vector<Ljava/io/InputStream;>;"
    iget-object v8, p0, Lorg/icepdf/core/pobjects/Page;->contents:Ljava/util/Vector;

    invoke-virtual {v8}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lorg/icepdf/core/pobjects/Stream;

    .line 408
    .local v7, "stream":Lorg/icepdf/core/pobjects/Stream;
    invoke-virtual {v7}, Lorg/icepdf/core/pobjects/Stream;->getInputStreamForDecodedStreamBytes()Ljava/io/InputStream;

    move-result-object v3

    .line 409
    .local v3, "input":Ljava/io/InputStream;
    invoke-virtual {v4, v3}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 445
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v3    # "input":Ljava/io/InputStream;
    .end local v4    # "inputStreamsVec":Ljava/util/Vector;, "Ljava/util/Vector<Ljava/io/InputStream;>;"
    .end local v5    # "lowMemory":Z
    .end local v7    # "stream":Lorg/icepdf/core/pobjects/Stream;
    :catch_0
    move-exception v1

    .line 448
    .local v1, "e":Ljava/lang/InterruptedException;
    const/4 v8, 0x0

    :try_start_2
    iput-boolean v8, p0, Lorg/icepdf/core/pobjects/Page;->isInited:Z

    .line 449
    sget-object v8, Lorg/icepdf/core/pobjects/Page;->logger:Ljava/util/logging/Logger;

    sget-object v9, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    const-string/jumbo v10, "Page initializing thread interrupted."

    invoke-virtual {v8, v9, v10, v1}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 357
    .end local v1    # "e":Ljava/lang/InterruptedException;
    :catchall_0
    move-exception v8

    monitor-exit p0

    throw v8

    .line 419
    .restart local v2    # "i$":Ljava/util/Iterator;
    .restart local v4    # "inputStreamsVec":Ljava/util/Vector;, "Ljava/util/Vector<Ljava/io/InputStream;>;"
    .restart local v5    # "lowMemory":Z
    :cond_2
    :try_start_3
    new-instance v6, Lorg/icepdf/core/io/SequenceInputStream;

    invoke-virtual {v4}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v8

    invoke-direct {v6, v8}, Lorg/icepdf/core/io/SequenceInputStream;-><init>(Ljava/util/Iterator;)V
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 425
    .local v6, "sis":Lorg/icepdf/core/io/SequenceInputStream;
    :try_start_4
    new-instance v0, Lorg/icepdf/core/util/ContentParser;

    iget-object v8, p0, Lorg/icepdf/core/pobjects/Page;->library:Lorg/icepdf/core/util/Library;

    iget-object v9, p0, Lorg/icepdf/core/pobjects/Page;->resources:Lorg/icepdf/core/pobjects/Resources;

    invoke-direct {v0, v8, v9}, Lorg/icepdf/core/util/ContentParser;-><init>(Lorg/icepdf/core/util/Library;Lorg/icepdf/core/pobjects/Resources;)V

    .line 426
    .local v0, "cp":Lorg/icepdf/core/util/ContentParser;
    invoke-virtual {v0, v6}, Lorg/icepdf/core/util/ContentParser;->parse(Ljava/io/InputStream;)Lorg/icepdf/core/pobjects/graphics/Shapes;

    move-result-object v8

    iput-object v8, p0, Lorg/icepdf/core/pobjects/Page;->shapes:Lorg/icepdf/core/pobjects/graphics/Shapes;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 432
    :try_start_5
    invoke-virtual {v6}, Lorg/icepdf/core/io/SequenceInputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1
    .catch Ljava/lang/InterruptedException; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 443
    .end local v0    # "cp":Lorg/icepdf/core/util/ContentParser;
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v4    # "inputStreamsVec":Ljava/util/Vector;, "Ljava/util/Vector<Ljava/io/InputStream;>;"
    .end local v6    # "sis":Lorg/icepdf/core/io/SequenceInputStream;
    :goto_2
    const/4 v8, 0x1

    :try_start_6
    iput-boolean v8, p0, Lorg/icepdf/core/pobjects/Page;->isInited:Z

    goto :goto_0

    .line 433
    .restart local v0    # "cp":Lorg/icepdf/core/util/ContentParser;
    .restart local v2    # "i$":Ljava/util/Iterator;
    .restart local v4    # "inputStreamsVec":Ljava/util/Vector;, "Ljava/util/Vector<Ljava/io/InputStream;>;"
    .restart local v6    # "sis":Lorg/icepdf/core/io/SequenceInputStream;
    :catch_1
    move-exception v1

    .line 434
    .local v1, "e":Ljava/io/IOException;
    sget-object v8, Lorg/icepdf/core/pobjects/Page;->logger:Ljava/util/logging/Logger;

    sget-object v9, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string/jumbo v10, "Error closing page stream."

    invoke-virtual {v8, v9, v10, v1}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_6
    .catch Ljava/lang/InterruptedException; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_2

    .line 427
    .end local v0    # "cp":Lorg/icepdf/core/util/ContentParser;
    .end local v1    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v1

    .line 428
    .local v1, "e":Ljava/lang/Exception;
    :try_start_7
    new-instance v8, Lorg/icepdf/core/pobjects/graphics/Shapes;

    invoke-direct {v8}, Lorg/icepdf/core/pobjects/graphics/Shapes;-><init>()V

    iput-object v8, p0, Lorg/icepdf/core/pobjects/Page;->shapes:Lorg/icepdf/core/pobjects/graphics/Shapes;

    .line 429
    sget-object v8, Lorg/icepdf/core/pobjects/Page;->logger:Ljava/util/logging/Logger;

    sget-object v9, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string/jumbo v10, "Error initializing Page."

    invoke-virtual {v8, v9, v10, v1}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 432
    :try_start_8
    invoke-virtual {v6}, Lorg/icepdf/core/io/SequenceInputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_3
    .catch Ljava/lang/InterruptedException; {:try_start_8 .. :try_end_8} :catch_0
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto :goto_2

    .line 433
    :catch_3
    move-exception v1

    .line 434
    .local v1, "e":Ljava/io/IOException;
    :try_start_9
    sget-object v8, Lorg/icepdf/core/pobjects/Page;->logger:Ljava/util/logging/Logger;

    sget-object v9, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string/jumbo v10, "Error closing page stream."

    invoke-virtual {v8, v9, v10, v1}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_9
    .catch Ljava/lang/InterruptedException; {:try_start_9 .. :try_end_9} :catch_0
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    goto :goto_2

    .line 431
    .end local v1    # "e":Ljava/io/IOException;
    :catchall_1
    move-exception v8

    .line 432
    :try_start_a
    invoke-virtual {v6}, Lorg/icepdf/core/io/SequenceInputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_4
    .catch Ljava/lang/InterruptedException; {:try_start_a .. :try_end_a} :catch_0
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    .line 435
    :goto_3
    :try_start_b
    throw v8

    .line 433
    :catch_4
    move-exception v1

    .line 434
    .restart local v1    # "e":Ljava/io/IOException;
    sget-object v9, Lorg/icepdf/core/pobjects/Page;->logger:Ljava/util/logging/Logger;

    sget-object v10, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string/jumbo v11, "Error closing page stream."

    invoke-virtual {v9, v10, v11, v1}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_3

    .line 440
    .end local v1    # "e":Ljava/io/IOException;
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v4    # "inputStreamsVec":Ljava/util/Vector;, "Ljava/util/Vector<Ljava/io/InputStream;>;"
    .end local v6    # "sis":Lorg/icepdf/core/io/SequenceInputStream;
    :cond_3
    new-instance v8, Lorg/icepdf/core/pobjects/graphics/Shapes;

    invoke-direct {v8}, Lorg/icepdf/core/pobjects/graphics/Shapes;-><init>()V

    iput-object v8, p0, Lorg/icepdf/core/pobjects/Page;->shapes:Lorg/icepdf/core/pobjects/graphics/Shapes;
    :try_end_b
    .catch Ljava/lang/InterruptedException; {:try_start_b .. :try_end_b} :catch_0
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    goto :goto_2
.end method

.method public notifyPaintPageListeners()V
    .locals 4

    .prologue
    .line 1759
    new-instance v1, Lorg/icepdf/core/events/PaintPageEvent;

    invoke-direct {v1, p0}, Lorg/icepdf/core/events/PaintPageEvent;-><init>(Lorg/icepdf/core/pobjects/Page;)V

    .line 1779
    .local v1, "evt":Lorg/icepdf/core/events/PaintPageEvent;
    iget-object v3, p0, Lorg/icepdf/core/pobjects/Page;->paintPageListeners:Ljava/util/Vector;

    invoke-virtual {v3}, Ljava/util/Vector;->size()I

    move-result v3

    add-int/lit8 v2, v3, -0x1

    .local v2, "i":I
    :goto_0
    if-ltz v2, :cond_0

    .line 1780
    iget-object v3, p0, Lorg/icepdf/core/pobjects/Page;->paintPageListeners:Ljava/util/Vector;

    invoke-virtual {v3, v2}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/icepdf/core/events/PaintPageListener;

    .line 1781
    .local v0, "client":Lorg/icepdf/core/events/PaintPageListener;
    invoke-interface {v0, v1}, Lorg/icepdf/core/events/PaintPageListener;->paintPage(Lorg/icepdf/core/events/PaintPageEvent;)V

    .line 1779
    add-int/lit8 v2, v2, -0x1

    goto :goto_0

    .line 1783
    .end local v0    # "client":Lorg/icepdf/core/events/PaintPageListener;
    :cond_0
    return-void
.end method

.method public paint(Landroid/graphics/Canvas;IIFFLjava/io/File;)V
    .locals 8
    .param p1, "g"    # Landroid/graphics/Canvas;
    .param p2, "renderHintType"    # I
    .param p3, "boundary"    # I
    .param p4, "userRotation"    # F
    .param p5, "userZoom"    # F
    .param p6, "folderPath"    # Ljava/io/File;

    .prologue
    .line 472
    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move-object v7, p6

    invoke-virtual/range {v0 .. v7}, Lorg/icepdf/core/pobjects/Page;->paint(Landroid/graphics/Canvas;IIFFLorg/icepdf/core/views/swing/PagePainter;Ljava/io/File;)V

    .line 473
    return-void
.end method

.method public paint(Landroid/graphics/Canvas;IIFFLorg/icepdf/core/views/swing/PagePainter;Ljava/io/File;)V
    .locals 10
    .param p1, "g"    # Landroid/graphics/Canvas;
    .param p2, "renderHintType"    # I
    .param p3, "boundary"    # I
    .param p4, "userRotation"    # F
    .param p5, "userZoom"    # F
    .param p6, "pagePainter"    # Lorg/icepdf/core/views/swing/PagePainter;
    .param p7, "folderPath"    # Ljava/io/File;

    .prologue
    .line 491
    const/4 v7, 0x1

    const/4 v8, 0x1

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move-object/from16 v6, p6

    move-object/from16 v9, p7

    invoke-virtual/range {v0 .. v9}, Lorg/icepdf/core/pobjects/Page;->paint(Landroid/graphics/Canvas;IIFFLorg/icepdf/core/views/swing/PagePainter;ZZLjava/io/File;)V

    .line 492
    return-void
.end method

.method public declared-synchronized paint(Landroid/graphics/Canvas;IIFFLorg/icepdf/core/views/swing/PagePainter;ZZLjava/io/File;)V
    .locals 18
    .param p1, "g"    # Landroid/graphics/Canvas;
    .param p2, "renderHintType"    # I
    .param p3, "boundary"    # I
    .param p4, "userRotation"    # F
    .param p5, "userZoom"    # F
    .param p6, "pagePainter"    # Lorg/icepdf/core/views/swing/PagePainter;
    .param p7, "paintAnnotations"    # Z
    .param p8, "paintSearchHighlight"    # Z
    .param p9, "folderPath"    # Ljava/io/File;

    .prologue
    .line 519
    monitor-enter p0

    :try_start_0
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lorg/icepdf/core/pobjects/Page;->isInited:Z

    if-nez v5, :cond_3

    if-nez p6, :cond_3

    .line 520
    move-object/from16 v0, p0

    move-object/from16 v1, p9

    invoke-virtual {v0, v1}, Lorg/icepdf/core/pobjects/Page;->init(Ljava/io/File;)V

    .line 527
    :cond_0
    move-object/from16 v4, p1

    .line 528
    .local v4, "g2":Landroid/graphics/Canvas;
    invoke-static {}, Lorg/icepdf/core/util/GraphicsRenderingHints;->getDefault()Lorg/icepdf/core/util/GraphicsRenderingHints;

    move-result-object v12

    .line 533
    .local v12, "grh":Lorg/icepdf/core/util/GraphicsRenderingHints;
    move-object/from16 v0, p0

    move/from16 v1, p3

    move/from16 v2, p4

    move/from16 v3, p5

    invoke-virtual {v0, v1, v2, v3}, Lorg/icepdf/core/pobjects/Page;->getPageTransform(IFF)Landroid/graphics/Matrix;

    move-result-object v10

    .line 535
    .local v10, "at":Landroid/graphics/Matrix;
    invoke-virtual {v4, v10}, Landroid/graphics/Canvas;->setMatrix(Landroid/graphics/Matrix;)V

    .line 537
    move-object/from16 v0, p0

    move/from16 v1, p3

    invoke-virtual {v0, v1}, Lorg/icepdf/core/pobjects/Page;->getPageBoundary(I)Lorg/icepdf/core/pobjects/PRectangle;

    move-result-object v13

    .line 538
    .local v13, "pageBoundary":Lorg/icepdf/core/pobjects/PRectangle;
    const/4 v5, 0x0

    iget v6, v13, Lorg/icepdf/core/pobjects/PRectangle;->left:F

    sub-float v15, v5, v6

    .line 539
    .local v15, "x":F
    const/4 v5, 0x0

    iget v6, v13, Lorg/icepdf/core/pobjects/PRectangle;->top:F

    iget v7, v13, Lorg/icepdf/core/pobjects/PRectangle;->bottom:F

    iget v8, v13, Lorg/icepdf/core/pobjects/PRectangle;->top:F

    sub-float/2addr v7, v8

    sub-float/2addr v6, v7

    sub-float v16, v5, v6

    .line 542
    .local v16, "y":F
    move/from16 v0, p2

    invoke-virtual {v12, v0}, Lorg/icepdf/core/util/GraphicsRenderingHints;->getPageBackgroundColor(I)Lorg/apache/poi/java/awt/Color;

    move-result-object v11

    .line 544
    .local v11, "backgroundColor":Lorg/apache/poi/java/awt/Color;
    new-instance v9, Landroid/graphics/Paint;

    invoke-direct {v9}, Landroid/graphics/Paint;-><init>()V

    .line 546
    .local v9, "g2_p":Landroid/graphics/Paint;
    if-eqz v11, :cond_1

    .line 547
    invoke-virtual {v11}, Lorg/apache/poi/java/awt/Color;->getRGB()I

    move-result v5

    invoke-virtual {v9, v5}, Landroid/graphics/Paint;->setColor(I)V

    .line 548
    const/4 v5, 0x0

    sub-float/2addr v5, v15

    const/4 v6, 0x0

    sub-float v6, v6, v16

    neg-float v7, v15

    iget v8, v13, Lorg/icepdf/core/pobjects/PRectangle;->right:F

    add-float/2addr v7, v8

    iget v8, v13, Lorg/icepdf/core/pobjects/PRectangle;->left:F

    sub-float/2addr v7, v8

    move/from16 v0, v16

    neg-float v8, v0

    iget v0, v13, Lorg/icepdf/core/pobjects/PRectangle;->bottom:F

    move/from16 v17, v0

    add-float v8, v8, v17

    iget v0, v13, Lorg/icepdf/core/pobjects/PRectangle;->top:F

    move/from16 v17, v0

    sub-float v8, v8, v17

    invoke-virtual/range {v4 .. v9}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 557
    :cond_1
    new-instance v14, Landroid/graphics/RectF;

    neg-float v5, v15

    move/from16 v0, v16

    neg-float v6, v0

    neg-float v7, v15

    iget v8, v13, Lorg/icepdf/core/pobjects/PRectangle;->right:F

    add-float/2addr v7, v8

    iget v8, v13, Lorg/icepdf/core/pobjects/PRectangle;->left:F

    sub-float/2addr v7, v8

    move/from16 v0, v16

    neg-float v8, v0

    iget v0, v13, Lorg/icepdf/core/pobjects/PRectangle;->bottom:F

    move/from16 v17, v0

    add-float v8, v8, v17

    iget v0, v13, Lorg/icepdf/core/pobjects/PRectangle;->top:F

    move/from16 v17, v0

    sub-float v8, v8, v17

    invoke-direct {v14, v5, v6, v7, v8}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 572
    .local v14, "rect":Landroid/graphics/RectF;
    invoke-virtual {v4, v14}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/RectF;)Z

    .line 577
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/icepdf/core/pobjects/Page;->shapes:Lorg/icepdf/core/pobjects/graphics/Shapes;

    if-eqz v5, :cond_2

    .line 583
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/icepdf/core/pobjects/Page;->shapes:Lorg/icepdf/core/pobjects/graphics/Shapes;

    move-object/from16 v0, p0

    invoke-virtual {v5, v0}, Lorg/icepdf/core/pobjects/graphics/Shapes;->setPageParent(Lorg/icepdf/core/pobjects/Page;)V

    .line 584
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/icepdf/core/pobjects/Page;->shapes:Lorg/icepdf/core/pobjects/graphics/Shapes;

    move-object/from16 v0, p6

    invoke-virtual {v5, v4, v0}, Lorg/icepdf/core/pobjects/graphics/Shapes;->paint(Landroid/graphics/Canvas;Lorg/icepdf/core/views/swing/PagePainter;)V

    .line 585
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/icepdf/core/pobjects/Page;->shapes:Lorg/icepdf/core/pobjects/graphics/Shapes;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Lorg/icepdf/core/pobjects/graphics/Shapes;->setPageParent(Lorg/icepdf/core/pobjects/Page;)V

    .line 638
    :cond_2
    invoke-virtual/range {p0 .. p0}, Lorg/icepdf/core/pobjects/Page;->notifyPaintPageListeners()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 639
    .end local v4    # "g2":Landroid/graphics/Canvas;
    .end local v9    # "g2_p":Landroid/graphics/Paint;
    .end local v10    # "at":Landroid/graphics/Matrix;
    .end local v11    # "backgroundColor":Lorg/apache/poi/java/awt/Color;
    .end local v12    # "grh":Lorg/icepdf/core/util/GraphicsRenderingHints;
    .end local v13    # "pageBoundary":Lorg/icepdf/core/pobjects/PRectangle;
    .end local v14    # "rect":Landroid/graphics/RectF;
    .end local v15    # "x":F
    .end local v16    # "y":F
    :goto_0
    monitor-exit p0

    return-void

    .line 521
    :cond_3
    :try_start_1
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lorg/icepdf/core/pobjects/Page;->isInited:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez v5, :cond_0

    goto :goto_0

    .line 519
    :catchall_0
    move-exception v5

    monitor-exit p0

    throw v5
.end method

.method public reduceMemory()V
    .locals 1

    .prologue
    .line 1735
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lorg/icepdf/core/pobjects/Page;->dispose(Z)V

    .line 1736
    return-void
.end method

.method public removePaintPageListener(Lorg/icepdf/core/events/PaintPageListener;)V
    .locals 2
    .param p1, "listener"    # Lorg/icepdf/core/events/PaintPageListener;

    .prologue
    .line 1749
    iget-object v1, p0, Lorg/icepdf/core/pobjects/Page;->paintPageListeners:Ljava/util/Vector;

    monitor-enter v1

    .line 1750
    :try_start_0
    iget-object v0, p0, Lorg/icepdf/core/pobjects/Page;->paintPageListeners:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1751
    iget-object v0, p0, Lorg/icepdf/core/pobjects/Page;->paintPageListeners:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->removeElement(Ljava/lang/Object;)Z

    .line 1754
    :cond_0
    monitor-exit v1

    .line 1755
    return-void

    .line 1754
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1380
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "PAGE= "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/icepdf/core/pobjects/Page;->entries:Ljava/util/Hashtable;

    invoke-virtual {v1}, Ljava/util/Hashtable;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
