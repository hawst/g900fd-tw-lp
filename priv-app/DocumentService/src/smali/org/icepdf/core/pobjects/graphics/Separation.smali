.class public Lorg/icepdf/core/pobjects/graphics/Separation;
.super Lorg/icepdf/core/pobjects/graphics/PColorSpace;
.source "Separation.java"


# static fields
.field public static final COLORANT_ALL:Ljava/lang/String; = "All"

.field public static final COLORANT_NONE:Ljava/lang/String; = "None"


# instance fields
.field protected alternate:Lorg/icepdf/core/pobjects/graphics/PColorSpace;

.field protected namedColor:Lorg/apache/poi/java/awt/Color;

.field private tint:F

.field protected tintTransform:Lorg/icepdf/core/pobjects/functions/Function;


# direct methods
.method protected constructor <init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 3
    .param p1, "l"    # Lorg/icepdf/core/util/Library;
    .param p2, "h"    # Ljava/util/Hashtable;
    .param p3, "name"    # Ljava/lang/Object;
    .param p4, "alternateSpace"    # Ljava/lang/Object;
    .param p5, "tintTransform"    # Ljava/lang/Object;

    .prologue
    .line 97
    invoke-direct {p0, p1, p2}, Lorg/icepdf/core/pobjects/graphics/PColorSpace;-><init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V

    .line 84
    const/high16 v2, 0x3f800000    # 1.0f

    iput v2, p0, Lorg/icepdf/core/pobjects/graphics/Separation;->tint:F

    .line 98
    invoke-static {p1, p4}, Lorg/icepdf/core/pobjects/graphics/Separation;->getColorSpace(Lorg/icepdf/core/util/Library;Ljava/lang/Object;)Lorg/icepdf/core/pobjects/graphics/PColorSpace;

    move-result-object v2

    iput-object v2, p0, Lorg/icepdf/core/pobjects/graphics/Separation;->alternate:Lorg/icepdf/core/pobjects/graphics/PColorSpace;

    .line 100
    invoke-virtual {p1, p5}, Lorg/icepdf/core/util/Library;->getObject(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-static {p1, v2}, Lorg/icepdf/core/pobjects/functions/Function;->getFunction(Lorg/icepdf/core/util/Library;Ljava/lang/Object;)Lorg/icepdf/core/pobjects/functions/Function;

    move-result-object v2

    iput-object v2, p0, Lorg/icepdf/core/pobjects/graphics/Separation;->tintTransform:Lorg/icepdf/core/pobjects/functions/Function;

    .line 102
    instance-of v2, p3, Lorg/icepdf/core/pobjects/Name;

    if-eqz v2, :cond_0

    .line 103
    check-cast p3, Lorg/icepdf/core/pobjects/Name;

    .end local p3    # "name":Ljava/lang/Object;
    invoke-virtual {p3}, Lorg/icepdf/core/pobjects/Name;->getName()Ljava/lang/String;

    move-result-object v0

    .line 106
    .local v0, "colorName":Ljava/lang/String;
    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lorg/icepdf/core/util/ColorUtil;->convertNamedColor(Ljava/lang/String;)I

    move-result v1

    .line 107
    .local v1, "colorVaue":I
    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    .line 108
    new-instance v2, Lorg/apache/poi/java/awt/Color;

    invoke-direct {v2, v1}, Lorg/apache/poi/java/awt/Color;-><init>(I)V

    iput-object v2, p0, Lorg/icepdf/core/pobjects/graphics/Separation;->namedColor:Lorg/apache/poi/java/awt/Color;

    .line 120
    .end local v0    # "colorName":Ljava/lang/String;
    .end local v1    # "colorVaue":I
    :cond_0
    :goto_0
    return-void

    .line 111
    .restart local v0    # "colorName":Ljava/lang/String;
    .restart local v1    # "colorVaue":I
    :cond_1
    const-string/jumbo v2, "All"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 113
    const-string/jumbo v2, "None"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    goto :goto_0
.end method


# virtual methods
.method public getColor([F)Lorg/apache/poi/java/awt/Color;
    .locals 10
    .param p1, "components"    # [F

    .prologue
    const/4 v7, 0x0

    .line 141
    iget-object v5, p0, Lorg/icepdf/core/pobjects/graphics/Separation;->namedColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v5, :cond_0

    .line 143
    aget v5, p1, v7

    iput v5, p0, Lorg/icepdf/core/pobjects/graphics/Separation;->tint:F

    .line 144
    iget-object v5, p0, Lorg/icepdf/core/pobjects/graphics/Separation;->namedColor:Lorg/apache/poi/java/awt/Color;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Lorg/apache/poi/java/awt/Color;->getComponents([F)[F

    move-result-object v1

    .line 145
    .local v1, "colour":[F
    new-instance v5, Lorg/apache/poi/java/awt/Color;

    aget v6, v1, v7

    const/4 v7, 0x1

    aget v7, v1, v7

    const/4 v8, 0x2

    aget v8, v1, v8

    iget v9, p0, Lorg/icepdf/core/pobjects/graphics/Separation;->tint:F

    invoke-direct {v5, v6, v7, v8, v9}, Lorg/apache/poi/java/awt/Color;-><init>(FFFF)V

    iput-object v5, p0, Lorg/icepdf/core/pobjects/graphics/Separation;->namedColor:Lorg/apache/poi/java/awt/Color;

    .line 146
    iget-object v5, p0, Lorg/icepdf/core/pobjects/graphics/Separation;->namedColor:Lorg/apache/poi/java/awt/Color;

    .line 172
    .end local v1    # "colour":[F
    :goto_0
    return-object v5

    .line 153
    :cond_0
    iget-object v5, p0, Lorg/icepdf/core/pobjects/graphics/Separation;->tintTransform:Lorg/icepdf/core/pobjects/functions/Function;

    if-nez v5, :cond_2

    .line 154
    aget v1, p1, v7

    .line 156
    .local v1, "colour":F
    iget-object v5, p0, Lorg/icepdf/core/pobjects/graphics/Separation;->alternate:Lorg/icepdf/core/pobjects/graphics/PColorSpace;

    invoke-virtual {v5}, Lorg/icepdf/core/pobjects/graphics/PColorSpace;->getNumComponents()I

    move-result v5

    new-array v0, v5, [F

    .line 157
    .local v0, "alternateColour":[F
    const/4 v2, 0x0

    .local v2, "i":I
    iget-object v5, p0, Lorg/icepdf/core/pobjects/graphics/Separation;->alternate:Lorg/icepdf/core/pobjects/graphics/PColorSpace;

    invoke-virtual {v5}, Lorg/icepdf/core/pobjects/graphics/PColorSpace;->getNumComponents()I

    move-result v3

    .local v3, "max":I
    :goto_1
    if-ge v2, v3, :cond_1

    .line 158
    aput v1, v0, v2

    .line 157
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 160
    :cond_1
    iget-object v5, p0, Lorg/icepdf/core/pobjects/graphics/Separation;->alternate:Lorg/icepdf/core/pobjects/graphics/PColorSpace;

    invoke-virtual {v5, v0}, Lorg/icepdf/core/pobjects/graphics/PColorSpace;->getColor([F)Lorg/apache/poi/java/awt/Color;

    move-result-object v5

    goto :goto_0

    .line 162
    .end local v0    # "alternateColour":[F
    .end local v1    # "colour":F
    .end local v2    # "i":I
    .end local v3    # "max":I
    :cond_2
    iget-object v5, p0, Lorg/icepdf/core/pobjects/graphics/Separation;->alternate:Lorg/icepdf/core/pobjects/graphics/PColorSpace;

    if-eqz v5, :cond_3

    .line 163
    iget-object v5, p0, Lorg/icepdf/core/pobjects/graphics/Separation;->tintTransform:Lorg/icepdf/core/pobjects/functions/Function;

    invoke-virtual {v5, p1}, Lorg/icepdf/core/pobjects/functions/Function;->calculate([F)[F

    move-result-object v4

    .line 165
    .local v4, "y":[F
    if-eqz v4, :cond_3

    array-length v5, v4

    if-lez v5, :cond_3

    .line 166
    iget-object v5, p0, Lorg/icepdf/core/pobjects/graphics/Separation;->alternate:Lorg/icepdf/core/pobjects/graphics/PColorSpace;

    invoke-static {v4}, Lorg/icepdf/core/pobjects/graphics/Separation;->reverse([F)[F

    move-result-object v6

    invoke-virtual {v5, v6}, Lorg/icepdf/core/pobjects/graphics/PColorSpace;->getColor([F)Lorg/apache/poi/java/awt/Color;

    move-result-object v5

    goto :goto_0

    .line 172
    .end local v4    # "y":[F
    :cond_3
    iget-object v5, p0, Lorg/icepdf/core/pobjects/graphics/Separation;->namedColor:Lorg/apache/poi/java/awt/Color;

    goto :goto_0
.end method

.method public getNumComponents()I
    .locals 1

    .prologue
    .line 128
    const/4 v0, 0x1

    return v0
.end method

.method public getTint()F
    .locals 1

    .prologue
    .line 176
    iget v0, p0, Lorg/icepdf/core/pobjects/graphics/Separation;->tint:F

    return v0
.end method
