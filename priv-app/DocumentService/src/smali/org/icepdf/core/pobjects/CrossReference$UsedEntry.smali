.class public Lorg/icepdf/core/pobjects/CrossReference$UsedEntry;
.super Lorg/icepdf/core/pobjects/CrossReference$Entry;
.source "CrossReference.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/icepdf/core/pobjects/CrossReference;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "UsedEntry"
.end annotation


# instance fields
.field private m_iGenerationNumber:I

.field private m_lFilePositionOfObject:J

.field final synthetic this$0:Lorg/icepdf/core/pobjects/CrossReference;


# direct methods
.method constructor <init>(Lorg/icepdf/core/pobjects/CrossReference;IJI)V
    .locals 1
    .param p2, "objectNumber"    # I
    .param p3, "filePositionOfObject"    # J
    .param p5, "generationNumber"    # I

    .prologue
    .line 282
    iput-object p1, p0, Lorg/icepdf/core/pobjects/CrossReference$UsedEntry;->this$0:Lorg/icepdf/core/pobjects/CrossReference;

    .line 283
    const/4 v0, 0x1

    invoke-direct {p0, v0, p2}, Lorg/icepdf/core/pobjects/CrossReference$Entry;-><init>(II)V

    .line 284
    iput-wide p3, p0, Lorg/icepdf/core/pobjects/CrossReference$UsedEntry;->m_lFilePositionOfObject:J

    .line 285
    iput p5, p0, Lorg/icepdf/core/pobjects/CrossReference$UsedEntry;->m_iGenerationNumber:I

    .line 286
    return-void
.end method


# virtual methods
.method public getFilePositionOfObject()J
    .locals 4

    .prologue
    .line 289
    iget-wide v0, p0, Lorg/icepdf/core/pobjects/CrossReference$UsedEntry;->m_lFilePositionOfObject:J

    iget-object v2, p0, Lorg/icepdf/core/pobjects/CrossReference$UsedEntry;->this$0:Lorg/icepdf/core/pobjects/CrossReference;

    iget v2, v2, Lorg/icepdf/core/pobjects/CrossReference;->offset:I

    int-to-long v2, v2

    add-long/2addr v0, v2

    return-wide v0
.end method

.method public getGenerationNumber()I
    .locals 1

    .prologue
    .line 293
    iget v0, p0, Lorg/icepdf/core/pobjects/CrossReference$UsedEntry;->m_iGenerationNumber:I

    return v0
.end method
