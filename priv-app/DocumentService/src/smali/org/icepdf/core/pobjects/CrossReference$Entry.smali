.class public Lorg/icepdf/core/pobjects/CrossReference$Entry;
.super Ljava/lang/Object;
.source "CrossReference.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/icepdf/core/pobjects/CrossReference;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Entry"
.end annotation


# static fields
.field public static final TYPE_COMPRESSED:I = 0x2

.field public static final TYPE_FREE:I = 0x0

.field public static final TYPE_USED:I = 0x1


# instance fields
.field private m_iObjectNumber:I

.field private m_iType:I


# direct methods
.method constructor <init>(II)V
    .locals 0
    .param p1, "type"    # I
    .param p2, "objectNumber"    # I

    .prologue
    .line 245
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 246
    iput p1, p0, Lorg/icepdf/core/pobjects/CrossReference$Entry;->m_iType:I

    .line 247
    iput p2, p0, Lorg/icepdf/core/pobjects/CrossReference$Entry;->m_iObjectNumber:I

    .line 248
    return-void
.end method


# virtual methods
.method getObjectNumber()I
    .locals 1

    .prologue
    .line 255
    iget v0, p0, Lorg/icepdf/core/pobjects/CrossReference$Entry;->m_iObjectNumber:I

    return v0
.end method

.method getType()I
    .locals 1

    .prologue
    .line 251
    iget v0, p0, Lorg/icepdf/core/pobjects/CrossReference$Entry;->m_iType:I

    return v0
.end method
