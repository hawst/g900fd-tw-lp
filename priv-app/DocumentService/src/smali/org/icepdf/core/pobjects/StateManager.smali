.class public Lorg/icepdf/core/pobjects/StateManager;
.super Ljava/lang/Object;
.source "StateManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/icepdf/core/pobjects/StateManager$1;,
        Lorg/icepdf/core/pobjects/StateManager$PObjectComparatorByReferenceObjectNumber;
    }
.end annotation


# static fields
.field private static final logger:Ljava/util/logging/Logger;


# instance fields
.field private changes:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lorg/icepdf/core/pobjects/Reference;",
            "Lorg/icepdf/core/pobjects/PObject;",
            ">;"
        }
    .end annotation
.end field

.field private nextReferenceNumber:I

.field private trailer:Lorg/icepdf/core/pobjects/PTrailer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 37
    const-class v0, Lorg/icepdf/core/pobjects/StateManager;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Lorg/icepdf/core/pobjects/StateManager;->logger:Ljava/util/logging/Logger;

    return-void
.end method

.method public constructor <init>(Lorg/icepdf/core/pobjects/PTrailer;)V
    .locals 1
    .param p1, "trailer"    # Lorg/icepdf/core/pobjects/PTrailer;

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    iput-object p1, p0, Lorg/icepdf/core/pobjects/StateManager;->trailer:Lorg/icepdf/core/pobjects/PTrailer;

    .line 56
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lorg/icepdf/core/pobjects/StateManager;->changes:Ljava/util/HashMap;

    .line 60
    if-eqz p1, :cond_0

    .line 61
    invoke-virtual {p1}, Lorg/icepdf/core/pobjects/PTrailer;->getNumberOfObjects()I

    move-result v0

    iput v0, p0, Lorg/icepdf/core/pobjects/StateManager;->nextReferenceNumber:I

    .line 63
    :cond_0
    return-void
.end method


# virtual methods
.method public addChange(Lorg/icepdf/core/pobjects/PObject;)V
    .locals 2
    .param p1, "pObject"    # Lorg/icepdf/core/pobjects/PObject;

    .prologue
    .line 85
    iget-object v0, p0, Lorg/icepdf/core/pobjects/StateManager;->changes:Ljava/util/HashMap;

    invoke-virtual {p1}, Lorg/icepdf/core/pobjects/PObject;->getReference()Lorg/icepdf/core/pobjects/Reference;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 86
    return-void
.end method

.method public contains(Lorg/icepdf/core/pobjects/Reference;)Z
    .locals 1
    .param p1, "reference"    # Lorg/icepdf/core/pobjects/Reference;

    .prologue
    .line 96
    iget-object v0, p0, Lorg/icepdf/core/pobjects/StateManager;->changes:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public getNewReferencNumber()Lorg/icepdf/core/pobjects/Reference;
    .locals 3

    .prologue
    .line 74
    new-instance v0, Lorg/icepdf/core/pobjects/Reference;

    iget v1, p0, Lorg/icepdf/core/pobjects/StateManager;->nextReferenceNumber:I

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lorg/icepdf/core/pobjects/Reference;-><init>(II)V

    .line 75
    .local v0, "newReference":Lorg/icepdf/core/pobjects/Reference;
    iget v1, p0, Lorg/icepdf/core/pobjects/StateManager;->nextReferenceNumber:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/icepdf/core/pobjects/StateManager;->nextReferenceNumber:I

    .line 76
    return-object v0
.end method

.method public getTrailer()Lorg/icepdf/core/pobjects/PTrailer;
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lorg/icepdf/core/pobjects/StateManager;->trailer:Lorg/icepdf/core/pobjects/PTrailer;

    return-object v0
.end method

.method public isChanged()Z
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lorg/icepdf/core/pobjects/StateManager;->changes:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public iteratorSortedByObjectNumber()Ljava/util/Iterator;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Lorg/icepdf/core/pobjects/PObject;",
            ">;"
        }
    .end annotation

    .prologue
    .line 120
    iget-object v3, p0, Lorg/icepdf/core/pobjects/StateManager;->changes:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v1

    .line 132
    .local v1, "coll":Ljava/util/Collection;, "Ljava/util/Collection<Lorg/icepdf/core/pobjects/PObject;>;"
    invoke-interface {v1}, Ljava/util/Collection;->size()I

    move-result v3

    new-array v3, v3, [Lorg/icepdf/core/pobjects/PObject;

    invoke-interface {v1, v3}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lorg/icepdf/core/pobjects/PObject;

    .line 133
    .local v0, "arr":[Lorg/icepdf/core/pobjects/PObject;
    new-instance v3, Lorg/icepdf/core/pobjects/StateManager$PObjectComparatorByReferenceObjectNumber;

    const/4 v4, 0x0

    invoke-direct {v3, v4}, Lorg/icepdf/core/pobjects/StateManager$PObjectComparatorByReferenceObjectNumber;-><init>(Lorg/icepdf/core/pobjects/StateManager$1;)V

    invoke-static {v0, v3}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    .line 134
    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    .line 135
    .local v2, "sortedList":Ljava/util/List;, "Ljava/util/List<Lorg/icepdf/core/pobjects/PObject;>;"
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    return-object v3
.end method

.method public removeChange(Lorg/icepdf/core/pobjects/PObject;)V
    .locals 2
    .param p1, "pObject"    # Lorg/icepdf/core/pobjects/PObject;

    .prologue
    .line 105
    iget-object v0, p0, Lorg/icepdf/core/pobjects/StateManager;->changes:Ljava/util/HashMap;

    invoke-virtual {p1}, Lorg/icepdf/core/pobjects/PObject;->getReference()Lorg/icepdf/core/pobjects/Reference;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 106
    return-void
.end method
