.class public Lorg/icepdf/core/pobjects/graphics/ExtGState;
.super Lorg/icepdf/core/pobjects/Dictionary;
.source "ExtGState.java"


# static fields
.field private static final logger:Ljava/util/logging/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 223
    const-class v0, Lorg/icepdf/core/pobjects/graphics/ExtGState;

    invoke-virtual {v0}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Lorg/icepdf/core/pobjects/graphics/ExtGState;->logger:Ljava/util/logging/Logger;

    return-void
.end method

.method public constructor <init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V
    .locals 0
    .param p1, "library"    # Lorg/icepdf/core/util/Library;
    .param p2, "graphicsState"    # Ljava/util/Hashtable;

    .prologue
    .line 234
    invoke-direct {p0, p1, p2}, Lorg/icepdf/core/pobjects/Dictionary;-><init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V

    .line 235
    return-void
.end method


# virtual methods
.method getLineCapStyle()Ljava/lang/Number;
    .locals 1

    .prologue
    .line 254
    const-string/jumbo v0, "LC"

    invoke-virtual {p0, v0}, Lorg/icepdf/core/pobjects/graphics/ExtGState;->getNumber(Ljava/lang/String;)Ljava/lang/Number;

    move-result-object v0

    return-object v0
.end method

.method getLineDashPattern()Ljava/util/Vector;
    .locals 13

    .prologue
    const/4 v10, 0x0

    .line 284
    const/4 v3, 0x0

    .line 285
    .local v3, "dashPattern":Ljava/util/Vector;
    invoke-static {v10}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    .line 286
    .local v5, "dashPhase":Ljava/lang/Number;
    const/4 v1, 0x0

    .line 287
    .local v1, "dashArray":[F
    iget-object v10, p0, Lorg/icepdf/core/pobjects/graphics/ExtGState;->entries:Ljava/util/Hashtable;

    const-string/jumbo v11, "D"

    invoke-virtual {v10, v11}, Ljava/util/Hashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_0

    iget-object v10, p0, Lorg/icepdf/core/pobjects/graphics/ExtGState;->entries:Ljava/util/Hashtable;

    new-instance v11, Lorg/icepdf/core/pobjects/Name;

    const-string/jumbo v12, "D"

    invoke-direct {v11, v12}, Lorg/icepdf/core/pobjects/Name;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v11}, Ljava/util/Hashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 289
    :cond_0
    :try_start_0
    iget-object v10, p0, Lorg/icepdf/core/pobjects/graphics/ExtGState;->entries:Ljava/util/Hashtable;

    const-string/jumbo v11, "D"

    invoke-virtual {v10, v11}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Vector;

    .line 290
    .local v2, "dashData":Ljava/util/Vector;
    if-nez v2, :cond_1

    .line 292
    iget-object v10, p0, Lorg/icepdf/core/pobjects/graphics/ExtGState;->entries:Ljava/util/Hashtable;

    new-instance v11, Lorg/icepdf/core/pobjects/Name;

    const-string/jumbo v12, "D"

    invoke-direct {v11, v12}, Lorg/icepdf/core/pobjects/Name;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v11}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "dashData":Ljava/util/Vector;
    check-cast v2, Ljava/util/Vector;

    .line 296
    .restart local v2    # "dashData":Ljava/util/Vector;
    :cond_1
    const/4 v10, 0x1

    invoke-virtual {v2, v10}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v10

    move-object v0, v10

    check-cast v0, Ljava/lang/Number;

    move-object v5, v0

    .line 298
    const/4 v10, 0x0

    invoke-virtual {v2, v10}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/Vector;

    .line 301
    .local v6, "dashVector":Ljava/util/Vector;
    invoke-virtual {v6}, Ljava/util/Vector;->size()I

    move-result v10

    if-lez v10, :cond_2

    .line 303
    invoke-virtual {v6}, Ljava/util/Vector;->size()I

    move-result v9

    .line 304
    .local v9, "sz":I
    new-array v1, v9, [F

    .line 305
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_0
    if-ge v8, v9, :cond_3

    .line 306
    invoke-virtual {v6, v8}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Number;

    invoke-virtual {v10}, Ljava/lang/Number;->floatValue()F

    move-result v10

    aput v10, v1, v8

    .line 305
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 311
    .end local v8    # "i":I
    .end local v9    # "sz":I
    :cond_2
    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    .line 312
    const/4 v1, 0x0

    .line 314
    :cond_3
    new-instance v4, Ljava/util/Vector;

    const/4 v10, 0x2

    invoke-direct {v4, v10}, Ljava/util/Vector;-><init>(I)V
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 315
    .end local v3    # "dashPattern":Ljava/util/Vector;
    .local v4, "dashPattern":Ljava/util/Vector;
    :try_start_1
    invoke-virtual {v4, v1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 316
    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/ClassCastException; {:try_start_1 .. :try_end_1} :catch_1

    move-object v3, v4

    .line 322
    .end local v2    # "dashData":Ljava/util/Vector;
    .end local v4    # "dashPattern":Ljava/util/Vector;
    .end local v6    # "dashVector":Ljava/util/Vector;
    .restart local v3    # "dashPattern":Ljava/util/Vector;
    :cond_4
    :goto_1
    return-object v3

    .line 318
    :catch_0
    move-exception v7

    .line 319
    .local v7, "e":Ljava/lang/ClassCastException;
    :goto_2
    sget-object v10, Lorg/icepdf/core/pobjects/graphics/ExtGState;->logger:Ljava/util/logging/Logger;

    sget-object v11, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string/jumbo v12, "Dash pattern syntax error: "

    invoke-virtual {v10, v11, v12, v7}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 318
    .end local v3    # "dashPattern":Ljava/util/Vector;
    .end local v7    # "e":Ljava/lang/ClassCastException;
    .restart local v2    # "dashData":Ljava/util/Vector;
    .restart local v4    # "dashPattern":Ljava/util/Vector;
    .restart local v6    # "dashVector":Ljava/util/Vector;
    :catch_1
    move-exception v7

    move-object v3, v4

    .end local v4    # "dashPattern":Ljava/util/Vector;
    .restart local v3    # "dashPattern":Ljava/util/Vector;
    goto :goto_2
.end method

.method getLineJoinStyle()Ljava/lang/Number;
    .locals 1

    .prologue
    .line 264
    const-string/jumbo v0, "LJ"

    invoke-virtual {p0, v0}, Lorg/icepdf/core/pobjects/graphics/ExtGState;->getNumber(Ljava/lang/String;)Ljava/lang/Number;

    move-result-object v0

    return-object v0
.end method

.method getLineWidth()Ljava/lang/Number;
    .locals 1

    .prologue
    .line 244
    const-string/jumbo v0, "LW"

    invoke-virtual {p0, v0}, Lorg/icepdf/core/pobjects/graphics/ExtGState;->getNumber(Ljava/lang/String;)Ljava/lang/Number;

    move-result-object v0

    return-object v0
.end method

.method getMiterLimit()Ljava/lang/Number;
    .locals 1

    .prologue
    .line 274
    const-string/jumbo v0, "ML"

    invoke-virtual {p0, v0}, Lorg/icepdf/core/pobjects/graphics/ExtGState;->getNumber(Ljava/lang/String;)Ljava/lang/Number;

    move-result-object v0

    return-object v0
.end method

.method getNonStrokingAlphConstant()Ljava/lang/Number;
    .locals 1

    .prologue
    .line 342
    const-string/jumbo v0, "ca"

    invoke-virtual {p0, v0}, Lorg/icepdf/core/pobjects/graphics/ExtGState;->getNumber(Ljava/lang/String;)Ljava/lang/Number;

    move-result-object v0

    return-object v0
.end method

.method getOverprint()Ljava/lang/Boolean;
    .locals 2

    .prologue
    .line 358
    const-string/jumbo v1, "OP"

    invoke-virtual {p0, v1}, Lorg/icepdf/core/pobjects/graphics/ExtGState;->getObject(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 359
    .local v0, "o":Ljava/lang/Object;
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 360
    check-cast v0, Ljava/lang/String;

    .end local v0    # "o":Ljava/lang/Object;
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    .line 364
    :goto_0
    return-object v0

    .line 361
    .restart local v0    # "o":Ljava/lang/Object;
    :cond_0
    instance-of v1, v0, Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    .line 362
    check-cast v0, Ljava/lang/Boolean;

    goto :goto_0

    .line 364
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method getOverprintFill()Ljava/lang/Boolean;
    .locals 2

    .prologue
    .line 375
    const-string/jumbo v1, "op"

    invoke-virtual {p0, v1}, Lorg/icepdf/core/pobjects/graphics/ExtGState;->getObject(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 376
    .local v0, "o":Ljava/lang/Object;
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 377
    check-cast v0, Ljava/lang/String;

    .end local v0    # "o":Ljava/lang/Object;
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    .line 381
    :goto_0
    return-object v0

    .line 378
    .restart local v0    # "o":Ljava/lang/Object;
    :cond_0
    instance-of v1, v0, Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    .line 379
    check-cast v0, Ljava/lang/Boolean;

    goto :goto_0

    .line 381
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method getOverprintMode()Ljava/lang/Number;
    .locals 1

    .prologue
    .line 390
    const-string/jumbo v0, "OPM"

    invoke-virtual {p0, v0}, Lorg/icepdf/core/pobjects/graphics/ExtGState;->getNumber(Ljava/lang/String;)Ljava/lang/Number;

    move-result-object v0

    return-object v0
.end method

.method public getSMask()Lorg/icepdf/core/pobjects/graphics/SoftMask;
    .locals 5

    .prologue
    .line 395
    iget-object v2, p0, Lorg/icepdf/core/pobjects/graphics/ExtGState;->library:Lorg/icepdf/core/util/Library;

    iget-object v3, p0, Lorg/icepdf/core/pobjects/graphics/ExtGState;->entries:Ljava/util/Hashtable;

    const-string/jumbo v4, "SMask"

    invoke-virtual {v2, v3, v4}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    .line 396
    .local v1, "tmp":Ljava/lang/Object;
    if-eqz v1, :cond_0

    instance-of v2, v1, Ljava/util/Hashtable;

    if-eqz v2, :cond_0

    .line 398
    new-instance v0, Lorg/icepdf/core/pobjects/graphics/SoftMask;

    iget-object v2, p0, Lorg/icepdf/core/pobjects/graphics/ExtGState;->library:Lorg/icepdf/core/util/Library;

    check-cast v1, Ljava/util/Hashtable;

    .end local v1    # "tmp":Ljava/lang/Object;
    invoke-direct {v0, v2, v1}, Lorg/icepdf/core/pobjects/graphics/SoftMask;-><init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V

    .line 401
    :goto_0
    return-object v0

    .restart local v1    # "tmp":Ljava/lang/Object;
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method getStrokingAlphConstant()Ljava/lang/Number;
    .locals 1

    .prologue
    .line 332
    const-string/jumbo v0, "CA"

    invoke-virtual {p0, v0}, Lorg/icepdf/core/pobjects/graphics/ExtGState;->getNumber(Ljava/lang/String;)Ljava/lang/Number;

    move-result-object v0

    return-object v0
.end method
