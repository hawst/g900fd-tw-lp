.class public abstract Lorg/icepdf/core/pobjects/graphics/PColorSpace;
.super Lorg/icepdf/core/pobjects/Dictionary;
.source "PColorSpace.java"


# static fields
.field private static final logger:Ljava/util/logging/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    const-class v0, Lorg/icepdf/core/pobjects/graphics/PColorSpace;

    invoke-virtual {v0}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Lorg/icepdf/core/pobjects/graphics/PColorSpace;->logger:Ljava/util/logging/Logger;

    return-void
.end method

.method constructor <init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V
    .locals 0
    .param p1, "l"    # Lorg/icepdf/core/util/Library;
    .param p2, "h"    # Ljava/util/Hashtable;

    .prologue
    .line 53
    invoke-direct {p0, p1, p2}, Lorg/icepdf/core/pobjects/Dictionary;-><init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V

    .line 54
    return-void
.end method

.method public static getColorSpace(Lorg/icepdf/core/util/Library;F)Lorg/icepdf/core/pobjects/graphics/PColorSpace;
    .locals 2
    .param p0, "library"    # Lorg/icepdf/core/util/Library;
    .param p1, "n"    # F

    .prologue
    const/4 v1, 0x0

    .line 138
    const/high16 v0, 0x40400000    # 3.0f

    cmpl-float v0, p1, v0

    if-nez v0, :cond_0

    .line 139
    new-instance v0, Lorg/icepdf/core/pobjects/graphics/DeviceRGB;

    invoke-direct {v0, p0, v1}, Lorg/icepdf/core/pobjects/graphics/DeviceRGB;-><init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V

    .line 143
    :goto_0
    return-object v0

    .line 140
    :cond_0
    const/high16 v0, 0x40800000    # 4.0f

    cmpl-float v0, p1, v0

    if-nez v0, :cond_1

    .line 141
    new-instance v0, Lorg/icepdf/core/pobjects/graphics/DeviceCMYK;

    invoke-direct {v0, p0, v1}, Lorg/icepdf/core/pobjects/graphics/DeviceCMYK;-><init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V

    goto :goto_0

    .line 143
    :cond_1
    new-instance v0, Lorg/icepdf/core/pobjects/graphics/DeviceGray;

    invoke-direct {v0, p0, v1}, Lorg/icepdf/core/pobjects/graphics/DeviceGray;-><init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V

    goto :goto_0
.end method

.method public static getColorSpace(Lorg/icepdf/core/util/Library;Ljava/lang/Object;)Lorg/icepdf/core/pobjects/graphics/PColorSpace;
    .locals 9
    .param p0, "library"    # Lorg/icepdf/core/util/Library;
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 62
    if-eqz p1, :cond_16

    .line 63
    instance-of v0, p1, Lorg/icepdf/core/pobjects/Reference;

    if-eqz v0, :cond_0

    .line 64
    check-cast p1, Lorg/icepdf/core/pobjects/Reference;

    .end local p1    # "o":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lorg/icepdf/core/util/Library;->getObject(Lorg/icepdf/core/pobjects/Reference;)Ljava/lang/Object;

    move-result-object p1

    .line 66
    .restart local p1    # "o":Ljava/lang/Object;
    :cond_0
    instance-of v0, p1, Lorg/icepdf/core/pobjects/Name;

    if-eqz v0, :cond_7

    .line 67
    const-string/jumbo v0, "DeviceGray"

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string/jumbo v0, "G"

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 68
    :cond_1
    new-instance v0, Lorg/icepdf/core/pobjects/graphics/DeviceGray;

    invoke-direct {v0, p0, v2}, Lorg/icepdf/core/pobjects/graphics/DeviceGray;-><init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V

    .line 125
    :goto_0
    return-object v0

    .line 69
    :cond_2
    const-string/jumbo v0, "DeviceRGB"

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    const-string/jumbo v0, "RGB"

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 70
    :cond_3
    new-instance v0, Lorg/icepdf/core/pobjects/graphics/DeviceRGB;

    invoke-direct {v0, p0, v2}, Lorg/icepdf/core/pobjects/graphics/DeviceRGB;-><init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V

    goto :goto_0

    .line 71
    :cond_4
    const-string/jumbo v0, "DeviceCMYK"

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    const-string/jumbo v0, "CMYK"

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 72
    :cond_5
    new-instance v0, Lorg/icepdf/core/pobjects/graphics/DeviceCMYK;

    invoke-direct {v0, p0, v2}, Lorg/icepdf/core/pobjects/graphics/DeviceCMYK;-><init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V

    goto :goto_0

    .line 73
    :cond_6
    const-string/jumbo v0, "Pattern"

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 74
    new-instance v0, Lorg/icepdf/core/pobjects/graphics/PatternColor;

    invoke-direct {v0, p0, v2}, Lorg/icepdf/core/pobjects/graphics/PatternColor;-><init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V

    goto :goto_0

    .line 76
    :cond_7
    instance-of v0, p1, Ljava/util/Vector;

    if-eqz v0, :cond_14

    move-object v8, p1

    .line 77
    check-cast v8, Ljava/util/Vector;

    .line 78
    .local v8, "v":Ljava/util/Vector;
    invoke-virtual {v8, v3}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    const-string/jumbo v1, "Indexed"

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_8

    invoke-virtual {v8, v3}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    const-string/jumbo v1, "I"

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 80
    :cond_8
    new-instance v0, Lorg/icepdf/core/pobjects/graphics/Indexed;

    invoke-direct {v0, p0, v2, v8}, Lorg/icepdf/core/pobjects/graphics/Indexed;-><init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;Ljava/util/Vector;)V

    goto :goto_0

    .line 81
    :cond_9
    invoke-virtual {v8, v3}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    const-string/jumbo v1, "CalRGB"

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 82
    new-instance v7, Lorg/icepdf/core/pobjects/graphics/CalRGB;

    invoke-virtual {v8, v4}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Hashtable;

    invoke-direct {v7, p0, v0}, Lorg/icepdf/core/pobjects/graphics/CalRGB;-><init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V

    move-object v0, v7

    goto :goto_0

    .line 83
    :cond_a
    invoke-virtual {v8, v3}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    const-string/jumbo v1, "Lab"

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 84
    new-instance v7, Lorg/icepdf/core/pobjects/graphics/Lab;

    invoke-virtual {v8, v4}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Hashtable;

    invoke-direct {v7, p0, v0}, Lorg/icepdf/core/pobjects/graphics/Lab;-><init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V

    move-object v0, v7

    goto/16 :goto_0

    .line 85
    :cond_b
    invoke-virtual {v8, v3}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    const-string/jumbo v1, "Separation"

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 86
    new-instance v0, Lorg/icepdf/core/pobjects/graphics/Separation;

    invoke-virtual {v8, v4}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v8, v5}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v8, v6}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v5

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lorg/icepdf/core/pobjects/graphics/Separation;-><init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 92
    :cond_c
    invoke-virtual {v8, v3}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    const-string/jumbo v1, "DeviceN"

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 93
    new-instance v0, Lorg/icepdf/core/pobjects/graphics/DeviceN;

    invoke-virtual {v8, v4}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v8, v5}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v8, v6}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v8}, Ljava/util/Vector;->size()I

    move-result v1

    const/4 v6, 0x4

    if-le v1, v6, :cond_d

    const/4 v1, 0x4

    invoke-virtual {v8, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v6

    :goto_1
    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lorg/icepdf/core/pobjects/graphics/DeviceN;-><init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    goto/16 :goto_0

    :cond_d
    move-object v6, v2

    goto :goto_1

    .line 100
    :cond_e
    invoke-virtual {v8, v3}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    const-string/jumbo v1, "ICCBased"

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 104
    invoke-virtual {v8, v4}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/icepdf/core/pobjects/Reference;

    invoke-virtual {p0, v0}, Lorg/icepdf/core/util/Library;->getICCBased(Lorg/icepdf/core/pobjects/Reference;)Lorg/icepdf/core/pobjects/graphics/ICCBased;

    move-result-object v0

    goto/16 :goto_0

    .line 105
    :cond_f
    invoke-virtual {v8, v3}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    const-string/jumbo v1, "DeviceRGB"

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 106
    new-instance v0, Lorg/icepdf/core/pobjects/graphics/DeviceRGB;

    invoke-direct {v0, p0, v2}, Lorg/icepdf/core/pobjects/graphics/DeviceRGB;-><init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V

    goto/16 :goto_0

    .line 107
    :cond_10
    invoke-virtual {v8, v3}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    const-string/jumbo v1, "DeviceCMYK"

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 108
    new-instance v0, Lorg/icepdf/core/pobjects/graphics/DeviceCMYK;

    invoke-direct {v0, p0, v2}, Lorg/icepdf/core/pobjects/graphics/DeviceCMYK;-><init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V

    goto/16 :goto_0

    .line 109
    :cond_11
    invoke-virtual {v8, v3}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    const-string/jumbo v1, "DeviceGray"

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 110
    new-instance v0, Lorg/icepdf/core/pobjects/graphics/DeviceRGB;

    invoke-direct {v0, p0, v2}, Lorg/icepdf/core/pobjects/graphics/DeviceRGB;-><init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V

    goto/16 :goto_0

    .line 111
    :cond_12
    invoke-virtual {v8, v3}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    const-string/jumbo v1, "Pattern"

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 112
    new-instance v7, Lorg/icepdf/core/pobjects/graphics/PatternColor;

    invoke-direct {v7, p0, v2}, Lorg/icepdf/core/pobjects/graphics/PatternColor;-><init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V

    .line 113
    .local v7, "patternColour":Lorg/icepdf/core/pobjects/graphics/PatternColor;
    invoke-virtual {v8}, Ljava/util/Vector;->size()I

    move-result v0

    if-le v0, v4, :cond_13

    .line 114
    invoke-virtual {v8, v4}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {p0, v0}, Lorg/icepdf/core/pobjects/graphics/PColorSpace;->getColorSpace(Lorg/icepdf/core/util/Library;Ljava/lang/Object;)Lorg/icepdf/core/pobjects/graphics/PColorSpace;

    move-result-object v0

    invoke-virtual {v7, v0}, Lorg/icepdf/core/pobjects/graphics/PatternColor;->setPColorSpace(Lorg/icepdf/core/pobjects/graphics/PColorSpace;)V

    :cond_13
    move-object v0, v7

    .line 116
    goto/16 :goto_0

    .line 118
    .end local v7    # "patternColour":Lorg/icepdf/core/pobjects/graphics/PatternColor;
    .end local v8    # "v":Ljava/util/Vector;
    :cond_14
    instance-of v0, p1, Ljava/util/Hashtable;

    if-eqz v0, :cond_15

    .line 119
    new-instance v7, Lorg/icepdf/core/pobjects/graphics/PatternColor;

    move-object v0, p1

    check-cast v0, Ljava/util/Hashtable;

    invoke-direct {v7, p0, v0}, Lorg/icepdf/core/pobjects/graphics/PatternColor;-><init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V

    move-object v0, v7

    goto/16 :goto_0

    .line 121
    :cond_15
    sget-object v0, Lorg/icepdf/core/pobjects/graphics/PColorSpace;->logger:Ljava/util/logging/Logger;

    sget-object v1, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    invoke-virtual {v0, v1}, Ljava/util/logging/Logger;->isLoggable(Ljava/util/logging/Level;)Z

    move-result v0

    if-eqz v0, :cond_16

    .line 122
    sget-object v0, Lorg/icepdf/core/pobjects/graphics/PColorSpace;->logger:Ljava/util/logging/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Unsupported Colorspace: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/logging/Logger;->fine(Ljava/lang/String;)V

    .line 125
    :cond_16
    new-instance v0, Lorg/icepdf/core/pobjects/graphics/DeviceGray;

    invoke-direct {v0, p0, v2}, Lorg/icepdf/core/pobjects/graphics/DeviceGray;-><init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V

    goto/16 :goto_0
.end method

.method public static reverse([F)[F
    .locals 3
    .param p0, "f"    # [F

    .prologue
    .line 164
    array-length v2, p0

    new-array v1, v2, [F

    .line 166
    .local v1, "n":[F
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v2, p0

    if-ge v0, v2, :cond_0

    .line 167
    array-length v2, p0

    sub-int/2addr v2, v0

    add-int/lit8 v2, v2, -0x1

    aget v2, p0, v2

    aput v2, v1, v0

    .line 166
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 171
    :cond_0
    return-object v1
.end method

.method public static reverseInPlace([F)V
    .locals 4
    .param p0, "f"    # [F

    .prologue
    .line 175
    array-length v3, p0

    div-int/lit8 v1, v3, 0x2

    .line 176
    .local v1, "num":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 177
    aget v2, p0, v0

    .line 178
    .local v2, "tmp":F
    array-length v3, p0

    add-int/lit8 v3, v3, -0x1

    sub-int/2addr v3, v0

    aget v3, p0, v3

    aput v3, p0, v0

    .line 179
    array-length v3, p0

    add-int/lit8 v3, v3, -0x1

    sub-int/2addr v3, v0

    aput v2, p0, v3

    .line 176
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 181
    .end local v2    # "tmp":F
    :cond_0
    return-void
.end method

.method public static reverseInPlace([I)V
    .locals 4
    .param p0, "f"    # [I

    .prologue
    .line 184
    array-length v3, p0

    div-int/lit8 v1, v3, 0x2

    .line 185
    .local v1, "num":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 186
    aget v2, p0, v0

    .line 187
    .local v2, "tmp":I
    array-length v3, p0

    add-int/lit8 v3, v3, -0x1

    sub-int/2addr v3, v0

    aget v3, p0, v3

    aput v3, p0, v0

    .line 188
    array-length v3, p0

    add-int/lit8 v3, v3, -0x1

    sub-int/2addr v3, v0

    aput v2, p0, v3

    .line 185
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 190
    .end local v2    # "tmp":I
    :cond_0
    return-void
.end method


# virtual methods
.method public abstract getColor([F)Lorg/apache/poi/java/awt/Color;
.end method

.method public getDescription()Ljava/lang/String;
    .locals 3

    .prologue
    .line 43
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    .line 44
    .local v1, "name":Ljava/lang/String;
    const/16 v2, 0x2e

    invoke-virtual {v1, v2}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v0

    .line 45
    .local v0, "index":I
    add-int/lit8 v2, v0, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method public abstract getNumComponents()I
.end method

.method public normaliseComponentsToFloats([I[FF)V
    .locals 3
    .param p1, "in"    # [I
    .param p2, "out"    # [F
    .param p3, "maxval"    # F

    .prologue
    .line 154
    invoke-virtual {p0}, Lorg/icepdf/core/pobjects/graphics/PColorSpace;->getNumComponents()I

    move-result v0

    .line 155
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_0

    .line 156
    aget v2, p1, v1

    int-to-float v2, v2

    div-float/2addr v2, p3

    aput v2, p2, v1

    .line 155
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 157
    :cond_0
    return-void
.end method
