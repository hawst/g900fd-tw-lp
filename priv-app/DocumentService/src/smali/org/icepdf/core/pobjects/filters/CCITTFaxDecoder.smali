.class public Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;
.super Ljava/lang/Object;
.source "CCITTFaxDecoder.java"


# static fields
.field static additionalMakeup:[S

.field static black:[S

.field static flipTable:[B

.field static initBlack:[S

.field static table1:[I

.field static table2:[I

.field static twoBitBlack:[S

.field static twoDCodes:[B

.field static white:[S


# instance fields
.field private align:Z

.field private bitPointer:I

.field private bytePointer:I

.field private changingElemSize:I

.field private currChangingElems:[I

.field private data:[B

.field private fillBits:Z

.field private fillOrder:I

.field private lastChangingElement:I

.field private prevChangingElems:[I

.field private w:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/16 v2, 0x10

    const/16 v1, 0x9

    .line 86
    new-array v0, v1, [I

    fill-array-data v0, :array_0

    sput-object v0, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->table1:[I

    .line 98
    new-array v0, v1, [I

    fill-array-data v0, :array_1

    sput-object v0, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->table2:[I

    .line 110
    const/16 v0, 0x100

    new-array v0, v0, [B

    fill-array-data v0, :array_2

    sput-object v0, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->flipTable:[B

    .line 131
    const/16 v0, 0x400

    new-array v0, v0, [S

    fill-array-data v0, :array_3

    sput-object v0, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->white:[S

    .line 390
    new-array v0, v2, [S

    fill-array-data v0, :array_4

    sput-object v0, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->additionalMakeup:[S

    .line 396
    new-array v0, v2, [S

    fill-array-data v0, :array_5

    sput-object v0, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->initBlack:[S

    .line 402
    const/4 v0, 0x4

    new-array v0, v0, [S

    fill-array-data v0, :array_6

    sput-object v0, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->twoBitBlack:[S

    .line 405
    const/16 v0, 0x200

    new-array v0, v0, [S

    fill-array-data v0, :array_7

    sput-object v0, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->black:[S

    .line 535
    const/16 v0, 0x80

    new-array v0, v0, [B

    fill-array-data v0, :array_8

    sput-object v0, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->twoDCodes:[B

    return-void

    .line 86
    nop

    :array_0
    .array-data 4
        0x0
        0x1
        0x3
        0x7
        0xf
        0x1f
        0x3f
        0x7f
        0xff
    .end array-data

    .line 98
    :array_1
    .array-data 4
        0x0
        0x80
        0xc0
        0xe0
        0xf0
        0xf8
        0xfc
        0xfe
        0xff
    .end array-data

    .line 110
    :array_2
    .array-data 1
        0x0t
        -0x80t
        0x40t
        -0x40t
        0x20t
        -0x60t
        0x60t
        -0x20t
        0x10t
        -0x70t
        0x50t
        -0x30t
        0x30t
        -0x50t
        0x70t
        -0x10t
        0x8t
        -0x78t
        0x48t
        -0x38t
        0x28t
        -0x58t
        0x68t
        -0x18t
        0x18t
        -0x68t
        0x58t
        -0x28t
        0x38t
        -0x48t
        0x78t
        -0x8t
        0x4t
        -0x7ct
        0x44t
        -0x3ct
        0x24t
        -0x5ct
        0x64t
        -0x1ct
        0x14t
        -0x6ct
        0x54t
        -0x2ct
        0x34t
        -0x4ct
        0x74t
        -0xct
        0xct
        -0x74t
        0x4ct
        -0x34t
        0x2ct
        -0x54t
        0x6ct
        -0x14t
        0x1ct
        -0x64t
        0x5ct
        -0x24t
        0x3ct
        -0x44t
        0x7ct
        -0x4t
        0x2t
        -0x7et
        0x42t
        -0x3et
        0x22t
        -0x5et
        0x62t
        -0x1et
        0x12t
        -0x6et
        0x52t
        -0x2et
        0x32t
        -0x4et
        0x72t
        -0xet
        0xat
        -0x76t
        0x4at
        -0x36t
        0x2at
        -0x56t
        0x6at
        -0x16t
        0x1at
        -0x66t
        0x5at
        -0x26t
        0x3at
        -0x46t
        0x7at
        -0x6t
        0x6t
        -0x7at
        0x46t
        -0x3at
        0x26t
        -0x5at
        0x66t
        -0x1at
        0x16t
        -0x6at
        0x56t
        -0x2at
        0x36t
        -0x4at
        0x76t
        -0xat
        0xet
        -0x72t
        0x4et
        -0x32t
        0x2et
        -0x52t
        0x6et
        -0x12t
        0x1et
        -0x62t
        0x5et
        -0x22t
        0x3et
        -0x42t
        0x7et
        -0x2t
        0x1t
        -0x7ft
        0x41t
        -0x3ft
        0x21t
        -0x5ft
        0x61t
        -0x1ft
        0x11t
        -0x6ft
        0x51t
        -0x2ft
        0x31t
        -0x4ft
        0x71t
        -0xft
        0x9t
        -0x77t
        0x49t
        -0x37t
        0x29t
        -0x57t
        0x69t
        -0x17t
        0x19t
        -0x67t
        0x59t
        -0x27t
        0x39t
        -0x47t
        0x79t
        -0x7t
        0x5t
        -0x7bt
        0x45t
        -0x3bt
        0x25t
        -0x5bt
        0x65t
        -0x1bt
        0x15t
        -0x6bt
        0x55t
        -0x2bt
        0x35t
        -0x4bt
        0x75t
        -0xbt
        0xdt
        -0x73t
        0x4dt
        -0x33t
        0x2dt
        -0x53t
        0x6dt
        -0x13t
        0x1dt
        -0x63t
        0x5dt
        -0x23t
        0x3dt
        -0x43t
        0x7dt
        -0x3t
        0x3t
        -0x7dt
        0x43t
        -0x3dt
        0x23t
        -0x5dt
        0x63t
        -0x1dt
        0x13t
        -0x6dt
        0x53t
        -0x2dt
        0x33t
        -0x4dt
        0x73t
        -0xdt
        0xbt
        -0x75t
        0x4bt
        -0x35t
        0x2bt
        -0x55t
        0x6bt
        -0x15t
        0x1bt
        -0x65t
        0x5bt
        -0x25t
        0x3bt
        -0x45t
        0x7bt
        -0x5t
        0x7t
        -0x79t
        0x47t
        -0x39t
        0x27t
        -0x59t
        0x67t
        -0x19t
        0x17t
        -0x69t
        0x57t
        -0x29t
        0x37t
        -0x49t
        0x77t
        -0x9t
        0xft
        -0x71t
        0x4ft
        -0x31t
        0x2ft
        -0x51t
        0x6ft
        -0x11t
        0x1ft
        -0x61t
        0x5ft
        -0x21t
        0x3ft
        -0x41t
        0x7ft
        -0x1t
    .end array-data

    .line 131
    :array_3
    .array-data 2
        0x191es
        0x1900s
        0x1900s
        0x1900s
        0xc99s
        0xc99s
        0xc99s
        0xc99s
        0x3b0s
        0x3b0s
        0x3b0s
        0x3b0s
        0x3d0s
        0x3d0s
        0x3d0s
        0x3d0s
        0x5b0s
        0x5b0s
        0x5b0s
        0x5b0s
        0x5d0s
        0x5d0s
        0x5d0s
        0x5d0s
        0x2ces
        0x2ces
        0x2ces
        0x2ces
        0x2ces
        0x2ces
        0x2ces
        0x2ces
        0x2ees
        0x2ees
        0x2ees
        0x2ees
        0x2ees
        0x2ees
        0x2ees
        0x2ees
        0x5f0s
        0x5f0s
        0x5f0s
        0x5f0s
        0x610s
        0x610s
        0x610s
        0x610s
        0x1acs
        0x1acs
        0x1acs
        0x1acs
        0x1acs
        0x1acs
        0x1acs
        0x1acs
        0x1acs
        0x1acs
        0x1acs
        0x1acs
        0x1acs
        0x1acs
        0x1acs
        0x1acs
        0x28es
        0x28es
        0x28es
        0x28es
        0x28es
        0x28es
        0x28es
        0x28es
        0x430s
        0x430s
        0x430s
        0x430s
        0x450s
        0x450s
        0x450s
        0x450s
        0x470s
        0x470s
        0x470s
        0x470s
        0x490s
        0x490s
        0x490s
        0x490s
        0x4b0s
        0x4b0s
        0x4b0s
        0x4b0s
        0x4d0s
        0x4d0s
        0x4d0s
        0x4d0s
        0x26es
        0x26es
        0x26es
        0x26es
        0x26es
        0x26es
        0x26es
        0x26es
        0x3f0s
        0x3f0s
        0x3f0s
        0x3f0s
        0x410s
        0x410s
        0x410s
        0x410s
        0x2cs
        0x2cs
        0x2cs
        0x2cs
        0x2cs
        0x2cs
        0x2cs
        0x2cs
        0x2cs
        0x2cs
        0x2cs
        0x2cs
        0x2cs
        0x2cs
        0x2cs
        0x2cs
        0x18cs
        0x18cs
        0x18cs
        0x18cs
        0x18cs
        0x18cs
        0x18cs
        0x18cs
        0x18cs
        0x18cs
        0x18cs
        0x18cs
        0x18cs
        0x18cs
        0x18cs
        0x18cs
        0x6b0s
        0x6b0s
        0x6b0s
        0x6b0s
        0x6d0s
        0x6d0s
        0x6d0s
        0x6d0s
        0x34es
        0x34es
        0x34es
        0x34es
        0x34es
        0x34es
        0x34es
        0x34es
        0x4f0s
        0x4f0s
        0x4f0s
        0x4f0s
        0x510s
        0x510s
        0x510s
        0x510s
        0x530s
        0x530s
        0x530s
        0x530s
        0x550s
        0x550s
        0x550s
        0x550s
        0x570s
        0x570s
        0x570s
        0x570s
        0x590s
        0x590s
        0x590s
        0x590s
        0x2aes
        0x2aes
        0x2aes
        0x2aes
        0x2aes
        0x2aes
        0x2aes
        0x2aes
        0x38es
        0x38es
        0x38es
        0x38es
        0x38es
        0x38es
        0x38es
        0x38es
        0x7b0s
        0x7b0s
        0x7b0s
        0x7b0s
        0x7d0s
        0x7d0s
        0x7d0s
        0x7d0s
        0x7f0s
        0x7f0s
        0x7f0s
        0x7f0s
        0x10s
        0x10s
        0x10s
        0x10s
        0x2811s
        0x2811s
        0x2811s
        0x2811s
        0x3011s
        0x3011s
        0x3011s
        0x3011s
        0x14as
        0x14as
        0x14as
        0x14as
        0x14as
        0x14as
        0x14as
        0x14as
        0x14as
        0x14as
        0x14as
        0x14as
        0x14as
        0x14as
        0x14as
        0x14as
        0x14as
        0x14as
        0x14as
        0x14as
        0x14as
        0x14as
        0x14as
        0x14as
        0x14as
        0x14as
        0x14as
        0x14as
        0x14as
        0x14as
        0x14as
        0x14as
        0x16as
        0x16as
        0x16as
        0x16as
        0x16as
        0x16as
        0x16as
        0x16as
        0x16as
        0x16as
        0x16as
        0x16as
        0x16as
        0x16as
        0x16as
        0x16as
        0x16as
        0x16as
        0x16as
        0x16as
        0x16as
        0x16as
        0x16as
        0x16as
        0x16as
        0x16as
        0x16as
        0x16as
        0x16as
        0x16as
        0x16as
        0x16as
        0x36es
        0x36es
        0x36es
        0x36es
        0x36es
        0x36es
        0x36es
        0x36es
        0x770s
        0x770s
        0x770s
        0x770s
        0x790s
        0x790s
        0x790s
        0x790s
        -0x47eds
        -0x47eds
        -0x3feds
        -0x3feds
        -0x37eds
        -0x37eds
        -0x27eds
        -0x27eds
        0x24es
        0x24es
        0x24es
        0x24es
        0x24es
        0x24es
        0x24es
        0x24es
        0x30es
        0x30es
        0x30es
        0x30es
        0x30es
        0x30es
        0x30es
        0x30es
        0x630s
        0x630s
        0x630s
        0x630s
        0x650s
        0x650s
        0x650s
        0x650s
        0x670s
        0x670s
        0x670s
        0x670s
        0x690s
        0x690s
        0x690s
        0x690s
        0x32es
        0x32es
        0x32es
        0x32es
        0x32es
        0x32es
        0x32es
        0x32es
        0x6f0s
        0x6f0s
        0x6f0s
        0x6f0s
        0x710s
        0x710s
        0x710s
        0x710s
        0x730s
        0x730s
        0x730s
        0x730s
        0x750s
        0x750s
        0x750s
        0x750s
        0x180ds
        0x180ds
        0x180ds
        0x180ds
        0x180ds
        0x180ds
        0x180ds
        0x180ds
        0x180ds
        0x180ds
        0x180ds
        0x180ds
        0x180ds
        0x180ds
        0x180ds
        0x180ds
        -0x2ff3s
        -0x2ff3s
        -0x2ff3s
        -0x2ff3s
        -0x2ff3s
        -0x2ff3s
        -0x2ff3s
        -0x2ff3s
        -0x2ff3s
        -0x2ff3s
        -0x2ff3s
        -0x2ff3s
        -0x2ff3s
        -0x2ff3s
        -0x2ff3s
        -0x2ff3s
        0x3811s
        0x3811s
        0x3811s
        0x3811s
        0x4011s
        0x4011s
        0x4011s
        0x4011s
        0x5813s
        0x5813s
        0x6013s
        0x6013s
        0x5011s
        0x5011s
        0x5011s
        0x5011s
        0x4811s
        0x4811s
        0x4811s
        0x4811s
        0x6813s
        0x6813s
        0x7013s
        0x7013s
        0x7813s
        0x7813s
        -0x7feds
        -0x7feds
        -0x77eds
        -0x77eds
        -0x6feds
        -0x6feds
        -0x67eds
        -0x67eds
        -0x5feds
        -0x5feds
        -0x57eds
        -0x57eds
        -0x4feds
        -0x4feds
        0x200fs
        0x200fs
        0x200fs
        0x200fs
        0x200fs
        0x200fs
        0x200fs
        0x200fs
        0x48s
        0x48s
        0x48s
        0x48s
        0x48s
        0x48s
        0x48s
        0x48s
        0x48s
        0x48s
        0x48s
        0x48s
        0x48s
        0x48s
        0x48s
        0x48s
        0x48s
        0x48s
        0x48s
        0x48s
        0x48s
        0x48s
        0x48s
        0x48s
        0x48s
        0x48s
        0x48s
        0x48s
        0x48s
        0x48s
        0x48s
        0x48s
        0x48s
        0x48s
        0x48s
        0x48s
        0x48s
        0x48s
        0x48s
        0x48s
        0x48s
        0x48s
        0x48s
        0x48s
        0x48s
        0x48s
        0x48s
        0x48s
        0x48s
        0x48s
        0x48s
        0x48s
        0x48s
        0x48s
        0x48s
        0x48s
        0x48s
        0x48s
        0x48s
        0x48s
        0x48s
        0x48s
        0x48s
        0x48s
        0x68s
        0x68s
        0x68s
        0x68s
        0x68s
        0x68s
        0x68s
        0x68s
        0x68s
        0x68s
        0x68s
        0x68s
        0x68s
        0x68s
        0x68s
        0x68s
        0x68s
        0x68s
        0x68s
        0x68s
        0x68s
        0x68s
        0x68s
        0x68s
        0x68s
        0x68s
        0x68s
        0x68s
        0x68s
        0x68s
        0x68s
        0x68s
        0x68s
        0x68s
        0x68s
        0x68s
        0x68s
        0x68s
        0x68s
        0x68s
        0x68s
        0x68s
        0x68s
        0x68s
        0x68s
        0x68s
        0x68s
        0x68s
        0x68s
        0x68s
        0x68s
        0x68s
        0x68s
        0x68s
        0x68s
        0x68s
        0x68s
        0x68s
        0x68s
        0x68s
        0x68s
        0x68s
        0x68s
        0x68s
        0x100bs
        0x100bs
        0x100bs
        0x100bs
        0x100bs
        0x100bs
        0x100bs
        0x100bs
        0x100bs
        0x100bs
        0x100bs
        0x100bs
        0x100bs
        0x100bs
        0x100bs
        0x100bs
        0x100bs
        0x100bs
        0x100bs
        0x100bs
        0x100bs
        0x100bs
        0x100bs
        0x100bs
        0x100bs
        0x100bs
        0x100bs
        0x100bs
        0x100bs
        0x100bs
        0x100bs
        0x100bs
        0x10as
        0x10as
        0x10as
        0x10as
        0x10as
        0x10as
        0x10as
        0x10as
        0x10as
        0x10as
        0x10as
        0x10as
        0x10as
        0x10as
        0x10as
        0x10as
        0x10as
        0x10as
        0x10as
        0x10as
        0x10as
        0x10as
        0x10as
        0x10as
        0x10as
        0x10as
        0x10as
        0x10as
        0x10as
        0x10as
        0x10as
        0x10as
        0x12as
        0x12as
        0x12as
        0x12as
        0x12as
        0x12as
        0x12as
        0x12as
        0x12as
        0x12as
        0x12as
        0x12as
        0x12as
        0x12as
        0x12as
        0x12as
        0x12as
        0x12as
        0x12as
        0x12as
        0x12as
        0x12as
        0x12as
        0x12as
        0x12as
        0x12as
        0x12as
        0x12as
        0x12as
        0x12as
        0x12as
        0x12as
        0x20cs
        0x20cs
        0x20cs
        0x20cs
        0x20cs
        0x20cs
        0x20cs
        0x20cs
        0x20cs
        0x20cs
        0x20cs
        0x20cs
        0x20cs
        0x20cs
        0x20cs
        0x20cs
        0x22cs
        0x22cs
        0x22cs
        0x22cs
        0x22cs
        0x22cs
        0x22cs
        0x22cs
        0x22cs
        0x22cs
        0x22cs
        0x22cs
        0x22cs
        0x22cs
        0x22cs
        0x22cs
        0x88s
        0x88s
        0x88s
        0x88s
        0x88s
        0x88s
        0x88s
        0x88s
        0x88s
        0x88s
        0x88s
        0x88s
        0x88s
        0x88s
        0x88s
        0x88s
        0x88s
        0x88s
        0x88s
        0x88s
        0x88s
        0x88s
        0x88s
        0x88s
        0x88s
        0x88s
        0x88s
        0x88s
        0x88s
        0x88s
        0x88s
        0x88s
        0x88s
        0x88s
        0x88s
        0x88s
        0x88s
        0x88s
        0x88s
        0x88s
        0x88s
        0x88s
        0x88s
        0x88s
        0x88s
        0x88s
        0x88s
        0x88s
        0x88s
        0x88s
        0x88s
        0x88s
        0x88s
        0x88s
        0x88s
        0x88s
        0x88s
        0x88s
        0x88s
        0x88s
        0x88s
        0x88s
        0x88s
        0x88s
        0xa8s
        0xa8s
        0xa8s
        0xa8s
        0xa8s
        0xa8s
        0xa8s
        0xa8s
        0xa8s
        0xa8s
        0xa8s
        0xa8s
        0xa8s
        0xa8s
        0xa8s
        0xa8s
        0xa8s
        0xa8s
        0xa8s
        0xa8s
        0xa8s
        0xa8s
        0xa8s
        0xa8s
        0xa8s
        0xa8s
        0xa8s
        0xa8s
        0xa8s
        0xa8s
        0xa8s
        0xa8s
        0xa8s
        0xa8s
        0xa8s
        0xa8s
        0xa8s
        0xa8s
        0xa8s
        0xa8s
        0xa8s
        0xa8s
        0xa8s
        0xa8s
        0xa8s
        0xa8s
        0xa8s
        0xa8s
        0xa8s
        0xa8s
        0xa8s
        0xa8s
        0xa8s
        0xa8s
        0xa8s
        0xa8s
        0xa8s
        0xa8s
        0xa8s
        0xa8s
        0xa8s
        0xa8s
        0xa8s
        0xa8s
        0x1ccs
        0x1ccs
        0x1ccs
        0x1ccs
        0x1ccs
        0x1ccs
        0x1ccs
        0x1ccs
        0x1ccs
        0x1ccs
        0x1ccs
        0x1ccs
        0x1ccs
        0x1ccs
        0x1ccs
        0x1ccs
        0x1ecs
        0x1ecs
        0x1ecs
        0x1ecs
        0x1ecs
        0x1ecs
        0x1ecs
        0x1ecs
        0x1ecs
        0x1ecs
        0x1ecs
        0x1ecs
        0x1ecs
        0x1ecs
        0x1ecs
        0x1ecs
        0x80bs
        0x80bs
        0x80bs
        0x80bs
        0x80bs
        0x80bs
        0x80bs
        0x80bs
        0x80bs
        0x80bs
        0x80bs
        0x80bs
        0x80bs
        0x80bs
        0x80bs
        0x80bs
        0x80bs
        0x80bs
        0x80bs
        0x80bs
        0x80bs
        0x80bs
        0x80bs
        0x80bs
        0x80bs
        0x80bs
        0x80bs
        0x80bs
        0x80bs
        0x80bs
        0x80bs
        0x80bs
        0xc8s
        0xc8s
        0xc8s
        0xc8s
        0xc8s
        0xc8s
        0xc8s
        0xc8s
        0xc8s
        0xc8s
        0xc8s
        0xc8s
        0xc8s
        0xc8s
        0xc8s
        0xc8s
        0xc8s
        0xc8s
        0xc8s
        0xc8s
        0xc8s
        0xc8s
        0xc8s
        0xc8s
        0xc8s
        0xc8s
        0xc8s
        0xc8s
        0xc8s
        0xc8s
        0xc8s
        0xc8s
        0xc8s
        0xc8s
        0xc8s
        0xc8s
        0xc8s
        0xc8s
        0xc8s
        0xc8s
        0xc8s
        0xc8s
        0xc8s
        0xc8s
        0xc8s
        0xc8s
        0xc8s
        0xc8s
        0xc8s
        0xc8s
        0xc8s
        0xc8s
        0xc8s
        0xc8s
        0xc8s
        0xc8s
        0xc8s
        0xc8s
        0xc8s
        0xc8s
        0xc8s
        0xc8s
        0xc8s
        0xc8s
        0xe8s
        0xe8s
        0xe8s
        0xe8s
        0xe8s
        0xe8s
        0xe8s
        0xe8s
        0xe8s
        0xe8s
        0xe8s
        0xe8s
        0xe8s
        0xe8s
        0xe8s
        0xe8s
        0xe8s
        0xe8s
        0xe8s
        0xe8s
        0xe8s
        0xe8s
        0xe8s
        0xe8s
        0xe8s
        0xe8s
        0xe8s
        0xe8s
        0xe8s
        0xe8s
        0xe8s
        0xe8s
        0xe8s
        0xe8s
        0xe8s
        0xe8s
        0xe8s
        0xe8s
        0xe8s
        0xe8s
        0xe8s
        0xe8s
        0xe8s
        0xe8s
        0xe8s
        0xe8s
        0xe8s
        0xe8s
        0xe8s
        0xe8s
        0xe8s
        0xe8s
        0xe8s
        0xe8s
        0xe8s
        0xe8s
        0xe8s
        0xe8s
        0xe8s
        0xe8s
        0xe8s
        0xe8s
        0xe8s
        0xe8s
    .end array-data

    .line 390
    :array_4
    .array-data 2
        0x7007s
        0x7007s
        0x7c08s
        -0x7ff7s
        -0x7bf7s
        -0x77f7s
        -0x73f7s
        -0x6ff7s
        0x7407s
        0x7407s
        0x7807s
        0x7807s
        -0x6bf7s
        -0x67f7s
        -0x63f7s
        -0x5ff7s
    .end array-data

    .line 396
    :array_5
    .array-data 2
        0xc9as
        0x190cs
        0xc8s
        0xa8s
        0x26s
        0x26s
        0x86s
        0x86s
        0x64s
        0x64s
        0x64s
        0x64s
        0x44s
        0x44s
        0x44s
        0x44s
    .end array-data

    .line 402
    :array_6
    .array-data 2
        0x124s
        0x104s
        0xe2s
        0xe2s
    .end array-data

    .line 405
    :array_7
    .array-data 2
        0x3es
        0x3es
        0x1es
        0x1es
        0x0s
        0x0s
        0x0s
        0x0s
        0x0s
        0x0s
        0x0s
        0x0s
        0x0s
        0x0s
        0x0s
        0x0s
        0x0s
        0x0s
        0x0s
        0x0s
        0x0s
        0x0s
        0x0s
        0x0s
        0x0s
        0x0s
        0x0s
        0x0s
        0x0s
        0x0s
        0x0s
        0x0s
        0xc99s
        0xc99s
        0xc99s
        0xc99s
        0xc99s
        0xc99s
        0xc99s
        0xc99s
        0xc99s
        0xc99s
        0xc99s
        0xc99s
        0xc99s
        0xc99s
        0xc99s
        0xc99s
        0xc99s
        0xc99s
        0xc99s
        0xc99s
        0xc99s
        0xc99s
        0xc99s
        0xc99s
        0xc99s
        0xc99s
        0xc99s
        0xc99s
        0xc99s
        0xc99s
        0xc99s
        0xc99s
        0x24cs
        0x24cs
        0x24cs
        0x24cs
        0x24cs
        0x24cs
        0x24cs
        0x24cs
        0x690s
        0x690s
        0x5013s
        0x5813s
        0x6013s
        0x6813s
        0x6f0s
        0x6f0s
        0x710s
        0x710s
        -0x5feds
        -0x57eds
        -0x4feds
        -0x47eds
        0x770s
        0x770s
        0x790s
        0x790s
        -0x3feds
        -0x37eds
        0x30es
        0x30es
        0x30es
        0x30es
        0x32es
        0x32es
        0x32es
        0x32es
        -0x2feds
        -0x27eds
        0x2811s
        0x2811s
        0x3011s
        0x3011s
        0x3811s
        0x3811s
        0x4013s
        0x4813s
        0x6b0s
        0x6b0s
        0x6d0s
        0x6d0s
        0x7013s
        0x7813s
        -0x7feds
        -0x77eds
        -0x6feds
        -0x67eds
        0x80ds
        0x80ds
        0x80ds
        0x80ds
        0x80ds
        0x80ds
        0x80ds
        0x80ds
        0x1a8s
        0x1a8s
        0x1a8s
        0x1a8s
        0x1a8s
        0x1a8s
        0x1a8s
        0x1a8s
        0x1a8s
        0x1a8s
        0x1a8s
        0x1a8s
        0x1a8s
        0x1a8s
        0x1a8s
        0x1a8s
        0x1a8s
        0x1a8s
        0x1a8s
        0x1a8s
        0x1a8s
        0x1a8s
        0x1a8s
        0x1a8s
        0x1a8s
        0x1a8s
        0x1a8s
        0x1a8s
        0x1a8s
        0x1a8s
        0x1a8s
        0x1a8s
        0x2ees
        0x2ees
        0x2ees
        0x2ees
        0x650s
        0x650s
        0x670s
        0x670s
        0x590s
        0x590s
        0x5b0s
        0x5b0s
        0x5d0s
        0x5d0s
        0x5f0s
        0x5f0s
        0x730s
        0x730s
        0x750s
        0x750s
        0x7b0s
        0x7b0s
        0x2011s
        0x2011s
        0x20cs
        0x20cs
        0x20cs
        0x20cs
        0x20cs
        0x20cs
        0x20cs
        0x20cs
        0x22cs
        0x22cs
        0x22cs
        0x22cs
        0x22cs
        0x22cs
        0x22cs
        0x22cs
        0x610s
        0x610s
        0x630s
        0x630s
        0x7d0s
        0x7d0s
        0x7f0s
        0x7f0s
        0x3d0s
        0x3d0s
        0x3f0s
        0x3f0s
        0x410s
        0x410s
        0x430s
        0x430s
        0x510s
        0x510s
        0x530s
        0x530s
        0x2ces
        0x2ces
        0x2ces
        0x2ces
        0x1c8s
        0x1c8s
        0x1c8s
        0x1c8s
        0x1c8s
        0x1c8s
        0x1c8s
        0x1c8s
        0x1c8s
        0x1c8s
        0x1c8s
        0x1c8s
        0x1c8s
        0x1c8s
        0x1c8s
        0x1c8s
        0x1c8s
        0x1c8s
        0x1c8s
        0x1c8s
        0x1c8s
        0x1c8s
        0x1c8s
        0x1c8s
        0x1c8s
        0x1c8s
        0x1c8s
        0x1c8s
        0x1c8s
        0x1c8s
        0x1c8s
        0x1c8s
        0x146s
        0x146s
        0x146s
        0x146s
        0x146s
        0x146s
        0x146s
        0x146s
        0x146s
        0x146s
        0x146s
        0x146s
        0x146s
        0x146s
        0x146s
        0x146s
        0x146s
        0x146s
        0x146s
        0x146s
        0x146s
        0x146s
        0x146s
        0x146s
        0x146s
        0x146s
        0x146s
        0x146s
        0x146s
        0x146s
        0x146s
        0x146s
        0x146s
        0x146s
        0x146s
        0x146s
        0x146s
        0x146s
        0x146s
        0x146s
        0x146s
        0x146s
        0x146s
        0x146s
        0x146s
        0x146s
        0x146s
        0x146s
        0x146s
        0x146s
        0x146s
        0x146s
        0x146s
        0x146s
        0x146s
        0x146s
        0x146s
        0x146s
        0x146s
        0x146s
        0x146s
        0x146s
        0x146s
        0x146s
        0x166s
        0x166s
        0x166s
        0x166s
        0x166s
        0x166s
        0x166s
        0x166s
        0x166s
        0x166s
        0x166s
        0x166s
        0x166s
        0x166s
        0x166s
        0x166s
        0x166s
        0x166s
        0x166s
        0x166s
        0x166s
        0x166s
        0x166s
        0x166s
        0x166s
        0x166s
        0x166s
        0x166s
        0x166s
        0x166s
        0x166s
        0x166s
        0x166s
        0x166s
        0x166s
        0x166s
        0x166s
        0x166s
        0x166s
        0x166s
        0x166s
        0x166s
        0x166s
        0x166s
        0x166s
        0x166s
        0x166s
        0x166s
        0x166s
        0x166s
        0x166s
        0x166s
        0x166s
        0x166s
        0x166s
        0x166s
        0x166s
        0x166s
        0x166s
        0x166s
        0x166s
        0x166s
        0x166s
        0x166s
        0x1eas
        0x1eas
        0x1eas
        0x1eas
        0x1eas
        0x1eas
        0x1eas
        0x1eas
        0x1eas
        0x1eas
        0x1eas
        0x1eas
        0x1eas
        0x1eas
        0x1eas
        0x1eas
        0x1011s
        0x1011s
        0x1811s
        0x1811s
        0x350s
        0x350s
        0x370s
        0x370s
        0x390s
        0x390s
        0x3b0s
        0x3b0s
        0x26es
        0x26es
        0x26es
        0x26es
        0x28es
        0x28es
        0x28es
        0x28es
        0x450s
        0x450s
        0x470s
        0x470s
        0x490s
        0x490s
        0x4b0s
        0x4b0s
        0x4d0s
        0x4d0s
        0x4f0s
        0x4f0s
        0x2aes
        0x2aes
        0x2aes
        0x2aes
        0x550s
        0x550s
        0x570s
        0x570s
        0xcs
        0xcs
        0xcs
        0xcs
        0xcs
        0xcs
        0xcs
        0xcs
        0x186s
        0x186s
        0x186s
        0x186s
        0x186s
        0x186s
        0x186s
        0x186s
        0x186s
        0x186s
        0x186s
        0x186s
        0x186s
        0x186s
        0x186s
        0x186s
        0x186s
        0x186s
        0x186s
        0x186s
        0x186s
        0x186s
        0x186s
        0x186s
        0x186s
        0x186s
        0x186s
        0x186s
        0x186s
        0x186s
        0x186s
        0x186s
        0x186s
        0x186s
        0x186s
        0x186s
        0x186s
        0x186s
        0x186s
        0x186s
        0x186s
        0x186s
        0x186s
        0x186s
        0x186s
        0x186s
        0x186s
        0x186s
        0x186s
        0x186s
        0x186s
        0x186s
        0x186s
        0x186s
        0x186s
        0x186s
        0x186s
        0x186s
        0x186s
        0x186s
        0x186s
        0x186s
        0x186s
        0x186s
    .end array-data

    .line 535
    :array_8
    .array-data 1
        0x50t
        0x58t
        0x17t
        0x47t
        0x1et
        0x1et
        0x3et
        0x3et
        0x4t
        0x4t
        0x4t
        0x4t
        0x4t
        0x4t
        0x4t
        0x4t
        0xbt
        0xbt
        0xbt
        0xbt
        0xbt
        0xbt
        0xbt
        0xbt
        0xbt
        0xbt
        0xbt
        0xbt
        0xbt
        0xbt
        0xbt
        0xbt
        0x23t
        0x23t
        0x23t
        0x23t
        0x23t
        0x23t
        0x23t
        0x23t
        0x23t
        0x23t
        0x23t
        0x23t
        0x23t
        0x23t
        0x23t
        0x23t
        0x33t
        0x33t
        0x33t
        0x33t
        0x33t
        0x33t
        0x33t
        0x33t
        0x33t
        0x33t
        0x33t
        0x33t
        0x33t
        0x33t
        0x33t
        0x33t
        0x29t
        0x29t
        0x29t
        0x29t
        0x29t
        0x29t
        0x29t
        0x29t
        0x29t
        0x29t
        0x29t
        0x29t
        0x29t
        0x29t
        0x29t
        0x29t
        0x29t
        0x29t
        0x29t
        0x29t
        0x29t
        0x29t
        0x29t
        0x29t
        0x29t
        0x29t
        0x29t
        0x29t
        0x29t
        0x29t
        0x29t
        0x29t
        0x29t
        0x29t
        0x29t
        0x29t
        0x29t
        0x29t
        0x29t
        0x29t
        0x29t
        0x29t
        0x29t
        0x29t
        0x29t
        0x29t
        0x29t
        0x29t
        0x29t
        0x29t
        0x29t
        0x29t
        0x29t
        0x29t
        0x29t
        0x29t
        0x29t
        0x29t
        0x29t
        0x29t
        0x29t
        0x29t
        0x29t
        0x29t
    .end array-data
.end method

.method public constructor <init>(III)V
    .locals 1
    .param p1, "fillOrder"    # I
    .param p2, "w"    # I
    .param p3, "h"    # I

    .prologue
    const/4 v0, 0x0

    .line 585
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 562
    iput-boolean v0, p0, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->align:Z

    .line 568
    iput v0, p0, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->changingElemSize:I

    .line 575
    iput v0, p0, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->lastChangingElement:I

    .line 577
    iput-boolean v0, p0, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->fillBits:Z

    .line 586
    iput p1, p0, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->fillOrder:I

    .line 587
    iput p2, p0, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->w:I

    .line 589
    iput v0, p0, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->bitPointer:I

    .line 590
    iput v0, p0, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->bytePointer:I

    .line 591
    new-array v0, p2, [I

    iput-object v0, p0, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->prevChangingElems:[I

    .line 592
    new-array v0, p2, [I

    iput-object v0, p0, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->currChangingElems:[I

    .line 593
    return-void
.end method

.method private align()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 596
    iget-boolean v1, p0, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->align:Z

    if-eqz v1, :cond_0

    iget v1, p0, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->bitPointer:I

    if-eqz v1, :cond_0

    .line 597
    iget v1, p0, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->bytePointer:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->bytePointer:I

    .line 598
    iput v0, p0, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->bitPointer:I

    .line 599
    const/4 v0, 0x1

    .line 601
    :cond_0
    return v0
.end method

.method private decodeBlackCodeWord()I
    .locals 9

    .prologue
    const/4 v8, 0x4

    .line 622
    const/4 v1, -0x1

    .line 623
    .local v1, "code":I
    const/4 v6, 0x0

    .line 624
    .local v6, "runLength":I
    const/4 v5, 0x0

    .line 626
    .local v5, "isWhite":Z
    :cond_0
    :goto_0
    if-nez v5, :cond_5

    .line 627
    invoke-direct {p0, v8}, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->nextLesserThan8Bits(I)I

    move-result v2

    .line 628
    .local v2, "current":I
    sget-object v7, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->initBlack:[S

    aget-short v3, v7, v2

    .line 631
    .local v3, "entry":I
    and-int/lit8 v4, v3, 0x1

    .line 632
    .local v4, "isT":I
    ushr-int/lit8 v7, v3, 0x1

    and-int/lit8 v0, v7, 0xf

    .line 633
    .local v0, "bits":I
    ushr-int/lit8 v7, v3, 0x5

    and-int/lit16 v1, v7, 0x7ff

    .line 635
    const/16 v7, 0x64

    if-ne v1, v7, :cond_3

    .line 636
    const/16 v7, 0x9

    invoke-direct {p0, v7}, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->nextNBits(I)I

    move-result v2

    .line 637
    sget-object v7, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->black:[S

    aget-short v3, v7, v2

    .line 640
    and-int/lit8 v4, v3, 0x1

    .line 641
    ushr-int/lit8 v7, v3, 0x1

    and-int/lit8 v0, v7, 0xf

    .line 642
    ushr-int/lit8 v7, v3, 0x5

    and-int/lit16 v1, v7, 0x7ff

    .line 644
    const/16 v7, 0xc

    if-ne v0, v7, :cond_1

    .line 646
    const/4 v7, 0x5

    invoke-direct {p0, v7}, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->updatePointer(I)V

    .line 647
    invoke-direct {p0, v8}, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->nextLesserThan8Bits(I)I

    move-result v2

    .line 648
    sget-object v7, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->additionalMakeup:[S

    aget-short v3, v7, v2

    .line 649
    ushr-int/lit8 v7, v3, 0x1

    and-int/lit8 v0, v7, 0x7

    .line 650
    ushr-int/lit8 v7, v3, 0x4

    and-int/lit16 v1, v7, 0xfff

    .line 651
    add-int/2addr v6, v1

    .line 653
    rsub-int/lit8 v7, v0, 0x4

    invoke-direct {p0, v7}, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->updatePointer(I)V

    goto :goto_0

    .line 654
    :cond_1
    const/16 v7, 0xf

    if-ne v0, v7, :cond_2

    .line 656
    new-instance v7, Ljava/lang/RuntimeException;

    const-string/jumbo v8, "EOL code word encountered in Black run."

    invoke-direct {v7, v8}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 659
    :cond_2
    add-int/2addr v6, v1

    .line 660
    rsub-int/lit8 v7, v0, 0x9

    invoke-direct {p0, v7}, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->updatePointer(I)V

    .line 661
    if-nez v4, :cond_0

    .line 662
    const/4 v5, 0x1

    goto :goto_0

    .line 665
    :cond_3
    const/16 v7, 0xc8

    if-ne v1, v7, :cond_4

    .line 667
    const/4 v7, 0x2

    invoke-direct {p0, v7}, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->nextLesserThan8Bits(I)I

    move-result v2

    .line 668
    sget-object v7, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->twoBitBlack:[S

    aget-short v3, v7, v2

    .line 669
    ushr-int/lit8 v7, v3, 0x5

    and-int/lit16 v1, v7, 0x7ff

    .line 670
    add-int/2addr v6, v1

    .line 671
    ushr-int/lit8 v7, v3, 0x1

    and-int/lit8 v0, v7, 0xf

    .line 672
    rsub-int/lit8 v7, v0, 0x2

    invoke-direct {p0, v7}, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->updatePointer(I)V

    .line 673
    const/4 v5, 0x1

    goto :goto_0

    .line 676
    :cond_4
    add-int/2addr v6, v1

    .line 677
    rsub-int/lit8 v7, v0, 0x4

    invoke-direct {p0, v7}, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->updatePointer(I)V

    .line 678
    const/4 v5, 0x1

    goto/16 :goto_0

    .line 682
    .end local v0    # "bits":I
    .end local v2    # "current":I
    .end local v3    # "entry":I
    .end local v4    # "isT":I
    :cond_5
    return v6
.end method

.method private decodeWhiteCodeWord()I
    .locals 10

    .prologue
    .line 1224
    const/4 v1, -0x1

    .line 1225
    .local v1, "code":I
    const/4 v6, 0x0

    .line 1226
    .local v6, "runLength":I
    const/4 v5, 0x1

    .line 1228
    .local v5, "isWhite":Z
    :cond_0
    :goto_0
    if-eqz v5, :cond_4

    .line 1229
    const/16 v8, 0xa

    invoke-direct {p0, v8}, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->nextNBits(I)I

    move-result v2

    .line 1230
    .local v2, "current":I
    sget-object v8, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->white:[S

    aget-short v3, v8, v2

    .line 1233
    .local v3, "entry":I
    and-int/lit8 v4, v3, 0x1

    .line 1234
    .local v4, "isT":I
    ushr-int/lit8 v8, v3, 0x1

    and-int/lit8 v0, v8, 0xf

    .line 1236
    .local v0, "bits":I
    const/16 v8, 0xc

    if-ne v0, v8, :cond_1

    .line 1238
    const/4 v8, 0x2

    invoke-direct {p0, v8}, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->nextLesserThan8Bits(I)I

    move-result v7

    .line 1240
    .local v7, "twoBits":I
    shl-int/lit8 v8, v2, 0x2

    and-int/lit8 v8, v8, 0xc

    or-int v2, v8, v7

    .line 1241
    sget-object v8, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->additionalMakeup:[S

    aget-short v3, v8, v2

    .line 1242
    ushr-int/lit8 v8, v3, 0x1

    and-int/lit8 v0, v8, 0x7

    .line 1243
    ushr-int/lit8 v8, v3, 0x4

    and-int/lit16 v1, v8, 0xfff

    .line 1244
    add-int/2addr v6, v1

    .line 1245
    rsub-int/lit8 v8, v0, 0x4

    invoke-direct {p0, v8}, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->updatePointer(I)V

    goto :goto_0

    .line 1246
    .end local v7    # "twoBits":I
    :cond_1
    if-nez v0, :cond_2

    .line 1247
    new-instance v8, Ljava/lang/RuntimeException;

    const-string/jumbo v9, "Invalid code encountered."

    invoke-direct {v8, v9}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 1248
    :cond_2
    const/16 v8, 0xf

    if-ne v0, v8, :cond_3

    .line 1249
    new-instance v8, Ljava/lang/RuntimeException;

    const-string/jumbo v9, "EOL code word encountered in White run."

    invoke-direct {v8, v9}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 1253
    :cond_3
    ushr-int/lit8 v8, v3, 0x5

    and-int/lit16 v1, v8, 0x7ff

    .line 1254
    add-int/2addr v6, v1

    .line 1255
    rsub-int/lit8 v8, v0, 0xa

    invoke-direct {p0, v8}, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->updatePointer(I)V

    .line 1256
    if-nez v4, :cond_0

    .line 1257
    const/4 v5, 0x0

    goto :goto_0

    .line 1262
    .end local v0    # "bits":I
    .end local v2    # "current":I
    .end local v3    # "entry":I
    .end local v4    # "isT":I
    :cond_4
    return v6
.end method

.method private getNextChangingElement(IZ[I)V
    .locals 7
    .param p1, "a0"    # I
    .param p2, "isWhite"    # Z
    .param p3, "ret"    # [I

    .prologue
    const/4 v5, 0x0

    .line 1267
    iget-object v2, p0, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->prevChangingElems:[I

    .line 1268
    .local v2, "pce":[I
    iget v0, p0, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->changingElemSize:I

    .line 1273
    .local v0, "ces":I
    iget v6, p0, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->lastChangingElement:I

    if-lez v6, :cond_2

    iget v6, p0, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->lastChangingElement:I

    add-int/lit8 v3, v6, -0x1

    .line 1274
    .local v3, "start":I
    :goto_0
    if-eqz p2, :cond_3

    .line 1275
    and-int/lit8 v3, v3, -0x2

    .line 1280
    :goto_1
    move v1, v3

    .line 1281
    .local v1, "i":I
    :goto_2
    if-ge v1, v0, :cond_0

    .line 1282
    aget v4, v2, v1

    .line 1283
    .local v4, "temp":I
    if-le v4, p1, :cond_4

    .line 1284
    iput v1, p0, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->lastChangingElement:I

    .line 1285
    aput v4, p3, v5

    .line 1290
    .end local v4    # "temp":I
    :cond_0
    add-int/lit8 v5, v1, 0x1

    if-ge v5, v0, :cond_1

    .line 1291
    const/4 v5, 0x1

    add-int/lit8 v6, v1, 0x1

    aget v6, v2, v6

    aput v6, p3, v5

    .line 1293
    :cond_1
    return-void

    .end local v1    # "i":I
    .end local v3    # "start":I
    :cond_2
    move v3, v5

    .line 1273
    goto :goto_0

    .line 1277
    .restart local v3    # "start":I
    :cond_3
    or-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 1281
    .restart local v1    # "i":I
    .restart local v4    # "temp":I
    :cond_4
    add-int/lit8 v1, v1, 0x2

    goto :goto_2
.end method

.method private nextLesserThan8Bits(I)I
    .locals 12
    .param p1, "bitsToGet"    # I

    .prologue
    .line 1306
    iget-object v9, p0, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->data:[B

    array-length v9, v9

    add-int/lit8 v6, v9, -0x1

    .line 1307
    .local v6, "l":I
    iget v3, p0, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->bytePointer:I

    .line 1309
    .local v3, "bp":I
    iget v9, p0, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->fillOrder:I

    const/4 v10, 0x1

    if-ne v9, v10, :cond_2

    .line 1310
    iget-object v9, p0, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->data:[B

    aget-byte v0, v9, v3

    .line 1311
    .local v0, "b":B
    if-ne v3, v6, :cond_1

    .line 1312
    const/4 v7, 0x0

    .line 1327
    .local v7, "next":B
    :goto_0
    iget v9, p0, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->bitPointer:I

    rsub-int/lit8 v2, v9, 0x8

    .line 1328
    .local v2, "bitsLeft":I
    sub-int v1, p1, v2

    .line 1330
    .local v1, "bitsFromNextByte":I
    sub-int v8, v2, p1

    .line 1333
    .local v8, "shift":I
    if-ltz v8, :cond_5

    .line 1334
    sget-object v9, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->table1:[I

    aget v9, v9, v2

    and-int/2addr v9, v0

    ushr-int v4, v9, v8

    .line 1335
    .local v4, "i1":I
    iget v9, p0, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->bitPointer:I

    add-int/2addr v9, p1

    iput v9, p0, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->bitPointer:I

    .line 1336
    iget v9, p0, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->bitPointer:I

    const/16 v10, 0x8

    if-ne v9, v10, :cond_0

    .line 1337
    const/4 v9, 0x0

    iput v9, p0, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->bitPointer:I

    .line 1338
    iget v9, p0, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->bytePointer:I

    add-int/lit8 v9, v9, 0x1

    iput v9, p0, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->bytePointer:I

    .line 1349
    :cond_0
    :goto_1
    return v4

    .line 1314
    .end local v1    # "bitsFromNextByte":I
    .end local v2    # "bitsLeft":I
    .end local v4    # "i1":I
    .end local v7    # "next":B
    .end local v8    # "shift":I
    :cond_1
    iget-object v9, p0, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->data:[B

    add-int/lit8 v10, v3, 0x1

    aget-byte v7, v9, v10

    .restart local v7    # "next":B
    goto :goto_0

    .line 1316
    .end local v0    # "b":B
    .end local v7    # "next":B
    :cond_2
    iget v9, p0, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->fillOrder:I

    const/4 v10, 0x2

    if-ne v9, v10, :cond_4

    .line 1317
    sget-object v9, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->flipTable:[B

    iget-object v10, p0, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->data:[B

    aget-byte v10, v10, v3

    and-int/lit16 v10, v10, 0xff

    aget-byte v0, v9, v10

    .line 1318
    .restart local v0    # "b":B
    if-ne v3, v6, :cond_3

    .line 1319
    const/4 v7, 0x0

    .restart local v7    # "next":B
    goto :goto_0

    .line 1321
    .end local v7    # "next":B
    :cond_3
    sget-object v9, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->flipTable:[B

    iget-object v10, p0, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->data:[B

    add-int/lit8 v11, v3, 0x1

    aget-byte v10, v10, v11

    and-int/lit16 v10, v10, 0xff

    aget-byte v7, v9, v10

    .restart local v7    # "next":B
    goto :goto_0

    .line 1324
    .end local v0    # "b":B
    .end local v7    # "next":B
    :cond_4
    new-instance v9, Ljava/lang/RuntimeException;

    const-string/jumbo v10, "tag must be either 1 or 2."

    invoke-direct {v9, v10}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v9

    .line 1341
    .restart local v0    # "b":B
    .restart local v1    # "bitsFromNextByte":I
    .restart local v2    # "bitsLeft":I
    .restart local v7    # "next":B
    .restart local v8    # "shift":I
    :cond_5
    sget-object v9, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->table1:[I

    aget v9, v9, v2

    and-int/2addr v9, v0

    neg-int v10, v8

    shl-int v4, v9, v10

    .line 1342
    .restart local v4    # "i1":I
    sget-object v9, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->table2:[I

    aget v9, v9, v1

    and-int/2addr v9, v7

    rsub-int/lit8 v10, v1, 0x8

    ushr-int v5, v9, v10

    .line 1344
    .local v5, "i2":I
    or-int/2addr v4, v5

    .line 1345
    iget v9, p0, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->bytePointer:I

    add-int/lit8 v9, v9, 0x1

    iput v9, p0, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->bytePointer:I

    .line 1346
    iput v1, p0, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->bitPointer:I

    goto :goto_1
.end method

.method private nextNBits(I)I
    .locals 15
    .param p1, "bitsToGet"    # I

    .prologue
    .line 1356
    iget-object v12, p0, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->data:[B

    array-length v12, v12

    add-int/lit8 v9, v12, -0x1

    .line 1357
    .local v9, "l":I
    iget v4, p0, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->bytePointer:I

    .line 1359
    .local v4, "bp":I
    iget v12, p0, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->fillOrder:I

    const/4 v13, 0x1

    if-ne v12, v13, :cond_3

    .line 1360
    iget-object v12, p0, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->data:[B

    aget-byte v0, v12, v4

    .line 1362
    .local v0, "b":B
    if-ne v4, v9, :cond_1

    .line 1363
    const/4 v10, 0x0

    .line 1364
    .local v10, "next":B
    const/4 v11, 0x0

    .line 1389
    .local v11, "next2next":B
    :goto_0
    iget v12, p0, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->bitPointer:I

    rsub-int/lit8 v3, v12, 0x8

    .line 1390
    .local v3, "bitsLeft":I
    sub-int v2, p1, v3

    .line 1391
    .local v2, "bitsFromNextByte":I
    const/4 v1, 0x0

    .line 1392
    .local v1, "bitsFromNext2NextByte":I
    const/16 v12, 0x8

    if-le v2, v12, :cond_0

    .line 1393
    add-int/lit8 v1, v2, -0x8

    .line 1394
    const/16 v2, 0x8

    .line 1397
    :cond_0
    iget v12, p0, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->bytePointer:I

    add-int/lit8 v12, v12, 0x1

    iput v12, p0, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->bytePointer:I

    .line 1399
    sget-object v12, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->table1:[I

    aget v12, v12, v3

    and-int/2addr v12, v0

    sub-int v13, p1, v3

    shl-int v6, v12, v13

    .line 1400
    .local v6, "i1":I
    sget-object v12, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->table2:[I

    aget v12, v12, v2

    and-int/2addr v12, v10

    rsub-int/lit8 v13, v2, 0x8

    ushr-int v7, v12, v13

    .line 1402
    .local v7, "i2":I
    const/4 v8, 0x0

    .line 1403
    .local v8, "i3":I
    if-eqz v1, :cond_7

    .line 1404
    shl-int/2addr v7, v1

    .line 1405
    sget-object v12, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->table2:[I

    aget v12, v12, v1

    and-int/2addr v12, v11

    rsub-int/lit8 v13, v1, 0x8

    ushr-int v8, v12, v13

    .line 1406
    or-int/2addr v7, v8

    .line 1407
    iget v12, p0, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->bytePointer:I

    add-int/lit8 v12, v12, 0x1

    iput v12, p0, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->bytePointer:I

    .line 1408
    iput v1, p0, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->bitPointer:I

    .line 1418
    :goto_1
    or-int v5, v6, v7

    .line 1419
    .local v5, "i":I
    return v5

    .line 1365
    .end local v1    # "bitsFromNext2NextByte":I
    .end local v2    # "bitsFromNextByte":I
    .end local v3    # "bitsLeft":I
    .end local v5    # "i":I
    .end local v6    # "i1":I
    .end local v7    # "i2":I
    .end local v8    # "i3":I
    .end local v10    # "next":B
    .end local v11    # "next2next":B
    :cond_1
    add-int/lit8 v12, v4, 0x1

    if-ne v12, v9, :cond_2

    .line 1366
    iget-object v12, p0, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->data:[B

    add-int/lit8 v13, v4, 0x1

    aget-byte v10, v12, v13

    .line 1367
    .restart local v10    # "next":B
    const/4 v11, 0x0

    .restart local v11    # "next2next":B
    goto :goto_0

    .line 1369
    .end local v10    # "next":B
    .end local v11    # "next2next":B
    :cond_2
    iget-object v12, p0, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->data:[B

    add-int/lit8 v13, v4, 0x1

    aget-byte v10, v12, v13

    .line 1370
    .restart local v10    # "next":B
    iget-object v12, p0, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->data:[B

    add-int/lit8 v13, v4, 0x2

    aget-byte v11, v12, v13

    .restart local v11    # "next2next":B
    goto :goto_0

    .line 1372
    .end local v0    # "b":B
    .end local v10    # "next":B
    .end local v11    # "next2next":B
    :cond_3
    iget v12, p0, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->fillOrder:I

    const/4 v13, 0x2

    if-ne v12, v13, :cond_6

    .line 1373
    sget-object v12, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->flipTable:[B

    iget-object v13, p0, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->data:[B

    aget-byte v13, v13, v4

    and-int/lit16 v13, v13, 0xff

    aget-byte v0, v12, v13

    .line 1375
    .restart local v0    # "b":B
    if-ne v4, v9, :cond_4

    .line 1376
    const/4 v10, 0x0

    .line 1377
    .restart local v10    # "next":B
    const/4 v11, 0x0

    .restart local v11    # "next2next":B
    goto :goto_0

    .line 1378
    .end local v10    # "next":B
    .end local v11    # "next2next":B
    :cond_4
    add-int/lit8 v12, v4, 0x1

    if-ne v12, v9, :cond_5

    .line 1379
    sget-object v12, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->flipTable:[B

    iget-object v13, p0, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->data:[B

    add-int/lit8 v14, v4, 0x1

    aget-byte v13, v13, v14

    and-int/lit16 v13, v13, 0xff

    aget-byte v10, v12, v13

    .line 1380
    .restart local v10    # "next":B
    const/4 v11, 0x0

    .restart local v11    # "next2next":B
    goto :goto_0

    .line 1382
    .end local v10    # "next":B
    .end local v11    # "next2next":B
    :cond_5
    sget-object v12, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->flipTable:[B

    iget-object v13, p0, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->data:[B

    add-int/lit8 v14, v4, 0x1

    aget-byte v13, v13, v14

    and-int/lit16 v13, v13, 0xff

    aget-byte v10, v12, v13

    .line 1383
    .restart local v10    # "next":B
    sget-object v12, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->flipTable:[B

    iget-object v13, p0, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->data:[B

    add-int/lit8 v14, v4, 0x2

    aget-byte v13, v13, v14

    and-int/lit16 v13, v13, 0xff

    aget-byte v11, v12, v13

    .restart local v11    # "next2next":B
    goto/16 :goto_0

    .line 1386
    .end local v0    # "b":B
    .end local v10    # "next":B
    .end local v11    # "next2next":B
    :cond_6
    new-instance v12, Ljava/lang/RuntimeException;

    const-string/jumbo v13, "tag must be either 1 or 2."

    invoke-direct {v12, v13}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v12

    .line 1410
    .restart local v0    # "b":B
    .restart local v1    # "bitsFromNext2NextByte":I
    .restart local v2    # "bitsFromNextByte":I
    .restart local v3    # "bitsLeft":I
    .restart local v6    # "i1":I
    .restart local v7    # "i2":I
    .restart local v8    # "i3":I
    .restart local v10    # "next":B
    .restart local v11    # "next2next":B
    :cond_7
    const/16 v12, 0x8

    if-ne v2, v12, :cond_8

    .line 1411
    const/4 v12, 0x0

    iput v12, p0, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->bitPointer:I

    .line 1412
    iget v12, p0, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->bytePointer:I

    add-int/lit8 v12, v12, 0x1

    iput v12, p0, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->bytePointer:I

    goto :goto_1

    .line 1414
    :cond_8
    iput v2, p0, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->bitPointer:I

    goto :goto_1
.end method

.method private readEOL(Z)I
    .locals 8
    .param p1, "isFirstEOL"    # Z

    .prologue
    const/16 v7, 0x8

    const/4 v6, 0x4

    const/4 v3, 0x1

    .line 1424
    invoke-direct {p0}, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->seekEOL()Z

    move-result v4

    if-nez v4, :cond_0

    .line 1425
    new-instance v3, Ljava/lang/RuntimeException;

    const-string/jumbo v4, "EOL not found"

    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 1428
    :cond_0
    iget-boolean v4, p0, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->fillBits:Z

    if-nez v4, :cond_2

    .line 1429
    const/16 v4, 0xc

    invoke-direct {p0, v4}, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->nextNBits(I)I

    move-result v1

    .line 1430
    .local v1, "next12Bits":I
    if-eqz p1, :cond_1

    if-nez v1, :cond_1

    .line 1435
    invoke-direct {p0, v6}, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->nextNBits(I)I

    move-result v4

    if-ne v4, v3, :cond_1

    .line 1437
    iput-boolean v3, p0, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->fillBits:Z

    .line 1499
    .end local v1    # "next12Bits":I
    :goto_0
    return v3

    .line 1441
    .restart local v1    # "next12Bits":I
    :cond_1
    if-eq v1, v3, :cond_5

    .line 1442
    new-instance v3, Ljava/lang/RuntimeException;

    const-string/jumbo v4, "Scanline must begin with EOL code word."

    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 1449
    .end local v1    # "next12Bits":I
    :cond_2
    iget v4, p0, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->bitPointer:I

    rsub-int/lit8 v0, v4, 0x8

    .line 1451
    .local v0, "bitsLeft":I
    invoke-direct {p0, v0}, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->nextNBits(I)I

    move-result v4

    if-eqz v4, :cond_3

    .line 1452
    new-instance v3, Ljava/lang/RuntimeException;

    const-string/jumbo v4, "All fill bits preceding EOL code must be 0."

    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 1460
    :cond_3
    if-ge v0, v6, :cond_4

    .line 1461
    invoke-direct {p0, v7}, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->nextNBits(I)I

    move-result v4

    if-eqz v4, :cond_4

    .line 1462
    new-instance v3, Ljava/lang/RuntimeException;

    const-string/jumbo v4, "All fill bits preceding EOL code must be 0."

    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 1472
    :cond_4
    invoke-direct {p0, v7}, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->nextNBits(I)I

    move-result v2

    .line 1474
    .local v2, "next8":I
    if-eqz p1, :cond_7

    and-int/lit16 v4, v2, 0xf0

    const/16 v5, 0x10

    if-ne v4, v5, :cond_7

    .line 1480
    const/4 v4, 0x0

    iput-boolean v4, p0, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->fillBits:Z

    .line 1481
    invoke-direct {p0, v6}, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->updatePointer(I)V

    .line 1499
    .end local v0    # "bitsLeft":I
    .end local v2    # "next8":I
    :cond_5
    invoke-direct {p0, v3}, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->nextLesserThan8Bits(I)I

    move-result v3

    goto :goto_0

    .line 1494
    .restart local v0    # "bitsLeft":I
    .restart local v2    # "next8":I
    :cond_6
    invoke-direct {p0, v7}, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->nextNBits(I)I

    move-result v2

    .line 1489
    :cond_7
    if-eq v2, v3, :cond_5

    .line 1491
    if-eqz v2, :cond_6

    .line 1492
    new-instance v3, Ljava/lang/RuntimeException;

    const-string/jumbo v4, "0 bits expected before EOL"

    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v3
.end method

.method private seekEOL()Z
    .locals 7

    .prologue
    const/16 v6, 0xc

    const/4 v3, 0x1

    .line 1507
    iget-object v4, p0, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->data:[B

    array-length v4, v4

    mul-int/lit8 v4, v4, 0x8

    add-int/lit8 v1, v4, -0x1

    .line 1508
    .local v1, "bitIndexMax":I
    iget v4, p0, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->bytePointer:I

    mul-int/lit8 v4, v4, 0x8

    iget v5, p0, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->bitPointer:I

    add-int v0, v4, v5

    .line 1511
    .local v0, "bitIndex":I
    :cond_0
    add-int/lit8 v4, v1, -0xc

    if-gt v0, v4, :cond_2

    .line 1513
    invoke-direct {p0, v6}, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->nextNBits(I)I

    move-result v2

    .line 1514
    .local v2, "next12Bits":I
    add-int/lit8 v0, v0, 0xc

    .line 1518
    :goto_0
    if-eq v2, v3, :cond_1

    if-ge v0, v1, :cond_1

    .line 1519
    and-int/lit16 v4, v2, 0x7ff

    shl-int/lit8 v4, v4, 0x1

    invoke-direct {p0, v3}, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->nextLesserThan8Bits(I)I

    move-result v5

    and-int/lit8 v5, v5, 0x1

    or-int v2, v4, v5

    .line 1521
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1525
    :cond_1
    if-ne v2, v3, :cond_0

    .line 1526
    invoke-direct {p0, v6}, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->updatePointer(I)V

    .line 1532
    .end local v2    # "next12Bits":I
    :goto_1
    return v3

    :cond_2
    const/4 v3, 0x0

    goto :goto_1
.end method

.method private setToBlack([BIII)V
    .locals 10
    .param p1, "buffer"    # [B
    .param p2, "lineOffset"    # I
    .param p3, "bitOffset"    # I
    .param p4, "numBits"    # I

    .prologue
    const/4 v9, 0x1

    .line 1545
    mul-int/lit8 v7, p2, 0x8

    add-int v0, v7, p3

    .line 1546
    .local v0, "bitNum":I
    add-int v3, v0, p4

    .line 1548
    .local v3, "lastBit":I
    shr-int/lit8 v1, v0, 0x3

    .line 1551
    .local v1, "byteNum":I
    and-int/lit8 v5, v0, 0x7

    .line 1552
    .local v5, "shift":I
    if-lez v5, :cond_1

    .line 1553
    rsub-int/lit8 v7, v5, 0x7

    shl-int v4, v9, v7

    .line 1554
    .local v4, "maskVal":I
    aget-byte v6, p1, v1

    .line 1555
    .local v6, "val":B
    :goto_0
    if-lez v4, :cond_0

    if-ge v0, v3, :cond_0

    .line 1556
    or-int v7, v6, v4

    int-to-byte v6, v7

    .line 1557
    shr-int/lit8 v4, v4, 0x1

    .line 1558
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1560
    :cond_0
    aput-byte v6, p1, v1

    .line 1564
    .end local v4    # "maskVal":I
    .end local v6    # "val":B
    :cond_1
    shr-int/lit8 v1, v0, 0x3

    move v2, v1

    .line 1565
    .end local v1    # "byteNum":I
    .local v2, "byteNum":I
    :goto_1
    add-int/lit8 v7, v3, -0x7

    if-ge v0, v7, :cond_3

    .line 1566
    add-int/lit8 v1, v2, 0x1

    .end local v2    # "byteNum":I
    .restart local v1    # "byteNum":I
    const/4 v7, -0x1

    aput-byte v7, p1, v2

    .line 1567
    add-int/lit8 v0, v0, 0x8

    move v2, v1

    .end local v1    # "byteNum":I
    .restart local v2    # "byteNum":I
    goto :goto_1

    .line 1571
    .end local v2    # "byteNum":I
    .restart local v1    # "byteNum":I
    :goto_2
    if-ge v0, v3, :cond_2

    .line 1572
    shr-int/lit8 v1, v0, 0x3

    .line 1573
    aget-byte v7, p1, v1

    and-int/lit8 v8, v0, 0x7

    rsub-int/lit8 v8, v8, 0x7

    shl-int v8, v9, v8

    or-int/2addr v7, v8

    int-to-byte v7, v7

    aput-byte v7, p1, v1

    .line 1574
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1576
    :cond_2
    return-void

    .end local v1    # "byteNum":I
    .restart local v2    # "byteNum":I
    :cond_3
    move v1, v2

    .end local v2    # "byteNum":I
    .restart local v1    # "byteNum":I
    goto :goto_2
.end method

.method private updatePointer(I)V
    .locals 3
    .param p1, "bitsToMoveBack"    # I

    .prologue
    .line 1580
    const/16 v1, 0x8

    if-le p1, v1, :cond_0

    .line 1581
    iget v1, p0, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->bytePointer:I

    div-int/lit8 v2, p1, 0x8

    sub-int/2addr v1, v2

    iput v1, p0, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->bytePointer:I

    .line 1582
    rem-int/lit8 p1, p1, 0x8

    .line 1585
    :cond_0
    iget v1, p0, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->bitPointer:I

    sub-int v0, v1, p1

    .line 1587
    .local v0, "i":I
    if-gez v0, :cond_1

    .line 1588
    iget v1, p0, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->bytePointer:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->bytePointer:I

    .line 1589
    add-int/lit8 v1, v0, 0x8

    iput v1, p0, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->bitPointer:I

    .line 1593
    :goto_0
    return-void

    .line 1591
    :cond_1
    iput v0, p0, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->bitPointer:I

    goto :goto_0
.end method


# virtual methods
.method protected consumeEOL()Z
    .locals 3

    .prologue
    const/16 v2, 0xc

    const/4 v1, 0x1

    .line 606
    invoke-direct {p0, v2}, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->nextNBits(I)I

    move-result v0

    .line 607
    .local v0, "next12Bits":I
    if-ne v0, v1, :cond_0

    .line 613
    :goto_0
    return v1

    .line 612
    :cond_0
    invoke-direct {p0, v2}, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->updatePointer(I)V

    .line 613
    const/4 v1, 0x0

    goto :goto_0
.end method

.method protected decodeNextScanline([BII)V
    .locals 10
    .param p1, "buffer"    # [B
    .param p2, "lineOffset"    # I
    .param p3, "bitOffset"    # I

    .prologue
    .line 687
    const/4 v0, 0x0

    .line 688
    .local v0, "bits":I
    const/4 v1, 0x0

    .line 689
    .local v1, "code":I
    const/4 v4, 0x0

    .line 693
    .local v4, "isT":I
    const/4 v5, 0x1

    .line 696
    .local v5, "isWhite":Z
    const/4 v7, 0x0

    iput v7, p0, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->changingElemSize:I

    .line 699
    :cond_0
    iget v7, p0, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->w:I

    if-ge p3, v7, :cond_6

    .line 700
    :cond_1
    :goto_0
    if-eqz v5, :cond_5

    .line 702
    const/16 v7, 0xa

    invoke-direct {p0, v7}, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->nextNBits(I)I

    move-result v2

    .line 703
    .local v2, "current":I
    sget-object v7, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->white:[S

    aget-short v3, v7, v2

    .line 706
    .local v3, "entry":I
    and-int/lit8 v4, v3, 0x1

    .line 707
    ushr-int/lit8 v7, v3, 0x1

    and-int/lit8 v0, v7, 0xf

    .line 709
    const/16 v7, 0xc

    if-ne v0, v7, :cond_2

    .line 711
    const/4 v7, 0x2

    invoke-direct {p0, v7}, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->nextLesserThan8Bits(I)I

    move-result v6

    .line 713
    .local v6, "twoBits":I
    shl-int/lit8 v7, v2, 0x2

    and-int/lit8 v7, v7, 0xc

    or-int v2, v7, v6

    .line 714
    sget-object v7, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->additionalMakeup:[S

    aget-short v3, v7, v2

    .line 715
    ushr-int/lit8 v7, v3, 0x1

    and-int/lit8 v0, v7, 0x7

    .line 716
    ushr-int/lit8 v7, v3, 0x4

    and-int/lit16 v1, v7, 0xfff

    .line 717
    add-int/2addr p3, v1

    .line 719
    rsub-int/lit8 v7, v0, 0x4

    invoke-direct {p0, v7}, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->updatePointer(I)V

    goto :goto_0

    .line 720
    .end local v6    # "twoBits":I
    :cond_2
    if-nez v0, :cond_3

    .line 721
    new-instance v7, Ljava/lang/RuntimeException;

    const-string/jumbo v8, "Invalid code encountered."

    invoke-direct {v7, v8}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 722
    :cond_3
    const/16 v7, 0xf

    if-ne v0, v7, :cond_4

    .line 725
    const/16 v7, 0xa

    invoke-direct {p0, v7}, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->updatePointer(I)V

    .line 826
    .end local v2    # "current":I
    .end local v3    # "entry":I
    :goto_1
    return-void

    .line 729
    .restart local v2    # "current":I
    .restart local v3    # "entry":I
    :cond_4
    ushr-int/lit8 v7, v3, 0x5

    and-int/lit16 v1, v7, 0x7ff

    .line 730
    add-int/2addr p3, v1

    .line 732
    rsub-int/lit8 v7, v0, 0xa

    invoke-direct {p0, v7}, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->updatePointer(I)V

    .line 733
    if-nez v4, :cond_1

    .line 734
    const/4 v5, 0x0

    .line 735
    iget-object v7, p0, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->currChangingElems:[I

    iget v8, p0, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->changingElemSize:I

    add-int/lit8 v9, v8, 0x1

    iput v9, p0, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->changingElemSize:I

    aput p3, v7, v8

    goto :goto_0

    .line 742
    .end local v2    # "current":I
    .end local v3    # "entry":I
    :cond_5
    iget v7, p0, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->w:I

    if-ne p3, v7, :cond_7

    .line 743
    invoke-direct {p0}, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->align()Z

    .line 825
    :cond_6
    :goto_2
    iget-object v7, p0, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->currChangingElems:[I

    iget v8, p0, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->changingElemSize:I

    add-int/lit8 v9, v8, 0x1

    iput v9, p0, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->changingElemSize:I

    aput p3, v7, v8

    goto :goto_1

    .line 747
    :cond_7
    :goto_3
    if-nez v5, :cond_c

    .line 749
    const/4 v7, 0x4

    invoke-direct {p0, v7}, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->nextLesserThan8Bits(I)I

    move-result v2

    .line 750
    .restart local v2    # "current":I
    sget-object v7, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->initBlack:[S

    aget-short v3, v7, v2

    .line 753
    .restart local v3    # "entry":I
    and-int/lit8 v4, v3, 0x1

    .line 754
    ushr-int/lit8 v7, v3, 0x1

    and-int/lit8 v0, v7, 0xf

    .line 755
    ushr-int/lit8 v7, v3, 0x5

    and-int/lit16 v1, v7, 0x7ff

    .line 757
    const/16 v7, 0x64

    if-ne v1, v7, :cond_a

    .line 758
    const/16 v7, 0x9

    invoke-direct {p0, v7}, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->nextNBits(I)I

    move-result v2

    .line 759
    sget-object v7, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->black:[S

    aget-short v3, v7, v2

    .line 762
    and-int/lit8 v4, v3, 0x1

    .line 763
    ushr-int/lit8 v7, v3, 0x1

    and-int/lit8 v0, v7, 0xf

    .line 764
    ushr-int/lit8 v7, v3, 0x5

    and-int/lit16 v1, v7, 0x7ff

    .line 766
    const/16 v7, 0xc

    if-ne v0, v7, :cond_8

    .line 768
    const/4 v7, 0x5

    invoke-direct {p0, v7}, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->updatePointer(I)V

    .line 769
    const/4 v7, 0x4

    invoke-direct {p0, v7}, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->nextLesserThan8Bits(I)I

    move-result v2

    .line 770
    sget-object v7, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->additionalMakeup:[S

    aget-short v3, v7, v2

    .line 771
    ushr-int/lit8 v7, v3, 0x1

    and-int/lit8 v0, v7, 0x7

    .line 772
    ushr-int/lit8 v7, v3, 0x4

    and-int/lit16 v1, v7, 0xfff

    .line 774
    invoke-direct {p0, p1, p2, p3, v1}, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->setToBlack([BIII)V

    .line 775
    add-int/2addr p3, v1

    .line 777
    rsub-int/lit8 v7, v0, 0x4

    invoke-direct {p0, v7}, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->updatePointer(I)V

    goto :goto_3

    .line 778
    :cond_8
    const/16 v7, 0xf

    if-ne v0, v7, :cond_9

    .line 781
    const/16 v7, 0x9

    invoke-direct {p0, v7}, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->updatePointer(I)V

    goto/16 :goto_1

    .line 784
    :cond_9
    invoke-direct {p0, p1, p2, p3, v1}, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->setToBlack([BIII)V

    .line 785
    add-int/2addr p3, v1

    .line 787
    rsub-int/lit8 v7, v0, 0x9

    invoke-direct {p0, v7}, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->updatePointer(I)V

    .line 788
    if-nez v4, :cond_7

    .line 789
    const/4 v5, 0x1

    .line 790
    iget-object v7, p0, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->currChangingElems:[I

    iget v8, p0, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->changingElemSize:I

    add-int/lit8 v9, v8, 0x1

    iput v9, p0, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->changingElemSize:I

    aput p3, v7, v8

    goto :goto_3

    .line 793
    :cond_a
    const/16 v7, 0xc8

    if-ne v1, v7, :cond_b

    .line 795
    const/4 v7, 0x2

    invoke-direct {p0, v7}, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->nextLesserThan8Bits(I)I

    move-result v2

    .line 796
    sget-object v7, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->twoBitBlack:[S

    aget-short v3, v7, v2

    .line 797
    ushr-int/lit8 v7, v3, 0x5

    and-int/lit16 v1, v7, 0x7ff

    .line 798
    ushr-int/lit8 v7, v3, 0x1

    and-int/lit8 v0, v7, 0xf

    .line 800
    invoke-direct {p0, p1, p2, p3, v1}, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->setToBlack([BIII)V

    .line 801
    add-int/2addr p3, v1

    .line 803
    rsub-int/lit8 v7, v0, 0x2

    invoke-direct {p0, v7}, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->updatePointer(I)V

    .line 804
    const/4 v5, 0x1

    .line 805
    iget-object v7, p0, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->currChangingElems:[I

    iget v8, p0, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->changingElemSize:I

    add-int/lit8 v9, v8, 0x1

    iput v9, p0, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->changingElemSize:I

    aput p3, v7, v8

    goto/16 :goto_3

    .line 808
    :cond_b
    invoke-direct {p0, p1, p2, p3, v1}, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->setToBlack([BIII)V

    .line 809
    add-int/2addr p3, v1

    .line 811
    rsub-int/lit8 v7, v0, 0x4

    invoke-direct {p0, v7}, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->updatePointer(I)V

    .line 812
    const/4 v5, 0x1

    .line 813
    iget-object v7, p0, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->currChangingElems:[I

    iget v8, p0, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->changingElemSize:I

    add-int/lit8 v9, v8, 0x1

    iput v9, p0, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->changingElemSize:I

    aput p3, v7, v8

    goto/16 :goto_3

    .line 818
    .end local v2    # "current":I
    .end local v3    # "entry":I
    :cond_c
    iget v7, p0, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->w:I

    if-ne p3, v7, :cond_0

    .line 820
    invoke-direct {p0}, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->align()Z

    goto/16 :goto_2
.end method

.method public decodeT41D([B[BII)V
    .locals 5
    .param p1, "buffer"    # [B
    .param p2, "compData"    # [B
    .param p3, "startX"    # I
    .param p4, "height"    # I

    .prologue
    const/4 v4, 0x0

    .line 831
    iput-object p2, p0, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->data:[B

    .line 832
    iget v3, p0, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->w:I

    add-int/lit8 v3, v3, 0x7

    div-int/lit8 v2, v3, 0x8

    .line 833
    .local v2, "scanlineStride":I
    iput v4, p0, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->bitPointer:I

    .line 834
    iput v4, p0, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->bytePointer:I

    .line 836
    const/4 v1, 0x0

    .line 837
    .local v1, "lineOffset":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, p4, :cond_0

    .line 838
    invoke-virtual {p0}, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->consumeEOL()Z

    .line 839
    invoke-virtual {p0, p1, v1, p3}, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->decodeNextScanline([BII)V

    .line 840
    add-int/2addr v1, v2

    .line 837
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 842
    :cond_0
    return-void
.end method

.method public decodeT42D([B[BII)V
    .locals 22
    .param p1, "buffer"    # [B
    .param p2, "compData"    # [B
    .param p3, "startX"    # I
    .param p4, "height"    # I

    .prologue
    .line 847
    move-object/from16 v0, p2

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->data:[B

    .line 848
    move-object/from16 v0, p0

    iget v0, v0, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->w:I

    move/from16 v20, v0

    add-int/lit8 v20, v20, 0x7

    div-int/lit8 v18, v20, 0x8

    .line 849
    .local v18, "scanlineStride":I
    const/16 v20, 0x0

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->bitPointer:I

    .line 850
    const/16 v20, 0x0

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->bytePointer:I

    .line 856
    const/16 v20, 0x2

    move/from16 v0, v20

    new-array v5, v0, [I

    .line 861
    .local v5, "b":[I
    const/4 v11, 0x0

    .line 865
    .local v11, "currIndex":I
    const/16 v20, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-direct {v0, v1}, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->readEOL(Z)I

    move-result v20

    const/16 v21, 0x1

    move/from16 v0, v20

    move/from16 v1, v21

    if-eq v0, v1, :cond_0

    .line 866
    new-instance v20, Ljava/lang/RuntimeException;

    const-string/jumbo v21, "First scanline must be 1D encoded."

    invoke-direct/range {v20 .. v21}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v20

    .line 869
    :cond_0
    const/4 v15, 0x0

    .line 874
    .local v15, "lineOffset":I
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p3

    invoke-virtual {v0, v1, v15, v2}, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->decodeNextScanline([BII)V

    .line 875
    add-int v15, v15, v18

    .line 877
    const/16 v16, 0x1

    .local v16, "lines":I
    :goto_0
    move/from16 v0, v16

    move/from16 v1, p4

    if-ge v0, v1, :cond_a

    .line 880
    const/16 v20, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-direct {v0, v1}, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->readEOL(Z)I

    move-result v20

    if-nez v20, :cond_9

    .line 885
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->prevChangingElems:[I

    move-object/from16 v19, v0

    .line 886
    .local v19, "temp":[I
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->currChangingElems:[I

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->prevChangingElems:[I

    .line 887
    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->currChangingElems:[I

    .line 888
    const/4 v11, 0x0

    .line 891
    const/4 v3, -0x1

    .line 892
    .local v3, "a0":I
    const/4 v14, 0x1

    .line 893
    .local v14, "isWhite":Z
    move/from16 v8, p3

    .line 895
    .local v8, "bitOffset":I
    const/16 v20, 0x0

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->lastChangingElement:I

    .line 897
    :goto_1
    move-object/from16 v0, p0

    iget v0, v0, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->w:I

    move/from16 v20, v0

    move/from16 v0, v20

    if-ge v8, v0, :cond_8

    .line 899
    move-object/from16 v0, p0

    invoke-direct {v0, v3, v14, v5}, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->getNextChangingElement(IZ[I)V

    .line 901
    const/16 v20, 0x0

    aget v6, v5, v20

    .line 902
    .local v6, "b1":I
    const/16 v20, 0x1

    aget v7, v5, v20

    .line 905
    .local v7, "b2":I
    const/16 v20, 0x7

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-direct {v0, v1}, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->nextLesserThan8Bits(I)I

    move-result v13

    .line 908
    .local v13, "entry":I
    sget-object v20, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->twoDCodes:[B

    aget-byte v20, v20, v13

    move/from16 v0, v20

    and-int/lit16 v13, v0, 0xff

    .line 911
    and-int/lit8 v20, v13, 0x78

    ushr-int/lit8 v10, v20, 0x3

    .line 912
    .local v10, "code":I
    and-int/lit8 v9, v13, 0x7

    .line 914
    .local v9, "bits":I
    if-nez v10, :cond_2

    .line 915
    if-nez v14, :cond_1

    .line 916
    sub-int v20, v7, v8

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v20

    invoke-direct {v0, v1, v15, v8, v2}, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->setToBlack([BIII)V

    .line 919
    :cond_1
    move v3, v7

    move v8, v7

    .line 922
    rsub-int/lit8 v20, v9, 0x7

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-direct {v0, v1}, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->updatePointer(I)V

    goto :goto_1

    .line 923
    :cond_2
    const/16 v20, 0x1

    move/from16 v0, v20

    if-ne v10, v0, :cond_4

    .line 925
    rsub-int/lit8 v20, v9, 0x7

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-direct {v0, v1}, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->updatePointer(I)V

    .line 929
    if-eqz v14, :cond_3

    .line 930
    invoke-direct/range {p0 .. p0}, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->decodeWhiteCodeWord()I

    move-result v17

    .line 931
    .local v17, "number":I
    add-int v8, v8, v17

    .line 932
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->currChangingElems:[I

    move-object/from16 v20, v0

    add-int/lit8 v12, v11, 0x1

    .end local v11    # "currIndex":I
    .local v12, "currIndex":I
    aput v8, v20, v11

    .line 934
    invoke-direct/range {p0 .. p0}, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->decodeBlackCodeWord()I

    move-result v17

    .line 935
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v17

    invoke-direct {v0, v1, v15, v8, v2}, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->setToBlack([BIII)V

    .line 936
    add-int v8, v8, v17

    .line 937
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->currChangingElems:[I

    move-object/from16 v20, v0

    add-int/lit8 v11, v12, 0x1

    .end local v12    # "currIndex":I
    .restart local v11    # "currIndex":I
    aput v8, v20, v12

    .line 949
    :goto_2
    move v3, v8

    .line 950
    goto/16 :goto_1

    .line 939
    .end local v17    # "number":I
    :cond_3
    invoke-direct/range {p0 .. p0}, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->decodeBlackCodeWord()I

    move-result v17

    .line 940
    .restart local v17    # "number":I
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v17

    invoke-direct {v0, v1, v15, v8, v2}, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->setToBlack([BIII)V

    .line 941
    add-int v8, v8, v17

    .line 942
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->currChangingElems:[I

    move-object/from16 v20, v0

    add-int/lit8 v12, v11, 0x1

    .end local v11    # "currIndex":I
    .restart local v12    # "currIndex":I
    aput v8, v20, v11

    .line 944
    invoke-direct/range {p0 .. p0}, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->decodeWhiteCodeWord()I

    move-result v17

    .line 945
    add-int v8, v8, v17

    .line 946
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->currChangingElems:[I

    move-object/from16 v20, v0

    add-int/lit8 v11, v12, 0x1

    .end local v12    # "currIndex":I
    .restart local v11    # "currIndex":I
    aput v8, v20, v12

    goto :goto_2

    .line 950
    .end local v17    # "number":I
    :cond_4
    const/16 v20, 0x8

    move/from16 v0, v20

    if-gt v10, v0, :cond_7

    .line 952
    add-int/lit8 v20, v10, -0x5

    add-int v4, v6, v20

    .line 954
    .local v4, "a1":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->currChangingElems:[I

    move-object/from16 v20, v0

    add-int/lit8 v12, v11, 0x1

    .end local v11    # "currIndex":I
    .restart local v12    # "currIndex":I
    aput v4, v20, v11

    .line 958
    if-nez v14, :cond_5

    .line 959
    sub-int v20, v4, v8

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v20

    invoke-direct {v0, v1, v15, v8, v2}, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->setToBlack([BIII)V

    .line 962
    :cond_5
    move v3, v4

    move v8, v4

    .line 963
    if-nez v14, :cond_6

    const/4 v14, 0x1

    .line 965
    :goto_3
    rsub-int/lit8 v20, v9, 0x7

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-direct {v0, v1}, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->updatePointer(I)V

    move v11, v12

    .end local v12    # "currIndex":I
    .restart local v11    # "currIndex":I
    goto/16 :goto_1

    .line 963
    .end local v11    # "currIndex":I
    .restart local v12    # "currIndex":I
    :cond_6
    const/4 v14, 0x0

    goto :goto_3

    .line 967
    .end local v4    # "a1":I
    .end local v12    # "currIndex":I
    .restart local v11    # "currIndex":I
    :cond_7
    new-instance v20, Ljava/lang/RuntimeException;

    const-string/jumbo v21, "Invalid code encountered while decoding 2D group 3 compressed data."

    invoke-direct/range {v20 .. v21}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v20

    .line 974
    .end local v6    # "b1":I
    .end local v7    # "b2":I
    .end local v9    # "bits":I
    .end local v10    # "code":I
    .end local v13    # "entry":I
    :cond_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->currChangingElems:[I

    move-object/from16 v20, v0

    add-int/lit8 v12, v11, 0x1

    .end local v11    # "currIndex":I
    .restart local v12    # "currIndex":I
    aput v8, v20, v11

    .line 975
    move-object/from16 v0, p0

    iput v12, v0, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->changingElemSize:I

    move v11, v12

    .line 981
    .end local v3    # "a0":I
    .end local v8    # "bitOffset":I
    .end local v12    # "currIndex":I
    .end local v14    # "isWhite":Z
    .end local v19    # "temp":[I
    .restart local v11    # "currIndex":I
    :goto_4
    add-int v15, v15, v18

    .line 877
    add-int/lit8 v16, v16, 0x1

    goto/16 :goto_0

    .line 978
    :cond_9
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p3

    invoke-virtual {v0, v1, v15, v2}, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->decodeNextScanline([BII)V

    goto :goto_4

    .line 983
    :cond_a
    return-void
.end method

.method public declared-synchronized decodeT6([B[BII)V
    .locals 26
    .param p1, "buffer"    # [B
    .param p2, "compData"    # [B
    .param p3, "startX"    # I
    .param p4, "height"    # I

    .prologue
    .line 987
    monitor-enter p0

    :try_start_0
    move-object/from16 v0, p2

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->data:[B

    .line 988
    move-object/from16 v0, p0

    iget v0, v0, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->w:I

    move/from16 v24, v0

    add-int/lit8 v24, v24, 0x7

    div-int/lit8 v21, v24, 0x8

    .line 989
    .local v21, "scanlineStride":I
    const/16 v24, 0x0

    move/from16 v0, v24

    move-object/from16 v1, p0

    iput v0, v1, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->bitPointer:I

    .line 990
    const/16 v24, 0x0

    move/from16 v0, v24

    move-object/from16 v1, p0

    iput v0, v1, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->bytePointer:I

    .line 1004
    const/16 v24, 0x2

    move/from16 v0, v24

    new-array v6, v0, [I

    .line 1010
    .local v6, "b":[I
    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->currChangingElems:[I

    .line 1015
    .local v11, "cce":[I
    const/16 v24, 0x0

    move/from16 v0, v24

    move-object/from16 v1, p0

    iput v0, v1, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->changingElemSize:I

    .line 1016
    move-object/from16 v0, p0

    iget v0, v0, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->changingElemSize:I

    move/from16 v24, v0

    add-int/lit8 v25, v24, 0x1

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput v0, v1, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->changingElemSize:I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->w:I

    move/from16 v25, v0

    aput v25, v11, v24

    .line 1017
    move-object/from16 v0, p0

    iget v0, v0, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->changingElemSize:I

    move/from16 v24, v0

    add-int/lit8 v25, v24, 0x1

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput v0, v1, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->changingElemSize:I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->w:I

    move/from16 v25, v0

    aput v25, v11, v24

    .line 1019
    const/16 v18, 0x0

    .line 1022
    .local v18, "lineOffset":I
    const/16 v19, 0x0

    .local v19, "lines":I
    :goto_0
    move/from16 v0, v19

    move/from16 v1, p4

    if-ge v0, v1, :cond_15

    .line 1024
    const/4 v4, -0x1

    .line 1025
    .local v4, "a0":I
    const/16 v17, 0x1

    .line 1030
    .local v17, "isWhite":Z
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->prevChangingElems:[I

    move-object/from16 v22, v0

    .line 1031
    .local v22, "temp":[I
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->currChangingElems:[I

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->prevChangingElems:[I

    .line 1032
    move-object/from16 v0, v22

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->currChangingElems:[I

    move-object/from16 v11, v22

    .line 1033
    const/4 v13, 0x0

    .line 1036
    .local v13, "currIndex":I
    move/from16 v9, p3

    .line 1039
    .local v9, "bitOffset":I
    const/16 v24, 0x0

    move/from16 v0, v24

    move-object/from16 v1, p0

    iput v0, v1, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->lastChangingElement:I

    move v14, v13

    .line 1042
    .end local v13    # "currIndex":I
    .local v14, "currIndex":I
    :cond_0
    :goto_1
    move-object/from16 v0, p0

    iget v0, v0, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->w:I

    move/from16 v24, v0

    move/from16 v0, v24

    if-ge v9, v0, :cond_14

    .line 1044
    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-direct {v0, v4, v1, v6}, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->getNextChangingElement(IZ[I)V

    .line 1045
    const/16 v24, 0x0

    aget v7, v6, v24

    .line 1046
    .local v7, "b1":I
    const/16 v24, 0x1

    aget v8, v6, v24

    .line 1049
    .local v8, "b2":I
    const/16 v24, 0x7

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-direct {v0, v1}, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->nextLesserThan8Bits(I)I

    move-result v15

    .line 1051
    .local v15, "entry":I
    sget-object v24, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->twoDCodes:[B

    aget-byte v24, v24, v15

    move/from16 v0, v24

    and-int/lit16 v15, v0, 0xff

    .line 1054
    and-int/lit8 v24, v15, 0x78

    ushr-int/lit8 v12, v24, 0x3

    .line 1055
    .local v12, "code":I
    and-int/lit8 v10, v15, 0x7

    .line 1057
    .local v10, "bits":I
    if-nez v12, :cond_3

    .line 1059
    if-nez v17, :cond_2

    .line 1060
    move-object/from16 v0, p0

    iget v0, v0, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->w:I

    move/from16 v24, v0

    move/from16 v0, v24

    if-le v8, v0, :cond_1

    .line 1061
    move-object/from16 v0, p0

    iget v8, v0, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->w:I

    .line 1063
    :cond_1
    sub-int v24, v8, v9

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v18

    move/from16 v3, v24

    invoke-direct {v0, v1, v2, v9, v3}, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->setToBlack([BIII)V

    .line 1066
    :cond_2
    move v4, v8

    move v9, v8

    .line 1069
    rsub-int/lit8 v24, v10, 0x7

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-direct {v0, v1}, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->updatePointer(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 987
    .end local v4    # "a0":I
    .end local v6    # "b":[I
    .end local v7    # "b1":I
    .end local v8    # "b2":I
    .end local v9    # "bitOffset":I
    .end local v10    # "bits":I
    .end local v11    # "cce":[I
    .end local v12    # "code":I
    .end local v14    # "currIndex":I
    .end local v15    # "entry":I
    .end local v17    # "isWhite":Z
    .end local v18    # "lineOffset":I
    .end local v19    # "lines":I
    .end local v21    # "scanlineStride":I
    .end local v22    # "temp":[I
    :catchall_0
    move-exception v24

    monitor-exit p0

    throw v24

    .line 1070
    .restart local v4    # "a0":I
    .restart local v6    # "b":[I
    .restart local v7    # "b1":I
    .restart local v8    # "b2":I
    .restart local v9    # "bitOffset":I
    .restart local v10    # "bits":I
    .restart local v11    # "cce":[I
    .restart local v12    # "code":I
    .restart local v14    # "currIndex":I
    .restart local v15    # "entry":I
    .restart local v17    # "isWhite":Z
    .restart local v18    # "lineOffset":I
    .restart local v19    # "lines":I
    .restart local v21    # "scanlineStride":I
    .restart local v22    # "temp":[I
    :cond_3
    const/16 v24, 0x1

    move/from16 v0, v24

    if-ne v12, v0, :cond_7

    .line 1072
    rsub-int/lit8 v24, v10, 0x7

    :try_start_1
    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-direct {v0, v1}, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->updatePointer(I)V

    .line 1076
    if-eqz v17, :cond_5

    .line 1078
    invoke-direct/range {p0 .. p0}, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->decodeWhiteCodeWord()I

    move-result v20

    .line 1079
    .local v20, "number":I
    add-int v9, v9, v20

    .line 1080
    add-int/lit8 v13, v14, 0x1

    .end local v14    # "currIndex":I
    .restart local v13    # "currIndex":I
    aput v9, v11, v14

    .line 1082
    invoke-direct/range {p0 .. p0}, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->decodeBlackCodeWord()I

    move-result v20

    .line 1083
    move-object/from16 v0, p0

    iget v0, v0, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->w:I

    move/from16 v24, v0

    sub-int v24, v24, v9

    move/from16 v0, v20

    move/from16 v1, v24

    if-le v0, v1, :cond_4

    .line 1084
    move-object/from16 v0, p0

    iget v0, v0, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->w:I

    move/from16 v24, v0

    sub-int v20, v24, v9

    .line 1086
    :cond_4
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v18

    move/from16 v3, v20

    invoke-direct {v0, v1, v2, v9, v3}, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->setToBlack([BIII)V

    .line 1087
    add-int v9, v9, v20

    .line 1088
    add-int/lit8 v14, v13, 0x1

    .end local v13    # "currIndex":I
    .restart local v14    # "currIndex":I
    aput v9, v11, v13

    move v13, v14

    .line 1104
    .end local v14    # "currIndex":I
    .restart local v13    # "currIndex":I
    :goto_2
    move v4, v9

    move v14, v13

    .line 1105
    .end local v13    # "currIndex":I
    .restart local v14    # "currIndex":I
    goto/16 :goto_1

    .line 1091
    .end local v20    # "number":I
    :cond_5
    invoke-direct/range {p0 .. p0}, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->decodeBlackCodeWord()I

    move-result v20

    .line 1092
    .restart local v20    # "number":I
    move-object/from16 v0, p0

    iget v0, v0, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->w:I

    move/from16 v24, v0

    sub-int v24, v24, v9

    move/from16 v0, v20

    move/from16 v1, v24

    if-le v0, v1, :cond_6

    .line 1093
    move-object/from16 v0, p0

    iget v0, v0, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->w:I

    move/from16 v24, v0

    sub-int v20, v24, v9

    .line 1095
    :cond_6
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v18

    move/from16 v3, v20

    invoke-direct {v0, v1, v2, v9, v3}, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->setToBlack([BIII)V

    .line 1096
    add-int v9, v9, v20

    .line 1097
    add-int/lit8 v13, v14, 0x1

    .end local v14    # "currIndex":I
    .restart local v13    # "currIndex":I
    aput v9, v11, v14

    .line 1099
    invoke-direct/range {p0 .. p0}, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->decodeWhiteCodeWord()I

    move-result v20

    .line 1100
    add-int v9, v9, v20

    .line 1101
    add-int/lit8 v14, v13, 0x1

    .end local v13    # "currIndex":I
    .restart local v14    # "currIndex":I
    aput v9, v11, v13

    move v13, v14

    .end local v14    # "currIndex":I
    .restart local v13    # "currIndex":I
    goto :goto_2

    .line 1105
    .end local v13    # "currIndex":I
    .end local v20    # "number":I
    .restart local v14    # "currIndex":I
    :cond_7
    const/16 v24, 0x8

    move/from16 v0, v24

    if-gt v12, v0, :cond_b

    .line 1106
    add-int/lit8 v24, v12, -0x5

    add-int v5, v7, v24

    .line 1107
    .local v5, "a1":I
    add-int/lit8 v13, v14, 0x1

    .end local v14    # "currIndex":I
    .restart local v13    # "currIndex":I
    aput v5, v11, v14

    .line 1111
    if-nez v17, :cond_9

    .line 1112
    move-object/from16 v0, p0

    iget v0, v0, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->w:I

    move/from16 v24, v0

    move/from16 v0, v24

    if-le v5, v0, :cond_8

    .line 1113
    move-object/from16 v0, p0

    iget v5, v0, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->w:I

    .line 1115
    :cond_8
    sub-int v24, v5, v9

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v18

    move/from16 v3, v24

    invoke-direct {v0, v1, v2, v9, v3}, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->setToBlack([BIII)V

    .line 1118
    :cond_9
    move v4, v5

    move v9, v5

    .line 1119
    if-nez v17, :cond_a

    const/16 v17, 0x1

    .line 1121
    :goto_3
    rsub-int/lit8 v24, v10, 0x7

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-direct {v0, v1}, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->updatePointer(I)V

    move v14, v13

    .end local v13    # "currIndex":I
    .restart local v14    # "currIndex":I
    goto/16 :goto_1

    .line 1119
    .end local v14    # "currIndex":I
    .restart local v13    # "currIndex":I
    :cond_a
    const/16 v17, 0x0

    goto :goto_3

    .line 1122
    .end local v5    # "a1":I
    .end local v13    # "currIndex":I
    .restart local v14    # "currIndex":I
    :cond_b
    const/16 v24, 0xb

    move/from16 v0, v24

    if-ne v12, v0, :cond_13

    .line 1123
    const/16 v24, 0x3

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-direct {v0, v1}, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->nextLesserThan8Bits(I)I

    move-result v24

    const/16 v25, 0x7

    move/from16 v0, v24

    move/from16 v1, v25

    if-eq v0, v1, :cond_c

    .line 1124
    new-instance v24, Ljava/lang/RuntimeException;

    const-string/jumbo v25, "Invalid code encountered while decoding 2D group 4 compressed data."

    invoke-direct/range {v24 .. v25}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v24

    .line 1128
    :cond_c
    const/16 v23, 0x0

    .line 1129
    .local v23, "zeros":I
    const/16 v16, 0x0

    .line 1131
    .local v16, "exit":Z
    :goto_4
    if-nez v16, :cond_0

    .line 1132
    :goto_5
    const/16 v24, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-direct {v0, v1}, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->nextLesserThan8Bits(I)I

    move-result v24

    const/16 v25, 0x1

    move/from16 v0, v24

    move/from16 v1, v25

    if-eq v0, v1, :cond_d

    .line 1133
    add-int/lit8 v23, v23, 0x1

    goto :goto_5

    .line 1136
    :cond_d
    const/16 v24, 0x5

    move/from16 v0, v23

    move/from16 v1, v24

    if-le v0, v1, :cond_10

    .line 1140
    add-int/lit8 v23, v23, -0x6

    .line 1142
    if-nez v17, :cond_e

    if-lez v23, :cond_e

    .line 1143
    add-int/lit8 v13, v14, 0x1

    .end local v14    # "currIndex":I
    .restart local v13    # "currIndex":I
    aput v9, v11, v14

    move v14, v13

    .line 1147
    .end local v13    # "currIndex":I
    .restart local v14    # "currIndex":I
    :cond_e
    add-int v9, v9, v23

    .line 1148
    if-lez v23, :cond_f

    .line 1150
    const/16 v17, 0x1

    .line 1155
    :cond_f
    const/16 v24, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-direct {v0, v1}, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->nextLesserThan8Bits(I)I

    move-result v24

    if-nez v24, :cond_11

    .line 1156
    if-nez v17, :cond_19

    .line 1157
    add-int/lit8 v13, v14, 0x1

    .end local v14    # "currIndex":I
    .restart local v13    # "currIndex":I
    aput v9, v11, v14

    .line 1159
    :goto_6
    const/16 v17, 0x1

    .line 1167
    :goto_7
    const/16 v16, 0x1

    move v14, v13

    .line 1170
    .end local v13    # "currIndex":I
    .restart local v14    # "currIndex":I
    :cond_10
    const/16 v24, 0x5

    move/from16 v0, v23

    move/from16 v1, v24

    if-ne v0, v1, :cond_12

    .line 1171
    if-nez v17, :cond_17

    .line 1172
    add-int/lit8 v13, v14, 0x1

    .end local v14    # "currIndex":I
    .restart local v13    # "currIndex":I
    aput v9, v11, v14

    .line 1174
    :goto_8
    add-int v9, v9, v23

    .line 1177
    const/16 v17, 0x1

    move v14, v13

    .end local v13    # "currIndex":I
    .restart local v14    # "currIndex":I
    goto :goto_4

    .line 1161
    :cond_11
    if-eqz v17, :cond_18

    .line 1162
    add-int/lit8 v13, v14, 0x1

    .end local v14    # "currIndex":I
    .restart local v13    # "currIndex":I
    aput v9, v11, v14

    .line 1164
    :goto_9
    const/16 v17, 0x0

    goto :goto_7

    .line 1179
    .end local v13    # "currIndex":I
    .restart local v14    # "currIndex":I
    :cond_12
    add-int v9, v9, v23

    .line 1181
    add-int/lit8 v13, v14, 0x1

    .end local v14    # "currIndex":I
    .restart local v13    # "currIndex":I
    aput v9, v11, v14

    .line 1182
    const/16 v24, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v18

    move/from16 v3, v24

    invoke-direct {v0, v1, v2, v9, v3}, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->setToBlack([BIII)V

    .line 1183
    add-int/lit8 v9, v9, 0x1

    .line 1186
    const/16 v17, 0x0

    move v14, v13

    .end local v13    # "currIndex":I
    .restart local v14    # "currIndex":I
    goto :goto_4

    .line 1192
    .end local v16    # "exit":Z
    .end local v23    # "zeros":I
    :cond_13
    rsub-int/lit8 v24, v10, 0x7

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-direct {v0, v1}, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->updatePointer(I)V

    .line 1194
    move-object/from16 v0, p0

    iget v9, v0, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->w:I

    goto/16 :goto_1

    .line 1201
    .end local v7    # "b1":I
    .end local v8    # "b2":I
    .end local v10    # "bits":I
    .end local v12    # "code":I
    .end local v15    # "entry":I
    :cond_14
    invoke-direct/range {p0 .. p0}, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->align()Z

    .line 1206
    move-object/from16 v0, p0

    iget v0, v0, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->w:I

    move/from16 v24, v0

    move/from16 v0, v24

    if-gt v14, v0, :cond_16

    .line 1207
    add-int/lit8 v13, v14, 0x1

    .end local v14    # "currIndex":I
    .restart local v13    # "currIndex":I
    aput v9, v11, v14

    .line 1211
    :goto_a
    move-object/from16 v0, p0

    iput v13, v0, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->changingElemSize:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1213
    add-int v18, v18, v21

    .line 1022
    add-int/lit8 v19, v19, 0x1

    goto/16 :goto_0

    .line 1215
    .end local v4    # "a0":I
    .end local v9    # "bitOffset":I
    .end local v13    # "currIndex":I
    .end local v17    # "isWhite":Z
    .end local v22    # "temp":[I
    :cond_15
    monitor-exit p0

    return-void

    .restart local v4    # "a0":I
    .restart local v9    # "bitOffset":I
    .restart local v14    # "currIndex":I
    .restart local v17    # "isWhite":Z
    .restart local v22    # "temp":[I
    :cond_16
    move v13, v14

    .end local v14    # "currIndex":I
    .restart local v13    # "currIndex":I
    goto :goto_a

    .end local v13    # "currIndex":I
    .restart local v7    # "b1":I
    .restart local v8    # "b2":I
    .restart local v10    # "bits":I
    .restart local v12    # "code":I
    .restart local v14    # "currIndex":I
    .restart local v15    # "entry":I
    .restart local v16    # "exit":Z
    .restart local v23    # "zeros":I
    :cond_17
    move v13, v14

    .end local v14    # "currIndex":I
    .restart local v13    # "currIndex":I
    goto :goto_8

    .end local v13    # "currIndex":I
    .restart local v14    # "currIndex":I
    :cond_18
    move v13, v14

    .end local v14    # "currIndex":I
    .restart local v13    # "currIndex":I
    goto :goto_9

    .end local v13    # "currIndex":I
    .restart local v14    # "currIndex":I
    :cond_19
    move v13, v14

    .end local v14    # "currIndex":I
    .restart local v13    # "currIndex":I
    goto :goto_6
.end method

.method public isAlign()Z
    .locals 1

    .prologue
    .line 1296
    iget-boolean v0, p0, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->align:Z

    return v0
.end method

.method public isFillBits()Z
    .locals 1

    .prologue
    .line 1300
    iget-boolean v0, p0, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->fillBits:Z

    return v0
.end method

.method public setAlign(Z)V
    .locals 0
    .param p1, "align"    # Z

    .prologue
    .line 1536
    iput-boolean p1, p0, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->align:Z

    .line 1537
    return-void
.end method

.method public setFillBits(Z)V
    .locals 0
    .param p1, "fillBits"    # Z

    .prologue
    .line 1540
    iput-boolean p1, p0, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->fillBits:Z

    .line 1541
    return-void
.end method
