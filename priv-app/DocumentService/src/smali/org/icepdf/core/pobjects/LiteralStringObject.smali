.class public Lorg/icepdf/core/pobjects/LiteralStringObject;
.super Ljava/lang/Object;
.source "LiteralStringObject.java"

# interfaces
.implements Lorg/icepdf/core/pobjects/StringObject;


# static fields
.field private static hexChar:[C


# instance fields
.field reference:Lorg/icepdf/core/pobjects/Reference;

.field private stringData:Ljava/lang/StringBuilder;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    const/16 v0, 0x10

    new-array v0, v0, [C

    fill-array-data v0, :array_0

    sput-object v0, Lorg/icepdf/core/pobjects/LiteralStringObject;->hexChar:[C

    return-void

    :array_0
    .array-data 2
        0x30s
        0x31s
        0x32s
        0x33s
        0x34s
        0x35s
        0x36s
        0x37s
        0x38s
        0x39s
        0x61s
        0x62s
        0x63s
        0x64s
        0x65s
        0x66s
    .end array-data
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "string"    # Ljava/lang/String;

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0, p1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lorg/icepdf/core/pobjects/LiteralStringObject;->stringData:Ljava/lang/StringBuilder;

    .line 62
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lorg/icepdf/core/pobjects/Reference;Lorg/icepdf/core/pobjects/security/SecurityManager;)V
    .locals 2
    .param p1, "string"    # Ljava/lang/String;
    .param p2, "reference"    # Lorg/icepdf/core/pobjects/Reference;
    .param p3, "securityManager"    # Lorg/icepdf/core/pobjects/security/SecurityManager;

    .prologue
    .line 78
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 80
    iput-object p2, p0, Lorg/icepdf/core/pobjects/LiteralStringObject;->reference:Lorg/icepdf/core/pobjects/Reference;

    .line 82
    new-instance v0, Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v1, p3}, Lorg/icepdf/core/pobjects/LiteralStringObject;->encryption(Ljava/lang/String;ZLorg/icepdf/core/pobjects/security/SecurityManager;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lorg/icepdf/core/pobjects/LiteralStringObject;->stringData:Ljava/lang/StringBuilder;

    .line 84
    return-void
.end method

.method public constructor <init>(Ljava/lang/StringBuilder;)V
    .locals 2
    .param p1, "stringBuffer"    # Ljava/lang/StringBuilder;

    .prologue
    .line 95
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 97
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->deleteCharAt(I)Ljava/lang/StringBuilder;

    .line 98
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->deleteCharAt(I)Ljava/lang/StringBuilder;

    .line 100
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    iput-object v0, p0, Lorg/icepdf/core/pobjects/LiteralStringObject;->stringData:Ljava/lang/StringBuilder;

    .line 101
    iget-object v0, p0, Lorg/icepdf/core/pobjects/LiteralStringObject;->stringData:Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 102
    return-void
.end method

.method public constructor <init>([B)V
    .locals 2
    .param p1, "bytes"    # [B

    .prologue
    .line 50
    new-instance v0, Ljava/lang/StringBuilder;

    array-length v1, p1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, p1}, Ljava/lang/String;-><init>([B)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/icepdf/core/pobjects/LiteralStringObject;-><init>(Ljava/lang/StringBuilder;)V

    .line 51
    return-void
.end method

.method private stringToHex(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;
    .locals 6
    .param p1, "string"    # Ljava/lang/StringBuilder;

    .prologue
    .line 237
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->length()I

    move-result v4

    mul-int/lit8 v4, v4, 0x2

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 239
    .local v1, "hh":Ljava/lang/StringBuilder;
    const/4 v2, 0x0

    .local v2, "i":I
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    .local v3, "max":I
    :goto_0
    if-ge v2, v3, :cond_0

    .line 240
    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v0

    .line 241
    .local v0, "charCode":I
    sget-object v4, Lorg/icepdf/core/pobjects/LiteralStringObject;->hexChar:[C

    and-int/lit16 v5, v0, 0xf0

    ushr-int/lit8 v5, v5, 0x4

    aget-char v4, v4, v5

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 242
    sget-object v4, Lorg/icepdf/core/pobjects/LiteralStringObject;->hexChar:[C

    and-int/lit8 v5, v0, 0xf

    aget-char v4, v4, v5

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 239
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 244
    .end local v0    # "charCode":I
    :cond_0
    return-object v1
.end method


# virtual methods
.method public encryption(Ljava/lang/String;ZLorg/icepdf/core/pobjects/security/SecurityManager;)Ljava/lang/String;
    .locals 3
    .param p1, "string"    # Ljava/lang/String;
    .param p2, "decrypt"    # Z
    .param p3, "securityManager"    # Lorg/icepdf/core/pobjects/security/SecurityManager;

    .prologue
    .line 286
    if-eqz p3, :cond_0

    iget-object v2, p0, Lorg/icepdf/core/pobjects/LiteralStringObject;->reference:Lorg/icepdf/core/pobjects/Reference;

    if-eqz v2, :cond_0

    .line 288
    invoke-virtual {p3}, Lorg/icepdf/core/pobjects/security/SecurityManager;->getDecryptionKey()[B

    move-result-object v0

    .line 291
    .local v0, "key":[B
    invoke-static {p1}, Lorg/icepdf/core/util/Utils;->convertByteCharSequenceToByteArray(Ljava/lang/CharSequence;)[B

    move-result-object v1

    .line 295
    .local v1, "textBytes":[B
    if-eqz p2, :cond_1

    .line 296
    iget-object v2, p0, Lorg/icepdf/core/pobjects/LiteralStringObject;->reference:Lorg/icepdf/core/pobjects/Reference;

    invoke-virtual {p3, v2, v0, v1}, Lorg/icepdf/core/pobjects/security/SecurityManager;->decrypt(Lorg/icepdf/core/pobjects/Reference;[B[B)[B

    move-result-object v1

    .line 307
    :goto_0
    if-eqz v1, :cond_0

    .line 308
    invoke-static {v1}, Lorg/icepdf/core/util/Utils;->convertByteArrayToByteString([B)Ljava/lang/String;

    move-result-object p1

    .line 310
    .end local v0    # "key":[B
    .end local v1    # "textBytes":[B
    .end local p1    # "string":Ljava/lang/String;
    :cond_0
    return-object p1

    .line 300
    .restart local v0    # "key":[B
    .restart local v1    # "textBytes":[B
    .restart local p1    # "string":Ljava/lang/String;
    :cond_1
    iget-object v2, p0, Lorg/icepdf/core/pobjects/LiteralStringObject;->reference:Lorg/icepdf/core/pobjects/Reference;

    invoke-virtual {p3, v2, v0, v1}, Lorg/icepdf/core/pobjects/security/SecurityManager;->encrypt(Lorg/icepdf/core/pobjects/Reference;[B[B)[B

    move-result-object v1

    goto :goto_0
.end method

.method public getDecryptedLiteralString(Lorg/icepdf/core/pobjects/security/SecurityManager;)Ljava/lang/String;
    .locals 2
    .param p1, "securityManager"    # Lorg/icepdf/core/pobjects/security/SecurityManager;

    .prologue
    .line 272
    iget-object v0, p0, Lorg/icepdf/core/pobjects/LiteralStringObject;->stringData:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1, p1}, Lorg/icepdf/core/pobjects/LiteralStringObject;->encryption(Ljava/lang/String;ZLorg/icepdf/core/pobjects/security/SecurityManager;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getHexString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lorg/icepdf/core/pobjects/LiteralStringObject;->stringData:Ljava/lang/StringBuilder;

    invoke-direct {p0, v0}, Lorg/icepdf/core/pobjects/LiteralStringObject;->stringToHex(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getHexStringBuffer()Ljava/lang/StringBuilder;
    .locals 1

    .prologue
    .line 159
    iget-object v0, p0, Lorg/icepdf/core/pobjects/LiteralStringObject;->stringData:Ljava/lang/StringBuilder;

    invoke-direct {p0, v0}, Lorg/icepdf/core/pobjects/LiteralStringObject;->stringToHex(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;

    move-result-object v0

    return-object v0
.end method

.method public getLength()I
    .locals 1

    .prologue
    .line 227
    iget-object v0, p0, Lorg/icepdf/core/pobjects/LiteralStringObject;->stringData:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    return v0
.end method

.method public getLiteralString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 179
    iget-object v0, p0, Lorg/icepdf/core/pobjects/LiteralStringObject;->stringData:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getLiteralStringBuffer()Ljava/lang/StringBuilder;
    .locals 1

    .prologue
    .line 169
    iget-object v0, p0, Lorg/icepdf/core/pobjects/LiteralStringObject;->stringData:Ljava/lang/StringBuilder;

    return-object v0
.end method

.method public getLiteralStringBuffer(ILorg/icepdf/core/pobjects/fonts/FontFile;)Ljava/lang/StringBuilder;
    .locals 8
    .param p1, "fontFormat"    # I
    .param p2, "font"    # Lorg/icepdf/core/pobjects/fonts/FontFile;

    .prologue
    .line 198
    const/4 v6, 0x1

    if-eq p1, v6, :cond_0

    invoke-interface {p2}, Lorg/icepdf/core/pobjects/fonts/FontFile;->isOneByteEncoding()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 199
    :cond_0
    iget-object v5, p0, Lorg/icepdf/core/pobjects/LiteralStringObject;->stringData:Ljava/lang/StringBuilder;

    .line 218
    :cond_1
    :goto_0
    return-object v5

    .line 200
    :cond_2
    const/4 v6, 0x2

    if-ne p1, v6, :cond_4

    .line 201
    const/4 v0, 0x2

    .line 202
    .local v0, "charOffset":I
    invoke-virtual {p0}, Lorg/icepdf/core/pobjects/LiteralStringObject;->getLength()I

    move-result v4

    .line 203
    .local v4, "length":I
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 204
    .local v5, "tmp":Ljava/lang/StringBuilder;
    const/4 v3, 0x0

    .line 206
    .local v3, "lastIndex":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    if-ge v2, v4, :cond_1

    .line 207
    sub-int v6, v2, v3

    add-int v7, v3, v0

    invoke-virtual {p0, v6, v7}, Lorg/icepdf/core/pobjects/LiteralStringObject;->getUnsignedInt(II)I

    move-result v1

    .line 209
    .local v1, "charValue":I
    if-lez v1, :cond_3

    int-to-char v6, v1

    invoke-interface {p2, v6}, Lorg/icepdf/core/pobjects/fonts/FontFile;->canDisplayEchar(C)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 210
    int-to-char v6, v1

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 211
    const/4 v3, 0x0

    .line 206
    :goto_2
    add-int/2addr v2, v0

    goto :goto_1

    .line 213
    :cond_3
    add-int/2addr v3, v0

    goto :goto_2

    .line 218
    .end local v0    # "charOffset":I
    .end local v1    # "charValue":I
    .end local v2    # "i":I
    .end local v3    # "lastIndex":I
    .end local v4    # "length":I
    .end local v5    # "tmp":Ljava/lang/StringBuilder;
    :cond_4
    const/4 v5, 0x0

    goto :goto_0
.end method

.method public getReference()Lorg/icepdf/core/pobjects/Reference;
    .locals 1

    .prologue
    .line 262
    iget-object v0, p0, Lorg/icepdf/core/pobjects/LiteralStringObject;->reference:Lorg/icepdf/core/pobjects/Reference;

    return-object v0
.end method

.method public getUnsignedInt(II)I
    .locals 3
    .param p1, "start"    # I
    .param p2, "offset"    # I

    .prologue
    const/4 v0, 0x0

    .line 113
    if-ltz p1, :cond_0

    iget-object v1, p0, Lorg/icepdf/core/pobjects/LiteralStringObject;->stringData:Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    add-int v2, p1, p2

    if-ge v1, v2, :cond_2

    .line 114
    :cond_0
    iget-object v1, p0, Lorg/icepdf/core/pobjects/LiteralStringObject;->stringData:Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v0

    .line 128
    :cond_1
    :goto_0
    return v0

    .line 116
    :cond_2
    const/4 v1, 0x1

    if-ne p2, v1, :cond_3

    .line 117
    iget-object v0, p0, Lorg/icepdf/core/pobjects/LiteralStringObject;->stringData:Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v0

    goto :goto_0

    .line 119
    :cond_3
    const/4 v1, 0x2

    if-ne p2, v1, :cond_4

    .line 120
    iget-object v0, p0, Lorg/icepdf/core/pobjects/LiteralStringObject;->stringData:Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v0

    and-int/lit16 v0, v0, 0xff

    shl-int/lit8 v0, v0, 0x8

    iget-object v1, p0, Lorg/icepdf/core/pobjects/LiteralStringObject;->stringData:Ljava/lang/StringBuilder;

    add-int/lit8 v2, p1, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v1

    and-int/lit16 v1, v1, 0xff

    or-int/2addr v0, v1

    goto :goto_0

    .line 122
    :cond_4
    const/4 v1, 0x4

    if-ne p2, v1, :cond_1

    .line 123
    iget-object v0, p0, Lorg/icepdf/core/pobjects/LiteralStringObject;->stringData:Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v0

    and-int/lit16 v0, v0, 0xff

    shl-int/lit8 v0, v0, 0x18

    iget-object v1, p0, Lorg/icepdf/core/pobjects/LiteralStringObject;->stringData:Ljava/lang/StringBuilder;

    add-int/lit8 v2, p1, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v1

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x10

    or-int/2addr v0, v1

    iget-object v1, p0, Lorg/icepdf/core/pobjects/LiteralStringObject;->stringData:Ljava/lang/StringBuilder;

    add-int/lit8 v2, p1, 0x2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v1

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x8

    or-int/2addr v0, v1

    iget-object v1, p0, Lorg/icepdf/core/pobjects/LiteralStringObject;->stringData:Ljava/lang/StringBuilder;

    add-int/lit8 v2, p1, 0x3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v1

    and-int/lit16 v1, v1, 0xff

    or-int/2addr v0, v1

    goto :goto_0
.end method

.method public setReference(Lorg/icepdf/core/pobjects/Reference;)V
    .locals 0
    .param p1, "reference"    # Lorg/icepdf/core/pobjects/Reference;

    .prologue
    .line 253
    iput-object p1, p0, Lorg/icepdf/core/pobjects/LiteralStringObject;->reference:Lorg/icepdf/core/pobjects/Reference;

    .line 254
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lorg/icepdf/core/pobjects/LiteralStringObject;->stringData:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
