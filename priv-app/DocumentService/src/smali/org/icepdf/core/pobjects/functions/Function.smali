.class public abstract Lorg/icepdf/core/pobjects/functions/Function;
.super Ljava/lang/Object;
.source "Function.java"


# static fields
.field private static final logger:Ljava/util/logging/Logger;


# instance fields
.field protected domain:[F

.field protected functionType:I

.field protected range:[F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 62
    const-class v0, Lorg/icepdf/core/pobjects/functions/Function;

    invoke-virtual {v0}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Lorg/icepdf/core/pobjects/functions/Function;->logger:Ljava/util/logging/Logger;

    return-void
.end method

.method protected constructor <init>(Lorg/icepdf/core/pobjects/Dictionary;)V
    .locals 5
    .param p1, "d"    # Lorg/icepdf/core/pobjects/Dictionary;

    .prologue
    .line 137
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 138
    const-string/jumbo v3, "Domain"

    invoke-virtual {p1, v3}, Lorg/icepdf/core/pobjects/Dictionary;->getObject(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Vector;

    .line 139
    .local v0, "dom":Ljava/util/Vector;
    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v3

    new-array v3, v3, [F

    iput-object v3, p0, Lorg/icepdf/core/pobjects/functions/Function;->domain:[F

    .line 140
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v3

    if-ge v1, v3, :cond_0

    .line 141
    iget-object v4, p0, Lorg/icepdf/core/pobjects/functions/Function;->domain:[F

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Number;

    invoke-virtual {v3}, Ljava/lang/Number;->floatValue()F

    move-result v3

    aput v3, v4, v1

    .line 140
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 143
    :cond_0
    const-string/jumbo v3, "Range"

    invoke-virtual {p1, v3}, Lorg/icepdf/core/pobjects/Dictionary;->getObject(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Vector;

    .line 144
    .local v2, "r":Ljava/util/Vector;
    if-eqz v2, :cond_1

    .line 145
    invoke-virtual {v2}, Ljava/util/Vector;->size()I

    move-result v3

    new-array v3, v3, [F

    iput-object v3, p0, Lorg/icepdf/core/pobjects/functions/Function;->range:[F

    .line 146
    const/4 v1, 0x0

    :goto_1
    invoke-virtual {v2}, Ljava/util/Vector;->size()I

    move-result v3

    if-ge v1, v3, :cond_1

    .line 147
    iget-object v4, p0, Lorg/icepdf/core/pobjects/functions/Function;->range:[F

    invoke-virtual {v2, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Number;

    invoke-virtual {v3}, Ljava/lang/Number;->floatValue()F

    move-result v3

    aput v3, v4, v1

    .line 146
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 150
    :cond_1
    return-void
.end method

.method public static getFunction(Lorg/icepdf/core/util/Library;Ljava/lang/Object;)Lorg/icepdf/core/pobjects/functions/Function;
    .locals 3
    .param p0, "l"    # Lorg/icepdf/core/util/Library;
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 97
    const/4 v0, 0x0

    .line 99
    .local v0, "d":Lorg/icepdf/core/pobjects/Dictionary;
    instance-of v2, p1, Lorg/icepdf/core/pobjects/Reference;

    if-eqz v2, :cond_0

    .line 100
    check-cast p1, Lorg/icepdf/core/pobjects/Reference;

    .end local p1    # "o":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lorg/icepdf/core/util/Library;->getObject(Lorg/icepdf/core/pobjects/Reference;)Ljava/lang/Object;

    move-result-object p1

    .line 104
    .restart local p1    # "o":Ljava/lang/Object;
    :cond_0
    instance-of v2, p1, Lorg/icepdf/core/pobjects/Dictionary;

    if-eqz v2, :cond_3

    move-object v0, p1

    .line 105
    check-cast v0, Lorg/icepdf/core/pobjects/Dictionary;

    .line 110
    .end local p1    # "o":Ljava/lang/Object;
    :cond_1
    :goto_0
    if-eqz v0, :cond_2

    .line 113
    const-string/jumbo v2, "FunctionType"

    invoke-virtual {v0, v2}, Lorg/icepdf/core/pobjects/Dictionary;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 114
    .local v1, "fType":I
    packed-switch v1, :pswitch_data_0

    .line 129
    .end local v1    # "fType":I
    :cond_2
    :pswitch_0
    const/4 v2, 0x0

    :goto_1
    return-object v2

    .line 106
    .restart local p1    # "o":Ljava/lang/Object;
    :cond_3
    instance-of v2, p1, Ljava/util/Hashtable;

    if-eqz v2, :cond_1

    .line 107
    new-instance v0, Lorg/icepdf/core/pobjects/Dictionary;

    .end local v0    # "d":Lorg/icepdf/core/pobjects/Dictionary;
    check-cast p1, Ljava/util/Hashtable;

    .end local p1    # "o":Ljava/lang/Object;
    invoke-direct {v0, p0, p1}, Lorg/icepdf/core/pobjects/Dictionary;-><init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V

    .restart local v0    # "d":Lorg/icepdf/core/pobjects/Dictionary;
    goto :goto_0

    .line 117
    .restart local v1    # "fType":I
    :pswitch_1
    new-instance v2, Lorg/icepdf/core/pobjects/functions/Function_0;

    invoke-direct {v2, v0}, Lorg/icepdf/core/pobjects/functions/Function_0;-><init>(Lorg/icepdf/core/pobjects/Dictionary;)V

    goto :goto_1

    .line 120
    :pswitch_2
    new-instance v2, Lorg/icepdf/core/pobjects/functions/Function_2;

    invoke-direct {v2, v0}, Lorg/icepdf/core/pobjects/functions/Function_2;-><init>(Lorg/icepdf/core/pobjects/Dictionary;)V

    goto :goto_1

    .line 123
    :pswitch_3
    new-instance v2, Lorg/icepdf/core/pobjects/functions/Function_3;

    invoke-direct {v2, v0}, Lorg/icepdf/core/pobjects/functions/Function_3;-><init>(Lorg/icepdf/core/pobjects/Dictionary;)V

    goto :goto_1

    .line 126
    :pswitch_4
    new-instance v2, Lorg/icepdf/core/pobjects/functions/Function_4;

    invoke-direct {v2, v0}, Lorg/icepdf/core/pobjects/functions/Function_4;-><init>(Lorg/icepdf/core/pobjects/Dictionary;)V

    goto :goto_1

    .line 114
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static interpolate(FFFFF)F
    .locals 2
    .param p0, "x"    # F
    .param p1, "xmin"    # F
    .param p2, "xmax"    # F
    .param p3, "ymin"    # F
    .param p4, "ymax"    # F

    .prologue
    .line 187
    sub-float v0, p0, p1

    sub-float v1, p4, p3

    mul-float/2addr v0, v1

    sub-float v1, p2, p1

    div-float/2addr v0, v1

    add-float/2addr v0, p3

    return v0
.end method


# virtual methods
.method public abstract calculate([F)[F
.end method

.method public getDomain()[F
    .locals 1

    .prologue
    .line 204
    iget-object v0, p0, Lorg/icepdf/core/pobjects/functions/Function;->domain:[F

    return-object v0
.end method

.method public getFunctionType()I
    .locals 1

    .prologue
    .line 170
    iget v0, p0, Lorg/icepdf/core/pobjects/functions/Function;->functionType:I

    return v0
.end method

.method public getRange()[F
    .locals 1

    .prologue
    .line 208
    iget-object v0, p0, Lorg/icepdf/core/pobjects/functions/Function;->range:[F

    return-object v0
.end method
