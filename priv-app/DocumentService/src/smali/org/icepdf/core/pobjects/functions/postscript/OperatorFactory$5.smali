.class final Lorg/icepdf/core/pobjects/functions/postscript/OperatorFactory$5;
.super Lorg/icepdf/core/pobjects/functions/postscript/Operator;
.source "OperatorFactory.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/icepdf/core/pobjects/functions/postscript/OperatorFactory;->getOperator([CII)Lorg/icepdf/core/pobjects/functions/postscript/Operator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>(I)V
    .locals 0
    .param p1, "x0"    # I

    .prologue
    .line 123
    invoke-direct {p0, p1}, Lorg/icepdf/core/pobjects/functions/postscript/Operator;-><init>(I)V

    return-void
.end method


# virtual methods
.method public eval(Ljava/util/Stack;)V
    .locals 6
    .param p1, "stack"    # Ljava/util/Stack;

    .prologue
    .line 125
    invoke-virtual {p1}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 126
    .local v2, "shift":J
    invoke-virtual {p1}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 127
    .local v0, "int1":J
    long-to-int v4, v2

    shl-long v4, v0, v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {p1, v4}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 128
    return-void
.end method
