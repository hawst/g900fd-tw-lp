.class public Lorg/icepdf/core/pobjects/PageTree;
.super Lorg/icepdf/core/pobjects/Dictionary;
.source "PageTree.java"


# instance fields
.field private cropBox:Lorg/icepdf/core/pobjects/PRectangle;

.field private inited:Z

.field protected isRotationFactor:Z

.field private kidsCount:I

.field private kidsPageAndPages:Ljava/util/Vector;

.field private kidsReferences:Ljava/util/Vector;

.field private loadedResources:Z

.field private mediaBox:Lorg/icepdf/core/pobjects/PRectangle;

.field private parent:Lorg/icepdf/core/pobjects/PageTree;

.field private resources:Lorg/icepdf/core/pobjects/Resources;

.field protected rotationFactor:F


# direct methods
.method public constructor <init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V
    .locals 2
    .param p1, "l"    # Lorg/icepdf/core/util/Library;
    .param p2, "h"    # Ljava/util/Hashtable;

    .prologue
    const/4 v1, 0x0

    .line 75
    invoke-direct {p0, p1, p2}, Lorg/icepdf/core/pobjects/Dictionary;-><init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V

    .line 40
    iput v1, p0, Lorg/icepdf/core/pobjects/PageTree;->kidsCount:I

    .line 61
    const/4 v0, 0x0

    iput v0, p0, Lorg/icepdf/core/pobjects/PageTree;->rotationFactor:F

    .line 66
    iput-boolean v1, p0, Lorg/icepdf/core/pobjects/PageTree;->isRotationFactor:Z

    .line 76
    return-void
.end method

.method private getPageOrPagesPotentiallyNotInitedFromReferenceAt(I)Ljava/lang/Object;
    .locals 3
    .param p1, "index"    # I

    .prologue
    .line 275
    iget-object v2, p0, Lorg/icepdf/core/pobjects/PageTree;->kidsPageAndPages:Ljava/util/Vector;

    invoke-virtual {v2, p1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 276
    .local v0, "pageOrPages":Ljava/lang/Object;
    if-nez v0, :cond_0

    .line 277
    iget-object v2, p0, Lorg/icepdf/core/pobjects/PageTree;->kidsReferences:Ljava/util/Vector;

    invoke-virtual {v2, p1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/icepdf/core/pobjects/Reference;

    .line 278
    .local v1, "ref":Lorg/icepdf/core/pobjects/Reference;
    iget-object v2, p0, Lorg/icepdf/core/pobjects/PageTree;->library:Lorg/icepdf/core/util/Library;

    invoke-virtual {v2, v1}, Lorg/icepdf/core/util/Library;->getObject(Lorg/icepdf/core/pobjects/Reference;)Ljava/lang/Object;

    move-result-object v0

    .line 279
    iget-object v2, p0, Lorg/icepdf/core/pobjects/PageTree;->kidsPageAndPages:Ljava/util/Vector;

    invoke-virtual {v2, p1, v0}, Ljava/util/Vector;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 281
    .end local v1    # "ref":Lorg/icepdf/core/pobjects/Reference;
    :cond_0
    return-object v0
.end method

.method private getPagePotentiallyNotInitedByRecursiveIndex(I)Lorg/icepdf/core/pobjects/Page;
    .locals 7
    .param p1, "globalIndex"    # I

    .prologue
    .line 291
    const/4 v1, 0x0

    .line 292
    .local v1, "globalIndexSoFar":I
    iget-object v6, p0, Lorg/icepdf/core/pobjects/PageTree;->kidsPageAndPages:Ljava/util/Vector;

    invoke-virtual {v6}, Ljava/util/Vector;->size()I

    move-result v4

    .line 293
    .local v4, "numLocalKids":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v4, :cond_4

    .line 294
    invoke-direct {p0, v2}, Lorg/icepdf/core/pobjects/PageTree;->getPageOrPagesPotentiallyNotInitedFromReferenceAt(I)Ljava/lang/Object;

    move-result-object v5

    .line 295
    .local v5, "pageOrPages":Ljava/lang/Object;
    instance-of v6, v5, Lorg/icepdf/core/pobjects/Page;

    if-eqz v6, :cond_2

    .line 296
    if-ne p1, v1, :cond_0

    .line 297
    check-cast v5, Lorg/icepdf/core/pobjects/Page;

    .line 310
    .end local v5    # "pageOrPages":Ljava/lang/Object;
    :goto_1
    return-object v5

    .line 298
    .restart local v5    # "pageOrPages":Ljava/lang/Object;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    .line 293
    :cond_1
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 299
    :cond_2
    instance-of v6, v5, Lorg/icepdf/core/pobjects/PageTree;

    if-eqz v6, :cond_1

    move-object v0, v5

    .line 300
    check-cast v0, Lorg/icepdf/core/pobjects/PageTree;

    .line 301
    .local v0, "childPageTree":Lorg/icepdf/core/pobjects/PageTree;
    invoke-virtual {v0}, Lorg/icepdf/core/pobjects/PageTree;->init()V

    .line 302
    invoke-virtual {v0}, Lorg/icepdf/core/pobjects/PageTree;->getNumberOfPages()I

    move-result v3

    .line 303
    .local v3, "numChildPages":I
    if-lt p1, v1, :cond_3

    add-int v6, v1, v3

    if-ge p1, v6, :cond_3

    .line 304
    sub-int v6, p1, v1

    invoke-direct {v0, v6}, Lorg/icepdf/core/pobjects/PageTree;->getPagePotentiallyNotInitedByRecursiveIndex(I)Lorg/icepdf/core/pobjects/Page;

    move-result-object v5

    goto :goto_1

    .line 307
    :cond_3
    add-int/2addr v1, v3

    goto :goto_2

    .line 310
    .end local v0    # "childPageTree":Lorg/icepdf/core/pobjects/PageTree;
    .end local v3    # "numChildPages":I
    .end local v5    # "pageOrPages":Ljava/lang/Object;
    :cond_4
    const/4 v5, 0x0

    goto :goto_1
.end method

.method private indexOfKidReference(Lorg/icepdf/core/pobjects/Reference;)I
    .locals 3
    .param p1, "r"    # Lorg/icepdf/core/pobjects/Reference;

    .prologue
    .line 260
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lorg/icepdf/core/pobjects/PageTree;->kidsReferences:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 261
    iget-object v2, p0, Lorg/icepdf/core/pobjects/PageTree;->kidsReferences:Ljava/util/Vector;

    invoke-virtual {v2, v0}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/icepdf/core/pobjects/Reference;

    .line 262
    .local v1, "ref":Lorg/icepdf/core/pobjects/Reference;
    invoke-virtual {v1, p1}, Lorg/icepdf/core/pobjects/Reference;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 265
    .end local v0    # "i":I
    .end local v1    # "ref":Lorg/icepdf/core/pobjects/Reference;
    :goto_1
    return v0

    .line 260
    .restart local v0    # "i":I
    .restart local v1    # "ref":Lorg/icepdf/core/pobjects/Reference;
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 265
    .end local v1    # "ref":Lorg/icepdf/core/pobjects/Reference;
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method


# virtual methods
.method protected declared-synchronized dispose(Z)V
    .locals 4
    .param p1, "cache"    # Z

    .prologue
    .line 82
    monitor-enter p0

    :try_start_0
    iget-object v3, p0, Lorg/icepdf/core/pobjects/PageTree;->kidsReferences:Ljava/util/Vector;

    if-eqz v3, :cond_0

    .line 83
    if-nez p1, :cond_0

    .line 84
    iget-object v3, p0, Lorg/icepdf/core/pobjects/PageTree;->kidsReferences:Ljava/util/Vector;

    invoke-virtual {v3}, Ljava/util/Vector;->clear()V

    .line 85
    iget-object v3, p0, Lorg/icepdf/core/pobjects/PageTree;->kidsReferences:Ljava/util/Vector;

    invoke-virtual {v3}, Ljava/util/Vector;->trimToSize()V

    .line 88
    :cond_0
    iget-object v3, p0, Lorg/icepdf/core/pobjects/PageTree;->kidsPageAndPages:Ljava/util/Vector;

    if-eqz v3, :cond_4

    .line 89
    iget-object v3, p0, Lorg/icepdf/core/pobjects/PageTree;->kidsPageAndPages:Ljava/util/Vector;

    invoke-virtual {v3}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 90
    .local v2, "pageOrPages":Ljava/lang/Object;
    instance-of v3, v2, Lorg/icepdf/core/pobjects/Page;

    if-eqz v3, :cond_2

    .line 91
    check-cast v2, Lorg/icepdf/core/pobjects/Page;

    .end local v2    # "pageOrPages":Ljava/lang/Object;
    invoke-virtual {v2, p1}, Lorg/icepdf/core/pobjects/Page;->dispose(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 82
    .end local v1    # "i$":Ljava/util/Iterator;
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3

    .line 92
    .restart local v1    # "i$":Ljava/util/Iterator;
    .restart local v2    # "pageOrPages":Ljava/lang/Object;
    :cond_2
    :try_start_1
    instance-of v3, v2, Lorg/icepdf/core/pobjects/PageTree;

    if-eqz v3, :cond_1

    .line 93
    check-cast v2, Lorg/icepdf/core/pobjects/PageTree;

    .end local v2    # "pageOrPages":Ljava/lang/Object;
    invoke-virtual {v2, p1}, Lorg/icepdf/core/pobjects/PageTree;->dispose(Z)V

    goto :goto_0

    .line 95
    :cond_3
    if-nez p1, :cond_4

    .line 96
    iget-object v3, p0, Lorg/icepdf/core/pobjects/PageTree;->kidsPageAndPages:Ljava/util/Vector;

    invoke-virtual {v3}, Ljava/util/Vector;->clear()V

    .line 97
    iget-object v3, p0, Lorg/icepdf/core/pobjects/PageTree;->kidsPageAndPages:Ljava/util/Vector;

    invoke-virtual {v3}, Ljava/util/Vector;->trimToSize()V

    .line 109
    .end local v1    # "i$":Ljava/util/Iterator;
    :cond_4
    iget-object v3, p0, Lorg/icepdf/core/pobjects/PageTree;->resources:Lorg/icepdf/core/pobjects/Resources;

    if-eqz v3, :cond_5

    .line 110
    iget-object v3, p0, Lorg/icepdf/core/pobjects/PageTree;->resources:Lorg/icepdf/core/pobjects/Resources;

    invoke-virtual {v3, p1, p0}, Lorg/icepdf/core/pobjects/Resources;->dispose(ZLorg/icepdf/core/pobjects/Dictionary;)Z

    move-result v0

    .line 111
    .local v0, "disposeSuccess":Z
    if-eqz v0, :cond_5

    .line 112
    const/4 v3, 0x0

    iput-boolean v3, p0, Lorg/icepdf/core/pobjects/PageTree;->loadedResources:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 115
    .end local v0    # "disposeSuccess":Z
    :cond_5
    monitor-exit p0

    return-void
.end method

.method public getCropBox()Lorg/icepdf/core/pobjects/PRectangle;
    .locals 1

    .prologue
    .line 180
    iget-object v0, p0, Lorg/icepdf/core/pobjects/PageTree;->cropBox:Lorg/icepdf/core/pobjects/PRectangle;

    return-object v0
.end method

.method public getMediaBox()Lorg/icepdf/core/pobjects/PRectangle;
    .locals 1

    .prologue
    .line 170
    iget-object v0, p0, Lorg/icepdf/core/pobjects/PageTree;->mediaBox:Lorg/icepdf/core/pobjects/PRectangle;

    return-object v0
.end method

.method public declared-synchronized getNumberOfPages()I
    .locals 1

    .prologue
    .line 327
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lorg/icepdf/core/pobjects/PageTree;->kidsCount:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getPage(ILjava/lang/Object;)Lorg/icepdf/core/pobjects/Page;
    .locals 2
    .param p1, "pageNumber"    # I
    .param p2, "user"    # Ljava/lang/Object;

    .prologue
    .line 349
    if-gez p1, :cond_1

    .line 350
    const/4 v0, 0x0

    .line 357
    :cond_0
    :goto_0
    return-object v0

    .line 351
    :cond_1
    invoke-direct {p0, p1}, Lorg/icepdf/core/pobjects/PageTree;->getPagePotentiallyNotInitedByRecursiveIndex(I)Lorg/icepdf/core/pobjects/Page;

    move-result-object v0

    .line 352
    .local v0, "p":Lorg/icepdf/core/pobjects/Page;
    if-eqz v0, :cond_0

    .line 354
    iget-object v1, p0, Lorg/icepdf/core/pobjects/PageTree;->library:Lorg/icepdf/core/util/Library;

    iget-object v1, v1, Lorg/icepdf/core/util/Library;->memoryManager:Lorg/icepdf/core/util/MemoryManager;

    invoke-virtual {v1, p2, v0}, Lorg/icepdf/core/util/MemoryManager;->lock(Ljava/lang/Object;Lorg/icepdf/core/util/MemoryManageable;)V

    goto :goto_0
.end method

.method public getPageNumber(Lorg/icepdf/core/pobjects/Reference;)I
    .locals 14
    .param p1, "r"    # Lorg/icepdf/core/pobjects/Reference;

    .prologue
    const/4 v10, -0x1

    .line 216
    iget-object v11, p0, Lorg/icepdf/core/pobjects/PageTree;->library:Lorg/icepdf/core/util/Library;

    invoke-virtual {v11, p1}, Lorg/icepdf/core/util/Library;->getObject(Lorg/icepdf/core/pobjects/Reference;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lorg/icepdf/core/pobjects/Page;

    .line 217
    .local v8, "pg":Lorg/icepdf/core/pobjects/Page;
    if-nez v8, :cond_1

    move v3, v10

    .line 250
    :cond_0
    :goto_0
    return v3

    .line 220
    :cond_1
    const/4 v3, 0x0

    .line 221
    .local v3, "globalIndex":I
    move-object v0, p1

    .line 222
    .local v0, "currChildRef":Lorg/icepdf/core/pobjects/Reference;
    invoke-virtual {v8}, Lorg/icepdf/core/pobjects/Page;->getParentReference()Lorg/icepdf/core/pobjects/Reference;

    move-result-object v2

    .line 223
    .local v2, "currParentRef":Lorg/icepdf/core/pobjects/Reference;
    invoke-virtual {v8}, Lorg/icepdf/core/pobjects/Page;->getParent()Lorg/icepdf/core/pobjects/PageTree;

    move-result-object v1

    .line 224
    .local v1, "currParent":Lorg/icepdf/core/pobjects/PageTree;
    :goto_1
    if-eqz v2, :cond_0

    if-eqz v1, :cond_0

    .line 225
    invoke-virtual {v1}, Lorg/icepdf/core/pobjects/PageTree;->init()V

    .line 226
    invoke-direct {v1, v0}, Lorg/icepdf/core/pobjects/PageTree;->indexOfKidReference(Lorg/icepdf/core/pobjects/Reference;)I

    move-result v9

    .line 227
    .local v9, "refIndex":I
    if-gez v9, :cond_2

    move v3, v10

    .line 228
    goto :goto_0

    .line 229
    :cond_2
    const/4 v5, 0x0

    .line 230
    .local v5, "localIndex":I
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_2
    if-ge v4, v9, :cond_5

    .line 231
    invoke-direct {v1, v4}, Lorg/icepdf/core/pobjects/PageTree;->getPageOrPagesPotentiallyNotInitedFromReferenceAt(I)Ljava/lang/Object;

    move-result-object v6

    .line 232
    .local v6, "pageOrPages":Ljava/lang/Object;
    instance-of v11, v6, Lorg/icepdf/core/pobjects/Page;

    if-eqz v11, :cond_4

    .line 233
    add-int/lit8 v5, v5, 0x1

    .line 230
    :cond_3
    :goto_3
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 234
    :cond_4
    instance-of v11, v6, Lorg/icepdf/core/pobjects/PageTree;

    if-eqz v11, :cond_3

    move-object v7, v6

    .line 235
    check-cast v7, Lorg/icepdf/core/pobjects/PageTree;

    .line 236
    .local v7, "peerPageTree":Lorg/icepdf/core/pobjects/PageTree;
    invoke-virtual {v7}, Lorg/icepdf/core/pobjects/PageTree;->init()V

    .line 237
    invoke-virtual {v7}, Lorg/icepdf/core/pobjects/PageTree;->getNumberOfPages()I

    move-result v11

    add-int/2addr v5, v11

    goto :goto_3

    .line 240
    .end local v6    # "pageOrPages":Ljava/lang/Object;
    .end local v7    # "peerPageTree":Lorg/icepdf/core/pobjects/PageTree;
    :cond_5
    add-int/2addr v3, v5

    .line 241
    move-object v0, v2

    .line 242
    iget-object v11, v1, Lorg/icepdf/core/pobjects/PageTree;->entries:Ljava/util/Hashtable;

    const-string/jumbo v12, "Parent"

    invoke-virtual {v11, v12}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "currParentRef":Lorg/icepdf/core/pobjects/Reference;
    check-cast v2, Lorg/icepdf/core/pobjects/Reference;

    .line 243
    .restart local v2    # "currParentRef":Lorg/icepdf/core/pobjects/Reference;
    if-nez v2, :cond_6

    .line 245
    iget-object v11, v1, Lorg/icepdf/core/pobjects/PageTree;->entries:Ljava/util/Hashtable;

    new-instance v12, Lorg/icepdf/core/pobjects/Name;

    const-string/jumbo v13, "Parent"

    invoke-direct {v12, v13}, Lorg/icepdf/core/pobjects/Name;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v12}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "currParentRef":Lorg/icepdf/core/pobjects/Reference;
    check-cast v2, Lorg/icepdf/core/pobjects/Reference;

    .line 248
    .restart local v2    # "currParentRef":Lorg/icepdf/core/pobjects/Reference;
    :cond_6
    iget-object v1, v1, Lorg/icepdf/core/pobjects/PageTree;->parent:Lorg/icepdf/core/pobjects/PageTree;

    .line 249
    goto :goto_1
.end method

.method public getPageReference(I)Lorg/icepdf/core/pobjects/Reference;
    .locals 2
    .param p1, "pageNumber"    # I

    .prologue
    const/4 v1, 0x0

    .line 367
    if-gez p1, :cond_1

    .line 373
    :cond_0
    :goto_0
    return-object v1

    .line 369
    :cond_1
    invoke-direct {p0, p1}, Lorg/icepdf/core/pobjects/PageTree;->getPagePotentiallyNotInitedByRecursiveIndex(I)Lorg/icepdf/core/pobjects/Page;

    move-result-object v0

    .line 370
    .local v0, "p":Lorg/icepdf/core/pobjects/Page;
    if-eqz v0, :cond_0

    .line 371
    invoke-virtual {v0}, Lorg/icepdf/core/pobjects/Page;->getPObjectReference()Lorg/icepdf/core/pobjects/Reference;

    move-result-object v1

    goto :goto_0
.end method

.method public getParent()Lorg/icepdf/core/pobjects/PageTree;
    .locals 1

    .prologue
    .line 205
    iget-object v0, p0, Lorg/icepdf/core/pobjects/PageTree;->parent:Lorg/icepdf/core/pobjects/PageTree;

    return-object v0
.end method

.method public getResources()Lorg/icepdf/core/pobjects/Resources;
    .locals 3

    .prologue
    .line 192
    iget-boolean v0, p0, Lorg/icepdf/core/pobjects/PageTree;->loadedResources:Z

    if-nez v0, :cond_0

    .line 193
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/icepdf/core/pobjects/PageTree;->loadedResources:Z

    .line 194
    iget-object v0, p0, Lorg/icepdf/core/pobjects/PageTree;->library:Lorg/icepdf/core/util/Library;

    iget-object v1, p0, Lorg/icepdf/core/pobjects/PageTree;->entries:Ljava/util/Hashtable;

    const-string/jumbo v2, "Resources"

    invoke-virtual {v0, v1, v2}, Lorg/icepdf/core/util/Library;->getResources(Ljava/util/Hashtable;Ljava/lang/String;)Lorg/icepdf/core/pobjects/Resources;

    move-result-object v0

    iput-object v0, p0, Lorg/icepdf/core/pobjects/PageTree;->resources:Lorg/icepdf/core/pobjects/Resources;

    .line 196
    :cond_0
    iget-object v0, p0, Lorg/icepdf/core/pobjects/PageTree;->resources:Lorg/icepdf/core/pobjects/Resources;

    return-object v0
.end method

.method public declared-synchronized init()V
    .locals 8

    .prologue
    .line 121
    monitor-enter p0

    :try_start_0
    iget-boolean v5, p0, Lorg/icepdf/core/pobjects/PageTree;->inited:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v5, :cond_0

    .line 157
    :goto_0
    monitor-exit p0

    return-void

    .line 124
    :cond_0
    :try_start_1
    iget-object v5, p0, Lorg/icepdf/core/pobjects/PageTree;->library:Lorg/icepdf/core/util/Library;

    iget-object v6, p0, Lorg/icepdf/core/pobjects/PageTree;->entries:Ljava/util/Hashtable;

    const-string/jumbo v7, "Parent"

    invoke-virtual {v5, v6, v7}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    .line 125
    .local v3, "parentTree":Ljava/lang/Object;
    instance-of v5, v3, Lorg/icepdf/core/pobjects/PageTree;

    if-eqz v5, :cond_1

    .line 126
    iget-object v5, p0, Lorg/icepdf/core/pobjects/PageTree;->library:Lorg/icepdf/core/util/Library;

    iget-object v6, p0, Lorg/icepdf/core/pobjects/PageTree;->entries:Ljava/util/Hashtable;

    const-string/jumbo v7, "Parent"

    invoke-virtual {v5, v6, v7}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/icepdf/core/pobjects/PageTree;

    iput-object v5, p0, Lorg/icepdf/core/pobjects/PageTree;->parent:Lorg/icepdf/core/pobjects/PageTree;

    .line 128
    :cond_1
    iget-object v5, p0, Lorg/icepdf/core/pobjects/PageTree;->library:Lorg/icepdf/core/util/Library;

    iget-object v6, p0, Lorg/icepdf/core/pobjects/PageTree;->entries:Ljava/util/Hashtable;

    const-string/jumbo v7, "Count"

    invoke-virtual {v5, v6, v7}, Lorg/icepdf/core/util/Library;->getNumber(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Number;

    move-result-object v2

    .line 129
    .local v2, "nn":Ljava/lang/Number;
    if-eqz v2, :cond_5

    .line 130
    invoke-virtual {v2}, Ljava/lang/Number;->intValue()I

    move-result v5

    iput v5, p0, Lorg/icepdf/core/pobjects/PageTree;->kidsCount:I

    .line 134
    :goto_1
    iget-object v5, p0, Lorg/icepdf/core/pobjects/PageTree;->library:Lorg/icepdf/core/util/Library;

    iget-object v6, p0, Lorg/icepdf/core/pobjects/PageTree;->entries:Ljava/util/Hashtable;

    const-string/jumbo v7, "MediaBox"

    invoke-virtual {v5, v6, v7}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/Vector;

    move-object v0, v5

    check-cast v0, Ljava/util/Vector;

    move-object v1, v0

    .line 135
    .local v1, "boxDimensions":Ljava/util/Vector;
    if-eqz v1, :cond_2

    .line 136
    new-instance v5, Lorg/icepdf/core/pobjects/PRectangle;

    invoke-direct {v5, v1}, Lorg/icepdf/core/pobjects/PRectangle;-><init>(Ljava/util/Vector;)V

    iput-object v5, p0, Lorg/icepdf/core/pobjects/PageTree;->mediaBox:Lorg/icepdf/core/pobjects/PRectangle;

    .line 139
    :cond_2
    iget-object v5, p0, Lorg/icepdf/core/pobjects/PageTree;->library:Lorg/icepdf/core/util/Library;

    iget-object v6, p0, Lorg/icepdf/core/pobjects/PageTree;->entries:Ljava/util/Hashtable;

    const-string/jumbo v7, "CropBox"

    invoke-virtual {v5, v6, v7}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/Vector;

    move-object v0, v5

    check-cast v0, Ljava/util/Vector;

    move-object v1, v0

    .line 140
    if-eqz v1, :cond_3

    .line 141
    new-instance v5, Lorg/icepdf/core/pobjects/PRectangle;

    invoke-direct {v5, v1}, Lorg/icepdf/core/pobjects/PRectangle;-><init>(Ljava/util/Vector;)V

    iput-object v5, p0, Lorg/icepdf/core/pobjects/PageTree;->cropBox:Lorg/icepdf/core/pobjects/PRectangle;

    .line 144
    :cond_3
    iget-object v5, p0, Lorg/icepdf/core/pobjects/PageTree;->library:Lorg/icepdf/core/util/Library;

    iget-object v6, p0, Lorg/icepdf/core/pobjects/PageTree;->entries:Ljava/util/Hashtable;

    const-string/jumbo v7, "Kids"

    invoke-virtual {v5, v6, v7}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/Vector;

    iput-object v5, p0, Lorg/icepdf/core/pobjects/PageTree;->kidsReferences:Ljava/util/Vector;

    .line 145
    new-instance v5, Ljava/util/Vector;

    iget-object v6, p0, Lorg/icepdf/core/pobjects/PageTree;->kidsReferences:Ljava/util/Vector;

    invoke-virtual {v6}, Ljava/util/Vector;->size()I

    move-result v6

    invoke-direct {v5, v6}, Ljava/util/Vector;-><init>(I)V

    iput-object v5, p0, Lorg/icepdf/core/pobjects/PageTree;->kidsPageAndPages:Ljava/util/Vector;

    .line 146
    iget-object v5, p0, Lorg/icepdf/core/pobjects/PageTree;->kidsPageAndPages:Ljava/util/Vector;

    iget-object v6, p0, Lorg/icepdf/core/pobjects/PageTree;->kidsReferences:Ljava/util/Vector;

    invoke-virtual {v6}, Ljava/util/Vector;->size()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/util/Vector;->setSize(I)V

    .line 149
    iget-object v5, p0, Lorg/icepdf/core/pobjects/PageTree;->library:Lorg/icepdf/core/util/Library;

    iget-object v6, p0, Lorg/icepdf/core/pobjects/PageTree;->entries:Ljava/util/Hashtable;

    const-string/jumbo v7, "Rotate"

    invoke-virtual {v5, v6, v7}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    .line 150
    .local v4, "tmpRotation":Ljava/lang/Object;
    if-eqz v4, :cond_4

    .line 151
    check-cast v4, Ljava/lang/Number;

    .end local v4    # "tmpRotation":Ljava/lang/Object;
    invoke-virtual {v4}, Ljava/lang/Number;->floatValue()F

    move-result v5

    iput v5, p0, Lorg/icepdf/core/pobjects/PageTree;->rotationFactor:F

    .line 153
    const/4 v5, 0x1

    iput-boolean v5, p0, Lorg/icepdf/core/pobjects/PageTree;->isRotationFactor:Z

    .line 156
    :cond_4
    const/4 v5, 0x1

    iput-boolean v5, p0, Lorg/icepdf/core/pobjects/PageTree;->inited:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    .line 121
    .end local v1    # "boxDimensions":Ljava/util/Vector;
    .end local v2    # "nn":Ljava/lang/Number;
    .end local v3    # "parentTree":Ljava/lang/Object;
    :catchall_0
    move-exception v5

    monitor-exit p0

    throw v5

    .line 132
    .restart local v2    # "nn":Ljava/lang/Number;
    .restart local v3    # "parentTree":Ljava/lang/Object;
    :cond_5
    const/4 v5, 0x0

    :try_start_2
    iput v5, p0, Lorg/icepdf/core/pobjects/PageTree;->kidsCount:I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method

.method initRootPageTree()V
    .locals 0

    .prologue
    .line 161
    return-void
.end method

.method public releasePage(ILjava/lang/Object;)V
    .locals 2
    .param p1, "pageNumber"    # I
    .param p2, "user"    # Ljava/lang/Object;

    .prologue
    .line 417
    invoke-direct {p0, p1}, Lorg/icepdf/core/pobjects/PageTree;->getPagePotentiallyNotInitedByRecursiveIndex(I)Lorg/icepdf/core/pobjects/Page;

    move-result-object v0

    .line 420
    .local v0, "page":Lorg/icepdf/core/pobjects/Page;
    iget-object v1, p0, Lorg/icepdf/core/pobjects/PageTree;->library:Lorg/icepdf/core/util/Library;

    iget-object v1, v1, Lorg/icepdf/core/util/Library;->memoryManager:Lorg/icepdf/core/util/MemoryManager;

    invoke-virtual {v1, p2, v0}, Lorg/icepdf/core/util/MemoryManager;->release(Ljava/lang/Object;Lorg/icepdf/core/util/MemoryManageable;)V

    .line 421
    return-void
.end method

.method public releasePage(Lorg/icepdf/core/pobjects/Page;Ljava/lang/Object;)V
    .locals 1
    .param p1, "page"    # Lorg/icepdf/core/pobjects/Page;
    .param p2, "user"    # Ljava/lang/Object;

    .prologue
    .line 395
    iget-object v0, p0, Lorg/icepdf/core/pobjects/PageTree;->library:Lorg/icepdf/core/util/Library;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/icepdf/core/pobjects/PageTree;->library:Lorg/icepdf/core/util/Library;

    iget-object v0, v0, Lorg/icepdf/core/util/Library;->memoryManager:Lorg/icepdf/core/util/MemoryManager;

    if-eqz v0, :cond_0

    .line 396
    iget-object v0, p0, Lorg/icepdf/core/pobjects/PageTree;->library:Lorg/icepdf/core/util/Library;

    iget-object v0, v0, Lorg/icepdf/core/util/Library;->memoryManager:Lorg/icepdf/core/util/MemoryManager;

    invoke-virtual {v0, p2, p1}, Lorg/icepdf/core/util/MemoryManager;->release(Ljava/lang/Object;Lorg/icepdf/core/util/MemoryManageable;)V

    .line 397
    :cond_0
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 429
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "PAGES= "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/icepdf/core/pobjects/PageTree;->entries:Ljava/util/Hashtable;

    invoke-virtual {v1}, Ljava/util/Hashtable;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
