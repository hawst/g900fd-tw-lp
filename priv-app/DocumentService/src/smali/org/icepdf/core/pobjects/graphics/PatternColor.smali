.class public Lorg/icepdf/core/pobjects/graphics/PatternColor;
.super Lorg/icepdf/core/pobjects/graphics/PColorSpace;
.source "PatternColor.java"


# instance fields
.field private PColorSpace:Lorg/icepdf/core/pobjects/graphics/PColorSpace;

.field private pattern:Lorg/icepdf/core/pobjects/graphics/Pattern;


# direct methods
.method public constructor <init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V
    .locals 0
    .param p1, "library"    # Lorg/icepdf/core/util/Library;
    .param p2, "entries"    # Ljava/util/Hashtable;

    .prologue
    .line 48
    invoke-direct {p0, p1, p2}, Lorg/icepdf/core/pobjects/graphics/PColorSpace;-><init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V

    .line 49
    return-void
.end method


# virtual methods
.method public getColor([F)Lorg/apache/poi/java/awt/Color;
    .locals 1
    .param p1, "f"    # [F

    .prologue
    .line 70
    iget-object v0, p0, Lorg/icepdf/core/pobjects/graphics/PatternColor;->PColorSpace:Lorg/icepdf/core/pobjects/graphics/PColorSpace;

    if-eqz v0, :cond_0

    .line 71
    iget-object v0, p0, Lorg/icepdf/core/pobjects/graphics/PatternColor;->PColorSpace:Lorg/icepdf/core/pobjects/graphics/PColorSpace;

    invoke-virtual {v0, p1}, Lorg/icepdf/core/pobjects/graphics/PColorSpace;->getColor([F)Lorg/apache/poi/java/awt/Color;

    move-result-object v0

    .line 73
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lorg/apache/poi/java/awt/Color;->black:Lorg/apache/poi/java/awt/Color;

    goto :goto_0
.end method

.method public getNumComponents()I
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lorg/icepdf/core/pobjects/graphics/PatternColor;->PColorSpace:Lorg/icepdf/core/pobjects/graphics/PColorSpace;

    if-eqz v0, :cond_0

    .line 58
    iget-object v0, p0, Lorg/icepdf/core/pobjects/graphics/PatternColor;->PColorSpace:Lorg/icepdf/core/pobjects/graphics/PColorSpace;

    invoke-virtual {v0}, Lorg/icepdf/core/pobjects/graphics/PColorSpace;->getNumComponents()I

    move-result v0

    .line 60
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getPColorSpace()Lorg/icepdf/core/pobjects/graphics/PColorSpace;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lorg/icepdf/core/pobjects/graphics/PatternColor;->PColorSpace:Lorg/icepdf/core/pobjects/graphics/PColorSpace;

    return-object v0
.end method

.method public getPattern()Lorg/icepdf/core/pobjects/graphics/Pattern;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lorg/icepdf/core/pobjects/graphics/PatternColor;->pattern:Lorg/icepdf/core/pobjects/graphics/Pattern;

    return-object v0
.end method

.method public getPattern(Lorg/icepdf/core/pobjects/Reference;)Lorg/icepdf/core/pobjects/graphics/Pattern;
    .locals 1
    .param p1, "reference"    # Lorg/icepdf/core/pobjects/Reference;

    .prologue
    .line 77
    iget-object v0, p0, Lorg/icepdf/core/pobjects/graphics/PatternColor;->entries:Ljava/util/Hashtable;

    if-eqz v0, :cond_0

    .line 78
    iget-object v0, p0, Lorg/icepdf/core/pobjects/graphics/PatternColor;->entries:Ljava/util/Hashtable;

    invoke-virtual {v0, p1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/icepdf/core/pobjects/graphics/Pattern;

    .line 80
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setPColorSpace(Lorg/icepdf/core/pobjects/graphics/PColorSpace;)V
    .locals 0
    .param p1, "PColorSpace"    # Lorg/icepdf/core/pobjects/graphics/PColorSpace;

    .prologue
    .line 88
    iput-object p1, p0, Lorg/icepdf/core/pobjects/graphics/PatternColor;->PColorSpace:Lorg/icepdf/core/pobjects/graphics/PColorSpace;

    .line 89
    return-void
.end method

.method public setPattern(Lorg/icepdf/core/pobjects/graphics/Pattern;)V
    .locals 0
    .param p1, "pattern"    # Lorg/icepdf/core/pobjects/graphics/Pattern;

    .prologue
    .line 96
    iput-object p1, p0, Lorg/icepdf/core/pobjects/graphics/PatternColor;->pattern:Lorg/icepdf/core/pobjects/graphics/Pattern;

    .line 97
    return-void
.end method
