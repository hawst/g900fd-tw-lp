.class public Lorg/icepdf/core/pobjects/security/SecurityManager;
.super Ljava/lang/Object;
.source "SecurityManager.java"


# static fields
.field private static foundJCE:Z

.field private static final logger:Ljava/util/logging/Logger;


# instance fields
.field private encryptDictionary:Lorg/icepdf/core/pobjects/security/EncryptionDictionary;

.field private securityHandler:Lorg/icepdf/core/pobjects/security/SecurityHandler;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    .line 53
    const-class v4, Lorg/icepdf/core/pobjects/security/SecurityManager;

    invoke-virtual {v4}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v4

    sput-object v4, Lorg/icepdf/core/pobjects/security/SecurityManager;->logger:Ljava/util/logging/Logger;

    .line 64
    const/4 v4, 0x0

    sput-boolean v4, Lorg/icepdf/core/pobjects/security/SecurityManager;->foundJCE:Z

    .line 69
    const-string/jumbo v1, "org.bouncycastle.jce.provider.BouncyCastleProvider"

    .line 73
    .local v1, "defaultSecurityProvider":Ljava/lang/String;
    const-string/jumbo v4, "org.icepdf.core.security.jceProvider"

    invoke-static {v4}, Lorg/icepdf/core/util/Defs;->sysProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 77
    .local v0, "customSecurityProvider":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 78
    move-object v1, v0

    .line 82
    :cond_0
    :try_start_0
    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v3

    .line 83
    .local v3, "provider":Ljava/lang/Object;
    check-cast v3, Ljava/security/Provider;

    .end local v3    # "provider":Ljava/lang/Object;
    const/4 v4, 0x2

    invoke-static {v3, v4}, Ljava/security/Security;->insertProviderAt(Ljava/security/Provider;I)I
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2

    .line 95
    :goto_0
    :try_start_1
    const-string/jumbo v4, "javax.crypto.Cipher"

    invoke-static {v4}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    .line 96
    const/4 v4, 0x1

    sput-boolean v4, Lorg/icepdf/core/pobjects/security/SecurityManager;->foundJCE:Z
    :try_end_1
    .catch Ljava/lang/ClassNotFoundException; {:try_start_1 .. :try_end_1} :catch_3

    .line 100
    :goto_1
    return-void

    .line 84
    :catch_0
    move-exception v2

    .line 85
    .local v2, "e":Ljava/lang/ClassNotFoundException;
    sget-object v4, Lorg/icepdf/core/pobjects/security/SecurityManager;->logger:Ljava/util/logging/Logger;

    sget-object v5, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string/jumbo v6, "Optional BouncyCastle security provider not found"

    invoke-virtual {v4, v5, v6}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    goto :goto_0

    .line 87
    .end local v2    # "e":Ljava/lang/ClassNotFoundException;
    :catch_1
    move-exception v2

    .line 88
    .local v2, "e":Ljava/lang/InstantiationException;
    sget-object v4, Lorg/icepdf/core/pobjects/security/SecurityManager;->logger:Ljava/util/logging/Logger;

    sget-object v5, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string/jumbo v6, "Optional BouncyCastle security provider could not be instantiated"

    invoke-virtual {v4, v5, v6}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    goto :goto_0

    .line 90
    .end local v2    # "e":Ljava/lang/InstantiationException;
    :catch_2
    move-exception v2

    .line 91
    .local v2, "e":Ljava/lang/IllegalAccessException;
    sget-object v4, Lorg/icepdf/core/pobjects/security/SecurityManager;->logger:Ljava/util/logging/Logger;

    sget-object v5, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string/jumbo v6, "Optional BouncyCastle security provider could not be created"

    invoke-virtual {v4, v5, v6}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    goto :goto_0

    .line 97
    .end local v2    # "e":Ljava/lang/IllegalAccessException;
    :catch_3
    move-exception v2

    .line 98
    .local v2, "e":Ljava/lang/ClassNotFoundException;
    sget-object v4, Lorg/icepdf/core/pobjects/security/SecurityManager;->logger:Ljava/util/logging/Logger;

    sget-object v5, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    const-string/jumbo v6, "Sun JCE Support Not Found"

    invoke-virtual {v4, v5, v6}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public constructor <init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;Ljava/util/Vector;)V
    .locals 3
    .param p1, "library"    # Lorg/icepdf/core/util/Library;
    .param p2, "encryptionDictionary"    # Ljava/util/Hashtable;
    .param p3, "fileID"    # Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/icepdf/core/exceptions/PDFSecurityException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 119
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    iput-object v0, p0, Lorg/icepdf/core/pobjects/security/SecurityManager;->encryptDictionary:Lorg/icepdf/core/pobjects/security/EncryptionDictionary;

    .line 61
    iput-object v0, p0, Lorg/icepdf/core/pobjects/security/SecurityManager;->securityHandler:Lorg/icepdf/core/pobjects/security/SecurityHandler;

    .line 123
    sget-boolean v0, Lorg/icepdf/core/pobjects/security/SecurityManager;->foundJCE:Z

    if-nez v0, :cond_0

    .line 124
    sget-object v0, Lorg/icepdf/core/pobjects/security/SecurityManager;->logger:Ljava/util/logging/Logger;

    sget-object v1, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    const-string/jumbo v2, "Sun JCE support was not found on classpath"

    invoke-virtual {v0, v1, v2}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    .line 125
    new-instance v0, Lorg/icepdf/core/exceptions/PDFSecurityException;

    const-string/jumbo v1, "Sun JCE Support Not Found"

    invoke-direct {v0, v1}, Lorg/icepdf/core/exceptions/PDFSecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 129
    :cond_0
    new-instance v0, Lorg/icepdf/core/pobjects/security/EncryptionDictionary;

    invoke-direct {v0, p1, p2, p3}, Lorg/icepdf/core/pobjects/security/EncryptionDictionary;-><init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;Ljava/util/Vector;)V

    iput-object v0, p0, Lorg/icepdf/core/pobjects/security/SecurityManager;->encryptDictionary:Lorg/icepdf/core/pobjects/security/EncryptionDictionary;

    .line 133
    iget-object v0, p0, Lorg/icepdf/core/pobjects/security/SecurityManager;->encryptDictionary:Lorg/icepdf/core/pobjects/security/EncryptionDictionary;

    invoke-virtual {v0}, Lorg/icepdf/core/pobjects/security/EncryptionDictionary;->getPreferredSecurityHandlerName()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "Standard"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 135
    new-instance v0, Lorg/icepdf/core/pobjects/security/StandardSecurityHandler;

    iget-object v1, p0, Lorg/icepdf/core/pobjects/security/SecurityManager;->encryptDictionary:Lorg/icepdf/core/pobjects/security/EncryptionDictionary;

    invoke-direct {v0, v1}, Lorg/icepdf/core/pobjects/security/StandardSecurityHandler;-><init>(Lorg/icepdf/core/pobjects/security/EncryptionDictionary;)V

    iput-object v0, p0, Lorg/icepdf/core/pobjects/security/SecurityManager;->securityHandler:Lorg/icepdf/core/pobjects/security/SecurityHandler;

    .line 137
    iget-object v0, p0, Lorg/icepdf/core/pobjects/security/SecurityManager;->securityHandler:Lorg/icepdf/core/pobjects/security/SecurityHandler;

    invoke-virtual {v0}, Lorg/icepdf/core/pobjects/security/SecurityHandler;->init()V

    .line 141
    return-void

    .line 139
    :cond_1
    new-instance v0, Lorg/icepdf/core/exceptions/PDFSecurityException;

    const-string/jumbo v1, "Security Provider Not Found."

    invoke-direct {v0, v1}, Lorg/icepdf/core/exceptions/PDFSecurityException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public decrypt(Lorg/icepdf/core/pobjects/Reference;[B[B)[B
    .locals 1
    .param p1, "objectReference"    # Lorg/icepdf/core/pobjects/Reference;
    .param p2, "encryptionKey"    # [B
    .param p3, "data"    # [B

    .prologue
    .line 218
    iget-object v0, p0, Lorg/icepdf/core/pobjects/security/SecurityManager;->securityHandler:Lorg/icepdf/core/pobjects/security/SecurityHandler;

    invoke-virtual {v0, p1, p2, p3}, Lorg/icepdf/core/pobjects/security/SecurityHandler;->decrypt(Lorg/icepdf/core/pobjects/Reference;[B[B)[B

    move-result-object v0

    return-object v0
.end method

.method public dispose()V
    .locals 0

    .prologue
    .line 107
    return-void
.end method

.method public encrypt(Lorg/icepdf/core/pobjects/Reference;[B[B)[B
    .locals 1
    .param p1, "objectReference"    # Lorg/icepdf/core/pobjects/Reference;
    .param p2, "encryptionKey"    # [B
    .param p3, "data"    # [B

    .prologue
    .line 202
    iget-object v0, p0, Lorg/icepdf/core/pobjects/security/SecurityManager;->securityHandler:Lorg/icepdf/core/pobjects/security/SecurityHandler;

    invoke-virtual {v0, p1, p2, p3}, Lorg/icepdf/core/pobjects/security/SecurityHandler;->encrypt(Lorg/icepdf/core/pobjects/Reference;[B[B)[B

    move-result-object v0

    return-object v0
.end method

.method public getDecryptionKey()[B
    .locals 1

    .prologue
    .line 186
    iget-object v0, p0, Lorg/icepdf/core/pobjects/security/SecurityManager;->securityHandler:Lorg/icepdf/core/pobjects/security/SecurityHandler;

    invoke-virtual {v0}, Lorg/icepdf/core/pobjects/security/SecurityHandler;->getDecryptionKey()[B

    move-result-object v0

    return-object v0
.end method

.method public getEncryptionDictionary()Lorg/icepdf/core/pobjects/security/EncryptionDictionary;
    .locals 1

    .prologue
    .line 168
    iget-object v0, p0, Lorg/icepdf/core/pobjects/security/SecurityManager;->encryptDictionary:Lorg/icepdf/core/pobjects/security/EncryptionDictionary;

    return-object v0
.end method

.method public getEncryptionInputStream(Lorg/icepdf/core/pobjects/Reference;[BLjava/util/Hashtable;Ljava/io/InputStream;Z)Ljava/io/InputStream;
    .locals 2
    .param p1, "objectReference"    # Lorg/icepdf/core/pobjects/Reference;
    .param p2, "encryptionKey"    # [B
    .param p3, "decodeParams"    # Ljava/util/Hashtable;
    .param p4, "input"    # Ljava/io/InputStream;
    .param p5, "returnInputIfNullResult"    # Z

    .prologue
    .line 239
    iget-object v1, p0, Lorg/icepdf/core/pobjects/security/SecurityManager;->securityHandler:Lorg/icepdf/core/pobjects/security/SecurityHandler;

    invoke-virtual {v1, p1, p2, p3, p4}, Lorg/icepdf/core/pobjects/security/SecurityHandler;->getEncryptionInputStream(Lorg/icepdf/core/pobjects/Reference;[BLjava/util/Hashtable;Ljava/io/InputStream;)Ljava/io/InputStream;

    move-result-object v0

    .line 241
    .local v0, "result":Ljava/io/InputStream;
    if-eqz p5, :cond_0

    if-nez v0, :cond_0

    .line 242
    move-object v0, p4

    .line 243
    :cond_0
    return-object v0
.end method

.method public getEncryptionKey()[B
    .locals 1

    .prologue
    .line 177
    iget-object v0, p0, Lorg/icepdf/core/pobjects/security/SecurityManager;->securityHandler:Lorg/icepdf/core/pobjects/security/SecurityHandler;

    invoke-virtual {v0}, Lorg/icepdf/core/pobjects/security/SecurityHandler;->getEncryptionKey()[B

    move-result-object v0

    return-object v0
.end method

.method public getPermissions()Lorg/icepdf/core/pobjects/security/Permissions;
    .locals 1

    .prologue
    .line 149
    iget-object v0, p0, Lorg/icepdf/core/pobjects/security/SecurityManager;->securityHandler:Lorg/icepdf/core/pobjects/security/SecurityHandler;

    invoke-virtual {v0}, Lorg/icepdf/core/pobjects/security/SecurityHandler;->getPermissions()Lorg/icepdf/core/pobjects/security/Permissions;

    move-result-object v0

    return-object v0
.end method

.method public getSecurityHandler()Lorg/icepdf/core/pobjects/security/SecurityHandler;
    .locals 1

    .prologue
    .line 158
    iget-object v0, p0, Lorg/icepdf/core/pobjects/security/SecurityManager;->securityHandler:Lorg/icepdf/core/pobjects/security/SecurityHandler;

    return-object v0
.end method

.method public isAuthorized(Ljava/lang/String;)Z
    .locals 1
    .param p1, "password"    # Ljava/lang/String;

    .prologue
    .line 255
    iget-object v0, p0, Lorg/icepdf/core/pobjects/security/SecurityManager;->securityHandler:Lorg/icepdf/core/pobjects/security/SecurityHandler;

    invoke-virtual {v0, p1}, Lorg/icepdf/core/pobjects/security/SecurityHandler;->isAuthorized(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method
