.class public Lorg/icepdf/core/pobjects/security/CryptFilter;
.super Lorg/icepdf/core/pobjects/Dictionary;
.source "CryptFilter.java"


# instance fields
.field public cryptFilters:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lorg/icepdf/core/pobjects/Name;",
            "Lorg/icepdf/core/pobjects/security/CryptFilterEntry;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V
    .locals 0
    .param p1, "library"    # Lorg/icepdf/core/util/Library;
    .param p2, "entries"    # Ljava/util/Hashtable;

    .prologue
    .line 65
    invoke-direct {p0, p1, p2}, Lorg/icepdf/core/pobjects/Dictionary;-><init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V

    .line 66
    return-void
.end method


# virtual methods
.method public getCryptFilterByName(Lorg/icepdf/core/pobjects/Name;)Lorg/icepdf/core/pobjects/security/CryptFilterEntry;
    .locals 8
    .param p1, "cryptFilterName"    # Lorg/icepdf/core/pobjects/Name;

    .prologue
    .line 77
    iget-object v3, p0, Lorg/icepdf/core/pobjects/security/CryptFilter;->cryptFilters:Ljava/util/HashMap;

    if-nez v3, :cond_0

    .line 78
    new-instance v3, Ljava/util/HashMap;

    const/4 v4, 0x1

    invoke-direct {v3, v4}, Ljava/util/HashMap;-><init>(I)V

    iput-object v3, p0, Lorg/icepdf/core/pobjects/security/CryptFilter;->cryptFilters:Ljava/util/HashMap;

    .line 79
    iget-object v3, p0, Lorg/icepdf/core/pobjects/security/CryptFilter;->entries:Ljava/util/Hashtable;

    invoke-virtual {v3}, Ljava/util/Hashtable;->keySet()Ljava/util/Set;

    move-result-object v0

    .line 80
    .local v0, "cryptKeys":Ljava/util/Set;
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 81
    .local v2, "name":Ljava/lang/Object;
    iget-object v5, p0, Lorg/icepdf/core/pobjects/security/CryptFilter;->cryptFilters:Ljava/util/HashMap;

    move-object v3, v2

    check-cast v3, Lorg/icepdf/core/pobjects/Name;

    new-instance v6, Lorg/icepdf/core/pobjects/security/CryptFilterEntry;

    iget-object v7, p0, Lorg/icepdf/core/pobjects/security/CryptFilter;->library:Lorg/icepdf/core/util/Library;

    iget-object v4, p0, Lorg/icepdf/core/pobjects/security/CryptFilter;->entries:Ljava/util/Hashtable;

    invoke-virtual {v4, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Hashtable;

    invoke-direct {v6, v7, v4}, Lorg/icepdf/core/pobjects/security/CryptFilterEntry;-><init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V

    invoke-virtual {v5, v3, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 85
    .end local v0    # "cryptKeys":Ljava/util/Set;
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "name":Ljava/lang/Object;
    :cond_0
    iget-object v3, p0, Lorg/icepdf/core/pobjects/security/CryptFilter;->cryptFilters:Ljava/util/HashMap;

    invoke-virtual {v3, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/icepdf/core/pobjects/security/CryptFilterEntry;

    return-object v3
.end method
