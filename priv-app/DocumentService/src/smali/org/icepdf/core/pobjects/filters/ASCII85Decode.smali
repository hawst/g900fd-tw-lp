.class public Lorg/icepdf/core/pobjects/filters/ASCII85Decode;
.super Lorg/icepdf/core/pobjects/filters/ChunkingInputStream;
.source "ASCII85Decode.java"


# instance fields
.field private eof:Z


# direct methods
.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 1
    .param p1, "input"    # Ljava/io/InputStream;

    .prologue
    .line 29
    invoke-direct {p0}, Lorg/icepdf/core/pobjects/filters/ChunkingInputStream;-><init>()V

    .line 26
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/icepdf/core/pobjects/filters/ASCII85Decode;->eof:Z

    .line 31
    invoke-virtual {p0, p1}, Lorg/icepdf/core/pobjects/filters/ASCII85Decode;->setInputStream(Ljava/io/InputStream;)V

    .line 32
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lorg/icepdf/core/pobjects/filters/ASCII85Decode;->setBufferSize(I)V

    .line 33
    return-void
.end method


# virtual methods
.method protected fillInternalBuffer()I
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 36
    iget-boolean v3, p0, Lorg/icepdf/core/pobjects/filters/ASCII85Decode;->eof:Z

    if-eqz v3, :cond_0

    .line 37
    const/4 v3, -0x1

    .line 86
    :goto_0
    return v3

    .line 38
    :cond_0
    const-wide/16 v4, 0x0

    .line 39
    .local v4, "value":J
    const/4 v2, 0x0

    .line 40
    .local v2, "count":I
    const-wide/16 v0, 0x0

    .line 42
    .local v0, "c":J
    :cond_1
    iget-object v3, p0, Lorg/icepdf/core/pobjects/filters/ASCII85Decode;->in:Ljava/io/InputStream;

    invoke-virtual {v3}, Ljava/io/InputStream;->read()I

    move-result v3

    int-to-long v0, v3

    .line 43
    const-wide/16 v6, 0x0

    cmp-long v3, v0, v6

    if-gez v3, :cond_6

    .line 44
    const/4 v3, 0x1

    iput-boolean v3, p0, Lorg/icepdf/core/pobjects/filters/ASCII85Decode;->eof:Z

    .line 73
    :goto_1
    const/4 v3, 0x2

    if-ne v2, v3, :cond_9

    .line 74
    const-wide/32 v6, 0x95eed

    mul-long/2addr v6, v4

    const-wide/32 v8, 0xffffff

    add-long v4, v6, v8

    .line 80
    :cond_2
    :goto_2
    const/4 v3, 0x2

    if-lt v2, v3, :cond_3

    .line 81
    iget-object v3, p0, Lorg/icepdf/core/pobjects/filters/ASCII85Decode;->buffer:[B

    const/4 v6, 0x0

    const/16 v7, 0x18

    shr-long v8, v4, v7

    const-wide/16 v10, 0xff

    and-long/2addr v8, v10

    long-to-int v7, v8

    int-to-byte v7, v7

    aput-byte v7, v3, v6

    .line 82
    :cond_3
    const/4 v3, 0x3

    if-lt v2, v3, :cond_4

    .line 83
    iget-object v3, p0, Lorg/icepdf/core/pobjects/filters/ASCII85Decode;->buffer:[B

    const/4 v6, 0x1

    const/16 v7, 0x10

    shr-long v8, v4, v7

    const-wide/16 v10, 0xff

    and-long/2addr v8, v10

    long-to-int v7, v8

    int-to-byte v7, v7

    aput-byte v7, v3, v6

    .line 84
    :cond_4
    const/4 v3, 0x4

    if-lt v2, v3, :cond_5

    .line 85
    iget-object v3, p0, Lorg/icepdf/core/pobjects/filters/ASCII85Decode;->buffer:[B

    const/4 v6, 0x2

    const/16 v7, 0x8

    shr-long v8, v4, v7

    const-wide/16 v10, 0xff

    and-long/2addr v8, v10

    long-to-int v7, v8

    int-to-byte v7, v7

    aput-byte v7, v3, v6

    .line 86
    :cond_5
    add-int/lit8 v3, v2, -0x1

    goto :goto_0

    .line 47
    :cond_6
    const-wide/16 v6, 0x0

    cmp-long v3, v0, v6

    if-eqz v3, :cond_1

    const-wide/16 v6, 0x9

    cmp-long v3, v0, v6

    if-eqz v3, :cond_1

    const-wide/16 v6, 0xa

    cmp-long v3, v0, v6

    if-eqz v3, :cond_1

    const-wide/16 v6, 0xc

    cmp-long v3, v0, v6

    if-eqz v3, :cond_1

    const-wide/16 v6, 0xd

    cmp-long v3, v0, v6

    if-eqz v3, :cond_1

    const-wide/16 v6, 0x20

    cmp-long v3, v0, v6

    if-eqz v3, :cond_1

    .line 49
    const-wide/16 v6, 0x7e

    cmp-long v3, v0, v6

    if-nez v3, :cond_7

    .line 50
    const/4 v3, 0x1

    iput-boolean v3, p0, Lorg/icepdf/core/pobjects/filters/ASCII85Decode;->eof:Z

    goto :goto_1

    .line 53
    :cond_7
    const-wide/16 v6, 0x7a

    cmp-long v3, v0, v6

    if-nez v3, :cond_8

    .line 54
    iget-object v3, p0, Lorg/icepdf/core/pobjects/filters/ASCII85Decode;->buffer:[B

    const/4 v6, 0x0

    const/4 v7, 0x0

    aput-byte v7, v3, v6

    .line 55
    iget-object v3, p0, Lorg/icepdf/core/pobjects/filters/ASCII85Decode;->buffer:[B

    const/4 v6, 0x1

    const/4 v7, 0x0

    aput-byte v7, v3, v6

    .line 56
    iget-object v3, p0, Lorg/icepdf/core/pobjects/filters/ASCII85Decode;->buffer:[B

    const/4 v6, 0x2

    const/4 v7, 0x0

    aput-byte v7, v3, v6

    .line 57
    iget-object v3, p0, Lorg/icepdf/core/pobjects/filters/ASCII85Decode;->buffer:[B

    const/4 v6, 0x3

    const/4 v7, 0x0

    aput-byte v7, v3, v6

    .line 58
    const/4 v2, 0x0

    .line 59
    const/4 v3, 0x4

    goto/16 :goto_0

    .line 61
    :cond_8
    add-int/lit8 v2, v2, 0x1

    .line 62
    const-wide/16 v6, 0x55

    mul-long/2addr v6, v4

    const-wide/16 v8, 0x21

    sub-long v8, v0, v8

    add-long v4, v6, v8

    .line 63
    const/4 v3, 0x5

    if-ne v2, v3, :cond_1

    .line 64
    iget-object v3, p0, Lorg/icepdf/core/pobjects/filters/ASCII85Decode;->buffer:[B

    const/4 v6, 0x0

    const/16 v7, 0x18

    shr-long v8, v4, v7

    const-wide/16 v10, 0xff

    and-long/2addr v8, v10

    long-to-int v7, v8

    int-to-byte v7, v7

    aput-byte v7, v3, v6

    .line 65
    iget-object v3, p0, Lorg/icepdf/core/pobjects/filters/ASCII85Decode;->buffer:[B

    const/4 v6, 0x1

    const/16 v7, 0x10

    shr-long v8, v4, v7

    const-wide/16 v10, 0xff

    and-long/2addr v8, v10

    long-to-int v7, v8

    int-to-byte v7, v7

    aput-byte v7, v3, v6

    .line 66
    iget-object v3, p0, Lorg/icepdf/core/pobjects/filters/ASCII85Decode;->buffer:[B

    const/4 v6, 0x2

    const/16 v7, 0x8

    shr-long v8, v4, v7

    const-wide/16 v10, 0xff

    and-long/2addr v8, v10

    long-to-int v7, v8

    int-to-byte v7, v7

    aput-byte v7, v3, v6

    .line 67
    iget-object v3, p0, Lorg/icepdf/core/pobjects/filters/ASCII85Decode;->buffer:[B

    const/4 v6, 0x3

    const-wide/16 v8, 0xff

    and-long/2addr v8, v4

    long-to-int v7, v8

    int-to-byte v7, v7

    aput-byte v7, v3, v6

    .line 68
    const-wide/16 v4, 0x0

    .line 69
    const/4 v2, 0x0

    .line 70
    const/4 v3, 0x4

    goto/16 :goto_0

    .line 75
    :cond_9
    const/4 v3, 0x3

    if-ne v2, v3, :cond_a

    .line 76
    const-wide/16 v6, 0x1c39

    mul-long/2addr v6, v4

    const-wide/32 v8, 0xffff

    add-long v4, v6, v8

    goto/16 :goto_2

    .line 77
    :cond_a
    const/4 v3, 0x4

    if-ne v2, v3, :cond_2

    .line 78
    const-wide/16 v6, 0x55

    mul-long/2addr v6, v4

    const-wide/16 v8, 0xff

    add-long v4, v6, v8

    goto/16 :goto_2
.end method
