.class public Lorg/icepdf/core/pobjects/Thumbnail;
.super Lorg/icepdf/core/pobjects/Dictionary;
.source "Thumbnail.java"


# instance fields
.field private dimension:Lorg/apache/poi/java/awt/Dimension;

.field private image:Landroid/graphics/Bitmap;

.field private initialized:Z

.field private thumbStream:Lorg/icepdf/core/pobjects/Stream;


# direct methods
.method public constructor <init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V
    .locals 5
    .param p1, "library"    # Lorg/icepdf/core/util/Library;
    .param p2, "entries"    # Ljava/util/Hashtable;

    .prologue
    .line 46
    invoke-direct {p0, p1, p2}, Lorg/icepdf/core/pobjects/Dictionary;-><init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V

    .line 47
    const-string/jumbo v3, "Thumb"

    invoke-virtual {p1, p2, v3}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    .line 48
    .local v1, "thumb":Ljava/lang/Object;
    if-eqz v1, :cond_0

    instance-of v3, v1, Lorg/icepdf/core/pobjects/Stream;

    if-eqz v3, :cond_0

    .line 50
    check-cast v1, Lorg/icepdf/core/pobjects/Stream;

    .end local v1    # "thumb":Ljava/lang/Object;
    iput-object v1, p0, Lorg/icepdf/core/pobjects/Thumbnail;->thumbStream:Lorg/icepdf/core/pobjects/Stream;

    .line 53
    iget-object v3, p0, Lorg/icepdf/core/pobjects/Thumbnail;->thumbStream:Lorg/icepdf/core/pobjects/Stream;

    iget-object v3, v3, Lorg/icepdf/core/pobjects/Stream;->entries:Ljava/util/Hashtable;

    const-string/jumbo v4, "Width"

    invoke-virtual {p1, v3, v4}, Lorg/icepdf/core/util/Library;->getInt(Ljava/util/Hashtable;Ljava/lang/String;)I

    move-result v2

    .line 54
    .local v2, "width":I
    iget-object v3, p0, Lorg/icepdf/core/pobjects/Thumbnail;->thumbStream:Lorg/icepdf/core/pobjects/Stream;

    iget-object v3, v3, Lorg/icepdf/core/pobjects/Stream;->entries:Ljava/util/Hashtable;

    const-string/jumbo v4, "Height"

    invoke-virtual {p1, v3, v4}, Lorg/icepdf/core/util/Library;->getInt(Ljava/util/Hashtable;Ljava/lang/String;)I

    move-result v0

    .line 55
    .local v0, "height":I
    new-instance v3, Lorg/apache/poi/java/awt/Dimension;

    invoke-direct {v3, v2, v0}, Lorg/apache/poi/java/awt/Dimension;-><init>(II)V

    iput-object v3, p0, Lorg/icepdf/core/pobjects/Thumbnail;->dimension:Lorg/apache/poi/java/awt/Dimension;

    .line 58
    .end local v0    # "height":I
    .end local v2    # "width":I
    :cond_0
    return-void
.end method


# virtual methods
.method public getDimension()Lorg/apache/poi/java/awt/Dimension;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lorg/icepdf/core/pobjects/Thumbnail;->dimension:Lorg/apache/poi/java/awt/Dimension;

    return-object v0
.end method

.method public getImage()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 68
    iget-boolean v0, p0, Lorg/icepdf/core/pobjects/Thumbnail;->initialized:Z

    if-nez v0, :cond_0

    .line 69
    invoke-virtual {p0}, Lorg/icepdf/core/pobjects/Thumbnail;->init()V

    .line 71
    :cond_0
    iget-object v0, p0, Lorg/icepdf/core/pobjects/Thumbnail;->image:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public init()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 62
    new-instance v0, Lorg/icepdf/core/pobjects/Resources;

    iget-object v1, p0, Lorg/icepdf/core/pobjects/Thumbnail;->library:Lorg/icepdf/core/util/Library;

    iget-object v2, p0, Lorg/icepdf/core/pobjects/Thumbnail;->thumbStream:Lorg/icepdf/core/pobjects/Stream;

    iget-object v2, v2, Lorg/icepdf/core/pobjects/Stream;->entries:Ljava/util/Hashtable;

    invoke-direct {v0, v1, v2}, Lorg/icepdf/core/pobjects/Resources;-><init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V

    .line 63
    .local v0, "resource":Lorg/icepdf/core/pobjects/Resources;
    iget-object v1, p0, Lorg/icepdf/core/pobjects/Thumbnail;->thumbStream:Lorg/icepdf/core/pobjects/Stream;

    const/4 v2, 0x0

    invoke-virtual {v1, v2, v0, v3, v3}, Lorg/icepdf/core/pobjects/Stream;->getImage(Lorg/apache/poi/java/awt/Color;Lorg/icepdf/core/pobjects/Resources;ZZ)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lorg/icepdf/core/pobjects/Thumbnail;->image:Landroid/graphics/Bitmap;

    .line 64
    const/4 v1, 0x1

    iput-boolean v1, p0, Lorg/icepdf/core/pobjects/Thumbnail;->initialized:Z

    .line 65
    return-void
.end method
