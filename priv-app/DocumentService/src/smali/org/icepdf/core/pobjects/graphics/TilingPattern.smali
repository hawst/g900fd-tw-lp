.class public Lorg/icepdf/core/pobjects/graphics/TilingPattern;
.super Lorg/icepdf/core/pobjects/Stream;
.source "TilingPattern.java"

# interfaces
.implements Lorg/icepdf/core/pobjects/graphics/Pattern;


# static fields
.field public static final PAINTING_TYPE_COLORED_TILING_PATTERN:I = 0x1

.field public static final PAINTING_TYPE_UNCOLORED_TILING_PATTERN:I = 0x2

.field public static final TILING_TYPE_CONSTANT_SPACING:I = 0x1

.field public static final TILING_TYPE_CONSTANT_SPACING_FASTER:I = 0x3

.field public static final TILING_TYPE_NO_DISTORTION:I = 0x2

.field private static final logger:Ljava/util/logging/Logger;


# instance fields
.field private bBox:Landroid/graphics/RectF;

.field private bBoxMod:Landroid/graphics/RectF;

.field public fillColour:Lorg/apache/poi/java/awt/Color;

.field private inited:Z

.field private matrix:Landroid/graphics/Matrix;

.field private paintType:I

.field private parentGraphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

.field private patternPaint:Landroid/graphics/Paint;

.field private patternType:I

.field private resources:Lorg/icepdf/core/pobjects/Resources;

.field private shapes:Lorg/icepdf/core/pobjects/graphics/Shapes;

.field private tilingType:I

.field private type:Ljava/lang/String;

.field private unColored:Lorg/apache/poi/java/awt/Color;

.field private xStep:F

.field private yStep:F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 53
    const-class v0, Lorg/icepdf/core/pobjects/graphics/TilingPattern;

    invoke-virtual {v0}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Lorg/icepdf/core/pobjects/graphics/TilingPattern;->logger:Ljava/util/logging/Logger;

    return-void
.end method

.method public constructor <init>(Lorg/icepdf/core/pobjects/Stream;)V
    .locals 3
    .param p1, "stream"    # Lorg/icepdf/core/pobjects/Stream;

    .prologue
    .line 160
    invoke-virtual {p1}, Lorg/icepdf/core/pobjects/Stream;->getLibrary()Lorg/icepdf/core/util/Library;

    move-result-object v0

    invoke-virtual {p1}, Lorg/icepdf/core/pobjects/Stream;->getEntries()Ljava/util/Hashtable;

    move-result-object v1

    invoke-virtual {p1}, Lorg/icepdf/core/pobjects/Stream;->getStreamInput()Lorg/icepdf/core/io/SeekableInputConstrainedWrapper;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, Lorg/icepdf/core/pobjects/graphics/TilingPattern;-><init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;Lorg/icepdf/core/io/SeekableInputConstrainedWrapper;)V

    .line 161
    return-void
.end method

.method public constructor <init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;Lorg/icepdf/core/io/SeekableInputConstrainedWrapper;)V
    .locals 4
    .param p1, "l"    # Lorg/icepdf/core/util/Library;
    .param p2, "h"    # Ljava/util/Hashtable;
    .param p3, "streamInputWrapper"    # Lorg/icepdf/core/io/SeekableInputConstrainedWrapper;

    .prologue
    .line 169
    invoke-direct {p0, p1, p2, p3}, Lorg/icepdf/core/pobjects/Stream;-><init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;Lorg/icepdf/core/io/SeekableInputConstrainedWrapper;)V

    .line 145
    const/4 v1, 0x0

    iput-object v1, p0, Lorg/icepdf/core/pobjects/graphics/TilingPattern;->fillColour:Lorg/apache/poi/java/awt/Color;

    .line 171
    iget-object v1, p0, Lorg/icepdf/core/pobjects/graphics/TilingPattern;->library:Lorg/icepdf/core/util/Library;

    iget-object v2, p0, Lorg/icepdf/core/pobjects/graphics/TilingPattern;->entries:Ljava/util/Hashtable;

    const-string/jumbo v3, "Type"

    invoke-virtual {v1, v2, v3}, Lorg/icepdf/core/util/Library;->getName(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lorg/icepdf/core/pobjects/graphics/TilingPattern;->type:Ljava/lang/String;

    .line 173
    iget-object v1, p0, Lorg/icepdf/core/pobjects/graphics/TilingPattern;->library:Lorg/icepdf/core/util/Library;

    iget-object v2, p0, Lorg/icepdf/core/pobjects/graphics/TilingPattern;->entries:Ljava/util/Hashtable;

    const-string/jumbo v3, "PatternType"

    invoke-virtual {v1, v2, v3}, Lorg/icepdf/core/util/Library;->getInt(Ljava/util/Hashtable;Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lorg/icepdf/core/pobjects/graphics/TilingPattern;->patternType:I

    .line 175
    iget-object v1, p0, Lorg/icepdf/core/pobjects/graphics/TilingPattern;->library:Lorg/icepdf/core/util/Library;

    iget-object v2, p0, Lorg/icepdf/core/pobjects/graphics/TilingPattern;->entries:Ljava/util/Hashtable;

    const-string/jumbo v3, "PaintType"

    invoke-virtual {v1, v2, v3}, Lorg/icepdf/core/util/Library;->getInt(Ljava/util/Hashtable;Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lorg/icepdf/core/pobjects/graphics/TilingPattern;->paintType:I

    .line 177
    iget-object v1, p0, Lorg/icepdf/core/pobjects/graphics/TilingPattern;->library:Lorg/icepdf/core/util/Library;

    iget-object v2, p0, Lorg/icepdf/core/pobjects/graphics/TilingPattern;->entries:Ljava/util/Hashtable;

    const-string/jumbo v3, "TilingType"

    invoke-virtual {v1, v2, v3}, Lorg/icepdf/core/util/Library;->getInt(Ljava/util/Hashtable;Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lorg/icepdf/core/pobjects/graphics/TilingPattern;->tilingType:I

    .line 179
    iget-object v1, p0, Lorg/icepdf/core/pobjects/graphics/TilingPattern;->library:Lorg/icepdf/core/util/Library;

    iget-object v2, p0, Lorg/icepdf/core/pobjects/graphics/TilingPattern;->entries:Ljava/util/Hashtable;

    const-string/jumbo v3, "BBox"

    invoke-virtual {v1, v2, v3}, Lorg/icepdf/core/util/Library;->getRectangle(Ljava/util/Hashtable;Ljava/lang/String;)Landroid/graphics/RectF;

    move-result-object v1

    iput-object v1, p0, Lorg/icepdf/core/pobjects/graphics/TilingPattern;->bBox:Landroid/graphics/RectF;

    .line 181
    iget-object v1, p0, Lorg/icepdf/core/pobjects/graphics/TilingPattern;->library:Lorg/icepdf/core/util/Library;

    iget-object v2, p0, Lorg/icepdf/core/pobjects/graphics/TilingPattern;->entries:Ljava/util/Hashtable;

    const-string/jumbo v3, "XStep"

    invoke-virtual {v1, v2, v3}, Lorg/icepdf/core/util/Library;->getFloat(Ljava/util/Hashtable;Ljava/lang/String;)F

    move-result v1

    iput v1, p0, Lorg/icepdf/core/pobjects/graphics/TilingPattern;->xStep:F

    .line 183
    iget-object v1, p0, Lorg/icepdf/core/pobjects/graphics/TilingPattern;->library:Lorg/icepdf/core/util/Library;

    iget-object v2, p0, Lorg/icepdf/core/pobjects/graphics/TilingPattern;->entries:Ljava/util/Hashtable;

    const-string/jumbo v3, "YStep"

    invoke-virtual {v1, v2, v3}, Lorg/icepdf/core/util/Library;->getFloat(Ljava/util/Hashtable;Ljava/lang/String;)F

    move-result v1

    iput v1, p0, Lorg/icepdf/core/pobjects/graphics/TilingPattern;->yStep:F

    .line 185
    iget-object v1, p0, Lorg/icepdf/core/pobjects/graphics/TilingPattern;->library:Lorg/icepdf/core/util/Library;

    iget-object v2, p0, Lorg/icepdf/core/pobjects/graphics/TilingPattern;->entries:Ljava/util/Hashtable;

    const-string/jumbo v3, "Resources"

    invoke-virtual {v1, v2, v3}, Lorg/icepdf/core/util/Library;->getResources(Ljava/util/Hashtable;Ljava/lang/String;)Lorg/icepdf/core/pobjects/Resources;

    move-result-object v1

    iput-object v1, p0, Lorg/icepdf/core/pobjects/graphics/TilingPattern;->resources:Lorg/icepdf/core/pobjects/Resources;

    .line 187
    iget-object v1, p0, Lorg/icepdf/core/pobjects/graphics/TilingPattern;->library:Lorg/icepdf/core/util/Library;

    iget-object v2, p0, Lorg/icepdf/core/pobjects/graphics/TilingPattern;->entries:Ljava/util/Hashtable;

    const-string/jumbo v3, "Matrix"

    invoke-virtual {v1, v2, v3}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Vector;

    .line 188
    .local v0, "v":Ljava/util/Vector;
    if-eqz v0, :cond_0

    .line 189
    invoke-static {v0}, Lorg/icepdf/core/pobjects/graphics/TilingPattern;->getAffineTransform(Ljava/util/Vector;)Landroid/graphics/Matrix;

    move-result-object v1

    iput-object v1, p0, Lorg/icepdf/core/pobjects/graphics/TilingPattern;->matrix:Landroid/graphics/Matrix;

    .line 195
    :goto_0
    return-void

    .line 192
    :cond_0
    new-instance v1, Landroid/graphics/Matrix;

    invoke-direct {v1}, Landroid/graphics/Matrix;-><init>()V

    iput-object v1, p0, Lorg/icepdf/core/pobjects/graphics/TilingPattern;->matrix:Landroid/graphics/Matrix;

    goto :goto_0
.end method

.method private static getAffineTransform(Ljava/util/Vector;)Landroid/graphics/Matrix;
    .locals 5
    .param p0, "v"    # Ljava/util/Vector;

    .prologue
    const/4 v4, 0x6

    .line 210
    new-array v0, v4, [F

    .line 211
    .local v0, "f":[F
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v4, :cond_0

    .line 212
    invoke-virtual {p0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Number;

    invoke-virtual {v3}, Ljava/lang/Number;->floatValue()F

    move-result v3

    aput v3, v0, v1

    .line 211
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 215
    :cond_0
    invoke-static {v0}, Lcom/samsung/thumbnail/util/Utils;->createMatrixFromArray([F)Landroid/graphics/Matrix;

    move-result-object v2

    .line 217
    .local v2, "ret":Landroid/graphics/Matrix;
    return-object v2
.end method

.method private paintPattern(Landroid/graphics/Canvas;Lorg/icepdf/core/pobjects/graphics/Shapes;Landroid/graphics/RectF;)V
    .locals 0
    .param p1, "g"    # Landroid/graphics/Canvas;
    .param p2, "tilingShapes"    # Lorg/icepdf/core/pobjects/graphics/Shapes;
    .param p3, "bBoxMod"    # Landroid/graphics/RectF;

    .prologue
    .line 393
    invoke-virtual {p2, p1}, Lorg/icepdf/core/pobjects/graphics/Shapes;->paint(Landroid/graphics/Canvas;)V

    .line 436
    return-void
.end method


# virtual methods
.method public getBBox()Landroid/graphics/RectF;
    .locals 1

    .prologue
    .line 467
    iget-object v0, p0, Lorg/icepdf/core/pobjects/graphics/TilingPattern;->bBox:Landroid/graphics/RectF;

    return-object v0
.end method

.method public getFirstColor()Lorg/apache/poi/java/awt/Color;
    .locals 3

    .prologue
    .line 228
    iget-object v2, p0, Lorg/icepdf/core/pobjects/graphics/TilingPattern;->shapes:Lorg/icepdf/core/pobjects/graphics/Shapes;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lorg/icepdf/core/pobjects/graphics/TilingPattern;->unColored:Lorg/apache/poi/java/awt/Color;

    if-nez v2, :cond_1

    .line 229
    const/4 v0, 0x0

    .local v0, "i":I
    iget-object v2, p0, Lorg/icepdf/core/pobjects/graphics/TilingPattern;->shapes:Lorg/icepdf/core/pobjects/graphics/Shapes;

    iget-object v2, v2, Lorg/icepdf/core/pobjects/graphics/Shapes;->shapes:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->size()I

    move-result v1

    .local v1, "max":I
    :goto_0
    if-ge v0, v1, :cond_1

    .line 230
    iget-object v2, p0, Lorg/icepdf/core/pobjects/graphics/TilingPattern;->shapes:Lorg/icepdf/core/pobjects/graphics/Shapes;

    iget-object v2, v2, Lorg/icepdf/core/pobjects/graphics/Shapes;->shapes:Ljava/util/Vector;

    invoke-virtual {v2, v0}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v2

    instance-of v2, v2, Lorg/apache/poi/java/awt/Color;

    if-eqz v2, :cond_0

    .line 231
    iget-object v2, p0, Lorg/icepdf/core/pobjects/graphics/TilingPattern;->shapes:Lorg/icepdf/core/pobjects/graphics/Shapes;

    iget-object v2, v2, Lorg/icepdf/core/pobjects/graphics/Shapes;->shapes:Ljava/util/Vector;

    invoke-virtual {v2, v0}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/poi/java/awt/Color;

    iput-object v2, p0, Lorg/icepdf/core/pobjects/graphics/TilingPattern;->unColored:Lorg/apache/poi/java/awt/Color;

    .line 232
    iget-object v2, p0, Lorg/icepdf/core/pobjects/graphics/TilingPattern;->unColored:Lorg/apache/poi/java/awt/Color;

    .line 241
    .end local v0    # "i":I
    .end local v1    # "max":I
    :goto_1
    return-object v2

    .line 229
    .restart local v0    # "i":I
    .restart local v1    # "max":I
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 237
    .end local v0    # "i":I
    .end local v1    # "max":I
    :cond_1
    iget-object v2, p0, Lorg/icepdf/core/pobjects/graphics/TilingPattern;->unColored:Lorg/apache/poi/java/awt/Color;

    if-nez v2, :cond_2

    .line 238
    sget-object v2, Lorg/apache/poi/java/awt/Color;->black:Lorg/apache/poi/java/awt/Color;

    iput-object v2, p0, Lorg/icepdf/core/pobjects/graphics/TilingPattern;->unColored:Lorg/apache/poi/java/awt/Color;

    .line 239
    iget-object v2, p0, Lorg/icepdf/core/pobjects/graphics/TilingPattern;->unColored:Lorg/apache/poi/java/awt/Color;

    goto :goto_1

    .line 241
    :cond_2
    iget-object v2, p0, Lorg/icepdf/core/pobjects/graphics/TilingPattern;->unColored:Lorg/apache/poi/java/awt/Color;

    goto :goto_1
.end method

.method public getInvMatrix()Landroid/graphics/Matrix;
    .locals 2

    .prologue
    .line 492
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    .line 494
    .local v0, "ret":Landroid/graphics/Matrix;
    iget-object v1, p0, Lorg/icepdf/core/pobjects/graphics/TilingPattern;->matrix:Landroid/graphics/Matrix;

    invoke-virtual {v1, v0}, Landroid/graphics/Matrix;->invert(Landroid/graphics/Matrix;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 500
    .end local v0    # "ret":Landroid/graphics/Matrix;
    :goto_0
    return-object v0

    .restart local v0    # "ret":Landroid/graphics/Matrix;
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getMatrix()Landroid/graphics/Matrix;
    .locals 1

    .prologue
    .line 488
    iget-object v0, p0, Lorg/icepdf/core/pobjects/graphics/TilingPattern;->matrix:Landroid/graphics/Matrix;

    return-object v0
.end method

.method public getPaint()Landroid/graphics/Paint;
    .locals 1

    .prologue
    .line 439
    iget-object v0, p0, Lorg/icepdf/core/pobjects/graphics/TilingPattern;->patternPaint:Landroid/graphics/Paint;

    return-object v0
.end method

.method public getPaintType()I
    .locals 1

    .prologue
    .line 451
    iget v0, p0, Lorg/icepdf/core/pobjects/graphics/TilingPattern;->paintType:I

    return v0
.end method

.method public getParentGraphicState()Lorg/icepdf/core/pobjects/graphics/GraphicsState;
    .locals 1

    .prologue
    .line 522
    iget-object v0, p0, Lorg/icepdf/core/pobjects/graphics/TilingPattern;->parentGraphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    return-object v0
.end method

.method public getPatternType()I
    .locals 1

    .prologue
    .line 443
    iget v0, p0, Lorg/icepdf/core/pobjects/graphics/TilingPattern;->patternType:I

    return v0
.end method

.method public getShapes()Lorg/icepdf/core/pobjects/graphics/Shapes;
    .locals 1

    .prologue
    .line 509
    iget-object v0, p0, Lorg/icepdf/core/pobjects/graphics/TilingPattern;->shapes:Lorg/icepdf/core/pobjects/graphics/Shapes;

    return-object v0
.end method

.method public getTilingType()I
    .locals 1

    .prologue
    .line 459
    iget v0, p0, Lorg/icepdf/core/pobjects/graphics/TilingPattern;->tilingType:I

    return v0
.end method

.method public getType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 199
    iget-object v0, p0, Lorg/icepdf/core/pobjects/graphics/TilingPattern;->type:Ljava/lang/String;

    return-object v0
.end method

.method public getUnColored()Lorg/apache/poi/java/awt/Color;
    .locals 1

    .prologue
    .line 526
    iget-object v0, p0, Lorg/icepdf/core/pobjects/graphics/TilingPattern;->unColored:Lorg/apache/poi/java/awt/Color;

    return-object v0
.end method

.method public getXStep()F
    .locals 1

    .prologue
    .line 480
    iget v0, p0, Lorg/icepdf/core/pobjects/graphics/TilingPattern;->xStep:F

    return v0
.end method

.method public getYStep()F
    .locals 1

    .prologue
    .line 484
    iget v0, p0, Lorg/icepdf/core/pobjects/graphics/TilingPattern;->yStep:F

    return v0
.end method

.method public getbBoxMod()Landroid/graphics/RectF;
    .locals 1

    .prologue
    .line 476
    iget-object v0, p0, Lorg/icepdf/core/pobjects/graphics/TilingPattern;->bBoxMod:Landroid/graphics/RectF;

    return-object v0
.end method

.method public init()V
    .locals 11

    .prologue
    const v9, 0x46fffe00    # 32767.0f

    .line 250
    iget-boolean v6, p0, Lorg/icepdf/core/pobjects/graphics/TilingPattern;->inited:Z

    if-eqz v6, :cond_0

    .line 304
    :goto_0
    return-void

    .line 253
    :cond_0
    const/4 v6, 0x1

    iput-boolean v6, p0, Lorg/icepdf/core/pobjects/graphics/TilingPattern;->inited:Z

    .line 256
    iget-object v6, p0, Lorg/icepdf/core/pobjects/graphics/TilingPattern;->library:Lorg/icepdf/core/util/Library;

    iget-object v7, p0, Lorg/icepdf/core/pobjects/graphics/TilingPattern;->entries:Ljava/util/Hashtable;

    const-string/jumbo v8, "Resources"

    invoke-virtual {v6, v7, v8}, Lorg/icepdf/core/util/Library;->getResources(Ljava/util/Hashtable;Ljava/lang/String;)Lorg/icepdf/core/pobjects/Resources;

    move-result-object v4

    .line 258
    .local v4, "leafResources":Lorg/icepdf/core/pobjects/Resources;
    if-eqz v4, :cond_1

    .line 259
    iput-object v4, p0, Lorg/icepdf/core/pobjects/graphics/TilingPattern;->resources:Lorg/icepdf/core/pobjects/Resources;

    .line 266
    :cond_1
    new-instance v0, Lorg/icepdf/core/util/ContentParser;

    iget-object v6, p0, Lorg/icepdf/core/pobjects/graphics/TilingPattern;->library:Lorg/icepdf/core/util/Library;

    invoke-direct {v0, v6, v4}, Lorg/icepdf/core/util/ContentParser;-><init>(Lorg/icepdf/core/util/Library;Lorg/icepdf/core/pobjects/Resources;)V

    .line 267
    .local v0, "cp":Lorg/icepdf/core/util/ContentParser;
    iget-object v6, p0, Lorg/icepdf/core/pobjects/graphics/TilingPattern;->parentGraphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    invoke-virtual {v0, v6}, Lorg/icepdf/core/util/ContentParser;->setGraphicsState(Lorg/icepdf/core/pobjects/graphics/GraphicsState;)V

    .line 268
    invoke-virtual {p0}, Lorg/icepdf/core/pobjects/graphics/TilingPattern;->getInputStreamForDecodedStreamBytes()Ljava/io/InputStream;

    move-result-object v3

    .line 269
    .local v3, "in":Ljava/io/InputStream;
    if-eqz v3, :cond_2

    .line 271
    :try_start_0
    invoke-virtual {v0, v3}, Lorg/icepdf/core/util/ContentParser;->parse(Ljava/io/InputStream;)Lorg/icepdf/core/pobjects/graphics/Shapes;

    move-result-object v6

    iput-object v6, p0, Lorg/icepdf/core/pobjects/graphics/TilingPattern;->shapes:Lorg/icepdf/core/pobjects/graphics/Shapes;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 276
    :try_start_1
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 282
    :cond_2
    :goto_1
    iget v6, p0, Lorg/icepdf/core/pobjects/graphics/TilingPattern;->xStep:F

    cmpl-float v6, v6, v9

    if-nez v6, :cond_3

    .line 283
    iget-object v6, p0, Lorg/icepdf/core/pobjects/graphics/TilingPattern;->bBox:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->right:F

    iget-object v7, p0, Lorg/icepdf/core/pobjects/graphics/TilingPattern;->bBox:Landroid/graphics/RectF;

    iget v7, v7, Landroid/graphics/RectF;->left:F

    sub-float/2addr v6, v7

    iput v6, p0, Lorg/icepdf/core/pobjects/graphics/TilingPattern;->xStep:F

    .line 285
    :cond_3
    iget v6, p0, Lorg/icepdf/core/pobjects/graphics/TilingPattern;->yStep:F

    cmpl-float v6, v6, v9

    if-nez v6, :cond_4

    .line 286
    iget-object v6, p0, Lorg/icepdf/core/pobjects/graphics/TilingPattern;->bBox:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->bottom:F

    iget-object v7, p0, Lorg/icepdf/core/pobjects/graphics/TilingPattern;->bBox:Landroid/graphics/RectF;

    iget v7, v7, Landroid/graphics/RectF;->top:F

    sub-float/2addr v6, v7

    iput v6, p0, Lorg/icepdf/core/pobjects/graphics/TilingPattern;->yStep:F

    .line 291
    :cond_4
    iget-object v6, p0, Lorg/icepdf/core/pobjects/graphics/TilingPattern;->bBox:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->right:F

    iget-object v7, p0, Lorg/icepdf/core/pobjects/graphics/TilingPattern;->bBox:Landroid/graphics/RectF;

    iget v7, v7, Landroid/graphics/RectF;->left:F

    sub-float/2addr v6, v7

    iget v7, p0, Lorg/icepdf/core/pobjects/graphics/TilingPattern;->xStep:F

    invoke-static {v6, v7}, Ljava/lang/Float;->compare(FF)I

    move-result v6

    if-nez v6, :cond_5

    iget-object v6, p0, Lorg/icepdf/core/pobjects/graphics/TilingPattern;->bBox:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->right:F

    iget-object v7, p0, Lorg/icepdf/core/pobjects/graphics/TilingPattern;->bBox:Landroid/graphics/RectF;

    iget v7, v7, Landroid/graphics/RectF;->left:F

    sub-float v5, v6, v7

    .line 292
    .local v5, "width":F
    :goto_2
    iget-object v6, p0, Lorg/icepdf/core/pobjects/graphics/TilingPattern;->bBox:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->bottom:F

    iget-object v7, p0, Lorg/icepdf/core/pobjects/graphics/TilingPattern;->bBox:Landroid/graphics/RectF;

    iget v7, v7, Landroid/graphics/RectF;->top:F

    sub-float/2addr v6, v7

    iget v7, p0, Lorg/icepdf/core/pobjects/graphics/TilingPattern;->yStep:F

    invoke-static {v6, v7}, Ljava/lang/Float;->compare(FF)I

    move-result v6

    if-nez v6, :cond_6

    iget-object v6, p0, Lorg/icepdf/core/pobjects/graphics/TilingPattern;->bBox:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->bottom:F

    iget-object v7, p0, Lorg/icepdf/core/pobjects/graphics/TilingPattern;->bBox:Landroid/graphics/RectF;

    iget v7, v7, Landroid/graphics/RectF;->top:F

    sub-float v2, v6, v7

    .line 294
    .local v2, "height":F
    :goto_3
    new-instance v6, Landroid/graphics/RectF;

    iget-object v7, p0, Lorg/icepdf/core/pobjects/graphics/TilingPattern;->bBox:Landroid/graphics/RectF;

    iget v7, v7, Landroid/graphics/RectF;->left:F

    iget-object v8, p0, Lorg/icepdf/core/pobjects/graphics/TilingPattern;->bBox:Landroid/graphics/RectF;

    iget v8, v8, Landroid/graphics/RectF;->top:F

    iget-object v9, p0, Lorg/icepdf/core/pobjects/graphics/TilingPattern;->bBox:Landroid/graphics/RectF;

    iget v9, v9, Landroid/graphics/RectF;->left:F

    add-float/2addr v9, v5

    iget-object v10, p0, Lorg/icepdf/core/pobjects/graphics/TilingPattern;->bBox:Landroid/graphics/RectF;

    iget v10, v10, Landroid/graphics/RectF;->top:F

    add-float/2addr v10, v2

    invoke-direct {v6, v7, v8, v9, v10}, Landroid/graphics/RectF;-><init>(FFFF)V

    iput-object v6, p0, Lorg/icepdf/core/pobjects/graphics/TilingPattern;->bBoxMod:Landroid/graphics/RectF;

    .line 303
    iget-object v6, p0, Lorg/icepdf/core/pobjects/graphics/TilingPattern;->matrix:Landroid/graphics/Matrix;

    iget-object v7, p0, Lorg/icepdf/core/pobjects/graphics/TilingPattern;->bBoxMod:Landroid/graphics/RectF;

    invoke-virtual {v6, v7}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    goto/16 :goto_0

    .line 272
    .end local v2    # "height":F
    .end local v5    # "width":F
    :catch_0
    move-exception v1

    .line 273
    .local v1, "e":Ljava/lang/Throwable;
    :try_start_2
    sget-object v6, Lorg/icepdf/core/pobjects/graphics/TilingPattern;->logger:Ljava/util/logging/Logger;

    sget-object v7, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string/jumbo v8, "Error processing tiling pattern."

    invoke-virtual {v6, v7, v8, v1}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 276
    :try_start_3
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    goto/16 :goto_1

    .line 277
    :catch_1
    move-exception v6

    goto/16 :goto_1

    .line 275
    .end local v1    # "e":Ljava/lang/Throwable;
    :catchall_0
    move-exception v6

    .line 276
    :try_start_4
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    .line 278
    :goto_4
    throw v6

    .line 291
    :cond_5
    iget v5, p0, Lorg/icepdf/core/pobjects/graphics/TilingPattern;->xStep:F

    goto :goto_2

    .line 292
    .restart local v5    # "width":F
    :cond_6
    iget v2, p0, Lorg/icepdf/core/pobjects/graphics/TilingPattern;->yStep:F

    goto :goto_3

    .line 277
    .end local v5    # "width":F
    :catch_2
    move-exception v6

    goto/16 :goto_1

    :catch_3
    move-exception v7

    goto :goto_4
.end method

.method public paintPattern(Landroid/graphics/Canvas;Lorg/icepdf/core/pobjects/Page;)V
    .locals 10
    .param p1, "g"    # Landroid/graphics/Canvas;
    .param p2, "parentPage"    # Lorg/icepdf/core/pobjects/Page;

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x0

    .line 314
    iget-boolean v6, p0, Lorg/icepdf/core/pobjects/graphics/TilingPattern;->inited:Z

    if-nez v6, :cond_0

    .line 315
    invoke-virtual {p0}, Lorg/icepdf/core/pobjects/graphics/TilingPattern;->init()V

    .line 317
    :cond_0
    iget-object v6, p0, Lorg/icepdf/core/pobjects/graphics/TilingPattern;->patternPaint:Landroid/graphics/Paint;

    if-nez v6, :cond_5

    .line 320
    iget-object v6, p0, Lorg/icepdf/core/pobjects/graphics/TilingPattern;->bBoxMod:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->right:F

    iget-object v7, p0, Lorg/icepdf/core/pobjects/graphics/TilingPattern;->bBoxMod:Landroid/graphics/RectF;

    iget v7, v7, Landroid/graphics/RectF;->left:F

    sub-float/2addr v6, v7

    float-to-int v5, v6

    .line 321
    .local v5, "width":I
    iget-object v6, p0, Lorg/icepdf/core/pobjects/graphics/TilingPattern;->bBoxMod:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->bottom:F

    iget-object v7, p0, Lorg/icepdf/core/pobjects/graphics/TilingPattern;->bBoxMod:Landroid/graphics/RectF;

    iget v7, v7, Landroid/graphics/RectF;->top:F

    sub-float/2addr v6, v7

    float-to-int v3, v6

    .line 324
    .local v3, "height":I
    if-nez v5, :cond_1

    .line 325
    const/4 v5, 0x1

    .line 327
    :cond_1
    if-nez v3, :cond_2

    .line 328
    const/4 v3, 0x1

    .line 332
    :cond_2
    sget-object v6, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v5, v3, v6}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 334
    .local v0, "bi":Landroid/graphics/Bitmap;
    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 339
    .local v1, "canvas":Landroid/graphics/Canvas;
    new-instance v6, Landroid/graphics/Paint;

    const/4 v7, 0x1

    invoke-direct {v6, v7}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v6, p0, Lorg/icepdf/core/pobjects/graphics/TilingPattern;->patternPaint:Landroid/graphics/Paint;

    .line 340
    iget-object v6, p0, Lorg/icepdf/core/pobjects/graphics/TilingPattern;->patternPaint:Landroid/graphics/Paint;

    sget-object v7, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v6, v7}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 342
    iget-object v6, p0, Lorg/icepdf/core/pobjects/graphics/TilingPattern;->patternPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v6}, Landroid/graphics/Canvas;->drawPaint(Landroid/graphics/Paint;)V

    .line 348
    invoke-virtual {p0}, Lorg/icepdf/core/pobjects/graphics/TilingPattern;->getShapes()Lorg/icepdf/core/pobjects/graphics/Shapes;

    move-result-object v4

    .line 349
    .local v4, "tilingShapes":Lorg/icepdf/core/pobjects/graphics/Shapes;
    if-eqz v4, :cond_3

    .line 350
    invoke-virtual {v4, p2}, Lorg/icepdf/core/pobjects/graphics/Shapes;->setPageParent(Lorg/icepdf/core/pobjects/Page;)V

    .line 355
    iget-object v6, p0, Lorg/icepdf/core/pobjects/graphics/TilingPattern;->bBoxMod:Landroid/graphics/RectF;

    invoke-virtual {v6}, Landroid/graphics/RectF;->width()F

    move-result v6

    float-to-int v6, v6

    iget-object v7, p0, Lorg/icepdf/core/pobjects/graphics/TilingPattern;->bBoxMod:Landroid/graphics/RectF;

    invoke-virtual {v7}, Landroid/graphics/RectF;->height()F

    move-result v7

    float-to-int v7, v7

    invoke-virtual {v1, v9, v9, v6, v7}, Landroid/graphics/Canvas;->clipRect(IIII)Z

    .line 359
    iget-object v6, p0, Lorg/icepdf/core/pobjects/graphics/TilingPattern;->bBoxMod:Landroid/graphics/RectF;

    invoke-direct {p0, v1, v4, v6}, Lorg/icepdf/core/pobjects/graphics/TilingPattern;->paintPattern(Landroid/graphics/Canvas;Lorg/icepdf/core/pobjects/graphics/Shapes;Landroid/graphics/RectF;)V

    .line 361
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getClipBounds()Landroid/graphics/Rect;

    move-result-object v2

    .line 362
    .local v2, "dst":Landroid/graphics/Rect;
    invoke-virtual {p1, v0, v8, v2, v8}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 363
    invoke-virtual {v4, v8}, Lorg/icepdf/core/pobjects/graphics/Shapes;->setPageParent(Lorg/icepdf/core/pobjects/Page;)V

    .line 381
    .end local v2    # "dst":Landroid/graphics/Rect;
    :cond_3
    if-eqz v0, :cond_4

    .line 382
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 388
    .end local v0    # "bi":Landroid/graphics/Bitmap;
    .end local v1    # "canvas":Landroid/graphics/Canvas;
    .end local v3    # "height":I
    .end local v4    # "tilingShapes":Lorg/icepdf/core/pobjects/graphics/Shapes;
    .end local v5    # "width":I
    :cond_4
    :goto_0
    return-void

    .line 385
    :cond_5
    iget-object v6, p0, Lorg/icepdf/core/pobjects/graphics/TilingPattern;->patternPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v6}, Landroid/graphics/Canvas;->drawPaint(Landroid/graphics/Paint;)V

    goto :goto_0
.end method

.method public setMatrix(Landroid/graphics/Matrix;)V
    .locals 0
    .param p1, "matrix"    # Landroid/graphics/Matrix;

    .prologue
    .line 505
    iput-object p1, p0, Lorg/icepdf/core/pobjects/graphics/TilingPattern;->matrix:Landroid/graphics/Matrix;

    .line 506
    return-void
.end method

.method public setPaintType(I)V
    .locals 0
    .param p1, "paintType"    # I

    .prologue
    .line 455
    iput p1, p0, Lorg/icepdf/core/pobjects/graphics/TilingPattern;->paintType:I

    .line 456
    return-void
.end method

.method public setParentGraphicState(Lorg/icepdf/core/pobjects/graphics/GraphicsState;)V
    .locals 0
    .param p1, "graphicsState"    # Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    .prologue
    .line 518
    iput-object p1, p0, Lorg/icepdf/core/pobjects/graphics/TilingPattern;->parentGraphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    .line 519
    return-void
.end method

.method public setPatternType(I)V
    .locals 0
    .param p1, "patternType"    # I

    .prologue
    .line 447
    iput p1, p0, Lorg/icepdf/core/pobjects/graphics/TilingPattern;->patternType:I

    .line 448
    return-void
.end method

.method public setShapes(Lorg/icepdf/core/pobjects/graphics/Shapes;)V
    .locals 0
    .param p1, "shapes"    # Lorg/icepdf/core/pobjects/graphics/Shapes;

    .prologue
    .line 513
    iput-object p1, p0, Lorg/icepdf/core/pobjects/graphics/TilingPattern;->shapes:Lorg/icepdf/core/pobjects/graphics/Shapes;

    .line 514
    return-void
.end method

.method public setTilingType(I)V
    .locals 0
    .param p1, "tilingType"    # I

    .prologue
    .line 463
    iput p1, p0, Lorg/icepdf/core/pobjects/graphics/TilingPattern;->tilingType:I

    .line 464
    return-void
.end method

.method public setUnColored(Lorg/apache/poi/java/awt/Color;)V
    .locals 0
    .param p1, "unColored"    # Lorg/apache/poi/java/awt/Color;

    .prologue
    .line 530
    iput-object p1, p0, Lorg/icepdf/core/pobjects/graphics/TilingPattern;->unColored:Lorg/apache/poi/java/awt/Color;

    .line 531
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 537
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "Tiling Pattern: \n           type: pattern \n    patternType: tilling\n      paintType: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v0, p0, Lorg/icepdf/core/pobjects/graphics/TilingPattern;->paintType:I

    const/4 v2, 0x1

    if-ne v0, v2, :cond_0

    const-string/jumbo v0, "colored"

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "\n    tilingType: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lorg/icepdf/core/pobjects/graphics/TilingPattern;->tilingType:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "\n          bbox: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/icepdf/core/pobjects/graphics/TilingPattern;->bBox:Landroid/graphics/RectF;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "\n         xStep: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lorg/icepdf/core/pobjects/graphics/TilingPattern;->xStep:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "\n         yStep: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lorg/icepdf/core/pobjects/graphics/TilingPattern;->yStep:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "\n      resource: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/icepdf/core/pobjects/graphics/TilingPattern;->resources:Lorg/icepdf/core/pobjects/Resources;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "\n        matrix: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/icepdf/core/pobjects/graphics/TilingPattern;->matrix:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string/jumbo v0, "uncoloured"

    goto :goto_0
.end method
