.class public Lorg/icepdf/core/pobjects/Document;
.super Ljava/lang/Object;
.source "Document.java"


# static fields
.field private static isCachingEnabled:Z

.field private static final logger:Ljava/util/logging/Logger;


# instance fields
.field private cachedFilePath:Ljava/lang/String;

.field private catalog:Lorg/icepdf/core/pobjects/Catalog;

.field private documentSeekableInput:Lorg/icepdf/core/io/SeekableInput;

.field private library:Lorg/icepdf/core/util/Library;

.field private origin:Ljava/lang/String;

.field private pTrailer:Lorg/icepdf/core/pobjects/PTrailer;

.field private securityCallback:Lorg/icepdf/core/SecurityCallback;

.field private stateManager:Lorg/icepdf/core/pobjects/StateManager;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 79
    const-class v0, Lorg/icepdf/core/pobjects/Document;

    invoke-virtual {v0}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Lorg/icepdf/core/pobjects/Document;->logger:Ljava/util/logging/Logger;

    .line 129
    const-string/jumbo v0, "org.icepdf.core.streamcache.enabled"

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lorg/icepdf/core/util/Defs;->sysPropertyBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lorg/icepdf/core/pobjects/Document;->isCachingEnabled:Z

    .line 131
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "mContext"    # Landroid/content/Context;

    .prologue
    .line 137
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 123
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/icepdf/core/pobjects/Document;->library:Lorg/icepdf/core/util/Library;

    .line 138
    return-void
.end method

.method private attemptAuthorizeSecurityManager()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/icepdf/core/exceptions/PDFSecurityException;
        }
    .end annotation

    .prologue
    .line 943
    iget-object v2, p0, Lorg/icepdf/core/pobjects/Document;->library:Lorg/icepdf/core/util/Library;

    iget-object v2, v2, Lorg/icepdf/core/util/Library;->securityManager:Lorg/icepdf/core/pobjects/security/SecurityManager;

    const-string/jumbo v3, ""

    invoke-virtual {v2, v3}, Lorg/icepdf/core/pobjects/security/SecurityManager;->isAuthorized(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 945
    const/4 v0, 0x1

    .line 954
    .local v0, "count":I
    :cond_0
    iget-object v2, p0, Lorg/icepdf/core/pobjects/Document;->securityCallback:Lorg/icepdf/core/SecurityCallback;

    if-eqz v2, :cond_1

    .line 955
    iget-object v2, p0, Lorg/icepdf/core/pobjects/Document;->securityCallback:Lorg/icepdf/core/SecurityCallback;

    invoke-interface {v2, p0}, Lorg/icepdf/core/SecurityCallback;->requestPassword(Lorg/icepdf/core/pobjects/Document;)Ljava/lang/String;

    move-result-object v1

    .line 956
    .local v1, "password":Ljava/lang/String;
    if-nez v1, :cond_2

    .line 957
    new-instance v2, Lorg/icepdf/core/exceptions/PDFSecurityException;

    const-string/jumbo v3, "Encryption error"

    invoke-direct {v2, v3}, Lorg/icepdf/core/exceptions/PDFSecurityException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 960
    .end local v1    # "password":Ljava/lang/String;
    :cond_1
    new-instance v2, Lorg/icepdf/core/exceptions/PDFSecurityException;

    const-string/jumbo v3, "Encryption error"

    invoke-direct {v2, v3}, Lorg/icepdf/core/exceptions/PDFSecurityException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 965
    .restart local v1    # "password":Ljava/lang/String;
    :cond_2
    iget-object v2, p0, Lorg/icepdf/core/pobjects/Document;->library:Lorg/icepdf/core/util/Library;

    iget-object v2, v2, Lorg/icepdf/core/util/Library;->securityManager:Lorg/icepdf/core/pobjects/security/SecurityManager;

    invoke-virtual {v2, v1}, Lorg/icepdf/core/pobjects/security/SecurityManager;->isAuthorized(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 977
    .end local v0    # "count":I
    .end local v1    # "password":Ljava/lang/String;
    :cond_3
    iget-object v2, p0, Lorg/icepdf/core/pobjects/Document;->library:Lorg/icepdf/core/util/Library;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lorg/icepdf/core/util/Library;->setEncrypted(Z)V

    .line 978
    return-void

    .line 968
    .restart local v0    # "count":I
    .restart local v1    # "password":Ljava/lang/String;
    :cond_4
    add-int/lit8 v0, v0, 0x1

    .line 970
    const/4 v2, 0x3

    if-le v0, v2, :cond_0

    .line 971
    new-instance v2, Lorg/icepdf/core/exceptions/PDFSecurityException;

    const-string/jumbo v3, "Encryption error"

    invoke-direct {v2, v3}, Lorg/icepdf/core/exceptions/PDFSecurityException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method private checkHeader(Lorg/icepdf/core/io/SeekableInput;)V
    .locals 9
    .param p1, "in"    # Lorg/icepdf/core/io/SeekableInput;

    .prologue
    const/4 v8, 0x7

    .line 452
    const/4 v2, 0x0

    .line 453
    .local v2, "header":Z
    invoke-interface {p1}, Lorg/icepdf/core/io/SeekableInput;->markSupported()Z

    move-result v7

    if-nez v7, :cond_1

    .line 490
    :cond_0
    :goto_0
    return-void

    .line 456
    :cond_1
    const/4 v6, 0x7

    .line 457
    .local v6, "scanLength":I
    :try_start_0
    const-string/jumbo v4, "%PDF-1."

    .line 458
    .local v4, "scanFor":Ljava/lang/String;
    const/4 v5, 0x0

    .line 459
    .local v5, "scanForIndex":I
    const/4 v7, 0x7

    invoke-interface {p1, v7}, Lorg/icepdf/core/io/SeekableInput;->mark(I)V

    .line 460
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    if-ge v3, v8, :cond_5

    .line 461
    invoke-interface {p1}, Lorg/icepdf/core/io/SeekableInput;->read()I

    move-result v0

    .line 462
    .local v0, "data":I
    if-gez v0, :cond_2

    .line 463
    invoke-interface {p1}, Lorg/icepdf/core/io/SeekableInput;->reset()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 481
    .end local v0    # "data":I
    .end local v3    # "i":I
    .end local v4    # "scanFor":Ljava/lang/String;
    .end local v5    # "scanForIndex":I
    :catch_0
    move-exception v1

    .line 483
    .local v1, "e":Ljava/io/IOException;
    :try_start_1
    invoke-interface {p1}, Lorg/icepdf/core/io/SeekableInput;->reset()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 488
    .end local v1    # "e":Ljava/io/IOException;
    :goto_2
    if-nez v2, :cond_0

    .line 489
    new-instance v7, Ljava/lang/RuntimeException;

    const-string/jumbo v8, "Invalid Header"

    invoke-direct {v7, v8}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 467
    .restart local v0    # "data":I
    .restart local v3    # "i":I
    .restart local v4    # "scanFor":Ljava/lang/String;
    .restart local v5    # "scanForIndex":I
    :cond_2
    :try_start_2
    const-string/jumbo v7, "%PDF-1."

    invoke-virtual {v7, v5}, Ljava/lang/String;->charAt(I)C

    move-result v7

    if-ne v0, v7, :cond_4

    .line 468
    add-int/lit8 v5, v5, 0x1

    .line 469
    const-string/jumbo v7, "%PDF-1."

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    if-ne v5, v7, :cond_3

    .line 471
    const/4 v2, 0x1

    .line 460
    :cond_3
    :goto_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 474
    :cond_4
    const/4 v5, 0x0

    goto :goto_3

    .line 479
    .end local v0    # "data":I
    :cond_5
    invoke-interface {p1}, Lorg/icepdf/core/io/SeekableInput;->reset()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_2

    .line 485
    .end local v3    # "i":I
    .end local v4    # "scanFor":Ljava/lang/String;
    .end local v5    # "scanForIndex":I
    .restart local v1    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v7

    goto :goto_2
.end method

.method private getDocumentCachedFilePath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 173
    iget-object v0, p0, Lorg/icepdf/core/pobjects/Document;->cachedFilePath:Ljava/lang/String;

    return-object v0
.end method

.method private getInitialCrossReferencePosition(Lorg/icepdf/core/io/SeekableInput;)J
    .locals 18
    .param p1, "in"    # Lorg/icepdf/core/io/SeekableInput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 657
    invoke-interface/range {p1 .. p1}, Lorg/icepdf/core/io/SeekableInput;->seekEnd()V

    .line 659
    invoke-interface/range {p1 .. p1}, Lorg/icepdf/core/io/SeekableInput;->getAbsolutePosition()J

    move-result-wide v8

    .line 660
    .local v8, "endOfFile":J
    const-wide/16 v14, 0x1

    sub-long v6, v8, v14

    .line 661
    .local v6, "currentPosition":J
    const-wide/16 v2, -0x1

    .line 662
    .local v2, "afterStartxref":J
    const-string/jumbo v10, "startxref"

    .line 663
    .local v10, "startxref":Ljava/lang/String;
    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v13

    add-int/lit8 v11, v13, -0x1

    .line 665
    .local v11, "startxrefIndexToMatch":I
    :goto_0
    const-wide/16 v14, 0x0

    cmp-long v13, v6, v14

    if-ltz v13, :cond_1

    sub-long v14, v8, v6

    const-wide/16 v16, 0x800

    cmp-long v13, v14, v16

    if-gez v13, :cond_1

    .line 666
    move-object/from16 v0, p1

    invoke-interface {v0, v6, v7}, Lorg/icepdf/core/io/SeekableInput;->seekAbsolute(J)V

    .line 667
    invoke-interface/range {p1 .. p1}, Lorg/icepdf/core/io/SeekableInput;->read()I

    move-result v4

    .line 668
    .local v4, "curr":I
    if-gez v4, :cond_0

    .line 669
    new-instance v13, Ljava/io/EOFException;

    const-string/jumbo v14, "Could not find startxref at end of file"

    invoke-direct {v13, v14}, Ljava/io/EOFException;-><init>(Ljava/lang/String;)V

    throw v13

    .line 671
    :cond_0
    invoke-virtual {v10, v11}, Ljava/lang/String;->charAt(I)C

    move-result v13

    if-ne v4, v13, :cond_3

    .line 673
    if-nez v11, :cond_2

    .line 674
    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v13

    int-to-long v14, v13

    add-long v2, v6, v14

    .line 682
    .end local v4    # "curr":I
    :cond_1
    const-wide/16 v14, 0x0

    cmp-long v13, v2, v14

    if-gez v13, :cond_4

    .line 683
    new-instance v13, Ljava/io/EOFException;

    const-string/jumbo v14, "Could not find startxref near end of file"

    invoke-direct {v13, v14}, Ljava/io/EOFException;-><init>(Ljava/lang/String;)V

    throw v13

    .line 677
    .restart local v4    # "curr":I
    :cond_2
    add-int/lit8 v11, v11, -0x1

    .line 680
    :goto_1
    const-wide/16 v14, 0x1

    sub-long/2addr v6, v14

    .line 681
    goto :goto_0

    .line 679
    :cond_3
    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v13

    add-int/lit8 v11, v13, -0x1

    goto :goto_1

    .line 685
    .end local v4    # "curr":I
    :cond_4
    move-object/from16 v0, p1

    invoke-interface {v0, v2, v3}, Lorg/icepdf/core/io/SeekableInput;->seekAbsolute(J)V

    .line 686
    new-instance v5, Lorg/icepdf/core/util/Parser;

    move-object/from16 v0, p1

    invoke-direct {v5, v0}, Lorg/icepdf/core/util/Parser;-><init>(Lorg/icepdf/core/io/SeekableInput;)V

    .line 687
    .local v5, "parser":Lorg/icepdf/core/util/Parser;
    invoke-virtual {v5}, Lorg/icepdf/core/util/Parser;->getToken()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/Number;

    .line 688
    .local v12, "xrefPositionObj":Ljava/lang/Number;
    if-nez v12, :cond_5

    .line 689
    new-instance v13, Ljava/lang/RuntimeException;

    const-string/jumbo v14, "Could not find ending cross reference position"

    invoke-direct {v13, v14}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v13

    .line 691
    :cond_5
    invoke-virtual {v12}, Ljava/lang/Number;->longValue()J

    move-result-wide v14

    return-wide v14
.end method

.method public static getLibraryVersion()Ljava/lang/String;
    .locals 2

    .prologue
    .line 89
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lorg/icepdf/core/application/ProductInfo;->PRIMARY:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lorg/icepdf/core/application/ProductInfo;->SECONDARY:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lorg/icepdf/core/application/ProductInfo;->TERTIARY:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lorg/icepdf/core/application/ProductInfo;->RELEASE_TYPE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private loadDocumentViaLinearTraversal(Lorg/icepdf/core/io/SeekableInput;)V
    .locals 12
    .param p1, "sInput"    # Lorg/icepdf/core/io/SeekableInput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/icepdf/core/exceptions/PDFException;,
            Lorg/icepdf/core/exceptions/PDFSecurityException;
        }
    .end annotation

    .prologue
    .line 712
    invoke-interface {p1}, Lorg/icepdf/core/io/SeekableInput;->getInputStream()Ljava/io/InputStream;

    move-result-object v1

    .line 713
    .local v1, "in":Ljava/io/InputStream;
    invoke-direct {p0, v1}, Lorg/icepdf/core/pobjects/Document;->skipPastAnyPrefixJunk(Ljava/io/InputStream;)V

    .line 715
    iget-object v9, p0, Lorg/icepdf/core/pobjects/Document;->library:Lorg/icepdf/core/util/Library;

    invoke-virtual {v9}, Lorg/icepdf/core/util/Library;->setLinearTraversal()V

    .line 716
    new-instance v6, Lorg/icepdf/core/util/Parser;

    invoke-direct {v6, v1}, Lorg/icepdf/core/util/Parser;-><init>(Ljava/io/InputStream;)V

    .line 719
    .local v6, "parser":Lorg/icepdf/core/util/Parser;
    const/4 v0, 0x0

    .line 726
    .local v0, "documentTrailer":Lorg/icepdf/core/pobjects/PTrailer;
    :cond_0
    iget-object v9, p0, Lorg/icepdf/core/pobjects/Document;->library:Lorg/icepdf/core/util/Library;

    invoke-virtual {v6, v9}, Lorg/icepdf/core/util/Parser;->getObject(Lorg/icepdf/core/util/Library;)Ljava/lang/Object;

    move-result-object v7

    .line 729
    .local v7, "pdfObject":Ljava/lang/Object;
    if-nez v7, :cond_4

    .line 783
    :cond_1
    :goto_0
    if-eqz v0, :cond_2

    .line 786
    new-instance v2, Lorg/icepdf/core/util/LazyObjectLoader;

    iget-object v9, p0, Lorg/icepdf/core/pobjects/Document;->library:Lorg/icepdf/core/util/Library;

    invoke-virtual {v0}, Lorg/icepdf/core/pobjects/PTrailer;->getPrimaryCrossReference()Lorg/icepdf/core/pobjects/CrossReference;

    move-result-object v10

    invoke-direct {v2, v9, p1, v10}, Lorg/icepdf/core/util/LazyObjectLoader;-><init>(Lorg/icepdf/core/util/Library;Lorg/icepdf/core/io/SeekableInput;Lorg/icepdf/core/pobjects/CrossReference;)V

    .line 788
    .local v2, "lol":Lorg/icepdf/core/util/LazyObjectLoader;
    iget-object v9, p0, Lorg/icepdf/core/pobjects/Document;->library:Lorg/icepdf/core/util/Library;

    invoke-virtual {v9, v2}, Lorg/icepdf/core/util/Library;->setLazyObjectLoader(Lorg/icepdf/core/util/LazyObjectLoader;)V

    .line 796
    .end local v2    # "lol":Lorg/icepdf/core/util/LazyObjectLoader;
    :cond_2
    iput-object v0, p0, Lorg/icepdf/core/pobjects/Document;->pTrailer:Lorg/icepdf/core/pobjects/PTrailer;

    .line 797
    iget-object v9, p0, Lorg/icepdf/core/pobjects/Document;->library:Lorg/icepdf/core/util/Library;

    iget-object v10, p0, Lorg/icepdf/core/pobjects/Document;->catalog:Lorg/icepdf/core/pobjects/Catalog;

    invoke-virtual {v9, v10}, Lorg/icepdf/core/util/Library;->setCatalog(Lorg/icepdf/core/pobjects/Catalog;)V

    .line 800
    if-eqz v0, :cond_3

    .line 801
    invoke-direct {p0, v0}, Lorg/icepdf/core/pobjects/Document;->makeSecurityManager(Lorg/icepdf/core/pobjects/PTrailer;)Z

    move-result v3

    .line 802
    .local v3, "madeSecurityManager":Z
    if-eqz v3, :cond_3

    .line 803
    invoke-direct {p0}, Lorg/icepdf/core/pobjects/Document;->attemptAuthorizeSecurityManager()V

    .line 805
    .end local v3    # "madeSecurityManager":Z
    :cond_3
    return-void

    .line 734
    :cond_4
    sget-object v9, Lorg/icepdf/core/pobjects/Document;->logger:Ljava/util/logging/Logger;

    sget-object v10, Ljava/util/logging/Level;->FINER:Ljava/util/logging/Level;

    invoke-virtual {v9, v10}, Ljava/util/logging/Logger;->isLoggable(Ljava/util/logging/Level;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 735
    sget-object v9, Lorg/icepdf/core/pobjects/Document;->logger:Ljava/util/logging/Logger;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string/jumbo v11, " "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/util/logging/Logger;->finer(Ljava/lang/String;)V

    .line 739
    :cond_5
    instance-of v9, v7, Lorg/icepdf/core/pobjects/PObject;

    if-eqz v9, :cond_6

    move-object v8, v7

    .line 740
    check-cast v8, Lorg/icepdf/core/pobjects/PObject;

    .line 741
    .local v8, "tmp":Lorg/icepdf/core/pobjects/PObject;
    invoke-virtual {v8}, Lorg/icepdf/core/pobjects/PObject;->getObject()Ljava/lang/Object;

    move-result-object v5

    .line 742
    .local v5, "obj":Ljava/lang/Object;
    if-eqz v5, :cond_6

    .line 743
    move-object v7, v5

    .line 748
    .end local v5    # "obj":Ljava/lang/Object;
    .end local v8    # "tmp":Lorg/icepdf/core/pobjects/PObject;
    :cond_6
    instance-of v9, v7, Lorg/icepdf/core/pobjects/Catalog;

    if-eqz v9, :cond_7

    move-object v9, v7

    .line 749
    check-cast v9, Lorg/icepdf/core/pobjects/Catalog;

    iput-object v9, p0, Lorg/icepdf/core/pobjects/Document;->catalog:Lorg/icepdf/core/pobjects/Catalog;

    .line 754
    iget-object v9, p0, Lorg/icepdf/core/pobjects/Document;->catalog:Lorg/icepdf/core/pobjects/Catalog;

    if-eqz v9, :cond_7

    if-nez v0, :cond_1

    .line 761
    :cond_7
    instance-of v9, v7, Lorg/icepdf/core/pobjects/PTrailer;

    if-eqz v9, :cond_0

    .line 762
    if-nez v0, :cond_8

    move-object v0, v7

    .line 763
    check-cast v0, Lorg/icepdf/core/pobjects/PTrailer;

    .line 774
    :goto_1
    iget-object v9, p0, Lorg/icepdf/core/pobjects/Document;->catalog:Lorg/icepdf/core/pobjects/Catalog;

    if-eqz v9, :cond_0

    if-eqz v0, :cond_0

    goto/16 :goto_0

    :cond_8
    move-object v4, v7

    .line 766
    check-cast v4, Lorg/icepdf/core/pobjects/PTrailer;

    .line 767
    .local v4, "nextTrailer":Lorg/icepdf/core/pobjects/PTrailer;
    invoke-virtual {v0, v4}, Lorg/icepdf/core/pobjects/PTrailer;->addNextTrailer(Lorg/icepdf/core/pobjects/PTrailer;)V

    .line 768
    move-object v0, v4

    goto :goto_1
.end method

.method private loadDocumentViaXRefs(Lorg/icepdf/core/io/SeekableInput;)V
    .locals 14
    .param p1, "in"    # Lorg/icepdf/core/io/SeekableInput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/icepdf/core/exceptions/PDFException;,
            Lorg/icepdf/core/exceptions/PDFSecurityException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 600
    invoke-direct {p0, p1}, Lorg/icepdf/core/pobjects/Document;->skipPastAnyPrefixJunk(Lorg/icepdf/core/io/SeekableInput;)I

    move-result v4

    .line 601
    .local v4, "offset":I
    invoke-direct {p0, p1}, Lorg/icepdf/core/pobjects/Document;->getInitialCrossReferencePosition(Lorg/icepdf/core/io/SeekableInput;)J

    move-result-wide v10

    int-to-long v12, v4

    add-long v8, v10, v12

    .line 602
    .local v8, "xrefPosition":J
    const/4 v0, 0x0

    .line 603
    .local v0, "documentTrailer":Lorg/icepdf/core/pobjects/PTrailer;
    const-wide/16 v10, 0x0

    cmp-long v7, v8, v10

    if-lez v7, :cond_3

    .line 604
    invoke-interface {p1, v8, v9}, Lorg/icepdf/core/io/SeekableInput;->seekAbsolute(J)V

    .line 606
    new-instance v5, Lorg/icepdf/core/util/Parser;

    invoke-direct {v5, p1}, Lorg/icepdf/core/util/Parser;-><init>(Lorg/icepdf/core/io/SeekableInput;)V

    .line 607
    .local v5, "parser":Lorg/icepdf/core/util/Parser;
    iget-object v7, p0, Lorg/icepdf/core/pobjects/Document;->library:Lorg/icepdf/core/util/Library;

    invoke-virtual {v5, v7}, Lorg/icepdf/core/util/Parser;->getObject(Lorg/icepdf/core/util/Library;)Ljava/lang/Object;

    move-result-object v3

    .line 608
    .local v3, "obj":Ljava/lang/Object;
    instance-of v7, v3, Lorg/icepdf/core/pobjects/PObject;

    if-eqz v7, :cond_0

    .line 609
    check-cast v3, Lorg/icepdf/core/pobjects/PObject;

    .end local v3    # "obj":Ljava/lang/Object;
    invoke-virtual {v3}, Lorg/icepdf/core/pobjects/PObject;->getObject()Ljava/lang/Object;

    move-result-object v3

    .restart local v3    # "obj":Ljava/lang/Object;
    :cond_0
    move-object v6, v3

    .line 610
    check-cast v6, Lorg/icepdf/core/pobjects/PTrailer;

    .line 612
    .local v6, "trailer":Lorg/icepdf/core/pobjects/PTrailer;
    if-nez v6, :cond_1

    .line 613
    new-instance v7, Ljava/lang/RuntimeException;

    const-string/jumbo v10, "Could not find trailer"

    invoke-direct {v7, v10}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 614
    :cond_1
    invoke-virtual {v6}, Lorg/icepdf/core/pobjects/PTrailer;->getPrimaryCrossReference()Lorg/icepdf/core/pobjects/CrossReference;

    move-result-object v7

    if-nez v7, :cond_2

    .line 615
    new-instance v7, Ljava/lang/RuntimeException;

    const-string/jumbo v10, "Could not find cross reference"

    invoke-direct {v7, v10}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 616
    :cond_2
    invoke-virtual {v6, v8, v9}, Lorg/icepdf/core/pobjects/PTrailer;->setPosition(J)V

    .line 618
    if-nez v0, :cond_4

    .line 619
    move-object v0, v6

    .line 630
    .end local v3    # "obj":Ljava/lang/Object;
    .end local v5    # "parser":Lorg/icepdf/core/util/Parser;
    .end local v6    # "trailer":Lorg/icepdf/core/pobjects/PTrailer;
    :cond_3
    :goto_0
    if-nez v0, :cond_5

    .line 631
    new-instance v7, Ljava/lang/RuntimeException;

    const-string/jumbo v10, "Could not find document trailer"

    invoke-direct {v7, v10}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 621
    .restart local v3    # "obj":Ljava/lang/Object;
    .restart local v5    # "parser":Lorg/icepdf/core/util/Parser;
    .restart local v6    # "trailer":Lorg/icepdf/core/pobjects/PTrailer;
    :cond_4
    invoke-virtual {v0, v6}, Lorg/icepdf/core/pobjects/PTrailer;->addPreviousTrailer(Lorg/icepdf/core/pobjects/PTrailer;)V

    goto :goto_0

    .line 632
    .end local v3    # "obj":Ljava/lang/Object;
    .end local v5    # "parser":Lorg/icepdf/core/util/Parser;
    .end local v6    # "trailer":Lorg/icepdf/core/pobjects/PTrailer;
    :cond_5
    if-lez v4, :cond_6

    .line 635
    invoke-virtual {v0}, Lorg/icepdf/core/pobjects/PTrailer;->getCrossReferenceTable()Lorg/icepdf/core/pobjects/CrossReference;

    move-result-object v7

    invoke-virtual {v7, v4}, Lorg/icepdf/core/pobjects/CrossReference;->setOffset(I)V

    .line 638
    :cond_6
    new-instance v1, Lorg/icepdf/core/util/LazyObjectLoader;

    iget-object v7, p0, Lorg/icepdf/core/pobjects/Document;->library:Lorg/icepdf/core/util/Library;

    invoke-virtual {v0}, Lorg/icepdf/core/pobjects/PTrailer;->getPrimaryCrossReference()Lorg/icepdf/core/pobjects/CrossReference;

    move-result-object v10

    invoke-direct {v1, v7, p1, v10}, Lorg/icepdf/core/util/LazyObjectLoader;-><init>(Lorg/icepdf/core/util/Library;Lorg/icepdf/core/io/SeekableInput;Lorg/icepdf/core/pobjects/CrossReference;)V

    .line 640
    .local v1, "lol":Lorg/icepdf/core/util/LazyObjectLoader;
    iget-object v7, p0, Lorg/icepdf/core/pobjects/Document;->library:Lorg/icepdf/core/util/Library;

    invoke-virtual {v7, v1}, Lorg/icepdf/core/util/Library;->setLazyObjectLoader(Lorg/icepdf/core/util/LazyObjectLoader;)V

    .line 642
    iput-object v0, p0, Lorg/icepdf/core/pobjects/Document;->pTrailer:Lorg/icepdf/core/pobjects/PTrailer;

    .line 643
    invoke-virtual {v0}, Lorg/icepdf/core/pobjects/PTrailer;->getRootCatalog()Lorg/icepdf/core/pobjects/Catalog;

    move-result-object v7

    iput-object v7, p0, Lorg/icepdf/core/pobjects/Document;->catalog:Lorg/icepdf/core/pobjects/Catalog;

    .line 644
    iget-object v7, p0, Lorg/icepdf/core/pobjects/Document;->library:Lorg/icepdf/core/util/Library;

    iget-object v10, p0, Lorg/icepdf/core/pobjects/Document;->catalog:Lorg/icepdf/core/pobjects/Catalog;

    invoke-virtual {v7, v10}, Lorg/icepdf/core/util/Library;->setCatalog(Lorg/icepdf/core/pobjects/Catalog;)V

    .line 646
    iget-object v7, p0, Lorg/icepdf/core/pobjects/Document;->catalog:Lorg/icepdf/core/pobjects/Catalog;

    if-nez v7, :cond_7

    .line 647
    new-instance v7, Ljava/lang/NullPointerException;

    const-string/jumbo v10, "Loading via xref failed to find catalog"

    invoke-direct {v7, v10}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 650
    :cond_7
    invoke-direct {p0, v0}, Lorg/icepdf/core/pobjects/Document;->makeSecurityManager(Lorg/icepdf/core/pobjects/PTrailer;)Z

    move-result v2

    .line 651
    .local v2, "madeSecurityManager":Z
    if-eqz v2, :cond_8

    .line 652
    invoke-direct {p0}, Lorg/icepdf/core/pobjects/Document;->attemptAuthorizeSecurityManager()V

    .line 653
    :cond_8
    return-void
.end method

.method private makeSecurityManager(Lorg/icepdf/core/pobjects/PTrailer;)Z
    .locals 6
    .param p1, "documentTrailer"    # Lorg/icepdf/core/pobjects/PTrailer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/icepdf/core/exceptions/PDFSecurityException;
        }
    .end annotation

    .prologue
    .line 920
    const/4 v2, 0x0

    .line 921
    .local v2, "madeSecurityManager":Z
    invoke-virtual {p1}, Lorg/icepdf/core/pobjects/PTrailer;->getEncrypt()Ljava/util/Hashtable;

    move-result-object v0

    .line 922
    .local v0, "encryptDictionary":Ljava/util/Hashtable;
    invoke-virtual {p1}, Lorg/icepdf/core/pobjects/PTrailer;->getID()Ljava/util/Vector;

    move-result-object v1

    .line 923
    .local v1, "fileID":Ljava/util/Vector;
    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    .line 925
    iget-object v3, p0, Lorg/icepdf/core/pobjects/Document;->library:Lorg/icepdf/core/util/Library;

    new-instance v4, Lorg/icepdf/core/pobjects/security/SecurityManager;

    iget-object v5, p0, Lorg/icepdf/core/pobjects/Document;->library:Lorg/icepdf/core/util/Library;

    invoke-direct {v4, v5, v0, v1}, Lorg/icepdf/core/pobjects/security/SecurityManager;-><init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;Ljava/util/Vector;)V

    iput-object v4, v3, Lorg/icepdf/core/util/Library;->securityManager:Lorg/icepdf/core/pobjects/security/SecurityManager;

    .line 927
    const/4 v2, 0x1

    .line 929
    :cond_0
    return v2
.end method

.method private setDocumentCachedFilePath(Ljava/lang/String;)V
    .locals 0
    .param p1, "o"    # Ljava/lang/String;

    .prologue
    .line 164
    iput-object p1, p0, Lorg/icepdf/core/pobjects/Document;->cachedFilePath:Ljava/lang/String;

    .line 165
    return-void
.end method

.method private setDocumentOrigin(Ljava/lang/String;)V
    .locals 4
    .param p1, "o"    # Ljava/lang/String;

    .prologue
    .line 148
    iput-object p1, p0, Lorg/icepdf/core/pobjects/Document;->origin:Ljava/lang/String;

    .line 149
    sget-object v0, Lorg/icepdf/core/pobjects/Document;->logger:Ljava/util/logging/Logger;

    sget-object v1, Ljava/util/logging/Level;->CONFIG:Ljava/util/logging/Level;

    invoke-virtual {v0, v1}, Ljava/util/logging/Logger;->isLoggable(Ljava/util/logging/Level;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 150
    sget-object v0, Lorg/icepdf/core/pobjects/Document;->logger:Ljava/util/logging/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "MEMFREE: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Runtime;->freeMemory()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " of "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Runtime;->totalMemory()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/logging/Logger;->config(Ljava/lang/String;)V

    .line 152
    sget-object v0, Lorg/icepdf/core/pobjects/Document;->logger:Ljava/util/logging/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "LOADING: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/logging/Logger;->config(Ljava/lang/String;)V

    .line 154
    :cond_0
    return-void
.end method

.method private setInputStream(Lorg/icepdf/core/io/SeekableInput;)V
    .locals 5
    .param p1, "in"    # Lorg/icepdf/core/io/SeekableInput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/icepdf/core/exceptions/InvalidHeaderException;,
            Lorg/icepdf/core/exceptions/PDFException;,
            Lorg/icepdf/core/exceptions/PDFSecurityException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 507
    :try_start_0
    iput-object p1, p0, Lorg/icepdf/core/pobjects/Document;->documentSeekableInput:Lorg/icepdf/core/io/SeekableInput;

    .line 510
    new-instance v2, Lorg/icepdf/core/util/Library;

    invoke-direct {v2}, Lorg/icepdf/core/util/Library;-><init>()V

    iput-object v2, p0, Lorg/icepdf/core/pobjects/Document;->library:Lorg/icepdf/core/util/Library;
    :try_end_0
    .catch Lorg/icepdf/core/exceptions/PDFException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lorg/icepdf/core/exceptions/PDFSecurityException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_7

    .line 514
    :try_start_1
    invoke-direct {p0, p1}, Lorg/icepdf/core/pobjects/Document;->checkHeader(Lorg/icepdf/core/io/SeekableInput;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lorg/icepdf/core/exceptions/PDFException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lorg/icepdf/core/exceptions/PDFSecurityException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_5

    .line 523
    const/4 v1, 0x0

    .line 525
    .local v1, "loaded":Z
    :try_start_2
    invoke-direct {p0, p1}, Lorg/icepdf/core/pobjects/Document;->loadDocumentViaXRefs(Lorg/icepdf/core/io/SeekableInput;)V
    :try_end_2
    .catch Lorg/icepdf/core/exceptions/PDFException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Lorg/icepdf/core/exceptions/PDFSecurityException; {:try_start_2 .. :try_end_2} :catch_4
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_6
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_5

    .line 526
    const/4 v1, 0x1

    .line 537
    :cond_0
    :goto_0
    if-nez v1, :cond_3

    .line 539
    :try_start_3
    iget-object v2, p0, Lorg/icepdf/core/pobjects/Document;->catalog:Lorg/icepdf/core/pobjects/Catalog;

    if-eqz v2, :cond_1

    .line 540
    iget-object v2, p0, Lorg/icepdf/core/pobjects/Document;->catalog:Lorg/icepdf/core/pobjects/Catalog;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lorg/icepdf/core/pobjects/Catalog;->dispose(Z)V

    .line 541
    const/4 v2, 0x0

    iput-object v2, p0, Lorg/icepdf/core/pobjects/Document;->catalog:Lorg/icepdf/core/pobjects/Catalog;

    .line 543
    :cond_1
    iget-object v2, p0, Lorg/icepdf/core/pobjects/Document;->library:Lorg/icepdf/core/util/Library;

    if-eqz v2, :cond_2

    .line 544
    iget-object v2, p0, Lorg/icepdf/core/pobjects/Document;->library:Lorg/icepdf/core/util/Library;

    invoke-virtual {v2}, Lorg/icepdf/core/util/Library;->dispose()V

    .line 545
    const/4 v2, 0x0

    iput-object v2, p0, Lorg/icepdf/core/pobjects/Document;->library:Lorg/icepdf/core/util/Library;

    .line 547
    :cond_2
    new-instance v2, Lorg/icepdf/core/util/Library;

    invoke-direct {v2}, Lorg/icepdf/core/util/Library;-><init>()V

    iput-object v2, p0, Lorg/icepdf/core/pobjects/Document;->library:Lorg/icepdf/core/util/Library;

    .line 548
    const/4 v2, 0x0

    iput-object v2, p0, Lorg/icepdf/core/pobjects/Document;->pTrailer:Lorg/icepdf/core/pobjects/PTrailer;

    .line 550
    const-wide/16 v2, 0x0

    invoke-interface {p1, v2, v3}, Lorg/icepdf/core/io/SeekableInput;->seekAbsolute(J)V

    .line 551
    invoke-direct {p0, p1}, Lorg/icepdf/core/pobjects/Document;->loadDocumentViaLinearTraversal(Lorg/icepdf/core/io/SeekableInput;)V

    .line 559
    :cond_3
    iget-object v2, p0, Lorg/icepdf/core/pobjects/Document;->catalog:Lorg/icepdf/core/pobjects/Catalog;

    if-eqz v2, :cond_4

    .line 560
    iget-object v2, p0, Lorg/icepdf/core/pobjects/Document;->catalog:Lorg/icepdf/core/pobjects/Catalog;

    invoke-virtual {v2}, Lorg/icepdf/core/pobjects/Catalog;->init()V

    .line 563
    :cond_4
    new-instance v2, Lorg/icepdf/core/pobjects/StateManager;

    iget-object v3, p0, Lorg/icepdf/core/pobjects/Document;->pTrailer:Lorg/icepdf/core/pobjects/PTrailer;

    invoke-direct {v2, v3}, Lorg/icepdf/core/pobjects/StateManager;-><init>(Lorg/icepdf/core/pobjects/PTrailer;)V

    iput-object v2, p0, Lorg/icepdf/core/pobjects/Document;->stateManager:Lorg/icepdf/core/pobjects/StateManager;

    .line 564
    iget-object v2, p0, Lorg/icepdf/core/pobjects/Document;->library:Lorg/icepdf/core/util/Library;

    iget-object v3, p0, Lorg/icepdf/core/pobjects/Document;->stateManager:Lorg/icepdf/core/pobjects/StateManager;

    invoke-virtual {v2, v3}, Lorg/icepdf/core/util/Library;->setStateManager(Lorg/icepdf/core/pobjects/StateManager;)V

    .line 581
    return-void

    .line 516
    .end local v1    # "loaded":Z
    :catch_0
    move-exception v0

    .line 517
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {p0}, Lorg/icepdf/core/pobjects/Document;->dispose()V

    .line 518
    new-instance v2, Lorg/icepdf/core/exceptions/InvalidHeaderException;

    const-string/jumbo v3, "Invalid Header. Unable to parse the document"

    invoke-direct {v2, v3}, Lorg/icepdf/core/exceptions/InvalidHeaderException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_3
    .catch Lorg/icepdf/core/exceptions/PDFException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Lorg/icepdf/core/exceptions/PDFSecurityException; {:try_start_3 .. :try_end_3} :catch_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_5
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_7

    .line 565
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v0

    .line 566
    .local v0, "e":Lorg/icepdf/core/exceptions/PDFException;
    sget-object v2, Lorg/icepdf/core/pobjects/Document;->logger:Ljava/util/logging/Logger;

    sget-object v3, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string/jumbo v4, "Error loading PDF file during linear parse."

    invoke-virtual {v2, v3, v4, v0}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 568
    invoke-virtual {p0}, Lorg/icepdf/core/pobjects/Document;->dispose()V

    .line 569
    throw v0

    .line 527
    .end local v0    # "e":Lorg/icepdf/core/exceptions/PDFException;
    .restart local v1    # "loaded":Z
    :catch_2
    move-exception v0

    .line 528
    .restart local v0    # "e":Lorg/icepdf/core/exceptions/PDFException;
    :try_start_4
    throw v0
    :try_end_4
    .catch Lorg/icepdf/core/exceptions/PDFException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Lorg/icepdf/core/exceptions/PDFSecurityException; {:try_start_4 .. :try_end_4} :catch_3
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_5
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_7

    .line 570
    .end local v0    # "e":Lorg/icepdf/core/exceptions/PDFException;
    .end local v1    # "loaded":Z
    :catch_3
    move-exception v0

    .line 571
    .local v0, "e":Lorg/icepdf/core/exceptions/PDFSecurityException;
    invoke-virtual {p0}, Lorg/icepdf/core/pobjects/Document;->dispose()V

    .line 572
    throw v0

    .line 529
    .end local v0    # "e":Lorg/icepdf/core/exceptions/PDFSecurityException;
    .restart local v1    # "loaded":Z
    :catch_4
    move-exception v0

    .line 530
    .restart local v0    # "e":Lorg/icepdf/core/exceptions/PDFSecurityException;
    :try_start_5
    throw v0
    :try_end_5
    .catch Lorg/icepdf/core/exceptions/PDFException; {:try_start_5 .. :try_end_5} :catch_1
    .catch Lorg/icepdf/core/exceptions/PDFSecurityException; {:try_start_5 .. :try_end_5} :catch_3
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_7

    .line 573
    .end local v0    # "e":Lorg/icepdf/core/exceptions/PDFSecurityException;
    .end local v1    # "loaded":Z
    :catch_5
    move-exception v0

    .line 574
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {p0}, Lorg/icepdf/core/pobjects/Document;->dispose()V

    .line 575
    throw v0

    .line 531
    .end local v0    # "e":Ljava/io/IOException;
    .restart local v1    # "loaded":Z
    :catch_6
    move-exception v0

    .line 532
    .local v0, "e":Ljava/lang/Exception;
    :try_start_6
    sget-object v2, Lorg/icepdf/core/pobjects/Document;->logger:Ljava/util/logging/Logger;

    sget-object v3, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    invoke-virtual {v2, v3}, Ljava/util/logging/Logger;->isLoggable(Ljava/util/logging/Level;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 533
    sget-object v2, Lorg/icepdf/core/pobjects/Document;->logger:Ljava/util/logging/Logger;

    const-string/jumbo v3, "Cross reference deferred loading failed, will fall back to linear reading."

    invoke-virtual {v2, v3}, Ljava/util/logging/Logger;->warning(Ljava/lang/String;)V
    :try_end_6
    .catch Lorg/icepdf/core/exceptions/PDFException; {:try_start_6 .. :try_end_6} :catch_1
    .catch Lorg/icepdf/core/exceptions/PDFSecurityException; {:try_start_6 .. :try_end_6} :catch_3
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_5
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_7

    goto/16 :goto_0

    .line 576
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v1    # "loaded":Z
    :catch_7
    move-exception v0

    .line 577
    .restart local v0    # "e":Ljava/lang/Exception;
    invoke-virtual {p0}, Lorg/icepdf/core/pobjects/Document;->dispose()V

    .line 578
    sget-object v2, Lorg/icepdf/core/pobjects/Document;->logger:Ljava/util/logging/Logger;

    sget-object v3, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    const-string/jumbo v4, "Error loading PDF Document."

    invoke-virtual {v2, v3, v4, v0}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 579
    new-instance v2, Ljava/io/IOException;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method private skipPastAnyPrefixJunk(Lorg/icepdf/core/io/SeekableInput;)I
    .locals 9
    .param p1, "in"    # Lorg/icepdf/core/io/SeekableInput;

    .prologue
    const/16 v8, 0x800

    const/4 v6, 0x0

    .line 872
    invoke-interface {p1}, Lorg/icepdf/core/io/SeekableInput;->markSupported()Z

    move-result v7

    if-nez v7, :cond_1

    move v2, v6

    .line 900
    :cond_0
    :goto_0
    return v2

    .line 875
    :cond_1
    const/16 v5, 0x800

    .line 876
    .local v5, "scanLength":I
    :try_start_0
    const-string/jumbo v3, "%PDF-1."

    .line 877
    .local v3, "scanFor":Ljava/lang/String;
    const/4 v4, 0x0

    .line 878
    .local v4, "scanForIndex":I
    const/16 v7, 0x800

    invoke-interface {p1, v7}, Lorg/icepdf/core/io/SeekableInput;->mark(I)V

    .line 879
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    if-ge v2, v8, :cond_3

    .line 880
    invoke-interface {p1}, Lorg/icepdf/core/io/SeekableInput;->read()I

    move-result v0

    .line 881
    .local v0, "data":I
    if-gez v0, :cond_2

    .line 882
    invoke-interface {p1}, Lorg/icepdf/core/io/SeekableInput;->reset()V

    move v2, v6

    .line 883
    goto :goto_0

    .line 885
    :cond_2
    const-string/jumbo v7, "%PDF-1."

    invoke-virtual {v7, v4}, Ljava/lang/String;->charAt(I)C

    move-result v7

    if-eq v0, v7, :cond_0

    .line 888
    const/4 v4, 0x0

    .line 879
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 893
    .end local v0    # "data":I
    :cond_3
    invoke-interface {p1}, Lorg/icepdf/core/io/SeekableInput;->reset()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .end local v2    # "i":I
    .end local v3    # "scanFor":Ljava/lang/String;
    .end local v4    # "scanForIndex":I
    :goto_2
    move v2, v6

    .line 900
    goto :goto_0

    .line 894
    :catch_0
    move-exception v1

    .line 896
    .local v1, "e":Ljava/io/IOException;
    :try_start_1
    invoke-interface {p1}, Lorg/icepdf/core/io/SeekableInput;->reset()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_2

    .line 897
    :catch_1
    move-exception v7

    goto :goto_2
.end method

.method private skipPastAnyPrefixJunk(Ljava/io/InputStream;)V
    .locals 9
    .param p1, "in"    # Ljava/io/InputStream;

    .prologue
    const/16 v8, 0x800

    .line 822
    invoke-virtual {p1}, Ljava/io/InputStream;->markSupported()Z

    move-result v7

    if-nez v7, :cond_1

    .line 860
    :cond_0
    :goto_0
    return-void

    .line 825
    :cond_1
    const/16 v6, 0x800

    .line 826
    .local v6, "scanLength":I
    :try_start_0
    const-string/jumbo v3, "%PDF-1."

    .line 827
    .local v3, "scanFor":Ljava/lang/String;
    const/4 v4, 0x0

    .line 828
    .local v4, "scanForIndex":I
    const/4 v5, 0x0

    .line 829
    .local v5, "scanForWhiteSpace":Z
    const/16 v7, 0x800

    invoke-virtual {p1, v7}, Ljava/io/InputStream;->mark(I)V

    .line 830
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    if-ge v2, v8, :cond_6

    .line 831
    invoke-virtual {p1}, Ljava/io/InputStream;->read()I

    move-result v0

    .line 832
    .local v0, "data":I
    if-gez v0, :cond_2

    .line 833
    invoke-virtual {p1}, Ljava/io/InputStream;->reset()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 854
    .end local v0    # "data":I
    .end local v2    # "i":I
    .end local v3    # "scanFor":Ljava/lang/String;
    .end local v4    # "scanForIndex":I
    .end local v5    # "scanForWhiteSpace":Z
    :catch_0
    move-exception v1

    .line 856
    .local v1, "e":Ljava/io/IOException;
    :try_start_1
    invoke-virtual {p1}, Ljava/io/InputStream;->reset()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 857
    :catch_1
    move-exception v7

    goto :goto_0

    .line 836
    .end local v1    # "e":Ljava/io/IOException;
    .restart local v0    # "data":I
    .restart local v2    # "i":I
    .restart local v3    # "scanFor":Ljava/lang/String;
    .restart local v4    # "scanForIndex":I
    .restart local v5    # "scanForWhiteSpace":Z
    :cond_2
    if-eqz v5, :cond_4

    .line 837
    int-to-char v7, v0

    :try_start_2
    invoke-static {v7}, Lorg/icepdf/core/util/Parser;->isWhitespace(C)Z

    move-result v7

    if-nez v7, :cond_0

    .line 830
    :cond_3
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 841
    :cond_4
    const-string/jumbo v7, "%PDF-1."

    invoke-virtual {v7, v4}, Ljava/lang/String;->charAt(I)C

    move-result v7

    if-ne v0, v7, :cond_5

    .line 842
    add-int/lit8 v4, v4, 0x1

    .line 843
    const-string/jumbo v7, "%PDF-1."

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    if-ne v4, v7, :cond_3

    .line 845
    const/4 v5, 0x1

    goto :goto_2

    .line 848
    :cond_5
    const/4 v4, 0x0

    goto :goto_2

    .line 853
    .end local v0    # "data":I
    :cond_6
    invoke-virtual {p1}, Ljava/io/InputStream;->reset()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0
.end method


# virtual methods
.method protected appendIncrementalUpdate(Ljava/io/OutputStream;J)J
    .locals 2
    .param p1, "out"    # Ljava/io/OutputStream;
    .param p2, "documentLength"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1219
    invoke-static {p0, p1, p2, p3}, Lorg/icepdf/core/util/IncrementalUpdater;->appendIncrementalUpdate(Lorg/icepdf/core/pobjects/Document;Ljava/io/OutputStream;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public dispose()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 1125
    iget-object v4, p0, Lorg/icepdf/core/pobjects/Document;->catalog:Lorg/icepdf/core/pobjects/Catalog;

    if-eqz v4, :cond_0

    .line 1126
    iget-object v4, p0, Lorg/icepdf/core/pobjects/Document;->catalog:Lorg/icepdf/core/pobjects/Catalog;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lorg/icepdf/core/pobjects/Catalog;->dispose(Z)V

    .line 1127
    iput-object v7, p0, Lorg/icepdf/core/pobjects/Document;->catalog:Lorg/icepdf/core/pobjects/Catalog;

    .line 1129
    :cond_0
    iget-object v4, p0, Lorg/icepdf/core/pobjects/Document;->library:Lorg/icepdf/core/util/Library;

    if-eqz v4, :cond_1

    .line 1130
    iget-object v4, p0, Lorg/icepdf/core/pobjects/Document;->library:Lorg/icepdf/core/util/Library;

    invoke-virtual {v4}, Lorg/icepdf/core/util/Library;->dispose()V

    .line 1131
    iput-object v7, p0, Lorg/icepdf/core/pobjects/Document;->library:Lorg/icepdf/core/util/Library;

    .line 1133
    :cond_1
    iput-object v7, p0, Lorg/icepdf/core/pobjects/Document;->pTrailer:Lorg/icepdf/core/pobjects/PTrailer;

    .line 1134
    iget-object v4, p0, Lorg/icepdf/core/pobjects/Document;->documentSeekableInput:Lorg/icepdf/core/io/SeekableInput;

    if-eqz v4, :cond_2

    .line 1136
    :try_start_0
    iget-object v4, p0, Lorg/icepdf/core/pobjects/Document;->documentSeekableInput:Lorg/icepdf/core/io/SeekableInput;

    invoke-interface {v4}, Lorg/icepdf/core/io/SeekableInput;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1141
    :goto_0
    iput-object v7, p0, Lorg/icepdf/core/pobjects/Document;->documentSeekableInput:Lorg/icepdf/core/io/SeekableInput;

    .line 1143
    :cond_2
    invoke-direct {p0}, Lorg/icepdf/core/pobjects/Document;->getDocumentCachedFilePath()Ljava/lang/String;

    move-result-object v2

    .line 1144
    .local v2, "fileToDelete":Ljava/lang/String;
    if-eqz v2, :cond_3

    .line 1145
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1146
    .local v1, "file":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    move-result v3

    .line 1147
    .local v3, "success":Z
    if-nez v3, :cond_3

    sget-object v4, Lorg/icepdf/core/pobjects/Document;->logger:Ljava/util/logging/Logger;

    sget-object v5, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    invoke-virtual {v4, v5}, Ljava/util/logging/Logger;->isLoggable(Ljava/util/logging/Level;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 1148
    sget-object v4, Lorg/icepdf/core/pobjects/Document;->logger:Ljava/util/logging/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "Error deleting URL cached to file "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/logging/Logger;->warning(Ljava/lang/String;)V

    .line 1152
    .end local v1    # "file":Ljava/io/File;
    .end local v3    # "success":Z
    :cond_3
    return-void

    .line 1137
    .end local v2    # "fileToDelete":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 1138
    .local v0, "e":Ljava/io/IOException;
    sget-object v4, Lorg/icepdf/core/pobjects/Document;->logger:Ljava/util/logging/Logger;

    sget-object v5, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string/jumbo v6, "Error closing document input stream."

    invoke-virtual {v4, v5, v6, v0}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public getCatalog()Lorg/icepdf/core/pobjects/Catalog;
    .locals 1

    .prologue
    .line 1371
    iget-object v0, p0, Lorg/icepdf/core/pobjects/Document;->catalog:Lorg/icepdf/core/pobjects/Catalog;

    return-object v0
.end method

.method public getDocumentLocation()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1053
    iget-object v0, p0, Lorg/icepdf/core/pobjects/Document;->cachedFilePath:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1054
    iget-object v0, p0, Lorg/icepdf/core/pobjects/Document;->cachedFilePath:Ljava/lang/String;

    .line 1055
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lorg/icepdf/core/pobjects/Document;->origin:Ljava/lang/String;

    goto :goto_0
.end method

.method public getDocumentOrigin()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1040
    iget-object v0, p0, Lorg/icepdf/core/pobjects/Document;->origin:Ljava/lang/String;

    return-object v0
.end method

.method public getNumberOfPages()I
    .locals 4

    .prologue
    .line 1076
    :try_start_0
    iget-object v1, p0, Lorg/icepdf/core/pobjects/Document;->catalog:Lorg/icepdf/core/pobjects/Catalog;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lorg/icepdf/core/pobjects/Document;->catalog:Lorg/icepdf/core/pobjects/Catalog;

    invoke-virtual {v1}, Lorg/icepdf/core/pobjects/Catalog;->getPageTree()Lorg/icepdf/core/pobjects/PageTree;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1077
    iget-object v1, p0, Lorg/icepdf/core/pobjects/Document;->catalog:Lorg/icepdf/core/pobjects/Catalog;

    invoke-virtual {v1}, Lorg/icepdf/core/pobjects/Catalog;->getPageTree()Lorg/icepdf/core/pobjects/PageTree;

    move-result-object v1

    invoke-virtual {v1}, Lorg/icepdf/core/pobjects/PageTree;->getNumberOfPages()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1081
    :goto_0
    return v1

    .line 1078
    :catch_0
    move-exception v0

    .line 1079
    .local v0, "e":Ljava/lang/Exception;
    sget-object v1, Lorg/icepdf/core/pobjects/Document;->logger:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string/jumbo v3, "Error getting number of pages."

    invoke-virtual {v1, v2, v3, v0}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1081
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getPageDimension(IF)Lorg/icepdf/core/pobjects/PDimension;
    .locals 3
    .param p1, "pageNumber"    # I
    .param p2, "userRotation"    # F

    .prologue
    .line 994
    iget-object v2, p0, Lorg/icepdf/core/pobjects/Document;->catalog:Lorg/icepdf/core/pobjects/Catalog;

    invoke-virtual {v2}, Lorg/icepdf/core/pobjects/Catalog;->getPageTree()Lorg/icepdf/core/pobjects/PageTree;

    move-result-object v2

    invoke-virtual {v2, p1, p0}, Lorg/icepdf/core/pobjects/PageTree;->getPage(ILjava/lang/Object;)Lorg/icepdf/core/pobjects/Page;

    move-result-object v0

    .line 995
    .local v0, "page":Lorg/icepdf/core/pobjects/Page;
    invoke-virtual {v0, p2}, Lorg/icepdf/core/pobjects/Page;->getSize(F)Lorg/icepdf/core/pobjects/PDimension;

    move-result-object v1

    .line 996
    .local v1, "pd":Lorg/icepdf/core/pobjects/PDimension;
    iget-object v2, p0, Lorg/icepdf/core/pobjects/Document;->catalog:Lorg/icepdf/core/pobjects/Catalog;

    invoke-virtual {v2}, Lorg/icepdf/core/pobjects/Catalog;->getPageTree()Lorg/icepdf/core/pobjects/PageTree;

    move-result-object v2

    invoke-virtual {v2, v0, p0}, Lorg/icepdf/core/pobjects/PageTree;->releasePage(Lorg/icepdf/core/pobjects/Page;Ljava/lang/Object;)V

    .line 997
    return-object v1
.end method

.method public getPageDimension(IFF)Lorg/icepdf/core/pobjects/PDimension;
    .locals 4
    .param p1, "pageNumber"    # I
    .param p2, "userRotation"    # F
    .param p3, "userZoom"    # F

    .prologue
    const/4 v3, 0x0

    .line 1019
    iget-object v2, p0, Lorg/icepdf/core/pobjects/Document;->catalog:Lorg/icepdf/core/pobjects/Catalog;

    invoke-virtual {v2}, Lorg/icepdf/core/pobjects/Catalog;->getPageTree()Lorg/icepdf/core/pobjects/PageTree;

    move-result-object v2

    invoke-virtual {v2, p1, p0}, Lorg/icepdf/core/pobjects/PageTree;->getPage(ILjava/lang/Object;)Lorg/icepdf/core/pobjects/Page;

    move-result-object v0

    .line 1020
    .local v0, "page":Lorg/icepdf/core/pobjects/Page;
    if-eqz v0, :cond_0

    .line 1021
    invoke-virtual {v0, p2, p3}, Lorg/icepdf/core/pobjects/Page;->getSize(FF)Lorg/icepdf/core/pobjects/PDimension;

    move-result-object v1

    .line 1022
    .local v1, "pd":Lorg/icepdf/core/pobjects/PDimension;
    iget-object v2, p0, Lorg/icepdf/core/pobjects/Document;->catalog:Lorg/icepdf/core/pobjects/Catalog;

    invoke-virtual {v2}, Lorg/icepdf/core/pobjects/Catalog;->getPageTree()Lorg/icepdf/core/pobjects/PageTree;

    move-result-object v2

    invoke-virtual {v2, v0, p0}, Lorg/icepdf/core/pobjects/PageTree;->releasePage(Lorg/icepdf/core/pobjects/Page;Ljava/lang/Object;)V

    .line 1025
    .end local v1    # "pd":Lorg/icepdf/core/pobjects/PDimension;
    :goto_0
    return-object v1

    :cond_0
    new-instance v1, Lorg/icepdf/core/pobjects/PDimension;

    invoke-direct {v1, v3, v3}, Lorg/icepdf/core/pobjects/PDimension;-><init>(II)V

    goto :goto_0
.end method

.method public getPageImage(IIIFFLjava/io/File;)Landroid/graphics/Bitmap;
    .locals 14
    .param p1, "pageNumber"    # I
    .param p2, "renderHintType"    # I
    .param p3, "pageBoundary"    # I
    .param p4, "userRotation"    # F
    .param p5, "userZoom"    # F
    .param p6, "folderPath"    # Ljava/io/File;

    .prologue
    .line 1249
    iget-object v5, p0, Lorg/icepdf/core/pobjects/Document;->catalog:Lorg/icepdf/core/pobjects/Catalog;

    invoke-virtual {v5}, Lorg/icepdf/core/pobjects/Catalog;->getPageTree()Lorg/icepdf/core/pobjects/PageTree;

    move-result-object v5

    invoke-virtual {v5, p1, p0}, Lorg/icepdf/core/pobjects/PageTree;->getPage(ILjava/lang/Object;)Lorg/icepdf/core/pobjects/Page;

    move-result-object v3

    .line 1250
    .local v3, "page":Lorg/icepdf/core/pobjects/Page;
    move/from16 v0, p3

    move/from16 v1, p4

    move/from16 v2, p5

    invoke-virtual {v3, v0, v1, v2}, Lorg/icepdf/core/pobjects/Page;->getSize(IFF)Lorg/icepdf/core/pobjects/PDimension;

    move-result-object v13

    .line 1252
    .local v13, "sz":Lorg/icepdf/core/pobjects/PDimension;
    invoke-virtual {v13}, Lorg/icepdf/core/pobjects/PDimension;->getWidth()F

    move-result v5

    float-to-int v12, v5

    .line 1253
    .local v12, "pageWidth":I
    invoke-virtual {v13}, Lorg/icepdf/core/pobjects/PDimension;->getHeight()F

    move-result v5

    float-to-int v11, v5

    .line 1255
    .local v11, "pageHeight":I
    sget-object v5, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v12, v11, v5}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v10

    .line 1258
    .local v10, "image":Landroid/graphics/Bitmap;
    new-instance v4, Landroid/graphics/Canvas;

    invoke-direct {v4, v10}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .local v4, "g":Landroid/graphics/Canvas;
    move/from16 v5, p2

    move/from16 v6, p3

    move/from16 v7, p4

    move/from16 v8, p5

    move-object/from16 v9, p6

    .line 1260
    invoke-virtual/range {v3 .. v9}, Lorg/icepdf/core/pobjects/Page;->paint(Landroid/graphics/Canvas;IIFFLjava/io/File;)V

    .line 1262
    iget-object v5, p0, Lorg/icepdf/core/pobjects/Document;->catalog:Lorg/icepdf/core/pobjects/Catalog;

    invoke-virtual {v5}, Lorg/icepdf/core/pobjects/Catalog;->getPageTree()Lorg/icepdf/core/pobjects/PageTree;

    move-result-object v5

    invoke-virtual {v5, v3, p0}, Lorg/icepdf/core/pobjects/PageTree;->releasePage(Lorg/icepdf/core/pobjects/Page;Ljava/lang/Object;)V

    .line 1263
    return-object v10
.end method

.method public getPageImages(I)Ljava/util/Vector;
    .locals 3
    .param p1, "pageNumber"    # I

    .prologue
    .line 1347
    iget-object v2, p0, Lorg/icepdf/core/pobjects/Document;->catalog:Lorg/icepdf/core/pobjects/Catalog;

    invoke-virtual {v2}, Lorg/icepdf/core/pobjects/Catalog;->getPageTree()Lorg/icepdf/core/pobjects/PageTree;

    move-result-object v2

    invoke-virtual {v2, p1, p0}, Lorg/icepdf/core/pobjects/PageTree;->getPage(ILjava/lang/Object;)Lorg/icepdf/core/pobjects/Page;

    move-result-object v1

    .line 1348
    .local v1, "pg":Lorg/icepdf/core/pobjects/Page;
    invoke-virtual {v1}, Lorg/icepdf/core/pobjects/Page;->getImages()Ljava/util/Vector;

    move-result-object v0

    .line 1349
    .local v0, "images":Ljava/util/Vector;
    iget-object v2, p0, Lorg/icepdf/core/pobjects/Document;->catalog:Lorg/icepdf/core/pobjects/Catalog;

    invoke-virtual {v2}, Lorg/icepdf/core/pobjects/Catalog;->getPageTree()Lorg/icepdf/core/pobjects/PageTree;

    move-result-object v2

    invoke-virtual {v2, v1, p0}, Lorg/icepdf/core/pobjects/PageTree;->releasePage(Lorg/icepdf/core/pobjects/Page;Ljava/lang/Object;)V

    .line 1350
    return-object v0
.end method

.method public getPageText(I)Lorg/icepdf/core/pobjects/graphics/text/PageText;
    .locals 4
    .param p1, "pageNumber"    # I

    .prologue
    .line 1280
    iget-object v3, p0, Lorg/icepdf/core/pobjects/Document;->catalog:Lorg/icepdf/core/pobjects/Catalog;

    invoke-virtual {v3}, Lorg/icepdf/core/pobjects/Catalog;->getPageTree()Lorg/icepdf/core/pobjects/PageTree;

    move-result-object v0

    .line 1281
    .local v0, "pageTree":Lorg/icepdf/core/pobjects/PageTree;
    if-ltz p1, :cond_0

    invoke-virtual {v0}, Lorg/icepdf/core/pobjects/PageTree;->getNumberOfPages()I

    move-result v3

    if-ge p1, v3, :cond_0

    .line 1282
    invoke-virtual {v0, p1, p0}, Lorg/icepdf/core/pobjects/PageTree;->getPage(ILjava/lang/Object;)Lorg/icepdf/core/pobjects/Page;

    move-result-object v1

    .line 1283
    .local v1, "pg":Lorg/icepdf/core/pobjects/Page;
    invoke-virtual {v1}, Lorg/icepdf/core/pobjects/Page;->getText()Lorg/icepdf/core/pobjects/graphics/text/PageText;

    move-result-object v2

    .line 1284
    .local v2, "text":Lorg/icepdf/core/pobjects/graphics/text/PageText;
    iget-object v3, p0, Lorg/icepdf/core/pobjects/Document;->catalog:Lorg/icepdf/core/pobjects/Catalog;

    invoke-virtual {v3}, Lorg/icepdf/core/pobjects/Catalog;->getPageTree()Lorg/icepdf/core/pobjects/PageTree;

    move-result-object v3

    invoke-virtual {v3, v1, p0}, Lorg/icepdf/core/pobjects/PageTree;->releasePage(Lorg/icepdf/core/pobjects/Page;Ljava/lang/Object;)V

    .line 1287
    .end local v1    # "pg":Lorg/icepdf/core/pobjects/Page;
    .end local v2    # "text":Lorg/icepdf/core/pobjects/graphics/text/PageText;
    :goto_0
    return-object v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public getPageTree()Lorg/icepdf/core/pobjects/PageTree;
    .locals 1

    .prologue
    .line 1361
    iget-object v0, p0, Lorg/icepdf/core/pobjects/Document;->catalog:Lorg/icepdf/core/pobjects/Catalog;

    invoke-virtual {v0}, Lorg/icepdf/core/pobjects/Catalog;->getPageTree()Lorg/icepdf/core/pobjects/PageTree;

    move-result-object v0

    return-object v0
.end method

.method public getPageViewText(I)Lorg/icepdf/core/pobjects/graphics/text/PageText;
    .locals 4
    .param p1, "pageNumber"    # I

    .prologue
    .line 1303
    iget-object v3, p0, Lorg/icepdf/core/pobjects/Document;->catalog:Lorg/icepdf/core/pobjects/Catalog;

    invoke-virtual {v3}, Lorg/icepdf/core/pobjects/Catalog;->getPageTree()Lorg/icepdf/core/pobjects/PageTree;

    move-result-object v0

    .line 1304
    .local v0, "pageTree":Lorg/icepdf/core/pobjects/PageTree;
    if-ltz p1, :cond_0

    invoke-virtual {v0}, Lorg/icepdf/core/pobjects/PageTree;->getNumberOfPages()I

    move-result v3

    if-ge p1, v3, :cond_0

    .line 1305
    invoke-virtual {v0, p1, p0}, Lorg/icepdf/core/pobjects/PageTree;->getPage(ILjava/lang/Object;)Lorg/icepdf/core/pobjects/Page;

    move-result-object v1

    .line 1306
    .local v1, "pg":Lorg/icepdf/core/pobjects/Page;
    invoke-virtual {v1}, Lorg/icepdf/core/pobjects/Page;->getViewText()Lorg/icepdf/core/pobjects/graphics/text/PageText;

    move-result-object v2

    .line 1307
    .local v2, "text":Lorg/icepdf/core/pobjects/graphics/text/PageText;
    iget-object v3, p0, Lorg/icepdf/core/pobjects/Document;->catalog:Lorg/icepdf/core/pobjects/Catalog;

    invoke-virtual {v3}, Lorg/icepdf/core/pobjects/Catalog;->getPageTree()Lorg/icepdf/core/pobjects/PageTree;

    move-result-object v3

    invoke-virtual {v3, v1, p0}, Lorg/icepdf/core/pobjects/PageTree;->releasePage(Lorg/icepdf/core/pobjects/Page;Ljava/lang/Object;)V

    .line 1310
    .end local v1    # "pg":Lorg/icepdf/core/pobjects/Page;
    .end local v2    # "text":Lorg/icepdf/core/pobjects/graphics/text/PageText;
    :goto_0
    return-object v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public getSecurityManager()Lorg/icepdf/core/pobjects/security/SecurityManager;
    .locals 1

    .prologue
    .line 1321
    iget-object v0, p0, Lorg/icepdf/core/pobjects/Document;->library:Lorg/icepdf/core/util/Library;

    iget-object v0, v0, Lorg/icepdf/core/util/Library;->securityManager:Lorg/icepdf/core/pobjects/security/SecurityManager;

    return-object v0
.end method

.method public getStateManager()Lorg/icepdf/core/pobjects/StateManager;
    .locals 1

    .prologue
    .line 1065
    iget-object v0, p0, Lorg/icepdf/core/pobjects/Document;->stateManager:Lorg/icepdf/core/pobjects/StateManager;

    return-object v0
.end method

.method public paintPage(ILandroid/graphics/Canvas;IIFF)V
    .locals 7
    .param p1, "pageNumber"    # I
    .param p2, "g"    # Landroid/graphics/Canvas;
    .param p3, "renderHintType"    # I
    .param p4, "pageBoundary"    # I
    .param p5, "userRotation"    # F
    .param p6, "userZoom"    # F

    .prologue
    .line 1107
    iget-object v2, p0, Lorg/icepdf/core/pobjects/Document;->catalog:Lorg/icepdf/core/pobjects/Catalog;

    invoke-virtual {v2}, Lorg/icepdf/core/pobjects/Catalog;->getPageTree()Lorg/icepdf/core/pobjects/PageTree;

    move-result-object v2

    invoke-virtual {v2, p1, p0}, Lorg/icepdf/core/pobjects/PageTree;->getPage(ILjava/lang/Object;)Lorg/icepdf/core/pobjects/Page;

    move-result-object v0

    .line 1114
    .local v0, "page":Lorg/icepdf/core/pobjects/Page;
    move-object v1, p2

    .line 1116
    .local v1, "gg":Landroid/graphics/Canvas;
    const/4 v6, 0x0

    move v2, p3

    move v3, p4

    move v4, p5

    move v5, p6

    invoke-virtual/range {v0 .. v6}, Lorg/icepdf/core/pobjects/Page;->paint(Landroid/graphics/Canvas;IIFFLjava/io/File;)V

    .line 1118
    iget-object v2, p0, Lorg/icepdf/core/pobjects/Document;->catalog:Lorg/icepdf/core/pobjects/Catalog;

    invoke-virtual {v2}, Lorg/icepdf/core/pobjects/Catalog;->getPageTree()Lorg/icepdf/core/pobjects/PageTree;

    move-result-object v2

    invoke-virtual {v2, v0, p0}, Lorg/icepdf/core/pobjects/PageTree;->releasePage(Lorg/icepdf/core/pobjects/Page;Ljava/lang/Object;)V

    .line 1119
    return-void
.end method

.method public saveToOutputStream(Ljava/io/OutputStream;)J
    .locals 6
    .param p1, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1201
    invoke-virtual {p0, p1}, Lorg/icepdf/core/pobjects/Document;->writeToOutputStream(Ljava/io/OutputStream;)J

    move-result-wide v2

    .line 1202
    .local v2, "documentLength":J
    invoke-virtual {p0, p1, v2, v3}, Lorg/icepdf/core/pobjects/Document;->appendIncrementalUpdate(Ljava/io/OutputStream;J)J

    move-result-wide v0

    .line 1203
    .local v0, "appendedLength":J
    add-long v4, v2, v0

    return-wide v4
.end method

.method public setByteArray([BIILjava/lang/String;)V
    .locals 7
    .param p1, "data"    # [B
    .param p2, "offset"    # I
    .param p3, "length"    # I
    .param p4, "pathOrURL"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/icepdf/core/exceptions/InvalidHeaderException;,
            Lorg/icepdf/core/exceptions/PDFException;,
            Lorg/icepdf/core/exceptions/PDFSecurityException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 377
    invoke-direct {p0, p4}, Lorg/icepdf/core/pobjects/Document;->setDocumentOrigin(Ljava/lang/String;)V

    .line 379
    sget-boolean v5, Lorg/icepdf/core/pobjects/Document;->isCachingEnabled:Z

    if-nez v5, :cond_0

    .line 381
    new-instance v0, Lorg/icepdf/core/io/SeekableByteArrayInputStream;

    invoke-direct {v0, p1, p2, p3}, Lorg/icepdf/core/io/SeekableByteArrayInputStream;-><init>([BII)V

    .line 383
    .local v0, "byteArrayInputStream":Lorg/icepdf/core/io/SeekableByteArrayInputStream;
    invoke-direct {p0, v0}, Lorg/icepdf/core/pobjects/Document;->setInputStream(Lorg/icepdf/core/io/SeekableInput;)V

    .line 421
    .end local v0    # "byteArrayInputStream":Lorg/icepdf/core/io/SeekableByteArrayInputStream;
    :goto_0
    return-void

    .line 390
    :cond_0
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "ICEpdfTempFile"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->hashCode()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v6, ".tmp"

    invoke-static {v5, v6}, Ljava/io/File;->createTempFile(Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object v4

    .line 393
    .local v4, "tempFile":Ljava/io/File;
    invoke-virtual {v4}, Ljava/io/File;->deleteOnExit()V

    .line 397
    const/4 v1, 0x0

    .line 399
    .local v1, "fileOutputStream":Ljava/io/FileOutputStream;
    :try_start_0
    new-instance v2, Ljava/io/FileOutputStream;

    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x1

    invoke-direct {v2, v5, v6}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 404
    .end local v1    # "fileOutputStream":Ljava/io/FileOutputStream;
    .local v2, "fileOutputStream":Ljava/io/FileOutputStream;
    :try_start_1
    invoke-virtual {v2, p1, p2, p3}, Ljava/io/FileOutputStream;->write([BII)V

    .line 406
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->flush()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 408
    if-eqz v2, :cond_1

    .line 409
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V

    .line 414
    :cond_1
    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v5}, Lorg/icepdf/core/pobjects/Document;->setDocumentCachedFilePath(Ljava/lang/String;)V

    .line 417
    invoke-static {v4}, Lorg/icepdf/core/io/RandomAccessFileInputStream;->build(Ljava/io/File;)Lorg/icepdf/core/io/RandomAccessFileInputStream;

    move-result-object v3

    .line 419
    .local v3, "rafis":Lorg/icepdf/core/io/RandomAccessFileInputStream;
    invoke-direct {p0, v3}, Lorg/icepdf/core/pobjects/Document;->setInputStream(Lorg/icepdf/core/io/SeekableInput;)V

    goto :goto_0

    .line 408
    .end local v2    # "fileOutputStream":Ljava/io/FileOutputStream;
    .end local v3    # "rafis":Lorg/icepdf/core/io/RandomAccessFileInputStream;
    .restart local v1    # "fileOutputStream":Ljava/io/FileOutputStream;
    :catchall_0
    move-exception v5

    :goto_1
    if-eqz v1, :cond_2

    .line 409
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V

    :cond_2
    throw v5

    .line 408
    .end local v1    # "fileOutputStream":Ljava/io/FileOutputStream;
    .restart local v2    # "fileOutputStream":Ljava/io/FileOutputStream;
    :catchall_1
    move-exception v5

    move-object v1, v2

    .end local v2    # "fileOutputStream":Ljava/io/FileOutputStream;
    .restart local v1    # "fileOutputStream":Ljava/io/FileOutputStream;
    goto :goto_1
.end method

.method public setFile(Ljava/lang/String;)V
    .locals 2
    .param p1, "filepath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/icepdf/core/exceptions/InvalidHeaderException;,
            Lorg/icepdf/core/exceptions/PDFException;,
            Lorg/icepdf/core/exceptions/PDFSecurityException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 191
    invoke-direct {p0, p1}, Lorg/icepdf/core/pobjects/Document;->setDocumentOrigin(Ljava/lang/String;)V

    .line 192
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Lorg/icepdf/core/io/RandomAccessFileInputStream;->build(Ljava/io/File;)Lorg/icepdf/core/io/RandomAccessFileInputStream;

    move-result-object v0

    .line 207
    .local v0, "rafis":Lorg/icepdf/core/io/RandomAccessFileInputStream;
    invoke-direct {p0, v0}, Lorg/icepdf/core/pobjects/Document;->setInputStream(Lorg/icepdf/core/io/SeekableInput;)V

    .line 208
    return-void
.end method

.method public setInputStream(Ljava/io/InputStream;Ljava/lang/String;)V
    .locals 13
    .param p1, "in"    # Ljava/io/InputStream;
    .param p2, "pathOrURL"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/icepdf/core/exceptions/InvalidHeaderException;,
            Lorg/icepdf/core/exceptions/PDFException;,
            Lorg/icepdf/core/exceptions/PDFSecurityException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v12, 0x1000

    .line 266
    invoke-direct {p0, p2}, Lorg/icepdf/core/pobjects/Document;->setDocumentOrigin(Ljava/lang/String;)V

    .line 268
    sget-boolean v10, Lorg/icepdf/core/pobjects/Document;->isCachingEnabled:Z

    if-nez v10, :cond_3

    .line 272
    new-instance v2, Lorg/icepdf/core/io/ConservativeSizingByteArrayOutputStream;

    const v10, 0x19000

    const/4 v11, 0x0

    invoke-direct {v2, v10, v11}, Lorg/icepdf/core/io/ConservativeSizingByteArrayOutputStream;-><init>(ILorg/icepdf/core/util/MemoryManager;)V

    .line 276
    .local v2, "byteArrayOutputStream":Lorg/icepdf/core/io/ConservativeSizingByteArrayOutputStream;
    new-array v0, v12, [B

    .line 281
    .local v0, "buffer":[B
    :goto_0
    const/4 v10, 0x0

    :try_start_0
    array-length v11, v0

    invoke-virtual {p1, v0, v10, v11}, Ljava/io/InputStream;->read([BII)I

    move-result v6

    .local v6, "length":I
    if-lez v6, :cond_1

    .line 282
    const/4 v10, 0x0

    invoke-virtual {v2, v0, v10, v6}, Lorg/icepdf/core/io/ConservativeSizingByteArrayOutputStream;->write([BII)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 297
    .end local v6    # "length":I
    :catch_0
    move-exception v10

    .line 301
    if-eqz v2, :cond_0

    .line 302
    invoke-virtual {v2}, Lorg/icepdf/core/io/ConservativeSizingByteArrayOutputStream;->close()V

    .line 348
    .end local v2    # "byteArrayOutputStream":Lorg/icepdf/core/io/ConservativeSizingByteArrayOutputStream;
    :cond_0
    :goto_1
    return-void

    .line 285
    .restart local v2    # "byteArrayOutputStream":Lorg/icepdf/core/io/ConservativeSizingByteArrayOutputStream;
    .restart local v6    # "length":I
    :cond_1
    :try_start_1
    invoke-virtual {v2}, Lorg/icepdf/core/io/ConservativeSizingByteArrayOutputStream;->flush()V

    .line 287
    invoke-virtual {v2}, Lorg/icepdf/core/io/ConservativeSizingByteArrayOutputStream;->size()I

    move-result v8

    .line 288
    .local v8, "size":I
    invoke-virtual {v2}, Lorg/icepdf/core/io/ConservativeSizingByteArrayOutputStream;->trim()Z

    .line 289
    invoke-virtual {v2}, Lorg/icepdf/core/io/ConservativeSizingByteArrayOutputStream;->relinquishByteArray()[B

    move-result-object v3

    .line 294
    .local v3, "data":[B
    new-instance v1, Lorg/icepdf/core/io/SeekableByteArrayInputStream;

    const/4 v10, 0x0

    invoke-direct {v1, v3, v10, v8}, Lorg/icepdf/core/io/SeekableByteArrayInputStream;-><init>([BII)V

    .line 296
    .local v1, "byteArrayInputStream":Lorg/icepdf/core/io/SeekableByteArrayInputStream;
    invoke-direct {p0, v1}, Lorg/icepdf/core/pobjects/Document;->setInputStream(Lorg/icepdf/core/io/SeekableInput;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 301
    if-eqz v2, :cond_0

    .line 302
    invoke-virtual {v2}, Lorg/icepdf/core/io/ConservativeSizingByteArrayOutputStream;->close()V

    goto :goto_1

    .line 301
    .end local v1    # "byteArrayInputStream":Lorg/icepdf/core/io/SeekableByteArrayInputStream;
    .end local v3    # "data":[B
    .end local v6    # "length":I
    .end local v8    # "size":I
    :catchall_0
    move-exception v10

    if-eqz v2, :cond_2

    .line 302
    invoke-virtual {v2}, Lorg/icepdf/core/io/ConservativeSizingByteArrayOutputStream;->close()V

    :cond_2
    throw v10

    .line 311
    .end local v0    # "buffer":[B
    .end local v2    # "byteArrayOutputStream":Lorg/icepdf/core/io/ConservativeSizingByteArrayOutputStream;
    :cond_3
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "ICEpdfTempFile"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/Object;->hashCode()I

    move-result v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    const-string/jumbo v11, ".tmp"

    invoke-static {v10, v11}, Ljava/io/File;->createTempFile(Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object v9

    .line 314
    .local v9, "tempFile":Ljava/io/File;
    invoke-virtual {v9}, Ljava/io/File;->deleteOnExit()V

    .line 317
    const/4 v4, 0x0

    .line 320
    .local v4, "fileOutputStream":Ljava/io/FileOutputStream;
    :try_start_2
    new-instance v5, Ljava/io/FileOutputStream;

    invoke-virtual {v9}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x1

    invoke-direct {v5, v10, v11}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;Z)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 324
    .end local v4    # "fileOutputStream":Ljava/io/FileOutputStream;
    .local v5, "fileOutputStream":Ljava/io/FileOutputStream;
    const/16 v10, 0x1000

    :try_start_3
    new-array v0, v10, [B

    .line 327
    .restart local v0    # "buffer":[B
    :goto_2
    const/4 v10, 0x0

    array-length v11, v0

    invoke-virtual {p1, v0, v10, v11}, Ljava/io/InputStream;->read([BII)I

    move-result v6

    .restart local v6    # "length":I
    if-lez v6, :cond_5

    .line 328
    const/4 v10, 0x0

    invoke-virtual {v5, v0, v10, v6}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_2

    .line 334
    .end local v0    # "buffer":[B
    .end local v6    # "length":I
    :catchall_1
    move-exception v10

    move-object v4, v5

    .end local v5    # "fileOutputStream":Ljava/io/FileOutputStream;
    .restart local v4    # "fileOutputStream":Ljava/io/FileOutputStream;
    :goto_3
    if-eqz v4, :cond_4

    .line 335
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V

    :cond_4
    throw v10

    .line 331
    .end local v4    # "fileOutputStream":Ljava/io/FileOutputStream;
    .restart local v0    # "buffer":[B
    .restart local v5    # "fileOutputStream":Ljava/io/FileOutputStream;
    .restart local v6    # "length":I
    :cond_5
    :try_start_4
    invoke-virtual {v5}, Ljava/io/FileOutputStream;->flush()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 334
    if-eqz v5, :cond_6

    .line 335
    invoke-virtual {v5}, Ljava/io/FileOutputStream;->close()V

    .line 341
    :cond_6
    invoke-virtual {v9}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v10

    invoke-direct {p0, v10}, Lorg/icepdf/core/pobjects/Document;->setDocumentCachedFilePath(Ljava/lang/String;)V

    .line 344
    invoke-static {v9}, Lorg/icepdf/core/io/RandomAccessFileInputStream;->build(Ljava/io/File;)Lorg/icepdf/core/io/RandomAccessFileInputStream;

    move-result-object v7

    .line 346
    .local v7, "rafis":Lorg/icepdf/core/io/RandomAccessFileInputStream;
    invoke-direct {p0, v7}, Lorg/icepdf/core/pobjects/Document;->setInputStream(Lorg/icepdf/core/io/SeekableInput;)V

    goto/16 :goto_1

    .line 334
    .end local v0    # "buffer":[B
    .end local v5    # "fileOutputStream":Ljava/io/FileOutputStream;
    .end local v6    # "length":I
    .end local v7    # "rafis":Lorg/icepdf/core/io/RandomAccessFileInputStream;
    .restart local v4    # "fileOutputStream":Ljava/io/FileOutputStream;
    :catchall_2
    move-exception v10

    goto :goto_3
.end method

.method public setInputStream(Lorg/icepdf/core/io/SeekableInput;Ljava/lang/String;)V
    .locals 0
    .param p1, "in"    # Lorg/icepdf/core/io/SeekableInput;
    .param p2, "pathOrURL"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/icepdf/core/exceptions/InvalidHeaderException;,
            Lorg/icepdf/core/exceptions/PDFException;,
            Lorg/icepdf/core/exceptions/PDFSecurityException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 441
    invoke-direct {p0, p2}, Lorg/icepdf/core/pobjects/Document;->setDocumentOrigin(Ljava/lang/String;)V

    .line 442
    invoke-direct {p0, p1}, Lorg/icepdf/core/pobjects/Document;->setInputStream(Lorg/icepdf/core/io/SeekableInput;)V

    .line 443
    return-void
.end method

.method public setSecurityCallback(Lorg/icepdf/core/SecurityCallback;)V
    .locals 0
    .param p1, "securityCallback"    # Lorg/icepdf/core/SecurityCallback;

    .prologue
    .line 1333
    iput-object p1, p0, Lorg/icepdf/core/pobjects/Document;->securityCallback:Lorg/icepdf/core/SecurityCallback;

    .line 1334
    return-void
.end method

.method public setUrl(Ljava/net/URL;)V
    .locals 4
    .param p1, "url"    # Ljava/net/URL;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/icepdf/core/exceptions/InvalidHeaderException;,
            Lorg/icepdf/core/exceptions/PDFException;,
            Lorg/icepdf/core/exceptions/PDFSecurityException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 228
    const/4 v0, 0x0

    .line 231
    .local v0, "in":Ljava/io/InputStream;
    :try_start_0
    invoke-virtual {p1}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v2

    .line 234
    .local v2, "urlConnection":Ljava/net/URLConnection;
    invoke-virtual {v2}, Ljava/net/URLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v0

    .line 236
    invoke-virtual {p1}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object v1

    .line 238
    .local v1, "pathOrURL":Ljava/lang/String;
    invoke-virtual {p0, v0, v1}, Lorg/icepdf/core/pobjects/Document;->setInputStream(Ljava/io/InputStream;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 240
    if-eqz v0, :cond_0

    .line 241
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    .line 244
    :cond_0
    return-void

    .line 240
    .end local v1    # "pathOrURL":Ljava/lang/String;
    .end local v2    # "urlConnection":Ljava/net/URLConnection;
    :catchall_0
    move-exception v3

    if-eqz v0, :cond_1

    .line 241
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    :cond_1
    throw v3
.end method

.method public writeToOutputStream(Ljava/io/OutputStream;)J
    .locals 10
    .param p1, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 1166
    iget-object v1, p0, Lorg/icepdf/core/pobjects/Document;->documentSeekableInput:Lorg/icepdf/core/io/SeekableInput;

    invoke-interface {v1}, Lorg/icepdf/core/io/SeekableInput;->getLength()J

    move-result-wide v4

    .line 1167
    .local v4, "documentLength":J
    new-instance v0, Lorg/icepdf/core/io/SeekableInputConstrainedWrapper;

    iget-object v1, p0, Lorg/icepdf/core/pobjects/Document;->documentSeekableInput:Lorg/icepdf/core/io/SeekableInput;

    const-wide/16 v2, 0x0

    invoke-direct/range {v0 .. v6}, Lorg/icepdf/core/io/SeekableInputConstrainedWrapper;-><init>(Lorg/icepdf/core/io/SeekableInput;JJZ)V

    .line 1170
    .local v0, "wrapper":Lorg/icepdf/core/io/SeekableInputConstrainedWrapper;
    :try_start_0
    invoke-virtual {v0}, Lorg/icepdf/core/io/SeekableInputConstrainedWrapper;->prepareForCurrentUse()V

    .line 1172
    const/16 v1, 0x1000

    new-array v7, v1, [B

    .line 1174
    .local v7, "buffer":[B
    :goto_0
    const/4 v1, 0x0

    array-length v2, v7

    invoke-virtual {v0, v7, v1, v2}, Lorg/icepdf/core/io/SeekableInputConstrainedWrapper;->read([BII)I

    move-result v9

    .local v9, "length":I
    if-lez v9, :cond_0

    .line 1175
    const/4 v1, 0x0

    invoke-virtual {p1, v7, v1, v9}, Ljava/io/OutputStream;->write([BII)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1177
    .end local v7    # "buffer":[B
    .end local v9    # "length":I
    :catch_0
    move-exception v8

    .line 1178
    .local v8, "e":Ljava/lang/Throwable;
    :try_start_1
    sget-object v1, Lorg/icepdf/core/pobjects/Document;->logger:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string/jumbo v3, "Error writting PDF output stream."

    invoke-virtual {v1, v2, v3, v8}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1179
    new-instance v1, Ljava/io/IOException;

    invoke-virtual {v8}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1181
    .end local v8    # "e":Ljava/lang/Throwable;
    :catchall_0
    move-exception v1

    .line 1182
    :try_start_2
    invoke-virtual {v0}, Lorg/icepdf/core/io/SeekableInputConstrainedWrapper;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    .line 1184
    :goto_1
    throw v1

    .line 1182
    .restart local v7    # "buffer":[B
    .restart local v9    # "length":I
    :cond_0
    :try_start_3
    invoke-virtual {v0}, Lorg/icepdf/core/io/SeekableInputConstrainedWrapper;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    .line 1186
    :goto_2
    return-wide v4

    .line 1183
    :catch_1
    move-exception v1

    goto :goto_2

    .end local v7    # "buffer":[B
    .end local v9    # "length":I
    :catch_2
    move-exception v2

    goto :goto_1
.end method
