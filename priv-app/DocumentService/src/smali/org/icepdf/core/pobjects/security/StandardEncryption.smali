.class Lorg/icepdf/core/pobjects/security/StandardEncryption;
.super Ljava/lang/Object;
.source "StandardEncryption.java"


# static fields
.field private static final AES_sAIT:[B

.field public static final ENCRYPTION_TYPE_AES_V2:Ljava/lang/String; = "AESV2"

.field public static final ENCRYPTION_TYPE_NONE:Ljava/lang/String; = "None"

.field public static final ENCRYPTION_TYPE_V2:Ljava/lang/String; = "V2"

.field public static final ENCRYPTION_TYPE_V3:Ljava/lang/String; = "V3"

.field private static final PADDING:[B

.field private static final logger:Ljava/util/logging/Logger;


# instance fields
.field private encryptionDictionary:Lorg/icepdf/core/pobjects/security/EncryptionDictionary;

.field private encryptionKey:[B

.field private objectReference:Lorg/icepdf/core/pobjects/Reference;

.field private ownerPassword:Ljava/lang/String;

.field private rc4Key:[B

.field private userPassword:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 45
    const-class v0, Lorg/icepdf/core/pobjects/security/StandardEncryption;

    invoke-virtual {v0}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Lorg/icepdf/core/pobjects/security/StandardEncryption;->logger:Ljava/util/logging/Logger;

    .line 78
    const/16 v0, 0x20

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    sput-object v0, Lorg/icepdf/core/pobjects/security/StandardEncryption;->PADDING:[B

    .line 91
    const/4 v0, 0x4

    new-array v0, v0, [B

    fill-array-data v0, :array_1

    sput-object v0, Lorg/icepdf/core/pobjects/security/StandardEncryption;->AES_sAIT:[B

    return-void

    .line 78
    :array_0
    .array-data 1
        0x28t
        -0x41t
        0x4et
        0x5et
        0x4et
        0x75t
        -0x76t
        0x41t
        0x64t
        0x0t
        0x4et
        0x56t
        -0x1t
        -0x6t
        0x1t
        0x8t
        0x2et
        0x2et
        0x0t
        -0x4at
        -0x30t
        0x68t
        0x3et
        -0x80t
        0x2ft
        0xct
        -0x57t
        -0x2t
        0x64t
        0x53t
        0x69t
        0x7at
    .end array-data

    .line 91
    :array_1
    .array-data 1
        0x73t
        0x41t
        0x6ct
        0x54t
    .end array-data
.end method

.method public constructor <init>(Lorg/icepdf/core/pobjects/security/EncryptionDictionary;)V
    .locals 1
    .param p1, "encryptionDictionary"    # Lorg/icepdf/core/pobjects/security/EncryptionDictionary;

    .prologue
    .line 121
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 108
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/icepdf/core/pobjects/security/StandardEncryption;->rc4Key:[B

    .line 111
    const-string/jumbo v0, ""

    iput-object v0, p0, Lorg/icepdf/core/pobjects/security/StandardEncryption;->userPassword:Ljava/lang/String;

    .line 114
    const-string/jumbo v0, ""

    iput-object v0, p0, Lorg/icepdf/core/pobjects/security/StandardEncryption;->ownerPassword:Ljava/lang/String;

    .line 122
    iput-object p1, p0, Lorg/icepdf/core/pobjects/security/StandardEncryption;->encryptionDictionary:Lorg/icepdf/core/pobjects/security/EncryptionDictionary;

    .line 123
    return-void
.end method

.method private static AES256CBC([B[B)[B
    .locals 8
    .param p0, "intermediateKey"    # [B
    .param p1, "encryptedString"    # [B

    .prologue
    .line 1142
    const/4 v2, 0x0

    .line 1145
    .local v2, "finalData":[B
    :try_start_0
    new-instance v4, Ljavax/crypto/spec/SecretKeySpec;

    const-string/jumbo v5, "AES"

    invoke-direct {v4, p0, v5}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    .line 1146
    .local v4, "key":Ljavax/crypto/spec/SecretKeySpec;
    const-string/jumbo v5, "AES/CBC/NoPadding"

    invoke-static {v5}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v0

    .line 1148
    .local v0, "aes":Ljavax/crypto/Cipher;
    new-instance v3, Ljavax/crypto/spec/IvParameterSpec;

    const/16 v5, 0x10

    new-array v5, v5, [B

    invoke-direct {v3, v5}, Ljavax/crypto/spec/IvParameterSpec;-><init>([B)V

    .line 1151
    .local v3, "iVParameterSpec":Ljavax/crypto/spec/IvParameterSpec;
    const/4 v5, 0x2

    invoke-virtual {v0, v5, v4, v3}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;Ljava/security/spec/AlgorithmParameterSpec;)V

    .line 1153
    invoke-virtual {v0, p1}, Ljavax/crypto/Cipher;->doFinal([B)[B
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljavax/crypto/IllegalBlockSizeException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljavax/crypto/BadPaddingException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljavax/crypto/NoSuchPaddingException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/security/InvalidKeyException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/security/InvalidAlgorithmParameterException; {:try_start_0 .. :try_end_0} :catch_5

    move-result-object v2

    .line 1167
    .end local v0    # "aes":Ljavax/crypto/Cipher;
    .end local v3    # "iVParameterSpec":Ljavax/crypto/spec/IvParameterSpec;
    .end local v4    # "key":Ljavax/crypto/spec/SecretKeySpec;
    :goto_0
    return-object v2

    .line 1154
    :catch_0
    move-exception v1

    .line 1155
    .local v1, "ex":Ljava/security/NoSuchAlgorithmException;
    sget-object v5, Lorg/icepdf/core/pobjects/security/StandardEncryption;->logger:Ljava/util/logging/Logger;

    sget-object v6, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string/jumbo v7, "NoSuchAlgorithmException."

    invoke-virtual {v5, v6, v7, v1}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 1156
    .end local v1    # "ex":Ljava/security/NoSuchAlgorithmException;
    :catch_1
    move-exception v1

    .line 1157
    .local v1, "ex":Ljavax/crypto/IllegalBlockSizeException;
    sget-object v5, Lorg/icepdf/core/pobjects/security/StandardEncryption;->logger:Ljava/util/logging/Logger;

    sget-object v6, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string/jumbo v7, "IllegalBlockSizeException."

    invoke-virtual {v5, v6, v7, v1}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 1158
    .end local v1    # "ex":Ljavax/crypto/IllegalBlockSizeException;
    :catch_2
    move-exception v1

    .line 1159
    .local v1, "ex":Ljavax/crypto/BadPaddingException;
    sget-object v5, Lorg/icepdf/core/pobjects/security/StandardEncryption;->logger:Ljava/util/logging/Logger;

    sget-object v6, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string/jumbo v7, "BadPaddingException."

    invoke-virtual {v5, v6, v7, v1}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 1160
    .end local v1    # "ex":Ljavax/crypto/BadPaddingException;
    :catch_3
    move-exception v1

    .line 1161
    .local v1, "ex":Ljavax/crypto/NoSuchPaddingException;
    sget-object v5, Lorg/icepdf/core/pobjects/security/StandardEncryption;->logger:Ljava/util/logging/Logger;

    sget-object v6, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string/jumbo v7, "NoSuchPaddingException."

    invoke-virtual {v5, v6, v7, v1}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 1162
    .end local v1    # "ex":Ljavax/crypto/NoSuchPaddingException;
    :catch_4
    move-exception v1

    .line 1163
    .local v1, "ex":Ljava/security/InvalidKeyException;
    sget-object v5, Lorg/icepdf/core/pobjects/security/StandardEncryption;->logger:Ljava/util/logging/Logger;

    sget-object v6, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string/jumbo v7, "InvalidKeyException."

    invoke-virtual {v5, v6, v7, v1}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 1164
    .end local v1    # "ex":Ljava/security/InvalidKeyException;
    :catch_5
    move-exception v1

    .line 1165
    .local v1, "ex":Ljava/security/InvalidAlgorithmParameterException;
    sget-object v5, Lorg/icepdf/core/pobjects/security/StandardEncryption;->logger:Ljava/util/logging/Logger;

    sget-object v6, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string/jumbo v7, "InvalidAlgorithmParameterException"

    invoke-virtual {v5, v6, v7, v1}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private static byteCompare([B[BI)Z
    .locals 3
    .param p0, "byteArray1"    # [B
    .param p1, "byteArray2"    # [B
    .param p2, "range"    # I

    .prologue
    .line 1180
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, p2, :cond_1

    .line 1181
    aget-byte v1, p0, v0

    aget-byte v2, p1, v0

    if-eq v1, v2, :cond_0

    .line 1182
    const/4 v1, 0x0

    .line 1185
    :goto_1
    return v1

    .line 1180
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1185
    :cond_1
    const/4 v1, 0x1

    goto :goto_1
.end method

.method protected static padPassword(Ljava/lang/String;)[B
    .locals 6
    .param p0, "password"    # Ljava/lang/String;

    .prologue
    const/16 v4, 0x20

    const/4 v5, 0x0

    .line 682
    new-array v1, v4, [B

    .line 685
    .local v1, "paddedPassword":[B
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_1

    .line 686
    :cond_0
    sget-object v1, Lorg/icepdf/core/pobjects/security/StandardEncryption;->PADDING:[B

    .line 706
    .end local v1    # "paddedPassword":[B
    :goto_0
    return-object v1

    .line 690
    .restart local v1    # "paddedPassword":[B
    :cond_1
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 692
    .local v2, "passwordLength":I
    invoke-static {p0}, Lorg/icepdf/core/util/Utils;->convertByteCharSequenceToByteArray(Ljava/lang/CharSequence;)[B

    move-result-object v0

    .line 695
    .local v0, "bytePassword":[B
    invoke-static {v0, v5, v1, v5, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 698
    sget-object v3, Lorg/icepdf/core/pobjects/security/StandardEncryption;->PADDING:[B

    rsub-int/lit8 v4, v2, 0x20

    invoke-static {v3, v5, v1, v2, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_0
.end method


# virtual methods
.method public authenticateOwnerPassword(Ljava/lang/String;)Z
    .locals 14
    .param p1, "ownerPassword"    # Ljava/lang/String;

    .prologue
    const/4 v13, 0x2

    const/4 v12, 0x1

    .line 1041
    const-string/jumbo v11, ""

    invoke-virtual {p0, p1, v11, v12}, Lorg/icepdf/core/pobjects/security/StandardEncryption;->calculateOwnerPassword(Ljava/lang/String;Ljava/lang/String;Z)[B

    move-result-object v2

    .line 1045
    .local v2, "encryptionKey":[B
    const/4 v1, 0x0

    .line 1048
    .local v1, "decryptedO":[B
    :try_start_0
    iget-object v11, p0, Lorg/icepdf/core/pobjects/security/StandardEncryption;->encryptionDictionary:Lorg/icepdf/core/pobjects/security/EncryptionDictionary;

    invoke-virtual {v11}, Lorg/icepdf/core/pobjects/security/EncryptionDictionary;->getBigO()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Lorg/icepdf/core/util/Utils;->convertByteCharSequenceToByteArray(Ljava/lang/CharSequence;)[B

    move-result-object v0

    .line 1051
    .local v0, "bigO":[B
    if-nez v0, :cond_1

    .line 1052
    const/4 v6, 0x0

    .line 1121
    .end local v0    # "bigO":[B
    :cond_0
    :goto_0
    return v6

    .line 1053
    .restart local v0    # "bigO":[B
    :cond_1
    iget-object v11, p0, Lorg/icepdf/core/pobjects/security/StandardEncryption;->encryptionDictionary:Lorg/icepdf/core/pobjects/security/EncryptionDictionary;

    invoke-virtual {v11}, Lorg/icepdf/core/pobjects/security/EncryptionDictionary;->getRevisionNumber()I

    move-result v11

    if-ne v11, v13, :cond_4

    .line 1059
    new-instance v8, Ljavax/crypto/spec/SecretKeySpec;

    const-string/jumbo v11, "RC4"

    invoke-direct {v8, v2, v11}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    .line 1060
    .local v8, "key":Ljavax/crypto/spec/SecretKeySpec;
    const-string/jumbo v11, "RC4"

    invoke-static {v11}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v9

    .line 1061
    .local v9, "rc4":Ljavax/crypto/Cipher;
    const/4 v11, 0x2

    invoke-virtual {v9, v11, v8}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;)V

    .line 1062
    invoke-virtual {v9, v0}, Ljavax/crypto/Cipher;->doFinal([B)[B
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljavax/crypto/IllegalBlockSizeException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljavax/crypto/BadPaddingException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljavax/crypto/NoSuchPaddingException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/security/InvalidKeyException; {:try_start_0 .. :try_end_0} :catch_4

    move-result-object v1

    .line 1107
    .end local v0    # "bigO":[B
    .end local v8    # "key":Ljavax/crypto/spec/SecretKeySpec;
    .end local v9    # "rc4":Ljavax/crypto/Cipher;
    :cond_2
    :goto_1
    const/4 v10, 0x0

    .line 1108
    .local v10, "tmpUserPassword":Ljava/lang/String;
    const/4 v6, 0x0

    .line 1109
    .local v6, "isValid":Z
    if-eqz v1, :cond_3

    .line 1110
    invoke-static {v1}, Lorg/icepdf/core/util/Utils;->convertByteArrayToByteString([B)Ljava/lang/String;

    move-result-object v10

    .line 1112
    invoke-virtual {p0, v10}, Lorg/icepdf/core/pobjects/security/StandardEncryption;->authenticateUserPassword(Ljava/lang/String;)Z

    move-result v6

    .line 1115
    :cond_3
    if-eqz v6, :cond_0

    .line 1116
    iput-object v10, p0, Lorg/icepdf/core/pobjects/security/StandardEncryption;->userPassword:Ljava/lang/String;

    .line 1117
    iput-object p1, p0, Lorg/icepdf/core/pobjects/security/StandardEncryption;->ownerPassword:Ljava/lang/String;

    goto :goto_0

    .line 1072
    .end local v6    # "isValid":Z
    .end local v10    # "tmpUserPassword":Ljava/lang/String;
    .restart local v0    # "bigO":[B
    :cond_4
    :try_start_1
    array-length v11, v2

    new-array v5, v11, [B

    .line 1074
    .local v5, "indexedKey":[B
    move-object v1, v0

    .line 1076
    const/16 v4, 0x13

    .local v4, "i":I
    :goto_2
    if-ltz v4, :cond_2

    .line 1079
    const/4 v7, 0x0

    .local v7, "j":I
    :goto_3
    array-length v11, v5

    if-ge v7, v11, :cond_5

    .line 1080
    aget-byte v11, v2, v7

    int-to-byte v12, v4

    xor-int/2addr v11, v12

    int-to-byte v11, v11

    aput-byte v11, v5, v7

    .line 1079
    add-int/lit8 v7, v7, 0x1

    goto :goto_3

    .line 1083
    :cond_5
    new-instance v8, Ljavax/crypto/spec/SecretKeySpec;

    const-string/jumbo v11, "RC4"

    invoke-direct {v8, v5, v11}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    .line 1084
    .restart local v8    # "key":Ljavax/crypto/spec/SecretKeySpec;
    const-string/jumbo v11, "RC4"

    invoke-static {v11}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v9

    .line 1085
    .restart local v9    # "rc4":Ljavax/crypto/Cipher;
    const/4 v11, 0x1

    invoke-virtual {v9, v11, v8}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;)V

    .line 1087
    invoke-virtual {v9, v1}, Ljavax/crypto/Cipher;->update([B)[B
    :try_end_1
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljavax/crypto/IllegalBlockSizeException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljavax/crypto/BadPaddingException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljavax/crypto/NoSuchPaddingException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/security/InvalidKeyException; {:try_start_1 .. :try_end_1} :catch_4

    move-result-object v1

    .line 1076
    add-int/lit8 v4, v4, -0x1

    goto :goto_2

    .line 1091
    .end local v0    # "bigO":[B
    .end local v4    # "i":I
    .end local v5    # "indexedKey":[B
    .end local v7    # "j":I
    .end local v8    # "key":Ljavax/crypto/spec/SecretKeySpec;
    .end local v9    # "rc4":Ljavax/crypto/Cipher;
    :catch_0
    move-exception v3

    .line 1092
    .local v3, "ex":Ljava/security/NoSuchAlgorithmException;
    sget-object v11, Lorg/icepdf/core/pobjects/security/StandardEncryption;->logger:Ljava/util/logging/Logger;

    sget-object v12, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string/jumbo v13, "NoSuchAlgorithmException."

    invoke-virtual {v11, v12, v13, v3}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 1093
    .end local v3    # "ex":Ljava/security/NoSuchAlgorithmException;
    :catch_1
    move-exception v3

    .line 1094
    .local v3, "ex":Ljavax/crypto/IllegalBlockSizeException;
    sget-object v11, Lorg/icepdf/core/pobjects/security/StandardEncryption;->logger:Ljava/util/logging/Logger;

    sget-object v12, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string/jumbo v13, "IllegalBlockSizeException."

    invoke-virtual {v11, v12, v13, v3}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 1095
    .end local v3    # "ex":Ljavax/crypto/IllegalBlockSizeException;
    :catch_2
    move-exception v3

    .line 1096
    .local v3, "ex":Ljavax/crypto/BadPaddingException;
    sget-object v11, Lorg/icepdf/core/pobjects/security/StandardEncryption;->logger:Ljava/util/logging/Logger;

    sget-object v12, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string/jumbo v13, "BadPaddingException."

    invoke-virtual {v11, v12, v13, v3}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 1097
    .end local v3    # "ex":Ljavax/crypto/BadPaddingException;
    :catch_3
    move-exception v3

    .line 1098
    .local v3, "ex":Ljavax/crypto/NoSuchPaddingException;
    sget-object v11, Lorg/icepdf/core/pobjects/security/StandardEncryption;->logger:Ljava/util/logging/Logger;

    sget-object v12, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string/jumbo v13, "NoSuchPaddingException."

    invoke-virtual {v11, v12, v13, v3}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 1099
    .end local v3    # "ex":Ljavax/crypto/NoSuchPaddingException;
    :catch_4
    move-exception v3

    .line 1100
    .local v3, "ex":Ljava/security/InvalidKeyException;
    sget-object v11, Lorg/icepdf/core/pobjects/security/StandardEncryption;->logger:Ljava/util/logging/Logger;

    sget-object v12, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string/jumbo v13, "InvalidKeyException."

    invoke-virtual {v11, v12, v13, v3}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method public authenticateUserPassword(Ljava/lang/String;)Z
    .locals 7
    .param p1, "userPassword"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 1001
    invoke-virtual {p0, p1}, Lorg/icepdf/core/pobjects/security/StandardEncryption;->calculateUserPassword(Ljava/lang/String;)[B

    move-result-object v3

    .line 1003
    .local v3, "tmpUValue":[B
    iget-object v5, p0, Lorg/icepdf/core/pobjects/security/StandardEncryption;->encryptionDictionary:Lorg/icepdf/core/pobjects/security/EncryptionDictionary;

    invoke-virtual {v5}, Lorg/icepdf/core/pobjects/security/EncryptionDictionary;->getBigU()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lorg/icepdf/core/util/Utils;->convertByteCharSequenceToByteArray(Ljava/lang/CharSequence;)[B

    move-result-object v0

    .line 1008
    .local v0, "bigU":[B
    iget-object v5, p0, Lorg/icepdf/core/pobjects/security/StandardEncryption;->encryptionDictionary:Lorg/icepdf/core/pobjects/security/EncryptionDictionary;

    invoke-virtual {v5}, Lorg/icepdf/core/pobjects/security/EncryptionDictionary;->getRevisionNumber()I

    move-result v5

    const/4 v6, 0x2

    if-ne v5, v6, :cond_1

    .line 1009
    const/16 v5, 0x20

    new-array v4, v5, [B

    .line 1010
    .local v4, "trunkUValue":[B
    array-length v5, v4

    invoke-static {v3, v1, v4, v1, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1025
    :goto_0
    const/4 v1, 0x1

    .line 1026
    .local v1, "found":Z
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    array-length v5, v4

    if-ge v2, v5, :cond_0

    .line 1027
    aget-byte v5, v4, v2

    aget-byte v6, v0, v2

    if-eq v5, v6, :cond_2

    .line 1028
    const/4 v1, 0x0

    .line 1032
    .end local v1    # "found":Z
    .end local v2    # "i":I
    .end local v4    # "trunkUValue":[B
    :cond_0
    return v1

    .line 1013
    :cond_1
    iget-object v5, p0, Lorg/icepdf/core/pobjects/security/StandardEncryption;->encryptionDictionary:Lorg/icepdf/core/pobjects/security/EncryptionDictionary;

    invoke-virtual {v5}, Lorg/icepdf/core/pobjects/security/EncryptionDictionary;->getRevisionNumber()I

    move-result v5

    const/4 v6, 0x3

    if-lt v5, v6, :cond_0

    iget-object v5, p0, Lorg/icepdf/core/pobjects/security/StandardEncryption;->encryptionDictionary:Lorg/icepdf/core/pobjects/security/EncryptionDictionary;

    invoke-virtual {v5}, Lorg/icepdf/core/pobjects/security/EncryptionDictionary;->getRevisionNumber()I

    move-result v5

    const/4 v6, 0x5

    if-ge v5, v6, :cond_0

    .line 1015
    const/16 v5, 0x10

    new-array v4, v5, [B

    .line 1016
    .restart local v4    # "trunkUValue":[B
    array-length v5, v4

    invoke-static {v3, v1, v4, v1, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_0

    .line 1026
    .restart local v1    # "found":Z
    .restart local v2    # "i":I
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method public calculateOwnerPassword(Ljava/lang/String;Ljava/lang/String;Z)[B
    .locals 17
    .param p1, "ownerPassword"    # Ljava/lang/String;
    .param p2, "userPassword"    # Ljava/lang/String;
    .param p3, "isAuthentication"    # Z

    .prologue
    .line 733
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    move-result v14

    if-nez v14, :cond_0

    invoke-virtual/range {p2 .. p2}, Ljava/lang/String;->length()I

    move-result v14

    if-eqz v14, :cond_0

    .line 734
    move-object/from16 p1, p2

    .line 736
    :cond_0
    invoke-static/range {p1 .. p1}, Lorg/icepdf/core/pobjects/security/StandardEncryption;->padPassword(Ljava/lang/String;)[B

    move-result-object v11

    .line 739
    .local v11, "paddedOwnerPassword":[B
    const/4 v10, 0x0

    .line 741
    .local v10, "md5":Ljava/security/MessageDigest;
    :try_start_0
    const-string/jumbo v14, "MD5"

    invoke-static {v14}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v10

    .line 746
    :goto_0
    invoke-virtual {v10, v11}, Ljava/security/MessageDigest;->digest([B)[B

    move-result-object v11

    .line 751
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/icepdf/core/pobjects/security/StandardEncryption;->encryptionDictionary:Lorg/icepdf/core/pobjects/security/EncryptionDictionary;

    invoke-virtual {v14}, Lorg/icepdf/core/pobjects/security/EncryptionDictionary;->getRevisionNumber()I

    move-result v14

    const/4 v15, 0x3

    if-lt v14, v15, :cond_1

    .line 752
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_1
    const/16 v14, 0x32

    if-ge v6, v14, :cond_1

    .line 753
    invoke-virtual {v10, v11}, Ljava/security/MessageDigest;->digest([B)[B

    move-result-object v11

    .line 752
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 742
    .end local v6    # "i":I
    :catch_0
    move-exception v2

    .line 743
    .local v2, "e":Ljava/security/NoSuchAlgorithmException;
    sget-object v14, Lorg/icepdf/core/pobjects/security/StandardEncryption;->logger:Ljava/util/logging/Logger;

    sget-object v15, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string/jumbo v16, "Could not fint MD5 Digest"

    move-object/from16 v0, v16

    invoke-virtual {v14, v15, v0, v2}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 763
    .end local v2    # "e":Ljava/security/NoSuchAlgorithmException;
    :cond_1
    const/4 v1, 0x5

    .line 764
    .local v1, "dataSize":I
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/icepdf/core/pobjects/security/StandardEncryption;->encryptionDictionary:Lorg/icepdf/core/pobjects/security/EncryptionDictionary;

    invoke-virtual {v14}, Lorg/icepdf/core/pobjects/security/EncryptionDictionary;->getRevisionNumber()I

    move-result v14

    const/4 v15, 0x3

    if-lt v14, v15, :cond_2

    .line 765
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/icepdf/core/pobjects/security/StandardEncryption;->encryptionDictionary:Lorg/icepdf/core/pobjects/security/EncryptionDictionary;

    invoke-virtual {v14}, Lorg/icepdf/core/pobjects/security/EncryptionDictionary;->getKeyLength()I

    move-result v14

    div-int/lit8 v1, v14, 0x8

    .line 767
    :cond_2
    array-length v14, v11

    if-le v1, v14, :cond_3

    .line 768
    array-length v1, v11

    .line 772
    :cond_3
    new-array v3, v1, [B

    .line 774
    .local v3, "encryptionKey":[B
    const/4 v14, 0x0

    const/4 v15, 0x0

    invoke-static {v11, v14, v3, v15, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 777
    if-eqz p3, :cond_4

    .line 849
    .end local v3    # "encryptionKey":[B
    :goto_2
    return-object v3

    .line 782
    .restart local v3    # "encryptionKey":[B
    :cond_4
    invoke-static/range {p2 .. p2}, Lorg/icepdf/core/pobjects/security/StandardEncryption;->padPassword(Ljava/lang/String;)[B

    move-result-object v12

    .line 786
    .local v12, "paddedUserPassword":[B
    const/4 v5, 0x0

    .line 789
    .local v5, "finalData":[B
    :try_start_1
    new-instance v9, Ljavax/crypto/spec/SecretKeySpec;

    const-string/jumbo v14, "RC4"

    invoke-direct {v9, v3, v14}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    .line 790
    .local v9, "key":Ljavax/crypto/spec/SecretKeySpec;
    const-string/jumbo v14, "RC4"

    invoke-static {v14}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v13

    .line 791
    .local v13, "rc4":Ljavax/crypto/Cipher;
    const/4 v14, 0x1

    invoke-virtual {v13, v14, v9}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;)V

    .line 794
    invoke-virtual {v13, v12}, Ljavax/crypto/Cipher;->update([B)[B

    move-result-object v5

    .line 803
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/icepdf/core/pobjects/security/StandardEncryption;->encryptionDictionary:Lorg/icepdf/core/pobjects/security/EncryptionDictionary;

    invoke-virtual {v14}, Lorg/icepdf/core/pobjects/security/EncryptionDictionary;->getRevisionNumber()I

    move-result v14

    const/4 v15, 0x3

    if-lt v14, v15, :cond_6

    .line 806
    array-length v14, v3

    new-array v7, v14, [B

    .line 808
    .local v7, "indexedKey":[B
    const/4 v6, 0x1

    .restart local v6    # "i":I
    :goto_3
    const/16 v14, 0x13

    if-gt v6, v14, :cond_6

    .line 811
    const/4 v8, 0x0

    .local v8, "j":I
    :goto_4
    array-length v14, v3

    if-ge v8, v14, :cond_5

    .line 812
    aget-byte v14, v3, v8

    xor-int/2addr v14, v6

    int-to-byte v14, v14

    aput-byte v14, v7, v8

    .line 811
    add-int/lit8 v8, v8, 0x1

    goto :goto_4

    .line 815
    :cond_5
    new-instance v9, Ljavax/crypto/spec/SecretKeySpec;

    .end local v9    # "key":Ljavax/crypto/spec/SecretKeySpec;
    const-string/jumbo v14, "RC4"

    invoke-direct {v9, v7, v14}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    .line 817
    .restart local v9    # "key":Ljavax/crypto/spec/SecretKeySpec;
    const/4 v14, 0x1

    invoke-virtual {v13, v14, v9}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;)V

    .line 819
    invoke-virtual {v13, v5}, Ljavax/crypto/Cipher;->update([B)[B
    :try_end_1
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljavax/crypto/NoSuchPaddingException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/security/InvalidKeyException; {:try_start_1 .. :try_end_1} :catch_3

    move-result-object v5

    .line 808
    add-int/lit8 v6, v6, 0x1

    goto :goto_3

    .line 823
    .end local v6    # "i":I
    .end local v7    # "indexedKey":[B
    .end local v8    # "j":I
    .end local v9    # "key":Ljavax/crypto/spec/SecretKeySpec;
    .end local v13    # "rc4":Ljavax/crypto/Cipher;
    :catch_1
    move-exception v4

    .line 824
    .local v4, "ex":Ljava/security/NoSuchAlgorithmException;
    sget-object v14, Lorg/icepdf/core/pobjects/security/StandardEncryption;->logger:Ljava/util/logging/Logger;

    sget-object v15, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string/jumbo v16, "NoSuchAlgorithmException."

    move-object/from16 v0, v16

    invoke-virtual {v14, v15, v0, v4}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    .end local v4    # "ex":Ljava/security/NoSuchAlgorithmException;
    :cond_6
    :goto_5
    move-object v3, v5

    .line 849
    goto :goto_2

    .line 825
    :catch_2
    move-exception v4

    .line 826
    .local v4, "ex":Ljavax/crypto/NoSuchPaddingException;
    sget-object v14, Lorg/icepdf/core/pobjects/security/StandardEncryption;->logger:Ljava/util/logging/Logger;

    sget-object v15, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string/jumbo v16, "NoSuchPaddingException."

    move-object/from16 v0, v16

    invoke-virtual {v14, v15, v0, v4}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_5

    .line 827
    .end local v4    # "ex":Ljavax/crypto/NoSuchPaddingException;
    :catch_3
    move-exception v4

    .line 828
    .local v4, "ex":Ljava/security/InvalidKeyException;
    sget-object v14, Lorg/icepdf/core/pobjects/security/StandardEncryption;->logger:Ljava/util/logging/Logger;

    sget-object v15, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string/jumbo v16, "InvalidKeyException."

    move-object/from16 v0, v16

    invoke-virtual {v14, v15, v0, v4}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_5
.end method

.method public calculateUserPassword(Ljava/lang/String;)[B
    .locals 22
    .param p1, "userPassword"    # Ljava/lang/String;

    .prologue
    .line 869
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/core/pobjects/security/StandardEncryption;->encryptionDictionary:Lorg/icepdf/core/pobjects/security/EncryptionDictionary;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lorg/icepdf/core/pobjects/security/EncryptionDictionary;->getKeyLength()I

    move-result v18

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Lorg/icepdf/core/pobjects/security/StandardEncryption;->encryptionKeyAlgorithm(Ljava/lang/String;I)[B

    move-result-object v6

    .line 874
    .local v6, "encryptionKey":[B
    if-nez v6, :cond_0

    .line 875
    const/4 v9, 0x0

    .line 987
    :goto_0
    return-object v9

    .line 878
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/core/pobjects/security/StandardEncryption;->encryptionDictionary:Lorg/icepdf/core/pobjects/security/EncryptionDictionary;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lorg/icepdf/core/pobjects/security/EncryptionDictionary;->getRevisionNumber()I

    move-result v18

    const/16 v19, 0x2

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_1

    .line 884
    sget-object v18, Lorg/icepdf/core/pobjects/security/StandardEncryption;->PADDING:[B

    invoke-virtual/range {v18 .. v18}, [B->clone()Ljava/lang/Object;

    move-result-object v16

    check-cast v16, [B

    .line 886
    .local v16, "paddedUserPassword":[B
    const/4 v9, 0x0

    .line 889
    .local v9, "finalData":[B
    :try_start_0
    new-instance v14, Ljavax/crypto/spec/SecretKeySpec;

    const-string/jumbo v18, "RC4"

    move-object/from16 v0, v18

    invoke-direct {v14, v6, v0}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    .line 890
    .local v14, "key":Ljavax/crypto/spec/SecretKeySpec;
    const-string/jumbo v18, "RC4"

    invoke-static/range {v18 .. v18}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v17

    .line 891
    .local v17, "rc4":Ljavax/crypto/Cipher;
    const/16 v18, 0x1

    move-object/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v0, v1, v14}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;)V

    .line 894
    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljavax/crypto/Cipher;->doFinal([B)[B
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljavax/crypto/IllegalBlockSizeException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljavax/crypto/BadPaddingException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljavax/crypto/NoSuchPaddingException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/security/InvalidKeyException; {:try_start_0 .. :try_end_0} :catch_4

    move-result-object v9

    goto :goto_0

    .line 896
    .end local v14    # "key":Ljavax/crypto/spec/SecretKeySpec;
    .end local v17    # "rc4":Ljavax/crypto/Cipher;
    :catch_0
    move-exception v7

    .line 897
    .local v7, "ex":Ljava/security/NoSuchAlgorithmException;
    sget-object v18, Lorg/icepdf/core/pobjects/security/StandardEncryption;->logger:Ljava/util/logging/Logger;

    sget-object v19, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string/jumbo v20, "NoSuchAlgorithmException."

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    move-object/from16 v2, v20

    invoke-virtual {v0, v1, v2, v7}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 898
    .end local v7    # "ex":Ljava/security/NoSuchAlgorithmException;
    :catch_1
    move-exception v7

    .line 899
    .local v7, "ex":Ljavax/crypto/IllegalBlockSizeException;
    sget-object v18, Lorg/icepdf/core/pobjects/security/StandardEncryption;->logger:Ljava/util/logging/Logger;

    sget-object v19, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string/jumbo v20, "IllegalBlockSizeException."

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    move-object/from16 v2, v20

    invoke-virtual {v0, v1, v2, v7}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 900
    .end local v7    # "ex":Ljavax/crypto/IllegalBlockSizeException;
    :catch_2
    move-exception v7

    .line 901
    .local v7, "ex":Ljavax/crypto/BadPaddingException;
    sget-object v18, Lorg/icepdf/core/pobjects/security/StandardEncryption;->logger:Ljava/util/logging/Logger;

    sget-object v19, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string/jumbo v20, "BadPaddingException."

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    move-object/from16 v2, v20

    invoke-virtual {v0, v1, v2, v7}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 902
    .end local v7    # "ex":Ljavax/crypto/BadPaddingException;
    :catch_3
    move-exception v7

    .line 903
    .local v7, "ex":Ljavax/crypto/NoSuchPaddingException;
    sget-object v18, Lorg/icepdf/core/pobjects/security/StandardEncryption;->logger:Ljava/util/logging/Logger;

    sget-object v19, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string/jumbo v20, "NoSuchPaddingException."

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    move-object/from16 v2, v20

    invoke-virtual {v0, v1, v2, v7}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0

    .line 904
    .end local v7    # "ex":Ljavax/crypto/NoSuchPaddingException;
    :catch_4
    move-exception v7

    .line 905
    .local v7, "ex":Ljava/security/InvalidKeyException;
    sget-object v18, Lorg/icepdf/core/pobjects/security/StandardEncryption;->logger:Ljava/util/logging/Logger;

    sget-object v19, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string/jumbo v20, "InvalidKeyException."

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    move-object/from16 v2, v20

    invoke-virtual {v0, v1, v2, v7}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0

    .line 911
    .end local v7    # "ex":Ljava/security/InvalidKeyException;
    .end local v9    # "finalData":[B
    .end local v16    # "paddedUserPassword":[B
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/core/pobjects/security/StandardEncryption;->encryptionDictionary:Lorg/icepdf/core/pobjects/security/EncryptionDictionary;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lorg/icepdf/core/pobjects/security/EncryptionDictionary;->getRevisionNumber()I

    move-result v18

    const/16 v19, 0x3

    move/from16 v0, v18

    move/from16 v1, v19

    if-lt v0, v1, :cond_4

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/core/pobjects/security/StandardEncryption;->encryptionDictionary:Lorg/icepdf/core/pobjects/security/EncryptionDictionary;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lorg/icepdf/core/pobjects/security/EncryptionDictionary;->getRevisionNumber()I

    move-result v18

    const/16 v19, 0x5

    move/from16 v0, v18

    move/from16 v1, v19

    if-ge v0, v1, :cond_4

    .line 916
    sget-object v18, Lorg/icepdf/core/pobjects/security/StandardEncryption;->PADDING:[B

    invoke-virtual/range {v18 .. v18}, [B->clone()Ljava/lang/Object;

    move-result-object v16

    check-cast v16, [B

    .line 917
    .restart local v16    # "paddedUserPassword":[B
    const/4 v15, 0x0

    .line 919
    .local v15, "md5":Ljava/security/MessageDigest;
    :try_start_1
    const-string/jumbo v18, "MD5"

    invoke-static/range {v18 .. v18}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;
    :try_end_1
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_1 .. :try_end_1} :catch_5

    move-result-object v15

    .line 924
    :goto_1
    invoke-virtual/range {v15 .. v16}, Ljava/security/MessageDigest;->update([B)V

    .line 928
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/core/pobjects/security/StandardEncryption;->encryptionDictionary:Lorg/icepdf/core/pobjects/security/EncryptionDictionary;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lorg/icepdf/core/pobjects/security/EncryptionDictionary;->getFileID()Ljava/util/Vector;

    move-result-object v18

    const/16 v19, 0x0

    invoke-virtual/range {v18 .. v19}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lorg/icepdf/core/pobjects/StringObject;

    invoke-interface/range {v18 .. v18}, Lorg/icepdf/core/pobjects/StringObject;->getLiteralString()Ljava/lang/String;

    move-result-object v10

    .line 929
    .local v10, "firstFileID":Ljava/lang/String;
    invoke-static {v10}, Lorg/icepdf/core/util/Utils;->convertByteCharSequenceToByteArray(Ljava/lang/CharSequence;)[B

    move-result-object v8

    .line 930
    .local v8, "fileID":[B
    invoke-virtual {v15, v8}, Ljava/security/MessageDigest;->digest([B)[B

    move-result-object v5

    .line 941
    .local v5, "encryptData":[B
    :try_start_2
    new-instance v14, Ljavax/crypto/spec/SecretKeySpec;

    const-string/jumbo v18, "RC4"

    move-object/from16 v0, v18

    invoke-direct {v14, v6, v0}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    .line 942
    .restart local v14    # "key":Ljavax/crypto/spec/SecretKeySpec;
    const-string/jumbo v18, "RC4"

    invoke-static/range {v18 .. v18}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v17

    .line 943
    .restart local v17    # "rc4":Ljavax/crypto/Cipher;
    const/16 v18, 0x1

    move-object/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v0, v1, v14}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;)V

    .line 946
    move-object/from16 v0, v17

    invoke-virtual {v0, v5}, Ljavax/crypto/Cipher;->update([B)[B

    move-result-object v5

    .line 956
    array-length v0, v6

    move/from16 v18, v0

    move/from16 v0, v18

    new-array v12, v0, [B

    .line 958
    .local v12, "indexedKey":[B
    const/4 v11, 0x1

    .local v11, "i":I
    :goto_2
    const/16 v18, 0x13

    move/from16 v0, v18

    if-gt v11, v0, :cond_3

    .line 961
    const/4 v13, 0x0

    .local v13, "j":I
    :goto_3
    array-length v0, v6

    move/from16 v18, v0

    move/from16 v0, v18

    if-ge v13, v0, :cond_2

    .line 962
    aget-byte v18, v6, v13

    int-to-byte v0, v11

    move/from16 v19, v0

    xor-int v18, v18, v19

    move/from16 v0, v18

    int-to-byte v0, v0

    move/from16 v18, v0

    aput-byte v18, v12, v13
    :try_end_2
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_2 .. :try_end_2} :catch_6
    .catch Ljavax/crypto/NoSuchPaddingException; {:try_start_2 .. :try_end_2} :catch_7
    .catch Ljava/security/InvalidKeyException; {:try_start_2 .. :try_end_2} :catch_8

    .line 961
    add-int/lit8 v13, v13, 0x1

    goto :goto_3

    .line 920
    .end local v5    # "encryptData":[B
    .end local v8    # "fileID":[B
    .end local v10    # "firstFileID":Ljava/lang/String;
    .end local v11    # "i":I
    .end local v12    # "indexedKey":[B
    .end local v13    # "j":I
    .end local v14    # "key":Ljavax/crypto/spec/SecretKeySpec;
    .end local v17    # "rc4":Ljavax/crypto/Cipher;
    :catch_5
    move-exception v4

    .line 921
    .local v4, "e":Ljava/security/NoSuchAlgorithmException;
    sget-object v18, Lorg/icepdf/core/pobjects/security/StandardEncryption;->logger:Ljava/util/logging/Logger;

    sget-object v19, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string/jumbo v20, "MD5 digester could not be found"

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    move-object/from16 v2, v20

    invoke-virtual {v0, v1, v2, v4}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 965
    .end local v4    # "e":Ljava/security/NoSuchAlgorithmException;
    .restart local v5    # "encryptData":[B
    .restart local v8    # "fileID":[B
    .restart local v10    # "firstFileID":Ljava/lang/String;
    .restart local v11    # "i":I
    .restart local v12    # "indexedKey":[B
    .restart local v13    # "j":I
    .restart local v14    # "key":Ljavax/crypto/spec/SecretKeySpec;
    .restart local v17    # "rc4":Ljavax/crypto/Cipher;
    :cond_2
    :try_start_3
    new-instance v14, Ljavax/crypto/spec/SecretKeySpec;

    .end local v14    # "key":Ljavax/crypto/spec/SecretKeySpec;
    const-string/jumbo v18, "RC4"

    move-object/from16 v0, v18

    invoke-direct {v14, v12, v0}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    .line 966
    .restart local v14    # "key":Ljavax/crypto/spec/SecretKeySpec;
    const/16 v18, 0x1

    move-object/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v0, v1, v14}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;)V

    .line 968
    move-object/from16 v0, v17

    invoke-virtual {v0, v5}, Ljavax/crypto/Cipher;->update([B)[B
    :try_end_3
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_3 .. :try_end_3} :catch_6
    .catch Ljavax/crypto/NoSuchPaddingException; {:try_start_3 .. :try_end_3} :catch_7
    .catch Ljava/security/InvalidKeyException; {:try_start_3 .. :try_end_3} :catch_8

    move-result-object v5

    .line 958
    add-int/lit8 v11, v11, 0x1

    goto :goto_2

    .line 971
    .end local v11    # "i":I
    .end local v12    # "indexedKey":[B
    .end local v13    # "j":I
    .end local v14    # "key":Ljavax/crypto/spec/SecretKeySpec;
    .end local v17    # "rc4":Ljavax/crypto/Cipher;
    :catch_6
    move-exception v7

    .line 972
    .local v7, "ex":Ljava/security/NoSuchAlgorithmException;
    sget-object v18, Lorg/icepdf/core/pobjects/security/StandardEncryption;->logger:Ljava/util/logging/Logger;

    sget-object v19, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string/jumbo v20, "NoSuchAlgorithmException."

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    move-object/from16 v2, v20

    invoke-virtual {v0, v1, v2, v7}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 981
    .end local v7    # "ex":Ljava/security/NoSuchAlgorithmException;
    :cond_3
    :goto_4
    const/16 v18, 0x20

    move/from16 v0, v18

    new-array v9, v0, [B

    .line 982
    .restart local v9    # "finalData":[B
    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x10

    move/from16 v0, v18

    move/from16 v1, v19

    move/from16 v2, v20

    invoke-static {v5, v0, v9, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 983
    sget-object v18, Lorg/icepdf/core/pobjects/security/StandardEncryption;->PADDING:[B

    const/16 v19, 0x0

    const/16 v20, 0x10

    const/16 v21, 0x10

    move-object/from16 v0, v18

    move/from16 v1, v19

    move/from16 v2, v20

    move/from16 v3, v21

    invoke-static {v0, v1, v9, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto/16 :goto_0

    .line 973
    .end local v9    # "finalData":[B
    :catch_7
    move-exception v7

    .line 974
    .local v7, "ex":Ljavax/crypto/NoSuchPaddingException;
    sget-object v18, Lorg/icepdf/core/pobjects/security/StandardEncryption;->logger:Ljava/util/logging/Logger;

    sget-object v19, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string/jumbo v20, "NoSuchPaddingException."

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    move-object/from16 v2, v20

    invoke-virtual {v0, v1, v2, v7}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_4

    .line 975
    .end local v7    # "ex":Ljavax/crypto/NoSuchPaddingException;
    :catch_8
    move-exception v7

    .line 976
    .local v7, "ex":Ljava/security/InvalidKeyException;
    sget-object v18, Lorg/icepdf/core/pobjects/security/StandardEncryption;->logger:Ljava/util/logging/Logger;

    sget-object v19, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string/jumbo v20, "InvalidKeyException."

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    move-object/from16 v2, v20

    invoke-virtual {v0, v1, v2, v7}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_4

    .line 987
    .end local v5    # "encryptData":[B
    .end local v7    # "ex":Ljava/security/InvalidKeyException;
    .end local v8    # "fileID":[B
    .end local v10    # "firstFileID":Ljava/lang/String;
    .end local v15    # "md5":Ljava/security/MessageDigest;
    .end local v16    # "paddedUserPassword":[B
    :cond_4
    const/4 v9, 0x0

    goto/16 :goto_0
.end method

.method public encryptionKeyAlgorithm(Ljava/lang/String;I)[B
    .locals 33
    .param p1, "password"    # Ljava/lang/String;
    .param p2, "keyLength"    # I

    .prologue
    .line 471
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/core/pobjects/security/StandardEncryption;->encryptionDictionary:Lorg/icepdf/core/pobjects/security/EncryptionDictionary;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Lorg/icepdf/core/pobjects/security/EncryptionDictionary;->getRevisionNumber()I

    move-result v30

    const/16 v31, 0x5

    move/from16 v0, v30

    move/from16 v1, v31

    if-ge v0, v1, :cond_5

    .line 473
    invoke-static/range {p1 .. p1}, Lorg/icepdf/core/pobjects/security/StandardEncryption;->padPassword(Ljava/lang/String;)[B

    move-result-object v23

    .line 476
    .local v23, "paddedPassword":[B
    const/16 v16, 0x0

    .line 478
    .local v16, "md5":Ljava/security/MessageDigest;
    :try_start_0
    const-string/jumbo v30, "MD5"

    invoke-static/range {v30 .. v30}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v16

    .line 483
    :goto_0
    move-object/from16 v0, v16

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/security/MessageDigest;->update([B)V

    .line 486
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/core/pobjects/security/StandardEncryption;->encryptionDictionary:Lorg/icepdf/core/pobjects/security/EncryptionDictionary;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Lorg/icepdf/core/pobjects/security/EncryptionDictionary;->getBigO()Ljava/lang/String;

    move-result-object v30

    invoke-static/range {v30 .. v30}, Lorg/icepdf/core/util/Utils;->convertByteCharSequenceToByteArray(Ljava/lang/CharSequence;)[B

    move-result-object v5

    .line 488
    .local v5, "bigO":[B
    move-object/from16 v0, v16

    invoke-virtual {v0, v5}, Ljava/security/MessageDigest;->update([B)V

    .line 491
    const/4 v12, 0x0

    .local v12, "i":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/core/pobjects/security/StandardEncryption;->encryptionDictionary:Lorg/icepdf/core/pobjects/security/EncryptionDictionary;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Lorg/icepdf/core/pobjects/security/EncryptionDictionary;->getPermissions()I

    move-result v21

    .local v21, "p":I
    :goto_1
    const/16 v30, 0x4

    move/from16 v0, v30

    if-ge v12, v0, :cond_0

    .line 493
    move/from16 v0, v21

    and-int/lit16 v0, v0, 0xff

    move/from16 v30, v0

    move/from16 v0, v30

    int-to-byte v0, v0

    move/from16 v30, v0

    move-object/from16 v0, v16

    move/from16 v1, v30

    invoke-virtual {v0, v1}, Ljava/security/MessageDigest;->update(B)V

    .line 491
    add-int/lit8 v12, v12, 0x1

    .line 492
    shr-int/lit8 v21, v21, 0x8

    goto :goto_1

    .line 479
    .end local v5    # "bigO":[B
    .end local v12    # "i":I
    .end local v21    # "p":I
    :catch_0
    move-exception v8

    .line 480
    .local v8, "ex":Ljava/security/NoSuchAlgorithmException;
    sget-object v30, Lorg/icepdf/core/pobjects/security/StandardEncryption;->logger:Ljava/util/logging/Logger;

    sget-object v31, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string/jumbo v32, "NoSuchAlgorithmException."

    move-object/from16 v0, v30

    move-object/from16 v1, v31

    move-object/from16 v2, v32

    invoke-virtual {v0, v1, v2, v8}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 497
    .end local v8    # "ex":Ljava/security/NoSuchAlgorithmException;
    .restart local v5    # "bigO":[B
    .restart local v12    # "i":I
    .restart local v21    # "p":I
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/core/pobjects/security/StandardEncryption;->encryptionDictionary:Lorg/icepdf/core/pobjects/security/EncryptionDictionary;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Lorg/icepdf/core/pobjects/security/EncryptionDictionary;->getFileID()Ljava/util/Vector;

    move-result-object v30

    const/16 v31, 0x0

    invoke-virtual/range {v30 .. v31}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v30

    check-cast v30, Lorg/icepdf/core/pobjects/StringObject;

    invoke-interface/range {v30 .. v30}, Lorg/icepdf/core/pobjects/StringObject;->getLiteralString()Ljava/lang/String;

    move-result-object v10

    .line 499
    .local v10, "firstFileID":Ljava/lang/String;
    invoke-static {v10}, Lorg/icepdf/core/util/Utils;->convertByteCharSequenceToByteArray(Ljava/lang/CharSequence;)[B

    move-result-object v9

    .line 500
    .local v9, "fileID":[B
    move-object/from16 v0, v16

    invoke-virtual {v0, v9}, Ljava/security/MessageDigest;->digest([B)[B

    move-result-object v23

    .line 511
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/core/pobjects/security/StandardEncryption;->encryptionDictionary:Lorg/icepdf/core/pobjects/security/EncryptionDictionary;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Lorg/icepdf/core/pobjects/security/EncryptionDictionary;->getRevisionNumber()I

    move-result v30

    const/16 v31, 0x3

    move/from16 v0, v30

    move/from16 v1, v31

    if-lt v0, v1, :cond_1

    .line 512
    const/4 v12, 0x0

    :goto_2
    const/16 v30, 0x32

    move/from16 v0, v30

    if-ge v12, v0, :cond_1

    .line 513
    move-object/from16 v0, v16

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/security/MessageDigest;->digest([B)[B

    move-result-object v23

    .line 512
    add-int/lit8 v12, v12, 0x1

    goto :goto_2

    .line 519
    :cond_1
    const/16 v19, 0x0

    .line 520
    .local v19, "out":[B
    const/16 v17, 0x5

    .line 522
    .local v17, "n":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/core/pobjects/security/StandardEncryption;->encryptionDictionary:Lorg/icepdf/core/pobjects/security/EncryptionDictionary;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Lorg/icepdf/core/pobjects/security/EncryptionDictionary;->getRevisionNumber()I

    move-result v30

    const/16 v31, 0x2

    move/from16 v0, v30

    move/from16 v1, v31

    if-ne v0, v1, :cond_4

    .line 523
    move/from16 v0, v17

    new-array v0, v0, [B

    move-object/from16 v19, v0

    .line 528
    :cond_2
    :goto_3
    move-object/from16 v0, v23

    array-length v0, v0

    move/from16 v30, v0

    move/from16 v0, v17

    move/from16 v1, v30

    if-le v0, v1, :cond_3

    .line 529
    move-object/from16 v0, v23

    array-length v0, v0

    move/from16 v17, v0

    .line 533
    :cond_3
    const/16 v30, 0x0

    const/16 v31, 0x0

    move-object/from16 v0, v23

    move/from16 v1, v30

    move-object/from16 v2, v19

    move/from16 v3, v31

    move/from16 v4, v17

    invoke-static {v0, v1, v2, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 539
    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/icepdf/core/pobjects/security/StandardEncryption;->encryptionKey:[B

    .line 661
    .end local v5    # "bigO":[B
    .end local v9    # "fileID":[B
    .end local v10    # "firstFileID":Ljava/lang/String;
    .end local v12    # "i":I
    .end local v16    # "md5":Ljava/security/MessageDigest;
    .end local v17    # "n":I
    .end local v19    # "out":[B
    .end local v21    # "p":I
    .end local v23    # "paddedPassword":[B
    :goto_4
    return-object v19

    .line 524
    .restart local v5    # "bigO":[B
    .restart local v9    # "fileID":[B
    .restart local v10    # "firstFileID":Ljava/lang/String;
    .restart local v12    # "i":I
    .restart local v16    # "md5":Ljava/security/MessageDigest;
    .restart local v17    # "n":I
    .restart local v19    # "out":[B
    .restart local v21    # "p":I
    .restart local v23    # "paddedPassword":[B
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/core/pobjects/security/StandardEncryption;->encryptionDictionary:Lorg/icepdf/core/pobjects/security/EncryptionDictionary;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Lorg/icepdf/core/pobjects/security/EncryptionDictionary;->getRevisionNumber()I

    move-result v30

    const/16 v31, 0x3

    move/from16 v0, v30

    move/from16 v1, v31

    if-lt v0, v1, :cond_2

    .line 525
    div-int/lit8 v17, p2, 0x8

    .line 526
    move/from16 v0, v17

    new-array v0, v0, [B

    move-object/from16 v19, v0

    goto :goto_3

    .line 544
    .end local v5    # "bigO":[B
    .end local v9    # "fileID":[B
    .end local v10    # "firstFileID":Ljava/lang/String;
    .end local v12    # "i":I
    .end local v16    # "md5":Ljava/security/MessageDigest;
    .end local v17    # "n":I
    .end local v19    # "out":[B
    .end local v21    # "p":I
    .end local v23    # "paddedPassword":[B
    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/core/pobjects/security/StandardEncryption;->encryptionDictionary:Lorg/icepdf/core/pobjects/security/EncryptionDictionary;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Lorg/icepdf/core/pobjects/security/EncryptionDictionary;->getRevisionNumber()I

    move-result v30

    const/16 v31, 0x5

    move/from16 v0, v30

    move/from16 v1, v31

    if-ne v0, v1, :cond_f

    .line 546
    :try_start_1
    invoke-static/range {p1 .. p1}, Lorg/icepdf/core/util/Utils;->convertByteCharSequenceToByteArray(Ljava/lang/CharSequence;)[B

    move-result-object v24

    .line 547
    .local v24, "passwordBytes":[B
    if-nez v24, :cond_6

    .line 548
    const/16 v30, 0x0

    move/from16 v0, v30

    new-array v0, v0, [B

    move-object/from16 v24, v0

    .line 551
    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/core/pobjects/security/StandardEncryption;->encryptionDictionary:Lorg/icepdf/core/pobjects/security/EncryptionDictionary;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Lorg/icepdf/core/pobjects/security/EncryptionDictionary;->getBigO()Ljava/lang/String;

    move-result-object v30

    invoke-static/range {v30 .. v30}, Lorg/icepdf/core/util/Utils;->convertByteCharSequenceToByteArray(Ljava/lang/CharSequence;)[B

    move-result-object v20

    .line 553
    .local v20, "ownerPassword":[B
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/core/pobjects/security/StandardEncryption;->encryptionDictionary:Lorg/icepdf/core/pobjects/security/EncryptionDictionary;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Lorg/icepdf/core/pobjects/security/EncryptionDictionary;->getBigU()Ljava/lang/String;

    move-result-object v30

    invoke-static/range {v30 .. v30}, Lorg/icepdf/core/util/Utils;->convertByteCharSequenceToByteArray(Ljava/lang/CharSequence;)[B

    move-result-object v29

    .line 560
    .local v29, "userPassword":[B
    const-string/jumbo v30, "SHA-256"

    invoke-static/range {v30 .. v30}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v15

    .line 563
    .local v15, "md":Ljava/security/MessageDigest;
    const/16 v30, 0x0

    move-object/from16 v0, v24

    array-length v0, v0

    move/from16 v31, v0

    const/16 v32, 0x7f

    invoke-static/range {v31 .. v32}, Ljava/lang/Math;->min(II)I

    move-result v31

    move-object/from16 v0, v24

    move/from16 v1, v30

    move/from16 v2, v31

    invoke-virtual {v15, v0, v1, v2}, Ljava/security/MessageDigest;->update([BII)V

    .line 565
    const/16 v30, 0x20

    const/16 v31, 0x8

    move-object/from16 v0, v20

    move/from16 v1, v30

    move/from16 v2, v31

    invoke-virtual {v15, v0, v1, v2}, Ljava/security/MessageDigest;->update([BII)V

    .line 567
    const/16 v30, 0x0

    const/16 v31, 0x30

    move-object/from16 v0, v29

    move/from16 v1, v30

    move/from16 v2, v31

    invoke-virtual {v15, v0, v1, v2}, Ljava/security/MessageDigest;->update([BII)V

    .line 569
    invoke-virtual {v15}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v11

    .line 572
    .local v11, "hash":[B
    const/16 v30, 0x20

    move-object/from16 v0, v20

    move/from16 v1, v30

    invoke-static {v11, v0, v1}, Lorg/icepdf/core/pobjects/security/StandardEncryption;->byteCompare([B[BI)Z

    move-result v13

    .line 573
    .local v13, "isOwnerPassword":Z
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/core/pobjects/security/StandardEncryption;->encryptionDictionary:Lorg/icepdf/core/pobjects/security/EncryptionDictionary;

    move-object/from16 v30, v0

    move-object/from16 v0, v30

    invoke-virtual {v0, v13}, Lorg/icepdf/core/pobjects/security/EncryptionDictionary;->setAuthenticatedOwnerPassword(Z)V

    .line 574
    if-eqz v13, :cond_9

    .line 576
    const/16 v30, 0x0

    move-object/from16 v0, v24

    array-length v0, v0

    move/from16 v31, v0

    const/16 v32, 0x7f

    invoke-static/range {v31 .. v32}, Ljava/lang/Math;->min(II)I

    move-result v31

    move-object/from16 v0, v24

    move/from16 v1, v30

    move/from16 v2, v31

    invoke-virtual {v15, v0, v1, v2}, Ljava/security/MessageDigest;->update([BII)V

    .line 578
    const/16 v30, 0x20

    const/16 v31, 0x8

    move-object/from16 v0, v20

    move/from16 v1, v30

    move/from16 v2, v31

    invoke-virtual {v15, v0, v1, v2}, Ljava/security/MessageDigest;->update([BII)V

    .line 580
    const/16 v30, 0x0

    const/16 v31, 0x30

    move-object/from16 v0, v29

    move/from16 v1, v30

    move/from16 v2, v31

    invoke-virtual {v15, v0, v1, v2}, Ljava/security/MessageDigest;->update([BII)V

    .line 581
    invoke-virtual {v15}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v11

    .line 586
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/core/pobjects/security/StandardEncryption;->encryptionDictionary:Lorg/icepdf/core/pobjects/security/EncryptionDictionary;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Lorg/icepdf/core/pobjects/security/EncryptionDictionary;->getBigOE()Ljava/lang/String;

    move-result-object v30

    invoke-static/range {v30 .. v30}, Lorg/icepdf/core/util/Utils;->convertByteCharSequenceToByteArray(Ljava/lang/CharSequence;)[B

    move-result-object v18

    .line 588
    .local v18, "oePassword":[B
    move-object/from16 v0, v18

    invoke-static {v11, v0}, Lorg/icepdf/core/pobjects/security/StandardEncryption;->AES256CBC([B[B)[B

    move-result-object v30

    move-object/from16 v0, v30

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/icepdf/core/pobjects/security/StandardEncryption;->encryptionKey:[B

    .line 621
    .end local v18    # "oePassword":[B
    :goto_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/core/pobjects/security/StandardEncryption;->encryptionDictionary:Lorg/icepdf/core/pobjects/security/EncryptionDictionary;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Lorg/icepdf/core/pobjects/security/EncryptionDictionary;->getPerms()Ljava/lang/String;

    move-result-object v27

    .line 622
    .local v27, "stringPerms":Ljava/lang/CharSequence;
    const/16 v26, 0x0

    .line 623
    .local v26, "perms":[B
    if-eqz v27, :cond_7

    .line 624
    invoke-static/range {v27 .. v27}, Lorg/icepdf/core/util/Utils;->convertByteCharSequenceToByteArray(Ljava/lang/CharSequence;)[B

    move-result-object v26

    .line 625
    :cond_7
    const/4 v6, 0x0

    .line 626
    .local v6, "decryptedPerms":[B
    if-eqz v26, :cond_8

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/core/pobjects/security/StandardEncryption;->encryptionKey:[B

    move-object/from16 v30, v0

    if-eqz v30, :cond_8

    .line 627
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/core/pobjects/security/StandardEncryption;->encryptionKey:[B

    move-object/from16 v30, v0

    move-object/from16 v0, v30

    move-object/from16 v1, v26

    invoke-static {v0, v1}, Lorg/icepdf/core/pobjects/security/StandardEncryption;->AES256CBC([B[B)[B

    move-result-object v6

    .line 631
    :cond_8
    if-nez v6, :cond_b

    .line 632
    sget-object v30, Lorg/icepdf/core/pobjects/security/StandardEncryption;->logger:Ljava/util/logging/Logger;

    const-string/jumbo v31, "User password is incorrect."

    invoke-virtual/range {v30 .. v31}, Ljava/util/logging/Logger;->warning(Ljava/lang/String;)V

    .line 633
    const/16 v19, 0x0

    goto/16 :goto_4

    .line 593
    .end local v6    # "decryptedPerms":[B
    .end local v26    # "perms":[B
    .end local v27    # "stringPerms":Ljava/lang/CharSequence;
    :cond_9
    const/16 v30, 0x0

    move-object/from16 v0, v24

    array-length v0, v0

    move/from16 v31, v0

    const/16 v32, 0x7f

    invoke-static/range {v31 .. v32}, Ljava/lang/Math;->min(II)I

    move-result v31

    move-object/from16 v0, v24

    move/from16 v1, v30

    move/from16 v2, v31

    invoke-virtual {v15, v0, v1, v2}, Ljava/security/MessageDigest;->update([BII)V

    .line 595
    const/16 v30, 0x20

    const/16 v31, 0x8

    move-object/from16 v0, v29

    move/from16 v1, v30

    move/from16 v2, v31

    invoke-virtual {v15, v0, v1, v2}, Ljava/security/MessageDigest;->update([BII)V

    .line 596
    invoke-virtual {v15}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v11

    .line 598
    const/16 v30, 0x20

    move-object/from16 v0, v29

    move/from16 v1, v30

    invoke-static {v11, v0, v1}, Lorg/icepdf/core/pobjects/security/StandardEncryption;->byteCompare([B[BI)Z

    move-result v14

    .line 599
    .local v14, "isUserPassword":Z
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/core/pobjects/security/StandardEncryption;->encryptionDictionary:Lorg/icepdf/core/pobjects/security/EncryptionDictionary;

    move-object/from16 v30, v0

    move-object/from16 v0, v30

    invoke-virtual {v0, v14}, Lorg/icepdf/core/pobjects/security/EncryptionDictionary;->setAuthenticatedUserPassword(Z)V

    .line 600
    if-eqz v14, :cond_a

    .line 602
    const/16 v30, 0x0

    move-object/from16 v0, v24

    array-length v0, v0

    move/from16 v31, v0

    const/16 v32, 0x7f

    invoke-static/range {v31 .. v32}, Ljava/lang/Math;->min(II)I

    move-result v31

    move-object/from16 v0, v24

    move/from16 v1, v30

    move/from16 v2, v31

    invoke-virtual {v15, v0, v1, v2}, Ljava/security/MessageDigest;->update([BII)V

    .line 604
    const/16 v30, 0x28

    const/16 v31, 0x8

    move-object/from16 v0, v29

    move/from16 v1, v30

    move/from16 v2, v31

    invoke-virtual {v15, v0, v1, v2}, Ljava/security/MessageDigest;->update([BII)V

    .line 605
    invoke-virtual {v15}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v11

    .line 610
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/core/pobjects/security/StandardEncryption;->encryptionDictionary:Lorg/icepdf/core/pobjects/security/EncryptionDictionary;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Lorg/icepdf/core/pobjects/security/EncryptionDictionary;->getBigUE()Ljava/lang/String;

    move-result-object v30

    invoke-static/range {v30 .. v30}, Lorg/icepdf/core/util/Utils;->convertByteCharSequenceToByteArray(Ljava/lang/CharSequence;)[B

    move-result-object v28

    .line 612
    .local v28, "uePassword":[B
    move-object/from16 v0, v28

    invoke-static {v11, v0}, Lorg/icepdf/core/pobjects/security/StandardEncryption;->AES256CBC([B[B)[B

    move-result-object v30

    move-object/from16 v0, v30

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/icepdf/core/pobjects/security/StandardEncryption;->encryptionKey:[B
    :try_end_1
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_5

    .line 655
    .end local v11    # "hash":[B
    .end local v13    # "isOwnerPassword":Z
    .end local v14    # "isUserPassword":Z
    .end local v15    # "md":Ljava/security/MessageDigest;
    .end local v20    # "ownerPassword":[B
    .end local v24    # "passwordBytes":[B
    .end local v28    # "uePassword":[B
    .end local v29    # "userPassword":[B
    :catch_1
    move-exception v7

    .line 656
    .local v7, "e":Ljava/security/NoSuchAlgorithmException;
    sget-object v30, Lorg/icepdf/core/pobjects/security/StandardEncryption;->logger:Ljava/util/logging/Logger;

    const-string/jumbo v31, "Error computing the the 3.2a Encryption key."

    invoke-virtual/range {v30 .. v31}, Ljava/util/logging/Logger;->warning(Ljava/lang/String;)V

    .line 661
    .end local v7    # "e":Ljava/security/NoSuchAlgorithmException;
    :goto_6
    const/16 v19, 0x0

    goto/16 :goto_4

    .line 614
    .restart local v11    # "hash":[B
    .restart local v13    # "isOwnerPassword":Z
    .restart local v14    # "isUserPassword":Z
    .restart local v15    # "md":Ljava/security/MessageDigest;
    .restart local v20    # "ownerPassword":[B
    .restart local v24    # "passwordBytes":[B
    .restart local v29    # "userPassword":[B
    :cond_a
    :try_start_2
    sget-object v30, Lorg/icepdf/core/pobjects/security/StandardEncryption;->logger:Ljava/util/logging/Logger;

    const-string/jumbo v31, "User password is incorrect. "

    invoke-virtual/range {v30 .. v31}, Ljava/util/logging/Logger;->warning(Ljava/lang/String;)V

    goto/16 :goto_5

    .line 637
    .end local v14    # "isUserPassword":Z
    .restart local v6    # "decryptedPerms":[B
    .restart local v26    # "perms":[B
    .restart local v27    # "stringPerms":Ljava/lang/CharSequence;
    :cond_b
    const/16 v30, 0x9

    aget-byte v30, v6, v30

    const/16 v31, 0x61

    move/from16 v0, v30

    move/from16 v1, v31

    if-ne v0, v1, :cond_c

    const/16 v30, 0xa

    aget-byte v30, v6, v30

    const/16 v31, 0x64

    move/from16 v0, v30

    move/from16 v1, v31

    if-ne v0, v1, :cond_c

    const/16 v30, 0xb

    aget-byte v30, v6, v30

    const/16 v31, 0x62

    move/from16 v0, v30

    move/from16 v1, v31

    if-eq v0, v1, :cond_d

    .line 640
    :cond_c
    sget-object v30, Lorg/icepdf/core/pobjects/security/StandardEncryption;->logger:Ljava/util/logging/Logger;

    const-string/jumbo v31, "User password is incorrect."

    invoke-virtual/range {v30 .. v31}, Ljava/util/logging/Logger;->warning(Ljava/lang/String;)V

    .line 641
    const/16 v19, 0x0

    goto/16 :goto_4

    .line 646
    :cond_d
    const/16 v30, 0x0

    aget-byte v30, v6, v30

    move/from16 v0, v30

    and-int/lit16 v0, v0, 0xff

    move/from16 v30, v0

    const/16 v31, 0x1

    aget-byte v31, v6, v31

    move/from16 v0, v31

    and-int/lit16 v0, v0, 0xff

    move/from16 v31, v0

    shl-int/lit8 v31, v31, 0x8

    or-int v30, v30, v31

    const/16 v31, 0x2

    aget-byte v31, v6, v31

    move/from16 v0, v31

    and-int/lit16 v0, v0, 0xff

    move/from16 v31, v0

    shl-int/lit8 v31, v31, 0x10

    or-int v30, v30, v31

    const/16 v31, 0x2

    aget-byte v31, v6, v31

    move/from16 v0, v31

    and-int/lit16 v0, v0, 0xff

    move/from16 v31, v0

    shl-int/lit8 v31, v31, 0x18

    or-int v25, v30, v31

    .line 650
    .local v25, "permissions":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/core/pobjects/security/StandardEncryption;->encryptionDictionary:Lorg/icepdf/core/pobjects/security/EncryptionDictionary;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Lorg/icepdf/core/pobjects/security/EncryptionDictionary;->getPermissions()I

    move-result v22

    .line 651
    .local v22, "pPermissions":I
    move/from16 v0, v22

    move/from16 v1, v25

    if-eq v0, v1, :cond_e

    .line 652
    sget-object v30, Lorg/icepdf/core/pobjects/security/StandardEncryption;->logger:Ljava/util/logging/Logger;

    const-string/jumbo v31, "Perms and P do not match"

    invoke-virtual/range {v30 .. v31}, Ljava/util/logging/Logger;->warning(Ljava/lang/String;)V

    .line 654
    :cond_e
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/core/pobjects/security/StandardEncryption;->encryptionKey:[B

    move-object/from16 v19, v0
    :try_end_2
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_2 .. :try_end_2} :catch_1

    goto/16 :goto_4

    .line 659
    .end local v6    # "decryptedPerms":[B
    .end local v11    # "hash":[B
    .end local v13    # "isOwnerPassword":Z
    .end local v15    # "md":Ljava/security/MessageDigest;
    .end local v20    # "ownerPassword":[B
    .end local v22    # "pPermissions":I
    .end local v24    # "passwordBytes":[B
    .end local v25    # "permissions":I
    .end local v26    # "perms":[B
    .end local v27    # "stringPerms":Ljava/lang/CharSequence;
    .end local v29    # "userPassword":[B
    :cond_f
    sget-object v30, Lorg/icepdf/core/pobjects/security/StandardEncryption;->logger:Ljava/util/logging/Logger;

    const-string/jumbo v31, "Adobe standard Encryption R = 6 is not supported."

    invoke-virtual/range {v30 .. v31}, Ljava/util/logging/Logger;->warning(Ljava/lang/String;)V

    goto/16 :goto_6
.end method

.method public generalEncryptionAlgorithm(Lorg/icepdf/core/pobjects/Reference;[BLjava/lang/String;[B)[B
    .locals 17
    .param p1, "objectReference"    # Lorg/icepdf/core/pobjects/Reference;
    .param p2, "encryptionKey"    # [B
    .param p3, "algorithmType"    # Ljava/lang/String;
    .param p4, "inputData"    # [B

    .prologue
    .line 140
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    if-nez p4, :cond_1

    .line 143
    :cond_0
    const/4 v4, 0x0

    .line 265
    :goto_0
    return-object v4

    .line 147
    :cond_1
    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/icepdf/core/pobjects/security/StandardEncryption;->encryptionDictionary:Lorg/icepdf/core/pobjects/security/EncryptionDictionary;

    invoke-virtual {v13}, Lorg/icepdf/core/pobjects/security/EncryptionDictionary;->getVersion()I

    move-result v13

    const/4 v14, 0x5

    if-ge v13, v14, :cond_5

    .line 150
    const-string/jumbo v13, "V2"

    move-object/from16 v0, p3

    invoke-virtual {v0, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    .line 154
    .local v8, "isRc4":Z
    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/icepdf/core/pobjects/security/StandardEncryption;->rc4Key:[B

    if-eqz v13, :cond_2

    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/icepdf/core/pobjects/security/StandardEncryption;->encryptionKey:[B

    move-object/from16 v0, p2

    if-ne v13, v0, :cond_2

    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/icepdf/core/pobjects/security/StandardEncryption;->objectReference:Lorg/icepdf/core/pobjects/Reference;

    move-object/from16 v0, p1

    if-eq v13, v0, :cond_3

    .line 157
    :cond_2
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/icepdf/core/pobjects/security/StandardEncryption;->objectReference:Lorg/icepdf/core/pobjects/Reference;

    .line 160
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v8}, Lorg/icepdf/core/pobjects/security/StandardEncryption;->resetObjectReference(Lorg/icepdf/core/pobjects/Reference;Z)[B

    move-result-object v12

    .line 164
    .local v12, "step3Bytes":[B
    move-object/from16 v0, p2

    array-length v10, v0

    .line 165
    .local v10, "n":I
    add-int/lit8 v13, v10, 0x5

    const/16 v14, 0x10

    invoke-static {v13, v14}, Ljava/lang/Math;->min(II)I

    move-result v13

    new-array v13, v13, [B

    move-object/from16 v0, p0

    iput-object v13, v0, Lorg/icepdf/core/pobjects/security/StandardEncryption;->rc4Key:[B

    .line 166
    const/4 v13, 0x0

    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/icepdf/core/pobjects/security/StandardEncryption;->rc4Key:[B

    const/4 v15, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/core/pobjects/security/StandardEncryption;->rc4Key:[B

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    array-length v0, v0

    move/from16 v16, v0

    invoke-static/range {v12 .. v16}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 170
    .end local v10    # "n":I
    .end local v12    # "step3Bytes":[B
    :cond_3
    const/4 v4, 0x0

    .line 173
    .local v4, "finalData":[B
    if-eqz v8, :cond_4

    .line 175
    :try_start_0
    new-instance v9, Ljavax/crypto/spec/SecretKeySpec;

    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/icepdf/core/pobjects/security/StandardEncryption;->rc4Key:[B

    const-string/jumbo v14, "RC4"

    invoke-direct {v9, v13, v14}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    .line 176
    .local v9, "key":Ljavax/crypto/spec/SecretKeySpec;
    const-string/jumbo v13, "RC4"

    invoke-static {v13}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v11

    .line 177
    .local v11, "rc4":Ljavax/crypto/Cipher;
    const/4 v13, 0x2

    invoke-virtual {v11, v13, v9}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;)V

    .line 180
    move-object/from16 v0, p4

    invoke-virtual {v11, v0}, Ljavax/crypto/Cipher;->doFinal([B)[B

    move-result-object v4

    .line 181
    goto :goto_0

    .line 182
    .end local v9    # "key":Ljavax/crypto/spec/SecretKeySpec;
    .end local v11    # "rc4":Ljavax/crypto/Cipher;
    :cond_4
    new-instance v9, Ljavax/crypto/spec/SecretKeySpec;

    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/icepdf/core/pobjects/security/StandardEncryption;->rc4Key:[B

    const-string/jumbo v14, "AES"

    invoke-direct {v9, v13, v14}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    .line 183
    .restart local v9    # "key":Ljavax/crypto/spec/SecretKeySpec;
    const-string/jumbo v13, "AES/CBC/PKCS5Padding"

    invoke-static {v13}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v2

    .line 186
    .local v2, "aes":Ljavax/crypto/Cipher;
    const/16 v13, 0x10

    new-array v6, v13, [B

    .line 187
    .local v6, "initialisationVector":[B
    const/4 v13, 0x0

    const/4 v14, 0x0

    const/16 v15, 0x10

    move-object/from16 v0, p4

    invoke-static {v0, v13, v6, v14, v15}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 190
    move-object/from16 v0, p4

    array-length v13, v0

    add-int/lit8 v13, v13, -0x10

    new-array v7, v13, [B

    .line 191
    .local v7, "intermData":[B
    const/16 v13, 0x10

    const/4 v14, 0x0

    array-length v15, v7

    move-object/from16 v0, p4

    invoke-static {v0, v13, v7, v14, v15}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 193
    new-instance v5, Ljavax/crypto/spec/IvParameterSpec;

    invoke-direct {v5, v6}, Ljavax/crypto/spec/IvParameterSpec;-><init>([B)V

    .line 196
    .local v5, "iVParameterSpec":Ljavax/crypto/spec/IvParameterSpec;
    const/4 v13, 0x2

    invoke-virtual {v2, v13, v9, v5}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;Ljava/security/spec/AlgorithmParameterSpec;)V

    .line 199
    invoke-virtual {v2, v7}, Ljavax/crypto/Cipher;->doFinal([B)[B
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljavax/crypto/IllegalBlockSizeException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljavax/crypto/BadPaddingException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljavax/crypto/NoSuchPaddingException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/security/InvalidKeyException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/security/InvalidAlgorithmParameterException; {:try_start_0 .. :try_end_0} :catch_5

    move-result-object v4

    goto/16 :goto_0

    .line 202
    .end local v2    # "aes":Ljavax/crypto/Cipher;
    .end local v5    # "iVParameterSpec":Ljavax/crypto/spec/IvParameterSpec;
    .end local v6    # "initialisationVector":[B
    .end local v7    # "intermData":[B
    .end local v9    # "key":Ljavax/crypto/spec/SecretKeySpec;
    :catch_0
    move-exception v3

    .line 203
    .local v3, "ex":Ljava/security/NoSuchAlgorithmException;
    sget-object v13, Lorg/icepdf/core/pobjects/security/StandardEncryption;->logger:Ljava/util/logging/Logger;

    sget-object v14, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string/jumbo v15, "NoSuchAlgorithmException."

    invoke-virtual {v13, v14, v15, v3}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0

    .line 204
    .end local v3    # "ex":Ljava/security/NoSuchAlgorithmException;
    :catch_1
    move-exception v3

    .line 205
    .local v3, "ex":Ljavax/crypto/IllegalBlockSizeException;
    sget-object v13, Lorg/icepdf/core/pobjects/security/StandardEncryption;->logger:Ljava/util/logging/Logger;

    sget-object v14, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string/jumbo v15, "IllegalBlockSizeException."

    invoke-virtual {v13, v14, v15, v3}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0

    .line 206
    .end local v3    # "ex":Ljavax/crypto/IllegalBlockSizeException;
    :catch_2
    move-exception v3

    .line 207
    .local v3, "ex":Ljavax/crypto/BadPaddingException;
    sget-object v13, Lorg/icepdf/core/pobjects/security/StandardEncryption;->logger:Ljava/util/logging/Logger;

    sget-object v14, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string/jumbo v15, "BadPaddingException."

    invoke-virtual {v13, v14, v15, v3}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0

    .line 208
    .end local v3    # "ex":Ljavax/crypto/BadPaddingException;
    :catch_3
    move-exception v3

    .line 209
    .local v3, "ex":Ljavax/crypto/NoSuchPaddingException;
    sget-object v13, Lorg/icepdf/core/pobjects/security/StandardEncryption;->logger:Ljava/util/logging/Logger;

    sget-object v14, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string/jumbo v15, "NoSuchPaddingException."

    invoke-virtual {v13, v14, v15, v3}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0

    .line 210
    .end local v3    # "ex":Ljavax/crypto/NoSuchPaddingException;
    :catch_4
    move-exception v3

    .line 211
    .local v3, "ex":Ljava/security/InvalidKeyException;
    sget-object v13, Lorg/icepdf/core/pobjects/security/StandardEncryption;->logger:Ljava/util/logging/Logger;

    sget-object v14, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string/jumbo v15, "InvalidKeyException."

    invoke-virtual {v13, v14, v15, v3}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0

    .line 212
    .end local v3    # "ex":Ljava/security/InvalidKeyException;
    :catch_5
    move-exception v3

    .line 213
    .local v3, "ex":Ljava/security/InvalidAlgorithmParameterException;
    sget-object v13, Lorg/icepdf/core/pobjects/security/StandardEncryption;->logger:Ljava/util/logging/Logger;

    sget-object v14, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string/jumbo v15, "InvalidAlgorithmParameterException"

    invoke-virtual {v13, v14, v15, v3}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0

    .line 219
    .end local v3    # "ex":Ljava/security/InvalidAlgorithmParameterException;
    .end local v4    # "finalData":[B
    .end local v8    # "isRc4":Z
    :cond_5
    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/icepdf/core/pobjects/security/StandardEncryption;->encryptionDictionary:Lorg/icepdf/core/pobjects/security/EncryptionDictionary;

    invoke-virtual {v13}, Lorg/icepdf/core/pobjects/security/EncryptionDictionary;->getVersion()I

    move-result v13

    const/4 v14, 0x5

    if-ne v13, v14, :cond_6

    .line 229
    const/4 v4, 0x0

    .line 231
    .restart local v4    # "finalData":[B
    :try_start_1
    new-instance v9, Ljavax/crypto/spec/SecretKeySpec;

    const-string/jumbo v13, "AES"

    move-object/from16 v0, p2

    invoke-direct {v9, v0, v13}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    .line 232
    .restart local v9    # "key":Ljavax/crypto/spec/SecretKeySpec;
    const-string/jumbo v13, "AES/CBC/PKCS5Padding"

    invoke-static {v13}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v2

    .line 235
    .restart local v2    # "aes":Ljavax/crypto/Cipher;
    const/16 v13, 0x10

    new-array v6, v13, [B

    .line 236
    .restart local v6    # "initialisationVector":[B
    const/4 v13, 0x0

    const/4 v14, 0x0

    const/16 v15, 0x10

    move-object/from16 v0, p4

    invoke-static {v0, v13, v6, v14, v15}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 239
    move-object/from16 v0, p4

    array-length v13, v0

    add-int/lit8 v13, v13, -0x10

    new-array v7, v13, [B

    .line 240
    .restart local v7    # "intermData":[B
    const/16 v13, 0x10

    const/4 v14, 0x0

    array-length v15, v7

    move-object/from16 v0, p4

    invoke-static {v0, v13, v7, v14, v15}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 242
    new-instance v5, Ljavax/crypto/spec/IvParameterSpec;

    invoke-direct {v5, v6}, Ljavax/crypto/spec/IvParameterSpec;-><init>([B)V

    .line 245
    .restart local v5    # "iVParameterSpec":Ljavax/crypto/spec/IvParameterSpec;
    const/4 v13, 0x2

    invoke-virtual {v2, v13, v9, v5}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;Ljava/security/spec/AlgorithmParameterSpec;)V

    .line 248
    invoke-virtual {v2, v7}, Ljavax/crypto/Cipher;->doFinal([B)[B
    :try_end_1
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_1 .. :try_end_1} :catch_6
    .catch Ljavax/crypto/IllegalBlockSizeException; {:try_start_1 .. :try_end_1} :catch_7
    .catch Ljavax/crypto/BadPaddingException; {:try_start_1 .. :try_end_1} :catch_8
    .catch Ljavax/crypto/NoSuchPaddingException; {:try_start_1 .. :try_end_1} :catch_9
    .catch Ljava/security/InvalidKeyException; {:try_start_1 .. :try_end_1} :catch_a
    .catch Ljava/security/InvalidAlgorithmParameterException; {:try_start_1 .. :try_end_1} :catch_b

    move-result-object v4

    .line 249
    goto/16 :goto_0

    .line 251
    .end local v2    # "aes":Ljavax/crypto/Cipher;
    .end local v5    # "iVParameterSpec":Ljavax/crypto/spec/IvParameterSpec;
    .end local v6    # "initialisationVector":[B
    .end local v7    # "intermData":[B
    .end local v9    # "key":Ljavax/crypto/spec/SecretKeySpec;
    :catch_6
    move-exception v3

    .line 252
    .local v3, "ex":Ljava/security/NoSuchAlgorithmException;
    sget-object v13, Lorg/icepdf/core/pobjects/security/StandardEncryption;->logger:Ljava/util/logging/Logger;

    sget-object v14, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string/jumbo v15, "NoSuchAlgorithmException."

    invoke-virtual {v13, v14, v15, v3}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 265
    .end local v3    # "ex":Ljava/security/NoSuchAlgorithmException;
    .end local v4    # "finalData":[B
    :cond_6
    :goto_1
    const/4 v4, 0x0

    goto/16 :goto_0

    .line 253
    .restart local v4    # "finalData":[B
    :catch_7
    move-exception v3

    .line 254
    .local v3, "ex":Ljavax/crypto/IllegalBlockSizeException;
    sget-object v13, Lorg/icepdf/core/pobjects/security/StandardEncryption;->logger:Ljava/util/logging/Logger;

    sget-object v14, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string/jumbo v15, "IllegalBlockSizeException."

    invoke-virtual {v13, v14, v15, v3}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 255
    .end local v3    # "ex":Ljavax/crypto/IllegalBlockSizeException;
    :catch_8
    move-exception v3

    .line 256
    .local v3, "ex":Ljavax/crypto/BadPaddingException;
    sget-object v13, Lorg/icepdf/core/pobjects/security/StandardEncryption;->logger:Ljava/util/logging/Logger;

    sget-object v14, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string/jumbo v15, "BadPaddingException."

    invoke-virtual {v13, v14, v15, v3}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 257
    .end local v3    # "ex":Ljavax/crypto/BadPaddingException;
    :catch_9
    move-exception v3

    .line 258
    .local v3, "ex":Ljavax/crypto/NoSuchPaddingException;
    sget-object v13, Lorg/icepdf/core/pobjects/security/StandardEncryption;->logger:Ljava/util/logging/Logger;

    sget-object v14, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string/jumbo v15, "NoSuchPaddingException."

    invoke-virtual {v13, v14, v15, v3}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 259
    .end local v3    # "ex":Ljavax/crypto/NoSuchPaddingException;
    :catch_a
    move-exception v3

    .line 260
    .local v3, "ex":Ljava/security/InvalidKeyException;
    sget-object v13, Lorg/icepdf/core/pobjects/security/StandardEncryption;->logger:Ljava/util/logging/Logger;

    sget-object v14, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string/jumbo v15, "InvalidKeyException."

    invoke-virtual {v13, v14, v15, v3}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 261
    .end local v3    # "ex":Ljava/security/InvalidKeyException;
    :catch_b
    move-exception v3

    .line 262
    .local v3, "ex":Ljava/security/InvalidAlgorithmParameterException;
    sget-object v13, Lorg/icepdf/core/pobjects/security/StandardEncryption;->logger:Ljava/util/logging/Logger;

    sget-object v14, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string/jumbo v15, "InvalidAlgorithmParameterException"

    invoke-virtual {v13, v14, v15, v3}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method public generalEncryptionInputStream(Lorg/icepdf/core/pobjects/Reference;[BLjava/lang/String;Ljava/io/InputStream;)Ljava/io/InputStream;
    .locals 15
    .param p1, "objectReference"    # Lorg/icepdf/core/pobjects/Reference;
    .param p2, "encryptionKey"    # [B
    .param p3, "algorithmType"    # Ljava/lang/String;
    .param p4, "input"    # Ljava/io/InputStream;

    .prologue
    .line 277
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    if-nez p4, :cond_1

    .line 279
    :cond_0
    const/4 v2, 0x0

    .line 386
    :goto_0
    return-object v2

    .line 283
    :cond_1
    iget-object v11, p0, Lorg/icepdf/core/pobjects/security/StandardEncryption;->encryptionDictionary:Lorg/icepdf/core/pobjects/security/EncryptionDictionary;

    invoke-virtual {v11}, Lorg/icepdf/core/pobjects/security/EncryptionDictionary;->getVersion()I

    move-result v11

    const/4 v12, 0x5

    if-ge v11, v12, :cond_6

    .line 285
    const-string/jumbo v11, "V2"

    move-object/from16 v0, p3

    invoke-virtual {v0, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    .line 289
    .local v6, "isRc4":Z
    iget-object v11, p0, Lorg/icepdf/core/pobjects/security/StandardEncryption;->rc4Key:[B

    if-eqz v11, :cond_2

    iget-object v11, p0, Lorg/icepdf/core/pobjects/security/StandardEncryption;->encryptionKey:[B

    move-object/from16 v0, p2

    if-ne v11, v0, :cond_2

    iget-object v11, p0, Lorg/icepdf/core/pobjects/security/StandardEncryption;->objectReference:Lorg/icepdf/core/pobjects/Reference;

    move-object/from16 v0, p1

    if-eq v11, v0, :cond_3

    .line 292
    :cond_2
    move-object/from16 v0, p1

    iput-object v0, p0, Lorg/icepdf/core/pobjects/security/StandardEncryption;->objectReference:Lorg/icepdf/core/pobjects/Reference;

    .line 295
    move-object/from16 v0, p1

    invoke-virtual {p0, v0, v6}, Lorg/icepdf/core/pobjects/security/StandardEncryption;->resetObjectReference(Lorg/icepdf/core/pobjects/Reference;Z)[B

    move-result-object v10

    .line 299
    .local v10, "step3Bytes":[B
    move-object/from16 v0, p2

    array-length v8, v0

    .line 300
    .local v8, "n":I
    add-int/lit8 v11, v8, 0x5

    const/16 v12, 0x10

    invoke-static {v11, v12}, Ljava/lang/Math;->min(II)I

    move-result v11

    new-array v11, v11, [B

    iput-object v11, p0, Lorg/icepdf/core/pobjects/security/StandardEncryption;->rc4Key:[B

    .line 301
    const/4 v11, 0x0

    iget-object v12, p0, Lorg/icepdf/core/pobjects/security/StandardEncryption;->rc4Key:[B

    const/4 v13, 0x0

    iget-object v14, p0, Lorg/icepdf/core/pobjects/security/StandardEncryption;->rc4Key:[B

    array-length v14, v14

    invoke-static {v10, v11, v12, v13, v14}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 307
    .end local v8    # "n":I
    .end local v10    # "step3Bytes":[B
    :cond_3
    if-eqz v6, :cond_4

    .line 308
    :try_start_0
    new-instance v7, Ljavax/crypto/spec/SecretKeySpec;

    iget-object v11, p0, Lorg/icepdf/core/pobjects/security/StandardEncryption;->rc4Key:[B

    const-string/jumbo v12, "RC4"

    invoke-direct {v7, v11, v12}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    .line 309
    .local v7, "key":Ljavax/crypto/spec/SecretKeySpec;
    const-string/jumbo v11, "RC4"

    invoke-static {v11}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v9

    .line 310
    .local v9, "rc4":Ljavax/crypto/Cipher;
    const/4 v11, 0x2

    invoke-virtual {v9, v11, v7}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;)V

    .line 313
    new-instance v2, Ljavax/crypto/CipherInputStream;

    move-object/from16 v0, p4

    invoke-direct {v2, v0, v9}, Ljavax/crypto/CipherInputStream;-><init>(Ljava/io/InputStream;Ljavax/crypto/Cipher;)V

    .line 314
    .local v2, "cin":Ljavax/crypto/CipherInputStream;
    goto :goto_0

    .line 318
    .end local v2    # "cin":Ljavax/crypto/CipherInputStream;
    .end local v7    # "key":Ljavax/crypto/spec/SecretKeySpec;
    .end local v9    # "rc4":Ljavax/crypto/Cipher;
    :cond_4
    new-instance v7, Ljavax/crypto/spec/SecretKeySpec;

    iget-object v11, p0, Lorg/icepdf/core/pobjects/security/StandardEncryption;->rc4Key:[B

    const-string/jumbo v12, "AES"

    invoke-direct {v7, v11, v12}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    .line 319
    .restart local v7    # "key":Ljavax/crypto/spec/SecretKeySpec;
    const-string/jumbo v11, "AES/CBC/PKCS5Padding"

    invoke-static {v11}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v1

    .line 322
    .local v1, "aes":Ljavax/crypto/Cipher;
    const/16 v11, 0x10

    new-array v5, v11, [B

    .line 323
    .local v5, "initialisationVector":[B
    move-object/from16 v0, p4

    invoke-virtual {v0, v5}, Ljava/io/InputStream;->read([B)I

    .line 325
    new-instance v4, Ljavax/crypto/spec/IvParameterSpec;

    invoke-direct {v4, v5}, Ljavax/crypto/spec/IvParameterSpec;-><init>([B)V

    .line 328
    .local v4, "iVParameterSpec":Ljavax/crypto/spec/IvParameterSpec;
    const/4 v11, 0x2

    invoke-virtual {v1, v11, v7, v4}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;Ljava/security/spec/AlgorithmParameterSpec;)V

    .line 331
    new-instance v2, Ljavax/crypto/CipherInputStream;

    move-object/from16 v0, p4

    invoke-direct {v2, v0, v1}, Ljavax/crypto/CipherInputStream;-><init>(Ljava/io/InputStream;Ljavax/crypto/Cipher;)V
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljavax/crypto/NoSuchPaddingException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/security/InvalidKeyException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/security/InvalidAlgorithmParameterException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_4

    .line 332
    .restart local v2    # "cin":Ljavax/crypto/CipherInputStream;
    goto/16 :goto_0

    .line 334
    .end local v1    # "aes":Ljavax/crypto/Cipher;
    .end local v2    # "cin":Ljavax/crypto/CipherInputStream;
    .end local v4    # "iVParameterSpec":Ljavax/crypto/spec/IvParameterSpec;
    .end local v5    # "initialisationVector":[B
    .end local v7    # "key":Ljavax/crypto/spec/SecretKeySpec;
    :catch_0
    move-exception v3

    .line 335
    .local v3, "ex":Ljava/security/NoSuchAlgorithmException;
    sget-object v11, Lorg/icepdf/core/pobjects/security/StandardEncryption;->logger:Ljava/util/logging/Logger;

    sget-object v12, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string/jumbo v13, "NoSuchAlgorithmException."

    invoke-virtual {v11, v12, v13, v3}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 386
    .end local v3    # "ex":Ljava/security/NoSuchAlgorithmException;
    .end local v6    # "isRc4":Z
    :cond_5
    :goto_1
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 336
    .restart local v6    # "isRc4":Z
    :catch_1
    move-exception v3

    .line 337
    .local v3, "ex":Ljavax/crypto/NoSuchPaddingException;
    sget-object v11, Lorg/icepdf/core/pobjects/security/StandardEncryption;->logger:Ljava/util/logging/Logger;

    sget-object v12, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string/jumbo v13, "NoSuchPaddingException."

    invoke-virtual {v11, v12, v13, v3}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 338
    .end local v3    # "ex":Ljavax/crypto/NoSuchPaddingException;
    :catch_2
    move-exception v3

    .line 339
    .local v3, "ex":Ljava/security/InvalidKeyException;
    sget-object v11, Lorg/icepdf/core/pobjects/security/StandardEncryption;->logger:Ljava/util/logging/Logger;

    sget-object v12, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string/jumbo v13, "InvalidKeyException."

    invoke-virtual {v11, v12, v13, v3}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 340
    .end local v3    # "ex":Ljava/security/InvalidKeyException;
    :catch_3
    move-exception v3

    .line 341
    .local v3, "ex":Ljava/security/InvalidAlgorithmParameterException;
    sget-object v11, Lorg/icepdf/core/pobjects/security/StandardEncryption;->logger:Ljava/util/logging/Logger;

    sget-object v12, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string/jumbo v13, "InvalidAlgorithmParameterException"

    invoke-virtual {v11, v12, v13, v3}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 342
    .end local v3    # "ex":Ljava/security/InvalidAlgorithmParameterException;
    :catch_4
    move-exception v3

    .line 343
    .local v3, "ex":Ljava/io/IOException;
    sget-object v11, Lorg/icepdf/core/pobjects/security/StandardEncryption;->logger:Ljava/util/logging/Logger;

    sget-object v12, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string/jumbo v13, "InvalidAlgorithmParameterException"

    invoke-virtual {v11, v12, v13, v3}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 347
    .end local v3    # "ex":Ljava/io/IOException;
    .end local v6    # "isRc4":Z
    :cond_6
    iget-object v11, p0, Lorg/icepdf/core/pobjects/security/StandardEncryption;->encryptionDictionary:Lorg/icepdf/core/pobjects/security/EncryptionDictionary;

    invoke-virtual {v11}, Lorg/icepdf/core/pobjects/security/EncryptionDictionary;->getVersion()I

    move-result v11

    const/4 v12, 0x5

    if-ne v11, v12, :cond_5

    .line 358
    :try_start_1
    new-instance v7, Ljavax/crypto/spec/SecretKeySpec;

    const-string/jumbo v11, "AES"

    move-object/from16 v0, p2

    invoke-direct {v7, v0, v11}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    .line 359
    .restart local v7    # "key":Ljavax/crypto/spec/SecretKeySpec;
    const-string/jumbo v11, "AES/CBC/PKCS5Padding"

    invoke-static {v11}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v1

    .line 362
    .restart local v1    # "aes":Ljavax/crypto/Cipher;
    const/16 v11, 0x10

    new-array v5, v11, [B

    .line 363
    .restart local v5    # "initialisationVector":[B
    move-object/from16 v0, p4

    invoke-virtual {v0, v5}, Ljava/io/InputStream;->read([B)I

    .line 365
    new-instance v4, Ljavax/crypto/spec/IvParameterSpec;

    invoke-direct {v4, v5}, Ljavax/crypto/spec/IvParameterSpec;-><init>([B)V

    .line 368
    .restart local v4    # "iVParameterSpec":Ljavax/crypto/spec/IvParameterSpec;
    const/4 v11, 0x2

    invoke-virtual {v1, v11, v7, v4}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;Ljava/security/spec/AlgorithmParameterSpec;)V

    .line 371
    new-instance v2, Ljavax/crypto/CipherInputStream;

    move-object/from16 v0, p4

    invoke-direct {v2, v0, v1}, Ljavax/crypto/CipherInputStream;-><init>(Ljava/io/InputStream;Ljavax/crypto/Cipher;)V
    :try_end_1
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_1 .. :try_end_1} :catch_5
    .catch Ljavax/crypto/NoSuchPaddingException; {:try_start_1 .. :try_end_1} :catch_6
    .catch Ljava/security/InvalidKeyException; {:try_start_1 .. :try_end_1} :catch_7
    .catch Ljava/security/InvalidAlgorithmParameterException; {:try_start_1 .. :try_end_1} :catch_8
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_9

    .line 372
    .restart local v2    # "cin":Ljavax/crypto/CipherInputStream;
    goto/16 :goto_0

    .line 374
    .end local v1    # "aes":Ljavax/crypto/Cipher;
    .end local v2    # "cin":Ljavax/crypto/CipherInputStream;
    .end local v4    # "iVParameterSpec":Ljavax/crypto/spec/IvParameterSpec;
    .end local v5    # "initialisationVector":[B
    .end local v7    # "key":Ljavax/crypto/spec/SecretKeySpec;
    :catch_5
    move-exception v3

    .line 375
    .local v3, "ex":Ljava/security/NoSuchAlgorithmException;
    sget-object v11, Lorg/icepdf/core/pobjects/security/StandardEncryption;->logger:Ljava/util/logging/Logger;

    sget-object v12, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string/jumbo v13, "NoSuchAlgorithmException."

    invoke-virtual {v11, v12, v13, v3}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 376
    .end local v3    # "ex":Ljava/security/NoSuchAlgorithmException;
    :catch_6
    move-exception v3

    .line 377
    .local v3, "ex":Ljavax/crypto/NoSuchPaddingException;
    sget-object v11, Lorg/icepdf/core/pobjects/security/StandardEncryption;->logger:Ljava/util/logging/Logger;

    sget-object v12, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string/jumbo v13, "NoSuchPaddingException."

    invoke-virtual {v11, v12, v13, v3}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 378
    .end local v3    # "ex":Ljavax/crypto/NoSuchPaddingException;
    :catch_7
    move-exception v3

    .line 379
    .local v3, "ex":Ljava/security/InvalidKeyException;
    sget-object v11, Lorg/icepdf/core/pobjects/security/StandardEncryption;->logger:Ljava/util/logging/Logger;

    sget-object v12, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string/jumbo v13, "InvalidKeyException."

    invoke-virtual {v11, v12, v13, v3}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_1

    .line 380
    .end local v3    # "ex":Ljava/security/InvalidKeyException;
    :catch_8
    move-exception v3

    .line 381
    .local v3, "ex":Ljava/security/InvalidAlgorithmParameterException;
    sget-object v11, Lorg/icepdf/core/pobjects/security/StandardEncryption;->logger:Ljava/util/logging/Logger;

    sget-object v12, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string/jumbo v13, "InvalidAlgorithmParameterException"

    invoke-virtual {v11, v12, v13, v3}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_1

    .line 382
    .end local v3    # "ex":Ljava/security/InvalidAlgorithmParameterException;
    :catch_9
    move-exception v3

    .line 383
    .local v3, "ex":Ljava/io/IOException;
    sget-object v11, Lorg/icepdf/core/pobjects/security/StandardEncryption;->logger:Ljava/util/logging/Logger;

    sget-object v12, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string/jumbo v13, "InvalidAlgorithmParameterException"

    invoke-virtual {v11, v12, v13, v3}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_1
.end method

.method public getOwnerPassword()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1129
    iget-object v0, p0, Lorg/icepdf/core/pobjects/security/StandardEncryption;->ownerPassword:Ljava/lang/String;

    return-object v0
.end method

.method public getUserPassword()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1125
    iget-object v0, p0, Lorg/icepdf/core/pobjects/security/StandardEncryption;->userPassword:Ljava/lang/String;

    return-object v0
.end method

.method public resetObjectReference(Lorg/icepdf/core/pobjects/Reference;Z)[B
    .locals 10
    .param p1, "objectReference"    # Lorg/icepdf/core/pobjects/Reference;
    .param p2, "isRc4"    # Z

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 415
    invoke-virtual {p1}, Lorg/icepdf/core/pobjects/Reference;->getObjectNumber()I

    move-result v3

    .line 416
    .local v3, "objectNumber":I
    invoke-virtual {p1}, Lorg/icepdf/core/pobjects/Reference;->getGenerationNumber()I

    move-result v0

    .line 420
    .local v0, "generationNumber":I
    const/4 v2, 0x5

    .line 421
    .local v2, "n":I
    iget-object v6, p0, Lorg/icepdf/core/pobjects/security/StandardEncryption;->encryptionDictionary:Lorg/icepdf/core/pobjects/security/EncryptionDictionary;

    invoke-virtual {v6}, Lorg/icepdf/core/pobjects/security/EncryptionDictionary;->getVersion()I

    move-result v6

    if-le v6, v9, :cond_0

    .line 422
    iget-object v6, p0, Lorg/icepdf/core/pobjects/security/StandardEncryption;->encryptionDictionary:Lorg/icepdf/core/pobjects/security/EncryptionDictionary;

    invoke-virtual {v6}, Lorg/icepdf/core/pobjects/security/EncryptionDictionary;->getKeyLength()I

    move-result v6

    div-int/lit8 v2, v6, 0x8

    .line 426
    :cond_0
    const/4 v4, 0x5

    .line 427
    .local v4, "paddingLength":I
    if-nez p2, :cond_1

    .line 428
    add-int/lit8 v4, v4, 0x4

    .line 431
    :cond_1
    add-int v6, v2, v4

    new-array v5, v6, [B

    .line 434
    .local v5, "step2Bytes":[B
    iget-object v6, p0, Lorg/icepdf/core/pobjects/security/StandardEncryption;->encryptionKey:[B

    invoke-static {v6, v8, v5, v8, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 437
    and-int/lit16 v6, v3, 0xff

    int-to-byte v6, v6

    aput-byte v6, v5, v2

    .line 438
    add-int/lit8 v6, v2, 0x1

    shr-int/lit8 v7, v3, 0x8

    and-int/lit16 v7, v7, 0xff

    int-to-byte v7, v7

    aput-byte v7, v5, v6

    .line 439
    add-int/lit8 v6, v2, 0x2

    shr-int/lit8 v7, v3, 0x10

    and-int/lit16 v7, v7, 0xff

    int-to-byte v7, v7

    aput-byte v7, v5, v6

    .line 441
    add-int/lit8 v6, v2, 0x3

    and-int/lit16 v7, v0, 0xff

    int-to-byte v7, v7

    aput-byte v7, v5, v6

    .line 442
    add-int/lit8 v6, v2, 0x4

    shr-int/lit8 v7, v0, 0x8

    and-int/lit16 v7, v7, 0xff

    int-to-byte v7, v7

    aput-byte v7, v5, v6

    .line 445
    if-nez p2, :cond_2

    .line 446
    add-int/lit8 v6, v2, 0x5

    sget-object v7, Lorg/icepdf/core/pobjects/security/StandardEncryption;->AES_sAIT:[B

    aget-byte v7, v7, v8

    aput-byte v7, v5, v6

    .line 447
    add-int/lit8 v6, v2, 0x6

    sget-object v7, Lorg/icepdf/core/pobjects/security/StandardEncryption;->AES_sAIT:[B

    aget-byte v7, v7, v9

    aput-byte v7, v5, v6

    .line 448
    add-int/lit8 v6, v2, 0x7

    sget-object v7, Lorg/icepdf/core/pobjects/security/StandardEncryption;->AES_sAIT:[B

    const/4 v8, 0x2

    aget-byte v7, v7, v8

    aput-byte v7, v5, v6

    .line 449
    add-int/lit8 v6, v2, 0x8

    sget-object v7, Lorg/icepdf/core/pobjects/security/StandardEncryption;->AES_sAIT:[B

    const/4 v8, 0x3

    aget-byte v7, v7, v8

    aput-byte v7, v5, v6

    .line 453
    :cond_2
    const/4 v1, 0x0

    .line 455
    .local v1, "md5":Ljava/security/MessageDigest;
    :try_start_0
    const-string/jumbo v6, "MD5"

    invoke-static {v6}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 459
    :goto_0
    invoke-virtual {v1, v5}, Ljava/security/MessageDigest;->update([B)V

    .line 462
    invoke-virtual {v1}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v6

    return-object v6

    .line 456
    :catch_0
    move-exception v6

    goto :goto_0
.end method
