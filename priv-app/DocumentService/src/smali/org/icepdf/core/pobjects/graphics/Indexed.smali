.class public Lorg/icepdf/core/pobjects/graphics/Indexed;
.super Lorg/icepdf/core/pobjects/graphics/PColorSpace;
.source "Indexed.java"


# instance fields
.field colorSpace:Lorg/icepdf/core/pobjects/graphics/PColorSpace;

.field colors:[B

.field private cols:[Lorg/apache/poi/java/awt/Color;

.field hival:I

.field private inited:Z


# direct methods
.method constructor <init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;Ljava/util/Vector;)V
    .locals 7
    .param p1, "library"    # Lorg/icepdf/core/util/Library;
    .param p2, "entries"    # Ljava/util/Hashtable;
    .param p3, "dictionary"    # Ljava/util/Vector;

    .prologue
    const/4 v6, 0x3

    .line 51
    invoke-direct {p0, p1, p2}, Lorg/icepdf/core/pobjects/graphics/PColorSpace;-><init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V

    .line 32
    const/4 v5, 0x6

    new-array v5, v5, [B

    fill-array-data v5, :array_0

    iput-object v5, p0, Lorg/icepdf/core/pobjects/graphics/Indexed;->colors:[B

    .line 35
    const/4 v5, 0x0

    iput-boolean v5, p0, Lorg/icepdf/core/pobjects/graphics/Indexed;->inited:Z

    .line 53
    const/4 v5, 0x1

    invoke-virtual {p3, v5}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v5

    invoke-static {p1, v5}, Lorg/icepdf/core/pobjects/graphics/Indexed;->getColorSpace(Lorg/icepdf/core/util/Library;Ljava/lang/Object;)Lorg/icepdf/core/pobjects/graphics/PColorSpace;

    move-result-object v5

    iput-object v5, p0, Lorg/icepdf/core/pobjects/graphics/Indexed;->colorSpace:Lorg/icepdf/core/pobjects/graphics/PColorSpace;

    .line 55
    const/4 v5, 0x2

    invoke-virtual {p3, v5}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Number;

    check-cast v5, Ljava/lang/Number;

    invoke-virtual {v5}, Ljava/lang/Number;->intValue()I

    move-result v5

    iput v5, p0, Lorg/icepdf/core/pobjects/graphics/Indexed;->hival:I

    .line 57
    invoke-virtual {p3, v6}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v5

    instance-of v5, v5, Lorg/icepdf/core/pobjects/StringObject;

    if-eqz v5, :cond_2

    .line 59
    invoke-virtual {p3, v6}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/icepdf/core/pobjects/StringObject;

    .line 60
    .local v4, "tmpText":Lorg/icepdf/core/pobjects/StringObject;
    iget-object v5, p1, Lorg/icepdf/core/util/Library;->securityManager:Lorg/icepdf/core/pobjects/security/SecurityManager;

    invoke-interface {v4, v5}, Lorg/icepdf/core/pobjects/StringObject;->getDecryptedLiteralString(Lorg/icepdf/core/pobjects/security/SecurityManager;)Ljava/lang/String;

    move-result-object v3

    .line 62
    .local v3, "tmp":Ljava/lang/String;
    iget-object v5, p0, Lorg/icepdf/core/pobjects/graphics/Indexed;->colorSpace:Lorg/icepdf/core/pobjects/graphics/PColorSpace;

    invoke-virtual {v5}, Lorg/icepdf/core/pobjects/graphics/PColorSpace;->getNumComponents()I

    move-result v5

    iget v6, p0, Lorg/icepdf/core/pobjects/graphics/Indexed;->hival:I

    add-int/lit8 v6, v6, 0x1

    mul-int/2addr v5, v6

    new-array v2, v5, [B

    .line 63
    .local v2, "textBytes":[B
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v5, v2

    if-ge v0, v5, :cond_0

    .line 64
    invoke-virtual {v3, v0}, Ljava/lang/String;->charAt(I)C

    move-result v5

    int-to-byte v5, v5

    aput-byte v5, v2, v0

    .line 63
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 66
    :cond_0
    iput-object v2, p0, Lorg/icepdf/core/pobjects/graphics/Indexed;->colors:[B

    .line 71
    .end local v0    # "i":I
    .end local v2    # "textBytes":[B
    .end local v3    # "tmp":Ljava/lang/String;
    .end local v4    # "tmpText":Lorg/icepdf/core/pobjects/StringObject;
    :cond_1
    :goto_1
    return-void

    .line 67
    :cond_2
    invoke-virtual {p3, v6}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v5

    instance-of v5, v5, Lorg/icepdf/core/pobjects/Reference;

    if-eqz v5, :cond_1

    .line 68
    invoke-virtual {p3, v6}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/icepdf/core/pobjects/Reference;

    check-cast v5, Lorg/icepdf/core/pobjects/Reference;

    invoke-virtual {p1, v5}, Lorg/icepdf/core/util/Library;->getObject(Lorg/icepdf/core/pobjects/Reference;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/icepdf/core/pobjects/Stream;

    move-object v1, v5

    check-cast v1, Lorg/icepdf/core/pobjects/Stream;

    .line 69
    .local v1, "lookup":Lorg/icepdf/core/pobjects/Stream;
    invoke-virtual {v1}, Lorg/icepdf/core/pobjects/Stream;->getBytes()[B

    move-result-object v5

    iput-object v5, p0, Lorg/icepdf/core/pobjects/graphics/Indexed;->colors:[B

    goto :goto_1

    .line 32
    nop

    :array_0
    .array-data 1
        -0x1t
        -0x1t
        -0x1t
        0x0t
        0x0t
        0x0t
    .end array-data
.end method


# virtual methods
.method public accessColorTable()[Lorg/apache/poi/java/awt/Color;
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lorg/icepdf/core/pobjects/graphics/Indexed;->cols:[Lorg/apache/poi/java/awt/Color;

    return-object v0
.end method

.method public getColor([F)Lorg/apache/poi/java/awt/Color;
    .locals 4
    .param p1, "f"    # [F

    .prologue
    const/4 v3, 0x0

    .line 118
    invoke-virtual {p0}, Lorg/icepdf/core/pobjects/graphics/Indexed;->init()V

    .line 119
    aget v1, p1, v3

    iget-object v2, p0, Lorg/icepdf/core/pobjects/graphics/Indexed;->cols:[Lorg/apache/poi/java/awt/Color;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    int-to-float v2, v2

    mul-float/2addr v1, v2

    float-to-int v0, v1

    .line 120
    .local v0, "index":I
    iget-object v1, p0, Lorg/icepdf/core/pobjects/graphics/Indexed;->cols:[Lorg/apache/poi/java/awt/Color;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 121
    iget-object v1, p0, Lorg/icepdf/core/pobjects/graphics/Indexed;->cols:[Lorg/apache/poi/java/awt/Color;

    aget-object v1, v1, v0

    .line 123
    :goto_0
    return-object v1

    :cond_0
    iget-object v1, p0, Lorg/icepdf/core/pobjects/graphics/Indexed;->cols:[Lorg/apache/poi/java/awt/Color;

    aget v2, p1, v3

    float-to-int v2, v2

    aget-object v1, v1, v2

    goto :goto_0
.end method

.method public getDescription()Ljava/lang/String;
    .locals 3

    .prologue
    .line 83
    invoke-super {p0}, Lorg/icepdf/core/pobjects/graphics/PColorSpace;->getDescription()Ljava/lang/String;

    move-result-object v0

    .line 84
    .local v0, "desc":Ljava/lang/String;
    iget-object v1, p0, Lorg/icepdf/core/pobjects/graphics/Indexed;->colorSpace:Lorg/icepdf/core/pobjects/graphics/PColorSpace;

    if-eqz v1, :cond_0

    .line 85
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/icepdf/core/pobjects/graphics/Indexed;->colorSpace:Lorg/icepdf/core/pobjects/graphics/PColorSpace;

    invoke-virtual {v2}, Lorg/icepdf/core/pobjects/graphics/PColorSpace;->getDescription()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 86
    :cond_0
    return-object v0
.end method

.method public getNumComponents()I
    .locals 1

    .prologue
    .line 79
    const/4 v0, 0x1

    return v0
.end method

.method public init()V
    .locals 8

    .prologue
    .line 93
    iget-boolean v5, p0, Lorg/icepdf/core/pobjects/graphics/Indexed;->inited:Z

    if-eqz v5, :cond_1

    .line 109
    :cond_0
    return-void

    .line 96
    :cond_1
    const/4 v5, 0x1

    iput-boolean v5, p0, Lorg/icepdf/core/pobjects/graphics/Indexed;->inited:Z

    .line 98
    iget-object v5, p0, Lorg/icepdf/core/pobjects/graphics/Indexed;->colorSpace:Lorg/icepdf/core/pobjects/graphics/PColorSpace;

    invoke-virtual {v5}, Lorg/icepdf/core/pobjects/graphics/PColorSpace;->getNumComponents()I

    move-result v4

    .line 99
    .local v4, "numCSComps":I
    new-array v0, v4, [I

    .line 100
    .local v0, "b1":[I
    new-array v1, v4, [F

    .line 101
    .local v1, "f1":[F
    iget v5, p0, Lorg/icepdf/core/pobjects/graphics/Indexed;->hival:I

    add-int/lit8 v5, v5, 0x1

    new-array v5, v5, [Lorg/apache/poi/java/awt/Color;

    iput-object v5, p0, Lorg/icepdf/core/pobjects/graphics/Indexed;->cols:[Lorg/apache/poi/java/awt/Color;

    .line 102
    const/4 v3, 0x0

    .local v3, "j":I
    :goto_0
    iget v5, p0, Lorg/icepdf/core/pobjects/graphics/Indexed;->hival:I

    if-gt v3, v5, :cond_0

    .line 103
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    if-ge v2, v4, :cond_2

    .line 104
    add-int/lit8 v5, v4, -0x1

    sub-int/2addr v5, v2

    iget-object v6, p0, Lorg/icepdf/core/pobjects/graphics/Indexed;->colors:[B

    mul-int v7, v3, v4

    add-int/2addr v7, v2

    aget-byte v6, v6, v7

    and-int/lit16 v6, v6, 0xff

    aput v6, v0, v5

    .line 103
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 106
    :cond_2
    iget-object v5, p0, Lorg/icepdf/core/pobjects/graphics/Indexed;->colorSpace:Lorg/icepdf/core/pobjects/graphics/PColorSpace;

    const/high16 v6, 0x437f0000    # 255.0f

    invoke-virtual {v5, v0, v1, v6}, Lorg/icepdf/core/pobjects/graphics/PColorSpace;->normaliseComponentsToFloats([I[FF)V

    .line 107
    iget-object v5, p0, Lorg/icepdf/core/pobjects/graphics/Indexed;->cols:[Lorg/apache/poi/java/awt/Color;

    iget-object v6, p0, Lorg/icepdf/core/pobjects/graphics/Indexed;->colorSpace:Lorg/icepdf/core/pobjects/graphics/PColorSpace;

    invoke-virtual {v6, v1}, Lorg/icepdf/core/pobjects/graphics/PColorSpace;->getColor([F)Lorg/apache/poi/java/awt/Color;

    move-result-object v6

    aput-object v6, v5, v3

    .line 102
    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method
