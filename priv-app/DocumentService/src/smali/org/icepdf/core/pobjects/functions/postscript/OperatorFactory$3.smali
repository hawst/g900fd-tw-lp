.class final Lorg/icepdf/core/pobjects/functions/postscript/OperatorFactory$3;
.super Lorg/icepdf/core/pobjects/functions/postscript/Operator;
.source "OperatorFactory.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/icepdf/core/pobjects/functions/postscript/OperatorFactory;->getOperator([CII)Lorg/icepdf/core/pobjects/functions/postscript/Operator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>(I)V
    .locals 0
    .param p1, "x0"    # I

    .prologue
    .line 86
    invoke-direct {p0, p1}, Lorg/icepdf/core/pobjects/functions/postscript/Operator;-><init>(I)V

    return-void
.end method


# virtual methods
.method public eval(Ljava/util/Stack;)V
    .locals 6
    .param p1, "stack"    # Ljava/util/Stack;

    .prologue
    .line 88
    invoke-virtual {p1}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v4

    .line 89
    .local v4, "value":Ljava/lang/Object;
    instance-of v5, v4, Ljava/lang/Boolean;

    if-eqz v5, :cond_1

    .line 90
    check-cast v4, Ljava/lang/Boolean;

    .end local v4    # "value":Ljava/lang/Object;
    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 91
    .local v1, "bool2":Z
    invoke-virtual {p1}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 92
    .local v0, "bool1":Z
    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    const/4 v5, 0x1

    :goto_0
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {p1, v5}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 98
    .end local v0    # "bool1":Z
    .end local v1    # "bool2":Z
    :goto_1
    return-void

    .line 92
    .restart local v0    # "bool1":Z
    .restart local v1    # "bool2":Z
    :cond_0
    const/4 v5, 0x0

    goto :goto_0

    .line 94
    .end local v0    # "bool1":Z
    .end local v1    # "bool2":Z
    .restart local v4    # "value":Ljava/lang/Object;
    :cond_1
    check-cast v4, Ljava/lang/Float;

    .end local v4    # "value":Ljava/lang/Object;
    invoke-virtual {v4}, Ljava/lang/Float;->intValue()I

    move-result v2

    .line 95
    .local v2, "val1":I
    invoke-virtual {p1}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Float;

    invoke-virtual {v5}, Ljava/lang/Float;->intValue()I

    move-result v3

    .line 96
    .local v3, "val2":I
    and-int v5, v2, v3

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {p1, v5}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1
.end method
