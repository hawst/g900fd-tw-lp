.class public Lorg/icepdf/core/pobjects/graphics/DeviceCMYK;
.super Lorg/icepdf/core/pobjects/graphics/PColorSpace;
.source "DeviceCMYK.java"


# static fields
.field public static cmykBlackRatio:D


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 34
    const-string/jumbo v0, "org.icepdf.core.color.cmyk.black"

    const-wide/high16 v2, 0x4008000000000000L    # 3.0

    invoke-static {v0, v2, v3}, Lorg/icepdf/core/util/Defs;->sysPropertyDouble(Ljava/lang/String;D)D

    move-result-wide v0

    sput-wide v0, Lorg/icepdf/core/pobjects/graphics/DeviceCMYK;->cmykBlackRatio:D

    .line 37
    return-void
.end method

.method constructor <init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V
    .locals 0
    .param p1, "l"    # Lorg/icepdf/core/util/Library;
    .param p2, "h"    # Ljava/util/Hashtable;

    .prologue
    .line 40
    invoke-direct {p0, p1, p2}, Lorg/icepdf/core/pobjects/graphics/PColorSpace;-><init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V

    .line 41
    return-void
.end method

.method private static alternative2([F)Lorg/apache/poi/java/awt/Color;
    .locals 40
    .param p0, "f"    # [F

    .prologue
    .line 196
    const/4 v4, 0x3

    aget v27, p0, v4

    .line 197
    .local v27, "inCyan":F
    const/4 v4, 0x2

    aget v28, p0, v4

    .line 198
    .local v28, "inMagenta":F
    const/4 v4, 0x1

    aget v29, p0, v4

    .line 199
    .local v29, "inYellow":F
    const/4 v4, 0x0

    aget v26, p0, v4

    .line 202
    .local v26, "inBlack":F
    const/4 v4, 0x0

    cmpl-float v4, v27, v4

    if-eqz v4, :cond_0

    const/4 v4, 0x0

    cmpl-float v4, v28, v4

    if-eqz v4, :cond_0

    const/4 v4, 0x0

    cmpl-float v4, v29, v4

    if-eqz v4, :cond_0

    .line 203
    move/from16 v0, v26

    float-to-double v4, v0

    sget-wide v6, Lorg/icepdf/core/pobjects/graphics/DeviceCMYK;->cmykBlackRatio:D

    div-double/2addr v4, v6

    double-to-float v0, v4

    move/from16 v26, v0

    .line 207
    :cond_0
    const-wide/16 v4, 0x0

    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    add-float v8, v27, v26

    float-to-double v8, v8

    invoke-static/range {v4 .. v9}, Lorg/icepdf/core/pobjects/graphics/DeviceCMYK;->clip(DDD)D

    move-result-wide v24

    .line 208
    .local v24, "c":D
    const-wide/16 v4, 0x0

    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    add-float v8, v28, v26

    float-to-double v8, v8

    invoke-static/range {v4 .. v9}, Lorg/icepdf/core/pobjects/graphics/DeviceCMYK;->clip(DDD)D

    move-result-wide v30

    .line 209
    .local v30, "m":D
    const-wide/16 v4, 0x0

    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    add-float v8, v29, v26

    float-to-double v8, v8

    invoke-static/range {v4 .. v9}, Lorg/icepdf/core/pobjects/graphics/DeviceCMYK;->clip(DDD)D

    move-result-wide v36

    .line 210
    .local v36, "y":D
    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    sub-double v4, v4, v24

    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    sub-double v6, v6, v30

    mul-double/2addr v4, v6

    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    sub-double v6, v6, v36

    mul-double v20, v4, v6

    .line 211
    .local v20, "aw":D
    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    sub-double v4, v4, v30

    mul-double v4, v4, v24

    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    sub-double v6, v6, v36

    mul-double v12, v4, v6

    .line 212
    .local v12, "ac":D
    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    sub-double v4, v4, v24

    mul-double v4, v4, v30

    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    sub-double v6, v6, v36

    mul-double v16, v4, v6

    .line 213
    .local v16, "am":D
    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    sub-double v4, v4, v24

    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    sub-double v6, v6, v30

    mul-double/2addr v4, v6

    mul-double v22, v4, v36

    .line 214
    .local v22, "ay":D
    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    sub-double v4, v4, v24

    mul-double v4, v4, v30

    mul-double v18, v4, v36

    .line 215
    .local v18, "ar":D
    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    sub-double v4, v4, v30

    mul-double v4, v4, v24

    mul-double v14, v4, v36

    .line 216
    .local v14, "ag":D
    mul-double v4, v24, v30

    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    sub-double v6, v6, v36

    mul-double v10, v4, v6

    .line 218
    .local v10, "ab":D
    const-wide/16 v4, 0x0

    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    const-wide v8, 0x3fed3d07c84b5dccL    # 0.9137

    mul-double v8, v8, v16

    add-double v8, v8, v20

    const-wide v38, 0x3fefe00d1b71758eL    # 0.9961

    mul-double v38, v38, v22

    add-double v8, v8, v38

    const-wide v38, 0x3fef9f559b3d07c8L    # 0.9882

    mul-double v38, v38, v18

    add-double v8, v8, v38

    invoke-static/range {v4 .. v9}, Lorg/icepdf/core/pobjects/graphics/DeviceCMYK;->clip(DDD)D

    move-result-wide v4

    double-to-float v0, v4

    move/from16 v34, v0

    .line 219
    .local v34, "outRed":F
    const-wide/16 v4, 0x0

    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    const-wide v8, 0x3fe3d3c36113404fL    # 0.6196

    mul-double/2addr v8, v12

    add-double v8, v8, v20

    add-double v8, v8, v22

    const-wide v38, 0x3fe0902de00d1b71L    # 0.5176

    mul-double v38, v38, v14

    add-double v8, v8, v38

    invoke-static/range {v4 .. v9}, Lorg/icepdf/core/pobjects/graphics/DeviceCMYK;->clip(DDD)D

    move-result-wide v4

    double-to-float v0, v4

    move/from16 v33, v0

    .line 220
    .local v33, "outGreen":F
    const-wide/16 v4, 0x0

    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    const-wide v8, 0x3fe8f9096bb98c7eL    # 0.7804

    mul-double/2addr v8, v12

    add-double v8, v8, v20

    const-wide v38, 0x3fe15182a9930be1L    # 0.5412

    mul-double v38, v38, v16

    add-double v8, v8, v38

    const-wide v38, 0x3fb113404ea4a8c1L    # 0.0667

    mul-double v38, v38, v18

    add-double v8, v8, v38

    const-wide v38, 0x3fcb1c432ca57a78L    # 0.2118

    mul-double v38, v38, v14

    add-double v8, v8, v38

    const-wide v38, 0x3fdf1f8a0902de01L    # 0.4863

    mul-double v38, v38, v10

    add-double v8, v8, v38

    invoke-static/range {v4 .. v9}, Lorg/icepdf/core/pobjects/graphics/DeviceCMYK;->clip(DDD)D

    move-result-wide v4

    double-to-float v0, v4

    move/from16 v32, v0

    .line 222
    .local v32, "outBlue":F
    new-instance v4, Lorg/apache/poi/java/awt/Color;

    move/from16 v0, v34

    move/from16 v1, v33

    move/from16 v2, v32

    invoke-direct {v4, v0, v1, v2}, Lorg/apache/poi/java/awt/Color;-><init>(FFF)V

    return-object v4
.end method

.method private static clip(DDD)D
    .locals 2
    .param p0, "floor"    # D
    .param p2, "ceiling"    # D
    .param p4, "value"    # D

    .prologue
    .line 233
    cmpg-double v0, p4, p0

    if-gez v0, :cond_0

    .line 234
    move-wide p4, p0

    .line 236
    :cond_0
    cmpl-double v0, p4, p2

    if-lez v0, :cond_1

    .line 237
    move-wide p4, p2

    .line 239
    :cond_1
    return-wide p4
.end method


# virtual methods
.method public getColor([F)Lorg/apache/poi/java/awt/Color;
    .locals 1
    .param p1, "f"    # [F

    .prologue
    .line 56
    invoke-static {p1}, Lorg/icepdf/core/pobjects/graphics/DeviceCMYK;->alternative2([F)Lorg/apache/poi/java/awt/Color;

    move-result-object v0

    return-object v0
.end method

.method public getNumComponents()I
    .locals 1

    .prologue
    .line 44
    const/4 v0, 0x4

    return v0
.end method
