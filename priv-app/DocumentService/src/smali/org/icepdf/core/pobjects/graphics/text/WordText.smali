.class public Lorg/icepdf/core/pobjects/graphics/text/WordText;
.super Lorg/icepdf/core/pobjects/graphics/text/AbstractText;
.source "WordText.java"

# interfaces
.implements Lorg/icepdf/core/pobjects/graphics/text/TextSelect;


# static fields
.field private static final logger:Ljava/util/logging/Logger;

.field public static spaceFraction:I


# instance fields
.field private currentGlyph:Lorg/icepdf/core/pobjects/graphics/text/GlyphText;

.field private glyphs:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lorg/icepdf/core/pobjects/graphics/text/GlyphText;",
            ">;"
        }
    .end annotation
.end field

.field private isWhiteSpace:Z

.field private text:Ljava/lang/StringBuilder;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 42
    const-class v1, Lorg/icepdf/core/pobjects/graphics/text/WordText;

    invoke-virtual {v1}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v1

    sput-object v1, Lorg/icepdf/core/pobjects/graphics/text/WordText;->logger:Ljava/util/logging/Logger;

    .line 52
    :try_start_0
    const-string/jumbo v1, "org.icepdf.core.views.page.text.spaceFraction"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lorg/icepdf/core/util/Defs;->sysPropertyInt(Ljava/lang/String;I)I

    move-result v1

    sput v1, Lorg/icepdf/core/pobjects/graphics/text/WordText;->spaceFraction:I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 59
    .local v0, "e":Ljava/lang/NumberFormatException;
    :cond_0
    :goto_0
    return-void

    .line 54
    .end local v0    # "e":Ljava/lang/NumberFormatException;
    :catch_0
    move-exception v0

    .line 55
    .restart local v0    # "e":Ljava/lang/NumberFormatException;
    sget-object v1, Lorg/icepdf/core/pobjects/graphics/text/WordText;->logger:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    invoke-virtual {v1, v2}, Ljava/util/logging/Logger;->isLoggable(Ljava/util/logging/Level;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 56
    sget-object v1, Lorg/icepdf/core/pobjects/graphics/text/WordText;->logger:Ljava/util/logging/Logger;

    const-string/jumbo v2, "Error reading text selection colour"

    invoke-virtual {v1, v2}, Ljava/util/logging/Logger;->warning(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 69
    invoke-direct {p0}, Lorg/icepdf/core/pobjects/graphics/text/AbstractText;-><init>()V

    .line 70
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lorg/icepdf/core/pobjects/graphics/text/WordText;->text:Ljava/lang/StringBuilder;

    .line 71
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lorg/icepdf/core/pobjects/graphics/text/WordText;->glyphs:Ljava/util/ArrayList;

    .line 72
    return-void
.end method

.method protected static detectPunctuation(Lorg/icepdf/core/pobjects/graphics/text/GlyphText;)Z
    .locals 4
    .param p0, "sprite"    # Lorg/icepdf/core/pobjects/graphics/text/GlyphText;

    .prologue
    const/4 v2, 0x0

    .line 99
    invoke-virtual {p0}, Lorg/icepdf/core/pobjects/graphics/text/GlyphText;->getUnicode()Ljava/lang/String;

    move-result-object v1

    .line 100
    .local v1, "glyphText":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 101
    invoke-virtual {p0}, Lorg/icepdf/core/pobjects/graphics/text/GlyphText;->getUnicode()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 102
    .local v0, "c":I
    invoke-static {v0}, Lorg/icepdf/core/pobjects/graphics/text/WordText;->isPunctuation(I)Z

    move-result v2

    .line 104
    .end local v0    # "c":I
    :cond_0
    return v2
.end method

.method protected static detectWhiteSpace(Lorg/icepdf/core/pobjects/graphics/text/GlyphText;)Z
    .locals 4
    .param p0, "sprite"    # Lorg/icepdf/core/pobjects/graphics/text/GlyphText;

    .prologue
    const/4 v2, 0x0

    .line 109
    invoke-virtual {p0}, Lorg/icepdf/core/pobjects/graphics/text/GlyphText;->getUnicode()Ljava/lang/String;

    move-result-object v1

    .line 110
    .local v1, "glyphText":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 111
    invoke-virtual {p0}, Lorg/icepdf/core/pobjects/graphics/text/GlyphText;->getUnicode()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 112
    .local v0, "c":I
    invoke-static {v0}, Lorg/icepdf/core/pobjects/graphics/text/WordText;->isWhiteSpace(I)Z

    move-result v2

    .line 114
    .end local v0    # "c":I
    :cond_0
    return v2
.end method

.method public static isPunctuation(I)Z
    .locals 1
    .param p0, "c"    # I

    .prologue
    .line 119
    const/16 v0, 0x2e

    if-eq p0, v0, :cond_0

    const/16 v0, 0x2c

    if-eq p0, v0, :cond_0

    const/16 v0, 0x3f

    if-eq p0, v0, :cond_0

    const/16 v0, 0x21

    if-eq p0, v0, :cond_0

    const/16 v0, 0x3a

    if-eq p0, v0, :cond_0

    const/16 v0, 0x3b

    if-eq p0, v0, :cond_0

    const/16 v0, 0x22

    if-eq p0, v0, :cond_0

    const/16 v0, 0x27

    if-eq p0, v0, :cond_0

    const/16 v0, 0x2f

    if-eq p0, v0, :cond_0

    const/16 v0, 0x5c

    if-eq p0, v0, :cond_0

    const/16 v0, 0x60

    if-eq p0, v0, :cond_0

    const/16 v0, 0x23

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isWhiteSpace(I)Z
    .locals 1
    .param p0, "c"    # I

    .prologue
    .line 125
    const/16 v0, 0x20

    if-eq p0, v0, :cond_0

    const/16 v0, 0x9

    if-eq p0, v0, :cond_0

    const/16 v0, 0xd

    if-eq p0, v0, :cond_0

    const/16 v0, 0xa

    if-eq p0, v0, :cond_0

    const/16 v0, 0xc

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected addText(Lorg/icepdf/core/pobjects/graphics/text/GlyphText;)V
    .locals 9
    .param p1, "sprite"    # Lorg/icepdf/core/pobjects/graphics/text/GlyphText;

    .prologue
    .line 175
    iget-object v4, p0, Lorg/icepdf/core/pobjects/graphics/text/WordText;->glyphs:Ljava/util/ArrayList;

    invoke-virtual {v4, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 177
    iput-object p1, p0, Lorg/icepdf/core/pobjects/graphics/text/WordText;->currentGlyph:Lorg/icepdf/core/pobjects/graphics/text/GlyphText;

    .line 180
    iget-object v4, p0, Lorg/icepdf/core/pobjects/graphics/text/WordText;->bounds:Landroid/graphics/RectF;

    if-nez v4, :cond_0

    .line 181
    invoke-virtual {p1}, Lorg/icepdf/core/pobjects/graphics/text/GlyphText;->getBounds()Landroid/graphics/RectF;

    move-result-object v0

    .line 182
    .local v0, "rect":Landroid/graphics/RectF;
    new-instance v4, Landroid/graphics/RectF;

    iget v5, v0, Landroid/graphics/RectF;->left:F

    iget v6, v0, Landroid/graphics/RectF;->top:F

    iget v7, v0, Landroid/graphics/RectF;->right:F

    iget v8, v0, Landroid/graphics/RectF;->bottom:F

    invoke-direct {v4, v5, v6, v7, v8}, Landroid/graphics/RectF;-><init>(FFFF)V

    iput-object v4, p0, Lorg/icepdf/core/pobjects/graphics/text/WordText;->bounds:Landroid/graphics/RectF;

    .line 204
    .end local v0    # "rect":Landroid/graphics/RectF;
    :goto_0
    iget-object v4, p0, Lorg/icepdf/core/pobjects/graphics/text/WordText;->text:Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lorg/icepdf/core/pobjects/graphics/text/GlyphText;->getUnicode()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 205
    return-void

    .line 187
    :cond_0
    invoke-virtual {p1}, Lorg/icepdf/core/pobjects/graphics/text/GlyphText;->getBounds()Landroid/graphics/RectF;

    move-result-object v3

    .line 189
    .local v3, "wordBounds":Landroid/graphics/RectF;
    new-instance v1, Lorg/apache/poi/java/awt/geom/Rectangle2D$Float;

    iget-object v4, p0, Lorg/icepdf/core/pobjects/graphics/text/WordText;->bounds:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->left:F

    iget-object v5, p0, Lorg/icepdf/core/pobjects/graphics/text/WordText;->bounds:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->top:F

    iget-object v6, p0, Lorg/icepdf/core/pobjects/graphics/text/WordText;->bounds:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->right:F

    iget-object v7, p0, Lorg/icepdf/core/pobjects/graphics/text/WordText;->bounds:Landroid/graphics/RectF;

    iget v7, v7, Landroid/graphics/RectF;->left:F

    sub-float/2addr v6, v7

    iget-object v7, p0, Lorg/icepdf/core/pobjects/graphics/text/WordText;->bounds:Landroid/graphics/RectF;

    iget v7, v7, Landroid/graphics/RectF;->bottom:F

    iget-object v8, p0, Lorg/icepdf/core/pobjects/graphics/text/WordText;->bounds:Landroid/graphics/RectF;

    iget v8, v8, Landroid/graphics/RectF;->top:F

    sub-float/2addr v7, v8

    invoke-direct {v1, v4, v5, v6, v7}, Lorg/apache/poi/java/awt/geom/Rectangle2D$Float;-><init>(FFFF)V

    .line 192
    .local v1, "rect1":Lorg/apache/poi/java/awt/geom/Rectangle2D$Float;
    new-instance v2, Lorg/apache/poi/java/awt/geom/Rectangle2D$Float;

    iget v4, v3, Landroid/graphics/RectF;->left:F

    iget v5, v3, Landroid/graphics/RectF;->top:F

    iget v6, v3, Landroid/graphics/RectF;->right:F

    iget v7, v3, Landroid/graphics/RectF;->left:F

    sub-float/2addr v6, v7

    iget v7, v3, Landroid/graphics/RectF;->bottom:F

    iget v8, v3, Landroid/graphics/RectF;->top:F

    sub-float/2addr v7, v8

    invoke-direct {v2, v4, v5, v6, v7}, Lorg/apache/poi/java/awt/geom/Rectangle2D$Float;-><init>(FFFF)V

    .line 195
    .local v2, "rect2":Lorg/apache/poi/java/awt/geom/Rectangle2D$Float;
    invoke-virtual {v1, v2}, Lorg/apache/poi/java/awt/geom/Rectangle2D$Float;->add(Lorg/apache/poi/java/awt/geom/Rectangle2D;)V

    .line 197
    iget-object v4, p0, Lorg/icepdf/core/pobjects/graphics/text/WordText;->bounds:Landroid/graphics/RectF;

    iget v5, v1, Lorg/apache/poi/java/awt/geom/Rectangle2D$Float;->x:F

    iput v5, v4, Landroid/graphics/RectF;->left:F

    .line 198
    iget-object v4, p0, Lorg/icepdf/core/pobjects/graphics/text/WordText;->bounds:Landroid/graphics/RectF;

    iget v5, v1, Lorg/apache/poi/java/awt/geom/Rectangle2D$Float;->y:F

    iput v5, v4, Landroid/graphics/RectF;->top:F

    .line 199
    iget-object v4, p0, Lorg/icepdf/core/pobjects/graphics/text/WordText;->bounds:Landroid/graphics/RectF;

    iget v5, v1, Lorg/apache/poi/java/awt/geom/Rectangle2D$Float;->x:F

    iget v6, v1, Lorg/apache/poi/java/awt/geom/Rectangle2D$Float;->width:F

    add-float/2addr v5, v6

    iput v5, v4, Landroid/graphics/RectF;->right:F

    .line 200
    iget-object v4, p0, Lorg/icepdf/core/pobjects/graphics/text/WordText;->bounds:Landroid/graphics/RectF;

    iget v5, v1, Lorg/apache/poi/java/awt/geom/Rectangle2D$Float;->y:F

    iget v6, v1, Lorg/apache/poi/java/awt/geom/Rectangle2D$Float;->height:F

    add-float/2addr v5, v6

    iput v5, v4, Landroid/graphics/RectF;->bottom:F

    goto :goto_0
.end method

.method protected buildSpaceWord(Lorg/icepdf/core/pobjects/graphics/text/GlyphText;)Lorg/icepdf/core/pobjects/graphics/text/WordText;
    .locals 22
    .param p1, "sprite"    # Lorg/icepdf/core/pobjects/graphics/text/GlyphText;

    .prologue
    .line 132
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/icepdf/core/pobjects/graphics/text/WordText;->currentGlyph:Lorg/icepdf/core/pobjects/graphics/text/GlyphText;

    invoke-virtual {v3}, Lorg/icepdf/core/pobjects/graphics/text/GlyphText;->getBounds()Landroid/graphics/RectF;

    move-result-object v8

    .line 133
    .local v8, "bounds1":Landroid/graphics/RectF;
    invoke-virtual/range {p1 .. p1}, Lorg/icepdf/core/pobjects/graphics/text/GlyphText;->getBounds()Landroid/graphics/RectF;

    move-result-object v9

    .line 134
    .local v9, "bounds2":Landroid/graphics/RectF;
    iget v3, v9, Landroid/graphics/RectF;->left:F

    iget v4, v8, Landroid/graphics/RectF;->right:F

    sub-float v14, v3, v4

    .line 137
    .local v14, "space":F
    iget v3, v8, Landroid/graphics/RectF;->right:F

    iget v4, v8, Landroid/graphics/RectF;->left:F

    sub-float/2addr v3, v4

    iget v4, v9, Landroid/graphics/RectF;->right:F

    iget v5, v9, Landroid/graphics/RectF;->left:F

    sub-float/2addr v4, v5

    invoke-static {v3, v4}, Ljava/lang/Math;->max(FF)F

    move-result v3

    const/high16 v4, 0x40000000    # 2.0f

    div-float v11, v3, v4

    .line 138
    .local v11, "maxWidth":F
    div-float v3, v14, v11

    float-to-int v0, v3

    move/from16 v17, v0

    .line 139
    .local v17, "spaces":I
    const/4 v3, 0x1

    move/from16 v0, v17

    if-ge v0, v3, :cond_0

    const/16 v17, 0x1

    .line 140
    :cond_0
    move/from16 v0, v17

    int-to-float v3, v0

    div-float v16, v14, v3

    .line 142
    .local v16, "spaceWidth":F
    new-instance v18, Lorg/icepdf/core/pobjects/graphics/text/WordText;

    invoke-direct/range {v18 .. v18}, Lorg/icepdf/core/pobjects/graphics/text/WordText;-><init>()V

    .line 143
    .local v18, "whiteSpace":Lorg/icepdf/core/pobjects/graphics/text/WordText;
    iget v3, v8, Landroid/graphics/RectF;->right:F

    float-to-double v12, v3

    .line 146
    .local v12, "offset":D
    new-instance v15, Landroid/graphics/RectF;

    iget v3, v8, Landroid/graphics/RectF;->right:F

    iget v4, v8, Landroid/graphics/RectF;->top:F

    iget v5, v8, Landroid/graphics/RectF;->right:F

    add-float v5, v5, v16

    iget v6, v8, Landroid/graphics/RectF;->bottom:F

    invoke-direct {v15, v3, v4, v5, v6}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 154
    .local v15, "spaceBounds":Landroid/graphics/RectF;
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_0
    move/from16 v0, v17

    if-ge v10, v0, :cond_1

    const/16 v3, 0x32

    if-ge v10, v3, :cond_1

    .line 155
    new-instance v2, Lorg/icepdf/core/pobjects/graphics/text/GlyphText;

    double-to-float v3, v12

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/icepdf/core/pobjects/graphics/text/WordText;->currentGlyph:Lorg/icepdf/core/pobjects/graphics/text/GlyphText;

    invoke-virtual {v4}, Lorg/icepdf/core/pobjects/graphics/text/GlyphText;->getY()F

    move-result v4

    new-instance v5, Landroid/graphics/RectF;

    iget v6, v15, Landroid/graphics/RectF;->left:F

    iget v7, v15, Landroid/graphics/RectF;->top:F

    iget v0, v15, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    iget v0, v15, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-direct {v5, v6, v7, v0, v1}, Landroid/graphics/RectF;-><init>(FFFF)V

    const/16 v6, 0x20

    invoke-static {v6}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v6

    const/16 v7, 0x20

    invoke-static {v7}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v7

    invoke-direct/range {v2 .. v7}, Lorg/icepdf/core/pobjects/graphics/text/GlyphText;-><init>(FFLandroid/graphics/RectF;Ljava/lang/String;Ljava/lang/String;)V

    .line 163
    .local v2, "spaceText":Lorg/icepdf/core/pobjects/graphics/text/GlyphText;
    iget v3, v15, Landroid/graphics/RectF;->right:F

    iget v4, v15, Landroid/graphics/RectF;->left:F

    sub-float v19, v3, v4

    .line 164
    .local v19, "width":F
    iget v3, v15, Landroid/graphics/RectF;->left:F

    add-float v3, v3, v19

    iput v3, v15, Landroid/graphics/RectF;->left:F

    .line 165
    iget v3, v15, Landroid/graphics/RectF;->right:F

    add-float v3, v3, v19

    iput v3, v15, Landroid/graphics/RectF;->right:F

    .line 166
    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Lorg/icepdf/core/pobjects/graphics/text/WordText;->addText(Lorg/icepdf/core/pobjects/graphics/text/GlyphText;)V

    .line 167
    const/4 v3, 0x1

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Lorg/icepdf/core/pobjects/graphics/text/WordText;->setWhiteSpace(Z)V

    .line 168
    move/from16 v0, v16

    float-to-double v4, v0

    add-double/2addr v12, v4

    .line 154
    add-int/lit8 v10, v10, 0x1

    goto :goto_0

    .line 170
    .end local v2    # "spaceText":Lorg/icepdf/core/pobjects/graphics/text/GlyphText;
    .end local v19    # "width":F
    :cond_1
    return-object v18
.end method

.method public clearHighlighted()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 251
    invoke-virtual {p0, v3}, Lorg/icepdf/core/pobjects/graphics/text/WordText;->setHighlighted(Z)V

    .line 252
    invoke-virtual {p0, v3}, Lorg/icepdf/core/pobjects/graphics/text/WordText;->setHasHighlight(Z)V

    .line 253
    iget-object v2, p0, Lorg/icepdf/core/pobjects/graphics/text/WordText;->glyphs:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/icepdf/core/pobjects/graphics/text/GlyphText;

    .line 254
    .local v0, "glyph":Lorg/icepdf/core/pobjects/graphics/text/GlyphText;
    invoke-virtual {v0, v3}, Lorg/icepdf/core/pobjects/graphics/text/GlyphText;->setHighlighted(Z)V

    goto :goto_0

    .line 256
    .end local v0    # "glyph":Lorg/icepdf/core/pobjects/graphics/text/GlyphText;
    :cond_0
    return-void
.end method

.method public clearSelected()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 259
    invoke-virtual {p0, v3}, Lorg/icepdf/core/pobjects/graphics/text/WordText;->setSelected(Z)V

    .line 260
    invoke-virtual {p0, v3}, Lorg/icepdf/core/pobjects/graphics/text/WordText;->setHasSelected(Z)V

    .line 261
    iget-object v2, p0, Lorg/icepdf/core/pobjects/graphics/text/WordText;->glyphs:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/icepdf/core/pobjects/graphics/text/GlyphText;

    .line 262
    .local v0, "glyph":Lorg/icepdf/core/pobjects/graphics/text/GlyphText;
    invoke-virtual {v0, v3}, Lorg/icepdf/core/pobjects/graphics/text/GlyphText;->setSelected(Z)V

    goto :goto_0

    .line 264
    .end local v0    # "glyph":Lorg/icepdf/core/pobjects/graphics/text/GlyphText;
    :cond_0
    return-void
.end method

.method protected detectSpace(Lorg/icepdf/core/pobjects/graphics/text/GlyphText;)Z
    .locals 7
    .param p1, "sprite"    # Lorg/icepdf/core/pobjects/graphics/text/GlyphText;

    .prologue
    const/4 v4, 0x0

    .line 83
    iget-object v5, p0, Lorg/icepdf/core/pobjects/graphics/text/WordText;->currentGlyph:Lorg/icepdf/core/pobjects/graphics/text/GlyphText;

    if-eqz v5, :cond_0

    .line 84
    iget-object v5, p0, Lorg/icepdf/core/pobjects/graphics/text/WordText;->currentGlyph:Lorg/icepdf/core/pobjects/graphics/text/GlyphText;

    invoke-virtual {v5}, Lorg/icepdf/core/pobjects/graphics/text/GlyphText;->getBounds()Landroid/graphics/RectF;

    move-result-object v0

    .line 85
    .local v0, "bounds1":Landroid/graphics/RectF;
    invoke-virtual {p1}, Lorg/icepdf/core/pobjects/graphics/text/GlyphText;->getBounds()Landroid/graphics/RectF;

    move-result-object v1

    .line 86
    .local v1, "bounds2":Landroid/graphics/RectF;
    iget v5, v1, Landroid/graphics/RectF;->left:F

    iget v6, v0, Landroid/graphics/RectF;->right:F

    sub-float v2, v5, v6

    .line 87
    .local v2, "space":F
    const/4 v5, 0x0

    cmpg-float v5, v2, v5

    if-gtz v5, :cond_1

    .line 94
    .end local v0    # "bounds1":Landroid/graphics/RectF;
    .end local v1    # "bounds2":Landroid/graphics/RectF;
    .end local v2    # "space":F
    :cond_0
    :goto_0
    return v4

    .line 91
    .restart local v0    # "bounds1":Landroid/graphics/RectF;
    .restart local v1    # "bounds2":Landroid/graphics/RectF;
    .restart local v2    # "space":F
    :cond_1
    iget v5, v0, Landroid/graphics/RectF;->right:F

    iget v6, v0, Landroid/graphics/RectF;->left:F

    sub-float/2addr v5, v6

    sget v6, Lorg/icepdf/core/pobjects/graphics/text/WordText;->spaceFraction:I

    int-to-float v6, v6

    div-float v3, v5, v6

    .line 92
    .local v3, "tolerance":F
    cmpl-float v5, v2, v3

    if-lez v5, :cond_0

    const/4 v4, 0x1

    goto :goto_0
.end method

.method public getBounds()Landroid/graphics/RectF;
    .locals 10

    .prologue
    .line 208
    iget-object v5, p0, Lorg/icepdf/core/pobjects/graphics/text/WordText;->bounds:Landroid/graphics/RectF;

    if-nez v5, :cond_1

    .line 210
    iget-object v5, p0, Lorg/icepdf/core/pobjects/graphics/text/WordText;->glyphs:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/icepdf/core/pobjects/graphics/text/GlyphText;

    .line 211
    .local v0, "glyph":Lorg/icepdf/core/pobjects/graphics/text/GlyphText;
    iget-object v5, p0, Lorg/icepdf/core/pobjects/graphics/text/WordText;->bounds:Landroid/graphics/RectF;

    if-nez v5, :cond_0

    .line 212
    new-instance v5, Landroid/graphics/RectF;

    invoke-virtual {v0}, Lorg/icepdf/core/pobjects/graphics/text/GlyphText;->getBounds()Landroid/graphics/RectF;

    move-result-object v6

    invoke-direct {v5, v6}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    iput-object v5, p0, Lorg/icepdf/core/pobjects/graphics/text/WordText;->bounds:Landroid/graphics/RectF;

    goto :goto_0

    .line 214
    :cond_0
    invoke-virtual {v0}, Lorg/icepdf/core/pobjects/graphics/text/GlyphText;->getBounds()Landroid/graphics/RectF;

    move-result-object v4

    .line 216
    .local v4, "wordBounds":Landroid/graphics/RectF;
    new-instance v2, Lorg/apache/poi/java/awt/geom/Rectangle2D$Float;

    iget-object v5, p0, Lorg/icepdf/core/pobjects/graphics/text/WordText;->bounds:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->left:F

    iget-object v6, p0, Lorg/icepdf/core/pobjects/graphics/text/WordText;->bounds:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->top:F

    iget-object v7, p0, Lorg/icepdf/core/pobjects/graphics/text/WordText;->bounds:Landroid/graphics/RectF;

    iget v7, v7, Landroid/graphics/RectF;->right:F

    iget-object v8, p0, Lorg/icepdf/core/pobjects/graphics/text/WordText;->bounds:Landroid/graphics/RectF;

    iget v8, v8, Landroid/graphics/RectF;->left:F

    sub-float/2addr v7, v8

    iget-object v8, p0, Lorg/icepdf/core/pobjects/graphics/text/WordText;->bounds:Landroid/graphics/RectF;

    iget v8, v8, Landroid/graphics/RectF;->bottom:F

    iget-object v9, p0, Lorg/icepdf/core/pobjects/graphics/text/WordText;->bounds:Landroid/graphics/RectF;

    iget v9, v9, Landroid/graphics/RectF;->top:F

    sub-float/2addr v8, v9

    invoke-direct {v2, v5, v6, v7, v8}, Lorg/apache/poi/java/awt/geom/Rectangle2D$Float;-><init>(FFFF)V

    .line 219
    .local v2, "rect1":Lorg/apache/poi/java/awt/geom/Rectangle2D$Float;
    new-instance v3, Lorg/apache/poi/java/awt/geom/Rectangle2D$Float;

    iget v5, v4, Landroid/graphics/RectF;->left:F

    iget v6, v4, Landroid/graphics/RectF;->top:F

    iget v7, v4, Landroid/graphics/RectF;->right:F

    iget v8, v4, Landroid/graphics/RectF;->left:F

    sub-float/2addr v7, v8

    iget v8, v4, Landroid/graphics/RectF;->bottom:F

    iget v9, v4, Landroid/graphics/RectF;->top:F

    sub-float/2addr v8, v9

    invoke-direct {v3, v5, v6, v7, v8}, Lorg/apache/poi/java/awt/geom/Rectangle2D$Float;-><init>(FFFF)V

    .line 222
    .local v3, "rect2":Lorg/apache/poi/java/awt/geom/Rectangle2D$Float;
    invoke-virtual {v2, v3}, Lorg/apache/poi/java/awt/geom/Rectangle2D$Float;->add(Lorg/apache/poi/java/awt/geom/Rectangle2D;)V

    .line 224
    iget-object v5, p0, Lorg/icepdf/core/pobjects/graphics/text/WordText;->bounds:Landroid/graphics/RectF;

    iget v6, v2, Lorg/apache/poi/java/awt/geom/Rectangle2D$Float;->x:F

    iput v6, v5, Landroid/graphics/RectF;->left:F

    .line 225
    iget-object v5, p0, Lorg/icepdf/core/pobjects/graphics/text/WordText;->bounds:Landroid/graphics/RectF;

    iget v6, v2, Lorg/apache/poi/java/awt/geom/Rectangle2D$Float;->y:F

    iput v6, v5, Landroid/graphics/RectF;->top:F

    .line 226
    iget-object v5, p0, Lorg/icepdf/core/pobjects/graphics/text/WordText;->bounds:Landroid/graphics/RectF;

    iget v6, v2, Lorg/apache/poi/java/awt/geom/Rectangle2D$Float;->x:F

    iget v7, v2, Lorg/apache/poi/java/awt/geom/Rectangle2D$Float;->width:F

    add-float/2addr v6, v7

    iput v6, v5, Landroid/graphics/RectF;->right:F

    .line 227
    iget-object v5, p0, Lorg/icepdf/core/pobjects/graphics/text/WordText;->bounds:Landroid/graphics/RectF;

    iget v6, v2, Lorg/apache/poi/java/awt/geom/Rectangle2D$Float;->y:F

    iget v7, v2, Lorg/apache/poi/java/awt/geom/Rectangle2D$Float;->height:F

    add-float/2addr v6, v7

    iput v6, v5, Landroid/graphics/RectF;->bottom:F

    goto :goto_0

    .line 233
    .end local v0    # "glyph":Lorg/icepdf/core/pobjects/graphics/text/GlyphText;
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "rect1":Lorg/apache/poi/java/awt/geom/Rectangle2D$Float;
    .end local v3    # "rect2":Lorg/apache/poi/java/awt/geom/Rectangle2D$Float;
    .end local v4    # "wordBounds":Landroid/graphics/RectF;
    :cond_1
    iget-object v5, p0, Lorg/icepdf/core/pobjects/graphics/text/WordText;->bounds:Landroid/graphics/RectF;

    return-object v5
.end method

.method public getGlyphs()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lorg/icepdf/core/pobjects/graphics/text/GlyphText;",
            ">;"
        }
    .end annotation

    .prologue
    .line 237
    iget-object v0, p0, Lorg/icepdf/core/pobjects/graphics/text/WordText;->glyphs:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getSelected()Ljava/lang/StringBuilder;
    .locals 4

    .prologue
    .line 241
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 242
    .local v2, "selectedText":Ljava/lang/StringBuilder;
    iget-object v3, p0, Lorg/icepdf/core/pobjects/graphics/text/WordText;->glyphs:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/icepdf/core/pobjects/graphics/text/GlyphText;

    .line 243
    .local v0, "glyph":Lorg/icepdf/core/pobjects/graphics/text/GlyphText;
    invoke-virtual {v0}, Lorg/icepdf/core/pobjects/graphics/text/GlyphText;->isSelected()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 244
    invoke-virtual {v0}, Lorg/icepdf/core/pobjects/graphics/text/GlyphText;->getUnicode()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 247
    .end local v0    # "glyph":Lorg/icepdf/core/pobjects/graphics/text/GlyphText;
    :cond_1
    return-object v2
.end method

.method public getText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 281
    iget-object v0, p0, Lorg/icepdf/core/pobjects/graphics/text/WordText;->text:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public isWhiteSpace()Z
    .locals 1

    .prologue
    .line 75
    iget-boolean v0, p0, Lorg/icepdf/core/pobjects/graphics/text/WordText;->isWhiteSpace:Z

    return v0
.end method

.method public selectAll()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 267
    invoke-virtual {p0, v3}, Lorg/icepdf/core/pobjects/graphics/text/WordText;->setSelected(Z)V

    .line 268
    invoke-virtual {p0, v3}, Lorg/icepdf/core/pobjects/graphics/text/WordText;->setHasSelected(Z)V

    .line 269
    iget-object v2, p0, Lorg/icepdf/core/pobjects/graphics/text/WordText;->glyphs:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/icepdf/core/pobjects/graphics/text/GlyphText;

    .line 270
    .local v0, "glyph":Lorg/icepdf/core/pobjects/graphics/text/GlyphText;
    invoke-virtual {v0, v3}, Lorg/icepdf/core/pobjects/graphics/text/GlyphText;->setSelected(Z)V

    goto :goto_0

    .line 272
    .end local v0    # "glyph":Lorg/icepdf/core/pobjects/graphics/text/GlyphText;
    :cond_0
    return-void
.end method

.method public setWhiteSpace(Z)V
    .locals 0
    .param p1, "whiteSpace"    # Z

    .prologue
    .line 79
    iput-boolean p1, p0, Lorg/icepdf/core/pobjects/graphics/text/WordText;->isWhiteSpace:Z

    .line 80
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 286
    invoke-virtual {p0}, Lorg/icepdf/core/pobjects/graphics/text/WordText;->getText()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
