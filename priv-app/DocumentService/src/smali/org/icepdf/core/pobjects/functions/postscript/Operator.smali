.class public abstract Lorg/icepdf/core/pobjects/functions/postscript/Operator;
.super Ljava/lang/Object;
.source "Operator.java"


# instance fields
.field protected type:I


# direct methods
.method protected constructor <init>(I)V
    .locals 0
    .param p1, "type"    # I

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput p1, p0, Lorg/icepdf/core/pobjects/functions/postscript/Operator;->type:I

    .line 32
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 2
    .param p1, "op"    # Ljava/lang/Object;

    .prologue
    .line 37
    instance-of v0, p1, Lorg/icepdf/core/pobjects/functions/postscript/Operator;

    if-eqz v0, :cond_0

    check-cast p1, Lorg/icepdf/core/pobjects/functions/postscript/Operator;

    .end local p1    # "op":Ljava/lang/Object;
    iget v0, p1, Lorg/icepdf/core/pobjects/functions/postscript/Operator;->type:I

    iget v1, p0, Lorg/icepdf/core/pobjects/functions/postscript/Operator;->type:I

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public abstract eval(Ljava/util/Stack;)V
.end method

.method public getType()I
    .locals 1

    .prologue
    .line 41
    iget v0, p0, Lorg/icepdf/core/pobjects/functions/postscript/Operator;->type:I

    return v0
.end method
