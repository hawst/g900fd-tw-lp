.class public Lorg/icepdf/core/pobjects/actions/URIAction;
.super Lorg/icepdf/core/pobjects/actions/Action;
.source "URIAction.java"


# static fields
.field public static final URI_KEY:Lorg/icepdf/core/pobjects/Name;


# instance fields
.field private URI:Lorg/icepdf/core/pobjects/StringObject;

.field private isMap:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 37
    new-instance v0, Lorg/icepdf/core/pobjects/Name;

    const-string/jumbo v1, "URI"

    invoke-direct {v0, v1}, Lorg/icepdf/core/pobjects/Name;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/icepdf/core/pobjects/actions/URIAction;->URI_KEY:Lorg/icepdf/core/pobjects/Name;

    return-void
.end method

.method public constructor <init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V
    .locals 0
    .param p1, "l"    # Lorg/icepdf/core/util/Library;
    .param p2, "h"    # Ljava/util/Hashtable;

    .prologue
    .line 52
    invoke-direct {p0, p1, p2}, Lorg/icepdf/core/pobjects/actions/Action;-><init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V

    .line 53
    return-void
.end method


# virtual methods
.method public getURI()Ljava/lang/String;
    .locals 3

    .prologue
    .line 76
    sget-object v1, Lorg/icepdf/core/pobjects/actions/URIAction;->URI_KEY:Lorg/icepdf/core/pobjects/Name;

    invoke-virtual {p0, v1}, Lorg/icepdf/core/pobjects/actions/URIAction;->getObject(Lorg/icepdf/core/pobjects/Name;)Ljava/lang/Object;

    move-result-object v0

    .line 77
    .local v0, "actionURI":Ljava/lang/Object;
    instance-of v1, v0, Lorg/icepdf/core/pobjects/StringObject;

    if-eqz v1, :cond_0

    .line 78
    check-cast v0, Lorg/icepdf/core/pobjects/StringObject;

    .end local v0    # "actionURI":Ljava/lang/Object;
    iput-object v0, p0, Lorg/icepdf/core/pobjects/actions/URIAction;->URI:Lorg/icepdf/core/pobjects/StringObject;

    .line 80
    :cond_0
    iget-object v1, p0, Lorg/icepdf/core/pobjects/actions/URIAction;->URI:Lorg/icepdf/core/pobjects/StringObject;

    iget-object v2, p0, Lorg/icepdf/core/pobjects/actions/URIAction;->library:Lorg/icepdf/core/util/Library;

    iget-object v2, v2, Lorg/icepdf/core/util/Library;->securityManager:Lorg/icepdf/core/pobjects/security/SecurityManager;

    invoke-interface {v1, v2}, Lorg/icepdf/core/pobjects/StringObject;->getDecryptedLiteralString(Lorg/icepdf/core/pobjects/security/SecurityManager;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public isMap()Z
    .locals 1

    .prologue
    .line 90
    iget-boolean v0, p0, Lorg/icepdf/core/pobjects/actions/URIAction;->isMap:Z

    return v0
.end method

.method public setURI(Ljava/lang/String;)V
    .locals 3
    .param p1, "URI"    # Ljava/lang/String;

    .prologue
    .line 61
    new-instance v0, Lorg/icepdf/core/pobjects/LiteralStringObject;

    invoke-virtual {p0}, Lorg/icepdf/core/pobjects/actions/URIAction;->getPObjectReference()Lorg/icepdf/core/pobjects/Reference;

    move-result-object v1

    iget-object v2, p0, Lorg/icepdf/core/pobjects/actions/URIAction;->library:Lorg/icepdf/core/util/Library;

    iget-object v2, v2, Lorg/icepdf/core/util/Library;->securityManager:Lorg/icepdf/core/pobjects/security/SecurityManager;

    invoke-direct {v0, p1, v1, v2}, Lorg/icepdf/core/pobjects/LiteralStringObject;-><init>(Ljava/lang/String;Lorg/icepdf/core/pobjects/Reference;Lorg/icepdf/core/pobjects/security/SecurityManager;)V

    .line 64
    .local v0, "tmp":Lorg/icepdf/core/pobjects/StringObject;
    iget-object v1, p0, Lorg/icepdf/core/pobjects/actions/URIAction;->entries:Ljava/util/Hashtable;

    sget-object v2, Lorg/icepdf/core/pobjects/actions/URIAction;->URI_KEY:Lorg/icepdf/core/pobjects/Name;

    invoke-virtual {v1, v2, v0}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 65
    iput-object v0, p0, Lorg/icepdf/core/pobjects/actions/URIAction;->URI:Lorg/icepdf/core/pobjects/StringObject;

    .line 66
    return-void
.end method
