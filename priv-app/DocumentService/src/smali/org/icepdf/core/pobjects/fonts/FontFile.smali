.class public interface abstract Lorg/icepdf/core/pobjects/fonts/FontFile;
.super Ljava/lang/Object;
.source "FontFile.java"


# static fields
.field public static final LAYOUT_NONE:J


# virtual methods
.method public abstract canDisplayEchar(C)Z
.end method

.method public abstract deriveFont(F)Lorg/icepdf/core/pobjects/fonts/FontFile;
.end method

.method public abstract deriveFont(Landroid/graphics/Matrix;)Lorg/icepdf/core/pobjects/fonts/FontFile;
.end method

.method public abstract deriveFont(Ljava/util/Map;IFFF[C)Lorg/icepdf/core/pobjects/fonts/FontFile;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Float;",
            ">;IFFF[C)",
            "Lorg/icepdf/core/pobjects/fonts/FontFile;"
        }
    .end annotation
.end method

.method public abstract deriveFont(Lorg/icepdf/core/pobjects/fonts/Encoding;Lorg/icepdf/core/pobjects/fonts/CMap;)Lorg/icepdf/core/pobjects/fonts/FontFile;
.end method

.method public abstract deriveFont([FIFFF[C)Lorg/icepdf/core/pobjects/fonts/FontFile;
.end method

.method public abstract drawEstring(Landroid/graphics/Canvas;Ljava/lang/String;FFJILorg/apache/poi/java/awt/Color;Landroid/graphics/Paint;)V
.end method

.method public abstract echarAdvance(C)Landroid/graphics/PointF;
.end method

.method public abstract getAscent()D
.end method

.method public abstract getDescent()D
.end method

.method public abstract getEstringBounds(Ljava/lang/String;II)Landroid/graphics/RectF;
.end method

.method public abstract getEstringOutline(Ljava/lang/String;FF)Landroid/graphics/Path;
.end method

.method public abstract getFamily()Ljava/lang/String;
.end method

.method public abstract getFormat()Ljava/lang/String;
.end method

.method public abstract getMaxCharBounds()Landroid/graphics/RectF;
.end method

.method public abstract getName()Ljava/lang/String;
.end method

.method public abstract getNumGlyphs()I
.end method

.method public abstract getRights()I
.end method

.method public abstract getSize()F
.end method

.method public abstract getSpaceEchar()C
.end method

.method public abstract getStyle()I
.end method

.method public abstract getToUnicode()Lorg/icepdf/core/pobjects/fonts/CMap;
.end method

.method public abstract getTransform()Landroid/graphics/Matrix;
.end method

.method public abstract isHinted()Z
.end method

.method public abstract isOneByteEncoding()Z
.end method

.method public abstract toUnicode(C)Ljava/lang/String;
.end method

.method public abstract toUnicode(Ljava/lang/String;)Ljava/lang/String;
.end method
