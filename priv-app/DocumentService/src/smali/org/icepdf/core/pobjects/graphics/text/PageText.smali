.class public Lorg/icepdf/core/pobjects/graphics/text/PageText;
.super Ljava/lang/Object;
.source "PageText.java"

# interfaces
.implements Lorg/icepdf/core/pobjects/graphics/text/TextSelect;


# instance fields
.field private currentLine:Lorg/icepdf/core/pobjects/graphics/text/LineText;

.field private pageLines:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lorg/icepdf/core/pobjects/graphics/text/LineText;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0x32

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lorg/icepdf/core/pobjects/graphics/text/PageText;->pageLines:Ljava/util/ArrayList;

    .line 50
    return-void
.end method


# virtual methods
.method public addGlyph(Lorg/icepdf/core/pobjects/graphics/text/GlyphText;)V
    .locals 1
    .param p1, "sprite"    # Lorg/icepdf/core/pobjects/graphics/text/GlyphText;

    .prologue
    .line 63
    iget-object v0, p0, Lorg/icepdf/core/pobjects/graphics/text/PageText;->currentLine:Lorg/icepdf/core/pobjects/graphics/text/LineText;

    if-nez v0, :cond_0

    .line 64
    invoke-virtual {p0}, Lorg/icepdf/core/pobjects/graphics/text/PageText;->newLine()V

    .line 66
    :cond_0
    iget-object v0, p0, Lorg/icepdf/core/pobjects/graphics/text/PageText;->currentLine:Lorg/icepdf/core/pobjects/graphics/text/LineText;

    invoke-virtual {v0, p1}, Lorg/icepdf/core/pobjects/graphics/text/LineText;->addText(Lorg/icepdf/core/pobjects/graphics/text/GlyphText;)V

    .line 67
    return-void
.end method

.method public applyXObjectTransform(Landroid/graphics/Matrix;)V
    .locals 7
    .param p1, "transform"    # Landroid/graphics/Matrix;

    .prologue
    .line 80
    iget-object v6, p0, Lorg/icepdf/core/pobjects/graphics/text/PageText;->pageLines:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/icepdf/core/pobjects/graphics/text/LineText;

    .line 81
    .local v4, "lineText":Lorg/icepdf/core/pobjects/graphics/text/LineText;
    invoke-virtual {v4}, Lorg/icepdf/core/pobjects/graphics/text/LineText;->clearBounds()V

    .line 82
    invoke-virtual {v4}, Lorg/icepdf/core/pobjects/graphics/text/LineText;->getWords()Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/icepdf/core/pobjects/graphics/text/WordText;

    .line 83
    .local v5, "wordText":Lorg/icepdf/core/pobjects/graphics/text/WordText;
    invoke-virtual {v5}, Lorg/icepdf/core/pobjects/graphics/text/WordText;->clearBounds()V

    .line 84
    invoke-virtual {v5}, Lorg/icepdf/core/pobjects/graphics/text/WordText;->getGlyphs()Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/icepdf/core/pobjects/graphics/text/GlyphText;

    .line 85
    .local v0, "glyph":Lorg/icepdf/core/pobjects/graphics/text/GlyphText;
    invoke-virtual {v0, p1}, Lorg/icepdf/core/pobjects/graphics/text/GlyphText;->normalizeToUserSpace(Landroid/graphics/Matrix;)V

    goto :goto_0

    .line 89
    .end local v0    # "glyph":Lorg/icepdf/core/pobjects/graphics/text/GlyphText;
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v4    # "lineText":Lorg/icepdf/core/pobjects/graphics/text/LineText;
    .end local v5    # "wordText":Lorg/icepdf/core/pobjects/graphics/text/WordText;
    :cond_2
    return-void
.end method

.method public clearHighlighted()V
    .locals 3

    .prologue
    .line 98
    iget-object v2, p0, Lorg/icepdf/core/pobjects/graphics/text/PageText;->pageLines:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/icepdf/core/pobjects/graphics/text/LineText;

    .line 99
    .local v1, "lineText":Lorg/icepdf/core/pobjects/graphics/text/LineText;
    invoke-virtual {v1}, Lorg/icepdf/core/pobjects/graphics/text/LineText;->clearHighlighted()V

    goto :goto_0

    .line 101
    .end local v1    # "lineText":Lorg/icepdf/core/pobjects/graphics/text/LineText;
    :cond_0
    return-void
.end method

.method public clearSelected()V
    .locals 3

    .prologue
    .line 92
    iget-object v2, p0, Lorg/icepdf/core/pobjects/graphics/text/PageText;->pageLines:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/icepdf/core/pobjects/graphics/text/LineText;

    .line 93
    .local v1, "lineText":Lorg/icepdf/core/pobjects/graphics/text/LineText;
    invoke-virtual {v1}, Lorg/icepdf/core/pobjects/graphics/text/LineText;->clearSelected()V

    goto :goto_0

    .line 95
    .end local v1    # "lineText":Lorg/icepdf/core/pobjects/graphics/text/LineText;
    :cond_0
    return-void
.end method

.method public deselectAll()V
    .locals 3

    .prologue
    .line 118
    iget-object v2, p0, Lorg/icepdf/core/pobjects/graphics/text/PageText;->pageLines:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/icepdf/core/pobjects/graphics/text/LineText;

    .line 119
    .local v1, "lineText":Lorg/icepdf/core/pobjects/graphics/text/LineText;
    invoke-virtual {v1}, Lorg/icepdf/core/pobjects/graphics/text/LineText;->clearSelected()V

    goto :goto_0

    .line 121
    .end local v1    # "lineText":Lorg/icepdf/core/pobjects/graphics/text/LineText;
    :cond_0
    return-void
.end method

.method public dispose()V
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Lorg/icepdf/core/pobjects/graphics/text/PageText;->pageLines:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 125
    iget-object v0, p0, Lorg/icepdf/core/pobjects/graphics/text/PageText;->pageLines:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 126
    iget-object v0, p0, Lorg/icepdf/core/pobjects/graphics/text/PageText;->pageLines:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->trimToSize()V

    .line 128
    :cond_0
    return-void
.end method

.method public getPageLines()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lorg/icepdf/core/pobjects/graphics/text/LineText;",
            ">;"
        }
    .end annotation

    .prologue
    .line 70
    iget-object v0, p0, Lorg/icepdf/core/pobjects/graphics/text/PageText;->pageLines:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getSelected()Ljava/lang/StringBuilder;
    .locals 4

    .prologue
    .line 104
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 105
    .local v2, "selectedText":Ljava/lang/StringBuilder;
    iget-object v3, p0, Lorg/icepdf/core/pobjects/graphics/text/PageText;->pageLines:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/icepdf/core/pobjects/graphics/text/LineText;

    .line 106
    .local v1, "lineText":Lorg/icepdf/core/pobjects/graphics/text/LineText;
    invoke-virtual {v1}, Lorg/icepdf/core/pobjects/graphics/text/LineText;->getSelected()Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 108
    .end local v1    # "lineText":Lorg/icepdf/core/pobjects/graphics/text/LineText;
    :cond_0
    return-object v2
.end method

.method public newLine()V
    .locals 2

    .prologue
    .line 54
    iget-object v0, p0, Lorg/icepdf/core/pobjects/graphics/text/PageText;->currentLine:Lorg/icepdf/core/pobjects/graphics/text/LineText;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/icepdf/core/pobjects/graphics/text/PageText;->currentLine:Lorg/icepdf/core/pobjects/graphics/text/LineText;

    invoke-virtual {v0}, Lorg/icepdf/core/pobjects/graphics/text/LineText;->getWords()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 60
    :goto_0
    return-void

    .line 58
    :cond_0
    new-instance v0, Lorg/icepdf/core/pobjects/graphics/text/LineText;

    invoke-direct {v0}, Lorg/icepdf/core/pobjects/graphics/text/LineText;-><init>()V

    iput-object v0, p0, Lorg/icepdf/core/pobjects/graphics/text/PageText;->currentLine:Lorg/icepdf/core/pobjects/graphics/text/LineText;

    .line 59
    iget-object v0, p0, Lorg/icepdf/core/pobjects/graphics/text/PageText;->pageLines:Ljava/util/ArrayList;

    iget-object v1, p0, Lorg/icepdf/core/pobjects/graphics/text/PageText;->currentLine:Lorg/icepdf/core/pobjects/graphics/text/LineText;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public selectAll()V
    .locals 3

    .prologue
    .line 112
    iget-object v2, p0, Lorg/icepdf/core/pobjects/graphics/text/PageText;->pageLines:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/icepdf/core/pobjects/graphics/text/LineText;

    .line 113
    .local v1, "lineText":Lorg/icepdf/core/pobjects/graphics/text/LineText;
    invoke-virtual {v1}, Lorg/icepdf/core/pobjects/graphics/text/LineText;->selectAll()V

    goto :goto_0

    .line 115
    .end local v1    # "lineText":Lorg/icepdf/core/pobjects/graphics/text/LineText;
    :cond_0
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 131
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 132
    .local v0, "extractedText":Ljava/lang/StringBuilder;
    iget-object v5, p0, Lorg/icepdf/core/pobjects/graphics/text/PageText;->pageLines:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/icepdf/core/pobjects/graphics/text/LineText;

    .line 134
    .local v3, "lineText":Lorg/icepdf/core/pobjects/graphics/text/LineText;
    invoke-virtual {v3}, Lorg/icepdf/core/pobjects/graphics/text/LineText;->getWords()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/icepdf/core/pobjects/graphics/text/WordText;

    .line 135
    .local v4, "wordText":Lorg/icepdf/core/pobjects/graphics/text/WordText;
    invoke-virtual {v4}, Lorg/icepdf/core/pobjects/graphics/text/WordText;->getText()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 137
    .end local v4    # "wordText":Lorg/icepdf/core/pobjects/graphics/text/WordText;
    :cond_0
    const/16 v5, 0xa

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 139
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v3    # "lineText":Lorg/icepdf/core/pobjects/graphics/text/LineText;
    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    return-object v5
.end method
