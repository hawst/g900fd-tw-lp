.class public Lorg/icepdf/core/pobjects/graphics/text/GlyphText;
.super Lorg/icepdf/core/pobjects/graphics/text/AbstractText;
.source "GlyphText.java"


# static fields
.field private static final logger:Ljava/util/logging/Logger;


# instance fields
.field private cid:Ljava/lang/String;

.field private unicode:Ljava/lang/String;

.field private x:F

.field private y:F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    const-class v0, Lorg/icepdf/core/pobjects/graphics/text/GlyphText;

    invoke-virtual {v0}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Lorg/icepdf/core/pobjects/graphics/text/GlyphText;->logger:Ljava/util/logging/Logger;

    return-void
.end method

.method public constructor <init>(FFLandroid/graphics/RectF;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "x"    # F
    .param p2, "y"    # F
    .param p3, "bounds"    # Landroid/graphics/RectF;
    .param p4, "cid"    # Ljava/lang/String;
    .param p5, "unicode"    # Ljava/lang/String;

    .prologue
    .line 46
    invoke-direct {p0}, Lorg/icepdf/core/pobjects/graphics/text/AbstractText;-><init>()V

    .line 47
    iput p1, p0, Lorg/icepdf/core/pobjects/graphics/text/GlyphText;->x:F

    .line 48
    iput p2, p0, Lorg/icepdf/core/pobjects/graphics/text/GlyphText;->y:F

    .line 49
    iput-object p3, p0, Lorg/icepdf/core/pobjects/graphics/text/GlyphText;->bounds:Landroid/graphics/RectF;

    .line 50
    iput-object p4, p0, Lorg/icepdf/core/pobjects/graphics/text/GlyphText;->cid:Ljava/lang/String;

    .line 51
    iput-object p5, p0, Lorg/icepdf/core/pobjects/graphics/text/GlyphText;->unicode:Ljava/lang/String;

    .line 52
    return-void
.end method


# virtual methods
.method public getBounds()Landroid/graphics/RectF;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lorg/icepdf/core/pobjects/graphics/text/GlyphText;->bounds:Landroid/graphics/RectF;

    return-object v0
.end method

.method public getCid()Ljava/lang/String;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lorg/icepdf/core/pobjects/graphics/text/GlyphText;->cid:Ljava/lang/String;

    return-object v0
.end method

.method public getUnicode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lorg/icepdf/core/pobjects/graphics/text/GlyphText;->unicode:Ljava/lang/String;

    return-object v0
.end method

.method public getX()F
    .locals 1

    .prologue
    .line 87
    iget v0, p0, Lorg/icepdf/core/pobjects/graphics/text/GlyphText;->x:F

    return v0
.end method

.method public getY()F
    .locals 1

    .prologue
    .line 91
    iget v0, p0, Lorg/icepdf/core/pobjects/graphics/text/GlyphText;->y:F

    return v0
.end method

.method public normalizeToUserSpace(Landroid/graphics/Matrix;)V
    .locals 4
    .param p1, "af"    # Landroid/graphics/Matrix;

    .prologue
    .line 61
    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    .line 63
    .local v1, "newBounds":Landroid/graphics/RectF;
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    .line 65
    .local v0, "generalPath":Landroid/graphics/Path;
    iget-object v2, p0, Lorg/icepdf/core/pobjects/graphics/text/GlyphText;->bounds:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->left:F

    iget-object v3, p0, Lorg/icepdf/core/pobjects/graphics/text/GlyphText;->bounds:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->top:F

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->moveTo(FF)V

    .line 66
    iget-object v2, p0, Lorg/icepdf/core/pobjects/graphics/text/GlyphText;->bounds:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->right:F

    iget-object v3, p0, Lorg/icepdf/core/pobjects/graphics/text/GlyphText;->bounds:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->top:F

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 67
    iget-object v2, p0, Lorg/icepdf/core/pobjects/graphics/text/GlyphText;->bounds:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->right:F

    iget-object v3, p0, Lorg/icepdf/core/pobjects/graphics/text/GlyphText;->bounds:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 68
    iget-object v2, p0, Lorg/icepdf/core/pobjects/graphics/text/GlyphText;->bounds:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->left:F

    iget-object v3, p0, Lorg/icepdf/core/pobjects/graphics/text/GlyphText;->bounds:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 69
    invoke-virtual {v0}, Landroid/graphics/Path;->close()V

    .line 71
    invoke-virtual {v0, p1}, Landroid/graphics/Path;->transform(Landroid/graphics/Matrix;)V

    .line 73
    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->computeBounds(Landroid/graphics/RectF;Z)V

    .line 75
    iput-object v1, p0, Lorg/icepdf/core/pobjects/graphics/text/GlyphText;->bounds:Landroid/graphics/RectF;

    .line 76
    return-void
.end method
