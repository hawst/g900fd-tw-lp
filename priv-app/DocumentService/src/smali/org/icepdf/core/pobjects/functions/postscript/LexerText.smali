.class public Lorg/icepdf/core/pobjects/functions/postscript/LexerText;
.super Ljava/lang/Object;
.source "LexerText.java"


# static fields
.field public static final TEST_1:Ljava/lang/String; = "{1.000000 3 1 roll 1.000000 3 1 roll 1.000000 3 1 roll 5 -1 roll \n2 index -0.874500 mul 1.000000 add mul 1 index -0.098000 mul 1.000000 add mul 5 \n1 roll 4 -1 roll 2 index -0.796100 mul 1.000000 add mul 1 index -0.247100 \nmul 1.000000 add mul 4 1 roll 3 -1 roll 2 index -0.647100 mul 1.000000 \nadd mul 1 index -0.878400 mul 1.000000 add mul 3 1 roll pop pop }"

.field public static final TEST_2:Ljava/lang/String; = "{1.000000 2 1 roll 1.000000 2 1 roll 1.000000 2 1 roll 0 index 1.000000 \ncvr exch sub 2 1 roll 5 -1 roll 1.000000 cvr exch sub 5 1 \nroll 4 -1 roll 1.000000 cvr exch sub 4 1 roll 3 -1 roll 1.000000 \ncvr exch sub 3 1 roll 2 -1 roll 1.000000 cvr exch sub 2 1 \nroll pop }"

.field public static final TEST_3:Ljava/lang/String; = "{0 0 0 0 5 4 roll 0 index 3 -1 roll add 2 1 roll pop dup 1 gt {pop 1} if 4 1 roll dup 1 gt {pop 1} if 4 1 roll dup 1 gt {pop 1} if 4 1 roll dup 1 gt {pop 1} if 4 1 roll}"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static main([Ljava/lang/String;)V
    .locals 4
    .param p0, "args"    # [Ljava/lang/String;

    .prologue
    .line 55
    :try_start_0
    new-instance v1, Lorg/icepdf/core/pobjects/functions/postscript/LexerText;

    invoke-direct {v1}, Lorg/icepdf/core/pobjects/functions/postscript/LexerText;-><init>()V

    invoke-virtual {v1}, Lorg/icepdf/core/pobjects/functions/postscript/LexerText;->test8()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 60
    :goto_0
    return-void

    .line 57
    :catch_0
    move-exception v0

    .line 58
    .local v0, "e":Ljava/io/IOException;
    const-string/jumbo v1, "DocumentService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Exception: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method public test1()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 63
    const-string/jumbo v2, "{1.000000 3 1 roll 1.000000 3 1 roll 1.000000 3 1 roll 5 -1 roll \n2 index -0.874500 mul 1.000000 add mul 1 index -0.098000 mul 1.000000 add mul 5 \n1 roll 4 -1 roll 2 index -0.796100 mul 1.000000 add mul 1 index -0.247100 \nmul 1.000000 add mul 4 1 roll 3 -1 roll 2 index -0.647100 mul 1.000000 \nadd mul 1 index -0.878400 mul 1.000000 add mul 3 1 roll pop pop }"

    .line 71
    .local v2, "test":Ljava/lang/String;
    new-instance v0, Ljava/io/ByteArrayInputStream;

    invoke-virtual {v2}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    invoke-direct {v0, v3}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 72
    .local v0, "function_4":Ljava/io/InputStream;
    new-instance v1, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;

    invoke-direct {v1}, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;-><init>()V

    .line 73
    .local v1, "lex":Lorg/icepdf/core/pobjects/functions/postscript/Lexer;
    invoke-virtual {v1, v0}, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->setInputStream(Ljava/io/InputStream;)V

    .line 74
    const/4 v3, 0x2

    new-array v3, v3, [F

    fill-array-data v3, :array_0

    invoke-virtual {v1, v3}, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->parse([F)V

    .line 77
    return-void

    .line 74
    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
    .end array-data
.end method

.method public test2()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 80
    const-string/jumbo v2, "{2 index 1.000000 cvr exch sub 4 1 roll 1 index 1.000000 cvr exch sub \n4 1 roll 0 index 1.000000 cvr exch sub 4 1 roll 1.000000 4 1 \nroll 7 -1 roll 1.000000 cvr exch sub 7 1 roll 6 -1 roll 1.000000 \ncvr exch sub 6 1 roll 5 -1 roll 1.000000 cvr exch sub 5 1 \nroll 4 -1 roll 1.000000 cvr exch sub 4 1 roll pop pop pop }"

    .line 87
    .local v2, "test":Ljava/lang/String;
    new-instance v0, Ljava/io/ByteArrayInputStream;

    invoke-virtual {v2}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    invoke-direct {v0, v3}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 88
    .local v0, "function_4":Ljava/io/InputStream;
    new-instance v1, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;

    invoke-direct {v1}, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;-><init>()V

    .line 89
    .local v1, "lex":Lorg/icepdf/core/pobjects/functions/postscript/Lexer;
    invoke-virtual {v1, v0}, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->setInputStream(Ljava/io/InputStream;)V

    .line 90
    const/4 v3, 0x3

    new-array v3, v3, [F

    fill-array-data v3, :array_0

    invoke-virtual {v1, v3}, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->parse([F)V

    .line 93
    return-void

    .line 90
    :array_0
    .array-data 4
        0x3eb8b806
        0x3dc0ffff
        0x3b808003
    .end array-data
.end method

.method public test5()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 97
    const-string/jumbo v2, "{2 index 1.000000 cvr exch sub 4 1 roll 1 index 1.000000 cvr exch sub \n4 1 roll 0 index 1.000000 cvr exch sub 4 1 roll 1.000000 4 1 \nroll 7 -1 roll 1.000000 cvr exch sub 7 1 roll 6 -1 roll 1.000000 \ncvr exch sub 6 1 roll 5 -1 roll 1.000000 cvr exch sub 5 1 \nroll 4 -1 roll 1.000000 cvr exch sub 4 1 roll pop pop pop }"

    .line 104
    .local v2, "test":Ljava/lang/String;
    new-instance v0, Ljava/io/ByteArrayInputStream;

    invoke-virtual {v2}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    invoke-direct {v0, v3}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 105
    .local v0, "function_4":Ljava/io/InputStream;
    new-instance v1, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;

    invoke-direct {v1}, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;-><init>()V

    .line 106
    .local v1, "lex":Lorg/icepdf/core/pobjects/functions/postscript/Lexer;
    invoke-virtual {v1, v0}, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->setInputStream(Ljava/io/InputStream;)V

    .line 107
    const/4 v3, 0x3

    new-array v3, v3, [F

    fill-array-data v3, :array_0

    invoke-virtual {v1, v3}, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->parse([F)V

    .line 110
    return-void

    .line 107
    :array_0
    .array-data 4
        0x3eb8b806
        0x3dc0ffff
        0x3b808003
    .end array-data
.end method

.method public test6()V
    .locals 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 114
    const-string/jumbo v5, "{1.000000 2 1 roll 1.000000 2 1 roll 1.000000 2 1 roll 0 index 1.000000 \ncvr exch sub 2 1 roll 5 -1 roll 1.000000 cvr exch sub 5 1 \nroll 4 -1 roll 1.000000 cvr exch sub 4 1 roll 3 -1 roll 1.000000 \ncvr exch sub 3 1 roll 2 -1 roll 1.000000 cvr exch sub 2 1 \nroll pop }"

    .line 121
    .local v5, "test":Ljava/lang/String;
    new-instance v0, Ljava/io/ByteArrayInputStream;

    invoke-virtual {v5}, Ljava/lang/String;->getBytes()[B

    move-result-object v8

    invoke-direct {v0, v8}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 122
    .local v0, "function_4":Ljava/io/InputStream;
    new-instance v2, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;

    invoke-direct {v2}, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;-><init>()V

    .line 123
    .local v2, "lex":Lorg/icepdf/core/pobjects/functions/postscript/Lexer;
    invoke-virtual {v2, v0}, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->setInputStream(Ljava/io/InputStream;)V

    .line 124
    const/4 v8, 0x1

    new-array v8, v8, [F

    const/4 v9, 0x0

    const v10, 0x3e9999fe    # 0.300003f

    aput v10, v8, v9

    invoke-virtual {v2, v8}, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->parse([F)V

    .line 129
    const/4 v3, 0x4

    .line 130
    .local v3, "n":I
    const/16 v8, 0x8

    new-array v4, v8, [F

    fill-array-data v4, :array_0

    .line 132
    .local v4, "range":[F
    new-array v7, v3, [F

    .line 136
    .local v7, "y":[F
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v3, :cond_0

    .line 137
    invoke-virtual {v2}, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->getStack()Ljava/util/Stack;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/util/Stack;->elementAt(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Float;

    invoke-virtual {v8}, Ljava/lang/Float;->floatValue()F

    move-result v6

    .line 138
    .local v6, "value":F
    mul-int/lit8 v8, v1, 0x2

    aget v8, v4, v8

    invoke-static {v6, v8}, Ljava/lang/Math;->max(FF)F

    move-result v8

    mul-int/lit8 v9, v1, 0x2

    add-int/lit8 v9, v9, 0x1

    aget v9, v4, v9

    invoke-static {v8, v9}, Ljava/lang/Math;->min(FF)F

    move-result v8

    aput v8, v7, v1

    .line 136
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 142
    .end local v6    # "value":F
    :cond_0
    return-void

    .line 130
    nop

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
        0x0
        0x3f800000    # 1.0f
        0x0
        0x3f800000    # 1.0f
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method public test7()V
    .locals 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 146
    const-string/jumbo v5, "{0 0 0 0 5 4 roll 0 index 3 -1 roll add 2 1 roll pop dup 1 gt {pop 1} if 4 1 roll dup 1 gt {pop 1} if 4 1 roll dup 1 gt {pop 1} if 4 1 roll dup 1 gt {pop 1} if 4 1 roll}"

    .line 151
    .local v5, "test":Ljava/lang/String;
    new-instance v0, Ljava/io/ByteArrayInputStream;

    invoke-virtual {v5}, Ljava/lang/String;->getBytes()[B

    move-result-object v8

    invoke-direct {v0, v8}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 152
    .local v0, "function_4":Ljava/io/InputStream;
    new-instance v2, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;

    invoke-direct {v2}, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;-><init>()V

    .line 153
    .local v2, "lex":Lorg/icepdf/core/pobjects/functions/postscript/Lexer;
    invoke-virtual {v2, v0}, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->setInputStream(Ljava/io/InputStream;)V

    .line 154
    const/4 v8, 0x1

    new-array v8, v8, [F

    const/4 v9, 0x0

    const/high16 v10, 0x3f800000    # 1.0f

    aput v10, v8, v9

    invoke-virtual {v2, v8}, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->parse([F)V

    .line 159
    const/4 v3, 0x4

    .line 160
    .local v3, "n":I
    const/16 v8, 0x8

    new-array v4, v8, [F

    fill-array-data v4, :array_0

    .line 162
    .local v4, "range":[F
    new-array v7, v3, [F

    .line 166
    .local v7, "y":[F
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v3, :cond_0

    .line 167
    invoke-virtual {v2}, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->getStack()Ljava/util/Stack;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/util/Stack;->elementAt(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Float;

    invoke-virtual {v8}, Ljava/lang/Float;->floatValue()F

    move-result v6

    .line 168
    .local v6, "value":F
    mul-int/lit8 v8, v1, 0x2

    aget v8, v4, v8

    invoke-static {v6, v8}, Ljava/lang/Math;->max(FF)F

    move-result v8

    mul-int/lit8 v9, v1, 0x2

    add-int/lit8 v9, v9, 0x1

    aget v9, v4, v9

    invoke-static {v8, v9}, Ljava/lang/Math;->min(FF)F

    move-result v8

    aput v8, v7, v1

    .line 166
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 172
    .end local v6    # "value":F
    :cond_0
    return-void

    .line 160
    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
        0x0
        0x3f800000    # 1.0f
        0x0
        0x3f800000    # 1.0f
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method public test8()V
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 175
    const-string/jumbo v5, "{3 copy pop 0 exch pop exch pop 4 1 roll 3 copy exch pop exch pop 5 1 roll 3 copy pop 0 exch 2 copy lt {exch} if pop exch pop 6 1 roll pop 0 exch pop exch 2 copy lt {exch} if pop 4 1 roll}"

    .line 180
    .local v5, "test":Ljava/lang/String;
    new-instance v0, Ljava/io/ByteArrayInputStream;

    invoke-virtual {v5}, Ljava/lang/String;->getBytes()[B

    move-result-object v8

    invoke-direct {v0, v8}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 181
    .local v0, "function_4":Ljava/io/InputStream;
    new-instance v2, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;

    invoke-direct {v2}, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;-><init>()V

    .line 182
    .local v2, "lex":Lorg/icepdf/core/pobjects/functions/postscript/Lexer;
    invoke-virtual {v2, v0}, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->setInputStream(Ljava/io/InputStream;)V

    .line 183
    const/4 v8, 0x3

    new-array v8, v8, [F

    fill-array-data v8, :array_0

    invoke-virtual {v2, v8}, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->parse([F)V

    .line 188
    const/4 v3, 0x4

    .line 189
    .local v3, "n":I
    const/16 v8, 0x8

    new-array v4, v8, [F

    fill-array-data v4, :array_1

    .line 191
    .local v4, "range":[F
    new-array v7, v3, [F

    .line 196
    .local v7, "y":[F
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v3, :cond_0

    .line 197
    invoke-virtual {v2}, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->getStack()Ljava/util/Stack;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/util/Stack;->elementAt(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Float;

    invoke-virtual {v8}, Ljava/lang/Float;->floatValue()F

    move-result v6

    .line 198
    .local v6, "value":F
    mul-int/lit8 v8, v1, 0x2

    aget v8, v4, v8

    invoke-static {v6, v8}, Ljava/lang/Math;->max(FF)F

    move-result v8

    mul-int/lit8 v9, v1, 0x2

    add-int/lit8 v9, v9, 0x1

    aget v9, v4, v9

    invoke-static {v8, v9}, Ljava/lang/Math;->min(FF)F

    move-result v8

    aput v8, v7, v1

    .line 196
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 202
    .end local v6    # "value":F
    :cond_0
    return-void

    .line 183
    :array_0
    .array-data 4
        0x3f5c28f6    # 0.86f
        0x3da3d708    # 0.07999998f
        0x0
    .end array-data

    .line 189
    :array_1
    .array-data 4
        0x0
        0x3f800000    # 1.0f
        0x0
        0x3f800000    # 1.0f
        0x0
        0x3f800000    # 1.0f
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method
