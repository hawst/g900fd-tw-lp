.class public interface abstract Lorg/icepdf/core/pobjects/graphics/Pattern;
.super Ljava/lang/Object;
.source "Pattern.java"


# static fields
.field public static final PATTERN_TYPE_SHADING:I = 0x2

.field public static final PATTERN_TYPE_TILING:I = 0x1


# virtual methods
.method public abstract getBBox()Landroid/graphics/RectF;
.end method

.method public abstract getMatrix()Landroid/graphics/Matrix;
.end method

.method public abstract getPaint()Landroid/graphics/Paint;
.end method

.method public abstract getPatternType()I
.end method

.method public abstract getType()Ljava/lang/String;
.end method

.method public abstract init()V
.end method

.method public abstract setMatrix(Landroid/graphics/Matrix;)V
.end method

.method public abstract setParentGraphicState(Lorg/icepdf/core/pobjects/graphics/GraphicsState;)V
.end method
