.class public Lorg/icepdf/core/pobjects/OutlineItem;
.super Lorg/icepdf/core/pobjects/Dictionary;
.source "OutlineItem.java"


# instance fields
.field private action:Lorg/icepdf/core/pobjects/actions/Action;

.field private count:I

.field private dest:Lorg/icepdf/core/pobjects/Destination;

.field private first:Lorg/icepdf/core/pobjects/Reference;

.field private last:Lorg/icepdf/core/pobjects/Reference;

.field private loadedSubItems:Z

.field private next:Lorg/icepdf/core/pobjects/Reference;

.field private parent:Lorg/icepdf/core/pobjects/Reference;

.field private prev:Lorg/icepdf/core/pobjects/Reference;

.field private subItems:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<",
            "Lorg/icepdf/core/pobjects/OutlineItem;",
            ">;"
        }
    .end annotation
.end field

.field private title:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V
    .locals 3
    .param p1, "l"    # Lorg/icepdf/core/util/Library;
    .param p2, "h"    # Ljava/util/Hashtable;

    .prologue
    .line 85
    invoke-direct {p0, p1, p2}, Lorg/icepdf/core/pobjects/Dictionary;-><init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V

    .line 71
    const/4 v0, -0x1

    iput v0, p0, Lorg/icepdf/core/pobjects/OutlineItem;->count:I

    .line 86
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/icepdf/core/pobjects/OutlineItem;->loadedSubItems:Z

    .line 87
    new-instance v0, Ljava/util/Vector;

    invoke-direct {p0}, Lorg/icepdf/core/pobjects/OutlineItem;->getCount()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    const/16 v2, 0x10

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/Vector;-><init>(I)V

    iput-object v0, p0, Lorg/icepdf/core/pobjects/OutlineItem;->subItems:Ljava/util/Vector;

    .line 88
    return-void
.end method

.method private ensureSubItemsLoaded()V
    .locals 5

    .prologue
    .line 352
    iget-boolean v4, p0, Lorg/icepdf/core/pobjects/OutlineItem;->loadedSubItems:Z

    if-eqz v4, :cond_1

    .line 389
    :cond_0
    :goto_0
    return-void

    .line 355
    :cond_1
    const/4 v4, 0x1

    iput-boolean v4, p0, Lorg/icepdf/core/pobjects/OutlineItem;->loadedSubItems:Z

    .line 357
    invoke-virtual {p0}, Lorg/icepdf/core/pobjects/OutlineItem;->getFirst()Lorg/icepdf/core/pobjects/Reference;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 359
    invoke-virtual {p0}, Lorg/icepdf/core/pobjects/OutlineItem;->getFirst()Lorg/icepdf/core/pobjects/Reference;

    move-result-object v1

    .line 364
    .local v1, "nextReference":Lorg/icepdf/core/pobjects/Reference;
    :cond_2
    if-eqz v1, :cond_0

    .line 367
    iget-object v4, p0, Lorg/icepdf/core/pobjects/OutlineItem;->library:Lorg/icepdf/core/util/Library;

    invoke-virtual {v4, v1}, Lorg/icepdf/core/util/Library;->getObject(Lorg/icepdf/core/pobjects/Reference;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Hashtable;

    .line 368
    .local v0, "dictionary":Ljava/util/Hashtable;
    if-eqz v0, :cond_0

    .line 372
    new-instance v3, Lorg/icepdf/core/pobjects/OutlineItem;

    iget-object v4, p0, Lorg/icepdf/core/pobjects/OutlineItem;->library:Lorg/icepdf/core/util/Library;

    invoke-direct {v3, v4, v0}, Lorg/icepdf/core/pobjects/OutlineItem;-><init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V

    .line 375
    .local v3, "outLineItem":Lorg/icepdf/core/pobjects/OutlineItem;
    iget-object v4, p0, Lorg/icepdf/core/pobjects/OutlineItem;->subItems:Ljava/util/Vector;

    invoke-virtual {v4, v3}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 379
    move-object v2, v1

    .line 380
    .local v2, "oldNextReference":Lorg/icepdf/core/pobjects/Reference;
    invoke-virtual {v3}, Lorg/icepdf/core/pobjects/OutlineItem;->getNext()Lorg/icepdf/core/pobjects/Reference;

    move-result-object v1

    .line 384
    invoke-virtual {v2, v1}, Lorg/icepdf/core/pobjects/Reference;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    goto :goto_0
.end method

.method private getCount()I
    .locals 3

    .prologue
    .line 255
    iget v0, p0, Lorg/icepdf/core/pobjects/OutlineItem;->count:I

    if-gez v0, :cond_0

    .line 257
    iget-object v0, p0, Lorg/icepdf/core/pobjects/OutlineItem;->library:Lorg/icepdf/core/util/Library;

    iget-object v1, p0, Lorg/icepdf/core/pobjects/OutlineItem;->entries:Ljava/util/Hashtable;

    const-string/jumbo v2, "Count"

    invoke-virtual {v0, v1, v2}, Lorg/icepdf/core/util/Library;->getInt(Ljava/util/Hashtable;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lorg/icepdf/core/pobjects/OutlineItem;->count:I

    .line 259
    :cond_0
    iget v0, p0, Lorg/icepdf/core/pobjects/OutlineItem;->count:I

    return v0
.end method


# virtual methods
.method public getAction()Lorg/icepdf/core/pobjects/actions/Action;
    .locals 4

    .prologue
    .line 132
    iget-object v1, p0, Lorg/icepdf/core/pobjects/OutlineItem;->action:Lorg/icepdf/core/pobjects/actions/Action;

    if-nez v1, :cond_0

    .line 133
    iget-object v1, p0, Lorg/icepdf/core/pobjects/OutlineItem;->library:Lorg/icepdf/core/util/Library;

    iget-object v2, p0, Lorg/icepdf/core/pobjects/OutlineItem;->entries:Ljava/util/Hashtable;

    const-string/jumbo v3, "A"

    invoke-virtual {v1, v2, v3}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 134
    .local v0, "obj":Ljava/lang/Object;
    instance-of v1, v0, Ljava/util/Hashtable;

    if-eqz v1, :cond_0

    .line 135
    new-instance v1, Lorg/icepdf/core/pobjects/actions/Action;

    iget-object v2, p0, Lorg/icepdf/core/pobjects/OutlineItem;->library:Lorg/icepdf/core/util/Library;

    check-cast v0, Ljava/util/Hashtable;

    .end local v0    # "obj":Ljava/lang/Object;
    invoke-direct {v1, v2, v0}, Lorg/icepdf/core/pobjects/actions/Action;-><init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V

    iput-object v1, p0, Lorg/icepdf/core/pobjects/OutlineItem;->action:Lorg/icepdf/core/pobjects/actions/Action;

    .line 138
    :cond_0
    iget-object v1, p0, Lorg/icepdf/core/pobjects/OutlineItem;->action:Lorg/icepdf/core/pobjects/actions/Action;

    return-object v1
.end method

.method public getDest()Lorg/icepdf/core/pobjects/Destination;
    .locals 4

    .prologue
    .line 337
    iget-object v1, p0, Lorg/icepdf/core/pobjects/OutlineItem;->dest:Lorg/icepdf/core/pobjects/Destination;

    if-nez v1, :cond_0

    .line 339
    iget-object v1, p0, Lorg/icepdf/core/pobjects/OutlineItem;->library:Lorg/icepdf/core/util/Library;

    iget-object v2, p0, Lorg/icepdf/core/pobjects/OutlineItem;->entries:Ljava/util/Hashtable;

    const-string/jumbo v3, "Dest"

    invoke-virtual {v1, v2, v3}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 340
    .local v0, "obj":Ljava/lang/Object;
    if-eqz v0, :cond_0

    .line 341
    new-instance v1, Lorg/icepdf/core/pobjects/Destination;

    iget-object v2, p0, Lorg/icepdf/core/pobjects/OutlineItem;->library:Lorg/icepdf/core/util/Library;

    invoke-direct {v1, v2, v0}, Lorg/icepdf/core/pobjects/Destination;-><init>(Lorg/icepdf/core/util/Library;Ljava/lang/Object;)V

    iput-object v1, p0, Lorg/icepdf/core/pobjects/OutlineItem;->dest:Lorg/icepdf/core/pobjects/Destination;

    .line 344
    .end local v0    # "obj":Ljava/lang/Object;
    :cond_0
    iget-object v1, p0, Lorg/icepdf/core/pobjects/OutlineItem;->dest:Lorg/icepdf/core/pobjects/Destination;

    return-object v1
.end method

.method public getFirst()Lorg/icepdf/core/pobjects/Reference;
    .locals 4

    .prologue
    .line 148
    iget-object v1, p0, Lorg/icepdf/core/pobjects/OutlineItem;->first:Lorg/icepdf/core/pobjects/Reference;

    if-nez v1, :cond_1

    .line 149
    iget-object v1, p0, Lorg/icepdf/core/pobjects/OutlineItem;->entries:Ljava/util/Hashtable;

    const-string/jumbo v2, "First"

    invoke-virtual {v1, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 151
    .local v0, "attribute":Ljava/lang/Object;
    if-nez v0, :cond_0

    .line 153
    iget-object v1, p0, Lorg/icepdf/core/pobjects/OutlineItem;->entries:Ljava/util/Hashtable;

    new-instance v2, Lorg/icepdf/core/pobjects/Name;

    const-string/jumbo v3, "First"

    invoke-direct {v2, v3}, Lorg/icepdf/core/pobjects/Name;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 156
    :cond_0
    instance-of v1, v0, Lorg/icepdf/core/pobjects/Reference;

    if-eqz v1, :cond_1

    .line 157
    check-cast v0, Lorg/icepdf/core/pobjects/Reference;

    .end local v0    # "attribute":Ljava/lang/Object;
    iput-object v0, p0, Lorg/icepdf/core/pobjects/OutlineItem;->first:Lorg/icepdf/core/pobjects/Reference;

    .line 160
    :cond_1
    iget-object v1, p0, Lorg/icepdf/core/pobjects/OutlineItem;->first:Lorg/icepdf/core/pobjects/Reference;

    return-object v1
.end method

.method public getLast()Lorg/icepdf/core/pobjects/Reference;
    .locals 4

    .prologue
    .line 170
    iget-object v1, p0, Lorg/icepdf/core/pobjects/OutlineItem;->last:Lorg/icepdf/core/pobjects/Reference;

    if-nez v1, :cond_1

    .line 171
    iget-object v1, p0, Lorg/icepdf/core/pobjects/OutlineItem;->entries:Ljava/util/Hashtable;

    const-string/jumbo v2, "Last"

    invoke-virtual {v1, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 173
    .local v0, "attribute":Ljava/lang/Object;
    if-nez v0, :cond_0

    .line 175
    iget-object v1, p0, Lorg/icepdf/core/pobjects/OutlineItem;->entries:Ljava/util/Hashtable;

    new-instance v2, Lorg/icepdf/core/pobjects/Name;

    const-string/jumbo v3, "Last"

    invoke-direct {v2, v3}, Lorg/icepdf/core/pobjects/Name;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 178
    :cond_0
    instance-of v1, v0, Lorg/icepdf/core/pobjects/Reference;

    if-eqz v1, :cond_1

    .line 179
    check-cast v0, Lorg/icepdf/core/pobjects/Reference;

    .end local v0    # "attribute":Ljava/lang/Object;
    iput-object v0, p0, Lorg/icepdf/core/pobjects/OutlineItem;->last:Lorg/icepdf/core/pobjects/Reference;

    .line 182
    :cond_1
    iget-object v1, p0, Lorg/icepdf/core/pobjects/OutlineItem;->last:Lorg/icepdf/core/pobjects/Reference;

    return-object v1
.end method

.method public getNext()Lorg/icepdf/core/pobjects/Reference;
    .locals 4

    .prologue
    .line 191
    iget-object v1, p0, Lorg/icepdf/core/pobjects/OutlineItem;->next:Lorg/icepdf/core/pobjects/Reference;

    if-nez v1, :cond_1

    .line 192
    iget-object v1, p0, Lorg/icepdf/core/pobjects/OutlineItem;->entries:Ljava/util/Hashtable;

    const-string/jumbo v2, "Next"

    invoke-virtual {v1, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 194
    .local v0, "attribute":Ljava/lang/Object;
    if-nez v0, :cond_0

    .line 196
    iget-object v1, p0, Lorg/icepdf/core/pobjects/OutlineItem;->entries:Ljava/util/Hashtable;

    new-instance v2, Lorg/icepdf/core/pobjects/Name;

    const-string/jumbo v3, "Next"

    invoke-direct {v2, v3}, Lorg/icepdf/core/pobjects/Name;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 199
    :cond_0
    instance-of v1, v0, Lorg/icepdf/core/pobjects/Reference;

    if-eqz v1, :cond_1

    .line 200
    check-cast v0, Lorg/icepdf/core/pobjects/Reference;

    .end local v0    # "attribute":Ljava/lang/Object;
    iput-object v0, p0, Lorg/icepdf/core/pobjects/OutlineItem;->next:Lorg/icepdf/core/pobjects/Reference;

    .line 203
    :cond_1
    iget-object v1, p0, Lorg/icepdf/core/pobjects/OutlineItem;->next:Lorg/icepdf/core/pobjects/Reference;

    return-object v1
.end method

.method public getParent()Lorg/icepdf/core/pobjects/Reference;
    .locals 4

    .prologue
    .line 234
    iget-object v1, p0, Lorg/icepdf/core/pobjects/OutlineItem;->parent:Lorg/icepdf/core/pobjects/Reference;

    if-nez v1, :cond_1

    .line 235
    iget-object v1, p0, Lorg/icepdf/core/pobjects/OutlineItem;->entries:Ljava/util/Hashtable;

    const-string/jumbo v2, "Parent"

    invoke-virtual {v1, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 237
    .local v0, "attribute":Ljava/lang/Object;
    if-nez v0, :cond_0

    .line 239
    iget-object v1, p0, Lorg/icepdf/core/pobjects/OutlineItem;->entries:Ljava/util/Hashtable;

    new-instance v2, Lorg/icepdf/core/pobjects/Name;

    const-string/jumbo v3, "Parent"

    invoke-direct {v2, v3}, Lorg/icepdf/core/pobjects/Name;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 242
    :cond_0
    instance-of v1, v0, Lorg/icepdf/core/pobjects/Reference;

    if-eqz v1, :cond_1

    .line 243
    check-cast v0, Lorg/icepdf/core/pobjects/Reference;

    .end local v0    # "attribute":Ljava/lang/Object;
    iput-object v0, p0, Lorg/icepdf/core/pobjects/OutlineItem;->parent:Lorg/icepdf/core/pobjects/Reference;

    .line 246
    :cond_1
    iget-object v1, p0, Lorg/icepdf/core/pobjects/OutlineItem;->parent:Lorg/icepdf/core/pobjects/Reference;

    return-object v1
.end method

.method public getPrev()Lorg/icepdf/core/pobjects/Reference;
    .locals 4

    .prologue
    .line 212
    iget-object v1, p0, Lorg/icepdf/core/pobjects/OutlineItem;->prev:Lorg/icepdf/core/pobjects/Reference;

    if-nez v1, :cond_1

    .line 213
    iget-object v1, p0, Lorg/icepdf/core/pobjects/OutlineItem;->entries:Ljava/util/Hashtable;

    const-string/jumbo v2, "Prev"

    invoke-virtual {v1, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 215
    .local v0, "attribute":Ljava/lang/Object;
    if-nez v0, :cond_0

    .line 217
    iget-object v1, p0, Lorg/icepdf/core/pobjects/OutlineItem;->entries:Ljava/util/Hashtable;

    new-instance v2, Lorg/icepdf/core/pobjects/Name;

    const-string/jumbo v3, "Prev"

    invoke-direct {v2, v3}, Lorg/icepdf/core/pobjects/Name;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 220
    :cond_0
    instance-of v1, v0, Lorg/icepdf/core/pobjects/Reference;

    if-eqz v1, :cond_1

    .line 221
    check-cast v0, Lorg/icepdf/core/pobjects/Reference;

    .end local v0    # "attribute":Ljava/lang/Object;
    iput-object v0, p0, Lorg/icepdf/core/pobjects/OutlineItem;->prev:Lorg/icepdf/core/pobjects/Reference;

    .line 224
    :cond_1
    iget-object v1, p0, Lorg/icepdf/core/pobjects/OutlineItem;->prev:Lorg/icepdf/core/pobjects/Reference;

    return-object v1
.end method

.method public getSubItem(I)Lorg/icepdf/core/pobjects/OutlineItem;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 121
    invoke-direct {p0}, Lorg/icepdf/core/pobjects/OutlineItem;->ensureSubItemsLoaded()V

    .line 122
    iget-object v0, p0, Lorg/icepdf/core/pobjects/OutlineItem;->subItems:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/icepdf/core/pobjects/OutlineItem;

    return-object v0
.end method

.method public getSubItemCount()I
    .locals 1

    .prologue
    .line 106
    invoke-direct {p0}, Lorg/icepdf/core/pobjects/OutlineItem;->ensureSubItemsLoaded()V

    .line 107
    iget-object v0, p0, Lorg/icepdf/core/pobjects/OutlineItem;->subItems:Ljava/util/Vector;

    if-eqz v0, :cond_0

    .line 108
    iget-object v0, p0, Lorg/icepdf/core/pobjects/OutlineItem;->subItems:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    .line 110
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 12

    .prologue
    .line 285
    iget-object v9, p0, Lorg/icepdf/core/pobjects/OutlineItem;->title:Ljava/lang/String;

    if-nez v9, :cond_1

    .line 287
    iget-object v9, p0, Lorg/icepdf/core/pobjects/OutlineItem;->library:Lorg/icepdf/core/util/Library;

    iget-object v10, p0, Lorg/icepdf/core/pobjects/OutlineItem;->entries:Ljava/util/Hashtable;

    const-string/jumbo v11, "Title"

    invoke-virtual {v9, v10, v11}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    .line 288
    .local v4, "obj":Ljava/lang/Object;
    instance-of v9, v4, Lorg/icepdf/core/pobjects/StringObject;

    if-eqz v9, :cond_1

    move-object v5, v4

    .line 289
    check-cast v5, Lorg/icepdf/core/pobjects/StringObject;

    .line 290
    .local v5, "outlineText":Lorg/icepdf/core/pobjects/StringObject;
    iget-object v9, p0, Lorg/icepdf/core/pobjects/OutlineItem;->library:Lorg/icepdf/core/util/Library;

    iget-object v9, v9, Lorg/icepdf/core/util/Library;->securityManager:Lorg/icepdf/core/pobjects/security/SecurityManager;

    invoke-interface {v5, v9}, Lorg/icepdf/core/pobjects/StringObject;->getDecryptedLiteralString(Lorg/icepdf/core/pobjects/security/SecurityManager;)Ljava/lang/String;

    move-result-object v8

    .line 294
    .local v8, "titleText":Ljava/lang/String;
    if-eqz v8, :cond_2

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v9

    const/4 v10, 0x2

    if-lt v9, v10, :cond_2

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Ljava/lang/String;->charAt(I)C

    move-result v9

    const/16 v10, 0xfe

    if-ne v9, v10, :cond_2

    const/4 v9, 0x1

    invoke-virtual {v8, v9}, Ljava/lang/String;->charAt(I)C

    move-result v9

    const/16 v10, 0xff

    if-ne v9, v10, :cond_2

    .line 298
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    .line 301
    .local v7, "sb1":Ljava/lang/StringBuilder;
    const/4 v3, 0x2

    .local v3, "i":I
    :goto_0
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v9

    if-ge v3, v9, :cond_0

    .line 312
    invoke-virtual {v8, v3}, Ljava/lang/String;->charAt(I)C

    move-result v9

    and-int/lit16 v0, v9, 0xff

    .line 313
    .local v0, "b1":I
    add-int/lit8 v9, v3, 0x1

    invoke-virtual {v8, v9}, Ljava/lang/String;->charAt(I)C

    move-result v9

    and-int/lit16 v1, v9, 0xff

    .line 315
    .local v1, "b2":I
    mul-int/lit16 v9, v0, 0x100

    add-int/2addr v9, v1

    int-to-char v9, v9

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 301
    add-int/lit8 v3, v3, 0x2

    goto :goto_0

    .line 317
    .end local v0    # "b1":I
    .end local v1    # "b2":I
    :cond_0
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    iput-object v9, p0, Lorg/icepdf/core/pobjects/OutlineItem;->title:Ljava/lang/String;

    .line 328
    .end local v3    # "i":I
    .end local v4    # "obj":Ljava/lang/Object;
    .end local v5    # "outlineText":Lorg/icepdf/core/pobjects/StringObject;
    .end local v7    # "sb1":Ljava/lang/StringBuilder;
    .end local v8    # "titleText":Ljava/lang/String;
    :cond_1
    :goto_1
    iget-object v9, p0, Lorg/icepdf/core/pobjects/OutlineItem;->title:Ljava/lang/String;

    return-object v9

    .line 318
    .restart local v4    # "obj":Ljava/lang/Object;
    .restart local v5    # "outlineText":Lorg/icepdf/core/pobjects/StringObject;
    .restart local v8    # "titleText":Ljava/lang/String;
    :cond_2
    if-eqz v8, :cond_1

    .line 319
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 320
    .local v6, "sb":Ljava/lang/StringBuilder;
    invoke-static {}, Lorg/icepdf/core/pobjects/fonts/ofont/Encoding;->getPDFDoc()Lorg/icepdf/core/pobjects/fonts/ofont/Encoding;

    move-result-object v2

    .line 321
    .local v2, "enc":Lorg/icepdf/core/pobjects/fonts/ofont/Encoding;
    const/4 v3, 0x0

    .restart local v3    # "i":I
    :goto_2
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v9

    if-ge v3, v9, :cond_3

    .line 322
    invoke-virtual {v8, v3}, Ljava/lang/String;->charAt(I)C

    move-result v9

    invoke-virtual {v2, v9}, Lorg/icepdf/core/pobjects/fonts/ofont/Encoding;->get(C)C

    move-result v9

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 321
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 324
    :cond_3
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    iput-object v9, p0, Lorg/icepdf/core/pobjects/OutlineItem;->title:Ljava/lang/String;

    goto :goto_1
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 97
    invoke-virtual {p0}, Lorg/icepdf/core/pobjects/OutlineItem;->getTitle()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lorg/icepdf/core/pobjects/OutlineItem;->getDest()Lorg/icepdf/core/pobjects/Destination;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lorg/icepdf/core/pobjects/OutlineItem;->getAction()Lorg/icepdf/core/pobjects/actions/Action;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
