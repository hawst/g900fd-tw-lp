.class public Lorg/icepdf/core/pobjects/graphics/TextState;
.super Ljava/lang/Object;
.source "TextState.java"


# static fields
.field public static final MODE_ADD:I = 0x7

.field public static final MODE_FILL:I = 0x0

.field public static final MODE_FILL_ADD:I = 0x4

.field public static final MODE_FILL_STROKE:I = 0x2

.field public static final MODE_FILL_STROKE_ADD:I = 0x6

.field public static final MODE_INVISIBLE:I = 0x3

.field public static final MODE_STROKE:I = 0x1

.field public static final MODE_STROKE_ADD:I = 0x5


# instance fields
.field public cspace:F

.field public currentfont:Lorg/icepdf/core/pobjects/fonts/FontFile;

.field public font:Lorg/icepdf/core/pobjects/fonts/Font;

.field public hScalling:F

.field public leading:F

.field public rmode:I

.field public tlmatrix:Landroid/graphics/Matrix;

.field public tmatrix:Landroid/graphics/Matrix;

.field public trise:F

.field public tsize:F

.field protected type3BBox:Lorg/icepdf/core/pobjects/PRectangle;

.field protected type3HorizontalDisplacement:Landroid/graphics/PointF;

.field public wspace:F


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 143
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 86
    iput v1, p0, Lorg/icepdf/core/pobjects/graphics/TextState;->cspace:F

    .line 93
    iput v1, p0, Lorg/icepdf/core/pobjects/graphics/TextState;->wspace:F

    .line 100
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lorg/icepdf/core/pobjects/graphics/TextState;->hScalling:F

    .line 107
    iput v1, p0, Lorg/icepdf/core/pobjects/graphics/TextState;->leading:F

    .line 112
    iput v1, p0, Lorg/icepdf/core/pobjects/graphics/TextState;->tsize:F

    .line 117
    const/4 v0, 0x0

    iput v0, p0, Lorg/icepdf/core/pobjects/graphics/TextState;->rmode:I

    .line 122
    iput v1, p0, Lorg/icepdf/core/pobjects/graphics/TextState;->trise:F

    .line 127
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lorg/icepdf/core/pobjects/graphics/TextState;->tmatrix:Landroid/graphics/Matrix;

    .line 128
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lorg/icepdf/core/pobjects/graphics/TextState;->tlmatrix:Landroid/graphics/Matrix;

    .line 144
    return-void
.end method

.method public constructor <init>(Lorg/icepdf/core/pobjects/graphics/TextState;)V
    .locals 2
    .param p1, "ts"    # Lorg/icepdf/core/pobjects/graphics/TextState;

    .prologue
    const/4 v1, 0x0

    .line 152
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 86
    iput v1, p0, Lorg/icepdf/core/pobjects/graphics/TextState;->cspace:F

    .line 93
    iput v1, p0, Lorg/icepdf/core/pobjects/graphics/TextState;->wspace:F

    .line 100
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lorg/icepdf/core/pobjects/graphics/TextState;->hScalling:F

    .line 107
    iput v1, p0, Lorg/icepdf/core/pobjects/graphics/TextState;->leading:F

    .line 112
    iput v1, p0, Lorg/icepdf/core/pobjects/graphics/TextState;->tsize:F

    .line 117
    const/4 v0, 0x0

    iput v0, p0, Lorg/icepdf/core/pobjects/graphics/TextState;->rmode:I

    .line 122
    iput v1, p0, Lorg/icepdf/core/pobjects/graphics/TextState;->trise:F

    .line 127
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lorg/icepdf/core/pobjects/graphics/TextState;->tmatrix:Landroid/graphics/Matrix;

    .line 128
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lorg/icepdf/core/pobjects/graphics/TextState;->tlmatrix:Landroid/graphics/Matrix;

    .line 154
    iget v0, p1, Lorg/icepdf/core/pobjects/graphics/TextState;->cspace:F

    iput v0, p0, Lorg/icepdf/core/pobjects/graphics/TextState;->cspace:F

    .line 155
    iget v0, p1, Lorg/icepdf/core/pobjects/graphics/TextState;->wspace:F

    iput v0, p0, Lorg/icepdf/core/pobjects/graphics/TextState;->wspace:F

    .line 156
    iget v0, p1, Lorg/icepdf/core/pobjects/graphics/TextState;->hScalling:F

    iput v0, p0, Lorg/icepdf/core/pobjects/graphics/TextState;->hScalling:F

    .line 157
    iget v0, p1, Lorg/icepdf/core/pobjects/graphics/TextState;->leading:F

    iput v0, p0, Lorg/icepdf/core/pobjects/graphics/TextState;->leading:F

    .line 158
    iget-object v0, p1, Lorg/icepdf/core/pobjects/graphics/TextState;->font:Lorg/icepdf/core/pobjects/fonts/Font;

    iput-object v0, p0, Lorg/icepdf/core/pobjects/graphics/TextState;->font:Lorg/icepdf/core/pobjects/fonts/Font;

    .line 160
    iget-object v0, p1, Lorg/icepdf/core/pobjects/graphics/TextState;->currentfont:Lorg/icepdf/core/pobjects/fonts/FontFile;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lorg/icepdf/core/pobjects/graphics/TextState;->currentfont:Lorg/icepdf/core/pobjects/fonts/FontFile;

    new-instance v1, Landroid/graphics/Matrix;

    invoke-direct {v1}, Landroid/graphics/Matrix;-><init>()V

    invoke-interface {v0, v1}, Lorg/icepdf/core/pobjects/fonts/FontFile;->deriveFont(Landroid/graphics/Matrix;)Lorg/icepdf/core/pobjects/fonts/FontFile;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lorg/icepdf/core/pobjects/graphics/TextState;->currentfont:Lorg/icepdf/core/pobjects/fonts/FontFile;

    .line 162
    iget v0, p1, Lorg/icepdf/core/pobjects/graphics/TextState;->tsize:F

    iput v0, p0, Lorg/icepdf/core/pobjects/graphics/TextState;->tsize:F

    .line 163
    new-instance v0, Landroid/graphics/Matrix;

    iget-object v1, p1, Lorg/icepdf/core/pobjects/graphics/TextState;->tmatrix:Landroid/graphics/Matrix;

    invoke-direct {v0, v1}, Landroid/graphics/Matrix;-><init>(Landroid/graphics/Matrix;)V

    iput-object v0, p0, Lorg/icepdf/core/pobjects/graphics/TextState;->tmatrix:Landroid/graphics/Matrix;

    .line 164
    new-instance v0, Landroid/graphics/Matrix;

    iget-object v1, p1, Lorg/icepdf/core/pobjects/graphics/TextState;->tlmatrix:Landroid/graphics/Matrix;

    invoke-direct {v0, v1}, Landroid/graphics/Matrix;-><init>(Landroid/graphics/Matrix;)V

    iput-object v0, p0, Lorg/icepdf/core/pobjects/graphics/TextState;->tlmatrix:Landroid/graphics/Matrix;

    .line 165
    iget v0, p1, Lorg/icepdf/core/pobjects/graphics/TextState;->rmode:I

    iput v0, p0, Lorg/icepdf/core/pobjects/graphics/TextState;->rmode:I

    .line 166
    iget v0, p1, Lorg/icepdf/core/pobjects/graphics/TextState;->trise:F

    iput v0, p0, Lorg/icepdf/core/pobjects/graphics/TextState;->trise:F

    .line 167
    return-void

    .line 160
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getType3BBox()Lorg/icepdf/core/pobjects/PRectangle;
    .locals 1

    .prologue
    .line 170
    iget-object v0, p0, Lorg/icepdf/core/pobjects/graphics/TextState;->type3BBox:Lorg/icepdf/core/pobjects/PRectangle;

    return-object v0
.end method

.method public getType3HorizontalDisplacement()Landroid/graphics/PointF;
    .locals 1

    .prologue
    .line 178
    iget-object v0, p0, Lorg/icepdf/core/pobjects/graphics/TextState;->type3HorizontalDisplacement:Landroid/graphics/PointF;

    return-object v0
.end method

.method public setType3BBox(Lorg/icepdf/core/pobjects/PRectangle;)V
    .locals 0
    .param p1, "type3BBox"    # Lorg/icepdf/core/pobjects/PRectangle;

    .prologue
    .line 174
    iput-object p1, p0, Lorg/icepdf/core/pobjects/graphics/TextState;->type3BBox:Lorg/icepdf/core/pobjects/PRectangle;

    .line 175
    return-void
.end method

.method public setType3HorizontalDisplacement(Landroid/graphics/PointF;)V
    .locals 0
    .param p1, "type3HorizontalDisplacement"    # Landroid/graphics/PointF;

    .prologue
    .line 182
    iput-object p1, p0, Lorg/icepdf/core/pobjects/graphics/TextState;->type3HorizontalDisplacement:Landroid/graphics/PointF;

    .line 183
    return-void
.end method
