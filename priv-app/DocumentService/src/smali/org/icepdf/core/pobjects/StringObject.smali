.class public interface abstract Lorg/icepdf/core/pobjects/StringObject;
.super Ljava/lang/Object;
.source "StringObject.java"


# virtual methods
.method public abstract getDecryptedLiteralString(Lorg/icepdf/core/pobjects/security/SecurityManager;)Ljava/lang/String;
.end method

.method public abstract getHexString()Ljava/lang/String;
.end method

.method public abstract getHexStringBuffer()Ljava/lang/StringBuilder;
.end method

.method public abstract getLength()I
.end method

.method public abstract getLiteralString()Ljava/lang/String;
.end method

.method public abstract getLiteralStringBuffer()Ljava/lang/StringBuilder;
.end method

.method public abstract getLiteralStringBuffer(ILorg/icepdf/core/pobjects/fonts/FontFile;)Ljava/lang/StringBuilder;
.end method

.method public abstract getReference()Lorg/icepdf/core/pobjects/Reference;
.end method

.method public abstract getUnsignedInt(II)I
.end method

.method public abstract setReference(Lorg/icepdf/core/pobjects/Reference;)V
.end method

.method public abstract toString()Ljava/lang/String;
.end method
