.class public Lorg/icepdf/core/pobjects/graphics/CalRGB;
.super Lorg/icepdf/core/pobjects/graphics/PColorSpace;
.source "CalRGB.java"


# instance fields
.field gamma:[F

.field matrix:[F

.field whitepoint:[F


# direct methods
.method constructor <init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V
    .locals 6
    .param p1, "l"    # Lorg/icepdf/core/util/Library;
    .param p2, "h"    # Ljava/util/Hashtable;

    .prologue
    const/16 v5, 0x9

    const/4 v4, 0x3

    .line 40
    invoke-direct {p0, p1, p2}, Lorg/icepdf/core/pobjects/graphics/PColorSpace;-><init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V

    .line 28
    new-array v2, v4, [F

    fill-array-data v2, :array_0

    iput-object v2, p0, Lorg/icepdf/core/pobjects/graphics/CalRGB;->whitepoint:[F

    .line 31
    new-array v2, v4, [F

    fill-array-data v2, :array_1

    iput-object v2, p0, Lorg/icepdf/core/pobjects/graphics/CalRGB;->gamma:[F

    .line 34
    new-array v2, v5, [F

    fill-array-data v2, :array_2

    iput-object v2, p0, Lorg/icepdf/core/pobjects/graphics/CalRGB;->matrix:[F

    .line 41
    const-string/jumbo v2, "WhitePoint"

    invoke-virtual {p2, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Vector;

    .line 42
    .local v1, "m":Ljava/util/Vector;
    if-nez v1, :cond_0

    .line 44
    new-instance v2, Lorg/icepdf/core/pobjects/Name;

    const-string/jumbo v3, "WhitePoint"

    invoke-direct {v2, v3}, Lorg/icepdf/core/pobjects/Name;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "m":Ljava/util/Vector;
    check-cast v1, Ljava/util/Vector;

    .line 46
    .restart local v1    # "m":Ljava/util/Vector;
    :cond_0
    if-eqz v1, :cond_1

    .line 47
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v4, :cond_1

    .line 48
    iget-object v3, p0, Lorg/icepdf/core/pobjects/graphics/CalRGB;->whitepoint:[F

    invoke-virtual {v1, v0}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Number;

    invoke-virtual {v2}, Ljava/lang/Number;->floatValue()F

    move-result v2

    aput v2, v3, v0

    .line 47
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 51
    .end local v0    # "i":I
    :cond_1
    const-string/jumbo v2, "Gamma"

    invoke-virtual {p2, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "m":Ljava/util/Vector;
    check-cast v1, Ljava/util/Vector;

    .line 52
    .restart local v1    # "m":Ljava/util/Vector;
    if-nez v1, :cond_2

    .line 54
    new-instance v2, Lorg/icepdf/core/pobjects/Name;

    const-string/jumbo v3, "Gamma"

    invoke-direct {v2, v3}, Lorg/icepdf/core/pobjects/Name;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "m":Ljava/util/Vector;
    check-cast v1, Ljava/util/Vector;

    .line 57
    .restart local v1    # "m":Ljava/util/Vector;
    :cond_2
    if-eqz v1, :cond_3

    .line 58
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_1
    if-ge v0, v4, :cond_3

    .line 59
    iget-object v3, p0, Lorg/icepdf/core/pobjects/graphics/CalRGB;->gamma:[F

    invoke-virtual {v1, v0}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Number;

    invoke-virtual {v2}, Ljava/lang/Number;->floatValue()F

    move-result v2

    aput v2, v3, v0

    .line 58
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 62
    .end local v0    # "i":I
    :cond_3
    const-string/jumbo v2, "Matrix"

    invoke-virtual {p2, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "m":Ljava/util/Vector;
    check-cast v1, Ljava/util/Vector;

    .line 63
    .restart local v1    # "m":Ljava/util/Vector;
    if-nez v1, :cond_4

    .line 65
    new-instance v2, Lorg/icepdf/core/pobjects/Name;

    const-string/jumbo v3, "Matrix"

    invoke-direct {v2, v3}, Lorg/icepdf/core/pobjects/Name;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "m":Ljava/util/Vector;
    check-cast v1, Ljava/util/Vector;

    .line 67
    .restart local v1    # "m":Ljava/util/Vector;
    :cond_4
    if-eqz v1, :cond_5

    .line 68
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_2
    if-ge v0, v5, :cond_5

    .line 69
    iget-object v3, p0, Lorg/icepdf/core/pobjects/graphics/CalRGB;->matrix:[F

    invoke-virtual {v1, v0}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Number;

    invoke-virtual {v2}, Ljava/lang/Number;->floatValue()F

    move-result v2

    aput v2, v3, v0

    .line 68
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 72
    .end local v0    # "i":I
    :cond_5
    return-void

    .line 28
    nop

    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
    .end array-data

    .line 31
    :array_1
    .array-data 4
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
    .end array-data

    .line 34
    :array_2
    .array-data 4
        0x3f800000    # 1.0f
        0x0
        0x0
        0x0
        0x3f800000    # 1.0f
        0x0
        0x0
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method


# virtual methods
.method public getColor([F)Lorg/apache/poi/java/awt/Color;
    .locals 4
    .param p1, "f"    # [F

    .prologue
    .line 82
    new-instance v0, Lorg/apache/poi/java/awt/Color;

    const/4 v1, 0x2

    aget v1, p1, v1

    const/4 v2, 0x1

    aget v2, p1, v2

    const/4 v3, 0x0

    aget v3, p1, v3

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/poi/java/awt/Color;-><init>(FFF)V

    return-object v0
.end method

.method public getNumComponents()I
    .locals 1

    .prologue
    .line 76
    const/4 v0, 0x3

    return v0
.end method
