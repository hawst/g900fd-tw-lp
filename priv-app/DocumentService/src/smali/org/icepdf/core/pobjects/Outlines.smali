.class public Lorg/icepdf/core/pobjects/Outlines;
.super Lorg/icepdf/core/pobjects/Dictionary;
.source "Outlines.java"


# instance fields
.field private count:Ljava/lang/Integer;


# direct methods
.method public constructor <init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V
    .locals 3
    .param p1, "l"    # Lorg/icepdf/core/util/Library;
    .param p2, "h"    # Ljava/util/Hashtable;

    .prologue
    .line 51
    invoke-direct {p0, p1, p2}, Lorg/icepdf/core/pobjects/Dictionary;-><init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V

    .line 52
    iget-object v0, p0, Lorg/icepdf/core/pobjects/Outlines;->entries:Ljava/util/Hashtable;

    if-eqz v0, :cond_0

    .line 53
    iget-object v0, p0, Lorg/icepdf/core/pobjects/Outlines;->library:Lorg/icepdf/core/util/Library;

    iget-object v1, p0, Lorg/icepdf/core/pobjects/Outlines;->entries:Ljava/util/Hashtable;

    const-string/jumbo v2, "Count"

    invoke-virtual {v0, v1, v2}, Lorg/icepdf/core/util/Library;->getInt(Ljava/util/Hashtable;Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lorg/icepdf/core/pobjects/Outlines;->count:Ljava/lang/Integer;

    .line 55
    :cond_0
    return-void
.end method


# virtual methods
.method public dispose()V
    .locals 0

    .prologue
    .line 74
    return-void
.end method

.method public getRootOutlineItem()Lorg/icepdf/core/pobjects/OutlineItem;
    .locals 3

    .prologue
    .line 64
    iget-object v0, p0, Lorg/icepdf/core/pobjects/Outlines;->count:Ljava/lang/Integer;

    if-nez v0, :cond_0

    .line 65
    const/4 v0, 0x0

    .line 66
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lorg/icepdf/core/pobjects/OutlineItem;

    iget-object v1, p0, Lorg/icepdf/core/pobjects/Outlines;->library:Lorg/icepdf/core/util/Library;

    iget-object v2, p0, Lorg/icepdf/core/pobjects/Outlines;->entries:Ljava/util/Hashtable;

    invoke-direct {v0, v1, v2}, Lorg/icepdf/core/pobjects/OutlineItem;-><init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V

    goto :goto_0
.end method
