.class public Lorg/icepdf/core/pobjects/graphics/DeviceN;
.super Lorg/icepdf/core/pobjects/graphics/PColorSpace;
.source "DeviceN.java"


# instance fields
.field alternate:Lorg/icepdf/core/pobjects/graphics/PColorSpace;

.field colorants:Ljava/util/Hashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field colorspaces:[Lorg/icepdf/core/pobjects/graphics/PColorSpace;

.field foundCMYK:Z

.field names:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<",
            "Lorg/icepdf/core/pobjects/Name;",
            ">;"
        }
    .end annotation
.end field

.field tintTransform:Lorg/icepdf/core/pobjects/functions/Function;


# direct methods
.method constructor <init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 13
    .param p1, "l"    # Lorg/icepdf/core/util/Library;
    .param p2, "h"    # Ljava/util/Hashtable;
    .param p3, "o1"    # Ljava/lang/Object;
    .param p4, "o2"    # Ljava/lang/Object;
    .param p5, "o3"    # Ljava/lang/Object;
    .param p6, "o4"    # Ljava/lang/Object;

    .prologue
    .line 51
    invoke-direct {p0, p1, p2}, Lorg/icepdf/core/pobjects/graphics/PColorSpace;-><init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V

    .line 45
    new-instance v10, Ljava/util/Hashtable;

    invoke-direct {v10}, Ljava/util/Hashtable;-><init>()V

    iput-object v10, p0, Lorg/icepdf/core/pobjects/graphics/DeviceN;->colorants:Ljava/util/Hashtable;

    .line 52
    check-cast p3, Ljava/util/Vector;

    .end local p3    # "o1":Ljava/lang/Object;
    move-object/from16 v0, p3

    iput-object v0, p0, Lorg/icepdf/core/pobjects/graphics/DeviceN;->names:Ljava/util/Vector;

    .line 53
    move-object/from16 v0, p4

    invoke-static {p1, v0}, Lorg/icepdf/core/pobjects/graphics/DeviceN;->getColorSpace(Lorg/icepdf/core/util/Library;Ljava/lang/Object;)Lorg/icepdf/core/pobjects/graphics/PColorSpace;

    move-result-object v10

    iput-object v10, p0, Lorg/icepdf/core/pobjects/graphics/DeviceN;->alternate:Lorg/icepdf/core/pobjects/graphics/PColorSpace;

    .line 54
    move-object/from16 v0, p5

    invoke-virtual {p1, v0}, Lorg/icepdf/core/util/Library;->getObject(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    invoke-static {p1, v10}, Lorg/icepdf/core/pobjects/functions/Function;->getFunction(Lorg/icepdf/core/util/Library;Ljava/lang/Object;)Lorg/icepdf/core/pobjects/functions/Function;

    move-result-object v10

    iput-object v10, p0, Lorg/icepdf/core/pobjects/graphics/DeviceN;->tintTransform:Lorg/icepdf/core/pobjects/functions/Function;

    .line 55
    if-eqz p6, :cond_0

    .line 56
    iget-object v10, p0, Lorg/icepdf/core/pobjects/graphics/DeviceN;->library:Lorg/icepdf/core/util/Library;

    move-object/from16 v0, p6

    invoke-virtual {v10, v0}, Lorg/icepdf/core/util/Library;->getObject(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Hashtable;

    .line 57
    .local v3, "h1":Ljava/util/Hashtable;
    iget-object v10, p0, Lorg/icepdf/core/pobjects/graphics/DeviceN;->library:Lorg/icepdf/core/util/Library;

    const-string/jumbo v11, "Colorants"

    invoke-virtual {v10, v3, v11}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Hashtable;

    .line 58
    .local v4, "h2":Ljava/util/Hashtable;
    if-eqz v4, :cond_0

    .line 59
    invoke-virtual {v4}, Ljava/util/Hashtable;->keys()Ljava/util/Enumeration;

    move-result-object v2

    .line 60
    .local v2, "e":Ljava/util/Enumeration;
    :goto_0
    invoke-interface {v2}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v10

    if-eqz v10, :cond_0

    .line 61
    invoke-interface {v2}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v8

    .line 62
    .local v8, "o":Ljava/lang/Object;
    invoke-virtual {v4, v8}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    .line 63
    .local v9, "oo":Ljava/lang/Object;
    iget-object v10, p0, Lorg/icepdf/core/pobjects/graphics/DeviceN;->colorants:Ljava/util/Hashtable;

    iget-object v11, p0, Lorg/icepdf/core/pobjects/graphics/DeviceN;->library:Lorg/icepdf/core/util/Library;

    iget-object v12, p0, Lorg/icepdf/core/pobjects/graphics/DeviceN;->library:Lorg/icepdf/core/util/Library;

    invoke-virtual {v12, v9}, Lorg/icepdf/core/util/Library;->getObject(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v12

    invoke-static {v11, v12}, Lorg/icepdf/core/pobjects/graphics/DeviceN;->getColorSpace(Lorg/icepdf/core/util/Library;Ljava/lang/Object;)Lorg/icepdf/core/pobjects/graphics/PColorSpace;

    move-result-object v11

    invoke-virtual {v10, v8, v11}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 67
    .end local v2    # "e":Ljava/util/Enumeration;
    .end local v3    # "h1":Ljava/util/Hashtable;
    .end local v4    # "h2":Ljava/util/Hashtable;
    .end local v8    # "o":Ljava/lang/Object;
    .end local v9    # "oo":Ljava/lang/Object;
    :cond_0
    iget-object v10, p0, Lorg/icepdf/core/pobjects/graphics/DeviceN;->names:Ljava/util/Vector;

    invoke-virtual {v10}, Ljava/util/Vector;->size()I

    move-result v10

    new-array v10, v10, [Lorg/icepdf/core/pobjects/graphics/PColorSpace;

    iput-object v10, p0, Lorg/icepdf/core/pobjects/graphics/DeviceN;->colorspaces:[Lorg/icepdf/core/pobjects/graphics/PColorSpace;

    .line 68
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_1
    iget-object v10, p0, Lorg/icepdf/core/pobjects/graphics/DeviceN;->colorspaces:[Lorg/icepdf/core/pobjects/graphics/PColorSpace;

    array-length v10, v10

    if-ge v5, v10, :cond_2

    .line 69
    iget-object v11, p0, Lorg/icepdf/core/pobjects/graphics/DeviceN;->colorspaces:[Lorg/icepdf/core/pobjects/graphics/PColorSpace;

    iget-object v12, p0, Lorg/icepdf/core/pobjects/graphics/DeviceN;->colorants:Ljava/util/Hashtable;

    iget-object v10, p0, Lorg/icepdf/core/pobjects/graphics/DeviceN;->names:Ljava/util/Vector;

    invoke-virtual {v10, v5}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lorg/icepdf/core/pobjects/Name;

    invoke-virtual {v10}, Lorg/icepdf/core/pobjects/Name;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v12, v10}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lorg/icepdf/core/pobjects/graphics/PColorSpace;

    aput-object v10, v11, v5

    .line 70
    iget-object v10, p0, Lorg/icepdf/core/pobjects/graphics/DeviceN;->colorspaces:[Lorg/icepdf/core/pobjects/graphics/PColorSpace;

    aget-object v10, v10, v5

    if-nez v10, :cond_1

    .line 72
    iget-object v11, p0, Lorg/icepdf/core/pobjects/graphics/DeviceN;->colorspaces:[Lorg/icepdf/core/pobjects/graphics/PColorSpace;

    iget-object v10, p0, Lorg/icepdf/core/pobjects/graphics/DeviceN;->colorants:Ljava/util/Hashtable;

    iget-object v12, p0, Lorg/icepdf/core/pobjects/graphics/DeviceN;->names:Ljava/util/Vector;

    invoke-virtual {v12, v5}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v12

    invoke-virtual {v10, v12}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lorg/icepdf/core/pobjects/graphics/PColorSpace;

    aput-object v10, v11, v5

    .line 68
    :cond_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 78
    :cond_2
    iget-object v10, p0, Lorg/icepdf/core/pobjects/graphics/DeviceN;->names:Ljava/util/Vector;

    invoke-virtual {v10}, Ljava/util/Vector;->size()I

    move-result v10

    const/4 v11, 0x4

    if-ne v10, v11, :cond_8

    .line 79
    const/4 v1, 0x0

    .line 80
    .local v1, "cmykCount":I
    iget-object v10, p0, Lorg/icepdf/core/pobjects/graphics/DeviceN;->names:Ljava/util/Vector;

    invoke-virtual {v10}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .local v6, "i$":Ljava/util/Iterator;
    :cond_3
    :goto_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_8

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lorg/icepdf/core/pobjects/Name;

    .line 81
    .local v7, "name":Lorg/icepdf/core/pobjects/Name;
    invoke-virtual {v7}, Lorg/icepdf/core/pobjects/Name;->getName()Ljava/lang/String;

    move-result-object v10

    sget-object v11, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v10, v11}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v10

    const-string/jumbo v11, "c"

    invoke-virtual {v10, v11}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_5

    .line 82
    add-int/lit8 v1, v1, 0x1

    .line 90
    :cond_4
    :goto_3
    const/4 v10, 0x4

    if-ne v1, v10, :cond_3

    .line 91
    const/4 v10, 0x1

    iput-boolean v10, p0, Lorg/icepdf/core/pobjects/graphics/DeviceN;->foundCMYK:Z

    goto :goto_2

    .line 83
    :cond_5
    invoke-virtual {v7}, Lorg/icepdf/core/pobjects/Name;->getName()Ljava/lang/String;

    move-result-object v10

    sget-object v11, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v10, v11}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v10

    const-string/jumbo v11, "m"

    invoke-virtual {v10, v11}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_6

    .line 84
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 85
    :cond_6
    invoke-virtual {v7}, Lorg/icepdf/core/pobjects/Name;->getName()Ljava/lang/String;

    move-result-object v10

    sget-object v11, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v10, v11}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v10

    const-string/jumbo v11, "y"

    invoke-virtual {v10, v11}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_7

    .line 86
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 87
    :cond_7
    invoke-virtual {v7}, Lorg/icepdf/core/pobjects/Name;->getName()Ljava/lang/String;

    move-result-object v10

    sget-object v11, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v10, v11}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v10

    const-string/jumbo v11, "b"

    invoke-virtual {v10, v11}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 88
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 95
    .end local v1    # "cmykCount":I
    .end local v6    # "i$":Ljava/util/Iterator;
    .end local v7    # "name":Lorg/icepdf/core/pobjects/Name;
    :cond_8
    return-void
.end method


# virtual methods
.method public getColor([F)Lorg/apache/poi/java/awt/Color;
    .locals 3
    .param p1, "f"    # [F

    .prologue
    const/4 v2, 0x0

    .line 102
    iget-boolean v1, p0, Lorg/icepdf/core/pobjects/graphics/DeviceN;->foundCMYK:Z

    if-eqz v1, :cond_0

    .line 103
    new-instance v1, Lorg/icepdf/core/pobjects/graphics/DeviceCMYK;

    invoke-direct {v1, v2, v2}, Lorg/icepdf/core/pobjects/graphics/DeviceCMYK;-><init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V

    invoke-virtual {v1, p1}, Lorg/icepdf/core/pobjects/graphics/DeviceCMYK;->getColor([F)Lorg/apache/poi/java/awt/Color;

    move-result-object v1

    .line 106
    :goto_0
    return-object v1

    .line 105
    :cond_0
    iget-object v1, p0, Lorg/icepdf/core/pobjects/graphics/DeviceN;->tintTransform:Lorg/icepdf/core/pobjects/functions/Function;

    invoke-virtual {v1, p1}, Lorg/icepdf/core/pobjects/functions/Function;->calculate([F)[F

    move-result-object v0

    .line 106
    .local v0, "y":[F
    iget-object v1, p0, Lorg/icepdf/core/pobjects/graphics/DeviceN;->alternate:Lorg/icepdf/core/pobjects/graphics/PColorSpace;

    invoke-static {v0}, Lorg/icepdf/core/pobjects/graphics/DeviceN;->reverse([F)[F

    move-result-object v2

    invoke-virtual {v1, v2}, Lorg/icepdf/core/pobjects/graphics/PColorSpace;->getColor([F)Lorg/apache/poi/java/awt/Color;

    move-result-object v1

    goto :goto_0
.end method

.method public getNumComponents()I
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lorg/icepdf/core/pobjects/graphics/DeviceN;->names:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    return v0
.end method
