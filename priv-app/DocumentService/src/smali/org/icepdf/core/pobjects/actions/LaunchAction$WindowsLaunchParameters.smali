.class public Lorg/icepdf/core/pobjects/actions/LaunchAction$WindowsLaunchParameters;
.super Ljava/lang/Object;
.source "LaunchAction.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/icepdf/core/pobjects/actions/LaunchAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "WindowsLaunchParameters"
.end annotation


# instance fields
.field private final DIRECTORY_KEY:Lorg/icepdf/core/pobjects/Name;

.field private final FILE_KEY:Lorg/icepdf/core/pobjects/Name;

.field private final OPEN_KEY:Lorg/icepdf/core/pobjects/Name;

.field private final PARAMETER_KEY:Lorg/icepdf/core/pobjects/Name;

.field private defaultDirectory:Ljava/lang/String;

.field private launchFile:Ljava/lang/String;

.field private launchFileSpecification:Lorg/icepdf/core/pobjects/actions/FileSpecification;

.field private operation:Ljava/lang/String;

.field private parameters:Ljava/lang/String;

.field final synthetic this$0:Lorg/icepdf/core/pobjects/actions/LaunchAction;


# direct methods
.method public constructor <init>(Lorg/icepdf/core/pobjects/actions/LaunchAction;)V
    .locals 3

    .prologue
    .line 171
    iput-object p1, p0, Lorg/icepdf/core/pobjects/actions/LaunchAction$WindowsLaunchParameters;->this$0:Lorg/icepdf/core/pobjects/actions/LaunchAction;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 150
    new-instance v1, Lorg/icepdf/core/pobjects/Name;

    const-string/jumbo v2, "F"

    invoke-direct {v1, v2}, Lorg/icepdf/core/pobjects/Name;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lorg/icepdf/core/pobjects/actions/LaunchAction$WindowsLaunchParameters;->FILE_KEY:Lorg/icepdf/core/pobjects/Name;

    .line 151
    new-instance v1, Lorg/icepdf/core/pobjects/Name;

    const-string/jumbo v2, "D"

    invoke-direct {v1, v2}, Lorg/icepdf/core/pobjects/Name;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lorg/icepdf/core/pobjects/actions/LaunchAction$WindowsLaunchParameters;->DIRECTORY_KEY:Lorg/icepdf/core/pobjects/Name;

    .line 152
    new-instance v1, Lorg/icepdf/core/pobjects/Name;

    const-string/jumbo v2, "O"

    invoke-direct {v1, v2}, Lorg/icepdf/core/pobjects/Name;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lorg/icepdf/core/pobjects/actions/LaunchAction$WindowsLaunchParameters;->OPEN_KEY:Lorg/icepdf/core/pobjects/Name;

    .line 153
    new-instance v1, Lorg/icepdf/core/pobjects/Name;

    const-string/jumbo v2, "P"

    invoke-direct {v1, v2}, Lorg/icepdf/core/pobjects/Name;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lorg/icepdf/core/pobjects/actions/LaunchAction$WindowsLaunchParameters;->PARAMETER_KEY:Lorg/icepdf/core/pobjects/Name;

    .line 174
    iget-object v1, p0, Lorg/icepdf/core/pobjects/actions/LaunchAction$WindowsLaunchParameters;->FILE_KEY:Lorg/icepdf/core/pobjects/Name;

    invoke-virtual {p1, v1}, Lorg/icepdf/core/pobjects/actions/LaunchAction;->getObject(Lorg/icepdf/core/pobjects/Name;)Ljava/lang/Object;

    move-result-object v0

    .line 175
    .local v0, "value":Ljava/lang/Object;
    instance-of v1, v0, Ljava/util/Hashtable;

    if-eqz v1, :cond_4

    .line 176
    new-instance v1, Lorg/icepdf/core/pobjects/actions/FileSpecification;

    # getter for: Lorg/icepdf/core/pobjects/actions/LaunchAction;->library:Lorg/icepdf/core/util/Library;
    invoke-static {p1}, Lorg/icepdf/core/pobjects/actions/LaunchAction;->access$000(Lorg/icepdf/core/pobjects/actions/LaunchAction;)Lorg/icepdf/core/util/Library;

    move-result-object v2

    check-cast v0, Ljava/util/Hashtable;

    .end local v0    # "value":Ljava/lang/Object;
    invoke-direct {v1, v2, v0}, Lorg/icepdf/core/pobjects/actions/FileSpecification;-><init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V

    iput-object v1, p0, Lorg/icepdf/core/pobjects/actions/LaunchAction$WindowsLaunchParameters;->launchFileSpecification:Lorg/icepdf/core/pobjects/actions/FileSpecification;

    .line 182
    :cond_0
    :goto_0
    iget-object v1, p0, Lorg/icepdf/core/pobjects/actions/LaunchAction$WindowsLaunchParameters;->DIRECTORY_KEY:Lorg/icepdf/core/pobjects/Name;

    invoke-virtual {p1, v1}, Lorg/icepdf/core/pobjects/actions/LaunchAction;->getObject(Lorg/icepdf/core/pobjects/Name;)Ljava/lang/Object;

    move-result-object v0

    .line 183
    .restart local v0    # "value":Ljava/lang/Object;
    instance-of v1, v0, Lorg/icepdf/core/pobjects/StringObject;

    if-eqz v1, :cond_1

    .line 184
    check-cast v0, Lorg/icepdf/core/pobjects/StringObject;

    .end local v0    # "value":Ljava/lang/Object;
    # getter for: Lorg/icepdf/core/pobjects/actions/LaunchAction;->library:Lorg/icepdf/core/util/Library;
    invoke-static {p1}, Lorg/icepdf/core/pobjects/actions/LaunchAction;->access$200(Lorg/icepdf/core/pobjects/actions/LaunchAction;)Lorg/icepdf/core/util/Library;

    move-result-object v1

    invoke-virtual {v1}, Lorg/icepdf/core/util/Library;->getSecurityManager()Lorg/icepdf/core/pobjects/security/SecurityManager;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/icepdf/core/pobjects/StringObject;->getDecryptedLiteralString(Lorg/icepdf/core/pobjects/security/SecurityManager;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lorg/icepdf/core/pobjects/actions/LaunchAction$WindowsLaunchParameters;->defaultDirectory:Ljava/lang/String;

    .line 187
    :cond_1
    iget-object v1, p0, Lorg/icepdf/core/pobjects/actions/LaunchAction$WindowsLaunchParameters;->OPEN_KEY:Lorg/icepdf/core/pobjects/Name;

    invoke-virtual {p1, v1}, Lorg/icepdf/core/pobjects/actions/LaunchAction;->getObject(Lorg/icepdf/core/pobjects/Name;)Ljava/lang/Object;

    move-result-object v0

    .line 188
    .restart local v0    # "value":Ljava/lang/Object;
    instance-of v1, v0, Lorg/icepdf/core/pobjects/StringObject;

    if-eqz v1, :cond_2

    .line 189
    check-cast v0, Lorg/icepdf/core/pobjects/StringObject;

    .end local v0    # "value":Ljava/lang/Object;
    # getter for: Lorg/icepdf/core/pobjects/actions/LaunchAction;->library:Lorg/icepdf/core/util/Library;
    invoke-static {p1}, Lorg/icepdf/core/pobjects/actions/LaunchAction;->access$300(Lorg/icepdf/core/pobjects/actions/LaunchAction;)Lorg/icepdf/core/util/Library;

    move-result-object v1

    invoke-virtual {v1}, Lorg/icepdf/core/util/Library;->getSecurityManager()Lorg/icepdf/core/pobjects/security/SecurityManager;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/icepdf/core/pobjects/StringObject;->getDecryptedLiteralString(Lorg/icepdf/core/pobjects/security/SecurityManager;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lorg/icepdf/core/pobjects/actions/LaunchAction$WindowsLaunchParameters;->operation:Ljava/lang/String;

    .line 192
    :cond_2
    iget-object v1, p0, Lorg/icepdf/core/pobjects/actions/LaunchAction$WindowsLaunchParameters;->PARAMETER_KEY:Lorg/icepdf/core/pobjects/Name;

    invoke-virtual {p1, v1}, Lorg/icepdf/core/pobjects/actions/LaunchAction;->getObject(Lorg/icepdf/core/pobjects/Name;)Ljava/lang/Object;

    move-result-object v0

    .line 193
    .restart local v0    # "value":Ljava/lang/Object;
    instance-of v1, v0, Lorg/icepdf/core/pobjects/StringObject;

    if-eqz v1, :cond_3

    .line 194
    check-cast v0, Lorg/icepdf/core/pobjects/StringObject;

    .end local v0    # "value":Ljava/lang/Object;
    invoke-interface {v0}, Lorg/icepdf/core/pobjects/StringObject;->getLiteralString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lorg/icepdf/core/pobjects/actions/LaunchAction$WindowsLaunchParameters;->parameters:Ljava/lang/String;

    .line 198
    :cond_3
    return-void

    .line 178
    .restart local v0    # "value":Ljava/lang/Object;
    :cond_4
    instance-of v1, v0, Lorg/icepdf/core/pobjects/StringObject;

    if-eqz v1, :cond_0

    .line 179
    check-cast v0, Lorg/icepdf/core/pobjects/StringObject;

    .end local v0    # "value":Ljava/lang/Object;
    # getter for: Lorg/icepdf/core/pobjects/actions/LaunchAction;->library:Lorg/icepdf/core/util/Library;
    invoke-static {p1}, Lorg/icepdf/core/pobjects/actions/LaunchAction;->access$100(Lorg/icepdf/core/pobjects/actions/LaunchAction;)Lorg/icepdf/core/util/Library;

    move-result-object v1

    invoke-virtual {v1}, Lorg/icepdf/core/util/Library;->getSecurityManager()Lorg/icepdf/core/pobjects/security/SecurityManager;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/icepdf/core/pobjects/StringObject;->getDecryptedLiteralString(Lorg/icepdf/core/pobjects/security/SecurityManager;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lorg/icepdf/core/pobjects/actions/LaunchAction$WindowsLaunchParameters;->launchFile:Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method public getDefaultDirectory()Ljava/lang/String;
    .locals 1

    .prologue
    .line 217
    iget-object v0, p0, Lorg/icepdf/core/pobjects/actions/LaunchAction$WindowsLaunchParameters;->defaultDirectory:Ljava/lang/String;

    return-object v0
.end method

.method public getLaunchFile()Ljava/lang/String;
    .locals 1

    .prologue
    .line 207
    iget-object v0, p0, Lorg/icepdf/core/pobjects/actions/LaunchAction$WindowsLaunchParameters;->launchFile:Ljava/lang/String;

    return-object v0
.end method

.method public getLaunchFileSpecification()Lorg/icepdf/core/pobjects/actions/FileSpecification;
    .locals 1

    .prologue
    .line 246
    iget-object v0, p0, Lorg/icepdf/core/pobjects/actions/LaunchAction$WindowsLaunchParameters;->launchFileSpecification:Lorg/icepdf/core/pobjects/actions/FileSpecification;

    return-object v0
.end method

.method public getOperation()Ljava/lang/String;
    .locals 1

    .prologue
    .line 226
    iget-object v0, p0, Lorg/icepdf/core/pobjects/actions/LaunchAction$WindowsLaunchParameters;->operation:Ljava/lang/String;

    return-object v0
.end method

.method public getParameters()Ljava/lang/String;
    .locals 1

    .prologue
    .line 236
    iget-object v0, p0, Lorg/icepdf/core/pobjects/actions/LaunchAction$WindowsLaunchParameters;->parameters:Ljava/lang/String;

    return-object v0
.end method
