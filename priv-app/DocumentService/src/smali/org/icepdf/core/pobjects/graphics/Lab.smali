.class public Lorg/icepdf/core/pobjects/graphics/Lab;
.super Lorg/icepdf/core/pobjects/graphics/PColorSpace;
.source "Lab.java"


# instance fields
.field private aBase:F

.field private aSpread:F

.field private bBase:F

.field private bSpread:F

.field private blackPoint:[F

.field private lBase:F

.field private lSpread:F

.field private range:[F

.field private whitePoint:[F

.field private xBase:F

.field private xSpread:F

.field private yBase:F

.field private ySpread:F

.field private zBase:F

.field private zSpread:F


# direct methods
.method constructor <init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V
    .locals 7
    .param p1, "l"    # Lorg/icepdf/core/util/Library;
    .param p2, "h"    # Ljava/util/Hashtable;

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 55
    invoke-direct {p0, p1, p2}, Lorg/icepdf/core/pobjects/graphics/PColorSpace;-><init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V

    .line 27
    new-array v1, v6, [F

    fill-array-data v1, :array_0

    iput-object v1, p0, Lorg/icepdf/core/pobjects/graphics/Lab;->whitePoint:[F

    .line 30
    new-array v1, v6, [F

    fill-array-data v1, :array_1

    iput-object v1, p0, Lorg/icepdf/core/pobjects/graphics/Lab;->blackPoint:[F

    .line 33
    const/4 v1, 0x4

    new-array v1, v1, [F

    fill-array-data v1, :array_2

    iput-object v1, p0, Lorg/icepdf/core/pobjects/graphics/Lab;->range:[F

    .line 56
    const-string/jumbo v1, "WhitePoint"

    invoke-virtual {p1, p2, v1}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Vector;

    .line 57
    .local v0, "v":Ljava/util/Vector;
    if-eqz v0, :cond_0

    .line 58
    iget-object v2, p0, Lorg/icepdf/core/pobjects/graphics/Lab;->whitePoint:[F

    invoke-virtual {v0, v3}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Number;

    invoke-virtual {v1}, Ljava/lang/Number;->floatValue()F

    move-result v1

    aput v1, v2, v3

    .line 59
    iget-object v2, p0, Lorg/icepdf/core/pobjects/graphics/Lab;->whitePoint:[F

    invoke-virtual {v0, v4}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Number;

    invoke-virtual {v1}, Ljava/lang/Number;->floatValue()F

    move-result v1

    aput v1, v2, v4

    .line 60
    iget-object v2, p0, Lorg/icepdf/core/pobjects/graphics/Lab;->whitePoint:[F

    invoke-virtual {v0, v5}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Number;

    invoke-virtual {v1}, Ljava/lang/Number;->floatValue()F

    move-result v1

    aput v1, v2, v5

    .line 62
    :cond_0
    const-string/jumbo v1, "Range"

    invoke-virtual {p1, p2, v1}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "v":Ljava/util/Vector;
    check-cast v0, Ljava/util/Vector;

    .line 63
    .restart local v0    # "v":Ljava/util/Vector;
    if-eqz v0, :cond_1

    .line 64
    iget-object v2, p0, Lorg/icepdf/core/pobjects/graphics/Lab;->range:[F

    invoke-virtual {v0, v3}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Number;

    invoke-virtual {v1}, Ljava/lang/Number;->floatValue()F

    move-result v1

    aput v1, v2, v3

    .line 65
    iget-object v2, p0, Lorg/icepdf/core/pobjects/graphics/Lab;->range:[F

    invoke-virtual {v0, v4}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Number;

    invoke-virtual {v1}, Ljava/lang/Number;->floatValue()F

    move-result v1

    aput v1, v2, v4

    .line 66
    iget-object v2, p0, Lorg/icepdf/core/pobjects/graphics/Lab;->range:[F

    invoke-virtual {v0, v5}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Number;

    invoke-virtual {v1}, Ljava/lang/Number;->floatValue()F

    move-result v1

    aput v1, v2, v5

    .line 67
    iget-object v2, p0, Lorg/icepdf/core/pobjects/graphics/Lab;->range:[F

    invoke-virtual {v0, v6}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Number;

    invoke-virtual {v1}, Ljava/lang/Number;->floatValue()F

    move-result v1

    aput v1, v2, v6

    .line 70
    :cond_1
    const/4 v1, 0x0

    iput v1, p0, Lorg/icepdf/core/pobjects/graphics/Lab;->lBase:F

    .line 71
    const/high16 v1, 0x42c80000    # 100.0f

    iput v1, p0, Lorg/icepdf/core/pobjects/graphics/Lab;->lSpread:F

    .line 72
    iget-object v1, p0, Lorg/icepdf/core/pobjects/graphics/Lab;->range:[F

    aget v1, v1, v3

    iput v1, p0, Lorg/icepdf/core/pobjects/graphics/Lab;->aBase:F

    .line 73
    iget-object v1, p0, Lorg/icepdf/core/pobjects/graphics/Lab;->range:[F

    aget v1, v1, v4

    iget v2, p0, Lorg/icepdf/core/pobjects/graphics/Lab;->aBase:F

    sub-float/2addr v1, v2

    iput v1, p0, Lorg/icepdf/core/pobjects/graphics/Lab;->aSpread:F

    .line 74
    iget-object v1, p0, Lorg/icepdf/core/pobjects/graphics/Lab;->range:[F

    aget v1, v1, v5

    iput v1, p0, Lorg/icepdf/core/pobjects/graphics/Lab;->bBase:F

    .line 75
    iget-object v1, p0, Lorg/icepdf/core/pobjects/graphics/Lab;->range:[F

    aget v1, v1, v6

    iget v2, p0, Lorg/icepdf/core/pobjects/graphics/Lab;->bBase:F

    sub-float/2addr v1, v2

    iput v1, p0, Lorg/icepdf/core/pobjects/graphics/Lab;->bSpread:F

    .line 77
    iget-object v1, p0, Lorg/icepdf/core/pobjects/graphics/Lab;->blackPoint:[F

    aget v1, v1, v3

    iput v1, p0, Lorg/icepdf/core/pobjects/graphics/Lab;->xBase:F

    .line 78
    iget-object v1, p0, Lorg/icepdf/core/pobjects/graphics/Lab;->whitePoint:[F

    aget v1, v1, v3

    iget v2, p0, Lorg/icepdf/core/pobjects/graphics/Lab;->xBase:F

    sub-float/2addr v1, v2

    iput v1, p0, Lorg/icepdf/core/pobjects/graphics/Lab;->xSpread:F

    .line 79
    iget-object v1, p0, Lorg/icepdf/core/pobjects/graphics/Lab;->blackPoint:[F

    aget v1, v1, v4

    iput v1, p0, Lorg/icepdf/core/pobjects/graphics/Lab;->yBase:F

    .line 80
    iget-object v1, p0, Lorg/icepdf/core/pobjects/graphics/Lab;->whitePoint:[F

    aget v1, v1, v4

    iget v2, p0, Lorg/icepdf/core/pobjects/graphics/Lab;->yBase:F

    sub-float/2addr v1, v2

    iput v1, p0, Lorg/icepdf/core/pobjects/graphics/Lab;->ySpread:F

    .line 81
    iget-object v1, p0, Lorg/icepdf/core/pobjects/graphics/Lab;->blackPoint:[F

    aget v1, v1, v5

    iput v1, p0, Lorg/icepdf/core/pobjects/graphics/Lab;->zBase:F

    .line 82
    iget-object v1, p0, Lorg/icepdf/core/pobjects/graphics/Lab;->whitePoint:[F

    aget v1, v1, v5

    iget v2, p0, Lorg/icepdf/core/pobjects/graphics/Lab;->zBase:F

    sub-float/2addr v1, v2

    iput v1, p0, Lorg/icepdf/core/pobjects/graphics/Lab;->zSpread:F

    .line 83
    return-void

    .line 27
    :array_0
    .array-data 4
        0x3f735200
        0x3f800000    # 1.0f
        0x3f8b5ec8    # 1.08883f
    .end array-data

    .line 30
    :array_1
    .array-data 4
        0x0
        0x0
        0x0
    .end array-data

    .line 33
    :array_2
    .array-data 4
        -0x3d380000    # -100.0f
        0x42c80000    # 100.0f
        -0x3d380000    # -100.0f
        0x42c80000    # 100.0f
    .end array-data
.end method

.method private g(D)D
    .locals 5
    .param p1, "x"    # D

    .prologue
    .line 97
    const-wide v0, 0x3fca7bb300000000L    # 0.2069000005722046

    cmpg-double v0, p1, v0

    if-gez v0, :cond_0

    .line 98
    const-wide v0, 0x3fc070110a137f39L    # 0.12842

    const-wide v2, 0x3fc1a7b0b3919264L    # 0.13793

    sub-double v2, p1, v2

    mul-double p1, v0, v2

    .line 101
    :goto_0
    return-wide p1

    .line 100
    :cond_0
    mul-double v0, p1, p1

    mul-double/2addr p1, v0

    goto :goto_0
.end method

.method private gg(D)D
    .locals 5
    .param p1, "r"    # D

    .prologue
    .line 105
    const-wide v0, 0x3f69a5c37387b719L    # 0.0031308

    cmpl-double v0, p1, v0

    if-lez v0, :cond_0

    .line 106
    const-wide v0, 0x3ff0e147ae147ae1L    # 1.055

    const-wide v2, 0x3fdaaaaaaaaaaaabL    # 0.4166666666666667

    invoke-static {p1, p2, v2, v3}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v2

    mul-double/2addr v0, v2

    const-wide v2, 0x3fac28f5c28f5c29L    # 0.055

    sub-double p1, v0, v2

    .line 109
    :goto_0
    return-wide p1

    .line 108
    :cond_0
    const-wide v0, 0x4029d70a3d70a3d7L    # 12.92

    mul-double/2addr p1, v0

    goto :goto_0
.end method


# virtual methods
.method public getColor([F)Lorg/apache/poi/java/awt/Color;
    .locals 36
    .param p1, "f"    # [F

    .prologue
    .line 124
    const/16 v23, 0x0

    aget v23, p1, v23

    move/from16 v0, v23

    float-to-double v0, v0

    move-wide/from16 v16, v0

    .line 125
    .local v16, "cie_b":D
    const/16 v23, 0x1

    aget v23, p1, v23

    move/from16 v0, v23

    float-to-double v14, v0

    .line 126
    .local v14, "cie_a":D
    const/16 v23, 0x2

    aget v23, p1, v23

    move/from16 v0, v23

    float-to-double v12, v0

    .line 128
    .local v12, "cie_L":D
    const-wide/high16 v32, 0x4030000000000000L    # 16.0

    add-double v32, v32, v12

    const-wide/high16 v34, 0x405d000000000000L    # 116.0

    div-double v28, v32, v34

    .line 129
    .local v28, "var_Y":D
    const-wide v32, 0x3f60624dd2f1a9fcL    # 0.002

    mul-double v32, v32, v14

    add-double v26, v28, v32

    .line 130
    .local v26, "var_X":D
    const-wide v32, 0x3f747ae147ae147bL    # 0.005

    mul-double v32, v32, v16

    sub-double v30, v28, v32

    .line 131
    .local v30, "var_Z":D
    move-object/from16 v0, p0

    move-wide/from16 v1, v26

    invoke-direct {v0, v1, v2}, Lorg/icepdf/core/pobjects/graphics/Lab;->g(D)D

    move-result-wide v4

    .line 132
    .local v4, "X":D
    move-object/from16 v0, p0

    move-wide/from16 v1, v28

    invoke-direct {v0, v1, v2}, Lorg/icepdf/core/pobjects/graphics/Lab;->g(D)D

    move-result-wide v6

    .line 133
    .local v6, "Y":D
    move-object/from16 v0, p0

    move-wide/from16 v1, v30

    invoke-direct {v0, v1, v2}, Lorg/icepdf/core/pobjects/graphics/Lab;->g(D)D

    move-result-wide v8

    .line 134
    .local v8, "Z":D
    move-object/from16 v0, p0

    iget v0, v0, Lorg/icepdf/core/pobjects/graphics/Lab;->xBase:F

    move/from16 v23, v0

    move/from16 v0, v23

    float-to-double v0, v0

    move-wide/from16 v32, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/icepdf/core/pobjects/graphics/Lab;->xSpread:F

    move/from16 v23, v0

    move/from16 v0, v23

    float-to-double v0, v0

    move-wide/from16 v34, v0

    mul-double v34, v34, v4

    add-double v4, v32, v34

    .line 135
    move-object/from16 v0, p0

    iget v0, v0, Lorg/icepdf/core/pobjects/graphics/Lab;->yBase:F

    move/from16 v23, v0

    move/from16 v0, v23

    float-to-double v0, v0

    move-wide/from16 v32, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/icepdf/core/pobjects/graphics/Lab;->ySpread:F

    move/from16 v23, v0

    move/from16 v0, v23

    float-to-double v0, v0

    move-wide/from16 v34, v0

    mul-double v34, v34, v6

    add-double v6, v32, v34

    .line 136
    move-object/from16 v0, p0

    iget v0, v0, Lorg/icepdf/core/pobjects/graphics/Lab;->zBase:F

    move/from16 v23, v0

    move/from16 v0, v23

    float-to-double v0, v0

    move-wide/from16 v32, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/icepdf/core/pobjects/graphics/Lab;->zSpread:F

    move/from16 v23, v0

    move/from16 v0, v23

    float-to-double v0, v0

    move-wide/from16 v34, v0

    mul-double v34, v34, v8

    add-double v8, v32, v34

    .line 137
    const-wide/16 v32, 0x0

    const-wide/high16 v34, 0x3ff0000000000000L    # 1.0

    move-wide/from16 v0, v34

    invoke-static {v0, v1, v4, v5}, Ljava/lang/Math;->min(DD)D

    move-result-wide v34

    invoke-static/range {v32 .. v35}, Ljava/lang/Math;->max(DD)D

    move-result-wide v4

    .line 138
    const-wide/16 v32, 0x0

    const-wide/high16 v34, 0x3ff0000000000000L    # 1.0

    move-wide/from16 v0, v34

    invoke-static {v0, v1, v6, v7}, Ljava/lang/Math;->min(DD)D

    move-result-wide v34

    invoke-static/range {v32 .. v35}, Ljava/lang/Math;->max(DD)D

    move-result-wide v6

    .line 139
    const-wide/16 v32, 0x0

    const-wide/high16 v34, 0x3ff0000000000000L    # 1.0

    move-wide/from16 v0, v34

    invoke-static {v0, v1, v8, v9}, Ljava/lang/Math;->min(DD)D

    move-result-wide v34

    invoke-static/range {v32 .. v35}, Ljava/lang/Math;->max(DD)D

    move-result-wide v8

    .line 147
    const-wide v32, 0x4009ed916872b021L    # 3.241

    mul-double v32, v32, v4

    const-wide v34, -0x400766cf41f212d7L    # -1.5374

    mul-double v34, v34, v6

    add-double v32, v32, v34

    const-wide v34, -0x402016f0068db8bbL    # -0.4986

    mul-double v34, v34, v8

    add-double v24, v32, v34

    .line 148
    .local v24, "r":D
    const-wide v32, -0x4010fc504816f007L    # -0.9692

    mul-double v32, v32, v4

    const-wide v34, 0x3ffe04189374bc6aL    # 1.876

    mul-double v34, v34, v6

    add-double v32, v32, v34

    const-wide v34, 0x3fa54c985f06f694L    # 0.0416

    mul-double v34, v34, v8

    add-double v18, v32, v34

    .line 149
    .local v18, "g":D
    const-wide v32, 0x3fac779a6b50b0f2L    # 0.0556

    mul-double v32, v32, v4

    const-wide v34, -0x4035e353f7ced917L    # -0.204

    mul-double v34, v34, v6

    add-double v32, v32, v34

    const-wide v34, 0x3ff0e978d4fdf3b6L    # 1.057

    mul-double v34, v34, v8

    add-double v10, v32, v34

    .line 150
    .local v10, "b":D
    move-object/from16 v0, p0

    move-wide/from16 v1, v24

    invoke-direct {v0, v1, v2}, Lorg/icepdf/core/pobjects/graphics/Lab;->gg(D)D

    move-result-wide v24

    .line 151
    move-object/from16 v0, p0

    move-wide/from16 v1, v18

    invoke-direct {v0, v1, v2}, Lorg/icepdf/core/pobjects/graphics/Lab;->gg(D)D

    move-result-wide v18

    .line 152
    move-object/from16 v0, p0

    invoke-direct {v0, v10, v11}, Lorg/icepdf/core/pobjects/graphics/Lab;->gg(D)D

    move-result-wide v10

    .line 153
    const-wide v32, 0x406fe00000000000L    # 255.0

    mul-double v32, v32, v24

    move-wide/from16 v0, v32

    double-to-int v0, v0

    move/from16 v22, v0

    .line 154
    .local v22, "ir":I
    const-wide v32, 0x406fe00000000000L    # 255.0

    mul-double v32, v32, v18

    move-wide/from16 v0, v32

    double-to-int v0, v0

    move/from16 v21, v0

    .line 155
    .local v21, "ig":I
    const-wide v32, 0x406fe00000000000L    # 255.0

    mul-double v32, v32, v10

    move-wide/from16 v0, v32

    double-to-int v0, v0

    move/from16 v20, v0

    .line 156
    .local v20, "ib":I
    const/16 v23, 0x0

    const/16 v32, 0xff

    move/from16 v0, v32

    move/from16 v1, v22

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v32

    move/from16 v0, v23

    move/from16 v1, v32

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v22

    .line 157
    const/16 v23, 0x0

    const/16 v32, 0xff

    move/from16 v0, v32

    move/from16 v1, v21

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v32

    move/from16 v0, v23

    move/from16 v1, v32

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v21

    .line 158
    const/16 v23, 0x0

    const/16 v32, 0xff

    move/from16 v0, v32

    move/from16 v1, v20

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v32

    move/from16 v0, v23

    move/from16 v1, v32

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v20

    .line 159
    new-instance v23, Lorg/apache/poi/java/awt/Color;

    move-object/from16 v0, v23

    move/from16 v1, v22

    move/from16 v2, v21

    move/from16 v3, v20

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/poi/java/awt/Color;-><init>(III)V

    return-object v23
.end method

.method public getNumComponents()I
    .locals 1

    .prologue
    .line 89
    const/4 v0, 0x3

    return v0
.end method

.method public normaliseComponentsToFloats([I[FF)V
    .locals 6
    .param p1, "in"    # [I
    .param p2, "out"    # [F
    .param p3, "maxval"    # F

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 113
    invoke-super {p0, p1, p2, p3}, Lorg/icepdf/core/pobjects/graphics/PColorSpace;->normaliseComponentsToFloats([I[FF)V

    .line 114
    iget v0, p0, Lorg/icepdf/core/pobjects/graphics/Lab;->lBase:F

    iget v1, p0, Lorg/icepdf/core/pobjects/graphics/Lab;->lSpread:F

    aget v2, p2, v5

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    aput v0, p2, v5

    .line 115
    iget v0, p0, Lorg/icepdf/core/pobjects/graphics/Lab;->aBase:F

    iget v1, p0, Lorg/icepdf/core/pobjects/graphics/Lab;->aSpread:F

    aget v2, p2, v4

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    aput v0, p2, v4

    .line 116
    iget v0, p0, Lorg/icepdf/core/pobjects/graphics/Lab;->bBase:F

    iget v1, p0, Lorg/icepdf/core/pobjects/graphics/Lab;->bSpread:F

    aget v2, p2, v3

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    aput v0, p2, v3

    .line 117
    return-void
.end method
