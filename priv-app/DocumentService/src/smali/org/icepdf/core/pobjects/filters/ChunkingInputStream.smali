.class public abstract Lorg/icepdf/core/pobjects/filters/ChunkingInputStream;
.super Ljava/io/InputStream;
.source "ChunkingInputStream.java"


# instance fields
.field protected buffer:[B

.field private bufferAvailable:I

.field private bufferPosition:I

.field protected in:Ljava/io/InputStream;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 33
    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    .line 34
    iput-object v1, p0, Lorg/icepdf/core/pobjects/filters/ChunkingInputStream;->in:Ljava/io/InputStream;

    .line 35
    iput-object v1, p0, Lorg/icepdf/core/pobjects/filters/ChunkingInputStream;->buffer:[B

    .line 36
    iput v0, p0, Lorg/icepdf/core/pobjects/filters/ChunkingInputStream;->bufferPosition:I

    .line 37
    iput v0, p0, Lorg/icepdf/core/pobjects/filters/ChunkingInputStream;->bufferAvailable:I

    .line 38
    return-void
.end method

.method private ensureDataAvailable()I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 86
    iget v1, p0, Lorg/icepdf/core/pobjects/filters/ChunkingInputStream;->bufferAvailable:I

    if-lez v1, :cond_0

    .line 87
    iget v1, p0, Lorg/icepdf/core/pobjects/filters/ChunkingInputStream;->bufferAvailable:I

    .line 93
    :goto_0
    return v1

    .line 88
    :cond_0
    iput v2, p0, Lorg/icepdf/core/pobjects/filters/ChunkingInputStream;->bufferPosition:I

    .line 89
    iput v2, p0, Lorg/icepdf/core/pobjects/filters/ChunkingInputStream;->bufferAvailable:I

    .line 90
    invoke-virtual {p0}, Lorg/icepdf/core/pobjects/filters/ChunkingInputStream;->fillInternalBuffer()I

    move-result v0

    .line 91
    .local v0, "avail":I
    if-lez v0, :cond_1

    .line 92
    iput v0, p0, Lorg/icepdf/core/pobjects/filters/ChunkingInputStream;->bufferAvailable:I

    .line 93
    :cond_1
    iget v1, p0, Lorg/icepdf/core/pobjects/filters/ChunkingInputStream;->bufferAvailable:I

    goto :goto_0
.end method


# virtual methods
.method public available()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 168
    iget v0, p0, Lorg/icepdf/core/pobjects/filters/ChunkingInputStream;->bufferAvailable:I

    return v0
.end method

.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 172
    iget-object v0, p0, Lorg/icepdf/core/pobjects/filters/ChunkingInputStream;->in:Ljava/io/InputStream;

    if-eqz v0, :cond_0

    .line 173
    iget-object v0, p0, Lorg/icepdf/core/pobjects/filters/ChunkingInputStream;->in:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    .line 174
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/icepdf/core/pobjects/filters/ChunkingInputStream;->in:Ljava/io/InputStream;

    .line 176
    :cond_0
    return-void
.end method

.method protected fillBufferFromInputStream()I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 56
    const/4 v0, 0x0

    iget-object v1, p0, Lorg/icepdf/core/pobjects/filters/ChunkingInputStream;->buffer:[B

    array-length v1, v1

    invoke-virtual {p0, v0, v1}, Lorg/icepdf/core/pobjects/filters/ChunkingInputStream;->fillBufferFromInputStream(II)I

    move-result v0

    return v0
.end method

.method protected fillBufferFromInputStream(II)I
    .locals 7
    .param p1, "offset"    # I
    .param p2, "length"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 60
    const/4 v2, 0x0

    .line 61
    .local v2, "read":I
    iget-object v3, p0, Lorg/icepdf/core/pobjects/filters/ChunkingInputStream;->in:Ljava/io/InputStream;

    invoke-virtual {v3}, Ljava/io/InputStream;->available()I

    move-result v1

    .line 64
    .local v1, "mayRead":I
    :goto_0
    if-ltz v1, :cond_1

    if-ge v2, p2, :cond_1

    .line 65
    :try_start_0
    iget-object v3, p0, Lorg/icepdf/core/pobjects/filters/ChunkingInputStream;->in:Ljava/io/InputStream;

    iget-object v4, p0, Lorg/icepdf/core/pobjects/filters/ChunkingInputStream;->buffer:[B

    add-int v5, p1, v2

    sub-int v6, p2, v2

    invoke-virtual {v3, v4, v5, v6}, Ljava/io/InputStream;->read([BII)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 66
    .local v0, "currRead":I
    if-gez v0, :cond_0

    if-nez v2, :cond_0

    .line 75
    .end local v0    # "currRead":I
    :goto_1
    return v0

    .line 68
    .restart local v0    # "currRead":I
    :cond_0
    if-gtz v0, :cond_2

    .end local v0    # "currRead":I
    :cond_1
    :goto_2
    move v0, v2

    .line 75
    goto :goto_1

    .line 70
    .restart local v0    # "currRead":I
    :cond_2
    add-int/2addr v2, v0

    goto :goto_0

    .line 72
    .end local v0    # "currRead":I
    :catch_0
    move-exception v3

    goto :goto_2
.end method

.method protected abstract fillInternalBuffer()I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public mark(I)V
    .locals 0
    .param p1, "readlimit"    # I

    .prologue
    .line 106
    return-void
.end method

.method public markSupported()Z
    .locals 1

    .prologue
    .line 102
    const/4 v0, 0x0

    return v0
.end method

.method public read()I
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 112
    invoke-direct {p0}, Lorg/icepdf/core/pobjects/filters/ChunkingInputStream;->ensureDataAvailable()I

    move-result v0

    .line 113
    .local v0, "avail":I
    if-gtz v0, :cond_0

    .line 114
    const/4 v2, -0x1

    .line 118
    :goto_0
    return v2

    .line 115
    :cond_0
    iget-object v2, p0, Lorg/icepdf/core/pobjects/filters/ChunkingInputStream;->buffer:[B

    iget v3, p0, Lorg/icepdf/core/pobjects/filters/ChunkingInputStream;->bufferPosition:I

    aget-byte v1, v2, v3

    .line 116
    .local v1, "b":B
    iget v2, p0, Lorg/icepdf/core/pobjects/filters/ChunkingInputStream;->bufferPosition:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lorg/icepdf/core/pobjects/filters/ChunkingInputStream;->bufferPosition:I

    .line 117
    iget v2, p0, Lorg/icepdf/core/pobjects/filters/ChunkingInputStream;->bufferAvailable:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lorg/icepdf/core/pobjects/filters/ChunkingInputStream;->bufferAvailable:I

    .line 118
    and-int/lit16 v2, v1, 0xff

    goto :goto_0
.end method

.method public read([B)I
    .locals 2
    .param p1, "b"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 122
    const/4 v0, 0x0

    array-length v1, p1

    invoke-virtual {p0, p1, v0, v1}, Lorg/icepdf/core/pobjects/filters/ChunkingInputStream;->read([BII)I

    move-result v0

    return v0
.end method

.method public read([BII)I
    .locals 9
    .param p1, "b"    # [B
    .param p2, "off"    # I
    .param p3, "length"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 126
    const/4 v4, 0x0

    .line 127
    .local v4, "read":I
    :goto_0
    if-ge v4, p3, :cond_0

    .line 128
    invoke-direct {p0}, Lorg/icepdf/core/pobjects/filters/ChunkingInputStream;->ensureDataAvailable()I

    move-result v0

    .line 129
    .local v0, "avail":I
    if-gtz v0, :cond_2

    .line 130
    if-lez v4, :cond_1

    .line 145
    .end local v0    # "avail":I
    .end local v4    # "read":I
    :cond_0
    :goto_1
    return v4

    .line 133
    .restart local v0    # "avail":I
    .restart local v4    # "read":I
    :cond_1
    const/4 v4, -0x1

    goto :goto_1

    .line 136
    :cond_2
    sub-int v8, p3, v4

    invoke-static {v8, v0}, Ljava/lang/Math;->min(II)I

    move-result v7

    .line 137
    .local v7, "toRead":I
    iget v5, p0, Lorg/icepdf/core/pobjects/filters/ChunkingInputStream;->bufferPosition:I

    .line 138
    .local v5, "srcIdx":I
    add-int v1, p2, v4

    .line 139
    .local v1, "dstIdx":I
    const/4 v3, 0x0

    .local v3, "i":I
    move v2, v1

    .end local v1    # "dstIdx":I
    .local v2, "dstIdx":I
    move v6, v5

    .end local v5    # "srcIdx":I
    .local v6, "srcIdx":I
    :goto_2
    if-ge v3, v7, :cond_3

    .line 140
    add-int/lit8 v1, v2, 0x1

    .end local v2    # "dstIdx":I
    .restart local v1    # "dstIdx":I
    iget-object v8, p0, Lorg/icepdf/core/pobjects/filters/ChunkingInputStream;->buffer:[B

    add-int/lit8 v5, v6, 0x1

    .end local v6    # "srcIdx":I
    .restart local v5    # "srcIdx":I
    aget-byte v8, v8, v6

    aput-byte v8, p1, v2

    .line 139
    add-int/lit8 v3, v3, 0x1

    move v2, v1

    .end local v1    # "dstIdx":I
    .restart local v2    # "dstIdx":I
    move v6, v5

    .end local v5    # "srcIdx":I
    .restart local v6    # "srcIdx":I
    goto :goto_2

    .line 141
    :cond_3
    iget v8, p0, Lorg/icepdf/core/pobjects/filters/ChunkingInputStream;->bufferPosition:I

    add-int/2addr v8, v7

    iput v8, p0, Lorg/icepdf/core/pobjects/filters/ChunkingInputStream;->bufferPosition:I

    .line 142
    iget v8, p0, Lorg/icepdf/core/pobjects/filters/ChunkingInputStream;->bufferAvailable:I

    sub-int/2addr v8, v7

    iput v8, p0, Lorg/icepdf/core/pobjects/filters/ChunkingInputStream;->bufferAvailable:I

    .line 143
    add-int/2addr v4, v7

    .line 144
    goto :goto_0
.end method

.method public reset()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 109
    return-void
.end method

.method protected setBufferSize(I)V
    .locals 1
    .param p1, "size"    # I

    .prologue
    .line 45
    new-array v0, p1, [B

    iput-object v0, p0, Lorg/icepdf/core/pobjects/filters/ChunkingInputStream;->buffer:[B

    .line 46
    return-void
.end method

.method protected setInputStream(Ljava/io/InputStream;)V
    .locals 0
    .param p1, "input"    # Ljava/io/InputStream;

    .prologue
    .line 41
    iput-object p1, p0, Lorg/icepdf/core/pobjects/filters/ChunkingInputStream;->in:Ljava/io/InputStream;

    .line 42
    return-void
.end method

.method public skip(J)J
    .locals 11
    .param p1, "n"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 149
    const-wide/16 v2, 0x0

    .line 150
    .local v2, "skipped":J
    :goto_0
    cmp-long v1, v2, p1

    if-gez v1, :cond_0

    .line 151
    invoke-direct {p0}, Lorg/icepdf/core/pobjects/filters/ChunkingInputStream;->ensureDataAvailable()I

    move-result v0

    .line 152
    .local v0, "avail":I
    if-gtz v0, :cond_2

    .line 153
    const-wide/16 v6, 0x0

    cmp-long v1, v2, v6

    if-lez v1, :cond_1

    .line 164
    .end local v0    # "avail":I
    .end local v2    # "skipped":J
    :cond_0
    :goto_1
    return-wide v2

    .line 156
    .restart local v0    # "avail":I
    .restart local v2    # "skipped":J
    :cond_1
    const-wide/16 v2, -0x1

    goto :goto_1

    .line 159
    :cond_2
    sub-long v6, p1, v2

    int-to-long v8, v0

    invoke-static {v6, v7, v8, v9}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v4

    .line 160
    .local v4, "toSkip":J
    iget v1, p0, Lorg/icepdf/core/pobjects/filters/ChunkingInputStream;->bufferPosition:I

    int-to-long v6, v1

    add-long/2addr v6, v4

    long-to-int v1, v6

    iput v1, p0, Lorg/icepdf/core/pobjects/filters/ChunkingInputStream;->bufferPosition:I

    .line 161
    iget v1, p0, Lorg/icepdf/core/pobjects/filters/ChunkingInputStream;->bufferAvailable:I

    int-to-long v6, v1

    sub-long/2addr v6, v4

    long-to-int v1, v6

    iput v1, p0, Lorg/icepdf/core/pobjects/filters/ChunkingInputStream;->bufferAvailable:I

    .line 162
    add-long/2addr v2, v4

    .line 163
    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 180
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 181
    .local v0, "sb":Ljava/lang/StringBuilder;
    invoke-super {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 182
    const-string/jumbo v1, ": "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 183
    iget-object v1, p0, Lorg/icepdf/core/pobjects/filters/ChunkingInputStream;->in:Ljava/io/InputStream;

    if-nez v1, :cond_0

    .line 184
    const-string/jumbo v1, "null"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 187
    :goto_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 186
    :cond_0
    iget-object v1, p0, Lorg/icepdf/core/pobjects/filters/ChunkingInputStream;->in:Ljava/io/InputStream;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method
