.class public Lorg/icepdf/core/pobjects/actions/LaunchAction;
.super Lorg/icepdf/core/pobjects/actions/Action;
.source "LaunchAction.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/icepdf/core/pobjects/actions/LaunchAction$WindowsLaunchParameters;
    }
.end annotation


# static fields
.field public static final FILE_KEY:Lorg/icepdf/core/pobjects/Name;

.field public static final MAC_KEY:Lorg/icepdf/core/pobjects/Name;

.field public static final NEW_WINDOW_KEY:Lorg/icepdf/core/pobjects/Name;

.field public static final UNIX_KEY:Lorg/icepdf/core/pobjects/Name;

.field public static final WIN_KEY:Lorg/icepdf/core/pobjects/Name;


# instance fields
.field private externalFile:Ljava/lang/String;

.field private fileSpecification:Lorg/icepdf/core/pobjects/actions/FileSpecification;

.field private isNewWindow:Ljava/lang/Boolean;

.field private winLaunchParameters:Lorg/icepdf/core/pobjects/actions/LaunchAction$WindowsLaunchParameters;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 37
    new-instance v0, Lorg/icepdf/core/pobjects/Name;

    const-string/jumbo v1, "F"

    invoke-direct {v0, v1}, Lorg/icepdf/core/pobjects/Name;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/icepdf/core/pobjects/actions/LaunchAction;->FILE_KEY:Lorg/icepdf/core/pobjects/Name;

    .line 39
    new-instance v0, Lorg/icepdf/core/pobjects/Name;

    const-string/jumbo v1, "Win"

    invoke-direct {v0, v1}, Lorg/icepdf/core/pobjects/Name;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/icepdf/core/pobjects/actions/LaunchAction;->WIN_KEY:Lorg/icepdf/core/pobjects/Name;

    .line 41
    new-instance v0, Lorg/icepdf/core/pobjects/Name;

    const-string/jumbo v1, "Mac"

    invoke-direct {v0, v1}, Lorg/icepdf/core/pobjects/Name;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/icepdf/core/pobjects/actions/LaunchAction;->MAC_KEY:Lorg/icepdf/core/pobjects/Name;

    .line 43
    new-instance v0, Lorg/icepdf/core/pobjects/Name;

    const-string/jumbo v1, "Unix"

    invoke-direct {v0, v1}, Lorg/icepdf/core/pobjects/Name;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/icepdf/core/pobjects/actions/LaunchAction;->UNIX_KEY:Lorg/icepdf/core/pobjects/Name;

    .line 45
    new-instance v0, Lorg/icepdf/core/pobjects/Name;

    const-string/jumbo v1, "NewWindow"

    invoke-direct {v0, v1}, Lorg/icepdf/core/pobjects/Name;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/icepdf/core/pobjects/actions/LaunchAction;->NEW_WINDOW_KEY:Lorg/icepdf/core/pobjects/Name;

    return-void
.end method

.method public constructor <init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V
    .locals 1
    .param p1, "l"    # Lorg/icepdf/core/util/Library;
    .param p2, "h"    # Ljava/util/Hashtable;

    .prologue
    .line 68
    invoke-direct {p0, p1, p2}, Lorg/icepdf/core/pobjects/actions/Action;-><init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V

    .line 69
    new-instance v0, Lorg/icepdf/core/pobjects/actions/LaunchAction$WindowsLaunchParameters;

    invoke-direct {v0, p0}, Lorg/icepdf/core/pobjects/actions/LaunchAction$WindowsLaunchParameters;-><init>(Lorg/icepdf/core/pobjects/actions/LaunchAction;)V

    iput-object v0, p0, Lorg/icepdf/core/pobjects/actions/LaunchAction;->winLaunchParameters:Lorg/icepdf/core/pobjects/actions/LaunchAction$WindowsLaunchParameters;

    .line 70
    return-void
.end method

.method static synthetic access$000(Lorg/icepdf/core/pobjects/actions/LaunchAction;)Lorg/icepdf/core/util/Library;
    .locals 1
    .param p0, "x0"    # Lorg/icepdf/core/pobjects/actions/LaunchAction;

    .prologue
    .line 34
    iget-object v0, p0, Lorg/icepdf/core/pobjects/actions/LaunchAction;->library:Lorg/icepdf/core/util/Library;

    return-object v0
.end method

.method static synthetic access$100(Lorg/icepdf/core/pobjects/actions/LaunchAction;)Lorg/icepdf/core/util/Library;
    .locals 1
    .param p0, "x0"    # Lorg/icepdf/core/pobjects/actions/LaunchAction;

    .prologue
    .line 34
    iget-object v0, p0, Lorg/icepdf/core/pobjects/actions/LaunchAction;->library:Lorg/icepdf/core/util/Library;

    return-object v0
.end method

.method static synthetic access$200(Lorg/icepdf/core/pobjects/actions/LaunchAction;)Lorg/icepdf/core/util/Library;
    .locals 1
    .param p0, "x0"    # Lorg/icepdf/core/pobjects/actions/LaunchAction;

    .prologue
    .line 34
    iget-object v0, p0, Lorg/icepdf/core/pobjects/actions/LaunchAction;->library:Lorg/icepdf/core/util/Library;

    return-object v0
.end method

.method static synthetic access$300(Lorg/icepdf/core/pobjects/actions/LaunchAction;)Lorg/icepdf/core/util/Library;
    .locals 1
    .param p0, "x0"    # Lorg/icepdf/core/pobjects/actions/LaunchAction;

    .prologue
    .line 34
    iget-object v0, p0, Lorg/icepdf/core/pobjects/actions/LaunchAction;->library:Lorg/icepdf/core/util/Library;

    return-object v0
.end method


# virtual methods
.method public getExternalFile()Ljava/lang/String;
    .locals 2

    .prologue
    .line 80
    sget-object v1, Lorg/icepdf/core/pobjects/actions/LaunchAction;->FILE_KEY:Lorg/icepdf/core/pobjects/Name;

    invoke-virtual {p0, v1}, Lorg/icepdf/core/pobjects/actions/LaunchAction;->getObject(Lorg/icepdf/core/pobjects/Name;)Ljava/lang/Object;

    move-result-object v0

    .line 81
    .local v0, "value":Ljava/lang/Object;
    instance-of v1, v0, Lorg/icepdf/core/pobjects/StringObject;

    if-eqz v1, :cond_1

    .line 82
    check-cast v0, Lorg/icepdf/core/pobjects/StringObject;

    .end local v0    # "value":Ljava/lang/Object;
    iget-object v1, p0, Lorg/icepdf/core/pobjects/actions/LaunchAction;->library:Lorg/icepdf/core/util/Library;

    invoke-virtual {v1}, Lorg/icepdf/core/util/Library;->getSecurityManager()Lorg/icepdf/core/pobjects/security/SecurityManager;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/icepdf/core/pobjects/StringObject;->getDecryptedLiteralString(Lorg/icepdf/core/pobjects/security/SecurityManager;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lorg/icepdf/core/pobjects/actions/LaunchAction;->externalFile:Ljava/lang/String;

    .line 87
    :cond_0
    :goto_0
    iget-object v1, p0, Lorg/icepdf/core/pobjects/actions/LaunchAction;->externalFile:Ljava/lang/String;

    return-object v1

    .line 84
    .restart local v0    # "value":Ljava/lang/Object;
    :cond_1
    invoke-virtual {p0}, Lorg/icepdf/core/pobjects/actions/LaunchAction;->getFileSpecification()Lorg/icepdf/core/pobjects/actions/FileSpecification;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 85
    invoke-virtual {p0}, Lorg/icepdf/core/pobjects/actions/LaunchAction;->getFileSpecification()Lorg/icepdf/core/pobjects/actions/FileSpecification;

    move-result-object v1

    invoke-virtual {v1}, Lorg/icepdf/core/pobjects/actions/FileSpecification;->getFileSpecification()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lorg/icepdf/core/pobjects/actions/LaunchAction;->externalFile:Ljava/lang/String;

    goto :goto_0
.end method

.method public getFileSpecification()Lorg/icepdf/core/pobjects/actions/FileSpecification;
    .locals 3

    .prologue
    .line 134
    sget-object v1, Lorg/icepdf/core/pobjects/actions/LaunchAction;->FILE_KEY:Lorg/icepdf/core/pobjects/Name;

    invoke-virtual {p0, v1}, Lorg/icepdf/core/pobjects/actions/LaunchAction;->getObject(Lorg/icepdf/core/pobjects/Name;)Ljava/lang/Object;

    move-result-object v0

    .line 135
    .local v0, "value":Ljava/lang/Object;
    instance-of v1, v0, Ljava/util/Hashtable;

    if-eqz v1, :cond_0

    .line 136
    new-instance v1, Lorg/icepdf/core/pobjects/actions/FileSpecification;

    iget-object v2, p0, Lorg/icepdf/core/pobjects/actions/LaunchAction;->library:Lorg/icepdf/core/util/Library;

    check-cast v0, Ljava/util/Hashtable;

    .end local v0    # "value":Ljava/lang/Object;
    invoke-direct {v1, v2, v0}, Lorg/icepdf/core/pobjects/actions/FileSpecification;-><init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V

    iput-object v1, p0, Lorg/icepdf/core/pobjects/actions/LaunchAction;->fileSpecification:Lorg/icepdf/core/pobjects/actions/FileSpecification;

    .line 138
    :cond_0
    iget-object v1, p0, Lorg/icepdf/core/pobjects/actions/LaunchAction;->fileSpecification:Lorg/icepdf/core/pobjects/actions/FileSpecification;

    return-object v1
.end method

.method public getNewWindow()Z
    .locals 2

    .prologue
    .line 111
    sget-object v1, Lorg/icepdf/core/pobjects/actions/LaunchAction;->NEW_WINDOW_KEY:Lorg/icepdf/core/pobjects/Name;

    invoke-virtual {p0, v1}, Lorg/icepdf/core/pobjects/actions/LaunchAction;->getObject(Lorg/icepdf/core/pobjects/Name;)Ljava/lang/Object;

    move-result-object v0

    .line 112
    .local v0, "value":Ljava/lang/Object;
    instance-of v1, v0, Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    .line 113
    check-cast v0, Ljava/lang/Boolean;

    .end local v0    # "value":Ljava/lang/Object;
    iput-object v0, p0, Lorg/icepdf/core/pobjects/actions/LaunchAction;->isNewWindow:Ljava/lang/Boolean;

    .line 115
    :cond_0
    iget-object v1, p0, Lorg/icepdf/core/pobjects/actions/LaunchAction;->isNewWindow:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    return v1
.end method

.method public getWinLaunchParameters()Lorg/icepdf/core/pobjects/actions/LaunchAction$WindowsLaunchParameters;
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Lorg/icepdf/core/pobjects/actions/LaunchAction;->winLaunchParameters:Lorg/icepdf/core/pobjects/actions/LaunchAction$WindowsLaunchParameters;

    return-object v0
.end method

.method public setExternalFile(Ljava/lang/String;)V
    .locals 3
    .param p1, "externalFile"    # Ljava/lang/String;

    .prologue
    .line 99
    new-instance v0, Lorg/icepdf/core/pobjects/LiteralStringObject;

    invoke-virtual {p0}, Lorg/icepdf/core/pobjects/actions/LaunchAction;->getPObjectReference()Lorg/icepdf/core/pobjects/Reference;

    move-result-object v1

    iget-object v2, p0, Lorg/icepdf/core/pobjects/actions/LaunchAction;->library:Lorg/icepdf/core/util/Library;

    iget-object v2, v2, Lorg/icepdf/core/util/Library;->securityManager:Lorg/icepdf/core/pobjects/security/SecurityManager;

    invoke-direct {v0, p1, v1, v2}, Lorg/icepdf/core/pobjects/LiteralStringObject;-><init>(Ljava/lang/String;Lorg/icepdf/core/pobjects/Reference;Lorg/icepdf/core/pobjects/security/SecurityManager;)V

    .line 101
    .local v0, "tmp":Lorg/icepdf/core/pobjects/StringObject;
    iget-object v1, p0, Lorg/icepdf/core/pobjects/actions/LaunchAction;->entries:Ljava/util/Hashtable;

    sget-object v2, Lorg/icepdf/core/pobjects/actions/LaunchAction;->FILE_KEY:Lorg/icepdf/core/pobjects/Name;

    invoke-virtual {v1, v2, v0}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 102
    iput-object p1, p0, Lorg/icepdf/core/pobjects/actions/LaunchAction;->externalFile:Ljava/lang/String;

    .line 103
    return-void
.end method
