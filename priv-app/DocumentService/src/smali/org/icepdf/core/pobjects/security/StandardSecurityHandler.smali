.class public Lorg/icepdf/core/pobjects/security/StandardSecurityHandler;
.super Lorg/icepdf/core/pobjects/security/SecurityHandler;
.source "StandardSecurityHandler.java"


# instance fields
.field private encryptionKey:[B

.field private initiated:Z

.field private password:Ljava/lang/String;

.field private standardEncryption:Lorg/icepdf/core/pobjects/security/StandardEncryption;


# direct methods
.method public constructor <init>(Lorg/icepdf/core/pobjects/security/EncryptionDictionary;)V
    .locals 1
    .param p1, "encryptionDictionary"    # Lorg/icepdf/core/pobjects/security/EncryptionDictionary;

    .prologue
    .line 114
    invoke-direct {p0, p1}, Lorg/icepdf/core/pobjects/security/SecurityHandler;-><init>(Lorg/icepdf/core/pobjects/security/EncryptionDictionary;)V

    .line 100
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/icepdf/core/pobjects/security/StandardSecurityHandler;->standardEncryption:Lorg/icepdf/core/pobjects/security/StandardEncryption;

    .line 116
    const-string/jumbo v0, "Adobe Standard Security"

    iput-object v0, p0, Lorg/icepdf/core/pobjects/security/StandardSecurityHandler;->handlerName:Ljava/lang/String;

    .line 117
    return-void
.end method


# virtual methods
.method public decrypt(Lorg/icepdf/core/pobjects/Reference;[B[B)[B
    .locals 1
    .param p1, "objectReference"    # Lorg/icepdf/core/pobjects/Reference;
    .param p2, "encryptionKey"    # [B
    .param p3, "data"    # [B

    .prologue
    .line 195
    invoke-virtual {p0, p1, p2, p3}, Lorg/icepdf/core/pobjects/security/StandardSecurityHandler;->encrypt(Lorg/icepdf/core/pobjects/Reference;[B[B)[B

    move-result-object v0

    return-object v0
.end method

.method public dispose()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 286
    iput-object v0, p0, Lorg/icepdf/core/pobjects/security/StandardSecurityHandler;->standardEncryption:Lorg/icepdf/core/pobjects/security/StandardEncryption;

    .line 287
    iput-object v0, p0, Lorg/icepdf/core/pobjects/security/StandardSecurityHandler;->encryptionKey:[B

    .line 288
    iput-object v0, p0, Lorg/icepdf/core/pobjects/security/StandardSecurityHandler;->permissions:Lorg/icepdf/core/pobjects/security/Permissions;

    .line 290
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/icepdf/core/pobjects/security/StandardSecurityHandler;->initiated:Z

    .line 291
    return-void
.end method

.method public encrypt(Lorg/icepdf/core/pobjects/Reference;[B[B)[B
    .locals 5
    .param p1, "objectReference"    # Lorg/icepdf/core/pobjects/Reference;
    .param p2, "encryptionKey"    # [B
    .param p3, "data"    # [B

    .prologue
    .line 174
    const-string/jumbo v0, ""

    .line 175
    .local v0, "algorithmType":Ljava/lang/String;
    iget-object v3, p0, Lorg/icepdf/core/pobjects/security/StandardSecurityHandler;->encryptionDictionary:Lorg/icepdf/core/pobjects/security/EncryptionDictionary;

    invoke-virtual {v3}, Lorg/icepdf/core/pobjects/security/EncryptionDictionary;->getCryptFilter()Lorg/icepdf/core/pobjects/security/CryptFilter;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 176
    iget-object v3, p0, Lorg/icepdf/core/pobjects/security/StandardSecurityHandler;->encryptionDictionary:Lorg/icepdf/core/pobjects/security/EncryptionDictionary;

    invoke-virtual {v3}, Lorg/icepdf/core/pobjects/security/EncryptionDictionary;->getCryptFilter()Lorg/icepdf/core/pobjects/security/CryptFilter;

    move-result-object v3

    iget-object v4, p0, Lorg/icepdf/core/pobjects/security/StandardSecurityHandler;->encryptionDictionary:Lorg/icepdf/core/pobjects/security/EncryptionDictionary;

    invoke-virtual {v4}, Lorg/icepdf/core/pobjects/security/EncryptionDictionary;->getStrF()Lorg/icepdf/core/pobjects/Name;

    move-result-object v4

    invoke-virtual {v3, v4}, Lorg/icepdf/core/pobjects/security/CryptFilter;->getCryptFilterByName(Lorg/icepdf/core/pobjects/Name;)Lorg/icepdf/core/pobjects/security/CryptFilterEntry;

    move-result-object v2

    .line 179
    .local v2, "cryptFilterEntry":Lorg/icepdf/core/pobjects/security/CryptFilterEntry;
    invoke-virtual {v2}, Lorg/icepdf/core/pobjects/security/CryptFilterEntry;->getCryptFilterMethod()Lorg/icepdf/core/pobjects/Name;

    move-result-object v1

    .line 180
    .local v1, "algrthmname":Lorg/icepdf/core/pobjects/Name;
    if-eqz v1, :cond_0

    .line 181
    invoke-virtual {v1}, Lorg/icepdf/core/pobjects/Name;->getName()Ljava/lang/String;

    move-result-object v0

    .line 187
    .end local v1    # "algrthmname":Lorg/icepdf/core/pobjects/Name;
    .end local v2    # "cryptFilterEntry":Lorg/icepdf/core/pobjects/security/CryptFilterEntry;
    :cond_0
    :goto_0
    iget-object v3, p0, Lorg/icepdf/core/pobjects/security/StandardSecurityHandler;->standardEncryption:Lorg/icepdf/core/pobjects/security/StandardEncryption;

    invoke-virtual {v3, p1, p2, v0, p3}, Lorg/icepdf/core/pobjects/security/StandardEncryption;->generalEncryptionAlgorithm(Lorg/icepdf/core/pobjects/Reference;[BLjava/lang/String;[B)[B

    move-result-object v3

    return-object v3

    .line 183
    :cond_1
    const-string/jumbo v0, "V2"

    goto :goto_0
.end method

.method public getDecryptionKey()[B
    .locals 1

    .prologue
    .line 260
    invoke-virtual {p0}, Lorg/icepdf/core/pobjects/security/StandardSecurityHandler;->getEncryptionKey()[B

    move-result-object v0

    return-object v0
.end method

.method public getEncryptionInputStream(Lorg/icepdf/core/pobjects/Reference;[BLjava/util/Hashtable;Ljava/io/InputStream;)Ljava/io/InputStream;
    .locals 6
    .param p1, "objectReference"    # Lorg/icepdf/core/pobjects/Reference;
    .param p2, "encryptionKey"    # [B
    .param p3, "decodeParams"    # Ljava/util/Hashtable;
    .param p4, "input"    # Ljava/io/InputStream;

    .prologue
    .line 205
    const/4 v2, 0x0

    .line 206
    .local v2, "cryptFilter":Lorg/icepdf/core/pobjects/security/CryptFilterEntry;
    if-eqz p3, :cond_4

    .line 207
    const-string/jumbo v4, "Name"

    invoke-virtual {p3, v4}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/icepdf/core/pobjects/Name;

    .line 209
    .local v3, "filterName":Lorg/icepdf/core/pobjects/Name;
    if-nez v3, :cond_0

    .line 211
    new-instance v4, Lorg/icepdf/core/pobjects/Name;

    const-string/jumbo v5, "Name"

    invoke-direct {v4, v5}, Lorg/icepdf/core/pobjects/Name;-><init>(Ljava/lang/String;)V

    invoke-virtual {p3, v4}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "filterName":Lorg/icepdf/core/pobjects/Name;
    check-cast v3, Lorg/icepdf/core/pobjects/Name;

    .line 216
    .restart local v3    # "filterName":Lorg/icepdf/core/pobjects/Name;
    :cond_0
    const-string/jumbo v4, "Identity"

    invoke-virtual {v3, v4}, Lorg/icepdf/core/pobjects/Name;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 241
    .end local v3    # "filterName":Lorg/icepdf/core/pobjects/Name;
    .end local p4    # "input":Ljava/io/InputStream;
    :goto_0
    return-object p4

    .line 220
    .restart local v3    # "filterName":Lorg/icepdf/core/pobjects/Name;
    .restart local p4    # "input":Ljava/io/InputStream;
    :cond_1
    iget-object v4, p0, Lorg/icepdf/core/pobjects/security/StandardSecurityHandler;->encryptionDictionary:Lorg/icepdf/core/pobjects/security/EncryptionDictionary;

    invoke-virtual {v4}, Lorg/icepdf/core/pobjects/security/EncryptionDictionary;->getCryptFilter()Lorg/icepdf/core/pobjects/security/CryptFilter;

    move-result-object v4

    invoke-virtual {v4, v3}, Lorg/icepdf/core/pobjects/security/CryptFilter;->getCryptFilterByName(Lorg/icepdf/core/pobjects/Name;)Lorg/icepdf/core/pobjects/security/CryptFilterEntry;

    move-result-object v2

    .line 232
    .end local v3    # "filterName":Lorg/icepdf/core/pobjects/Name;
    :cond_2
    :goto_1
    const-string/jumbo v0, ""

    .line 233
    .local v0, "algorithmType":Ljava/lang/String;
    if-eqz v2, :cond_5

    .line 234
    invoke-virtual {v2}, Lorg/icepdf/core/pobjects/security/CryptFilterEntry;->getCryptFilterMethod()Lorg/icepdf/core/pobjects/Name;

    move-result-object v1

    .line 235
    .local v1, "algrthmname":Lorg/icepdf/core/pobjects/Name;
    if-eqz v1, :cond_3

    .line 236
    invoke-virtual {v1}, Lorg/icepdf/core/pobjects/Name;->getName()Ljava/lang/String;

    move-result-object v0

    .line 241
    .end local v1    # "algrthmname":Lorg/icepdf/core/pobjects/Name;
    :cond_3
    :goto_2
    iget-object v4, p0, Lorg/icepdf/core/pobjects/security/StandardSecurityHandler;->standardEncryption:Lorg/icepdf/core/pobjects/security/StandardEncryption;

    invoke-virtual {v4, p1, p2, v0, p4}, Lorg/icepdf/core/pobjects/security/StandardEncryption;->generalEncryptionInputStream(Lorg/icepdf/core/pobjects/Reference;[BLjava/lang/String;Ljava/io/InputStream;)Ljava/io/InputStream;

    move-result-object p4

    goto :goto_0

    .line 226
    .end local v0    # "algorithmType":Ljava/lang/String;
    :cond_4
    iget-object v4, p0, Lorg/icepdf/core/pobjects/security/StandardSecurityHandler;->encryptionDictionary:Lorg/icepdf/core/pobjects/security/EncryptionDictionary;

    invoke-virtual {v4}, Lorg/icepdf/core/pobjects/security/EncryptionDictionary;->getCryptFilter()Lorg/icepdf/core/pobjects/security/CryptFilter;

    move-result-object v4

    if-eqz v4, :cond_2

    .line 227
    iget-object v4, p0, Lorg/icepdf/core/pobjects/security/StandardSecurityHandler;->encryptionDictionary:Lorg/icepdf/core/pobjects/security/EncryptionDictionary;

    invoke-virtual {v4}, Lorg/icepdf/core/pobjects/security/EncryptionDictionary;->getCryptFilter()Lorg/icepdf/core/pobjects/security/CryptFilter;

    move-result-object v4

    iget-object v5, p0, Lorg/icepdf/core/pobjects/security/StandardSecurityHandler;->encryptionDictionary:Lorg/icepdf/core/pobjects/security/EncryptionDictionary;

    invoke-virtual {v5}, Lorg/icepdf/core/pobjects/security/EncryptionDictionary;->getStmF()Lorg/icepdf/core/pobjects/Name;

    move-result-object v5

    invoke-virtual {v4, v5}, Lorg/icepdf/core/pobjects/security/CryptFilter;->getCryptFilterByName(Lorg/icepdf/core/pobjects/Name;)Lorg/icepdf/core/pobjects/security/CryptFilterEntry;

    move-result-object v2

    goto :goto_1

    .line 238
    .restart local v0    # "algorithmType":Ljava/lang/String;
    :cond_5
    const-string/jumbo v0, "V2"

    goto :goto_2
.end method

.method public getEncryptionKey()[B
    .locals 3

    .prologue
    .line 247
    iget-boolean v0, p0, Lorg/icepdf/core/pobjects/security/StandardSecurityHandler;->initiated:Z

    if-nez v0, :cond_0

    .line 249
    invoke-virtual {p0}, Lorg/icepdf/core/pobjects/security/StandardSecurityHandler;->init()V

    .line 252
    :cond_0
    iget-object v0, p0, Lorg/icepdf/core/pobjects/security/StandardSecurityHandler;->standardEncryption:Lorg/icepdf/core/pobjects/security/StandardEncryption;

    iget-object v1, p0, Lorg/icepdf/core/pobjects/security/StandardSecurityHandler;->password:Ljava/lang/String;

    iget-object v2, p0, Lorg/icepdf/core/pobjects/security/StandardSecurityHandler;->encryptionDictionary:Lorg/icepdf/core/pobjects/security/EncryptionDictionary;

    invoke-virtual {v2}, Lorg/icepdf/core/pobjects/security/EncryptionDictionary;->getKeyLength()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lorg/icepdf/core/pobjects/security/StandardEncryption;->encryptionKeyAlgorithm(Ljava/lang/String;I)[B

    move-result-object v0

    iput-object v0, p0, Lorg/icepdf/core/pobjects/security/StandardSecurityHandler;->encryptionKey:[B

    .line 256
    iget-object v0, p0, Lorg/icepdf/core/pobjects/security/StandardSecurityHandler;->encryptionKey:[B

    return-object v0
.end method

.method public getHandlerName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 272
    iget-object v0, p0, Lorg/icepdf/core/pobjects/security/StandardSecurityHandler;->handlerName:Ljava/lang/String;

    return-object v0
.end method

.method public getPermissions()Lorg/icepdf/core/pobjects/security/Permissions;
    .locals 1

    .prologue
    .line 264
    iget-boolean v0, p0, Lorg/icepdf/core/pobjects/security/StandardSecurityHandler;->initiated:Z

    if-nez v0, :cond_0

    .line 266
    invoke-virtual {p0}, Lorg/icepdf/core/pobjects/security/StandardSecurityHandler;->init()V

    .line 268
    :cond_0
    iget-object v0, p0, Lorg/icepdf/core/pobjects/security/StandardSecurityHandler;->permissions:Lorg/icepdf/core/pobjects/security/Permissions;

    return-object v0
.end method

.method public init()V
    .locals 2

    .prologue
    .line 277
    new-instance v0, Lorg/icepdf/core/pobjects/security/StandardEncryption;

    iget-object v1, p0, Lorg/icepdf/core/pobjects/security/StandardSecurityHandler;->encryptionDictionary:Lorg/icepdf/core/pobjects/security/EncryptionDictionary;

    invoke-direct {v0, v1}, Lorg/icepdf/core/pobjects/security/StandardEncryption;-><init>(Lorg/icepdf/core/pobjects/security/EncryptionDictionary;)V

    iput-object v0, p0, Lorg/icepdf/core/pobjects/security/StandardSecurityHandler;->standardEncryption:Lorg/icepdf/core/pobjects/security/StandardEncryption;

    .line 279
    new-instance v0, Lorg/icepdf/core/pobjects/security/Permissions;

    iget-object v1, p0, Lorg/icepdf/core/pobjects/security/StandardSecurityHandler;->encryptionDictionary:Lorg/icepdf/core/pobjects/security/EncryptionDictionary;

    invoke-direct {v0, v1}, Lorg/icepdf/core/pobjects/security/Permissions;-><init>(Lorg/icepdf/core/pobjects/security/EncryptionDictionary;)V

    iput-object v0, p0, Lorg/icepdf/core/pobjects/security/StandardSecurityHandler;->permissions:Lorg/icepdf/core/pobjects/security/Permissions;

    .line 280
    iget-object v0, p0, Lorg/icepdf/core/pobjects/security/StandardSecurityHandler;->permissions:Lorg/icepdf/core/pobjects/security/Permissions;

    invoke-virtual {v0}, Lorg/icepdf/core/pobjects/security/Permissions;->init()V

    .line 282
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/icepdf/core/pobjects/security/StandardSecurityHandler;->initiated:Z

    .line 283
    return-void
.end method

.method public isAuthorized(Ljava/lang/String;)Z
    .locals 5
    .param p1, "password"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x5

    const/4 v2, 0x0

    .line 120
    iget-object v3, p0, Lorg/icepdf/core/pobjects/security/StandardSecurityHandler;->encryptionDictionary:Lorg/icepdf/core/pobjects/security/EncryptionDictionary;

    invoke-virtual {v3}, Lorg/icepdf/core/pobjects/security/EncryptionDictionary;->getRevisionNumber()I

    move-result v3

    if-ge v3, v4, :cond_3

    .line 121
    iget-object v2, p0, Lorg/icepdf/core/pobjects/security/StandardSecurityHandler;->standardEncryption:Lorg/icepdf/core/pobjects/security/StandardEncryption;

    invoke-virtual {v2, p1}, Lorg/icepdf/core/pobjects/security/StandardEncryption;->authenticateUserPassword(Ljava/lang/String;)Z

    move-result v1

    .line 123
    .local v1, "value":Z
    if-nez v1, :cond_2

    .line 125
    iget-object v2, p0, Lorg/icepdf/core/pobjects/security/StandardSecurityHandler;->standardEncryption:Lorg/icepdf/core/pobjects/security/StandardEncryption;

    invoke-virtual {v2, p1}, Lorg/icepdf/core/pobjects/security/StandardEncryption;->authenticateOwnerPassword(Ljava/lang/String;)Z

    move-result v1

    .line 127
    if-eqz v1, :cond_0

    .line 128
    iget-object v2, p0, Lorg/icepdf/core/pobjects/security/StandardSecurityHandler;->standardEncryption:Lorg/icepdf/core/pobjects/security/StandardEncryption;

    invoke-virtual {v2}, Lorg/icepdf/core/pobjects/security/StandardEncryption;->getUserPassword()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lorg/icepdf/core/pobjects/security/StandardSecurityHandler;->password:Ljava/lang/String;

    :cond_0
    :goto_0
    move v2, v1

    .line 143
    .end local v1    # "value":Z
    :cond_1
    :goto_1
    return v2

    .line 132
    .restart local v1    # "value":Z
    :cond_2
    iput-object p1, p0, Lorg/icepdf/core/pobjects/security/StandardSecurityHandler;->password:Ljava/lang/String;

    goto :goto_0

    .line 135
    .end local v1    # "value":Z
    :cond_3
    iget-object v3, p0, Lorg/icepdf/core/pobjects/security/StandardSecurityHandler;->encryptionDictionary:Lorg/icepdf/core/pobjects/security/EncryptionDictionary;

    invoke-virtual {v3}, Lorg/icepdf/core/pobjects/security/EncryptionDictionary;->getRevisionNumber()I

    move-result v3

    if-ne v3, v4, :cond_1

    .line 137
    iget-object v3, p0, Lorg/icepdf/core/pobjects/security/StandardSecurityHandler;->standardEncryption:Lorg/icepdf/core/pobjects/security/StandardEncryption;

    iget-object v4, p0, Lorg/icepdf/core/pobjects/security/StandardSecurityHandler;->encryptionDictionary:Lorg/icepdf/core/pobjects/security/EncryptionDictionary;

    invoke-virtual {v4}, Lorg/icepdf/core/pobjects/security/EncryptionDictionary;->getKeyLength()I

    move-result v4

    invoke-virtual {v3, p1, v4}, Lorg/icepdf/core/pobjects/security/StandardEncryption;->encryptionKeyAlgorithm(Ljava/lang/String;I)[B

    move-result-object v0

    .line 140
    .local v0, "encryptionKey":[B
    iput-object p1, p0, Lorg/icepdf/core/pobjects/security/StandardSecurityHandler;->password:Ljava/lang/String;

    .line 141
    if-eqz v0, :cond_1

    const/4 v2, 0x1

    goto :goto_1
.end method

.method public isOwnerAuthorized(Ljava/lang/String;)Z
    .locals 2
    .param p1, "password"    # Ljava/lang/String;

    .prologue
    .line 149
    iget-object v0, p0, Lorg/icepdf/core/pobjects/security/StandardSecurityHandler;->encryptionDictionary:Lorg/icepdf/core/pobjects/security/EncryptionDictionary;

    invoke-virtual {v0}, Lorg/icepdf/core/pobjects/security/EncryptionDictionary;->getRevisionNumber()I

    move-result v0

    const/4 v1, 0x5

    if-ge v0, v1, :cond_0

    .line 150
    iget-object v0, p0, Lorg/icepdf/core/pobjects/security/StandardSecurityHandler;->standardEncryption:Lorg/icepdf/core/pobjects/security/StandardEncryption;

    invoke-virtual {v0, p1}, Lorg/icepdf/core/pobjects/security/StandardEncryption;->authenticateOwnerPassword(Ljava/lang/String;)Z

    move-result v0

    .line 152
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lorg/icepdf/core/pobjects/security/StandardSecurityHandler;->encryptionDictionary:Lorg/icepdf/core/pobjects/security/EncryptionDictionary;

    invoke-virtual {v0}, Lorg/icepdf/core/pobjects/security/EncryptionDictionary;->isAuthenticatedOwnerPassword()Z

    move-result v0

    goto :goto_0
.end method

.method public isUserAuthorized(Ljava/lang/String;)Z
    .locals 3
    .param p1, "password"    # Ljava/lang/String;

    .prologue
    .line 158
    iget-object v1, p0, Lorg/icepdf/core/pobjects/security/StandardSecurityHandler;->encryptionDictionary:Lorg/icepdf/core/pobjects/security/EncryptionDictionary;

    invoke-virtual {v1}, Lorg/icepdf/core/pobjects/security/EncryptionDictionary;->getRevisionNumber()I

    move-result v1

    const/4 v2, 0x5

    if-ge v1, v2, :cond_1

    .line 159
    iget-object v1, p0, Lorg/icepdf/core/pobjects/security/StandardSecurityHandler;->standardEncryption:Lorg/icepdf/core/pobjects/security/StandardEncryption;

    invoke-virtual {v1, p1}, Lorg/icepdf/core/pobjects/security/StandardEncryption;->authenticateUserPassword(Ljava/lang/String;)Z

    move-result v0

    .line 160
    .local v0, "value":Z
    if-eqz v0, :cond_0

    .line 161
    iput-object p1, p0, Lorg/icepdf/core/pobjects/security/StandardSecurityHandler;->password:Ljava/lang/String;

    .line 165
    .end local v0    # "value":Z
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lorg/icepdf/core/pobjects/security/StandardSecurityHandler;->encryptionDictionary:Lorg/icepdf/core/pobjects/security/EncryptionDictionary;

    invoke-virtual {v1}, Lorg/icepdf/core/pobjects/security/EncryptionDictionary;->isAuthenticatedUserPassword()Z

    move-result v0

    goto :goto_0
.end method
