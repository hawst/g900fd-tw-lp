.class public Lorg/icepdf/core/pobjects/NameTree;
.super Lorg/icepdf/core/pobjects/Dictionary;
.source "NameTree.java"


# instance fields
.field private root:Lorg/icepdf/core/pobjects/NameNode;


# direct methods
.method public constructor <init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V
    .locals 0
    .param p1, "l"    # Lorg/icepdf/core/util/Library;
    .param p2, "h"    # Ljava/util/Hashtable;

    .prologue
    .line 45
    invoke-direct {p0, p1, p2}, Lorg/icepdf/core/pobjects/Dictionary;-><init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V

    .line 46
    return-void
.end method


# virtual methods
.method public dispose()V
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lorg/icepdf/core/pobjects/NameTree;->root:Lorg/icepdf/core/pobjects/NameNode;

    invoke-virtual {v0}, Lorg/icepdf/core/pobjects/NameNode;->dispose()V

    .line 64
    return-void
.end method

.method public getRoot()Lorg/icepdf/core/pobjects/NameNode;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lorg/icepdf/core/pobjects/NameTree;->root:Lorg/icepdf/core/pobjects/NameNode;

    return-object v0
.end method

.method public init()V
    .locals 3

    .prologue
    .line 52
    iget-boolean v0, p0, Lorg/icepdf/core/pobjects/NameTree;->inited:Z

    if-eqz v0, :cond_0

    .line 57
    :goto_0
    return-void

    .line 55
    :cond_0
    new-instance v0, Lorg/icepdf/core/pobjects/NameNode;

    iget-object v1, p0, Lorg/icepdf/core/pobjects/NameTree;->library:Lorg/icepdf/core/util/Library;

    iget-object v2, p0, Lorg/icepdf/core/pobjects/NameTree;->entries:Ljava/util/Hashtable;

    invoke-direct {v0, v1, v2}, Lorg/icepdf/core/pobjects/NameNode;-><init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V

    iput-object v0, p0, Lorg/icepdf/core/pobjects/NameTree;->root:Lorg/icepdf/core/pobjects/NameNode;

    .line 56
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/icepdf/core/pobjects/NameTree;->inited:Z

    goto :goto_0
.end method

.method public searchName(Ljava/lang/String;)Ljava/lang/Object;
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 75
    iget-object v0, p0, Lorg/icepdf/core/pobjects/NameTree;->root:Lorg/icepdf/core/pobjects/NameNode;

    invoke-virtual {v0, p1}, Lorg/icepdf/core/pobjects/NameNode;->searchName(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
