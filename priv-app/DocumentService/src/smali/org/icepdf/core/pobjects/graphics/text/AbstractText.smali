.class public abstract Lorg/icepdf/core/pobjects/graphics/text/AbstractText;
.super Ljava/lang/Object;
.source "AbstractText.java"

# interfaces
.implements Lorg/icepdf/core/pobjects/graphics/text/Text;


# instance fields
.field protected bounds:Landroid/graphics/RectF;

.field protected hasHighlight:Z

.field protected hasSelected:Z

.field protected highlight:Z

.field protected selected:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public clearBounds()V
    .locals 1

    .prologue
    .line 54
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/icepdf/core/pobjects/graphics/text/AbstractText;->bounds:Landroid/graphics/RectF;

    .line 55
    return-void
.end method

.method public abstract getBounds()Landroid/graphics/RectF;
.end method

.method public hasHighligh()Z
    .locals 1

    .prologue
    .line 123
    iget-boolean v0, p0, Lorg/icepdf/core/pobjects/graphics/text/AbstractText;->hasHighlight:Z

    return v0
.end method

.method public hasSelected()Z
    .locals 1

    .prologue
    .line 134
    iget-boolean v0, p0, Lorg/icepdf/core/pobjects/graphics/text/AbstractText;->hasSelected:Z

    return v0
.end method

.method public intersects(Landroid/graphics/RectF;)Z
    .locals 1
    .param p1, "rect"    # Landroid/graphics/RectF;

    .prologue
    .line 73
    const/4 v0, 0x0

    return v0
.end method

.method public isHighlighted()Z
    .locals 1

    .prologue
    .line 102
    iget-boolean v0, p0, Lorg/icepdf/core/pobjects/graphics/text/AbstractText;->highlight:Z

    return v0
.end method

.method public isSelected()Z
    .locals 1

    .prologue
    .line 82
    iget-boolean v0, p0, Lorg/icepdf/core/pobjects/graphics/text/AbstractText;->selected:Z

    return v0
.end method

.method public setHasHighlight(Z)V
    .locals 0
    .param p1, "hasHighlight"    # Z

    .prologue
    .line 144
    iput-boolean p1, p0, Lorg/icepdf/core/pobjects/graphics/text/AbstractText;->hasHighlight:Z

    .line 145
    return-void
.end method

.method public setHasSelected(Z)V
    .locals 0
    .param p1, "hasSelected"    # Z

    .prologue
    .line 154
    iput-boolean p1, p0, Lorg/icepdf/core/pobjects/graphics/text/AbstractText;->hasSelected:Z

    .line 155
    return-void
.end method

.method public setHighlighted(Z)V
    .locals 0
    .param p1, "highlight"    # Z

    .prologue
    .line 112
    iput-boolean p1, p0, Lorg/icepdf/core/pobjects/graphics/text/AbstractText;->highlight:Z

    .line 113
    return-void
.end method

.method public setSelected(Z)V
    .locals 0
    .param p1, "selected"    # Z

    .prologue
    .line 92
    iput-boolean p1, p0, Lorg/icepdf/core/pobjects/graphics/text/AbstractText;->selected:Z

    .line 93
    return-void
.end method
