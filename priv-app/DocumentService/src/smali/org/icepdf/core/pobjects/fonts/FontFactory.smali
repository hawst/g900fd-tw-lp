.class public Lorg/icepdf/core/pobjects/fonts/FontFactory;
.super Ljava/lang/Object;
.source "FontFactory.java"


# static fields
.field private static awtFontSubstitution:Z

.field private static fontFactory:Lorg/icepdf/core/pobjects/fonts/FontFactory;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 116
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getInstance()Lorg/icepdf/core/pobjects/fonts/FontFactory;
    .locals 1

    .prologue
    .line 109
    sget-object v0, Lorg/icepdf/core/pobjects/fonts/FontFactory;->fontFactory:Lorg/icepdf/core/pobjects/fonts/FontFactory;

    if-nez v0, :cond_0

    .line 110
    new-instance v0, Lorg/icepdf/core/pobjects/fonts/FontFactory;

    invoke-direct {v0}, Lorg/icepdf/core/pobjects/fonts/FontFactory;-><init>()V

    sput-object v0, Lorg/icepdf/core/pobjects/fonts/FontFactory;->fontFactory:Lorg/icepdf/core/pobjects/fonts/FontFactory;

    .line 112
    :cond_0
    sget-object v0, Lorg/icepdf/core/pobjects/fonts/FontFactory;->fontFactory:Lorg/icepdf/core/pobjects/fonts/FontFactory;

    return-object v0
.end method


# virtual methods
.method public getFont(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)Lorg/icepdf/core/pobjects/fonts/Font;
    .locals 1
    .param p1, "library"    # Lorg/icepdf/core/util/Library;
    .param p2, "entries"    # Ljava/util/Hashtable;

    .prologue
    .line 120
    new-instance v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;

    invoke-direct {v0, p1, p2}, Lorg/icepdf/core/pobjects/fonts/ofont/Font;-><init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V

    .line 139
    .local v0, "fontDictionary":Lorg/icepdf/core/pobjects/fonts/Font;
    return-object v0
.end method

.method public isAwtFontSubstitution()Z
    .locals 1

    .prologue
    .line 250
    sget-boolean v0, Lorg/icepdf/core/pobjects/fonts/FontFactory;->awtFontSubstitution:Z

    return v0
.end method

.method public setAwtFontSubstitution(Z)V
    .locals 0
    .param p1, "awtFontSubstitution"    # Z

    .prologue
    .line 254
    sput-boolean p1, Lorg/icepdf/core/pobjects/fonts/FontFactory;->awtFontSubstitution:Z

    .line 255
    return-void
.end method

.method public toggleAwtFontSubstitution()V
    .locals 1

    .prologue
    .line 258
    sget-boolean v0, Lorg/icepdf/core/pobjects/fonts/FontFactory;->awtFontSubstitution:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/icepdf/core/pobjects/fonts/FontFactory;->awtFontSubstitution:Z

    .line 259
    return-void

    .line 258
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
