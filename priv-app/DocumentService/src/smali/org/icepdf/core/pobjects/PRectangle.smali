.class public Lorg/icepdf/core/pobjects/PRectangle;
.super Landroid/graphics/RectF;
.source "PRectangle.java"


# instance fields
.field private p1left:F

.field private p1top:F

.field private p2left:F

.field private p2top:F


# direct methods
.method private constructor <init>(FFFF)V
    .locals 0
    .param p1, "x"    # F
    .param p2, "y"    # F
    .param p3, "width"    # F
    .param p4, "height"    # F

    .prologue
    .line 92
    invoke-direct {p0, p1, p2, p3, p4}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 93
    return-void
.end method

.method public constructor <init>(Landroid/graphics/PointF;Landroid/graphics/PointF;)V
    .locals 4
    .param p1, "p1"    # Landroid/graphics/PointF;
    .param p2, "p2"    # Landroid/graphics/PointF;

    .prologue
    .line 73
    invoke-direct {p0}, Landroid/graphics/RectF;-><init>()V

    .line 74
    iget v0, p1, Landroid/graphics/PointF;->x:F

    iget v1, p1, Landroid/graphics/PointF;->y:F

    iget v2, p2, Landroid/graphics/PointF;->x:F

    iget v3, p2, Landroid/graphics/PointF;->y:F

    invoke-direct {p0, v0, v1, v2, v3}, Lorg/icepdf/core/pobjects/PRectangle;->normalizeCoordinates(FFFF)V

    .line 76
    iget v0, p1, Landroid/graphics/PointF;->x:F

    iput v0, p0, Lorg/icepdf/core/pobjects/PRectangle;->p1left:F

    .line 77
    iget v0, p1, Landroid/graphics/PointF;->y:F

    iput v0, p0, Lorg/icepdf/core/pobjects/PRectangle;->p1top:F

    .line 78
    iget v0, p2, Landroid/graphics/PointF;->x:F

    iput v0, p0, Lorg/icepdf/core/pobjects/PRectangle;->p2left:F

    .line 79
    iget v0, p2, Landroid/graphics/PointF;->y:F

    iput v0, p0, Lorg/icepdf/core/pobjects/PRectangle;->p2top:F

    .line 80
    return-void
.end method

.method public constructor <init>(Ljava/util/Vector;)V
    .locals 6
    .param p1, "coordinates"    # Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 107
    invoke-direct {p0}, Landroid/graphics/RectF;-><init>()V

    .line 108
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/util/Vector;->size()I

    move-result v4

    const/4 v5, 0x4

    if-ge v4, v5, :cond_1

    .line 109
    :cond_0
    new-instance v4, Ljava/lang/IllegalArgumentException;

    invoke-direct {v4}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v4

    .line 110
    :cond_1
    const/4 v4, 0x0

    invoke-virtual {p1, v4}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Number;

    invoke-virtual {v4}, Ljava/lang/Number;->floatValue()F

    move-result v0

    .line 111
    .local v0, "x1":F
    const/4 v4, 0x1

    invoke-virtual {p1, v4}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Number;

    invoke-virtual {v4}, Ljava/lang/Number;->floatValue()F

    move-result v2

    .line 113
    .local v2, "y1":F
    const/4 v4, 0x2

    invoke-virtual {p1, v4}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Number;

    invoke-virtual {v4}, Ljava/lang/Number;->floatValue()F

    move-result v1

    .line 114
    .local v1, "x2":F
    const/4 v4, 0x3

    invoke-virtual {p1, v4}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Number;

    invoke-virtual {v4}, Ljava/lang/Number;->floatValue()F

    move-result v3

    .line 117
    .local v3, "y2":F
    iput v0, p0, Lorg/icepdf/core/pobjects/PRectangle;->p1left:F

    .line 118
    iput v2, p0, Lorg/icepdf/core/pobjects/PRectangle;->p1top:F

    .line 119
    iput v1, p0, Lorg/icepdf/core/pobjects/PRectangle;->p2left:F

    .line 120
    iput v3, p0, Lorg/icepdf/core/pobjects/PRectangle;->p2top:F

    .line 123
    invoke-direct {p0, v0, v2, v1, v3}, Lorg/icepdf/core/pobjects/PRectangle;->normalizeCoordinates(FFFF)V

    .line 124
    return-void
.end method

.method public constructor <init>(Lorg/icepdf/core/pobjects/PRectangle;)V
    .locals 1
    .param p1, "pIn"    # Lorg/icepdf/core/pobjects/PRectangle;

    .prologue
    .line 61
    invoke-direct {p0}, Landroid/graphics/RectF;-><init>()V

    .line 62
    iget v0, p1, Lorg/icepdf/core/pobjects/PRectangle;->p1left:F

    iput v0, p0, Lorg/icepdf/core/pobjects/PRectangle;->p1left:F

    .line 63
    iget v0, p1, Lorg/icepdf/core/pobjects/PRectangle;->p1top:F

    iput v0, p0, Lorg/icepdf/core/pobjects/PRectangle;->p1top:F

    .line 64
    iget v0, p1, Lorg/icepdf/core/pobjects/PRectangle;->p2left:F

    iput v0, p0, Lorg/icepdf/core/pobjects/PRectangle;->p2left:F

    .line 65
    iget v0, p1, Lorg/icepdf/core/pobjects/PRectangle;->p2top:F

    iput v0, p0, Lorg/icepdf/core/pobjects/PRectangle;->p2top:F

    .line 66
    iget v0, p1, Lorg/icepdf/core/pobjects/PRectangle;->left:F

    iput v0, p0, Lorg/icepdf/core/pobjects/PRectangle;->left:F

    .line 67
    iget v0, p1, Lorg/icepdf/core/pobjects/PRectangle;->right:F

    iput v0, p0, Lorg/icepdf/core/pobjects/PRectangle;->right:F

    .line 68
    iget v0, p1, Lorg/icepdf/core/pobjects/PRectangle;->top:F

    iput v0, p0, Lorg/icepdf/core/pobjects/PRectangle;->top:F

    .line 69
    iget v0, p1, Lorg/icepdf/core/pobjects/PRectangle;->bottom:F

    iput v0, p0, Lorg/icepdf/core/pobjects/PRectangle;->bottom:F

    .line 70
    return-void
.end method

.method public static getPRectangleVector(Landroid/graphics/RectF;)Ljava/util/Vector;
    .locals 10
    .param p0, "rect"    # Landroid/graphics/RectF;

    .prologue
    .line 189
    new-instance v4, Landroid/graphics/RectF;

    iget v6, p0, Landroid/graphics/RectF;->left:F

    iget v7, p0, Landroid/graphics/RectF;->top:F

    iget v8, p0, Landroid/graphics/RectF;->right:F

    iget v9, p0, Landroid/graphics/RectF;->bottom:F

    invoke-direct {v4, v6, v7, v8, v9}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 193
    .end local p0    # "rect":Landroid/graphics/RectF;
    .local v4, "rect":Landroid/graphics/RectF;
    iget v6, v4, Landroid/graphics/RectF;->left:F

    invoke-static {v6}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    .line 194
    .local v0, "p1left":Ljava/lang/Number;
    iget v6, v4, Landroid/graphics/RectF;->top:F

    invoke-static {v6}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    .line 195
    .local v3, "pltop":Ljava/lang/Number;
    iget v6, v4, Landroid/graphics/RectF;->left:F

    invoke-static {v6}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    .line 196
    .local v1, "p2left":Ljava/lang/Number;
    iget v6, v4, Landroid/graphics/RectF;->top:F

    invoke-static {v6}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    .line 198
    .local v2, "p2top":Ljava/lang/Number;
    new-instance v5, Ljava/util/Vector;

    const/4 v6, 0x4

    invoke-direct {v5, v6}, Ljava/util/Vector;-><init>(I)V

    .line 199
    .local v5, "rectVector":Ljava/util/Vector;, "Ljava/util/Vector<Ljava/lang/Number;>;"
    invoke-virtual {v5, v0}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 200
    invoke-virtual {v5, v3}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 201
    invoke-virtual {v5, v1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 202
    invoke-virtual {v5, v2}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 203
    return-object v5
.end method

.method private normalizeCoordinates(FFFF)V
    .locals 5
    .param p1, "x1"    # F
    .param p2, "y1"    # F
    .param p3, "x2"    # F
    .param p4, "y2"    # F

    .prologue
    .line 216
    move v2, p1

    .local v2, "x":F
    move v3, p2

    .line 217
    .local v3, "y":F
    sub-float v4, p3, p1

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v1

    .line 218
    .local v1, "w":F
    sub-float v4, p4, p2

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v0

    .line 220
    .local v0, "h":F
    cmpl-float v4, p1, p3

    if-lez v4, :cond_0

    .line 221
    move v2, p3

    .line 224
    :cond_0
    cmpg-float v4, p2, p4

    if-gez v4, :cond_1

    .line 225
    move v3, p4

    .line 228
    :cond_1
    iput v2, p0, Lorg/icepdf/core/pobjects/PRectangle;->left:F

    .line 229
    iput v3, p0, Lorg/icepdf/core/pobjects/PRectangle;->top:F

    .line 230
    iget v4, p0, Lorg/icepdf/core/pobjects/PRectangle;->left:F

    add-float/2addr v4, v1

    iput v4, p0, Lorg/icepdf/core/pobjects/PRectangle;->right:F

    .line 231
    iget v4, p0, Lorg/icepdf/core/pobjects/PRectangle;->top:F

    add-float/2addr v4, v0

    iput v4, p0, Lorg/icepdf/core/pobjects/PRectangle;->bottom:F

    .line 233
    return-void
.end method


# virtual methods
.method public createCartesianIntersection(Lorg/icepdf/core/pobjects/PRectangle;)Lorg/icepdf/core/pobjects/PRectangle;
    .locals 10
    .param p1, "src2"    # Lorg/icepdf/core/pobjects/PRectangle;

    .prologue
    const/4 v9, 0x0

    .line 136
    new-instance v0, Lorg/icepdf/core/pobjects/PRectangle;

    iget v5, p1, Lorg/icepdf/core/pobjects/PRectangle;->left:F

    iget v6, p1, Lorg/icepdf/core/pobjects/PRectangle;->top:F

    iget v7, p1, Lorg/icepdf/core/pobjects/PRectangle;->right:F

    iget v8, p1, Lorg/icepdf/core/pobjects/PRectangle;->bottom:F

    invoke-direct {v0, v5, v6, v7, v8}, Lorg/icepdf/core/pobjects/PRectangle;-><init>(FFFF)V

    .line 138
    .local v0, "rec":Lorg/icepdf/core/pobjects/PRectangle;
    iget v5, p0, Lorg/icepdf/core/pobjects/PRectangle;->left:F

    iget v6, v0, Lorg/icepdf/core/pobjects/PRectangle;->left:F

    cmpl-float v5, v5, v6

    if-lez v5, :cond_2

    iget v1, p0, Lorg/icepdf/core/pobjects/PRectangle;->left:F

    .line 139
    .local v1, "xLeft":F
    :goto_0
    iget v5, p0, Lorg/icepdf/core/pobjects/PRectangle;->right:F

    iget v6, v0, Lorg/icepdf/core/pobjects/PRectangle;->right:F

    cmpl-float v5, v5, v6

    if-lez v5, :cond_3

    iget v2, v0, Lorg/icepdf/core/pobjects/PRectangle;->right:F

    .line 142
    .local v2, "xRight":F
    :goto_1
    iget v5, p0, Lorg/icepdf/core/pobjects/PRectangle;->bottom:F

    iget v6, v0, Lorg/icepdf/core/pobjects/PRectangle;->bottom:F

    cmpg-float v5, v5, v6

    if-gez v5, :cond_4

    iget v3, v0, Lorg/icepdf/core/pobjects/PRectangle;->bottom:F

    .line 144
    .local v3, "yBottom":F
    :goto_2
    iget v5, p0, Lorg/icepdf/core/pobjects/PRectangle;->top:F

    iget v6, v0, Lorg/icepdf/core/pobjects/PRectangle;->top:F

    cmpl-float v5, v5, v6

    if-lez v5, :cond_5

    iget v4, v0, Lorg/icepdf/core/pobjects/PRectangle;->top:F

    .line 146
    .local v4, "yTop":F
    :goto_3
    iput v1, v0, Lorg/icepdf/core/pobjects/PRectangle;->left:F

    .line 147
    iput v4, v0, Lorg/icepdf/core/pobjects/PRectangle;->top:F

    .line 148
    iput v2, v0, Lorg/icepdf/core/pobjects/PRectangle;->right:F

    .line 149
    iput v3, v0, Lorg/icepdf/core/pobjects/PRectangle;->bottom:F

    .line 152
    iget v5, v0, Lorg/icepdf/core/pobjects/PRectangle;->right:F

    iget v6, v0, Lorg/icepdf/core/pobjects/PRectangle;->left:F

    sub-float/2addr v5, v6

    cmpg-float v5, v5, v9

    if-ltz v5, :cond_0

    iget v5, v0, Lorg/icepdf/core/pobjects/PRectangle;->bottom:F

    iget v6, v0, Lorg/icepdf/core/pobjects/PRectangle;->top:F

    sub-float/2addr v5, v6

    cmpg-float v5, v5, v9

    if-gez v5, :cond_1

    .line 153
    :cond_0
    iput v9, v0, Lorg/icepdf/core/pobjects/PRectangle;->bottom:F

    iput v9, v0, Lorg/icepdf/core/pobjects/PRectangle;->top:F

    iput v9, v0, Lorg/icepdf/core/pobjects/PRectangle;->right:F

    iput v9, v0, Lorg/icepdf/core/pobjects/PRectangle;->left:F

    .line 155
    :cond_1
    return-object v0

    .line 138
    .end local v1    # "xLeft":F
    .end local v2    # "xRight":F
    .end local v3    # "yBottom":F
    .end local v4    # "yTop":F
    :cond_2
    iget v1, v0, Lorg/icepdf/core/pobjects/PRectangle;->left:F

    goto :goto_0

    .line 139
    .restart local v1    # "xLeft":F
    :cond_3
    iget v2, p0, Lorg/icepdf/core/pobjects/PRectangle;->right:F

    goto :goto_1

    .line 142
    .restart local v2    # "xRight":F
    :cond_4
    iget v3, p0, Lorg/icepdf/core/pobjects/PRectangle;->bottom:F

    goto :goto_2

    .line 144
    .restart local v3    # "yBottom":F
    :cond_5
    iget v4, p0, Lorg/icepdf/core/pobjects/PRectangle;->top:F

    goto :goto_3
.end method

.method public getOriginalPoints()Landroid/graphics/RectF;
    .locals 6

    .prologue
    .line 166
    new-instance v0, Landroid/graphics/RectF;

    iget v1, p0, Lorg/icepdf/core/pobjects/PRectangle;->p1left:F

    iget v2, p0, Lorg/icepdf/core/pobjects/PRectangle;->p1top:F

    iget v3, p0, Lorg/icepdf/core/pobjects/PRectangle;->p1left:F

    iget v4, p0, Lorg/icepdf/core/pobjects/PRectangle;->p2left:F

    add-float/2addr v3, v4

    iget v4, p0, Lorg/icepdf/core/pobjects/PRectangle;->p1top:F

    iget v5, p0, Lorg/icepdf/core/pobjects/PRectangle;->p2top:F

    add-float/2addr v4, v5

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    return-object v0
.end method

.method public toJava2dCoordinates()Landroid/graphics/RectF;
    .locals 5

    .prologue
    .line 176
    new-instance v0, Landroid/graphics/RectF;

    iget v1, p0, Lorg/icepdf/core/pobjects/PRectangle;->left:F

    iget v2, p0, Lorg/icepdf/core/pobjects/PRectangle;->top:F

    iget v3, p0, Lorg/icepdf/core/pobjects/PRectangle;->right:F

    iget v4, p0, Lorg/icepdf/core/pobjects/PRectangle;->bottom:F

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    return-object v0
.end method
