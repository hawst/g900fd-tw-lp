.class public Lorg/icepdf/core/pobjects/security/EncryptionDictionary;
.super Lorg/icepdf/core/pobjects/Dictionary;
.source "EncryptionDictionary.java"


# instance fields
.field private cryptFilter:Lorg/icepdf/core/pobjects/security/CryptFilter;

.field private fileID:Ljava/util/Vector;

.field private isAuthenticatedOwnerPassword:Z

.field private isAuthenticatedUserPassword:Z


# direct methods
.method public constructor <init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;Ljava/util/Vector;)V
    .locals 1
    .param p1, "lib"    # Lorg/icepdf/core/util/Library;
    .param p2, "encryptionDictionary"    # Ljava/util/Hashtable;
    .param p3, "fileID"    # Ljava/util/Vector;

    .prologue
    .line 301
    invoke-direct {p0, p1, p2}, Lorg/icepdf/core/pobjects/Dictionary;-><init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V

    .line 282
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/icepdf/core/pobjects/security/EncryptionDictionary;->fileID:Ljava/util/Vector;

    .line 302
    iput-object p2, p0, Lorg/icepdf/core/pobjects/security/EncryptionDictionary;->entries:Ljava/util/Hashtable;

    .line 303
    iput-object p3, p0, Lorg/icepdf/core/pobjects/security/EncryptionDictionary;->fileID:Ljava/util/Vector;

    .line 304
    return-void
.end method


# virtual methods
.method public getBigO()Ljava/lang/String;
    .locals 4

    .prologue
    .line 396
    iget-object v1, p0, Lorg/icepdf/core/pobjects/security/EncryptionDictionary;->library:Lorg/icepdf/core/util/Library;

    iget-object v2, p0, Lorg/icepdf/core/pobjects/security/EncryptionDictionary;->entries:Ljava/util/Hashtable;

    const-string/jumbo v3, "O"

    invoke-virtual {v1, v2, v3}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 397
    .local v0, "tmp":Ljava/lang/Object;
    instance-of v1, v0, Lorg/icepdf/core/pobjects/StringObject;

    if-eqz v1, :cond_0

    .line 398
    check-cast v0, Lorg/icepdf/core/pobjects/StringObject;

    .end local v0    # "tmp":Ljava/lang/Object;
    invoke-interface {v0}, Lorg/icepdf/core/pobjects/StringObject;->getLiteralString()Ljava/lang/String;

    move-result-object v1

    .line 400
    :goto_0
    return-object v1

    .restart local v0    # "tmp":Ljava/lang/Object;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getBigOE()Ljava/lang/String;
    .locals 4

    .prologue
    .line 526
    iget-object v1, p0, Lorg/icepdf/core/pobjects/security/EncryptionDictionary;->library:Lorg/icepdf/core/util/Library;

    iget-object v2, p0, Lorg/icepdf/core/pobjects/security/EncryptionDictionary;->entries:Ljava/util/Hashtable;

    const-string/jumbo v3, "OE"

    invoke-virtual {v1, v2, v3}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 527
    .local v0, "tmp":Ljava/lang/Object;
    instance-of v1, v0, Lorg/icepdf/core/pobjects/StringObject;

    if-eqz v1, :cond_0

    .line 528
    iget-object v1, p0, Lorg/icepdf/core/pobjects/security/EncryptionDictionary;->library:Lorg/icepdf/core/util/Library;

    iget-object v2, p0, Lorg/icepdf/core/pobjects/security/EncryptionDictionary;->entries:Ljava/util/Hashtable;

    const-string/jumbo v3, "OE"

    invoke-virtual {v1, v2, v3}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/icepdf/core/pobjects/StringObject;

    invoke-interface {v1}, Lorg/icepdf/core/pobjects/StringObject;->getLiteralString()Ljava/lang/String;

    move-result-object v1

    .line 530
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getBigU()Ljava/lang/String;
    .locals 4

    .prologue
    .line 410
    iget-object v1, p0, Lorg/icepdf/core/pobjects/security/EncryptionDictionary;->library:Lorg/icepdf/core/util/Library;

    iget-object v2, p0, Lorg/icepdf/core/pobjects/security/EncryptionDictionary;->entries:Ljava/util/Hashtable;

    const-string/jumbo v3, "U"

    invoke-virtual {v1, v2, v3}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 411
    .local v0, "tmp":Ljava/lang/Object;
    instance-of v1, v0, Lorg/icepdf/core/pobjects/StringObject;

    if-eqz v1, :cond_0

    .line 412
    iget-object v1, p0, Lorg/icepdf/core/pobjects/security/EncryptionDictionary;->library:Lorg/icepdf/core/util/Library;

    iget-object v2, p0, Lorg/icepdf/core/pobjects/security/EncryptionDictionary;->entries:Ljava/util/Hashtable;

    const-string/jumbo v3, "U"

    invoke-virtual {v1, v2, v3}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/icepdf/core/pobjects/StringObject;

    invoke-interface {v1}, Lorg/icepdf/core/pobjects/StringObject;->getLiteralString()Ljava/lang/String;

    move-result-object v1

    .line 414
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getBigUE()Ljava/lang/String;
    .locals 4

    .prologue
    .line 541
    iget-object v1, p0, Lorg/icepdf/core/pobjects/security/EncryptionDictionary;->library:Lorg/icepdf/core/util/Library;

    iget-object v2, p0, Lorg/icepdf/core/pobjects/security/EncryptionDictionary;->entries:Ljava/util/Hashtable;

    const-string/jumbo v3, "UE"

    invoke-virtual {v1, v2, v3}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 542
    .local v0, "tmp":Ljava/lang/Object;
    instance-of v1, v0, Lorg/icepdf/core/pobjects/StringObject;

    if-eqz v1, :cond_0

    .line 543
    iget-object v1, p0, Lorg/icepdf/core/pobjects/security/EncryptionDictionary;->library:Lorg/icepdf/core/util/Library;

    iget-object v2, p0, Lorg/icepdf/core/pobjects/security/EncryptionDictionary;->entries:Ljava/util/Hashtable;

    const-string/jumbo v3, "UE"

    invoke-virtual {v1, v2, v3}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/icepdf/core/pobjects/StringObject;

    invoke-interface {v1}, Lorg/icepdf/core/pobjects/StringObject;->getLiteralString()Ljava/lang/String;

    move-result-object v1

    .line 545
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getCryptFilter()Lorg/icepdf/core/pobjects/security/CryptFilter;
    .locals 4

    .prologue
    .line 443
    iget-object v1, p0, Lorg/icepdf/core/pobjects/security/EncryptionDictionary;->cryptFilter:Lorg/icepdf/core/pobjects/security/CryptFilter;

    if-nez v1, :cond_0

    .line 444
    iget-object v1, p0, Lorg/icepdf/core/pobjects/security/EncryptionDictionary;->library:Lorg/icepdf/core/util/Library;

    iget-object v2, p0, Lorg/icepdf/core/pobjects/security/EncryptionDictionary;->entries:Ljava/util/Hashtable;

    const-string/jumbo v3, "CF"

    invoke-virtual {v1, v2, v3}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Hashtable;

    .line 445
    .local v0, "tmp":Ljava/util/Hashtable;
    if-eqz v0, :cond_0

    .line 446
    new-instance v1, Lorg/icepdf/core/pobjects/security/CryptFilter;

    iget-object v2, p0, Lorg/icepdf/core/pobjects/security/EncryptionDictionary;->library:Lorg/icepdf/core/util/Library;

    invoke-direct {v1, v2, v0}, Lorg/icepdf/core/pobjects/security/CryptFilter;-><init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V

    iput-object v1, p0, Lorg/icepdf/core/pobjects/security/EncryptionDictionary;->cryptFilter:Lorg/icepdf/core/pobjects/security/CryptFilter;

    .line 447
    iget-object v1, p0, Lorg/icepdf/core/pobjects/security/EncryptionDictionary;->cryptFilter:Lorg/icepdf/core/pobjects/security/CryptFilter;

    .line 450
    .end local v0    # "tmp":Ljava/util/Hashtable;
    :goto_0
    return-object v1

    :cond_0
    iget-object v1, p0, Lorg/icepdf/core/pobjects/security/EncryptionDictionary;->cryptFilter:Lorg/icepdf/core/pobjects/security/CryptFilter;

    goto :goto_0
.end method

.method public getEEF()Lorg/icepdf/core/pobjects/Name;
    .locals 4

    .prologue
    .line 511
    iget-object v1, p0, Lorg/icepdf/core/pobjects/security/EncryptionDictionary;->library:Lorg/icepdf/core/util/Library;

    iget-object v2, p0, Lorg/icepdf/core/pobjects/security/EncryptionDictionary;->entries:Ljava/util/Hashtable;

    const-string/jumbo v3, "EEF"

    invoke-virtual {v1, v2, v3}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 512
    .local v0, "tmp":Ljava/lang/Object;
    if-eqz v0, :cond_0

    instance-of v1, v0, Lorg/icepdf/core/pobjects/Name;

    if-eqz v1, :cond_0

    .line 513
    check-cast v0, Lorg/icepdf/core/pobjects/Name;

    .line 515
    .end local v0    # "tmp":Ljava/lang/Object;
    :goto_0
    return-object v0

    .restart local v0    # "tmp":Ljava/lang/Object;
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getEntries()Ljava/util/Hashtable;
    .locals 1

    .prologue
    .line 611
    iget-object v0, p0, Lorg/icepdf/core/pobjects/security/EncryptionDictionary;->entries:Ljava/util/Hashtable;

    return-object v0
.end method

.method public getFileID()Ljava/util/Vector;
    .locals 1

    .prologue
    .line 312
    iget-object v0, p0, Lorg/icepdf/core/pobjects/security/EncryptionDictionary;->fileID:Ljava/util/Vector;

    return-object v0
.end method

.method public getKeyLength()I
    .locals 5

    .prologue
    .line 369
    const/16 v1, 0x28

    .line 370
    .local v1, "length":I
    iget-object v2, p0, Lorg/icepdf/core/pobjects/security/EncryptionDictionary;->library:Lorg/icepdf/core/util/Library;

    iget-object v3, p0, Lorg/icepdf/core/pobjects/security/EncryptionDictionary;->entries:Ljava/util/Hashtable;

    const-string/jumbo v4, "Length"

    invoke-virtual {v2, v3, v4}, Lorg/icepdf/core/util/Library;->getInt(Ljava/util/Hashtable;Ljava/lang/String;)I

    move-result v0

    .line 371
    .local v0, "len":I
    if-eqz v0, :cond_0

    .line 372
    move v1, v0

    .line 374
    :cond_0
    return v1
.end method

.method public getPermissions()I
    .locals 3

    .prologue
    .line 425
    iget-object v0, p0, Lorg/icepdf/core/pobjects/security/EncryptionDictionary;->library:Lorg/icepdf/core/util/Library;

    iget-object v1, p0, Lorg/icepdf/core/pobjects/security/EncryptionDictionary;->entries:Ljava/util/Hashtable;

    const-string/jumbo v2, "P"

    invoke-virtual {v0, v1, v2}, Lorg/icepdf/core/util/Library;->getInt(Ljava/util/Hashtable;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public getPerms()Ljava/lang/String;
    .locals 4

    .prologue
    .line 556
    iget-object v1, p0, Lorg/icepdf/core/pobjects/security/EncryptionDictionary;->library:Lorg/icepdf/core/util/Library;

    iget-object v2, p0, Lorg/icepdf/core/pobjects/security/EncryptionDictionary;->entries:Ljava/util/Hashtable;

    const-string/jumbo v3, "Perms"

    invoke-virtual {v1, v2, v3}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 557
    .local v0, "tmp":Ljava/lang/Object;
    instance-of v1, v0, Lorg/icepdf/core/pobjects/StringObject;

    if-eqz v1, :cond_0

    .line 558
    iget-object v1, p0, Lorg/icepdf/core/pobjects/security/EncryptionDictionary;->library:Lorg/icepdf/core/util/Library;

    iget-object v2, p0, Lorg/icepdf/core/pobjects/security/EncryptionDictionary;->entries:Ljava/util/Hashtable;

    const-string/jumbo v3, "Perms"

    invoke-virtual {v1, v2, v3}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/icepdf/core/pobjects/StringObject;

    invoke-interface {v1}, Lorg/icepdf/core/pobjects/StringObject;->getLiteralString()Ljava/lang/String;

    move-result-object v1

    .line 560
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getPreferredSecurityHandlerName()Ljava/lang/String;
    .locals 3

    .prologue
    .line 325
    iget-object v0, p0, Lorg/icepdf/core/pobjects/security/EncryptionDictionary;->library:Lorg/icepdf/core/util/Library;

    iget-object v1, p0, Lorg/icepdf/core/pobjects/security/EncryptionDictionary;->entries:Ljava/util/Hashtable;

    const-string/jumbo v2, "Filter"

    invoke-virtual {v0, v1, v2}, Lorg/icepdf/core/util/Library;->getName(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPreferredSecurityHandlerSubName()Ljava/lang/String;
    .locals 3

    .prologue
    .line 334
    iget-object v0, p0, Lorg/icepdf/core/pobjects/security/EncryptionDictionary;->library:Lorg/icepdf/core/util/Library;

    iget-object v1, p0, Lorg/icepdf/core/pobjects/security/EncryptionDictionary;->entries:Ljava/util/Hashtable;

    const-string/jumbo v2, "SubFilter"

    invoke-virtual {v0, v1, v2}, Lorg/icepdf/core/util/Library;->getName(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getRevisionNumber()I
    .locals 3

    .prologue
    .line 387
    iget-object v0, p0, Lorg/icepdf/core/pobjects/security/EncryptionDictionary;->library:Lorg/icepdf/core/util/Library;

    iget-object v1, p0, Lorg/icepdf/core/pobjects/security/EncryptionDictionary;->entries:Ljava/util/Hashtable;

    const-string/jumbo v2, "R"

    invoke-virtual {v0, v1, v2}, Lorg/icepdf/core/util/Library;->getInt(Ljava/util/Hashtable;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public getStmF()Lorg/icepdf/core/pobjects/Name;
    .locals 4

    .prologue
    .line 468
    iget-object v1, p0, Lorg/icepdf/core/pobjects/security/EncryptionDictionary;->library:Lorg/icepdf/core/util/Library;

    iget-object v2, p0, Lorg/icepdf/core/pobjects/security/EncryptionDictionary;->entries:Ljava/util/Hashtable;

    const-string/jumbo v3, "StmF"

    invoke-virtual {v1, v2, v3}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 469
    .local v0, "tmp":Ljava/lang/Object;
    if-eqz v0, :cond_0

    instance-of v1, v0, Lorg/icepdf/core/pobjects/Name;

    if-eqz v1, :cond_0

    .line 470
    check-cast v0, Lorg/icepdf/core/pobjects/Name;

    .line 472
    .end local v0    # "tmp":Ljava/lang/Object;
    :goto_0
    return-object v0

    .restart local v0    # "tmp":Ljava/lang/Object;
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getStrF()Lorg/icepdf/core/pobjects/Name;
    .locals 4

    .prologue
    .line 487
    iget-object v1, p0, Lorg/icepdf/core/pobjects/security/EncryptionDictionary;->library:Lorg/icepdf/core/util/Library;

    iget-object v2, p0, Lorg/icepdf/core/pobjects/security/EncryptionDictionary;->entries:Ljava/util/Hashtable;

    const-string/jumbo v3, "StrF"

    invoke-virtual {v1, v2, v3}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 488
    .local v0, "tmp":Ljava/lang/Object;
    if-eqz v0, :cond_0

    instance-of v1, v0, Lorg/icepdf/core/pobjects/Name;

    if-eqz v1, :cond_0

    .line 489
    check-cast v0, Lorg/icepdf/core/pobjects/Name;

    .line 491
    .end local v0    # "tmp":Ljava/lang/Object;
    :goto_0
    return-object v0

    .restart local v0    # "tmp":Ljava/lang/Object;
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getValue(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "key"    # Ljava/lang/Object;

    .prologue
    .line 602
    iget-object v0, p0, Lorg/icepdf/core/pobjects/security/EncryptionDictionary;->entries:Ljava/util/Hashtable;

    invoke-virtual {v0, p1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getVersion()I
    .locals 3

    .prologue
    .line 360
    iget-object v0, p0, Lorg/icepdf/core/pobjects/security/EncryptionDictionary;->library:Lorg/icepdf/core/util/Library;

    iget-object v1, p0, Lorg/icepdf/core/pobjects/security/EncryptionDictionary;->entries:Ljava/util/Hashtable;

    const-string/jumbo v2, "V"

    invoke-virtual {v0, v1, v2}, Lorg/icepdf/core/util/Library;->getInt(Ljava/util/Hashtable;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method protected isAuthenticatedOwnerPassword()Z
    .locals 1

    .prologue
    .line 584
    iget-boolean v0, p0, Lorg/icepdf/core/pobjects/security/EncryptionDictionary;->isAuthenticatedOwnerPassword:Z

    return v0
.end method

.method protected isAuthenticatedUserPassword()Z
    .locals 1

    .prologue
    .line 576
    iget-boolean v0, p0, Lorg/icepdf/core/pobjects/security/EncryptionDictionary;->isAuthenticatedUserPassword:Z

    return v0
.end method

.method public isEncryptMetaData()Z
    .locals 3

    .prologue
    .line 572
    iget-object v0, p0, Lorg/icepdf/core/pobjects/security/EncryptionDictionary;->library:Lorg/icepdf/core/util/Library;

    iget-object v1, p0, Lorg/icepdf/core/pobjects/security/EncryptionDictionary;->entries:Ljava/util/Hashtable;

    const-string/jumbo v2, "EncryptMetadata"

    invoke-virtual {v0, v1, v2}, Lorg/icepdf/core/util/Library;->getBoolean(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method protected setAuthenticatedOwnerPassword(Z)V
    .locals 0
    .param p1, "authenticatedOwnerPassword"    # Z

    .prologue
    .line 588
    iput-boolean p1, p0, Lorg/icepdf/core/pobjects/security/EncryptionDictionary;->isAuthenticatedOwnerPassword:Z

    .line 589
    return-void
.end method

.method protected setAuthenticatedUserPassword(Z)V
    .locals 0
    .param p1, "authenticatedUserPassword"    # Z

    .prologue
    .line 580
    iput-boolean p1, p0, Lorg/icepdf/core/pobjects/security/EncryptionDictionary;->isAuthenticatedUserPassword:Z

    .line 581
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 615
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "Encryption Dictionary:  \n  fileID: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lorg/icepdf/core/pobjects/security/EncryptionDictionary;->getFileID()Ljava/util/Vector;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " \n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "  Filter: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lorg/icepdf/core/pobjects/security/EncryptionDictionary;->getPreferredSecurityHandlerName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " \n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "  SubFilter: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lorg/icepdf/core/pobjects/security/EncryptionDictionary;->getPreferredSecurityHandlerSubName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " \n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "  V: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lorg/icepdf/core/pobjects/security/EncryptionDictionary;->getVersion()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " \n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "  P: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lorg/icepdf/core/pobjects/security/EncryptionDictionary;->getPermissions()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " \n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "  Length:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lorg/icepdf/core/pobjects/security/EncryptionDictionary;->getKeyLength()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " \n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "  CF: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/icepdf/core/pobjects/security/EncryptionDictionary;->cryptFilter:Lorg/icepdf/core/pobjects/security/CryptFilter;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " \n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "  StmF: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lorg/icepdf/core/pobjects/security/EncryptionDictionary;->getStmF()Lorg/icepdf/core/pobjects/Name;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " \n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "  StrF: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lorg/icepdf/core/pobjects/security/EncryptionDictionary;->getStrF()Lorg/icepdf/core/pobjects/Name;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " \n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "  R: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lorg/icepdf/core/pobjects/security/EncryptionDictionary;->getRevisionNumber()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " \n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "  O: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lorg/icepdf/core/pobjects/security/EncryptionDictionary;->getBigO()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " \n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "  U: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lorg/icepdf/core/pobjects/security/EncryptionDictionary;->getBigU()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " \n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " UE: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lorg/icepdf/core/pobjects/security/EncryptionDictionary;->getBigUE()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " \n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " OE: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lorg/icepdf/core/pobjects/security/EncryptionDictionary;->getBigOE()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " \n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "  Recipients: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "not done yet"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " \n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
