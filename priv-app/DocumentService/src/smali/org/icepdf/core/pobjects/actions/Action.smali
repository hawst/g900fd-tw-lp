.class public Lorg/icepdf/core/pobjects/actions/Action;
.super Lorg/icepdf/core/pobjects/Dictionary;
.source "Action.java"


# static fields
.field public static final ACTION_TYPE:Lorg/icepdf/core/pobjects/Name;

.field public static final ACTION_TYPE_GOTO:Lorg/icepdf/core/pobjects/Name;

.field public static final ACTION_TYPE_GOTO_REMOTE:Lorg/icepdf/core/pobjects/Name;

.field public static final ACTION_TYPE_KEY:Lorg/icepdf/core/pobjects/Name;

.field public static final ACTION_TYPE_LAUNCH:Lorg/icepdf/core/pobjects/Name;

.field public static final ACTION_TYPE_URI:Lorg/icepdf/core/pobjects/Name;

.field public static final NEXT_KEY:Lorg/icepdf/core/pobjects/Name;


# instance fields
.field private type:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 39
    new-instance v0, Lorg/icepdf/core/pobjects/Name;

    const-string/jumbo v1, "Action"

    invoke-direct {v0, v1}, Lorg/icepdf/core/pobjects/Name;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/icepdf/core/pobjects/actions/Action;->ACTION_TYPE:Lorg/icepdf/core/pobjects/Name;

    .line 41
    new-instance v0, Lorg/icepdf/core/pobjects/Name;

    const-string/jumbo v1, "S"

    invoke-direct {v0, v1}, Lorg/icepdf/core/pobjects/Name;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/icepdf/core/pobjects/actions/Action;->ACTION_TYPE_KEY:Lorg/icepdf/core/pobjects/Name;

    .line 43
    new-instance v0, Lorg/icepdf/core/pobjects/Name;

    const-string/jumbo v1, "Next"

    invoke-direct {v0, v1}, Lorg/icepdf/core/pobjects/Name;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/icepdf/core/pobjects/actions/Action;->NEXT_KEY:Lorg/icepdf/core/pobjects/Name;

    .line 45
    new-instance v0, Lorg/icepdf/core/pobjects/Name;

    const-string/jumbo v1, "GoTo"

    invoke-direct {v0, v1}, Lorg/icepdf/core/pobjects/Name;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/icepdf/core/pobjects/actions/Action;->ACTION_TYPE_GOTO:Lorg/icepdf/core/pobjects/Name;

    .line 47
    new-instance v0, Lorg/icepdf/core/pobjects/Name;

    const-string/jumbo v1, "GoToR"

    invoke-direct {v0, v1}, Lorg/icepdf/core/pobjects/Name;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/icepdf/core/pobjects/actions/Action;->ACTION_TYPE_GOTO_REMOTE:Lorg/icepdf/core/pobjects/Name;

    .line 49
    new-instance v0, Lorg/icepdf/core/pobjects/Name;

    const-string/jumbo v1, "Launch"

    invoke-direct {v0, v1}, Lorg/icepdf/core/pobjects/Name;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/icepdf/core/pobjects/actions/Action;->ACTION_TYPE_LAUNCH:Lorg/icepdf/core/pobjects/Name;

    .line 51
    new-instance v0, Lorg/icepdf/core/pobjects/Name;

    const-string/jumbo v1, "URI"

    invoke-direct {v0, v1}, Lorg/icepdf/core/pobjects/Name;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/icepdf/core/pobjects/actions/Action;->ACTION_TYPE_URI:Lorg/icepdf/core/pobjects/Name;

    return-void
.end method

.method public constructor <init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V
    .locals 1
    .param p1, "l"    # Lorg/icepdf/core/util/Library;
    .param p2, "h"    # Ljava/util/Hashtable;

    .prologue
    .line 66
    invoke-direct {p0, p1, p2}, Lorg/icepdf/core/pobjects/Dictionary;-><init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V

    .line 67
    sget-object v0, Lorg/icepdf/core/pobjects/actions/Action;->ACTION_TYPE_KEY:Lorg/icepdf/core/pobjects/Name;

    invoke-virtual {p0, v0}, Lorg/icepdf/core/pobjects/actions/Action;->getObject(Lorg/icepdf/core/pobjects/Name;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/icepdf/core/pobjects/actions/Action;->type:Ljava/lang/String;

    .line 68
    return-void
.end method

.method public static buildAction(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)Lorg/icepdf/core/pobjects/actions/Action;
    .locals 2
    .param p0, "library"    # Lorg/icepdf/core/util/Library;
    .param p1, "hashTable"    # Ljava/util/Hashtable;

    .prologue
    .line 71
    sget-object v1, Lorg/icepdf/core/pobjects/actions/Action;->ACTION_TYPE_KEY:Lorg/icepdf/core/pobjects/Name;

    invoke-virtual {p1, v1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/icepdf/core/pobjects/Name;

    .line 72
    .local v0, "actionType":Lorg/icepdf/core/pobjects/Name;
    if-eqz v0, :cond_3

    .line 73
    sget-object v1, Lorg/icepdf/core/pobjects/actions/Action;->ACTION_TYPE_GOTO:Lorg/icepdf/core/pobjects/Name;

    invoke-virtual {v0, v1}, Lorg/icepdf/core/pobjects/Name;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 74
    new-instance v1, Lorg/icepdf/core/pobjects/actions/GoToAction;

    invoke-direct {v1, p0, p1}, Lorg/icepdf/core/pobjects/actions/GoToAction;-><init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V

    .line 83
    :goto_0
    return-object v1

    .line 75
    :cond_0
    sget-object v1, Lorg/icepdf/core/pobjects/actions/Action;->ACTION_TYPE_GOTO_REMOTE:Lorg/icepdf/core/pobjects/Name;

    invoke-virtual {v0, v1}, Lorg/icepdf/core/pobjects/Name;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 76
    new-instance v1, Lorg/icepdf/core/pobjects/actions/GoToRAction;

    invoke-direct {v1, p0, p1}, Lorg/icepdf/core/pobjects/actions/GoToRAction;-><init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V

    goto :goto_0

    .line 77
    :cond_1
    sget-object v1, Lorg/icepdf/core/pobjects/actions/Action;->ACTION_TYPE_LAUNCH:Lorg/icepdf/core/pobjects/Name;

    invoke-virtual {v0, v1}, Lorg/icepdf/core/pobjects/Name;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 78
    new-instance v1, Lorg/icepdf/core/pobjects/actions/LaunchAction;

    invoke-direct {v1, p0, p1}, Lorg/icepdf/core/pobjects/actions/LaunchAction;-><init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V

    goto :goto_0

    .line 79
    :cond_2
    sget-object v1, Lorg/icepdf/core/pobjects/actions/Action;->ACTION_TYPE_URI:Lorg/icepdf/core/pobjects/Name;

    invoke-virtual {v0, v1}, Lorg/icepdf/core/pobjects/Name;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 80
    new-instance v1, Lorg/icepdf/core/pobjects/actions/URIAction;

    invoke-direct {v1, p0, p1}, Lorg/icepdf/core/pobjects/actions/URIAction;-><init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V

    goto :goto_0

    .line 83
    :cond_3
    new-instance v1, Lorg/icepdf/core/pobjects/actions/Action;

    invoke-direct {v1, p0, p1}, Lorg/icepdf/core/pobjects/actions/Action;-><init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V

    goto :goto_0
.end method


# virtual methods
.method public getType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lorg/icepdf/core/pobjects/actions/Action;->type:Ljava/lang/String;

    return-object v0
.end method

.method public similar(Lorg/icepdf/core/pobjects/actions/Action;)Z
    .locals 2
    .param p1, "obj"    # Lorg/icepdf/core/pobjects/actions/Action;

    .prologue
    .line 100
    invoke-virtual {p0}, Lorg/icepdf/core/pobjects/actions/Action;->getPObjectReference()Lorg/icepdf/core/pobjects/Reference;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lorg/icepdf/core/pobjects/actions/Action;->getPObjectReference()Lorg/icepdf/core/pobjects/Reference;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 102
    invoke-virtual {p0}, Lorg/icepdf/core/pobjects/actions/Action;->getPObjectReference()Lorg/icepdf/core/pobjects/Reference;

    move-result-object v0

    invoke-virtual {p1}, Lorg/icepdf/core/pobjects/actions/Action;->getPObjectReference()Lorg/icepdf/core/pobjects/Reference;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/icepdf/core/pobjects/Reference;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 105
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lorg/icepdf/core/pobjects/actions/Action;->getType()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lorg/icepdf/core/pobjects/actions/Action;->getType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method
