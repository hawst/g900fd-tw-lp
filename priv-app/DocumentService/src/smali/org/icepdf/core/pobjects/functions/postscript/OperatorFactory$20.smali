.class final Lorg/icepdf/core/pobjects/functions/postscript/OperatorFactory$20;
.super Lorg/icepdf/core/pobjects/functions/postscript/Operator;
.source "OperatorFactory.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/icepdf/core/pobjects/functions/postscript/OperatorFactory;->getOperator([CII)Lorg/icepdf/core/pobjects/functions/postscript/Operator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>(I)V
    .locals 0
    .param p1, "x0"    # I

    .prologue
    .line 359
    invoke-direct {p0, p1}, Lorg/icepdf/core/pobjects/functions/postscript/Operator;-><init>(I)V

    return-void
.end method


# virtual methods
.method public eval(Ljava/util/Stack;)V
    .locals 3
    .param p1, "stack"    # Ljava/util/Stack;

    .prologue
    .line 362
    new-instance v1, Ljava/util/Stack;

    invoke-direct {v1}, Ljava/util/Stack;-><init>()V

    .line 364
    .local v1, "expressionStack":Ljava/util/Stack;
    invoke-virtual {p1}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v2

    instance-of v2, v2, Lorg/icepdf/core/pobjects/functions/postscript/Expression;

    if-eqz v2, :cond_1

    .line 366
    :goto_0
    invoke-virtual {p1}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v2

    instance-of v2, v2, Lorg/icepdf/core/pobjects/functions/postscript/Expression;

    if-nez v2, :cond_0

    .line 367
    invoke-virtual {p1}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 370
    :cond_0
    invoke-virtual {p1}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    .line 371
    invoke-virtual {p1}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 373
    .local v0, "bool":Z
    if-eqz v0, :cond_1

    .line 376
    :goto_1
    invoke-virtual {v1}, Ljava/util/Stack;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 377
    invoke-virtual {v1}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 381
    .end local v0    # "bool":Z
    :cond_1
    return-void
.end method
