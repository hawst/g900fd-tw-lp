.class Lorg/icepdf/core/pobjects/filters/CCITTFax$Code;
.super Ljava/lang/Object;
.source "CCITTFax.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/icepdf/core/pobjects/filters/CCITTFax;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Code"
.end annotation


# instance fields
.field private length:I

.field private tablePosition:I

.field private value:J


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 133
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 134
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lorg/icepdf/core/pobjects/filters/CCITTFax$Code;->value:J

    .line 135
    const/4 v0, 0x0

    iput v0, p0, Lorg/icepdf/core/pobjects/filters/CCITTFax$Code;->length:I

    .line 136
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;I)V
    .locals 6
    .param p1, "strValue"    # Ljava/lang/String;
    .param p2, "tablePosition"    # I

    .prologue
    const/4 v2, 0x0

    .line 138
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 139
    const-wide/16 v4, 0x0

    iput-wide v4, p0, Lorg/icepdf/core/pobjects/filters/CCITTFax$Code;->value:J

    .line 140
    iput v2, p0, Lorg/icepdf/core/pobjects/filters/CCITTFax$Code;->length:I

    .line 141
    iput p2, p0, Lorg/icepdf/core/pobjects/filters/CCITTFax$Code;->tablePosition:I

    .line 142
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 143
    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v1

    const/16 v3, 0x31

    if-ne v1, v3, :cond_0

    const/4 v1, 0x1

    :goto_1
    invoke-virtual {p0, v1}, Lorg/icepdf/core/pobjects/filters/CCITTFax$Code;->append(Z)V

    .line 142
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    move v1, v2

    .line 143
    goto :goto_1

    .line 144
    :cond_1
    return-void
.end method


# virtual methods
.method public final append(Z)V
    .locals 5
    .param p1, "bit"    # Z

    .prologue
    .line 150
    if-eqz p1, :cond_0

    .line 151
    iget v2, p0, Lorg/icepdf/core/pobjects/filters/CCITTFax$Code;->length:I

    const/16 v3, 0x3f

    if-gt v2, v3, :cond_0

    .line 152
    const-wide/16 v2, 0x1

    iget v4, p0, Lorg/icepdf/core/pobjects/filters/CCITTFax$Code;->length:I

    shl-long v0, v2, v4

    .line 153
    .local v0, "mask":J
    iget-wide v2, p0, Lorg/icepdf/core/pobjects/filters/CCITTFax$Code;->value:J

    or-long/2addr v2, v0

    iput-wide v2, p0, Lorg/icepdf/core/pobjects/filters/CCITTFax$Code;->value:J

    .line 156
    .end local v0    # "mask":J
    :cond_0
    iget v2, p0, Lorg/icepdf/core/pobjects/filters/CCITTFax$Code;->length:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lorg/icepdf/core/pobjects/filters/CCITTFax$Code;->length:I

    .line 157
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6
    .param p1, "ob"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 160
    instance-of v2, p1, Lorg/icepdf/core/pobjects/filters/CCITTFax$Code;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 161
    check-cast v0, Lorg/icepdf/core/pobjects/filters/CCITTFax$Code;

    .line 162
    .local v0, "c":Lorg/icepdf/core/pobjects/filters/CCITTFax$Code;
    iget-wide v2, p0, Lorg/icepdf/core/pobjects/filters/CCITTFax$Code;->value:J

    iget-wide v4, v0, Lorg/icepdf/core/pobjects/filters/CCITTFax$Code;->value:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    iget v2, p0, Lorg/icepdf/core/pobjects/filters/CCITTFax$Code;->length:I

    iget v3, v0, Lorg/icepdf/core/pobjects/filters/CCITTFax$Code;->length:I

    if-ne v2, v3, :cond_0

    const/4 v1, 0x1

    .line 164
    .end local v0    # "c":Lorg/icepdf/core/pobjects/filters/CCITTFax$Code;
    :cond_0
    return v1
.end method

.method public final getLength()I
    .locals 1

    .prologue
    .line 173
    iget v0, p0, Lorg/icepdf/core/pobjects/filters/CCITTFax$Code;->length:I

    return v0
.end method

.method public final getTablePosition()I
    .locals 1

    .prologue
    .line 177
    iget v0, p0, Lorg/icepdf/core/pobjects/filters/CCITTFax$Code;->tablePosition:I

    return v0
.end method

.method public final reset()V
    .locals 2

    .prologue
    .line 168
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lorg/icepdf/core/pobjects/filters/CCITTFax$Code;->value:J

    .line 169
    const/4 v0, 0x0

    iput v0, p0, Lorg/icepdf/core/pobjects/filters/CCITTFax$Code;->length:I

    .line 170
    return-void
.end method
