.class final Lorg/icepdf/core/pobjects/functions/postscript/OperatorFactory$37;
.super Lorg/icepdf/core/pobjects/functions/postscript/Operator;
.source "OperatorFactory.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/icepdf/core/pobjects/functions/postscript/OperatorFactory;->getOperator([CII)Lorg/icepdf/core/pobjects/functions/postscript/Operator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>(I)V
    .locals 0
    .param p1, "x0"    # I

    .prologue
    .line 665
    invoke-direct {p0, p1}, Lorg/icepdf/core/pobjects/functions/postscript/Operator;-><init>(I)V

    return-void
.end method


# virtual methods
.method public eval(Ljava/util/Stack;)V
    .locals 4
    .param p1, "stack"    # Ljava/util/Stack;

    .prologue
    .line 667
    invoke-virtual {p1}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .line 668
    .local v0, "num":F
    float-to-double v2, v0

    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 669
    return-void
.end method
