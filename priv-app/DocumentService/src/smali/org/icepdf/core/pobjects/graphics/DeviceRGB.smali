.class public Lorg/icepdf/core/pobjects/graphics/DeviceRGB;
.super Lorg/icepdf/core/pobjects/graphics/PColorSpace;
.source "DeviceRGB.java"


# direct methods
.method constructor <init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V
    .locals 0
    .param p1, "l"    # Lorg/icepdf/core/util/Library;
    .param p2, "h"    # Ljava/util/Hashtable;

    .prologue
    .line 31
    invoke-direct {p0, p1, p2}, Lorg/icepdf/core/pobjects/graphics/PColorSpace;-><init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V

    .line 32
    return-void
.end method

.method private validateColorRange(F)F
    .locals 3
    .param p1, "component"    # F

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v0, 0x0

    .line 42
    cmpg-float v2, p1, v0

    if-gez v2, :cond_1

    move p1, v0

    .line 47
    .end local p1    # "component":F
    :cond_0
    :goto_0
    return p1

    .line 44
    .restart local p1    # "component":F
    :cond_1
    cmpl-float v0, p1, v1

    if-lez v0, :cond_0

    move p1, v1

    .line 45
    goto :goto_0
.end method


# virtual methods
.method public getColor([F)Lorg/apache/poi/java/awt/Color;
    .locals 4
    .param p1, "colours"    # [F

    .prologue
    .line 59
    new-instance v0, Lorg/apache/poi/java/awt/Color;

    const/4 v1, 0x2

    aget v1, p1, v1

    invoke-direct {p0, v1}, Lorg/icepdf/core/pobjects/graphics/DeviceRGB;->validateColorRange(F)F

    move-result v1

    const/4 v2, 0x1

    aget v2, p1, v2

    invoke-direct {p0, v2}, Lorg/icepdf/core/pobjects/graphics/DeviceRGB;->validateColorRange(F)F

    move-result v2

    const/4 v3, 0x0

    aget v3, p1, v3

    invoke-direct {p0, v3}, Lorg/icepdf/core/pobjects/graphics/DeviceRGB;->validateColorRange(F)F

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/poi/java/awt/Color;-><init>(FFF)V

    return-object v0
.end method

.method public getNumComponents()I
    .locals 1

    .prologue
    .line 38
    const/4 v0, 0x3

    return v0
.end method
