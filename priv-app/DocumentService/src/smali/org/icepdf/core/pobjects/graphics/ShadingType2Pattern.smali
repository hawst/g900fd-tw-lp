.class public Lorg/icepdf/core/pobjects/graphics/ShadingType2Pattern;
.super Lorg/icepdf/core/pobjects/graphics/ShadingPattern;
.source "ShadingType2Pattern.java"


# static fields
.field private static final logger:Ljava/util/logging/Logger;


# instance fields
.field protected coords:Ljava/util/Vector;

.field protected domain:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field protected extend:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field protected function:[Lorg/icepdf/core/pobjects/functions/Function;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    const-class v0, Lorg/icepdf/core/pobjects/graphics/ShadingType2Pattern;

    invoke-virtual {v0}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Lorg/icepdf/core/pobjects/graphics/ShadingType2Pattern;->logger:Ljava/util/logging/Logger;

    return-void
.end method

.method public constructor <init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V
    .locals 0
    .param p1, "library"    # Lorg/icepdf/core/util/Library;
    .param p2, "entries"    # Ljava/util/Hashtable;

    .prologue
    .line 68
    invoke-direct {p0, p1, p2}, Lorg/icepdf/core/pobjects/graphics/ShadingPattern;-><init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V

    .line 69
    return-void
.end method

.method private calculateColour(Lorg/icepdf/core/pobjects/graphics/PColorSpace;Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;FF)Lorg/apache/poi/java/awt/Color;
    .locals 8
    .param p1, "colorSpace"    # Lorg/icepdf/core/pobjects/graphics/PColorSpace;
    .param p2, "xy"    # Landroid/graphics/PointF;
    .param p3, "point1"    # Landroid/graphics/PointF;
    .param p4, "point2"    # Landroid/graphics/PointF;
    .param p5, "t0"    # F
    .param p6, "t1"    # F

    .prologue
    .line 247
    invoke-direct {p0, p2, p3, p4}, Lorg/icepdf/core/pobjects/graphics/ShadingType2Pattern;->linearMapping(Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;)F

    move-result v5

    .line 248
    .local v5, "xPrime":F
    iget-object v6, p0, Lorg/icepdf/core/pobjects/graphics/ShadingType2Pattern;->extend:Ljava/util/Vector;

    invoke-direct {p0, v5, p5, p6, v6}, Lorg/icepdf/core/pobjects/graphics/ShadingType2Pattern;->parametrixValue(FFFLjava/util/Vector;)F

    move-result v4

    .line 250
    .local v4, "t":F
    const/4 v6, 0x1

    new-array v1, v6, [F

    .line 251
    .local v1, "input":[F
    const/4 v6, 0x0

    aput v4, v1, v6

    .line 253
    iget-object v6, p0, Lorg/icepdf/core/pobjects/graphics/ShadingType2Pattern;->function:[Lorg/icepdf/core/pobjects/functions/Function;

    if-eqz v6, :cond_5

    .line 256
    iget-object v6, p0, Lorg/icepdf/core/pobjects/graphics/ShadingType2Pattern;->function:[Lorg/icepdf/core/pobjects/functions/Function;

    array-length v2, v6

    .line 258
    .local v2, "length":I
    const/4 v6, 0x1

    if-ne v2, v6, :cond_2

    .line 259
    iget-object v6, p0, Lorg/icepdf/core/pobjects/graphics/ShadingType2Pattern;->function:[Lorg/icepdf/core/pobjects/functions/Function;

    const/4 v7, 0x0

    aget-object v6, v6, v7

    invoke-virtual {v6, v1}, Lorg/icepdf/core/pobjects/functions/Function;->calculate([F)[F

    move-result-object v3

    .line 270
    .local v3, "output":[F
    :cond_0
    if-eqz v3, :cond_4

    .line 271
    instance-of v6, p1, Lorg/icepdf/core/pobjects/graphics/DeviceN;

    if-nez v6, :cond_1

    .line 272
    invoke-static {v3}, Lorg/icepdf/core/pobjects/graphics/PColorSpace;->reverse([F)[F

    move-result-object v3

    .line 274
    :cond_1
    invoke-virtual {p1, v3}, Lorg/icepdf/core/pobjects/graphics/PColorSpace;->getColor([F)Lorg/apache/poi/java/awt/Color;

    move-result-object v6

    .line 281
    .end local v2    # "length":I
    .end local v3    # "output":[F
    :goto_0
    return-object v6

    .line 262
    .restart local v2    # "length":I
    :cond_2
    new-array v3, v2, [F

    .line 263
    .restart local v3    # "output":[F
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    if-ge v0, v2, :cond_0

    .line 265
    iget-object v6, p0, Lorg/icepdf/core/pobjects/graphics/ShadingType2Pattern;->function:[Lorg/icepdf/core/pobjects/functions/Function;

    aget-object v6, v6, v0

    invoke-virtual {v6, v1}, Lorg/icepdf/core/pobjects/functions/Function;->calculate([F)[F

    move-result-object v6

    if-eqz v6, :cond_3

    .line 266
    iget-object v6, p0, Lorg/icepdf/core/pobjects/graphics/ShadingType2Pattern;->function:[Lorg/icepdf/core/pobjects/functions/Function;

    aget-object v6, v6, v0

    invoke-virtual {v6, v1}, Lorg/icepdf/core/pobjects/functions/Function;->calculate([F)[F

    move-result-object v6

    const/4 v7, 0x0

    aget v6, v6, v7

    aput v6, v3, v0

    .line 263
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 276
    .end local v0    # "i":I
    :cond_4
    const/4 v6, 0x0

    goto :goto_0

    .line 280
    .end local v2    # "length":I
    .end local v3    # "output":[F
    :cond_5
    sget-object v6, Lorg/icepdf/core/pobjects/graphics/ShadingType2Pattern;->logger:Ljava/util/logging/Logger;

    const-string/jumbo v7, "Error processing Shading Type 2 Pattern."

    invoke-virtual {v6, v7}, Ljava/util/logging/Logger;->fine(Ljava/lang/String;)V

    .line 281
    const/4 v6, 0x0

    goto :goto_0
.end method

.method private linearMapping(Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;)F
    .locals 12
    .param p1, "xy"    # Landroid/graphics/PointF;
    .param p2, "point1"    # Landroid/graphics/PointF;
    .param p3, "point2"    # Landroid/graphics/PointF;

    .prologue
    .line 296
    iget v3, p1, Landroid/graphics/PointF;->x:F

    .line 297
    .local v3, "x":F
    iget v6, p1, Landroid/graphics/PointF;->y:F

    .line 298
    .local v6, "y":F
    iget v4, p2, Landroid/graphics/PointF;->x:F

    .line 299
    .local v4, "x0":F
    iget v7, p2, Landroid/graphics/PointF;->y:F

    .line 300
    .local v7, "y0":F
    iget v5, p3, Landroid/graphics/PointF;->x:F

    .line 301
    .local v5, "x1":F
    iget v8, p3, Landroid/graphics/PointF;->y:F

    .line 302
    .local v8, "y1":F
    sub-float v9, v5, v4

    sub-float v10, v3, v4

    mul-float/2addr v9, v10

    sub-float v10, v8, v7

    sub-float v11, v6, v7

    mul-float/2addr v10, v11

    add-float v2, v9, v10

    .line 303
    .local v2, "top":F
    sub-float v9, v5, v4

    sub-float v10, v5, v4

    mul-float/2addr v9, v10

    sub-float v10, v8, v7

    sub-float v11, v8, v7

    mul-float/2addr v10, v11

    add-float v0, v9, v10

    .line 306
    .local v0, "bottom":F
    div-float v9, v2, v0

    const/high16 v10, 0x42c80000    # 100.0f

    mul-float/2addr v9, v10

    float-to-int v1, v9

    .line 307
    .local v1, "map":I
    int-to-float v9, v1

    const/high16 v10, 0x42c80000    # 100.0f

    div-float/2addr v9, v10

    return v9
.end method

.method private parametrixValue(FFFLjava/util/Vector;)F
    .locals 1
    .param p1, "linearMapping"    # F
    .param p2, "t0"    # F
    .param p3, "t1"    # F
    .param p4, "extended"    # Ljava/util/Vector;

    .prologue
    .line 323
    const/4 v0, 0x0

    cmpg-float v0, p1, v0

    if-gez v0, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p4, v0}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 328
    .end local p2    # "t0":F
    :goto_0
    return p2

    .line 325
    .restart local p2    # "t0":F
    :cond_0
    const/high16 v0, 0x3f800000    # 1.0f

    cmpl-float v0, p1, v0

    if-lez v0, :cond_1

    const/4 v0, 0x1

    invoke-virtual {p4, v0}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    move p2, p3

    .line 326
    goto :goto_0

    .line 328
    :cond_1
    sub-float v0, p3, p2

    mul-float/2addr v0, p1

    add-float/2addr p2, v0

    goto :goto_0
.end method


# virtual methods
.method public getPaint()Landroid/graphics/Paint;
    .locals 1

    .prologue
    .line 334
    invoke-virtual {p0}, Lorg/icepdf/core/pobjects/graphics/ShadingType2Pattern;->init()V

    .line 336
    const/4 v0, 0x0

    return-object v0
.end method

.method public declared-synchronized init()V
    .locals 12

    .prologue
    .line 73
    monitor-enter p0

    :try_start_0
    iget-boolean v7, p0, Lorg/icepdf/core/pobjects/graphics/ShadingType2Pattern;->inited:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v7, :cond_0

    .line 157
    :goto_0
    monitor-exit p0

    return-void

    .line 78
    :cond_0
    :try_start_1
    iget-object v7, p0, Lorg/icepdf/core/pobjects/graphics/ShadingType2Pattern;->shading:Ljava/util/Hashtable;

    if-nez v7, :cond_1

    .line 79
    iget-object v7, p0, Lorg/icepdf/core/pobjects/graphics/ShadingType2Pattern;->library:Lorg/icepdf/core/util/Library;

    iget-object v8, p0, Lorg/icepdf/core/pobjects/graphics/ShadingType2Pattern;->entries:Ljava/util/Hashtable;

    const-string/jumbo v9, "Shading"

    invoke-virtual {v7, v8, v9}, Lorg/icepdf/core/util/Library;->getDictionary(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/util/Hashtable;

    move-result-object v7

    iput-object v7, p0, Lorg/icepdf/core/pobjects/graphics/ShadingType2Pattern;->shading:Ljava/util/Hashtable;

    .line 82
    :cond_1
    iget-object v7, p0, Lorg/icepdf/core/pobjects/graphics/ShadingType2Pattern;->library:Lorg/icepdf/core/util/Library;

    iget-object v8, p0, Lorg/icepdf/core/pobjects/graphics/ShadingType2Pattern;->shading:Ljava/util/Hashtable;

    const-string/jumbo v9, "ShadingType"

    invoke-virtual {v7, v8, v9}, Lorg/icepdf/core/util/Library;->getInt(Ljava/util/Hashtable;Ljava/lang/String;)I

    move-result v7

    iput v7, p0, Lorg/icepdf/core/pobjects/graphics/ShadingType2Pattern;->shadingType:I

    .line 83
    iget-object v7, p0, Lorg/icepdf/core/pobjects/graphics/ShadingType2Pattern;->library:Lorg/icepdf/core/util/Library;

    iget-object v8, p0, Lorg/icepdf/core/pobjects/graphics/ShadingType2Pattern;->shading:Ljava/util/Hashtable;

    const-string/jumbo v9, "BBox"

    invoke-virtual {v7, v8, v9}, Lorg/icepdf/core/util/Library;->getRectangle(Ljava/util/Hashtable;Ljava/lang/String;)Landroid/graphics/RectF;

    move-result-object v7

    iput-object v7, p0, Lorg/icepdf/core/pobjects/graphics/ShadingType2Pattern;->bBox:Landroid/graphics/RectF;

    .line 84
    iget-object v7, p0, Lorg/icepdf/core/pobjects/graphics/ShadingType2Pattern;->library:Lorg/icepdf/core/util/Library;

    iget-object v8, p0, Lorg/icepdf/core/pobjects/graphics/ShadingType2Pattern;->library:Lorg/icepdf/core/util/Library;

    iget-object v9, p0, Lorg/icepdf/core/pobjects/graphics/ShadingType2Pattern;->shading:Ljava/util/Hashtable;

    const-string/jumbo v10, "ColorSpace"

    invoke-virtual {v8, v9, v10}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v8

    invoke-static {v7, v8}, Lorg/icepdf/core/pobjects/graphics/PColorSpace;->getColorSpace(Lorg/icepdf/core/util/Library;Ljava/lang/Object;)Lorg/icepdf/core/pobjects/graphics/PColorSpace;

    move-result-object v7

    iput-object v7, p0, Lorg/icepdf/core/pobjects/graphics/ShadingType2Pattern;->colorSpace:Lorg/icepdf/core/pobjects/graphics/PColorSpace;

    .line 86
    iget-object v7, p0, Lorg/icepdf/core/pobjects/graphics/ShadingType2Pattern;->library:Lorg/icepdf/core/util/Library;

    iget-object v8, p0, Lorg/icepdf/core/pobjects/graphics/ShadingType2Pattern;->shading:Ljava/util/Hashtable;

    const-string/jumbo v9, "Background"

    invoke-virtual {v7, v8, v9}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    if-eqz v7, :cond_2

    iget-object v7, p0, Lorg/icepdf/core/pobjects/graphics/ShadingType2Pattern;->library:Lorg/icepdf/core/util/Library;

    iget-object v8, p0, Lorg/icepdf/core/pobjects/graphics/ShadingType2Pattern;->shading:Ljava/util/Hashtable;

    const-string/jumbo v9, "Background"

    invoke-virtual {v7, v8, v9}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    instance-of v7, v7, Ljava/util/Vector;

    if-eqz v7, :cond_2

    .line 88
    iget-object v7, p0, Lorg/icepdf/core/pobjects/graphics/ShadingType2Pattern;->library:Lorg/icepdf/core/util/Library;

    iget-object v8, p0, Lorg/icepdf/core/pobjects/graphics/ShadingType2Pattern;->shading:Ljava/util/Hashtable;

    const-string/jumbo v9, "Background"

    invoke-virtual {v7, v8, v9}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/util/Vector;

    iput-object v7, p0, Lorg/icepdf/core/pobjects/graphics/ShadingType2Pattern;->background:Ljava/util/Vector;

    .line 90
    :cond_2
    iget-object v7, p0, Lorg/icepdf/core/pobjects/graphics/ShadingType2Pattern;->library:Lorg/icepdf/core/util/Library;

    iget-object v8, p0, Lorg/icepdf/core/pobjects/graphics/ShadingType2Pattern;->shading:Ljava/util/Hashtable;

    const-string/jumbo v9, "AntiAlias"

    invoke-virtual {v7, v8, v9}, Lorg/icepdf/core/util/Library;->getBoolean(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v7

    iput-boolean v7, p0, Lorg/icepdf/core/pobjects/graphics/ShadingType2Pattern;->antiAlias:Z

    .line 93
    iget-object v7, p0, Lorg/icepdf/core/pobjects/graphics/ShadingType2Pattern;->library:Lorg/icepdf/core/util/Library;

    iget-object v8, p0, Lorg/icepdf/core/pobjects/graphics/ShadingType2Pattern;->shading:Ljava/util/Hashtable;

    const-string/jumbo v9, "Domain"

    invoke-virtual {v7, v8, v9}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    instance-of v7, v7, Ljava/util/Vector;

    if-eqz v7, :cond_6

    .line 94
    iget-object v7, p0, Lorg/icepdf/core/pobjects/graphics/ShadingType2Pattern;->library:Lorg/icepdf/core/util/Library;

    iget-object v8, p0, Lorg/icepdf/core/pobjects/graphics/ShadingType2Pattern;->shading:Ljava/util/Hashtable;

    const-string/jumbo v9, "Domain"

    invoke-virtual {v7, v8, v9}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/util/Vector;

    iput-object v7, p0, Lorg/icepdf/core/pobjects/graphics/ShadingType2Pattern;->domain:Ljava/util/Vector;

    .line 101
    :goto_1
    iget-object v7, p0, Lorg/icepdf/core/pobjects/graphics/ShadingType2Pattern;->library:Lorg/icepdf/core/util/Library;

    iget-object v8, p0, Lorg/icepdf/core/pobjects/graphics/ShadingType2Pattern;->shading:Ljava/util/Hashtable;

    const-string/jumbo v9, "Coords"

    invoke-virtual {v7, v8, v9}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    instance-of v7, v7, Ljava/util/Vector;

    if-eqz v7, :cond_3

    .line 102
    iget-object v7, p0, Lorg/icepdf/core/pobjects/graphics/ShadingType2Pattern;->library:Lorg/icepdf/core/util/Library;

    iget-object v8, p0, Lorg/icepdf/core/pobjects/graphics/ShadingType2Pattern;->shading:Ljava/util/Hashtable;

    const-string/jumbo v9, "Coords"

    invoke-virtual {v7, v8, v9}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/util/Vector;

    iput-object v7, p0, Lorg/icepdf/core/pobjects/graphics/ShadingType2Pattern;->coords:Ljava/util/Vector;

    .line 104
    :cond_3
    iget-object v7, p0, Lorg/icepdf/core/pobjects/graphics/ShadingType2Pattern;->library:Lorg/icepdf/core/util/Library;

    iget-object v8, p0, Lorg/icepdf/core/pobjects/graphics/ShadingType2Pattern;->shading:Ljava/util/Hashtable;

    const-string/jumbo v9, "Extend"

    invoke-virtual {v7, v8, v9}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    instance-of v7, v7, Ljava/util/Vector;

    if-eqz v7, :cond_7

    .line 105
    iget-object v7, p0, Lorg/icepdf/core/pobjects/graphics/ShadingType2Pattern;->library:Lorg/icepdf/core/util/Library;

    iget-object v8, p0, Lorg/icepdf/core/pobjects/graphics/ShadingType2Pattern;->shading:Ljava/util/Hashtable;

    const-string/jumbo v9, "Extend"

    invoke-virtual {v7, v8, v9}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/util/Vector;

    iput-object v7, p0, Lorg/icepdf/core/pobjects/graphics/ShadingType2Pattern;->extend:Ljava/util/Vector;

    .line 111
    :goto_2
    iget-object v7, p0, Lorg/icepdf/core/pobjects/graphics/ShadingType2Pattern;->library:Lorg/icepdf/core/util/Library;

    iget-object v8, p0, Lorg/icepdf/core/pobjects/graphics/ShadingType2Pattern;->shading:Ljava/util/Hashtable;

    const-string/jumbo v9, "Function"

    invoke-virtual {v7, v8, v9}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    .line 112
    .local v6, "tmp":Ljava/lang/Object;
    if-eqz v6, :cond_4

    .line 113
    instance-of v7, v6, Ljava/util/Vector;

    if-nez v7, :cond_8

    .line 114
    const/4 v7, 0x1

    new-array v7, v7, [Lorg/icepdf/core/pobjects/functions/Function;

    const/4 v8, 0x0

    iget-object v9, p0, Lorg/icepdf/core/pobjects/graphics/ShadingType2Pattern;->library:Lorg/icepdf/core/util/Library;

    invoke-static {v9, v6}, Lorg/icepdf/core/pobjects/functions/Function;->getFunction(Lorg/icepdf/core/util/Library;Ljava/lang/Object;)Lorg/icepdf/core/pobjects/functions/Function;

    move-result-object v9

    aput-object v9, v7, v8

    iput-object v7, p0, Lorg/icepdf/core/pobjects/graphics/ShadingType2Pattern;->function:[Lorg/icepdf/core/pobjects/functions/Function;

    .line 130
    :cond_4
    new-instance v5, Landroid/graphics/PointF;

    iget-object v7, p0, Lorg/icepdf/core/pobjects/graphics/ShadingType2Pattern;->coords:Ljava/util/Vector;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Number;

    invoke-virtual {v7}, Ljava/lang/Number;->floatValue()F

    move-result v8

    iget-object v7, p0, Lorg/icepdf/core/pobjects/graphics/ShadingType2Pattern;->coords:Ljava/util/Vector;

    const/4 v9, 0x1

    invoke-virtual {v7, v9}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Number;

    invoke-virtual {v7}, Ljava/lang/Number;->floatValue()F

    move-result v7

    invoke-direct {v5, v8, v7}, Landroid/graphics/PointF;-><init>(FF)V

    .line 134
    .local v5, "startPoint":Landroid/graphics/PointF;
    new-instance v2, Landroid/graphics/PointF;

    iget-object v7, p0, Lorg/icepdf/core/pobjects/graphics/ShadingType2Pattern;->coords:Ljava/util/Vector;

    const/4 v8, 0x2

    invoke-virtual {v7, v8}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Number;

    invoke-virtual {v7}, Ljava/lang/Number;->floatValue()F

    move-result v8

    iget-object v7, p0, Lorg/icepdf/core/pobjects/graphics/ShadingType2Pattern;->coords:Ljava/util/Vector;

    const/4 v9, 0x3

    invoke-virtual {v7, v9}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Number;

    invoke-virtual {v7}, Ljava/lang/Number;->floatValue()F

    move-result v7

    invoke-direct {v2, v8, v7}, Landroid/graphics/PointF;-><init>(FF)V

    .line 140
    .local v2, "endPoint":Landroid/graphics/PointF;
    invoke-virtual {v5, v2}, Landroid/graphics/PointF;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 141
    iget v7, v2, Landroid/graphics/PointF;->x:F

    const/high16 v8, 0x3f800000    # 1.0f

    add-float/2addr v7, v8

    iput v7, v2, Landroid/graphics/PointF;->x:F

    .line 156
    :cond_5
    const/4 v7, 0x1

    iput-boolean v7, p0, Lorg/icepdf/core/pobjects/graphics/ShadingType2Pattern;->inited:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    .line 73
    .end local v2    # "endPoint":Landroid/graphics/PointF;
    .end local v5    # "startPoint":Landroid/graphics/PointF;
    .end local v6    # "tmp":Ljava/lang/Object;
    :catchall_0
    move-exception v7

    monitor-exit p0

    throw v7

    .line 96
    :cond_6
    :try_start_2
    new-instance v7, Ljava/util/Vector;

    const/4 v8, 0x2

    invoke-direct {v7, v8}, Ljava/util/Vector;-><init>(I)V

    iput-object v7, p0, Lorg/icepdf/core/pobjects/graphics/ShadingType2Pattern;->domain:Ljava/util/Vector;

    .line 97
    iget-object v7, p0, Lorg/icepdf/core/pobjects/graphics/ShadingType2Pattern;->domain:Ljava/util/Vector;

    new-instance v8, Ljava/lang/Float;

    const-wide/16 v10, 0x0

    invoke-direct {v8, v10, v11}, Ljava/lang/Float;-><init>(D)V

    invoke-virtual {v7, v8}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 98
    iget-object v7, p0, Lorg/icepdf/core/pobjects/graphics/ShadingType2Pattern;->domain:Ljava/util/Vector;

    new-instance v8, Ljava/lang/Float;

    const-wide/high16 v10, 0x3ff0000000000000L    # 1.0

    invoke-direct {v8, v10, v11}, Ljava/lang/Float;-><init>(D)V

    invoke-virtual {v7, v8}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 107
    :cond_7
    new-instance v7, Ljava/util/Vector;

    const/4 v8, 0x2

    invoke-direct {v7, v8}, Ljava/util/Vector;-><init>(I)V

    iput-object v7, p0, Lorg/icepdf/core/pobjects/graphics/ShadingType2Pattern;->extend:Ljava/util/Vector;

    .line 108
    iget-object v7, p0, Lorg/icepdf/core/pobjects/graphics/ShadingType2Pattern;->extend:Ljava/util/Vector;

    const/4 v8, 0x0

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 109
    iget-object v7, p0, Lorg/icepdf/core/pobjects/graphics/ShadingType2Pattern;->extend:Ljava/util/Vector;

    const/4 v8, 0x0

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    .line 117
    .restart local v6    # "tmp":Ljava/lang/Object;
    :cond_8
    move-object v0, v6

    check-cast v0, Ljava/util/Vector;

    move-object v3, v0

    .line 118
    .local v3, "functionTemp":Ljava/util/Vector;
    invoke-virtual {v3}, Ljava/util/Vector;->size()I

    move-result v7

    new-array v7, v7, [Lorg/icepdf/core/pobjects/functions/Function;

    iput-object v7, p0, Lorg/icepdf/core/pobjects/graphics/ShadingType2Pattern;->function:[Lorg/icepdf/core/pobjects/functions/Function;

    .line 119
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_3
    invoke-virtual {v3}, Ljava/util/Vector;->size()I

    move-result v7

    if-ge v4, v7, :cond_4

    .line 120
    iget-object v7, p0, Lorg/icepdf/core/pobjects/graphics/ShadingType2Pattern;->function:[Lorg/icepdf/core/pobjects/functions/Function;

    iget-object v8, p0, Lorg/icepdf/core/pobjects/graphics/ShadingType2Pattern;->library:Lorg/icepdf/core/util/Library;

    invoke-virtual {v3, v4}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v9

    invoke-static {v8, v9}, Lorg/icepdf/core/pobjects/functions/Function;->getFunction(Lorg/icepdf/core/util/Library;Ljava/lang/Object;)Lorg/icepdf/core/pobjects/functions/Function;

    move-result-object v8

    aput-object v8, v7, v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 119
    add-int/lit8 v4, v4, 0x1

    goto :goto_3
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 340
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-super {p0}, Lorg/icepdf/core/pobjects/graphics/ShadingPattern;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "\n                    domain: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/icepdf/core/pobjects/graphics/ShadingType2Pattern;->domain:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "\n                    coords: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/icepdf/core/pobjects/graphics/ShadingType2Pattern;->coords:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "\n                    extend: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/icepdf/core/pobjects/graphics/ShadingType2Pattern;->extend:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "\n                 function: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/icepdf/core/pobjects/graphics/ShadingType2Pattern;->function:[Lorg/icepdf/core/pobjects/functions/Function;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
