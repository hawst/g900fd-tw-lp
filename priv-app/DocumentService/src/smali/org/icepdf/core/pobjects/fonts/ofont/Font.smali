.class public Lorg/icepdf/core/pobjects/fonts/ofont/Font;
.super Lorg/icepdf/core/pobjects/fonts/Font;
.source "Font.java"


# static fields
.field private static final DEFAULT_FONT_SIZE:F = 12.0f

.field public static Default14Fonts:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field static final type1Diff:[[Ljava/lang/String;


# instance fields
.field private cMap:[C

.field private cidWidths:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field protected defaultfont:Lorg/icepdf/core/pobjects/fonts/DefaultFonts;

.field private encoding:Lorg/icepdf/core/pobjects/fonts/ofont/Encoding;

.field private encodingName:Ljava/lang/String;

.field private originalFontName:Ljava/lang/String;

.field protected style:I

.field private toUnicodeCMap:Lorg/icepdf/core/pobjects/fonts/ofont/CMap;

.field private widths:Ljava/util/Vector;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 69
    const/16 v0, 0x27

    new-array v0, v0, [[Ljava/lang/String;

    new-array v1, v7, [Ljava/lang/String;

    const-string/jumbo v2, "Bookman-Demi"

    aput-object v2, v1, v4

    const-string/jumbo v2, "URWBookmanL-DemiBold"

    aput-object v2, v1, v5

    const-string/jumbo v2, "Arial"

    aput-object v2, v1, v6

    aput-object v1, v0, v4

    new-array v1, v7, [Ljava/lang/String;

    const-string/jumbo v2, "Bookman-DemiItalic"

    aput-object v2, v1, v4

    const-string/jumbo v2, "URWBookmanL-DemiBoldItal"

    aput-object v2, v1, v5

    const-string/jumbo v2, "Arial"

    aput-object v2, v1, v6

    aput-object v1, v0, v5

    new-array v1, v7, [Ljava/lang/String;

    const-string/jumbo v2, "Bookman-Light"

    aput-object v2, v1, v4

    const-string/jumbo v2, "URWBookmanL-Ligh"

    aput-object v2, v1, v5

    const-string/jumbo v2, "Arial"

    aput-object v2, v1, v6

    aput-object v1, v0, v6

    new-array v1, v7, [Ljava/lang/String;

    const-string/jumbo v2, "Bookman-LightItalic"

    aput-object v2, v1, v4

    const-string/jumbo v2, "URWBookmanL-LighItal"

    aput-object v2, v1, v5

    const-string/jumbo v2, "Arial"

    aput-object v2, v1, v6

    aput-object v1, v0, v7

    new-array v1, v7, [Ljava/lang/String;

    const-string/jumbo v2, "Courier"

    aput-object v2, v1, v4

    const-string/jumbo v2, "Nimbus Mono L Regular"

    aput-object v2, v1, v5

    const-string/jumbo v2, "Nimbus Mono L"

    aput-object v2, v1, v6

    aput-object v1, v0, v8

    const/4 v1, 0x5

    new-array v2, v7, [Ljava/lang/String;

    const-string/jumbo v3, "Courier-Oblique"

    aput-object v3, v2, v4

    const-string/jumbo v3, "Nimbus Mono L Regular Oblique"

    aput-object v3, v2, v5

    const-string/jumbo v3, "Nimbus Mono L"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/4 v1, 0x6

    new-array v2, v7, [Ljava/lang/String;

    const-string/jumbo v3, "Courier-Bold"

    aput-object v3, v2, v4

    const-string/jumbo v3, "Nimbus Mono L Bold"

    aput-object v3, v2, v5

    const-string/jumbo v3, "Nimbus Mono L"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/4 v1, 0x7

    new-array v2, v7, [Ljava/lang/String;

    const-string/jumbo v3, "Courier-BoldOblique"

    aput-object v3, v2, v4

    const-string/jumbo v3, "Nimbus Mono L Bold Oblique"

    aput-object v3, v2, v5

    const-string/jumbo v3, "Nimbus Mono L"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x8

    new-array v2, v7, [Ljava/lang/String;

    const-string/jumbo v3, "AvantGarde-Book"

    aput-object v3, v2, v4

    const-string/jumbo v3, "URWGothicL-Book"

    aput-object v3, v2, v5

    const-string/jumbo v3, "Arial"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x9

    new-array v2, v7, [Ljava/lang/String;

    const-string/jumbo v3, "AvantGarde-BookOblique"

    aput-object v3, v2, v4

    const-string/jumbo v3, "URWGothicL-BookObli"

    aput-object v3, v2, v5

    const-string/jumbo v3, "Arial"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0xa

    new-array v2, v7, [Ljava/lang/String;

    const-string/jumbo v3, "AvantGarde-Demi"

    aput-object v3, v2, v4

    const-string/jumbo v3, "URWGothicL-Demi"

    aput-object v3, v2, v5

    const-string/jumbo v3, "Arial"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0xb

    new-array v2, v7, [Ljava/lang/String;

    const-string/jumbo v3, "AvantGarde-DemiOblique"

    aput-object v3, v2, v4

    const-string/jumbo v3, "URWGothicL-DemiObli"

    aput-object v3, v2, v5

    const-string/jumbo v3, "Arial"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0xc

    new-array v2, v7, [Ljava/lang/String;

    const-string/jumbo v3, "Helvetica"

    aput-object v3, v2, v4

    const-string/jumbo v3, "Nimbus Sans L Regular"

    aput-object v3, v2, v5

    const-string/jumbo v3, "Nimbus Sans L"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0xd

    new-array v2, v7, [Ljava/lang/String;

    const-string/jumbo v3, "Helvetica-Oblique"

    aput-object v3, v2, v4

    const-string/jumbo v3, "Nimbus Sans L Regular Italic"

    aput-object v3, v2, v5

    const-string/jumbo v3, "Nimbus Sans L"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0xe

    new-array v2, v7, [Ljava/lang/String;

    const-string/jumbo v3, "Helvetica-Bold"

    aput-object v3, v2, v4

    const-string/jumbo v3, "Nimbus Sans L Bold"

    aput-object v3, v2, v5

    const-string/jumbo v3, "Nimbus Sans L"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0xf

    new-array v2, v7, [Ljava/lang/String;

    const-string/jumbo v3, "Helvetica-BoldOblique"

    aput-object v3, v2, v4

    const-string/jumbo v3, "Nimbus Sans L Bold Italic"

    aput-object v3, v2, v5

    const-string/jumbo v3, "Nimbus Sans L"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x10

    new-array v2, v7, [Ljava/lang/String;

    const-string/jumbo v3, "Helvetica-Narrow"

    aput-object v3, v2, v4

    const-string/jumbo v3, "Nimbus Sans L Regular Condensed"

    aput-object v3, v2, v5

    const-string/jumbo v3, "Nimbus Sans L"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x11

    new-array v2, v7, [Ljava/lang/String;

    const-string/jumbo v3, "Helvetica-Narrow-Oblique"

    aput-object v3, v2, v4

    const-string/jumbo v3, "Nimbus Sans L Regular Condensed Italic"

    aput-object v3, v2, v5

    const-string/jumbo v3, "Nimbus Sans L"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x12

    new-array v2, v7, [Ljava/lang/String;

    const-string/jumbo v3, "Helvetica-Narrow-Bold"

    aput-object v3, v2, v4

    const-string/jumbo v3, "Nimbus Sans L Bold Condensed"

    aput-object v3, v2, v5

    const-string/jumbo v3, "Nimbus Sans L"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x13

    new-array v2, v7, [Ljava/lang/String;

    const-string/jumbo v3, "Helvetica-Narrow-BoldOblique"

    aput-object v3, v2, v4

    const-string/jumbo v3, "Nimbus Sans L Bold Condensed Italic"

    aput-object v3, v2, v5

    const-string/jumbo v3, "Nimbus Sans L"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x14

    new-array v2, v7, [Ljava/lang/String;

    const-string/jumbo v3, "Helvetica-Condensed"

    aput-object v3, v2, v4

    const-string/jumbo v3, "Nimbus Sans L Regular Condensed"

    aput-object v3, v2, v5

    const-string/jumbo v3, "Nimbus Sans L"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x15

    new-array v2, v7, [Ljava/lang/String;

    const-string/jumbo v3, "Helvetica-Condensed-Oblique"

    aput-object v3, v2, v4

    const-string/jumbo v3, "Nimbus Sans L Regular Condensed Italic"

    aput-object v3, v2, v5

    const-string/jumbo v3, "Nimbus Sans L"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x16

    new-array v2, v7, [Ljava/lang/String;

    const-string/jumbo v3, "Helvetica-Condensed-Bold"

    aput-object v3, v2, v4

    const-string/jumbo v3, "Nimbus Sans L Bold Condensed"

    aput-object v3, v2, v5

    const-string/jumbo v3, "Nimbus Sans L"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x17

    new-array v2, v7, [Ljava/lang/String;

    const-string/jumbo v3, "Helvetica-Condensed-BoldOblique"

    aput-object v3, v2, v4

    const-string/jumbo v3, "Nimbus Sans L Bold Condensed Italic"

    aput-object v3, v2, v5

    const-string/jumbo v3, "Nimbus Sans L"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x18

    new-array v2, v7, [Ljava/lang/String;

    const-string/jumbo v3, "Palatino-Roman"

    aput-object v3, v2, v4

    const-string/jumbo v3, "URWPalladioL-Roma"

    aput-object v3, v2, v5

    const-string/jumbo v3, "Arial"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x19

    new-array v2, v7, [Ljava/lang/String;

    const-string/jumbo v3, "Palatino-Italic"

    aput-object v3, v2, v4

    const-string/jumbo v3, "URWPalladioL-Ital"

    aput-object v3, v2, v5

    const-string/jumbo v3, "Arial"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    new-array v2, v7, [Ljava/lang/String;

    const-string/jumbo v3, "Palatino-Bold"

    aput-object v3, v2, v4

    const-string/jumbo v3, "URWPalladioL-Bold"

    aput-object v3, v2, v5

    const-string/jumbo v3, "Arial"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    new-array v2, v7, [Ljava/lang/String;

    const-string/jumbo v3, "Palatino-BoldItalic"

    aput-object v3, v2, v4

    const-string/jumbo v3, "URWPalladioL-BoldItal"

    aput-object v3, v2, v5

    const-string/jumbo v3, "Arial"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    new-array v2, v7, [Ljava/lang/String;

    const-string/jumbo v3, "NewCenturySchlbk-Roman"

    aput-object v3, v2, v4

    const-string/jumbo v3, "CenturySchL-Roma"

    aput-object v3, v2, v5

    const-string/jumbo v3, "Arial"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    new-array v2, v7, [Ljava/lang/String;

    const-string/jumbo v3, "NewCenturySchlbk-Italic"

    aput-object v3, v2, v4

    const-string/jumbo v3, "CenturySchL-Ital"

    aput-object v3, v2, v5

    const-string/jumbo v3, "Arial"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    new-array v2, v7, [Ljava/lang/String;

    const-string/jumbo v3, "NewCenturySchlbk-Bold"

    aput-object v3, v2, v4

    const-string/jumbo v3, "CenturySchL-Bold"

    aput-object v3, v2, v5

    const-string/jumbo v3, "Arial"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    new-array v2, v7, [Ljava/lang/String;

    const-string/jumbo v3, "NewCenturySchlbk-BoldItalic"

    aput-object v3, v2, v4

    const-string/jumbo v3, "CenturySchL-BoldItal"

    aput-object v3, v2, v5

    const-string/jumbo v3, "Arial"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x20

    new-array v2, v7, [Ljava/lang/String;

    const-string/jumbo v3, "Times-Roman"

    aput-object v3, v2, v4

    const-string/jumbo v3, "Nimbus Roman No9 L Regular"

    aput-object v3, v2, v5

    const-string/jumbo v3, "Nimbus Roman No9 L"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x21

    new-array v2, v7, [Ljava/lang/String;

    const-string/jumbo v3, "Times-Italic"

    aput-object v3, v2, v4

    const-string/jumbo v3, "Nimbus Roman No9 L Regular Italic"

    aput-object v3, v2, v5

    const-string/jumbo v3, "Nimbus Roman No9 L"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x22

    new-array v2, v7, [Ljava/lang/String;

    const-string/jumbo v3, "Times-Bold"

    aput-object v3, v2, v4

    const-string/jumbo v3, "Nimbus Roman No9 L Medium"

    aput-object v3, v2, v5

    const-string/jumbo v3, "Nimbus Roman No9 L"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x23

    new-array v2, v7, [Ljava/lang/String;

    const-string/jumbo v3, "Times-BoldItalic"

    aput-object v3, v2, v4

    const-string/jumbo v3, "Nimbus Roman No9 L Medium Italic"

    aput-object v3, v2, v5

    const-string/jumbo v3, "Nimbus Roman No9 L"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x24

    new-array v2, v7, [Ljava/lang/String;

    const-string/jumbo v3, "Symbol"

    aput-object v3, v2, v4

    const-string/jumbo v3, "Standard Symbols L"

    aput-object v3, v2, v5

    const-string/jumbo v3, "Standard Symbols L"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x25

    new-array v2, v7, [Ljava/lang/String;

    const-string/jumbo v3, "ZapfChancery-MediumItalic"

    aput-object v3, v2, v4

    const-string/jumbo v3, "URWChanceryL-MediItal"

    aput-object v3, v2, v5

    const-string/jumbo v3, "Arial"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x26

    new-array v2, v7, [Ljava/lang/String;

    const-string/jumbo v3, "ZapfDingbats"

    aput-object v3, v2, v4

    const-string/jumbo v3, "Dingbats"

    aput-object v3, v2, v5

    const-string/jumbo v3, "Dingbats"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    sput-object v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->type1Diff:[[Ljava/lang/String;

    .line 159
    const/16 v0, 0xe

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "Courier"

    aput-object v1, v0, v4

    const-string/jumbo v1, "Courier-Bold"

    aput-object v1, v0, v5

    const-string/jumbo v1, "Courier-Oblique"

    aput-object v1, v0, v6

    const-string/jumbo v1, "Courier-BoldOblique"

    aput-object v1, v0, v7

    const-string/jumbo v1, "Helvetica"

    aput-object v1, v0, v8

    const/4 v1, 0x5

    const-string/jumbo v2, "Helvetica-Bold"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string/jumbo v2, "Helvetica-Oblique"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string/jumbo v2, "Helvetica-BoldOblique"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string/jumbo v2, "Times-Roman"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string/jumbo v2, "Times-Bold"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string/jumbo v2, "Times-Italic"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string/jumbo v2, "Times-BoldItalic"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string/jumbo v2, "ZapfDingbats"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string/jumbo v2, "Symbol"

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->Default14Fonts:Ljava/util/List;

    return-void
.end method

.method public constructor <init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V
    .locals 9
    .param p1, "library"    # Lorg/icepdf/core/util/Library;
    .param p2, "entries"    # Ljava/util/Hashtable;

    .prologue
    const/16 v7, 0x100

    const/4 v8, 0x0

    .line 182
    invoke-direct {p0, p1, p2}, Lorg/icepdf/core/pobjects/fonts/Font;-><init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V

    .line 67
    iput v8, p0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->style:I

    .line 185
    new-array v6, v7, [C

    iput-object v6, p0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->cMap:[C

    .line 186
    const/4 v2, 0x0

    .local v2, "i":C
    :goto_0
    if-ge v2, v7, :cond_0

    .line 187
    iget-object v6, p0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->cMap:[C

    aput-char v2, v6, v2

    .line 186
    add-int/lit8 v6, v2, 0x1

    int-to-char v2, v6

    goto :goto_0

    .line 191
    :cond_0
    iget-object v6, p0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->basefont:Ljava/lang/String;

    invoke-static {v6}, Lorg/icepdf/core/util/FontUtil;->guessAndroidFontStyle(Ljava/lang/String;)I

    move-result v6

    iput v6, p0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->style:I

    .line 194
    const-string/jumbo v6, "Name"

    invoke-virtual {p1, p2, v6}, Lorg/icepdf/core/util/Library;->getName(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->name:Ljava/lang/String;

    .line 197
    const-string/jumbo v6, "Subtype"

    invoke-virtual {p1, p2, v6}, Lorg/icepdf/core/util/Library;->getName(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->subtype:Ljava/lang/String;

    .line 201
    const-string/jumbo v6, "Serif"

    iput-object v6, p0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->basefont:Ljava/lang/String;

    .line 202
    const-string/jumbo v6, "BaseFont"

    invoke-virtual {p2, v6}, Ljava/util/Hashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1

    new-instance v6, Lorg/icepdf/core/pobjects/Name;

    const-string/jumbo v7, "BaseFont"

    invoke-direct {v6, v7}, Lorg/icepdf/core/pobjects/Name;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2, v6}, Ljava/util/Hashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 203
    :cond_1
    const-string/jumbo v6, "BaseFont"

    invoke-virtual {p2, v6}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    .line 204
    .local v5, "o":Ljava/lang/Object;
    if-nez v5, :cond_2

    .line 206
    new-instance v6, Lorg/icepdf/core/pobjects/Name;

    const-string/jumbo v7, "BaseFont"

    invoke-direct {v6, v7}, Lorg/icepdf/core/pobjects/Name;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2, v6}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    .line 208
    :cond_2
    instance-of v6, v5, Lorg/icepdf/core/pobjects/Name;

    if-eqz v6, :cond_3

    .line 209
    check-cast v5, Lorg/icepdf/core/pobjects/Name;

    .end local v5    # "o":Ljava/lang/Object;
    invoke-virtual {v5}, Lorg/icepdf/core/pobjects/Name;->getName()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->basefont:Ljava/lang/String;

    .line 213
    :cond_3
    iget-object v6, p0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->basefont:Ljava/lang/String;

    iput-object v6, p0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->originalFontName:Ljava/lang/String;

    .line 216
    iget-object v6, p0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->basefont:Ljava/lang/String;

    invoke-direct {p0, v6}, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->cleanFontName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->basefont:Ljava/lang/String;

    .line 219
    iget-object v6, p0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->subtype:Ljava/lang/String;

    const-string/jumbo v7, "Type3"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 220
    const-string/jumbo v6, "Symbol"

    iput-object v6, p0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->basefont:Ljava/lang/String;

    .line 221
    invoke-static {}, Lorg/icepdf/core/pobjects/fonts/ofont/Encoding;->getSymbol()Lorg/icepdf/core/pobjects/fonts/ofont/Encoding;

    move-result-object v6

    iput-object v6, p0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->encoding:Lorg/icepdf/core/pobjects/fonts/ofont/Encoding;

    .line 225
    :cond_4
    iget-object v6, p0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->subtype:Ljava/lang/String;

    const-string/jumbo v7, "Type1"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 227
    iget-object v6, p0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->basefont:Ljava/lang/String;

    const-string/jumbo v7, "Symbol"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 228
    invoke-static {}, Lorg/icepdf/core/pobjects/fonts/ofont/Encoding;->getSymbol()Lorg/icepdf/core/pobjects/fonts/ofont/Encoding;

    move-result-object v6

    iput-object v6, p0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->encoding:Lorg/icepdf/core/pobjects/fonts/ofont/Encoding;

    .line 245
    :cond_5
    :goto_1
    iget-object v6, p0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->subtype:Ljava/lang/String;

    const-string/jumbo v7, "TrueType"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 246
    iget-object v6, p0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->basefont:Ljava/lang/String;

    const-string/jumbo v7, "Symbol"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 247
    const-string/jumbo v6, "winAnsi"

    iput-object v6, p0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->encodingName:Ljava/lang/String;

    .line 248
    invoke-static {}, Lorg/icepdf/core/pobjects/fonts/ofont/Encoding;->getWinAnsi()Lorg/icepdf/core/pobjects/fonts/ofont/Encoding;

    move-result-object v6

    iput-object v6, p0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->encoding:Lorg/icepdf/core/pobjects/fonts/ofont/Encoding;

    .line 252
    :cond_6
    return-void

    .line 230
    :cond_7
    iget-object v6, p0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->basefont:Ljava/lang/String;

    const-string/jumbo v7, "ZapfDingbats"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_8

    iget-object v6, p0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->subtype:Ljava/lang/String;

    const-string/jumbo v7, "Type1"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 232
    invoke-static {}, Lorg/icepdf/core/pobjects/fonts/ofont/Encoding;->getZapfDingBats()Lorg/icepdf/core/pobjects/fonts/ofont/Encoding;

    move-result-object v6

    iput-object v6, p0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->encoding:Lorg/icepdf/core/pobjects/fonts/ofont/Encoding;

    goto :goto_1

    .line 235
    :cond_8
    sget-object v1, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->type1Diff:[[Ljava/lang/String;

    .local v1, "arr$":[[Ljava/lang/String;
    array-length v4, v1

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_2
    if-ge v3, v4, :cond_5

    aget-object v0, v1, v3

    .line 236
    .local v0, "aType1Diff":[Ljava/lang/String;
    iget-object v6, p0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->basefont:Ljava/lang/String;

    aget-object v7, v0, v8

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_9

    .line 237
    const-string/jumbo v6, "standard"

    iput-object v6, p0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->encodingName:Ljava/lang/String;

    .line 238
    invoke-static {}, Lorg/icepdf/core/pobjects/fonts/ofont/Encoding;->getStandard()Lorg/icepdf/core/pobjects/fonts/ofont/Encoding;

    move-result-object v6

    iput-object v6, p0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->encoding:Lorg/icepdf/core/pobjects/fonts/ofont/Encoding;

    goto :goto_1

    .line 235
    :cond_9
    add-int/lit8 v3, v3, 0x1

    goto :goto_2
.end method

.method private calculateCIDWidths()Ljava/util/Map;
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation

    .prologue
    const/high16 v14, 0x447a0000    # 1000.0f

    .line 690
    new-instance v1, Ljava/util/HashMap;

    const/16 v11, 0x4b

    invoke-direct {v1, v11}, Ljava/util/HashMap;-><init>(I)V

    .line 692
    .local v1, "cidWidths":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Integer;Ljava/lang/Float;>;"
    iget-object v11, p0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->library:Lorg/icepdf/core/util/Library;

    iget-object v12, p0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->entries:Ljava/util/Hashtable;

    const-string/jumbo v13, "W"

    invoke-virtual {v11, v12, v13}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    .line 693
    .local v7, "o":Ljava/lang/Object;
    instance-of v11, v7, Ljava/util/Vector;

    if-eqz v11, :cond_8

    move-object v0, v7

    .line 694
    check-cast v0, Ljava/util/Vector;

    .line 701
    .local v0, "cidWidth":Ljava/util/Vector;
    const/4 v4, 0x0

    .local v4, "i":I
    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v11

    add-int/lit8 v6, v11, -0x2

    .local v6, "max":I
    :goto_0
    if-ge v4, v6, :cond_8

    .line 702
    invoke-virtual {v0, v4}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v2

    .line 703
    .local v2, "current":Ljava/lang/Object;
    add-int/lit8 v11, v4, 0x1

    invoke-virtual {v0, v11}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v8

    .line 705
    .local v8, "peek":Ljava/lang/Object;
    instance-of v11, v2, Ljava/lang/Integer;

    if-eqz v11, :cond_3

    instance-of v11, v8, Ljava/util/Vector;

    if-eqz v11, :cond_3

    move-object v11, v2

    .line 708
    check-cast v11, Ljava/lang/Integer;

    invoke-virtual {v11}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .local v3, "currentChar":I
    move-object v10, v8

    .line 709
    check-cast v10, Ljava/util/Vector;

    .line 710
    .local v10, "subWidth":Ljava/util/Vector;
    const/4 v5, 0x0

    .local v5, "j":I
    invoke-virtual {v10}, Ljava/util/Vector;->size()I

    move-result v9

    .local v9, "subMax":I
    :goto_1
    if-ge v5, v9, :cond_2

    .line 711
    invoke-virtual {v10, v5}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v11

    instance-of v11, v11, Ljava/lang/Integer;

    if-eqz v11, :cond_1

    .line 712
    add-int v11, v3, v5

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v10, v5}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/Integer;

    invoke-virtual {v11}, Ljava/lang/Integer;->intValue()I

    move-result v11

    int-to-float v11, v11

    div-float/2addr v11, v14

    invoke-static {v11}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v11

    invoke-virtual {v1, v12, v11}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 710
    :cond_0
    :goto_2
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 713
    :cond_1
    invoke-virtual {v10, v5}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v11

    instance-of v11, v11, Ljava/lang/Float;

    if-eqz v11, :cond_0

    .line 714
    add-int v11, v3, v5

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v10, v5}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/Float;

    invoke-virtual {v11}, Ljava/lang/Float;->floatValue()F

    move-result v11

    div-float/2addr v11, v14

    invoke-static {v11}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v11

    invoke-virtual {v1, v12, v11}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 717
    :cond_2
    add-int/lit8 v4, v4, 0x1

    .line 719
    .end local v3    # "currentChar":I
    .end local v5    # "j":I
    .end local v9    # "subMax":I
    .end local v10    # "subWidth":Ljava/util/Vector;
    :cond_3
    instance-of v11, v2, Ljava/lang/Integer;

    if-eqz v11, :cond_7

    instance-of v11, v8, Ljava/lang/Integer;

    if-eqz v11, :cond_7

    .line 721
    check-cast v2, Ljava/lang/Integer;

    .end local v2    # "current":Ljava/lang/Object;
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v5

    .restart local v5    # "j":I
    :goto_3
    move-object v11, v8

    check-cast v11, Ljava/lang/Integer;

    invoke-virtual {v11}, Ljava/lang/Integer;->intValue()I

    move-result v11

    if-gt v5, v11, :cond_6

    .line 723
    move v3, v5

    .line 724
    .restart local v3    # "currentChar":I
    add-int/lit8 v11, v4, 0x2

    invoke-virtual {v0, v11}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v11

    instance-of v11, v11, Ljava/lang/Integer;

    if-eqz v11, :cond_5

    .line 725
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    add-int/lit8 v11, v4, 0x2

    invoke-virtual {v0, v11}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/Integer;

    invoke-virtual {v11}, Ljava/lang/Integer;->intValue()I

    move-result v11

    int-to-float v11, v11

    div-float/2addr v11, v14

    invoke-static {v11}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v11

    invoke-virtual {v1, v12, v11}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 721
    :cond_4
    :goto_4
    add-int/lit8 v5, v5, 0x1

    goto :goto_3

    .line 726
    :cond_5
    add-int/lit8 v11, v4, 0x2

    invoke-virtual {v0, v11}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v11

    instance-of v11, v11, Ljava/lang/Float;

    if-eqz v11, :cond_4

    .line 727
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    add-int/lit8 v11, v4, 0x2

    invoke-virtual {v0, v11}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/Float;

    invoke-virtual {v11}, Ljava/lang/Float;->floatValue()F

    move-result v11

    div-float/2addr v11, v14

    invoke-static {v11}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v11

    invoke-virtual {v1, v12, v11}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_4

    .line 730
    .end local v3    # "currentChar":I
    :cond_6
    add-int/lit8 v4, v4, 0x2

    .line 701
    .end local v5    # "j":I
    :cond_7
    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_0

    .line 734
    .end local v0    # "cidWidth":Ljava/util/Vector;
    .end local v4    # "i":I
    .end local v6    # "max":I
    .end local v8    # "peek":Ljava/lang/Object;
    :cond_8
    return-object v1
.end method

.method private cleanFontName(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p1, "fontName"    # Ljava/lang/String;

    .prologue
    const/16 v4, 0x2b

    .line 659
    if-eqz p1, :cond_3

    .line 660
    invoke-virtual {p1, v4}, Ljava/lang/String;->indexOf(I)I

    move-result v2

    if-ltz v2, :cond_0

    .line 661
    invoke-virtual {p1, v4}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    .line 662
    .local v0, "index":I
    add-int/lit8 v2, v0, 0x1

    invoke-virtual {p1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 664
    .local v1, "tmp":Ljava/lang/String;
    :try_start_0
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 665
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object p1

    .line 674
    .end local v0    # "index":I
    .end local v1    # "tmp":Ljava/lang/String;
    :cond_0
    :goto_0
    invoke-virtual {p1, v4}, Ljava/lang/String;->indexOf(I)I

    move-result v2

    if-ltz v2, :cond_1

    .line 675
    invoke-virtual {p1, v4}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    .line 677
    .restart local v0    # "index":I
    add-int/lit8 v2, v0, 0x1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    .line 679
    goto :goto_0

    .line 682
    .end local v0    # "index":I
    :cond_1
    iget-object v2, p0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->subtype:Ljava/lang/String;

    const-string/jumbo v3, "Type0"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->subtype:Ljava/lang/String;

    const-string/jumbo v3, "Type1"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->subtype:Ljava/lang/String;

    const-string/jumbo v3, "MMType1"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->subtype:Ljava/lang/String;

    const-string/jumbo v3, "TrueType"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 683
    :cond_2
    const/16 v2, 0x2c

    const/16 v3, 0x2d

    invoke-virtual {p1, v2, v3}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object p1

    .line 686
    :cond_3
    return-object p1

    .line 666
    .restart local v0    # "index":I
    .restart local v1    # "tmp":Ljava/lang/String;
    :catch_0
    move-exception v2

    goto :goto_0
.end method

.method private getWidth(IF)F
    .locals 4
    .param p1, "character"    # I
    .param p2, "advance"    # F

    .prologue
    const/high16 v3, 0x447a0000    # 1000.0f

    .line 601
    iget v1, p0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->firstchar:I

    sub-int/2addr p1, v1

    .line 602
    iget-object v1, p0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->widths:Ljava/util/Vector;

    if-eqz v1, :cond_1

    .line 603
    if-ltz p1, :cond_0

    iget-object v1, p0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->widths:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v1

    if-ge p1, v1, :cond_0

    .line 604
    iget-object v1, p0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->widths:Ljava/util/Vector;

    invoke-virtual {v1, p1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Number;

    invoke-virtual {v1}, Ljava/lang/Number;->floatValue()F

    move-result v1

    div-float p2, v1, v3

    .line 619
    .end local p2    # "advance":F
    :cond_0
    :goto_0
    return p2

    .line 608
    .restart local p2    # "advance":F
    :cond_1
    iget-object v1, p0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->defaultfont:Lorg/icepdf/core/pobjects/fonts/DefaultFonts;

    if-eqz v1, :cond_2

    .line 609
    iget-object v1, p0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->defaultfont:Lorg/icepdf/core/pobjects/fonts/DefaultFonts;

    invoke-virtual {v1}, Lorg/icepdf/core/pobjects/fonts/DefaultFonts;->getWidths()[F

    move-result-object v1

    aget v1, v1, p1

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    .line 610
    .local v0, "i":Ljava/lang/Float;
    if-eqz v0, :cond_0

    .line 611
    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v1

    div-float p2, v1, v3

    goto :goto_0

    .line 615
    .end local v0    # "i":Ljava/lang/Float;
    :cond_2
    iget-object v1, p0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->fontDescriptor:Lorg/icepdf/core/pobjects/fonts/FontDescriptor;

    if-eqz v1, :cond_0

    .line 616
    iget-object v1, p0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->fontDescriptor:Lorg/icepdf/core/pobjects/fonts/FontDescriptor;

    invoke-virtual {v1}, Lorg/icepdf/core/pobjects/fonts/FontDescriptor;->getMissingWidth()F

    move-result v1

    const/4 v2, 0x0

    cmpl-float v1, v1, v2

    if-lez v1, :cond_0

    .line 617
    iget-object v1, p0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->fontDescriptor:Lorg/icepdf/core/pobjects/fonts/FontDescriptor;

    invoke-virtual {v1}, Lorg/icepdf/core/pobjects/fonts/FontDescriptor;->getMissingWidth()F

    move-result v1

    div-float p2, v1, v3

    goto :goto_0
.end method

.method private setBaseEncoding(Ljava/lang/String;)V
    .locals 3
    .param p1, "baseencoding"    # Ljava/lang/String;

    .prologue
    .line 558
    if-nez p1, :cond_1

    .line 559
    const-string/jumbo v1, "none"

    iput-object v1, p0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->encodingName:Ljava/lang/String;

    .line 580
    :cond_0
    return-void

    .line 561
    :cond_1
    const-string/jumbo v1, "StandardEncoding"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 562
    const-string/jumbo v1, "StandardEncoding"

    iput-object v1, p0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->encodingName:Ljava/lang/String;

    .line 563
    invoke-static {}, Lorg/icepdf/core/pobjects/fonts/ofont/Encoding;->getStandard()Lorg/icepdf/core/pobjects/fonts/ofont/Encoding;

    move-result-object v1

    iput-object v1, p0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->encoding:Lorg/icepdf/core/pobjects/fonts/ofont/Encoding;

    .line 575
    :cond_2
    :goto_0
    iget-object v1, p0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->encoding:Lorg/icepdf/core/pobjects/fonts/ofont/Encoding;

    if-eqz v1, :cond_0

    .line 576
    const/4 v0, 0x0

    .local v0, "i":C
    :goto_1
    const/16 v1, 0x100

    if-ge v0, v1, :cond_0

    .line 577
    iget-object v1, p0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->cMap:[C

    iget-object v2, p0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->encoding:Lorg/icepdf/core/pobjects/fonts/ofont/Encoding;

    invoke-virtual {v2, v0}, Lorg/icepdf/core/pobjects/fonts/ofont/Encoding;->get(C)C

    move-result v2

    aput-char v2, v1, v0

    .line 576
    add-int/lit8 v1, v0, 0x1

    int-to-char v0, v1

    goto :goto_1

    .line 564
    .end local v0    # "i":C
    :cond_3
    const-string/jumbo v1, "MacRomanEncoding"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 565
    const-string/jumbo v1, "MacRomanEncoding"

    iput-object v1, p0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->encodingName:Ljava/lang/String;

    .line 566
    invoke-static {}, Lorg/icepdf/core/pobjects/fonts/ofont/Encoding;->getMacRoman()Lorg/icepdf/core/pobjects/fonts/ofont/Encoding;

    move-result-object v1

    iput-object v1, p0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->encoding:Lorg/icepdf/core/pobjects/fonts/ofont/Encoding;

    goto :goto_0

    .line 567
    :cond_4
    const-string/jumbo v1, "WinAnsiEncoding"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 568
    const-string/jumbo v1, "WinAnsiEncoding"

    iput-object v1, p0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->encodingName:Ljava/lang/String;

    .line 569
    invoke-static {}, Lorg/icepdf/core/pobjects/fonts/ofont/Encoding;->getWinAnsi()Lorg/icepdf/core/pobjects/fonts/ofont/Encoding;

    move-result-object v1

    iput-object v1, p0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->encoding:Lorg/icepdf/core/pobjects/fonts/ofont/Encoding;

    goto :goto_0

    .line 570
    :cond_5
    const-string/jumbo v1, "PDFDocEncoding"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 571
    const-string/jumbo v1, "PDFDocEncoding"

    iput-object v1, p0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->encodingName:Ljava/lang/String;

    .line 572
    invoke-static {}, Lorg/icepdf/core/pobjects/fonts/ofont/Encoding;->getPDFDoc()Lorg/icepdf/core/pobjects/fonts/ofont/Encoding;

    move-result-object v1

    iput-object v1, p0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->encoding:Lorg/icepdf/core/pobjects/fonts/ofont/Encoding;

    goto :goto_0
.end method

.method private setWidth()V
    .locals 15

    .prologue
    const/high16 v6, 0x447a0000    # 1000.0f

    .line 627
    const/4 v3, 0x0

    .line 628
    .local v3, "missingWidth":F
    const/4 v4, 0x0

    .line 629
    .local v4, "ascent":F
    const/4 v5, 0x0

    .line 630
    .local v5, "descent":F
    iget-object v0, p0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->fontDescriptor:Lorg/icepdf/core/pobjects/fonts/FontDescriptor;

    if-eqz v0, :cond_0

    .line 631
    iget-object v0, p0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->fontDescriptor:Lorg/icepdf/core/pobjects/fonts/FontDescriptor;

    invoke-virtual {v0}, Lorg/icepdf/core/pobjects/fonts/FontDescriptor;->getMissingWidth()F

    move-result v0

    const/4 v2, 0x0

    cmpl-float v0, v0, v2

    if-lez v0, :cond_0

    .line 632
    iget-object v0, p0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->fontDescriptor:Lorg/icepdf/core/pobjects/fonts/FontDescriptor;

    invoke-virtual {v0}, Lorg/icepdf/core/pobjects/fonts/FontDescriptor;->getMissingWidth()F

    move-result v0

    div-float v3, v0, v6

    .line 633
    iget-object v0, p0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->fontDescriptor:Lorg/icepdf/core/pobjects/fonts/FontDescriptor;

    invoke-virtual {v0}, Lorg/icepdf/core/pobjects/fonts/FontDescriptor;->getAscent()F

    move-result v0

    div-float v4, v0, v6

    .line 634
    iget-object v0, p0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->fontDescriptor:Lorg/icepdf/core/pobjects/fonts/FontDescriptor;

    invoke-virtual {v0}, Lorg/icepdf/core/pobjects/fonts/FontDescriptor;->getDescent()F

    move-result v0

    div-float v5, v0, v6

    .line 637
    :cond_0
    iget-object v0, p0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->widths:Ljava/util/Vector;

    if-eqz v0, :cond_4

    .line 638
    iget v0, p0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->firstchar:I

    rsub-int v0, v0, 0x100

    new-array v1, v0, [F

    .line 639
    .local v1, "newWidth":[F
    const/4 v13, 0x0

    .local v13, "i":I
    iget-object v0, p0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->widths:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v14

    .local v14, "max":I
    :goto_0
    if-ge v13, v14, :cond_2

    .line 640
    iget-object v0, p0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->widths:Ljava/util/Vector;

    invoke-virtual {v0, v13}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 641
    iget-object v0, p0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->widths:Ljava/util/Vector;

    invoke-virtual {v0, v13}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->floatValue()F

    move-result v0

    div-float/2addr v0, v6

    aput v0, v1, v13

    .line 639
    :cond_1
    add-int/lit8 v13, v13, 0x1

    goto :goto_0

    .line 644
    :cond_2
    iget-object v0, p0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->font:Lorg/icepdf/core/pobjects/fonts/FontFile;

    iget v2, p0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->firstchar:I

    iget-object v6, p0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->cMap:[C

    invoke-interface/range {v0 .. v6}, Lorg/icepdf/core/pobjects/fonts/FontFile;->deriveFont([FIFFF[C)Lorg/icepdf/core/pobjects/fonts/FontFile;

    move-result-object v0

    iput-object v0, p0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->font:Lorg/icepdf/core/pobjects/fonts/FontFile;

    .line 652
    .end local v1    # "newWidth":[F
    .end local v13    # "i":I
    .end local v14    # "max":I
    :cond_3
    :goto_1
    return-void

    .line 645
    :cond_4
    iget-object v0, p0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->cidWidths:Ljava/util/Map;

    if-eqz v0, :cond_5

    .line 647
    iget-object v6, p0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->font:Lorg/icepdf/core/pobjects/fonts/FontFile;

    iget-object v7, p0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->cidWidths:Ljava/util/Map;

    iget v8, p0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->firstchar:I

    const/4 v12, 0x0

    move v9, v3

    move v10, v4

    move v11, v5

    invoke-interface/range {v6 .. v12}, Lorg/icepdf/core/pobjects/fonts/FontFile;->deriveFont(Ljava/util/Map;IFFF[C)Lorg/icepdf/core/pobjects/fonts/FontFile;

    move-result-object v0

    iput-object v0, p0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->font:Lorg/icepdf/core/pobjects/fonts/FontFile;

    goto :goto_1

    .line 648
    :cond_5
    iget-object v0, p0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->defaultfont:Lorg/icepdf/core/pobjects/fonts/DefaultFonts;

    if-eqz v0, :cond_3

    .line 649
    iget-object v6, p0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->font:Lorg/icepdf/core/pobjects/fonts/FontFile;

    iget-object v0, p0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->defaultfont:Lorg/icepdf/core/pobjects/fonts/DefaultFonts;

    invoke-virtual {v0}, Lorg/icepdf/core/pobjects/fonts/DefaultFonts;->getWidths()[F

    move-result-object v7

    iget v8, p0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->firstchar:I

    iget-object v12, p0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->cMap:[C

    move v9, v3

    move v10, v4

    move v11, v5

    invoke-interface/range {v6 .. v12}, Lorg/icepdf/core/pobjects/fonts/FontFile;->deriveFont([FIFFF[C)Lorg/icepdf/core/pobjects/fonts/FontFile;

    move-result-object v0

    iput-object v0, p0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->font:Lorg/icepdf/core/pobjects/fonts/FontFile;

    goto :goto_1
.end method


# virtual methods
.method public init()V
    .locals 35

    .prologue
    .line 260
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->inited:Z

    if-eqz v3, :cond_0

    .line 549
    :goto_0
    return-void

    .line 265
    :cond_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->encoding:Lorg/icepdf/core/pobjects/fonts/ofont/Encoding;

    if-eqz v3, :cond_1

    .line 266
    const/16 v26, 0x0

    .local v26, "i":C
    :goto_1
    const/16 v3, 0x100

    move/from16 v0, v26

    if-ge v0, v3, :cond_1

    .line 267
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->cMap:[C

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->encoding:Lorg/icepdf/core/pobjects/fonts/ofont/Encoding;

    move/from16 v0, v26

    invoke-virtual {v4, v0}, Lorg/icepdf/core/pobjects/fonts/ofont/Encoding;->get(C)C

    move-result v4

    aput-char v4, v3, v26

    .line 266
    add-int/lit8 v3, v26, 0x1

    int-to-char v0, v3

    move/from16 v26, v0

    goto :goto_1

    .line 272
    .end local v26    # "i":C
    :cond_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->library:Lorg/icepdf/core/util/Library;

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->entries:Ljava/util/Hashtable;

    const-string/jumbo v5, "ToUnicode"

    invoke-virtual {v3, v4, v5}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v31

    .line 273
    .local v31, "objectUnicode":Ljava/lang/Object;
    if-eqz v31, :cond_2

    move-object/from16 v0, v31

    instance-of v3, v0, Lorg/icepdf/core/pobjects/Stream;

    if-eqz v3, :cond_2

    .line 274
    new-instance v3, Lorg/icepdf/core/pobjects/fonts/ofont/CMap;

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->library:Lorg/icepdf/core/util/Library;

    new-instance v5, Ljava/util/Hashtable;

    invoke-direct {v5}, Ljava/util/Hashtable;-><init>()V

    check-cast v31, Lorg/icepdf/core/pobjects/Stream;

    .end local v31    # "objectUnicode":Ljava/lang/Object;
    move-object/from16 v0, v31

    invoke-direct {v3, v4, v5, v0}, Lorg/icepdf/core/pobjects/fonts/ofont/CMap;-><init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;Lorg/icepdf/core/pobjects/Stream;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->toUnicodeCMap:Lorg/icepdf/core/pobjects/fonts/ofont/CMap;

    .line 275
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->toUnicodeCMap:Lorg/icepdf/core/pobjects/fonts/ofont/CMap;

    invoke-virtual {v3}, Lorg/icepdf/core/pobjects/fonts/ofont/CMap;->init()V

    .line 279
    :cond_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->library:Lorg/icepdf/core/util/Library;

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->entries:Ljava/util/Hashtable;

    const-string/jumbo v5, "Encoding"

    invoke-virtual {v3, v4, v5}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v30

    .line 280
    .local v30, "o":Ljava/lang/Object;
    if-eqz v30, :cond_7

    .line 281
    move-object/from16 v0, v30

    instance-of v3, v0, Ljava/util/Hashtable;

    if-eqz v3, :cond_6

    move-object/from16 v22, v30

    .line 282
    check-cast v22, Ljava/util/Hashtable;

    .line 283
    .local v22, "encoding":Ljava/util/Hashtable;
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->library:Lorg/icepdf/core/util/Library;

    const-string/jumbo v4, "BaseEncoding"

    move-object/from16 v0, v22

    invoke-virtual {v3, v0, v4}, Lorg/icepdf/core/util/Library;->getName(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->setBaseEncoding(Ljava/lang/String;)V

    .line 284
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->library:Lorg/icepdf/core/util/Library;

    const-string/jumbo v4, "Differences"

    move-object/from16 v0, v22

    invoke-virtual {v3, v0, v4}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Ljava/util/Vector;

    .line 285
    .local v20, "differences":Ljava/util/Vector;
    if-eqz v20, :cond_7

    .line 286
    const/4 v15, 0x0

    .line 287
    .local v15, "c":I
    invoke-virtual/range {v20 .. v20}, Ljava/util/Vector;->elements()Ljava/util/Enumeration;

    move-result-object v21

    .line 288
    .local v21, "e":Ljava/util/Enumeration;
    :cond_3
    :goto_2
    invoke-interface/range {v21 .. v21}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v3

    if-eqz v3, :cond_7

    .line 289
    invoke-interface/range {v21 .. v21}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v33

    .line 290
    .local v33, "oo":Ljava/lang/Object;
    move-object/from16 v0, v33

    instance-of v3, v0, Ljava/lang/Number;

    if-eqz v3, :cond_4

    .line 291
    check-cast v33, Ljava/lang/Number;

    .end local v33    # "oo":Ljava/lang/Object;
    invoke-virtual/range {v33 .. v33}, Ljava/lang/Number;->intValue()I

    move-result v15

    goto :goto_2

    .line 292
    .restart local v33    # "oo":Ljava/lang/Object;
    :cond_4
    move-object/from16 v0, v33

    instance-of v3, v0, Lorg/icepdf/core/pobjects/Name;

    if-eqz v3, :cond_3

    .line 293
    invoke-virtual/range {v33 .. v33}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v29

    .line 294
    .local v29, "n":Ljava/lang/String;
    invoke-static/range {v29 .. v29}, Lorg/icepdf/core/pobjects/fonts/ofont/Encoding;->getUV(Ljava/lang/String;)I

    move-result v16

    .line 295
    .local v16, "c1":I
    const/4 v3, -0x1

    move/from16 v0, v16

    if-ne v0, v3, :cond_5

    .line 296
    const/4 v3, 0x0

    move-object/from16 v0, v29

    invoke-virtual {v0, v3}, Ljava/lang/String;->charAt(I)C

    move-result v3

    const/16 v4, 0x61

    if-ne v3, v4, :cond_5

    .line 297
    const/4 v3, 0x1

    move-object/from16 v0, v29

    invoke-virtual {v0, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v29

    .line 299
    :try_start_0
    invoke-static/range {v29 .. v29}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v16

    .line 305
    :cond_5
    :goto_3
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->cMap:[C

    move/from16 v0, v16

    int-to-char v4, v0

    aput-char v4, v3, v15

    .line 306
    add-int/lit8 v15, v15, 0x1

    goto :goto_2

    .line 310
    .end local v15    # "c":I
    .end local v16    # "c1":I
    .end local v20    # "differences":Ljava/util/Vector;
    .end local v21    # "e":Ljava/util/Enumeration;
    .end local v22    # "encoding":Ljava/util/Hashtable;
    .end local v29    # "n":Ljava/lang/String;
    .end local v33    # "oo":Ljava/lang/Object;
    :cond_6
    move-object/from16 v0, v30

    instance-of v3, v0, Lorg/icepdf/core/pobjects/Name;

    if-eqz v3, :cond_7

    move-object/from16 v3, v30

    .line 311
    check-cast v3, Lorg/icepdf/core/pobjects/Name;

    invoke-virtual {v3}, Lorg/icepdf/core/pobjects/Name;->getName()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->setBaseEncoding(Ljava/lang/String;)V

    .line 317
    :cond_7
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->library:Lorg/icepdf/core/util/Library;

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->entries:Ljava/util/Hashtable;

    const-string/jumbo v5, "Widths"

    invoke-virtual {v3, v4, v5}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Vector;

    check-cast v3, Ljava/util/Vector;

    move-object/from16 v0, p0

    iput-object v3, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->widths:Ljava/util/Vector;

    .line 318
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->widths:Ljava/util/Vector;

    if-eqz v3, :cond_16

    .line 321
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->library:Lorg/icepdf/core/util/Library;

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->entries:Ljava/util/Hashtable;

    const-string/jumbo v5, "FirstChar"

    invoke-virtual {v3, v4, v5}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v30

    .line 322
    if-eqz v30, :cond_8

    .line 323
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->library:Lorg/icepdf/core/util/Library;

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->entries:Ljava/util/Hashtable;

    const-string/jumbo v5, "FirstChar"

    invoke-virtual {v3, v4, v5}, Lorg/icepdf/core/util/Library;->getFloat(Ljava/util/Hashtable;Ljava/lang/String;)F

    move-result v3

    float-to-int v3, v3

    move-object/from16 v0, p0

    iput v3, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->firstchar:I

    .line 340
    :cond_8
    :goto_4
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->library:Lorg/icepdf/core/util/Library;

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->entries:Ljava/util/Hashtable;

    const-string/jumbo v5, "FontDescriptor"

    invoke-virtual {v3, v4, v5}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v32

    .line 341
    .local v32, "of":Ljava/lang/Object;
    move-object/from16 v0, v32

    instance-of v3, v0, Lorg/icepdf/core/pobjects/fonts/FontDescriptor;

    if-eqz v3, :cond_9

    .line 342
    check-cast v32, Lorg/icepdf/core/pobjects/fonts/FontDescriptor;

    .end local v32    # "of":Ljava/lang/Object;
    move-object/from16 v0, v32

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->fontDescriptor:Lorg/icepdf/core/pobjects/fonts/FontDescriptor;

    .line 343
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->fontDescriptor:Lorg/icepdf/core/pobjects/fonts/FontDescriptor;

    invoke-virtual {v3}, Lorg/icepdf/core/pobjects/fonts/FontDescriptor;->init()V

    .line 350
    :cond_9
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->fontDescriptor:Lorg/icepdf/core/pobjects/fonts/FontDescriptor;

    if-nez v3, :cond_a

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->basefont:Ljava/lang/String;

    if-eqz v3, :cond_a

    .line 352
    sget-object v3, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->Default14Fonts:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->basefont:Ljava/lang/String;

    invoke-interface {v3, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v23

    .line 354
    .local v23, "flag":Z
    if-eqz v23, :cond_a

    .line 355
    new-instance v17, Lorg/icepdf/core/pobjects/fonts/DefaultFonts;

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->basefont:Ljava/lang/String;

    move-object/from16 v0, v17

    invoke-direct {v0, v3}, Lorg/icepdf/core/pobjects/fonts/DefaultFonts;-><init>(Ljava/lang/String;)V

    .line 359
    .local v17, "defaultfont":Lorg/icepdf/core/pobjects/fonts/DefaultFonts;
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->library:Lorg/icepdf/core/util/Library;

    move-object/from16 v0, v17

    invoke-static {v3, v0}, Lorg/icepdf/core/pobjects/fonts/FontDescriptor;->createDescriptor(Lorg/icepdf/core/util/Library;Lorg/icepdf/core/pobjects/fonts/DefaultFonts;)Lorg/icepdf/core/pobjects/fonts/FontDescriptor;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->fontDescriptor:Lorg/icepdf/core/pobjects/fonts/FontDescriptor;

    .line 368
    .end local v17    # "defaultfont":Lorg/icepdf/core/pobjects/fonts/DefaultFonts;
    .end local v23    # "flag":Z
    :cond_a
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->fontDescriptor:Lorg/icepdf/core/pobjects/fonts/FontDescriptor;

    if-eqz v3, :cond_b

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->fontDescriptor:Lorg/icepdf/core/pobjects/fonts/FontDescriptor;

    invoke-virtual {v3}, Lorg/icepdf/core/pobjects/fonts/FontDescriptor;->getFontName()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_b

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->fontDescriptor:Lorg/icepdf/core/pobjects/fonts/FontDescriptor;

    invoke-virtual {v3}, Lorg/icepdf/core/pobjects/fonts/FontDescriptor;->getFontName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_b

    .line 369
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->fontDescriptor:Lorg/icepdf/core/pobjects/fonts/FontDescriptor;

    invoke-virtual {v3}, Lorg/icepdf/core/pobjects/fonts/FontDescriptor;->getFontName()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->basefont:Ljava/lang/String;

    .line 370
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->basefont:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->cleanFontName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->basefont:Ljava/lang/String;

    .line 374
    :cond_b
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->fontDescriptor:Lorg/icepdf/core/pobjects/fonts/FontDescriptor;

    if-eqz v3, :cond_c

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->fontDescriptor:Lorg/icepdf/core/pobjects/fonts/FontDescriptor;

    invoke-virtual {v3}, Lorg/icepdf/core/pobjects/fonts/FontDescriptor;->getFlags()I

    move-result v3

    and-int/lit8 v3, v3, 0x40

    if-eqz v3, :cond_c

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->encoding:Lorg/icepdf/core/pobjects/fonts/ofont/Encoding;

    if-nez v3, :cond_c

    .line 375
    const-string/jumbo v3, "standard"

    move-object/from16 v0, p0

    iput-object v3, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->encodingName:Ljava/lang/String;

    .line 376
    invoke-static {}, Lorg/icepdf/core/pobjects/fonts/ofont/Encoding;->getStandard()Lorg/icepdf/core/pobjects/fonts/ofont/Encoding;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->encoding:Lorg/icepdf/core/pobjects/fonts/ofont/Encoding;

    .line 383
    :cond_c
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->library:Lorg/icepdf/core/util/Library;

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->entries:Ljava/util/Hashtable;

    const-string/jumbo v5, "DescendantFonts"

    invoke-virtual {v3, v4, v5}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v19

    .line 384
    .local v19, "desendantFont":Ljava/lang/Object;
    if-eqz v19, :cond_d

    move-object/from16 v34, v19

    .line 385
    check-cast v34, Ljava/util/Vector;

    .line 386
    .local v34, "tmp":Ljava/util/Vector;
    const/4 v3, 0x0

    move-object/from16 v0, v34

    invoke-virtual {v0, v3}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v3

    instance-of v3, v3, Lorg/icepdf/core/pobjects/Reference;

    if-eqz v3, :cond_d

    .line 388
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->library:Lorg/icepdf/core/util/Library;

    const/4 v3, 0x0

    move-object/from16 v0, v34

    invoke-virtual {v0, v3}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/icepdf/core/pobjects/Reference;

    invoke-virtual {v4, v3}, Lorg/icepdf/core/util/Library;->getObject(Lorg/icepdf/core/pobjects/Reference;)Ljava/lang/Object;

    move-result-object v25

    .line 389
    .local v25, "fontReference":Ljava/lang/Object;
    move-object/from16 v0, v25

    instance-of v3, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;

    if-eqz v3, :cond_d

    move-object/from16 v18, v25

    .line 391
    check-cast v18, Lorg/icepdf/core/pobjects/fonts/ofont/Font;

    .line 392
    .local v18, "desendant":Lorg/icepdf/core/pobjects/fonts/ofont/Font;
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->toUnicodeCMap:Lorg/icepdf/core/pobjects/fonts/ofont/CMap;

    move-object/from16 v0, v18

    iput-object v3, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->toUnicodeCMap:Lorg/icepdf/core/pobjects/fonts/ofont/CMap;

    .line 393
    invoke-virtual/range {v18 .. v18}, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->init()V

    .line 394
    move-object/from16 v0, v18

    iget-object v3, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->cidWidths:Ljava/util/Map;

    move-object/from16 v0, p0

    iput-object v3, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->cidWidths:Ljava/util/Map;

    .line 397
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->fontDescriptor:Lorg/icepdf/core/pobjects/fonts/FontDescriptor;

    if-nez v3, :cond_d

    .line 398
    move-object/from16 v0, v18

    iget-object v3, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->fontDescriptor:Lorg/icepdf/core/pobjects/fonts/FontDescriptor;

    move-object/from16 v0, p0

    iput-object v3, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->fontDescriptor:Lorg/icepdf/core/pobjects/fonts/FontDescriptor;

    .line 399
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->fontDescriptor:Lorg/icepdf/core/pobjects/fonts/FontDescriptor;

    invoke-virtual {v3}, Lorg/icepdf/core/pobjects/fonts/FontDescriptor;->getFontName()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->basefont:Ljava/lang/String;

    .line 400
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->basefont:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->cleanFontName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->basefont:Ljava/lang/String;

    .line 410
    .end local v18    # "desendant":Lorg/icepdf/core/pobjects/fonts/ofont/Font;
    .end local v25    # "fontReference":Ljava/lang/Object;
    .end local v34    # "tmp":Ljava/util/Vector;
    :cond_d
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->subtype:Ljava/lang/String;

    const-string/jumbo v4, "Type1"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_e

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->basefont:Ljava/lang/String;

    if-eqz v3, :cond_e

    .line 411
    sget-object v3, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->Default14Fonts:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->basefont:Ljava/lang/String;

    invoke-interface {v3, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v23

    .line 413
    .restart local v23    # "flag":Z
    if-eqz v23, :cond_e

    .line 414
    new-instance v24, Lorg/icepdf/core/pobjects/fonts/DefaultFonts;

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->basefont:Ljava/lang/String;

    move-object/from16 v0, v24

    invoke-direct {v0, v3}, Lorg/icepdf/core/pobjects/fonts/DefaultFonts;-><init>(Ljava/lang/String;)V

    .line 415
    .local v24, "font":Lorg/icepdf/core/pobjects/fonts/DefaultFonts;
    move-object/from16 v0, v24

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->defaultfont:Lorg/icepdf/core/pobjects/fonts/DefaultFonts;

    .line 421
    .end local v23    # "flag":Z
    .end local v24    # "font":Lorg/icepdf/core/pobjects/fonts/DefaultFonts;
    :cond_e
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->subtype:Ljava/lang/String;

    const-string/jumbo v4, "Type1"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_f

    .line 422
    sget-object v14, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->type1Diff:[[Ljava/lang/String;

    .local v14, "arr$":[[Ljava/lang/String;
    array-length v0, v14

    move/from16 v28, v0

    .local v28, "len$":I
    const/16 v27, 0x0

    .local v27, "i$":I
    :goto_5
    move/from16 v0, v27

    move/from16 v1, v28

    if-ge v0, v1, :cond_f

    aget-object v13, v14, v27

    .line 426
    .local v13, "aType1Diff":[Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->basefont:Ljava/lang/String;

    if-eqz v3, :cond_18

    .line 427
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->basefont:Ljava/lang/String;

    const/4 v4, 0x0

    aget-object v4, v13, v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_18

    .line 431
    new-instance v2, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;

    const/4 v3, 0x1

    aget-object v3, v13, v3

    move-object/from16 v0, p0

    iget v4, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->style:I

    invoke-static {v3, v4}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->basefont:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->basefont:Ljava/lang/String;

    const/high16 v6, 0x41400000    # 12.0f

    move-object/from16 v0, p0

    iget-object v7, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->originalFontName:Ljava/lang/String;

    invoke-direct/range {v2 .. v7}, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;-><init>(Landroid/graphics/Typeface;Ljava/lang/String;Ljava/lang/String;FLjava/lang/String;)V

    .line 436
    .local v2, "f":Lorg/icepdf/core/pobjects/fonts/ofont/OFont;
    invoke-virtual {v2}, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->getFamily()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x2

    aget-object v4, v13, v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_18

    .line 437
    const/4 v3, 0x1

    aget-object v3, v13, v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->basefont:Ljava/lang/String;

    .line 446
    .end local v2    # "f":Lorg/icepdf/core/pobjects/fonts/ofont/OFont;
    .end local v13    # "aType1Diff":[Ljava/lang/String;
    .end local v14    # "arr$":[[Ljava/lang/String;
    .end local v27    # "i$":I
    .end local v28    # "len$":I
    :cond_f
    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->isFontSubstitution:Z

    .line 450
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->fontDescriptor:Lorg/icepdf/core/pobjects/fonts/FontDescriptor;

    if-eqz v3, :cond_10

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->fontDescriptor:Lorg/icepdf/core/pobjects/fonts/FontDescriptor;

    invoke-virtual {v3}, Lorg/icepdf/core/pobjects/fonts/FontDescriptor;->getEmbeddedFont()Lorg/icepdf/core/pobjects/fonts/FontFile;

    move-result-object v3

    if-eqz v3, :cond_10

    .line 451
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->fontDescriptor:Lorg/icepdf/core/pobjects/fonts/FontDescriptor;

    invoke-virtual {v3}, Lorg/icepdf/core/pobjects/fonts/FontDescriptor;->getEmbeddedFont()Lorg/icepdf/core/pobjects/fonts/FontFile;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->font:Lorg/icepdf/core/pobjects/fonts/FontFile;

    .line 452
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->isFontSubstitution:Z

    .line 453
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->isAFMFont:Z

    .line 459
    :cond_10
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->font:Lorg/icepdf/core/pobjects/fonts/FontFile;

    if-nez v3, :cond_11

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->basefont:Ljava/lang/String;

    if-eqz v3, :cond_11

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->basefont:Ljava/lang/String;

    const-string/jumbo v4, "-"

    invoke-virtual {v3, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_11

    .line 461
    new-instance v3, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->basefont:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->originalFontName:Ljava/lang/String;

    invoke-direct {v3, v4, v5}, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->font:Lorg/icepdf/core/pobjects/fonts/FontFile;

    .line 462
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->font:Lorg/icepdf/core/pobjects/fonts/FontFile;

    invoke-interface {v3}, Lorg/icepdf/core/pobjects/fonts/FontFile;->getName()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->basefont:Ljava/lang/String;

    .line 467
    :cond_11
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->font:Lorg/icepdf/core/pobjects/fonts/FontFile;

    if-nez v3, :cond_12

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->basefont:Ljava/lang/String;

    if-eqz v3, :cond_12

    .line 468
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->basefont:Ljava/lang/String;

    invoke-static {v3}, Lorg/icepdf/core/util/FontUtil;->guessFamily(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 469
    .local v6, "fontFamily":Ljava/lang/String;
    new-instance v3, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->basefont:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v5, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->style:I

    invoke-static {v4, v5}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->basefont:Ljava/lang/String;

    const/high16 v7, 0x41400000    # 12.0f

    move-object/from16 v0, p0

    iget-object v8, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->originalFontName:Ljava/lang/String;

    invoke-direct/range {v3 .. v8}, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;-><init>(Landroid/graphics/Typeface;Ljava/lang/String;Ljava/lang/String;FLjava/lang/String;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->font:Lorg/icepdf/core/pobjects/fonts/FontFile;

    .line 471
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->font:Lorg/icepdf/core/pobjects/fonts/FontFile;

    invoke-interface {v3}, Lorg/icepdf/core/pobjects/fonts/FontFile;->getName()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->basefont:Ljava/lang/String;

    .line 472
    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->isFontSubstitution:Z

    .line 476
    .end local v6    # "fontFamily":Ljava/lang/String;
    :cond_12
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->font:Lorg/icepdf/core/pobjects/fonts/FontFile;

    if-nez v3, :cond_13

    .line 478
    new-instance v7, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->basefont:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v4, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->style:I

    invoke-static {v3, v4}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v9, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->basefont:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->basefont:Ljava/lang/String;

    const/high16 v11, 0x41400000    # 12.0f

    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->originalFontName:Ljava/lang/String;

    invoke-direct/range {v7 .. v12}, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;-><init>(Landroid/graphics/Typeface;Ljava/lang/String;Ljava/lang/String;FLjava/lang/String;)V

    move-object/from16 v0, p0

    iput-object v7, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->font:Lorg/icepdf/core/pobjects/fonts/FontFile;

    .line 480
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->font:Lorg/icepdf/core/pobjects/fonts/FontFile;

    invoke-interface {v3}, Lorg/icepdf/core/pobjects/fonts/FontFile;->getName()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->basefont:Ljava/lang/String;

    .line 488
    :cond_13
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->isFontSubstitution:Z

    if-nez v3, :cond_15

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->font:Lorg/icepdf/core/pobjects/fonts/FontFile;

    invoke-interface {v3}, Lorg/icepdf/core/pobjects/fonts/FontFile;->getName()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v3, v4}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->font:Lorg/icepdf/core/pobjects/fonts/FontFile;

    invoke-interface {v4}, Lorg/icepdf/core/pobjects/fonts/FontFile;->getFamily()Ljava/lang/String;

    move-result-object v4

    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v4, v5}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    if-gez v3, :cond_15

    .line 491
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->font:Lorg/icepdf/core/pobjects/fonts/FontFile;

    invoke-interface {v3}, Lorg/icepdf/core/pobjects/fonts/FontFile;->getName()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v3, v4}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "times new roman"

    invoke-virtual {v3, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    const/4 v4, -0x1

    if-ne v3, v4, :cond_14

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->font:Lorg/icepdf/core/pobjects/fonts/FontFile;

    invoke-interface {v3}, Lorg/icepdf/core/pobjects/fonts/FontFile;->getName()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v3, v4}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "timesnewroman"

    invoke-virtual {v3, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    const/4 v4, -0x1

    if-ne v3, v4, :cond_14

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->font:Lorg/icepdf/core/pobjects/fonts/FontFile;

    invoke-interface {v3}, Lorg/icepdf/core/pobjects/fonts/FontFile;->getName()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v3, v4}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "bodoni"

    invoke-virtual {v3, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    const/4 v4, -0x1

    if-ne v3, v4, :cond_14

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->font:Lorg/icepdf/core/pobjects/fonts/FontFile;

    invoke-interface {v3}, Lorg/icepdf/core/pobjects/fonts/FontFile;->getName()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v3, v4}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "garamond"

    invoke-virtual {v3, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    const/4 v4, -0x1

    if-ne v3, v4, :cond_14

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->font:Lorg/icepdf/core/pobjects/fonts/FontFile;

    invoke-interface {v3}, Lorg/icepdf/core/pobjects/fonts/FontFile;->getName()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v3, v4}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "minion web"

    invoke-virtual {v3, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    const/4 v4, -0x1

    if-ne v3, v4, :cond_14

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->font:Lorg/icepdf/core/pobjects/fonts/FontFile;

    invoke-interface {v3}, Lorg/icepdf/core/pobjects/fonts/FontFile;->getName()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v3, v4}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "stone serif"

    invoke-virtual {v3, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    const/4 v4, -0x1

    if-ne v3, v4, :cond_14

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->font:Lorg/icepdf/core/pobjects/fonts/FontFile;

    invoke-interface {v3}, Lorg/icepdf/core/pobjects/fonts/FontFile;->getName()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v3, v4}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "stoneserif"

    invoke-virtual {v3, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    const/4 v4, -0x1

    if-ne v3, v4, :cond_14

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->font:Lorg/icepdf/core/pobjects/fonts/FontFile;

    invoke-interface {v3}, Lorg/icepdf/core/pobjects/fonts/FontFile;->getName()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v3, v4}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "georgia"

    invoke-virtual {v3, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    const/4 v4, -0x1

    if-ne v3, v4, :cond_14

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->font:Lorg/icepdf/core/pobjects/fonts/FontFile;

    invoke-interface {v3}, Lorg/icepdf/core/pobjects/fonts/FontFile;->getName()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v3, v4}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "bitstream cyberbit"

    invoke-virtual {v3, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_19

    .line 500
    :cond_14
    const-string/jumbo v3, "serif"

    move-object/from16 v0, p0

    iput-object v3, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->basefont:Ljava/lang/String;

    .line 501
    new-instance v7, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->basefont:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v4, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->style:I

    invoke-static {v3, v4}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v9, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->basefont:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->basefont:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->font:Lorg/icepdf/core/pobjects/fonts/FontFile;

    invoke-interface {v3}, Lorg/icepdf/core/pobjects/fonts/FontFile;->getSize()F

    move-result v11

    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->originalFontName:Ljava/lang/String;

    invoke-direct/range {v7 .. v12}, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;-><init>(Landroid/graphics/Typeface;Ljava/lang/String;Ljava/lang/String;FLjava/lang/String;)V

    move-object/from16 v0, p0

    iput-object v7, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->font:Lorg/icepdf/core/pobjects/fonts/FontFile;

    .line 540
    :cond_15
    :goto_6
    invoke-direct/range {p0 .. p0}, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->setWidth()V

    .line 541
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->font:Lorg/icepdf/core/pobjects/fonts/FontFile;

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->encoding:Lorg/icepdf/core/pobjects/fonts/ofont/Encoding;

    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->toUnicodeCMap:Lorg/icepdf/core/pobjects/fonts/ofont/CMap;

    invoke-interface {v3, v4, v5}, Lorg/icepdf/core/pobjects/fonts/FontFile;->deriveFont(Lorg/icepdf/core/pobjects/fonts/Encoding;Lorg/icepdf/core/pobjects/fonts/CMap;)Lorg/icepdf/core/pobjects/fonts/FontFile;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->font:Lorg/icepdf/core/pobjects/fonts/FontFile;

    .line 548
    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->inited:Z

    goto/16 :goto_0

    .line 326
    .end local v19    # "desendantFont":Ljava/lang/Object;
    :cond_16
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->library:Lorg/icepdf/core/util/Library;

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->entries:Ljava/util/Hashtable;

    const-string/jumbo v5, "W"

    invoke-virtual {v3, v4, v5}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_17

    .line 328
    invoke-direct/range {p0 .. p0}, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->calculateCIDWidths()Ljava/util/Map;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->cidWidths:Ljava/util/Map;

    .line 330
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput v3, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->firstchar:I

    .line 332
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->isAFMFont:Z

    goto/16 :goto_4

    .line 335
    :cond_17
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->isAFMFont:Z

    goto/16 :goto_4

    .line 422
    .restart local v13    # "aType1Diff":[Ljava/lang/String;
    .restart local v14    # "arr$":[[Ljava/lang/String;
    .restart local v19    # "desendantFont":Ljava/lang/Object;
    .restart local v27    # "i$":I
    .restart local v28    # "len$":I
    :cond_18
    add-int/lit8 v27, v27, 0x1

    goto/16 :goto_5

    .line 504
    .end local v13    # "aType1Diff":[Ljava/lang/String;
    .end local v14    # "arr$":[[Ljava/lang/String;
    .end local v27    # "i$":I
    .end local v28    # "len$":I
    :cond_19
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->font:Lorg/icepdf/core/pobjects/fonts/FontFile;

    invoke-interface {v3}, Lorg/icepdf/core/pobjects/fonts/FontFile;->getName()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v3, v4}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "helvetica"

    invoke-virtual {v3, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    const/4 v4, -0x1

    if-ne v3, v4, :cond_1a

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->font:Lorg/icepdf/core/pobjects/fonts/FontFile;

    invoke-interface {v3}, Lorg/icepdf/core/pobjects/fonts/FontFile;->getName()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v3, v4}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "arial"

    invoke-virtual {v3, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    const/4 v4, -0x1

    if-ne v3, v4, :cond_1a

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->font:Lorg/icepdf/core/pobjects/fonts/FontFile;

    invoke-interface {v3}, Lorg/icepdf/core/pobjects/fonts/FontFile;->getName()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v3, v4}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "trebuchet"

    invoke-virtual {v3, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    const/4 v4, -0x1

    if-ne v3, v4, :cond_1a

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->font:Lorg/icepdf/core/pobjects/fonts/FontFile;

    invoke-interface {v3}, Lorg/icepdf/core/pobjects/fonts/FontFile;->getName()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v3, v4}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "avant garde gothic"

    invoke-virtual {v3, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    const/4 v4, -0x1

    if-ne v3, v4, :cond_1a

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->font:Lorg/icepdf/core/pobjects/fonts/FontFile;

    invoke-interface {v3}, Lorg/icepdf/core/pobjects/fonts/FontFile;->getName()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v3, v4}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "avantgardegothic"

    invoke-virtual {v3, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    const/4 v4, -0x1

    if-ne v3, v4, :cond_1a

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->font:Lorg/icepdf/core/pobjects/fonts/FontFile;

    invoke-interface {v3}, Lorg/icepdf/core/pobjects/fonts/FontFile;->getName()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v3, v4}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "verdana"

    invoke-virtual {v3, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    const/4 v4, -0x1

    if-ne v3, v4, :cond_1a

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->font:Lorg/icepdf/core/pobjects/fonts/FontFile;

    invoke-interface {v3}, Lorg/icepdf/core/pobjects/fonts/FontFile;->getName()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v3, v4}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "univers"

    invoke-virtual {v3, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    const/4 v4, -0x1

    if-ne v3, v4, :cond_1a

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->font:Lorg/icepdf/core/pobjects/fonts/FontFile;

    invoke-interface {v3}, Lorg/icepdf/core/pobjects/fonts/FontFile;->getName()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v3, v4}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "futura"

    invoke-virtual {v3, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    const/4 v4, -0x1

    if-ne v3, v4, :cond_1a

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->font:Lorg/icepdf/core/pobjects/fonts/FontFile;

    invoke-interface {v3}, Lorg/icepdf/core/pobjects/fonts/FontFile;->getName()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v3, v4}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "stone sans"

    invoke-virtual {v3, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    const/4 v4, -0x1

    if-ne v3, v4, :cond_1a

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->font:Lorg/icepdf/core/pobjects/fonts/FontFile;

    invoke-interface {v3}, Lorg/icepdf/core/pobjects/fonts/FontFile;->getName()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v3, v4}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "stonesans"

    invoke-virtual {v3, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    const/4 v4, -0x1

    if-ne v3, v4, :cond_1a

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->font:Lorg/icepdf/core/pobjects/fonts/FontFile;

    invoke-interface {v3}, Lorg/icepdf/core/pobjects/fonts/FontFile;->getName()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v3, v4}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "gill sans"

    invoke-virtual {v3, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    const/4 v4, -0x1

    if-ne v3, v4, :cond_1a

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->font:Lorg/icepdf/core/pobjects/fonts/FontFile;

    invoke-interface {v3}, Lorg/icepdf/core/pobjects/fonts/FontFile;->getName()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v3, v4}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "gillsans"

    invoke-virtual {v3, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    const/4 v4, -0x1

    if-ne v3, v4, :cond_1a

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->font:Lorg/icepdf/core/pobjects/fonts/FontFile;

    invoke-interface {v3}, Lorg/icepdf/core/pobjects/fonts/FontFile;->getName()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v3, v4}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "akzidenz"

    invoke-virtual {v3, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    const/4 v4, -0x1

    if-ne v3, v4, :cond_1a

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->font:Lorg/icepdf/core/pobjects/fonts/FontFile;

    invoke-interface {v3}, Lorg/icepdf/core/pobjects/fonts/FontFile;->getName()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v3, v4}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "grotesk"

    invoke-virtual {v3, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_1b

    .line 518
    :cond_1a
    const-string/jumbo v3, "sansserif"

    move-object/from16 v0, p0

    iput-object v3, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->basefont:Ljava/lang/String;

    .line 519
    new-instance v7, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->basefont:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v4, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->style:I

    invoke-static {v3, v4}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v9, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->basefont:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->basefont:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->font:Lorg/icepdf/core/pobjects/fonts/FontFile;

    invoke-interface {v3}, Lorg/icepdf/core/pobjects/fonts/FontFile;->getSize()F

    move-result v11

    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->originalFontName:Ljava/lang/String;

    invoke-direct/range {v7 .. v12}, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;-><init>(Landroid/graphics/Typeface;Ljava/lang/String;Ljava/lang/String;FLjava/lang/String;)V

    move-object/from16 v0, p0

    iput-object v7, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->font:Lorg/icepdf/core/pobjects/fonts/FontFile;

    goto/16 :goto_6

    .line 522
    :cond_1b
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->font:Lorg/icepdf/core/pobjects/fonts/FontFile;

    invoke-interface {v3}, Lorg/icepdf/core/pobjects/fonts/FontFile;->getName()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v3, v4}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "courier"

    invoke-virtual {v3, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    const/4 v4, -0x1

    if-ne v3, v4, :cond_1c

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->font:Lorg/icepdf/core/pobjects/fonts/FontFile;

    invoke-interface {v3}, Lorg/icepdf/core/pobjects/fonts/FontFile;->getName()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v3, v4}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "courier new"

    invoke-virtual {v3, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    const/4 v4, -0x1

    if-ne v3, v4, :cond_1c

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->font:Lorg/icepdf/core/pobjects/fonts/FontFile;

    invoke-interface {v3}, Lorg/icepdf/core/pobjects/fonts/FontFile;->getName()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v3, v4}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "couriernew"

    invoke-virtual {v3, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    const/4 v4, -0x1

    if-ne v3, v4, :cond_1c

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->font:Lorg/icepdf/core/pobjects/fonts/FontFile;

    invoke-interface {v3}, Lorg/icepdf/core/pobjects/fonts/FontFile;->getName()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v3, v4}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "prestige"

    invoke-virtual {v3, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    const/4 v4, -0x1

    if-ne v3, v4, :cond_1c

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->font:Lorg/icepdf/core/pobjects/fonts/FontFile;

    invoke-interface {v3}, Lorg/icepdf/core/pobjects/fonts/FontFile;->getName()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v3, v4}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "eversonmono"

    invoke-virtual {v3, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    const/4 v4, -0x1

    if-ne v3, v4, :cond_1c

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->font:Lorg/icepdf/core/pobjects/fonts/FontFile;

    invoke-interface {v3}, Lorg/icepdf/core/pobjects/fonts/FontFile;->getName()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v3, v4}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "Everson Mono"

    invoke-virtual {v3, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_1d

    .line 528
    :cond_1c
    const-string/jumbo v3, "monospaced"

    move-object/from16 v0, p0

    iput-object v3, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->basefont:Ljava/lang/String;

    .line 529
    new-instance v7, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->basefont:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v4, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->style:I

    invoke-static {v3, v4}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v9, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->basefont:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->basefont:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->font:Lorg/icepdf/core/pobjects/fonts/FontFile;

    invoke-interface {v3}, Lorg/icepdf/core/pobjects/fonts/FontFile;->getSize()F

    move-result v11

    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->originalFontName:Ljava/lang/String;

    invoke-direct/range {v7 .. v12}, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;-><init>(Landroid/graphics/Typeface;Ljava/lang/String;Ljava/lang/String;FLjava/lang/String;)V

    move-object/from16 v0, p0

    iput-object v7, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->font:Lorg/icepdf/core/pobjects/fonts/FontFile;

    goto/16 :goto_6

    .line 533
    :cond_1d
    const-string/jumbo v3, "serif"

    move-object/from16 v0, p0

    iput-object v3, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->basefont:Ljava/lang/String;

    .line 534
    new-instance v7, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->basefont:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v4, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->style:I

    invoke-static {v3, v4}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v9, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->basefont:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->basefont:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->font:Lorg/icepdf/core/pobjects/fonts/FontFile;

    invoke-interface {v3}, Lorg/icepdf/core/pobjects/fonts/FontFile;->getSize()F

    move-result v11

    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->originalFontName:Ljava/lang/String;

    invoke-direct/range {v7 .. v12}, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;-><init>(Landroid/graphics/Typeface;Ljava/lang/String;Ljava/lang/String;FLjava/lang/String;)V

    move-object/from16 v0, p0

    iput-object v7, v0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->font:Lorg/icepdf/core/pobjects/fonts/FontFile;

    goto/16 :goto_6

    .line 300
    .end local v19    # "desendantFont":Ljava/lang/Object;
    .restart local v15    # "c":I
    .restart local v16    # "c1":I
    .restart local v20    # "differences":Ljava/util/Vector;
    .restart local v21    # "e":Ljava/util/Enumeration;
    .restart local v22    # "encoding":Ljava/util/Hashtable;
    .restart local v29    # "n":Ljava/lang/String;
    .restart local v33    # "oo":Ljava/lang/Object;
    :catch_0
    move-exception v3

    goto/16 :goto_3
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 589
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "FONT= "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->encodingName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/icepdf/core/pobjects/fonts/ofont/Font;->entries:Ljava/util/Hashtable;

    invoke-virtual {v1}, Ljava/util/Hashtable;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
