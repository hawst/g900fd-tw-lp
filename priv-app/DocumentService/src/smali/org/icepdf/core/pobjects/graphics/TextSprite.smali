.class public Lorg/icepdf/core/pobjects/graphics/TextSprite;
.super Ljava/lang/Object;
.source "TextSprite.java"


# static fields
.field private static final OPTIMIZED_DRAWING_ENABLED:Z


# instance fields
.field bounds:Landroid/graphics/RectF;

.field private font:Lorg/icepdf/core/pobjects/fonts/FontFile;

.field private glyphTexts:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lorg/icepdf/core/pobjects/graphics/text/GlyphText;",
            ">;"
        }
    .end annotation
.end field

.field private graphicStateTransform:Landroid/graphics/Matrix;

.field private rmode:I

.field private strokeColor:Lorg/apache/poi/java/awt/Color;

.field private strokePaint:Landroid/graphics/Paint;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 45
    const-string/jumbo v0, "org.icepdf.core.text.optimized"

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lorg/icepdf/core/util/Defs;->booleanProperty(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lorg/icepdf/core/pobjects/graphics/TextSprite;->OPTIMIZED_DRAWING_ENABLED:Z

    return-void
.end method

.method public constructor <init>(Lorg/icepdf/core/pobjects/fonts/FontFile;ILandroid/graphics/Matrix;)V
    .locals 1
    .param p1, "font"    # Lorg/icepdf/core/pobjects/fonts/FontFile;
    .param p2, "size"    # I
    .param p3, "graphicStateTransform"    # Landroid/graphics/Matrix;

    .prologue
    .line 72
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 73
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lorg/icepdf/core/pobjects/graphics/TextSprite;->glyphTexts:Ljava/util/ArrayList;

    .line 75
    iput-object p3, p0, Lorg/icepdf/core/pobjects/graphics/TextSprite;->graphicStateTransform:Landroid/graphics/Matrix;

    .line 76
    iput-object p1, p0, Lorg/icepdf/core/pobjects/graphics/TextSprite;->font:Lorg/icepdf/core/pobjects/fonts/FontFile;

    .line 77
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lorg/icepdf/core/pobjects/graphics/TextSprite;->bounds:Landroid/graphics/RectF;

    .line 78
    return-void
.end method


# virtual methods
.method public addText(Ljava/lang/String;Ljava/lang/String;FFF)Lorg/icepdf/core/pobjects/graphics/text/GlyphText;
    .locals 16
    .param p1, "cid"    # Ljava/lang/String;
    .param p2, "unicode"    # Ljava/lang/String;
    .param p3, "x"    # F
    .param p4, "y"    # F
    .param p5, "width"    # F

    .prologue
    .line 95
    move/from16 v12, p5

    .line 97
    .local v12, "w":F
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/icepdf/core/pobjects/graphics/TextSprite;->font:Lorg/icepdf/core/pobjects/fonts/FontFile;

    invoke-interface {v3}, Lorg/icepdf/core/pobjects/fonts/FontFile;->getAscent()D

    move-result-wide v6

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/icepdf/core/pobjects/graphics/TextSprite;->font:Lorg/icepdf/core/pobjects/fonts/FontFile;

    invoke-interface {v3}, Lorg/icepdf/core/pobjects/fonts/FontFile;->getDescent()D

    move-result-wide v14

    add-double/2addr v6, v14

    double-to-float v8, v6

    .line 99
    .local v8, "h":F
    const/4 v3, 0x0

    cmpg-float v3, v8, v3

    if-gtz v3, :cond_0

    .line 100
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/icepdf/core/pobjects/graphics/TextSprite;->font:Lorg/icepdf/core/pobjects/fonts/FontFile;

    invoke-interface {v3}, Lorg/icepdf/core/pobjects/fonts/FontFile;->getMaxCharBounds()Landroid/graphics/RectF;

    move-result-object v9

    .line 102
    .local v9, "rect":Landroid/graphics/RectF;
    iget v3, v9, Landroid/graphics/RectF;->bottom:F

    iget v4, v9, Landroid/graphics/RectF;->top:F

    sub-float v8, v3, v4

    .line 104
    .end local v9    # "rect":Landroid/graphics/RectF;
    :cond_0
    const/4 v3, 0x0

    cmpg-float v3, v12, v3

    if-gtz v3, :cond_1

    .line 105
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/icepdf/core/pobjects/graphics/TextSprite;->font:Lorg/icepdf/core/pobjects/fonts/FontFile;

    invoke-interface {v3}, Lorg/icepdf/core/pobjects/fonts/FontFile;->getMaxCharBounds()Landroid/graphics/RectF;

    move-result-object v9

    .line 106
    .restart local v9    # "rect":Landroid/graphics/RectF;
    iget v3, v9, Landroid/graphics/RectF;->right:F

    iget v4, v9, Landroid/graphics/RectF;->left:F

    sub-float v12, v3, v4

    .line 110
    .end local v9    # "rect":Landroid/graphics/RectF;
    :cond_1
    const/4 v3, 0x0

    cmpg-float v3, v8, v3

    if-gtz v3, :cond_2

    .line 111
    const/high16 v8, 0x3f800000    # 1.0f

    .line 113
    :cond_2
    const/4 v3, 0x0

    cmpg-float v3, v12, v3

    if-gtz v3, :cond_3

    .line 114
    const/high16 v12, 0x3f800000    # 1.0f

    .line 116
    :cond_3
    new-instance v5, Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/icepdf/core/pobjects/graphics/TextSprite;->font:Lorg/icepdf/core/pobjects/fonts/FontFile;

    invoke-interface {v3}, Lorg/icepdf/core/pobjects/fonts/FontFile;->getAscent()D

    move-result-wide v6

    double-to-float v3, v6

    sub-float v3, p4, v3

    add-float v4, p3, v12

    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/icepdf/core/pobjects/graphics/TextSprite;->font:Lorg/icepdf/core/pobjects/fonts/FontFile;

    invoke-interface {v6}, Lorg/icepdf/core/pobjects/fonts/FontFile;->getAscent()D

    move-result-wide v6

    double-to-float v6, v6

    sub-float v6, p4, v6

    add-float/2addr v6, v8

    move/from16 v0, p3

    invoke-direct {v5, v0, v3, v4, v6}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 120
    .local v5, "glyphBounds":Landroid/graphics/RectF;
    move-object v13, v5

    .line 122
    .local v13, "wordBounds":Landroid/graphics/RectF;
    new-instance v10, Lorg/apache/poi/java/awt/geom/Rectangle2D$Float;

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/icepdf/core/pobjects/graphics/TextSprite;->bounds:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->left:F

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/icepdf/core/pobjects/graphics/TextSprite;->bounds:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->top:F

    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/icepdf/core/pobjects/graphics/TextSprite;->bounds:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->right:F

    move-object/from16 v0, p0

    iget-object v7, v0, Lorg/icepdf/core/pobjects/graphics/TextSprite;->bounds:Landroid/graphics/RectF;

    iget v7, v7, Landroid/graphics/RectF;->left:F

    sub-float/2addr v6, v7

    move-object/from16 v0, p0

    iget-object v7, v0, Lorg/icepdf/core/pobjects/graphics/TextSprite;->bounds:Landroid/graphics/RectF;

    iget v7, v7, Landroid/graphics/RectF;->bottom:F

    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/icepdf/core/pobjects/graphics/TextSprite;->bounds:Landroid/graphics/RectF;

    iget v14, v14, Landroid/graphics/RectF;->top:F

    sub-float/2addr v7, v14

    invoke-direct {v10, v3, v4, v6, v7}, Lorg/apache/poi/java/awt/geom/Rectangle2D$Float;-><init>(FFFF)V

    .line 125
    .local v10, "rect1":Lorg/apache/poi/java/awt/geom/Rectangle2D$Float;
    new-instance v11, Lorg/apache/poi/java/awt/geom/Rectangle2D$Float;

    iget v3, v13, Landroid/graphics/RectF;->left:F

    iget v4, v13, Landroid/graphics/RectF;->top:F

    iget v6, v13, Landroid/graphics/RectF;->right:F

    iget v7, v13, Landroid/graphics/RectF;->left:F

    sub-float/2addr v6, v7

    iget v7, v13, Landroid/graphics/RectF;->bottom:F

    iget v14, v13, Landroid/graphics/RectF;->top:F

    sub-float/2addr v7, v14

    invoke-direct {v11, v3, v4, v6, v7}, Lorg/apache/poi/java/awt/geom/Rectangle2D$Float;-><init>(FFFF)V

    .line 128
    .local v11, "rect2":Lorg/apache/poi/java/awt/geom/Rectangle2D$Float;
    invoke-virtual {v10, v11}, Lorg/apache/poi/java/awt/geom/Rectangle2D$Float;->add(Lorg/apache/poi/java/awt/geom/Rectangle2D;)V

    .line 130
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/icepdf/core/pobjects/graphics/TextSprite;->bounds:Landroid/graphics/RectF;

    iget v4, v10, Lorg/apache/poi/java/awt/geom/Rectangle2D$Float;->x:F

    iput v4, v3, Landroid/graphics/RectF;->left:F

    .line 131
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/icepdf/core/pobjects/graphics/TextSprite;->bounds:Landroid/graphics/RectF;

    iget v4, v10, Lorg/apache/poi/java/awt/geom/Rectangle2D$Float;->y:F

    iput v4, v3, Landroid/graphics/RectF;->top:F

    .line 132
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/icepdf/core/pobjects/graphics/TextSprite;->bounds:Landroid/graphics/RectF;

    iget v4, v10, Lorg/apache/poi/java/awt/geom/Rectangle2D$Float;->x:F

    iget v6, v10, Lorg/apache/poi/java/awt/geom/Rectangle2D$Float;->width:F

    add-float/2addr v4, v6

    iput v4, v3, Landroid/graphics/RectF;->right:F

    .line 133
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/icepdf/core/pobjects/graphics/TextSprite;->bounds:Landroid/graphics/RectF;

    iget v4, v10, Lorg/apache/poi/java/awt/geom/Rectangle2D$Float;->y:F

    iget v6, v10, Lorg/apache/poi/java/awt/geom/Rectangle2D$Float;->height:F

    add-float/2addr v4, v6

    iput v4, v3, Landroid/graphics/RectF;->bottom:F

    .line 136
    new-instance v2, Lorg/icepdf/core/pobjects/graphics/text/GlyphText;

    move/from16 v3, p3

    move/from16 v4, p4

    move-object/from16 v6, p1

    move-object/from16 v7, p2

    invoke-direct/range {v2 .. v7}, Lorg/icepdf/core/pobjects/graphics/text/GlyphText;-><init>(FFLandroid/graphics/RectF;Ljava/lang/String;Ljava/lang/String;)V

    .line 138
    .local v2, "glyphText":Lorg/icepdf/core/pobjects/graphics/text/GlyphText;
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/icepdf/core/pobjects/graphics/TextSprite;->graphicStateTransform:Landroid/graphics/Matrix;

    invoke-virtual {v2, v3}, Lorg/icepdf/core/pobjects/graphics/text/GlyphText;->normalizeToUserSpace(Landroid/graphics/Matrix;)V

    .line 139
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/icepdf/core/pobjects/graphics/TextSprite;->glyphTexts:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 140
    return-object v2
.end method

.method public dispose()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 371
    iget-object v0, p0, Lorg/icepdf/core/pobjects/graphics/TextSprite;->glyphTexts:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 372
    iget-object v0, p0, Lorg/icepdf/core/pobjects/graphics/TextSprite;->glyphTexts:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->trimToSize()V

    .line 373
    iput-object v1, p0, Lorg/icepdf/core/pobjects/graphics/TextSprite;->strokeColor:Lorg/apache/poi/java/awt/Color;

    .line 374
    iput-object v1, p0, Lorg/icepdf/core/pobjects/graphics/TextSprite;->font:Lorg/icepdf/core/pobjects/fonts/FontFile;

    .line 375
    return-void
.end method

.method public getBounds()Landroid/graphics/RectF;
    .locals 1

    .prologue
    .line 212
    iget-object v0, p0, Lorg/icepdf/core/pobjects/graphics/TextSprite;->bounds:Landroid/graphics/RectF;

    return-object v0
.end method

.method public getGlyphOutline()Landroid/graphics/Path;
    .locals 8

    .prologue
    .line 249
    const/4 v0, 0x0

    .line 250
    .local v0, "glyphOutline":Landroid/graphics/Path;
    const/4 v3, 0x0

    .line 251
    .local v3, "temp":Landroid/graphics/Path;
    iget-object v4, p0, Lorg/icepdf/core/pobjects/graphics/TextSprite;->glyphTexts:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/icepdf/core/pobjects/graphics/text/GlyphText;

    .line 254
    .local v1, "glyphText":Lorg/icepdf/core/pobjects/graphics/text/GlyphText;
    if-eqz v0, :cond_1

    .line 255
    iget-object v4, p0, Lorg/icepdf/core/pobjects/graphics/TextSprite;->font:Lorg/icepdf/core/pobjects/fonts/FontFile;

    invoke-virtual {v1}, Lorg/icepdf/core/pobjects/graphics/text/GlyphText;->getCid()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1}, Lorg/icepdf/core/pobjects/graphics/text/GlyphText;->getX()F

    move-result v6

    invoke-virtual {v1}, Lorg/icepdf/core/pobjects/graphics/text/GlyphText;->getY()F

    move-result v7

    invoke-interface {v4, v5, v6, v7}, Lorg/icepdf/core/pobjects/fonts/FontFile;->getEstringOutline(Ljava/lang/String;FF)Landroid/graphics/Path;

    move-result-object v3

    .line 258
    if-eqz v3, :cond_0

    .line 259
    new-instance v4, Landroid/graphics/Path;

    invoke-direct {v4, v3}, Landroid/graphics/Path;-><init>(Landroid/graphics/Path;)V

    invoke-virtual {v0, v4}, Landroid/graphics/Path;->addPath(Landroid/graphics/Path;)V

    goto :goto_0

    .line 263
    :cond_1
    iget-object v4, p0, Lorg/icepdf/core/pobjects/graphics/TextSprite;->font:Lorg/icepdf/core/pobjects/fonts/FontFile;

    invoke-virtual {v1}, Lorg/icepdf/core/pobjects/graphics/text/GlyphText;->getCid()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1}, Lorg/icepdf/core/pobjects/graphics/text/GlyphText;->getX()F

    move-result v6

    invoke-virtual {v1}, Lorg/icepdf/core/pobjects/graphics/text/GlyphText;->getY()F

    move-result v7

    invoke-interface {v4, v5, v6, v7}, Lorg/icepdf/core/pobjects/fonts/FontFile;->getEstringOutline(Ljava/lang/String;FF)Landroid/graphics/Path;

    move-result-object v3

    .line 266
    if-eqz v3, :cond_0

    .line 267
    new-instance v0, Landroid/graphics/Path;

    .end local v0    # "glyphOutline":Landroid/graphics/Path;
    invoke-direct {v0, v3}, Landroid/graphics/Path;-><init>(Landroid/graphics/Path;)V

    .restart local v0    # "glyphOutline":Landroid/graphics/Path;
    goto :goto_0

    .line 271
    .end local v1    # "glyphText":Lorg/icepdf/core/pobjects/graphics/text/GlyphText;
    :cond_2
    return-object v0
.end method

.method public getGlyphSprites()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lorg/icepdf/core/pobjects/graphics/text/GlyphText;",
            ">;"
        }
    .end annotation

    .prologue
    .line 148
    iget-object v0, p0, Lorg/icepdf/core/pobjects/graphics/TextSprite;->glyphTexts:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getGraphicStateTransform()Landroid/graphics/Matrix;
    .locals 1

    .prologue
    .line 152
    iget-object v0, p0, Lorg/icepdf/core/pobjects/graphics/TextSprite;->graphicStateTransform:Landroid/graphics/Matrix;

    return-object v0
.end method

.method public intersects(Landroid/graphics/Path;)Z
    .locals 5
    .param p1, "shape"    # Landroid/graphics/Path;

    .prologue
    const/4 v2, 0x1

    .line 334
    const/4 v0, 0x0

    .line 335
    .local v0, "isIntersecting":Z
    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    .line 336
    .local v1, "shapeBound":Landroid/graphics/RectF;
    invoke-virtual {p1, v1, v2}, Landroid/graphics/Path;->computeBounds(Landroid/graphics/RectF;Z)V

    .line 338
    iget v3, v1, Landroid/graphics/RectF;->left:F

    iget-object v4, p0, Lorg/icepdf/core/pobjects/graphics/TextSprite;->bounds:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->left:F

    cmpl-float v3, v3, v4

    if-ltz v3, :cond_0

    iget v3, v1, Landroid/graphics/RectF;->left:F

    iget-object v4, p0, Lorg/icepdf/core/pobjects/graphics/TextSprite;->bounds:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->right:F

    cmpg-float v3, v3, v4

    if-lez v3, :cond_1

    :cond_0
    iget v3, v1, Landroid/graphics/RectF;->right:F

    iget-object v4, p0, Lorg/icepdf/core/pobjects/graphics/TextSprite;->bounds:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->left:F

    cmpl-float v3, v3, v4

    if-ltz v3, :cond_4

    iget v3, v1, Landroid/graphics/RectF;->right:F

    iget-object v4, p0, Lorg/icepdf/core/pobjects/graphics/TextSprite;->bounds:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->right:F

    cmpg-float v3, v3, v4

    if-gtz v3, :cond_4

    :cond_1
    iget v3, v1, Landroid/graphics/RectF;->top:F

    iget-object v4, p0, Lorg/icepdf/core/pobjects/graphics/TextSprite;->bounds:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->top:F

    cmpl-float v3, v3, v4

    if-ltz v3, :cond_2

    iget v3, v1, Landroid/graphics/RectF;->top:F

    iget-object v4, p0, Lorg/icepdf/core/pobjects/graphics/TextSprite;->bounds:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->bottom:F

    cmpg-float v3, v3, v4

    if-lez v3, :cond_3

    :cond_2
    iget v3, v1, Landroid/graphics/RectF;->bottom:F

    iget-object v4, p0, Lorg/icepdf/core/pobjects/graphics/TextSprite;->bounds:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->top:F

    cmpl-float v3, v3, v4

    if-ltz v3, :cond_4

    iget v3, v1, Landroid/graphics/RectF;->bottom:F

    iget-object v4, p0, Lorg/icepdf/core/pobjects/graphics/TextSprite;->bounds:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->bottom:F

    cmpg-float v3, v3, v4

    if-gtz v3, :cond_4

    .line 346
    :cond_3
    const/4 v0, 0x1

    .line 349
    :cond_4
    if-nez v0, :cond_9

    .line 351
    iget-object v3, p0, Lorg/icepdf/core/pobjects/graphics/TextSprite;->bounds:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->left:F

    iget v4, v1, Landroid/graphics/RectF;->left:F

    cmpl-float v3, v3, v4

    if-ltz v3, :cond_5

    iget-object v3, p0, Lorg/icepdf/core/pobjects/graphics/TextSprite;->bounds:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->left:F

    iget v4, v1, Landroid/graphics/RectF;->right:F

    cmpg-float v3, v3, v4

    if-lez v3, :cond_6

    :cond_5
    iget-object v3, p0, Lorg/icepdf/core/pobjects/graphics/TextSprite;->bounds:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->right:F

    iget v4, v1, Landroid/graphics/RectF;->left:F

    cmpl-float v3, v3, v4

    if-ltz v3, :cond_9

    iget-object v3, p0, Lorg/icepdf/core/pobjects/graphics/TextSprite;->bounds:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->right:F

    iget v4, v1, Landroid/graphics/RectF;->right:F

    cmpg-float v3, v3, v4

    if-gtz v3, :cond_9

    :cond_6
    iget-object v3, p0, Lorg/icepdf/core/pobjects/graphics/TextSprite;->bounds:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->top:F

    iget v4, v1, Landroid/graphics/RectF;->top:F

    cmpl-float v3, v3, v4

    if-ltz v3, :cond_7

    iget-object v3, p0, Lorg/icepdf/core/pobjects/graphics/TextSprite;->bounds:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->top:F

    iget v4, v1, Landroid/graphics/RectF;->bottom:F

    cmpg-float v3, v3, v4

    if-lez v3, :cond_8

    :cond_7
    iget-object v3, p0, Lorg/icepdf/core/pobjects/graphics/TextSprite;->bounds:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->bottom:F

    iget v4, v1, Landroid/graphics/RectF;->top:F

    cmpl-float v3, v3, v4

    if-ltz v3, :cond_9

    iget-object v3, p0, Lorg/icepdf/core/pobjects/graphics/TextSprite;->bounds:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->bottom:F

    iget v4, v1, Landroid/graphics/RectF;->bottom:F

    cmpg-float v3, v3, v4

    if-gtz v3, :cond_9

    .line 358
    :cond_8
    const/4 v0, 0x1

    .line 363
    :cond_9
    sget-boolean v3, Lorg/icepdf/core/pobjects/graphics/TextSprite;->OPTIMIZED_DRAWING_ENABLED:Z

    if-eqz v3, :cond_a

    if-eqz v0, :cond_b

    :cond_a
    :goto_0
    return v2

    :cond_b
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public paint(Landroid/graphics/Canvas;)V
    .locals 12
    .param p1, "g"    # Landroid/graphics/Canvas;

    .prologue
    .line 222
    move-object v2, p1

    .line 227
    .local v2, "g2d":Landroid/graphics/Canvas;
    iget-object v1, p0, Lorg/icepdf/core/pobjects/graphics/TextSprite;->glyphTexts:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v11

    .local v11, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/icepdf/core/pobjects/graphics/text/GlyphText;

    .line 230
    .local v0, "glyphText":Lorg/icepdf/core/pobjects/graphics/text/GlyphText;
    iget-object v1, p0, Lorg/icepdf/core/pobjects/graphics/TextSprite;->font:Lorg/icepdf/core/pobjects/fonts/FontFile;

    invoke-virtual {v0}, Lorg/icepdf/core/pobjects/graphics/text/GlyphText;->getCid()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lorg/icepdf/core/pobjects/graphics/text/GlyphText;->getX()F

    move-result v4

    invoke-virtual {v0}, Lorg/icepdf/core/pobjects/graphics/text/GlyphText;->getY()F

    move-result v5

    const-wide/16 v6, 0x0

    iget v8, p0, Lorg/icepdf/core/pobjects/graphics/TextSprite;->rmode:I

    iget-object v9, p0, Lorg/icepdf/core/pobjects/graphics/TextSprite;->strokeColor:Lorg/apache/poi/java/awt/Color;

    iget-object v10, p0, Lorg/icepdf/core/pobjects/graphics/TextSprite;->strokePaint:Landroid/graphics/Paint;

    invoke-interface/range {v1 .. v10}, Lorg/icepdf/core/pobjects/fonts/FontFile;->drawEstring(Landroid/graphics/Canvas;Ljava/lang/String;FFJILorg/apache/poi/java/awt/Color;Landroid/graphics/Paint;)V

    goto :goto_0

    .line 239
    .end local v0    # "glyphText":Lorg/icepdf/core/pobjects/graphics/text/GlyphText;
    :cond_0
    return-void
.end method

.method public setGraphicStateTransform(Landroid/graphics/Matrix;)V
    .locals 3
    .param p1, "graphicStateTransform"    # Landroid/graphics/Matrix;

    .prologue
    .line 162
    iput-object p1, p0, Lorg/icepdf/core/pobjects/graphics/TextSprite;->graphicStateTransform:Landroid/graphics/Matrix;

    .line 163
    iget-object v2, p0, Lorg/icepdf/core/pobjects/graphics/TextSprite;->glyphTexts:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/icepdf/core/pobjects/graphics/text/GlyphText;

    .line 164
    .local v1, "sprite":Lorg/icepdf/core/pobjects/graphics/text/GlyphText;
    iget-object v2, p0, Lorg/icepdf/core/pobjects/graphics/TextSprite;->graphicStateTransform:Landroid/graphics/Matrix;

    invoke-virtual {v1, v2}, Lorg/icepdf/core/pobjects/graphics/text/GlyphText;->normalizeToUserSpace(Landroid/graphics/Matrix;)V

    goto :goto_0

    .line 166
    .end local v1    # "sprite":Lorg/icepdf/core/pobjects/graphics/text/GlyphText;
    :cond_0
    return-void
.end method

.method public setRMode(I)V
    .locals 0
    .param p1, "rmode"    # I

    .prologue
    .line 185
    if-ltz p1, :cond_0

    .line 186
    iput p1, p0, Lorg/icepdf/core/pobjects/graphics/TextSprite;->rmode:I

    .line 188
    :cond_0
    return-void
.end method

.method public setStrokeColor(Lorg/apache/poi/java/awt/Color;)V
    .locals 0
    .param p1, "color"    # Lorg/apache/poi/java/awt/Color;

    .prologue
    .line 199
    iput-object p1, p0, Lorg/icepdf/core/pobjects/graphics/TextSprite;->strokeColor:Lorg/apache/poi/java/awt/Color;

    .line 200
    return-void
.end method

.method public setStrokePaint(Landroid/graphics/Paint;)V
    .locals 0
    .param p1, "paint"    # Landroid/graphics/Paint;

    .prologue
    .line 203
    iput-object p1, p0, Lorg/icepdf/core/pobjects/graphics/TextSprite;->strokePaint:Landroid/graphics/Paint;

    .line 204
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 191
    new-instance v2, Ljava/lang/StringBuilder;

    iget-object v3, p0, Lorg/icepdf/core/pobjects/graphics/TextSprite;->glyphTexts:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 192
    .local v2, "text":Ljava/lang/StringBuilder;
    iget-object v3, p0, Lorg/icepdf/core/pobjects/graphics/TextSprite;->glyphTexts:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/icepdf/core/pobjects/graphics/text/GlyphText;

    .line 193
    .local v0, "glyphText":Lorg/icepdf/core/pobjects/graphics/text/GlyphText;
    invoke-virtual {v0}, Lorg/icepdf/core/pobjects/graphics/text/GlyphText;->getUnicode()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 195
    .end local v0    # "glyphText":Lorg/icepdf/core/pobjects/graphics/text/GlyphText;
    :cond_0
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method
