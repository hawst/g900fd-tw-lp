.class public Lorg/icepdf/core/pobjects/Destination;
.super Ljava/lang/Object;
.source "Destination.java"


# static fields
.field public static final TYPE_FIT:Lorg/icepdf/core/pobjects/Name;

.field public static final TYPE_FITB:Lorg/icepdf/core/pobjects/Name;

.field public static final TYPE_FITBH:Lorg/icepdf/core/pobjects/Name;

.field public static final TYPE_FITBV:Lorg/icepdf/core/pobjects/Name;

.field public static final TYPE_FITH:Lorg/icepdf/core/pobjects/Name;

.field public static final TYPE_FITR:Lorg/icepdf/core/pobjects/Name;

.field public static final TYPE_FITV:Lorg/icepdf/core/pobjects/Name;

.field public static final TYPE_XYZ:Lorg/icepdf/core/pobjects/Name;

.field private static final logger:Ljava/util/logging/Logger;


# instance fields
.field private bottom:Ljava/lang/Float;

.field private inited:Z

.field private left:Ljava/lang/Float;

.field private library:Lorg/icepdf/core/util/Library;

.field private namedDestination:Lorg/icepdf/core/pobjects/Name;

.field private object:Ljava/lang/Object;

.field private ref:Lorg/icepdf/core/pobjects/Reference;

.field private right:Ljava/lang/Float;

.field private top:Ljava/lang/Float;

.field private type:Lorg/icepdf/core/pobjects/Name;

.field private zoom:Ljava/lang/Float;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 57
    const-class v0, Lorg/icepdf/core/pobjects/Destination;

    invoke-virtual {v0}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Lorg/icepdf/core/pobjects/Destination;->logger:Ljava/util/logging/Logger;

    .line 61
    new-instance v0, Lorg/icepdf/core/pobjects/Name;

    const-string/jumbo v1, "XYZ"

    invoke-direct {v0, v1}, Lorg/icepdf/core/pobjects/Name;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/icepdf/core/pobjects/Destination;->TYPE_XYZ:Lorg/icepdf/core/pobjects/Name;

    .line 62
    new-instance v0, Lorg/icepdf/core/pobjects/Name;

    const-string/jumbo v1, "Fit"

    invoke-direct {v0, v1}, Lorg/icepdf/core/pobjects/Name;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/icepdf/core/pobjects/Destination;->TYPE_FIT:Lorg/icepdf/core/pobjects/Name;

    .line 63
    new-instance v0, Lorg/icepdf/core/pobjects/Name;

    const-string/jumbo v1, "FitH"

    invoke-direct {v0, v1}, Lorg/icepdf/core/pobjects/Name;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/icepdf/core/pobjects/Destination;->TYPE_FITH:Lorg/icepdf/core/pobjects/Name;

    .line 64
    new-instance v0, Lorg/icepdf/core/pobjects/Name;

    const-string/jumbo v1, "FitV"

    invoke-direct {v0, v1}, Lorg/icepdf/core/pobjects/Name;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/icepdf/core/pobjects/Destination;->TYPE_FITV:Lorg/icepdf/core/pobjects/Name;

    .line 65
    new-instance v0, Lorg/icepdf/core/pobjects/Name;

    const-string/jumbo v1, "FitR"

    invoke-direct {v0, v1}, Lorg/icepdf/core/pobjects/Name;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/icepdf/core/pobjects/Destination;->TYPE_FITR:Lorg/icepdf/core/pobjects/Name;

    .line 66
    new-instance v0, Lorg/icepdf/core/pobjects/Name;

    const-string/jumbo v1, "FitB"

    invoke-direct {v0, v1}, Lorg/icepdf/core/pobjects/Name;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/icepdf/core/pobjects/Destination;->TYPE_FITB:Lorg/icepdf/core/pobjects/Name;

    .line 67
    new-instance v0, Lorg/icepdf/core/pobjects/Name;

    const-string/jumbo v1, "FitBH"

    invoke-direct {v0, v1}, Lorg/icepdf/core/pobjects/Name;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/icepdf/core/pobjects/Destination;->TYPE_FITBH:Lorg/icepdf/core/pobjects/Name;

    .line 68
    new-instance v0, Lorg/icepdf/core/pobjects/Name;

    const-string/jumbo v1, "FitBV"

    invoke-direct {v0, v1}, Lorg/icepdf/core/pobjects/Name;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/icepdf/core/pobjects/Destination;->TYPE_FITBV:Lorg/icepdf/core/pobjects/Name;

    return-void
.end method

.method public constructor <init>(Lorg/icepdf/core/util/Library;Ljava/lang/Object;)V
    .locals 1
    .param p1, "l"    # Lorg/icepdf/core/util/Library;
    .param p2, "h"    # Ljava/lang/Object;

    .prologue
    const/4 v0, 0x0

    .line 101
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 83
    iput-object v0, p0, Lorg/icepdf/core/pobjects/Destination;->left:Ljava/lang/Float;

    .line 84
    iput-object v0, p0, Lorg/icepdf/core/pobjects/Destination;->bottom:Ljava/lang/Float;

    .line 85
    iput-object v0, p0, Lorg/icepdf/core/pobjects/Destination;->right:Ljava/lang/Float;

    .line 86
    iput-object v0, p0, Lorg/icepdf/core/pobjects/Destination;->top:Ljava/lang/Float;

    .line 87
    iput-object v0, p0, Lorg/icepdf/core/pobjects/Destination;->zoom:Ljava/lang/Float;

    .line 102
    iput-object p1, p0, Lorg/icepdf/core/pobjects/Destination;->library:Lorg/icepdf/core/util/Library;

    .line 103
    iput-object p2, p0, Lorg/icepdf/core/pobjects/Destination;->object:Ljava/lang/Object;

    .line 104
    invoke-direct {p0}, Lorg/icepdf/core/pobjects/Destination;->init()V

    .line 105
    return-void
.end method

.method public static destinationSyntax(Lorg/icepdf/core/pobjects/Reference;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Vector;
    .locals 2
    .param p0, "page"    # Lorg/icepdf/core/pobjects/Reference;
    .param p1, "type"    # Ljava/lang/Object;
    .param p2, "left"    # Ljava/lang/Object;
    .param p3, "top"    # Ljava/lang/Object;
    .param p4, "zoom"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/icepdf/core/pobjects/Reference;",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ")",
            "Ljava/util/Vector",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 360
    new-instance v0, Ljava/util/Vector;

    const/4 v1, 0x5

    invoke-direct {v0, v1}, Ljava/util/Vector;-><init>(I)V

    .line 361
    .local v0, "destSyntax":Ljava/util/Vector;, "Ljava/util/Vector<Ljava/lang/Object;>;"
    invoke-virtual {v0, p0}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 362
    invoke-virtual {v0, p1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 363
    invoke-virtual {v0, p2}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 364
    invoke-virtual {v0, p3}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 365
    invoke-virtual {v0, p4}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 366
    return-object v0
.end method

.method public static destinationSyntax(Lorg/icepdf/core/pobjects/Reference;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Vector;
    .locals 2
    .param p0, "page"    # Lorg/icepdf/core/pobjects/Reference;
    .param p1, "type"    # Ljava/lang/Object;
    .param p2, "left"    # Ljava/lang/Object;
    .param p3, "bottom"    # Ljava/lang/Object;
    .param p4, "right"    # Ljava/lang/Object;
    .param p5, "top"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/icepdf/core/pobjects/Reference;",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ")",
            "Ljava/util/Vector",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 383
    new-instance v0, Ljava/util/Vector;

    const/4 v1, 0x6

    invoke-direct {v0, v1}, Ljava/util/Vector;-><init>(I)V

    .line 384
    .local v0, "destSyntax":Ljava/util/Vector;, "Ljava/util/Vector<Ljava/lang/Object;>;"
    invoke-virtual {v0, p0}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 385
    invoke-virtual {v0, p1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 386
    invoke-virtual {v0, p2}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 387
    invoke-virtual {v0, p3}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 388
    invoke-virtual {v0, p4}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 389
    invoke-virtual {v0, p5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 390
    return-object v0
.end method

.method public static destinationSyntax(Lorg/icepdf/core/pobjects/Reference;Lorg/icepdf/core/pobjects/Name;)Ljava/util/Vector;
    .locals 2
    .param p0, "page"    # Lorg/icepdf/core/pobjects/Reference;
    .param p1, "type"    # Lorg/icepdf/core/pobjects/Name;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/icepdf/core/pobjects/Reference;",
            "Lorg/icepdf/core/pobjects/Name;",
            ")",
            "Ljava/util/Vector",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 325
    new-instance v0, Ljava/util/Vector;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Ljava/util/Vector;-><init>(I)V

    .line 326
    .local v0, "destSyntax":Ljava/util/Vector;, "Ljava/util/Vector<Ljava/lang/Object;>;"
    invoke-virtual {v0, p0}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 327
    invoke-virtual {v0, p1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 328
    return-object v0
.end method

.method public static destinationSyntax(Lorg/icepdf/core/pobjects/Reference;Lorg/icepdf/core/pobjects/Name;Ljava/lang/Object;)Ljava/util/Vector;
    .locals 2
    .param p0, "page"    # Lorg/icepdf/core/pobjects/Reference;
    .param p1, "type"    # Lorg/icepdf/core/pobjects/Name;
    .param p2, "offset"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/icepdf/core/pobjects/Reference;",
            "Lorg/icepdf/core/pobjects/Name;",
            "Ljava/lang/Object;",
            ")",
            "Ljava/util/Vector",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 341
    new-instance v0, Ljava/util/Vector;

    const/4 v1, 0x3

    invoke-direct {v0, v1}, Ljava/util/Vector;-><init>(I)V

    .line 342
    .local v0, "destSyntax":Ljava/util/Vector;, "Ljava/util/Vector<Ljava/lang/Object;>;"
    invoke-virtual {v0, p0}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    .line 343
    invoke-virtual {v0, p1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    .line 344
    invoke-virtual {v0, p2}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    .line 345
    return-object v0
.end method

.method private init()V
    .locals 13

    .prologue
    .line 113
    iget-boolean v10, p0, Lorg/icepdf/core/pobjects/Destination;->inited:Z

    if-eqz v10, :cond_1

    .line 185
    :cond_0
    :goto_0
    return-void

    .line 116
    :cond_1
    const/4 v10, 0x1

    iput-boolean v10, p0, Lorg/icepdf/core/pobjects/Destination;->inited:Z

    .line 119
    iget-object v10, p0, Lorg/icepdf/core/pobjects/Destination;->object:Ljava/lang/Object;

    instance-of v10, v10, Ljava/util/Vector;

    if-eqz v10, :cond_2

    .line 120
    iget-object v10, p0, Lorg/icepdf/core/pobjects/Destination;->object:Ljava/lang/Object;

    check-cast v10, Ljava/util/Vector;

    invoke-direct {p0, v10}, Lorg/icepdf/core/pobjects/Destination;->parse(Ljava/util/Vector;)V

    goto :goto_0

    .line 125
    :cond_2
    iget-object v10, p0, Lorg/icepdf/core/pobjects/Destination;->object:Ljava/lang/Object;

    instance-of v10, v10, Lorg/icepdf/core/pobjects/Name;

    if-nez v10, :cond_3

    iget-object v10, p0, Lorg/icepdf/core/pobjects/Destination;->object:Ljava/lang/Object;

    instance-of v10, v10, Lorg/icepdf/core/pobjects/StringObject;

    if-eqz v10, :cond_0

    .line 128
    :cond_3
    iget-object v10, p0, Lorg/icepdf/core/pobjects/Destination;->object:Ljava/lang/Object;

    instance-of v10, v10, Lorg/icepdf/core/pobjects/StringObject;

    if-eqz v10, :cond_6

    .line 129
    iget-object v9, p0, Lorg/icepdf/core/pobjects/Destination;->object:Ljava/lang/Object;

    check-cast v9, Lorg/icepdf/core/pobjects/StringObject;

    .line 130
    .local v9, "stringObject":Lorg/icepdf/core/pobjects/StringObject;
    iget-object v10, p0, Lorg/icepdf/core/pobjects/Destination;->library:Lorg/icepdf/core/util/Library;

    iget-object v10, v10, Lorg/icepdf/core/util/Library;->securityManager:Lorg/icepdf/core/pobjects/security/SecurityManager;

    invoke-interface {v9, v10}, Lorg/icepdf/core/pobjects/StringObject;->getDecryptedLiteralString(Lorg/icepdf/core/pobjects/security/SecurityManager;)Ljava/lang/String;

    move-result-object v8

    .line 136
    .end local v9    # "stringObject":Lorg/icepdf/core/pobjects/StringObject;
    .local v8, "s":Ljava/lang/String;
    :goto_1
    new-instance v10, Lorg/icepdf/core/pobjects/Name;

    invoke-direct {v10, v8}, Lorg/icepdf/core/pobjects/Name;-><init>(Ljava/lang/String;)V

    iput-object v10, p0, Lorg/icepdf/core/pobjects/Destination;->namedDestination:Lorg/icepdf/core/pobjects/Name;

    .line 138
    const/4 v2, 0x0

    .line 139
    .local v2, "found":Z
    iget-object v10, p0, Lorg/icepdf/core/pobjects/Destination;->library:Lorg/icepdf/core/util/Library;

    invoke-virtual {v10}, Lorg/icepdf/core/util/Library;->getCatalog()Lorg/icepdf/core/pobjects/Catalog;

    move-result-object v0

    .line 140
    .local v0, "catalog":Lorg/icepdf/core/pobjects/Catalog;
    if-eqz v0, :cond_0

    .line 141
    invoke-virtual {v0}, Lorg/icepdf/core/pobjects/Catalog;->getNameTree()Lorg/icepdf/core/pobjects/NameTree;

    move-result-object v4

    .line 142
    .local v4, "nameTree":Lorg/icepdf/core/pobjects/NameTree;
    if-eqz v4, :cond_4

    .line 143
    invoke-virtual {v4, v8}, Lorg/icepdf/core/pobjects/NameTree;->searchName(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    .line 144
    .local v5, "o":Ljava/lang/Object;
    if-eqz v5, :cond_4

    .line 145
    instance-of v10, v5, Ljava/util/Vector;

    if-eqz v10, :cond_7

    .line 146
    check-cast v5, Ljava/util/Vector;

    .end local v5    # "o":Ljava/lang/Object;
    invoke-direct {p0, v5}, Lorg/icepdf/core/pobjects/Destination;->parse(Ljava/util/Vector;)V

    .line 147
    const/4 v2, 0x1

    .line 163
    :cond_4
    :goto_2
    if-nez v2, :cond_0

    .line 164
    invoke-virtual {v0}, Lorg/icepdf/core/pobjects/Catalog;->getDestinations()Lorg/icepdf/core/pobjects/Dictionary;

    move-result-object v1

    .line 165
    .local v1, "dests":Lorg/icepdf/core/pobjects/Dictionary;
    if-eqz v1, :cond_0

    .line 166
    invoke-virtual {v1, v8}, Lorg/icepdf/core/pobjects/Dictionary;->getObject(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    .line 167
    .local v7, "ob":Ljava/lang/Object;
    instance-of v10, v7, Ljava/util/Hashtable;

    if-eqz v10, :cond_9

    move-object v3, v7

    .line 168
    check-cast v3, Ljava/util/Hashtable;

    .line 169
    .local v3, "h":Ljava/util/Hashtable;
    const-string/jumbo v10, "D"

    invoke-virtual {v3, v10}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    .line 170
    .local v6, "o1":Ljava/lang/Object;
    if-nez v6, :cond_5

    .line 172
    new-instance v10, Lorg/icepdf/core/pobjects/Name;

    const-string/jumbo v11, "D"

    invoke-direct {v10, v11}, Lorg/icepdf/core/pobjects/Name;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v10}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    .line 175
    :cond_5
    check-cast v6, Ljava/util/Vector;

    .end local v6    # "o1":Ljava/lang/Object;
    check-cast v6, Ljava/util/Vector;

    invoke-direct {p0, v6}, Lorg/icepdf/core/pobjects/Destination;->parse(Ljava/util/Vector;)V

    goto/16 :goto_0

    .line 132
    .end local v0    # "catalog":Lorg/icepdf/core/pobjects/Catalog;
    .end local v1    # "dests":Lorg/icepdf/core/pobjects/Dictionary;
    .end local v2    # "found":Z
    .end local v3    # "h":Ljava/util/Hashtable;
    .end local v4    # "nameTree":Lorg/icepdf/core/pobjects/NameTree;
    .end local v7    # "ob":Ljava/lang/Object;
    .end local v8    # "s":Ljava/lang/String;
    :cond_6
    iget-object v10, p0, Lorg/icepdf/core/pobjects/Destination;->object:Ljava/lang/Object;

    invoke-virtual {v10}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v8

    .restart local v8    # "s":Ljava/lang/String;
    goto :goto_1

    .line 148
    .restart local v0    # "catalog":Lorg/icepdf/core/pobjects/Catalog;
    .restart local v2    # "found":Z
    .restart local v4    # "nameTree":Lorg/icepdf/core/pobjects/NameTree;
    .restart local v5    # "o":Ljava/lang/Object;
    :cond_7
    instance-of v10, v5, Ljava/util/Hashtable;

    if-eqz v10, :cond_4

    move-object v3, v5

    .line 149
    check-cast v3, Ljava/util/Hashtable;

    .line 150
    .restart local v3    # "h":Ljava/util/Hashtable;
    const-string/jumbo v10, "D"

    invoke-virtual {v3, v10}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    .line 151
    .restart local v6    # "o1":Ljava/lang/Object;
    if-nez v6, :cond_8

    .line 153
    new-instance v10, Lorg/icepdf/core/pobjects/Name;

    const-string/jumbo v11, "D"

    invoke-direct {v10, v11}, Lorg/icepdf/core/pobjects/Name;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v10}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    .line 156
    :cond_8
    instance-of v10, v6, Ljava/util/Vector;

    if-eqz v10, :cond_4

    .line 157
    check-cast v6, Ljava/util/Vector;

    .end local v6    # "o1":Ljava/lang/Object;
    invoke-direct {p0, v6}, Lorg/icepdf/core/pobjects/Destination;->parse(Ljava/util/Vector;)V

    .line 158
    const/4 v2, 0x1

    goto :goto_2

    .line 177
    .end local v3    # "h":Ljava/util/Hashtable;
    .end local v5    # "o":Ljava/lang/Object;
    .restart local v1    # "dests":Lorg/icepdf/core/pobjects/Dictionary;
    .restart local v7    # "ob":Ljava/lang/Object;
    :cond_9
    sget-object v10, Lorg/icepdf/core/pobjects/Destination;->logger:Ljava/util/logging/Logger;

    sget-object v11, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    invoke-virtual {v10, v11}, Ljava/util/logging/Logger;->isLoggable(Ljava/util/logging/Level;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 178
    sget-object v10, Lorg/icepdf/core/pobjects/Destination;->logger:Ljava/util/logging/Logger;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v12, "Destination type missed="

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/util/logging/Logger;->warning(Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method private parse(Ljava/util/Vector;)V
    .locals 6
    .param p1, "v"    # Ljava/util/Vector;

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x3

    const/4 v3, 0x2

    .line 203
    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 204
    .local v0, "ob":Ljava/lang/Object;
    instance-of v1, v0, Lorg/icepdf/core/pobjects/Reference;

    if-eqz v1, :cond_0

    .line 205
    check-cast v0, Lorg/icepdf/core/pobjects/Reference;

    .end local v0    # "ob":Ljava/lang/Object;
    iput-object v0, p0, Lorg/icepdf/core/pobjects/Destination;->ref:Lorg/icepdf/core/pobjects/Reference;

    .line 208
    :cond_0
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 209
    .restart local v0    # "ob":Ljava/lang/Object;
    instance-of v1, v0, Lorg/icepdf/core/pobjects/Name;

    if-eqz v1, :cond_4

    move-object v1, v0

    .line 210
    check-cast v1, Lorg/icepdf/core/pobjects/Name;

    iput-object v1, p0, Lorg/icepdf/core/pobjects/Destination;->type:Lorg/icepdf/core/pobjects/Name;

    .line 215
    :goto_0
    iget-object v1, p0, Lorg/icepdf/core/pobjects/Destination;->type:Lorg/icepdf/core/pobjects/Name;

    sget-object v2, Lorg/icepdf/core/pobjects/Destination;->TYPE_XYZ:Lorg/icepdf/core/pobjects/Name;

    invoke-virtual {v1, v2}, Lorg/icepdf/core/pobjects/Name;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 216
    invoke-virtual {p1, v3}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 217
    if-eqz v0, :cond_1

    const-string/jumbo v1, "null"

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 218
    check-cast v0, Ljava/lang/Number;

    .end local v0    # "ob":Ljava/lang/Object;
    invoke-virtual {v0}, Ljava/lang/Number;->floatValue()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    iput-object v1, p0, Lorg/icepdf/core/pobjects/Destination;->left:Ljava/lang/Float;

    .line 220
    :cond_1
    invoke-virtual {p1, v4}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 221
    .restart local v0    # "ob":Ljava/lang/Object;
    if-eqz v0, :cond_2

    const-string/jumbo v1, "null"

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 222
    check-cast v0, Ljava/lang/Number;

    .end local v0    # "ob":Ljava/lang/Object;
    invoke-virtual {v0}, Ljava/lang/Number;->floatValue()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    iput-object v1, p0, Lorg/icepdf/core/pobjects/Destination;->top:Ljava/lang/Float;

    .line 224
    :cond_2
    invoke-virtual {p1, v5}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 225
    .restart local v0    # "ob":Ljava/lang/Object;
    if-eqz v0, :cond_3

    const-string/jumbo v1, "null"

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    const-string/jumbo v1, "0"

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    move-object v1, v0

    .line 226
    check-cast v1, Ljava/lang/Number;

    invoke-virtual {v1}, Ljava/lang/Number;->floatValue()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    iput-object v1, p0, Lorg/icepdf/core/pobjects/Destination;->zoom:Ljava/lang/Float;

    .line 273
    :cond_3
    :goto_1
    return-void

    .line 212
    :cond_4
    new-instance v1, Lorg/icepdf/core/pobjects/Name;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lorg/icepdf/core/pobjects/Name;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lorg/icepdf/core/pobjects/Destination;->type:Lorg/icepdf/core/pobjects/Name;

    goto :goto_0

    .line 230
    :cond_5
    iget-object v1, p0, Lorg/icepdf/core/pobjects/Destination;->type:Lorg/icepdf/core/pobjects/Name;

    sget-object v2, Lorg/icepdf/core/pobjects/Destination;->TYPE_FITH:Lorg/icepdf/core/pobjects/Name;

    invoke-virtual {v1, v2}, Lorg/icepdf/core/pobjects/Name;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 231
    invoke-virtual {p1, v3}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 232
    if-eqz v0, :cond_3

    const-string/jumbo v1, "null"

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    move-object v1, v0

    .line 233
    check-cast v1, Ljava/lang/Number;

    invoke-virtual {v1}, Ljava/lang/Number;->floatValue()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    iput-object v1, p0, Lorg/icepdf/core/pobjects/Destination;->top:Ljava/lang/Float;

    goto :goto_1

    .line 237
    :cond_6
    iget-object v1, p0, Lorg/icepdf/core/pobjects/Destination;->type:Lorg/icepdf/core/pobjects/Name;

    sget-object v2, Lorg/icepdf/core/pobjects/Destination;->TYPE_FITR:Lorg/icepdf/core/pobjects/Name;

    invoke-virtual {v1, v2}, Lorg/icepdf/core/pobjects/Name;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 238
    invoke-virtual {p1, v3}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 239
    if-eqz v0, :cond_7

    const-string/jumbo v1, "null"

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 240
    check-cast v0, Ljava/lang/Number;

    .end local v0    # "ob":Ljava/lang/Object;
    invoke-virtual {v0}, Ljava/lang/Number;->floatValue()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    iput-object v1, p0, Lorg/icepdf/core/pobjects/Destination;->left:Ljava/lang/Float;

    .line 242
    :cond_7
    invoke-virtual {p1, v4}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 243
    .restart local v0    # "ob":Ljava/lang/Object;
    if-eqz v0, :cond_8

    const-string/jumbo v1, "null"

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_8

    .line 244
    check-cast v0, Ljava/lang/Number;

    .end local v0    # "ob":Ljava/lang/Object;
    invoke-virtual {v0}, Ljava/lang/Number;->floatValue()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    iput-object v1, p0, Lorg/icepdf/core/pobjects/Destination;->bottom:Ljava/lang/Float;

    .line 246
    :cond_8
    invoke-virtual {p1, v5}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 247
    .restart local v0    # "ob":Ljava/lang/Object;
    if-eqz v0, :cond_9

    const-string/jumbo v1, "null"

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_9

    .line 248
    check-cast v0, Ljava/lang/Number;

    .end local v0    # "ob":Ljava/lang/Object;
    invoke-virtual {v0}, Ljava/lang/Number;->floatValue()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    iput-object v1, p0, Lorg/icepdf/core/pobjects/Destination;->right:Ljava/lang/Float;

    .line 250
    :cond_9
    const/4 v1, 0x5

    invoke-virtual {p1, v1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 251
    .restart local v0    # "ob":Ljava/lang/Object;
    if-eqz v0, :cond_3

    const-string/jumbo v1, "null"

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    move-object v1, v0

    .line 252
    check-cast v1, Ljava/lang/Number;

    invoke-virtual {v1}, Ljava/lang/Number;->floatValue()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    iput-object v1, p0, Lorg/icepdf/core/pobjects/Destination;->top:Ljava/lang/Float;

    goto/16 :goto_1

    .line 256
    :cond_a
    iget-object v1, p0, Lorg/icepdf/core/pobjects/Destination;->type:Lorg/icepdf/core/pobjects/Name;

    sget-object v2, Lorg/icepdf/core/pobjects/Destination;->TYPE_FITB:Lorg/icepdf/core/pobjects/Name;

    invoke-virtual {v1, v2}, Lorg/icepdf/core/pobjects/Name;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 260
    iget-object v1, p0, Lorg/icepdf/core/pobjects/Destination;->type:Lorg/icepdf/core/pobjects/Name;

    sget-object v2, Lorg/icepdf/core/pobjects/Destination;->TYPE_FITBH:Lorg/icepdf/core/pobjects/Name;

    invoke-virtual {v1, v2}, Lorg/icepdf/core/pobjects/Name;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 261
    invoke-virtual {p1, v3}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 262
    if-eqz v0, :cond_3

    const-string/jumbo v1, "null"

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    move-object v1, v0

    .line 263
    check-cast v1, Ljava/lang/Number;

    invoke-virtual {v1}, Ljava/lang/Number;->floatValue()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    iput-object v1, p0, Lorg/icepdf/core/pobjects/Destination;->top:Ljava/lang/Float;

    goto/16 :goto_1

    .line 267
    :cond_b
    iget-object v1, p0, Lorg/icepdf/core/pobjects/Destination;->type:Lorg/icepdf/core/pobjects/Name;

    sget-object v2, Lorg/icepdf/core/pobjects/Destination;->TYPE_FITBV:Lorg/icepdf/core/pobjects/Name;

    invoke-virtual {v1, v2}, Lorg/icepdf/core/pobjects/Name;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 268
    invoke-virtual {p1, v3}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 269
    if-eqz v0, :cond_3

    const-string/jumbo v1, "null"

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    move-object v1, v0

    .line 270
    check-cast v1, Ljava/lang/Number;

    invoke-virtual {v1}, Ljava/lang/Number;->floatValue()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    iput-object v1, p0, Lorg/icepdf/core/pobjects/Destination;->left:Ljava/lang/Float;

    goto/16 :goto_1
.end method


# virtual methods
.method public getBottom()Ljava/lang/Float;
    .locals 1

    .prologue
    .line 459
    iget-object v0, p0, Lorg/icepdf/core/pobjects/Destination;->bottom:Ljava/lang/Float;

    return-object v0
.end method

.method public getEncodedDestination()Ljava/lang/Object;
    .locals 3

    .prologue
    const/high16 v2, 0x7fc00000    # NaNf

    .line 480
    iget-object v1, p0, Lorg/icepdf/core/pobjects/Destination;->namedDestination:Lorg/icepdf/core/pobjects/Name;

    if-eqz v1, :cond_1

    .line 481
    iget-object v0, p0, Lorg/icepdf/core/pobjects/Destination;->namedDestination:Lorg/icepdf/core/pobjects/Name;

    .line 515
    :cond_0
    :goto_0
    return-object v0

    .line 484
    :cond_1
    iget-object v1, p0, Lorg/icepdf/core/pobjects/Destination;->object:Ljava/lang/Object;

    instance-of v1, v1, Ljava/util/Vector;

    if-eqz v1, :cond_8

    .line 485
    new-instance v0, Ljava/util/Vector;

    const/4 v1, 0x7

    invoke-direct {v0, v1}, Ljava/util/Vector;-><init>(I)V

    .line 486
    .local v0, "v":Ljava/util/Vector;, "Ljava/util/Vector<Ljava/lang/Object;>;"
    iget-object v1, p0, Lorg/icepdf/core/pobjects/Destination;->ref:Lorg/icepdf/core/pobjects/Reference;

    if-eqz v1, :cond_2

    .line 487
    iget-object v1, p0, Lorg/icepdf/core/pobjects/Destination;->ref:Lorg/icepdf/core/pobjects/Reference;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 490
    :cond_2
    iget-object v1, p0, Lorg/icepdf/core/pobjects/Destination;->type:Lorg/icepdf/core/pobjects/Name;

    if-eqz v1, :cond_3

    .line 491
    iget-object v1, p0, Lorg/icepdf/core/pobjects/Destination;->type:Lorg/icepdf/core/pobjects/Name;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 494
    :cond_3
    iget-object v1, p0, Lorg/icepdf/core/pobjects/Destination;->left:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    cmpl-float v1, v1, v2

    if-eqz v1, :cond_4

    .line 495
    iget-object v1, p0, Lorg/icepdf/core/pobjects/Destination;->left:Ljava/lang/Float;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 498
    :cond_4
    iget-object v1, p0, Lorg/icepdf/core/pobjects/Destination;->bottom:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    cmpl-float v1, v1, v2

    if-eqz v1, :cond_5

    .line 499
    iget-object v1, p0, Lorg/icepdf/core/pobjects/Destination;->bottom:Ljava/lang/Float;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 502
    :cond_5
    iget-object v1, p0, Lorg/icepdf/core/pobjects/Destination;->right:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    cmpl-float v1, v1, v2

    if-eqz v1, :cond_6

    .line 503
    iget-object v1, p0, Lorg/icepdf/core/pobjects/Destination;->right:Ljava/lang/Float;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 506
    :cond_6
    iget-object v1, p0, Lorg/icepdf/core/pobjects/Destination;->top:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    cmpl-float v1, v1, v2

    if-eqz v1, :cond_7

    .line 507
    iget-object v1, p0, Lorg/icepdf/core/pobjects/Destination;->top:Ljava/lang/Float;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 510
    :cond_7
    iget-object v1, p0, Lorg/icepdf/core/pobjects/Destination;->zoom:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    cmpl-float v1, v1, v2

    if-eqz v1, :cond_0

    .line 511
    iget-object v1, p0, Lorg/icepdf/core/pobjects/Destination;->zoom:Ljava/lang/Float;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 515
    .end local v0    # "v":Ljava/util/Vector;, "Ljava/util/Vector<Ljava/lang/Object;>;"
    :cond_8
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getLeft()Ljava/lang/Float;
    .locals 1

    .prologue
    .line 410
    iget-object v0, p0, Lorg/icepdf/core/pobjects/Destination;->left:Ljava/lang/Float;

    return-object v0
.end method

.method public getNamedDestination()Lorg/icepdf/core/pobjects/Name;
    .locals 1

    .prologue
    .line 281
    iget-object v0, p0, Lorg/icepdf/core/pobjects/Destination;->namedDestination:Lorg/icepdf/core/pobjects/Name;

    return-object v0
.end method

.method public getObject()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 192
    iget-object v0, p0, Lorg/icepdf/core/pobjects/Destination;->object:Ljava/lang/Object;

    return-object v0
.end method

.method public getPageReference()Lorg/icepdf/core/pobjects/Reference;
    .locals 1

    .prologue
    .line 399
    iget-object v0, p0, Lorg/icepdf/core/pobjects/Destination;->ref:Lorg/icepdf/core/pobjects/Reference;

    return-object v0
.end method

.method public getRef()Lorg/icepdf/core/pobjects/Reference;
    .locals 1

    .prologue
    .line 439
    iget-object v0, p0, Lorg/icepdf/core/pobjects/Destination;->ref:Lorg/icepdf/core/pobjects/Reference;

    return-object v0
.end method

.method public getRight()Ljava/lang/Float;
    .locals 1

    .prologue
    .line 469
    iget-object v0, p0, Lorg/icepdf/core/pobjects/Destination;->right:Ljava/lang/Float;

    return-object v0
.end method

.method public getTop()Ljava/lang/Float;
    .locals 1

    .prologue
    .line 421
    iget-object v0, p0, Lorg/icepdf/core/pobjects/Destination;->top:Ljava/lang/Float;

    return-object v0
.end method

.method public getType()Lorg/icepdf/core/pobjects/Name;
    .locals 1

    .prologue
    .line 449
    iget-object v0, p0, Lorg/icepdf/core/pobjects/Destination;->type:Lorg/icepdf/core/pobjects/Name;

    return-object v0
.end method

.method public getZoom()Ljava/lang/Float;
    .locals 1

    .prologue
    .line 430
    iget-object v0, p0, Lorg/icepdf/core/pobjects/Destination;->zoom:Ljava/lang/Float;

    return-object v0
.end method

.method public setDestinationSyntax(Ljava/util/Vector;)V
    .locals 1
    .param p1, "destinationSyntax"    # Ljava/util/Vector;

    .prologue
    .line 309
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/icepdf/core/pobjects/Destination;->namedDestination:Lorg/icepdf/core/pobjects/Name;

    .line 310
    iput-object p1, p0, Lorg/icepdf/core/pobjects/Destination;->object:Ljava/lang/Object;

    .line 312
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/icepdf/core/pobjects/Destination;->inited:Z

    .line 313
    invoke-direct {p0}, Lorg/icepdf/core/pobjects/Destination;->init()V

    .line 314
    return-void
.end method

.method public setNamedDestination(Lorg/icepdf/core/pobjects/Name;)V
    .locals 1
    .param p1, "dest"    # Lorg/icepdf/core/pobjects/Name;

    .prologue
    .line 291
    iput-object p1, p0, Lorg/icepdf/core/pobjects/Destination;->namedDestination:Lorg/icepdf/core/pobjects/Name;

    .line 294
    iput-object p1, p0, Lorg/icepdf/core/pobjects/Destination;->object:Ljava/lang/Object;

    .line 296
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/icepdf/core/pobjects/Destination;->inited:Z

    .line 297
    invoke-direct {p0}, Lorg/icepdf/core/pobjects/Destination;->init()V

    .line 298
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 524
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "Destination  ref: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lorg/icepdf/core/pobjects/Destination;->getPageReference()Lorg/icepdf/core/pobjects/Reference;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " ,  top: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lorg/icepdf/core/pobjects/Destination;->getTop()Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " ,  left: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lorg/icepdf/core/pobjects/Destination;->getLeft()Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " ,  zoom: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lorg/icepdf/core/pobjects/Destination;->getZoom()Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
