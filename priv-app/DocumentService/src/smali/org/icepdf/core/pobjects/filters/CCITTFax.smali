.class public Lorg/icepdf/core/pobjects/filters/CCITTFax;
.super Ljava/lang/Object;
.source "CCITTFax.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/icepdf/core/pobjects/filters/CCITTFax$Code;
    }
.end annotation


# static fields
.field static final _extmcodes:[Ljava/lang/String;

.field static final _mbcodes:[Ljava/lang/String;

.field static final _modecodes:[Ljava/lang/String;

.field static final _mwcodes:[Ljava/lang/String;

.field static final _tbcodes:[Ljava/lang/String;

.field static final _twcodes:[Ljava/lang/String;

.field static black:I

.field static final extmcodes:[[Lorg/icepdf/core/pobjects/filters/CCITTFax$Code;

.field static final mbcodes:[[Lorg/icepdf/core/pobjects/filters/CCITTFax$Code;

.field static final modecodes:[[Lorg/icepdf/core/pobjects/filters/CCITTFax$Code;

.field static final mwcodes:[[Lorg/icepdf/core/pobjects/filters/CCITTFax$Code;

.field static final tbcodes:[[Lorg/icepdf/core/pobjects/filters/CCITTFax$Code;

.field static final twcodes:[[Lorg/icepdf/core/pobjects/filters/CCITTFax$Code;

.field static white:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 55
    const/16 v0, 0x40

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "00110101"

    aput-object v1, v0, v3

    const-string/jumbo v1, "000111"

    aput-object v1, v0, v4

    const-string/jumbo v1, "0111"

    aput-object v1, v0, v5

    const-string/jumbo v1, "1000"

    aput-object v1, v0, v6

    const-string/jumbo v1, "1011"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string/jumbo v2, "1100"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string/jumbo v2, "1110"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string/jumbo v2, "1111"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string/jumbo v2, "10011"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string/jumbo v2, "10100"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string/jumbo v2, "00111"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string/jumbo v2, "01000"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string/jumbo v2, "001000"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string/jumbo v2, "000011"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string/jumbo v2, "110100"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string/jumbo v2, "110101"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string/jumbo v2, "101010"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string/jumbo v2, "101011"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string/jumbo v2, "0100111"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string/jumbo v2, "0001100"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string/jumbo v2, "0001000"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string/jumbo v2, "0010111"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string/jumbo v2, "0000011"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string/jumbo v2, "0000100"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string/jumbo v2, "0101000"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string/jumbo v2, "0101011"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string/jumbo v2, "0010011"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string/jumbo v2, "0100100"

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string/jumbo v2, "0011000"

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    const-string/jumbo v2, "00000010"

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    const-string/jumbo v2, "00000011"

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    const-string/jumbo v2, "00011010"

    aput-object v2, v0, v1

    const/16 v1, 0x20

    const-string/jumbo v2, "00011011"

    aput-object v2, v0, v1

    const/16 v1, 0x21

    const-string/jumbo v2, "00010010"

    aput-object v2, v0, v1

    const/16 v1, 0x22

    const-string/jumbo v2, "00010011"

    aput-object v2, v0, v1

    const/16 v1, 0x23

    const-string/jumbo v2, "00010100"

    aput-object v2, v0, v1

    const/16 v1, 0x24

    const-string/jumbo v2, "00010101"

    aput-object v2, v0, v1

    const/16 v1, 0x25

    const-string/jumbo v2, "00010110"

    aput-object v2, v0, v1

    const/16 v1, 0x26

    const-string/jumbo v2, "00010111"

    aput-object v2, v0, v1

    const/16 v1, 0x27

    const-string/jumbo v2, "00101000"

    aput-object v2, v0, v1

    const/16 v1, 0x28

    const-string/jumbo v2, "00101001"

    aput-object v2, v0, v1

    const/16 v1, 0x29

    const-string/jumbo v2, "00101010"

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    const-string/jumbo v2, "00101011"

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    const-string/jumbo v2, "00101100"

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    const-string/jumbo v2, "00101101"

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    const-string/jumbo v2, "00000100"

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    const-string/jumbo v2, "00000101"

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    const-string/jumbo v2, "00001010"

    aput-object v2, v0, v1

    const/16 v1, 0x30

    const-string/jumbo v2, "00001011"

    aput-object v2, v0, v1

    const/16 v1, 0x31

    const-string/jumbo v2, "01010010"

    aput-object v2, v0, v1

    const/16 v1, 0x32

    const-string/jumbo v2, "01010011"

    aput-object v2, v0, v1

    const/16 v1, 0x33

    const-string/jumbo v2, "01010100"

    aput-object v2, v0, v1

    const/16 v1, 0x34

    const-string/jumbo v2, "01010101"

    aput-object v2, v0, v1

    const/16 v1, 0x35

    const-string/jumbo v2, "00100100"

    aput-object v2, v0, v1

    const/16 v1, 0x36

    const-string/jumbo v2, "00100101"

    aput-object v2, v0, v1

    const/16 v1, 0x37

    const-string/jumbo v2, "01011000"

    aput-object v2, v0, v1

    const/16 v1, 0x38

    const-string/jumbo v2, "01011001"

    aput-object v2, v0, v1

    const/16 v1, 0x39

    const-string/jumbo v2, "01011010"

    aput-object v2, v0, v1

    const/16 v1, 0x3a

    const-string/jumbo v2, "01011011"

    aput-object v2, v0, v1

    const/16 v1, 0x3b

    const-string/jumbo v2, "01001010"

    aput-object v2, v0, v1

    const/16 v1, 0x3c

    const-string/jumbo v2, "01001011"

    aput-object v2, v0, v1

    const/16 v1, 0x3d

    const-string/jumbo v2, "00110010"

    aput-object v2, v0, v1

    const/16 v1, 0x3e

    const-string/jumbo v2, "00110011"

    aput-object v2, v0, v1

    const/16 v1, 0x3f

    const-string/jumbo v2, "00110100"

    aput-object v2, v0, v1

    sput-object v0, Lorg/icepdf/core/pobjects/filters/CCITTFax;->_twcodes:[Ljava/lang/String;

    .line 71
    const/16 v0, 0x1b

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "11011"

    aput-object v1, v0, v3

    const-string/jumbo v1, "10010"

    aput-object v1, v0, v4

    const-string/jumbo v1, "010111"

    aput-object v1, v0, v5

    const-string/jumbo v1, "0110111"

    aput-object v1, v0, v6

    const-string/jumbo v1, "00110110"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string/jumbo v2, "00110111"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string/jumbo v2, "01100100"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string/jumbo v2, "01100101"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string/jumbo v2, "01101000"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string/jumbo v2, "01100111"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string/jumbo v2, "011001100"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string/jumbo v2, "011001101"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string/jumbo v2, "011010010"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string/jumbo v2, "011010011"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string/jumbo v2, "011010100"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string/jumbo v2, "011010101"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string/jumbo v2, "011010110"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string/jumbo v2, "011010111"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string/jumbo v2, "011011000"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string/jumbo v2, "011011001"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string/jumbo v2, "011011010"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string/jumbo v2, "011011011"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string/jumbo v2, "010011000"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string/jumbo v2, "010011001"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string/jumbo v2, "010011010"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string/jumbo v2, "011000"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string/jumbo v2, "010011011"

    aput-object v2, v0, v1

    sput-object v0, Lorg/icepdf/core/pobjects/filters/CCITTFax;->_mwcodes:[Ljava/lang/String;

    .line 80
    const/16 v0, 0x40

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "0000110111"

    aput-object v1, v0, v3

    const-string/jumbo v1, "010"

    aput-object v1, v0, v4

    const-string/jumbo v1, "11"

    aput-object v1, v0, v5

    const-string/jumbo v1, "10"

    aput-object v1, v0, v6

    const-string/jumbo v1, "011"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string/jumbo v2, "0011"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string/jumbo v2, "0010"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string/jumbo v2, "00011"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string/jumbo v2, "000101"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string/jumbo v2, "000100"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string/jumbo v2, "0000100"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string/jumbo v2, "0000101"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string/jumbo v2, "0000111"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string/jumbo v2, "00000100"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string/jumbo v2, "00000111"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string/jumbo v2, "000011000"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string/jumbo v2, "0000010111"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string/jumbo v2, "0000011000"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string/jumbo v2, "0000001000"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string/jumbo v2, "00001100111"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string/jumbo v2, "00001101000"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string/jumbo v2, "00001101100"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string/jumbo v2, "00000110111"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string/jumbo v2, "00000101000"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string/jumbo v2, "00000010111"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string/jumbo v2, "00000011000"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string/jumbo v2, "000011001010"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string/jumbo v2, "000011001011"

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string/jumbo v2, "000011001100"

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    const-string/jumbo v2, "000011001101"

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    const-string/jumbo v2, "000001101000"

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    const-string/jumbo v2, "000001101001"

    aput-object v2, v0, v1

    const/16 v1, 0x20

    const-string/jumbo v2, "000001101010"

    aput-object v2, v0, v1

    const/16 v1, 0x21

    const-string/jumbo v2, "000001101011"

    aput-object v2, v0, v1

    const/16 v1, 0x22

    const-string/jumbo v2, "000011010010"

    aput-object v2, v0, v1

    const/16 v1, 0x23

    const-string/jumbo v2, "000011010011"

    aput-object v2, v0, v1

    const/16 v1, 0x24

    const-string/jumbo v2, "000011010100"

    aput-object v2, v0, v1

    const/16 v1, 0x25

    const-string/jumbo v2, "000011010101"

    aput-object v2, v0, v1

    const/16 v1, 0x26

    const-string/jumbo v2, "000011010110"

    aput-object v2, v0, v1

    const/16 v1, 0x27

    const-string/jumbo v2, "000011010111"

    aput-object v2, v0, v1

    const/16 v1, 0x28

    const-string/jumbo v2, "000001101100"

    aput-object v2, v0, v1

    const/16 v1, 0x29

    const-string/jumbo v2, "000001101101"

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    const-string/jumbo v2, "000011011010"

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    const-string/jumbo v2, "000011011011"

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    const-string/jumbo v2, "000001010100"

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    const-string/jumbo v2, "000001010101"

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    const-string/jumbo v2, "000001010110"

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    const-string/jumbo v2, "000001010111"

    aput-object v2, v0, v1

    const/16 v1, 0x30

    const-string/jumbo v2, "000001100100"

    aput-object v2, v0, v1

    const/16 v1, 0x31

    const-string/jumbo v2, "000001100101"

    aput-object v2, v0, v1

    const/16 v1, 0x32

    const-string/jumbo v2, "000001010010"

    aput-object v2, v0, v1

    const/16 v1, 0x33

    const-string/jumbo v2, "000001010011"

    aput-object v2, v0, v1

    const/16 v1, 0x34

    const-string/jumbo v2, "000000100100"

    aput-object v2, v0, v1

    const/16 v1, 0x35

    const-string/jumbo v2, "000000110111"

    aput-object v2, v0, v1

    const/16 v1, 0x36

    const-string/jumbo v2, "000000111000"

    aput-object v2, v0, v1

    const/16 v1, 0x37

    const-string/jumbo v2, "000000100111"

    aput-object v2, v0, v1

    const/16 v1, 0x38

    const-string/jumbo v2, "000000101000"

    aput-object v2, v0, v1

    const/16 v1, 0x39

    const-string/jumbo v2, "000001011000"

    aput-object v2, v0, v1

    const/16 v1, 0x3a

    const-string/jumbo v2, "000001011001"

    aput-object v2, v0, v1

    const/16 v1, 0x3b

    const-string/jumbo v2, "000000101011"

    aput-object v2, v0, v1

    const/16 v1, 0x3c

    const-string/jumbo v2, "000000101100"

    aput-object v2, v0, v1

    const/16 v1, 0x3d

    const-string/jumbo v2, "000001011010"

    aput-object v2, v0, v1

    const/16 v1, 0x3e

    const-string/jumbo v2, "000001100110"

    aput-object v2, v0, v1

    const/16 v1, 0x3f

    const-string/jumbo v2, "000001100111"

    aput-object v2, v0, v1

    sput-object v0, Lorg/icepdf/core/pobjects/filters/CCITTFax;->_tbcodes:[Ljava/lang/String;

    .line 97
    const/16 v0, 0x1b

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "0000001111"

    aput-object v1, v0, v3

    const-string/jumbo v1, "000011001000"

    aput-object v1, v0, v4

    const-string/jumbo v1, "000011001001"

    aput-object v1, v0, v5

    const-string/jumbo v1, "000001011011"

    aput-object v1, v0, v6

    const-string/jumbo v1, "000000110011"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string/jumbo v2, "000000110100"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string/jumbo v2, "000000110101"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string/jumbo v2, "0000001101100"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string/jumbo v2, "0000001101101"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string/jumbo v2, "0000001001010"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string/jumbo v2, "0000001001011"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string/jumbo v2, "0000001001100"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string/jumbo v2, "0000001001101"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string/jumbo v2, "0000001110010"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string/jumbo v2, "0000001110011"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string/jumbo v2, "0000001110100"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string/jumbo v2, "0000001110101"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string/jumbo v2, "0000001110110"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string/jumbo v2, "0000001110111"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string/jumbo v2, "0000001010010"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string/jumbo v2, "0000001010011"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string/jumbo v2, "0000001010100"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string/jumbo v2, "0000001010101"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string/jumbo v2, "0000001011010"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string/jumbo v2, "0000001011011"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string/jumbo v2, "0000001100100"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string/jumbo v2, "0000001100101"

    aput-object v2, v0, v1

    sput-object v0, Lorg/icepdf/core/pobjects/filters/CCITTFax;->_mbcodes:[Ljava/lang/String;

    .line 106
    const/16 v0, 0xd

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "00000001000"

    aput-object v1, v0, v3

    const-string/jumbo v1, "00000001100"

    aput-object v1, v0, v4

    const-string/jumbo v1, "00000001101"

    aput-object v1, v0, v5

    const-string/jumbo v1, "000000010010"

    aput-object v1, v0, v6

    const-string/jumbo v1, "000000010011"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string/jumbo v2, "000000010100"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string/jumbo v2, "000000010101"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string/jumbo v2, "000000010110"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string/jumbo v2, "000000010111"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string/jumbo v2, "000000011100"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string/jumbo v2, "000000011101"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string/jumbo v2, "000000011110"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string/jumbo v2, "000000011111"

    aput-object v2, v0, v1

    sput-object v0, Lorg/icepdf/core/pobjects/filters/CCITTFax;->_extmcodes:[Ljava/lang/String;

    .line 113
    const/16 v0, 0xc

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "0001"

    aput-object v1, v0, v3

    const-string/jumbo v1, "001"

    aput-object v1, v0, v4

    const-string/jumbo v1, "1"

    aput-object v1, v0, v5

    const-string/jumbo v1, "011"

    aput-object v1, v0, v6

    const-string/jumbo v1, "000011"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string/jumbo v2, "0000011"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string/jumbo v2, "010"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string/jumbo v2, "000010"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string/jumbo v2, "0000010"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string/jumbo v2, "0000001111"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string/jumbo v2, "000000001111"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string/jumbo v2, "000000000001"

    aput-object v2, v0, v1

    sput-object v0, Lorg/icepdf/core/pobjects/filters/CCITTFax;->_modecodes:[Ljava/lang/String;

    .line 181
    sget-object v0, Lorg/icepdf/core/pobjects/filters/CCITTFax;->_twcodes:[Ljava/lang/String;

    invoke-static {v0}, Lorg/icepdf/core/pobjects/filters/CCITTFax;->convertStringArrayToCodeArray2D([Ljava/lang/String;)[[Lorg/icepdf/core/pobjects/filters/CCITTFax$Code;

    move-result-object v0

    sput-object v0, Lorg/icepdf/core/pobjects/filters/CCITTFax;->twcodes:[[Lorg/icepdf/core/pobjects/filters/CCITTFax$Code;

    .line 182
    sget-object v0, Lorg/icepdf/core/pobjects/filters/CCITTFax;->_mwcodes:[Ljava/lang/String;

    invoke-static {v0}, Lorg/icepdf/core/pobjects/filters/CCITTFax;->convertStringArrayToCodeArray2D([Ljava/lang/String;)[[Lorg/icepdf/core/pobjects/filters/CCITTFax$Code;

    move-result-object v0

    sput-object v0, Lorg/icepdf/core/pobjects/filters/CCITTFax;->mwcodes:[[Lorg/icepdf/core/pobjects/filters/CCITTFax$Code;

    .line 183
    sget-object v0, Lorg/icepdf/core/pobjects/filters/CCITTFax;->_tbcodes:[Ljava/lang/String;

    invoke-static {v0}, Lorg/icepdf/core/pobjects/filters/CCITTFax;->convertStringArrayToCodeArray2D([Ljava/lang/String;)[[Lorg/icepdf/core/pobjects/filters/CCITTFax$Code;

    move-result-object v0

    sput-object v0, Lorg/icepdf/core/pobjects/filters/CCITTFax;->tbcodes:[[Lorg/icepdf/core/pobjects/filters/CCITTFax$Code;

    .line 184
    sget-object v0, Lorg/icepdf/core/pobjects/filters/CCITTFax;->_mbcodes:[Ljava/lang/String;

    invoke-static {v0}, Lorg/icepdf/core/pobjects/filters/CCITTFax;->convertStringArrayToCodeArray2D([Ljava/lang/String;)[[Lorg/icepdf/core/pobjects/filters/CCITTFax$Code;

    move-result-object v0

    sput-object v0, Lorg/icepdf/core/pobjects/filters/CCITTFax;->mbcodes:[[Lorg/icepdf/core/pobjects/filters/CCITTFax$Code;

    .line 185
    sget-object v0, Lorg/icepdf/core/pobjects/filters/CCITTFax;->_extmcodes:[Ljava/lang/String;

    invoke-static {v0}, Lorg/icepdf/core/pobjects/filters/CCITTFax;->convertStringArrayToCodeArray2D([Ljava/lang/String;)[[Lorg/icepdf/core/pobjects/filters/CCITTFax$Code;

    move-result-object v0

    sput-object v0, Lorg/icepdf/core/pobjects/filters/CCITTFax;->extmcodes:[[Lorg/icepdf/core/pobjects/filters/CCITTFax$Code;

    .line 186
    sget-object v0, Lorg/icepdf/core/pobjects/filters/CCITTFax;->_modecodes:[Ljava/lang/String;

    invoke-static {v0}, Lorg/icepdf/core/pobjects/filters/CCITTFax;->convertStringArrayToCodeArray2D([Ljava/lang/String;)[[Lorg/icepdf/core/pobjects/filters/CCITTFax$Code;

    move-result-object v0

    sput-object v0, Lorg/icepdf/core/pobjects/filters/CCITTFax;->modecodes:[[Lorg/icepdf/core/pobjects/filters/CCITTFax$Code;

    .line 243
    sput v3, Lorg/icepdf/core/pobjects/filters/CCITTFax;->black:I

    .line 244
    sput v4, Lorg/icepdf/core/pobjects/filters/CCITTFax;->white:I

    .line 287
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 128
    return-void
.end method

.method public static Group4Decode(Ljava/io/InputStream;Ljava/io/OutputStream;IZ)V
    .locals 12
    .param p0, "in"    # Ljava/io/InputStream;
    .param p1, "out"    # Ljava/io/OutputStream;
    .param p2, "width"    # I
    .param p3, "blackIs1"    # Z

    .prologue
    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 497
    new-instance v3, Lorg/icepdf/core/io/BitStream;

    invoke-direct {v3, p0}, Lorg/icepdf/core/io/BitStream;-><init>(Ljava/io/InputStream;)V

    .line 498
    .local v3, "inb":Lorg/icepdf/core/io/BitStream;
    new-instance v5, Lorg/icepdf/core/io/BitStream;

    invoke-direct {v5, p1}, Lorg/icepdf/core/io/BitStream;-><init>(Ljava/io/OutputStream;)V

    .line 500
    .local v5, "outb":Lorg/icepdf/core/io/BitStream;
    sput v8, Lorg/icepdf/core/pobjects/filters/CCITTFax;->black:I

    .line 501
    sput v7, Lorg/icepdf/core/pobjects/filters/CCITTFax;->white:I

    .line 504
    if-eqz p3, :cond_0

    .line 505
    sput v7, Lorg/icepdf/core/pobjects/filters/CCITTFax;->black:I

    .line 506
    sput v8, Lorg/icepdf/core/pobjects/filters/CCITTFax;->white:I

    .line 509
    :cond_0
    new-instance v0, Lorg/icepdf/core/pobjects/filters/CCITTFax$Code;

    invoke-direct {v0}, Lorg/icepdf/core/pobjects/filters/CCITTFax$Code;-><init>()V

    .line 512
    .local v0, "code":Lorg/icepdf/core/pobjects/filters/CCITTFax$Code;
    :try_start_0
    new-instance v2, Lorg/icepdf/core/pobjects/filters/G4State;

    invoke-direct {v2, p2}, Lorg/icepdf/core/pobjects/filters/G4State;-><init>(I)V

    .line 513
    .local v2, "graphicState":Lorg/icepdf/core/pobjects/filters/G4State;
    :cond_1
    :goto_0
    invoke-virtual {v3}, Lorg/icepdf/core/io/BitStream;->atEndOfFile()Z

    move-result v6

    if-nez v6, :cond_a

    .line 514
    invoke-static {v3, v0}, Lorg/icepdf/core/pobjects/filters/CCITTFax;->readmode(Lorg/icepdf/core/io/BitStream;Lorg/icepdf/core/pobjects/filters/CCITTFax$Code;)I

    move-result v4

    .line 515
    .local v4, "mode":I
    packed-switch v4, :pswitch_data_0

    .line 575
    :cond_2
    :goto_1
    :pswitch_0
    iget v6, v2, Lorg/icepdf/core/pobjects/filters/G4State;->a0:I

    iget v9, v2, Lorg/icepdf/core/pobjects/filters/G4State;->width:I

    if-lt v6, v9, :cond_1

    .line 576
    invoke-static {v5, v2}, Lorg/icepdf/core/pobjects/filters/CCITTFax;->resetRuns(Lorg/icepdf/core/io/BitStream;Lorg/icepdf/core/pobjects/filters/G4State;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 585
    .end local v2    # "graphicState":Lorg/icepdf/core/pobjects/filters/G4State;
    .end local v4    # "mode":I
    :catch_0
    move-exception v1

    .line 586
    .local v1, "e":Ljava/lang/Exception;
    const-string/jumbo v6, "DocumentService"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "Exception: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 588
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_2
    return-void

    .line 517
    .restart local v2    # "graphicState":Lorg/icepdf/core/pobjects/filters/G4State;
    .restart local v4    # "mode":I
    :pswitch_1
    :try_start_1
    invoke-static {v2}, Lorg/icepdf/core/pobjects/filters/CCITTFax;->decodePass(Lorg/icepdf/core/pobjects/filters/G4State;)V

    goto :goto_0

    .line 520
    :pswitch_2
    invoke-static {v3, v5, v2, v0}, Lorg/icepdf/core/pobjects/filters/CCITTFax;->decodeHorizontal(Lorg/icepdf/core/io/BitStream;Lorg/icepdf/core/io/BitStream;Lorg/icepdf/core/pobjects/filters/G4State;Lorg/icepdf/core/pobjects/filters/CCITTFax$Code;)V

    .line 521
    invoke-static {v3, v5, v2, v0}, Lorg/icepdf/core/pobjects/filters/CCITTFax;->decodeHorizontal(Lorg/icepdf/core/io/BitStream;Lorg/icepdf/core/io/BitStream;Lorg/icepdf/core/pobjects/filters/G4State;Lorg/icepdf/core/pobjects/filters/CCITTFax$Code;)V

    .line 522
    invoke-static {v2}, Lorg/icepdf/core/pobjects/filters/CCITTFax;->detectB1(Lorg/icepdf/core/pobjects/filters/G4State;)V

    goto :goto_1

    .line 525
    :pswitch_3
    invoke-static {v2}, Lorg/icepdf/core/pobjects/filters/CCITTFax;->detectB1(Lorg/icepdf/core/pobjects/filters/G4State;)V

    .line 526
    iget v6, v2, Lorg/icepdf/core/pobjects/filters/G4State;->b1:I

    iget v9, v2, Lorg/icepdf/core/pobjects/filters/G4State;->a0:I

    sub-int/2addr v6, v9

    invoke-static {v6, v2, v5}, Lorg/icepdf/core/pobjects/filters/CCITTFax;->addRun(ILorg/icepdf/core/pobjects/filters/G4State;Lorg/icepdf/core/io/BitStream;)V

    .line 527
    iget-boolean v6, v2, Lorg/icepdf/core/pobjects/filters/G4State;->white:Z

    if-nez v6, :cond_3

    move v6, v7

    :goto_3
    iput-boolean v6, v2, Lorg/icepdf/core/pobjects/filters/G4State;->white:Z

    .line 528
    iget v6, v2, Lorg/icepdf/core/pobjects/filters/G4State;->b1:I

    iget-object v9, v2, Lorg/icepdf/core/pobjects/filters/G4State;->ref:[I

    iget v10, v2, Lorg/icepdf/core/pobjects/filters/G4State;->refIndex:I

    add-int/lit8 v11, v10, 0x1

    iput v11, v2, Lorg/icepdf/core/pobjects/filters/G4State;->refIndex:I

    aget v9, v9, v10

    add-int/2addr v6, v9

    iput v6, v2, Lorg/icepdf/core/pobjects/filters/G4State;->b1:I

    goto :goto_1

    :cond_3
    move v6, v8

    .line 527
    goto :goto_3

    .line 531
    :pswitch_4
    invoke-static {v2}, Lorg/icepdf/core/pobjects/filters/CCITTFax;->detectB1(Lorg/icepdf/core/pobjects/filters/G4State;)V

    .line 532
    iget v6, v2, Lorg/icepdf/core/pobjects/filters/G4State;->b1:I

    iget v9, v2, Lorg/icepdf/core/pobjects/filters/G4State;->a0:I

    sub-int/2addr v6, v9

    add-int/lit8 v6, v6, 0x1

    invoke-static {v6, v2, v5}, Lorg/icepdf/core/pobjects/filters/CCITTFax;->addRun(ILorg/icepdf/core/pobjects/filters/G4State;Lorg/icepdf/core/io/BitStream;)V

    .line 533
    iget-boolean v6, v2, Lorg/icepdf/core/pobjects/filters/G4State;->white:Z

    if-nez v6, :cond_4

    move v6, v7

    :goto_4
    iput-boolean v6, v2, Lorg/icepdf/core/pobjects/filters/G4State;->white:Z

    .line 534
    iget v6, v2, Lorg/icepdf/core/pobjects/filters/G4State;->b1:I

    iget-object v9, v2, Lorg/icepdf/core/pobjects/filters/G4State;->ref:[I

    iget v10, v2, Lorg/icepdf/core/pobjects/filters/G4State;->refIndex:I

    add-int/lit8 v11, v10, 0x1

    iput v11, v2, Lorg/icepdf/core/pobjects/filters/G4State;->refIndex:I

    aget v9, v9, v10

    add-int/2addr v6, v9

    iput v6, v2, Lorg/icepdf/core/pobjects/filters/G4State;->b1:I

    goto :goto_1

    :cond_4
    move v6, v8

    .line 533
    goto :goto_4

    .line 537
    :pswitch_5
    invoke-static {v2}, Lorg/icepdf/core/pobjects/filters/CCITTFax;->detectB1(Lorg/icepdf/core/pobjects/filters/G4State;)V

    .line 538
    iget v6, v2, Lorg/icepdf/core/pobjects/filters/G4State;->b1:I

    iget v9, v2, Lorg/icepdf/core/pobjects/filters/G4State;->a0:I

    sub-int/2addr v6, v9

    add-int/lit8 v6, v6, 0x2

    invoke-static {v6, v2, v5}, Lorg/icepdf/core/pobjects/filters/CCITTFax;->addRun(ILorg/icepdf/core/pobjects/filters/G4State;Lorg/icepdf/core/io/BitStream;)V

    .line 539
    iget-boolean v6, v2, Lorg/icepdf/core/pobjects/filters/G4State;->white:Z

    if-nez v6, :cond_5

    move v6, v7

    :goto_5
    iput-boolean v6, v2, Lorg/icepdf/core/pobjects/filters/G4State;->white:Z

    .line 540
    iget v6, v2, Lorg/icepdf/core/pobjects/filters/G4State;->b1:I

    iget-object v9, v2, Lorg/icepdf/core/pobjects/filters/G4State;->ref:[I

    iget v10, v2, Lorg/icepdf/core/pobjects/filters/G4State;->refIndex:I

    add-int/lit8 v11, v10, 0x1

    iput v11, v2, Lorg/icepdf/core/pobjects/filters/G4State;->refIndex:I

    aget v9, v9, v10

    add-int/2addr v6, v9

    iput v6, v2, Lorg/icepdf/core/pobjects/filters/G4State;->b1:I

    goto/16 :goto_1

    :cond_5
    move v6, v8

    .line 539
    goto :goto_5

    .line 543
    :pswitch_6
    invoke-static {v2}, Lorg/icepdf/core/pobjects/filters/CCITTFax;->detectB1(Lorg/icepdf/core/pobjects/filters/G4State;)V

    .line 544
    iget v6, v2, Lorg/icepdf/core/pobjects/filters/G4State;->b1:I

    iget v9, v2, Lorg/icepdf/core/pobjects/filters/G4State;->a0:I

    sub-int/2addr v6, v9

    add-int/lit8 v6, v6, 0x3

    invoke-static {v6, v2, v5}, Lorg/icepdf/core/pobjects/filters/CCITTFax;->addRun(ILorg/icepdf/core/pobjects/filters/G4State;Lorg/icepdf/core/io/BitStream;)V

    .line 545
    iget-boolean v6, v2, Lorg/icepdf/core/pobjects/filters/G4State;->white:Z

    if-nez v6, :cond_6

    move v6, v7

    :goto_6
    iput-boolean v6, v2, Lorg/icepdf/core/pobjects/filters/G4State;->white:Z

    .line 546
    iget v6, v2, Lorg/icepdf/core/pobjects/filters/G4State;->b1:I

    iget-object v9, v2, Lorg/icepdf/core/pobjects/filters/G4State;->ref:[I

    iget v10, v2, Lorg/icepdf/core/pobjects/filters/G4State;->refIndex:I

    add-int/lit8 v11, v10, 0x1

    iput v11, v2, Lorg/icepdf/core/pobjects/filters/G4State;->refIndex:I

    aget v9, v9, v10

    add-int/2addr v6, v9

    iput v6, v2, Lorg/icepdf/core/pobjects/filters/G4State;->b1:I

    goto/16 :goto_1

    :cond_6
    move v6, v8

    .line 545
    goto :goto_6

    .line 549
    :pswitch_7
    invoke-static {v2}, Lorg/icepdf/core/pobjects/filters/CCITTFax;->detectB1(Lorg/icepdf/core/pobjects/filters/G4State;)V

    .line 550
    iget v6, v2, Lorg/icepdf/core/pobjects/filters/G4State;->b1:I

    iget v9, v2, Lorg/icepdf/core/pobjects/filters/G4State;->a0:I

    sub-int/2addr v6, v9

    add-int/lit8 v6, v6, -0x1

    invoke-static {v6, v2, v5}, Lorg/icepdf/core/pobjects/filters/CCITTFax;->addRun(ILorg/icepdf/core/pobjects/filters/G4State;Lorg/icepdf/core/io/BitStream;)V

    .line 551
    iget-boolean v6, v2, Lorg/icepdf/core/pobjects/filters/G4State;->white:Z

    if-nez v6, :cond_7

    move v6, v7

    :goto_7
    iput-boolean v6, v2, Lorg/icepdf/core/pobjects/filters/G4State;->white:Z

    .line 552
    iget v6, v2, Lorg/icepdf/core/pobjects/filters/G4State;->refIndex:I

    if-lez v6, :cond_2

    .line 553
    iget v6, v2, Lorg/icepdf/core/pobjects/filters/G4State;->b1:I

    iget-object v9, v2, Lorg/icepdf/core/pobjects/filters/G4State;->ref:[I

    iget v10, v2, Lorg/icepdf/core/pobjects/filters/G4State;->refIndex:I

    add-int/lit8 v10, v10, -0x1

    iput v10, v2, Lorg/icepdf/core/pobjects/filters/G4State;->refIndex:I

    aget v9, v9, v10

    sub-int/2addr v6, v9

    iput v6, v2, Lorg/icepdf/core/pobjects/filters/G4State;->b1:I

    goto/16 :goto_1

    :cond_7
    move v6, v8

    .line 551
    goto :goto_7

    .line 556
    :pswitch_8
    invoke-static {v2}, Lorg/icepdf/core/pobjects/filters/CCITTFax;->detectB1(Lorg/icepdf/core/pobjects/filters/G4State;)V

    .line 557
    iget v6, v2, Lorg/icepdf/core/pobjects/filters/G4State;->b1:I

    iget v9, v2, Lorg/icepdf/core/pobjects/filters/G4State;->a0:I

    sub-int/2addr v6, v9

    add-int/lit8 v6, v6, -0x2

    invoke-static {v6, v2, v5}, Lorg/icepdf/core/pobjects/filters/CCITTFax;->addRun(ILorg/icepdf/core/pobjects/filters/G4State;Lorg/icepdf/core/io/BitStream;)V

    .line 558
    iget-boolean v6, v2, Lorg/icepdf/core/pobjects/filters/G4State;->white:Z

    if-nez v6, :cond_8

    move v6, v7

    :goto_8
    iput-boolean v6, v2, Lorg/icepdf/core/pobjects/filters/G4State;->white:Z

    .line 559
    iget v6, v2, Lorg/icepdf/core/pobjects/filters/G4State;->refIndex:I

    if-lez v6, :cond_2

    .line 560
    iget v6, v2, Lorg/icepdf/core/pobjects/filters/G4State;->b1:I

    iget-object v9, v2, Lorg/icepdf/core/pobjects/filters/G4State;->ref:[I

    iget v10, v2, Lorg/icepdf/core/pobjects/filters/G4State;->refIndex:I

    add-int/lit8 v10, v10, -0x1

    iput v10, v2, Lorg/icepdf/core/pobjects/filters/G4State;->refIndex:I

    aget v9, v9, v10

    sub-int/2addr v6, v9

    iput v6, v2, Lorg/icepdf/core/pobjects/filters/G4State;->b1:I

    goto/16 :goto_1

    :cond_8
    move v6, v8

    .line 558
    goto :goto_8

    .line 563
    :pswitch_9
    invoke-static {v2}, Lorg/icepdf/core/pobjects/filters/CCITTFax;->detectB1(Lorg/icepdf/core/pobjects/filters/G4State;)V

    .line 564
    iget v6, v2, Lorg/icepdf/core/pobjects/filters/G4State;->b1:I

    iget v9, v2, Lorg/icepdf/core/pobjects/filters/G4State;->a0:I

    sub-int/2addr v6, v9

    add-int/lit8 v6, v6, -0x3

    invoke-static {v6, v2, v5}, Lorg/icepdf/core/pobjects/filters/CCITTFax;->addRun(ILorg/icepdf/core/pobjects/filters/G4State;Lorg/icepdf/core/io/BitStream;)V

    .line 565
    iget-boolean v6, v2, Lorg/icepdf/core/pobjects/filters/G4State;->white:Z

    if-nez v6, :cond_9

    move v6, v7

    :goto_9
    iput-boolean v6, v2, Lorg/icepdf/core/pobjects/filters/G4State;->white:Z

    .line 566
    iget v6, v2, Lorg/icepdf/core/pobjects/filters/G4State;->refIndex:I

    if-lez v6, :cond_2

    .line 567
    iget v6, v2, Lorg/icepdf/core/pobjects/filters/G4State;->b1:I

    iget-object v9, v2, Lorg/icepdf/core/pobjects/filters/G4State;->ref:[I

    iget v10, v2, Lorg/icepdf/core/pobjects/filters/G4State;->refIndex:I

    add-int/lit8 v10, v10, -0x1

    iput v10, v2, Lorg/icepdf/core/pobjects/filters/G4State;->refIndex:I

    aget v9, v9, v10

    sub-int/2addr v6, v9

    iput v6, v2, Lorg/icepdf/core/pobjects/filters/G4State;->b1:I

    goto/16 :goto_1

    :cond_9
    move v6, v8

    .line 565
    goto :goto_9

    .line 570
    :pswitch_a
    invoke-static {v5, v2}, Lorg/icepdf/core/pobjects/filters/CCITTFax;->resetRuns(Lorg/icepdf/core/io/BitStream;Lorg/icepdf/core/pobjects/filters/G4State;)V

    goto/16 :goto_1

    .line 580
    .end local v4    # "mode":I
    :cond_a
    invoke-virtual {v3}, Lorg/icepdf/core/io/BitStream;->close()V

    .line 581
    invoke-virtual {v5}, Lorg/icepdf/core/io/BitStream;->close()V

    .line 582
    invoke-virtual {p0}, Ljava/io/InputStream;->close()V

    .line 584
    invoke-virtual {p1}, Ljava/io/OutputStream;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_2

    .line 515
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_0
        :pswitch_0
        :pswitch_a
    .end packed-switch
.end method

.method static addRun(ILorg/icepdf/core/pobjects/filters/G4State;Lorg/icepdf/core/io/BitStream;)V
    .locals 3
    .param p0, "x"    # I
    .param p1, "s"    # Lorg/icepdf/core/pobjects/filters/G4State;
    .param p2, "out"    # Lorg/icepdf/core/io/BitStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 364
    iget v0, p1, Lorg/icepdf/core/pobjects/filters/G4State;->runLength:I

    add-int/2addr v0, p0

    iput v0, p1, Lorg/icepdf/core/pobjects/filters/G4State;->runLength:I

    .line 365
    iget-object v0, p1, Lorg/icepdf/core/pobjects/filters/G4State;->cur:[I

    iget v1, p1, Lorg/icepdf/core/pobjects/filters/G4State;->curIndex:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p1, Lorg/icepdf/core/pobjects/filters/G4State;->curIndex:I

    iget v2, p1, Lorg/icepdf/core/pobjects/filters/G4State;->runLength:I

    aput v2, v0, v1

    .line 366
    iget v0, p1, Lorg/icepdf/core/pobjects/filters/G4State;->a0:I

    add-int/2addr v0, p0

    iput v0, p1, Lorg/icepdf/core/pobjects/filters/G4State;->a0:I

    .line 367
    iget v0, p1, Lorg/icepdf/core/pobjects/filters/G4State;->runLength:I

    if-lez v0, :cond_0

    .line 369
    iget-boolean v0, p1, Lorg/icepdf/core/pobjects/filters/G4State;->white:Z

    if-eqz v0, :cond_1

    sget v0, Lorg/icepdf/core/pobjects/filters/CCITTFax;->white:I

    :goto_0
    iget v1, p1, Lorg/icepdf/core/pobjects/filters/G4State;->runLength:I

    invoke-virtual {p2, v0, v1}, Lorg/icepdf/core/io/BitStream;->putRunBits(II)V

    .line 371
    :cond_0
    invoke-virtual {p2}, Lorg/icepdf/core/io/BitStream;->close()V

    .line 372
    const/4 v0, 0x0

    iput v0, p1, Lorg/icepdf/core/pobjects/filters/G4State;->runLength:I

    .line 373
    return-void

    .line 369
    :cond_1
    sget v0, Lorg/icepdf/core/pobjects/filters/CCITTFax;->black:I

    goto :goto_0
.end method

.method public static attemptDeriveBufferedImageFromBytes(Lorg/icepdf/core/pobjects/Stream;Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;Lorg/apache/poi/java/awt/Color;)Landroid/graphics/Bitmap;
    .locals 1
    .param p0, "stream"    # Lorg/icepdf/core/pobjects/Stream;
    .param p1, "library"    # Lorg/icepdf/core/util/Library;
    .param p2, "streamDictionary"    # Ljava/util/Hashtable;
    .param p3, "fill"    # Lorg/apache/poi/java/awt/Color;

    .prologue
    .line 765
    const/4 v0, 0x0

    return-object v0
.end method

.method private static convertStringArrayToCodeArray2D([Ljava/lang/String;)[[Lorg/icepdf/core/pobjects/filters/CCITTFax$Code;
    .locals 10
    .param p0, "strArray"    # [Ljava/lang/String;

    .prologue
    .line 189
    array-length v7, p0

    .line 192
    .local v7, "len":I
    const/16 v8, 0x40

    new-array v1, v8, [I

    .line 193
    .local v1, "codeLengths":[I
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    if-ge v4, v7, :cond_0

    .line 194
    aget-object v8, p0, v4

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v3

    .line 195
    .local v3, "entryLength":I
    aget v8, v1, v3

    add-int/lit8 v8, v8, 0x1

    aput v8, v1, v3

    .line 193
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 204
    .end local v3    # "entryLength":I
    :cond_0
    array-length v8, v1

    add-int/lit8 v6, v8, -0x1

    .line 205
    .local v6, "largestLength":I
    :goto_1
    if-lez v6, :cond_1

    aget v8, v1, v6

    if-nez v8, :cond_1

    .line 206
    add-int/lit8 v6, v6, -0x1

    goto :goto_1

    .line 207
    :cond_1
    add-int/lit8 v8, v6, 0x1

    new-array v0, v8, [[Lorg/icepdf/core/pobjects/filters/CCITTFax$Code;

    .line 208
    .local v0, "codeArray":[[Lorg/icepdf/core/pobjects/filters/CCITTFax$Code;
    const/4 v4, 0x0

    :goto_2
    array-length v8, v0

    if-ge v4, v8, :cond_2

    .line 209
    aget v8, v1, v4

    new-array v8, v8, [Lorg/icepdf/core/pobjects/filters/CCITTFax$Code;

    aput-object v8, v0, v4

    .line 208
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 211
    :cond_2
    const/4 v4, 0x0

    :goto_3
    if-ge v4, v7, :cond_5

    .line 212
    aget-object v8, p0, v4

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v3

    .line 213
    .restart local v3    # "entryLength":I
    aget-object v2, v0, v3

    .line 214
    .local v2, "entries":[Lorg/icepdf/core/pobjects/filters/CCITTFax$Code;
    const/4 v5, 0x0

    .local v5, "j":I
    :goto_4
    array-length v8, v2

    if-ge v5, v8, :cond_3

    .line 215
    aget-object v8, v2, v5

    if-nez v8, :cond_4

    .line 216
    new-instance v8, Lorg/icepdf/core/pobjects/filters/CCITTFax$Code;

    aget-object v9, p0, v4

    invoke-direct {v8, v9, v4}, Lorg/icepdf/core/pobjects/filters/CCITTFax$Code;-><init>(Ljava/lang/String;I)V

    aput-object v8, v2, v5

    .line 211
    :cond_3
    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    .line 214
    :cond_4
    add-int/lit8 v5, v5, 0x1

    goto :goto_4

    .line 222
    .end local v2    # "entries":[Lorg/icepdf/core/pobjects/filters/CCITTFax$Code;
    .end local v3    # "entryLength":I
    .end local v5    # "j":I
    :cond_5
    return-object v0
.end method

.method static decodeHorizontal(Lorg/icepdf/core/io/BitStream;Lorg/icepdf/core/io/BitStream;Lorg/icepdf/core/pobjects/filters/G4State;Lorg/icepdf/core/pobjects/filters/CCITTFax$Code;)V
    .locals 4
    .param p0, "in"    # Lorg/icepdf/core/io/BitStream;
    .param p1, "out"    # Lorg/icepdf/core/io/BitStream;
    .param p2, "s"    # Lorg/icepdf/core/pobjects/filters/G4State;
    .param p3, "code"    # Lorg/icepdf/core/pobjects/filters/CCITTFax$Code;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v3, 0x40

    const/4 v2, 0x0

    .line 432
    const/4 v0, 0x0

    .line 434
    .local v0, "rl":I
    :cond_0
    iget-boolean v1, p2, Lorg/icepdf/core/pobjects/filters/G4State;->white:Z

    if-eqz v1, :cond_1

    invoke-static {p0, p3}, Lorg/icepdf/core/pobjects/filters/CCITTFax;->findWhite(Lorg/icepdf/core/io/BitStream;Lorg/icepdf/core/pobjects/filters/CCITTFax$Code;)I

    move-result v0

    .line 435
    :goto_0
    if-ltz v0, :cond_4

    .line 436
    if-ge v0, v3, :cond_3

    .line 437
    iget v1, p2, Lorg/icepdf/core/pobjects/filters/G4State;->longrun:I

    add-int/2addr v1, v0

    invoke-static {v1, p2, p1}, Lorg/icepdf/core/pobjects/filters/CCITTFax;->addRun(ILorg/icepdf/core/pobjects/filters/G4State;Lorg/icepdf/core/io/BitStream;)V

    .line 438
    iget-boolean v1, p2, Lorg/icepdf/core/pobjects/filters/G4State;->white:Z

    if-nez v1, :cond_2

    const/4 v1, 0x1

    :goto_1
    iput-boolean v1, p2, Lorg/icepdf/core/pobjects/filters/G4State;->white:Z

    .line 439
    iput v2, p2, Lorg/icepdf/core/pobjects/filters/G4State;->longrun:I

    .line 446
    :goto_2
    if-ge v0, v3, :cond_0

    .line 447
    invoke-virtual {p1}, Lorg/icepdf/core/io/BitStream;->close()V

    .line 448
    return-void

    .line 434
    :cond_1
    invoke-static {p0, p3}, Lorg/icepdf/core/pobjects/filters/CCITTFax;->findBlack(Lorg/icepdf/core/io/BitStream;Lorg/icepdf/core/pobjects/filters/CCITTFax$Code;)I

    move-result v0

    goto :goto_0

    :cond_2
    move v1, v2

    .line 438
    goto :goto_1

    .line 441
    :cond_3
    iget v1, p2, Lorg/icepdf/core/pobjects/filters/G4State;->longrun:I

    add-int/2addr v1, v0

    iput v1, p2, Lorg/icepdf/core/pobjects/filters/G4State;->longrun:I

    goto :goto_2

    .line 444
    :cond_4
    invoke-static {v0, p2, p1}, Lorg/icepdf/core/pobjects/filters/CCITTFax;->addRun(ILorg/icepdf/core/pobjects/filters/G4State;Lorg/icepdf/core/io/BitStream;)V

    goto :goto_2
.end method

.method static decodePass(Lorg/icepdf/core/pobjects/filters/G4State;)V
    .locals 4
    .param p0, "s"    # Lorg/icepdf/core/pobjects/filters/G4State;

    .prologue
    .line 418
    invoke-static {p0}, Lorg/icepdf/core/pobjects/filters/CCITTFax;->detectB1(Lorg/icepdf/core/pobjects/filters/G4State;)V

    .line 419
    iget v0, p0, Lorg/icepdf/core/pobjects/filters/G4State;->b1:I

    iget-object v1, p0, Lorg/icepdf/core/pobjects/filters/G4State;->ref:[I

    iget v2, p0, Lorg/icepdf/core/pobjects/filters/G4State;->refIndex:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lorg/icepdf/core/pobjects/filters/G4State;->refIndex:I

    aget v1, v1, v2

    add-int/2addr v0, v1

    iput v0, p0, Lorg/icepdf/core/pobjects/filters/G4State;->b1:I

    .line 420
    iget v0, p0, Lorg/icepdf/core/pobjects/filters/G4State;->runLength:I

    iget v1, p0, Lorg/icepdf/core/pobjects/filters/G4State;->b1:I

    iget v2, p0, Lorg/icepdf/core/pobjects/filters/G4State;->a0:I

    sub-int/2addr v1, v2

    add-int/2addr v0, v1

    iput v0, p0, Lorg/icepdf/core/pobjects/filters/G4State;->runLength:I

    .line 421
    iget v0, p0, Lorg/icepdf/core/pobjects/filters/G4State;->b1:I

    iput v0, p0, Lorg/icepdf/core/pobjects/filters/G4State;->a0:I

    .line 422
    iget v0, p0, Lorg/icepdf/core/pobjects/filters/G4State;->b1:I

    iget-object v1, p0, Lorg/icepdf/core/pobjects/filters/G4State;->ref:[I

    iget v2, p0, Lorg/icepdf/core/pobjects/filters/G4State;->refIndex:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lorg/icepdf/core/pobjects/filters/G4State;->refIndex:I

    aget v1, v1, v2

    add-int/2addr v0, v1

    iput v0, p0, Lorg/icepdf/core/pobjects/filters/G4State;->b1:I

    .line 423
    return-void
.end method

.method static detectB1(Lorg/icepdf/core/pobjects/filters/G4State;)V
    .locals 4
    .param p0, "s"    # Lorg/icepdf/core/pobjects/filters/G4State;

    .prologue
    .line 398
    iget v1, p0, Lorg/icepdf/core/pobjects/filters/G4State;->curIndex:I

    if-eqz v1, :cond_2

    .line 399
    :cond_0
    :goto_0
    iget v1, p0, Lorg/icepdf/core/pobjects/filters/G4State;->b1:I

    iget v2, p0, Lorg/icepdf/core/pobjects/filters/G4State;->a0:I

    if-gt v1, v2, :cond_2

    iget v1, p0, Lorg/icepdf/core/pobjects/filters/G4State;->b1:I

    iget v2, p0, Lorg/icepdf/core/pobjects/filters/G4State;->width:I

    if-ge v1, v2, :cond_2

    .line 400
    iget-object v1, p0, Lorg/icepdf/core/pobjects/filters/G4State;->ref:[I

    iget v2, p0, Lorg/icepdf/core/pobjects/filters/G4State;->refIndex:I

    aget v1, v1, v2

    iget-object v2, p0, Lorg/icepdf/core/pobjects/filters/G4State;->ref:[I

    iget v3, p0, Lorg/icepdf/core/pobjects/filters/G4State;->refIndex:I

    add-int/lit8 v3, v3, 0x1

    aget v2, v2, v3

    add-int v0, v1, v2

    .line 401
    .local v0, "r":I
    if-nez v0, :cond_1

    .line 402
    iget v1, p0, Lorg/icepdf/core/pobjects/filters/G4State;->width:I

    iput v1, p0, Lorg/icepdf/core/pobjects/filters/G4State;->b1:I

    .line 403
    :cond_1
    iget v1, p0, Lorg/icepdf/core/pobjects/filters/G4State;->b1:I

    add-int/2addr v1, v0

    iput v1, p0, Lorg/icepdf/core/pobjects/filters/G4State;->b1:I

    .line 404
    iget v1, p0, Lorg/icepdf/core/pobjects/filters/G4State;->refIndex:I

    add-int/lit8 v1, v1, 0x2

    iget-object v2, p0, Lorg/icepdf/core/pobjects/filters/G4State;->ref:[I

    array-length v2, v2

    if-ge v1, v2, :cond_0

    .line 405
    iget v1, p0, Lorg/icepdf/core/pobjects/filters/G4State;->refIndex:I

    add-int/lit8 v1, v1, 0x2

    iput v1, p0, Lorg/icepdf/core/pobjects/filters/G4State;->refIndex:I

    goto :goto_0

    .line 412
    .end local v0    # "r":I
    :cond_2
    return-void
.end method

.method static findBlack(Lorg/icepdf/core/io/BitStream;Lorg/icepdf/core/pobjects/filters/CCITTFax$Code;)I
    .locals 5
    .param p0, "inb"    # Lorg/icepdf/core/io/BitStream;
    .param p1, "code"    # Lorg/icepdf/core/pobjects/filters/CCITTFax$Code;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 331
    invoke-virtual {p1}, Lorg/icepdf/core/pobjects/filters/CCITTFax$Code;->reset()V

    .line 332
    :cond_0
    invoke-virtual {p0}, Lorg/icepdf/core/io/BitStream;->atEndOfFile()Z

    move-result v2

    if-nez v2, :cond_4

    .line 333
    invoke-virtual {p0, v3}, Lorg/icepdf/core/io/BitStream;->getBits(I)I

    move-result v0

    .line 334
    .local v0, "i":I
    if-eqz v0, :cond_1

    move v2, v3

    :goto_0
    invoke-virtual {p1, v2}, Lorg/icepdf/core/pobjects/filters/CCITTFax$Code;->append(Z)V

    .line 336
    sget-object v2, Lorg/icepdf/core/pobjects/filters/CCITTFax;->tbcodes:[[Lorg/icepdf/core/pobjects/filters/CCITTFax$Code;

    invoke-static {p1, v2}, Lorg/icepdf/core/pobjects/filters/CCITTFax;->findPositionInTable(Lorg/icepdf/core/pobjects/filters/CCITTFax$Code;[[Lorg/icepdf/core/pobjects/filters/CCITTFax$Code;)I

    move-result v1

    .line 337
    .local v1, "j":I
    if-ltz v1, :cond_2

    .line 354
    .end local v0    # "i":I
    .end local v1    # "j":I
    :goto_1
    return v1

    .restart local v0    # "i":I
    :cond_1
    move v2, v4

    .line 334
    goto :goto_0

    .line 341
    .restart local v1    # "j":I
    :cond_2
    sget-object v2, Lorg/icepdf/core/pobjects/filters/CCITTFax;->mbcodes:[[Lorg/icepdf/core/pobjects/filters/CCITTFax$Code;

    invoke-static {p1, v2}, Lorg/icepdf/core/pobjects/filters/CCITTFax;->findPositionInTable(Lorg/icepdf/core/pobjects/filters/CCITTFax$Code;[[Lorg/icepdf/core/pobjects/filters/CCITTFax$Code;)I

    move-result v1

    .line 342
    if-ltz v1, :cond_3

    .line 344
    add-int/lit8 v2, v1, 0x1

    mul-int/lit8 v1, v2, 0x40

    goto :goto_1

    .line 346
    :cond_3
    sget-object v2, Lorg/icepdf/core/pobjects/filters/CCITTFax;->extmcodes:[[Lorg/icepdf/core/pobjects/filters/CCITTFax$Code;

    invoke-static {p1, v2}, Lorg/icepdf/core/pobjects/filters/CCITTFax;->findPositionInTable(Lorg/icepdf/core/pobjects/filters/CCITTFax$Code;[[Lorg/icepdf/core/pobjects/filters/CCITTFax$Code;)I

    move-result v1

    .line 347
    if-ltz v1, :cond_0

    .line 349
    mul-int/lit8 v2, v1, 0x40

    add-int/lit16 v1, v2, 0x700

    goto :goto_1

    .line 352
    .end local v0    # "i":I
    .end local v1    # "j":I
    :cond_4
    invoke-virtual {p0}, Lorg/icepdf/core/io/BitStream;->close()V

    move v1, v4

    .line 354
    goto :goto_1
.end method

.method private static findPositionInTable(Lorg/icepdf/core/pobjects/filters/CCITTFax$Code;[[Lorg/icepdf/core/pobjects/filters/CCITTFax$Code;)I
    .locals 7
    .param p0, "lookFor"    # Lorg/icepdf/core/pobjects/filters/CCITTFax$Code;
    .param p1, "lookIn"    # [[Lorg/icepdf/core/pobjects/filters/CCITTFax$Code;

    .prologue
    const/4 v5, -0x1

    .line 226
    invoke-virtual {p0}, Lorg/icepdf/core/pobjects/filters/CCITTFax$Code;->getLength()I

    move-result v2

    .line 227
    .local v2, "lookForIndex":I
    array-length v6, p1

    if-lt v2, v6, :cond_1

    .line 238
    :cond_0
    :goto_0
    return v5

    .line 229
    :cond_1
    aget-object v3, p1, v2

    .line 230
    .local v3, "lookInWithSameLength":[Lorg/icepdf/core/pobjects/filters/CCITTFax$Code;
    if-eqz v3, :cond_0

    .line 232
    array-length v1, v3

    .line 233
    .local v1, "len":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    if-ge v0, v1, :cond_0

    .line 234
    aget-object v4, v3, v0

    .line 235
    .local v4, "potentialMatch":Lorg/icepdf/core/pobjects/filters/CCITTFax$Code;
    invoke-virtual {p0, v4}, Lorg/icepdf/core/pobjects/filters/CCITTFax$Code;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 236
    invoke-virtual {v4}, Lorg/icepdf/core/pobjects/filters/CCITTFax$Code;->getTablePosition()I

    move-result v5

    goto :goto_0

    .line 233
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method static findWhite(Lorg/icepdf/core/io/BitStream;Lorg/icepdf/core/pobjects/filters/CCITTFax$Code;)I
    .locals 5
    .param p0, "inb"    # Lorg/icepdf/core/io/BitStream;
    .param p1, "code"    # Lorg/icepdf/core/pobjects/filters/CCITTFax$Code;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 297
    invoke-virtual {p1}, Lorg/icepdf/core/pobjects/filters/CCITTFax$Code;->reset()V

    .line 298
    :cond_0
    invoke-virtual {p0}, Lorg/icepdf/core/io/BitStream;->atEndOfFile()Z

    move-result v2

    if-nez v2, :cond_4

    .line 299
    invoke-virtual {p0, v3}, Lorg/icepdf/core/io/BitStream;->getBits(I)I

    move-result v0

    .line 300
    .local v0, "i":I
    if-eqz v0, :cond_1

    move v2, v3

    :goto_0
    invoke-virtual {p1, v2}, Lorg/icepdf/core/pobjects/filters/CCITTFax$Code;->append(Z)V

    .line 302
    sget-object v2, Lorg/icepdf/core/pobjects/filters/CCITTFax;->twcodes:[[Lorg/icepdf/core/pobjects/filters/CCITTFax$Code;

    invoke-static {p1, v2}, Lorg/icepdf/core/pobjects/filters/CCITTFax;->findPositionInTable(Lorg/icepdf/core/pobjects/filters/CCITTFax$Code;[[Lorg/icepdf/core/pobjects/filters/CCITTFax$Code;)I

    move-result v1

    .line 303
    .local v1, "j":I
    if-ltz v1, :cond_2

    .line 320
    .end local v0    # "i":I
    .end local v1    # "j":I
    :goto_1
    return v1

    .restart local v0    # "i":I
    :cond_1
    move v2, v4

    .line 300
    goto :goto_0

    .line 307
    .restart local v1    # "j":I
    :cond_2
    sget-object v2, Lorg/icepdf/core/pobjects/filters/CCITTFax;->mwcodes:[[Lorg/icepdf/core/pobjects/filters/CCITTFax$Code;

    invoke-static {p1, v2}, Lorg/icepdf/core/pobjects/filters/CCITTFax;->findPositionInTable(Lorg/icepdf/core/pobjects/filters/CCITTFax$Code;[[Lorg/icepdf/core/pobjects/filters/CCITTFax$Code;)I

    move-result v1

    .line 308
    if-ltz v1, :cond_3

    .line 310
    add-int/lit8 v2, v1, 0x1

    mul-int/lit8 v1, v2, 0x40

    goto :goto_1

    .line 312
    :cond_3
    sget-object v2, Lorg/icepdf/core/pobjects/filters/CCITTFax;->extmcodes:[[Lorg/icepdf/core/pobjects/filters/CCITTFax$Code;

    invoke-static {p1, v2}, Lorg/icepdf/core/pobjects/filters/CCITTFax;->findPositionInTable(Lorg/icepdf/core/pobjects/filters/CCITTFax$Code;[[Lorg/icepdf/core/pobjects/filters/CCITTFax$Code;)I

    move-result v1

    .line 313
    if-ltz v1, :cond_0

    .line 315
    mul-int/lit8 v2, v1, 0x40

    add-int/lit16 v1, v2, 0x700

    goto :goto_1

    .line 318
    .end local v0    # "i":I
    .end local v1    # "j":I
    :cond_4
    invoke-virtual {p0}, Lorg/icepdf/core/io/BitStream;->close()V

    move v1, v4

    .line 320
    goto :goto_1
.end method

.method static readmode(Lorg/icepdf/core/io/BitStream;Lorg/icepdf/core/pobjects/filters/CCITTFax$Code;)I
    .locals 4
    .param p0, "inb"    # Lorg/icepdf/core/io/BitStream;
    .param p1, "code"    # Lorg/icepdf/core/pobjects/filters/CCITTFax$Code;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 381
    invoke-virtual {p1}, Lorg/icepdf/core/pobjects/filters/CCITTFax$Code;->reset()V

    .line 382
    :cond_0
    invoke-virtual {p0}, Lorg/icepdf/core/io/BitStream;->atEndOfFile()Z

    move-result v2

    if-nez v2, :cond_2

    .line 383
    invoke-virtual {p0, v3}, Lorg/icepdf/core/io/BitStream;->getBits(I)I

    move-result v0

    .line 384
    .local v0, "i":I
    if-eqz v0, :cond_1

    move v2, v3

    :goto_0
    invoke-virtual {p1, v2}, Lorg/icepdf/core/pobjects/filters/CCITTFax$Code;->append(Z)V

    .line 385
    sget-object v2, Lorg/icepdf/core/pobjects/filters/CCITTFax;->modecodes:[[Lorg/icepdf/core/pobjects/filters/CCITTFax$Code;

    invoke-static {p1, v2}, Lorg/icepdf/core/pobjects/filters/CCITTFax;->findPositionInTable(Lorg/icepdf/core/pobjects/filters/CCITTFax$Code;[[Lorg/icepdf/core/pobjects/filters/CCITTFax$Code;)I

    move-result v1

    .line 386
    .local v1, "j":I
    if-ltz v1, :cond_0

    .line 391
    .end local v0    # "i":I
    .end local v1    # "j":I
    :goto_1
    return v1

    .line 384
    .restart local v0    # "i":I
    :cond_1
    const/4 v2, 0x0

    goto :goto_0

    .line 390
    .end local v0    # "i":I
    :cond_2
    invoke-virtual {p0}, Lorg/icepdf/core/io/BitStream;->close()V

    .line 391
    const/4 v1, -0x1

    goto :goto_1
.end method

.method static resetRuns(Lorg/icepdf/core/io/BitStream;Lorg/icepdf/core/pobjects/filters/G4State;)V
    .locals 7
    .param p0, "outb"    # Lorg/icepdf/core/io/BitStream;
    .param p1, "state"    # Lorg/icepdf/core/pobjects/filters/G4State;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 457
    iput-boolean v6, p1, Lorg/icepdf/core/pobjects/filters/G4State;->white:Z

    .line 458
    invoke-static {v5, p1, p0}, Lorg/icepdf/core/pobjects/filters/CCITTFax;->addRun(ILorg/icepdf/core/pobjects/filters/G4State;Lorg/icepdf/core/io/BitStream;)V

    .line 459
    iget v2, p1, Lorg/icepdf/core/pobjects/filters/G4State;->a0:I

    iget v3, p1, Lorg/icepdf/core/pobjects/filters/G4State;->width:I

    if-eq v2, v3, :cond_3

    .line 461
    :goto_0
    iget v2, p1, Lorg/icepdf/core/pobjects/filters/G4State;->a0:I

    iget v3, p1, Lorg/icepdf/core/pobjects/filters/G4State;->width:I

    if-le v2, v3, :cond_0

    .line 462
    iget v2, p1, Lorg/icepdf/core/pobjects/filters/G4State;->a0:I

    iget-object v3, p1, Lorg/icepdf/core/pobjects/filters/G4State;->cur:[I

    iget v4, p1, Lorg/icepdf/core/pobjects/filters/G4State;->curIndex:I

    add-int/lit8 v4, v4, -0x1

    iput v4, p1, Lorg/icepdf/core/pobjects/filters/G4State;->curIndex:I

    aget v3, v3, v4

    sub-int/2addr v2, v3

    iput v2, p1, Lorg/icepdf/core/pobjects/filters/G4State;->a0:I

    goto :goto_0

    .line 463
    :cond_0
    iget v2, p1, Lorg/icepdf/core/pobjects/filters/G4State;->a0:I

    iget v3, p1, Lorg/icepdf/core/pobjects/filters/G4State;->width:I

    if-ge v2, v3, :cond_4

    .line 464
    iget v2, p1, Lorg/icepdf/core/pobjects/filters/G4State;->a0:I

    if-gez v2, :cond_1

    .line 465
    iput v5, p1, Lorg/icepdf/core/pobjects/filters/G4State;->a0:I

    .line 466
    :cond_1
    iget v2, p1, Lorg/icepdf/core/pobjects/filters/G4State;->curIndex:I

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_2

    .line 467
    invoke-static {v5, p1, p0}, Lorg/icepdf/core/pobjects/filters/CCITTFax;->addRun(ILorg/icepdf/core/pobjects/filters/G4State;Lorg/icepdf/core/io/BitStream;)V

    .line 468
    :cond_2
    iget v2, p1, Lorg/icepdf/core/pobjects/filters/G4State;->width:I

    iget v3, p1, Lorg/icepdf/core/pobjects/filters/G4State;->a0:I

    sub-int/2addr v2, v3

    invoke-static {v2, p1, p0}, Lorg/icepdf/core/pobjects/filters/CCITTFax;->addRun(ILorg/icepdf/core/pobjects/filters/G4State;Lorg/icepdf/core/io/BitStream;)V

    .line 474
    :cond_3
    :goto_1
    iget-object v1, p1, Lorg/icepdf/core/pobjects/filters/G4State;->ref:[I

    .line 475
    .local v1, "tmp":[I
    iget-object v2, p1, Lorg/icepdf/core/pobjects/filters/G4State;->cur:[I

    iput-object v2, p1, Lorg/icepdf/core/pobjects/filters/G4State;->ref:[I

    .line 476
    iput-object v1, p1, Lorg/icepdf/core/pobjects/filters/G4State;->cur:[I

    .line 478
    iget v0, p1, Lorg/icepdf/core/pobjects/filters/G4State;->curIndex:I

    .local v0, "i":I
    :goto_2
    iget v2, p1, Lorg/icepdf/core/pobjects/filters/G4State;->width:I

    if-ge v0, v2, :cond_5

    .line 479
    iget-object v2, p1, Lorg/icepdf/core/pobjects/filters/G4State;->ref:[I

    aput v5, v2, v0

    .line 478
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 469
    .end local v0    # "i":I
    .end local v1    # "tmp":[I
    :cond_4
    iget v2, p1, Lorg/icepdf/core/pobjects/filters/G4State;->a0:I

    iget v3, p1, Lorg/icepdf/core/pobjects/filters/G4State;->width:I

    if-le v2, v3, :cond_3

    .line 470
    iget v2, p1, Lorg/icepdf/core/pobjects/filters/G4State;->width:I

    invoke-static {v2, p1, p0}, Lorg/icepdf/core/pobjects/filters/CCITTFax;->addRun(ILorg/icepdf/core/pobjects/filters/G4State;Lorg/icepdf/core/io/BitStream;)V

    .line 471
    invoke-static {v5, p1, p0}, Lorg/icepdf/core/pobjects/filters/CCITTFax;->addRun(ILorg/icepdf/core/pobjects/filters/G4State;Lorg/icepdf/core/io/BitStream;)V

    goto :goto_1

    .line 480
    .restart local v0    # "i":I
    .restart local v1    # "tmp":[I
    :cond_5
    const/4 v0, 0x0

    :goto_3
    iget v2, p1, Lorg/icepdf/core/pobjects/filters/G4State;->width:I

    if-ge v0, v2, :cond_6

    .line 481
    iget-object v2, p1, Lorg/icepdf/core/pobjects/filters/G4State;->cur:[I

    aput v5, v2, v0

    .line 480
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 482
    :cond_6
    iput v5, p1, Lorg/icepdf/core/pobjects/filters/G4State;->runLength:I

    .line 483
    iput v5, p1, Lorg/icepdf/core/pobjects/filters/G4State;->a0:I

    .line 484
    iget-object v2, p1, Lorg/icepdf/core/pobjects/filters/G4State;->ref:[I

    aget v2, v2, v5

    iput v2, p1, Lorg/icepdf/core/pobjects/filters/G4State;->b1:I

    .line 485
    iput v6, p1, Lorg/icepdf/core/pobjects/filters/G4State;->refIndex:I

    .line 486
    iput v5, p1, Lorg/icepdf/core/pobjects/filters/G4State;->curIndex:I

    .line 488
    invoke-virtual {p0}, Lorg/icepdf/core/io/BitStream;->close()V

    .line 489
    return-void
.end method
