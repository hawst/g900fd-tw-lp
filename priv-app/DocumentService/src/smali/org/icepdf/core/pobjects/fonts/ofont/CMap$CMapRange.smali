.class Lorg/icepdf/core/pobjects/fonts/ofont/CMap$CMapRange;
.super Ljava/lang/Object;
.source "CMap.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/icepdf/core/pobjects/fonts/ofont/CMap;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "CMapRange"
.end annotation


# instance fields
.field endRange:I

.field offsetValue:I

.field offsetVecor:Ljava/util/Vector;

.field startRange:I

.field final synthetic this$0:Lorg/icepdf/core/pobjects/fonts/ofont/CMap;


# direct methods
.method public constructor <init>(Lorg/icepdf/core/pobjects/fonts/ofont/CMap;III)V
    .locals 1
    .param p2, "startRange"    # I
    .param p3, "endRange"    # I
    .param p4, "offsetValue"    # I

    .prologue
    const/4 v0, 0x0

    .line 565
    iput-object p1, p0, Lorg/icepdf/core/pobjects/fonts/ofont/CMap$CMapRange;->this$0:Lorg/icepdf/core/pobjects/fonts/ofont/CMap;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 549
    iput v0, p0, Lorg/icepdf/core/pobjects/fonts/ofont/CMap$CMapRange;->startRange:I

    .line 551
    iput v0, p0, Lorg/icepdf/core/pobjects/fonts/ofont/CMap$CMapRange;->endRange:I

    .line 553
    iput v0, p0, Lorg/icepdf/core/pobjects/fonts/ofont/CMap$CMapRange;->offsetValue:I

    .line 555
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/icepdf/core/pobjects/fonts/ofont/CMap$CMapRange;->offsetVecor:Ljava/util/Vector;

    .line 566
    iput p2, p0, Lorg/icepdf/core/pobjects/fonts/ofont/CMap$CMapRange;->startRange:I

    .line 567
    iput p3, p0, Lorg/icepdf/core/pobjects/fonts/ofont/CMap$CMapRange;->endRange:I

    .line 568
    iput p4, p0, Lorg/icepdf/core/pobjects/fonts/ofont/CMap$CMapRange;->offsetValue:I

    .line 569
    return-void
.end method

.method public constructor <init>(Lorg/icepdf/core/pobjects/fonts/ofont/CMap;IILjava/util/Vector;)V
    .locals 1
    .param p2, "startRange"    # I
    .param p3, "endRange"    # I
    .param p4, "offsetVecor"    # Ljava/util/Vector;

    .prologue
    const/4 v0, 0x0

    .line 580
    iput-object p1, p0, Lorg/icepdf/core/pobjects/fonts/ofont/CMap$CMapRange;->this$0:Lorg/icepdf/core/pobjects/fonts/ofont/CMap;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 549
    iput v0, p0, Lorg/icepdf/core/pobjects/fonts/ofont/CMap$CMapRange;->startRange:I

    .line 551
    iput v0, p0, Lorg/icepdf/core/pobjects/fonts/ofont/CMap$CMapRange;->endRange:I

    .line 553
    iput v0, p0, Lorg/icepdf/core/pobjects/fonts/ofont/CMap$CMapRange;->offsetValue:I

    .line 555
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/icepdf/core/pobjects/fonts/ofont/CMap$CMapRange;->offsetVecor:Ljava/util/Vector;

    .line 581
    iput p2, p0, Lorg/icepdf/core/pobjects/fonts/ofont/CMap$CMapRange;->startRange:I

    .line 582
    iput p3, p0, Lorg/icepdf/core/pobjects/fonts/ofont/CMap$CMapRange;->endRange:I

    .line 583
    iput-object p4, p0, Lorg/icepdf/core/pobjects/fonts/ofont/CMap$CMapRange;->offsetVecor:Ljava/util/Vector;

    .line 584
    return-void
.end method


# virtual methods
.method public getCMapValue(I)[C
    .locals 8
    .param p1, "value"    # I

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 609
    iget-object v4, p0, Lorg/icepdf/core/pobjects/fonts/ofont/CMap$CMapRange;->offsetVecor:Ljava/util/Vector;

    if-nez v4, :cond_0

    .line 610
    new-array v3, v7, [C

    iget v4, p0, Lorg/icepdf/core/pobjects/fonts/ofont/CMap$CMapRange;->offsetValue:I

    iget v5, p0, Lorg/icepdf/core/pobjects/fonts/ofont/CMap$CMapRange;->startRange:I

    sub-int v5, p1, v5

    add-int/2addr v4, v5

    int-to-char v4, v4

    aput-char v4, v3, v6

    .line 626
    :goto_0
    return-object v3

    .line 614
    :cond_0
    iget-object v4, p0, Lorg/icepdf/core/pobjects/fonts/ofont/CMap$CMapRange;->offsetVecor:Ljava/util/Vector;

    iget v5, p0, Lorg/icepdf/core/pobjects/fonts/ofont/CMap$CMapRange;->startRange:I

    sub-int v5, p1, v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/icepdf/core/pobjects/StringObject;

    .line 616
    .local v1, "hexToken":Lorg/icepdf/core/pobjects/StringObject;
    invoke-interface {v1}, Lorg/icepdf/core/pobjects/StringObject;->getLength()I

    move-result v4

    invoke-interface {v1, v6, v4}, Lorg/icepdf/core/pobjects/StringObject;->getUnsignedInt(II)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 618
    .local v2, "hexValue":Ljava/lang/Integer;
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-static {v4, v7}, Lcom/samsung/thumbnail/util/Utils;->getUnicodeForBullet(IZ)Ljava/lang/String;

    move-result-object v0

    .line 620
    .local v0, "bulletUnicode":Ljava/lang/String;
    invoke-static {v0}, Lcom/samsung/thumbnail/util/Utils;->isValidInteger(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 622
    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v3

    goto :goto_0

    .line 624
    :cond_1
    iget-object v4, p0, Lorg/icepdf/core/pobjects/fonts/ofont/CMap$CMapRange;->this$0:Lorg/icepdf/core/pobjects/fonts/ofont/CMap;

    invoke-interface {v1}, Lorg/icepdf/core/pobjects/StringObject;->getLiteralStringBuffer()Ljava/lang/StringBuilder;

    move-result-object v5

    # invokes: Lorg/icepdf/core/pobjects/fonts/ofont/CMap;->convertToString(Ljava/lang/CharSequence;)[C
    invoke-static {v4, v5}, Lorg/icepdf/core/pobjects/fonts/ofont/CMap;->access$000(Lorg/icepdf/core/pobjects/fonts/ofont/CMap;Ljava/lang/CharSequence;)[C

    move-result-object v3

    .line 626
    .local v3, "test":[C
    goto :goto_0
.end method

.method public inRange(I)Z
    .locals 1
    .param p1, "value"    # I

    .prologue
    .line 594
    iget v0, p0, Lorg/icepdf/core/pobjects/fonts/ofont/CMap$CMapRange;->startRange:I

    if-lt p1, v0, :cond_0

    iget v0, p0, Lorg/icepdf/core/pobjects/fonts/ofont/CMap$CMapRange;->endRange:I

    if-gt p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
