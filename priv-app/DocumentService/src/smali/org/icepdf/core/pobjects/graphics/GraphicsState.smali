.class public Lorg/icepdf/core/pobjects/graphics/GraphicsState;
.super Ljava/lang/Object;
.source "GraphicsState.java"


# static fields
.field private static enabledOverpaint:Z

.field private static final logger:Ljava/util/logging/Logger;


# instance fields
.field private CTM:Landroid/graphics/Matrix;

.field private alphaRule:I

.field private clip:Landroid/graphics/Path;

.field private clipChange:Z

.field private dashArray:[F

.field private dashPhase:F

.field private fillAlpha:F

.field private fillColor:Lorg/apache/poi/java/awt/Color;

.field private fillColorSpace:Lorg/icepdf/core/pobjects/graphics/PColorSpace;

.field private isolated:Z

.field private knockOut:Z

.field private lineCap:I

.field private lineJoin:I

.field private lineWidth:F

.field private miterLimit:F

.field private overprintMode:I

.field private overprintOther:Z

.field private overprintStroking:Z

.field private parentGraphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

.field private shapes:Lorg/icepdf/core/pobjects/graphics/Shapes;

.field private softMask:Lorg/icepdf/core/pobjects/graphics/SoftMask;

.field private strokeAlpha:F

.field private strokeColor:Lorg/apache/poi/java/awt/Color;

.field private strokeColorSpace:Lorg/icepdf/core/pobjects/graphics/PColorSpace;

.field private textState:Lorg/icepdf/core/pobjects/graphics/TextState;

.field private transparencyGroup:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 208
    const-class v0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    invoke-virtual {v0}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->logger:Ljava/util/logging/Logger;

    .line 216
    const-string/jumbo v0, "org.icepdf.core.overpaint"

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lorg/icepdf/core/util/Defs;->sysPropertyBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->enabledOverpaint:Z

    .line 219
    return-void
.end method

.method public constructor <init>(Lorg/icepdf/core/pobjects/graphics/GraphicsState;)V
    .locals 4
    .param p1, "parentGraphicsState"    # Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    .prologue
    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 310
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 223
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->CTM:Landroid/graphics/Matrix;

    .line 226
    iput v2, p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->lineCap:I

    .line 229
    iput v3, p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->lineWidth:F

    .line 232
    iput v2, p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->lineJoin:I

    .line 235
    const/high16 v0, 0x41200000    # 10.0f

    iput v0, p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->miterLimit:F

    .line 238
    iput-object v1, p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->dashArray:[F

    .line 241
    const/4 v0, 0x0

    iput v0, p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->dashPhase:F

    .line 244
    sget-object v0, Lorg/apache/poi/java/awt/Color;->black:Lorg/apache/poi/java/awt/Color;

    iput-object v0, p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->fillColor:Lorg/apache/poi/java/awt/Color;

    .line 247
    sget-object v0, Lorg/apache/poi/java/awt/Color;->black:Lorg/apache/poi/java/awt/Color;

    iput-object v0, p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->strokeColor:Lorg/apache/poi/java/awt/Color;

    .line 250
    iput v3, p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->strokeAlpha:F

    .line 253
    iput v3, p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->fillAlpha:F

    .line 262
    iput v2, p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->alphaRule:I

    .line 269
    new-instance v0, Lorg/icepdf/core/pobjects/graphics/DeviceGray;

    invoke-direct {v0, v1, v1}, Lorg/icepdf/core/pobjects/graphics/DeviceGray;-><init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V

    iput-object v0, p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->fillColorSpace:Lorg/icepdf/core/pobjects/graphics/PColorSpace;

    .line 272
    new-instance v0, Lorg/icepdf/core/pobjects/graphics/DeviceGray;

    invoke-direct {v0, v1, v1}, Lorg/icepdf/core/pobjects/graphics/DeviceGray;-><init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V

    iput-object v0, p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->strokeColorSpace:Lorg/icepdf/core/pobjects/graphics/PColorSpace;

    .line 275
    new-instance v0, Lorg/icepdf/core/pobjects/graphics/TextState;

    invoke-direct {v0}, Lorg/icepdf/core/pobjects/graphics/TextState;-><init>()V

    iput-object v0, p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->textState:Lorg/icepdf/core/pobjects/graphics/TextState;

    .line 278
    iput-object v1, p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->parentGraphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    .line 285
    iput-boolean v2, p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->clipChange:Z

    .line 314
    new-instance v0, Landroid/graphics/Matrix;

    iget-object v1, p1, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->CTM:Landroid/graphics/Matrix;

    invoke-direct {v0, v1}, Landroid/graphics/Matrix;-><init>(Landroid/graphics/Matrix;)V

    iput-object v0, p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->CTM:Landroid/graphics/Matrix;

    .line 316
    iget v0, p1, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->lineCap:I

    iput v0, p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->lineCap:I

    .line 317
    iget v0, p1, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->lineWidth:F

    iput v0, p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->lineWidth:F

    .line 318
    iget v0, p1, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->miterLimit:F

    iput v0, p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->miterLimit:F

    .line 319
    iget v0, p1, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->lineJoin:I

    iput v0, p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->lineJoin:I

    .line 321
    new-instance v0, Lorg/apache/poi/java/awt/Color;

    iget-object v1, p1, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->fillColor:Lorg/apache/poi/java/awt/Color;

    invoke-virtual {v1}, Lorg/apache/poi/java/awt/Color;->getRed()I

    move-result v1

    iget-object v2, p1, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->fillColor:Lorg/apache/poi/java/awt/Color;

    invoke-virtual {v2}, Lorg/apache/poi/java/awt/Color;->getGreen()I

    move-result v2

    iget-object v3, p1, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->fillColor:Lorg/apache/poi/java/awt/Color;

    invoke-virtual {v3}, Lorg/apache/poi/java/awt/Color;->getBlue()I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/poi/java/awt/Color;-><init>(III)V

    iput-object v0, p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->fillColor:Lorg/apache/poi/java/awt/Color;

    .line 325
    new-instance v0, Lorg/apache/poi/java/awt/Color;

    iget-object v1, p1, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->strokeColor:Lorg/apache/poi/java/awt/Color;

    invoke-virtual {v1}, Lorg/apache/poi/java/awt/Color;->getRed()I

    move-result v1

    iget-object v2, p1, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->strokeColor:Lorg/apache/poi/java/awt/Color;

    invoke-virtual {v2}, Lorg/apache/poi/java/awt/Color;->getGreen()I

    move-result v2

    iget-object v3, p1, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->strokeColor:Lorg/apache/poi/java/awt/Color;

    invoke-virtual {v3}, Lorg/apache/poi/java/awt/Color;->getBlue()I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/poi/java/awt/Color;-><init>(III)V

    iput-object v0, p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->strokeColor:Lorg/apache/poi/java/awt/Color;

    .line 328
    iget-object v0, p1, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->shapes:Lorg/icepdf/core/pobjects/graphics/Shapes;

    iput-object v0, p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->shapes:Lorg/icepdf/core/pobjects/graphics/Shapes;

    .line 329
    iget-object v0, p1, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->clip:Landroid/graphics/Path;

    if-eqz v0, :cond_0

    .line 330
    new-instance v0, Landroid/graphics/Path;

    iget-object v1, p1, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->clip:Landroid/graphics/Path;

    invoke-direct {v0, v1}, Landroid/graphics/Path;-><init>(Landroid/graphics/Path;)V

    iput-object v0, p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->clip:Landroid/graphics/Path;

    .line 333
    :cond_0
    iget-object v0, p1, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->fillColorSpace:Lorg/icepdf/core/pobjects/graphics/PColorSpace;

    iput-object v0, p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->fillColorSpace:Lorg/icepdf/core/pobjects/graphics/PColorSpace;

    .line 334
    iget-object v0, p1, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->strokeColorSpace:Lorg/icepdf/core/pobjects/graphics/PColorSpace;

    iput-object v0, p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->strokeColorSpace:Lorg/icepdf/core/pobjects/graphics/PColorSpace;

    .line 335
    new-instance v0, Lorg/icepdf/core/pobjects/graphics/TextState;

    iget-object v1, p1, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->textState:Lorg/icepdf/core/pobjects/graphics/TextState;

    invoke-direct {v0, v1}, Lorg/icepdf/core/pobjects/graphics/TextState;-><init>(Lorg/icepdf/core/pobjects/graphics/TextState;)V

    iput-object v0, p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->textState:Lorg/icepdf/core/pobjects/graphics/TextState;

    .line 336
    invoke-virtual {p1}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->getDashPhase()F

    move-result v0

    iput v0, p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->dashPhase:F

    .line 337
    invoke-virtual {p1}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->getDashArray()[F

    move-result-object v0

    iput-object v0, p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->dashArray:[F

    .line 340
    invoke-virtual {p1}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->getOverprintMode()I

    move-result v0

    iput v0, p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->overprintMode:I

    .line 341
    invoke-virtual {p1}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->isOverprintOther()Z

    move-result v0

    iput-boolean v0, p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->overprintOther:Z

    .line 342
    invoke-virtual {p1}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->isOverprintStroking()Z

    move-result v0

    iput-boolean v0, p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->overprintStroking:Z

    .line 345
    invoke-virtual {p1}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->getFillAlpha()F

    move-result v0

    iput v0, p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->fillAlpha:F

    .line 346
    invoke-virtual {p1}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->getStrokeAlpha()F

    move-result v0

    iput v0, p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->strokeAlpha:F

    .line 347
    invoke-virtual {p1}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->getAlphaRule()I

    move-result v0

    iput v0, p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->alphaRule:I

    .line 350
    invoke-virtual {p1}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->getSoftMask()Lorg/icepdf/core/pobjects/graphics/SoftMask;

    move-result-object v0

    iput-object v0, p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->softMask:Lorg/icepdf/core/pobjects/graphics/SoftMask;

    .line 352
    return-void
.end method

.method public constructor <init>(Lorg/icepdf/core/pobjects/graphics/Shapes;)V
    .locals 4
    .param p1, "shapes"    # Lorg/icepdf/core/pobjects/graphics/Shapes;

    .prologue
    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 300
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 223
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->CTM:Landroid/graphics/Matrix;

    .line 226
    iput v2, p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->lineCap:I

    .line 229
    iput v3, p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->lineWidth:F

    .line 232
    iput v2, p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->lineJoin:I

    .line 235
    const/high16 v0, 0x41200000    # 10.0f

    iput v0, p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->miterLimit:F

    .line 238
    iput-object v1, p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->dashArray:[F

    .line 241
    const/4 v0, 0x0

    iput v0, p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->dashPhase:F

    .line 244
    sget-object v0, Lorg/apache/poi/java/awt/Color;->black:Lorg/apache/poi/java/awt/Color;

    iput-object v0, p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->fillColor:Lorg/apache/poi/java/awt/Color;

    .line 247
    sget-object v0, Lorg/apache/poi/java/awt/Color;->black:Lorg/apache/poi/java/awt/Color;

    iput-object v0, p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->strokeColor:Lorg/apache/poi/java/awt/Color;

    .line 250
    iput v3, p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->strokeAlpha:F

    .line 253
    iput v3, p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->fillAlpha:F

    .line 262
    iput v2, p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->alphaRule:I

    .line 269
    new-instance v0, Lorg/icepdf/core/pobjects/graphics/DeviceGray;

    invoke-direct {v0, v1, v1}, Lorg/icepdf/core/pobjects/graphics/DeviceGray;-><init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V

    iput-object v0, p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->fillColorSpace:Lorg/icepdf/core/pobjects/graphics/PColorSpace;

    .line 272
    new-instance v0, Lorg/icepdf/core/pobjects/graphics/DeviceGray;

    invoke-direct {v0, v1, v1}, Lorg/icepdf/core/pobjects/graphics/DeviceGray;-><init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V

    iput-object v0, p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->strokeColorSpace:Lorg/icepdf/core/pobjects/graphics/PColorSpace;

    .line 275
    new-instance v0, Lorg/icepdf/core/pobjects/graphics/TextState;

    invoke-direct {v0}, Lorg/icepdf/core/pobjects/graphics/TextState;-><init>()V

    iput-object v0, p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->textState:Lorg/icepdf/core/pobjects/graphics/TextState;

    .line 278
    iput-object v1, p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->parentGraphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    .line 285
    iput-boolean v2, p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->clipChange:Z

    .line 301
    iput-object p1, p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->shapes:Lorg/icepdf/core/pobjects/graphics/Shapes;

    .line 302
    return-void
.end method

.method private processOverPaint(Ljava/lang/Boolean;Ljava/lang/Boolean;)V
    .locals 2
    .param p1, "OP"    # Ljava/lang/Boolean;
    .param p2, "op"    # Ljava/lang/Boolean;

    .prologue
    .line 487
    sget-boolean v0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->enabledOverpaint:Z

    if-eqz v0, :cond_1

    if-eqz p1, :cond_1

    if-nez p2, :cond_1

    iget v0, p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->overprintMode:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 489
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->overprintStroking:Z

    .line 490
    iget-boolean v0, p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->overprintStroking:Z

    iput-boolean v0, p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->overprintOther:Z

    .line 498
    :cond_0
    :goto_0
    return-void

    .line 493
    :cond_1
    if-eqz p1, :cond_0

    goto :goto_0
.end method


# virtual methods
.method public concatenate(Lorg/icepdf/core/pobjects/graphics/ExtGState;)V
    .locals 5
    .param p1, "extGState"    # Lorg/icepdf/core/pobjects/graphics/ExtGState;

    .prologue
    .line 428
    invoke-virtual {p1}, Lorg/icepdf/core/pobjects/graphics/ExtGState;->getLineWidth()Ljava/lang/Number;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 429
    invoke-virtual {p1}, Lorg/icepdf/core/pobjects/graphics/ExtGState;->getLineWidth()Ljava/lang/Number;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Number;->floatValue()F

    move-result v2

    invoke-virtual {p0, v2}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->setLineWidth(F)V

    .line 432
    :cond_0
    invoke-virtual {p1}, Lorg/icepdf/core/pobjects/graphics/ExtGState;->getLineCapStyle()Ljava/lang/Number;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 433
    invoke-virtual {p1}, Lorg/icepdf/core/pobjects/graphics/ExtGState;->getLineCapStyle()Ljava/lang/Number;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Number;->intValue()I

    move-result v2

    invoke-virtual {p0, v2}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->setLineCap(I)V

    .line 436
    :cond_1
    invoke-virtual {p1}, Lorg/icepdf/core/pobjects/graphics/ExtGState;->getLineJoinStyle()Ljava/lang/Number;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 437
    invoke-virtual {p1}, Lorg/icepdf/core/pobjects/graphics/ExtGState;->getLineJoinStyle()Ljava/lang/Number;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Number;->intValue()I

    move-result v2

    invoke-virtual {p0, v2}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->setLineJoin(I)V

    .line 440
    :cond_2
    invoke-virtual {p1}, Lorg/icepdf/core/pobjects/graphics/ExtGState;->getMiterLimit()Ljava/lang/Number;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 441
    invoke-virtual {p1}, Lorg/icepdf/core/pobjects/graphics/ExtGState;->getMiterLimit()Ljava/lang/Number;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Number;->floatValue()F

    move-result v2

    invoke-virtual {p0, v2}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->setMiterLimit(F)V

    .line 444
    :cond_3
    invoke-virtual {p1}, Lorg/icepdf/core/pobjects/graphics/ExtGState;->getLineDashPattern()Ljava/util/Vector;

    move-result-object v2

    if-eqz v2, :cond_4

    .line 445
    invoke-virtual {p1}, Lorg/icepdf/core/pobjects/graphics/ExtGState;->getLineDashPattern()Ljava/util/Vector;

    move-result-object v0

    .line 447
    .local v0, "dasshPattern":Ljava/util/Vector;
    if-eqz v0, :cond_4

    .line 449
    const/4 v2, 0x0

    :try_start_0
    invoke-virtual {v0, v2}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [F

    check-cast v2, [F

    invoke-virtual {p0, v2}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->setDashArray([F)V

    .line 450
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Number;

    invoke-virtual {v2}, Ljava/lang/Number;->floatValue()F

    move-result v2

    invoke-virtual {p0, v2}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->setDashPhase(F)V
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 458
    .end local v0    # "dasshPattern":Ljava/util/Vector;
    :cond_4
    :goto_0
    invoke-virtual {p1}, Lorg/icepdf/core/pobjects/graphics/ExtGState;->getNonStrokingAlphConstant()Ljava/lang/Number;

    move-result-object v2

    if-eqz v2, :cond_5

    .line 459
    invoke-virtual {p1}, Lorg/icepdf/core/pobjects/graphics/ExtGState;->getNonStrokingAlphConstant()Ljava/lang/Number;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Number;->floatValue()F

    move-result v2

    invoke-virtual {p0, v2}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->setFillAlpha(F)V

    .line 462
    :cond_5
    invoke-virtual {p1}, Lorg/icepdf/core/pobjects/graphics/ExtGState;->getStrokingAlphConstant()Ljava/lang/Number;

    move-result-object v2

    if-eqz v2, :cond_6

    .line 463
    invoke-virtual {p1}, Lorg/icepdf/core/pobjects/graphics/ExtGState;->getStrokingAlphConstant()Ljava/lang/Number;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Number;->floatValue()F

    move-result v2

    invoke-virtual {p0, v2}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->setStrokeAlpha(F)V

    .line 466
    :cond_6
    invoke-virtual {p1}, Lorg/icepdf/core/pobjects/graphics/ExtGState;->getOverprintMode()Ljava/lang/Number;

    move-result-object v2

    if-eqz v2, :cond_7

    .line 467
    invoke-virtual {p1}, Lorg/icepdf/core/pobjects/graphics/ExtGState;->getOverprintMode()Ljava/lang/Number;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Number;->intValue()I

    move-result v2

    invoke-virtual {p0, v2}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->setOverprintMode(I)V

    .line 471
    :cond_7
    invoke-virtual {p1}, Lorg/icepdf/core/pobjects/graphics/ExtGState;->getSMask()Lorg/icepdf/core/pobjects/graphics/SoftMask;

    move-result-object v2

    if-eqz v2, :cond_8

    .line 472
    invoke-virtual {p1}, Lorg/icepdf/core/pobjects/graphics/ExtGState;->getSMask()Lorg/icepdf/core/pobjects/graphics/SoftMask;

    move-result-object v2

    invoke-virtual {p0, v2}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->setSoftMask(Lorg/icepdf/core/pobjects/graphics/SoftMask;)V

    .line 476
    :cond_8
    invoke-virtual {p1}, Lorg/icepdf/core/pobjects/graphics/ExtGState;->getOverprint()Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {p1}, Lorg/icepdf/core/pobjects/graphics/ExtGState;->getOverprintFill()Ljava/lang/Boolean;

    move-result-object v3

    invoke-direct {p0, v2, v3}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->processOverPaint(Ljava/lang/Boolean;Ljava/lang/Boolean;)V

    .line 477
    return-void

    .line 452
    .restart local v0    # "dasshPattern":Ljava/util/Vector;
    :catch_0
    move-exception v1

    .line 453
    .local v1, "e":Ljava/lang/ClassCastException;
    sget-object v2, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->logger:Ljava/util/logging/Logger;

    sget-object v3, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string/jumbo v4, "Dash cast error: "

    invoke-virtual {v2, v3, v4, v1}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public getAlphaRule()I
    .locals 1

    .prologue
    .line 766
    iget v0, p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->alphaRule:I

    return v0
.end method

.method public getCTM()Landroid/graphics/Matrix;
    .locals 1

    .prologue
    .line 622
    iget-object v0, p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->CTM:Landroid/graphics/Matrix;

    return-object v0
.end method

.method public getClip()Landroid/graphics/Path;
    .locals 1

    .prologue
    .line 618
    iget-object v0, p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->clip:Landroid/graphics/Path;

    return-object v0
.end method

.method public getDashArray()[F
    .locals 1

    .prologue
    .line 662
    iget-object v0, p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->dashArray:[F

    return-object v0
.end method

.method public getDashPhase()F
    .locals 1

    .prologue
    .line 670
    iget v0, p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->dashPhase:F

    return v0
.end method

.method public getFillAlpha()F
    .locals 1

    .prologue
    .line 710
    iget v0, p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->fillAlpha:F

    return v0
.end method

.method public getFillColor()Lorg/apache/poi/java/awt/Color;
    .locals 1

    .prologue
    .line 686
    iget-object v0, p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->fillColor:Lorg/apache/poi/java/awt/Color;

    return-object v0
.end method

.method public getFillColorSpace()Lorg/icepdf/core/pobjects/graphics/PColorSpace;
    .locals 1

    .prologue
    .line 718
    iget-object v0, p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->fillColorSpace:Lorg/icepdf/core/pobjects/graphics/PColorSpace;

    return-object v0
.end method

.method public getLineCap()I
    .locals 1

    .prologue
    .line 630
    iget v0, p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->lineCap:I

    return v0
.end method

.method public getLineJoin()I
    .locals 1

    .prologue
    .line 654
    iget v0, p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->lineJoin:I

    return v0
.end method

.method public getLineWidth()F
    .locals 1

    .prologue
    .line 638
    iget v0, p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->lineWidth:F

    return v0
.end method

.method public getMiterLimit()F
    .locals 1

    .prologue
    .line 678
    iget v0, p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->miterLimit:F

    return v0
.end method

.method public getOverprintMode()I
    .locals 1

    .prologue
    .line 742
    iget v0, p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->overprintMode:I

    return v0
.end method

.method public getSoftMask()Lorg/icepdf/core/pobjects/graphics/SoftMask;
    .locals 1

    .prologue
    .line 798
    iget-object v0, p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->softMask:Lorg/icepdf/core/pobjects/graphics/SoftMask;

    return-object v0
.end method

.method public getStrokeAlpha()F
    .locals 1

    .prologue
    .line 702
    iget v0, p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->strokeAlpha:F

    return v0
.end method

.method public getStrokeColor()Lorg/apache/poi/java/awt/Color;
    .locals 1

    .prologue
    .line 694
    iget-object v0, p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->strokeColor:Lorg/apache/poi/java/awt/Color;

    return-object v0
.end method

.method public getStrokeColorSpace()Lorg/icepdf/core/pobjects/graphics/PColorSpace;
    .locals 1

    .prologue
    .line 726
    iget-object v0, p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->strokeColorSpace:Lorg/icepdf/core/pobjects/graphics/PColorSpace;

    return-object v0
.end method

.method public getTextState()Lorg/icepdf/core/pobjects/graphics/TextState;
    .locals 1

    .prologue
    .line 734
    iget-object v0, p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->textState:Lorg/icepdf/core/pobjects/graphics/TextState;

    return-object v0
.end method

.method public isIsolated()Z
    .locals 1

    .prologue
    .line 782
    iget-boolean v0, p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->isolated:Z

    return v0
.end method

.method public isKnockOut()Z
    .locals 1

    .prologue
    .line 790
    iget-boolean v0, p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->knockOut:Z

    return v0
.end method

.method public isOverprintOther()Z
    .locals 1

    .prologue
    .line 750
    iget-boolean v0, p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->overprintOther:Z

    return v0
.end method

.method public isOverprintStroking()Z
    .locals 1

    .prologue
    .line 746
    iget-boolean v0, p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->overprintStroking:Z

    return v0
.end method

.method public isTransparencyGroup()Z
    .locals 1

    .prologue
    .line 774
    iget-boolean v0, p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->transparencyGroup:Z

    return v0
.end method

.method public restore()Lorg/icepdf/core/pobjects/graphics/GraphicsState;
    .locals 7

    .prologue
    .line 514
    iget-object v0, p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->parentGraphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    if-eqz v0, :cond_3

    .line 516
    iget-object v0, p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->parentGraphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    iget-object v1, p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->parentGraphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    iget-object v1, v1, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->CTM:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->set(Landroid/graphics/Matrix;)V

    .line 518
    iget-object v0, p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->parentGraphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    iget-boolean v0, v0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->clipChange:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->clipChange:Z

    if-eqz v0, :cond_2

    .line 519
    :cond_0
    iget-object v0, p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->parentGraphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    iget-object v0, v0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->clip:Landroid/graphics/Path;

    if-eqz v0, :cond_4

    .line 520
    iget-object v0, p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->parentGraphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    iget-object v0, v0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->clip:Landroid/graphics/Path;

    iget-object v1, p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->clip:Landroid/graphics/Path;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 522
    iget-object v0, p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->parentGraphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    iget-object v0, v0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->clip:Landroid/graphics/Path;

    if-eqz v0, :cond_1

    .line 523
    iget-object v0, p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->parentGraphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    iget-object v0, v0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->shapes:Lorg/icepdf/core/pobjects/graphics/Shapes;

    new-instance v1, Landroid/graphics/Path;

    iget-object v2, p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->parentGraphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    iget-object v2, v2, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->clip:Landroid/graphics/Path;

    invoke-direct {v1, v2}, Landroid/graphics/Path;-><init>(Landroid/graphics/Path;)V

    invoke-virtual {v0, v1}, Lorg/icepdf/core/pobjects/graphics/Shapes;->add(Ljava/lang/Object;)V

    .line 524
    :cond_1
    iget-object v0, p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->parentGraphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    iget-object v0, v0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->shapes:Lorg/icepdf/core/pobjects/graphics/Shapes;

    invoke-virtual {v0}, Lorg/icepdf/core/pobjects/graphics/Shapes;->addClipCommand()V

    .line 531
    :cond_2
    :goto_0
    iget-object v0, p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->parentGraphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    iget v0, v0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->lineWidth:F

    iget-object v1, p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->parentGraphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    iget v1, v1, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->lineCap:I

    iget-object v2, p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->parentGraphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    iget v2, v2, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->lineJoin:I

    iget-object v3, p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->parentGraphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    iget v3, v3, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->miterLimit:F

    iget-object v4, p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->parentGraphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    iget-object v4, v4, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->dashArray:[F

    iget-object v5, p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->parentGraphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    iget v5, v5, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->dashPhase:F

    invoke-static/range {v0 .. v5}, Lcom/samsung/thumbnail/util/Utils;->setPaintValues(FIIF[FF)Landroid/graphics/Paint;

    move-result-object v6

    .line 538
    .local v6, "temp":Landroid/graphics/Paint;
    iget-object v0, p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->parentGraphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    iget-object v0, v0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->shapes:Lorg/icepdf/core/pobjects/graphics/Shapes;

    invoke-virtual {v0, v6}, Lorg/icepdf/core/pobjects/graphics/Shapes;->add(Ljava/lang/Object;)V

    .line 544
    iget-object v0, p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->parentGraphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    iget-object v0, v0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->shapes:Lorg/icepdf/core/pobjects/graphics/Shapes;

    iget-object v1, p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->parentGraphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    invoke-virtual {v1}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->getFillColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/icepdf/core/pobjects/graphics/Shapes;->add(Ljava/lang/Object;)V

    .line 551
    .end local v6    # "temp":Landroid/graphics/Paint;
    :cond_3
    iget-object v0, p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->parentGraphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    return-object v0

    .line 527
    :cond_4
    iget-object v0, p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->parentGraphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    iget-object v0, v0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->shapes:Lorg/icepdf/core/pobjects/graphics/Shapes;

    invoke-virtual {v0}, Lorg/icepdf/core/pobjects/graphics/Shapes;->addNoClipCommand()V

    goto :goto_0
.end method

.method public save()Lorg/icepdf/core/pobjects/graphics/GraphicsState;
    .locals 1

    .prologue
    .line 411
    new-instance v0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    invoke-direct {v0, p0}, Lorg/icepdf/core/pobjects/graphics/GraphicsState;-><init>(Lorg/icepdf/core/pobjects/graphics/GraphicsState;)V

    .line 412
    .local v0, "gs":Lorg/icepdf/core/pobjects/graphics/GraphicsState;
    iput-object p0, v0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->parentGraphicState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    .line 413
    return-object v0
.end method

.method public scale(DD)V
    .locals 3
    .param p1, "x"    # D
    .param p3, "y"    # D

    .prologue
    .line 389
    iget-object v0, p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->CTM:Landroid/graphics/Matrix;

    double-to-float v1, p1

    double-to-float v2, p3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Matrix;->preScale(FF)Z

    .line 390
    iget-object v0, p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->shapes:Lorg/icepdf/core/pobjects/graphics/Shapes;

    new-instance v1, Landroid/graphics/Matrix;

    iget-object v2, p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->CTM:Landroid/graphics/Matrix;

    invoke-direct {v1, v2}, Landroid/graphics/Matrix;-><init>(Landroid/graphics/Matrix;)V

    invoke-virtual {v0, v1}, Lorg/icepdf/core/pobjects/graphics/Shapes;->add(Ljava/lang/Object;)V

    .line 391
    return-void
.end method

.method public set(Landroid/graphics/Matrix;)V
    .locals 3
    .param p1, "af"    # Landroid/graphics/Matrix;

    .prologue
    .line 400
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0, p1}, Landroid/graphics/Matrix;-><init>(Landroid/graphics/Matrix;)V

    iput-object v0, p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->CTM:Landroid/graphics/Matrix;

    .line 401
    iget-object v0, p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->shapes:Lorg/icepdf/core/pobjects/graphics/Shapes;

    new-instance v1, Landroid/graphics/Matrix;

    iget-object v2, p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->CTM:Landroid/graphics/Matrix;

    invoke-direct {v1, v2}, Landroid/graphics/Matrix;-><init>(Landroid/graphics/Matrix;)V

    invoke-virtual {v0, v1}, Lorg/icepdf/core/pobjects/graphics/Shapes;->add(Ljava/lang/Object;)V

    .line 402
    return-void
.end method

.method public setAlphaRule(I)V
    .locals 0
    .param p1, "alphaRule"    # I

    .prologue
    .line 770
    iput p1, p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->alphaRule:I

    .line 771
    return-void
.end method

.method public setCTM(Landroid/graphics/Matrix;)V
    .locals 0
    .param p1, "ctm"    # Landroid/graphics/Matrix;

    .prologue
    .line 626
    iput-object p1, p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->CTM:Landroid/graphics/Matrix;

    .line 627
    return-void
.end method

.method public setClip(Landroid/graphics/Path;)V
    .locals 1
    .param p1, "newClip"    # Landroid/graphics/Path;

    .prologue
    .line 613
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->clip:Landroid/graphics/Path;

    .line 614
    iget-object v0, p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->shapes:Lorg/icepdf/core/pobjects/graphics/Shapes;

    invoke-virtual {v0}, Lorg/icepdf/core/pobjects/graphics/Shapes;->addNoClipCommand()V

    .line 615
    return-void
.end method

.method public setDashArray([F)V
    .locals 0
    .param p1, "dashArray"    # [F

    .prologue
    .line 666
    iput-object p1, p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->dashArray:[F

    .line 667
    return-void
.end method

.method public setDashPhase(F)V
    .locals 0
    .param p1, "dashPhase"    # F

    .prologue
    .line 674
    iput p1, p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->dashPhase:F

    .line 675
    return-void
.end method

.method public setFillAlpha(F)V
    .locals 0
    .param p1, "alpha"    # F

    .prologue
    .line 706
    iput p1, p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->fillAlpha:F

    .line 707
    return-void
.end method

.method public setFillColor(Lorg/apache/poi/java/awt/Color;)V
    .locals 0
    .param p1, "fillColor"    # Lorg/apache/poi/java/awt/Color;

    .prologue
    .line 690
    iput-object p1, p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->fillColor:Lorg/apache/poi/java/awt/Color;

    .line 691
    return-void
.end method

.method public setFillColorSpace(Lorg/icepdf/core/pobjects/graphics/PColorSpace;)V
    .locals 0
    .param p1, "fillColorSpace"    # Lorg/icepdf/core/pobjects/graphics/PColorSpace;

    .prologue
    .line 722
    iput-object p1, p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->fillColorSpace:Lorg/icepdf/core/pobjects/graphics/PColorSpace;

    .line 723
    return-void
.end method

.method public setIsolated(Z)V
    .locals 0
    .param p1, "isolated"    # Z

    .prologue
    .line 786
    iput-boolean p1, p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->isolated:Z

    .line 787
    return-void
.end method

.method public setKnockOut(Z)V
    .locals 0
    .param p1, "knockOut"    # Z

    .prologue
    .line 794
    iput-boolean p1, p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->knockOut:Z

    .line 795
    return-void
.end method

.method public setLineCap(I)V
    .locals 0
    .param p1, "lineCap"    # I

    .prologue
    .line 634
    iput p1, p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->lineCap:I

    .line 635
    return-void
.end method

.method public setLineJoin(I)V
    .locals 0
    .param p1, "lineJoin"    # I

    .prologue
    .line 658
    iput p1, p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->lineJoin:I

    .line 659
    return-void
.end method

.method public setLineWidth(F)V
    .locals 1
    .param p1, "lineWidth"    # F

    .prologue
    .line 643
    const/4 v0, 0x1

    cmpg-float v0, p1, v0

    if-lez v0, :cond_0

    const v0, 0x7f7fffff    # Float.MAX_VALUE

    cmpl-float v0, p1, v0

    if-gez v0, :cond_0

    const/4 v0, 0x0

    cmpl-float v0, p1, v0

    if-nez v0, :cond_1

    .line 646
    :cond_0
    const v0, 0x3a83126f    # 0.001f

    iput v0, p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->lineWidth:F

    .line 651
    :goto_0
    return-void

    .line 648
    :cond_1
    iput p1, p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->lineWidth:F

    goto :goto_0
.end method

.method public setMiterLimit(F)V
    .locals 0
    .param p1, "miterLimit"    # F

    .prologue
    .line 682
    iput p1, p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->miterLimit:F

    .line 683
    return-void
.end method

.method public setOverprintMode(I)V
    .locals 0
    .param p1, "overprintMode"    # I

    .prologue
    .line 754
    iput p1, p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->overprintMode:I

    .line 755
    return-void
.end method

.method public setOverprintOther(Z)V
    .locals 0
    .param p1, "overprintOther"    # Z

    .prologue
    .line 762
    iput-boolean p1, p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->overprintOther:Z

    .line 763
    return-void
.end method

.method public setOverprintStroking(Z)V
    .locals 0
    .param p1, "overprintStroking"    # Z

    .prologue
    .line 758
    iput-boolean p1, p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->overprintStroking:Z

    .line 759
    return-void
.end method

.method public setShapes(Lorg/icepdf/core/pobjects/graphics/Shapes;)V
    .locals 0
    .param p1, "shapes"    # Lorg/icepdf/core/pobjects/graphics/Shapes;

    .prologue
    .line 360
    iput-object p1, p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->shapes:Lorg/icepdf/core/pobjects/graphics/Shapes;

    .line 361
    return-void
.end method

.method public setSoftMask(Lorg/icepdf/core/pobjects/graphics/SoftMask;)V
    .locals 0
    .param p1, "softMask"    # Lorg/icepdf/core/pobjects/graphics/SoftMask;

    .prologue
    .line 802
    iput-object p1, p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->softMask:Lorg/icepdf/core/pobjects/graphics/SoftMask;

    .line 803
    return-void
.end method

.method public setStrokeAlpha(F)V
    .locals 0
    .param p1, "alpha"    # F

    .prologue
    .line 698
    iput p1, p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->strokeAlpha:F

    .line 699
    return-void
.end method

.method public setStrokeColor(Lorg/apache/poi/java/awt/Color;)V
    .locals 0
    .param p1, "strokeColor"    # Lorg/apache/poi/java/awt/Color;

    .prologue
    .line 714
    iput-object p1, p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->strokeColor:Lorg/apache/poi/java/awt/Color;

    .line 715
    return-void
.end method

.method public setStrokeColorSpace(Lorg/icepdf/core/pobjects/graphics/PColorSpace;)V
    .locals 0
    .param p1, "strokeColorSpace"    # Lorg/icepdf/core/pobjects/graphics/PColorSpace;

    .prologue
    .line 730
    iput-object p1, p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->strokeColorSpace:Lorg/icepdf/core/pobjects/graphics/PColorSpace;

    .line 731
    return-void
.end method

.method public setTextState(Lorg/icepdf/core/pobjects/graphics/TextState;)V
    .locals 0
    .param p1, "textState"    # Lorg/icepdf/core/pobjects/graphics/TextState;

    .prologue
    .line 738
    iput-object p1, p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->textState:Lorg/icepdf/core/pobjects/graphics/TextState;

    .line 739
    return-void
.end method

.method public setTransparencyGroup(Z)V
    .locals 0
    .param p1, "transparencyGroup"    # Z

    .prologue
    .line 778
    iput-boolean p1, p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->transparencyGroup:Z

    .line 779
    return-void
.end method

.method public translate(DD)V
    .locals 3
    .param p1, "x"    # D
    .param p3, "y"    # D

    .prologue
    .line 374
    iget-object v0, p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->CTM:Landroid/graphics/Matrix;

    double-to-float v1, p1

    double-to-float v2, p3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Matrix;->preTranslate(FF)Z

    .line 375
    iget-object v0, p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->shapes:Lorg/icepdf/core/pobjects/graphics/Shapes;

    new-instance v1, Landroid/graphics/Matrix;

    iget-object v2, p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->CTM:Landroid/graphics/Matrix;

    invoke-direct {v1, v2}, Landroid/graphics/Matrix;-><init>(Landroid/graphics/Matrix;)V

    invoke-virtual {v0, v1}, Lorg/icepdf/core/pobjects/graphics/Shapes;->add(Ljava/lang/Object;)V

    .line 376
    return-void
.end method

.method public updateClipCM(Landroid/graphics/Matrix;)V
    .locals 2
    .param p1, "af"    # Landroid/graphics/Matrix;

    .prologue
    .line 566
    iget-object v1, p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->clip:Landroid/graphics/Path;

    if-eqz v1, :cond_0

    .line 568
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    .line 570
    .local v0, "afInverse":Landroid/graphics/Matrix;
    invoke-virtual {p1, v0}, Landroid/graphics/Matrix;->invert(Landroid/graphics/Matrix;)Z

    .line 573
    iget-object v1, p0, Lorg/icepdf/core/pobjects/graphics/GraphicsState;->clip:Landroid/graphics/Path;

    invoke-virtual {v1, v0}, Landroid/graphics/Path;->transform(Landroid/graphics/Matrix;)V

    .line 575
    .end local v0    # "afInverse":Landroid/graphics/Matrix;
    :cond_0
    return-void
.end method
