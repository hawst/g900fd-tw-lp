.class public Lorg/icepdf/core/pobjects/CrossReference;
.super Ljava/lang/Object;
.source "CrossReference.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/icepdf/core/pobjects/CrossReference$CompressedEntry;,
        Lorg/icepdf/core/pobjects/CrossReference$UsedEntry;,
        Lorg/icepdf/core/pobjects/CrossReference$FreeEntry;,
        Lorg/icepdf/core/pobjects/CrossReference$Entry;
    }
.end annotation


# static fields
.field private static final logger:Ljava/util/logging/Logger;


# instance fields
.field private m_PTrailer:Lorg/icepdf/core/pobjects/PTrailer;

.field private m_bHaveTriedLoadingPeer:Z

.field private m_bHaveTriedLoadingPrevious:Z

.field private m_bIsCrossReferenceTable:Z

.field private m_hObjectNumber2Entry:Ljava/util/Hashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/Number;",
            "Lorg/icepdf/core/pobjects/CrossReference$Entry;",
            ">;"
        }
    .end annotation
.end field

.field private m_xrefPeer:Lorg/icepdf/core/pobjects/CrossReference;

.field private m_xrefPrevious:Lorg/icepdf/core/pobjects/CrossReference;

.field protected offset:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    const-class v0, Lorg/icepdf/core/pobjects/CrossReference;

    invoke-virtual {v0}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Lorg/icepdf/core/pobjects/CrossReference;->logger:Ljava/util/logging/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    new-instance v0, Ljava/util/Hashtable;

    const/16 v1, 0x1000

    invoke-direct {v0, v1}, Ljava/util/Hashtable;-><init>(I)V

    iput-object v0, p0, Lorg/icepdf/core/pobjects/CrossReference;->m_hObjectNumber2Entry:Ljava/util/Hashtable;

    .line 64
    return-void
.end method


# virtual methods
.method protected addCompressedEntry(III)V
    .locals 3
    .param p1, "objectNumber"    # I
    .param p2, "objectNumberOfContainingObjectStream"    # I
    .param p3, "indexWithinObjectStream"    # I

    .prologue
    .line 230
    new-instance v0, Lorg/icepdf/core/pobjects/CrossReference$CompressedEntry;

    invoke-direct {v0, p1, p2, p3}, Lorg/icepdf/core/pobjects/CrossReference$CompressedEntry;-><init>(III)V

    .line 233
    .local v0, "entry":Lorg/icepdf/core/pobjects/CrossReference$CompressedEntry;
    iget-object v1, p0, Lorg/icepdf/core/pobjects/CrossReference;->m_hObjectNumber2Entry:Ljava/util/Hashtable;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 234
    return-void
.end method

.method protected addFreeEntry(III)V
    .locals 0
    .param p1, "objectNumber"    # I
    .param p2, "nextFreeObjectNumber"    # I
    .param p3, "generationNumberIfReused"    # I

    .prologue
    .line 220
    return-void
.end method

.method public addToEndOfChainOfPreviousXRefs(Lorg/icepdf/core/pobjects/CrossReference;)V
    .locals 1
    .param p1, "prev"    # Lorg/icepdf/core/pobjects/CrossReference;

    .prologue
    .line 210
    iget-object v0, p0, Lorg/icepdf/core/pobjects/CrossReference;->m_xrefPrevious:Lorg/icepdf/core/pobjects/CrossReference;

    if-nez v0, :cond_0

    .line 211
    iput-object p1, p0, Lorg/icepdf/core/pobjects/CrossReference;->m_xrefPrevious:Lorg/icepdf/core/pobjects/CrossReference;

    .line 214
    :goto_0
    return-void

    .line 213
    :cond_0
    iget-object v0, p0, Lorg/icepdf/core/pobjects/CrossReference;->m_xrefPrevious:Lorg/icepdf/core/pobjects/CrossReference;

    invoke-virtual {v0, p1}, Lorg/icepdf/core/pobjects/CrossReference;->addToEndOfChainOfPreviousXRefs(Lorg/icepdf/core/pobjects/CrossReference;)V

    goto :goto_0
.end method

.method protected addUsedEntry(IJI)V
    .locals 8
    .param p1, "objectNumber"    # I
    .param p2, "filePositionOfObject"    # J
    .param p4, "generationNumber"    # I

    .prologue
    .line 223
    new-instance v1, Lorg/icepdf/core/pobjects/CrossReference$UsedEntry;

    move-object v2, p0

    move v3, p1

    move-wide v4, p2

    move v6, p4

    invoke-direct/range {v1 .. v6}, Lorg/icepdf/core/pobjects/CrossReference$UsedEntry;-><init>(Lorg/icepdf/core/pobjects/CrossReference;IJI)V

    .line 226
    .local v1, "entry":Lorg/icepdf/core/pobjects/CrossReference$UsedEntry;
    iget-object v0, p0, Lorg/icepdf/core/pobjects/CrossReference;->m_hObjectNumber2Entry:Ljava/util/Hashtable;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2, v1}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 227
    return-void
.end method

.method public addXRefStreamEntries(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;Ljava/io/InputStream;)V
    .locals 29
    .param p1, "library"    # Lorg/icepdf/core/util/Library;
    .param p2, "xrefStreamHash"    # Ljava/util/Hashtable;
    .param p3, "streamInput"    # Ljava/io/InputStream;

    .prologue
    .line 114
    :try_start_0
    const-string/jumbo v26, "Size"

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, v26

    invoke-virtual {v0, v1, v2}, Lorg/icepdf/core/util/Library;->getInt(Ljava/util/Hashtable;Ljava/lang/String;)I

    move-result v23

    .line 115
    .local v23, "size":I
    const-string/jumbo v26, "Index"

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, v26

    invoke-virtual {v0, v1, v2}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Ljava/util/Vector;

    .line 117
    .local v20, "objNumAndEntriesCountPairs":Ljava/util/Vector;, "Ljava/util/Vector<Ljava/lang/Number;>;"
    if-nez v20, :cond_0

    .line 118
    new-instance v20, Ljava/util/Vector;

    .end local v20    # "objNumAndEntriesCountPairs":Ljava/util/Vector;, "Ljava/util/Vector<Ljava/lang/Number;>;"
    const/16 v26, 0x2

    move-object/from16 v0, v20

    move/from16 v1, v26

    invoke-direct {v0, v1}, Ljava/util/Vector;-><init>(I)V

    .line 119
    .restart local v20    # "objNumAndEntriesCountPairs":Ljava/util/Vector;, "Ljava/util/Vector<Ljava/lang/Number;>;"
    const/16 v26, 0x0

    invoke-static/range {v26 .. v26}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v26

    move-object/from16 v0, v20

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 120
    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v26

    move-object/from16 v0, v20

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 122
    :cond_0
    const-string/jumbo v26, "W"

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, v26

    invoke-virtual {v0, v1, v2}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/util/Vector;

    .line 123
    .local v9, "fieldSizesVec":Ljava/util/Vector;
    const/4 v8, 0x0

    .line 124
    .local v8, "fieldSizes":[I
    if-eqz v9, :cond_1

    .line 125
    invoke-virtual {v9}, Ljava/util/Vector;->size()I

    move-result v26

    move/from16 v0, v26

    new-array v8, v0, [I

    .line 126
    const/16 v17, 0x0

    .local v17, "i":I
    :goto_0
    invoke-virtual {v9}, Ljava/util/Vector;->size()I

    move-result v26

    move/from16 v0, v17

    move/from16 v1, v26

    if-ge v0, v1, :cond_1

    .line 127
    move/from16 v0, v17

    invoke-virtual {v9, v0}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Ljava/lang/Number;

    invoke-virtual/range {v26 .. v26}, Ljava/lang/Number;->intValue()I

    move-result v26

    aput v26, v8, v17

    .line 126
    add-int/lit8 v17, v17, 0x1

    goto :goto_0

    .line 131
    .end local v17    # "i":I
    :cond_1
    if-eqz v8, :cond_6

    array-length v0, v8

    move/from16 v26, v0

    const/16 v27, 0x3

    move/from16 v0, v26

    move/from16 v1, v27

    if-lt v0, v1, :cond_6

    .line 132
    const/16 v26, 0x0

    aget v12, v8, v26

    .line 133
    .local v12, "fieldTypeSize":I
    const/16 v26, 0x1

    aget v11, v8, v26

    .line 134
    .local v11, "fieldTwoSize":I
    const/16 v26, 0x2

    aget v10, v8, v26

    .line 135
    .local v10, "fieldThreeSize":I
    const/16 v25, 0x0

    .local v25, "xrefSubsection":I
    :goto_1
    invoke-virtual/range {v20 .. v20}, Ljava/util/Vector;->size()I

    move-result v26

    move/from16 v0, v25

    move/from16 v1, v26

    if-ge v0, v1, :cond_6

    .line 136
    move-object/from16 v0, v20

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Ljava/lang/Number;

    invoke-virtual/range {v26 .. v26}, Ljava/lang/Number;->intValue()I

    move-result v24

    .line 137
    .local v24, "startingObjectNumber":I
    add-int/lit8 v26, v25, 0x1

    move-object/from16 v0, v20

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Ljava/lang/Number;

    invoke-virtual/range {v26 .. v26}, Ljava/lang/Number;->intValue()I

    move-result v6

    .line 138
    .local v6, "entriesCount":I
    add-int v4, v24, v6

    .line 139
    .local v4, "afterObjectNumber":I
    move/from16 v21, v24

    .local v21, "objectNumber":I
    :goto_2
    move/from16 v0, v21

    if-ge v0, v4, :cond_8

    .line 140
    const/4 v7, 0x1

    .line 141
    .local v7, "entryType":I
    if-lez v12, :cond_2

    .line 142
    move-object/from16 v0, p3

    invoke-static {v0, v12}, Lorg/icepdf/core/util/Utils;->readIntWithVaryingBytesBE(Ljava/io/InputStream;I)I

    move-result v7

    .line 143
    :cond_2
    if-nez v7, :cond_4

    .line 144
    move-object/from16 v0, p3

    invoke-static {v0, v11}, Lorg/icepdf/core/util/Utils;->readIntWithVaryingBytesBE(Ljava/io/InputStream;I)I

    move-result v19

    .line 146
    .local v19, "nextFreeObjectNumber":I
    move-object/from16 v0, p3

    invoke-static {v0, v10}, Lorg/icepdf/core/util/Utils;->readIntWithVaryingBytesBE(Ljava/io/InputStream;I)I

    move-result v16

    .line 148
    .local v16, "generationNumberIfReused":I
    move-object/from16 v0, p0

    move/from16 v1, v21

    move/from16 v2, v19

    move/from16 v3, v16

    invoke-virtual {v0, v1, v2, v3}, Lorg/icepdf/core/pobjects/CrossReference;->addFreeEntry(III)V

    .line 139
    .end local v16    # "generationNumberIfReused":I
    .end local v19    # "nextFreeObjectNumber":I
    :cond_3
    :goto_3
    add-int/lit8 v21, v21, 0x1

    goto :goto_2

    .line 149
    :cond_4
    const/16 v26, 0x1

    move/from16 v0, v26

    if-ne v7, v0, :cond_7

    .line 150
    move-object/from16 v0, p3

    invoke-static {v0, v11}, Lorg/icepdf/core/util/Utils;->readLongWithVaryingBytesBE(Ljava/io/InputStream;I)J

    move-result-wide v14

    .line 152
    .local v14, "filePositionOfObject":J
    const/4 v13, 0x0

    .line 153
    .local v13, "generationNumber":I
    if-lez v10, :cond_5

    .line 154
    move-object/from16 v0, p3

    invoke-static {v0, v10}, Lorg/icepdf/core/util/Utils;->readIntWithVaryingBytesBE(Ljava/io/InputStream;I)I

    move-result v13

    .line 157
    :cond_5
    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-virtual {v0, v1, v14, v15, v13}, Lorg/icepdf/core/pobjects/CrossReference;->addUsedEntry(IJI)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_3

    .line 171
    .end local v4    # "afterObjectNumber":I
    .end local v6    # "entriesCount":I
    .end local v7    # "entryType":I
    .end local v8    # "fieldSizes":[I
    .end local v9    # "fieldSizesVec":Ljava/util/Vector;
    .end local v10    # "fieldThreeSize":I
    .end local v11    # "fieldTwoSize":I
    .end local v12    # "fieldTypeSize":I
    .end local v13    # "generationNumber":I
    .end local v14    # "filePositionOfObject":J
    .end local v20    # "objNumAndEntriesCountPairs":Ljava/util/Vector;, "Ljava/util/Vector<Ljava/lang/Number;>;"
    .end local v21    # "objectNumber":I
    .end local v23    # "size":I
    .end local v24    # "startingObjectNumber":I
    .end local v25    # "xrefSubsection":I
    :catch_0
    move-exception v5

    .line 172
    .local v5, "e":Ljava/io/IOException;
    sget-object v26, Lorg/icepdf/core/pobjects/CrossReference;->logger:Ljava/util/logging/Logger;

    sget-object v27, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    const-string/jumbo v28, "Error parsing xRef stream entries."

    move-object/from16 v0, v26

    move-object/from16 v1, v27

    move-object/from16 v2, v28

    invoke-virtual {v0, v1, v2, v5}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 175
    .end local v5    # "e":Ljava/io/IOException;
    :cond_6
    return-void

    .line 158
    .restart local v4    # "afterObjectNumber":I
    .restart local v6    # "entriesCount":I
    .restart local v7    # "entryType":I
    .restart local v8    # "fieldSizes":[I
    .restart local v9    # "fieldSizesVec":Ljava/util/Vector;
    .restart local v10    # "fieldThreeSize":I
    .restart local v11    # "fieldTwoSize":I
    .restart local v12    # "fieldTypeSize":I
    .restart local v20    # "objNumAndEntriesCountPairs":Ljava/util/Vector;, "Ljava/util/Vector<Ljava/lang/Number;>;"
    .restart local v21    # "objectNumber":I
    .restart local v23    # "size":I
    .restart local v24    # "startingObjectNumber":I
    .restart local v25    # "xrefSubsection":I
    :cond_7
    const/16 v26, 0x2

    move/from16 v0, v26

    if-ne v7, v0, :cond_3

    .line 159
    :try_start_1
    move-object/from16 v0, p3

    invoke-static {v0, v11}, Lorg/icepdf/core/util/Utils;->readIntWithVaryingBytesBE(Ljava/io/InputStream;I)I

    move-result v22

    .line 161
    .local v22, "objectNumberOfContainingObjectStream":I
    move-object/from16 v0, p3

    invoke-static {v0, v10}, Lorg/icepdf/core/util/Utils;->readIntWithVaryingBytesBE(Ljava/io/InputStream;I)I

    move-result v18

    .line 163
    .local v18, "indexWithinObjectStream":I
    move-object/from16 v0, p0

    move/from16 v1, v21

    move/from16 v2, v22

    move/from16 v3, v18

    invoke-virtual {v0, v1, v2, v3}, Lorg/icepdf/core/pobjects/CrossReference;->addCompressedEntry(III)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_3

    .line 135
    .end local v7    # "entryType":I
    .end local v18    # "indexWithinObjectStream":I
    .end local v22    # "objectNumberOfContainingObjectStream":I
    :cond_8
    add-int/lit8 v25, v25, 0x2

    goto/16 :goto_1
.end method

.method public addXRefTableEntries(Lorg/icepdf/core/util/Parser;)V
    .locals 13
    .param p1, "parser"    # Lorg/icepdf/core/util/Parser;

    .prologue
    .line 71
    const/4 v10, 0x1

    iput-boolean v10, p0, Lorg/icepdf/core/pobjects/CrossReference;->m_bIsCrossReferenceTable:Z

    .line 76
    :cond_0
    const/16 v10, 0x10

    :try_start_0
    invoke-virtual {p1, v10}, Lorg/icepdf/core/util/Parser;->getNumberOrStringWithMark(I)Ljava/lang/Object;

    move-result-object v6

    .line 77
    .local v6, "startingObjectNumberOrTrailer":Ljava/lang/Object;
    instance-of v10, v6, Ljava/lang/Number;

    if-nez v10, :cond_1

    .line 78
    invoke-virtual {p1}, Lorg/icepdf/core/util/Parser;->ungetNumberOrStringWithReset()V

    .line 103
    .end local v6    # "startingObjectNumberOrTrailer":Ljava/lang/Object;
    :goto_0
    return-void

    .line 81
    .restart local v6    # "startingObjectNumberOrTrailer":Ljava/lang/Object;
    :cond_1
    check-cast v6, Ljava/lang/Number;

    .end local v6    # "startingObjectNumberOrTrailer":Ljava/lang/Object;
    invoke-virtual {v6}, Ljava/lang/Number;->intValue()I

    move-result v5

    .line 82
    .local v5, "startingObjectNumber":I
    invoke-virtual {p1}, Lorg/icepdf/core/util/Parser;->getToken()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Number;

    invoke-virtual {v10}, Ljava/lang/Number;->intValue()I

    move-result v4

    .line 83
    .local v4, "numEntries":I
    move v0, v5

    .line 84
    .local v0, "currNumber":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    if-ge v3, v4, :cond_0

    .line 85
    invoke-virtual {p1}, Lorg/icepdf/core/util/Parser;->getIntSurroundedByWhitespace()I

    move-result v10

    int-to-long v8, v10

    .line 86
    .local v8, "tenDigitNum":J
    invoke-virtual {p1}, Lorg/icepdf/core/util/Parser;->getIntSurroundedByWhitespace()I

    move-result v2

    .line 87
    .local v2, "generationNum":I
    invoke-virtual {p1}, Lorg/icepdf/core/util/Parser;->getCharSurroundedByWhitespace()C

    move-result v7

    .line 88
    .local v7, "usedOrFree":C
    const/16 v10, 0x6e

    if-ne v7, v10, :cond_3

    .line 89
    invoke-virtual {p0, v0, v8, v9, v2}, Lorg/icepdf/core/pobjects/CrossReference;->addUsedEntry(IJI)V

    .line 94
    :cond_2
    :goto_2
    add-int/lit8 v0, v0, 0x1

    .line 84
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 90
    :cond_3
    const/16 v10, 0x66

    if-ne v7, v10, :cond_2

    .line 91
    long-to-int v10, v8

    invoke-virtual {p0, v0, v10, v2}, Lorg/icepdf/core/pobjects/CrossReference;->addFreeEntry(III)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 100
    .end local v0    # "currNumber":I
    .end local v2    # "generationNum":I
    .end local v3    # "i":I
    .end local v4    # "numEntries":I
    .end local v5    # "startingObjectNumber":I
    .end local v7    # "usedOrFree":C
    .end local v8    # "tenDigitNum":J
    :catch_0
    move-exception v1

    .line 101
    .local v1, "e":Ljava/io/IOException;
    sget-object v10, Lorg/icepdf/core/pobjects/CrossReference;->logger:Ljava/util/logging/Logger;

    sget-object v11, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    const-string/jumbo v12, "Error parsing xRef table entries."

    invoke-virtual {v10, v11, v12, v1}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public getEntryForObject(Ljava/lang/Integer;)Lorg/icepdf/core/pobjects/CrossReference$Entry;
    .locals 4
    .param p1, "objectNumber"    # Ljava/lang/Integer;

    .prologue
    const/4 v3, 0x1

    .line 178
    iget-object v2, p0, Lorg/icepdf/core/pobjects/CrossReference;->m_hObjectNumber2Entry:Ljava/util/Hashtable;

    invoke-virtual {v2, p1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/icepdf/core/pobjects/CrossReference$Entry;

    .line 179
    .local v0, "entry":Lorg/icepdf/core/pobjects/CrossReference$Entry;
    if-eqz v0, :cond_0

    move-object v1, v0

    .line 206
    .end local v0    # "entry":Lorg/icepdf/core/pobjects/CrossReference$Entry;
    .local v1, "entry":Lorg/icepdf/core/pobjects/CrossReference$Entry;
    :goto_0
    return-object v1

    .line 182
    .end local v1    # "entry":Lorg/icepdf/core/pobjects/CrossReference$Entry;
    .restart local v0    # "entry":Lorg/icepdf/core/pobjects/CrossReference$Entry;
    :cond_0
    iget-boolean v2, p0, Lorg/icepdf/core/pobjects/CrossReference;->m_bIsCrossReferenceTable:Z

    if-eqz v2, :cond_1

    iget-boolean v2, p0, Lorg/icepdf/core/pobjects/CrossReference;->m_bHaveTriedLoadingPeer:Z

    if-nez v2, :cond_1

    iget-object v2, p0, Lorg/icepdf/core/pobjects/CrossReference;->m_xrefPeer:Lorg/icepdf/core/pobjects/CrossReference;

    if-nez v2, :cond_1

    iget-object v2, p0, Lorg/icepdf/core/pobjects/CrossReference;->m_PTrailer:Lorg/icepdf/core/pobjects/PTrailer;

    if-eqz v2, :cond_1

    .line 185
    iget-object v2, p0, Lorg/icepdf/core/pobjects/CrossReference;->m_PTrailer:Lorg/icepdf/core/pobjects/PTrailer;

    invoke-virtual {v2}, Lorg/icepdf/core/pobjects/PTrailer;->loadXRefStmIfApplicable()V

    .line 186
    iget-object v2, p0, Lorg/icepdf/core/pobjects/CrossReference;->m_PTrailer:Lorg/icepdf/core/pobjects/PTrailer;

    invoke-virtual {v2}, Lorg/icepdf/core/pobjects/PTrailer;->getCrossReferenceStream()Lorg/icepdf/core/pobjects/CrossReference;

    move-result-object v2

    iput-object v2, p0, Lorg/icepdf/core/pobjects/CrossReference;->m_xrefPeer:Lorg/icepdf/core/pobjects/CrossReference;

    .line 187
    iput-boolean v3, p0, Lorg/icepdf/core/pobjects/CrossReference;->m_bHaveTriedLoadingPeer:Z

    .line 189
    :cond_1
    iget-object v2, p0, Lorg/icepdf/core/pobjects/CrossReference;->m_xrefPeer:Lorg/icepdf/core/pobjects/CrossReference;

    if-eqz v2, :cond_2

    .line 190
    iget-object v2, p0, Lorg/icepdf/core/pobjects/CrossReference;->m_xrefPeer:Lorg/icepdf/core/pobjects/CrossReference;

    invoke-virtual {v2, p1}, Lorg/icepdf/core/pobjects/CrossReference;->getEntryForObject(Ljava/lang/Integer;)Lorg/icepdf/core/pobjects/CrossReference$Entry;

    move-result-object v0

    .line 191
    if-eqz v0, :cond_2

    move-object v1, v0

    .line 192
    .end local v0    # "entry":Lorg/icepdf/core/pobjects/CrossReference$Entry;
    .restart local v1    # "entry":Lorg/icepdf/core/pobjects/CrossReference$Entry;
    goto :goto_0

    .line 195
    .end local v1    # "entry":Lorg/icepdf/core/pobjects/CrossReference$Entry;
    .restart local v0    # "entry":Lorg/icepdf/core/pobjects/CrossReference$Entry;
    :cond_2
    iget-boolean v2, p0, Lorg/icepdf/core/pobjects/CrossReference;->m_bHaveTriedLoadingPrevious:Z

    if-nez v2, :cond_3

    iget-object v2, p0, Lorg/icepdf/core/pobjects/CrossReference;->m_xrefPrevious:Lorg/icepdf/core/pobjects/CrossReference;

    if-nez v2, :cond_3

    iget-object v2, p0, Lorg/icepdf/core/pobjects/CrossReference;->m_PTrailer:Lorg/icepdf/core/pobjects/PTrailer;

    if-eqz v2, :cond_3

    .line 198
    iget-object v2, p0, Lorg/icepdf/core/pobjects/CrossReference;->m_PTrailer:Lorg/icepdf/core/pobjects/PTrailer;

    invoke-virtual {v2}, Lorg/icepdf/core/pobjects/PTrailer;->onDemandLoadAndSetupPreviousTrailer()V

    .line 199
    iput-boolean v3, p0, Lorg/icepdf/core/pobjects/CrossReference;->m_bHaveTriedLoadingPrevious:Z

    .line 201
    :cond_3
    iget-object v2, p0, Lorg/icepdf/core/pobjects/CrossReference;->m_xrefPrevious:Lorg/icepdf/core/pobjects/CrossReference;

    if-eqz v2, :cond_4

    .line 202
    iget-object v2, p0, Lorg/icepdf/core/pobjects/CrossReference;->m_xrefPrevious:Lorg/icepdf/core/pobjects/CrossReference;

    invoke-virtual {v2, p1}, Lorg/icepdf/core/pobjects/CrossReference;->getEntryForObject(Ljava/lang/Integer;)Lorg/icepdf/core/pobjects/CrossReference$Entry;

    move-result-object v0

    .line 203
    if-eqz v0, :cond_4

    move-object v1, v0

    .line 204
    .end local v0    # "entry":Lorg/icepdf/core/pobjects/CrossReference$Entry;
    .restart local v1    # "entry":Lorg/icepdf/core/pobjects/CrossReference$Entry;
    goto :goto_0

    .end local v1    # "entry":Lorg/icepdf/core/pobjects/CrossReference$Entry;
    .restart local v0    # "entry":Lorg/icepdf/core/pobjects/CrossReference$Entry;
    :cond_4
    move-object v1, v0

    .line 206
    .end local v0    # "entry":Lorg/icepdf/core/pobjects/CrossReference$Entry;
    .restart local v1    # "entry":Lorg/icepdf/core/pobjects/CrossReference$Entry;
    goto :goto_0
.end method

.method public setOffset(I)V
    .locals 0
    .param p1, "offset"    # I

    .prologue
    .line 317
    iput p1, p0, Lorg/icepdf/core/pobjects/CrossReference;->offset:I

    .line 318
    return-void
.end method

.method public setTrailer(Lorg/icepdf/core/pobjects/PTrailer;)V
    .locals 0
    .param p1, "trailer"    # Lorg/icepdf/core/pobjects/PTrailer;

    .prologue
    .line 67
    iput-object p1, p0, Lorg/icepdf/core/pobjects/CrossReference;->m_PTrailer:Lorg/icepdf/core/pobjects/PTrailer;

    .line 68
    return-void
.end method
