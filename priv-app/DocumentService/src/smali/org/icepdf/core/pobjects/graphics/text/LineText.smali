.class public Lorg/icepdf/core/pobjects/graphics/text/LineText;
.super Lorg/icepdf/core/pobjects/graphics/text/AbstractText;
.source "LineText.java"

# interfaces
.implements Lorg/icepdf/core/pobjects/graphics/text/TextSelect;


# instance fields
.field private currentWord:Lorg/icepdf/core/pobjects/graphics/text/WordText;

.field private words:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lorg/icepdf/core/pobjects/graphics/text/WordText;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 35
    invoke-direct {p0}, Lorg/icepdf/core/pobjects/graphics/text/AbstractText;-><init>()V

    .line 36
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0x10

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lorg/icepdf/core/pobjects/graphics/text/LineText;->words:Ljava/util/ArrayList;

    .line 37
    return-void
.end method

.method private addWord(Lorg/icepdf/core/pobjects/graphics/text/WordText;)V
    .locals 1
    .param p1, "wordText"    # Lorg/icepdf/core/pobjects/graphics/text/WordText;

    .prologue
    .line 131
    iget-object v0, p0, Lorg/icepdf/core/pobjects/graphics/text/LineText;->words:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 134
    iput-object p1, p0, Lorg/icepdf/core/pobjects/graphics/text/LineText;->currentWord:Lorg/icepdf/core/pobjects/graphics/text/WordText;

    .line 136
    return-void
.end method

.method private getCurrentWord()Lorg/icepdf/core/pobjects/graphics/text/WordText;
    .locals 2

    .prologue
    .line 144
    iget-object v0, p0, Lorg/icepdf/core/pobjects/graphics/text/LineText;->currentWord:Lorg/icepdf/core/pobjects/graphics/text/WordText;

    if-nez v0, :cond_0

    .line 145
    new-instance v0, Lorg/icepdf/core/pobjects/graphics/text/WordText;

    invoke-direct {v0}, Lorg/icepdf/core/pobjects/graphics/text/WordText;-><init>()V

    iput-object v0, p0, Lorg/icepdf/core/pobjects/graphics/text/LineText;->currentWord:Lorg/icepdf/core/pobjects/graphics/text/WordText;

    .line 146
    iget-object v0, p0, Lorg/icepdf/core/pobjects/graphics/text/LineText;->words:Ljava/util/ArrayList;

    iget-object v1, p0, Lorg/icepdf/core/pobjects/graphics/text/LineText;->currentWord:Lorg/icepdf/core/pobjects/graphics/text/WordText;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 148
    :cond_0
    iget-object v0, p0, Lorg/icepdf/core/pobjects/graphics/text/LineText;->currentWord:Lorg/icepdf/core/pobjects/graphics/text/WordText;

    return-object v0
.end method


# virtual methods
.method protected addText(Lorg/icepdf/core/pobjects/graphics/text/GlyphText;)V
    .locals 5
    .param p1, "sprite"    # Lorg/icepdf/core/pobjects/graphics/text/GlyphText;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 84
    invoke-static {p1}, Lorg/icepdf/core/pobjects/graphics/text/WordText;->detectWhiteSpace(Lorg/icepdf/core/pobjects/graphics/text/GlyphText;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 86
    new-instance v0, Lorg/icepdf/core/pobjects/graphics/text/WordText;

    invoke-direct {v0}, Lorg/icepdf/core/pobjects/graphics/text/WordText;-><init>()V

    .line 87
    .local v0, "newWord":Lorg/icepdf/core/pobjects/graphics/text/WordText;
    invoke-virtual {v0, v3}, Lorg/icepdf/core/pobjects/graphics/text/WordText;->setWhiteSpace(Z)V

    .line 88
    invoke-virtual {v0, p1}, Lorg/icepdf/core/pobjects/graphics/text/WordText;->addText(Lorg/icepdf/core/pobjects/graphics/text/GlyphText;)V

    .line 89
    invoke-direct {p0, v0}, Lorg/icepdf/core/pobjects/graphics/text/LineText;->addWord(Lorg/icepdf/core/pobjects/graphics/text/WordText;)V

    .line 91
    iput-object v4, p0, Lorg/icepdf/core/pobjects/graphics/text/LineText;->currentWord:Lorg/icepdf/core/pobjects/graphics/text/WordText;

    .line 120
    .end local v0    # "newWord":Lorg/icepdf/core/pobjects/graphics/text/WordText;
    :goto_0
    return-void

    .line 94
    :cond_0
    invoke-static {p1}, Lorg/icepdf/core/pobjects/graphics/text/WordText;->detectPunctuation(Lorg/icepdf/core/pobjects/graphics/text/GlyphText;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 96
    new-instance v0, Lorg/icepdf/core/pobjects/graphics/text/WordText;

    invoke-direct {v0}, Lorg/icepdf/core/pobjects/graphics/text/WordText;-><init>()V

    .line 97
    .restart local v0    # "newWord":Lorg/icepdf/core/pobjects/graphics/text/WordText;
    invoke-virtual {v0, v3}, Lorg/icepdf/core/pobjects/graphics/text/WordText;->setWhiteSpace(Z)V

    .line 98
    invoke-virtual {v0, p1}, Lorg/icepdf/core/pobjects/graphics/text/WordText;->addText(Lorg/icepdf/core/pobjects/graphics/text/GlyphText;)V

    .line 99
    invoke-direct {p0, v0}, Lorg/icepdf/core/pobjects/graphics/text/LineText;->addWord(Lorg/icepdf/core/pobjects/graphics/text/WordText;)V

    .line 101
    iput-object v4, p0, Lorg/icepdf/core/pobjects/graphics/text/LineText;->currentWord:Lorg/icepdf/core/pobjects/graphics/text/WordText;

    goto :goto_0

    .line 105
    .end local v0    # "newWord":Lorg/icepdf/core/pobjects/graphics/text/WordText;
    :cond_1
    invoke-direct {p0}, Lorg/icepdf/core/pobjects/graphics/text/LineText;->getCurrentWord()Lorg/icepdf/core/pobjects/graphics/text/WordText;

    move-result-object v2

    invoke-virtual {v2, p1}, Lorg/icepdf/core/pobjects/graphics/text/WordText;->detectSpace(Lorg/icepdf/core/pobjects/graphics/text/GlyphText;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 107
    iget-object v2, p0, Lorg/icepdf/core/pobjects/graphics/text/LineText;->currentWord:Lorg/icepdf/core/pobjects/graphics/text/WordText;

    invoke-virtual {v2, p1}, Lorg/icepdf/core/pobjects/graphics/text/WordText;->buildSpaceWord(Lorg/icepdf/core/pobjects/graphics/text/GlyphText;)Lorg/icepdf/core/pobjects/graphics/text/WordText;

    move-result-object v1

    .line 108
    .local v1, "spaceWord":Lorg/icepdf/core/pobjects/graphics/text/WordText;
    invoke-virtual {v1, v3}, Lorg/icepdf/core/pobjects/graphics/text/WordText;->setWhiteSpace(Z)V

    .line 110
    invoke-direct {p0, v1}, Lorg/icepdf/core/pobjects/graphics/text/LineText;->addWord(Lorg/icepdf/core/pobjects/graphics/text/WordText;)V

    .line 112
    iput-object v4, p0, Lorg/icepdf/core/pobjects/graphics/text/LineText;->currentWord:Lorg/icepdf/core/pobjects/graphics/text/WordText;

    .line 114
    invoke-virtual {p0, p1}, Lorg/icepdf/core/pobjects/graphics/text/LineText;->addText(Lorg/icepdf/core/pobjects/graphics/text/GlyphText;)V

    goto :goto_0

    .line 118
    .end local v1    # "spaceWord":Lorg/icepdf/core/pobjects/graphics/text/WordText;
    :cond_2
    invoke-direct {p0}, Lorg/icepdf/core/pobjects/graphics/text/LineText;->getCurrentWord()Lorg/icepdf/core/pobjects/graphics/text/WordText;

    move-result-object v2

    invoke-virtual {v2, p1}, Lorg/icepdf/core/pobjects/graphics/text/WordText;->addText(Lorg/icepdf/core/pobjects/graphics/text/GlyphText;)V

    goto :goto_0
.end method

.method public clearHighlighted()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 186
    invoke-virtual {p0, v3}, Lorg/icepdf/core/pobjects/graphics/text/LineText;->setHighlighted(Z)V

    .line 187
    invoke-virtual {p0, v3}, Lorg/icepdf/core/pobjects/graphics/text/LineText;->setHasHighlight(Z)V

    .line 188
    iget-object v2, p0, Lorg/icepdf/core/pobjects/graphics/text/LineText;->words:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/icepdf/core/pobjects/graphics/text/WordText;

    .line 189
    .local v1, "word":Lorg/icepdf/core/pobjects/graphics/text/WordText;
    invoke-virtual {v1, v3}, Lorg/icepdf/core/pobjects/graphics/text/WordText;->setHighlighted(Z)V

    goto :goto_0

    .line 191
    .end local v1    # "word":Lorg/icepdf/core/pobjects/graphics/text/WordText;
    :cond_0
    return-void
.end method

.method public clearSelected()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 175
    invoke-virtual {p0, v2}, Lorg/icepdf/core/pobjects/graphics/text/LineText;->setSelected(Z)V

    .line 176
    invoke-virtual {p0, v2}, Lorg/icepdf/core/pobjects/graphics/text/LineText;->setHasSelected(Z)V

    .line 177
    iget-object v2, p0, Lorg/icepdf/core/pobjects/graphics/text/LineText;->words:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/icepdf/core/pobjects/graphics/text/WordText;

    .line 178
    .local v1, "word":Lorg/icepdf/core/pobjects/graphics/text/WordText;
    invoke-virtual {v1}, Lorg/icepdf/core/pobjects/graphics/text/WordText;->clearSelected()V

    goto :goto_0

    .line 180
    .end local v1    # "word":Lorg/icepdf/core/pobjects/graphics/text/WordText;
    :cond_0
    return-void
.end method

.method public getBounds()Landroid/graphics/RectF;
    .locals 10

    .prologue
    .line 41
    iget-object v5, p0, Lorg/icepdf/core/pobjects/graphics/text/LineText;->bounds:Landroid/graphics/RectF;

    if-nez v5, :cond_2

    .line 43
    iget-object v5, p0, Lorg/icepdf/core/pobjects/graphics/text/LineText;->words:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/icepdf/core/pobjects/graphics/text/WordText;

    .line 44
    .local v3, "word":Lorg/icepdf/core/pobjects/graphics/text/WordText;
    iget-object v5, p0, Lorg/icepdf/core/pobjects/graphics/text/LineText;->bounds:Landroid/graphics/RectF;

    if-nez v5, :cond_0

    .line 46
    new-instance v5, Landroid/graphics/RectF;

    invoke-virtual {v3}, Lorg/icepdf/core/pobjects/graphics/text/WordText;->getBounds()Landroid/graphics/RectF;

    move-result-object v6

    invoke-direct {v5, v6}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    iput-object v5, p0, Lorg/icepdf/core/pobjects/graphics/text/LineText;->bounds:Landroid/graphics/RectF;

    goto :goto_0

    .line 50
    :cond_0
    invoke-virtual {v3}, Lorg/icepdf/core/pobjects/graphics/text/WordText;->getBounds()Landroid/graphics/RectF;

    move-result-object v4

    .line 52
    .local v4, "wordBounds":Landroid/graphics/RectF;
    new-instance v1, Lorg/apache/poi/java/awt/geom/Rectangle2D$Float;

    iget-object v5, p0, Lorg/icepdf/core/pobjects/graphics/text/LineText;->bounds:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->left:F

    iget-object v6, p0, Lorg/icepdf/core/pobjects/graphics/text/LineText;->bounds:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->top:F

    iget-object v7, p0, Lorg/icepdf/core/pobjects/graphics/text/LineText;->bounds:Landroid/graphics/RectF;

    iget v7, v7, Landroid/graphics/RectF;->right:F

    iget-object v8, p0, Lorg/icepdf/core/pobjects/graphics/text/LineText;->bounds:Landroid/graphics/RectF;

    iget v8, v8, Landroid/graphics/RectF;->left:F

    sub-float/2addr v7, v8

    iget-object v8, p0, Lorg/icepdf/core/pobjects/graphics/text/LineText;->bounds:Landroid/graphics/RectF;

    iget v8, v8, Landroid/graphics/RectF;->bottom:F

    iget-object v9, p0, Lorg/icepdf/core/pobjects/graphics/text/LineText;->bounds:Landroid/graphics/RectF;

    iget v9, v9, Landroid/graphics/RectF;->top:F

    sub-float/2addr v8, v9

    invoke-direct {v1, v5, v6, v7, v8}, Lorg/apache/poi/java/awt/geom/Rectangle2D$Float;-><init>(FFFF)V

    .line 55
    .local v1, "rect1":Lorg/apache/poi/java/awt/geom/Rectangle2D$Float;
    new-instance v2, Lorg/apache/poi/java/awt/geom/Rectangle2D$Float;

    iget v5, v4, Landroid/graphics/RectF;->left:F

    iget v6, v4, Landroid/graphics/RectF;->top:F

    iget v7, v4, Landroid/graphics/RectF;->right:F

    iget v8, v4, Landroid/graphics/RectF;->left:F

    sub-float/2addr v7, v8

    iget v8, v4, Landroid/graphics/RectF;->bottom:F

    iget v9, v4, Landroid/graphics/RectF;->top:F

    sub-float/2addr v8, v9

    invoke-direct {v2, v5, v6, v7, v8}, Lorg/apache/poi/java/awt/geom/Rectangle2D$Float;-><init>(FFFF)V

    .line 58
    .local v2, "rect2":Lorg/apache/poi/java/awt/geom/Rectangle2D$Float;
    invoke-virtual {v1, v2}, Lorg/apache/poi/java/awt/geom/Rectangle2D$Float;->add(Lorg/apache/poi/java/awt/geom/Rectangle2D;)V

    .line 60
    iget-object v5, p0, Lorg/icepdf/core/pobjects/graphics/text/LineText;->bounds:Landroid/graphics/RectF;

    iget v6, v1, Lorg/apache/poi/java/awt/geom/Rectangle2D$Float;->x:F

    iput v6, v5, Landroid/graphics/RectF;->left:F

    .line 61
    iget-object v5, p0, Lorg/icepdf/core/pobjects/graphics/text/LineText;->bounds:Landroid/graphics/RectF;

    iget v6, v1, Lorg/apache/poi/java/awt/geom/Rectangle2D$Float;->y:F

    iput v6, v5, Landroid/graphics/RectF;->top:F

    .line 62
    iget-object v5, p0, Lorg/icepdf/core/pobjects/graphics/text/LineText;->bounds:Landroid/graphics/RectF;

    iget v6, v1, Lorg/apache/poi/java/awt/geom/Rectangle2D$Float;->x:F

    iget v7, v1, Lorg/apache/poi/java/awt/geom/Rectangle2D$Float;->width:F

    add-float/2addr v6, v7

    iput v6, v5, Landroid/graphics/RectF;->right:F

    .line 63
    iget-object v5, p0, Lorg/icepdf/core/pobjects/graphics/text/LineText;->bounds:Landroid/graphics/RectF;

    iget v6, v1, Lorg/apache/poi/java/awt/geom/Rectangle2D$Float;->y:F

    iget v7, v1, Lorg/apache/poi/java/awt/geom/Rectangle2D$Float;->height:F

    add-float/2addr v6, v7

    iput v6, v5, Landroid/graphics/RectF;->bottom:F

    goto :goto_0

    .line 68
    .end local v1    # "rect1":Lorg/apache/poi/java/awt/geom/Rectangle2D$Float;
    .end local v2    # "rect2":Lorg/apache/poi/java/awt/geom/Rectangle2D$Float;
    .end local v3    # "word":Lorg/icepdf/core/pobjects/graphics/text/WordText;
    .end local v4    # "wordBounds":Landroid/graphics/RectF;
    :cond_1
    iget-object v5, p0, Lorg/icepdf/core/pobjects/graphics/text/LineText;->bounds:Landroid/graphics/RectF;

    if-nez v5, :cond_2

    .line 69
    new-instance v5, Landroid/graphics/RectF;

    invoke-direct {v5}, Landroid/graphics/RectF;-><init>()V

    iput-object v5, p0, Lorg/icepdf/core/pobjects/graphics/text/LineText;->bounds:Landroid/graphics/RectF;

    .line 72
    .end local v0    # "i$":Ljava/util/Iterator;
    :cond_2
    iget-object v5, p0, Lorg/icepdf/core/pobjects/graphics/text/LineText;->bounds:Landroid/graphics/RectF;

    return-object v5
.end method

.method public getSelected()Ljava/lang/StringBuilder;
    .locals 4

    .prologue
    .line 201
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 202
    .local v1, "selectedText":Ljava/lang/StringBuilder;
    iget-object v3, p0, Lorg/icepdf/core/pobjects/graphics/text/LineText;->words:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/icepdf/core/pobjects/graphics/text/WordText;

    .line 203
    .local v2, "word":Lorg/icepdf/core/pobjects/graphics/text/WordText;
    invoke-virtual {v2}, Lorg/icepdf/core/pobjects/graphics/text/WordText;->getSelected()Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 205
    .end local v2    # "word":Lorg/icepdf/core/pobjects/graphics/text/WordText;
    :cond_0
    iget-boolean v3, p0, Lorg/icepdf/core/pobjects/graphics/text/LineText;->hasSelected:Z

    if-eqz v3, :cond_1

    .line 206
    const/16 v3, 0xa

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 208
    :cond_1
    return-object v1
.end method

.method public getWords()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lorg/icepdf/core/pobjects/graphics/text/WordText;",
            ">;"
        }
    .end annotation

    .prologue
    .line 157
    iget-object v0, p0, Lorg/icepdf/core/pobjects/graphics/text/LineText;->words:Ljava/util/ArrayList;

    return-object v0
.end method

.method public selectAll()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 164
    invoke-virtual {p0, v2}, Lorg/icepdf/core/pobjects/graphics/text/LineText;->setSelected(Z)V

    .line 165
    invoke-virtual {p0, v2}, Lorg/icepdf/core/pobjects/graphics/text/LineText;->setHasSelected(Z)V

    .line 166
    iget-object v2, p0, Lorg/icepdf/core/pobjects/graphics/text/LineText;->words:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/icepdf/core/pobjects/graphics/text/WordText;

    .line 167
    .local v1, "word":Lorg/icepdf/core/pobjects/graphics/text/WordText;
    invoke-virtual {v1}, Lorg/icepdf/core/pobjects/graphics/text/WordText;->selectAll()V

    goto :goto_0

    .line 169
    .end local v1    # "word":Lorg/icepdf/core/pobjects/graphics/text/WordText;
    :cond_0
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 212
    iget-object v0, p0, Lorg/icepdf/core/pobjects/graphics/text/LineText;->words:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
