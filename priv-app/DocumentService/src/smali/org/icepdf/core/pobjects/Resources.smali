.class public Lorg/icepdf/core/pobjects/Resources;
.super Lorg/icepdf/core/pobjects/Dictionary;
.source "Resources.java"


# static fields
.field private static final logger:Ljava/util/logging/Logger;

.field private static uniqueCounter:I


# instance fields
.field colorspaces:Ljava/util/Hashtable;

.field extGStates:Ljava/util/Hashtable;

.field fonts:Ljava/util/Hashtable;

.field patterns:Ljava/util/Hashtable;

.field private referenceCount:I

.field shading:Ljava/util/Hashtable;

.field xobjects:Ljava/util/Hashtable;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 40
    const/4 v0, 0x0

    sput v0, Lorg/icepdf/core/pobjects/Resources;->uniqueCounter:I

    .line 46
    const-class v0, Lorg/icepdf/core/pobjects/Resources;

    invoke-virtual {v0}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Lorg/icepdf/core/pobjects/Resources;->logger:Ljava/util/logging/Logger;

    return-void
.end method

.method public constructor <init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V
    .locals 3
    .param p1, "l"    # Lorg/icepdf/core/util/Library;
    .param p2, "h"    # Ljava/util/Hashtable;

    .prologue
    .line 64
    invoke-direct {p0, p1, p2}, Lorg/icepdf/core/pobjects/Dictionary;-><init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V

    .line 65
    iget-object v0, p0, Lorg/icepdf/core/pobjects/Resources;->library:Lorg/icepdf/core/util/Library;

    iget-object v1, p0, Lorg/icepdf/core/pobjects/Resources;->entries:Ljava/util/Hashtable;

    const-string/jumbo v2, "ColorSpace"

    invoke-virtual {v0, v1, v2}, Lorg/icepdf/core/util/Library;->getDictionary(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/util/Hashtable;

    move-result-object v0

    iput-object v0, p0, Lorg/icepdf/core/pobjects/Resources;->colorspaces:Ljava/util/Hashtable;

    .line 66
    iget-object v0, p0, Lorg/icepdf/core/pobjects/Resources;->library:Lorg/icepdf/core/util/Library;

    iget-object v1, p0, Lorg/icepdf/core/pobjects/Resources;->entries:Ljava/util/Hashtable;

    const-string/jumbo v2, "Font"

    invoke-virtual {v0, v1, v2}, Lorg/icepdf/core/util/Library;->getDictionary(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/util/Hashtable;

    move-result-object v0

    iput-object v0, p0, Lorg/icepdf/core/pobjects/Resources;->fonts:Ljava/util/Hashtable;

    .line 67
    iget-object v0, p0, Lorg/icepdf/core/pobjects/Resources;->library:Lorg/icepdf/core/util/Library;

    iget-object v1, p0, Lorg/icepdf/core/pobjects/Resources;->entries:Ljava/util/Hashtable;

    const-string/jumbo v2, "XObject"

    invoke-virtual {v0, v1, v2}, Lorg/icepdf/core/util/Library;->getDictionary(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/util/Hashtable;

    move-result-object v0

    iput-object v0, p0, Lorg/icepdf/core/pobjects/Resources;->xobjects:Ljava/util/Hashtable;

    .line 68
    iget-object v0, p0, Lorg/icepdf/core/pobjects/Resources;->library:Lorg/icepdf/core/util/Library;

    iget-object v1, p0, Lorg/icepdf/core/pobjects/Resources;->entries:Ljava/util/Hashtable;

    const-string/jumbo v2, "Pattern"

    invoke-virtual {v0, v1, v2}, Lorg/icepdf/core/util/Library;->getDictionary(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/util/Hashtable;

    move-result-object v0

    iput-object v0, p0, Lorg/icepdf/core/pobjects/Resources;->patterns:Ljava/util/Hashtable;

    .line 69
    iget-object v0, p0, Lorg/icepdf/core/pobjects/Resources;->library:Lorg/icepdf/core/util/Library;

    iget-object v1, p0, Lorg/icepdf/core/pobjects/Resources;->entries:Ljava/util/Hashtable;

    const-string/jumbo v2, "Shading"

    invoke-virtual {v0, v1, v2}, Lorg/icepdf/core/util/Library;->getDictionary(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/util/Hashtable;

    move-result-object v0

    iput-object v0, p0, Lorg/icepdf/core/pobjects/Resources;->shading:Ljava/util/Hashtable;

    .line 70
    iget-object v0, p0, Lorg/icepdf/core/pobjects/Resources;->library:Lorg/icepdf/core/util/Library;

    iget-object v1, p0, Lorg/icepdf/core/pobjects/Resources;->entries:Ljava/util/Hashtable;

    const-string/jumbo v2, "ExtGState"

    invoke-virtual {v0, v1, v2}, Lorg/icepdf/core/util/Library;->getDictionary(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/util/Hashtable;

    move-result-object v0

    iput-object v0, p0, Lorg/icepdf/core/pobjects/Resources;->extGStates:Ljava/util/Hashtable;

    .line 71
    const/4 v0, 0x0

    iput v0, p0, Lorg/icepdf/core/pobjects/Resources;->referenceCount:I

    .line 72
    return-void
.end method

.method private clearResource(Ljava/util/Hashtable;)V
    .locals 5
    .param p1, "resource"    # Ljava/util/Hashtable;

    .prologue
    .line 155
    if-eqz p1, :cond_1

    .line 156
    invoke-virtual {p1}, Ljava/util/Hashtable;->keySet()Ljava/util/Set;

    move-result-object v2

    .line 158
    .local v2, "keys":Ljava/util/Set;
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 159
    .local v1, "key":Ljava/lang/Object;
    invoke-virtual {p1, v1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    .line 160
    .local v3, "value":Ljava/lang/Object;
    instance-of v4, v3, Lorg/icepdf/core/pobjects/Reference;

    if-eqz v4, :cond_0

    .line 161
    iget-object v4, p0, Lorg/icepdf/core/pobjects/Resources;->library:Lorg/icepdf/core/util/Library;

    check-cast v3, Lorg/icepdf/core/pobjects/Reference;

    .end local v3    # "value":Ljava/lang/Object;
    invoke-virtual {v4, v3}, Lorg/icepdf/core/util/Library;->removeObject(Lorg/icepdf/core/pobjects/Reference;)V

    goto :goto_0

    .line 165
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "key":Ljava/lang/Object;
    .end local v2    # "keys":Ljava/util/Set;
    :cond_1
    return-void
.end method

.method private static declared-synchronized getUniqueId()I
    .locals 3

    .prologue
    .line 43
    const-class v1, Lorg/icepdf/core/pobjects/Resources;

    monitor-enter v1

    :try_start_0
    sget v0, Lorg/icepdf/core/pobjects/Resources;->uniqueCounter:I

    add-int/lit8 v2, v0, 0x1

    sput v2, Lorg/icepdf/core/pobjects/Resources;->uniqueCounter:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public addReference(Ljava/lang/Object;)V
    .locals 1
    .param p1, "referer"    # Ljava/lang/Object;

    .prologue
    .line 81
    monitor-enter p0

    .line 82
    :try_start_0
    iget v0, p0, Lorg/icepdf/core/pobjects/Resources;->referenceCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/icepdf/core/pobjects/Resources;->referenceCount:I

    .line 88
    monitor-exit p0

    .line 89
    return-void

    .line 88
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public dispose(ZLorg/icepdf/core/pobjects/Dictionary;)Z
    .locals 6
    .param p1, "cache"    # Z
    .param p2, "referer"    # Lorg/icepdf/core/pobjects/Dictionary;

    .prologue
    .line 108
    monitor-enter p0

    .line 109
    :try_start_0
    iget v5, p0, Lorg/icepdf/core/pobjects/Resources;->referenceCount:I

    add-int/lit8 v5, v5, -0x1

    iput v5, p0, Lorg/icepdf/core/pobjects/Resources;->referenceCount:I

    .line 113
    iget v5, p0, Lorg/icepdf/core/pobjects/Resources;->referenceCount:I

    if-lez v5, :cond_0

    .line 115
    const/4 v5, 0x0

    monitor-exit p0

    .line 151
    :goto_0
    return v5

    .line 117
    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 123
    iget-object v5, p0, Lorg/icepdf/core/pobjects/Resources;->xobjects:Ljava/util/Hashtable;

    if-eqz v5, :cond_4

    .line 124
    iget-object v5, p0, Lorg/icepdf/core/pobjects/Resources;->xobjects:Ljava/util/Hashtable;

    invoke-virtual {v5}, Ljava/util/Hashtable;->elements()Ljava/util/Enumeration;

    move-result-object v4

    .line 125
    .local v4, "xobjectContent":Ljava/util/Enumeration;
    :cond_1
    :goto_1
    invoke-interface {v4}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v5

    if-eqz v5, :cond_4

    .line 126
    invoke-interface {v4}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v3

    .line 127
    .local v3, "tmp":Ljava/lang/Object;
    instance-of v5, v3, Lorg/icepdf/core/pobjects/Stream;

    if-eqz v5, :cond_2

    move-object v2, v3

    .line 128
    check-cast v2, Lorg/icepdf/core/pobjects/Stream;

    .line 129
    .local v2, "stream":Lorg/icepdf/core/pobjects/Stream;
    invoke-virtual {v2, p1}, Lorg/icepdf/core/pobjects/Stream;->dispose(Z)V

    .line 131
    .end local v2    # "stream":Lorg/icepdf/core/pobjects/Stream;
    :cond_2
    instance-of v5, v3, Lorg/icepdf/core/pobjects/Reference;

    if-eqz v5, :cond_1

    .line 132
    iget-object v5, p0, Lorg/icepdf/core/pobjects/Resources;->library:Lorg/icepdf/core/util/Library;

    check-cast v3, Lorg/icepdf/core/pobjects/Reference;

    .end local v3    # "tmp":Ljava/lang/Object;
    invoke-virtual {v5, v3}, Lorg/icepdf/core/util/Library;->getObject(Lorg/icepdf/core/pobjects/Reference;)Ljava/lang/Object;

    move-result-object v1

    .line 133
    .local v1, "reference":Ljava/lang/Object;
    instance-of v5, v1, Lorg/icepdf/core/pobjects/Form;

    if-eqz v5, :cond_3

    move-object v0, v1

    .line 134
    check-cast v0, Lorg/icepdf/core/pobjects/Form;

    .line 135
    .local v0, "form":Lorg/icepdf/core/pobjects/Form;
    invoke-virtual {v0, p1}, Lorg/icepdf/core/pobjects/Form;->dispose(Z)V

    .line 137
    .end local v0    # "form":Lorg/icepdf/core/pobjects/Form;
    :cond_3
    instance-of v5, v1, Lorg/icepdf/core/pobjects/Stream;

    if-eqz v5, :cond_1

    move-object v2, v1

    .line 138
    check-cast v2, Lorg/icepdf/core/pobjects/Stream;

    .line 139
    .restart local v2    # "stream":Lorg/icepdf/core/pobjects/Stream;
    invoke-virtual {v2, p1}, Lorg/icepdf/core/pobjects/Stream;->dispose(Z)V

    goto :goto_1

    .line 117
    .end local v1    # "reference":Ljava/lang/Object;
    .end local v2    # "stream":Lorg/icepdf/core/pobjects/Stream;
    .end local v4    # "xobjectContent":Ljava/util/Enumeration;
    :catchall_0
    move-exception v5

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v5

    .line 145
    :cond_4
    iget-object v5, p0, Lorg/icepdf/core/pobjects/Resources;->colorspaces:Ljava/util/Hashtable;

    invoke-direct {p0, v5}, Lorg/icepdf/core/pobjects/Resources;->clearResource(Ljava/util/Hashtable;)V

    .line 146
    iget-object v5, p0, Lorg/icepdf/core/pobjects/Resources;->fonts:Ljava/util/Hashtable;

    invoke-direct {p0, v5}, Lorg/icepdf/core/pobjects/Resources;->clearResource(Ljava/util/Hashtable;)V

    .line 147
    iget-object v5, p0, Lorg/icepdf/core/pobjects/Resources;->xobjects:Ljava/util/Hashtable;

    invoke-direct {p0, v5}, Lorg/icepdf/core/pobjects/Resources;->clearResource(Ljava/util/Hashtable;)V

    .line 148
    iget-object v5, p0, Lorg/icepdf/core/pobjects/Resources;->patterns:Ljava/util/Hashtable;

    invoke-direct {p0, v5}, Lorg/icepdf/core/pobjects/Resources;->clearResource(Ljava/util/Hashtable;)V

    .line 149
    iget-object v5, p0, Lorg/icepdf/core/pobjects/Resources;->shading:Ljava/util/Hashtable;

    invoke-direct {p0, v5}, Lorg/icepdf/core/pobjects/Resources;->clearResource(Ljava/util/Hashtable;)V

    .line 150
    iget-object v5, p0, Lorg/icepdf/core/pobjects/Resources;->extGStates:Ljava/util/Hashtable;

    invoke-direct {p0, v5}, Lorg/icepdf/core/pobjects/Resources;->clearResource(Ljava/util/Hashtable;)V

    .line 151
    const/4 v5, 0x1

    goto :goto_0
.end method

.method public getColorSpace(Ljava/lang/Object;)Lorg/icepdf/core/pobjects/graphics/PColorSpace;
    .locals 3
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 173
    if-nez p1, :cond_1

    .line 174
    const/4 v0, 0x0

    .line 203
    :cond_0
    :goto_0
    return-object v0

    .line 179
    :cond_1
    iget-object v2, p0, Lorg/icepdf/core/pobjects/Resources;->colorspaces:Ljava/util/Hashtable;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lorg/icepdf/core/pobjects/Resources;->colorspaces:Ljava/util/Hashtable;

    invoke-virtual {v2, p1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 180
    iget-object v2, p0, Lorg/icepdf/core/pobjects/Resources;->colorspaces:Ljava/util/Hashtable;

    invoke-virtual {v2, p1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 181
    .local v1, "tmp":Ljava/lang/Object;
    iget-object v2, p0, Lorg/icepdf/core/pobjects/Resources;->library:Lorg/icepdf/core/util/Library;

    invoke-static {v2, v1}, Lorg/icepdf/core/pobjects/graphics/PColorSpace;->getColorSpace(Lorg/icepdf/core/util/Library;Ljava/lang/Object;)Lorg/icepdf/core/pobjects/graphics/PColorSpace;

    move-result-object v0

    .line 182
    .local v0, "cs":Lorg/icepdf/core/pobjects/graphics/PColorSpace;
    if-eqz v0, :cond_0

    .line 183
    invoke-virtual {v0}, Lorg/icepdf/core/pobjects/graphics/PColorSpace;->init()V

    goto :goto_0

    .line 188
    .end local v0    # "cs":Lorg/icepdf/core/pobjects/graphics/PColorSpace;
    .end local v1    # "tmp":Ljava/lang/Object;
    :cond_2
    iget-object v2, p0, Lorg/icepdf/core/pobjects/Resources;->patterns:Ljava/util/Hashtable;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lorg/icepdf/core/pobjects/Resources;->patterns:Ljava/util/Hashtable;

    invoke-virtual {v2, p1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 189
    iget-object v2, p0, Lorg/icepdf/core/pobjects/Resources;->patterns:Ljava/util/Hashtable;

    invoke-virtual {v2, p1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 190
    .restart local v1    # "tmp":Ljava/lang/Object;
    iget-object v2, p0, Lorg/icepdf/core/pobjects/Resources;->library:Lorg/icepdf/core/util/Library;

    invoke-static {v2, v1}, Lorg/icepdf/core/pobjects/graphics/PColorSpace;->getColorSpace(Lorg/icepdf/core/util/Library;Ljava/lang/Object;)Lorg/icepdf/core/pobjects/graphics/PColorSpace;

    move-result-object v0

    .line 191
    .restart local v0    # "cs":Lorg/icepdf/core/pobjects/graphics/PColorSpace;
    if-eqz v0, :cond_0

    .line 192
    invoke-virtual {v0}, Lorg/icepdf/core/pobjects/graphics/PColorSpace;->init()V

    goto :goto_0

    .line 199
    .end local v0    # "cs":Lorg/icepdf/core/pobjects/graphics/PColorSpace;
    .end local v1    # "tmp":Ljava/lang/Object;
    :cond_3
    iget-object v2, p0, Lorg/icepdf/core/pobjects/Resources;->library:Lorg/icepdf/core/util/Library;

    invoke-static {v2, p1}, Lorg/icepdf/core/pobjects/graphics/PColorSpace;->getColorSpace(Lorg/icepdf/core/util/Library;Ljava/lang/Object;)Lorg/icepdf/core/pobjects/graphics/PColorSpace;

    move-result-object v0

    .line 200
    .restart local v0    # "cs":Lorg/icepdf/core/pobjects/graphics/PColorSpace;
    if-eqz v0, :cond_0

    .line 201
    invoke-virtual {v0}, Lorg/icepdf/core/pobjects/graphics/PColorSpace;->init()V

    goto :goto_0
.end method

.method public getExtGState(Ljava/lang/String;)Lorg/icepdf/core/pobjects/graphics/ExtGState;
    .locals 4
    .param p1, "namedReference"    # Ljava/lang/String;

    .prologue
    .line 355
    const/4 v1, 0x0

    .line 356
    .local v1, "gsState":Lorg/icepdf/core/pobjects/graphics/ExtGState;
    iget-object v2, p0, Lorg/icepdf/core/pobjects/Resources;->extGStates:Ljava/util/Hashtable;

    if-eqz v2, :cond_0

    .line 357
    iget-object v2, p0, Lorg/icepdf/core/pobjects/Resources;->library:Lorg/icepdf/core/util/Library;

    iget-object v3, p0, Lorg/icepdf/core/pobjects/Resources;->extGStates:Ljava/util/Hashtable;

    invoke-virtual {v2, v3, p1}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 358
    .local v0, "attribute":Ljava/lang/Object;
    instance-of v2, v0, Ljava/util/Hashtable;

    if-eqz v2, :cond_1

    .line 359
    new-instance v1, Lorg/icepdf/core/pobjects/graphics/ExtGState;

    .end local v1    # "gsState":Lorg/icepdf/core/pobjects/graphics/ExtGState;
    iget-object v2, p0, Lorg/icepdf/core/pobjects/Resources;->library:Lorg/icepdf/core/util/Library;

    check-cast v0, Ljava/util/Hashtable;

    .end local v0    # "attribute":Ljava/lang/Object;
    invoke-direct {v1, v2, v0}, Lorg/icepdf/core/pobjects/graphics/ExtGState;-><init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V

    .line 366
    .restart local v1    # "gsState":Lorg/icepdf/core/pobjects/graphics/ExtGState;
    :cond_0
    :goto_0
    return-object v1

    .line 360
    .restart local v0    # "attribute":Ljava/lang/Object;
    :cond_1
    instance-of v2, v0, Lorg/icepdf/core/pobjects/Reference;

    if-eqz v2, :cond_0

    .line 361
    new-instance v1, Lorg/icepdf/core/pobjects/graphics/ExtGState;

    .end local v1    # "gsState":Lorg/icepdf/core/pobjects/graphics/ExtGState;
    iget-object v3, p0, Lorg/icepdf/core/pobjects/Resources;->library:Lorg/icepdf/core/util/Library;

    iget-object v2, p0, Lorg/icepdf/core/pobjects/Resources;->library:Lorg/icepdf/core/util/Library;

    check-cast v0, Lorg/icepdf/core/pobjects/Reference;

    .end local v0    # "attribute":Ljava/lang/Object;
    invoke-virtual {v2, v0}, Lorg/icepdf/core/util/Library;->getObject(Lorg/icepdf/core/pobjects/Reference;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Hashtable;

    invoke-direct {v1, v3, v2}, Lorg/icepdf/core/pobjects/graphics/ExtGState;-><init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V

    .restart local v1    # "gsState":Lorg/icepdf/core/pobjects/graphics/ExtGState;
    goto :goto_0
.end method

.method public getFont(Ljava/lang/String;)Lorg/icepdf/core/pobjects/fonts/Font;
    .locals 4
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    .line 212
    const/4 v0, 0x0

    .line 213
    .local v0, "font":Lorg/icepdf/core/pobjects/fonts/Font;
    iget-object v2, p0, Lorg/icepdf/core/pobjects/Resources;->fonts:Ljava/util/Hashtable;

    if-eqz v2, :cond_1

    .line 214
    iget-object v2, p0, Lorg/icepdf/core/pobjects/Resources;->fonts:Ljava/util/Hashtable;

    invoke-virtual {v2, p1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 216
    .local v1, "ob":Ljava/lang/Object;
    if-nez v1, :cond_0

    .line 218
    iget-object v2, p0, Lorg/icepdf/core/pobjects/Resources;->fonts:Ljava/util/Hashtable;

    new-instance v3, Lorg/icepdf/core/pobjects/Name;

    invoke-direct {v3, p1}, Lorg/icepdf/core/pobjects/Name;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 222
    :cond_0
    instance-of v2, v1, Lorg/icepdf/core/pobjects/fonts/Font;

    if-eqz v2, :cond_3

    move-object v0, v1

    .line 223
    check-cast v0, Lorg/icepdf/core/pobjects/fonts/Font;

    .line 235
    .end local v1    # "ob":Ljava/lang/Object;
    :cond_1
    :goto_0
    if-eqz v0, :cond_2

    .line 236
    invoke-virtual {v0}, Lorg/icepdf/core/pobjects/fonts/Font;->init()V

    .line 238
    :cond_2
    return-object v0

    .line 226
    .restart local v1    # "ob":Ljava/lang/Object;
    :cond_3
    instance-of v2, v1, Lorg/icepdf/core/pobjects/Reference;

    if-eqz v2, :cond_1

    .line 227
    iget-object v2, p0, Lorg/icepdf/core/pobjects/Resources;->library:Lorg/icepdf/core/util/Library;

    check-cast v1, Lorg/icepdf/core/pobjects/Reference;

    .end local v1    # "ob":Ljava/lang/Object;
    invoke-virtual {v2, v1}, Lorg/icepdf/core/util/Library;->getObject(Lorg/icepdf/core/pobjects/Reference;)Ljava/lang/Object;

    move-result-object v1

    .line 228
    .restart local v1    # "ob":Ljava/lang/Object;
    instance-of v2, v1, Lorg/icepdf/core/pobjects/fonts/Font;

    if-eqz v2, :cond_4

    move-object v0, v1

    .line 229
    check-cast v0, Lorg/icepdf/core/pobjects/fonts/Font;

    goto :goto_0

    .line 231
    :cond_4
    invoke-static {}, Lorg/icepdf/core/pobjects/fonts/FontFactory;->getInstance()Lorg/icepdf/core/pobjects/fonts/FontFactory;

    move-result-object v2

    iget-object v3, p0, Lorg/icepdf/core/pobjects/Resources;->library:Lorg/icepdf/core/util/Library;

    check-cast v1, Ljava/util/Hashtable;

    .end local v1    # "ob":Ljava/lang/Object;
    invoke-virtual {v2, v3, v1}, Lorg/icepdf/core/pobjects/fonts/FontFactory;->getFont(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)Lorg/icepdf/core/pobjects/fonts/Font;

    move-result-object v0

    goto :goto_0
.end method

.method public getForm(Ljava/lang/String;)Lorg/icepdf/core/pobjects/Form;
    .locals 4
    .param p1, "nameReference"    # Ljava/lang/String;

    .prologue
    .line 286
    const/4 v0, 0x0

    .line 287
    .local v0, "formXObject":Lorg/icepdf/core/pobjects/Form;
    iget-object v2, p0, Lorg/icepdf/core/pobjects/Resources;->library:Lorg/icepdf/core/util/Library;

    iget-object v3, p0, Lorg/icepdf/core/pobjects/Resources;->xobjects:Ljava/util/Hashtable;

    invoke-virtual {v2, v3, p1}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    .line 288
    .local v1, "tempForm":Ljava/lang/Object;
    instance-of v2, v1, Lorg/icepdf/core/pobjects/Form;

    if-eqz v2, :cond_0

    move-object v0, v1

    .line 289
    check-cast v0, Lorg/icepdf/core/pobjects/Form;

    .line 291
    :cond_0
    return-object v0
.end method

.method public getImage(Ljava/lang/String;Lorg/apache/poi/java/awt/Color;)Landroid/graphics/Bitmap;
    .locals 7
    .param p1, "s"    # Ljava/lang/String;
    .param p2, "fill"    # Lorg/apache/poi/java/awt/Color;

    .prologue
    const/4 v1, 0x0

    .line 249
    iget-object v3, p0, Lorg/icepdf/core/pobjects/Resources;->library:Lorg/icepdf/core/util/Library;

    iget-object v4, p0, Lorg/icepdf/core/pobjects/Resources;->xobjects:Ljava/util/Hashtable;

    invoke-virtual {v3, v4, p1}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/icepdf/core/pobjects/Stream;

    .line 250
    .local v2, "st":Lorg/icepdf/core/pobjects/Stream;
    if-nez v2, :cond_1

    .line 267
    :cond_0
    :goto_0
    return-object v1

    .line 254
    :cond_1
    invoke-virtual {v2}, Lorg/icepdf/core/pobjects/Stream;->isImageSubtype()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 258
    const/4 v1, 0x0

    .line 260
    .local v1, "image":Landroid/graphics/Bitmap;
    const/4 v3, 0x1

    const/4 v4, 0x0

    :try_start_0
    invoke-virtual {v2, p2, p0, v3, v4}, Lorg/icepdf/core/pobjects/Stream;->getImage(Lorg/apache/poi/java/awt/Color;Lorg/icepdf/core/pobjects/Resources;ZZ)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 262
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lorg/icepdf/core/pobjects/Stream;->dispose(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 264
    :catch_0
    move-exception v0

    .line 265
    .local v0, "e":Ljava/lang/Exception;
    sget-object v3, Lorg/icepdf/core/pobjects/Resources;->logger:Ljava/util/logging/Logger;

    sget-object v4, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "Error getting image by name: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5, v0}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public getPattern(Ljava/lang/String;)Lorg/icepdf/core/pobjects/graphics/Pattern;
    .locals 3
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 303
    iget-object v1, p0, Lorg/icepdf/core/pobjects/Resources;->patterns:Ljava/util/Hashtable;

    if-eqz v1, :cond_2

    .line 305
    iget-object v1, p0, Lorg/icepdf/core/pobjects/Resources;->library:Lorg/icepdf/core/util/Library;

    iget-object v2, p0, Lorg/icepdf/core/pobjects/Resources;->patterns:Ljava/util/Hashtable;

    invoke-virtual {v1, v2, p1}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 307
    .local v0, "attribute":Ljava/lang/Object;
    if-eqz v0, :cond_0

    instance-of v1, v0, Lorg/icepdf/core/pobjects/graphics/TilingPattern;

    if-eqz v1, :cond_0

    .line 308
    check-cast v0, Lorg/icepdf/core/pobjects/graphics/TilingPattern;

    .line 319
    .end local v0    # "attribute":Ljava/lang/Object;
    :goto_0
    return-object v0

    .line 310
    .restart local v0    # "attribute":Ljava/lang/Object;
    :cond_0
    if-eqz v0, :cond_1

    instance-of v1, v0, Lorg/icepdf/core/pobjects/Stream;

    if-eqz v1, :cond_1

    .line 311
    new-instance v1, Lorg/icepdf/core/pobjects/graphics/TilingPattern;

    check-cast v0, Lorg/icepdf/core/pobjects/Stream;

    .end local v0    # "attribute":Ljava/lang/Object;
    invoke-direct {v1, v0}, Lorg/icepdf/core/pobjects/graphics/TilingPattern;-><init>(Lorg/icepdf/core/pobjects/Stream;)V

    move-object v0, v1

    goto :goto_0

    .line 314
    .restart local v0    # "attribute":Ljava/lang/Object;
    :cond_1
    if-eqz v0, :cond_2

    instance-of v1, v0, Ljava/util/Hashtable;

    if-eqz v1, :cond_2

    .line 315
    iget-object v1, p0, Lorg/icepdf/core/pobjects/Resources;->library:Lorg/icepdf/core/util/Library;

    check-cast v0, Ljava/util/Hashtable;

    .end local v0    # "attribute":Ljava/lang/Object;
    invoke-static {v1, v0}, Lorg/icepdf/core/pobjects/graphics/ShadingPattern;->getShadingPattern(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)Lorg/icepdf/core/pobjects/graphics/ShadingPattern;

    move-result-object v0

    goto :goto_0

    .line 319
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getShading(Ljava/lang/String;)Lorg/icepdf/core/pobjects/graphics/ShadingPattern;
    .locals 3
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 331
    iget-object v1, p0, Lorg/icepdf/core/pobjects/Resources;->shading:Ljava/util/Hashtable;

    if-eqz v1, :cond_0

    .line 332
    iget-object v1, p0, Lorg/icepdf/core/pobjects/Resources;->library:Lorg/icepdf/core/util/Library;

    iget-object v2, p0, Lorg/icepdf/core/pobjects/Resources;->shading:Ljava/util/Hashtable;

    invoke-virtual {v1, v2, p1}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 333
    .local v0, "shadingDictionary":Ljava/lang/Object;
    if-eqz v0, :cond_0

    instance-of v1, v0, Ljava/util/Hashtable;

    if-eqz v1, :cond_0

    .line 334
    iget-object v1, p0, Lorg/icepdf/core/pobjects/Resources;->library:Lorg/icepdf/core/util/Library;

    iget-object v2, p0, Lorg/icepdf/core/pobjects/Resources;->entries:Ljava/util/Hashtable;

    check-cast v0, Ljava/util/Hashtable;

    .end local v0    # "shadingDictionary":Ljava/lang/Object;
    invoke-static {v1, v2, v0}, Lorg/icepdf/core/pobjects/graphics/ShadingPattern;->getShadingPattern(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;Ljava/util/Hashtable;)Lorg/icepdf/core/pobjects/graphics/ShadingPattern;

    move-result-object v1

    .line 344
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isForm(Ljava/lang/String;)Z
    .locals 3
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    .line 275
    iget-object v1, p0, Lorg/icepdf/core/pobjects/Resources;->library:Lorg/icepdf/core/util/Library;

    iget-object v2, p0, Lorg/icepdf/core/pobjects/Resources;->xobjects:Ljava/util/Hashtable;

    invoke-virtual {v1, v2, p1}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 276
    .local v0, "o":Ljava/lang/Object;
    instance-of v1, v0, Lorg/icepdf/core/pobjects/Form;

    return v1
.end method

.method public removeReference(Ljava/lang/Object;)V
    .locals 1
    .param p1, "referer"    # Ljava/lang/Object;

    .prologue
    .line 92
    monitor-enter p0

    .line 93
    :try_start_0
    iget v0, p0, Lorg/icepdf/core/pobjects/Resources;->referenceCount:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lorg/icepdf/core/pobjects/Resources;->referenceCount:I

    .line 96
    monitor-exit p0

    .line 97
    return-void

    .line 96
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
