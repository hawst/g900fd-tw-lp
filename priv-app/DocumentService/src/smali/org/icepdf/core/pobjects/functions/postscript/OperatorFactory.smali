.class public Lorg/icepdf/core/pobjects/functions/postscript/OperatorFactory;
.super Ljava/lang/Object;
.source "OperatorFactory.java"


# static fields
.field private static operatorCache:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lorg/icepdf/core/pobjects/functions/postscript/Operator;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lorg/icepdf/core/pobjects/functions/postscript/OperatorFactory;->operatorCache:Ljava/util/HashMap;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getOperator([CII)Lorg/icepdf/core/pobjects/functions/postscript/Operator;
    .locals 5
    .param p0, "ch"    # [C
    .param p1, "offset"    # I
    .param p2, "length"    # I

    .prologue
    .line 37
    invoke-static {p0, p1, p2}, Lorg/icepdf/core/pobjects/functions/postscript/OperatorNames;->getType([CII)I

    move-result v2

    .line 40
    .local v2, "operatorType":I
    sget-object v3, Lorg/icepdf/core/pobjects/functions/postscript/OperatorFactory;->operatorCache:Ljava/util/HashMap;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/icepdf/core/pobjects/functions/postscript/Operator;

    .line 41
    .local v0, "operator":Lorg/icepdf/core/pobjects/functions/postscript/Operator;
    if-eqz v0, :cond_0

    move-object v1, v0

    .line 740
    .end local v0    # "operator":Lorg/icepdf/core/pobjects/functions/postscript/Operator;
    .local v1, "operator":Lorg/icepdf/core/pobjects/functions/postscript/Operator;
    :goto_0
    return-object v1

    .line 46
    .end local v1    # "operator":Lorg/icepdf/core/pobjects/functions/postscript/Operator;
    .restart local v0    # "operator":Lorg/icepdf/core/pobjects/functions/postscript/Operator;
    :cond_0
    packed-switch v2, :pswitch_data_0

    .line 727
    :pswitch_0
    new-instance v0, Lorg/icepdf/core/pobjects/functions/postscript/OperatorFactory$41;

    .end local v0    # "operator":Lorg/icepdf/core/pobjects/functions/postscript/Operator;
    const/4 v3, 0x0

    invoke-direct {v0, v3}, Lorg/icepdf/core/pobjects/functions/postscript/OperatorFactory$41;-><init>(I)V

    .line 737
    .restart local v0    # "operator":Lorg/icepdf/core/pobjects/functions/postscript/Operator;
    :goto_1
    if-eqz v0, :cond_1

    .line 738
    sget-object v3, Lorg/icepdf/core/pobjects/functions/postscript/OperatorFactory;->operatorCache:Ljava/util/HashMap;

    invoke-virtual {v0}, Lorg/icepdf/core/pobjects/functions/postscript/Operator;->getType()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    move-object v1, v0

    .line 740
    .end local v0    # "operator":Lorg/icepdf/core/pobjects/functions/postscript/Operator;
    .restart local v1    # "operator":Lorg/icepdf/core/pobjects/functions/postscript/Operator;
    goto :goto_0

    .line 54
    .end local v1    # "operator":Lorg/icepdf/core/pobjects/functions/postscript/Operator;
    .restart local v0    # "operator":Lorg/icepdf/core/pobjects/functions/postscript/Operator;
    :pswitch_1
    new-instance v0, Lorg/icepdf/core/pobjects/functions/postscript/OperatorFactory$1;

    .end local v0    # "operator":Lorg/icepdf/core/pobjects/functions/postscript/Operator;
    const/4 v3, 0x1

    invoke-direct {v0, v3}, Lorg/icepdf/core/pobjects/functions/postscript/OperatorFactory$1;-><init>(I)V

    .line 60
    .restart local v0    # "operator":Lorg/icepdf/core/pobjects/functions/postscript/Operator;
    goto :goto_1

    .line 67
    :pswitch_2
    new-instance v0, Lorg/icepdf/core/pobjects/functions/postscript/OperatorFactory$2;

    .end local v0    # "operator":Lorg/icepdf/core/pobjects/functions/postscript/Operator;
    const/4 v3, 0x2

    invoke-direct {v0, v3}, Lorg/icepdf/core/pobjects/functions/postscript/OperatorFactory$2;-><init>(I)V

    .line 74
    .restart local v0    # "operator":Lorg/icepdf/core/pobjects/functions/postscript/Operator;
    goto :goto_1

    .line 86
    :pswitch_3
    new-instance v0, Lorg/icepdf/core/pobjects/functions/postscript/OperatorFactory$3;

    .end local v0    # "operator":Lorg/icepdf/core/pobjects/functions/postscript/Operator;
    const/4 v3, 0x3

    invoke-direct {v0, v3}, Lorg/icepdf/core/pobjects/functions/postscript/OperatorFactory$3;-><init>(I)V

    .line 100
    .restart local v0    # "operator":Lorg/icepdf/core/pobjects/functions/postscript/Operator;
    goto :goto_1

    .line 109
    :pswitch_4
    new-instance v0, Lorg/icepdf/core/pobjects/functions/postscript/OperatorFactory$4;

    .end local v0    # "operator":Lorg/icepdf/core/pobjects/functions/postscript/Operator;
    const/4 v3, 0x4

    invoke-direct {v0, v3}, Lorg/icepdf/core/pobjects/functions/postscript/OperatorFactory$4;-><init>(I)V

    .line 116
    .restart local v0    # "operator":Lorg/icepdf/core/pobjects/functions/postscript/Operator;
    goto :goto_1

    .line 123
    :pswitch_5
    new-instance v0, Lorg/icepdf/core/pobjects/functions/postscript/OperatorFactory$5;

    .end local v0    # "operator":Lorg/icepdf/core/pobjects/functions/postscript/Operator;
    const/4 v3, 0x5

    invoke-direct {v0, v3}, Lorg/icepdf/core/pobjects/functions/postscript/OperatorFactory$5;-><init>(I)V

    .line 130
    .restart local v0    # "operator":Lorg/icepdf/core/pobjects/functions/postscript/Operator;
    goto :goto_1

    .line 138
    :pswitch_6
    new-instance v0, Lorg/icepdf/core/pobjects/functions/postscript/OperatorFactory$6;

    .end local v0    # "operator":Lorg/icepdf/core/pobjects/functions/postscript/Operator;
    const/4 v3, 0x6

    invoke-direct {v0, v3}, Lorg/icepdf/core/pobjects/functions/postscript/OperatorFactory$6;-><init>(I)V

    .line 144
    .restart local v0    # "operator":Lorg/icepdf/core/pobjects/functions/postscript/Operator;
    goto :goto_1

    .line 151
    :pswitch_7
    new-instance v0, Lorg/icepdf/core/pobjects/functions/postscript/OperatorFactory$7;

    .end local v0    # "operator":Lorg/icepdf/core/pobjects/functions/postscript/Operator;
    const/4 v3, 0x7

    invoke-direct {v0, v3}, Lorg/icepdf/core/pobjects/functions/postscript/OperatorFactory$7;-><init>(I)V

    .line 157
    .restart local v0    # "operator":Lorg/icepdf/core/pobjects/functions/postscript/Operator;
    goto :goto_1

    .line 171
    :pswitch_8
    new-instance v0, Lorg/icepdf/core/pobjects/functions/postscript/OperatorFactory$8;

    .end local v0    # "operator":Lorg/icepdf/core/pobjects/functions/postscript/Operator;
    const/16 v3, 0x8

    invoke-direct {v0, v3}, Lorg/icepdf/core/pobjects/functions/postscript/OperatorFactory$8;-><init>(I)V

    .line 180
    .restart local v0    # "operator":Lorg/icepdf/core/pobjects/functions/postscript/Operator;
    goto :goto_1

    .line 190
    :pswitch_9
    new-instance v0, Lorg/icepdf/core/pobjects/functions/postscript/OperatorFactory$9;

    .end local v0    # "operator":Lorg/icepdf/core/pobjects/functions/postscript/Operator;
    const/16 v3, 0x9

    invoke-direct {v0, v3}, Lorg/icepdf/core/pobjects/functions/postscript/OperatorFactory$9;-><init>(I)V

    .line 198
    .restart local v0    # "operator":Lorg/icepdf/core/pobjects/functions/postscript/Operator;
    goto :goto_1

    .line 204
    :pswitch_a
    new-instance v0, Lorg/icepdf/core/pobjects/functions/postscript/OperatorFactory$10;

    .end local v0    # "operator":Lorg/icepdf/core/pobjects/functions/postscript/Operator;
    const/16 v3, 0xa

    invoke-direct {v0, v3}, Lorg/icepdf/core/pobjects/functions/postscript/OperatorFactory$10;-><init>(I)V

    .line 212
    .restart local v0    # "operator":Lorg/icepdf/core/pobjects/functions/postscript/Operator;
    goto :goto_1

    .line 219
    :pswitch_b
    new-instance v0, Lorg/icepdf/core/pobjects/functions/postscript/OperatorFactory$11;

    .end local v0    # "operator":Lorg/icepdf/core/pobjects/functions/postscript/Operator;
    const/16 v3, 0xb

    invoke-direct {v0, v3}, Lorg/icepdf/core/pobjects/functions/postscript/OperatorFactory$11;-><init>(I)V

    .line 228
    .restart local v0    # "operator":Lorg/icepdf/core/pobjects/functions/postscript/Operator;
    goto :goto_1

    .line 234
    :pswitch_c
    new-instance v0, Lorg/icepdf/core/pobjects/functions/postscript/OperatorFactory$12;

    .end local v0    # "operator":Lorg/icepdf/core/pobjects/functions/postscript/Operator;
    const/16 v3, 0xc

    invoke-direct {v0, v3}, Lorg/icepdf/core/pobjects/functions/postscript/OperatorFactory$12;-><init>(I)V

    .line 240
    .restart local v0    # "operator":Lorg/icepdf/core/pobjects/functions/postscript/Operator;
    goto :goto_1

    .line 247
    :pswitch_d
    new-instance v0, Lorg/icepdf/core/pobjects/functions/postscript/OperatorFactory$13;

    .end local v0    # "operator":Lorg/icepdf/core/pobjects/functions/postscript/Operator;
    const/16 v3, 0xd

    invoke-direct {v0, v3}, Lorg/icepdf/core/pobjects/functions/postscript/OperatorFactory$13;-><init>(I)V

    .line 254
    .restart local v0    # "operator":Lorg/icepdf/core/pobjects/functions/postscript/Operator;
    goto :goto_1

    .line 261
    :pswitch_e
    new-instance v0, Lorg/icepdf/core/pobjects/functions/postscript/OperatorFactory$14;

    .end local v0    # "operator":Lorg/icepdf/core/pobjects/functions/postscript/Operator;
    const/16 v3, 0xe

    invoke-direct {v0, v3}, Lorg/icepdf/core/pobjects/functions/postscript/OperatorFactory$14;-><init>(I)V

    .line 269
    .restart local v0    # "operator":Lorg/icepdf/core/pobjects/functions/postscript/Operator;
    goto :goto_1

    .line 276
    :pswitch_f
    new-instance v0, Lorg/icepdf/core/pobjects/functions/postscript/OperatorFactory$15;

    .end local v0    # "operator":Lorg/icepdf/core/pobjects/functions/postscript/Operator;
    const/16 v3, 0xf

    invoke-direct {v0, v3}, Lorg/icepdf/core/pobjects/functions/postscript/OperatorFactory$15;-><init>(I)V

    .line 283
    .restart local v0    # "operator":Lorg/icepdf/core/pobjects/functions/postscript/Operator;
    goto/16 :goto_1

    .line 291
    :pswitch_10
    new-instance v0, Lorg/icepdf/core/pobjects/functions/postscript/OperatorFactory$16;

    .end local v0    # "operator":Lorg/icepdf/core/pobjects/functions/postscript/Operator;
    const/16 v3, 0x11

    invoke-direct {v0, v3}, Lorg/icepdf/core/pobjects/functions/postscript/OperatorFactory$16;-><init>(I)V

    .line 297
    .restart local v0    # "operator":Lorg/icepdf/core/pobjects/functions/postscript/Operator;
    goto/16 :goto_1

    .line 311
    :pswitch_11
    new-instance v0, Lorg/icepdf/core/pobjects/functions/postscript/OperatorFactory$17;

    .end local v0    # "operator":Lorg/icepdf/core/pobjects/functions/postscript/Operator;
    const/16 v3, 0x12

    invoke-direct {v0, v3}, Lorg/icepdf/core/pobjects/functions/postscript/OperatorFactory$17;-><init>(I)V

    .line 318
    .restart local v0    # "operator":Lorg/icepdf/core/pobjects/functions/postscript/Operator;
    goto/16 :goto_1

    .line 324
    :pswitch_12
    new-instance v0, Lorg/icepdf/core/pobjects/functions/postscript/OperatorFactory$18;

    .end local v0    # "operator":Lorg/icepdf/core/pobjects/functions/postscript/Operator;
    const/16 v3, 0x13

    invoke-direct {v0, v3}, Lorg/icepdf/core/pobjects/functions/postscript/OperatorFactory$18;-><init>(I)V

    .line 331
    .restart local v0    # "operator":Lorg/icepdf/core/pobjects/functions/postscript/Operator;
    goto/16 :goto_1

    .line 344
    :pswitch_13
    new-instance v0, Lorg/icepdf/core/pobjects/functions/postscript/OperatorFactory$19;

    .end local v0    # "operator":Lorg/icepdf/core/pobjects/functions/postscript/Operator;
    const/16 v3, 0x14

    invoke-direct {v0, v3}, Lorg/icepdf/core/pobjects/functions/postscript/OperatorFactory$19;-><init>(I)V

    .line 351
    .restart local v0    # "operator":Lorg/icepdf/core/pobjects/functions/postscript/Operator;
    goto/16 :goto_1

    .line 359
    :pswitch_14
    new-instance v0, Lorg/icepdf/core/pobjects/functions/postscript/OperatorFactory$20;

    .end local v0    # "operator":Lorg/icepdf/core/pobjects/functions/postscript/Operator;
    const/16 v3, 0x15

    invoke-direct {v0, v3}, Lorg/icepdf/core/pobjects/functions/postscript/OperatorFactory$20;-><init>(I)V

    .line 383
    .restart local v0    # "operator":Lorg/icepdf/core/pobjects/functions/postscript/Operator;
    goto/16 :goto_1

    .line 392
    :pswitch_15
    new-instance v0, Lorg/icepdf/core/pobjects/functions/postscript/OperatorFactory$21;

    .end local v0    # "operator":Lorg/icepdf/core/pobjects/functions/postscript/Operator;
    const/16 v3, 0x16

    invoke-direct {v0, v3}, Lorg/icepdf/core/pobjects/functions/postscript/OperatorFactory$21;-><init>(I)V

    .line 431
    .restart local v0    # "operator":Lorg/icepdf/core/pobjects/functions/postscript/Operator;
    goto/16 :goto_1

    .line 442
    :pswitch_16
    new-instance v0, Lorg/icepdf/core/pobjects/functions/postscript/OperatorFactory$22;

    .end local v0    # "operator":Lorg/icepdf/core/pobjects/functions/postscript/Operator;
    const/16 v3, 0x18

    invoke-direct {v0, v3}, Lorg/icepdf/core/pobjects/functions/postscript/OperatorFactory$22;-><init>(I)V

    .line 448
    .restart local v0    # "operator":Lorg/icepdf/core/pobjects/functions/postscript/Operator;
    goto/16 :goto_1

    .line 458
    :pswitch_17
    new-instance v0, Lorg/icepdf/core/pobjects/functions/postscript/OperatorFactory$23;

    .end local v0    # "operator":Lorg/icepdf/core/pobjects/functions/postscript/Operator;
    const/16 v3, 0x19

    invoke-direct {v0, v3}, Lorg/icepdf/core/pobjects/functions/postscript/OperatorFactory$23;-><init>(I)V

    .line 465
    .restart local v0    # "operator":Lorg/icepdf/core/pobjects/functions/postscript/Operator;
    goto/16 :goto_1

    .line 471
    :pswitch_18
    new-instance v0, Lorg/icepdf/core/pobjects/functions/postscript/OperatorFactory$24;

    .end local v0    # "operator":Lorg/icepdf/core/pobjects/functions/postscript/Operator;
    const/16 v3, 0x17

    invoke-direct {v0, v3}, Lorg/icepdf/core/pobjects/functions/postscript/OperatorFactory$24;-><init>(I)V

    .line 477
    .restart local v0    # "operator":Lorg/icepdf/core/pobjects/functions/postscript/Operator;
    goto/16 :goto_1

    .line 483
    :pswitch_19
    new-instance v0, Lorg/icepdf/core/pobjects/functions/postscript/OperatorFactory$25;

    .end local v0    # "operator":Lorg/icepdf/core/pobjects/functions/postscript/Operator;
    const/16 v3, 0x1a

    invoke-direct {v0, v3}, Lorg/icepdf/core/pobjects/functions/postscript/OperatorFactory$25;-><init>(I)V

    .line 489
    .restart local v0    # "operator":Lorg/icepdf/core/pobjects/functions/postscript/Operator;
    goto/16 :goto_1

    .line 494
    :pswitch_1a
    new-instance v0, Lorg/icepdf/core/pobjects/functions/postscript/OperatorFactory$26;

    .end local v0    # "operator":Lorg/icepdf/core/pobjects/functions/postscript/Operator;
    const/16 v3, 0x1b

    invoke-direct {v0, v3}, Lorg/icepdf/core/pobjects/functions/postscript/OperatorFactory$26;-><init>(I)V

    .line 501
    .restart local v0    # "operator":Lorg/icepdf/core/pobjects/functions/postscript/Operator;
    goto/16 :goto_1

    .line 507
    :pswitch_1b
    new-instance v0, Lorg/icepdf/core/pobjects/functions/postscript/OperatorFactory$27;

    .end local v0    # "operator":Lorg/icepdf/core/pobjects/functions/postscript/Operator;
    const/16 v3, 0x1c

    invoke-direct {v0, v3}, Lorg/icepdf/core/pobjects/functions/postscript/OperatorFactory$27;-><init>(I)V

    .line 514
    .restart local v0    # "operator":Lorg/icepdf/core/pobjects/functions/postscript/Operator;
    goto/16 :goto_1

    .line 520
    :pswitch_1c
    new-instance v0, Lorg/icepdf/core/pobjects/functions/postscript/OperatorFactory$28;

    .end local v0    # "operator":Lorg/icepdf/core/pobjects/functions/postscript/Operator;
    const/16 v3, 0x1d

    invoke-direct {v0, v3}, Lorg/icepdf/core/pobjects/functions/postscript/OperatorFactory$28;-><init>(I)V

    .line 527
    .restart local v0    # "operator":Lorg/icepdf/core/pobjects/functions/postscript/Operator;
    goto/16 :goto_1

    .line 534
    :pswitch_1d
    new-instance v0, Lorg/icepdf/core/pobjects/functions/postscript/OperatorFactory$29;

    .end local v0    # "operator":Lorg/icepdf/core/pobjects/functions/postscript/Operator;
    const/16 v3, 0x1e

    invoke-direct {v0, v3}, Lorg/icepdf/core/pobjects/functions/postscript/OperatorFactory$29;-><init>(I)V

    .line 541
    .restart local v0    # "operator":Lorg/icepdf/core/pobjects/functions/postscript/Operator;
    goto/16 :goto_1

    .line 547
    :pswitch_1e
    new-instance v0, Lorg/icepdf/core/pobjects/functions/postscript/OperatorFactory$30;

    .end local v0    # "operator":Lorg/icepdf/core/pobjects/functions/postscript/Operator;
    const/16 v3, 0x1f

    invoke-direct {v0, v3}, Lorg/icepdf/core/pobjects/functions/postscript/OperatorFactory$30;-><init>(I)V

    .line 553
    .restart local v0    # "operator":Lorg/icepdf/core/pobjects/functions/postscript/Operator;
    goto/16 :goto_1

    .line 559
    :pswitch_1f
    new-instance v0, Lorg/icepdf/core/pobjects/functions/postscript/OperatorFactory$31;

    .end local v0    # "operator":Lorg/icepdf/core/pobjects/functions/postscript/Operator;
    const/16 v3, 0x20

    invoke-direct {v0, v3}, Lorg/icepdf/core/pobjects/functions/postscript/OperatorFactory$31;-><init>(I)V

    .line 565
    .restart local v0    # "operator":Lorg/icepdf/core/pobjects/functions/postscript/Operator;
    goto/16 :goto_1

    .line 571
    :pswitch_20
    new-instance v0, Lorg/icepdf/core/pobjects/functions/postscript/OperatorFactory$32;

    .end local v0    # "operator":Lorg/icepdf/core/pobjects/functions/postscript/Operator;
    const/16 v3, 0x21

    invoke-direct {v0, v3}, Lorg/icepdf/core/pobjects/functions/postscript/OperatorFactory$32;-><init>(I)V

    .line 578
    .restart local v0    # "operator":Lorg/icepdf/core/pobjects/functions/postscript/Operator;
    goto/16 :goto_1

    .line 584
    :pswitch_21
    new-instance v0, Lorg/icepdf/core/pobjects/functions/postscript/OperatorFactory$33;

    .end local v0    # "operator":Lorg/icepdf/core/pobjects/functions/postscript/Operator;
    const/16 v3, 0x22

    invoke-direct {v0, v3}, Lorg/icepdf/core/pobjects/functions/postscript/OperatorFactory$33;-><init>(I)V

    .line 589
    .restart local v0    # "operator":Lorg/icepdf/core/pobjects/functions/postscript/Operator;
    goto/16 :goto_1

    .line 612
    :pswitch_22
    new-instance v0, Lorg/icepdf/core/pobjects/functions/postscript/OperatorFactory$34;

    .end local v0    # "operator":Lorg/icepdf/core/pobjects/functions/postscript/Operator;
    const/16 v3, 0x23

    invoke-direct {v0, v3}, Lorg/icepdf/core/pobjects/functions/postscript/OperatorFactory$34;-><init>(I)V

    .line 635
    .restart local v0    # "operator":Lorg/icepdf/core/pobjects/functions/postscript/Operator;
    goto/16 :goto_1

    .line 641
    :pswitch_23
    new-instance v0, Lorg/icepdf/core/pobjects/functions/postscript/OperatorFactory$35;

    .end local v0    # "operator":Lorg/icepdf/core/pobjects/functions/postscript/Operator;
    const/16 v3, 0x24

    invoke-direct {v0, v3}, Lorg/icepdf/core/pobjects/functions/postscript/OperatorFactory$35;-><init>(I)V

    .line 647
    .restart local v0    # "operator":Lorg/icepdf/core/pobjects/functions/postscript/Operator;
    goto/16 :goto_1

    .line 653
    :pswitch_24
    new-instance v0, Lorg/icepdf/core/pobjects/functions/postscript/OperatorFactory$36;

    .end local v0    # "operator":Lorg/icepdf/core/pobjects/functions/postscript/Operator;
    const/16 v3, 0x25

    invoke-direct {v0, v3}, Lorg/icepdf/core/pobjects/functions/postscript/OperatorFactory$36;-><init>(I)V

    .line 659
    .restart local v0    # "operator":Lorg/icepdf/core/pobjects/functions/postscript/Operator;
    goto/16 :goto_1

    .line 665
    :pswitch_25
    new-instance v0, Lorg/icepdf/core/pobjects/functions/postscript/OperatorFactory$37;

    .end local v0    # "operator":Lorg/icepdf/core/pobjects/functions/postscript/Operator;
    const/16 v3, 0x26

    invoke-direct {v0, v3}, Lorg/icepdf/core/pobjects/functions/postscript/OperatorFactory$37;-><init>(I)V

    .line 671
    .restart local v0    # "operator":Lorg/icepdf/core/pobjects/functions/postscript/Operator;
    goto/16 :goto_1

    .line 677
    :pswitch_26
    new-instance v0, Lorg/icepdf/core/pobjects/functions/postscript/OperatorFactory$38;

    .end local v0    # "operator":Lorg/icepdf/core/pobjects/functions/postscript/Operator;
    const/16 v3, 0x27

    invoke-direct {v0, v3}, Lorg/icepdf/core/pobjects/functions/postscript/OperatorFactory$38;-><init>(I)V

    .line 684
    .restart local v0    # "operator":Lorg/icepdf/core/pobjects/functions/postscript/Operator;
    goto/16 :goto_1

    .line 690
    :pswitch_27
    new-instance v0, Lorg/icepdf/core/pobjects/functions/postscript/OperatorFactory$39;

    .end local v0    # "operator":Lorg/icepdf/core/pobjects/functions/postscript/Operator;
    const/16 v3, 0x29

    invoke-direct {v0, v3}, Lorg/icepdf/core/pobjects/functions/postscript/OperatorFactory$39;-><init>(I)V

    .line 696
    .restart local v0    # "operator":Lorg/icepdf/core/pobjects/functions/postscript/Operator;
    goto/16 :goto_1

    .line 705
    :pswitch_28
    new-instance v0, Lorg/icepdf/core/pobjects/functions/postscript/OperatorFactory$40;

    .end local v0    # "operator":Lorg/icepdf/core/pobjects/functions/postscript/Operator;
    const/16 v3, 0x2a

    invoke-direct {v0, v3}, Lorg/icepdf/core/pobjects/functions/postscript/OperatorFactory$40;-><init>(I)V

    .line 719
    .restart local v0    # "operator":Lorg/icepdf/core/pobjects/functions/postscript/Operator;
    goto/16 :goto_1

    .line 721
    :pswitch_29
    new-instance v0, Lorg/icepdf/core/pobjects/functions/postscript/Expression;

    .end local v0    # "operator":Lorg/icepdf/core/pobjects/functions/postscript/Operator;
    const/16 v3, 0x2b

    invoke-direct {v0, v3}, Lorg/icepdf/core/pobjects/functions/postscript/Expression;-><init>(I)V

    .line 722
    .restart local v0    # "operator":Lorg/icepdf/core/pobjects/functions/postscript/Operator;
    goto/16 :goto_1

    .line 724
    :pswitch_2a
    new-instance v0, Lorg/icepdf/core/pobjects/functions/postscript/Expression;

    .end local v0    # "operator":Lorg/icepdf/core/pobjects/functions/postscript/Operator;
    const/16 v3, 0x2c

    invoke-direct {v0, v3}, Lorg/icepdf/core/pobjects/functions/postscript/Expression;-><init>(I)V

    .line 725
    .restart local v0    # "operator":Lorg/icepdf/core/pobjects/functions/postscript/Operator;
    goto/16 :goto_1

    .line 46
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_0
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_18
        :pswitch_16
        :pswitch_17
        :pswitch_19
        :pswitch_1a
        :pswitch_1b
        :pswitch_1c
        :pswitch_1d
        :pswitch_1e
        :pswitch_1f
        :pswitch_20
        :pswitch_21
        :pswitch_22
        :pswitch_23
        :pswitch_24
        :pswitch_25
        :pswitch_26
        :pswitch_0
        :pswitch_27
        :pswitch_28
        :pswitch_29
        :pswitch_2a
    .end packed-switch
.end method
