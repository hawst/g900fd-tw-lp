.class public Lorg/icepdf/core/pobjects/actions/GoToAction;
.super Lorg/icepdf/core/pobjects/actions/Action;
.source "GoToAction.java"


# static fields
.field public static final DESTINATION_KEY:Lorg/icepdf/core/pobjects/Name;


# instance fields
.field private destination:Lorg/icepdf/core/pobjects/Destination;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 33
    new-instance v0, Lorg/icepdf/core/pobjects/Name;

    const-string/jumbo v1, "D"

    invoke-direct {v0, v1}, Lorg/icepdf/core/pobjects/Name;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/icepdf/core/pobjects/actions/GoToAction;->DESTINATION_KEY:Lorg/icepdf/core/pobjects/Name;

    return-void
.end method

.method public constructor <init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V
    .locals 3
    .param p1, "l"    # Lorg/icepdf/core/util/Library;
    .param p2, "h"    # Ljava/util/Hashtable;

    .prologue
    .line 45
    invoke-direct {p0, p1, p2}, Lorg/icepdf/core/pobjects/actions/Action;-><init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V

    .line 47
    new-instance v0, Lorg/icepdf/core/pobjects/Destination;

    iget-object v1, p0, Lorg/icepdf/core/pobjects/actions/GoToAction;->library:Lorg/icepdf/core/util/Library;

    sget-object v2, Lorg/icepdf/core/pobjects/actions/GoToAction;->DESTINATION_KEY:Lorg/icepdf/core/pobjects/Name;

    invoke-virtual {p0, v2}, Lorg/icepdf/core/pobjects/actions/GoToAction;->getObject(Lorg/icepdf/core/pobjects/Name;)Ljava/lang/Object;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/icepdf/core/pobjects/Destination;-><init>(Lorg/icepdf/core/util/Library;Ljava/lang/Object;)V

    iput-object v0, p0, Lorg/icepdf/core/pobjects/actions/GoToAction;->destination:Lorg/icepdf/core/pobjects/Destination;

    .line 48
    return-void
.end method


# virtual methods
.method public getDestination()Lorg/icepdf/core/pobjects/Destination;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lorg/icepdf/core/pobjects/actions/GoToAction;->destination:Lorg/icepdf/core/pobjects/Destination;

    return-object v0
.end method

.method public setDestination(Lorg/icepdf/core/pobjects/Destination;)V
    .locals 3
    .param p1, "destination"    # Lorg/icepdf/core/pobjects/Destination;

    .prologue
    .line 56
    iget-object v0, p0, Lorg/icepdf/core/pobjects/actions/GoToAction;->entries:Ljava/util/Hashtable;

    sget-object v1, Lorg/icepdf/core/pobjects/actions/GoToAction;->DESTINATION_KEY:Lorg/icepdf/core/pobjects/Name;

    invoke-virtual {p1}, Lorg/icepdf/core/pobjects/Destination;->getObject()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 57
    iput-object p1, p0, Lorg/icepdf/core/pobjects/actions/GoToAction;->destination:Lorg/icepdf/core/pobjects/Destination;

    .line 58
    return-void
.end method
