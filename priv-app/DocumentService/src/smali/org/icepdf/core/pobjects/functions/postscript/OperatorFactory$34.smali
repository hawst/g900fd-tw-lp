.class final Lorg/icepdf/core/pobjects/functions/postscript/OperatorFactory$34;
.super Lorg/icepdf/core/pobjects/functions/postscript/Operator;
.source "OperatorFactory.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/icepdf/core/pobjects/functions/postscript/OperatorFactory;->getOperator([CII)Lorg/icepdf/core/pobjects/functions/postscript/Operator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>(I)V
    .locals 0
    .param p1, "x0"    # I

    .prologue
    .line 612
    invoke-direct {p0, p1}, Lorg/icepdf/core/pobjects/functions/postscript/Operator;-><init>(I)V

    return-void
.end method


# virtual methods
.method public eval(Ljava/util/Stack;)V
    .locals 6
    .param p1, "stack"    # Ljava/util/Stack;

    .prologue
    const/4 v5, 0x0

    .line 614
    invoke-virtual {p1}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Float;

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v1

    .line 615
    .local v1, "j":F
    invoke-virtual {p1}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Float;

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v3

    .line 618
    .local v3, "n":F
    cmpl-float v4, v1, v5

    if-lez v4, :cond_0

    .line 619
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    int-to-float v4, v0

    cmpg-float v4, v4, v1

    if-gez v4, :cond_1

    .line 620
    invoke-virtual {p1}, Ljava/util/Stack;->lastElement()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {p1}, Ljava/util/Stack;->size()I

    move-result v5

    int-to-float v5, v5

    sub-float/2addr v5, v3

    float-to-int v5, v5

    invoke-virtual {p1, v4, v5}, Ljava/util/Stack;->insertElementAt(Ljava/lang/Object;I)V

    .line 623
    invoke-virtual {p1}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    .line 619
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 628
    .end local v0    # "i":I
    :cond_0
    cmpg-float v4, v1, v5

    if-gez v4, :cond_1

    .line 629
    const/4 v0, 0x0

    .restart local v0    # "i":I
    neg-float v4, v1

    float-to-int v2, v4

    .local v2, "max":I
    :goto_1
    if-ge v0, v2, :cond_1

    .line 630
    invoke-virtual {p1}, Ljava/util/Stack;->size()I

    move-result v4

    int-to-float v4, v4

    sub-float/2addr v4, v3

    float-to-int v4, v4

    invoke-virtual {p1, v4}, Ljava/util/Stack;->remove(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {p1, v4}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 629
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 633
    .end local v0    # "i":I
    .end local v2    # "max":I
    :cond_1
    return-void
.end method
