.class public Lorg/icepdf/core/pobjects/graphics/DeviceGray;
.super Lorg/icepdf/core/pobjects/graphics/PColorSpace;
.source "DeviceGray.java"


# direct methods
.method public constructor <init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V
    .locals 0
    .param p1, "l"    # Lorg/icepdf/core/util/Library;
    .param p2, "h"    # Ljava/util/Hashtable;

    .prologue
    .line 28
    invoke-direct {p0, p1, p2}, Lorg/icepdf/core/pobjects/graphics/PColorSpace;-><init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V

    .line 29
    return-void
.end method


# virtual methods
.method public getColor([F)Lorg/apache/poi/java/awt/Color;
    .locals 7
    .param p1, "f"    # [F

    .prologue
    const/4 v6, 0x0

    .line 37
    aget v1, p1, v6

    float-to-double v2, v1

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    cmpl-double v1, v2, v4

    if-lez v1, :cond_0

    aget v1, p1, v6

    const/high16 v2, 0x437f0000    # 255.0f

    div-float v0, v1, v2

    .line 38
    .local v0, "color":F
    :goto_0
    new-instance v1, Lorg/apache/poi/java/awt/Color;

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-direct {v1, v0, v0, v0, v2}, Lorg/apache/poi/java/awt/Color;-><init>(FFFF)V

    return-object v1

    .line 37
    .end local v0    # "color":F
    :cond_0
    aget v0, p1, v6

    goto :goto_0
.end method

.method public getNumComponents()I
    .locals 1

    .prologue
    .line 33
    const/4 v0, 0x1

    return v0
.end method
