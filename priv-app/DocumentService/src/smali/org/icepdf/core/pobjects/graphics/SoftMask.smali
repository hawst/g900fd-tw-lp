.class public Lorg/icepdf/core/pobjects/graphics/SoftMask;
.super Lorg/icepdf/core/pobjects/Dictionary;
.source "SoftMask.java"


# static fields
.field public static final SOFT_MASK_TYPE_ALPHA:Ljava/lang/String; = "Alpha"

.field public static final SOFT_MASK_TYPE_LUMINOSITY:Ljava/lang/String; = "Luminosity"

.field private static final logger:Ljava/util/logging/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 52
    const-class v0, Lorg/icepdf/core/pobjects/graphics/SoftMask;

    invoke-virtual {v0}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Lorg/icepdf/core/pobjects/graphics/SoftMask;->logger:Ljava/util/logging/Logger;

    return-void
.end method

.method public constructor <init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V
    .locals 0
    .param p1, "library"    # Lorg/icepdf/core/util/Library;
    .param p2, "dictionary"    # Ljava/util/Hashtable;

    .prologue
    .line 60
    invoke-direct {p0, p1, p2}, Lorg/icepdf/core/pobjects/Dictionary;-><init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V

    .line 61
    return-void
.end method


# virtual methods
.method public getBC()Ljava/util/Vector;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Vector",
            "<",
            "Ljava/lang/Number;",
            ">;"
        }
    .end annotation

    .prologue
    .line 110
    iget-object v1, p0, Lorg/icepdf/core/pobjects/graphics/SoftMask;->library:Lorg/icepdf/core/util/Library;

    iget-object v2, p0, Lorg/icepdf/core/pobjects/graphics/SoftMask;->entries:Ljava/util/Hashtable;

    const-string/jumbo v3, "BC"

    invoke-virtual {v1, v2, v3}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 111
    .local v0, "BCKey":Ljava/lang/Object;
    instance-of v1, v0, Ljava/util/Vector;

    if-eqz v1, :cond_0

    .line 112
    check-cast v0, Ljava/util/Vector;

    .line 114
    .end local v0    # "BCKey":Ljava/lang/Object;
    :goto_0
    return-object v0

    .restart local v0    # "BCKey":Ljava/lang/Object;
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getG()Lorg/icepdf/core/pobjects/Form;
    .locals 5

    .prologue
    .line 90
    iget-object v2, p0, Lorg/icepdf/core/pobjects/graphics/SoftMask;->library:Lorg/icepdf/core/util/Library;

    iget-object v3, p0, Lorg/icepdf/core/pobjects/graphics/SoftMask;->entries:Ljava/util/Hashtable;

    const-string/jumbo v4, "G"

    invoke-virtual {v2, v3, v4}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 91
    .local v0, "GKey":Ljava/lang/Object;
    if-eqz v0, :cond_0

    instance-of v2, v0, Lorg/icepdf/core/pobjects/Form;

    if-eqz v2, :cond_0

    move-object v1, v0

    .line 92
    check-cast v1, Lorg/icepdf/core/pobjects/Form;

    .line 93
    .local v1, "smaskForm":Lorg/icepdf/core/pobjects/Form;
    invoke-virtual {v1}, Lorg/icepdf/core/pobjects/Form;->init()V

    .line 96
    .end local v1    # "smaskForm":Lorg/icepdf/core/pobjects/Form;
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getS()Ljava/lang/String;
    .locals 3

    .prologue
    .line 77
    iget-object v0, p0, Lorg/icepdf/core/pobjects/graphics/SoftMask;->library:Lorg/icepdf/core/util/Library;

    iget-object v1, p0, Lorg/icepdf/core/pobjects/graphics/SoftMask;->entries:Ljava/util/Hashtable;

    const-string/jumbo v2, "S"

    invoke-virtual {v0, v1, v2}, Lorg/icepdf/core/util/Library;->getName(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
