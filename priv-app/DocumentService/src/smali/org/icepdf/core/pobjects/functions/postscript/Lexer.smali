.class public Lorg/icepdf/core/pobjects/functions/postscript/Lexer;
.super Ljava/lang/Object;
.source "Lexer.java"


# static fields
.field private static final TOKEN_BOOLEAN:I = 0x5

.field private static final TOKEN_EXPRESSION:I = 0x3

.field private static final TOKEN_NUMBER:I = 0x1

.field private static final TOKEN_OPERAND:I = 0x2


# instance fields
.field private buf:[C

.field private expressionDepth:I

.field private numRead:I

.field private pos:I

.field private reader:Ljava/io/Reader;

.field private stack:Ljava/util/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Stack",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private startTokenPos:I

.field private tokenType:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    const/16 v0, 0x808

    new-array v0, v0, [C

    iput-object v0, p0, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->buf:[C

    .line 36
    iput v1, p0, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->pos:I

    iput v1, p0, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->numRead:I

    iput v1, p0, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->startTokenPos:I

    .line 37
    iput v1, p0, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->tokenType:I

    .line 53
    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    iput-object v0, p0, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->stack:Ljava/util/Stack;

    .line 54
    return-void
.end method

.method private booleanStart()V
    .locals 6

    .prologue
    .line 258
    :goto_0
    iget v0, p0, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->pos:I

    iget v1, p0, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->numRead:I

    if-ge v0, v1, :cond_0

    .line 259
    iget-object v0, p0, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->buf:[C

    iget v1, p0, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->pos:I

    aget-char v0, v0, v1

    invoke-static {v0}, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->isDelimiter(C)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 264
    :cond_0
    iget v0, p0, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->pos:I

    iget v1, p0, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->numRead:I

    if-ge v0, v1, :cond_1

    .line 265
    iget-object v0, p0, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->stack:Ljava/util/Stack;

    new-instance v1, Ljava/lang/String;

    iget-object v2, p0, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->buf:[C

    iget v3, p0, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->startTokenPos:I

    iget v4, p0, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->pos:I

    iget v5, p0, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->startTokenPos:I

    sub-int/2addr v4, v5

    invoke-direct {v1, v2, v3, v4}, Ljava/lang/String;-><init>([CII)V

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 267
    :cond_1
    invoke-direct {p0}, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->parseNextState()V

    .line 268
    return-void

    .line 262
    :cond_2
    iget v0, p0, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->pos:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->pos:I

    goto :goto_0
.end method

.method private expressionStart()V
    .locals 5

    .prologue
    .line 180
    :goto_0
    iget v1, p0, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->pos:I

    iget v2, p0, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->numRead:I

    if-ge v1, v2, :cond_0

    .line 181
    iget-object v1, p0, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->buf:[C

    iget v2, p0, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->pos:I

    aget-char v1, v1, v2

    const/16 v2, 0x7b

    if-eq v1, v2, :cond_4

    iget-object v1, p0, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->buf:[C

    iget v2, p0, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->pos:I

    aget-char v1, v1, v2

    const/16 v2, 0x7d

    if-eq v1, v2, :cond_4

    .line 186
    :cond_0
    iget v1, p0, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->pos:I

    iget v2, p0, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->numRead:I

    if-ge v1, v2, :cond_3

    .line 187
    iget-object v1, p0, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->buf:[C

    iget v2, p0, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->startTokenPos:I

    iget v3, p0, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->pos:I

    iget v4, p0, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->startTokenPos:I

    sub-int/2addr v3, v4

    invoke-static {v1, v2, v3}, Lorg/icepdf/core/pobjects/functions/postscript/OperatorFactory;->getOperator([CII)Lorg/icepdf/core/pobjects/functions/postscript/Operator;

    move-result-object v0

    .line 190
    .local v0, "operand":Lorg/icepdf/core/pobjects/functions/postscript/Operator;
    invoke-virtual {v0}, Lorg/icepdf/core/pobjects/functions/postscript/Operator;->getType()I

    move-result v1

    const/16 v2, 0x2b

    if-ne v1, v2, :cond_1

    .line 191
    iget v1, p0, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->expressionDepth:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->expressionDepth:I

    .line 194
    :cond_1
    iget v1, p0, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->expressionDepth:I

    const/4 v2, 0x1

    if-le v1, v2, :cond_2

    .line 195
    iget-object v1, p0, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->stack:Ljava/util/Stack;

    invoke-virtual {v1, v0}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 198
    :cond_2
    invoke-virtual {v0}, Lorg/icepdf/core/pobjects/functions/postscript/Operator;->getType()I

    move-result v1

    const/16 v2, 0x2c

    if-ne v1, v2, :cond_3

    .line 199
    iget v1, p0, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->expressionDepth:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->expressionDepth:I

    .line 203
    .end local v0    # "operand":Lorg/icepdf/core/pobjects/functions/postscript/Operator;
    :cond_3
    invoke-direct {p0}, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->parseNextState()V

    .line 204
    return-void

    .line 184
    :cond_4
    iget v1, p0, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->pos:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->pos:I

    goto :goto_0
.end method

.method private static isDelimiter(C)Z
    .locals 1
    .param p0, "c"    # C

    .prologue
    .line 276
    const/16 v0, 0x20

    if-eq p0, v0, :cond_0

    const/16 v0, 0x9

    if-eq p0, v0, :cond_0

    const/16 v0, 0xa

    if-eq p0, v0, :cond_0

    const/16 v0, 0xd

    if-eq p0, v0, :cond_0

    const/16 v0, 0x7b

    if-eq p0, v0, :cond_0

    const/16 v0, 0x7d

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private numberStart()V
    .locals 6

    .prologue
    .line 240
    iget v0, p0, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->pos:I

    iput v0, p0, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->startTokenPos:I

    .line 241
    :goto_0
    iget v0, p0, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->pos:I

    iget v1, p0, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->numRead:I

    if-ge v0, v1, :cond_0

    .line 242
    iget-object v0, p0, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->buf:[C

    iget v1, p0, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->pos:I

    aget-char v0, v0, v1

    invoke-static {v0}, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->isDelimiter(C)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 247
    :cond_0
    iget v0, p0, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->pos:I

    iget v1, p0, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->numRead:I

    if-ge v0, v1, :cond_1

    .line 249
    iget-object v0, p0, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->stack:Ljava/util/Stack;

    new-instance v1, Ljava/lang/String;

    iget-object v2, p0, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->buf:[C

    iget v3, p0, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->startTokenPos:I

    iget v4, p0, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->pos:I

    iget v5, p0, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->startTokenPos:I

    sub-int/2addr v4, v5

    invoke-direct {v1, v2, v3, v4}, Ljava/lang/String;-><init>([CII)V

    invoke-static {v1}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v1

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 251
    :cond_1
    invoke-direct {p0}, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->parseNextState()V

    .line 252
    return-void

    .line 245
    :cond_2
    iget v0, p0, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->pos:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->pos:I

    goto :goto_0
.end method

.method private operandStart()V
    .locals 5

    .prologue
    .line 210
    iget v1, p0, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->pos:I

    iput v1, p0, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->startTokenPos:I

    .line 211
    :goto_0
    iget v1, p0, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->pos:I

    iget v2, p0, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->numRead:I

    if-ge v1, v2, :cond_0

    .line 212
    iget-object v1, p0, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->buf:[C

    iget v2, p0, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->pos:I

    aget-char v1, v1, v2

    invoke-static {v1}, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->isDelimiter(C)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 217
    :cond_0
    iget v1, p0, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->pos:I

    iget v2, p0, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->numRead:I

    if-ge v1, v2, :cond_1

    iget v1, p0, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->pos:I

    iget v2, p0, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->startTokenPos:I

    if-le v1, v2, :cond_1

    .line 218
    iget-object v1, p0, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->buf:[C

    iget v2, p0, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->startTokenPos:I

    iget v3, p0, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->pos:I

    iget v4, p0, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->startTokenPos:I

    sub-int/2addr v3, v4

    invoke-static {v1, v2, v3}, Lorg/icepdf/core/pobjects/functions/postscript/OperatorFactory;->getOperator([CII)Lorg/icepdf/core/pobjects/functions/postscript/Operator;

    move-result-object v0

    .line 220
    .local v0, "operand":Lorg/icepdf/core/pobjects/functions/postscript/Operator;
    iget v1, p0, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->expressionDepth:I

    const/4 v2, 0x1

    if-le v1, v2, :cond_3

    .line 221
    iget-object v1, p0, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->stack:Ljava/util/Stack;

    invoke-virtual {v1, v0}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 233
    .end local v0    # "operand":Lorg/icepdf/core/pobjects/functions/postscript/Operator;
    :cond_1
    :goto_1
    invoke-direct {p0}, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->parseNextState()V

    .line 234
    return-void

    .line 215
    :cond_2
    iget v1, p0, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->pos:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->pos:I

    goto :goto_0

    .line 224
    .restart local v0    # "operand":Lorg/icepdf/core/pobjects/functions/postscript/Operator;
    :cond_3
    iget-object v1, p0, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->stack:Ljava/util/Stack;

    invoke-virtual {v0, v1}, Lorg/icepdf/core/pobjects/functions/postscript/Operator;->eval(Ljava/util/Stack;)V

    .line 227
    iget-object v1, p0, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->stack:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v1

    instance-of v1, v1, Lorg/icepdf/core/pobjects/functions/postscript/Operator;

    if-eqz v1, :cond_1

    .line 228
    iget-object v1, p0, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->stack:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "operand":Lorg/icepdf/core/pobjects/functions/postscript/Operator;
    check-cast v0, Lorg/icepdf/core/pobjects/functions/postscript/Operator;

    .line 229
    .restart local v0    # "operand":Lorg/icepdf/core/pobjects/functions/postscript/Operator;
    iget-object v1, p0, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->stack:Ljava/util/Stack;

    invoke-virtual {v0, v1}, Lorg/icepdf/core/pobjects/functions/postscript/Operator;->eval(Ljava/util/Stack;)V

    goto :goto_1
.end method

.method private parseNextState()V
    .locals 3

    .prologue
    const/16 v2, 0x7b

    .line 137
    :goto_0
    iget v0, p0, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->pos:I

    iget v1, p0, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->numRead:I

    if-ge v0, v1, :cond_0

    .line 138
    iget-object v0, p0, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->buf:[C

    iget v1, p0, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->pos:I

    aget-char v0, v0, v1

    const/16 v1, 0x20

    if-eq v0, v1, :cond_2

    iget-object v0, p0, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->buf:[C

    iget v1, p0, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->pos:I

    aget-char v0, v0, v1

    const/16 v1, 0x9

    if-eq v0, v1, :cond_2

    iget-object v0, p0, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->buf:[C

    iget v1, p0, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->pos:I

    aget-char v0, v0, v1

    const/16 v1, 0xa

    if-eq v0, v1, :cond_2

    iget-object v0, p0, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->buf:[C

    iget v1, p0, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->pos:I

    aget-char v0, v0, v1

    const/16 v1, 0xd

    if-eq v0, v1, :cond_2

    .line 146
    :cond_0
    iget v0, p0, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->pos:I

    iget v1, p0, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->numRead:I

    if-ge v0, v1, :cond_1

    .line 147
    iget v0, p0, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->pos:I

    iput v0, p0, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->startTokenPos:I

    .line 149
    iget-object v0, p0, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->buf:[C

    iget v1, p0, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->pos:I

    aget-char v0, v0, v1

    const/16 v1, 0x41

    if-ge v0, v1, :cond_3

    .line 150
    const/4 v0, 0x1

    iput v0, p0, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->tokenType:I

    .line 171
    :cond_1
    :goto_1
    return-void

    .line 142
    :cond_2
    iget v0, p0, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->pos:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->pos:I

    goto :goto_0

    .line 155
    :cond_3
    iget-object v0, p0, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->buf:[C

    iget v1, p0, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->pos:I

    aget-char v0, v0, v1

    const/16 v1, 0x66

    if-ne v0, v1, :cond_4

    iget-object v0, p0, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->buf:[C

    iget v1, p0, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->pos:I

    add-int/lit8 v1, v1, 0x1

    aget-char v0, v0, v1

    const/16 v1, 0x61

    if-eq v0, v1, :cond_5

    :cond_4
    iget-object v0, p0, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->buf:[C

    iget v1, p0, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->pos:I

    aget-char v0, v0, v1

    const/16 v1, 0x74

    if-ne v0, v1, :cond_6

    iget-object v0, p0, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->buf:[C

    iget v1, p0, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->pos:I

    add-int/lit8 v1, v1, 0x3

    aget-char v0, v0, v1

    const/16 v1, 0x65

    if-ne v0, v1, :cond_6

    .line 157
    :cond_5
    const/4 v0, 0x5

    iput v0, p0, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->tokenType:I

    goto :goto_1

    .line 160
    :cond_6
    iget-object v0, p0, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->buf:[C

    iget v1, p0, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->pos:I

    aget-char v0, v0, v1

    if-ge v0, v2, :cond_7

    .line 161
    const/4 v0, 0x2

    iput v0, p0, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->tokenType:I

    goto :goto_1

    .line 164
    :cond_7
    iget-object v0, p0, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->buf:[C

    iget v1, p0, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->pos:I

    aget-char v0, v0, v1

    if-eq v0, v2, :cond_8

    iget-object v0, p0, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->buf:[C

    iget v1, p0, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->pos:I

    aget-char v0, v0, v1

    const/16 v1, 0x7d

    if-ne v0, v1, :cond_9

    .line 165
    :cond_8
    const/4 v0, 0x3

    iput v0, p0, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->tokenType:I

    goto :goto_1

    .line 167
    :cond_9
    invoke-direct {p0}, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->parseNextState()V

    goto :goto_1
.end method


# virtual methods
.method public getStack()Ljava/util/Stack;
    .locals 1

    .prologue
    .line 130
    iget-object v0, p0, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->stack:Ljava/util/Stack;

    return-object v0
.end method

.method public parse([F)V
    .locals 13
    .param p1, "input"    # [F
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v12, 0x3

    const/4 v11, 0x0

    .line 78
    iget-object v6, p0, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->reader:Ljava/io/Reader;

    if-nez v6, :cond_0

    .line 79
    new-instance v6, Ljava/io/IOException;

    const-string/jumbo v7, "Type 4 function, null input stream reader."

    invoke-direct {v6, v7}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 83
    :cond_0
    move-object v0, p1

    .local v0, "arr$":[F
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_1

    aget v6, v0, v2

    invoke-static {v6}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    .line 84
    .local v5, "num":Ljava/lang/Number;
    iget-object v6, p0, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->stack:Ljava/util/Stack;

    invoke-virtual {v6, v5}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 83
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 87
    .end local v5    # "num":Ljava/lang/Number;
    :cond_1
    iput v12, p0, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->tokenType:I

    .line 89
    const/4 v1, 0x0

    .line 90
    .local v1, "done":Z
    :cond_2
    if-nez v1, :cond_4

    .line 94
    iget v6, p0, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->pos:I

    iget-object v7, p0, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->buf:[C

    array-length v7, v7

    if-ne v6, v7, :cond_3

    .line 96
    iget-object v6, p0, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->buf:[C

    iget v7, p0, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->startTokenPos:I

    iget-object v8, p0, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->buf:[C

    iget v9, p0, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->pos:I

    iget v10, p0, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->startTokenPos:I

    sub-int/2addr v9, v10

    invoke-static {v6, v7, v8, v11, v9}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 97
    iget-object v6, p0, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->buf:[C

    array-length v6, v6

    iget v7, p0, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->startTokenPos:I

    sub-int/2addr v6, v7

    iput v6, p0, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->pos:I

    .line 98
    iput v11, p0, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->startTokenPos:I

    .line 99
    iget v6, p0, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->pos:I

    iput v6, p0, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->numRead:I

    .line 102
    :cond_3
    iget-object v6, p0, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->reader:Ljava/io/Reader;

    iget-object v7, p0, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->buf:[C

    iget v8, p0, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->pos:I

    iget-object v9, p0, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->buf:[C

    array-length v9, v9

    iget v10, p0, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->pos:I

    sub-int/2addr v9, v10

    invoke-virtual {v6, v7, v8, v9}, Ljava/io/Reader;->read([CII)I

    move-result v4

    .line 103
    .local v4, "n":I
    if-gtz v4, :cond_5

    .line 119
    .end local v4    # "n":I
    :cond_4
    return-void

    .line 104
    .restart local v4    # "n":I
    :cond_5
    iget v6, p0, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->numRead:I

    add-int/2addr v6, v4

    iput v6, p0, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->numRead:I

    .line 107
    :cond_6
    :goto_1
    iget v6, p0, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->pos:I

    iget v7, p0, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->numRead:I

    if-ge v6, v7, :cond_2

    .line 108
    iget v6, p0, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->tokenType:I

    const/4 v7, 0x1

    if-ne v6, v7, :cond_7

    .line 109
    invoke-direct {p0}, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->numberStart()V

    goto :goto_1

    .line 110
    :cond_7
    iget v6, p0, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->tokenType:I

    const/4 v7, 0x2

    if-ne v6, v7, :cond_8

    .line 111
    invoke-direct {p0}, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->operandStart()V

    goto :goto_1

    .line 112
    :cond_8
    iget v6, p0, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->tokenType:I

    const/4 v7, 0x5

    if-ne v6, v7, :cond_9

    .line 113
    invoke-direct {p0}, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->booleanStart()V

    goto :goto_1

    .line 114
    :cond_9
    iget v6, p0, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->tokenType:I

    if-ne v6, v12, :cond_6

    .line 115
    invoke-direct {p0}, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->expressionStart()V

    goto :goto_1
.end method

.method public setInputStream(Ljava/io/InputStream;)V
    .locals 1
    .param p1, "in"    # Ljava/io/InputStream;

    .prologue
    .line 62
    new-instance v0, Ljava/io/InputStreamReader;

    invoke-direct {v0, p1}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-virtual {p0, v0}, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->setReader(Ljava/io/Reader;)V

    .line 63
    return-void
.end method

.method protected setReader(Ljava/io/Reader;)V
    .locals 0
    .param p1, "reader"    # Ljava/io/Reader;

    .prologue
    .line 66
    iput-object p1, p0, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->reader:Ljava/io/Reader;

    .line 67
    return-void
.end method
