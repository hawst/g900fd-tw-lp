.class public Lorg/icepdf/core/pobjects/HexStringObject;
.super Ljava/lang/Object;
.source "HexStringObject.java"

# interfaces
.implements Lorg/icepdf/core/pobjects/StringObject;


# static fields
.field private static logger:Ljava/util/logging/Logger;


# instance fields
.field reference:Lorg/icepdf/core/pobjects/Reference;

.field private stringData:Ljava/lang/StringBuilder;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    const-class v0, Lorg/icepdf/core/pobjects/HexStringObject;

    invoke-virtual {v0}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Lorg/icepdf/core/pobjects/HexStringObject;->logger:Ljava/util/logging/Logger;

    return-void
.end method

.method public constructor <init>(Ljava/lang/StringBuilder;)V
    .locals 2
    .param p1, "stringBuffer"    # Ljava/lang/StringBuilder;

    .prologue
    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->deleteCharAt(I)Ljava/lang/StringBuilder;

    .line 67
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->deleteCharAt(I)Ljava/lang/StringBuilder;

    .line 69
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    iput-object v0, p0, Lorg/icepdf/core/pobjects/HexStringObject;->stringData:Ljava/lang/StringBuilder;

    .line 70
    iget-object v0, p0, Lorg/icepdf/core/pobjects/HexStringObject;->stringData:Ljava/lang/StringBuilder;

    const/4 v1, 0x2

    invoke-static {p1, v1}, Lorg/icepdf/core/pobjects/HexStringObject;->normalizeHex(Ljava/lang/StringBuilder;I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 71
    return-void
.end method

.method public constructor <init>([B)V
    .locals 2
    .param p1, "bytes"    # [B

    .prologue
    .line 53
    new-instance v0, Ljava/lang/StringBuilder;

    array-length v1, p1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, p1}, Ljava/lang/String;-><init>([B)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/icepdf/core/pobjects/HexStringObject;-><init>(Ljava/lang/StringBuilder;)V

    .line 54
    return-void
.end method

.method private hexToString(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;
    .locals 12
    .param p1, "hh"    # Ljava/lang/StringBuilder;

    .prologue
    const/4 v11, 0x2

    const/16 v10, 0x66

    const/16 v9, 0x46

    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 267
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->length()I

    move-result v4

    if-nez v4, :cond_2

    .line 268
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 296
    :cond_1
    return-object v2

    .line 273
    :cond_2
    invoke-virtual {p1, v6}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v4

    if-ne v4, v9, :cond_4

    move v4, v5

    :goto_0
    invoke-virtual {p1, v6}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v7

    if-ne v7, v10, :cond_5

    move v7, v5

    :goto_1
    or-int/2addr v4, v7

    if-eqz v4, :cond_3

    invoke-virtual {p1, v5}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v4

    const/16 v7, 0x45

    if-ne v4, v7, :cond_6

    move v4, v5

    :goto_2
    invoke-virtual {p1, v5}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v7

    const/16 v8, 0x65

    if-ne v7, v8, :cond_7

    move v7, v5

    :goto_3
    or-int/2addr v4, v7

    if-eqz v4, :cond_3

    invoke-virtual {p1, v11}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v4

    if-ne v4, v9, :cond_8

    move v4, v5

    :goto_4
    invoke-virtual {p1, v11}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v7

    if-ne v7, v10, :cond_9

    move v7, v5

    :goto_5
    or-int/2addr v4, v7

    if-eqz v4, :cond_3

    const/4 v4, 0x3

    invoke-virtual {p1, v4}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v4

    if-ne v4, v9, :cond_a

    move v4, v5

    :goto_6
    const/4 v7, 0x3

    invoke-virtual {p1, v7}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v7

    if-ne v7, v10, :cond_b

    :goto_7
    or-int/2addr v4, v5

    if-nez v4, :cond_c

    .line 277
    :cond_3
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    .line 278
    .local v1, "length":I
    new-instance v2, Ljava/lang/StringBuilder;

    div-int/lit8 v4, v1, 0x2

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 281
    .local v2, "sb":Ljava/lang/StringBuilder;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_8
    if-ge v0, v1, :cond_1

    .line 282
    add-int/lit8 v4, v0, 0x2

    invoke-virtual {p1, v0, v4}, Ljava/lang/StringBuilder;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 283
    .local v3, "subStr":Ljava/lang/String;
    const/16 v4, 0x10

    invoke-static {v3, v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v4

    int-to-char v4, v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 281
    add-int/lit8 v0, v0, 0x2

    goto :goto_8

    .end local v0    # "i":I
    .end local v1    # "length":I
    .end local v2    # "sb":Ljava/lang/StringBuilder;
    .end local v3    # "subStr":Ljava/lang/String;
    :cond_4
    move v4, v6

    .line 273
    goto :goto_0

    :cond_5
    move v7, v6

    goto :goto_1

    :cond_6
    move v4, v6

    goto :goto_2

    :cond_7
    move v7, v6

    goto :goto_3

    :cond_8
    move v4, v6

    goto :goto_4

    :cond_9
    move v7, v6

    goto :goto_5

    :cond_a
    move v4, v6

    goto :goto_6

    :cond_b
    move v5, v6

    goto :goto_7

    .line 289
    :cond_c
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    .line 290
    .restart local v1    # "length":I
    new-instance v2, Ljava/lang/StringBuilder;

    div-int/lit8 v4, v1, 0x4

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 292
    .restart local v2    # "sb":Ljava/lang/StringBuilder;
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_9
    if-ge v0, v1, :cond_1

    .line 293
    add-int/lit8 v4, v0, 0x4

    invoke-virtual {p1, v0, v4}, Ljava/lang/StringBuilder;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 294
    .restart local v3    # "subStr":Ljava/lang/String;
    const/16 v4, 0x10

    invoke-static {v3, v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v4

    int-to-char v4, v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 292
    add-int/lit8 v0, v0, 0x4

    goto :goto_9
.end method

.method private static isNoneHexChar(C)Z
    .locals 1
    .param p0, "c"    # C

    .prologue
    .line 251
    const/16 v0, 0x30

    if-lt p0, v0, :cond_0

    const/16 v0, 0x39

    if-le p0, v0, :cond_3

    :cond_0
    const/16 v0, 0x41

    if-lt p0, v0, :cond_1

    const/16 v0, 0x46

    if-le p0, v0, :cond_3

    :cond_1
    const/16 v0, 0x61

    if-lt p0, v0, :cond_2

    const/16 v0, 0x66

    if-le p0, v0, :cond_3

    :cond_2
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static normalizeHex(Ljava/lang/StringBuilder;I)Ljava/lang/StringBuilder;
    .locals 3
    .param p0, "hex"    # Ljava/lang/StringBuilder;
    .param p1, "step"    # I

    .prologue
    .line 220
    invoke-virtual {p0}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    .line 221
    .local v1, "length":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_1

    .line 222
    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v2

    invoke-static {v2}, Lorg/icepdf/core/pobjects/HexStringObject;->isNoneHexChar(C)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 223
    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->deleteCharAt(I)Ljava/lang/StringBuilder;

    .line 224
    add-int/lit8 v1, v1, -0x1

    .line 225
    add-int/lit8 v0, v0, -0x1

    .line 221
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 228
    :cond_1
    invoke-virtual {p0}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    .line 229
    const/4 v2, 0x2

    if-ne p1, v2, :cond_2

    .line 231
    rem-int/lit8 v2, v1, 0x2

    if-eqz v2, :cond_2

    .line 232
    const/16 v2, 0x30

    invoke-virtual {p0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 235
    :cond_2
    const/4 v2, 0x4

    if-ne p1, v2, :cond_3

    .line 236
    rem-int/lit8 v2, v1, 0x4

    if-eqz v2, :cond_3

    .line 237
    const-string/jumbo v2, "00"

    invoke-virtual {p0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 240
    :cond_3
    return-object p0
.end method


# virtual methods
.method public getDecryptedLiteralString(Lorg/icepdf/core/pobjects/security/SecurityManager;)Ljava/lang/String;
    .locals 3
    .param p1, "securityManager"    # Lorg/icepdf/core/pobjects/security/SecurityManager;

    .prologue
    .line 326
    if-eqz p1, :cond_0

    iget-object v2, p0, Lorg/icepdf/core/pobjects/HexStringObject;->reference:Lorg/icepdf/core/pobjects/Reference;

    if-eqz v2, :cond_0

    .line 328
    invoke-virtual {p1}, Lorg/icepdf/core/pobjects/security/SecurityManager;->getDecryptionKey()[B

    move-result-object v0

    .line 331
    .local v0, "key":[B
    iget-object v2, p0, Lorg/icepdf/core/pobjects/HexStringObject;->stringData:Ljava/lang/StringBuilder;

    invoke-static {v2}, Lorg/icepdf/core/util/Utils;->convertByteCharSequenceToByteArray(Ljava/lang/CharSequence;)[B

    move-result-object v1

    .line 335
    .local v1, "textBytes":[B
    iget-object v2, p0, Lorg/icepdf/core/pobjects/HexStringObject;->reference:Lorg/icepdf/core/pobjects/Reference;

    invoke-virtual {p1, v2, v0, v1}, Lorg/icepdf/core/pobjects/security/SecurityManager;->decrypt(Lorg/icepdf/core/pobjects/Reference;[B[B)[B

    move-result-object v1

    .line 341
    if-eqz v1, :cond_0

    .line 342
    invoke-static {v1}, Lorg/icepdf/core/util/Utils;->convertByteArrayToByteString([B)Ljava/lang/String;

    move-result-object v2

    .line 344
    .end local v0    # "key":[B
    .end local v1    # "textBytes":[B
    :goto_0
    return-object v2

    :cond_0
    invoke-virtual {p0}, Lorg/icepdf/core/pobjects/HexStringObject;->getLiteralString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method public getHexString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, Lorg/icepdf/core/pobjects/HexStringObject;->stringData:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getHexStringBuffer()Ljava/lang/StringBuilder;
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Lorg/icepdf/core/pobjects/HexStringObject;->stringData:Ljava/lang/StringBuilder;

    return-object v0
.end method

.method public getLength()I
    .locals 1

    .prologue
    .line 207
    iget-object v0, p0, Lorg/icepdf/core/pobjects/HexStringObject;->stringData:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    return v0
.end method

.method public getLiteralString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 144
    iget-object v0, p0, Lorg/icepdf/core/pobjects/HexStringObject;->stringData:Ljava/lang/StringBuilder;

    invoke-direct {p0, v0}, Lorg/icepdf/core/pobjects/HexStringObject;->hexToString(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getLiteralStringBuffer()Ljava/lang/StringBuilder;
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lorg/icepdf/core/pobjects/HexStringObject;->stringData:Ljava/lang/StringBuilder;

    invoke-direct {p0, v0}, Lorg/icepdf/core/pobjects/HexStringObject;->hexToString(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;

    move-result-object v0

    return-object v0
.end method

.method public getLiteralStringBuffer(ILorg/icepdf/core/pobjects/fonts/FontFile;)Ljava/lang/StringBuilder;
    .locals 10
    .param p1, "fontFormat"    # I
    .param p2, "font"    # Lorg/icepdf/core/pobjects/fonts/FontFile;

    .prologue
    const/4 v9, 0x2

    .line 162
    const/4 v7, 0x1

    if-eq p1, v7, :cond_0

    invoke-interface {p2}, Lorg/icepdf/core/pobjects/fonts/FontFile;->isOneByteEncoding()Z

    move-result v7

    if-eqz v7, :cond_3

    .line 163
    :cond_0
    new-instance v7, Ljava/lang/StringBuilder;

    iget-object v8, p0, Lorg/icepdf/core/pobjects/HexStringObject;->stringData:Ljava/lang/StringBuilder;

    invoke-static {v8, v9}, Lorg/icepdf/core/pobjects/HexStringObject;->normalizeHex(Ljava/lang/StringBuilder;I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iput-object v7, p0, Lorg/icepdf/core/pobjects/HexStringObject;->stringData:Ljava/lang/StringBuilder;

    .line 164
    const/4 v0, 0x2

    .line 165
    .local v0, "charOffset":I
    invoke-virtual {p0}, Lorg/icepdf/core/pobjects/HexStringObject;->getLength()I

    move-result v4

    .line 166
    .local v4, "length":I
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 167
    .local v6, "tmp":Ljava/lang/StringBuilder;
    const/4 v3, 0x0

    .line 170
    .local v3, "lastIndex":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v4, :cond_6

    .line 171
    add-int v5, v3, v0

    .line 172
    .local v5, "offset":I
    sub-int v7, v2, v3

    invoke-virtual {p0, v7, v5}, Lorg/icepdf/core/pobjects/HexStringObject;->getUnsignedInt(II)I

    move-result v1

    .line 175
    .local v1, "charValue":I
    if-ge v5, v4, :cond_1

    if-eqz v1, :cond_2

    :cond_1
    int-to-char v7, v1

    invoke-interface {p2, v7}, Lorg/icepdf/core/pobjects/fonts/FontFile;->canDisplayEchar(C)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 177
    int-to-char v7, v1

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 178
    const/4 v3, 0x0

    .line 170
    :goto_1
    add-int/2addr v2, v0

    goto :goto_0

    .line 180
    :cond_2
    add-int/2addr v3, v0

    goto :goto_1

    .line 184
    .end local v0    # "charOffset":I
    .end local v1    # "charValue":I
    .end local v2    # "i":I
    .end local v3    # "lastIndex":I
    .end local v4    # "length":I
    .end local v5    # "offset":I
    .end local v6    # "tmp":Ljava/lang/StringBuilder;
    :cond_3
    if-ne p1, v9, :cond_5

    .line 185
    new-instance v7, Ljava/lang/StringBuilder;

    iget-object v8, p0, Lorg/icepdf/core/pobjects/HexStringObject;->stringData:Ljava/lang/StringBuilder;

    const/4 v9, 0x4

    invoke-static {v8, v9}, Lorg/icepdf/core/pobjects/HexStringObject;->normalizeHex(Ljava/lang/StringBuilder;I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iput-object v7, p0, Lorg/icepdf/core/pobjects/HexStringObject;->stringData:Ljava/lang/StringBuilder;

    .line 186
    const/4 v0, 0x4

    .line 187
    .restart local v0    # "charOffset":I
    invoke-virtual {p0}, Lorg/icepdf/core/pobjects/HexStringObject;->getLength()I

    move-result v4

    .line 189
    .restart local v4    # "length":I
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 190
    .restart local v6    # "tmp":Ljava/lang/StringBuilder;
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_2
    if-ge v2, v4, :cond_6

    .line 191
    invoke-virtual {p0, v2, v0}, Lorg/icepdf/core/pobjects/HexStringObject;->getUnsignedInt(II)I

    move-result v1

    .line 192
    .restart local v1    # "charValue":I
    int-to-char v7, v1

    invoke-interface {p2, v7}, Lorg/icepdf/core/pobjects/fonts/FontFile;->canDisplayEchar(C)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 193
    int-to-char v7, v1

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 190
    :cond_4
    add-int/2addr v2, v0

    goto :goto_2

    .line 198
    .end local v0    # "charOffset":I
    .end local v1    # "charValue":I
    .end local v2    # "i":I
    .end local v4    # "length":I
    .end local v6    # "tmp":Ljava/lang/StringBuilder;
    :cond_5
    const/4 v6, 0x0

    :cond_6
    return-object v6
.end method

.method public getReference()Lorg/icepdf/core/pobjects/Reference;
    .locals 1

    .prologue
    .line 315
    iget-object v0, p0, Lorg/icepdf/core/pobjects/HexStringObject;->reference:Lorg/icepdf/core/pobjects/Reference;

    return-object v0
.end method

.method public getUnsignedInt(II)I
    .locals 5
    .param p1, "start"    # I
    .param p2, "offset"    # I

    .prologue
    .line 82
    if-ltz p1, :cond_0

    iget-object v2, p0, Lorg/icepdf/core/pobjects/HexStringObject;->stringData:Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    add-int v3, p1, p2

    if-ge v2, v3, :cond_2

    .line 83
    :cond_0
    const/4 v1, 0x0

    .line 94
    :cond_1
    :goto_0
    return v1

    .line 84
    :cond_2
    const/4 v1, 0x0

    .line 86
    .local v1, "unsignedInt":I
    :try_start_0
    iget-object v2, p0, Lorg/icepdf/core/pobjects/HexStringObject;->stringData:Ljava/lang/StringBuilder;

    add-int v3, p1, p2

    invoke-virtual {v2, p1, v3}, Ljava/lang/StringBuilder;->substring(II)Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x10

    invoke-static {v2, v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    .line 89
    :catch_0
    move-exception v0

    .line 90
    .local v0, "e":Ljava/lang/NumberFormatException;
    sget-object v2, Lorg/icepdf/core/pobjects/HexStringObject;->logger:Ljava/util/logging/Logger;

    sget-object v3, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    invoke-virtual {v2, v3}, Ljava/util/logging/Logger;->isLoggable(Ljava/util/logging/Level;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 91
    sget-object v2, Lorg/icepdf/core/pobjects/HexStringObject;->logger:Ljava/util/logging/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Number Format Exception "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/logging/Logger;->finer(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setReference(Lorg/icepdf/core/pobjects/Reference;)V
    .locals 0
    .param p1, "reference"    # Lorg/icepdf/core/pobjects/Reference;

    .prologue
    .line 306
    iput-object p1, p0, Lorg/icepdf/core/pobjects/HexStringObject;->reference:Lorg/icepdf/core/pobjects/Reference;

    .line 307
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 104
    invoke-virtual {p0}, Lorg/icepdf/core/pobjects/HexStringObject;->getLiteralString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
