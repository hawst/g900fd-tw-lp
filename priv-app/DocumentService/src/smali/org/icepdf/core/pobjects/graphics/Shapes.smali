.class public Lorg/icepdf/core/pobjects/graphics/Shapes;
.super Ljava/lang/Object;
.source "Shapes.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/icepdf/core/pobjects/graphics/Shapes$NoClip;,
        Lorg/icepdf/core/pobjects/graphics/Shapes$Clip;,
        Lorg/icepdf/core/pobjects/graphics/Shapes$Fill;,
        Lorg/icepdf/core/pobjects/graphics/Shapes$Draw;
    }
.end annotation


# static fields
.field private static final logger:Ljava/util/logging/Logger;

.field private static paintDelay:I

.field private static scaleImages:Z


# instance fields
.field private images:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private pageText:Lorg/icepdf/core/pobjects/graphics/text/PageText;

.field private parentPage:Lorg/icepdf/core/pobjects/Page;

.field protected shapes:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/16 v2, 0xc8

    .line 49
    const-class v1, Lorg/icepdf/core/pobjects/graphics/Shapes;

    invoke-virtual {v1}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v1

    sput-object v1, Lorg/icepdf/core/pobjects/graphics/Shapes;->logger:Ljava/util/logging/Logger;

    .line 52
    sput v2, Lorg/icepdf/core/pobjects/graphics/Shapes;->paintDelay:I

    .line 59
    const-string/jumbo v1, "org.icepdf.core.scaleImages"

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lorg/icepdf/core/util/Defs;->sysPropertyBoolean(Ljava/lang/String;Z)Z

    move-result v1

    sput-boolean v1, Lorg/icepdf/core/pobjects/graphics/Shapes;->scaleImages:Z

    .line 65
    :try_start_0
    const-string/jumbo v1, "org.icepdf.core.views.refreshfrequency"

    const/16 v2, 0xc8

    invoke-static {v1, v2}, Lorg/icepdf/core/util/Defs;->intProperty(Ljava/lang/String;I)I

    move-result v1

    sput v1, Lorg/icepdf/core/pobjects/graphics/Shapes;->paintDelay:I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 71
    .local v0, "e":Ljava/lang/NumberFormatException;
    :goto_0
    return-void

    .line 68
    .end local v0    # "e":Ljava/lang/NumberFormatException;
    :catch_0
    move-exception v0

    .line 69
    .restart local v0    # "e":Ljava/lang/NumberFormatException;
    sget-object v1, Lorg/icepdf/core/pobjects/graphics/Shapes;->logger:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string/jumbo v3, "Error reading buffered scale factor"

    invoke-virtual {v1, v2, v3}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 107
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 74
    new-instance v0, Ljava/util/Vector;

    const/16 v1, 0x3e8

    invoke-direct {v0, v1}, Ljava/util/Vector;-><init>(I)V

    iput-object v0, p0, Lorg/icepdf/core/pobjects/graphics/Shapes;->shapes:Ljava/util/Vector;

    .line 76
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lorg/icepdf/core/pobjects/graphics/Shapes;->images:Ljava/util/Vector;

    .line 87
    new-instance v0, Lorg/icepdf/core/pobjects/graphics/text/PageText;

    invoke-direct {v0}, Lorg/icepdf/core/pobjects/graphics/text/PageText;-><init>()V

    iput-object v0, p0, Lorg/icepdf/core/pobjects/graphics/Shapes;->pageText:Lorg/icepdf/core/pobjects/graphics/text/PageText;

    .line 109
    return-void
.end method


# virtual methods
.method public add(Ljava/lang/Object;)V
    .locals 25
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 179
    move-object/from16 v0, p1

    instance-of v0, v0, Landroid/graphics/Bitmap;

    move/from16 v21, v0

    if-eqz v21, :cond_4

    move-object/from16 v8, p1

    .line 180
    check-cast v8, Landroid/graphics/Bitmap;

    .line 181
    .local v8, "image":Landroid/graphics/Bitmap;
    invoke-virtual {v8}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v20

    .line 182
    .local v20, "width":I
    invoke-virtual {v8}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    .line 186
    .local v7, "height":I
    sget-boolean v21, Lorg/icepdf/core/pobjects/graphics/Shapes;->scaleImages:Z

    if-eqz v21, :cond_3

    .line 187
    const-wide/high16 v14, 0x3ff0000000000000L    # 1.0

    .line 188
    .local v14, "scaleFactor":D
    const/16 v21, 0x5dc

    move/from16 v0, v20

    move/from16 v1, v21

    if-lt v0, v1, :cond_1

    .line 189
    const-wide/high16 v14, 0x3fe0000000000000L    # 0.5

    .line 193
    :cond_0
    :goto_0
    const-wide/high16 v22, 0x3ff0000000000000L    # 1.0

    cmpg-double v21, v14, v22

    if-gez v21, :cond_2

    .line 202
    move/from16 v0, v20

    int-to-double v0, v0

    move-wide/from16 v22, v0

    mul-double v22, v22, v14

    move-wide/from16 v0, v22

    double-to-int v11, v0

    .line 203
    .local v11, "newWidth":I
    int-to-double v0, v7

    move-wide/from16 v22, v0

    mul-double v22, v22, v14

    move-wide/from16 v0, v22

    double-to-int v10, v0

    .line 205
    .local v10, "newHeight":I
    sget-object v21, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    move-object/from16 v0, v21

    invoke-static {v11, v10, v0}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v17

    .line 208
    .local v17, "scaledImage":Landroid/graphics/Bitmap;
    int-to-float v0, v11

    move/from16 v21, v0

    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v22, v0

    div-float v12, v21, v22

    .line 209
    .local v12, "ratioX":F
    int-to-float v0, v10

    move/from16 v21, v0

    int-to-float v0, v7

    move/from16 v22, v0

    div-float v13, v21, v22

    .line 210
    .local v13, "ratioY":F
    int-to-float v0, v11

    move/from16 v21, v0

    const/high16 v22, 0x40000000    # 2.0f

    div-float v5, v21, v22

    .line 211
    .local v5, "centerX":F
    int-to-float v0, v10

    move/from16 v21, v0

    const/high16 v22, 0x40000000    # 2.0f

    div-float v6, v21, v22

    .line 213
    .local v6, "centerY":F
    new-instance v16, Landroid/graphics/Matrix;

    invoke-direct/range {v16 .. v16}, Landroid/graphics/Matrix;-><init>()V

    .line 214
    .local v16, "scaleMatrix":Landroid/graphics/Matrix;
    move-object/from16 v0, v16

    invoke-virtual {v0, v12, v13, v5, v6}, Landroid/graphics/Matrix;->setScale(FFFF)V

    .line 216
    new-instance v4, Landroid/graphics/Canvas;

    move-object/from16 v0, v17

    invoke-direct {v4, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 217
    .local v4, "canvas":Landroid/graphics/Canvas;
    move-object/from16 v0, v16

    invoke-virtual {v4, v0}, Landroid/graphics/Canvas;->setMatrix(Landroid/graphics/Matrix;)V

    .line 218
    div-int/lit8 v21, v20, 0x2

    move/from16 v0, v21

    int-to-float v0, v0

    move/from16 v21, v0

    sub-float v21, v5, v21

    div-int/lit8 v22, v7, 0x2

    move/from16 v0, v22

    int-to-float v0, v0

    move/from16 v22, v0

    sub-float v22, v6, v22

    new-instance v23, Landroid/graphics/Paint;

    const/16 v24, 0x2

    invoke-direct/range {v23 .. v24}, Landroid/graphics/Paint;-><init>(I)V

    move/from16 v0, v21

    move/from16 v1, v22

    move-object/from16 v2, v23

    invoke-virtual {v4, v8, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 228
    .end local v4    # "canvas":Landroid/graphics/Canvas;
    .end local v5    # "centerX":F
    .end local v6    # "centerY":F
    .end local v10    # "newHeight":I
    .end local v11    # "newWidth":I
    .end local v12    # "ratioX":F
    .end local v13    # "ratioY":F
    .end local v14    # "scaleFactor":D
    .end local v16    # "scaleMatrix":Landroid/graphics/Matrix;
    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/core/pobjects/graphics/Shapes;->images:Ljava/util/Vector;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 229
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/core/pobjects/graphics/Shapes;->shapes:Ljava/util/Vector;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 254
    .end local v7    # "height":I
    .end local v8    # "image":Landroid/graphics/Bitmap;
    .end local v17    # "scaledImage":Landroid/graphics/Bitmap;
    .end local v20    # "width":I
    :goto_2
    return-void

    .line 190
    .restart local v7    # "height":I
    .restart local v8    # "image":Landroid/graphics/Bitmap;
    .restart local v14    # "scaleFactor":D
    .restart local v20    # "width":I
    :cond_1
    const/16 v21, 0x3e8

    move/from16 v0, v20

    move/from16 v1, v21

    if-le v0, v1, :cond_0

    .line 191
    const-wide/high16 v14, 0x3fe8000000000000L    # 0.75

    goto/16 :goto_0

    .line 223
    :cond_2
    move-object/from16 v17, v8

    .restart local v17    # "scaledImage":Landroid/graphics/Bitmap;
    goto :goto_1

    .line 226
    .end local v14    # "scaleFactor":D
    .end local v17    # "scaledImage":Landroid/graphics/Bitmap;
    :cond_3
    move-object/from16 v17, v8

    .restart local v17    # "scaledImage":Landroid/graphics/Bitmap;
    goto :goto_1

    .line 236
    .end local v7    # "height":I
    .end local v8    # "image":Landroid/graphics/Bitmap;
    .end local v17    # "scaledImage":Landroid/graphics/Bitmap;
    .end local v20    # "width":I
    :cond_4
    move-object/from16 v0, p1

    instance-of v0, v0, Ljava/util/Vector;

    move/from16 v21, v0

    if-eqz v21, :cond_6

    move-object/from16 v18, p1

    .line 237
    check-cast v18, Ljava/util/Vector;

    .line 238
    .local v18, "tmp":Ljava/util/Vector;
    invoke-virtual/range {v18 .. v18}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v9

    .line 240
    .local v9, "iterator":Ljava/util/Iterator;
    :cond_5
    :goto_3
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v21

    if-eqz v21, :cond_6

    .line 241
    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v19

    .line 242
    .local v19, "tmpImage":Ljava/lang/Object;
    move-object/from16 v0, v19

    instance-of v0, v0, Landroid/graphics/Bitmap;

    move/from16 v21, v0

    if-eqz v21, :cond_5

    .line 243
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/core/pobjects/graphics/Shapes;->images:Ljava/util/Vector;

    move-object/from16 v21, v0

    check-cast v19, Landroid/graphics/Bitmap;

    .end local v19    # "tmpImage":Ljava/lang/Object;
    move-object/from16 v0, v21

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    goto :goto_3

    .line 248
    .end local v9    # "iterator":Ljava/util/Iterator;
    .end local v18    # "tmp":Ljava/util/Vector;
    :cond_6
    move-object/from16 v0, p1

    instance-of v0, v0, Lorg/icepdf/core/pobjects/graphics/Shapes;

    move/from16 v21, v0

    if-eqz v21, :cond_7

    move-object/from16 v18, p1

    .line 249
    check-cast v18, Lorg/icepdf/core/pobjects/graphics/Shapes;

    .line 250
    .local v18, "tmp":Lorg/icepdf/core/pobjects/graphics/Shapes;
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/core/pobjects/graphics/Shapes;->pageText:Lorg/icepdf/core/pobjects/graphics/text/PageText;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lorg/icepdf/core/pobjects/graphics/text/PageText;->getPageLines()Ljava/util/ArrayList;

    move-result-object v21

    invoke-virtual/range {v18 .. v18}, Lorg/icepdf/core/pobjects/graphics/Shapes;->getPageText()Lorg/icepdf/core/pobjects/graphics/text/PageText;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Lorg/icepdf/core/pobjects/graphics/text/PageText;->getPageLines()Ljava/util/ArrayList;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 252
    .end local v18    # "tmp":Lorg/icepdf/core/pobjects/graphics/Shapes;
    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/core/pobjects/graphics/Shapes;->shapes:Ljava/util/Vector;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    goto :goto_2
.end method

.method public addClipCommand()V
    .locals 2

    .prologue
    .line 278
    iget-object v0, p0, Lorg/icepdf/core/pobjects/graphics/Shapes;->shapes:Ljava/util/Vector;

    new-instance v1, Lorg/icepdf/core/pobjects/graphics/Shapes$Clip;

    invoke-direct {v1}, Lorg/icepdf/core/pobjects/graphics/Shapes$Clip;-><init>()V

    invoke-virtual {v0, v1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 279
    return-void
.end method

.method public addDrawCommand()V
    .locals 2

    .prologue
    .line 261
    iget-object v0, p0, Lorg/icepdf/core/pobjects/graphics/Shapes;->shapes:Ljava/util/Vector;

    new-instance v1, Lorg/icepdf/core/pobjects/graphics/Shapes$Draw;

    invoke-direct {v1}, Lorg/icepdf/core/pobjects/graphics/Shapes$Draw;-><init>()V

    invoke-virtual {v0, v1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 262
    return-void
.end method

.method public addFillCommand()V
    .locals 2

    .prologue
    .line 269
    iget-object v0, p0, Lorg/icepdf/core/pobjects/graphics/Shapes;->shapes:Ljava/util/Vector;

    new-instance v1, Lorg/icepdf/core/pobjects/graphics/Shapes$Fill;

    invoke-direct {v1}, Lorg/icepdf/core/pobjects/graphics/Shapes$Fill;-><init>()V

    invoke-virtual {v0, v1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 270
    return-void
.end method

.method public addNoClipCommand()V
    .locals 2

    .prologue
    .line 285
    iget-object v0, p0, Lorg/icepdf/core/pobjects/graphics/Shapes;->shapes:Ljava/util/Vector;

    new-instance v1, Lorg/icepdf/core/pobjects/graphics/Shapes$NoClip;

    invoke-direct {v1}, Lorg/icepdf/core/pobjects/graphics/Shapes$NoClip;-><init>()V

    invoke-virtual {v0, v1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 286
    return-void
.end method

.method public contract()V
    .locals 2

    .prologue
    .line 685
    iget-object v0, p0, Lorg/icepdf/core/pobjects/graphics/Shapes;->shapes:Ljava/util/Vector;

    if-eqz v0, :cond_0

    .line 686
    iget-object v0, p0, Lorg/icepdf/core/pobjects/graphics/Shapes;->shapes:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->capacity()I

    move-result v0

    iget-object v1, p0, Lorg/icepdf/core/pobjects/graphics/Shapes;->shapes:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v1

    sub-int/2addr v0, v1

    const/16 v1, 0xc8

    if-le v0, v1, :cond_0

    .line 687
    iget-object v0, p0, Lorg/icepdf/core/pobjects/graphics/Shapes;->shapes:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->trimToSize()V

    .line 690
    :cond_0
    return-void
.end method

.method public dispose()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 141
    iget-object v2, p0, Lorg/icepdf/core/pobjects/graphics/Shapes;->images:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->clear()V

    .line 142
    iget-object v2, p0, Lorg/icepdf/core/pobjects/graphics/Shapes;->images:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->trimToSize()V

    .line 145
    iget-object v2, p0, Lorg/icepdf/core/pobjects/graphics/Shapes;->shapes:Ljava/util/Vector;

    if-eqz v2, :cond_3

    .line 146
    iget-object v2, p0, Lorg/icepdf/core/pobjects/graphics/Shapes;->shapes:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 147
    .local v1, "tmp":Ljava/lang/Object;
    instance-of v2, v1, Landroid/graphics/Bitmap;

    if-nez v2, :cond_0

    .line 151
    instance-of v2, v1, Lorg/icepdf/core/pobjects/graphics/TextSprite;

    if-eqz v2, :cond_1

    .line 152
    check-cast v1, Lorg/icepdf/core/pobjects/graphics/TextSprite;

    .end local v1    # "tmp":Ljava/lang/Object;
    invoke-virtual {v1}, Lorg/icepdf/core/pobjects/graphics/TextSprite;->dispose()V

    goto :goto_0

    .line 153
    .restart local v1    # "tmp":Ljava/lang/Object;
    :cond_1
    instance-of v2, v1, Lorg/icepdf/core/pobjects/graphics/Shapes;

    if-eqz v2, :cond_0

    .line 154
    check-cast v1, Lorg/icepdf/core/pobjects/graphics/Shapes;

    .end local v1    # "tmp":Ljava/lang/Object;
    invoke-virtual {v1}, Lorg/icepdf/core/pobjects/graphics/Shapes;->dispose()V

    goto :goto_0

    .line 160
    :cond_2
    iget-object v2, p0, Lorg/icepdf/core/pobjects/graphics/Shapes;->shapes:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->clear()V

    .line 161
    iget-object v2, p0, Lorg/icepdf/core/pobjects/graphics/Shapes;->shapes:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->trimToSize()V

    .line 162
    iput-object v3, p0, Lorg/icepdf/core/pobjects/graphics/Shapes;->shapes:Ljava/util/Vector;

    .line 165
    .end local v0    # "i$":Ljava/util/Iterator;
    :cond_3
    iget-object v2, p0, Lorg/icepdf/core/pobjects/graphics/Shapes;->pageText:Lorg/icepdf/core/pobjects/graphics/text/PageText;

    if-eqz v2, :cond_4

    .line 166
    iget-object v2, p0, Lorg/icepdf/core/pobjects/graphics/Shapes;->pageText:Lorg/icepdf/core/pobjects/graphics/text/PageText;

    invoke-virtual {v2}, Lorg/icepdf/core/pobjects/graphics/text/PageText;->dispose()V

    .line 167
    iput-object v3, p0, Lorg/icepdf/core/pobjects/graphics/Shapes;->pageText:Lorg/icepdf/core/pobjects/graphics/text/PageText;

    .line 169
    :cond_4
    return-void
.end method

.method public getImages()Ljava/util/Vector;
    .locals 1

    .prologue
    .line 681
    iget-object v0, p0, Lorg/icepdf/core/pobjects/graphics/Shapes;->images:Ljava/util/Vector;

    return-object v0
.end method

.method public getPageText()Lorg/icepdf/core/pobjects/graphics/text/PageText;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lorg/icepdf/core/pobjects/graphics/Shapes;->pageText:Lorg/icepdf/core/pobjects/graphics/text/PageText;

    return-object v0
.end method

.method public getShapesCount()I
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lorg/icepdf/core/pobjects/graphics/Shapes;->shapes:Ljava/util/Vector;

    if-eqz v0, :cond_0

    .line 122
    iget-object v0, p0, Lorg/icepdf/core/pobjects/graphics/Shapes;->shapes:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    .line 124
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public declared-synchronized paint(Landroid/graphics/Canvas;)V
    .locals 1
    .param p1, "g"    # Landroid/graphics/Canvas;

    .prologue
    .line 294
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0, p1, v0}, Lorg/icepdf/core/pobjects/graphics/Shapes;->paint(Landroid/graphics/Canvas;Lorg/icepdf/core/views/swing/PagePainter;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 295
    monitor-exit p0

    return-void

    .line 294
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized paint(Landroid/graphics/Canvas;Lorg/icepdf/core/views/swing/PagePainter;)V
    .locals 56
    .param p1, "g"    # Landroid/graphics/Canvas;
    .param p2, "pagePainter"    # Lorg/icepdf/core/views/swing/PagePainter;

    .prologue
    .line 311
    monitor-enter p0

    :try_start_0
    const-string/jumbo v52, "org.icepdf.core.paint.disableClipping"

    const/16 v53, 0x0

    invoke-static/range {v52 .. v53}, Lorg/icepdf/core/util/Defs;->sysPropertyBoolean(Ljava/lang/String;Z)Z

    move-result v26

    .line 320
    .local v26, "disableClipping":Z
    const/16 v39, 0x0

    .line 321
    .local v39, "shape":Landroid/graphics/Path;
    new-instance v34, Landroid/graphics/Paint;

    const/16 v52, 0x1

    move-object/from16 v0, v34

    move/from16 v1, v52

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    .line 323
    .local v34, "myPaint":Landroid/graphics/Paint;
    const/high16 v52, -0x1000000

    move-object/from16 v0, v34

    move/from16 v1, v52

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 324
    sget-object v52, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    move-object/from16 v0, v34

    move-object/from16 v1, v52

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 326
    new-instance v13, Landroid/graphics/Matrix;

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->getMatrix()Landroid/graphics/Matrix;

    move-result-object v52

    move-object/from16 v0, v52

    invoke-direct {v13, v0}, Landroid/graphics/Matrix;-><init>(Landroid/graphics/Matrix;)V

    .line 328
    .local v13, "base":Landroid/graphics/Matrix;
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->getClipBounds()Landroid/graphics/Rect;

    move-result-object v19

    .line 329
    .local v19, "clipRect":Landroid/graphics/Rect;
    new-instance v16, Landroid/graphics/Path;

    invoke-direct/range {v16 .. v16}, Landroid/graphics/Path;-><init>()V

    .line 330
    .local v16, "clip":Landroid/graphics/Path;
    move-object/from16 v0, v19

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v52, v0

    move/from16 v0, v52

    int-to-float v0, v0

    move/from16 v52, v0

    move-object/from16 v0, v19

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v53, v0

    move/from16 v0, v53

    int-to-float v0, v0

    move/from16 v53, v0

    move-object/from16 v0, v16

    move/from16 v1, v52

    move/from16 v2, v53

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 331
    move-object/from16 v0, v19

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v52, v0

    move/from16 v0, v52

    int-to-float v0, v0

    move/from16 v52, v0

    move-object/from16 v0, v19

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v53, v0

    move/from16 v0, v53

    int-to-float v0, v0

    move/from16 v53, v0

    move-object/from16 v0, v16

    move/from16 v1, v52

    move/from16 v2, v53

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 332
    move-object/from16 v0, v19

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v52, v0

    move/from16 v0, v52

    int-to-float v0, v0

    move/from16 v52, v0

    move-object/from16 v0, v19

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    move/from16 v53, v0

    move/from16 v0, v53

    int-to-float v0, v0

    move/from16 v53, v0

    move-object/from16 v0, v16

    move/from16 v1, v52

    move/from16 v2, v53

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 333
    move-object/from16 v0, v19

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v52, v0

    move/from16 v0, v52

    int-to-float v0, v0

    move/from16 v52, v0

    move-object/from16 v0, v19

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    move/from16 v53, v0

    move/from16 v0, v53

    int-to-float v0, v0

    move/from16 v53, v0

    move-object/from16 v0, v16

    move/from16 v1, v52

    move/from16 v2, v53

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 334
    invoke-virtual/range {v16 .. v16}, Landroid/graphics/Path;->close()V

    .line 341
    new-instance v17, Landroid/graphics/Path;

    invoke-direct/range {v17 .. v17}, Landroid/graphics/Path;-><init>()V

    .line 343
    .local v17, "clipArea":Landroid/graphics/Path;
    if-eqz v16, :cond_0

    .line 344
    new-instance v17, Landroid/graphics/Path;

    .end local v17    # "clipArea":Landroid/graphics/Path;
    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-direct {v0, v1}, Landroid/graphics/Path;-><init>(Landroid/graphics/Path;)V

    .line 349
    .restart local v17    # "clipArea":Landroid/graphics/Path;
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v32

    .line 352
    .local v32, "lastPaintTime":J
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/core/pobjects/graphics/Shapes;->shapes:Ljava/util/Vector;

    move-object/from16 v52, v0

    invoke-virtual/range {v52 .. v52}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v41

    .local v41, "shapesEnumeration":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Object;>;"
    move-object/from16 v18, v17

    .end local v17    # "clipArea":Landroid/graphics/Path;
    .local v18, "clipArea":Landroid/graphics/Path;
    move-object/from16 v35, v34

    .line 353
    .end local v34    # "myPaint":Landroid/graphics/Paint;
    .local v35, "myPaint":Landroid/graphics/Paint;
    :goto_0
    invoke-interface/range {v41 .. v41}, Ljava/util/Iterator;->hasNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v52

    if-eqz v52, :cond_1f

    .line 361
    :try_start_1
    invoke-interface/range {v41 .. v41}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v36

    .line 363
    .local v36, "nextShape":Ljava/lang/Object;
    move-object/from16 v0, v36

    instance-of v0, v0, Lorg/icepdf/core/pobjects/graphics/TextSprite;

    move/from16 v52, v0

    if-eqz v52, :cond_1

    .line 364
    move-object/from16 v0, v36

    check-cast v0, Lorg/icepdf/core/pobjects/graphics/TextSprite;

    move-object/from16 v52, v0

    move-object/from16 v0, v52

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lorg/icepdf/core/pobjects/graphics/TextSprite;->intersects(Landroid/graphics/Path;)Z

    move-result v52

    if-eqz v52, :cond_1e

    .line 365
    move-object/from16 v0, v36

    check-cast v0, Lorg/icepdf/core/pobjects/graphics/TextSprite;

    move-object/from16 v52, v0

    move-object/from16 v0, v52

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Lorg/icepdf/core/pobjects/graphics/TextSprite;->setStrokePaint(Landroid/graphics/Paint;)V

    .line 366
    check-cast v36, Lorg/icepdf/core/pobjects/graphics/TextSprite;

    .end local v36    # "nextShape":Ljava/lang/Object;
    move-object/from16 v0, v36

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Lorg/icepdf/core/pobjects/graphics/TextSprite;->paint(Landroid/graphics/Canvas;)V

    .line 368
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v24

    .line 369
    .local v24, "currentTime":J
    sub-long v52, v24, v32

    sget v54, Lorg/icepdf/core/pobjects/graphics/Shapes;->paintDelay:I

    move/from16 v0, v54

    int-to-long v0, v0

    move-wide/from16 v54, v0

    cmp-long v52, v52, v54

    if-lez v52, :cond_1e

    .line 371
    move-wide/from16 v32, v24

    .line 372
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/core/pobjects/graphics/Shapes;->parentPage:Lorg/icepdf/core/pobjects/Page;

    move-object/from16 v52, v0

    if-eqz v52, :cond_1e

    .line 373
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/core/pobjects/graphics/Shapes;->parentPage:Lorg/icepdf/core/pobjects/Page;

    move-object/from16 v52, v0

    invoke-virtual/range {v52 .. v52}, Lorg/icepdf/core/pobjects/Page;->notifyPaintPageListeners()V

    move-object/from16 v17, v18

    .end local v18    # "clipArea":Landroid/graphics/Path;
    .restart local v17    # "clipArea":Landroid/graphics/Path;
    move-object/from16 v34, v35

    .end local v24    # "currentTime":J
    .end local v35    # "myPaint":Landroid/graphics/Paint;
    .restart local v34    # "myPaint":Landroid/graphics/Paint;
    :goto_1
    move-object/from16 v18, v17

    .end local v17    # "clipArea":Landroid/graphics/Path;
    .restart local v18    # "clipArea":Landroid/graphics/Path;
    move-object/from16 v35, v34

    .line 649
    .end local v34    # "myPaint":Landroid/graphics/Paint;
    .restart local v35    # "myPaint":Landroid/graphics/Paint;
    goto :goto_0

    .line 377
    .restart local v36    # "nextShape":Ljava/lang/Object;
    :cond_1
    move-object/from16 v0, v36

    instance-of v0, v0, Landroid/graphics/RectF;

    move/from16 v52, v0

    if-nez v52, :cond_2

    move-object/from16 v0, v36

    instance-of v0, v0, Landroid/graphics/Rect;

    move/from16 v52, v0

    if-eqz v52, :cond_3

    .line 379
    :cond_2
    check-cast v36, Landroid/graphics/RectF;

    .end local v36    # "nextShape":Ljava/lang/Object;
    const/16 v52, 0x1

    move-object/from16 v0, v18

    move-object/from16 v1, v36

    move/from16 v2, v52

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->computeBounds(Landroid/graphics/RectF;Z)V

    move-object/from16 v17, v18

    .end local v18    # "clipArea":Landroid/graphics/Path;
    .restart local v17    # "clipArea":Landroid/graphics/Path;
    move-object/from16 v34, v35

    .end local v35    # "myPaint":Landroid/graphics/Paint;
    .restart local v34    # "myPaint":Landroid/graphics/Paint;
    goto :goto_1

    .line 380
    .end local v17    # "clipArea":Landroid/graphics/Path;
    .end local v34    # "myPaint":Landroid/graphics/Paint;
    .restart local v18    # "clipArea":Landroid/graphics/Path;
    .restart local v35    # "myPaint":Landroid/graphics/Paint;
    .restart local v36    # "nextShape":Ljava/lang/Object;
    :cond_3
    move-object/from16 v0, v36

    instance-of v0, v0, Landroid/graphics/Path;

    move/from16 v52, v0

    if-eqz v52, :cond_4

    .line 381
    move-object/from16 v0, v36

    check-cast v0, Landroid/graphics/Path;

    move-object/from16 v39, v0

    move-object/from16 v17, v18

    .end local v18    # "clipArea":Landroid/graphics/Path;
    .restart local v17    # "clipArea":Landroid/graphics/Path;
    move-object/from16 v34, v35

    .end local v35    # "myPaint":Landroid/graphics/Paint;
    .restart local v34    # "myPaint":Landroid/graphics/Paint;
    goto :goto_1

    .line 382
    .end local v17    # "clipArea":Landroid/graphics/Path;
    .end local v34    # "myPaint":Landroid/graphics/Paint;
    .restart local v18    # "clipArea":Landroid/graphics/Path;
    .restart local v35    # "myPaint":Landroid/graphics/Paint;
    :cond_4
    move-object/from16 v0, v36

    instance-of v0, v0, Landroid/graphics/Paint$Style;

    move/from16 v52, v0

    if-eqz v52, :cond_5

    .line 383
    move-object/from16 v0, v36

    check-cast v0, Landroid/graphics/Paint$Style;

    move-object/from16 v42, v0

    .line 384
    .local v42, "style":Landroid/graphics/Paint$Style;
    move-object/from16 v0, v35

    move-object/from16 v1, v42

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    move-object/from16 v17, v18

    .end local v18    # "clipArea":Landroid/graphics/Path;
    .restart local v17    # "clipArea":Landroid/graphics/Path;
    move-object/from16 v34, v35

    .line 385
    .end local v35    # "myPaint":Landroid/graphics/Paint;
    .restart local v34    # "myPaint":Landroid/graphics/Paint;
    goto :goto_1

    .end local v17    # "clipArea":Landroid/graphics/Path;
    .end local v34    # "myPaint":Landroid/graphics/Paint;
    .end local v42    # "style":Landroid/graphics/Paint$Style;
    .restart local v18    # "clipArea":Landroid/graphics/Path;
    .restart local v35    # "myPaint":Landroid/graphics/Paint;
    :cond_5
    move-object/from16 v0, v36

    instance-of v0, v0, Lorg/icepdf/core/pobjects/graphics/Shapes$Fill;

    move/from16 v52, v0

    if-eqz v52, :cond_9

    .line 387
    new-instance v23, Landroid/graphics/RectF;

    invoke-direct/range {v23 .. v23}, Landroid/graphics/RectF;-><init>()V

    .line 388
    .local v23, "cliprect":Landroid/graphics/RectF;
    new-instance v40, Landroid/graphics/RectF;

    invoke-direct/range {v40 .. v40}, Landroid/graphics/RectF;-><init>()V

    .line 389
    .local v40, "shapeRect":Landroid/graphics/RectF;
    const/16 v52, 0x1

    move-object/from16 v0, v18

    move-object/from16 v1, v23

    move/from16 v2, v52

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->computeBounds(Landroid/graphics/RectF;Z)V

    .line 391
    if-eqz v39, :cond_6

    .line 392
    const/16 v52, 0x1

    move-object/from16 v0, v39

    move-object/from16 v1, v40

    move/from16 v2, v52

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->computeBounds(Landroid/graphics/RectF;Z)V

    .line 394
    :cond_6
    move-object/from16 v0, v23

    move-object/from16 v1, v40

    invoke-static {v0, v1}, Lcom/samsung/thumbnail/util/Utils;->isIntersecting(Landroid/graphics/RectF;Landroid/graphics/RectF;)Z

    move-result v52

    if-eqz v52, :cond_8

    .line 396
    sget-object v52, Landroid/graphics/Paint$Style;->FILL_AND_STROKE:Landroid/graphics/Paint$Style;

    move-object/from16 v0, v35

    move-object/from16 v1, v52

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 398
    if-eqz v39, :cond_7

    .line 399
    move-object/from16 v0, p1

    move-object/from16 v1, v39

    move-object/from16 v2, v35

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 401
    :cond_7
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v24

    .line 402
    .restart local v24    # "currentTime":J
    sub-long v52, v24, v32

    sget v54, Lorg/icepdf/core/pobjects/graphics/Shapes;->paintDelay:I

    move/from16 v0, v54

    int-to-long v0, v0

    move-wide/from16 v54, v0

    cmp-long v52, v52, v54

    if-lez v52, :cond_8

    .line 404
    move-wide/from16 v32, v24

    .line 406
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/core/pobjects/graphics/Shapes;->parentPage:Lorg/icepdf/core/pobjects/Page;

    move-object/from16 v52, v0

    if-eqz v52, :cond_8

    .line 407
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/core/pobjects/graphics/Shapes;->parentPage:Lorg/icepdf/core/pobjects/Page;

    move-object/from16 v52, v0

    invoke-virtual/range {v52 .. v52}, Lorg/icepdf/core/pobjects/Page;->notifyPaintPageListeners()V

    .end local v24    # "currentTime":J
    :cond_8
    move-object/from16 v17, v18

    .end local v18    # "clipArea":Landroid/graphics/Path;
    .restart local v17    # "clipArea":Landroid/graphics/Path;
    move-object/from16 v34, v35

    .line 410
    .end local v35    # "myPaint":Landroid/graphics/Paint;
    .restart local v34    # "myPaint":Landroid/graphics/Paint;
    goto/16 :goto_1

    .end local v17    # "clipArea":Landroid/graphics/Path;
    .end local v23    # "cliprect":Landroid/graphics/RectF;
    .end local v34    # "myPaint":Landroid/graphics/Paint;
    .end local v40    # "shapeRect":Landroid/graphics/RectF;
    .restart local v18    # "clipArea":Landroid/graphics/Path;
    .restart local v35    # "myPaint":Landroid/graphics/Paint;
    :cond_9
    move-object/from16 v0, v36

    instance-of v0, v0, Landroid/graphics/Matrix;

    move/from16 v52, v0

    if-eqz v52, :cond_a

    .line 411
    new-instance v10, Landroid/graphics/Matrix;

    invoke-direct {v10, v13}, Landroid/graphics/Matrix;-><init>(Landroid/graphics/Matrix;)V

    .line 412
    .local v10, "af":Landroid/graphics/Matrix;
    check-cast v36, Landroid/graphics/Matrix;

    .end local v36    # "nextShape":Ljava/lang/Object;
    move-object/from16 v0, v36

    invoke-virtual {v10, v0}, Landroid/graphics/Matrix;->preConcat(Landroid/graphics/Matrix;)Z

    .line 413
    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/graphics/Canvas;->setMatrix(Landroid/graphics/Matrix;)V

    .line 415
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->getClipBounds()Landroid/graphics/Rect;

    move-result-object v52

    if-eqz v52, :cond_23

    .line 417
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->getClipBounds()Landroid/graphics/Rect;

    move-result-object v19

    .line 418
    new-instance v20, Landroid/graphics/Path;

    invoke-direct/range {v20 .. v20}, Landroid/graphics/Path;-><init>()V

    .line 419
    .local v20, "clip_new":Landroid/graphics/Path;
    move-object/from16 v0, v19

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v52, v0

    move/from16 v0, v52

    int-to-float v0, v0

    move/from16 v52, v0

    move-object/from16 v0, v19

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v53, v0

    move/from16 v0, v53

    int-to-float v0, v0

    move/from16 v53, v0

    move-object/from16 v0, v20

    move/from16 v1, v52

    move/from16 v2, v53

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 420
    move-object/from16 v0, v19

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v52, v0

    move/from16 v0, v52

    int-to-float v0, v0

    move/from16 v52, v0

    move-object/from16 v0, v19

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v53, v0

    move/from16 v0, v53

    int-to-float v0, v0

    move/from16 v53, v0

    move-object/from16 v0, v20

    move/from16 v1, v52

    move/from16 v2, v53

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 421
    move-object/from16 v0, v19

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v52, v0

    move/from16 v0, v52

    int-to-float v0, v0

    move/from16 v52, v0

    move-object/from16 v0, v19

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    move/from16 v53, v0

    move/from16 v0, v53

    int-to-float v0, v0

    move/from16 v53, v0

    move-object/from16 v0, v20

    move/from16 v1, v52

    move/from16 v2, v53

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 422
    move-object/from16 v0, v19

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v52, v0

    move/from16 v0, v52

    int-to-float v0, v0

    move/from16 v52, v0

    move-object/from16 v0, v19

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    move/from16 v53, v0

    move/from16 v0, v53

    int-to-float v0, v0

    move/from16 v53, v0

    move-object/from16 v0, v20

    move/from16 v1, v52

    move/from16 v2, v53

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 423
    invoke-virtual/range {v20 .. v20}, Landroid/graphics/Path;->close()V

    .line 425
    if-eqz v20, :cond_23

    .line 426
    new-instance v17, Landroid/graphics/Path;

    move-object/from16 v0, v17

    move-object/from16 v1, v20

    invoke-direct {v0, v1}, Landroid/graphics/Path;-><init>(Landroid/graphics/Path;)V

    .end local v18    # "clipArea":Landroid/graphics/Path;
    .end local v20    # "clip_new":Landroid/graphics/Path;
    .restart local v17    # "clipArea":Landroid/graphics/Path;
    :goto_2
    move-object/from16 v34, v35

    .line 429
    .end local v35    # "myPaint":Landroid/graphics/Paint;
    .restart local v34    # "myPaint":Landroid/graphics/Paint;
    goto/16 :goto_1

    .line 432
    .end local v10    # "af":Landroid/graphics/Matrix;
    .end local v17    # "clipArea":Landroid/graphics/Path;
    .end local v34    # "myPaint":Landroid/graphics/Paint;
    .restart local v18    # "clipArea":Landroid/graphics/Path;
    .restart local v35    # "myPaint":Landroid/graphics/Paint;
    .restart local v36    # "nextShape":Ljava/lang/Object;
    :cond_a
    move-object/from16 v0, v36

    instance-of v0, v0, Landroid/graphics/Paint;

    move/from16 v52, v0

    if-eqz v52, :cond_b

    .line 433
    move-object/from16 v0, v36

    check-cast v0, Landroid/graphics/Paint;

    move-object/from16 v37, v0

    .line 434
    .local v37, "p":Landroid/graphics/Paint;
    new-instance v34, Landroid/graphics/Paint;

    move-object/from16 v0, v34

    move-object/from16 v1, v37

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(Landroid/graphics/Paint;)V
    :try_end_1
    .catch Ljava/util/NoSuchElementException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 435
    .end local v35    # "myPaint":Landroid/graphics/Paint;
    .restart local v34    # "myPaint":Landroid/graphics/Paint;
    :try_start_2
    invoke-virtual/range {v37 .. v37}, Landroid/graphics/Paint;->getStyle()Landroid/graphics/Paint$Style;

    move-result-object v52

    move-object/from16 v0, v34

    move-object/from16 v1, v52

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 436
    const/16 v52, 0x1

    move-object/from16 v0, v34

    move/from16 v1, v52

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V
    :try_end_2
    .catch Ljava/util/NoSuchElementException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-object/from16 v17, v18

    .line 437
    .end local v18    # "clipArea":Landroid/graphics/Path;
    .restart local v17    # "clipArea":Landroid/graphics/Path;
    goto/16 :goto_1

    .end local v17    # "clipArea":Landroid/graphics/Path;
    .end local v34    # "myPaint":Landroid/graphics/Paint;
    .end local v37    # "p":Landroid/graphics/Paint;
    .restart local v18    # "clipArea":Landroid/graphics/Path;
    .restart local v35    # "myPaint":Landroid/graphics/Paint;
    :cond_b
    :try_start_3
    move-object/from16 v0, v36

    instance-of v0, v0, Lorg/icepdf/core/pobjects/graphics/Shapes$Clip;

    move/from16 v52, v0

    if-eqz v52, :cond_d

    .line 440
    new-instance v10, Landroid/graphics/Matrix;

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->getMatrix()Landroid/graphics/Matrix;

    move-result-object v52

    move-object/from16 v0, v52

    invoke-direct {v10, v0}, Landroid/graphics/Matrix;-><init>(Landroid/graphics/Matrix;)V

    .line 443
    .restart local v10    # "af":Landroid/graphics/Matrix;
    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Landroid/graphics/Canvas;->setMatrix(Landroid/graphics/Matrix;)V

    .line 445
    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/graphics/Canvas;->clipPath(Landroid/graphics/Path;)Z

    .line 447
    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/graphics/Canvas;->setMatrix(Landroid/graphics/Matrix;)V

    .line 448
    if-eqz v39, :cond_c

    if-nez v26, :cond_c

    .line 458
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    .line 461
    move-object/from16 v0, p1

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/graphics/Canvas;->clipPath(Landroid/graphics/Path;)Z

    move-result v52

    if-nez v52, :cond_c

    .line 462
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->restore()V

    .line 468
    :cond_c
    if-eqz v39, :cond_22

    .line 469
    new-instance v17, Landroid/graphics/Path;

    move-object/from16 v0, v17

    move-object/from16 v1, v39

    invoke-direct {v0, v1}, Landroid/graphics/Path;-><init>(Landroid/graphics/Path;)V

    .end local v18    # "clipArea":Landroid/graphics/Path;
    .restart local v17    # "clipArea":Landroid/graphics/Path;
    :goto_3
    move-object/from16 v34, v35

    .line 471
    .end local v35    # "myPaint":Landroid/graphics/Paint;
    .restart local v34    # "myPaint":Landroid/graphics/Paint;
    goto/16 :goto_1

    .end local v10    # "af":Landroid/graphics/Matrix;
    .end local v17    # "clipArea":Landroid/graphics/Path;
    .end local v34    # "myPaint":Landroid/graphics/Paint;
    .restart local v18    # "clipArea":Landroid/graphics/Path;
    .restart local v35    # "myPaint":Landroid/graphics/Paint;
    :cond_d
    move-object/from16 v0, v36

    instance-of v0, v0, Lorg/icepdf/core/pobjects/graphics/Shapes$Draw;

    move/from16 v52, v0

    if-eqz v52, :cond_12

    .line 473
    new-instance v23, Landroid/graphics/RectF;

    invoke-direct/range {v23 .. v23}, Landroid/graphics/RectF;-><init>()V

    .line 474
    .restart local v23    # "cliprect":Landroid/graphics/RectF;
    new-instance v40, Landroid/graphics/RectF;

    invoke-direct/range {v40 .. v40}, Landroid/graphics/RectF;-><init>()V

    .line 475
    .restart local v40    # "shapeRect":Landroid/graphics/RectF;
    const/16 v52, 0x1

    move-object/from16 v0, v18

    move-object/from16 v1, v23

    move/from16 v2, v52

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->computeBounds(Landroid/graphics/RectF;Z)V

    .line 477
    if-eqz v39, :cond_e

    .line 478
    const/16 v52, 0x1

    move-object/from16 v0, v39

    move-object/from16 v1, v40

    move/from16 v2, v52

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->computeBounds(Landroid/graphics/RectF;Z)V

    .line 480
    :cond_e
    sget-object v52, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    move-object/from16 v0, v35

    move-object/from16 v1, v52

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 481
    move-object/from16 v0, v23

    move-object/from16 v1, v40

    invoke-static {v0, v1}, Lcom/samsung/thumbnail/util/Utils;->isIntersecting(Landroid/graphics/RectF;Landroid/graphics/RectF;)Z

    move-result v52

    if-nez v52, :cond_f

    invoke-virtual/range {v40 .. v40}, Landroid/graphics/RectF;->width()F

    move-result v52

    move/from16 v0, v52

    float-to-double v0, v0

    move-wide/from16 v52, v0

    const-wide/high16 v54, 0x3ff0000000000000L    # 1.0

    cmpg-double v52, v52, v54

    if-ltz v52, :cond_f

    invoke-virtual/range {v40 .. v40}, Landroid/graphics/RectF;->height()F

    move-result v52

    move/from16 v0, v52

    float-to-double v0, v0

    move-wide/from16 v52, v0

    const-wide/high16 v54, 0x3ff0000000000000L    # 1.0

    cmpg-double v52, v52, v54

    if-gez v52, :cond_11

    .line 485
    :cond_f
    if-eqz v39, :cond_10

    .line 486
    move-object/from16 v0, p1

    move-object/from16 v1, v39

    move-object/from16 v2, v35

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 489
    :cond_10
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v24

    .line 490
    .restart local v24    # "currentTime":J
    sub-long v52, v24, v32

    sget v54, Lorg/icepdf/core/pobjects/graphics/Shapes;->paintDelay:I

    move/from16 v0, v54

    int-to-long v0, v0

    move-wide/from16 v54, v0

    cmp-long v52, v52, v54

    if-lez v52, :cond_11

    .line 492
    move-wide/from16 v32, v24

    .line 494
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/core/pobjects/graphics/Shapes;->parentPage:Lorg/icepdf/core/pobjects/Page;

    move-object/from16 v52, v0

    if-eqz v52, :cond_11

    .line 495
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/core/pobjects/graphics/Shapes;->parentPage:Lorg/icepdf/core/pobjects/Page;

    move-object/from16 v52, v0

    invoke-virtual/range {v52 .. v52}, Lorg/icepdf/core/pobjects/Page;->notifyPaintPageListeners()V

    .end local v24    # "currentTime":J
    :cond_11
    move-object/from16 v17, v18

    .end local v18    # "clipArea":Landroid/graphics/Path;
    .restart local v17    # "clipArea":Landroid/graphics/Path;
    move-object/from16 v34, v35

    .line 498
    .end local v35    # "myPaint":Landroid/graphics/Paint;
    .restart local v34    # "myPaint":Landroid/graphics/Paint;
    goto/16 :goto_1

    .end local v17    # "clipArea":Landroid/graphics/Path;
    .end local v23    # "cliprect":Landroid/graphics/RectF;
    .end local v34    # "myPaint":Landroid/graphics/Paint;
    .end local v40    # "shapeRect":Landroid/graphics/RectF;
    .restart local v18    # "clipArea":Landroid/graphics/Path;
    .restart local v35    # "myPaint":Landroid/graphics/Paint;
    :cond_12
    move-object/from16 v0, v36

    instance-of v0, v0, Lorg/icepdf/core/pobjects/graphics/Shapes$NoClip;

    move/from16 v52, v0

    if-eqz v52, :cond_13

    .line 499
    new-instance v10, Landroid/graphics/Matrix;

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->getMatrix()Landroid/graphics/Matrix;

    move-result-object v52

    move-object/from16 v0, v52

    invoke-direct {v10, v0}, Landroid/graphics/Matrix;-><init>(Landroid/graphics/Matrix;)V

    .line 500
    .restart local v10    # "af":Landroid/graphics/Matrix;
    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Landroid/graphics/Canvas;->setMatrix(Landroid/graphics/Matrix;)V

    .line 501
    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/graphics/Canvas;->clipPath(Landroid/graphics/Path;)Z

    .line 502
    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/graphics/Canvas;->setMatrix(Landroid/graphics/Matrix;)V

    .line 504
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->getClipBounds()Landroid/graphics/Rect;

    move-result-object v21

    .line 505
    .local v21, "clipbounds":Landroid/graphics/Rect;
    new-instance v43, Landroid/graphics/Path;

    invoke-direct/range {v43 .. v43}, Landroid/graphics/Path;-><init>()V

    .line 506
    .local v43, "tempclipPath":Landroid/graphics/Path;
    move-object/from16 v0, v21

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v52, v0

    move/from16 v0, v52

    int-to-float v0, v0

    move/from16 v52, v0

    move-object/from16 v0, v21

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v53, v0

    move/from16 v0, v53

    int-to-float v0, v0

    move/from16 v53, v0

    move-object/from16 v0, v43

    move/from16 v1, v52

    move/from16 v2, v53

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 507
    move-object/from16 v0, v21

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v52, v0

    move/from16 v0, v52

    int-to-float v0, v0

    move/from16 v52, v0

    move-object/from16 v0, v21

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v53, v0

    move/from16 v0, v53

    int-to-float v0, v0

    move/from16 v53, v0

    move-object/from16 v0, v43

    move/from16 v1, v52

    move/from16 v2, v53

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 508
    move-object/from16 v0, v21

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v52, v0

    move/from16 v0, v52

    int-to-float v0, v0

    move/from16 v52, v0

    move-object/from16 v0, v21

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    move/from16 v53, v0

    move/from16 v0, v53

    int-to-float v0, v0

    move/from16 v53, v0

    move-object/from16 v0, v43

    move/from16 v1, v52

    move/from16 v2, v53

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 509
    move-object/from16 v0, v21

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v52, v0

    move/from16 v0, v52

    int-to-float v0, v0

    move/from16 v52, v0

    move-object/from16 v0, v21

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    move/from16 v53, v0

    move/from16 v0, v53

    int-to-float v0, v0

    move/from16 v53, v0

    move-object/from16 v0, v43

    move/from16 v1, v52

    move/from16 v2, v53

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 510
    invoke-virtual/range {v43 .. v43}, Landroid/graphics/Path;->close()V

    .line 512
    if-eqz v43, :cond_21

    .line 513
    new-instance v17, Landroid/graphics/Path;

    move-object/from16 v0, v17

    move-object/from16 v1, v43

    invoke-direct {v0, v1}, Landroid/graphics/Path;-><init>(Landroid/graphics/Path;)V

    .end local v18    # "clipArea":Landroid/graphics/Path;
    .restart local v17    # "clipArea":Landroid/graphics/Path;
    :goto_4
    move-object/from16 v34, v35

    .line 515
    .end local v35    # "myPaint":Landroid/graphics/Paint;
    .restart local v34    # "myPaint":Landroid/graphics/Paint;
    goto/16 :goto_1

    .line 518
    .end local v10    # "af":Landroid/graphics/Matrix;
    .end local v17    # "clipArea":Landroid/graphics/Path;
    .end local v21    # "clipbounds":Landroid/graphics/Rect;
    .end local v34    # "myPaint":Landroid/graphics/Paint;
    .end local v43    # "tempclipPath":Landroid/graphics/Path;
    .restart local v18    # "clipArea":Landroid/graphics/Path;
    .restart local v35    # "myPaint":Landroid/graphics/Paint;
    :cond_13
    move-object/from16 v0, v36

    instance-of v0, v0, Landroid/graphics/Bitmap;

    move/from16 v52, v0

    if-eqz v52, :cond_14

    .line 519
    move-object/from16 v0, v36

    check-cast v0, Landroid/graphics/Bitmap;

    move-object/from16 v46, v0

    .line 520
    .local v46, "tmpImage":Landroid/graphics/Bitmap;
    new-instance v27, Landroid/graphics/RectF;

    invoke-direct/range {v27 .. v27}, Landroid/graphics/RectF;-><init>()V

    .line 521
    .local v27, "dst":Landroid/graphics/RectF;
    const/16 v52, 0x0

    move/from16 v0, v52

    move-object/from16 v1, v27

    iput v0, v1, Landroid/graphics/RectF;->left:F

    .line 522
    const/16 v52, 0x0

    move/from16 v0, v52

    move-object/from16 v1, v27

    iput v0, v1, Landroid/graphics/RectF;->top:F

    .line 523
    const/high16 v52, 0x3f800000    # 1.0f

    move/from16 v0, v52

    move-object/from16 v1, v27

    iput v0, v1, Landroid/graphics/RectF;->bottom:F

    .line 524
    const/high16 v52, 0x3f800000    # 1.0f

    move/from16 v0, v52

    move-object/from16 v1, v27

    iput v0, v1, Landroid/graphics/RectF;->right:F

    .line 526
    const/16 v52, 0x0

    const/16 v53, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v46

    move-object/from16 v2, v52

    move-object/from16 v3, v27

    move-object/from16 v4, v53

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    move-object/from16 v17, v18

    .end local v18    # "clipArea":Landroid/graphics/Path;
    .restart local v17    # "clipArea":Landroid/graphics/Path;
    move-object/from16 v34, v35

    .line 527
    .end local v35    # "myPaint":Landroid/graphics/Paint;
    .restart local v34    # "myPaint":Landroid/graphics/Paint;
    goto/16 :goto_1

    .line 529
    .end local v17    # "clipArea":Landroid/graphics/Path;
    .end local v27    # "dst":Landroid/graphics/RectF;
    .end local v34    # "myPaint":Landroid/graphics/Paint;
    .end local v46    # "tmpImage":Landroid/graphics/Bitmap;
    .restart local v18    # "clipArea":Landroid/graphics/Path;
    .restart local v35    # "myPaint":Landroid/graphics/Paint;
    :cond_14
    move-object/from16 v0, v36

    instance-of v0, v0, Lorg/icepdf/core/pobjects/graphics/Shapes;

    move/from16 v52, v0

    if-eqz v52, :cond_15

    .line 530
    move-object/from16 v0, v36

    check-cast v0, Lorg/icepdf/core/pobjects/graphics/Shapes;

    move-object/from16 v52, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/core/pobjects/graphics/Shapes;->parentPage:Lorg/icepdf/core/pobjects/Page;

    move-object/from16 v53, v0

    invoke-virtual/range {v52 .. v53}, Lorg/icepdf/core/pobjects/graphics/Shapes;->setPageParent(Lorg/icepdf/core/pobjects/Page;)V

    .line 531
    move-object/from16 v0, v36

    check-cast v0, Lorg/icepdf/core/pobjects/graphics/Shapes;

    move-object/from16 v52, v0

    move-object/from16 v0, v52

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Lorg/icepdf/core/pobjects/graphics/Shapes;->paint(Landroid/graphics/Canvas;)V

    .line 532
    check-cast v36, Lorg/icepdf/core/pobjects/graphics/Shapes;

    .end local v36    # "nextShape":Ljava/lang/Object;
    const/16 v52, 0x0

    move-object/from16 v0, v36

    move-object/from16 v1, v52

    invoke-virtual {v0, v1}, Lorg/icepdf/core/pobjects/graphics/Shapes;->setPageParent(Lorg/icepdf/core/pobjects/Page;)V

    move-object/from16 v17, v18

    .end local v18    # "clipArea":Landroid/graphics/Path;
    .restart local v17    # "clipArea":Landroid/graphics/Path;
    move-object/from16 v34, v35

    .end local v35    # "myPaint":Landroid/graphics/Paint;
    .restart local v34    # "myPaint":Landroid/graphics/Paint;
    goto/16 :goto_1

    .line 538
    .end local v17    # "clipArea":Landroid/graphics/Path;
    .end local v34    # "myPaint":Landroid/graphics/Paint;
    .restart local v18    # "clipArea":Landroid/graphics/Path;
    .restart local v35    # "myPaint":Landroid/graphics/Paint;
    .restart local v36    # "nextShape":Ljava/lang/Object;
    :cond_15
    move-object/from16 v0, v36

    instance-of v0, v0, Lorg/icepdf/core/pobjects/Form;

    move/from16 v52, v0

    if-eqz v52, :cond_19

    .line 542
    move-object/from16 v0, v36

    check-cast v0, Lorg/icepdf/core/pobjects/Form;

    move-object/from16 v50, v0

    .line 544
    .local v50, "xForm":Lorg/icepdf/core/pobjects/Form;
    invoke-virtual/range {v50 .. v50}, Lorg/icepdf/core/pobjects/Form;->getBBox()Landroid/graphics/RectF;

    move-result-object v12

    .line 548
    .local v12, "bBox":Landroid/graphics/RectF;
    iget v0, v12, Landroid/graphics/RectF;->right:F

    move/from16 v52, v0

    iget v0, v12, Landroid/graphics/RectF;->left:F

    move/from16 v53, v0

    sub-float v52, v52, v53

    move/from16 v0, v52

    iput v0, v12, Landroid/graphics/RectF;->right:F

    .line 549
    iget v0, v12, Landroid/graphics/RectF;->bottom:F

    move/from16 v52, v0

    iget v0, v12, Landroid/graphics/RectF;->top:F

    move/from16 v53, v0

    sub-float v52, v52, v53

    move/from16 v0, v52

    iput v0, v12, Landroid/graphics/RectF;->bottom:F

    .line 550
    iget v0, v12, Landroid/graphics/RectF;->top:F

    move/from16 v52, v0

    iget v0, v12, Landroid/graphics/RectF;->bottom:F

    move/from16 v53, v0

    sub-float v52, v52, v53

    move/from16 v0, v52

    iput v0, v12, Landroid/graphics/RectF;->top:F

    .line 551
    invoke-virtual {v12}, Landroid/graphics/RectF;->width()F

    move-result v52

    move/from16 v0, v52

    float-to-int v0, v0

    move/from16 v49, v0

    .line 552
    .local v49, "width":I
    invoke-virtual {v12}, Landroid/graphics/RectF;->height()F

    move-result v52

    move/from16 v0, v52

    float-to-int v0, v0

    move/from16 v30, v0

    .line 554
    .local v30, "height":I
    if-nez v49, :cond_16

    .line 555
    const/16 v49, 0x1

    .line 557
    :cond_16
    if-nez v30, :cond_17

    .line 558
    const/16 v30, 0x1

    .line 561
    :cond_17
    sget-object v52, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    move/from16 v0, v49

    move/from16 v1, v30

    move-object/from16 v2, v52

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v14

    .line 564
    .local v14, "bi":Landroid/graphics/Bitmap;
    new-instance v15, Landroid/graphics/Canvas;

    invoke-direct {v15, v14}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 569
    .local v15, "canvas":Landroid/graphics/Canvas;
    invoke-virtual/range {v50 .. v50}, Lorg/icepdf/core/pobjects/Form;->getShapes()Lorg/icepdf/core/pobjects/graphics/Shapes;

    move-result-object v51

    .line 570
    .local v51, "xFormShapes":Lorg/icepdf/core/pobjects/graphics/Shapes;
    if-eqz v51, :cond_18

    .line 571
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/core/pobjects/graphics/Shapes;->parentPage:Lorg/icepdf/core/pobjects/Page;

    move-object/from16 v52, v0

    invoke-virtual/range {v51 .. v52}, Lorg/icepdf/core/pobjects/graphics/Shapes;->setPageParent(Lorg/icepdf/core/pobjects/Page;)V

    .line 574
    iget v0, v12, Landroid/graphics/RectF;->left:F

    move/from16 v52, v0

    move/from16 v0, v52

    float-to-int v0, v0

    move/from16 v52, v0

    move/from16 v0, v52

    neg-int v0, v0

    move/from16 v52, v0

    move/from16 v0, v52

    int-to-float v0, v0

    move/from16 v52, v0

    iget v0, v12, Landroid/graphics/RectF;->top:F

    move/from16 v53, v0

    move/from16 v0, v53

    float-to-int v0, v0

    move/from16 v53, v0

    move/from16 v0, v53

    neg-int v0, v0

    move/from16 v53, v0

    move/from16 v0, v53

    int-to-float v0, v0

    move/from16 v53, v0

    move/from16 v0, v52

    move/from16 v1, v53

    invoke-virtual {v15, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 576
    invoke-virtual {v15, v12}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/RectF;)Z

    .line 578
    move-object/from16 v0, v51

    invoke-virtual {v0, v15}, Lorg/icepdf/core/pobjects/graphics/Shapes;->paint(Landroid/graphics/Canvas;)V

    .line 579
    const/16 v52, 0x0

    invoke-virtual/range {v51 .. v52}, Lorg/icepdf/core/pobjects/graphics/Shapes;->setPageParent(Lorg/icepdf/core/pobjects/Page;)V

    .line 582
    :cond_18
    iget v0, v12, Landroid/graphics/RectF;->left:F

    move/from16 v52, v0

    iget v0, v12, Landroid/graphics/RectF;->top:F

    move/from16 v53, v0

    const/16 v54, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v52

    move/from16 v2, v53

    move-object/from16 v3, v54

    invoke-virtual {v0, v14, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    move-object/from16 v17, v18

    .end local v18    # "clipArea":Landroid/graphics/Path;
    .restart local v17    # "clipArea":Landroid/graphics/Path;
    move-object/from16 v34, v35

    .line 584
    .end local v35    # "myPaint":Landroid/graphics/Paint;
    .restart local v34    # "myPaint":Landroid/graphics/Paint;
    goto/16 :goto_1

    .line 593
    .end local v12    # "bBox":Landroid/graphics/RectF;
    .end local v14    # "bi":Landroid/graphics/Bitmap;
    .end local v15    # "canvas":Landroid/graphics/Canvas;
    .end local v17    # "clipArea":Landroid/graphics/Path;
    .end local v30    # "height":I
    .end local v34    # "myPaint":Landroid/graphics/Paint;
    .end local v49    # "width":I
    .end local v50    # "xForm":Lorg/icepdf/core/pobjects/Form;
    .end local v51    # "xFormShapes":Lorg/icepdf/core/pobjects/graphics/Shapes;
    .restart local v18    # "clipArea":Landroid/graphics/Path;
    .restart local v35    # "myPaint":Landroid/graphics/Paint;
    :cond_19
    move-object/from16 v0, v36

    instance-of v0, v0, Lorg/apache/poi/java/awt/Color;

    move/from16 v52, v0

    if-eqz v52, :cond_1a

    .line 595
    move-object/from16 v0, v36

    check-cast v0, Lorg/apache/poi/java/awt/Color;

    move-object/from16 v47, v0

    .line 596
    .local v47, "toPaint":Lorg/apache/poi/java/awt/Color;
    invoke-virtual/range {v47 .. v47}, Lorg/apache/poi/java/awt/Color;->getFAlpha()F

    move-result v52

    const/high16 v53, 0x437f0000    # 255.0f

    mul-float v52, v52, v53

    move/from16 v0, v52

    float-to-double v0, v0

    move-wide/from16 v52, v0

    const-wide/high16 v54, 0x3fe0000000000000L    # 0.5

    add-double v52, v52, v54

    move-wide/from16 v0, v52

    double-to-int v6, v0

    .line 597
    .local v6, "A":I
    invoke-virtual/range {v47 .. v47}, Lorg/apache/poi/java/awt/Color;->getRed()I

    move-result v9

    .line 598
    .local v9, "R":I
    invoke-virtual/range {v47 .. v47}, Lorg/apache/poi/java/awt/Color;->getGreen()I

    move-result v8

    .line 599
    .local v8, "G":I
    invoke-virtual/range {v47 .. v47}, Lorg/apache/poi/java/awt/Color;->getBlue()I

    move-result v7

    .line 600
    .local v7, "B":I
    invoke-static {v6, v9, v8, v7}, Landroid/graphics/Color;->argb(IIII)I

    move-result v52

    move-object/from16 v0, v35

    move/from16 v1, v52

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    move-object/from16 v17, v18

    .end local v18    # "clipArea":Landroid/graphics/Path;
    .restart local v17    # "clipArea":Landroid/graphics/Path;
    move-object/from16 v34, v35

    .line 601
    .end local v35    # "myPaint":Landroid/graphics/Paint;
    .restart local v34    # "myPaint":Landroid/graphics/Paint;
    goto/16 :goto_1

    .line 603
    .end local v6    # "A":I
    .end local v7    # "B":I
    .end local v8    # "G":I
    .end local v9    # "R":I
    .end local v17    # "clipArea":Landroid/graphics/Path;
    .end local v34    # "myPaint":Landroid/graphics/Paint;
    .end local v47    # "toPaint":Lorg/apache/poi/java/awt/Color;
    .restart local v18    # "clipArea":Landroid/graphics/Path;
    .restart local v35    # "myPaint":Landroid/graphics/Paint;
    :cond_1a
    move-object/from16 v0, v36

    instance-of v0, v0, Lorg/icepdf/core/pobjects/graphics/TilingPattern;

    move/from16 v52, v0

    if-eqz v52, :cond_1b

    .line 604
    move-object/from16 v0, v36

    check-cast v0, Lorg/icepdf/core/pobjects/graphics/TilingPattern;

    move-object/from16 v45, v0

    .line 605
    .local v45, "tilingPattern":Lorg/icepdf/core/pobjects/graphics/TilingPattern;
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/core/pobjects/graphics/Shapes;->parentPage:Lorg/icepdf/core/pobjects/Page;

    move-object/from16 v52, v0

    move-object/from16 v0, v45

    move-object/from16 v1, p1

    move-object/from16 v2, v52

    invoke-virtual {v0, v1, v2}, Lorg/icepdf/core/pobjects/graphics/TilingPattern;->paintPattern(Landroid/graphics/Canvas;Lorg/icepdf/core/pobjects/Page;)V

    move-object/from16 v17, v18

    .end local v18    # "clipArea":Landroid/graphics/Path;
    .restart local v17    # "clipArea":Landroid/graphics/Path;
    move-object/from16 v34, v35

    .line 606
    .end local v35    # "myPaint":Landroid/graphics/Paint;
    .restart local v34    # "myPaint":Landroid/graphics/Paint;
    goto/16 :goto_1

    .line 607
    .end local v17    # "clipArea":Landroid/graphics/Path;
    .end local v34    # "myPaint":Landroid/graphics/Paint;
    .end local v45    # "tilingPattern":Lorg/icepdf/core/pobjects/graphics/TilingPattern;
    .restart local v18    # "clipArea":Landroid/graphics/Path;
    .restart local v35    # "myPaint":Landroid/graphics/Paint;
    :cond_1b
    move-object/from16 v0, v36

    instance-of v0, v0, Lorg/icepdf/core/pobjects/graphics/GlyphOutlineClip;

    move/from16 v52, v0

    if-eqz v52, :cond_1c

    .line 610
    new-instance v38, Landroid/graphics/Matrix;

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->getMatrix()Landroid/graphics/Matrix;

    move-result-object v52

    move-object/from16 v0, v38

    move-object/from16 v1, v52

    invoke-direct {v0, v1}, Landroid/graphics/Matrix;-><init>(Landroid/graphics/Matrix;)V

    .line 611
    .local v38, "preTrans":Landroid/graphics/Matrix;
    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Landroid/graphics/Canvas;->setMatrix(Landroid/graphics/Matrix;)V

    .line 614
    check-cast v36, Lorg/icepdf/core/pobjects/graphics/GlyphOutlineClip;

    .end local v36    # "nextShape":Ljava/lang/Object;
    invoke-virtual/range {v36 .. v36}, Lorg/icepdf/core/pobjects/graphics/GlyphOutlineClip;->getGlyphOutlineClip()Landroid/graphics/Path;

    move-result-object v29

    .line 615
    .local v29, "glyphClip":Landroid/graphics/Path;
    move-object/from16 v0, p1

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Landroid/graphics/Canvas;->clipPath(Landroid/graphics/Path;)Z

    .line 616
    move-object/from16 v0, p1

    move-object/from16 v1, v38

    invoke-virtual {v0, v1}, Landroid/graphics/Canvas;->setMatrix(Landroid/graphics/Matrix;)V

    .line 619
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->getClipBounds()Landroid/graphics/Rect;

    move-result-object v22

    .line 620
    .local v22, "clipbounds1":Landroid/graphics/Rect;
    new-instance v44, Landroid/graphics/Path;

    invoke-direct/range {v44 .. v44}, Landroid/graphics/Path;-><init>()V

    .line 621
    .local v44, "tempclipPath1":Landroid/graphics/Path;
    move-object/from16 v0, v22

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v52, v0

    move/from16 v0, v52

    int-to-float v0, v0

    move/from16 v52, v0

    move-object/from16 v0, v22

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v53, v0

    move/from16 v0, v53

    int-to-float v0, v0

    move/from16 v53, v0

    move-object/from16 v0, v44

    move/from16 v1, v52

    move/from16 v2, v53

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 622
    move-object/from16 v0, v22

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v52, v0

    move/from16 v0, v52

    int-to-float v0, v0

    move/from16 v52, v0

    move-object/from16 v0, v22

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v53, v0

    move/from16 v0, v53

    int-to-float v0, v0

    move/from16 v53, v0

    move-object/from16 v0, v44

    move/from16 v1, v52

    move/from16 v2, v53

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 623
    move-object/from16 v0, v22

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v52, v0

    move/from16 v0, v52

    int-to-float v0, v0

    move/from16 v52, v0

    move-object/from16 v0, v22

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    move/from16 v53, v0

    move/from16 v0, v53

    int-to-float v0, v0

    move/from16 v53, v0

    move-object/from16 v0, v44

    move/from16 v1, v52

    move/from16 v2, v53

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 624
    move-object/from16 v0, v22

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v52, v0

    move/from16 v0, v52

    int-to-float v0, v0

    move/from16 v52, v0

    move-object/from16 v0, v22

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    move/from16 v53, v0

    move/from16 v0, v53

    int-to-float v0, v0

    move/from16 v53, v0

    move-object/from16 v0, v44

    move/from16 v1, v52

    move/from16 v2, v53

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 625
    invoke-virtual/range {v44 .. v44}, Landroid/graphics/Path;->close()V

    .line 627
    if-eqz v44, :cond_20

    .line 628
    new-instance v17, Landroid/graphics/Path;

    move-object/from16 v0, v17

    move-object/from16 v1, v44

    invoke-direct {v0, v1}, Landroid/graphics/Path;-><init>(Landroid/graphics/Path;)V

    .end local v18    # "clipArea":Landroid/graphics/Path;
    .restart local v17    # "clipArea":Landroid/graphics/Path;
    :goto_5
    move-object/from16 v34, v35

    .line 630
    .end local v35    # "myPaint":Landroid/graphics/Paint;
    .restart local v34    # "myPaint":Landroid/graphics/Paint;
    goto/16 :goto_1

    .end local v17    # "clipArea":Landroid/graphics/Path;
    .end local v22    # "clipbounds1":Landroid/graphics/Rect;
    .end local v29    # "glyphClip":Landroid/graphics/Path;
    .end local v34    # "myPaint":Landroid/graphics/Paint;
    .end local v38    # "preTrans":Landroid/graphics/Matrix;
    .end local v44    # "tempclipPath1":Landroid/graphics/Path;
    .restart local v18    # "clipArea":Landroid/graphics/Path;
    .restart local v35    # "myPaint":Landroid/graphics/Paint;
    .restart local v36    # "nextShape":Ljava/lang/Object;
    :cond_1c
    move-object/from16 v0, v36

    instance-of v0, v0, Ljava/util/Vector;

    move/from16 v52, v0

    if-eqz v52, :cond_1e

    .line 631
    new-instance v48, Lorg/icepdf/core/pobjects/graphics/Shapes;

    invoke-direct/range {v48 .. v48}, Lorg/icepdf/core/pobjects/graphics/Shapes;-><init>()V

    .line 632
    .local v48, "tshape":Lorg/icepdf/core/pobjects/graphics/Shapes;
    move-object/from16 v0, v36

    check-cast v0, Ljava/util/Vector;

    move-object v11, v0

    .line 633
    .local v11, "arr":Ljava/util/Vector;, "Ljava/util/Vector<Ljava/lang/Object;>;"
    const/16 v31, 0x0

    .local v31, "i":I
    :goto_6
    invoke-virtual {v11}, Ljava/util/Vector;->size()I

    move-result v52

    move/from16 v0, v31

    move/from16 v1, v52

    if-ge v0, v1, :cond_1d

    .line 634
    move/from16 v0, v31

    invoke-virtual {v11, v0}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v52

    move-object/from16 v0, v48

    move-object/from16 v1, v52

    invoke-virtual {v0, v1}, Lorg/icepdf/core/pobjects/graphics/Shapes;->add(Ljava/lang/Object;)V

    .line 633
    add-int/lit8 v31, v31, 0x1

    goto :goto_6

    .line 636
    :cond_1d
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/core/pobjects/graphics/Shapes;->parentPage:Lorg/icepdf/core/pobjects/Page;

    move-object/from16 v52, v0

    move-object/from16 v0, v48

    move-object/from16 v1, v52

    invoke-virtual {v0, v1}, Lorg/icepdf/core/pobjects/graphics/Shapes;->setPageParent(Lorg/icepdf/core/pobjects/Page;)V

    .line 637
    move-object/from16 v0, v48

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Lorg/icepdf/core/pobjects/graphics/Shapes;->paint(Landroid/graphics/Canvas;)V

    .line 638
    const/16 v52, 0x0

    move-object/from16 v0, v48

    move-object/from16 v1, v52

    invoke-virtual {v0, v1}, Lorg/icepdf/core/pobjects/graphics/Shapes;->setPageParent(Lorg/icepdf/core/pobjects/Page;)V
    :try_end_3
    .catch Ljava/util/NoSuchElementException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .end local v11    # "arr":Ljava/util/Vector;, "Ljava/util/Vector<Ljava/lang/Object;>;"
    .end local v31    # "i":I
    .end local v36    # "nextShape":Ljava/lang/Object;
    .end local v48    # "tshape":Lorg/icepdf/core/pobjects/graphics/Shapes;
    :cond_1e
    move-object/from16 v17, v18

    .end local v18    # "clipArea":Landroid/graphics/Path;
    .restart local v17    # "clipArea":Landroid/graphics/Path;
    move-object/from16 v34, v35

    .end local v35    # "myPaint":Landroid/graphics/Paint;
    .restart local v34    # "myPaint":Landroid/graphics/Paint;
    goto/16 :goto_1

    .line 645
    .end local v17    # "clipArea":Landroid/graphics/Path;
    .end local v34    # "myPaint":Landroid/graphics/Paint;
    .restart local v18    # "clipArea":Landroid/graphics/Path;
    .restart local v35    # "myPaint":Landroid/graphics/Paint;
    :catch_0
    move-exception v52

    move-object/from16 v34, v35

    .end local v35    # "myPaint":Landroid/graphics/Paint;
    .restart local v34    # "myPaint":Landroid/graphics/Paint;
    :goto_7
    move-object/from16 v35, v34

    .line 649
    .end local v34    # "myPaint":Landroid/graphics/Paint;
    .restart local v35    # "myPaint":Landroid/graphics/Paint;
    goto/16 :goto_0

    .line 647
    :catch_1
    move-exception v28

    move-object/from16 v34, v35

    .line 648
    .end local v35    # "myPaint":Landroid/graphics/Paint;
    .local v28, "e":Ljava/lang/Exception;
    .restart local v34    # "myPaint":Landroid/graphics/Paint;
    :goto_8
    :try_start_4
    sget-object v52, Lorg/icepdf/core/pobjects/graphics/Shapes;->logger:Ljava/util/logging/Logger;

    sget-object v53, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string/jumbo v54, "Error painting shapes."

    move-object/from16 v0, v52

    move-object/from16 v1, v53

    move-object/from16 v2, v54

    move-object/from16 v3, v28

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-object/from16 v35, v34

    .line 649
    .end local v34    # "myPaint":Landroid/graphics/Paint;
    .restart local v35    # "myPaint":Landroid/graphics/Paint;
    goto/16 :goto_0

    .line 659
    .end local v28    # "e":Ljava/lang/Exception;
    :cond_1f
    monitor-exit p0

    return-void

    .line 311
    .end local v13    # "base":Landroid/graphics/Matrix;
    .end local v16    # "clip":Landroid/graphics/Path;
    .end local v18    # "clipArea":Landroid/graphics/Path;
    .end local v19    # "clipRect":Landroid/graphics/Rect;
    .end local v26    # "disableClipping":Z
    .end local v32    # "lastPaintTime":J
    .end local v35    # "myPaint":Landroid/graphics/Paint;
    .end local v39    # "shape":Landroid/graphics/Path;
    .end local v41    # "shapesEnumeration":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Object;>;"
    :catchall_0
    move-exception v52

    monitor-exit p0

    throw v52

    .line 647
    .restart local v13    # "base":Landroid/graphics/Matrix;
    .restart local v16    # "clip":Landroid/graphics/Path;
    .restart local v18    # "clipArea":Landroid/graphics/Path;
    .restart local v19    # "clipRect":Landroid/graphics/Rect;
    .restart local v26    # "disableClipping":Z
    .restart local v32    # "lastPaintTime":J
    .restart local v34    # "myPaint":Landroid/graphics/Paint;
    .restart local v36    # "nextShape":Ljava/lang/Object;
    .restart local v37    # "p":Landroid/graphics/Paint;
    .restart local v39    # "shape":Landroid/graphics/Path;
    .restart local v41    # "shapesEnumeration":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Object;>;"
    :catch_2
    move-exception v28

    goto :goto_8

    .line 645
    :catch_3
    move-exception v52

    goto :goto_7

    .end local v34    # "myPaint":Landroid/graphics/Paint;
    .end local v36    # "nextShape":Ljava/lang/Object;
    .end local v37    # "p":Landroid/graphics/Paint;
    .restart local v22    # "clipbounds1":Landroid/graphics/Rect;
    .restart local v29    # "glyphClip":Landroid/graphics/Path;
    .restart local v35    # "myPaint":Landroid/graphics/Paint;
    .restart local v38    # "preTrans":Landroid/graphics/Matrix;
    .restart local v44    # "tempclipPath1":Landroid/graphics/Path;
    :cond_20
    move-object/from16 v17, v18

    .end local v18    # "clipArea":Landroid/graphics/Path;
    .restart local v17    # "clipArea":Landroid/graphics/Path;
    goto :goto_5

    .end local v17    # "clipArea":Landroid/graphics/Path;
    .end local v22    # "clipbounds1":Landroid/graphics/Rect;
    .end local v29    # "glyphClip":Landroid/graphics/Path;
    .end local v38    # "preTrans":Landroid/graphics/Matrix;
    .end local v44    # "tempclipPath1":Landroid/graphics/Path;
    .restart local v10    # "af":Landroid/graphics/Matrix;
    .restart local v18    # "clipArea":Landroid/graphics/Path;
    .restart local v21    # "clipbounds":Landroid/graphics/Rect;
    .restart local v36    # "nextShape":Ljava/lang/Object;
    .restart local v43    # "tempclipPath":Landroid/graphics/Path;
    :cond_21
    move-object/from16 v17, v18

    .end local v18    # "clipArea":Landroid/graphics/Path;
    .restart local v17    # "clipArea":Landroid/graphics/Path;
    goto/16 :goto_4

    .end local v17    # "clipArea":Landroid/graphics/Path;
    .end local v21    # "clipbounds":Landroid/graphics/Rect;
    .end local v43    # "tempclipPath":Landroid/graphics/Path;
    .restart local v18    # "clipArea":Landroid/graphics/Path;
    :cond_22
    move-object/from16 v17, v18

    .end local v18    # "clipArea":Landroid/graphics/Path;
    .restart local v17    # "clipArea":Landroid/graphics/Path;
    goto/16 :goto_3

    .end local v17    # "clipArea":Landroid/graphics/Path;
    .end local v36    # "nextShape":Ljava/lang/Object;
    .restart local v18    # "clipArea":Landroid/graphics/Path;
    :cond_23
    move-object/from16 v17, v18

    .end local v18    # "clipArea":Landroid/graphics/Path;
    .restart local v17    # "clipArea":Landroid/graphics/Path;
    goto/16 :goto_2
.end method

.method public setPageParent(Lorg/icepdf/core/pobjects/Page;)V
    .locals 0
    .param p1, "parent"    # Lorg/icepdf/core/pobjects/Page;

    .prologue
    .line 129
    iput-object p1, p0, Lorg/icepdf/core/pobjects/graphics/Shapes;->parentPage:Lorg/icepdf/core/pobjects/Page;

    .line 130
    return-void
.end method
