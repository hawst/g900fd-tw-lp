.class public Lorg/icepdf/core/pobjects/graphics/GlyphOutlineClip;
.super Ljava/lang/Object;
.source "GlyphOutlineClip.java"


# instance fields
.field private path:Landroid/graphics/Path;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public addTextSprite(Lorg/icepdf/core/pobjects/graphics/TextSprite;)V
    .locals 2
    .param p1, "nextSprite"    # Lorg/icepdf/core/pobjects/graphics/TextSprite;

    .prologue
    .line 34
    invoke-virtual {p1}, Lorg/icepdf/core/pobjects/graphics/TextSprite;->getGlyphOutline()Landroid/graphics/Path;

    move-result-object v0

    .line 42
    .local v0, "area":Landroid/graphics/Path;
    if-eqz v0, :cond_0

    .line 44
    invoke-virtual {p1}, Lorg/icepdf/core/pobjects/graphics/TextSprite;->getGraphicStateTransform()Landroid/graphics/Matrix;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/Path;->transform(Landroid/graphics/Matrix;)V

    .line 45
    iget-object v1, p0, Lorg/icepdf/core/pobjects/graphics/GlyphOutlineClip;->path:Landroid/graphics/Path;

    if-nez v1, :cond_1

    .line 46
    new-instance v1, Landroid/graphics/Path;

    invoke-direct {v1, v0}, Landroid/graphics/Path;-><init>(Landroid/graphics/Path;)V

    iput-object v1, p0, Lorg/icepdf/core/pobjects/graphics/GlyphOutlineClip;->path:Landroid/graphics/Path;

    .line 51
    :cond_0
    :goto_0
    return-void

    .line 48
    :cond_1
    iget-object v1, p0, Lorg/icepdf/core/pobjects/graphics/GlyphOutlineClip;->path:Landroid/graphics/Path;

    invoke-virtual {v1, v0}, Landroid/graphics/Path;->addPath(Landroid/graphics/Path;)V

    goto :goto_0
.end method

.method public getGlyphOutlineClip()Landroid/graphics/Path;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lorg/icepdf/core/pobjects/graphics/GlyphOutlineClip;->path:Landroid/graphics/Path;

    return-object v0
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lorg/icepdf/core/pobjects/graphics/GlyphOutlineClip;->path:Landroid/graphics/Path;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
