.class public Lorg/icepdf/core/pobjects/Dictionary;
.super Ljava/lang/Object;
.source "Dictionary.java"


# static fields
.field public static final SUBTYPE_KEY:Lorg/icepdf/core/pobjects/Name;

.field public static final TYPE_KEY:Lorg/icepdf/core/pobjects/Name;


# instance fields
.field protected entries:Ljava/util/Hashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field protected inited:Z

.field protected isDeleted:Z

.field protected isNew:Z

.field protected library:Lorg/icepdf/core/util/Library;

.field private pObjectReference:Lorg/icepdf/core/pobjects/Reference;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 38
    new-instance v0, Lorg/icepdf/core/pobjects/Name;

    const-string/jumbo v1, "Type"

    invoke-direct {v0, v1}, Lorg/icepdf/core/pobjects/Name;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/icepdf/core/pobjects/Dictionary;->TYPE_KEY:Lorg/icepdf/core/pobjects/Name;

    .line 40
    new-instance v0, Lorg/icepdf/core/pobjects/Name;

    const-string/jumbo v1, "Subtype"

    invoke-direct {v0, v1}, Lorg/icepdf/core/pobjects/Name;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/icepdf/core/pobjects/Dictionary;->SUBTYPE_KEY:Lorg/icepdf/core/pobjects/Name;

    return-void
.end method

.method public constructor <init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V
    .locals 1
    .param p1, "library"    # Lorg/icepdf/core/util/Library;
    .param p2, "entries"    # Ljava/util/Hashtable;

    .prologue
    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 78
    iput-object p1, p0, Lorg/icepdf/core/pobjects/Dictionary;->library:Lorg/icepdf/core/util/Library;

    .line 79
    iput-object p2, p0, Lorg/icepdf/core/pobjects/Dictionary;->entries:Ljava/util/Hashtable;

    .line 80
    iget-object v0, p0, Lorg/icepdf/core/pobjects/Dictionary;->entries:Ljava/util/Hashtable;

    if-nez v0, :cond_0

    .line 81
    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, Lorg/icepdf/core/pobjects/Dictionary;->entries:Ljava/util/Hashtable;

    .line 83
    :cond_0
    return-void
.end method


# virtual methods
.method public getEntries()Ljava/util/Hashtable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 125
    iget-object v0, p0, Lorg/icepdf/core/pobjects/Dictionary;->entries:Ljava/util/Hashtable;

    return-object v0
.end method

.method public getFloat(Ljava/lang/String;)F
    .locals 2
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 172
    iget-object v0, p0, Lorg/icepdf/core/pobjects/Dictionary;->library:Lorg/icepdf/core/util/Library;

    iget-object v1, p0, Lorg/icepdf/core/pobjects/Dictionary;->entries:Ljava/util/Hashtable;

    invoke-virtual {v0, v1, p1}, Lorg/icepdf/core/util/Library;->getFloat(Ljava/util/Hashtable;Ljava/lang/String;)F

    move-result v0

    return v0
.end method

.method public getInt(Ljava/lang/String;)I
    .locals 2
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 159
    iget-object v0, p0, Lorg/icepdf/core/pobjects/Dictionary;->library:Lorg/icepdf/core/util/Library;

    iget-object v1, p0, Lorg/icepdf/core/pobjects/Dictionary;->entries:Ljava/util/Hashtable;

    invoke-virtual {v0, v1, p1}, Lorg/icepdf/core/util/Library;->getInt(Ljava/util/Hashtable;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public getLibrary()Lorg/icepdf/core/util/Library;
    .locals 1

    .prologue
    .line 182
    iget-object v0, p0, Lorg/icepdf/core/pobjects/Dictionary;->library:Lorg/icepdf/core/util/Library;

    return-object v0
.end method

.method protected getNumber(Ljava/lang/String;)Ljava/lang/Number;
    .locals 2
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 146
    iget-object v0, p0, Lorg/icepdf/core/pobjects/Dictionary;->library:Lorg/icepdf/core/util/Library;

    iget-object v1, p0, Lorg/icepdf/core/pobjects/Dictionary;->entries:Ljava/util/Hashtable;

    invoke-virtual {v0, v1, p1}, Lorg/icepdf/core/util/Library;->getNumber(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Number;

    move-result-object v0

    return-object v0
.end method

.method public getObject(Ljava/lang/String;)Ljava/lang/Object;
    .locals 2
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 129
    iget-object v0, p0, Lorg/icepdf/core/pobjects/Dictionary;->library:Lorg/icepdf/core/util/Library;

    iget-object v1, p0, Lorg/icepdf/core/pobjects/Dictionary;->entries:Ljava/util/Hashtable;

    invoke-virtual {v0, v1, p1}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getObject(Lorg/icepdf/core/pobjects/Name;)Ljava/lang/Object;
    .locals 3
    .param p1, "key"    # Lorg/icepdf/core/pobjects/Name;

    .prologue
    .line 133
    iget-object v0, p0, Lorg/icepdf/core/pobjects/Dictionary;->library:Lorg/icepdf/core/util/Library;

    iget-object v1, p0, Lorg/icepdf/core/pobjects/Dictionary;->entries:Ljava/util/Hashtable;

    invoke-virtual {p1}, Lorg/icepdf/core/pobjects/Name;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getPObjectReference()Lorg/icepdf/core/pobjects/Reference;
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lorg/icepdf/core/pobjects/Dictionary;->pObjectReference:Lorg/icepdf/core/pobjects/Reference;

    return-object v0
.end method

.method public init()V
    .locals 0

    .prologue
    .line 117
    return-void
.end method

.method public isDeleted()Z
    .locals 1

    .prologue
    .line 186
    iget-boolean v0, p0, Lorg/icepdf/core/pobjects/Dictionary;->isDeleted:Z

    return v0
.end method

.method public isNew()Z
    .locals 1

    .prologue
    .line 194
    iget-boolean v0, p0, Lorg/icepdf/core/pobjects/Dictionary;->isNew:Z

    return v0
.end method

.method public setDeleted(Z)V
    .locals 0
    .param p1, "deleted"    # Z

    .prologue
    .line 190
    iput-boolean p1, p0, Lorg/icepdf/core/pobjects/Dictionary;->isDeleted:Z

    .line 191
    return-void
.end method

.method public setNew(Z)V
    .locals 0
    .param p1, "aNew"    # Z

    .prologue
    .line 198
    iput-boolean p1, p0, Lorg/icepdf/core/pobjects/Dictionary;->isNew:Z

    .line 199
    return-void
.end method

.method public setPObjectReference(Lorg/icepdf/core/pobjects/Reference;)V
    .locals 0
    .param p1, "reference"    # Lorg/icepdf/core/pobjects/Reference;

    .prologue
    .line 98
    iput-object p1, p0, Lorg/icepdf/core/pobjects/Dictionary;->pObjectReference:Lorg/icepdf/core/pobjects/Reference;

    .line 99
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 207
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/icepdf/core/pobjects/Dictionary;->entries:Ljava/util/Hashtable;

    invoke-virtual {v1}, Ljava/util/Hashtable;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
