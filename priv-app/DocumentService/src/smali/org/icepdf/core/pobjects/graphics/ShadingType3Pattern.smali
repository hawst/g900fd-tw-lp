.class public Lorg/icepdf/core/pobjects/graphics/ShadingType3Pattern;
.super Lorg/icepdf/core/pobjects/graphics/ShadingPattern;
.source "ShadingType3Pattern.java"


# static fields
.field private static final logger:Ljava/util/logging/Logger;


# instance fields
.field protected coords:Ljava/util/Vector;

.field protected domain:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<",
            "Ljava/lang/Number;",
            ">;"
        }
    .end annotation
.end field

.field protected extend:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field protected function:[Lorg/icepdf/core/pobjects/functions/Function;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 37
    const-class v0, Lorg/icepdf/core/pobjects/graphics/ShadingType3Pattern;

    invoke-virtual {v0}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Lorg/icepdf/core/pobjects/graphics/ShadingType3Pattern;->logger:Ljava/util/logging/Logger;

    return-void
.end method

.method public constructor <init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V
    .locals 0
    .param p1, "library"    # Lorg/icepdf/core/util/Library;
    .param p2, "entries"    # Ljava/util/Hashtable;

    .prologue
    .line 66
    invoke-direct {p0, p1, p2}, Lorg/icepdf/core/pobjects/graphics/ShadingPattern;-><init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V

    .line 67
    return-void
.end method

.method private calculateColour(Lorg/icepdf/core/pobjects/graphics/PColorSpace;FFF)Lorg/apache/poi/java/awt/Color;
    .locals 9
    .param p1, "colorSpace"    # Lorg/icepdf/core/pobjects/graphics/PColorSpace;
    .param p2, "s"    # F
    .param p3, "t0"    # F
    .param p4, "t1"    # F

    .prologue
    const/4 v5, 0x0

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 175
    iget-object v6, p0, Lorg/icepdf/core/pobjects/graphics/ShadingType3Pattern;->extend:Ljava/util/Vector;

    invoke-direct {p0, p2, p3, p4, v6}, Lorg/icepdf/core/pobjects/graphics/ShadingType3Pattern;->parametrixValue(FFFLjava/util/Vector;)F

    move-result v4

    .line 177
    .local v4, "t":F
    new-array v1, v8, [F

    .line 178
    .local v1, "input":[F
    aput v4, v1, v7

    .line 180
    iget-object v6, p0, Lorg/icepdf/core/pobjects/graphics/ShadingType3Pattern;->function:[Lorg/icepdf/core/pobjects/functions/Function;

    if-eqz v6, :cond_5

    .line 183
    iget-object v6, p0, Lorg/icepdf/core/pobjects/graphics/ShadingType3Pattern;->function:[Lorg/icepdf/core/pobjects/functions/Function;

    array-length v2, v6

    .line 185
    .local v2, "length":I
    if-ne v2, v8, :cond_3

    .line 186
    iget-object v6, p0, Lorg/icepdf/core/pobjects/graphics/ShadingType3Pattern;->function:[Lorg/icepdf/core/pobjects/functions/Function;

    aget-object v6, v6, v7

    invoke-virtual {v6, v1}, Lorg/icepdf/core/pobjects/functions/Function;->calculate([F)[F

    move-result-object v3

    .line 197
    .local v3, "output":[F
    :cond_0
    if-eqz v3, :cond_2

    .line 198
    instance-of v5, p1, Lorg/icepdf/core/pobjects/graphics/DeviceN;

    if-nez v5, :cond_1

    .line 199
    invoke-static {v3}, Lorg/icepdf/core/pobjects/graphics/PColorSpace;->reverse([F)[F

    move-result-object v3

    .line 201
    :cond_1
    invoke-virtual {p1, v3}, Lorg/icepdf/core/pobjects/graphics/PColorSpace;->getColor([F)Lorg/apache/poi/java/awt/Color;

    move-result-object v5

    .line 208
    .end local v2    # "length":I
    .end local v3    # "output":[F
    :cond_2
    :goto_0
    return-object v5

    .line 189
    .restart local v2    # "length":I
    :cond_3
    new-array v3, v2, [F

    .line 190
    .restart local v3    # "output":[F
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    if-ge v0, v2, :cond_0

    .line 192
    iget-object v6, p0, Lorg/icepdf/core/pobjects/graphics/ShadingType3Pattern;->function:[Lorg/icepdf/core/pobjects/functions/Function;

    aget-object v6, v6, v0

    invoke-virtual {v6, v1}, Lorg/icepdf/core/pobjects/functions/Function;->calculate([F)[F

    move-result-object v6

    if-eqz v6, :cond_4

    .line 193
    iget-object v6, p0, Lorg/icepdf/core/pobjects/graphics/ShadingType3Pattern;->function:[Lorg/icepdf/core/pobjects/functions/Function;

    aget-object v6, v6, v0

    invoke-virtual {v6, v1}, Lorg/icepdf/core/pobjects/functions/Function;->calculate([F)[F

    move-result-object v6

    aget v6, v6, v7

    aput v6, v3, v0

    .line 190
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 207
    .end local v0    # "i":I
    .end local v2    # "length":I
    .end local v3    # "output":[F
    :cond_5
    sget-object v6, Lorg/icepdf/core/pobjects/graphics/ShadingType3Pattern;->logger:Ljava/util/logging/Logger;

    const-string/jumbo v7, "Error processing Shading Type 3 Pattern."

    invoke-virtual {v6, v7}, Ljava/util/logging/Logger;->fine(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private parametrixValue(FFFLjava/util/Vector;)F
    .locals 1
    .param p1, "linearMapping"    # F
    .param p2, "t0"    # F
    .param p3, "t1"    # F
    .param p4, "extended"    # Ljava/util/Vector;

    .prologue
    .line 225
    sub-float v0, p3, p2

    mul-float/2addr v0, p1

    add-float/2addr v0, p2

    return v0
.end method


# virtual methods
.method public getPaint()Landroid/graphics/Paint;
    .locals 1

    .prologue
    .line 229
    invoke-virtual {p0}, Lorg/icepdf/core/pobjects/graphics/ShadingType3Pattern;->init()V

    .line 230
    const/4 v0, 0x0

    return-object v0
.end method

.method public declared-synchronized init()V
    .locals 16

    .prologue
    .line 71
    monitor-enter p0

    :try_start_0
    move-object/from16 v0, p0

    iget-boolean v12, v0, Lorg/icepdf/core/pobjects/graphics/ShadingType3Pattern;->inited:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v12, :cond_1

    .line 169
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 76
    :cond_1
    :try_start_1
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/icepdf/core/pobjects/graphics/ShadingType3Pattern;->shading:Ljava/util/Hashtable;

    if-nez v12, :cond_2

    .line 77
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/icepdf/core/pobjects/graphics/ShadingType3Pattern;->library:Lorg/icepdf/core/util/Library;

    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/icepdf/core/pobjects/graphics/ShadingType3Pattern;->entries:Ljava/util/Hashtable;

    const-string/jumbo v14, "Shading"

    invoke-virtual {v12, v13, v14}, Lorg/icepdf/core/util/Library;->getDictionary(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/util/Hashtable;

    move-result-object v12

    move-object/from16 v0, p0

    iput-object v12, v0, Lorg/icepdf/core/pobjects/graphics/ShadingType3Pattern;->shading:Ljava/util/Hashtable;

    .line 80
    :cond_2
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/icepdf/core/pobjects/graphics/ShadingType3Pattern;->library:Lorg/icepdf/core/util/Library;

    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/icepdf/core/pobjects/graphics/ShadingType3Pattern;->shading:Ljava/util/Hashtable;

    const-string/jumbo v14, "ShadingType"

    invoke-virtual {v12, v13, v14}, Lorg/icepdf/core/util/Library;->getInt(Ljava/util/Hashtable;Ljava/lang/String;)I

    move-result v12

    move-object/from16 v0, p0

    iput v12, v0, Lorg/icepdf/core/pobjects/graphics/ShadingType3Pattern;->shadingType:I

    .line 81
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/icepdf/core/pobjects/graphics/ShadingType3Pattern;->library:Lorg/icepdf/core/util/Library;

    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/icepdf/core/pobjects/graphics/ShadingType3Pattern;->shading:Ljava/util/Hashtable;

    const-string/jumbo v14, "BBox"

    invoke-virtual {v12, v13, v14}, Lorg/icepdf/core/util/Library;->getRectangle(Ljava/util/Hashtable;Ljava/lang/String;)Landroid/graphics/RectF;

    move-result-object v12

    move-object/from16 v0, p0

    iput-object v12, v0, Lorg/icepdf/core/pobjects/graphics/ShadingType3Pattern;->bBox:Landroid/graphics/RectF;

    .line 82
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/icepdf/core/pobjects/graphics/ShadingType3Pattern;->library:Lorg/icepdf/core/util/Library;

    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/icepdf/core/pobjects/graphics/ShadingType3Pattern;->library:Lorg/icepdf/core/util/Library;

    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/icepdf/core/pobjects/graphics/ShadingType3Pattern;->shading:Ljava/util/Hashtable;

    const-string/jumbo v15, "ColorSpace"

    invoke-virtual {v13, v14, v15}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v13

    invoke-static {v12, v13}, Lorg/icepdf/core/pobjects/graphics/PColorSpace;->getColorSpace(Lorg/icepdf/core/util/Library;Ljava/lang/Object;)Lorg/icepdf/core/pobjects/graphics/PColorSpace;

    move-result-object v12

    move-object/from16 v0, p0

    iput-object v12, v0, Lorg/icepdf/core/pobjects/graphics/ShadingType3Pattern;->colorSpace:Lorg/icepdf/core/pobjects/graphics/PColorSpace;

    .line 84
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/icepdf/core/pobjects/graphics/ShadingType3Pattern;->library:Lorg/icepdf/core/util/Library;

    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/icepdf/core/pobjects/graphics/ShadingType3Pattern;->shading:Ljava/util/Hashtable;

    const-string/jumbo v14, "Background"

    invoke-virtual {v12, v13, v14}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v12

    if-eqz v12, :cond_3

    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/icepdf/core/pobjects/graphics/ShadingType3Pattern;->library:Lorg/icepdf/core/util/Library;

    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/icepdf/core/pobjects/graphics/ShadingType3Pattern;->shading:Ljava/util/Hashtable;

    const-string/jumbo v14, "Background"

    invoke-virtual {v12, v13, v14}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v12

    instance-of v12, v12, Ljava/util/Vector;

    if-eqz v12, :cond_3

    .line 86
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/icepdf/core/pobjects/graphics/ShadingType3Pattern;->library:Lorg/icepdf/core/util/Library;

    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/icepdf/core/pobjects/graphics/ShadingType3Pattern;->shading:Ljava/util/Hashtable;

    const-string/jumbo v14, "Background"

    invoke-virtual {v12, v13, v14}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/util/Vector;

    move-object/from16 v0, p0

    iput-object v12, v0, Lorg/icepdf/core/pobjects/graphics/ShadingType3Pattern;->background:Ljava/util/Vector;

    .line 88
    :cond_3
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/icepdf/core/pobjects/graphics/ShadingType3Pattern;->library:Lorg/icepdf/core/util/Library;

    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/icepdf/core/pobjects/graphics/ShadingType3Pattern;->shading:Ljava/util/Hashtable;

    const-string/jumbo v14, "AntiAlias"

    invoke-virtual {v12, v13, v14}, Lorg/icepdf/core/util/Library;->getBoolean(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v12

    move-object/from16 v0, p0

    iput-boolean v12, v0, Lorg/icepdf/core/pobjects/graphics/ShadingType3Pattern;->antiAlias:Z

    .line 91
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/icepdf/core/pobjects/graphics/ShadingType3Pattern;->library:Lorg/icepdf/core/util/Library;

    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/icepdf/core/pobjects/graphics/ShadingType3Pattern;->shading:Ljava/util/Hashtable;

    const-string/jumbo v14, "Domain"

    invoke-virtual {v12, v13, v14}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v12

    instance-of v12, v12, Ljava/util/Vector;

    if-eqz v12, :cond_7

    .line 92
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/icepdf/core/pobjects/graphics/ShadingType3Pattern;->library:Lorg/icepdf/core/util/Library;

    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/icepdf/core/pobjects/graphics/ShadingType3Pattern;->shading:Ljava/util/Hashtable;

    const-string/jumbo v14, "Domain"

    invoke-virtual {v12, v13, v14}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/util/Vector;

    move-object/from16 v0, p0

    iput-object v12, v0, Lorg/icepdf/core/pobjects/graphics/ShadingType3Pattern;->domain:Ljava/util/Vector;

    .line 99
    :goto_1
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/icepdf/core/pobjects/graphics/ShadingType3Pattern;->library:Lorg/icepdf/core/util/Library;

    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/icepdf/core/pobjects/graphics/ShadingType3Pattern;->shading:Ljava/util/Hashtable;

    const-string/jumbo v14, "Coords"

    invoke-virtual {v12, v13, v14}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v12

    instance-of v12, v12, Ljava/util/Vector;

    if-eqz v12, :cond_4

    .line 100
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/icepdf/core/pobjects/graphics/ShadingType3Pattern;->library:Lorg/icepdf/core/util/Library;

    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/icepdf/core/pobjects/graphics/ShadingType3Pattern;->shading:Ljava/util/Hashtable;

    const-string/jumbo v14, "Coords"

    invoke-virtual {v12, v13, v14}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/util/Vector;

    move-object/from16 v0, p0

    iput-object v12, v0, Lorg/icepdf/core/pobjects/graphics/ShadingType3Pattern;->coords:Ljava/util/Vector;

    .line 102
    :cond_4
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/icepdf/core/pobjects/graphics/ShadingType3Pattern;->library:Lorg/icepdf/core/util/Library;

    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/icepdf/core/pobjects/graphics/ShadingType3Pattern;->shading:Ljava/util/Hashtable;

    const-string/jumbo v14, "Extend"

    invoke-virtual {v12, v13, v14}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v12

    instance-of v12, v12, Ljava/util/Vector;

    if-eqz v12, :cond_8

    .line 103
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/icepdf/core/pobjects/graphics/ShadingType3Pattern;->library:Lorg/icepdf/core/util/Library;

    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/icepdf/core/pobjects/graphics/ShadingType3Pattern;->shading:Ljava/util/Hashtable;

    const-string/jumbo v14, "Extend"

    invoke-virtual {v12, v13, v14}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/util/Vector;

    move-object/from16 v0, p0

    iput-object v12, v0, Lorg/icepdf/core/pobjects/graphics/ShadingType3Pattern;->extend:Ljava/util/Vector;

    .line 109
    :goto_2
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/icepdf/core/pobjects/graphics/ShadingType3Pattern;->library:Lorg/icepdf/core/util/Library;

    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/icepdf/core/pobjects/graphics/ShadingType3Pattern;->shading:Ljava/util/Hashtable;

    const-string/jumbo v14, "Function"

    invoke-virtual {v12, v13, v14}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v11

    .line 110
    .local v11, "tmp":Ljava/lang/Object;
    if-eqz v11, :cond_5

    .line 111
    instance-of v12, v11, Ljava/util/Vector;

    if-nez v12, :cond_9

    .line 112
    const/4 v12, 0x1

    new-array v12, v12, [Lorg/icepdf/core/pobjects/functions/Function;

    const/4 v13, 0x0

    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/icepdf/core/pobjects/graphics/ShadingType3Pattern;->library:Lorg/icepdf/core/util/Library;

    invoke-static {v14, v11}, Lorg/icepdf/core/pobjects/functions/Function;->getFunction(Lorg/icepdf/core/util/Library;Ljava/lang/Object;)Lorg/icepdf/core/pobjects/functions/Function;

    move-result-object v14

    aput-object v14, v12, v13

    move-object/from16 v0, p0

    iput-object v12, v0, Lorg/icepdf/core/pobjects/graphics/ShadingType3Pattern;->function:[Lorg/icepdf/core/pobjects/functions/Function;

    .line 123
    :cond_5
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/icepdf/core/pobjects/graphics/ShadingType3Pattern;->domain:Ljava/util/Vector;

    const/4 v13, 0x0

    invoke-virtual {v12, v13}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/Number;

    invoke-virtual {v12}, Ljava/lang/Number;->floatValue()F

    move-result v9

    .line 124
    .local v9, "t0":F
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/icepdf/core/pobjects/graphics/ShadingType3Pattern;->domain:Ljava/util/Vector;

    const/4 v13, 0x1

    invoke-virtual {v12, v13}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/Number;

    invoke-virtual {v12}, Ljava/lang/Number;->floatValue()F

    move-result v10

    .line 125
    .local v10, "t1":F
    const/4 v12, 0x5

    new-array v8, v12, [F

    fill-array-data v8, :array_0

    .line 136
    .local v8, "s":[F
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/icepdf/core/pobjects/graphics/ShadingType3Pattern;->coords:Ljava/util/Vector;

    const/4 v13, 0x2

    invoke-virtual {v12, v13}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/Number;

    invoke-virtual {v12}, Ljava/lang/Number;->floatValue()F

    move-result v6

    .line 137
    .local v6, "radius":F
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/icepdf/core/pobjects/graphics/ShadingType3Pattern;->coords:Ljava/util/Vector;

    const/4 v13, 0x5

    invoke-virtual {v12, v13}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/Number;

    invoke-virtual {v12}, Ljava/lang/Number;->floatValue()F

    move-result v7

    .line 141
    .local v7, "radius2":F
    cmpl-float v12, v7, v6

    if-lez v12, :cond_6

    .line 146
    :cond_6
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/icepdf/core/pobjects/graphics/ShadingType3Pattern;->colorSpace:Lorg/icepdf/core/pobjects/graphics/PColorSpace;

    const/4 v13, 0x0

    aget v13, v8, v13

    move-object/from16 v0, p0

    invoke-direct {v0, v12, v13, v9, v10}, Lorg/icepdf/core/pobjects/graphics/ShadingType3Pattern;->calculateColour(Lorg/icepdf/core/pobjects/graphics/PColorSpace;FFF)Lorg/apache/poi/java/awt/Color;

    move-result-object v2

    .line 147
    .local v2, "color1":Lorg/apache/poi/java/awt/Color;
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/icepdf/core/pobjects/graphics/ShadingType3Pattern;->colorSpace:Lorg/icepdf/core/pobjects/graphics/PColorSpace;

    const/4 v13, 0x1

    aget v13, v8, v13

    move-object/from16 v0, p0

    invoke-direct {v0, v12, v13, v9, v10}, Lorg/icepdf/core/pobjects/graphics/ShadingType3Pattern;->calculateColour(Lorg/icepdf/core/pobjects/graphics/PColorSpace;FFF)Lorg/apache/poi/java/awt/Color;

    move-result-object v3

    .line 152
    .local v3, "color2":Lorg/apache/poi/java/awt/Color;
    if-eqz v2, :cond_0

    if-eqz v3, :cond_0

    .line 168
    const/4 v12, 0x1

    move-object/from16 v0, p0

    iput-boolean v12, v0, Lorg/icepdf/core/pobjects/graphics/ShadingType3Pattern;->inited:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    .line 71
    .end local v2    # "color1":Lorg/apache/poi/java/awt/Color;
    .end local v3    # "color2":Lorg/apache/poi/java/awt/Color;
    .end local v6    # "radius":F
    .end local v7    # "radius2":F
    .end local v8    # "s":[F
    .end local v9    # "t0":F
    .end local v10    # "t1":F
    .end local v11    # "tmp":Ljava/lang/Object;
    :catchall_0
    move-exception v12

    monitor-exit p0

    throw v12

    .line 94
    :cond_7
    :try_start_2
    new-instance v12, Ljava/util/Vector;

    const/4 v13, 0x2

    invoke-direct {v12, v13}, Ljava/util/Vector;-><init>(I)V

    move-object/from16 v0, p0

    iput-object v12, v0, Lorg/icepdf/core/pobjects/graphics/ShadingType3Pattern;->domain:Ljava/util/Vector;

    .line 95
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/icepdf/core/pobjects/graphics/ShadingType3Pattern;->domain:Ljava/util/Vector;

    new-instance v13, Ljava/lang/Float;

    const-wide/16 v14, 0x0

    invoke-direct {v13, v14, v15}, Ljava/lang/Float;-><init>(D)V

    invoke-virtual {v12, v13}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 96
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/icepdf/core/pobjects/graphics/ShadingType3Pattern;->domain:Ljava/util/Vector;

    new-instance v13, Ljava/lang/Float;

    const-wide/high16 v14, 0x3ff0000000000000L    # 1.0

    invoke-direct {v13, v14, v15}, Ljava/lang/Float;-><init>(D)V

    invoke-virtual {v12, v13}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 105
    :cond_8
    new-instance v12, Ljava/util/Vector;

    const/4 v13, 0x2

    invoke-direct {v12, v13}, Ljava/util/Vector;-><init>(I)V

    move-object/from16 v0, p0

    iput-object v12, v0, Lorg/icepdf/core/pobjects/graphics/ShadingType3Pattern;->extend:Ljava/util/Vector;

    .line 106
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/icepdf/core/pobjects/graphics/ShadingType3Pattern;->extend:Ljava/util/Vector;

    const/4 v13, 0x0

    invoke-static {v13}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 107
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/icepdf/core/pobjects/graphics/ShadingType3Pattern;->extend:Ljava/util/Vector;

    const/4 v13, 0x0

    invoke-static {v13}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    .line 115
    .restart local v11    # "tmp":Ljava/lang/Object;
    :cond_9
    move-object v0, v11

    check-cast v0, Ljava/util/Vector;

    move-object v4, v0

    .line 116
    .local v4, "functionTemp":Ljava/util/Vector;
    invoke-virtual {v4}, Ljava/util/Vector;->size()I

    move-result v12

    new-array v12, v12, [Lorg/icepdf/core/pobjects/functions/Function;

    move-object/from16 v0, p0

    iput-object v12, v0, Lorg/icepdf/core/pobjects/graphics/ShadingType3Pattern;->function:[Lorg/icepdf/core/pobjects/functions/Function;

    .line 117
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_3
    invoke-virtual {v4}, Ljava/util/Vector;->size()I

    move-result v12

    if-ge v5, v12, :cond_5

    .line 118
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/icepdf/core/pobjects/graphics/ShadingType3Pattern;->function:[Lorg/icepdf/core/pobjects/functions/Function;

    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/icepdf/core/pobjects/graphics/ShadingType3Pattern;->library:Lorg/icepdf/core/util/Library;

    invoke-virtual {v4, v5}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v14

    invoke-static {v13, v14}, Lorg/icepdf/core/pobjects/functions/Function;->getFunction(Lorg/icepdf/core/util/Library;Ljava/lang/Object;)Lorg/icepdf/core/pobjects/functions/Function;

    move-result-object v13

    aput-object v13, v12, v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 117
    add-int/lit8 v5, v5, 0x1

    goto :goto_3

    .line 125
    :array_0
    .array-data 4
        0x0
        0x3e800000    # 0.25f
        0x3f000000    # 0.5f
        0x3f400000    # 0.75f
        0x3f800000    # 1.0f
    .end array-data
.end method

.method public toSting()Ljava/lang/String;
    .locals 2

    .prologue
    .line 234
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-super {p0}, Lorg/icepdf/core/pobjects/graphics/ShadingPattern;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "\n                    domain: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/icepdf/core/pobjects/graphics/ShadingType3Pattern;->domain:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "\n                    coords: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/icepdf/core/pobjects/graphics/ShadingType3Pattern;->coords:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "\n                    extend: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/icepdf/core/pobjects/graphics/ShadingType3Pattern;->extend:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "\n                 function: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/icepdf/core/pobjects/graphics/ShadingType3Pattern;->function:[Lorg/icepdf/core/pobjects/functions/Function;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
