.class public Lorg/icepdf/core/pobjects/functions/Function_2;
.super Lorg/icepdf/core/pobjects/functions/Function;
.source "Function_2.java"


# instance fields
.field private C0:[F

.field private C1:[F

.field private N:F


# direct methods
.method constructor <init>(Lorg/icepdf/core/pobjects/Dictionary;)V
    .locals 7
    .param p1, "d"    # Lorg/icepdf/core/pobjects/Dictionary;

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 66
    invoke-direct {p0, p1}, Lorg/icepdf/core/pobjects/functions/Function;-><init>(Lorg/icepdf/core/pobjects/Dictionary;)V

    .line 54
    new-array v3, v6, [F

    const/4 v4, 0x0

    aput v4, v3, v5

    iput-object v3, p0, Lorg/icepdf/core/pobjects/functions/Function_2;->C0:[F

    .line 58
    new-array v3, v6, [F

    const/high16 v4, 0x3f800000    # 1.0f

    aput v4, v3, v5

    iput-object v3, p0, Lorg/icepdf/core/pobjects/functions/Function_2;->C1:[F

    .line 68
    const-string/jumbo v3, "N"

    invoke-virtual {p1, v3}, Lorg/icepdf/core/pobjects/Dictionary;->getFloat(Ljava/lang/String;)F

    move-result v3

    iput v3, p0, Lorg/icepdf/core/pobjects/functions/Function_2;->N:F

    .line 71
    const-string/jumbo v3, "C0"

    invoke-virtual {p1, v3}, Lorg/icepdf/core/pobjects/Dictionary;->getObject(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Vector;

    .line 72
    .local v0, "c0":Ljava/util/Vector;
    if-eqz v0, :cond_0

    .line 73
    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v3

    new-array v3, v3, [F

    iput-object v3, p0, Lorg/icepdf/core/pobjects/functions/Function_2;->C0:[F

    .line 74
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v3

    if-ge v2, v3, :cond_0

    .line 75
    iget-object v4, p0, Lorg/icepdf/core/pobjects/functions/Function_2;->C0:[F

    invoke-virtual {v0, v2}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Number;

    invoke-virtual {v3}, Ljava/lang/Number;->floatValue()F

    move-result v3

    aput v3, v4, v2

    .line 74
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 87
    .end local v2    # "i":I
    :cond_0
    const-string/jumbo v3, "C1"

    invoke-virtual {p1, v3}, Lorg/icepdf/core/pobjects/Dictionary;->getObject(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Vector;

    .line 88
    .local v1, "c1":Ljava/util/Vector;
    if-eqz v1, :cond_1

    .line 89
    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v3

    new-array v3, v3, [F

    iput-object v3, p0, Lorg/icepdf/core/pobjects/functions/Function_2;->C1:[F

    .line 90
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_1
    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v3

    if-ge v2, v3, :cond_1

    .line 91
    iget-object v4, p0, Lorg/icepdf/core/pobjects/functions/Function_2;->C1:[F

    invoke-virtual {v1, v2}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Number;

    invoke-virtual {v3}, Ljava/lang/Number;->floatValue()F

    move-result v3

    aput v3, v4, v2

    .line 90
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 102
    .end local v2    # "i":I
    :cond_1
    return-void
.end method


# virtual methods
.method public calculate([F)[F
    .locals 10
    .param p1, "x"    # [F

    .prologue
    .line 117
    array-length v4, p1

    iget-object v5, p0, Lorg/icepdf/core/pobjects/functions/Function_2;->C0:[F

    array-length v5, v5

    mul-int/2addr v4, v5

    new-array v2, v4, [F

    .line 120
    .local v2, "y":[F
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v4, p1

    if-ge v0, v4, :cond_2

    .line 122
    const/4 v1, 0x0

    .local v1, "j":I
    :goto_1
    iget-object v4, p0, Lorg/icepdf/core/pobjects/functions/Function_2;->C0:[F

    array-length v4, v4

    if-ge v1, v4, :cond_1

    .line 124
    iget-object v4, p0, Lorg/icepdf/core/pobjects/functions/Function_2;->C0:[F

    aget v4, v4, v1

    float-to-double v4, v4

    aget v6, p1, v0

    float-to-double v6, v6

    iget v8, p0, Lorg/icepdf/core/pobjects/functions/Function_2;->N:F

    float-to-double v8, v8

    invoke-static {v6, v7, v8, v9}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v6

    iget-object v8, p0, Lorg/icepdf/core/pobjects/functions/Function_2;->C1:[F

    aget v8, v8, v1

    iget-object v9, p0, Lorg/icepdf/core/pobjects/functions/Function_2;->C0:[F

    aget v9, v9, v1

    sub-float/2addr v8, v9

    float-to-double v8, v8

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    double-to-float v3, v4

    .line 127
    .local v3, "yValue":F
    iget-object v4, p0, Lorg/icepdf/core/pobjects/functions/Function_2;->range:[F

    if-eqz v4, :cond_0

    .line 128
    iget-object v4, p0, Lorg/icepdf/core/pobjects/functions/Function_2;->range:[F

    mul-int/lit8 v5, v1, 0x2

    aget v4, v4, v5

    invoke-static {v3, v4}, Ljava/lang/Math;->max(FF)F

    move-result v4

    iget-object v5, p0, Lorg/icepdf/core/pobjects/functions/Function_2;->range:[F

    mul-int/lit8 v6, v1, 0x2

    add-int/lit8 v6, v6, 0x1

    aget v5, v5, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->min(FF)F

    move-result v3

    .line 131
    :cond_0
    iget-object v4, p0, Lorg/icepdf/core/pobjects/functions/Function_2;->C0:[F

    array-length v4, v4

    mul-int/2addr v4, v0

    add-int/2addr v4, v1

    aput v3, v2, v4

    .line 122
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 120
    .end local v3    # "yValue":F
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 134
    .end local v1    # "j":I
    :cond_2
    return-object v2
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 138
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "FunctionType: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lorg/icepdf/core/pobjects/functions/Function_2;->functionType:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "\n    domain: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/icepdf/core/pobjects/functions/Function_2;->domain:[F

    invoke-static {v1}, Ljava/util/Arrays;->toString([F)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "\n     range: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/icepdf/core/pobjects/functions/Function_2;->range:[F

    invoke-static {v1}, Ljava/util/Arrays;->toString([F)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "\n         N: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lorg/icepdf/core/pobjects/functions/Function_2;->N:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "\n        C0: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/icepdf/core/pobjects/functions/Function_2;->C0:[F

    invoke-static {v1}, Ljava/util/Arrays;->toString([F)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "\n        C1: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/icepdf/core/pobjects/functions/Function_2;->C1:[F

    invoke-static {v1}, Ljava/util/Arrays;->toString([F)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
