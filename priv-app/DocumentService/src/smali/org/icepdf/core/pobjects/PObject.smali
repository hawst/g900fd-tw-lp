.class public Lorg/icepdf/core/pobjects/PObject;
.super Ljava/lang/Object;
.source "PObject.java"


# instance fields
.field private object:Ljava/lang/Object;

.field private objectReference:Lorg/icepdf/core/pobjects/Reference;


# direct methods
.method public constructor <init>(Ljava/lang/Object;Ljava/lang/Number;Ljava/lang/Number;)V
    .locals 1
    .param p1, "object"    # Ljava/lang/Object;
    .param p2, "objectNumber"    # Ljava/lang/Number;
    .param p3, "objectGeneration"    # Ljava/lang/Number;

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/icepdf/core/pobjects/PObject;->objectReference:Lorg/icepdf/core/pobjects/Reference;

    .line 38
    iput-object p1, p0, Lorg/icepdf/core/pobjects/PObject;->object:Ljava/lang/Object;

    .line 39
    new-instance v0, Lorg/icepdf/core/pobjects/Reference;

    invoke-direct {v0, p2, p3}, Lorg/icepdf/core/pobjects/Reference;-><init>(Ljava/lang/Number;Ljava/lang/Number;)V

    iput-object v0, p0, Lorg/icepdf/core/pobjects/PObject;->objectReference:Lorg/icepdf/core/pobjects/Reference;

    .line 40
    return-void
.end method

.method public constructor <init>(Ljava/lang/Object;Lorg/icepdf/core/pobjects/Reference;)V
    .locals 1
    .param p1, "object"    # Ljava/lang/Object;
    .param p2, "objectReference"    # Lorg/icepdf/core/pobjects/Reference;

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/icepdf/core/pobjects/PObject;->objectReference:Lorg/icepdf/core/pobjects/Reference;

    .line 51
    iput-object p1, p0, Lorg/icepdf/core/pobjects/PObject;->object:Ljava/lang/Object;

    .line 52
    iput-object p2, p0, Lorg/icepdf/core/pobjects/PObject;->objectReference:Lorg/icepdf/core/pobjects/Reference;

    .line 53
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 83
    if-ne p0, p1, :cond_1

    .line 99
    :cond_0
    :goto_0
    return v1

    .line 86
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    :cond_2
    move v1, v2

    .line 87
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 90
    check-cast v0, Lorg/icepdf/core/pobjects/PObject;

    .line 92
    .local v0, "pObject":Lorg/icepdf/core/pobjects/PObject;
    iget-object v3, p0, Lorg/icepdf/core/pobjects/PObject;->object:Ljava/lang/Object;

    if-eqz v3, :cond_5

    iget-object v3, p0, Lorg/icepdf/core/pobjects/PObject;->object:Ljava/lang/Object;

    iget-object v4, v0, Lorg/icepdf/core/pobjects/PObject;->object:Ljava/lang/Object;

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_6

    :cond_4
    move v1, v2

    .line 93
    goto :goto_0

    .line 92
    :cond_5
    iget-object v3, v0, Lorg/icepdf/core/pobjects/PObject;->object:Ljava/lang/Object;

    if-nez v3, :cond_4

    .line 95
    :cond_6
    iget-object v3, p0, Lorg/icepdf/core/pobjects/PObject;->objectReference:Lorg/icepdf/core/pobjects/Reference;

    if-eqz v3, :cond_7

    iget-object v3, p0, Lorg/icepdf/core/pobjects/PObject;->objectReference:Lorg/icepdf/core/pobjects/Reference;

    iget-object v4, v0, Lorg/icepdf/core/pobjects/PObject;->objectReference:Lorg/icepdf/core/pobjects/Reference;

    invoke-virtual {v3, v4}, Lorg/icepdf/core/pobjects/Reference;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :goto_1
    move v1, v2

    .line 96
    goto :goto_0

    .line 95
    :cond_7
    iget-object v3, v0, Lorg/icepdf/core/pobjects/PObject;->objectReference:Lorg/icepdf/core/pobjects/Reference;

    if-eqz v3, :cond_0

    goto :goto_1
.end method

.method public getObject()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lorg/icepdf/core/pobjects/PObject;->object:Ljava/lang/Object;

    return-object v0
.end method

.method public getReference()Lorg/icepdf/core/pobjects/Reference;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lorg/icepdf/core/pobjects/PObject;->objectReference:Lorg/icepdf/core/pobjects/Reference;

    return-object v0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 76
    iget-object v2, p0, Lorg/icepdf/core/pobjects/PObject;->object:Ljava/lang/Object;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lorg/icepdf/core/pobjects/PObject;->object:Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v0

    .line 77
    .local v0, "result":I
    :goto_0
    mul-int/lit8 v2, v0, 0x1f

    iget-object v3, p0, Lorg/icepdf/core/pobjects/PObject;->objectReference:Lorg/icepdf/core/pobjects/Reference;

    if-eqz v3, :cond_0

    iget-object v1, p0, Lorg/icepdf/core/pobjects/PObject;->objectReference:Lorg/icepdf/core/pobjects/Reference;

    invoke-virtual {v1}, Lorg/icepdf/core/pobjects/Reference;->hashCode()I

    move-result v1

    :cond_0
    add-int v0, v2, v1

    .line 78
    return v0

    .end local v0    # "result":I
    :cond_1
    move v0, v1

    .line 76
    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 108
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lorg/icepdf/core/pobjects/PObject;->objectReference:Lorg/icepdf/core/pobjects/Reference;

    invoke-virtual {v1}, Lorg/icepdf/core/pobjects/Reference;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/icepdf/core/pobjects/PObject;->object:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
