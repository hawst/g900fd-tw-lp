.class public Lorg/icepdf/core/pobjects/actions/ActionFactory;
.super Ljava/lang/Object;
.source "ActionFactory.java"


# static fields
.field public static final GOTO_ACTION:I = 0x1

.field public static final LAUNCH_ACTION:I = 0x3

.field public static final URI_ACTION:I = 0x2


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static buildAction(Lorg/icepdf/core/util/Library;I)Lorg/icepdf/core/pobjects/actions/Action;
    .locals 5
    .param p0, "library"    # Lorg/icepdf/core/util/Library;
    .param p1, "type"    # I

    .prologue
    const/4 v0, 0x0

    .line 50
    invoke-virtual {p0}, Lorg/icepdf/core/util/Library;->getStateManager()Lorg/icepdf/core/pobjects/StateManager;

    move-result-object v2

    .line 53
    .local v2, "stateManager":Lorg/icepdf/core/pobjects/StateManager;
    new-instance v1, Ljava/util/Hashtable;

    invoke-direct {v1}, Ljava/util/Hashtable;-><init>()V

    .line 54
    .local v1, "entries":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Lorg/icepdf/core/pobjects/Name;Ljava/lang/Object;>;"
    const/4 v3, 0x1

    if-ne v3, p1, :cond_1

    .line 56
    sget-object v3, Lorg/icepdf/core/pobjects/Dictionary;->TYPE_KEY:Lorg/icepdf/core/pobjects/Name;

    sget-object v4, Lorg/icepdf/core/pobjects/actions/Action;->ACTION_TYPE:Lorg/icepdf/core/pobjects/Name;

    invoke-virtual {v1, v3, v4}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 57
    sget-object v3, Lorg/icepdf/core/pobjects/actions/Action;->ACTION_TYPE_KEY:Lorg/icepdf/core/pobjects/Name;

    sget-object v4, Lorg/icepdf/core/pobjects/actions/Action;->ACTION_TYPE_GOTO:Lorg/icepdf/core/pobjects/Name;

    invoke-virtual {v1, v3, v4}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 59
    sget-object v3, Lorg/icepdf/core/pobjects/actions/GoToAction;->DESTINATION_KEY:Lorg/icepdf/core/pobjects/Name;

    new-instance v4, Lorg/icepdf/core/pobjects/Destination;

    invoke-direct {v4, p0, v0}, Lorg/icepdf/core/pobjects/Destination;-><init>(Lorg/icepdf/core/util/Library;Ljava/lang/Object;)V

    invoke-virtual {v1, v3, v4}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 60
    new-instance v0, Lorg/icepdf/core/pobjects/actions/GoToAction;

    invoke-direct {v0, p0, v1}, Lorg/icepdf/core/pobjects/actions/GoToAction;-><init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V

    .line 61
    .local v0, "action":Lorg/icepdf/core/pobjects/actions/GoToAction;
    invoke-virtual {v2}, Lorg/icepdf/core/pobjects/StateManager;->getNewReferencNumber()Lorg/icepdf/core/pobjects/Reference;

    move-result-object v3

    invoke-virtual {v0, v3}, Lorg/icepdf/core/pobjects/actions/GoToAction;->setPObjectReference(Lorg/icepdf/core/pobjects/Reference;)V

    .line 82
    .end local v0    # "action":Lorg/icepdf/core/pobjects/actions/GoToAction;
    :cond_0
    :goto_0
    return-object v0

    .line 63
    :cond_1
    const/4 v3, 0x2

    if-ne v3, p1, :cond_2

    .line 65
    sget-object v3, Lorg/icepdf/core/pobjects/Dictionary;->TYPE_KEY:Lorg/icepdf/core/pobjects/Name;

    sget-object v4, Lorg/icepdf/core/pobjects/actions/Action;->ACTION_TYPE:Lorg/icepdf/core/pobjects/Name;

    invoke-virtual {v1, v3, v4}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 66
    sget-object v3, Lorg/icepdf/core/pobjects/actions/Action;->ACTION_TYPE_KEY:Lorg/icepdf/core/pobjects/Name;

    sget-object v4, Lorg/icepdf/core/pobjects/actions/Action;->ACTION_TYPE_URI:Lorg/icepdf/core/pobjects/Name;

    invoke-virtual {v1, v3, v4}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 68
    sget-object v3, Lorg/icepdf/core/pobjects/actions/URIAction;->URI_KEY:Lorg/icepdf/core/pobjects/Name;

    const-string/jumbo v4, ""

    invoke-virtual {v1, v3, v4}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 69
    new-instance v0, Lorg/icepdf/core/pobjects/actions/URIAction;

    invoke-direct {v0, p0, v1}, Lorg/icepdf/core/pobjects/actions/URIAction;-><init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V

    .line 70
    .local v0, "action":Lorg/icepdf/core/pobjects/actions/URIAction;
    invoke-virtual {v2}, Lorg/icepdf/core/pobjects/StateManager;->getNewReferencNumber()Lorg/icepdf/core/pobjects/Reference;

    move-result-object v3

    invoke-virtual {v0, v3}, Lorg/icepdf/core/pobjects/actions/URIAction;->setPObjectReference(Lorg/icepdf/core/pobjects/Reference;)V

    goto :goto_0

    .line 72
    .end local v0    # "action":Lorg/icepdf/core/pobjects/actions/URIAction;
    :cond_2
    const/4 v3, 0x3

    if-ne v3, p1, :cond_0

    .line 74
    sget-object v3, Lorg/icepdf/core/pobjects/Dictionary;->TYPE_KEY:Lorg/icepdf/core/pobjects/Name;

    sget-object v4, Lorg/icepdf/core/pobjects/actions/Action;->ACTION_TYPE:Lorg/icepdf/core/pobjects/Name;

    invoke-virtual {v1, v3, v4}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 75
    sget-object v3, Lorg/icepdf/core/pobjects/actions/Action;->ACTION_TYPE_KEY:Lorg/icepdf/core/pobjects/Name;

    sget-object v4, Lorg/icepdf/core/pobjects/actions/Action;->ACTION_TYPE_LAUNCH:Lorg/icepdf/core/pobjects/Name;

    invoke-virtual {v1, v3, v4}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 77
    sget-object v3, Lorg/icepdf/core/pobjects/actions/LaunchAction;->FILE_KEY:Lorg/icepdf/core/pobjects/Name;

    const-string/jumbo v4, ""

    invoke-virtual {v1, v3, v4}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 78
    new-instance v0, Lorg/icepdf/core/pobjects/actions/LaunchAction;

    invoke-direct {v0, p0, v1}, Lorg/icepdf/core/pobjects/actions/LaunchAction;-><init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V

    .line 79
    .local v0, "action":Lorg/icepdf/core/pobjects/actions/LaunchAction;
    invoke-virtual {v2}, Lorg/icepdf/core/pobjects/StateManager;->getNewReferencNumber()Lorg/icepdf/core/pobjects/Reference;

    move-result-object v3

    invoke-virtual {v0, v3}, Lorg/icepdf/core/pobjects/actions/LaunchAction;->setPObjectReference(Lorg/icepdf/core/pobjects/Reference;)V

    goto :goto_0
.end method
