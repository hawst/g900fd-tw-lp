.class public Lorg/icepdf/core/pobjects/filters/ASCIIHexDecode;
.super Lorg/icepdf/core/pobjects/filters/ChunkingInputStream;
.source "ASCIIHexDecode.java"


# direct methods
.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 1
    .param p1, "input"    # Ljava/io/InputStream;

    .prologue
    .line 28
    invoke-direct {p0}, Lorg/icepdf/core/pobjects/filters/ChunkingInputStream;-><init>()V

    .line 30
    invoke-virtual {p0, p1}, Lorg/icepdf/core/pobjects/filters/ASCIIHexDecode;->setInputStream(Ljava/io/InputStream;)V

    .line 31
    const/16 v0, 0x1000

    invoke-virtual {p0, v0}, Lorg/icepdf/core/pobjects/filters/ASCIIHexDecode;->setBufferSize(I)V

    .line 32
    return-void
.end method


# virtual methods
.method protected fillInternalBuffer()I
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v11, 0x61

    const/16 v10, 0x5a

    const/16 v9, 0x41

    const/16 v8, 0x39

    const/16 v7, 0x30

    .line 35
    const/4 v3, 0x0

    .line 37
    .local v3, "numRead":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v6, p0, Lorg/icepdf/core/pobjects/filters/ASCIIHexDecode;->buffer:[B

    array-length v6, v6

    if-ge v1, v6, :cond_1

    .line 38
    const/4 v5, 0x0

    .line 42
    .local v5, "val":B
    :cond_0
    iget-object v6, p0, Lorg/icepdf/core/pobjects/filters/ASCIIHexDecode;->in:Ljava/io/InputStream;

    invoke-virtual {v6}, Ljava/io/InputStream;->read()I

    move-result v0

    .line 43
    .local v0, "hi":I
    int-to-char v6, v0

    invoke-static {v6}, Lorg/icepdf/core/util/Parser;->isWhitespace(C)Z

    move-result v6

    if-nez v6, :cond_0

    .line 44
    if-gez v0, :cond_3

    .line 76
    .end local v0    # "hi":I
    .end local v5    # "val":B
    :cond_1
    if-nez v3, :cond_2

    .line 77
    const/4 v3, -0x1

    .line 78
    .end local v3    # "numRead":I
    :cond_2
    return v3

    .line 47
    .restart local v0    # "hi":I
    .restart local v3    # "numRead":I
    .restart local v5    # "val":B
    :cond_3
    iget-object v6, p0, Lorg/icepdf/core/pobjects/filters/ASCIIHexDecode;->in:Ljava/io/InputStream;

    invoke-virtual {v6}, Ljava/io/InputStream;->read()I

    move-result v2

    .line 48
    .local v2, "lo":I
    int-to-char v6, v2

    invoke-static {v6}, Lorg/icepdf/core/util/Parser;->isWhitespace(C)Z

    move-result v6

    if-nez v6, :cond_3

    .line 50
    if-lt v0, v7, :cond_6

    if-gt v0, v8, :cond_6

    .line 51
    add-int/lit8 v0, v0, -0x30

    .line 52
    shl-int/lit8 v6, v0, 0x4

    and-int/lit16 v6, v6, 0xf0

    int-to-byte v6, v6

    or-int/2addr v6, v5

    int-to-byte v5, v6

    .line 61
    :cond_4
    :goto_1
    if-ltz v2, :cond_5

    .line 62
    if-lt v2, v7, :cond_8

    if-gt v2, v8, :cond_8

    .line 63
    add-int/lit8 v2, v2, -0x30

    .line 64
    and-int/lit8 v6, v2, 0xf

    int-to-byte v6, v6

    or-int/2addr v6, v5

    int-to-byte v5, v6

    .line 73
    :cond_5
    :goto_2
    iget-object v6, p0, Lorg/icepdf/core/pobjects/filters/ASCIIHexDecode;->buffer:[B

    add-int/lit8 v4, v3, 0x1

    .end local v3    # "numRead":I
    .local v4, "numRead":I
    aput-byte v5, v6, v3

    .line 37
    add-int/lit8 v1, v1, 0x1

    move v3, v4

    .end local v4    # "numRead":I
    .restart local v3    # "numRead":I
    goto :goto_0

    .line 53
    :cond_6
    if-lt v0, v11, :cond_7

    const/16 v6, 0x7a

    if-gt v0, v6, :cond_7

    .line 54
    add-int/lit8 v6, v0, -0x61

    add-int/lit8 v0, v6, 0xa

    .line 55
    shl-int/lit8 v6, v0, 0x4

    and-int/lit16 v6, v6, 0xf0

    int-to-byte v6, v6

    or-int/2addr v6, v5

    int-to-byte v5, v6

    goto :goto_1

    .line 56
    :cond_7
    if-lt v0, v9, :cond_4

    if-gt v0, v10, :cond_4

    .line 57
    add-int/lit8 v6, v0, -0x41

    add-int/lit8 v0, v6, 0xa

    .line 58
    shl-int/lit8 v6, v0, 0x4

    and-int/lit16 v6, v6, 0xf0

    int-to-byte v6, v6

    or-int/2addr v6, v5

    int-to-byte v5, v6

    goto :goto_1

    .line 65
    :cond_8
    if-lt v2, v11, :cond_9

    const/16 v6, 0x7a

    if-gt v2, v6, :cond_9

    .line 66
    add-int/lit8 v6, v2, -0x61

    add-int/lit8 v2, v6, 0xa

    .line 67
    and-int/lit8 v6, v2, 0xf

    int-to-byte v6, v6

    or-int/2addr v6, v5

    int-to-byte v5, v6

    goto :goto_2

    .line 68
    :cond_9
    if-lt v2, v9, :cond_5

    if-gt v2, v10, :cond_5

    .line 69
    add-int/lit8 v6, v2, -0x41

    add-int/lit8 v2, v6, 0xa

    .line 70
    and-int/lit8 v6, v2, 0xf

    int-to-byte v6, v6

    or-int/2addr v6, v5

    int-to-byte v5, v6

    goto :goto_2
.end method
