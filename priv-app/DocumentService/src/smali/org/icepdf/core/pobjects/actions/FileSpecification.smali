.class public Lorg/icepdf/core/pobjects/actions/FileSpecification;
.super Lorg/icepdf/core/pobjects/Dictionary;
.source "FileSpecification.java"


# direct methods
.method public constructor <init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V
    .locals 0
    .param p1, "l"    # Lorg/icepdf/core/util/Library;
    .param p2, "h"    # Ljava/util/Hashtable;

    .prologue
    .line 41
    invoke-direct {p0, p1, p2}, Lorg/icepdf/core/pobjects/Dictionary;-><init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V

    .line 42
    return-void
.end method


# virtual methods
.method public getDescription()Ljava/lang/String;
    .locals 5

    .prologue
    .line 178
    iget-object v2, p0, Lorg/icepdf/core/pobjects/actions/FileSpecification;->library:Lorg/icepdf/core/util/Library;

    iget-object v3, p0, Lorg/icepdf/core/pobjects/actions/FileSpecification;->entries:Ljava/util/Hashtable;

    const-string/jumbo v4, "Desc"

    invoke-virtual {v2, v3, v4}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 179
    .local v0, "description":Ljava/lang/Object;
    instance-of v2, v0, Lorg/icepdf/core/pobjects/StringObject;

    if-eqz v2, :cond_0

    move-object v1, v0

    .line 180
    check-cast v1, Lorg/icepdf/core/pobjects/StringObject;

    .line 181
    .local v1, "tmp":Lorg/icepdf/core/pobjects/StringObject;
    iget-object v2, p0, Lorg/icepdf/core/pobjects/actions/FileSpecification;->library:Lorg/icepdf/core/util/Library;

    iget-object v2, v2, Lorg/icepdf/core/util/Library;->securityManager:Lorg/icepdf/core/pobjects/security/SecurityManager;

    invoke-interface {v1, v2}, Lorg/icepdf/core/pobjects/StringObject;->getDecryptedLiteralString(Lorg/icepdf/core/pobjects/security/SecurityManager;)Ljava/lang/String;

    move-result-object v2

    .line 185
    .end local v1    # "tmp":Lorg/icepdf/core/pobjects/StringObject;
    :goto_0
    return-object v2

    .line 182
    :cond_0
    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 183
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 185
    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public getDos()Ljava/lang/String;
    .locals 3

    .prologue
    .line 86
    iget-object v0, p0, Lorg/icepdf/core/pobjects/actions/FileSpecification;->library:Lorg/icepdf/core/util/Library;

    iget-object v1, p0, Lorg/icepdf/core/pobjects/actions/FileSpecification;->entries:Ljava/util/Hashtable;

    const-string/jumbo v2, "DOS"

    invoke-virtual {v0, v1, v2}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lorg/icepdf/core/pobjects/StringObject;

    if-eqz v0, :cond_0

    .line 87
    iget-object v0, p0, Lorg/icepdf/core/pobjects/actions/FileSpecification;->library:Lorg/icepdf/core/util/Library;

    iget-object v1, p0, Lorg/icepdf/core/pobjects/actions/FileSpecification;->entries:Ljava/util/Hashtable;

    const-string/jumbo v2, "DOS"

    invoke-virtual {v0, v1, v2}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/icepdf/core/pobjects/StringObject;

    iget-object v1, p0, Lorg/icepdf/core/pobjects/actions/FileSpecification;->library:Lorg/icepdf/core/util/Library;

    invoke-virtual {v1}, Lorg/icepdf/core/util/Library;->getSecurityManager()Lorg/icepdf/core/pobjects/security/SecurityManager;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/icepdf/core/pobjects/StringObject;->getDecryptedLiteralString(Lorg/icepdf/core/pobjects/security/SecurityManager;)Ljava/lang/String;

    move-result-object v0

    .line 91
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getEmbeddedFileDictionary()Ljava/util/Hashtable;
    .locals 3

    .prologue
    .line 157
    iget-object v0, p0, Lorg/icepdf/core/pobjects/actions/FileSpecification;->library:Lorg/icepdf/core/util/Library;

    iget-object v1, p0, Lorg/icepdf/core/pobjects/actions/FileSpecification;->entries:Ljava/util/Hashtable;

    const-string/jumbo v2, "EF"

    invoke-virtual {v0, v1, v2}, Lorg/icepdf/core/util/Library;->getDictionary(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/util/Hashtable;

    move-result-object v0

    return-object v0
.end method

.method public getFileSpecification()Ljava/lang/String;
    .locals 3

    .prologue
    .line 71
    iget-object v0, p0, Lorg/icepdf/core/pobjects/actions/FileSpecification;->library:Lorg/icepdf/core/util/Library;

    iget-object v1, p0, Lorg/icepdf/core/pobjects/actions/FileSpecification;->entries:Ljava/util/Hashtable;

    const-string/jumbo v2, "F"

    invoke-virtual {v0, v1, v2}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lorg/icepdf/core/pobjects/StringObject;

    if-eqz v0, :cond_0

    .line 72
    iget-object v0, p0, Lorg/icepdf/core/pobjects/actions/FileSpecification;->library:Lorg/icepdf/core/util/Library;

    iget-object v1, p0, Lorg/icepdf/core/pobjects/actions/FileSpecification;->entries:Ljava/util/Hashtable;

    const-string/jumbo v2, "F"

    invoke-virtual {v0, v1, v2}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/icepdf/core/pobjects/StringObject;

    iget-object v1, p0, Lorg/icepdf/core/pobjects/actions/FileSpecification;->library:Lorg/icepdf/core/util/Library;

    invoke-virtual {v1}, Lorg/icepdf/core/util/Library;->getSecurityManager()Lorg/icepdf/core/pobjects/security/SecurityManager;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/icepdf/core/pobjects/StringObject;->getDecryptedLiteralString(Lorg/icepdf/core/pobjects/security/SecurityManager;)Ljava/lang/String;

    move-result-object v0

    .line 76
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getFileSystemName()Ljava/lang/String;
    .locals 3

    .prologue
    .line 62
    iget-object v0, p0, Lorg/icepdf/core/pobjects/actions/FileSpecification;->library:Lorg/icepdf/core/util/Library;

    iget-object v1, p0, Lorg/icepdf/core/pobjects/actions/FileSpecification;->entries:Ljava/util/Hashtable;

    const-string/jumbo v2, "FS"

    invoke-virtual {v0, v1, v2}, Lorg/icepdf/core/util/Library;->getName(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getId()Ljava/lang/String;
    .locals 3

    .prologue
    .line 132
    iget-object v0, p0, Lorg/icepdf/core/pobjects/actions/FileSpecification;->library:Lorg/icepdf/core/util/Library;

    iget-object v1, p0, Lorg/icepdf/core/pobjects/actions/FileSpecification;->entries:Ljava/util/Hashtable;

    const-string/jumbo v2, "ID"

    invoke-virtual {v0, v1, v2}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 133
    iget-object v0, p0, Lorg/icepdf/core/pobjects/actions/FileSpecification;->library:Lorg/icepdf/core/util/Library;

    iget-object v1, p0, Lorg/icepdf/core/pobjects/actions/FileSpecification;->entries:Ljava/util/Hashtable;

    const-string/jumbo v2, "ID"

    invoke-virtual {v0, v1, v2}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 135
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getMac()Ljava/lang/String;
    .locals 3

    .prologue
    .line 101
    iget-object v0, p0, Lorg/icepdf/core/pobjects/actions/FileSpecification;->library:Lorg/icepdf/core/util/Library;

    iget-object v1, p0, Lorg/icepdf/core/pobjects/actions/FileSpecification;->entries:Ljava/util/Hashtable;

    const-string/jumbo v2, "Mac"

    invoke-virtual {v0, v1, v2}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lorg/icepdf/core/pobjects/StringObject;

    if-eqz v0, :cond_0

    .line 102
    iget-object v0, p0, Lorg/icepdf/core/pobjects/actions/FileSpecification;->library:Lorg/icepdf/core/util/Library;

    iget-object v1, p0, Lorg/icepdf/core/pobjects/actions/FileSpecification;->entries:Ljava/util/Hashtable;

    const-string/jumbo v2, "Mac"

    invoke-virtual {v0, v1, v2}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/icepdf/core/pobjects/StringObject;

    iget-object v1, p0, Lorg/icepdf/core/pobjects/actions/FileSpecification;->library:Lorg/icepdf/core/util/Library;

    invoke-virtual {v1}, Lorg/icepdf/core/util/Library;->getSecurityManager()Lorg/icepdf/core/pobjects/security/SecurityManager;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/icepdf/core/pobjects/StringObject;->getDecryptedLiteralString(Lorg/icepdf/core/pobjects/security/SecurityManager;)Ljava/lang/String;

    move-result-object v0

    .line 106
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getRelatedFilesDictionary()Ljava/util/Hashtable;
    .locals 3

    .prologue
    .line 169
    iget-object v0, p0, Lorg/icepdf/core/pobjects/actions/FileSpecification;->library:Lorg/icepdf/core/util/Library;

    iget-object v1, p0, Lorg/icepdf/core/pobjects/actions/FileSpecification;->entries:Ljava/util/Hashtable;

    const-string/jumbo v2, "RF"

    invoke-virtual {v0, v1, v2}, Lorg/icepdf/core/util/Library;->getDictionary(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/util/Hashtable;

    move-result-object v0

    return-object v0
.end method

.method public getType()Ljava/lang/String;
    .locals 3

    .prologue
    .line 51
    iget-object v0, p0, Lorg/icepdf/core/pobjects/actions/FileSpecification;->library:Lorg/icepdf/core/util/Library;

    iget-object v1, p0, Lorg/icepdf/core/pobjects/actions/FileSpecification;->entries:Ljava/util/Hashtable;

    const-string/jumbo v2, "Type"

    invoke-virtual {v0, v1, v2}, Lorg/icepdf/core/util/Library;->getName(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getUnix()Ljava/lang/String;
    .locals 3

    .prologue
    .line 116
    iget-object v0, p0, Lorg/icepdf/core/pobjects/actions/FileSpecification;->library:Lorg/icepdf/core/util/Library;

    iget-object v1, p0, Lorg/icepdf/core/pobjects/actions/FileSpecification;->entries:Ljava/util/Hashtable;

    const-string/jumbo v2, "Unix"

    invoke-virtual {v0, v1, v2}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lorg/icepdf/core/pobjects/StringObject;

    if-eqz v0, :cond_0

    .line 117
    iget-object v0, p0, Lorg/icepdf/core/pobjects/actions/FileSpecification;->library:Lorg/icepdf/core/util/Library;

    iget-object v1, p0, Lorg/icepdf/core/pobjects/actions/FileSpecification;->entries:Ljava/util/Hashtable;

    const-string/jumbo v2, "Unix"

    invoke-virtual {v0, v1, v2}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/icepdf/core/pobjects/StringObject;

    iget-object v1, p0, Lorg/icepdf/core/pobjects/actions/FileSpecification;->library:Lorg/icepdf/core/util/Library;

    invoke-virtual {v1}, Lorg/icepdf/core/util/Library;->getSecurityManager()Lorg/icepdf/core/pobjects/security/SecurityManager;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/icepdf/core/pobjects/StringObject;->getDecryptedLiteralString(Lorg/icepdf/core/pobjects/security/SecurityManager;)Ljava/lang/String;

    move-result-object v0

    .line 121
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isVolitile()Ljava/lang/Boolean;
    .locals 3

    .prologue
    .line 147
    iget-object v0, p0, Lorg/icepdf/core/pobjects/actions/FileSpecification;->library:Lorg/icepdf/core/util/Library;

    iget-object v1, p0, Lorg/icepdf/core/pobjects/actions/FileSpecification;->entries:Ljava/util/Hashtable;

    const-string/jumbo v2, "V"

    invoke-virtual {v0, v1, v2}, Lorg/icepdf/core/util/Library;->getBoolean(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
