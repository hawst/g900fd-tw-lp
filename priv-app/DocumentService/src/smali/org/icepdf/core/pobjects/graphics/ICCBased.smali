.class public Lorg/icepdf/core/pobjects/graphics/ICCBased;
.super Lorg/icepdf/core/pobjects/graphics/PColorSpace;
.source "ICCBased.java"


# static fields
.field private static final logger:Ljava/util/logging/Logger;


# instance fields
.field alternate:Lorg/icepdf/core/pobjects/graphics/PColorSpace;

.field colorSpace:I

.field private lastInput:[F

.field private lastOutput:Lorg/apache/poi/java/awt/Color;

.field numcomp:I

.field stream:Lorg/icepdf/core/pobjects/Stream;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    const-class v0, Lorg/icepdf/core/pobjects/graphics/ICCBased;

    invoke-virtual {v0}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Lorg/icepdf/core/pobjects/graphics/ICCBased;->logger:Ljava/util/logging/Logger;

    return-void
.end method

.method public constructor <init>(Lorg/icepdf/core/util/Library;Lorg/icepdf/core/pobjects/Stream;)V
    .locals 2
    .param p1, "l"    # Lorg/icepdf/core/util/Library;
    .param p2, "h"    # Lorg/icepdf/core/pobjects/Stream;

    .prologue
    const/4 v1, 0x0

    .line 49
    invoke-virtual {p2}, Lorg/icepdf/core/pobjects/Stream;->getEntries()Ljava/util/Hashtable;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lorg/icepdf/core/pobjects/graphics/PColorSpace;-><init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V

    .line 39
    const/4 v0, -0x1

    iput v0, p0, Lorg/icepdf/core/pobjects/graphics/ICCBased;->colorSpace:I

    .line 50
    const-string/jumbo v0, "N"

    invoke-virtual {p2, v0}, Lorg/icepdf/core/pobjects/Stream;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lorg/icepdf/core/pobjects/graphics/ICCBased;->numcomp:I

    .line 51
    iget v0, p0, Lorg/icepdf/core/pobjects/graphics/ICCBased;->numcomp:I

    packed-switch v0, :pswitch_data_0

    .line 62
    :goto_0
    :pswitch_0
    iput-object p2, p0, Lorg/icepdf/core/pobjects/graphics/ICCBased;->stream:Lorg/icepdf/core/pobjects/Stream;

    .line 63
    return-void

    .line 53
    :pswitch_1
    new-instance v0, Lorg/icepdf/core/pobjects/graphics/DeviceGray;

    invoke-direct {v0, p1, v1}, Lorg/icepdf/core/pobjects/graphics/DeviceGray;-><init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V

    iput-object v0, p0, Lorg/icepdf/core/pobjects/graphics/ICCBased;->alternate:Lorg/icepdf/core/pobjects/graphics/PColorSpace;

    goto :goto_0

    .line 56
    :pswitch_2
    new-instance v0, Lorg/icepdf/core/pobjects/graphics/DeviceRGB;

    invoke-direct {v0, p1, v1}, Lorg/icepdf/core/pobjects/graphics/DeviceRGB;-><init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V

    iput-object v0, p0, Lorg/icepdf/core/pobjects/graphics/ICCBased;->alternate:Lorg/icepdf/core/pobjects/graphics/PColorSpace;

    goto :goto_0

    .line 59
    :pswitch_3
    new-instance v0, Lorg/icepdf/core/pobjects/graphics/DeviceCMYK;

    invoke-direct {v0, p1, v1}, Lorg/icepdf/core/pobjects/graphics/DeviceCMYK;-><init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V

    iput-object v0, p0, Lorg/icepdf/core/pobjects/graphics/ICCBased;->alternate:Lorg/icepdf/core/pobjects/graphics/PColorSpace;

    goto :goto_0

    .line 51
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public getAlternate()Lorg/icepdf/core/pobjects/graphics/PColorSpace;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lorg/icepdf/core/pobjects/graphics/ICCBased;->alternate:Lorg/icepdf/core/pobjects/graphics/PColorSpace;

    return-object v0
.end method

.method public getColor([F)Lorg/apache/poi/java/awt/Color;
    .locals 17
    .param p1, "f"    # [F

    .prologue
    .line 120
    invoke-virtual/range {p0 .. p0}, Lorg/icepdf/core/pobjects/graphics/ICCBased;->init()V

    .line 121
    move-object/from16 v0, p0

    iget v14, v0, Lorg/icepdf/core/pobjects/graphics/ICCBased;->colorSpace:I

    const/4 v15, -0x1

    if-eq v14, v15, :cond_b

    .line 123
    :try_start_0
    monitor-enter p0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 127
    :try_start_1
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/icepdf/core/pobjects/graphics/ICCBased;->lastOutput:Lorg/apache/poi/java/awt/Color;

    if-eqz v14, :cond_2

    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/icepdf/core/pobjects/graphics/ICCBased;->lastInput:[F

    if-eqz v14, :cond_2

    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/icepdf/core/pobjects/graphics/ICCBased;->lastInput:[F

    array-length v14, v14

    move-object/from16 v0, p1

    array-length v15, v0

    if-ne v14, v15, :cond_2

    .line 129
    const/4 v8, 0x1

    .line 130
    .local v8, "matches":Z
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/icepdf/core/pobjects/graphics/ICCBased;->lastInput:[F

    array-length v10, v14

    .line 131
    .local v10, "num":I
    add-int/lit8 v7, v10, -0x1

    .local v7, "i":I
    :goto_0
    if-ltz v7, :cond_0

    .line 132
    aget v14, p1, v7

    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/icepdf/core/pobjects/graphics/ICCBased;->lastInput:[F

    aget v15, v15, v7

    cmpl-float v14, v14, v15

    if-eqz v14, :cond_1

    .line 133
    const/4 v8, 0x0

    .line 137
    :cond_0
    if-eqz v8, :cond_2

    .line 138
    move-object/from16 v0, p0

    iget-object v1, v0, Lorg/icepdf/core/pobjects/graphics/ICCBased;->lastOutput:Lorg/apache/poi/java/awt/Color;

    monitor-exit p0

    .line 194
    .end local v7    # "i":I
    .end local v8    # "matches":Z
    .end local v10    # "num":I
    :goto_1
    return-object v1

    .line 131
    .restart local v7    # "i":I
    .restart local v8    # "matches":Z
    .restart local v10    # "num":I
    :cond_1
    add-int/lit8 v7, v7, -0x1

    goto :goto_0

    .line 141
    .end local v7    # "i":I
    .end local v8    # "matches":Z
    .end local v10    # "num":I
    :cond_2
    const/4 v9, 0x4

    .line 144
    .local v9, "n":I
    new-array v6, v9, [F

    .line 145
    .local v6, "fvalue":[F
    move v12, v9

    .line 146
    .local v12, "toCopy":I
    move-object/from16 v0, p1

    array-length v4, v0

    .line 147
    .local v4, "fLength":I
    if-ge v4, v12, :cond_3

    .line 148
    move v12, v4

    .line 150
    :cond_3
    const/4 v7, 0x0

    .restart local v7    # "i":I
    :goto_2
    if-ge v7, v12, :cond_6

    .line 151
    add-int/lit8 v14, v4, -0x1

    sub-int/2addr v14, v7

    aget v2, p1, v14

    .line 152
    .local v2, "curr":F
    const/4 v14, 0x0

    cmpg-float v14, v2, v14

    if-gez v14, :cond_5

    .line 153
    const/4 v2, 0x0

    .line 156
    :cond_4
    :goto_3
    aput v2, v6, v7

    .line 150
    add-int/lit8 v7, v7, 0x1

    goto :goto_2

    .line 154
    :cond_5
    const/high16 v14, 0x3f800000    # 1.0f

    cmpl-float v14, v2, v14

    if-lez v14, :cond_4

    .line 155
    const/high16 v2, 0x3f800000    # 1.0f

    goto :goto_3

    .line 159
    .end local v2    # "curr":F
    :cond_6
    const/4 v14, 0x4

    new-array v5, v14, [F

    .line 160
    .local v5, "frgbvalue":[F
    move-object/from16 v0, p0

    iget v14, v0, Lorg/icepdf/core/pobjects/graphics/ICCBased;->colorSpace:I

    if-eqz v14, :cond_7

    .line 163
    const/4 v14, 0x0

    aget v14, v6, v14

    const/4 v15, 0x1

    aget v15, v6, v15

    const/16 v16, 0x2

    aget v16, v6, v16

    invoke-static/range {v14 .. v16}, Lorg/apache/poi/java/awt/Color;->getHSBColor(FFF)Lorg/apache/poi/java/awt/Color;

    move-result-object v11

    .line 164
    .local v11, "tmp":Lorg/apache/poi/java/awt/Color;
    invoke-virtual {v11, v5}, Lorg/apache/poi/java/awt/Color;->getRGBColorComponents([F)[F

    .line 168
    .end local v11    # "tmp":Lorg/apache/poi/java/awt/Color;
    :cond_7
    const/high16 v14, -0x1000000

    const/4 v15, 0x0

    aget v15, v5, v15

    const/high16 v16, 0x437f0000    # 255.0f

    mul-float v15, v15, v16

    float-to-int v15, v15

    and-int/lit16 v15, v15, 0xff

    shl-int/lit8 v15, v15, 0x10

    or-int/2addr v14, v15

    const/4 v15, 0x1

    aget v15, v5, v15

    const/high16 v16, 0x437f0000    # 255.0f

    mul-float v15, v15, v16

    float-to-int v15, v15

    and-int/lit16 v15, v15, 0xff

    shl-int/lit8 v15, v15, 0x8

    or-int/2addr v14, v15

    const/4 v15, 0x2

    aget v15, v5, v15

    const/high16 v16, 0x437f0000    # 255.0f

    mul-float v15, v15, v16

    float-to-int v15, v15

    and-int/lit16 v15, v15, 0xff

    or-int v13, v14, v15

    .line 172
    .local v13, "value":I
    new-instance v1, Lorg/apache/poi/java/awt/Color;

    invoke-direct {v1, v13}, Lorg/apache/poi/java/awt/Color;-><init>(I)V

    .line 175
    .local v1, "c":Lorg/apache/poi/java/awt/Color;
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/icepdf/core/pobjects/graphics/ICCBased;->lastInput:[F

    if-eqz v14, :cond_8

    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/icepdf/core/pobjects/graphics/ICCBased;->lastInput:[F

    array-length v14, v14

    if-eq v14, v4, :cond_9

    .line 176
    :cond_8
    new-array v14, v4, [F

    move-object/from16 v0, p0

    iput-object v14, v0, Lorg/icepdf/core/pobjects/graphics/ICCBased;->lastInput:[F

    .line 177
    :cond_9
    add-int/lit8 v7, v4, -0x1

    :goto_4
    if-ltz v7, :cond_a

    .line 178
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/icepdf/core/pobjects/graphics/ICCBased;->lastInput:[F

    aget v15, p1, v7

    aput v15, v14, v7

    .line 177
    add-int/lit8 v7, v7, -0x1

    goto :goto_4

    .line 179
    :cond_a
    move-object/from16 v0, p0

    iput-object v1, v0, Lorg/icepdf/core/pobjects/graphics/ICCBased;->lastOutput:Lorg/apache/poi/java/awt/Color;

    .line 181
    monitor-exit p0

    goto/16 :goto_1

    .line 182
    .end local v1    # "c":Lorg/apache/poi/java/awt/Color;
    .end local v4    # "fLength":I
    .end local v5    # "frgbvalue":[F
    .end local v6    # "fvalue":[F
    .end local v7    # "i":I
    .end local v9    # "n":I
    .end local v12    # "toCopy":I
    .end local v13    # "value":I
    :catchall_0
    move-exception v14

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v14
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 190
    :catch_0
    move-exception v3

    .line 191
    .local v3, "e":Ljava/lang/Exception;
    sget-object v14, Lorg/icepdf/core/pobjects/graphics/ICCBased;->logger:Ljava/util/logging/Logger;

    sget-object v15, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string/jumbo v16, "Error getting ICCBased colour"

    move-object/from16 v0, v16

    invoke-virtual {v14, v15, v0, v3}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 194
    .end local v3    # "e":Ljava/lang/Exception;
    :cond_b
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/icepdf/core/pobjects/graphics/ICCBased;->alternate:Lorg/icepdf/core/pobjects/graphics/PColorSpace;

    move-object/from16 v0, p1

    invoke-virtual {v14, v0}, Lorg/icepdf/core/pobjects/graphics/PColorSpace;->getColor([F)Lorg/apache/poi/java/awt/Color;

    move-result-object v1

    goto/16 :goto_1
.end method

.method public getColorSpace()I
    .locals 1

    .prologue
    .line 198
    iget v0, p0, Lorg/icepdf/core/pobjects/graphics/ICCBased;->colorSpace:I

    return v0
.end method

.method public getNumComponents()I
    .locals 1

    .prologue
    .line 207
    iget v0, p0, Lorg/icepdf/core/pobjects/graphics/ICCBased;->numcomp:I

    return v0
.end method

.method public declared-synchronized init()V
    .locals 8

    .prologue
    .line 69
    monitor-enter p0

    :try_start_0
    iget-boolean v5, p0, Lorg/icepdf/core/pobjects/graphics/ICCBased;->inited:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    if-eqz v5, :cond_1

    .line 102
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 72
    :cond_1
    const/4 v5, 0x1

    :try_start_1
    iput-boolean v5, p0, Lorg/icepdf/core/pobjects/graphics/ICCBased;->inited:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 73
    const/4 v3, 0x0

    .line 75
    .local v3, "in":Ljava/io/InputStream;
    :try_start_2
    iget-object v5, p0, Lorg/icepdf/core/pobjects/graphics/ICCBased;->stream:Lorg/icepdf/core/pobjects/Stream;

    invoke-virtual {v5}, Lorg/icepdf/core/pobjects/Stream;->init()V

    .line 76
    iget-object v5, p0, Lorg/icepdf/core/pobjects/graphics/ICCBased;->stream:Lorg/icepdf/core/pobjects/Stream;

    invoke-virtual {v5}, Lorg/icepdf/core/pobjects/Stream;->getInputStreamForDecodedStreamBytes()Ljava/io/InputStream;

    move-result-object v3

    .line 77
    sget-object v5, Lorg/icepdf/core/pobjects/graphics/ICCBased;->logger:Ljava/util/logging/Logger;

    sget-object v6, Ljava/util/logging/Level;->FINEST:Ljava/util/logging/Level;

    invoke-virtual {v5, v6}, Ljava/util/logging/Logger;->isLoggable(Ljava/util/logging/Level;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 79
    instance-of v5, v3, Lorg/icepdf/core/io/SeekableInput;

    if-eqz v5, :cond_3

    .line 80
    move-object v0, v3

    check-cast v0, Lorg/icepdf/core/io/SeekableInput;

    move-object v5, v0

    const/4 v6, 0x0

    invoke-static {v5, v6}, Lorg/icepdf/core/util/Utils;->getContentFromSeekableInput(Lorg/icepdf/core/io/SeekableInput;Z)Ljava/lang/String;

    move-result-object v1

    .line 86
    .local v1, "content":Ljava/lang/String;
    :goto_1
    sget-object v5, Lorg/icepdf/core/pobjects/graphics/ICCBased;->logger:Ljava/util/logging/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "Content = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/logging/Logger;->finest(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 97
    .end local v1    # "content":Ljava/lang/String;
    :cond_2
    if-eqz v3, :cond_0

    :try_start_3
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_0

    .line 99
    :catch_0
    move-exception v5

    goto :goto_0

    .line 82
    :cond_3
    const/4 v5, 0x1

    :try_start_4
    new-array v4, v5, [Ljava/io/InputStream;

    const/4 v5, 0x0

    aput-object v3, v4, v5

    .line 83
    .local v4, "inArray":[Ljava/io/InputStream;
    const/4 v5, 0x0

    invoke-static {v4, v5}, Lorg/icepdf/core/util/Utils;->getContentAndReplaceInputStream([Ljava/io/InputStream;Z)Ljava/lang/String;

    move-result-object v1

    .line 84
    .restart local v1    # "content":Ljava/lang/String;
    const/4 v5, 0x0

    aget-object v3, v4, v5
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    .line 92
    .end local v1    # "content":Ljava/lang/String;
    .end local v4    # "inArray":[Ljava/io/InputStream;
    :catch_1
    move-exception v2

    .line 93
    .local v2, "e":Ljava/lang/Exception;
    :try_start_5
    sget-object v5, Lorg/icepdf/core/pobjects/graphics/ICCBased;->logger:Ljava/util/logging/Logger;

    sget-object v6, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string/jumbo v7, "Error Processing ICCBased Colour Profile"

    invoke-virtual {v5, v6, v7, v2}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 97
    if-eqz v3, :cond_0

    :try_start_6
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    goto :goto_0

    .line 99
    :catch_2
    move-exception v5

    goto :goto_0

    .line 96
    .end local v2    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v5

    .line 97
    if-eqz v3, :cond_4

    :try_start_7
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_3
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 100
    :cond_4
    :goto_2
    :try_start_8
    throw v5
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 69
    .end local v3    # "in":Ljava/io/InputStream;
    :catchall_1
    move-exception v5

    monitor-exit p0

    throw v5

    .line 99
    .restart local v3    # "in":Ljava/io/InputStream;
    :catch_3
    move-exception v6

    goto :goto_2
.end method
