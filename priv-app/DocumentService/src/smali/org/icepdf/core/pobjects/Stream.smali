.class public Lorg/icepdf/core/pobjects/Stream;
.super Lorg/icepdf/core/pobjects/Dictionary;
.source "Stream.java"


# static fields
.field private static final GRAY_1_BIT_INDEX_TO_RGB:[I

.field private static final GRAY_1_BIT_INDEX_TO_RGB_REVERSED:[I

.field private static forceJaiccittfax:Z

.field private static final logger:Ljava/util/logging/Logger;

.field private static pageRatio:D

.field private static scaleImages:Z


# instance fields
.field private image:Lorg/icepdf/core/util/ImageCache;

.field private final imageLock:Ljava/lang/Object;

.field private inlineImage:Z

.field private pObjectReference:Lorg/icepdf/core/pobjects/Reference;

.field private streamInput:Lorg/icepdf/core/io/SeekableInputConstrainedWrapper;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    .line 67
    const-class v0, Lorg/icepdf/core/pobjects/Stream;

    invoke-virtual {v0}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Lorg/icepdf/core/pobjects/Stream;->logger:Ljava/util/logging/Logger;

    .line 98
    const-string/jumbo v0, "org.icepdf.core.scaleImages"

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lorg/icepdf/core/util/Defs;->sysPropertyBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lorg/icepdf/core/pobjects/Stream;->scaleImages:Z

    .line 110
    const-string/jumbo v0, "org.icepdf.core.pageRatio"

    const-wide v2, 0x3fe6a150a8542a15L    # 0.7071917808219178

    invoke-static {v0, v2, v3}, Lorg/icepdf/core/util/Defs;->sysPropertyDouble(Ljava/lang/String;D)D

    move-result-wide v0

    sput-wide v0, Lorg/icepdf/core/pobjects/Stream;->pageRatio:D

    .line 115
    const-string/jumbo v0, "org.icepdf.core.ccittfax.jai"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lorg/icepdf/core/util/Defs;->sysPropertyBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lorg/icepdf/core/pobjects/Stream;->forceJaiccittfax:Z

    .line 120
    new-array v0, v4, [I

    fill-array-data v0, :array_0

    sput-object v0, Lorg/icepdf/core/pobjects/Stream;->GRAY_1_BIT_INDEX_TO_RGB_REVERSED:[I

    .line 125
    new-array v0, v4, [I

    fill-array-data v0, :array_1

    sput-object v0, Lorg/icepdf/core/pobjects/Stream;->GRAY_1_BIT_INDEX_TO_RGB:[I

    return-void

    .line 120
    :array_0
    .array-data 4
        -0x1
        -0x1000000
    .end array-data

    .line 125
    :array_1
    .array-data 4
        -0x1000000
        -0x1
    .end array-data
.end method

.method public constructor <init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V
    .locals 2
    .param p1, "library"    # Lorg/icepdf/core/util/Library;
    .param p2, "entries"    # Ljava/util/Hashtable;

    .prologue
    const/4 v1, 0x0

    .line 1008
    invoke-direct {p0, p1, p2}, Lorg/icepdf/core/pobjects/Dictionary;-><init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V

    .line 74
    iput-object v1, p0, Lorg/icepdf/core/pobjects/Stream;->image:Lorg/icepdf/core/util/ImageCache;

    .line 75
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lorg/icepdf/core/pobjects/Stream;->imageLock:Ljava/lang/Object;

    .line 78
    iput-object v1, p0, Lorg/icepdf/core/pobjects/Stream;->pObjectReference:Lorg/icepdf/core/pobjects/Reference;

    .line 1009
    return-void
.end method

.method public constructor <init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;Lorg/icepdf/core/io/SeekableInputConstrainedWrapper;)V
    .locals 2
    .param p1, "l"    # Lorg/icepdf/core/util/Library;
    .param p2, "h"    # Ljava/util/Hashtable;
    .param p3, "streamInputWrapper"    # Lorg/icepdf/core/io/SeekableInputConstrainedWrapper;

    .prologue
    const/4 v1, 0x0

    .line 180
    invoke-direct {p0, p1, p2}, Lorg/icepdf/core/pobjects/Dictionary;-><init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V

    .line 74
    iput-object v1, p0, Lorg/icepdf/core/pobjects/Stream;->image:Lorg/icepdf/core/util/ImageCache;

    .line 75
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lorg/icepdf/core/pobjects/Stream;->imageLock:Ljava/lang/Object;

    .line 78
    iput-object v1, p0, Lorg/icepdf/core/pobjects/Stream;->pObjectReference:Lorg/icepdf/core/pobjects/Reference;

    .line 181
    iput-object p3, p0, Lorg/icepdf/core/pobjects/Stream;->streamInput:Lorg/icepdf/core/io/SeekableInputConstrainedWrapper;

    .line 182
    return-void
.end method

.method private static alterBufferedImage(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;[I[I)Landroid/graphics/Bitmap;
    .locals 49
    .param p0, "bi"    # Landroid/graphics/Bitmap;
    .param p1, "smaskImage"    # Landroid/graphics/Bitmap;
    .param p2, "maskImage"    # Landroid/graphics/Bitmap;
    .param p3, "maskMinRGB"    # [I
    .param p4, "maskMaxRGB"    # [I

    .prologue
    .line 1737
    const/16 v46, 0x0

    .line 1738
    .local v46, "smaskWidth":I
    const/16 v45, 0x0

    .line 1740
    .local v45, "smaskHeight":I
    invoke-virtual/range {p0 .. p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v13

    .line 1741
    .local v13, "width":I
    invoke-virtual/range {p0 .. p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v31

    .line 1743
    .local v31, "height":I
    if-eqz p1, :cond_2

    .line 1744
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v46

    .line 1745
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v45

    .line 1747
    move/from16 v0, v46

    if-lt v13, v0, :cond_0

    move/from16 v0, v31

    move/from16 v1, v45

    if-ge v0, v1, :cond_1

    .line 1749
    :cond_0
    move/from16 v0, v46

    int-to-float v4, v0

    int-to-float v5, v13

    div-float v43, v4, v5

    .line 1750
    .local v43, "scaleX":F
    move/from16 v0, v45

    int-to-float v4, v0

    move/from16 v0, v31

    int-to-float v5, v0

    div-float v44, v4, v5

    .line 1752
    .local v44, "scaleY":F
    new-instance v9, Landroid/graphics/Matrix;

    invoke-direct {v9}, Landroid/graphics/Matrix;-><init>()V

    .line 1753
    .local v9, "tx":Landroid/graphics/Matrix;
    move/from16 v0, v43

    move/from16 v1, v44

    invoke-virtual {v9, v0, v1}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 1754
    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {p0 .. p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v7

    invoke-virtual/range {p0 .. p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v8

    const/4 v10, 0x1

    move-object/from16 v4, p0

    invoke-static/range {v4 .. v10}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v27

    .line 1757
    .local v27, "b":Landroid/graphics/Bitmap;
    move-object/from16 p0, v27

    .line 1759
    .end local v9    # "tx":Landroid/graphics/Matrix;
    .end local v27    # "b":Landroid/graphics/Bitmap;
    .end local v43    # "scaleX":F
    .end local v44    # "scaleY":F
    :cond_1
    invoke-virtual/range {p0 .. p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v13

    .line 1760
    invoke-virtual/range {p0 .. p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v31

    .line 1763
    :cond_2
    const/16 v40, 0x0

    .line 1764
    .local v40, "maskWidth":I
    const/16 v33, 0x0

    .line 1765
    .local v33, "maskHeight":I
    if-eqz p2, :cond_3

    .line 1766
    invoke-virtual/range {p2 .. p2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v40

    .line 1767
    invoke-virtual/range {p2 .. p2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v33

    .line 1770
    :cond_3
    const/16 v39, 0xff

    .line 1771
    .local v39, "maskMinRed":I
    const/16 v38, 0xff

    .line 1772
    .local v38, "maskMinGreen":I
    const/16 v37, 0xff

    .line 1773
    .local v37, "maskMinBlue":I
    const/16 v36, 0x0

    .line 1774
    .local v36, "maskMaxRed":I
    const/16 v35, 0x0

    .line 1775
    .local v35, "maskMaxGreen":I
    const/16 v34, 0x0

    .line 1776
    .local v34, "maskMaxBlue":I
    if-eqz p3, :cond_4

    if-eqz p4, :cond_4

    .line 1777
    const/4 v4, 0x0

    aget v39, p3, v4

    .line 1778
    const/4 v4, 0x1

    aget v38, p3, v4

    .line 1779
    const/4 v4, 0x2

    aget v37, p3, v4

    .line 1780
    const/4 v4, 0x0

    aget v36, p4, v4

    .line 1781
    const/4 v4, 0x1

    aget v35, p4, v4

    .line 1782
    const/4 v4, 0x2

    aget v34, p4, v4

    .line 1785
    :cond_4
    if-nez p1, :cond_7

    if-nez p2, :cond_7

    if-eqz p3, :cond_5

    if-nez p4, :cond_7

    .line 1787
    :cond_5
    const/4 v10, 0x0

    .line 1861
    :cond_6
    :goto_0
    return-object v10

    .line 1790
    :cond_7
    const/16 v48, 0x0

    .local v48, "y":I
    :goto_1
    move/from16 v0, v48

    move/from16 v1, v31

    if-ge v0, v1, :cond_e

    .line 1791
    const/16 v47, 0x0

    .local v47, "x":I
    :goto_2
    move/from16 v0, v47

    if-ge v0, v13, :cond_d

    .line 1792
    const/16 v29, 0x0

    .line 1793
    .local v29, "gotARBG":Z
    const/16 v25, 0x0

    .line 1794
    .local v25, "argb":I
    const/16 v24, 0xff

    .line 1795
    .local v24, "alpha":I
    move/from16 v0, v48

    move/from16 v1, v45

    if-ge v0, v1, :cond_b

    move/from16 v0, v47

    move/from16 v1, v46

    if-ge v0, v1, :cond_b

    if-eqz p1, :cond_b

    .line 1797
    move-object/from16 v0, p1

    move/from16 v1, v47

    move/from16 v2, v48

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v4

    and-int/lit16 v0, v4, 0xff

    move/from16 v24, v0

    .line 1816
    :cond_8
    :goto_3
    const/16 v4, 0xff

    move/from16 v0, v24

    if-eq v0, v4, :cond_a

    .line 1817
    if-nez v29, :cond_9

    .line 1818
    move-object/from16 v0, p0

    move/from16 v1, v47

    move/from16 v2, v48

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v25

    .line 1819
    :cond_9
    const v4, 0xffffff

    and-int v25, v25, v4

    .line 1820
    shl-int/lit8 v4, v24, 0x18

    const/high16 v5, -0x1000000

    and-int/2addr v4, v5

    or-int v25, v25, v4

    .line 1821
    move-object/from16 v0, p0

    move/from16 v1, v47

    move/from16 v2, v48

    move/from16 v3, v25

    invoke-virtual {v0, v1, v2, v3}, Landroid/graphics/Bitmap;->setPixel(III)V

    .line 1791
    :cond_a
    add-int/lit8 v47, v47, 0x1

    goto :goto_2

    .line 1798
    :cond_b
    move/from16 v0, v48

    move/from16 v1, v33

    if-ge v0, v1, :cond_c

    move/from16 v0, v47

    move/from16 v1, v40

    if-ge v0, v1, :cond_c

    if-eqz p2, :cond_c

    .line 1803
    move-object/from16 v0, p2

    move/from16 v1, v47

    move/from16 v2, v48

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v4

    ushr-int/lit8 v4, v4, 0x18

    and-int/lit16 v0, v4, 0xff

    move/from16 v24, v0

    goto :goto_3

    .line 1805
    :cond_c
    const/16 v29, 0x1

    .line 1806
    move-object/from16 v0, p0

    move/from16 v1, v47

    move/from16 v2, v48

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v25

    .line 1807
    shr-int/lit8 v4, v25, 0x10

    and-int/lit16 v0, v4, 0xff

    move/from16 v41, v0

    .line 1808
    .local v41, "red":I
    shr-int/lit8 v4, v25, 0x8

    and-int/lit16 v0, v4, 0xff

    move/from16 v30, v0

    .line 1809
    .local v30, "green":I
    move/from16 v0, v25

    and-int/lit16 v0, v0, 0xff

    move/from16 v28, v0

    .line 1810
    .local v28, "blue":I
    move/from16 v0, v28

    move/from16 v1, v37

    if-lt v0, v1, :cond_8

    move/from16 v0, v28

    move/from16 v1, v34

    if-gt v0, v1, :cond_8

    move/from16 v0, v30

    move/from16 v1, v38

    if-lt v0, v1, :cond_8

    move/from16 v0, v30

    move/from16 v1, v35

    if-gt v0, v1, :cond_8

    move/from16 v0, v41

    move/from16 v1, v39

    if-lt v0, v1, :cond_8

    move/from16 v0, v41

    move/from16 v1, v36

    if-gt v0, v1, :cond_8

    .line 1813
    const/16 v24, 0x0

    goto/16 :goto_3

    .line 1790
    .end local v24    # "alpha":I
    .end local v25    # "argb":I
    .end local v28    # "blue":I
    .end local v29    # "gotARBG":Z
    .end local v30    # "green":I
    .end local v41    # "red":I
    :cond_d
    add-int/lit8 v48, v48, 0x1

    goto/16 :goto_1

    .line 1839
    .end local v47    # "x":I
    :cond_e
    move-object/from16 v10, p0

    .line 1840
    .local v10, "tmpImage":Landroid/graphics/Bitmap;
    if-eqz p1, :cond_6

    .line 1841
    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    move/from16 v0, v31

    invoke-static {v13, v0, v4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v26

    .line 1842
    .local v26, "argbImage":Landroid/graphics/Bitmap;
    new-array v11, v13, [I

    .line 1843
    .local v11, "srcBand":[I
    new-array v0, v13, [I

    move-object/from16 v42, v0

    .line 1845
    .local v42, "sMaskBand":[I
    const/4 v15, 0x0

    .local v15, "i":I
    :goto_4
    move/from16 v0, v31

    if-ge v15, v0, :cond_10

    .line 1848
    const/4 v12, 0x0

    const/4 v14, 0x0

    const/16 v17, 0x1

    move/from16 v16, v13

    invoke-virtual/range {v10 .. v17}, Landroid/graphics/Bitmap;->getPixels([IIIIIII)V

    .line 1849
    const/16 v18, 0x0

    const/16 v20, 0x0

    const/16 v23, 0x1

    move-object/from16 v16, p1

    move-object/from16 v17, v42

    move/from16 v19, v13

    move/from16 v21, v15

    move/from16 v22, v13

    invoke-virtual/range {v16 .. v23}, Landroid/graphics/Bitmap;->getPixels([IIIIIII)V

    .line 1851
    const/16 v32, 0x0

    .local v32, "j":I
    :goto_5
    move/from16 v0, v32

    if-ge v0, v13, :cond_f

    .line 1852
    aget v4, v42, v32

    and-int/lit16 v4, v4, 0xff

    shl-int/lit8 v4, v4, 0x18

    aget v5, v11, v32

    const v6, 0xffffff

    and-int/2addr v5, v6

    or-int/2addr v4, v5

    aput v4, v42, v32

    .line 1851
    add-int/lit8 v32, v32, 0x1

    goto :goto_5

    .line 1856
    :cond_f
    const/16 v18, 0x0

    const/16 v20, 0x0

    const/16 v23, 0x1

    move-object/from16 v16, v26

    move-object/from16 v17, v42

    move/from16 v19, v13

    move/from16 v21, v15

    move/from16 v22, v13

    invoke-virtual/range {v16 .. v23}, Landroid/graphics/Bitmap;->setPixels([IIIIIII)V

    .line 1845
    add-int/lit8 v15, v15, 0x1

    goto :goto_4

    .line 1859
    .end local v32    # "j":I
    :cond_10
    move-object/from16 v10, v26

    goto/16 :goto_0
.end method

.method private static applyExplicitMask(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 20
    .param p0, "baseImage"    # Landroid/graphics/Bitmap;
    .param p1, "maskImage"    # Landroid/graphics/Bitmap;

    .prologue
    .line 1611
    invoke-virtual/range {p0 .. p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v11

    .line 1612
    .local v11, "baseWidth":I
    invoke-virtual/range {p0 .. p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v10

    .line 1614
    .local v10, "baseHeight":I
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v14

    .line 1615
    .local v14, "maskWidth":I
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v12

    .line 1619
    .local v12, "maskHeight":I
    if-ne v11, v14, :cond_0

    if-eq v10, v12, :cond_1

    .line 1621
    :cond_0
    int-to-float v3, v14

    int-to-float v4, v11

    div-float v16, v3, v4

    .line 1622
    .local v16, "scaleX":F
    int-to-float v3, v12

    int-to-float v4, v10

    div-float v17, v3, v4

    .line 1624
    .local v17, "scaleY":F
    new-instance v8, Landroid/graphics/Matrix;

    invoke-direct {v8}, Landroid/graphics/Matrix;-><init>()V

    .line 1625
    .local v8, "tx":Landroid/graphics/Matrix;
    move/from16 v0, v16

    move/from16 v1, v17

    invoke-virtual {v8, v0, v1}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 1629
    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {p0 .. p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    invoke-virtual/range {p0 .. p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    const/4 v9, 0x1

    move-object/from16 v3, p0

    invoke-static/range {v3 .. v9}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v15

    .line 1630
    .local v15, "sbim":Landroid/graphics/Bitmap;
    move-object/from16 p0, v15

    .line 1635
    .end local v8    # "tx":Landroid/graphics/Matrix;
    .end local v15    # "sbim":Landroid/graphics/Bitmap;
    .end local v16    # "scaleX":F
    .end local v17    # "scaleY":F
    :cond_1
    const/16 v19, 0x0

    .local v19, "y":I
    :goto_0
    move/from16 v0, v19

    if-ge v0, v12, :cond_5

    .line 1636
    const/16 v18, 0x0

    .local v18, "x":I
    :goto_1
    move/from16 v0, v18

    if-ge v0, v14, :cond_4

    .line 1638
    move-object/from16 v0, p1

    move/from16 v1, v18

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v13

    .line 1639
    .local v13, "maskPixel":I
    const/4 v3, -0x1

    if-eq v13, v3, :cond_2

    const v3, 0xffffff

    if-eq v13, v3, :cond_2

    if-nez v13, :cond_3

    .line 1640
    :cond_2
    sget-object v3, Lorg/apache/poi/java/awt/Color;->WHITE:Lorg/apache/poi/java/awt/Color;

    invoke-virtual {v3}, Lorg/apache/poi/java/awt/Color;->getRGB()I

    move-result v3

    move-object/from16 v0, p0

    move/from16 v1, v18

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2, v3}, Landroid/graphics/Bitmap;->setPixel(III)V

    .line 1636
    :cond_3
    add-int/lit8 v18, v18, 0x1

    goto :goto_1

    .line 1635
    .end local v13    # "maskPixel":I
    :cond_4
    add-int/lit8 v19, v19, 0x1

    goto :goto_0

    .line 1644
    .end local v18    # "x":I
    :cond_5
    return-object p0
.end method

.method private ccittFaxDecode(II)[B
    .locals 17
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    .line 2215
    invoke-direct/range {p0 .. p0}, Lorg/icepdf/core/pobjects/Stream;->getDecodedStreamBytes()[B

    move-result-object v13

    .line 2216
    .local v13, "streamData":[B
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/icepdf/core/pobjects/Stream;->library:Lorg/icepdf/core/util/Library;

    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/icepdf/core/pobjects/Stream;->entries:Ljava/util/Hashtable;

    const-string/jumbo v16, "DecodeParms"

    invoke-virtual/range {v14 .. v16}, Lorg/icepdf/core/util/Library;->getDictionary(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/util/Hashtable;

    move-result-object v3

    .line 2217
    .local v3, "decodeParms":Ljava/util/Hashtable;
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/icepdf/core/pobjects/Stream;->library:Lorg/icepdf/core/util/Library;

    const-string/jumbo v15, "K"

    invoke-virtual {v14, v3, v15}, Lorg/icepdf/core/util/Library;->getFloat(Ljava/util/Hashtable;Ljava/lang/String;)F

    move-result v10

    .line 2219
    .local v10, "k":F
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/icepdf/core/pobjects/Stream;->library:Lorg/icepdf/core/util/Library;

    move-object/from16 v0, p0

    invoke-virtual {v0, v14, v3}, Lorg/icepdf/core/pobjects/Stream;->getBlackIs1(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)Z

    move-result v1

    .line 2221
    .local v1, "blackIs1":Z
    const/4 v7, 0x0

    .line 2222
    .local v7, "encodedByteAlign":Z
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/icepdf/core/pobjects/Stream;->library:Lorg/icepdf/core/util/Library;

    const-string/jumbo v15, "EncodedByteAlign"

    invoke-virtual {v14, v3, v15}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v8

    .line 2223
    .local v8, "encodedByteAlignObject":Ljava/lang/Object;
    instance-of v14, v8, Ljava/lang/Boolean;

    if-eqz v14, :cond_0

    .line 2224
    check-cast v8, Ljava/lang/Boolean;

    .end local v8    # "encodedByteAlignObject":Ljava/lang/Object;
    invoke-virtual {v8}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v7

    .line 2226
    :cond_0
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/icepdf/core/pobjects/Stream;->library:Lorg/icepdf/core/util/Library;

    const-string/jumbo v15, "Columns"

    invoke-virtual {v14, v3, v15}, Lorg/icepdf/core/util/Library;->getInt(Ljava/util/Hashtable;Ljava/lang/String;)I

    move-result v2

    .line 2227
    .local v2, "columns":I
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/icepdf/core/pobjects/Stream;->library:Lorg/icepdf/core/util/Library;

    const-string/jumbo v15, "Rows"

    invoke-virtual {v14, v3, v15}, Lorg/icepdf/core/util/Library;->getInt(Ljava/util/Hashtable;Ljava/lang/String;)I

    move-result v11

    .line 2229
    .local v11, "rows":I
    if-nez v2, :cond_1

    .line 2230
    move/from16 v2, p1

    .line 2232
    :cond_1
    if-nez v11, :cond_2

    .line 2233
    move/from16 v11, p2

    .line 2235
    :cond_2
    add-int/lit8 v14, v2, 0x7

    shr-int/lit8 v14, v14, 0x3

    mul-int v12, v11, v14

    .line 2236
    .local v12, "size":I
    new-array v4, v12, [B

    .line 2237
    .local v4, "decodedStreamData":[B
    new-instance v5, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;

    const/4 v14, 0x1

    invoke-direct {v5, v14, v2, v11}, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;-><init>(III)V

    .line 2238
    .local v5, "decoder":Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;
    invoke-virtual {v5, v7}, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->setAlign(Z)V

    .line 2241
    if-eqz v13, :cond_3

    .line 2244
    const/4 v14, 0x0

    cmpl-float v14, v10, v14

    if-nez v14, :cond_4

    .line 2245
    const/4 v14, 0x0

    :try_start_0
    invoke-virtual {v5, v4, v13, v14, v11}, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->decodeT41D([B[BII)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2259
    :cond_3
    :goto_0
    if-nez v1, :cond_6

    .line 2261
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_1
    array-length v14, v4

    if-ge v9, v14, :cond_6

    .line 2262
    aget-byte v14, v4, v9

    xor-int/lit8 v14, v14, -0x1

    int-to-byte v14, v14

    aput-byte v14, v4, v9

    .line 2261
    add-int/lit8 v9, v9, 0x1

    goto :goto_1

    .line 2246
    .end local v9    # "i":I
    :cond_4
    const/4 v14, 0x0

    cmpl-float v14, v10, v14

    if-lez v14, :cond_5

    .line 2247
    const/4 v14, 0x0

    :try_start_1
    invoke-virtual {v5, v4, v13, v14, v11}, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->decodeT42D([B[BII)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 2251
    :catch_0
    move-exception v6

    .line 2252
    .local v6, "e":Ljava/lang/Exception;
    sget-object v14, Lorg/icepdf/core/pobjects/Stream;->logger:Ljava/util/logging/Logger;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v16, "Error decoding CCITTFax image k: "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v10}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/util/logging/Logger;->warning(Ljava/lang/String;)V

    .line 2255
    const/4 v14, 0x0

    invoke-virtual {v5, v4, v13, v14, v11}, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->decodeT6([B[BII)V

    goto :goto_0

    .line 2248
    .end local v6    # "e":Ljava/lang/Exception;
    :cond_5
    const/4 v14, 0x0

    cmpg-float v14, v10, v14

    if-gez v14, :cond_3

    .line 2249
    const/4 v14, 0x0

    :try_start_2
    invoke-virtual {v5, v4, v13, v14, v11}, Lorg/icepdf/core/pobjects/filters/CCITTFaxDecoder;->decodeT6([B[BII)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    .line 2265
    :cond_6
    return-object v4
.end method

.method private checkMemory(I)Z
    .locals 1
    .param p1, "memoryNeeded"    # I

    .prologue
    .line 233
    iget-object v0, p0, Lorg/icepdf/core/pobjects/Stream;->library:Lorg/icepdf/core/util/Library;

    iget-object v0, v0, Lorg/icepdf/core/util/Library;->memoryManager:Lorg/icepdf/core/util/MemoryManager;

    invoke-virtual {v0, p1}, Lorg/icepdf/core/util/MemoryManager;->checkMemory(I)Z

    move-result v0

    return v0
.end method

.method private containsFilter([Ljava/lang/String;)Z
    .locals 9
    .param p1, "searchFilterNames"    # [Ljava/lang/String;

    .prologue
    const/4 v7, 0x0

    .line 516
    invoke-direct {p0}, Lorg/icepdf/core/pobjects/Stream;->getFilterNames()Ljava/util/Vector;

    move-result-object v2

    .line 517
    .local v2, "filterNames":Ljava/util/Vector;
    if-nez v2, :cond_1

    .line 527
    :cond_0
    :goto_0
    return v7

    .line 519
    :cond_1
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    invoke-virtual {v2}, Ljava/util/Vector;->size()I

    move-result v8

    if-ge v3, v8, :cond_0

    .line 520
    invoke-virtual {v2, v3}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 521
    .local v1, "filterName":Ljava/lang/String;
    move-object v0, p1

    .local v0, "arr$":[Ljava/lang/String;
    array-length v5, v0

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_2
    if-ge v4, v5, :cond_3

    aget-object v6, v0, v4

    .line 522
    .local v6, "search":Ljava/lang/String;
    invoke-virtual {v6, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 523
    const/4 v7, 0x1

    goto :goto_0

    .line 521
    :cond_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 519
    .end local v6    # "search":Ljava/lang/String;
    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_1
.end method

.method private copyDecodedStreamBytesIntoRGB([I)V
    .locals 13
    .param p1, "pixels"    # [I

    .prologue
    const/4 v12, 0x2

    const/4 v11, 0x1

    const/4 v10, 0x3

    .line 458
    new-array v6, v10, [B

    .line 460
    .local v6, "rgb":[B
    const/4 v4, 0x0

    .line 462
    .local v4, "input":Ljava/io/InputStream;
    :try_start_0
    invoke-virtual {p0}, Lorg/icepdf/core/pobjects/Stream;->getInputStreamForDecodedStreamBytes()Ljava/io/InputStream;

    move-result-object v4

    .line 463
    const/4 v5, 0x0

    .local v5, "pixelIndex":I
    :goto_0
    array-length v8, p1

    if-ge v5, v8, :cond_5

    .line 464
    const/high16 v0, -0x1000000

    .line 465
    .local v0, "argb":I
    if-eqz v4, :cond_3

    .line 466
    const/4 v7, 0x3

    .line 467
    .local v7, "toRead":I
    const/4 v3, 0x0

    .line 468
    .local v3, "haveRead":I
    :goto_1
    if-ge v3, v10, :cond_0

    .line 469
    rsub-int/lit8 v8, v3, 0x3

    invoke-virtual {v4, v6, v3, v8}, Ljava/io/InputStream;->read([BII)I

    move-result v1

    .line 470
    .local v1, "currRead":I
    if-gez v1, :cond_4

    .line 474
    .end local v1    # "currRead":I
    :cond_0
    if-lt v3, v11, :cond_1

    .line 475
    const/4 v8, 0x0

    aget-byte v8, v6, v8

    shl-int/lit8 v8, v8, 0x10

    const/high16 v9, 0xff0000

    and-int/2addr v8, v9

    or-int/2addr v0, v8

    .line 476
    :cond_1
    if-lt v3, v12, :cond_2

    .line 477
    const/4 v8, 0x1

    aget-byte v8, v6, v8

    shl-int/lit8 v8, v8, 0x8

    const v9, 0xff00

    and-int/2addr v8, v9

    or-int/2addr v0, v8

    .line 478
    :cond_2
    if-lt v3, v10, :cond_3

    .line 479
    const/4 v8, 0x2

    aget-byte v8, v6, v8

    and-int/lit16 v8, v8, 0xff

    or-int/2addr v0, v8

    .line 481
    .end local v3    # "haveRead":I
    .end local v7    # "toRead":I
    :cond_3
    aput v0, p1, v5

    .line 463
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 472
    .restart local v1    # "currRead":I
    .restart local v3    # "haveRead":I
    .restart local v7    # "toRead":I
    :cond_4
    add-int/2addr v3, v1

    .line 473
    goto :goto_1

    .line 483
    .end local v0    # "argb":I
    .end local v1    # "currRead":I
    .end local v3    # "haveRead":I
    .end local v7    # "toRead":I
    :cond_5
    if-eqz v4, :cond_6

    .line 484
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 489
    :cond_6
    if-eqz v4, :cond_7

    .line 491
    :try_start_1
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 497
    .end local v5    # "pixelIndex":I
    :cond_7
    :goto_2
    return-void

    .line 492
    .restart local v5    # "pixelIndex":I
    :catch_0
    move-exception v2

    .line 493
    .local v2, "e":Ljava/io/IOException;
    const-string/jumbo v8, "DocumentService"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "Exception: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v2}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 485
    .end local v2    # "e":Ljava/io/IOException;
    .end local v5    # "pixelIndex":I
    :catch_1
    move-exception v2

    .line 486
    .restart local v2    # "e":Ljava/io/IOException;
    :try_start_2
    sget-object v8, Lorg/icepdf/core/pobjects/Stream;->logger:Ljava/util/logging/Logger;

    sget-object v9, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string/jumbo v10, "Problem copying decoding stream bytes: "

    invoke-virtual {v8, v9, v10, v2}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 489
    if-eqz v4, :cond_7

    .line 491
    :try_start_3
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_2

    .line 492
    :catch_2
    move-exception v2

    .line 493
    const-string/jumbo v8, "DocumentService"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "Exception: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v2}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 489
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v8

    if-eqz v4, :cond_8

    .line 491
    :try_start_4
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    .line 494
    :cond_8
    :goto_3
    throw v8

    .line 492
    :catch_3
    move-exception v2

    .line 493
    .restart local v2    # "e":Ljava/io/IOException;
    const-string/jumbo v9, "DocumentService"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "Exception: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v2}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3
.end method

.method private dctDecode(IILorg/icepdf/core/pobjects/graphics/PColorSpace;ILandroid/graphics/Bitmap;Landroid/graphics/Bitmap;[I[ILjava/util/Vector;)Landroid/graphics/Bitmap;
    .locals 20
    .param p1, "width"    # I
    .param p2, "height"    # I
    .param p3, "colourSpace"    # Lorg/icepdf/core/pobjects/graphics/PColorSpace;
    .param p4, "bitspercomponent"    # I
    .param p5, "smaskImage"    # Landroid/graphics/Bitmap;
    .param p6, "maskImage"    # Landroid/graphics/Bitmap;
    .param p7, "maskMinRGB"    # [I
    .param p8, "maskMaxRGB"    # [I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Lorg/icepdf/core/pobjects/graphics/PColorSpace;",
            "I",
            "Landroid/graphics/Bitmap;",
            "Landroid/graphics/Bitmap;",
            "[I[I",
            "Ljava/util/Vector",
            "<",
            "Ljava/lang/Integer;",
            ">;)",
            "Landroid/graphics/Bitmap;"
        }
    .end annotation

    .prologue
    .line 634
    .local p9, "decode":Ljava/util/Vector;, "Ljava/util/Vector<Ljava/lang/Integer;>;"
    invoke-virtual/range {p0 .. p0}, Lorg/icepdf/core/pobjects/Stream;->getInputStreamForDecodedStreamBytes()Ljava/io/InputStream;

    move-result-object v17

    .line 635
    .local v17, "input":Ljava/io/InputStream;
    invoke-static/range {v17 .. v17}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 638
    .local v2, "bi":Landroid/graphics/Bitmap;
    if-eqz p5, :cond_2

    .line 639
    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    move/from16 v0, p1

    move/from16 v1, p2

    invoke-static {v0, v1, v4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v16

    .line 640
    .local v16, "argbImage":Landroid/graphics/Bitmap;
    move/from16 v0, p1

    new-array v3, v0, [I

    .line 641
    .local v3, "srcBand":[I
    move/from16 v0, p1

    new-array v0, v0, [I

    move-object/from16 v19, v0

    .line 643
    .local v19, "sMaskBand":[I
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_0
    move/from16 v0, p2

    if-ge v7, v0, :cond_1

    .line 644
    const/4 v4, 0x0

    const/4 v6, 0x0

    const/4 v9, 0x1

    move/from16 v5, p1

    move/from16 v8, p1

    invoke-virtual/range {v2 .. v9}, Landroid/graphics/Bitmap;->getPixels([IIIIIII)V

    .line 645
    const/4 v10, 0x0

    const/4 v12, 0x0

    const/4 v15, 0x1

    move-object/from16 v8, p5

    move-object/from16 v9, v19

    move/from16 v11, p1

    move v13, v7

    move/from16 v14, p1

    invoke-virtual/range {v8 .. v15}, Landroid/graphics/Bitmap;->getPixels([IIIIIII)V

    .line 647
    const/16 v18, 0x0

    .local v18, "j":I
    :goto_1
    move/from16 v0, v18

    move/from16 v1, p1

    if-ge v0, v1, :cond_0

    .line 648
    aget v4, v19, v18

    and-int/lit16 v4, v4, 0xff

    shl-int/lit8 v4, v4, 0x18

    aget v5, v3, v18

    const v6, 0xffffff

    and-int/2addr v5, v6

    or-int/2addr v4, v5

    aput v4, v19, v18

    .line 647
    add-int/lit8 v18, v18, 0x1

    goto :goto_1

    .line 651
    :cond_0
    const/4 v10, 0x0

    const/4 v12, 0x0

    const/4 v15, 0x1

    move-object/from16 v8, v16

    move-object/from16 v9, v19

    move/from16 v11, p1

    move v13, v7

    move/from16 v14, p1

    invoke-virtual/range {v8 .. v15}, Landroid/graphics/Bitmap;->setPixels([IIIIIII)V

    .line 643
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 654
    .end local v18    # "j":I
    :cond_1
    move-object/from16 v2, v16

    .line 656
    .end local v3    # "srcBand":[I
    .end local v7    # "i":I
    .end local v16    # "argbImage":Landroid/graphics/Bitmap;
    .end local v19    # "sMaskBand":[I
    :cond_2
    return-object v2
.end method

.method private getDecodedStreamBytes()[B
    .locals 12

    .prologue
    const/4 v7, 0x0

    const/16 v8, 0x400

    .line 346
    invoke-virtual {p0}, Lorg/icepdf/core/pobjects/Stream;->getInputStreamForDecodedStreamBytes()Ljava/io/InputStream;

    move-result-object v2

    .line 348
    .local v2, "input":Ljava/io/InputStream;
    const/4 v3, 0x0

    .line 349
    .local v3, "out":Lorg/icepdf/core/io/ConservativeSizingByteArrayOutputStream;
    if-nez v2, :cond_1

    .line 391
    :cond_0
    :goto_0
    return-object v7

    .line 352
    :cond_1
    const/16 v9, 0x400

    :try_start_0
    iget-object v10, p0, Lorg/icepdf/core/pobjects/Stream;->streamInput:Lorg/icepdf/core/io/SeekableInputConstrainedWrapper;

    invoke-virtual {v10}, Lorg/icepdf/core/io/SeekableInputConstrainedWrapper;->getLength()J

    move-result-wide v10

    long-to-int v10, v10

    invoke-static {v9, v10}, Ljava/lang/Math;->max(II)I

    move-result v5

    .line 353
    .local v5, "outLength":I
    new-instance v4, Lorg/icepdf/core/io/ConservativeSizingByteArrayOutputStream;

    iget-object v9, p0, Lorg/icepdf/core/pobjects/Stream;->library:Lorg/icepdf/core/util/Library;

    iget-object v9, v9, Lorg/icepdf/core/util/Library;->memoryManager:Lorg/icepdf/core/util/MemoryManager;

    invoke-direct {v4, v5, v9}, Lorg/icepdf/core/io/ConservativeSizingByteArrayOutputStream;-><init>(ILorg/icepdf/core/util/MemoryManager;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_7
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 355
    .end local v3    # "out":Lorg/icepdf/core/io/ConservativeSizingByteArrayOutputStream;
    .local v4, "out":Lorg/icepdf/core/io/ConservativeSizingByteArrayOutputStream;
    if-le v5, v8, :cond_2

    const/16 v8, 0x1000

    :cond_2
    :try_start_1
    new-array v0, v8, [B

    .line 357
    .local v0, "buffer":[B
    :goto_1
    invoke-virtual {v2, v0}, Ljava/io/InputStream;->read([B)I

    move-result v6

    .line 358
    .local v6, "read":I
    if-gtz v6, :cond_5

    .line 362
    invoke-virtual {v4}, Lorg/icepdf/core/io/ConservativeSizingByteArrayOutputStream;->flush()V

    .line 363
    invoke-virtual {v4}, Lorg/icepdf/core/io/ConservativeSizingByteArrayOutputStream;->close()V

    .line 365
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    .line 367
    invoke-virtual {v4}, Lorg/icepdf/core/io/ConservativeSizingByteArrayOutputStream;->toByteArray()[B
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v7

    .line 373
    .local v7, "ret":[B
    if-eqz v2, :cond_3

    .line 375
    :try_start_2
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    .line 381
    :cond_3
    :goto_2
    if-eqz v4, :cond_4

    .line 383
    :try_start_3
    invoke-virtual {v4}, Lorg/icepdf/core/io/ConservativeSizingByteArrayOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3

    :cond_4
    :goto_3
    move-object v3, v4

    .line 386
    .end local v4    # "out":Lorg/icepdf/core/io/ConservativeSizingByteArrayOutputStream;
    .restart local v3    # "out":Lorg/icepdf/core/io/ConservativeSizingByteArrayOutputStream;
    goto :goto_0

    .line 360
    .end local v3    # "out":Lorg/icepdf/core/io/ConservativeSizingByteArrayOutputStream;
    .end local v7    # "ret":[B
    .restart local v4    # "out":Lorg/icepdf/core/io/ConservativeSizingByteArrayOutputStream;
    :cond_5
    const/4 v8, 0x0

    :try_start_4
    invoke-virtual {v4, v0, v8, v6}, Lorg/icepdf/core/io/ConservativeSizingByteArrayOutputStream;->write([BII)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_1

    .line 369
    .end local v0    # "buffer":[B
    .end local v6    # "read":I
    :catch_0
    move-exception v1

    move-object v3, v4

    .line 370
    .end local v4    # "out":Lorg/icepdf/core/io/ConservativeSizingByteArrayOutputStream;
    .end local v5    # "outLength":I
    .local v1, "e":Ljava/io/IOException;
    .restart local v3    # "out":Lorg/icepdf/core/io/ConservativeSizingByteArrayOutputStream;
    :goto_4
    :try_start_5
    sget-object v8, Lorg/icepdf/core/pobjects/Stream;->logger:Ljava/util/logging/Logger;

    sget-object v9, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string/jumbo v10, "Problem decoding stream bytes: "

    invoke-virtual {v8, v9, v10, v1}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 373
    if-eqz v2, :cond_6

    .line 375
    :try_start_6
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4

    .line 381
    :cond_6
    :goto_5
    if-eqz v3, :cond_0

    .line 383
    :try_start_7
    invoke-virtual {v3}, Lorg/icepdf/core/io/ConservativeSizingByteArrayOutputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_1

    goto :goto_0

    .line 384
    :catch_1
    move-exception v1

    .line 385
    const-string/jumbo v8, "DocumentService"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "Exception: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 376
    .end local v1    # "e":Ljava/io/IOException;
    .end local v3    # "out":Lorg/icepdf/core/io/ConservativeSizingByteArrayOutputStream;
    .restart local v0    # "buffer":[B
    .restart local v4    # "out":Lorg/icepdf/core/io/ConservativeSizingByteArrayOutputStream;
    .restart local v5    # "outLength":I
    .restart local v6    # "read":I
    .restart local v7    # "ret":[B
    :catch_2
    move-exception v1

    .line 377
    .restart local v1    # "e":Ljava/io/IOException;
    const-string/jumbo v8, "DocumentService"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "Exception: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 384
    .end local v1    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v1

    .line 385
    .restart local v1    # "e":Ljava/io/IOException;
    const-string/jumbo v8, "DocumentService"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "Exception: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 376
    .end local v0    # "buffer":[B
    .end local v4    # "out":Lorg/icepdf/core/io/ConservativeSizingByteArrayOutputStream;
    .end local v5    # "outLength":I
    .end local v6    # "read":I
    .end local v7    # "ret":[B
    .restart local v3    # "out":Lorg/icepdf/core/io/ConservativeSizingByteArrayOutputStream;
    :catch_4
    move-exception v1

    .line 377
    const-string/jumbo v8, "DocumentService"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "Exception: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_5

    .line 373
    .end local v1    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v8

    :goto_6
    if-eqz v2, :cond_7

    .line 375
    :try_start_8
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_5

    .line 381
    :cond_7
    :goto_7
    if-eqz v3, :cond_8

    .line 383
    :try_start_9
    invoke-virtual {v3}, Lorg/icepdf/core/io/ConservativeSizingByteArrayOutputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_6

    .line 386
    :cond_8
    :goto_8
    throw v8

    .line 376
    :catch_5
    move-exception v1

    .line 377
    .restart local v1    # "e":Ljava/io/IOException;
    const-string/jumbo v9, "DocumentService"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "Exception: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_7

    .line 384
    .end local v1    # "e":Ljava/io/IOException;
    :catch_6
    move-exception v1

    .line 385
    .restart local v1    # "e":Ljava/io/IOException;
    const-string/jumbo v9, "DocumentService"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "Exception: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_8

    .line 373
    .end local v1    # "e":Ljava/io/IOException;
    .end local v3    # "out":Lorg/icepdf/core/io/ConservativeSizingByteArrayOutputStream;
    .restart local v4    # "out":Lorg/icepdf/core/io/ConservativeSizingByteArrayOutputStream;
    .restart local v5    # "outLength":I
    :catchall_1
    move-exception v8

    move-object v3, v4

    .end local v4    # "out":Lorg/icepdf/core/io/ConservativeSizingByteArrayOutputStream;
    .restart local v3    # "out":Lorg/icepdf/core/io/ConservativeSizingByteArrayOutputStream;
    goto :goto_6

    .line 369
    .end local v5    # "outLength":I
    :catch_7
    move-exception v1

    goto/16 :goto_4
.end method

.method private getDecodedStreamBytesAndSize(I)[Ljava/lang/Object;
    .locals 14
    .param p1, "presize"    # I

    .prologue
    .line 403
    invoke-virtual {p0}, Lorg/icepdf/core/pobjects/Stream;->getInputStreamForDecodedStreamBytes()Ljava/io/InputStream;

    move-result-object v3

    .line 405
    .local v3, "input":Ljava/io/InputStream;
    const/4 v4, 0x0

    .line 406
    .local v4, "out":Lorg/icepdf/core/io/ConservativeSizingByteArrayOutputStream;
    if-nez v3, :cond_0

    .line 407
    const/4 v8, 0x0

    .line 454
    :goto_0
    return-object v8

    .line 410
    :cond_0
    if-lez p1, :cond_3

    .line 411
    move v6, p1

    .line 414
    .local v6, "outLength":I
    :goto_1
    :try_start_0
    new-instance v5, Lorg/icepdf/core/io/ConservativeSizingByteArrayOutputStream;

    iget-object v10, p0, Lorg/icepdf/core/pobjects/Stream;->library:Lorg/icepdf/core/util/Library;

    iget-object v10, v10, Lorg/icepdf/core/util/Library;->memoryManager:Lorg/icepdf/core/util/MemoryManager;

    invoke-direct {v5, v6, v10}, Lorg/icepdf/core/io/ConservativeSizingByteArrayOutputStream;-><init>(ILorg/icepdf/core/util/MemoryManager;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_7
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 416
    .end local v4    # "out":Lorg/icepdf/core/io/ConservativeSizingByteArrayOutputStream;
    .local v5, "out":Lorg/icepdf/core/io/ConservativeSizingByteArrayOutputStream;
    const/16 v10, 0x400

    if-le v6, v10, :cond_4

    const/16 v10, 0x1000

    :goto_2
    :try_start_1
    new-array v0, v10, [B

    .line 418
    .local v0, "buffer":[B
    :goto_3
    invoke-virtual {v3, v0}, Ljava/io/InputStream;->read([B)I

    move-result v7

    .line 419
    .local v7, "read":I
    if-gtz v7, :cond_5

    .line 423
    invoke-virtual {v5}, Lorg/icepdf/core/io/ConservativeSizingByteArrayOutputStream;->flush()V

    .line 424
    invoke-virtual {v5}, Lorg/icepdf/core/io/ConservativeSizingByteArrayOutputStream;->close()V

    .line 425
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V

    .line 427
    invoke-virtual {v5}, Lorg/icepdf/core/io/ConservativeSizingByteArrayOutputStream;->size()I

    move-result v9

    .line 430
    .local v9, "size":I
    invoke-virtual {v5}, Lorg/icepdf/core/io/ConservativeSizingByteArrayOutputStream;->relinquishByteArray()[B

    move-result-object v1

    .line 431
    .local v1, "data":[B
    const/4 v10, 0x2

    new-array v8, v10, [Ljava/lang/Object;

    const/4 v10, 0x0

    aput-object v1, v8, v10

    const/4 v10, 0x1

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v8, v10
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 437
    .local v8, "ret":[Ljava/lang/Object;
    if-eqz v3, :cond_1

    .line 439
    :try_start_2
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 445
    :cond_1
    :goto_4
    if-eqz v5, :cond_2

    .line 447
    :try_start_3
    invoke-virtual {v5}, Lorg/icepdf/core/io/ConservativeSizingByteArrayOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    :cond_2
    :goto_5
    move-object v4, v5

    .line 450
    .end local v5    # "out":Lorg/icepdf/core/io/ConservativeSizingByteArrayOutputStream;
    .restart local v4    # "out":Lorg/icepdf/core/io/ConservativeSizingByteArrayOutputStream;
    goto :goto_0

    .line 413
    .end local v0    # "buffer":[B
    .end local v1    # "data":[B
    .end local v6    # "outLength":I
    .end local v7    # "read":I
    .end local v8    # "ret":[Ljava/lang/Object;
    .end local v9    # "size":I
    :cond_3
    const/16 v10, 0x400

    :try_start_4
    iget-object v11, p0, Lorg/icepdf/core/pobjects/Stream;->streamInput:Lorg/icepdf/core/io/SeekableInputConstrainedWrapper;

    invoke-virtual {v11}, Lorg/icepdf/core/io/SeekableInputConstrainedWrapper;->getLength()J

    move-result-wide v12

    long-to-int v11, v12

    invoke-static {v10, v11}, Ljava/lang/Math;->max(II)I
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_7
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result v6

    .restart local v6    # "outLength":I
    goto :goto_1

    .line 416
    .end local v4    # "out":Lorg/icepdf/core/io/ConservativeSizingByteArrayOutputStream;
    .restart local v5    # "out":Lorg/icepdf/core/io/ConservativeSizingByteArrayOutputStream;
    :cond_4
    const/16 v10, 0x400

    goto :goto_2

    .line 421
    .restart local v0    # "buffer":[B
    .restart local v7    # "read":I
    :cond_5
    const/4 v10, 0x0

    :try_start_5
    invoke-virtual {v5, v0, v10, v7}, Lorg/icepdf/core/io/ConservativeSizingByteArrayOutputStream;->write([BII)V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    goto :goto_3

    .line 433
    .end local v0    # "buffer":[B
    .end local v7    # "read":I
    :catch_0
    move-exception v2

    move-object v4, v5

    .line 434
    .end local v5    # "out":Lorg/icepdf/core/io/ConservativeSizingByteArrayOutputStream;
    .end local v6    # "outLength":I
    .local v2, "e":Ljava/io/IOException;
    .restart local v4    # "out":Lorg/icepdf/core/io/ConservativeSizingByteArrayOutputStream;
    :goto_6
    :try_start_6
    sget-object v10, Lorg/icepdf/core/pobjects/Stream;->logger:Ljava/util/logging/Logger;

    sget-object v11, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string/jumbo v12, "Problem decoding stream bytes: "

    invoke-virtual {v10, v11, v12, v2}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 437
    if-eqz v3, :cond_6

    .line 439
    :try_start_7
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_3

    .line 445
    :cond_6
    :goto_7
    if-eqz v4, :cond_7

    .line 447
    :try_start_8
    invoke-virtual {v4}, Lorg/icepdf/core/io/ConservativeSizingByteArrayOutputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_4

    .line 454
    :cond_7
    :goto_8
    const/4 v8, 0x0

    goto :goto_0

    .line 440
    .end local v2    # "e":Ljava/io/IOException;
    .end local v4    # "out":Lorg/icepdf/core/io/ConservativeSizingByteArrayOutputStream;
    .restart local v0    # "buffer":[B
    .restart local v1    # "data":[B
    .restart local v5    # "out":Lorg/icepdf/core/io/ConservativeSizingByteArrayOutputStream;
    .restart local v6    # "outLength":I
    .restart local v7    # "read":I
    .restart local v8    # "ret":[Ljava/lang/Object;
    .restart local v9    # "size":I
    :catch_1
    move-exception v2

    .line 441
    .restart local v2    # "e":Ljava/io/IOException;
    const-string/jumbo v10, "DocumentService"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v12, "Exception: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v2}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4

    .line 448
    .end local v2    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v2

    .line 449
    .restart local v2    # "e":Ljava/io/IOException;
    const-string/jumbo v10, "DocumentService"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v12, "Exception: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v2}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_5

    .line 440
    .end local v0    # "buffer":[B
    .end local v1    # "data":[B
    .end local v5    # "out":Lorg/icepdf/core/io/ConservativeSizingByteArrayOutputStream;
    .end local v6    # "outLength":I
    .end local v7    # "read":I
    .end local v8    # "ret":[Ljava/lang/Object;
    .end local v9    # "size":I
    .restart local v4    # "out":Lorg/icepdf/core/io/ConservativeSizingByteArrayOutputStream;
    :catch_3
    move-exception v2

    .line 441
    const-string/jumbo v10, "DocumentService"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v12, "Exception: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v2}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_7

    .line 448
    :catch_4
    move-exception v2

    .line 449
    const-string/jumbo v10, "DocumentService"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v12, "Exception: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v2}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_8

    .line 437
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v10

    :goto_9
    if-eqz v3, :cond_8

    .line 439
    :try_start_9
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_5

    .line 445
    :cond_8
    :goto_a
    if-eqz v4, :cond_9

    .line 447
    :try_start_a
    invoke-virtual {v4}, Lorg/icepdf/core/io/ConservativeSizingByteArrayOutputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_6

    .line 450
    :cond_9
    :goto_b
    throw v10

    .line 440
    :catch_5
    move-exception v2

    .line 441
    .restart local v2    # "e":Ljava/io/IOException;
    const-string/jumbo v11, "DocumentService"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v13, "Exception: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v2}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_a

    .line 448
    .end local v2    # "e":Ljava/io/IOException;
    :catch_6
    move-exception v2

    .line 449
    .restart local v2    # "e":Ljava/io/IOException;
    const-string/jumbo v11, "DocumentService"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v13, "Exception: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v2}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_b

    .line 437
    .end local v2    # "e":Ljava/io/IOException;
    .end local v4    # "out":Lorg/icepdf/core/io/ConservativeSizingByteArrayOutputStream;
    .restart local v5    # "out":Lorg/icepdf/core/io/ConservativeSizingByteArrayOutputStream;
    .restart local v6    # "outLength":I
    :catchall_1
    move-exception v10

    move-object v4, v5

    .end local v5    # "out":Lorg/icepdf/core/io/ConservativeSizingByteArrayOutputStream;
    .restart local v4    # "out":Lorg/icepdf/core/io/ConservativeSizingByteArrayOutputStream;
    goto :goto_9

    .line 433
    .end local v6    # "outLength":I
    :catch_7
    move-exception v2

    goto/16 :goto_6
.end method

.method private getFilterNames()Ljava/util/Vector;
    .locals 5

    .prologue
    .line 531
    const/4 v0, 0x0

    .line 532
    .local v0, "filterNames":Ljava/util/Vector;
    iget-object v2, p0, Lorg/icepdf/core/pobjects/Stream;->library:Lorg/icepdf/core/util/Library;

    iget-object v3, p0, Lorg/icepdf/core/pobjects/Stream;->entries:Ljava/util/Hashtable;

    const-string/jumbo v4, "Filter"

    invoke-virtual {v2, v3, v4}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    .line 533
    .local v1, "o":Ljava/lang/Object;
    instance-of v2, v1, Lorg/icepdf/core/pobjects/Name;

    if-eqz v2, :cond_1

    .line 534
    new-instance v0, Ljava/util/Vector;

    .end local v0    # "filterNames":Ljava/util/Vector;
    const/4 v2, 0x1

    invoke-direct {v0, v2}, Ljava/util/Vector;-><init>(I)V

    .line 535
    .restart local v0    # "filterNames":Ljava/util/Vector;
    invoke-virtual {v0, v1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    .line 539
    :cond_0
    :goto_0
    return-object v0

    .line 536
    :cond_1
    instance-of v2, v1, Ljava/util/Vector;

    if-eqz v2, :cond_0

    move-object v0, v1

    .line 537
    check-cast v0, Ljava/util/Vector;

    goto :goto_0
.end method

.method private getImage(Lorg/icepdf/core/pobjects/graphics/PColorSpace;Lorg/apache/poi/java/awt/Color;IIIIZLjava/util/Vector;Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;[I[III)Landroid/graphics/Bitmap;
    .locals 26
    .param p1, "colourSpace"    # Lorg/icepdf/core/pobjects/graphics/PColorSpace;
    .param p2, "fill"    # Lorg/apache/poi/java/awt/Color;
    .param p3, "width"    # I
    .param p4, "height"    # I
    .param p5, "colorSpaceCompCount"    # I
    .param p6, "bitsPerComponent"    # I
    .param p7, "imageMask"    # Z
    .param p8, "decode"    # Ljava/util/Vector;
    .param p9, "smaskImage"    # Landroid/graphics/Bitmap;
    .param p10, "maskImage"    # Landroid/graphics/Bitmap;
    .param p11, "maskMinRGB"    # [I
    .param p12, "maskMaxRGB"    # [I
    .param p13, "maskMinIndex"    # I
    .param p14, "maskMaxIndex"    # I

    .prologue
    .line 2505
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/icepdf/core/pobjects/Stream;->image:Lorg/icepdf/core/util/ImageCache;

    if-eqz v5, :cond_2

    .line 2508
    mul-int v5, p3, p4

    const/4 v6, 0x4

    move/from16 v0, p5

    invoke-static {v0, v6}, Ljava/lang/Math;->max(II)I

    move-result v6

    mul-int/2addr v5, v6

    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lorg/icepdf/core/pobjects/Stream;->checkMemory(I)Z

    .line 2513
    const/16 v25, 0x0

    .line 2514
    .local v25, "img":Landroid/graphics/Bitmap;
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/icepdf/core/pobjects/Stream;->imageLock:Ljava/lang/Object;

    monitor-enter v6

    .line 2515
    :try_start_0
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/icepdf/core/pobjects/Stream;->image:Lorg/icepdf/core/util/ImageCache;

    if-eqz v5, :cond_0

    .line 2516
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/icepdf/core/pobjects/Stream;->image:Lorg/icepdf/core/util/ImageCache;

    invoke-virtual {v5}, Lorg/icepdf/core/util/ImageCache;->readImage()Landroid/graphics/Bitmap;

    move-result-object v25

    .line 2518
    :cond_0
    monitor-exit v6

    .line 2519
    if-eqz v25, :cond_2

    move-object/from16 v23, v25

    .line 2641
    .end local v25    # "img":Landroid/graphics/Bitmap;
    :cond_1
    :goto_0
    return-object v23

    .line 2518
    .restart local v25    # "img":Landroid/graphics/Bitmap;
    :catchall_0
    move-exception v5

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v5

    .line 2528
    .end local v25    # "img":Landroid/graphics/Bitmap;
    :cond_2
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/icepdf/core/pobjects/Stream;->image:Lorg/icepdf/core/util/ImageCache;

    if-nez v5, :cond_5

    .line 2529
    const/16 v23, 0x0

    .line 2532
    .local v23, "decodedImage":Landroid/graphics/Bitmap;
    invoke-direct/range {p0 .. p0}, Lorg/icepdf/core/pobjects/Stream;->shouldUseDCTDecode()Z

    move-result v5

    if-eqz v5, :cond_b

    .line 2533
    sget-boolean v5, Lorg/icepdf/core/tag/Tagger;->tagging:Z

    if-eqz v5, :cond_3

    .line 2534
    const-string/jumbo v5, "DCTDecode"

    invoke-static {v5}, Lorg/icepdf/core/tag/Tagger;->tagImage(Ljava/lang/String;)V

    :cond_3
    move-object/from16 v5, p0

    move/from16 v6, p3

    move/from16 v7, p4

    move-object/from16 v8, p1

    move/from16 v9, p6

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p8

    .line 2535
    invoke-direct/range {v5 .. v14}, Lorg/icepdf/core/pobjects/Stream;->dctDecode(IILorg/icepdf/core/pobjects/graphics/PColorSpace;ILandroid/graphics/Bitmap;Landroid/graphics/Bitmap;[I[ILjava/util/Vector;)Landroid/graphics/Bitmap;

    move-result-object v23

    .line 2552
    :cond_4
    :goto_1
    if-nez v23, :cond_1

    .line 2558
    .end local v23    # "decodedImage":Landroid/graphics/Bitmap;
    :cond_5
    const/16 v20, 0x0

    .line 2559
    .local v20, "data":[B
    const/16 v21, 0x0

    .line 2561
    .local v21, "dataLength":I
    invoke-direct/range {p0 .. p0}, Lorg/icepdf/core/pobjects/Stream;->shouldUseCCITTFaxDecode()Z

    move-result v5

    if-eqz v5, :cond_10

    .line 2563
    sget-boolean v5, Lorg/icepdf/core/tag/Tagger;->tagging:Z

    if-eqz v5, :cond_6

    .line 2564
    const-string/jumbo v5, "CCITTFaxDecode"

    invoke-static {v5}, Lorg/icepdf/core/tag/Tagger;->tagImage(Ljava/lang/String;)V

    .line 2568
    :cond_6
    :try_start_1
    sget-boolean v5, Lorg/icepdf/core/pobjects/Stream;->forceJaiccittfax:Z

    if-eqz v5, :cond_f

    .line 2569
    new-instance v5, Ljava/lang/Throwable;

    const-string/jumbo v6, "Forcing CCITTFAX decode via JAI"

    invoke-direct {v5, v6}, Ljava/lang/Throwable;-><init>(Ljava/lang/String;)V

    throw v5
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    .line 2573
    :catch_0
    move-exception v24

    .line 2576
    .local v24, "e":Ljava/lang/Throwable;
    sget-boolean v5, Lorg/icepdf/core/tag/Tagger;->tagging:Z

    if-eqz v5, :cond_7

    .line 2577
    const-string/jumbo v5, "CCITTFaxDecode JAI"

    invoke-static {v5}, Lorg/icepdf/core/tag/Tagger;->tagImage(Ljava/lang/String;)V

    .line 2582
    :cond_7
    const/16 v23, 0x0

    .line 2583
    .restart local v23    # "decodedImage":Landroid/graphics/Bitmap;
    if-eqz v20, :cond_8

    .line 2584
    const/4 v5, 0x0

    move-object/from16 v0, v20

    move/from16 v1, v21

    invoke-static {v0, v5, v1}, Landroid/graphics/BitmapFactory;->decodeByteArray([BII)Landroid/graphics/Bitmap;

    move-result-object v23

    .line 2585
    :cond_8
    if-nez v23, :cond_1

    .line 2603
    .end local v23    # "decodedImage":Landroid/graphics/Bitmap;
    .end local v24    # "e":Ljava/lang/Throwable;
    :cond_9
    :goto_2
    if-eqz v20, :cond_a

    move-object/from16 v5, p0

    move-object/from16 v6, p1

    move-object/from16 v7, p2

    move/from16 v8, p3

    move/from16 v9, p4

    move/from16 v10, p5

    move/from16 v11, p6

    move/from16 v12, p7

    move-object/from16 v13, p8

    move-object/from16 v14, p9

    move-object/from16 v15, p10

    move-object/from16 v16, p11

    move-object/from16 v17, p12

    move/from16 v18, p13

    move/from16 v19, p14

    .line 2605
    :try_start_2
    invoke-direct/range {v5 .. v21}, Lorg/icepdf/core/pobjects/Stream;->makeImageWithRasterFromBytes(Lorg/icepdf/core/pobjects/graphics/PColorSpace;Lorg/apache/poi/java/awt/Color;IIIIZLjava/util/Vector;Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;[I[III[BI)Landroid/graphics/Bitmap;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    move-result-object v23

    .line 2618
    .restart local v23    # "decodedImage":Landroid/graphics/Bitmap;
    if-nez v23, :cond_1

    .line 2628
    .end local v23    # "decodedImage":Landroid/graphics/Bitmap;
    :cond_a
    :goto_3
    const/16 v23, 0x0

    .line 2641
    .restart local v23    # "decodedImage":Landroid/graphics/Bitmap;
    goto/16 :goto_0

    .line 2539
    .end local v20    # "data":[B
    .end local v21    # "dataLength":I
    :cond_b
    invoke-direct/range {p0 .. p0}, Lorg/icepdf/core/pobjects/Stream;->shouldUseJBIG2Decode()Z

    move-result v5

    if-eqz v5, :cond_d

    .line 2540
    sget-boolean v5, Lorg/icepdf/core/tag/Tagger;->tagging:Z

    if-eqz v5, :cond_c

    .line 2541
    const-string/jumbo v5, "JBIG2Decode"

    invoke-static {v5}, Lorg/icepdf/core/tag/Tagger;->tagImage(Ljava/lang/String;)V

    .line 2542
    :cond_c
    move-object/from16 v0, p0

    move/from16 v1, p3

    move/from16 v2, p4

    move-object/from16 v3, p2

    move/from16 v4, p7

    invoke-direct {v0, v1, v2, v3, v4}, Lorg/icepdf/core/pobjects/Stream;->jbig2Decode(IILorg/apache/poi/java/awt/Color;Z)Landroid/graphics/Bitmap;

    move-result-object v23

    goto/16 :goto_1

    .line 2545
    :cond_d
    invoke-direct/range {p0 .. p0}, Lorg/icepdf/core/pobjects/Stream;->shouldUseJPXDecode()Z

    move-result v5

    if-eqz v5, :cond_4

    .line 2546
    sget-boolean v5, Lorg/icepdf/core/tag/Tagger;->tagging:Z

    if-eqz v5, :cond_e

    .line 2547
    const-string/jumbo v5, "JPXDecode"

    invoke-static {v5}, Lorg/icepdf/core/tag/Tagger;->tagImage(Ljava/lang/String;)V

    :cond_e
    move-object/from16 v5, p0

    move/from16 v6, p3

    move/from16 v7, p4

    move-object/from16 v8, p1

    move/from16 v9, p6

    move-object/from16 v10, p2

    move-object/from16 v11, p9

    move-object/from16 v12, p10

    move-object/from16 v13, p11

    move-object/from16 v14, p12

    move-object/from16 v15, p8

    .line 2548
    invoke-direct/range {v5 .. v15}, Lorg/icepdf/core/pobjects/Stream;->jpxDecode(IILorg/icepdf/core/pobjects/graphics/PColorSpace;ILorg/apache/poi/java/awt/Color;Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;[I[ILjava/util/Vector;)Landroid/graphics/Bitmap;

    move-result-object v23

    goto/16 :goto_1

    .line 2571
    .end local v23    # "decodedImage":Landroid/graphics/Bitmap;
    .restart local v20    # "data":[B
    .restart local v21    # "dataLength":I
    :cond_f
    :try_start_3
    move-object/from16 v0, p0

    move/from16 v1, p3

    move/from16 v2, p4

    invoke-direct {v0, v1, v2}, Lorg/icepdf/core/pobjects/Stream;->ccittFaxDecode(II)[B

    move-result-object v20

    .line 2572
    move-object/from16 v0, v20

    array-length v0, v0

    move/from16 v21, v0
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_0

    goto/16 :goto_2

    .line 2593
    :cond_10
    mul-int v5, p3, p4

    invoke-virtual/range {p1 .. p1}, Lorg/icepdf/core/pobjects/graphics/PColorSpace;->getNumComponents()I

    move-result v6

    mul-int/2addr v5, v6

    mul-int v5, v5, p6

    div-int/lit8 v5, v5, 0x8

    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lorg/icepdf/core/pobjects/Stream;->getDecodedStreamBytesAndSize(I)[Ljava/lang/Object;

    move-result-object v22

    .line 2597
    .local v22, "dataAndSize":[Ljava/lang/Object;
    if-eqz v22, :cond_9

    move-object/from16 v0, v22

    array-length v5, v0

    const/4 v6, 0x2

    if-lt v5, v6, :cond_9

    .line 2598
    const/4 v5, 0x0

    aget-object v5, v22, v5

    check-cast v5, [B

    move-object/from16 v20, v5

    check-cast v20, [B

    .line 2599
    const/4 v5, 0x1

    aget-object v5, v22, v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v21

    goto/16 :goto_2

    .line 2621
    .end local v22    # "dataAndSize":[Ljava/lang/Object;
    :catch_1
    move-exception v24

    .line 2622
    .local v24, "e":Ljava/lang/Exception;
    sget-object v5, Lorg/icepdf/core/pobjects/Stream;->logger:Ljava/util/logging/Logger;

    sget-object v6, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string/jumbo v7, "Error building image raster."

    move-object/from16 v0, v24

    invoke-virtual {v5, v6, v7, v0}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_3
.end method

.method private getNormalisedFilterNames()Ljava/util/Vector;
    .locals 4

    .prologue
    .line 543
    invoke-direct {p0}, Lorg/icepdf/core/pobjects/Stream;->getFilterNames()Ljava/util/Vector;

    move-result-object v1

    .line 544
    .local v1, "filterNames":Ljava/util/Vector;
    if-nez v1, :cond_1

    .line 545
    const/4 v1, 0x0

    .line 588
    .end local v1    # "filterNames":Ljava/util/Vector;
    :cond_0
    return-object v1

    .line 547
    .restart local v1    # "filterNames":Ljava/util/Vector;
    :cond_1
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v3

    if-ge v2, v3, :cond_0

    .line 548
    invoke-virtual {v1, v2}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 550
    .local v0, "filterName":Ljava/lang/String;
    const-string/jumbo v3, "FlateDecode"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    const-string/jumbo v3, "/Fl"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    const-string/jumbo v3, "Fl"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 553
    :cond_2
    const-string/jumbo v0, "FlateDecode"

    .line 586
    :cond_3
    :goto_1
    invoke-virtual {v1, v2, v0}, Ljava/util/Vector;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 547
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 554
    :cond_4
    const-string/jumbo v3, "LZWDecode"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    const-string/jumbo v3, "/LZW"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    const-string/jumbo v3, "LZW"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 558
    :cond_5
    const-string/jumbo v0, "LZWDecode"

    goto :goto_1

    .line 559
    :cond_6
    const-string/jumbo v3, "ASCII85Decode"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_7

    const-string/jumbo v3, "/A85"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_7

    const-string/jumbo v3, "A85"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 563
    :cond_7
    const-string/jumbo v0, "ASCII85Decode"

    goto :goto_1

    .line 564
    :cond_8
    const-string/jumbo v3, "ASCIIHexDecode"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_9

    const-string/jumbo v3, "/AHx"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_9

    const-string/jumbo v3, "AHx"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 568
    :cond_9
    const-string/jumbo v0, "ASCIIHexDecode"

    goto :goto_1

    .line 569
    :cond_a
    const-string/jumbo v3, "RunLengthDecode"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_b

    const-string/jumbo v3, "/RL"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_b

    const-string/jumbo v3, "RL"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_c

    .line 573
    :cond_b
    const-string/jumbo v0, "RunLengthDecode"

    goto/16 :goto_1

    .line 574
    :cond_c
    const-string/jumbo v3, "CCITTFaxDecode"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_d

    const-string/jumbo v3, "/CCF"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_d

    const-string/jumbo v3, "CCF"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_e

    .line 578
    :cond_d
    const-string/jumbo v0, "CCITTFaxDecode"

    goto/16 :goto_1

    .line 579
    :cond_e
    const-string/jumbo v3, "DCTDecode"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_f

    const-string/jumbo v3, "/DCT"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_f

    const-string/jumbo v3, "DCT"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 583
    :cond_f
    const-string/jumbo v0, "DCTDecode"

    goto/16 :goto_1
.end method

.method private jbig2Decode(IILorg/apache/poi/java/awt/Color;Z)Landroid/graphics/Bitmap;
    .locals 2
    .param p1, "width"    # I
    .param p2, "height"    # I
    .param p3, "fill"    # Lorg/apache/poi/java/awt/Color;
    .param p4, "imageMask"    # Z

    .prologue
    .line 932
    invoke-virtual {p0}, Lorg/icepdf/core/pobjects/Stream;->getInputStreamForDecodedStreamBytes()Ljava/io/InputStream;

    move-result-object v1

    .line 933
    .local v1, "input":Ljava/io/InputStream;
    invoke-static {v1}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 934
    .local v0, "bi":Landroid/graphics/Bitmap;
    return-object v0
.end method

.method private jpxDecode(IILorg/icepdf/core/pobjects/graphics/PColorSpace;ILorg/apache/poi/java/awt/Color;Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;[I[ILjava/util/Vector;)Landroid/graphics/Bitmap;
    .locals 2
    .param p1, "width"    # I
    .param p2, "height"    # I
    .param p3, "colourSpace"    # Lorg/icepdf/core/pobjects/graphics/PColorSpace;
    .param p4, "bitsPerComponent"    # I
    .param p5, "fill"    # Lorg/apache/poi/java/awt/Color;
    .param p6, "sMaskImage"    # Landroid/graphics/Bitmap;
    .param p7, "maskImage"    # Landroid/graphics/Bitmap;
    .param p8, "maskMinRGB"    # [I
    .param p9, "maskMaxRGB"    # [I
    .param p10, "decode"    # Ljava/util/Vector;

    .prologue
    .line 1030
    invoke-virtual {p0}, Lorg/icepdf/core/pobjects/Stream;->getInputStreamForDecodedStreamBytes()Ljava/io/InputStream;

    move-result-object v1

    .line 1031
    .local v1, "input":Ljava/io/InputStream;
    invoke-static {v1}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1032
    .local v0, "bi":Landroid/graphics/Bitmap;
    return-object v0
.end method

.method private makeImageWithRasterFromBytes(Lorg/icepdf/core/pobjects/graphics/PColorSpace;Lorg/apache/poi/java/awt/Color;IIIIZLjava/util/Vector;Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;[I[III[BI)Landroid/graphics/Bitmap;
    .locals 36
    .param p1, "colourSpace"    # Lorg/icepdf/core/pobjects/graphics/PColorSpace;
    .param p2, "fill"    # Lorg/apache/poi/java/awt/Color;
    .param p3, "width"    # I
    .param p4, "height"    # I
    .param p5, "colorSpaceCompCount"    # I
    .param p6, "bitspercomponent"    # I
    .param p7, "imageMask"    # Z
    .param p8, "decode"    # Ljava/util/Vector;
    .param p9, "smaskImage"    # Landroid/graphics/Bitmap;
    .param p10, "maskImage"    # Landroid/graphics/Bitmap;
    .param p11, "maskMinRGB"    # [I
    .param p12, "maskMaxRGB"    # [I
    .param p13, "maskMinIndex"    # I
    .param p14, "maskMaxIndex"    # I
    .param p15, "data"    # [B
    .param p16, "dataLength"    # I

    .prologue
    .line 2657
    const/4 v4, 0x0

    .line 2661
    .local v4, "bi":Landroid/graphics/Bitmap;
    move-object/from16 v0, p1

    instance-of v6, v0, Lorg/icepdf/core/pobjects/graphics/ICCBased;

    if-eqz v6, :cond_0

    move-object/from16 v25, p1

    .line 2662
    check-cast v25, Lorg/icepdf/core/pobjects/graphics/ICCBased;

    .line 2663
    .local v25, "iccBased":Lorg/icepdf/core/pobjects/graphics/ICCBased;
    invoke-virtual/range {v25 .. v25}, Lorg/icepdf/core/pobjects/graphics/ICCBased;->getAlternate()Lorg/icepdf/core/pobjects/graphics/PColorSpace;

    move-result-object v6

    if-eqz v6, :cond_0

    .line 2665
    invoke-virtual/range {v25 .. v25}, Lorg/icepdf/core/pobjects/graphics/ICCBased;->getAlternate()Lorg/icepdf/core/pobjects/graphics/PColorSpace;

    move-result-object p1

    .line 2669
    .end local v25    # "iccBased":Lorg/icepdf/core/pobjects/graphics/ICCBased;
    :cond_0
    move-object/from16 v0, p1

    instance-of v6, v0, Lorg/icepdf/core/pobjects/graphics/DeviceGray;

    if-eqz v6, :cond_19

    .line 2671
    if-eqz p7, :cond_a

    const/4 v6, 0x1

    move/from16 v0, p6

    if-ne v0, v6, :cond_a

    .line 2688
    const/4 v8, 0x0

    const/4 v6, 0x0

    move-object/from16 v0, p8

    invoke-virtual {v0, v6}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Number;

    invoke-virtual {v6}, Ljava/lang/Number;->floatValue()F

    move-result v6

    cmpl-float v6, v8, v6

    if-nez v6, :cond_1

    const/16 v21, 0x1

    .line 2690
    .local v21, "defaultDecode":Z
    :goto_0
    const v12, 0xffffff

    .line 2691
    .local v12, "a":I
    const/4 v6, 0x2

    new-array v14, v6, [I

    const/4 v8, 0x0

    if-eqz v21, :cond_2

    invoke-virtual/range {p2 .. p2}, Lorg/apache/poi/java/awt/Color;->getRGB()I

    move-result v6

    :goto_1
    aput v6, v14, v8

    const/4 v6, 0x1

    if-eqz v21, :cond_3

    .end local v12    # "a":I
    :goto_2
    aput v12, v14, v6

    .line 2696
    .local v14, "cmap":[I
    mul-int v6, p3, p4

    new-array v0, v6, [I

    move-object/from16 v31, v0

    .line 2697
    .local v31, "pixel":[I
    const/16 v20, 0x0

    .line 2698
    .local v20, "count":I
    const/4 v6, 0x0

    aget v6, v14, v6

    move-object/from16 v0, v31

    invoke-static {v0, v6}, Ljava/util/Arrays;->fill([II)V

    .line 2699
    const/16 v24, 0x0

    .local v24, "i":I
    :goto_3
    move-object/from16 v0, p15

    array-length v6, v0

    move/from16 v0, v24

    if-ge v0, v6, :cond_8

    .line 2701
    aget-byte v27, p15, v24

    .line 2702
    .local v27, "j":I
    const/4 v13, 0x0

    .line 2703
    .local v13, "c":I
    new-instance v32, Ljava/util/Stack;

    invoke-direct/range {v32 .. v32}, Ljava/util/Stack;-><init>()V

    .line 2704
    .local v32, "st":Ljava/util/Stack;, "Ljava/util/Stack<Ljava/lang/Integer;>;"
    rem-int v6, v20, p3

    sub-int v6, p3, v6

    const/16 v8, 0x8

    if-ge v6, v8, :cond_4

    .line 2706
    rem-int v6, v20, p3

    sub-int v6, p3, v6

    add-int v20, v20, v6

    .line 2699
    :goto_4
    add-int/lit8 v24, v24, 0x1

    goto :goto_3

    .line 2688
    .end local v13    # "c":I
    .end local v14    # "cmap":[I
    .end local v20    # "count":I
    .end local v21    # "defaultDecode":Z
    .end local v24    # "i":I
    .end local v27    # "j":I
    .end local v31    # "pixel":[I
    .end local v32    # "st":Ljava/util/Stack;, "Ljava/util/Stack<Ljava/lang/Integer;>;"
    :cond_1
    const/16 v21, 0x0

    goto :goto_0

    .restart local v12    # "a":I
    .restart local v21    # "defaultDecode":Z
    :cond_2
    move v6, v12

    .line 2691
    goto :goto_1

    :cond_3
    invoke-virtual/range {p2 .. p2}, Lorg/apache/poi/java/awt/Color;->getRGB()I

    move-result v12

    goto :goto_2

    .line 2712
    .end local v12    # "a":I
    .restart local v13    # "c":I
    .restart local v14    # "cmap":[I
    .restart local v20    # "count":I
    .restart local v24    # "i":I
    .restart local v27    # "j":I
    .restart local v31    # "pixel":[I
    .restart local v32    # "st":Ljava/util/Stack;, "Ljava/util/Stack<Ljava/lang/Integer;>;"
    :cond_4
    :goto_5
    if-eqz v27, :cond_6

    .line 2714
    rsub-int/lit8 v6, v27, 0x0

    and-int v13, v27, v6

    .line 2715
    invoke-static {v13}, Ljava/lang/Integer;->numberOfLeadingZeros(I)I

    move-result v6

    add-int/lit8 v26, v6, -0x18

    .line 2716
    .local v26, "index":I
    if-ltz v26, :cond_5

    const/16 v6, 0x8

    move/from16 v0, v26

    if-ge v0, v6, :cond_5

    .line 2717
    invoke-static/range {v26 .. v26}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    move-object/from16 v0, v32

    invoke-virtual {v0, v6}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2718
    :cond_5
    xor-int v27, v27, v13

    .line 2719
    goto :goto_5

    .line 2720
    .end local v26    # "index":I
    :cond_6
    :goto_6
    invoke-virtual/range {v32 .. v32}, Ljava/util/Stack;->empty()Z

    move-result v6

    if-nez v6, :cond_7

    .line 2722
    invoke-virtual/range {v32 .. v32}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v22

    .line 2723
    .local v22, "ele":I
    add-int v6, v20, v22

    const/4 v8, 0x1

    aget v8, v14, v8

    aput v8, v31, v6

    goto :goto_6

    .line 2726
    .end local v22    # "ele":I
    :cond_7
    add-int/lit8 v20, v20, 0x8

    goto :goto_4

    .line 2728
    .end local v13    # "c":I
    .end local v27    # "j":I
    .end local v32    # "st":Ljava/util/Stack;, "Ljava/util/Stack<Ljava/lang/Integer;>;"
    :cond_8
    sget-object v6, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    move/from16 v0, p3

    move/from16 v1, p4

    invoke-static {v0, v1, v6}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 2729
    move/from16 v28, p4

    .line 2730
    .local v28, "maxH":I
    move/from16 v7, p3

    .line 2731
    .local v7, "maxW":I
    new-array v5, v7, [I

    .line 2732
    .local v5, "line":[I
    const/4 v6, 0x0

    aget v6, v14, v6

    invoke-static {v5, v6}, Ljava/util/Arrays;->fill([II)V

    .line 2733
    const/16 v29, 0x0

    .line 2735
    .local v29, "n":I
    const/4 v9, 0x0

    .local v9, "h":I
    :goto_7
    move/from16 v0, v28

    if-ge v9, v0, :cond_16

    .line 2736
    const/16 v35, 0x0

    .local v35, "w":I
    move/from16 v30, v29

    .end local v29    # "n":I
    .local v30, "n":I
    :goto_8
    move/from16 v0, v35

    move/from16 v1, p3

    if-ge v0, v1, :cond_9

    .line 2738
    add-int/lit8 v29, v30, 0x1

    .end local v30    # "n":I
    .restart local v29    # "n":I
    aget v6, v31, v30

    aput v6, v5, v35

    .line 2736
    add-int/lit8 v35, v35, 0x1

    move/from16 v30, v29

    .end local v29    # "n":I
    .restart local v30    # "n":I
    goto :goto_8

    .line 2740
    :cond_9
    const/4 v6, 0x0

    const/4 v8, 0x0

    const/4 v11, 0x1

    move v10, v7

    invoke-virtual/range {v4 .. v11}, Landroid/graphics/Bitmap;->setPixels([IIIIIII)V

    .line 2735
    add-int/lit8 v9, v9, 0x1

    move/from16 v29, v30

    .end local v30    # "n":I
    .restart local v29    # "n":I
    goto :goto_7

    .line 2742
    .end local v5    # "line":[I
    .end local v7    # "maxW":I
    .end local v9    # "h":I
    .end local v14    # "cmap":[I
    .end local v20    # "count":I
    .end local v21    # "defaultDecode":Z
    .end local v24    # "i":I
    .end local v28    # "maxH":I
    .end local v29    # "n":I
    .end local v31    # "pixel":[I
    .end local v35    # "w":I
    :cond_a
    const/4 v6, 0x1

    move/from16 v0, p6

    if-eq v0, v6, :cond_b

    const/4 v6, 0x2

    move/from16 v0, p6

    if-eq v0, v6, :cond_b

    const/4 v6, 0x4

    move/from16 v0, p6

    if-ne v0, v6, :cond_14

    .line 2761
    :cond_b
    const/4 v14, 0x0

    .line 2762
    .restart local v14    # "cmap":[I
    const/4 v6, 0x1

    move/from16 v0, p6

    if-ne v0, v6, :cond_16

    .line 2763
    const/4 v8, 0x0

    const/4 v6, 0x0

    move-object/from16 v0, p8

    invoke-virtual {v0, v6}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Number;

    invoke-virtual {v6}, Ljava/lang/Number;->floatValue()F

    move-result v6

    cmpl-float v6, v8, v6

    if-nez v6, :cond_c

    const/16 v21, 0x1

    .line 2764
    .restart local v21    # "defaultDecode":Z
    :goto_9
    if-eqz v21, :cond_d

    sget-object v14, Lorg/icepdf/core/pobjects/Stream;->GRAY_1_BIT_INDEX_TO_RGB:[I

    .line 2765
    :goto_a
    mul-int v6, p3, p4

    new-array v0, v6, [I

    move-object/from16 v31, v0

    .line 2766
    .restart local v31    # "pixel":[I
    const/16 v20, 0x0

    .line 2767
    .restart local v20    # "count":I
    const/4 v6, 0x0

    aget v6, v14, v6

    move-object/from16 v0, v31

    invoke-static {v0, v6}, Ljava/util/Arrays;->fill([II)V

    .line 2768
    const/16 v24, 0x0

    .restart local v24    # "i":I
    :goto_b
    move-object/from16 v0, p15

    array-length v6, v0

    move/from16 v0, v24

    if-ge v0, v6, :cond_12

    .line 2770
    aget-byte v27, p15, v24

    .line 2771
    .restart local v27    # "j":I
    const/4 v13, 0x0

    .line 2772
    .restart local v13    # "c":I
    new-instance v32, Ljava/util/Stack;

    invoke-direct/range {v32 .. v32}, Ljava/util/Stack;-><init>()V

    .line 2773
    .restart local v32    # "st":Ljava/util/Stack;, "Ljava/util/Stack<Ljava/lang/Integer;>;"
    rem-int v6, v20, p3

    sub-int v6, p3, v6

    const/16 v8, 0x8

    if-ge v6, v8, :cond_e

    .line 2775
    rem-int v6, v20, p3

    sub-int v6, p3, v6

    add-int v20, v20, v6

    .line 2768
    :goto_c
    add-int/lit8 v24, v24, 0x1

    goto :goto_b

    .line 2763
    .end local v13    # "c":I
    .end local v20    # "count":I
    .end local v21    # "defaultDecode":Z
    .end local v24    # "i":I
    .end local v27    # "j":I
    .end local v31    # "pixel":[I
    .end local v32    # "st":Ljava/util/Stack;, "Ljava/util/Stack<Ljava/lang/Integer;>;"
    :cond_c
    const/16 v21, 0x0

    goto :goto_9

    .line 2764
    .restart local v21    # "defaultDecode":Z
    :cond_d
    sget-object v14, Lorg/icepdf/core/pobjects/Stream;->GRAY_1_BIT_INDEX_TO_RGB_REVERSED:[I

    goto :goto_a

    .line 2781
    .restart local v13    # "c":I
    .restart local v20    # "count":I
    .restart local v24    # "i":I
    .restart local v27    # "j":I
    .restart local v31    # "pixel":[I
    .restart local v32    # "st":Ljava/util/Stack;, "Ljava/util/Stack<Ljava/lang/Integer;>;"
    :cond_e
    :goto_d
    if-eqz v27, :cond_10

    .line 2783
    rsub-int/lit8 v6, v27, 0x0

    and-int v13, v27, v6

    .line 2784
    invoke-static {v13}, Ljava/lang/Integer;->numberOfLeadingZeros(I)I

    move-result v6

    add-int/lit8 v26, v6, -0x18

    .line 2785
    .restart local v26    # "index":I
    if-ltz v26, :cond_f

    const/16 v6, 0x8

    move/from16 v0, v26

    if-ge v0, v6, :cond_f

    .line 2786
    invoke-static/range {v26 .. v26}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    move-object/from16 v0, v32

    invoke-virtual {v0, v6}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2787
    :cond_f
    xor-int v27, v27, v13

    .line 2788
    goto :goto_d

    .line 2789
    .end local v26    # "index":I
    :cond_10
    :goto_e
    invoke-virtual/range {v32 .. v32}, Ljava/util/Stack;->empty()Z

    move-result v6

    if-nez v6, :cond_11

    .line 2791
    invoke-virtual/range {v32 .. v32}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v22

    .line 2792
    .restart local v22    # "ele":I
    add-int v6, v20, v22

    const/4 v8, 0x1

    aget v8, v14, v8

    aput v8, v31, v6

    goto :goto_e

    .line 2795
    .end local v22    # "ele":I
    :cond_11
    add-int/lit8 v20, v20, 0x8

    goto :goto_c

    .line 2797
    .end local v13    # "c":I
    .end local v27    # "j":I
    .end local v32    # "st":Ljava/util/Stack;, "Ljava/util/Stack<Ljava/lang/Integer;>;"
    :cond_12
    sget-object v6, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    move/from16 v0, p3

    move/from16 v1, p4

    invoke-static {v0, v1, v6}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 2798
    move/from16 v28, p4

    .line 2799
    .restart local v28    # "maxH":I
    move/from16 v7, p3

    .line 2800
    .restart local v7    # "maxW":I
    new-array v5, v7, [I

    .line 2801
    .restart local v5    # "line":[I
    const/4 v6, 0x0

    aget v6, v14, v6

    invoke-static {v5, v6}, Ljava/util/Arrays;->fill([II)V

    .line 2802
    const/16 v29, 0x0

    .line 2804
    .restart local v29    # "n":I
    const/4 v9, 0x0

    .restart local v9    # "h":I
    :goto_f
    move/from16 v0, v28

    if-ge v9, v0, :cond_16

    .line 2805
    const/16 v35, 0x0

    .restart local v35    # "w":I
    move/from16 v30, v29

    .end local v29    # "n":I
    .restart local v30    # "n":I
    :goto_10
    move/from16 v0, v35

    move/from16 v1, p3

    if-ge v0, v1, :cond_13

    .line 2807
    add-int/lit8 v29, v30, 0x1

    .end local v30    # "n":I
    .restart local v29    # "n":I
    aget v6, v31, v30

    aput v6, v5, v35

    .line 2805
    add-int/lit8 v35, v35, 0x1

    move/from16 v30, v29

    .end local v29    # "n":I
    .restart local v30    # "n":I
    goto :goto_10

    .line 2809
    :cond_13
    const/4 v6, 0x0

    const/4 v8, 0x0

    const/4 v11, 0x1

    move v10, v7

    invoke-virtual/range {v4 .. v11}, Landroid/graphics/Bitmap;->setPixels([IIIIIII)V

    .line 2804
    add-int/lit8 v9, v9, 0x1

    move/from16 v29, v30

    .end local v30    # "n":I
    .restart local v29    # "n":I
    goto :goto_f

    .line 2813
    .end local v5    # "line":[I
    .end local v7    # "maxW":I
    .end local v9    # "h":I
    .end local v14    # "cmap":[I
    .end local v20    # "count":I
    .end local v21    # "defaultDecode":Z
    .end local v24    # "i":I
    .end local v28    # "maxH":I
    .end local v29    # "n":I
    .end local v31    # "pixel":[I
    .end local v35    # "w":I
    :cond_14
    const/16 v6, 0x8

    move/from16 v0, p6

    if-ne v0, v6, :cond_16

    .line 2814
    sget-object v6, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    move/from16 v0, p3

    move/from16 v1, p4

    invoke-static {v0, v1, v6}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 2815
    move/from16 v28, p4

    .line 2816
    .restart local v28    # "maxH":I
    move/from16 v7, p3

    .line 2817
    .restart local v7    # "maxW":I
    new-array v5, v7, [I

    .line 2818
    .restart local v5    # "line":[I
    const/16 v29, 0x0

    .line 2819
    .restart local v29    # "n":I
    const/4 v9, 0x0

    .restart local v9    # "h":I
    :goto_11
    move/from16 v0, v28

    if-ge v9, v0, :cond_16

    .line 2820
    const/16 v35, 0x0

    .restart local v35    # "w":I
    :goto_12
    move/from16 v0, v35

    move/from16 v1, p3

    if-ge v0, v1, :cond_15

    .line 2821
    aget-byte v6, p15, v29

    and-int/lit16 v0, v6, 0xff

    move/from16 v23, v0

    .line 2822
    .local v23, "gray":I
    shl-int/lit8 v6, v23, 0x8

    or-int v6, v6, v23

    shl-int/lit8 v6, v6, 0x8

    or-int v6, v6, v23

    const/high16 v8, -0x1000000

    or-int/2addr v6, v8

    aput v6, v5, v35

    .line 2823
    add-int/lit8 v29, v29, 0x1

    .line 2820
    add-int/lit8 v35, v35, 0x1

    goto :goto_12

    .line 2825
    .end local v23    # "gray":I
    :cond_15
    const/4 v6, 0x0

    const/4 v8, 0x0

    const/4 v11, 0x1

    move v10, v7

    invoke-virtual/range {v4 .. v11}, Landroid/graphics/Bitmap;->setPixels([IIIIIII)V

    .line 2819
    add-int/lit8 v9, v9, 0x1

    goto :goto_11

    .line 2831
    .end local v5    # "line":[I
    .end local v7    # "maxW":I
    .end local v9    # "h":I
    .end local v28    # "maxH":I
    .end local v29    # "n":I
    .end local v35    # "w":I
    :cond_16
    if-eqz p10, :cond_17

    if-eqz v4, :cond_17

    .line 2832
    move-object/from16 v0, p10

    invoke-static {v4, v0}, Lorg/icepdf/core/pobjects/Stream;->applyExplicitMask(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 2836
    :cond_17
    if-eqz p9, :cond_18

    if-eqz v4, :cond_18

    .line 2837
    move-object/from16 v0, p9

    move-object/from16 v1, p10

    move-object/from16 v2, p11

    move-object/from16 v3, p12

    invoke-static {v4, v0, v1, v2, v3}, Lorg/icepdf/core/pobjects/Stream;->alterBufferedImage(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;[I[I)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 3014
    .end local p1    # "colourSpace":Lorg/icepdf/core/pobjects/graphics/PColorSpace;
    :cond_18
    :goto_13
    return-object v4

    .line 2840
    .restart local p1    # "colourSpace":Lorg/icepdf/core/pobjects/graphics/PColorSpace;
    :cond_19
    move-object/from16 v0, p1

    instance-of v6, v0, Lorg/icepdf/core/pobjects/graphics/DeviceRGB;

    if-eqz v6, :cond_1f

    .line 2841
    const/16 v6, 0x8

    move/from16 v0, p6

    if-ne v0, v6, :cond_18

    .line 2855
    mul-int v6, p3, p4

    mul-int/lit8 v6, v6, 0x4

    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Lorg/icepdf/core/pobjects/Stream;->checkMemory(I)Z

    .line 2856
    if-nez p9, :cond_1a

    if-nez p10, :cond_1a

    if-eqz p11, :cond_1c

    if-eqz p12, :cond_1c

    :cond_1a
    const/16 v33, 0x1

    .line 2857
    .local v33, "usingAlpha":Z
    :goto_14
    move/from16 v28, p4

    .line 2858
    .restart local v28    # "maxH":I
    move/from16 v7, p3

    .line 2859
    .restart local v7    # "maxW":I
    move-object/from16 v0, p15

    array-length v6, v0

    mul-int/lit8 v8, v7, 0x2

    mul-int v8, v8, v28

    if-ne v6, v8, :cond_1d

    .line 2861
    sget-object v6, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    move/from16 v0, v28

    invoke-static {v7, v0, v6}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 2862
    invoke-static/range {p15 .. p15}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v6

    invoke-virtual {v4, v6}, Landroid/graphics/Bitmap;->copyPixelsFromBuffer(Ljava/nio/Buffer;)V

    .line 2878
    :cond_1b
    if-eqz v33, :cond_18

    .line 2879
    move-object/from16 v0, p9

    move-object/from16 v1, p10

    move-object/from16 v2, p11

    move-object/from16 v3, p12

    invoke-static {v4, v0, v1, v2, v3}, Lorg/icepdf/core/pobjects/Stream;->alterBufferedImage(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;[I[I)Landroid/graphics/Bitmap;

    move-result-object v4

    goto :goto_13

    .line 2856
    .end local v7    # "maxW":I
    .end local v28    # "maxH":I
    .end local v33    # "usingAlpha":Z
    :cond_1c
    const/16 v33, 0x0

    goto :goto_14

    .line 2866
    .restart local v7    # "maxW":I
    .restart local v28    # "maxH":I
    .restart local v33    # "usingAlpha":Z
    :cond_1d
    sget-object v6, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    move/from16 v0, p3

    move/from16 v1, p4

    invoke-static {v0, v1, v6}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 2867
    new-array v5, v7, [I

    .line 2868
    .restart local v5    # "line":[I
    const/16 v29, 0x0

    .line 2869
    .restart local v29    # "n":I
    const/4 v9, 0x0

    .restart local v9    # "h":I
    :goto_15
    move/from16 v0, v28

    if-ge v9, v0, :cond_1b

    .line 2870
    const/16 v35, 0x0

    .restart local v35    # "w":I
    :goto_16
    move/from16 v0, v35

    move/from16 v1, p3

    if-ge v0, v1, :cond_1e

    .line 2871
    aget-byte v6, p15, v29

    and-int/lit16 v6, v6, 0xff

    shl-int/lit8 v6, v6, 0x8

    add-int/lit8 v8, v29, 0x1

    aget-byte v8, p15, v8

    and-int/lit16 v8, v8, 0xff

    or-int/2addr v6, v8

    shl-int/lit8 v6, v6, 0x8

    add-int/lit8 v8, v29, 0x2

    aget-byte v8, p15, v8

    and-int/lit16 v8, v8, 0xff

    or-int/2addr v6, v8

    const/high16 v8, -0x1000000

    or-int/2addr v6, v8

    aput v6, v5, v35

    .line 2873
    add-int/lit8 v29, v29, 0x3

    .line 2870
    add-int/lit8 v35, v35, 0x1

    goto :goto_16

    .line 2875
    :cond_1e
    const/4 v6, 0x0

    const/4 v8, 0x0

    const/4 v11, 0x1

    move v10, v7

    invoke-virtual/range {v4 .. v11}, Landroid/graphics/Bitmap;->setPixels([IIIIIII)V

    .line 2869
    add-int/lit8 v9, v9, 0x1

    goto :goto_15

    .line 2883
    .end local v5    # "line":[I
    .end local v7    # "maxW":I
    .end local v9    # "h":I
    .end local v28    # "maxH":I
    .end local v29    # "n":I
    .end local v33    # "usingAlpha":Z
    .end local v35    # "w":I
    :cond_1f
    move-object/from16 v0, p1

    instance-of v6, v0, Lorg/icepdf/core/pobjects/graphics/DeviceCMYK;

    if-nez v6, :cond_18

    .line 2911
    move-object/from16 v0, p1

    instance-of v6, v0, Lorg/icepdf/core/pobjects/graphics/Indexed;

    if-eqz v6, :cond_18

    .line 2912
    const/4 v6, 0x1

    move/from16 v0, p6

    if-eq v0, v6, :cond_20

    const/4 v6, 0x2

    move/from16 v0, p6

    if-eq v0, v6, :cond_20

    const/4 v6, 0x4

    move/from16 v0, p6

    if-ne v0, v6, :cond_2b

    .line 2913
    :cond_20
    sget-boolean v6, Lorg/icepdf/core/tag/Tagger;->tagging:Z

    if-eqz v6, :cond_21

    .line 2914
    const-string/jumbo v6, "HandledBy=RasterFromBytes_Indexed_124"

    invoke-static {v6}, Lorg/icepdf/core/tag/Tagger;->tagImage(Ljava/lang/String;)V

    .line 2915
    :cond_21
    invoke-virtual/range {p1 .. p1}, Lorg/icepdf/core/pobjects/graphics/PColorSpace;->init()V

    .line 2916
    check-cast p1, Lorg/icepdf/core/pobjects/graphics/Indexed;

    .end local p1    # "colourSpace":Lorg/icepdf/core/pobjects/graphics/PColorSpace;
    invoke-virtual/range {p1 .. p1}, Lorg/icepdf/core/pobjects/graphics/Indexed;->accessColorTable()[Lorg/apache/poi/java/awt/Color;

    move-result-object v18

    .line 2917
    .local v18, "colors":[Lorg/apache/poi/java/awt/Color;
    if-nez v18, :cond_23

    const/4 v6, 0x0

    :goto_17
    new-array v14, v6, [I

    .line 2918
    .restart local v14    # "cmap":[I
    const/16 v24, 0x0

    .restart local v24    # "i":I
    :goto_18
    array-length v6, v14

    move/from16 v0, v24

    if-ge v0, v6, :cond_24

    .line 2920
    if-eqz v18, :cond_22

    .line 2921
    aget-object v6, v18, v24

    invoke-virtual {v6}, Lorg/apache/poi/java/awt/Color;->getRGB()I

    move-result v6

    aput v6, v14, v24

    .line 2918
    :cond_22
    add-int/lit8 v24, v24, 0x1

    goto :goto_18

    .line 2917
    .end local v14    # "cmap":[I
    .end local v24    # "i":I
    :cond_23
    move-object/from16 v0, v18

    array-length v6, v0

    goto :goto_17

    .line 2923
    .restart local v14    # "cmap":[I
    .restart local v24    # "i":I
    :cond_24
    const/4 v6, 0x1

    shl-int v16, v6, p6

    .line 2924
    .local v16, "cmapMaxLength":I
    array-length v6, v14

    move/from16 v0, v16

    if-le v6, v0, :cond_25

    .line 2925
    move/from16 v0, v16

    new-array v0, v0, [I

    move-object/from16 v17, v0

    .line 2926
    .local v17, "cmapTruncated":[I
    const/4 v6, 0x0

    const/4 v8, 0x0

    move-object/from16 v0, v17

    move/from16 v1, v16

    invoke-static {v14, v6, v0, v8, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2927
    move-object/from16 v14, v17

    .line 2929
    .end local v17    # "cmapTruncated":[I
    :cond_25
    if-ltz p13, :cond_27

    if-ltz p14, :cond_27

    const/16 v34, 0x1

    .line 2930
    .local v34, "usingIndexedAlpha":Z
    :goto_19
    if-nez p9, :cond_26

    if-nez p10, :cond_26

    if-eqz p11, :cond_28

    if-eqz p12, :cond_28

    :cond_26
    const/16 v33, 0x1

    .line 2932
    .restart local v33    # "usingAlpha":Z
    :goto_1a
    sget-boolean v6, Lorg/icepdf/core/tag/Tagger;->tagging:Z

    if-eqz v6, :cond_18

    .line 2933
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "RasterFromBytes_Indexed_124_alpha="

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    if-eqz v34, :cond_29

    const-string/jumbo v6, "indexed"

    :goto_1b
    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lorg/icepdf/core/tag/Tagger;->tagImage(Ljava/lang/String;)V

    goto/16 :goto_13

    .line 2929
    .end local v33    # "usingAlpha":Z
    .end local v34    # "usingIndexedAlpha":Z
    :cond_27
    const/16 v34, 0x0

    goto :goto_19

    .line 2930
    .restart local v34    # "usingIndexedAlpha":Z
    :cond_28
    const/16 v33, 0x0

    goto :goto_1a

    .line 2933
    .restart local v33    # "usingAlpha":Z
    :cond_29
    if-eqz v33, :cond_2a

    const-string/jumbo v6, "alpha"

    goto :goto_1b

    :cond_2a
    const-string/jumbo v6, "false"

    goto :goto_1b

    .line 2947
    .end local v14    # "cmap":[I
    .end local v16    # "cmapMaxLength":I
    .end local v18    # "colors":[Lorg/apache/poi/java/awt/Color;
    .end local v24    # "i":I
    .end local v33    # "usingAlpha":Z
    .end local v34    # "usingIndexedAlpha":Z
    .restart local p1    # "colourSpace":Lorg/icepdf/core/pobjects/graphics/PColorSpace;
    :cond_2b
    const/16 v6, 0x8

    move/from16 v0, p6

    if-ne v0, v6, :cond_18

    .line 2948
    sget-boolean v6, Lorg/icepdf/core/tag/Tagger;->tagging:Z

    if-eqz v6, :cond_2c

    .line 2949
    const-string/jumbo v6, "HandledBy=RasterFromBytes_Indexed_8"

    invoke-static {v6}, Lorg/icepdf/core/tag/Tagger;->tagImage(Ljava/lang/String;)V

    .line 2952
    :cond_2c
    invoke-virtual/range {p1 .. p1}, Lorg/icepdf/core/pobjects/graphics/PColorSpace;->init()V

    .line 2953
    check-cast p1, Lorg/icepdf/core/pobjects/graphics/Indexed;

    .end local p1    # "colourSpace":Lorg/icepdf/core/pobjects/graphics/PColorSpace;
    invoke-virtual/range {p1 .. p1}, Lorg/icepdf/core/pobjects/graphics/Indexed;->accessColorTable()[Lorg/apache/poi/java/awt/Color;

    move-result-object v18

    .line 2954
    .restart local v18    # "colors":[Lorg/apache/poi/java/awt/Color;
    if-nez v18, :cond_2d

    const/16 v19, 0x0

    .line 2955
    .local v19, "colorsLength":I
    :goto_1c
    const/16 v6, 0x100

    new-array v14, v6, [I

    .line 2956
    .restart local v14    # "cmap":[I
    const/16 v24, 0x0

    .restart local v24    # "i":I
    :goto_1d
    move/from16 v0, v24

    move/from16 v1, v19

    if-ge v0, v1, :cond_2e

    .line 2957
    aget-object v6, v18, v24

    invoke-virtual {v6}, Lorg/apache/poi/java/awt/Color;->getRGB()I

    move-result v6

    aput v6, v14, v24

    .line 2956
    add-int/lit8 v24, v24, 0x1

    goto :goto_1d

    .line 2954
    .end local v14    # "cmap":[I
    .end local v19    # "colorsLength":I
    .end local v24    # "i":I
    :cond_2d
    move-object/from16 v0, v18

    array-length v0, v0

    move/from16 v19, v0

    goto :goto_1c

    .line 2959
    .restart local v14    # "cmap":[I
    .restart local v19    # "colorsLength":I
    .restart local v24    # "i":I
    :cond_2e
    move/from16 v24, v19

    :goto_1e
    array-length v6, v14

    move/from16 v0, v24

    if-ge v0, v6, :cond_2f

    .line 2960
    const/high16 v6, -0x1000000

    aput v6, v14, v24

    .line 2959
    add-int/lit8 v24, v24, 0x1

    goto :goto_1e

    .line 2963
    :cond_2f
    if-ltz p13, :cond_32

    if-ltz p14, :cond_32

    const/16 v34, 0x1

    .line 2964
    .restart local v34    # "usingIndexedAlpha":Z
    :goto_1f
    if-nez p9, :cond_30

    if-nez p10, :cond_30

    if-eqz p11, :cond_33

    if-eqz p12, :cond_33

    :cond_30
    const/16 v33, 0x1

    .line 2965
    .restart local v33    # "usingAlpha":Z
    :goto_20
    sget-boolean v6, Lorg/icepdf/core/tag/Tagger;->tagging:Z

    if-eqz v6, :cond_31

    .line 2966
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "RasterFromBytes_Indexed_8_alpha="

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    if-eqz v34, :cond_34

    const-string/jumbo v6, "indexed"

    :goto_21
    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lorg/icepdf/core/tag/Tagger;->tagImage(Ljava/lang/String;)V

    .line 2998
    :cond_31
    move/from16 v28, p4

    .line 2999
    .restart local v28    # "maxH":I
    move/from16 v7, p3

    .line 3000
    .restart local v7    # "maxW":I
    sget-object v6, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    move/from16 v0, p3

    move/from16 v1, p4

    invoke-static {v0, v1, v6}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 3001
    new-array v5, v7, [I

    .line 3002
    .restart local v5    # "line":[I
    const/16 v29, 0x0

    .line 3003
    .restart local v29    # "n":I
    const/4 v9, 0x0

    .restart local v9    # "h":I
    :goto_22
    move/from16 v0, v28

    if-ge v9, v0, :cond_18

    .line 3004
    const/16 v35, 0x0

    .restart local v35    # "w":I
    :goto_23
    move/from16 v0, v35

    move/from16 v1, p3

    if-ge v0, v1, :cond_36

    .line 3006
    aget-byte v6, p15, v29

    and-int/lit16 v15, v6, 0xff

    .line 3007
    .local v15, "cmapIndex":I
    aget v6, v14, v15

    aput v6, v5, v35

    .line 3008
    add-int/lit8 v29, v29, 0x1

    .line 3004
    add-int/lit8 v35, v35, 0x1

    goto :goto_23

    .line 2963
    .end local v5    # "line":[I
    .end local v7    # "maxW":I
    .end local v9    # "h":I
    .end local v15    # "cmapIndex":I
    .end local v28    # "maxH":I
    .end local v29    # "n":I
    .end local v33    # "usingAlpha":Z
    .end local v34    # "usingIndexedAlpha":Z
    .end local v35    # "w":I
    :cond_32
    const/16 v34, 0x0

    goto :goto_1f

    .line 2964
    .restart local v34    # "usingIndexedAlpha":Z
    :cond_33
    const/16 v33, 0x0

    goto :goto_20

    .line 2966
    .restart local v33    # "usingAlpha":Z
    :cond_34
    if-eqz v33, :cond_35

    const-string/jumbo v6, "alpha"

    goto :goto_21

    :cond_35
    const-string/jumbo v6, "false"

    goto :goto_21

    .line 3010
    .restart local v5    # "line":[I
    .restart local v7    # "maxW":I
    .restart local v9    # "h":I
    .restart local v28    # "maxH":I
    .restart local v29    # "n":I
    .restart local v35    # "w":I
    :cond_36
    const/4 v6, 0x0

    const/4 v8, 0x0

    const/4 v11, 0x1

    move v10, v7

    invoke-virtual/range {v4 .. v11}, Landroid/graphics/Bitmap;->setPixels([IIIIIII)V

    .line 3003
    add-int/lit8 v9, v9, 0x1

    goto :goto_22
.end method

.method private shouldUseCCITTFaxDecode()Z
    .locals 3

    .prologue
    .line 500
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v2, "CCITTFaxDecode"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string/jumbo v2, "/CCF"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string/jumbo v2, "CCF"

    aput-object v2, v0, v1

    invoke-direct {p0, v0}, Lorg/icepdf/core/pobjects/Stream;->containsFilter([Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private shouldUseDCTDecode()Z
    .locals 3

    .prologue
    .line 504
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v2, "DCTDecode"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string/jumbo v2, "/DCT"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string/jumbo v2, "DCT"

    aput-object v2, v0, v1

    invoke-direct {p0, v0}, Lorg/icepdf/core/pobjects/Stream;->containsFilter([Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private shouldUseJBIG2Decode()Z
    .locals 3

    .prologue
    .line 508
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v2, "JBIG2Decode"

    aput-object v2, v0, v1

    invoke-direct {p0, v0}, Lorg/icepdf/core/pobjects/Stream;->containsFilter([Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private shouldUseJPXDecode()Z
    .locals 3

    .prologue
    .line 512
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v2, "JPXDecode"

    aput-object v2, v0, v1

    invoke-direct {p0, v0}, Lorg/icepdf/core/pobjects/Stream;->containsFilter([Ljava/lang/String;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public dispose(Z)V
    .locals 5
    .param p1, "cache"    # Z

    .prologue
    const/4 v4, 0x0

    .line 596
    iget-object v1, p0, Lorg/icepdf/core/pobjects/Stream;->streamInput:Lorg/icepdf/core/io/SeekableInputConstrainedWrapper;

    if-eqz v1, :cond_0

    .line 597
    if-nez p1, :cond_3

    .line 599
    :try_start_0
    iget-object v1, p0, Lorg/icepdf/core/pobjects/Stream;->streamInput:Lorg/icepdf/core/io/SeekableInputConstrainedWrapper;

    invoke-virtual {v1}, Lorg/icepdf/core/io/SeekableInputConstrainedWrapper;->dispose()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 603
    :goto_0
    iput-object v4, p0, Lorg/icepdf/core/pobjects/Stream;->streamInput:Lorg/icepdf/core/io/SeekableInputConstrainedWrapper;

    .line 608
    :cond_0
    :goto_1
    iget-object v2, p0, Lorg/icepdf/core/pobjects/Stream;->imageLock:Ljava/lang/Object;

    monitor-enter v2

    .line 609
    :try_start_1
    iget-object v1, p0, Lorg/icepdf/core/pobjects/Stream;->image:Lorg/icepdf/core/util/ImageCache;

    if-eqz v1, :cond_2

    .line 610
    iget-object v3, p0, Lorg/icepdf/core/pobjects/Stream;->image:Lorg/icepdf/core/util/ImageCache;

    iget-object v1, p0, Lorg/icepdf/core/pobjects/Stream;->streamInput:Lorg/icepdf/core/io/SeekableInputConstrainedWrapper;

    if-eqz v1, :cond_4

    const/4 v1, 0x1

    :goto_2
    invoke-virtual {v3, p1, v1}, Lorg/icepdf/core/util/ImageCache;->dispose(ZZ)V

    .line 611
    if-eqz p1, :cond_1

    iget-object v1, p0, Lorg/icepdf/core/pobjects/Stream;->image:Lorg/icepdf/core/util/ImageCache;

    invoke-virtual {v1}, Lorg/icepdf/core/util/ImageCache;->isCachedSomehow()Z

    move-result v1

    if-nez v1, :cond_2

    .line 612
    :cond_1
    const/4 v1, 0x0

    iput-object v1, p0, Lorg/icepdf/core/pobjects/Stream;->image:Lorg/icepdf/core/util/ImageCache;

    .line 615
    :cond_2
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 616
    return-void

    .line 600
    :catch_0
    move-exception v0

    .line 601
    .local v0, "e":Ljava/io/IOException;
    sget-object v1, Lorg/icepdf/core/pobjects/Stream;->logger:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string/jumbo v3, "Error disposing stream."

    invoke-virtual {v1, v2, v3, v0}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 605
    .end local v0    # "e":Ljava/io/IOException;
    :cond_3
    iget-object v1, p0, Lorg/icepdf/core/pobjects/Stream;->library:Lorg/icepdf/core/util/Library;

    invoke-virtual {p0}, Lorg/icepdf/core/pobjects/Stream;->getPObjectReference()Lorg/icepdf/core/pobjects/Reference;

    move-result-object v2

    invoke-virtual {v1, v2}, Lorg/icepdf/core/util/Library;->removeObject(Lorg/icepdf/core/pobjects/Reference;)V

    goto :goto_1

    .line 610
    :cond_4
    const/4 v1, 0x0

    goto :goto_2

    .line 615
    :catchall_0
    move-exception v1

    :try_start_2
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1
.end method

.method public getBlackIs1(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)Z
    .locals 2
    .param p1, "library"    # Lorg/icepdf/core/util/Library;
    .param p2, "decodeParmsDictionary"    # Ljava/util/Hashtable;

    .prologue
    .line 3559
    invoke-virtual {p0, p1, p2}, Lorg/icepdf/core/pobjects/Stream;->getBlackIs1OrNull(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)Ljava/lang/Boolean;

    move-result-object v0

    .line 3560
    .local v0, "blackIs1":Ljava/lang/Boolean;
    if-eqz v0, :cond_0

    .line 3561
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 3562
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getBlackIs1OrNull(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)Ljava/lang/Boolean;
    .locals 3
    .param p1, "library"    # Lorg/icepdf/core/util/Library;
    .param p2, "decodeParmsDictionary"    # Ljava/util/Hashtable;

    .prologue
    .line 3570
    const-string/jumbo v2, "BlackIs1"

    invoke-virtual {p1, p2, v2}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 3571
    .local v0, "blackIs1Obj":Ljava/lang/Object;
    if-eqz v0, :cond_6

    .line 3572
    instance-of v2, v0, Ljava/lang/Boolean;

    if-eqz v2, :cond_0

    .line 3573
    check-cast v0, Ljava/lang/Boolean;

    .line 3590
    .end local v0    # "blackIs1Obj":Ljava/lang/Object;
    :goto_0
    return-object v0

    .line 3574
    .restart local v0    # "blackIs1Obj":Ljava/lang/Object;
    :cond_0
    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_6

    move-object v1, v0

    .line 3575
    check-cast v1, Ljava/lang/String;

    .line 3576
    .local v1, "blackIs1String":Ljava/lang/String;
    const-string/jumbo v2, "true"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 3577
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    goto :goto_0

    .line 3578
    :cond_1
    const-string/jumbo v2, "t"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 3579
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    goto :goto_0

    .line 3580
    :cond_2
    const-string/jumbo v2, "1"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 3581
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    goto :goto_0

    .line 3582
    :cond_3
    const-string/jumbo v2, "false"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 3583
    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    goto :goto_0

    .line 3584
    :cond_4
    const-string/jumbo v2, "f"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 3585
    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    goto :goto_0

    .line 3586
    :cond_5
    const-string/jumbo v2, "0"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 3587
    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    goto :goto_0

    .line 3590
    .end local v1    # "blackIs1String":Ljava/lang/String;
    :cond_6
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getBytes()[B
    .locals 2

    .prologue
    .line 2274
    invoke-direct {p0}, Lorg/icepdf/core/pobjects/Stream;->getDecodedStreamBytes()[B

    move-result-object v0

    .line 2275
    .local v0, "data":[B
    if-nez v0, :cond_0

    .line 2276
    const/4 v1, 0x0

    new-array v0, v1, [B

    .line 2277
    :cond_0
    return-object v0
.end method

.method public getImage(Lorg/apache/poi/java/awt/Color;Lorg/icepdf/core/pobjects/Resources;ZZ)Landroid/graphics/Bitmap;
    .locals 40
    .param p1, "fill"    # Lorg/apache/poi/java/awt/Color;
    .param p2, "resources"    # Lorg/icepdf/core/pobjects/Resources;
    .param p3, "allowScaling"    # Z
    .param p4, "mask"    # Z

    .prologue
    .line 2294
    sget-boolean v4, Lorg/icepdf/core/tag/Tagger;->tagging:Z

    if-eqz v4, :cond_0

    .line 2295
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/icepdf/core/pobjects/Stream;->pObjectReference:Lorg/icepdf/core/pobjects/Reference;

    move-object/from16 v0, p0

    iget-boolean v6, v0, Lorg/icepdf/core/pobjects/Stream;->inlineImage:Z

    invoke-static {v4, v6}, Lorg/icepdf/core/tag/Tagger;->beginImage(Lorg/icepdf/core/pobjects/Reference;Z)V

    .line 2299
    :cond_0
    sget-boolean v4, Lorg/icepdf/core/tag/Tagger;->tagging:Z

    if-eqz v4, :cond_1

    .line 2300
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "Filter="

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-direct/range {p0 .. p0}, Lorg/icepdf/core/pobjects/Stream;->getNormalisedFilterNames()Ljava/util/Vector;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lorg/icepdf/core/tag/Tagger;->tagImage(Ljava/lang/String;)V

    .line 2303
    :cond_1
    const/4 v5, 0x0

    .line 2304
    .local v5, "colourSpace":Lorg/icepdf/core/pobjects/graphics/PColorSpace;
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/icepdf/core/pobjects/Stream;->library:Lorg/icepdf/core/util/Library;

    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/icepdf/core/pobjects/Stream;->entries:Ljava/util/Hashtable;

    const-string/jumbo v36, "ColorSpace"

    move-object/from16 v0, v36

    invoke-virtual {v4, v6, v0}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v33

    .line 2305
    .local v33, "o":Ljava/lang/Object;
    if-eqz p2, :cond_2

    .line 2306
    move-object/from16 v0, p2

    move-object/from16 v1, v33

    invoke-virtual {v0, v1}, Lorg/icepdf/core/pobjects/Resources;->getColorSpace(Ljava/lang/Object;)Lorg/icepdf/core/pobjects/graphics/PColorSpace;

    move-result-object v5

    .line 2310
    :cond_2
    if-nez v5, :cond_3

    .line 2311
    new-instance v5, Lorg/icepdf/core/pobjects/graphics/DeviceGray;

    .end local v5    # "colourSpace":Lorg/icepdf/core/pobjects/graphics/PColorSpace;
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/icepdf/core/pobjects/Stream;->library:Lorg/icepdf/core/util/Library;

    const/4 v6, 0x0

    invoke-direct {v5, v4, v6}, Lorg/icepdf/core/pobjects/graphics/DeviceGray;-><init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V

    .line 2312
    .restart local v5    # "colourSpace":Lorg/icepdf/core/pobjects/graphics/PColorSpace;
    sget-boolean v4, Lorg/icepdf/core/tag/Tagger;->tagging:Z

    if-eqz v4, :cond_3

    .line 2313
    const-string/jumbo v4, "ColorSpace_Implicit_DeviceGray"

    invoke-static {v4}, Lorg/icepdf/core/tag/Tagger;->tagImage(Ljava/lang/String;)V

    .line 2315
    :cond_3
    sget-boolean v4, Lorg/icepdf/core/tag/Tagger;->tagging:Z

    if-eqz v4, :cond_4

    .line 2316
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "ColorSpace="

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v5}, Lorg/icepdf/core/pobjects/graphics/PColorSpace;->getDescription()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lorg/icepdf/core/tag/Tagger;->tagImage(Ljava/lang/String;)V

    .line 2318
    :cond_4
    invoke-virtual/range {p0 .. p0}, Lorg/icepdf/core/pobjects/Stream;->isImageMask()Z

    move-result v11

    .line 2319
    .local v11, "imageMask":Z
    sget-boolean v4, Lorg/icepdf/core/tag/Tagger;->tagging:Z

    if-eqz v4, :cond_5

    .line 2320
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "ImageMask="

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v11}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lorg/icepdf/core/tag/Tagger;->tagImage(Ljava/lang/String;)V

    .line 2321
    :cond_5
    if-eqz v11, :cond_6

    .line 2322
    const/16 p3, 0x0

    .line 2325
    :cond_6
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/icepdf/core/pobjects/Stream;->library:Lorg/icepdf/core/util/Library;

    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/icepdf/core/pobjects/Stream;->entries:Ljava/util/Hashtable;

    const-string/jumbo v36, "BitsPerComponent"

    move-object/from16 v0, v36

    invoke-virtual {v4, v6, v0}, Lorg/icepdf/core/util/Library;->getInt(Ljava/util/Hashtable;Ljava/lang/String;)I

    move-result v10

    .line 2326
    .local v10, "bitspercomponent":I
    if-eqz v11, :cond_7

    if-nez v10, :cond_7

    .line 2327
    const/4 v10, 0x1

    .line 2328
    sget-boolean v4, Lorg/icepdf/core/tag/Tagger;->tagging:Z

    if-eqz v4, :cond_7

    .line 2329
    const-string/jumbo v4, "BitsPerComponent_Implicit_1"

    invoke-static {v4}, Lorg/icepdf/core/tag/Tagger;->tagImage(Ljava/lang/String;)V

    .line 2331
    :cond_7
    sget-boolean v4, Lorg/icepdf/core/tag/Tagger;->tagging:Z

    if-eqz v4, :cond_8

    .line 2332
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "BitsPerComponent="

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lorg/icepdf/core/tag/Tagger;->tagImage(Ljava/lang/String;)V

    .line 2335
    :cond_8
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/icepdf/core/pobjects/Stream;->library:Lorg/icepdf/core/util/Library;

    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/icepdf/core/pobjects/Stream;->entries:Ljava/util/Hashtable;

    const-string/jumbo v36, "Width"

    move-object/from16 v0, v36

    invoke-virtual {v4, v6, v0}, Lorg/icepdf/core/util/Library;->getInt(Ljava/util/Hashtable;Ljava/lang/String;)I

    move-result v7

    .line 2336
    .local v7, "width":I
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/icepdf/core/pobjects/Stream;->library:Lorg/icepdf/core/util/Library;

    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/icepdf/core/pobjects/Stream;->entries:Ljava/util/Hashtable;

    const-string/jumbo v36, "Height"

    move-object/from16 v0, v36

    invoke-virtual {v4, v6, v0}, Lorg/icepdf/core/util/Library;->getInt(Ljava/util/Hashtable;Ljava/lang/String;)I

    move-result v8

    .line 2341
    .local v8, "height":I
    if-nez v8, :cond_a

    .line 2342
    const-wide/high16 v36, 0x3ff0000000000000L    # 1.0

    sget-wide v38, Lorg/icepdf/core/pobjects/Stream;->pageRatio:D

    div-double v36, v36, v38

    int-to-double v0, v7

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-wide/from16 v0, v36

    double-to-int v8, v0

    .line 2349
    :cond_9
    :goto_0
    invoke-virtual {v5}, Lorg/icepdf/core/pobjects/graphics/PColorSpace;->getNumComponents()I

    move-result v9

    .line 2352
    .local v9, "colorSpaceCompCount":I
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/icepdf/core/pobjects/Stream;->library:Lorg/icepdf/core/util/Library;

    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/icepdf/core/pobjects/Stream;->entries:Ljava/util/Hashtable;

    const-string/jumbo v36, "Decode"

    move-object/from16 v0, v36

    invoke-virtual {v4, v6, v0}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/util/Vector;

    .line 2353
    .local v12, "decode":Ljava/util/Vector;, "Ljava/util/Vector<Ljava/lang/Integer;>;"
    if-nez v12, :cond_c

    .line 2354
    invoke-virtual {v5}, Lorg/icepdf/core/pobjects/graphics/PColorSpace;->getNumComponents()I

    move-result v20

    .line 2355
    .local v20, "depth":I
    new-instance v12, Ljava/util/Vector;

    .end local v12    # "decode":Ljava/util/Vector;, "Ljava/util/Vector<Ljava/lang/Integer;>;"
    move/from16 v0, v20

    invoke-direct {v12, v0}, Ljava/util/Vector;-><init>(I)V

    .line 2357
    .restart local v12    # "decode":Ljava/util/Vector;, "Ljava/util/Vector<Ljava/lang/Integer;>;"
    const/16 v21, 0x0

    .local v21, "i":I
    :goto_1
    move/from16 v0, v21

    move/from16 v1, v20

    if-ge v0, v1, :cond_b

    .line 2358
    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v12, v4}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    .line 2359
    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v12, v4}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    .line 2357
    add-int/lit8 v21, v21, 0x1

    goto :goto_1

    .line 2343
    .end local v9    # "colorSpaceCompCount":I
    .end local v12    # "decode":Ljava/util/Vector;, "Ljava/util/Vector<Ljava/lang/Integer;>;"
    .end local v20    # "depth":I
    .end local v21    # "i":I
    :cond_a
    if-nez v7, :cond_9

    .line 2344
    sget-wide v36, Lorg/icepdf/core/pobjects/Stream;->pageRatio:D

    int-to-double v0, v8

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-wide/from16 v0, v36

    double-to-int v7, v0

    goto :goto_0

    .line 2361
    .restart local v9    # "colorSpaceCompCount":I
    .restart local v12    # "decode":Ljava/util/Vector;, "Ljava/util/Vector<Ljava/lang/Integer;>;"
    .restart local v20    # "depth":I
    .restart local v21    # "i":I
    :cond_b
    sget-boolean v4, Lorg/icepdf/core/tag/Tagger;->tagging:Z

    if-eqz v4, :cond_c

    .line 2362
    const-string/jumbo v4, "Decode_Implicit_01"

    invoke-static {v4}, Lorg/icepdf/core/tag/Tagger;->tagImage(Ljava/lang/String;)V

    .line 2364
    .end local v20    # "depth":I
    .end local v21    # "i":I
    :cond_c
    sget-boolean v4, Lorg/icepdf/core/tag/Tagger;->tagging:Z

    if-eqz v4, :cond_d

    .line 2365
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "Decode="

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lorg/icepdf/core/tag/Tagger;->tagImage(Ljava/lang/String;)V

    .line 2367
    :cond_d
    const/4 v13, 0x0

    .line 2368
    .local v13, "smaskImage":Landroid/graphics/Bitmap;
    const/4 v14, 0x0

    .line 2369
    .local v14, "maskImage":Landroid/graphics/Bitmap;
    const/4 v15, 0x0

    .line 2370
    .local v15, "maskMinRGB":[I
    const/16 v16, 0x0

    .line 2371
    .local v16, "maskMaxRGB":[I
    const/16 v17, -0x1

    .line 2372
    .local v17, "maskMinIndex":I
    const/16 v18, -0x1

    .line 2373
    .local v18, "maskMaxIndex":I
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/icepdf/core/pobjects/Stream;->library:Lorg/icepdf/core/util/Library;

    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/icepdf/core/pobjects/Stream;->entries:Ljava/util/Hashtable;

    const-string/jumbo v36, "SMask"

    move-object/from16 v0, v36

    invoke-virtual {v4, v6, v0}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v34

    .line 2374
    .local v34, "smaskObj":Ljava/lang/Object;
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/icepdf/core/pobjects/Stream;->library:Lorg/icepdf/core/util/Library;

    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/icepdf/core/pobjects/Stream;->entries:Ljava/util/Hashtable;

    const-string/jumbo v36, "Mask"

    move-object/from16 v0, v36

    invoke-virtual {v4, v6, v0}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v28

    .line 2375
    .local v28, "maskObj":Ljava/lang/Object;
    move-object/from16 v0, v34

    instance-of v4, v0, Lorg/icepdf/core/pobjects/Stream;

    if-eqz v4, :cond_f

    .line 2376
    sget-boolean v4, Lorg/icepdf/core/tag/Tagger;->tagging:Z

    if-eqz v4, :cond_e

    .line 2377
    const-string/jumbo v4, "SMaskStream"

    invoke-static {v4}, Lorg/icepdf/core/tag/Tagger;->tagImage(Ljava/lang/String;)V

    :cond_e
    move-object/from16 v35, v34

    .line 2378
    check-cast v35, Lorg/icepdf/core/pobjects/Stream;

    .line 2379
    .local v35, "smaskStream":Lorg/icepdf/core/pobjects/Stream;
    invoke-virtual/range {v35 .. v35}, Lorg/icepdf/core/pobjects/Stream;->isImageSubtype()Z

    move-result v4

    if-eqz v4, :cond_f

    .line 2380
    const/4 v4, 0x0

    const/4 v6, 0x1

    move-object/from16 v0, v35

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    invoke-virtual {v0, v1, v2, v4, v6}, Lorg/icepdf/core/pobjects/Stream;->getImage(Lorg/apache/poi/java/awt/Color;Lorg/icepdf/core/pobjects/Resources;ZZ)Landroid/graphics/Bitmap;

    move-result-object v13

    .line 2382
    .end local v35    # "smaskStream":Lorg/icepdf/core/pobjects/Stream;
    :cond_f
    if-eqz v13, :cond_11

    .line 2383
    sget-boolean v4, Lorg/icepdf/core/tag/Tagger;->tagging:Z

    if-eqz v4, :cond_10

    .line 2384
    const-string/jumbo v4, "SMaskImage"

    invoke-static {v4}, Lorg/icepdf/core/tag/Tagger;->tagImage(Ljava/lang/String;)V

    .line 2385
    :cond_10
    const/16 p3, 0x0

    .line 2387
    :cond_11
    if-eqz v28, :cond_13

    if-nez v13, :cond_13

    .line 2388
    move-object/from16 v0, v28

    instance-of v4, v0, Lorg/icepdf/core/pobjects/Stream;

    if-eqz v4, :cond_17

    .line 2389
    sget-boolean v4, Lorg/icepdf/core/tag/Tagger;->tagging:Z

    if-eqz v4, :cond_12

    .line 2390
    const-string/jumbo v4, "MaskStream"

    invoke-static {v4}, Lorg/icepdf/core/tag/Tagger;->tagImage(Ljava/lang/String;)V

    :cond_12
    move-object/from16 v29, v28

    .line 2391
    check-cast v29, Lorg/icepdf/core/pobjects/Stream;

    .line 2392
    .local v29, "maskStream":Lorg/icepdf/core/pobjects/Stream;
    invoke-virtual/range {v29 .. v29}, Lorg/icepdf/core/pobjects/Stream;->isImageSubtype()Z

    move-result v4

    if-eqz v4, :cond_13

    .line 2393
    const/4 v4, 0x0

    const/4 v6, 0x1

    move-object/from16 v0, v29

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    invoke-virtual {v0, v1, v2, v4, v6}, Lorg/icepdf/core/pobjects/Stream;->getImage(Lorg/apache/poi/java/awt/Color;Lorg/icepdf/core/pobjects/Resources;ZZ)Landroid/graphics/Bitmap;

    move-result-object v14

    .line 2394
    if-eqz v14, :cond_13

    .line 2395
    sget-boolean v4, Lorg/icepdf/core/tag/Tagger;->tagging:Z

    if-eqz v4, :cond_13

    .line 2396
    const-string/jumbo v4, "MaskImage"

    invoke-static {v4}, Lorg/icepdf/core/tag/Tagger;->tagImage(Ljava/lang/String;)V

    .end local v29    # "maskStream":Lorg/icepdf/core/pobjects/Stream;
    :cond_13
    :goto_2
    move-object/from16 v4, p0

    move-object/from16 v6, p1

    .line 2445
    invoke-direct/range {v4 .. v18}, Lorg/icepdf/core/pobjects/Stream;->getImage(Lorg/icepdf/core/pobjects/graphics/PColorSpace;Lorg/apache/poi/java/awt/Color;IIIIZLjava/util/Vector;Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;[I[III)Landroid/graphics/Bitmap;

    move-result-object v23

    .line 2456
    .local v23, "img":Landroid/graphics/Bitmap;
    if-eqz v23, :cond_14

    .line 2461
    :cond_14
    sget-boolean v4, Lorg/icepdf/core/tag/Tagger;->tagging:Z

    if-eqz v4, :cond_15

    .line 2462
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/icepdf/core/pobjects/Stream;->pObjectReference:Lorg/icepdf/core/pobjects/Reference;

    invoke-static {v4}, Lorg/icepdf/core/tag/Tagger;->endImage(Lorg/icepdf/core/pobjects/Reference;)V

    .line 2466
    :cond_15
    if-nez p4, :cond_16

    .line 2467
    int-to-double v0, v7

    move-wide/from16 v36, v0

    const-wide/high16 v38, 0x3fe0000000000000L    # 0.5

    mul-double v36, v36, v38

    move-wide/from16 v0, v36

    double-to-int v4, v0

    int-to-double v0, v8

    move-wide/from16 v36, v0

    const-wide/high16 v38, 0x3fe0000000000000L    # 0.5

    mul-double v36, v36, v38

    move-wide/from16 v0, v36

    double-to-int v6, v0

    const/16 v36, 0x1

    move-object/from16 v0, v23

    move/from16 v1, v36

    invoke-static {v0, v4, v6, v1}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v23

    .line 2468
    :cond_16
    return-object v23

    .line 2399
    .end local v23    # "img":Landroid/graphics/Bitmap;
    :cond_17
    move-object/from16 v0, v28

    instance-of v4, v0, Ljava/util/Vector;

    if-eqz v4, :cond_13

    .line 2400
    sget-boolean v4, Lorg/icepdf/core/tag/Tagger;->tagging:Z

    if-eqz v4, :cond_18

    .line 2401
    const-string/jumbo v4, "MaskVector"

    invoke-static {v4}, Lorg/icepdf/core/tag/Tagger;->tagImage(Ljava/lang/String;)V

    :cond_18
    move-object/from16 v30, v28

    .line 2402
    check-cast v30, Ljava/util/Vector;

    .line 2403
    .local v30, "maskVector":Ljava/util/Vector;
    new-array v0, v9, [I

    move-object/from16 v27, v0

    .line 2404
    .local v27, "maskMinOrigCompsInt":[I
    new-array v0, v9, [I

    move-object/from16 v25, v0

    .line 2405
    .local v25, "maskMaxOrigCompsInt":[I
    const/16 v21, 0x0

    .restart local v21    # "i":I
    :goto_3
    move/from16 v0, v21

    if-ge v0, v9, :cond_1b

    .line 2406
    mul-int/lit8 v4, v21, 0x2

    invoke-virtual/range {v30 .. v30}, Ljava/util/Vector;->size()I

    move-result v6

    if-ge v4, v6, :cond_19

    .line 2407
    mul-int/lit8 v4, v21, 0x2

    move-object/from16 v0, v30

    invoke-virtual {v0, v4}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Number;

    invoke-virtual {v4}, Ljava/lang/Number;->intValue()I

    move-result v4

    aput v4, v27, v21

    .line 2408
    :cond_19
    mul-int/lit8 v4, v21, 0x2

    add-int/lit8 v4, v4, 0x1

    invoke-virtual/range {v30 .. v30}, Ljava/util/Vector;->size()I

    move-result v6

    if-ge v4, v6, :cond_1a

    .line 2409
    mul-int/lit8 v4, v21, 0x2

    add-int/lit8 v4, v4, 0x1

    move-object/from16 v0, v30

    invoke-virtual {v0, v4}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Number;

    invoke-virtual {v4}, Ljava/lang/Number;->intValue()I

    move-result v4

    aput v4, v25, v21

    .line 2405
    :cond_1a
    add-int/lit8 v21, v21, 0x1

    goto :goto_3

    .line 2411
    :cond_1b
    instance-of v4, v5, Lorg/icepdf/core/pobjects/graphics/Indexed;

    if-eqz v4, :cond_1c

    move-object/from16 v22, v5

    .line 2412
    check-cast v22, Lorg/icepdf/core/pobjects/graphics/Indexed;

    .line 2413
    .local v22, "icolourSpace":Lorg/icepdf/core/pobjects/graphics/Indexed;
    invoke-virtual/range {v22 .. v22}, Lorg/icepdf/core/pobjects/graphics/Indexed;->accessColorTable()[Lorg/apache/poi/java/awt/Color;

    move-result-object v19

    .line 2414
    .local v19, "colors":[Lorg/apache/poi/java/awt/Color;
    if-eqz v19, :cond_13

    move-object/from16 v0, v27

    array-length v4, v0

    const/4 v6, 0x1

    if-lt v4, v6, :cond_13

    move-object/from16 v0, v25

    array-length v4, v0

    const/4 v6, 0x1

    if-lt v4, v6, :cond_13

    .line 2417
    const/4 v4, 0x0

    aget v17, v27, v4

    .line 2418
    const/4 v4, 0x0

    aget v18, v25, v4

    .line 2419
    if-ltz v17, :cond_13

    move-object/from16 v0, v19

    array-length v4, v0

    move/from16 v0, v17

    if-ge v0, v4, :cond_13

    if-ltz v18, :cond_13

    move-object/from16 v0, v19

    array-length v4, v0

    move/from16 v0, v18

    if-ge v0, v4, :cond_13

    .line 2421
    const/4 v4, 0x0

    aget v4, v27, v4

    aget-object v32, v19, v4

    .line 2422
    .local v32, "minColor":Lorg/apache/poi/java/awt/Color;
    const/4 v4, 0x0

    aget v4, v25, v4

    aget-object v31, v19, v4

    .line 2423
    .local v31, "maxColor":Lorg/apache/poi/java/awt/Color;
    const/4 v4, 0x3

    new-array v15, v4, [I

    .end local v15    # "maskMinRGB":[I
    const/4 v4, 0x0

    invoke-virtual/range {v32 .. v32}, Lorg/apache/poi/java/awt/Color;->getRed()I

    move-result v6

    aput v6, v15, v4

    const/4 v4, 0x1

    invoke-virtual/range {v32 .. v32}, Lorg/apache/poi/java/awt/Color;->getGreen()I

    move-result v6

    aput v6, v15, v4

    const/4 v4, 0x2

    invoke-virtual/range {v32 .. v32}, Lorg/apache/poi/java/awt/Color;->getBlue()I

    move-result v6

    aput v6, v15, v4

    .line 2424
    .restart local v15    # "maskMinRGB":[I
    const/4 v4, 0x3

    new-array v0, v4, [I

    move-object/from16 v16, v0

    .end local v16    # "maskMaxRGB":[I
    const/4 v4, 0x0

    invoke-virtual/range {v31 .. v31}, Lorg/apache/poi/java/awt/Color;->getRed()I

    move-result v6

    aput v6, v16, v4

    const/4 v4, 0x1

    invoke-virtual/range {v31 .. v31}, Lorg/apache/poi/java/awt/Color;->getGreen()I

    move-result v6

    aput v6, v16, v4

    const/4 v4, 0x2

    invoke-virtual/range {v31 .. v31}, Lorg/apache/poi/java/awt/Color;->getBlue()I

    move-result v6

    aput v6, v16, v4

    .restart local v16    # "maskMaxRGB":[I
    goto/16 :goto_2

    .line 2428
    .end local v19    # "colors":[Lorg/apache/poi/java/awt/Color;
    .end local v22    # "icolourSpace":Lorg/icepdf/core/pobjects/graphics/Indexed;
    .end local v31    # "maxColor":Lorg/apache/poi/java/awt/Color;
    .end local v32    # "minColor":Lorg/apache/poi/java/awt/Color;
    :cond_1c
    invoke-static/range {v27 .. v27}, Lorg/icepdf/core/pobjects/graphics/PColorSpace;->reverseInPlace([I)V

    .line 2429
    invoke-static/range {v25 .. v25}, Lorg/icepdf/core/pobjects/graphics/PColorSpace;->reverseInPlace([I)V

    .line 2430
    new-array v0, v9, [F

    move-object/from16 v26, v0

    .line 2431
    .local v26, "maskMinOrigComps":[F
    new-array v0, v9, [F

    move-object/from16 v24, v0

    .line 2432
    .local v24, "maskMaxOrigComps":[F
    const/4 v4, 0x1

    shl-int/2addr v4, v10

    add-int/lit8 v4, v4, -0x1

    int-to-float v4, v4

    move-object/from16 v0, v27

    move-object/from16 v1, v26

    invoke-virtual {v5, v0, v1, v4}, Lorg/icepdf/core/pobjects/graphics/PColorSpace;->normaliseComponentsToFloats([I[FF)V

    .line 2433
    const/4 v4, 0x1

    shl-int/2addr v4, v10

    add-int/lit8 v4, v4, -0x1

    int-to-float v4, v4

    move-object/from16 v0, v25

    move-object/from16 v1, v24

    invoke-virtual {v5, v0, v1, v4}, Lorg/icepdf/core/pobjects/graphics/PColorSpace;->normaliseComponentsToFloats([I[FF)V

    .line 2435
    move-object/from16 v0, v26

    invoke-virtual {v5, v0}, Lorg/icepdf/core/pobjects/graphics/PColorSpace;->getColor([F)Lorg/apache/poi/java/awt/Color;

    move-result-object v32

    .line 2436
    .restart local v32    # "minColor":Lorg/apache/poi/java/awt/Color;
    move-object/from16 v0, v24

    invoke-virtual {v5, v0}, Lorg/icepdf/core/pobjects/graphics/PColorSpace;->getColor([F)Lorg/apache/poi/java/awt/Color;

    move-result-object v31

    .line 2437
    .restart local v31    # "maxColor":Lorg/apache/poi/java/awt/Color;
    invoke-static/range {v26 .. v26}, Lorg/icepdf/core/pobjects/graphics/PColorSpace;->reverseInPlace([F)V

    .line 2438
    invoke-static/range {v24 .. v24}, Lorg/icepdf/core/pobjects/graphics/PColorSpace;->reverseInPlace([F)V

    .line 2439
    const/4 v4, 0x3

    new-array v15, v4, [I

    .end local v15    # "maskMinRGB":[I
    const/4 v4, 0x0

    invoke-virtual/range {v32 .. v32}, Lorg/apache/poi/java/awt/Color;->getRed()I

    move-result v6

    aput v6, v15, v4

    const/4 v4, 0x1

    invoke-virtual/range {v32 .. v32}, Lorg/apache/poi/java/awt/Color;->getGreen()I

    move-result v6

    aput v6, v15, v4

    const/4 v4, 0x2

    invoke-virtual/range {v32 .. v32}, Lorg/apache/poi/java/awt/Color;->getBlue()I

    move-result v6

    aput v6, v15, v4

    .line 2440
    .restart local v15    # "maskMinRGB":[I
    const/4 v4, 0x3

    new-array v0, v4, [I

    move-object/from16 v16, v0

    .end local v16    # "maskMaxRGB":[I
    const/4 v4, 0x0

    invoke-virtual/range {v31 .. v31}, Lorg/apache/poi/java/awt/Color;->getRed()I

    move-result v6

    aput v6, v16, v4

    const/4 v4, 0x1

    invoke-virtual/range {v31 .. v31}, Lorg/apache/poi/java/awt/Color;->getGreen()I

    move-result v6

    aput v6, v16, v4

    const/4 v4, 0x2

    invoke-virtual/range {v31 .. v31}, Lorg/apache/poi/java/awt/Color;->getBlue()I

    move-result v6

    aput v6, v16, v4

    .restart local v16    # "maskMaxRGB":[I
    goto/16 :goto_2
.end method

.method public getInputStreamForDecodedStreamBytes()Ljava/io/InputStream;
    .locals 18

    .prologue
    .line 250
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/icepdf/core/pobjects/Stream;->streamInput:Lorg/icepdf/core/io/SeekableInputConstrainedWrapper;

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/icepdf/core/pobjects/Stream;->streamInput:Lorg/icepdf/core/io/SeekableInputConstrainedWrapper;

    invoke-virtual {v2}, Lorg/icepdf/core/io/SeekableInputConstrainedWrapper;->getLength()J

    move-result-wide v2

    const-wide/16 v16, 0x1

    cmp-long v2, v2, v16

    if-gez v2, :cond_2

    .line 251
    :cond_0
    const/4 v6, 0x0

    .line 342
    :cond_1
    :goto_0
    return-object v6

    .line 254
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/icepdf/core/pobjects/Stream;->streamInput:Lorg/icepdf/core/io/SeekableInputConstrainedWrapper;

    invoke-virtual {v2}, Lorg/icepdf/core/io/SeekableInputConstrainedWrapper;->getLength()J

    move-result-wide v14

    .line 255
    .local v14, "streamLength":J
    long-to-int v13, v14

    .line 256
    .local v13, "memoryNeeded":I
    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lorg/icepdf/core/pobjects/Stream;->checkMemory(I)Z

    .line 257
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/icepdf/core/pobjects/Stream;->streamInput:Lorg/icepdf/core/io/SeekableInputConstrainedWrapper;

    invoke-virtual {v2}, Lorg/icepdf/core/io/SeekableInputConstrainedWrapper;->prepareForCurrentUse()V

    .line 258
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/icepdf/core/pobjects/Stream;->streamInput:Lorg/icepdf/core/io/SeekableInputConstrainedWrapper;

    .line 260
    .local v12, "input":Ljava/io/InputStream;
    long-to-int v2, v14

    const/16 v3, 0x40

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    const/16 v3, 0x4000

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v8

    .line 261
    .local v8, "bufferSize":I
    new-instance v6, Ljava/io/BufferedInputStream;

    invoke-direct {v6, v12, v8}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;I)V

    .line 265
    .end local v12    # "input":Ljava/io/InputStream;
    .local v6, "input":Ljava/io/InputStream;
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/icepdf/core/pobjects/Stream;->library:Lorg/icepdf/core/util/Library;

    iget-object v2, v2, Lorg/icepdf/core/util/Library;->securityManager:Lorg/icepdf/core/pobjects/security/SecurityManager;

    if-eqz v2, :cond_3

    .line 267
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/icepdf/core/pobjects/Stream;->library:Lorg/icepdf/core/util/Library;

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/icepdf/core/pobjects/Stream;->entries:Ljava/util/Hashtable;

    const-string/jumbo v4, "DecodeParam"

    invoke-virtual {v2, v3, v4}, Lorg/icepdf/core/util/Library;->getDictionary(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/util/Hashtable;

    move-result-object v5

    .line 268
    .local v5, "decodeParams":Ljava/util/Hashtable;
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/icepdf/core/pobjects/Stream;->library:Lorg/icepdf/core/util/Library;

    invoke-virtual {v2}, Lorg/icepdf/core/util/Library;->getSecurityManager()Lorg/icepdf/core/pobjects/security/SecurityManager;

    move-result-object v2

    invoke-virtual/range {p0 .. p0}, Lorg/icepdf/core/pobjects/Stream;->getPObjectReference()Lorg/icepdf/core/pobjects/Reference;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/icepdf/core/pobjects/Stream;->library:Lorg/icepdf/core/util/Library;

    invoke-virtual {v4}, Lorg/icepdf/core/util/Library;->getSecurityManager()Lorg/icepdf/core/pobjects/security/SecurityManager;

    move-result-object v4

    invoke-virtual {v4}, Lorg/icepdf/core/pobjects/security/SecurityManager;->getDecryptionKey()[B

    move-result-object v4

    const/4 v7, 0x1

    invoke-virtual/range {v2 .. v7}, Lorg/icepdf/core/pobjects/security/SecurityManager;->getEncryptionInputStream(Lorg/icepdf/core/pobjects/Reference;[BLjava/util/Hashtable;Ljava/io/InputStream;Z)Ljava/io/InputStream;

    move-result-object v6

    .line 276
    .end local v5    # "decodeParams":Ljava/util/Hashtable;
    :cond_3
    invoke-direct/range {p0 .. p0}, Lorg/icepdf/core/pobjects/Stream;->getFilterNames()Ljava/util/Vector;

    move-result-object v10

    .line 277
    .local v10, "filterNames":Ljava/util/Vector;
    if-eqz v10, :cond_1

    .line 283
    const/4 v11, 0x0

    .local v11, "i":I
    :goto_1
    invoke-virtual {v10}, Ljava/util/Vector;->size()I

    move-result v2

    if-ge v11, v2, :cond_f

    .line 285
    invoke-virtual {v10, v11}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v9

    .line 288
    .local v9, "filterName":Ljava/lang/String;
    const-string/jumbo v2, "FlateDecode"

    invoke-virtual {v9, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    const-string/jumbo v2, "/Fl"

    invoke-virtual {v9, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    const-string/jumbo v2, "Fl"

    invoke-virtual {v9, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 291
    :cond_4
    new-instance v12, Lorg/icepdf/core/pobjects/filters/FlateDecode;

    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/icepdf/core/pobjects/Stream;->library:Lorg/icepdf/core/util/Library;

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/icepdf/core/pobjects/Stream;->entries:Ljava/util/Hashtable;

    invoke-direct {v12, v2, v3, v6}, Lorg/icepdf/core/pobjects/filters/FlateDecode;-><init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;Ljava/io/InputStream;)V

    .line 292
    .end local v6    # "input":Ljava/io/InputStream;
    .restart local v12    # "input":Ljava/io/InputStream;
    mul-int/lit8 v13, v13, 0x2

    move-object v6, v12

    .line 283
    .end local v12    # "input":Ljava/io/InputStream;
    .restart local v6    # "input":Ljava/io/InputStream;
    :cond_5
    :goto_2
    add-int/lit8 v11, v11, 0x1

    goto :goto_1

    .line 293
    :cond_6
    const-string/jumbo v2, "LZWDecode"

    invoke-virtual {v9, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    const-string/jumbo v2, "/LZW"

    invoke-virtual {v9, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    const-string/jumbo v2, "LZW"

    invoke-virtual {v9, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 297
    :cond_7
    new-instance v12, Lorg/icepdf/core/pobjects/filters/LZWDecode;

    new-instance v2, Lorg/icepdf/core/io/BitStream;

    invoke-direct {v2, v6}, Lorg/icepdf/core/io/BitStream;-><init>(Ljava/io/InputStream;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/icepdf/core/pobjects/Stream;->library:Lorg/icepdf/core/util/Library;

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/icepdf/core/pobjects/Stream;->entries:Ljava/util/Hashtable;

    invoke-direct {v12, v2, v3, v4}, Lorg/icepdf/core/pobjects/filters/LZWDecode;-><init>(Lorg/icepdf/core/io/BitStream;Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V

    .line 298
    .end local v6    # "input":Ljava/io/InputStream;
    .restart local v12    # "input":Ljava/io/InputStream;
    mul-int/lit8 v13, v13, 0x2

    move-object v6, v12

    .end local v12    # "input":Ljava/io/InputStream;
    .restart local v6    # "input":Ljava/io/InputStream;
    goto :goto_2

    .line 299
    :cond_8
    const-string/jumbo v2, "ASCII85Decode"

    invoke-virtual {v9, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    const-string/jumbo v2, "/A85"

    invoke-virtual {v9, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    const-string/jumbo v2, "A85"

    invoke-virtual {v9, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 303
    :cond_9
    new-instance v12, Lorg/icepdf/core/pobjects/filters/ASCII85Decode;

    invoke-direct {v12, v6}, Lorg/icepdf/core/pobjects/filters/ASCII85Decode;-><init>(Ljava/io/InputStream;)V

    .line 304
    .end local v6    # "input":Ljava/io/InputStream;
    .restart local v12    # "input":Ljava/io/InputStream;
    mul-int/lit8 v13, v13, 0x2

    move-object v6, v12

    .end local v12    # "input":Ljava/io/InputStream;
    .restart local v6    # "input":Ljava/io/InputStream;
    goto :goto_2

    .line 305
    :cond_a
    const-string/jumbo v2, "ASCIIHexDecode"

    invoke-virtual {v9, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_b

    const-string/jumbo v2, "/AHx"

    invoke-virtual {v9, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_b

    const-string/jumbo v2, "AHx"

    invoke-virtual {v9, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_c

    .line 309
    :cond_b
    new-instance v12, Lorg/icepdf/core/pobjects/filters/ASCIIHexDecode;

    invoke-direct {v12, v6}, Lorg/icepdf/core/pobjects/filters/ASCIIHexDecode;-><init>(Ljava/io/InputStream;)V

    .line 310
    .end local v6    # "input":Ljava/io/InputStream;
    .restart local v12    # "input":Ljava/io/InputStream;
    div-int/lit8 v13, v13, 0x2

    move-object v6, v12

    .end local v12    # "input":Ljava/io/InputStream;
    .restart local v6    # "input":Ljava/io/InputStream;
    goto :goto_2

    .line 311
    :cond_c
    const-string/jumbo v2, "RunLengthDecode"

    invoke-virtual {v9, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_d

    const-string/jumbo v2, "/RL"

    invoke-virtual {v9, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_d

    const-string/jumbo v2, "RL"

    invoke-virtual {v9, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_e

    .line 315
    :cond_d
    new-instance v12, Lorg/icepdf/core/pobjects/filters/RunLengthDecode;

    invoke-direct {v12, v6}, Lorg/icepdf/core/pobjects/filters/RunLengthDecode;-><init>(Ljava/io/InputStream;)V

    .line 316
    .end local v6    # "input":Ljava/io/InputStream;
    .restart local v12    # "input":Ljava/io/InputStream;
    mul-int/lit8 v13, v13, 0x2

    move-object v6, v12

    .end local v12    # "input":Ljava/io/InputStream;
    .restart local v6    # "input":Ljava/io/InputStream;
    goto/16 :goto_2

    .line 317
    :cond_e
    const-string/jumbo v2, "CCITTFaxDecode"

    invoke-virtual {v9, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    const-string/jumbo v2, "/CCF"

    invoke-virtual {v9, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    const-string/jumbo v2, "CCF"

    invoke-virtual {v9, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 322
    const-string/jumbo v2, "DCTDecode"

    invoke-virtual {v9, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    const-string/jumbo v2, "/DCT"

    invoke-virtual {v9, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    const-string/jumbo v2, "DCT"

    invoke-virtual {v9, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 327
    const-string/jumbo v2, "JBIG2Decode"

    invoke-virtual {v9, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 330
    const-string/jumbo v2, "JPXDecode"

    invoke-virtual {v9, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 334
    sget-object v2, Lorg/icepdf/core/pobjects/Stream;->logger:Ljava/util/logging/Logger;

    sget-object v3, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    invoke-virtual {v2, v3}, Ljava/util/logging/Logger;->isLoggable(Ljava/util/logging/Level;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 335
    sget-object v2, Lorg/icepdf/core/pobjects/Stream;->logger:Ljava/util/logging/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "UNSUPPORTED:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/icepdf/core/pobjects/Stream;->entries:Ljava/util/Hashtable;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/logging/Logger;->fine(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 340
    .end local v9    # "filterName":Ljava/lang/String;
    :cond_f
    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lorg/icepdf/core/pobjects/Stream;->checkMemory(I)Z

    goto/16 :goto_0
.end method

.method public getPObjectReference()Lorg/icepdf/core/pobjects/Reference;
    .locals 1

    .prologue
    .line 199
    iget-object v0, p0, Lorg/icepdf/core/pobjects/Stream;->pObjectReference:Lorg/icepdf/core/pobjects/Reference;

    return-object v0
.end method

.method public getStreamInput()Lorg/icepdf/core/io/SeekableInputConstrainedWrapper;
    .locals 1

    .prologue
    .line 3600
    iget-object v0, p0, Lorg/icepdf/core/pobjects/Stream;->streamInput:Lorg/icepdf/core/io/SeekableInputConstrainedWrapper;

    return-object v0
.end method

.method public isImageMask()Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 3552
    iget-object v2, p0, Lorg/icepdf/core/pobjects/Stream;->library:Lorg/icepdf/core/util/Library;

    iget-object v3, p0, Lorg/icepdf/core/pobjects/Stream;->entries:Ljava/util/Hashtable;

    const-string/jumbo v4, "ImageMask"

    invoke-virtual {v2, v3, v4}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 3553
    .local v0, "o":Ljava/lang/Object;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "true"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    :cond_0
    return v1
.end method

.method isImageSubtype()Z
    .locals 4

    .prologue
    .line 221
    iget-object v1, p0, Lorg/icepdf/core/pobjects/Stream;->library:Lorg/icepdf/core/util/Library;

    iget-object v2, p0, Lorg/icepdf/core/pobjects/Stream;->entries:Ljava/util/Hashtable;

    const-string/jumbo v3, "Subtype"

    invoke-virtual {v1, v2, v3}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 222
    .local v0, "subtype":Ljava/lang/Object;
    if-eqz v0, :cond_0

    const-string/jumbo v1, "Image"

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isInlineImage()Z
    .locals 1

    .prologue
    .line 217
    iget-boolean v0, p0, Lorg/icepdf/core/pobjects/Stream;->inlineImage:Z

    return v0
.end method

.method public setInlineImage(Z)V
    .locals 0
    .param p1, "inlineImage"    # Z

    .prologue
    .line 209
    iput-boolean p1, p0, Lorg/icepdf/core/pobjects/Stream;->inlineImage:Z

    .line 210
    return-void
.end method

.method public setPObjectReference(Lorg/icepdf/core/pobjects/Reference;)V
    .locals 0
    .param p1, "reference"    # Lorg/icepdf/core/pobjects/Reference;

    .prologue
    .line 189
    iput-object p1, p0, Lorg/icepdf/core/pobjects/Stream;->pObjectReference:Lorg/icepdf/core/pobjects/Reference;

    .line 190
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 3607
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x40

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 3608
    .local v0, "sb":Ljava/lang/StringBuilder;
    const-string/jumbo v1, "STREAM= "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3609
    iget-object v1, p0, Lorg/icepdf/core/pobjects/Stream;->entries:Ljava/util/Hashtable;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 3610
    invoke-virtual {p0}, Lorg/icepdf/core/pobjects/Stream;->getPObjectReference()Lorg/icepdf/core/pobjects/Reference;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 3611
    const-string/jumbo v1, "  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3612
    invoke-virtual {p0}, Lorg/icepdf/core/pobjects/Stream;->getPObjectReference()Lorg/icepdf/core/pobjects/Reference;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 3614
    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
