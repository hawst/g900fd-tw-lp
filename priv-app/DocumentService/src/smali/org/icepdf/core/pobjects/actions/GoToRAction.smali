.class public Lorg/icepdf/core/pobjects/actions/GoToRAction;
.super Lorg/icepdf/core/pobjects/actions/Action;
.source "GoToRAction.java"


# instance fields
.field private externalDestination:Lorg/icepdf/core/pobjects/Destination;

.field private externalFile:Ljava/lang/String;

.field private fileSpecification:Lorg/icepdf/core/pobjects/actions/FileSpecification;

.field private isNewWindow:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V
    .locals 5
    .param p1, "l"    # Lorg/icepdf/core/util/Library;
    .param p2, "h"    # Ljava/util/Hashtable;

    .prologue
    .line 50
    invoke-direct {p0, p1, p2}, Lorg/icepdf/core/pobjects/actions/Action;-><init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V

    .line 52
    new-instance v0, Lorg/icepdf/core/pobjects/Destination;

    iget-object v1, p0, Lorg/icepdf/core/pobjects/actions/GoToRAction;->library:Lorg/icepdf/core/util/Library;

    iget-object v2, p0, Lorg/icepdf/core/pobjects/actions/GoToRAction;->library:Lorg/icepdf/core/util/Library;

    iget-object v3, p0, Lorg/icepdf/core/pobjects/actions/GoToRAction;->entries:Ljava/util/Hashtable;

    const-string/jumbo v4, "D"

    invoke-virtual {v2, v3, v4}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/icepdf/core/pobjects/Destination;-><init>(Lorg/icepdf/core/util/Library;Ljava/lang/Object;)V

    iput-object v0, p0, Lorg/icepdf/core/pobjects/actions/GoToRAction;->externalDestination:Lorg/icepdf/core/pobjects/Destination;

    .line 55
    iget-object v0, p0, Lorg/icepdf/core/pobjects/actions/GoToRAction;->library:Lorg/icepdf/core/util/Library;

    iget-object v1, p0, Lorg/icepdf/core/pobjects/actions/GoToRAction;->entries:Ljava/util/Hashtable;

    const-string/jumbo v2, "F"

    invoke-virtual {v0, v1, v2}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Ljava/util/Hashtable;

    if-eqz v0, :cond_1

    .line 56
    new-instance v0, Lorg/icepdf/core/pobjects/actions/FileSpecification;

    iget-object v1, p0, Lorg/icepdf/core/pobjects/actions/GoToRAction;->library:Lorg/icepdf/core/util/Library;

    iget-object v2, p0, Lorg/icepdf/core/pobjects/actions/GoToRAction;->library:Lorg/icepdf/core/util/Library;

    iget-object v3, p0, Lorg/icepdf/core/pobjects/actions/GoToRAction;->entries:Ljava/util/Hashtable;

    const-string/jumbo v4, "F"

    invoke-virtual {v2, v3, v4}, Lorg/icepdf/core/util/Library;->getDictionary(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/util/Hashtable;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/icepdf/core/pobjects/actions/FileSpecification;-><init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V

    iput-object v0, p0, Lorg/icepdf/core/pobjects/actions/GoToRAction;->fileSpecification:Lorg/icepdf/core/pobjects/actions/FileSpecification;

    .line 66
    :cond_0
    :goto_0
    iget-object v0, p0, Lorg/icepdf/core/pobjects/actions/GoToRAction;->library:Lorg/icepdf/core/util/Library;

    iget-object v1, p0, Lorg/icepdf/core/pobjects/actions/GoToRAction;->entries:Ljava/util/Hashtable;

    const-string/jumbo v2, "NewWindow"

    invoke-virtual {v0, v1, v2}, Lorg/icepdf/core/util/Library;->getBoolean(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lorg/icepdf/core/pobjects/actions/GoToRAction;->isNewWindow:Ljava/lang/Boolean;

    .line 68
    return-void

    .line 59
    :cond_1
    iget-object v0, p0, Lorg/icepdf/core/pobjects/actions/GoToRAction;->library:Lorg/icepdf/core/util/Library;

    iget-object v1, p0, Lorg/icepdf/core/pobjects/actions/GoToRAction;->entries:Ljava/util/Hashtable;

    const-string/jumbo v2, "F"

    invoke-virtual {v0, v1, v2}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lorg/icepdf/core/pobjects/StringObject;

    if-eqz v0, :cond_0

    .line 60
    iget-object v0, p0, Lorg/icepdf/core/pobjects/actions/GoToRAction;->library:Lorg/icepdf/core/util/Library;

    iget-object v1, p0, Lorg/icepdf/core/pobjects/actions/GoToRAction;->entries:Ljava/util/Hashtable;

    const-string/jumbo v2, "F"

    invoke-virtual {v0, v1, v2}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/icepdf/core/pobjects/StringObject;

    iget-object v1, p0, Lorg/icepdf/core/pobjects/actions/GoToRAction;->library:Lorg/icepdf/core/util/Library;

    invoke-virtual {v1}, Lorg/icepdf/core/util/Library;->getSecurityManager()Lorg/icepdf/core/pobjects/security/SecurityManager;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/icepdf/core/pobjects/StringObject;->getDecryptedLiteralString(Lorg/icepdf/core/pobjects/security/SecurityManager;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/icepdf/core/pobjects/actions/GoToRAction;->externalFile:Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method public getDestination()Lorg/icepdf/core/pobjects/Destination;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lorg/icepdf/core/pobjects/actions/GoToRAction;->externalDestination:Lorg/icepdf/core/pobjects/Destination;

    return-object v0
.end method

.method public getFile()Ljava/lang/String;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lorg/icepdf/core/pobjects/actions/GoToRAction;->externalFile:Ljava/lang/String;

    return-object v0
.end method

.method public getFileSpecification()Lorg/icepdf/core/pobjects/actions/FileSpecification;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lorg/icepdf/core/pobjects/actions/GoToRAction;->fileSpecification:Lorg/icepdf/core/pobjects/actions/FileSpecification;

    return-object v0
.end method

.method public isNewWindow()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lorg/icepdf/core/pobjects/actions/GoToRAction;->isNewWindow:Ljava/lang/Boolean;

    return-object v0
.end method
