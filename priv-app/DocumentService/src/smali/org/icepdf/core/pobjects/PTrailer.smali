.class public Lorg/icepdf/core/pobjects/PTrailer;
.super Lorg/icepdf/core/pobjects/Dictionary;
.source "PTrailer.java"


# instance fields
.field private m_CrossReferenceStream:Lorg/icepdf/core/pobjects/CrossReference;

.field private m_CrossReferenceTable:Lorg/icepdf/core/pobjects/CrossReference;

.field private position:J


# direct methods
.method public constructor <init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;Lorg/icepdf/core/pobjects/CrossReference;Lorg/icepdf/core/pobjects/CrossReference;)V
    .locals 1
    .param p1, "library"    # Lorg/icepdf/core/util/Library;
    .param p2, "dictionary"    # Ljava/util/Hashtable;
    .param p3, "xrefTable"    # Lorg/icepdf/core/pobjects/CrossReference;
    .param p4, "xrefStream"    # Lorg/icepdf/core/pobjects/CrossReference;

    .prologue
    .line 72
    invoke-direct {p0, p1, p2}, Lorg/icepdf/core/pobjects/Dictionary;-><init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V

    .line 74
    iput-object p3, p0, Lorg/icepdf/core/pobjects/PTrailer;->m_CrossReferenceTable:Lorg/icepdf/core/pobjects/CrossReference;

    .line 75
    iput-object p4, p0, Lorg/icepdf/core/pobjects/PTrailer;->m_CrossReferenceStream:Lorg/icepdf/core/pobjects/CrossReference;

    .line 76
    iget-object v0, p0, Lorg/icepdf/core/pobjects/PTrailer;->m_CrossReferenceTable:Lorg/icepdf/core/pobjects/CrossReference;

    if-eqz v0, :cond_0

    .line 77
    iget-object v0, p0, Lorg/icepdf/core/pobjects/PTrailer;->m_CrossReferenceTable:Lorg/icepdf/core/pobjects/CrossReference;

    invoke-virtual {v0, p0}, Lorg/icepdf/core/pobjects/CrossReference;->setTrailer(Lorg/icepdf/core/pobjects/PTrailer;)V

    .line 78
    :cond_0
    iget-object v0, p0, Lorg/icepdf/core/pobjects/PTrailer;->m_CrossReferenceStream:Lorg/icepdf/core/pobjects/CrossReference;

    if-eqz v0, :cond_1

    .line 79
    iget-object v0, p0, Lorg/icepdf/core/pobjects/PTrailer;->m_CrossReferenceStream:Lorg/icepdf/core/pobjects/CrossReference;

    invoke-virtual {v0, p0}, Lorg/icepdf/core/pobjects/CrossReference;->setTrailer(Lorg/icepdf/core/pobjects/PTrailer;)V

    .line 80
    :cond_1
    return-void
.end method


# virtual methods
.method protected addNextTrailer(Lorg/icepdf/core/pobjects/PTrailer;)V
    .locals 7
    .param p1, "nextTrailer"    # Lorg/icepdf/core/pobjects/PTrailer;

    .prologue
    .line 228
    invoke-virtual {p1}, Lorg/icepdf/core/pobjects/PTrailer;->getPrimaryCrossReference()Lorg/icepdf/core/pobjects/CrossReference;

    move-result-object v5

    invoke-virtual {p0}, Lorg/icepdf/core/pobjects/PTrailer;->getPrimaryCrossReference()Lorg/icepdf/core/pobjects/CrossReference;

    move-result-object v6

    invoke-virtual {v5, v6}, Lorg/icepdf/core/pobjects/CrossReference;->addToEndOfChainOfPreviousXRefs(Lorg/icepdf/core/pobjects/CrossReference;)V

    .line 231
    invoke-virtual {p1}, Lorg/icepdf/core/pobjects/PTrailer;->getDictionary()Ljava/util/Hashtable;

    move-result-object v4

    .line 232
    .local v4, "nextDictionary":Ljava/util/Hashtable;
    invoke-virtual {p0}, Lorg/icepdf/core/pobjects/PTrailer;->getDictionary()Ljava/util/Hashtable;

    move-result-object v0

    .line 233
    .local v0, "currDictionary":Ljava/util/Hashtable;
    invoke-virtual {v0}, Ljava/util/Hashtable;->keys()Ljava/util/Enumeration;

    move-result-object v2

    .line 234
    .local v2, "currKeys":Ljava/util/Enumeration;
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 235
    invoke-interface {v2}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v1

    .line 236
    .local v1, "currKey":Ljava/lang/Object;
    invoke-virtual {v4, v1}, Ljava/util/Hashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 237
    invoke-virtual {v0, v1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    .line 238
    .local v3, "currValue":Ljava/lang/Object;
    invoke-virtual {v4, v1, v3}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 241
    .end local v1    # "currKey":Ljava/lang/Object;
    .end local v3    # "currValue":Ljava/lang/Object;
    :cond_1
    return-void
.end method

.method protected addPreviousTrailer(Lorg/icepdf/core/pobjects/PTrailer;)V
    .locals 7
    .param p1, "previousTrailer"    # Lorg/icepdf/core/pobjects/PTrailer;

    .prologue
    .line 245
    invoke-virtual {p0}, Lorg/icepdf/core/pobjects/PTrailer;->getPrimaryCrossReference()Lorg/icepdf/core/pobjects/CrossReference;

    move-result-object v5

    invoke-virtual {p1}, Lorg/icepdf/core/pobjects/PTrailer;->getPrimaryCrossReference()Lorg/icepdf/core/pobjects/CrossReference;

    move-result-object v6

    invoke-virtual {v5, v6}, Lorg/icepdf/core/pobjects/CrossReference;->addToEndOfChainOfPreviousXRefs(Lorg/icepdf/core/pobjects/CrossReference;)V

    .line 248
    invoke-virtual {p0}, Lorg/icepdf/core/pobjects/PTrailer;->getDictionary()Ljava/util/Hashtable;

    move-result-object v0

    .line 249
    .local v0, "currDictionary":Ljava/util/Hashtable;
    invoke-virtual {p1}, Lorg/icepdf/core/pobjects/PTrailer;->getDictionary()Ljava/util/Hashtable;

    move-result-object v1

    .line 250
    .local v1, "prevDictionary":Ljava/util/Hashtable;
    invoke-virtual {v1}, Ljava/util/Hashtable;->keys()Ljava/util/Enumeration;

    move-result-object v3

    .line 251
    .local v3, "prevKeys":Ljava/util/Enumeration;
    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 252
    invoke-interface {v3}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v2

    .line 253
    .local v2, "prevKey":Ljava/lang/Object;
    invoke-virtual {v0, v2}, Ljava/util/Hashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 254
    invoke-virtual {v1, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    .line 255
    .local v4, "prevValue":Ljava/lang/Object;
    invoke-virtual {v0, v2, v4}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 258
    .end local v2    # "prevKey":Ljava/lang/Object;
    .end local v4    # "prevValue":Ljava/lang/Object;
    :cond_1
    return-void
.end method

.method protected getCrossReferenceStream()Lorg/icepdf/core/pobjects/CrossReference;
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, Lorg/icepdf/core/pobjects/PTrailer;->m_CrossReferenceStream:Lorg/icepdf/core/pobjects/CrossReference;

    return-object v0
.end method

.method protected getCrossReferenceTable()Lorg/icepdf/core/pobjects/CrossReference;
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lorg/icepdf/core/pobjects/PTrailer;->m_CrossReferenceTable:Lorg/icepdf/core/pobjects/CrossReference;

    return-object v0
.end method

.method public getDictionary()Ljava/util/Hashtable;
    .locals 1

    .prologue
    .line 294
    iget-object v0, p0, Lorg/icepdf/core/pobjects/PTrailer;->entries:Ljava/util/Hashtable;

    return-object v0
.end method

.method public getEncrypt()Ljava/util/Hashtable;
    .locals 4

    .prologue
    .line 188
    iget-object v1, p0, Lorg/icepdf/core/pobjects/PTrailer;->library:Lorg/icepdf/core/util/Library;

    iget-object v2, p0, Lorg/icepdf/core/pobjects/PTrailer;->entries:Ljava/util/Hashtable;

    const-string/jumbo v3, "Encrypt"

    invoke-virtual {v1, v2, v3}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 189
    .local v0, "encryptParams":Ljava/lang/Object;
    instance-of v1, v0, Ljava/util/Hashtable;

    if-eqz v1, :cond_0

    .line 190
    check-cast v0, Ljava/util/Hashtable;

    .line 192
    .end local v0    # "encryptParams":Ljava/lang/Object;
    :goto_0
    return-object v0

    .restart local v0    # "encryptParams":Ljava/lang/Object;
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getID()Ljava/util/Vector;
    .locals 3

    .prologue
    .line 204
    iget-object v0, p0, Lorg/icepdf/core/pobjects/PTrailer;->library:Lorg/icepdf/core/util/Library;

    iget-object v1, p0, Lorg/icepdf/core/pobjects/PTrailer;->entries:Ljava/util/Hashtable;

    const-string/jumbo v2, "ID"

    invoke-virtual {v0, v1, v2}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Vector;

    return-object v0
.end method

.method public getNumberOfObjects()I
    .locals 3

    .prologue
    .line 97
    iget-object v0, p0, Lorg/icepdf/core/pobjects/PTrailer;->library:Lorg/icepdf/core/util/Library;

    iget-object v1, p0, Lorg/icepdf/core/pobjects/PTrailer;->entries:Ljava/util/Hashtable;

    const-string/jumbo v2, "Size"

    invoke-virtual {v0, v1, v2}, Lorg/icepdf/core/util/Library;->getInt(Ljava/util/Hashtable;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public getPosition()J
    .locals 2

    .prologue
    .line 211
    iget-wide v0, p0, Lorg/icepdf/core/pobjects/PTrailer;->position:J

    return-wide v0
.end method

.method public getPrev()J
    .locals 3

    .prologue
    .line 111
    iget-object v0, p0, Lorg/icepdf/core/pobjects/PTrailer;->library:Lorg/icepdf/core/util/Library;

    iget-object v1, p0, Lorg/icepdf/core/pobjects/PTrailer;->entries:Ljava/util/Hashtable;

    const-string/jumbo v2, "Prev"

    invoke-virtual {v0, v1, v2}, Lorg/icepdf/core/util/Library;->getLong(Ljava/util/Hashtable;Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method protected getPrimaryCrossReference()Lorg/icepdf/core/pobjects/CrossReference;
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lorg/icepdf/core/pobjects/PTrailer;->m_CrossReferenceTable:Lorg/icepdf/core/pobjects/CrossReference;

    if-eqz v0, :cond_0

    .line 123
    iget-object v0, p0, Lorg/icepdf/core/pobjects/PTrailer;->m_CrossReferenceTable:Lorg/icepdf/core/pobjects/CrossReference;

    .line 125
    :goto_0
    return-object v0

    .line 124
    :cond_0
    invoke-virtual {p0}, Lorg/icepdf/core/pobjects/PTrailer;->loadXRefStmIfApplicable()V

    .line 125
    iget-object v0, p0, Lorg/icepdf/core/pobjects/PTrailer;->m_CrossReferenceStream:Lorg/icepdf/core/pobjects/CrossReference;

    goto :goto_0
.end method

.method public getRootCatalog()Lorg/icepdf/core/pobjects/Catalog;
    .locals 4

    .prologue
    .line 163
    iget-object v1, p0, Lorg/icepdf/core/pobjects/PTrailer;->library:Lorg/icepdf/core/util/Library;

    iget-object v2, p0, Lorg/icepdf/core/pobjects/PTrailer;->entries:Ljava/util/Hashtable;

    const-string/jumbo v3, "Root"

    invoke-virtual {v1, v2, v3}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 165
    .local v0, "tmp":Ljava/lang/Object;
    instance-of v1, v0, Lorg/icepdf/core/pobjects/Catalog;

    if-eqz v1, :cond_0

    .line 166
    iget-object v1, p0, Lorg/icepdf/core/pobjects/PTrailer;->library:Lorg/icepdf/core/util/Library;

    iget-object v2, p0, Lorg/icepdf/core/pobjects/PTrailer;->entries:Ljava/util/Hashtable;

    const-string/jumbo v3, "Root"

    invoke-virtual {v1, v2, v3}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/icepdf/core/pobjects/Catalog;

    .line 176
    .end local v0    # "tmp":Ljava/lang/Object;
    :goto_0
    return-object v1

    .line 170
    .restart local v0    # "tmp":Ljava/lang/Object;
    :cond_0
    instance-of v1, v0, Ljava/util/Hashtable;

    if-eqz v1, :cond_1

    .line 171
    new-instance v1, Lorg/icepdf/core/pobjects/Catalog;

    iget-object v2, p0, Lorg/icepdf/core/pobjects/PTrailer;->library:Lorg/icepdf/core/util/Library;

    check-cast v0, Ljava/util/Hashtable;

    .end local v0    # "tmp":Ljava/lang/Object;
    invoke-direct {v1, v2, v0}, Lorg/icepdf/core/pobjects/Catalog;-><init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V

    goto :goto_0

    .line 176
    .restart local v0    # "tmp":Ljava/lang/Object;
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getRootCatalogReference()Lorg/icepdf/core/pobjects/Reference;
    .locals 3

    .prologue
    .line 154
    iget-object v0, p0, Lorg/icepdf/core/pobjects/PTrailer;->library:Lorg/icepdf/core/util/Library;

    iget-object v1, p0, Lorg/icepdf/core/pobjects/PTrailer;->entries:Ljava/util/Hashtable;

    const-string/jumbo v2, "Root"

    invoke-virtual {v0, v1, v2}, Lorg/icepdf/core/util/Library;->getObjectReference(Ljava/util/Hashtable;Ljava/lang/String;)Lorg/icepdf/core/pobjects/Reference;

    move-result-object v0

    return-object v0
.end method

.method protected loadXRefStmIfApplicable()V
    .locals 6

    .prologue
    .line 272
    iget-object v1, p0, Lorg/icepdf/core/pobjects/PTrailer;->m_CrossReferenceStream:Lorg/icepdf/core/pobjects/CrossReference;

    if-nez v1, :cond_0

    .line 273
    iget-object v1, p0, Lorg/icepdf/core/pobjects/PTrailer;->library:Lorg/icepdf/core/util/Library;

    iget-object v4, p0, Lorg/icepdf/core/pobjects/PTrailer;->entries:Ljava/util/Hashtable;

    const-string/jumbo v5, "XRefStm"

    invoke-virtual {v1, v4, v5}, Lorg/icepdf/core/util/Library;->getLong(Ljava/util/Hashtable;Ljava/lang/String;)J

    move-result-wide v2

    .line 274
    .local v2, "xrefStreamPosition":J
    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-lez v1, :cond_0

    .line 281
    iget-object v1, p0, Lorg/icepdf/core/pobjects/PTrailer;->library:Lorg/icepdf/core/util/Library;

    invoke-virtual {v1, v2, v3}, Lorg/icepdf/core/util/Library;->getTrailerByFilePosition(J)Lorg/icepdf/core/pobjects/PTrailer;

    move-result-object v0

    .line 282
    .local v0, "trailer":Lorg/icepdf/core/pobjects/PTrailer;
    if-eqz v0, :cond_0

    .line 283
    invoke-virtual {v0}, Lorg/icepdf/core/pobjects/PTrailer;->getCrossReferenceStream()Lorg/icepdf/core/pobjects/CrossReference;

    move-result-object v1

    iput-object v1, p0, Lorg/icepdf/core/pobjects/PTrailer;->m_CrossReferenceStream:Lorg/icepdf/core/pobjects/CrossReference;

    .line 286
    .end local v0    # "trailer":Lorg/icepdf/core/pobjects/PTrailer;
    .end local v2    # "xrefStreamPosition":J
    :cond_0
    return-void
.end method

.method protected onDemandLoadAndSetupPreviousTrailer()V
    .locals 6

    .prologue
    .line 263
    invoke-virtual {p0}, Lorg/icepdf/core/pobjects/PTrailer;->getPrev()J

    move-result-wide v0

    .line 264
    .local v0, "position":J
    const-wide/16 v4, 0x0

    cmp-long v3, v0, v4

    if-lez v3, :cond_0

    .line 265
    iget-object v3, p0, Lorg/icepdf/core/pobjects/PTrailer;->library:Lorg/icepdf/core/util/Library;

    invoke-virtual {v3, v0, v1}, Lorg/icepdf/core/util/Library;->getTrailerByFilePosition(J)Lorg/icepdf/core/pobjects/PTrailer;

    move-result-object v2

    .line 266
    .local v2, "prevTrailer":Lorg/icepdf/core/pobjects/PTrailer;
    if-eqz v2, :cond_0

    .line 267
    invoke-virtual {p0, v2}, Lorg/icepdf/core/pobjects/PTrailer;->addPreviousTrailer(Lorg/icepdf/core/pobjects/PTrailer;)V

    .line 269
    .end local v2    # "prevTrailer":Lorg/icepdf/core/pobjects/PTrailer;
    :cond_0
    return-void
.end method

.method public setPosition(J)V
    .locals 1
    .param p1, "pos"    # J

    .prologue
    .line 219
    iput-wide p1, p0, Lorg/icepdf/core/pobjects/PTrailer;->position:J

    .line 220
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 303
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "PTRAILER= "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/icepdf/core/pobjects/PTrailer;->entries:Ljava/util/Hashtable;

    invoke-virtual {v1}, Ljava/util/Hashtable;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " xref table="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/icepdf/core/pobjects/PTrailer;->m_CrossReferenceTable:Lorg/icepdf/core/pobjects/CrossReference;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "  xref stream="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/icepdf/core/pobjects/PTrailer;->m_CrossReferenceStream:Lorg/icepdf/core/pobjects/CrossReference;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
