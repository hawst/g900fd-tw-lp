.class public Lorg/icepdf/core/pobjects/ObjectStream;
.super Lorg/icepdf/core/pobjects/Stream;
.source "ObjectStream.java"


# static fields
.field private static final logger:Ljava/util/logging/Logger;


# instance fields
.field private m_DecodedStream:Lorg/icepdf/core/io/SeekableInput;

.field private m_bInited:Z

.field private m_iaObjectNumbers:[I

.field private m_laObjectOffset:[J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    const-class v0, Lorg/icepdf/core/pobjects/Form;

    invoke-virtual {v0}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Lorg/icepdf/core/pobjects/ObjectStream;->logger:Ljava/util/logging/Logger;

    return-void
.end method

.method public constructor <init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;Lorg/icepdf/core/io/SeekableInputConstrainedWrapper;)V
    .locals 0
    .param p1, "l"    # Lorg/icepdf/core/util/Library;
    .param p2, "h"    # Ljava/util/Hashtable;
    .param p3, "streamInputWrapper"    # Lorg/icepdf/core/io/SeekableInputConstrainedWrapper;

    .prologue
    .line 50
    invoke-direct {p0, p1, p2, p3}, Lorg/icepdf/core/pobjects/Stream;-><init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;Lorg/icepdf/core/io/SeekableInputConstrainedWrapper;)V

    .line 51
    return-void
.end method


# virtual methods
.method public dispose(Z)V
    .locals 2
    .param p1, "cache"    # Z

    .prologue
    const/4 v1, 0x0

    .line 127
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/icepdf/core/pobjects/ObjectStream;->m_bInited:Z

    .line 128
    iput-object v1, p0, Lorg/icepdf/core/pobjects/ObjectStream;->m_DecodedStream:Lorg/icepdf/core/io/SeekableInput;

    .line 129
    iput-object v1, p0, Lorg/icepdf/core/pobjects/ObjectStream;->m_iaObjectNumbers:[I

    .line 130
    iput-object v1, p0, Lorg/icepdf/core/pobjects/ObjectStream;->m_laObjectOffset:[J

    .line 131
    invoke-super {p0, p1}, Lorg/icepdf/core/pobjects/Stream;->dispose(Z)V

    .line 132
    return-void
.end method

.method public declared-synchronized init()V
    .locals 10

    .prologue
    .line 54
    monitor-enter p0

    :try_start_0
    iget-boolean v6, p0, Lorg/icepdf/core/pobjects/ObjectStream;->m_bInited:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v6, :cond_0

    .line 75
    :goto_0
    monitor-exit p0

    return-void

    .line 56
    :cond_0
    const/4 v6, 0x1

    :try_start_1
    iput-boolean v6, p0, Lorg/icepdf/core/pobjects/ObjectStream;->m_bInited:Z

    .line 57
    iget-object v6, p0, Lorg/icepdf/core/pobjects/ObjectStream;->library:Lorg/icepdf/core/util/Library;

    iget-object v7, p0, Lorg/icepdf/core/pobjects/ObjectStream;->entries:Ljava/util/Hashtable;

    const-string/jumbo v8, "N"

    invoke-virtual {v6, v7, v8}, Lorg/icepdf/core/util/Library;->getInt(Ljava/util/Hashtable;Ljava/lang/String;)I

    move-result v4

    .line 58
    .local v4, "numObjects":I
    iget-object v6, p0, Lorg/icepdf/core/pobjects/ObjectStream;->library:Lorg/icepdf/core/util/Library;

    iget-object v7, p0, Lorg/icepdf/core/pobjects/ObjectStream;->entries:Ljava/util/Hashtable;

    const-string/jumbo v8, "First"

    invoke-virtual {v6, v7, v8}, Lorg/icepdf/core/util/Library;->getLong(Ljava/util/Hashtable;Ljava/lang/String;)J

    move-result-wide v2

    .line 59
    .local v2, "firstObjectsOffset":J
    invoke-virtual {p0}, Lorg/icepdf/core/pobjects/ObjectStream;->getBytes()[B

    move-result-object v0

    .line 60
    .local v0, "data":[B
    new-instance v6, Lorg/icepdf/core/io/SeekableByteArrayInputStream;

    invoke-direct {v6, v0}, Lorg/icepdf/core/io/SeekableByteArrayInputStream;-><init>([B)V

    iput-object v6, p0, Lorg/icepdf/core/pobjects/ObjectStream;->m_DecodedStream:Lorg/icepdf/core/io/SeekableInput;

    .line 61
    iget-object v6, p0, Lorg/icepdf/core/pobjects/ObjectStream;->m_DecodedStream:Lorg/icepdf/core/io/SeekableInput;

    invoke-interface {v6}, Lorg/icepdf/core/io/SeekableInput;->beginThreadAccess()V

    .line 62
    new-array v6, v4, [I

    iput-object v6, p0, Lorg/icepdf/core/pobjects/ObjectStream;->m_iaObjectNumbers:[I

    .line 63
    new-array v6, v4, [J

    iput-object v6, p0, Lorg/icepdf/core/pobjects/ObjectStream;->m_laObjectOffset:[J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 65
    :try_start_2
    new-instance v5, Lorg/icepdf/core/util/Parser;

    iget-object v6, p0, Lorg/icepdf/core/pobjects/ObjectStream;->m_DecodedStream:Lorg/icepdf/core/io/SeekableInput;

    invoke-direct {v5, v6}, Lorg/icepdf/core/util/Parser;-><init>(Lorg/icepdf/core/io/SeekableInput;)V

    .line 66
    .local v5, "parser":Lorg/icepdf/core/util/Parser;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-ge v1, v4, :cond_1

    .line 67
    iget-object v6, p0, Lorg/icepdf/core/pobjects/ObjectStream;->m_iaObjectNumbers:[I

    invoke-virtual {v5}, Lorg/icepdf/core/util/Parser;->getIntSurroundedByWhitespace()I

    move-result v7

    aput v7, v6, v1

    .line 68
    iget-object v6, p0, Lorg/icepdf/core/pobjects/ObjectStream;->m_laObjectOffset:[J

    invoke-virtual {v5}, Lorg/icepdf/core/util/Parser;->getLongSurroundedByWhitespace()J

    move-result-wide v8

    add-long/2addr v8, v2

    aput-wide v8, v6, v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 66
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 72
    :cond_1
    :try_start_3
    iget-object v6, p0, Lorg/icepdf/core/pobjects/ObjectStream;->m_DecodedStream:Lorg/icepdf/core/io/SeekableInput;

    invoke-interface {v6}, Lorg/icepdf/core/io/SeekableInput;->endThreadAccess()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 54
    .end local v0    # "data":[B
    .end local v1    # "i":I
    .end local v2    # "firstObjectsOffset":J
    .end local v4    # "numObjects":I
    .end local v5    # "parser":Lorg/icepdf/core/util/Parser;
    :catchall_0
    move-exception v6

    monitor-exit p0

    throw v6

    .line 72
    .restart local v0    # "data":[B
    .restart local v2    # "firstObjectsOffset":J
    .restart local v4    # "numObjects":I
    :catchall_1
    move-exception v6

    :try_start_4
    iget-object v7, p0, Lorg/icepdf/core/pobjects/ObjectStream;->m_DecodedStream:Lorg/icepdf/core/io/SeekableInput;

    invoke-interface {v7}, Lorg/icepdf/core/io/SeekableInput;->endThreadAccess()V

    throw v6
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0
.end method

.method public loadObject(Lorg/icepdf/core/util/Library;I)Z
    .locals 15
    .param p1, "library"    # Lorg/icepdf/core/util/Library;
    .param p2, "objectIndex"    # I

    .prologue
    const/4 v11, 0x1

    const/4 v12, 0x0

    .line 79
    invoke-virtual {p0}, Lorg/icepdf/core/pobjects/ObjectStream;->init()V

    .line 80
    iget-object v10, p0, Lorg/icepdf/core/pobjects/ObjectStream;->m_iaObjectNumbers:[I

    if-eqz v10, :cond_0

    iget-object v10, p0, Lorg/icepdf/core/pobjects/ObjectStream;->m_laObjectOffset:[J

    if-eqz v10, :cond_0

    iget-object v10, p0, Lorg/icepdf/core/pobjects/ObjectStream;->m_iaObjectNumbers:[I

    array-length v10, v10

    iget-object v13, p0, Lorg/icepdf/core/pobjects/ObjectStream;->m_laObjectOffset:[J

    array-length v13, v13

    if-ne v10, v13, :cond_0

    if-ltz p2, :cond_0

    iget-object v10, p0, Lorg/icepdf/core/pobjects/ObjectStream;->m_iaObjectNumbers:[I

    array-length v10, v10

    move/from16 v0, p2

    if-lt v0, v10, :cond_1

    :cond_0
    move v3, v12

    .line 123
    :goto_0
    return v3

    .line 88
    :cond_1
    const/4 v3, 0x0

    .line 90
    .local v3, "gotSomething":Z
    :try_start_0
    iget-object v10, p0, Lorg/icepdf/core/pobjects/ObjectStream;->m_iaObjectNumbers:[I

    aget v5, v10, p2

    .line 91
    .local v5, "objectNumber":I
    iget-object v10, p0, Lorg/icepdf/core/pobjects/ObjectStream;->m_laObjectOffset:[J

    aget-wide v8, v10, p2

    .line 93
    .local v8, "position":J
    iget-object v10, p0, Lorg/icepdf/core/pobjects/ObjectStream;->m_DecodedStream:Lorg/icepdf/core/io/SeekableInput;

    invoke-interface {v10}, Lorg/icepdf/core/io/SeekableInput;->beginThreadAccess()V

    .line 94
    iget-object v10, p0, Lorg/icepdf/core/pobjects/ObjectStream;->m_DecodedStream:Lorg/icepdf/core/io/SeekableInput;

    invoke-interface {v10, v8, v9}, Lorg/icepdf/core/io/SeekableInput;->seekAbsolute(J)V

    .line 95
    new-instance v6, Lorg/icepdf/core/util/Parser;

    iget-object v10, p0, Lorg/icepdf/core/pobjects/ObjectStream;->m_DecodedStream:Lorg/icepdf/core/io/SeekableInput;

    const/4 v13, 0x1

    invoke-direct {v6, v10, v13}, Lorg/icepdf/core/util/Parser;-><init>(Lorg/icepdf/core/io/SeekableInput;I)V

    .line 100
    .local v6, "parser":Lorg/icepdf/core/util/Parser;
    move-object/from16 v0, p1

    invoke-virtual {v6, v0}, Lorg/icepdf/core/util/Parser;->getObject(Lorg/icepdf/core/util/Library;)Ljava/lang/Object;

    move-result-object v4

    .line 101
    .local v4, "ob":Ljava/lang/Object;
    if-nez v4, :cond_4

    .line 102
    new-instance v7, Lorg/icepdf/core/pobjects/Reference;

    const/4 v10, 0x0

    invoke-direct {v7, v5, v10}, Lorg/icepdf/core/pobjects/Reference;-><init>(II)V

    .line 103
    .local v7, "ref":Lorg/icepdf/core/pobjects/Reference;
    move-object/from16 v0, p1

    invoke-virtual {v6, v0, v7}, Lorg/icepdf/core/util/Parser;->addPObject(Lorg/icepdf/core/util/Library;Lorg/icepdf/core/pobjects/Reference;)Lorg/icepdf/core/pobjects/PObject;

    move-result-object v4

    .line 109
    .end local v4    # "ob":Ljava/lang/Object;
    .end local v7    # "ref":Lorg/icepdf/core/pobjects/Reference;
    :cond_2
    :goto_1
    if-eqz v4, :cond_3

    instance-of v10, v4, Lorg/icepdf/core/pobjects/Dictionary;

    if-eqz v10, :cond_3

    .line 110
    move-object v0, v4

    check-cast v0, Lorg/icepdf/core/pobjects/Dictionary;

    move-object v10, v0

    new-instance v13, Lorg/icepdf/core/pobjects/Reference;

    const/4 v14, 0x0

    invoke-direct {v13, v5, v14}, Lorg/icepdf/core/pobjects/Reference;-><init>(II)V

    invoke-virtual {v10, v13}, Lorg/icepdf/core/pobjects/Dictionary;->setPObjectReference(Lorg/icepdf/core/pobjects/Reference;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 115
    :cond_3
    if-eqz v4, :cond_5

    move v3, v11

    .line 121
    :goto_2
    iget-object v10, p0, Lorg/icepdf/core/pobjects/ObjectStream;->m_DecodedStream:Lorg/icepdf/core/io/SeekableInput;

    invoke-interface {v10}, Lorg/icepdf/core/io/SeekableInput;->endThreadAccess()V

    goto :goto_0

    .line 104
    .restart local v4    # "ob":Ljava/lang/Object;
    :cond_4
    :try_start_1
    instance-of v10, v4, Lorg/icepdf/core/pobjects/PObject;

    if-nez v10, :cond_2

    .line 105
    new-instance v7, Lorg/icepdf/core/pobjects/Reference;

    const/4 v10, 0x0

    invoke-direct {v7, v5, v10}, Lorg/icepdf/core/pobjects/Reference;-><init>(II)V

    .line 106
    .restart local v7    # "ref":Lorg/icepdf/core/pobjects/Reference;
    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v7}, Lorg/icepdf/core/util/Library;->addObject(Ljava/lang/Object;Lorg/icepdf/core/pobjects/Reference;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 117
    .end local v4    # "ob":Ljava/lang/Object;
    .end local v5    # "objectNumber":I
    .end local v6    # "parser":Lorg/icepdf/core/util/Parser;
    .end local v7    # "ref":Lorg/icepdf/core/pobjects/Reference;
    .end local v8    # "position":J
    :catch_0
    move-exception v2

    .line 118
    .local v2, "e":Ljava/lang/Exception;
    :try_start_2
    sget-object v10, Lorg/icepdf/core/pobjects/ObjectStream;->logger:Ljava/util/logging/Logger;

    sget-object v11, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string/jumbo v12, "Error loading PDF object."

    invoke-virtual {v10, v11, v12, v2}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 121
    iget-object v10, p0, Lorg/icepdf/core/pobjects/ObjectStream;->m_DecodedStream:Lorg/icepdf/core/io/SeekableInput;

    invoke-interface {v10}, Lorg/icepdf/core/io/SeekableInput;->endThreadAccess()V

    goto :goto_0

    .end local v2    # "e":Ljava/lang/Exception;
    .restart local v5    # "objectNumber":I
    .restart local v6    # "parser":Lorg/icepdf/core/util/Parser;
    .restart local v8    # "position":J
    :cond_5
    move v3, v12

    .line 115
    goto :goto_2

    .line 121
    .end local v5    # "objectNumber":I
    .end local v6    # "parser":Lorg/icepdf/core/util/Parser;
    .end local v8    # "position":J
    :catchall_0
    move-exception v10

    iget-object v11, p0, Lorg/icepdf/core/pobjects/ObjectStream;->m_DecodedStream:Lorg/icepdf/core/io/SeekableInput;

    invoke-interface {v11}, Lorg/icepdf/core/io/SeekableInput;->endThreadAccess()V

    throw v10
.end method
