.class Lorg/icepdf/core/pobjects/fonts/ofont/CMap;
.super Lorg/icepdf/core/pobjects/Dictionary;
.source "CMap.java"

# interfaces
.implements Lorg/icepdf/core/pobjects/fonts/CMap;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/icepdf/core/pobjects/fonts/ofont/CMap$CMapRange;
    }
.end annotation


# static fields
.field private static final logger:Ljava/util/logging/Logger;


# instance fields
.field private bfChars:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "[C>;"
        }
    .end annotation
.end field

.field private bfRange:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/icepdf/core/pobjects/fonts/ofont/CMap$CMapRange;",
            ">;"
        }
    .end annotation
.end field

.field private cMapInputStream:Ljava/io/InputStream;

.field private cMapName:Ljava/lang/String;

.field private cMapStream:Lorg/icepdf/core/pobjects/Stream;

.field private cMapType:F

.field private codeSpaceRange:[[I

.field private oneByte:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 56
    const-class v0, Lorg/icepdf/core/pobjects/fonts/ofont/CMap;

    invoke-virtual {v0}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Lorg/icepdf/core/pobjects/fonts/ofont/CMap;->logger:Ljava/util/logging/Logger;

    return-void
.end method

.method public constructor <init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;Ljava/io/InputStream;)V
    .locals 0
    .param p1, "l"    # Lorg/icepdf/core/util/Library;
    .param p2, "h"    # Ljava/util/Hashtable;
    .param p3, "cMapInputStream"    # Ljava/io/InputStream;

    .prologue
    .line 167
    invoke-direct {p0, p1, p2}, Lorg/icepdf/core/pobjects/Dictionary;-><init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V

    .line 168
    iput-object p3, p0, Lorg/icepdf/core/pobjects/fonts/ofont/CMap;->cMapInputStream:Ljava/io/InputStream;

    .line 169
    return-void
.end method

.method public constructor <init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;Lorg/icepdf/core/pobjects/Stream;)V
    .locals 0
    .param p1, "library"    # Lorg/icepdf/core/util/Library;
    .param p2, "entries"    # Ljava/util/Hashtable;
    .param p3, "cMapStream"    # Lorg/icepdf/core/pobjects/Stream;

    .prologue
    .line 162
    invoke-direct {p0, p1, p2}, Lorg/icepdf/core/pobjects/Dictionary;-><init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V

    .line 163
    iput-object p3, p0, Lorg/icepdf/core/pobjects/fonts/ofont/CMap;->cMapStream:Lorg/icepdf/core/pobjects/Stream;

    .line 164
    return-void
.end method

.method static synthetic access$000(Lorg/icepdf/core/pobjects/fonts/ofont/CMap;Ljava/lang/CharSequence;)[C
    .locals 1
    .param p0, "x0"    # Lorg/icepdf/core/pobjects/fonts/ofont/CMap;
    .param p1, "x1"    # Ljava/lang/CharSequence;

    .prologue
    .line 54
    invoke-direct {p0, p1}, Lorg/icepdf/core/pobjects/fonts/ofont/CMap;->convertToString(Ljava/lang/CharSequence;)[C

    move-result-object v0

    return-object v0
.end method

.method private convertToString(Ljava/lang/CharSequence;)[C
    .locals 6
    .param p1, "s"    # Ljava/lang/CharSequence;

    .prologue
    .line 636
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v4

    rem-int/lit8 v4, v4, 0x2

    if-eqz v4, :cond_2

    .line 637
    :cond_0
    const/4 v0, 0x0

    .line 643
    :cond_1
    return-object v0

    .line 639
    :cond_2
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v3

    .line 640
    .local v3, "len":I
    div-int/lit8 v4, v3, 0x2

    new-array v0, v4, [C

    .line 641
    .local v0, "dest":[C
    const/4 v1, 0x0

    .local v1, "i":I
    const/4 v2, 0x0

    .local v2, "j":I
    :goto_0
    if-ge v1, v3, :cond_1

    .line 642
    invoke-interface {p1, v1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v4

    shl-int/lit8 v4, v4, 0x8

    add-int/lit8 v5, v1, 0x1

    invoke-interface {p1, v5}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v5

    or-int/2addr v4, v5

    int-to-char v4, v4

    aput-char v4, v0, v2

    .line 641
    add-int/lit8 v1, v1, 0x2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method


# virtual methods
.method public init()V
    .locals 31

    .prologue
    .line 215
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/core/pobjects/fonts/ofont/CMap;->cMapInputStream:Ljava/io/InputStream;

    move-object/from16 v26, v0

    if-nez v26, :cond_0

    .line 216
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/core/pobjects/fonts/ofont/CMap;->cMapStream:Lorg/icepdf/core/pobjects/Stream;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Lorg/icepdf/core/pobjects/Stream;->getInputStreamForDecodedStreamBytes()Ljava/io/InputStream;

    move-result-object v26

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/icepdf/core/pobjects/fonts/ofont/CMap;->cMapInputStream:Ljava/io/InputStream;

    .line 220
    :cond_0
    sget-object v26, Lorg/icepdf/core/pobjects/fonts/ofont/CMap;->logger:Ljava/util/logging/Logger;

    sget-object v27, Ljava/util/logging/Level;->FINER:Ljava/util/logging/Level;

    invoke-virtual/range {v26 .. v27}, Ljava/util/logging/Logger;->isLoggable(Ljava/util/logging/Level;)Z

    move-result v26

    if-eqz v26, :cond_1

    .line 222
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/core/pobjects/fonts/ofont/CMap;->cMapInputStream:Ljava/io/InputStream;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    instance-of v0, v0, Lorg/icepdf/core/io/SeekableInput;

    move/from16 v26, v0

    if-eqz v26, :cond_3

    .line 223
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/core/pobjects/fonts/ofont/CMap;->cMapInputStream:Ljava/io/InputStream;

    move-object/from16 v26, v0

    check-cast v26, Lorg/icepdf/core/io/SeekableInput;

    const/16 v27, 0x0

    invoke-static/range {v26 .. v27}, Lorg/icepdf/core/util/Utils;->getContentFromSeekableInput(Lorg/icepdf/core/io/SeekableInput;Z)Ljava/lang/String;

    move-result-object v6

    .line 230
    .local v6, "content":Ljava/lang/String;
    :goto_0
    sget-object v26, Lorg/icepdf/core/pobjects/fonts/ofont/CMap;->logger:Ljava/util/logging/Logger;

    const-string/jumbo v27, "<------------------------ CMap"

    invoke-virtual/range {v26 .. v27}, Ljava/util/logging/Logger;->finer(Ljava/lang/String;)V

    .line 231
    sget-object v26, Lorg/icepdf/core/pobjects/fonts/ofont/CMap;->logger:Ljava/util/logging/Logger;

    move-object/from16 v0, v26

    invoke-virtual {v0, v6}, Ljava/util/logging/Logger;->finer(Ljava/lang/String;)V

    .line 232
    sget-object v26, Lorg/icepdf/core/pobjects/fonts/ofont/CMap;->logger:Ljava/util/logging/Logger;

    const-string/jumbo v27, "CMap ------------------------>  "

    invoke-virtual/range {v26 .. v27}, Ljava/util/logging/Logger;->finer(Ljava/lang/String;)V

    .line 235
    .end local v6    # "content":Ljava/lang/String;
    :cond_1
    new-instance v20, Lorg/icepdf/core/util/Parser;

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/core/pobjects/fonts/ofont/CMap;->cMapInputStream:Ljava/io/InputStream;

    move-object/from16 v26, v0

    move-object/from16 v0, v20

    move-object/from16 v1, v26

    invoke-direct {v0, v1}, Lorg/icepdf/core/util/Parser;-><init>(Ljava/io/InputStream;)V

    .line 242
    .local v20, "parser":Lorg/icepdf/core/util/Parser;
    const/16 v21, 0x0

    .line 244
    :goto_1
    invoke-virtual/range {v20 .. v20}, Lorg/icepdf/core/util/Parser;->getStreamObject()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v24

    .line 246
    .local v24, "token":Ljava/lang/Object;
    if-nez v24, :cond_4

    .line 457
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/core/pobjects/fonts/ofont/CMap;->cMapInputStream:Ljava/io/InputStream;

    move-object/from16 v26, v0

    if-eqz v26, :cond_2

    .line 459
    :try_start_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/core/pobjects/fonts/ofont/CMap;->cMapInputStream:Ljava/io/InputStream;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4

    .line 466
    .end local v20    # "parser":Lorg/icepdf/core/util/Parser;
    .end local v24    # "token":Ljava/lang/Object;
    :cond_2
    :goto_2
    return-void

    .line 225
    :cond_3
    const/16 v26, 0x1

    :try_start_2
    move/from16 v0, v26

    new-array v12, v0, [Ljava/io/InputStream;

    const/16 v26, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/core/pobjects/fonts/ofont/CMap;->cMapInputStream:Ljava/io/InputStream;

    move-object/from16 v27, v0

    aput-object v27, v12, v26

    .line 226
    .local v12, "inArray":[Ljava/io/InputStream;
    const/16 v26, 0x0

    move/from16 v0, v26

    invoke-static {v12, v0}, Lorg/icepdf/core/util/Utils;->getContentAndReplaceInputStream([Ljava/io/InputStream;Z)Ljava/lang/String;

    move-result-object v6

    .line 227
    .restart local v6    # "content":Ljava/lang/String;
    const/16 v26, 0x0

    aget-object v26, v12, v26

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/icepdf/core/pobjects/fonts/ofont/CMap;->cMapInputStream:Ljava/io/InputStream;
    :try_end_2
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 450
    .end local v6    # "content":Ljava/lang/String;
    .end local v12    # "inArray":[Ljava/io/InputStream;
    :catch_0
    move-exception v7

    .line 451
    .local v7, "e":Ljava/io/UnsupportedEncodingException;
    :try_start_3
    sget-object v26, Lorg/icepdf/core/pobjects/fonts/ofont/CMap;->logger:Ljava/util/logging/Logger;

    sget-object v27, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    const-string/jumbo v28, "CMap parsing error"

    move-object/from16 v0, v26

    move-object/from16 v1, v27

    move-object/from16 v2, v28

    invoke-virtual {v0, v1, v2, v7}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 457
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/core/pobjects/fonts/ofont/CMap;->cMapInputStream:Ljava/io/InputStream;

    move-object/from16 v26, v0

    if-eqz v26, :cond_2

    .line 459
    :try_start_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/core/pobjects/fonts/ofont/CMap;->cMapInputStream:Ljava/io/InputStream;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_2

    .line 461
    :catch_1
    move-exception v7

    .line 462
    .local v7, "e":Ljava/io/IOException;
    sget-object v26, Lorg/icepdf/core/pobjects/fonts/ofont/CMap;->logger:Ljava/util/logging/Logger;

    sget-object v27, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string/jumbo v28, "Error clossing cmap stream"

    move-object/from16 v0, v26

    move-object/from16 v1, v27

    move-object/from16 v2, v28

    invoke-virtual {v0, v1, v2, v7}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2

    .line 250
    .end local v7    # "e":Ljava/io/IOException;
    .restart local v20    # "parser":Lorg/icepdf/core/util/Parser;
    .restart local v24    # "token":Ljava/lang/Object;
    :cond_4
    :try_start_5
    invoke-virtual/range {v24 .. v24}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v15

    .line 251
    .local v15, "nameString":Ljava/lang/String;
    sget-object v26, Ljava/util/Locale;->US:Ljava/util/Locale;

    move-object/from16 v0, v26

    invoke-virtual {v15, v0}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v26

    const-string/jumbo v27, "cidsysteminfo"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v26

    if-ltz v26, :cond_5

    .line 254
    invoke-virtual/range {v20 .. v20}, Lorg/icepdf/core/util/Parser;->getStreamObject()Ljava/lang/Object;

    move-result-object v24

    .line 255
    move-object/from16 v0, v24

    instance-of v0, v0, Ljava/util/Hashtable;

    move/from16 v26, v0

    if-eqz v26, :cond_5

    .line 258
    invoke-virtual/range {v20 .. v20}, Lorg/icepdf/core/util/Parser;->getStreamObject()Ljava/lang/Object;

    move-result-object v24

    .line 263
    :cond_5
    move-object/from16 v0, v24

    instance-of v0, v0, Lorg/icepdf/core/pobjects/Name;

    move/from16 v26, v0

    if-eqz v26, :cond_8

    .line 264
    invoke-virtual/range {v24 .. v24}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v15

    .line 266
    sget-object v26, Ljava/util/Locale;->US:Ljava/util/Locale;

    move-object/from16 v0, v26

    invoke-virtual {v15, v0}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v26

    const-string/jumbo v27, "cmapname"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v26

    if-ltz v26, :cond_6

    .line 268
    invoke-virtual/range {v20 .. v20}, Lorg/icepdf/core/util/Parser;->getStreamObject()Ljava/lang/Object;

    move-result-object v24

    .line 269
    invoke-virtual/range {v24 .. v24}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/icepdf/core/pobjects/fonts/ofont/CMap;->cMapName:Ljava/lang/String;

    .line 271
    invoke-virtual/range {v20 .. v20}, Lorg/icepdf/core/util/Parser;->getStreamObject()Ljava/lang/Object;

    move-result-object v24

    .line 274
    :cond_6
    sget-object v26, Ljava/util/Locale;->US:Ljava/util/Locale;

    move-object/from16 v0, v26

    invoke-virtual {v15, v0}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v26

    const-string/jumbo v27, "cmaptype"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v26

    if-ltz v26, :cond_7

    .line 276
    invoke-virtual/range {v20 .. v20}, Lorg/icepdf/core/util/Parser;->getStreamObject()Ljava/lang/Object;

    move-result-object v24

    .line 277
    invoke-virtual/range {v24 .. v24}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v26 .. v26}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v26

    move/from16 v0, v26

    move-object/from16 v1, p0

    iput v0, v1, Lorg/icepdf/core/pobjects/fonts/ofont/CMap;->cMapType:F

    .line 279
    invoke-virtual/range {v20 .. v20}, Lorg/icepdf/core/util/Parser;->getStreamObject()Ljava/lang/Object;

    move-result-object v24

    .line 282
    :cond_7
    sget-object v26, Ljava/util/Locale;->US:Ljava/util/Locale;

    move-object/from16 v0, v26

    invoke-virtual {v15, v0}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v26

    const-string/jumbo v27, "usemap"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v26

    if-ltz v26, :cond_8

    .line 287
    :cond_8
    move-object/from16 v0, v24

    instance-of v0, v0, Ljava/lang/String;

    move/from16 v26, v0

    if-eqz v26, :cond_17

    .line 288
    move-object/from16 v0, v24

    check-cast v0, Ljava/lang/String;

    move-object/from16 v23, v0

    .line 290
    .local v23, "stringToken":Ljava/lang/String;
    const-string/jumbo v26, "begincodespacerange"

    move-object/from16 v0, v26

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v26

    if-eqz v26, :cond_b

    .line 293
    const/16 v16, 0x0

    .line 294
    .local v16, "numberOfRanges":I
    if-eqz v21, :cond_9

    .line 297
    invoke-virtual/range {v21 .. v21}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v26 .. v26}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v26

    move/from16 v0, v26

    float-to-int v0, v0

    move/from16 v16, v0

    .line 300
    const/16 v26, 0x2

    move/from16 v0, v16

    move/from16 v1, v26

    filled-new-array {v0, v1}, [I

    move-result-object v26

    sget-object v27, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    move-object/from16 v0, v27

    move-object/from16 v1, v26

    invoke-static {v0, v1}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, [[I

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/icepdf/core/pobjects/fonts/ofont/CMap;->codeSpaceRange:[[I

    .line 303
    :cond_9
    const/4 v11, 0x0

    .local v11, "i":I
    :goto_3
    move/from16 v0, v16

    if-ge v11, v0, :cond_b

    .line 305
    invoke-virtual/range {v20 .. v20}, Lorg/icepdf/core/util/Parser;->getStreamObject()Ljava/lang/Object;

    move-result-object v24

    .line 306
    move-object/from16 v0, v24

    check-cast v0, Lorg/icepdf/core/pobjects/StringObject;

    move-object v9, v0

    .line 307
    .local v9, "hexToken":Lorg/icepdf/core/pobjects/StringObject;
    const/16 v26, 0x0

    invoke-interface {v9}, Lorg/icepdf/core/pobjects/StringObject;->getLength()I

    move-result v27

    move/from16 v0, v26

    move/from16 v1, v27

    invoke-interface {v9, v0, v1}, Lorg/icepdf/core/pobjects/StringObject;->getUnsignedInt(II)I

    move-result v22

    .line 310
    .local v22, "startRange":I
    invoke-virtual/range {v20 .. v20}, Lorg/icepdf/core/util/Parser;->getStreamObject()Ljava/lang/Object;

    move-result-object v24

    .line 311
    move-object/from16 v0, v24

    check-cast v0, Lorg/icepdf/core/pobjects/StringObject;

    move-object v9, v0

    .line 312
    invoke-interface {v9}, Lorg/icepdf/core/pobjects/StringObject;->getLength()I

    move-result v14

    .line 313
    .local v14, "length":I
    const/16 v26, 0x0

    move/from16 v0, v26

    invoke-interface {v9, v0, v14}, Lorg/icepdf/core/pobjects/StringObject;->getUnsignedInt(II)I

    move-result v8

    .line 314
    .local v8, "endRange":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/core/pobjects/fonts/ofont/CMap;->codeSpaceRange:[[I

    move-object/from16 v26, v0

    aget-object v26, v26, v11

    const/16 v27, 0x0

    aput v22, v26, v27

    .line 315
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/core/pobjects/fonts/ofont/CMap;->codeSpaceRange:[[I

    move-object/from16 v26, v0

    aget-object v26, v26, v11

    const/16 v27, 0x1

    aput v8, v26, v27

    .line 316
    const/16 v26, 0x2

    move/from16 v0, v26

    if-ne v14, v0, :cond_a

    .line 317
    const/16 v26, 0x1

    move/from16 v0, v26

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lorg/icepdf/core/pobjects/fonts/ofont/CMap;->oneByte:Z

    .line 303
    :cond_a
    add-int/lit8 v11, v11, 0x1

    goto :goto_3

    .line 322
    .end local v8    # "endRange":I
    .end local v9    # "hexToken":Lorg/icepdf/core/pobjects/StringObject;
    .end local v11    # "i":I
    .end local v14    # "length":I
    .end local v16    # "numberOfRanges":I
    .end local v22    # "startRange":I
    :cond_b
    const-string/jumbo v26, "beginbfchar"

    move-object/from16 v0, v26

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v26

    if-eqz v26, :cond_f

    .line 325
    const/16 v17, 0x0

    .line 326
    .local v17, "numberOfbfChar":I
    if-eqz v21, :cond_c

    .line 327
    invoke-virtual/range {v21 .. v21}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v26 .. v26}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v26

    move/from16 v0, v26

    float-to-int v0, v0

    move/from16 v17, v0

    .line 331
    :cond_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/core/pobjects/fonts/ofont/CMap;->bfChars:Ljava/util/Map;

    move-object/from16 v26, v0

    if-nez v26, :cond_d

    .line 332
    new-instance v26, Ljava/util/HashMap;

    move-object/from16 v0, v26

    move/from16 v1, v17

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/icepdf/core/pobjects/fonts/ofont/CMap;->bfChars:Ljava/util/Map;

    .line 335
    :cond_d
    const/4 v11, 0x0

    .restart local v11    # "i":I
    :goto_4
    move/from16 v0, v17

    if-ge v11, v0, :cond_f

    .line 337
    invoke-virtual/range {v20 .. v20}, Lorg/icepdf/core/util/Parser;->getStreamObject()Ljava/lang/Object;

    move-result-object v24

    .line 338
    move-object/from16 v0, v24

    check-cast v0, Lorg/icepdf/core/pobjects/StringObject;

    move-object v9, v0

    .line 339
    .restart local v9    # "hexToken":Lorg/icepdf/core/pobjects/StringObject;
    const/16 v26, 0x0

    invoke-interface {v9}, Lorg/icepdf/core/pobjects/StringObject;->getLength()I

    move-result v27

    move/from16 v0, v26

    move/from16 v1, v27

    invoke-interface {v9, v0, v1}, Lorg/icepdf/core/pobjects/StringObject;->getUnsignedInt(II)I

    move-result v26

    invoke-static/range {v26 .. v26}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    .line 342
    .local v13, "key":Ljava/lang/Integer;
    invoke-virtual/range {v20 .. v20}, Lorg/icepdf/core/util/Parser;->getStreamObject()Ljava/lang/Object;

    move-result-object v24

    .line 343
    move-object/from16 v0, v24

    check-cast v0, Lorg/icepdf/core/pobjects/StringObject;

    move-object v9, v0

    .line 345
    const/16 v25, 0x0

    .line 352
    .local v25, "value":[C
    const/16 v26, 0x0

    invoke-interface {v9}, Lorg/icepdf/core/pobjects/StringObject;->getLength()I

    move-result v27

    move/from16 v0, v26

    move/from16 v1, v27

    invoke-interface {v9, v0, v1}, Lorg/icepdf/core/pobjects/StringObject;->getUnsignedInt(II)I

    move-result v26

    invoke-static/range {v26 .. v26}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    .line 354
    .local v10, "hexValue":Ljava/lang/Integer;
    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v26

    const/16 v27, 0x1

    invoke-static/range {v26 .. v27}, Lcom/samsung/thumbnail/util/Utils;->getUnicodeForBullet(IZ)Ljava/lang/String;

    move-result-object v5

    .line 356
    .local v5, "bulletUnicode":Ljava/lang/String;
    invoke-static {v5}, Lcom/samsung/thumbnail/util/Utils;->isValidInteger(Ljava/lang/String;)Z

    move-result v26

    if-eqz v26, :cond_e

    .line 360
    invoke-interface {v9}, Lorg/icepdf/core/pobjects/StringObject;->getLiteralStringBuffer()Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-direct {v0, v1}, Lorg/icepdf/core/pobjects/fonts/ofont/CMap;->convertToString(Ljava/lang/CharSequence;)[C

    move-result-object v25

    .line 367
    :goto_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/core/pobjects/fonts/ofont/CMap;->bfChars:Ljava/util/Map;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    move-object/from16 v1, v25

    invoke-interface {v0, v13, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 335
    add-int/lit8 v11, v11, 0x1

    goto :goto_4

    .line 364
    :cond_e
    invoke-virtual {v5}, Ljava/lang/String;->toCharArray()[C

    move-result-object v25

    goto :goto_5

    .line 372
    .end local v5    # "bulletUnicode":Ljava/lang/String;
    .end local v9    # "hexToken":Lorg/icepdf/core/pobjects/StringObject;
    .end local v10    # "hexValue":Ljava/lang/Integer;
    .end local v11    # "i":I
    .end local v13    # "key":Ljava/lang/Integer;
    .end local v17    # "numberOfbfChar":I
    .end local v25    # "value":[C
    :cond_f
    const-string/jumbo v26, "beginbfrange"

    move-object/from16 v0, v26

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v26

    if-eqz v26, :cond_13

    .line 374
    const/16 v18, 0x0

    .line 375
    .local v18, "numberOfbfRanges":I
    if-eqz v21, :cond_10

    .line 376
    invoke-virtual/range {v21 .. v21}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v26 .. v26}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v26

    move/from16 v0, v26

    float-to-int v0, v0

    move/from16 v18, v0

    .line 380
    :cond_10
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/core/pobjects/fonts/ofont/CMap;->bfRange:Ljava/util/List;

    move-object/from16 v26, v0

    if-nez v26, :cond_11

    .line 381
    new-instance v26, Ljava/util/ArrayList;

    move-object/from16 v0, v26

    move/from16 v1, v18

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/icepdf/core/pobjects/fonts/ofont/CMap;->bfRange:Ljava/util/List;

    .line 387
    :cond_11
    const/4 v11, 0x0

    .restart local v11    # "i":I
    :goto_6
    move/from16 v0, v18

    if-ge v11, v0, :cond_13

    .line 389
    invoke-virtual/range {v20 .. v20}, Lorg/icepdf/core/util/Parser;->getStreamObject()Ljava/lang/Object;

    move-result-object v24

    .line 390
    move-object/from16 v0, v24

    instance-of v0, v0, Lorg/icepdf/core/pobjects/StringObject;

    move/from16 v26, v0

    if-eqz v26, :cond_13

    .line 391
    move-object/from16 v0, v24

    check-cast v0, Lorg/icepdf/core/pobjects/StringObject;

    move-object v9, v0

    .line 392
    .restart local v9    # "hexToken":Lorg/icepdf/core/pobjects/StringObject;
    const/16 v26, 0x0

    invoke-interface {v9}, Lorg/icepdf/core/pobjects/StringObject;->getLength()I

    move-result v27

    move/from16 v0, v26

    move/from16 v1, v27

    invoke-interface {v9, v0, v1}, Lorg/icepdf/core/pobjects/StringObject;->getUnsignedInt(II)I

    move-result v26

    invoke-static/range {v26 .. v26}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v22

    .line 398
    .local v22, "startRange":Ljava/lang/Integer;
    invoke-virtual/range {v20 .. v20}, Lorg/icepdf/core/util/Parser;->getStreamObject()Ljava/lang/Object;

    move-result-object v24

    .line 399
    move-object/from16 v0, v24

    instance-of v0, v0, Lorg/icepdf/core/pobjects/StringObject;

    move/from16 v26, v0

    if-eqz v26, :cond_13

    .line 400
    move-object/from16 v0, v24

    check-cast v0, Lorg/icepdf/core/pobjects/StringObject;

    move-object v9, v0

    .line 401
    const/16 v26, 0x0

    invoke-interface {v9}, Lorg/icepdf/core/pobjects/StringObject;->getLength()I

    move-result v27

    move/from16 v0, v26

    move/from16 v1, v27

    invoke-interface {v9, v0, v1}, Lorg/icepdf/core/pobjects/StringObject;->getUnsignedInt(II)I

    move-result v26

    invoke-static/range {v26 .. v26}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    .line 408
    .local v8, "endRange":Ljava/lang/Integer;
    invoke-virtual/range {v20 .. v20}, Lorg/icepdf/core/util/Parser;->getStreamObject()Ljava/lang/Object;

    move-result-object v24

    .line 409
    move-object/from16 v0, v24

    instance-of v0, v0, Ljava/util/Vector;

    move/from16 v26, v0

    if-eqz v26, :cond_12

    .line 410
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/core/pobjects/fonts/ofont/CMap;->bfRange:Ljava/util/List;

    move-object/from16 v27, v0

    new-instance v28, Lorg/icepdf/core/pobjects/fonts/ofont/CMap$CMapRange;

    invoke-virtual/range {v22 .. v22}, Ljava/lang/Integer;->intValue()I

    move-result v29

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v30

    move-object/from16 v0, v24

    check-cast v0, Ljava/util/Vector;

    move-object/from16 v26, v0

    move-object/from16 v0, v28

    move-object/from16 v1, p0

    move/from16 v2, v29

    move/from16 v3, v30

    move-object/from16 v4, v26

    invoke-direct {v0, v1, v2, v3, v4}, Lorg/icepdf/core/pobjects/fonts/ofont/CMap$CMapRange;-><init>(Lorg/icepdf/core/pobjects/fonts/ofont/CMap;IILjava/util/Vector;)V

    invoke-interface/range {v27 .. v28}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 387
    :goto_7
    add-int/lit8 v11, v11, 0x1

    goto :goto_6

    .line 414
    :cond_12
    move-object/from16 v0, v24

    check-cast v0, Lorg/icepdf/core/pobjects/StringObject;

    move-object v9, v0

    .line 415
    const/16 v26, 0x0

    invoke-interface {v9}, Lorg/icepdf/core/pobjects/StringObject;->getLength()I

    move-result v27

    move/from16 v0, v26

    move/from16 v1, v27

    invoke-interface {v9, v0, v1}, Lorg/icepdf/core/pobjects/StringObject;->getUnsignedInt(II)I

    move-result v26

    invoke-static/range {v26 .. v26}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v19

    .line 416
    .local v19, "offset":Ljava/lang/Integer;
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/core/pobjects/fonts/ofont/CMap;->bfRange:Ljava/util/List;

    move-object/from16 v26, v0

    new-instance v27, Lorg/icepdf/core/pobjects/fonts/ofont/CMap$CMapRange;

    invoke-virtual/range {v22 .. v22}, Ljava/lang/Integer;->intValue()I

    move-result v28

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v29

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Integer;->intValue()I

    move-result v30

    move-object/from16 v0, v27

    move-object/from16 v1, p0

    move/from16 v2, v28

    move/from16 v3, v29

    move/from16 v4, v30

    invoke-direct {v0, v1, v2, v3, v4}, Lorg/icepdf/core/pobjects/fonts/ofont/CMap$CMapRange;-><init>(Lorg/icepdf/core/pobjects/fonts/ofont/CMap;III)V

    invoke-interface/range {v26 .. v27}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_5
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_7

    .line 453
    .end local v8    # "endRange":Ljava/lang/Integer;
    .end local v9    # "hexToken":Lorg/icepdf/core/pobjects/StringObject;
    .end local v11    # "i":I
    .end local v15    # "nameString":Ljava/lang/String;
    .end local v18    # "numberOfbfRanges":I
    .end local v19    # "offset":Ljava/lang/Integer;
    .end local v20    # "parser":Lorg/icepdf/core/util/Parser;
    .end local v22    # "startRange":Ljava/lang/Integer;
    .end local v23    # "stringToken":Ljava/lang/String;
    .end local v24    # "token":Ljava/lang/Object;
    :catch_2
    move-exception v26

    .line 457
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/core/pobjects/fonts/ofont/CMap;->cMapInputStream:Ljava/io/InputStream;

    move-object/from16 v26, v0

    if-eqz v26, :cond_2

    .line 459
    :try_start_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/core/pobjects/fonts/ofont/CMap;->cMapInputStream:Ljava/io/InputStream;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Ljava/io/InputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    goto/16 :goto_2

    .line 461
    :catch_3
    move-exception v7

    .line 462
    .restart local v7    # "e":Ljava/io/IOException;
    sget-object v26, Lorg/icepdf/core/pobjects/fonts/ofont/CMap;->logger:Ljava/util/logging/Logger;

    sget-object v27, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string/jumbo v28, "Error clossing cmap stream"

    move-object/from16 v0, v26

    move-object/from16 v1, v27

    move-object/from16 v2, v28

    invoke-virtual {v0, v1, v2, v7}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_2

    .line 430
    .end local v7    # "e":Ljava/io/IOException;
    .restart local v15    # "nameString":Ljava/lang/String;
    .restart local v20    # "parser":Lorg/icepdf/core/util/Parser;
    .restart local v23    # "stringToken":Ljava/lang/String;
    .restart local v24    # "token":Ljava/lang/Object;
    :cond_13
    :try_start_7
    const-string/jumbo v26, "begincidchar"

    move-object/from16 v0, v23

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v26

    if-eqz v26, :cond_14

    .line 434
    :cond_14
    const-string/jumbo v26, "begincidrange"

    move-object/from16 v0, v23

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v26

    if-eqz v26, :cond_15

    .line 438
    :cond_15
    const-string/jumbo v26, "beginnotdefchar"

    move-object/from16 v0, v23

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v26

    if-eqz v26, :cond_16

    .line 442
    :cond_16
    const-string/jumbo v26, "beginnotdefrange"

    move-object/from16 v0, v23

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z
    :try_end_7
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_7 .. :try_end_7} :catch_0
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_2
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    move-result v26

    if-eqz v26, :cond_17

    .line 447
    .end local v23    # "stringToken":Ljava/lang/String;
    :cond_17
    move-object/from16 v21, v24

    .line 448
    .local v21, "previousToken":Ljava/lang/Object;
    goto/16 :goto_1

    .line 461
    .end local v15    # "nameString":Ljava/lang/String;
    .end local v21    # "previousToken":Ljava/lang/Object;
    :catch_4
    move-exception v7

    .line 462
    .restart local v7    # "e":Ljava/io/IOException;
    sget-object v26, Lorg/icepdf/core/pobjects/fonts/ofont/CMap;->logger:Ljava/util/logging/Logger;

    sget-object v27, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string/jumbo v28, "Error clossing cmap stream"

    move-object/from16 v0, v26

    move-object/from16 v1, v27

    move-object/from16 v2, v28

    invoke-virtual {v0, v1, v2, v7}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_2

    .line 457
    .end local v7    # "e":Ljava/io/IOException;
    .end local v20    # "parser":Lorg/icepdf/core/util/Parser;
    .end local v24    # "token":Ljava/lang/Object;
    :catchall_0
    move-exception v26

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/core/pobjects/fonts/ofont/CMap;->cMapInputStream:Ljava/io/InputStream;

    move-object/from16 v27, v0

    if-eqz v27, :cond_18

    .line 459
    :try_start_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/core/pobjects/fonts/ofont/CMap;->cMapInputStream:Ljava/io/InputStream;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Ljava/io/InputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_5

    .line 463
    :cond_18
    :goto_8
    throw v26

    .line 461
    :catch_5
    move-exception v7

    .line 462
    .restart local v7    # "e":Ljava/io/IOException;
    sget-object v27, Lorg/icepdf/core/pobjects/fonts/ofont/CMap;->logger:Ljava/util/logging/Logger;

    sget-object v28, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string/jumbo v29, "Error clossing cmap stream"

    move-object/from16 v0, v27

    move-object/from16 v1, v28

    move-object/from16 v2, v29

    invoke-virtual {v0, v1, v2, v7}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_8
.end method

.method public isOneByte(I)Z
    .locals 1
    .param p1, "cid"    # I

    .prologue
    .line 172
    iget-boolean v0, p0, Lorg/icepdf/core/pobjects/fonts/ofont/CMap;->oneByte:Z

    return v0
.end method

.method public toSelector(C)C
    .locals 6
    .param p1, "charMap"    # C

    .prologue
    const/4 v5, 0x0

    .line 508
    iget-object v3, p0, Lorg/icepdf/core/pobjects/fonts/ofont/CMap;->bfChars:Ljava/util/Map;

    if-eqz v3, :cond_1

    .line 509
    iget-object v3, p0, Lorg/icepdf/core/pobjects/fonts/ofont/CMap;->bfChars:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [C

    .line 510
    .local v2, "tmp":[C
    if-eqz v2, :cond_1

    .line 511
    aget-char p1, v2, v5

    .line 522
    .end local v2    # "tmp":[C
    .end local p1    # "charMap":C
    :cond_0
    :goto_0
    return p1

    .line 515
    .restart local p1    # "charMap":C
    :cond_1
    iget-object v3, p0, Lorg/icepdf/core/pobjects/fonts/ofont/CMap;->bfRange:Ljava/util/List;

    if-eqz v3, :cond_0

    .line 516
    iget-object v3, p0, Lorg/icepdf/core/pobjects/fonts/ofont/CMap;->bfRange:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/icepdf/core/pobjects/fonts/ofont/CMap$CMapRange;

    .line 517
    .local v0, "aBfRange":Lorg/icepdf/core/pobjects/fonts/ofont/CMap$CMapRange;
    invoke-virtual {v0, p1}, Lorg/icepdf/core/pobjects/fonts/ofont/CMap$CMapRange;->inRange(I)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 518
    invoke-virtual {v0, p1}, Lorg/icepdf/core/pobjects/fonts/ofont/CMap$CMapRange;->getCMapValue(I)[C

    move-result-object v3

    aget-char p1, v3, v5

    goto :goto_0
.end method

.method public toSelector(CZ)C
    .locals 1
    .param p1, "charMap"    # C
    .param p2, "isCFF"    # Z

    .prologue
    .line 526
    invoke-virtual {p0, p1}, Lorg/icepdf/core/pobjects/fonts/ofont/CMap;->toSelector(C)C

    move-result v0

    return v0
.end method

.method public toUnicode(C)Ljava/lang/String;
    .locals 5
    .param p1, "ch"    # C

    .prologue
    .line 470
    iget-object v3, p0, Lorg/icepdf/core/pobjects/fonts/ofont/CMap;->bfChars:Ljava/util/Map;

    if-eqz v3, :cond_0

    .line 471
    iget-object v3, p0, Lorg/icepdf/core/pobjects/fonts/ofont/CMap;->bfChars:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [C

    .line 472
    .local v2, "tmp":[C
    if-eqz v2, :cond_0

    .line 473
    invoke-static {v2}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v3

    .line 484
    .end local v2    # "tmp":[C
    :goto_0
    return-object v3

    .line 477
    :cond_0
    iget-object v3, p0, Lorg/icepdf/core/pobjects/fonts/ofont/CMap;->bfRange:Ljava/util/List;

    if-eqz v3, :cond_2

    .line 478
    iget-object v3, p0, Lorg/icepdf/core/pobjects/fonts/ofont/CMap;->bfRange:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/icepdf/core/pobjects/fonts/ofont/CMap$CMapRange;

    .line 479
    .local v0, "aBfRange":Lorg/icepdf/core/pobjects/fonts/ofont/CMap$CMapRange;
    invoke-virtual {v0, p1}, Lorg/icepdf/core/pobjects/fonts/ofont/CMap$CMapRange;->inRange(I)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 480
    invoke-virtual {v0, p1}, Lorg/icepdf/core/pobjects/fonts/ofont/CMap$CMapRange;->getCMapValue(I)[C

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 484
    .end local v0    # "aBfRange":Lorg/icepdf/core/pobjects/fonts/ofont/CMap$CMapRange;
    .end local v1    # "i$":Ljava/util/Iterator;
    :cond_2
    invoke-static {p1}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v3

    goto :goto_0
.end method
