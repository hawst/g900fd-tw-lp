.class public Lorg/icepdf/core/pobjects/PDate;
.super Ljava/lang/Object;
.source "PDate.java"


# static fields
.field private static final DATE_PREFIX:Ljava/lang/String; = "D:"

.field private static final OFFSET_0:I = 0x1

.field private static final OFFSET_DD:I = 0x2

.field private static final OFFSET_HH:I = 0x2

.field private static final OFFSET_MM:I = 0x2

.field private static final OFFSET_SS:I = 0x2

.field private static final OFFSET_YYYY:I = 0x4

.field private static final OFFSET_mm:I = 0x2

.field private static monthNames:[Ljava/lang/String;


# instance fields
.field private day:Ljava/lang/String;

.field private hour:Ljava/lang/String;

.field private minute:Ljava/lang/String;

.field private month:Ljava/lang/String;

.field private notStandardFormat:Z

.field private second:Ljava/lang/String;

.field private timeZoneHour:Ljava/lang/String;

.field private timeZoneMinute:Ljava/lang/String;

.field private timeZoneOffset:Ljava/lang/String;

.field private year:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 80
    const/16 v0, 0xd

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v2, ""

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string/jumbo v2, "January"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string/jumbo v2, "February"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string/jumbo v2, "March"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string/jumbo v2, "April"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string/jumbo v2, "May"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string/jumbo v2, "June"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string/jumbo v2, "July"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string/jumbo v2, "August"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string/jumbo v2, "September"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string/jumbo v2, "October"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string/jumbo v2, "November"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string/jumbo v2, "December"

    aput-object v2, v0, v1

    sput-object v0, Lorg/icepdf/core/pobjects/PDate;->monthNames:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lorg/icepdf/core/pobjects/security/SecurityManager;Ljava/lang/String;)V
    .locals 1
    .param p1, "securityManager"    # Lorg/icepdf/core/pobjects/security/SecurityManager;
    .param p2, "date"    # Ljava/lang/String;

    .prologue
    .line 115
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 96
    const-string/jumbo v0, ""

    iput-object v0, p0, Lorg/icepdf/core/pobjects/PDate;->year:Ljava/lang/String;

    .line 97
    const-string/jumbo v0, ""

    iput-object v0, p0, Lorg/icepdf/core/pobjects/PDate;->month:Ljava/lang/String;

    .line 98
    const-string/jumbo v0, ""

    iput-object v0, p0, Lorg/icepdf/core/pobjects/PDate;->day:Ljava/lang/String;

    .line 99
    const-string/jumbo v0, ""

    iput-object v0, p0, Lorg/icepdf/core/pobjects/PDate;->hour:Ljava/lang/String;

    .line 100
    const-string/jumbo v0, ""

    iput-object v0, p0, Lorg/icepdf/core/pobjects/PDate;->minute:Ljava/lang/String;

    .line 101
    const-string/jumbo v0, ""

    iput-object v0, p0, Lorg/icepdf/core/pobjects/PDate;->second:Ljava/lang/String;

    .line 102
    const-string/jumbo v0, ""

    iput-object v0, p0, Lorg/icepdf/core/pobjects/PDate;->timeZoneOffset:Ljava/lang/String;

    .line 103
    const-string/jumbo v0, ""

    iput-object v0, p0, Lorg/icepdf/core/pobjects/PDate;->timeZoneHour:Ljava/lang/String;

    .line 104
    const-string/jumbo v0, ""

    iput-object v0, p0, Lorg/icepdf/core/pobjects/PDate;->timeZoneMinute:Ljava/lang/String;

    .line 107
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/icepdf/core/pobjects/PDate;->notStandardFormat:Z

    .line 117
    if-eqz p2, :cond_0

    .line 118
    invoke-direct {p0, p2}, Lorg/icepdf/core/pobjects/PDate;->parseDate(Ljava/lang/String;)V

    .line 120
    :cond_0
    return-void
.end method

.method private getMonth(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "month"    # Ljava/lang/String;

    .prologue
    .line 398
    const/4 v0, 0x0

    .line 400
    .local v0, "monthIndex":I
    :try_start_0
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 406
    :goto_0
    sget-object v1, Lorg/icepdf/core/pobjects/PDate;->monthNames:[Ljava/lang/String;

    aget-object v1, v1, v0

    return-object v1

    .line 403
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method private parseAdobeDate(Ljava/lang/String;)V
    .locals 4
    .param p1, "date"    # Ljava/lang/String;

    .prologue
    .line 342
    const/4 v1, 0x0

    .line 343
    .local v1, "totalOffset":I
    const/4 v0, 0x0

    .line 346
    .local v0, "currentOffset":I
    const/4 v2, 0x4

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    if-gt v2, v3, :cond_0

    .line 347
    add-int/lit8 v0, v1, 0x4

    .line 348
    invoke-virtual {p1, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lorg/icepdf/core/pobjects/PDate;->year:Ljava/lang/String;

    .line 349
    add-int/lit8 v1, v1, 0x4

    .line 351
    :cond_0
    add-int/lit8 v2, v1, 0x2

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    if-gt v2, v3, :cond_1

    .line 352
    add-int/lit8 v0, v1, 0x2

    .line 353
    invoke-virtual {p1, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lorg/icepdf/core/pobjects/PDate;->month:Ljava/lang/String;

    .line 354
    add-int/lit8 v1, v1, 0x2

    .line 356
    :cond_1
    add-int/lit8 v2, v1, 0x2

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    if-gt v2, v3, :cond_2

    .line 357
    add-int/lit8 v0, v1, 0x2

    .line 358
    invoke-virtual {p1, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lorg/icepdf/core/pobjects/PDate;->day:Ljava/lang/String;

    .line 359
    add-int/lit8 v1, v1, 0x2

    .line 361
    :cond_2
    add-int/lit8 v2, v1, 0x2

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    if-gt v2, v3, :cond_3

    .line 362
    add-int/lit8 v0, v1, 0x2

    .line 363
    invoke-virtual {p1, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lorg/icepdf/core/pobjects/PDate;->hour:Ljava/lang/String;

    .line 364
    add-int/lit8 v1, v1, 0x2

    .line 366
    :cond_3
    add-int/lit8 v2, v1, 0x2

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    if-gt v2, v3, :cond_4

    .line 367
    add-int/lit8 v0, v1, 0x2

    .line 368
    invoke-virtual {p1, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lorg/icepdf/core/pobjects/PDate;->minute:Ljava/lang/String;

    .line 369
    add-int/lit8 v1, v1, 0x2

    .line 371
    :cond_4
    add-int/lit8 v2, v1, 0x2

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    if-gt v2, v3, :cond_5

    .line 372
    add-int/lit8 v0, v1, 0x2

    .line 373
    invoke-virtual {p1, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lorg/icepdf/core/pobjects/PDate;->second:Ljava/lang/String;

    .line 374
    add-int/lit8 v1, v1, 0x2

    .line 376
    :cond_5
    add-int/lit8 v2, v1, 0x1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    if-gt v2, v3, :cond_6

    .line 377
    add-int/lit8 v0, v1, 0x1

    .line 378
    invoke-virtual {p1, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lorg/icepdf/core/pobjects/PDate;->timeZoneOffset:Ljava/lang/String;

    .line 379
    add-int/lit8 v1, v1, 0x1

    .line 381
    :cond_6
    add-int/lit8 v2, v1, 0x2

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    if-gt v2, v3, :cond_7

    .line 382
    add-int/lit8 v0, v1, 0x2

    .line 383
    invoke-virtual {p1, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lorg/icepdf/core/pobjects/PDate;->timeZoneHour:Ljava/lang/String;

    .line 384
    add-int/lit8 v1, v1, 0x2

    .line 387
    :cond_7
    add-int/lit8 v2, v1, 0x4

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    if-gt v2, v3, :cond_8

    .line 389
    add-int/lit8 v2, v1, 0x1

    add-int/lit8 v3, v1, 0x3

    invoke-virtual {p1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lorg/icepdf/core/pobjects/PDate;->timeZoneMinute:Ljava/lang/String;

    .line 392
    :cond_8
    return-void
.end method

.method private parseDate(Ljava/lang/String;)V
    .locals 1
    .param p1, "date"    # Ljava/lang/String;

    .prologue
    .line 281
    const-string/jumbo v0, "D:"

    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    if-ltz v0, :cond_0

    .line 282
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p1

    .line 283
    invoke-direct {p0, p1}, Lorg/icepdf/core/pobjects/PDate;->parseAdobeDate(Ljava/lang/String;)V

    .line 303
    :goto_0
    return-void

    .line 286
    :cond_0
    const-string/jumbo v0, "/"

    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    if-ltz v0, :cond_1

    .line 287
    invoke-direct {p0, p1}, Lorg/icepdf/core/pobjects/PDate;->parseGhostScriptDate(Ljava/lang/String;)V

    goto :goto_0

    .line 291
    :cond_1
    iput-object p1, p0, Lorg/icepdf/core/pobjects/PDate;->year:Ljava/lang/String;

    .line 292
    iput-object p1, p0, Lorg/icepdf/core/pobjects/PDate;->month:Ljava/lang/String;

    .line 293
    iput-object p1, p0, Lorg/icepdf/core/pobjects/PDate;->day:Ljava/lang/String;

    .line 294
    iput-object p1, p0, Lorg/icepdf/core/pobjects/PDate;->hour:Ljava/lang/String;

    .line 295
    iput-object p1, p0, Lorg/icepdf/core/pobjects/PDate;->minute:Ljava/lang/String;

    .line 296
    iput-object p1, p0, Lorg/icepdf/core/pobjects/PDate;->second:Ljava/lang/String;

    .line 297
    iput-object p1, p0, Lorg/icepdf/core/pobjects/PDate;->timeZoneOffset:Ljava/lang/String;

    .line 298
    iput-object p1, p0, Lorg/icepdf/core/pobjects/PDate;->timeZoneHour:Ljava/lang/String;

    .line 299
    iput-object p1, p0, Lorg/icepdf/core/pobjects/PDate;->timeZoneMinute:Ljava/lang/String;

    .line 300
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/icepdf/core/pobjects/PDate;->notStandardFormat:Z

    goto :goto_0
.end method

.method private parseGhostScriptDate(Ljava/lang/String;)V
    .locals 5
    .param p1, "date"    # Ljava/lang/String;

    .prologue
    .line 314
    new-instance v0, Ljava/util/StringTokenizer;

    invoke-direct {v0, p1}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;)V

    .line 316
    .local v0, "dateTime":Ljava/util/StringTokenizer;
    new-instance v1, Ljava/util/StringTokenizer;

    invoke-virtual {v0}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "/"

    invoke-direct {v1, v3, v4}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 319
    .local v1, "dateToken":Ljava/util/StringTokenizer;
    new-instance v2, Ljava/util/StringTokenizer;

    invoke-virtual {v0}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, ":"

    invoke-direct {v2, v3, v4}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 323
    .local v2, "timeToken":Ljava/util/StringTokenizer;
    invoke-virtual {v1}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lorg/icepdf/core/pobjects/PDate;->month:Ljava/lang/String;

    .line 324
    invoke-virtual {v1}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lorg/icepdf/core/pobjects/PDate;->day:Ljava/lang/String;

    .line 325
    invoke-virtual {v1}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lorg/icepdf/core/pobjects/PDate;->year:Ljava/lang/String;

    .line 328
    invoke-virtual {v2}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lorg/icepdf/core/pobjects/PDate;->hour:Ljava/lang/String;

    .line 329
    invoke-virtual {v2}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lorg/icepdf/core/pobjects/PDate;->minute:Ljava/lang/String;

    .line 330
    invoke-virtual {v2}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lorg/icepdf/core/pobjects/PDate;->second:Ljava/lang/String;

    .line 331
    return-void
.end method


# virtual methods
.method public getDay()Ljava/lang/String;
    .locals 1

    .prologue
    .line 155
    iget-object v0, p0, Lorg/icepdf/core/pobjects/PDate;->day:Ljava/lang/String;

    return-object v0
.end method

.method public getHour()Ljava/lang/String;
    .locals 1

    .prologue
    .line 167
    iget-object v0, p0, Lorg/icepdf/core/pobjects/PDate;->hour:Ljava/lang/String;

    return-object v0
.end method

.method public getMinute()Ljava/lang/String;
    .locals 1

    .prologue
    .line 179
    iget-object v0, p0, Lorg/icepdf/core/pobjects/PDate;->minute:Ljava/lang/String;

    return-object v0
.end method

.method public getMonth()Ljava/lang/String;
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, Lorg/icepdf/core/pobjects/PDate;->month:Ljava/lang/String;

    return-object v0
.end method

.method public getSecond()Ljava/lang/String;
    .locals 1

    .prologue
    .line 191
    iget-object v0, p0, Lorg/icepdf/core/pobjects/PDate;->second:Ljava/lang/String;

    return-object v0
.end method

.method public getTimeZoneHour()Ljava/lang/String;
    .locals 1

    .prologue
    .line 203
    iget-object v0, p0, Lorg/icepdf/core/pobjects/PDate;->timeZoneHour:Ljava/lang/String;

    return-object v0
.end method

.method public getTimeZoneMinute()Ljava/lang/String;
    .locals 1

    .prologue
    .line 215
    iget-object v0, p0, Lorg/icepdf/core/pobjects/PDate;->timeZoneMinute:Ljava/lang/String;

    return-object v0
.end method

.method public getTimeZoneOffset()Z
    .locals 2

    .prologue
    .line 228
    const-string/jumbo v0, "-"

    iget-object v1, p0, Lorg/icepdf/core/pobjects/PDate;->timeZoneOffset:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getYear()Ljava/lang/String;
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, Lorg/icepdf/core/pobjects/PDate;->year:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 239
    iget-boolean v1, p0, Lorg/icepdf/core/pobjects/PDate;->notStandardFormat:Z

    if-nez v1, :cond_a

    .line 240
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x28

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 241
    .local v0, "sb":Ljava/lang/StringBuilder;
    iget-object v1, p0, Lorg/icepdf/core/pobjects/PDate;->month:Ljava/lang/String;

    invoke-direct {p0, v1}, Lorg/icepdf/core/pobjects/PDate;->getMonth(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_0

    .line 242
    iget-object v1, p0, Lorg/icepdf/core/pobjects/PDate;->month:Ljava/lang/String;

    invoke-direct {p0, v1}, Lorg/icepdf/core/pobjects/PDate;->getMonth(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 243
    :cond_0
    iget-object v1, p0, Lorg/icepdf/core/pobjects/PDate;->day:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_1

    .line 244
    const-string/jumbo v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/icepdf/core/pobjects/PDate;->day:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 245
    :cond_1
    iget-object v1, p0, Lorg/icepdf/core/pobjects/PDate;->year:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_2

    .line 246
    const-string/jumbo v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/icepdf/core/pobjects/PDate;->year:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 247
    :cond_2
    iget-object v1, p0, Lorg/icepdf/core/pobjects/PDate;->hour:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_3

    .line 248
    const-string/jumbo v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/icepdf/core/pobjects/PDate;->hour:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 249
    :cond_3
    iget-object v1, p0, Lorg/icepdf/core/pobjects/PDate;->minute:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_4

    .line 250
    const-string/jumbo v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/icepdf/core/pobjects/PDate;->minute:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 251
    :cond_4
    iget-object v1, p0, Lorg/icepdf/core/pobjects/PDate;->second:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_5

    .line 252
    const-string/jumbo v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/icepdf/core/pobjects/PDate;->second:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 253
    :cond_5
    iget-object v1, p0, Lorg/icepdf/core/pobjects/PDate;->timeZoneOffset:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_6

    .line 254
    iget-object v1, p0, Lorg/icepdf/core/pobjects/PDate;->timeZoneOffset:Ljava/lang/String;

    const-string/jumbo v2, "Z"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 255
    const-string/jumbo v1, " (UTC)"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 265
    :cond_6
    :goto_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 268
    .end local v0    # "sb":Ljava/lang/StringBuilder;
    :goto_1
    return-object v1

    .line 257
    .restart local v0    # "sb":Ljava/lang/StringBuilder;
    :cond_7
    const-string/jumbo v1, " (UTC "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/icepdf/core/pobjects/PDate;->timeZoneOffset:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 258
    iget-object v1, p0, Lorg/icepdf/core/pobjects/PDate;->timeZoneHour:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_8

    .line 259
    const-string/jumbo v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/icepdf/core/pobjects/PDate;->timeZoneHour:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 260
    :cond_8
    iget-object v1, p0, Lorg/icepdf/core/pobjects/PDate;->timeZoneMinute:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_9

    .line 261
    const-string/jumbo v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/icepdf/core/pobjects/PDate;->timeZoneMinute:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 262
    :cond_9
    const-string/jumbo v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 268
    .end local v0    # "sb":Ljava/lang/StringBuilder;
    :cond_a
    iget-object v1, p0, Lorg/icepdf/core/pobjects/PDate;->day:Ljava/lang/String;

    goto :goto_1
.end method
