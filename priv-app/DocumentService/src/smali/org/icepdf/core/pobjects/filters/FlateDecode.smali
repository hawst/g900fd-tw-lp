.class public Lorg/icepdf/core/pobjects/filters/FlateDecode;
.super Lorg/icepdf/core/pobjects/filters/ChunkingInputStream;
.source "FlateDecode.java"


# static fields
.field private static final LZW_FLATE_PREDICTOR_NONE:I = 0x1

.field private static final LZW_FLATE_PREDICTOR_PNG_AVG:I = 0xd

.field private static final LZW_FLATE_PREDICTOR_PNG_NONE:I = 0xa

.field private static final LZW_FLATE_PREDICTOR_PNG_OPTIMUM:I = 0xf

.field private static final LZW_FLATE_PREDICTOR_PNG_PAETH:I = 0xe

.field private static final LZW_FLATE_PREDICTOR_PNG_SUB:I = 0xb

.field private static final LZW_FLATE_PREDICTOR_PNG_UP:I = 0xc

.field private static final LZW_FLATE_PREDICTOR_TIFF_2:I = 0x2


# instance fields
.field private aboveBuffer:[B

.field private bitsPerComponent:I

.field private bpp:I

.field private numComponents:I

.field private originalInputKeptSolelyForDebugging:Ljava/io/InputStream;

.field private predictor:I

.field private width:I


# direct methods
.method public constructor <init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;Ljava/io/InputStream;)V
    .locals 8
    .param p1, "library"    # Lorg/icepdf/core/util/Library;
    .param p2, "props"    # Ljava/util/Hashtable;
    .param p3, "input"    # Ljava/io/InputStream;

    .prologue
    const/4 v5, 0x0

    const/4 v7, 0x1

    .line 82
    invoke-direct {p0}, Lorg/icepdf/core/pobjects/filters/ChunkingInputStream;-><init>()V

    .line 76
    iput v7, p0, Lorg/icepdf/core/pobjects/filters/FlateDecode;->bpp:I

    .line 83
    iput-object p3, p0, Lorg/icepdf/core/pobjects/filters/FlateDecode;->originalInputKeptSolelyForDebugging:Ljava/io/InputStream;

    .line 84
    iput v5, p0, Lorg/icepdf/core/pobjects/filters/FlateDecode;->width:I

    .line 85
    iput v5, p0, Lorg/icepdf/core/pobjects/filters/FlateDecode;->numComponents:I

    .line 86
    iput v5, p0, Lorg/icepdf/core/pobjects/filters/FlateDecode;->bitsPerComponent:I

    .line 87
    iput v7, p0, Lorg/icepdf/core/pobjects/filters/FlateDecode;->bpp:I

    .line 89
    const/16 v2, 0x1000

    .line 92
    .local v2, "intermediateBufferSize":I
    const-string/jumbo v5, "DecodeParms"

    invoke-virtual {p1, p2, v5}, Lorg/icepdf/core/util/Library;->getDictionary(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/util/Hashtable;

    move-result-object v1

    .line 93
    .local v1, "decodeParmsDictionary":Ljava/util/Hashtable;
    const-string/jumbo v5, "Predictor"

    invoke-virtual {p1, v1, v5}, Lorg/icepdf/core/util/Library;->getInt(Ljava/util/Hashtable;Ljava/lang/String;)I

    move-result v5

    iput v5, p0, Lorg/icepdf/core/pobjects/filters/FlateDecode;->predictor:I

    .line 94
    iget v5, p0, Lorg/icepdf/core/pobjects/filters/FlateDecode;->predictor:I

    if-eq v5, v7, :cond_0

    iget v5, p0, Lorg/icepdf/core/pobjects/filters/FlateDecode;->predictor:I

    const/4 v6, 0x2

    if-eq v5, v6, :cond_0

    iget v5, p0, Lorg/icepdf/core/pobjects/filters/FlateDecode;->predictor:I

    const/16 v6, 0xa

    if-eq v5, v6, :cond_0

    iget v5, p0, Lorg/icepdf/core/pobjects/filters/FlateDecode;->predictor:I

    const/16 v6, 0xb

    if-eq v5, v6, :cond_0

    iget v5, p0, Lorg/icepdf/core/pobjects/filters/FlateDecode;->predictor:I

    const/16 v6, 0xc

    if-eq v5, v6, :cond_0

    iget v5, p0, Lorg/icepdf/core/pobjects/filters/FlateDecode;->predictor:I

    const/16 v6, 0xd

    if-eq v5, v6, :cond_0

    iget v5, p0, Lorg/icepdf/core/pobjects/filters/FlateDecode;->predictor:I

    const/16 v6, 0xe

    if-eq v5, v6, :cond_0

    iget v5, p0, Lorg/icepdf/core/pobjects/filters/FlateDecode;->predictor:I

    const/16 v6, 0xf

    if-eq v5, v6, :cond_0

    .line 98
    iput v7, p0, Lorg/icepdf/core/pobjects/filters/FlateDecode;->predictor:I

    .line 101
    :cond_0
    iget v5, p0, Lorg/icepdf/core/pobjects/filters/FlateDecode;->predictor:I

    if-eq v5, v7, :cond_3

    .line 102
    const-string/jumbo v5, "Width"

    invoke-virtual {p1, p2, v5}, Lorg/icepdf/core/util/Library;->getNumber(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Number;

    move-result-object v4

    .line 103
    .local v4, "widthNumber":Ljava/lang/Number;
    if-eqz v4, :cond_4

    .line 104
    invoke-virtual {v4}, Ljava/lang/Number;->intValue()I

    move-result v5

    iput v5, p0, Lorg/icepdf/core/pobjects/filters/FlateDecode;->width:I

    .line 115
    :goto_0
    iput v7, p0, Lorg/icepdf/core/pobjects/filters/FlateDecode;->numComponents:I

    .line 116
    const/16 v5, 0x8

    iput v5, p0, Lorg/icepdf/core/pobjects/filters/FlateDecode;->bitsPerComponent:I

    .line 118
    const-string/jumbo v5, "Colors"

    invoke-virtual {p1, v1, v5}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    .line 119
    .local v3, "numComponentsDecodeParmsObj":Ljava/lang/Object;
    instance-of v5, v3, Ljava/lang/Number;

    if-eqz v5, :cond_1

    .line 120
    check-cast v3, Ljava/lang/Number;

    .end local v3    # "numComponentsDecodeParmsObj":Ljava/lang/Object;
    invoke-virtual {v3}, Ljava/lang/Number;->intValue()I

    move-result v5

    iput v5, p0, Lorg/icepdf/core/pobjects/filters/FlateDecode;->numComponents:I

    .line 123
    :cond_1
    const-string/jumbo v5, "BitsPerComponent"

    invoke-virtual {p1, v1, v5}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 124
    .local v0, "bitsPerComponentDecodeParmsObj":Ljava/lang/Object;
    instance-of v5, v0, Ljava/lang/Number;

    if-eqz v5, :cond_2

    .line 125
    check-cast v0, Ljava/lang/Number;

    .end local v0    # "bitsPerComponentDecodeParmsObj":Ljava/lang/Object;
    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v5

    iput v5, p0, Lorg/icepdf/core/pobjects/filters/FlateDecode;->bitsPerComponent:I

    .line 129
    :cond_2
    iget v5, p0, Lorg/icepdf/core/pobjects/filters/FlateDecode;->numComponents:I

    iget v6, p0, Lorg/icepdf/core/pobjects/filters/FlateDecode;->bitsPerComponent:I

    mul-int/2addr v5, v6

    invoke-static {v5}, Lorg/icepdf/core/util/Utils;->numBytesToHoldBits(I)I

    move-result v5

    invoke-static {v7, v5}, Ljava/lang/Math;->max(II)I

    move-result v5

    iput v5, p0, Lorg/icepdf/core/pobjects/filters/FlateDecode;->bpp:I

    .line 133
    iget v5, p0, Lorg/icepdf/core/pobjects/filters/FlateDecode;->width:I

    iget v6, p0, Lorg/icepdf/core/pobjects/filters/FlateDecode;->numComponents:I

    mul-int/2addr v5, v6

    iget v6, p0, Lorg/icepdf/core/pobjects/filters/FlateDecode;->bitsPerComponent:I

    mul-int/2addr v5, v6

    invoke-static {v5}, Lorg/icepdf/core/util/Utils;->numBytesToHoldBits(I)I

    move-result v2

    .line 139
    .end local v4    # "widthNumber":Ljava/lang/Number;
    :cond_3
    new-instance v5, Ljava/util/zip/InflaterInputStream;

    invoke-direct {v5, p3}, Ljava/util/zip/InflaterInputStream;-><init>(Ljava/io/InputStream;)V

    invoke-virtual {p0, v5}, Lorg/icepdf/core/pobjects/filters/FlateDecode;->setInputStream(Ljava/io/InputStream;)V

    .line 140
    invoke-virtual {p0, v2}, Lorg/icepdf/core/pobjects/filters/FlateDecode;->setBufferSize(I)V

    .line 141
    new-array v5, v2, [B

    iput-object v5, p0, Lorg/icepdf/core/pobjects/filters/FlateDecode;->aboveBuffer:[B

    .line 142
    return-void

    .line 106
    .restart local v4    # "widthNumber":Ljava/lang/Number;
    :cond_4
    const-string/jumbo v5, "Columns"

    invoke-virtual {p1, v1, v5}, Lorg/icepdf/core/util/Library;->getInt(Ljava/util/Hashtable;Ljava/lang/String;)I

    move-result v5

    iput v5, p0, Lorg/icepdf/core/pobjects/filters/FlateDecode;->width:I

    goto :goto_0
.end method


# virtual methods
.method protected fillInternalBuffer()I
    .locals 22
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 146
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/core/pobjects/filters/FlateDecode;->aboveBuffer:[B

    move-object/from16 v17, v0

    .line 147
    .local v17, "temp":[B
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/core/pobjects/filters/FlateDecode;->buffer:[B

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/icepdf/core/pobjects/filters/FlateDecode;->aboveBuffer:[B

    .line 148
    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/icepdf/core/pobjects/filters/FlateDecode;->buffer:[B

    .line 153
    move-object/from16 v0, p0

    iget v0, v0, Lorg/icepdf/core/pobjects/filters/FlateDecode;->predictor:I

    move/from16 v18, v0

    const/16 v19, 0x1

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_1

    .line 154
    invoke-virtual/range {p0 .. p0}, Lorg/icepdf/core/pobjects/filters/FlateDecode;->fillBufferFromInputStream()I

    move-result v9

    .line 155
    .local v9, "numRead":I
    if-gtz v9, :cond_0

    .line 156
    const/4 v9, -0x1

    .line 262
    .end local v9    # "numRead":I
    :cond_0
    :goto_0
    return v9

    .line 158
    :cond_1
    move-object/from16 v0, p0

    iget v0, v0, Lorg/icepdf/core/pobjects/filters/FlateDecode;->predictor:I

    move/from16 v18, v0

    const/16 v19, 0x2

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_4

    .line 159
    invoke-virtual/range {p0 .. p0}, Lorg/icepdf/core/pobjects/filters/FlateDecode;->fillBufferFromInputStream()I

    move-result v9

    .line 160
    .restart local v9    # "numRead":I
    if-gtz v9, :cond_2

    .line 161
    const/4 v9, -0x1

    goto :goto_0

    .line 162
    :cond_2
    move-object/from16 v0, p0

    iget v0, v0, Lorg/icepdf/core/pobjects/filters/FlateDecode;->bitsPerComponent:I

    move/from16 v18, v0

    const/16 v19, 0x8

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_0

    .line 163
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_1
    if-ge v7, v9, :cond_0

    .line 164
    move-object/from16 v0, p0

    iget v0, v0, Lorg/icepdf/core/pobjects/filters/FlateDecode;->numComponents:I

    move/from16 v18, v0

    sub-int v15, v7, v18

    .line 165
    .local v15, "prevIndex":I
    if-ltz v15, :cond_3

    .line 166
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/core/pobjects/filters/FlateDecode;->buffer:[B

    move-object/from16 v18, v0

    aget-byte v19, v18, v7

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/core/pobjects/filters/FlateDecode;->buffer:[B

    move-object/from16 v20, v0

    aget-byte v20, v20, v15

    add-int v19, v19, v20

    move/from16 v0, v19

    int-to-byte v0, v0

    move/from16 v19, v0

    aput-byte v19, v18, v7

    .line 163
    :cond_3
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 175
    .end local v7    # "i":I
    .end local v9    # "numRead":I
    .end local v15    # "prevIndex":I
    :cond_4
    move-object/from16 v0, p0

    iget v0, v0, Lorg/icepdf/core/pobjects/filters/FlateDecode;->predictor:I

    move/from16 v18, v0

    const/16 v19, 0xa

    move/from16 v0, v18

    move/from16 v1, v19

    if-lt v0, v1, :cond_12

    move-object/from16 v0, p0

    iget v0, v0, Lorg/icepdf/core/pobjects/filters/FlateDecode;->predictor:I

    move/from16 v18, v0

    const/16 v19, 0xf

    move/from16 v0, v18

    move/from16 v1, v19

    if-gt v0, v1, :cond_12

    .line 176
    move-object/from16 v0, p0

    iget v6, v0, Lorg/icepdf/core/pobjects/filters/FlateDecode;->predictor:I

    .line 177
    .local v6, "currPredictor":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/core/pobjects/filters/FlateDecode;->in:Ljava/io/InputStream;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Ljava/io/InputStream;->read()I

    move-result v5

    .line 179
    .local v5, "cp":I
    if-gez v5, :cond_5

    .line 180
    const/4 v9, -0x1

    goto :goto_0

    .line 185
    :cond_5
    add-int/lit8 v6, v5, 0xa

    .line 189
    invoke-virtual/range {p0 .. p0}, Lorg/icepdf/core/pobjects/filters/FlateDecode;->fillBufferFromInputStream()I

    move-result v9

    .line 190
    .restart local v9    # "numRead":I
    if-gtz v9, :cond_6

    .line 191
    const/4 v9, -0x1

    goto/16 :goto_0

    .line 194
    :cond_6
    const/4 v7, 0x0

    .restart local v7    # "i":I
    :goto_2
    if-ge v7, v9, :cond_0

    .line 197
    const/16 v18, 0xa

    move/from16 v0, v18

    if-eq v6, v0, :cond_0

    .line 200
    const/16 v18, 0xb

    move/from16 v0, v18

    if-ne v6, v0, :cond_8

    .line 201
    move-object/from16 v0, p0

    iget v0, v0, Lorg/icepdf/core/pobjects/filters/FlateDecode;->bpp:I

    move/from16 v18, v0

    sub-int v18, v7, v18

    if-ltz v18, :cond_7

    .line 202
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/core/pobjects/filters/FlateDecode;->buffer:[B

    move-object/from16 v18, v0

    aget-byte v19, v18, v7

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/core/pobjects/filters/FlateDecode;->buffer:[B

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/icepdf/core/pobjects/filters/FlateDecode;->bpp:I

    move/from16 v21, v0

    sub-int v21, v7, v21

    aget-byte v20, v20, v21

    add-int v19, v19, v20

    move/from16 v0, v19

    int-to-byte v0, v0

    move/from16 v19, v0

    aput-byte v19, v18, v7

    .line 194
    :cond_7
    :goto_3
    add-int/lit8 v7, v7, 0x1

    goto :goto_2

    .line 206
    :cond_8
    const/16 v18, 0xc

    move/from16 v0, v18

    if-ne v6, v0, :cond_9

    .line 207
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/core/pobjects/filters/FlateDecode;->aboveBuffer:[B

    move-object/from16 v18, v0

    if-eqz v18, :cond_7

    .line 208
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/core/pobjects/filters/FlateDecode;->buffer:[B

    move-object/from16 v18, v0

    aget-byte v19, v18, v7

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/core/pobjects/filters/FlateDecode;->aboveBuffer:[B

    move-object/from16 v20, v0

    aget-byte v20, v20, v7

    add-int v19, v19, v20

    move/from16 v0, v19

    int-to-byte v0, v0

    move/from16 v19, v0

    aput-byte v19, v18, v7

    goto :goto_3

    .line 211
    :cond_9
    const/16 v18, 0xd

    move/from16 v0, v18

    if-ne v6, v0, :cond_c

    .line 214
    const/4 v8, 0x0

    .line 215
    .local v8, "left":I
    move-object/from16 v0, p0

    iget v0, v0, Lorg/icepdf/core/pobjects/filters/FlateDecode;->bpp:I

    move/from16 v18, v0

    sub-int v18, v7, v18

    if-ltz v18, :cond_a

    .line 216
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/core/pobjects/filters/FlateDecode;->buffer:[B

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/icepdf/core/pobjects/filters/FlateDecode;->bpp:I

    move/from16 v19, v0

    sub-int v19, v7, v19

    aget-byte v18, v18, v19

    move/from16 v0, v18

    and-int/lit16 v8, v0, 0xff

    .line 217
    :cond_a
    const/4 v2, 0x0

    .line 218
    .local v2, "above":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/core/pobjects/filters/FlateDecode;->aboveBuffer:[B

    move-object/from16 v18, v0

    if-eqz v18, :cond_b

    .line 219
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/core/pobjects/filters/FlateDecode;->aboveBuffer:[B

    move-object/from16 v18, v0

    aget-byte v18, v18, v7

    move/from16 v0, v18

    and-int/lit16 v2, v0, 0xff

    .line 220
    :cond_b
    add-int v16, v8, v2

    .line 221
    .local v16, "sum":I
    ushr-int/lit8 v18, v16, 0x1

    move/from16 v0, v18

    and-int/lit16 v0, v0, 0xff

    move/from16 v18, v0

    move/from16 v0, v18

    int-to-byte v4, v0

    .line 222
    .local v4, "avg":B
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/core/pobjects/filters/FlateDecode;->buffer:[B

    move-object/from16 v18, v0

    aget-byte v19, v18, v7

    add-int v19, v19, v4

    move/from16 v0, v19

    int-to-byte v0, v0

    move/from16 v19, v0

    aput-byte v19, v18, v7

    goto/16 :goto_3

    .line 226
    .end local v2    # "above":I
    .end local v4    # "avg":B
    .end local v8    # "left":I
    .end local v16    # "sum":I
    :cond_c
    const/16 v18, 0xe

    move/from16 v0, v18

    if-ne v6, v0, :cond_7

    .line 237
    const/4 v8, 0x0

    .line 238
    .restart local v8    # "left":I
    move-object/from16 v0, p0

    iget v0, v0, Lorg/icepdf/core/pobjects/filters/FlateDecode;->bpp:I

    move/from16 v18, v0

    sub-int v18, v7, v18

    if-ltz v18, :cond_d

    .line 239
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/core/pobjects/filters/FlateDecode;->buffer:[B

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/icepdf/core/pobjects/filters/FlateDecode;->bpp:I

    move/from16 v19, v0

    sub-int v19, v7, v19

    aget-byte v18, v18, v19

    move/from16 v0, v18

    and-int/lit16 v8, v0, 0xff

    .line 240
    :cond_d
    const/4 v2, 0x0

    .line 241
    .restart local v2    # "above":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/core/pobjects/filters/FlateDecode;->aboveBuffer:[B

    move-object/from16 v18, v0

    if-eqz v18, :cond_e

    .line 242
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/core/pobjects/filters/FlateDecode;->aboveBuffer:[B

    move-object/from16 v18, v0

    aget-byte v18, v18, v7

    move/from16 v0, v18

    and-int/lit16 v2, v0, 0xff

    .line 243
    :cond_e
    const/4 v3, 0x0

    .line 244
    .local v3, "aboveLeft":I
    move-object/from16 v0, p0

    iget v0, v0, Lorg/icepdf/core/pobjects/filters/FlateDecode;->bpp:I

    move/from16 v18, v0

    sub-int v18, v7, v18

    if-ltz v18, :cond_f

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/core/pobjects/filters/FlateDecode;->aboveBuffer:[B

    move-object/from16 v18, v0

    if-eqz v18, :cond_f

    .line 245
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/core/pobjects/filters/FlateDecode;->aboveBuffer:[B

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/icepdf/core/pobjects/filters/FlateDecode;->bpp:I

    move/from16 v19, v0

    sub-int v19, v7, v19

    aget-byte v18, v18, v19

    move/from16 v0, v18

    and-int/lit16 v3, v0, 0xff

    .line 246
    :cond_f
    add-int v18, v8, v2

    sub-int v10, v18, v3

    .line 247
    .local v10, "p":I
    sub-int v18, v10, v8

    invoke-static/range {v18 .. v18}, Ljava/lang/Math;->abs(I)I

    move-result v13

    .line 248
    .local v13, "pLeft":I
    sub-int v18, v10, v2

    invoke-static/range {v18 .. v18}, Ljava/lang/Math;->abs(I)I

    move-result v11

    .line 249
    .local v11, "pAbove":I
    sub-int v18, v10, v3

    invoke-static/range {v18 .. v18}, Ljava/lang/Math;->abs(I)I

    move-result v12

    .line 250
    .local v12, "pAboveLeft":I
    if-gt v13, v11, :cond_10

    if-gt v13, v12, :cond_10

    move v14, v8

    .line 255
    .local v14, "paeth":I
    :goto_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/core/pobjects/filters/FlateDecode;->buffer:[B

    move-object/from16 v18, v0

    aget-byte v19, v18, v7

    and-int/lit16 v0, v14, 0xff

    move/from16 v20, v0

    move/from16 v0, v20

    int-to-byte v0, v0

    move/from16 v20, v0

    add-int v19, v19, v20

    move/from16 v0, v19

    int-to-byte v0, v0

    move/from16 v19, v0

    aput-byte v19, v18, v7

    goto/16 :goto_3

    .line 250
    .end local v14    # "paeth":I
    :cond_10
    if-gt v11, v12, :cond_11

    move v14, v2

    goto :goto_4

    :cond_11
    move v14, v3

    goto :goto_4

    .line 262
    .end local v2    # "above":I
    .end local v3    # "aboveLeft":I
    .end local v5    # "cp":I
    .end local v6    # "currPredictor":I
    .end local v7    # "i":I
    .end local v8    # "left":I
    .end local v9    # "numRead":I
    .end local v10    # "p":I
    .end local v11    # "pAbove":I
    .end local v12    # "pAboveLeft":I
    .end local v13    # "pLeft":I
    :cond_12
    const/4 v9, -0x1

    goto/16 :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 267
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 268
    .local v0, "sb":Ljava/lang/StringBuilder;
    invoke-super {p0}, Lorg/icepdf/core/pobjects/filters/ChunkingInputStream;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 269
    const-string/jumbo v1, ", orig: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 270
    iget-object v1, p0, Lorg/icepdf/core/pobjects/filters/FlateDecode;->originalInputKeptSolelyForDebugging:Ljava/io/InputStream;

    if-nez v1, :cond_0

    .line 271
    const-string/jumbo v1, "null"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 274
    :goto_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 273
    :cond_0
    iget-object v1, p0, Lorg/icepdf/core/pobjects/filters/FlateDecode;->originalInputKeptSolelyForDebugging:Ljava/io/InputStream;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method
