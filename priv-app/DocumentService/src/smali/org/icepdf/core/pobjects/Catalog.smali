.class public Lorg/icepdf/core/pobjects/Catalog;
.super Lorg/icepdf/core/pobjects/Dictionary;
.source "Catalog.java"


# static fields
.field private static final logger:Ljava/util/logging/Logger;


# instance fields
.field private dests:Lorg/icepdf/core/pobjects/Dictionary;

.field private destsInited:Z

.field private nameTree:Lorg/icepdf/core/pobjects/NameTree;

.field private namesTreeInited:Z

.field private outlines:Lorg/icepdf/core/pobjects/Outlines;

.field private outlinesInited:Z

.field private pageTree:Lorg/icepdf/core/pobjects/PageTree;

.field private viewerPref:Lorg/icepdf/core/pobjects/ViewerPreferences;

.field private viewerPrefInited:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 44
    const-class v0, Lorg/icepdf/core/pobjects/Catalog;

    invoke-virtual {v0}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Lorg/icepdf/core/pobjects/Catalog;->logger:Ljava/util/logging/Logger;

    .line 60
    sget-object v0, Lorg/icepdf/core/pobjects/Catalog;->logger:Ljava/util/logging/Logger;

    sget-object v1, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    invoke-virtual {v0, v1}, Ljava/util/logging/Logger;->isLoggable(Ljava/util/logging/Level;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 61
    sget-object v0, Lorg/icepdf/core/pobjects/Catalog;->logger:Ljava/util/logging/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "ICEsoft ICEpdf Core "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lorg/icepdf/core/pobjects/Document;->getLibraryVersion()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    .line 63
    :cond_0
    return-void
.end method

.method public constructor <init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V
    .locals 1
    .param p1, "l"    # Lorg/icepdf/core/util/Library;
    .param p2, "h"    # Ljava/util/Hashtable;

    .prologue
    const/4 v0, 0x0

    .line 72
    invoke-direct {p0, p1, p2}, Lorg/icepdf/core/pobjects/Dictionary;-><init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V

    .line 53
    iput-boolean v0, p0, Lorg/icepdf/core/pobjects/Catalog;->outlinesInited:Z

    .line 54
    iput-boolean v0, p0, Lorg/icepdf/core/pobjects/Catalog;->namesTreeInited:Z

    .line 55
    iput-boolean v0, p0, Lorg/icepdf/core/pobjects/Catalog;->destsInited:Z

    .line 56
    iput-boolean v0, p0, Lorg/icepdf/core/pobjects/Catalog;->viewerPrefInited:Z

    .line 73
    return-void
.end method


# virtual methods
.method public dispose(Z)V
    .locals 2
    .param p1, "cache"    # Z

    .prologue
    const/4 v1, 0x0

    .line 106
    iget-object v0, p0, Lorg/icepdf/core/pobjects/Catalog;->nameTree:Lorg/icepdf/core/pobjects/NameTree;

    if-eqz v0, :cond_0

    .line 107
    iget-object v0, p0, Lorg/icepdf/core/pobjects/Catalog;->nameTree:Lorg/icepdf/core/pobjects/NameTree;

    invoke-virtual {v0}, Lorg/icepdf/core/pobjects/NameTree;->dispose()V

    .line 108
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/icepdf/core/pobjects/Catalog;->namesTreeInited:Z

    .line 109
    if-nez p1, :cond_0

    .line 110
    iput-object v1, p0, Lorg/icepdf/core/pobjects/Catalog;->nameTree:Lorg/icepdf/core/pobjects/NameTree;

    .line 113
    :cond_0
    iget-object v0, p0, Lorg/icepdf/core/pobjects/Catalog;->pageTree:Lorg/icepdf/core/pobjects/PageTree;

    if-eqz v0, :cond_1

    .line 114
    iget-object v0, p0, Lorg/icepdf/core/pobjects/Catalog;->pageTree:Lorg/icepdf/core/pobjects/PageTree;

    invoke-virtual {v0, p1}, Lorg/icepdf/core/pobjects/PageTree;->dispose(Z)V

    .line 115
    if-nez p1, :cond_1

    .line 116
    iput-object v1, p0, Lorg/icepdf/core/pobjects/Catalog;->pageTree:Lorg/icepdf/core/pobjects/PageTree;

    .line 118
    :cond_1
    iget-object v0, p0, Lorg/icepdf/core/pobjects/Catalog;->outlines:Lorg/icepdf/core/pobjects/Outlines;

    if-eqz v0, :cond_2

    .line 119
    if-nez p1, :cond_2

    .line 120
    iget-object v0, p0, Lorg/icepdf/core/pobjects/Catalog;->outlines:Lorg/icepdf/core/pobjects/Outlines;

    invoke-virtual {v0}, Lorg/icepdf/core/pobjects/Outlines;->dispose()V

    .line 121
    iput-object v1, p0, Lorg/icepdf/core/pobjects/Catalog;->outlines:Lorg/icepdf/core/pobjects/Outlines;

    .line 124
    :cond_2
    return-void
.end method

.method public getDestinations()Lorg/icepdf/core/pobjects/Dictionary;
    .locals 4

    .prologue
    .line 184
    iget-boolean v1, p0, Lorg/icepdf/core/pobjects/Catalog;->destsInited:Z

    if-nez v1, :cond_0

    .line 185
    const/4 v1, 0x1

    iput-boolean v1, p0, Lorg/icepdf/core/pobjects/Catalog;->destsInited:Z

    .line 186
    iget-object v1, p0, Lorg/icepdf/core/pobjects/Catalog;->library:Lorg/icepdf/core/util/Library;

    iget-object v2, p0, Lorg/icepdf/core/pobjects/Catalog;->entries:Ljava/util/Hashtable;

    const-string/jumbo v3, "Dests"

    invoke-virtual {v1, v2, v3}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 187
    .local v0, "o":Ljava/lang/Object;
    if-eqz v0, :cond_0

    .line 188
    new-instance v1, Lorg/icepdf/core/pobjects/Dictionary;

    iget-object v2, p0, Lorg/icepdf/core/pobjects/Catalog;->library:Lorg/icepdf/core/util/Library;

    check-cast v0, Ljava/util/Hashtable;

    .end local v0    # "o":Ljava/lang/Object;
    invoke-direct {v1, v2, v0}, Lorg/icepdf/core/pobjects/Dictionary;-><init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V

    iput-object v1, p0, Lorg/icepdf/core/pobjects/Catalog;->dests:Lorg/icepdf/core/pobjects/Dictionary;

    .line 189
    iget-object v1, p0, Lorg/icepdf/core/pobjects/Catalog;->dests:Lorg/icepdf/core/pobjects/Dictionary;

    invoke-virtual {v1}, Lorg/icepdf/core/pobjects/Dictionary;->init()V

    .line 192
    :cond_0
    iget-object v1, p0, Lorg/icepdf/core/pobjects/Catalog;->dests:Lorg/icepdf/core/pobjects/Dictionary;

    return-object v1
.end method

.method public getNameTree()Lorg/icepdf/core/pobjects/NameTree;
    .locals 6

    .prologue
    .line 163
    iget-boolean v3, p0, Lorg/icepdf/core/pobjects/Catalog;->namesTreeInited:Z

    if-nez v3, :cond_0

    .line 164
    const/4 v3, 0x1

    iput-boolean v3, p0, Lorg/icepdf/core/pobjects/Catalog;->namesTreeInited:Z

    .line 165
    iget-object v3, p0, Lorg/icepdf/core/pobjects/Catalog;->library:Lorg/icepdf/core/util/Library;

    iget-object v4, p0, Lorg/icepdf/core/pobjects/Catalog;->entries:Ljava/util/Hashtable;

    const-string/jumbo v5, "Names"

    invoke-virtual {v3, v4, v5}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    .line 166
    .local v2, "o":Ljava/lang/Object;
    if-eqz v2, :cond_0

    instance-of v3, v2, Ljava/util/Hashtable;

    if-eqz v3, :cond_0

    move-object v0, v2

    .line 167
    check-cast v0, Ljava/util/Hashtable;

    .line 168
    .local v0, "dest":Ljava/util/Hashtable;
    iget-object v3, p0, Lorg/icepdf/core/pobjects/Catalog;->library:Lorg/icepdf/core/util/Library;

    const-string/jumbo v4, "Dests"

    invoke-virtual {v3, v0, v4}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    .line 169
    .local v1, "names":Ljava/lang/Object;
    if-eqz v1, :cond_0

    instance-of v3, v1, Ljava/util/Hashtable;

    if-eqz v3, :cond_0

    .line 170
    new-instance v3, Lorg/icepdf/core/pobjects/NameTree;

    iget-object v4, p0, Lorg/icepdf/core/pobjects/Catalog;->library:Lorg/icepdf/core/util/Library;

    check-cast v1, Ljava/util/Hashtable;

    .end local v1    # "names":Ljava/lang/Object;
    invoke-direct {v3, v4, v1}, Lorg/icepdf/core/pobjects/NameTree;-><init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V

    iput-object v3, p0, Lorg/icepdf/core/pobjects/Catalog;->nameTree:Lorg/icepdf/core/pobjects/NameTree;

    .line 171
    iget-object v3, p0, Lorg/icepdf/core/pobjects/Catalog;->nameTree:Lorg/icepdf/core/pobjects/NameTree;

    invoke-virtual {v3}, Lorg/icepdf/core/pobjects/NameTree;->init()V

    .line 175
    .end local v0    # "dest":Ljava/util/Hashtable;
    .end local v2    # "o":Ljava/lang/Object;
    :cond_0
    iget-object v3, p0, Lorg/icepdf/core/pobjects/Catalog;->nameTree:Lorg/icepdf/core/pobjects/NameTree;

    return-object v3
.end method

.method public getOutlines()Lorg/icepdf/core/pobjects/Outlines;
    .locals 4

    .prologue
    .line 145
    iget-boolean v1, p0, Lorg/icepdf/core/pobjects/Catalog;->outlinesInited:Z

    if-nez v1, :cond_0

    .line 146
    const/4 v1, 0x1

    iput-boolean v1, p0, Lorg/icepdf/core/pobjects/Catalog;->outlinesInited:Z

    .line 147
    iget-object v1, p0, Lorg/icepdf/core/pobjects/Catalog;->library:Lorg/icepdf/core/util/Library;

    iget-object v2, p0, Lorg/icepdf/core/pobjects/Catalog;->entries:Ljava/util/Hashtable;

    const-string/jumbo v3, "Outlines"

    invoke-virtual {v1, v2, v3}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 148
    .local v0, "o":Ljava/lang/Object;
    if-eqz v0, :cond_0

    .line 149
    new-instance v1, Lorg/icepdf/core/pobjects/Outlines;

    iget-object v2, p0, Lorg/icepdf/core/pobjects/Catalog;->library:Lorg/icepdf/core/util/Library;

    check-cast v0, Ljava/util/Hashtable;

    .end local v0    # "o":Ljava/lang/Object;
    invoke-direct {v1, v2, v0}, Lorg/icepdf/core/pobjects/Outlines;-><init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V

    iput-object v1, p0, Lorg/icepdf/core/pobjects/Catalog;->outlines:Lorg/icepdf/core/pobjects/Outlines;

    .line 151
    :cond_0
    iget-object v1, p0, Lorg/icepdf/core/pobjects/Catalog;->outlines:Lorg/icepdf/core/pobjects/Outlines;

    return-object v1
.end method

.method public getPageTree()Lorg/icepdf/core/pobjects/PageTree;
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lorg/icepdf/core/pobjects/Catalog;->pageTree:Lorg/icepdf/core/pobjects/PageTree;

    return-object v0
.end method

.method public getViewerPreferences()Lorg/icepdf/core/pobjects/ViewerPreferences;
    .locals 4

    .prologue
    .line 203
    iget-boolean v1, p0, Lorg/icepdf/core/pobjects/Catalog;->viewerPrefInited:Z

    if-nez v1, :cond_0

    .line 204
    const/4 v1, 0x1

    iput-boolean v1, p0, Lorg/icepdf/core/pobjects/Catalog;->viewerPrefInited:Z

    .line 205
    iget-object v1, p0, Lorg/icepdf/core/pobjects/Catalog;->library:Lorg/icepdf/core/util/Library;

    iget-object v2, p0, Lorg/icepdf/core/pobjects/Catalog;->entries:Ljava/util/Hashtable;

    const-string/jumbo v3, "ViewerPreferences"

    invoke-virtual {v1, v2, v3}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 206
    .local v0, "o":Ljava/lang/Object;
    if-eqz v0, :cond_0

    .line 207
    new-instance v1, Lorg/icepdf/core/pobjects/ViewerPreferences;

    iget-object v2, p0, Lorg/icepdf/core/pobjects/Catalog;->library:Lorg/icepdf/core/util/Library;

    check-cast v0, Ljava/util/Hashtable;

    .end local v0    # "o":Ljava/lang/Object;
    invoke-direct {v1, v2, v0}, Lorg/icepdf/core/pobjects/ViewerPreferences;-><init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V

    iput-object v1, p0, Lorg/icepdf/core/pobjects/Catalog;->viewerPref:Lorg/icepdf/core/pobjects/ViewerPreferences;

    .line 208
    iget-object v1, p0, Lorg/icepdf/core/pobjects/Catalog;->viewerPref:Lorg/icepdf/core/pobjects/ViewerPreferences;

    invoke-virtual {v1}, Lorg/icepdf/core/pobjects/ViewerPreferences;->init()V

    .line 211
    :cond_0
    iget-object v1, p0, Lorg/icepdf/core/pobjects/Catalog;->viewerPref:Lorg/icepdf/core/pobjects/ViewerPreferences;

    return-object v1
.end method

.method public declared-synchronized init()V
    .locals 4

    .prologue
    .line 79
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lorg/icepdf/core/pobjects/Catalog;->library:Lorg/icepdf/core/util/Library;

    iget-object v2, p0, Lorg/icepdf/core/pobjects/Catalog;->entries:Ljava/util/Hashtable;

    const-string/jumbo v3, "Pages"

    invoke-virtual {v1, v2, v3}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 80
    .local v0, "tmp":Ljava/lang/Object;
    const/4 v1, 0x0

    iput-object v1, p0, Lorg/icepdf/core/pobjects/Catalog;->pageTree:Lorg/icepdf/core/pobjects/PageTree;

    .line 81
    instance-of v1, v0, Lorg/icepdf/core/pobjects/PageTree;

    if-eqz v1, :cond_1

    .line 82
    check-cast v0, Lorg/icepdf/core/pobjects/PageTree;

    .end local v0    # "tmp":Ljava/lang/Object;
    iput-object v0, p0, Lorg/icepdf/core/pobjects/Catalog;->pageTree:Lorg/icepdf/core/pobjects/PageTree;

    .line 90
    :cond_0
    :goto_0
    iget-object v1, p0, Lorg/icepdf/core/pobjects/Catalog;->pageTree:Lorg/icepdf/core/pobjects/PageTree;

    if-eqz v1, :cond_2

    .line 91
    iget-object v1, p0, Lorg/icepdf/core/pobjects/Catalog;->pageTree:Lorg/icepdf/core/pobjects/PageTree;

    invoke-virtual {v1}, Lorg/icepdf/core/pobjects/PageTree;->init()V

    .line 92
    iget-object v1, p0, Lorg/icepdf/core/pobjects/Catalog;->pageTree:Lorg/icepdf/core/pobjects/PageTree;

    invoke-virtual {v1}, Lorg/icepdf/core/pobjects/PageTree;->initRootPageTree()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 96
    :goto_1
    monitor-exit p0

    return-void

    .line 86
    .restart local v0    # "tmp":Ljava/lang/Object;
    :cond_1
    :try_start_1
    instance-of v1, v0, Ljava/util/Hashtable;

    if-eqz v1, :cond_0

    .line 87
    new-instance v1, Lorg/icepdf/core/pobjects/PageTree;

    iget-object v2, p0, Lorg/icepdf/core/pobjects/Catalog;->library:Lorg/icepdf/core/util/Library;

    check-cast v0, Ljava/util/Hashtable;

    .end local v0    # "tmp":Ljava/lang/Object;
    invoke-direct {v1, v2, v0}, Lorg/icepdf/core/pobjects/PageTree;-><init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V

    iput-object v1, p0, Lorg/icepdf/core/pobjects/Catalog;->pageTree:Lorg/icepdf/core/pobjects/PageTree;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 79
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 94
    :cond_2
    :try_start_2
    const-string/jumbo v1, "LOG"

    const-string/jumbo v2, "Error parsing page tree : pageTree is null"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 220
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "CATALOG= "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/icepdf/core/pobjects/Catalog;->entries:Ljava/util/Hashtable;

    invoke-virtual {v1}, Ljava/util/Hashtable;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
