.class public Lorg/icepdf/core/pobjects/fonts/DefaultFonts;
.super Ljava/lang/Object;
.source "DefaultFonts.java"


# static fields
.field private static final CourierBoldObliqueWidth:[F

.field private static final CourierBoldWidth:[F

.field private static final CourierObliqueWidth:[F

.field private static final CourierWidth:[F

.field private static final HelveticaBoldObliqueWidth:[F

.field private static final HelveticaBoldWidth:[F

.field private static final HelveticaObliqueWidth:[F

.field private static final HelveticaWidth:[F

.field private static final SymbolWidth:[F

.field private static final TimesBoldItalicWidth:[F

.field private static final TimesBoldWidth:[F

.field private static final TimesItalicWidth:[F

.field private static final TimesRomanWidth:[F

.field private static final ZapfDingbatsWidth:[F


# instance fields
.field private fontName:Ljava/lang/String;

.field private widths:[F


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/16 v1, 0xff

    .line 15
    new-array v0, v1, [F

    fill-array-data v0, :array_0

    sput-object v0, Lorg/icepdf/core/pobjects/fonts/DefaultFonts;->CourierWidth:[F

    .line 33
    new-array v0, v1, [F

    fill-array-data v0, :array_1

    sput-object v0, Lorg/icepdf/core/pobjects/fonts/DefaultFonts;->CourierBoldWidth:[F

    .line 51
    new-array v0, v1, [F

    fill-array-data v0, :array_2

    sput-object v0, Lorg/icepdf/core/pobjects/fonts/DefaultFonts;->CourierObliqueWidth:[F

    .line 69
    new-array v0, v1, [F

    fill-array-data v0, :array_3

    sput-object v0, Lorg/icepdf/core/pobjects/fonts/DefaultFonts;->CourierBoldObliqueWidth:[F

    .line 87
    new-array v0, v1, [F

    fill-array-data v0, :array_4

    sput-object v0, Lorg/icepdf/core/pobjects/fonts/DefaultFonts;->HelveticaWidth:[F

    .line 104
    new-array v0, v1, [F

    fill-array-data v0, :array_5

    sput-object v0, Lorg/icepdf/core/pobjects/fonts/DefaultFonts;->HelveticaBoldWidth:[F

    .line 120
    new-array v0, v1, [F

    fill-array-data v0, :array_6

    sput-object v0, Lorg/icepdf/core/pobjects/fonts/DefaultFonts;->HelveticaObliqueWidth:[F

    .line 136
    new-array v0, v1, [F

    fill-array-data v0, :array_7

    sput-object v0, Lorg/icepdf/core/pobjects/fonts/DefaultFonts;->HelveticaBoldObliqueWidth:[F

    .line 151
    new-array v0, v1, [F

    fill-array-data v0, :array_8

    sput-object v0, Lorg/icepdf/core/pobjects/fonts/DefaultFonts;->TimesRomanWidth:[F

    .line 166
    new-array v0, v1, [F

    fill-array-data v0, :array_9

    sput-object v0, Lorg/icepdf/core/pobjects/fonts/DefaultFonts;->TimesBoldWidth:[F

    .line 181
    new-array v0, v1, [F

    fill-array-data v0, :array_a

    sput-object v0, Lorg/icepdf/core/pobjects/fonts/DefaultFonts;->TimesItalicWidth:[F

    .line 196
    new-array v0, v1, [F

    fill-array-data v0, :array_b

    sput-object v0, Lorg/icepdf/core/pobjects/fonts/DefaultFonts;->TimesBoldItalicWidth:[F

    .line 211
    new-array v0, v1, [F

    fill-array-data v0, :array_c

    sput-object v0, Lorg/icepdf/core/pobjects/fonts/DefaultFonts;->ZapfDingbatsWidth:[F

    .line 227
    new-array v0, v1, [F

    fill-array-data v0, :array_d

    sput-object v0, Lorg/icepdf/core/pobjects/fonts/DefaultFonts;->SymbolWidth:[F

    return-void

    .line 15
    nop

    :array_0
    .array-data 4
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
    .end array-data

    .line 33
    :array_1
    .array-data 4
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
    .end array-data

    .line 51
    :array_2
    .array-data 4
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
    .end array-data

    .line 69
    :array_3
    .array-data 4
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x3f19999a    # 0.6f
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
    .end array-data

    .line 87
    :array_4
    .array-data 4
        0x3e8e5604    # 0.278f
        0x3e8e5604    # 0.278f
        0x3eb5c28f    # 0.355f
        0x3f0e5604    # 0.556f
        0x3f0e5604    # 0.556f
        0x3f639581    # 0.889f
        0x3f2ac083    # 0.667f
        0x3e6353f8    # 0.222f
        0x3eaa7efa    # 0.333f
        0x3eaa7efa    # 0.333f
        0x3ec72b02    # 0.389f
        0x3f158106    # 0.584f
        0x3e8e5604    # 0.278f
        0x3eaa7efa    # 0.333f
        0x3e8e5604    # 0.278f
        0x3e8e5604    # 0.278f
        0x3f0e5604    # 0.556f
        0x3f0e5604    # 0.556f
        0x3f0e5604    # 0.556f
        0x3f0e5604    # 0.556f
        0x3f0e5604    # 0.556f
        0x3f0e5604    # 0.556f
        0x3f0e5604    # 0.556f
        0x3f0e5604    # 0.556f
        0x3f0e5604    # 0.556f
        0x3f0e5604    # 0.556f
        0x3e8e5604    # 0.278f
        0x3e8e5604    # 0.278f
        0x3f158106    # 0.584f
        0x3f158106    # 0.584f
        0x3f158106    # 0.584f
        0x3f0e5604    # 0.556f
        0x3f81eb85    # 1.015f
        0x3f2ac083    # 0.667f
        0x3f2ac083    # 0.667f
        0x3f38d4fe    # 0.722f
        0x3f38d4fe    # 0.722f
        0x3f2ac083    # 0.667f
        0x3f1c6a7f    # 0.611f
        0x3f472b02    # 0.778f
        0x3f38d4fe    # 0.722f
        0x3e8e5604    # 0.278f
        0x3f000000    # 0.5f
        0x3f2ac083    # 0.667f
        0x3f0e5604    # 0.556f
        0x3f553f7d    # 0.833f
        0x3f38d4fe    # 0.722f
        0x3f472b02    # 0.778f
        0x3f2ac083    # 0.667f
        0x3f472b02    # 0.778f
        0x3f38d4fe    # 0.722f
        0x3f2ac083    # 0.667f
        0x3f1c6a7f    # 0.611f
        0x3f38d4fe    # 0.722f
        0x3f2ac083    # 0.667f
        0x3f71a9fc    # 0.944f
        0x3f2ac083    # 0.667f
        0x3f2ac083    # 0.667f
        0x3f1c6a7f    # 0.611f
        0x3e8e5604    # 0.278f
        0x3e8e5604    # 0.278f
        0x3e8e5604    # 0.278f
        0x3ef020c5    # 0.469f
        0x3f0e5604    # 0.556f
        0x3e6353f8    # 0.222f
        0x3f0e5604    # 0.556f
        0x3f0e5604    # 0.556f
        0x3f000000    # 0.5f
        0x3f0e5604    # 0.556f
        0x3f0e5604    # 0.556f
        0x3e8e5604    # 0.278f
        0x3f0e5604    # 0.556f
        0x3f0e5604    # 0.556f
        0x3e6353f8    # 0.222f
        0x3e6353f8    # 0.222f
        0x3f000000    # 0.5f
        0x3e6353f8    # 0.222f
        0x3f553f7d    # 0.833f
        0x3f0e5604    # 0.556f
        0x3f0e5604    # 0.556f
        0x3f0e5604    # 0.556f
        0x3f0e5604    # 0.556f
        0x3eaa7efa    # 0.333f
        0x3f000000    # 0.5f
        0x3e8e5604    # 0.278f
        0x3f0e5604    # 0.556f
        0x3f000000    # 0.5f
        0x3f38d4fe    # 0.722f
        0x3f000000    # 0.5f
        0x3f000000    # 0.5f
        0x3f000000    # 0.5f
        0x3eab020c    # 0.334f
        0x3e851eb8    # 0.26f
        0x3eab020c    # 0.334f
        0x3f158106    # 0.584f
        0x3eaa7efa    # 0.333f
        0x3f0e5604    # 0.556f
        0x3f0e5604    # 0.556f
        0x3e2b020c    # 0.167f
        0x3f0e5604    # 0.556f
        0x3f0e5604    # 0.556f
        0x3f0e5604    # 0.556f
        0x3f0e5604    # 0.556f
        0x3e439581    # 0.191f
        0x3eaa7efa    # 0.333f
        0x3f0e5604    # 0.556f
        0x3eaa7efa    # 0.333f
        0x3eaa7efa    # 0.333f
        0x3f000000    # 0.5f
        0x3f000000    # 0.5f
        0x3f0e5604    # 0.556f
        0x3f0e5604    # 0.556f
        0x3f0e5604    # 0.556f
        0x3e8e5604    # 0.278f
        0x3f0978d5    # 0.537f
        0x3eb33333    # 0.35f
        0x3e6353f8    # 0.222f
        0x3eaa7efa    # 0.333f
        0x3eaa7efa    # 0.333f
        0x3f0e5604    # 0.556f
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x3f1c6a7f    # 0.611f
        0x3eaa7efa    # 0.333f
        0x3eaa7efa    # 0.333f
        0x3eaa7efa    # 0.333f
        0x3eaa7efa    # 0.333f
        0x3eaa7efa    # 0.333f
        0x3eaa7efa    # 0.333f
        0x3eaa7efa    # 0.333f
        0x3eaa7efa    # 0.333f
        0x3eaa7efa    # 0.333f
        0x3eaa7efa    # 0.333f
        0x3eaa7efa    # 0.333f
        0x3eaa7efa    # 0.333f
        0x3eaa7efa    # 0.333f
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x3ebd70a4    # 0.37f
        0x3f0e5604    # 0.556f
        0x3f472b02    # 0.778f
        0x3f800000    # 1.0f
        0x3ebae148    # 0.365f
        0x3f639581    # 0.889f
        0x3e8e5604    # 0.278f
        0x3e6353f8    # 0.222f
        0x3f1c6a7f    # 0.611f
        0x3f71a9fc    # 0.944f
        0x3f1c6a7f    # 0.611f
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
    .end array-data

    .line 104
    :array_5
    .array-data 4
        0x3e8e5604    # 0.278f
        0x3eaa7efa    # 0.333f
        0x3ef2b021    # 0.474f
        0x3f0e5604    # 0.556f
        0x3f0e5604    # 0.556f
        0x3f639581    # 0.889f
        0x3f38d4fe    # 0.722f
        0x3e8e5604    # 0.278f
        0x3eaa7efa    # 0.333f
        0x3eaa7efa    # 0.333f
        0x3ec72b02    # 0.389f
        0x3f158106    # 0.584f
        0x3e8e5604    # 0.278f
        0x3eaa7efa    # 0.333f
        0x3e8e5604    # 0.278f
        0x3e8e5604    # 0.278f
        0x3f0e5604    # 0.556f
        0x3f0e5604    # 0.556f
        0x3f0e5604    # 0.556f
        0x3f0e5604    # 0.556f
        0x3f0e5604    # 0.556f
        0x3f0e5604    # 0.556f
        0x3f0e5604    # 0.556f
        0x3f0e5604    # 0.556f
        0x3f0e5604    # 0.556f
        0x3f0e5604    # 0.556f
        0x3eaa7efa    # 0.333f
        0x3eaa7efa    # 0.333f
        0x3f158106    # 0.584f
        0x3f158106    # 0.584f
        0x3f158106    # 0.584f
        0x3f1c6a7f    # 0.611f
        0x3f79999a    # 0.975f
        0x3f38d4fe    # 0.722f
        0x3f38d4fe    # 0.722f
        0x3f38d4fe    # 0.722f
        0x3f38d4fe    # 0.722f
        0x3f2ac083    # 0.667f
        0x3f1c6a7f    # 0.611f
        0x3f472b02    # 0.778f
        0x3f38d4fe    # 0.722f
        0x3e8e5604    # 0.278f
        0x3f0e5604    # 0.556f
        0x3f38d4fe    # 0.722f
        0x3f1c6a7f    # 0.611f
        0x3f553f7d    # 0.833f
        0x3f38d4fe    # 0.722f
        0x3f472b02    # 0.778f
        0x3f2ac083    # 0.667f
        0x3f472b02    # 0.778f
        0x3f38d4fe    # 0.722f
        0x3f2ac083    # 0.667f
        0x3f1c6a7f    # 0.611f
        0x3f38d4fe    # 0.722f
        0x3f2ac083    # 0.667f
        0x3f71a9fc    # 0.944f
        0x3f2ac083    # 0.667f
        0x3f2ac083    # 0.667f
        0x3f1c6a7f    # 0.611f
        0x3eaa7efa    # 0.333f
        0x3e8e5604    # 0.278f
        0x3eaa7efa    # 0.333f
        0x3f158106    # 0.584f
        0x3f0e5604    # 0.556f
        0x3e8e5604    # 0.278f
        0x3f0e5604    # 0.556f
        0x3f1c6a7f    # 0.611f
        0x3f0e5604    # 0.556f
        0x3f1c6a7f    # 0.611f
        0x3f0e5604    # 0.556f
        0x3eaa7efa    # 0.333f
        0x3f1c6a7f    # 0.611f
        0x3f1c6a7f    # 0.611f
        0x3e8e5604    # 0.278f
        0x3e8e5604    # 0.278f
        0x3f0e5604    # 0.556f
        0x3e8e5604    # 0.278f
        0x3f639581    # 0.889f
        0x3f1c6a7f    # 0.611f
        0x3f1c6a7f    # 0.611f
        0x3f1c6a7f    # 0.611f
        0x3f1c6a7f    # 0.611f
        0x3ec72b02    # 0.389f
        0x3f0e5604    # 0.556f
        0x3eaa7efa    # 0.333f
        0x3f1c6a7f    # 0.611f
        0x3f0e5604    # 0.556f
        0x3f472b02    # 0.778f
        0x3f0e5604    # 0.556f
        0x3f0e5604    # 0.556f
        0x3f000000    # 0.5f
        0x3ec72b02    # 0.389f
        0x3e8f5c29    # 0.28f
        0x3ec72b02    # 0.389f
        0x3f158106    # 0.584f
        0x3eaa7efa    # 0.333f
        0x3f0e5604    # 0.556f
        0x3f0e5604    # 0.556f
        0x3e2b020c    # 0.167f
        0x3f0e5604    # 0.556f
        0x3f0e5604    # 0.556f
        0x3f0e5604    # 0.556f
        0x3f0e5604    # 0.556f
        0x3e73b646    # 0.238f
        0x3f000000    # 0.5f
        0x3f0e5604    # 0.556f
        0x3eaa7efa    # 0.333f
        0x3eaa7efa    # 0.333f
        0x3f1c6a7f    # 0.611f
        0x3f1c6a7f    # 0.611f
        0x3f0e5604    # 0.556f
        0x3f0e5604    # 0.556f
        0x3f0e5604    # 0.556f
        0x3e8e5604    # 0.278f
        0x3f0e5604    # 0.556f
        0x3eb33333    # 0.35f
        0x3e8e5604    # 0.278f
        0x3f000000    # 0.5f
        0x3f000000    # 0.5f
        0x3f0e5604    # 0.556f
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x3f1c6a7f    # 0.611f
        0x3eaa7efa    # 0.333f
        0x3eaa7efa    # 0.333f
        0x3eaa7efa    # 0.333f
        0x3eaa7efa    # 0.333f
        0x3eaa7efa    # 0.333f
        0x3eaa7efa    # 0.333f
        0x3eaa7efa    # 0.333f
        0x3eaa7efa    # 0.333f
        0x3eaa7efa    # 0.333f
        0x3eaa7efa    # 0.333f
        0x3eaa7efa    # 0.333f
        0x3eaa7efa    # 0.333f
        0x3eaa7efa    # 0.333f
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x3ebd70a4    # 0.37f
        0x3f1c6a7f    # 0.611f
        0x3f472b02    # 0.778f
        0x3f800000    # 1.0f
        0x3ebae148    # 0.365f
        0x3f639581    # 0.889f
        0x3e8e5604    # 0.278f
        0x3e8e5604    # 0.278f
        0x3f1c6a7f    # 0.611f
        0x3f71a9fc    # 0.944f
        0x3f1c6a7f    # 0.611f
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
    .end array-data

    .line 120
    :array_6
    .array-data 4
        0x3e8e5604    # 0.278f
        0x3e8e5604    # 0.278f
        0x3eb5c28f    # 0.355f
        0x3f0e5604    # 0.556f
        0x3f0e5604    # 0.556f
        0x3f639581    # 0.889f
        0x3f2ac083    # 0.667f
        0x3e6353f8    # 0.222f
        0x3eaa7efa    # 0.333f
        0x3eaa7efa    # 0.333f
        0x3ec72b02    # 0.389f
        0x3f158106    # 0.584f
        0x3e8e5604    # 0.278f
        0x3eaa7efa    # 0.333f
        0x3e8e5604    # 0.278f
        0x3e8e5604    # 0.278f
        0x3f0e5604    # 0.556f
        0x3f0e5604    # 0.556f
        0x3f0e5604    # 0.556f
        0x3f0e5604    # 0.556f
        0x3f0e5604    # 0.556f
        0x3f0e5604    # 0.556f
        0x3f0e5604    # 0.556f
        0x3f0e5604    # 0.556f
        0x3f0e5604    # 0.556f
        0x3f0e5604    # 0.556f
        0x3e8e5604    # 0.278f
        0x3e8e5604    # 0.278f
        0x3f158106    # 0.584f
        0x3f158106    # 0.584f
        0x3f158106    # 0.584f
        0x3f0e5604    # 0.556f
        0x3f81eb85    # 1.015f
        0x3f2ac083    # 0.667f
        0x3f2ac083    # 0.667f
        0x3f38d4fe    # 0.722f
        0x3f38d4fe    # 0.722f
        0x3f2ac083    # 0.667f
        0x3f1c6a7f    # 0.611f
        0x3f472b02    # 0.778f
        0x3f38d4fe    # 0.722f
        0x3e8e5604    # 0.278f
        0x3f000000    # 0.5f
        0x3f2ac083    # 0.667f
        0x3f0e5604    # 0.556f
        0x3f553f7d    # 0.833f
        0x3f38d4fe    # 0.722f
        0x3f472b02    # 0.778f
        0x3f2ac083    # 0.667f
        0x3f472b02    # 0.778f
        0x3f38d4fe    # 0.722f
        0x3f2ac083    # 0.667f
        0x3f1c6a7f    # 0.611f
        0x3f38d4fe    # 0.722f
        0x3f2ac083    # 0.667f
        0x3f71a9fc    # 0.944f
        0x3f2ac083    # 0.667f
        0x3f2ac083    # 0.667f
        0x3f1c6a7f    # 0.611f
        0x3e8e5604    # 0.278f
        0x3e8e5604    # 0.278f
        0x3e8e5604    # 0.278f
        0x3ef020c5    # 0.469f
        0x3f0e5604    # 0.556f
        0x3e6353f8    # 0.222f
        0x3f0e5604    # 0.556f
        0x3f0e5604    # 0.556f
        0x3f000000    # 0.5f
        0x3f0e5604    # 0.556f
        0x3f0e5604    # 0.556f
        0x3e8e5604    # 0.278f
        0x3f0e5604    # 0.556f
        0x3f0e5604    # 0.556f
        0x3e6353f8    # 0.222f
        0x3e6353f8    # 0.222f
        0x3f000000    # 0.5f
        0x3e6353f8    # 0.222f
        0x3f553f7d    # 0.833f
        0x3f0e5604    # 0.556f
        0x3f0e5604    # 0.556f
        0x3f0e5604    # 0.556f
        0x3f0e5604    # 0.556f
        0x3eaa7efa    # 0.333f
        0x3f000000    # 0.5f
        0x3e8e5604    # 0.278f
        0x3f0e5604    # 0.556f
        0x3f000000    # 0.5f
        0x3f38d4fe    # 0.722f
        0x3f000000    # 0.5f
        0x3f000000    # 0.5f
        0x3f000000    # 0.5f
        0x3eab020c    # 0.334f
        0x3e851eb8    # 0.26f
        0x3eab020c    # 0.334f
        0x3f158106    # 0.584f
        0x3eaa7efa    # 0.333f
        0x3f0e5604    # 0.556f
        0x3f0e5604    # 0.556f
        0x3e2b020c    # 0.167f
        0x3f0e5604    # 0.556f
        0x3f0e5604    # 0.556f
        0x3f0e5604    # 0.556f
        0x3f0e5604    # 0.556f
        0x3e439581    # 0.191f
        0x3eaa7efa    # 0.333f
        0x3f0e5604    # 0.556f
        0x3eaa7efa    # 0.333f
        0x3eaa7efa    # 0.333f
        0x3f000000    # 0.5f
        0x3f000000    # 0.5f
        0x3f0e5604    # 0.556f
        0x3f0e5604    # 0.556f
        0x3f0e5604    # 0.556f
        0x3e8e5604    # 0.278f
        0x3f0978d5    # 0.537f
        0x3eb33333    # 0.35f
        0x3e6353f8    # 0.222f
        0x3eaa7efa    # 0.333f
        0x3eaa7efa    # 0.333f
        0x3f0e5604    # 0.556f
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x3f1c6a7f    # 0.611f
        0x3eaa7efa    # 0.333f
        0x3eaa7efa    # 0.333f
        0x3eaa7efa    # 0.333f
        0x3eaa7efa    # 0.333f
        0x3eaa7efa    # 0.333f
        0x3eaa7efa    # 0.333f
        0x3eaa7efa    # 0.333f
        0x3eaa7efa    # 0.333f
        0x3eaa7efa    # 0.333f
        0x3eaa7efa    # 0.333f
        0x3eaa7efa    # 0.333f
        0x3eaa7efa    # 0.333f
        0x3eaa7efa    # 0.333f
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x3ebd70a4    # 0.37f
        0x3f0e5604    # 0.556f
        0x3f472b02    # 0.778f
        0x3f800000    # 1.0f
        0x3ebae148    # 0.365f
        0x3f639581    # 0.889f
        0x3e8e5604    # 0.278f
        0x3e6353f8    # 0.222f
        0x3f1c6a7f    # 0.611f
        0x3f71a9fc    # 0.944f
        0x3f1c6a7f    # 0.611f
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
    .end array-data

    .line 136
    :array_7
    .array-data 4
        0x3e8e5604    # 0.278f
        0x3eaa7efa    # 0.333f
        0x3ef2b021    # 0.474f
        0x3f0e5604    # 0.556f
        0x3f0e5604    # 0.556f
        0x3f639581    # 0.889f
        0x3f38d4fe    # 0.722f
        0x3e8e5604    # 0.278f
        0x3eaa7efa    # 0.333f
        0x3eaa7efa    # 0.333f
        0x3ec72b02    # 0.389f
        0x3f158106    # 0.584f
        0x3e8e5604    # 0.278f
        0x3eaa7efa    # 0.333f
        0x3e8e5604    # 0.278f
        0x3e8e5604    # 0.278f
        0x3f0e5604    # 0.556f
        0x3f0e5604    # 0.556f
        0x3f0e5604    # 0.556f
        0x3f0e5604    # 0.556f
        0x3f0e5604    # 0.556f
        0x3f0e5604    # 0.556f
        0x3f0e5604    # 0.556f
        0x3f0e5604    # 0.556f
        0x3f0e5604    # 0.556f
        0x3f0e5604    # 0.556f
        0x3eaa7efa    # 0.333f
        0x3eaa7efa    # 0.333f
        0x3f158106    # 0.584f
        0x3f158106    # 0.584f
        0x3f158106    # 0.584f
        0x3f1c6a7f    # 0.611f
        0x3f79999a    # 0.975f
        0x3f38d4fe    # 0.722f
        0x3f38d4fe    # 0.722f
        0x3f38d4fe    # 0.722f
        0x3f38d4fe    # 0.722f
        0x3f2ac083    # 0.667f
        0x3f1c6a7f    # 0.611f
        0x3f472b02    # 0.778f
        0x3f38d4fe    # 0.722f
        0x3e8e5604    # 0.278f
        0x3f0e5604    # 0.556f
        0x3f38d4fe    # 0.722f
        0x3f1c6a7f    # 0.611f
        0x3f553f7d    # 0.833f
        0x3f38d4fe    # 0.722f
        0x3f472b02    # 0.778f
        0x3f2ac083    # 0.667f
        0x3f472b02    # 0.778f
        0x3f38d4fe    # 0.722f
        0x3f2ac083    # 0.667f
        0x3f1c6a7f    # 0.611f
        0x3f38d4fe    # 0.722f
        0x3f2ac083    # 0.667f
        0x3f71a9fc    # 0.944f
        0x3f2ac083    # 0.667f
        0x3f2ac083    # 0.667f
        0x3f1c6a7f    # 0.611f
        0x3eaa7efa    # 0.333f
        0x3e8e5604    # 0.278f
        0x3eaa7efa    # 0.333f
        0x3f158106    # 0.584f
        0x3f0e5604    # 0.556f
        0x3e8e5604    # 0.278f
        0x3f0e5604    # 0.556f
        0x3f1c6a7f    # 0.611f
        0x3f0e5604    # 0.556f
        0x3f1c6a7f    # 0.611f
        0x3f0e5604    # 0.556f
        0x3eaa7efa    # 0.333f
        0x3f1c6a7f    # 0.611f
        0x3f1c6a7f    # 0.611f
        0x3e8e5604    # 0.278f
        0x3e8e5604    # 0.278f
        0x3f0e5604    # 0.556f
        0x3e8e5604    # 0.278f
        0x3f639581    # 0.889f
        0x3f1c6a7f    # 0.611f
        0x3f1c6a7f    # 0.611f
        0x3f1c6a7f    # 0.611f
        0x3f1c6a7f    # 0.611f
        0x3ec72b02    # 0.389f
        0x3f0e5604    # 0.556f
        0x3eaa7efa    # 0.333f
        0x3f1c6a7f    # 0.611f
        0x3f0e5604    # 0.556f
        0x3f472b02    # 0.778f
        0x3f0e5604    # 0.556f
        0x3f0e5604    # 0.556f
        0x3f000000    # 0.5f
        0x3ec72b02    # 0.389f
        0x3e8f5c29    # 0.28f
        0x3ec72b02    # 0.389f
        0x3f158106    # 0.584f
        0x3eaa7efa    # 0.333f
        0x3f0e5604    # 0.556f
        0x3f0e5604    # 0.556f
        0x3e2b020c    # 0.167f
        0x3f0e5604    # 0.556f
        0x3f0e5604    # 0.556f
        0x3f0e5604    # 0.556f
        0x3f0e5604    # 0.556f
        0x3e73b646    # 0.238f
        0x3f000000    # 0.5f
        0x3f0e5604    # 0.556f
        0x3eaa7efa    # 0.333f
        0x3eaa7efa    # 0.333f
        0x3f1c6a7f    # 0.611f
        0x3f1c6a7f    # 0.611f
        0x3f0e5604    # 0.556f
        0x3f0e5604    # 0.556f
        0x3f0e5604    # 0.556f
        0x3e8e5604    # 0.278f
        0x3f0e5604    # 0.556f
        0x3eb33333    # 0.35f
        0x3e8e5604    # 0.278f
        0x3f000000    # 0.5f
        0x3f000000    # 0.5f
        0x3f0e5604    # 0.556f
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x3f1c6a7f    # 0.611f
        0x3eaa7efa    # 0.333f
        0x3eaa7efa    # 0.333f
        0x3eaa7efa    # 0.333f
        0x3eaa7efa    # 0.333f
        0x3eaa7efa    # 0.333f
        0x3eaa7efa    # 0.333f
        0x3eaa7efa    # 0.333f
        0x3eaa7efa    # 0.333f
        0x3eaa7efa    # 0.333f
        0x3eaa7efa    # 0.333f
        0x3eaa7efa    # 0.333f
        0x3eaa7efa    # 0.333f
        0x3eaa7efa    # 0.333f
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x3ebd70a4    # 0.37f
        0x3f1c6a7f    # 0.611f
        0x3f472b02    # 0.778f
        0x3f800000    # 1.0f
        0x3ebae148    # 0.365f
        0x3f639581    # 0.889f
        0x3e8e5604    # 0.278f
        0x3e8e5604    # 0.278f
        0x3f1c6a7f    # 0.611f
        0x3f71a9fc    # 0.944f
        0x3f1c6a7f    # 0.611f
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
    .end array-data

    .line 151
    :array_8
    .array-data 4
        0x3e800000    # 0.25f
        0x3eaa7efa    # 0.333f
        0x3ed0e560    # 0.408f
        0x3f000000    # 0.5f
        0x3f000000    # 0.5f
        0x3f553f7d    # 0.833f
        0x3f472b02    # 0.778f
        0x3eaa7efa    # 0.333f
        0x3eaa7efa    # 0.333f
        0x3eaa7efa    # 0.333f
        0x3f000000    # 0.5f
        0x3f10624e    # 0.564f
        0x3e800000    # 0.25f
        0x3eaa7efa    # 0.333f
        0x3e800000    # 0.25f
        0x3e8e5604    # 0.278f
        0x3f000000    # 0.5f
        0x3f000000    # 0.5f
        0x3f000000    # 0.5f
        0x3f000000    # 0.5f
        0x3f000000    # 0.5f
        0x3f000000    # 0.5f
        0x3f000000    # 0.5f
        0x3f000000    # 0.5f
        0x3f000000    # 0.5f
        0x3f000000    # 0.5f
        0x3e8e5604    # 0.278f
        0x3e8e5604    # 0.278f
        0x3f10624e    # 0.564f
        0x3f10624e    # 0.564f
        0x3f10624e    # 0.564f
        0x3ee353f8    # 0.444f
        0x3f6bc6a8    # 0.921f
        0x3f38d4fe    # 0.722f
        0x3f2ac083    # 0.667f
        0x3f2ac083    # 0.667f
        0x3f38d4fe    # 0.722f
        0x3f1c6a7f    # 0.611f
        0x3f0e5604    # 0.556f
        0x3f38d4fe    # 0.722f
        0x3f38d4fe    # 0.722f
        0x3eaa7efa    # 0.333f
        0x3ec72b02    # 0.389f
        0x3f38d4fe    # 0.722f
        0x3f1c6a7f    # 0.611f
        0x3f639581    # 0.889f
        0x3f38d4fe    # 0.722f
        0x3f38d4fe    # 0.722f
        0x3f0e5604    # 0.556f
        0x3f38d4fe    # 0.722f
        0x3f2ac083    # 0.667f
        0x3f0e5604    # 0.556f
        0x3f1c6a7f    # 0.611f
        0x3f38d4fe    # 0.722f
        0x3f38d4fe    # 0.722f
        0x3f71a9fc    # 0.944f
        0x3f38d4fe    # 0.722f
        0x3f38d4fe    # 0.722f
        0x3f1c6a7f    # 0.611f
        0x3eaa7efa    # 0.333f
        0x3e8e5604    # 0.278f
        0x3eaa7efa    # 0.333f
        0x3ef020c5    # 0.469f
        0x3f000000    # 0.5f
        0x3eaa7efa    # 0.333f
        0x3ee353f8    # 0.444f
        0x3f000000    # 0.5f
        0x3ee353f8    # 0.444f
        0x3f000000    # 0.5f
        0x3ee353f8    # 0.444f
        0x3eaa7efa    # 0.333f
        0x3f000000    # 0.5f
        0x3f000000    # 0.5f
        0x3e8e5604    # 0.278f
        0x3e8e5604    # 0.278f
        0x3f000000    # 0.5f
        0x3e8e5604    # 0.278f
        0x3f472b02    # 0.778f
        0x3f000000    # 0.5f
        0x3f000000    # 0.5f
        0x3f000000    # 0.5f
        0x3f000000    # 0.5f
        0x3eaa7efa    # 0.333f
        0x3ec72b02    # 0.389f
        0x3e8e5604    # 0.278f
        0x3f000000    # 0.5f
        0x3f000000    # 0.5f
        0x3f38d4fe    # 0.722f
        0x3f000000    # 0.5f
        0x3f000000    # 0.5f
        0x3ee353f8    # 0.444f
        0x3ef5c28f    # 0.48f
        0x3e4ccccd    # 0.2f
        0x3ef5c28f    # 0.48f
        0x3f0a7efa    # 0.541f
        0x3eaa7efa    # 0.333f
        0x3f000000    # 0.5f
        0x3f000000    # 0.5f
        0x3e2b020c    # 0.167f
        0x3f000000    # 0.5f
        0x3f000000    # 0.5f
        0x3f000000    # 0.5f
        0x3f000000    # 0.5f
        0x3e3851ec    # 0.18f
        0x3ee353f8    # 0.444f
        0x3f000000    # 0.5f
        0x3eaa7efa    # 0.333f
        0x3eaa7efa    # 0.333f
        0x3f0e5604    # 0.556f
        0x3f0e5604    # 0.556f
        0x3f000000    # 0.5f
        0x3f000000    # 0.5f
        0x3f000000    # 0.5f
        0x3e800000    # 0.25f
        0x3ee7ef9e    # 0.453f
        0x3eb33333    # 0.35f
        0x3eaa7efa    # 0.333f
        0x3ee353f8    # 0.444f
        0x3ee353f8    # 0.444f
        0x3f000000    # 0.5f
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x3ee353f8    # 0.444f
        0x3eaa7efa    # 0.333f
        0x3eaa7efa    # 0.333f
        0x3eaa7efa    # 0.333f
        0x3eaa7efa    # 0.333f
        0x3eaa7efa    # 0.333f
        0x3eaa7efa    # 0.333f
        0x3eaa7efa    # 0.333f
        0x3eaa7efa    # 0.333f
        0x3eaa7efa    # 0.333f
        0x3eaa7efa    # 0.333f
        0x3eaa7efa    # 0.333f
        0x3eaa7efa    # 0.333f
        0x3eaa7efa    # 0.333f
        0x3f800000    # 1.0f
        0x3f639581    # 0.889f
        0x3e8d4fdf    # 0.276f
        0x3f1c6a7f    # 0.611f
        0x3f38d4fe    # 0.722f
        0x3f639581    # 0.889f
        0x3e9eb852    # 0.31f
        0x3f2ac083    # 0.667f
        0x3e8e5604    # 0.278f
        0x3e8e5604    # 0.278f
        0x3f000000    # 0.5f
        0x3f38d4fe    # 0.722f
        0x3f000000    # 0.5f
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
    .end array-data

    .line 166
    :array_9
    .array-data 4
        0x3e800000    # 0.25f
        0x3eaa7efa    # 0.333f
        0x3f0e147b    # 0.555f
        0x3f000000    # 0.5f
        0x3f000000    # 0.5f
        0x3f800000    # 1.0f
        0x3f553f7d    # 0.833f
        0x3eaa7efa    # 0.333f
        0x3eaa7efa    # 0.333f
        0x3eaa7efa    # 0.333f
        0x3f000000    # 0.5f
        0x3f11eb85    # 0.57f
        0x3e800000    # 0.25f
        0x3eaa7efa    # 0.333f
        0x3e800000    # 0.25f
        0x3e8e5604    # 0.278f
        0x3f000000    # 0.5f
        0x3f000000    # 0.5f
        0x3f000000    # 0.5f
        0x3f000000    # 0.5f
        0x3f000000    # 0.5f
        0x3f000000    # 0.5f
        0x3f000000    # 0.5f
        0x3f000000    # 0.5f
        0x3f000000    # 0.5f
        0x3f000000    # 0.5f
        0x3eaa7efa    # 0.333f
        0x3eaa7efa    # 0.333f
        0x3f11eb85    # 0.57f
        0x3f11eb85    # 0.57f
        0x3f11eb85    # 0.57f
        0x3f000000    # 0.5f
        0x3f6e147b    # 0.93f
        0x3f38d4fe    # 0.722f
        0x3f2ac083    # 0.667f
        0x3f38d4fe    # 0.722f
        0x3f38d4fe    # 0.722f
        0x3f2ac083    # 0.667f
        0x3f1c6a7f    # 0.611f
        0x3f472b02    # 0.778f
        0x3f472b02    # 0.778f
        0x3ec72b02    # 0.389f
        0x3f000000    # 0.5f
        0x3f472b02    # 0.778f
        0x3f2ac083    # 0.667f
        0x3f71a9fc    # 0.944f
        0x3f38d4fe    # 0.722f
        0x3f472b02    # 0.778f
        0x3f1c6a7f    # 0.611f
        0x3f472b02    # 0.778f
        0x3f38d4fe    # 0.722f
        0x3f0e5604    # 0.556f
        0x3f2ac083    # 0.667f
        0x3f38d4fe    # 0.722f
        0x3f38d4fe    # 0.722f
        0x3f800000    # 1.0f
        0x3f38d4fe    # 0.722f
        0x3f38d4fe    # 0.722f
        0x3f2ac083    # 0.667f
        0x3eaa7efa    # 0.333f
        0x3e8e5604    # 0.278f
        0x3eaa7efa    # 0.333f
        0x3f14bc6a    # 0.581f
        0x3f000000    # 0.5f
        0x3eaa7efa    # 0.333f
        0x3f000000    # 0.5f
        0x3f0e5604    # 0.556f
        0x3ee353f8    # 0.444f
        0x3f0e5604    # 0.556f
        0x3ee353f8    # 0.444f
        0x3eaa7efa    # 0.333f
        0x3f000000    # 0.5f
        0x3f0e5604    # 0.556f
        0x3e8e5604    # 0.278f
        0x3eaa7efa    # 0.333f
        0x3f0e5604    # 0.556f
        0x3e8e5604    # 0.278f
        0x3f553f7d    # 0.833f
        0x3f0e5604    # 0.556f
        0x3f000000    # 0.5f
        0x3f0e5604    # 0.556f
        0x3f0e5604    # 0.556f
        0x3ee353f8    # 0.444f
        0x3ec72b02    # 0.389f
        0x3eaa7efa    # 0.333f
        0x3f0e5604    # 0.556f
        0x3f000000    # 0.5f
        0x3f38d4fe    # 0.722f
        0x3f000000    # 0.5f
        0x3f000000    # 0.5f
        0x3ee353f8    # 0.444f
        0x3ec9ba5e    # 0.394f
        0x3e6147ae    # 0.22f
        0x3ec9ba5e    # 0.394f
        0x3f051eb8    # 0.52f
        0x3eaa7efa    # 0.333f
        0x3f000000    # 0.5f
        0x3f000000    # 0.5f
        0x3e2b020c    # 0.167f
        0x3f000000    # 0.5f
        0x3f000000    # 0.5f
        0x3f000000    # 0.5f
        0x3f000000    # 0.5f
        0x3e8e5604    # 0.278f
        0x3f000000    # 0.5f
        0x3f000000    # 0.5f
        0x3eaa7efa    # 0.333f
        0x3eaa7efa    # 0.333f
        0x3f0e5604    # 0.556f
        0x3f0e5604    # 0.556f
        0x3f000000    # 0.5f
        0x3f000000    # 0.5f
        0x3f000000    # 0.5f
        0x3e800000    # 0.25f
        0x3f0a3d71    # 0.54f
        0x3eb33333    # 0.35f
        0x3eaa7efa    # 0.333f
        0x3f000000    # 0.5f
        0x3f000000    # 0.5f
        0x3f000000    # 0.5f
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x3f000000    # 0.5f
        0x3eaa7efa    # 0.333f
        0x3eaa7efa    # 0.333f
        0x3eaa7efa    # 0.333f
        0x3eaa7efa    # 0.333f
        0x3eaa7efa    # 0.333f
        0x3eaa7efa    # 0.333f
        0x3eaa7efa    # 0.333f
        0x3eaa7efa    # 0.333f
        0x3eaa7efa    # 0.333f
        0x3eaa7efa    # 0.333f
        0x3eaa7efa    # 0.333f
        0x3eaa7efa    # 0.333f
        0x3eaa7efa    # 0.333f
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x3e99999a    # 0.3f
        0x3f2ac083    # 0.667f
        0x3f472b02    # 0.778f
        0x3f800000    # 1.0f
        0x3ea8f5c3    # 0.33f
        0x3f38d4fe    # 0.722f
        0x3e8e5604    # 0.278f
        0x3e8e5604    # 0.278f
        0x3f000000    # 0.5f
        0x3f38d4fe    # 0.722f
        0x3f0e5604    # 0.556f
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
    .end array-data

    .line 181
    :array_a
    .array-data 4
        0x3e800000    # 0.25f
        0x3eaa7efa    # 0.333f
        0x3ed70a3d    # 0.42f
        0x3f000000    # 0.5f
        0x3f000000    # 0.5f
        0x3f553f7d    # 0.833f
        0x3f472b02    # 0.778f
        0x3eaa7efa    # 0.333f
        0x3eaa7efa    # 0.333f
        0x3eaa7efa    # 0.333f
        0x3f000000    # 0.5f
        0x3f2ccccd    # 0.675f
        0x3e800000    # 0.25f
        0x3eaa7efa    # 0.333f
        0x3e800000    # 0.25f
        0x3e8e5604    # 0.278f
        0x3f000000    # 0.5f
        0x3f000000    # 0.5f
        0x3f000000    # 0.5f
        0x3f000000    # 0.5f
        0x3f000000    # 0.5f
        0x3f000000    # 0.5f
        0x3f000000    # 0.5f
        0x3f000000    # 0.5f
        0x3f000000    # 0.5f
        0x3f000000    # 0.5f
        0x3eaa7efa    # 0.333f
        0x3eaa7efa    # 0.333f
        0x3f2ccccd    # 0.675f
        0x3f2ccccd    # 0.675f
        0x3f2ccccd    # 0.675f
        0x3f000000    # 0.5f
        0x3f6b851f    # 0.92f
        0x3f1c6a7f    # 0.611f
        0x3f1c6a7f    # 0.611f
        0x3f2ac083    # 0.667f
        0x3f38d4fe    # 0.722f
        0x3f1c6a7f    # 0.611f
        0x3f1c6a7f    # 0.611f
        0x3f38d4fe    # 0.722f
        0x3f38d4fe    # 0.722f
        0x3eaa7efa    # 0.333f
        0x3ee353f8    # 0.444f
        0x3f2ac083    # 0.667f
        0x3f0e5604    # 0.556f
        0x3f553f7d    # 0.833f
        0x3f2ac083    # 0.667f
        0x3f38d4fe    # 0.722f
        0x3f1c6a7f    # 0.611f
        0x3f38d4fe    # 0.722f
        0x3f1c6a7f    # 0.611f
        0x3f000000    # 0.5f
        0x3f0e5604    # 0.556f
        0x3f38d4fe    # 0.722f
        0x3f1c6a7f    # 0.611f
        0x3f553f7d    # 0.833f
        0x3f1c6a7f    # 0.611f
        0x3f0e5604    # 0.556f
        0x3f0e5604    # 0.556f
        0x3ec72b02    # 0.389f
        0x3e8e5604    # 0.278f
        0x3ec72b02    # 0.389f
        0x3ed81062    # 0.422f
        0x3f000000    # 0.5f
        0x3eaa7efa    # 0.333f
        0x3f000000    # 0.5f
        0x3f000000    # 0.5f
        0x3ee353f8    # 0.444f
        0x3f000000    # 0.5f
        0x3ee353f8    # 0.444f
        0x3e8e5604    # 0.278f
        0x3f000000    # 0.5f
        0x3f000000    # 0.5f
        0x3e8e5604    # 0.278f
        0x3e8e5604    # 0.278f
        0x3ee353f8    # 0.444f
        0x3e8e5604    # 0.278f
        0x3f38d4fe    # 0.722f
        0x3f000000    # 0.5f
        0x3f000000    # 0.5f
        0x3f000000    # 0.5f
        0x3f000000    # 0.5f
        0x3ec72b02    # 0.389f
        0x3ec72b02    # 0.389f
        0x3e8e5604    # 0.278f
        0x3f000000    # 0.5f
        0x3ee353f8    # 0.444f
        0x3f2ac083    # 0.667f
        0x3ee353f8    # 0.444f
        0x3ee353f8    # 0.444f
        0x3ec72b02    # 0.389f
        0x3ecccccd    # 0.4f
        0x3e8ccccd    # 0.275f
        0x3ecccccd    # 0.4f
        0x3f0a7efa    # 0.541f
        0x3ec72b02    # 0.389f
        0x3f000000    # 0.5f
        0x3f000000    # 0.5f
        0x3e2b020c    # 0.167f
        0x3f000000    # 0.5f
        0x3f000000    # 0.5f
        0x3f000000    # 0.5f
        0x3f000000    # 0.5f
        0x3e5b22d1    # 0.214f
        0x3f0e5604    # 0.556f
        0x3f000000    # 0.5f
        0x3eaa7efa    # 0.333f
        0x3eaa7efa    # 0.333f
        0x3f000000    # 0.5f
        0x3f000000    # 0.5f
        0x3f000000    # 0.5f
        0x3f000000    # 0.5f
        0x3f000000    # 0.5f
        0x3e800000    # 0.25f
        0x3f05e354    # 0.523f
        0x3eb33333    # 0.35f
        0x3eaa7efa    # 0.333f
        0x3f0e5604    # 0.556f
        0x3f0e5604    # 0.556f
        0x3f000000    # 0.5f
        0x3f639581    # 0.889f
        0x3f800000    # 1.0f
        0x3f000000    # 0.5f
        0x3eaa7efa    # 0.333f
        0x3eaa7efa    # 0.333f
        0x3eaa7efa    # 0.333f
        0x3eaa7efa    # 0.333f
        0x3eaa7efa    # 0.333f
        0x3eaa7efa    # 0.333f
        0x3eaa7efa    # 0.333f
        0x3eaa7efa    # 0.333f
        0x3eaa7efa    # 0.333f
        0x3eaa7efa    # 0.333f
        0x3eaa7efa    # 0.333f
        0x3eaa7efa    # 0.333f
        0x3eaa7efa    # 0.333f
        0x3f639581    # 0.889f
        0x3f639581    # 0.889f
        0x3e8d4fdf    # 0.276f
        0x3f0e5604    # 0.556f
        0x3f38d4fe    # 0.722f
        0x3f71a9fc    # 0.944f
        0x3e9eb852    # 0.31f
        0x3f2ac083    # 0.667f
        0x3e8e5604    # 0.278f
        0x3e8e5604    # 0.278f
        0x3f000000    # 0.5f
        0x3f2ac083    # 0.667f
        0x3f000000    # 0.5f
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
    .end array-data

    .line 196
    :array_b
    .array-data 4
        0x3e800000    # 0.25f
        0x3ec72b02    # 0.389f
        0x3f0e147b    # 0.555f
        0x3f000000    # 0.5f
        0x3f000000    # 0.5f
        0x3f553f7d    # 0.833f
        0x3f472b02    # 0.778f
        0x3eaa7efa    # 0.333f
        0x3eaa7efa    # 0.333f
        0x3eaa7efa    # 0.333f
        0x3f000000    # 0.5f
        0x3f11eb85    # 0.57f
        0x3e800000    # 0.25f
        0x3eaa7efa    # 0.333f
        0x3e800000    # 0.25f
        0x3e8e5604    # 0.278f
        0x3f000000    # 0.5f
        0x3f000000    # 0.5f
        0x3f000000    # 0.5f
        0x3f000000    # 0.5f
        0x3f000000    # 0.5f
        0x3f000000    # 0.5f
        0x3f000000    # 0.5f
        0x3f000000    # 0.5f
        0x3f000000    # 0.5f
        0x3f000000    # 0.5f
        0x3eaa7efa    # 0.333f
        0x3eaa7efa    # 0.333f
        0x3f11eb85    # 0.57f
        0x3f11eb85    # 0.57f
        0x3f11eb85    # 0.57f
        0x3f000000    # 0.5f
        0x3f54fdf4    # 0.832f
        0x3f2ac083    # 0.667f
        0x3f2ac083    # 0.667f
        0x3f2ac083    # 0.667f
        0x3f38d4fe    # 0.722f
        0x3f2ac083    # 0.667f
        0x3f2ac083    # 0.667f
        0x3f38d4fe    # 0.722f
        0x3f472b02    # 0.778f
        0x3ec72b02    # 0.389f
        0x3f000000    # 0.5f
        0x3f2ac083    # 0.667f
        0x3f1c6a7f    # 0.611f
        0x3f639581    # 0.889f
        0x3f38d4fe    # 0.722f
        0x3f38d4fe    # 0.722f
        0x3f1c6a7f    # 0.611f
        0x3f38d4fe    # 0.722f
        0x3f2ac083    # 0.667f
        0x3f0e5604    # 0.556f
        0x3f1c6a7f    # 0.611f
        0x3f38d4fe    # 0.722f
        0x3f2ac083    # 0.667f
        0x3f639581    # 0.889f
        0x3f2ac083    # 0.667f
        0x3f1c6a7f    # 0.611f
        0x3f1c6a7f    # 0.611f
        0x3eaa7efa    # 0.333f
        0x3e8e5604    # 0.278f
        0x3eaa7efa    # 0.333f
        0x3f11eb85    # 0.57f
        0x3f000000    # 0.5f
        0x3eaa7efa    # 0.333f
        0x3f000000    # 0.5f
        0x3f000000    # 0.5f
        0x3ee353f8    # 0.444f
        0x3f000000    # 0.5f
        0x3ee353f8    # 0.444f
        0x3eaa7efa    # 0.333f
        0x3f000000    # 0.5f
        0x3f0e5604    # 0.556f
        0x3e8e5604    # 0.278f
        0x3e8e5604    # 0.278f
        0x3f000000    # 0.5f
        0x3e8e5604    # 0.278f
        0x3f472b02    # 0.778f
        0x3f0e5604    # 0.556f
        0x3f000000    # 0.5f
        0x3f000000    # 0.5f
        0x3f000000    # 0.5f
        0x3ec72b02    # 0.389f
        0x3ec72b02    # 0.389f
        0x3e8e5604    # 0.278f
        0x3f0e5604    # 0.556f
        0x3ee353f8    # 0.444f
        0x3f2ac083    # 0.667f
        0x3f000000    # 0.5f
        0x3ee353f8    # 0.444f
        0x3ec72b02    # 0.389f
        0x3eb22d0e    # 0.348f
        0x3e6147ae    # 0.22f
        0x3eb22d0e    # 0.348f
        0x3f11eb85    # 0.57f
        0x3ec72b02    # 0.389f
        0x3f000000    # 0.5f
        0x3f000000    # 0.5f
        0x3e2b020c    # 0.167f
        0x3f000000    # 0.5f
        0x3f000000    # 0.5f
        0x3f000000    # 0.5f
        0x3f000000    # 0.5f
        0x3e8e5604    # 0.278f
        0x3f000000    # 0.5f
        0x3f000000    # 0.5f
        0x3eaa7efa    # 0.333f
        0x3eaa7efa    # 0.333f
        0x3f0e5604    # 0.556f
        0x3f0e5604    # 0.556f
        0x3f000000    # 0.5f
        0x3f000000    # 0.5f
        0x3f000000    # 0.5f
        0x3e800000    # 0.25f
        0x3f000000    # 0.5f
        0x3eb33333    # 0.35f
        0x3eaa7efa    # 0.333f
        0x3f000000    # 0.5f
        0x3f000000    # 0.5f
        0x3f000000    # 0.5f
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x3f000000    # 0.5f
        0x3eaa7efa    # 0.333f
        0x3eaa7efa    # 0.333f
        0x3eaa7efa    # 0.333f
        0x3eaa7efa    # 0.333f
        0x3eaa7efa    # 0.333f
        0x3eaa7efa    # 0.333f
        0x3eaa7efa    # 0.333f
        0x3eaa7efa    # 0.333f
        0x3eaa7efa    # 0.333f
        0x3eaa7efa    # 0.333f
        0x3eaa7efa    # 0.333f
        0x3eaa7efa    # 0.333f
        0x3eaa7efa    # 0.333f
        0x3f800000    # 1.0f
        0x3f71a9fc    # 0.944f
        0x3e883127    # 0.266f
        0x3f1c6a7f    # 0.611f
        0x3f38d4fe    # 0.722f
        0x3f71a9fc    # 0.944f
        0x3e99999a    # 0.3f
        0x3f38d4fe    # 0.722f
        0x3e8e5604    # 0.278f
        0x3e8e5604    # 0.278f
        0x3f000000    # 0.5f
        0x3f38d4fe    # 0.722f
        0x3f000000    # 0.5f
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
    .end array-data

    .line 211
    :array_c
    .array-data 4
        0x3e8e5604    # 0.278f
        0x3f795810    # 0.974f
        0x3f760419    # 0.961f
        0x3f795810    # 0.974f
        0x3f7ae148    # 0.98f
        0x3f381062    # 0.719f
        0x3f49fbe7    # 0.789f
        0x3f4a3d71    # 0.79f
        0x3f4a7efa    # 0.791f
        0x3f30a3d7    # 0.69f
        0x3f75c28f    # 0.96f
        0x3f70624e    # 0.939f
        0x3f0c8b44    # 0.549f
        0x3f5ae148    # 0.855f
        0x3f69374c    # 0.911f
        0x3f6ed917    # 0.933f
        0x3f69374c    # 0.911f
        0x3f71eb85    # 0.945f
        0x3f795810    # 0.974f
        0x3f4147ae    # 0.755f
        0x3f589375    # 0.846f
        0x3f43126f    # 0.762f
        0x3f42d0e5    # 0.761f
        0x3f122d0e    # 0.571f
        0x3f2d4fdf    # 0.677f
        0x3f4353f8    # 0.763f
        0x3f428f5c    # 0.76f
        0x3f424dd3    # 0.759f
        0x3f410625    # 0.754f
        0x3efced91    # 0.494f
        0x3f0d4fdf    # 0.552f
        0x3f0978d5    # 0.537f
        0x3f13b646    # 0.577f
        0x3f3126e9    # 0.692f
        0x3f49374c    # 0.786f
        0x3f49ba5e    # 0.788f
        0x3f49ba5e    # 0.788f
        0x3f4a3d71    # 0.79f
        0x3f4b020c    # 0.793f
        0x3f4b4396    # 0.794f
        0x3f50e560    # 0.816f
        0x3f52b021    # 0.823f
        0x3f49fbe7    # 0.789f
        0x3f574bc7    # 0.841f
        0x3f52b021    # 0.823f
        0x3f553f7d    # 0.833f
        0x3f50e560    # 0.816f
        0x3f54bc6a    # 0.831f
        0x3f6c49ba    # 0.923f
        0x3f3e76c9    # 0.744f
        0x3f391687    # 0.723f
        0x3f3fbe77    # 0.749f
        0x3f4a3d71    # 0.79f
        0x3f4ac083    # 0.792f
        0x3f31eb85    # 0.695f
        0x3f46a7f0    # 0.776f
        0x3f449ba6    # 0.768f
        0x3f4ac083    # 0.792f
        0x3f424dd3    # 0.759f
        0x3f34fdf4    # 0.707f
        0x3f353f7d    # 0.708f
        0x3f2e978d    # 0.682f
        0x3f3374bc    # 0.701f
        0x3f5374bc    # 0.826f
        0x3f50a3d7    # 0.815f
        0x3f49fbe7    # 0.789f
        0x3f49fbe7    # 0.789f
        0x3f34fdf4    # 0.707f
        0x3f2fdf3b    # 0.687f
        0x3f322d0e    # 0.696f
        0x3f30624e    # 0.689f
        0x3f49374c    # 0.786f
        0x3f4978d5    # 0.787f
        0x3f36872b    # 0.713f
        0x3f4a7efa    # 0.791f
        0x3f48f5c3    # 0.785f
        0x3f4a7efa    # 0.791f
        0x3f5f7cee    # 0.873f
        0x3f42d0e5    # 0.761f
        0x3f43126f    # 0.762f
        0x3f43126f    # 0.762f
        0x3f424dd3    # 0.759f
        0x3f424dd3    # 0.759f
        0x3f645a1d    # 0.892f
        0x3f645a1d    # 0.892f
        0x3f49ba5e    # 0.788f
        0x3f48b439    # 0.784f
        0x3ee04189    # 0.438f
        0x3e0d4fdf    # 0.138f
        0x3e8dd2f2    # 0.277f
        0x3ed47ae1    # 0.415f
        0x3ec8b439    # 0.392f
        0x3ec8b439    # 0.392f
        0x3f2b020c    # 0.668f
        0x3f2b020c    # 0.668f
        0x3ec7ae14    # 0.39f
        0x3ec7ae14    # 0.39f
        0x3ea24dd3    # 0.317f
        0x3ea24dd3    # 0.317f
        0x3e8d4fdf    # 0.276f
        0x3e8d4fdf    # 0.276f
        0x3f024dd3    # 0.509f
        0x3f024dd3    # 0.509f
        0x3ed1eb85    # 0.41f
        0x3ed1eb85    # 0.41f
        0x3e6f9db2    # 0.234f
        0x3e6f9db2    # 0.234f
        0x3eab020c    # 0.334f
        0x3eab020c    # 0.334f
        0x3f3b645a    # 0.732f
        0x3f0b4396    # 0.544f
        0x3f0b4396    # 0.544f
        0x3f68f5c3    # 0.91f
        0x3f2ac083    # 0.667f
        0x3f428f5c    # 0.76f
        0x3f428f5c    # 0.76f
        0x3f46a7f0    # 0.776f
        0x3f1851ec    # 0.595f
        0x3f31a9fc    # 0.694f
        0x3f204189    # 0.626f
        0x3f49ba5e    # 0.788f
        0x3f49ba5e    # 0.788f
        0x3f49ba5e    # 0.788f
        0x3f49ba5e    # 0.788f
        0x3f49ba5e    # 0.788f
        0x3f49ba5e    # 0.788f
        0x3f49ba5e    # 0.788f
        0x3f49ba5e    # 0.788f
        0x3f49ba5e    # 0.788f
        0x3f49ba5e    # 0.788f
        0x3f49ba5e    # 0.788f
        0x3f49ba5e    # 0.788f
        0x3f49ba5e    # 0.788f
        0x3f49ba5e    # 0.788f
        0x3f49ba5e    # 0.788f
        0x3f49ba5e    # 0.788f
        0x3f49ba5e    # 0.788f
        0x3f49ba5e    # 0.788f
        0x3f49ba5e    # 0.788f
        0x3f49ba5e    # 0.788f
        0x3f49ba5e    # 0.788f
        0x3f49ba5e    # 0.788f
        0x3f49ba5e    # 0.788f
        0x3f49ba5e    # 0.788f
        0x3f49ba5e    # 0.788f
        0x3f49ba5e    # 0.788f
        0x3f49ba5e    # 0.788f
        0x3f49ba5e    # 0.788f
        0x3f49ba5e    # 0.788f
        0x3f49ba5e    # 0.788f
        0x3f49ba5e    # 0.788f
        0x3f49ba5e    # 0.788f
        0x3f49ba5e    # 0.788f
        0x3f49ba5e    # 0.788f
        0x3f49ba5e    # 0.788f
        0x3f49ba5e    # 0.788f
        0x3f49ba5e    # 0.788f
        0x3f49ba5e    # 0.788f
        0x3f49ba5e    # 0.788f
        0x3f49ba5e    # 0.788f
        0x3f64dd2f    # 0.894f
        0x3f56872b    # 0.838f
        0x3f820c4a    # 1.016f
        0x3eea7efa    # 0.458f
        0x3f3f7cee    # 0.748f
        0x3f6c8b44    # 0.924f
        0x3f3f7cee    # 0.748f
        0x3f6b020c    # 0.918f
        0x3f6d4fdf    # 0.927f
        0x3f6d9168    # 0.928f
        0x3f6d9168    # 0.928f
        0x3f558106    # 0.834f
        0x3f5f7cee    # 0.873f
        0x3f53f7cf    # 0.828f
        0x3f6c8b44    # 0.924f
        0x3f6c8b44    # 0.924f
        0x3f6ac083    # 0.917f
        0x3f6e147b    # 0.93f
        0x3f6e5604    # 0.931f
        0x3eed0e56    # 0.463f
        0x3f620c4a    # 0.883f
        0x3f560419    # 0.836f
        0x3f560419    # 0.836f
        0x3f5df3b6    # 0.867f
        0x3f5df3b6    # 0.867f
        0x3f322d0e    # 0.696f
        0x3f322d0e    # 0.696f
        0x3f5fbe77    # 0.874f
        0x3f5fbe77    # 0.874f
        0x3f428f5c    # 0.76f
        0x3f722d0e    # 0.946f
        0x3f456042    # 0.771f
        0x3f5d70a4    # 0.865f
        0x3f456042    # 0.771f
        0x3f6353f8    # 0.888f
        0x3f778d50    # 0.967f
        0x3f6353f8    # 0.888f
        0x3f54bc6a    # 0.831f
        0x3f5f7cee    # 0.873f
        0x3f6d4fdf    # 0.927f
        0x3f7851ec    # 0.97f
        0x3f6b020c    # 0.918f
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
    .end array-data

    .line 227
    :array_d
    .array-data 4
        0x3e800000    # 0.25f
        0x3eaa7efa    # 0.333f
        0x3f36872b    # 0.713f
        0x3f000000    # 0.5f
        0x3f0c8b44    # 0.549f
        0x3f553f7d    # 0.833f
        0x3f472b02    # 0.778f
        0x3ee0c49c    # 0.439f
        0x3eaa7efa    # 0.333f
        0x3eaa7efa    # 0.333f
        0x3f000000    # 0.5f
        0x3f0c8b44    # 0.549f
        0x3e800000    # 0.25f
        0x3f0c8b44    # 0.549f
        0x3e800000    # 0.25f
        0x3e8e5604    # 0.278f
        0x3f000000    # 0.5f
        0x3f000000    # 0.5f
        0x3f000000    # 0.5f
        0x3f000000    # 0.5f
        0x3f000000    # 0.5f
        0x3f000000    # 0.5f
        0x3f000000    # 0.5f
        0x3f000000    # 0.5f
        0x3f000000    # 0.5f
        0x3f000000    # 0.5f
        0x3e8e5604    # 0.278f
        0x3e8e5604    # 0.278f
        0x3f0c8b44    # 0.549f
        0x3f0c8b44    # 0.549f
        0x3f0c8b44    # 0.549f
        0x3ee353f8    # 0.444f
        0x3f0c8b44    # 0.549f
        0x3f38d4fe    # 0.722f
        0x3f2ac083    # 0.667f
        0x3f38d4fe    # 0.722f
        0x3f1cac08    # 0.612f
        0x3f1c6a7f    # 0.611f
        0x3f4353f8    # 0.763f
        0x3f1a5e35    # 0.603f
        0x3f38d4fe    # 0.722f
        0x3eaa7efa    # 0.333f
        0x3f218937    # 0.631f
        0x3f38d4fe    # 0.722f
        0x3f2f9db2    # 0.686f
        0x3f639581    # 0.889f
        0x3f38d4fe    # 0.722f
        0x3f38d4fe    # 0.722f
        0x3f449ba6    # 0.768f
        0x3f3db22d    # 0.741f
        0x3f0e5604    # 0.556f
        0x3f178d50    # 0.592f
        0x3f1c6a7f    # 0.611f
        0x3f30a3d7    # 0.69f
        0x3ee0c49c    # 0.439f
        0x3f449ba6    # 0.768f
        0x3f251eb8    # 0.645f
        0x3f4b851f    # 0.795f
        0x3f1c6a7f    # 0.611f
        0x3eaa7efa    # 0.333f
        0x3f5ced91    # 0.863f
        0x3eaa7efa    # 0.333f
        0x3f2872b0    # 0.658f
        0x3f000000    # 0.5f
        0x3f000000    # 0.5f
        0x3f218937    # 0.631f
        0x3f0c8b44    # 0.549f
        0x3f0c8b44    # 0.549f
        0x3efced91    # 0.494f
        0x3ee0c49c    # 0.439f
        0x3f056042    # 0.521f
        0x3ed26e98    # 0.411f
        0x3f1a5e35    # 0.603f
        0x3ea872b0    # 0.329f
        0x3f1a5e35    # 0.603f
        0x3f0c8b44    # 0.549f
        0x3f0c8b44    # 0.549f
        0x3f1374bc    # 0.576f
        0x3f056042    # 0.521f
        0x3f0c8b44    # 0.549f
        0x3f0c8b44    # 0.549f
        0x3f056042    # 0.521f
        0x3f0c8b44    # 0.549f
        0x3f1a5e35    # 0.603f
        0x3ee0c49c    # 0.439f
        0x3f1374bc    # 0.576f
        0x3f36872b    # 0.713f
        0x3f2f9db2    # 0.686f
        0x3efc6a7f    # 0.493f
        0x3f2f9db2    # 0.686f
        0x3efced91    # 0.494f
        0x3ef5c28f    # 0.48f
        0x3e4ccccd    # 0.2f
        0x3ef5c28f    # 0.48f
        0x3f0c8b44    # 0.549f
        0x3f400000    # 0.75f
        0x3f1eb852    # 0.62f
        0x3e7ced91    # 0.247f
        0x3f0c8b44    # 0.549f
        0x3e2b020c    # 0.167f
        0x3f36872b    # 0.713f
        0x3f000000    # 0.5f
        0x3f40c49c    # 0.753f
        0x3f40c49c    # 0.753f
        0x3f40c49c    # 0.753f
        0x3f40c49c    # 0.753f
        0x3f856042    # 1.042f
        0x3f7cac08    # 0.987f
        0x3f1a5e35    # 0.603f
        0x3f7cac08    # 0.987f
        0x3f1a5e35    # 0.603f
        0x3ecccccd    # 0.4f
        0x3f0c8b44    # 0.549f
        0x3ed26e98    # 0.411f
        0x3f0c8b44    # 0.549f
        0x3f0c8b44    # 0.549f
        0x3f36872b    # 0.713f
        0x3efced91    # 0.494f
        0x3eeb851f    # 0.46f
        0x3f0c8b44    # 0.549f
        0x3f0c8b44    # 0.549f
        0x3f0c8b44    # 0.549f
        0x3f0c8b44    # 0.549f
        0x3f800000    # 1.0f
        0x3f1a5e35    # 0.603f
        0x3f800000    # 1.0f
        0x3f2872b0    # 0.658f
        0x3f52b021    # 0.823f
        0x3f2f9db2    # 0.686f
        0x3f4b851f    # 0.795f
        0x3f7cac08    # 0.987f
        0x3f449ba6    # 0.768f
        0x3f449ba6    # 0.768f
        0x3f52b021    # 0.823f
        0x3f449ba6    # 0.768f
        0x3f449ba6    # 0.768f
        0x3f36872b    # 0.713f
        0x3f36872b    # 0.713f
        0x3f36872b    # 0.713f
        0x3f36872b    # 0.713f
        0x3f36872b    # 0.713f
        0x3f36872b    # 0.713f
        0x3f36872b    # 0.713f
        0x3f449ba6    # 0.768f
        0x3f36872b    # 0.713f
        0x3f4a3d71    # 0.79f
        0x3f4a3d71    # 0.79f
        0x3f63d70a    # 0.89f
        0x3f52b021    # 0.823f
        0x3f0c8b44    # 0.549f
        0x3e800000    # 0.25f
        0x3f36872b    # 0.713f
        0x3f1a5e35    # 0.603f
        0x3f1a5e35    # 0.603f
        0x3f856042    # 1.042f
        0x3f7cac08    # 0.987f
        0x3f1a5e35    # 0.603f
        0x3f7cac08    # 0.987f
        0x3f1a5e35    # 0.603f
        0x3efced91    # 0.494f
        0x3ea872b0    # 0.329f
        0x3f4a3d71    # 0.79f
        0x3f4a3d71    # 0.79f
        0x3f49374c    # 0.786f
        0x3f36872b    # 0.713f
        0x3ec49ba6    # 0.384f
        0x3ec49ba6    # 0.384f
        0x3ec49ba6    # 0.384f
        0x3ec49ba6    # 0.384f
        0x3ec49ba6    # 0.384f
        0x3ec49ba6    # 0.384f
        0x3efced91    # 0.494f
        0x3efced91    # 0.494f
        0x3efced91    # 0.494f
        0x3efced91    # 0.494f
        0x3ea872b0    # 0.329f
        0x3e8c49ba    # 0.274f
        0x3f2f9db2    # 0.686f
        0x3f2f9db2    # 0.686f
        0x3f2f9db2    # 0.686f
        0x3ec49ba6    # 0.384f
        0x3ec49ba6    # 0.384f
        0x3ec49ba6    # 0.384f
        0x3ec49ba6    # 0.384f
        0x3ec49ba6    # 0.384f
        0x3ec49ba6    # 0.384f
        0x3efced91    # 0.494f
        0x3efced91    # 0.494f
        0x3efced91    # 0.494f
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
    .end array-data
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "fontname"    # Ljava/lang/String;

    .prologue
    .line 246
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 243
    const/16 v0, 0xff

    new-array v0, v0, [F

    iput-object v0, p0, Lorg/icepdf/core/pobjects/fonts/DefaultFonts;->widths:[F

    .line 247
    iput-object p1, p0, Lorg/icepdf/core/pobjects/fonts/DefaultFonts;->fontName:Ljava/lang/String;

    .line 248
    invoke-direct {p0, p1}, Lorg/icepdf/core/pobjects/fonts/DefaultFonts;->selectWidthArray(Ljava/lang/String;)[F

    move-result-object v0

    iput-object v0, p0, Lorg/icepdf/core/pobjects/fonts/DefaultFonts;->widths:[F

    .line 249
    return-void
.end method

.method private selectWidthArray(Ljava/lang/String;)[F
    .locals 1
    .param p1, "fontname"    # Ljava/lang/String;

    .prologue
    .line 270
    const-string/jumbo v0, "Courier"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 271
    sget-object v0, Lorg/icepdf/core/pobjects/fonts/DefaultFonts;->CourierWidth:[F

    .line 299
    :goto_0
    return-object v0

    .line 272
    :cond_0
    const-string/jumbo v0, "Courier-Bold"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 273
    sget-object v0, Lorg/icepdf/core/pobjects/fonts/DefaultFonts;->CourierBoldWidth:[F

    goto :goto_0

    .line 274
    :cond_1
    const-string/jumbo v0, "Courier-BoldOblique"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 275
    sget-object v0, Lorg/icepdf/core/pobjects/fonts/DefaultFonts;->CourierBoldObliqueWidth:[F

    goto :goto_0

    .line 276
    :cond_2
    const-string/jumbo v0, "Courier-Oblique"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 277
    sget-object v0, Lorg/icepdf/core/pobjects/fonts/DefaultFonts;->CourierObliqueWidth:[F

    goto :goto_0

    .line 278
    :cond_3
    const-string/jumbo v0, "Helvetica"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 279
    sget-object v0, Lorg/icepdf/core/pobjects/fonts/DefaultFonts;->HelveticaWidth:[F

    goto :goto_0

    .line 280
    :cond_4
    const-string/jumbo v0, "Helvetica-Bold"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 281
    sget-object v0, Lorg/icepdf/core/pobjects/fonts/DefaultFonts;->HelveticaBoldWidth:[F

    goto :goto_0

    .line 282
    :cond_5
    const-string/jumbo v0, "Helvetica-BoldOblique"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 283
    sget-object v0, Lorg/icepdf/core/pobjects/fonts/DefaultFonts;->HelveticaBoldObliqueWidth:[F

    goto :goto_0

    .line 284
    :cond_6
    const-string/jumbo v0, "Helvetica-Oblique"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 285
    sget-object v0, Lorg/icepdf/core/pobjects/fonts/DefaultFonts;->HelveticaObliqueWidth:[F

    goto :goto_0

    .line 286
    :cond_7
    const-string/jumbo v0, "Symbol"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 287
    sget-object v0, Lorg/icepdf/core/pobjects/fonts/DefaultFonts;->SymbolWidth:[F

    goto :goto_0

    .line 288
    :cond_8
    const-string/jumbo v0, "Times-Roman"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 289
    sget-object v0, Lorg/icepdf/core/pobjects/fonts/DefaultFonts;->TimesRomanWidth:[F

    goto :goto_0

    .line 290
    :cond_9
    const-string/jumbo v0, "Times-Bold"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 291
    sget-object v0, Lorg/icepdf/core/pobjects/fonts/DefaultFonts;->TimesBoldWidth:[F

    goto :goto_0

    .line 292
    :cond_a
    const-string/jumbo v0, "Times-BoldItalic"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 293
    sget-object v0, Lorg/icepdf/core/pobjects/fonts/DefaultFonts;->TimesBoldItalicWidth:[F

    goto/16 :goto_0

    .line 294
    :cond_b
    const-string/jumbo v0, "Times-Italic"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 295
    sget-object v0, Lorg/icepdf/core/pobjects/fonts/DefaultFonts;->TimesItalicWidth:[F

    goto/16 :goto_0

    .line 296
    :cond_c
    const-string/jumbo v0, "ZapfDingbats"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 297
    sget-object v0, Lorg/icepdf/core/pobjects/fonts/DefaultFonts;->ZapfDingbatsWidth:[F

    goto/16 :goto_0

    .line 299
    :cond_d
    const/4 v0, 0x0

    goto/16 :goto_0
.end method


# virtual methods
.method public getFontname()Ljava/lang/String;
    .locals 1

    .prologue
    .line 260
    iget-object v0, p0, Lorg/icepdf/core/pobjects/fonts/DefaultFonts;->fontName:Ljava/lang/String;

    return-object v0
.end method

.method public getWidths()[F
    .locals 1

    .prologue
    .line 254
    iget-object v0, p0, Lorg/icepdf/core/pobjects/fonts/DefaultFonts;->widths:[F

    return-object v0
.end method
