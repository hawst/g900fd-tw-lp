.class public Lorg/icepdf/core/pobjects/fonts/ofont/OFont;
.super Ljava/lang/Object;
.source "OFont.java"

# interfaces
.implements Lorg/icepdf/core/pobjects/fonts/FontFile;


# static fields
.field private static final TAG:Ljava/lang/String; = "DocumentParser"

.field private static final at:Landroid/graphics/Matrix;

.field private static final log:Ljava/util/logging/Logger;


# instance fields
.field private androidFont:Landroid/text/TextPaint;

.field private androidTypeface:Landroid/graphics/Typeface;

.field protected ascent:F

.field protected cMap:[C

.field protected cidWidths:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field protected descent:F

.field private echarAdvanceCache:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/PointF;",
            ">;"
        }
    .end annotation
.end field

.field protected encoding:Lorg/icepdf/core/pobjects/fonts/Encoding;

.field private family:Ljava/lang/String;

.field protected firstCh:I

.field private maxCharBounds:Landroid/graphics/RectF;

.field protected missingWidth:F

.field private name:Ljava/lang/String;

.field private originalFontName:Ljava/lang/String;

.field private style:I

.field protected toUnicode:Lorg/icepdf/core/pobjects/fonts/CMap;

.field protected widths:[F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 49
    const-class v0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;

    invoke-virtual {v0}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->log:Ljava/util/logging/Logger;

    .line 52
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    sput-object v0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->at:Landroid/graphics/Matrix;

    return-void
.end method

.method public constructor <init>(Landroid/graphics/Typeface;Ljava/lang/String;Ljava/lang/String;FLjava/lang/String;)V
    .locals 3
    .param p1, "androidTypeface"    # Landroid/graphics/Typeface;
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "family"    # Ljava/lang/String;
    .param p4, "size"    # F
    .param p5, "orgName"    # Ljava/lang/String;

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    .line 148
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0, v2, v2, v1, v1}, Landroid/graphics/RectF;-><init>(FFFF)V

    iput-object v0, p0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->maxCharBounds:Landroid/graphics/RectF;

    .line 149
    invoke-virtual {p1}, Landroid/graphics/Typeface;->getStyle()I

    move-result v0

    iput v0, p0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->style:I

    .line 150
    iput-object p2, p0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->name:Ljava/lang/String;

    .line 151
    iput-object p5, p0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->originalFontName:Ljava/lang/String;

    .line 153
    new-instance v0, Landroid/text/TextPaint;

    invoke-direct {v0}, Landroid/text/TextPaint;-><init>()V

    iput-object v0, p0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->androidFont:Landroid/text/TextPaint;

    .line 161
    iput-object p1, p0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->androidTypeface:Landroid/graphics/Typeface;

    .line 163
    iget-object v0, p0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->androidFont:Landroid/text/TextPaint;

    iget-object v1, p0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->androidTypeface:Landroid/graphics/Typeface;

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 165
    cmpl-float v0, p4, v2

    if-eqz v0, :cond_0

    .line 166
    iget-object v0, p0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->androidFont:Landroid/text/TextPaint;

    invoke-virtual {v0, p4}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 167
    :cond_0
    iget-object v0, p0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->androidFont:Landroid/text/TextPaint;

    const/16 v1, 0x81

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setFlags(I)V

    .line 170
    iput-object p3, p0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->family:Ljava/lang/String;

    .line 171
    new-instance v0, Ljava/util/HashMap;

    const/16 v1, 0x100

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->echarAdvanceCache:Ljava/util/HashMap;

    .line 174
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 8
    .param p1, "str"    # Ljava/lang/String;
    .param p2, "orgName"    # Ljava/lang/String;

    .prologue
    const/high16 v7, 0x3f800000    # 1.0f

    const/4 v6, 0x0

    .line 81
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    new-instance v5, Landroid/graphics/RectF;

    invoke-direct {v5, v6, v6, v7, v7}, Landroid/graphics/RectF;-><init>(FFFF)V

    iput-object v5, p0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->maxCharBounds:Landroid/graphics/RectF;

    .line 83
    new-instance v4, Ljava/util/StringTokenizer;

    const-string/jumbo v5, "-"

    invoke-direct {v4, p1, v5}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 84
    .local v4, "tokenizer":Ljava/util/StringTokenizer;
    invoke-virtual {v4}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v1

    .line 85
    .local v1, "nameToken":Ljava/lang/String;
    const-string/jumbo v2, "12"

    .line 87
    .local v2, "sizeToken":Ljava/lang/String;
    invoke-virtual {v4}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 88
    invoke-virtual {v4}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v3

    .line 90
    .local v3, "token":Ljava/lang/String;
    const/4 v5, 0x0

    invoke-virtual {v3, v5}, Ljava/lang/String;->charAt(I)C

    move-result v5

    invoke-static {v5}, Ljava/lang/Character;->isDigit(C)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 91
    move-object v2, v3

    .line 100
    .end local v3    # "token":Ljava/lang/String;
    :cond_0
    :goto_0
    invoke-static {p1}, Lorg/icepdf/core/util/FontUtil;->guessAndroidFontStyle(Ljava/lang/String;)I

    move-result v5

    iput v5, p0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->style:I

    .line 101
    iput-object v1, p0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->name:Ljava/lang/String;

    .line 102
    iput-object p2, p0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->originalFontName:Ljava/lang/String;

    .line 104
    new-instance v5, Landroid/text/TextPaint;

    invoke-direct {v5}, Landroid/text/TextPaint;-><init>()V

    iput-object v5, p0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->androidFont:Landroid/text/TextPaint;

    .line 112
    invoke-static {p1}, Lorg/icepdf/core/util/FontUtil;->guessAndroidFontStyle(Ljava/lang/String;)I

    move-result v5

    invoke-static {v1, v5}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v5

    iput-object v5, p0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->androidTypeface:Landroid/graphics/Typeface;

    .line 115
    iget-object v5, p0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->androidFont:Landroid/text/TextPaint;

    iget-object v6, p0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->androidTypeface:Landroid/graphics/Typeface;

    invoke-virtual {v5, v6}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 116
    iget-object v5, p0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->androidFont:Landroid/text/TextPaint;

    const/16 v6, 0x81

    invoke-virtual {v5, v6}, Landroid/text/TextPaint;->setFlags(I)V

    .line 122
    :try_start_0
    iget-object v5, p0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->androidFont:Landroid/text/TextPaint;

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    int-to-float v6, v6

    invoke-virtual {v5, v6}, Landroid/text/TextPaint;->setTextSize(F)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 128
    :goto_1
    iput-object v1, p0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->family:Ljava/lang/String;

    .line 129
    new-instance v5, Ljava/util/HashMap;

    const/16 v6, 0x100

    invoke-direct {v5, v6}, Ljava/util/HashMap;-><init>(I)V

    iput-object v5, p0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->echarAdvanceCache:Ljava/util/HashMap;

    .line 132
    return-void

    .line 93
    .restart local v3    # "token":Ljava/lang/String;
    :cond_1
    invoke-virtual {v4}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 94
    invoke-virtual {v4}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 123
    .end local v3    # "token":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 124
    .local v0, "e":Ljava/lang/NumberFormatException;
    const-string/jumbo v5, "DocumentParser"

    const-string/jumbo v6, "Exception while parsing font from name"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 125
    iget-object v5, p0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->androidFont:Landroid/text/TextPaint;

    const/high16 v6, 0x41400000    # 12.0f

    invoke-virtual {v5, v6}, Landroid/text/TextPaint;->setTextSize(F)V

    goto :goto_1
.end method

.method private constructor <init>(Lorg/icepdf/core/pobjects/fonts/ofont/OFont;)V
    .locals 3
    .param p1, "font"    # Lorg/icepdf/core/pobjects/fonts/ofont/OFont;

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    .line 176
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0, v1, v1, v2, v2}, Landroid/graphics/RectF;-><init>(FFFF)V

    iput-object v0, p0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->maxCharBounds:Landroid/graphics/RectF;

    .line 177
    iget v0, p1, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->style:I

    iput v0, p0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->style:I

    .line 178
    iget-object v0, p1, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->name:Ljava/lang/String;

    iput-object v0, p0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->name:Ljava/lang/String;

    .line 179
    iget-object v0, p1, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->originalFontName:Ljava/lang/String;

    iput-object v0, p0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->originalFontName:Ljava/lang/String;

    .line 181
    iget-object v0, p1, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->echarAdvanceCache:Ljava/util/HashMap;

    iput-object v0, p0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->echarAdvanceCache:Ljava/util/HashMap;

    .line 182
    new-instance v0, Landroid/text/TextPaint;

    invoke-direct {v0}, Landroid/text/TextPaint;-><init>()V

    iput-object v0, p0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->androidFont:Landroid/text/TextPaint;

    .line 184
    iget-object v0, p0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->androidFont:Landroid/text/TextPaint;

    iget-object v1, p1, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->androidFont:Landroid/text/TextPaint;

    invoke-virtual {v1}, Landroid/text/TextPaint;->getTextSize()F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 193
    iget-object v0, p1, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->androidTypeface:Landroid/graphics/Typeface;

    iput-object v0, p0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->androidTypeface:Landroid/graphics/Typeface;

    .line 195
    iget-object v0, p0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->androidFont:Landroid/text/TextPaint;

    iget-object v1, p0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->androidTypeface:Landroid/graphics/Typeface;

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 197
    iget-object v0, p0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->androidFont:Landroid/text/TextPaint;

    const/16 v1, 0x81

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setFlags(I)V

    .line 199
    iget-object v0, p1, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->family:Ljava/lang/String;

    iput-object v0, p0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->family:Ljava/lang/String;

    .line 200
    iget-object v0, p1, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->encoding:Lorg/icepdf/core/pobjects/fonts/Encoding;

    iput-object v0, p0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->encoding:Lorg/icepdf/core/pobjects/fonts/Encoding;

    .line 201
    iget-object v0, p1, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->toUnicode:Lorg/icepdf/core/pobjects/fonts/CMap;

    iput-object v0, p0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->toUnicode:Lorg/icepdf/core/pobjects/fonts/CMap;

    .line 202
    iget v0, p1, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->missingWidth:F

    iput v0, p0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->missingWidth:F

    .line 203
    iget v0, p1, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->firstCh:I

    iput v0, p0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->firstCh:I

    .line 204
    iget v0, p1, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->ascent:F

    iput v0, p0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->ascent:F

    .line 205
    iget v0, p1, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->descent:F

    iput v0, p0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->descent:F

    .line 206
    iget-object v0, p1, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->widths:[F

    iput-object v0, p0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->widths:[F

    .line 207
    iget-object v0, p1, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->cidWidths:Ljava/util/Map;

    iput-object v0, p0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->cidWidths:Ljava/util/Map;

    .line 208
    iget-object v0, p1, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->cMap:[C

    iput-object v0, p0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->cMap:[C

    .line 209
    iget-object v0, p1, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->maxCharBounds:Landroid/graphics/RectF;

    iput-object v0, p0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->maxCharBounds:Landroid/graphics/RectF;

    .line 210
    return-void
.end method

.method private findAlternateSymbol(C)C
    .locals 4
    .param p1, "character"    # C

    .prologue
    .line 433
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    sget-object v2, Lorg/icepdf/core/pobjects/fonts/ofont/Encoding;->symbolAlaises:[[I

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 434
    const/4 v1, 0x0

    .local v1, "j":I
    :goto_1
    sget-object v2, Lorg/icepdf/core/pobjects/fonts/ofont/Encoding;->symbolAlaises:[[I

    aget-object v2, v2, v0

    array-length v2, v2

    if-ge v1, v2, :cond_2

    .line 435
    sget-object v2, Lorg/icepdf/core/pobjects/fonts/ofont/Encoding;->symbolAlaises:[[I

    aget-object v2, v2, v0

    aget v2, v2, v1

    if-ne v2, p1, :cond_1

    .line 438
    sget-object v2, Lorg/icepdf/core/pobjects/fonts/ofont/Encoding;->symbolAlaises:[[I

    aget-object v2, v2, v0

    const/4 v3, 0x0

    aget v2, v2, v3

    int-to-char p1, v2

    .line 442
    .end local v1    # "j":I
    .end local p1    # "character":C
    :cond_0
    return p1

    .line 434
    .restart local v1    # "j":I
    .restart local p1    # "character":C
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 433
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private getCMapping(C)C
    .locals 1
    .param p1, "currentChar"    # C

    .prologue
    .line 410
    iget-object v0, p0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->toUnicode:Lorg/icepdf/core/pobjects/fonts/CMap;

    if-eqz v0, :cond_0

    .line 411
    iget-object v0, p0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->toUnicode:Lorg/icepdf/core/pobjects/fonts/CMap;

    invoke-interface {v0, p1}, Lorg/icepdf/core/pobjects/fonts/CMap;->toSelector(C)C

    move-result p1

    .line 413
    .end local p1    # "currentChar":C
    :cond_0
    return p1
.end method


# virtual methods
.method public canDisplayEchar(C)Z
    .locals 1
    .param p1, "ech"    # C

    .prologue
    .line 293
    const/4 v0, 0x1

    return v0
.end method

.method public deriveFont(F)Lorg/icepdf/core/pobjects/fonts/FontFile;
    .locals 3
    .param p1, "pointsize"    # F

    .prologue
    .line 297
    new-instance v0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;

    invoke-direct {v0, p0}, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;-><init>(Lorg/icepdf/core/pobjects/fonts/ofont/OFont;)V

    .line 299
    .local v0, "font":Lorg/icepdf/core/pobjects/fonts/ofont/OFont;
    invoke-virtual {v0}, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->getSize()F

    move-result v1

    cmpl-float v1, v1, p1

    if-eqz v1, :cond_0

    .line 300
    iget-object v1, p0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->echarAdvanceCache:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->clear()V

    .line 305
    :cond_0
    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1}, Landroid/text/TextPaint;-><init>()V

    iput-object v1, v0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->androidFont:Landroid/text/TextPaint;

    .line 306
    iget-object v1, v0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->androidFont:Landroid/text/TextPaint;

    invoke-virtual {v1, p1}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 312
    iget-object v1, p0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->androidTypeface:Landroid/graphics/Typeface;

    if-eqz v1, :cond_1

    .line 313
    iget-object v1, p0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->androidTypeface:Landroid/graphics/Typeface;

    iput-object v1, v0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->androidTypeface:Landroid/graphics/Typeface;

    .line 319
    :goto_0
    iget-object v1, v0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->androidFont:Landroid/text/TextPaint;

    iget-object v2, v0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->androidTypeface:Landroid/graphics/Typeface;

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 320
    iget-object v1, v0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->androidFont:Landroid/text/TextPaint;

    const/16 v2, 0x81

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setFlags(I)V

    .line 323
    iget-object v1, p0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->name:Ljava/lang/String;

    iput-object v1, v0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->name:Ljava/lang/String;

    .line 324
    iget-object v1, p0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->originalFontName:Ljava/lang/String;

    iput-object v1, v0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->originalFontName:Ljava/lang/String;

    .line 325
    iget-object v1, p0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->family:Ljava/lang/String;

    iput-object v1, v0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->family:Ljava/lang/String;

    .line 327
    iget-object v1, p0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->maxCharBounds:Landroid/graphics/RectF;

    iput-object v1, v0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->maxCharBounds:Landroid/graphics/RectF;

    .line 328
    return-object v0

    .line 315
    :cond_1
    iget-object v1, p0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->family:Ljava/lang/String;

    iget-object v2, p0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->androidTypeface:Landroid/graphics/Typeface;

    invoke-virtual {v2}, Landroid/graphics/Typeface;->getStyle()I

    move-result v2

    invoke-static {v1, v2}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v1

    iput-object v1, v0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->androidTypeface:Landroid/graphics/Typeface;

    goto :goto_0
.end method

.method public deriveFont(Landroid/graphics/Matrix;)Lorg/icepdf/core/pobjects/fonts/FontFile;
    .locals 3
    .param p1, "at"    # Landroid/graphics/Matrix;

    .prologue
    .line 257
    new-instance v0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;

    invoke-direct {v0, p0}, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;-><init>(Lorg/icepdf/core/pobjects/fonts/ofont/OFont;)V

    .line 265
    .local v0, "font":Lorg/icepdf/core/pobjects/fonts/ofont/OFont;
    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1}, Landroid/text/TextPaint;-><init>()V

    iput-object v1, v0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->androidFont:Landroid/text/TextPaint;

    .line 267
    iget-object v1, v0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->androidFont:Landroid/text/TextPaint;

    iget-object v2, p0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->androidFont:Landroid/text/TextPaint;

    invoke-virtual {v2}, Landroid/text/TextPaint;->getTextSize()F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 273
    iget-object v1, p0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->androidTypeface:Landroid/graphics/Typeface;

    if-eqz v1, :cond_0

    .line 274
    iget-object v1, p0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->androidTypeface:Landroid/graphics/Typeface;

    iput-object v1, v0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->androidTypeface:Landroid/graphics/Typeface;

    .line 280
    :goto_0
    iget-object v1, v0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->androidFont:Landroid/text/TextPaint;

    iget-object v2, v0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->androidTypeface:Landroid/graphics/Typeface;

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 281
    iget-object v1, v0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->androidFont:Landroid/text/TextPaint;

    const/16 v2, 0x81

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setFlags(I)V

    .line 284
    iget-object v1, p0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->name:Ljava/lang/String;

    iput-object v1, v0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->name:Ljava/lang/String;

    .line 285
    iget-object v1, p0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->originalFontName:Ljava/lang/String;

    iput-object v1, v0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->originalFontName:Ljava/lang/String;

    .line 286
    iget-object v1, p0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->family:Ljava/lang/String;

    iput-object v1, v0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->family:Ljava/lang/String;

    .line 288
    iget-object v1, p0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->maxCharBounds:Landroid/graphics/RectF;

    iput-object v1, v0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->maxCharBounds:Landroid/graphics/RectF;

    .line 289
    return-object v0

    .line 276
    :cond_0
    iget-object v1, p0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->family:Ljava/lang/String;

    iget-object v2, p0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->androidTypeface:Landroid/graphics/Typeface;

    invoke-virtual {v2}, Landroid/graphics/Typeface;->getStyle()I

    move-result v2

    invoke-static {v1, v2}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v1

    iput-object v1, v0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->androidTypeface:Landroid/graphics/Typeface;

    goto :goto_0
.end method

.method public deriveFont(Ljava/util/Map;IFFF[C)Lorg/icepdf/core/pobjects/fonts/FontFile;
    .locals 2
    .param p2, "firstCh"    # I
    .param p3, "missingWidth"    # F
    .param p4, "ascent"    # F
    .param p5, "descent"    # F
    .param p6, "diff"    # [C
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Float;",
            ">;IFFF[C)",
            "Lorg/icepdf/core/pobjects/fonts/FontFile;"
        }
    .end annotation

    .prologue
    .line 245
    .local p1, "widths":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Ljava/lang/Float;>;"
    new-instance v0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;

    invoke-direct {v0, p0}, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;-><init>(Lorg/icepdf/core/pobjects/fonts/ofont/OFont;)V

    .line 246
    .local v0, "font":Lorg/icepdf/core/pobjects/fonts/ofont/OFont;
    iget-object v1, p0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->echarAdvanceCache:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->clear()V

    .line 247
    iget v1, p0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->missingWidth:F

    iput v1, v0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->missingWidth:F

    .line 248
    iput p2, v0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->firstCh:I

    .line 249
    iput p4, v0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->ascent:F

    .line 250
    iput p5, v0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->descent:F

    .line 251
    iput-object p1, v0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->cidWidths:Ljava/util/Map;

    .line 252
    iput-object p6, v0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->cMap:[C

    .line 253
    return-object v0
.end method

.method public deriveFont(Lorg/icepdf/core/pobjects/fonts/Encoding;Lorg/icepdf/core/pobjects/fonts/CMap;)Lorg/icepdf/core/pobjects/fonts/FontFile;
    .locals 2
    .param p1, "encoding"    # Lorg/icepdf/core/pobjects/fonts/Encoding;
    .param p2, "toUnicode"    # Lorg/icepdf/core/pobjects/fonts/CMap;

    .prologue
    .line 223
    new-instance v0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;

    invoke-direct {v0, p0}, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;-><init>(Lorg/icepdf/core/pobjects/fonts/ofont/OFont;)V

    .line 224
    .local v0, "font":Lorg/icepdf/core/pobjects/fonts/ofont/OFont;
    iget-object v1, p0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->echarAdvanceCache:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->clear()V

    .line 225
    iput-object p1, v0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->encoding:Lorg/icepdf/core/pobjects/fonts/Encoding;

    .line 226
    iput-object p2, v0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->toUnicode:Lorg/icepdf/core/pobjects/fonts/CMap;

    .line 227
    return-object v0
.end method

.method public deriveFont([FIFFF[C)Lorg/icepdf/core/pobjects/fonts/FontFile;
    .locals 2
    .param p1, "widths"    # [F
    .param p2, "firstCh"    # I
    .param p3, "missingWidth"    # F
    .param p4, "ascent"    # F
    .param p5, "descent"    # F
    .param p6, "diff"    # [C

    .prologue
    .line 232
    new-instance v0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;

    invoke-direct {v0, p0}, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;-><init>(Lorg/icepdf/core/pobjects/fonts/ofont/OFont;)V

    .line 233
    .local v0, "font":Lorg/icepdf/core/pobjects/fonts/ofont/OFont;
    iget-object v1, p0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->echarAdvanceCache:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->clear()V

    .line 234
    iget v1, p0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->missingWidth:F

    iput v1, v0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->missingWidth:F

    .line 235
    iput p2, v0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->firstCh:I

    .line 236
    iput p4, v0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->ascent:F

    .line 237
    iput p5, v0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->descent:F

    .line 238
    iput-object p1, v0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->widths:[F

    .line 239
    iput-object p6, v0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->cMap:[C

    .line 240
    return-object v0
.end method

.method public drawEstring(Landroid/graphics/Canvas;Ljava/lang/String;FFJILorg/apache/poi/java/awt/Color;Landroid/graphics/Paint;)V
    .locals 7
    .param p1, "g"    # Landroid/graphics/Canvas;
    .param p2, "displayText"    # Ljava/lang/String;
    .param p3, "x"    # F
    .param p4, "y"    # F
    .param p5, "layout"    # J
    .param p7, "mode"    # I
    .param p8, "strokecolor"    # Lorg/apache/poi/java/awt/Color;
    .param p9, "paint"    # Landroid/graphics/Paint;

    .prologue
    .line 510
    invoke-virtual {p0, p2}, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->toUnicode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    .line 512
    if-eqz p7, :cond_0

    const/4 v0, 0x2

    if-eq v0, p7, :cond_0

    const/4 v0, 0x4

    if-eq v0, p7, :cond_0

    const/4 v0, 0x6

    if-ne v0, p7, :cond_2

    .line 516
    :cond_0
    if-eqz p9, :cond_1

    .line 517
    iget-object v0, p0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->androidFont:Landroid/text/TextPaint;

    invoke-virtual/range {p9 .. p9}, Landroid/graphics/Paint;->getColor()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setColor(I)V

    .line 518
    iget-object v0, p0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->androidFont:Landroid/text/TextPaint;

    invoke-virtual/range {p9 .. p9}, Landroid/graphics/Paint;->getStrokeWidth()F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setStrokeWidth(F)V

    .line 521
    :cond_1
    const/4 v2, 0x0

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v3

    iget-object v6, p0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->androidFont:Landroid/text/TextPaint;

    move-object v0, p1

    move-object v1, p2

    move v4, p3

    move v5, p4

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;IIFFLandroid/graphics/Paint;)V

    .line 524
    :cond_2
    const/4 v0, 0x1

    if-eq v0, p7, :cond_3

    const/4 v0, 0x2

    if-eq v0, p7, :cond_3

    const/4 v0, 0x5

    if-eq v0, p7, :cond_3

    const/4 v0, 0x6

    if-ne v0, p7, :cond_5

    .line 528
    :cond_3
    if-eqz p9, :cond_4

    .line 529
    iget-object v0, p0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->androidFont:Landroid/text/TextPaint;

    invoke-virtual/range {p9 .. p9}, Landroid/graphics/Paint;->getColor()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setColor(I)V

    .line 530
    iget-object v0, p0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->androidFont:Landroid/text/TextPaint;

    invoke-virtual/range {p9 .. p9}, Landroid/graphics/Paint;->getStrokeWidth()F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setStrokeWidth(F)V

    .line 533
    :cond_4
    const/4 v2, 0x0

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v3

    iget-object v6, p0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->androidFont:Landroid/text/TextPaint;

    move-object v0, p1

    move-object v1, p2

    move v4, p3

    move v5, p4

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;IIFFLandroid/graphics/Paint;)V

    .line 537
    :cond_5
    return-void
.end method

.method public echarAdvance(C)Landroid/graphics/PointF;
    .locals 10
    .param p1, "ech"    # C

    .prologue
    .line 338
    invoke-static {p1}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v6

    .line 339
    .local v6, "text":Ljava/lang/String;
    iget-object v8, p0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->echarAdvanceCache:Ljava/util/HashMap;

    invoke-virtual {v8, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/graphics/PointF;

    .line 342
    .local v5, "echarAdvance":Landroid/graphics/PointF;
    if-nez v5, :cond_1

    .line 348
    invoke-direct {p0, p1}, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->getCMapping(C)C

    move-result v4

    .line 350
    .local v4, "echGlyph":C
    iget-object v8, p0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->androidFont:Landroid/text/TextPaint;

    invoke-virtual {v8}, Landroid/text/TextPaint;->ascent()F

    move-result v2

    .line 351
    .local v2, "ascent":F
    iget-object v8, p0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->androidFont:Landroid/text/TextPaint;

    invoke-virtual {v8}, Landroid/text/TextPaint;->descent()F

    move-result v3

    .line 352
    .local v3, "descent":F
    const/4 v7, 0x0

    .line 354
    .local v7, "width":F
    iget-object v8, p0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->androidFont:Landroid/text/TextPaint;

    invoke-static {v4}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v7

    .line 372
    move v0, v7

    .line 373
    .local v0, "advance":F
    neg-float v8, v2

    add-float v1, v8, v3

    .line 375
    .local v1, "advanceY":F
    iget-object v8, p0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->echarAdvanceCache:Ljava/util/HashMap;

    new-instance v9, Landroid/graphics/PointF;

    invoke-direct {v9, v0, v1}, Landroid/graphics/PointF;-><init>(FF)V

    invoke-virtual {v8, v6, v9}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 385
    .end local v2    # "ascent":F
    .end local v3    # "descent":F
    .end local v4    # "echGlyph":C
    .end local v7    # "width":F
    :goto_0
    iget-object v8, p0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->widths:[F

    if-eqz v8, :cond_2

    iget v8, p0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->firstCh:I

    sub-int v8, p1, v8

    if-ltz v8, :cond_2

    iget v8, p0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->firstCh:I

    sub-int v8, p1, v8

    iget-object v9, p0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->widths:[F

    array-length v9, v9

    if-ge v8, v9, :cond_2

    .line 387
    iget-object v8, p0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->widths:[F

    iget v9, p0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->firstCh:I

    sub-int v9, p1, v9

    aget v8, v8, v9

    iget-object v9, p0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->androidFont:Landroid/text/TextPaint;

    invoke-virtual {v9}, Landroid/text/TextPaint;->getTextSize()F

    move-result v9

    mul-float v0, v8, v9

    .line 398
    :cond_0
    :goto_1
    new-instance v8, Landroid/graphics/PointF;

    invoke-direct {v8, v0, v1}, Landroid/graphics/PointF;-><init>(FF)V

    return-object v8

    .line 379
    .end local v0    # "advance":F
    .end local v1    # "advanceY":F
    :cond_1
    iget v0, v5, Landroid/graphics/PointF;->x:F

    .line 380
    .restart local v0    # "advance":F
    iget v1, v5, Landroid/graphics/PointF;->y:F

    .restart local v1    # "advanceY":F
    goto :goto_0

    .line 388
    :cond_2
    iget-object v8, p0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->cidWidths:Ljava/util/Map;

    if-eqz v8, :cond_3

    .line 389
    iget-object v8, p0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->cidWidths:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-interface {v8, v9}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Float;

    .line 390
    .local v7, "width":Ljava/lang/Float;
    if-eqz v7, :cond_0

    .line 391
    iget-object v8, p0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->cidWidths:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-interface {v8, v9}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Float;

    invoke-virtual {v8}, Ljava/lang/Float;->floatValue()F

    move-result v8

    iget-object v9, p0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->androidFont:Landroid/text/TextPaint;

    invoke-virtual {v9}, Landroid/text/TextPaint;->getTextSize()F

    move-result v9

    mul-float v0, v8, v9

    goto :goto_1

    .line 395
    .end local v7    # "width":Ljava/lang/Float;
    :cond_3
    iget v8, p0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->missingWidth:F

    const/4 v9, 0x0

    cmpl-float v8, v8, v9

    if-lez v8, :cond_0

    .line 396
    iget v8, p0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->missingWidth:F

    const/high16 v9, 0x447a0000    # 1000.0f

    div-float v0, v8, v9

    goto :goto_1
.end method

.method public getAscent()D
    .locals 2

    .prologue
    .line 467
    iget v0, p0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->ascent:F

    float-to-double v0, v0

    return-wide v0
.end method

.method public getCharDiff(C)C
    .locals 1
    .param p1, "character"    # C

    .prologue
    .line 424
    iget-object v0, p0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->cMap:[C

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->cMap:[C

    array-length v0, v0

    if-ge p1, v0, :cond_0

    .line 425
    iget-object v0, p0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->cMap:[C

    aget-char p1, v0, p1

    .line 427
    .end local p1    # "character":C
    :cond_0
    return p1
.end method

.method public getDescent()D
    .locals 2

    .prologue
    .line 471
    iget v0, p0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->descent:F

    float-to-double v0, v0

    return-wide v0
.end method

.method public getEstringBounds(Ljava/lang/String;II)Landroid/graphics/RectF;
    .locals 1
    .param p1, "estr"    # Ljava/lang/String;
    .param p2, "beginIndex"    # I
    .param p3, "limit"    # I

    .prologue
    .line 499
    const/4 v0, 0x0

    return-object v0
.end method

.method public getEstringOutline(Ljava/lang/String;FF)Landroid/graphics/Path;
    .locals 1
    .param p1, "displayText"    # Ljava/lang/String;
    .param p2, "x"    # F
    .param p3, "y"    # F

    .prologue
    .line 641
    invoke-virtual {p0, p1}, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->toUnicode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 666
    const/4 v0, 0x0

    return-object v0
.end method

.method public getFamily()Ljava/lang/String;
    .locals 1

    .prologue
    .line 459
    iget-object v0, p0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->family:Ljava/lang/String;

    return-object v0
.end method

.method public getFormat()Ljava/lang/String;
    .locals 1

    .prologue
    .line 503
    const/4 v0, 0x0

    return-object v0
.end method

.method public getMaxCharBounds()Landroid/graphics/RectF;
    .locals 1

    .prologue
    .line 475
    iget-object v0, p0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->maxCharBounds:Landroid/graphics/RectF;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 487
    iget-object v0, p0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getNumGlyphs()I
    .locals 1

    .prologue
    .line 451
    const/4 v0, 0x1

    return v0
.end method

.method public getRights()I
    .locals 1

    .prologue
    .line 483
    const/4 v0, 0x0

    return v0
.end method

.method public getSize()F
    .locals 1

    .prologue
    .line 463
    iget-object v0, p0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->androidFont:Landroid/text/TextPaint;

    invoke-virtual {v0}, Landroid/text/TextPaint;->getTextSize()F

    move-result v0

    return v0
.end method

.method public getSpaceEchar()C
    .locals 1

    .prologue
    .line 495
    const/16 v0, 0x20

    return v0
.end method

.method public getStyle()I
    .locals 1

    .prologue
    .line 455
    iget-object v0, p0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->androidTypeface:Landroid/graphics/Typeface;

    invoke-virtual {v0}, Landroid/graphics/Typeface;->getStyle()I

    move-result v0

    return v0
.end method

.method public getToUnicode()Lorg/icepdf/core/pobjects/fonts/CMap;
    .locals 1

    .prologue
    .line 446
    iget-object v0, p0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->toUnicode:Lorg/icepdf/core/pobjects/fonts/CMap;

    return-object v0
.end method

.method public getTransform()Landroid/graphics/Matrix;
    .locals 1

    .prologue
    .line 479
    sget-object v0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->at:Landroid/graphics/Matrix;

    return-object v0
.end method

.method public isHinted()Z
    .locals 1

    .prologue
    .line 491
    const/4 v0, 0x0

    return v0
.end method

.method public isOneByteEncoding()Z
    .locals 1

    .prologue
    .line 636
    const/4 v0, 0x0

    return v0
.end method

.method public toUnicode(C)Ljava/lang/String;
    .locals 3
    .param p1, "c1"    # C

    .prologue
    .line 582
    iget-object v2, p0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->toUnicode:Lorg/icepdf/core/pobjects/fonts/CMap;

    if-nez v2, :cond_1

    invoke-virtual {p0, p1}, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->getCharDiff(C)C

    move-result v1

    .line 589
    .local v1, "c":C
    :goto_0
    iget-object v2, p0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->toUnicode:Lorg/icepdf/core/pobjects/fonts/CMap;

    if-eqz v2, :cond_2

    .line 590
    iget-object v2, p0, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->toUnicode:Lorg/icepdf/core/pobjects/fonts/CMap;

    invoke-interface {v2, v1}, Lorg/icepdf/core/pobjects/fonts/CMap;->toUnicode(C)Ljava/lang/String;

    move-result-object v0

    .line 632
    :cond_0
    :goto_1
    return-object v0

    .end local v1    # "c":C
    :cond_1
    move v1, p1

    .line 582
    goto :goto_0

    .line 593
    .restart local v1    # "c":C
    :cond_2
    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/samsung/thumbnail/util/Utils;->getUnicodeForBullet(IZ)Ljava/lang/String;

    move-result-object v0

    .line 595
    .local v0, "bulletUnicode":Ljava/lang/String;
    invoke-static {v0}, Lcom/samsung/thumbnail/util/Utils;->isValidInteger(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 599
    invoke-direct {p0, v1}, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->getCMapping(C)C

    move-result v1

    .line 632
    invoke-static {v1}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method public toUnicode(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p1, "displayText"    # Ljava/lang/String;

    .prologue
    .line 545
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 546
    .local v3, "sb":Ljava/lang/StringBuilder;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    if-ge v2, v4, :cond_0

    .line 548
    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 549
    .local v0, "displayChar":C
    move v1, v0

    .line 569
    .local v1, "displayCharCode":I
    invoke-virtual {p0, v0}, Lorg/icepdf/core/pobjects/fonts/ofont/OFont;->toUnicode(C)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 546
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 573
    .end local v0    # "displayChar":C
    .end local v1    # "displayCharCode":I
    :cond_0
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4
.end method
