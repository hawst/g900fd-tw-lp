.class public abstract Lorg/icepdf/core/pobjects/fonts/Font;
.super Lorg/icepdf/core/pobjects/Dictionary;
.source "Font.java"


# static fields
.field public static final CID_FORMAT:I = 0x2

.field public static final SIMPLE_FORMAT:I = 0x1


# instance fields
.field protected basefont:Ljava/lang/String;

.field protected firstchar:I

.field protected font:Lorg/icepdf/core/pobjects/fonts/FontFile;

.field protected fontDescriptor:Lorg/icepdf/core/pobjects/fonts/FontDescriptor;

.field protected inited:Z

.field protected isAFMFont:Z

.field protected isFontSubstitution:Z

.field protected isVerticalWriting:Z

.field protected name:Ljava/lang/String;

.field protected subTypeFormat:I

.field protected subtype:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V
    .locals 5
    .param p1, "library"    # Lorg/icepdf/core/util/Library;
    .param p2, "entries"    # Ljava/util/Hashtable;

    .prologue
    const/4 v1, 0x1

    .line 179
    invoke-direct {p0, p1, p2}, Lorg/icepdf/core/pobjects/Dictionary;-><init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V

    .line 89
    iput v1, p0, Lorg/icepdf/core/pobjects/fonts/Font;->subTypeFormat:I

    .line 95
    const/16 v2, 0x20

    iput v2, p0, Lorg/icepdf/core/pobjects/fonts/Font;->firstchar:I

    .line 182
    const-string/jumbo v2, "Name"

    invoke-virtual {p1, p2, v2}, Lorg/icepdf/core/util/Library;->getName(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lorg/icepdf/core/pobjects/fonts/Font;->name:Ljava/lang/String;

    .line 185
    const-string/jumbo v2, "Subtype"

    invoke-virtual {p1, p2, v2}, Lorg/icepdf/core/util/Library;->getName(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lorg/icepdf/core/pobjects/fonts/Font;->subtype:Ljava/lang/String;

    .line 188
    iget-object v2, p0, Lorg/icepdf/core/pobjects/fonts/Font;->subtype:Ljava/lang/String;

    const-string/jumbo v3, "type0"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    iget-object v2, p0, Lorg/icepdf/core/pobjects/fonts/Font;->subtype:Ljava/lang/String;

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v2, v4}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v4, "cid"

    invoke-virtual {v2, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    const/4 v4, -0x1

    if-eq v2, v4, :cond_4

    move v2, v1

    :goto_0
    or-int/2addr v2, v3

    if-eqz v2, :cond_0

    const/4 v1, 0x2

    :cond_0
    iput v1, p0, Lorg/icepdf/core/pobjects/fonts/Font;->subTypeFormat:I

    .line 193
    const-string/jumbo v1, "Serif"

    iput-object v1, p0, Lorg/icepdf/core/pobjects/fonts/Font;->basefont:Ljava/lang/String;

    .line 194
    const-string/jumbo v1, "BaseFont"

    invoke-virtual {p2, v1}, Ljava/util/Hashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    new-instance v1, Lorg/icepdf/core/pobjects/Name;

    const-string/jumbo v2, "BaseFont"

    invoke-direct {v1, v2}, Lorg/icepdf/core/pobjects/Name;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2, v1}, Ljava/util/Hashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 195
    :cond_1
    const-string/jumbo v1, "BaseFont"

    invoke-virtual {p2, v1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 196
    .local v0, "o":Ljava/lang/Object;
    if-nez v0, :cond_2

    .line 198
    new-instance v1, Lorg/icepdf/core/pobjects/Name;

    const-string/jumbo v2, "BaseFont"

    invoke-direct {v1, v2}, Lorg/icepdf/core/pobjects/Name;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2, v1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 200
    :cond_2
    instance-of v1, v0, Lorg/icepdf/core/pobjects/Name;

    if-eqz v1, :cond_3

    .line 201
    check-cast v0, Lorg/icepdf/core/pobjects/Name;

    .end local v0    # "o":Ljava/lang/Object;
    invoke-virtual {v0}, Lorg/icepdf/core/pobjects/Name;->getName()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lorg/icepdf/core/pobjects/fonts/Font;->basefont:Ljava/lang/String;

    .line 204
    :cond_3
    return-void

    .line 188
    :cond_4
    const/4 v2, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getFont()Lorg/icepdf/core/pobjects/fonts/FontFile;
    .locals 1

    .prologue
    .line 263
    iget-object v0, p0, Lorg/icepdf/core/pobjects/fonts/Font;->font:Lorg/icepdf/core/pobjects/fonts/FontFile;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 235
    iget-object v0, p0, Lorg/icepdf/core/pobjects/fonts/Font;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getSubType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 244
    iget-object v0, p0, Lorg/icepdf/core/pobjects/fonts/Font;->subtype:Ljava/lang/String;

    return-object v0
.end method

.method public getSubTypeFormat()I
    .locals 1

    .prologue
    .line 253
    iget v0, p0, Lorg/icepdf/core/pobjects/fonts/Font;->subTypeFormat:I

    return v0
.end method

.method public abstract init()V
.end method

.method public isFontSubstitution()Z
    .locals 1

    .prologue
    .line 286
    iget-boolean v0, p0, Lorg/icepdf/core/pobjects/fonts/Font;->isFontSubstitution:Z

    return v0
.end method

.method public isVerticalWriting()Z
    .locals 1

    .prologue
    .line 272
    iget-boolean v0, p0, Lorg/icepdf/core/pobjects/fonts/Font;->isVerticalWriting:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 312
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lorg/icepdf/core/pobjects/fonts/Font;->getPObjectReference()Lorg/icepdf/core/pobjects/Reference;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " FONT= "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/icepdf/core/pobjects/fonts/Font;->basefont:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/icepdf/core/pobjects/fonts/Font;->entries:Ljava/util/Hashtable;

    invoke-virtual {v1}, Ljava/util/Hashtable;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
