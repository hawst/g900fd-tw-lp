.class public Lorg/icepdf/core/pobjects/Form;
.super Lorg/icepdf/core/pobjects/Stream;
.source "Form.java"


# static fields
.field private static final logger:Ljava/util/logging/Logger;


# instance fields
.field private bbox:Landroid/graphics/RectF;

.field private graphicsState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

.field private inited:Z

.field private isolated:Z

.field private knockOut:Z

.field private matrix:Landroid/graphics/Matrix;

.field private parentResource:Lorg/icepdf/core/pobjects/Resources;

.field private resources:Lorg/icepdf/core/pobjects/Resources;

.field private shapes:Lorg/icepdf/core/pobjects/graphics/Shapes;

.field private transparencyGroup:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 43
    const-class v0, Lorg/icepdf/core/pobjects/Form;

    invoke-virtual {v0}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Lorg/icepdf/core/pobjects/Form;->logger:Ljava/util/logging/Logger;

    return-void
.end method

.method public constructor <init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;Lorg/icepdf/core/io/SeekableInputConstrainedWrapper;)V
    .locals 4
    .param p1, "l"    # Lorg/icepdf/core/util/Library;
    .param p2, "h"    # Ljava/util/Hashtable;
    .param p3, "streamInputWrapper"    # Lorg/icepdf/core/io/SeekableInputConstrainedWrapper;

    .prologue
    .line 66
    invoke-direct {p0, p1, p2, p3}, Lorg/icepdf/core/pobjects/Stream;-><init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;Lorg/icepdf/core/io/SeekableInputConstrainedWrapper;)V

    .line 46
    new-instance v1, Landroid/graphics/Matrix;

    invoke-direct {v1}, Landroid/graphics/Matrix;-><init>()V

    iput-object v1, p0, Lorg/icepdf/core/pobjects/Form;->matrix:Landroid/graphics/Matrix;

    .line 57
    const/4 v1, 0x0

    iput-boolean v1, p0, Lorg/icepdf/core/pobjects/Form;->inited:Z

    .line 70
    iget-object v1, p0, Lorg/icepdf/core/pobjects/Form;->library:Lorg/icepdf/core/util/Library;

    iget-object v2, p0, Lorg/icepdf/core/pobjects/Form;->entries:Ljava/util/Hashtable;

    const-string/jumbo v3, "Group"

    invoke-virtual {v1, v2, v3}, Lorg/icepdf/core/util/Library;->getDictionary(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/util/Hashtable;

    move-result-object v0

    .line 71
    .local v0, "group":Ljava/util/Hashtable;
    if-eqz v0, :cond_0

    .line 72
    const/4 v1, 0x1

    iput-boolean v1, p0, Lorg/icepdf/core/pobjects/Form;->transparencyGroup:Z

    .line 73
    iget-object v1, p0, Lorg/icepdf/core/pobjects/Form;->library:Lorg/icepdf/core/util/Library;

    const-string/jumbo v2, "I"

    invoke-virtual {v1, v0, v2}, Lorg/icepdf/core/util/Library;->getBoolean(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    iput-boolean v1, p0, Lorg/icepdf/core/pobjects/Form;->isolated:Z

    .line 74
    iget-object v1, p0, Lorg/icepdf/core/pobjects/Form;->library:Lorg/icepdf/core/util/Library;

    const-string/jumbo v2, "K"

    invoke-virtual {v1, v0, v2}, Lorg/icepdf/core/util/Library;->getBoolean(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    iput-boolean v1, p0, Lorg/icepdf/core/pobjects/Form;->knockOut:Z

    .line 76
    :cond_0
    return-void
.end method

.method private static getAffineTransform(Ljava/util/Vector;)Landroid/graphics/Matrix;
    .locals 5
    .param p0, "v"    # Ljava/util/Vector;

    .prologue
    const/4 v4, 0x6

    .line 157
    new-array v0, v4, [F

    .line 158
    .local v0, "f":[F
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v4, :cond_0

    .line 159
    invoke-virtual {p0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Number;

    invoke-virtual {v3}, Ljava/lang/Number;->floatValue()F

    move-result v3

    aput v3, v0, v1

    .line 158
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 162
    :cond_0
    invoke-static {v0}, Lcom/samsung/thumbnail/util/Utils;->createMatrixFromArray([F)Landroid/graphics/Matrix;

    move-result-object v2

    .line 164
    .local v2, "m":Landroid/graphics/Matrix;
    return-object v2
.end method


# virtual methods
.method public completed()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 126
    iget-object v0, p0, Lorg/icepdf/core/pobjects/Form;->resources:Lorg/icepdf/core/pobjects/Resources;

    if-eqz v0, :cond_0

    .line 127
    iget-object v0, p0, Lorg/icepdf/core/pobjects/Form;->resources:Lorg/icepdf/core/pobjects/Resources;

    invoke-virtual {v0, p0}, Lorg/icepdf/core/pobjects/Resources;->removeReference(Ljava/lang/Object;)V

    .line 128
    iput-object v1, p0, Lorg/icepdf/core/pobjects/Form;->resources:Lorg/icepdf/core/pobjects/Resources;

    .line 130
    :cond_0
    iput-object v1, p0, Lorg/icepdf/core/pobjects/Form;->parentResource:Lorg/icepdf/core/pobjects/Resources;

    .line 131
    iput-object v1, p0, Lorg/icepdf/core/pobjects/Form;->graphicsState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    .line 132
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/icepdf/core/pobjects/Form;->inited:Z

    .line 133
    return-void
.end method

.method public dispose(Z)V
    .locals 2
    .param p1, "cache"    # Z

    .prologue
    .line 84
    iget-object v0, p0, Lorg/icepdf/core/pobjects/Form;->shapes:Lorg/icepdf/core/pobjects/graphics/Shapes;

    if-eqz v0, :cond_0

    .line 85
    iget-object v0, p0, Lorg/icepdf/core/pobjects/Form;->shapes:Lorg/icepdf/core/pobjects/graphics/Shapes;

    invoke-virtual {v0}, Lorg/icepdf/core/pobjects/graphics/Shapes;->dispose()V

    .line 87
    :cond_0
    if-eqz p1, :cond_1

    .line 88
    iget-object v0, p0, Lorg/icepdf/core/pobjects/Form;->library:Lorg/icepdf/core/util/Library;

    invoke-virtual {p0}, Lorg/icepdf/core/pobjects/Form;->getPObjectReference()Lorg/icepdf/core/pobjects/Reference;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/icepdf/core/util/Library;->removeObject(Lorg/icepdf/core/pobjects/Reference;)V

    .line 91
    :cond_1
    invoke-virtual {p0, p1}, Lorg/icepdf/core/pobjects/Form;->disposeResources(Z)V

    .line 92
    return-void
.end method

.method public disposeResources(Z)V
    .locals 2
    .param p1, "cache"    # Z

    .prologue
    const/4 v1, 0x0

    .line 104
    iget-object v0, p0, Lorg/icepdf/core/pobjects/Form;->resources:Lorg/icepdf/core/pobjects/Resources;

    if-eqz v0, :cond_0

    .line 105
    iget-object v0, p0, Lorg/icepdf/core/pobjects/Form;->resources:Lorg/icepdf/core/pobjects/Resources;

    invoke-virtual {v0, p1, p0}, Lorg/icepdf/core/pobjects/Resources;->dispose(ZLorg/icepdf/core/pobjects/Dictionary;)Z

    .line 107
    :cond_0
    iget-object v0, p0, Lorg/icepdf/core/pobjects/Form;->parentResource:Lorg/icepdf/core/pobjects/Resources;

    if-eqz v0, :cond_1

    .line 109
    iput-object v1, p0, Lorg/icepdf/core/pobjects/Form;->parentResource:Lorg/icepdf/core/pobjects/Resources;

    .line 111
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/icepdf/core/pobjects/Form;->inited:Z

    .line 112
    iput-object v1, p0, Lorg/icepdf/core/pobjects/Form;->graphicsState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    .line 114
    invoke-super {p0, p1}, Lorg/icepdf/core/pobjects/Stream;->dispose(Z)V

    .line 115
    return-void
.end method

.method public getBBox()Landroid/graphics/RectF;
    .locals 1

    .prologue
    .line 240
    iget-object v0, p0, Lorg/icepdf/core/pobjects/Form;->bbox:Landroid/graphics/RectF;

    return-object v0
.end method

.method public getMatrix()Landroid/graphics/Matrix;
    .locals 1

    .prologue
    .line 251
    iget-object v0, p0, Lorg/icepdf/core/pobjects/Form;->matrix:Landroid/graphics/Matrix;

    return-object v0
.end method

.method public getShapes()Lorg/icepdf/core/pobjects/graphics/Shapes;
    .locals 1

    .prologue
    .line 231
    iget-object v0, p0, Lorg/icepdf/core/pobjects/Form;->shapes:Lorg/icepdf/core/pobjects/graphics/Shapes;

    return-object v0
.end method

.method public declared-synchronized init()V
    .locals 8

    .prologue
    .line 182
    monitor-enter p0

    :try_start_0
    iget-boolean v5, p0, Lorg/icepdf/core/pobjects/Form;->inited:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v5, :cond_0

    .line 223
    :goto_0
    monitor-exit p0

    return-void

    .line 185
    :cond_0
    :try_start_1
    iget-object v5, p0, Lorg/icepdf/core/pobjects/Form;->library:Lorg/icepdf/core/util/Library;

    iget-object v6, p0, Lorg/icepdf/core/pobjects/Form;->entries:Ljava/util/Hashtable;

    const-string/jumbo v7, "Matrix"

    invoke-virtual {v5, v6, v7}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Vector;

    .line 186
    .local v4, "v":Ljava/util/Vector;
    if-eqz v4, :cond_1

    .line 187
    invoke-static {v4}, Lorg/icepdf/core/pobjects/Form;->getAffineTransform(Ljava/util/Vector;)Landroid/graphics/Matrix;

    move-result-object v5

    iput-object v5, p0, Lorg/icepdf/core/pobjects/Form;->matrix:Landroid/graphics/Matrix;

    .line 189
    :cond_1
    iget-object v5, p0, Lorg/icepdf/core/pobjects/Form;->library:Lorg/icepdf/core/util/Library;

    iget-object v6, p0, Lorg/icepdf/core/pobjects/Form;->entries:Ljava/util/Hashtable;

    const-string/jumbo v7, "BBox"

    invoke-virtual {v5, v6, v7}, Lorg/icepdf/core/util/Library;->getRectangle(Ljava/util/Hashtable;Ljava/lang/String;)Landroid/graphics/RectF;

    move-result-object v5

    iput-object v5, p0, Lorg/icepdf/core/pobjects/Form;->bbox:Landroid/graphics/RectF;

    .line 191
    iget-object v5, p0, Lorg/icepdf/core/pobjects/Form;->library:Lorg/icepdf/core/util/Library;

    iget-object v6, p0, Lorg/icepdf/core/pobjects/Form;->entries:Ljava/util/Hashtable;

    const-string/jumbo v7, "Resources"

    invoke-virtual {v5, v6, v7}, Lorg/icepdf/core/util/Library;->getResources(Ljava/util/Hashtable;Ljava/lang/String;)Lorg/icepdf/core/pobjects/Resources;

    move-result-object v3

    .line 193
    .local v3, "leafResources":Lorg/icepdf/core/pobjects/Resources;
    if-eqz v3, :cond_3

    .line 194
    iput-object v3, p0, Lorg/icepdf/core/pobjects/Form;->resources:Lorg/icepdf/core/pobjects/Resources;

    .line 195
    iget-object v5, p0, Lorg/icepdf/core/pobjects/Form;->resources:Lorg/icepdf/core/pobjects/Resources;

    invoke-virtual {v5, p0}, Lorg/icepdf/core/pobjects/Resources;->addReference(Ljava/lang/Object;)V

    .line 201
    :goto_1
    new-instance v0, Lorg/icepdf/core/util/ContentParser;

    iget-object v5, p0, Lorg/icepdf/core/pobjects/Form;->library:Lorg/icepdf/core/util/Library;

    invoke-direct {v0, v5, v3}, Lorg/icepdf/core/util/ContentParser;-><init>(Lorg/icepdf/core/util/Library;Lorg/icepdf/core/pobjects/Resources;)V

    .line 202
    .local v0, "cp":Lorg/icepdf/core/util/ContentParser;
    iget-object v5, p0, Lorg/icepdf/core/pobjects/Form;->graphicsState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    invoke-virtual {v0, v5}, Lorg/icepdf/core/util/ContentParser;->setGraphicsState(Lorg/icepdf/core/pobjects/graphics/GraphicsState;)V

    .line 203
    invoke-virtual {p0}, Lorg/icepdf/core/pobjects/Form;->getInputStreamForDecodedStreamBytes()Ljava/io/InputStream;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v2

    .line 204
    .local v2, "in":Ljava/io/InputStream;
    if-eqz v2, :cond_2

    .line 206
    :try_start_2
    invoke-virtual {v0, v2}, Lorg/icepdf/core/util/ContentParser;->parse(Ljava/io/InputStream;)Lorg/icepdf/core/pobjects/graphics/Shapes;

    move-result-object v5

    iput-object v5, p0, Lorg/icepdf/core/pobjects/Form;->shapes:Lorg/icepdf/core/pobjects/graphics/Shapes;
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 215
    :try_start_3
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 222
    :cond_2
    :goto_2
    const/4 v5, 0x1

    :try_start_4
    iput-boolean v5, p0, Lorg/icepdf/core/pobjects/Form;->inited:Z
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 182
    .end local v0    # "cp":Lorg/icepdf/core/util/ContentParser;
    .end local v2    # "in":Ljava/io/InputStream;
    .end local v3    # "leafResources":Lorg/icepdf/core/pobjects/Resources;
    .end local v4    # "v":Ljava/util/Vector;
    :catchall_0
    move-exception v5

    monitor-exit p0

    throw v5

    .line 197
    .restart local v3    # "leafResources":Lorg/icepdf/core/pobjects/Resources;
    .restart local v4    # "v":Ljava/util/Vector;
    :cond_3
    :try_start_5
    iget-object v3, p0, Lorg/icepdf/core/pobjects/Form;->parentResource:Lorg/icepdf/core/pobjects/Resources;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_1

    .line 208
    .restart local v0    # "cp":Lorg/icepdf/core/util/ContentParser;
    .restart local v2    # "in":Ljava/io/InputStream;
    :catch_0
    move-exception v1

    .line 210
    .local v1, "e":Ljava/lang/Throwable;
    :try_start_6
    new-instance v5, Lorg/icepdf/core/pobjects/graphics/Shapes;

    invoke-direct {v5}, Lorg/icepdf/core/pobjects/graphics/Shapes;-><init>()V

    iput-object v5, p0, Lorg/icepdf/core/pobjects/Form;->shapes:Lorg/icepdf/core/pobjects/graphics/Shapes;

    .line 211
    sget-object v5, Lorg/icepdf/core/pobjects/Form;->logger:Ljava/util/logging/Logger;

    sget-object v6, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string/jumbo v7, "Error parsing Form content stream."

    invoke-virtual {v5, v6, v7, v1}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 215
    :try_start_7
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto :goto_2

    .line 217
    :catch_1
    move-exception v5

    goto :goto_2

    .line 214
    .end local v1    # "e":Ljava/lang/Throwable;
    :catchall_1
    move-exception v5

    .line 215
    :try_start_8
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_3
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 219
    :goto_3
    :try_start_9
    throw v5
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 217
    :catch_2
    move-exception v5

    goto :goto_2

    :catch_3
    move-exception v6

    goto :goto_3
.end method

.method public isIsolated()Z
    .locals 1

    .prologue
    .line 270
    iget-boolean v0, p0, Lorg/icepdf/core/pobjects/Form;->isolated:Z

    return v0
.end method

.method public isKnockOut()Z
    .locals 1

    .prologue
    .line 280
    iget-boolean v0, p0, Lorg/icepdf/core/pobjects/Form;->knockOut:Z

    return v0
.end method

.method public isTransparencyGroup()Z
    .locals 1

    .prologue
    .line 260
    iget-boolean v0, p0, Lorg/icepdf/core/pobjects/Form;->transparencyGroup:Z

    return v0
.end method

.method public setGraphicsState(Lorg/icepdf/core/pobjects/graphics/GraphicsState;)V
    .locals 0
    .param p1, "graphicsState"    # Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    .prologue
    .line 144
    if-eqz p1, :cond_0

    .line 145
    iput-object p1, p0, Lorg/icepdf/core/pobjects/Form;->graphicsState:Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    .line 147
    :cond_0
    return-void
.end method

.method public setParentResources(Lorg/icepdf/core/pobjects/Resources;)V
    .locals 0
    .param p1, "parentResource"    # Lorg/icepdf/core/pobjects/Resources;

    .prologue
    .line 175
    iput-object p1, p0, Lorg/icepdf/core/pobjects/Form;->parentResource:Lorg/icepdf/core/pobjects/Resources;

    .line 176
    return-void
.end method
