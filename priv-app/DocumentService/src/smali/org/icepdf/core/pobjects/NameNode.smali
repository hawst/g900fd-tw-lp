.class public Lorg/icepdf/core/pobjects/NameNode;
.super Lorg/icepdf/core/pobjects/Dictionary;
.source "NameNode.java"


# static fields
.field private static NOT_FOUND:Ljava/lang/Object;

.field private static NOT_FOUND_IS_GREATER:Ljava/lang/Object;

.field private static NOT_FOUND_IS_LESSER:Ljava/lang/Object;


# instance fields
.field private kidsNodes:Ljava/util/Vector;

.field private kidsReferences:Ljava/util/Vector;

.field private lowerLimit:Ljava/lang/String;

.field private namesAndValues:Ljava/util/Vector;

.field private namesAreDecrypted:Z

.field private upperLimit:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lorg/icepdf/core/pobjects/NameNode;->NOT_FOUND:Ljava/lang/Object;

    .line 30
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lorg/icepdf/core/pobjects/NameNode;->NOT_FOUND_IS_LESSER:Ljava/lang/Object;

    .line 31
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lorg/icepdf/core/pobjects/NameNode;->NOT_FOUND_IS_GREATER:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V
    .locals 8
    .param p1, "l"    # Lorg/icepdf/core/util/Library;
    .param p2, "h"    # Ljava/util/Hashtable;

    .prologue
    const/4 v7, 0x0

    .line 45
    invoke-direct {p0, p1, p2}, Lorg/icepdf/core/pobjects/Dictionary;-><init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V

    .line 46
    iget-object v4, p0, Lorg/icepdf/core/pobjects/NameNode;->library:Lorg/icepdf/core/util/Library;

    iget-object v5, p0, Lorg/icepdf/core/pobjects/NameNode;->entries:Ljava/util/Hashtable;

    const-string/jumbo v6, "Kids"

    invoke-virtual {v4, v5, v6}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    .line 47
    .local v1, "o":Ljava/lang/Object;
    if-eqz v1, :cond_0

    instance-of v4, v1, Ljava/util/Vector;

    if-eqz v4, :cond_0

    move-object v3, v1

    .line 48
    check-cast v3, Ljava/util/Vector;

    .line 49
    .local v3, "v":Ljava/util/Vector;
    iput-object v3, p0, Lorg/icepdf/core/pobjects/NameNode;->kidsReferences:Ljava/util/Vector;

    .line 50
    iget-object v4, p0, Lorg/icepdf/core/pobjects/NameNode;->kidsReferences:Ljava/util/Vector;

    invoke-virtual {v4}, Ljava/util/Vector;->size()I

    move-result v2

    .line 51
    .local v2, "sz":I
    if-lez v2, :cond_0

    .line 52
    new-instance v4, Ljava/util/Vector;

    invoke-direct {v4, v2}, Ljava/util/Vector;-><init>(I)V

    iput-object v4, p0, Lorg/icepdf/core/pobjects/NameNode;->kidsNodes:Ljava/util/Vector;

    .line 53
    iget-object v4, p0, Lorg/icepdf/core/pobjects/NameNode;->kidsNodes:Ljava/util/Vector;

    invoke-virtual {v4, v2}, Ljava/util/Vector;->setSize(I)V

    .line 56
    .end local v2    # "sz":I
    .end local v3    # "v":Ljava/util/Vector;
    :cond_0
    iput-boolean v7, p0, Lorg/icepdf/core/pobjects/NameNode;->namesAreDecrypted:Z

    .line 57
    iget-object v4, p0, Lorg/icepdf/core/pobjects/NameNode;->library:Lorg/icepdf/core/util/Library;

    iget-object v5, p0, Lorg/icepdf/core/pobjects/NameNode;->entries:Ljava/util/Hashtable;

    const-string/jumbo v6, "Names"

    invoke-virtual {v4, v5, v6}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    .line 58
    if-eqz v1, :cond_1

    instance-of v4, v1, Ljava/util/Vector;

    if-eqz v4, :cond_1

    .line 59
    check-cast v1, Ljava/util/Vector;

    .end local v1    # "o":Ljava/lang/Object;
    iput-object v1, p0, Lorg/icepdf/core/pobjects/NameNode;->namesAndValues:Ljava/util/Vector;

    .line 61
    :cond_1
    iget-object v4, p0, Lorg/icepdf/core/pobjects/NameNode;->library:Lorg/icepdf/core/util/Library;

    iget-object v5, p0, Lorg/icepdf/core/pobjects/NameNode;->entries:Ljava/util/Hashtable;

    const-string/jumbo v6, "Limits"

    invoke-virtual {v4, v5, v6}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    .line 62
    .restart local v1    # "o":Ljava/lang/Object;
    if-eqz v1, :cond_2

    instance-of v4, v1, Ljava/util/Vector;

    if-eqz v4, :cond_2

    move-object v0, v1

    .line 63
    check-cast v0, Ljava/util/Vector;

    .line 64
    .local v0, "limits":Ljava/util/Vector;
    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v4

    const/4 v5, 0x2

    if-lt v4, v5, :cond_2

    .line 65
    invoke-virtual {v0, v7}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-direct {p0, v4}, Lorg/icepdf/core/pobjects/NameNode;->decryptIfText(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lorg/icepdf/core/pobjects/NameNode;->lowerLimit:Ljava/lang/String;

    .line 66
    const/4 v4, 0x1

    invoke-virtual {v0, v4}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-direct {p0, v4}, Lorg/icepdf/core/pobjects/NameNode;->decryptIfText(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lorg/icepdf/core/pobjects/NameNode;->upperLimit:Ljava/lang/String;

    .line 69
    .end local v0    # "limits":Ljava/util/Vector;
    :cond_2
    return-void
.end method

.method private binarySearchKids(IILjava/lang/String;)Ljava/lang/Object;
    .locals 5
    .param p1, "firstIndex"    # I
    .param p2, "lastIndex"    # I
    .param p3, "name"    # Ljava/lang/String;

    .prologue
    .line 213
    if-le p1, p2, :cond_1

    .line 214
    sget-object v3, Lorg/icepdf/core/pobjects/NameNode;->NOT_FOUND:Ljava/lang/Object;

    .line 237
    :cond_0
    :goto_0
    return-object v3

    .line 215
    :cond_1
    sub-int v4, p2, p1

    div-int/lit8 v4, v4, 0x2

    add-int v1, p1, v4

    .line 216
    .local v1, "pivot":I
    invoke-virtual {p0, v1}, Lorg/icepdf/core/pobjects/NameNode;->getNode(I)Lorg/icepdf/core/pobjects/NameNode;

    move-result-object v4

    invoke-direct {v4, p3}, Lorg/icepdf/core/pobjects/NameNode;->search(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    .line 218
    .local v3, "ret":Ljava/lang/Object;
    sget-object v4, Lorg/icepdf/core/pobjects/NameNode;->NOT_FOUND_IS_LESSER:Ljava/lang/Object;

    if-ne v3, v4, :cond_2

    .line 220
    add-int/lit8 v4, v1, -0x1

    invoke-direct {p0, p1, v4, p3}, Lorg/icepdf/core/pobjects/NameNode;->binarySearchKids(IILjava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    goto :goto_0

    .line 221
    :cond_2
    sget-object v4, Lorg/icepdf/core/pobjects/NameNode;->NOT_FOUND_IS_GREATER:Ljava/lang/Object;

    if-ne v3, v4, :cond_3

    .line 223
    add-int/lit8 v4, v1, 0x1

    invoke-direct {p0, v4, p2, p3}, Lorg/icepdf/core/pobjects/NameNode;->binarySearchKids(IILjava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    goto :goto_0

    .line 224
    :cond_3
    sget-object v4, Lorg/icepdf/core/pobjects/NameNode;->NOT_FOUND:Ljava/lang/Object;

    if-ne v3, v4, :cond_0

    .line 227
    move v0, p1

    .local v0, "i":I
    :goto_1
    if-gt v0, p2, :cond_0

    .line 228
    if-ne v0, v1, :cond_5

    .line 227
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 230
    :cond_5
    invoke-virtual {p0, v0}, Lorg/icepdf/core/pobjects/NameNode;->getNode(I)Lorg/icepdf/core/pobjects/NameNode;

    move-result-object v4

    invoke-direct {v4, p3}, Lorg/icepdf/core/pobjects/NameNode;->search(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    .line 231
    .local v2, "r":Ljava/lang/Object;
    sget-object v4, Lorg/icepdf/core/pobjects/NameNode;->NOT_FOUND:Ljava/lang/Object;

    if-eq v2, v4, :cond_4

    sget-object v4, Lorg/icepdf/core/pobjects/NameNode;->NOT_FOUND_IS_LESSER:Ljava/lang/Object;

    if-eq v2, v4, :cond_4

    sget-object v4, Lorg/icepdf/core/pobjects/NameNode;->NOT_FOUND_IS_GREATER:Ljava/lang/Object;

    if-eq v2, v4, :cond_4

    .line 232
    move-object v3, v2

    .line 233
    goto :goto_0
.end method

.method private binarySearchNames(IILjava/lang/String;)Ljava/lang/Object;
    .locals 5
    .param p1, "firstIndex"    # I
    .param p2, "lastIndex"    # I
    .param p3, "name"    # Ljava/lang/String;

    .prologue
    .line 241
    if-le p1, p2, :cond_1

    .line 242
    sget-object v1, Lorg/icepdf/core/pobjects/NameNode;->NOT_FOUND:Ljava/lang/Object;

    .line 260
    :cond_0
    :goto_0
    return-object v1

    .line 243
    :cond_1
    sub-int v3, p2, p1

    div-int/lit8 v3, v3, 0x2

    add-int v2, p1, v3

    .line 244
    .local v2, "pivot":I
    and-int/lit8 v2, v2, -0x2

    .line 246
    iget-object v3, p0, Lorg/icepdf/core/pobjects/NameNode;->namesAndValues:Ljava/util/Vector;

    invoke-virtual {v3, v2}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    .line 247
    .local v0, "cmp":I
    if-nez v0, :cond_2

    .line 249
    iget-object v3, p0, Lorg/icepdf/core/pobjects/NameNode;->namesAndValues:Ljava/util/Vector;

    add-int/lit8 v4, v2, 0x1

    invoke-virtual {v3, v4}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v1

    .line 250
    .local v1, "ob":Ljava/lang/Object;
    instance-of v3, v1, Lorg/icepdf/core/pobjects/Reference;

    if-eqz v3, :cond_0

    .line 251
    iget-object v3, p0, Lorg/icepdf/core/pobjects/NameNode;->library:Lorg/icepdf/core/util/Library;

    check-cast v1, Lorg/icepdf/core/pobjects/Reference;

    .end local v1    # "ob":Ljava/lang/Object;
    invoke-virtual {v3, v1}, Lorg/icepdf/core/util/Library;->getObject(Lorg/icepdf/core/pobjects/Reference;)Ljava/lang/Object;

    move-result-object v1

    .restart local v1    # "ob":Ljava/lang/Object;
    goto :goto_0

    .line 253
    .end local v1    # "ob":Ljava/lang/Object;
    :cond_2
    if-lez v0, :cond_3

    .line 255
    add-int/lit8 v3, v2, -0x1

    invoke-direct {p0, p1, v3, p3}, Lorg/icepdf/core/pobjects/NameNode;->binarySearchNames(IILjava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    goto :goto_0

    .line 256
    :cond_3
    if-gez v0, :cond_4

    .line 258
    add-int/lit8 v3, v2, 0x2

    invoke-direct {p0, v3, p2, p3}, Lorg/icepdf/core/pobjects/NameNode;->binarySearchNames(IILjava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    goto :goto_0

    .line 260
    :cond_4
    sget-object v1, Lorg/icepdf/core/pobjects/NameNode;->NOT_FOUND:Ljava/lang/Object;

    goto :goto_0
.end method

.method private decryptIfText(Ljava/lang/Object;)Ljava/lang/String;
    .locals 2
    .param p1, "tmp"    # Ljava/lang/Object;

    .prologue
    .line 121
    instance-of v1, p1, Lorg/icepdf/core/pobjects/StringObject;

    if-eqz v1, :cond_0

    move-object v0, p1

    .line 122
    check-cast v0, Lorg/icepdf/core/pobjects/StringObject;

    .line 123
    .local v0, "nameText":Lorg/icepdf/core/pobjects/StringObject;
    iget-object v1, p0, Lorg/icepdf/core/pobjects/NameNode;->library:Lorg/icepdf/core/util/Library;

    iget-object v1, v1, Lorg/icepdf/core/util/Library;->securityManager:Lorg/icepdf/core/pobjects/security/SecurityManager;

    invoke-interface {v0, v1}, Lorg/icepdf/core/pobjects/StringObject;->getDecryptedLiteralString(Lorg/icepdf/core/pobjects/security/SecurityManager;)Ljava/lang/String;

    move-result-object p1

    .line 128
    .end local v0    # "nameText":Lorg/icepdf/core/pobjects/StringObject;
    .end local p1    # "tmp":Ljava/lang/Object;
    :goto_0
    return-object p1

    .line 125
    .restart local p1    # "tmp":Ljava/lang/Object;
    :cond_0
    instance-of v1, p1, Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 126
    check-cast p1, Ljava/lang/String;

    goto :goto_0

    .line 128
    :cond_1
    const/4 p1, 0x0

    goto :goto_0
.end method

.method private ensureNamesDecrypted()V
    .locals 3

    .prologue
    .line 102
    iget-boolean v1, p0, Lorg/icepdf/core/pobjects/NameNode;->namesAreDecrypted:Z

    if-eqz v1, :cond_1

    .line 111
    :cond_0
    return-void

    .line 104
    :cond_1
    const/4 v1, 0x1

    iput-boolean v1, p0, Lorg/icepdf/core/pobjects/NameNode;->namesAreDecrypted:Z

    .line 107
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lorg/icepdf/core/pobjects/NameNode;->namesAndValues:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 108
    iget-object v1, p0, Lorg/icepdf/core/pobjects/NameNode;->namesAndValues:Ljava/util/Vector;

    iget-object v2, p0, Lorg/icepdf/core/pobjects/NameNode;->namesAndValues:Ljava/util/Vector;

    invoke-virtual {v2, v0}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-direct {p0, v2}, Lorg/icepdf/core/pobjects/NameNode;->decryptIfText(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/Vector;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 107
    add-int/lit8 v0, v0, 0x2

    goto :goto_0
.end method

.method private search(Ljava/lang/String;)Ljava/lang/Object;
    .locals 7
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x0

    .line 146
    iget-object v4, p0, Lorg/icepdf/core/pobjects/NameNode;->kidsReferences:Ljava/util/Vector;

    if-eqz v4, :cond_5

    .line 148
    iget-object v4, p0, Lorg/icepdf/core/pobjects/NameNode;->lowerLimit:Ljava/lang/String;

    if-eqz v4, :cond_2

    .line 149
    iget-object v4, p0, Lorg/icepdf/core/pobjects/NameNode;->lowerLimit:Ljava/lang/String;

    invoke-virtual {v4, p1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    .line 150
    .local v0, "cmp":I
    if-lez v0, :cond_1

    .line 152
    sget-object v2, Lorg/icepdf/core/pobjects/NameNode;->NOT_FOUND_IS_LESSER:Ljava/lang/Object;

    .line 209
    .end local v0    # "cmp":I
    :cond_0
    :goto_0
    return-object v2

    .line 153
    .restart local v0    # "cmp":I
    :cond_1
    if-nez v0, :cond_2

    .line 154
    invoke-virtual {p0, v6}, Lorg/icepdf/core/pobjects/NameNode;->getNode(I)Lorg/icepdf/core/pobjects/NameNode;

    move-result-object v4

    invoke-direct {v4, p1}, Lorg/icepdf/core/pobjects/NameNode;->search(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    goto :goto_0

    .line 156
    .end local v0    # "cmp":I
    :cond_2
    iget-object v4, p0, Lorg/icepdf/core/pobjects/NameNode;->upperLimit:Ljava/lang/String;

    if-eqz v4, :cond_4

    .line 157
    iget-object v4, p0, Lorg/icepdf/core/pobjects/NameNode;->upperLimit:Ljava/lang/String;

    invoke-virtual {v4, p1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    .line 158
    .restart local v0    # "cmp":I
    if-gez v0, :cond_3

    .line 160
    sget-object v2, Lorg/icepdf/core/pobjects/NameNode;->NOT_FOUND_IS_GREATER:Ljava/lang/Object;

    goto :goto_0

    .line 161
    :cond_3
    if-nez v0, :cond_4

    .line 162
    iget-object v4, p0, Lorg/icepdf/core/pobjects/NameNode;->kidsReferences:Ljava/util/Vector;

    invoke-virtual {v4}, Ljava/util/Vector;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {p0, v4}, Lorg/icepdf/core/pobjects/NameNode;->getNode(I)Lorg/icepdf/core/pobjects/NameNode;

    move-result-object v4

    invoke-direct {v4, p1}, Lorg/icepdf/core/pobjects/NameNode;->search(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    goto :goto_0

    .line 166
    .end local v0    # "cmp":I
    :cond_4
    iget-object v4, p0, Lorg/icepdf/core/pobjects/NameNode;->kidsReferences:Ljava/util/Vector;

    invoke-virtual {v4}, Ljava/util/Vector;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-direct {p0, v6, v4, p1}, Lorg/icepdf/core/pobjects/NameNode;->binarySearchKids(IILjava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    goto :goto_0

    .line 167
    :cond_5
    iget-object v4, p0, Lorg/icepdf/core/pobjects/NameNode;->namesAndValues:Ljava/util/Vector;

    if-eqz v4, :cond_c

    .line 169
    iget-object v4, p0, Lorg/icepdf/core/pobjects/NameNode;->namesAndValues:Ljava/util/Vector;

    invoke-virtual {v4}, Ljava/util/Vector;->size()I

    move-result v1

    .line 171
    .local v1, "numNamesAndValues":I
    iget-object v4, p0, Lorg/icepdf/core/pobjects/NameNode;->lowerLimit:Ljava/lang/String;

    if-eqz v4, :cond_7

    .line 172
    iget-object v4, p0, Lorg/icepdf/core/pobjects/NameNode;->lowerLimit:Ljava/lang/String;

    invoke-virtual {v4, p1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    .line 173
    .restart local v0    # "cmp":I
    if-lez v0, :cond_6

    .line 175
    sget-object v2, Lorg/icepdf/core/pobjects/NameNode;->NOT_FOUND_IS_LESSER:Ljava/lang/Object;

    goto :goto_0

    .line 176
    :cond_6
    if-nez v0, :cond_7

    .line 177
    invoke-direct {p0}, Lorg/icepdf/core/pobjects/NameNode;->ensureNamesDecrypted()V

    .line 178
    iget-object v4, p0, Lorg/icepdf/core/pobjects/NameNode;->namesAndValues:Ljava/util/Vector;

    invoke-virtual {v4, v6}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 179
    iget-object v4, p0, Lorg/icepdf/core/pobjects/NameNode;->namesAndValues:Ljava/util/Vector;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v2

    .line 180
    .local v2, "ob":Ljava/lang/Object;
    instance-of v4, v2, Lorg/icepdf/core/pobjects/Reference;

    if-eqz v4, :cond_0

    .line 181
    iget-object v4, p0, Lorg/icepdf/core/pobjects/NameNode;->library:Lorg/icepdf/core/util/Library;

    check-cast v2, Lorg/icepdf/core/pobjects/Reference;

    .end local v2    # "ob":Ljava/lang/Object;
    invoke-virtual {v4, v2}, Lorg/icepdf/core/util/Library;->getObject(Lorg/icepdf/core/pobjects/Reference;)Ljava/lang/Object;

    move-result-object v2

    .restart local v2    # "ob":Ljava/lang/Object;
    goto :goto_0

    .line 186
    .end local v0    # "cmp":I
    .end local v2    # "ob":Ljava/lang/Object;
    :cond_7
    iget-object v4, p0, Lorg/icepdf/core/pobjects/NameNode;->upperLimit:Ljava/lang/String;

    if-eqz v4, :cond_9

    .line 187
    iget-object v4, p0, Lorg/icepdf/core/pobjects/NameNode;->upperLimit:Ljava/lang/String;

    invoke-virtual {v4, p1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    .line 188
    .restart local v0    # "cmp":I
    if-gez v0, :cond_8

    .line 190
    sget-object v2, Lorg/icepdf/core/pobjects/NameNode;->NOT_FOUND_IS_GREATER:Ljava/lang/Object;

    goto/16 :goto_0

    .line 191
    :cond_8
    if-nez v0, :cond_9

    .line 192
    invoke-direct {p0}, Lorg/icepdf/core/pobjects/NameNode;->ensureNamesDecrypted()V

    .line 193
    iget-object v4, p0, Lorg/icepdf/core/pobjects/NameNode;->namesAndValues:Ljava/util/Vector;

    add-int/lit8 v5, v1, -0x2

    invoke-virtual {v4, v5}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_9

    .line 194
    iget-object v4, p0, Lorg/icepdf/core/pobjects/NameNode;->namesAndValues:Ljava/util/Vector;

    add-int/lit8 v5, v1, -0x1

    invoke-virtual {v4, v5}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v2

    .line 195
    .restart local v2    # "ob":Ljava/lang/Object;
    instance-of v4, v2, Lorg/icepdf/core/pobjects/Reference;

    if-eqz v4, :cond_0

    .line 196
    iget-object v4, p0, Lorg/icepdf/core/pobjects/NameNode;->library:Lorg/icepdf/core/util/Library;

    check-cast v2, Lorg/icepdf/core/pobjects/Reference;

    .end local v2    # "ob":Ljava/lang/Object;
    invoke-virtual {v4, v2}, Lorg/icepdf/core/util/Library;->getObject(Lorg/icepdf/core/pobjects/Reference;)Ljava/lang/Object;

    move-result-object v2

    .restart local v2    # "ob":Ljava/lang/Object;
    goto/16 :goto_0

    .line 203
    .end local v0    # "cmp":I
    .end local v2    # "ob":Ljava/lang/Object;
    :cond_9
    invoke-direct {p0}, Lorg/icepdf/core/pobjects/NameNode;->ensureNamesDecrypted()V

    .line 204
    add-int/lit8 v4, v1, -0x1

    invoke-direct {p0, v6, v4, p1}, Lorg/icepdf/core/pobjects/NameNode;->binarySearchNames(IILjava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    .line 205
    .local v3, "ret":Ljava/lang/Object;
    sget-object v4, Lorg/icepdf/core/pobjects/NameNode;->NOT_FOUND:Ljava/lang/Object;

    if-eq v3, v4, :cond_a

    sget-object v4, Lorg/icepdf/core/pobjects/NameNode;->NOT_FOUND_IS_LESSER:Ljava/lang/Object;

    if-eq v3, v4, :cond_a

    sget-object v4, Lorg/icepdf/core/pobjects/NameNode;->NOT_FOUND_IS_GREATER:Ljava/lang/Object;

    if-ne v3, v4, :cond_b

    .line 206
    :cond_a
    const/4 v3, 0x0

    .end local v3    # "ret":Ljava/lang/Object;
    :cond_b
    move-object v2, v3

    .line 207
    goto/16 :goto_0

    .line 209
    .end local v1    # "numNamesAndValues":I
    :cond_c
    const/4 v2, 0x0

    goto/16 :goto_0
.end method


# virtual methods
.method public dispose()V
    .locals 1

    .prologue
    .line 275
    iget-object v0, p0, Lorg/icepdf/core/pobjects/NameNode;->namesAndValues:Ljava/util/Vector;

    if-eqz v0, :cond_0

    .line 276
    iget-object v0, p0, Lorg/icepdf/core/pobjects/NameNode;->namesAndValues:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->clear()V

    .line 277
    iget-object v0, p0, Lorg/icepdf/core/pobjects/NameNode;->namesAndValues:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->trimToSize()V

    .line 279
    :cond_0
    iget-object v0, p0, Lorg/icepdf/core/pobjects/NameNode;->kidsReferences:Ljava/util/Vector;

    if-eqz v0, :cond_1

    .line 280
    iget-object v0, p0, Lorg/icepdf/core/pobjects/NameNode;->kidsReferences:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->clear()V

    .line 281
    iget-object v0, p0, Lorg/icepdf/core/pobjects/NameNode;->kidsReferences:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->trimToSize()V

    .line 283
    :cond_1
    iget-object v0, p0, Lorg/icepdf/core/pobjects/NameNode;->kidsNodes:Ljava/util/Vector;

    if-eqz v0, :cond_2

    .line 284
    iget-object v0, p0, Lorg/icepdf/core/pobjects/NameNode;->kidsNodes:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->clear()V

    .line 285
    iget-object v0, p0, Lorg/icepdf/core/pobjects/NameNode;->kidsNodes:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->trimToSize()V

    .line 287
    :cond_2
    return-void
.end method

.method public getKidsNodes()Ljava/util/Vector;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lorg/icepdf/core/pobjects/NameNode;->kidsNodes:Ljava/util/Vector;

    return-object v0
.end method

.method public getKidsReferences()Ljava/util/Vector;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lorg/icepdf/core/pobjects/NameNode;->kidsReferences:Ljava/util/Vector;

    return-object v0
.end method

.method public getLowerLimit()Ljava/lang/String;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lorg/icepdf/core/pobjects/NameNode;->lowerLimit:Ljava/lang/String;

    return-object v0
.end method

.method public getNamesAndValues()Ljava/util/Vector;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lorg/icepdf/core/pobjects/NameNode;->namesAndValues:Ljava/util/Vector;

    return-object v0
.end method

.method public getNode(I)Lorg/icepdf/core/pobjects/NameNode;
    .locals 4
    .param p1, "index"    # I

    .prologue
    .line 264
    iget-object v3, p0, Lorg/icepdf/core/pobjects/NameNode;->kidsNodes:Ljava/util/Vector;

    invoke-virtual {v3, p1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/icepdf/core/pobjects/NameNode;

    .line 265
    .local v0, "n":Lorg/icepdf/core/pobjects/NameNode;
    if-nez v0, :cond_0

    .line 266
    iget-object v3, p0, Lorg/icepdf/core/pobjects/NameNode;->kidsReferences:Ljava/util/Vector;

    invoke-virtual {v3, p1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/icepdf/core/pobjects/Reference;

    .line 267
    .local v2, "r":Lorg/icepdf/core/pobjects/Reference;
    iget-object v3, p0, Lorg/icepdf/core/pobjects/NameNode;->library:Lorg/icepdf/core/util/Library;

    invoke-virtual {v3, v2}, Lorg/icepdf/core/util/Library;->getObject(Lorg/icepdf/core/pobjects/Reference;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Hashtable;

    .line 268
    .local v1, "nh":Ljava/util/Hashtable;
    new-instance v0, Lorg/icepdf/core/pobjects/NameNode;

    .end local v0    # "n":Lorg/icepdf/core/pobjects/NameNode;
    iget-object v3, p0, Lorg/icepdf/core/pobjects/NameNode;->library:Lorg/icepdf/core/util/Library;

    invoke-direct {v0, v3, v1}, Lorg/icepdf/core/pobjects/NameNode;-><init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V

    .line 269
    .restart local v0    # "n":Lorg/icepdf/core/pobjects/NameNode;
    iget-object v3, p0, Lorg/icepdf/core/pobjects/NameNode;->kidsNodes:Ljava/util/Vector;

    invoke-virtual {v3, p1, v0}, Ljava/util/Vector;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 271
    .end local v1    # "nh":Ljava/util/Hashtable;
    .end local v2    # "r":Lorg/icepdf/core/pobjects/Reference;
    :cond_0
    return-object v0
.end method

.method public getUpperLimit()Ljava/lang/String;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lorg/icepdf/core/pobjects/NameNode;->upperLimit:Ljava/lang/String;

    return-object v0
.end method

.method public hasLimits()Z
    .locals 3

    .prologue
    .line 78
    iget-object v0, p0, Lorg/icepdf/core/pobjects/NameNode;->library:Lorg/icepdf/core/util/Library;

    iget-object v1, p0, Lorg/icepdf/core/pobjects/NameNode;->entries:Ljava/util/Hashtable;

    const-string/jumbo v2, "Limits"

    invoke-virtual {v0, v1, v2}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lorg/icepdf/core/pobjects/NameNode;->kidsNodes:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method searchName(Ljava/lang/String;)Ljava/lang/Object;
    .locals 2
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 136
    invoke-direct {p0, p1}, Lorg/icepdf/core/pobjects/NameNode;->search(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 137
    .local v0, "ret":Ljava/lang/Object;
    sget-object v1, Lorg/icepdf/core/pobjects/NameNode;->NOT_FOUND:Ljava/lang/Object;

    if-eq v0, v1, :cond_0

    sget-object v1, Lorg/icepdf/core/pobjects/NameNode;->NOT_FOUND_IS_LESSER:Ljava/lang/Object;

    if-eq v0, v1, :cond_0

    sget-object v1, Lorg/icepdf/core/pobjects/NameNode;->NOT_FOUND_IS_GREATER:Ljava/lang/Object;

    if-ne v0, v1, :cond_1

    .line 138
    :cond_0
    const/4 v0, 0x0

    .line 140
    .end local v0    # "ret":Ljava/lang/Object;
    :cond_1
    return-object v0
.end method
