.class public Lorg/icepdf/core/pobjects/functions/Function_3;
.super Lorg/icepdf/core/pobjects/functions/Function;
.source "Function_3.java"


# instance fields
.field private bounds:[F

.field private encode:[F

.field private functions:[Lorg/icepdf/core/pobjects/functions/Function;


# direct methods
.method constructor <init>(Lorg/icepdf/core/pobjects/Dictionary;)V
    .locals 7
    .param p1, "d"    # Lorg/icepdf/core/pobjects/Dictionary;

    .prologue
    .line 52
    invoke-direct {p0, p1}, Lorg/icepdf/core/pobjects/functions/Function;-><init>(Lorg/icepdf/core/pobjects/Dictionary;)V

    .line 55
    const-string/jumbo v4, "Bounds"

    invoke-virtual {p1, v4}, Lorg/icepdf/core/pobjects/Dictionary;->getObject(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Vector;

    .line 56
    .local v0, "boundTemp":Ljava/util/Vector;
    if-eqz v0, :cond_0

    .line 57
    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v4

    new-array v4, v4, [F

    iput-object v4, p0, Lorg/icepdf/core/pobjects/functions/Function_3;->bounds:[F

    .line 58
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v4

    if-ge v3, v4, :cond_0

    .line 59
    iget-object v5, p0, Lorg/icepdf/core/pobjects/functions/Function_3;->bounds:[F

    invoke-virtual {v0, v3}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Number;

    invoke-virtual {v4}, Ljava/lang/Number;->floatValue()F

    move-result v4

    aput v4, v5, v3

    .line 58
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 64
    .end local v3    # "i":I
    :cond_0
    const-string/jumbo v4, "Encode"

    invoke-virtual {p1, v4}, Lorg/icepdf/core/pobjects/Dictionary;->getObject(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Vector;

    .line 65
    .local v1, "encodeTemp":Ljava/util/Vector;
    if-eqz v1, :cond_1

    .line 66
    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v4

    new-array v4, v4, [F

    iput-object v4, p0, Lorg/icepdf/core/pobjects/functions/Function_3;->encode:[F

    .line 67
    const/4 v3, 0x0

    .restart local v3    # "i":I
    :goto_1
    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v4

    if-ge v3, v4, :cond_1

    .line 68
    iget-object v5, p0, Lorg/icepdf/core/pobjects/functions/Function_3;->encode:[F

    invoke-virtual {v1, v3}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Number;

    invoke-virtual {v4}, Ljava/lang/Number;->floatValue()F

    move-result v4

    aput v4, v5, v3

    .line 67
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 72
    .end local v3    # "i":I
    :cond_1
    const-string/jumbo v4, "Functions"

    invoke-virtual {p1, v4}, Lorg/icepdf/core/pobjects/Dictionary;->getObject(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Vector;

    .line 73
    .local v2, "functionTemp":Ljava/util/Vector;
    if-eqz v1, :cond_2

    .line 74
    invoke-virtual {v2}, Ljava/util/Vector;->size()I

    move-result v4

    new-array v4, v4, [Lorg/icepdf/core/pobjects/functions/Function;

    iput-object v4, p0, Lorg/icepdf/core/pobjects/functions/Function_3;->functions:[Lorg/icepdf/core/pobjects/functions/Function;

    .line 75
    const/4 v3, 0x0

    .restart local v3    # "i":I
    :goto_2
    invoke-virtual {v2}, Ljava/util/Vector;->size()I

    move-result v4

    if-ge v3, v4, :cond_2

    .line 76
    iget-object v4, p0, Lorg/icepdf/core/pobjects/functions/Function_3;->functions:[Lorg/icepdf/core/pobjects/functions/Function;

    invoke-virtual {p1}, Lorg/icepdf/core/pobjects/Dictionary;->getLibrary()Lorg/icepdf/core/util/Library;

    move-result-object v5

    invoke-virtual {v2, v3}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v6

    invoke-static {v5, v6}, Lorg/icepdf/core/pobjects/functions/Function;->getFunction(Lorg/icepdf/core/util/Library;Ljava/lang/Object;)Lorg/icepdf/core/pobjects/functions/Function;

    move-result-object v5

    aput-object v5, v4, v3

    .line 75
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 81
    .end local v3    # "i":I
    :cond_2
    return-void
.end method

.method private encode([FLorg/icepdf/core/pobjects/functions/Function;I)[F
    .locals 10
    .param p1, "x"    # [F
    .param p2, "function"    # Lorg/icepdf/core/pobjects/functions/Function;
    .param p3, "i"    # I

    .prologue
    const/4 v6, 0x1

    const/4 v9, 0x0

    .line 140
    iget-object v3, p0, Lorg/icepdf/core/pobjects/functions/Function_3;->functions:[Lorg/icepdf/core/pobjects/functions/Function;

    array-length v2, v3

    .line 142
    .local v2, "k":I
    if-gtz p3, :cond_3

    if-ge p3, v2, :cond_3

    iget-object v3, p0, Lorg/icepdf/core/pobjects/functions/Function_3;->bounds:[F

    array-length v3, v3

    if-lez v3, :cond_3

    .line 146
    add-int/lit8 v3, p3, -0x1

    const/4 v4, -0x1

    if-ne v3, v4, :cond_1

    .line 148
    iget-object v3, p0, Lorg/icepdf/core/pobjects/functions/Function_3;->domain:[F

    aget v0, v3, v9

    .line 152
    .local v0, "b1":F
    :goto_0
    add-int/lit8 v3, v2, -0x1

    if-ne p3, v3, :cond_2

    .line 154
    iget-object v3, p0, Lorg/icepdf/core/pobjects/functions/Function_3;->domain:[F

    aget v1, v3, v6

    .line 159
    .local v1, "b2":F
    :goto_1
    add-int/lit8 v3, v2, -0x2

    iget-object v4, p0, Lorg/icepdf/core/pobjects/functions/Function_3;->bounds:[F

    array-length v4, v4

    if-ge v3, v4, :cond_0

    iget-object v3, p0, Lorg/icepdf/core/pobjects/functions/Function_3;->bounds:[F

    add-int/lit8 v4, v2, -0x2

    aget v3, v3, v4

    iget-object v4, p0, Lorg/icepdf/core/pobjects/functions/Function_3;->domain:[F

    aget v4, v4, v6

    cmpl-float v3, v3, v4

    if-nez v3, :cond_0

    .line 160
    iget-object v3, p0, Lorg/icepdf/core/pobjects/functions/Function_3;->encode:[F

    mul-int/lit8 v4, p3, 0x2

    aget v3, v3, v4

    aput v3, p1, v9

    .line 163
    :cond_0
    aget v3, p1, v9

    iget-object v4, p0, Lorg/icepdf/core/pobjects/functions/Function_3;->encode:[F

    mul-int/lit8 v5, p3, 0x2

    aget v4, v4, v5

    iget-object v5, p0, Lorg/icepdf/core/pobjects/functions/Function_3;->encode:[F

    mul-int/lit8 v6, p3, 0x2

    add-int/lit8 v6, v6, 0x1

    aget v5, v5, v6

    invoke-static {v3, v0, v1, v4, v5}, Lorg/icepdf/core/pobjects/functions/Function_3;->interpolate(FFFFF)F

    move-result v3

    aput v3, p1, v9

    .line 165
    invoke-virtual {p2, p1}, Lorg/icepdf/core/pobjects/functions/Function;->calculate([F)[F

    move-result-object p1

    .line 173
    .end local v0    # "b1":F
    .end local v1    # "b2":F
    :goto_2
    if-eqz p1, :cond_4

    .line 174
    invoke-direct {p0, p1}, Lorg/icepdf/core/pobjects/functions/Function_3;->validateAgainstRange([F)[F

    move-result-object v3

    .line 176
    :goto_3
    return-object v3

    .line 150
    :cond_1
    iget-object v3, p0, Lorg/icepdf/core/pobjects/functions/Function_3;->bounds:[F

    add-int/lit8 v4, p3, -0x1

    aget v0, v3, v4

    .restart local v0    # "b1":F
    goto :goto_0

    .line 156
    :cond_2
    iget-object v3, p0, Lorg/icepdf/core/pobjects/functions/Function_3;->bounds:[F

    aget v1, v3, p3

    .restart local v1    # "b2":F
    goto :goto_1

    .line 167
    .end local v0    # "b1":F
    .end local v1    # "b2":F
    :cond_3
    aget v3, p1, v9

    iget-object v4, p0, Lorg/icepdf/core/pobjects/functions/Function_3;->domain:[F

    aget v4, v4, v9

    iget-object v5, p0, Lorg/icepdf/core/pobjects/functions/Function_3;->domain:[F

    aget v5, v5, v6

    iget-object v6, p0, Lorg/icepdf/core/pobjects/functions/Function_3;->encode:[F

    mul-int/lit8 v7, p3, 0x2

    aget v6, v6, v7

    iget-object v7, p0, Lorg/icepdf/core/pobjects/functions/Function_3;->encode:[F

    mul-int/lit8 v8, p3, 0x2

    add-int/lit8 v8, v8, 0x1

    aget v7, v7, v8

    invoke-static {v3, v4, v5, v6, v7}, Lorg/icepdf/core/pobjects/functions/Function_3;->interpolate(FFFFF)F

    move-result v3

    aput v3, p1, v9

    .line 168
    invoke-virtual {p2, p1}, Lorg/icepdf/core/pobjects/functions/Function;->calculate([F)[F

    move-result-object p1

    goto :goto_2

    .line 176
    :cond_4
    const/4 v3, 0x4

    new-array v3, v3, [F

    fill-array-data v3, :array_0

    goto :goto_3

    nop

    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
    .end array-data
.end method

.method private validateAgainstRange([F)[F
    .locals 6
    .param p1, "values"    # [F

    .prologue
    const/4 v5, 0x0

    .line 192
    const/4 v0, 0x0

    .local v0, "j":I
    array-length v1, p1

    .local v1, "max":I
    :goto_0
    if-ge v0, v1, :cond_3

    .line 193
    iget-object v2, p0, Lorg/icepdf/core/pobjects/functions/Function_3;->range:[F

    if-eqz v2, :cond_1

    aget v2, p1, v0

    iget-object v3, p0, Lorg/icepdf/core/pobjects/functions/Function_3;->range:[F

    mul-int/lit8 v4, v0, 0x2

    aget v3, v3, v4

    cmpg-float v2, v2, v3

    if-gez v2, :cond_1

    .line 194
    iget-object v2, p0, Lorg/icepdf/core/pobjects/functions/Function_3;->range:[F

    mul-int/lit8 v3, v0, 0x2

    aget v2, v2, v3

    aput v2, p1, v0

    .line 192
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 195
    :cond_1
    iget-object v2, p0, Lorg/icepdf/core/pobjects/functions/Function_3;->range:[F

    if-eqz v2, :cond_2

    aget v2, p1, v0

    iget-object v3, p0, Lorg/icepdf/core/pobjects/functions/Function_3;->range:[F

    mul-int/lit8 v4, v0, 0x2

    add-int/lit8 v4, v4, 0x1

    aget v3, v3, v4

    cmpl-float v2, v2, v3

    if-lez v2, :cond_2

    .line 196
    iget-object v2, p0, Lorg/icepdf/core/pobjects/functions/Function_3;->range:[F

    mul-int/lit8 v3, v0, 0x2

    add-int/lit8 v3, v3, 0x1

    aget v2, v2, v3

    aput v2, p1, v0

    goto :goto_1

    .line 197
    :cond_2
    aget v2, p1, v0

    cmpg-float v2, v2, v5

    if-gez v2, :cond_0

    .line 198
    aput v5, p1, v0

    goto :goto_1

    .line 201
    :cond_3
    return-object p1
.end method


# virtual methods
.method public calculate([F)[F
    .locals 7
    .param p1, "x"    # [F

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 91
    iget-object v2, p0, Lorg/icepdf/core/pobjects/functions/Function_3;->functions:[Lorg/icepdf/core/pobjects/functions/Function;

    array-length v1, v2

    .line 93
    .local v1, "k":I
    if-ne v1, v6, :cond_0

    iget-object v2, p0, Lorg/icepdf/core/pobjects/functions/Function_3;->bounds:[F

    array-length v2, v2

    if-nez v2, :cond_0

    .line 94
    iget-object v2, p0, Lorg/icepdf/core/pobjects/functions/Function_3;->domain:[F

    aget v2, v2, v5

    aget v3, p1, v5

    cmpg-float v2, v2, v3

    if-gtz v2, :cond_0

    aget v2, p1, v5

    iget-object v3, p0, Lorg/icepdf/core/pobjects/functions/Function_3;->domain:[F

    aget v3, v3, v6

    cmpg-float v2, v2, v3

    if-gtz v2, :cond_0

    .line 95
    iget-object v2, p0, Lorg/icepdf/core/pobjects/functions/Function_3;->functions:[Lorg/icepdf/core/pobjects/functions/Function;

    aget-object v2, v2, v5

    invoke-direct {p0, p1, v2, v5}, Lorg/icepdf/core/pobjects/functions/Function_3;->encode([FLorg/icepdf/core/pobjects/functions/Function;I)[F

    move-result-object v2

    .line 124
    :goto_0
    return-object v2

    .line 103
    :cond_0
    const/4 v0, 0x0

    .local v0, "b":I
    :goto_1
    iget-object v2, p0, Lorg/icepdf/core/pobjects/functions/Function_3;->bounds:[F

    array-length v2, v2

    if-ge v0, v2, :cond_4

    .line 105
    if-nez v0, :cond_1

    .line 107
    iget-object v2, p0, Lorg/icepdf/core/pobjects/functions/Function_3;->domain:[F

    aget v2, v2, v5

    aget v3, p1, v5

    cmpg-float v2, v2, v3

    if-gtz v2, :cond_1

    aget v2, p1, v5

    iget-object v3, p0, Lorg/icepdf/core/pobjects/functions/Function_3;->bounds:[F

    aget v3, v3, v0

    cmpg-float v2, v2, v3

    if-gez v2, :cond_1

    .line 108
    iget-object v2, p0, Lorg/icepdf/core/pobjects/functions/Function_3;->functions:[Lorg/icepdf/core/pobjects/functions/Function;

    aget-object v2, v2, v0

    invoke-direct {p0, p1, v2, v0}, Lorg/icepdf/core/pobjects/functions/Function_3;->encode([FLorg/icepdf/core/pobjects/functions/Function;I)[F

    move-result-object v2

    goto :goto_0

    .line 112
    :cond_1
    add-int/lit8 v2, v1, -0x2

    if-ne v0, v2, :cond_2

    .line 114
    iget-object v2, p0, Lorg/icepdf/core/pobjects/functions/Function_3;->bounds:[F

    aget v2, v2, v0

    aget v3, p1, v5

    cmpg-float v2, v2, v3

    if-gtz v2, :cond_2

    aget v2, p1, v5

    iget-object v3, p0, Lorg/icepdf/core/pobjects/functions/Function_3;->domain:[F

    aget v3, v3, v6

    cmpg-float v2, v2, v3

    if-gtz v2, :cond_2

    .line 115
    iget-object v2, p0, Lorg/icepdf/core/pobjects/functions/Function_3;->functions:[Lorg/icepdf/core/pobjects/functions/Function;

    add-int/lit8 v3, v1, -0x1

    aget-object v2, v2, v3

    add-int/lit8 v3, v1, -0x1

    invoke-direct {p0, p1, v2, v3}, Lorg/icepdf/core/pobjects/functions/Function_3;->encode([FLorg/icepdf/core/pobjects/functions/Function;I)[F

    move-result-object v2

    goto :goto_0

    .line 119
    :cond_2
    iget-object v2, p0, Lorg/icepdf/core/pobjects/functions/Function_3;->bounds:[F

    aget v2, v2, v0

    aget v3, p1, v5

    cmpg-float v2, v2, v3

    if-gtz v2, :cond_3

    aget v2, p1, v5

    iget-object v3, p0, Lorg/icepdf/core/pobjects/functions/Function_3;->bounds:[F

    add-int/lit8 v4, v0, 0x1

    aget v3, v3, v4

    cmpg-float v2, v2, v3

    if-gez v2, :cond_3

    .line 120
    iget-object v2, p0, Lorg/icepdf/core/pobjects/functions/Function_3;->functions:[Lorg/icepdf/core/pobjects/functions/Function;

    aget-object v2, v2, v0

    invoke-direct {p0, p1, v2, v0}, Lorg/icepdf/core/pobjects/functions/Function_3;->encode([FLorg/icepdf/core/pobjects/functions/Function;I)[F

    move-result-object v2

    goto :goto_0

    .line 103
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 124
    :cond_4
    const/4 v2, 0x0

    goto :goto_0
.end method
