.class public Lorg/icepdf/core/pobjects/ViewerPreferences;
.super Lorg/icepdf/core/pobjects/Dictionary;
.source "ViewerPreferences.java"


# instance fields
.field private root:Lorg/icepdf/core/pobjects/NameNode;


# direct methods
.method public constructor <init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V
    .locals 0
    .param p1, "l"    # Lorg/icepdf/core/util/Library;
    .param p2, "h"    # Ljava/util/Hashtable;

    .prologue
    .line 37
    invoke-direct {p0, p1, p2}, Lorg/icepdf/core/pobjects/Dictionary;-><init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V

    .line 38
    return-void
.end method


# virtual methods
.method public dispose()V
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lorg/icepdf/core/pobjects/ViewerPreferences;->root:Lorg/icepdf/core/pobjects/NameNode;

    invoke-virtual {v0}, Lorg/icepdf/core/pobjects/NameNode;->dispose()V

    .line 56
    return-void
.end method

.method public getFitWindow()Z
    .locals 3

    .prologue
    .line 83
    iget-object v0, p0, Lorg/icepdf/core/pobjects/ViewerPreferences;->library:Lorg/icepdf/core/util/Library;

    iget-object v1, p0, Lorg/icepdf/core/pobjects/ViewerPreferences;->entries:Ljava/util/Hashtable;

    const-string/jumbo v2, "FitWindow"

    invoke-virtual {v0, v1, v2}, Lorg/icepdf/core/util/Library;->getBoolean(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public getHideMenubar()Z
    .locals 3

    .prologue
    .line 79
    iget-object v0, p0, Lorg/icepdf/core/pobjects/ViewerPreferences;->library:Lorg/icepdf/core/util/Library;

    iget-object v1, p0, Lorg/icepdf/core/pobjects/ViewerPreferences;->entries:Ljava/util/Hashtable;

    const-string/jumbo v2, "HideMenubar"

    invoke-virtual {v0, v1, v2}, Lorg/icepdf/core/util/Library;->getBoolean(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public getHideToolbar()Z
    .locals 3

    .prologue
    .line 75
    iget-object v0, p0, Lorg/icepdf/core/pobjects/ViewerPreferences;->library:Lorg/icepdf/core/util/Library;

    iget-object v1, p0, Lorg/icepdf/core/pobjects/ViewerPreferences;->entries:Ljava/util/Hashtable;

    const-string/jumbo v2, "HideToolbar"

    invoke-virtual {v0, v1, v2}, Lorg/icepdf/core/util/Library;->getBoolean(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public getRoot()Lorg/icepdf/core/pobjects/NameNode;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lorg/icepdf/core/pobjects/ViewerPreferences;->root:Lorg/icepdf/core/pobjects/NameNode;

    return-object v0
.end method

.method public hasFitWindow()Z
    .locals 3

    .prologue
    .line 71
    iget-object v0, p0, Lorg/icepdf/core/pobjects/ViewerPreferences;->library:Lorg/icepdf/core/util/Library;

    iget-object v1, p0, Lorg/icepdf/core/pobjects/ViewerPreferences;->entries:Ljava/util/Hashtable;

    const-string/jumbo v2, "FitWindow"

    invoke-virtual {v0, v1, v2}, Lorg/icepdf/core/util/Library;->isValidEntry(Ljava/util/Hashtable;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public hasHideMenubar()Z
    .locals 3

    .prologue
    .line 67
    iget-object v0, p0, Lorg/icepdf/core/pobjects/ViewerPreferences;->library:Lorg/icepdf/core/util/Library;

    iget-object v1, p0, Lorg/icepdf/core/pobjects/ViewerPreferences;->entries:Ljava/util/Hashtable;

    const-string/jumbo v2, "HideMenubar"

    invoke-virtual {v0, v1, v2}, Lorg/icepdf/core/util/Library;->isValidEntry(Ljava/util/Hashtable;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public hasHideToolbar()Z
    .locals 3

    .prologue
    .line 63
    iget-object v0, p0, Lorg/icepdf/core/pobjects/ViewerPreferences;->library:Lorg/icepdf/core/util/Library;

    iget-object v1, p0, Lorg/icepdf/core/pobjects/ViewerPreferences;->entries:Ljava/util/Hashtable;

    const-string/jumbo v2, "HideToolbar"

    invoke-virtual {v0, v1, v2}, Lorg/icepdf/core/util/Library;->isValidEntry(Ljava/util/Hashtable;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public init()V
    .locals 3

    .prologue
    .line 44
    iget-boolean v0, p0, Lorg/icepdf/core/pobjects/ViewerPreferences;->inited:Z

    if-eqz v0, :cond_0

    .line 49
    :goto_0
    return-void

    .line 47
    :cond_0
    new-instance v0, Lorg/icepdf/core/pobjects/NameNode;

    iget-object v1, p0, Lorg/icepdf/core/pobjects/ViewerPreferences;->library:Lorg/icepdf/core/util/Library;

    iget-object v2, p0, Lorg/icepdf/core/pobjects/ViewerPreferences;->entries:Ljava/util/Hashtable;

    invoke-direct {v0, v1, v2}, Lorg/icepdf/core/pobjects/NameNode;-><init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V

    iput-object v0, p0, Lorg/icepdf/core/pobjects/ViewerPreferences;->root:Lorg/icepdf/core/pobjects/NameNode;

    .line 48
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/icepdf/core/pobjects/ViewerPreferences;->inited:Z

    goto :goto_0
.end method
