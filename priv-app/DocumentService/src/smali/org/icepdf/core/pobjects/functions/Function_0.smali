.class public Lorg/icepdf/core/pobjects/functions/Function_0;
.super Lorg/icepdf/core/pobjects/functions/Function;
.source "Function_0.java"


# static fields
.field private static final logger:Ljava/util/logging/Logger;


# instance fields
.field private bitspersample:I

.field private bytes:[B

.field private decode:[F

.field private encode:[F

.field private size:[I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 41
    const-class v0, Lorg/icepdf/core/pobjects/functions/Function_0;

    invoke-virtual {v0}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Lorg/icepdf/core/pobjects/functions/Function_0;->logger:Ljava/util/logging/Logger;

    return-void
.end method

.method constructor <init>(Lorg/icepdf/core/pobjects/Dictionary;)V
    .locals 9
    .param p1, "d"    # Lorg/icepdf/core/pobjects/Dictionary;

    .prologue
    const/4 v8, 0x0

    .line 77
    invoke-direct {p0, p1}, Lorg/icepdf/core/pobjects/functions/Function;-><init>(Lorg/icepdf/core/pobjects/Dictionary;)V

    .line 79
    const-string/jumbo v5, "Size"

    invoke-virtual {p1, v5}, Lorg/icepdf/core/pobjects/Dictionary;->getObject(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Vector;

    .line 82
    .local v3, "s":Ljava/util/Vector;
    invoke-virtual {v3}, Ljava/util/Vector;->size()I

    move-result v5

    new-array v5, v5, [I

    iput-object v5, p0, Lorg/icepdf/core/pobjects/functions/Function_0;->size:[I

    .line 83
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {v3}, Ljava/util/Vector;->size()I

    move-result v5

    if-ge v2, v5, :cond_0

    .line 84
    iget-object v6, p0, Lorg/icepdf/core/pobjects/functions/Function_0;->size:[I

    invoke-virtual {v3, v2}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Number;

    invoke-virtual {v5}, Ljava/lang/Number;->floatValue()F

    move-result v5

    float-to-int v5, v5

    aput v5, v6, v2

    .line 83
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 88
    :cond_0
    const-string/jumbo v5, "BitsPerSample"

    invoke-virtual {p1, v5}, Lorg/icepdf/core/pobjects/Dictionary;->getInt(Ljava/lang/String;)I

    move-result v5

    iput v5, p0, Lorg/icepdf/core/pobjects/functions/Function_0;->bitspersample:I

    .line 92
    const-string/jumbo v5, "Encode"

    invoke-virtual {p1, v5}, Lorg/icepdf/core/pobjects/Dictionary;->getObject(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Vector;

    .line 93
    .local v1, "enc":Ljava/util/Vector;
    iget-object v5, p0, Lorg/icepdf/core/pobjects/functions/Function_0;->size:[I

    array-length v5, v5

    mul-int/lit8 v5, v5, 0x2

    new-array v5, v5, [F

    iput-object v5, p0, Lorg/icepdf/core/pobjects/functions/Function_0;->encode:[F

    .line 94
    if-eqz v1, :cond_1

    .line 95
    const/4 v2, 0x0

    :goto_1
    iget-object v5, p0, Lorg/icepdf/core/pobjects/functions/Function_0;->size:[I

    array-length v5, v5

    mul-int/lit8 v5, v5, 0x2

    if-ge v2, v5, :cond_2

    .line 96
    iget-object v6, p0, Lorg/icepdf/core/pobjects/functions/Function_0;->encode:[F

    invoke-virtual {v1, v2}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Number;

    invoke-virtual {v5}, Ljava/lang/Number;->floatValue()F

    move-result v5

    aput v5, v6, v2

    .line 95
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 102
    :cond_1
    const/4 v2, 0x0

    :goto_2
    iget-object v5, p0, Lorg/icepdf/core/pobjects/functions/Function_0;->size:[I

    array-length v5, v5

    if-ge v2, v5, :cond_2

    .line 103
    iget-object v5, p0, Lorg/icepdf/core/pobjects/functions/Function_0;->encode:[F

    mul-int/lit8 v6, v2, 0x2

    const/4 v7, 0x0

    aput v7, v5, v6

    .line 104
    iget-object v5, p0, Lorg/icepdf/core/pobjects/functions/Function_0;->encode:[F

    mul-int/lit8 v6, v2, 0x2

    add-int/lit8 v6, v6, 0x1

    iget-object v7, p0, Lorg/icepdf/core/pobjects/functions/Function_0;->size:[I

    aget v7, v7, v2

    add-int/lit8 v7, v7, -0x1

    int-to-float v7, v7

    aput v7, v5, v6

    .line 102
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 110
    :cond_2
    const-string/jumbo v5, "Decode"

    invoke-virtual {p1, v5}, Lorg/icepdf/core/pobjects/Dictionary;->getObject(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Vector;

    .line 111
    .local v0, "dec":Ljava/util/Vector;
    iget-object v5, p0, Lorg/icepdf/core/pobjects/functions/Function_0;->range:[F

    array-length v5, v5

    new-array v5, v5, [F

    iput-object v5, p0, Lorg/icepdf/core/pobjects/functions/Function_0;->decode:[F

    .line 112
    if-eqz v0, :cond_3

    .line 113
    const/4 v2, 0x0

    :goto_3
    iget-object v5, p0, Lorg/icepdf/core/pobjects/functions/Function_0;->range:[F

    array-length v5, v5

    if-ge v2, v5, :cond_4

    .line 114
    iget-object v6, p0, Lorg/icepdf/core/pobjects/functions/Function_0;->decode:[F

    invoke-virtual {v0, v2}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Number;

    invoke-virtual {v5}, Ljava/lang/Number;->floatValue()F

    move-result v5

    aput v5, v6, v2

    .line 113
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 118
    :cond_3
    iget-object v5, p0, Lorg/icepdf/core/pobjects/functions/Function_0;->range:[F

    iget-object v6, p0, Lorg/icepdf/core/pobjects/functions/Function_0;->decode:[F

    iget-object v7, p0, Lorg/icepdf/core/pobjects/functions/Function_0;->range:[F

    array-length v7, v7

    invoke-static {v5, v8, v6, v8, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_4
    move-object v4, p1

    .line 125
    check-cast v4, Lorg/icepdf/core/pobjects/Stream;

    .line 126
    .local v4, "stream":Lorg/icepdf/core/pobjects/Stream;
    invoke-virtual {v4}, Lorg/icepdf/core/pobjects/Stream;->getBytes()[B

    move-result-object v5

    iput-object v5, p0, Lorg/icepdf/core/pobjects/functions/Function_0;->bytes:[B

    .line 127
    return-void
.end method


# virtual methods
.method public calculate([F)[F
    .locals 19
    .param p1, "x"    # [F

    .prologue
    .line 138
    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/icepdf/core/pobjects/functions/Function_0;->range:[F

    array-length v13, v13

    div-int/lit8 v10, v13, 0x2

    .line 140
    .local v10, "n":I
    new-array v12, v10, [F

    .line 143
    .local v12, "y":[F
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_0
    :try_start_0
    move-object/from16 v0, p1

    array-length v13, v0

    if-ge v7, v13, :cond_2

    .line 146
    aget v13, p1, v7

    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/icepdf/core/pobjects/functions/Function_0;->domain:[F

    mul-int/lit8 v15, v7, 0x2

    aget v14, v14, v15

    invoke-static {v13, v14}, Ljava/lang/Math;->max(FF)F

    move-result v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/icepdf/core/pobjects/functions/Function_0;->domain:[F

    mul-int/lit8 v15, v7, 0x2

    add-int/lit8 v15, v15, 0x1

    aget v14, v14, v15

    invoke-static {v13, v14}, Ljava/lang/Math;->min(FF)F

    move-result v13

    aput v13, p1, v7

    .line 149
    aget v13, p1, v7

    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/icepdf/core/pobjects/functions/Function_0;->domain:[F

    mul-int/lit8 v15, v7, 0x2

    aget v14, v14, v15

    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/icepdf/core/pobjects/functions/Function_0;->domain:[F

    mul-int/lit8 v16, v7, 0x2

    add-int/lit8 v16, v16, 0x1

    aget v15, v15, v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/core/pobjects/functions/Function_0;->encode:[F

    move-object/from16 v16, v0

    mul-int/lit8 v17, v7, 0x2

    aget v16, v16, v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/core/pobjects/functions/Function_0;->encode:[F

    move-object/from16 v17, v0

    mul-int/lit8 v18, v7, 0x2

    add-int/lit8 v18, v18, 0x1

    aget v17, v17, v18

    invoke-static/range {v13 .. v17}, Lorg/icepdf/core/pobjects/functions/Function_0;->interpolate(FFFFF)F

    move-result v4

    .line 153
    .local v4, "e":F
    const/4 v13, 0x0

    invoke-static {v4, v13}, Ljava/lang/Math;->max(FF)F

    move-result v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/icepdf/core/pobjects/functions/Function_0;->size:[I

    aget v14, v14, v7

    add-int/lit8 v14, v14, -0x1

    int-to-float v14, v14

    invoke-static {v13, v14}, Ljava/lang/Math;->min(FF)F

    move-result v4

    .line 157
    float-to-double v14, v4

    invoke-static {v14, v15}, Ljava/lang/Math;->floor(D)D

    move-result-wide v14

    double-to-int v5, v14

    .line 158
    .local v5, "e1":I
    float-to-double v14, v4

    invoke-static {v14, v15}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v14

    double-to-int v6, v14

    .line 161
    .local v6, "e2":I
    const/4 v9, 0x0

    .local v9, "j":I
    :goto_1
    if-ge v9, v10, :cond_1

    .line 163
    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/icepdf/core/pobjects/functions/Function_0;->bytes:[B

    mul-int v14, v5, v10

    add-int/2addr v14, v9

    aget-byte v13, v13, v14

    and-int/lit16 v2, v13, 0xff

    .line 164
    .local v2, "b1":I
    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/icepdf/core/pobjects/functions/Function_0;->bytes:[B

    mul-int v14, v6, v10

    add-int/2addr v14, v9

    aget-byte v13, v13, v14

    and-int/lit16 v3, v13, 0xff

    .line 166
    .local v3, "b2":I
    int-to-float v13, v2

    int-to-float v14, v3

    add-float/2addr v13, v14

    const/high16 v14, 0x40000000    # 2.0f

    div-float v11, v13, v14

    .line 168
    .local v11, "r":F
    const/4 v13, 0x0

    const-wide/high16 v14, 0x4000000000000000L    # 2.0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/icepdf/core/pobjects/functions/Function_0;->bitspersample:I

    move/from16 v16, v0

    move/from16 v0, v16

    int-to-double v0, v0

    move-wide/from16 v16, v0

    invoke-static/range {v14 .. v17}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v14

    double-to-float v14, v14

    const/high16 v15, 0x3f800000    # 1.0f

    sub-float/2addr v14, v15

    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/icepdf/core/pobjects/functions/Function_0;->decode:[F

    mul-int/lit8 v16, v9, 0x2

    aget v15, v15, v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/core/pobjects/functions/Function_0;->decode:[F

    move-object/from16 v16, v0

    mul-int/lit8 v17, v9, 0x2

    add-int/lit8 v17, v17, 0x1

    aget v16, v16, v17

    move/from16 v0, v16

    invoke-static {v11, v13, v14, v15, v0}, Lorg/icepdf/core/pobjects/functions/Function_0;->interpolate(FFFFF)F

    move-result v11

    .line 172
    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/icepdf/core/pobjects/functions/Function_0;->range:[F

    mul-int/lit8 v14, v9, 0x2

    aget v13, v13, v14

    invoke-static {v11, v13}, Ljava/lang/Math;->max(FF)F

    move-result v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/icepdf/core/pobjects/functions/Function_0;->range:[F

    mul-int/lit8 v15, v9, 0x2

    add-int/lit8 v15, v15, 0x1

    aget v14, v14, v15

    invoke-static {v13, v14}, Ljava/lang/Math;->min(FF)F

    move-result v11

    .line 173
    mul-int v13, v7, v10

    add-int v8, v13, v9

    .line 175
    .local v8, "index":I
    array-length v13, v12

    if-ge v8, v13, :cond_0

    .line 176
    aput v11, v12, v8
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 161
    :cond_0
    add-int/lit8 v9, v9, 0x1

    goto :goto_1

    .line 143
    .end local v2    # "b1":I
    .end local v3    # "b2":I
    .end local v8    # "index":I
    .end local v11    # "r":F
    :cond_1
    add-int/lit8 v7, v7, 0x1

    goto/16 :goto_0

    .line 182
    .end local v4    # "e":F
    .end local v5    # "e1":I
    .end local v6    # "e2":I
    .end local v9    # "j":I
    :catch_0
    move-exception v4

    .line 183
    .local v4, "e":Ljava/lang/Exception;
    sget-object v13, Lorg/icepdf/core/pobjects/functions/Function_0;->logger:Ljava/util/logging/Logger;

    sget-object v14, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    const-string/jumbo v15, "Error calculating function 0 values"

    invoke-virtual {v13, v14, v15, v4}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 185
    .end local v4    # "e":Ljava/lang/Exception;
    :cond_2
    return-object v12
.end method
