.class Lorg/icepdf/core/pobjects/StateManager$PObjectComparatorByReferenceObjectNumber;
.super Ljava/lang/Object;
.source "StateManager.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/icepdf/core/pobjects/StateManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "PObjectComparatorByReferenceObjectNumber"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lorg/icepdf/core/pobjects/PObject;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 143
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lorg/icepdf/core/pobjects/StateManager$1;)V
    .locals 0
    .param p1, "x0"    # Lorg/icepdf/core/pobjects/StateManager$1;

    .prologue
    .line 143
    invoke-direct {p0}, Lorg/icepdf/core/pobjects/StateManager$PObjectComparatorByReferenceObjectNumber;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 143
    check-cast p1, Lorg/icepdf/core/pobjects/PObject;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lorg/icepdf/core/pobjects/PObject;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lorg/icepdf/core/pobjects/StateManager$PObjectComparatorByReferenceObjectNumber;->compare(Lorg/icepdf/core/pobjects/PObject;Lorg/icepdf/core/pobjects/PObject;)I

    move-result v0

    return v0
.end method

.method public compare(Lorg/icepdf/core/pobjects/PObject;Lorg/icepdf/core/pobjects/PObject;)I
    .locals 7
    .param p1, "a"    # Lorg/icepdf/core/pobjects/PObject;
    .param p2, "b"    # Lorg/icepdf/core/pobjects/PObject;

    .prologue
    const/4 v6, 0x1

    const/4 v4, 0x0

    const/4 v5, -0x1

    .line 146
    if-nez p1, :cond_1

    if-nez p2, :cond_1

    .line 166
    :cond_0
    :goto_0
    return v4

    .line 148
    :cond_1
    if-nez p1, :cond_2

    move v4, v5

    .line 149
    goto :goto_0

    .line 150
    :cond_2
    if-nez p2, :cond_3

    move v4, v6

    .line 151
    goto :goto_0

    .line 152
    :cond_3
    invoke-virtual {p1}, Lorg/icepdf/core/pobjects/PObject;->getReference()Lorg/icepdf/core/pobjects/Reference;

    move-result-object v0

    .line 153
    .local v0, "ar":Lorg/icepdf/core/pobjects/Reference;
    invoke-virtual {p2}, Lorg/icepdf/core/pobjects/PObject;->getReference()Lorg/icepdf/core/pobjects/Reference;

    move-result-object v2

    .line 154
    .local v2, "br":Lorg/icepdf/core/pobjects/Reference;
    if-nez v0, :cond_4

    if-eqz v2, :cond_0

    .line 156
    :cond_4
    if-nez v0, :cond_5

    move v4, v5

    .line 157
    goto :goto_0

    .line 158
    :cond_5
    if-nez v2, :cond_6

    move v4, v6

    .line 159
    goto :goto_0

    .line 160
    :cond_6
    invoke-virtual {v0}, Lorg/icepdf/core/pobjects/Reference;->getObjectNumber()I

    move-result v1

    .line 161
    .local v1, "aron":I
    invoke-virtual {v2}, Lorg/icepdf/core/pobjects/Reference;->getObjectNumber()I

    move-result v3

    .line 162
    .local v3, "bron":I
    if-ge v1, v3, :cond_7

    move v4, v5

    .line 163
    goto :goto_0

    .line 164
    :cond_7
    if-le v1, v3, :cond_0

    move v4, v6

    .line 165
    goto :goto_0
.end method
