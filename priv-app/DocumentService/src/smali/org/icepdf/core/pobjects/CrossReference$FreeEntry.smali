.class public Lorg/icepdf/core/pobjects/CrossReference$FreeEntry;
.super Lorg/icepdf/core/pobjects/CrossReference$Entry;
.source "CrossReference.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/icepdf/core/pobjects/CrossReference;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "FreeEntry"
.end annotation


# instance fields
.field private m_iGenerationNumberIfReused:I

.field private m_iNextFreeObjectNumber:I


# direct methods
.method constructor <init>(III)V
    .locals 1
    .param p1, "objectNumber"    # I
    .param p2, "nextFreeObjectNumber"    # I
    .param p3, "generationNumberIfReused"    # I

    .prologue
    .line 264
    const/4 v0, 0x0

    invoke-direct {p0, v0, p1}, Lorg/icepdf/core/pobjects/CrossReference$Entry;-><init>(II)V

    .line 265
    iput p2, p0, Lorg/icepdf/core/pobjects/CrossReference$FreeEntry;->m_iNextFreeObjectNumber:I

    .line 266
    iput p3, p0, Lorg/icepdf/core/pobjects/CrossReference$FreeEntry;->m_iGenerationNumberIfReused:I

    .line 267
    return-void
.end method


# virtual methods
.method public getGenerationNumberIfReused()I
    .locals 1

    .prologue
    .line 274
    iget v0, p0, Lorg/icepdf/core/pobjects/CrossReference$FreeEntry;->m_iGenerationNumberIfReused:I

    return v0
.end method

.method public getNextFreeObjectNumber()I
    .locals 1

    .prologue
    .line 270
    iget v0, p0, Lorg/icepdf/core/pobjects/CrossReference$FreeEntry;->m_iNextFreeObjectNumber:I

    return v0
.end method
