.class public interface abstract Lorg/icepdf/core/pobjects/graphics/text/Text;
.super Ljava/lang/Object;
.source "Text.java"


# virtual methods
.method public abstract getBounds()Landroid/graphics/RectF;
.end method

.method public abstract hasHighligh()Z
.end method

.method public abstract hasSelected()Z
.end method

.method public abstract isHighlighted()Z
.end method

.method public abstract isSelected()Z
.end method

.method public abstract setHasHighlight(Z)V
.end method

.method public abstract setHasSelected(Z)V
.end method

.method public abstract setHighlighted(Z)V
.end method

.method public abstract setSelected(Z)V
.end method
