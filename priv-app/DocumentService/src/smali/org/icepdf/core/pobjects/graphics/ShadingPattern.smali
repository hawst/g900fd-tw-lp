.class public abstract Lorg/icepdf/core/pobjects/graphics/ShadingPattern;
.super Lorg/icepdf/core/pobjects/Dictionary;
.source "ShadingPattern.java"

# interfaces
.implements Lorg/icepdf/core/pobjects/graphics/Pattern;


# static fields
.field public static final SHADING_PATTERN_TYPE_1:I = 0x1

.field public static final SHADING_PATTERN_TYPE_2:I = 0x2

.field public static final SHADING_PATTERN_TYPE_3:I = 0x3

.field public static final SHADING_PATTERN_TYPE_4:I = 0x4

.field public static final SHADING_PATTERN_TYPE_5:I = 0x5

.field public static final SHADING_PATTERN_TYPE_6:I = 0x6

.field private static final logger:Ljava/util/logging/Logger;


# instance fields
.field protected antiAlias:Z

.field protected bBox:Landroid/graphics/RectF;

.field protected background:Ljava/util/Vector;

.field protected colorSpace:Lorg/icepdf/core/pobjects/graphics/PColorSpace;

.field protected extGState:Lorg/icepdf/core/pobjects/graphics/ExtGState;

.field protected inited:Z

.field protected matrix:Landroid/graphics/Matrix;

.field protected patternType:I

.field protected shading:Ljava/util/Hashtable;

.field protected shadingType:I

.field protected type:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 44
    const-class v0, Lorg/icepdf/core/pobjects/graphics/ShadingPattern;

    invoke-virtual {v0}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Lorg/icepdf/core/pobjects/graphics/ShadingPattern;->logger:Ljava/util/logging/Logger;

    return-void
.end method

.method public constructor <init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V
    .locals 4
    .param p1, "library"    # Lorg/icepdf/core/util/Library;
    .param p2, "entries"    # Ljava/util/Hashtable;

    .prologue
    .line 97
    invoke-direct {p0, p1, p2}, Lorg/icepdf/core/pobjects/Dictionary;-><init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V

    .line 99
    const-string/jumbo v2, "Type"

    invoke-virtual {p1, p2, v2}, Lorg/icepdf/core/util/Library;->getName(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lorg/icepdf/core/pobjects/graphics/ShadingPattern;->type:Ljava/lang/String;

    .line 101
    const-string/jumbo v2, "PatternType"

    invoke-virtual {p1, p2, v2}, Lorg/icepdf/core/util/Library;->getInt(Ljava/util/Hashtable;Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lorg/icepdf/core/pobjects/graphics/ShadingPattern;->patternType:I

    .line 103
    const-string/jumbo v2, "ExtGState"

    invoke-virtual {p1, p2, v2}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 104
    .local v0, "attribute":Ljava/lang/Object;
    instance-of v2, v0, Ljava/util/Hashtable;

    if-eqz v2, :cond_1

    .line 105
    new-instance v2, Lorg/icepdf/core/pobjects/graphics/ExtGState;

    check-cast v0, Ljava/util/Hashtable;

    .end local v0    # "attribute":Ljava/lang/Object;
    invoke-direct {v2, p1, v0}, Lorg/icepdf/core/pobjects/graphics/ExtGState;-><init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V

    iput-object v2, p0, Lorg/icepdf/core/pobjects/graphics/ShadingPattern;->extGState:Lorg/icepdf/core/pobjects/graphics/ExtGState;

    .line 112
    :cond_0
    :goto_0
    const-string/jumbo v2, "Matrix"

    invoke-virtual {p1, p2, v2}, Lorg/icepdf/core/util/Library;->getObject(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Vector;

    .line 113
    .local v1, "v":Ljava/util/Vector;
    if-eqz v1, :cond_2

    .line 114
    invoke-static {v1}, Lorg/icepdf/core/pobjects/graphics/ShadingPattern;->getAffineTransform(Ljava/util/Vector;)Landroid/graphics/Matrix;

    move-result-object v2

    iput-object v2, p0, Lorg/icepdf/core/pobjects/graphics/ShadingPattern;->matrix:Landroid/graphics/Matrix;

    .line 119
    :goto_1
    return-void

    .line 106
    .end local v1    # "v":Ljava/util/Vector;
    .restart local v0    # "attribute":Ljava/lang/Object;
    :cond_1
    instance-of v2, v0, Lorg/icepdf/core/pobjects/Reference;

    if-eqz v2, :cond_0

    .line 107
    new-instance v3, Lorg/icepdf/core/pobjects/graphics/ExtGState;

    check-cast v0, Lorg/icepdf/core/pobjects/Reference;

    .end local v0    # "attribute":Ljava/lang/Object;
    invoke-virtual {p1, v0}, Lorg/icepdf/core/util/Library;->getObject(Lorg/icepdf/core/pobjects/Reference;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Hashtable;

    invoke-direct {v3, p1, v2}, Lorg/icepdf/core/pobjects/graphics/ExtGState;-><init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V

    iput-object v3, p0, Lorg/icepdf/core/pobjects/graphics/ShadingPattern;->extGState:Lorg/icepdf/core/pobjects/graphics/ExtGState;

    goto :goto_0

    .line 117
    .restart local v1    # "v":Ljava/util/Vector;
    :cond_2
    new-instance v2, Landroid/graphics/Matrix;

    invoke-direct {v2}, Landroid/graphics/Matrix;-><init>()V

    iput-object v2, p0, Lorg/icepdf/core/pobjects/graphics/ShadingPattern;->matrix:Landroid/graphics/Matrix;

    goto :goto_1
.end method

.method private static getAffineTransform(Ljava/util/Vector;)Landroid/graphics/Matrix;
    .locals 5
    .param p0, "v"    # Ljava/util/Vector;

    .prologue
    const/4 v4, 0x6

    .line 194
    new-array v0, v4, [F

    .line 195
    .local v0, "f":[F
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v4, :cond_0

    .line 196
    invoke-virtual {p0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Number;

    invoke-virtual {v3}, Ljava/lang/Number;->floatValue()F

    move-result v3

    aput v3, v0, v1

    .line 195
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 199
    :cond_0
    invoke-static {v0}, Lcom/samsung/thumbnail/util/Utils;->createMatrixFromArray([F)Landroid/graphics/Matrix;

    move-result-object v2

    .line 201
    .local v2, "ret":Landroid/graphics/Matrix;
    return-object v2
.end method

.method public static getShadingPattern(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)Lorg/icepdf/core/pobjects/graphics/ShadingPattern;
    .locals 2
    .param p0, "library"    # Lorg/icepdf/core/util/Library;
    .param p1, "attribute"    # Ljava/util/Hashtable;

    .prologue
    .line 133
    const-string/jumbo v1, "Shading"

    invoke-virtual {p0, p1, v1}, Lorg/icepdf/core/util/Library;->getDictionary(Ljava/util/Hashtable;Ljava/lang/String;)Ljava/util/Hashtable;

    move-result-object v0

    .line 135
    .local v0, "shading":Ljava/util/Hashtable;
    if-eqz v0, :cond_0

    .line 136
    invoke-static {p0, p1, v0}, Lorg/icepdf/core/pobjects/graphics/ShadingPattern;->shadingFactory(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;Ljava/util/Hashtable;)Lorg/icepdf/core/pobjects/graphics/ShadingPattern;

    move-result-object v1

    .line 138
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static getShadingPattern(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;Ljava/util/Hashtable;)Lorg/icepdf/core/pobjects/graphics/ShadingPattern;
    .locals 1
    .param p0, "library"    # Lorg/icepdf/core/util/Library;
    .param p1, "entries"    # Ljava/util/Hashtable;
    .param p2, "shading"    # Ljava/util/Hashtable;

    .prologue
    .line 155
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    .line 156
    invoke-static {p0, p2, p2}, Lorg/icepdf/core/pobjects/graphics/ShadingPattern;->shadingFactory(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;Ljava/util/Hashtable;)Lorg/icepdf/core/pobjects/graphics/ShadingPattern;

    move-result-object v0

    .line 159
    .local v0, "shadingPattern":Lorg/icepdf/core/pobjects/graphics/ShadingPattern;
    if-eqz v0, :cond_0

    .line 160
    invoke-virtual {v0, p2}, Lorg/icepdf/core/pobjects/graphics/ShadingPattern;->setShading(Ljava/util/Hashtable;)V

    .line 165
    .end local v0    # "shadingPattern":Lorg/icepdf/core/pobjects/graphics/ShadingPattern;
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static shadingFactory(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;Ljava/util/Hashtable;)Lorg/icepdf/core/pobjects/graphics/ShadingPattern;
    .locals 4
    .param p0, "library"    # Lorg/icepdf/core/util/Library;
    .param p1, "attribute"    # Ljava/util/Hashtable;
    .param p2, "patternDictionary"    # Ljava/util/Hashtable;

    .prologue
    .line 172
    const-string/jumbo v1, "ShadingType"

    invoke-virtual {p0, p2, v1}, Lorg/icepdf/core/util/Library;->getInt(Ljava/util/Hashtable;Ljava/lang/String;)I

    move-result v0

    .line 173
    .local v0, "shadingType":I
    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 174
    new-instance v1, Lorg/icepdf/core/pobjects/graphics/ShadingType2Pattern;

    invoke-direct {v1, p0, p1}, Lorg/icepdf/core/pobjects/graphics/ShadingType2Pattern;-><init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V

    .line 183
    :goto_0
    return-object v1

    .line 175
    :cond_0
    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    .line 176
    new-instance v1, Lorg/icepdf/core/pobjects/graphics/ShadingType3Pattern;

    invoke-direct {v1, p0, p1}, Lorg/icepdf/core/pobjects/graphics/ShadingType3Pattern;-><init>(Lorg/icepdf/core/util/Library;Ljava/util/Hashtable;)V

    goto :goto_0

    .line 178
    :cond_1
    sget-object v1, Lorg/icepdf/core/pobjects/graphics/ShadingPattern;->logger:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    invoke-virtual {v1, v2}, Ljava/util/logging/Logger;->isLoggable(Ljava/util/logging/Level;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 179
    sget-object v1, Lorg/icepdf/core/pobjects/graphics/ShadingPattern;->logger:Ljava/util/logging/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Shading pattern of Type "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " are not currently supported"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/logging/Logger;->fine(Ljava/lang/String;)V

    .line 183
    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getBBox()Landroid/graphics/RectF;
    .locals 1

    .prologue
    .line 231
    iget-object v0, p0, Lorg/icepdf/core/pobjects/graphics/ShadingPattern;->bBox:Landroid/graphics/RectF;

    return-object v0
.end method

.method public getBackground()Ljava/util/Vector;
    .locals 1

    .prologue
    .line 255
    iget-object v0, p0, Lorg/icepdf/core/pobjects/graphics/ShadingPattern;->background:Ljava/util/Vector;

    return-object v0
.end method

.method public getColorSpace()Lorg/icepdf/core/pobjects/graphics/PColorSpace;
    .locals 1

    .prologue
    .line 251
    iget-object v0, p0, Lorg/icepdf/core/pobjects/graphics/ShadingPattern;->colorSpace:Lorg/icepdf/core/pobjects/graphics/PColorSpace;

    return-object v0
.end method

.method public getExtGState()Lorg/icepdf/core/pobjects/graphics/ExtGState;
    .locals 1

    .prologue
    .line 263
    iget-object v0, p0, Lorg/icepdf/core/pobjects/graphics/ShadingPattern;->extGState:Lorg/icepdf/core/pobjects/graphics/ExtGState;

    return-object v0
.end method

.method public getMatrix()Landroid/graphics/Matrix;
    .locals 1

    .prologue
    .line 235
    iget-object v0, p0, Lorg/icepdf/core/pobjects/graphics/ShadingPattern;->matrix:Landroid/graphics/Matrix;

    return-object v0
.end method

.method public abstract getPaint()Landroid/graphics/Paint;
.end method

.method public getPatternType()I
    .locals 1

    .prologue
    .line 227
    iget v0, p0, Lorg/icepdf/core/pobjects/graphics/ShadingPattern;->patternType:I

    return v0
.end method

.method public getShadingType()I
    .locals 1

    .prologue
    .line 239
    iget v0, p0, Lorg/icepdf/core/pobjects/graphics/ShadingPattern;->shadingType:I

    return v0
.end method

.method public getType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 247
    iget-object v0, p0, Lorg/icepdf/core/pobjects/graphics/ShadingPattern;->type:Ljava/lang/String;

    return-object v0
.end method

.method public abstract init()V
.end method

.method public isAntiAlias()Z
    .locals 1

    .prologue
    .line 259
    iget-boolean v0, p0, Lorg/icepdf/core/pobjects/graphics/ShadingPattern;->antiAlias:Z

    return v0
.end method

.method public isInited()Z
    .locals 1

    .prologue
    .line 267
    iget-boolean v0, p0, Lorg/icepdf/core/pobjects/graphics/ShadingPattern;->inited:Z

    return v0
.end method

.method public setMatrix(Landroid/graphics/Matrix;)V
    .locals 0
    .param p1, "matrix"    # Landroid/graphics/Matrix;

    .prologue
    .line 223
    iput-object p1, p0, Lorg/icepdf/core/pobjects/graphics/ShadingPattern;->matrix:Landroid/graphics/Matrix;

    .line 224
    return-void
.end method

.method public setParentGraphicState(Lorg/icepdf/core/pobjects/graphics/GraphicsState;)V
    .locals 0
    .param p1, "graphicsState"    # Lorg/icepdf/core/pobjects/graphics/GraphicsState;

    .prologue
    .line 220
    return-void
.end method

.method public setShading(Ljava/util/Hashtable;)V
    .locals 0
    .param p1, "shading"    # Ljava/util/Hashtable;

    .prologue
    .line 243
    iput-object p1, p0, Lorg/icepdf/core/pobjects/graphics/ShadingPattern;->shading:Ljava/util/Hashtable;

    .line 244
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 271
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "Shading Pattern: \n           type: pattern \n    patternType: shading\n         matrix: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/icepdf/core/pobjects/graphics/ShadingPattern;->matrix:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "\n      extGState: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/icepdf/core/pobjects/graphics/ShadingPattern;->extGState:Lorg/icepdf/core/pobjects/graphics/ExtGState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "\n        shading dictionary: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/icepdf/core/pobjects/graphics/ShadingPattern;->shading:Ljava/util/Hashtable;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "\n               shadingType: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lorg/icepdf/core/pobjects/graphics/ShadingPattern;->shadingType:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "\n               colourSpace: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/icepdf/core/pobjects/graphics/ShadingPattern;->colorSpace:Lorg/icepdf/core/pobjects/graphics/PColorSpace;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "\n                background: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/icepdf/core/pobjects/graphics/ShadingPattern;->background:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "\n                      bbox: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/icepdf/core/pobjects/graphics/ShadingPattern;->bBox:Landroid/graphics/RectF;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "\n                 antiAlias: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lorg/icepdf/core/pobjects/graphics/ShadingPattern;->antiAlias:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
