.class public Lorg/icepdf/core/pobjects/functions/postscript/OperatorNames;
.super Ljava/lang/Object;
.source "OperatorNames.java"


# static fields
.field public static final NO_OP:I = 0x0

.field public static final OP_ABS:I = 0x1

.field public static final OP_ADD:I = 0x2

.field public static final OP_AND:I = 0x3

.field public static final OP_ATAN:I = 0x4

.field public static final OP_BITSHIFT:I = 0x5

.field public static final OP_CEILING:I = 0x6

.field public static final OP_COPY:I = 0x8

.field public static final OP_COS:I = 0x7

.field public static final OP_CVI:I = 0x9

.field public static final OP_CVR:I = 0xa

.field public static final OP_DIV:I = 0xb

.field public static final OP_DUP:I = 0xc

.field public static final OP_EQ:I = 0xd

.field public static final OP_EXCH:I = 0xe

.field public static final OP_EXP:I = 0xf

.field public static final OP_EXP_END:I = 0x2c

.field public static final OP_EXP_START:I = 0x2b

.field public static final OP_FALSE:I = 0x10

.field public static final OP_FLOOR:I = 0x11

.field public static final OP_GE:I = 0x12

.field public static final OP_GT:I = 0x13

.field public static final OP_IDIV:I = 0x14

.field public static final OP_IF:I = 0x15

.field public static final OP_IFELSE:I = 0x16

.field public static final OP_INDEX:I = 0x18

.field public static final OP_LE:I = 0x19

.field public static final OP_LN:I = 0x17

.field public static final OP_LOG:I = 0x1a

.field public static final OP_LT:I = 0x1b

.field public static final OP_MOD:I = 0x1c

.field public static final OP_MUL:I = 0x1d

.field public static final OP_NE:I = 0x1e

.field public static final OP_NEG:I = 0x1f

.field public static final OP_NOT:I = 0x20

.field public static final OP_OR:I = 0x21

.field public static final OP_POP:I = 0x22

.field public static final OP_ROLL:I = 0x23

.field public static final OP_ROUND:I = 0x24

.field public static final OP_SIN:I = 0x25

.field public static final OP_SQRT:I = 0x26

.field public static final OP_SUB:I = 0x27

.field public static final OP_TRUE:I = 0x28

.field public static final OP_TRUNCATE:I = 0x29

.field public static final OP_XOR:I = 0x2a


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getType([CII)I
    .locals 8
    .param p0, "ch"    # [C
    .param p1, "offset"    # I
    .param p2, "length"    # I

    .prologue
    const/16 v7, 0x49

    const/16 v6, 0x45

    const/4 v5, 0x5

    const/4 v4, 0x2

    const/4 v3, 0x4

    .line 41
    aget-char v0, p0, p1

    .line 44
    .local v0, "c":C
    packed-switch v0, :pswitch_data_0

    .line 176
    :cond_0
    :pswitch_0
    const/4 v3, 0x0

    :cond_1
    :goto_0
    return v3

    .line 47
    :pswitch_1
    if-eq p2, v3, :cond_1

    .line 48
    add-int/lit8 v3, p1, 0x1

    aget-char v1, p0, v3

    .line 49
    .local v1, "c1":C
    const/16 v3, 0x62

    if-eq v1, v3, :cond_2

    const/16 v3, 0x42

    if-ne v1, v3, :cond_3

    .line 50
    :cond_2
    const/4 v3, 0x1

    goto :goto_0

    .line 51
    :cond_3
    const/16 v3, 0x64

    if-eq v1, v3, :cond_4

    const/16 v3, 0x44

    if-ne v1, v3, :cond_0

    :cond_4
    move v3, v4

    .line 52
    goto :goto_0

    .end local v1    # "c1":C
    :pswitch_2
    move v3, v5

    .line 57
    goto :goto_0

    .line 60
    :pswitch_3
    const/16 v4, 0x8

    if-ne p2, v4, :cond_5

    const/4 v3, 0x6

    goto :goto_0

    .line 61
    :cond_5
    if-ne p2, v3, :cond_6

    const/16 v3, 0x8

    goto :goto_0

    .line 62
    :cond_6
    add-int/lit8 v3, p1, 0x1

    aget-char v1, p0, v3

    .line 63
    .restart local v1    # "c1":C
    const/16 v3, 0x6f

    if-eq v1, v3, :cond_7

    const/16 v3, 0x4f

    if-ne v1, v3, :cond_8

    .line 64
    :cond_7
    const/4 v3, 0x7

    goto :goto_0

    .line 65
    :cond_8
    const/16 v3, 0x76

    if-eq v1, v3, :cond_9

    const/16 v3, 0x56

    if-ne v1, v3, :cond_0

    .line 66
    :cond_9
    add-int/lit8 v3, p1, 0x2

    aget-char v2, p0, v3

    .line 67
    .local v2, "c2":C
    const/16 v3, 0x69

    if-eq v2, v3, :cond_a

    if-ne v2, v7, :cond_b

    .line 68
    :cond_a
    const/16 v3, 0x9

    goto :goto_0

    .line 69
    :cond_b
    const/16 v3, 0x72

    if-eq v2, v3, :cond_c

    const/16 v3, 0x52

    if-ne v2, v3, :cond_0

    .line 70
    :cond_c
    const/16 v3, 0xa

    goto :goto_0

    .line 76
    .end local v1    # "c1":C
    .end local v2    # "c2":C
    :pswitch_4
    add-int/lit8 v3, p1, 0x1

    aget-char v1, p0, v3

    .line 77
    .restart local v1    # "c1":C
    const/16 v3, 0x69

    if-eq v1, v3, :cond_d

    if-ne v1, v7, :cond_e

    .line 78
    :cond_d
    const/16 v3, 0xb

    goto :goto_0

    .line 79
    :cond_e
    const/16 v3, 0x75

    if-eq v1, v3, :cond_f

    const/16 v3, 0x55

    if-ne v1, v3, :cond_0

    .line 80
    :cond_f
    const/16 v3, 0xc

    goto :goto_0

    .line 85
    .end local v1    # "c1":C
    :pswitch_5
    if-ne p2, v4, :cond_10

    const/16 v3, 0xd

    goto :goto_0

    .line 86
    :cond_10
    const/4 v4, 0x3

    if-ne p2, v4, :cond_11

    const/16 v3, 0xf

    goto :goto_0

    .line 87
    :cond_11
    if-ne p2, v3, :cond_0

    const/16 v3, 0xe

    goto :goto_0

    .line 91
    :pswitch_6
    const/16 v3, 0x11

    goto :goto_0

    .line 94
    :pswitch_7
    add-int/lit8 v3, p1, 0x1

    aget-char v1, p0, v3

    .line 95
    .restart local v1    # "c1":C
    const/16 v3, 0x65

    if-eq v1, v3, :cond_12

    if-ne v1, v6, :cond_13

    .line 96
    :cond_12
    const/16 v3, 0x12

    goto/16 :goto_0

    .line 97
    :cond_13
    const/16 v3, 0x74

    if-eq v1, v3, :cond_14

    const/16 v3, 0x54

    if-ne v1, v3, :cond_0

    .line 98
    :cond_14
    const/16 v3, 0x13

    goto/16 :goto_0

    .line 103
    .end local v1    # "c1":C
    :pswitch_8
    const/4 v3, 0x6

    if-ne p2, v3, :cond_15

    const/16 v3, 0x16

    goto/16 :goto_0

    .line 104
    :cond_15
    add-int/lit8 v3, p1, 0x1

    aget-char v1, p0, v3

    .line 105
    .restart local v1    # "c1":C
    const/16 v3, 0x64

    if-eq v1, v3, :cond_16

    const/16 v3, 0x44

    if-ne v1, v3, :cond_17

    .line 106
    :cond_16
    const/16 v3, 0x14

    goto/16 :goto_0

    .line 107
    :cond_17
    const/16 v3, 0x66

    if-eq v1, v3, :cond_18

    const/16 v3, 0x46

    if-ne v1, v3, :cond_19

    .line 108
    :cond_18
    const/16 v3, 0x15

    goto/16 :goto_0

    .line 109
    :cond_19
    const/16 v3, 0x6e

    if-eq v1, v3, :cond_1a

    const/16 v3, 0x4e

    if-ne v1, v3, :cond_0

    .line 110
    :cond_1a
    if-ne p2, v5, :cond_1b

    const/16 v3, 0x18

    goto/16 :goto_0

    .line 111
    :cond_1b
    if-ne p2, v4, :cond_0

    const/16 v3, 0x17

    goto/16 :goto_0

    .line 116
    .end local v1    # "c1":C
    :pswitch_9
    const/4 v3, 0x3

    if-ne p2, v3, :cond_1c

    const/16 v3, 0x1a

    goto/16 :goto_0

    .line 117
    :cond_1c
    add-int/lit8 v3, p1, 0x1

    aget-char v1, p0, v3

    .line 118
    .restart local v1    # "c1":C
    const/16 v3, 0x65

    if-eq v1, v3, :cond_1d

    if-ne v1, v6, :cond_1e

    .line 119
    :cond_1d
    const/16 v3, 0x19

    goto/16 :goto_0

    .line 120
    :cond_1e
    const/16 v3, 0x74

    if-eq v1, v3, :cond_1f

    const/16 v3, 0x54

    if-ne v1, v3, :cond_0

    .line 121
    :cond_1f
    const/16 v3, 0x1b

    goto/16 :goto_0

    .line 126
    .end local v1    # "c1":C
    :pswitch_a
    add-int/lit8 v3, p1, 0x1

    aget-char v1, p0, v3

    .line 127
    .restart local v1    # "c1":C
    const/16 v3, 0x6f

    if-eq v1, v3, :cond_20

    const/16 v3, 0x4f

    if-ne v1, v3, :cond_21

    .line 128
    :cond_20
    const/16 v3, 0x1c

    goto/16 :goto_0

    .line 129
    :cond_21
    const/16 v3, 0x75

    if-eq v1, v3, :cond_22

    const/16 v3, 0x55

    if-ne v1, v3, :cond_0

    .line 130
    :cond_22
    const/16 v3, 0x1d

    goto/16 :goto_0

    .line 135
    .end local v1    # "c1":C
    :pswitch_b
    if-ne p2, v4, :cond_23

    const/16 v3, 0x1e

    goto/16 :goto_0

    .line 136
    :cond_23
    add-int/lit8 v3, p1, 0x1

    aget-char v1, p0, v3

    .line 138
    .restart local v1    # "c1":C
    const/16 v3, 0x65

    if-eq v1, v3, :cond_24

    if-ne v1, v6, :cond_25

    .line 139
    :cond_24
    const/16 v3, 0x1f

    goto/16 :goto_0

    .line 140
    :cond_25
    const/16 v3, 0x6f

    if-eq v1, v3, :cond_26

    const/16 v3, 0x4f

    if-ne v1, v3, :cond_0

    .line 141
    :cond_26
    const/16 v3, 0x20

    goto/16 :goto_0

    .line 146
    .end local v1    # "c1":C
    :pswitch_c
    const/16 v3, 0x21

    goto/16 :goto_0

    .line 149
    :pswitch_d
    const/16 v3, 0x22

    goto/16 :goto_0

    .line 152
    :pswitch_e
    if-ne p2, v3, :cond_27

    const/16 v3, 0x23

    goto/16 :goto_0

    .line 153
    :cond_27
    if-ne p2, v5, :cond_0

    const/16 v3, 0x24

    goto/16 :goto_0

    .line 157
    :pswitch_f
    if-ne p2, v3, :cond_28

    const/16 v3, 0x26

    goto/16 :goto_0

    .line 158
    :cond_28
    add-int/lit8 v3, p1, 0x1

    aget-char v1, p0, v3

    .line 159
    .restart local v1    # "c1":C
    const/16 v3, 0x75

    if-eq v1, v3, :cond_29

    const/16 v3, 0x55

    if-ne v1, v3, :cond_2a

    .line 160
    :cond_29
    const/16 v3, 0x27

    goto/16 :goto_0

    .line 161
    :cond_2a
    const/16 v3, 0x69

    if-eq v1, v3, :cond_2b

    if-ne v1, v7, :cond_0

    .line 162
    :cond_2b
    const/16 v3, 0x25

    goto/16 :goto_0

    .line 167
    .end local v1    # "c1":C
    :pswitch_10
    const/16 v3, 0x29

    goto/16 :goto_0

    .line 170
    :pswitch_11
    const/16 v3, 0x2a

    goto/16 :goto_0

    .line 172
    :pswitch_12
    const/16 v3, 0x2b

    goto/16 :goto_0

    .line 174
    :pswitch_13
    const/16 v3, 0x2c

    goto/16 :goto_0

    .line 44
    :pswitch_data_0
    .packed-switch 0x41
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_0
        :pswitch_8
        :pswitch_0
        :pswitch_0
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_0
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_11
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_0
        :pswitch_8
        :pswitch_0
        :pswitch_0
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_0
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_11
        :pswitch_0
        :pswitch_0
        :pswitch_12
        :pswitch_0
        :pswitch_13
    .end packed-switch
.end method
