.class public Lorg/icepdf/core/pobjects/functions/Function_4;
.super Lorg/icepdf/core/pobjects/functions/Function;
.source "Function_4.java"


# static fields
.field private static final logger:Ljava/util/logging/Logger;


# instance fields
.field private functionContent:Ljava/lang/String;

.field private resultCache:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "[F>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 49
    const-class v0, Lorg/icepdf/core/pobjects/functions/Function_4;

    invoke-virtual {v0}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Lorg/icepdf/core/pobjects/functions/Function_4;->logger:Ljava/util/logging/Logger;

    return-void
.end method

.method public constructor <init>(Lorg/icepdf/core/pobjects/Dictionary;)V
    .locals 7
    .param p1, "d"    # Lorg/icepdf/core/pobjects/Dictionary;

    .prologue
    const/4 v5, 0x0

    .line 59
    invoke-direct {p0, p1}, Lorg/icepdf/core/pobjects/functions/Function;-><init>(Lorg/icepdf/core/pobjects/Dictionary;)V

    .line 61
    instance-of v4, p1, Lorg/icepdf/core/pobjects/Stream;

    if-eqz v4, :cond_2

    move-object v1, p1

    .line 62
    check-cast v1, Lorg/icepdf/core/pobjects/Stream;

    .line 63
    .local v1, "functionStream":Lorg/icepdf/core/pobjects/Stream;
    invoke-virtual {v1}, Lorg/icepdf/core/pobjects/Stream;->getInputStreamForDecodedStreamBytes()Ljava/io/InputStream;

    move-result-object v3

    .line 64
    .local v3, "input":Ljava/io/InputStream;
    instance-of v4, v3, Lorg/icepdf/core/io/SeekableInput;

    if-eqz v4, :cond_1

    move-object v4, v3

    .line 65
    check-cast v4, Lorg/icepdf/core/io/SeekableInput;

    invoke-static {v4, v5}, Lorg/icepdf/core/util/Utils;->getContentFromSeekableInput(Lorg/icepdf/core/io/SeekableInput;Z)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lorg/icepdf/core/pobjects/functions/Function_4;->functionContent:Ljava/lang/String;

    .line 70
    :goto_0
    sget-object v4, Lorg/icepdf/core/pobjects/functions/Function_4;->logger:Ljava/util/logging/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "Function 4: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lorg/icepdf/core/pobjects/functions/Function_4;->functionContent:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/logging/Logger;->finest(Ljava/lang/String;)V

    .line 72
    if-eqz v3, :cond_0

    .line 74
    :try_start_0
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 83
    .end local v1    # "functionStream":Lorg/icepdf/core/pobjects/Stream;
    .end local v3    # "input":Ljava/io/InputStream;
    :cond_0
    :goto_1
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    iput-object v4, p0, Lorg/icepdf/core/pobjects/functions/Function_4;->resultCache:Ljava/util/HashMap;

    .line 84
    return-void

    .line 67
    .restart local v1    # "functionStream":Lorg/icepdf/core/pobjects/Stream;
    .restart local v3    # "input":Ljava/io/InputStream;
    :cond_1
    const/4 v4, 0x1

    new-array v2, v4, [Ljava/io/InputStream;

    aput-object v3, v2, v5

    .line 68
    .local v2, "inArray":[Ljava/io/InputStream;
    invoke-static {v2, v5}, Lorg/icepdf/core/util/Utils;->getContentAndReplaceInputStream([Ljava/io/InputStream;Z)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lorg/icepdf/core/pobjects/functions/Function_4;->functionContent:Ljava/lang/String;

    goto :goto_0

    .line 75
    .end local v2    # "inArray":[Ljava/io/InputStream;
    :catch_0
    move-exception v0

    .line 76
    .local v0, "e":Ljava/io/IOException;
    const-string/jumbo v4, "DocumentService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "Exception: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 80
    .end local v0    # "e":Ljava/io/IOException;
    .end local v1    # "functionStream":Lorg/icepdf/core/pobjects/Stream;
    .end local v3    # "input":Ljava/io/InputStream;
    :cond_2
    sget-object v4, Lorg/icepdf/core/pobjects/functions/Function_4;->logger:Ljava/util/logging/Logger;

    const-string/jumbo v5, "Type 4 function operands could not be found."

    invoke-virtual {v4, v5}, Ljava/util/logging/Logger;->warning(Ljava/lang/String;)V

    goto :goto_1
.end method

.method private calculateColourKey([F)Ljava/lang/Integer;
    .locals 13
    .param p1, "colours"    # [F

    .prologue
    const/4 v12, 0x2

    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 137
    array-length v5, p1

    .line 139
    .local v5, "length":I
    aget v6, p1, v10

    float-to-double v6, v6

    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    cmpg-double v6, v6, v8

    if-lez v6, :cond_2

    .line 140
    if-ne v5, v11, :cond_0

    .line 141
    aget v6, p1, v10

    float-to-int v6, v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    .line 154
    :goto_0
    return-object v6

    .line 142
    :cond_0
    if-ne v5, v12, :cond_1

    .line 143
    aget v6, p1, v11

    float-to-int v6, v6

    shl-int/lit8 v6, v6, 0x8

    aget v7, p1, v10

    float-to-int v7, v7

    or-int/2addr v6, v7

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    goto :goto_0

    .line 144
    :cond_1
    const/4 v6, 0x3

    if-ne v5, v6, :cond_2

    .line 145
    aget v6, p1, v12

    float-to-int v6, v6

    shl-int/lit8 v6, v6, 0x10

    aget v7, p1, v11

    float-to-int v7, v7

    shl-int/lit8 v7, v7, 0x8

    or-int/2addr v6, v7

    aget v7, p1, v10

    float-to-int v7, v7

    or-int/2addr v6, v7

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    goto :goto_0

    .line 150
    :cond_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 151
    .local v1, "builder":Ljava/lang/StringBuilder;
    move-object v0, p1

    .local v0, "arr$":[F
    array-length v4, v0

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_1
    if-ge v3, v4, :cond_3

    aget v2, v0, v3

    .line 152
    .local v2, "colour":F
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 151
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 154
    .end local v2    # "colour":F
    :cond_3
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->hashCode()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    goto :goto_0
.end method


# virtual methods
.method public calculate([F)[F
    .locals 12
    .param p1, "x"    # [F

    .prologue
    .line 95
    invoke-direct {p0, p1}, Lorg/icepdf/core/pobjects/functions/Function_4;->calculateColourKey([F)Ljava/lang/Integer;

    move-result-object v0

    .line 96
    .local v0, "colourKey":Ljava/lang/Integer;
    iget-object v9, p0, Lorg/icepdf/core/pobjects/functions/Function_4;->resultCache:Ljava/util/HashMap;

    invoke-virtual {v9, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, [F

    .line 97
    .local v6, "result":[F
    if-eqz v6, :cond_0

    .line 128
    .end local v6    # "result":[F
    :goto_0
    return-object v6

    .line 102
    .restart local v6    # "result":[F
    :cond_0
    new-instance v1, Ljava/io/ByteArrayInputStream;

    iget-object v9, p0, Lorg/icepdf/core/pobjects/functions/Function_4;->functionContent:Ljava/lang/String;

    invoke-virtual {v9}, Ljava/lang/String;->getBytes()[B

    move-result-object v9

    invoke-direct {v1, v9}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 103
    .local v1, "content":Ljava/io/InputStream;
    new-instance v4, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;

    invoke-direct {v4}, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;-><init>()V

    .line 104
    .local v4, "lex":Lorg/icepdf/core/pobjects/functions/postscript/Lexer;
    invoke-virtual {v4, v1}, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->setInputStream(Ljava/io/InputStream;)V

    .line 108
    :try_start_0
    invoke-virtual {v4, p1}, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->parse([F)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 114
    :goto_1
    invoke-virtual {v4}, Lorg/icepdf/core/pobjects/functions/postscript/Lexer;->getStack()Ljava/util/Stack;

    move-result-object v7

    .line 117
    .local v7, "stack":Ljava/util/Stack;
    iget-object v9, p0, Lorg/icepdf/core/pobjects/functions/Function_4;->range:[F

    array-length v9, v9

    div-int/lit8 v5, v9, 0x2

    .line 119
    .local v5, "n":I
    new-array v8, v5, [F

    .line 122
    .local v8, "y":[F
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_2
    if-ge v3, v5, :cond_1

    .line 123
    invoke-virtual {v7, v3}, Ljava/util/Stack;->elementAt(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Float;

    invoke-virtual {v9}, Ljava/lang/Float;->floatValue()F

    move-result v9

    iget-object v10, p0, Lorg/icepdf/core/pobjects/functions/Function_4;->range:[F

    mul-int/lit8 v11, v3, 0x2

    aget v10, v10, v11

    invoke-static {v9, v10}, Ljava/lang/Math;->max(FF)F

    move-result v9

    iget-object v10, p0, Lorg/icepdf/core/pobjects/functions/Function_4;->range:[F

    mul-int/lit8 v11, v3, 0x2

    add-int/lit8 v11, v11, 0x1

    aget v10, v10, v11

    invoke-static {v9, v10}, Ljava/lang/Math;->min(FF)F

    move-result v9

    aput v9, v8, v3

    .line 122
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 109
    .end local v3    # "i":I
    .end local v5    # "n":I
    .end local v7    # "stack":Ljava/util/Stack;
    .end local v8    # "y":[F
    :catch_0
    move-exception v2

    .line 110
    .local v2, "e":Ljava/io/IOException;
    sget-object v9, Lorg/icepdf/core/pobjects/functions/Function_4;->logger:Ljava/util/logging/Logger;

    sget-object v10, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    const-string/jumbo v11, "Error Processing Type 4 definition"

    invoke-virtual {v9, v10, v11, v2}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 127
    .end local v2    # "e":Ljava/io/IOException;
    .restart local v3    # "i":I
    .restart local v5    # "n":I
    .restart local v7    # "stack":Ljava/util/Stack;
    .restart local v8    # "y":[F
    :cond_1
    iget-object v9, p0, Lorg/icepdf/core/pobjects/functions/Function_4;->resultCache:Ljava/util/HashMap;

    invoke-virtual {v9, v0, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v6, v8

    .line 128
    goto :goto_0
.end method
