.class final Lorg/icepdf/core/pobjects/functions/postscript/OperatorFactory$38;
.super Lorg/icepdf/core/pobjects/functions/postscript/Operator;
.source "OperatorFactory.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/icepdf/core/pobjects/functions/postscript/OperatorFactory;->getOperator([CII)Lorg/icepdf/core/pobjects/functions/postscript/Operator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>(I)V
    .locals 0
    .param p1, "x0"    # I

    .prologue
    .line 677
    invoke-direct {p0, p1}, Lorg/icepdf/core/pobjects/functions/postscript/Operator;-><init>(I)V

    return-void
.end method


# virtual methods
.method public eval(Ljava/util/Stack;)V
    .locals 3
    .param p1, "stack"    # Ljava/util/Stack;

    .prologue
    .line 679
    invoke-virtual {p1}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v1

    .line 680
    .local v1, "num2":F
    invoke-virtual {p1}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .line 681
    .local v0, "num1":F
    sub-float v2, v0, v1

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 682
    return-void
.end method
