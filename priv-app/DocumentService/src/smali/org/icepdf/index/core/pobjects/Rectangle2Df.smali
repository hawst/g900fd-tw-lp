.class public Lorg/icepdf/index/core/pobjects/Rectangle2Df;
.super Ljava/lang/Object;
.source "Rectangle2Df.java"

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field private static final serialVersionUID:J = -0x53ef25335d4a989L


# instance fields
.field public height:F

.field public width:F

.field public x:F

.field public y:F


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    invoke-virtual {p0, v0, v0, v0, v0}, Lorg/icepdf/index/core/pobjects/Rectangle2Df;->setRect(FFFF)V

    .line 41
    return-void
.end method

.method public constructor <init>(FFFF)V
    .locals 0
    .param p1, "x"    # F
    .param p2, "y"    # F
    .param p3, "width"    # F
    .param p4, "height"    # F

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    invoke-virtual {p0, p1, p2, p3, p4}, Lorg/icepdf/index/core/pobjects/Rectangle2Df;->setRect(FFFF)V

    .line 45
    return-void
.end method


# virtual methods
.method public add(Landroid/graphics/PointF;)V
    .locals 6
    .param p1, "dst"    # Landroid/graphics/PointF;

    .prologue
    .line 104
    new-instance v0, Landroid/graphics/RectF;

    iget v1, p0, Lorg/icepdf/index/core/pobjects/Rectangle2Df;->x:F

    iget v2, p0, Lorg/icepdf/index/core/pobjects/Rectangle2Df;->y:F

    iget v3, p0, Lorg/icepdf/index/core/pobjects/Rectangle2Df;->x:F

    iget v4, p0, Lorg/icepdf/index/core/pobjects/Rectangle2Df;->width:F

    add-float/2addr v3, v4

    iget v4, p0, Lorg/icepdf/index/core/pobjects/Rectangle2Df;->y:F

    iget v5, p0, Lorg/icepdf/index/core/pobjects/Rectangle2Df;->height:F

    add-float/2addr v4, v5

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 105
    .local v0, "r":Landroid/graphics/RectF;
    iget v1, p1, Landroid/graphics/PointF;->x:F

    iget v2, p1, Landroid/graphics/PointF;->y:F

    invoke-virtual {v0, v1, v2}, Landroid/graphics/RectF;->union(FF)V

    .line 106
    iget v1, v0, Landroid/graphics/RectF;->left:F

    iput v1, p0, Lorg/icepdf/index/core/pobjects/Rectangle2Df;->x:F

    .line 107
    iget v1, v0, Landroid/graphics/RectF;->top:F

    iput v1, p0, Lorg/icepdf/index/core/pobjects/Rectangle2Df;->y:F

    .line 108
    iget v1, v0, Landroid/graphics/RectF;->right:F

    iget v2, v0, Landroid/graphics/RectF;->left:F

    sub-float/2addr v1, v2

    iput v1, p0, Lorg/icepdf/index/core/pobjects/Rectangle2Df;->width:F

    .line 109
    iget v1, v0, Landroid/graphics/RectF;->bottom:F

    iget v2, v0, Landroid/graphics/RectF;->top:F

    sub-float/2addr v1, v2

    iput v1, p0, Lorg/icepdf/index/core/pobjects/Rectangle2Df;->height:F

    .line 110
    return-void
.end method

.method public add(Lorg/icepdf/index/core/pobjects/Rectangle2Df;)V
    .locals 6
    .param p1, "other"    # Lorg/icepdf/index/core/pobjects/Rectangle2Df;

    .prologue
    .line 74
    if-eqz p1, :cond_0

    .line 75
    invoke-virtual {p1}, Lorg/icepdf/index/core/pobjects/Rectangle2Df;->toRectF()Landroid/graphics/RectF;

    move-result-object v0

    .line 76
    .local v0, "otherRF":Landroid/graphics/RectF;
    invoke-virtual {p0}, Lorg/icepdf/index/core/pobjects/Rectangle2Df;->toRectF()Landroid/graphics/RectF;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/RectF;->union(Landroid/graphics/RectF;)V

    .line 77
    iget v1, v0, Landroid/graphics/RectF;->left:F

    iget v2, v0, Landroid/graphics/RectF;->top:F

    iget v3, v0, Landroid/graphics/RectF;->right:F

    iget v4, v0, Landroid/graphics/RectF;->left:F

    sub-float/2addr v3, v4

    iget v4, v0, Landroid/graphics/RectF;->bottom:F

    iget v5, v0, Landroid/graphics/RectF;->top:F

    sub-float/2addr v4, v5

    invoke-virtual {p0, v1, v2, v3, v4}, Lorg/icepdf/index/core/pobjects/Rectangle2Df;->setRect(FFFF)V

    .line 79
    .end local v0    # "otherRF":Landroid/graphics/RectF;
    :cond_0
    return-void
.end method

.method protected clone()Ljava/lang/Object;
    .locals 5

    .prologue
    .line 116
    const/4 v1, 0x0

    .line 118
    .local v1, "result":Ljava/lang/Object;
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 123
    .end local v1    # "result":Ljava/lang/Object;
    :goto_0
    return-object v1

    .line 119
    .restart local v1    # "result":Ljava/lang/Object;
    :catch_0
    move-exception v0

    .line 121
    .local v0, "e":Ljava/lang/CloneNotSupportedException;
    const-string/jumbo v2, "DocumentService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Exception :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/CloneNotSupportedException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public createIntersection(Landroid/graphics/Rect;)Lorg/icepdf/index/core/pobjects/Rectangle2Df;
    .locals 7
    .param p1, "clip"    # Landroid/graphics/Rect;

    .prologue
    const/4 v6, 0x0

    .line 127
    if-nez p1, :cond_0

    .line 128
    new-instance v1, Lorg/icepdf/index/core/pobjects/Rectangle2Df;

    iget v2, p0, Lorg/icepdf/index/core/pobjects/Rectangle2Df;->x:F

    iget v3, p0, Lorg/icepdf/index/core/pobjects/Rectangle2Df;->y:F

    iget v4, p0, Lorg/icepdf/index/core/pobjects/Rectangle2Df;->width:F

    iget v5, p0, Lorg/icepdf/index/core/pobjects/Rectangle2Df;->height:F

    invoke-direct {v1, v2, v3, v4, v5}, Lorg/icepdf/index/core/pobjects/Rectangle2Df;-><init>(FFFF)V

    .line 132
    :goto_0
    return-object v1

    .line 129
    :cond_0
    new-instance v0, Landroid/graphics/Rect;

    iget v1, p0, Lorg/icepdf/index/core/pobjects/Rectangle2Df;->x:F

    float-to-int v1, v1

    iget v2, p0, Lorg/icepdf/index/core/pobjects/Rectangle2Df;->y:F

    float-to-int v2, v2

    iget v3, p0, Lorg/icepdf/index/core/pobjects/Rectangle2Df;->x:F

    iget v4, p0, Lorg/icepdf/index/core/pobjects/Rectangle2Df;->width:F

    add-float/2addr v3, v4

    float-to-int v3, v3

    iget v4, p0, Lorg/icepdf/index/core/pobjects/Rectangle2Df;->y:F

    iget v5, p0, Lorg/icepdf/index/core/pobjects/Rectangle2Df;->height:F

    add-float/2addr v4, v5

    float-to-int v4, v4

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 130
    .local v0, "rThis":Landroid/graphics/Rect;
    invoke-virtual {v0, p1}, Landroid/graphics/Rect;->intersect(Landroid/graphics/Rect;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 131
    new-instance v1, Lorg/icepdf/index/core/pobjects/Rectangle2Df;

    invoke-direct {v1, v6, v6, v6, v6}, Lorg/icepdf/index/core/pobjects/Rectangle2Df;-><init>(FFFF)V

    goto :goto_0

    .line 132
    :cond_1
    new-instance v1, Lorg/icepdf/index/core/pobjects/Rectangle2Df;

    iget v2, v0, Landroid/graphics/Rect;->left:I

    int-to-float v2, v2

    iget v3, v0, Landroid/graphics/Rect;->top:I

    int-to-float v3, v3

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v4

    int-to-float v4, v4

    iget v5, v0, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v6

    sub-int/2addr v5, v6

    int-to-float v5, v5

    invoke-direct {v1, v2, v3, v4, v5}, Lorg/icepdf/index/core/pobjects/Rectangle2Df;-><init>(FFFF)V

    goto :goto_0
.end method

.method public getHeight()F
    .locals 1

    .prologue
    .line 63
    iget v0, p0, Lorg/icepdf/index/core/pobjects/Rectangle2Df;->height:F

    return v0
.end method

.method public getMaxX()F
    .locals 2

    .prologue
    .line 93
    iget v0, p0, Lorg/icepdf/index/core/pobjects/Rectangle2Df;->width:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    .line 94
    iget v0, p0, Lorg/icepdf/index/core/pobjects/Rectangle2Df;->x:F

    iget v1, p0, Lorg/icepdf/index/core/pobjects/Rectangle2Df;->width:F

    add-float/2addr v0, v1

    .line 95
    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lorg/icepdf/index/core/pobjects/Rectangle2Df;->x:F

    goto :goto_0
.end method

.method public getMaxY()F
    .locals 2

    .prologue
    .line 98
    iget v0, p0, Lorg/icepdf/index/core/pobjects/Rectangle2Df;->height:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    .line 99
    iget v0, p0, Lorg/icepdf/index/core/pobjects/Rectangle2Df;->y:F

    iget v1, p0, Lorg/icepdf/index/core/pobjects/Rectangle2Df;->height:F

    add-float/2addr v0, v1

    .line 100
    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lorg/icepdf/index/core/pobjects/Rectangle2Df;->y:F

    goto :goto_0
.end method

.method public getMinX()F
    .locals 2

    .prologue
    .line 82
    iget v0, p0, Lorg/icepdf/index/core/pobjects/Rectangle2Df;->width:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    .line 83
    iget v0, p0, Lorg/icepdf/index/core/pobjects/Rectangle2Df;->x:F

    .line 84
    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lorg/icepdf/index/core/pobjects/Rectangle2Df;->x:F

    iget v1, p0, Lorg/icepdf/index/core/pobjects/Rectangle2Df;->width:F

    add-float/2addr v0, v1

    goto :goto_0
.end method

.method public getMinY()F
    .locals 2

    .prologue
    .line 87
    iget v0, p0, Lorg/icepdf/index/core/pobjects/Rectangle2Df;->height:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    .line 88
    iget v0, p0, Lorg/icepdf/index/core/pobjects/Rectangle2Df;->y:F

    .line 89
    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lorg/icepdf/index/core/pobjects/Rectangle2Df;->y:F

    iget v1, p0, Lorg/icepdf/index/core/pobjects/Rectangle2Df;->height:F

    add-float/2addr v0, v1

    goto :goto_0
.end method

.method public getWidth()F
    .locals 1

    .prologue
    .line 66
    iget v0, p0, Lorg/icepdf/index/core/pobjects/Rectangle2Df;->width:F

    return v0
.end method

.method public setRect(FFFF)V
    .locals 0
    .param p1, "x"    # F
    .param p2, "y"    # F
    .param p3, "width"    # F
    .param p4, "height"    # F

    .prologue
    .line 48
    iput p1, p0, Lorg/icepdf/index/core/pobjects/Rectangle2Df;->x:F

    .line 49
    iput p2, p0, Lorg/icepdf/index/core/pobjects/Rectangle2Df;->y:F

    .line 50
    iput p3, p0, Lorg/icepdf/index/core/pobjects/Rectangle2Df;->width:F

    .line 51
    iput p4, p0, Lorg/icepdf/index/core/pobjects/Rectangle2Df;->height:F

    .line 52
    return-void
.end method

.method public toRect()Landroid/graphics/Rect;
    .locals 6

    .prologue
    .line 55
    new-instance v0, Landroid/graphics/Rect;

    iget v1, p0, Lorg/icepdf/index/core/pobjects/Rectangle2Df;->x:F

    float-to-int v1, v1

    iget v2, p0, Lorg/icepdf/index/core/pobjects/Rectangle2Df;->y:F

    float-to-int v2, v2

    iget v3, p0, Lorg/icepdf/index/core/pobjects/Rectangle2Df;->x:F

    iget v4, p0, Lorg/icepdf/index/core/pobjects/Rectangle2Df;->width:F

    add-float/2addr v3, v4

    float-to-int v3, v3

    iget v4, p0, Lorg/icepdf/index/core/pobjects/Rectangle2Df;->y:F

    iget v5, p0, Lorg/icepdf/index/core/pobjects/Rectangle2Df;->height:F

    add-float/2addr v4, v5

    float-to-int v4, v4

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    return-object v0
.end method

.method public toRectF()Landroid/graphics/RectF;
    .locals 6

    .prologue
    .line 59
    new-instance v0, Landroid/graphics/RectF;

    iget v1, p0, Lorg/icepdf/index/core/pobjects/Rectangle2Df;->x:F

    iget v2, p0, Lorg/icepdf/index/core/pobjects/Rectangle2Df;->y:F

    iget v3, p0, Lorg/icepdf/index/core/pobjects/Rectangle2Df;->x:F

    iget v4, p0, Lorg/icepdf/index/core/pobjects/Rectangle2Df;->width:F

    add-float/2addr v3, v4

    iget v4, p0, Lorg/icepdf/index/core/pobjects/Rectangle2Df;->y:F

    iget v5, p0, Lorg/icepdf/index/core/pobjects/Rectangle2Df;->height:F

    add-float/2addr v4, v5

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    return-object v0
.end method
