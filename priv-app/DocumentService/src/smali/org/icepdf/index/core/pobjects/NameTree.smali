.class public Lorg/icepdf/index/core/pobjects/NameTree;
.super Lorg/icepdf/index/core/pobjects/Dictionary;
.source "NameTree.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/icepdf/index/core/pobjects/NameTree$Node;
    }
.end annotation


# instance fields
.field private root:Lorg/icepdf/index/core/pobjects/NameTree$Node;


# direct methods
.method public constructor <init>(Lorg/icepdf/index/core/util/Library;Lorg/icepdf/index/core/util/Hashtable;)V
    .locals 0
    .param p1, "l"    # Lorg/icepdf/index/core/util/Library;
    .param p2, "h"    # Lorg/icepdf/index/core/util/Hashtable;

    .prologue
    .line 47
    invoke-direct {p0, p1, p2}, Lorg/icepdf/index/core/pobjects/Dictionary;-><init>(Lorg/icepdf/index/core/util/Library;Lorg/icepdf/index/core/util/Hashtable;)V

    .line 48
    return-void
.end method


# virtual methods
.method public dispose()V
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/NameTree;->root:Lorg/icepdf/index/core/pobjects/NameTree$Node;

    invoke-virtual {v0}, Lorg/icepdf/index/core/pobjects/NameTree$Node;->dispose()V

    .line 66
    return-void
.end method

.method public init()V
    .locals 3

    .prologue
    .line 54
    iget-boolean v0, p0, Lorg/icepdf/index/core/pobjects/NameTree;->inited:Z

    if-eqz v0, :cond_0

    .line 59
    :goto_0
    return-void

    .line 57
    :cond_0
    new-instance v0, Lorg/icepdf/index/core/pobjects/NameTree$Node;

    iget-object v1, p0, Lorg/icepdf/index/core/pobjects/NameTree;->library:Lorg/icepdf/index/core/util/Library;

    iget-object v2, p0, Lorg/icepdf/index/core/pobjects/NameTree;->entries:Lorg/icepdf/index/core/util/Hashtable;

    invoke-direct {v0, v1, v2}, Lorg/icepdf/index/core/pobjects/NameTree$Node;-><init>(Lorg/icepdf/index/core/util/Library;Lorg/icepdf/index/core/util/Hashtable;)V

    iput-object v0, p0, Lorg/icepdf/index/core/pobjects/NameTree;->root:Lorg/icepdf/index/core/pobjects/NameTree$Node;

    .line 58
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/icepdf/index/core/pobjects/NameTree;->inited:Z

    goto :goto_0
.end method

.method public searchName(Ljava/lang/String;)Ljava/lang/Object;
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 77
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/NameTree;->root:Lorg/icepdf/index/core/pobjects/NameTree$Node;

    invoke-virtual {v0, p1}, Lorg/icepdf/index/core/pobjects/NameTree$Node;->searchName(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
