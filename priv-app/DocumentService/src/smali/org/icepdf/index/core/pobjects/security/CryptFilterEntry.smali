.class public Lorg/icepdf/index/core/pobjects/security/CryptFilterEntry;
.super Lorg/icepdf/index/core/pobjects/Dictionary;
.source "CryptFilterEntry.java"


# static fields
.field public static final TYPE:Lorg/icepdf/index/core/pobjects/Name;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 31
    new-instance v0, Lorg/icepdf/index/core/pobjects/Name;

    const-string/jumbo v1, "CryptFilter"

    invoke-direct {v0, v1}, Lorg/icepdf/index/core/pobjects/Name;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/icepdf/index/core/pobjects/security/CryptFilterEntry;->TYPE:Lorg/icepdf/index/core/pobjects/Name;

    return-void
.end method

.method public constructor <init>(Lorg/icepdf/index/core/util/Library;Lorg/icepdf/index/core/util/Hashtable;)V
    .locals 0
    .param p1, "library"    # Lorg/icepdf/index/core/util/Library;
    .param p2, "entries"    # Lorg/icepdf/index/core/util/Hashtable;

    .prologue
    .line 34
    invoke-direct {p0, p1, p2}, Lorg/icepdf/index/core/pobjects/Dictionary;-><init>(Lorg/icepdf/index/core/util/Library;Lorg/icepdf/index/core/util/Hashtable;)V

    .line 35
    return-void
.end method


# virtual methods
.method public getAuthEvent()Lorg/icepdf/index/core/pobjects/Name;
    .locals 4

    .prologue
    .line 100
    iget-object v1, p0, Lorg/icepdf/index/core/pobjects/security/CryptFilterEntry;->library:Lorg/icepdf/index/core/util/Library;

    iget-object v2, p0, Lorg/icepdf/index/core/pobjects/security/CryptFilterEntry;->entries:Lorg/icepdf/index/core/util/Hashtable;

    const-string/jumbo v3, "AuthEvent"

    invoke-virtual {v1, v2, v3}, Lorg/icepdf/index/core/util/Library;->getObject(Lorg/icepdf/index/core/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 101
    .local v0, "tmp":Ljava/lang/Object;
    instance-of v1, v0, Lorg/icepdf/index/core/pobjects/Name;

    if-eqz v1, :cond_0

    .line 102
    check-cast v0, Lorg/icepdf/index/core/pobjects/Name;

    .line 104
    .end local v0    # "tmp":Ljava/lang/Object;
    :goto_0
    return-object v0

    .restart local v0    # "tmp":Ljava/lang/Object;
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getCryptFilterMethod()Lorg/icepdf/index/core/pobjects/Name;
    .locals 4

    .prologue
    .line 76
    iget-object v1, p0, Lorg/icepdf/index/core/pobjects/security/CryptFilterEntry;->library:Lorg/icepdf/index/core/util/Library;

    iget-object v2, p0, Lorg/icepdf/index/core/pobjects/security/CryptFilterEntry;->entries:Lorg/icepdf/index/core/util/Hashtable;

    const-string/jumbo v3, "CFM"

    invoke-virtual {v1, v2, v3}, Lorg/icepdf/index/core/util/Library;->getObject(Lorg/icepdf/index/core/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 77
    .local v0, "tmp":Ljava/lang/Object;
    instance-of v1, v0, Lorg/icepdf/index/core/pobjects/Name;

    if-eqz v1, :cond_0

    .line 78
    check-cast v0, Lorg/icepdf/index/core/pobjects/Name;

    .line 80
    .end local v0    # "tmp":Ljava/lang/Object;
    :goto_0
    return-object v0

    .restart local v0    # "tmp":Ljava/lang/Object;
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getLength()I
    .locals 4

    .prologue
    .line 118
    iget-object v1, p0, Lorg/icepdf/index/core/pobjects/security/CryptFilterEntry;->library:Lorg/icepdf/index/core/util/Library;

    iget-object v2, p0, Lorg/icepdf/index/core/pobjects/security/CryptFilterEntry;->entries:Lorg/icepdf/index/core/util/Hashtable;

    const-string/jumbo v3, "Length"

    invoke-virtual {v1, v2, v3}, Lorg/icepdf/index/core/util/Library;->getInt(Lorg/icepdf/index/core/util/Hashtable;Ljava/lang/String;)I

    move-result v0

    .line 119
    .local v0, "length":I
    mul-int/lit8 v1, v0, 0x8

    const/16 v2, 0x80

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    return v1
.end method

.method public getType()Lorg/icepdf/index/core/pobjects/Name;
    .locals 1

    .prologue
    .line 43
    sget-object v0, Lorg/icepdf/index/core/pobjects/security/CryptFilterEntry;->TYPE:Lorg/icepdf/index/core/pobjects/Name;

    return-object v0
.end method
