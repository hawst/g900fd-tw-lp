.class public Lorg/icepdf/index/core/io/SeekableInputConstrainedWrapper;
.super Ljava/io/InputStream;
.source "SeekableInputConstrainedWrapper.java"


# instance fields
.field private filePositionBeforeUse:J

.field private filePositionOfStreamData:J

.field private lengthOfStreamData:J

.field private streamDataInput:Lorg/icepdf/index/core/io/SeekableInput;

.field private takeOwnershipOfStreamDataInput:Z

.field private usedYet:Z


# direct methods
.method public constructor <init>(Lorg/icepdf/index/core/io/SeekableInput;JJZ)V
    .locals 2
    .param p1, "in"    # Lorg/icepdf/index/core/io/SeekableInput;
    .param p2, "offset"    # J
    .param p4, "length"    # J
    .param p6, "takeOwnership"    # Z

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    .line 34
    iput-object p1, p0, Lorg/icepdf/index/core/io/SeekableInputConstrainedWrapper;->streamDataInput:Lorg/icepdf/index/core/io/SeekableInput;

    .line 35
    iput-wide p2, p0, Lorg/icepdf/index/core/io/SeekableInputConstrainedWrapper;->filePositionOfStreamData:J

    .line 36
    iput-wide p4, p0, Lorg/icepdf/index/core/io/SeekableInputConstrainedWrapper;->lengthOfStreamData:J

    .line 37
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lorg/icepdf/index/core/io/SeekableInputConstrainedWrapper;->filePositionBeforeUse:J

    .line 38
    iput-boolean p6, p0, Lorg/icepdf/index/core/io/SeekableInputConstrainedWrapper;->takeOwnershipOfStreamDataInput:Z

    .line 39
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/icepdf/index/core/io/SeekableInputConstrainedWrapper;->usedYet:Z

    .line 40
    return-void
.end method

.method private endCurrentUse()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 75
    iget-boolean v0, p0, Lorg/icepdf/index/core/io/SeekableInputConstrainedWrapper;->usedYet:Z

    if-eqz v0, :cond_0

    .line 76
    iget-object v0, p0, Lorg/icepdf/index/core/io/SeekableInputConstrainedWrapper;->streamDataInput:Lorg/icepdf/index/core/io/SeekableInput;

    iget-wide v2, p0, Lorg/icepdf/index/core/io/SeekableInputConstrainedWrapper;->filePositionBeforeUse:J

    invoke-interface {v0, v2, v3}, Lorg/icepdf/index/core/io/SeekableInput;->seekAbsolute(J)V

    .line 77
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/icepdf/index/core/io/SeekableInputConstrainedWrapper;->usedYet:Z

    .line 79
    :cond_0
    invoke-virtual {p0}, Lorg/icepdf/index/core/io/SeekableInputConstrainedWrapper;->endThreadAccess()V

    .line 81
    return-void
.end method

.method private ensureReadyOnFirstUse()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 62
    invoke-virtual {p0}, Lorg/icepdf/index/core/io/SeekableInputConstrainedWrapper;->beginThreadAccess()V

    .line 63
    iget-boolean v0, p0, Lorg/icepdf/index/core/io/SeekableInputConstrainedWrapper;->usedYet:Z

    if-eqz v0, :cond_0

    .line 71
    :goto_0
    return-void

    .line 65
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/icepdf/index/core/io/SeekableInputConstrainedWrapper;->usedYet:Z

    .line 68
    iget-object v0, p0, Lorg/icepdf/index/core/io/SeekableInputConstrainedWrapper;->streamDataInput:Lorg/icepdf/index/core/io/SeekableInput;

    invoke-interface {v0}, Lorg/icepdf/index/core/io/SeekableInput;->getAbsolutePosition()J

    move-result-wide v0

    iput-wide v0, p0, Lorg/icepdf/index/core/io/SeekableInputConstrainedWrapper;->filePositionBeforeUse:J

    .line 69
    iget-object v0, p0, Lorg/icepdf/index/core/io/SeekableInputConstrainedWrapper;->streamDataInput:Lorg/icepdf/index/core/io/SeekableInput;

    iget-wide v2, p0, Lorg/icepdf/index/core/io/SeekableInputConstrainedWrapper;->filePositionOfStreamData:J

    invoke-interface {v0, v2, v3}, Lorg/icepdf/index/core/io/SeekableInput;->seekAbsolute(J)V

    goto :goto_0
.end method

.method private getBytesRemaining()J
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 84
    iget-object v4, p0, Lorg/icepdf/index/core/io/SeekableInputConstrainedWrapper;->streamDataInput:Lorg/icepdf/index/core/io/SeekableInput;

    invoke-interface {v4}, Lorg/icepdf/index/core/io/SeekableInput;->getAbsolutePosition()J

    move-result-wide v0

    .line 85
    .local v0, "absPos":J
    iget-wide v4, p0, Lorg/icepdf/index/core/io/SeekableInputConstrainedWrapper;->filePositionOfStreamData:J

    cmp-long v4, v0, v4

    if-gez v4, :cond_0

    .line 86
    const-wide/16 v4, -0x1

    .line 88
    :goto_0
    return-wide v4

    .line 87
    :cond_0
    iget-wide v4, p0, Lorg/icepdf/index/core/io/SeekableInputConstrainedWrapper;->filePositionOfStreamData:J

    iget-wide v6, p0, Lorg/icepdf/index/core/io/SeekableInputConstrainedWrapper;->lengthOfStreamData:J

    add-long v2, v4, v6

    .line 88
    .local v2, "end":J
    sub-long v4, v2, v0

    goto :goto_0
.end method


# virtual methods
.method public available()I
    .locals 1

    .prologue
    .line 130
    const/4 v0, 0x0

    return v0
.end method

.method public beginThreadAccess()V
    .locals 1

    .prologue
    .line 203
    iget-object v0, p0, Lorg/icepdf/index/core/io/SeekableInputConstrainedWrapper;->streamDataInput:Lorg/icepdf/index/core/io/SeekableInput;

    if-eqz v0, :cond_0

    .line 204
    iget-object v0, p0, Lorg/icepdf/index/core/io/SeekableInputConstrainedWrapper;->streamDataInput:Lorg/icepdf/index/core/io/SeekableInput;

    invoke-interface {v0}, Lorg/icepdf/index/core/io/SeekableInput;->beginThreadAccess()V

    .line 205
    :cond_0
    return-void
.end method

.method public close()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 125
    invoke-virtual {p0}, Lorg/icepdf/index/core/io/SeekableInputConstrainedWrapper;->beginThreadAccess()V

    .line 126
    invoke-direct {p0}, Lorg/icepdf/index/core/io/SeekableInputConstrainedWrapper;->endCurrentUse()V

    .line 127
    return-void
.end method

.method public dispose()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 47
    invoke-virtual {p0}, Lorg/icepdf/index/core/io/SeekableInputConstrainedWrapper;->beginThreadAccess()V

    .line 49
    iget-boolean v0, p0, Lorg/icepdf/index/core/io/SeekableInputConstrainedWrapper;->takeOwnershipOfStreamDataInput:Z

    if-eqz v0, :cond_1

    .line 50
    iget-object v0, p0, Lorg/icepdf/index/core/io/SeekableInputConstrainedWrapper;->streamDataInput:Lorg/icepdf/index/core/io/SeekableInput;

    if-eqz v0, :cond_0

    .line 51
    iget-object v0, p0, Lorg/icepdf/index/core/io/SeekableInputConstrainedWrapper;->streamDataInput:Lorg/icepdf/index/core/io/SeekableInput;

    invoke-interface {v0}, Lorg/icepdf/index/core/io/SeekableInput;->close()V

    .line 52
    invoke-virtual {p0}, Lorg/icepdf/index/core/io/SeekableInputConstrainedWrapper;->endThreadAccess()V

    .line 53
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/icepdf/index/core/io/SeekableInputConstrainedWrapper;->streamDataInput:Lorg/icepdf/index/core/io/SeekableInput;

    .line 59
    :cond_0
    :goto_0
    return-void

    .line 56
    :cond_1
    invoke-direct {p0}, Lorg/icepdf/index/core/io/SeekableInputConstrainedWrapper;->endCurrentUse()V

    goto :goto_0
.end method

.method public endThreadAccess()V
    .locals 1

    .prologue
    .line 208
    iget-object v0, p0, Lorg/icepdf/index/core/io/SeekableInputConstrainedWrapper;->streamDataInput:Lorg/icepdf/index/core/io/SeekableInput;

    if-eqz v0, :cond_0

    .line 209
    iget-object v0, p0, Lorg/icepdf/index/core/io/SeekableInputConstrainedWrapper;->streamDataInput:Lorg/icepdf/index/core/io/SeekableInput;

    invoke-interface {v0}, Lorg/icepdf/index/core/io/SeekableInput;->endThreadAccess()V

    .line 210
    :cond_0
    return-void
.end method

.method public getInputStream()Ljava/io/InputStream;
    .locals 0

    .prologue
    .line 198
    return-object p0
.end method

.method public getLength()J
    .locals 2

    .prologue
    .line 192
    iget-wide v0, p0, Lorg/icepdf/index/core/io/SeekableInputConstrainedWrapper;->lengthOfStreamData:J

    return-wide v0
.end method

.method public mark(I)V
    .locals 0
    .param p1, "readLimit"    # I

    .prologue
    .line 134
    return-void
.end method

.method public markSupported()Z
    .locals 1

    .prologue
    .line 137
    const/4 v0, 0x0

    return v0
.end method

.method public prepareForCurrentUse()V
    .locals 1

    .prologue
    .line 43
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/icepdf/index/core/io/SeekableInputConstrainedWrapper;->usedYet:Z

    .line 44
    return-void
.end method

.method public read()I
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 98
    invoke-direct {p0}, Lorg/icepdf/index/core/io/SeekableInputConstrainedWrapper;->ensureReadyOnFirstUse()V

    .line 99
    invoke-direct {p0}, Lorg/icepdf/index/core/io/SeekableInputConstrainedWrapper;->getBytesRemaining()J

    move-result-wide v0

    .line 101
    .local v0, "remain":J
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-gtz v2, :cond_0

    .line 102
    const/4 v2, -0x1

    .line 103
    :goto_0
    return v2

    :cond_0
    iget-object v2, p0, Lorg/icepdf/index/core/io/SeekableInputConstrainedWrapper;->streamDataInput:Lorg/icepdf/index/core/io/SeekableInput;

    invoke-interface {v2}, Lorg/icepdf/index/core/io/SeekableInput;->read()I

    move-result v2

    goto :goto_0
.end method

.method public read([B)I
    .locals 2
    .param p1, "buffer"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 107
    const/4 v0, 0x0

    array-length v1, p1

    invoke-virtual {p0, p1, v0, v1}, Lorg/icepdf/index/core/io/SeekableInputConstrainedWrapper;->read([BII)I

    move-result v0

    return v0
.end method

.method public read([BII)I
    .locals 6
    .param p1, "buffer"    # [B
    .param p2, "offset"    # I
    .param p3, "length"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 111
    invoke-direct {p0}, Lorg/icepdf/index/core/io/SeekableInputConstrainedWrapper;->ensureReadyOnFirstUse()V

    .line 112
    invoke-direct {p0}, Lorg/icepdf/index/core/io/SeekableInputConstrainedWrapper;->getBytesRemaining()J

    move-result-wide v0

    .line 114
    .local v0, "remain":J
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-gtz v2, :cond_0

    .line 115
    const/4 v2, -0x1

    .line 117
    :goto_0
    return v2

    .line 116
    :cond_0
    int-to-long v2, p3

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v2

    const-wide/32 v4, 0x7fffffff

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v2

    long-to-int p3, v2

    .line 117
    iget-object v2, p0, Lorg/icepdf/index/core/io/SeekableInputConstrainedWrapper;->streamDataInput:Lorg/icepdf/index/core/io/SeekableInput;

    invoke-interface {v2, p1, p2, p3}, Lorg/icepdf/index/core/io/SeekableInput;->read([BII)I

    move-result v2

    goto :goto_0
.end method

.method public reset()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 141
    return-void
.end method

.method public seekAbsolute(J)V
    .locals 3
    .param p1, "absolutePosition"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 158
    invoke-direct {p0}, Lorg/icepdf/index/core/io/SeekableInputConstrainedWrapper;->ensureReadyOnFirstUse()V

    .line 161
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-gez v0, :cond_0

    .line 162
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Attempt to absolutely seek to negative location: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 164
    :cond_0
    iget-wide v0, p0, Lorg/icepdf/index/core/io/SeekableInputConstrainedWrapper;->filePositionOfStreamData:J

    add-long/2addr p1, v0

    .line 165
    iget-object v0, p0, Lorg/icepdf/index/core/io/SeekableInputConstrainedWrapper;->streamDataInput:Lorg/icepdf/index/core/io/SeekableInput;

    invoke-interface {v0, p1, p2}, Lorg/icepdf/index/core/io/SeekableInput;->seekAbsolute(J)V

    .line 166
    return-void
.end method

.method public seekEnd()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 179
    invoke-direct {p0}, Lorg/icepdf/index/core/io/SeekableInputConstrainedWrapper;->ensureReadyOnFirstUse()V

    .line 180
    iget-object v0, p0, Lorg/icepdf/index/core/io/SeekableInputConstrainedWrapper;->streamDataInput:Lorg/icepdf/index/core/io/SeekableInput;

    iget-wide v2, p0, Lorg/icepdf/index/core/io/SeekableInputConstrainedWrapper;->filePositionOfStreamData:J

    iget-wide v4, p0, Lorg/icepdf/index/core/io/SeekableInputConstrainedWrapper;->lengthOfStreamData:J

    add-long/2addr v2, v4

    invoke-interface {v0, v2, v3}, Lorg/icepdf/index/core/io/SeekableInput;->seekAbsolute(J)V

    .line 181
    return-void
.end method

.method public seekRelative(J)V
    .locals 5
    .param p1, "relativeOffset"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 169
    invoke-direct {p0}, Lorg/icepdf/index/core/io/SeekableInputConstrainedWrapper;->ensureReadyOnFirstUse()V

    .line 170
    iget-object v2, p0, Lorg/icepdf/index/core/io/SeekableInputConstrainedWrapper;->streamDataInput:Lorg/icepdf/index/core/io/SeekableInput;

    invoke-interface {v2}, Lorg/icepdf/index/core/io/SeekableInput;->getAbsolutePosition()J

    move-result-wide v0

    .line 171
    .local v0, "pos":J
    add-long/2addr v0, p1

    .line 172
    iget-wide v2, p0, Lorg/icepdf/index/core/io/SeekableInputConstrainedWrapper;->filePositionOfStreamData:J

    cmp-long v2, v0, v2

    if-gez v2, :cond_0

    .line 173
    iget-wide v0, p0, Lorg/icepdf/index/core/io/SeekableInputConstrainedWrapper;->filePositionOfStreamData:J

    .line 175
    :cond_0
    iget-object v2, p0, Lorg/icepdf/index/core/io/SeekableInputConstrainedWrapper;->streamDataInput:Lorg/icepdf/index/core/io/SeekableInput;

    invoke-interface {v2, v0, v1}, Lorg/icepdf/index/core/io/SeekableInput;->seekAbsolute(J)V

    .line 176
    return-void
.end method

.method public skip(J)J
    .locals 7
    .param p1, "n"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 144
    invoke-direct {p0}, Lorg/icepdf/index/core/io/SeekableInputConstrainedWrapper;->ensureReadyOnFirstUse()V

    .line 145
    invoke-direct {p0}, Lorg/icepdf/index/core/io/SeekableInputConstrainedWrapper;->getBytesRemaining()J

    move-result-wide v0

    .line 146
    .local v0, "remain":J
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-gtz v2, :cond_0

    .line 147
    const-wide/16 v2, -0x1

    .line 149
    :goto_0
    return-wide v2

    .line 148
    :cond_0
    invoke-static {v0, v1, p1, p2}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v2

    const-wide/32 v4, 0x7fffffff

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v2

    long-to-int v2, v2

    int-to-long p1, v2

    .line 149
    iget-object v2, p0, Lorg/icepdf/index/core/io/SeekableInputConstrainedWrapper;->streamDataInput:Lorg/icepdf/index/core/io/SeekableInput;

    invoke-interface {v2, p1, p2}, Lorg/icepdf/index/core/io/SeekableInput;->skip(J)J

    move-result-wide v2

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 214
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 215
    .local v0, "sb":Ljava/lang/StringBuffer;
    invoke-super {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 216
    const-string/jumbo v1, " ( "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 217
    const-string/jumbo v1, "pos="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-wide v2, p0, Lorg/icepdf/index/core/io/SeekableInputConstrainedWrapper;->filePositionOfStreamData:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 218
    const-string/jumbo v1, "len="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-wide v2, p0, Lorg/icepdf/index/core/io/SeekableInputConstrainedWrapper;->lengthOfStreamData:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 219
    const-string/jumbo v1, "posToRestore="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-wide v2, p0, Lorg/icepdf/index/core/io/SeekableInputConstrainedWrapper;->filePositionBeforeUse:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 220
    const-string/jumbo v1, "own="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-boolean v2, p0, Lorg/icepdf/index/core/io/SeekableInputConstrainedWrapper;->takeOwnershipOfStreamDataInput:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 221
    const-string/jumbo v1, "usedYet="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-boolean v2, p0, Lorg/icepdf/index/core/io/SeekableInputConstrainedWrapper;->usedYet:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    .line 222
    const-string/jumbo v1, " ) "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 223
    const-string/jumbo v1, ": "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 224
    iget-object v1, p0, Lorg/icepdf/index/core/io/SeekableInputConstrainedWrapper;->streamDataInput:Lorg/icepdf/index/core/io/SeekableInput;

    if-nez v1, :cond_0

    .line 225
    const-string/jumbo v1, "null "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 228
    :goto_0
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 227
    :cond_0
    iget-object v1, p0, Lorg/icepdf/index/core/io/SeekableInputConstrainedWrapper;->streamDataInput:Lorg/icepdf/index/core/io/SeekableInput;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0
.end method
