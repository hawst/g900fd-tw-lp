.class public final Lorg/icepdf/index/core/util/ContentParser$CustomStack;
.super Ljava/util/Stack;
.source "ContentParser.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/icepdf/index/core/util/ContentParser;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1c
    name = "CustomStack"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/Stack",
        "<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# static fields
.field private static final serialVersionUID:J = 0x599df8db7683ef54L


# instance fields
.field max_size:I

.field tempObjArray:[Ljava/lang/Object;


# direct methods
.method public constructor <init>(I)V
    .locals 1
    .param p1, "maximumSize"    # I

    .prologue
    .line 306
    invoke-direct {p0}, Ljava/util/Stack;-><init>()V

    .line 303
    const/4 v0, 0x2

    iput v0, p0, Lorg/icepdf/index/core/util/ContentParser$CustomStack;->max_size:I

    .line 307
    iput p1, p0, Lorg/icepdf/index/core/util/ContentParser$CustomStack;->max_size:I

    .line 308
    iget v0, p0, Lorg/icepdf/index/core/util/ContentParser$CustomStack;->max_size:I

    new-array v0, v0, [Ljava/lang/Object;

    iput-object v0, p0, Lorg/icepdf/index/core/util/ContentParser$CustomStack;->tempObjArray:[Ljava/lang/Object;

    .line 309
    return-void
.end method


# virtual methods
.method public declared-synchronized equals(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 315
    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1}, Ljava/util/Stack;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized hashCode()I
    .locals 1

    .prologue
    .line 321
    monitor-enter p0

    :try_start_0
    invoke-super {p0}, Ljava/util/Stack;->hashCode()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public push(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p1, "object"    # Ljava/lang/Object;

    .prologue
    .line 326
    invoke-virtual {p0}, Lorg/icepdf/index/core/util/ContentParser$CustomStack;->size()I

    move-result v0

    .line 327
    .local v0, "size":I
    iget v1, p0, Lorg/icepdf/index/core/util/ContentParser$CustomStack;->max_size:I

    if-lt v0, v1, :cond_0

    .line 328
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lorg/icepdf/index/core/util/ContentParser$CustomStack;->removeElementAt(I)V

    .line 331
    :cond_0
    invoke-super {p0, p1}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    return-object v1
.end method
