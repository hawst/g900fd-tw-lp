.class public Lorg/icepdf/index/core/pobjects/graphics/text/WordText;
.super Lorg/icepdf/index/core/pobjects/graphics/text/AbstractText;
.source "WordText.java"


# static fields
.field private static final logger:Ljava/util/logging/Logger;

.field public static spaceFraction:I


# instance fields
.field private currentGlyph:Lorg/icepdf/index/core/pobjects/graphics/text/GlyphText;

.field private glyphs:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lorg/icepdf/index/core/pobjects/graphics/text/GlyphText;",
            ">;"
        }
    .end annotation
.end field

.field private isWhiteSpace:Z

.field private text:Ljava/lang/StringBuilder;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 42
    const-class v1, Lorg/icepdf/index/core/pobjects/graphics/text/WordText;

    invoke-virtual {v1}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v1

    sput-object v1, Lorg/icepdf/index/core/pobjects/graphics/text/WordText;->logger:Ljava/util/logging/Logger;

    .line 52
    :try_start_0
    const-string/jumbo v1, "org.icepdf.core.views.page.text.spaceFraction"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lorg/icepdf/index/core/util/Defs;->sysPropertyInt(Ljava/lang/String;I)I

    move-result v1

    sput v1, Lorg/icepdf/index/core/pobjects/graphics/text/WordText;->spaceFraction:I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 59
    .local v0, "e":Ljava/lang/NumberFormatException;
    :cond_0
    :goto_0
    return-void

    .line 54
    .end local v0    # "e":Ljava/lang/NumberFormatException;
    :catch_0
    move-exception v0

    .line 55
    .restart local v0    # "e":Ljava/lang/NumberFormatException;
    sget-object v1, Lorg/icepdf/index/core/pobjects/graphics/text/WordText;->logger:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    invoke-virtual {v1, v2}, Ljava/util/logging/Logger;->isLoggable(Ljava/util/logging/Level;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 56
    sget-object v1, Lorg/icepdf/index/core/pobjects/graphics/text/WordText;->logger:Ljava/util/logging/Logger;

    const-string/jumbo v2, "Error reading text selection colour"

    invoke-virtual {v1, v2}, Ljava/util/logging/Logger;->warning(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 69
    invoke-direct {p0}, Lorg/icepdf/index/core/pobjects/graphics/text/AbstractText;-><init>()V

    .line 70
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lorg/icepdf/index/core/pobjects/graphics/text/WordText;->text:Ljava/lang/StringBuilder;

    .line 71
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lorg/icepdf/index/core/pobjects/graphics/text/WordText;->glyphs:Ljava/util/ArrayList;

    .line 72
    return-void
.end method

.method protected static detectPunctuation(Lorg/icepdf/index/core/pobjects/graphics/text/GlyphText;)Z
    .locals 4
    .param p0, "sprite"    # Lorg/icepdf/index/core/pobjects/graphics/text/GlyphText;

    .prologue
    const/4 v2, 0x0

    .line 99
    invoke-virtual {p0}, Lorg/icepdf/index/core/pobjects/graphics/text/GlyphText;->getUnicode()Ljava/lang/String;

    move-result-object v1

    .line 100
    .local v1, "glyphText":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 101
    invoke-virtual {p0}, Lorg/icepdf/index/core/pobjects/graphics/text/GlyphText;->getUnicode()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 102
    .local v0, "c":I
    invoke-static {v0}, Lorg/icepdf/index/core/pobjects/graphics/text/WordText;->isPunctuation(I)Z

    move-result v2

    .line 104
    .end local v0    # "c":I
    :cond_0
    return v2
.end method

.method protected static detectWhiteSpace(Lorg/icepdf/index/core/pobjects/graphics/text/GlyphText;)Z
    .locals 4
    .param p0, "sprite"    # Lorg/icepdf/index/core/pobjects/graphics/text/GlyphText;

    .prologue
    const/4 v2, 0x0

    .line 109
    invoke-virtual {p0}, Lorg/icepdf/index/core/pobjects/graphics/text/GlyphText;->getUnicode()Ljava/lang/String;

    move-result-object v1

    .line 110
    .local v1, "glyphText":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 111
    invoke-virtual {p0}, Lorg/icepdf/index/core/pobjects/graphics/text/GlyphText;->getUnicode()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 112
    .local v0, "c":I
    invoke-static {v0}, Lorg/icepdf/index/core/pobjects/graphics/text/WordText;->isWhiteSpace(I)Z

    move-result v2

    .line 114
    .end local v0    # "c":I
    :cond_0
    return v2
.end method

.method public static isPunctuation(I)Z
    .locals 1
    .param p0, "c"    # I

    .prologue
    .line 119
    const/16 v0, 0x2e

    if-eq p0, v0, :cond_0

    const/16 v0, 0x2c

    if-eq p0, v0, :cond_0

    const/16 v0, 0x3f

    if-eq p0, v0, :cond_0

    const/16 v0, 0x21

    if-eq p0, v0, :cond_0

    const/16 v0, 0x3a

    if-eq p0, v0, :cond_0

    const/16 v0, 0x3b

    if-eq p0, v0, :cond_0

    const/16 v0, 0x22

    if-eq p0, v0, :cond_0

    const/16 v0, 0x27

    if-eq p0, v0, :cond_0

    const/16 v0, 0x2f

    if-eq p0, v0, :cond_0

    const/16 v0, 0x5c

    if-eq p0, v0, :cond_0

    const/16 v0, 0x60

    if-eq p0, v0, :cond_0

    const/16 v0, 0x23

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isWhiteSpace(I)Z
    .locals 1
    .param p0, "c"    # I

    .prologue
    .line 125
    const/16 v0, 0x20

    if-eq p0, v0, :cond_0

    const/16 v0, 0x9

    if-eq p0, v0, :cond_0

    const/16 v0, 0xd

    if-eq p0, v0, :cond_0

    const/16 v0, 0xa

    if-eq p0, v0, :cond_0

    const/16 v0, 0xc

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected addText(Lorg/icepdf/index/core/pobjects/graphics/text/GlyphText;)V
    .locals 6
    .param p1, "sprite"    # Lorg/icepdf/index/core/pobjects/graphics/text/GlyphText;

    .prologue
    .line 172
    iget-object v1, p0, Lorg/icepdf/index/core/pobjects/graphics/text/WordText;->glyphs:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 174
    iput-object p1, p0, Lorg/icepdf/index/core/pobjects/graphics/text/WordText;->currentGlyph:Lorg/icepdf/index/core/pobjects/graphics/text/GlyphText;

    .line 177
    iget-object v1, p0, Lorg/icepdf/index/core/pobjects/graphics/text/WordText;->bounds:Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;

    if-nez v1, :cond_0

    .line 178
    invoke-virtual {p1}, Lorg/icepdf/index/core/pobjects/graphics/text/GlyphText;->getBounds()Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;

    move-result-object v0

    .line 179
    .local v0, "rect":Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;
    new-instance v1, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;

    iget v2, v0, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;->x:F

    iget v3, v0, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;->y:F

    iget v4, v0, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;->width:F

    iget v5, v0, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;->height:F

    invoke-direct {v1, v2, v3, v4, v5}, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;-><init>(FFFF)V

    iput-object v1, p0, Lorg/icepdf/index/core/pobjects/graphics/text/WordText;->bounds:Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;

    .line 185
    .end local v0    # "rect":Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;
    :goto_0
    iget-object v1, p0, Lorg/icepdf/index/core/pobjects/graphics/text/WordText;->text:Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lorg/icepdf/index/core/pobjects/graphics/text/GlyphText;->getUnicode()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 186
    return-void

    .line 181
    :cond_0
    iget-object v1, p0, Lorg/icepdf/index/core/pobjects/graphics/text/WordText;->bounds:Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;

    invoke-virtual {p1}, Lorg/icepdf/index/core/pobjects/graphics/text/GlyphText;->getBounds()Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;

    move-result-object v2

    invoke-virtual {v1, v2}, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;->add(Lorg/icepdf/index/java/awt/geom/Rectangle2D;)V

    goto :goto_0
.end method

.method protected buildSpaceWord(Lorg/icepdf/index/core/pobjects/graphics/text/GlyphText;)Lorg/icepdf/index/core/pobjects/graphics/text/WordText;
    .locals 21
    .param p1, "sprite"    # Lorg/icepdf/index/core/pobjects/graphics/text/GlyphText;

    .prologue
    .line 132
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/icepdf/index/core/pobjects/graphics/text/WordText;->currentGlyph:Lorg/icepdf/index/core/pobjects/graphics/text/GlyphText;

    invoke-virtual {v3}, Lorg/icepdf/index/core/pobjects/graphics/text/GlyphText;->getBounds()Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;

    move-result-object v8

    .line 133
    .local v8, "bounds1":Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;
    invoke-virtual/range {p1 .. p1}, Lorg/icepdf/index/core/pobjects/graphics/text/GlyphText;->getBounds()Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;

    move-result-object v9

    .line 134
    .local v9, "bounds2":Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;
    iget v3, v9, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;->x:F

    iget v4, v8, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;->x:F

    iget v5, v8, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;->width:F

    add-float/2addr v4, v5

    sub-float v14, v3, v4

    .line 137
    .local v14, "space":F
    iget v3, v8, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;->width:F

    iget v4, v9, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;->width:F

    invoke-static {v3, v4}, Ljava/lang/Math;->max(FF)F

    move-result v3

    const/high16 v4, 0x40000000    # 2.0f

    div-float v11, v3, v4

    .line 138
    .local v11, "maxWidth":F
    div-float v3, v14, v11

    float-to-int v0, v3

    move/from16 v17, v0

    .line 139
    .local v17, "spaces":I
    const/4 v3, 0x1

    move/from16 v0, v17

    if-ge v0, v3, :cond_0

    const/16 v17, 0x1

    .line 140
    :cond_0
    move/from16 v0, v17

    int-to-float v3, v0

    div-float v16, v14, v3

    .line 142
    .local v16, "spaceWidth":F
    new-instance v18, Lorg/icepdf/index/core/pobjects/graphics/text/WordText;

    invoke-direct/range {v18 .. v18}, Lorg/icepdf/index/core/pobjects/graphics/text/WordText;-><init>()V

    .line 143
    .local v18, "whiteSpace":Lorg/icepdf/index/core/pobjects/graphics/text/WordText;
    iget v3, v8, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;->x:F

    iget v4, v8, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;->width:F

    add-float/2addr v3, v4

    float-to-double v12, v3

    .line 146
    .local v12, "offset":D
    new-instance v15, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;

    iget v3, v8, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;->x:F

    iget v4, v8, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;->width:F

    add-float/2addr v3, v4

    iget v4, v8, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;->y:F

    iget v5, v8, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;->height:F

    move/from16 v0, v16

    invoke-direct {v15, v3, v4, v0, v5}, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;-><init>(FFFF)V

    .line 154
    .local v15, "spaceBounds":Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_0
    move/from16 v0, v17

    if-ge v10, v0, :cond_1

    const/16 v3, 0x32

    if-ge v10, v3, :cond_1

    .line 155
    new-instance v2, Lorg/icepdf/index/core/pobjects/graphics/text/GlyphText;

    double-to-float v3, v12

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/icepdf/index/core/pobjects/graphics/text/WordText;->currentGlyph:Lorg/icepdf/index/core/pobjects/graphics/text/GlyphText;

    invoke-virtual {v4}, Lorg/icepdf/index/core/pobjects/graphics/text/GlyphText;->getY()F

    move-result v4

    new-instance v5, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;

    iget v6, v15, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;->x:F

    iget v7, v15, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;->y:F

    iget v0, v15, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;->width:F

    move/from16 v19, v0

    iget v0, v15, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;->height:F

    move/from16 v20, v0

    move/from16 v0, v19

    move/from16 v1, v20

    invoke-direct {v5, v6, v7, v0, v1}, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;-><init>(FFFF)V

    const/16 v6, 0x20

    invoke-static {v6}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v6

    const/16 v7, 0x20

    invoke-static {v7}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v7

    invoke-direct/range {v2 .. v7}, Lorg/icepdf/index/core/pobjects/graphics/text/GlyphText;-><init>(FFLorg/icepdf/index/java/awt/geom/Rectangle2D$Float;Ljava/lang/String;Ljava/lang/String;)V

    .line 162
    .local v2, "spaceText":Lorg/icepdf/index/core/pobjects/graphics/text/GlyphText;
    iget v3, v15, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;->x:F

    iget v4, v15, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;->width:F

    add-float/2addr v3, v4

    iput v3, v15, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;->x:F

    .line 163
    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Lorg/icepdf/index/core/pobjects/graphics/text/WordText;->addText(Lorg/icepdf/index/core/pobjects/graphics/text/GlyphText;)V

    .line 164
    const/4 v3, 0x1

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Lorg/icepdf/index/core/pobjects/graphics/text/WordText;->setWhiteSpace(Z)V

    .line 165
    move/from16 v0, v16

    float-to-double v4, v0

    add-double/2addr v12, v4

    .line 154
    add-int/lit8 v10, v10, 0x1

    goto :goto_0

    .line 167
    .end local v2    # "spaceText":Lorg/icepdf/index/core/pobjects/graphics/text/GlyphText;
    :cond_1
    return-object v18
.end method

.method public bridge synthetic clearBounds()V
    .locals 0

    .prologue
    .line 40
    invoke-super {p0}, Lorg/icepdf/index/core/pobjects/graphics/text/AbstractText;->clearBounds()V

    return-void
.end method

.method protected detectSpace(Lorg/icepdf/index/core/pobjects/graphics/text/GlyphText;)Z
    .locals 8
    .param p1, "sprite"    # Lorg/icepdf/index/core/pobjects/graphics/text/GlyphText;

    .prologue
    const/4 v4, 0x0

    .line 83
    iget-object v5, p0, Lorg/icepdf/index/core/pobjects/graphics/text/WordText;->currentGlyph:Lorg/icepdf/index/core/pobjects/graphics/text/GlyphText;

    if-eqz v5, :cond_0

    .line 84
    iget-object v5, p0, Lorg/icepdf/index/core/pobjects/graphics/text/WordText;->currentGlyph:Lorg/icepdf/index/core/pobjects/graphics/text/GlyphText;

    invoke-virtual {v5}, Lorg/icepdf/index/core/pobjects/graphics/text/GlyphText;->getBounds()Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;

    move-result-object v0

    .line 85
    .local v0, "bounds1":Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;
    invoke-virtual {p1}, Lorg/icepdf/index/core/pobjects/graphics/text/GlyphText;->getBounds()Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;

    move-result-object v1

    .line 86
    .local v1, "bounds2":Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;
    iget v5, v1, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;->x:F

    iget v6, v0, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;->x:F

    iget v7, v0, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;->width:F

    add-float/2addr v6, v7

    sub-float v2, v5, v6

    .line 87
    .local v2, "space":F
    const/4 v5, 0x0

    cmpg-float v5, v2, v5

    if-gtz v5, :cond_1

    .line 94
    .end local v0    # "bounds1":Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;
    .end local v1    # "bounds2":Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;
    .end local v2    # "space":F
    :cond_0
    :goto_0
    return v4

    .line 91
    .restart local v0    # "bounds1":Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;
    .restart local v1    # "bounds2":Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;
    .restart local v2    # "space":F
    :cond_1
    iget v5, v0, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;->width:F

    sget v6, Lorg/icepdf/index/core/pobjects/graphics/text/WordText;->spaceFraction:I

    int-to-float v6, v6

    div-float v3, v5, v6

    .line 92
    .local v3, "tolerance":F
    cmpl-float v5, v2, v3

    if-lez v5, :cond_0

    const/4 v4, 0x1

    goto :goto_0
.end method

.method public getBounds()Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;
    .locals 4

    .prologue
    .line 189
    iget-object v2, p0, Lorg/icepdf/index/core/pobjects/graphics/text/WordText;->bounds:Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;

    if-nez v2, :cond_1

    .line 191
    iget-object v2, p0, Lorg/icepdf/index/core/pobjects/graphics/text/WordText;->glyphs:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/icepdf/index/core/pobjects/graphics/text/GlyphText;

    .line 192
    .local v0, "glyph":Lorg/icepdf/index/core/pobjects/graphics/text/GlyphText;
    iget-object v2, p0, Lorg/icepdf/index/core/pobjects/graphics/text/WordText;->bounds:Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;

    if-nez v2, :cond_0

    .line 193
    new-instance v2, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;

    invoke-direct {v2}, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;-><init>()V

    iput-object v2, p0, Lorg/icepdf/index/core/pobjects/graphics/text/WordText;->bounds:Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;

    .line 194
    iget-object v2, p0, Lorg/icepdf/index/core/pobjects/graphics/text/WordText;->bounds:Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;

    invoke-virtual {v0}, Lorg/icepdf/index/core/pobjects/graphics/text/GlyphText;->getBounds()Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;

    move-result-object v3

    invoke-virtual {v2, v3}, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;->setRect(Lorg/icepdf/index/java/awt/geom/Rectangle2D;)V

    goto :goto_0

    .line 196
    :cond_0
    iget-object v2, p0, Lorg/icepdf/index/core/pobjects/graphics/text/WordText;->bounds:Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;

    invoke-virtual {v0}, Lorg/icepdf/index/core/pobjects/graphics/text/GlyphText;->getBounds()Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;

    move-result-object v3

    invoke-virtual {v2, v3}, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;->add(Lorg/icepdf/index/java/awt/geom/Rectangle2D;)V

    goto :goto_0

    .line 200
    .end local v0    # "glyph":Lorg/icepdf/index/core/pobjects/graphics/text/GlyphText;
    .end local v1    # "i$":Ljava/util/Iterator;
    :cond_1
    iget-object v2, p0, Lorg/icepdf/index/core/pobjects/graphics/text/WordText;->bounds:Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;

    return-object v2
.end method

.method public getGlyphs()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lorg/icepdf/index/core/pobjects/graphics/text/GlyphText;",
            ">;"
        }
    .end annotation

    .prologue
    .line 204
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/graphics/text/WordText;->glyphs:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 216
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/graphics/text/WordText;->text:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public isWhiteSpace()Z
    .locals 1

    .prologue
    .line 75
    iget-boolean v0, p0, Lorg/icepdf/index/core/pobjects/graphics/text/WordText;->isWhiteSpace:Z

    return v0
.end method

.method public bridge synthetic setHasSelected(Z)V
    .locals 0
    .param p1, "x0"    # Z

    .prologue
    .line 40
    invoke-super {p0, p1}, Lorg/icepdf/index/core/pobjects/graphics/text/AbstractText;->setHasSelected(Z)V

    return-void
.end method

.method public setWhiteSpace(Z)V
    .locals 0
    .param p1, "whiteSpace"    # Z

    .prologue
    .line 79
    iput-boolean p1, p0, Lorg/icepdf/index/core/pobjects/graphics/text/WordText;->isWhiteSpace:Z

    .line 80
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 221
    invoke-virtual {p0}, Lorg/icepdf/index/core/pobjects/graphics/text/WordText;->getText()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
