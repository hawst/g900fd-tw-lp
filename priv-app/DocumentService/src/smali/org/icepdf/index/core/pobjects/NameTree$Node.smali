.class Lorg/icepdf/index/core/pobjects/NameTree$Node;
.super Lorg/icepdf/index/core/pobjects/Dictionary;
.source "NameTree.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/icepdf/index/core/pobjects/NameTree;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Node"
.end annotation


# static fields
.field private static NOT_FOUND:Ljava/lang/Object;

.field private static NOT_FOUND_IS_GREATER:Ljava/lang/Object;

.field private static NOT_FOUND_IS_LESSER:Ljava/lang/Object;


# instance fields
.field private kidsNodes:Ljava/util/Vector;

.field private kidsReferences:Ljava/util/Vector;

.field private lowerLimit:Ljava/lang/Object;

.field private namesAndValues:Ljava/util/Vector;

.field private namesAreDecrypted:Z

.field private upperLimit:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 84
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lorg/icepdf/index/core/pobjects/NameTree$Node;->NOT_FOUND:Ljava/lang/Object;

    .line 85
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lorg/icepdf/index/core/pobjects/NameTree$Node;->NOT_FOUND_IS_LESSER:Ljava/lang/Object;

    .line 86
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lorg/icepdf/index/core/pobjects/NameTree$Node;->NOT_FOUND_IS_GREATER:Ljava/lang/Object;

    return-void
.end method

.method constructor <init>(Lorg/icepdf/index/core/util/Library;Lorg/icepdf/index/core/util/Hashtable;)V
    .locals 6
    .param p1, "l"    # Lorg/icepdf/index/core/util/Library;
    .param p2, "h"    # Lorg/icepdf/index/core/util/Hashtable;

    .prologue
    .line 100
    invoke-direct {p0, p1, p2}, Lorg/icepdf/index/core/pobjects/Dictionary;-><init>(Lorg/icepdf/index/core/util/Library;Lorg/icepdf/index/core/util/Hashtable;)V

    .line 101
    iget-object v3, p0, Lorg/icepdf/index/core/pobjects/NameTree$Node;->library:Lorg/icepdf/index/core/util/Library;

    iget-object v4, p0, Lorg/icepdf/index/core/pobjects/NameTree$Node;->entries:Lorg/icepdf/index/core/util/Hashtable;

    const-string/jumbo v5, "Kids"

    invoke-virtual {v3, v4, v5}, Lorg/icepdf/index/core/util/Library;->getObject(Lorg/icepdf/index/core/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 102
    .local v0, "o":Ljava/lang/Object;
    if-eqz v0, :cond_0

    instance-of v3, v0, Ljava/util/Vector;

    if-eqz v3, :cond_0

    move-object v2, v0

    .line 103
    check-cast v2, Ljava/util/Vector;

    .line 104
    .local v2, "v":Ljava/util/Vector;
    iput-object v2, p0, Lorg/icepdf/index/core/pobjects/NameTree$Node;->kidsReferences:Ljava/util/Vector;

    .line 105
    iget-object v3, p0, Lorg/icepdf/index/core/pobjects/NameTree$Node;->kidsReferences:Ljava/util/Vector;

    invoke-virtual {v3}, Ljava/util/Vector;->size()I

    move-result v1

    .line 106
    .local v1, "sz":I
    if-lez v1, :cond_0

    .line 107
    new-instance v3, Ljava/util/Vector;

    invoke-direct {v3, v1}, Ljava/util/Vector;-><init>(I)V

    iput-object v3, p0, Lorg/icepdf/index/core/pobjects/NameTree$Node;->kidsNodes:Ljava/util/Vector;

    .line 108
    iget-object v3, p0, Lorg/icepdf/index/core/pobjects/NameTree$Node;->kidsNodes:Ljava/util/Vector;

    invoke-virtual {v3, v1}, Ljava/util/Vector;->setSize(I)V

    .line 111
    .end local v1    # "sz":I
    .end local v2    # "v":Ljava/util/Vector;
    :cond_0
    const/4 v3, 0x0

    iput-boolean v3, p0, Lorg/icepdf/index/core/pobjects/NameTree$Node;->namesAreDecrypted:Z

    .line 112
    iget-object v3, p0, Lorg/icepdf/index/core/pobjects/NameTree$Node;->library:Lorg/icepdf/index/core/util/Library;

    iget-object v4, p0, Lorg/icepdf/index/core/pobjects/NameTree$Node;->entries:Lorg/icepdf/index/core/util/Hashtable;

    const-string/jumbo v5, "Names"

    invoke-virtual {v3, v4, v5}, Lorg/icepdf/index/core/util/Library;->getObject(Lorg/icepdf/index/core/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 113
    if-eqz v0, :cond_1

    instance-of v3, v0, Ljava/util/Vector;

    if-eqz v3, :cond_1

    .line 114
    check-cast v0, Ljava/util/Vector;

    .end local v0    # "o":Ljava/lang/Object;
    iput-object v0, p0, Lorg/icepdf/index/core/pobjects/NameTree$Node;->namesAndValues:Ljava/util/Vector;

    .line 126
    :cond_1
    return-void
.end method

.method private binarySearchKids(IILjava/lang/String;)Ljava/lang/Object;
    .locals 5
    .param p1, "firstIndex"    # I
    .param p2, "lastIndex"    # I
    .param p3, "name"    # Ljava/lang/String;

    .prologue
    .line 231
    if-le p1, p2, :cond_1

    .line 232
    sget-object v3, Lorg/icepdf/index/core/pobjects/NameTree$Node;->NOT_FOUND:Ljava/lang/Object;

    .line 255
    :cond_0
    :goto_0
    return-object v3

    .line 233
    :cond_1
    sub-int v4, p2, p1

    div-int/lit8 v4, v4, 0x2

    add-int v1, p1, v4

    .line 234
    .local v1, "pivot":I
    invoke-direct {p0, v1}, Lorg/icepdf/index/core/pobjects/NameTree$Node;->getNode(I)Lorg/icepdf/index/core/pobjects/NameTree$Node;

    move-result-object v4

    invoke-direct {v4, p3}, Lorg/icepdf/index/core/pobjects/NameTree$Node;->search(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    .line 236
    .local v3, "ret":Ljava/lang/Object;
    sget-object v4, Lorg/icepdf/index/core/pobjects/NameTree$Node;->NOT_FOUND_IS_LESSER:Ljava/lang/Object;

    if-ne v3, v4, :cond_2

    .line 238
    add-int/lit8 v4, v1, -0x1

    invoke-direct {p0, p1, v4, p3}, Lorg/icepdf/index/core/pobjects/NameTree$Node;->binarySearchKids(IILjava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    goto :goto_0

    .line 239
    :cond_2
    sget-object v4, Lorg/icepdf/index/core/pobjects/NameTree$Node;->NOT_FOUND_IS_GREATER:Ljava/lang/Object;

    if-ne v3, v4, :cond_3

    .line 241
    add-int/lit8 v4, v1, 0x1

    invoke-direct {p0, v4, p2, p3}, Lorg/icepdf/index/core/pobjects/NameTree$Node;->binarySearchKids(IILjava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    goto :goto_0

    .line 242
    :cond_3
    sget-object v4, Lorg/icepdf/index/core/pobjects/NameTree$Node;->NOT_FOUND:Ljava/lang/Object;

    if-ne v3, v4, :cond_0

    .line 245
    move v0, p1

    .local v0, "i":I
    :goto_1
    if-gt v0, p2, :cond_0

    .line 246
    if-ne v0, v1, :cond_5

    .line 245
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 248
    :cond_5
    invoke-direct {p0, v0}, Lorg/icepdf/index/core/pobjects/NameTree$Node;->getNode(I)Lorg/icepdf/index/core/pobjects/NameTree$Node;

    move-result-object v4

    invoke-direct {v4, p3}, Lorg/icepdf/index/core/pobjects/NameTree$Node;->search(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    .line 249
    .local v2, "r":Ljava/lang/Object;
    sget-object v4, Lorg/icepdf/index/core/pobjects/NameTree$Node;->NOT_FOUND:Ljava/lang/Object;

    if-eq v2, v4, :cond_4

    sget-object v4, Lorg/icepdf/index/core/pobjects/NameTree$Node;->NOT_FOUND_IS_LESSER:Ljava/lang/Object;

    if-eq v2, v4, :cond_4

    sget-object v4, Lorg/icepdf/index/core/pobjects/NameTree$Node;->NOT_FOUND_IS_GREATER:Ljava/lang/Object;

    if-eq v2, v4, :cond_4

    .line 250
    move-object v3, v2

    .line 251
    goto :goto_0
.end method

.method private binarySearchNames(IILjava/lang/String;)Ljava/lang/Object;
    .locals 5
    .param p1, "firstIndex"    # I
    .param p2, "lastIndex"    # I
    .param p3, "name"    # Ljava/lang/String;

    .prologue
    .line 259
    if-le p1, p2, :cond_1

    .line 260
    sget-object v1, Lorg/icepdf/index/core/pobjects/NameTree$Node;->NOT_FOUND:Ljava/lang/Object;

    .line 278
    :cond_0
    :goto_0
    return-object v1

    .line 261
    :cond_1
    sub-int v3, p2, p1

    div-int/lit8 v3, v3, 0x2

    add-int v2, p1, v3

    .line 262
    .local v2, "pivot":I
    and-int/lit8 v2, v2, -0x2

    .line 264
    iget-object v3, p0, Lorg/icepdf/index/core/pobjects/NameTree$Node;->namesAndValues:Ljava/util/Vector;

    invoke-virtual {v3, v2}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    .line 265
    .local v0, "cmp":I
    if-nez v0, :cond_2

    .line 267
    iget-object v3, p0, Lorg/icepdf/index/core/pobjects/NameTree$Node;->namesAndValues:Ljava/util/Vector;

    add-int/lit8 v4, v2, 0x1

    invoke-virtual {v3, v4}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v1

    .line 268
    .local v1, "ob":Ljava/lang/Object;
    instance-of v3, v1, Lorg/icepdf/index/core/pobjects/Reference;

    if-eqz v3, :cond_0

    .line 269
    iget-object v3, p0, Lorg/icepdf/index/core/pobjects/NameTree$Node;->library:Lorg/icepdf/index/core/util/Library;

    check-cast v1, Lorg/icepdf/index/core/pobjects/Reference;

    .end local v1    # "ob":Ljava/lang/Object;
    invoke-virtual {v3, v1}, Lorg/icepdf/index/core/util/Library;->getObject(Lorg/icepdf/index/core/pobjects/Reference;)Ljava/lang/Object;

    move-result-object v1

    .restart local v1    # "ob":Ljava/lang/Object;
    goto :goto_0

    .line 271
    .end local v1    # "ob":Ljava/lang/Object;
    :cond_2
    if-lez v0, :cond_3

    .line 273
    add-int/lit8 v3, v2, -0x1

    invoke-direct {p0, p1, v3, p3}, Lorg/icepdf/index/core/pobjects/NameTree$Node;->binarySearchNames(IILjava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    goto :goto_0

    .line 274
    :cond_3
    if-gez v0, :cond_4

    .line 276
    add-int/lit8 v3, v2, 0x2

    invoke-direct {p0, v3, p2, p3}, Lorg/icepdf/index/core/pobjects/NameTree$Node;->binarySearchNames(IILjava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    goto :goto_0

    .line 278
    :cond_4
    sget-object v1, Lorg/icepdf/index/core/pobjects/NameTree$Node;->NOT_FOUND:Ljava/lang/Object;

    goto :goto_0
.end method

.method private ensureNamesDecrypted()V
    .locals 3

    .prologue
    .line 129
    iget-boolean v2, p0, Lorg/icepdf/index/core/pobjects/NameTree$Node;->namesAreDecrypted:Z

    if-eqz v2, :cond_1

    .line 138
    :cond_0
    return-void

    .line 131
    :cond_1
    const/4 v2, 0x1

    iput-boolean v2, p0, Lorg/icepdf/index/core/pobjects/NameTree$Node;->namesAreDecrypted:Z

    .line 133
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lorg/icepdf/index/core/pobjects/NameTree$Node;->namesAndValues:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 134
    iget-object v2, p0, Lorg/icepdf/index/core/pobjects/NameTree$Node;->namesAndValues:Ljava/util/Vector;

    invoke-virtual {v2, v0}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v1

    .line 136
    .local v1, "tmp":Ljava/lang/Object;
    iget-object v2, p0, Lorg/icepdf/index/core/pobjects/NameTree$Node;->namesAndValues:Ljava/util/Vector;

    invoke-virtual {v2, v0, v1}, Ljava/util/Vector;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 133
    add-int/lit8 v0, v0, 0x2

    goto :goto_0
.end method

.method private getNode(I)Lorg/icepdf/index/core/pobjects/NameTree$Node;
    .locals 4
    .param p1, "index"    # I

    .prologue
    .line 282
    iget-object v3, p0, Lorg/icepdf/index/core/pobjects/NameTree$Node;->kidsNodes:Ljava/util/Vector;

    invoke-virtual {v3, p1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/icepdf/index/core/pobjects/NameTree$Node;

    .line 283
    .local v0, "n":Lorg/icepdf/index/core/pobjects/NameTree$Node;
    if-nez v0, :cond_0

    .line 284
    iget-object v3, p0, Lorg/icepdf/index/core/pobjects/NameTree$Node;->kidsReferences:Ljava/util/Vector;

    invoke-virtual {v3, p1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/icepdf/index/core/pobjects/Reference;

    .line 285
    .local v2, "r":Lorg/icepdf/index/core/pobjects/Reference;
    iget-object v3, p0, Lorg/icepdf/index/core/pobjects/NameTree$Node;->library:Lorg/icepdf/index/core/util/Library;

    invoke-virtual {v3, v2}, Lorg/icepdf/index/core/util/Library;->getObject(Lorg/icepdf/index/core/pobjects/Reference;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/icepdf/index/core/util/Hashtable;

    .line 286
    .local v1, "nh":Lorg/icepdf/index/core/util/Hashtable;
    new-instance v0, Lorg/icepdf/index/core/pobjects/NameTree$Node;

    .end local v0    # "n":Lorg/icepdf/index/core/pobjects/NameTree$Node;
    iget-object v3, p0, Lorg/icepdf/index/core/pobjects/NameTree$Node;->library:Lorg/icepdf/index/core/util/Library;

    invoke-direct {v0, v3, v1}, Lorg/icepdf/index/core/pobjects/NameTree$Node;-><init>(Lorg/icepdf/index/core/util/Library;Lorg/icepdf/index/core/util/Hashtable;)V

    .line 287
    .restart local v0    # "n":Lorg/icepdf/index/core/pobjects/NameTree$Node;
    iget-object v3, p0, Lorg/icepdf/index/core/pobjects/NameTree$Node;->kidsNodes:Ljava/util/Vector;

    invoke-virtual {v3, p1, v0}, Ljava/util/Vector;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 289
    .end local v1    # "nh":Lorg/icepdf/index/core/util/Hashtable;
    .end local v2    # "r":Lorg/icepdf/index/core/pobjects/Reference;
    :cond_0
    return-object v0
.end method

.method private search(Ljava/lang/String;)Ljava/lang/Object;
    .locals 7
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x0

    .line 164
    iget-object v4, p0, Lorg/icepdf/index/core/pobjects/NameTree$Node;->kidsReferences:Ljava/util/Vector;

    if-eqz v4, :cond_5

    .line 166
    iget-object v4, p0, Lorg/icepdf/index/core/pobjects/NameTree$Node;->lowerLimit:Ljava/lang/Object;

    if-eqz v4, :cond_2

    .line 167
    iget-object v4, p0, Lorg/icepdf/index/core/pobjects/NameTree$Node;->lowerLimit:Ljava/lang/Object;

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    .line 168
    .local v0, "cmp":I
    if-lez v0, :cond_1

    .line 170
    sget-object v2, Lorg/icepdf/index/core/pobjects/NameTree$Node;->NOT_FOUND_IS_LESSER:Ljava/lang/Object;

    .line 227
    .end local v0    # "cmp":I
    :cond_0
    :goto_0
    return-object v2

    .line 171
    .restart local v0    # "cmp":I
    :cond_1
    if-nez v0, :cond_2

    .line 172
    invoke-direct {p0, v6}, Lorg/icepdf/index/core/pobjects/NameTree$Node;->getNode(I)Lorg/icepdf/index/core/pobjects/NameTree$Node;

    move-result-object v4

    invoke-direct {v4, p1}, Lorg/icepdf/index/core/pobjects/NameTree$Node;->search(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    goto :goto_0

    .line 174
    .end local v0    # "cmp":I
    :cond_2
    iget-object v4, p0, Lorg/icepdf/index/core/pobjects/NameTree$Node;->upperLimit:Ljava/lang/Object;

    if-eqz v4, :cond_4

    .line 175
    iget-object v4, p0, Lorg/icepdf/index/core/pobjects/NameTree$Node;->upperLimit:Ljava/lang/Object;

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    .line 176
    .restart local v0    # "cmp":I
    if-gez v0, :cond_3

    .line 178
    sget-object v2, Lorg/icepdf/index/core/pobjects/NameTree$Node;->NOT_FOUND_IS_GREATER:Ljava/lang/Object;

    goto :goto_0

    .line 179
    :cond_3
    if-nez v0, :cond_4

    .line 180
    iget-object v4, p0, Lorg/icepdf/index/core/pobjects/NameTree$Node;->kidsReferences:Ljava/util/Vector;

    invoke-virtual {v4}, Ljava/util/Vector;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-direct {p0, v4}, Lorg/icepdf/index/core/pobjects/NameTree$Node;->getNode(I)Lorg/icepdf/index/core/pobjects/NameTree$Node;

    move-result-object v4

    invoke-direct {v4, p1}, Lorg/icepdf/index/core/pobjects/NameTree$Node;->search(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    goto :goto_0

    .line 184
    .end local v0    # "cmp":I
    :cond_4
    iget-object v4, p0, Lorg/icepdf/index/core/pobjects/NameTree$Node;->kidsReferences:Ljava/util/Vector;

    invoke-virtual {v4}, Ljava/util/Vector;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-direct {p0, v6, v4, p1}, Lorg/icepdf/index/core/pobjects/NameTree$Node;->binarySearchKids(IILjava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    goto :goto_0

    .line 185
    :cond_5
    iget-object v4, p0, Lorg/icepdf/index/core/pobjects/NameTree$Node;->namesAndValues:Ljava/util/Vector;

    if-eqz v4, :cond_c

    .line 187
    iget-object v4, p0, Lorg/icepdf/index/core/pobjects/NameTree$Node;->namesAndValues:Ljava/util/Vector;

    invoke-virtual {v4}, Ljava/util/Vector;->size()I

    move-result v1

    .line 189
    .local v1, "numNamesAndValues":I
    iget-object v4, p0, Lorg/icepdf/index/core/pobjects/NameTree$Node;->lowerLimit:Ljava/lang/Object;

    if-eqz v4, :cond_7

    .line 190
    iget-object v4, p0, Lorg/icepdf/index/core/pobjects/NameTree$Node;->lowerLimit:Ljava/lang/Object;

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    .line 191
    .restart local v0    # "cmp":I
    if-lez v0, :cond_6

    .line 193
    sget-object v2, Lorg/icepdf/index/core/pobjects/NameTree$Node;->NOT_FOUND_IS_LESSER:Ljava/lang/Object;

    goto :goto_0

    .line 194
    :cond_6
    if-nez v0, :cond_7

    .line 195
    invoke-direct {p0}, Lorg/icepdf/index/core/pobjects/NameTree$Node;->ensureNamesDecrypted()V

    .line 196
    iget-object v4, p0, Lorg/icepdf/index/core/pobjects/NameTree$Node;->namesAndValues:Ljava/util/Vector;

    invoke-virtual {v4, v6}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 197
    iget-object v4, p0, Lorg/icepdf/index/core/pobjects/NameTree$Node;->namesAndValues:Ljava/util/Vector;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v2

    .line 198
    .local v2, "ob":Ljava/lang/Object;
    instance-of v4, v2, Lorg/icepdf/index/core/pobjects/Reference;

    if-eqz v4, :cond_0

    .line 199
    iget-object v4, p0, Lorg/icepdf/index/core/pobjects/NameTree$Node;->library:Lorg/icepdf/index/core/util/Library;

    check-cast v2, Lorg/icepdf/index/core/pobjects/Reference;

    .end local v2    # "ob":Ljava/lang/Object;
    invoke-virtual {v4, v2}, Lorg/icepdf/index/core/util/Library;->getObject(Lorg/icepdf/index/core/pobjects/Reference;)Ljava/lang/Object;

    move-result-object v2

    .restart local v2    # "ob":Ljava/lang/Object;
    goto/16 :goto_0

    .line 204
    .end local v0    # "cmp":I
    .end local v2    # "ob":Ljava/lang/Object;
    :cond_7
    iget-object v4, p0, Lorg/icepdf/index/core/pobjects/NameTree$Node;->upperLimit:Ljava/lang/Object;

    if-eqz v4, :cond_9

    .line 205
    iget-object v4, p0, Lorg/icepdf/index/core/pobjects/NameTree$Node;->upperLimit:Ljava/lang/Object;

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    .line 206
    .restart local v0    # "cmp":I
    if-gez v0, :cond_8

    .line 208
    sget-object v2, Lorg/icepdf/index/core/pobjects/NameTree$Node;->NOT_FOUND_IS_GREATER:Ljava/lang/Object;

    goto/16 :goto_0

    .line 209
    :cond_8
    if-nez v0, :cond_9

    .line 210
    invoke-direct {p0}, Lorg/icepdf/index/core/pobjects/NameTree$Node;->ensureNamesDecrypted()V

    .line 211
    iget-object v4, p0, Lorg/icepdf/index/core/pobjects/NameTree$Node;->namesAndValues:Ljava/util/Vector;

    add-int/lit8 v5, v1, -0x2

    invoke-virtual {v4, v5}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_9

    .line 212
    iget-object v4, p0, Lorg/icepdf/index/core/pobjects/NameTree$Node;->namesAndValues:Ljava/util/Vector;

    add-int/lit8 v5, v1, -0x1

    invoke-virtual {v4, v5}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v2

    .line 213
    .restart local v2    # "ob":Ljava/lang/Object;
    instance-of v4, v2, Lorg/icepdf/index/core/pobjects/Reference;

    if-eqz v4, :cond_0

    .line 214
    iget-object v4, p0, Lorg/icepdf/index/core/pobjects/NameTree$Node;->library:Lorg/icepdf/index/core/util/Library;

    check-cast v2, Lorg/icepdf/index/core/pobjects/Reference;

    .end local v2    # "ob":Ljava/lang/Object;
    invoke-virtual {v4, v2}, Lorg/icepdf/index/core/util/Library;->getObject(Lorg/icepdf/index/core/pobjects/Reference;)Ljava/lang/Object;

    move-result-object v2

    .restart local v2    # "ob":Ljava/lang/Object;
    goto/16 :goto_0

    .line 221
    .end local v0    # "cmp":I
    .end local v2    # "ob":Ljava/lang/Object;
    :cond_9
    invoke-direct {p0}, Lorg/icepdf/index/core/pobjects/NameTree$Node;->ensureNamesDecrypted()V

    .line 222
    add-int/lit8 v4, v1, -0x1

    invoke-direct {p0, v6, v4, p1}, Lorg/icepdf/index/core/pobjects/NameTree$Node;->binarySearchNames(IILjava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    .line 223
    .local v3, "ret":Ljava/lang/Object;
    sget-object v4, Lorg/icepdf/index/core/pobjects/NameTree$Node;->NOT_FOUND:Ljava/lang/Object;

    if-eq v3, v4, :cond_a

    sget-object v4, Lorg/icepdf/index/core/pobjects/NameTree$Node;->NOT_FOUND_IS_LESSER:Ljava/lang/Object;

    if-eq v3, v4, :cond_a

    sget-object v4, Lorg/icepdf/index/core/pobjects/NameTree$Node;->NOT_FOUND_IS_GREATER:Ljava/lang/Object;

    if-ne v3, v4, :cond_b

    .line 224
    :cond_a
    const/4 v3, 0x0

    .end local v3    # "ret":Ljava/lang/Object;
    :cond_b
    move-object v2, v3

    .line 225
    goto/16 :goto_0

    .line 227
    .end local v1    # "numNamesAndValues":I
    :cond_c
    const/4 v2, 0x0

    goto/16 :goto_0
.end method


# virtual methods
.method public dispose()V
    .locals 1

    .prologue
    .line 293
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/NameTree$Node;->namesAndValues:Ljava/util/Vector;

    if-eqz v0, :cond_0

    .line 294
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/NameTree$Node;->namesAndValues:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->clear()V

    .line 295
    :cond_0
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/NameTree$Node;->kidsReferences:Ljava/util/Vector;

    if-eqz v0, :cond_1

    .line 296
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/NameTree$Node;->kidsReferences:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->clear()V

    .line 297
    :cond_1
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/NameTree$Node;->kidsNodes:Ljava/util/Vector;

    if-eqz v0, :cond_2

    .line 298
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/NameTree$Node;->kidsNodes:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->clear()V

    .line 299
    :cond_2
    return-void
.end method

.method searchName(Ljava/lang/String;)Ljava/lang/Object;
    .locals 2
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 154
    invoke-direct {p0, p1}, Lorg/icepdf/index/core/pobjects/NameTree$Node;->search(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 155
    .local v0, "ret":Ljava/lang/Object;
    sget-object v1, Lorg/icepdf/index/core/pobjects/NameTree$Node;->NOT_FOUND:Ljava/lang/Object;

    if-eq v0, v1, :cond_0

    sget-object v1, Lorg/icepdf/index/core/pobjects/NameTree$Node;->NOT_FOUND_IS_LESSER:Ljava/lang/Object;

    if-eq v0, v1, :cond_0

    sget-object v1, Lorg/icepdf/index/core/pobjects/NameTree$Node;->NOT_FOUND_IS_GREATER:Ljava/lang/Object;

    if-ne v0, v1, :cond_1

    .line 156
    :cond_0
    const/4 v0, 0x0

    .line 158
    .end local v0    # "ret":Ljava/lang/Object;
    :cond_1
    return-object v0
.end method
