.class public Lorg/icepdf/index/core/pobjects/security/Permissions;
.super Ljava/lang/Object;
.source "Permissions.java"


# static fields
.field private static final ACCESSIBILITY_BIT_10:I = -0xd40

.field private static final ASSEMBLE_DOCUMENT_BIT_11:I = -0xb40

.field public static final AUTHORING_FORM_FIELDS:I = 0x4

.field public static final CONTENT_ACCESSABILITY:I = 0x6

.field public static final CONTENT_EXTRACTION:I = 0x3

.field private static final DATA_EXTRACTION_BIT_5:I = -0xf30

.field public static final DOCUMENT_ASSEMBLY:I = 0x7

.field public static final FORM_FIELD_FILL_SIGNING:I = 0x5

.field public static final MODIFY_DOCUMENT:I = 0x2

.field private static final MODIFY_DOCUMENT_BIT_4:I = -0xf38

.field private static final MODIFY_FORMS_BIT_9:I = -0xe40

.field private static final MODIFY_TEXT_BIT_6:I = -0xf20

.field public static final PRINT_DOCUMENT:I = 0x0

.field private static final PRINT_DOCUMENT_BIT_3:I = -0xf3c

.field public static final PRINT_DOCUMENT_QUALITY:I = 0x1

.field private static final PRINT_QUALITY_BIT_12:I = -0x740


# instance fields
.field isInit:Z

.field private permissionFlags:I

.field private permissions:[Z

.field private revision:I


# direct methods
.method public constructor <init>(Lorg/icepdf/index/core/pobjects/security/EncryptionDictionary;)V
    .locals 1
    .param p1, "dictionary"    # Lorg/icepdf/index/core/pobjects/security/EncryptionDictionary;

    .prologue
    .line 192
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 175
    const/16 v0, 0xa

    new-array v0, v0, [Z

    iput-object v0, p0, Lorg/icepdf/index/core/pobjects/security/Permissions;->permissions:[Z

    .line 179
    const/16 v0, -0xf40

    iput v0, p0, Lorg/icepdf/index/core/pobjects/security/Permissions;->permissionFlags:I

    .line 182
    const/4 v0, 0x2

    iput v0, p0, Lorg/icepdf/index/core/pobjects/security/Permissions;->revision:I

    .line 185
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/icepdf/index/core/pobjects/security/Permissions;->isInit:Z

    .line 193
    invoke-virtual {p1}, Lorg/icepdf/index/core/pobjects/security/EncryptionDictionary;->getPermissions()I

    move-result v0

    iput v0, p0, Lorg/icepdf/index/core/pobjects/security/Permissions;->permissionFlags:I

    .line 194
    invoke-virtual {p1}, Lorg/icepdf/index/core/pobjects/security/EncryptionDictionary;->getRevisionNumber()I

    move-result v0

    iput v0, p0, Lorg/icepdf/index/core/pobjects/security/Permissions;->revision:I

    .line 195
    return-void
.end method


# virtual methods
.method public getPermissions(I)Z
    .locals 1
    .param p1, "permissionIndex"    # I

    .prologue
    .line 306
    iget-boolean v0, p0, Lorg/icepdf/index/core/pobjects/security/Permissions;->isInit:Z

    if-nez v0, :cond_0

    .line 307
    invoke-virtual {p0}, Lorg/icepdf/index/core/pobjects/security/Permissions;->init()V

    .line 310
    :cond_0
    if-ltz p1, :cond_1

    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/security/Permissions;->permissions:[Z

    array-length v0, v0

    if-gt p1, v0, :cond_1

    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/security/Permissions;->permissions:[Z

    aget-boolean v0, v0, p1

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public init()V
    .locals 7

    .prologue
    const/16 v6, -0xf3c

    const/4 v2, 0x0

    const/4 v5, 0x2

    const/4 v4, 0x3

    const/4 v3, 0x1

    .line 203
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lorg/icepdf/index/core/pobjects/security/Permissions;->permissions:[Z

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 204
    iget-object v1, p0, Lorg/icepdf/index/core/pobjects/security/Permissions;->permissions:[Z

    aput-boolean v2, v1, v0

    .line 203
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 208
    :cond_0
    iget v1, p0, Lorg/icepdf/index/core/pobjects/security/Permissions;->revision:I

    if-ne v1, v5, :cond_a

    .line 210
    iget v1, p0, Lorg/icepdf/index/core/pobjects/security/Permissions;->permissionFlags:I

    and-int/lit16 v1, v1, -0xf3c

    if-ne v1, v6, :cond_1

    .line 212
    iget-object v1, p0, Lorg/icepdf/index/core/pobjects/security/Permissions;->permissions:[Z

    aput-boolean v3, v1, v2

    .line 215
    :cond_1
    iget v1, p0, Lorg/icepdf/index/core/pobjects/security/Permissions;->permissionFlags:I

    and-int/lit16 v1, v1, -0xf38

    const/16 v2, -0xf38

    if-ne v1, v2, :cond_2

    .line 217
    iget-object v1, p0, Lorg/icepdf/index/core/pobjects/security/Permissions;->permissions:[Z

    aput-boolean v3, v1, v5

    .line 220
    :cond_2
    iget v1, p0, Lorg/icepdf/index/core/pobjects/security/Permissions;->permissionFlags:I

    and-int/lit16 v1, v1, -0xf30

    const/16 v2, -0xf30

    if-ne v1, v2, :cond_3

    .line 222
    iget-object v1, p0, Lorg/icepdf/index/core/pobjects/security/Permissions;->permissions:[Z

    aput-boolean v3, v1, v4

    .line 225
    :cond_3
    iget-object v1, p0, Lorg/icepdf/index/core/pobjects/security/Permissions;->permissions:[Z

    aget-boolean v1, v1, v4

    if-eqz v1, :cond_4

    .line 226
    iget-object v1, p0, Lorg/icepdf/index/core/pobjects/security/Permissions;->permissions:[Z

    const/4 v2, 0x4

    aput-boolean v3, v1, v2

    .line 230
    :cond_4
    iget-object v1, p0, Lorg/icepdf/index/core/pobjects/security/Permissions;->permissions:[Z

    aget-boolean v1, v1, v4

    if-eqz v1, :cond_5

    .line 231
    iget-object v1, p0, Lorg/icepdf/index/core/pobjects/security/Permissions;->permissions:[Z

    const/4 v2, 0x5

    aput-boolean v3, v1, v2

    .line 235
    :cond_5
    iget-object v1, p0, Lorg/icepdf/index/core/pobjects/security/Permissions;->permissions:[Z

    aget-boolean v1, v1, v4

    if-eqz v1, :cond_6

    .line 236
    iget-object v1, p0, Lorg/icepdf/index/core/pobjects/security/Permissions;->permissions:[Z

    const/4 v2, 0x6

    aput-boolean v3, v1, v2

    .line 240
    :cond_6
    iget-object v1, p0, Lorg/icepdf/index/core/pobjects/security/Permissions;->permissions:[Z

    aget-boolean v1, v1, v5

    if-eqz v1, :cond_7

    .line 241
    iget-object v1, p0, Lorg/icepdf/index/core/pobjects/security/Permissions;->permissions:[Z

    const/4 v2, 0x7

    aput-boolean v3, v1, v2

    .line 244
    :cond_7
    iget v1, p0, Lorg/icepdf/index/core/pobjects/security/Permissions;->permissionFlags:I

    and-int/lit16 v1, v1, -0x740

    const/16 v2, -0x740

    if-ne v1, v2, :cond_8

    .line 246
    iget-object v1, p0, Lorg/icepdf/index/core/pobjects/security/Permissions;->permissions:[Z

    aput-boolean v3, v1, v3

    .line 249
    :cond_8
    iput-boolean v3, p0, Lorg/icepdf/index/core/pobjects/security/Permissions;->isInit:Z

    .line 296
    :cond_9
    :goto_1
    return-void

    .line 253
    :cond_a
    iget v1, p0, Lorg/icepdf/index/core/pobjects/security/Permissions;->revision:I

    if-ne v1, v4, :cond_9

    .line 255
    iget v1, p0, Lorg/icepdf/index/core/pobjects/security/Permissions;->permissionFlags:I

    and-int/lit16 v1, v1, -0xf3c

    if-ne v1, v6, :cond_b

    .line 257
    iget-object v1, p0, Lorg/icepdf/index/core/pobjects/security/Permissions;->permissions:[Z

    aput-boolean v3, v1, v2

    .line 260
    :cond_b
    iget v1, p0, Lorg/icepdf/index/core/pobjects/security/Permissions;->permissionFlags:I

    and-int/lit16 v1, v1, -0xf38

    const/16 v2, -0xf38

    if-ne v1, v2, :cond_c

    .line 262
    iget-object v1, p0, Lorg/icepdf/index/core/pobjects/security/Permissions;->permissions:[Z

    aput-boolean v3, v1, v5

    .line 265
    :cond_c
    iget v1, p0, Lorg/icepdf/index/core/pobjects/security/Permissions;->permissionFlags:I

    and-int/lit16 v1, v1, -0xf30

    const/16 v2, -0xf30

    if-ne v1, v2, :cond_d

    .line 267
    iget-object v1, p0, Lorg/icepdf/index/core/pobjects/security/Permissions;->permissions:[Z

    aput-boolean v3, v1, v4

    .line 270
    :cond_d
    iget v1, p0, Lorg/icepdf/index/core/pobjects/security/Permissions;->permissionFlags:I

    and-int/lit16 v1, v1, -0xf20

    const/16 v2, -0xf20

    if-ne v1, v2, :cond_e

    .line 272
    iget-object v1, p0, Lorg/icepdf/index/core/pobjects/security/Permissions;->permissions:[Z

    const/4 v2, 0x4

    aput-boolean v3, v1, v2

    .line 275
    :cond_e
    iget v1, p0, Lorg/icepdf/index/core/pobjects/security/Permissions;->permissionFlags:I

    and-int/lit16 v1, v1, -0xe40

    const/16 v2, -0xe40

    if-ne v1, v2, :cond_f

    .line 277
    iget-object v1, p0, Lorg/icepdf/index/core/pobjects/security/Permissions;->permissions:[Z

    const/4 v2, 0x5

    aput-boolean v3, v1, v2

    .line 280
    :cond_f
    iget v1, p0, Lorg/icepdf/index/core/pobjects/security/Permissions;->permissionFlags:I

    and-int/lit16 v1, v1, -0xd40

    const/16 v2, -0xd40

    if-ne v1, v2, :cond_10

    .line 282
    iget-object v1, p0, Lorg/icepdf/index/core/pobjects/security/Permissions;->permissions:[Z

    const/4 v2, 0x6

    aput-boolean v3, v1, v2

    .line 285
    :cond_10
    iget v1, p0, Lorg/icepdf/index/core/pobjects/security/Permissions;->permissionFlags:I

    and-int/lit16 v1, v1, -0xb40

    const/16 v2, -0xb40

    if-ne v1, v2, :cond_11

    .line 287
    iget-object v1, p0, Lorg/icepdf/index/core/pobjects/security/Permissions;->permissions:[Z

    const/4 v2, 0x7

    aput-boolean v3, v1, v2

    .line 290
    :cond_11
    iget v1, p0, Lorg/icepdf/index/core/pobjects/security/Permissions;->permissionFlags:I

    and-int/lit16 v1, v1, -0x740

    const/16 v2, -0x740

    if-ne v1, v2, :cond_12

    .line 292
    iget-object v1, p0, Lorg/icepdf/index/core/pobjects/security/Permissions;->permissions:[Z

    aput-boolean v3, v1, v3

    .line 294
    :cond_12
    iput-boolean v3, p0, Lorg/icepdf/index/core/pobjects/security/Permissions;->isInit:Z

    goto :goto_1
.end method
