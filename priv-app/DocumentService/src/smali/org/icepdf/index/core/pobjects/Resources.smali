.class public Lorg/icepdf/index/core/pobjects/Resources;
.super Lorg/icepdf/index/core/pobjects/Dictionary;
.source "Resources.java"


# instance fields
.field colorspaces:Lorg/icepdf/index/core/util/Hashtable;

.field fonts:Lorg/icepdf/index/core/util/Hashtable;

.field private images:Lorg/icepdf/index/core/util/Hashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/icepdf/index/core/util/Hashtable",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field patterns:Lorg/icepdf/index/core/util/Hashtable;

.field xobjects:Lorg/icepdf/index/core/util/Hashtable;


# direct methods
.method public constructor <init>(Lorg/icepdf/index/core/util/Library;Lorg/icepdf/index/core/util/Hashtable;)V
    .locals 3
    .param p1, "l"    # Lorg/icepdf/index/core/util/Library;
    .param p2, "h"    # Lorg/icepdf/index/core/util/Hashtable;

    .prologue
    .line 50
    invoke-direct {p0, p1, p2}, Lorg/icepdf/index/core/pobjects/Dictionary;-><init>(Lorg/icepdf/index/core/util/Library;Lorg/icepdf/index/core/util/Hashtable;)V

    .line 43
    new-instance v0, Lorg/icepdf/index/core/util/Hashtable;

    invoke-direct {v0}, Lorg/icepdf/index/core/util/Hashtable;-><init>()V

    iput-object v0, p0, Lorg/icepdf/index/core/pobjects/Resources;->images:Lorg/icepdf/index/core/util/Hashtable;

    .line 51
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/Resources;->library:Lorg/icepdf/index/core/util/Library;

    iget-object v1, p0, Lorg/icepdf/index/core/pobjects/Resources;->entries:Lorg/icepdf/index/core/util/Hashtable;

    const-string/jumbo v2, "ColorSpace"

    invoke-virtual {v0, v1, v2}, Lorg/icepdf/index/core/util/Library;->getDictionary(Lorg/icepdf/index/core/util/Hashtable;Ljava/lang/String;)Lorg/icepdf/index/core/util/Hashtable;

    move-result-object v0

    iput-object v0, p0, Lorg/icepdf/index/core/pobjects/Resources;->colorspaces:Lorg/icepdf/index/core/util/Hashtable;

    .line 52
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/Resources;->library:Lorg/icepdf/index/core/util/Library;

    iget-object v1, p0, Lorg/icepdf/index/core/pobjects/Resources;->entries:Lorg/icepdf/index/core/util/Hashtable;

    const-string/jumbo v2, "Font"

    invoke-virtual {v0, v1, v2}, Lorg/icepdf/index/core/util/Library;->getDictionary(Lorg/icepdf/index/core/util/Hashtable;Ljava/lang/String;)Lorg/icepdf/index/core/util/Hashtable;

    move-result-object v0

    iput-object v0, p0, Lorg/icepdf/index/core/pobjects/Resources;->fonts:Lorg/icepdf/index/core/util/Hashtable;

    .line 53
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/Resources;->library:Lorg/icepdf/index/core/util/Library;

    iget-object v1, p0, Lorg/icepdf/index/core/pobjects/Resources;->entries:Lorg/icepdf/index/core/util/Hashtable;

    const-string/jumbo v2, "XObject"

    invoke-virtual {v0, v1, v2}, Lorg/icepdf/index/core/util/Library;->getDictionary(Lorg/icepdf/index/core/util/Hashtable;Ljava/lang/String;)Lorg/icepdf/index/core/util/Hashtable;

    move-result-object v0

    iput-object v0, p0, Lorg/icepdf/index/core/pobjects/Resources;->xobjects:Lorg/icepdf/index/core/util/Hashtable;

    .line 54
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/Resources;->library:Lorg/icepdf/index/core/util/Library;

    iget-object v1, p0, Lorg/icepdf/index/core/pobjects/Resources;->entries:Lorg/icepdf/index/core/util/Hashtable;

    const-string/jumbo v2, "Pattern"

    invoke-virtual {v0, v1, v2}, Lorg/icepdf/index/core/util/Library;->getDictionary(Lorg/icepdf/index/core/util/Hashtable;Ljava/lang/String;)Lorg/icepdf/index/core/util/Hashtable;

    move-result-object v0

    iput-object v0, p0, Lorg/icepdf/index/core/pobjects/Resources;->patterns:Lorg/icepdf/index/core/util/Hashtable;

    .line 57
    return-void
.end method


# virtual methods
.method public dispose(Z)V
    .locals 7
    .param p1, "cache"    # Z

    .prologue
    .line 69
    iget-object v6, p0, Lorg/icepdf/index/core/pobjects/Resources;->images:Lorg/icepdf/index/core/util/Hashtable;

    if-eqz v6, :cond_2

    .line 70
    iget-object v6, p0, Lorg/icepdf/index/core/pobjects/Resources;->images:Lorg/icepdf/index/core/util/Hashtable;

    invoke-virtual {v6}, Lorg/icepdf/index/core/util/Hashtable;->elements()Ljava/util/Enumeration;

    move-result-object v2

    .line 72
    .local v2, "shapeContent":Ljava/util/Enumeration;
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 73
    invoke-interface {v2}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    .line 74
    .local v0, "image":Ljava/lang/Object;
    instance-of v6, v0, Landroid/graphics/Bitmap;

    if-eqz v6, :cond_0

    move-object v4, v0

    .line 75
    check-cast v4, Landroid/graphics/Bitmap;

    .line 76
    .local v4, "tmp":Landroid/graphics/Bitmap;
    invoke-virtual {v4}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_0

    .line 80
    .end local v0    # "image":Ljava/lang/Object;
    .end local v4    # "tmp":Landroid/graphics/Bitmap;
    :cond_1
    iget-object v6, p0, Lorg/icepdf/index/core/pobjects/Resources;->images:Lorg/icepdf/index/core/util/Hashtable;

    invoke-virtual {v6}, Lorg/icepdf/index/core/util/Hashtable;->clear()V

    .line 86
    .end local v2    # "shapeContent":Ljava/util/Enumeration;
    :cond_2
    iget-object v6, p0, Lorg/icepdf/index/core/pobjects/Resources;->xobjects:Lorg/icepdf/index/core/util/Hashtable;

    if-eqz v6, :cond_5

    .line 87
    iget-object v6, p0, Lorg/icepdf/index/core/pobjects/Resources;->xobjects:Lorg/icepdf/index/core/util/Hashtable;

    invoke-virtual {v6}, Lorg/icepdf/index/core/util/Hashtable;->elements()Ljava/util/Enumeration;

    move-result-object v5

    .line 88
    .local v5, "xobjectContent":Ljava/util/Enumeration;
    :cond_3
    :goto_1
    invoke-interface {v5}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v6

    if-eqz v6, :cond_5

    .line 89
    invoke-interface {v5}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v4

    .line 90
    .local v4, "tmp":Ljava/lang/Object;
    instance-of v6, v4, Lorg/icepdf/index/core/pobjects/Stream;

    if-eqz v6, :cond_4

    move-object v3, v4

    .line 91
    check-cast v3, Lorg/icepdf/index/core/pobjects/Stream;

    .line 92
    .local v3, "stream":Lorg/icepdf/index/core/pobjects/Stream;
    invoke-virtual {v3, p1}, Lorg/icepdf/index/core/pobjects/Stream;->dispose(Z)V

    .line 94
    .end local v3    # "stream":Lorg/icepdf/index/core/pobjects/Stream;
    :cond_4
    instance-of v6, v4, Lorg/icepdf/index/core/pobjects/Reference;

    if-eqz v6, :cond_3

    .line 95
    iget-object v6, p0, Lorg/icepdf/index/core/pobjects/Resources;->library:Lorg/icepdf/index/core/util/Library;

    check-cast v4, Lorg/icepdf/index/core/pobjects/Reference;

    .end local v4    # "tmp":Ljava/lang/Object;
    invoke-virtual {v6, v4}, Lorg/icepdf/index/core/util/Library;->getObject(Lorg/icepdf/index/core/pobjects/Reference;)Ljava/lang/Object;

    move-result-object v1

    .line 100
    .local v1, "reference":Ljava/lang/Object;
    instance-of v6, v1, Lorg/icepdf/index/core/pobjects/Stream;

    if-eqz v6, :cond_3

    move-object v3, v1

    .line 101
    check-cast v3, Lorg/icepdf/index/core/pobjects/Stream;

    .line 102
    .restart local v3    # "stream":Lorg/icepdf/index/core/pobjects/Stream;
    invoke-virtual {v3, p1}, Lorg/icepdf/index/core/pobjects/Stream;->dispose(Z)V

    goto :goto_1

    .line 107
    .end local v1    # "reference":Ljava/lang/Object;
    .end local v3    # "stream":Lorg/icepdf/index/core/pobjects/Stream;
    .end local v5    # "xobjectContent":Ljava/util/Enumeration;
    :cond_5
    return-void
.end method

.method public getFont(Ljava/lang/String;)Lorg/icepdf/index/core/pobjects/fonts/Font;
    .locals 3
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    .line 115
    const/4 v0, 0x0

    .line 116
    .local v0, "font":Lorg/icepdf/index/core/pobjects/fonts/Font;
    iget-object v2, p0, Lorg/icepdf/index/core/pobjects/Resources;->fonts:Lorg/icepdf/index/core/util/Hashtable;

    if-eqz v2, :cond_0

    .line 117
    iget-object v2, p0, Lorg/icepdf/index/core/pobjects/Resources;->fonts:Lorg/icepdf/index/core/util/Hashtable;

    invoke-virtual {v2, p1}, Lorg/icepdf/index/core/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 119
    .local v1, "ob":Ljava/lang/Object;
    instance-of v2, v1, Lorg/icepdf/index/core/pobjects/fonts/Font;

    if-eqz v2, :cond_2

    move-object v0, v1

    .line 120
    check-cast v0, Lorg/icepdf/index/core/pobjects/fonts/Font;

    .line 127
    .end local v1    # "ob":Ljava/lang/Object;
    :cond_0
    :goto_0
    if-eqz v0, :cond_1

    .line 128
    invoke-virtual {v0}, Lorg/icepdf/index/core/pobjects/fonts/Font;->init()V

    .line 130
    :cond_1
    return-object v0

    .line 123
    .restart local v1    # "ob":Ljava/lang/Object;
    :cond_2
    instance-of v2, v1, Lorg/icepdf/index/core/pobjects/Reference;

    if-eqz v2, :cond_0

    .line 124
    iget-object v2, p0, Lorg/icepdf/index/core/pobjects/Resources;->library:Lorg/icepdf/index/core/util/Library;

    check-cast v1, Lorg/icepdf/index/core/pobjects/Reference;

    .end local v1    # "ob":Ljava/lang/Object;
    invoke-virtual {v2, v1}, Lorg/icepdf/index/core/util/Library;->getObject(Lorg/icepdf/index/core/pobjects/Reference;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "font":Lorg/icepdf/index/core/pobjects/fonts/Font;
    check-cast v0, Lorg/icepdf/index/core/pobjects/fonts/Font;

    .restart local v0    # "font":Lorg/icepdf/index/core/pobjects/fonts/Font;
    goto :goto_0
.end method
