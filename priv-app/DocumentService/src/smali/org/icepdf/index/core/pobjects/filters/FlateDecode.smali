.class public Lorg/icepdf/index/core/pobjects/filters/FlateDecode;
.super Lorg/icepdf/index/core/pobjects/filters/ChunkingInputStream;
.source "FlateDecode.java"


# static fields
.field private static final LZW_FLATE_PREDICTOR_NONE:I = 0x1

.field private static final LZW_FLATE_PREDICTOR_PNG_AVG:I = 0xd

.field private static final LZW_FLATE_PREDICTOR_PNG_NONE:I = 0xa

.field private static final LZW_FLATE_PREDICTOR_PNG_OPTIMUM:I = 0xf

.field private static final LZW_FLATE_PREDICTOR_PNG_PAETH:I = 0xe

.field private static final LZW_FLATE_PREDICTOR_PNG_SUB:I = 0xb

.field private static final LZW_FLATE_PREDICTOR_PNG_UP:I = 0xc

.field private static final LZW_FLATE_PREDICTOR_TIFF_2:I = 0x2


# instance fields
.field private aboveBuffer:[B

.field private bitsPerComponent:I

.field private bpp:I

.field private numComponents:I

.field private originalInputKeptSolelyForDebugging:Ljava/io/InputStream;

.field private predictor:I

.field private width:I


# direct methods
.method public constructor <init>(Lorg/icepdf/index/core/util/Library;Lorg/icepdf/index/core/util/Hashtable;Ljava/io/InputStream;)V
    .locals 8
    .param p1, "library"    # Lorg/icepdf/index/core/util/Library;
    .param p2, "props"    # Lorg/icepdf/index/core/util/Hashtable;
    .param p3, "input"    # Ljava/io/InputStream;

    .prologue
    const/4 v5, 0x0

    const/4 v7, 0x1

    .line 84
    invoke-direct {p0}, Lorg/icepdf/index/core/pobjects/filters/ChunkingInputStream;-><init>()V

    .line 78
    iput v7, p0, Lorg/icepdf/index/core/pobjects/filters/FlateDecode;->bpp:I

    .line 85
    iput-object p3, p0, Lorg/icepdf/index/core/pobjects/filters/FlateDecode;->originalInputKeptSolelyForDebugging:Ljava/io/InputStream;

    .line 86
    iput v5, p0, Lorg/icepdf/index/core/pobjects/filters/FlateDecode;->width:I

    .line 87
    iput v5, p0, Lorg/icepdf/index/core/pobjects/filters/FlateDecode;->numComponents:I

    .line 88
    iput v5, p0, Lorg/icepdf/index/core/pobjects/filters/FlateDecode;->bitsPerComponent:I

    .line 89
    iput v7, p0, Lorg/icepdf/index/core/pobjects/filters/FlateDecode;->bpp:I

    .line 91
    const/16 v2, 0x1000

    .line 94
    .local v2, "intermediateBufferSize":I
    const-string/jumbo v5, "DecodeParms"

    invoke-virtual {p1, p2, v5}, Lorg/icepdf/index/core/util/Library;->getDictionary(Lorg/icepdf/index/core/util/Hashtable;Ljava/lang/String;)Lorg/icepdf/index/core/util/Hashtable;

    move-result-object v1

    .line 95
    .local v1, "decodeParmsDictionary":Lorg/icepdf/index/core/util/Hashtable;
    const-string/jumbo v5, "Predictor"

    invoke-virtual {p1, v1, v5}, Lorg/icepdf/index/core/util/Library;->getInt(Lorg/icepdf/index/core/util/Hashtable;Ljava/lang/String;)I

    move-result v5

    iput v5, p0, Lorg/icepdf/index/core/pobjects/filters/FlateDecode;->predictor:I

    .line 96
    iget v5, p0, Lorg/icepdf/index/core/pobjects/filters/FlateDecode;->predictor:I

    if-eq v5, v7, :cond_0

    iget v5, p0, Lorg/icepdf/index/core/pobjects/filters/FlateDecode;->predictor:I

    const/4 v6, 0x2

    if-eq v5, v6, :cond_0

    iget v5, p0, Lorg/icepdf/index/core/pobjects/filters/FlateDecode;->predictor:I

    const/16 v6, 0xa

    if-eq v5, v6, :cond_0

    iget v5, p0, Lorg/icepdf/index/core/pobjects/filters/FlateDecode;->predictor:I

    const/16 v6, 0xb

    if-eq v5, v6, :cond_0

    iget v5, p0, Lorg/icepdf/index/core/pobjects/filters/FlateDecode;->predictor:I

    const/16 v6, 0xc

    if-eq v5, v6, :cond_0

    iget v5, p0, Lorg/icepdf/index/core/pobjects/filters/FlateDecode;->predictor:I

    const/16 v6, 0xd

    if-eq v5, v6, :cond_0

    iget v5, p0, Lorg/icepdf/index/core/pobjects/filters/FlateDecode;->predictor:I

    const/16 v6, 0xe

    if-eq v5, v6, :cond_0

    iget v5, p0, Lorg/icepdf/index/core/pobjects/filters/FlateDecode;->predictor:I

    const/16 v6, 0xf

    if-eq v5, v6, :cond_0

    .line 100
    iput v7, p0, Lorg/icepdf/index/core/pobjects/filters/FlateDecode;->predictor:I

    .line 103
    :cond_0
    iget v5, p0, Lorg/icepdf/index/core/pobjects/filters/FlateDecode;->predictor:I

    if-eq v5, v7, :cond_3

    .line 104
    const-string/jumbo v5, "Width"

    invoke-virtual {p1, p2, v5}, Lorg/icepdf/index/core/util/Library;->getNumber(Lorg/icepdf/index/core/util/Hashtable;Ljava/lang/String;)Ljava/lang/Number;

    move-result-object v4

    .line 105
    .local v4, "widthNumber":Ljava/lang/Number;
    if-eqz v4, :cond_4

    .line 106
    invoke-virtual {v4}, Ljava/lang/Number;->intValue()I

    move-result v5

    iput v5, p0, Lorg/icepdf/index/core/pobjects/filters/FlateDecode;->width:I

    .line 117
    :goto_0
    iput v7, p0, Lorg/icepdf/index/core/pobjects/filters/FlateDecode;->numComponents:I

    .line 118
    const/16 v5, 0x8

    iput v5, p0, Lorg/icepdf/index/core/pobjects/filters/FlateDecode;->bitsPerComponent:I

    .line 120
    const-string/jumbo v5, "Colors"

    invoke-virtual {p1, v1, v5}, Lorg/icepdf/index/core/util/Library;->getObject(Lorg/icepdf/index/core/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    .line 121
    .local v3, "numComponentsDecodeParmsObj":Ljava/lang/Object;
    instance-of v5, v3, Ljava/lang/Number;

    if-eqz v5, :cond_1

    .line 122
    check-cast v3, Ljava/lang/Number;

    .end local v3    # "numComponentsDecodeParmsObj":Ljava/lang/Object;
    invoke-virtual {v3}, Ljava/lang/Number;->intValue()I

    move-result v5

    iput v5, p0, Lorg/icepdf/index/core/pobjects/filters/FlateDecode;->numComponents:I

    .line 125
    :cond_1
    const-string/jumbo v5, "BitsPerComponent"

    invoke-virtual {p1, v1, v5}, Lorg/icepdf/index/core/util/Library;->getObject(Lorg/icepdf/index/core/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 126
    .local v0, "bitsPerComponentDecodeParmsObj":Ljava/lang/Object;
    instance-of v5, v0, Ljava/lang/Number;

    if-eqz v5, :cond_2

    .line 127
    check-cast v0, Ljava/lang/Number;

    .end local v0    # "bitsPerComponentDecodeParmsObj":Ljava/lang/Object;
    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v5

    iput v5, p0, Lorg/icepdf/index/core/pobjects/filters/FlateDecode;->bitsPerComponent:I

    .line 131
    :cond_2
    iget v5, p0, Lorg/icepdf/index/core/pobjects/filters/FlateDecode;->numComponents:I

    iget v6, p0, Lorg/icepdf/index/core/pobjects/filters/FlateDecode;->bitsPerComponent:I

    mul-int/2addr v5, v6

    invoke-static {v5}, Lorg/icepdf/index/core/util/Utils;->numBytesToHoldBits(I)I

    move-result v5

    invoke-static {v7, v5}, Ljava/lang/Math;->max(II)I

    move-result v5

    iput v5, p0, Lorg/icepdf/index/core/pobjects/filters/FlateDecode;->bpp:I

    .line 135
    iget v5, p0, Lorg/icepdf/index/core/pobjects/filters/FlateDecode;->width:I

    iget v6, p0, Lorg/icepdf/index/core/pobjects/filters/FlateDecode;->numComponents:I

    mul-int/2addr v5, v6

    iget v6, p0, Lorg/icepdf/index/core/pobjects/filters/FlateDecode;->bitsPerComponent:I

    mul-int/2addr v5, v6

    invoke-static {v5}, Lorg/icepdf/index/core/util/Utils;->numBytesToHoldBits(I)I

    move-result v2

    .line 141
    .end local v4    # "widthNumber":Ljava/lang/Number;
    :cond_3
    new-instance v5, Ljava/util/zip/InflaterInputStream;

    invoke-direct {v5, p3}, Ljava/util/zip/InflaterInputStream;-><init>(Ljava/io/InputStream;)V

    invoke-virtual {p0, v5}, Lorg/icepdf/index/core/pobjects/filters/FlateDecode;->setInputStream(Ljava/io/InputStream;)V

    .line 142
    invoke-virtual {p0, v2}, Lorg/icepdf/index/core/pobjects/filters/FlateDecode;->setBufferSize(I)V

    .line 143
    new-array v5, v2, [B

    iput-object v5, p0, Lorg/icepdf/index/core/pobjects/filters/FlateDecode;->aboveBuffer:[B

    .line 144
    return-void

    .line 108
    .restart local v4    # "widthNumber":Ljava/lang/Number;
    :cond_4
    const-string/jumbo v5, "Columns"

    invoke-virtual {p1, v1, v5}, Lorg/icepdf/index/core/util/Library;->getInt(Lorg/icepdf/index/core/util/Hashtable;Ljava/lang/String;)I

    move-result v5

    iput v5, p0, Lorg/icepdf/index/core/pobjects/filters/FlateDecode;->width:I

    goto :goto_0
.end method


# virtual methods
.method protected fillInternalBuffer()I
    .locals 22
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 148
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/filters/FlateDecode;->aboveBuffer:[B

    move-object/from16 v17, v0

    .line 149
    .local v17, "temp":[B
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/filters/FlateDecode;->buffer:[B

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/icepdf/index/core/pobjects/filters/FlateDecode;->aboveBuffer:[B

    .line 150
    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/icepdf/index/core/pobjects/filters/FlateDecode;->buffer:[B

    .line 155
    move-object/from16 v0, p0

    iget v0, v0, Lorg/icepdf/index/core/pobjects/filters/FlateDecode;->predictor:I

    move/from16 v18, v0

    const/16 v19, 0x1

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_1

    .line 156
    invoke-virtual/range {p0 .. p0}, Lorg/icepdf/index/core/pobjects/filters/FlateDecode;->fillBufferFromInputStream()I

    move-result v9

    .line 157
    .local v9, "numRead":I
    if-gtz v9, :cond_0

    .line 158
    const/4 v9, -0x1

    .line 264
    .end local v9    # "numRead":I
    :cond_0
    :goto_0
    return v9

    .line 160
    :cond_1
    move-object/from16 v0, p0

    iget v0, v0, Lorg/icepdf/index/core/pobjects/filters/FlateDecode;->predictor:I

    move/from16 v18, v0

    const/16 v19, 0x2

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_4

    .line 161
    invoke-virtual/range {p0 .. p0}, Lorg/icepdf/index/core/pobjects/filters/FlateDecode;->fillBufferFromInputStream()I

    move-result v9

    .line 162
    .restart local v9    # "numRead":I
    if-gtz v9, :cond_2

    .line 163
    const/4 v9, -0x1

    goto :goto_0

    .line 164
    :cond_2
    move-object/from16 v0, p0

    iget v0, v0, Lorg/icepdf/index/core/pobjects/filters/FlateDecode;->bitsPerComponent:I

    move/from16 v18, v0

    const/16 v19, 0x8

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_0

    .line 165
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_1
    if-ge v7, v9, :cond_0

    .line 166
    move-object/from16 v0, p0

    iget v0, v0, Lorg/icepdf/index/core/pobjects/filters/FlateDecode;->numComponents:I

    move/from16 v18, v0

    sub-int v15, v7, v18

    .line 167
    .local v15, "prevIndex":I
    if-ltz v15, :cond_3

    .line 168
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/filters/FlateDecode;->buffer:[B

    move-object/from16 v18, v0

    aget-byte v19, v18, v7

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/filters/FlateDecode;->buffer:[B

    move-object/from16 v20, v0

    aget-byte v20, v20, v15

    add-int v19, v19, v20

    move/from16 v0, v19

    int-to-byte v0, v0

    move/from16 v19, v0

    aput-byte v19, v18, v7

    .line 165
    :cond_3
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 177
    .end local v7    # "i":I
    .end local v9    # "numRead":I
    .end local v15    # "prevIndex":I
    :cond_4
    move-object/from16 v0, p0

    iget v0, v0, Lorg/icepdf/index/core/pobjects/filters/FlateDecode;->predictor:I

    move/from16 v18, v0

    const/16 v19, 0xa

    move/from16 v0, v18

    move/from16 v1, v19

    if-lt v0, v1, :cond_12

    move-object/from16 v0, p0

    iget v0, v0, Lorg/icepdf/index/core/pobjects/filters/FlateDecode;->predictor:I

    move/from16 v18, v0

    const/16 v19, 0xf

    move/from16 v0, v18

    move/from16 v1, v19

    if-gt v0, v1, :cond_12

    .line 178
    move-object/from16 v0, p0

    iget v6, v0, Lorg/icepdf/index/core/pobjects/filters/FlateDecode;->predictor:I

    .line 179
    .local v6, "currPredictor":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/filters/FlateDecode;->in:Ljava/io/InputStream;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Ljava/io/InputStream;->read()I

    move-result v5

    .line 181
    .local v5, "cp":I
    if-gez v5, :cond_5

    .line 182
    const/4 v9, -0x1

    goto :goto_0

    .line 187
    :cond_5
    add-int/lit8 v6, v5, 0xa

    .line 191
    invoke-virtual/range {p0 .. p0}, Lorg/icepdf/index/core/pobjects/filters/FlateDecode;->fillBufferFromInputStream()I

    move-result v9

    .line 192
    .restart local v9    # "numRead":I
    if-gtz v9, :cond_6

    .line 193
    const/4 v9, -0x1

    goto/16 :goto_0

    .line 196
    :cond_6
    const/4 v7, 0x0

    .restart local v7    # "i":I
    :goto_2
    if-ge v7, v9, :cond_0

    .line 199
    const/16 v18, 0xa

    move/from16 v0, v18

    if-eq v6, v0, :cond_0

    .line 202
    const/16 v18, 0xb

    move/from16 v0, v18

    if-ne v6, v0, :cond_8

    .line 203
    move-object/from16 v0, p0

    iget v0, v0, Lorg/icepdf/index/core/pobjects/filters/FlateDecode;->bpp:I

    move/from16 v18, v0

    sub-int v18, v7, v18

    if-ltz v18, :cond_7

    .line 204
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/filters/FlateDecode;->buffer:[B

    move-object/from16 v18, v0

    aget-byte v19, v18, v7

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/filters/FlateDecode;->buffer:[B

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/icepdf/index/core/pobjects/filters/FlateDecode;->bpp:I

    move/from16 v21, v0

    sub-int v21, v7, v21

    aget-byte v20, v20, v21

    add-int v19, v19, v20

    move/from16 v0, v19

    int-to-byte v0, v0

    move/from16 v19, v0

    aput-byte v19, v18, v7

    .line 196
    :cond_7
    :goto_3
    add-int/lit8 v7, v7, 0x1

    goto :goto_2

    .line 208
    :cond_8
    const/16 v18, 0xc

    move/from16 v0, v18

    if-ne v6, v0, :cond_9

    .line 209
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/filters/FlateDecode;->aboveBuffer:[B

    move-object/from16 v18, v0

    if-eqz v18, :cond_7

    .line 210
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/filters/FlateDecode;->buffer:[B

    move-object/from16 v18, v0

    aget-byte v19, v18, v7

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/filters/FlateDecode;->aboveBuffer:[B

    move-object/from16 v20, v0

    aget-byte v20, v20, v7

    add-int v19, v19, v20

    move/from16 v0, v19

    int-to-byte v0, v0

    move/from16 v19, v0

    aput-byte v19, v18, v7

    goto :goto_3

    .line 213
    :cond_9
    const/16 v18, 0xd

    move/from16 v0, v18

    if-ne v6, v0, :cond_c

    .line 216
    const/4 v8, 0x0

    .line 217
    .local v8, "left":I
    move-object/from16 v0, p0

    iget v0, v0, Lorg/icepdf/index/core/pobjects/filters/FlateDecode;->bpp:I

    move/from16 v18, v0

    sub-int v18, v7, v18

    if-ltz v18, :cond_a

    .line 218
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/filters/FlateDecode;->buffer:[B

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/icepdf/index/core/pobjects/filters/FlateDecode;->bpp:I

    move/from16 v19, v0

    sub-int v19, v7, v19

    aget-byte v18, v18, v19

    move/from16 v0, v18

    and-int/lit16 v8, v0, 0xff

    .line 219
    :cond_a
    const/4 v2, 0x0

    .line 220
    .local v2, "above":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/filters/FlateDecode;->aboveBuffer:[B

    move-object/from16 v18, v0

    if-eqz v18, :cond_b

    .line 221
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/filters/FlateDecode;->aboveBuffer:[B

    move-object/from16 v18, v0

    aget-byte v18, v18, v7

    move/from16 v0, v18

    and-int/lit16 v2, v0, 0xff

    .line 222
    :cond_b
    add-int v16, v8, v2

    .line 223
    .local v16, "sum":I
    ushr-int/lit8 v18, v16, 0x1

    move/from16 v0, v18

    and-int/lit16 v0, v0, 0xff

    move/from16 v18, v0

    move/from16 v0, v18

    int-to-byte v4, v0

    .line 224
    .local v4, "avg":B
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/filters/FlateDecode;->buffer:[B

    move-object/from16 v18, v0

    aget-byte v19, v18, v7

    add-int v19, v19, v4

    move/from16 v0, v19

    int-to-byte v0, v0

    move/from16 v19, v0

    aput-byte v19, v18, v7

    goto/16 :goto_3

    .line 228
    .end local v2    # "above":I
    .end local v4    # "avg":B
    .end local v8    # "left":I
    .end local v16    # "sum":I
    :cond_c
    const/16 v18, 0xe

    move/from16 v0, v18

    if-ne v6, v0, :cond_7

    .line 239
    const/4 v8, 0x0

    .line 240
    .restart local v8    # "left":I
    move-object/from16 v0, p0

    iget v0, v0, Lorg/icepdf/index/core/pobjects/filters/FlateDecode;->bpp:I

    move/from16 v18, v0

    sub-int v18, v7, v18

    if-ltz v18, :cond_d

    .line 241
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/filters/FlateDecode;->buffer:[B

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/icepdf/index/core/pobjects/filters/FlateDecode;->bpp:I

    move/from16 v19, v0

    sub-int v19, v7, v19

    aget-byte v18, v18, v19

    move/from16 v0, v18

    and-int/lit16 v8, v0, 0xff

    .line 242
    :cond_d
    const/4 v2, 0x0

    .line 243
    .restart local v2    # "above":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/filters/FlateDecode;->aboveBuffer:[B

    move-object/from16 v18, v0

    if-eqz v18, :cond_e

    .line 244
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/filters/FlateDecode;->aboveBuffer:[B

    move-object/from16 v18, v0

    aget-byte v18, v18, v7

    move/from16 v0, v18

    and-int/lit16 v2, v0, 0xff

    .line 245
    :cond_e
    const/4 v3, 0x0

    .line 246
    .local v3, "aboveLeft":I
    move-object/from16 v0, p0

    iget v0, v0, Lorg/icepdf/index/core/pobjects/filters/FlateDecode;->bpp:I

    move/from16 v18, v0

    sub-int v18, v7, v18

    if-ltz v18, :cond_f

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/filters/FlateDecode;->aboveBuffer:[B

    move-object/from16 v18, v0

    if-eqz v18, :cond_f

    .line 247
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/filters/FlateDecode;->aboveBuffer:[B

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/icepdf/index/core/pobjects/filters/FlateDecode;->bpp:I

    move/from16 v19, v0

    sub-int v19, v7, v19

    aget-byte v18, v18, v19

    move/from16 v0, v18

    and-int/lit16 v3, v0, 0xff

    .line 248
    :cond_f
    add-int v18, v8, v2

    sub-int v10, v18, v3

    .line 249
    .local v10, "p":I
    sub-int v18, v10, v8

    invoke-static/range {v18 .. v18}, Ljava/lang/Math;->abs(I)I

    move-result v13

    .line 250
    .local v13, "pLeft":I
    sub-int v18, v10, v2

    invoke-static/range {v18 .. v18}, Ljava/lang/Math;->abs(I)I

    move-result v11

    .line 251
    .local v11, "pAbove":I
    sub-int v18, v10, v3

    invoke-static/range {v18 .. v18}, Ljava/lang/Math;->abs(I)I

    move-result v12

    .line 252
    .local v12, "pAboveLeft":I
    if-gt v13, v11, :cond_10

    if-gt v13, v12, :cond_10

    move v14, v8

    .line 257
    .local v14, "paeth":I
    :goto_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/filters/FlateDecode;->buffer:[B

    move-object/from16 v18, v0

    aget-byte v19, v18, v7

    and-int/lit16 v0, v14, 0xff

    move/from16 v20, v0

    move/from16 v0, v20

    int-to-byte v0, v0

    move/from16 v20, v0

    add-int v19, v19, v20

    move/from16 v0, v19

    int-to-byte v0, v0

    move/from16 v19, v0

    aput-byte v19, v18, v7

    goto/16 :goto_3

    .line 252
    .end local v14    # "paeth":I
    :cond_10
    if-gt v11, v12, :cond_11

    move v14, v2

    goto :goto_4

    :cond_11
    move v14, v3

    goto :goto_4

    .line 264
    .end local v2    # "above":I
    .end local v3    # "aboveLeft":I
    .end local v5    # "cp":I
    .end local v6    # "currPredictor":I
    .end local v7    # "i":I
    .end local v8    # "left":I
    .end local v9    # "numRead":I
    .end local v10    # "p":I
    .end local v11    # "pAbove":I
    .end local v12    # "pAboveLeft":I
    .end local v13    # "pLeft":I
    :cond_12
    const/4 v9, -0x1

    goto/16 :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 269
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 270
    .local v0, "sb":Ljava/lang/StringBuffer;
    invoke-super {p0}, Lorg/icepdf/index/core/pobjects/filters/ChunkingInputStream;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 271
    const-string/jumbo v1, ", orig: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 272
    iget-object v1, p0, Lorg/icepdf/index/core/pobjects/filters/FlateDecode;->originalInputKeptSolelyForDebugging:Ljava/io/InputStream;

    if-nez v1, :cond_0

    .line 273
    const-string/jumbo v1, "null"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 276
    :goto_0
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 275
    :cond_0
    iget-object v1, p0, Lorg/icepdf/index/core/pobjects/filters/FlateDecode;->originalInputKeptSolelyForDebugging:Ljava/io/InputStream;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0
.end method
