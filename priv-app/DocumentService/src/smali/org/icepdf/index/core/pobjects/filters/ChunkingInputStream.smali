.class public abstract Lorg/icepdf/index/core/pobjects/filters/ChunkingInputStream;
.super Ljava/io/InputStream;
.source "ChunkingInputStream.java"


# instance fields
.field protected buffer:[B

.field private bufferAvailable:I

.field private bufferPosition:I

.field protected in:Ljava/io/InputStream;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 33
    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    .line 34
    iput-object v1, p0, Lorg/icepdf/index/core/pobjects/filters/ChunkingInputStream;->in:Ljava/io/InputStream;

    .line 35
    iput-object v1, p0, Lorg/icepdf/index/core/pobjects/filters/ChunkingInputStream;->buffer:[B

    .line 36
    iput v0, p0, Lorg/icepdf/index/core/pobjects/filters/ChunkingInputStream;->bufferPosition:I

    .line 37
    iput v0, p0, Lorg/icepdf/index/core/pobjects/filters/ChunkingInputStream;->bufferAvailable:I

    .line 38
    return-void
.end method

.method private ensureDataAvailable()I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 80
    iget v1, p0, Lorg/icepdf/index/core/pobjects/filters/ChunkingInputStream;->bufferAvailable:I

    if-lez v1, :cond_0

    .line 81
    iget v1, p0, Lorg/icepdf/index/core/pobjects/filters/ChunkingInputStream;->bufferAvailable:I

    .line 87
    :goto_0
    return v1

    .line 82
    :cond_0
    iput v2, p0, Lorg/icepdf/index/core/pobjects/filters/ChunkingInputStream;->bufferPosition:I

    .line 83
    iput v2, p0, Lorg/icepdf/index/core/pobjects/filters/ChunkingInputStream;->bufferAvailable:I

    .line 84
    invoke-virtual {p0}, Lorg/icepdf/index/core/pobjects/filters/ChunkingInputStream;->fillInternalBuffer()I

    move-result v0

    .line 85
    .local v0, "avail":I
    if-lez v0, :cond_1

    .line 86
    iput v0, p0, Lorg/icepdf/index/core/pobjects/filters/ChunkingInputStream;->bufferAvailable:I

    .line 87
    :cond_1
    iget v1, p0, Lorg/icepdf/index/core/pobjects/filters/ChunkingInputStream;->bufferAvailable:I

    goto :goto_0
.end method


# virtual methods
.method public available()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 162
    iget v0, p0, Lorg/icepdf/index/core/pobjects/filters/ChunkingInputStream;->bufferAvailable:I

    return v0
.end method

.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 166
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/filters/ChunkingInputStream;->in:Ljava/io/InputStream;

    if-eqz v0, :cond_0

    .line 167
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/filters/ChunkingInputStream;->in:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    .line 168
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/icepdf/index/core/pobjects/filters/ChunkingInputStream;->in:Ljava/io/InputStream;

    .line 170
    :cond_0
    return-void
.end method

.method protected fillBufferFromInputStream()I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 56
    const/4 v0, 0x0

    iget-object v1, p0, Lorg/icepdf/index/core/pobjects/filters/ChunkingInputStream;->buffer:[B

    array-length v1, v1

    invoke-virtual {p0, v0, v1}, Lorg/icepdf/index/core/pobjects/filters/ChunkingInputStream;->fillBufferFromInputStream(II)I

    move-result v0

    return v0
.end method

.method protected fillBufferFromInputStream(II)I
    .locals 6
    .param p1, "offset"    # I
    .param p2, "length"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 60
    const/4 v1, 0x0

    .line 61
    .local v1, "read":I
    :goto_0
    if-ge v1, p2, :cond_1

    .line 62
    iget-object v2, p0, Lorg/icepdf/index/core/pobjects/filters/ChunkingInputStream;->in:Ljava/io/InputStream;

    iget-object v3, p0, Lorg/icepdf/index/core/pobjects/filters/ChunkingInputStream;->buffer:[B

    add-int v4, p1, v1

    sub-int v5, p2, v1

    invoke-virtual {v2, v3, v4, v5}, Ljava/io/InputStream;->read([BII)I

    move-result v0

    .line 63
    .local v0, "currRead":I
    if-gez v0, :cond_0

    if-nez v1, :cond_0

    .line 69
    .end local v0    # "currRead":I
    :goto_1
    return v0

    .line 65
    .restart local v0    # "currRead":I
    :cond_0
    if-gtz v0, :cond_2

    .end local v0    # "currRead":I
    :cond_1
    move v0, v1

    .line 69
    goto :goto_1

    .line 67
    .restart local v0    # "currRead":I
    :cond_2
    add-int/2addr v1, v0

    .line 68
    goto :goto_0
.end method

.method protected abstract fillInternalBuffer()I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public mark(I)V
    .locals 0
    .param p1, "readlimit"    # I

    .prologue
    .line 100
    return-void
.end method

.method public markSupported()Z
    .locals 1

    .prologue
    .line 96
    const/4 v0, 0x0

    return v0
.end method

.method public read()I
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 106
    invoke-direct {p0}, Lorg/icepdf/index/core/pobjects/filters/ChunkingInputStream;->ensureDataAvailable()I

    move-result v0

    .line 107
    .local v0, "avail":I
    if-gtz v0, :cond_0

    .line 108
    const/4 v2, -0x1

    .line 112
    :goto_0
    return v2

    .line 109
    :cond_0
    iget-object v2, p0, Lorg/icepdf/index/core/pobjects/filters/ChunkingInputStream;->buffer:[B

    iget v3, p0, Lorg/icepdf/index/core/pobjects/filters/ChunkingInputStream;->bufferPosition:I

    aget-byte v1, v2, v3

    .line 110
    .local v1, "b":B
    iget v2, p0, Lorg/icepdf/index/core/pobjects/filters/ChunkingInputStream;->bufferPosition:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lorg/icepdf/index/core/pobjects/filters/ChunkingInputStream;->bufferPosition:I

    .line 111
    iget v2, p0, Lorg/icepdf/index/core/pobjects/filters/ChunkingInputStream;->bufferAvailable:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lorg/icepdf/index/core/pobjects/filters/ChunkingInputStream;->bufferAvailable:I

    .line 112
    and-int/lit16 v2, v1, 0xff

    goto :goto_0
.end method

.method public read([B)I
    .locals 2
    .param p1, "b"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 116
    const/4 v0, 0x0

    array-length v1, p1

    invoke-virtual {p0, p1, v0, v1}, Lorg/icepdf/index/core/pobjects/filters/ChunkingInputStream;->read([BII)I

    move-result v0

    return v0
.end method

.method public read([BII)I
    .locals 9
    .param p1, "b"    # [B
    .param p2, "off"    # I
    .param p3, "length"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 120
    const/4 v4, 0x0

    .line 121
    .local v4, "read":I
    :goto_0
    if-ge v4, p3, :cond_0

    .line 122
    invoke-direct {p0}, Lorg/icepdf/index/core/pobjects/filters/ChunkingInputStream;->ensureDataAvailable()I

    move-result v0

    .line 123
    .local v0, "avail":I
    if-gtz v0, :cond_2

    .line 124
    if-lez v4, :cond_1

    .line 139
    .end local v0    # "avail":I
    .end local v4    # "read":I
    :cond_0
    :goto_1
    return v4

    .line 127
    .restart local v0    # "avail":I
    .restart local v4    # "read":I
    :cond_1
    const/4 v4, -0x1

    goto :goto_1

    .line 130
    :cond_2
    sub-int v8, p3, v4

    invoke-static {v8, v0}, Ljava/lang/Math;->min(II)I

    move-result v7

    .line 131
    .local v7, "toRead":I
    iget v5, p0, Lorg/icepdf/index/core/pobjects/filters/ChunkingInputStream;->bufferPosition:I

    .line 132
    .local v5, "srcIdx":I
    add-int v1, p2, v4

    .line 133
    .local v1, "dstIdx":I
    const/4 v3, 0x0

    .local v3, "i":I
    move v2, v1

    .end local v1    # "dstIdx":I
    .local v2, "dstIdx":I
    move v6, v5

    .end local v5    # "srcIdx":I
    .local v6, "srcIdx":I
    :goto_2
    if-ge v3, v7, :cond_3

    .line 134
    add-int/lit8 v1, v2, 0x1

    .end local v2    # "dstIdx":I
    .restart local v1    # "dstIdx":I
    iget-object v8, p0, Lorg/icepdf/index/core/pobjects/filters/ChunkingInputStream;->buffer:[B

    add-int/lit8 v5, v6, 0x1

    .end local v6    # "srcIdx":I
    .restart local v5    # "srcIdx":I
    aget-byte v8, v8, v6

    aput-byte v8, p1, v2

    .line 133
    add-int/lit8 v3, v3, 0x1

    move v2, v1

    .end local v1    # "dstIdx":I
    .restart local v2    # "dstIdx":I
    move v6, v5

    .end local v5    # "srcIdx":I
    .restart local v6    # "srcIdx":I
    goto :goto_2

    .line 135
    :cond_3
    iget v8, p0, Lorg/icepdf/index/core/pobjects/filters/ChunkingInputStream;->bufferPosition:I

    add-int/2addr v8, v7

    iput v8, p0, Lorg/icepdf/index/core/pobjects/filters/ChunkingInputStream;->bufferPosition:I

    .line 136
    iget v8, p0, Lorg/icepdf/index/core/pobjects/filters/ChunkingInputStream;->bufferAvailable:I

    sub-int/2addr v8, v7

    iput v8, p0, Lorg/icepdf/index/core/pobjects/filters/ChunkingInputStream;->bufferAvailable:I

    .line 137
    add-int/2addr v4, v7

    .line 138
    goto :goto_0
.end method

.method public reset()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 103
    return-void
.end method

.method protected setBufferSize(I)V
    .locals 1
    .param p1, "size"    # I

    .prologue
    .line 45
    new-array v0, p1, [B

    iput-object v0, p0, Lorg/icepdf/index/core/pobjects/filters/ChunkingInputStream;->buffer:[B

    .line 46
    return-void
.end method

.method protected setInputStream(Ljava/io/InputStream;)V
    .locals 0
    .param p1, "input"    # Ljava/io/InputStream;

    .prologue
    .line 41
    iput-object p1, p0, Lorg/icepdf/index/core/pobjects/filters/ChunkingInputStream;->in:Ljava/io/InputStream;

    .line 42
    return-void
.end method

.method public skip(J)J
    .locals 11
    .param p1, "n"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 143
    const-wide/16 v2, 0x0

    .line 144
    .local v2, "skipped":J
    :goto_0
    cmp-long v1, v2, p1

    if-gez v1, :cond_0

    .line 145
    invoke-direct {p0}, Lorg/icepdf/index/core/pobjects/filters/ChunkingInputStream;->ensureDataAvailable()I

    move-result v0

    .line 146
    .local v0, "avail":I
    if-gtz v0, :cond_2

    .line 147
    const-wide/16 v6, 0x0

    cmp-long v1, v2, v6

    if-lez v1, :cond_1

    .line 158
    .end local v0    # "avail":I
    .end local v2    # "skipped":J
    :cond_0
    :goto_1
    return-wide v2

    .line 150
    .restart local v0    # "avail":I
    .restart local v2    # "skipped":J
    :cond_1
    const-wide/16 v2, -0x1

    goto :goto_1

    .line 153
    :cond_2
    sub-long v6, p1, v2

    int-to-long v8, v0

    invoke-static {v6, v7, v8, v9}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v4

    .line 154
    .local v4, "toSkip":J
    iget v1, p0, Lorg/icepdf/index/core/pobjects/filters/ChunkingInputStream;->bufferPosition:I

    int-to-long v6, v1

    add-long/2addr v6, v4

    long-to-int v1, v6

    iput v1, p0, Lorg/icepdf/index/core/pobjects/filters/ChunkingInputStream;->bufferPosition:I

    .line 155
    iget v1, p0, Lorg/icepdf/index/core/pobjects/filters/ChunkingInputStream;->bufferAvailable:I

    int-to-long v6, v1

    sub-long/2addr v6, v4

    long-to-int v1, v6

    iput v1, p0, Lorg/icepdf/index/core/pobjects/filters/ChunkingInputStream;->bufferAvailable:I

    .line 156
    add-long/2addr v2, v4

    .line 157
    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 174
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 175
    .local v0, "sb":Ljava/lang/StringBuffer;
    invoke-super {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 176
    const-string/jumbo v1, ": "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 177
    iget-object v1, p0, Lorg/icepdf/index/core/pobjects/filters/ChunkingInputStream;->in:Ljava/io/InputStream;

    if-nez v1, :cond_0

    .line 178
    const-string/jumbo v1, "null"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 181
    :goto_0
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 180
    :cond_0
    iget-object v1, p0, Lorg/icepdf/index/core/pobjects/filters/ChunkingInputStream;->in:Ljava/io/InputStream;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0
.end method
