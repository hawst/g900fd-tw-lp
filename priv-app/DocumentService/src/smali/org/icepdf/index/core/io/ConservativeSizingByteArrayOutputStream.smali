.class public Lorg/icepdf/index/core/io/ConservativeSizingByteArrayOutputStream;
.super Ljava/io/OutputStream;
.source "ConservativeSizingByteArrayOutputStream.java"


# instance fields
.field protected buf:[B

.field protected count:I

.field protected memoryManager:Lorg/icepdf/index/core/util/MemoryManager;


# direct methods
.method public constructor <init>(ILorg/icepdf/index/core/util/MemoryManager;)V
    .locals 3
    .param p1, "capacity"    # I
    .param p2, "mm"    # Lorg/icepdf/index/core/util/MemoryManager;

    .prologue
    const/4 v1, 0x0

    .line 38
    invoke-direct {p0}, Ljava/io/OutputStream;-><init>()V

    .line 39
    if-gez p1, :cond_0

    .line 40
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Negative initial capacity: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 42
    :cond_0
    iput-object p2, p0, Lorg/icepdf/index/core/io/ConservativeSizingByteArrayOutputStream;->memoryManager:Lorg/icepdf/index/core/util/MemoryManager;

    .line 43
    invoke-virtual {p0, p1, v1}, Lorg/icepdf/index/core/io/ConservativeSizingByteArrayOutputStream;->allocateByteArray(IZ)[B

    move-result-object v0

    iput-object v0, p0, Lorg/icepdf/index/core/io/ConservativeSizingByteArrayOutputStream;->buf:[B

    .line 44
    iput v1, p0, Lorg/icepdf/index/core/io/ConservativeSizingByteArrayOutputStream;->count:I

    .line 45
    return-void
.end method

.method public constructor <init>([BLorg/icepdf/index/core/util/MemoryManager;)V
    .locals 2
    .param p1, "buffer"    # [B
    .param p2, "mm"    # Lorg/icepdf/index/core/util/MemoryManager;

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/io/OutputStream;-><init>()V

    .line 55
    if-nez p1, :cond_0

    .line 56
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "Initial buffer is null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 57
    :cond_0
    array-length v0, p1

    if-nez v0, :cond_1

    .line 58
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "Initial buffer has zero length"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 59
    :cond_1
    iput-object p2, p0, Lorg/icepdf/index/core/io/ConservativeSizingByteArrayOutputStream;->memoryManager:Lorg/icepdf/index/core/util/MemoryManager;

    .line 60
    iput-object p1, p0, Lorg/icepdf/index/core/io/ConservativeSizingByteArrayOutputStream;->buf:[B

    .line 61
    const/4 v0, 0x0

    iput v0, p0, Lorg/icepdf/index/core/io/ConservativeSizingByteArrayOutputStream;->count:I

    .line 62
    return-void
.end method


# virtual methods
.method protected allocateByteArray(IZ)[B
    .locals 2
    .param p1, "size"    # I
    .param p2, "returnNullIfNoMemory"    # Z

    .prologue
    .line 178
    const/4 v0, 0x1

    .line 179
    .local v0, "canAlloc":Z
    iget-object v1, p0, Lorg/icepdf/index/core/io/ConservativeSizingByteArrayOutputStream;->memoryManager:Lorg/icepdf/index/core/util/MemoryManager;

    if-eqz v1, :cond_0

    const/high16 v1, 0x80000

    if-lt p1, v1, :cond_0

    .line 180
    iget-object v1, p0, Lorg/icepdf/index/core/io/ConservativeSizingByteArrayOutputStream;->memoryManager:Lorg/icepdf/index/core/util/MemoryManager;

    invoke-virtual {v1, p1}, Lorg/icepdf/index/core/util/MemoryManager;->checkMemory(I)Z

    move-result v0

    .line 181
    :cond_0
    if-eqz p2, :cond_1

    if-nez v0, :cond_1

    .line 182
    const/4 v1, 0x0

    .line 183
    :goto_0
    return-object v1

    :cond_1
    new-array v1, p1, [B

    goto :goto_0
.end method

.method public declared-synchronized relinquishByteArray()[B
    .locals 2

    .prologue
    .line 123
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/icepdf/index/core/io/ConservativeSizingByteArrayOutputStream;->buf:[B

    .line 124
    .local v0, "returnBuf":[B
    const/4 v1, 0x0

    iput-object v1, p0, Lorg/icepdf/index/core/io/ConservativeSizingByteArrayOutputStream;->buf:[B

    .line 125
    const/16 v1, 0x40

    new-array v1, v1, [B

    iput-object v1, p0, Lorg/icepdf/index/core/io/ConservativeSizingByteArrayOutputStream;->buf:[B

    .line 126
    const/4 v1, 0x0

    iput v1, p0, Lorg/icepdf/index/core/io/ConservativeSizingByteArrayOutputStream;->count:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 127
    monitor-exit p0

    return-object v0

    .line 123
    .end local v0    # "returnBuf":[B
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized reset()V
    .locals 1

    .prologue
    .line 91
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput v0, p0, Lorg/icepdf/index/core/io/ConservativeSizingByteArrayOutputStream;->count:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 92
    monitor-exit p0

    return-void

    .line 91
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected resizeArrayToFit(I)V
    .locals 6
    .param p1, "newCount"    # I

    .prologue
    const/4 v5, 0x0

    .line 149
    iget-object v3, p0, Lorg/icepdf/index/core/io/ConservativeSizingByteArrayOutputStream;->buf:[B

    array-length v2, v3

    .line 150
    .local v2, "steppedSize":I
    if-nez v2, :cond_0

    .line 151
    const/16 v2, 0x40

    .line 170
    :goto_0
    invoke-static {v2, p1}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 171
    .local v1, "newBufSize":I
    invoke-virtual {p0, v1, v5}, Lorg/icepdf/index/core/io/ConservativeSizingByteArrayOutputStream;->allocateByteArray(IZ)[B

    move-result-object v0

    .line 172
    .local v0, "newBuf":[B
    iget-object v3, p0, Lorg/icepdf/index/core/io/ConservativeSizingByteArrayOutputStream;->buf:[B

    iget v4, p0, Lorg/icepdf/index/core/io/ConservativeSizingByteArrayOutputStream;->count:I

    invoke-static {v3, v5, v0, v5, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 173
    const/4 v3, 0x0

    iput-object v3, p0, Lorg/icepdf/index/core/io/ConservativeSizingByteArrayOutputStream;->buf:[B

    .line 174
    iput-object v0, p0, Lorg/icepdf/index/core/io/ConservativeSizingByteArrayOutputStream;->buf:[B

    .line 175
    return-void

    .line 152
    .end local v0    # "newBuf":[B
    .end local v1    # "newBufSize":I
    :cond_0
    const/16 v3, 0x400

    if-gt v2, v3, :cond_1

    .line 153
    mul-int/lit8 v2, v2, 0x4

    goto :goto_0

    .line 154
    :cond_1
    const/16 v3, 0xfb8

    if-gt v2, v3, :cond_2

    .line 155
    mul-int/lit8 v2, v2, 0x2

    goto :goto_0

    .line 156
    :cond_2
    const/high16 v3, 0x200000

    if-gt v2, v3, :cond_3

    .line 157
    mul-int/lit8 v2, v2, 0x2

    .line 158
    and-int/lit16 v2, v2, -0x1000

    goto :goto_0

    .line 159
    :cond_3
    const/high16 v3, 0x400000

    if-gt v2, v3, :cond_4

    .line 160
    mul-int/lit8 v3, v2, 0x3

    div-int/lit8 v2, v3, 0x2

    .line 161
    and-int/lit16 v2, v2, -0x1000

    goto :goto_0

    .line 162
    :cond_4
    const/high16 v3, 0xf00000

    if-gt v2, v3, :cond_5

    .line 163
    mul-int/lit8 v3, v2, 0x5

    div-int/lit8 v2, v3, 0x4

    .line 164
    and-int/lit16 v2, v2, -0x1000

    goto :goto_0

    .line 166
    :cond_5
    const/high16 v3, 0x300000

    add-int/2addr v2, v3

    .line 167
    and-int/lit16 v2, v2, -0x1000

    goto :goto_0
.end method

.method public setMemoryManager(Lorg/icepdf/index/core/util/MemoryManager;)V
    .locals 0
    .param p1, "mm"    # Lorg/icepdf/index/core/util/MemoryManager;

    .prologue
    .line 65
    iput-object p1, p0, Lorg/icepdf/index/core/io/ConservativeSizingByteArrayOutputStream;->memoryManager:Lorg/icepdf/index/core/util/MemoryManager;

    .line 66
    return-void
.end method

.method public size()I
    .locals 1

    .prologue
    .line 113
    iget v0, p0, Lorg/icepdf/index/core/io/ConservativeSizingByteArrayOutputStream;->count:I

    return v0
.end method

.method public declared-synchronized toByteArray()[B
    .locals 5

    .prologue
    .line 102
    monitor-enter p0

    :try_start_0
    iget v1, p0, Lorg/icepdf/index/core/io/ConservativeSizingByteArrayOutputStream;->count:I

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Lorg/icepdf/index/core/io/ConservativeSizingByteArrayOutputStream;->allocateByteArray(IZ)[B

    move-result-object v0

    .line 103
    .local v0, "newBuf":[B
    iget-object v1, p0, Lorg/icepdf/index/core/io/ConservativeSizingByteArrayOutputStream;->buf:[B

    const/4 v2, 0x0

    const/4 v3, 0x0

    iget v4, p0, Lorg/icepdf/index/core/io/ConservativeSizingByteArrayOutputStream;->count:I

    invoke-static {v1, v2, v0, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 104
    monitor-exit p0

    return-object v0

    .line 102
    .end local v0    # "newBuf":[B
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public trim()Z
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 134
    iget v3, p0, Lorg/icepdf/index/core/io/ConservativeSizingByteArrayOutputStream;->count:I

    if-nez v3, :cond_1

    iget-object v3, p0, Lorg/icepdf/index/core/io/ConservativeSizingByteArrayOutputStream;->buf:[B

    if-eqz v3, :cond_0

    iget-object v3, p0, Lorg/icepdf/index/core/io/ConservativeSizingByteArrayOutputStream;->buf:[B

    array-length v3, v3

    if-nez v3, :cond_1

    .line 145
    :cond_0
    :goto_0
    return v1

    .line 136
    :cond_1
    iget v3, p0, Lorg/icepdf/index/core/io/ConservativeSizingByteArrayOutputStream;->count:I

    iget-object v4, p0, Lorg/icepdf/index/core/io/ConservativeSizingByteArrayOutputStream;->buf:[B

    array-length v4, v4

    if-eq v3, v4, :cond_0

    .line 139
    iget v3, p0, Lorg/icepdf/index/core/io/ConservativeSizingByteArrayOutputStream;->count:I

    invoke-virtual {p0, v3, v1}, Lorg/icepdf/index/core/io/ConservativeSizingByteArrayOutputStream;->allocateByteArray(IZ)[B

    move-result-object v0

    .line 140
    .local v0, "newBuf":[B
    if-nez v0, :cond_2

    move v1, v2

    .line 141
    goto :goto_0

    .line 142
    :cond_2
    iget-object v3, p0, Lorg/icepdf/index/core/io/ConservativeSizingByteArrayOutputStream;->buf:[B

    iget v4, p0, Lorg/icepdf/index/core/io/ConservativeSizingByteArrayOutputStream;->count:I

    invoke-static {v3, v2, v0, v2, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 143
    const/4 v2, 0x0

    iput-object v2, p0, Lorg/icepdf/index/core/io/ConservativeSizingByteArrayOutputStream;->buf:[B

    .line 144
    iput-object v0, p0, Lorg/icepdf/index/core/io/ConservativeSizingByteArrayOutputStream;->buf:[B

    goto :goto_0
.end method

.method public declared-synchronized write(I)V
    .locals 4
    .param p1, "b"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 69
    monitor-enter p0

    :try_start_0
    iget v1, p0, Lorg/icepdf/index/core/io/ConservativeSizingByteArrayOutputStream;->count:I

    add-int/lit8 v0, v1, 0x1

    .line 70
    .local v0, "newCount":I
    iget-object v1, p0, Lorg/icepdf/index/core/io/ConservativeSizingByteArrayOutputStream;->buf:[B

    array-length v1, v1

    if-le v0, v1, :cond_0

    .line 71
    invoke-virtual {p0, v0}, Lorg/icepdf/index/core/io/ConservativeSizingByteArrayOutputStream;->resizeArrayToFit(I)V

    .line 72
    :cond_0
    iget-object v1, p0, Lorg/icepdf/index/core/io/ConservativeSizingByteArrayOutputStream;->buf:[B

    iget v2, p0, Lorg/icepdf/index/core/io/ConservativeSizingByteArrayOutputStream;->count:I

    int-to-byte v3, p1

    aput-byte v3, v1, v2

    .line 73
    iput v0, p0, Lorg/icepdf/index/core/io/ConservativeSizingByteArrayOutputStream;->count:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 74
    monitor-exit p0

    return-void

    .line 69
    .end local v0    # "newCount":I
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized write([BII)V
    .locals 3
    .param p1, "b"    # [B
    .param p2, "off"    # I
    .param p3, "len"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 77
    monitor-enter p0

    if-ltz p2, :cond_0

    :try_start_0
    array-length v1, p1

    if-ge p2, v1, :cond_0

    if-ltz p3, :cond_0

    add-int v1, p2, p3

    array-length v2, p1

    if-gt v1, v2, :cond_0

    add-int v1, p2, p3

    if-gez v1, :cond_1

    .line 79
    :cond_0
    new-instance v1, Ljava/lang/IndexOutOfBoundsException;

    invoke-direct {v1}, Ljava/lang/IndexOutOfBoundsException;-><init>()V

    throw v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 77
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 81
    :cond_1
    if-nez p3, :cond_2

    .line 88
    :goto_0
    monitor-exit p0

    return-void

    .line 83
    :cond_2
    :try_start_1
    iget v1, p0, Lorg/icepdf/index/core/io/ConservativeSizingByteArrayOutputStream;->count:I

    add-int v0, v1, p3

    .line 84
    .local v0, "newCount":I
    iget-object v1, p0, Lorg/icepdf/index/core/io/ConservativeSizingByteArrayOutputStream;->buf:[B

    array-length v1, v1

    if-le v0, v1, :cond_3

    .line 85
    invoke-virtual {p0, v0}, Lorg/icepdf/index/core/io/ConservativeSizingByteArrayOutputStream;->resizeArrayToFit(I)V

    .line 86
    :cond_3
    iget-object v1, p0, Lorg/icepdf/index/core/io/ConservativeSizingByteArrayOutputStream;->buf:[B

    iget v2, p0, Lorg/icepdf/index/core/io/ConservativeSizingByteArrayOutputStream;->count:I

    invoke-static {p1, p2, v1, v2, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 87
    iput v0, p0, Lorg/icepdf/index/core/io/ConservativeSizingByteArrayOutputStream;->count:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method
