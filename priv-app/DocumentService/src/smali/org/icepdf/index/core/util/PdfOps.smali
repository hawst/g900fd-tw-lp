.class public final Lorg/icepdf/index/core/util/PdfOps;
.super Ljava/lang/Object;
.source "PdfOps.java"


# static fields
.field public static final BDC_TOKEN:Ljava/lang/String; = "BDC"

.field public static final BI_TOKEN:Ljava/lang/String; = "BI"

.field public static final BMC_TOKEN:Ljava/lang/String; = "BMC"

.field public static final BPC_NAME:Ljava/lang/String; = "BitsPerComponent"

.field public static final BPC_TOKEN:Ljava/lang/String; = "BPC"

.field public static final BT_TOKEN:Ljava/lang/String; = "BT"

.field public static final BX_TOKEN:Ljava/lang/String; = "BX"

.field public static final B_STAR_TOKEN:Ljava/lang/String; = "B*"

.field public static final B_TOKEN:Ljava/lang/String; = "B"

.field public static final CS_NAME:Ljava/lang/String; = "ColorSpace"

.field public static final CS_TOKEN:Ljava/lang/String; = "CS"

.field public static final DOUBLE_QUOTE__TOKEN:Ljava/lang/String; = "\""

.field public static final DP_NAME:Ljava/lang/String; = "DecodeParms"

.field public static final DP_TOKEN:Ljava/lang/String; = "DP"

.field public static final D_NAME:Ljava/lang/String; = "Decode"

.field public static final D_TOKEN:Ljava/lang/String; = "D"

.field public static final Do_TOKEN:Ljava/lang/String; = "Do"

.field public static final EMC_TOKEN:Ljava/lang/String; = "EMC"

.field public static final ET_TOKEN:Ljava/lang/String; = "ET"

.field public static final EX_TOKEN:Ljava/lang/String; = "EX"

.field public static final F_NAME:Ljava/lang/String; = "Filter"

.field public static final F_TOKEN:Ljava/lang/String; = "F"

.field public static final G_TOKEN:Ljava/lang/String; = "G"

.field public static final H_NAME:Ljava/lang/String; = "Height"

.field public static final H_TOKEN:Ljava/lang/String; = "H"

.field public static final ID_TOKEN:Ljava/lang/String; = "ID"

.field public static final IM_NAME:Ljava/lang/String; = "ImageMask"

.field public static final IM_TOKEN:Ljava/lang/String; = "IM"

.field public static final I_NAME:Ljava/lang/String; = "Indexed"

.field public static final I_TOKEN:Ljava/lang/String; = "I"

.field public static final J_TOKEN:Ljava/lang/String; = "J"

.field public static final K_TOKEN:Ljava/lang/String; = "K"

.field public static final LW_TOKEN:Ljava/lang/String; = "LW"

.field public static final MP_TOKEN:Ljava/lang/String; = "MP"

.field public static final M_TOKEN:Ljava/lang/String; = "M"

.field public static final Q_TOKEN:Ljava/lang/String; = "Q"

.field public static final RG_TOKEN:Ljava/lang/String; = "RG"

.field public static final SCN_TOKEN:Ljava/lang/String; = "SCN"

.field public static final SC_TOKEN:Ljava/lang/String; = "SC"

.field public static final SINGLE_QUOTE_TOKEN:Ljava/lang/String; = "\'"

.field public static final S_TOKEN:Ljava/lang/String; = "S"

.field public static final TD_TOKEN:Ljava/lang/String; = "TD"

.field public static final TJ_TOKEN:Ljava/lang/String; = "TJ"

.field public static final TL_TOKEN:Ljava/lang/String; = "TL"

.field public static final T_STAR_TOKEN:Ljava/lang/String; = "T*"

.field public static final Tc_TOKEN:Ljava/lang/String; = "Tc"

.field public static final Td_TOKEN:Ljava/lang/String; = "Td"

.field public static final Tf_TOKEN:Ljava/lang/String; = "Tf"

.field public static final Tj_TOKEN:Ljava/lang/String; = "Tj"

.field public static final Tm_TOKEN:Ljava/lang/String; = "Tm"

.field public static final Tr_TOKEN:Ljava/lang/String; = "Tr"

.field public static final Ts_TOKEN:Ljava/lang/String; = "Ts"

.field public static final Tw_TOKEN:Ljava/lang/String; = "Tw"

.field public static final Tz_TOKEN:Ljava/lang/String; = "Tz"

.field public static final W_NAME:Ljava/lang/String; = "Width"

.field public static final W_STAR_TOKEN:Ljava/lang/String; = "W*"

.field public static final W_TOKEN:Ljava/lang/String; = "W"

.field public static final b_STAR_TOKEN:Ljava/lang/String; = "b*"

.field public static final b_TOKEN:Ljava/lang/String; = "b"

.field public static final c_TOKEN:Ljava/lang/String; = "c"

.field public static final cm_TOKEN:Ljava/lang/String; = "cm"

.field public static final cs_TOKEN:Ljava/lang/String; = "cs"

.field public static final d0_TOKEN:Ljava/lang/String; = "d0"

.field public static final d1_TOKEN:Ljava/lang/String; = "d1"

.field public static final d_TOKEN:Ljava/lang/String; = "d"

.field public static final f_STAR_TOKEN:Ljava/lang/String; = "f*"

.field public static final f_TOKEN:Ljava/lang/String; = "f"

.field public static final g_TOKEN:Ljava/lang/String; = "g"

.field public static final gs_TOKEN:Ljava/lang/String; = "gs"

.field public static final h_TOKEN:Ljava/lang/String; = "h"

.field public static final i_TOKEN:Ljava/lang/String; = "i"

.field public static final j_TOKEN:Ljava/lang/String; = "j"

.field public static final k_TOKEN:Ljava/lang/String; = "k"

.field public static final l_TOKEN:Ljava/lang/String; = "l"

.field public static final m_TOKEN:Ljava/lang/String; = "m"

.field public static final n_TOKEN:Ljava/lang/String; = "n"

.field public static final q_TOKEN:Ljava/lang/String; = "q"

.field public static final re_TOKEN:Ljava/lang/String; = "re"

.field public static final rg_TOKEN:Ljava/lang/String; = "rg"

.field public static final ri_TOKEN:Ljava/lang/String; = "ri"

.field public static final s_TOKEN:Ljava/lang/String; = "s"

.field public static final sc_TOKEN:Ljava/lang/String; = "sc"

.field public static final scn_TOKEN:Ljava/lang/String; = "scn"

.field public static final sh_TOKEN:Ljava/lang/String; = "sh"

.field public static final v_TOKEN:Ljava/lang/String; = "v"

.field public static final w_TOKEN:Ljava/lang/String; = "w"

.field public static final y_TOKEN:Ljava/lang/String; = "y"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
