.class public Lorg/icepdf/index/core/pobjects/graphics/Shapes;
.super Ljava/lang/Object;
.source "Shapes.java"


# instance fields
.field private pageText:Lorg/icepdf/index/core/pobjects/graphics/text/PageText;

.field protected shapes:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    new-instance v0, Lorg/icepdf/index/core/pobjects/graphics/text/PageText;

    invoke-direct {v0}, Lorg/icepdf/index/core/pobjects/graphics/text/PageText;-><init>()V

    iput-object v0, p0, Lorg/icepdf/index/core/pobjects/graphics/Shapes;->pageText:Lorg/icepdf/index/core/pobjects/graphics/text/PageText;

    .line 48
    new-instance v0, Ljava/util/Vector;

    const/16 v1, 0x3e8

    const/16 v2, 0x32

    invoke-direct {v0, v1, v2}, Ljava/util/Vector;-><init>(II)V

    iput-object v0, p0, Lorg/icepdf/index/core/pobjects/graphics/Shapes;->shapes:Ljava/util/Vector;

    return-void
.end method


# virtual methods
.method public add(Ljava/lang/Object;)V
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 100
    instance-of v3, p1, Ljava/util/Vector;

    if-eqz v3, :cond_1

    move-object v1, p1

    .line 101
    check-cast v1, Ljava/util/Vector;

    .line 102
    .local v1, "tmp":Ljava/util/Vector;
    invoke-virtual {v1}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 104
    .local v0, "iterator":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 105
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 106
    .local v2, "tmpImage":Ljava/lang/Object;
    instance-of v3, v2, Landroid/graphics/Bitmap;

    if-eqz v3, :cond_0

    goto :goto_0

    .line 112
    .end local v0    # "iterator":Ljava/util/Iterator;
    .end local v1    # "tmp":Ljava/util/Vector;
    .end local v2    # "tmpImage":Ljava/lang/Object;
    :cond_1
    instance-of v3, p1, Lorg/icepdf/index/core/pobjects/graphics/Shapes;

    if-eqz v3, :cond_2

    move-object v1, p1

    .line 113
    check-cast v1, Lorg/icepdf/index/core/pobjects/graphics/Shapes;

    .line 114
    .local v1, "tmp":Lorg/icepdf/index/core/pobjects/graphics/Shapes;
    iget-object v3, p0, Lorg/icepdf/index/core/pobjects/graphics/Shapes;->pageText:Lorg/icepdf/index/core/pobjects/graphics/text/PageText;

    invoke-virtual {v3}, Lorg/icepdf/index/core/pobjects/graphics/text/PageText;->getPageLines()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v1}, Lorg/icepdf/index/core/pobjects/graphics/Shapes;->getPageText()Lorg/icepdf/index/core/pobjects/graphics/text/PageText;

    move-result-object v4

    invoke-virtual {v4}, Lorg/icepdf/index/core/pobjects/graphics/text/PageText;->getPageLines()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 116
    .end local v1    # "tmp":Lorg/icepdf/index/core/pobjects/graphics/Shapes;
    :cond_2
    iget-object v3, p0, Lorg/icepdf/index/core/pobjects/graphics/Shapes;->shapes:Ljava/util/Vector;

    invoke-virtual {v3, p1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 118
    return-void
.end method

.method public dispose()V
    .locals 3

    .prologue
    .line 74
    iget-object v2, p0, Lorg/icepdf/index/core/pobjects/graphics/Shapes;->shapes:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->elements()Ljava/util/Enumeration;

    move-result-object v0

    .line 75
    .local v0, "shapeContent":Ljava/util/Enumeration;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 76
    invoke-interface {v0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v1

    .line 77
    .local v1, "tmp":Ljava/lang/Object;
    instance-of v2, v1, Lorg/icepdf/index/core/pobjects/graphics/TextSprite;

    if-eqz v2, :cond_0

    .line 78
    check-cast v1, Lorg/icepdf/index/core/pobjects/graphics/TextSprite;

    .end local v1    # "tmp":Ljava/lang/Object;
    invoke-virtual {v1}, Lorg/icepdf/index/core/pobjects/graphics/TextSprite;->dispose()V

    goto :goto_0

    .line 84
    :cond_1
    iget-object v2, p0, Lorg/icepdf/index/core/pobjects/graphics/Shapes;->shapes:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->clear()V

    .line 85
    return-void
.end method

.method public getPageText()Lorg/icepdf/index/core/pobjects/graphics/text/PageText;
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/graphics/Shapes;->pageText:Lorg/icepdf/index/core/pobjects/graphics/text/PageText;

    return-object v0
.end method

.method public getShapesCount()I
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/graphics/Shapes;->shapes:Ljava/util/Vector;

    if-eqz v0, :cond_0

    .line 57
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/graphics/Shapes;->shapes:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    .line 59
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setPageParent(Lorg/icepdf/index/core/pobjects/Page;)V
    .locals 0
    .param p1, "parent"    # Lorg/icepdf/index/core/pobjects/Page;

    .prologue
    .line 64
    return-void
.end method
