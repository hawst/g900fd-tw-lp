.class public interface abstract Lorg/icepdf/index/core/pobjects/fonts/FontFile;
.super Ljava/lang/Object;
.source "FontFile.java"


# static fields
.field public static final LAYOUT_NONE:J


# virtual methods
.method public abstract canDisplayEchar(C)Z
.end method

.method public abstract deriveFont(F)Lorg/icepdf/index/core/pobjects/fonts/FontFile;
.end method

.method public abstract deriveFont(Landroid/graphics/Matrix;)Lorg/icepdf/index/core/pobjects/fonts/FontFile;
.end method

.method public abstract deriveFont(Lorg/icepdf/index/core/pobjects/fonts/Encoding;Lorg/icepdf/index/core/pobjects/fonts/CMap;)Lorg/icepdf/index/core/pobjects/fonts/FontFile;
.end method

.method public abstract deriveFont(Lorg/icepdf/index/core/pobjects/fonts/Encoding;Lorg/icepdf/index/core/pobjects/fonts/CMap;[C)Lorg/icepdf/index/core/pobjects/fonts/FontFile;
.end method

.method public abstract drawEstring(Landroid/graphics/Canvas;Ljava/lang/String;FFJII)V
.end method

.method public abstract echarAdvance(C)Lorg/icepdf/index/java/awt/geom/Point2D;
.end method

.method public abstract getAscent()D
.end method

.method public abstract getDescent()D
.end method

.method public abstract getEstringBounds(Ljava/lang/String;II)Lorg/icepdf/index/core/pobjects/Rectangle2Df;
.end method

.method public abstract getEstringOutline(Ljava/lang/String;FF)Lorg/icepdf/index/java/awt/Shape;
.end method

.method public abstract getFamily()Ljava/lang/String;
.end method

.method public abstract getFormat()Ljava/lang/String;
.end method

.method public abstract getMaxCharBounds()Lorg/icepdf/index/core/pobjects/Rectangle2Df;
.end method

.method public abstract getName()Ljava/lang/String;
.end method

.method public abstract getNumGlyphs()I
.end method

.method public abstract getRights()I
.end method

.method public abstract getSize()F
.end method

.method public abstract getSpaceEchar()C
.end method

.method public abstract getStyle()I
.end method

.method public abstract getToUnicode()Lorg/icepdf/index/core/pobjects/fonts/CMap;
.end method

.method public abstract getTransform()Landroid/graphics/Matrix;
.end method

.method public abstract isHinted()Z
.end method

.method public abstract toUnicode(C)Ljava/lang/String;
.end method
