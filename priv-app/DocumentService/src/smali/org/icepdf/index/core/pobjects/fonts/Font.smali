.class public abstract Lorg/icepdf/index/core/pobjects/fonts/Font;
.super Lorg/icepdf/index/core/pobjects/Dictionary;
.source "Font.java"


# static fields
.field public static final CID_FORMAT:I = 0x2

.field public static final SIMPLE_FORMAT:I = 0x1

.field protected static final TO_UNICODE:[[Ljava/lang/String;


# instance fields
.field protected basefont:Ljava/lang/String;

.field protected firstchar:I

.field protected font:Lorg/icepdf/index/core/pobjects/fonts/FontFile;

.field protected fontDescriptor:Lorg/icepdf/index/core/pobjects/fonts/FontDescriptor;

.field protected isAFMFont:Z

.field protected isFontSubstitution:Z

.field protected isVerticalWriting:Z

.field protected name:Ljava/lang/String;

.field protected subTypeFormat:I

.field protected subtype:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 116
    const/16 v0, 0x10

    new-array v0, v0, [[Ljava/lang/String;

    new-array v1, v7, [Ljava/lang/String;

    const-string/jumbo v2, "GBpc-EUC-UCS2"

    aput-object v2, v1, v4

    const-string/jumbo v2, "GBpc-EUC-H"

    aput-object v2, v1, v5

    const-string/jumbo v2, "GBpc-EUC-V"

    aput-object v2, v1, v6

    aput-object v1, v0, v4

    new-array v1, v7, [Ljava/lang/String;

    const-string/jumbo v2, "GBK-EUC-UCS2"

    aput-object v2, v1, v4

    const-string/jumbo v2, "GBK-EUC-H"

    aput-object v2, v1, v5

    const-string/jumbo v2, "GBK-EUC-V"

    aput-object v2, v1, v6

    aput-object v1, v0, v5

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const-string/jumbo v2, "UniGB-UCS2-H"

    aput-object v2, v1, v4

    const-string/jumbo v2, "GB-EUC-H"

    aput-object v2, v1, v5

    const-string/jumbo v2, "GBT-EUC-H"

    aput-object v2, v1, v6

    const-string/jumbo v2, "GBK2K-H"

    aput-object v2, v1, v7

    const-string/jumbo v2, "GBKp-EUC-H"

    aput-object v2, v1, v8

    aput-object v1, v0, v6

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const-string/jumbo v2, "UniGB-UCS2-V"

    aput-object v2, v1, v4

    const-string/jumbo v2, "GB-EUC-V"

    aput-object v2, v1, v5

    const-string/jumbo v2, "GBT-EUC-V"

    aput-object v2, v1, v6

    const-string/jumbo v2, "GBK2K-V"

    aput-object v2, v1, v7

    const-string/jumbo v2, "GBKp-EUC-V"

    aput-object v2, v1, v8

    aput-object v1, v0, v7

    new-array v1, v7, [Ljava/lang/String;

    const-string/jumbo v2, "B5pc-UCS2"

    aput-object v2, v1, v4

    const-string/jumbo v2, "B5pc-H"

    aput-object v2, v1, v5

    const-string/jumbo v2, "B5pc-V"

    aput-object v2, v1, v6

    aput-object v1, v0, v8

    const/4 v1, 0x5

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/String;

    const-string/jumbo v3, "ETen-B5-UCS2"

    aput-object v3, v2, v4

    const-string/jumbo v3, "ETen-B5-H"

    aput-object v3, v2, v5

    const-string/jumbo v3, "ETen-B5-V"

    aput-object v3, v2, v6

    const-string/jumbo v3, "ETenms-B5-H"

    aput-object v3, v2, v7

    const-string/jumbo v3, "ETenms-B5-V"

    aput-object v3, v2, v8

    aput-object v2, v0, v1

    const/4 v1, 0x6

    new-array v2, v7, [Ljava/lang/String;

    const-string/jumbo v3, "UniCNS-UCS2-H"

    aput-object v3, v2, v4

    const-string/jumbo v3, "HKscs-B5-H"

    aput-object v3, v2, v5

    const-string/jumbo v3, "CNS-EUC-H"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/4 v1, 0x7

    new-array v2, v7, [Ljava/lang/String;

    const-string/jumbo v3, "UniCNS-UCS2-V"

    aput-object v3, v2, v4

    const-string/jumbo v3, "HKscs-B5-V"

    aput-object v3, v2, v5

    const-string/jumbo v3, "CNS-EUC-V"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x8

    new-array v2, v7, [Ljava/lang/String;

    const-string/jumbo v3, "90pv-RKSJ-UCS2"

    aput-object v3, v2, v4

    const-string/jumbo v3, "90pv-RKSJ-H"

    aput-object v3, v2, v5

    const-string/jumbo v3, "83pv-RKSJ-H"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/String;

    const-string/jumbo v3, "90ms-RKSJ-UCS2"

    aput-object v3, v2, v4

    const-string/jumbo v3, "90ms-RKSJ-H"

    aput-object v3, v2, v5

    const-string/jumbo v3, "90ms-RKSJ-V"

    aput-object v3, v2, v6

    const-string/jumbo v3, "90msp-RKSJ-H"

    aput-object v3, v2, v7

    const-string/jumbo v3, "90msp-RKSJ-V"

    aput-object v3, v2, v8

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/String;

    const-string/jumbo v3, "UniJIS-UCS2-H"

    aput-object v3, v2, v4

    const-string/jumbo v3, "Ext-RKSJ-H"

    aput-object v3, v2, v5

    const-string/jumbo v3, "H"

    aput-object v3, v2, v6

    const-string/jumbo v3, "Add-RKSJ-H"

    aput-object v3, v2, v7

    const-string/jumbo v3, "EUC-H"

    aput-object v3, v2, v8

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/String;

    const-string/jumbo v3, "UniJIS-UCS2-V"

    aput-object v3, v2, v4

    const-string/jumbo v3, "Ext-RKSJ-V"

    aput-object v3, v2, v5

    const-string/jumbo v3, "V"

    aput-object v3, v2, v6

    const-string/jumbo v3, "Add-RKSJ-V"

    aput-object v3, v2, v7

    const-string/jumbo v3, "EUC-V"

    aput-object v3, v2, v8

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/String;

    const-string/jumbo v3, "KSCms-UHC-UCS2"

    aput-object v3, v2, v4

    const-string/jumbo v3, "KSCms-UHC-H"

    aput-object v3, v2, v5

    const-string/jumbo v3, "KSCms-UHC-V"

    aput-object v3, v2, v6

    const-string/jumbo v3, "KSCms-UHC-HW-H"

    aput-object v3, v2, v7

    const-string/jumbo v3, "KSCms-UHC-HW-V"

    aput-object v3, v2, v8

    aput-object v2, v0, v1

    const/16 v1, 0xd

    new-array v2, v6, [Ljava/lang/String;

    const-string/jumbo v3, "KSCpc-EUC-UCS2"

    aput-object v3, v2, v4

    const-string/jumbo v3, "KSCpc-EUC-H"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0xe

    new-array v2, v6, [Ljava/lang/String;

    const-string/jumbo v3, "UniKS-UCS2-H"

    aput-object v3, v2, v4

    const-string/jumbo v3, "KSC-EUC-H"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0xf

    new-array v2, v6, [Ljava/lang/String;

    const-string/jumbo v3, "UniKS-UCS2-V"

    aput-object v3, v2, v4

    const-string/jumbo v3, "KSC-EUC-V"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    sput-object v0, Lorg/icepdf/index/core/pobjects/fonts/Font;->TO_UNICODE:[[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lorg/icepdf/index/core/util/Library;Lorg/icepdf/index/core/util/Hashtable;)V
    .locals 4
    .param p1, "library"    # Lorg/icepdf/index/core/util/Library;
    .param p2, "entries"    # Lorg/icepdf/index/core/util/Hashtable;

    .prologue
    const/4 v1, 0x1

    .line 177
    invoke-direct {p0, p1, p2}, Lorg/icepdf/index/core/pobjects/Dictionary;-><init>(Lorg/icepdf/index/core/util/Library;Lorg/icepdf/index/core/util/Hashtable;)V

    .line 90
    iput v1, p0, Lorg/icepdf/index/core/pobjects/fonts/Font;->subTypeFormat:I

    .line 96
    const/16 v2, 0x20

    iput v2, p0, Lorg/icepdf/index/core/pobjects/fonts/Font;->firstchar:I

    .line 180
    const-string/jumbo v2, "Name"

    invoke-virtual {p1, p2, v2}, Lorg/icepdf/index/core/util/Library;->getName(Lorg/icepdf/index/core/util/Hashtable;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lorg/icepdf/index/core/pobjects/fonts/Font;->name:Ljava/lang/String;

    .line 183
    const-string/jumbo v2, "Subtype"

    invoke-virtual {p1, p2, v2}, Lorg/icepdf/index/core/util/Library;->getName(Lorg/icepdf/index/core/util/Hashtable;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lorg/icepdf/index/core/pobjects/fonts/Font;->subtype:Ljava/lang/String;

    .line 186
    iget-object v2, p0, Lorg/icepdf/index/core/pobjects/fonts/Font;->subtype:Ljava/lang/String;

    const-string/jumbo v3, "type0"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lorg/icepdf/index/core/pobjects/fonts/Font;->subtype:Ljava/lang/String;

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v2, v3}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "cid"

    invoke-virtual {v2, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    const/4 v3, -0x1

    if-eq v2, v3, :cond_1

    :cond_0
    const/4 v1, 0x2

    :cond_1
    iput v1, p0, Lorg/icepdf/index/core/pobjects/fonts/Font;->subTypeFormat:I

    .line 192
    const-string/jumbo v1, "Serif"

    iput-object v1, p0, Lorg/icepdf/index/core/pobjects/fonts/Font;->basefont:Ljava/lang/String;

    .line 193
    const-string/jumbo v1, "BaseFont"

    invoke-virtual {p2, v1}, Lorg/icepdf/index/core/util/Hashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 194
    const-string/jumbo v1, "BaseFont"

    invoke-virtual {p2, v1}, Lorg/icepdf/index/core/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 195
    .local v0, "o":Ljava/lang/Object;
    instance-of v1, v0, Lorg/icepdf/index/core/pobjects/Name;

    if-eqz v1, :cond_2

    .line 196
    check-cast v0, Lorg/icepdf/index/core/pobjects/Name;

    .end local v0    # "o":Ljava/lang/Object;
    invoke-virtual {v0}, Lorg/icepdf/index/core/pobjects/Name;->getName()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lorg/icepdf/index/core/pobjects/fonts/Font;->basefont:Ljava/lang/String;

    .line 199
    :cond_2
    return-void
.end method


# virtual methods
.method public getFont()Lorg/icepdf/index/core/pobjects/fonts/FontFile;
    .locals 1

    .prologue
    .line 257
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/fonts/Font;->font:Lorg/icepdf/index/core/pobjects/fonts/FontFile;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 229
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/fonts/Font;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getSubType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 238
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/fonts/Font;->subtype:Ljava/lang/String;

    return-object v0
.end method

.method public getSubTypeFormat()I
    .locals 1

    .prologue
    .line 247
    iget v0, p0, Lorg/icepdf/index/core/pobjects/fonts/Font;->subTypeFormat:I

    return v0
.end method

.method public abstract init()V
.end method

.method public isAFMFont()Z
    .locals 1

    .prologue
    .line 275
    iget-boolean v0, p0, Lorg/icepdf/index/core/pobjects/fonts/Font;->isAFMFont:Z

    return v0
.end method

.method public isFontSubstitution()Z
    .locals 1

    .prologue
    .line 279
    iget-boolean v0, p0, Lorg/icepdf/index/core/pobjects/fonts/Font;->isFontSubstitution:Z

    return v0
.end method

.method public isVerticalWriting()Z
    .locals 1

    .prologue
    .line 266
    iget-boolean v0, p0, Lorg/icepdf/index/core/pobjects/fonts/Font;->isVerticalWriting:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 304
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lorg/icepdf/index/core/pobjects/fonts/Font;->getPObjectReference()Lorg/icepdf/index/core/pobjects/Reference;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " FONT= "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/icepdf/index/core/pobjects/fonts/Font;->basefont:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/icepdf/index/core/pobjects/fonts/Font;->entries:Lorg/icepdf/index/core/util/Hashtable;

    invoke-virtual {v1}, Lorg/icepdf/index/core/util/Hashtable;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
