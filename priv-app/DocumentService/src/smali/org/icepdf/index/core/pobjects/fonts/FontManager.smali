.class public Lorg/icepdf/index/core/pobjects/fonts/FontManager;
.super Ljava/lang/Object;
.source "FontManager.java"


# static fields
.field private static BOLD:I

.field private static BOLD_ITALIC:I

.field private static final CHINESE_SIMPLIFIED_FONT_NAMES:[Ljava/lang/String;

.field private static final CHINESE_TRADITIONAL_FONT_NAMES:[Ljava/lang/String;

.field private static ITALIC:I

.field private static final JAPANESE_FONT_NAMES:[Ljava/lang/String;

.field private static final KOREAN_FONT_NAMES:[Ljava/lang/String;

.field private static PLAIN:I

.field private static SYSTEM_FONT_PATHS:[Ljava/lang/String;

.field private static final TYPE1_FONT_DIFFS:[[Ljava/lang/String;

.field private static fontList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<[",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private static fontManager:Lorg/icepdf/index/core/pobjects/fonts/FontManager;


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 53
    const v0, -0xfffffff

    sput v0, Lorg/icepdf/index/core/pobjects/fonts/FontManager;->PLAIN:I

    .line 54
    const v0, -0xffffff0

    sput v0, Lorg/icepdf/index/core/pobjects/fonts/FontManager;->BOLD:I

    .line 55
    const v0, -0xfffff00

    sput v0, Lorg/icepdf/index/core/pobjects/fonts/FontManager;->ITALIC:I

    .line 56
    const v0, -0xffff000

    sput v0, Lorg/icepdf/index/core/pobjects/fonts/FontManager;->BOLD_ITALIC:I

    .line 59
    const/16 v0, 0x29

    new-array v0, v0, [[Ljava/lang/String;

    new-array v1, v8, [Ljava/lang/String;

    const-string/jumbo v2, "Bookman-Demi"

    aput-object v2, v1, v5

    const-string/jumbo v2, "URWBookmanL-DemiBold"

    aput-object v2, v1, v6

    const-string/jumbo v2, "Arial"

    aput-object v2, v1, v7

    aput-object v1, v0, v5

    new-array v1, v8, [Ljava/lang/String;

    const-string/jumbo v2, "Bookman-DemiItalic"

    aput-object v2, v1, v5

    const-string/jumbo v2, "URWBookmanL-DemiBoldItal"

    aput-object v2, v1, v6

    const-string/jumbo v2, "Arial"

    aput-object v2, v1, v7

    aput-object v1, v0, v6

    new-array v1, v8, [Ljava/lang/String;

    const-string/jumbo v2, "Bookman-Light"

    aput-object v2, v1, v5

    const-string/jumbo v2, "URWBookmanL-Ligh"

    aput-object v2, v1, v6

    const-string/jumbo v2, "Arial"

    aput-object v2, v1, v7

    aput-object v1, v0, v7

    new-array v1, v8, [Ljava/lang/String;

    const-string/jumbo v2, "Bookman-LightItalic"

    aput-object v2, v1, v5

    const-string/jumbo v2, "URWBookmanL-LighItal"

    aput-object v2, v1, v6

    const-string/jumbo v2, "Arial"

    aput-object v2, v1, v7

    aput-object v1, v0, v8

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const-string/jumbo v2, "Courier"

    aput-object v2, v1, v5

    const-string/jumbo v2, "NimbusMonL-Regu"

    aput-object v2, v1, v6

    const-string/jumbo v2, "Nimbus Mono L"

    aput-object v2, v1, v7

    const-string/jumbo v2, "CourierNew"

    aput-object v2, v1, v8

    const-string/jumbo v2, "CourierNewPSMT"

    aput-object v2, v1, v9

    aput-object v1, v0, v9

    const/4 v1, 0x5

    const/4 v2, 0x7

    new-array v2, v2, [Ljava/lang/String;

    const-string/jumbo v3, "Courier-Oblique"

    aput-object v3, v2, v5

    const-string/jumbo v3, "NimbusMonL-ReguObli"

    aput-object v3, v2, v6

    const-string/jumbo v3, "Nimbus Mono L"

    aput-object v3, v2, v7

    const-string/jumbo v3, "Courier,Italic"

    aput-object v3, v2, v8

    const-string/jumbo v3, "CourierNew-Italic"

    aput-object v3, v2, v9

    const/4 v3, 0x5

    const-string/jumbo v4, "CourierNew,Italic"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string/jumbo v4, "CourierNewPS-ItalicMT"

    aput-object v4, v2, v3

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const/4 v2, 0x7

    new-array v2, v2, [Ljava/lang/String;

    const-string/jumbo v3, "Courier-Bold"

    aput-object v3, v2, v5

    const-string/jumbo v3, "NimbusMonL-Bold"

    aput-object v3, v2, v6

    const-string/jumbo v3, "Nimbus Mono L"

    aput-object v3, v2, v7

    const-string/jumbo v3, "Courier,Bold"

    aput-object v3, v2, v8

    const-string/jumbo v3, "CourierNew,Bold"

    aput-object v3, v2, v9

    const/4 v3, 0x5

    const-string/jumbo v4, "CourierNew-Bold"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string/jumbo v4, "CourierNewPS-BoldMT"

    aput-object v4, v2, v3

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const/4 v2, 0x7

    new-array v2, v2, [Ljava/lang/String;

    const-string/jumbo v3, "Courier-BoldOblique"

    aput-object v3, v2, v5

    const-string/jumbo v3, "NimbusMonL-BoldObli"

    aput-object v3, v2, v6

    const-string/jumbo v3, "Nimbus Mono L"

    aput-object v3, v2, v7

    const-string/jumbo v3, "Courier,BoldItalic"

    aput-object v3, v2, v8

    const-string/jumbo v3, "CourierNew-BoldItalic"

    aput-object v3, v2, v9

    const/4 v3, 0x5

    const-string/jumbo v4, "CourierNew,BoldItalic"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string/jumbo v4, "CourierNewPS-BoldItalicMT"

    aput-object v4, v2, v3

    aput-object v2, v0, v1

    const/16 v1, 0x8

    new-array v2, v8, [Ljava/lang/String;

    const-string/jumbo v3, "AvantGarde-Book"

    aput-object v3, v2, v5

    const-string/jumbo v3, "URWGothicL-Book"

    aput-object v3, v2, v6

    const-string/jumbo v3, "Arial"

    aput-object v3, v2, v7

    aput-object v2, v0, v1

    const/16 v1, 0x9

    new-array v2, v8, [Ljava/lang/String;

    const-string/jumbo v3, "AvantGarde-BookOblique"

    aput-object v3, v2, v5

    const-string/jumbo v3, "URWGothicL-BookObli"

    aput-object v3, v2, v6

    const-string/jumbo v3, "Arial"

    aput-object v3, v2, v7

    aput-object v2, v0, v1

    const/16 v1, 0xa

    new-array v2, v8, [Ljava/lang/String;

    const-string/jumbo v3, "AvantGarde-Demi"

    aput-object v3, v2, v5

    const-string/jumbo v3, "URWGothicL-Demi"

    aput-object v3, v2, v6

    const-string/jumbo v3, "Arial"

    aput-object v3, v2, v7

    aput-object v2, v0, v1

    const/16 v1, 0xb

    new-array v2, v8, [Ljava/lang/String;

    const-string/jumbo v3, "AvantGarde-DemiOblique"

    aput-object v3, v2, v5

    const-string/jumbo v3, "URWGothicL-DemiObli"

    aput-object v3, v2, v6

    const-string/jumbo v3, "Arial"

    aput-object v3, v2, v7

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/String;

    const-string/jumbo v3, "Helvetica"

    aput-object v3, v2, v5

    const-string/jumbo v3, "Arial"

    aput-object v3, v2, v6

    const-string/jumbo v3, "ArialMT"

    aput-object v3, v2, v7

    const-string/jumbo v3, "NimbusSanL-Regu"

    aput-object v3, v2, v8

    const-string/jumbo v3, "Nimbus Sans L"

    aput-object v3, v2, v9

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const/16 v2, 0x8

    new-array v2, v2, [Ljava/lang/String;

    const-string/jumbo v3, "Helvetica-Oblique"

    aput-object v3, v2, v5

    const-string/jumbo v3, "NimbusSanL-ReguItal"

    aput-object v3, v2, v6

    const-string/jumbo v3, "Nimbus Sans L"

    aput-object v3, v2, v7

    const-string/jumbo v3, "Helvetica,Italic"

    aput-object v3, v2, v8

    const-string/jumbo v3, "Helvetica-Italic"

    aput-object v3, v2, v9

    const/4 v3, 0x5

    const-string/jumbo v4, "Arial,Italic"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string/jumbo v4, "Arial-Italic"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    const-string/jumbo v4, "Arial-ItalicMT"

    aput-object v4, v2, v3

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const/4 v2, 0x7

    new-array v2, v2, [Ljava/lang/String;

    const-string/jumbo v3, "Helvetica-Bold"

    aput-object v3, v2, v5

    const-string/jumbo v3, "Helvetica,Bold"

    aput-object v3, v2, v6

    const-string/jumbo v3, "Arial,Bold"

    aput-object v3, v2, v7

    const-string/jumbo v3, "Arial-Bold"

    aput-object v3, v2, v8

    const-string/jumbo v3, "Arial-BoldMT"

    aput-object v3, v2, v9

    const/4 v3, 0x5

    const-string/jumbo v4, "NimbusSanL-Bold"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string/jumbo v4, "Nimbus Sans L"

    aput-object v4, v2, v3

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const/16 v2, 0x9

    new-array v2, v2, [Ljava/lang/String;

    const-string/jumbo v3, "Helvetica-BoldOblique"

    aput-object v3, v2, v5

    const-string/jumbo v3, "NimbusSanL-BoldItal"

    aput-object v3, v2, v6

    const-string/jumbo v3, "Helvetica-BlackOblique"

    aput-object v3, v2, v7

    const-string/jumbo v3, "Nimbus Sans L"

    aput-object v3, v2, v8

    const-string/jumbo v3, "Helvetica,BoldItalic"

    aput-object v3, v2, v9

    const/4 v3, 0x5

    const-string/jumbo v4, "Helvetica-BoldItalic"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string/jumbo v4, "Arial,BoldItalic"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    const-string/jumbo v4, "Arial-BoldItalic"

    aput-object v4, v2, v3

    const/16 v3, 0x8

    const-string/jumbo v4, "Arial-BoldItalicMT"

    aput-object v4, v2, v3

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const/4 v2, 0x7

    new-array v2, v2, [Ljava/lang/String;

    const-string/jumbo v3, "Helvetica-Black"

    aput-object v3, v2, v5

    const-string/jumbo v3, "Helvetica,Bold"

    aput-object v3, v2, v6

    const-string/jumbo v3, "Arial,Bold"

    aput-object v3, v2, v7

    const-string/jumbo v3, "Arial-Bold"

    aput-object v3, v2, v8

    const-string/jumbo v3, "Arial-BoldMT"

    aput-object v3, v2, v9

    const/4 v3, 0x5

    const-string/jumbo v4, "NimbusSanL-Bold"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string/jumbo v4, "Nimbus Sans L"

    aput-object v4, v2, v3

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const/16 v2, 0x9

    new-array v2, v2, [Ljava/lang/String;

    const-string/jumbo v3, "Helvetica-BlackOblique"

    aput-object v3, v2, v5

    const-string/jumbo v3, "NimbusSanL-BoldItal"

    aput-object v3, v2, v6

    const-string/jumbo v3, "Helvetica-BlackOblique"

    aput-object v3, v2, v7

    const-string/jumbo v3, "Nimbus Sans L"

    aput-object v3, v2, v8

    const-string/jumbo v3, "Helvetica,BoldItalic"

    aput-object v3, v2, v9

    const/4 v3, 0x5

    const-string/jumbo v4, "Helvetica-BoldItalic"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string/jumbo v4, "Arial,BoldItalic"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    const-string/jumbo v4, "Arial-BoldItalic"

    aput-object v4, v2, v3

    const/16 v3, 0x8

    const-string/jumbo v4, "Arial-BoldItalicMT"

    aput-object v4, v2, v3

    aput-object v2, v0, v1

    const/16 v1, 0x12

    new-array v2, v8, [Ljava/lang/String;

    const-string/jumbo v3, "Helvetica-Narrow"

    aput-object v3, v2, v5

    const-string/jumbo v3, "NimbusSanL-ReguCond"

    aput-object v3, v2, v6

    const-string/jumbo v3, "Nimbus Sans L"

    aput-object v3, v2, v7

    aput-object v2, v0, v1

    const/16 v1, 0x13

    new-array v2, v8, [Ljava/lang/String;

    const-string/jumbo v3, "Helvetica-Narrow-Oblique"

    aput-object v3, v2, v5

    const-string/jumbo v3, "NimbusSanL-ReguCondItal"

    aput-object v3, v2, v6

    const-string/jumbo v3, "Nimbus Sans L"

    aput-object v3, v2, v7

    aput-object v2, v0, v1

    const/16 v1, 0x14

    new-array v2, v8, [Ljava/lang/String;

    const-string/jumbo v3, "Helvetica-Narrow-Bold"

    aput-object v3, v2, v5

    const-string/jumbo v3, "NimbusSanL-BoldCond"

    aput-object v3, v2, v6

    const-string/jumbo v3, "Nimbus Sans L"

    aput-object v3, v2, v7

    aput-object v2, v0, v1

    const/16 v1, 0x15

    new-array v2, v8, [Ljava/lang/String;

    const-string/jumbo v3, "Helvetica-Narrow-BoldOblique"

    aput-object v3, v2, v5

    const-string/jumbo v3, "NimbusSanL-BoldCondItal"

    aput-object v3, v2, v6

    const-string/jumbo v3, "Nimbus Sans L"

    aput-object v3, v2, v7

    aput-object v2, v0, v1

    const/16 v1, 0x16

    new-array v2, v8, [Ljava/lang/String;

    const-string/jumbo v3, "Helvetica-Condensed"

    aput-object v3, v2, v5

    const-string/jumbo v3, "NimbusSanL-ReguCond"

    aput-object v3, v2, v6

    const-string/jumbo v3, "Nimbus Sans L"

    aput-object v3, v2, v7

    aput-object v2, v0, v1

    const/16 v1, 0x17

    new-array v2, v8, [Ljava/lang/String;

    const-string/jumbo v3, "Helvetica-Condensed-Oblique"

    aput-object v3, v2, v5

    const-string/jumbo v3, "NimbusSanL-ReguCondItal"

    aput-object v3, v2, v6

    const-string/jumbo v3, "Nimbus Sans L"

    aput-object v3, v2, v7

    aput-object v2, v0, v1

    const/16 v1, 0x18

    new-array v2, v8, [Ljava/lang/String;

    const-string/jumbo v3, "Helvetica-Condensed-Bold"

    aput-object v3, v2, v5

    const-string/jumbo v3, "NimbusSanL-BoldCond"

    aput-object v3, v2, v6

    const-string/jumbo v3, "Nimbus Sans L"

    aput-object v3, v2, v7

    aput-object v2, v0, v1

    const/16 v1, 0x19

    new-array v2, v8, [Ljava/lang/String;

    const-string/jumbo v3, "Helvetica-Condensed-BoldOblique"

    aput-object v3, v2, v5

    const-string/jumbo v3, "NimbusSanL-BoldCondItal"

    aput-object v3, v2, v6

    const-string/jumbo v3, "Nimbus Sans L"

    aput-object v3, v2, v7

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    new-array v2, v8, [Ljava/lang/String;

    const-string/jumbo v3, "Palatino-Roman"

    aput-object v3, v2, v5

    const-string/jumbo v3, "URWPalladioL-Roma"

    aput-object v3, v2, v6

    const-string/jumbo v3, "Arial"

    aput-object v3, v2, v7

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    new-array v2, v8, [Ljava/lang/String;

    const-string/jumbo v3, "Palatino-Italic"

    aput-object v3, v2, v5

    const-string/jumbo v3, "URWPalladioL-Ital"

    aput-object v3, v2, v6

    const-string/jumbo v3, "Arial"

    aput-object v3, v2, v7

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    new-array v2, v8, [Ljava/lang/String;

    const-string/jumbo v3, "Palatino-Bold"

    aput-object v3, v2, v5

    const-string/jumbo v3, "URWPalladioL-Bold"

    aput-object v3, v2, v6

    const-string/jumbo v3, "Arial"

    aput-object v3, v2, v7

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    new-array v2, v8, [Ljava/lang/String;

    const-string/jumbo v3, "Palatino-BoldItalic"

    aput-object v3, v2, v5

    const-string/jumbo v3, "URWPalladioL-BoldItal"

    aput-object v3, v2, v6

    const-string/jumbo v3, "Arial"

    aput-object v3, v2, v7

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    new-array v2, v8, [Ljava/lang/String;

    const-string/jumbo v3, "NewCenturySchlbk-Roman"

    aput-object v3, v2, v5

    const-string/jumbo v3, "CenturySchL-Roma"

    aput-object v3, v2, v6

    const-string/jumbo v3, "Arial"

    aput-object v3, v2, v7

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    new-array v2, v8, [Ljava/lang/String;

    const-string/jumbo v3, "NewCenturySchlbk-Italic"

    aput-object v3, v2, v5

    const-string/jumbo v3, "CenturySchL-Ital"

    aput-object v3, v2, v6

    const-string/jumbo v3, "Arial"

    aput-object v3, v2, v7

    aput-object v2, v0, v1

    const/16 v1, 0x20

    new-array v2, v8, [Ljava/lang/String;

    const-string/jumbo v3, "NewCenturySchlbk-Bold"

    aput-object v3, v2, v5

    const-string/jumbo v3, "CenturySchL-Bold"

    aput-object v3, v2, v6

    const-string/jumbo v3, "Arial"

    aput-object v3, v2, v7

    aput-object v2, v0, v1

    const/16 v1, 0x21

    new-array v2, v8, [Ljava/lang/String;

    const-string/jumbo v3, "NewCenturySchlbk-BoldItalic"

    aput-object v3, v2, v5

    const-string/jumbo v3, "CenturySchL-BoldItal"

    aput-object v3, v2, v6

    const-string/jumbo v3, "Arial"

    aput-object v3, v2, v7

    aput-object v2, v0, v1

    const/16 v1, 0x22

    const/4 v2, 0x6

    new-array v2, v2, [Ljava/lang/String;

    const-string/jumbo v3, "Times-Roman"

    aput-object v3, v2, v5

    const-string/jumbo v3, "NimbusRomNo9L-Regu"

    aput-object v3, v2, v6

    const-string/jumbo v3, "Nimbus Roman No9 L"

    aput-object v3, v2, v7

    const-string/jumbo v3, "TimesNewRoman"

    aput-object v3, v2, v8

    const-string/jumbo v3, "TimesNewRomanPS"

    aput-object v3, v2, v9

    const/4 v3, 0x5

    const-string/jumbo v4, "TimesNewRomanPSMT"

    aput-object v4, v2, v3

    aput-object v2, v0, v1

    const/16 v1, 0x23

    const/4 v2, 0x7

    new-array v2, v2, [Ljava/lang/String;

    const-string/jumbo v3, "Times-Italic"

    aput-object v3, v2, v5

    const-string/jumbo v3, "NimbusRomNo9L-ReguItal"

    aput-object v3, v2, v6

    const-string/jumbo v3, "Nimbus Roman No9 L"

    aput-object v3, v2, v7

    const-string/jumbo v3, "TimesNewRoman,Italic"

    aput-object v3, v2, v8

    const-string/jumbo v3, "TimesNewRoman-Italic"

    aput-object v3, v2, v9

    const/4 v3, 0x5

    const-string/jumbo v4, "TimesNewRomanPS-Italic"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string/jumbo v4, "TimesNewRomanPS-ItalicMT"

    aput-object v4, v2, v3

    aput-object v2, v0, v1

    const/16 v1, 0x24

    const/4 v2, 0x7

    new-array v2, v2, [Ljava/lang/String;

    const-string/jumbo v3, "Times-Bold"

    aput-object v3, v2, v5

    const-string/jumbo v3, "NimbusRomNo9L-Medi"

    aput-object v3, v2, v6

    const-string/jumbo v3, "Nimbus Roman No9 L"

    aput-object v3, v2, v7

    const-string/jumbo v3, "TimesNewRoman,Bold"

    aput-object v3, v2, v8

    const-string/jumbo v3, "TimesNewRoman-Bold"

    aput-object v3, v2, v9

    const/4 v3, 0x5

    const-string/jumbo v4, "TimesNewRomanPS-Bold"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string/jumbo v4, "TimesNewRomanPS-BoldMT"

    aput-object v4, v2, v3

    aput-object v2, v0, v1

    const/16 v1, 0x25

    const/4 v2, 0x7

    new-array v2, v2, [Ljava/lang/String;

    const-string/jumbo v3, "Times-BoldItalic"

    aput-object v3, v2, v5

    const-string/jumbo v3, "NimbusRomNo9L-MediItal"

    aput-object v3, v2, v6

    const-string/jumbo v3, "Nimbus Roman No9 L"

    aput-object v3, v2, v7

    const-string/jumbo v3, "TimesNewRoman,BoldItalic"

    aput-object v3, v2, v8

    const-string/jumbo v3, "TimesNewRoman-BoldItalic"

    aput-object v3, v2, v9

    const/4 v3, 0x5

    const-string/jumbo v4, "TimesNewRomanPS-BoldItalic"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string/jumbo v4, "TimesNewRomanPS-BoldItalicMT"

    aput-object v4, v2, v3

    aput-object v2, v0, v1

    const/16 v1, 0x26

    new-array v2, v8, [Ljava/lang/String;

    const-string/jumbo v3, "Symbol"

    aput-object v3, v2, v5

    const-string/jumbo v3, "StandardSymL"

    aput-object v3, v2, v6

    const-string/jumbo v3, "Standard Symbols L"

    aput-object v3, v2, v7

    aput-object v2, v0, v1

    const/16 v1, 0x27

    new-array v2, v8, [Ljava/lang/String;

    const-string/jumbo v3, "ZapfChancery-MediumItalic"

    aput-object v3, v2, v5

    const-string/jumbo v3, "URWChanceryL-MediItal"

    aput-object v3, v2, v6

    const-string/jumbo v3, "Arial"

    aput-object v3, v2, v7

    aput-object v2, v0, v1

    const/16 v1, 0x28

    new-array v2, v8, [Ljava/lang/String;

    const-string/jumbo v3, "ZapfDingbats"

    aput-object v3, v2, v5

    const-string/jumbo v3, "Dingbats"

    aput-object v3, v2, v6

    const-string/jumbo v3, "Zapf-Dingbats"

    aput-object v3, v2, v7

    aput-object v2, v0, v1

    sput-object v0, Lorg/icepdf/index/core/pobjects/fonts/FontManager;->TYPE1_FONT_DIFFS:[[Ljava/lang/String;

    .line 105
    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "Arial Unicode MS"

    aput-object v1, v0, v5

    const-string/jumbo v1, "PMingLiU"

    aput-object v1, v0, v6

    const-string/jumbo v1, "MingLiU"

    aput-object v1, v0, v7

    const-string/jumbo v1, "MS PMincho"

    aput-object v1, v0, v8

    const-string/jumbo v1, "MS Mincho"

    aput-object v1, v0, v9

    const/4 v1, 0x5

    const-string/jumbo v2, "Kochi Mincho"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string/jumbo v2, "Hiragino Mincho Pro"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string/jumbo v2, "KozMinPro Regular Acro"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string/jumbo v2, "HeiseiMin W3 Acro"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string/jumbo v2, "Adobe Ming Std Acro"

    aput-object v2, v0, v1

    sput-object v0, Lorg/icepdf/index/core/pobjects/fonts/FontManager;->JAPANESE_FONT_NAMES:[Ljava/lang/String;

    .line 111
    const/16 v0, 0x9

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "Arial Unicode MS"

    aput-object v1, v0, v5

    const-string/jumbo v1, "PMingLiU"

    aput-object v1, v0, v6

    const-string/jumbo v1, "MingLiU"

    aput-object v1, v0, v7

    const-string/jumbo v1, "SimSun"

    aput-object v1, v0, v8

    const-string/jumbo v1, "NSimSun"

    aput-object v1, v0, v9

    const/4 v1, 0x5

    const-string/jumbo v2, "Kochi Mincho"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string/jumbo v2, "STFangsong"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string/jumbo v2, "STSong Light Acro"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string/jumbo v2, "Adobe Song Std Acro"

    aput-object v2, v0, v1

    sput-object v0, Lorg/icepdf/index/core/pobjects/fonts/FontManager;->CHINESE_SIMPLIFIED_FONT_NAMES:[Ljava/lang/String;

    .line 117
    const/16 v0, 0x9

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "Arial Unicode MS"

    aput-object v1, v0, v5

    const-string/jumbo v1, "PMingLiU"

    aput-object v1, v0, v6

    const-string/jumbo v1, "MingLiU"

    aput-object v1, v0, v7

    const-string/jumbo v1, "SimSun"

    aput-object v1, v0, v8

    const-string/jumbo v1, "NSimSun"

    aput-object v1, v0, v9

    const/4 v1, 0x5

    const-string/jumbo v2, "Kochi Mincho"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string/jumbo v2, "BiauKai"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string/jumbo v2, "MSungStd Light Acro"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string/jumbo v2, "Adobe Song Std Acro"

    aput-object v2, v0, v1

    sput-object v0, Lorg/icepdf/index/core/pobjects/fonts/FontManager;->CHINESE_TRADITIONAL_FONT_NAMES:[Ljava/lang/String;

    .line 123
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "Arial Unicode MS"

    aput-object v1, v0, v5

    const-string/jumbo v1, "Gulim"

    aput-object v1, v0, v6

    const-string/jumbo v1, "Batang"

    aput-object v1, v0, v7

    const-string/jumbo v1, "BatangChe"

    aput-object v1, v0, v8

    const-string/jumbo v1, "HYSMyeongJoStd Medium Acro"

    aput-object v1, v0, v9

    const/4 v1, 0x5

    const-string/jumbo v2, "Adobe Myungjo Std Acro"

    aput-object v2, v0, v1

    sput-object v0, Lorg/icepdf/index/core/pobjects/fonts/FontManager;->KOREAN_FONT_NAMES:[Ljava/lang/String;

    .line 131
    const/16 v0, 0x52

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "c:\\windows\\fonts\\"

    aput-object v1, v0, v5

    const-string/jumbo v1, "d:\\windows\\fonts\\"

    aput-object v1, v0, v6

    const-string/jumbo v1, "e:\\windows\\fonts\\"

    aput-object v1, v0, v7

    const-string/jumbo v1, "f:\\windows\\fonts\\"

    aput-object v1, v0, v8

    const-string/jumbo v1, "c:\\winnt\\Fonts\\"

    aput-object v1, v0, v9

    const/4 v1, 0x5

    const-string/jumbo v2, "d:\\winnt\\Fonts\\"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string/jumbo v2, "c:\\cygwin\\usr\\share\\ghostscript\\fonts\\"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string/jumbo v2, "d:\\cygwin\\usr\\share\\ghostscript\\fonts\\"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string/jumbo v2, "/Network/Library/Fonts/"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string/jumbo v2, "/System/Library/Fonts/"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string/jumbo v2, "/System Folder/Fonts"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string/jumbo v2, "/usr/local/share/ghostscript/"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string/jumbo v2, "/Applications/GarageBand.app/Contents/Resources/"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string/jumbo v2, "/Applications/NeoOffice.app/Contents/share/fonts/truetype/"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string/jumbo v2, "/Library/Dictionaries/Shogakukan Daijisen.dictionary/Contents/"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string/jumbo v2, "/Library/Dictionaries/Shogakukan Progressive English-Japanese Japanese-English Dictionary.dictionary/Contents/"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string/jumbo v2, "/Library/Dictionaries/Shogakukan Ruigo Reikai Jiten.dictionary/Contents/"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string/jumbo v2, "/Library/Fonts/"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string/jumbo v2, "/Volumes/Untitled/WINDOWS/Fonts/"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string/jumbo v2, "/usr/share/enscript/"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string/jumbo v2, "/usr/share/groff/1.19.2/font/devps/generate/"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string/jumbo v2, "/usr/X11/lib/X11/fonts/Type1/"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string/jumbo v2, "/usr/X11/lib/X11/fonts/TrueType/"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string/jumbo v2, "/usr/X11/lib/X11/fonts/"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string/jumbo v2, "/etc/fonts/"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string/jumbo v2, "/system/etc/fonts/"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string/jumbo v2, "/usr/lib/X11/fonts"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string/jumbo v2, "/usr/share/a2ps/afm/"

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string/jumbo v2, "/usr/share/enscript/afm/"

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    const-string/jumbo v2, "/usr/share/fonts/local/"

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    const-string/jumbo v2, "/usr/share/fonts/truetype/"

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    const-string/jumbo v2, "/usr/share/fonts/truetype/freefont/"

    aput-object v2, v0, v1

    const/16 v1, 0x20

    const-string/jumbo v2, "/usr/share/fonts/truetype/msttcorefonts/"

    aput-object v2, v0, v1

    const/16 v1, 0x21

    const-string/jumbo v2, "/usr/share/fonts/Type1/"

    aput-object v2, v0, v1

    const/16 v1, 0x22

    const-string/jumbo v2, "/usr/share/fonts/type1/gsfonts/"

    aput-object v2, v0, v1

    const/16 v1, 0x23

    const-string/jumbo v2, "/usr/share/fonts/X11/Type1/"

    aput-object v2, v0, v1

    const/16 v1, 0x24

    const-string/jumbo v2, "/usr/share/ghostscript/fonts/"

    aput-object v2, v0, v1

    const/16 v1, 0x25

    const-string/jumbo v2, "/usr/share/groff/1.18.1/font/devps/"

    aput-object v2, v0, v1

    const/16 v1, 0x26

    const-string/jumbo v2, "/usr/share/groff/1.18.1/font/devps/generate/"

    aput-object v2, v0, v1

    const/16 v1, 0x27

    const-string/jumbo v2, "/usr/share/libwmf/fonts/"

    aput-object v2, v0, v1

    const/16 v1, 0x28

    const-string/jumbo v2, "/usr/share/ogonkify/afm/"

    aput-object v2, v0, v1

    const/16 v1, 0x29

    const-string/jumbo v2, "/usr/X11R6/lib/X11/fonts/"

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    const-string/jumbo v2, "/var/lib/defoma/gs.d/dirs/fonts/"

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    const-string/jumbo v2, "/usr/openwin/lib/locale/ar/X11/fonts/TrueType/"

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    const-string/jumbo v2, "/usr/openwin/lib/locale/euro_fonts/X11/fonts/TrueType/"

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    const-string/jumbo v2, "/usr/openwin/lib/locale/hi_IN.UTF-8/X11/fonts/TrueType/"

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    const-string/jumbo v2, "/usr/openwin/lib/locale/iso_8859_13/X11/fonts/TrueType/"

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    const-string/jumbo v2, "/usr/openwin/lib/locale/iso_8859_15/X11/fonts/TrueType/"

    aput-object v2, v0, v1

    const/16 v1, 0x30

    const-string/jumbo v2, "/usr/openwin/lib/locale/iso_8859_2/X11/fonts/TrueType/"

    aput-object v2, v0, v1

    const/16 v1, 0x31

    const-string/jumbo v2, "/usr/openwin/lib/locale/iso_8859_2/X11/fonts/Type1/afm/"

    aput-object v2, v0, v1

    const/16 v1, 0x32

    const-string/jumbo v2, "/usr/openwin/lib/locale/iso_8859_4/X11/fonts/Type1/afm/"

    aput-object v2, v0, v1

    const/16 v1, 0x33

    const-string/jumbo v2, "/usr/openwin/lib/locale/iso_8859_5/X11/fonts/TrueType/"

    aput-object v2, v0, v1

    const/16 v1, 0x34

    const-string/jumbo v2, "/usr/openwin/lib/locale/iso_8859_5/X11/fonts/Type1/afm/"

    aput-object v2, v0, v1

    const/16 v1, 0x35

    const-string/jumbo v2, "/usr/openwin/lib/locale/iso_8859_7/X11/fonts/TrueType/"

    aput-object v2, v0, v1

    const/16 v1, 0x36

    const-string/jumbo v2, "/usr/openwin/lib/locale/iso_8859_7/X11/fonts/Type1/afm/"

    aput-object v2, v0, v1

    const/16 v1, 0x37

    const-string/jumbo v2, "/usr/openwin/lib/locale/iso_8859_8/X11/fonts/TrueType/"

    aput-object v2, v0, v1

    const/16 v1, 0x38

    const-string/jumbo v2, "/usr/openwin/lib/locale/iso_8859_8/X11/fonts/Type1/"

    aput-object v2, v0, v1

    const/16 v1, 0x39

    const-string/jumbo v2, "/usr/openwin/lib/locale/iso_8859_8/X11/fonts/Type1/afm/"

    aput-object v2, v0, v1

    const/16 v1, 0x3a

    const-string/jumbo v2, "/usr/openwin/lib/locale/iso_8859_9/X11/fonts/TrueType/"

    aput-object v2, v0, v1

    const/16 v1, 0x3b

    const-string/jumbo v2, "/usr/openwin/lib/locale/iso_8859_9/X11/fonts/Type1/afm/"

    aput-object v2, v0, v1

    const/16 v1, 0x3c

    const-string/jumbo v2, "/usr/openwin/lib/locale/ja//X11/fonts/TrueType/"

    aput-object v2, v0, v1

    const/16 v1, 0x3d

    const-string/jumbo v2, "/usr/openwin/lib/locale/K0I8-R/X11/fonts/TrueType/"

    aput-object v2, v0, v1

    const/16 v1, 0x3e

    const-string/jumbo v2, "/usr/openwin/lib/locale/ru.ansi-1251/X11/fonts/TrueType/"

    aput-object v2, v0, v1

    const/16 v1, 0x3f

    const-string/jumbo v2, "/usr/openwin/lib/locale/th_TH/X11/fonts/TrueType/"

    aput-object v2, v0, v1

    const/16 v1, 0x40

    const-string/jumbo v2, "/usr/openwin/lib/locale/zh.GBK/X11/fonts/TrueType/"

    aput-object v2, v0, v1

    const/16 v1, 0x41

    const-string/jumbo v2, "/usr/openwin/lib/locale/zh/X11/fonts/TrueType/"

    aput-object v2, v0, v1

    const/16 v1, 0x42

    const-string/jumbo v2, "/usr/openwin/lib/locale/zh_CN.GB18030/X11/fonts/TrueType/"

    aput-object v2, v0, v1

    const/16 v1, 0x43

    const-string/jumbo v2, "/usr/openwin/lib/locale/zh_TW.BIG5/X11/fonts/TrueType/"

    aput-object v2, v0, v1

    const/16 v1, 0x44

    const-string/jumbo v2, "/usr/openwin/lib/locale/zh_TW/X11/fonts/TrueType/"

    aput-object v2, v0, v1

    const/16 v1, 0x45

    const-string/jumbo v2, "/usr/openwin/lib/X11/fonts/"

    aput-object v2, v0, v1

    const/16 v1, 0x46

    const-string/jumbo v2, "/usr/openwin/lib/X11/fonts/F3/afm/"

    aput-object v2, v0, v1

    const/16 v1, 0x47

    const-string/jumbo v2, "/usr/openwin/lib/X11/fonts/misc/"

    aput-object v2, v0, v1

    const/16 v1, 0x48

    const-string/jumbo v2, "/usr/openwin/lib/X11/fonts/TrueType/"

    aput-object v2, v0, v1

    const/16 v1, 0x49

    const-string/jumbo v2, "/usr/openwin/lib/X11/fonts/Type1/"

    aput-object v2, v0, v1

    const/16 v1, 0x4a

    const-string/jumbo v2, "/usr/openwin/lib/X11/fonts/Type1/afm/"

    aput-object v2, v0, v1

    const/16 v1, 0x4b

    const-string/jumbo v2, "/usr/openwin/lib/X11/fonts/Type1/outline/"

    aput-object v2, v0, v1

    const/16 v1, 0x4c

    const-string/jumbo v2, "/usr/openwin/lib/X11/fonts/Type1/sun/"

    aput-object v2, v0, v1

    const/16 v1, 0x4d

    const-string/jumbo v2, "/usr/openwin/lib/X11/fonts/Type1/sun/afm/"

    aput-object v2, v0, v1

    const/16 v1, 0x4e

    const-string/jumbo v2, "/usr/sfw/share/a2ps/afm/"

    aput-object v2, v0, v1

    const/16 v1, 0x4f

    const-string/jumbo v2, "/usr/sfw/share/ghostscript/fonts/"

    aput-object v2, v0, v1

    const/16 v1, 0x50

    const-string/jumbo v2, "/usr/sfw/share/ghostscript/fonts/"

    aput-object v2, v0, v1

    const/16 v1, 0x51

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "java.home"

    invoke-static {v3}, Lorg/icepdf/index/core/util/Defs;->sysProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "/lib/fonts"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    sput-object v0, Lorg/icepdf/index/core/pobjects/fonts/FontManager;->SYSTEM_FONT_PATHS:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private buildFont(Ljava/lang/String;)Lorg/icepdf/index/core/pobjects/fonts/FontFile;
    .locals 4
    .param p1, "fontPath"    # Ljava/lang/String;

    .prologue
    .line 700
    const/4 v1, 0x0

    .line 702
    .local v1, "font":Lorg/icepdf/index/core/pobjects/fonts/FontFile;
    :try_start_0
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 703
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->canRead()Z

    move-result v3

    if-nez v3, :cond_0

    .line 704
    const/4 v3, 0x0

    .line 727
    .end local v0    # "file":Ljava/io/File;
    :goto_0
    return-object v3

    .line 707
    .restart local v0    # "file":Ljava/io/File;
    :cond_0
    invoke-static {}, Lorg/icepdf/index/core/pobjects/fonts/FontFactory;->getInstance()Lorg/icepdf/index/core/pobjects/fonts/FontFactory;

    move-result-object v2

    .line 709
    .local v2, "fontFactory":Lorg/icepdf/index/core/pobjects/fonts/FontFactory;
    const-string/jumbo v3, ".ttf"

    invoke-virtual {p1, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    const-string/jumbo v3, ".TTF"

    invoke-virtual {p1, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    const-string/jumbo v3, ".dfont"

    invoke-virtual {p1, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    const-string/jumbo v3, ".DFONT"

    invoke-virtual {p1, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    const-string/jumbo v3, ".ttc"

    invoke-virtual {p1, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    const-string/jumbo v3, ".TTC"

    invoke-virtual {p1, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 712
    :cond_1
    const/4 v3, 0x0

    invoke-virtual {v2, v0, v3}, Lorg/icepdf/index/core/pobjects/fonts/FontFactory;->createFontFile(Ljava/io/File;I)Lorg/icepdf/index/core/pobjects/fonts/FontFile;

    move-result-object v1

    .end local v0    # "file":Ljava/io/File;
    .end local v2    # "fontFactory":Lorg/icepdf/index/core/pobjects/fonts/FontFactory;
    :cond_2
    :goto_1
    move-object v3, v1

    .line 727
    goto :goto_0

    .line 715
    .restart local v0    # "file":Ljava/io/File;
    .restart local v2    # "fontFactory":Lorg/icepdf/index/core/pobjects/fonts/FontFactory;
    :cond_3
    const-string/jumbo v3, ".pfa"

    invoke-virtual {p1, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_4

    const-string/jumbo v3, ".PFA"

    invoke-virtual {p1, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_4

    const-string/jumbo v3, ".pfb"

    invoke-virtual {p1, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_4

    const-string/jumbo v3, ".PFB"

    invoke-virtual {p1, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 717
    :cond_4
    const/4 v3, 0x1

    invoke-virtual {v2, v0, v3}, Lorg/icepdf/index/core/pobjects/fonts/FontFactory;->createFontFile(Ljava/io/File;I)Lorg/icepdf/index/core/pobjects/fonts/FontFile;

    move-result-object v1

    goto :goto_1

    .line 720
    :cond_5
    const-string/jumbo v3, ".otf"

    invoke-virtual {p1, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_6

    const-string/jumbo v3, ".OTF"

    invoke-virtual {p1, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_6

    const-string/jumbo v3, ".otc"

    invoke-virtual {p1, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_6

    const-string/jumbo v3, ".OTC"

    invoke-virtual {p1, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 722
    :cond_6
    const/4 v3, 0x5

    invoke-virtual {v2, v0, v3}, Lorg/icepdf/index/core/pobjects/fonts/FontFactory;->createFontFile(Ljava/io/File;I)Lorg/icepdf/index/core/pobjects/fonts/FontFile;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    goto :goto_1

    .line 724
    .end local v0    # "file":Ljava/io/File;
    .end local v2    # "fontFactory":Lorg/icepdf/index/core/pobjects/fonts/FontFactory;
    :catch_0
    move-exception v3

    goto :goto_1
.end method

.method private findFont(Ljava/lang/String;I)Lorg/icepdf/index/core/pobjects/fonts/FontFile;
    .locals 11
    .param p1, "fontName"    # Ljava/lang/String;
    .param p2, "flags"    # I

    .prologue
    .line 619
    const/4 v3, 0x0

    .line 625
    .local v3, "font":Lorg/icepdf/index/core/pobjects/fonts/FontFile;
    invoke-static {p1}, Lorg/icepdf/index/core/pobjects/fonts/FontManager;->guessFontStyle(Ljava/lang/String;)I

    move-result v1

    .line 626
    .local v1, "decorations":I
    invoke-static {p1}, Lorg/icepdf/index/core/pobjects/fonts/FontManager;->normalizeString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 633
    .local v7, "name":Ljava/lang/String;
    sget-object v9, Lorg/icepdf/index/core/pobjects/fonts/FontManager;->fontList:Ljava/util/ArrayList;

    if-eqz v9, :cond_2

    .line 634
    sget-object v9, Lorg/icepdf/index/core/pobjects/fonts/FontManager;->fontList:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v9

    add-int/lit8 v6, v9, -0x1

    .local v6, "i":I
    :goto_0
    if-ltz v6, :cond_2

    .line 635
    sget-object v9, Lorg/icepdf/index/core/pobjects/fonts/FontManager;->fontList:Ljava/util/ArrayList;

    invoke-virtual {v9, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Ljava/lang/Object;

    .line 636
    .local v4, "fontData":[Ljava/lang/Object;
    const/4 v9, 0x0

    aget-object v0, v4, v9

    check-cast v0, Ljava/lang/String;

    .line 637
    .local v0, "baseName":Ljava/lang/String;
    const/4 v9, 0x1

    aget-object v2, v4, v9

    check-cast v2, Ljava/lang/String;

    .line 641
    .local v2, "familyName":Ljava/lang/String;
    invoke-virtual {v7, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v9

    if-gez v9, :cond_0

    sget-object v9, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p1, v9}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v9

    if-ltz v9, :cond_9

    .line 643
    :cond_0
    const/4 v9, 0x2

    aget-object v9, v4, v9

    check-cast v9, Ljava/lang/Integer;

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v8

    .line 644
    .local v8, "style":I
    const/4 v5, 0x0

    .line 647
    .local v5, "found":Z
    const-string/jumbo v9, "opensymbol"

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_1

    const-string/jumbo v9, "starsymbol"

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_1

    const-string/jumbo v9, "arial-black"

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_1

    const-string/jumbo v9, "arial-blackitalic"

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_1

    const-string/jumbo v9, "timesnewromanps"

    invoke-virtual {v0, v9}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v9

    if-ltz v9, :cond_3

    .line 676
    :cond_1
    :goto_1
    if-eqz v5, :cond_9

    .line 680
    const/4 v9, 0x3

    aget-object v9, v4, v9

    check-cast v9, Ljava/lang/String;

    invoke-direct {p0, v9}, Lorg/icepdf/index/core/pobjects/fonts/FontManager;->buildFont(Ljava/lang/String;)Lorg/icepdf/index/core/pobjects/fonts/FontFile;

    move-result-object v3

    .line 682
    if-eqz v3, :cond_9

    .line 689
    .end local v0    # "baseName":Ljava/lang/String;
    .end local v2    # "familyName":Ljava/lang/String;
    .end local v4    # "fontData":[Ljava/lang/Object;
    .end local v5    # "found":Z
    .end local v6    # "i":I
    .end local v8    # "style":I
    :cond_2
    return-object v3

    .line 655
    .restart local v0    # "baseName":Ljava/lang/String;
    .restart local v2    # "familyName":Ljava/lang/String;
    .restart local v4    # "fontData":[Ljava/lang/Object;
    .restart local v5    # "found":Z
    .restart local v6    # "i":I
    .restart local v8    # "style":I
    :cond_3
    sget v9, Lorg/icepdf/index/core/pobjects/fonts/FontManager;->BOLD_ITALIC:I

    and-int/2addr v9, v1

    sget v10, Lorg/icepdf/index/core/pobjects/fonts/FontManager;->BOLD_ITALIC:I

    if-ne v9, v10, :cond_4

    sget v9, Lorg/icepdf/index/core/pobjects/fonts/FontManager;->BOLD_ITALIC:I

    and-int/2addr v9, v8

    sget v10, Lorg/icepdf/index/core/pobjects/fonts/FontManager;->BOLD_ITALIC:I

    if-ne v9, v10, :cond_4

    .line 657
    const/4 v5, 0x1

    goto :goto_1

    .line 658
    :cond_4
    sget v9, Lorg/icepdf/index/core/pobjects/fonts/FontManager;->BOLD:I

    and-int/2addr v9, v1

    sget v10, Lorg/icepdf/index/core/pobjects/fonts/FontManager;->BOLD:I

    if-ne v9, v10, :cond_5

    sget v9, Lorg/icepdf/index/core/pobjects/fonts/FontManager;->BOLD:I

    and-int/2addr v9, v8

    sget v10, Lorg/icepdf/index/core/pobjects/fonts/FontManager;->BOLD:I

    if-ne v9, v10, :cond_5

    .line 660
    const/4 v5, 0x1

    goto :goto_1

    .line 661
    :cond_5
    sget v9, Lorg/icepdf/index/core/pobjects/fonts/FontManager;->ITALIC:I

    and-int/2addr v9, v1

    sget v10, Lorg/icepdf/index/core/pobjects/fonts/FontManager;->ITALIC:I

    if-ne v9, v10, :cond_6

    sget v9, Lorg/icepdf/index/core/pobjects/fonts/FontManager;->ITALIC:I

    and-int/2addr v9, v8

    sget v10, Lorg/icepdf/index/core/pobjects/fonts/FontManager;->ITALIC:I

    if-ne v9, v10, :cond_6

    .line 663
    const/4 v5, 0x1

    goto :goto_1

    .line 664
    :cond_6
    sget v9, Lorg/icepdf/index/core/pobjects/fonts/FontManager;->PLAIN:I

    and-int/2addr v9, v1

    sget v10, Lorg/icepdf/index/core/pobjects/fonts/FontManager;->PLAIN:I

    if-ne v9, v10, :cond_7

    sget v9, Lorg/icepdf/index/core/pobjects/fonts/FontManager;->PLAIN:I

    and-int/2addr v9, v8

    sget v10, Lorg/icepdf/index/core/pobjects/fonts/FontManager;->PLAIN:I

    if-ne v9, v10, :cond_7

    .line 666
    const/4 v5, 0x1

    goto :goto_1

    .line 670
    :cond_7
    const-string/jumbo v9, "wingdings"

    invoke-virtual {v0, v9}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v9

    if-gez v9, :cond_8

    const-string/jumbo v9, "zapfdingbats"

    invoke-virtual {v0, v9}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v9

    if-gez v9, :cond_8

    const-string/jumbo v9, "symbol"

    invoke-virtual {v0, v9}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v9

    if-ltz v9, :cond_1

    .line 673
    :cond_8
    const/4 v5, 0x1

    goto :goto_1

    .line 634
    .end local v5    # "found":Z
    .end local v8    # "style":I
    :cond_9
    add-int/lit8 v6, v6, -0x1

    goto/16 :goto_0
.end method

.method private getAsianInstance(Ljava/lang/String;[Ljava/lang/String;I)Lorg/icepdf/index/core/pobjects/fonts/FontFile;
    .locals 4
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "list"    # [Ljava/lang/String;
    .param p3, "flags"    # I

    .prologue
    .line 515
    sget-object v3, Lorg/icepdf/index/core/pobjects/fonts/FontManager;->fontList:Ljava/util/ArrayList;

    if-nez v3, :cond_0

    .line 516
    const/4 v3, 0x0

    invoke-virtual {p0, v3}, Lorg/icepdf/index/core/pobjects/fonts/FontManager;->readSystemFonts([Ljava/lang/String;)V

    .line 519
    :cond_0
    const/4 v0, 0x0

    .line 520
    .local v0, "font":Lorg/icepdf/index/core/pobjects/fonts/FontFile;
    if-eqz p2, :cond_4

    .line 522
    array-length v3, p2

    add-int/lit8 v2, v3, -0x1

    .local v2, "i":I
    :goto_0
    if-ltz v2, :cond_2

    .line 524
    invoke-direct {p0, p1, p3}, Lorg/icepdf/index/core/pobjects/fonts/FontManager;->findFont(Ljava/lang/String;I)Lorg/icepdf/index/core/pobjects/fonts/FontFile;

    move-result-object v0

    .line 525
    if-eqz v0, :cond_1

    move-object v1, v0

    .line 547
    .end local v0    # "font":Lorg/icepdf/index/core/pobjects/fonts/FontFile;
    .end local v2    # "i":I
    .local v1, "font":Lorg/icepdf/index/core/pobjects/fonts/FontFile;
    :goto_1
    return-object v1

    .line 522
    .end local v1    # "font":Lorg/icepdf/index/core/pobjects/fonts/FontFile;
    .restart local v0    # "font":Lorg/icepdf/index/core/pobjects/fonts/FontFile;
    .restart local v2    # "i":I
    :cond_1
    add-int/lit8 v2, v2, -0x1

    goto :goto_0

    .line 535
    :cond_2
    array-length v3, p2

    add-int/lit8 v2, v3, -0x1

    :goto_2
    if-ltz v2, :cond_4

    .line 537
    aget-object v3, p2, v2

    invoke-direct {p0, v3, p3}, Lorg/icepdf/index/core/pobjects/fonts/FontManager;->findFont(Ljava/lang/String;I)Lorg/icepdf/index/core/pobjects/fonts/FontFile;

    move-result-object v0

    .line 538
    if-eqz v0, :cond_3

    move-object v1, v0

    .line 542
    .end local v0    # "font":Lorg/icepdf/index/core/pobjects/fonts/FontFile;
    .restart local v1    # "font":Lorg/icepdf/index/core/pobjects/fonts/FontFile;
    goto :goto_1

    .line 535
    .end local v1    # "font":Lorg/icepdf/index/core/pobjects/fonts/FontFile;
    .restart local v0    # "font":Lorg/icepdf/index/core/pobjects/fonts/FontFile;
    :cond_3
    add-int/lit8 v2, v2, -0x1

    goto :goto_2

    .end local v2    # "i":I
    :cond_4
    move-object v1, v0

    .line 547
    .end local v0    # "font":Lorg/icepdf/index/core/pobjects/fonts/FontFile;
    .restart local v1    # "font":Lorg/icepdf/index/core/pobjects/fonts/FontFile;
    goto :goto_1
.end method

.method private getCoreJavaFont(Ljava/lang/String;I)Lorg/icepdf/index/core/pobjects/fonts/FontFile;
    .locals 5
    .param p1, "fontName"    # Ljava/lang/String;
    .param p2, "flags"    # I

    .prologue
    const/4 v4, 0x0

    const/4 v3, -0x1

    .line 740
    invoke-static {p1}, Lorg/icepdf/index/core/pobjects/fonts/FontManager;->guessFontStyle(Ljava/lang/String;)I

    move-result v0

    .line 741
    .local v0, "decorations":I
    invoke-static {p1}, Lorg/icepdf/index/core/pobjects/fonts/FontManager;->normalizeString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 745
    const-string/jumbo v2, "timesnewroman"

    invoke-virtual {p1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    if-gez v2, :cond_0

    const-string/jumbo v2, "bodoni"

    invoke-virtual {p1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    if-gez v2, :cond_0

    const-string/jumbo v2, "garamond"

    invoke-virtual {p1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    if-gez v2, :cond_0

    const-string/jumbo v2, "minionweb"

    invoke-virtual {p1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    if-gez v2, :cond_0

    const-string/jumbo v2, "stoneserif"

    invoke-virtual {p1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    if-gez v2, :cond_0

    const-string/jumbo v2, "georgia"

    invoke-virtual {p1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    if-gez v2, :cond_0

    const-string/jumbo v2, "bitstreamcyberbit"

    invoke-virtual {p1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    if-ltz v2, :cond_1

    .line 753
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "lucidabright-"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-direct {p0, v0, p2}, Lorg/icepdf/index/core/pobjects/fonts/FontManager;->getFontSytle(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2, v4}, Lorg/icepdf/index/core/pobjects/fonts/FontManager;->findFont(Ljava/lang/String;I)Lorg/icepdf/index/core/pobjects/fonts/FontFile;

    move-result-object v1

    .line 789
    .local v1, "font":Lorg/icepdf/index/core/pobjects/fonts/FontFile;
    :goto_0
    return-object v1

    .line 757
    .end local v1    # "font":Lorg/icepdf/index/core/pobjects/fonts/FontFile;
    :cond_1
    const-string/jumbo v2, "helvetica"

    invoke-virtual {p1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    if-ne v2, v3, :cond_2

    const-string/jumbo v2, "arial"

    invoke-virtual {p1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    if-ne v2, v3, :cond_2

    const-string/jumbo v2, "trebuchet"

    invoke-virtual {p1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    if-ne v2, v3, :cond_2

    const-string/jumbo v2, "avantgardegothic"

    invoke-virtual {p1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    if-ne v2, v3, :cond_2

    const-string/jumbo v2, "verdana"

    invoke-virtual {p1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    if-ne v2, v3, :cond_2

    const-string/jumbo v2, "univers"

    invoke-virtual {p1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    if-ne v2, v3, :cond_2

    const-string/jumbo v2, "futura"

    invoke-virtual {p1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    if-ne v2, v3, :cond_2

    const-string/jumbo v2, "stonesans"

    invoke-virtual {p1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    if-ne v2, v3, :cond_2

    const-string/jumbo v2, "gillsans"

    invoke-virtual {p1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    if-ne v2, v3, :cond_2

    const-string/jumbo v2, "akzidenz"

    invoke-virtual {p1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    if-ne v2, v3, :cond_2

    const-string/jumbo v2, "frutiger"

    invoke-virtual {p1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    if-ne v2, v3, :cond_2

    const-string/jumbo v2, "grotesk"

    invoke-virtual {p1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    if-eq v2, v3, :cond_3

    .line 770
    :cond_2
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "lucidasans-"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-direct {p0, v0, p2}, Lorg/icepdf/index/core/pobjects/fonts/FontManager;->getFontSytle(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2, v4}, Lorg/icepdf/index/core/pobjects/fonts/FontManager;->findFont(Ljava/lang/String;I)Lorg/icepdf/index/core/pobjects/fonts/FontFile;

    move-result-object v1

    .restart local v1    # "font":Lorg/icepdf/index/core/pobjects/fonts/FontFile;
    goto/16 :goto_0

    .line 774
    .end local v1    # "font":Lorg/icepdf/index/core/pobjects/fonts/FontFile;
    :cond_3
    const-string/jumbo v2, "courier"

    invoke-virtual {p1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    if-ne v2, v3, :cond_4

    const-string/jumbo v2, "couriernew"

    invoke-virtual {p1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    if-ne v2, v3, :cond_4

    const-string/jumbo v2, "prestige"

    invoke-virtual {p1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    if-ne v2, v3, :cond_4

    const-string/jumbo v2, "eversonmono"

    invoke-virtual {p1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    if-eq v2, v3, :cond_5

    .line 779
    :cond_4
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "lucidasanstypewriter-"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-direct {p0, v0, p2}, Lorg/icepdf/index/core/pobjects/fonts/FontManager;->getFontSytle(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2, v4}, Lorg/icepdf/index/core/pobjects/fonts/FontManager;->findFont(Ljava/lang/String;I)Lorg/icepdf/index/core/pobjects/fonts/FontFile;

    move-result-object v1

    .restart local v1    # "font":Lorg/icepdf/index/core/pobjects/fonts/FontFile;
    goto/16 :goto_0

    .line 786
    .end local v1    # "font":Lorg/icepdf/index/core/pobjects/fonts/FontFile;
    :cond_5
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "lucidabright-"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-direct {p0, v0, p2}, Lorg/icepdf/index/core/pobjects/fonts/FontManager;->getFontSytle(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2, v4}, Lorg/icepdf/index/core/pobjects/fonts/FontManager;->findFont(Ljava/lang/String;I)Lorg/icepdf/index/core/pobjects/fonts/FontFile;

    move-result-object v1

    .restart local v1    # "font":Lorg/icepdf/index/core/pobjects/fonts/FontFile;
    goto/16 :goto_0
.end method

.method private getFontSytle(II)Ljava/lang/String;
    .locals 3
    .param p1, "sytle"    # I
    .param p2, "flags"    # I

    .prologue
    .line 867
    const-string/jumbo v0, ""

    .line 868
    .local v0, "style":Ljava/lang/String;
    sget v1, Lorg/icepdf/index/core/pobjects/fonts/FontManager;->BOLD_ITALIC:I

    and-int/2addr v1, p1

    sget v2, Lorg/icepdf/index/core/pobjects/fonts/FontManager;->BOLD_ITALIC:I

    if-ne v1, v2, :cond_1

    .line 869
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " BoldItalic"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 877
    :cond_0
    :goto_0
    return-object v0

    .line 870
    :cond_1
    sget v1, Lorg/icepdf/index/core/pobjects/fonts/FontManager;->BOLD:I

    and-int/2addr v1, p1

    sget v2, Lorg/icepdf/index/core/pobjects/fonts/FontManager;->BOLD:I

    if-ne v1, v2, :cond_2

    .line 871
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " Bold"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 872
    :cond_2
    sget v1, Lorg/icepdf/index/core/pobjects/fonts/FontManager;->ITALIC:I

    and-int/2addr v1, p1

    sget v2, Lorg/icepdf/index/core/pobjects/fonts/FontManager;->ITALIC:I

    if-ne v1, v2, :cond_3

    .line 873
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " Italic"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 874
    :cond_3
    sget v1, Lorg/icepdf/index/core/pobjects/fonts/FontManager;->PLAIN:I

    and-int/2addr v1, p1

    sget v2, Lorg/icepdf/index/core/pobjects/fonts/FontManager;->PLAIN:I

    if-ne v1, v2, :cond_0

    .line 875
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " Plain"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static getInstance()Lorg/icepdf/index/core/pobjects/fonts/FontManager;
    .locals 1

    .prologue
    .line 236
    sget-object v0, Lorg/icepdf/index/core/pobjects/fonts/FontManager;->fontManager:Lorg/icepdf/index/core/pobjects/fonts/FontManager;

    if-nez v0, :cond_0

    .line 237
    new-instance v0, Lorg/icepdf/index/core/pobjects/fonts/FontManager;

    invoke-direct {v0}, Lorg/icepdf/index/core/pobjects/fonts/FontManager;-><init>()V

    sput-object v0, Lorg/icepdf/index/core/pobjects/fonts/FontManager;->fontManager:Lorg/icepdf/index/core/pobjects/fonts/FontManager;

    .line 239
    :cond_0
    sget-object v0, Lorg/icepdf/index/core/pobjects/fonts/FontManager;->fontManager:Lorg/icepdf/index/core/pobjects/fonts/FontManager;

    return-object v0
.end method

.method private getType1Fonts(Ljava/lang/String;I)Lorg/icepdf/index/core/pobjects/fonts/FontFile;
    .locals 9
    .param p1, "fontName"    # Ljava/lang/String;
    .param p2, "flags"    # I

    .prologue
    .line 801
    const/4 v0, 0x0

    .line 802
    .local v0, "font":Lorg/icepdf/index/core/pobjects/fonts/FontFile;
    const/4 v1, 0x0

    .line 803
    .local v1, "found":Z
    const/4 v3, 0x1

    .line 805
    .local v3, "isType1Available":Z
    const/4 v2, 0x0

    .local v2, "i":I
    sget-object v7, Lorg/icepdf/index/core/pobjects/fonts/FontManager;->TYPE1_FONT_DIFFS:[[Ljava/lang/String;

    array-length v5, v7

    .local v5, "max":I
    :goto_0
    if-ge v2, v5, :cond_1

    .line 806
    const/4 v4, 0x0

    .local v4, "j":I
    sget-object v7, Lorg/icepdf/index/core/pobjects/fonts/FontManager;->TYPE1_FONT_DIFFS:[[Ljava/lang/String;

    aget-object v7, v7, v2

    array-length v6, v7

    .local v6, "max2":I
    :goto_1
    if-ge v4, v6, :cond_0

    .line 808
    sget-object v7, Lorg/icepdf/index/core/pobjects/fonts/FontManager;->TYPE1_FONT_DIFFS:[[Ljava/lang/String;

    aget-object v7, v7, v2

    const/4 v8, 0x0

    aget-object v7, v7, v8

    invoke-virtual {v7, p1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v7

    if-ltz v7, :cond_4

    .line 810
    if-eqz v3, :cond_3

    .line 811
    sget-object v7, Lorg/icepdf/index/core/pobjects/fonts/FontManager;->TYPE1_FONT_DIFFS:[[Ljava/lang/String;

    aget-object v7, v7, v2

    const/4 v8, 0x1

    aget-object v7, v7, v8

    invoke-direct {p0, v7, p2}, Lorg/icepdf/index/core/pobjects/fonts/FontManager;->findFont(Ljava/lang/String;I)Lorg/icepdf/index/core/pobjects/fonts/FontFile;

    move-result-object v0

    .line 812
    if-eqz v0, :cond_2

    .line 813
    const/4 v1, 0x1

    .line 829
    :cond_0
    :goto_2
    if-eqz v1, :cond_5

    .line 831
    .end local v4    # "j":I
    .end local v6    # "max2":I
    :cond_1
    return-object v0

    .line 816
    .restart local v4    # "j":I
    .restart local v6    # "max2":I
    :cond_2
    const/4 v3, 0x0

    .line 820
    :cond_3
    sget-object v7, Lorg/icepdf/index/core/pobjects/fonts/FontManager;->TYPE1_FONT_DIFFS:[[Ljava/lang/String;

    aget-object v7, v7, v2

    aget-object v7, v7, v4

    invoke-direct {p0, v7, p2}, Lorg/icepdf/index/core/pobjects/fonts/FontManager;->findFont(Ljava/lang/String;I)Lorg/icepdf/index/core/pobjects/fonts/FontFile;

    move-result-object v0

    .line 821
    if-eqz v0, :cond_4

    .line 822
    const/4 v1, 0x1

    .line 823
    goto :goto_2

    .line 806
    :cond_4
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 805
    :cond_5
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public static guessFamily(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 416
    move-object v0, p0

    .line 419
    .local v0, "fam":Ljava/lang/String;
    const/16 v2, 0x2c

    invoke-virtual {v0, v2}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    .local v1, "inx":I
    if-lez v1, :cond_0

    .line 420
    invoke-virtual {v0, v3, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 422
    :cond_0
    const/16 v2, 0x2d

    invoke-virtual {v0, v2}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v1

    if-lez v1, :cond_1

    .line 423
    invoke-virtual {v0, v3, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 424
    :cond_1
    return-object v0
.end method

.method private static guessFontStyle(Ljava/lang/String;)I
    .locals 2
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 842
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p0, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p0

    .line 843
    const/4 v0, 0x0

    .line 844
    .local v0, "decorations":I
    const-string/jumbo v1, "boldital"

    invoke-virtual {p0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    if-gtz v1, :cond_0

    const-string/jumbo v1, "demiital"

    invoke-virtual {p0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    if-lez v1, :cond_1

    .line 845
    :cond_0
    sget v1, Lorg/icepdf/index/core/pobjects/fonts/FontManager;->BOLD_ITALIC:I

    or-int/2addr v0, v1

    .line 854
    :goto_0
    return v0

    .line 846
    :cond_1
    const-string/jumbo v1, "bold"

    invoke-virtual {p0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    if-gtz v1, :cond_2

    const-string/jumbo v1, "black"

    invoke-virtual {p0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    if-gtz v1, :cond_2

    const-string/jumbo v1, "demi"

    invoke-virtual {p0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    if-lez v1, :cond_3

    .line 848
    :cond_2
    sget v1, Lorg/icepdf/index/core/pobjects/fonts/FontManager;->BOLD:I

    or-int/2addr v0, v1

    goto :goto_0

    .line 849
    :cond_3
    const-string/jumbo v1, "ital"

    invoke-virtual {p0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    if-gtz v1, :cond_4

    const-string/jumbo v1, "obli"

    invoke-virtual {p0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    if-lez v1, :cond_5

    .line 850
    :cond_4
    sget v1, Lorg/icepdf/index/core/pobjects/fonts/FontManager;->ITALIC:I

    or-int/2addr v0, v1

    goto :goto_0

    .line 852
    :cond_5
    sget v1, Lorg/icepdf/index/core/pobjects/fonts/FontManager;->PLAIN:I

    or-int/2addr v0, v1

    goto :goto_0
.end method

.method private static normalizeString(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 887
    invoke-static {p0}, Lorg/icepdf/index/core/pobjects/fonts/FontManager;->guessFamily(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 888
    new-instance v1, Ljava/lang/StringBuffer;

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p0, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 889
    .local v1, "normalized":Ljava/lang/StringBuffer;
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->length()I

    move-result v2

    add-int/lit8 v0, v2, -0x1

    .local v0, "k":I
    :goto_0
    if-ltz v0, :cond_1

    .line 890
    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v2

    const/16 v3, 0x20

    if-ne v2, v3, :cond_0

    .line 891
    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->deleteCharAt(I)Ljava/lang/StringBuffer;

    .line 892
    add-int/lit8 v0, v0, -0x1

    .line 889
    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 895
    :cond_1
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method


# virtual methods
.method public clearFontList()V
    .locals 1

    .prologue
    .line 326
    sget-object v0, Lorg/icepdf/index/core/pobjects/fonts/FontManager;->fontList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 327
    sget-object v0, Lorg/icepdf/index/core/pobjects/fonts/FontManager;->fontList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 329
    :cond_0
    return-void
.end method

.method public getAvailableFamilies()[Ljava/lang/String;
    .locals 5

    .prologue
    .line 452
    sget-object v4, Lorg/icepdf/index/core/pobjects/fonts/FontManager;->fontList:Ljava/util/ArrayList;

    if-eqz v4, :cond_0

    .line 453
    sget-object v4, Lorg/icepdf/index/core/pobjects/fonts/FontManager;->fontList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    new-array v0, v4, [Ljava/lang/String;

    .line 454
    .local v0, "availableNames":[Ljava/lang/String;
    sget-object v4, Lorg/icepdf/index/core/pobjects/fonts/FontManager;->fontList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 456
    .local v3, "nameIterator":Ljava/util/Iterator;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 457
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Ljava/lang/Object;

    move-object v1, v4

    check-cast v1, [Ljava/lang/Object;

    .line 458
    .local v1, "fontData":[Ljava/lang/Object;
    const/4 v4, 0x1

    aget-object v4, v1, v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v0, v2

    .line 456
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 462
    .end local v0    # "availableNames":[Ljava/lang/String;
    .end local v1    # "fontData":[Ljava/lang/Object;
    .end local v2    # "i":I
    .end local v3    # "nameIterator":Ljava/util/Iterator;
    :cond_0
    const/4 v0, 0x0

    :cond_1
    return-object v0
.end method

.method public getAvailableNames()[Ljava/lang/String;
    .locals 5

    .prologue
    .line 433
    sget-object v4, Lorg/icepdf/index/core/pobjects/fonts/FontManager;->fontList:Ljava/util/ArrayList;

    if-eqz v4, :cond_0

    .line 434
    sget-object v4, Lorg/icepdf/index/core/pobjects/fonts/FontManager;->fontList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    new-array v0, v4, [Ljava/lang/String;

    .line 435
    .local v0, "availableNames":[Ljava/lang/String;
    sget-object v4, Lorg/icepdf/index/core/pobjects/fonts/FontManager;->fontList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 437
    .local v3, "nameIterator":Ljava/util/Iterator;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 438
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Ljava/lang/Object;

    move-object v1, v4

    check-cast v1, [Ljava/lang/Object;

    .line 439
    .local v1, "fontData":[Ljava/lang/Object;
    const/4 v4, 0x0

    aget-object v4, v1, v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v0, v2

    .line 437
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 443
    .end local v0    # "availableNames":[Ljava/lang/String;
    .end local v1    # "fontData":[Ljava/lang/Object;
    .end local v2    # "i":I
    .end local v3    # "nameIterator":Ljava/util/Iterator;
    :cond_0
    const/4 v0, 0x0

    :cond_1
    return-object v0
.end method

.method public getAvailableStyle()[Ljava/lang/String;
    .locals 8

    .prologue
    .line 471
    sget-object v6, Lorg/icepdf/index/core/pobjects/fonts/FontManager;->fontList:Ljava/util/ArrayList;

    if-eqz v6, :cond_4

    .line 472
    sget-object v6, Lorg/icepdf/index/core/pobjects/fonts/FontManager;->fontList:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    new-array v0, v6, [Ljava/lang/String;

    .line 473
    .local v0, "availableStyles":[Ljava/lang/String;
    sget-object v6, Lorg/icepdf/index/core/pobjects/fonts/FontManager;->fontList:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .line 476
    .local v4, "nameIterator":Ljava/util/Iterator;
    const-string/jumbo v5, ""

    .line 477
    .local v5, "style":Ljava/lang/String;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_5

    .line 478
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, [Ljava/lang/Object;

    move-object v2, v6

    check-cast v2, [Ljava/lang/Object;

    .line 479
    .local v2, "fontData":[Ljava/lang/Object;
    const/4 v6, 0x2

    aget-object v6, v2, v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 480
    .local v1, "decorations":I
    sget v6, Lorg/icepdf/index/core/pobjects/fonts/FontManager;->BOLD_ITALIC:I

    and-int/2addr v6, v1

    sget v7, Lorg/icepdf/index/core/pobjects/fonts/FontManager;->BOLD_ITALIC:I

    if-ne v6, v7, :cond_1

    .line 481
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, " BoldItalic"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 489
    :cond_0
    :goto_1
    aput-object v5, v0, v3

    .line 490
    const-string/jumbo v5, ""

    .line 477
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 482
    :cond_1
    sget v6, Lorg/icepdf/index/core/pobjects/fonts/FontManager;->BOLD:I

    and-int/2addr v6, v1

    sget v7, Lorg/icepdf/index/core/pobjects/fonts/FontManager;->BOLD:I

    if-ne v6, v7, :cond_2

    .line 483
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, " Bold"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto :goto_1

    .line 484
    :cond_2
    sget v6, Lorg/icepdf/index/core/pobjects/fonts/FontManager;->ITALIC:I

    and-int/2addr v6, v1

    sget v7, Lorg/icepdf/index/core/pobjects/fonts/FontManager;->ITALIC:I

    if-ne v6, v7, :cond_3

    .line 485
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, " Italic"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto :goto_1

    .line 486
    :cond_3
    sget v6, Lorg/icepdf/index/core/pobjects/fonts/FontManager;->PLAIN:I

    and-int/2addr v6, v1

    sget v7, Lorg/icepdf/index/core/pobjects/fonts/FontManager;->PLAIN:I

    if-ne v6, v7, :cond_0

    .line 487
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, " Plain"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto :goto_1

    .line 494
    .end local v0    # "availableStyles":[Ljava/lang/String;
    .end local v1    # "decorations":I
    .end local v2    # "fontData":[Ljava/lang/Object;
    .end local v3    # "i":I
    .end local v4    # "nameIterator":Ljava/util/Iterator;
    .end local v5    # "style":Ljava/lang/String;
    :cond_4
    const/4 v0, 0x0

    :cond_5
    return-object v0
.end method

.method public getChineseSimplifiedInstance(Ljava/lang/String;I)Lorg/icepdf/index/core/pobjects/fonts/FontFile;
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "fontFlags"    # I

    .prologue
    .line 510
    sget-object v0, Lorg/icepdf/index/core/pobjects/fonts/FontManager;->CHINESE_SIMPLIFIED_FONT_NAMES:[Ljava/lang/String;

    invoke-direct {p0, p1, v0, p2}, Lorg/icepdf/index/core/pobjects/fonts/FontManager;->getAsianInstance(Ljava/lang/String;[Ljava/lang/String;I)Lorg/icepdf/index/core/pobjects/fonts/FontFile;

    move-result-object v0

    return-object v0
.end method

.method public getChineseTraditionalInstance(Ljava/lang/String;I)Lorg/icepdf/index/core/pobjects/fonts/FontFile;
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "fontFlags"    # I

    .prologue
    .line 506
    sget-object v0, Lorg/icepdf/index/core/pobjects/fonts/FontManager;->CHINESE_TRADITIONAL_FONT_NAMES:[Ljava/lang/String;

    invoke-direct {p0, p1, v0, p2}, Lorg/icepdf/index/core/pobjects/fonts/FontManager;->getAsianInstance(Ljava/lang/String;[Ljava/lang/String;I)Lorg/icepdf/index/core/pobjects/fonts/FontFile;

    move-result-object v0

    return-object v0
.end method

.method public getFontProperties()Ljava/util/Properties;
    .locals 9

    .prologue
    .line 253
    sget-object v7, Lorg/icepdf/index/core/pobjects/fonts/FontManager;->fontList:Ljava/util/ArrayList;

    if-nez v7, :cond_0

    .line 254
    const/4 v7, 0x0

    invoke-virtual {p0, v7}, Lorg/icepdf/index/core/pobjects/fonts/FontManager;->readSystemFonts([Ljava/lang/String;)V

    .line 257
    :cond_0
    new-instance v4, Ljava/util/Properties;

    invoke-direct {v4}, Ljava/util/Properties;-><init>()V

    .line 258
    .local v4, "fontProperites":Ljava/util/Properties;
    sget-object v7, Lorg/icepdf/index/core/pobjects/fonts/FontManager;->fontList:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 267
    .local v3, "fontIterator":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 268
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, [Ljava/lang/Object;

    move-object v0, v7

    check-cast v0, [Ljava/lang/Object;

    .line 269
    .local v0, "currentFont":[Ljava/lang/Object;
    const/4 v7, 0x0

    aget-object v5, v0, v7

    check-cast v5, Ljava/lang/String;

    .line 270
    .local v5, "name":Ljava/lang/String;
    const/4 v7, 0x1

    aget-object v2, v0, v7

    check-cast v2, Ljava/lang/String;

    .line 271
    .local v2, "family":Ljava/lang/String;
    const/4 v7, 0x2

    aget-object v1, v0, v7

    check-cast v1, Ljava/lang/Integer;

    .line 272
    .local v1, "decorations":Ljava/lang/Integer;
    const/4 v7, 0x3

    aget-object v6, v0, v7

    check-cast v6, Ljava/lang/String;

    .line 274
    .local v6, "path":Ljava/lang/String;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, "|"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, "|"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v5, v7}, Ljava/util/Properties;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 276
    .end local v0    # "currentFont":[Ljava/lang/Object;
    .end local v1    # "decorations":Ljava/lang/Integer;
    .end local v2    # "family":Ljava/lang/String;
    .end local v5    # "name":Ljava/lang/String;
    .end local v6    # "path":Ljava/lang/String;
    :cond_1
    return-object v4
.end method

.method public getInstance(Ljava/lang/String;I)Lorg/icepdf/index/core/pobjects/fonts/FontFile;
    .locals 5
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "flags"    # I

    .prologue
    .line 561
    sget-object v3, Lorg/icepdf/index/core/pobjects/fonts/FontManager;->fontList:Ljava/util/ArrayList;

    if-nez v3, :cond_0

    .line 562
    const/4 v3, 0x0

    invoke-virtual {p0, v3}, Lorg/icepdf/index/core/pobjects/fonts/FontManager;->readSystemFonts([Ljava/lang/String;)V

    .line 568
    :cond_0
    invoke-direct {p0, p1, p2}, Lorg/icepdf/index/core/pobjects/fonts/FontManager;->getType1Fonts(Ljava/lang/String;I)Lorg/icepdf/index/core/pobjects/fonts/FontFile;

    move-result-object v0

    .line 569
    .local v0, "font":Lorg/icepdf/index/core/pobjects/fonts/FontFile;
    if-eqz v0, :cond_1

    move-object v1, v0

    .line 606
    .end local v0    # "font":Lorg/icepdf/index/core/pobjects/fonts/FontFile;
    .local v1, "font":Lorg/icepdf/index/core/pobjects/fonts/FontFile;
    :goto_0
    return-object v1

    .line 577
    .end local v1    # "font":Lorg/icepdf/index/core/pobjects/fonts/FontFile;
    .restart local v0    # "font":Lorg/icepdf/index/core/pobjects/fonts/FontFile;
    :cond_1
    invoke-direct {p0, p1, p2}, Lorg/icepdf/index/core/pobjects/fonts/FontManager;->findFont(Ljava/lang/String;I)Lorg/icepdf/index/core/pobjects/fonts/FontFile;

    move-result-object v0

    .line 578
    if-eqz v0, :cond_2

    move-object v1, v0

    .line 582
    .end local v0    # "font":Lorg/icepdf/index/core/pobjects/fonts/FontFile;
    .restart local v1    # "font":Lorg/icepdf/index/core/pobjects/fonts/FontFile;
    goto :goto_0

    .line 586
    .end local v1    # "font":Lorg/icepdf/index/core/pobjects/fonts/FontFile;
    .restart local v0    # "font":Lorg/icepdf/index/core/pobjects/fonts/FontFile;
    :cond_2
    invoke-direct {p0, p1, p2}, Lorg/icepdf/index/core/pobjects/fonts/FontManager;->getCoreJavaFont(Ljava/lang/String;I)Lorg/icepdf/index/core/pobjects/fonts/FontFile;

    move-result-object v0

    .line 587
    if-eqz v0, :cond_3

    move-object v1, v0

    .line 591
    .end local v0    # "font":Lorg/icepdf/index/core/pobjects/fonts/FontFile;
    .restart local v1    # "font":Lorg/icepdf/index/core/pobjects/fonts/FontFile;
    goto :goto_0

    .line 596
    .end local v1    # "font":Lorg/icepdf/index/core/pobjects/fonts/FontFile;
    .restart local v0    # "font":Lorg/icepdf/index/core/pobjects/fonts/FontFile;
    :cond_3
    sget-object v3, Lorg/icepdf/index/core/pobjects/fonts/FontManager;->fontList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lez v3, :cond_4

    .line 597
    sget-object v3, Lorg/icepdf/index/core/pobjects/fonts/FontManager;->fontList:Ljava/util/ArrayList;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/lang/Object;

    .line 598
    .local v2, "fontData":[Ljava/lang/Object;
    const/4 v3, 0x3

    aget-object v3, v2, v3

    check-cast v3, Ljava/lang/String;

    invoke-direct {p0, v3}, Lorg/icepdf/index/core/pobjects/fonts/FontManager;->buildFont(Ljava/lang/String;)Lorg/icepdf/index/core/pobjects/fonts/FontFile;

    move-result-object v0

    .line 600
    .end local v2    # "fontData":[Ljava/lang/Object;
    :cond_4
    if-nez v0, :cond_5

    :cond_5
    move-object v1, v0

    .line 606
    .end local v0    # "font":Lorg/icepdf/index/core/pobjects/fonts/FontFile;
    .restart local v1    # "font":Lorg/icepdf/index/core/pobjects/fonts/FontFile;
    goto :goto_0
.end method

.method public getJapaneseInstance(Ljava/lang/String;I)Lorg/icepdf/index/core/pobjects/fonts/FontFile;
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "fontFlags"    # I

    .prologue
    .line 498
    sget-object v0, Lorg/icepdf/index/core/pobjects/fonts/FontManager;->JAPANESE_FONT_NAMES:[Ljava/lang/String;

    invoke-direct {p0, p1, v0, p2}, Lorg/icepdf/index/core/pobjects/fonts/FontManager;->getAsianInstance(Ljava/lang/String;[Ljava/lang/String;I)Lorg/icepdf/index/core/pobjects/fonts/FontFile;

    move-result-object v0

    return-object v0
.end method

.method public getKoreanInstance(Ljava/lang/String;I)Lorg/icepdf/index/core/pobjects/fonts/FontFile;
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "fontFlags"    # I

    .prologue
    .line 502
    sget-object v0, Lorg/icepdf/index/core/pobjects/fonts/FontManager;->KOREAN_FONT_NAMES:[Ljava/lang/String;

    invoke-direct {p0, p1, v0, p2}, Lorg/icepdf/index/core/pobjects/fonts/FontManager;->getAsianInstance(Ljava/lang/String;[Ljava/lang/String;I)Lorg/icepdf/index/core/pobjects/fonts/FontFile;

    move-result-object v0

    return-object v0
.end method

.method public readSystemFonts([Ljava/lang/String;)V
    .locals 16
    .param p1, "extraFontPaths"    # [Ljava/lang/String;

    .prologue
    .line 343
    sget-object v11, Lorg/icepdf/index/core/pobjects/fonts/FontManager;->fontList:Ljava/util/ArrayList;

    if-nez v11, :cond_0

    .line 344
    new-instance v11, Ljava/util/ArrayList;

    const/16 v12, 0x96

    invoke-direct {v11, v12}, Ljava/util/ArrayList;-><init>(I)V

    sput-object v11, Lorg/icepdf/index/core/pobjects/fonts/FontManager;->fontList:Ljava/util/ArrayList;

    .line 357
    :cond_0
    if-nez p1, :cond_2

    .line 358
    sget-object v3, Lorg/icepdf/index/core/pobjects/fonts/FontManager;->SYSTEM_FONT_PATHS:[Ljava/lang/String;

    .line 373
    .local v3, "fontDirectories":[Ljava/lang/String;
    :goto_0
    array-length v11, v3

    add-int/lit8 v7, v11, -0x1

    .local v7, "i":I
    :goto_1
    if-ltz v7, :cond_4

    .line 374
    aget-object v10, v3, v7

    .line 376
    .local v10, "path":Ljava/lang/String;
    if-eqz v10, :cond_3

    .line 377
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 378
    .local v1, "directory":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->canRead()Z

    move-result v11

    if-eqz v11, :cond_3

    .line 379
    invoke-virtual {v1}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v6

    .line 380
    .local v6, "fontPaths":[Ljava/lang/String;
    array-length v11, v6

    add-int/lit8 v8, v11, -0x1

    .local v8, "j":I
    :goto_2
    if-ltz v8, :cond_3

    .line 381
    aget-object v4, v6, v8

    .line 382
    .local v4, "fontName":Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuffer;

    const/16 v11, 0x19

    invoke-direct {v5, v11}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 383
    .local v5, "fontPath":Ljava/lang/StringBuffer;
    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v5, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v11

    sget-char v12, Ljava/io/File;->separatorChar:C

    invoke-virtual {v11, v12}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v11

    invoke-virtual {v11, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 389
    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p0

    invoke-direct {v0, v11}, Lorg/icepdf/index/core/pobjects/fonts/FontManager;->buildFont(Ljava/lang/String;)Lorg/icepdf/index/core/pobjects/fonts/FontFile;

    move-result-object v2

    .line 391
    .local v2, "font":Lorg/icepdf/index/core/pobjects/fonts/FontFile;
    if-eqz v2, :cond_1

    .line 393
    invoke-interface {v2}, Lorg/icepdf/index/core/pobjects/fonts/FontFile;->getName()Ljava/lang/String;

    move-result-object v11

    sget-object v12, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v11, v12}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v4

    .line 395
    sget-object v11, Lorg/icepdf/index/core/pobjects/fonts/FontManager;->fontList:Ljava/util/ArrayList;

    const/4 v12, 0x4

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    invoke-interface {v2}, Lorg/icepdf/index/core/pobjects/fonts/FontFile;->getName()Ljava/lang/String;

    move-result-object v14

    sget-object v15, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v14, v15}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v14

    aput-object v14, v12, v13

    const/4 v13, 0x1

    invoke-interface {v2}, Lorg/icepdf/index/core/pobjects/fonts/FontFile;->getFamily()Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, Lorg/icepdf/index/core/pobjects/fonts/FontManager;->normalizeString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    aput-object v14, v12, v13

    const/4 v13, 0x2

    invoke-static {v4}, Lorg/icepdf/index/core/pobjects/fonts/FontManager;->guessFontStyle(Ljava/lang/String;)I

    move-result v14

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    aput-object v14, v12, v13

    const/4 v13, 0x3

    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v14

    aput-object v14, v12, v13

    invoke-virtual {v11, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 380
    :cond_1
    add-int/lit8 v8, v8, -0x1

    goto :goto_2

    .line 360
    .end local v1    # "directory":Ljava/io/File;
    .end local v2    # "font":Lorg/icepdf/index/core/pobjects/fonts/FontFile;
    .end local v3    # "fontDirectories":[Ljava/lang/String;
    .end local v4    # "fontName":Ljava/lang/String;
    .end local v5    # "fontPath":Ljava/lang/StringBuffer;
    .end local v6    # "fontPaths":[Ljava/lang/String;
    .end local v7    # "i":I
    .end local v8    # "j":I
    .end local v10    # "path":Ljava/lang/String;
    :cond_2
    sget-object v11, Lorg/icepdf/index/core/pobjects/fonts/FontManager;->SYSTEM_FONT_PATHS:[Ljava/lang/String;

    array-length v11, v11

    move-object/from16 v0, p1

    array-length v12, v0

    add-int v9, v11, v12

    .line 361
    .local v9, "length":I
    new-array v3, v9, [Ljava/lang/String;

    .line 363
    .restart local v3    # "fontDirectories":[Ljava/lang/String;
    sget-object v11, Lorg/icepdf/index/core/pobjects/fonts/FontManager;->SYSTEM_FONT_PATHS:[Ljava/lang/String;

    const/4 v12, 0x0

    const/4 v13, 0x0

    sget-object v14, Lorg/icepdf/index/core/pobjects/fonts/FontManager;->SYSTEM_FONT_PATHS:[Ljava/lang/String;

    array-length v14, v14

    invoke-static {v11, v12, v3, v13, v14}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 365
    const/4 v11, 0x0

    sget-object v12, Lorg/icepdf/index/core/pobjects/fonts/FontManager;->SYSTEM_FONT_PATHS:[Ljava/lang/String;

    array-length v12, v12

    move-object/from16 v0, p1

    array-length v13, v0

    move-object/from16 v0, p1

    invoke-static {v0, v11, v3, v12, v13}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto/16 :goto_0

    .line 373
    .end local v9    # "length":I
    .restart local v7    # "i":I
    .restart local v10    # "path":Ljava/lang/String;
    :cond_3
    add-int/lit8 v7, v7, -0x1

    goto/16 :goto_1

    .line 407
    .end local v10    # "path":Ljava/lang/String;
    :cond_4
    return-void
.end method

.method public setFontProperties(Ljava/util/Properties;)V
    .locals 11
    .param p1, "fontProperties"    # Ljava/util/Properties;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 290
    const-string/jumbo v2, "Error parsing font properties "

    .line 292
    .local v2, "errorString":Ljava/lang/String;
    :try_start_0
    new-instance v8, Ljava/util/ArrayList;

    const/16 v9, 0x96

    invoke-direct {v8, v9}, Ljava/util/ArrayList;-><init>(I)V

    sput-object v8, Lorg/icepdf/index/core/pobjects/fonts/FontManager;->fontList:Ljava/util/ArrayList;

    .line 293
    invoke-virtual {p1}, Ljava/util/Properties;->propertyNames()Ljava/util/Enumeration;

    move-result-object v4

    .line 300
    .local v4, "fonts":Ljava/util/Enumeration;
    :goto_0
    invoke-interface {v4}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v8

    if-eqz v8, :cond_1

    .line 301
    invoke-interface {v4}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 302
    .local v5, "name":Ljava/lang/String;
    new-instance v7, Ljava/util/StringTokenizer;

    invoke-virtual {p1, v5}, Ljava/util/Properties;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    const-string/jumbo v9, "|"

    invoke-direct {v7, v8, v9}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 304
    .local v7, "tokens":Ljava/util/StringTokenizer;
    invoke-virtual {v7}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v3

    .line 305
    .local v3, "family":Ljava/lang/String;
    invoke-virtual {v7}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    .line 306
    .local v0, "decorations":Ljava/lang/Integer;
    invoke-virtual {v7}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v6

    .line 307
    .local v6, "path":Ljava/lang/String;
    if-eqz v5, :cond_0

    if-eqz v3, :cond_0

    if-eqz v6, :cond_0

    .line 308
    sget-object v8, Lorg/icepdf/index/core/pobjects/fonts/FontManager;->fontList:Ljava/util/ArrayList;

    const/4 v9, 0x4

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    aput-object v5, v9, v10

    const/4 v10, 0x1

    aput-object v3, v9, v10

    const/4 v10, 0x2

    aput-object v0, v9, v10

    const/4 v10, 0x3

    aput-object v6, v9, v10

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 314
    .end local v0    # "decorations":Ljava/lang/Integer;
    .end local v3    # "family":Ljava/lang/String;
    .end local v4    # "fonts":Ljava/util/Enumeration;
    .end local v5    # "name":Ljava/lang/String;
    .end local v6    # "path":Ljava/lang/String;
    .end local v7    # "tokens":Ljava/util/StringTokenizer;
    :catch_0
    move-exception v1

    .line 316
    .local v1, "e":Ljava/lang/Throwable;
    new-instance v8, Ljava/lang/IllegalArgumentException;

    invoke-direct {v8, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 310
    .end local v1    # "e":Ljava/lang/Throwable;
    .restart local v0    # "decorations":Ljava/lang/Integer;
    .restart local v3    # "family":Ljava/lang/String;
    .restart local v4    # "fonts":Ljava/util/Enumeration;
    .restart local v5    # "name":Ljava/lang/String;
    .restart local v6    # "path":Ljava/lang/String;
    .restart local v7    # "tokens":Ljava/util/StringTokenizer;
    :cond_0
    :try_start_1
    new-instance v8, Ljava/lang/IllegalArgumentException;

    invoke-direct {v8, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v8
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    .line 318
    .end local v0    # "decorations":Ljava/lang/Integer;
    .end local v3    # "family":Ljava/lang/String;
    .end local v5    # "name":Ljava/lang/String;
    .end local v6    # "path":Ljava/lang/String;
    .end local v7    # "tokens":Ljava/util/StringTokenizer;
    :cond_1
    return-void
.end method
