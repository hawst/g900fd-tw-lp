.class public Lorg/icepdf/index/core/pobjects/Page;
.super Lorg/icepdf/index/core/pobjects/Dictionary;
.source "Page.java"

# interfaces
.implements Lorg/icepdf/index/core/util/MemoryManageable;


# static fields
.field public static final BOUNDARY_ARTBOX:I = 0x5

.field public static final BOUNDARY_BLEEDBOX:I = 0x3

.field public static final BOUNDARY_CROPBOX:I = 0x2

.field public static final BOUNDARY_MEDIABOX:I = 0x1

.field public static final BOUNDARY_TRIMBOX:I = 0x4


# instance fields
.field private contents:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<",
            "Lorg/icepdf/index/core/pobjects/Stream;",
            ">;"
        }
    .end annotation
.end field

.field private isInited:Z

.field private final isInitedLock:Ljava/lang/Object;

.field private resources:Lorg/icepdf/index/core/pobjects/Resources;

.field private shapes:Lorg/icepdf/index/core/pobjects/graphics/Shapes;


# direct methods
.method public constructor <init>(Lorg/icepdf/index/core/util/Library;Lorg/icepdf/index/core/util/Hashtable;)V
    .locals 1
    .param p1, "l"    # Lorg/icepdf/index/core/util/Library;
    .param p2, "h"    # Lorg/icepdf/index/core/util/Hashtable;

    .prologue
    .line 150
    invoke-direct {p0, p1, p2}, Lorg/icepdf/index/core/pobjects/Dictionary;-><init>(Lorg/icepdf/index/core/util/Library;Lorg/icepdf/index/core/util/Hashtable;)V

    .line 101
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/icepdf/index/core/pobjects/Page;->isInited:Z

    .line 103
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lorg/icepdf/index/core/pobjects/Page;->isInitedLock:Ljava/lang/Object;

    .line 114
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/icepdf/index/core/pobjects/Page;->shapes:Lorg/icepdf/index/core/pobjects/graphics/Shapes;

    .line 151
    return-void
.end method

.method private initPageContents()V
    .locals 7

    .prologue
    .line 214
    iget-object v4, p0, Lorg/icepdf/index/core/pobjects/Page;->library:Lorg/icepdf/index/core/util/Library;

    iget-object v5, p0, Lorg/icepdf/index/core/pobjects/Page;->entries:Lorg/icepdf/index/core/util/Hashtable;

    const-string/jumbo v6, "Contents"

    invoke-virtual {v4, v5, v6}, Lorg/icepdf/index/core/util/Library;->getObject(Lorg/icepdf/index/core/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    .line 217
    .local v2, "pageContent":Ljava/lang/Object;
    instance-of v4, v2, Lorg/icepdf/index/core/pobjects/Stream;

    if-eqz v4, :cond_1

    .line 218
    new-instance v4, Ljava/util/Vector;

    invoke-direct {v4}, Ljava/util/Vector;-><init>()V

    iput-object v4, p0, Lorg/icepdf/index/core/pobjects/Page;->contents:Ljava/util/Vector;

    move-object v3, v2

    .line 219
    check-cast v3, Lorg/icepdf/index/core/pobjects/Stream;

    .line 220
    .local v3, "tmpStream":Lorg/icepdf/index/core/pobjects/Stream;
    iget-object v4, p0, Lorg/icepdf/index/core/pobjects/Page;->library:Lorg/icepdf/index/core/util/Library;

    iget-object v5, p0, Lorg/icepdf/index/core/pobjects/Page;->entries:Lorg/icepdf/index/core/util/Hashtable;

    const-string/jumbo v6, "Contents"

    invoke-virtual {v4, v5, v6}, Lorg/icepdf/index/core/util/Library;->getObjectReference(Lorg/icepdf/index/core/util/Hashtable;Ljava/lang/String;)Lorg/icepdf/index/core/pobjects/Reference;

    move-result-object v4

    invoke-virtual {v3, v4}, Lorg/icepdf/index/core/pobjects/Stream;->setPObjectReference(Lorg/icepdf/index/core/pobjects/Reference;)V

    .line 222
    iget-object v4, p0, Lorg/icepdf/index/core/pobjects/Page;->contents:Ljava/util/Vector;

    invoke-virtual {v4, v3}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    .line 235
    .end local v3    # "tmpStream":Lorg/icepdf/index/core/pobjects/Stream;
    :cond_0
    return-void

    .line 225
    :cond_1
    instance-of v4, v2, Ljava/util/Vector;

    if-eqz v4, :cond_0

    move-object v0, v2

    .line 226
    check-cast v0, Ljava/util/Vector;

    .line 227
    .local v0, "conts":Ljava/util/Vector;
    new-instance v4, Ljava/util/Vector;

    invoke-direct {v4}, Ljava/util/Vector;-><init>()V

    iput-object v4, p0, Lorg/icepdf/index/core/pobjects/Page;->contents:Ljava/util/Vector;

    .line 229
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v4

    if-ge v1, v4, :cond_0

    .line 230
    iget-object v5, p0, Lorg/icepdf/index/core/pobjects/Page;->library:Lorg/icepdf/index/core/util/Library;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/icepdf/index/core/pobjects/Reference;

    invoke-virtual {v5, v4}, Lorg/icepdf/index/core/util/Library;->getObject(Lorg/icepdf/index/core/pobjects/Reference;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/icepdf/index/core/pobjects/Stream;

    .line 231
    .restart local v3    # "tmpStream":Lorg/icepdf/index/core/pobjects/Stream;
    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/icepdf/index/core/pobjects/Reference;

    invoke-virtual {v3, v4}, Lorg/icepdf/index/core/pobjects/Stream;->setPObjectReference(Lorg/icepdf/index/core/pobjects/Reference;)V

    .line 232
    iget-object v4, p0, Lorg/icepdf/index/core/pobjects/Page;->contents:Ljava/util/Vector;

    invoke-virtual {v4, v3}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    .line 229
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private initPageResources()V
    .locals 6

    .prologue
    .line 238
    iget-object v3, p0, Lorg/icepdf/index/core/pobjects/Page;->library:Lorg/icepdf/index/core/util/Library;

    iget-object v4, p0, Lorg/icepdf/index/core/pobjects/Page;->entries:Lorg/icepdf/index/core/util/Hashtable;

    const-string/jumbo v5, "Resources"

    invoke-virtual {v3, v4, v5}, Lorg/icepdf/index/core/util/Library;->getResources(Lorg/icepdf/index/core/util/Hashtable;Ljava/lang/String;)Lorg/icepdf/index/core/pobjects/Resources;

    move-result-object v2

    .line 239
    .local v2, "res":Lorg/icepdf/index/core/pobjects/Resources;
    if-nez v2, :cond_0

    .line 240
    invoke-virtual {p0}, Lorg/icepdf/index/core/pobjects/Page;->getParent()Lorg/icepdf/index/core/pobjects/PageTree;

    move-result-object v1

    .line 241
    .local v1, "pt":Lorg/icepdf/index/core/pobjects/PageTree;
    :goto_0
    if-eqz v1, :cond_0

    .line 242
    invoke-virtual {v1}, Lorg/icepdf/index/core/pobjects/PageTree;->getResources()Lorg/icepdf/index/core/pobjects/Resources;

    move-result-object v0

    .line 243
    .local v0, "parentResources":Lorg/icepdf/index/core/pobjects/Resources;
    if-eqz v0, :cond_1

    .line 244
    move-object v2, v0

    .line 250
    .end local v0    # "parentResources":Lorg/icepdf/index/core/pobjects/Resources;
    .end local v1    # "pt":Lorg/icepdf/index/core/pobjects/PageTree;
    :cond_0
    iput-object v2, p0, Lorg/icepdf/index/core/pobjects/Page;->resources:Lorg/icepdf/index/core/pobjects/Resources;

    .line 251
    return-void

    .line 247
    .restart local v0    # "parentResources":Lorg/icepdf/index/core/pobjects/Resources;
    .restart local v1    # "pt":Lorg/icepdf/index/core/pobjects/PageTree;
    :cond_1
    invoke-virtual {v1}, Lorg/icepdf/index/core/pobjects/PageTree;->getParent()Lorg/icepdf/index/core/pobjects/PageTree;

    move-result-object v1

    .line 248
    goto :goto_0
.end method


# virtual methods
.method protected dispose(Z)V
    .locals 6
    .param p1, "cache"    # Z

    .prologue
    .line 164
    iget-object v5, p0, Lorg/icepdf/index/core/pobjects/Page;->isInitedLock:Ljava/lang/Object;

    monitor-enter v5

    .line 165
    :try_start_0
    iget-boolean v4, p0, Lorg/icepdf/index/core/pobjects/Page;->isInited:Z

    if-eqz v4, :cond_4

    .line 167
    const/4 v4, 0x0

    iput-boolean v4, p0, Lorg/icepdf/index/core/pobjects/Page;->isInited:Z

    .line 173
    iget-object v4, p0, Lorg/icepdf/index/core/pobjects/Page;->contents:Ljava/util/Vector;

    if-eqz v4, :cond_2

    .line 174
    iget-object v4, p0, Lorg/icepdf/index/core/pobjects/Page;->contents:Ljava/util/Vector;

    invoke-virtual {v4}, Ljava/util/Vector;->elements()Ljava/util/Enumeration;

    move-result-object v1

    .line 176
    .local v1, "pageContent":Ljava/util/Enumeration;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 177
    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v3

    .line 178
    .local v3, "tmp":Ljava/lang/Object;
    instance-of v4, v3, Lorg/icepdf/index/core/pobjects/Stream;

    if-eqz v4, :cond_0

    .line 180
    move-object v0, v3

    check-cast v0, Lorg/icepdf/index/core/pobjects/Stream;

    move-object v2, v0

    .line 181
    .local v2, "stream":Lorg/icepdf/index/core/pobjects/Stream;
    invoke-virtual {v2, p1}, Lorg/icepdf/index/core/pobjects/Stream;->dispose(Z)V

    goto :goto_0

    .line 206
    .end local v1    # "pageContent":Ljava/util/Enumeration;
    .end local v2    # "stream":Lorg/icepdf/index/core/pobjects/Stream;
    .end local v3    # "tmp":Ljava/lang/Object;
    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4

    .line 184
    .restart local v1    # "pageContent":Ljava/util/Enumeration;
    :cond_1
    :try_start_1
    iget-object v4, p0, Lorg/icepdf/index/core/pobjects/Page;->contents:Ljava/util/Vector;

    invoke-virtual {v4}, Ljava/util/Vector;->clear()V

    .line 188
    .end local v1    # "pageContent":Ljava/util/Enumeration;
    :cond_2
    iget-object v4, p0, Lorg/icepdf/index/core/pobjects/Page;->shapes:Lorg/icepdf/index/core/pobjects/graphics/Shapes;

    if-eqz v4, :cond_3

    .line 189
    iget-object v4, p0, Lorg/icepdf/index/core/pobjects/Page;->shapes:Lorg/icepdf/index/core/pobjects/graphics/Shapes;

    invoke-virtual {v4}, Lorg/icepdf/index/core/pobjects/graphics/Shapes;->dispose()V

    .line 190
    const/4 v4, 0x0

    iput-object v4, p0, Lorg/icepdf/index/core/pobjects/Page;->shapes:Lorg/icepdf/index/core/pobjects/graphics/Shapes;

    .line 200
    :cond_3
    iget-object v4, p0, Lorg/icepdf/index/core/pobjects/Page;->resources:Lorg/icepdf/index/core/pobjects/Resources;

    if-eqz v4, :cond_4

    .line 201
    iget-object v4, p0, Lorg/icepdf/index/core/pobjects/Page;->resources:Lorg/icepdf/index/core/pobjects/Resources;

    invoke-virtual {v4, p1}, Lorg/icepdf/index/core/pobjects/Resources;->dispose(Z)V

    .line 202
    const/4 v4, 0x0

    iput-object v4, p0, Lorg/icepdf/index/core/pobjects/Page;->resources:Lorg/icepdf/index/core/pobjects/Resources;

    .line 206
    :cond_4
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 207
    return-void
.end method

.method public getParent()Lorg/icepdf/index/core/pobjects/PageTree;
    .locals 3

    .prologue
    .line 274
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/Page;->library:Lorg/icepdf/index/core/util/Library;

    iget-object v1, p0, Lorg/icepdf/index/core/pobjects/Page;->entries:Lorg/icepdf/index/core/util/Hashtable;

    const-string/jumbo v2, "Parent"

    invoke-virtual {v0, v1, v2}, Lorg/icepdf/index/core/util/Library;->getObject(Lorg/icepdf/index/core/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/icepdf/index/core/pobjects/PageTree;

    return-object v0
.end method

.method protected getParentReference()Lorg/icepdf/index/core/pobjects/Reference;
    .locals 2

    .prologue
    .line 264
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/Page;->entries:Lorg/icepdf/index/core/util/Hashtable;

    const-string/jumbo v1, "Parent"

    invoke-virtual {v0, v1}, Lorg/icepdf/index/core/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/icepdf/index/core/pobjects/Reference;

    return-object v0
.end method

.method public getResources()Lorg/icepdf/index/core/pobjects/Resources;
    .locals 1

    .prologue
    .line 450
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/Page;->resources:Lorg/icepdf/index/core/pobjects/Resources;

    return-object v0
.end method

.method public getText()Ljava/lang/StringBuilder;
    .locals 12

    .prologue
    .line 394
    const/4 v2, 0x0

    .line 401
    .local v2, "extractedText":Ljava/lang/StringBuilder;
    iget-object v9, p0, Lorg/icepdf/index/core/pobjects/Page;->contents:Ljava/util/Vector;

    if-nez v9, :cond_0

    .line 403
    invoke-direct {p0}, Lorg/icepdf/index/core/pobjects/Page;->initPageContents()V

    .line 406
    :cond_0
    iget-object v9, p0, Lorg/icepdf/index/core/pobjects/Page;->resources:Lorg/icepdf/index/core/pobjects/Resources;

    if-nez v9, :cond_1

    .line 408
    invoke-direct {p0}, Lorg/icepdf/index/core/pobjects/Page;->initPageResources()V

    .line 411
    :cond_1
    iget-object v9, p0, Lorg/icepdf/index/core/pobjects/Page;->contents:Ljava/util/Vector;

    if-eqz v9, :cond_3

    .line 412
    new-instance v4, Ljava/util/Vector;

    iget-object v9, p0, Lorg/icepdf/index/core/pobjects/Page;->contents:Ljava/util/Vector;

    invoke-virtual {v9}, Ljava/util/Vector;->size()I

    move-result v9

    invoke-direct {v4, v9}, Ljava/util/Vector;-><init>(I)V

    .line 414
    .local v4, "inputStreamsVec":Ljava/util/Vector;, "Ljava/util/Vector<Ljava/io/InputStream;>;"
    const/4 v7, 0x0

    .local v7, "st":I
    iget-object v9, p0, Lorg/icepdf/index/core/pobjects/Page;->contents:Ljava/util/Vector;

    invoke-virtual {v9}, Ljava/util/Vector;->size()I

    move-result v5

    .local v5, "max":I
    :goto_0
    if-ge v7, v5, :cond_2

    .line 415
    iget-object v9, p0, Lorg/icepdf/index/core/pobjects/Page;->contents:Ljava/util/Vector;

    invoke-virtual {v9, v7}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lorg/icepdf/index/core/pobjects/Stream;

    .line 416
    .local v8, "stream":Lorg/icepdf/index/core/pobjects/Stream;
    invoke-virtual {v8}, Lorg/icepdf/index/core/pobjects/Stream;->getInputStreamForDecodedStreamBytes()Ljava/io/InputStream;

    move-result-object v3

    .line 418
    .local v3, "input":Ljava/io/InputStream;
    invoke-virtual {v4, v3}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 414
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 420
    .end local v3    # "input":Ljava/io/InputStream;
    .end local v8    # "stream":Lorg/icepdf/index/core/pobjects/Stream;
    :cond_2
    new-instance v6, Lorg/icepdf/index/core/io/SequenceInputStream;

    invoke-virtual {v4}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v9

    invoke-direct {v6, v9}, Lorg/icepdf/index/core/io/SequenceInputStream;-><init>(Ljava/util/Iterator;)V

    .line 427
    .local v6, "sis":Lorg/icepdf/index/core/io/SequenceInputStream;
    :try_start_0
    new-instance v0, Lorg/icepdf/index/core/util/ContentParser;

    iget-object v9, p0, Lorg/icepdf/index/core/pobjects/Page;->library:Lorg/icepdf/index/core/util/Library;

    iget-object v10, p0, Lorg/icepdf/index/core/pobjects/Page;->resources:Lorg/icepdf/index/core/pobjects/Resources;

    invoke-direct {v0, v9, v10}, Lorg/icepdf/index/core/util/ContentParser;-><init>(Lorg/icepdf/index/core/util/Library;Lorg/icepdf/index/core/pobjects/Resources;)V

    .line 429
    .local v0, "cp":Lorg/icepdf/index/core/util/ContentParser;
    invoke-virtual {v0, v6}, Lorg/icepdf/index/core/util/ContentParser;->parseTextBlocks(Ljava/io/InputStream;)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 437
    :try_start_1
    invoke-virtual {v6}, Lorg/icepdf/index/core/io/SequenceInputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 443
    .end local v0    # "cp":Lorg/icepdf/index/core/util/ContentParser;
    .end local v4    # "inputStreamsVec":Ljava/util/Vector;, "Ljava/util/Vector<Ljava/io/InputStream;>;"
    .end local v5    # "max":I
    .end local v6    # "sis":Lorg/icepdf/index/core/io/SequenceInputStream;
    .end local v7    # "st":I
    :cond_3
    :goto_1
    return-object v2

    .line 430
    .restart local v4    # "inputStreamsVec":Ljava/util/Vector;, "Ljava/util/Vector<Ljava/io/InputStream;>;"
    .restart local v5    # "max":I
    .restart local v6    # "sis":Lorg/icepdf/index/core/io/SequenceInputStream;
    .restart local v7    # "st":I
    :catch_0
    move-exception v1

    .line 431
    .local v1, "e":Ljava/lang/Exception;
    :try_start_2
    const-string/jumbo v9, "Page"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "Exception :"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 437
    :try_start_3
    invoke-virtual {v6}, Lorg/icepdf/index/core/io/SequenceInputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_1

    .line 438
    :catch_1
    move-exception v9

    goto :goto_1

    .line 436
    .end local v1    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v9

    .line 437
    :try_start_4
    invoke-virtual {v6}, Lorg/icepdf/index/core/io/SequenceInputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    .line 440
    :goto_2
    throw v9

    .line 438
    .restart local v0    # "cp":Lorg/icepdf/index/core/util/ContentParser;
    :catch_2
    move-exception v9

    goto :goto_1

    .end local v0    # "cp":Lorg/icepdf/index/core/util/ContentParser;
    :catch_3
    move-exception v10

    goto :goto_2
.end method

.method public getTextContent()Ljava/lang/String;
    .locals 12

    .prologue
    .line 316
    const/4 v5, 0x0

    .line 318
    .local v5, "returnText":Ljava/lang/String;
    iget-boolean v9, p0, Lorg/icepdf/index/core/pobjects/Page;->isInited:Z

    if-eqz v9, :cond_0

    .line 319
    iget-object v9, p0, Lorg/icepdf/index/core/pobjects/Page;->shapes:Lorg/icepdf/index/core/pobjects/graphics/Shapes;

    if-eqz v9, :cond_0

    iget-object v9, p0, Lorg/icepdf/index/core/pobjects/Page;->shapes:Lorg/icepdf/index/core/pobjects/graphics/Shapes;

    invoke-virtual {v9}, Lorg/icepdf/index/core/pobjects/graphics/Shapes;->getPageText()Lorg/icepdf/index/core/pobjects/graphics/text/PageText;

    move-result-object v9

    if-eqz v9, :cond_0

    .line 321
    iget-object v9, p0, Lorg/icepdf/index/core/pobjects/Page;->shapes:Lorg/icepdf/index/core/pobjects/graphics/Shapes;

    invoke-virtual {v9}, Lorg/icepdf/index/core/pobjects/graphics/Shapes;->getPageText()Lorg/icepdf/index/core/pobjects/graphics/text/PageText;

    move-result-object v9

    invoke-virtual {v9}, Lorg/icepdf/index/core/pobjects/graphics/text/PageText;->toString()Ljava/lang/String;

    move-result-object v9

    .line 375
    :goto_0
    return-object v9

    .line 330
    :cond_0
    iget-object v9, p0, Lorg/icepdf/index/core/pobjects/Page;->contents:Ljava/util/Vector;

    if-nez v9, :cond_1

    .line 332
    invoke-direct {p0}, Lorg/icepdf/index/core/pobjects/Page;->initPageContents()V

    .line 335
    :cond_1
    iget-object v9, p0, Lorg/icepdf/index/core/pobjects/Page;->resources:Lorg/icepdf/index/core/pobjects/Resources;

    if-nez v9, :cond_2

    .line 337
    invoke-direct {p0}, Lorg/icepdf/index/core/pobjects/Page;->initPageResources()V

    .line 340
    :cond_2
    iget-object v9, p0, Lorg/icepdf/index/core/pobjects/Page;->contents:Ljava/util/Vector;

    if-eqz v9, :cond_4

    .line 341
    new-instance v3, Ljava/util/Vector;

    iget-object v9, p0, Lorg/icepdf/index/core/pobjects/Page;->contents:Ljava/util/Vector;

    invoke-virtual {v9}, Ljava/util/Vector;->size()I

    move-result v9

    invoke-direct {v3, v9}, Ljava/util/Vector;-><init>(I)V

    .line 343
    .local v3, "inputStreamsVec":Ljava/util/Vector;, "Ljava/util/Vector<Ljava/io/InputStream;>;"
    const/4 v7, 0x0

    .local v7, "st":I
    iget-object v9, p0, Lorg/icepdf/index/core/pobjects/Page;->contents:Ljava/util/Vector;

    invoke-virtual {v9}, Ljava/util/Vector;->size()I

    move-result v4

    .local v4, "max":I
    :goto_1
    if-ge v7, v4, :cond_3

    .line 344
    iget-object v9, p0, Lorg/icepdf/index/core/pobjects/Page;->contents:Ljava/util/Vector;

    invoke-virtual {v9, v7}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lorg/icepdf/index/core/pobjects/Stream;

    .line 345
    .local v8, "stream":Lorg/icepdf/index/core/pobjects/Stream;
    invoke-virtual {v8}, Lorg/icepdf/index/core/pobjects/Stream;->getInputStreamForDecodedStreamBytes()Ljava/io/InputStream;

    move-result-object v2

    .line 346
    .local v2, "input":Ljava/io/InputStream;
    invoke-virtual {v3, v2}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 343
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 348
    .end local v2    # "input":Ljava/io/InputStream;
    .end local v8    # "stream":Lorg/icepdf/index/core/pobjects/Stream;
    :cond_3
    new-instance v6, Lorg/icepdf/index/core/io/SequenceInputStream;

    invoke-virtual {v3}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v9

    invoke-direct {v6, v9}, Lorg/icepdf/index/core/io/SequenceInputStream;-><init>(Ljava/util/Iterator;)V

    .line 354
    .local v6, "sis":Lorg/icepdf/index/core/io/SequenceInputStream;
    :try_start_0
    new-instance v0, Lorg/icepdf/index/core/util/ContentParser;

    iget-object v9, p0, Lorg/icepdf/index/core/pobjects/Page;->library:Lorg/icepdf/index/core/util/Library;

    iget-object v10, p0, Lorg/icepdf/index/core/pobjects/Page;->resources:Lorg/icepdf/index/core/pobjects/Resources;

    invoke-direct {v0, v9, v10}, Lorg/icepdf/index/core/util/ContentParser;-><init>(Lorg/icepdf/index/core/util/Library;Lorg/icepdf/index/core/pobjects/Resources;)V

    .line 356
    .local v0, "cp":Lorg/icepdf/index/core/util/ContentParser;
    invoke-virtual {v0, v6}, Lorg/icepdf/index/core/util/ContentParser;->parseTextContent(Ljava/io/InputStream;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v5

    .line 367
    :try_start_1
    invoke-virtual {v6}, Lorg/icepdf/index/core/io/SequenceInputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .end local v0    # "cp":Lorg/icepdf/index/core/util/ContentParser;
    .end local v3    # "inputStreamsVec":Ljava/util/Vector;, "Ljava/util/Vector<Ljava/io/InputStream;>;"
    .end local v4    # "max":I
    .end local v6    # "sis":Lorg/icepdf/index/core/io/SequenceInputStream;
    .end local v7    # "st":I
    :cond_4
    :goto_2
    move-object v9, v5

    .line 375
    goto :goto_0

    .line 358
    .restart local v3    # "inputStreamsVec":Ljava/util/Vector;, "Ljava/util/Vector<Ljava/io/InputStream;>;"
    .restart local v4    # "max":I
    .restart local v6    # "sis":Lorg/icepdf/index/core/io/SequenceInputStream;
    .restart local v7    # "st":I
    :catch_0
    move-exception v1

    .line 359
    .local v1, "e":Ljava/lang/Exception;
    :try_start_2
    const-string/jumbo v9, "Page"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "Exception :"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 361
    instance-of v9, v1, Ljava/lang/StringIndexOutOfBoundsException;

    if-eqz v9, :cond_5

    .line 362
    check-cast v1, Ljava/lang/StringIndexOutOfBoundsException;

    .end local v1    # "e":Ljava/lang/Exception;
    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 366
    :catchall_0
    move-exception v9

    .line 367
    :try_start_3
    invoke-virtual {v6}, Lorg/icepdf/index/core/io/SequenceInputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3

    .line 371
    :goto_3
    throw v9

    .line 367
    .restart local v1    # "e":Ljava/lang/Exception;
    :cond_5
    :try_start_4
    invoke-virtual {v6}, Lorg/icepdf/index/core/io/SequenceInputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_2

    .line 369
    :catch_1
    move-exception v9

    goto :goto_2

    .end local v1    # "e":Ljava/lang/Exception;
    .restart local v0    # "cp":Lorg/icepdf/index/core/util/ContentParser;
    :catch_2
    move-exception v9

    goto :goto_2

    .end local v0    # "cp":Lorg/icepdf/index/core/util/ContentParser;
    :catch_3
    move-exception v10

    goto :goto_3
.end method

.method public isInitiated()Z
    .locals 1

    .prologue
    .line 210
    iget-boolean v0, p0, Lorg/icepdf/index/core/pobjects/Page;->isInited:Z

    return v0
.end method

.method public reduceMemory()V
    .locals 1

    .prologue
    .line 457
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lorg/icepdf/index/core/pobjects/Page;->dispose(Z)V

    .line 458
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 286
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "PAGE= "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/icepdf/index/core/pobjects/Page;->entries:Lorg/icepdf/index/core/util/Hashtable;

    invoke-virtual {v1}, Lorg/icepdf/index/core/util/Hashtable;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
