.class public abstract Lorg/icepdf/index/core/pobjects/security/SecurityHandler;
.super Ljava/lang/Object;
.source "SecurityHandler.java"

# interfaces
.implements Lorg/icepdf/index/core/pobjects/security/SecurityHandlerInterface;


# instance fields
.field protected encryptionDictionary:Lorg/icepdf/index/core/pobjects/security/EncryptionDictionary;

.field protected handlerName:Ljava/lang/String;

.field protected permissions:Lorg/icepdf/index/core/pobjects/security/Permissions;


# direct methods
.method public constructor <init>(Lorg/icepdf/index/core/pobjects/security/EncryptionDictionary;)V
    .locals 1
    .param p1, "encryptionDictionary"    # Lorg/icepdf/index/core/pobjects/security/EncryptionDictionary;

    .prologue
    const/4 v0, 0x0

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object v0, p0, Lorg/icepdf/index/core/pobjects/security/SecurityHandler;->handlerName:Ljava/lang/String;

    .line 31
    iput-object v0, p0, Lorg/icepdf/index/core/pobjects/security/SecurityHandler;->encryptionDictionary:Lorg/icepdf/index/core/pobjects/security/EncryptionDictionary;

    .line 33
    iput-object v0, p0, Lorg/icepdf/index/core/pobjects/security/SecurityHandler;->permissions:Lorg/icepdf/index/core/pobjects/security/Permissions;

    .line 36
    iput-object p1, p0, Lorg/icepdf/index/core/pobjects/security/SecurityHandler;->encryptionDictionary:Lorg/icepdf/index/core/pobjects/security/EncryptionDictionary;

    .line 37
    return-void
.end method


# virtual methods
.method public abstract decrypt(Lorg/icepdf/index/core/pobjects/Reference;[B[B)[B
.end method

.method public abstract dispose()V
.end method

.method public abstract encrypt(Lorg/icepdf/index/core/pobjects/Reference;[B[B)[B
.end method

.method public abstract getDecryptionKey()[B
.end method

.method public abstract getEncryptionInputStream(Lorg/icepdf/index/core/pobjects/Reference;[BLjava/util/Hashtable;Ljava/io/InputStream;)Ljava/io/InputStream;
.end method

.method public abstract getEncryptionKey()[B
.end method

.method public abstract getHandlerName()Ljava/lang/String;
.end method

.method public abstract getPermissions()Lorg/icepdf/index/core/pobjects/security/Permissions;
.end method

.method public abstract init()V
.end method

.method public abstract isAuthorized(Ljava/lang/String;)Z
.end method

.method public abstract isOwnerAuthorized(Ljava/lang/String;)Z
.end method

.method public abstract isUserAuthorized(Ljava/lang/String;)Z
.end method
