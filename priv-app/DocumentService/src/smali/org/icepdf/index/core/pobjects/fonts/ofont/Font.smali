.class public Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;
.super Lorg/icepdf/index/core/pobjects/fonts/Font;
.source "Font.java"


# static fields
.field private static final DEFAULT_FONT_SIZE:F = 12.0f

.field static final type1Diff:[[Ljava/lang/String;


# instance fields
.field protected afm:Lorg/icepdf/index/core/pobjects/fonts/AFM;

.field private cMap:[C

.field private cidWidths:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private encoding:Lorg/icepdf/index/core/pobjects/fonts/ofont/Encoding;

.field private encodingName:Ljava/lang/String;

.field fontnames:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;",
            ">;"
        }
    .end annotation
.end field

.field protected style:I

.field private toUnicodeCMap:Lorg/icepdf/index/core/pobjects/fonts/ofont/CMap;

.field private widths:Ljava/util/Vector;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 68
    const/16 v0, 0x27

    new-array v0, v0, [[Ljava/lang/String;

    new-array v1, v7, [Ljava/lang/String;

    const-string/jumbo v2, "Bookman-Demi"

    aput-object v2, v1, v4

    const-string/jumbo v2, "URWBookmanL-DemiBold"

    aput-object v2, v1, v5

    const-string/jumbo v2, "Arial"

    aput-object v2, v1, v6

    aput-object v1, v0, v4

    new-array v1, v7, [Ljava/lang/String;

    const-string/jumbo v2, "Bookman-DemiItalic"

    aput-object v2, v1, v4

    const-string/jumbo v2, "URWBookmanL-DemiBoldItal"

    aput-object v2, v1, v5

    const-string/jumbo v2, "Arial"

    aput-object v2, v1, v6

    aput-object v1, v0, v5

    new-array v1, v7, [Ljava/lang/String;

    const-string/jumbo v2, "Bookman-Light"

    aput-object v2, v1, v4

    const-string/jumbo v2, "URWBookmanL-Ligh"

    aput-object v2, v1, v5

    const-string/jumbo v2, "Arial"

    aput-object v2, v1, v6

    aput-object v1, v0, v6

    new-array v1, v7, [Ljava/lang/String;

    const-string/jumbo v2, "Bookman-LightItalic"

    aput-object v2, v1, v4

    const-string/jumbo v2, "URWBookmanL-LighItal"

    aput-object v2, v1, v5

    const-string/jumbo v2, "Arial"

    aput-object v2, v1, v6

    aput-object v1, v0, v7

    const/4 v1, 0x4

    new-array v2, v7, [Ljava/lang/String;

    const-string/jumbo v3, "Courier"

    aput-object v3, v2, v4

    const-string/jumbo v3, "Nimbus Mono L Regular"

    aput-object v3, v2, v5

    const-string/jumbo v3, "Nimbus Mono L"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/4 v1, 0x5

    new-array v2, v7, [Ljava/lang/String;

    const-string/jumbo v3, "Courier-Oblique"

    aput-object v3, v2, v4

    const-string/jumbo v3, "Nimbus Mono L Regular Oblique"

    aput-object v3, v2, v5

    const-string/jumbo v3, "Nimbus Mono L"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/4 v1, 0x6

    new-array v2, v7, [Ljava/lang/String;

    const-string/jumbo v3, "Courier-Bold"

    aput-object v3, v2, v4

    const-string/jumbo v3, "Nimbus Mono L Bold"

    aput-object v3, v2, v5

    const-string/jumbo v3, "Nimbus Mono L"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/4 v1, 0x7

    new-array v2, v7, [Ljava/lang/String;

    const-string/jumbo v3, "Courier-BoldOblique"

    aput-object v3, v2, v4

    const-string/jumbo v3, "Nimbus Mono L Bold Oblique"

    aput-object v3, v2, v5

    const-string/jumbo v3, "Nimbus Mono L"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x8

    new-array v2, v7, [Ljava/lang/String;

    const-string/jumbo v3, "AvantGarde-Book"

    aput-object v3, v2, v4

    const-string/jumbo v3, "URWGothicL-Book"

    aput-object v3, v2, v5

    const-string/jumbo v3, "Arial"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x9

    new-array v2, v7, [Ljava/lang/String;

    const-string/jumbo v3, "AvantGarde-BookOblique"

    aput-object v3, v2, v4

    const-string/jumbo v3, "URWGothicL-BookObli"

    aput-object v3, v2, v5

    const-string/jumbo v3, "Arial"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0xa

    new-array v2, v7, [Ljava/lang/String;

    const-string/jumbo v3, "AvantGarde-Demi"

    aput-object v3, v2, v4

    const-string/jumbo v3, "URWGothicL-Demi"

    aput-object v3, v2, v5

    const-string/jumbo v3, "Arial"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0xb

    new-array v2, v7, [Ljava/lang/String;

    const-string/jumbo v3, "AvantGarde-DemiOblique"

    aput-object v3, v2, v4

    const-string/jumbo v3, "URWGothicL-DemiObli"

    aput-object v3, v2, v5

    const-string/jumbo v3, "Arial"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0xc

    new-array v2, v7, [Ljava/lang/String;

    const-string/jumbo v3, "Helvetica"

    aput-object v3, v2, v4

    const-string/jumbo v3, "Nimbus Sans L Regular"

    aput-object v3, v2, v5

    const-string/jumbo v3, "Nimbus Sans L"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0xd

    new-array v2, v7, [Ljava/lang/String;

    const-string/jumbo v3, "Helvetica-Oblique"

    aput-object v3, v2, v4

    const-string/jumbo v3, "Nimbus Sans L Regular Italic"

    aput-object v3, v2, v5

    const-string/jumbo v3, "Nimbus Sans L"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0xe

    new-array v2, v7, [Ljava/lang/String;

    const-string/jumbo v3, "Helvetica-Bold"

    aput-object v3, v2, v4

    const-string/jumbo v3, "Nimbus Sans L Bold"

    aput-object v3, v2, v5

    const-string/jumbo v3, "Nimbus Sans L"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0xf

    new-array v2, v7, [Ljava/lang/String;

    const-string/jumbo v3, "Helvetica-BoldOblique"

    aput-object v3, v2, v4

    const-string/jumbo v3, "Nimbus Sans L Bold Italic"

    aput-object v3, v2, v5

    const-string/jumbo v3, "Nimbus Sans L"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x10

    new-array v2, v7, [Ljava/lang/String;

    const-string/jumbo v3, "Helvetica-Narrow"

    aput-object v3, v2, v4

    const-string/jumbo v3, "Nimbus Sans L Regular Condensed"

    aput-object v3, v2, v5

    const-string/jumbo v3, "Nimbus Sans L"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x11

    new-array v2, v7, [Ljava/lang/String;

    const-string/jumbo v3, "Helvetica-Narrow-Oblique"

    aput-object v3, v2, v4

    const-string/jumbo v3, "Nimbus Sans L Regular Condensed Italic"

    aput-object v3, v2, v5

    const-string/jumbo v3, "Nimbus Sans L"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x12

    new-array v2, v7, [Ljava/lang/String;

    const-string/jumbo v3, "Helvetica-Narrow-Bold"

    aput-object v3, v2, v4

    const-string/jumbo v3, "Nimbus Sans L Bold Condensed"

    aput-object v3, v2, v5

    const-string/jumbo v3, "Nimbus Sans L"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x13

    new-array v2, v7, [Ljava/lang/String;

    const-string/jumbo v3, "Helvetica-Narrow-BoldOblique"

    aput-object v3, v2, v4

    const-string/jumbo v3, "Nimbus Sans L Bold Condensed Italic"

    aput-object v3, v2, v5

    const-string/jumbo v3, "Nimbus Sans L"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x14

    new-array v2, v7, [Ljava/lang/String;

    const-string/jumbo v3, "Helvetica-Condensed"

    aput-object v3, v2, v4

    const-string/jumbo v3, "Nimbus Sans L Regular Condensed"

    aput-object v3, v2, v5

    const-string/jumbo v3, "Nimbus Sans L"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x15

    new-array v2, v7, [Ljava/lang/String;

    const-string/jumbo v3, "Helvetica-Condensed-Oblique"

    aput-object v3, v2, v4

    const-string/jumbo v3, "Nimbus Sans L Regular Condensed Italic"

    aput-object v3, v2, v5

    const-string/jumbo v3, "Nimbus Sans L"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x16

    new-array v2, v7, [Ljava/lang/String;

    const-string/jumbo v3, "Helvetica-Condensed-Bold"

    aput-object v3, v2, v4

    const-string/jumbo v3, "Nimbus Sans L Bold Condensed"

    aput-object v3, v2, v5

    const-string/jumbo v3, "Nimbus Sans L"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x17

    new-array v2, v7, [Ljava/lang/String;

    const-string/jumbo v3, "Helvetica-Condensed-BoldOblique"

    aput-object v3, v2, v4

    const-string/jumbo v3, "Nimbus Sans L Bold Condensed Italic"

    aput-object v3, v2, v5

    const-string/jumbo v3, "Nimbus Sans L"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x18

    new-array v2, v7, [Ljava/lang/String;

    const-string/jumbo v3, "Palatino-Roman"

    aput-object v3, v2, v4

    const-string/jumbo v3, "URWPalladioL-Roma"

    aput-object v3, v2, v5

    const-string/jumbo v3, "Arial"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x19

    new-array v2, v7, [Ljava/lang/String;

    const-string/jumbo v3, "Palatino-Italic"

    aput-object v3, v2, v4

    const-string/jumbo v3, "URWPalladioL-Ital"

    aput-object v3, v2, v5

    const-string/jumbo v3, "Arial"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    new-array v2, v7, [Ljava/lang/String;

    const-string/jumbo v3, "Palatino-Bold"

    aput-object v3, v2, v4

    const-string/jumbo v3, "URWPalladioL-Bold"

    aput-object v3, v2, v5

    const-string/jumbo v3, "Arial"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    new-array v2, v7, [Ljava/lang/String;

    const-string/jumbo v3, "Palatino-BoldItalic"

    aput-object v3, v2, v4

    const-string/jumbo v3, "URWPalladioL-BoldItal"

    aput-object v3, v2, v5

    const-string/jumbo v3, "Arial"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    new-array v2, v7, [Ljava/lang/String;

    const-string/jumbo v3, "NewCenturySchlbk-Roman"

    aput-object v3, v2, v4

    const-string/jumbo v3, "CenturySchL-Roma"

    aput-object v3, v2, v5

    const-string/jumbo v3, "Arial"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    new-array v2, v7, [Ljava/lang/String;

    const-string/jumbo v3, "NewCenturySchlbk-Italic"

    aput-object v3, v2, v4

    const-string/jumbo v3, "CenturySchL-Ital"

    aput-object v3, v2, v5

    const-string/jumbo v3, "Arial"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    new-array v2, v7, [Ljava/lang/String;

    const-string/jumbo v3, "NewCenturySchlbk-Bold"

    aput-object v3, v2, v4

    const-string/jumbo v3, "CenturySchL-Bold"

    aput-object v3, v2, v5

    const-string/jumbo v3, "Arial"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    new-array v2, v7, [Ljava/lang/String;

    const-string/jumbo v3, "NewCenturySchlbk-BoldItalic"

    aput-object v3, v2, v4

    const-string/jumbo v3, "CenturySchL-BoldItal"

    aput-object v3, v2, v5

    const-string/jumbo v3, "Arial"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x20

    new-array v2, v7, [Ljava/lang/String;

    const-string/jumbo v3, "Times-Roman"

    aput-object v3, v2, v4

    const-string/jumbo v3, "Nimbus Roman No9 L Regular"

    aput-object v3, v2, v5

    const-string/jumbo v3, "Nimbus Roman No9 L"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x21

    new-array v2, v7, [Ljava/lang/String;

    const-string/jumbo v3, "Times-Italic"

    aput-object v3, v2, v4

    const-string/jumbo v3, "Nimbus Roman No9 L Regular Italic"

    aput-object v3, v2, v5

    const-string/jumbo v3, "Nimbus Roman No9 L"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x22

    new-array v2, v7, [Ljava/lang/String;

    const-string/jumbo v3, "Times-Bold"

    aput-object v3, v2, v4

    const-string/jumbo v3, "Nimbus Roman No9 L Medium"

    aput-object v3, v2, v5

    const-string/jumbo v3, "Nimbus Roman No9 L"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x23

    new-array v2, v7, [Ljava/lang/String;

    const-string/jumbo v3, "Times-BoldItalic"

    aput-object v3, v2, v4

    const-string/jumbo v3, "Nimbus Roman No9 L Medium Italic"

    aput-object v3, v2, v5

    const-string/jumbo v3, "Nimbus Roman No9 L"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x24

    new-array v2, v7, [Ljava/lang/String;

    const-string/jumbo v3, "Symbol"

    aput-object v3, v2, v4

    const-string/jumbo v3, "Standard Symbols L"

    aput-object v3, v2, v5

    const-string/jumbo v3, "Standard Symbols L"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x25

    new-array v2, v7, [Ljava/lang/String;

    const-string/jumbo v3, "ZapfChancery-MediumItalic"

    aput-object v3, v2, v4

    const-string/jumbo v3, "URWChanceryL-MediItal"

    aput-object v3, v2, v5

    const-string/jumbo v3, "Arial"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x26

    new-array v2, v7, [Ljava/lang/String;

    const-string/jumbo v3, "ZapfDingbats"

    aput-object v3, v2, v4

    const-string/jumbo v3, "Dingbats"

    aput-object v3, v2, v5

    const-string/jumbo v3, "Dingbats"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    sput-object v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->type1Diff:[[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lorg/icepdf/index/core/util/Library;Lorg/icepdf/index/core/util/Hashtable;)V
    .locals 9
    .param p1, "library"    # Lorg/icepdf/index/core/util/Library;
    .param p2, "entries"    # Lorg/icepdf/index/core/util/Hashtable;

    .prologue
    const/16 v7, 0x100

    const/4 v8, 0x0

    .line 158
    invoke-direct {p0, p1, p2}, Lorg/icepdf/index/core/pobjects/fonts/Font;-><init>(Lorg/icepdf/index/core/util/Library;Lorg/icepdf/index/core/util/Hashtable;)V

    .line 66
    iput v8, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->style:I

    .line 225
    new-instance v6, Ljava/util/HashSet;

    invoke-direct {v6}, Ljava/util/HashSet;-><init>()V

    iput-object v6, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->fontnames:Ljava/util/Collection;

    .line 161
    new-array v6, v7, [C

    iput-object v6, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->cMap:[C

    .line 162
    const/4 v2, 0x0

    .local v2, "i":C
    :goto_0
    if-ge v2, v7, :cond_0

    .line 163
    iget-object v6, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->cMap:[C

    aput-char v2, v6, v2

    .line 162
    add-int/lit8 v6, v2, 0x1

    int-to-char v2, v6

    goto :goto_0

    .line 167
    :cond_0
    iget-object v6, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->basefont:Ljava/lang/String;

    invoke-static {v6}, Lorg/icepdf/index/core/util/FontUtil;->guessAndroidFontStyle(Ljava/lang/String;)I

    move-result v6

    iput v6, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->style:I

    .line 170
    const-string/jumbo v6, "Name"

    invoke-virtual {p1, p2, v6}, Lorg/icepdf/index/core/util/Library;->getName(Lorg/icepdf/index/core/util/Hashtable;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->name:Ljava/lang/String;

    .line 173
    const-string/jumbo v6, "Subtype"

    invoke-virtual {p1, p2, v6}, Lorg/icepdf/index/core/util/Library;->getName(Lorg/icepdf/index/core/util/Hashtable;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->subtype:Ljava/lang/String;

    .line 177
    const-string/jumbo v6, "Serif"

    iput-object v6, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->basefont:Ljava/lang/String;

    .line 178
    const-string/jumbo v6, "BaseFont"

    invoke-virtual {p2, v6}, Lorg/icepdf/index/core/util/Hashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 179
    const-string/jumbo v6, "BaseFont"

    invoke-virtual {p2, v6}, Lorg/icepdf/index/core/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    .line 180
    .local v5, "o":Ljava/lang/Object;
    instance-of v6, v5, Lorg/icepdf/index/core/pobjects/Name;

    if-eqz v6, :cond_1

    .line 181
    check-cast v5, Lorg/icepdf/index/core/pobjects/Name;

    .end local v5    # "o":Ljava/lang/Object;
    invoke-virtual {v5}, Lorg/icepdf/index/core/pobjects/Name;->getName()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->basefont:Ljava/lang/String;

    .line 186
    :cond_1
    iget-object v6, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->basefont:Ljava/lang/String;

    invoke-direct {p0, v6}, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->cleanFontName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->basefont:Ljava/lang/String;

    .line 189
    iget-object v6, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->subtype:Ljava/lang/String;

    const-string/jumbo v7, "Type3"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 190
    const-string/jumbo v6, "Symbol"

    iput-object v6, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->basefont:Ljava/lang/String;

    .line 191
    invoke-static {}, Lorg/icepdf/index/core/pobjects/fonts/ofont/Encoding;->getSymbol()Lorg/icepdf/index/core/pobjects/fonts/ofont/Encoding;

    move-result-object v6

    iput-object v6, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->encoding:Lorg/icepdf/index/core/pobjects/fonts/ofont/Encoding;

    .line 196
    :cond_2
    iget-object v6, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->subtype:Ljava/lang/String;

    const-string/jumbo v7, "Type1"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    iget-object v6, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->basefont:Ljava/lang/String;

    if-eqz v6, :cond_3

    .line 198
    iget-object v6, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->basefont:Ljava/lang/String;

    const-string/jumbo v7, "Symbol"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 199
    invoke-static {}, Lorg/icepdf/index/core/pobjects/fonts/ofont/Encoding;->getSymbol()Lorg/icepdf/index/core/pobjects/fonts/ofont/Encoding;

    move-result-object v6

    iput-object v6, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->encoding:Lorg/icepdf/index/core/pobjects/fonts/ofont/Encoding;

    .line 216
    :cond_3
    :goto_1
    iget-object v6, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->subtype:Ljava/lang/String;

    const-string/jumbo v7, "TrueType"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 217
    iget-object v6, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->basefont:Ljava/lang/String;

    if-eqz v6, :cond_4

    iget-object v6, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->basefont:Ljava/lang/String;

    const-string/jumbo v7, "Symbol"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 218
    const-string/jumbo v6, "winAnsi"

    iput-object v6, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->encodingName:Ljava/lang/String;

    .line 219
    invoke-static {}, Lorg/icepdf/index/core/pobjects/fonts/ofont/Encoding;->getWinAnsi()Lorg/icepdf/index/core/pobjects/fonts/ofont/Encoding;

    move-result-object v6

    iput-object v6, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->encoding:Lorg/icepdf/index/core/pobjects/fonts/ofont/Encoding;

    .line 223
    :cond_4
    return-void

    .line 201
    :cond_5
    iget-object v6, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->basefont:Ljava/lang/String;

    const-string/jumbo v7, "ZapfDingbats"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_6

    iget-object v6, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->subtype:Ljava/lang/String;

    const-string/jumbo v7, "Type1"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 203
    invoke-static {}, Lorg/icepdf/index/core/pobjects/fonts/ofont/Encoding;->getZapfDingBats()Lorg/icepdf/index/core/pobjects/fonts/ofont/Encoding;

    move-result-object v6

    iput-object v6, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->encoding:Lorg/icepdf/index/core/pobjects/fonts/ofont/Encoding;

    goto :goto_1

    .line 206
    :cond_6
    sget-object v1, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->type1Diff:[[Ljava/lang/String;

    .local v1, "arr$":[[Ljava/lang/String;
    array-length v4, v1

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_2
    if-ge v3, v4, :cond_3

    aget-object v0, v1, v3

    .line 207
    .local v0, "aType1Diff":[Ljava/lang/String;
    iget-object v6, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->basefont:Ljava/lang/String;

    aget-object v7, v0, v8

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 208
    const-string/jumbo v6, "standard"

    iput-object v6, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->encodingName:Ljava/lang/String;

    .line 209
    invoke-static {}, Lorg/icepdf/index/core/pobjects/fonts/ofont/Encoding;->getStandard()Lorg/icepdf/index/core/pobjects/fonts/ofont/Encoding;

    move-result-object v6

    iput-object v6, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->encoding:Lorg/icepdf/index/core/pobjects/fonts/ofont/Encoding;

    goto :goto_1

    .line 206
    :cond_7
    add-int/lit8 v3, v3, 0x1

    goto :goto_2
.end method

.method private calculateCIDWidths()Ljava/util/Map;
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation

    .prologue
    const/high16 v14, 0x447a0000    # 1000.0f

    .line 690
    new-instance v1, Ljava/util/HashMap;

    const/16 v11, 0x4b

    invoke-direct {v1, v11}, Ljava/util/HashMap;-><init>(I)V

    .line 692
    .local v1, "cidWidths":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Integer;Ljava/lang/Float;>;"
    iget-object v11, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->library:Lorg/icepdf/index/core/util/Library;

    iget-object v12, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->entries:Lorg/icepdf/index/core/util/Hashtable;

    const-string/jumbo v13, "W"

    invoke-virtual {v11, v12, v13}, Lorg/icepdf/index/core/util/Library;->getObject(Lorg/icepdf/index/core/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    .line 693
    .local v7, "o":Ljava/lang/Object;
    instance-of v11, v7, Ljava/util/Vector;

    if-eqz v11, :cond_6

    move-object v0, v7

    .line 694
    check-cast v0, Ljava/util/Vector;

    .line 701
    .local v0, "cidWidth":Ljava/util/Vector;
    const/4 v4, 0x0

    .local v4, "i":I
    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v11

    add-int/lit8 v6, v11, -0x2

    .local v6, "max":I
    :goto_0
    if-ge v4, v6, :cond_6

    .line 702
    invoke-virtual {v0, v4}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v2

    .line 703
    .local v2, "current":Ljava/lang/Object;
    add-int/lit8 v11, v4, 0x1

    invoke-virtual {v0, v11}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v8

    .line 705
    .local v8, "peek":Ljava/lang/Object;
    instance-of v11, v2, Ljava/lang/Integer;

    if-eqz v11, :cond_3

    instance-of v11, v8, Ljava/util/Vector;

    if-eqz v11, :cond_3

    move-object v11, v2

    .line 708
    check-cast v11, Ljava/lang/Integer;

    invoke-virtual {v11}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .local v3, "currentChar":I
    move-object v10, v8

    .line 709
    check-cast v10, Ljava/util/Vector;

    .line 710
    .local v10, "subWidth":Ljava/util/Vector;
    const/4 v5, 0x0

    .local v5, "j":I
    invoke-virtual {v10}, Ljava/util/Vector;->size()I

    move-result v9

    .local v9, "subMax":I
    :goto_1
    if-ge v5, v9, :cond_2

    .line 711
    invoke-virtual {v10, v5}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v11

    instance-of v11, v11, Ljava/lang/Integer;

    if-eqz v11, :cond_1

    .line 712
    add-int v11, v3, v5

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v10, v5}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/Integer;

    invoke-virtual {v11}, Ljava/lang/Integer;->intValue()I

    move-result v11

    int-to-float v11, v11

    div-float/2addr v11, v14

    invoke-static {v11}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v11

    invoke-virtual {v1, v12, v11}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 710
    :cond_0
    :goto_2
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 713
    :cond_1
    invoke-virtual {v10, v5}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v11

    instance-of v11, v11, Ljava/lang/Float;

    if-eqz v11, :cond_0

    .line 714
    add-int v11, v3, v5

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v10, v5}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/Float;

    invoke-virtual {v11}, Ljava/lang/Float;->floatValue()F

    move-result v11

    div-float/2addr v11, v14

    invoke-static {v11}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v11

    invoke-virtual {v1, v12, v11}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 717
    :cond_2
    add-int/lit8 v4, v4, 0x1

    .line 719
    .end local v3    # "currentChar":I
    .end local v5    # "j":I
    .end local v9    # "subMax":I
    .end local v10    # "subWidth":Ljava/util/Vector;
    :cond_3
    instance-of v11, v2, Ljava/lang/Integer;

    if-eqz v11, :cond_5

    instance-of v11, v8, Ljava/lang/Integer;

    if-eqz v11, :cond_5

    .line 721
    check-cast v2, Ljava/lang/Integer;

    .end local v2    # "current":Ljava/lang/Object;
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v5

    .restart local v5    # "j":I
    :goto_3
    move-object v11, v8

    check-cast v11, Ljava/lang/Integer;

    invoke-virtual {v11}, Ljava/lang/Integer;->intValue()I

    move-result v11

    if-gt v5, v11, :cond_4

    .line 723
    move v3, v5

    .line 724
    .restart local v3    # "currentChar":I
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    add-int/lit8 v11, v4, 0x2

    invoke-virtual {v0, v11}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/Integer;

    invoke-virtual {v11}, Ljava/lang/Integer;->intValue()I

    move-result v11

    int-to-float v11, v11

    div-float/2addr v11, v14

    invoke-static {v11}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v11

    invoke-virtual {v1, v12, v11}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 721
    add-int/lit8 v5, v5, 0x1

    goto :goto_3

    .line 726
    .end local v3    # "currentChar":I
    :cond_4
    add-int/lit8 v4, v4, 0x2

    .line 701
    .end local v5    # "j":I
    :cond_5
    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_0

    .line 730
    .end local v0    # "cidWidth":Ljava/util/Vector;
    .end local v4    # "i":I
    .end local v6    # "max":I
    .end local v8    # "peek":Ljava/lang/Object;
    :cond_6
    return-object v1
.end method

.method private cleanFontName(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p1, "fontName"    # Ljava/lang/String;

    .prologue
    const/16 v4, 0x2b

    .line 653
    if-nez p1, :cond_0

    .line 654
    const/4 v2, 0x0

    .line 686
    :goto_0
    return-object v2

    .line 659
    :cond_0
    invoke-virtual {p1, v4}, Ljava/lang/String;->indexOf(I)I

    move-result v2

    if-ltz v2, :cond_1

    .line 660
    invoke-virtual {p1, v4}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    .line 661
    .local v0, "index":I
    add-int/lit8 v2, v0, 0x1

    invoke-virtual {p1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 663
    .local v1, "tmp":Ljava/lang/String;
    :try_start_0
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 664
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object p1

    .line 673
    .end local v0    # "index":I
    .end local v1    # "tmp":Ljava/lang/String;
    :cond_1
    :goto_1
    invoke-virtual {p1, v4}, Ljava/lang/String;->indexOf(I)I

    move-result v2

    if-ltz v2, :cond_2

    .line 674
    invoke-virtual {p1, v4}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    .line 676
    .restart local v0    # "index":I
    add-int/lit8 v2, v0, 0x1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    .line 678
    goto :goto_1

    .line 681
    .end local v0    # "index":I
    :cond_2
    iget-object v2, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->subtype:Ljava/lang/String;

    const-string/jumbo v3, "Type0"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    iget-object v2, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->subtype:Ljava/lang/String;

    const-string/jumbo v3, "Type1"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    iget-object v2, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->subtype:Ljava/lang/String;

    const-string/jumbo v3, "MMType1"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    iget-object v2, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->subtype:Ljava/lang/String;

    const-string/jumbo v3, "TrueType"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 683
    :cond_3
    const/16 v2, 0x2c

    const/16 v3, 0x2d

    invoke-virtual {p1, v2, v3}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object p1

    :cond_4
    move-object v2, p1

    .line 686
    goto :goto_0

    .line 665
    .restart local v0    # "index":I
    .restart local v1    # "tmp":Ljava/lang/String;
    :catch_0
    move-exception v2

    goto :goto_1
.end method

.method private setBaseEncoding(Ljava/lang/String;)V
    .locals 3
    .param p1, "baseencoding"    # Ljava/lang/String;

    .prologue
    .line 556
    if-nez p1, :cond_1

    .line 557
    const-string/jumbo v1, "none"

    iput-object v1, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->encodingName:Ljava/lang/String;

    .line 578
    :cond_0
    return-void

    .line 559
    :cond_1
    const-string/jumbo v1, "StandardEncoding"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 560
    const-string/jumbo v1, "StandardEncoding"

    iput-object v1, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->encodingName:Ljava/lang/String;

    .line 561
    invoke-static {}, Lorg/icepdf/index/core/pobjects/fonts/ofont/Encoding;->getStandard()Lorg/icepdf/index/core/pobjects/fonts/ofont/Encoding;

    move-result-object v1

    iput-object v1, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->encoding:Lorg/icepdf/index/core/pobjects/fonts/ofont/Encoding;

    .line 573
    :cond_2
    :goto_0
    iget-object v1, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->encoding:Lorg/icepdf/index/core/pobjects/fonts/ofont/Encoding;

    if-eqz v1, :cond_0

    .line 574
    const/4 v0, 0x0

    .local v0, "i":C
    :goto_1
    const/16 v1, 0x100

    if-ge v0, v1, :cond_0

    .line 575
    iget-object v1, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->cMap:[C

    iget-object v2, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->encoding:Lorg/icepdf/index/core/pobjects/fonts/ofont/Encoding;

    invoke-virtual {v2, v0}, Lorg/icepdf/index/core/pobjects/fonts/ofont/Encoding;->get(C)C

    move-result v2

    aput-char v2, v1, v0

    .line 574
    add-int/lit8 v1, v0, 0x1

    int-to-char v0, v1

    goto :goto_1

    .line 562
    .end local v0    # "i":C
    :cond_3
    const-string/jumbo v1, "MacRomanEncoding"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 563
    const-string/jumbo v1, "MacRomanEncoding"

    iput-object v1, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->encodingName:Ljava/lang/String;

    .line 564
    invoke-static {}, Lorg/icepdf/index/core/pobjects/fonts/ofont/Encoding;->getMacRoman()Lorg/icepdf/index/core/pobjects/fonts/ofont/Encoding;

    move-result-object v1

    iput-object v1, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->encoding:Lorg/icepdf/index/core/pobjects/fonts/ofont/Encoding;

    goto :goto_0

    .line 565
    :cond_4
    const-string/jumbo v1, "WinAnsiEncoding"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 566
    const-string/jumbo v1, "WinAnsiEncoding"

    iput-object v1, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->encodingName:Ljava/lang/String;

    .line 567
    invoke-static {}, Lorg/icepdf/index/core/pobjects/fonts/ofont/Encoding;->getWinAnsi()Lorg/icepdf/index/core/pobjects/fonts/ofont/Encoding;

    move-result-object v1

    iput-object v1, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->encoding:Lorg/icepdf/index/core/pobjects/fonts/ofont/Encoding;

    goto :goto_0

    .line 568
    :cond_5
    const-string/jumbo v1, "PDFDocEncoding"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 569
    const-string/jumbo v1, "PDFDocEncoding"

    iput-object v1, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->encodingName:Ljava/lang/String;

    .line 570
    invoke-static {}, Lorg/icepdf/index/core/pobjects/fonts/ofont/Encoding;->getPDFDoc()Lorg/icepdf/index/core/pobjects/fonts/ofont/Encoding;

    move-result-object v1

    iput-object v1, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->encoding:Lorg/icepdf/index/core/pobjects/fonts/ofont/Encoding;

    goto :goto_0
.end method


# virtual methods
.method public getFonts()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;",
            ">;"
        }
    .end annotation

    .prologue
    .line 227
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->fontnames:Ljava/util/Collection;

    return-object v0
.end method

.method public init()V
    .locals 38

    .prologue
    .line 237
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->inited:Z

    move/from16 v33, v0

    if-eqz v33, :cond_0

    .line 547
    :goto_0
    return-void

    .line 242
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->encoding:Lorg/icepdf/index/core/pobjects/fonts/ofont/Encoding;

    move-object/from16 v33, v0

    if-eqz v33, :cond_1

    .line 243
    const/16 v22, 0x0

    .local v22, "i":C
    :goto_1
    const/16 v33, 0x100

    move/from16 v0, v22

    move/from16 v1, v33

    if-ge v0, v1, :cond_1

    .line 244
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->cMap:[C

    move-object/from16 v33, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->encoding:Lorg/icepdf/index/core/pobjects/fonts/ofont/Encoding;

    move-object/from16 v34, v0

    move-object/from16 v0, v34

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lorg/icepdf/index/core/pobjects/fonts/ofont/Encoding;->get(C)C

    move-result v34

    aput-char v34, v33, v22

    .line 243
    add-int/lit8 v33, v22, 0x1

    move/from16 v0, v33

    int-to-char v0, v0

    move/from16 v22, v0

    goto :goto_1

    .line 249
    .end local v22    # "i":C
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->library:Lorg/icepdf/index/core/util/Library;

    move-object/from16 v33, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->entries:Lorg/icepdf/index/core/util/Hashtable;

    move-object/from16 v34, v0

    const-string/jumbo v35, "ToUnicode"

    invoke-virtual/range {v33 .. v35}, Lorg/icepdf/index/core/util/Library;->getObject(Lorg/icepdf/index/core/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v27

    .line 250
    .local v27, "objectUnicode":Ljava/lang/Object;
    if-eqz v27, :cond_2

    move-object/from16 v0, v27

    instance-of v0, v0, Lorg/icepdf/index/core/pobjects/Stream;

    move/from16 v33, v0

    if-eqz v33, :cond_2

    .line 251
    new-instance v33, Lorg/icepdf/index/core/pobjects/fonts/ofont/CMap;

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->library:Lorg/icepdf/index/core/util/Library;

    move-object/from16 v34, v0

    new-instance v35, Lorg/icepdf/index/core/util/Hashtable;

    invoke-direct/range {v35 .. v35}, Lorg/icepdf/index/core/util/Hashtable;-><init>()V

    check-cast v27, Lorg/icepdf/index/core/pobjects/Stream;

    .end local v27    # "objectUnicode":Ljava/lang/Object;
    move-object/from16 v0, v33

    move-object/from16 v1, v34

    move-object/from16 v2, v35

    move-object/from16 v3, v27

    invoke-direct {v0, v1, v2, v3}, Lorg/icepdf/index/core/pobjects/fonts/ofont/CMap;-><init>(Lorg/icepdf/index/core/util/Library;Lorg/icepdf/index/core/util/Hashtable;Lorg/icepdf/index/core/pobjects/Stream;)V

    move-object/from16 v0, v33

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->toUnicodeCMap:Lorg/icepdf/index/core/pobjects/fonts/ofont/CMap;

    .line 252
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->toUnicodeCMap:Lorg/icepdf/index/core/pobjects/fonts/ofont/CMap;

    move-object/from16 v33, v0

    invoke-virtual/range {v33 .. v33}, Lorg/icepdf/index/core/pobjects/fonts/ofont/CMap;->init()V

    .line 256
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->library:Lorg/icepdf/index/core/util/Library;

    move-object/from16 v33, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->entries:Lorg/icepdf/index/core/util/Hashtable;

    move-object/from16 v34, v0

    const-string/jumbo v35, "Encoding"

    invoke-virtual/range {v33 .. v35}, Lorg/icepdf/index/core/util/Library;->getObject(Lorg/icepdf/index/core/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v26

    .line 257
    .local v26, "o":Ljava/lang/Object;
    if-eqz v26, :cond_7

    .line 258
    move-object/from16 v0, v26

    instance-of v0, v0, Lorg/icepdf/index/core/util/Hashtable;

    move/from16 v33, v0

    if-eqz v33, :cond_6

    move-object/from16 v15, v26

    .line 259
    check-cast v15, Lorg/icepdf/index/core/util/Hashtable;

    .line 260
    .local v15, "encoding":Lorg/icepdf/index/core/util/Hashtable;
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->library:Lorg/icepdf/index/core/util/Library;

    move-object/from16 v33, v0

    const-string/jumbo v34, "BaseEncoding"

    move-object/from16 v0, v33

    move-object/from16 v1, v34

    invoke-virtual {v0, v15, v1}, Lorg/icepdf/index/core/util/Library;->getName(Lorg/icepdf/index/core/util/Hashtable;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v33

    move-object/from16 v0, p0

    move-object/from16 v1, v33

    invoke-direct {v0, v1}, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->setBaseEncoding(Ljava/lang/String;)V

    .line 261
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->library:Lorg/icepdf/index/core/util/Library;

    move-object/from16 v33, v0

    const-string/jumbo v34, "Differences"

    move-object/from16 v0, v33

    move-object/from16 v1, v34

    invoke-virtual {v0, v15, v1}, Lorg/icepdf/index/core/util/Library;->getObject(Lorg/icepdf/index/core/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/util/Vector;

    .line 262
    .local v13, "differences":Ljava/util/Vector;
    if-eqz v13, :cond_7

    .line 263
    const/4 v9, 0x0

    .line 264
    .local v9, "c":I
    invoke-virtual {v13}, Ljava/util/Vector;->elements()Ljava/util/Enumeration;

    move-result-object v14

    .line 265
    .local v14, "e":Ljava/util/Enumeration;
    :cond_3
    :goto_2
    invoke-interface {v14}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v33

    if-eqz v33, :cond_7

    .line 266
    invoke-interface {v14}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v29

    .line 267
    .local v29, "oo":Ljava/lang/Object;
    move-object/from16 v0, v29

    instance-of v0, v0, Ljava/lang/Number;

    move/from16 v33, v0

    if-eqz v33, :cond_4

    .line 268
    check-cast v29, Ljava/lang/Number;

    .end local v29    # "oo":Ljava/lang/Object;
    invoke-virtual/range {v29 .. v29}, Ljava/lang/Number;->intValue()I

    move-result v9

    goto :goto_2

    .line 269
    .restart local v29    # "oo":Ljava/lang/Object;
    :cond_4
    move-object/from16 v0, v29

    instance-of v0, v0, Lorg/icepdf/index/core/pobjects/Name;

    move/from16 v33, v0

    if-eqz v33, :cond_3

    .line 270
    invoke-virtual/range {v29 .. v29}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v25

    .line 271
    .local v25, "n":Ljava/lang/String;
    invoke-static/range {v25 .. v25}, Lorg/icepdf/index/core/pobjects/fonts/ofont/Encoding;->getUV(Ljava/lang/String;)I

    move-result v10

    .line 272
    .local v10, "c1":I
    const/16 v33, -0x1

    move/from16 v0, v33

    if-ne v10, v0, :cond_5

    .line 273
    const/16 v33, 0x0

    move-object/from16 v0, v25

    move/from16 v1, v33

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v33

    const/16 v34, 0x61

    move/from16 v0, v33

    move/from16 v1, v34

    if-ne v0, v1, :cond_5

    .line 274
    const/16 v33, 0x1

    move-object/from16 v0, v25

    move/from16 v1, v33

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v25

    .line 276
    :try_start_0
    invoke-static/range {v25 .. v25}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v10

    .line 282
    :cond_5
    :goto_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->cMap:[C

    move-object/from16 v33, v0

    int-to-char v0, v10

    move/from16 v34, v0

    aput-char v34, v33, v9

    .line 283
    add-int/lit8 v9, v9, 0x1

    goto :goto_2

    .line 287
    .end local v9    # "c":I
    .end local v10    # "c1":I
    .end local v13    # "differences":Ljava/util/Vector;
    .end local v14    # "e":Ljava/util/Enumeration;
    .end local v15    # "encoding":Lorg/icepdf/index/core/util/Hashtable;
    .end local v25    # "n":Ljava/lang/String;
    .end local v29    # "oo":Ljava/lang/Object;
    :cond_6
    move-object/from16 v0, v26

    instance-of v0, v0, Lorg/icepdf/index/core/pobjects/Name;

    move/from16 v33, v0

    if-eqz v33, :cond_7

    move-object/from16 v33, v26

    .line 288
    check-cast v33, Lorg/icepdf/index/core/pobjects/Name;

    invoke-virtual/range {v33 .. v33}, Lorg/icepdf/index/core/pobjects/Name;->getName()Ljava/lang/String;

    move-result-object v33

    move-object/from16 v0, p0

    move-object/from16 v1, v33

    invoke-direct {v0, v1}, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->setBaseEncoding(Ljava/lang/String;)V

    .line 294
    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->library:Lorg/icepdf/index/core/util/Library;

    move-object/from16 v33, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->entries:Lorg/icepdf/index/core/util/Hashtable;

    move-object/from16 v34, v0

    const-string/jumbo v35, "Widths"

    invoke-virtual/range {v33 .. v35}, Lorg/icepdf/index/core/util/Library;->getObject(Lorg/icepdf/index/core/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v33

    check-cast v33, Ljava/util/Vector;

    check-cast v33, Ljava/util/Vector;

    move-object/from16 v0, v33

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->widths:Ljava/util/Vector;

    .line 295
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->widths:Ljava/util/Vector;

    move-object/from16 v33, v0

    if-eqz v33, :cond_12

    .line 298
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->library:Lorg/icepdf/index/core/util/Library;

    move-object/from16 v33, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->entries:Lorg/icepdf/index/core/util/Hashtable;

    move-object/from16 v34, v0

    const-string/jumbo v35, "FirstChar"

    invoke-virtual/range {v33 .. v35}, Lorg/icepdf/index/core/util/Library;->getObject(Lorg/icepdf/index/core/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v26

    .line 299
    if-eqz v26, :cond_8

    .line 300
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->library:Lorg/icepdf/index/core/util/Library;

    move-object/from16 v33, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->entries:Lorg/icepdf/index/core/util/Hashtable;

    move-object/from16 v34, v0

    const-string/jumbo v35, "FirstChar"

    invoke-virtual/range {v33 .. v35}, Lorg/icepdf/index/core/util/Library;->getFloat(Lorg/icepdf/index/core/util/Hashtable;Ljava/lang/String;)F

    move-result v33

    move/from16 v0, v33

    float-to-int v0, v0

    move/from16 v33, v0

    move/from16 v0, v33

    move-object/from16 v1, p0

    iput v0, v1, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->firstchar:I

    .line 317
    :cond_8
    :goto_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->library:Lorg/icepdf/index/core/util/Library;

    move-object/from16 v33, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->entries:Lorg/icepdf/index/core/util/Hashtable;

    move-object/from16 v34, v0

    const-string/jumbo v35, "FontDescriptor"

    invoke-virtual/range {v33 .. v35}, Lorg/icepdf/index/core/util/Library;->getObject(Lorg/icepdf/index/core/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v28

    .line 318
    .local v28, "of":Ljava/lang/Object;
    move-object/from16 v0, v28

    instance-of v0, v0, Lorg/icepdf/index/core/pobjects/fonts/FontDescriptor;

    move/from16 v33, v0

    if-eqz v33, :cond_9

    .line 319
    check-cast v28, Lorg/icepdf/index/core/pobjects/fonts/FontDescriptor;

    .end local v28    # "of":Ljava/lang/Object;
    move-object/from16 v0, v28

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->fontDescriptor:Lorg/icepdf/index/core/pobjects/fonts/FontDescriptor;

    .line 320
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->fontDescriptor:Lorg/icepdf/index/core/pobjects/fonts/FontDescriptor;

    move-object/from16 v33, v0

    invoke-virtual/range {v33 .. v33}, Lorg/icepdf/index/core/pobjects/fonts/FontDescriptor;->init()V

    .line 326
    :cond_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->fontDescriptor:Lorg/icepdf/index/core/pobjects/fonts/FontDescriptor;

    move-object/from16 v33, v0

    if-nez v33, :cond_a

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->basefont:Ljava/lang/String;

    move-object/from16 v33, v0

    if-eqz v33, :cond_a

    .line 328
    sget-object v33, Lorg/icepdf/index/core/pobjects/fonts/AFM;->AFMs:Ljava/util/HashMap;

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->basefont:Ljava/lang/String;

    move-object/from16 v34, v0

    sget-object v35, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual/range {v34 .. v35}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v34

    invoke-virtual/range {v33 .. v34}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    .line 329
    .local v7, "afm":Ljava/lang/Object;
    if-eqz v7, :cond_a

    move-object/from16 v19, v7

    .line 330
    check-cast v19, Lorg/icepdf/index/core/pobjects/fonts/AFM;

    .line 333
    .local v19, "fontMetrix":Lorg/icepdf/index/core/pobjects/fonts/AFM;
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->library:Lorg/icepdf/index/core/util/Library;

    move-object/from16 v33, v0

    move-object/from16 v0, v33

    move-object/from16 v1, v19

    invoke-static {v0, v1}, Lorg/icepdf/index/core/pobjects/fonts/FontDescriptor;->createDescriptor(Lorg/icepdf/index/core/util/Library;Lorg/icepdf/index/core/pobjects/fonts/AFM;)Lorg/icepdf/index/core/pobjects/fonts/FontDescriptor;

    move-result-object v33

    move-object/from16 v0, v33

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->fontDescriptor:Lorg/icepdf/index/core/pobjects/fonts/FontDescriptor;

    .line 334
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->fontDescriptor:Lorg/icepdf/index/core/pobjects/fonts/FontDescriptor;

    move-object/from16 v33, v0

    invoke-virtual/range {v33 .. v33}, Lorg/icepdf/index/core/pobjects/fonts/FontDescriptor;->init()V

    .line 339
    .end local v7    # "afm":Ljava/lang/Object;
    .end local v19    # "fontMetrix":Lorg/icepdf/index/core/pobjects/fonts/AFM;
    :cond_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->fontDescriptor:Lorg/icepdf/index/core/pobjects/fonts/FontDescriptor;

    move-object/from16 v33, v0

    if-eqz v33, :cond_b

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->fontDescriptor:Lorg/icepdf/index/core/pobjects/fonts/FontDescriptor;

    move-object/from16 v33, v0

    invoke-virtual/range {v33 .. v33}, Lorg/icepdf/index/core/pobjects/fonts/FontDescriptor;->getFontName()Ljava/lang/String;

    move-result-object v33

    invoke-virtual/range {v33 .. v33}, Ljava/lang/String;->length()I

    move-result v33

    if-lez v33, :cond_b

    .line 340
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->fontDescriptor:Lorg/icepdf/index/core/pobjects/fonts/FontDescriptor;

    move-object/from16 v33, v0

    invoke-virtual/range {v33 .. v33}, Lorg/icepdf/index/core/pobjects/fonts/FontDescriptor;->getFontName()Ljava/lang/String;

    move-result-object v33

    move-object/from16 v0, v33

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->basefont:Ljava/lang/String;

    .line 341
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->basefont:Ljava/lang/String;

    move-object/from16 v33, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v33

    invoke-direct {v0, v1}, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->cleanFontName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v33

    move-object/from16 v0, v33

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->basefont:Ljava/lang/String;

    .line 345
    :cond_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->fontDescriptor:Lorg/icepdf/index/core/pobjects/fonts/FontDescriptor;

    move-object/from16 v33, v0

    if-eqz v33, :cond_c

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->fontDescriptor:Lorg/icepdf/index/core/pobjects/fonts/FontDescriptor;

    move-object/from16 v33, v0

    invoke-virtual/range {v33 .. v33}, Lorg/icepdf/index/core/pobjects/fonts/FontDescriptor;->getFlags()I

    move-result v33

    and-int/lit8 v33, v33, 0x40

    if-eqz v33, :cond_c

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->encoding:Lorg/icepdf/index/core/pobjects/fonts/ofont/Encoding;

    move-object/from16 v33, v0

    if-nez v33, :cond_c

    .line 346
    const-string/jumbo v33, "standard"

    move-object/from16 v0, v33

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->encodingName:Ljava/lang/String;

    .line 347
    invoke-static {}, Lorg/icepdf/index/core/pobjects/fonts/ofont/Encoding;->getStandard()Lorg/icepdf/index/core/pobjects/fonts/ofont/Encoding;

    move-result-object v33

    move-object/from16 v0, v33

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->encoding:Lorg/icepdf/index/core/pobjects/fonts/ofont/Encoding;

    .line 354
    :cond_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->library:Lorg/icepdf/index/core/util/Library;

    move-object/from16 v33, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->entries:Lorg/icepdf/index/core/util/Hashtable;

    move-object/from16 v34, v0

    const-string/jumbo v35, "DescendantFonts"

    invoke-virtual/range {v33 .. v35}, Lorg/icepdf/index/core/util/Library;->getObject(Lorg/icepdf/index/core/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v12

    .line 355
    .local v12, "desendantFont":Ljava/lang/Object;
    if-eqz v12, :cond_d

    move-object/from16 v32, v12

    .line 356
    check-cast v32, Ljava/util/Vector;

    .line 357
    .local v32, "tmp":Ljava/util/Vector;
    const/16 v33, 0x0

    invoke-virtual/range {v32 .. v33}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v33

    move-object/from16 v0, v33

    instance-of v0, v0, Lorg/icepdf/index/core/pobjects/Reference;

    move/from16 v33, v0

    if-eqz v33, :cond_d

    .line 359
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->library:Lorg/icepdf/index/core/util/Library;

    move-object/from16 v34, v0

    const/16 v33, 0x0

    invoke-virtual/range {v32 .. v33}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v33

    check-cast v33, Lorg/icepdf/index/core/pobjects/Reference;

    move-object/from16 v0, v34

    move-object/from16 v1, v33

    invoke-virtual {v0, v1}, Lorg/icepdf/index/core/util/Library;->getObject(Lorg/icepdf/index/core/pobjects/Reference;)Ljava/lang/Object;

    move-result-object v21

    .line 360
    .local v21, "fontReference":Ljava/lang/Object;
    move-object/from16 v0, v21

    instance-of v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;

    move/from16 v33, v0

    if-eqz v33, :cond_d

    move-object/from16 v11, v21

    .line 362
    check-cast v11, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;

    .line 363
    .local v11, "desendant":Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->toUnicodeCMap:Lorg/icepdf/index/core/pobjects/fonts/ofont/CMap;

    move-object/from16 v33, v0

    move-object/from16 v0, v33

    iput-object v0, v11, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->toUnicodeCMap:Lorg/icepdf/index/core/pobjects/fonts/ofont/CMap;

    .line 364
    invoke-virtual {v11}, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->init()V

    .line 365
    iget-object v0, v11, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->cidWidths:Ljava/util/Map;

    move-object/from16 v33, v0

    move-object/from16 v0, v33

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->cidWidths:Ljava/util/Map;

    .line 368
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->fontDescriptor:Lorg/icepdf/index/core/pobjects/fonts/FontDescriptor;

    move-object/from16 v33, v0

    if-nez v33, :cond_d

    .line 369
    iget-object v0, v11, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->fontDescriptor:Lorg/icepdf/index/core/pobjects/fonts/FontDescriptor;

    move-object/from16 v33, v0

    move-object/from16 v0, v33

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->fontDescriptor:Lorg/icepdf/index/core/pobjects/fonts/FontDescriptor;

    .line 370
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->fontDescriptor:Lorg/icepdf/index/core/pobjects/fonts/FontDescriptor;

    move-object/from16 v33, v0

    invoke-virtual/range {v33 .. v33}, Lorg/icepdf/index/core/pobjects/fonts/FontDescriptor;->getFontName()Ljava/lang/String;

    move-result-object v33

    move-object/from16 v0, v33

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->basefont:Ljava/lang/String;

    .line 371
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->basefont:Ljava/lang/String;

    move-object/from16 v33, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v33

    invoke-direct {v0, v1}, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->cleanFontName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v33

    move-object/from16 v0, v33

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->basefont:Ljava/lang/String;

    .line 379
    .end local v11    # "desendant":Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;
    .end local v21    # "fontReference":Ljava/lang/Object;
    .end local v32    # "tmp":Ljava/util/Vector;
    :cond_d
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->subtype:Ljava/lang/String;

    move-object/from16 v33, v0

    const-string/jumbo v34, "Type1"

    invoke-virtual/range {v33 .. v34}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v33

    if-eqz v33, :cond_e

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->basefont:Ljava/lang/String;

    move-object/from16 v33, v0

    if-eqz v33, :cond_e

    .line 380
    sget-object v33, Lorg/icepdf/index/core/pobjects/fonts/AFM;->AFMs:Ljava/util/HashMap;

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->basefont:Ljava/lang/String;

    move-object/from16 v34, v0

    invoke-virtual/range {v34 .. v34}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v34

    invoke-virtual/range {v33 .. v34}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/icepdf/index/core/pobjects/fonts/AFM;

    .line 381
    .local v5, "a":Lorg/icepdf/index/core/pobjects/fonts/AFM;
    if-eqz v5, :cond_e

    invoke-virtual {v5}, Lorg/icepdf/index/core/pobjects/fonts/AFM;->getFontName()Ljava/lang/String;

    move-result-object v33

    if-eqz v33, :cond_e

    .line 382
    move-object/from16 v0, p0

    iput-object v5, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->afm:Lorg/icepdf/index/core/pobjects/fonts/AFM;

    .line 386
    .end local v5    # "a":Lorg/icepdf/index/core/pobjects/fonts/AFM;
    :cond_e
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->subtype:Ljava/lang/String;

    move-object/from16 v33, v0

    const-string/jumbo v34, "Type1"

    invoke-virtual/range {v33 .. v34}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v33

    if-eqz v33, :cond_f

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->basefont:Ljava/lang/String;

    move-object/from16 v33, v0

    if-eqz v33, :cond_f

    .line 387
    sget-object v8, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->type1Diff:[[Ljava/lang/String;

    .local v8, "arr$":[[Ljava/lang/String;
    array-length v0, v8

    move/from16 v24, v0

    .local v24, "len$":I
    const/16 v23, 0x0

    .local v23, "i$":I
    :goto_5
    move/from16 v0, v23

    move/from16 v1, v24

    if-ge v0, v1, :cond_f

    aget-object v6, v8, v23

    .line 389
    .local v6, "aType1Diff":[Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->basefont:Ljava/lang/String;

    move-object/from16 v33, v0

    const/16 v34, 0x0

    aget-object v34, v6, v34

    invoke-virtual/range {v33 .. v34}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v33

    if-eqz v33, :cond_14

    .line 392
    new-instance v16, Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;

    const/16 v33, 0x1

    aget-object v33, v6, v33

    move-object/from16 v0, p0

    iget v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->style:I

    move/from16 v34, v0

    invoke-static/range {v33 .. v34}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v33

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->basefont:Ljava/lang/String;

    move-object/from16 v34, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->basefont:Ljava/lang/String;

    move-object/from16 v35, v0

    const/high16 v36, 0x41400000    # 12.0f

    move-object/from16 v0, v16

    move-object/from16 v1, v33

    move-object/from16 v2, v34

    move-object/from16 v3, v35

    move/from16 v4, v36

    invoke-direct {v0, v1, v2, v3, v4}, Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;-><init>(Landroid/graphics/Typeface;Ljava/lang/String;Ljava/lang/String;F)V

    .line 393
    .local v16, "f":Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->fontnames:Ljava/util/Collection;

    move-object/from16 v33, v0

    move-object/from16 v0, v33

    move-object/from16 v1, v16

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 395
    invoke-virtual/range {v16 .. v16}, Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;->getFamily()Ljava/lang/String;

    move-result-object v33

    const/16 v34, 0x2

    aget-object v34, v6, v34

    invoke-virtual/range {v33 .. v34}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v33

    if-eqz v33, :cond_14

    .line 396
    const/16 v33, 0x1

    aget-object v33, v6, v33

    move-object/from16 v0, v33

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->basefont:Ljava/lang/String;

    .line 404
    .end local v6    # "aType1Diff":[Ljava/lang/String;
    .end local v8    # "arr$":[[Ljava/lang/String;
    .end local v16    # "f":Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;
    .end local v23    # "i$":I
    .end local v24    # "len$":I
    :cond_f
    const/16 v33, 0x1

    move/from16 v0, v33

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->isFontSubstitution:Z

    .line 408
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->fontDescriptor:Lorg/icepdf/index/core/pobjects/fonts/FontDescriptor;

    move-object/from16 v33, v0

    if-eqz v33, :cond_10

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->fontDescriptor:Lorg/icepdf/index/core/pobjects/fonts/FontDescriptor;

    move-object/from16 v33, v0

    invoke-virtual/range {v33 .. v33}, Lorg/icepdf/index/core/pobjects/fonts/FontDescriptor;->getEmbeddedFont()Lorg/icepdf/index/core/pobjects/fonts/FontFile;

    move-result-object v33

    if-eqz v33, :cond_10

    .line 409
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->fontDescriptor:Lorg/icepdf/index/core/pobjects/fonts/FontDescriptor;

    move-object/from16 v33, v0

    invoke-virtual/range {v33 .. v33}, Lorg/icepdf/index/core/pobjects/fonts/FontDescriptor;->getEmbeddedFont()Lorg/icepdf/index/core/pobjects/fonts/FontFile;

    move-result-object v33

    move-object/from16 v0, v33

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->font:Lorg/icepdf/index/core/pobjects/fonts/FontFile;

    .line 410
    const/16 v33, 0x0

    move/from16 v0, v33

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->isFontSubstitution:Z

    .line 411
    const/16 v33, 0x0

    move/from16 v0, v33

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->isAFMFont:Z

    .line 415
    :cond_10
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->font:Lorg/icepdf/index/core/pobjects/fonts/FontFile;

    move-object/from16 v33, v0

    if-nez v33, :cond_16

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->basefont:Ljava/lang/String;

    move-object/from16 v33, v0

    if-eqz v33, :cond_16

    .line 420
    invoke-virtual/range {p0 .. p0}, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->getFonts()Ljava/util/Collection;

    move-result-object v33

    invoke-interface/range {v33 .. v33}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v23

    .local v23, "i$":Ljava/util/Iterator;
    :cond_11
    invoke-interface/range {v23 .. v23}, Ljava/util/Iterator;->hasNext()Z

    move-result v33

    if-eqz v33, :cond_16

    invoke-interface/range {v23 .. v23}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;

    .line 423
    .local v17, "font1":Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;
    new-instance v30, Ljava/util/StringTokenizer;

    invoke-virtual/range {v17 .. v17}, Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;->getName()Ljava/lang/String;

    move-result-object v33

    const-string/jumbo v34, " "

    const/16 v35, 0x0

    move-object/from16 v0, v30

    move-object/from16 v1, v33

    move-object/from16 v2, v34

    move/from16 v3, v35

    invoke-direct {v0, v1, v2, v3}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 424
    .local v30, "st":Ljava/util/StringTokenizer;
    const-string/jumbo v20, ""

    .line 425
    .local v20, "fontName":Ljava/lang/String;
    new-instance v31, Ljava/lang/StringBuilder;

    invoke-direct/range {v31 .. v31}, Ljava/lang/StringBuilder;-><init>()V

    .line 426
    .local v31, "strBuilder":Ljava/lang/StringBuilder;
    :goto_6
    invoke-virtual/range {v30 .. v30}, Ljava/util/StringTokenizer;->hasMoreElements()Z

    move-result v33

    if-eqz v33, :cond_15

    .line 427
    invoke-virtual/range {v30 .. v30}, Ljava/util/StringTokenizer;->nextElement()Ljava/lang/Object;

    move-result-object v33

    move-object/from16 v0, v31

    move-object/from16 v1, v33

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto :goto_6

    .line 303
    .end local v12    # "desendantFont":Ljava/lang/Object;
    .end local v17    # "font1":Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;
    .end local v20    # "fontName":Ljava/lang/String;
    .end local v23    # "i$":Ljava/util/Iterator;
    .end local v30    # "st":Ljava/util/StringTokenizer;
    .end local v31    # "strBuilder":Ljava/lang/StringBuilder;
    :cond_12
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->library:Lorg/icepdf/index/core/util/Library;

    move-object/from16 v33, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->entries:Lorg/icepdf/index/core/util/Hashtable;

    move-object/from16 v34, v0

    const-string/jumbo v35, "W"

    invoke-virtual/range {v33 .. v35}, Lorg/icepdf/index/core/util/Library;->getObject(Lorg/icepdf/index/core/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v33

    if-eqz v33, :cond_13

    .line 305
    invoke-direct/range {p0 .. p0}, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->calculateCIDWidths()Ljava/util/Map;

    move-result-object v33

    move-object/from16 v0, v33

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->cidWidths:Ljava/util/Map;

    .line 307
    const/16 v33, 0x0

    move/from16 v0, v33

    move-object/from16 v1, p0

    iput v0, v1, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->firstchar:I

    .line 309
    const/16 v33, 0x0

    move/from16 v0, v33

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->isAFMFont:Z

    goto/16 :goto_4

    .line 312
    :cond_13
    const/16 v33, 0x0

    move/from16 v0, v33

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->isAFMFont:Z

    goto/16 :goto_4

    .line 387
    .restart local v6    # "aType1Diff":[Ljava/lang/String;
    .restart local v8    # "arr$":[[Ljava/lang/String;
    .restart local v12    # "desendantFont":Ljava/lang/Object;
    .local v23, "i$":I
    .restart local v24    # "len$":I
    :cond_14
    add-int/lit8 v23, v23, 0x1

    goto/16 :goto_5

    .line 430
    .end local v6    # "aType1Diff":[Ljava/lang/String;
    .end local v8    # "arr$":[[Ljava/lang/String;
    .end local v24    # "len$":I
    .restart local v17    # "font1":Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;
    .restart local v20    # "fontName":Ljava/lang/String;
    .local v23, "i$":Ljava/util/Iterator;
    .restart local v30    # "st":Ljava/util/StringTokenizer;
    .restart local v31    # "strBuilder":Ljava/lang/StringBuilder;
    :cond_15
    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    .line 434
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->basefont:Ljava/lang/String;

    move-object/from16 v33, v0

    move-object/from16 v0, v20

    move-object/from16 v1, v33

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v33

    if-eqz v33, :cond_11

    .line 436
    new-instance v33, Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;

    move-object/from16 v0, p0

    iget v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->style:I

    move/from16 v34, v0

    move-object/from16 v0, v20

    move/from16 v1, v34

    invoke-static {v0, v1}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v34

    invoke-virtual/range {v17 .. v17}, Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;->getFamily()Ljava/lang/String;

    move-result-object v35

    const/high16 v36, 0x41400000    # 12.0f

    move-object/from16 v0, v33

    move-object/from16 v1, v34

    move-object/from16 v2, v20

    move-object/from16 v3, v35

    move/from16 v4, v36

    invoke-direct {v0, v1, v2, v3, v4}, Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;-><init>(Landroid/graphics/Typeface;Ljava/lang/String;Ljava/lang/String;F)V

    move-object/from16 v0, v33

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->font:Lorg/icepdf/index/core/pobjects/fonts/FontFile;

    .line 437
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->fontnames:Ljava/util/Collection;

    move-object/from16 v34, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->font:Lorg/icepdf/index/core/pobjects/fonts/FontFile;

    move-object/from16 v33, v0

    check-cast v33, Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;

    move-object/from16 v0, v34

    move-object/from16 v1, v33

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 438
    invoke-virtual/range {v17 .. v17}, Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;->getName()Ljava/lang/String;

    move-result-object v33

    move-object/from16 v0, v33

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->basefont:Ljava/lang/String;

    .line 439
    const/16 v33, 0x1

    move/from16 v0, v33

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->isFontSubstitution:Z

    .line 446
    .end local v17    # "font1":Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;
    .end local v20    # "fontName":Ljava/lang/String;
    .end local v23    # "i$":Ljava/util/Iterator;
    .end local v30    # "st":Ljava/util/StringTokenizer;
    .end local v31    # "strBuilder":Ljava/lang/StringBuilder;
    :cond_16
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->font:Lorg/icepdf/index/core/pobjects/fonts/FontFile;

    move-object/from16 v33, v0

    if-nez v33, :cond_18

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->basefont:Ljava/lang/String;

    move-object/from16 v33, v0

    if-eqz v33, :cond_18

    .line 449
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->basefont:Ljava/lang/String;

    move-object/from16 v33, v0

    invoke-static/range {v33 .. v33}, Lorg/icepdf/index/core/util/FontUtil;->guessFamily(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    .line 451
    .local v18, "fontFamily":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->getFonts()Ljava/util/Collection;

    move-result-object v33

    invoke-interface/range {v33 .. v33}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v23

    .restart local v23    # "i$":Ljava/util/Iterator;
    :cond_17
    invoke-interface/range {v23 .. v23}, Ljava/util/Iterator;->hasNext()Z

    move-result v33

    if-eqz v33, :cond_18

    invoke-interface/range {v23 .. v23}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;

    .line 453
    .restart local v17    # "font1":Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;
    invoke-virtual/range {v17 .. v17}, Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;->getFamily()Ljava/lang/String;

    move-result-object v33

    move-object/from16 v0, v33

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v33

    if-eqz v33, :cond_17

    .line 455
    new-instance v33, Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;

    invoke-virtual/range {v17 .. v17}, Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;->getFamily()Ljava/lang/String;

    move-result-object v34

    move-object/from16 v0, p0

    iget v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->style:I

    move/from16 v35, v0

    invoke-static/range {v34 .. v35}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v34

    invoke-virtual/range {v17 .. v17}, Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;->getName()Ljava/lang/String;

    move-result-object v35

    invoke-virtual/range {v17 .. v17}, Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;->getFamily()Ljava/lang/String;

    move-result-object v36

    const/high16 v37, 0x41400000    # 12.0f

    invoke-direct/range {v33 .. v37}, Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;-><init>(Landroid/graphics/Typeface;Ljava/lang/String;Ljava/lang/String;F)V

    move-object/from16 v0, v33

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->font:Lorg/icepdf/index/core/pobjects/fonts/FontFile;

    .line 456
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->fontnames:Ljava/util/Collection;

    move-object/from16 v34, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->font:Lorg/icepdf/index/core/pobjects/fonts/FontFile;

    move-object/from16 v33, v0

    check-cast v33, Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;

    move-object/from16 v0, v34

    move-object/from16 v1, v33

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 457
    const/16 v33, 0x1

    move/from16 v0, v33

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->isFontSubstitution:Z

    .line 465
    .end local v17    # "font1":Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;
    .end local v18    # "fontFamily":Ljava/lang/String;
    .end local v23    # "i$":Ljava/util/Iterator;
    :cond_18
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->font:Lorg/icepdf/index/core/pobjects/fonts/FontFile;

    move-object/from16 v33, v0

    if-nez v33, :cond_19

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->basefont:Ljava/lang/String;

    move-object/from16 v33, v0

    if-eqz v33, :cond_19

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->basefont:Ljava/lang/String;

    move-object/from16 v33, v0

    const-string/jumbo v34, "-"

    invoke-virtual/range {v33 .. v34}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v33

    const/16 v34, -0x1

    move/from16 v0, v33

    move/from16 v1, v34

    if-eq v0, v1, :cond_19

    .line 467
    new-instance v33, Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->basefont:Ljava/lang/String;

    move-object/from16 v34, v0

    invoke-direct/range {v33 .. v34}, Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v33

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->font:Lorg/icepdf/index/core/pobjects/fonts/FontFile;

    .line 468
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->fontnames:Ljava/util/Collection;

    move-object/from16 v34, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->font:Lorg/icepdf/index/core/pobjects/fonts/FontFile;

    move-object/from16 v33, v0

    check-cast v33, Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;

    move-object/from16 v0, v34

    move-object/from16 v1, v33

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 469
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->font:Lorg/icepdf/index/core/pobjects/fonts/FontFile;

    move-object/from16 v33, v0

    invoke-interface/range {v33 .. v33}, Lorg/icepdf/index/core/pobjects/fonts/FontFile;->getName()Ljava/lang/String;

    move-result-object v33

    move-object/from16 v0, v33

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->basefont:Ljava/lang/String;

    .line 472
    :cond_19
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->font:Lorg/icepdf/index/core/pobjects/fonts/FontFile;

    move-object/from16 v33, v0

    if-nez v33, :cond_1a

    .line 474
    new-instance v33, Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->basefont:Ljava/lang/String;

    move-object/from16 v34, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->style:I

    move/from16 v35, v0

    invoke-static/range {v34 .. v35}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v34

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->basefont:Ljava/lang/String;

    move-object/from16 v35, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->basefont:Ljava/lang/String;

    move-object/from16 v36, v0

    const/high16 v37, 0x41400000    # 12.0f

    invoke-direct/range {v33 .. v37}, Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;-><init>(Landroid/graphics/Typeface;Ljava/lang/String;Ljava/lang/String;F)V

    move-object/from16 v0, v33

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->font:Lorg/icepdf/index/core/pobjects/fonts/FontFile;

    .line 475
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->fontnames:Ljava/util/Collection;

    move-object/from16 v34, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->font:Lorg/icepdf/index/core/pobjects/fonts/FontFile;

    move-object/from16 v33, v0

    check-cast v33, Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;

    move-object/from16 v0, v34

    move-object/from16 v1, v33

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 476
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->font:Lorg/icepdf/index/core/pobjects/fonts/FontFile;

    move-object/from16 v33, v0

    invoke-interface/range {v33 .. v33}, Lorg/icepdf/index/core/pobjects/fonts/FontFile;->getName()Ljava/lang/String;

    move-result-object v33

    move-object/from16 v0, v33

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->basefont:Ljava/lang/String;

    .line 483
    :cond_1a
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->isFontSubstitution:Z

    move/from16 v33, v0

    if-nez v33, :cond_1c

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->font:Lorg/icepdf/index/core/pobjects/fonts/FontFile;

    move-object/from16 v33, v0

    invoke-interface/range {v33 .. v33}, Lorg/icepdf/index/core/pobjects/fonts/FontFile;->getName()Ljava/lang/String;

    move-result-object v33

    sget-object v34, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual/range {v33 .. v34}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v33

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->font:Lorg/icepdf/index/core/pobjects/fonts/FontFile;

    move-object/from16 v34, v0

    invoke-interface/range {v34 .. v34}, Lorg/icepdf/index/core/pobjects/fonts/FontFile;->getFamily()Ljava/lang/String;

    move-result-object v34

    sget-object v35, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual/range {v34 .. v35}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v34

    invoke-virtual/range {v33 .. v34}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v33

    if-gez v33, :cond_1c

    .line 487
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->font:Lorg/icepdf/index/core/pobjects/fonts/FontFile;

    move-object/from16 v33, v0

    invoke-interface/range {v33 .. v33}, Lorg/icepdf/index/core/pobjects/fonts/FontFile;->getName()Ljava/lang/String;

    move-result-object v33

    sget-object v34, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual/range {v33 .. v34}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v33

    const-string/jumbo v34, "times new roman"

    invoke-virtual/range {v33 .. v34}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v33

    const/16 v34, -0x1

    move/from16 v0, v33

    move/from16 v1, v34

    if-ne v0, v1, :cond_1b

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->font:Lorg/icepdf/index/core/pobjects/fonts/FontFile;

    move-object/from16 v33, v0

    invoke-interface/range {v33 .. v33}, Lorg/icepdf/index/core/pobjects/fonts/FontFile;->getName()Ljava/lang/String;

    move-result-object v33

    sget-object v34, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual/range {v33 .. v34}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v33

    const-string/jumbo v34, "timesnewroman"

    invoke-virtual/range {v33 .. v34}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v33

    const/16 v34, -0x1

    move/from16 v0, v33

    move/from16 v1, v34

    if-ne v0, v1, :cond_1b

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->font:Lorg/icepdf/index/core/pobjects/fonts/FontFile;

    move-object/from16 v33, v0

    invoke-interface/range {v33 .. v33}, Lorg/icepdf/index/core/pobjects/fonts/FontFile;->getName()Ljava/lang/String;

    move-result-object v33

    sget-object v34, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual/range {v33 .. v34}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v33

    const-string/jumbo v34, "bodoni"

    invoke-virtual/range {v33 .. v34}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v33

    const/16 v34, -0x1

    move/from16 v0, v33

    move/from16 v1, v34

    if-ne v0, v1, :cond_1b

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->font:Lorg/icepdf/index/core/pobjects/fonts/FontFile;

    move-object/from16 v33, v0

    invoke-interface/range {v33 .. v33}, Lorg/icepdf/index/core/pobjects/fonts/FontFile;->getName()Ljava/lang/String;

    move-result-object v33

    sget-object v34, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual/range {v33 .. v34}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v33

    const-string/jumbo v34, "garamond"

    invoke-virtual/range {v33 .. v34}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v33

    const/16 v34, -0x1

    move/from16 v0, v33

    move/from16 v1, v34

    if-ne v0, v1, :cond_1b

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->font:Lorg/icepdf/index/core/pobjects/fonts/FontFile;

    move-object/from16 v33, v0

    invoke-interface/range {v33 .. v33}, Lorg/icepdf/index/core/pobjects/fonts/FontFile;->getName()Ljava/lang/String;

    move-result-object v33

    sget-object v34, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual/range {v33 .. v34}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v33

    const-string/jumbo v34, "minion web"

    invoke-virtual/range {v33 .. v34}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v33

    const/16 v34, -0x1

    move/from16 v0, v33

    move/from16 v1, v34

    if-ne v0, v1, :cond_1b

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->font:Lorg/icepdf/index/core/pobjects/fonts/FontFile;

    move-object/from16 v33, v0

    invoke-interface/range {v33 .. v33}, Lorg/icepdf/index/core/pobjects/fonts/FontFile;->getName()Ljava/lang/String;

    move-result-object v33

    sget-object v34, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual/range {v33 .. v34}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v33

    const-string/jumbo v34, "stone serif"

    invoke-virtual/range {v33 .. v34}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v33

    const/16 v34, -0x1

    move/from16 v0, v33

    move/from16 v1, v34

    if-ne v0, v1, :cond_1b

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->font:Lorg/icepdf/index/core/pobjects/fonts/FontFile;

    move-object/from16 v33, v0

    invoke-interface/range {v33 .. v33}, Lorg/icepdf/index/core/pobjects/fonts/FontFile;->getName()Ljava/lang/String;

    move-result-object v33

    sget-object v34, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual/range {v33 .. v34}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v33

    const-string/jumbo v34, "stoneserif"

    invoke-virtual/range {v33 .. v34}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v33

    const/16 v34, -0x1

    move/from16 v0, v33

    move/from16 v1, v34

    if-ne v0, v1, :cond_1b

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->font:Lorg/icepdf/index/core/pobjects/fonts/FontFile;

    move-object/from16 v33, v0

    invoke-interface/range {v33 .. v33}, Lorg/icepdf/index/core/pobjects/fonts/FontFile;->getName()Ljava/lang/String;

    move-result-object v33

    sget-object v34, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual/range {v33 .. v34}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v33

    const-string/jumbo v34, "georgia"

    invoke-virtual/range {v33 .. v34}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v33

    const/16 v34, -0x1

    move/from16 v0, v33

    move/from16 v1, v34

    if-ne v0, v1, :cond_1b

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->font:Lorg/icepdf/index/core/pobjects/fonts/FontFile;

    move-object/from16 v33, v0

    invoke-interface/range {v33 .. v33}, Lorg/icepdf/index/core/pobjects/fonts/FontFile;->getName()Ljava/lang/String;

    move-result-object v33

    sget-object v34, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual/range {v33 .. v34}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v33

    const-string/jumbo v34, "bitstream cyberbit"

    invoke-virtual/range {v33 .. v34}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v33

    const/16 v34, -0x1

    move/from16 v0, v33

    move/from16 v1, v34

    if-eq v0, v1, :cond_1d

    .line 496
    :cond_1b
    const-string/jumbo v33, "serif"

    move-object/from16 v0, v33

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->basefont:Ljava/lang/String;

    .line 497
    new-instance v33, Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->basefont:Ljava/lang/String;

    move-object/from16 v34, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->style:I

    move/from16 v35, v0

    invoke-static/range {v34 .. v35}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v34

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->basefont:Ljava/lang/String;

    move-object/from16 v35, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->basefont:Ljava/lang/String;

    move-object/from16 v36, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->font:Lorg/icepdf/index/core/pobjects/fonts/FontFile;

    move-object/from16 v37, v0

    invoke-interface/range {v37 .. v37}, Lorg/icepdf/index/core/pobjects/fonts/FontFile;->getSize()F

    move-result v37

    invoke-direct/range {v33 .. v37}, Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;-><init>(Landroid/graphics/Typeface;Ljava/lang/String;Ljava/lang/String;F)V

    move-object/from16 v0, v33

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->font:Lorg/icepdf/index/core/pobjects/fonts/FontFile;

    .line 498
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->fontnames:Ljava/util/Collection;

    move-object/from16 v34, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->font:Lorg/icepdf/index/core/pobjects/fonts/FontFile;

    move-object/from16 v33, v0

    check-cast v33, Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;

    move-object/from16 v0, v34

    move-object/from16 v1, v33

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 539
    :cond_1c
    :goto_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->font:Lorg/icepdf/index/core/pobjects/fonts/FontFile;

    move-object/from16 v33, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->encoding:Lorg/icepdf/index/core/pobjects/fonts/ofont/Encoding;

    move-object/from16 v34, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->toUnicodeCMap:Lorg/icepdf/index/core/pobjects/fonts/ofont/CMap;

    move-object/from16 v35, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->cMap:[C

    move-object/from16 v36, v0

    invoke-interface/range {v33 .. v36}, Lorg/icepdf/index/core/pobjects/fonts/FontFile;->deriveFont(Lorg/icepdf/index/core/pobjects/fonts/Encoding;Lorg/icepdf/index/core/pobjects/fonts/CMap;[C)Lorg/icepdf/index/core/pobjects/fonts/FontFile;

    move-result-object v33

    move-object/from16 v0, v33

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->font:Lorg/icepdf/index/core/pobjects/fonts/FontFile;

    .line 546
    const/16 v33, 0x1

    move/from16 v0, v33

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->inited:Z

    goto/16 :goto_0

    .line 500
    :cond_1d
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->font:Lorg/icepdf/index/core/pobjects/fonts/FontFile;

    move-object/from16 v33, v0

    invoke-interface/range {v33 .. v33}, Lorg/icepdf/index/core/pobjects/fonts/FontFile;->getName()Ljava/lang/String;

    move-result-object v33

    sget-object v34, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual/range {v33 .. v34}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v33

    const-string/jumbo v34, "helvetica"

    invoke-virtual/range {v33 .. v34}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v33

    const/16 v34, -0x1

    move/from16 v0, v33

    move/from16 v1, v34

    if-ne v0, v1, :cond_1e

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->font:Lorg/icepdf/index/core/pobjects/fonts/FontFile;

    move-object/from16 v33, v0

    invoke-interface/range {v33 .. v33}, Lorg/icepdf/index/core/pobjects/fonts/FontFile;->getName()Ljava/lang/String;

    move-result-object v33

    sget-object v34, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual/range {v33 .. v34}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v33

    const-string/jumbo v34, "arial"

    invoke-virtual/range {v33 .. v34}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v33

    const/16 v34, -0x1

    move/from16 v0, v33

    move/from16 v1, v34

    if-ne v0, v1, :cond_1e

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->font:Lorg/icepdf/index/core/pobjects/fonts/FontFile;

    move-object/from16 v33, v0

    invoke-interface/range {v33 .. v33}, Lorg/icepdf/index/core/pobjects/fonts/FontFile;->getName()Ljava/lang/String;

    move-result-object v33

    sget-object v34, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual/range {v33 .. v34}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v33

    const-string/jumbo v34, "trebuchet"

    invoke-virtual/range {v33 .. v34}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v33

    const/16 v34, -0x1

    move/from16 v0, v33

    move/from16 v1, v34

    if-ne v0, v1, :cond_1e

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->font:Lorg/icepdf/index/core/pobjects/fonts/FontFile;

    move-object/from16 v33, v0

    invoke-interface/range {v33 .. v33}, Lorg/icepdf/index/core/pobjects/fonts/FontFile;->getName()Ljava/lang/String;

    move-result-object v33

    sget-object v34, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual/range {v33 .. v34}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v33

    const-string/jumbo v34, "avant garde gothic"

    invoke-virtual/range {v33 .. v34}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v33

    const/16 v34, -0x1

    move/from16 v0, v33

    move/from16 v1, v34

    if-ne v0, v1, :cond_1e

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->font:Lorg/icepdf/index/core/pobjects/fonts/FontFile;

    move-object/from16 v33, v0

    invoke-interface/range {v33 .. v33}, Lorg/icepdf/index/core/pobjects/fonts/FontFile;->getName()Ljava/lang/String;

    move-result-object v33

    sget-object v34, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual/range {v33 .. v34}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v33

    const-string/jumbo v34, "avantgardegothic"

    invoke-virtual/range {v33 .. v34}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v33

    const/16 v34, -0x1

    move/from16 v0, v33

    move/from16 v1, v34

    if-ne v0, v1, :cond_1e

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->font:Lorg/icepdf/index/core/pobjects/fonts/FontFile;

    move-object/from16 v33, v0

    invoke-interface/range {v33 .. v33}, Lorg/icepdf/index/core/pobjects/fonts/FontFile;->getName()Ljava/lang/String;

    move-result-object v33

    sget-object v34, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual/range {v33 .. v34}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v33

    const-string/jumbo v34, "verdana"

    invoke-virtual/range {v33 .. v34}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v33

    const/16 v34, -0x1

    move/from16 v0, v33

    move/from16 v1, v34

    if-ne v0, v1, :cond_1e

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->font:Lorg/icepdf/index/core/pobjects/fonts/FontFile;

    move-object/from16 v33, v0

    invoke-interface/range {v33 .. v33}, Lorg/icepdf/index/core/pobjects/fonts/FontFile;->getName()Ljava/lang/String;

    move-result-object v33

    sget-object v34, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual/range {v33 .. v34}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v33

    const-string/jumbo v34, "univers"

    invoke-virtual/range {v33 .. v34}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v33

    const/16 v34, -0x1

    move/from16 v0, v33

    move/from16 v1, v34

    if-ne v0, v1, :cond_1e

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->font:Lorg/icepdf/index/core/pobjects/fonts/FontFile;

    move-object/from16 v33, v0

    invoke-interface/range {v33 .. v33}, Lorg/icepdf/index/core/pobjects/fonts/FontFile;->getName()Ljava/lang/String;

    move-result-object v33

    sget-object v34, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual/range {v33 .. v34}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v33

    const-string/jumbo v34, "futura"

    invoke-virtual/range {v33 .. v34}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v33

    const/16 v34, -0x1

    move/from16 v0, v33

    move/from16 v1, v34

    if-ne v0, v1, :cond_1e

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->font:Lorg/icepdf/index/core/pobjects/fonts/FontFile;

    move-object/from16 v33, v0

    invoke-interface/range {v33 .. v33}, Lorg/icepdf/index/core/pobjects/fonts/FontFile;->getName()Ljava/lang/String;

    move-result-object v33

    sget-object v34, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual/range {v33 .. v34}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v33

    const-string/jumbo v34, "stone sans"

    invoke-virtual/range {v33 .. v34}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v33

    const/16 v34, -0x1

    move/from16 v0, v33

    move/from16 v1, v34

    if-ne v0, v1, :cond_1e

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->font:Lorg/icepdf/index/core/pobjects/fonts/FontFile;

    move-object/from16 v33, v0

    invoke-interface/range {v33 .. v33}, Lorg/icepdf/index/core/pobjects/fonts/FontFile;->getName()Ljava/lang/String;

    move-result-object v33

    sget-object v34, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual/range {v33 .. v34}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v33

    const-string/jumbo v34, "stonesans"

    invoke-virtual/range {v33 .. v34}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v33

    const/16 v34, -0x1

    move/from16 v0, v33

    move/from16 v1, v34

    if-ne v0, v1, :cond_1e

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->font:Lorg/icepdf/index/core/pobjects/fonts/FontFile;

    move-object/from16 v33, v0

    invoke-interface/range {v33 .. v33}, Lorg/icepdf/index/core/pobjects/fonts/FontFile;->getName()Ljava/lang/String;

    move-result-object v33

    sget-object v34, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual/range {v33 .. v34}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v33

    const-string/jumbo v34, "gill sans"

    invoke-virtual/range {v33 .. v34}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v33

    const/16 v34, -0x1

    move/from16 v0, v33

    move/from16 v1, v34

    if-ne v0, v1, :cond_1e

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->font:Lorg/icepdf/index/core/pobjects/fonts/FontFile;

    move-object/from16 v33, v0

    invoke-interface/range {v33 .. v33}, Lorg/icepdf/index/core/pobjects/fonts/FontFile;->getName()Ljava/lang/String;

    move-result-object v33

    sget-object v34, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual/range {v33 .. v34}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v33

    const-string/jumbo v34, "gillsans"

    invoke-virtual/range {v33 .. v34}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v33

    const/16 v34, -0x1

    move/from16 v0, v33

    move/from16 v1, v34

    if-ne v0, v1, :cond_1e

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->font:Lorg/icepdf/index/core/pobjects/fonts/FontFile;

    move-object/from16 v33, v0

    invoke-interface/range {v33 .. v33}, Lorg/icepdf/index/core/pobjects/fonts/FontFile;->getName()Ljava/lang/String;

    move-result-object v33

    sget-object v34, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual/range {v33 .. v34}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v33

    const-string/jumbo v34, "akzidenz"

    invoke-virtual/range {v33 .. v34}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v33

    const/16 v34, -0x1

    move/from16 v0, v33

    move/from16 v1, v34

    if-ne v0, v1, :cond_1e

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->font:Lorg/icepdf/index/core/pobjects/fonts/FontFile;

    move-object/from16 v33, v0

    invoke-interface/range {v33 .. v33}, Lorg/icepdf/index/core/pobjects/fonts/FontFile;->getName()Ljava/lang/String;

    move-result-object v33

    sget-object v34, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual/range {v33 .. v34}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v33

    const-string/jumbo v34, "grotesk"

    invoke-virtual/range {v33 .. v34}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v33

    const/16 v34, -0x1

    move/from16 v0, v33

    move/from16 v1, v34

    if-eq v0, v1, :cond_1f

    .line 514
    :cond_1e
    const-string/jumbo v33, "sansserif"

    move-object/from16 v0, v33

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->basefont:Ljava/lang/String;

    .line 515
    new-instance v33, Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->basefont:Ljava/lang/String;

    move-object/from16 v34, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->style:I

    move/from16 v35, v0

    invoke-static/range {v34 .. v35}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v34

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->basefont:Ljava/lang/String;

    move-object/from16 v35, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->basefont:Ljava/lang/String;

    move-object/from16 v36, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->font:Lorg/icepdf/index/core/pobjects/fonts/FontFile;

    move-object/from16 v37, v0

    invoke-interface/range {v37 .. v37}, Lorg/icepdf/index/core/pobjects/fonts/FontFile;->getSize()F

    move-result v37

    invoke-direct/range {v33 .. v37}, Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;-><init>(Landroid/graphics/Typeface;Ljava/lang/String;Ljava/lang/String;F)V

    move-object/from16 v0, v33

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->font:Lorg/icepdf/index/core/pobjects/fonts/FontFile;

    .line 516
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->fontnames:Ljava/util/Collection;

    move-object/from16 v34, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->font:Lorg/icepdf/index/core/pobjects/fonts/FontFile;

    move-object/from16 v33, v0

    check-cast v33, Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;

    move-object/from16 v0, v34

    move-object/from16 v1, v33

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto/16 :goto_7

    .line 518
    :cond_1f
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->font:Lorg/icepdf/index/core/pobjects/fonts/FontFile;

    move-object/from16 v33, v0

    invoke-interface/range {v33 .. v33}, Lorg/icepdf/index/core/pobjects/fonts/FontFile;->getName()Ljava/lang/String;

    move-result-object v33

    sget-object v34, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual/range {v33 .. v34}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v33

    const-string/jumbo v34, "courier"

    invoke-virtual/range {v33 .. v34}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v33

    const/16 v34, -0x1

    move/from16 v0, v33

    move/from16 v1, v34

    if-ne v0, v1, :cond_20

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->font:Lorg/icepdf/index/core/pobjects/fonts/FontFile;

    move-object/from16 v33, v0

    invoke-interface/range {v33 .. v33}, Lorg/icepdf/index/core/pobjects/fonts/FontFile;->getName()Ljava/lang/String;

    move-result-object v33

    sget-object v34, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual/range {v33 .. v34}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v33

    const-string/jumbo v34, "courier new"

    invoke-virtual/range {v33 .. v34}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v33

    const/16 v34, -0x1

    move/from16 v0, v33

    move/from16 v1, v34

    if-ne v0, v1, :cond_20

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->font:Lorg/icepdf/index/core/pobjects/fonts/FontFile;

    move-object/from16 v33, v0

    invoke-interface/range {v33 .. v33}, Lorg/icepdf/index/core/pobjects/fonts/FontFile;->getName()Ljava/lang/String;

    move-result-object v33

    sget-object v34, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual/range {v33 .. v34}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v33

    const-string/jumbo v34, "couriernew"

    invoke-virtual/range {v33 .. v34}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v33

    const/16 v34, -0x1

    move/from16 v0, v33

    move/from16 v1, v34

    if-ne v0, v1, :cond_20

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->font:Lorg/icepdf/index/core/pobjects/fonts/FontFile;

    move-object/from16 v33, v0

    invoke-interface/range {v33 .. v33}, Lorg/icepdf/index/core/pobjects/fonts/FontFile;->getName()Ljava/lang/String;

    move-result-object v33

    sget-object v34, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual/range {v33 .. v34}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v33

    const-string/jumbo v34, "prestige"

    invoke-virtual/range {v33 .. v34}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v33

    const/16 v34, -0x1

    move/from16 v0, v33

    move/from16 v1, v34

    if-ne v0, v1, :cond_20

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->font:Lorg/icepdf/index/core/pobjects/fonts/FontFile;

    move-object/from16 v33, v0

    invoke-interface/range {v33 .. v33}, Lorg/icepdf/index/core/pobjects/fonts/FontFile;->getName()Ljava/lang/String;

    move-result-object v33

    sget-object v34, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual/range {v33 .. v34}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v33

    const-string/jumbo v34, "eversonmono"

    invoke-virtual/range {v33 .. v34}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v33

    const/16 v34, -0x1

    move/from16 v0, v33

    move/from16 v1, v34

    if-ne v0, v1, :cond_20

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->font:Lorg/icepdf/index/core/pobjects/fonts/FontFile;

    move-object/from16 v33, v0

    invoke-interface/range {v33 .. v33}, Lorg/icepdf/index/core/pobjects/fonts/FontFile;->getName()Ljava/lang/String;

    move-result-object v33

    sget-object v34, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual/range {v33 .. v34}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v33

    const-string/jumbo v34, "Everson Mono"

    invoke-virtual/range {v33 .. v34}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v33

    const/16 v34, -0x1

    move/from16 v0, v33

    move/from16 v1, v34

    if-eq v0, v1, :cond_21

    .line 524
    :cond_20
    const-string/jumbo v33, "monospaced"

    move-object/from16 v0, v33

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->basefont:Ljava/lang/String;

    .line 525
    new-instance v33, Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->basefont:Ljava/lang/String;

    move-object/from16 v34, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->style:I

    move/from16 v35, v0

    invoke-static/range {v34 .. v35}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v34

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->basefont:Ljava/lang/String;

    move-object/from16 v35, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->basefont:Ljava/lang/String;

    move-object/from16 v36, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->font:Lorg/icepdf/index/core/pobjects/fonts/FontFile;

    move-object/from16 v37, v0

    invoke-interface/range {v37 .. v37}, Lorg/icepdf/index/core/pobjects/fonts/FontFile;->getSize()F

    move-result v37

    invoke-direct/range {v33 .. v37}, Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;-><init>(Landroid/graphics/Typeface;Ljava/lang/String;Ljava/lang/String;F)V

    move-object/from16 v0, v33

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->font:Lorg/icepdf/index/core/pobjects/fonts/FontFile;

    .line 526
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->fontnames:Ljava/util/Collection;

    move-object/from16 v34, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->font:Lorg/icepdf/index/core/pobjects/fonts/FontFile;

    move-object/from16 v33, v0

    check-cast v33, Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;

    move-object/from16 v0, v34

    move-object/from16 v1, v33

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto/16 :goto_7

    .line 529
    :cond_21
    const-string/jumbo v33, "serif"

    move-object/from16 v0, v33

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->basefont:Ljava/lang/String;

    .line 530
    new-instance v33, Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->basefont:Ljava/lang/String;

    move-object/from16 v34, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->style:I

    move/from16 v35, v0

    invoke-static/range {v34 .. v35}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v34

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->basefont:Ljava/lang/String;

    move-object/from16 v35, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->basefont:Ljava/lang/String;

    move-object/from16 v36, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->font:Lorg/icepdf/index/core/pobjects/fonts/FontFile;

    move-object/from16 v37, v0

    invoke-interface/range {v37 .. v37}, Lorg/icepdf/index/core/pobjects/fonts/FontFile;->getSize()F

    move-result v37

    invoke-direct/range {v33 .. v37}, Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;-><init>(Landroid/graphics/Typeface;Ljava/lang/String;Ljava/lang/String;F)V

    move-object/from16 v0, v33

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->font:Lorg/icepdf/index/core/pobjects/fonts/FontFile;

    .line 531
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->fontnames:Ljava/util/Collection;

    move-object/from16 v34, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->font:Lorg/icepdf/index/core/pobjects/fonts/FontFile;

    move-object/from16 v33, v0

    check-cast v33, Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;

    move-object/from16 v0, v34

    move-object/from16 v1, v33

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto/16 :goto_7

    .line 277
    .end local v12    # "desendantFont":Ljava/lang/Object;
    .restart local v9    # "c":I
    .restart local v10    # "c1":I
    .restart local v13    # "differences":Ljava/util/Vector;
    .restart local v14    # "e":Ljava/util/Enumeration;
    .restart local v15    # "encoding":Lorg/icepdf/index/core/util/Hashtable;
    .restart local v25    # "n":Ljava/lang/String;
    .restart local v29    # "oo":Ljava/lang/Object;
    :catch_0
    move-exception v33

    goto/16 :goto_3
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 587
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "FONT= "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->encodingName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;->entries:Lorg/icepdf/index/core/util/Hashtable;

    invoke-virtual {v1}, Lorg/icepdf/index/core/util/Hashtable;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
