.class public Lorg/icepdf/index/core/pobjects/fonts/FontFactory;
.super Ljava/lang/Object;
.source "FontFactory.java"


# static fields
.field private static final FONT_CLASS:Ljava/lang/String; = "org.icepdf.core.pobjects.fonts.nfont.Font"

.field public static final FONT_OPEN_TYPE:I = 0x5

.field public static final FONT_TRUE_TYPE:I = 0x0

.field public static final FONT_TYPE_0:I = 0x6

.field public static final FONT_TYPE_1:I = 0x1

.field public static final FONT_TYPE_3:I = 0x7

.field private static final NFONT_CLASS:Ljava/lang/String; = "org.icepdf.core.pobjects.fonts.nfont.NFont"

.field private static final NFONT_OPEN_TYPE:Ljava/lang/String; = "org.icepdf.core.pobjects.fonts.nfont.NFontOpenType"

.field private static final NFONT_TRUE_TYPE:Ljava/lang/String; = "org.icepdf.core.pobjects.fonts.nfont.NFontTrueType"

.field private static final NFONT_TRUE_TYPE_0:Ljava/lang/String; = "org.icepdf.core.pobjects.fonts.nfont.NFontType0"

.field private static final NFONT_TRUE_TYPE_1:Ljava/lang/String; = "org.icepdf.core.pobjects.fonts.nfont.NFontType1"

.field private static final NFONT_TRUE_TYPE_3:Ljava/lang/String; = "org.icepdf.core.pobjects.fonts.nfont.NFontType3"

.field private static awtFontLoading:Z

.field private static awtFontSubstitution:Z

.field private static fontFactory:Lorg/icepdf/index/core/pobjects/fonts/FontFactory;

.field private static foundNFont:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 43
    const-string/jumbo v0, "org.icepdf.core.awtFontLoading"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lorg/icepdf/index/core/util/Defs;->sysPropertyBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lorg/icepdf/index/core/pobjects/fonts/FontFactory;->awtFontLoading:Z

    .line 82
    :try_start_0
    const-string/jumbo v0, "org.icepdf.core.pobjects.fonts.nfont.NFont"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 86
    :goto_0
    return-void

    .line 83
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 105
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getInstance()Lorg/icepdf/index/core/pobjects/fonts/FontFactory;
    .locals 1

    .prologue
    .line 98
    sget-object v0, Lorg/icepdf/index/core/pobjects/fonts/FontFactory;->fontFactory:Lorg/icepdf/index/core/pobjects/fonts/FontFactory;

    if-nez v0, :cond_0

    .line 99
    new-instance v0, Lorg/icepdf/index/core/pobjects/fonts/FontFactory;

    invoke-direct {v0}, Lorg/icepdf/index/core/pobjects/fonts/FontFactory;-><init>()V

    sput-object v0, Lorg/icepdf/index/core/pobjects/fonts/FontFactory;->fontFactory:Lorg/icepdf/index/core/pobjects/fonts/FontFactory;

    .line 101
    :cond_0
    sget-object v0, Lorg/icepdf/index/core/pobjects/fonts/FontFactory;->fontFactory:Lorg/icepdf/index/core/pobjects/fonts/FontFactory;

    return-object v0
.end method

.method private getNFontClass(I)Ljava/lang/Class;
    .locals 2
    .param p1, "fontType"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 243
    const/4 v0, 0x0

    .line 244
    .local v0, "fontClass":Ljava/lang/Class;
    const/4 v1, 0x5

    if-ne v1, p1, :cond_1

    .line 245
    const-string/jumbo v1, "org.icepdf.core.pobjects.fonts.nfont.NFontOpenType"

    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 255
    :cond_0
    :goto_0
    return-object v0

    .line 246
    :cond_1
    if-nez p1, :cond_2

    .line 247
    const-string/jumbo v1, "org.icepdf.core.pobjects.fonts.nfont.NFontTrueType"

    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    goto :goto_0

    .line 248
    :cond_2
    const/4 v1, 0x6

    if-ne v1, p1, :cond_3

    .line 249
    const-string/jumbo v1, "org.icepdf.core.pobjects.fonts.nfont.NFontType0"

    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    goto :goto_0

    .line 250
    :cond_3
    const/4 v1, 0x1

    if-ne v1, p1, :cond_4

    .line 251
    const-string/jumbo v1, "org.icepdf.core.pobjects.fonts.nfont.NFontType1"

    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    goto :goto_0

    .line 252
    :cond_4
    const/4 v1, 0x7

    if-ne v1, p1, :cond_0

    .line 253
    const-string/jumbo v1, "org.icepdf.core.pobjects.fonts.nfont.NFontType3"

    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public createFontFile(Ljava/io/File;I)Lorg/icepdf/index/core/pobjects/fonts/FontFile;
    .locals 8
    .param p1, "file"    # Ljava/io/File;
    .param p2, "fontType"    # I

    .prologue
    .line 194
    const/4 v3, 0x0

    .line 195
    .local v3, "fontFile":Lorg/icepdf/index/core/pobjects/fonts/FontFile;
    invoke-virtual {p0}, Lorg/icepdf/index/core/pobjects/fonts/FontFactory;->foundFontEngine()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 197
    :try_start_0
    invoke-direct {p0, p2}, Lorg/icepdf/index/core/pobjects/fonts/FontFactory;->getNFontClass(I)Ljava/lang/Class;

    move-result-object v1

    .line 198
    .local v1, "fontClass":Ljava/lang/Class;
    if-eqz v1, :cond_0

    .line 200
    const/4 v6, 0x1

    new-array v5, v6, [Ljava/lang/Class;

    const/4 v6, 0x0

    const-class v7, Ljava/net/URL;

    aput-object v7, v5, v6

    .line 201
    .local v5, "urlArg":[Ljava/lang/Class;
    invoke-virtual {v1, v5}, Ljava/lang/Class;->getDeclaredConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v2

    .line 203
    .local v2, "fontClassConstructor":Ljava/lang/reflect/Constructor;
    const/4 v6, 0x1

    new-array v4, v6, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-virtual {p1}, Ljava/io/File;->toURL()Ljava/net/URL;

    move-result-object v7

    aput-object v7, v4, v6

    .line 204
    .local v4, "fontUrl":[Ljava/lang/Object;
    invoke-virtual {v2, v4}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    move-object v0, v6

    check-cast v0, Lorg/icepdf/index/core/pobjects/fonts/FontFile;

    move-object v3, v0
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 227
    .end local v1    # "fontClass":Ljava/lang/Class;
    .end local v2    # "fontClassConstructor":Ljava/lang/reflect/Constructor;
    .end local v4    # "fontUrl":[Ljava/lang/Object;
    .end local v5    # "urlArg":[Ljava/lang/Class;
    :cond_0
    :goto_0
    return-object v3

    .line 206
    :catch_0
    move-exception v6

    goto :goto_0
.end method

.method public createFontFile(Lorg/icepdf/index/core/pobjects/Stream;I)Lorg/icepdf/index/core/pobjects/fonts/FontFile;
    .locals 8
    .param p1, "fontStream"    # Lorg/icepdf/index/core/pobjects/Stream;
    .param p2, "fontType"    # I

    .prologue
    .line 132
    const/4 v4, 0x0

    .line 133
    .local v4, "fontFile":Lorg/icepdf/index/core/pobjects/fonts/FontFile;
    invoke-virtual {p0}, Lorg/icepdf/index/core/pobjects/fonts/FontFactory;->foundFontEngine()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 135
    :try_start_0
    invoke-direct {p0, p2}, Lorg/icepdf/index/core/pobjects/fonts/FontFactory;->getNFontClass(I)Ljava/lang/Class;

    move-result-object v2

    .line 136
    .local v2, "fontClass":Ljava/lang/Class;
    if-eqz v2, :cond_0

    .line 138
    const/4 v6, 0x1

    new-array v1, v6, [Ljava/lang/Class;

    const/4 v6, 0x0

    const-class v7, [B

    aput-object v7, v1, v6

    .line 139
    .local v1, "bytArrayArg":[Ljava/lang/Class;
    invoke-virtual {v2, v1}, Ljava/lang/Class;->getDeclaredConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v3

    .line 141
    .local v3, "fontClassConstructor":Ljava/lang/reflect/Constructor;
    const/4 v6, 0x1

    new-array v5, v6, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-virtual {p1}, Lorg/icepdf/index/core/pobjects/Stream;->getBytes()[B

    move-result-object v7

    aput-object v7, v5, v6

    .line 142
    .local v5, "fontStreamBytes":[Ljava/lang/Object;
    invoke-virtual {p1}, Lorg/icepdf/index/core/pobjects/Stream;->getBytes()[B

    move-result-object v6

    array-length v6, v6

    if-lez v6, :cond_0

    .line 143
    invoke-virtual {v3, v5}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    move-object v0, v6

    check-cast v0, Lorg/icepdf/index/core/pobjects/fonts/FontFile;

    move-object v4, v0
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 190
    .end local v1    # "bytArrayArg":[Ljava/lang/Class;
    .end local v2    # "fontClass":Ljava/lang/Class;
    .end local v3    # "fontClassConstructor":Ljava/lang/reflect/Constructor;
    .end local v5    # "fontStreamBytes":[Ljava/lang/Object;
    :cond_0
    :goto_0
    return-object v4

    .line 147
    :catch_0
    move-exception v6

    goto :goto_0
.end method

.method public foundFontEngine()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 283
    :try_start_0
    const-string/jumbo v1, "org.icepdf.core.pobjects.fonts.nfont.NFont"

    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    .line 284
    const/4 v1, 0x1

    sput-boolean v1, Lorg/icepdf/index/core/pobjects/fonts/FontFactory;->foundNFont:Z
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 288
    :goto_0
    sget-boolean v1, Lorg/icepdf/index/core/pobjects/fonts/FontFactory;->foundNFont:Z

    if-eqz v1, :cond_0

    sget-boolean v1, Lorg/icepdf/index/core/pobjects/fonts/FontFactory;->awtFontSubstitution:Z

    if-nez v1, :cond_0

    :goto_1
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    .line 285
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public getFont(Lorg/icepdf/index/core/util/Library;Lorg/icepdf/index/core/util/Hashtable;)Lorg/icepdf/index/core/pobjects/fonts/Font;
    .locals 8
    .param p1, "library"    # Lorg/icepdf/index/core/util/Library;
    .param p2, "entries"    # Lorg/icepdf/index/core/util/Hashtable;

    .prologue
    .line 109
    const/4 v4, 0x0

    .line 111
    .local v4, "fontDictionary":Lorg/icepdf/index/core/pobjects/fonts/Font;
    invoke-virtual {p0}, Lorg/icepdf/index/core/pobjects/fonts/FontFactory;->foundFontEngine()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 114
    :try_start_0
    const-string/jumbo v6, "org.icepdf.core.pobjects.fonts.nfont.Font"

    invoke-static {v6}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v2

    .line 115
    .local v2, "fontClass":Ljava/lang/Class;
    const/4 v6, 0x2

    new-array v1, v6, [Ljava/lang/Class;

    const/4 v6, 0x0

    const-class v7, Lorg/icepdf/index/core/util/Library;

    aput-object v7, v1, v6

    const/4 v6, 0x1

    const-class v7, Lorg/icepdf/index/core/util/Hashtable;

    aput-object v7, v1, v6

    .line 116
    .local v1, "fontArgs":[Ljava/lang/Class;
    invoke-virtual {v2, v1}, Ljava/lang/Class;->getDeclaredConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v3

    .line 118
    .local v3, "fontClassConstructor":Ljava/lang/reflect/Constructor;
    const/4 v6, 0x2

    new-array v5, v6, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object p1, v5, v6

    const/4 v6, 0x1

    aput-object p2, v5, v6

    .line 119
    .local v5, "fontUrl":[Ljava/lang/Object;
    invoke-virtual {v3, v5}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    move-object v0, v6

    check-cast v0, Lorg/icepdf/index/core/pobjects/fonts/Font;

    move-object v4, v0
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 128
    .end local v1    # "fontArgs":[Ljava/lang/Class;
    .end local v2    # "fontClass":Ljava/lang/Class;
    .end local v3    # "fontClassConstructor":Ljava/lang/reflect/Constructor;
    .end local v5    # "fontUrl":[Ljava/lang/Object;
    :goto_0
    return-object v4

    .line 125
    :cond_0
    new-instance v4, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;

    .end local v4    # "fontDictionary":Lorg/icepdf/index/core/pobjects/fonts/Font;
    invoke-direct {v4, p1, p2}, Lorg/icepdf/index/core/pobjects/fonts/ofont/Font;-><init>(Lorg/icepdf/index/core/util/Library;Lorg/icepdf/index/core/util/Hashtable;)V

    .restart local v4    # "fontDictionary":Lorg/icepdf/index/core/pobjects/fonts/Font;
    goto :goto_0

    .line 120
    :catch_0
    move-exception v6

    goto :goto_0
.end method

.method public isAwtFontSubstitution()Z
    .locals 1

    .prologue
    .line 231
    sget-boolean v0, Lorg/icepdf/index/core/pobjects/fonts/FontFactory;->awtFontSubstitution:Z

    return v0
.end method

.method public setAwtFontSubstitution(Z)V
    .locals 0
    .param p1, "awtFontSubstitution"    # Z

    .prologue
    .line 235
    sput-boolean p1, Lorg/icepdf/index/core/pobjects/fonts/FontFactory;->awtFontSubstitution:Z

    .line 236
    return-void
.end method

.method public toggleAwtFontSubstitution()V
    .locals 1

    .prologue
    .line 239
    sget-boolean v0, Lorg/icepdf/index/core/pobjects/fonts/FontFactory;->awtFontSubstitution:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/icepdf/index/core/pobjects/fonts/FontFactory;->awtFontSubstitution:Z

    .line 240
    return-void

    .line 239
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
