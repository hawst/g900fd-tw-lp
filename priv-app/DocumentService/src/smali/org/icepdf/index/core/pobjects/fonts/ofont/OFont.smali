.class public Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;
.super Ljava/lang/Object;
.source "OFont.java"

# interfaces
.implements Lorg/icepdf/index/core/pobjects/fonts/FontFile;


# instance fields
.field private final androidFont:Landroid/graphics/Paint;

.field private final androidTypeface:Landroid/graphics/Typeface;

.field protected ascent:F

.field private final at:Landroid/graphics/Matrix;

.field protected cMap:[C

.field protected cidWidths:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field protected descent:F

.field private echarAdvanceCache:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lorg/icepdf/index/java/awt/geom/Point2D$Float;",
            ">;"
        }
    .end annotation
.end field

.field protected encoding:Lorg/icepdf/index/core/pobjects/fonts/Encoding;

.field private final family:Ljava/lang/String;

.field protected firstCh:I

.field private maxCharBounds:Lorg/icepdf/index/core/pobjects/Rectangle2Df;

.field protected missingWidth:F

.field private final name:Ljava/lang/String;

.field protected toUnicode:Lorg/icepdf/index/core/pobjects/fonts/CMap;

.field protected widths:[F


# direct methods
.method public constructor <init>(Landroid/graphics/Typeface;Ljava/lang/String;Ljava/lang/String;F)V
    .locals 3
    .param p1, "androidTypeface"    # Landroid/graphics/Typeface;
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "family"    # Ljava/lang/String;
    .param p4, "size"    # F

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    .line 116
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;->at:Landroid/graphics/Matrix;

    .line 57
    new-instance v0, Lorg/icepdf/index/core/pobjects/Rectangle2Df;

    invoke-direct {v0, v1, v1, v2, v2}, Lorg/icepdf/index/core/pobjects/Rectangle2Df;-><init>(FFFF)V

    iput-object v0, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;->maxCharBounds:Lorg/icepdf/index/core/pobjects/Rectangle2Df;

    .line 117
    iput-object p1, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;->androidTypeface:Landroid/graphics/Typeface;

    .line 118
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;->androidFont:Landroid/graphics/Paint;

    .line 119
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;->androidFont:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 120
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;->androidFont:Landroid/graphics/Paint;

    invoke-virtual {v0, p4}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 121
    iput-object p2, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;->name:Ljava/lang/String;

    .line 122
    iput-object p3, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;->family:Ljava/lang/String;

    .line 123
    new-instance v0, Ljava/util/HashMap;

    const/16 v1, 0x100

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;->echarAdvanceCache:Ljava/util/HashMap;

    .line 125
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 8
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    const/high16 v7, 0x3f800000    # 1.0f

    const/4 v6, 0x0

    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    iput-object v5, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;->at:Landroid/graphics/Matrix;

    .line 57
    new-instance v5, Lorg/icepdf/index/core/pobjects/Rectangle2Df;

    invoke-direct {v5, v6, v6, v7, v7}, Lorg/icepdf/index/core/pobjects/Rectangle2Df;-><init>(FFFF)V

    iput-object v5, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;->maxCharBounds:Lorg/icepdf/index/core/pobjects/Rectangle2Df;

    .line 77
    new-instance v4, Ljava/util/StringTokenizer;

    const-string/jumbo v5, "-"

    invoke-direct {v4, p1, v5}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 78
    .local v4, "tokenizer":Ljava/util/StringTokenizer;
    invoke-virtual {v4}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v1

    .line 79
    .local v1, "nameToken":Ljava/lang/String;
    const-string/jumbo v2, "12"

    .line 81
    .local v2, "sizeToken":Ljava/lang/String;
    invoke-virtual {v4}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 82
    invoke-virtual {v4}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v3

    .line 84
    .local v3, "token":Ljava/lang/String;
    const/4 v5, 0x0

    invoke-virtual {v3, v5}, Ljava/lang/String;->charAt(I)C

    move-result v5

    invoke-static {v5}, Ljava/lang/Character;->isDigit(C)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 85
    move-object v2, v3

    .line 94
    .end local v3    # "token":Ljava/lang/String;
    :cond_0
    :goto_0
    new-instance v5, Landroid/graphics/Paint;

    const/4 v6, 0x1

    invoke-direct {v5, v6}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v5, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;->androidFont:Landroid/graphics/Paint;

    .line 95
    invoke-static {p1}, Lorg/icepdf/index/core/util/FontUtil;->guessAndroidFontStyle(Ljava/lang/String;)I

    move-result v5

    invoke-static {v1, v5}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v5

    iput-object v5, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;->androidTypeface:Landroid/graphics/Typeface;

    .line 96
    iget-object v5, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;->androidFont:Landroid/graphics/Paint;

    iget-object v6, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;->androidTypeface:Landroid/graphics/Typeface;

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 98
    :try_start_0
    iget-object v5, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;->androidFont:Landroid/graphics/Paint;

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    int-to-float v6, v6

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setTextSize(F)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 102
    :goto_1
    iput-object v1, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;->name:Ljava/lang/String;

    .line 103
    iput-object v1, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;->family:Ljava/lang/String;

    .line 104
    new-instance v5, Ljava/util/HashMap;

    const/16 v6, 0x100

    invoke-direct {v5, v6}, Ljava/util/HashMap;-><init>(I)V

    iput-object v5, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;->echarAdvanceCache:Ljava/util/HashMap;

    .line 106
    return-void

    .line 87
    .restart local v3    # "token":Ljava/lang/String;
    :cond_1
    invoke-virtual {v4}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 88
    invoke-virtual {v4}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 99
    .end local v3    # "token":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 100
    .local v0, "e":Ljava/lang/NumberFormatException;
    const-string/jumbo v5, "DocumentService"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "Exception :"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Ljava/lang/NumberFormatException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method private constructor <init>(Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;)V
    .locals 3
    .param p1, "font"    # Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    .line 127
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;->at:Landroid/graphics/Matrix;

    .line 57
    new-instance v0, Lorg/icepdf/index/core/pobjects/Rectangle2Df;

    invoke-direct {v0, v1, v1, v2, v2}, Lorg/icepdf/index/core/pobjects/Rectangle2Df;-><init>(FFFF)V

    iput-object v0, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;->maxCharBounds:Lorg/icepdf/index/core/pobjects/Rectangle2Df;

    .line 128
    iget-object v0, p1, Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;->echarAdvanceCache:Ljava/util/HashMap;

    iput-object v0, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;->echarAdvanceCache:Ljava/util/HashMap;

    .line 129
    new-instance v0, Landroid/graphics/Paint;

    iget-object v1, p1, Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;->androidFont:Landroid/graphics/Paint;

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(Landroid/graphics/Paint;)V

    iput-object v0, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;->androidFont:Landroid/graphics/Paint;

    .line 130
    iget-object v0, p1, Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;->androidTypeface:Landroid/graphics/Typeface;

    iput-object v0, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;->androidTypeface:Landroid/graphics/Typeface;

    .line 131
    iget-object v0, p1, Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;->name:Ljava/lang/String;

    iput-object v0, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;->name:Ljava/lang/String;

    .line 132
    iget-object v0, p1, Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;->family:Ljava/lang/String;

    iput-object v0, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;->family:Ljava/lang/String;

    .line 133
    iget-object v0, p1, Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;->encoding:Lorg/icepdf/index/core/pobjects/fonts/Encoding;

    iput-object v0, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;->encoding:Lorg/icepdf/index/core/pobjects/fonts/Encoding;

    .line 134
    iget-object v0, p1, Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;->toUnicode:Lorg/icepdf/index/core/pobjects/fonts/CMap;

    iput-object v0, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;->toUnicode:Lorg/icepdf/index/core/pobjects/fonts/CMap;

    .line 135
    iget v0, p1, Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;->missingWidth:F

    iput v0, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;->missingWidth:F

    .line 136
    iget v0, p1, Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;->firstCh:I

    iput v0, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;->firstCh:I

    .line 137
    iget v0, p1, Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;->ascent:F

    iput v0, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;->ascent:F

    .line 138
    iget v0, p1, Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;->descent:F

    iput v0, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;->descent:F

    .line 139
    iget-object v0, p1, Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;->widths:[F

    iput-object v0, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;->widths:[F

    .line 140
    iget-object v0, p1, Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;->cidWidths:Ljava/util/Map;

    iput-object v0, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;->cidWidths:Ljava/util/Map;

    .line 141
    iget-object v0, p1, Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;->cMap:[C

    iput-object v0, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;->cMap:[C

    .line 142
    iget-object v0, p1, Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;->maxCharBounds:Lorg/icepdf/index/core/pobjects/Rectangle2Df;

    iput-object v0, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;->maxCharBounds:Lorg/icepdf/index/core/pobjects/Rectangle2Df;

    .line 143
    return-void
.end method

.method private getCMapping(C)C
    .locals 1
    .param p1, "currentChar"    # C

    .prologue
    .line 294
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;->toUnicode:Lorg/icepdf/index/core/pobjects/fonts/CMap;

    if-eqz v0, :cond_0

    .line 295
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;->toUnicode:Lorg/icepdf/index/core/pobjects/fonts/CMap;

    invoke-interface {v0, p1}, Lorg/icepdf/index/core/pobjects/fonts/CMap;->toSelector(C)C

    move-result p1

    .line 297
    .end local p1    # "currentChar":C
    :cond_0
    return p1
.end method

.method private getCharDiff(C)C
    .locals 1
    .param p1, "character"    # C

    .prologue
    .line 307
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;->cMap:[C

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;->cMap:[C

    array-length v0, v0

    if-ge p1, v0, :cond_0

    .line 308
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;->cMap:[C

    aget-char p1, v0, p1

    .line 310
    .end local p1    # "character":C
    :cond_0
    return p1
.end method


# virtual methods
.method public canDisplayEchar(C)Z
    .locals 1
    .param p1, "ech"    # C

    .prologue
    .line 211
    const/4 v0, 0x1

    return v0
.end method

.method public deriveFont(F)Lorg/icepdf/index/core/pobjects/fonts/FontFile;
    .locals 2
    .param p1, "pointsize"    # F

    .prologue
    .line 215
    new-instance v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;

    invoke-direct {v0, p0}, Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;-><init>(Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;)V

    .line 217
    .local v0, "font":Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;
    invoke-virtual {v0}, Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;->getSize()F

    move-result v1

    cmpl-float v1, v1, p1

    if-eqz v1, :cond_0

    .line 218
    iget-object v1, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;->echarAdvanceCache:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->clear()V

    .line 222
    :cond_0
    iget-object v1, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;->maxCharBounds:Lorg/icepdf/index/core/pobjects/Rectangle2Df;

    iput-object v1, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;->maxCharBounds:Lorg/icepdf/index/core/pobjects/Rectangle2Df;

    .line 223
    return-object v0
.end method

.method public deriveFont(Landroid/graphics/Matrix;)Lorg/icepdf/index/core/pobjects/fonts/FontFile;
    .locals 1
    .param p1, "at"    # Landroid/graphics/Matrix;

    .prologue
    .line 198
    new-instance v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;

    invoke-direct {v0, p0}, Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;-><init>(Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;)V

    .line 207
    .local v0, "font":Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;
    return-object v0
.end method

.method public deriveFont(Lorg/icepdf/index/core/pobjects/fonts/Encoding;Lorg/icepdf/index/core/pobjects/fonts/CMap;)Lorg/icepdf/index/core/pobjects/fonts/FontFile;
    .locals 2
    .param p1, "encoding"    # Lorg/icepdf/index/core/pobjects/fonts/Encoding;
    .param p2, "toUnicode"    # Lorg/icepdf/index/core/pobjects/fonts/CMap;

    .prologue
    .line 155
    new-instance v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;

    invoke-direct {v0, p0}, Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;-><init>(Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;)V

    .line 156
    .local v0, "font":Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;
    iget-object v1, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;->echarAdvanceCache:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->clear()V

    .line 157
    iput-object p1, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;->encoding:Lorg/icepdf/index/core/pobjects/fonts/Encoding;

    .line 158
    iput-object p2, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;->toUnicode:Lorg/icepdf/index/core/pobjects/fonts/CMap;

    .line 159
    return-object v0
.end method

.method public deriveFont(Lorg/icepdf/index/core/pobjects/fonts/Encoding;Lorg/icepdf/index/core/pobjects/fonts/CMap;[C)Lorg/icepdf/index/core/pobjects/fonts/FontFile;
    .locals 2
    .param p1, "encoding"    # Lorg/icepdf/index/core/pobjects/fonts/Encoding;
    .param p2, "toUnicode"    # Lorg/icepdf/index/core/pobjects/fonts/CMap;
    .param p3, "diff"    # [C

    .prologue
    .line 163
    new-instance v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;

    invoke-direct {v0, p0}, Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;-><init>(Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;)V

    .line 164
    .local v0, "font":Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;
    iget-object v1, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;->echarAdvanceCache:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->clear()V

    .line 165
    iput-object p1, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;->encoding:Lorg/icepdf/index/core/pobjects/fonts/Encoding;

    .line 166
    iput-object p2, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;->toUnicode:Lorg/icepdf/index/core/pobjects/fonts/CMap;

    .line 167
    iput-object p3, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;->cMap:[C

    .line 168
    return-object v0
.end method

.method public drawEstring(Landroid/graphics/Canvas;Ljava/lang/String;FFJII)V
    .locals 7
    .param p1, "g"    # Landroid/graphics/Canvas;
    .param p2, "displayText"    # Ljava/lang/String;
    .param p3, "x"    # F
    .param p4, "y"    # F
    .param p5, "layout"    # J
    .param p7, "mode"    # I
    .param p8, "strokecolor"    # I

    .prologue
    .line 392
    invoke-virtual {p0, p2}, Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;->toUnicode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    .line 394
    if-eqz p7, :cond_0

    const/4 v0, 0x2

    if-eq v0, p7, :cond_0

    const/4 v0, 0x4

    if-eq v0, p7, :cond_0

    const/4 v0, 0x6

    if-ne v0, p7, :cond_1

    .line 397
    :cond_0
    const/4 v2, 0x0

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v3

    iget-object v6, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;->androidFont:Landroid/graphics/Paint;

    move-object v0, p1

    move-object v1, p2

    move v4, p3

    move v5, p4

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;IIFFLandroid/graphics/Paint;)V

    .line 399
    :cond_1
    const/4 v0, 0x1

    if-eq v0, p7, :cond_2

    const/4 v0, 0x2

    if-eq v0, p7, :cond_2

    const/4 v0, 0x5

    if-eq v0, p7, :cond_2

    const/4 v0, 0x6

    if-ne v0, p7, :cond_3

    .line 402
    :cond_2
    const/4 v2, 0x0

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v3

    iget-object v6, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;->androidFont:Landroid/graphics/Paint;

    move-object v0, p1

    move-object v1, p2

    move v4, p3

    move v5, p4

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;IIFFLandroid/graphics/Paint;)V

    .line 405
    :cond_3
    return-void
.end method

.method public echarAdvance(C)Lorg/icepdf/index/java/awt/geom/Point2D;
    .locals 11
    .param p1, "ech"    # C

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 233
    invoke-static {p1}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v7

    .line 234
    .local v7, "text":Ljava/lang/String;
    iget-object v8, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;->echarAdvanceCache:Ljava/util/HashMap;

    invoke-virtual {v8, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/icepdf/index/java/awt/geom/Point2D$Float;

    .line 237
    .local v5, "echarAdvance":Lorg/icepdf/index/java/awt/geom/Point2D$Float;
    if-nez v5, :cond_0

    .line 241
    invoke-direct {p0, p1}, Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;->getCMapping(C)C

    move-result v4

    .line 244
    .local v4, "echGlyph":C
    iget-object v8, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;->androidFont:Landroid/graphics/Paint;

    invoke-virtual {v8}, Landroid/graphics/Paint;->getFontMetrics()Landroid/graphics/Paint$FontMetrics;

    move-result-object v6

    .line 245
    .local v6, "metrics":Landroid/graphics/Paint$FontMetrics;
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    .line 246
    .local v2, "bounds":Landroid/graphics/Rect;
    new-array v3, v10, [C

    aput-char v4, v3, v9

    .line 248
    .local v3, "chars":[C
    iget-object v8, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;->androidFont:Landroid/graphics/Paint;

    invoke-virtual {v8, v3, v9, v10, v2}, Landroid/graphics/Paint;->getTextBounds([CIILandroid/graphics/Rect;)V

    .line 250
    iget-object v8, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;->maxCharBounds:Lorg/icepdf/index/core/pobjects/Rectangle2Df;

    const/4 v9, 0x0

    iput v9, v8, Lorg/icepdf/index/core/pobjects/Rectangle2Df;->x:F

    .line 251
    iget-object v8, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;->maxCharBounds:Lorg/icepdf/index/core/pobjects/Rectangle2Df;

    iget v9, v6, Landroid/graphics/Paint$FontMetrics;->ascent:F

    iput v9, v8, Lorg/icepdf/index/core/pobjects/Rectangle2Df;->y:F

    .line 252
    iget-object v8, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;->maxCharBounds:Lorg/icepdf/index/core/pobjects/Rectangle2Df;

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v9

    int-to-float v9, v9

    iput v9, v8, Lorg/icepdf/index/core/pobjects/Rectangle2Df;->width:F

    .line 253
    iget-object v8, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;->maxCharBounds:Lorg/icepdf/index/core/pobjects/Rectangle2Df;

    iget v9, v2, Landroid/graphics/Rect;->bottom:I

    int-to-float v9, v9

    iput v9, v8, Lorg/icepdf/index/core/pobjects/Rectangle2Df;->height:F

    .line 255
    iget v8, v6, Landroid/graphics/Paint$FontMetrics;->ascent:F

    iput v8, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;->ascent:F

    .line 256
    iget v8, v6, Landroid/graphics/Paint$FontMetrics;->descent:F

    iput v8, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;->descent:F

    .line 259
    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v8

    int-to-float v0, v8

    .line 260
    .local v0, "advance":F
    const/4 v1, 0x0

    .line 262
    .local v1, "advanceY":F
    iget-object v8, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;->echarAdvanceCache:Ljava/util/HashMap;

    new-instance v9, Lorg/icepdf/index/java/awt/geom/Point2D$Float;

    invoke-direct {v9, v0, v1}, Lorg/icepdf/index/java/awt/geom/Point2D$Float;-><init>(FF)V

    invoke-virtual {v8, v7, v9}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 283
    .end local v2    # "bounds":Landroid/graphics/Rect;
    .end local v3    # "chars":[C
    .end local v4    # "echGlyph":C
    .end local v6    # "metrics":Landroid/graphics/Paint$FontMetrics;
    :goto_0
    new-instance v8, Lorg/icepdf/index/java/awt/geom/Point2D$Float;

    invoke-direct {v8, v0, v1}, Lorg/icepdf/index/java/awt/geom/Point2D$Float;-><init>(FF)V

    return-object v8

    .line 265
    .end local v0    # "advance":F
    .end local v1    # "advanceY":F
    :cond_0
    iget v0, v5, Lorg/icepdf/index/java/awt/geom/Point2D$Float;->x:F

    .line 266
    .restart local v0    # "advance":F
    iget v1, v5, Lorg/icepdf/index/java/awt/geom/Point2D$Float;->y:F

    .restart local v1    # "advanceY":F
    goto :goto_0
.end method

.method public getAscent()D
    .locals 2

    .prologue
    .line 344
    iget v0, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;->ascent:F

    float-to-double v0, v0

    return-wide v0
.end method

.method public getDescent()D
    .locals 2

    .prologue
    .line 348
    iget v0, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;->descent:F

    float-to-double v0, v0

    return-wide v0
.end method

.method public getEstringBounds(Ljava/lang/String;II)Lorg/icepdf/index/core/pobjects/Rectangle2Df;
    .locals 1
    .param p1, "estr"    # Ljava/lang/String;
    .param p2, "beginIndex"    # I
    .param p3, "limit"    # I

    .prologue
    .line 382
    const/4 v0, 0x0

    return-object v0
.end method

.method public getEstringOutline(Ljava/lang/String;FF)Lorg/icepdf/index/java/awt/Shape;
    .locals 1
    .param p1, "displayText"    # Ljava/lang/String;
    .param p2, "x"    # F
    .param p3, "y"    # F

    .prologue
    .line 521
    const/4 v0, 0x0

    .line 550
    .local v0, "shape":Lorg/icepdf/index/java/awt/Shape;
    return-object v0
.end method

.method public getFamily()Ljava/lang/String;
    .locals 1

    .prologue
    .line 336
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;->family:Ljava/lang/String;

    return-object v0
.end method

.method public getFormat()Ljava/lang/String;
    .locals 1

    .prologue
    .line 386
    const/4 v0, 0x0

    return-object v0
.end method

.method public getMaxCharBounds()Lorg/icepdf/index/core/pobjects/Rectangle2Df;
    .locals 1

    .prologue
    .line 352
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;->maxCharBounds:Lorg/icepdf/index/core/pobjects/Rectangle2Df;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 365
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getNumGlyphs()I
    .locals 1

    .prologue
    .line 374
    const/16 v0, 0x100

    return v0
.end method

.method public getRights()I
    .locals 1

    .prologue
    .line 361
    const/4 v0, 0x0

    return v0
.end method

.method public getSize()F
    .locals 2

    .prologue
    .line 340
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;->androidFont:Landroid/graphics/Paint;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->getFontMetrics(Landroid/graphics/Paint$FontMetrics;)F

    move-result v0

    return v0
.end method

.method public getSpaceEchar()C
    .locals 1

    .prologue
    .line 378
    const/16 v0, 0x20

    return v0
.end method

.method public getStyle()I
    .locals 1

    .prologue
    .line 332
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;->androidTypeface:Landroid/graphics/Typeface;

    invoke-virtual {v0}, Landroid/graphics/Typeface;->getStyle()I

    move-result v0

    return v0
.end method

.method public getToUnicode()Lorg/icepdf/index/core/pobjects/fonts/CMap;
    .locals 1

    .prologue
    .line 328
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;->toUnicode:Lorg/icepdf/index/core/pobjects/fonts/CMap;

    return-object v0
.end method

.method public getTransform()Landroid/graphics/Matrix;
    .locals 1

    .prologue
    .line 357
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;->at:Landroid/graphics/Matrix;

    return-object v0
.end method

.method public isHinted()Z
    .locals 1

    .prologue
    .line 369
    const/4 v0, 0x0

    return v0
.end method

.method public toUnicode(C)Ljava/lang/String;
    .locals 2
    .param p1, "c1"    # C

    .prologue
    .line 470
    iget-object v1, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;->toUnicode:Lorg/icepdf/index/core/pobjects/fonts/CMap;

    if-nez v1, :cond_0

    invoke-direct {p0, p1}, Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;->getCharDiff(C)C

    move-result v0

    .line 478
    .local v0, "c":C
    :goto_0
    iget-object v1, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;->toUnicode:Lorg/icepdf/index/core/pobjects/fonts/CMap;

    if-eqz v1, :cond_1

    .line 479
    iget-object v1, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;->toUnicode:Lorg/icepdf/index/core/pobjects/fonts/CMap;

    invoke-interface {v1, v0}, Lorg/icepdf/index/core/pobjects/fonts/CMap;->toUnicode(C)Ljava/lang/String;

    move-result-object v1

    .line 515
    :goto_1
    return-object v1

    .end local v0    # "c":C
    :cond_0
    move v0, p1

    .line 470
    goto :goto_0

    .line 482
    .restart local v0    # "c":C
    :cond_1
    invoke-direct {p0, v0}, Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;->getCMapping(C)C

    move-result v0

    .line 515
    invoke-static {v0}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v1

    goto :goto_1
.end method

.method public toUnicode(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p1, "displayText"    # Ljava/lang/String;

    .prologue
    .line 409
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 410
    .local v3, "sb":Ljava/lang/StringBuffer;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    if-ge v2, v4, :cond_1

    .line 412
    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v1

    .line 417
    .local v1, "c1":C
    iget-object v4, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;->toUnicode:Lorg/icepdf/index/core/pobjects/fonts/CMap;

    if-nez v4, :cond_0

    invoke-direct {p0, v1}, Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;->getCharDiff(C)C

    move-result v0

    .line 424
    .local v0, "c":C
    :goto_1
    invoke-direct {p0, v0}, Lorg/icepdf/index/core/pobjects/fonts/ofont/OFont;->getCMapping(C)C

    move-result v0

    .line 461
    invoke-virtual {v3, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 410
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .end local v0    # "c":C
    :cond_0
    move v0, v1

    .line 417
    goto :goto_1

    .line 463
    .end local v1    # "c1":C
    :cond_1
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4
.end method
