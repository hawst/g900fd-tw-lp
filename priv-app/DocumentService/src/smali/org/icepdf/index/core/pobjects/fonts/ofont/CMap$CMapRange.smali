.class Lorg/icepdf/index/core/pobjects/fonts/ofont/CMap$CMapRange;
.super Ljava/lang/Object;
.source "CMap.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/icepdf/index/core/pobjects/fonts/ofont/CMap;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "CMapRange"
.end annotation


# instance fields
.field endRange:I

.field offsetValue:I

.field offsetVecor:Ljava/util/Vector;

.field startRange:I

.field final synthetic this$0:Lorg/icepdf/index/core/pobjects/fonts/ofont/CMap;


# direct methods
.method public constructor <init>(Lorg/icepdf/index/core/pobjects/fonts/ofont/CMap;III)V
    .locals 1
    .param p2, "startRange"    # I
    .param p3, "endRange"    # I
    .param p4, "offsetValue"    # I

    .prologue
    const/4 v0, 0x0

    .line 517
    iput-object p1, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/CMap$CMapRange;->this$0:Lorg/icepdf/index/core/pobjects/fonts/ofont/CMap;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 501
    iput v0, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/CMap$CMapRange;->startRange:I

    .line 503
    iput v0, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/CMap$CMapRange;->endRange:I

    .line 505
    iput v0, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/CMap$CMapRange;->offsetValue:I

    .line 507
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/CMap$CMapRange;->offsetVecor:Ljava/util/Vector;

    .line 518
    iput p2, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/CMap$CMapRange;->startRange:I

    .line 519
    iput p3, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/CMap$CMapRange;->endRange:I

    .line 520
    iput p4, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/CMap$CMapRange;->offsetValue:I

    .line 521
    return-void
.end method

.method public constructor <init>(Lorg/icepdf/index/core/pobjects/fonts/ofont/CMap;IILjava/util/Vector;)V
    .locals 1
    .param p2, "startRange"    # I
    .param p3, "endRange"    # I
    .param p4, "offsetVecor"    # Ljava/util/Vector;

    .prologue
    const/4 v0, 0x0

    .line 532
    iput-object p1, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/CMap$CMapRange;->this$0:Lorg/icepdf/index/core/pobjects/fonts/ofont/CMap;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 501
    iput v0, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/CMap$CMapRange;->startRange:I

    .line 503
    iput v0, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/CMap$CMapRange;->endRange:I

    .line 505
    iput v0, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/CMap$CMapRange;->offsetValue:I

    .line 507
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/CMap$CMapRange;->offsetVecor:Ljava/util/Vector;

    .line 533
    iput p2, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/CMap$CMapRange;->startRange:I

    .line 534
    iput p3, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/CMap$CMapRange;->endRange:I

    .line 535
    iput-object p4, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/CMap$CMapRange;->offsetVecor:Ljava/util/Vector;

    .line 536
    return-void
.end method


# virtual methods
.method public getCMapValue(I)[C
    .locals 5
    .param p1, "value"    # I

    .prologue
    .line 561
    iget-object v2, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/CMap$CMapRange;->offsetVecor:Ljava/util/Vector;

    if-nez v2, :cond_0

    .line 562
    const/4 v2, 0x1

    new-array v1, v2, [C

    const/4 v2, 0x0

    iget v3, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/CMap$CMapRange;->offsetValue:I

    iget v4, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/CMap$CMapRange;->startRange:I

    sub-int v4, p1, v4

    add-int/2addr v3, v4

    int-to-char v3, v3

    aput-char v3, v1, v2

    .line 568
    :goto_0
    return-object v1

    .line 566
    :cond_0
    iget-object v2, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/CMap$CMapRange;->offsetVecor:Ljava/util/Vector;

    iget v3, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/CMap$CMapRange;->startRange:I

    sub-int v3, p1, v3

    invoke-virtual {v2, v3}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/icepdf/index/core/pobjects/StringObject;

    .line 567
    .local v0, "hexToken":Lorg/icepdf/index/core/pobjects/StringObject;
    iget-object v2, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/CMap$CMapRange;->this$0:Lorg/icepdf/index/core/pobjects/fonts/ofont/CMap;

    invoke-interface {v0}, Lorg/icepdf/index/core/pobjects/StringObject;->getLiteralStringBuffer()Ljava/lang/StringBuffer;

    move-result-object v3

    # invokes: Lorg/icepdf/index/core/pobjects/fonts/ofont/CMap;->convertToString(Ljava/lang/CharSequence;)[C
    invoke-static {v2, v3}, Lorg/icepdf/index/core/pobjects/fonts/ofont/CMap;->access$000(Lorg/icepdf/index/core/pobjects/fonts/ofont/CMap;Ljava/lang/CharSequence;)[C

    move-result-object v1

    .line 568
    .local v1, "test":[C
    goto :goto_0
.end method

.method public inRange(I)Z
    .locals 1
    .param p1, "value"    # I

    .prologue
    .line 546
    iget v0, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/CMap$CMapRange;->startRange:I

    if-lt p1, v0, :cond_0

    iget v0, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/CMap$CMapRange;->endRange:I

    if-gt p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
