.class public Lorg/icepdf/index/core/pobjects/fonts/AFM;
.super Ljava/lang/Object;
.source "AFM.java"


# static fields
.field private static AFMFlags:[I = null

.field public static AFMnames:[Ljava/lang/String; = null

.field public static AFMs:Ljava/util/HashMap; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lorg/icepdf/index/core/pobjects/fonts/AFM;",
            ">;"
        }
    .end annotation
.end field

.field public static final COURIER:I = 0x0

.field public static final COURIER_BOLD:I = 0x1

.field public static final COURIER_BOLD_OBLIQUE:I = 0x3

.field public static final COURIER_OBLIQUE:I = 0x2

.field public static final HELVETICA:I = 0x4

.field public static final HELVETICA_BOLD:I = 0x5

.field public static final HELVETICA_BOLD_OBLIQUE:I = 0x7

.field public static final HELVETICA_OBLIQUE:I = 0x6

.field public static final SYMBOL:I = 0xd

.field public static final TIMES_BOLD:I = 0x9

.field public static final TIMES_BOLD_ITALIC:I = 0xb

.field public static final TIMES_ITALIC:I = 0xa

.field public static final TIMES_ROMAN:I = 0x8

.field public static final ZAPF_DINGBATS:I = 0xc


# instance fields
.field private avgWidth:I

.field private familyName:Ljava/lang/String;

.field private flags:I

.field private fontBBox:[I

.field private fontName:Ljava/lang/String;

.field private fullName:Ljava/lang/String;

.field private italicAngle:F

.field private maxWidth:I

.field private widths:[F


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/16 v6, 0xe

    .line 56
    new-array v3, v6, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string/jumbo v5, "Courier.afm"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string/jumbo v5, "Courier-Bold.afm"

    aput-object v5, v3, v4

    const/4 v4, 0x2

    const-string/jumbo v5, "Courier-Oblique.afm"

    aput-object v5, v3, v4

    const/4 v4, 0x3

    const-string/jumbo v5, "Courier-BoldOblique.afm"

    aput-object v5, v3, v4

    const/4 v4, 0x4

    const-string/jumbo v5, "Helvetica.afm"

    aput-object v5, v3, v4

    const/4 v4, 0x5

    const-string/jumbo v5, "Helvetica-Bold.afm"

    aput-object v5, v3, v4

    const/4 v4, 0x6

    const-string/jumbo v5, "Helvetica-Oblique.afm"

    aput-object v5, v3, v4

    const/4 v4, 0x7

    const-string/jumbo v5, "Helvetica-BoldOblique.afm"

    aput-object v5, v3, v4

    const/16 v4, 0x8

    const-string/jumbo v5, "Times-Roman.afm"

    aput-object v5, v3, v4

    const/16 v4, 0x9

    const-string/jumbo v5, "Times-Bold.afm"

    aput-object v5, v3, v4

    const/16 v4, 0xa

    const-string/jumbo v5, "Times-Italic.afm"

    aput-object v5, v3, v4

    const/16 v4, 0xb

    const-string/jumbo v5, "Times-BoldItalic.afm"

    aput-object v5, v3, v4

    const/16 v4, 0xc

    const-string/jumbo v5, "ZapfDingbats.afm"

    aput-object v5, v3, v4

    const/16 v4, 0xd

    const-string/jumbo v5, "Symbol.afm"

    aput-object v5, v3, v4

    sput-object v3, Lorg/icepdf/index/core/pobjects/fonts/AFM;->AFMnames:[Ljava/lang/String;

    .line 143
    new-array v3, v6, [I

    fill-array-data v3, :array_0

    sput-object v3, Lorg/icepdf/index/core/pobjects/fonts/AFM;->AFMFlags:[I

    .line 164
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3, v6}, Ljava/util/HashMap;-><init>(I)V

    sput-object v3, Lorg/icepdf/index/core/pobjects/fonts/AFM;->AFMs:Ljava/util/HashMap;

    .line 182
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    :try_start_0
    sget-object v3, Lorg/icepdf/index/core/pobjects/fonts/AFM;->AFMnames:[Ljava/lang/String;

    array-length v3, v3

    if-ge v2, v3, :cond_0

    .line 183
    new-instance v0, Lorg/icepdf/index/core/pobjects/fonts/AFM;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "afm/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Lorg/icepdf/index/core/pobjects/fonts/AFM;->AFMnames:[Ljava/lang/String;

    aget-object v4, v4, v2

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v3}, Lorg/icepdf/index/core/pobjects/fonts/AFM;-><init>(Ljava/lang/String;)V

    .line 184
    .local v0, "afm":Lorg/icepdf/index/core/pobjects/fonts/AFM;
    sget-object v3, Lorg/icepdf/index/core/pobjects/fonts/AFM;->AFMFlags:[I

    aget v3, v3, v2

    invoke-direct {v0, v3}, Lorg/icepdf/index/core/pobjects/fonts/AFM;->setFlags(I)V

    .line 185
    sget-object v3, Lorg/icepdf/index/core/pobjects/fonts/AFM;->AFMs:Ljava/util/HashMap;

    iget-object v4, v0, Lorg/icepdf/index/core/pobjects/fonts/AFM;->fontName:Ljava/lang/String;

    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v4, v5}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 182
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 187
    .end local v0    # "afm":Lorg/icepdf/index/core/pobjects/fonts/AFM;
    :catch_0
    move-exception v1

    .line 188
    .local v1, "ex":Ljava/lang/Exception;
    const-string/jumbo v3, "DocumentService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Exception :"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 190
    .end local v1    # "ex":Ljava/lang/Exception;
    :cond_0
    return-void

    .line 143
    :array_0
    .array-data 4
        0x23
        0x23
        0x63
        0x63
        0x20
        0x20
        0x60
        0x60
        0x22
        0x22
        0x62
        0x62
        0x4
        0x4
    .end array-data
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 3
    .param p1, "resource"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 199
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 170
    const/16 v1, 0xff

    new-array v1, v1, [F

    iput-object v1, p0, Lorg/icepdf/index/core/pobjects/fonts/AFM;->widths:[F

    .line 171
    const/4 v1, 0x4

    new-array v1, v1, [I

    iput-object v1, p0, Lorg/icepdf/index/core/pobjects/fonts/AFM;->fontBBox:[I

    .line 172
    const/4 v1, 0x0

    iput v1, p0, Lorg/icepdf/index/core/pobjects/fonts/AFM;->italicAngle:F

    .line 173
    iput v2, p0, Lorg/icepdf/index/core/pobjects/fonts/AFM;->maxWidth:I

    .line 174
    iput v2, p0, Lorg/icepdf/index/core/pobjects/fonts/AFM;->avgWidth:I

    .line 175
    iput v2, p0, Lorg/icepdf/index/core/pobjects/fonts/AFM;->flags:I

    .line 200
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/Class;->getResourceAsStream(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v0

    .line 202
    .local v0, "in":Ljava/io/InputStream;
    if-eqz v0, :cond_0

    .line 203
    :try_start_0
    new-instance v1, Ljava/io/InputStreamReader;

    invoke-direct {v1, v0}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {p0, v1}, Lorg/icepdf/index/core/pobjects/fonts/AFM;->parse(Ljava/io/Reader;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 206
    :cond_0
    if-eqz v0, :cond_1

    .line 208
    :try_start_1
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 219
    :cond_1
    :goto_0
    return-void

    .line 206
    :catchall_0
    move-exception v1

    if-eqz v0, :cond_2

    .line 208
    :try_start_2
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 211
    :cond_2
    :goto_1
    throw v1

    .line 209
    :catch_0
    move-exception v1

    goto :goto_0

    :catch_1
    move-exception v2

    goto :goto_1
.end method

.method private parse(Ljava/io/Reader;)V
    .locals 11
    .param p1, "i"    # Ljava/io/Reader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v10, 0x0

    .line 269
    new-instance v2, Ljava/io/BufferedReader;

    invoke-direct {v2, p1}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 271
    .local v2, "r":Ljava/io/BufferedReader;
    const/4 v1, 0x0

    .line 272
    .local v1, "count":I
    iput v10, p0, Lorg/icepdf/index/core/pobjects/fonts/AFM;->avgWidth:I

    .line 273
    iput v10, p0, Lorg/icepdf/index/core/pobjects/fonts/AFM;->maxWidth:I

    .line 274
    :cond_0
    :goto_0
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v3

    .local v3, "s":Ljava/lang/String;
    if-eqz v3, :cond_8

    .line 275
    new-instance v5, Ljava/util/StringTokenizer;

    const-string/jumbo v7, " ;\t\n\r\u000c"

    invoke-direct {v5, v3, v7}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 276
    .local v5, "st":Ljava/util/StringTokenizer;
    invoke-virtual {v5}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v4

    .line 277
    .local v4, "s1":Ljava/lang/String;
    const-string/jumbo v7, "FontName"

    invoke-virtual {v4, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 278
    invoke-virtual {v5}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lorg/icepdf/index/core/pobjects/fonts/AFM;->fontName:Ljava/lang/String;

    goto :goto_0

    .line 279
    :cond_1
    const-string/jumbo v7, "FullName"

    invoke-virtual {v4, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 280
    invoke-virtual {v5}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lorg/icepdf/index/core/pobjects/fonts/AFM;->fullName:Ljava/lang/String;

    goto :goto_0

    .line 281
    :cond_2
    const-string/jumbo v7, "FamilyName"

    invoke-virtual {v4, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 282
    invoke-virtual {v5}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lorg/icepdf/index/core/pobjects/fonts/AFM;->familyName:Ljava/lang/String;

    goto :goto_0

    .line 283
    :cond_3
    const-string/jumbo v7, "FontBBox"

    invoke-virtual {v4, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 284
    iget-object v7, p0, Lorg/icepdf/index/core/pobjects/fonts/AFM;->fontBBox:[I

    invoke-virtual {v5}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v8

    aput v8, v7, v10

    .line 285
    iget-object v7, p0, Lorg/icepdf/index/core/pobjects/fonts/AFM;->fontBBox:[I

    const/4 v8, 0x1

    invoke-virtual {v5}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v9

    aput v9, v7, v8

    .line 286
    iget-object v7, p0, Lorg/icepdf/index/core/pobjects/fonts/AFM;->fontBBox:[I

    const/4 v8, 0x2

    invoke-virtual {v5}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v9

    aput v9, v7, v8

    .line 287
    iget-object v7, p0, Lorg/icepdf/index/core/pobjects/fonts/AFM;->fontBBox:[I

    const/4 v8, 0x3

    invoke-virtual {v5}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v9

    aput v9, v7, v8

    goto/16 :goto_0

    .line 288
    :cond_4
    const-string/jumbo v7, "ItalicAngle"

    invoke-virtual {v4, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 289
    invoke-virtual {v5}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Float;->floatValue()F

    move-result v7

    iput v7, p0, Lorg/icepdf/index/core/pobjects/fonts/AFM;->italicAngle:F

    goto/16 :goto_0

    .line 292
    :cond_5
    const-string/jumbo v7, "C"

    invoke-virtual {v4, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 293
    invoke-virtual {v5}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 294
    .local v0, "c":I
    :cond_6
    invoke-virtual {v5}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v7

    const-string/jumbo v8, "WX"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 295
    invoke-virtual {v5}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    .line 296
    .local v6, "wx":I
    if-ltz v0, :cond_0

    const/16 v7, 0xff

    if-ge v0, v7, :cond_0

    .line 297
    iget-object v7, p0, Lorg/icepdf/index/core/pobjects/fonts/AFM;->widths:[F

    int-to-float v8, v6

    aput v8, v7, v1

    .line 299
    iget v7, p0, Lorg/icepdf/index/core/pobjects/fonts/AFM;->maxWidth:I

    if-le v6, v7, :cond_7

    .line 300
    iput v6, p0, Lorg/icepdf/index/core/pobjects/fonts/AFM;->maxWidth:I

    .line 303
    :cond_7
    iget v7, p0, Lorg/icepdf/index/core/pobjects/fonts/AFM;->avgWidth:I

    add-int/2addr v7, v6

    iput v7, p0, Lorg/icepdf/index/core/pobjects/fonts/AFM;->avgWidth:I

    .line 304
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_0

    .line 309
    .end local v0    # "c":I
    .end local v4    # "s1":Ljava/lang/String;
    .end local v5    # "st":Ljava/util/StringTokenizer;
    .end local v6    # "wx":I
    :cond_8
    iget v7, p0, Lorg/icepdf/index/core/pobjects/fonts/AFM;->avgWidth:I

    div-int/2addr v7, v1

    iput v7, p0, Lorg/icepdf/index/core/pobjects/fonts/AFM;->avgWidth:I

    .line 310
    return-void
.end method

.method private setFlags(I)V
    .locals 0
    .param p1, "value"    # I

    .prologue
    .line 258
    iput p1, p0, Lorg/icepdf/index/core/pobjects/fonts/AFM;->flags:I

    .line 259
    return-void
.end method


# virtual methods
.method public getAvgWidth()I
    .locals 1

    .prologue
    .line 250
    iget v0, p0, Lorg/icepdf/index/core/pobjects/fonts/AFM;->avgWidth:I

    return v0
.end method

.method public getFamilyName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 230
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/fonts/AFM;->familyName:Ljava/lang/String;

    return-object v0
.end method

.method public getFlags()I
    .locals 1

    .prologue
    .line 254
    iget v0, p0, Lorg/icepdf/index/core/pobjects/fonts/AFM;->flags:I

    return v0
.end method

.method public getFontBBox()[I
    .locals 1

    .prologue
    .line 234
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/fonts/AFM;->fontBBox:[I

    return-object v0
.end method

.method public getFontName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 222
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/fonts/AFM;->fontName:Ljava/lang/String;

    return-object v0
.end method

.method public getFullName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 226
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/fonts/AFM;->fullName:Ljava/lang/String;

    return-object v0
.end method

.method public getItalicAngle()F
    .locals 1

    .prologue
    .line 238
    iget v0, p0, Lorg/icepdf/index/core/pobjects/fonts/AFM;->italicAngle:F

    return v0
.end method

.method public getMaxWidth()I
    .locals 1

    .prologue
    .line 246
    iget v0, p0, Lorg/icepdf/index/core/pobjects/fonts/AFM;->maxWidth:I

    return v0
.end method

.method public getWidths()[F
    .locals 1

    .prologue
    .line 242
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/fonts/AFM;->widths:[F

    return-object v0
.end method
