.class public Lorg/icepdf/index/core/pobjects/PDimension;
.super Ljava/lang/Object;
.source "PDimension.java"


# instance fields
.field private height:F

.field private width:F


# direct methods
.method public constructor <init>(FF)V
    .locals 0
    .param p1, "w"    # F
    .param p2, "h"    # F

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    invoke-virtual {p0, p1, p2}, Lorg/icepdf/index/core/pobjects/PDimension;->set(FF)V

    .line 38
    return-void
.end method

.method public constructor <init>(II)V
    .locals 0
    .param p1, "w"    # I
    .param p2, "h"    # I

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    invoke-virtual {p0, p1, p2}, Lorg/icepdf/index/core/pobjects/PDimension;->set(II)V

    .line 48
    return-void
.end method


# virtual methods
.method public getHeight()F
    .locals 1

    .prologue
    .line 87
    iget v0, p0, Lorg/icepdf/index/core/pobjects/PDimension;->height:F

    return v0
.end method

.method public getWidth()F
    .locals 1

    .prologue
    .line 78
    iget v0, p0, Lorg/icepdf/index/core/pobjects/PDimension;->width:F

    return v0
.end method

.method public set(FF)V
    .locals 0
    .param p1, "w"    # F
    .param p2, "h"    # F

    .prologue
    .line 57
    iput p1, p0, Lorg/icepdf/index/core/pobjects/PDimension;->width:F

    .line 58
    iput p2, p0, Lorg/icepdf/index/core/pobjects/PDimension;->height:F

    .line 59
    return-void
.end method

.method public set(II)V
    .locals 1
    .param p1, "w"    # I
    .param p2, "h"    # I

    .prologue
    .line 68
    int-to-float v0, p1

    iput v0, p0, Lorg/icepdf/index/core/pobjects/PDimension;->width:F

    .line 69
    int-to-float v0, p2

    iput v0, p0, Lorg/icepdf/index/core/pobjects/PDimension;->height:F

    .line 70
    return-void
.end method

.method public toDimension()Landroid/graphics/Point;
    .locals 3

    .prologue
    .line 98
    new-instance v0, Landroid/graphics/Point;

    iget v1, p0, Lorg/icepdf/index/core/pobjects/PDimension;->width:F

    float-to-int v1, v1

    iget v2, p0, Lorg/icepdf/index/core/pobjects/PDimension;->height:F

    float-to-int v2, v2

    invoke-direct {v0, v1, v2}, Landroid/graphics/Point;-><init>(II)V

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 107
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "PDimension { width="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lorg/icepdf/index/core/pobjects/PDimension;->width:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", height="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lorg/icepdf/index/core/pobjects/PDimension;->height:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " }"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
