.class public Lorg/icepdf/index/core/pobjects/PRectangle;
.super Lorg/icepdf/index/core/pobjects/Rectangle2Df;
.source "PRectangle.java"


# instance fields
.field private p1x:F

.field private p1y:F

.field private p2x:F

.field private p2y:F


# direct methods
.method private constructor <init>(FFFF)V
    .locals 0
    .param p1, "x"    # F
    .param p2, "y"    # F
    .param p3, "width"    # F
    .param p4, "height"    # F

    .prologue
    .line 79
    invoke-direct {p0, p1, p2, p3, p4}, Lorg/icepdf/index/core/pobjects/Rectangle2Df;-><init>(FFFF)V

    .line 80
    return-void
.end method

.method public constructor <init>(Landroid/graphics/PointF;Landroid/graphics/PointF;)V
    .locals 4
    .param p1, "p1"    # Landroid/graphics/PointF;
    .param p2, "p2"    # Landroid/graphics/PointF;

    .prologue
    .line 60
    invoke-direct {p0}, Lorg/icepdf/index/core/pobjects/Rectangle2Df;-><init>()V

    .line 61
    iget v0, p1, Landroid/graphics/PointF;->x:F

    iget v1, p1, Landroid/graphics/PointF;->y:F

    iget v2, p2, Landroid/graphics/PointF;->x:F

    iget v3, p2, Landroid/graphics/PointF;->y:F

    invoke-direct {p0, v0, v1, v2, v3}, Lorg/icepdf/index/core/pobjects/PRectangle;->normalizeCoordinates(FFFF)V

    .line 63
    iget v0, p1, Landroid/graphics/PointF;->x:F

    iput v0, p0, Lorg/icepdf/index/core/pobjects/PRectangle;->p1x:F

    .line 64
    iget v0, p1, Landroid/graphics/PointF;->y:F

    iput v0, p0, Lorg/icepdf/index/core/pobjects/PRectangle;->p1y:F

    .line 65
    iget v0, p2, Landroid/graphics/PointF;->x:F

    iput v0, p0, Lorg/icepdf/index/core/pobjects/PRectangle;->p2x:F

    .line 66
    iget v0, p2, Landroid/graphics/PointF;->y:F

    iput v0, p0, Lorg/icepdf/index/core/pobjects/PRectangle;->p2y:F

    .line 67
    return-void
.end method

.method public constructor <init>(Ljava/util/Vector;)V
    .locals 6
    .param p1, "coordinates"    # Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 94
    invoke-direct {p0}, Lorg/icepdf/index/core/pobjects/Rectangle2Df;-><init>()V

    .line 95
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/util/Vector;->size()I

    move-result v4

    const/4 v5, 0x4

    if-ge v4, v5, :cond_1

    .line 96
    :cond_0
    new-instance v4, Ljava/lang/IllegalArgumentException;

    invoke-direct {v4}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v4

    .line 97
    :cond_1
    const/4 v4, 0x0

    invoke-virtual {p1, v4}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Number;

    invoke-virtual {v4}, Ljava/lang/Number;->floatValue()F

    move-result v0

    .line 98
    .local v0, "x1":F
    const/4 v4, 0x1

    invoke-virtual {p1, v4}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Number;

    invoke-virtual {v4}, Ljava/lang/Number;->floatValue()F

    move-result v2

    .line 100
    .local v2, "y1":F
    const/4 v4, 0x2

    invoke-virtual {p1, v4}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Number;

    invoke-virtual {v4}, Ljava/lang/Number;->floatValue()F

    move-result v1

    .line 101
    .local v1, "x2":F
    const/4 v4, 0x3

    invoke-virtual {p1, v4}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Number;

    invoke-virtual {v4}, Ljava/lang/Number;->floatValue()F

    move-result v3

    .line 104
    .local v3, "y2":F
    iput v0, p0, Lorg/icepdf/index/core/pobjects/PRectangle;->p1x:F

    .line 105
    iput v2, p0, Lorg/icepdf/index/core/pobjects/PRectangle;->p1y:F

    .line 106
    iput v1, p0, Lorg/icepdf/index/core/pobjects/PRectangle;->p2x:F

    .line 107
    iput v3, p0, Lorg/icepdf/index/core/pobjects/PRectangle;->p2y:F

    .line 110
    invoke-direct {p0, v0, v2, v1, v3}, Lorg/icepdf/index/core/pobjects/PRectangle;->normalizeCoordinates(FFFF)V

    .line 111
    return-void
.end method

.method private normalizeCoordinates(FFFF)V
    .locals 5
    .param p1, "x1"    # F
    .param p2, "y1"    # F
    .param p3, "x2"    # F
    .param p4, "y2"    # F

    .prologue
    .line 175
    move v2, p1

    .local v2, "x":F
    move v3, p2

    .line 176
    .local v3, "y":F
    sub-float v4, p3, p1

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v1

    .line 177
    .local v1, "w":F
    sub-float v4, p4, p2

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v0

    .line 179
    .local v0, "h":F
    cmpl-float v4, p1, p3

    if-lez v4, :cond_0

    .line 180
    move v2, p3

    .line 183
    :cond_0
    cmpg-float v4, p2, p4

    if-gez v4, :cond_1

    .line 184
    move v3, p4

    .line 186
    :cond_1
    invoke-virtual {p0, v2, v3, v1, v0}, Lorg/icepdf/index/core/pobjects/PRectangle;->setRect(FFFF)V

    .line 187
    return-void
.end method


# virtual methods
.method public createCartesianIntersection(Lorg/icepdf/index/core/pobjects/PRectangle;)Lorg/icepdf/index/core/pobjects/PRectangle;
    .locals 10
    .param p1, "src2"    # Lorg/icepdf/index/core/pobjects/PRectangle;

    .prologue
    const/4 v9, 0x0

    .line 123
    new-instance v0, Lorg/icepdf/index/core/pobjects/PRectangle;

    iget v5, p1, Lorg/icepdf/index/core/pobjects/PRectangle;->x:F

    iget v6, p1, Lorg/icepdf/index/core/pobjects/PRectangle;->y:F

    iget v7, p1, Lorg/icepdf/index/core/pobjects/PRectangle;->width:F

    iget v8, p1, Lorg/icepdf/index/core/pobjects/PRectangle;->height:F

    invoke-direct {v0, v5, v6, v7, v8}, Lorg/icepdf/index/core/pobjects/PRectangle;-><init>(FFFF)V

    .line 124
    .local v0, "rec":Lorg/icepdf/index/core/pobjects/PRectangle;
    iget v5, p0, Lorg/icepdf/index/core/pobjects/PRectangle;->x:F

    iget v6, v0, Lorg/icepdf/index/core/pobjects/PRectangle;->x:F

    cmpl-float v5, v5, v6

    if-lez v5, :cond_2

    iget v1, p0, Lorg/icepdf/index/core/pobjects/PRectangle;->x:F

    .line 125
    .local v1, "xLeft":F
    :goto_0
    iget v5, p0, Lorg/icepdf/index/core/pobjects/PRectangle;->x:F

    iget v6, p0, Lorg/icepdf/index/core/pobjects/PRectangle;->width:F

    add-float/2addr v5, v6

    iget v6, v0, Lorg/icepdf/index/core/pobjects/PRectangle;->x:F

    iget v7, v0, Lorg/icepdf/index/core/pobjects/PRectangle;->width:F

    add-float/2addr v6, v7

    cmpl-float v5, v5, v6

    if-lez v5, :cond_3

    iget v5, v0, Lorg/icepdf/index/core/pobjects/PRectangle;->x:F

    iget v6, v0, Lorg/icepdf/index/core/pobjects/PRectangle;->width:F

    add-float v2, v5, v6

    .line 128
    .local v2, "xRight":F
    :goto_1
    iget v5, p0, Lorg/icepdf/index/core/pobjects/PRectangle;->y:F

    iget v6, p0, Lorg/icepdf/index/core/pobjects/PRectangle;->height:F

    sub-float/2addr v5, v6

    iget v6, v0, Lorg/icepdf/index/core/pobjects/PRectangle;->y:F

    iget v7, v0, Lorg/icepdf/index/core/pobjects/PRectangle;->height:F

    sub-float/2addr v6, v7

    cmpg-float v5, v5, v6

    if-gez v5, :cond_4

    iget v5, v0, Lorg/icepdf/index/core/pobjects/PRectangle;->y:F

    iget v6, v0, Lorg/icepdf/index/core/pobjects/PRectangle;->height:F

    sub-float v3, v5, v6

    .line 130
    .local v3, "yBottom":F
    :goto_2
    iget v5, p0, Lorg/icepdf/index/core/pobjects/PRectangle;->y:F

    iget v6, v0, Lorg/icepdf/index/core/pobjects/PRectangle;->y:F

    cmpl-float v5, v5, v6

    if-lez v5, :cond_5

    iget v4, v0, Lorg/icepdf/index/core/pobjects/PRectangle;->y:F

    .line 132
    .local v4, "yTop":F
    :goto_3
    iput v1, v0, Lorg/icepdf/index/core/pobjects/PRectangle;->x:F

    .line 133
    iput v4, v0, Lorg/icepdf/index/core/pobjects/PRectangle;->y:F

    .line 134
    sub-float v5, v2, v1

    iput v5, v0, Lorg/icepdf/index/core/pobjects/PRectangle;->width:F

    .line 135
    sub-float v5, v4, v3

    iput v5, v0, Lorg/icepdf/index/core/pobjects/PRectangle;->height:F

    .line 138
    iget v5, v0, Lorg/icepdf/index/core/pobjects/PRectangle;->width:F

    cmpg-float v5, v5, v9

    if-ltz v5, :cond_0

    iget v5, v0, Lorg/icepdf/index/core/pobjects/PRectangle;->height:F

    cmpg-float v5, v5, v9

    if-gez v5, :cond_1

    .line 139
    :cond_0
    iput v9, v0, Lorg/icepdf/index/core/pobjects/PRectangle;->height:F

    iput v9, v0, Lorg/icepdf/index/core/pobjects/PRectangle;->width:F

    iput v9, v0, Lorg/icepdf/index/core/pobjects/PRectangle;->y:F

    iput v9, v0, Lorg/icepdf/index/core/pobjects/PRectangle;->x:F

    .line 141
    :cond_1
    return-object v0

    .line 124
    .end local v1    # "xLeft":F
    .end local v2    # "xRight":F
    .end local v3    # "yBottom":F
    .end local v4    # "yTop":F
    :cond_2
    iget v1, v0, Lorg/icepdf/index/core/pobjects/PRectangle;->x:F

    goto :goto_0

    .line 125
    .restart local v1    # "xLeft":F
    :cond_3
    iget v5, p0, Lorg/icepdf/index/core/pobjects/PRectangle;->x:F

    iget v6, p0, Lorg/icepdf/index/core/pobjects/PRectangle;->width:F

    add-float v2, v5, v6

    goto :goto_1

    .line 128
    .restart local v2    # "xRight":F
    :cond_4
    iget v5, p0, Lorg/icepdf/index/core/pobjects/PRectangle;->y:F

    iget v6, p0, Lorg/icepdf/index/core/pobjects/PRectangle;->height:F

    sub-float v3, v5, v6

    goto :goto_2

    .line 130
    .restart local v3    # "yBottom":F
    :cond_5
    iget v4, p0, Lorg/icepdf/index/core/pobjects/PRectangle;->y:F

    goto :goto_3
.end method

.method public getOriginalPoints()Lorg/icepdf/index/core/pobjects/Rectangle2Df;
    .locals 5

    .prologue
    .line 152
    new-instance v0, Lorg/icepdf/index/core/pobjects/Rectangle2Df;

    iget v1, p0, Lorg/icepdf/index/core/pobjects/PRectangle;->p1x:F

    iget v2, p0, Lorg/icepdf/index/core/pobjects/PRectangle;->p1y:F

    iget v3, p0, Lorg/icepdf/index/core/pobjects/PRectangle;->p2x:F

    iget v4, p0, Lorg/icepdf/index/core/pobjects/PRectangle;->p2y:F

    invoke-direct {v0, v1, v2, v3, v4}, Lorg/icepdf/index/core/pobjects/Rectangle2Df;-><init>(FFFF)V

    return-object v0
.end method

.method public toJava2dCoordinates()Lorg/icepdf/index/core/pobjects/Rectangle2Df;
    .locals 5

    .prologue
    .line 162
    new-instance v0, Lorg/icepdf/index/core/pobjects/Rectangle2Df;

    iget v1, p0, Lorg/icepdf/index/core/pobjects/PRectangle;->x:F

    iget v2, p0, Lorg/icepdf/index/core/pobjects/PRectangle;->y:F

    iget v3, p0, Lorg/icepdf/index/core/pobjects/PRectangle;->height:F

    sub-float/2addr v2, v3

    iget v3, p0, Lorg/icepdf/index/core/pobjects/PRectangle;->width:F

    iget v4, p0, Lorg/icepdf/index/core/pobjects/PRectangle;->height:F

    invoke-direct {v0, v1, v2, v3, v4}, Lorg/icepdf/index/core/pobjects/Rectangle2Df;-><init>(FFFF)V

    return-object v0
.end method
