.class public Lorg/icepdf/index/core/pobjects/CrossReference;
.super Ljava/lang/Object;
.source "CrossReference.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/icepdf/index/core/pobjects/CrossReference$CompressedEntry;,
        Lorg/icepdf/index/core/pobjects/CrossReference$UsedEntry;,
        Lorg/icepdf/index/core/pobjects/CrossReference$FreeEntry;,
        Lorg/icepdf/index/core/pobjects/CrossReference$Entry;
    }
.end annotation


# static fields
.field private static MAX_PERMISSIBLE_OBJCOUNT:I


# instance fields
.field private m_PTrailer:Lorg/icepdf/index/core/pobjects/PTrailer;

.field private m_bHaveTriedLoadingPeer:Z

.field private m_bHaveTriedLoadingPrevious:Z

.field private m_bIsCrossReferenceTable:Z

.field private m_hObjectNumber2Entry:Lorg/icepdf/index/core/util/Hashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/icepdf/index/core/util/Hashtable",
            "<",
            "Ljava/lang/Number;",
            "Lorg/icepdf/index/core/pobjects/CrossReference$Entry;",
            ">;"
        }
    .end annotation
.end field

.field private m_xrefPeer:Lorg/icepdf/index/core/pobjects/CrossReference;

.field private m_xrefPrevious:Lorg/icepdf/index/core/pobjects/CrossReference;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 71
    const/high16 v0, 0x80000

    sput v0, Lorg/icepdf/index/core/pobjects/CrossReference;->MAX_PERMISSIBLE_OBJCOUNT:I

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    new-instance v0, Lorg/icepdf/index/core/util/Hashtable;

    const/16 v1, 0x1000

    invoke-direct {v0, v1}, Lorg/icepdf/index/core/util/Hashtable;-><init>(I)V

    iput-object v0, p0, Lorg/icepdf/index/core/pobjects/CrossReference;->m_hObjectNumber2Entry:Lorg/icepdf/index/core/util/Hashtable;

    .line 62
    return-void
.end method


# virtual methods
.method protected addCompressedEntry(III)V
    .locals 3
    .param p1, "objectNumber"    # I
    .param p2, "objectNumberOfContainingObjectStream"    # I
    .param p3, "indexWithinObjectStream"    # I

    .prologue
    .line 253
    new-instance v0, Lorg/icepdf/index/core/pobjects/CrossReference$CompressedEntry;

    invoke-direct {v0, p1, p2, p3}, Lorg/icepdf/index/core/pobjects/CrossReference$CompressedEntry;-><init>(III)V

    .line 256
    .local v0, "entry":Lorg/icepdf/index/core/pobjects/CrossReference$CompressedEntry;
    iget-object v1, p0, Lorg/icepdf/index/core/pobjects/CrossReference;->m_hObjectNumber2Entry:Lorg/icepdf/index/core/util/Hashtable;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lorg/icepdf/index/core/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 257
    return-void
.end method

.method protected addFreeEntry(III)V
    .locals 3
    .param p1, "objectNumber"    # I
    .param p2, "nextFreeObjectNumber"    # I
    .param p3, "generationNumberIfReused"    # I

    .prologue
    .line 240
    new-instance v0, Lorg/icepdf/index/core/pobjects/CrossReference$FreeEntry;

    invoke-direct {v0, p1, p2, p3}, Lorg/icepdf/index/core/pobjects/CrossReference$FreeEntry;-><init>(III)V

    .line 242
    .local v0, "entry":Lorg/icepdf/index/core/pobjects/CrossReference$FreeEntry;
    iget-object v1, p0, Lorg/icepdf/index/core/pobjects/CrossReference;->m_hObjectNumber2Entry:Lorg/icepdf/index/core/util/Hashtable;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lorg/icepdf/index/core/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 243
    return-void
.end method

.method public addToEndOfChainOfPreviousXRefs(Lorg/icepdf/index/core/pobjects/CrossReference;)V
    .locals 1
    .param p1, "prev"    # Lorg/icepdf/index/core/pobjects/CrossReference;

    .prologue
    .line 232
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/CrossReference;->m_xrefPrevious:Lorg/icepdf/index/core/pobjects/CrossReference;

    if-nez v0, :cond_0

    .line 233
    iput-object p1, p0, Lorg/icepdf/index/core/pobjects/CrossReference;->m_xrefPrevious:Lorg/icepdf/index/core/pobjects/CrossReference;

    .line 236
    :goto_0
    return-void

    .line 235
    :cond_0
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/CrossReference;->m_xrefPrevious:Lorg/icepdf/index/core/pobjects/CrossReference;

    invoke-virtual {v0, p1}, Lorg/icepdf/index/core/pobjects/CrossReference;->addToEndOfChainOfPreviousXRefs(Lorg/icepdf/index/core/pobjects/CrossReference;)V

    goto :goto_0
.end method

.method protected addUsedEntry(IJI)V
    .locals 4
    .param p1, "objectNumber"    # I
    .param p2, "filePositionOfObject"    # J
    .param p4, "generationNumber"    # I

    .prologue
    .line 246
    new-instance v0, Lorg/icepdf/index/core/pobjects/CrossReference$UsedEntry;

    invoke-direct {v0, p1, p2, p3, p4}, Lorg/icepdf/index/core/pobjects/CrossReference$UsedEntry;-><init>(IJI)V

    .line 249
    .local v0, "entry":Lorg/icepdf/index/core/pobjects/CrossReference$UsedEntry;
    iget-object v1, p0, Lorg/icepdf/index/core/pobjects/CrossReference;->m_hObjectNumber2Entry:Lorg/icepdf/index/core/util/Hashtable;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lorg/icepdf/index/core/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 250
    return-void
.end method

.method public addXRefStreamEntries(Lorg/icepdf/index/core/util/Library;Lorg/icepdf/index/core/util/Hashtable;Ljava/io/InputStream;)V
    .locals 29
    .param p1, "library"    # Lorg/icepdf/index/core/util/Library;
    .param p2, "xrefStreamHash"    # Lorg/icepdf/index/core/util/Hashtable;
    .param p3, "streamInput"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/icepdf/index/core/exceptions/PDFException;
        }
    .end annotation

    .prologue
    .line 125
    :try_start_0
    const-string/jumbo v26, "Size"

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, v26

    invoke-virtual {v0, v1, v2}, Lorg/icepdf/index/core/util/Library;->getInt(Lorg/icepdf/index/core/util/Hashtable;Ljava/lang/String;)I

    move-result v23

    .line 128
    .local v23, "size":I
    sget v26, Lorg/icepdf/index/core/pobjects/CrossReference;->MAX_PERMISSIBLE_OBJCOUNT:I

    move/from16 v0, v23

    move/from16 v1, v26

    if-le v0, v1, :cond_1

    .line 130
    const-string/jumbo v26, "PDF"

    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v28, "Lucene updating Sobjectcount more:"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, v27

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    invoke-static/range {v26 .. v27}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 131
    new-instance v26, Lorg/icepdf/index/core/exceptions/PDFException;

    const-string/jumbo v27, "Too many Objects"

    invoke-direct/range {v26 .. v27}, Lorg/icepdf/index/core/exceptions/PDFException;-><init>(Ljava/lang/String;)V

    throw v26
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 193
    .end local v23    # "size":I
    :catch_0
    move-exception v5

    .line 194
    .local v5, "e":Ljava/io/IOException;
    const-string/jumbo v26, "Log"

    const-string/jumbo v27, "Error parsing xRef stream entries."

    move-object/from16 v0, v26

    move-object/from16 v1, v27

    invoke-static {v0, v1, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 197
    .end local v5    # "e":Ljava/io/IOException;
    :cond_0
    return-void

    .line 135
    .restart local v23    # "size":I
    :cond_1
    :try_start_1
    const-string/jumbo v26, "Index"

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, v26

    invoke-virtual {v0, v1, v2}, Lorg/icepdf/index/core/util/Library;->getObject(Lorg/icepdf/index/core/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Ljava/util/Vector;

    .line 137
    .local v20, "objNumAndEntriesCountPairs":Ljava/util/Vector;, "Ljava/util/Vector<Ljava/lang/Number;>;"
    if-nez v20, :cond_2

    .line 138
    new-instance v20, Ljava/util/Vector;

    .end local v20    # "objNumAndEntriesCountPairs":Ljava/util/Vector;, "Ljava/util/Vector<Ljava/lang/Number;>;"
    const/16 v26, 0x2

    move-object/from16 v0, v20

    move/from16 v1, v26

    invoke-direct {v0, v1}, Ljava/util/Vector;-><init>(I)V

    .line 139
    .restart local v20    # "objNumAndEntriesCountPairs":Ljava/util/Vector;, "Ljava/util/Vector<Ljava/lang/Number;>;"
    const/16 v26, 0x0

    invoke-static/range {v26 .. v26}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v26

    move-object/from16 v0, v20

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 140
    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v26

    move-object/from16 v0, v20

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 142
    :cond_2
    const-string/jumbo v26, "W"

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, v26

    invoke-virtual {v0, v1, v2}, Lorg/icepdf/index/core/util/Library;->getObject(Lorg/icepdf/index/core/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/util/Vector;

    .line 143
    .local v9, "fieldSizesVec":Ljava/util/Vector;
    const/4 v8, 0x0

    .line 144
    .local v8, "fieldSizes":[I
    if-eqz v9, :cond_3

    .line 145
    invoke-virtual {v9}, Ljava/util/Vector;->size()I

    move-result v26

    move/from16 v0, v26

    new-array v8, v0, [I

    .line 146
    const/16 v17, 0x0

    .local v17, "i":I
    :goto_0
    invoke-virtual {v9}, Ljava/util/Vector;->size()I

    move-result v26

    move/from16 v0, v17

    move/from16 v1, v26

    if-ge v0, v1, :cond_3

    .line 147
    move/from16 v0, v17

    invoke-virtual {v9, v0}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Ljava/lang/Number;

    invoke-virtual/range {v26 .. v26}, Ljava/lang/Number;->intValue()I

    move-result v26

    aput v26, v8, v17

    .line 146
    add-int/lit8 v17, v17, 0x1

    goto :goto_0

    .line 150
    .end local v17    # "i":I
    :cond_3
    const/4 v12, 0x0

    .line 152
    .local v12, "fieldTypeSize":I
    if-eqz v8, :cond_0

    .line 154
    const/16 v26, 0x0

    aget v12, v8, v26

    .line 155
    const/16 v26, 0x1

    aget v11, v8, v26

    .line 156
    .local v11, "fieldTwoSize":I
    const/16 v26, 0x2

    aget v10, v8, v26

    .line 157
    .local v10, "fieldThreeSize":I
    const/16 v25, 0x0

    .local v25, "xrefSubsection":I
    :goto_1
    invoke-virtual/range {v20 .. v20}, Ljava/util/Vector;->size()I

    move-result v26

    move/from16 v0, v25

    move/from16 v1, v26

    if-ge v0, v1, :cond_0

    .line 158
    move-object/from16 v0, v20

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Ljava/lang/Number;

    invoke-virtual/range {v26 .. v26}, Ljava/lang/Number;->intValue()I

    move-result v24

    .line 159
    .local v24, "startingObjectNumber":I
    add-int/lit8 v26, v25, 0x1

    move-object/from16 v0, v20

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Ljava/lang/Number;

    invoke-virtual/range {v26 .. v26}, Ljava/lang/Number;->intValue()I

    move-result v6

    .line 160
    .local v6, "entriesCount":I
    add-int v4, v24, v6

    .line 161
    .local v4, "afterObjectNumber":I
    move/from16 v21, v24

    .local v21, "objectNumber":I
    :goto_2
    move/from16 v0, v21

    if-ge v0, v4, :cond_9

    .line 162
    const/4 v7, 0x1

    .line 163
    .local v7, "entryType":I
    if-lez v12, :cond_4

    .line 164
    move-object/from16 v0, p3

    invoke-static {v0, v12}, Lorg/icepdf/index/core/util/Utils;->readIntWithVaryingBytesBE(Ljava/io/InputStream;I)I

    move-result v7

    .line 165
    :cond_4
    if-nez v7, :cond_6

    .line 166
    move-object/from16 v0, p3

    invoke-static {v0, v11}, Lorg/icepdf/index/core/util/Utils;->readIntWithVaryingBytesBE(Ljava/io/InputStream;I)I

    move-result v19

    .line 168
    .local v19, "nextFreeObjectNumber":I
    move-object/from16 v0, p3

    invoke-static {v0, v10}, Lorg/icepdf/index/core/util/Utils;->readIntWithVaryingBytesBE(Ljava/io/InputStream;I)I

    move-result v16

    .line 170
    .local v16, "generationNumberIfReused":I
    move-object/from16 v0, p0

    move/from16 v1, v21

    move/from16 v2, v19

    move/from16 v3, v16

    invoke-virtual {v0, v1, v2, v3}, Lorg/icepdf/index/core/pobjects/CrossReference;->addFreeEntry(III)V

    .line 161
    .end local v16    # "generationNumberIfReused":I
    .end local v19    # "nextFreeObjectNumber":I
    :cond_5
    :goto_3
    add-int/lit8 v21, v21, 0x1

    goto :goto_2

    .line 171
    :cond_6
    const/16 v26, 0x1

    move/from16 v0, v26

    if-ne v7, v0, :cond_8

    .line 172
    move-object/from16 v0, p3

    invoke-static {v0, v11}, Lorg/icepdf/index/core/util/Utils;->readLongWithVaryingBytesBE(Ljava/io/InputStream;I)J

    move-result-wide v14

    .line 174
    .local v14, "filePositionOfObject":J
    const/4 v13, 0x0

    .line 175
    .local v13, "generationNumber":I
    if-lez v10, :cond_7

    .line 176
    move-object/from16 v0, p3

    invoke-static {v0, v10}, Lorg/icepdf/index/core/util/Utils;->readIntWithVaryingBytesBE(Ljava/io/InputStream;I)I

    move-result v13

    .line 179
    :cond_7
    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-virtual {v0, v1, v14, v15, v13}, Lorg/icepdf/index/core/pobjects/CrossReference;->addUsedEntry(IJI)V

    goto :goto_3

    .line 180
    .end local v13    # "generationNumber":I
    .end local v14    # "filePositionOfObject":J
    :cond_8
    const/16 v26, 0x2

    move/from16 v0, v26

    if-ne v7, v0, :cond_5

    .line 181
    move-object/from16 v0, p3

    invoke-static {v0, v11}, Lorg/icepdf/index/core/util/Utils;->readIntWithVaryingBytesBE(Ljava/io/InputStream;I)I

    move-result v22

    .line 183
    .local v22, "objectNumberOfContainingObjectStream":I
    move-object/from16 v0, p3

    invoke-static {v0, v10}, Lorg/icepdf/index/core/util/Utils;->readIntWithVaryingBytesBE(Ljava/io/InputStream;I)I

    move-result v18

    .line 185
    .local v18, "indexWithinObjectStream":I
    move-object/from16 v0, p0

    move/from16 v1, v21

    move/from16 v2, v22

    move/from16 v3, v18

    invoke-virtual {v0, v1, v2, v3}, Lorg/icepdf/index/core/pobjects/CrossReference;->addCompressedEntry(III)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_3

    .line 157
    .end local v7    # "entryType":I
    .end local v18    # "indexWithinObjectStream":I
    .end local v22    # "objectNumberOfContainingObjectStream":I
    :cond_9
    add-int/lit8 v25, v25, 0x2

    goto/16 :goto_1
.end method

.method public addXRefTableEntries(Lorg/icepdf/index/core/util/Parser;)V
    .locals 13
    .param p1, "parser"    # Lorg/icepdf/index/core/util/Parser;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/icepdf/index/core/exceptions/PDFException;
        }
    .end annotation

    .prologue
    .line 73
    const/4 v10, 0x1

    iput-boolean v10, p0, Lorg/icepdf/index/core/pobjects/CrossReference;->m_bIsCrossReferenceTable:Z

    .line 78
    :cond_0
    const/16 v10, 0x10

    :try_start_0
    invoke-virtual {p1, v10}, Lorg/icepdf/index/core/util/Parser;->getNumberOrStringWithMark(I)Ljava/lang/Object;

    move-result-object v6

    .line 79
    .local v6, "startingObjectNumberOrTrailer":Ljava/lang/Object;
    instance-of v10, v6, Ljava/lang/Number;

    if-nez v10, :cond_1

    .line 80
    invoke-virtual {p1}, Lorg/icepdf/index/core/util/Parser;->ungetNumberOrStringWithReset()V

    .line 114
    .end local v6    # "startingObjectNumberOrTrailer":Ljava/lang/Object;
    :goto_0
    return-void

    .line 83
    .restart local v6    # "startingObjectNumberOrTrailer":Ljava/lang/Object;
    :cond_1
    check-cast v6, Ljava/lang/Number;

    .end local v6    # "startingObjectNumberOrTrailer":Ljava/lang/Object;
    invoke-virtual {v6}, Ljava/lang/Number;->intValue()I

    move-result v5

    .line 84
    .local v5, "startingObjectNumber":I
    invoke-virtual {p1}, Lorg/icepdf/index/core/util/Parser;->getToken()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Number;

    invoke-virtual {v10}, Ljava/lang/Number;->intValue()I

    move-result v4

    .line 85
    .local v4, "numEntries":I
    move v0, v5

    .line 88
    .local v0, "currNumber":I
    sget v10, Lorg/icepdf/index/core/pobjects/CrossReference;->MAX_PERMISSIBLE_OBJCOUNT:I

    if-le v4, v10, :cond_2

    .line 90
    const-string/jumbo v10, "PDF"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v12, "Lucene updating Tobjectcount more:"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 91
    new-instance v10, Lorg/icepdf/index/core/exceptions/PDFException;

    const-string/jumbo v11, "Too many Objects"

    invoke-direct {v10, v11}, Lorg/icepdf/index/core/exceptions/PDFException;-><init>(Ljava/lang/String;)V

    throw v10
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 111
    .end local v0    # "currNumber":I
    .end local v4    # "numEntries":I
    .end local v5    # "startingObjectNumber":I
    :catch_0
    move-exception v1

    .line 112
    .local v1, "e":Ljava/io/IOException;
    const-string/jumbo v10, "LOG"

    const-string/jumbo v11, "Error parsing xRef table entries."

    invoke-static {v10, v11, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 95
    .end local v1    # "e":Ljava/io/IOException;
    .restart local v0    # "currNumber":I
    .restart local v4    # "numEntries":I
    .restart local v5    # "startingObjectNumber":I
    :cond_2
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    if-ge v3, v4, :cond_0

    .line 96
    :try_start_1
    invoke-virtual {p1}, Lorg/icepdf/index/core/util/Parser;->getIntSurroundedByWhitespace()I

    move-result v10

    int-to-long v8, v10

    .line 97
    .local v8, "tenDigitNum":J
    invoke-virtual {p1}, Lorg/icepdf/index/core/util/Parser;->getIntSurroundedByWhitespace()I

    move-result v2

    .line 98
    .local v2, "generationNum":I
    invoke-virtual {p1}, Lorg/icepdf/index/core/util/Parser;->getCharSurroundedByWhitespace()C

    move-result v7

    .line 99
    .local v7, "usedOrFree":C
    const/16 v10, 0x6e

    if-ne v7, v10, :cond_4

    .line 100
    invoke-virtual {p0, v0, v8, v9, v2}, Lorg/icepdf/index/core/pobjects/CrossReference;->addUsedEntry(IJI)V

    .line 105
    :cond_3
    :goto_2
    add-int/lit8 v0, v0, 0x1

    .line 95
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 101
    :cond_4
    const/16 v10, 0x66

    if-ne v7, v10, :cond_3

    .line 102
    long-to-int v10, v8

    invoke-virtual {p0, v0, v10, v2}, Lorg/icepdf/index/core/pobjects/CrossReference;->addFreeEntry(III)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2
.end method

.method public getEntryForObject(Ljava/lang/Integer;)Lorg/icepdf/index/core/pobjects/CrossReference$Entry;
    .locals 4
    .param p1, "objectNumber"    # Ljava/lang/Integer;

    .prologue
    const/4 v3, 0x1

    .line 200
    iget-object v2, p0, Lorg/icepdf/index/core/pobjects/CrossReference;->m_hObjectNumber2Entry:Lorg/icepdf/index/core/util/Hashtable;

    invoke-virtual {v2, p1}, Lorg/icepdf/index/core/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/icepdf/index/core/pobjects/CrossReference$Entry;

    .line 201
    .local v0, "entry":Lorg/icepdf/index/core/pobjects/CrossReference$Entry;
    if-eqz v0, :cond_0

    move-object v1, v0

    .line 228
    .end local v0    # "entry":Lorg/icepdf/index/core/pobjects/CrossReference$Entry;
    .local v1, "entry":Lorg/icepdf/index/core/pobjects/CrossReference$Entry;
    :goto_0
    return-object v1

    .line 204
    .end local v1    # "entry":Lorg/icepdf/index/core/pobjects/CrossReference$Entry;
    .restart local v0    # "entry":Lorg/icepdf/index/core/pobjects/CrossReference$Entry;
    :cond_0
    iget-boolean v2, p0, Lorg/icepdf/index/core/pobjects/CrossReference;->m_bIsCrossReferenceTable:Z

    if-eqz v2, :cond_1

    iget-boolean v2, p0, Lorg/icepdf/index/core/pobjects/CrossReference;->m_bHaveTriedLoadingPeer:Z

    if-nez v2, :cond_1

    iget-object v2, p0, Lorg/icepdf/index/core/pobjects/CrossReference;->m_xrefPeer:Lorg/icepdf/index/core/pobjects/CrossReference;

    if-nez v2, :cond_1

    iget-object v2, p0, Lorg/icepdf/index/core/pobjects/CrossReference;->m_PTrailer:Lorg/icepdf/index/core/pobjects/PTrailer;

    if-eqz v2, :cond_1

    .line 207
    iget-object v2, p0, Lorg/icepdf/index/core/pobjects/CrossReference;->m_PTrailer:Lorg/icepdf/index/core/pobjects/PTrailer;

    invoke-virtual {v2}, Lorg/icepdf/index/core/pobjects/PTrailer;->loadXRefStmIfApplicable()V

    .line 208
    iget-object v2, p0, Lorg/icepdf/index/core/pobjects/CrossReference;->m_PTrailer:Lorg/icepdf/index/core/pobjects/PTrailer;

    invoke-virtual {v2}, Lorg/icepdf/index/core/pobjects/PTrailer;->getCrossReferenceStream()Lorg/icepdf/index/core/pobjects/CrossReference;

    move-result-object v2

    iput-object v2, p0, Lorg/icepdf/index/core/pobjects/CrossReference;->m_xrefPeer:Lorg/icepdf/index/core/pobjects/CrossReference;

    .line 209
    iput-boolean v3, p0, Lorg/icepdf/index/core/pobjects/CrossReference;->m_bHaveTriedLoadingPeer:Z

    .line 211
    :cond_1
    iget-object v2, p0, Lorg/icepdf/index/core/pobjects/CrossReference;->m_xrefPeer:Lorg/icepdf/index/core/pobjects/CrossReference;

    if-eqz v2, :cond_2

    .line 212
    iget-object v2, p0, Lorg/icepdf/index/core/pobjects/CrossReference;->m_xrefPeer:Lorg/icepdf/index/core/pobjects/CrossReference;

    invoke-virtual {v2, p1}, Lorg/icepdf/index/core/pobjects/CrossReference;->getEntryForObject(Ljava/lang/Integer;)Lorg/icepdf/index/core/pobjects/CrossReference$Entry;

    move-result-object v0

    .line 213
    if-eqz v0, :cond_2

    move-object v1, v0

    .line 214
    .end local v0    # "entry":Lorg/icepdf/index/core/pobjects/CrossReference$Entry;
    .restart local v1    # "entry":Lorg/icepdf/index/core/pobjects/CrossReference$Entry;
    goto :goto_0

    .line 217
    .end local v1    # "entry":Lorg/icepdf/index/core/pobjects/CrossReference$Entry;
    .restart local v0    # "entry":Lorg/icepdf/index/core/pobjects/CrossReference$Entry;
    :cond_2
    iget-boolean v2, p0, Lorg/icepdf/index/core/pobjects/CrossReference;->m_bHaveTriedLoadingPrevious:Z

    if-nez v2, :cond_3

    iget-object v2, p0, Lorg/icepdf/index/core/pobjects/CrossReference;->m_xrefPrevious:Lorg/icepdf/index/core/pobjects/CrossReference;

    if-nez v2, :cond_3

    iget-object v2, p0, Lorg/icepdf/index/core/pobjects/CrossReference;->m_PTrailer:Lorg/icepdf/index/core/pobjects/PTrailer;

    if-eqz v2, :cond_3

    .line 220
    iget-object v2, p0, Lorg/icepdf/index/core/pobjects/CrossReference;->m_PTrailer:Lorg/icepdf/index/core/pobjects/PTrailer;

    invoke-virtual {v2}, Lorg/icepdf/index/core/pobjects/PTrailer;->onDemandLoadAndSetupPreviousTrailer()V

    .line 221
    iput-boolean v3, p0, Lorg/icepdf/index/core/pobjects/CrossReference;->m_bHaveTriedLoadingPrevious:Z

    .line 223
    :cond_3
    iget-object v2, p0, Lorg/icepdf/index/core/pobjects/CrossReference;->m_xrefPrevious:Lorg/icepdf/index/core/pobjects/CrossReference;

    if-eqz v2, :cond_4

    .line 224
    iget-object v2, p0, Lorg/icepdf/index/core/pobjects/CrossReference;->m_xrefPrevious:Lorg/icepdf/index/core/pobjects/CrossReference;

    invoke-virtual {v2, p1}, Lorg/icepdf/index/core/pobjects/CrossReference;->getEntryForObject(Ljava/lang/Integer;)Lorg/icepdf/index/core/pobjects/CrossReference$Entry;

    move-result-object v0

    .line 225
    if-eqz v0, :cond_4

    move-object v1, v0

    .line 226
    .end local v0    # "entry":Lorg/icepdf/index/core/pobjects/CrossReference$Entry;
    .restart local v1    # "entry":Lorg/icepdf/index/core/pobjects/CrossReference$Entry;
    goto :goto_0

    .end local v1    # "entry":Lorg/icepdf/index/core/pobjects/CrossReference$Entry;
    .restart local v0    # "entry":Lorg/icepdf/index/core/pobjects/CrossReference$Entry;
    :cond_4
    move-object v1, v0

    .line 228
    .end local v0    # "entry":Lorg/icepdf/index/core/pobjects/CrossReference$Entry;
    .restart local v1    # "entry":Lorg/icepdf/index/core/pobjects/CrossReference$Entry;
    goto :goto_0
.end method

.method public setTrailer(Lorg/icepdf/index/core/pobjects/PTrailer;)V
    .locals 0
    .param p1, "trailer"    # Lorg/icepdf/index/core/pobjects/PTrailer;

    .prologue
    .line 65
    iput-object p1, p0, Lorg/icepdf/index/core/pobjects/CrossReference;->m_PTrailer:Lorg/icepdf/index/core/pobjects/PTrailer;

    .line 66
    return-void
.end method
