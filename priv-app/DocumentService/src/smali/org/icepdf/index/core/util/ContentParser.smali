.class public Lorg/icepdf/index/core/util/ContentParser;
.super Ljava/lang/Object;
.source "ContentParser.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/icepdf/index/core/util/ContentParser$CustomStack;
    }
.end annotation


# static fields
.field private static final MAX_OBJ_COUNT:I = 0x7a120

.field private static final MAX_SIZE_FOR_EXTERNAL_STACK:I = 0x2

.field private static final MAX_SIZE_FOR_STACK:I = 0x20

.field public static final OVERPAINT_ALPHA:F = 0.4f

.field private static final TAG:Ljava/lang/String; = "ANDICEPDF.ContentParser"


# instance fields
.field private graphicState:Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;

.field private resources:Lorg/icepdf/index/core/pobjects/Resources;

.field protected stack:Lorg/icepdf/index/core/util/ContentParser$CustomStack;

.field private text:Ljava/lang/StringBuilder;


# direct methods
.method public constructor <init>(Lorg/icepdf/index/core/util/Library;Lorg/icepdf/index/core/pobjects/Resources;)V
    .locals 2
    .param p1, "l"    # Lorg/icepdf/index/core/util/Library;
    .param p2, "r"    # Lorg/icepdf/index/core/pobjects/Resources;

    .prologue
    .line 83
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lorg/icepdf/index/core/util/ContentParser;->text:Ljava/lang/StringBuilder;

    .line 298
    new-instance v0, Lorg/icepdf/index/core/util/ContentParser$CustomStack;

    const/16 v1, 0x20

    invoke-direct {v0, v1}, Lorg/icepdf/index/core/util/ContentParser$CustomStack;-><init>(I)V

    iput-object v0, p0, Lorg/icepdf/index/core/util/ContentParser;->stack:Lorg/icepdf/index/core/util/ContentParser$CustomStack;

    .line 85
    iput-object p2, p0, Lorg/icepdf/index/core/util/ContentParser;->resources:Lorg/icepdf/index/core/pobjects/Resources;

    .line 87
    return-void
.end method

.method private static applyTextScaling(Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;)V
    .locals 8
    .param p0, "graphicState"    # Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;

    .prologue
    const/4 v1, 0x0

    .line 1344
    invoke-virtual {p0}, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->getTextState()Lorg/icepdf/index/core/pobjects/graphics/TextState;

    move-result-object v0

    iget v0, v0, Lorg/icepdf/index/core/pobjects/graphics/TextState;->hScalling:F

    const/high16 v3, 0x3f800000    # 1.0f

    move v2, v1

    move v4, v1

    move v5, v1

    invoke-static/range {v0 .. v5}, Lorg/icepdf/index/core/Util;->createMatrixFromValues(FFFFFF)Landroid/graphics/Matrix;

    move-result-object v7

    .line 1348
    .local v7, "horizontalScalingTransform":Landroid/graphics/Matrix;
    new-instance v6, Landroid/graphics/Matrix;

    invoke-virtual {p0}, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->getCTM()Landroid/graphics/Matrix;

    move-result-object v0

    invoke-direct {v6, v0}, Landroid/graphics/Matrix;-><init>(Landroid/graphics/Matrix;)V

    .line 1350
    .local v6, "af":Landroid/graphics/Matrix;
    invoke-virtual {v6, v7}, Landroid/graphics/Matrix;->preConcat(Landroid/graphics/Matrix;)Z

    .line 1352
    invoke-virtual {p0, v6}, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->set(Landroid/graphics/Matrix;)V

    .line 1353
    return-void
.end method

.method private static consume_Tf(Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;Ljava/util/Stack;Lorg/icepdf/index/core/pobjects/Resources;)V
    .locals 4
    .param p0, "graphicState"    # Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;
    .param p1, "stack"    # Ljava/util/Stack;
    .param p2, "resources"    # Lorg/icepdf/index/core/pobjects/Resources;

    .prologue
    .line 1023
    invoke-virtual {p1}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Number;

    invoke-virtual {v2}, Ljava/lang/Number;->floatValue()F

    move-result v1

    .line 1024
    .local v1, "size":F
    invoke-virtual {p1}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/icepdf/index/core/pobjects/Name;

    .line 1026
    .local v0, "name2":Lorg/icepdf/index/core/pobjects/Name;
    invoke-virtual {p0}, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->getTextState()Lorg/icepdf/index/core/pobjects/graphics/TextState;

    move-result-object v2

    invoke-virtual {v0}, Lorg/icepdf/index/core/pobjects/Name;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2, v3}, Lorg/icepdf/index/core/pobjects/Resources;->getFont(Ljava/lang/String;)Lorg/icepdf/index/core/pobjects/fonts/Font;

    move-result-object v3

    iput-object v3, v2, Lorg/icepdf/index/core/pobjects/graphics/TextState;->font:Lorg/icepdf/index/core/pobjects/fonts/Font;

    .line 1027
    invoke-virtual {p0}, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->getTextState()Lorg/icepdf/index/core/pobjects/graphics/TextState;

    move-result-object v2

    invoke-virtual {p0}, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->getTextState()Lorg/icepdf/index/core/pobjects/graphics/TextState;

    move-result-object v3

    iget-object v3, v3, Lorg/icepdf/index/core/pobjects/graphics/TextState;->font:Lorg/icepdf/index/core/pobjects/fonts/Font;

    invoke-virtual {v3}, Lorg/icepdf/index/core/pobjects/fonts/Font;->getFont()Lorg/icepdf/index/core/pobjects/fonts/FontFile;

    move-result-object v3

    invoke-interface {v3, v1}, Lorg/icepdf/index/core/pobjects/fonts/FontFile;->deriveFont(F)Lorg/icepdf/index/core/pobjects/fonts/FontFile;

    move-result-object v3

    iput-object v3, v2, Lorg/icepdf/index/core/pobjects/graphics/TextState;->currentfont:Lorg/icepdf/index/core/pobjects/fonts/FontFile;

    .line 1029
    return-void
.end method

.method private getUnicodeStringFromText(Ljava/lang/StringBuffer;Lorg/icepdf/index/core/pobjects/graphics/TextState;)Ljava/lang/StringBuilder;
    .locals 6
    .param p1, "displayText"    # Ljava/lang/StringBuffer;
    .param p2, "textState"    # Lorg/icepdf/index/core/pobjects/graphics/TextState;

    .prologue
    .line 994
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 996
    .local v0, "builder":Ljava/lang/StringBuilder;
    invoke-virtual {p1}, Ljava/lang/StringBuffer;->length()I

    move-result v5

    if-nez v5, :cond_1

    .line 1013
    :cond_0
    return-object v0

    .line 1001
    :cond_1
    iget-object v2, p2, Lorg/icepdf/index/core/pobjects/graphics/TextState;->currentfont:Lorg/icepdf/index/core/pobjects/fonts/FontFile;

    .line 1003
    .local v2, "currentFont":Lorg/icepdf/index/core/pobjects/fonts/FontFile;
    invoke-virtual {p1}, Ljava/lang/StringBuffer;->length()I

    move-result v4

    .line 1005
    .local v4, "textLength":I
    const/4 v1, 0x0

    .line 1007
    .local v1, "currentChar":C
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-ge v3, v4, :cond_0

    .line 1008
    invoke-virtual {p1, v3}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v1

    .line 1010
    invoke-interface {v2, v1}, Lorg/icepdf/index/core/pobjects/fonts/FontFile;->toUnicode(C)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1007
    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method

.method private parseInlineImage(Lorg/icepdf/index/core/util/Parser;)V
    .locals 5
    .param p1, "parser"    # Lorg/icepdf/index/core/util/Parser;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1041
    :try_start_0
    invoke-virtual {p1}, Lorg/icepdf/index/core/util/Parser;->getStreamObject()Ljava/lang/Object;

    move-result-object v2

    .line 1042
    .local v2, "tok":Ljava/lang/Object;
    :goto_0
    const-string/jumbo v3, "ID"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 1043
    invoke-virtual {p1}, Lorg/icepdf/index/core/util/Parser;->getStreamObject()Ljava/lang/Object;

    move-result-object v2

    .line 1044
    invoke-virtual {p1}, Lorg/icepdf/index/core/util/Parser;->getStreamObject()Ljava/lang/Object;

    move-result-object v2

    goto :goto_0

    .line 1046
    :cond_0
    invoke-virtual {p1}, Lorg/icepdf/index/core/util/Parser;->peek2()Ljava/lang/String;

    move-result-object v2

    .line 1047
    .local v2, "tok":Ljava/lang/String;
    const/4 v0, 0x0

    .line 1048
    .local v0, "ateEI":Z
    :goto_1
    const-string/jumbo v3, " EI"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 1049
    invoke-virtual {p1}, Lorg/icepdf/index/core/util/Parser;->readLineForInlineImage()Z

    move-result v0

    .line 1050
    if-eqz v0, :cond_3

    .line 1054
    :cond_1
    if-nez v0, :cond_2

    .line 1055
    invoke-virtual {p1}, Lorg/icepdf/index/core/util/Parser;->getToken()Ljava/lang/Object;

    .line 1064
    .end local v0    # "ateEI":Z
    .end local v2    # "tok":Ljava/lang/String;
    :cond_2
    :goto_2
    return-void

    .line 1052
    .restart local v0    # "ateEI":Z
    .restart local v2    # "tok":Ljava/lang/String;
    :cond_3
    invoke-virtual {p1}, Lorg/icepdf/index/core/util/Parser;->peek2()Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v2

    goto :goto_1

    .line 1058
    .end local v0    # "ateEI":Z
    .end local v2    # "tok":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 1059
    .local v1, "e":Ljava/io/IOException;
    throw v1

    .line 1060
    .end local v1    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v1

    .line 1062
    .local v1, "e":Ljava/lang/Exception;
    const-string/jumbo v3, "ANDICEPDF.ContentParser"

    const-string/jumbo v4, "Error parsing inline image."

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method


# virtual methods
.method public dispose(Z)V
    .locals 1
    .param p1, "cache"    # Z

    .prologue
    .line 1032
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/icepdf/index/core/util/ContentParser;->graphicState:Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;

    .line 1034
    iget-object v0, p0, Lorg/icepdf/index/core/util/ContentParser;->resources:Lorg/icepdf/index/core/pobjects/Resources;

    invoke-virtual {v0, p1}, Lorg/icepdf/index/core/pobjects/Resources;->dispose(Z)V

    .line 1035
    return-void
.end method

.method getExtractedText()Ljava/lang/StringBuilder;
    .locals 1

    .prologue
    .line 291
    iget-object v0, p0, Lorg/icepdf/index/core/util/ContentParser;->text:Ljava/lang/StringBuilder;

    return-object v0
.end method

.method public getGraphicsState()Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lorg/icepdf/index/core/util/ContentParser;->graphicState:Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;

    return-object v0
.end method

.method public parse(Ljava/io/InputStream;)Lorg/icepdf/index/core/pobjects/graphics/Shapes;
    .locals 12
    .param p1, "source"    # Ljava/io/InputStream;

    .prologue
    const/4 v10, 0x0

    .line 120
    new-instance v4, Lorg/icepdf/index/core/pobjects/graphics/Shapes;

    invoke-direct {v4}, Lorg/icepdf/index/core/pobjects/graphics/Shapes;-><init>()V

    .line 125
    .local v4, "shapes":Lorg/icepdf/index/core/pobjects/graphics/Shapes;
    instance-of v9, p1, Lorg/icepdf/index/core/io/SeekableInput;

    if-eqz v9, :cond_1

    move-object v9, p1

    .line 126
    check-cast v9, Lorg/icepdf/index/core/io/SeekableInput;

    invoke-static {v9, v10}, Lorg/icepdf/index/core/util/Utils;->getContentFromSeekableInput(Lorg/icepdf/index/core/io/SeekableInput;Z)Ljava/lang/String;

    move-result-object v0

    .line 139
    .local v0, "content":Ljava/lang/String;
    :goto_0
    new-instance v3, Lorg/icepdf/index/core/util/Parser;

    invoke-direct {v3, p1}, Lorg/icepdf/index/core/util/Parser;-><init>(Ljava/io/InputStream;)V

    .line 142
    .local v3, "parser":Lorg/icepdf/index/core/util/Parser;
    new-instance v5, Ljava/util/Stack;

    invoke-direct {v5}, Ljava/util/Stack;-><init>()V

    .line 144
    .local v5, "stack":Ljava/util/Stack;, "Ljava/util/Stack<Ljava/lang/Object;>;"
    const/4 v8, 0x0

    .line 156
    .local v8, "yBTstart":F
    :cond_0
    :goto_1
    :try_start_0
    invoke-virtual {v3}, Lorg/icepdf/index/core/util/Parser;->getStreamObject()Ljava/lang/Object;

    move-result-object v7

    .line 157
    .local v7, "tok":Ljava/lang/Object;
    const-string/jumbo v9, "ANDICEPDF.ContentParser"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, ""

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 160
    instance-of v9, v7, Ljava/lang/String;

    if-nez v9, :cond_2

    .line 161
    invoke-virtual {v5, v7}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 192
    .end local v7    # "tok":Ljava/lang/Object;
    :catch_0
    move-exception v1

    .line 194
    .local v1, "e":Ljava/io/IOException;
    :try_start_1
    const-string/jumbo v9, "ANDICEPDF.ContentParser"

    const-string/jumbo v10, "End of Content Stream"

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 207
    :goto_2
    invoke-virtual {v5}, Ljava/util/Stack;->isEmpty()Z

    move-result v9

    if-nez v9, :cond_4

    .line 208
    invoke-virtual {v5}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    .line 210
    .local v6, "tmp":Ljava/lang/String;
    const-string/jumbo v9, "TAG"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "STACK="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 128
    .end local v0    # "content":Ljava/lang/String;
    .end local v1    # "e":Ljava/io/IOException;
    .end local v3    # "parser":Lorg/icepdf/index/core/util/Parser;
    .end local v5    # "stack":Ljava/util/Stack;, "Ljava/util/Stack<Ljava/lang/Object;>;"
    .end local v6    # "tmp":Ljava/lang/String;
    .end local v8    # "yBTstart":F
    :cond_1
    const/4 v9, 0x1

    new-array v2, v9, [Ljava/io/InputStream;

    aput-object p1, v2, v10

    .line 129
    .local v2, "inArray":[Ljava/io/InputStream;
    invoke-static {v2, v10}, Lorg/icepdf/index/core/util/Utils;->getContentAndReplaceInputStream([Ljava/io/InputStream;Z)Ljava/lang/String;

    move-result-object v0

    .line 130
    .restart local v0    # "content":Ljava/lang/String;
    aget-object p1, v2, v10

    .line 132
    const-string/jumbo v9, "TAG"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "Content = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 167
    .end local v2    # "inArray":[Ljava/io/InputStream;
    .restart local v3    # "parser":Lorg/icepdf/index/core/util/Parser;
    .restart local v5    # "stack":Ljava/util/Stack;, "Ljava/util/Stack<Ljava/lang/Object;>;"
    .restart local v7    # "tok":Ljava/lang/Object;
    .restart local v8    # "yBTstart":F
    :cond_2
    :try_start_2
    const-string/jumbo v9, "Tf"

    invoke-virtual {v7, v9}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 168
    iget-object v9, p0, Lorg/icepdf/index/core/util/ContentParser;->graphicState:Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;

    iget-object v10, p0, Lorg/icepdf/index/core/util/ContentParser;->resources:Lorg/icepdf/index/core/pobjects/Resources;

    invoke-static {v9, v5, v10}, Lorg/icepdf/index/core/util/ContentParser;->consume_Tf(Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;Ljava/util/Stack;Lorg/icepdf/index/core/pobjects/Resources;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_1

    .line 196
    .end local v7    # "tok":Ljava/lang/Object;
    :catchall_0
    move-exception v9

    throw v9

    .line 175
    .restart local v7    # "tok":Ljava/lang/Object;
    :cond_3
    :try_start_3
    const-string/jumbo v9, "BT"

    invoke-virtual {v7, v9}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 179
    float-to-double v10, v8

    invoke-virtual {p0, v3, v4, v10, v11}, Lorg/icepdf/index/core/util/ContentParser;->parseText(Lorg/icepdf/index/core/util/Parser;Lorg/icepdf/index/core/pobjects/graphics/Shapes;D)F

    move-result v8

    .line 184
    invoke-virtual {v5}, Ljava/util/Stack;->clear()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_1

    .line 213
    .end local v7    # "tok":Ljava/lang/Object;
    .restart local v1    # "e":Ljava/io/IOException;
    :cond_4
    return-object v4
.end method

.method parseText(Lorg/icepdf/index/core/util/Parser;Lorg/icepdf/index/core/pobjects/graphics/Shapes;D)F
    .locals 27
    .param p1, "parser"    # Lorg/icepdf/index/core/util/Parser;
    .param p2, "shapes"    # Lorg/icepdf/index/core/pobjects/graphics/Shapes;
    .param p3, "previousBTStart"    # D
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 353
    const/4 v11, 0x1

    .line 354
    .local v11, "isYstart":Z
    const/16 v22, 0x0

    .line 355
    .local v22, "yBTStart":F
    new-instance v4, Lorg/icepdf/index/java/awt/geom/Point2D$Float;

    const/16 v23, 0x0

    const/16 v24, 0x0

    move/from16 v0, v23

    move/from16 v1, v24

    invoke-direct {v4, v0, v1}, Lorg/icepdf/index/java/awt/geom/Point2D$Float;-><init>(FF)V

    .line 359
    .local v4, "advance":Lorg/icepdf/index/java/awt/geom/Point2D$Float;
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/util/ContentParser;->graphicState:Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->getTextState()Lorg/icepdf/index/core/pobjects/graphics/TextState;

    move-result-object v23

    new-instance v24, Landroid/graphics/Matrix;

    invoke-direct/range {v24 .. v24}, Landroid/graphics/Matrix;-><init>()V

    move-object/from16 v0, v24

    move-object/from16 v1, v23

    iput-object v0, v1, Lorg/icepdf/index/core/pobjects/graphics/TextState;->tmatrix:Landroid/graphics/Matrix;

    .line 360
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/util/ContentParser;->graphicState:Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->getTextState()Lorg/icepdf/index/core/pobjects/graphics/TextState;

    move-result-object v23

    new-instance v24, Landroid/graphics/Matrix;

    invoke-direct/range {v24 .. v24}, Landroid/graphics/Matrix;-><init>()V

    move-object/from16 v0, v24

    move-object/from16 v1, v23

    iput-object v0, v1, Lorg/icepdf/index/core/pobjects/graphics/TextState;->tlmatrix:Landroid/graphics/Matrix;

    .line 361
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/util/ContentParser;->graphicState:Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;

    move-object/from16 v23, v0

    const/high16 v24, 0x3f800000    # 1.0f

    const/high16 v25, -0x40800000    # -1.0f

    invoke-virtual/range {v23 .. v25}, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->scale(FF)V

    .line 364
    invoke-virtual/range {p1 .. p1}, Lorg/icepdf/index/core/util/Parser;->getStreamObject()Ljava/lang/Object;

    move-result-object v14

    .line 365
    .local v14, "nextToken":Ljava/lang/Object;
    :goto_0
    const-string/jumbo v23, "ET"

    move-object/from16 v0, v23

    invoke-virtual {v14, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-nez v23, :cond_1d

    .line 369
    instance-of v0, v14, Ljava/lang/String;

    move/from16 v23, v0

    if-eqz v23, :cond_1c

    .line 372
    const-string/jumbo v23, "Tj"

    move-object/from16 v0, v23

    invoke-virtual {v14, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_3

    .line 374
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/util/ContentParser;->stack:Lorg/icepdf/index/core/util/ContentParser$CustomStack;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lorg/icepdf/index/core/util/ContentParser$CustomStack;->pop()Ljava/lang/Object;

    move-result-object v18

    .line 377
    .local v18, "tjValue":Ljava/lang/Object;
    move-object/from16 v0, v18

    instance-of v0, v0, Lorg/icepdf/index/core/pobjects/StringObject;

    move/from16 v23, v0

    if-eqz v23, :cond_1

    move-object/from16 v16, v18

    .line 378
    check-cast v16, Lorg/icepdf/index/core/pobjects/StringObject;

    .line 379
    .local v16, "stringObject":Lorg/icepdf/index/core/pobjects/StringObject;
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/util/ContentParser;->graphicState:Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->getTextState()Lorg/icepdf/index/core/pobjects/graphics/TextState;

    move-result-object v17

    .line 382
    .local v17, "textState":Lorg/icepdf/index/core/pobjects/graphics/TextState;
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/util/ContentParser;->graphicState:Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;

    move-object/from16 v23, v0

    invoke-static/range {v23 .. v23}, Lorg/icepdf/index/core/util/ContentParser;->applyTextScaling(Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;)V

    .line 383
    if-eqz v16, :cond_1

    .line 385
    move-object/from16 v0, v16

    instance-of v0, v0, Lorg/icepdf/index/core/pobjects/LiteralStringObject;

    move/from16 v23, v0

    if-eqz v23, :cond_2

    .line 387
    move-object/from16 v0, v17

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/graphics/TextState;->font:Lorg/icepdf/index/core/pobjects/fonts/Font;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lorg/icepdf/index/core/pobjects/fonts/Font;->getSubTypeFormat()I

    move-result v23

    const/16 v24, 0x1

    move/from16 v0, v23

    move/from16 v1, v24

    if-ne v0, v1, :cond_0

    .line 389
    move-object/from16 v0, v17

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/graphics/TextState;->font:Lorg/icepdf/index/core/pobjects/fonts/Font;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lorg/icepdf/index/core/pobjects/fonts/Font;->getSubTypeFormat()I

    move-result v23

    move-object/from16 v0, v17

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/graphics/TextState;->font:Lorg/icepdf/index/core/pobjects/fonts/Font;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Lorg/icepdf/index/core/pobjects/fonts/Font;->getFont()Lorg/icepdf/index/core/pobjects/fonts/FontFile;

    move-result-object v24

    move-object/from16 v0, v16

    move/from16 v1, v23

    move-object/from16 v2, v24

    invoke-interface {v0, v1, v2}, Lorg/icepdf/index/core/pobjects/StringObject;->getLiteralStringBuffer(ILorg/icepdf/index/core/pobjects/fonts/FontFile;)Ljava/lang/StringBuffer;

    move-result-object v15

    .line 392
    .local v15, "sb":Ljava/lang/StringBuffer;
    invoke-virtual {v15}, Ljava/lang/StringBuffer;->length()I

    move-result v23

    move/from16 v0, v23

    new-array v5, v0, [C

    .line 393
    .local v5, "ch":[C
    const/16 v23, 0x0

    invoke-virtual {v15}, Ljava/lang/StringBuffer;->length()I

    move-result v24

    const/16 v25, 0x0

    move/from16 v0, v23

    move/from16 v1, v24

    move/from16 v2, v25

    invoke-virtual {v15, v0, v1, v5, v2}, Ljava/lang/StringBuffer;->getChars(II[CI)V

    .line 394
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_1
    array-length v0, v5

    move/from16 v23, v0

    move/from16 v0, v23

    if-ge v10, v0, :cond_1

    .line 396
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/util/ContentParser;->text:Ljava/lang/StringBuilder;

    move-object/from16 v23, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/graphics/TextState;->currentfont:Lorg/icepdf/index/core/pobjects/fonts/FontFile;

    move-object/from16 v24, v0

    aget-char v25, v5, v10

    invoke-interface/range {v24 .. v25}, Lorg/icepdf/index/core/pobjects/fonts/FontFile;->toUnicode(C)Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 394
    add-int/lit8 v10, v10, 0x1

    goto :goto_1

    .line 400
    .end local v5    # "ch":[C
    .end local v10    # "i":I
    .end local v15    # "sb":Ljava/lang/StringBuffer;
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/util/ContentParser;->text:Ljava/lang/StringBuilder;

    move-object/from16 v23, v0

    invoke-interface/range {v16 .. v16}, Lorg/icepdf/index/core/pobjects/StringObject;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 688
    .end local v16    # "stringObject":Lorg/icepdf/index/core/pobjects/StringObject;
    .end local v17    # "textState":Lorg/icepdf/index/core/pobjects/graphics/TextState;
    .end local v18    # "tjValue":Ljava/lang/Object;
    :cond_1
    :goto_2
    invoke-virtual/range {p1 .. p1}, Lorg/icepdf/index/core/util/Parser;->getStreamObject()Ljava/lang/Object;

    move-result-object v14

    goto/16 :goto_0

    .line 402
    .restart local v16    # "stringObject":Lorg/icepdf/index/core/pobjects/StringObject;
    .restart local v17    # "textState":Lorg/icepdf/index/core/pobjects/graphics/TextState;
    .restart local v18    # "tjValue":Ljava/lang/Object;
    :cond_2
    move-object/from16 v0, v16

    instance-of v0, v0, Lorg/icepdf/index/core/pobjects/HexStringObject;

    move/from16 v23, v0

    if-eqz v23, :cond_1

    .line 404
    move-object/from16 v0, v17

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/graphics/TextState;->font:Lorg/icepdf/index/core/pobjects/fonts/Font;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lorg/icepdf/index/core/pobjects/fonts/Font;->getSubTypeFormat()I

    move-result v23

    move-object/from16 v0, v17

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/graphics/TextState;->font:Lorg/icepdf/index/core/pobjects/fonts/Font;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Lorg/icepdf/index/core/pobjects/fonts/Font;->getFont()Lorg/icepdf/index/core/pobjects/fonts/FontFile;

    move-result-object v24

    move-object/from16 v0, v16

    move/from16 v1, v23

    move-object/from16 v2, v24

    invoke-interface {v0, v1, v2}, Lorg/icepdf/index/core/pobjects/StringObject;->getLiteralStringBuffer(ILorg/icepdf/index/core/pobjects/fonts/FontFile;)Ljava/lang/StringBuffer;

    move-result-object v15

    .line 407
    .restart local v15    # "sb":Ljava/lang/StringBuffer;
    invoke-virtual {v15}, Ljava/lang/StringBuffer;->length()I

    move-result v23

    move/from16 v0, v23

    new-array v5, v0, [C

    .line 408
    .restart local v5    # "ch":[C
    const/16 v23, 0x0

    invoke-virtual {v15}, Ljava/lang/StringBuffer;->length()I

    move-result v24

    const/16 v25, 0x0

    move/from16 v0, v23

    move/from16 v1, v24

    move/from16 v2, v25

    invoke-virtual {v15, v0, v1, v5, v2}, Ljava/lang/StringBuffer;->getChars(II[CI)V

    .line 409
    const/4 v10, 0x0

    .restart local v10    # "i":I
    :goto_3
    array-length v0, v5

    move/from16 v23, v0

    move/from16 v0, v23

    if-ge v10, v0, :cond_1

    .line 411
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/util/ContentParser;->text:Ljava/lang/StringBuilder;

    move-object/from16 v23, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/graphics/TextState;->currentfont:Lorg/icepdf/index/core/pobjects/fonts/FontFile;

    move-object/from16 v24, v0

    aget-char v25, v5, v10

    invoke-interface/range {v24 .. v25}, Lorg/icepdf/index/core/pobjects/fonts/FontFile;->toUnicode(C)Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 409
    add-int/lit8 v10, v10, 0x1

    goto :goto_3

    .line 423
    .end local v5    # "ch":[C
    .end local v10    # "i":I
    .end local v15    # "sb":Ljava/lang/StringBuffer;
    .end local v16    # "stringObject":Lorg/icepdf/index/core/pobjects/StringObject;
    .end local v17    # "textState":Lorg/icepdf/index/core/pobjects/graphics/TextState;
    .end local v18    # "tjValue":Ljava/lang/Object;
    :cond_3
    const-string/jumbo v23, "Tc"

    move-object/from16 v0, v23

    invoke-virtual {v14, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_4

    .line 425
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/util/ContentParser;->graphicState:Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->getTextState()Lorg/icepdf/index/core/pobjects/graphics/TextState;

    move-result-object v24

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/util/ContentParser;->stack:Lorg/icepdf/index/core/util/ContentParser$CustomStack;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lorg/icepdf/index/core/util/ContentParser$CustomStack;->pop()Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Ljava/lang/Number;

    invoke-virtual/range {v23 .. v23}, Ljava/lang/Number;->floatValue()F

    move-result v23

    move/from16 v0, v23

    move-object/from16 v1, v24

    iput v0, v1, Lorg/icepdf/index/core/pobjects/graphics/TextState;->cspace:F

    goto/16 :goto_2

    .line 429
    :cond_4
    const-string/jumbo v23, "Tw"

    move-object/from16 v0, v23

    invoke-virtual {v14, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_5

    .line 431
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/util/ContentParser;->graphicState:Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->getTextState()Lorg/icepdf/index/core/pobjects/graphics/TextState;

    move-result-object v24

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/util/ContentParser;->stack:Lorg/icepdf/index/core/util/ContentParser$CustomStack;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lorg/icepdf/index/core/util/ContentParser$CustomStack;->pop()Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Ljava/lang/Number;

    invoke-virtual/range {v23 .. v23}, Ljava/lang/Number;->floatValue()F

    move-result v23

    move/from16 v0, v23

    move-object/from16 v1, v24

    iput v0, v1, Lorg/icepdf/index/core/pobjects/graphics/TextState;->wspace:F

    goto/16 :goto_2

    .line 436
    :cond_5
    const-string/jumbo v23, "Td"

    move-object/from16 v0, v23

    invoke-virtual {v14, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_7

    .line 438
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/util/ContentParser;->stack:Lorg/icepdf/index/core/util/ContentParser$CustomStack;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lorg/icepdf/index/core/util/ContentParser$CustomStack;->pop()Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Ljava/lang/Number;

    invoke-virtual/range {v23 .. v23}, Ljava/lang/Number;->floatValue()F

    move-result v21

    .line 439
    .local v21, "y":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/util/ContentParser;->stack:Lorg/icepdf/index/core/util/ContentParser$CustomStack;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lorg/icepdf/index/core/util/ContentParser$CustomStack;->pop()Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Ljava/lang/Number;

    invoke-virtual/range {v23 .. v23}, Ljava/lang/Number;->floatValue()F

    .line 443
    if-eqz v11, :cond_6

    .line 445
    const/4 v11, 0x0

    .line 446
    move/from16 v0, v22

    float-to-double v0, v0

    move-wide/from16 v24, v0

    cmpl-double v23, p3, v24

    if-eqz v23, :cond_6

    .line 447
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/util/ContentParser;->text:Ljava/lang/StringBuilder;

    move-object/from16 v23, v0

    const-string/jumbo v24, " "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 450
    :cond_6
    const/16 v23, 0x0

    cmpl-float v23, v21, v23

    if-eqz v23, :cond_1

    .line 451
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/util/ContentParser;->text:Ljava/lang/StringBuilder;

    move-object/from16 v23, v0

    const-string/jumbo v24, " "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_2

    .line 459
    .end local v21    # "y":F
    :cond_7
    const-string/jumbo v23, "Tf"

    move-object/from16 v0, v23

    invoke-virtual {v14, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_8

    .line 460
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/util/ContentParser;->graphicState:Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/util/ContentParser;->stack:Lorg/icepdf/index/core/util/ContentParser$CustomStack;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/util/ContentParser;->resources:Lorg/icepdf/index/core/pobjects/Resources;

    move-object/from16 v25, v0

    invoke-static/range {v23 .. v25}, Lorg/icepdf/index/core/util/ContentParser;->consume_Tf(Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;Ljava/util/Stack;Lorg/icepdf/index/core/pobjects/Resources;)V

    goto/16 :goto_2

    .line 464
    :cond_8
    const-string/jumbo v23, "TJ"

    move-object/from16 v0, v23

    invoke-virtual {v14, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_d

    .line 468
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/util/ContentParser;->graphicState:Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;

    move-object/from16 v23, v0

    invoke-static/range {v23 .. v23}, Lorg/icepdf/index/core/util/ContentParser;->applyTextScaling(Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;)V

    .line 470
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/util/ContentParser;->stack:Lorg/icepdf/index/core/util/ContentParser$CustomStack;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lorg/icepdf/index/core/util/ContentParser$CustomStack;->pop()Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Ljava/util/Vector;

    .line 476
    .local v20, "v":Ljava/util/Vector;
    invoke-virtual/range {v20 .. v20}, Ljava/util/Vector;->elements()Ljava/util/Enumeration;

    move-result-object v7

    .local v7, "e":Ljava/util/Enumeration;
    :cond_9
    :goto_4
    invoke-interface {v7}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v23

    if-eqz v23, :cond_1

    .line 477
    invoke-interface {v7}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v6

    .line 478
    .local v6, "currentObject":Ljava/lang/Object;
    instance-of v0, v6, Lorg/icepdf/index/core/pobjects/StringObject;

    move/from16 v23, v0

    if-eqz v23, :cond_c

    move-object/from16 v16, v6

    .line 479
    check-cast v16, Lorg/icepdf/index/core/pobjects/StringObject;

    .line 481
    .restart local v16    # "stringObject":Lorg/icepdf/index/core/pobjects/StringObject;
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/util/ContentParser;->graphicState:Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->getTextState()Lorg/icepdf/index/core/pobjects/graphics/TextState;

    move-result-object v17

    .line 482
    .restart local v17    # "textState":Lorg/icepdf/index/core/pobjects/graphics/TextState;
    if-eqz v16, :cond_9

    .line 484
    move-object/from16 v0, v16

    instance-of v0, v0, Lorg/icepdf/index/core/pobjects/LiteralStringObject;

    move/from16 v23, v0

    if-eqz v23, :cond_b

    .line 486
    move-object/from16 v0, v17

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/graphics/TextState;->font:Lorg/icepdf/index/core/pobjects/fonts/Font;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lorg/icepdf/index/core/pobjects/fonts/Font;->getSubTypeFormat()I

    move-result v23

    const/16 v24, 0x1

    move/from16 v0, v23

    move/from16 v1, v24

    if-ne v0, v1, :cond_a

    .line 488
    move-object/from16 v0, v17

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/graphics/TextState;->font:Lorg/icepdf/index/core/pobjects/fonts/Font;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lorg/icepdf/index/core/pobjects/fonts/Font;->getSubTypeFormat()I

    move-result v23

    move-object/from16 v0, v17

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/graphics/TextState;->font:Lorg/icepdf/index/core/pobjects/fonts/Font;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Lorg/icepdf/index/core/pobjects/fonts/Font;->getFont()Lorg/icepdf/index/core/pobjects/fonts/FontFile;

    move-result-object v24

    move-object/from16 v0, v16

    move/from16 v1, v23

    move-object/from16 v2, v24

    invoke-interface {v0, v1, v2}, Lorg/icepdf/index/core/pobjects/StringObject;->getLiteralStringBuffer(ILorg/icepdf/index/core/pobjects/fonts/FontFile;)Ljava/lang/StringBuffer;

    move-result-object v15

    .line 491
    .restart local v15    # "sb":Ljava/lang/StringBuffer;
    invoke-virtual {v15}, Ljava/lang/StringBuffer;->length()I

    move-result v23

    move/from16 v0, v23

    new-array v5, v0, [C

    .line 492
    .restart local v5    # "ch":[C
    const/16 v23, 0x0

    invoke-virtual {v15}, Ljava/lang/StringBuffer;->length()I

    move-result v24

    const/16 v25, 0x0

    move/from16 v0, v23

    move/from16 v1, v24

    move/from16 v2, v25

    invoke-virtual {v15, v0, v1, v5, v2}, Ljava/lang/StringBuffer;->getChars(II[CI)V

    .line 493
    const/4 v10, 0x0

    .restart local v10    # "i":I
    :goto_5
    array-length v0, v5

    move/from16 v23, v0

    move/from16 v0, v23

    if-ge v10, v0, :cond_9

    .line 495
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/util/ContentParser;->text:Ljava/lang/StringBuilder;

    move-object/from16 v23, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/graphics/TextState;->currentfont:Lorg/icepdf/index/core/pobjects/fonts/FontFile;

    move-object/from16 v24, v0

    aget-char v25, v5, v10

    invoke-interface/range {v24 .. v25}, Lorg/icepdf/index/core/pobjects/fonts/FontFile;->toUnicode(C)Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 493
    add-int/lit8 v10, v10, 0x1

    goto :goto_5

    .line 499
    .end local v5    # "ch":[C
    .end local v10    # "i":I
    .end local v15    # "sb":Ljava/lang/StringBuffer;
    :cond_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/util/ContentParser;->text:Ljava/lang/StringBuilder;

    move-object/from16 v23, v0

    invoke-interface/range {v16 .. v16}, Lorg/icepdf/index/core/pobjects/StringObject;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_4

    .line 501
    :cond_b
    move-object/from16 v0, v16

    instance-of v0, v0, Lorg/icepdf/index/core/pobjects/HexStringObject;

    move/from16 v23, v0

    if-eqz v23, :cond_9

    .line 503
    move-object/from16 v0, v17

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/graphics/TextState;->font:Lorg/icepdf/index/core/pobjects/fonts/Font;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lorg/icepdf/index/core/pobjects/fonts/Font;->getSubTypeFormat()I

    move-result v23

    move-object/from16 v0, v17

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/graphics/TextState;->font:Lorg/icepdf/index/core/pobjects/fonts/Font;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Lorg/icepdf/index/core/pobjects/fonts/Font;->getFont()Lorg/icepdf/index/core/pobjects/fonts/FontFile;

    move-result-object v24

    move-object/from16 v0, v16

    move/from16 v1, v23

    move-object/from16 v2, v24

    invoke-interface {v0, v1, v2}, Lorg/icepdf/index/core/pobjects/StringObject;->getLiteralStringBuffer(ILorg/icepdf/index/core/pobjects/fonts/FontFile;)Ljava/lang/StringBuffer;

    move-result-object v15

    .line 506
    .restart local v15    # "sb":Ljava/lang/StringBuffer;
    invoke-virtual {v15}, Ljava/lang/StringBuffer;->length()I

    move-result v23

    move/from16 v0, v23

    new-array v5, v0, [C

    .line 507
    .restart local v5    # "ch":[C
    const/16 v23, 0x0

    invoke-virtual {v15}, Ljava/lang/StringBuffer;->length()I

    move-result v24

    const/16 v25, 0x0

    move/from16 v0, v23

    move/from16 v1, v24

    move/from16 v2, v25

    invoke-virtual {v15, v0, v1, v5, v2}, Ljava/lang/StringBuffer;->getChars(II[CI)V

    .line 508
    const/4 v10, 0x0

    .restart local v10    # "i":I
    :goto_6
    array-length v0, v5

    move/from16 v23, v0

    move/from16 v0, v23

    if-ge v10, v0, :cond_9

    .line 510
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/util/ContentParser;->text:Ljava/lang/StringBuilder;

    move-object/from16 v23, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/graphics/TextState;->currentfont:Lorg/icepdf/index/core/pobjects/fonts/FontFile;

    move-object/from16 v24, v0

    aget-char v25, v5, v10

    invoke-interface/range {v24 .. v25}, Lorg/icepdf/index/core/pobjects/fonts/FontFile;->toUnicode(C)Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 508
    add-int/lit8 v10, v10, 0x1

    goto :goto_6

    .line 515
    .end local v5    # "ch":[C
    .end local v10    # "i":I
    .end local v15    # "sb":Ljava/lang/StringBuffer;
    .end local v16    # "stringObject":Lorg/icepdf/index/core/pobjects/StringObject;
    .end local v17    # "textState":Lorg/icepdf/index/core/pobjects/graphics/TextState;
    :cond_c
    instance-of v0, v6, Ljava/lang/Number;

    move/from16 v23, v0

    if-eqz v23, :cond_9

    move-object v8, v6

    .line 516
    check-cast v8, Ljava/lang/Number;

    .line 517
    .local v8, "f":Ljava/lang/Number;
    invoke-virtual {v8}, Ljava/lang/Number;->intValue()I

    move-result v23

    const/16 v24, -0x64

    move/from16 v0, v23

    move/from16 v1, v24

    if-ge v0, v1, :cond_9

    .line 518
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/util/ContentParser;->text:Ljava/lang/StringBuilder;

    move-object/from16 v23, v0

    const-string/jumbo v24, " "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_4

    .line 528
    .end local v6    # "currentObject":Ljava/lang/Object;
    .end local v7    # "e":Ljava/util/Enumeration;
    .end local v8    # "f":Ljava/lang/Number;
    .end local v20    # "v":Ljava/util/Vector;
    :cond_d
    const-string/jumbo v23, "TD"

    move-object/from16 v0, v23

    invoke-virtual {v14, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_f

    .line 530
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/util/ContentParser;->stack:Lorg/icepdf/index/core/util/ContentParser$CustomStack;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lorg/icepdf/index/core/util/ContentParser$CustomStack;->pop()Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Ljava/lang/Number;

    invoke-virtual/range {v23 .. v23}, Ljava/lang/Number;->floatValue()F

    move-result v21

    .line 531
    .restart local v21    # "y":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/util/ContentParser;->stack:Lorg/icepdf/index/core/util/ContentParser$CustomStack;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lorg/icepdf/index/core/util/ContentParser$CustomStack;->pop()Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Ljava/lang/Number;

    invoke-virtual/range {v23 .. v23}, Ljava/lang/Number;->floatValue()F

    .line 533
    if-eqz v11, :cond_e

    .line 534
    move/from16 v22, v21

    .line 535
    const/4 v11, 0x0

    .line 539
    :cond_e
    const/16 v23, 0x0

    cmpl-float v23, v21, v23

    if-eqz v23, :cond_1

    .line 540
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/util/ContentParser;->text:Ljava/lang/StringBuilder;

    move-object/from16 v23, v0

    const-string/jumbo v24, " "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_2

    .line 546
    .end local v21    # "y":F
    :cond_f
    const-string/jumbo v23, "TL"

    move-object/from16 v0, v23

    invoke-virtual {v14, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_10

    .line 548
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/util/ContentParser;->graphicState:Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->getTextState()Lorg/icepdf/index/core/pobjects/graphics/TextState;

    move-result-object v24

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/util/ContentParser;->stack:Lorg/icepdf/index/core/util/ContentParser$CustomStack;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lorg/icepdf/index/core/util/ContentParser$CustomStack;->pop()Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Ljava/lang/Number;

    invoke-virtual/range {v23 .. v23}, Ljava/lang/Number;->floatValue()F

    move-result v23

    move/from16 v0, v23

    move-object/from16 v1, v24

    iput v0, v1, Lorg/icepdf/index/core/pobjects/graphics/TextState;->leading:F

    goto/16 :goto_2

    .line 550
    :cond_10
    const-string/jumbo v23, "Tm"

    move-object/from16 v0, v23

    invoke-virtual {v14, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_14

    .line 552
    const/16 v23, 0x0

    const/16 v24, 0x0

    move/from16 v0, v23

    move/from16 v1, v24

    invoke-virtual {v4, v0, v1}, Lorg/icepdf/index/java/awt/geom/Point2D$Float;->setLocation(FF)V

    .line 558
    const/16 v23, 0x6

    move/from16 v0, v23

    new-array v0, v0, [F

    move-object/from16 v19, v0

    fill-array-data v19, :array_0

    .line 559
    .local v19, "tm":[F
    const/4 v10, 0x0

    .restart local v10    # "i":I
    const/4 v9, 0x5

    .local v9, "hits":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/util/ContentParser;->stack:Lorg/icepdf/index/core/util/ContentParser$CustomStack;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lorg/icepdf/index/core/util/ContentParser$CustomStack;->size()I

    move-result v12

    .local v12, "max":I
    :goto_7
    const/16 v23, -0x1

    move/from16 v0, v23

    if-eq v9, v0, :cond_12

    if-ge v10, v12, :cond_12

    .line 560
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/util/ContentParser;->stack:Lorg/icepdf/index/core/util/ContentParser$CustomStack;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lorg/icepdf/index/core/util/ContentParser$CustomStack;->pop()Ljava/lang/Object;

    move-result-object v13

    .line 561
    .local v13, "next":Ljava/lang/Object;
    instance-of v0, v13, Ljava/lang/Number;

    move/from16 v23, v0

    if-eqz v23, :cond_11

    .line 562
    check-cast v13, Ljava/lang/Number;

    .end local v13    # "next":Ljava/lang/Object;
    invoke-virtual {v13}, Ljava/lang/Number;->floatValue()F

    move-result v23

    aput v23, v19, v9

    .line 563
    add-int/lit8 v9, v9, -0x1

    .line 559
    :cond_11
    add-int/lit8 v10, v10, 0x1

    goto :goto_7

    .line 567
    :cond_12
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/util/ContentParser;->graphicState:Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;

    move-object/from16 v23, v0

    const/high16 v24, 0x3f800000    # 1.0f

    const/high16 v25, -0x40800000    # -1.0f

    invoke-virtual/range {v23 .. v25}, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->scale(FF)V

    .line 574
    if-eqz v11, :cond_13

    .line 575
    const/4 v11, 0x0

    .line 577
    :cond_13
    const/16 v23, 0x5

    aget v22, v19, v23

    .line 578
    move/from16 v0, v22

    float-to-double v0, v0

    move-wide/from16 v24, v0

    cmpl-double v23, p3, v24

    if-eqz v23, :cond_1

    .line 579
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/util/ContentParser;->text:Ljava/lang/StringBuilder;

    move-object/from16 v23, v0

    const-string/jumbo v24, " "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_2

    .line 583
    .end local v9    # "hits":I
    .end local v10    # "i":I
    .end local v12    # "max":I
    .end local v19    # "tm":[F
    :cond_14
    const-string/jumbo v23, "T*"

    move-object/from16 v0, v23

    invoke-virtual {v14, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_15

    .line 585
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/util/ContentParser;->text:Ljava/lang/StringBuilder;

    move-object/from16 v23, v0

    const-string/jumbo v24, " "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_2

    .line 586
    :cond_15
    const-string/jumbo v23, "BDC"

    move-object/from16 v0, v23

    invoke-virtual {v14, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_16

    .line 588
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/util/ContentParser;->stack:Lorg/icepdf/index/core/util/ContentParser$CustomStack;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lorg/icepdf/index/core/util/ContentParser$CustomStack;->pop()Ljava/lang/Object;

    .line 589
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/util/ContentParser;->stack:Lorg/icepdf/index/core/util/ContentParser$CustomStack;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lorg/icepdf/index/core/util/ContentParser$CustomStack;->pop()Ljava/lang/Object;

    goto/16 :goto_2

    .line 590
    :cond_16
    const-string/jumbo v23, "EMC"

    move-object/from16 v0, v23

    invoke-virtual {v14, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-nez v23, :cond_1

    .line 595
    const-string/jumbo v23, "\'"

    move-object/from16 v0, v23

    invoke-virtual {v14, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_19

    .line 597
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/util/ContentParser;->stack:Lorg/icepdf/index/core/util/ContentParser$CustomStack;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lorg/icepdf/index/core/util/ContentParser$CustomStack;->pop()Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lorg/icepdf/index/core/pobjects/StringObject;

    .line 599
    .restart local v16    # "stringObject":Lorg/icepdf/index/core/pobjects/StringObject;
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/util/ContentParser;->graphicState:Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->getTextState()Lorg/icepdf/index/core/pobjects/graphics/TextState;

    move-result-object v17

    .line 601
    .restart local v17    # "textState":Lorg/icepdf/index/core/pobjects/graphics/TextState;
    if-eqz v16, :cond_1

    .line 603
    move-object/from16 v0, v16

    instance-of v0, v0, Lorg/icepdf/index/core/pobjects/LiteralStringObject;

    move/from16 v23, v0

    if-eqz v23, :cond_18

    .line 605
    move-object/from16 v0, v17

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/graphics/TextState;->font:Lorg/icepdf/index/core/pobjects/fonts/Font;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lorg/icepdf/index/core/pobjects/fonts/Font;->getSubTypeFormat()I

    move-result v23

    const/16 v24, 0x1

    move/from16 v0, v23

    move/from16 v1, v24

    if-ne v0, v1, :cond_17

    .line 607
    move-object/from16 v0, v17

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/graphics/TextState;->font:Lorg/icepdf/index/core/pobjects/fonts/Font;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lorg/icepdf/index/core/pobjects/fonts/Font;->getSubTypeFormat()I

    move-result v23

    move-object/from16 v0, v17

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/graphics/TextState;->font:Lorg/icepdf/index/core/pobjects/fonts/Font;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Lorg/icepdf/index/core/pobjects/fonts/Font;->getFont()Lorg/icepdf/index/core/pobjects/fonts/FontFile;

    move-result-object v24

    move-object/from16 v0, v16

    move/from16 v1, v23

    move-object/from16 v2, v24

    invoke-interface {v0, v1, v2}, Lorg/icepdf/index/core/pobjects/StringObject;->getLiteralStringBuffer(ILorg/icepdf/index/core/pobjects/fonts/FontFile;)Ljava/lang/StringBuffer;

    move-result-object v15

    .line 610
    .restart local v15    # "sb":Ljava/lang/StringBuffer;
    invoke-virtual {v15}, Ljava/lang/StringBuffer;->length()I

    move-result v23

    move/from16 v0, v23

    new-array v5, v0, [C

    .line 611
    .restart local v5    # "ch":[C
    const/16 v23, 0x0

    invoke-virtual {v15}, Ljava/lang/StringBuffer;->length()I

    move-result v24

    const/16 v25, 0x0

    move/from16 v0, v23

    move/from16 v1, v24

    move/from16 v2, v25

    invoke-virtual {v15, v0, v1, v5, v2}, Ljava/lang/StringBuffer;->getChars(II[CI)V

    .line 612
    const/4 v10, 0x0

    .restart local v10    # "i":I
    :goto_8
    array-length v0, v5

    move/from16 v23, v0

    move/from16 v0, v23

    if-ge v10, v0, :cond_1

    .line 614
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/util/ContentParser;->text:Ljava/lang/StringBuilder;

    move-object/from16 v23, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/graphics/TextState;->currentfont:Lorg/icepdf/index/core/pobjects/fonts/FontFile;

    move-object/from16 v24, v0

    aget-char v25, v5, v10

    invoke-interface/range {v24 .. v25}, Lorg/icepdf/index/core/pobjects/fonts/FontFile;->toUnicode(C)Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 612
    add-int/lit8 v10, v10, 0x1

    goto :goto_8

    .line 618
    .end local v5    # "ch":[C
    .end local v10    # "i":I
    .end local v15    # "sb":Ljava/lang/StringBuffer;
    :cond_17
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/util/ContentParser;->text:Ljava/lang/StringBuilder;

    move-object/from16 v23, v0

    invoke-interface/range {v16 .. v16}, Lorg/icepdf/index/core/pobjects/StringObject;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_2

    .line 620
    :cond_18
    move-object/from16 v0, v16

    instance-of v0, v0, Lorg/icepdf/index/core/pobjects/HexStringObject;

    move/from16 v23, v0

    if-eqz v23, :cond_1

    .line 622
    move-object/from16 v0, v17

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/graphics/TextState;->font:Lorg/icepdf/index/core/pobjects/fonts/Font;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lorg/icepdf/index/core/pobjects/fonts/Font;->getSubTypeFormat()I

    move-result v23

    move-object/from16 v0, v17

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/graphics/TextState;->font:Lorg/icepdf/index/core/pobjects/fonts/Font;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Lorg/icepdf/index/core/pobjects/fonts/Font;->getFont()Lorg/icepdf/index/core/pobjects/fonts/FontFile;

    move-result-object v24

    move-object/from16 v0, v16

    move/from16 v1, v23

    move-object/from16 v2, v24

    invoke-interface {v0, v1, v2}, Lorg/icepdf/index/core/pobjects/StringObject;->getLiteralStringBuffer(ILorg/icepdf/index/core/pobjects/fonts/FontFile;)Ljava/lang/StringBuffer;

    move-result-object v15

    .line 625
    .restart local v15    # "sb":Ljava/lang/StringBuffer;
    invoke-virtual {v15}, Ljava/lang/StringBuffer;->length()I

    move-result v23

    move/from16 v0, v23

    new-array v5, v0, [C

    .line 626
    .restart local v5    # "ch":[C
    const/16 v23, 0x0

    invoke-virtual {v15}, Ljava/lang/StringBuffer;->length()I

    move-result v24

    const/16 v25, 0x0

    move/from16 v0, v23

    move/from16 v1, v24

    move/from16 v2, v25

    invoke-virtual {v15, v0, v1, v5, v2}, Ljava/lang/StringBuffer;->getChars(II[CI)V

    .line 627
    const/4 v10, 0x0

    .restart local v10    # "i":I
    :goto_9
    array-length v0, v5

    move/from16 v23, v0

    move/from16 v0, v23

    if-ge v10, v0, :cond_1

    .line 629
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/util/ContentParser;->text:Ljava/lang/StringBuilder;

    move-object/from16 v23, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/graphics/TextState;->currentfont:Lorg/icepdf/index/core/pobjects/fonts/FontFile;

    move-object/from16 v24, v0

    aget-char v25, v5, v10

    invoke-interface/range {v24 .. v25}, Lorg/icepdf/index/core/pobjects/fonts/FontFile;->toUnicode(C)Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 627
    add-int/lit8 v10, v10, 0x1

    goto :goto_9

    .line 642
    .end local v5    # "ch":[C
    .end local v10    # "i":I
    .end local v15    # "sb":Ljava/lang/StringBuffer;
    .end local v16    # "stringObject":Lorg/icepdf/index/core/pobjects/StringObject;
    .end local v17    # "textState":Lorg/icepdf/index/core/pobjects/graphics/TextState;
    :cond_19
    const-string/jumbo v23, "\""

    move-object/from16 v0, v23

    invoke-virtual {v14, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_1

    .line 644
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/util/ContentParser;->stack:Lorg/icepdf/index/core/util/ContentParser$CustomStack;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lorg/icepdf/index/core/util/ContentParser$CustomStack;->pop()Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lorg/icepdf/index/core/pobjects/StringObject;

    .line 645
    .restart local v16    # "stringObject":Lorg/icepdf/index/core/pobjects/StringObject;
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/util/ContentParser;->graphicState:Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->getTextState()Lorg/icepdf/index/core/pobjects/graphics/TextState;

    move-result-object v17

    .line 647
    .restart local v17    # "textState":Lorg/icepdf/index/core/pobjects/graphics/TextState;
    if-eqz v16, :cond_1

    .line 649
    move-object/from16 v0, v16

    instance-of v0, v0, Lorg/icepdf/index/core/pobjects/LiteralStringObject;

    move/from16 v23, v0

    if-eqz v23, :cond_1b

    .line 651
    move-object/from16 v0, v17

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/graphics/TextState;->font:Lorg/icepdf/index/core/pobjects/fonts/Font;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lorg/icepdf/index/core/pobjects/fonts/Font;->getSubTypeFormat()I

    move-result v23

    const/16 v24, 0x1

    move/from16 v0, v23

    move/from16 v1, v24

    if-ne v0, v1, :cond_1a

    .line 653
    move-object/from16 v0, v17

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/graphics/TextState;->font:Lorg/icepdf/index/core/pobjects/fonts/Font;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lorg/icepdf/index/core/pobjects/fonts/Font;->getSubTypeFormat()I

    move-result v23

    move-object/from16 v0, v17

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/graphics/TextState;->font:Lorg/icepdf/index/core/pobjects/fonts/Font;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Lorg/icepdf/index/core/pobjects/fonts/Font;->getFont()Lorg/icepdf/index/core/pobjects/fonts/FontFile;

    move-result-object v24

    move-object/from16 v0, v16

    move/from16 v1, v23

    move-object/from16 v2, v24

    invoke-interface {v0, v1, v2}, Lorg/icepdf/index/core/pobjects/StringObject;->getLiteralStringBuffer(ILorg/icepdf/index/core/pobjects/fonts/FontFile;)Ljava/lang/StringBuffer;

    move-result-object v15

    .line 656
    .restart local v15    # "sb":Ljava/lang/StringBuffer;
    invoke-virtual {v15}, Ljava/lang/StringBuffer;->length()I

    move-result v23

    move/from16 v0, v23

    new-array v5, v0, [C

    .line 657
    .restart local v5    # "ch":[C
    const/16 v23, 0x0

    invoke-virtual {v15}, Ljava/lang/StringBuffer;->length()I

    move-result v24

    const/16 v25, 0x0

    move/from16 v0, v23

    move/from16 v1, v24

    move/from16 v2, v25

    invoke-virtual {v15, v0, v1, v5, v2}, Ljava/lang/StringBuffer;->getChars(II[CI)V

    .line 658
    const/4 v10, 0x0

    .restart local v10    # "i":I
    :goto_a
    array-length v0, v5

    move/from16 v23, v0

    move/from16 v0, v23

    if-ge v10, v0, :cond_1

    .line 660
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/util/ContentParser;->text:Ljava/lang/StringBuilder;

    move-object/from16 v23, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/graphics/TextState;->currentfont:Lorg/icepdf/index/core/pobjects/fonts/FontFile;

    move-object/from16 v24, v0

    aget-char v25, v5, v10

    invoke-interface/range {v24 .. v25}, Lorg/icepdf/index/core/pobjects/fonts/FontFile;->toUnicode(C)Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 658
    add-int/lit8 v10, v10, 0x1

    goto :goto_a

    .line 664
    .end local v5    # "ch":[C
    .end local v10    # "i":I
    .end local v15    # "sb":Ljava/lang/StringBuffer;
    :cond_1a
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/util/ContentParser;->text:Ljava/lang/StringBuilder;

    move-object/from16 v23, v0

    invoke-interface/range {v16 .. v16}, Lorg/icepdf/index/core/pobjects/StringObject;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_2

    .line 666
    :cond_1b
    move-object/from16 v0, v16

    instance-of v0, v0, Lorg/icepdf/index/core/pobjects/HexStringObject;

    move/from16 v23, v0

    if-eqz v23, :cond_1

    .line 668
    move-object/from16 v0, v17

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/graphics/TextState;->font:Lorg/icepdf/index/core/pobjects/fonts/Font;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lorg/icepdf/index/core/pobjects/fonts/Font;->getSubTypeFormat()I

    move-result v23

    move-object/from16 v0, v17

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/graphics/TextState;->font:Lorg/icepdf/index/core/pobjects/fonts/Font;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Lorg/icepdf/index/core/pobjects/fonts/Font;->getFont()Lorg/icepdf/index/core/pobjects/fonts/FontFile;

    move-result-object v24

    move-object/from16 v0, v16

    move/from16 v1, v23

    move-object/from16 v2, v24

    invoke-interface {v0, v1, v2}, Lorg/icepdf/index/core/pobjects/StringObject;->getLiteralStringBuffer(ILorg/icepdf/index/core/pobjects/fonts/FontFile;)Ljava/lang/StringBuffer;

    move-result-object v15

    .line 671
    .restart local v15    # "sb":Ljava/lang/StringBuffer;
    invoke-virtual {v15}, Ljava/lang/StringBuffer;->length()I

    move-result v23

    move/from16 v0, v23

    new-array v5, v0, [C

    .line 672
    .restart local v5    # "ch":[C
    const/16 v23, 0x0

    invoke-virtual {v15}, Ljava/lang/StringBuffer;->length()I

    move-result v24

    const/16 v25, 0x0

    move/from16 v0, v23

    move/from16 v1, v24

    move/from16 v2, v25

    invoke-virtual {v15, v0, v1, v5, v2}, Ljava/lang/StringBuffer;->getChars(II[CI)V

    .line 673
    const/4 v10, 0x0

    .restart local v10    # "i":I
    :goto_b
    array-length v0, v5

    move/from16 v23, v0

    move/from16 v0, v23

    if-ge v10, v0, :cond_1

    .line 675
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/util/ContentParser;->text:Ljava/lang/StringBuilder;

    move-object/from16 v23, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/graphics/TextState;->currentfont:Lorg/icepdf/index/core/pobjects/fonts/FontFile;

    move-object/from16 v24, v0

    aget-char v25, v5, v10

    invoke-interface/range {v24 .. v25}, Lorg/icepdf/index/core/pobjects/fonts/FontFile;->toUnicode(C)Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 673
    add-int/lit8 v10, v10, 0x1

    goto :goto_b

    .line 685
    .end local v5    # "ch":[C
    .end local v10    # "i":I
    .end local v15    # "sb":Ljava/lang/StringBuffer;
    .end local v16    # "stringObject":Lorg/icepdf/index/core/pobjects/StringObject;
    .end local v17    # "textState":Lorg/icepdf/index/core/pobjects/graphics/TextState;
    :cond_1c
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/util/ContentParser;->stack:Lorg/icepdf/index/core/util/ContentParser$CustomStack;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    invoke-virtual {v0, v14}, Lorg/icepdf/index/core/util/ContentParser$CustomStack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_2

    .line 691
    :cond_1d
    :goto_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/util/ContentParser;->stack:Lorg/icepdf/index/core/util/ContentParser$CustomStack;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lorg/icepdf/index/core/util/ContentParser$CustomStack;->isEmpty()Z

    move-result v23

    if-nez v23, :cond_1e

    .line 693
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/util/ContentParser;->stack:Lorg/icepdf/index/core/util/ContentParser$CustomStack;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lorg/icepdf/index/core/util/ContentParser$CustomStack;->pop()Ljava/lang/Object;

    goto :goto_c

    .line 698
    :cond_1e
    return v22

    .line 558
    nop

    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x0
        0x0
        0x3f800000    # 1.0f
        0x0
        0x0
    .end array-data
.end method

.method public parseTextBlocks(Ljava/io/InputStream;)Ljava/lang/StringBuilder;
    .locals 8
    .param p1, "source"    # Ljava/io/InputStream;

    .prologue
    .line 225
    new-instance v1, Lorg/icepdf/index/core/util/Parser;

    invoke-direct {v1, p1}, Lorg/icepdf/index/core/util/Parser;-><init>(Ljava/io/InputStream;)V

    .line 226
    .local v1, "parser":Lorg/icepdf/index/core/util/Parser;
    new-instance v2, Lorg/icepdf/index/core/pobjects/graphics/Shapes;

    invoke-direct {v2}, Lorg/icepdf/index/core/pobjects/graphics/Shapes;-><init>()V

    .line 228
    .local v2, "shapes":Lorg/icepdf/index/core/pobjects/graphics/Shapes;
    iget-object v6, p0, Lorg/icepdf/index/core/util/ContentParser;->graphicState:Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;

    if-nez v6, :cond_0

    .line 229
    new-instance v6, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;

    invoke-direct {v6, v2}, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;-><init>(Lorg/icepdf/index/core/pobjects/graphics/Shapes;)V

    iput-object v6, p0, Lorg/icepdf/index/core/util/ContentParser;->graphicState:Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;

    .line 231
    :cond_0
    const/4 v5, 0x0

    .line 236
    .local v5, "yBTstart":F
    :try_start_0
    invoke-virtual {v1}, Lorg/icepdf/index/core/util/Parser;->getStreamObject()Ljava/lang/Object;

    move-result-object v4

    .line 237
    .local v4, "tok":Ljava/lang/Object;
    new-instance v3, Lorg/icepdf/index/core/util/ContentParser$CustomStack;

    const/4 v6, 0x2

    invoke-direct {v3, v6}, Lorg/icepdf/index/core/util/ContentParser$CustomStack;-><init>(I)V

    .line 239
    .local v3, "stack":Ljava/util/Stack;, "Ljava/util/Stack<Ljava/lang/Object;>;"
    const/4 v0, 0x0

    .line 240
    .local v0, "objCount":I
    :goto_0
    if-eqz v4, :cond_1

    .line 241
    add-int/lit8 v0, v0, 0x1

    .line 242
    const v6, 0x7a120

    if-le v0, v6, :cond_2

    .line 243
    const-string/jumbo v6, "ANDICEPDF.ContentParser"

    const-string/jumbo v7, "Object Count greater than 1000000... So breaking..."

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 277
    :cond_1
    invoke-virtual {v3}, Ljava/util/Stack;->clear()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 284
    .end local v0    # "objCount":I
    .end local v3    # "stack":Ljava/util/Stack;, "Ljava/util/Stack<Ljava/lang/Object;>;"
    .end local v4    # "tok":Ljava/lang/Object;
    :goto_1
    invoke-virtual {v2}, Lorg/icepdf/index/core/pobjects/graphics/Shapes;->dispose()V

    .line 285
    invoke-virtual {p0}, Lorg/icepdf/index/core/util/ContentParser;->getExtractedText()Ljava/lang/StringBuilder;

    move-result-object v6

    return-object v6

    .line 249
    .restart local v0    # "objCount":I
    .restart local v3    # "stack":Ljava/util/Stack;, "Ljava/util/Stack<Ljava/lang/Object;>;"
    .restart local v4    # "tok":Ljava/lang/Object;
    :cond_2
    :try_start_1
    instance-of v6, v4, Ljava/lang/String;

    if-eqz v6, :cond_6

    .line 251
    const-string/jumbo v6, "BT"

    invoke-virtual {v4, v6}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 253
    float-to-double v6, v5

    invoke-virtual {p0, v1, v2, v6, v7}, Lorg/icepdf/index/core/util/ContentParser;->parseText(Lorg/icepdf/index/core/util/Parser;Lorg/icepdf/index/core/pobjects/graphics/Shapes;D)F

    move-result v5

    .line 258
    invoke-virtual {v3}, Ljava/util/Stack;->clear()V

    .line 274
    :cond_3
    :goto_2
    invoke-virtual {v1}, Lorg/icepdf/index/core/util/Parser;->getStreamObject()Ljava/lang/Object;

    move-result-object v4

    goto :goto_0

    .line 262
    :cond_4
    const-string/jumbo v6, "Tf"

    invoke-virtual {v4, v6}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 263
    iget-object v6, p0, Lorg/icepdf/index/core/util/ContentParser;->graphicState:Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;

    iget-object v7, p0, Lorg/icepdf/index/core/util/ContentParser;->resources:Lorg/icepdf/index/core/pobjects/Resources;

    invoke-static {v6, v3, v7}, Lorg/icepdf/index/core/util/ContentParser;->consume_Tf(Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;Ljava/util/Stack;Lorg/icepdf/index/core/pobjects/Resources;)V

    .line 264
    invoke-virtual {v3}, Ljava/util/Stack;->clear()V

    goto :goto_2

    .line 278
    .end local v0    # "objCount":I
    .end local v3    # "stack":Ljava/util/Stack;, "Ljava/util/Stack<Ljava/lang/Object;>;"
    .end local v4    # "tok":Ljava/lang/Object;
    :catch_0
    move-exception v6

    goto :goto_1

    .line 267
    .restart local v0    # "objCount":I
    .restart local v3    # "stack":Ljava/util/Stack;, "Ljava/util/Stack<Ljava/lang/Object;>;"
    .restart local v4    # "tok":Ljava/lang/Object;
    :cond_5
    const-string/jumbo v6, "BI"

    invoke-virtual {v4, v6}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 268
    invoke-direct {p0, v1}, Lorg/icepdf/index/core/util/ContentParser;->parseInlineImage(Lorg/icepdf/index/core/util/Parser;)V

    goto :goto_2

    .line 272
    :cond_6
    invoke-virtual {v3, v4}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2
.end method

.method public parseTextContent(Ljava/io/InputStream;)Ljava/lang/String;
    .locals 21
    .param p1, "source"    # Ljava/io/InputStream;

    .prologue
    .line 711
    new-instance v8, Lorg/icepdf/index/core/util/Parser;

    move-object/from16 v0, p1

    invoke-direct {v8, v0}, Lorg/icepdf/index/core/util/Parser;-><init>(Ljava/io/InputStream;)V

    .line 712
    .local v8, "parser":Lorg/icepdf/index/core/util/Parser;
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    .line 714
    .local v13, "textContentBuilder":Ljava/lang/StringBuilder;
    const/16 v2, 0x20

    .line 715
    .local v2, "SPACE":C
    const/4 v9, 0x0

    .line 716
    .local v9, "shapes":Lorg/icepdf/index/core/pobjects/graphics/Shapes;
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/util/ContentParser;->graphicState:Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;

    move-object/from16 v19, v0

    if-nez v19, :cond_0

    .line 717
    new-instance v9, Lorg/icepdf/index/core/pobjects/graphics/Shapes;

    .end local v9    # "shapes":Lorg/icepdf/index/core/pobjects/graphics/Shapes;
    invoke-direct {v9}, Lorg/icepdf/index/core/pobjects/graphics/Shapes;-><init>()V

    .line 718
    .restart local v9    # "shapes":Lorg/icepdf/index/core/pobjects/graphics/Shapes;
    new-instance v19, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;

    move-object/from16 v0, v19

    invoke-direct {v0, v9}, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;-><init>(Lorg/icepdf/index/core/pobjects/graphics/Shapes;)V

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/icepdf/index/core/util/ContentParser;->graphicState:Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;

    .line 720
    :cond_0
    const/4 v5, 0x1

    .line 724
    .local v5, "extractDirectly":Z
    :try_start_0
    invoke-virtual {v8}, Lorg/icepdf/index/core/util/Parser;->getStreamObject()Ljava/lang/Object;

    move-result-object v16

    .line 725
    .local v16, "tok":Ljava/lang/Object;
    new-instance v10, Ljava/util/Stack;

    invoke-direct {v10}, Ljava/util/Stack;-><init>()V

    .line 727
    .local v10, "stack":Ljava/util/Stack;, "Ljava/util/Stack<Ljava/lang/Object;>;"
    const/4 v7, 0x0

    .line 729
    :goto_0
    if-eqz v16, :cond_1f

    .line 732
    move-object/from16 v0, v16

    instance-of v0, v0, Ljava/lang/String;

    move/from16 v19, v0

    if-eqz v19, :cond_1e

    .line 734
    const-string/jumbo v19, "BT"

    move-object/from16 v0, v16

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_1d

    .line 738
    invoke-virtual {v8}, Lorg/icepdf/index/core/util/Parser;->getStreamObject()Ljava/lang/Object;

    move-result-object v7

    .line 740
    .local v7, "nextToken":Ljava/lang/Object;
    new-instance v11, Ljava/util/Stack;

    invoke-direct {v11}, Ljava/util/Stack;-><init>()V

    .line 742
    .local v11, "stackText":Ljava/util/Stack;, "Ljava/util/Stack<Ljava/lang/Object;>;"
    :goto_1
    const-string/jumbo v19, "ET"

    move-object/from16 v0, v19

    invoke-virtual {v7, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-nez v19, :cond_1a

    .line 746
    instance-of v0, v7, Ljava/lang/String;

    move/from16 v19, v0

    if-eqz v19, :cond_e

    .line 749
    const-string/jumbo v19, "Tj"

    move-object/from16 v0, v19

    invoke-virtual {v7, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_4

    .line 750
    invoke-virtual {v11}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v15

    .line 753
    .local v15, "tjValue":Ljava/lang/Object;
    instance-of v0, v15, Lorg/icepdf/index/core/pobjects/StringObject;

    move/from16 v19, v0

    if-eqz v19, :cond_1

    .line 754
    move-object v0, v15

    check-cast v0, Lorg/icepdf/index/core/pobjects/StringObject;

    move-object v12, v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 755
    .local v12, "stringObject":Lorg/icepdf/index/core/pobjects/StringObject;
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_2
    const/16 v19, 0x2

    move/from16 v0, v19

    if-ge v6, v0, :cond_1

    .line 757
    if-eqz v5, :cond_2

    .line 758
    :try_start_1
    move-object v0, v15

    check-cast v0, Lorg/icepdf/index/core/pobjects/StringObject;

    move-object v12, v0

    .line 759
    invoke-interface {v12}, Lorg/icepdf/index/core/pobjects/StringObject;->getLiteralString()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_1
    .catch Ljava/lang/StringIndexOutOfBoundsException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 939
    .end local v6    # "i":I
    .end local v12    # "stringObject":Lorg/icepdf/index/core/pobjects/StringObject;
    .end local v15    # "tjValue":Ljava/lang/Object;
    :cond_1
    :goto_3
    :try_start_2
    invoke-virtual {v8}, Lorg/icepdf/index/core/util/Parser;->getStreamObject()Ljava/lang/Object;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    move-result-object v7

    goto :goto_1

    .line 764
    .restart local v6    # "i":I
    .restart local v12    # "stringObject":Lorg/icepdf/index/core/pobjects/StringObject;
    .restart local v15    # "tjValue":Ljava/lang/Object;
    :cond_2
    :try_start_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/util/ContentParser;->graphicState:Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->getTextState()Lorg/icepdf/index/core/pobjects/graphics/TextState;

    move-result-object v14

    .line 766
    .local v14, "textState":Lorg/icepdf/index/core/pobjects/graphics/TextState;
    iget-object v0, v14, Lorg/icepdf/index/core/pobjects/graphics/TextState;->font:Lorg/icepdf/index/core/pobjects/fonts/Font;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lorg/icepdf/index/core/pobjects/fonts/Font;->getSubTypeFormat()I

    move-result v19

    iget-object v0, v14, Lorg/icepdf/index/core/pobjects/graphics/TextState;->font:Lorg/icepdf/index/core/pobjects/fonts/Font;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lorg/icepdf/index/core/pobjects/fonts/Font;->getFont()Lorg/icepdf/index/core/pobjects/fonts/FontFile;

    move-result-object v20

    move/from16 v0, v19

    move-object/from16 v1, v20

    invoke-interface {v12, v0, v1}, Lorg/icepdf/index/core/pobjects/StringObject;->getLiteralStringBuffer(ILorg/icepdf/index/core/pobjects/fonts/FontFile;)Ljava/lang/StringBuffer;

    move-result-object v19

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v0, v1, v14}, Lorg/icepdf/index/core/util/ContentParser;->getUnicodeStringFromText(Ljava/lang/StringBuffer;Lorg/icepdf/index/core/pobjects/graphics/TextState;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;
    :try_end_3
    .catch Ljava/lang/StringIndexOutOfBoundsException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_3

    .line 772
    .end local v14    # "textState":Lorg/icepdf/index/core/pobjects/graphics/TextState;
    :catch_0
    move-exception v4

    .line 773
    .local v4, "e":Ljava/lang/StringIndexOutOfBoundsException;
    if-eqz v5, :cond_3

    .line 774
    const/4 v5, 0x0

    .line 755
    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    .line 776
    :cond_3
    :try_start_4
    throw v4
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    .line 969
    .end local v4    # "e":Ljava/lang/StringIndexOutOfBoundsException;
    .end local v6    # "i":I
    .end local v7    # "nextToken":Ljava/lang/Object;
    .end local v10    # "stack":Ljava/util/Stack;, "Ljava/util/Stack<Ljava/lang/Object;>;"
    .end local v11    # "stackText":Ljava/util/Stack;, "Ljava/util/Stack<Ljava/lang/Object;>;"
    .end local v12    # "stringObject":Lorg/icepdf/index/core/pobjects/StringObject;
    .end local v15    # "tjValue":Ljava/lang/Object;
    .end local v16    # "tok":Ljava/lang/Object;
    :catch_1
    move-exception v4

    .line 971
    .local v4, "e":Ljava/io/IOException;
    const-string/jumbo v19, "ANDICEPDF.ContentParser"

    const-string/jumbo v20, "End of Content Stream"

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 974
    .end local v4    # "e":Ljava/io/IOException;
    :goto_4
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    return-object v19

    .line 783
    .restart local v7    # "nextToken":Ljava/lang/Object;
    .restart local v10    # "stack":Ljava/util/Stack;, "Ljava/util/Stack<Ljava/lang/Object;>;"
    .restart local v11    # "stackText":Ljava/util/Stack;, "Ljava/util/Stack<Ljava/lang/Object;>;"
    .restart local v16    # "tok":Ljava/lang/Object;
    :cond_4
    :try_start_5
    const-string/jumbo v19, "Tc"

    move-object/from16 v0, v19

    invoke-virtual {v7, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_5

    .line 784
    invoke-virtual {v11}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    goto :goto_3

    .line 787
    :cond_5
    const-string/jumbo v19, "Tw"

    move-object/from16 v0, v19

    invoke-virtual {v7, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_6

    .line 788
    invoke-virtual {v11}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    goto :goto_3

    .line 792
    :cond_6
    const-string/jumbo v19, "Td"

    move-object/from16 v0, v19

    invoke-virtual {v7, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_8

    .line 793
    invoke-virtual {v11}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/lang/Number;

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Number;->floatValue()F

    move-result v18

    .line 794
    .local v18, "y":F
    const/16 v19, 0x0

    cmpl-float v19, v18, v19

    if-eqz v19, :cond_7

    .line 795
    const/16 v19, 0x20

    move/from16 v0, v19

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 797
    :cond_7
    invoke-virtual {v11}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    goto/16 :goto_3

    .line 800
    .end local v18    # "y":F
    :cond_8
    const-string/jumbo v19, "Tf"

    move-object/from16 v0, v19

    invoke-virtual {v7, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_9

    .line 801
    const/16 v19, 0x20

    move/from16 v0, v19

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 802
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/util/ContentParser;->graphicState:Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/util/ContentParser;->resources:Lorg/icepdf/index/core/pobjects/Resources;

    move-object/from16 v20, v0

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-static {v0, v11, v1}, Lorg/icepdf/index/core/util/ContentParser;->consume_Tf(Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;Ljava/util/Stack;Lorg/icepdf/index/core/pobjects/Resources;)V

    goto/16 :goto_3

    .line 805
    :cond_9
    const-string/jumbo v19, "TJ"

    move-object/from16 v0, v19

    invoke-virtual {v7, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_1

    .line 806
    invoke-virtual {v11}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Ljava/util/Vector;
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1

    .line 810
    .local v17, "v":Ljava/util/Vector;
    const/4 v6, 0x0

    .restart local v6    # "i":I
    :goto_5
    const/16 v19, 0x2

    move/from16 v0, v19

    if-ge v6, v0, :cond_1

    .line 812
    if-eqz v5, :cond_b

    .line 813
    :try_start_6
    invoke-virtual/range {v17 .. v17}, Ljava/util/Vector;->elements()Ljava/util/Enumeration;

    move-result-object v4

    .local v4, "e":Ljava/util/Enumeration;
    :cond_a
    :goto_6
    invoke-interface {v4}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v19

    if-eqz v19, :cond_1

    .line 814
    invoke-interface {v4}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v3

    .line 815
    .local v3, "currentObject":Ljava/lang/Object;
    instance-of v0, v3, Lorg/icepdf/index/core/pobjects/StringObject;

    move/from16 v19, v0

    if-eqz v19, :cond_a

    .line 816
    move-object v0, v3

    check-cast v0, Lorg/icepdf/index/core/pobjects/StringObject;

    move-object v12, v0

    .line 817
    .restart local v12    # "stringObject":Lorg/icepdf/index/core/pobjects/StringObject;
    invoke-interface {v12}, Lorg/icepdf/index/core/pobjects/StringObject;->getLiteralString()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_6

    .line 836
    .end local v3    # "currentObject":Ljava/lang/Object;
    .end local v4    # "e":Ljava/util/Enumeration;
    .end local v12    # "stringObject":Lorg/icepdf/index/core/pobjects/StringObject;
    :catch_2
    move-exception v4

    .line 837
    .local v4, "e":Ljava/lang/StringIndexOutOfBoundsException;
    if-eqz v5, :cond_d

    .line 838
    const/4 v5, 0x0

    .line 810
    add-int/lit8 v6, v6, 0x1

    goto :goto_5

    .line 823
    .end local v4    # "e":Ljava/lang/StringIndexOutOfBoundsException;
    :cond_b
    invoke-virtual/range {v17 .. v17}, Ljava/util/Vector;->elements()Ljava/util/Enumeration;

    move-result-object v4

    .local v4, "e":Ljava/util/Enumeration;
    :cond_c
    :goto_7
    invoke-interface {v4}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v19

    if-eqz v19, :cond_1

    .line 824
    invoke-interface {v4}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v3

    .line 825
    .restart local v3    # "currentObject":Ljava/lang/Object;
    instance-of v0, v3, Lorg/icepdf/index/core/pobjects/StringObject;

    move/from16 v19, v0

    if-eqz v19, :cond_c

    .line 826
    move-object v0, v3

    check-cast v0, Lorg/icepdf/index/core/pobjects/StringObject;

    move-object v12, v0

    .line 827
    .restart local v12    # "stringObject":Lorg/icepdf/index/core/pobjects/StringObject;
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/util/ContentParser;->graphicState:Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->getTextState()Lorg/icepdf/index/core/pobjects/graphics/TextState;

    move-result-object v14

    .line 828
    .restart local v14    # "textState":Lorg/icepdf/index/core/pobjects/graphics/TextState;
    iget-object v0, v14, Lorg/icepdf/index/core/pobjects/graphics/TextState;->font:Lorg/icepdf/index/core/pobjects/fonts/Font;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lorg/icepdf/index/core/pobjects/fonts/Font;->getSubTypeFormat()I

    move-result v19

    iget-object v0, v14, Lorg/icepdf/index/core/pobjects/graphics/TextState;->font:Lorg/icepdf/index/core/pobjects/fonts/Font;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lorg/icepdf/index/core/pobjects/fonts/Font;->getFont()Lorg/icepdf/index/core/pobjects/fonts/FontFile;

    move-result-object v20

    move/from16 v0, v19

    move-object/from16 v1, v20

    invoke-interface {v12, v0, v1}, Lorg/icepdf/index/core/pobjects/StringObject;->getLiteralStringBuffer(ILorg/icepdf/index/core/pobjects/fonts/FontFile;)Ljava/lang/StringBuffer;

    move-result-object v19

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v0, v1, v14}, Lorg/icepdf/index/core/util/ContentParser;->getUnicodeStringFromText(Ljava/lang/StringBuffer;Lorg/icepdf/index/core/pobjects/graphics/TextState;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;
    :try_end_6
    .catch Ljava/lang/StringIndexOutOfBoundsException; {:try_start_6 .. :try_end_6} :catch_2
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1

    goto :goto_7

    .line 840
    .end local v3    # "currentObject":Ljava/lang/Object;
    .end local v12    # "stringObject":Lorg/icepdf/index/core/pobjects/StringObject;
    .end local v14    # "textState":Lorg/icepdf/index/core/pobjects/graphics/TextState;
    .local v4, "e":Ljava/lang/StringIndexOutOfBoundsException;
    :cond_d
    :try_start_7
    throw v4

    .line 850
    .end local v4    # "e":Ljava/lang/StringIndexOutOfBoundsException;
    .end local v6    # "i":I
    .end local v17    # "v":Ljava/util/Vector;
    :cond_e
    const-string/jumbo v19, "TD"

    move-object/from16 v0, v19

    invoke-virtual {v7, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_10

    .line 851
    invoke-virtual {v11}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/lang/Number;

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Number;->floatValue()F

    move-result v18

    .line 852
    .restart local v18    # "y":F
    const/16 v19, 0x0

    cmpl-float v19, v18, v19

    if-eqz v19, :cond_f

    .line 853
    const/16 v19, 0x20

    move/from16 v0, v19

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 855
    :cond_f
    invoke-virtual {v11}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    goto/16 :goto_3

    .line 858
    .end local v18    # "y":F
    :cond_10
    const-string/jumbo v19, "TL"

    move-object/from16 v0, v19

    invoke-virtual {v7, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_11

    .line 859
    invoke-virtual {v11}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    goto/16 :goto_3

    .line 862
    :cond_11
    const-string/jumbo v19, "T*"

    move-object/from16 v0, v19

    invoke-virtual {v7, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_12

    .line 863
    const/16 v19, 0x20

    move/from16 v0, v19

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    .line 864
    :cond_12
    const-string/jumbo v19, "BDC"

    move-object/from16 v0, v19

    invoke-virtual {v7, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_13

    .line 865
    invoke-virtual {v11}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    .line 866
    invoke-virtual {v11}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    goto/16 :goto_3

    .line 867
    :cond_13
    const-string/jumbo v19, "EMC"

    move-object/from16 v0, v19

    invoke-virtual {v7, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-nez v19, :cond_1

    .line 871
    const-string/jumbo v19, "\'"

    move-object/from16 v0, v19

    invoke-virtual {v7, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_16

    .line 872
    invoke-virtual {v11}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lorg/icepdf/index/core/pobjects/StringObject;
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_1

    .line 875
    .restart local v12    # "stringObject":Lorg/icepdf/index/core/pobjects/StringObject;
    const/4 v6, 0x0

    .restart local v6    # "i":I
    :goto_8
    const/16 v19, 0x2

    move/from16 v0, v19

    if-ge v6, v0, :cond_1

    .line 877
    if-eqz v5, :cond_14

    .line 878
    :try_start_8
    invoke-interface {v12}, Lorg/icepdf/index/core/pobjects/StringObject;->getLiteralString()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    .line 891
    :catch_3
    move-exception v4

    .line 892
    .restart local v4    # "e":Ljava/lang/StringIndexOutOfBoundsException;
    if-eqz v5, :cond_15

    .line 893
    const/4 v5, 0x0

    .line 875
    add-int/lit8 v6, v6, 0x1

    goto :goto_8

    .line 883
    .end local v4    # "e":Ljava/lang/StringIndexOutOfBoundsException;
    :cond_14
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/util/ContentParser;->graphicState:Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->getTextState()Lorg/icepdf/index/core/pobjects/graphics/TextState;

    move-result-object v14

    .line 885
    .restart local v14    # "textState":Lorg/icepdf/index/core/pobjects/graphics/TextState;
    iget-object v0, v14, Lorg/icepdf/index/core/pobjects/graphics/TextState;->font:Lorg/icepdf/index/core/pobjects/fonts/Font;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lorg/icepdf/index/core/pobjects/fonts/Font;->getSubTypeFormat()I

    move-result v19

    iget-object v0, v14, Lorg/icepdf/index/core/pobjects/graphics/TextState;->font:Lorg/icepdf/index/core/pobjects/fonts/Font;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lorg/icepdf/index/core/pobjects/fonts/Font;->getFont()Lorg/icepdf/index/core/pobjects/fonts/FontFile;

    move-result-object v20

    move/from16 v0, v19

    move-object/from16 v1, v20

    invoke-interface {v12, v0, v1}, Lorg/icepdf/index/core/pobjects/StringObject;->getLiteralStringBuffer(ILorg/icepdf/index/core/pobjects/fonts/FontFile;)Ljava/lang/StringBuffer;

    move-result-object v19

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v0, v1, v14}, Lorg/icepdf/index/core/util/ContentParser;->getUnicodeStringFromText(Ljava/lang/StringBuffer;Lorg/icepdf/index/core/pobjects/graphics/TextState;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;
    :try_end_8
    .catch Ljava/lang/StringIndexOutOfBoundsException; {:try_start_8 .. :try_end_8} :catch_3
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_1

    goto/16 :goto_3

    .line 895
    .end local v14    # "textState":Lorg/icepdf/index/core/pobjects/graphics/TextState;
    .restart local v4    # "e":Ljava/lang/StringIndexOutOfBoundsException;
    :cond_15
    :try_start_9
    throw v4

    .line 906
    .end local v4    # "e":Ljava/lang/StringIndexOutOfBoundsException;
    .end local v6    # "i":I
    .end local v12    # "stringObject":Lorg/icepdf/index/core/pobjects/StringObject;
    :cond_16
    const-string/jumbo v19, "\""

    move-object/from16 v0, v19

    invoke-virtual {v7, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_19

    .line 908
    invoke-virtual {v11}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lorg/icepdf/index/core/pobjects/StringObject;
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_1

    .line 911
    .restart local v12    # "stringObject":Lorg/icepdf/index/core/pobjects/StringObject;
    const/4 v6, 0x0

    .restart local v6    # "i":I
    :goto_9
    const/16 v19, 0x2

    move/from16 v0, v19

    if-ge v6, v0, :cond_1

    .line 913
    if-eqz v5, :cond_17

    .line 914
    :try_start_a
    invoke-interface {v12}, Lorg/icepdf/index/core/pobjects/StringObject;->getLiteralString()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    .line 927
    :catch_4
    move-exception v4

    .line 928
    .restart local v4    # "e":Ljava/lang/StringIndexOutOfBoundsException;
    if-eqz v5, :cond_18

    .line 929
    const/4 v5, 0x0

    .line 911
    add-int/lit8 v6, v6, 0x1

    goto :goto_9

    .line 919
    .end local v4    # "e":Ljava/lang/StringIndexOutOfBoundsException;
    :cond_17
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/util/ContentParser;->graphicState:Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->getTextState()Lorg/icepdf/index/core/pobjects/graphics/TextState;

    move-result-object v14

    .line 921
    .restart local v14    # "textState":Lorg/icepdf/index/core/pobjects/graphics/TextState;
    iget-object v0, v14, Lorg/icepdf/index/core/pobjects/graphics/TextState;->font:Lorg/icepdf/index/core/pobjects/fonts/Font;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lorg/icepdf/index/core/pobjects/fonts/Font;->getSubTypeFormat()I

    move-result v19

    iget-object v0, v14, Lorg/icepdf/index/core/pobjects/graphics/TextState;->font:Lorg/icepdf/index/core/pobjects/fonts/Font;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lorg/icepdf/index/core/pobjects/fonts/Font;->getFont()Lorg/icepdf/index/core/pobjects/fonts/FontFile;

    move-result-object v20

    move/from16 v0, v19

    move-object/from16 v1, v20

    invoke-interface {v12, v0, v1}, Lorg/icepdf/index/core/pobjects/StringObject;->getLiteralStringBuffer(ILorg/icepdf/index/core/pobjects/fonts/FontFile;)Ljava/lang/StringBuffer;

    move-result-object v19

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v0, v1, v14}, Lorg/icepdf/index/core/util/ContentParser;->getUnicodeStringFromText(Ljava/lang/StringBuffer;Lorg/icepdf/index/core/pobjects/graphics/TextState;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;
    :try_end_a
    .catch Ljava/lang/StringIndexOutOfBoundsException; {:try_start_a .. :try_end_a} :catch_4
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_1

    goto/16 :goto_3

    .line 931
    .end local v14    # "textState":Lorg/icepdf/index/core/pobjects/graphics/TextState;
    .restart local v4    # "e":Ljava/lang/StringIndexOutOfBoundsException;
    :cond_18
    :try_start_b
    throw v4

    .line 937
    .end local v4    # "e":Ljava/lang/StringIndexOutOfBoundsException;
    .end local v6    # "i":I
    .end local v12    # "stringObject":Lorg/icepdf/index/core/pobjects/StringObject;
    :cond_19
    invoke-virtual {v11, v7}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_3

    .line 943
    :cond_1a
    :goto_a
    invoke-virtual {v11}, Ljava/util/Stack;->isEmpty()Z

    move-result v19

    if-nez v19, :cond_1b

    .line 944
    invoke-virtual {v11}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    goto :goto_a

    .line 952
    :cond_1b
    invoke-virtual {v10}, Ljava/util/Stack;->clear()V

    .line 965
    .end local v7    # "nextToken":Ljava/lang/Object;
    .end local v11    # "stackText":Ljava/util/Stack;, "Ljava/util/Stack<Ljava/lang/Object;>;"
    :cond_1c
    :goto_b
    invoke-virtual {v8}, Lorg/icepdf/index/core/util/Parser;->getStreamObject()Ljava/lang/Object;

    move-result-object v16

    goto/16 :goto_0

    .line 956
    :cond_1d
    const-string/jumbo v19, "Tf"

    move-object/from16 v0, v16

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_1c

    .line 959
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/util/ContentParser;->graphicState:Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/util/ContentParser;->resources:Lorg/icepdf/index/core/pobjects/Resources;

    move-object/from16 v20, v0

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-static {v0, v10, v1}, Lorg/icepdf/index/core/util/ContentParser;->consume_Tf(Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;Ljava/util/Stack;Lorg/icepdf/index/core/pobjects/Resources;)V

    .line 960
    invoke-virtual {v10}, Ljava/util/Stack;->clear()V

    goto :goto_b

    .line 963
    :cond_1e
    move-object/from16 v0, v16

    invoke-virtual {v10, v0}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_b

    .line 968
    :cond_1f
    invoke-virtual {v10}, Ljava/util/Stack;->clear()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_1

    goto/16 :goto_4
.end method

.method public setGraphicsState(Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;)V
    .locals 0
    .param p1, "graphicState"    # Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;

    .prologue
    .line 110
    iput-object p1, p0, Lorg/icepdf/index/core/util/ContentParser;->graphicState:Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;

    .line 111
    return-void
.end method
