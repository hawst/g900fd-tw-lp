.class public Lorg/icepdf/index/core/pobjects/CrossReference$CompressedEntry;
.super Lorg/icepdf/index/core/pobjects/CrossReference$Entry;
.source "CrossReference.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/icepdf/index/core/pobjects/CrossReference;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "CompressedEntry"
.end annotation


# instance fields
.field private m_iIndexWithinObjectStream:I

.field private m_iObjectNumberOfContainingObjectStream:I


# direct methods
.method constructor <init>(III)V
    .locals 1
    .param p1, "objectNumber"    # I
    .param p2, "objectNumberOfContainingObjectStream"    # I
    .param p3, "indexWithinObjectStream"    # I

    .prologue
    .line 325
    const/4 v0, 0x2

    invoke-direct {p0, v0, p1}, Lorg/icepdf/index/core/pobjects/CrossReference$Entry;-><init>(II)V

    .line 326
    iput p2, p0, Lorg/icepdf/index/core/pobjects/CrossReference$CompressedEntry;->m_iObjectNumberOfContainingObjectStream:I

    .line 327
    iput p3, p0, Lorg/icepdf/index/core/pobjects/CrossReference$CompressedEntry;->m_iIndexWithinObjectStream:I

    .line 328
    return-void
.end method


# virtual methods
.method public getIndexWithinObjectStream()I
    .locals 1

    .prologue
    .line 335
    iget v0, p0, Lorg/icepdf/index/core/pobjects/CrossReference$CompressedEntry;->m_iIndexWithinObjectStream:I

    return v0
.end method

.method public getObjectNumberOfContainingObjectStream()I
    .locals 1

    .prologue
    .line 331
    iget v0, p0, Lorg/icepdf/index/core/pobjects/CrossReference$CompressedEntry;->m_iObjectNumberOfContainingObjectStream:I

    return v0
.end method
