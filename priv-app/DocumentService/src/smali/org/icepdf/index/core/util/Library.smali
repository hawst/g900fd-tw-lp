.class public Lorg/icepdf/index/core/util/Library;
.super Ljava/lang/Object;
.source "Library.java"


# instance fields
.field private catalog:Lorg/icepdf/index/core/pobjects/Catalog;

.field private isEncrypted:Z

.field private isLinearTraversal:Z

.field private m_LazyObjectLoader:Lorg/icepdf/index/core/util/LazyObjectLoader;

.field public memoryManager:Lorg/icepdf/index/core/util/MemoryManager;

.field private refs:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Lorg/icepdf/index/core/pobjects/Reference;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public securityManager:Lorg/icepdf/index/core/pobjects/security/SecurityManager;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 428
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    const/16 v1, 0x400

    invoke-direct {v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;-><init>(I)V

    iput-object v0, p0, Lorg/icepdf/index/core/util/Library;->refs:Ljava/util/concurrent/ConcurrentHashMap;

    .line 59
    iput-boolean v2, p0, Lorg/icepdf/index/core/util/Library;->isEncrypted:Z

    .line 60
    iput-boolean v2, p0, Lorg/icepdf/index/core/util/Library;->isLinearTraversal:Z

    .line 430
    invoke-static {}, Lorg/icepdf/index/core/util/MemoryManager;->getInstance()Lorg/icepdf/index/core/util/MemoryManager;

    move-result-object v0

    iput-object v0, p0, Lorg/icepdf/index/core/util/Library;->memoryManager:Lorg/icepdf/index/core/util/MemoryManager;

    .line 432
    return-void
.end method

.method private printObjectDebug(Ljava/lang/Object;)V
    .locals 1
    .param p1, "ob"    # Ljava/lang/Object;

    .prologue
    .line 119
    if-nez p1, :cond_1

    .line 130
    :cond_0
    :goto_0
    return-void

    .line 121
    :cond_1
    instance-of v0, p1, Lorg/icepdf/index/core/pobjects/PObject;

    if-nez v0, :cond_0

    .line 124
    instance-of v0, p1, Lorg/icepdf/index/core/pobjects/Dictionary;

    if-eqz v0, :cond_0

    goto :goto_0
.end method


# virtual methods
.method public addObject(Ljava/lang/Object;Lorg/icepdf/index/core/pobjects/Reference;)V
    .locals 1
    .param p1, "object"    # Ljava/lang/Object;
    .param p2, "objectReference"    # Lorg/icepdf/index/core/pobjects/Reference;

    .prologue
    .line 422
    iget-object v0, p0, Lorg/icepdf/index/core/util/Library;->refs:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p2, p1}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 423
    return-void
.end method

.method public dispose()V
    .locals 1

    .prologue
    .line 559
    iget-object v0, p0, Lorg/icepdf/index/core/util/Library;->memoryManager:Lorg/icepdf/index/core/util/MemoryManager;

    if-eqz v0, :cond_0

    .line 560
    iget-object v0, p0, Lorg/icepdf/index/core/util/Library;->memoryManager:Lorg/icepdf/index/core/util/MemoryManager;

    invoke-virtual {v0, p0}, Lorg/icepdf/index/core/util/MemoryManager;->releaseAllByLibrary(Lorg/icepdf/index/core/util/Library;)V

    .line 565
    :cond_0
    iget-object v0, p0, Lorg/icepdf/index/core/util/Library;->refs:Ljava/util/concurrent/ConcurrentHashMap;

    if-eqz v0, :cond_1

    .line 566
    iget-object v0, p0, Lorg/icepdf/index/core/util/Library;->refs:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->clear()V

    .line 573
    :cond_1
    iget-object v0, p0, Lorg/icepdf/index/core/util/Library;->m_LazyObjectLoader:Lorg/icepdf/index/core/util/LazyObjectLoader;

    if-eqz v0, :cond_2

    .line 574
    iget-object v0, p0, Lorg/icepdf/index/core/util/Library;->m_LazyObjectLoader:Lorg/icepdf/index/core/util/LazyObjectLoader;

    invoke-virtual {v0}, Lorg/icepdf/index/core/util/LazyObjectLoader;->dispose()V

    .line 576
    :cond_2
    return-void
.end method

.method public disposeFontResources()V
    .locals 4

    .prologue
    .line 542
    iget-object v3, p0, Lorg/icepdf/index/core/util/Library;->refs:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v3}, Ljava/util/concurrent/ConcurrentHashMap;->keys()Ljava/util/Enumeration;

    move-result-object v1

    .line 545
    .local v1, "test":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Lorg/icepdf/index/core/pobjects/Reference;>;"
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 546
    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/icepdf/index/core/pobjects/Reference;

    .line 547
    .local v0, "ref":Lorg/icepdf/index/core/pobjects/Reference;
    iget-object v3, p0, Lorg/icepdf/index/core/util/Library;->refs:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v3, v0}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .line 548
    .local v2, "tmp":Ljava/lang/Object;
    instance-of v3, v2, Lorg/icepdf/index/core/pobjects/fonts/Font;

    if-nez v3, :cond_1

    instance-of v3, v2, Lorg/icepdf/index/core/pobjects/fonts/FontDescriptor;

    if-eqz v3, :cond_0

    .line 550
    :cond_1
    iget-object v3, p0, Lorg/icepdf/index/core/util/Library;->refs:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v3, v0}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 553
    .end local v0    # "ref":Lorg/icepdf/index/core/pobjects/Reference;
    .end local v2    # "tmp":Ljava/lang/Object;
    :cond_2
    return-void
.end method

.method public getBoolean(Lorg/icepdf/index/core/util/Hashtable;Ljava/lang/String;)Ljava/lang/Boolean;
    .locals 2
    .param p1, "dictionaryEntries"    # Lorg/icepdf/index/core/util/Hashtable;
    .param p2, "key"    # Ljava/lang/String;

    .prologue
    .line 228
    invoke-virtual {p0, p1, p2}, Lorg/icepdf/index/core/util/Library;->getObject(Lorg/icepdf/index/core/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 229
    .local v0, "o":Ljava/lang/Object;
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 230
    check-cast v0, Ljava/lang/String;

    .end local v0    # "o":Ljava/lang/Object;
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v1

    .line 231
    :goto_0
    return-object v1

    .restart local v0    # "o":Ljava/lang/Object;
    :cond_0
    instance-of v1, v0, Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    check-cast v0, Ljava/lang/Boolean;

    .end local v0    # "o":Ljava/lang/Object;
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    :goto_1
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public getCatalog()Lorg/icepdf/index/core/pobjects/Catalog;
    .locals 1

    .prologue
    .line 524
    iget-object v0, p0, Lorg/icepdf/index/core/util/Library;->catalog:Lorg/icepdf/index/core/pobjects/Catalog;

    return-object v0
.end method

.method public getDictionary(Lorg/icepdf/index/core/util/Hashtable;Ljava/lang/String;)Lorg/icepdf/index/core/util/Hashtable;
    .locals 6
    .param p1, "dictionaryEntries"    # Lorg/icepdf/index/core/util/Hashtable;
    .param p2, "key"    # Ljava/lang/String;

    .prologue
    .line 310
    invoke-virtual {p0, p1, p2}, Lorg/icepdf/index/core/util/Library;->getObject(Lorg/icepdf/index/core/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    .line 311
    .local v2, "o":Ljava/lang/Object;
    instance-of v5, v2, Lorg/icepdf/index/core/util/Hashtable;

    if-eqz v5, :cond_0

    .line 312
    check-cast v2, Lorg/icepdf/index/core/util/Hashtable;

    .line 324
    .end local v2    # "o":Ljava/lang/Object;
    :goto_0
    return-object v2

    .line 313
    .restart local v2    # "o":Ljava/lang/Object;
    :cond_0
    instance-of v5, v2, Ljava/util/Vector;

    if-eqz v5, :cond_3

    move-object v4, v2

    .line 314
    check-cast v4, Ljava/util/Vector;

    .line 315
    .local v4, "v":Ljava/util/Vector;
    new-instance v1, Lorg/icepdf/index/core/util/Hashtable;

    invoke-direct {v1}, Lorg/icepdf/index/core/util/Hashtable;-><init>()V

    .line 316
    .local v1, "h1":Lorg/icepdf/index/core/util/Hashtable;
    invoke-virtual {v4}, Ljava/util/Vector;->elements()Ljava/util/Enumeration;

    move-result-object v0

    .local v0, "e":Ljava/util/Enumeration;
    :cond_1
    :goto_1
    invoke-interface {v0}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 317
    invoke-interface {v0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v3

    .line 318
    .local v3, "o1":Ljava/lang/Object;
    instance-of v5, v3, Ljava/util/Map;

    if-eqz v5, :cond_1

    .line 319
    check-cast v3, Ljava/util/Map;

    .end local v3    # "o1":Ljava/lang/Object;
    invoke-virtual {v1, v3}, Lorg/icepdf/index/core/util/Hashtable;->putAll(Ljava/util/Map;)V

    goto :goto_1

    :cond_2
    move-object v2, v1

    .line 322
    goto :goto_0

    .line 324
    .end local v0    # "e":Ljava/util/Enumeration;
    .end local v1    # "h1":Lorg/icepdf/index/core/util/Hashtable;
    .end local v4    # "v":Ljava/util/Vector;
    :cond_3
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public getFloat(Lorg/icepdf/index/core/util/Hashtable;Ljava/lang/String;)F
    .locals 2
    .param p1, "dictionaryEntries"    # Lorg/icepdf/index/core/util/Hashtable;
    .param p2, "key"    # Ljava/lang/String;

    .prologue
    .line 245
    invoke-virtual {p0, p1, p2}, Lorg/icepdf/index/core/util/Library;->getNumber(Lorg/icepdf/index/core/util/Hashtable;Ljava/lang/String;)Ljava/lang/Number;

    move-result-object v0

    .line 246
    .local v0, "n":Ljava/lang/Number;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Number;->floatValue()F

    move-result v1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getInt(Lorg/icepdf/index/core/util/Hashtable;Ljava/lang/String;)I
    .locals 2
    .param p1, "dictionaryEntries"    # Lorg/icepdf/index/core/util/Hashtable;
    .param p2, "key"    # Ljava/lang/String;

    .prologue
    .line 260
    invoke-virtual {p0, p1, p2}, Lorg/icepdf/index/core/util/Library;->getNumber(Lorg/icepdf/index/core/util/Hashtable;Ljava/lang/String;)Ljava/lang/Number;

    move-result-object v0

    .line 261
    .local v0, "n":Ljava/lang/Number;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getLong(Lorg/icepdf/index/core/util/Hashtable;Ljava/lang/String;)J
    .locals 4
    .param p1, "dictionaryEntries"    # Lorg/icepdf/index/core/util/Hashtable;
    .param p2, "key"    # Ljava/lang/String;

    .prologue
    .line 275
    invoke-virtual {p0, p1, p2}, Lorg/icepdf/index/core/util/Library;->getNumber(Lorg/icepdf/index/core/util/Hashtable;Ljava/lang/String;)Ljava/lang/Number;

    move-result-object v0

    .line 276
    .local v0, "n":Ljava/lang/Number;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Number;->longValue()J

    move-result-wide v2

    :goto_0
    return-wide v2

    :cond_0
    const-wide/16 v2, 0x0

    goto :goto_0
.end method

.method public getName(Lorg/icepdf/index/core/util/Hashtable;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "dictionaryEntries"    # Lorg/icepdf/index/core/util/Hashtable;
    .param p2, "key"    # Ljava/lang/String;

    .prologue
    .line 290
    invoke-virtual {p0, p1, p2}, Lorg/icepdf/index/core/util/Library;->getObject(Lorg/icepdf/index/core/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 291
    .local v0, "o":Ljava/lang/Object;
    if-eqz v0, :cond_0

    .line 292
    instance-of v1, v0, Lorg/icepdf/index/core/pobjects/Name;

    if-eqz v1, :cond_0

    .line 293
    check-cast v0, Lorg/icepdf/index/core/pobjects/Name;

    .end local v0    # "o":Ljava/lang/Object;
    invoke-virtual {v0}, Lorg/icepdf/index/core/pobjects/Name;->getName()Ljava/lang/String;

    move-result-object v1

    .line 296
    :goto_0
    return-object v1

    .restart local v0    # "o":Ljava/lang/Object;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getNumber(Lorg/icepdf/index/core/util/Hashtable;Ljava/lang/String;)Ljava/lang/Number;
    .locals 2
    .param p1, "dictionaryEntries"    # Lorg/icepdf/index/core/util/Hashtable;
    .param p2, "key"    # Ljava/lang/String;

    .prologue
    .line 211
    invoke-virtual {p0, p1, p2}, Lorg/icepdf/index/core/util/Library;->getObject(Lorg/icepdf/index/core/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 212
    .local v0, "o":Ljava/lang/Object;
    instance-of v1, v0, Ljava/lang/Number;

    if-eqz v1, :cond_0

    .line 213
    check-cast v0, Ljava/lang/Number;

    .line 214
    .end local v0    # "o":Ljava/lang/Object;
    :goto_0
    return-object v0

    .restart local v0    # "o":Ljava/lang/Object;
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getObject(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "referenceObject"    # Ljava/lang/Object;

    .prologue
    .line 164
    instance-of v0, p1, Lorg/icepdf/index/core/pobjects/Reference;

    if-eqz v0, :cond_0

    .line 165
    check-cast p1, Lorg/icepdf/index/core/pobjects/Reference;

    .end local p1    # "referenceObject":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lorg/icepdf/index/core/util/Library;->getObject(Lorg/icepdf/index/core/pobjects/Reference;)Ljava/lang/Object;

    move-result-object p1

    .line 167
    :cond_0
    return-object p1
.end method

.method public getObject(Lorg/icepdf/index/core/pobjects/Reference;)Ljava/lang/Object;
    .locals 3
    .param p1, "reference"    # Lorg/icepdf/index/core/pobjects/Reference;

    .prologue
    .line 95
    :goto_0
    iget-object v2, p0, Lorg/icepdf/index/core/util/Library;->refs:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v2, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 96
    .local v1, "ob":Ljava/lang/Object;
    if-nez v1, :cond_0

    iget-object v2, p0, Lorg/icepdf/index/core/util/Library;->m_LazyObjectLoader:Lorg/icepdf/index/core/util/LazyObjectLoader;

    if-eqz v2, :cond_0

    .line 97
    iget-object v2, p0, Lorg/icepdf/index/core/util/Library;->m_LazyObjectLoader:Lorg/icepdf/index/core/util/LazyObjectLoader;

    invoke-virtual {v2, p1}, Lorg/icepdf/index/core/util/LazyObjectLoader;->loadObject(Lorg/icepdf/index/core/pobjects/Reference;)Z

    move-result v0

    .line 98
    .local v0, "gotSomething":Z
    if-eqz v0, :cond_0

    .line 99
    iget-object v2, p0, Lorg/icepdf/index/core/util/Library;->refs:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v2, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 102
    .end local v0    # "gotSomething":Z
    :cond_0
    invoke-direct {p0, v1}, Lorg/icepdf/index/core/util/Library;->printObjectDebug(Ljava/lang/Object;)V

    .line 103
    if-nez v1, :cond_2

    .line 109
    :cond_1
    return-object v1

    .line 105
    :cond_2
    instance-of v2, v1, Lorg/icepdf/index/core/pobjects/Reference;

    if-eqz v2, :cond_1

    move-object p1, v1

    .line 107
    check-cast p1, Lorg/icepdf/index/core/pobjects/Reference;

    goto :goto_0
.end method

.method public getObject(Lorg/icepdf/index/core/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;
    .locals 2
    .param p1, "dictionaryEntries"    # Lorg/icepdf/index/core/util/Hashtable;
    .param p2, "key"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 144
    if-nez p1, :cond_1

    move-object v0, v1

    .line 152
    :cond_0
    :goto_0
    return-object v0

    .line 147
    :cond_1
    invoke-virtual {p1, p2}, Lorg/icepdf/index/core/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 148
    .local v0, "o":Ljava/lang/Object;
    if-nez v0, :cond_2

    move-object v0, v1

    .line 149
    goto :goto_0

    .line 150
    :cond_2
    instance-of v1, v0, Lorg/icepdf/index/core/pobjects/Reference;

    if-eqz v1, :cond_0

    .line 151
    check-cast v0, Lorg/icepdf/index/core/pobjects/Reference;

    .end local v0    # "o":Ljava/lang/Object;
    invoke-virtual {p0, v0}, Lorg/icepdf/index/core/util/Library;->getObject(Lorg/icepdf/index/core/pobjects/Reference;)Ljava/lang/Object;

    move-result-object v0

    .restart local v0    # "o":Ljava/lang/Object;
    goto :goto_0
.end method

.method public getObjectReference(Lorg/icepdf/index/core/util/Hashtable;Ljava/lang/String;)Lorg/icepdf/index/core/pobjects/Reference;
    .locals 3
    .param p1, "dictionaryEntries"    # Lorg/icepdf/index/core/util/Hashtable;
    .param p2, "key"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 446
    if-nez p1, :cond_1

    .line 457
    :cond_0
    return-object v0

    .line 449
    :cond_1
    invoke-virtual {p1, p2}, Lorg/icepdf/index/core/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 450
    .local v1, "o":Ljava/lang/Object;
    if-eqz v1, :cond_0

    .line 452
    const/4 v0, 0x0

    .line 453
    .local v0, "currentRef":Lorg/icepdf/index/core/pobjects/Reference;
    :goto_0
    if-eqz v1, :cond_0

    instance-of v2, v1, Lorg/icepdf/index/core/pobjects/Reference;

    if-eqz v2, :cond_0

    move-object v0, v1

    .line 454
    check-cast v0, Lorg/icepdf/index/core/pobjects/Reference;

    .line 455
    invoke-virtual {p0, v0}, Lorg/icepdf/index/core/util/Library;->getObject(Lorg/icepdf/index/core/pobjects/Reference;)Ljava/lang/Object;

    move-result-object v1

    goto :goto_0
.end method

.method public getRectangle(Lorg/icepdf/index/core/util/Hashtable;Ljava/lang/String;)Lorg/icepdf/index/core/pobjects/Rectangle2Df;
    .locals 2
    .param p1, "dictionaryEntries"    # Lorg/icepdf/index/core/util/Hashtable;
    .param p2, "key"    # Ljava/lang/String;

    .prologue
    .line 336
    invoke-virtual {p0, p1, p2}, Lorg/icepdf/index/core/util/Library;->getObject(Lorg/icepdf/index/core/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Vector;

    .line 337
    .local v0, "v":Ljava/util/Vector;
    if-eqz v0, :cond_0

    .line 339
    new-instance v1, Lorg/icepdf/index/core/pobjects/PRectangle;

    invoke-direct {v1, v0}, Lorg/icepdf/index/core/pobjects/PRectangle;-><init>(Ljava/util/Vector;)V

    invoke-virtual {v1}, Lorg/icepdf/index/core/pobjects/PRectangle;->toJava2dCoordinates()Lorg/icepdf/index/core/pobjects/Rectangle2Df;

    move-result-object v1

    .line 341
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getResources(Lorg/icepdf/index/core/pobjects/Reference;)Lorg/icepdf/index/core/pobjects/Resources;
    .locals 5
    .param p1, "reference"    # Lorg/icepdf/index/core/pobjects/Reference;

    .prologue
    const/4 v3, 0x0

    .line 390
    :goto_0
    iget-object v4, p0, Lorg/icepdf/index/core/util/Library;->refs:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v4, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .line 391
    .local v2, "ob":Ljava/lang/Object;
    if-nez v2, :cond_0

    iget-object v4, p0, Lorg/icepdf/index/core/util/Library;->m_LazyObjectLoader:Lorg/icepdf/index/core/util/LazyObjectLoader;

    if-eqz v4, :cond_0

    .line 392
    iget-object v4, p0, Lorg/icepdf/index/core/util/Library;->m_LazyObjectLoader:Lorg/icepdf/index/core/util/LazyObjectLoader;

    invoke-virtual {v4, p1}, Lorg/icepdf/index/core/util/LazyObjectLoader;->loadObject(Lorg/icepdf/index/core/pobjects/Reference;)Z

    move-result v0

    .line 393
    .local v0, "gotSomething":Z
    if-eqz v0, :cond_0

    .line 394
    iget-object v4, p0, Lorg/icepdf/index/core/util/Library;->refs:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v4, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .line 397
    .end local v0    # "gotSomething":Z
    :cond_0
    if-nez v2, :cond_1

    move-object v2, v3

    .line 412
    .end local v2    # "ob":Ljava/lang/Object;
    :goto_1
    return-object v2

    .line 399
    .restart local v2    # "ob":Ljava/lang/Object;
    :cond_1
    instance-of v4, v2, Lorg/icepdf/index/core/pobjects/Resources;

    if-eqz v4, :cond_2

    .line 400
    check-cast v2, Lorg/icepdf/index/core/pobjects/Resources;

    goto :goto_1

    .line 401
    :cond_2
    instance-of v4, v2, Lorg/icepdf/index/core/pobjects/Reference;

    if-eqz v4, :cond_3

    move-object p1, v2

    .line 402
    check-cast p1, Lorg/icepdf/index/core/pobjects/Reference;

    .line 403
    goto :goto_0

    .line 404
    :cond_3
    instance-of v4, v2, Lorg/icepdf/index/core/util/Hashtable;

    if-eqz v4, :cond_4

    move-object v1, v2

    .line 405
    check-cast v1, Lorg/icepdf/index/core/util/Hashtable;

    .line 406
    .local v1, "ht":Lorg/icepdf/index/core/util/Hashtable;
    new-instance v3, Lorg/icepdf/index/core/pobjects/Resources;

    invoke-direct {v3, p0, v1}, Lorg/icepdf/index/core/pobjects/Resources;-><init>(Lorg/icepdf/index/core/util/Library;Lorg/icepdf/index/core/util/Hashtable;)V

    .line 407
    .local v3, "resources":Lorg/icepdf/index/core/pobjects/Resources;
    invoke-virtual {p0, v3, p1}, Lorg/icepdf/index/core/util/Library;->addObject(Ljava/lang/Object;Lorg/icepdf/index/core/pobjects/Reference;)V

    move-object v2, v3

    .line 408
    goto :goto_1

    .end local v1    # "ht":Lorg/icepdf/index/core/util/Hashtable;
    .end local v3    # "resources":Lorg/icepdf/index/core/pobjects/Resources;
    :cond_4
    move-object v2, v3

    .line 412
    goto :goto_1
.end method

.method public getResources(Lorg/icepdf/index/core/util/Hashtable;Ljava/lang/String;)Lorg/icepdf/index/core/pobjects/Resources;
    .locals 5
    .param p1, "dictionaryEntries"    # Lorg/icepdf/index/core/util/Hashtable;
    .param p2, "key"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 369
    if-nez p1, :cond_0

    move-object v1, v3

    .line 385
    :goto_0
    return-object v1

    .line 371
    :cond_0
    invoke-virtual {p1, p2}, Lorg/icepdf/index/core/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 372
    .local v1, "ob":Ljava/lang/Object;
    if-nez v1, :cond_1

    move-object v1, v3

    .line 373
    goto :goto_0

    .line 374
    :cond_1
    instance-of v4, v1, Lorg/icepdf/index/core/pobjects/Resources;

    if-eqz v4, :cond_2

    .line 375
    check-cast v1, Lorg/icepdf/index/core/pobjects/Resources;

    goto :goto_0

    .line 376
    :cond_2
    instance-of v4, v1, Lorg/icepdf/index/core/pobjects/Reference;

    if-eqz v4, :cond_3

    move-object v2, v1

    .line 377
    check-cast v2, Lorg/icepdf/index/core/pobjects/Reference;

    .line 378
    .local v2, "reference":Lorg/icepdf/index/core/pobjects/Reference;
    invoke-virtual {p0, v2}, Lorg/icepdf/index/core/util/Library;->getResources(Lorg/icepdf/index/core/pobjects/Reference;)Lorg/icepdf/index/core/pobjects/Resources;

    move-result-object v1

    goto :goto_0

    .line 379
    .end local v2    # "reference":Lorg/icepdf/index/core/pobjects/Reference;
    :cond_3
    instance-of v4, v1, Lorg/icepdf/index/core/util/Hashtable;

    if-eqz v4, :cond_4

    move-object v0, v1

    .line 380
    check-cast v0, Lorg/icepdf/index/core/util/Hashtable;

    .line 381
    .local v0, "ht":Lorg/icepdf/index/core/util/Hashtable;
    new-instance v3, Lorg/icepdf/index/core/pobjects/Resources;

    invoke-direct {v3, p0, v0}, Lorg/icepdf/index/core/pobjects/Resources;-><init>(Lorg/icepdf/index/core/util/Library;Lorg/icepdf/index/core/util/Hashtable;)V

    .line 382
    .local v3, "resources":Lorg/icepdf/index/core/pobjects/Resources;
    invoke-virtual {p1, p2, v3}, Lorg/icepdf/index/core/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v1, v3

    .line 383
    goto :goto_0

    .end local v0    # "ht":Lorg/icepdf/index/core/util/Hashtable;
    .end local v3    # "resources":Lorg/icepdf/index/core/pobjects/Resources;
    :cond_4
    move-object v1, v3

    .line 385
    goto :goto_0
.end method

.method public getSecurityManager()Lorg/icepdf/index/core/pobjects/security/SecurityManager;
    .locals 1

    .prologue
    .line 476
    iget-object v0, p0, Lorg/icepdf/index/core/util/Library;->securityManager:Lorg/icepdf/index/core/pobjects/security/SecurityManager;

    return-object v0
.end method

.method public getTrailerByFilePosition(J)Lorg/icepdf/index/core/pobjects/PTrailer;
    .locals 1
    .param p1, "position"    # J

    .prologue
    .line 80
    iget-object v0, p0, Lorg/icepdf/index/core/util/Library;->m_LazyObjectLoader:Lorg/icepdf/index/core/util/LazyObjectLoader;

    if-nez v0, :cond_0

    .line 81
    const/4 v0, 0x0

    .line 82
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lorg/icepdf/index/core/util/Library;->m_LazyObjectLoader:Lorg/icepdf/index/core/util/LazyObjectLoader;

    invoke-virtual {v0, p1, p2}, Lorg/icepdf/index/core/util/LazyObjectLoader;->loadTrailer(J)Lorg/icepdf/index/core/pobjects/PTrailer;

    move-result-object v0

    goto :goto_0
.end method

.method public isEncrypted()Z
    .locals 1

    .prologue
    .line 466
    iget-boolean v0, p0, Lorg/icepdf/index/core/util/Library;->isEncrypted:Z

    return v0
.end method

.method public isLinearTraversal()Z
    .locals 1

    .prologue
    .line 506
    iget-boolean v0, p0, Lorg/icepdf/index/core/util/Library;->isLinearTraversal:Z

    return v0
.end method

.method public isValidEntry(Lorg/icepdf/index/core/pobjects/Reference;)Z
    .locals 2
    .param p1, "reference"    # Lorg/icepdf/index/core/pobjects/Reference;

    .prologue
    .line 194
    iget-object v1, p0, Lorg/icepdf/index/core/util/Library;->refs:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 195
    .local v0, "ob":Ljava/lang/Object;
    if-nez v0, :cond_0

    iget-object v1, p0, Lorg/icepdf/index/core/util/Library;->m_LazyObjectLoader:Lorg/icepdf/index/core/util/LazyObjectLoader;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lorg/icepdf/index/core/util/Library;->m_LazyObjectLoader:Lorg/icepdf/index/core/util/LazyObjectLoader;

    invoke-virtual {v1, p1}, Lorg/icepdf/index/core/util/LazyObjectLoader;->haveEntry(Lorg/icepdf/index/core/pobjects/Reference;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isValidEntry(Lorg/icepdf/index/core/util/Hashtable;Ljava/lang/String;)Z
    .locals 3
    .param p1, "dictionaryEntries"    # Lorg/icepdf/index/core/util/Hashtable;
    .param p2, "key"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 180
    if-nez p1, :cond_1

    .line 184
    :cond_0
    :goto_0
    return v1

    .line 183
    :cond_1
    invoke-virtual {p1, p2}, Lorg/icepdf/index/core/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 184
    .local v0, "o":Ljava/lang/Object;
    if-eqz v0, :cond_0

    instance-of v2, v0, Lorg/icepdf/index/core/pobjects/Reference;

    if-eqz v2, :cond_2

    check-cast v0, Lorg/icepdf/index/core/pobjects/Reference;

    .end local v0    # "o":Ljava/lang/Object;
    invoke-virtual {p0, v0}, Lorg/icepdf/index/core/util/Library;->isValidEntry(Lorg/icepdf/index/core/pobjects/Reference;)Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_2
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public setCatalog(Lorg/icepdf/index/core/pobjects/Catalog;)V
    .locals 0
    .param p1, "c"    # Lorg/icepdf/index/core/pobjects/Catalog;

    .prologue
    .line 533
    iput-object p1, p0, Lorg/icepdf/index/core/util/Library;->catalog:Lorg/icepdf/index/core/pobjects/Catalog;

    .line 534
    return-void
.end method

.method public setEncrypted(Z)V
    .locals 0
    .param p1, "flag"    # Z

    .prologue
    .line 485
    iput-boolean p1, p0, Lorg/icepdf/index/core/util/Library;->isEncrypted:Z

    .line 486
    return-void
.end method

.method public setLazyObjectLoader(Lorg/icepdf/index/core/util/LazyObjectLoader;)V
    .locals 2
    .param p1, "lol"    # Lorg/icepdf/index/core/util/LazyObjectLoader;

    .prologue
    .line 68
    iput-object p1, p0, Lorg/icepdf/index/core/util/Library;->m_LazyObjectLoader:Lorg/icepdf/index/core/util/LazyObjectLoader;

    .line 69
    iget-object v0, p0, Lorg/icepdf/index/core/util/Library;->m_LazyObjectLoader:Lorg/icepdf/index/core/util/LazyObjectLoader;

    if-eqz v0, :cond_0

    .line 70
    iget-object v0, p0, Lorg/icepdf/index/core/util/Library;->memoryManager:Lorg/icepdf/index/core/util/MemoryManager;

    iget-object v1, p0, Lorg/icepdf/index/core/util/Library;->m_LazyObjectLoader:Lorg/icepdf/index/core/util/LazyObjectLoader;

    invoke-virtual {v0, v1}, Lorg/icepdf/index/core/util/MemoryManager;->registerMemoryManagerDelegate(Lorg/icepdf/index/core/util/MemoryManagerDelegate;)V

    .line 71
    :cond_0
    return-void
.end method

.method public setLinearTraversal()V
    .locals 1

    .prologue
    .line 495
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/icepdf/index/core/util/Library;->isLinearTraversal:Z

    .line 496
    return-void
.end method
