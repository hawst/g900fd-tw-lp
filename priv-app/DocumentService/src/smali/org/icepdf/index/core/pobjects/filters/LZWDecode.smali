.class public Lorg/icepdf/index/core/pobjects/filters/LZWDecode;
.super Lorg/icepdf/index/core/pobjects/filters/ChunkingInputStream;
.source "LZWDecode.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/icepdf/index/core/pobjects/filters/LZWDecode$Code;
    }
.end annotation


# instance fields
.field private code:I

.field private code_len:I

.field private codes:[Lorg/icepdf/index/core/pobjects/filters/LZWDecode$Code;

.field private firstTime:Z

.field private inb:Lorg/icepdf/index/core/io/BitStream;

.field private last_code:I

.field private old_code:I


# direct methods
.method public constructor <init>(Lorg/icepdf/index/core/io/BitStream;)V
    .locals 1
    .param p1, "inb"    # Lorg/icepdf/index/core/io/BitStream;

    .prologue
    const/4 v0, 0x0

    .line 37
    invoke-direct {p0}, Lorg/icepdf/index/core/pobjects/filters/ChunkingInputStream;-><init>()V

    .line 38
    iput-object p1, p0, Lorg/icepdf/index/core/pobjects/filters/LZWDecode;->inb:Lorg/icepdf/index/core/io/BitStream;

    .line 40
    iput v0, p0, Lorg/icepdf/index/core/pobjects/filters/LZWDecode;->code:I

    .line 41
    iput v0, p0, Lorg/icepdf/index/core/pobjects/filters/LZWDecode;->old_code:I

    .line 42
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/icepdf/index/core/pobjects/filters/LZWDecode;->firstTime:Z

    .line 43
    invoke-direct {p0}, Lorg/icepdf/index/core/pobjects/filters/LZWDecode;->initCodeTable()V

    .line 44
    const/16 v0, 0x1000

    invoke-virtual {p0, v0}, Lorg/icepdf/index/core/pobjects/filters/LZWDecode;->setBufferSize(I)V

    .line 45
    return-void
.end method

.method private addToBuffer(BI)V
    .locals 4
    .param p1, "b"    # B
    .param p2, "offset"    # I

    .prologue
    const/4 v3, 0x0

    .line 126
    iget-object v1, p0, Lorg/icepdf/index/core/pobjects/filters/LZWDecode;->buffer:[B

    array-length v1, v1

    if-lt p2, v1, :cond_0

    .line 127
    iget-object v1, p0, Lorg/icepdf/index/core/pobjects/filters/LZWDecode;->buffer:[B

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x2

    new-array v0, v1, [B

    .line 128
    .local v0, "bufferNew":[B
    iget-object v1, p0, Lorg/icepdf/index/core/pobjects/filters/LZWDecode;->buffer:[B

    iget-object v2, p0, Lorg/icepdf/index/core/pobjects/filters/LZWDecode;->buffer:[B

    array-length v2, v2

    invoke-static {v1, v3, v0, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 129
    iput-object v0, p0, Lorg/icepdf/index/core/pobjects/filters/LZWDecode;->buffer:[B

    .line 131
    .end local v0    # "bufferNew":[B
    :cond_0
    iget-object v1, p0, Lorg/icepdf/index/core/pobjects/filters/LZWDecode;->buffer:[B

    aput-byte p1, v1, p2

    .line 132
    return-void
.end method

.method private initCodeTable()V
    .locals 5

    .prologue
    .line 118
    const/16 v1, 0x9

    iput v1, p0, Lorg/icepdf/index/core/pobjects/filters/LZWDecode;->code_len:I

    .line 119
    const/16 v1, 0x101

    iput v1, p0, Lorg/icepdf/index/core/pobjects/filters/LZWDecode;->last_code:I

    .line 120
    const/16 v1, 0x1000

    new-array v1, v1, [Lorg/icepdf/index/core/pobjects/filters/LZWDecode$Code;

    iput-object v1, p0, Lorg/icepdf/index/core/pobjects/filters/LZWDecode;->codes:[Lorg/icepdf/index/core/pobjects/filters/LZWDecode$Code;

    .line 121
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/16 v1, 0x100

    if-ge v0, v1, :cond_0

    .line 122
    iget-object v1, p0, Lorg/icepdf/index/core/pobjects/filters/LZWDecode;->codes:[Lorg/icepdf/index/core/pobjects/filters/LZWDecode$Code;

    new-instance v2, Lorg/icepdf/index/core/pobjects/filters/LZWDecode$Code;

    const/4 v3, 0x0

    int-to-byte v4, v0

    invoke-direct {v2, v3, v4}, Lorg/icepdf/index/core/pobjects/filters/LZWDecode$Code;-><init>(Lorg/icepdf/index/core/pobjects/filters/LZWDecode$Code;B)V

    aput-object v2, v1, v0

    .line 121
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 123
    :cond_0
    return-void
.end method


# virtual methods
.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 136
    invoke-super {p0}, Lorg/icepdf/index/core/pobjects/filters/ChunkingInputStream;->close()V

    .line 138
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/filters/LZWDecode;->inb:Lorg/icepdf/index/core/io/BitStream;

    if-eqz v0, :cond_0

    .line 139
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/filters/LZWDecode;->inb:Lorg/icepdf/index/core/io/BitStream;

    invoke-virtual {v0}, Lorg/icepdf/index/core/io/BitStream;->close()V

    .line 140
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/icepdf/index/core/pobjects/filters/LZWDecode;->inb:Lorg/icepdf/index/core/io/BitStream;

    .line 142
    :cond_0
    return-void
.end method

.method protected fillInternalBuffer()I
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 48
    const/4 v2, 0x0

    .line 52
    .local v2, "numRead":I
    iget-boolean v4, p0, Lorg/icepdf/index/core/pobjects/filters/LZWDecode;->firstTime:Z

    if-eqz v4, :cond_3

    .line 53
    const/4 v4, 0x0

    iput-boolean v4, p0, Lorg/icepdf/index/core/pobjects/filters/LZWDecode;->firstTime:Z

    .line 54
    iget-object v4, p0, Lorg/icepdf/index/core/pobjects/filters/LZWDecode;->inb:Lorg/icepdf/index/core/io/BitStream;

    iget v5, p0, Lorg/icepdf/index/core/pobjects/filters/LZWDecode;->code_len:I

    invoke-virtual {v4, v5}, Lorg/icepdf/index/core/io/BitStream;->getBits(I)I

    move-result v4

    iput v4, p0, Lorg/icepdf/index/core/pobjects/filters/LZWDecode;->code:I

    iput v4, p0, Lorg/icepdf/index/core/pobjects/filters/LZWDecode;->old_code:I

    .line 59
    :cond_0
    iget v4, p0, Lorg/icepdf/index/core/pobjects/filters/LZWDecode;->code:I

    const/16 v5, 0x100

    if-ne v4, v5, :cond_4

    .line 60
    invoke-direct {p0}, Lorg/icepdf/index/core/pobjects/filters/LZWDecode;->initCodeTable()V

    .line 103
    :goto_0
    iget v4, p0, Lorg/icepdf/index/core/pobjects/filters/LZWDecode;->code_len:I

    const/16 v5, 0xc

    if-ge v4, v5, :cond_1

    iget v4, p0, Lorg/icepdf/index/core/pobjects/filters/LZWDecode;->last_code:I

    const/4 v5, 0x1

    iget v6, p0, Lorg/icepdf/index/core/pobjects/filters/LZWDecode;->code_len:I

    shl-int/2addr v5, v6

    add-int/lit8 v5, v5, -0x1

    if-ne v4, v5, :cond_1

    .line 105
    iget v4, p0, Lorg/icepdf/index/core/pobjects/filters/LZWDecode;->code_len:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lorg/icepdf/index/core/pobjects/filters/LZWDecode;->code_len:I

    .line 107
    :cond_1
    iget v4, p0, Lorg/icepdf/index/core/pobjects/filters/LZWDecode;->code:I

    iput v4, p0, Lorg/icepdf/index/core/pobjects/filters/LZWDecode;->old_code:I

    .line 108
    iget-object v4, p0, Lorg/icepdf/index/core/pobjects/filters/LZWDecode;->inb:Lorg/icepdf/index/core/io/BitStream;

    iget v5, p0, Lorg/icepdf/index/core/pobjects/filters/LZWDecode;->code_len:I

    invoke-virtual {v4, v5}, Lorg/icepdf/index/core/io/BitStream;->getBits(I)I

    move-result v4

    iput v4, p0, Lorg/icepdf/index/core/pobjects/filters/LZWDecode;->code:I

    .line 110
    iget-object v4, p0, Lorg/icepdf/index/core/pobjects/filters/LZWDecode;->inb:Lorg/icepdf/index/core/io/BitStream;

    invoke-virtual {v4}, Lorg/icepdf/index/core/io/BitStream;->atEndOfFile()Z

    move-result v4

    if-eqz v4, :cond_9

    :cond_2
    :goto_1
    move v4, v2

    .line 114
    :goto_2
    return v4

    .line 55
    :cond_3
    iget-object v4, p0, Lorg/icepdf/index/core/pobjects/filters/LZWDecode;->inb:Lorg/icepdf/index/core/io/BitStream;

    invoke-virtual {v4}, Lorg/icepdf/index/core/io/BitStream;->atEndOfFile()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 56
    const/4 v4, -0x1

    goto :goto_2

    .line 61
    :cond_4
    iget v4, p0, Lorg/icepdf/index/core/pobjects/filters/LZWDecode;->code:I

    const/16 v5, 0x101

    if-eq v4, v5, :cond_2

    .line 64
    iget-object v4, p0, Lorg/icepdf/index/core/pobjects/filters/LZWDecode;->codes:[Lorg/icepdf/index/core/pobjects/filters/LZWDecode$Code;

    iget v5, p0, Lorg/icepdf/index/core/pobjects/filters/LZWDecode;->code:I

    aget-object v4, v4, v5

    if-eqz v4, :cond_6

    .line 65
    new-instance v3, Ljava/util/Stack;

    invoke-direct {v3}, Ljava/util/Stack;-><init>()V

    .line 66
    .local v3, "stack":Ljava/util/Stack;
    iget-object v4, p0, Lorg/icepdf/index/core/pobjects/filters/LZWDecode;->codes:[Lorg/icepdf/index/core/pobjects/filters/LZWDecode$Code;

    iget v5, p0, Lorg/icepdf/index/core/pobjects/filters/LZWDecode;->code:I

    aget-object v4, v4, v5

    invoke-virtual {v4, v3}, Lorg/icepdf/index/core/pobjects/filters/LZWDecode$Code;->getString(Ljava/util/Stack;)V

    .line 67
    invoke-virtual {v3}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/icepdf/index/core/pobjects/filters/LZWDecode$Code;

    .line 68
    .local v0, "c":Lorg/icepdf/index/core/pobjects/filters/LZWDecode$Code;
    iget-byte v4, v0, Lorg/icepdf/index/core/pobjects/filters/LZWDecode$Code;->c:B

    invoke-direct {p0, v4, v2}, Lorg/icepdf/index/core/pobjects/filters/LZWDecode;->addToBuffer(BI)V

    .line 69
    add-int/lit8 v2, v2, 0x1

    .line 71
    iget-byte v1, v0, Lorg/icepdf/index/core/pobjects/filters/LZWDecode$Code;->c:B

    .line 72
    .local v1, "first":B
    :goto_3
    invoke-virtual {v3}, Ljava/util/Stack;->empty()Z

    move-result v4

    if-nez v4, :cond_5

    .line 73
    invoke-virtual {v3}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "c":Lorg/icepdf/index/core/pobjects/filters/LZWDecode$Code;
    check-cast v0, Lorg/icepdf/index/core/pobjects/filters/LZWDecode$Code;

    .line 74
    .restart local v0    # "c":Lorg/icepdf/index/core/pobjects/filters/LZWDecode$Code;
    iget-byte v4, v0, Lorg/icepdf/index/core/pobjects/filters/LZWDecode$Code;->c:B

    invoke-direct {p0, v4, v2}, Lorg/icepdf/index/core/pobjects/filters/LZWDecode;->addToBuffer(BI)V

    .line 75
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 79
    :cond_5
    iget-object v4, p0, Lorg/icepdf/index/core/pobjects/filters/LZWDecode;->codes:[Lorg/icepdf/index/core/pobjects/filters/LZWDecode$Code;

    iget v5, p0, Lorg/icepdf/index/core/pobjects/filters/LZWDecode;->last_code:I

    add-int/lit8 v6, v5, 0x1

    iput v6, p0, Lorg/icepdf/index/core/pobjects/filters/LZWDecode;->last_code:I

    new-instance v6, Lorg/icepdf/index/core/pobjects/filters/LZWDecode$Code;

    iget-object v7, p0, Lorg/icepdf/index/core/pobjects/filters/LZWDecode;->codes:[Lorg/icepdf/index/core/pobjects/filters/LZWDecode$Code;

    iget v8, p0, Lorg/icepdf/index/core/pobjects/filters/LZWDecode;->old_code:I

    aget-object v7, v7, v8

    invoke-direct {v6, v7, v1}, Lorg/icepdf/index/core/pobjects/filters/LZWDecode$Code;-><init>(Lorg/icepdf/index/core/pobjects/filters/LZWDecode$Code;B)V

    aput-object v6, v4, v5

    goto/16 :goto_0

    .line 82
    .end local v0    # "c":Lorg/icepdf/index/core/pobjects/filters/LZWDecode$Code;
    .end local v1    # "first":B
    .end local v3    # "stack":Ljava/util/Stack;
    :cond_6
    iget v4, p0, Lorg/icepdf/index/core/pobjects/filters/LZWDecode;->code:I

    iget v5, p0, Lorg/icepdf/index/core/pobjects/filters/LZWDecode;->last_code:I

    if-eq v4, v5, :cond_7

    .line 83
    new-instance v4, Ljava/lang/RuntimeException;

    const-string/jumbo v5, "LZWDecode failure"

    invoke-direct {v4, v5}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 84
    :cond_7
    new-instance v3, Ljava/util/Stack;

    invoke-direct {v3}, Ljava/util/Stack;-><init>()V

    .line 85
    .restart local v3    # "stack":Ljava/util/Stack;
    iget-object v4, p0, Lorg/icepdf/index/core/pobjects/filters/LZWDecode;->codes:[Lorg/icepdf/index/core/pobjects/filters/LZWDecode$Code;

    iget v5, p0, Lorg/icepdf/index/core/pobjects/filters/LZWDecode;->old_code:I

    aget-object v4, v4, v5

    invoke-virtual {v4, v3}, Lorg/icepdf/index/core/pobjects/filters/LZWDecode$Code;->getString(Ljava/util/Stack;)V

    .line 86
    invoke-virtual {v3}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/icepdf/index/core/pobjects/filters/LZWDecode$Code;

    .line 87
    .restart local v0    # "c":Lorg/icepdf/index/core/pobjects/filters/LZWDecode$Code;
    iget-byte v4, v0, Lorg/icepdf/index/core/pobjects/filters/LZWDecode$Code;->c:B

    invoke-direct {p0, v4, v2}, Lorg/icepdf/index/core/pobjects/filters/LZWDecode;->addToBuffer(BI)V

    .line 88
    add-int/lit8 v2, v2, 0x1

    .line 90
    iget-byte v1, v0, Lorg/icepdf/index/core/pobjects/filters/LZWDecode$Code;->c:B

    .line 91
    .restart local v1    # "first":B
    :goto_4
    invoke-virtual {v3}, Ljava/util/Stack;->empty()Z

    move-result v4

    if-nez v4, :cond_8

    .line 92
    invoke-virtual {v3}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "c":Lorg/icepdf/index/core/pobjects/filters/LZWDecode$Code;
    check-cast v0, Lorg/icepdf/index/core/pobjects/filters/LZWDecode$Code;

    .line 93
    .restart local v0    # "c":Lorg/icepdf/index/core/pobjects/filters/LZWDecode$Code;
    iget-byte v4, v0, Lorg/icepdf/index/core/pobjects/filters/LZWDecode$Code;->c:B

    invoke-direct {p0, v4, v2}, Lorg/icepdf/index/core/pobjects/filters/LZWDecode;->addToBuffer(BI)V

    .line 94
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    .line 97
    :cond_8
    invoke-direct {p0, v1, v2}, Lorg/icepdf/index/core/pobjects/filters/LZWDecode;->addToBuffer(BI)V

    .line 98
    add-int/lit8 v2, v2, 0x1

    .line 99
    iget-object v4, p0, Lorg/icepdf/index/core/pobjects/filters/LZWDecode;->codes:[Lorg/icepdf/index/core/pobjects/filters/LZWDecode$Code;

    iget v5, p0, Lorg/icepdf/index/core/pobjects/filters/LZWDecode;->code:I

    new-instance v6, Lorg/icepdf/index/core/pobjects/filters/LZWDecode$Code;

    iget-object v7, p0, Lorg/icepdf/index/core/pobjects/filters/LZWDecode;->codes:[Lorg/icepdf/index/core/pobjects/filters/LZWDecode$Code;

    iget v8, p0, Lorg/icepdf/index/core/pobjects/filters/LZWDecode;->old_code:I

    aget-object v7, v7, v8

    invoke-direct {v6, v7, v1}, Lorg/icepdf/index/core/pobjects/filters/LZWDecode$Code;-><init>(Lorg/icepdf/index/core/pobjects/filters/LZWDecode$Code;B)V

    aput-object v6, v4, v5

    .line 100
    iget v4, p0, Lorg/icepdf/index/core/pobjects/filters/LZWDecode;->last_code:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lorg/icepdf/index/core/pobjects/filters/LZWDecode;->last_code:I

    goto/16 :goto_0

    .line 112
    .end local v0    # "c":Lorg/icepdf/index/core/pobjects/filters/LZWDecode$Code;
    .end local v1    # "first":B
    .end local v3    # "stack":Ljava/util/Stack;
    :cond_9
    iget-object v4, p0, Lorg/icepdf/index/core/pobjects/filters/LZWDecode;->buffer:[B

    array-length v4, v4

    add-int/lit16 v4, v4, -0x200

    if-lt v2, v4, :cond_0

    goto/16 :goto_1
.end method
