.class public Lorg/icepdf/index/core/pobjects/Dictionary;
.super Ljava/lang/Object;
.source "Dictionary.java"


# instance fields
.field protected entries:Lorg/icepdf/index/core/util/Hashtable;

.field protected inited:Z

.field protected library:Lorg/icepdf/index/core/util/Library;

.field private pObjectReference:Lorg/icepdf/index/core/pobjects/Reference;


# direct methods
.method public constructor <init>(Lorg/icepdf/index/core/util/Library;Lorg/icepdf/index/core/util/Hashtable;)V
    .locals 2
    .param p1, "library"    # Lorg/icepdf/index/core/util/Library;
    .param p2, "entries2"    # Lorg/icepdf/index/core/util/Hashtable;

    .prologue
    const/4 v1, 0x0

    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput-object v1, p0, Lorg/icepdf/index/core/pobjects/Dictionary;->library:Lorg/icepdf/index/core/util/Library;

    .line 47
    iput-object v1, p0, Lorg/icepdf/index/core/pobjects/Dictionary;->entries:Lorg/icepdf/index/core/util/Hashtable;

    .line 52
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/icepdf/index/core/pobjects/Dictionary;->inited:Z

    .line 55
    iput-object v1, p0, Lorg/icepdf/index/core/pobjects/Dictionary;->pObjectReference:Lorg/icepdf/index/core/pobjects/Reference;

    .line 64
    iput-object p1, p0, Lorg/icepdf/index/core/pobjects/Dictionary;->library:Lorg/icepdf/index/core/util/Library;

    .line 65
    iput-object p2, p0, Lorg/icepdf/index/core/pobjects/Dictionary;->entries:Lorg/icepdf/index/core/util/Hashtable;

    .line 66
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/Dictionary;->entries:Lorg/icepdf/index/core/util/Hashtable;

    if-nez v0, :cond_0

    .line 67
    new-instance v0, Lorg/icepdf/index/core/util/Hashtable;

    invoke-direct {v0}, Lorg/icepdf/index/core/util/Hashtable;-><init>()V

    iput-object v0, p0, Lorg/icepdf/index/core/pobjects/Dictionary;->entries:Lorg/icepdf/index/core/util/Hashtable;

    .line 69
    :cond_0
    return-void
.end method


# virtual methods
.method public getEntries()Lorg/icepdf/index/core/util/Hashtable;
    .locals 2

    .prologue
    .line 114
    new-instance v0, Lorg/icepdf/index/core/util/Hashtable;

    iget-object v1, p0, Lorg/icepdf/index/core/pobjects/Dictionary;->entries:Lorg/icepdf/index/core/util/Hashtable;

    invoke-direct {v0, v1}, Lorg/icepdf/index/core/util/Hashtable;-><init>(Ljava/util/Map;)V

    return-object v0
.end method

.method public getFloat(Ljava/lang/String;)F
    .locals 2
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 157
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/Dictionary;->library:Lorg/icepdf/index/core/util/Library;

    iget-object v1, p0, Lorg/icepdf/index/core/pobjects/Dictionary;->entries:Lorg/icepdf/index/core/util/Hashtable;

    invoke-virtual {v0, v1, p1}, Lorg/icepdf/index/core/util/Library;->getFloat(Lorg/icepdf/index/core/util/Hashtable;Ljava/lang/String;)F

    move-result v0

    return v0
.end method

.method public getInt(Ljava/lang/String;)I
    .locals 2
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 144
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/Dictionary;->library:Lorg/icepdf/index/core/util/Library;

    iget-object v1, p0, Lorg/icepdf/index/core/pobjects/Dictionary;->entries:Lorg/icepdf/index/core/util/Hashtable;

    invoke-virtual {v0, v1, p1}, Lorg/icepdf/index/core/util/Library;->getInt(Lorg/icepdf/index/core/util/Hashtable;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public getLibrary()Lorg/icepdf/index/core/util/Library;
    .locals 1

    .prologue
    .line 167
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/Dictionary;->library:Lorg/icepdf/index/core/util/Library;

    return-object v0
.end method

.method protected getNumber(Ljava/lang/String;)Ljava/lang/Number;
    .locals 2
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 131
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/Dictionary;->library:Lorg/icepdf/index/core/util/Library;

    iget-object v1, p0, Lorg/icepdf/index/core/pobjects/Dictionary;->entries:Lorg/icepdf/index/core/util/Hashtable;

    invoke-virtual {v0, v1, p1}, Lorg/icepdf/index/core/util/Library;->getNumber(Lorg/icepdf/index/core/util/Hashtable;Ljava/lang/String;)Ljava/lang/Number;

    move-result-object v0

    return-object v0
.end method

.method public getObject(Ljava/lang/String;)Ljava/lang/Object;
    .locals 2
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 118
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/Dictionary;->library:Lorg/icepdf/index/core/util/Library;

    iget-object v1, p0, Lorg/icepdf/index/core/pobjects/Dictionary;->entries:Lorg/icepdf/index/core/util/Hashtable;

    invoke-virtual {v0, v1, p1}, Lorg/icepdf/index/core/util/Library;->getObject(Lorg/icepdf/index/core/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getPObjectReference()Lorg/icepdf/index/core/pobjects/Reference;
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/Dictionary;->pObjectReference:Lorg/icepdf/index/core/pobjects/Reference;

    return-object v0
.end method

.method public init()V
    .locals 0

    .prologue
    .line 106
    return-void
.end method

.method public setPObjectReference(Lorg/icepdf/index/core/pobjects/Reference;)V
    .locals 0
    .param p1, "reference"    # Lorg/icepdf/index/core/pobjects/Reference;

    .prologue
    .line 87
    iput-object p1, p0, Lorg/icepdf/index/core/pobjects/Dictionary;->pObjectReference:Lorg/icepdf/index/core/pobjects/Reference;

    .line 88
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 176
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/icepdf/index/core/pobjects/Dictionary;->entries:Lorg/icepdf/index/core/util/Hashtable;

    invoke-virtual {v1}, Lorg/icepdf/index/core/util/Hashtable;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
