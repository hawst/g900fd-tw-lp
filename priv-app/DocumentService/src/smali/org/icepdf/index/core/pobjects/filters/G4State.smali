.class Lorg/icepdf/index/core/pobjects/filters/G4State;
.super Ljava/lang/Object;
.source "G4State.java"


# instance fields
.field a0:I

.field b1:I

.field cur:[I

.field curIndex:I

.field longrun:I

.field ref:[I

.field refIndex:I

.field runLength:I

.field white:Z

.field width:I


# direct methods
.method constructor <init>(I)V
    .locals 4
    .param p1, "w"    # I

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-boolean v3, p0, Lorg/icepdf/index/core/pobjects/filters/G4State;->white:Z

    .line 40
    iput p1, p0, Lorg/icepdf/index/core/pobjects/filters/G4State;->width:I

    .line 41
    iget v0, p0, Lorg/icepdf/index/core/pobjects/filters/G4State;->width:I

    add-int/lit8 v0, v0, 0x1

    new-array v0, v0, [I

    iput-object v0, p0, Lorg/icepdf/index/core/pobjects/filters/G4State;->ref:[I

    .line 42
    iget v0, p0, Lorg/icepdf/index/core/pobjects/filters/G4State;->width:I

    add-int/lit8 v0, v0, 0x1

    new-array v0, v0, [I

    iput-object v0, p0, Lorg/icepdf/index/core/pobjects/filters/G4State;->cur:[I

    .line 43
    iput v2, p0, Lorg/icepdf/index/core/pobjects/filters/G4State;->a0:I

    .line 44
    iget v0, p0, Lorg/icepdf/index/core/pobjects/filters/G4State;->width:I

    iput v0, p0, Lorg/icepdf/index/core/pobjects/filters/G4State;->b1:I

    .line 45
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/filters/G4State;->ref:[I

    iget v1, p0, Lorg/icepdf/index/core/pobjects/filters/G4State;->width:I

    aput v1, v0, v2

    .line 46
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/filters/G4State;->ref:[I

    aput v2, v0, v3

    .line 47
    iput v2, p0, Lorg/icepdf/index/core/pobjects/filters/G4State;->runLength:I

    .line 48
    iput v2, p0, Lorg/icepdf/index/core/pobjects/filters/G4State;->longrun:I

    .line 49
    iput v3, p0, Lorg/icepdf/index/core/pobjects/filters/G4State;->refIndex:I

    .line 50
    iput v2, p0, Lorg/icepdf/index/core/pobjects/filters/G4State;->curIndex:I

    .line 51
    return-void
.end method
