.class public Lorg/icepdf/index/core/util/Defs;
.super Ljava/lang/Object;
.source "Defs.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static booleanProperty(Ljava/lang/String;)Z
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 70
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lorg/icepdf/index/core/util/Defs;->booleanProperty(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static booleanProperty(Ljava/lang/String;Z)Z
    .locals 4
    .param p0, "name"    # Ljava/lang/String;
    .param p1, "defaultValue"    # Z

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 80
    invoke-static {p0}, Lorg/icepdf/index/core/util/Defs;->property(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 81
    .local v0, "value":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 82
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 97
    .end local p1    # "defaultValue":Z
    :cond_0
    :goto_0
    return p1

    .line 84
    .restart local p1    # "defaultValue":Z
    :pswitch_0
    const-string/jumbo v2, "no"

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v3}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move p1, v1

    goto :goto_0

    .line 87
    :pswitch_1
    const-string/jumbo v1, "yes"

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v3}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    move p1, v2

    goto :goto_0

    .line 90
    :pswitch_2
    const-string/jumbo v1, "true"

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v3}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    move p1, v2

    goto :goto_0

    .line 93
    :pswitch_3
    const-string/jumbo v2, "false"

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v3}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move p1, v1

    goto :goto_0

    .line 82
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static intProperty(Ljava/lang/String;I)I
    .locals 2
    .param p0, "name"    # Ljava/lang/String;
    .param p1, "defaultValue"    # I

    .prologue
    .line 54
    invoke-static {p0}, Lorg/icepdf/index/core/util/Defs;->property(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 55
    .local v0, "value":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 57
    :try_start_0
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result p1

    .line 63
    .end local p1    # "defaultValue":I
    :cond_0
    :goto_0
    return p1

    .line 59
    .restart local p1    # "defaultValue":I
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public static property(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 28
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lorg/icepdf/index/core/util/Defs;->property(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static property(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;
    .param p1, "defaultValue"    # Ljava/lang/String;

    .prologue
    .line 38
    :try_start_0
    invoke-static {p0, p1}, Ljava/lang/System;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object p1

    .line 44
    .end local p1    # "defaultValue":Ljava/lang/String;
    :goto_0
    return-object p1

    .line 40
    .restart local p1    # "defaultValue":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 42
    .local v0, "ex":Ljava/lang/SecurityException;
    invoke-static {p0, p1}, Lorg/icepdf/index/core/util/Defs;->property(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    goto :goto_0
.end method

.method public static setProperty(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 2
    .param p0, "property"    # Ljava/lang/String;
    .param p1, "value"    # Ljava/lang/Object;

    .prologue
    .line 143
    :try_start_0
    invoke-static {}, Ljava/lang/System;->getProperties()Ljava/util/Properties;

    move-result-object v1

    .line 144
    .local v1, "prop":Ljava/util/Properties;
    if-eqz p1, :cond_0

    .line 145
    invoke-virtual {v1, p0, p1}, Ljava/util/Properties;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    .line 152
    .end local v1    # "prop":Ljava/util/Properties;
    :cond_0
    :goto_0
    return-void

    .line 148
    :catch_0
    move-exception v0

    .line 150
    .local v0, "ex":Ljava/lang/SecurityException;
    invoke-static {p0, p1}, Lorg/icepdf/index/core/util/Defs;->setProperty(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static setSystemProperty(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0, "name"    # Ljava/lang/String;
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 160
    invoke-static {p0, p1}, Lorg/icepdf/index/core/util/Defs;->setProperty(Ljava/lang/String;Ljava/lang/Object;)V

    .line 161
    return-void
.end method

.method public static sysProperty(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 104
    invoke-static {p0}, Lorg/icepdf/index/core/util/Defs;->property(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static sysProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;
    .param p1, "defaultValue"    # Ljava/lang/String;

    .prologue
    .line 111
    invoke-static {p0, p1}, Lorg/icepdf/index/core/util/Defs;->property(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static sysPropertyBoolean(Ljava/lang/String;)Z
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 125
    invoke-static {p0}, Lorg/icepdf/index/core/util/Defs;->booleanProperty(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static sysPropertyBoolean(Ljava/lang/String;Z)Z
    .locals 1
    .param p0, "name"    # Ljava/lang/String;
    .param p1, "defaultValue"    # Z

    .prologue
    .line 133
    invoke-static {p0, p1}, Lorg/icepdf/index/core/util/Defs;->booleanProperty(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static sysPropertyInt(Ljava/lang/String;I)I
    .locals 1
    .param p0, "name"    # Ljava/lang/String;
    .param p1, "defaultValue"    # I

    .prologue
    .line 118
    invoke-static {p0, p1}, Lorg/icepdf/index/core/util/Defs;->intProperty(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method
