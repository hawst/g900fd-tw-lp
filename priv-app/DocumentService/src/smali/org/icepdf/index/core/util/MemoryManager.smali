.class public Lorg/icepdf/index/core/util/MemoryManager;
.super Ljava/lang/Object;
.source "MemoryManager.java"


# static fields
.field private static instance:Lorg/icepdf/index/core/util/MemoryManager;


# instance fields
.field protected cumulativeDurationManagingMemory:J

.field protected cumulativeDurationNotManagingMemory:J

.field protected delegates:Ljava/util/ArrayList;

.field protected leastRecentlyUsed:Ljava/util/ArrayList;

.field protected locked:Ljava/util/WeakHashMap;

.field protected maxMemory:J

.field protected maxSize:I

.field protected minMemory:J

.field protected percentageDurationManagingMemory:I

.field protected previousTimestampManagedMemory:J

.field protected purgeSize:I

.field protected final runtime:Ljava/lang/Runtime;


# direct methods
.method protected constructor <init>()V
    .locals 4

    .prologue
    .line 103
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v1

    iput-object v1, p0, Lorg/icepdf/index/core/util/MemoryManager;->runtime:Ljava/lang/Runtime;

    .line 45
    const-wide/32 v2, 0x2dc6c0

    iput-wide v2, p0, Lorg/icepdf/index/core/util/MemoryManager;->minMemory:J

    .line 106
    :try_start_0
    const-string/jumbo v1, "org.icepdf.core.minMemory"

    invoke-static {v1}, Lorg/icepdf/index/core/util/MemoryManager;->parse(Ljava/lang/String;)I

    move-result v0

    .line 107
    .local v0, "t":I
    if-lez v0, :cond_0

    .line 108
    int-to-long v2, v0

    iput-wide v2, p0, Lorg/icepdf/index/core/util/MemoryManager;->minMemory:J
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 115
    .end local v0    # "t":I
    :cond_0
    :goto_0
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Runtime;->maxMemory()J

    move-result-wide v2

    iput-wide v2, p0, Lorg/icepdf/index/core/util/MemoryManager;->maxMemory:J

    .line 117
    const-string/jumbo v1, "org.icepdf.core.purgeSize"

    const/4 v2, 0x5

    invoke-static {v1, v2}, Lorg/icepdf/index/core/util/Defs;->sysPropertyInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lorg/icepdf/index/core/util/MemoryManager;->purgeSize:I

    .line 119
    const-string/jumbo v1, "org.icepdf.core.maxSize"

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lorg/icepdf/index/core/util/Defs;->sysPropertyInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lorg/icepdf/index/core/util/MemoryManager;->maxSize:I

    .line 122
    new-instance v1, Ljava/util/WeakHashMap;

    invoke-direct {v1}, Ljava/util/WeakHashMap;-><init>()V

    iput-object v1, p0, Lorg/icepdf/index/core/util/MemoryManager;->locked:Ljava/util/WeakHashMap;

    .line 123
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0x100

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v1, p0, Lorg/icepdf/index/core/util/MemoryManager;->leastRecentlyUsed:Ljava/util/ArrayList;

    .line 124
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0x40

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v1, p0, Lorg/icepdf/index/core/util/MemoryManager;->delegates:Ljava/util/ArrayList;

    .line 125
    return-void

    .line 110
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method private canAllocate(IZ)Z
    .locals 10
    .param p1, "bytes"    # I
    .param p2, "doGC"    # Z

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 417
    iget-object v6, p0, Lorg/icepdf/index/core/util/MemoryManager;->runtime:Ljava/lang/Runtime;

    invoke-virtual {v6}, Ljava/lang/Runtime;->freeMemory()J

    move-result-wide v0

    .line 420
    .local v0, "mem":J
    int-to-long v6, p1

    sub-long v6, v0, v6

    iget-wide v8, p0, Lorg/icepdf/index/core/util/MemoryManager;->minMemory:J

    cmp-long v6, v6, v8

    if-lez v6, :cond_1

    .line 462
    :cond_0
    :goto_0
    return v4

    .line 425
    :cond_1
    iget-object v6, p0, Lorg/icepdf/index/core/util/MemoryManager;->runtime:Ljava/lang/Runtime;

    invoke-virtual {v6}, Ljava/lang/Runtime;->totalMemory()J

    move-result-wide v2

    .line 427
    .local v2, "total":J
    iget-wide v6, p0, Lorg/icepdf/index/core/util/MemoryManager;->maxMemory:J

    cmp-long v6, v6, v2

    if-lez v6, :cond_2

    .line 428
    iget-wide v6, p0, Lorg/icepdf/index/core/util/MemoryManager;->maxMemory:J

    sub-long/2addr v6, v2

    add-long/2addr v0, v6

    .line 429
    int-to-long v6, p1

    sub-long v6, v0, v6

    iget-wide v8, p0, Lorg/icepdf/index/core/util/MemoryManager;->minMemory:J

    cmp-long v6, v6, v8

    if-gtz v6, :cond_0

    .line 437
    :cond_2
    if-nez p2, :cond_3

    move v4, v5

    .line 438
    goto :goto_0

    .line 441
    :cond_3
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 442
    iget-object v6, p0, Lorg/icepdf/index/core/util/MemoryManager;->runtime:Ljava/lang/Runtime;

    invoke-virtual {v6}, Ljava/lang/Runtime;->freeMemory()J

    move-result-wide v0

    .line 445
    int-to-long v6, p1

    sub-long v6, v0, v6

    iget-wide v8, p0, Lorg/icepdf/index/core/util/MemoryManager;->minMemory:J

    cmp-long v6, v6, v8

    if-gtz v6, :cond_0

    .line 451
    invoke-static {}, Ljava/lang/System;->runFinalization()V

    .line 452
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 453
    iget-object v6, p0, Lorg/icepdf/index/core/util/MemoryManager;->runtime:Ljava/lang/Runtime;

    invoke-virtual {v6}, Ljava/lang/Runtime;->freeMemory()J

    move-result-wide v0

    .line 456
    int-to-long v6, p1

    sub-long v6, v0, v6

    iget-wide v8, p0, Lorg/icepdf/index/core/util/MemoryManager;->minMemory:J

    cmp-long v6, v6, v8

    if-gtz v6, :cond_0

    move v4, v5

    .line 462
    goto :goto_0
.end method

.method private finishedMemoryProcessing(J)V
    .locals 13
    .param p1, "beginTime"    # J

    .prologue
    const-wide/16 v10, 0x0

    .line 502
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 503
    .local v2, "endTime":J
    sub-long v0, v2, p1

    .line 504
    .local v0, "duration":J
    cmp-long v6, v0, v10

    if-lez v6, :cond_0

    .line 505
    iget-wide v6, p0, Lorg/icepdf/index/core/util/MemoryManager;->cumulativeDurationManagingMemory:J

    add-long/2addr v6, v0

    iput-wide v6, p0, Lorg/icepdf/index/core/util/MemoryManager;->cumulativeDurationManagingMemory:J

    .line 506
    :cond_0
    iget-wide v6, p0, Lorg/icepdf/index/core/util/MemoryManager;->previousTimestampManagedMemory:J

    cmp-long v6, v6, v10

    if-eqz v6, :cond_1

    .line 507
    iget-wide v6, p0, Lorg/icepdf/index/core/util/MemoryManager;->previousTimestampManagedMemory:J

    sub-long v0, p1, v6

    .line 508
    cmp-long v6, v0, v10

    if-lez v6, :cond_1

    .line 509
    iget-wide v6, p0, Lorg/icepdf/index/core/util/MemoryManager;->cumulativeDurationNotManagingMemory:J

    add-long/2addr v6, v0

    iput-wide v6, p0, Lorg/icepdf/index/core/util/MemoryManager;->cumulativeDurationNotManagingMemory:J

    .line 511
    :cond_1
    iput-wide v2, p0, Lorg/icepdf/index/core/util/MemoryManager;->previousTimestampManagedMemory:J

    .line 513
    iget-wide v6, p0, Lorg/icepdf/index/core/util/MemoryManager;->cumulativeDurationManagingMemory:J

    iget-wide v8, p0, Lorg/icepdf/index/core/util/MemoryManager;->cumulativeDurationNotManagingMemory:J

    add-long v4, v6, v8

    .line 514
    .local v4, "totalDuration":J
    cmp-long v6, v4, v10

    if-lez v6, :cond_2

    .line 515
    iget-wide v6, p0, Lorg/icepdf/index/core/util/MemoryManager;->cumulativeDurationManagingMemory:J

    const-wide/16 v8, 0x64

    mul-long/2addr v6, v8

    div-long/2addr v6, v4

    long-to-int v6, v6

    iput v6, p0, Lorg/icepdf/index/core/util/MemoryManager;->percentageDurationManagingMemory:I

    .line 518
    :cond_2
    return-void
.end method

.method public static declared-synchronized getInstance()Lorg/icepdf/index/core/util/MemoryManager;
    .locals 2

    .prologue
    .line 94
    const-class v1, Lorg/icepdf/index/core/util/MemoryManager;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lorg/icepdf/index/core/util/MemoryManager;->instance:Lorg/icepdf/index/core/util/MemoryManager;

    if-nez v0, :cond_0

    .line 95
    new-instance v0, Lorg/icepdf/index/core/util/MemoryManager;

    invoke-direct {v0}, Lorg/icepdf/index/core/util/MemoryManager;-><init>()V

    sput-object v0, Lorg/icepdf/index/core/util/MemoryManager;->instance:Lorg/icepdf/index/core/util/MemoryManager;

    .line 97
    :cond_0
    sget-object v0, Lorg/icepdf/index/core/util/MemoryManager;->instance:Lorg/icepdf/index/core/util/MemoryManager;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 94
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static final parse(Ljava/lang/String;)I
    .locals 5
    .param p0, "memoryValue"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 350
    invoke-static {p0}, Lorg/icepdf/index/core/util/Defs;->sysProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 351
    .local v2, "s":Ljava/lang/String;
    if-nez v2, :cond_0

    .line 352
    const/4 v3, -0x1

    .line 365
    :goto_0
    return v3

    .line 354
    :cond_0
    const/4 v1, 0x1

    .line 355
    .local v1, "mult":I
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v2, v3}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 356
    .local v0, "c":C
    const/16 v3, 0x6b

    if-eq v0, v3, :cond_1

    const/16 v3, 0x4b

    if-ne v0, v3, :cond_2

    .line 357
    :cond_1
    const/16 v1, 0x400

    .line 358
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v2, v4, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 360
    :cond_2
    const/16 v3, 0x6d

    if-eq v0, v3, :cond_3

    const/16 v3, 0x4d

    if-ne v0, v3, :cond_4

    .line 361
    :cond_3
    const/high16 v1, 0x100000

    .line 362
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v2, v4, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 365
    :cond_4
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    mul-int/2addr v3, v1

    goto :goto_0
.end method

.method public static declared-synchronized setInstance(Lorg/icepdf/index/core/util/MemoryManager;)V
    .locals 2
    .param p0, "memoryMangaer"    # Lorg/icepdf/index/core/util/MemoryManager;

    .prologue
    .line 84
    const-class v0, Lorg/icepdf/index/core/util/MemoryManager;

    monitor-enter v0

    :try_start_0
    sput-object p0, Lorg/icepdf/index/core/util/MemoryManager;->instance:Lorg/icepdf/index/core/util/MemoryManager;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 85
    monitor-exit v0

    return-void

    .line 84
    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method


# virtual methods
.method public checkMemory(I)Z
    .locals 7
    .param p1, "memoryNeeded"    # I

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 481
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 482
    .local v0, "beginTime":J
    const/4 v2, 0x0

    .line 484
    .local v2, "count":I
    :cond_0
    if-lez v2, :cond_1

    move v4, v5

    :goto_0
    invoke-direct {p0, p1, v4}, Lorg/icepdf/index/core/util/MemoryManager;->canAllocate(IZ)Z

    move-result v4

    if-nez v4, :cond_3

    .line 486
    invoke-virtual {p0}, Lorg/icepdf/index/core/util/MemoryManager;->reduceMemory()Z

    move-result v3

    .line 487
    .local v3, "reducedSomething":Z
    if-nez v3, :cond_2

    if-lez v2, :cond_2

    .line 488
    invoke-direct {p0, v0, v1}, Lorg/icepdf/index/core/util/MemoryManager;->finishedMemoryProcessing(J)V

    .line 498
    .end local v3    # "reducedSomething":Z
    :goto_1
    return v6

    :cond_1
    move v4, v6

    .line 484
    goto :goto_0

    .line 491
    .restart local v3    # "reducedSomething":Z
    :cond_2
    add-int/lit8 v2, v2, 0x1

    .line 492
    const/16 v4, 0xa

    if-le v2, v4, :cond_0

    .line 493
    invoke-direct {p0, v0, v1}, Lorg/icepdf/index/core/util/MemoryManager;->finishedMemoryProcessing(J)V

    goto :goto_1

    .line 497
    .end local v3    # "reducedSomething":Z
    :cond_3
    invoke-direct {p0, v0, v1}, Lorg/icepdf/index/core/util/MemoryManager;->finishedMemoryProcessing(J)V

    move v6, v5

    .line 498
    goto :goto_1
.end method

.method public getFreeMemory()J
    .locals 2

    .prologue
    .line 409
    iget-object v0, p0, Lorg/icepdf/index/core/util/MemoryManager;->runtime:Ljava/lang/Runtime;

    invoke-virtual {v0}, Ljava/lang/Runtime;->freeMemory()J

    move-result-wide v0

    return-wide v0
.end method

.method public getMaxMemory()J
    .locals 2

    .prologue
    .line 400
    iget-wide v0, p0, Lorg/icepdf/index/core/util/MemoryManager;->maxMemory:J

    return-wide v0
.end method

.method public getMinMemory()J
    .locals 2

    .prologue
    .line 381
    iget-wide v0, p0, Lorg/icepdf/index/core/util/MemoryManager;->minMemory:J

    return-wide v0
.end method

.method protected declared-synchronized isLocked(Lorg/icepdf/index/core/util/MemoryManageable;)Z
    .locals 5
    .param p1, "mm"    # Lorg/icepdf/index/core/util/MemoryManageable;

    .prologue
    .line 315
    monitor-enter p0

    :try_start_0
    iget-object v4, p0, Lorg/icepdf/index/core/util/MemoryManager;->locked:Ljava/util/WeakHashMap;

    invoke-virtual {v4}, Ljava/util/WeakHashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    .line 316
    .local v0, "entries":Ljava/util/Set;
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 318
    .local v2, "entryIterator":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 319
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 320
    .local v1, "entry":Ljava/util/Map$Entry;
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/HashSet;

    .line 321
    .local v3, "inUse":Ljava/util/HashSet;
    if-eqz v3, :cond_0

    invoke-virtual {v3, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v4

    if-eqz v4, :cond_0

    .line 322
    const/4 v4, 0x1

    .line 324
    .end local v1    # "entry":Ljava/util/Map$Entry;
    .end local v3    # "inUse":Ljava/util/HashSet;
    :goto_0
    monitor-exit p0

    return v4

    :cond_1
    const/4 v4, 0x0

    goto :goto_0

    .line 315
    .end local v0    # "entries":Ljava/util/Set;
    .end local v2    # "entryIterator":Ljava/util/Iterator;
    :catchall_0
    move-exception v4

    monitor-exit p0

    throw v4
.end method

.method public isLowMemory()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 473
    invoke-direct {p0, v1, v0}, Lorg/icepdf/index/core/util/MemoryManager;->canAllocate(IZ)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public declared-synchronized lock(Ljava/lang/Object;Lorg/icepdf/index/core/util/MemoryManageable;)V
    .locals 5
    .param p1, "user"    # Ljava/lang/Object;
    .param p2, "mm"    # Lorg/icepdf/index/core/util/MemoryManageable;

    .prologue
    .line 128
    monitor-enter p0

    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 150
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 131
    :cond_1
    :try_start_0
    iget-object v4, p0, Lorg/icepdf/index/core/util/MemoryManager;->locked:Ljava/util/WeakHashMap;

    invoke-virtual {v4, p1}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashSet;

    .line 132
    .local v0, "inUse":Ljava/util/HashSet;
    if-nez v0, :cond_2

    .line 133
    new-instance v0, Ljava/util/HashSet;

    .end local v0    # "inUse":Ljava/util/HashSet;
    const/16 v4, 0x100

    invoke-direct {v0, v4}, Ljava/util/HashSet;-><init>(I)V

    .line 134
    .restart local v0    # "inUse":Ljava/util/HashSet;
    iget-object v4, p0, Lorg/icepdf/index/core/util/MemoryManager;->locked:Ljava/util/WeakHashMap;

    invoke-virtual {v4, p1, v0}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 136
    :cond_2
    invoke-virtual {v0, p2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 138
    iget-object v4, p0, Lorg/icepdf/index/core/util/MemoryManager;->leastRecentlyUsed:Ljava/util/ArrayList;

    invoke-virtual {v4, p2}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 139
    iget-object v4, p0, Lorg/icepdf/index/core/util/MemoryManager;->leastRecentlyUsed:Ljava/util/ArrayList;

    invoke-virtual {v4, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 141
    iget v4, p0, Lorg/icepdf/index/core/util/MemoryManager;->maxSize:I

    if-lez v4, :cond_0

    .line 142
    iget-object v4, p0, Lorg/icepdf/index/core/util/MemoryManager;->leastRecentlyUsed:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 143
    .local v2, "numUsed":I
    iget v4, p0, Lorg/icepdf/index/core/util/MemoryManager;->maxSize:I

    sub-int v3, v2, v4

    .line 144
    .local v3, "numUsedMoreThanShould":I
    if-lez v3, :cond_0

    .line 146
    iget v4, p0, Lorg/icepdf/index/core/util/MemoryManager;->purgeSize:I

    invoke-static {v4, v3}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 147
    .local v1, "numToDo":I
    invoke-virtual {p0, v1}, Lorg/icepdf/index/core/util/MemoryManager;->reduceMemory(I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 128
    .end local v0    # "inUse":Ljava/util/HashSet;
    .end local v1    # "numToDo":I
    .end local v2    # "numUsed":I
    .end local v3    # "numUsedMoreThanShould":I
    :catchall_0
    move-exception v4

    monitor-exit p0

    throw v4
.end method

.method protected declared-synchronized reduceMemory(I)I
    .locals 4
    .param p1, "numToDo"    # I

    .prologue
    .line 291
    monitor-enter p0

    const/4 v2, 0x0

    .line 293
    .local v2, "numDone":I
    const/4 v0, 0x0

    .line 294
    .local v0, "leastRecentlyUsedIndex":I
    :goto_0
    if-ge v2, p1, :cond_0

    :try_start_0
    iget-object v3, p0, Lorg/icepdf/index/core/util/MemoryManager;->leastRecentlyUsed:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v0, v3, :cond_0

    .line 296
    iget-object v3, p0, Lorg/icepdf/index/core/util/MemoryManager;->leastRecentlyUsed:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/icepdf/index/core/util/MemoryManageable;

    .line 299
    .local v1, "mm":Lorg/icepdf/index/core/util/MemoryManageable;
    invoke-virtual {p0, v1}, Lorg/icepdf/index/core/util/MemoryManager;->isLocked(Lorg/icepdf/index/core/util/MemoryManageable;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 300
    invoke-interface {v1}, Lorg/icepdf/index/core/util/MemoryManageable;->reduceMemory()V

    .line 301
    add-int/lit8 v2, v2, 0x1

    .line 302
    iget-object v3, p0, Lorg/icepdf/index/core/util/MemoryManager;->leastRecentlyUsed:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 307
    .end local v1    # "mm":Lorg/icepdf/index/core/util/MemoryManageable;
    :catch_0
    move-exception v3

    .line 311
    :cond_0
    monitor-exit p0

    return v2

    .line 304
    .restart local v1    # "mm":Lorg/icepdf/index/core/util/MemoryManageable;
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 291
    .end local v1    # "mm":Lorg/icepdf/index/core/util/MemoryManageable;
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3
.end method

.method protected declared-synchronized reduceMemory()Z
    .locals 9

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 265
    monitor-enter p0

    :try_start_0
    iget v4, p0, Lorg/icepdf/index/core/util/MemoryManager;->purgeSize:I

    .line 267
    .local v4, "numToDo":I
    const/4 v0, 0x0

    .line 268
    .local v0, "aggressive":I
    iget-object v7, p0, Lorg/icepdf/index/core/util/MemoryManager;->leastRecentlyUsed:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 269
    .local v2, "lruSize":I
    iget v7, p0, Lorg/icepdf/index/core/util/MemoryManager;->percentageDurationManagingMemory:I

    const/16 v8, 0xf

    if-gt v7, v8, :cond_0

    const/16 v7, 0x64

    if-le v2, v7, :cond_6

    .line 270
    :cond_0
    mul-int/lit8 v7, v2, 0x3c

    div-int/lit8 v0, v7, 0x64

    .line 275
    :cond_1
    :goto_0
    if-le v0, v4, :cond_2

    .line 276
    move v4, v0

    .line 278
    :cond_2
    invoke-virtual {p0, v4}, Lorg/icepdf/index/core/util/MemoryManager;->reduceMemory(I)I

    move-result v3

    .line 280
    .local v3, "numDone":I
    const/4 v1, 0x0

    .line 281
    .local v1, "delegatesReduced":Z
    if-nez v3, :cond_8

    .line 282
    const/4 v7, 0x1

    invoke-virtual {p0, v7}, Lorg/icepdf/index/core/util/MemoryManager;->reduceMemoryWithDelegates(Z)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    .line 286
    :cond_3
    :goto_1
    if-gtz v3, :cond_4

    if-eqz v1, :cond_5

    :cond_4
    move v5, v6

    :cond_5
    monitor-exit p0

    return v5

    .line 271
    .end local v1    # "delegatesReduced":Z
    .end local v3    # "numDone":I
    :cond_6
    const/16 v7, 0x32

    if-le v2, v7, :cond_7

    .line 272
    mul-int/lit8 v7, v2, 0x32

    :try_start_1
    div-int/lit8 v0, v7, 0x64

    goto :goto_0

    .line 273
    :cond_7
    const/16 v7, 0x14

    if-le v2, v7, :cond_1

    .line 274
    mul-int/lit8 v7, v2, 0x28

    div-int/lit8 v0, v7, 0x64

    goto :goto_0

    .line 283
    .restart local v1    # "delegatesReduced":Z
    .restart local v3    # "numDone":I
    :cond_8
    if-ge v3, v4, :cond_3

    .line 284
    const/4 v7, 0x0

    invoke-virtual {p0, v7}, Lorg/icepdf/index/core/util/MemoryManager;->reduceMemoryWithDelegates(Z)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v1

    goto :goto_1

    .line 265
    .end local v0    # "aggressive":I
    .end local v1    # "delegatesReduced":Z
    .end local v2    # "lruSize":I
    .end local v3    # "numDone":I
    .end local v4    # "numToDo":I
    :catchall_0
    move-exception v5

    monitor-exit p0

    throw v5
.end method

.method protected declared-synchronized reduceMemoryWithDelegates(Z)Z
    .locals 6
    .param p1, "aggressively"    # Z

    .prologue
    .line 328
    monitor-enter p0

    if-eqz p1, :cond_0

    const/4 v4, 0x1

    .line 330
    .local v4, "reductionPolicy":I
    :goto_0
    const/4 v0, 0x0

    .line 331
    .local v0, "anyReduced":Z
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    :try_start_0
    iget-object v5, p0, Lorg/icepdf/index/core/util/MemoryManager;->delegates:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-ge v1, v5, :cond_2

    .line 332
    iget-object v5, p0, Lorg/icepdf/index/core/util/MemoryManager;->delegates:Ljava/util/ArrayList;

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/icepdf/index/core/util/MemoryManagerDelegate;

    .line 333
    .local v2, "mmd":Lorg/icepdf/index/core/util/MemoryManagerDelegate;
    if-nez v2, :cond_1

    .line 331
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 328
    .end local v0    # "anyReduced":Z
    .end local v1    # "i":I
    .end local v2    # "mmd":Lorg/icepdf/index/core/util/MemoryManagerDelegate;
    .end local v4    # "reductionPolicy":I
    :cond_0
    const/4 v4, 0x0

    goto :goto_0

    .line 335
    .restart local v0    # "anyReduced":Z
    .restart local v1    # "i":I
    .restart local v2    # "mmd":Lorg/icepdf/index/core/util/MemoryManagerDelegate;
    .restart local v4    # "reductionPolicy":I
    :cond_1
    invoke-interface {v2, v4}, Lorg/icepdf/index/core/util/MemoryManagerDelegate;->reduceMemory(I)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    .line 336
    .local v3, "reduced":Z
    or-int/2addr v0, v3

    goto :goto_2

    .line 338
    .end local v2    # "mmd":Lorg/icepdf/index/core/util/MemoryManagerDelegate;
    .end local v3    # "reduced":Z
    :cond_2
    monitor-exit p0

    return v0

    .line 328
    :catchall_0
    move-exception v5

    monitor-exit p0

    throw v5
.end method

.method public declared-synchronized registerMemoryManagerDelegate(Lorg/icepdf/index/core/util/MemoryManagerDelegate;)V
    .locals 1
    .param p1, "delegate"    # Lorg/icepdf/index/core/util/MemoryManagerDelegate;

    .prologue
    .line 168
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/icepdf/index/core/util/MemoryManager;->delegates:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 169
    iget-object v0, p0, Lorg/icepdf/index/core/util/MemoryManager;->delegates:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 170
    :cond_0
    monitor-exit p0

    return-void

    .line 168
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized release(Ljava/lang/Object;Lorg/icepdf/index/core/util/MemoryManageable;)V
    .locals 2
    .param p1, "user"    # Ljava/lang/Object;
    .param p2, "mm"    # Lorg/icepdf/index/core/util/MemoryManageable;

    .prologue
    .line 153
    monitor-enter p0

    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 165
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 156
    :cond_1
    :try_start_0
    iget-object v1, p0, Lorg/icepdf/index/core/util/MemoryManager;->locked:Ljava/util/WeakHashMap;

    invoke-virtual {v1, p1}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashSet;

    .line 157
    .local v0, "inUse":Ljava/util/HashSet;
    if-eqz v0, :cond_0

    .line 160
    invoke-virtual {v0}, Ljava/util/HashSet;->size()I

    move-result v1

    if-nez v1, :cond_0

    .line 161
    iget-object v1, p0, Lorg/icepdf/index/core/util/MemoryManager;->locked:Ljava/util/WeakHashMap;

    invoke-virtual {v1, p1}, Ljava/util/WeakHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 153
    .end local v0    # "inUse":Ljava/util/HashSet;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized releaseAllByLibrary(Lorg/icepdf/index/core/util/Library;)V
    .locals 16
    .param p1, "library"    # Lorg/icepdf/index/core/util/Library;

    .prologue
    .line 174
    monitor-enter p0

    if-nez p1, :cond_1

    .line 258
    :cond_0
    monitor-exit p0

    return-void

    .line 179
    :cond_1
    :try_start_0
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/icepdf/index/core/util/MemoryManager;->leastRecentlyUsed:Ljava/util/ArrayList;

    invoke-virtual {v14}, Ljava/util/ArrayList;->size()I

    move-result v14

    add-int/lit8 v4, v14, -0x1

    .local v4, "i":I
    :goto_0
    if-ltz v4, :cond_4

    .line 180
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/icepdf/index/core/util/MemoryManager;->leastRecentlyUsed:Ljava/util/ArrayList;

    invoke-virtual {v14, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lorg/icepdf/index/core/util/MemoryManageable;

    .line 182
    .local v7, "mm":Lorg/icepdf/index/core/util/MemoryManageable;
    invoke-interface {v7}, Lorg/icepdf/index/core/util/MemoryManageable;->getLibrary()Lorg/icepdf/index/core/util/Library;

    move-result-object v6

    .line 183
    .local v6, "lib":Lorg/icepdf/index/core/util/Library;
    if-nez v6, :cond_3

    .line 179
    :cond_2
    :goto_1
    add-int/lit8 v4, v4, -0x1

    goto :goto_0

    .line 188
    :cond_3
    move-object/from16 v0, p1

    invoke-virtual {v6, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_2

    .line 189
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/icepdf/index/core/util/MemoryManager;->leastRecentlyUsed:Ljava/util/ArrayList;

    invoke-virtual {v14, v4}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 174
    .end local v4    # "i":I
    .end local v6    # "lib":Lorg/icepdf/index/core/util/Library;
    .end local v7    # "mm":Lorg/icepdf/index/core/util/MemoryManageable;
    :catchall_0
    move-exception v14

    monitor-exit p0

    throw v14

    .line 195
    .restart local v4    # "i":I
    :cond_4
    :try_start_1
    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    .line 196
    .local v13, "usersToRemove":Ljava/util/ArrayList;
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/icepdf/index/core/util/MemoryManager;->locked:Ljava/util/WeakHashMap;

    invoke-virtual {v14}, Ljava/util/WeakHashMap;->entrySet()Ljava/util/Set;

    move-result-object v1

    .line 197
    .local v1, "entries":Ljava/util/Set;
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 198
    .local v3, "entryIterator":Ljava/util/Iterator;
    :cond_5
    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_9

    .line 199
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 200
    .local v2, "entry":Ljava/util/Map$Entry;
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v12

    .line 202
    .local v12, "user":Ljava/lang/Object;
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/HashSet;

    .line 203
    .local v5, "inUse":Ljava/util/HashSet;
    if-eqz v5, :cond_5

    .line 205
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 206
    .local v10, "mmsToRemove":Ljava/util/ArrayList;
    invoke-virtual {v5}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v9

    .line 207
    .local v9, "mms":Ljava/util/Iterator;
    :cond_6
    :goto_3
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_7

    .line 208
    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lorg/icepdf/index/core/util/MemoryManageable;

    .line 210
    .restart local v7    # "mm":Lorg/icepdf/index/core/util/MemoryManageable;
    if-eqz v7, :cond_6

    .line 211
    invoke-interface {v7}, Lorg/icepdf/index/core/util/MemoryManageable;->getLibrary()Lorg/icepdf/index/core/util/Library;

    move-result-object v6

    .line 212
    .restart local v6    # "lib":Lorg/icepdf/index/core/util/Library;
    if-eqz v6, :cond_6

    .line 217
    move-object/from16 v0, p1

    invoke-virtual {v6, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_6

    .line 218
    invoke-virtual {v10, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 222
    .end local v6    # "lib":Lorg/icepdf/index/core/util/Library;
    .end local v7    # "mm":Lorg/icepdf/index/core/util/MemoryManageable;
    :cond_7
    const/4 v4, 0x0

    :goto_4
    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v14

    if-ge v4, v14, :cond_8

    .line 223
    invoke-virtual {v10, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v14

    invoke-virtual {v5, v14}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 222
    add-int/lit8 v4, v4, 0x1

    goto :goto_4

    .line 224
    :cond_8
    invoke-virtual {v10}, Ljava/util/ArrayList;->clear()V

    .line 231
    invoke-virtual {v5}, Ljava/util/HashSet;->size()I

    move-result v14

    if-nez v14, :cond_5

    .line 233
    invoke-virtual {v13, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 237
    .end local v2    # "entry":Ljava/util/Map$Entry;
    .end local v5    # "inUse":Ljava/util/HashSet;
    .end local v9    # "mms":Ljava/util/Iterator;
    .end local v10    # "mmsToRemove":Ljava/util/ArrayList;
    .end local v12    # "user":Ljava/lang/Object;
    :cond_9
    const/4 v4, 0x0

    :goto_5
    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v14

    if-ge v4, v14, :cond_a

    .line 238
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/icepdf/index/core/util/MemoryManager;->locked:Ljava/util/WeakHashMap;

    invoke-virtual {v13, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/util/WeakHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 237
    add-int/lit8 v4, v4, 0x1

    goto :goto_5

    .line 239
    :cond_a
    invoke-virtual {v13}, Ljava/util/ArrayList;->clear()V

    .line 241
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/icepdf/index/core/util/MemoryManager;->delegates:Ljava/util/ArrayList;

    invoke-virtual {v14}, Ljava/util/ArrayList;->size()I

    move-result v14

    add-int/lit8 v4, v14, -0x1

    :goto_6
    if-ltz v4, :cond_0

    .line 242
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/icepdf/index/core/util/MemoryManager;->delegates:Ljava/util/ArrayList;

    invoke-virtual {v14, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lorg/icepdf/index/core/util/MemoryManagerDelegate;

    .line 243
    .local v8, "mmd":Lorg/icepdf/index/core/util/MemoryManagerDelegate;
    const/4 v11, 0x0

    .line 244
    .local v11, "shouldRemove":Z
    if-nez v8, :cond_d

    .line 245
    const/4 v11, 0x1

    .line 255
    :cond_b
    :goto_7
    if-eqz v11, :cond_c

    .line 256
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/icepdf/index/core/util/MemoryManager;->delegates:Ljava/util/ArrayList;

    invoke-virtual {v14, v4}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 241
    :cond_c
    add-int/lit8 v4, v4, -0x1

    goto :goto_6

    .line 247
    :cond_d
    invoke-interface {v8}, Lorg/icepdf/index/core/util/MemoryManagerDelegate;->getLibrary()Lorg/icepdf/index/core/util/Library;

    move-result-object v6

    .line 248
    .restart local v6    # "lib":Lorg/icepdf/index/core/util/Library;
    if-nez v6, :cond_e

    .line 249
    const/4 v11, 0x1

    goto :goto_7

    .line 251
    :cond_e
    move-object/from16 v0, p1

    invoke-virtual {v6, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v14

    if-eqz v14, :cond_b

    .line 252
    const/4 v11, 0x1

    goto :goto_7
.end method

.method public setMaxMemory(J)V
    .locals 1
    .param p1, "m"    # J

    .prologue
    .line 393
    iput-wide p1, p0, Lorg/icepdf/index/core/util/MemoryManager;->maxMemory:J

    .line 394
    return-void
.end method

.method public setMinMemory(J)V
    .locals 1
    .param p1, "m"    # J

    .prologue
    .line 374
    iput-wide p1, p0, Lorg/icepdf/index/core/util/MemoryManager;->minMemory:J

    .line 375
    return-void
.end method
