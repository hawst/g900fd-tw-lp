.class Lorg/icepdf/index/core/pobjects/fonts/ofont/CMap;
.super Lorg/icepdf/index/core/pobjects/Dictionary;
.source "CMap.java"

# interfaces
.implements Lorg/icepdf/index/core/pobjects/fonts/CMap;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/icepdf/index/core/pobjects/fonts/ofont/CMap$CMapRange;
    }
.end annotation


# instance fields
.field private bfChars:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "[C>;"
        }
    .end annotation
.end field

.field private bfRange:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/icepdf/index/core/pobjects/fonts/ofont/CMap$CMapRange;",
            ">;"
        }
    .end annotation
.end field

.field private cMapInputStream:Ljava/io/InputStream;

.field private cMapName:Ljava/lang/String;

.field private cMapStream:Lorg/icepdf/index/core/pobjects/Stream;

.field private cMapType:F

.field private codeSpaceRange:[[I

.field private oneByte:Z


# direct methods
.method public constructor <init>(Lorg/icepdf/index/core/util/Library;Lorg/icepdf/index/core/util/Hashtable;Ljava/io/InputStream;)V
    .locals 0
    .param p1, "l"    # Lorg/icepdf/index/core/util/Library;
    .param p2, "h"    # Lorg/icepdf/index/core/util/Hashtable;
    .param p3, "cMapInputStream"    # Ljava/io/InputStream;

    .prologue
    .line 150
    invoke-direct {p0, p1, p2}, Lorg/icepdf/index/core/pobjects/Dictionary;-><init>(Lorg/icepdf/index/core/util/Library;Lorg/icepdf/index/core/util/Hashtable;)V

    .line 151
    iput-object p3, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/CMap;->cMapInputStream:Ljava/io/InputStream;

    .line 152
    return-void
.end method

.method public constructor <init>(Lorg/icepdf/index/core/util/Library;Lorg/icepdf/index/core/util/Hashtable;Lorg/icepdf/index/core/pobjects/Stream;)V
    .locals 0
    .param p1, "library"    # Lorg/icepdf/index/core/util/Library;
    .param p2, "entries"    # Lorg/icepdf/index/core/util/Hashtable;
    .param p3, "cMapStream"    # Lorg/icepdf/index/core/pobjects/Stream;

    .prologue
    .line 145
    invoke-direct {p0, p1, p2}, Lorg/icepdf/index/core/pobjects/Dictionary;-><init>(Lorg/icepdf/index/core/util/Library;Lorg/icepdf/index/core/util/Hashtable;)V

    .line 146
    iput-object p3, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/CMap;->cMapStream:Lorg/icepdf/index/core/pobjects/Stream;

    .line 147
    return-void
.end method

.method static synthetic access$000(Lorg/icepdf/index/core/pobjects/fonts/ofont/CMap;Ljava/lang/CharSequence;)[C
    .locals 1
    .param p0, "x0"    # Lorg/icepdf/index/core/pobjects/fonts/ofont/CMap;
    .param p1, "x1"    # Ljava/lang/CharSequence;

    .prologue
    .line 45
    invoke-direct {p0, p1}, Lorg/icepdf/index/core/pobjects/fonts/ofont/CMap;->convertToString(Ljava/lang/CharSequence;)[C

    move-result-object v0

    return-object v0
.end method

.method private convertToString(Ljava/lang/CharSequence;)[C
    .locals 6
    .param p1, "s"    # Ljava/lang/CharSequence;

    .prologue
    .line 576
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v4

    rem-int/lit8 v4, v4, 0x2

    if-eqz v4, :cond_1

    .line 577
    :cond_0
    new-instance v4, Ljava/lang/IllegalArgumentException;

    invoke-direct {v4}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v4

    .line 579
    :cond_1
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v3

    .line 580
    .local v3, "len":I
    div-int/lit8 v4, v3, 0x2

    new-array v0, v4, [C

    .line 581
    .local v0, "dest":[C
    const/4 v1, 0x0

    .local v1, "i":I
    const/4 v2, 0x0

    .local v2, "j":I
    :goto_0
    if-ge v1, v3, :cond_2

    .line 582
    invoke-interface {p1, v1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v4

    shl-int/lit8 v4, v4, 0x8

    add-int/lit8 v5, v1, 0x1

    invoke-interface {p1, v5}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v5

    or-int/2addr v4, v5

    int-to-char v4, v4

    aput-char v4, v0, v2

    .line 581
    add-int/lit8 v1, v1, 0x2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 583
    :cond_2
    return-object v0
.end method


# virtual methods
.method public init()V
    .locals 26

    .prologue
    .line 198
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/CMap;->cMapInputStream:Ljava/io/InputStream;

    move-object/from16 v21, v0

    if-nez v21, :cond_0

    .line 199
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/CMap;->cMapStream:Lorg/icepdf/index/core/pobjects/Stream;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lorg/icepdf/index/core/pobjects/Stream;->getInputStreamForDecodedStreamBytes()Ljava/io/InputStream;

    move-result-object v21

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/icepdf/index/core/pobjects/fonts/ofont/CMap;->cMapInputStream:Ljava/io/InputStream;

    .line 218
    :cond_0
    new-instance v15, Lorg/icepdf/index/core/util/Parser;

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/CMap;->cMapInputStream:Ljava/io/InputStream;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-direct {v15, v0}, Lorg/icepdf/index/core/util/Parser;-><init>(Ljava/io/InputStream;)V

    .line 225
    .local v15, "parser":Lorg/icepdf/index/core/util/Parser;
    const/16 v16, 0x0

    .line 227
    :goto_0
    invoke-virtual {v15}, Lorg/icepdf/index/core/util/Parser;->getStreamObject()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v19

    .line 229
    .local v19, "token":Ljava/lang/Object;
    if-nez v19, :cond_2

    .line 409
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/CMap;->cMapInputStream:Ljava/io/InputStream;

    move-object/from16 v21, v0

    if-eqz v21, :cond_1

    .line 411
    :try_start_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/CMap;->cMapInputStream:Ljava/io/InputStream;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_5

    .line 418
    .end local v15    # "parser":Lorg/icepdf/index/core/util/Parser;
    .end local v19    # "token":Ljava/lang/Object;
    :cond_1
    :goto_1
    return-void

    .line 233
    .restart local v15    # "parser":Lorg/icepdf/index/core/util/Parser;
    .restart local v19    # "token":Ljava/lang/Object;
    :cond_2
    :try_start_2
    invoke-virtual/range {v19 .. v19}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v10

    .line 234
    .local v10, "nameString":Ljava/lang/String;
    sget-object v21, Ljava/util/Locale;->US:Ljava/util/Locale;

    move-object/from16 v0, v21

    invoke-virtual {v10, v0}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v21

    const-string/jumbo v22, "cidsysteminfo"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v21

    if-ltz v21, :cond_3

    .line 237
    invoke-virtual {v15}, Lorg/icepdf/index/core/util/Parser;->getStreamObject()Ljava/lang/Object;

    move-result-object v19

    .line 238
    move-object/from16 v0, v19

    instance-of v0, v0, Lorg/icepdf/index/core/util/Hashtable;

    move/from16 v21, v0

    if-eqz v21, :cond_3

    .line 240
    invoke-virtual {v15}, Lorg/icepdf/index/core/util/Parser;->getStreamObject()Ljava/lang/Object;

    move-result-object v19

    .line 245
    :cond_3
    move-object/from16 v0, v19

    instance-of v0, v0, Lorg/icepdf/index/core/pobjects/Name;

    move/from16 v21, v0

    if-eqz v21, :cond_6

    .line 246
    invoke-virtual/range {v19 .. v19}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v10

    .line 248
    sget-object v21, Ljava/util/Locale;->US:Ljava/util/Locale;

    move-object/from16 v0, v21

    invoke-virtual {v10, v0}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v21

    const-string/jumbo v22, "cmapname"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v21

    if-ltz v21, :cond_4

    .line 250
    invoke-virtual {v15}, Lorg/icepdf/index/core/util/Parser;->getStreamObject()Ljava/lang/Object;

    move-result-object v19

    .line 251
    invoke-virtual/range {v19 .. v19}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/icepdf/index/core/pobjects/fonts/ofont/CMap;->cMapName:Ljava/lang/String;

    .line 253
    invoke-virtual {v15}, Lorg/icepdf/index/core/util/Parser;->getStreamObject()Ljava/lang/Object;

    move-result-object v19

    .line 256
    :cond_4
    sget-object v21, Ljava/util/Locale;->US:Ljava/util/Locale;

    move-object/from16 v0, v21

    invoke-virtual {v10, v0}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v21

    const-string/jumbo v22, "cmaptype"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v21

    if-ltz v21, :cond_5

    .line 258
    invoke-virtual {v15}, Lorg/icepdf/index/core/util/Parser;->getStreamObject()Ljava/lang/Object;

    move-result-object v19

    .line 259
    invoke-virtual/range {v19 .. v19}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v21

    move/from16 v0, v21

    move-object/from16 v1, p0

    iput v0, v1, Lorg/icepdf/index/core/pobjects/fonts/ofont/CMap;->cMapType:F

    .line 261
    invoke-virtual {v15}, Lorg/icepdf/index/core/util/Parser;->getStreamObject()Ljava/lang/Object;

    move-result-object v19

    .line 264
    :cond_5
    sget-object v21, Ljava/util/Locale;->US:Ljava/util/Locale;

    move-object/from16 v0, v21

    invoke-virtual {v10, v0}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v21

    const-string/jumbo v22, "usemap"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v21

    if-ltz v21, :cond_6

    .line 269
    :cond_6
    move-object/from16 v0, v19

    instance-of v0, v0, Ljava/lang/String;

    move/from16 v21, v0

    if-eqz v21, :cond_f

    .line 270
    move-object/from16 v0, v19

    check-cast v0, Ljava/lang/String;

    move-object/from16 v18, v0

    .line 272
    .local v18, "stringToken":Ljava/lang/String;
    const-string/jumbo v21, "begincodespacerange"

    move-object/from16 v0, v18

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v21

    if-eqz v21, :cond_9

    .line 275
    const/4 v11, 0x0

    .line 276
    .local v11, "numberOfRanges":I
    if-eqz v16, :cond_7

    .line 277
    invoke-virtual/range {v16 .. v16}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v21

    move/from16 v0, v21

    float-to-int v11, v0

    .line 279
    :cond_7
    const/16 v21, 0x2

    move/from16 v0, v21

    filled-new-array {v11, v0}, [I

    move-result-object v21

    sget-object v22, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    move-object/from16 v0, v22

    move-object/from16 v1, v21

    invoke-static {v0, v1}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v21

    check-cast v21, [[I

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/icepdf/index/core/pobjects/fonts/ofont/CMap;->codeSpaceRange:[[I

    .line 280
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_2
    if-ge v7, v11, :cond_9

    .line 282
    invoke-virtual {v15}, Lorg/icepdf/index/core/util/Parser;->getStreamObject()Ljava/lang/Object;

    move-result-object v19

    .line 283
    move-object/from16 v0, v19

    check-cast v0, Lorg/icepdf/index/core/pobjects/StringObject;

    move-object v6, v0

    .line 284
    .local v6, "hexToken":Lorg/icepdf/index/core/pobjects/StringObject;
    const/16 v21, 0x0

    invoke-interface {v6}, Lorg/icepdf/index/core/pobjects/StringObject;->getLength()I

    move-result v22

    move/from16 v0, v21

    move/from16 v1, v22

    invoke-interface {v6, v0, v1}, Lorg/icepdf/index/core/pobjects/StringObject;->getUnsignedInt(II)I

    move-result v17

    .line 287
    .local v17, "startRange":I
    invoke-virtual {v15}, Lorg/icepdf/index/core/util/Parser;->getStreamObject()Ljava/lang/Object;

    move-result-object v19

    .line 288
    move-object/from16 v0, v19

    check-cast v0, Lorg/icepdf/index/core/pobjects/StringObject;

    move-object v6, v0

    .line 289
    invoke-interface {v6}, Lorg/icepdf/index/core/pobjects/StringObject;->getLength()I

    move-result v9

    .line 290
    .local v9, "length":I
    const/16 v21, 0x0

    move/from16 v0, v21

    invoke-interface {v6, v0, v9}, Lorg/icepdf/index/core/pobjects/StringObject;->getUnsignedInt(II)I

    move-result v5

    .line 291
    .local v5, "endRange":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/CMap;->codeSpaceRange:[[I

    move-object/from16 v21, v0

    aget-object v21, v21, v7

    const/16 v22, 0x0

    aput v17, v21, v22

    .line 292
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/CMap;->codeSpaceRange:[[I

    move-object/from16 v21, v0

    aget-object v21, v21, v7

    const/16 v22, 0x1

    aput v5, v21, v22

    .line 293
    const/16 v21, 0x2

    move/from16 v0, v21

    if-ne v9, v0, :cond_8

    .line 294
    const/16 v21, 0x1

    move/from16 v0, v21

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lorg/icepdf/index/core/pobjects/fonts/ofont/CMap;->oneByte:Z

    .line 280
    :cond_8
    add-int/lit8 v7, v7, 0x1

    goto :goto_2

    .line 299
    .end local v5    # "endRange":I
    .end local v6    # "hexToken":Lorg/icepdf/index/core/pobjects/StringObject;
    .end local v7    # "i":I
    .end local v9    # "length":I
    .end local v11    # "numberOfRanges":I
    .end local v17    # "startRange":I
    :cond_9
    const-string/jumbo v21, "beginbfchar"

    move-object/from16 v0, v18

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v21

    if-eqz v21, :cond_b

    if-eqz v16, :cond_b

    .line 301
    invoke-virtual/range {v16 .. v16}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v21

    move/from16 v0, v21

    float-to-int v12, v0

    .line 303
    .local v12, "numberOfbfChar":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/CMap;->bfChars:Ljava/util/Map;

    move-object/from16 v21, v0

    if-nez v21, :cond_a

    .line 304
    new-instance v21, Ljava/util/HashMap;

    move-object/from16 v0, v21

    invoke-direct {v0, v12}, Ljava/util/HashMap;-><init>(I)V

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/icepdf/index/core/pobjects/fonts/ofont/CMap;->bfChars:Ljava/util/Map;

    .line 307
    :cond_a
    const/4 v7, 0x0

    .restart local v7    # "i":I
    :goto_3
    if-ge v7, v12, :cond_b

    .line 309
    invoke-virtual {v15}, Lorg/icepdf/index/core/util/Parser;->getStreamObject()Ljava/lang/Object;

    move-result-object v19

    .line 310
    move-object/from16 v0, v19

    check-cast v0, Lorg/icepdf/index/core/pobjects/StringObject;

    move-object v6, v0

    .line 311
    .restart local v6    # "hexToken":Lorg/icepdf/index/core/pobjects/StringObject;
    const/16 v21, 0x0

    invoke-interface {v6}, Lorg/icepdf/index/core/pobjects/StringObject;->getLength()I

    move-result v22

    move/from16 v0, v21

    move/from16 v1, v22

    invoke-interface {v6, v0, v1}, Lorg/icepdf/index/core/pobjects/StringObject;->getUnsignedInt(II)I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    .line 314
    .local v8, "key":Ljava/lang/Integer;
    invoke-virtual {v15}, Lorg/icepdf/index/core/util/Parser;->getStreamObject()Ljava/lang/Object;

    move-result-object v19

    .line 315
    move-object/from16 v0, v19

    check-cast v0, Lorg/icepdf/index/core/pobjects/StringObject;

    move-object v6, v0
    :try_end_2
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 316
    const/16 v20, 0x0

    .line 318
    .local v20, "value":[C
    :try_start_3
    invoke-interface {v6}, Lorg/icepdf/index/core/pobjects/StringObject;->getLiteralStringBuffer()Ljava/lang/StringBuffer;

    move-result-object v21

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-direct {v0, v1}, Lorg/icepdf/index/core/pobjects/fonts/ofont/CMap;->convertToString(Ljava/lang/CharSequence;)[C
    :try_end_3
    .catch Ljava/lang/NumberFormatException; {:try_start_3 .. :try_end_3} :catch_6
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v20

    .line 322
    :goto_4
    :try_start_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/CMap;->bfChars:Ljava/util/Map;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    move-object/from16 v1, v20

    invoke-interface {v0, v8, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 307
    add-int/lit8 v7, v7, 0x1

    goto :goto_3

    .line 326
    .end local v6    # "hexToken":Lorg/icepdf/index/core/pobjects/StringObject;
    .end local v7    # "i":I
    .end local v8    # "key":Ljava/lang/Integer;
    .end local v12    # "numberOfbfChar":I
    .end local v20    # "value":[C
    :cond_b
    const-string/jumbo v21, "beginbfrange"

    move-object/from16 v0, v18

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v21

    if-eqz v21, :cond_f

    .line 327
    const/4 v13, 0x0

    .line 328
    .local v13, "numberOfbfRanges":I
    if-eqz v16, :cond_c

    .line 329
    invoke-virtual/range {v16 .. v16}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v21

    move/from16 v0, v21

    float-to-int v13, v0

    .line 331
    :cond_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/CMap;->bfRange:Ljava/util/List;

    move-object/from16 v21, v0

    if-nez v21, :cond_d

    .line 332
    new-instance v21, Ljava/util/ArrayList;

    move-object/from16 v0, v21

    invoke-direct {v0, v13}, Ljava/util/ArrayList;-><init>(I)V

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/icepdf/index/core/pobjects/fonts/ofont/CMap;->bfRange:Ljava/util/List;

    .line 338
    :cond_d
    const/4 v7, 0x0

    .restart local v7    # "i":I
    :goto_5
    if-ge v7, v13, :cond_f

    .line 340
    invoke-virtual {v15}, Lorg/icepdf/index/core/util/Parser;->getStreamObject()Ljava/lang/Object;

    move-result-object v19

    .line 341
    move-object/from16 v0, v19

    instance-of v0, v0, Lorg/icepdf/index/core/pobjects/StringObject;

    move/from16 v21, v0

    if-eqz v21, :cond_f

    .line 342
    move-object/from16 v0, v19

    check-cast v0, Lorg/icepdf/index/core/pobjects/StringObject;

    move-object v6, v0

    .line 343
    .restart local v6    # "hexToken":Lorg/icepdf/index/core/pobjects/StringObject;
    const/16 v21, 0x0

    invoke-interface {v6}, Lorg/icepdf/index/core/pobjects/StringObject;->getLength()I

    move-result v22

    move/from16 v0, v21

    move/from16 v1, v22

    invoke-interface {v6, v0, v1}, Lorg/icepdf/index/core/pobjects/StringObject;->getUnsignedInt(II)I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    .line 349
    .local v17, "startRange":Ljava/lang/Integer;
    invoke-virtual {v15}, Lorg/icepdf/index/core/util/Parser;->getStreamObject()Ljava/lang/Object;

    move-result-object v19

    .line 350
    move-object/from16 v0, v19

    instance-of v0, v0, Lorg/icepdf/index/core/pobjects/StringObject;

    move/from16 v21, v0

    if-eqz v21, :cond_f

    .line 351
    move-object/from16 v0, v19

    check-cast v0, Lorg/icepdf/index/core/pobjects/StringObject;

    move-object v6, v0

    .line 352
    const/16 v21, 0x0

    invoke-interface {v6}, Lorg/icepdf/index/core/pobjects/StringObject;->getLength()I

    move-result v22

    move/from16 v0, v21

    move/from16 v1, v22

    invoke-interface {v6, v0, v1}, Lorg/icepdf/index/core/pobjects/StringObject;->getUnsignedInt(II)I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    .line 359
    .local v5, "endRange":Ljava/lang/Integer;
    invoke-virtual {v15}, Lorg/icepdf/index/core/util/Parser;->getStreamObject()Ljava/lang/Object;

    move-result-object v19

    .line 360
    move-object/from16 v0, v19

    instance-of v0, v0, Ljava/util/Vector;

    move/from16 v21, v0

    if-eqz v21, :cond_e

    .line 361
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/CMap;->bfRange:Ljava/util/List;

    move-object/from16 v22, v0

    new-instance v23, Lorg/icepdf/index/core/pobjects/fonts/ofont/CMap$CMapRange;

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Integer;->intValue()I

    move-result v24

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v25

    move-object/from16 v0, v19

    check-cast v0, Ljava/util/Vector;

    move-object/from16 v21, v0

    move-object/from16 v0, v23

    move-object/from16 v1, p0

    move/from16 v2, v24

    move/from16 v3, v25

    move-object/from16 v4, v21

    invoke-direct {v0, v1, v2, v3, v4}, Lorg/icepdf/index/core/pobjects/fonts/ofont/CMap$CMapRange;-><init>(Lorg/icepdf/index/core/pobjects/fonts/ofont/CMap;IILjava/util/Vector;)V

    invoke-interface/range {v22 .. v23}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 338
    :goto_6
    add-int/lit8 v7, v7, 0x1

    goto :goto_5

    .line 365
    :cond_e
    move-object/from16 v0, v19

    check-cast v0, Lorg/icepdf/index/core/pobjects/StringObject;

    move-object v6, v0

    .line 366
    const/16 v21, 0x0

    invoke-interface {v6}, Lorg/icepdf/index/core/pobjects/StringObject;->getLength()I

    move-result v22

    move/from16 v0, v21

    move/from16 v1, v22

    invoke-interface {v6, v0, v1}, Lorg/icepdf/index/core/pobjects/StringObject;->getUnsignedInt(II)I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    .line 367
    .local v14, "offset":Ljava/lang/Integer;
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/CMap;->bfRange:Ljava/util/List;

    move-object/from16 v21, v0

    new-instance v22, Lorg/icepdf/index/core/pobjects/fonts/ofont/CMap$CMapRange;

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Integer;->intValue()I

    move-result v23

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v24

    invoke-virtual {v14}, Ljava/lang/Integer;->intValue()I

    move-result v25

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    move/from16 v2, v23

    move/from16 v3, v24

    move/from16 v4, v25

    invoke-direct {v0, v1, v2, v3, v4}, Lorg/icepdf/index/core/pobjects/fonts/ofont/CMap$CMapRange;-><init>(Lorg/icepdf/index/core/pobjects/fonts/ofont/CMap;III)V

    invoke-interface/range {v21 .. v22}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_4
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_6

    .line 402
    .end local v5    # "endRange":Ljava/lang/Integer;
    .end local v6    # "hexToken":Lorg/icepdf/index/core/pobjects/StringObject;
    .end local v7    # "i":I
    .end local v10    # "nameString":Ljava/lang/String;
    .end local v13    # "numberOfbfRanges":I
    .end local v14    # "offset":Ljava/lang/Integer;
    .end local v15    # "parser":Lorg/icepdf/index/core/util/Parser;
    .end local v17    # "startRange":Ljava/lang/Integer;
    .end local v18    # "stringToken":Ljava/lang/String;
    .end local v19    # "token":Ljava/lang/Object;
    :catch_0
    move-exception v21

    .line 409
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/CMap;->cMapInputStream:Ljava/io/InputStream;

    move-object/from16 v21, v0

    if-eqz v21, :cond_1

    .line 411
    :try_start_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/CMap;->cMapInputStream:Ljava/io/InputStream;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Ljava/io/InputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1

    goto/16 :goto_1

    .line 413
    :catch_1
    move-exception v21

    goto/16 :goto_1

    .line 399
    .restart local v10    # "nameString":Ljava/lang/String;
    .restart local v15    # "parser":Lorg/icepdf/index/core/util/Parser;
    .restart local v19    # "token":Ljava/lang/Object;
    :cond_f
    move-object/from16 v16, v19

    .line 400
    .local v16, "previousToken":Ljava/lang/Object;
    goto/16 :goto_0

    .line 405
    .end local v10    # "nameString":Ljava/lang/String;
    .end local v15    # "parser":Lorg/icepdf/index/core/util/Parser;
    .end local v16    # "previousToken":Ljava/lang/Object;
    .end local v19    # "token":Ljava/lang/Object;
    :catch_2
    move-exception v21

    .line 409
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/CMap;->cMapInputStream:Ljava/io/InputStream;

    move-object/from16 v21, v0

    if-eqz v21, :cond_1

    .line 411
    :try_start_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/CMap;->cMapInputStream:Ljava/io/InputStream;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Ljava/io/InputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    goto/16 :goto_1

    .line 413
    :catch_3
    move-exception v21

    goto/16 :goto_1

    .line 409
    :catchall_0
    move-exception v21

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/CMap;->cMapInputStream:Ljava/io/InputStream;

    move-object/from16 v22, v0

    if-eqz v22, :cond_10

    .line 411
    :try_start_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/CMap;->cMapInputStream:Ljava/io/InputStream;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Ljava/io/InputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_4

    .line 415
    :cond_10
    :goto_7
    throw v21

    .line 413
    :catch_4
    move-exception v22

    goto :goto_7

    .restart local v15    # "parser":Lorg/icepdf/index/core/util/Parser;
    .restart local v19    # "token":Ljava/lang/Object;
    :catch_5
    move-exception v21

    goto/16 :goto_1

    .line 319
    .restart local v6    # "hexToken":Lorg/icepdf/index/core/pobjects/StringObject;
    .restart local v7    # "i":I
    .restart local v8    # "key":Ljava/lang/Integer;
    .restart local v10    # "nameString":Ljava/lang/String;
    .restart local v12    # "numberOfbfChar":I
    .restart local v18    # "stringToken":Ljava/lang/String;
    .restart local v20    # "value":[C
    :catch_6
    move-exception v21

    goto/16 :goto_4
.end method

.method public isOneByte(I)Z
    .locals 1
    .param p1, "cid"    # I

    .prologue
    .line 155
    iget-boolean v0, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/CMap;->oneByte:Z

    return v0
.end method

.method public toSelector(C)C
    .locals 6
    .param p1, "charMap"    # C

    .prologue
    const/4 v5, 0x0

    .line 460
    iget-object v3, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/CMap;->bfChars:Ljava/util/Map;

    if-eqz v3, :cond_1

    .line 461
    iget-object v3, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/CMap;->bfChars:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [C

    .line 462
    .local v2, "tmp":[C
    if-eqz v2, :cond_1

    .line 463
    aget-char p1, v2, v5

    .line 474
    .end local v2    # "tmp":[C
    .end local p1    # "charMap":C
    :cond_0
    :goto_0
    return p1

    .line 467
    .restart local p1    # "charMap":C
    :cond_1
    iget-object v3, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/CMap;->bfRange:Ljava/util/List;

    if-eqz v3, :cond_0

    .line 468
    iget-object v3, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/CMap;->bfRange:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/CMap$CMapRange;

    .line 469
    .local v0, "aBfRange":Lorg/icepdf/index/core/pobjects/fonts/ofont/CMap$CMapRange;
    invoke-virtual {v0, p1}, Lorg/icepdf/index/core/pobjects/fonts/ofont/CMap$CMapRange;->inRange(I)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 470
    invoke-virtual {v0, p1}, Lorg/icepdf/index/core/pobjects/fonts/ofont/CMap$CMapRange;->getCMapValue(I)[C

    move-result-object v3

    aget-char p1, v3, v5

    goto :goto_0
.end method

.method public toSelector(CZ)C
    .locals 1
    .param p1, "charMap"    # C
    .param p2, "isCFF"    # Z

    .prologue
    .line 478
    invoke-virtual {p0, p1}, Lorg/icepdf/index/core/pobjects/fonts/ofont/CMap;->toSelector(C)C

    move-result v0

    return v0
.end method

.method public toUnicode(C)Ljava/lang/String;
    .locals 5
    .param p1, "ch"    # C

    .prologue
    .line 422
    iget-object v3, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/CMap;->bfChars:Ljava/util/Map;

    if-eqz v3, :cond_0

    .line 423
    iget-object v3, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/CMap;->bfChars:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [C

    .line 424
    .local v2, "tmp":[C
    if-eqz v2, :cond_0

    .line 425
    invoke-static {v2}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v3

    .line 436
    .end local v2    # "tmp":[C
    :goto_0
    return-object v3

    .line 429
    :cond_0
    iget-object v3, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/CMap;->bfRange:Ljava/util/List;

    if-eqz v3, :cond_2

    .line 430
    iget-object v3, p0, Lorg/icepdf/index/core/pobjects/fonts/ofont/CMap;->bfRange:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/icepdf/index/core/pobjects/fonts/ofont/CMap$CMapRange;

    .line 431
    .local v0, "aBfRange":Lorg/icepdf/index/core/pobjects/fonts/ofont/CMap$CMapRange;
    invoke-virtual {v0, p1}, Lorg/icepdf/index/core/pobjects/fonts/ofont/CMap$CMapRange;->inRange(I)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 432
    invoke-virtual {v0, p1}, Lorg/icepdf/index/core/pobjects/fonts/ofont/CMap$CMapRange;->getCMapValue(I)[C

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 436
    .end local v0    # "aBfRange":Lorg/icepdf/index/core/pobjects/fonts/ofont/CMap$CMapRange;
    .end local v1    # "i$":Ljava/util/Iterator;
    :cond_2
    invoke-static {p1}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v3

    goto :goto_0
.end method
