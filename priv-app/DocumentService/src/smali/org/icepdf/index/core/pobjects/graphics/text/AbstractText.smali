.class abstract Lorg/icepdf/index/core/pobjects/graphics/text/AbstractText;
.super Ljava/lang/Object;
.source "AbstractText.java"

# interfaces
.implements Lorg/icepdf/index/core/pobjects/graphics/text/Text;


# instance fields
.field protected bounds:Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;

.field protected hasSelected:Z


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public clearBounds()V
    .locals 1

    .prologue
    .line 56
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/icepdf/index/core/pobjects/graphics/text/AbstractText;->bounds:Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;

    .line 57
    return-void
.end method

.method public abstract getBounds()Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;
.end method

.method public setHasSelected(Z)V
    .locals 0
    .param p1, "hasSelected"    # Z

    .prologue
    .line 61
    iput-boolean p1, p0, Lorg/icepdf/index/core/pobjects/graphics/text/AbstractText;->hasSelected:Z

    .line 62
    return-void
.end method
