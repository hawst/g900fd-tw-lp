.class public interface abstract Lorg/icepdf/index/core/pobjects/StringObject;
.super Ljava/lang/Object;
.source "StringObject.java"


# virtual methods
.method public abstract getDecryptedLiteralString(Lorg/icepdf/index/core/pobjects/security/SecurityManager;)Ljava/lang/String;
.end method

.method public abstract getHexString()Ljava/lang/String;
.end method

.method public abstract getHexStringBuffer()Ljava/lang/StringBuffer;
.end method

.method public abstract getLength()I
.end method

.method public abstract getLiteralString()Ljava/lang/String;
.end method

.method public abstract getLiteralStringBuffer()Ljava/lang/StringBuffer;
.end method

.method public abstract getLiteralStringBuffer(ILorg/icepdf/index/core/pobjects/fonts/FontFile;)Ljava/lang/StringBuffer;
.end method

.method public abstract getUnsignedInt(II)I
.end method

.method public abstract setReference(Lorg/icepdf/index/core/pobjects/Reference;)V
.end method

.method public abstract toString()Ljava/lang/String;
.end method
