.class public Lorg/icepdf/index/core/util/LazyObjectLoader;
.super Ljava/lang/Object;
.source "LazyObjectLoader.java"

# interfaces
.implements Lorg/icepdf/index/core/util/MemoryManagerDelegate;


# instance fields
.field protected leastRecentlyUsed:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lorg/icepdf/index/core/pobjects/ObjectStream;",
            ">;"
        }
    .end annotation
.end field

.field private final leastRectlyUsedLock:Ljava/lang/Object;

.field private library:Lorg/icepdf/index/core/util/Library;

.field private m_CrossReference:Lorg/icepdf/index/core/pobjects/CrossReference;

.field private m_SeekableInput:Lorg/icepdf/index/core/io/SeekableInput;


# direct methods
.method public constructor <init>(Lorg/icepdf/index/core/util/Library;Lorg/icepdf/index/core/io/SeekableInput;Lorg/icepdf/index/core/pobjects/CrossReference;)V
    .locals 2
    .param p1, "lib"    # Lorg/icepdf/index/core/util/Library;
    .param p2, "seekableInput"    # Lorg/icepdf/index/core/io/SeekableInput;
    .param p3, "xref"    # Lorg/icepdf/index/core/pobjects/CrossReference;

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lorg/icepdf/index/core/util/LazyObjectLoader;->leastRectlyUsedLock:Ljava/lang/Object;

    .line 38
    iput-object p1, p0, Lorg/icepdf/index/core/util/LazyObjectLoader;->library:Lorg/icepdf/index/core/util/Library;

    .line 39
    iput-object p2, p0, Lorg/icepdf/index/core/util/LazyObjectLoader;->m_SeekableInput:Lorg/icepdf/index/core/io/SeekableInput;

    .line 40
    iput-object p3, p0, Lorg/icepdf/index/core/util/LazyObjectLoader;->m_CrossReference:Lorg/icepdf/index/core/pobjects/CrossReference;

    .line 41
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0x100

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lorg/icepdf/index/core/util/LazyObjectLoader;->leastRecentlyUsed:Ljava/util/ArrayList;

    .line 42
    return-void
.end method


# virtual methods
.method public dispose()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 133
    iput-object v1, p0, Lorg/icepdf/index/core/util/LazyObjectLoader;->library:Lorg/icepdf/index/core/util/Library;

    .line 134
    iput-object v1, p0, Lorg/icepdf/index/core/util/LazyObjectLoader;->m_SeekableInput:Lorg/icepdf/index/core/io/SeekableInput;

    .line 135
    iput-object v1, p0, Lorg/icepdf/index/core/util/LazyObjectLoader;->m_CrossReference:Lorg/icepdf/index/core/pobjects/CrossReference;

    .line 136
    iget-object v0, p0, Lorg/icepdf/index/core/util/LazyObjectLoader;->leastRecentlyUsed:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 137
    iget-object v0, p0, Lorg/icepdf/index/core/util/LazyObjectLoader;->leastRecentlyUsed:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 138
    iput-object v1, p0, Lorg/icepdf/index/core/util/LazyObjectLoader;->leastRecentlyUsed:Ljava/util/ArrayList;

    .line 140
    :cond_0
    return-void
.end method

.method public getLibrary()Lorg/icepdf/index/core/util/Library;
    .locals 1

    .prologue
    .line 175
    iget-object v0, p0, Lorg/icepdf/index/core/util/LazyObjectLoader;->library:Lorg/icepdf/index/core/util/Library;

    return-object v0
.end method

.method public haveEntry(Lorg/icepdf/index/core/pobjects/Reference;)Z
    .locals 5
    .param p1, "reference"    # Lorg/icepdf/index/core/pobjects/Reference;

    .prologue
    const/4 v2, 0x0

    .line 99
    if-eqz p1, :cond_0

    iget-object v3, p0, Lorg/icepdf/index/core/util/LazyObjectLoader;->m_CrossReference:Lorg/icepdf/index/core/pobjects/CrossReference;

    if-nez v3, :cond_1

    .line 103
    :cond_0
    :goto_0
    return v2

    .line 101
    :cond_1
    invoke-virtual {p1}, Lorg/icepdf/index/core/pobjects/Reference;->getObjectNumber()I

    move-result v1

    .line 102
    .local v1, "objNum":I
    iget-object v3, p0, Lorg/icepdf/index/core/util/LazyObjectLoader;->m_CrossReference:Lorg/icepdf/index/core/pobjects/CrossReference;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Lorg/icepdf/index/core/pobjects/CrossReference;->getEntryForObject(Ljava/lang/Integer;)Lorg/icepdf/index/core/pobjects/CrossReference$Entry;

    move-result-object v0

    .line 103
    .local v0, "entry":Lorg/icepdf/index/core/pobjects/CrossReference$Entry;
    if-eqz v0, :cond_0

    const/4 v2, 0x1

    goto :goto_0
.end method

.method public loadObject(Lorg/icepdf/index/core/pobjects/Reference;)Z
    .locals 19
    .param p1, "reference"    # Lorg/icepdf/index/core/pobjects/Reference;

    .prologue
    .line 45
    if-eqz p1, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/util/LazyObjectLoader;->library:Lorg/icepdf/index/core/util/Library;

    move-object/from16 v17, v0

    if-eqz v17, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/util/LazyObjectLoader;->m_CrossReference:Lorg/icepdf/index/core/pobjects/CrossReference;

    move-object/from16 v17, v0

    if-nez v17, :cond_2

    .line 46
    :cond_0
    const/4 v4, 0x0

    .line 95
    :cond_1
    :goto_0
    return v4

    .line 47
    :cond_2
    invoke-virtual/range {p1 .. p1}, Lorg/icepdf/index/core/pobjects/Reference;->getObjectNumber()I

    move-result v6

    .line 48
    .local v6, "objNum":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/util/LazyObjectLoader;->m_CrossReference:Lorg/icepdf/index/core/pobjects/CrossReference;

    move-object/from16 v17, v0

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Lorg/icepdf/index/core/pobjects/CrossReference;->getEntryForObject(Ljava/lang/Integer;)Lorg/icepdf/index/core/pobjects/CrossReference$Entry;

    move-result-object v3

    .line 49
    .local v3, "entry":Lorg/icepdf/index/core/pobjects/CrossReference$Entry;
    if-nez v3, :cond_3

    .line 50
    const/4 v4, 0x0

    goto :goto_0

    .line 51
    :cond_3
    const/4 v4, 0x0

    .line 52
    .local v4, "gotSomething":Z
    instance-of v0, v3, Lorg/icepdf/index/core/pobjects/CrossReference$UsedEntry;

    move/from16 v17, v0

    if-eqz v17, :cond_7

    .line 54
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/util/LazyObjectLoader;->m_SeekableInput:Lorg/icepdf/index/core/io/SeekableInput;

    move-object/from16 v17, v0

    if-eqz v17, :cond_4

    .line 55
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/util/LazyObjectLoader;->m_SeekableInput:Lorg/icepdf/index/core/io/SeekableInput;

    move-object/from16 v17, v0

    invoke-interface/range {v17 .. v17}, Lorg/icepdf/index/core/io/SeekableInput;->beginThreadAccess()V

    .line 56
    move-object v0, v3

    check-cast v0, Lorg/icepdf/index/core/pobjects/CrossReference$UsedEntry;

    move-object/from16 v16, v0

    .line 57
    .local v16, "usedEntry":Lorg/icepdf/index/core/pobjects/CrossReference$UsedEntry;
    invoke-virtual/range {v16 .. v16}, Lorg/icepdf/index/core/pobjects/CrossReference$UsedEntry;->getFilePositionOfObject()J

    move-result-wide v12

    .line 58
    .local v12, "position":J
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/util/LazyObjectLoader;->m_SeekableInput:Lorg/icepdf/index/core/io/SeekableInput;

    move-object/from16 v17, v0

    invoke-interface/range {v17 .. v17}, Lorg/icepdf/index/core/io/SeekableInput;->getAbsolutePosition()J

    move-result-wide v14

    .line 59
    .local v14, "savedPosition":J
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/util/LazyObjectLoader;->m_SeekableInput:Lorg/icepdf/index/core/io/SeekableInput;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-interface {v0, v12, v13}, Lorg/icepdf/index/core/io/SeekableInput;->seekAbsolute(J)V

    .line 60
    new-instance v11, Lorg/icepdf/index/core/util/Parser;

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/util/LazyObjectLoader;->m_SeekableInput:Lorg/icepdf/index/core/io/SeekableInput;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-direct {v11, v0}, Lorg/icepdf/index/core/util/Parser;-><init>(Lorg/icepdf/index/core/io/SeekableInput;)V

    .line 61
    .local v11, "parser":Lorg/icepdf/index/core/util/Parser;
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/util/LazyObjectLoader;->library:Lorg/icepdf/index/core/util/Library;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v11, v0}, Lorg/icepdf/index/core/util/Parser;->getObject(Lorg/icepdf/index/core/util/Library;)Ljava/lang/Object;

    move-result-object v5

    .line 62
    .local v5, "ob":Ljava/lang/Object;
    if-eqz v5, :cond_5

    const/4 v4, 0x1

    .line 63
    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/util/LazyObjectLoader;->m_SeekableInput:Lorg/icepdf/index/core/io/SeekableInput;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-interface {v0, v14, v15}, Lorg/icepdf/index/core/io/SeekableInput;->seekAbsolute(J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 71
    .end local v5    # "ob":Ljava/lang/Object;
    .end local v11    # "parser":Lorg/icepdf/index/core/util/Parser;
    .end local v12    # "position":J
    .end local v14    # "savedPosition":J
    .end local v16    # "usedEntry":Lorg/icepdf/index/core/pobjects/CrossReference$UsedEntry;
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/util/LazyObjectLoader;->m_SeekableInput:Lorg/icepdf/index/core/io/SeekableInput;

    move-object/from16 v17, v0

    if-eqz v17, :cond_1

    .line 72
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/util/LazyObjectLoader;->m_SeekableInput:Lorg/icepdf/index/core/io/SeekableInput;

    move-object/from16 v17, v0

    invoke-interface/range {v17 .. v17}, Lorg/icepdf/index/core/io/SeekableInput;->endThreadAccess()V

    goto/16 :goto_0

    .line 62
    .restart local v5    # "ob":Ljava/lang/Object;
    .restart local v11    # "parser":Lorg/icepdf/index/core/util/Parser;
    .restart local v12    # "position":J
    .restart local v14    # "savedPosition":J
    .restart local v16    # "usedEntry":Lorg/icepdf/index/core/pobjects/CrossReference$UsedEntry;
    :cond_5
    const/4 v4, 0x0

    goto :goto_1

    .line 66
    .end local v5    # "ob":Ljava/lang/Object;
    .end local v11    # "parser":Lorg/icepdf/index/core/util/Parser;
    .end local v12    # "position":J
    .end local v14    # "savedPosition":J
    .end local v16    # "usedEntry":Lorg/icepdf/index/core/pobjects/CrossReference$UsedEntry;
    :catch_0
    move-exception v17

    .line 71
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/util/LazyObjectLoader;->m_SeekableInput:Lorg/icepdf/index/core/io/SeekableInput;

    move-object/from16 v17, v0

    if-eqz v17, :cond_1

    .line 72
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/util/LazyObjectLoader;->m_SeekableInput:Lorg/icepdf/index/core/io/SeekableInput;

    move-object/from16 v17, v0

    invoke-interface/range {v17 .. v17}, Lorg/icepdf/index/core/io/SeekableInput;->endThreadAccess()V

    goto/16 :goto_0

    .line 71
    :catchall_0
    move-exception v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/util/LazyObjectLoader;->m_SeekableInput:Lorg/icepdf/index/core/io/SeekableInput;

    move-object/from16 v18, v0

    if-eqz v18, :cond_6

    .line 72
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/util/LazyObjectLoader;->m_SeekableInput:Lorg/icepdf/index/core/io/SeekableInput;

    move-object/from16 v18, v0

    invoke-interface/range {v18 .. v18}, Lorg/icepdf/index/core/io/SeekableInput;->endThreadAccess()V

    :cond_6
    throw v17

    .line 74
    :cond_7
    instance-of v0, v3, Lorg/icepdf/index/core/pobjects/CrossReference$CompressedEntry;

    move/from16 v17, v0

    if-eqz v17, :cond_1

    .line 76
    :try_start_1
    move-object v0, v3

    check-cast v0, Lorg/icepdf/index/core/pobjects/CrossReference$CompressedEntry;

    move-object v2, v0

    .line 77
    .local v2, "compressedEntry":Lorg/icepdf/index/core/pobjects/CrossReference$CompressedEntry;
    invoke-virtual {v2}, Lorg/icepdf/index/core/pobjects/CrossReference$CompressedEntry;->getObjectNumberOfContainingObjectStream()I

    move-result v10

    .line 78
    .local v10, "objectStreamsObjectNumber":I
    invoke-virtual {v2}, Lorg/icepdf/index/core/pobjects/CrossReference$CompressedEntry;->getIndexWithinObjectStream()I

    move-result v7

    .line 79
    .local v7, "objectIndex":I
    new-instance v9, Lorg/icepdf/index/core/pobjects/Reference;

    const/16 v17, 0x0

    move/from16 v0, v17

    invoke-direct {v9, v10, v0}, Lorg/icepdf/index/core/pobjects/Reference;-><init>(II)V

    .line 80
    .local v9, "objectStreamRef":Lorg/icepdf/index/core/pobjects/Reference;
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/util/LazyObjectLoader;->library:Lorg/icepdf/index/core/util/Library;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v9}, Lorg/icepdf/index/core/util/Library;->getObject(Lorg/icepdf/index/core/pobjects/Reference;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lorg/icepdf/index/core/pobjects/ObjectStream;

    .line 81
    .local v8, "objectStream":Lorg/icepdf/index/core/pobjects/ObjectStream;
    if-eqz v8, :cond_1

    .line 82
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/util/LazyObjectLoader;->leastRectlyUsedLock:Ljava/lang/Object;

    move-object/from16 v18, v0

    monitor-enter v18
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 83
    :try_start_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/util/LazyObjectLoader;->leastRecentlyUsed:Ljava/util/ArrayList;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 84
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/util/LazyObjectLoader;->leastRecentlyUsed:Ljava/util/ArrayList;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 85
    monitor-exit v18
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 87
    :try_start_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/icepdf/index/core/util/LazyObjectLoader;->library:Lorg/icepdf/index/core/util/Library;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v8, v0, v7}, Lorg/icepdf/index/core/pobjects/ObjectStream;->loadObject(Lorg/icepdf/index/core/util/Library;I)Z
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    move-result v4

    goto/16 :goto_0

    .line 85
    :catchall_1
    move-exception v17

    :try_start_4
    monitor-exit v18
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :try_start_5
    throw v17
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1

    .line 90
    .end local v2    # "compressedEntry":Lorg/icepdf/index/core/pobjects/CrossReference$CompressedEntry;
    .end local v7    # "objectIndex":I
    .end local v8    # "objectStream":Lorg/icepdf/index/core/pobjects/ObjectStream;
    .end local v9    # "objectStreamRef":Lorg/icepdf/index/core/pobjects/Reference;
    .end local v10    # "objectStreamsObjectNumber":I
    :catch_1
    move-exception v17

    goto/16 :goto_0
.end method

.method public loadTrailer(J)Lorg/icepdf/index/core/pobjects/PTrailer;
    .locals 9
    .param p1, "position"    # J

    .prologue
    .line 107
    const/4 v6, 0x0

    .line 109
    .local v6, "trailer":Lorg/icepdf/index/core/pobjects/PTrailer;
    :try_start_0
    iget-object v7, p0, Lorg/icepdf/index/core/util/LazyObjectLoader;->m_SeekableInput:Lorg/icepdf/index/core/io/SeekableInput;

    if-eqz v7, :cond_1

    .line 110
    iget-object v7, p0, Lorg/icepdf/index/core/util/LazyObjectLoader;->m_SeekableInput:Lorg/icepdf/index/core/io/SeekableInput;

    invoke-interface {v7}, Lorg/icepdf/index/core/io/SeekableInput;->beginThreadAccess()V

    .line 111
    iget-object v7, p0, Lorg/icepdf/index/core/util/LazyObjectLoader;->m_SeekableInput:Lorg/icepdf/index/core/io/SeekableInput;

    invoke-interface {v7}, Lorg/icepdf/index/core/io/SeekableInput;->getAbsolutePosition()J

    move-result-wide v4

    .line 112
    .local v4, "savedPosition":J
    iget-object v7, p0, Lorg/icepdf/index/core/util/LazyObjectLoader;->m_SeekableInput:Lorg/icepdf/index/core/io/SeekableInput;

    invoke-interface {v7, p1, p2}, Lorg/icepdf/index/core/io/SeekableInput;->seekAbsolute(J)V

    .line 113
    new-instance v3, Lorg/icepdf/index/core/util/Parser;

    iget-object v7, p0, Lorg/icepdf/index/core/util/LazyObjectLoader;->m_SeekableInput:Lorg/icepdf/index/core/io/SeekableInput;

    invoke-direct {v3, v7}, Lorg/icepdf/index/core/util/Parser;-><init>(Lorg/icepdf/index/core/io/SeekableInput;)V

    .line 114
    .local v3, "parser":Lorg/icepdf/index/core/util/Parser;
    iget-object v7, p0, Lorg/icepdf/index/core/util/LazyObjectLoader;->library:Lorg/icepdf/index/core/util/Library;

    invoke-virtual {v3, v7}, Lorg/icepdf/index/core/util/Parser;->getObject(Lorg/icepdf/index/core/util/Library;)Ljava/lang/Object;

    move-result-object v2

    .line 115
    .local v2, "obj":Ljava/lang/Object;
    instance-of v7, v2, Lorg/icepdf/index/core/pobjects/PObject;

    if-eqz v7, :cond_0

    .line 116
    check-cast v2, Lorg/icepdf/index/core/pobjects/PObject;

    .end local v2    # "obj":Ljava/lang/Object;
    invoke-virtual {v2}, Lorg/icepdf/index/core/pobjects/PObject;->getObject()Ljava/lang/Object;

    move-result-object v2

    .line 117
    .restart local v2    # "obj":Ljava/lang/Object;
    :cond_0
    move-object v0, v2

    check-cast v0, Lorg/icepdf/index/core/pobjects/PTrailer;

    move-object v6, v0

    .line 118
    iget-object v7, p0, Lorg/icepdf/index/core/util/LazyObjectLoader;->m_SeekableInput:Lorg/icepdf/index/core/io/SeekableInput;

    invoke-interface {v7, v4, v5}, Lorg/icepdf/index/core/io/SeekableInput;->seekAbsolute(J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 126
    .end local v2    # "obj":Ljava/lang/Object;
    .end local v3    # "parser":Lorg/icepdf/index/core/util/Parser;
    .end local v4    # "savedPosition":J
    :cond_1
    iget-object v7, p0, Lorg/icepdf/index/core/util/LazyObjectLoader;->m_SeekableInput:Lorg/icepdf/index/core/io/SeekableInput;

    if-eqz v7, :cond_2

    .line 127
    iget-object v7, p0, Lorg/icepdf/index/core/util/LazyObjectLoader;->m_SeekableInput:Lorg/icepdf/index/core/io/SeekableInput;

    invoke-interface {v7}, Lorg/icepdf/index/core/io/SeekableInput;->endThreadAccess()V

    .line 129
    :cond_2
    :goto_0
    return-object v6

    .line 121
    :catch_0
    move-exception v7

    .line 126
    iget-object v7, p0, Lorg/icepdf/index/core/util/LazyObjectLoader;->m_SeekableInput:Lorg/icepdf/index/core/io/SeekableInput;

    if-eqz v7, :cond_2

    .line 127
    iget-object v7, p0, Lorg/icepdf/index/core/util/LazyObjectLoader;->m_SeekableInput:Lorg/icepdf/index/core/io/SeekableInput;

    invoke-interface {v7}, Lorg/icepdf/index/core/io/SeekableInput;->endThreadAccess()V

    goto :goto_0

    .line 126
    :catchall_0
    move-exception v7

    iget-object v8, p0, Lorg/icepdf/index/core/util/LazyObjectLoader;->m_SeekableInput:Lorg/icepdf/index/core/io/SeekableInput;

    if-eqz v8, :cond_3

    .line 127
    iget-object v8, p0, Lorg/icepdf/index/core/util/LazyObjectLoader;->m_SeekableInput:Lorg/icepdf/index/core/io/SeekableInput;

    invoke-interface {v8}, Lorg/icepdf/index/core/io/SeekableInput;->endThreadAccess()V

    :cond_3
    throw v7
.end method

.method public reduceMemory(I)Z
    .locals 9
    .param p1, "reductionPolicy"    # I

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 148
    const/4 v2, 0x0

    .line 149
    .local v2, "numToDo":I
    iget-object v6, p0, Lorg/icepdf/index/core/util/LazyObjectLoader;->leastRectlyUsedLock:Ljava/lang/Object;

    monitor-enter v6

    .line 150
    :try_start_0
    iget-object v7, p0, Lorg/icepdf/index/core/util/LazyObjectLoader;->leastRecentlyUsed:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 151
    .local v1, "lruSize":I
    if-ne p1, v4, :cond_2

    .line 152
    mul-int/lit8 v7, v1, 0x4b

    div-int/lit8 v2, v7, 0x64

    .line 160
    :cond_0
    :goto_0
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    if-ge v0, v2, :cond_4

    .line 161
    iget-object v7, p0, Lorg/icepdf/index/core/util/LazyObjectLoader;->leastRecentlyUsed:Ljava/util/ArrayList;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/icepdf/index/core/pobjects/ObjectStream;

    .line 162
    .local v3, "objStm":Lorg/icepdf/index/core/pobjects/ObjectStream;
    if-eqz v3, :cond_1

    .line 163
    const/4 v7, 0x1

    invoke-virtual {v3, v7}, Lorg/icepdf/index/core/pobjects/ObjectStream;->dispose(Z)V

    .line 160
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 153
    .end local v0    # "i":I
    .end local v3    # "objStm":Lorg/icepdf/index/core/pobjects/ObjectStream;
    :cond_2
    if-nez p1, :cond_0

    .line 154
    const/4 v7, 0x5

    if-le v1, v7, :cond_3

    .line 155
    mul-int/lit8 v7, v1, 0x32

    div-int/lit8 v2, v7, 0x64

    goto :goto_0

    .line 156
    :cond_3
    if-lez v1, :cond_0

    .line 157
    const/4 v2, 0x1

    goto :goto_0

    .line 165
    .restart local v0    # "i":I
    :cond_4
    monitor-exit v6

    .line 166
    if-lez v2, :cond_5

    :goto_2
    return v4

    .line 165
    .end local v0    # "i":I
    .end local v1    # "lruSize":I
    :catchall_0
    move-exception v4

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4

    .restart local v0    # "i":I
    .restart local v1    # "lruSize":I
    :cond_5
    move v4, v5

    .line 166
    goto :goto_2
.end method
