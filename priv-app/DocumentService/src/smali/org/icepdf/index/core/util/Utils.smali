.class public Lorg/icepdf/index/core/util/Utils;
.super Ljava/lang/Object;
.source "Utils.java"


# static fields
.field private static lastMemUsed:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 311
    const-wide/16 v0, 0x0

    sput-wide v0, Lorg/icepdf/index/core/util/Utils;->lastMemUsed:J

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static convertByteArrayToByteString([B)Ljava/lang/String;
    .locals 5
    .param p0, "bytes"    # [B

    .prologue
    .line 302
    array-length v2, p0

    .line 303
    .local v2, "max":I
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 304
    .local v3, "sb":Ljava/lang/StringBuilder;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v2, :cond_0

    .line 305
    aget-byte v4, p0, v1

    and-int/lit16 v0, v4, 0xff

    .line 306
    .local v0, "b":I
    int-to-char v4, v0

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 304
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 308
    .end local v0    # "b":I
    :cond_0
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4
.end method

.method public static convertByteArrayToHexString([BIIZIC)Ljava/lang/String;
    .locals 10
    .param p0, "buffer"    # [B
    .param p1, "offset"    # I
    .param p2, "length"    # I
    .param p3, "addSpaceSeparator"    # Z
    .param p4, "addDelimiterEverNBytes"    # I
    .param p5, "delimiter"    # C

    .prologue
    const/4 v9, 0x2

    .line 112
    if-eqz p3, :cond_1

    const/4 v8, 0x3

    :goto_0
    mul-int v5, p2, v8

    .line 113
    .local v5, "presize":I
    if-lez p4, :cond_0

    .line 114
    div-int v8, p2, p4

    add-int/2addr v5, v8

    .line 115
    :cond_0
    new-instance v7, Ljava/lang/StringBuffer;

    invoke-direct {v7, v5}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 116
    .local v7, "sb":Ljava/lang/StringBuffer;
    const/4 v1, 0x0

    .line 117
    .local v1, "delimiterCount":I
    add-int v2, p1, p2

    .line 118
    .local v2, "end":I
    move v4, p1

    .local v4, "index":I
    :goto_1
    if-ge v4, v2, :cond_5

    .line 119
    const/4 v0, 0x0

    .line 120
    .local v0, "currValue":I
    aget-byte v8, p0, v4

    and-int/lit16 v8, v8, 0xff

    or-int/2addr v0, v8

    .line 121
    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v6

    .line 122
    .local v6, "s":Ljava/lang/String;
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v3

    .local v3, "i":I
    :goto_2
    if-ge v3, v9, :cond_2

    .line 123
    const/16 v8, 0x30

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 122
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .end local v0    # "currValue":I
    .end local v1    # "delimiterCount":I
    .end local v2    # "end":I
    .end local v3    # "i":I
    .end local v4    # "index":I
    .end local v5    # "presize":I
    .end local v6    # "s":Ljava/lang/String;
    .end local v7    # "sb":Ljava/lang/StringBuffer;
    :cond_1
    move v8, v9

    .line 112
    goto :goto_0

    .line 124
    .restart local v0    # "currValue":I
    .restart local v1    # "delimiterCount":I
    .restart local v2    # "end":I
    .restart local v3    # "i":I
    .restart local v4    # "index":I
    .restart local v5    # "presize":I
    .restart local v6    # "s":Ljava/lang/String;
    .restart local v7    # "sb":Ljava/lang/StringBuffer;
    :cond_2
    invoke-virtual {v7, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 125
    if-eqz p3, :cond_3

    .line 126
    const/16 v8, 0x20

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 127
    :cond_3
    add-int/lit8 v1, v1, 0x1

    .line 128
    if-lez p4, :cond_4

    if-ne v1, p4, :cond_4

    .line 129
    const/4 v1, 0x0

    .line 130
    invoke-virtual {v7, p5}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 118
    :cond_4
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 133
    .end local v0    # "currValue":I
    .end local v3    # "i":I
    .end local v6    # "s":Ljava/lang/String;
    :cond_5
    invoke-virtual {v7}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v8

    return-object v8
.end method

.method public static convertByteArrayToHexString([BZ)Ljava/lang/String;
    .locals 6
    .param p0, "buffer"    # [B
    .param p1, "addSpaceSeparator"    # Z

    .prologue
    const/4 v1, 0x0

    .line 100
    array-length v2, p0

    const/4 v4, -0x1

    move-object v0, p0

    move v3, p1

    move v5, v1

    invoke-static/range {v0 .. v5}, Lorg/icepdf/index/core/util/Utils;->convertByteArrayToHexString([BIIZIC)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static convertByteArrayToHexString([BZIC)Ljava/lang/String;
    .locals 6
    .param p0, "buffer"    # [B
    .param p1, "addSpaceSeparator"    # Z
    .param p2, "addDelimiterEverNBytes"    # I
    .param p3, "delimiter"    # C

    .prologue
    .line 105
    const/4 v1, 0x0

    array-length v2, p0

    move-object v0, p0

    move v3, p1

    move v4, p2

    move v5, p3

    invoke-static/range {v0 .. v5}, Lorg/icepdf/index/core/util/Utils;->convertByteArrayToHexString([BIIZIC)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static convertByteCharSequenceToByteArray(Ljava/lang/CharSequence;)[B
    .locals 4
    .param p0, "string"    # Ljava/lang/CharSequence;

    .prologue
    .line 291
    if-nez p0, :cond_1

    .line 292
    const/4 v0, 0x0

    .line 298
    :cond_0
    return-object v0

    .line 293
    :cond_1
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v2

    .line 294
    .local v2, "max":I
    new-array v0, v2, [B

    .line 295
    .local v0, "bytes":[B
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v2, :cond_0

    .line 296
    invoke-interface {p0, v1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v3

    int-to-byte v3, v3

    aput-byte v3, v0, v1

    .line 295
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public static getContentAndReplaceInputStream([Ljava/io/InputStream;Z)Ljava/lang/String;
    .locals 9
    .param p0, "inArray"    # [Ljava/io/InputStream;
    .param p1, "convertToHex"    # Z

    .prologue
    .line 164
    const/4 v1, 0x0

    .line 166
    .local v1, "content":Ljava/lang/String;
    :try_start_0
    new-instance v5, Ljava/io/ByteArrayOutputStream;

    const/16 v7, 0x400

    invoke-direct {v5, v7}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    .line 167
    .local v5, "out":Ljava/io/ByteArrayOutputStream;
    const/4 v7, 0x0

    aget-object v4, p0, v7

    .line 169
    .local v4, "in":Ljava/io/InputStream;
    const/16 v7, 0x400

    new-array v0, v7, [B

    .line 171
    .local v0, "buf":[B
    :goto_0
    const/4 v7, 0x0

    array-length v8, v0

    invoke-virtual {v4, v0, v7, v8}, Ljava/io/InputStream;->read([BII)I

    move-result v6

    .line 172
    .local v6, "read":I
    if-gez v6, :cond_1

    .line 184
    instance-of v7, v4, Lorg/icepdf/index/core/io/SeekableInput;

    if-nez v7, :cond_0

    .line 185
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V

    .line 186
    :cond_0
    invoke-virtual {v5}, Ljava/io/ByteArrayOutputStream;->flush()V

    .line 187
    invoke-virtual {v5}, Ljava/io/ByteArrayOutputStream;->close()V

    .line 188
    invoke-virtual {v5}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v3

    .line 189
    .local v3, "data":[B
    const/4 v5, 0x0

    .line 190
    const/4 v7, 0x0

    new-instance v8, Ljava/io/ByteArrayInputStream;

    invoke-direct {v8, v3}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    aput-object v8, p0, v7

    .line 191
    if-eqz p1, :cond_2

    .line 192
    const/4 v7, 0x1

    invoke-static {v3, v7}, Lorg/icepdf/index/core/util/Utils;->convertByteArrayToHexString([BZ)Ljava/lang/String;

    move-result-object v1

    .line 202
    .end local v0    # "buf":[B
    .end local v3    # "data":[B
    .end local v4    # "in":Ljava/io/InputStream;
    .end local v5    # "out":Ljava/io/ByteArrayOutputStream;
    .end local v6    # "read":I
    :goto_1
    return-object v1

    .line 174
    .restart local v0    # "buf":[B
    .restart local v4    # "in":Ljava/io/InputStream;
    .restart local v5    # "out":Ljava/io/ByteArrayOutputStream;
    .restart local v6    # "read":I
    :cond_1
    const/4 v7, 0x0

    invoke-virtual {v5, v0, v7, v6}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    goto :goto_0

    .line 196
    .end local v0    # "buf":[B
    .end local v4    # "in":Ljava/io/InputStream;
    .end local v5    # "out":Ljava/io/ByteArrayOutputStream;
    .end local v6    # "read":I
    :catch_0
    move-exception v7

    goto :goto_1

    .line 194
    .restart local v0    # "buf":[B
    .restart local v3    # "data":[B
    .restart local v4    # "in":Ljava/io/InputStream;
    .restart local v5    # "out":Ljava/io/ByteArrayOutputStream;
    .restart local v6    # "read":I
    :cond_2
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v3}, Ljava/lang/String;-><init>([B)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1

    .end local v1    # "content":Ljava/lang/String;
    .local v2, "content":Ljava/lang/String;
    move-object v1, v2

    .end local v2    # "content":Ljava/lang/String;
    .restart local v1    # "content":Ljava/lang/String;
    goto :goto_1

    .line 199
    .end local v0    # "buf":[B
    .end local v3    # "data":[B
    .end local v4    # "in":Ljava/io/InputStream;
    .end local v5    # "out":Ljava/io/ByteArrayOutputStream;
    .end local v6    # "read":I
    :catch_1
    move-exception v7

    goto :goto_1
.end method

.method public static getContentFromSeekableInput(Lorg/icepdf/index/core/io/SeekableInput;Z)Ljava/lang/String;
    .locals 8
    .param p0, "in"    # Lorg/icepdf/index/core/io/SeekableInput;
    .param p1, "convertToHex"    # Z

    .prologue
    .line 206
    const/4 v0, 0x0

    .line 208
    .local v0, "content":Ljava/lang/String;
    :try_start_0
    invoke-interface {p0}, Lorg/icepdf/index/core/io/SeekableInput;->getAbsolutePosition()J

    move-result-wide v4

    .line 210
    .local v4, "position":J
    new-instance v3, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v3}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 223
    .local v3, "out":Ljava/io/ByteArrayOutputStream;
    :goto_0
    invoke-interface {p0}, Lorg/icepdf/index/core/io/SeekableInput;->getInputStream()Ljava/io/InputStream;

    move-result-object v7

    invoke-virtual {v7}, Ljava/io/InputStream;->read()I

    move-result v6

    .line 224
    .local v6, "read":I
    if-gez v6, :cond_0

    .line 229
    invoke-interface {p0, v4, v5}, Lorg/icepdf/index/core/io/SeekableInput;->seekAbsolute(J)V

    .line 231
    invoke-virtual {v3}, Ljava/io/ByteArrayOutputStream;->flush()V

    .line 232
    invoke-virtual {v3}, Ljava/io/ByteArrayOutputStream;->close()V

    .line 233
    invoke-virtual {v3}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v2

    .line 234
    .local v2, "data":[B
    if-eqz p1, :cond_1

    .line 235
    const/4 v7, 0x1

    invoke-static {v2, v7}, Lorg/icepdf/index/core/util/Utils;->convertByteArrayToHexString([BZ)Ljava/lang/String;

    move-result-object v0

    .line 242
    .end local v2    # "data":[B
    .end local v3    # "out":Ljava/io/ByteArrayOutputStream;
    .end local v4    # "position":J
    .end local v6    # "read":I
    :goto_1
    return-object v0

    .line 226
    .restart local v3    # "out":Ljava/io/ByteArrayOutputStream;
    .restart local v4    # "position":J
    .restart local v6    # "read":I
    :cond_0
    invoke-virtual {v3, v6}, Ljava/io/ByteArrayOutputStream;->write(I)V

    goto :goto_0

    .line 239
    .end local v3    # "out":Ljava/io/ByteArrayOutputStream;
    .end local v4    # "position":J
    .end local v6    # "read":I
    :catch_0
    move-exception v7

    goto :goto_1

    .line 237
    .restart local v2    # "data":[B
    .restart local v3    # "out":Ljava/io/ByteArrayOutputStream;
    .restart local v4    # "position":J
    .restart local v6    # "read":I
    :cond_1
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/lang/String;-><init>([B)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .end local v0    # "content":Ljava/lang/String;
    .local v1, "content":Ljava/lang/String;
    move-object v0, v1

    .end local v1    # "content":Ljava/lang/String;
    .restart local v0    # "content":Ljava/lang/String;
    goto :goto_1
.end method

.method public static numBytesToHoldBits(I)I
    .locals 2
    .param p0, "numBits"    # I

    .prologue
    .line 314
    div-int/lit8 v0, p0, 0x8

    .line 315
    .local v0, "numBytes":I
    rem-int/lit8 v1, p0, 0x8

    if-lez v1, :cond_0

    .line 316
    add-int/lit8 v0, v0, 0x1

    .line 317
    :cond_0
    return v0
.end method

.method public static printMemory(Ljava/lang/String;)V
    .locals 12
    .param p0, "str"    # Ljava/lang/String;

    .prologue
    const-wide/16 v10, 0x400

    .line 283
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Runtime;->totalMemory()J

    move-result-wide v2

    .line 284
    .local v2, "total":J
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Runtime;->freeMemory()J

    move-result-wide v0

    .line 285
    .local v0, "free":J
    sub-long v4, v2, v0

    .line 286
    .local v4, "used":J
    sget-object v6, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "MEM  "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, "    used: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    div-long v8, v4, v10

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, " KB    delta: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-wide v8, Lorg/icepdf/index/core/util/Utils;->lastMemUsed:J

    sub-long v8, v4, v8

    div-long/2addr v8, v10

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, " KB"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 287
    sput-wide v4, Lorg/icepdf/index/core/util/Utils;->lastMemUsed:J

    .line 288
    return-void
.end method

.method public static readIntWithVaryingBytesBE(Ljava/io/InputStream;I)I
    .locals 4
    .param p0, "in"    # Ljava/io/InputStream;
    .param p1, "numBytes"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 88
    const/4 v2, 0x0

    .line 89
    .local v2, "val":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, p1, :cond_1

    .line 90
    invoke-virtual {p0}, Ljava/io/InputStream;->read()I

    move-result v0

    .line 91
    .local v0, "curr":I
    if-gez v0, :cond_0

    .line 92
    new-instance v3, Ljava/io/EOFException;

    invoke-direct {v3}, Ljava/io/EOFException;-><init>()V

    throw v3

    .line 93
    :cond_0
    shl-int/lit8 v2, v2, 0x8

    .line 94
    and-int/lit16 v3, v0, 0xff

    or-int/2addr v2, v3

    .line 89
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 96
    .end local v0    # "curr":I
    :cond_1
    return v2
.end method

.method public static readLongWithVaryingBytesBE(Ljava/io/InputStream;I)J
    .locals 8
    .param p0, "in"    # Ljava/io/InputStream;
    .param p1, "numBytes"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 69
    const-wide/16 v2, 0x0

    .line 70
    .local v2, "val":J
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, p1, :cond_1

    .line 71
    invoke-virtual {p0}, Ljava/io/InputStream;->read()I

    move-result v0

    .line 72
    .local v0, "curr":I
    if-gez v0, :cond_0

    .line 73
    new-instance v4, Ljava/io/EOFException;

    invoke-direct {v4}, Ljava/io/EOFException;-><init>()V

    throw v4

    .line 74
    :cond_0
    const/16 v4, 0x8

    shl-long/2addr v2, v4

    .line 75
    int-to-long v4, v0

    const-wide/16 v6, 0xff

    and-long/2addr v4, v6

    or-long/2addr v2, v4

    .line 70
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 77
    .end local v0    # "curr":I
    :cond_1
    return-wide v2
.end method

.method public static reflectGraphicsEnvironmentISHeadlessInstance(Ljava/lang/Object;Z)Z
    .locals 5
    .param p0, "graphicsEnvironment"    # Ljava/lang/Object;
    .param p1, "defaultReturnIfNoMethod"    # Z

    .prologue
    .line 148
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    .line 149
    .local v0, "clazz":Ljava/lang/Class;
    const-string/jumbo v3, "isHeadlessInstance"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Class;

    invoke-virtual {v0, v3, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    .line 150
    .local v1, "isHeadlessInstanceMethod":Ljava/lang/reflect/Method;
    if-eqz v1, :cond_0

    .line 151
    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, p0, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .line 153
    .local v2, "ret":Ljava/lang/Object;
    instance-of v3, v2, Ljava/lang/Boolean;

    if-eqz v3, :cond_0

    .line 154
    check-cast v2, Ljava/lang/Boolean;

    .end local v2    # "ret":Ljava/lang/Object;
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result p1

    .line 160
    .end local v0    # "clazz":Ljava/lang/Class;
    .end local v1    # "isHeadlessInstanceMethod":Ljava/lang/reflect/Method;
    .end local p1    # "defaultReturnIfNoMethod":Z
    :cond_0
    :goto_0
    return p1

    .line 157
    .restart local p1    # "defaultReturnIfNoMethod":Z
    :catch_0
    move-exception v3

    goto :goto_0
.end method

.method public static replaceInputStreamWithSeekableInput(Ljava/io/InputStream;)Lorg/icepdf/index/core/io/SeekableInput;
    .locals 6
    .param p0, "in"    # Ljava/io/InputStream;

    .prologue
    .line 246
    instance-of v5, p0, Lorg/icepdf/index/core/io/SeekableInput;

    if-eqz v5, :cond_0

    .line 247
    check-cast p0, Lorg/icepdf/index/core/io/SeekableInput;

    .line 279
    .end local p0    # "in":Ljava/io/InputStream;
    .local v3, "sin":Lorg/icepdf/index/core/io/SeekableInput;
    :goto_0
    return-object p0

    .line 249
    .end local v3    # "sin":Lorg/icepdf/index/core/io/SeekableInput;
    .restart local p0    # "in":Ljava/io/InputStream;
    :cond_0
    const/4 v3, 0x0

    .line 251
    .restart local v3    # "sin":Lorg/icepdf/index/core/io/SeekableInput;
    :try_start_0
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    const/16 v5, 0x400

    invoke-direct {v1, v5}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    .line 264
    .local v1, "out":Ljava/io/ByteArrayOutputStream;
    :goto_1
    invoke-virtual {p0}, Ljava/io/InputStream;->read()I

    move-result v2

    .line 265
    .local v2, "read":I
    if-gez v2, :cond_1

    .line 270
    invoke-virtual {p0}, Ljava/io/InputStream;->close()V

    .line 271
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->flush()V

    .line 272
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->close()V

    .line 273
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    .line 274
    .local v0, "data":[B
    new-instance v4, Lorg/icepdf/index/core/io/SeekableByteArrayInputStream;

    invoke-direct {v4, v0}, Lorg/icepdf/index/core/io/SeekableByteArrayInputStream;-><init>([B)V

    .end local v3    # "sin":Lorg/icepdf/index/core/io/SeekableInput;
    .local v4, "sin":Lorg/icepdf/index/core/io/SeekableInput;
    move-object v3, v4

    .end local v0    # "data":[B
    .end local v1    # "out":Ljava/io/ByteArrayOutputStream;
    .end local v2    # "read":I
    .end local v4    # "sin":Lorg/icepdf/index/core/io/SeekableInput;
    .restart local v3    # "sin":Lorg/icepdf/index/core/io/SeekableInput;
    :goto_2
    move-object p0, v3

    .line 279
    goto :goto_0

    .line 267
    .restart local v1    # "out":Ljava/io/ByteArrayOutputStream;
    .restart local v2    # "read":I
    :cond_1
    invoke-virtual {v1, v2}, Ljava/io/ByteArrayOutputStream;->write(I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 276
    .end local v1    # "out":Ljava/io/ByteArrayOutputStream;
    .end local v2    # "read":I
    :catch_0
    move-exception v5

    goto :goto_2
.end method

.method public static setIntIntoByteArrayBE(I[BI)V
    .locals 2
    .param p0, "value"    # I
    .param p1, "buffer"    # [B
    .param p2, "offset"    # I

    .prologue
    .line 42
    add-int/lit8 v0, p2, 0x0

    ushr-int/lit8 v1, p0, 0x18

    and-int/lit16 v1, v1, 0xff

    int-to-byte v1, v1

    aput-byte v1, p1, v0

    .line 43
    add-int/lit8 v0, p2, 0x1

    ushr-int/lit8 v1, p0, 0x10

    and-int/lit16 v1, v1, 0xff

    int-to-byte v1, v1

    aput-byte v1, p1, v0

    .line 44
    add-int/lit8 v0, p2, 0x2

    ushr-int/lit8 v1, p0, 0x8

    and-int/lit16 v1, v1, 0xff

    int-to-byte v1, v1

    aput-byte v1, p1, v0

    .line 45
    add-int/lit8 v0, p2, 0x3

    ushr-int/lit8 v1, p0, 0x0

    and-int/lit16 v1, v1, 0xff

    int-to-byte v1, v1

    aput-byte v1, p1, v0

    .line 46
    return-void
.end method

.method public static setShortIntoByteArrayBE(S[BI)V
    .locals 2
    .param p0, "value"    # S
    .param p1, "buffer"    # [B
    .param p2, "offset"    # I

    .prologue
    .line 57
    add-int/lit8 v0, p2, 0x0

    ushr-int/lit8 v1, p0, 0x8

    and-int/lit16 v1, v1, 0xff

    int-to-byte v1, v1

    aput-byte v1, p1, v0

    .line 58
    add-int/lit8 v0, p2, 0x1

    ushr-int/lit8 v1, p0, 0x0

    and-int/lit16 v1, v1, 0xff

    int-to-byte v1, v1

    aput-byte v1, p1, v0

    .line 59
    return-void
.end method
