.class public Lorg/icepdf/index/core/pobjects/PTrailer;
.super Lorg/icepdf/index/core/pobjects/Dictionary;
.source "PTrailer.java"


# instance fields
.field private m_CrossReferenceStream:Lorg/icepdf/index/core/pobjects/CrossReference;

.field private m_CrossReferenceTable:Lorg/icepdf/index/core/pobjects/CrossReference;


# direct methods
.method public constructor <init>(Lorg/icepdf/index/core/util/Library;Lorg/icepdf/index/core/util/Hashtable;Lorg/icepdf/index/core/pobjects/CrossReference;Lorg/icepdf/index/core/pobjects/CrossReference;)V
    .locals 1
    .param p1, "library"    # Lorg/icepdf/index/core/util/Library;
    .param p2, "dictionary"    # Lorg/icepdf/index/core/util/Hashtable;
    .param p3, "xrefTable"    # Lorg/icepdf/index/core/pobjects/CrossReference;
    .param p4, "xrefStream"    # Lorg/icepdf/index/core/pobjects/CrossReference;

    .prologue
    .line 68
    invoke-direct {p0, p1, p2}, Lorg/icepdf/index/core/pobjects/Dictionary;-><init>(Lorg/icepdf/index/core/util/Library;Lorg/icepdf/index/core/util/Hashtable;)V

    .line 70
    iput-object p3, p0, Lorg/icepdf/index/core/pobjects/PTrailer;->m_CrossReferenceTable:Lorg/icepdf/index/core/pobjects/CrossReference;

    .line 71
    iput-object p4, p0, Lorg/icepdf/index/core/pobjects/PTrailer;->m_CrossReferenceStream:Lorg/icepdf/index/core/pobjects/CrossReference;

    .line 72
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/PTrailer;->m_CrossReferenceTable:Lorg/icepdf/index/core/pobjects/CrossReference;

    if-eqz v0, :cond_0

    .line 73
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/PTrailer;->m_CrossReferenceTable:Lorg/icepdf/index/core/pobjects/CrossReference;

    invoke-virtual {v0, p0}, Lorg/icepdf/index/core/pobjects/CrossReference;->setTrailer(Lorg/icepdf/index/core/pobjects/PTrailer;)V

    .line 74
    :cond_0
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/PTrailer;->m_CrossReferenceStream:Lorg/icepdf/index/core/pobjects/CrossReference;

    if-eqz v0, :cond_1

    .line 75
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/PTrailer;->m_CrossReferenceStream:Lorg/icepdf/index/core/pobjects/CrossReference;

    invoke-virtual {v0, p0}, Lorg/icepdf/index/core/pobjects/CrossReference;->setTrailer(Lorg/icepdf/index/core/pobjects/PTrailer;)V

    .line 76
    :cond_1
    return-void
.end method


# virtual methods
.method protected declared-synchronized addNextTrailer(Lorg/icepdf/index/core/pobjects/PTrailer;)V
    .locals 7
    .param p1, "nextTrailer"    # Lorg/icepdf/index/core/pobjects/PTrailer;

    .prologue
    .line 210
    monitor-enter p0

    :try_start_0
    invoke-virtual {p1}, Lorg/icepdf/index/core/pobjects/PTrailer;->getPrimaryCrossReference()Lorg/icepdf/index/core/pobjects/CrossReference;

    move-result-object v5

    invoke-virtual {p0}, Lorg/icepdf/index/core/pobjects/PTrailer;->getPrimaryCrossReference()Lorg/icepdf/index/core/pobjects/CrossReference;

    move-result-object v6

    invoke-virtual {v5, v6}, Lorg/icepdf/index/core/pobjects/CrossReference;->addToEndOfChainOfPreviousXRefs(Lorg/icepdf/index/core/pobjects/CrossReference;)V

    .line 213
    invoke-virtual {p1}, Lorg/icepdf/index/core/pobjects/PTrailer;->getDictionary()Lorg/icepdf/index/core/util/Hashtable;

    move-result-object v4

    .line 214
    .local v4, "nextDictionary":Lorg/icepdf/index/core/util/Hashtable;
    invoke-virtual {p0}, Lorg/icepdf/index/core/pobjects/PTrailer;->getDictionary()Lorg/icepdf/index/core/util/Hashtable;

    move-result-object v0

    .line 215
    .local v0, "currDictionary":Lorg/icepdf/index/core/util/Hashtable;
    invoke-virtual {v0}, Lorg/icepdf/index/core/util/Hashtable;->keys()Ljava/util/Enumeration;

    move-result-object v2

    .line 216
    .local v2, "currKeys":Ljava/util/Enumeration;
    :goto_0
    invoke-interface {v2}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 217
    monitor-enter v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 218
    :try_start_1
    invoke-interface {v2}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v1

    .line 219
    .local v1, "currKey":Ljava/lang/Object;
    invoke-virtual {v4, v1}, Lorg/icepdf/index/core/util/Hashtable;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 220
    invoke-virtual {v0, v1}, Lorg/icepdf/index/core/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    .line 221
    .local v3, "currValue":Ljava/lang/Object;
    invoke-virtual {v4, v1, v3}, Lorg/icepdf/index/core/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 223
    .end local v3    # "currValue":Ljava/lang/Object;
    :cond_0
    monitor-exit v2

    goto :goto_0

    .end local v1    # "currKey":Ljava/lang/Object;
    :catchall_0
    move-exception v5

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 210
    .end local v0    # "currDictionary":Lorg/icepdf/index/core/util/Hashtable;
    .end local v2    # "currKeys":Ljava/util/Enumeration;
    .end local v4    # "nextDictionary":Lorg/icepdf/index/core/util/Hashtable;
    :catchall_1
    move-exception v5

    monitor-exit p0

    throw v5

    .line 225
    .restart local v0    # "currDictionary":Lorg/icepdf/index/core/util/Hashtable;
    .restart local v2    # "currKeys":Ljava/util/Enumeration;
    .restart local v4    # "nextDictionary":Lorg/icepdf/index/core/util/Hashtable;
    :cond_1
    monitor-exit p0

    return-void
.end method

.method protected declared-synchronized addPreviousTrailer(Lorg/icepdf/index/core/pobjects/PTrailer;)V
    .locals 7
    .param p1, "previousTrailer"    # Lorg/icepdf/index/core/pobjects/PTrailer;

    .prologue
    .line 230
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lorg/icepdf/index/core/pobjects/PTrailer;->getPrimaryCrossReference()Lorg/icepdf/index/core/pobjects/CrossReference;

    move-result-object v5

    invoke-virtual {p1}, Lorg/icepdf/index/core/pobjects/PTrailer;->getPrimaryCrossReference()Lorg/icepdf/index/core/pobjects/CrossReference;

    move-result-object v6

    invoke-virtual {v5, v6}, Lorg/icepdf/index/core/pobjects/CrossReference;->addToEndOfChainOfPreviousXRefs(Lorg/icepdf/index/core/pobjects/CrossReference;)V

    .line 233
    invoke-virtual {p0}, Lorg/icepdf/index/core/pobjects/PTrailer;->getDictionary()Lorg/icepdf/index/core/util/Hashtable;

    move-result-object v0

    .line 234
    .local v0, "currDictionary":Lorg/icepdf/index/core/util/Hashtable;
    invoke-virtual {p1}, Lorg/icepdf/index/core/pobjects/PTrailer;->getDictionary()Lorg/icepdf/index/core/util/Hashtable;

    move-result-object v1

    .line 235
    .local v1, "prevDictionary":Lorg/icepdf/index/core/util/Hashtable;
    invoke-virtual {v1}, Lorg/icepdf/index/core/util/Hashtable;->keys()Ljava/util/Enumeration;

    move-result-object v3

    .line 236
    .local v3, "prevKeys":Ljava/util/Enumeration;
    :goto_0
    invoke-interface {v3}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 238
    monitor-enter v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 239
    :try_start_1
    invoke-interface {v3}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v2

    .line 240
    .local v2, "prevKey":Ljava/lang/Object;
    invoke-virtual {v0, v2}, Lorg/icepdf/index/core/util/Hashtable;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 241
    invoke-virtual {v1, v2}, Lorg/icepdf/index/core/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    .line 242
    .local v4, "prevValue":Ljava/lang/Object;
    invoke-virtual {v0, v2, v4}, Lorg/icepdf/index/core/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 244
    .end local v4    # "prevValue":Ljava/lang/Object;
    :cond_0
    monitor-exit v3

    goto :goto_0

    .end local v2    # "prevKey":Ljava/lang/Object;
    :catchall_0
    move-exception v5

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 230
    .end local v0    # "currDictionary":Lorg/icepdf/index/core/util/Hashtable;
    .end local v1    # "prevDictionary":Lorg/icepdf/index/core/util/Hashtable;
    .end local v3    # "prevKeys":Ljava/util/Enumeration;
    :catchall_1
    move-exception v5

    monitor-exit p0

    throw v5

    .line 246
    .restart local v0    # "currDictionary":Lorg/icepdf/index/core/util/Hashtable;
    .restart local v1    # "prevDictionary":Lorg/icepdf/index/core/util/Hashtable;
    .restart local v3    # "prevKeys":Ljava/util/Enumeration;
    :cond_1
    monitor-exit p0

    return-void
.end method

.method protected getCrossReferenceStream()Lorg/icepdf/index/core/pobjects/CrossReference;
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/PTrailer;->m_CrossReferenceStream:Lorg/icepdf/index/core/pobjects/CrossReference;

    return-object v0
.end method

.method protected getCrossReferenceTable()Lorg/icepdf/index/core/pobjects/CrossReference;
    .locals 1

    .prologue
    .line 130
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/PTrailer;->m_CrossReferenceTable:Lorg/icepdf/index/core/pobjects/CrossReference;

    return-object v0
.end method

.method public getDictionary()Lorg/icepdf/index/core/util/Hashtable;
    .locals 1

    .prologue
    .line 282
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/PTrailer;->entries:Lorg/icepdf/index/core/util/Hashtable;

    return-object v0
.end method

.method public getEncrypt()Lorg/icepdf/index/core/util/Hashtable;
    .locals 4

    .prologue
    .line 184
    iget-object v1, p0, Lorg/icepdf/index/core/pobjects/PTrailer;->library:Lorg/icepdf/index/core/util/Library;

    iget-object v2, p0, Lorg/icepdf/index/core/pobjects/PTrailer;->entries:Lorg/icepdf/index/core/util/Hashtable;

    const-string/jumbo v3, "Encrypt"

    invoke-virtual {v1, v2, v3}, Lorg/icepdf/index/core/util/Library;->getObject(Lorg/icepdf/index/core/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 185
    .local v0, "encryptParams":Ljava/lang/Object;
    instance-of v1, v0, Lorg/icepdf/index/core/util/Hashtable;

    if-eqz v1, :cond_0

    .line 186
    check-cast v0, Lorg/icepdf/index/core/util/Hashtable;

    .line 188
    .end local v0    # "encryptParams":Ljava/lang/Object;
    :goto_0
    return-object v0

    .restart local v0    # "encryptParams":Ljava/lang/Object;
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getID()Ljava/util/Vector;
    .locals 3

    .prologue
    .line 200
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/PTrailer;->library:Lorg/icepdf/index/core/util/Library;

    iget-object v1, p0, Lorg/icepdf/index/core/pobjects/PTrailer;->entries:Lorg/icepdf/index/core/util/Hashtable;

    const-string/jumbo v2, "ID"

    invoke-virtual {v0, v1, v2}, Lorg/icepdf/index/core/util/Library;->getObject(Lorg/icepdf/index/core/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Vector;

    return-object v0
.end method

.method public getNumberOfObjects()I
    .locals 3

    .prologue
    .line 93
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/PTrailer;->library:Lorg/icepdf/index/core/util/Library;

    iget-object v1, p0, Lorg/icepdf/index/core/pobjects/PTrailer;->entries:Lorg/icepdf/index/core/util/Hashtable;

    const-string/jumbo v2, "Size"

    invoke-virtual {v0, v1, v2}, Lorg/icepdf/index/core/util/Library;->getInt(Lorg/icepdf/index/core/util/Hashtable;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public getPrev()J
    .locals 3

    .prologue
    .line 107
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/PTrailer;->library:Lorg/icepdf/index/core/util/Library;

    iget-object v1, p0, Lorg/icepdf/index/core/pobjects/PTrailer;->entries:Lorg/icepdf/index/core/util/Hashtable;

    const-string/jumbo v2, "Prev"

    invoke-virtual {v0, v1, v2}, Lorg/icepdf/index/core/util/Library;->getLong(Lorg/icepdf/index/core/util/Hashtable;Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method protected getPrimaryCrossReference()Lorg/icepdf/index/core/pobjects/CrossReference;
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/PTrailer;->m_CrossReferenceTable:Lorg/icepdf/index/core/pobjects/CrossReference;

    if-eqz v0, :cond_0

    .line 119
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/PTrailer;->m_CrossReferenceTable:Lorg/icepdf/index/core/pobjects/CrossReference;

    .line 121
    :goto_0
    return-object v0

    .line 120
    :cond_0
    invoke-virtual {p0}, Lorg/icepdf/index/core/pobjects/PTrailer;->loadXRefStmIfApplicable()V

    .line 121
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/PTrailer;->m_CrossReferenceStream:Lorg/icepdf/index/core/pobjects/CrossReference;

    goto :goto_0
.end method

.method public getRootCatalog()Lorg/icepdf/index/core/pobjects/Catalog;
    .locals 4

    .prologue
    .line 159
    iget-object v1, p0, Lorg/icepdf/index/core/pobjects/PTrailer;->library:Lorg/icepdf/index/core/util/Library;

    iget-object v2, p0, Lorg/icepdf/index/core/pobjects/PTrailer;->entries:Lorg/icepdf/index/core/util/Hashtable;

    const-string/jumbo v3, "Root"

    invoke-virtual {v1, v2, v3}, Lorg/icepdf/index/core/util/Library;->getObject(Lorg/icepdf/index/core/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 161
    .local v0, "tmp":Ljava/lang/Object;
    instance-of v1, v0, Lorg/icepdf/index/core/pobjects/Catalog;

    if-eqz v1, :cond_0

    .line 162
    iget-object v1, p0, Lorg/icepdf/index/core/pobjects/PTrailer;->library:Lorg/icepdf/index/core/util/Library;

    iget-object v2, p0, Lorg/icepdf/index/core/pobjects/PTrailer;->entries:Lorg/icepdf/index/core/util/Hashtable;

    const-string/jumbo v3, "Root"

    invoke-virtual {v1, v2, v3}, Lorg/icepdf/index/core/util/Library;->getObject(Lorg/icepdf/index/core/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/icepdf/index/core/pobjects/Catalog;

    .line 172
    .end local v0    # "tmp":Ljava/lang/Object;
    :goto_0
    return-object v1

    .line 166
    .restart local v0    # "tmp":Ljava/lang/Object;
    :cond_0
    instance-of v1, v0, Lorg/icepdf/index/core/util/Hashtable;

    if-eqz v1, :cond_1

    .line 167
    new-instance v1, Lorg/icepdf/index/core/pobjects/Catalog;

    iget-object v2, p0, Lorg/icepdf/index/core/pobjects/PTrailer;->library:Lorg/icepdf/index/core/util/Library;

    check-cast v0, Lorg/icepdf/index/core/util/Hashtable;

    .end local v0    # "tmp":Ljava/lang/Object;
    invoke-direct {v1, v2, v0}, Lorg/icepdf/index/core/pobjects/Catalog;-><init>(Lorg/icepdf/index/core/util/Library;Lorg/icepdf/index/core/util/Hashtable;)V

    goto :goto_0

    .line 172
    .restart local v0    # "tmp":Ljava/lang/Object;
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getRootCatalogReference()Lorg/icepdf/index/core/pobjects/Reference;
    .locals 3

    .prologue
    .line 150
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/PTrailer;->library:Lorg/icepdf/index/core/util/Library;

    iget-object v1, p0, Lorg/icepdf/index/core/pobjects/PTrailer;->entries:Lorg/icepdf/index/core/util/Hashtable;

    const-string/jumbo v2, "Root"

    invoke-virtual {v0, v1, v2}, Lorg/icepdf/index/core/util/Library;->getObjectReference(Lorg/icepdf/index/core/util/Hashtable;Ljava/lang/String;)Lorg/icepdf/index/core/pobjects/Reference;

    move-result-object v0

    return-object v0
.end method

.method protected loadXRefStmIfApplicable()V
    .locals 6

    .prologue
    .line 260
    iget-object v1, p0, Lorg/icepdf/index/core/pobjects/PTrailer;->m_CrossReferenceStream:Lorg/icepdf/index/core/pobjects/CrossReference;

    if-nez v1, :cond_0

    .line 261
    iget-object v1, p0, Lorg/icepdf/index/core/pobjects/PTrailer;->library:Lorg/icepdf/index/core/util/Library;

    iget-object v4, p0, Lorg/icepdf/index/core/pobjects/PTrailer;->entries:Lorg/icepdf/index/core/util/Hashtable;

    const-string/jumbo v5, "XRefStm"

    invoke-virtual {v1, v4, v5}, Lorg/icepdf/index/core/util/Library;->getLong(Lorg/icepdf/index/core/util/Hashtable;Ljava/lang/String;)J

    move-result-wide v2

    .line 262
    .local v2, "xrefStreamPosition":J
    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-lez v1, :cond_0

    .line 269
    iget-object v1, p0, Lorg/icepdf/index/core/pobjects/PTrailer;->library:Lorg/icepdf/index/core/util/Library;

    invoke-virtual {v1, v2, v3}, Lorg/icepdf/index/core/util/Library;->getTrailerByFilePosition(J)Lorg/icepdf/index/core/pobjects/PTrailer;

    move-result-object v0

    .line 270
    .local v0, "trailer":Lorg/icepdf/index/core/pobjects/PTrailer;
    if-eqz v0, :cond_0

    .line 271
    invoke-virtual {v0}, Lorg/icepdf/index/core/pobjects/PTrailer;->getCrossReferenceStream()Lorg/icepdf/index/core/pobjects/CrossReference;

    move-result-object v1

    iput-object v1, p0, Lorg/icepdf/index/core/pobjects/PTrailer;->m_CrossReferenceStream:Lorg/icepdf/index/core/pobjects/CrossReference;

    .line 274
    .end local v0    # "trailer":Lorg/icepdf/index/core/pobjects/PTrailer;
    .end local v2    # "xrefStreamPosition":J
    :cond_0
    return-void
.end method

.method protected onDemandLoadAndSetupPreviousTrailer()V
    .locals 6

    .prologue
    .line 251
    invoke-virtual {p0}, Lorg/icepdf/index/core/pobjects/PTrailer;->getPrev()J

    move-result-wide v0

    .line 252
    .local v0, "position":J
    const-wide/16 v4, 0x0

    cmp-long v3, v0, v4

    if-lez v3, :cond_0

    .line 253
    iget-object v3, p0, Lorg/icepdf/index/core/pobjects/PTrailer;->library:Lorg/icepdf/index/core/util/Library;

    invoke-virtual {v3, v0, v1}, Lorg/icepdf/index/core/util/Library;->getTrailerByFilePosition(J)Lorg/icepdf/index/core/pobjects/PTrailer;

    move-result-object v2

    .line 254
    .local v2, "prevTrailer":Lorg/icepdf/index/core/pobjects/PTrailer;
    if-eqz v2, :cond_0

    .line 255
    invoke-virtual {p0, v2}, Lorg/icepdf/index/core/pobjects/PTrailer;->addPreviousTrailer(Lorg/icepdf/index/core/pobjects/PTrailer;)V

    .line 257
    .end local v2    # "prevTrailer":Lorg/icepdf/index/core/pobjects/PTrailer;
    :cond_0
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 291
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "PTRAILER= "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/icepdf/index/core/pobjects/PTrailer;->entries:Lorg/icepdf/index/core/util/Hashtable;

    invoke-virtual {v1}, Lorg/icepdf/index/core/util/Hashtable;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " xref table="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/icepdf/index/core/pobjects/PTrailer;->m_CrossReferenceTable:Lorg/icepdf/index/core/pobjects/CrossReference;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "  xref stream="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/icepdf/index/core/pobjects/PTrailer;->m_CrossReferenceStream:Lorg/icepdf/index/core/pobjects/CrossReference;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
