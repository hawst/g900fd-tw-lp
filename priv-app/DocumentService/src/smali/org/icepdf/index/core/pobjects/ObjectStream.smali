.class public Lorg/icepdf/index/core/pobjects/ObjectStream;
.super Lorg/icepdf/index/core/pobjects/Stream;
.source "ObjectStream.java"


# instance fields
.field private m_DecodedStream:Lorg/icepdf/index/core/io/SeekableInput;

.field private m_bInited:Z

.field private m_iaObjectNumbers:[I

.field private m_laObjectOffset:[J


# direct methods
.method public constructor <init>(Lorg/icepdf/index/core/util/Library;Lorg/icepdf/index/core/util/Hashtable;Lorg/icepdf/index/core/io/SeekableInputConstrainedWrapper;)V
    .locals 0
    .param p1, "l"    # Lorg/icepdf/index/core/util/Library;
    .param p2, "h"    # Lorg/icepdf/index/core/util/Hashtable;
    .param p3, "streamInputWrapper"    # Lorg/icepdf/index/core/io/SeekableInputConstrainedWrapper;

    .prologue
    .line 48
    invoke-direct {p0, p1, p2, p3}, Lorg/icepdf/index/core/pobjects/Stream;-><init>(Lorg/icepdf/index/core/util/Library;Lorg/icepdf/index/core/util/Hashtable;Lorg/icepdf/index/core/io/SeekableInputConstrainedWrapper;)V

    .line 49
    return-void
.end method


# virtual methods
.method public dispose(Z)V
    .locals 2
    .param p1, "cache"    # Z

    .prologue
    const/4 v1, 0x0

    .line 110
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/icepdf/index/core/pobjects/ObjectStream;->m_bInited:Z

    .line 111
    iput-object v1, p0, Lorg/icepdf/index/core/pobjects/ObjectStream;->m_DecodedStream:Lorg/icepdf/index/core/io/SeekableInput;

    .line 112
    iput-object v1, p0, Lorg/icepdf/index/core/pobjects/ObjectStream;->m_iaObjectNumbers:[I

    .line 113
    iput-object v1, p0, Lorg/icepdf/index/core/pobjects/ObjectStream;->m_laObjectOffset:[J

    .line 114
    invoke-super {p0, p1}, Lorg/icepdf/index/core/pobjects/Stream;->dispose(Z)V

    .line 115
    return-void
.end method

.method public init()V
    .locals 10

    .prologue
    .line 52
    iget-boolean v6, p0, Lorg/icepdf/index/core/pobjects/ObjectStream;->m_bInited:Z

    if-eqz v6, :cond_1

    .line 67
    :cond_0
    return-void

    .line 54
    :cond_1
    const/4 v6, 0x1

    iput-boolean v6, p0, Lorg/icepdf/index/core/pobjects/ObjectStream;->m_bInited:Z

    .line 56
    iget-object v6, p0, Lorg/icepdf/index/core/pobjects/ObjectStream;->library:Lorg/icepdf/index/core/util/Library;

    iget-object v7, p0, Lorg/icepdf/index/core/pobjects/ObjectStream;->entries:Lorg/icepdf/index/core/util/Hashtable;

    const-string/jumbo v8, "N"

    invoke-virtual {v6, v7, v8}, Lorg/icepdf/index/core/util/Library;->getInt(Lorg/icepdf/index/core/util/Hashtable;Ljava/lang/String;)I

    move-result v4

    .line 57
    .local v4, "numObjects":I
    iget-object v6, p0, Lorg/icepdf/index/core/pobjects/ObjectStream;->library:Lorg/icepdf/index/core/util/Library;

    iget-object v7, p0, Lorg/icepdf/index/core/pobjects/ObjectStream;->entries:Lorg/icepdf/index/core/util/Hashtable;

    const-string/jumbo v8, "First"

    invoke-virtual {v6, v7, v8}, Lorg/icepdf/index/core/util/Library;->getLong(Lorg/icepdf/index/core/util/Hashtable;Ljava/lang/String;)J

    move-result-wide v2

    .line 58
    .local v2, "firstObjectsOffset":J
    invoke-virtual {p0}, Lorg/icepdf/index/core/pobjects/ObjectStream;->getBytes()[B

    move-result-object v0

    .line 59
    .local v0, "data":[B
    new-instance v6, Lorg/icepdf/index/core/io/SeekableByteArrayInputStream;

    invoke-direct {v6, v0}, Lorg/icepdf/index/core/io/SeekableByteArrayInputStream;-><init>([B)V

    iput-object v6, p0, Lorg/icepdf/index/core/pobjects/ObjectStream;->m_DecodedStream:Lorg/icepdf/index/core/io/SeekableInput;

    .line 60
    new-array v6, v4, [I

    iput-object v6, p0, Lorg/icepdf/index/core/pobjects/ObjectStream;->m_iaObjectNumbers:[I

    .line 61
    new-array v6, v4, [J

    iput-object v6, p0, Lorg/icepdf/index/core/pobjects/ObjectStream;->m_laObjectOffset:[J

    .line 62
    new-instance v5, Lorg/icepdf/index/core/util/Parser;

    iget-object v6, p0, Lorg/icepdf/index/core/pobjects/ObjectStream;->m_DecodedStream:Lorg/icepdf/index/core/io/SeekableInput;

    invoke-direct {v5, v6}, Lorg/icepdf/index/core/util/Parser;-><init>(Lorg/icepdf/index/core/io/SeekableInput;)V

    .line 63
    .local v5, "parser":Lorg/icepdf/index/core/util/Parser;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v4, :cond_0

    .line 64
    iget-object v6, p0, Lorg/icepdf/index/core/pobjects/ObjectStream;->m_iaObjectNumbers:[I

    invoke-virtual {v5}, Lorg/icepdf/index/core/util/Parser;->getIntSurroundedByWhitespace()I

    move-result v7

    aput v7, v6, v1

    .line 65
    iget-object v6, p0, Lorg/icepdf/index/core/pobjects/ObjectStream;->m_laObjectOffset:[J

    invoke-virtual {v5}, Lorg/icepdf/index/core/util/Parser;->getLongSurroundedByWhitespace()J

    move-result-wide v8

    add-long/2addr v8, v2

    aput-wide v8, v6, v1

    .line 63
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public loadObject(Lorg/icepdf/index/core/util/Library;I)Z
    .locals 11
    .param p1, "library"    # Lorg/icepdf/index/core/util/Library;
    .param p2, "objectIndex"    # I

    .prologue
    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 71
    invoke-virtual {p0}, Lorg/icepdf/index/core/pobjects/ObjectStream;->init()V

    .line 72
    iget-object v9, p0, Lorg/icepdf/index/core/pobjects/ObjectStream;->m_iaObjectNumbers:[I

    if-eqz v9, :cond_0

    iget-object v9, p0, Lorg/icepdf/index/core/pobjects/ObjectStream;->m_laObjectOffset:[J

    if-eqz v9, :cond_0

    iget-object v9, p0, Lorg/icepdf/index/core/pobjects/ObjectStream;->m_iaObjectNumbers:[I

    array-length v9, v9

    iget-object v10, p0, Lorg/icepdf/index/core/pobjects/ObjectStream;->m_laObjectOffset:[J

    array-length v10, v10

    if-ne v9, v10, :cond_0

    if-ltz p2, :cond_0

    iget-object v9, p0, Lorg/icepdf/index/core/pobjects/ObjectStream;->m_iaObjectNumbers:[I

    array-length v9, v9

    if-lt p2, v9, :cond_1

    :cond_0
    move v0, v8

    .line 106
    :goto_0
    return v0

    .line 80
    :cond_1
    const/4 v0, 0x0

    .line 82
    .local v0, "gotSomething":Z
    :try_start_0
    iget-object v9, p0, Lorg/icepdf/index/core/pobjects/ObjectStream;->m_iaObjectNumbers:[I

    aget v2, v9, p2

    .line 83
    .local v2, "objectNumber":I
    iget-object v9, p0, Lorg/icepdf/index/core/pobjects/ObjectStream;->m_laObjectOffset:[J

    aget-wide v4, v9, p2

    .line 85
    .local v4, "position":J
    iget-object v9, p0, Lorg/icepdf/index/core/pobjects/ObjectStream;->m_DecodedStream:Lorg/icepdf/index/core/io/SeekableInput;

    invoke-interface {v9, v4, v5}, Lorg/icepdf/index/core/io/SeekableInput;->seekAbsolute(J)V

    .line 86
    new-instance v3, Lorg/icepdf/index/core/util/Parser;

    iget-object v9, p0, Lorg/icepdf/index/core/pobjects/ObjectStream;->m_DecodedStream:Lorg/icepdf/index/core/io/SeekableInput;

    const/4 v10, 0x1

    invoke-direct {v3, v9, v10}, Lorg/icepdf/index/core/util/Parser;-><init>(Lorg/icepdf/index/core/io/SeekableInput;I)V

    .line 91
    .local v3, "parser":Lorg/icepdf/index/core/util/Parser;
    invoke-virtual {v3, p1}, Lorg/icepdf/index/core/util/Parser;->getObject(Lorg/icepdf/index/core/util/Library;)Ljava/lang/Object;

    move-result-object v1

    .line 92
    .local v1, "ob":Ljava/lang/Object;
    if-nez v1, :cond_3

    .line 93
    new-instance v6, Lorg/icepdf/index/core/pobjects/Reference;

    const/4 v9, 0x0

    invoke-direct {v6, v2, v9}, Lorg/icepdf/index/core/pobjects/Reference;-><init>(II)V

    .line 94
    .local v6, "ref":Lorg/icepdf/index/core/pobjects/Reference;
    invoke-virtual {v3, p1, v6}, Lorg/icepdf/index/core/util/Parser;->addPObject(Lorg/icepdf/index/core/util/Library;Lorg/icepdf/index/core/pobjects/Reference;)Lorg/icepdf/index/core/pobjects/PObject;

    move-result-object v1

    .line 101
    .end local v1    # "ob":Ljava/lang/Object;
    .end local v6    # "ref":Lorg/icepdf/index/core/pobjects/Reference;
    :cond_2
    :goto_1
    if-eqz v1, :cond_4

    move v0, v7

    :goto_2
    goto :goto_0

    .line 95
    .restart local v1    # "ob":Ljava/lang/Object;
    :cond_3
    instance-of v9, v1, Lorg/icepdf/index/core/pobjects/PObject;

    if-nez v9, :cond_2

    .line 96
    new-instance v6, Lorg/icepdf/index/core/pobjects/Reference;

    const/4 v9, 0x0

    invoke-direct {v6, v2, v9}, Lorg/icepdf/index/core/pobjects/Reference;-><init>(II)V

    .line 97
    .restart local v6    # "ref":Lorg/icepdf/index/core/pobjects/Reference;
    invoke-virtual {p1, v1, v6}, Lorg/icepdf/index/core/util/Library;->addObject(Ljava/lang/Object;Lorg/icepdf/index/core/pobjects/Reference;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 103
    .end local v1    # "ob":Ljava/lang/Object;
    .end local v2    # "objectNumber":I
    .end local v3    # "parser":Lorg/icepdf/index/core/util/Parser;
    .end local v4    # "position":J
    .end local v6    # "ref":Lorg/icepdf/index/core/pobjects/Reference;
    :catch_0
    move-exception v7

    goto :goto_0

    .restart local v2    # "objectNumber":I
    .restart local v3    # "parser":Lorg/icepdf/index/core/util/Parser;
    .restart local v4    # "position":J
    :cond_4
    move v0, v8

    .line 101
    goto :goto_2
.end method
