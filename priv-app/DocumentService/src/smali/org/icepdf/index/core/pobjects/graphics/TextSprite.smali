.class public Lorg/icepdf/index/core/pobjects/graphics/TextSprite;
.super Ljava/lang/Object;
.source "TextSprite.java"


# instance fields
.field bounds:Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;

.field private font:Lorg/icepdf/index/core/pobjects/fonts/FontFile;

.field private glyphTexts:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lorg/icepdf/index/core/pobjects/graphics/text/GlyphText;",
            ">;"
        }
    .end annotation
.end field

.field private graphicStateTransform:Landroid/graphics/Matrix;

.field private textObjects:[[F


# direct methods
.method public constructor <init>(Lorg/icepdf/index/core/pobjects/fonts/FontFile;ILandroid/graphics/Matrix;)V
    .locals 1
    .param p1, "font"    # Lorg/icepdf/index/core/pobjects/fonts/FontFile;
    .param p2, "size"    # I
    .param p3, "graphicStateTransform"    # Landroid/graphics/Matrix;

    .prologue
    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lorg/icepdf/index/core/pobjects/graphics/TextSprite;->glyphTexts:Ljava/util/ArrayList;

    .line 69
    iput-object p3, p0, Lorg/icepdf/index/core/pobjects/graphics/TextSprite;->graphicStateTransform:Landroid/graphics/Matrix;

    .line 70
    iput-object p1, p0, Lorg/icepdf/index/core/pobjects/graphics/TextSprite;->font:Lorg/icepdf/index/core/pobjects/fonts/FontFile;

    .line 71
    new-instance v0, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;

    invoke-direct {v0}, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;-><init>()V

    iput-object v0, p0, Lorg/icepdf/index/core/pobjects/graphics/TextSprite;->bounds:Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;

    .line 72
    return-void
.end method


# virtual methods
.method public addText(Ljava/lang/String;Ljava/lang/String;FFF)Lorg/icepdf/index/core/pobjects/graphics/text/GlyphText;
    .locals 10
    .param p1, "cid"    # Ljava/lang/String;
    .param p2, "unicode"    # Ljava/lang/String;
    .param p3, "x"    # F
    .param p4, "y"    # F
    .param p5, "width"    # F

    .prologue
    const/4 v2, 0x0

    .line 90
    move v7, p5

    .line 92
    .local v7, "w":F
    iget-object v1, p0, Lorg/icepdf/index/core/pobjects/graphics/TextSprite;->font:Lorg/icepdf/index/core/pobjects/fonts/FontFile;

    invoke-interface {v1}, Lorg/icepdf/index/core/pobjects/fonts/FontFile;->getAscent()D

    move-result-wide v4

    iget-object v1, p0, Lorg/icepdf/index/core/pobjects/graphics/TextSprite;->font:Lorg/icepdf/index/core/pobjects/fonts/FontFile;

    invoke-interface {v1}, Lorg/icepdf/index/core/pobjects/fonts/FontFile;->getDescent()D

    move-result-wide v8

    add-double/2addr v4, v8

    double-to-float v6, v4

    .line 94
    .local v6, "h":F
    cmpg-float v1, v6, v2

    if-gtz v1, :cond_0

    .line 95
    iget-object v1, p0, Lorg/icepdf/index/core/pobjects/graphics/TextSprite;->font:Lorg/icepdf/index/core/pobjects/fonts/FontFile;

    invoke-interface {v1}, Lorg/icepdf/index/core/pobjects/fonts/FontFile;->getMaxCharBounds()Lorg/icepdf/index/core/pobjects/Rectangle2Df;

    move-result-object v1

    invoke-virtual {v1}, Lorg/icepdf/index/core/pobjects/Rectangle2Df;->getHeight()F

    move-result v6

    .line 97
    :cond_0
    cmpg-float v1, v7, v2

    if-gtz v1, :cond_1

    .line 98
    iget-object v1, p0, Lorg/icepdf/index/core/pobjects/graphics/TextSprite;->font:Lorg/icepdf/index/core/pobjects/fonts/FontFile;

    invoke-interface {v1}, Lorg/icepdf/index/core/pobjects/fonts/FontFile;->getMaxCharBounds()Lorg/icepdf/index/core/pobjects/Rectangle2Df;

    move-result-object v1

    invoke-virtual {v1}, Lorg/icepdf/index/core/pobjects/Rectangle2Df;->getWidth()F

    move-result v7

    .line 102
    :cond_1
    cmpg-float v1, v6, v2

    if-gtz v1, :cond_2

    .line 103
    const/high16 v6, 0x3f800000    # 1.0f

    .line 105
    :cond_2
    cmpg-float v1, v7, v2

    if-gtz v1, :cond_3

    .line 106
    const/high16 v7, 0x3f800000    # 1.0f

    .line 108
    :cond_3
    new-instance v3, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;

    iget-object v1, p0, Lorg/icepdf/index/core/pobjects/graphics/TextSprite;->font:Lorg/icepdf/index/core/pobjects/fonts/FontFile;

    invoke-interface {v1}, Lorg/icepdf/index/core/pobjects/fonts/FontFile;->getAscent()D

    move-result-wide v4

    double-to-float v1, v4

    sub-float v1, p4, v1

    invoke-direct {v3, p3, v1, v7, v6}, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;-><init>(FFFF)V

    .line 112
    .local v3, "glyphBounds":Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;
    iget-object v1, p0, Lorg/icepdf/index/core/pobjects/graphics/TextSprite;->bounds:Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;

    invoke-virtual {v1, v3}, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;->add(Lorg/icepdf/index/java/awt/geom/Rectangle2D;)V

    .line 115
    new-instance v0, Lorg/icepdf/index/core/pobjects/graphics/text/GlyphText;

    move v1, p3

    move v2, p4

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lorg/icepdf/index/core/pobjects/graphics/text/GlyphText;-><init>(FFLorg/icepdf/index/java/awt/geom/Rectangle2D$Float;Ljava/lang/String;Ljava/lang/String;)V

    .line 118
    .local v0, "glyphText":Lorg/icepdf/index/core/pobjects/graphics/text/GlyphText;
    iget-object v1, p0, Lorg/icepdf/index/core/pobjects/graphics/TextSprite;->glyphTexts:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 121
    return-object v0
.end method

.method public dispose()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 187
    move-object v0, v1

    check-cast v0, [[F

    iput-object v0, p0, Lorg/icepdf/index/core/pobjects/graphics/TextSprite;->textObjects:[[F

    .line 188
    iput-object v1, p0, Lorg/icepdf/index/core/pobjects/graphics/TextSprite;->font:Lorg/icepdf/index/core/pobjects/fonts/FontFile;

    .line 189
    return-void
.end method

.method public getGlyphOutline()Lorg/icepdf/index/java/awt/geom/Area;
    .locals 8

    .prologue
    .line 206
    const/4 v0, 0x0

    .line 207
    .local v0, "glyphOutline":Lorg/icepdf/index/java/awt/geom/Area;
    iget-object v3, p0, Lorg/icepdf/index/core/pobjects/graphics/TextSprite;->glyphTexts:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/icepdf/index/core/pobjects/graphics/text/GlyphText;

    .line 208
    .local v1, "glyphText":Lorg/icepdf/index/core/pobjects/graphics/text/GlyphText;
    if-eqz v0, :cond_0

    .line 209
    new-instance v3, Lorg/icepdf/index/java/awt/geom/Area;

    iget-object v4, p0, Lorg/icepdf/index/core/pobjects/graphics/TextSprite;->font:Lorg/icepdf/index/core/pobjects/fonts/FontFile;

    invoke-virtual {v1}, Lorg/icepdf/index/core/pobjects/graphics/text/GlyphText;->getCid()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1}, Lorg/icepdf/index/core/pobjects/graphics/text/GlyphText;->getX()F

    move-result v6

    invoke-virtual {v1}, Lorg/icepdf/index/core/pobjects/graphics/text/GlyphText;->getY()F

    move-result v7

    invoke-interface {v4, v5, v6, v7}, Lorg/icepdf/index/core/pobjects/fonts/FontFile;->getEstringOutline(Ljava/lang/String;FF)Lorg/icepdf/index/java/awt/Shape;

    move-result-object v4

    invoke-direct {v3, v4}, Lorg/icepdf/index/java/awt/geom/Area;-><init>(Lorg/icepdf/index/java/awt/Shape;)V

    invoke-virtual {v0, v3}, Lorg/icepdf/index/java/awt/geom/Area;->add(Lorg/icepdf/index/java/awt/geom/Area;)V

    goto :goto_0

    .line 213
    :cond_0
    new-instance v0, Lorg/icepdf/index/java/awt/geom/Area;

    .end local v0    # "glyphOutline":Lorg/icepdf/index/java/awt/geom/Area;
    iget-object v3, p0, Lorg/icepdf/index/core/pobjects/graphics/TextSprite;->font:Lorg/icepdf/index/core/pobjects/fonts/FontFile;

    invoke-virtual {v1}, Lorg/icepdf/index/core/pobjects/graphics/text/GlyphText;->getCid()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1}, Lorg/icepdf/index/core/pobjects/graphics/text/GlyphText;->getX()F

    move-result v5

    invoke-virtual {v1}, Lorg/icepdf/index/core/pobjects/graphics/text/GlyphText;->getY()F

    move-result v6

    invoke-interface {v3, v4, v5, v6}, Lorg/icepdf/index/core/pobjects/fonts/FontFile;->getEstringOutline(Ljava/lang/String;FF)Lorg/icepdf/index/java/awt/Shape;

    move-result-object v3

    invoke-direct {v0, v3}, Lorg/icepdf/index/java/awt/geom/Area;-><init>(Lorg/icepdf/index/java/awt/Shape;)V

    .restart local v0    # "glyphOutline":Lorg/icepdf/index/java/awt/geom/Area;
    goto :goto_0

    .line 218
    .end local v1    # "glyphText":Lorg/icepdf/index/core/pobjects/graphics/text/GlyphText;
    :cond_1
    return-object v0
.end method

.method public getGlyphSprites()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lorg/icepdf/index/core/pobjects/graphics/text/GlyphText;",
            ">;"
        }
    .end annotation

    .prologue
    .line 192
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/graphics/TextSprite;->glyphTexts:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getGraphicStateTransform()Landroid/graphics/Matrix;
    .locals 1

    .prologue
    .line 196
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/graphics/TextSprite;->graphicStateTransform:Landroid/graphics/Matrix;

    return-object v0
.end method

.method public getSize()I
    .locals 1

    .prologue
    .line 176
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/graphics/TextSprite;->textObjects:[[F

    if-eqz v0, :cond_0

    .line 177
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/graphics/TextSprite;->textObjects:[[F

    array-length v0, v0

    .line 179
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getTextObjects()[[F
    .locals 1

    .prologue
    .line 167
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/graphics/TextSprite;->textObjects:[[F

    return-object v0
.end method

.method public setStrokeColor(I)V
    .locals 0
    .param p1, "color"    # I

    .prologue
    .line 159
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 147
    iget-object v3, p0, Lorg/icepdf/index/core/pobjects/graphics/TextSprite;->textObjects:[[F

    if-eqz v3, :cond_1

    .line 149
    new-instance v2, Ljava/lang/StringBuffer;

    iget-object v3, p0, Lorg/icepdf/index/core/pobjects/graphics/TextSprite;->textObjects:[[F

    array-length v3, v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 150
    .local v2, "text":Ljava/lang/StringBuffer;
    const/4 v0, 0x0

    .local v0, "i":I
    iget-object v3, p0, Lorg/icepdf/index/core/pobjects/graphics/TextSprite;->textObjects:[[F

    array-length v1, v3

    .local v1, "max":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 151
    iget-object v3, p0, Lorg/icepdf/index/core/pobjects/graphics/TextSprite;->textObjects:[[F

    aget-object v3, v3, v0

    const/4 v4, 0x2

    aget v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(F)Ljava/lang/StringBuffer;

    .line 150
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 153
    :cond_0
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    .line 155
    .end local v0    # "i":I
    .end local v1    # "max":I
    .end local v2    # "text":Ljava/lang/StringBuffer;
    :goto_1
    return-object v3

    :cond_1
    const-string/jumbo v3, ""

    goto :goto_1
.end method
