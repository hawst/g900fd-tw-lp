.class public Lorg/icepdf/index/core/pobjects/Reference;
.super Ljava/lang/Object;
.source "Reference.java"


# instance fields
.field genf:I

.field objf:I


# direct methods
.method public constructor <init>(II)V
    .locals 1
    .param p1, "o"    # I
    .param p2, "g"    # I

    .prologue
    const/4 v0, 0x0

    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput v0, p0, Lorg/icepdf/index/core/pobjects/Reference;->objf:I

    .line 35
    iput v0, p0, Lorg/icepdf/index/core/pobjects/Reference;->genf:I

    .line 59
    iput p1, p0, Lorg/icepdf/index/core/pobjects/Reference;->objf:I

    .line 60
    iput p2, p0, Lorg/icepdf/index/core/pobjects/Reference;->genf:I

    .line 61
    return-void
.end method

.method public constructor <init>(Ljava/lang/Number;Ljava/lang/Number;)V
    .locals 1
    .param p1, "o"    # Ljava/lang/Number;
    .param p2, "g"    # Ljava/lang/Number;

    .prologue
    const/4 v0, 0x0

    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput v0, p0, Lorg/icepdf/index/core/pobjects/Reference;->objf:I

    .line 35
    iput v0, p0, Lorg/icepdf/index/core/pobjects/Reference;->genf:I

    .line 44
    if-eqz p1, :cond_0

    .line 45
    invoke-virtual {p1}, Ljava/lang/Number;->intValue()I

    move-result v0

    iput v0, p0, Lorg/icepdf/index/core/pobjects/Reference;->objf:I

    .line 47
    :cond_0
    if-eqz p2, :cond_1

    .line 48
    invoke-virtual {p2}, Ljava/lang/Number;->intValue()I

    move-result v0

    iput v0, p0, Lorg/icepdf/index/core/pobjects/Reference;->genf:I

    .line 50
    :cond_1
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 79
    if-ne p1, p0, :cond_1

    .line 85
    :cond_0
    :goto_0
    return v1

    .line 81
    :cond_1
    if-eqz p1, :cond_3

    instance-of v3, p1, Lorg/icepdf/index/core/pobjects/Reference;

    if-eqz v3, :cond_3

    move-object v0, p1

    .line 82
    check-cast v0, Lorg/icepdf/index/core/pobjects/Reference;

    .line 83
    .local v0, "tmp":Lorg/icepdf/index/core/pobjects/Reference;
    iget v3, v0, Lorg/icepdf/index/core/pobjects/Reference;->objf:I

    iget v4, p0, Lorg/icepdf/index/core/pobjects/Reference;->objf:I

    if-ne v3, v4, :cond_2

    iget v3, v0, Lorg/icepdf/index/core/pobjects/Reference;->genf:I

    iget v4, p0, Lorg/icepdf/index/core/pobjects/Reference;->genf:I

    if-eq v3, v4, :cond_0

    :cond_2
    move v1, v2

    goto :goto_0

    .end local v0    # "tmp":Lorg/icepdf/index/core/pobjects/Reference;
    :cond_3
    move v1, v2

    .line 85
    goto :goto_0
.end method

.method public getGenerationNumber()I
    .locals 1

    .prologue
    .line 103
    iget v0, p0, Lorg/icepdf/index/core/pobjects/Reference;->genf:I

    return v0
.end method

.method public getObjectNumber()I
    .locals 1

    .prologue
    .line 94
    iget v0, p0, Lorg/icepdf/index/core/pobjects/Reference;->objf:I

    return v0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 69
    iget v0, p0, Lorg/icepdf/index/core/pobjects/Reference;->objf:I

    mul-int/lit16 v0, v0, 0x3e8

    iget v1, p0, Lorg/icepdf/index/core/pobjects/Reference;->genf:I

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 112
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "R["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lorg/icepdf/index/core/pobjects/Reference;->objf:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lorg/icepdf/index/core/pobjects/Reference;->genf:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
