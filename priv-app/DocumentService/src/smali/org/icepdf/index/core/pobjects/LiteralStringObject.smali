.class public Lorg/icepdf/index/core/pobjects/LiteralStringObject;
.super Ljava/lang/Object;
.source "LiteralStringObject.java"

# interfaces
.implements Lorg/icepdf/index/core/pobjects/StringObject;


# static fields
.field private static hexChar:[C


# instance fields
.field reference:Lorg/icepdf/index/core/pobjects/Reference;

.field private stringData:Ljava/lang/StringBuffer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    const/16 v0, 0x10

    new-array v0, v0, [C

    fill-array-data v0, :array_0

    sput-object v0, Lorg/icepdf/index/core/pobjects/LiteralStringObject;->hexChar:[C

    return-void

    :array_0
    .array-data 2
        0x30s
        0x31s
        0x32s
        0x33s
        0x34s
        0x35s
        0x36s
        0x37s
        0x38s
        0x39s
        0x61s
        0x62s
        0x63s
        0x64s
        0x65s
        0x66s
    .end array-data
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "string"    # Ljava/lang/String;

    .prologue
    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0, p1}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lorg/icepdf/index/core/pobjects/LiteralStringObject;->stringData:Ljava/lang/StringBuffer;

    .line 61
    return-void
.end method

.method public constructor <init>(Ljava/lang/StringBuffer;)V
    .locals 2
    .param p1, "stringBuffer"    # Ljava/lang/StringBuffer;

    .prologue
    .line 72
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 74
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->deleteCharAt(I)Ljava/lang/StringBuffer;

    .line 75
    invoke-virtual {p1}, Ljava/lang/StringBuffer;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->deleteCharAt(I)Ljava/lang/StringBuffer;

    .line 77
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-virtual {p1}, Ljava/lang/StringBuffer;->length()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuffer;-><init>(I)V

    iput-object v0, p0, Lorg/icepdf/index/core/pobjects/LiteralStringObject;->stringData:Ljava/lang/StringBuffer;

    .line 78
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/LiteralStringObject;->stringData:Ljava/lang/StringBuffer;

    invoke-virtual {p1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 79
    return-void
.end method

.method public constructor <init>([B)V
    .locals 2
    .param p1, "bytes"    # [B

    .prologue
    .line 49
    new-instance v0, Ljava/lang/StringBuffer;

    array-length v1, p1

    invoke-direct {v0, v1}, Ljava/lang/StringBuffer;-><init>(I)V

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, p1}, Ljava/lang/String;-><init>([B)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/icepdf/index/core/pobjects/LiteralStringObject;-><init>(Ljava/lang/StringBuffer;)V

    .line 50
    return-void
.end method

.method private stringToHex(Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;
    .locals 6
    .param p1, "string"    # Ljava/lang/StringBuffer;

    .prologue
    .line 223
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-virtual {p1}, Ljava/lang/StringBuffer;->length()I

    move-result v4

    mul-int/lit8 v4, v4, 0x2

    invoke-direct {v1, v4}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 225
    .local v1, "hh":Ljava/lang/StringBuffer;
    const/4 v2, 0x0

    .local v2, "i":I
    invoke-virtual {p1}, Ljava/lang/StringBuffer;->length()I

    move-result v3

    .local v3, "max":I
    :goto_0
    if-ge v2, v3, :cond_0

    .line 226
    invoke-virtual {p1, v2}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v0

    .line 227
    .local v0, "charCode":I
    sget-object v4, Lorg/icepdf/index/core/pobjects/LiteralStringObject;->hexChar:[C

    and-int/lit16 v5, v0, 0xf0

    ushr-int/lit8 v5, v5, 0x4

    aget-char v4, v4, v5

    invoke-virtual {v1, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 228
    sget-object v4, Lorg/icepdf/index/core/pobjects/LiteralStringObject;->hexChar:[C

    and-int/lit8 v5, v0, 0xf

    aget-char v4, v4, v5

    invoke-virtual {v1, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 225
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 230
    .end local v0    # "charCode":I
    :cond_0
    return-object v1
.end method


# virtual methods
.method public getDecryptedLiteralString(Lorg/icepdf/index/core/pobjects/security/SecurityManager;)Ljava/lang/String;
    .locals 5
    .param p1, "securityManager"    # Lorg/icepdf/index/core/pobjects/security/SecurityManager;

    .prologue
    .line 250
    if-eqz p1, :cond_2

    iget-object v4, p0, Lorg/icepdf/index/core/pobjects/LiteralStringObject;->reference:Lorg/icepdf/index/core/pobjects/Reference;

    if-eqz v4, :cond_2

    .line 252
    invoke-virtual {p1}, Lorg/icepdf/index/core/pobjects/security/SecurityManager;->getDecryptionKey()[B

    move-result-object v1

    .line 255
    .local v1, "key":[B
    iget-object v4, p0, Lorg/icepdf/index/core/pobjects/LiteralStringObject;->stringData:Ljava/lang/StringBuffer;

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->length()I

    move-result v4

    new-array v3, v4, [B

    .line 256
    .local v3, "textBytes":[B
    const/4 v0, 0x0

    .local v0, "i":I
    array-length v2, v3

    .local v2, "max":I
    :goto_0
    if-ge v0, v2, :cond_0

    .line 257
    iget-object v4, p0, Lorg/icepdf/index/core/pobjects/LiteralStringObject;->stringData:Ljava/lang/StringBuffer;

    invoke-virtual {v4, v0}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v4

    int-to-byte v4, v4

    aput-byte v4, v3, v0

    .line 256
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 261
    :cond_0
    iget-object v4, p0, Lorg/icepdf/index/core/pobjects/LiteralStringObject;->reference:Lorg/icepdf/index/core/pobjects/Reference;

    invoke-virtual {p1, v4, v1, v3}, Lorg/icepdf/index/core/pobjects/security/SecurityManager;->decrypt(Lorg/icepdf/index/core/pobjects/Reference;[B[B)[B

    move-result-object v3

    .line 265
    if-nez v3, :cond_1

    .line 266
    const-string/jumbo v4, ""

    .line 271
    .end local v0    # "i":I
    .end local v1    # "key":[B
    .end local v2    # "max":I
    .end local v3    # "textBytes":[B
    :goto_1
    return-object v4

    .line 269
    .restart local v0    # "i":I
    .restart local v1    # "key":[B
    .restart local v2    # "max":I
    .restart local v3    # "textBytes":[B
    :cond_1
    new-instance v4, Ljava/lang/String;

    invoke-direct {v4, v3}, Ljava/lang/String;-><init>([B)V

    goto :goto_1

    .line 271
    .end local v0    # "i":I
    .end local v1    # "key":[B
    .end local v2    # "max":I
    .end local v3    # "textBytes":[B
    :cond_2
    iget-object v4, p0, Lorg/icepdf/index/core/pobjects/LiteralStringObject;->stringData:Ljava/lang/StringBuffer;

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_1
.end method

.method public getHexString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/LiteralStringObject;->stringData:Ljava/lang/StringBuffer;

    invoke-direct {p0, v0}, Lorg/icepdf/index/core/pobjects/LiteralStringObject;->stringToHex(Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getHexStringBuffer()Ljava/lang/StringBuffer;
    .locals 1

    .prologue
    .line 136
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/LiteralStringObject;->stringData:Ljava/lang/StringBuffer;

    invoke-direct {p0, v0}, Lorg/icepdf/index/core/pobjects/LiteralStringObject;->stringToHex(Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;

    move-result-object v0

    return-object v0
.end method

.method public getLength()I
    .locals 1

    .prologue
    .line 213
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/LiteralStringObject;->stringData:Ljava/lang/StringBuffer;

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->length()I

    move-result v0

    return v0
.end method

.method public getLiteralString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 156
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/LiteralStringObject;->stringData:Ljava/lang/StringBuffer;

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getLiteralStringBuffer()Ljava/lang/StringBuffer;
    .locals 1

    .prologue
    .line 146
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/LiteralStringObject;->stringData:Ljava/lang/StringBuffer;

    return-object v0
.end method

.method public getLiteralStringBuffer(ILorg/icepdf/index/core/pobjects/fonts/FontFile;)Ljava/lang/StringBuffer;
    .locals 8
    .param p1, "fontFormat"    # I
    .param p2, "font"    # Lorg/icepdf/index/core/pobjects/fonts/FontFile;

    .prologue
    .line 174
    const/4 v6, 0x1

    if-ne p1, v6, :cond_1

    .line 175
    const/4 v0, 0x1

    .line 176
    .local v0, "charOffset":I
    invoke-virtual {p0}, Lorg/icepdf/index/core/pobjects/LiteralStringObject;->getLength()I

    move-result v4

    .line 177
    .local v4, "length":I
    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5, v4}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 178
    .local v5, "tmp":Ljava/lang/StringBuffer;
    const/4 v3, 0x0

    .line 180
    .local v3, "lastIndex":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v4, :cond_4

    .line 181
    sub-int v6, v2, v3

    add-int v7, v3, v0

    invoke-virtual {p0, v6, v7}, Lorg/icepdf/index/core/pobjects/LiteralStringObject;->getUnsignedInt(II)I

    move-result v1

    .line 183
    .local v1, "charValue":I
    if-ltz v1, :cond_0

    .line 184
    int-to-char v6, v1

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 185
    const/4 v3, 0x0

    .line 180
    :goto_1
    add-int/2addr v2, v0

    goto :goto_0

    .line 187
    :cond_0
    add-int/2addr v3, v0

    goto :goto_1

    .line 191
    .end local v0    # "charOffset":I
    .end local v1    # "charValue":I
    .end local v2    # "i":I
    .end local v3    # "lastIndex":I
    .end local v4    # "length":I
    .end local v5    # "tmp":Ljava/lang/StringBuffer;
    :cond_1
    const/4 v6, 0x2

    if-ne p1, v6, :cond_3

    .line 192
    const/4 v0, 0x2

    .line 193
    .restart local v0    # "charOffset":I
    invoke-virtual {p0}, Lorg/icepdf/index/core/pobjects/LiteralStringObject;->getLength()I

    move-result v4

    .line 195
    .restart local v4    # "length":I
    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5, v4}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 196
    .restart local v5    # "tmp":Ljava/lang/StringBuffer;
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_2
    if-ge v2, v4, :cond_4

    .line 197
    invoke-virtual {p0, v2, v0}, Lorg/icepdf/index/core/pobjects/LiteralStringObject;->getUnsignedInt(II)I

    move-result v1

    .line 198
    .restart local v1    # "charValue":I
    int-to-char v6, v1

    invoke-interface {p2, v6}, Lorg/icepdf/index/core/pobjects/fonts/FontFile;->canDisplayEchar(C)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 199
    int-to-char v6, v1

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 196
    :cond_2
    add-int/2addr v2, v0

    goto :goto_2

    .line 204
    .end local v0    # "charOffset":I
    .end local v1    # "charValue":I
    .end local v2    # "i":I
    .end local v4    # "length":I
    .end local v5    # "tmp":Ljava/lang/StringBuffer;
    :cond_3
    const/4 v5, 0x0

    :cond_4
    return-object v5
.end method

.method public getUnsignedInt(II)I
    .locals 3
    .param p1, "start"    # I
    .param p2, "offset"    # I

    .prologue
    const/4 v0, 0x0

    .line 90
    if-ltz p1, :cond_0

    iget-object v1, p0, Lorg/icepdf/index/core/pobjects/LiteralStringObject;->stringData:Ljava/lang/StringBuffer;

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->length()I

    move-result v1

    add-int v2, p1, p2

    if-ge v1, v2, :cond_1

    .line 105
    :cond_0
    :goto_0
    return v0

    .line 93
    :cond_1
    const/4 v1, 0x1

    if-ne p2, v1, :cond_2

    .line 94
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/LiteralStringObject;->stringData:Ljava/lang/StringBuffer;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v0

    goto :goto_0

    .line 96
    :cond_2
    const/4 v1, 0x2

    if-ne p2, v1, :cond_3

    .line 97
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/LiteralStringObject;->stringData:Ljava/lang/StringBuffer;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v0

    and-int/lit16 v0, v0, 0xff

    shl-int/lit8 v0, v0, 0x8

    iget-object v1, p0, Lorg/icepdf/index/core/pobjects/LiteralStringObject;->stringData:Ljava/lang/StringBuffer;

    add-int/lit8 v2, p1, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v1

    and-int/lit16 v1, v1, 0xff

    or-int/2addr v0, v1

    goto :goto_0

    .line 99
    :cond_3
    const/4 v1, 0x4

    if-ne p2, v1, :cond_0

    .line 100
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/LiteralStringObject;->stringData:Ljava/lang/StringBuffer;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v0

    and-int/lit16 v0, v0, 0xff

    shl-int/lit8 v0, v0, 0x18

    iget-object v1, p0, Lorg/icepdf/index/core/pobjects/LiteralStringObject;->stringData:Ljava/lang/StringBuffer;

    add-int/lit8 v2, p1, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v1

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x10

    or-int/2addr v0, v1

    iget-object v1, p0, Lorg/icepdf/index/core/pobjects/LiteralStringObject;->stringData:Ljava/lang/StringBuffer;

    add-int/lit8 v2, p1, 0x2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v1

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x8

    or-int/2addr v0, v1

    iget-object v1, p0, Lorg/icepdf/index/core/pobjects/LiteralStringObject;->stringData:Ljava/lang/StringBuffer;

    add-int/lit8 v2, p1, 0x3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v1

    and-int/lit16 v1, v1, 0xff

    or-int/2addr v0, v1

    goto :goto_0
.end method

.method public setReference(Lorg/icepdf/index/core/pobjects/Reference;)V
    .locals 0
    .param p1, "reference"    # Lorg/icepdf/index/core/pobjects/Reference;

    .prologue
    .line 239
    iput-object p1, p0, Lorg/icepdf/index/core/pobjects/LiteralStringObject;->reference:Lorg/icepdf/index/core/pobjects/Reference;

    .line 240
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/LiteralStringObject;->stringData:Ljava/lang/StringBuffer;

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
