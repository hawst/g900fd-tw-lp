.class public Lorg/icepdf/index/core/pobjects/filters/RunLengthDecode;
.super Lorg/icepdf/index/core/pobjects/filters/ChunkingInputStream;
.source "RunLengthDecode.java"


# direct methods
.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 1
    .param p1, "input"    # Ljava/io/InputStream;

    .prologue
    .line 26
    invoke-direct {p0}, Lorg/icepdf/index/core/pobjects/filters/ChunkingInputStream;-><init>()V

    .line 28
    invoke-virtual {p0, p1}, Lorg/icepdf/index/core/pobjects/filters/RunLengthDecode;->setInputStream(Ljava/io/InputStream;)V

    .line 29
    const/16 v0, 0x1000

    invoke-virtual {p0, v0}, Lorg/icepdf/index/core/pobjects/filters/RunLengthDecode;->setBufferSize(I)V

    .line 30
    return-void
.end method


# virtual methods
.method protected fillInternalBuffer()I
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 33
    const/4 v5, 0x0

    .line 35
    .local v5, "numRead":I
    :goto_0
    iget-object v7, p0, Lorg/icepdf/index/core/pobjects/filters/RunLengthDecode;->buffer:[B

    array-length v7, v7

    add-int/lit16 v7, v7, -0x104

    if-ge v5, v7, :cond_0

    .line 36
    iget-object v7, p0, Lorg/icepdf/index/core/pobjects/filters/RunLengthDecode;->in:Ljava/io/InputStream;

    invoke-virtual {v7}, Ljava/io/InputStream;->read()I

    move-result v1

    .line 37
    .local v1, "i":I
    if-gez v1, :cond_2

    .line 51
    .end local v1    # "i":I
    :cond_0
    if-nez v5, :cond_1

    .line 52
    const/4 v5, -0x1

    .line 53
    .end local v5    # "numRead":I
    :cond_1
    return v5

    .line 39
    .restart local v1    # "i":I
    .restart local v5    # "numRead":I
    :cond_2
    const/16 v7, 0x80

    if-ge v1, v7, :cond_3

    .line 40
    add-int/lit8 v7, v1, 0x1

    invoke-virtual {p0, v5, v7}, Lorg/icepdf/index/core/pobjects/filters/RunLengthDecode;->fillBufferFromInputStream(II)I

    move-result v7

    add-int/2addr v5, v7

    goto :goto_0

    .line 42
    :cond_3
    rsub-int v0, v1, 0x101

    .line 43
    .local v0, "count":I
    iget-object v7, p0, Lorg/icepdf/index/core/pobjects/filters/RunLengthDecode;->in:Ljava/io/InputStream;

    invoke-virtual {v7}, Ljava/io/InputStream;->read()I

    move-result v2

    .line 44
    .local v2, "j":I
    and-int/lit16 v7, v2, 0xff

    int-to-byte v3, v7

    .line 45
    .local v3, "jj":B
    const/4 v4, 0x0

    .local v4, "k":I
    move v6, v5

    .end local v5    # "numRead":I
    .local v6, "numRead":I
    :goto_1
    if-ge v4, v0, :cond_4

    .line 46
    iget-object v7, p0, Lorg/icepdf/index/core/pobjects/filters/RunLengthDecode;->buffer:[B

    add-int/lit8 v5, v6, 0x1

    .end local v6    # "numRead":I
    .restart local v5    # "numRead":I
    aput-byte v3, v7, v6

    .line 45
    add-int/lit8 v4, v4, 0x1

    move v6, v5

    .end local v5    # "numRead":I
    .restart local v6    # "numRead":I
    goto :goto_1

    :cond_4
    move v5, v6

    .end local v6    # "numRead":I
    .restart local v5    # "numRead":I
    goto :goto_0
.end method
