.class public Lorg/icepdf/index/core/util/PropertyConstants;
.super Ljava/lang/Object;
.source "PropertyConstants.java"


# static fields
.field public static final DOCUMENT_CURRENT_PAGE:Ljava/lang/String; = "documentCurrentPage"

.field public static final DOCUMENT_TOOL_NONE:Ljava/lang/String; = "documentToolNone"

.field public static final DOCUMENT_TOOL_PAN:Ljava/lang/String; = "documentToolRotation"

.field public static final DOCUMENT_TOOL_ZOOM_IN:Ljava/lang/String; = "documentToolZoomIn"

.field public static final DOCUMENT_TOOL_ZOOM_OUT:Ljava/lang/String; = "documentToolZoomOut"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
