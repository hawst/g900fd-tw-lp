.class public Lorg/icepdf/index/core/pobjects/filters/CCITTFax;
.super Ljava/lang/Object;
.source "CCITTFax.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/icepdf/index/core/pobjects/filters/CCITTFax$Code;
    }
.end annotation


# static fields
.field private static final TIFF_COMPRESSION_GROUP3_1D:S = 0x2s

.field private static final TIFF_COMPRESSION_GROUP3_2D:S = 0x3s

.field private static final TIFF_COMPRESSION_GROUP4:S = 0x4s

.field private static final TIFF_COMPRESSION_NONE_default:S = 0x1s

.field private static final TIFF_PHOTOMETRIC_INTERPRETATION_BLACK_IS_ZERO:S = 0x1s

.field private static final TIFF_PHOTOMETRIC_INTERPRETATION_WHITE_IS_ZERO_default:S

.field private static USE_JAI_IMAGE_LIBRARY:Z

.field static final _extmcodes:[Ljava/lang/String;

.field static final _mbcodes:[Ljava/lang/String;

.field static final _modecodes:[Ljava/lang/String;

.field static final _mwcodes:[Ljava/lang/String;

.field static final _tbcodes:[Ljava/lang/String;

.field static final _twcodes:[Ljava/lang/String;

.field static black:I

.field static final extmcodes:[[Lorg/icepdf/index/core/pobjects/filters/CCITTFax$Code;

.field static final mbcodes:[[Lorg/icepdf/index/core/pobjects/filters/CCITTFax$Code;

.field static final modecodes:[[Lorg/icepdf/index/core/pobjects/filters/CCITTFax$Code;

.field static final mwcodes:[[Lorg/icepdf/index/core/pobjects/filters/CCITTFax$Code;

.field static final tbcodes:[[Lorg/icepdf/index/core/pobjects/filters/CCITTFax$Code;

.field static final twcodes:[[Lorg/icepdf/index/core/pobjects/filters/CCITTFax$Code;

.field static white:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 55
    const/16 v1, 0x40

    new-array v1, v1, [Ljava/lang/String;

    const-string/jumbo v2, "00110101"

    aput-object v2, v1, v4

    const-string/jumbo v2, "000111"

    aput-object v2, v1, v5

    const-string/jumbo v2, "0111"

    aput-object v2, v1, v6

    const-string/jumbo v2, "1000"

    aput-object v2, v1, v7

    const-string/jumbo v2, "1011"

    aput-object v2, v1, v8

    const/4 v2, 0x5

    const-string/jumbo v3, "1100"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string/jumbo v3, "1110"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string/jumbo v3, "1111"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string/jumbo v3, "10011"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string/jumbo v3, "10100"

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-string/jumbo v3, "00111"

    aput-object v3, v1, v2

    const/16 v2, 0xb

    const-string/jumbo v3, "01000"

    aput-object v3, v1, v2

    const/16 v2, 0xc

    const-string/jumbo v3, "001000"

    aput-object v3, v1, v2

    const/16 v2, 0xd

    const-string/jumbo v3, "000011"

    aput-object v3, v1, v2

    const/16 v2, 0xe

    const-string/jumbo v3, "110100"

    aput-object v3, v1, v2

    const/16 v2, 0xf

    const-string/jumbo v3, "110101"

    aput-object v3, v1, v2

    const/16 v2, 0x10

    const-string/jumbo v3, "101010"

    aput-object v3, v1, v2

    const/16 v2, 0x11

    const-string/jumbo v3, "101011"

    aput-object v3, v1, v2

    const/16 v2, 0x12

    const-string/jumbo v3, "0100111"

    aput-object v3, v1, v2

    const/16 v2, 0x13

    const-string/jumbo v3, "0001100"

    aput-object v3, v1, v2

    const/16 v2, 0x14

    const-string/jumbo v3, "0001000"

    aput-object v3, v1, v2

    const/16 v2, 0x15

    const-string/jumbo v3, "0010111"

    aput-object v3, v1, v2

    const/16 v2, 0x16

    const-string/jumbo v3, "0000011"

    aput-object v3, v1, v2

    const/16 v2, 0x17

    const-string/jumbo v3, "0000100"

    aput-object v3, v1, v2

    const/16 v2, 0x18

    const-string/jumbo v3, "0101000"

    aput-object v3, v1, v2

    const/16 v2, 0x19

    const-string/jumbo v3, "0101011"

    aput-object v3, v1, v2

    const/16 v2, 0x1a

    const-string/jumbo v3, "0010011"

    aput-object v3, v1, v2

    const/16 v2, 0x1b

    const-string/jumbo v3, "0100100"

    aput-object v3, v1, v2

    const/16 v2, 0x1c

    const-string/jumbo v3, "0011000"

    aput-object v3, v1, v2

    const/16 v2, 0x1d

    const-string/jumbo v3, "00000010"

    aput-object v3, v1, v2

    const/16 v2, 0x1e

    const-string/jumbo v3, "00000011"

    aput-object v3, v1, v2

    const/16 v2, 0x1f

    const-string/jumbo v3, "00011010"

    aput-object v3, v1, v2

    const/16 v2, 0x20

    const-string/jumbo v3, "00011011"

    aput-object v3, v1, v2

    const/16 v2, 0x21

    const-string/jumbo v3, "00010010"

    aput-object v3, v1, v2

    const/16 v2, 0x22

    const-string/jumbo v3, "00010011"

    aput-object v3, v1, v2

    const/16 v2, 0x23

    const-string/jumbo v3, "00010100"

    aput-object v3, v1, v2

    const/16 v2, 0x24

    const-string/jumbo v3, "00010101"

    aput-object v3, v1, v2

    const/16 v2, 0x25

    const-string/jumbo v3, "00010110"

    aput-object v3, v1, v2

    const/16 v2, 0x26

    const-string/jumbo v3, "00010111"

    aput-object v3, v1, v2

    const/16 v2, 0x27

    const-string/jumbo v3, "00101000"

    aput-object v3, v1, v2

    const/16 v2, 0x28

    const-string/jumbo v3, "00101001"

    aput-object v3, v1, v2

    const/16 v2, 0x29

    const-string/jumbo v3, "00101010"

    aput-object v3, v1, v2

    const/16 v2, 0x2a

    const-string/jumbo v3, "00101011"

    aput-object v3, v1, v2

    const/16 v2, 0x2b

    const-string/jumbo v3, "00101100"

    aput-object v3, v1, v2

    const/16 v2, 0x2c

    const-string/jumbo v3, "00101101"

    aput-object v3, v1, v2

    const/16 v2, 0x2d

    const-string/jumbo v3, "00000100"

    aput-object v3, v1, v2

    const/16 v2, 0x2e

    const-string/jumbo v3, "00000101"

    aput-object v3, v1, v2

    const/16 v2, 0x2f

    const-string/jumbo v3, "00001010"

    aput-object v3, v1, v2

    const/16 v2, 0x30

    const-string/jumbo v3, "00001011"

    aput-object v3, v1, v2

    const/16 v2, 0x31

    const-string/jumbo v3, "01010010"

    aput-object v3, v1, v2

    const/16 v2, 0x32

    const-string/jumbo v3, "01010011"

    aput-object v3, v1, v2

    const/16 v2, 0x33

    const-string/jumbo v3, "01010100"

    aput-object v3, v1, v2

    const/16 v2, 0x34

    const-string/jumbo v3, "01010101"

    aput-object v3, v1, v2

    const/16 v2, 0x35

    const-string/jumbo v3, "00100100"

    aput-object v3, v1, v2

    const/16 v2, 0x36

    const-string/jumbo v3, "00100101"

    aput-object v3, v1, v2

    const/16 v2, 0x37

    const-string/jumbo v3, "01011000"

    aput-object v3, v1, v2

    const/16 v2, 0x38

    const-string/jumbo v3, "01011001"

    aput-object v3, v1, v2

    const/16 v2, 0x39

    const-string/jumbo v3, "01011010"

    aput-object v3, v1, v2

    const/16 v2, 0x3a

    const-string/jumbo v3, "01011011"

    aput-object v3, v1, v2

    const/16 v2, 0x3b

    const-string/jumbo v3, "01001010"

    aput-object v3, v1, v2

    const/16 v2, 0x3c

    const-string/jumbo v3, "01001011"

    aput-object v3, v1, v2

    const/16 v2, 0x3d

    const-string/jumbo v3, "00110010"

    aput-object v3, v1, v2

    const/16 v2, 0x3e

    const-string/jumbo v3, "00110011"

    aput-object v3, v1, v2

    const/16 v2, 0x3f

    const-string/jumbo v3, "00110100"

    aput-object v3, v1, v2

    sput-object v1, Lorg/icepdf/index/core/pobjects/filters/CCITTFax;->_twcodes:[Ljava/lang/String;

    .line 71
    const/16 v1, 0x1b

    new-array v1, v1, [Ljava/lang/String;

    const-string/jumbo v2, "11011"

    aput-object v2, v1, v4

    const-string/jumbo v2, "10010"

    aput-object v2, v1, v5

    const-string/jumbo v2, "010111"

    aput-object v2, v1, v6

    const-string/jumbo v2, "0110111"

    aput-object v2, v1, v7

    const-string/jumbo v2, "00110110"

    aput-object v2, v1, v8

    const/4 v2, 0x5

    const-string/jumbo v3, "00110111"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string/jumbo v3, "01100100"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string/jumbo v3, "01100101"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string/jumbo v3, "01101000"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string/jumbo v3, "01100111"

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-string/jumbo v3, "011001100"

    aput-object v3, v1, v2

    const/16 v2, 0xb

    const-string/jumbo v3, "011001101"

    aput-object v3, v1, v2

    const/16 v2, 0xc

    const-string/jumbo v3, "011010010"

    aput-object v3, v1, v2

    const/16 v2, 0xd

    const-string/jumbo v3, "011010011"

    aput-object v3, v1, v2

    const/16 v2, 0xe

    const-string/jumbo v3, "011010100"

    aput-object v3, v1, v2

    const/16 v2, 0xf

    const-string/jumbo v3, "011010101"

    aput-object v3, v1, v2

    const/16 v2, 0x10

    const-string/jumbo v3, "011010110"

    aput-object v3, v1, v2

    const/16 v2, 0x11

    const-string/jumbo v3, "011010111"

    aput-object v3, v1, v2

    const/16 v2, 0x12

    const-string/jumbo v3, "011011000"

    aput-object v3, v1, v2

    const/16 v2, 0x13

    const-string/jumbo v3, "011011001"

    aput-object v3, v1, v2

    const/16 v2, 0x14

    const-string/jumbo v3, "011011010"

    aput-object v3, v1, v2

    const/16 v2, 0x15

    const-string/jumbo v3, "011011011"

    aput-object v3, v1, v2

    const/16 v2, 0x16

    const-string/jumbo v3, "010011000"

    aput-object v3, v1, v2

    const/16 v2, 0x17

    const-string/jumbo v3, "010011001"

    aput-object v3, v1, v2

    const/16 v2, 0x18

    const-string/jumbo v3, "010011010"

    aput-object v3, v1, v2

    const/16 v2, 0x19

    const-string/jumbo v3, "011000"

    aput-object v3, v1, v2

    const/16 v2, 0x1a

    const-string/jumbo v3, "010011011"

    aput-object v3, v1, v2

    sput-object v1, Lorg/icepdf/index/core/pobjects/filters/CCITTFax;->_mwcodes:[Ljava/lang/String;

    .line 80
    const/16 v1, 0x40

    new-array v1, v1, [Ljava/lang/String;

    const-string/jumbo v2, "0000110111"

    aput-object v2, v1, v4

    const-string/jumbo v2, "010"

    aput-object v2, v1, v5

    const-string/jumbo v2, "11"

    aput-object v2, v1, v6

    const-string/jumbo v2, "10"

    aput-object v2, v1, v7

    const-string/jumbo v2, "011"

    aput-object v2, v1, v8

    const/4 v2, 0x5

    const-string/jumbo v3, "0011"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string/jumbo v3, "0010"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string/jumbo v3, "00011"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string/jumbo v3, "000101"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string/jumbo v3, "000100"

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-string/jumbo v3, "0000100"

    aput-object v3, v1, v2

    const/16 v2, 0xb

    const-string/jumbo v3, "0000101"

    aput-object v3, v1, v2

    const/16 v2, 0xc

    const-string/jumbo v3, "0000111"

    aput-object v3, v1, v2

    const/16 v2, 0xd

    const-string/jumbo v3, "00000100"

    aput-object v3, v1, v2

    const/16 v2, 0xe

    const-string/jumbo v3, "00000111"

    aput-object v3, v1, v2

    const/16 v2, 0xf

    const-string/jumbo v3, "000011000"

    aput-object v3, v1, v2

    const/16 v2, 0x10

    const-string/jumbo v3, "0000010111"

    aput-object v3, v1, v2

    const/16 v2, 0x11

    const-string/jumbo v3, "0000011000"

    aput-object v3, v1, v2

    const/16 v2, 0x12

    const-string/jumbo v3, "0000001000"

    aput-object v3, v1, v2

    const/16 v2, 0x13

    const-string/jumbo v3, "00001100111"

    aput-object v3, v1, v2

    const/16 v2, 0x14

    const-string/jumbo v3, "00001101000"

    aput-object v3, v1, v2

    const/16 v2, 0x15

    const-string/jumbo v3, "00001101100"

    aput-object v3, v1, v2

    const/16 v2, 0x16

    const-string/jumbo v3, "00000110111"

    aput-object v3, v1, v2

    const/16 v2, 0x17

    const-string/jumbo v3, "00000101000"

    aput-object v3, v1, v2

    const/16 v2, 0x18

    const-string/jumbo v3, "00000010111"

    aput-object v3, v1, v2

    const/16 v2, 0x19

    const-string/jumbo v3, "00000011000"

    aput-object v3, v1, v2

    const/16 v2, 0x1a

    const-string/jumbo v3, "000011001010"

    aput-object v3, v1, v2

    const/16 v2, 0x1b

    const-string/jumbo v3, "000011001011"

    aput-object v3, v1, v2

    const/16 v2, 0x1c

    const-string/jumbo v3, "000011001100"

    aput-object v3, v1, v2

    const/16 v2, 0x1d

    const-string/jumbo v3, "000011001101"

    aput-object v3, v1, v2

    const/16 v2, 0x1e

    const-string/jumbo v3, "000001101000"

    aput-object v3, v1, v2

    const/16 v2, 0x1f

    const-string/jumbo v3, "000001101001"

    aput-object v3, v1, v2

    const/16 v2, 0x20

    const-string/jumbo v3, "000001101010"

    aput-object v3, v1, v2

    const/16 v2, 0x21

    const-string/jumbo v3, "000001101011"

    aput-object v3, v1, v2

    const/16 v2, 0x22

    const-string/jumbo v3, "000011010010"

    aput-object v3, v1, v2

    const/16 v2, 0x23

    const-string/jumbo v3, "000011010011"

    aput-object v3, v1, v2

    const/16 v2, 0x24

    const-string/jumbo v3, "000011010100"

    aput-object v3, v1, v2

    const/16 v2, 0x25

    const-string/jumbo v3, "000011010101"

    aput-object v3, v1, v2

    const/16 v2, 0x26

    const-string/jumbo v3, "000011010110"

    aput-object v3, v1, v2

    const/16 v2, 0x27

    const-string/jumbo v3, "000011010111"

    aput-object v3, v1, v2

    const/16 v2, 0x28

    const-string/jumbo v3, "000001101100"

    aput-object v3, v1, v2

    const/16 v2, 0x29

    const-string/jumbo v3, "000001101101"

    aput-object v3, v1, v2

    const/16 v2, 0x2a

    const-string/jumbo v3, "000011011010"

    aput-object v3, v1, v2

    const/16 v2, 0x2b

    const-string/jumbo v3, "000011011011"

    aput-object v3, v1, v2

    const/16 v2, 0x2c

    const-string/jumbo v3, "000001010100"

    aput-object v3, v1, v2

    const/16 v2, 0x2d

    const-string/jumbo v3, "000001010101"

    aput-object v3, v1, v2

    const/16 v2, 0x2e

    const-string/jumbo v3, "000001010110"

    aput-object v3, v1, v2

    const/16 v2, 0x2f

    const-string/jumbo v3, "000001010111"

    aput-object v3, v1, v2

    const/16 v2, 0x30

    const-string/jumbo v3, "000001100100"

    aput-object v3, v1, v2

    const/16 v2, 0x31

    const-string/jumbo v3, "000001100101"

    aput-object v3, v1, v2

    const/16 v2, 0x32

    const-string/jumbo v3, "000001010010"

    aput-object v3, v1, v2

    const/16 v2, 0x33

    const-string/jumbo v3, "000001010011"

    aput-object v3, v1, v2

    const/16 v2, 0x34

    const-string/jumbo v3, "000000100100"

    aput-object v3, v1, v2

    const/16 v2, 0x35

    const-string/jumbo v3, "000000110111"

    aput-object v3, v1, v2

    const/16 v2, 0x36

    const-string/jumbo v3, "000000111000"

    aput-object v3, v1, v2

    const/16 v2, 0x37

    const-string/jumbo v3, "000000100111"

    aput-object v3, v1, v2

    const/16 v2, 0x38

    const-string/jumbo v3, "000000101000"

    aput-object v3, v1, v2

    const/16 v2, 0x39

    const-string/jumbo v3, "000001011000"

    aput-object v3, v1, v2

    const/16 v2, 0x3a

    const-string/jumbo v3, "000001011001"

    aput-object v3, v1, v2

    const/16 v2, 0x3b

    const-string/jumbo v3, "000000101011"

    aput-object v3, v1, v2

    const/16 v2, 0x3c

    const-string/jumbo v3, "000000101100"

    aput-object v3, v1, v2

    const/16 v2, 0x3d

    const-string/jumbo v3, "000001011010"

    aput-object v3, v1, v2

    const/16 v2, 0x3e

    const-string/jumbo v3, "000001100110"

    aput-object v3, v1, v2

    const/16 v2, 0x3f

    const-string/jumbo v3, "000001100111"

    aput-object v3, v1, v2

    sput-object v1, Lorg/icepdf/index/core/pobjects/filters/CCITTFax;->_tbcodes:[Ljava/lang/String;

    .line 97
    const/16 v1, 0x1b

    new-array v1, v1, [Ljava/lang/String;

    const-string/jumbo v2, "0000001111"

    aput-object v2, v1, v4

    const-string/jumbo v2, "000011001000"

    aput-object v2, v1, v5

    const-string/jumbo v2, "000011001001"

    aput-object v2, v1, v6

    const-string/jumbo v2, "000001011011"

    aput-object v2, v1, v7

    const-string/jumbo v2, "000000110011"

    aput-object v2, v1, v8

    const/4 v2, 0x5

    const-string/jumbo v3, "000000110100"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string/jumbo v3, "000000110101"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string/jumbo v3, "0000001101100"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string/jumbo v3, "0000001101101"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string/jumbo v3, "0000001001010"

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-string/jumbo v3, "0000001001011"

    aput-object v3, v1, v2

    const/16 v2, 0xb

    const-string/jumbo v3, "0000001001100"

    aput-object v3, v1, v2

    const/16 v2, 0xc

    const-string/jumbo v3, "0000001001101"

    aput-object v3, v1, v2

    const/16 v2, 0xd

    const-string/jumbo v3, "0000001110010"

    aput-object v3, v1, v2

    const/16 v2, 0xe

    const-string/jumbo v3, "0000001110011"

    aput-object v3, v1, v2

    const/16 v2, 0xf

    const-string/jumbo v3, "0000001110100"

    aput-object v3, v1, v2

    const/16 v2, 0x10

    const-string/jumbo v3, "0000001110101"

    aput-object v3, v1, v2

    const/16 v2, 0x11

    const-string/jumbo v3, "0000001110110"

    aput-object v3, v1, v2

    const/16 v2, 0x12

    const-string/jumbo v3, "0000001110111"

    aput-object v3, v1, v2

    const/16 v2, 0x13

    const-string/jumbo v3, "0000001010010"

    aput-object v3, v1, v2

    const/16 v2, 0x14

    const-string/jumbo v3, "0000001010011"

    aput-object v3, v1, v2

    const/16 v2, 0x15

    const-string/jumbo v3, "0000001010100"

    aput-object v3, v1, v2

    const/16 v2, 0x16

    const-string/jumbo v3, "0000001010101"

    aput-object v3, v1, v2

    const/16 v2, 0x17

    const-string/jumbo v3, "0000001011010"

    aput-object v3, v1, v2

    const/16 v2, 0x18

    const-string/jumbo v3, "0000001011011"

    aput-object v3, v1, v2

    const/16 v2, 0x19

    const-string/jumbo v3, "0000001100100"

    aput-object v3, v1, v2

    const/16 v2, 0x1a

    const-string/jumbo v3, "0000001100101"

    aput-object v3, v1, v2

    sput-object v1, Lorg/icepdf/index/core/pobjects/filters/CCITTFax;->_mbcodes:[Ljava/lang/String;

    .line 106
    const/16 v1, 0xd

    new-array v1, v1, [Ljava/lang/String;

    const-string/jumbo v2, "00000001000"

    aput-object v2, v1, v4

    const-string/jumbo v2, "00000001100"

    aput-object v2, v1, v5

    const-string/jumbo v2, "00000001101"

    aput-object v2, v1, v6

    const-string/jumbo v2, "000000010010"

    aput-object v2, v1, v7

    const-string/jumbo v2, "000000010011"

    aput-object v2, v1, v8

    const/4 v2, 0x5

    const-string/jumbo v3, "000000010100"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string/jumbo v3, "000000010101"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string/jumbo v3, "000000010110"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string/jumbo v3, "000000010111"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string/jumbo v3, "000000011100"

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-string/jumbo v3, "000000011101"

    aput-object v3, v1, v2

    const/16 v2, 0xb

    const-string/jumbo v3, "000000011110"

    aput-object v3, v1, v2

    const/16 v2, 0xc

    const-string/jumbo v3, "000000011111"

    aput-object v3, v1, v2

    sput-object v1, Lorg/icepdf/index/core/pobjects/filters/CCITTFax;->_extmcodes:[Ljava/lang/String;

    .line 113
    const/16 v1, 0xc

    new-array v1, v1, [Ljava/lang/String;

    const-string/jumbo v2, "0001"

    aput-object v2, v1, v4

    const-string/jumbo v2, "001"

    aput-object v2, v1, v5

    const-string/jumbo v2, "1"

    aput-object v2, v1, v6

    const-string/jumbo v2, "011"

    aput-object v2, v1, v7

    const-string/jumbo v2, "000011"

    aput-object v2, v1, v8

    const/4 v2, 0x5

    const-string/jumbo v3, "0000011"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string/jumbo v3, "010"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string/jumbo v3, "000010"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string/jumbo v3, "0000010"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string/jumbo v3, "0000001111"

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-string/jumbo v3, "000000001111"

    aput-object v3, v1, v2

    const/16 v2, 0xb

    const-string/jumbo v3, "000000000001"

    aput-object v3, v1, v2

    sput-object v1, Lorg/icepdf/index/core/pobjects/filters/CCITTFax;->_modecodes:[Ljava/lang/String;

    .line 181
    sget-object v1, Lorg/icepdf/index/core/pobjects/filters/CCITTFax;->_twcodes:[Ljava/lang/String;

    invoke-static {v1}, Lorg/icepdf/index/core/pobjects/filters/CCITTFax;->convertStringArrayToCodeArray2D([Ljava/lang/String;)[[Lorg/icepdf/index/core/pobjects/filters/CCITTFax$Code;

    move-result-object v1

    sput-object v1, Lorg/icepdf/index/core/pobjects/filters/CCITTFax;->twcodes:[[Lorg/icepdf/index/core/pobjects/filters/CCITTFax$Code;

    .line 182
    sget-object v1, Lorg/icepdf/index/core/pobjects/filters/CCITTFax;->_mwcodes:[Ljava/lang/String;

    invoke-static {v1}, Lorg/icepdf/index/core/pobjects/filters/CCITTFax;->convertStringArrayToCodeArray2D([Ljava/lang/String;)[[Lorg/icepdf/index/core/pobjects/filters/CCITTFax$Code;

    move-result-object v1

    sput-object v1, Lorg/icepdf/index/core/pobjects/filters/CCITTFax;->mwcodes:[[Lorg/icepdf/index/core/pobjects/filters/CCITTFax$Code;

    .line 183
    sget-object v1, Lorg/icepdf/index/core/pobjects/filters/CCITTFax;->_tbcodes:[Ljava/lang/String;

    invoke-static {v1}, Lorg/icepdf/index/core/pobjects/filters/CCITTFax;->convertStringArrayToCodeArray2D([Ljava/lang/String;)[[Lorg/icepdf/index/core/pobjects/filters/CCITTFax$Code;

    move-result-object v1

    sput-object v1, Lorg/icepdf/index/core/pobjects/filters/CCITTFax;->tbcodes:[[Lorg/icepdf/index/core/pobjects/filters/CCITTFax$Code;

    .line 184
    sget-object v1, Lorg/icepdf/index/core/pobjects/filters/CCITTFax;->_mbcodes:[Ljava/lang/String;

    invoke-static {v1}, Lorg/icepdf/index/core/pobjects/filters/CCITTFax;->convertStringArrayToCodeArray2D([Ljava/lang/String;)[[Lorg/icepdf/index/core/pobjects/filters/CCITTFax$Code;

    move-result-object v1

    sput-object v1, Lorg/icepdf/index/core/pobjects/filters/CCITTFax;->mbcodes:[[Lorg/icepdf/index/core/pobjects/filters/CCITTFax$Code;

    .line 185
    sget-object v1, Lorg/icepdf/index/core/pobjects/filters/CCITTFax;->_extmcodes:[Ljava/lang/String;

    invoke-static {v1}, Lorg/icepdf/index/core/pobjects/filters/CCITTFax;->convertStringArrayToCodeArray2D([Ljava/lang/String;)[[Lorg/icepdf/index/core/pobjects/filters/CCITTFax$Code;

    move-result-object v1

    sput-object v1, Lorg/icepdf/index/core/pobjects/filters/CCITTFax;->extmcodes:[[Lorg/icepdf/index/core/pobjects/filters/CCITTFax$Code;

    .line 186
    sget-object v1, Lorg/icepdf/index/core/pobjects/filters/CCITTFax;->_modecodes:[Ljava/lang/String;

    invoke-static {v1}, Lorg/icepdf/index/core/pobjects/filters/CCITTFax;->convertStringArrayToCodeArray2D([Ljava/lang/String;)[[Lorg/icepdf/index/core/pobjects/filters/CCITTFax$Code;

    move-result-object v1

    sput-object v1, Lorg/icepdf/index/core/pobjects/filters/CCITTFax;->modecodes:[[Lorg/icepdf/index/core/pobjects/filters/CCITTFax$Code;

    .line 243
    sput v4, Lorg/icepdf/index/core/pobjects/filters/CCITTFax;->black:I

    .line 244
    sput v5, Lorg/icepdf/index/core/pobjects/filters/CCITTFax;->white:I

    .line 263
    sput-boolean v4, Lorg/icepdf/index/core/pobjects/filters/CCITTFax;->USE_JAI_IMAGE_LIBRARY:Z

    .line 267
    :try_start_0
    const-string/jumbo v1, "javax.media.jai.JAI"

    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 268
    .local v0, "jaiClass":Ljava/lang/Class;
    if-eqz v0, :cond_0

    .line 269
    const/4 v1, 0x1

    sput-boolean v1, Lorg/icepdf/index/core/pobjects/filters/CCITTFax;->USE_JAI_IMAGE_LIBRARY:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 273
    :cond_0
    :goto_0
    return-void

    .line 271
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 128
    return-void
.end method

.method public static Group4Decode(Ljava/io/InputStream;Ljava/io/OutputStream;IZ)V
    .locals 12
    .param p0, "in"    # Ljava/io/InputStream;
    .param p1, "out"    # Ljava/io/OutputStream;
    .param p2, "width"    # I
    .param p3, "blackIs1"    # Z

    .prologue
    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 483
    new-instance v3, Lorg/icepdf/index/core/io/BitStream;

    invoke-direct {v3, p0}, Lorg/icepdf/index/core/io/BitStream;-><init>(Ljava/io/InputStream;)V

    .line 484
    .local v3, "inb":Lorg/icepdf/index/core/io/BitStream;
    new-instance v5, Lorg/icepdf/index/core/io/BitStream;

    invoke-direct {v5, p1}, Lorg/icepdf/index/core/io/BitStream;-><init>(Ljava/io/OutputStream;)V

    .line 486
    .local v5, "outb":Lorg/icepdf/index/core/io/BitStream;
    sput v8, Lorg/icepdf/index/core/pobjects/filters/CCITTFax;->black:I

    .line 487
    sput v7, Lorg/icepdf/index/core/pobjects/filters/CCITTFax;->white:I

    .line 490
    if-eqz p3, :cond_0

    .line 491
    sput v7, Lorg/icepdf/index/core/pobjects/filters/CCITTFax;->black:I

    .line 492
    sput v8, Lorg/icepdf/index/core/pobjects/filters/CCITTFax;->white:I

    .line 495
    :cond_0
    new-instance v0, Lorg/icepdf/index/core/pobjects/filters/CCITTFax$Code;

    invoke-direct {v0}, Lorg/icepdf/index/core/pobjects/filters/CCITTFax$Code;-><init>()V

    .line 498
    .local v0, "code":Lorg/icepdf/index/core/pobjects/filters/CCITTFax$Code;
    :try_start_0
    new-instance v2, Lorg/icepdf/index/core/pobjects/filters/G4State;

    invoke-direct {v2, p2}, Lorg/icepdf/index/core/pobjects/filters/G4State;-><init>(I)V

    .line 499
    .local v2, "graphicState":Lorg/icepdf/index/core/pobjects/filters/G4State;
    :cond_1
    :goto_0
    invoke-virtual {v3}, Lorg/icepdf/index/core/io/BitStream;->atEndOfFile()Z

    move-result v6

    if-nez v6, :cond_a

    .line 500
    invoke-static {v3, v0}, Lorg/icepdf/index/core/pobjects/filters/CCITTFax;->readmode(Lorg/icepdf/index/core/io/BitStream;Lorg/icepdf/index/core/pobjects/filters/CCITTFax$Code;)I

    move-result v4

    .line 501
    .local v4, "mode":I
    packed-switch v4, :pswitch_data_0

    .line 561
    :cond_2
    :goto_1
    :pswitch_0
    iget v6, v2, Lorg/icepdf/index/core/pobjects/filters/G4State;->a0:I

    iget v9, v2, Lorg/icepdf/index/core/pobjects/filters/G4State;->width:I

    if-lt v6, v9, :cond_1

    .line 562
    invoke-static {v5, v2}, Lorg/icepdf/index/core/pobjects/filters/CCITTFax;->resetRuns(Lorg/icepdf/index/core/io/BitStream;Lorg/icepdf/index/core/pobjects/filters/G4State;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 571
    .end local v2    # "graphicState":Lorg/icepdf/index/core/pobjects/filters/G4State;
    .end local v4    # "mode":I
    :catch_0
    move-exception v1

    .line 572
    .local v1, "e":Ljava/lang/Exception;
    const-string/jumbo v6, "CCITTF"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "Exception :"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 574
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_2
    return-void

    .line 503
    .restart local v2    # "graphicState":Lorg/icepdf/index/core/pobjects/filters/G4State;
    .restart local v4    # "mode":I
    :pswitch_1
    :try_start_1
    invoke-static {v2}, Lorg/icepdf/index/core/pobjects/filters/CCITTFax;->decodePass(Lorg/icepdf/index/core/pobjects/filters/G4State;)V

    goto :goto_0

    .line 506
    :pswitch_2
    invoke-static {v3, v5, v2, v0}, Lorg/icepdf/index/core/pobjects/filters/CCITTFax;->decodeHorizontal(Lorg/icepdf/index/core/io/BitStream;Lorg/icepdf/index/core/io/BitStream;Lorg/icepdf/index/core/pobjects/filters/G4State;Lorg/icepdf/index/core/pobjects/filters/CCITTFax$Code;)V

    .line 507
    invoke-static {v3, v5, v2, v0}, Lorg/icepdf/index/core/pobjects/filters/CCITTFax;->decodeHorizontal(Lorg/icepdf/index/core/io/BitStream;Lorg/icepdf/index/core/io/BitStream;Lorg/icepdf/index/core/pobjects/filters/G4State;Lorg/icepdf/index/core/pobjects/filters/CCITTFax$Code;)V

    .line 508
    invoke-static {v2}, Lorg/icepdf/index/core/pobjects/filters/CCITTFax;->detectB1(Lorg/icepdf/index/core/pobjects/filters/G4State;)V

    goto :goto_1

    .line 511
    :pswitch_3
    invoke-static {v2}, Lorg/icepdf/index/core/pobjects/filters/CCITTFax;->detectB1(Lorg/icepdf/index/core/pobjects/filters/G4State;)V

    .line 512
    iget v6, v2, Lorg/icepdf/index/core/pobjects/filters/G4State;->b1:I

    iget v9, v2, Lorg/icepdf/index/core/pobjects/filters/G4State;->a0:I

    sub-int/2addr v6, v9

    invoke-static {v6, v2, v5}, Lorg/icepdf/index/core/pobjects/filters/CCITTFax;->addRun(ILorg/icepdf/index/core/pobjects/filters/G4State;Lorg/icepdf/index/core/io/BitStream;)V

    .line 513
    iget-boolean v6, v2, Lorg/icepdf/index/core/pobjects/filters/G4State;->white:Z

    if-nez v6, :cond_3

    move v6, v7

    :goto_3
    iput-boolean v6, v2, Lorg/icepdf/index/core/pobjects/filters/G4State;->white:Z

    .line 514
    iget v6, v2, Lorg/icepdf/index/core/pobjects/filters/G4State;->b1:I

    iget-object v9, v2, Lorg/icepdf/index/core/pobjects/filters/G4State;->ref:[I

    iget v10, v2, Lorg/icepdf/index/core/pobjects/filters/G4State;->refIndex:I

    add-int/lit8 v11, v10, 0x1

    iput v11, v2, Lorg/icepdf/index/core/pobjects/filters/G4State;->refIndex:I

    aget v9, v9, v10

    add-int/2addr v6, v9

    iput v6, v2, Lorg/icepdf/index/core/pobjects/filters/G4State;->b1:I

    goto :goto_1

    :cond_3
    move v6, v8

    .line 513
    goto :goto_3

    .line 517
    :pswitch_4
    invoke-static {v2}, Lorg/icepdf/index/core/pobjects/filters/CCITTFax;->detectB1(Lorg/icepdf/index/core/pobjects/filters/G4State;)V

    .line 518
    iget v6, v2, Lorg/icepdf/index/core/pobjects/filters/G4State;->b1:I

    iget v9, v2, Lorg/icepdf/index/core/pobjects/filters/G4State;->a0:I

    sub-int/2addr v6, v9

    add-int/lit8 v6, v6, 0x1

    invoke-static {v6, v2, v5}, Lorg/icepdf/index/core/pobjects/filters/CCITTFax;->addRun(ILorg/icepdf/index/core/pobjects/filters/G4State;Lorg/icepdf/index/core/io/BitStream;)V

    .line 519
    iget-boolean v6, v2, Lorg/icepdf/index/core/pobjects/filters/G4State;->white:Z

    if-nez v6, :cond_4

    move v6, v7

    :goto_4
    iput-boolean v6, v2, Lorg/icepdf/index/core/pobjects/filters/G4State;->white:Z

    .line 520
    iget v6, v2, Lorg/icepdf/index/core/pobjects/filters/G4State;->b1:I

    iget-object v9, v2, Lorg/icepdf/index/core/pobjects/filters/G4State;->ref:[I

    iget v10, v2, Lorg/icepdf/index/core/pobjects/filters/G4State;->refIndex:I

    add-int/lit8 v11, v10, 0x1

    iput v11, v2, Lorg/icepdf/index/core/pobjects/filters/G4State;->refIndex:I

    aget v9, v9, v10

    add-int/2addr v6, v9

    iput v6, v2, Lorg/icepdf/index/core/pobjects/filters/G4State;->b1:I

    goto :goto_1

    :cond_4
    move v6, v8

    .line 519
    goto :goto_4

    .line 523
    :pswitch_5
    invoke-static {v2}, Lorg/icepdf/index/core/pobjects/filters/CCITTFax;->detectB1(Lorg/icepdf/index/core/pobjects/filters/G4State;)V

    .line 524
    iget v6, v2, Lorg/icepdf/index/core/pobjects/filters/G4State;->b1:I

    iget v9, v2, Lorg/icepdf/index/core/pobjects/filters/G4State;->a0:I

    sub-int/2addr v6, v9

    add-int/lit8 v6, v6, 0x2

    invoke-static {v6, v2, v5}, Lorg/icepdf/index/core/pobjects/filters/CCITTFax;->addRun(ILorg/icepdf/index/core/pobjects/filters/G4State;Lorg/icepdf/index/core/io/BitStream;)V

    .line 525
    iget-boolean v6, v2, Lorg/icepdf/index/core/pobjects/filters/G4State;->white:Z

    if-nez v6, :cond_5

    move v6, v7

    :goto_5
    iput-boolean v6, v2, Lorg/icepdf/index/core/pobjects/filters/G4State;->white:Z

    .line 526
    iget v6, v2, Lorg/icepdf/index/core/pobjects/filters/G4State;->b1:I

    iget-object v9, v2, Lorg/icepdf/index/core/pobjects/filters/G4State;->ref:[I

    iget v10, v2, Lorg/icepdf/index/core/pobjects/filters/G4State;->refIndex:I

    add-int/lit8 v11, v10, 0x1

    iput v11, v2, Lorg/icepdf/index/core/pobjects/filters/G4State;->refIndex:I

    aget v9, v9, v10

    add-int/2addr v6, v9

    iput v6, v2, Lorg/icepdf/index/core/pobjects/filters/G4State;->b1:I

    goto/16 :goto_1

    :cond_5
    move v6, v8

    .line 525
    goto :goto_5

    .line 529
    :pswitch_6
    invoke-static {v2}, Lorg/icepdf/index/core/pobjects/filters/CCITTFax;->detectB1(Lorg/icepdf/index/core/pobjects/filters/G4State;)V

    .line 530
    iget v6, v2, Lorg/icepdf/index/core/pobjects/filters/G4State;->b1:I

    iget v9, v2, Lorg/icepdf/index/core/pobjects/filters/G4State;->a0:I

    sub-int/2addr v6, v9

    add-int/lit8 v6, v6, 0x3

    invoke-static {v6, v2, v5}, Lorg/icepdf/index/core/pobjects/filters/CCITTFax;->addRun(ILorg/icepdf/index/core/pobjects/filters/G4State;Lorg/icepdf/index/core/io/BitStream;)V

    .line 531
    iget-boolean v6, v2, Lorg/icepdf/index/core/pobjects/filters/G4State;->white:Z

    if-nez v6, :cond_6

    move v6, v7

    :goto_6
    iput-boolean v6, v2, Lorg/icepdf/index/core/pobjects/filters/G4State;->white:Z

    .line 532
    iget v6, v2, Lorg/icepdf/index/core/pobjects/filters/G4State;->b1:I

    iget-object v9, v2, Lorg/icepdf/index/core/pobjects/filters/G4State;->ref:[I

    iget v10, v2, Lorg/icepdf/index/core/pobjects/filters/G4State;->refIndex:I

    add-int/lit8 v11, v10, 0x1

    iput v11, v2, Lorg/icepdf/index/core/pobjects/filters/G4State;->refIndex:I

    aget v9, v9, v10

    add-int/2addr v6, v9

    iput v6, v2, Lorg/icepdf/index/core/pobjects/filters/G4State;->b1:I

    goto/16 :goto_1

    :cond_6
    move v6, v8

    .line 531
    goto :goto_6

    .line 535
    :pswitch_7
    invoke-static {v2}, Lorg/icepdf/index/core/pobjects/filters/CCITTFax;->detectB1(Lorg/icepdf/index/core/pobjects/filters/G4State;)V

    .line 536
    iget v6, v2, Lorg/icepdf/index/core/pobjects/filters/G4State;->b1:I

    iget v9, v2, Lorg/icepdf/index/core/pobjects/filters/G4State;->a0:I

    sub-int/2addr v6, v9

    add-int/lit8 v6, v6, -0x1

    invoke-static {v6, v2, v5}, Lorg/icepdf/index/core/pobjects/filters/CCITTFax;->addRun(ILorg/icepdf/index/core/pobjects/filters/G4State;Lorg/icepdf/index/core/io/BitStream;)V

    .line 537
    iget-boolean v6, v2, Lorg/icepdf/index/core/pobjects/filters/G4State;->white:Z

    if-nez v6, :cond_7

    move v6, v7

    :goto_7
    iput-boolean v6, v2, Lorg/icepdf/index/core/pobjects/filters/G4State;->white:Z

    .line 538
    iget v6, v2, Lorg/icepdf/index/core/pobjects/filters/G4State;->refIndex:I

    if-lez v6, :cond_2

    .line 539
    iget v6, v2, Lorg/icepdf/index/core/pobjects/filters/G4State;->b1:I

    iget-object v9, v2, Lorg/icepdf/index/core/pobjects/filters/G4State;->ref:[I

    iget v10, v2, Lorg/icepdf/index/core/pobjects/filters/G4State;->refIndex:I

    add-int/lit8 v10, v10, -0x1

    iput v10, v2, Lorg/icepdf/index/core/pobjects/filters/G4State;->refIndex:I

    aget v9, v9, v10

    sub-int/2addr v6, v9

    iput v6, v2, Lorg/icepdf/index/core/pobjects/filters/G4State;->b1:I

    goto/16 :goto_1

    :cond_7
    move v6, v8

    .line 537
    goto :goto_7

    .line 542
    :pswitch_8
    invoke-static {v2}, Lorg/icepdf/index/core/pobjects/filters/CCITTFax;->detectB1(Lorg/icepdf/index/core/pobjects/filters/G4State;)V

    .line 543
    iget v6, v2, Lorg/icepdf/index/core/pobjects/filters/G4State;->b1:I

    iget v9, v2, Lorg/icepdf/index/core/pobjects/filters/G4State;->a0:I

    sub-int/2addr v6, v9

    add-int/lit8 v6, v6, -0x2

    invoke-static {v6, v2, v5}, Lorg/icepdf/index/core/pobjects/filters/CCITTFax;->addRun(ILorg/icepdf/index/core/pobjects/filters/G4State;Lorg/icepdf/index/core/io/BitStream;)V

    .line 544
    iget-boolean v6, v2, Lorg/icepdf/index/core/pobjects/filters/G4State;->white:Z

    if-nez v6, :cond_8

    move v6, v7

    :goto_8
    iput-boolean v6, v2, Lorg/icepdf/index/core/pobjects/filters/G4State;->white:Z

    .line 545
    iget v6, v2, Lorg/icepdf/index/core/pobjects/filters/G4State;->refIndex:I

    if-lez v6, :cond_2

    .line 546
    iget v6, v2, Lorg/icepdf/index/core/pobjects/filters/G4State;->b1:I

    iget-object v9, v2, Lorg/icepdf/index/core/pobjects/filters/G4State;->ref:[I

    iget v10, v2, Lorg/icepdf/index/core/pobjects/filters/G4State;->refIndex:I

    add-int/lit8 v10, v10, -0x1

    iput v10, v2, Lorg/icepdf/index/core/pobjects/filters/G4State;->refIndex:I

    aget v9, v9, v10

    sub-int/2addr v6, v9

    iput v6, v2, Lorg/icepdf/index/core/pobjects/filters/G4State;->b1:I

    goto/16 :goto_1

    :cond_8
    move v6, v8

    .line 544
    goto :goto_8

    .line 549
    :pswitch_9
    invoke-static {v2}, Lorg/icepdf/index/core/pobjects/filters/CCITTFax;->detectB1(Lorg/icepdf/index/core/pobjects/filters/G4State;)V

    .line 550
    iget v6, v2, Lorg/icepdf/index/core/pobjects/filters/G4State;->b1:I

    iget v9, v2, Lorg/icepdf/index/core/pobjects/filters/G4State;->a0:I

    sub-int/2addr v6, v9

    add-int/lit8 v6, v6, -0x3

    invoke-static {v6, v2, v5}, Lorg/icepdf/index/core/pobjects/filters/CCITTFax;->addRun(ILorg/icepdf/index/core/pobjects/filters/G4State;Lorg/icepdf/index/core/io/BitStream;)V

    .line 551
    iget-boolean v6, v2, Lorg/icepdf/index/core/pobjects/filters/G4State;->white:Z

    if-nez v6, :cond_9

    move v6, v7

    :goto_9
    iput-boolean v6, v2, Lorg/icepdf/index/core/pobjects/filters/G4State;->white:Z

    .line 552
    iget v6, v2, Lorg/icepdf/index/core/pobjects/filters/G4State;->refIndex:I

    if-lez v6, :cond_2

    .line 553
    iget v6, v2, Lorg/icepdf/index/core/pobjects/filters/G4State;->b1:I

    iget-object v9, v2, Lorg/icepdf/index/core/pobjects/filters/G4State;->ref:[I

    iget v10, v2, Lorg/icepdf/index/core/pobjects/filters/G4State;->refIndex:I

    add-int/lit8 v10, v10, -0x1

    iput v10, v2, Lorg/icepdf/index/core/pobjects/filters/G4State;->refIndex:I

    aget v9, v9, v10

    sub-int/2addr v6, v9

    iput v6, v2, Lorg/icepdf/index/core/pobjects/filters/G4State;->b1:I

    goto/16 :goto_1

    :cond_9
    move v6, v8

    .line 551
    goto :goto_9

    .line 556
    :pswitch_a
    invoke-static {v5, v2}, Lorg/icepdf/index/core/pobjects/filters/CCITTFax;->resetRuns(Lorg/icepdf/index/core/io/BitStream;Lorg/icepdf/index/core/pobjects/filters/G4State;)V

    goto/16 :goto_1

    .line 566
    .end local v4    # "mode":I
    :cond_a
    invoke-virtual {v3}, Lorg/icepdf/index/core/io/BitStream;->close()V

    .line 567
    invoke-virtual {v5}, Lorg/icepdf/index/core/io/BitStream;->close()V

    .line 568
    invoke-virtual {p0}, Ljava/io/InputStream;->close()V

    .line 570
    invoke-virtual {p1}, Ljava/io/OutputStream;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_2

    .line 501
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_0
        :pswitch_0
        :pswitch_a
    .end packed-switch
.end method

.method static addRun(ILorg/icepdf/index/core/pobjects/filters/G4State;Lorg/icepdf/index/core/io/BitStream;)V
    .locals 3
    .param p0, "x"    # I
    .param p1, "s"    # Lorg/icepdf/index/core/pobjects/filters/G4State;
    .param p2, "out"    # Lorg/icepdf/index/core/io/BitStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 350
    iget v0, p1, Lorg/icepdf/index/core/pobjects/filters/G4State;->runLength:I

    add-int/2addr v0, p0

    iput v0, p1, Lorg/icepdf/index/core/pobjects/filters/G4State;->runLength:I

    .line 351
    iget-object v0, p1, Lorg/icepdf/index/core/pobjects/filters/G4State;->cur:[I

    iget v1, p1, Lorg/icepdf/index/core/pobjects/filters/G4State;->curIndex:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p1, Lorg/icepdf/index/core/pobjects/filters/G4State;->curIndex:I

    iget v2, p1, Lorg/icepdf/index/core/pobjects/filters/G4State;->runLength:I

    aput v2, v0, v1

    .line 352
    iget v0, p1, Lorg/icepdf/index/core/pobjects/filters/G4State;->a0:I

    add-int/2addr v0, p0

    iput v0, p1, Lorg/icepdf/index/core/pobjects/filters/G4State;->a0:I

    .line 353
    iget v0, p1, Lorg/icepdf/index/core/pobjects/filters/G4State;->runLength:I

    if-lez v0, :cond_0

    .line 355
    iget-boolean v0, p1, Lorg/icepdf/index/core/pobjects/filters/G4State;->white:Z

    if-eqz v0, :cond_1

    sget v0, Lorg/icepdf/index/core/pobjects/filters/CCITTFax;->white:I

    :goto_0
    iget v1, p1, Lorg/icepdf/index/core/pobjects/filters/G4State;->runLength:I

    invoke-virtual {p2, v0, v1}, Lorg/icepdf/index/core/io/BitStream;->putRunBits(II)V

    .line 357
    :cond_0
    invoke-virtual {p2}, Lorg/icepdf/index/core/io/BitStream;->close()V

    .line 358
    const/4 v0, 0x0

    iput v0, p1, Lorg/icepdf/index/core/pobjects/filters/G4State;->runLength:I

    .line 359
    return-void

    .line 355
    :cond_1
    sget v0, Lorg/icepdf/index/core/pobjects/filters/CCITTFax;->black:I

    goto :goto_0
.end method

.method public static attemptDeriveBufferedImageFromBytes(Lorg/icepdf/index/core/pobjects/Stream;Lorg/icepdf/index/core/util/Library;Lorg/icepdf/index/core/util/Hashtable;I)Landroid/graphics/Bitmap;
    .locals 34
    .param p0, "stream"    # Lorg/icepdf/index/core/pobjects/Stream;
    .param p1, "library"    # Lorg/icepdf/index/core/util/Library;
    .param p2, "streamDictionary"    # Lorg/icepdf/index/core/util/Hashtable;
    .param p3, "fill"    # I

    .prologue
    .line 578
    sget-boolean v32, Lorg/icepdf/index/core/pobjects/filters/CCITTFax;->USE_JAI_IMAGE_LIBRARY:Z

    if-nez v32, :cond_1

    .line 579
    const/16 v22, 0x0

    .line 741
    :cond_0
    :goto_0
    return-object v22

    .line 584
    :cond_1
    const-string/jumbo v32, "DecodeParms"

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, v32

    invoke-virtual {v0, v1, v2}, Lorg/icepdf/index/core/util/Library;->getDictionary(Lorg/icepdf/index/core/util/Hashtable;Ljava/lang/String;)Lorg/icepdf/index/core/util/Hashtable;

    move-result-object v13

    .line 585
    .local v13, "decodeParmsDictionary":Lorg/icepdf/index/core/util/Hashtable;
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v13}, Lorg/icepdf/index/core/pobjects/Stream;->getBlackIs1OrNull(Lorg/icepdf/index/core/util/Library;Lorg/icepdf/index/core/util/Hashtable;)Ljava/lang/Boolean;

    move-result-object v8

    .line 586
    .local v8, "blackIs1Obj":Ljava/lang/Boolean;
    const-string/jumbo v32, "K"

    move-object/from16 v0, p1

    move-object/from16 v1, v32

    invoke-virtual {v0, v13, v1}, Lorg/icepdf/index/core/util/Library;->getFloat(Lorg/icepdf/index/core/util/Hashtable;Ljava/lang/String;)F

    move-result v25

    .line 588
    .local v25, "k":F
    const/16 v17, 0x0

    .line 590
    .local v17, "hasHeader":Z
    invoke-virtual/range {p0 .. p0}, Lorg/icepdf/index/core/pobjects/Stream;->getInputStreamForDecodedStreamBytes()Ljava/io/InputStream;

    move-result-object v23

    .line 591
    .local v23, "input":Ljava/io/InputStream;
    if-nez v23, :cond_2

    .line 592
    const/16 v22, 0x0

    goto :goto_0

    .line 593
    :cond_2
    new-instance v24, Lorg/icepdf/index/core/io/ZeroPaddedInputStream;

    move-object/from16 v0, v24

    move-object/from16 v1, v23

    invoke-direct {v0, v1}, Lorg/icepdf/index/core/io/ZeroPaddedInputStream;-><init>(Ljava/io/InputStream;)V

    .line 594
    .end local v23    # "input":Ljava/io/InputStream;
    .local v24, "input":Ljava/io/InputStream;
    new-instance v9, Ljava/io/BufferedInputStream;

    const/16 v32, 0x400

    move-object/from16 v0, v24

    move/from16 v1, v32

    invoke-direct {v9, v0, v1}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;I)V

    .line 595
    .local v9, "bufferedInput":Ljava/io/BufferedInputStream;
    const/16 v32, 0x4

    move/from16 v0, v32

    invoke-virtual {v9, v0}, Ljava/io/BufferedInputStream;->mark(I)V

    .line 597
    :try_start_0
    invoke-virtual {v9}, Ljava/io/BufferedInputStream;->read()I

    move-result v18

    .line 598
    .local v18, "hb1":I
    invoke-virtual {v9}, Ljava/io/BufferedInputStream;->read()I

    move-result v19

    .line 599
    .local v19, "hb2":I
    invoke-virtual {v9}, Ljava/io/BufferedInputStream;->reset()V

    .line 600
    if-ltz v18, :cond_3

    if-gez v19, :cond_4

    .line 601
    :cond_3
    invoke-virtual/range {v24 .. v24}, Ljava/io/InputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 602
    const/16 v22, 0x0

    goto :goto_0

    .line 604
    :cond_4
    const/16 v32, 0x4d

    move/from16 v0, v18

    move/from16 v1, v32

    if-ne v0, v1, :cond_5

    const/16 v32, 0x4d

    move/from16 v0, v19

    move/from16 v1, v32

    if-eq v0, v1, :cond_6

    :cond_5
    const/16 v32, 0x49

    move/from16 v0, v18

    move/from16 v1, v32

    if-ne v0, v1, :cond_e

    const/16 v32, 0x49

    move/from16 v0, v19

    move/from16 v1, v32

    if-ne v0, v1, :cond_e

    :cond_6
    const/16 v17, 0x1

    .line 613
    :goto_1
    move-object/from16 v23, v9

    .line 615
    .end local v24    # "input":Ljava/io/InputStream;
    .restart local v23    # "input":Ljava/io/InputStream;
    const/16 v22, 0x0

    .line 617
    .local v22, "img":Landroid/graphics/Bitmap;
    const/4 v15, 0x0

    .line 618
    .local v15, "fakeHeaderBytes":[B
    if-nez v17, :cond_14

    .line 624
    const/16 v32, 0xae

    move/from16 v0, v32

    new-array v15, v0, [B

    .end local v15    # "fakeHeaderBytes":[B
    fill-array-data v15, :array_0

    .line 654
    .restart local v15    # "fakeHeaderBytes":[B
    const/16 v28, 0x0

    .line 655
    .local v28, "pdfStatesBlackAndWhite":Z
    const/4 v7, 0x0

    .line 656
    .local v7, "blackIs1":Z
    if-eqz v8, :cond_7

    .line 657
    invoke-virtual {v8}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v7

    .line 658
    const/16 v28, 0x1

    .line 660
    :cond_7
    const-string/jumbo v32, "Width"

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, v32

    invoke-virtual {v0, v1, v2}, Lorg/icepdf/index/core/util/Library;->getInt(Lorg/icepdf/index/core/util/Hashtable;Ljava/lang/String;)I

    move-result v31

    .line 661
    .local v31, "width":I
    const-string/jumbo v32, "Height"

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, v32

    invoke-virtual {v0, v1, v2}, Lorg/icepdf/index/core/util/Library;->getInt(Lorg/icepdf/index/core/util/Hashtable;Ljava/lang/String;)I

    move-result v20

    .line 663
    .local v20, "height":I
    const-string/jumbo v32, "Columns"

    move-object/from16 v0, p1

    move-object/from16 v1, v32

    invoke-virtual {v0, v13, v1}, Lorg/icepdf/index/core/util/Library;->getObject(Lorg/icepdf/index/core/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v11

    .line 664
    .local v11, "columnsObj":Ljava/lang/Object;
    if-eqz v11, :cond_8

    instance-of v0, v11, Ljava/lang/Number;

    move/from16 v32, v0

    if-eqz v32, :cond_8

    .line 665
    check-cast v11, Ljava/lang/Number;

    .end local v11    # "columnsObj":Ljava/lang/Object;
    invoke-virtual {v11}, Ljava/lang/Number;->intValue()I

    move-result v10

    .line 666
    .local v10, "columns":I
    move/from16 v0, v31

    if-le v10, v0, :cond_8

    .line 667
    move/from16 v31, v10

    .line 670
    .end local v10    # "columns":I
    :cond_8
    const/16 v32, 0x1e

    move/from16 v0, v31

    move/from16 v1, v32

    invoke-static {v0, v15, v1}, Lorg/icepdf/index/core/util/Utils;->setIntIntoByteArrayBE(I[BI)V

    .line 671
    const/16 v32, 0x2a

    move/from16 v0, v20

    move/from16 v1, v32

    invoke-static {v0, v15, v1}, Lorg/icepdf/index/core/util/Utils;->setIntIntoByteArrayBE(I[BI)V

    .line 672
    const-string/jumbo v32, "BitsPerComponent"

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, v32

    invoke-virtual {v0, v1, v2}, Lorg/icepdf/index/core/util/Library;->getObject(Lorg/icepdf/index/core/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    .line 674
    .local v6, "bitsPerComponent":Ljava/lang/Object;
    if-eqz v6, :cond_9

    instance-of v0, v6, Ljava/lang/Number;

    move/from16 v32, v0

    if-eqz v32, :cond_9

    .line 675
    check-cast v6, Ljava/lang/Number;

    .end local v6    # "bitsPerComponent":Ljava/lang/Object;
    invoke-virtual {v6}, Ljava/lang/Number;->shortValue()S

    move-result v32

    const/16 v33, 0x36

    move/from16 v0, v32

    move/from16 v1, v33

    invoke-static {v0, v15, v1}, Lorg/icepdf/index/core/util/Utils;->setShortIntoByteArrayBE(S[BI)V

    .line 677
    :cond_9
    const/4 v12, 0x1

    .line 678
    .local v12, "compression":S
    const/16 v32, 0x0

    cmpg-float v32, v25, v32

    if-gez v32, :cond_f

    const/4 v12, 0x4

    .line 681
    :cond_a
    :goto_2
    const/16 v32, 0x42

    move/from16 v0, v32

    invoke-static {v12, v15, v0}, Lorg/icepdf/index/core/util/Utils;->setShortIntoByteArrayBE(S[BI)V

    .line 682
    const/16 v29, 0x0

    .line 686
    .local v29, "photometricInterpretation":S
    if-eqz v28, :cond_b

    .line 687
    if-nez v7, :cond_b

    .line 688
    const/16 v29, 0x1

    .line 690
    :cond_b
    const/16 v32, 0x4e

    move/from16 v0, v29

    move/from16 v1, v32

    invoke-static {v0, v15, v1}, Lorg/icepdf/index/core/util/Utils;->setShortIntoByteArrayBE(S[BI)V

    .line 692
    const/16 v32, 0x66

    move/from16 v0, v20

    move/from16 v1, v32

    invoke-static {v0, v15, v1}, Lorg/icepdf/index/core/util/Utils;->setIntIntoByteArrayBE(I[BI)V

    .line 693
    const v26, 0x7ffffffe

    .line 694
    .local v26, "lengthOfCompressedData":I
    const-string/jumbo v32, "Length"

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, v32

    invoke-virtual {v0, v1, v2}, Lorg/icepdf/index/core/util/Library;->getObject(Lorg/icepdf/index/core/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v27

    .line 695
    .local v27, "lengthValue":Ljava/lang/Object;
    if-eqz v27, :cond_11

    move-object/from16 v0, v27

    instance-of v0, v0, Ljava/lang/Number;

    move/from16 v32, v0

    if-eqz v32, :cond_11

    .line 696
    check-cast v27, Ljava/lang/Number;

    .end local v27    # "lengthValue":Ljava/lang/Object;
    invoke-virtual/range {v27 .. v27}, Ljava/lang/Number;->intValue()I

    move-result v26

    .line 703
    :cond_c
    :goto_3
    const/16 v32, 0x72

    move/from16 v0, v26

    move/from16 v1, v32

    invoke-static {v0, v15, v1}, Lorg/icepdf/index/core/util/Utils;->setIntIntoByteArrayBE(I[BI)V

    .line 705
    new-instance v16, Ljava/io/ByteArrayInputStream;

    move-object/from16 v0, v16

    invoke-direct {v0, v15}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 706
    .local v16, "fakeHeaderBytesIn":Ljava/io/ByteArrayInputStream;
    new-instance v30, Lorg/icepdf/index/core/io/SequenceInputStream;

    move-object/from16 v0, v30

    move-object/from16 v1, v16

    move-object/from16 v2, v23

    invoke-direct {v0, v1, v2}, Lorg/icepdf/index/core/io/SequenceInputStream;-><init>(Ljava/io/InputStream;Ljava/io/InputStream;)V

    .line 708
    .local v30, "sin":Lorg/icepdf/index/core/io/SequenceInputStream;
    move-object/from16 v0, v30

    move-object/from16 v1, p1

    move/from16 v2, v26

    move/from16 v3, v31

    move/from16 v4, v20

    invoke-static {v0, v1, v2, v3, v4}, Lorg/icepdf/index/core/pobjects/filters/CCITTFax;->deriveBufferedImageFromTIFFBytes(Ljava/io/InputStream;Lorg/icepdf/index/core/util/Library;III)Landroid/graphics/Bitmap;

    move-result-object v22

    .line 709
    if-nez v22, :cond_0

    .line 710
    const/16 v21, 0x1

    .local v21, "i":I
    :goto_4
    const/16 v32, 0x2

    move/from16 v0, v21

    move/from16 v1, v32

    if-gt v0, v1, :cond_0

    .line 711
    add-int/lit8 v32, v12, 0x1

    move/from16 v0, v32

    int-to-short v12, v0

    .line 715
    const/16 v32, 0x4

    move/from16 v0, v32

    if-le v12, v0, :cond_d

    .line 716
    const/4 v12, 0x2

    .line 718
    :cond_d
    const/16 v32, 0x42

    move/from16 v0, v32

    invoke-static {v12, v15, v0}, Lorg/icepdf/index/core/util/Utils;->setShortIntoByteArrayBE(S[BI)V

    .line 719
    invoke-virtual/range {p0 .. p0}, Lorg/icepdf/index/core/pobjects/Stream;->getInputStreamForDecodedStreamBytes()Ljava/io/InputStream;

    move-result-object v23

    .line 720
    if-nez v23, :cond_12

    .line 721
    const/16 v22, 0x0

    goto/16 :goto_0

    .line 604
    .end local v7    # "blackIs1":Z
    .end local v12    # "compression":S
    .end local v15    # "fakeHeaderBytes":[B
    .end local v16    # "fakeHeaderBytesIn":Ljava/io/ByteArrayInputStream;
    .end local v20    # "height":I
    .end local v21    # "i":I
    .end local v22    # "img":Landroid/graphics/Bitmap;
    .end local v23    # "input":Ljava/io/InputStream;
    .end local v26    # "lengthOfCompressedData":I
    .end local v28    # "pdfStatesBlackAndWhite":Z
    .end local v29    # "photometricInterpretation":S
    .end local v30    # "sin":Lorg/icepdf/index/core/io/SequenceInputStream;
    .end local v31    # "width":I
    .restart local v24    # "input":Ljava/io/InputStream;
    :cond_e
    const/16 v17, 0x0

    goto/16 :goto_1

    .line 606
    .end local v18    # "hb1":I
    .end local v19    # "hb2":I
    :catch_0
    move-exception v14

    .line 608
    .local v14, "e":Ljava/io/IOException;
    :try_start_1
    invoke-virtual/range {v24 .. v24}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 611
    :goto_5
    const/16 v22, 0x0

    goto/16 :goto_0

    .line 679
    .end local v14    # "e":Ljava/io/IOException;
    .end local v24    # "input":Ljava/io/InputStream;
    .restart local v7    # "blackIs1":Z
    .restart local v12    # "compression":S
    .restart local v15    # "fakeHeaderBytes":[B
    .restart local v18    # "hb1":I
    .restart local v19    # "hb2":I
    .restart local v20    # "height":I
    .restart local v22    # "img":Landroid/graphics/Bitmap;
    .restart local v23    # "input":Ljava/io/InputStream;
    .restart local v28    # "pdfStatesBlackAndWhite":Z
    .restart local v31    # "width":I
    :cond_f
    const/16 v32, 0x0

    cmpl-float v32, v25, v32

    if-lez v32, :cond_10

    const/4 v12, 0x3

    goto/16 :goto_2

    .line 680
    :cond_10
    const/16 v32, 0x0

    cmpl-float v32, v25, v32

    if-nez v32, :cond_a

    const/4 v12, 0x2

    goto/16 :goto_2

    .line 699
    .restart local v26    # "lengthOfCompressedData":I
    .restart local v27    # "lengthValue":Ljava/lang/Object;
    .restart local v29    # "photometricInterpretation":S
    :cond_11
    mul-int v5, v31, v20

    .line 700
    .local v5, "approxLen":I
    if-lez v5, :cond_c

    .line 701
    move/from16 v26, v5

    goto :goto_3

    .line 722
    .end local v5    # "approxLen":I
    .end local v27    # "lengthValue":Ljava/lang/Object;
    .restart local v16    # "fakeHeaderBytesIn":Ljava/io/ByteArrayInputStream;
    .restart local v21    # "i":I
    .restart local v30    # "sin":Lorg/icepdf/index/core/io/SequenceInputStream;
    :cond_12
    new-instance v24, Lorg/icepdf/index/core/io/ZeroPaddedInputStream;

    move-object/from16 v0, v24

    move-object/from16 v1, v23

    invoke-direct {v0, v1}, Lorg/icepdf/index/core/io/ZeroPaddedInputStream;-><init>(Ljava/io/InputStream;)V

    .line 723
    .end local v23    # "input":Ljava/io/InputStream;
    .restart local v24    # "input":Ljava/io/InputStream;
    new-instance v16, Ljava/io/ByteArrayInputStream;

    .end local v16    # "fakeHeaderBytesIn":Ljava/io/ByteArrayInputStream;
    move-object/from16 v0, v16

    invoke-direct {v0, v15}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 724
    .restart local v16    # "fakeHeaderBytesIn":Ljava/io/ByteArrayInputStream;
    new-instance v30, Lorg/icepdf/index/core/io/SequenceInputStream;

    .end local v30    # "sin":Lorg/icepdf/index/core/io/SequenceInputStream;
    move-object/from16 v0, v30

    move-object/from16 v1, v16

    move-object/from16 v2, v24

    invoke-direct {v0, v1, v2}, Lorg/icepdf/index/core/io/SequenceInputStream;-><init>(Ljava/io/InputStream;Ljava/io/InputStream;)V

    .line 725
    .restart local v30    # "sin":Lorg/icepdf/index/core/io/SequenceInputStream;
    move-object/from16 v0, v30

    move-object/from16 v1, p1

    move/from16 v2, v26

    move/from16 v3, v31

    move/from16 v4, v20

    invoke-static {v0, v1, v2, v3, v4}, Lorg/icepdf/index/core/pobjects/filters/CCITTFax;->deriveBufferedImageFromTIFFBytes(Ljava/io/InputStream;Lorg/icepdf/index/core/util/Library;III)Landroid/graphics/Bitmap;

    move-result-object v22

    .line 726
    if-eqz v22, :cond_13

    move-object/from16 v23, v24

    .line 727
    .end local v24    # "input":Ljava/io/InputStream;
    .restart local v23    # "input":Ljava/io/InputStream;
    goto/16 :goto_0

    .line 710
    .end local v23    # "input":Ljava/io/InputStream;
    .restart local v24    # "input":Ljava/io/InputStream;
    :cond_13
    add-int/lit8 v21, v21, 0x1

    move-object/from16 v23, v24

    .end local v24    # "input":Ljava/io/InputStream;
    .restart local v23    # "input":Ljava/io/InputStream;
    goto :goto_4

    .line 731
    .end local v7    # "blackIs1":Z
    .end local v12    # "compression":S
    .end local v16    # "fakeHeaderBytesIn":Ljava/io/ByteArrayInputStream;
    .end local v20    # "height":I
    .end local v21    # "i":I
    .end local v26    # "lengthOfCompressedData":I
    .end local v28    # "pdfStatesBlackAndWhite":Z
    .end local v29    # "photometricInterpretation":S
    .end local v30    # "sin":Lorg/icepdf/index/core/io/SequenceInputStream;
    .end local v31    # "width":I
    :cond_14
    const-string/jumbo v32, "Width"

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, v32

    invoke-virtual {v0, v1, v2}, Lorg/icepdf/index/core/util/Library;->getInt(Lorg/icepdf/index/core/util/Hashtable;Ljava/lang/String;)I

    move-result v31

    .line 732
    .restart local v31    # "width":I
    const-string/jumbo v32, "Height"

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, v32

    invoke-virtual {v0, v1, v2}, Lorg/icepdf/index/core/util/Library;->getInt(Lorg/icepdf/index/core/util/Hashtable;Ljava/lang/String;)I

    move-result v20

    .line 733
    .restart local v20    # "height":I
    mul-int v5, v31, v20

    .line 734
    .restart local v5    # "approxLen":I
    move-object/from16 v0, v23

    move-object/from16 v1, p1

    move/from16 v2, v31

    move/from16 v3, v20

    invoke-static {v0, v1, v5, v2, v3}, Lorg/icepdf/index/core/pobjects/filters/CCITTFax;->deriveBufferedImageFromTIFFBytes(Ljava/io/InputStream;Lorg/icepdf/index/core/util/Library;III)Landroid/graphics/Bitmap;

    move-result-object v22

    goto/16 :goto_0

    .line 609
    .end local v5    # "approxLen":I
    .end local v15    # "fakeHeaderBytes":[B
    .end local v18    # "hb1":I
    .end local v19    # "hb2":I
    .end local v20    # "height":I
    .end local v22    # "img":Landroid/graphics/Bitmap;
    .end local v23    # "input":Ljava/io/InputStream;
    .end local v31    # "width":I
    .restart local v14    # "e":Ljava/io/IOException;
    .restart local v24    # "input":Ljava/io/InputStream;
    :catch_1
    move-exception v32

    goto :goto_5

    .line 624
    :array_0
    .array-data 1
        0x4dt
        0x4dt
        0x0t
        0x2at
        0x0t
        0x0t
        0x0t
        0x8t
        0x0t
        0xct
        0x0t
        -0x2t
        0x0t
        0x4t
        0x0t
        0x0t
        0x0t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x0t
        0x0t
        0x4t
        0x0t
        0x0t
        0x0t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x1t
        0x0t
        0x4t
        0x0t
        0x0t
        0x0t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x2t
        0x0t
        0x3t
        0x0t
        0x0t
        0x0t
        0x1t
        0x0t
        0x1t
        0x0t
        0x0t
        0x1t
        0x3t
        0x0t
        0x3t
        0x0t
        0x0t
        0x0t
        0x1t
        0x0t
        0x1t
        0x0t
        0x0t
        0x1t
        0x6t
        0x0t
        0x3t
        0x0t
        0x0t
        0x0t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x11t
        0x0t
        0x4t
        0x0t
        0x0t
        0x0t
        0x1t
        0x0t
        0x0t
        0x0t
        -0x52t
        0x1t
        0x16t
        0x0t
        0x4t
        0x0t
        0x0t
        0x0t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x17t
        0x0t
        0x4t
        0x0t
        0x0t
        0x0t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x1at
        0x0t
        0x5t
        0x0t
        0x0t
        0x0t
        0x1t
        0x0t
        0x0t
        0x0t
        -0x62t
        0x1t
        0x1bt
        0x0t
        0x5t
        0x0t
        0x0t
        0x0t
        0x1t
        0x0t
        0x0t
        0x0t
        -0x5at
        0x1t
        0x28t
        0x0t
        0x3t
        0x0t
        0x0t
        0x0t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x0t
        0x0t
        0x0t
        0x1t
        0x0t
        0x0t
        0x0t
        0x1t
        0x0t
        0x0t
        0x0t
        0x1t
    .end array-data
.end method

.method private static convertStringArrayToCodeArray2D([Ljava/lang/String;)[[Lorg/icepdf/index/core/pobjects/filters/CCITTFax$Code;
    .locals 10
    .param p0, "strArray"    # [Ljava/lang/String;

    .prologue
    .line 189
    array-length v7, p0

    .line 192
    .local v7, "len":I
    const/16 v8, 0x40

    new-array v1, v8, [I

    .line 193
    .local v1, "codeLengths":[I
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    if-ge v4, v7, :cond_0

    .line 194
    aget-object v8, p0, v4

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v3

    .line 195
    .local v3, "entryLength":I
    aget v8, v1, v3

    add-int/lit8 v8, v8, 0x1

    aput v8, v1, v3

    .line 193
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 204
    .end local v3    # "entryLength":I
    :cond_0
    array-length v8, v1

    add-int/lit8 v6, v8, -0x1

    .line 205
    .local v6, "largestLength":I
    :goto_1
    if-lez v6, :cond_1

    aget v8, v1, v6

    if-nez v8, :cond_1

    .line 206
    add-int/lit8 v6, v6, -0x1

    goto :goto_1

    .line 207
    :cond_1
    add-int/lit8 v8, v6, 0x1

    new-array v0, v8, [[Lorg/icepdf/index/core/pobjects/filters/CCITTFax$Code;

    .line 208
    .local v0, "codeArray":[[Lorg/icepdf/index/core/pobjects/filters/CCITTFax$Code;
    const/4 v4, 0x0

    :goto_2
    array-length v8, v0

    if-ge v4, v8, :cond_2

    .line 209
    aget v8, v1, v4

    new-array v8, v8, [Lorg/icepdf/index/core/pobjects/filters/CCITTFax$Code;

    aput-object v8, v0, v4

    .line 208
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 211
    :cond_2
    const/4 v4, 0x0

    :goto_3
    if-ge v4, v7, :cond_5

    .line 212
    aget-object v8, p0, v4

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v3

    .line 213
    .restart local v3    # "entryLength":I
    aget-object v2, v0, v3

    .line 214
    .local v2, "entries":[Lorg/icepdf/index/core/pobjects/filters/CCITTFax$Code;
    const/4 v5, 0x0

    .local v5, "j":I
    :goto_4
    array-length v8, v2

    if-ge v5, v8, :cond_3

    .line 215
    aget-object v8, v2, v5

    if-nez v8, :cond_4

    .line 216
    new-instance v8, Lorg/icepdf/index/core/pobjects/filters/CCITTFax$Code;

    aget-object v9, p0, v4

    invoke-direct {v8, v9, v4}, Lorg/icepdf/index/core/pobjects/filters/CCITTFax$Code;-><init>(Ljava/lang/String;I)V

    aput-object v8, v2, v5

    .line 211
    :cond_3
    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    .line 214
    :cond_4
    add-int/lit8 v5, v5, 0x1

    goto :goto_4

    .line 222
    .end local v2    # "entries":[Lorg/icepdf/index/core/pobjects/filters/CCITTFax$Code;
    .end local v3    # "entryLength":I
    .end local v5    # "j":I
    :cond_5
    return-object v0
.end method

.method static decodeHorizontal(Lorg/icepdf/index/core/io/BitStream;Lorg/icepdf/index/core/io/BitStream;Lorg/icepdf/index/core/pobjects/filters/G4State;Lorg/icepdf/index/core/pobjects/filters/CCITTFax$Code;)V
    .locals 4
    .param p0, "in"    # Lorg/icepdf/index/core/io/BitStream;
    .param p1, "out"    # Lorg/icepdf/index/core/io/BitStream;
    .param p2, "s"    # Lorg/icepdf/index/core/pobjects/filters/G4State;
    .param p3, "code"    # Lorg/icepdf/index/core/pobjects/filters/CCITTFax$Code;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v3, 0x40

    const/4 v2, 0x0

    .line 418
    const/4 v0, 0x0

    .line 420
    .local v0, "rl":I
    :cond_0
    iget-boolean v1, p2, Lorg/icepdf/index/core/pobjects/filters/G4State;->white:Z

    if-eqz v1, :cond_1

    invoke-static {p0, p3}, Lorg/icepdf/index/core/pobjects/filters/CCITTFax;->findWhite(Lorg/icepdf/index/core/io/BitStream;Lorg/icepdf/index/core/pobjects/filters/CCITTFax$Code;)I

    move-result v0

    .line 421
    :goto_0
    if-ltz v0, :cond_4

    .line 422
    if-ge v0, v3, :cond_3

    .line 423
    iget v1, p2, Lorg/icepdf/index/core/pobjects/filters/G4State;->longrun:I

    add-int/2addr v1, v0

    invoke-static {v1, p2, p1}, Lorg/icepdf/index/core/pobjects/filters/CCITTFax;->addRun(ILorg/icepdf/index/core/pobjects/filters/G4State;Lorg/icepdf/index/core/io/BitStream;)V

    .line 424
    iget-boolean v1, p2, Lorg/icepdf/index/core/pobjects/filters/G4State;->white:Z

    if-nez v1, :cond_2

    const/4 v1, 0x1

    :goto_1
    iput-boolean v1, p2, Lorg/icepdf/index/core/pobjects/filters/G4State;->white:Z

    .line 425
    iput v2, p2, Lorg/icepdf/index/core/pobjects/filters/G4State;->longrun:I

    .line 432
    :goto_2
    if-ge v0, v3, :cond_0

    .line 433
    invoke-virtual {p1}, Lorg/icepdf/index/core/io/BitStream;->close()V

    .line 434
    return-void

    .line 420
    :cond_1
    invoke-static {p0, p3}, Lorg/icepdf/index/core/pobjects/filters/CCITTFax;->findBlack(Lorg/icepdf/index/core/io/BitStream;Lorg/icepdf/index/core/pobjects/filters/CCITTFax$Code;)I

    move-result v0

    goto :goto_0

    :cond_2
    move v1, v2

    .line 424
    goto :goto_1

    .line 427
    :cond_3
    iget v1, p2, Lorg/icepdf/index/core/pobjects/filters/G4State;->longrun:I

    add-int/2addr v1, v0

    iput v1, p2, Lorg/icepdf/index/core/pobjects/filters/G4State;->longrun:I

    goto :goto_2

    .line 430
    :cond_4
    invoke-static {v0, p2, p1}, Lorg/icepdf/index/core/pobjects/filters/CCITTFax;->addRun(ILorg/icepdf/index/core/pobjects/filters/G4State;Lorg/icepdf/index/core/io/BitStream;)V

    goto :goto_2
.end method

.method static decodePass(Lorg/icepdf/index/core/pobjects/filters/G4State;)V
    .locals 4
    .param p0, "s"    # Lorg/icepdf/index/core/pobjects/filters/G4State;

    .prologue
    .line 404
    invoke-static {p0}, Lorg/icepdf/index/core/pobjects/filters/CCITTFax;->detectB1(Lorg/icepdf/index/core/pobjects/filters/G4State;)V

    .line 405
    iget v0, p0, Lorg/icepdf/index/core/pobjects/filters/G4State;->b1:I

    iget-object v1, p0, Lorg/icepdf/index/core/pobjects/filters/G4State;->ref:[I

    iget v2, p0, Lorg/icepdf/index/core/pobjects/filters/G4State;->refIndex:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lorg/icepdf/index/core/pobjects/filters/G4State;->refIndex:I

    aget v1, v1, v2

    add-int/2addr v0, v1

    iput v0, p0, Lorg/icepdf/index/core/pobjects/filters/G4State;->b1:I

    .line 406
    iget v0, p0, Lorg/icepdf/index/core/pobjects/filters/G4State;->runLength:I

    iget v1, p0, Lorg/icepdf/index/core/pobjects/filters/G4State;->b1:I

    iget v2, p0, Lorg/icepdf/index/core/pobjects/filters/G4State;->a0:I

    sub-int/2addr v1, v2

    add-int/2addr v0, v1

    iput v0, p0, Lorg/icepdf/index/core/pobjects/filters/G4State;->runLength:I

    .line 407
    iget v0, p0, Lorg/icepdf/index/core/pobjects/filters/G4State;->b1:I

    iput v0, p0, Lorg/icepdf/index/core/pobjects/filters/G4State;->a0:I

    .line 408
    iget v0, p0, Lorg/icepdf/index/core/pobjects/filters/G4State;->b1:I

    iget-object v1, p0, Lorg/icepdf/index/core/pobjects/filters/G4State;->ref:[I

    iget v2, p0, Lorg/icepdf/index/core/pobjects/filters/G4State;->refIndex:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lorg/icepdf/index/core/pobjects/filters/G4State;->refIndex:I

    aget v1, v1, v2

    add-int/2addr v0, v1

    iput v0, p0, Lorg/icepdf/index/core/pobjects/filters/G4State;->b1:I

    .line 409
    return-void
.end method

.method private static deriveBufferedImageFromTIFFBytes(Ljava/io/InputStream;Lorg/icepdf/index/core/util/Library;III)Landroid/graphics/Bitmap;
    .locals 4
    .param p0, "in"    # Ljava/io/InputStream;
    .param p1, "library"    # Lorg/icepdf/index/core/util/Library;
    .param p2, "compressedBytes"    # I
    .param p3, "width"    # I
    .param p4, "height"    # I

    .prologue
    .line 753
    const/4 v1, 0x0

    .line 756
    .local v1, "img":Landroid/graphics/Bitmap;
    :try_start_0
    new-instance v2, Ljava/lang/Exception;

    const-string/jumbo v3, "deocde tiff not yet supported"

    invoke-direct {v2, v3}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 820
    :catch_0
    move-exception v0

    .line 821
    .local v0, "e":Ljava/lang/Throwable;
    const/4 v1, 0x0

    .line 826
    :try_start_1
    invoke-virtual {p0}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 830
    :goto_0
    return-object v1

    .line 825
    .end local v0    # "e":Ljava/lang/Throwable;
    :catchall_0
    move-exception v2

    .line 826
    :try_start_2
    invoke-virtual {p0}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    .line 828
    :goto_1
    throw v2

    .line 827
    .restart local v0    # "e":Ljava/lang/Throwable;
    :catch_1
    move-exception v2

    goto :goto_0

    .end local v0    # "e":Ljava/lang/Throwable;
    :catch_2
    move-exception v3

    goto :goto_1
.end method

.method static detectB1(Lorg/icepdf/index/core/pobjects/filters/G4State;)V
    .locals 4
    .param p0, "s"    # Lorg/icepdf/index/core/pobjects/filters/G4State;

    .prologue
    .line 384
    iget v1, p0, Lorg/icepdf/index/core/pobjects/filters/G4State;->curIndex:I

    if-eqz v1, :cond_2

    .line 385
    :cond_0
    :goto_0
    iget v1, p0, Lorg/icepdf/index/core/pobjects/filters/G4State;->b1:I

    iget v2, p0, Lorg/icepdf/index/core/pobjects/filters/G4State;->a0:I

    if-gt v1, v2, :cond_2

    iget v1, p0, Lorg/icepdf/index/core/pobjects/filters/G4State;->b1:I

    iget v2, p0, Lorg/icepdf/index/core/pobjects/filters/G4State;->width:I

    if-ge v1, v2, :cond_2

    .line 386
    iget-object v1, p0, Lorg/icepdf/index/core/pobjects/filters/G4State;->ref:[I

    iget v2, p0, Lorg/icepdf/index/core/pobjects/filters/G4State;->refIndex:I

    aget v1, v1, v2

    iget-object v2, p0, Lorg/icepdf/index/core/pobjects/filters/G4State;->ref:[I

    iget v3, p0, Lorg/icepdf/index/core/pobjects/filters/G4State;->refIndex:I

    add-int/lit8 v3, v3, 0x1

    aget v2, v2, v3

    add-int v0, v1, v2

    .line 387
    .local v0, "r":I
    if-nez v0, :cond_1

    .line 388
    iget v1, p0, Lorg/icepdf/index/core/pobjects/filters/G4State;->width:I

    iput v1, p0, Lorg/icepdf/index/core/pobjects/filters/G4State;->b1:I

    .line 389
    :cond_1
    iget v1, p0, Lorg/icepdf/index/core/pobjects/filters/G4State;->b1:I

    add-int/2addr v1, v0

    iput v1, p0, Lorg/icepdf/index/core/pobjects/filters/G4State;->b1:I

    .line 390
    iget v1, p0, Lorg/icepdf/index/core/pobjects/filters/G4State;->refIndex:I

    add-int/lit8 v1, v1, 0x2

    iget-object v2, p0, Lorg/icepdf/index/core/pobjects/filters/G4State;->ref:[I

    array-length v2, v2

    if-ge v1, v2, :cond_0

    .line 391
    iget v1, p0, Lorg/icepdf/index/core/pobjects/filters/G4State;->refIndex:I

    add-int/lit8 v1, v1, 0x2

    iput v1, p0, Lorg/icepdf/index/core/pobjects/filters/G4State;->refIndex:I

    goto :goto_0

    .line 398
    .end local v0    # "r":I
    :cond_2
    return-void
.end method

.method static findBlack(Lorg/icepdf/index/core/io/BitStream;Lorg/icepdf/index/core/pobjects/filters/CCITTFax$Code;)I
    .locals 5
    .param p0, "inb"    # Lorg/icepdf/index/core/io/BitStream;
    .param p1, "code"    # Lorg/icepdf/index/core/pobjects/filters/CCITTFax$Code;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 317
    invoke-virtual {p1}, Lorg/icepdf/index/core/pobjects/filters/CCITTFax$Code;->reset()V

    .line 318
    :cond_0
    invoke-virtual {p0}, Lorg/icepdf/index/core/io/BitStream;->atEndOfFile()Z

    move-result v2

    if-nez v2, :cond_4

    .line 319
    invoke-virtual {p0, v3}, Lorg/icepdf/index/core/io/BitStream;->getBits(I)I

    move-result v0

    .line 320
    .local v0, "i":I
    if-eqz v0, :cond_1

    move v2, v3

    :goto_0
    invoke-virtual {p1, v2}, Lorg/icepdf/index/core/pobjects/filters/CCITTFax$Code;->append(Z)V

    .line 322
    sget-object v2, Lorg/icepdf/index/core/pobjects/filters/CCITTFax;->tbcodes:[[Lorg/icepdf/index/core/pobjects/filters/CCITTFax$Code;

    invoke-static {p1, v2}, Lorg/icepdf/index/core/pobjects/filters/CCITTFax;->findPositionInTable(Lorg/icepdf/index/core/pobjects/filters/CCITTFax$Code;[[Lorg/icepdf/index/core/pobjects/filters/CCITTFax$Code;)I

    move-result v1

    .line 323
    .local v1, "j":I
    if-ltz v1, :cond_2

    .line 340
    .end local v0    # "i":I
    .end local v1    # "j":I
    :goto_1
    return v1

    .restart local v0    # "i":I
    :cond_1
    move v2, v4

    .line 320
    goto :goto_0

    .line 327
    .restart local v1    # "j":I
    :cond_2
    sget-object v2, Lorg/icepdf/index/core/pobjects/filters/CCITTFax;->mbcodes:[[Lorg/icepdf/index/core/pobjects/filters/CCITTFax$Code;

    invoke-static {p1, v2}, Lorg/icepdf/index/core/pobjects/filters/CCITTFax;->findPositionInTable(Lorg/icepdf/index/core/pobjects/filters/CCITTFax$Code;[[Lorg/icepdf/index/core/pobjects/filters/CCITTFax$Code;)I

    move-result v1

    .line 328
    if-ltz v1, :cond_3

    .line 330
    add-int/lit8 v2, v1, 0x1

    mul-int/lit8 v1, v2, 0x40

    goto :goto_1

    .line 332
    :cond_3
    sget-object v2, Lorg/icepdf/index/core/pobjects/filters/CCITTFax;->extmcodes:[[Lorg/icepdf/index/core/pobjects/filters/CCITTFax$Code;

    invoke-static {p1, v2}, Lorg/icepdf/index/core/pobjects/filters/CCITTFax;->findPositionInTable(Lorg/icepdf/index/core/pobjects/filters/CCITTFax$Code;[[Lorg/icepdf/index/core/pobjects/filters/CCITTFax$Code;)I

    move-result v1

    .line 333
    if-ltz v1, :cond_0

    .line 335
    mul-int/lit8 v2, v1, 0x40

    add-int/lit16 v1, v2, 0x700

    goto :goto_1

    .line 338
    .end local v0    # "i":I
    .end local v1    # "j":I
    :cond_4
    invoke-virtual {p0}, Lorg/icepdf/index/core/io/BitStream;->close()V

    move v1, v4

    .line 340
    goto :goto_1
.end method

.method private static findPositionInTable(Lorg/icepdf/index/core/pobjects/filters/CCITTFax$Code;[[Lorg/icepdf/index/core/pobjects/filters/CCITTFax$Code;)I
    .locals 7
    .param p0, "lookFor"    # Lorg/icepdf/index/core/pobjects/filters/CCITTFax$Code;
    .param p1, "lookIn"    # [[Lorg/icepdf/index/core/pobjects/filters/CCITTFax$Code;

    .prologue
    const/4 v5, -0x1

    .line 226
    invoke-virtual {p0}, Lorg/icepdf/index/core/pobjects/filters/CCITTFax$Code;->getLength()I

    move-result v2

    .line 227
    .local v2, "lookForIndex":I
    array-length v6, p1

    if-lt v2, v6, :cond_1

    .line 238
    :cond_0
    :goto_0
    return v5

    .line 229
    :cond_1
    aget-object v3, p1, v2

    .line 230
    .local v3, "lookInWithSameLength":[Lorg/icepdf/index/core/pobjects/filters/CCITTFax$Code;
    if-eqz v3, :cond_0

    .line 232
    array-length v1, v3

    .line 233
    .local v1, "len":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    if-ge v0, v1, :cond_0

    .line 234
    aget-object v4, v3, v0

    .line 235
    .local v4, "potentialMatch":Lorg/icepdf/index/core/pobjects/filters/CCITTFax$Code;
    invoke-virtual {p0, v4}, Lorg/icepdf/index/core/pobjects/filters/CCITTFax$Code;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 236
    invoke-virtual {v4}, Lorg/icepdf/index/core/pobjects/filters/CCITTFax$Code;->getTablePosition()I

    move-result v5

    goto :goto_0

    .line 233
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method static findWhite(Lorg/icepdf/index/core/io/BitStream;Lorg/icepdf/index/core/pobjects/filters/CCITTFax$Code;)I
    .locals 5
    .param p0, "inb"    # Lorg/icepdf/index/core/io/BitStream;
    .param p1, "code"    # Lorg/icepdf/index/core/pobjects/filters/CCITTFax$Code;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 283
    invoke-virtual {p1}, Lorg/icepdf/index/core/pobjects/filters/CCITTFax$Code;->reset()V

    .line 284
    :cond_0
    invoke-virtual {p0}, Lorg/icepdf/index/core/io/BitStream;->atEndOfFile()Z

    move-result v2

    if-nez v2, :cond_4

    .line 285
    invoke-virtual {p0, v3}, Lorg/icepdf/index/core/io/BitStream;->getBits(I)I

    move-result v0

    .line 286
    .local v0, "i":I
    if-eqz v0, :cond_1

    move v2, v3

    :goto_0
    invoke-virtual {p1, v2}, Lorg/icepdf/index/core/pobjects/filters/CCITTFax$Code;->append(Z)V

    .line 288
    sget-object v2, Lorg/icepdf/index/core/pobjects/filters/CCITTFax;->twcodes:[[Lorg/icepdf/index/core/pobjects/filters/CCITTFax$Code;

    invoke-static {p1, v2}, Lorg/icepdf/index/core/pobjects/filters/CCITTFax;->findPositionInTable(Lorg/icepdf/index/core/pobjects/filters/CCITTFax$Code;[[Lorg/icepdf/index/core/pobjects/filters/CCITTFax$Code;)I

    move-result v1

    .line 289
    .local v1, "j":I
    if-ltz v1, :cond_2

    .line 306
    .end local v0    # "i":I
    .end local v1    # "j":I
    :goto_1
    return v1

    .restart local v0    # "i":I
    :cond_1
    move v2, v4

    .line 286
    goto :goto_0

    .line 293
    .restart local v1    # "j":I
    :cond_2
    sget-object v2, Lorg/icepdf/index/core/pobjects/filters/CCITTFax;->mwcodes:[[Lorg/icepdf/index/core/pobjects/filters/CCITTFax$Code;

    invoke-static {p1, v2}, Lorg/icepdf/index/core/pobjects/filters/CCITTFax;->findPositionInTable(Lorg/icepdf/index/core/pobjects/filters/CCITTFax$Code;[[Lorg/icepdf/index/core/pobjects/filters/CCITTFax$Code;)I

    move-result v1

    .line 294
    if-ltz v1, :cond_3

    .line 296
    add-int/lit8 v2, v1, 0x1

    mul-int/lit8 v1, v2, 0x40

    goto :goto_1

    .line 298
    :cond_3
    sget-object v2, Lorg/icepdf/index/core/pobjects/filters/CCITTFax;->extmcodes:[[Lorg/icepdf/index/core/pobjects/filters/CCITTFax$Code;

    invoke-static {p1, v2}, Lorg/icepdf/index/core/pobjects/filters/CCITTFax;->findPositionInTable(Lorg/icepdf/index/core/pobjects/filters/CCITTFax$Code;[[Lorg/icepdf/index/core/pobjects/filters/CCITTFax$Code;)I

    move-result v1

    .line 299
    if-ltz v1, :cond_0

    .line 301
    mul-int/lit8 v2, v1, 0x40

    add-int/lit16 v1, v2, 0x700

    goto :goto_1

    .line 304
    .end local v0    # "i":I
    .end local v1    # "j":I
    :cond_4
    invoke-virtual {p0}, Lorg/icepdf/index/core/io/BitStream;->close()V

    move v1, v4

    .line 306
    goto :goto_1
.end method

.method static readmode(Lorg/icepdf/index/core/io/BitStream;Lorg/icepdf/index/core/pobjects/filters/CCITTFax$Code;)I
    .locals 4
    .param p0, "inb"    # Lorg/icepdf/index/core/io/BitStream;
    .param p1, "code"    # Lorg/icepdf/index/core/pobjects/filters/CCITTFax$Code;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 367
    invoke-virtual {p1}, Lorg/icepdf/index/core/pobjects/filters/CCITTFax$Code;->reset()V

    .line 368
    :cond_0
    invoke-virtual {p0}, Lorg/icepdf/index/core/io/BitStream;->atEndOfFile()Z

    move-result v2

    if-nez v2, :cond_2

    .line 369
    invoke-virtual {p0, v3}, Lorg/icepdf/index/core/io/BitStream;->getBits(I)I

    move-result v0

    .line 370
    .local v0, "i":I
    if-eqz v0, :cond_1

    move v2, v3

    :goto_0
    invoke-virtual {p1, v2}, Lorg/icepdf/index/core/pobjects/filters/CCITTFax$Code;->append(Z)V

    .line 371
    sget-object v2, Lorg/icepdf/index/core/pobjects/filters/CCITTFax;->modecodes:[[Lorg/icepdf/index/core/pobjects/filters/CCITTFax$Code;

    invoke-static {p1, v2}, Lorg/icepdf/index/core/pobjects/filters/CCITTFax;->findPositionInTable(Lorg/icepdf/index/core/pobjects/filters/CCITTFax$Code;[[Lorg/icepdf/index/core/pobjects/filters/CCITTFax$Code;)I

    move-result v1

    .line 372
    .local v1, "j":I
    if-ltz v1, :cond_0

    .line 377
    .end local v0    # "i":I
    .end local v1    # "j":I
    :goto_1
    return v1

    .line 370
    .restart local v0    # "i":I
    :cond_1
    const/4 v2, 0x0

    goto :goto_0

    .line 376
    .end local v0    # "i":I
    :cond_2
    invoke-virtual {p0}, Lorg/icepdf/index/core/io/BitStream;->close()V

    .line 377
    const/4 v1, -0x1

    goto :goto_1
.end method

.method static resetRuns(Lorg/icepdf/index/core/io/BitStream;Lorg/icepdf/index/core/pobjects/filters/G4State;)V
    .locals 7
    .param p0, "outb"    # Lorg/icepdf/index/core/io/BitStream;
    .param p1, "state"    # Lorg/icepdf/index/core/pobjects/filters/G4State;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 443
    iput-boolean v6, p1, Lorg/icepdf/index/core/pobjects/filters/G4State;->white:Z

    .line 444
    invoke-static {v5, p1, p0}, Lorg/icepdf/index/core/pobjects/filters/CCITTFax;->addRun(ILorg/icepdf/index/core/pobjects/filters/G4State;Lorg/icepdf/index/core/io/BitStream;)V

    .line 445
    iget v2, p1, Lorg/icepdf/index/core/pobjects/filters/G4State;->a0:I

    iget v3, p1, Lorg/icepdf/index/core/pobjects/filters/G4State;->width:I

    if-eq v2, v3, :cond_3

    .line 447
    :goto_0
    iget v2, p1, Lorg/icepdf/index/core/pobjects/filters/G4State;->a0:I

    iget v3, p1, Lorg/icepdf/index/core/pobjects/filters/G4State;->width:I

    if-le v2, v3, :cond_0

    .line 448
    iget v2, p1, Lorg/icepdf/index/core/pobjects/filters/G4State;->a0:I

    iget-object v3, p1, Lorg/icepdf/index/core/pobjects/filters/G4State;->cur:[I

    iget v4, p1, Lorg/icepdf/index/core/pobjects/filters/G4State;->curIndex:I

    add-int/lit8 v4, v4, -0x1

    iput v4, p1, Lorg/icepdf/index/core/pobjects/filters/G4State;->curIndex:I

    aget v3, v3, v4

    sub-int/2addr v2, v3

    iput v2, p1, Lorg/icepdf/index/core/pobjects/filters/G4State;->a0:I

    goto :goto_0

    .line 449
    :cond_0
    iget v2, p1, Lorg/icepdf/index/core/pobjects/filters/G4State;->a0:I

    iget v3, p1, Lorg/icepdf/index/core/pobjects/filters/G4State;->width:I

    if-ge v2, v3, :cond_4

    .line 450
    iget v2, p1, Lorg/icepdf/index/core/pobjects/filters/G4State;->a0:I

    if-gez v2, :cond_1

    .line 451
    iput v5, p1, Lorg/icepdf/index/core/pobjects/filters/G4State;->a0:I

    .line 452
    :cond_1
    iget v2, p1, Lorg/icepdf/index/core/pobjects/filters/G4State;->curIndex:I

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_2

    .line 453
    invoke-static {v5, p1, p0}, Lorg/icepdf/index/core/pobjects/filters/CCITTFax;->addRun(ILorg/icepdf/index/core/pobjects/filters/G4State;Lorg/icepdf/index/core/io/BitStream;)V

    .line 454
    :cond_2
    iget v2, p1, Lorg/icepdf/index/core/pobjects/filters/G4State;->width:I

    iget v3, p1, Lorg/icepdf/index/core/pobjects/filters/G4State;->a0:I

    sub-int/2addr v2, v3

    invoke-static {v2, p1, p0}, Lorg/icepdf/index/core/pobjects/filters/CCITTFax;->addRun(ILorg/icepdf/index/core/pobjects/filters/G4State;Lorg/icepdf/index/core/io/BitStream;)V

    .line 460
    :cond_3
    :goto_1
    iget-object v1, p1, Lorg/icepdf/index/core/pobjects/filters/G4State;->ref:[I

    .line 461
    .local v1, "tmp":[I
    iget-object v2, p1, Lorg/icepdf/index/core/pobjects/filters/G4State;->cur:[I

    iput-object v2, p1, Lorg/icepdf/index/core/pobjects/filters/G4State;->ref:[I

    .line 462
    iput-object v1, p1, Lorg/icepdf/index/core/pobjects/filters/G4State;->cur:[I

    .line 464
    iget v0, p1, Lorg/icepdf/index/core/pobjects/filters/G4State;->curIndex:I

    .local v0, "i":I
    :goto_2
    iget v2, p1, Lorg/icepdf/index/core/pobjects/filters/G4State;->width:I

    if-ge v0, v2, :cond_5

    .line 465
    iget-object v2, p1, Lorg/icepdf/index/core/pobjects/filters/G4State;->ref:[I

    aput v5, v2, v0

    .line 464
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 455
    .end local v0    # "i":I
    .end local v1    # "tmp":[I
    :cond_4
    iget v2, p1, Lorg/icepdf/index/core/pobjects/filters/G4State;->a0:I

    iget v3, p1, Lorg/icepdf/index/core/pobjects/filters/G4State;->width:I

    if-le v2, v3, :cond_3

    .line 456
    iget v2, p1, Lorg/icepdf/index/core/pobjects/filters/G4State;->width:I

    invoke-static {v2, p1, p0}, Lorg/icepdf/index/core/pobjects/filters/CCITTFax;->addRun(ILorg/icepdf/index/core/pobjects/filters/G4State;Lorg/icepdf/index/core/io/BitStream;)V

    .line 457
    invoke-static {v5, p1, p0}, Lorg/icepdf/index/core/pobjects/filters/CCITTFax;->addRun(ILorg/icepdf/index/core/pobjects/filters/G4State;Lorg/icepdf/index/core/io/BitStream;)V

    goto :goto_1

    .line 466
    .restart local v0    # "i":I
    .restart local v1    # "tmp":[I
    :cond_5
    const/4 v0, 0x0

    :goto_3
    iget v2, p1, Lorg/icepdf/index/core/pobjects/filters/G4State;->width:I

    if-ge v0, v2, :cond_6

    .line 467
    iget-object v2, p1, Lorg/icepdf/index/core/pobjects/filters/G4State;->cur:[I

    aput v5, v2, v0

    .line 466
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 468
    :cond_6
    iput v5, p1, Lorg/icepdf/index/core/pobjects/filters/G4State;->runLength:I

    .line 469
    iput v5, p1, Lorg/icepdf/index/core/pobjects/filters/G4State;->a0:I

    .line 470
    iget-object v2, p1, Lorg/icepdf/index/core/pobjects/filters/G4State;->ref:[I

    aget v2, v2, v5

    iput v2, p1, Lorg/icepdf/index/core/pobjects/filters/G4State;->b1:I

    .line 471
    iput v6, p1, Lorg/icepdf/index/core/pobjects/filters/G4State;->refIndex:I

    .line 472
    iput v5, p1, Lorg/icepdf/index/core/pobjects/filters/G4State;->curIndex:I

    .line 474
    invoke-virtual {p0}, Lorg/icepdf/index/core/io/BitStream;->close()V

    .line 475
    return-void
.end method
