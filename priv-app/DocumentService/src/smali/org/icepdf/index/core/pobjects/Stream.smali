.class public Lorg/icepdf/index/core/pobjects/Stream;
.super Lorg/icepdf/index/core/pobjects/Dictionary;
.source "Stream.java"


# static fields
.field private static final JPEG_ENC_CMYK:I = 0x2

.field private static final JPEG_ENC_GRAY:I = 0x5

.field private static final JPEG_ENC_RGB:I = 0x1

.field private static final JPEG_ENC_UNKNOWN_PROBABLY_YCbCr:I = 0x0

.field private static final JPEG_ENC_YCCK:I = 0x4

.field private static final JPEG_ENC_YCbCr:I = 0x3

.field private static scaleImages:Z


# instance fields
.field private pObjectReference:Lorg/icepdf/index/core/pobjects/Reference;

.field private streamInput:Lorg/icepdf/index/core/io/SeekableInputConstrainedWrapper;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 65
    const-string/jumbo v0, "org.icepdf.core.scaleImages"

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lorg/icepdf/index/core/util/Defs;->sysPropertyBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lorg/icepdf/index/core/pobjects/Stream;->scaleImages:Z

    .line 68
    return-void
.end method

.method public constructor <init>(Lorg/icepdf/index/core/util/Library;Lorg/icepdf/index/core/util/Hashtable;Lorg/icepdf/index/core/io/SeekableInputConstrainedWrapper;)V
    .locals 1
    .param p1, "l"    # Lorg/icepdf/index/core/util/Library;
    .param p2, "h"    # Lorg/icepdf/index/core/util/Hashtable;
    .param p3, "streamInputWrapper"    # Lorg/icepdf/index/core/io/SeekableInputConstrainedWrapper;

    .prologue
    .line 85
    invoke-direct {p0, p1, p2}, Lorg/icepdf/index/core/pobjects/Dictionary;-><init>(Lorg/icepdf/index/core/util/Library;Lorg/icepdf/index/core/util/Hashtable;)V

    .line 58
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/icepdf/index/core/pobjects/Stream;->pObjectReference:Lorg/icepdf/index/core/pobjects/Reference;

    .line 86
    iput-object p3, p0, Lorg/icepdf/index/core/pobjects/Stream;->streamInput:Lorg/icepdf/index/core/io/SeekableInputConstrainedWrapper;

    .line 87
    return-void
.end method

.method private checkMemory(I)Z
    .locals 1
    .param p1, "memoryNeeded"    # I

    .prologue
    .line 119
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/Stream;->library:Lorg/icepdf/index/core/util/Library;

    iget-object v0, v0, Lorg/icepdf/index/core/util/Library;->memoryManager:Lorg/icepdf/index/core/util/MemoryManager;

    invoke-virtual {v0, p1}, Lorg/icepdf/index/core/util/MemoryManager;->checkMemory(I)Z

    move-result v0

    return v0
.end method

.method private getDecodedStreamBytes()[B
    .locals 12

    .prologue
    const/4 v6, 0x0

    const/16 v7, 0x400

    .line 236
    invoke-virtual {p0}, Lorg/icepdf/index/core/pobjects/Stream;->getInputStreamForDecodedStreamBytes()Ljava/io/InputStream;

    move-result-object v1

    .line 237
    .local v1, "input":Ljava/io/InputStream;
    if-nez v1, :cond_1

    .line 274
    :cond_0
    :goto_0
    return-object v6

    .line 239
    :cond_1
    const/4 v2, 0x0

    .line 241
    .local v2, "out":Lorg/icepdf/index/core/io/ConservativeSizingByteArrayOutputStream;
    const/16 v8, 0x400

    :try_start_0
    iget-object v9, p0, Lorg/icepdf/index/core/pobjects/Stream;->streamInput:Lorg/icepdf/index/core/io/SeekableInputConstrainedWrapper;

    invoke-virtual {v9}, Lorg/icepdf/index/core/io/SeekableInputConstrainedWrapper;->getLength()J

    move-result-wide v10

    long-to-int v9, v10

    invoke-static {v8, v9}, Ljava/lang/Math;->max(II)I

    move-result v4

    .line 242
    .local v4, "outLength":I
    new-instance v3, Lorg/icepdf/index/core/io/ConservativeSizingByteArrayOutputStream;

    iget-object v8, p0, Lorg/icepdf/index/core/pobjects/Stream;->library:Lorg/icepdf/index/core/util/Library;

    iget-object v8, v8, Lorg/icepdf/index/core/util/Library;->memoryManager:Lorg/icepdf/index/core/util/MemoryManager;

    invoke-direct {v3, v4, v8}, Lorg/icepdf/index/core/io/ConservativeSizingByteArrayOutputStream;-><init>(ILorg/icepdf/index/core/util/MemoryManager;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_7
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 243
    .end local v2    # "out":Lorg/icepdf/index/core/io/ConservativeSizingByteArrayOutputStream;
    .local v3, "out":Lorg/icepdf/index/core/io/ConservativeSizingByteArrayOutputStream;
    if-le v4, v7, :cond_2

    const/16 v7, 0x1000

    :cond_2
    :try_start_1
    new-array v0, v7, [B

    .line 245
    .local v0, "buffer":[B
    :goto_1
    invoke-virtual {v1, v0}, Ljava/io/InputStream;->read([B)I

    move-result v5

    .line 246
    .local v5, "read":I
    if-gtz v5, :cond_4

    .line 250
    invoke-virtual {v3}, Lorg/icepdf/index/core/io/ConservativeSizingByteArrayOutputStream;->flush()V

    .line 254
    invoke-virtual {v3}, Lorg/icepdf/index/core/io/ConservativeSizingByteArrayOutputStream;->toByteArray()[B
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v6

    .line 260
    .local v6, "ret":[B
    if-eqz v1, :cond_3

    .line 262
    :try_start_2
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3

    .line 266
    :cond_3
    :goto_2
    if-eqz v3, :cond_0

    .line 268
    :try_start_3
    invoke-virtual {v3}, Lorg/icepdf/index/core/io/ConservativeSizingByteArrayOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_0

    .line 269
    :catch_0
    move-exception v7

    goto :goto_0

    .line 248
    .end local v6    # "ret":[B
    :cond_4
    const/4 v7, 0x0

    :try_start_4
    invoke-virtual {v3, v0, v7, v5}, Lorg/icepdf/index/core/io/ConservativeSizingByteArrayOutputStream;->write([BII)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_1

    .line 257
    .end local v0    # "buffer":[B
    .end local v5    # "read":I
    :catch_1
    move-exception v7

    move-object v2, v3

    .line 260
    .end local v3    # "out":Lorg/icepdf/index/core/io/ConservativeSizingByteArrayOutputStream;
    .end local v4    # "outLength":I
    .restart local v2    # "out":Lorg/icepdf/index/core/io/ConservativeSizingByteArrayOutputStream;
    :goto_3
    if-eqz v1, :cond_5

    .line 262
    :try_start_5
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_4

    .line 266
    :cond_5
    :goto_4
    if-eqz v2, :cond_0

    .line 268
    :try_start_6
    invoke-virtual {v2}, Lorg/icepdf/index/core/io/ConservativeSizingByteArrayOutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2

    goto :goto_0

    .line 269
    :catch_2
    move-exception v7

    goto :goto_0

    .line 260
    :catchall_0
    move-exception v7

    :goto_5
    if-eqz v1, :cond_6

    .line 262
    :try_start_7
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_5

    .line 266
    :cond_6
    :goto_6
    if-eqz v2, :cond_7

    .line 268
    :try_start_8
    invoke-virtual {v2}, Lorg/icepdf/index/core/io/ConservativeSizingByteArrayOutputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_6

    .line 270
    :cond_7
    :goto_7
    throw v7

    .line 263
    .end local v2    # "out":Lorg/icepdf/index/core/io/ConservativeSizingByteArrayOutputStream;
    .restart local v0    # "buffer":[B
    .restart local v3    # "out":Lorg/icepdf/index/core/io/ConservativeSizingByteArrayOutputStream;
    .restart local v4    # "outLength":I
    .restart local v5    # "read":I
    .restart local v6    # "ret":[B
    :catch_3
    move-exception v7

    goto :goto_2

    .end local v0    # "buffer":[B
    .end local v3    # "out":Lorg/icepdf/index/core/io/ConservativeSizingByteArrayOutputStream;
    .end local v4    # "outLength":I
    .end local v5    # "read":I
    .end local v6    # "ret":[B
    .restart local v2    # "out":Lorg/icepdf/index/core/io/ConservativeSizingByteArrayOutputStream;
    :catch_4
    move-exception v7

    goto :goto_4

    :catch_5
    move-exception v8

    goto :goto_6

    .line 269
    :catch_6
    move-exception v8

    goto :goto_7

    .line 260
    .end local v2    # "out":Lorg/icepdf/index/core/io/ConservativeSizingByteArrayOutputStream;
    .restart local v3    # "out":Lorg/icepdf/index/core/io/ConservativeSizingByteArrayOutputStream;
    .restart local v4    # "outLength":I
    :catchall_1
    move-exception v7

    move-object v2, v3

    .end local v3    # "out":Lorg/icepdf/index/core/io/ConservativeSizingByteArrayOutputStream;
    .restart local v2    # "out":Lorg/icepdf/index/core/io/ConservativeSizingByteArrayOutputStream;
    goto :goto_5

    .line 257
    .end local v4    # "outLength":I
    :catch_7
    move-exception v7

    goto :goto_3
.end method

.method private getFilterNames()Ljava/util/Vector;
    .locals 5

    .prologue
    .line 369
    const/4 v0, 0x0

    .line 370
    .local v0, "filterNames":Ljava/util/Vector;
    iget-object v2, p0, Lorg/icepdf/index/core/pobjects/Stream;->library:Lorg/icepdf/index/core/util/Library;

    iget-object v3, p0, Lorg/icepdf/index/core/pobjects/Stream;->entries:Lorg/icepdf/index/core/util/Hashtable;

    const-string/jumbo v4, "Filter"

    invoke-virtual {v2, v3, v4}, Lorg/icepdf/index/core/util/Library;->getObject(Lorg/icepdf/index/core/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    .line 371
    .local v1, "o":Ljava/lang/Object;
    instance-of v2, v1, Lorg/icepdf/index/core/pobjects/Name;

    if-eqz v2, :cond_1

    .line 372
    new-instance v0, Ljava/util/Vector;

    .end local v0    # "filterNames":Ljava/util/Vector;
    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    .line 373
    .restart local v0    # "filterNames":Ljava/util/Vector;
    invoke-virtual {v0, v1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    .line 377
    :cond_0
    :goto_0
    return-object v0

    .line 374
    :cond_1
    instance-of v2, v1, Ljava/util/Vector;

    if-eqz v2, :cond_0

    move-object v0, v1

    .line 375
    check-cast v0, Ljava/util/Vector;

    goto :goto_0
.end method


# virtual methods
.method public dispose(Z)V
    .locals 1
    .param p1, "cache"    # Z

    .prologue
    .line 422
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/Stream;->streamInput:Lorg/icepdf/index/core/io/SeekableInputConstrainedWrapper;

    if-eqz v0, :cond_0

    .line 423
    if-nez p1, :cond_0

    .line 425
    :try_start_0
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/Stream;->streamInput:Lorg/icepdf/index/core/io/SeekableInputConstrainedWrapper;

    invoke-virtual {v0}, Lorg/icepdf/index/core/io/SeekableInputConstrainedWrapper;->dispose()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 430
    :goto_0
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/icepdf/index/core/pobjects/Stream;->streamInput:Lorg/icepdf/index/core/io/SeekableInputConstrainedWrapper;

    .line 434
    :cond_0
    return-void

    .line 427
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public getBlackIs1(Lorg/icepdf/index/core/util/Library;Lorg/icepdf/index/core/util/Hashtable;)Z
    .locals 2
    .param p1, "library"    # Lorg/icepdf/index/core/util/Library;
    .param p2, "decodeParmsDictionary"    # Lorg/icepdf/index/core/util/Hashtable;

    .prologue
    .line 2056
    invoke-virtual {p0, p1, p2}, Lorg/icepdf/index/core/pobjects/Stream;->getBlackIs1OrNull(Lorg/icepdf/index/core/util/Library;Lorg/icepdf/index/core/util/Hashtable;)Ljava/lang/Boolean;

    move-result-object v0

    .line 2057
    .local v0, "blackIs1":Ljava/lang/Boolean;
    if-eqz v0, :cond_0

    .line 2058
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 2059
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getBlackIs1OrNull(Lorg/icepdf/index/core/util/Library;Lorg/icepdf/index/core/util/Hashtable;)Ljava/lang/Boolean;
    .locals 3
    .param p1, "library"    # Lorg/icepdf/index/core/util/Library;
    .param p2, "decodeParmsDictionary"    # Lorg/icepdf/index/core/util/Hashtable;

    .prologue
    .line 2067
    const-string/jumbo v2, "BlackIs1"

    invoke-virtual {p1, p2, v2}, Lorg/icepdf/index/core/util/Library;->getObject(Lorg/icepdf/index/core/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 2068
    .local v0, "blackIs1Obj":Ljava/lang/Object;
    if-eqz v0, :cond_6

    .line 2069
    instance-of v2, v0, Ljava/lang/Boolean;

    if-eqz v2, :cond_0

    .line 2070
    check-cast v0, Ljava/lang/Boolean;

    .line 2087
    .end local v0    # "blackIs1Obj":Ljava/lang/Object;
    :goto_0
    return-object v0

    .line 2071
    .restart local v0    # "blackIs1Obj":Ljava/lang/Object;
    :cond_0
    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_6

    move-object v1, v0

    .line 2072
    check-cast v1, Ljava/lang/String;

    .line 2073
    .local v1, "blackIs1String":Ljava/lang/String;
    const-string/jumbo v2, "true"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2074
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    goto :goto_0

    .line 2075
    :cond_1
    const-string/jumbo v2, "t"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2076
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    goto :goto_0

    .line 2077
    :cond_2
    const-string/jumbo v2, "1"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2078
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    goto :goto_0

    .line 2079
    :cond_3
    const-string/jumbo v2, "false"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 2080
    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    goto :goto_0

    .line 2081
    :cond_4
    const-string/jumbo v2, "f"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 2082
    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    goto :goto_0

    .line 2083
    :cond_5
    const-string/jumbo v2, "0"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 2084
    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    goto :goto_0

    .line 2087
    .end local v1    # "blackIs1String":Ljava/lang/String;
    :cond_6
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getBytes()[B
    .locals 2

    .prologue
    .line 1328
    invoke-direct {p0}, Lorg/icepdf/index/core/pobjects/Stream;->getDecodedStreamBytes()[B

    move-result-object v0

    .line 1329
    .local v0, "data":[B
    if-nez v0, :cond_0

    .line 1330
    const/4 v1, 0x0

    new-array v0, v1, [B

    .line 1331
    :cond_0
    return-object v0
.end method

.method public getInputStreamForDecodedStreamBytes()Ljava/io/InputStream;
    .locals 18

    .prologue
    .line 134
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/icepdf/index/core/pobjects/Stream;->streamInput:Lorg/icepdf/index/core/io/SeekableInputConstrainedWrapper;

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/icepdf/index/core/pobjects/Stream;->streamInput:Lorg/icepdf/index/core/io/SeekableInputConstrainedWrapper;

    invoke-virtual {v2}, Lorg/icepdf/index/core/io/SeekableInputConstrainedWrapper;->getLength()J

    move-result-wide v2

    const-wide/16 v16, 0x1

    cmp-long v2, v2, v16

    if-gez v2, :cond_2

    .line 135
    :cond_0
    const/4 v6, 0x0

    .line 232
    :cond_1
    :goto_0
    return-object v6

    .line 138
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/icepdf/index/core/pobjects/Stream;->streamInput:Lorg/icepdf/index/core/io/SeekableInputConstrainedWrapper;

    invoke-virtual {v2}, Lorg/icepdf/index/core/io/SeekableInputConstrainedWrapper;->getLength()J

    move-result-wide v14

    .line 139
    .local v14, "streamLength":J
    long-to-int v13, v14

    .line 140
    .local v13, "memoryNeeded":I
    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lorg/icepdf/index/core/pobjects/Stream;->checkMemory(I)Z

    .line 141
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/icepdf/index/core/pobjects/Stream;->streamInput:Lorg/icepdf/index/core/io/SeekableInputConstrainedWrapper;

    invoke-virtual {v2}, Lorg/icepdf/index/core/io/SeekableInputConstrainedWrapper;->prepareForCurrentUse()V

    .line 142
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/icepdf/index/core/pobjects/Stream;->streamInput:Lorg/icepdf/index/core/io/SeekableInputConstrainedWrapper;

    .line 144
    .local v12, "input":Ljava/io/InputStream;
    long-to-int v2, v14

    const/16 v3, 0x40

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    const/16 v3, 0x4000

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v8

    .line 145
    .local v8, "bufferSize":I
    new-instance v6, Ljava/io/BufferedInputStream;

    invoke-direct {v6, v12, v8}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;I)V

    .line 147
    .end local v12    # "input":Ljava/io/InputStream;
    .local v6, "input":Ljava/io/InputStream;
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/icepdf/index/core/pobjects/Stream;->library:Lorg/icepdf/index/core/util/Library;

    iget-object v2, v2, Lorg/icepdf/index/core/util/Library;->securityManager:Lorg/icepdf/index/core/pobjects/security/SecurityManager;

    if-eqz v2, :cond_3

    .line 149
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/icepdf/index/core/pobjects/Stream;->library:Lorg/icepdf/index/core/util/Library;

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/icepdf/index/core/pobjects/Stream;->entries:Lorg/icepdf/index/core/util/Hashtable;

    const-string/jumbo v4, "DecodeParam"

    invoke-virtual {v2, v3, v4}, Lorg/icepdf/index/core/util/Library;->getDictionary(Lorg/icepdf/index/core/util/Hashtable;Ljava/lang/String;)Lorg/icepdf/index/core/util/Hashtable;

    move-result-object v5

    .line 150
    .local v5, "decodeParams":Lorg/icepdf/index/core/util/Hashtable;
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/icepdf/index/core/pobjects/Stream;->library:Lorg/icepdf/index/core/util/Library;

    invoke-virtual {v2}, Lorg/icepdf/index/core/util/Library;->getSecurityManager()Lorg/icepdf/index/core/pobjects/security/SecurityManager;

    move-result-object v2

    invoke-virtual/range {p0 .. p0}, Lorg/icepdf/index/core/pobjects/Stream;->getPObjectReference()Lorg/icepdf/index/core/pobjects/Reference;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/icepdf/index/core/pobjects/Stream;->library:Lorg/icepdf/index/core/util/Library;

    invoke-virtual {v4}, Lorg/icepdf/index/core/util/Library;->getSecurityManager()Lorg/icepdf/index/core/pobjects/security/SecurityManager;

    move-result-object v4

    invoke-virtual {v4}, Lorg/icepdf/index/core/pobjects/security/SecurityManager;->getDecryptionKey()[B

    move-result-object v4

    const/4 v7, 0x1

    invoke-virtual/range {v2 .. v7}, Lorg/icepdf/index/core/pobjects/security/SecurityManager;->getEncryptionInputStream(Lorg/icepdf/index/core/pobjects/Reference;[BLorg/icepdf/index/core/util/Hashtable;Ljava/io/InputStream;Z)Ljava/io/InputStream;

    move-result-object v6

    .line 159
    .end local v5    # "decodeParams":Lorg/icepdf/index/core/util/Hashtable;
    :cond_3
    invoke-direct/range {p0 .. p0}, Lorg/icepdf/index/core/pobjects/Stream;->getFilterNames()Ljava/util/Vector;

    move-result-object v10

    .line 160
    .local v10, "filterNames":Ljava/util/Vector;
    if-eqz v10, :cond_1

    .line 166
    const/4 v11, 0x0

    .local v11, "i":I
    :goto_1
    invoke-virtual {v10}, Ljava/util/Vector;->size()I

    move-result v2

    if-ge v11, v2, :cond_10

    .line 168
    invoke-virtual {v10, v11}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v9

    .line 171
    .local v9, "filterName":Ljava/lang/String;
    const-string/jumbo v2, "FlateDecode"

    invoke-virtual {v9, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    const-string/jumbo v2, "/Fl"

    invoke-virtual {v9, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    const-string/jumbo v2, "Fl"

    invoke-virtual {v9, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 174
    :cond_4
    new-instance v12, Lorg/icepdf/index/core/pobjects/filters/FlateDecode;

    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/icepdf/index/core/pobjects/Stream;->library:Lorg/icepdf/index/core/util/Library;

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/icepdf/index/core/pobjects/Stream;->entries:Lorg/icepdf/index/core/util/Hashtable;

    invoke-direct {v12, v2, v3, v6}, Lorg/icepdf/index/core/pobjects/filters/FlateDecode;-><init>(Lorg/icepdf/index/core/util/Library;Lorg/icepdf/index/core/util/Hashtable;Ljava/io/InputStream;)V

    .line 175
    .end local v6    # "input":Ljava/io/InputStream;
    .restart local v12    # "input":Ljava/io/InputStream;
    mul-int/lit8 v13, v13, 0x2

    move-object v6, v12

    .line 166
    .end local v12    # "input":Ljava/io/InputStream;
    .restart local v6    # "input":Ljava/io/InputStream;
    :cond_5
    :goto_2
    add-int/lit8 v11, v11, 0x1

    goto :goto_1

    .line 176
    :cond_6
    const-string/jumbo v2, "LZWDecode"

    invoke-virtual {v9, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    const-string/jumbo v2, "/LZW"

    invoke-virtual {v9, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    const-string/jumbo v2, "LZW"

    invoke-virtual {v9, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 180
    :cond_7
    new-instance v12, Lorg/icepdf/index/core/pobjects/filters/LZWDecode;

    new-instance v2, Lorg/icepdf/index/core/io/BitStream;

    invoke-direct {v2, v6}, Lorg/icepdf/index/core/io/BitStream;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v12, v2}, Lorg/icepdf/index/core/pobjects/filters/LZWDecode;-><init>(Lorg/icepdf/index/core/io/BitStream;)V

    .line 181
    .end local v6    # "input":Ljava/io/InputStream;
    .restart local v12    # "input":Ljava/io/InputStream;
    mul-int/lit8 v13, v13, 0x2

    move-object v6, v12

    .end local v12    # "input":Ljava/io/InputStream;
    .restart local v6    # "input":Ljava/io/InputStream;
    goto :goto_2

    .line 182
    :cond_8
    const-string/jumbo v2, "ASCII85Decode"

    invoke-virtual {v9, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    const-string/jumbo v2, "/A85"

    invoke-virtual {v9, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    const-string/jumbo v2, "A85"

    invoke-virtual {v9, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 186
    :cond_9
    new-instance v12, Lorg/icepdf/index/core/pobjects/filters/ASCII85Decode;

    invoke-direct {v12, v6}, Lorg/icepdf/index/core/pobjects/filters/ASCII85Decode;-><init>(Ljava/io/InputStream;)V

    .line 187
    .end local v6    # "input":Ljava/io/InputStream;
    .restart local v12    # "input":Ljava/io/InputStream;
    mul-int/lit8 v13, v13, 0x2

    move-object v6, v12

    .end local v12    # "input":Ljava/io/InputStream;
    .restart local v6    # "input":Ljava/io/InputStream;
    goto :goto_2

    .line 188
    :cond_a
    const-string/jumbo v2, "ASCIIHexDecode"

    invoke-virtual {v9, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_b

    const-string/jumbo v2, "/AHx"

    invoke-virtual {v9, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_b

    const-string/jumbo v2, "AHx"

    invoke-virtual {v9, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_c

    .line 192
    :cond_b
    new-instance v12, Lorg/icepdf/index/core/pobjects/filters/ASCIIHexDecode;

    invoke-direct {v12, v6}, Lorg/icepdf/index/core/pobjects/filters/ASCIIHexDecode;-><init>(Ljava/io/InputStream;)V

    .line 193
    .end local v6    # "input":Ljava/io/InputStream;
    .restart local v12    # "input":Ljava/io/InputStream;
    div-int/lit8 v13, v13, 0x2

    move-object v6, v12

    .end local v12    # "input":Ljava/io/InputStream;
    .restart local v6    # "input":Ljava/io/InputStream;
    goto :goto_2

    .line 194
    :cond_c
    const-string/jumbo v2, "RunLengthDecode"

    invoke-virtual {v9, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_d

    const-string/jumbo v2, "/RL"

    invoke-virtual {v9, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_d

    const-string/jumbo v2, "RL"

    invoke-virtual {v9, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_e

    .line 198
    :cond_d
    new-instance v12, Lorg/icepdf/index/core/pobjects/filters/RunLengthDecode;

    invoke-direct {v12, v6}, Lorg/icepdf/index/core/pobjects/filters/RunLengthDecode;-><init>(Ljava/io/InputStream;)V

    .line 199
    .end local v6    # "input":Ljava/io/InputStream;
    .restart local v12    # "input":Ljava/io/InputStream;
    mul-int/lit8 v13, v13, 0x2

    move-object v6, v12

    .end local v12    # "input":Ljava/io/InputStream;
    .restart local v6    # "input":Ljava/io/InputStream;
    goto/16 :goto_2

    .line 200
    :cond_e
    const-string/jumbo v2, "CCITTFaxDecode"

    invoke-virtual {v9, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    const-string/jumbo v2, "/CCF"

    invoke-virtual {v9, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    const-string/jumbo v2, "CCF"

    invoke-virtual {v9, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 205
    const-string/jumbo v2, "DCTDecode"

    invoke-virtual {v9, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    const-string/jumbo v2, "/DCT"

    invoke-virtual {v9, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    const-string/jumbo v2, "DCT"

    invoke-virtual {v9, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 210
    const-string/jumbo v2, "JPXDecode"

    invoke-virtual {v9, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 215
    if-eqz v6, :cond_f

    .line 217
    :try_start_0
    invoke-virtual {v6}, Ljava/io/InputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 224
    :cond_f
    :goto_3
    const/4 v6, 0x0

    goto/16 :goto_0

    .line 230
    .end local v9    # "filterName":Ljava/lang/String;
    :cond_10
    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lorg/icepdf/index/core/pobjects/Stream;->checkMemory(I)Z

    goto/16 :goto_0

    .line 219
    .restart local v9    # "filterName":Ljava/lang/String;
    :catch_0
    move-exception v2

    goto :goto_3
.end method

.method public getPObjectReference()Lorg/icepdf/index/core/pobjects/Reference;
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/Stream;->pObjectReference:Lorg/icepdf/index/core/pobjects/Reference;

    return-object v0
.end method

.method public isImageMask()Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2049
    iget-object v2, p0, Lorg/icepdf/index/core/pobjects/Stream;->library:Lorg/icepdf/index/core/util/Library;

    iget-object v3, p0, Lorg/icepdf/index/core/pobjects/Stream;->entries:Lorg/icepdf/index/core/util/Hashtable;

    const-string/jumbo v4, "ImageMask"

    invoke-virtual {v2, v3, v4}, Lorg/icepdf/index/core/util/Library;->getObject(Lorg/icepdf/index/core/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 2050
    .local v0, "o":Ljava/lang/Object;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "true"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    :cond_0
    return v1
.end method

.method isImageSubtype()Z
    .locals 4

    .prologue
    .line 108
    iget-object v1, p0, Lorg/icepdf/index/core/pobjects/Stream;->library:Lorg/icepdf/index/core/util/Library;

    iget-object v2, p0, Lorg/icepdf/index/core/pobjects/Stream;->entries:Lorg/icepdf/index/core/util/Hashtable;

    const-string/jumbo v3, "Subtype"

    invoke-virtual {v1, v2, v3}, Lorg/icepdf/index/core/util/Library;->getObject(Lorg/icepdf/index/core/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 109
    .local v0, "subtype":Ljava/lang/Object;
    if-eqz v0, :cond_0

    const-string/jumbo v1, "Image"

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setPObjectReference(Lorg/icepdf/index/core/pobjects/Reference;)V
    .locals 0
    .param p1, "reference"    # Lorg/icepdf/index/core/pobjects/Reference;

    .prologue
    .line 94
    iput-object p1, p0, Lorg/icepdf/index/core/pobjects/Stream;->pObjectReference:Lorg/icepdf/index/core/pobjects/Reference;

    .line 95
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2094
    new-instance v0, Ljava/lang/StringBuffer;

    const/16 v1, 0x40

    invoke-direct {v0, v1}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 2095
    .local v0, "sb":Ljava/lang/StringBuffer;
    const-string/jumbo v1, "STREAM= "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 2096
    iget-object v1, p0, Lorg/icepdf/index/core/pobjects/Stream;->entries:Lorg/icepdf/index/core/util/Hashtable;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    .line 2097
    invoke-virtual {p0}, Lorg/icepdf/index/core/pobjects/Stream;->getPObjectReference()Lorg/icepdf/index/core/pobjects/Reference;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2098
    const-string/jumbo v1, "  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 2099
    invoke-virtual {p0}, Lorg/icepdf/index/core/pobjects/Stream;->getPObjectReference()Lorg/icepdf/index/core/pobjects/Reference;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    .line 2101
    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
