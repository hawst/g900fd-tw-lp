.class public Lorg/icepdf/index/core/pobjects/Catalog;
.super Lorg/icepdf/index/core/pobjects/Dictionary;
.source "Catalog.java"


# instance fields
.field private nameTree:Lorg/icepdf/index/core/pobjects/NameTree;

.field private namesTreeInited:Z

.field private pageTree:Lorg/icepdf/index/core/pobjects/PageTree;


# direct methods
.method public constructor <init>(Lorg/icepdf/index/core/util/Library;Lorg/icepdf/index/core/util/Hashtable;)V
    .locals 1
    .param p1, "l"    # Lorg/icepdf/index/core/util/Library;
    .param p2, "h"    # Lorg/icepdf/index/core/util/Hashtable;

    .prologue
    .line 62
    invoke-direct {p0, p1, p2}, Lorg/icepdf/index/core/pobjects/Dictionary;-><init>(Lorg/icepdf/index/core/util/Library;Lorg/icepdf/index/core/util/Hashtable;)V

    .line 51
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/icepdf/index/core/pobjects/Catalog;->namesTreeInited:Z

    .line 63
    return-void
.end method


# virtual methods
.method public dispose(Z)V
    .locals 2
    .param p1, "cache"    # Z

    .prologue
    const/4 v1, 0x0

    .line 96
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/Catalog;->nameTree:Lorg/icepdf/index/core/pobjects/NameTree;

    if-eqz v0, :cond_0

    .line 97
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/Catalog;->nameTree:Lorg/icepdf/index/core/pobjects/NameTree;

    invoke-virtual {v0}, Lorg/icepdf/index/core/pobjects/NameTree;->dispose()V

    .line 98
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/icepdf/index/core/pobjects/Catalog;->namesTreeInited:Z

    .line 99
    if-nez p1, :cond_0

    .line 100
    iput-object v1, p0, Lorg/icepdf/index/core/pobjects/Catalog;->nameTree:Lorg/icepdf/index/core/pobjects/NameTree;

    .line 103
    :cond_0
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/Catalog;->pageTree:Lorg/icepdf/index/core/pobjects/PageTree;

    if-eqz v0, :cond_1

    .line 104
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/Catalog;->pageTree:Lorg/icepdf/index/core/pobjects/PageTree;

    invoke-virtual {v0, p1}, Lorg/icepdf/index/core/pobjects/PageTree;->dispose(Z)V

    .line 105
    if-nez p1, :cond_1

    .line 106
    iput-object v1, p0, Lorg/icepdf/index/core/pobjects/Catalog;->pageTree:Lorg/icepdf/index/core/pobjects/PageTree;

    .line 114
    :cond_1
    return-void
.end method

.method public getNameTree()Lorg/icepdf/index/core/pobjects/NameTree;
    .locals 6

    .prologue
    .line 137
    iget-boolean v3, p0, Lorg/icepdf/index/core/pobjects/Catalog;->namesTreeInited:Z

    if-nez v3, :cond_0

    .line 138
    const/4 v3, 0x1

    iput-boolean v3, p0, Lorg/icepdf/index/core/pobjects/Catalog;->namesTreeInited:Z

    .line 139
    iget-object v3, p0, Lorg/icepdf/index/core/pobjects/Catalog;->library:Lorg/icepdf/index/core/util/Library;

    iget-object v4, p0, Lorg/icepdf/index/core/pobjects/Catalog;->entries:Lorg/icepdf/index/core/util/Hashtable;

    const-string/jumbo v5, "Names"

    invoke-virtual {v3, v4, v5}, Lorg/icepdf/index/core/util/Library;->getObject(Lorg/icepdf/index/core/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    .line 140
    .local v2, "o":Ljava/lang/Object;
    if-eqz v2, :cond_0

    instance-of v3, v2, Lorg/icepdf/index/core/util/Hashtable;

    if-eqz v3, :cond_0

    move-object v0, v2

    .line 141
    check-cast v0, Lorg/icepdf/index/core/util/Hashtable;

    .line 142
    .local v0, "dest":Lorg/icepdf/index/core/util/Hashtable;
    iget-object v3, p0, Lorg/icepdf/index/core/pobjects/Catalog;->library:Lorg/icepdf/index/core/util/Library;

    const-string/jumbo v4, "Dests"

    invoke-virtual {v3, v0, v4}, Lorg/icepdf/index/core/util/Library;->getObject(Lorg/icepdf/index/core/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    .line 143
    .local v1, "names":Ljava/lang/Object;
    if-eqz v1, :cond_0

    instance-of v3, v1, Lorg/icepdf/index/core/util/Hashtable;

    if-eqz v3, :cond_0

    .line 144
    new-instance v3, Lorg/icepdf/index/core/pobjects/NameTree;

    iget-object v4, p0, Lorg/icepdf/index/core/pobjects/Catalog;->library:Lorg/icepdf/index/core/util/Library;

    check-cast v1, Lorg/icepdf/index/core/util/Hashtable;

    .end local v1    # "names":Ljava/lang/Object;
    invoke-direct {v3, v4, v1}, Lorg/icepdf/index/core/pobjects/NameTree;-><init>(Lorg/icepdf/index/core/util/Library;Lorg/icepdf/index/core/util/Hashtable;)V

    iput-object v3, p0, Lorg/icepdf/index/core/pobjects/Catalog;->nameTree:Lorg/icepdf/index/core/pobjects/NameTree;

    .line 145
    iget-object v3, p0, Lorg/icepdf/index/core/pobjects/Catalog;->nameTree:Lorg/icepdf/index/core/pobjects/NameTree;

    invoke-virtual {v3}, Lorg/icepdf/index/core/pobjects/NameTree;->init()V

    .line 149
    .end local v0    # "dest":Lorg/icepdf/index/core/util/Hashtable;
    .end local v2    # "o":Ljava/lang/Object;
    :cond_0
    iget-object v3, p0, Lorg/icepdf/index/core/pobjects/Catalog;->nameTree:Lorg/icepdf/index/core/pobjects/NameTree;

    return-object v3
.end method

.method public getPageTree()Lorg/icepdf/index/core/pobjects/PageTree;
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/Catalog;->pageTree:Lorg/icepdf/index/core/pobjects/PageTree;

    return-object v0
.end method

.method public init()V
    .locals 4

    .prologue
    .line 69
    iget-object v1, p0, Lorg/icepdf/index/core/pobjects/Catalog;->library:Lorg/icepdf/index/core/util/Library;

    iget-object v2, p0, Lorg/icepdf/index/core/pobjects/Catalog;->entries:Lorg/icepdf/index/core/util/Hashtable;

    const-string/jumbo v3, "Pages"

    invoke-virtual {v1, v2, v3}, Lorg/icepdf/index/core/util/Library;->getObject(Lorg/icepdf/index/core/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 70
    .local v0, "tmp":Ljava/lang/Object;
    const/4 v1, 0x0

    iput-object v1, p0, Lorg/icepdf/index/core/pobjects/Catalog;->pageTree:Lorg/icepdf/index/core/pobjects/PageTree;

    .line 71
    instance-of v1, v0, Lorg/icepdf/index/core/pobjects/PageTree;

    if-eqz v1, :cond_1

    .line 72
    check-cast v0, Lorg/icepdf/index/core/pobjects/PageTree;

    .end local v0    # "tmp":Ljava/lang/Object;
    iput-object v0, p0, Lorg/icepdf/index/core/pobjects/Catalog;->pageTree:Lorg/icepdf/index/core/pobjects/PageTree;

    .line 80
    :cond_0
    :goto_0
    iget-object v1, p0, Lorg/icepdf/index/core/pobjects/Catalog;->pageTree:Lorg/icepdf/index/core/pobjects/PageTree;

    if-eqz v1, :cond_2

    .line 81
    iget-object v1, p0, Lorg/icepdf/index/core/pobjects/Catalog;->pageTree:Lorg/icepdf/index/core/pobjects/PageTree;

    invoke-virtual {v1}, Lorg/icepdf/index/core/pobjects/PageTree;->init()V

    .line 82
    iget-object v1, p0, Lorg/icepdf/index/core/pobjects/Catalog;->pageTree:Lorg/icepdf/index/core/pobjects/PageTree;

    invoke-virtual {v1}, Lorg/icepdf/index/core/pobjects/PageTree;->initRootPageTree()V

    .line 86
    :goto_1
    return-void

    .line 76
    .restart local v0    # "tmp":Ljava/lang/Object;
    :cond_1
    instance-of v1, v0, Lorg/icepdf/index/core/util/Hashtable;

    if-eqz v1, :cond_0

    .line 77
    new-instance v1, Lorg/icepdf/index/core/pobjects/PageTree;

    iget-object v2, p0, Lorg/icepdf/index/core/pobjects/Catalog;->library:Lorg/icepdf/index/core/util/Library;

    check-cast v0, Lorg/icepdf/index/core/util/Hashtable;

    .end local v0    # "tmp":Ljava/lang/Object;
    invoke-direct {v1, v2, v0}, Lorg/icepdf/index/core/pobjects/PageTree;-><init>(Lorg/icepdf/index/core/util/Library;Lorg/icepdf/index/core/util/Hashtable;)V

    iput-object v1, p0, Lorg/icepdf/index/core/pobjects/Catalog;->pageTree:Lorg/icepdf/index/core/pobjects/PageTree;

    goto :goto_0

    .line 84
    :cond_2
    const-string/jumbo v1, "LOG"

    const-string/jumbo v2, "Error parsing page tree : pageTree is null"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 160
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "CATALOG= "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/icepdf/index/core/pobjects/Catalog;->entries:Lorg/icepdf/index/core/util/Hashtable;

    invoke-virtual {v1}, Lorg/icepdf/index/core/util/Hashtable;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
