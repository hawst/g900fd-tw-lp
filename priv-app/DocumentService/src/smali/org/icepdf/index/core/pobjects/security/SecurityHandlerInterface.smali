.class public interface abstract Lorg/icepdf/index/core/pobjects/security/SecurityHandlerInterface;
.super Ljava/lang/Object;
.source "SecurityHandlerInterface.java"


# virtual methods
.method public abstract decrypt(Lorg/icepdf/index/core/pobjects/Reference;[B[B)[B
.end method

.method public abstract dispose()V
.end method

.method public abstract encrypt(Lorg/icepdf/index/core/pobjects/Reference;[B[B)[B
.end method

.method public abstract getDecryptionKey()[B
.end method

.method public abstract getEncryptionInputStream(Lorg/icepdf/index/core/pobjects/Reference;[BLjava/util/Hashtable;Ljava/io/InputStream;)Ljava/io/InputStream;
.end method

.method public abstract getEncryptionKey()[B
.end method

.method public abstract getHandlerName()Ljava/lang/String;
.end method

.method public abstract getPermissions()Lorg/icepdf/index/core/pobjects/security/Permissions;
.end method

.method public abstract init()V
.end method

.method public abstract isAuthorized(Ljava/lang/String;)Z
.end method

.method public abstract isOwnerAuthorized(Ljava/lang/String;)Z
.end method

.method public abstract isUserAuthorized(Ljava/lang/String;)Z
.end method
