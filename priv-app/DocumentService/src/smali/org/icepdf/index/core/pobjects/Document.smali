.class public Lorg/icepdf/index/core/pobjects/Document;
.super Ljava/lang/Object;
.source "Document.java"


# static fields
.field private static isCachingEnabled:Z


# instance fields
.field private catalog:Lorg/icepdf/index/core/pobjects/Catalog;

.field private documentSeekableInput:Lorg/icepdf/index/core/io/SeekableInput;

.field private library:Lorg/icepdf/index/core/util/Library;

.field private pTrailer:Lorg/icepdf/index/core/pobjects/PTrailer;

.field private securityCallback:Lorg/icepdf/index/core/SecurityCallback;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 107
    const-string/jumbo v0, "org.icepdf.core.streamcache.enabled"

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lorg/icepdf/index/core/util/Defs;->sysPropertyBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lorg/icepdf/index/core/pobjects/Document;->isCachingEnabled:Z

    .line 110
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 116
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 101
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/icepdf/index/core/pobjects/Document;->library:Lorg/icepdf/index/core/util/Library;

    .line 117
    return-void
.end method

.method private attemptAuthorizeSecurityManager()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/icepdf/index/core/exceptions/PDFSecurityException;
        }
    .end annotation

    .prologue
    .line 648
    iget-object v2, p0, Lorg/icepdf/index/core/pobjects/Document;->library:Lorg/icepdf/index/core/util/Library;

    iget-object v2, v2, Lorg/icepdf/index/core/util/Library;->securityManager:Lorg/icepdf/index/core/pobjects/security/SecurityManager;

    const-string/jumbo v3, ""

    invoke-virtual {v2, v3}, Lorg/icepdf/index/core/pobjects/security/SecurityManager;->isAuthorized(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 650
    const/4 v0, 0x1

    .line 659
    .local v0, "count":I
    :cond_0
    iget-object v2, p0, Lorg/icepdf/index/core/pobjects/Document;->securityCallback:Lorg/icepdf/index/core/SecurityCallback;

    if-eqz v2, :cond_1

    .line 660
    iget-object v2, p0, Lorg/icepdf/index/core/pobjects/Document;->securityCallback:Lorg/icepdf/index/core/SecurityCallback;

    invoke-interface {v2, p0}, Lorg/icepdf/index/core/SecurityCallback;->requestPassword(Lorg/icepdf/index/core/pobjects/Document;)Ljava/lang/String;

    move-result-object v1

    .line 661
    .local v1, "password":Ljava/lang/String;
    if-nez v1, :cond_2

    .line 662
    new-instance v2, Lorg/icepdf/index/core/exceptions/PDFSecurityException;

    const-string/jumbo v3, "Encryption error"

    invoke-direct {v2, v3}, Lorg/icepdf/index/core/exceptions/PDFSecurityException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 665
    .end local v1    # "password":Ljava/lang/String;
    :cond_1
    new-instance v2, Lorg/icepdf/index/core/exceptions/PDFSecurityException;

    const-string/jumbo v3, "Encryption error"

    invoke-direct {v2, v3}, Lorg/icepdf/index/core/exceptions/PDFSecurityException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 670
    .restart local v1    # "password":Ljava/lang/String;
    :cond_2
    iget-object v2, p0, Lorg/icepdf/index/core/pobjects/Document;->library:Lorg/icepdf/index/core/util/Library;

    iget-object v2, v2, Lorg/icepdf/index/core/util/Library;->securityManager:Lorg/icepdf/index/core/pobjects/security/SecurityManager;

    invoke-virtual {v2, v1}, Lorg/icepdf/index/core/pobjects/security/SecurityManager;->isAuthorized(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 682
    .end local v0    # "count":I
    .end local v1    # "password":Ljava/lang/String;
    :cond_3
    iget-object v2, p0, Lorg/icepdf/index/core/pobjects/Document;->library:Lorg/icepdf/index/core/util/Library;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lorg/icepdf/index/core/util/Library;->setEncrypted(Z)V

    .line 683
    return-void

    .line 673
    .restart local v0    # "count":I
    .restart local v1    # "password":Ljava/lang/String;
    :cond_4
    add-int/lit8 v0, v0, 0x1

    .line 675
    const/4 v2, 0x3

    if-le v0, v2, :cond_0

    .line 676
    new-instance v2, Lorg/icepdf/index/core/exceptions/PDFSecurityException;

    const-string/jumbo v3, "Encryption error"

    invoke-direct {v2, v3}, Lorg/icepdf/index/core/exceptions/PDFSecurityException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method private checkHeader(Lorg/icepdf/index/core/io/SeekableInput;)V
    .locals 9
    .param p1, "in"    # Lorg/icepdf/index/core/io/SeekableInput;

    .prologue
    const/4 v8, 0x7

    .line 263
    const/4 v2, 0x0

    .line 264
    .local v2, "header":Z
    invoke-interface {p1}, Lorg/icepdf/index/core/io/SeekableInput;->markSupported()Z

    move-result v7

    if-nez v7, :cond_1

    .line 301
    :cond_0
    :goto_0
    return-void

    .line 267
    :cond_1
    const/4 v6, 0x7

    .line 268
    .local v6, "scanLength":I
    :try_start_0
    const-string/jumbo v4, "%PDF-1."

    .line 269
    .local v4, "scanFor":Ljava/lang/String;
    const/4 v5, 0x0

    .line 270
    .local v5, "scanForIndex":I
    const/4 v7, 0x7

    invoke-interface {p1, v7}, Lorg/icepdf/index/core/io/SeekableInput;->mark(I)V

    .line 271
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    if-ge v3, v8, :cond_5

    .line 272
    invoke-interface {p1}, Lorg/icepdf/index/core/io/SeekableInput;->read()I

    move-result v0

    .line 273
    .local v0, "data":I
    if-gez v0, :cond_2

    .line 274
    invoke-interface {p1}, Lorg/icepdf/index/core/io/SeekableInput;->reset()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 292
    .end local v0    # "data":I
    .end local v3    # "i":I
    .end local v4    # "scanFor":Ljava/lang/String;
    .end local v5    # "scanForIndex":I
    :catch_0
    move-exception v1

    .line 294
    .local v1, "e":Ljava/io/IOException;
    :try_start_1
    invoke-interface {p1}, Lorg/icepdf/index/core/io/SeekableInput;->reset()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 299
    .end local v1    # "e":Ljava/io/IOException;
    :goto_2
    if-nez v2, :cond_0

    .line 300
    new-instance v7, Ljava/lang/RuntimeException;

    const-string/jumbo v8, "Invalid Header"

    invoke-direct {v7, v8}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 278
    .restart local v0    # "data":I
    .restart local v3    # "i":I
    .restart local v4    # "scanFor":Ljava/lang/String;
    .restart local v5    # "scanForIndex":I
    :cond_2
    :try_start_2
    const-string/jumbo v7, "%PDF-1."

    invoke-virtual {v7, v5}, Ljava/lang/String;->charAt(I)C

    move-result v7

    if-ne v0, v7, :cond_4

    .line 279
    add-int/lit8 v5, v5, 0x1

    .line 280
    const-string/jumbo v7, "%PDF-1."

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    if-ne v5, v7, :cond_3

    .line 282
    const/4 v2, 0x1

    .line 271
    :cond_3
    :goto_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 285
    :cond_4
    const/4 v5, 0x0

    goto :goto_3

    .line 290
    .end local v0    # "data":I
    :cond_5
    invoke-interface {p1}, Lorg/icepdf/index/core/io/SeekableInput;->reset()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_2

    .line 296
    .end local v3    # "i":I
    .end local v4    # "scanFor":Ljava/lang/String;
    .end local v5    # "scanForIndex":I
    .restart local v1    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v7

    goto :goto_2
.end method

.method private getInitialCrossReferencePosition(Lorg/icepdf/index/core/io/SeekableInput;)J
    .locals 18
    .param p1, "in"    # Lorg/icepdf/index/core/io/SeekableInput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 451
    invoke-interface/range {p1 .. p1}, Lorg/icepdf/index/core/io/SeekableInput;->seekEnd()V

    .line 453
    invoke-interface/range {p1 .. p1}, Lorg/icepdf/index/core/io/SeekableInput;->getAbsolutePosition()J

    move-result-wide v8

    .line 454
    .local v8, "endOfFile":J
    const-wide/16 v14, 0x1

    sub-long v6, v8, v14

    .line 455
    .local v6, "currentPosition":J
    const-wide/16 v2, -0x1

    .line 456
    .local v2, "afterStartxref":J
    const-string/jumbo v10, "startxref"

    .line 457
    .local v10, "startxref":Ljava/lang/String;
    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v13

    add-int/lit8 v11, v13, -0x1

    .line 459
    .local v11, "startxrefIndexToMatch":I
    :goto_0
    const-wide/16 v14, 0x0

    cmp-long v13, v6, v14

    if-ltz v13, :cond_1

    sub-long v14, v8, v6

    const-wide/16 v16, 0x800

    cmp-long v13, v14, v16

    if-gez v13, :cond_1

    .line 460
    move-object/from16 v0, p1

    invoke-interface {v0, v6, v7}, Lorg/icepdf/index/core/io/SeekableInput;->seekAbsolute(J)V

    .line 461
    invoke-interface/range {p1 .. p1}, Lorg/icepdf/index/core/io/SeekableInput;->read()I

    move-result v4

    .line 462
    .local v4, "curr":I
    if-gez v4, :cond_0

    .line 463
    new-instance v13, Ljava/io/EOFException;

    const-string/jumbo v14, "Could not find startxref at end of file"

    invoke-direct {v13, v14}, Ljava/io/EOFException;-><init>(Ljava/lang/String;)V

    throw v13

    .line 464
    :cond_0
    invoke-virtual {v10, v11}, Ljava/lang/String;->charAt(I)C

    move-result v13

    if-ne v4, v13, :cond_3

    .line 466
    if-nez v11, :cond_2

    .line 467
    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v13

    int-to-long v14, v13

    add-long v2, v6, v14

    .line 475
    .end local v4    # "curr":I
    :cond_1
    const-wide/16 v14, 0x0

    cmp-long v13, v2, v14

    if-gez v13, :cond_4

    .line 476
    new-instance v13, Ljava/io/EOFException;

    const-string/jumbo v14, "Could not find startxref near end of file"

    invoke-direct {v13, v14}, Ljava/io/EOFException;-><init>(Ljava/lang/String;)V

    throw v13

    .line 470
    .restart local v4    # "curr":I
    :cond_2
    add-int/lit8 v11, v11, -0x1

    .line 473
    :goto_1
    const-wide/16 v14, 0x1

    sub-long/2addr v6, v14

    .line 474
    goto :goto_0

    .line 472
    :cond_3
    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v13

    add-int/lit8 v11, v13, -0x1

    goto :goto_1

    .line 478
    .end local v4    # "curr":I
    :cond_4
    move-object/from16 v0, p1

    invoke-interface {v0, v2, v3}, Lorg/icepdf/index/core/io/SeekableInput;->seekAbsolute(J)V

    .line 479
    new-instance v5, Lorg/icepdf/index/core/util/Parser;

    move-object/from16 v0, p1

    invoke-direct {v5, v0}, Lorg/icepdf/index/core/util/Parser;-><init>(Lorg/icepdf/index/core/io/SeekableInput;)V

    .line 480
    .local v5, "parser":Lorg/icepdf/index/core/util/Parser;
    invoke-virtual {v5}, Lorg/icepdf/index/core/util/Parser;->getToken()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/Number;

    .line 481
    .local v12, "xrefPositionObj":Ljava/lang/Number;
    if-nez v12, :cond_5

    .line 482
    new-instance v13, Ljava/lang/RuntimeException;

    const-string/jumbo v14, "Could not find ending cross reference position"

    invoke-direct {v13, v14}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v13

    .line 483
    :cond_5
    invoke-virtual {v12}, Ljava/lang/Number;->longValue()J

    move-result-wide v14

    return-wide v14
.end method

.method private loadDocumentViaXRefs(Lorg/icepdf/index/core/io/SeekableInput;)V
    .locals 10
    .param p1, "in"    # Lorg/icepdf/index/core/io/SeekableInput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/icepdf/index/core/exceptions/PDFException;,
            Lorg/icepdf/index/core/exceptions/PDFSecurityException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 404
    invoke-direct {p0, p1}, Lorg/icepdf/index/core/pobjects/Document;->getInitialCrossReferencePosition(Lorg/icepdf/index/core/io/SeekableInput;)J

    move-result-wide v6

    .line 405
    .local v6, "xrefPosition":J
    const/4 v0, 0x0

    .line 406
    .local v0, "documentTrailer":Lorg/icepdf/index/core/pobjects/PTrailer;
    const-wide/16 v8, 0x0

    cmp-long v8, v6, v8

    if-lez v8, :cond_3

    .line 407
    invoke-interface {p1, v6, v7}, Lorg/icepdf/index/core/io/SeekableInput;->seekAbsolute(J)V

    .line 409
    new-instance v4, Lorg/icepdf/index/core/util/Parser;

    invoke-direct {v4, p1}, Lorg/icepdf/index/core/util/Parser;-><init>(Lorg/icepdf/index/core/io/SeekableInput;)V

    .line 410
    .local v4, "parser":Lorg/icepdf/index/core/util/Parser;
    iget-object v8, p0, Lorg/icepdf/index/core/pobjects/Document;->library:Lorg/icepdf/index/core/util/Library;

    invoke-virtual {v4, v8}, Lorg/icepdf/index/core/util/Parser;->getObject(Lorg/icepdf/index/core/util/Library;)Ljava/lang/Object;

    move-result-object v3

    .line 411
    .local v3, "obj":Ljava/lang/Object;
    instance-of v8, v3, Lorg/icepdf/index/core/pobjects/PObject;

    if-eqz v8, :cond_0

    .line 412
    check-cast v3, Lorg/icepdf/index/core/pobjects/PObject;

    .end local v3    # "obj":Ljava/lang/Object;
    invoke-virtual {v3}, Lorg/icepdf/index/core/pobjects/PObject;->getObject()Ljava/lang/Object;

    move-result-object v3

    .restart local v3    # "obj":Ljava/lang/Object;
    :cond_0
    move-object v5, v3

    .line 413
    check-cast v5, Lorg/icepdf/index/core/pobjects/PTrailer;

    .line 415
    .local v5, "trailer":Lorg/icepdf/index/core/pobjects/PTrailer;
    if-nez v5, :cond_1

    .line 416
    new-instance v8, Ljava/lang/RuntimeException;

    const-string/jumbo v9, "Could not find trailer"

    invoke-direct {v8, v9}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 417
    :cond_1
    invoke-virtual {v5}, Lorg/icepdf/index/core/pobjects/PTrailer;->getPrimaryCrossReference()Lorg/icepdf/index/core/pobjects/CrossReference;

    move-result-object v8

    if-nez v8, :cond_2

    .line 418
    new-instance v8, Ljava/lang/RuntimeException;

    const-string/jumbo v9, "Could not find cross reference"

    invoke-direct {v8, v9}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 420
    :cond_2
    if-nez v0, :cond_4

    .line 421
    move-object v0, v5

    .line 431
    .end local v3    # "obj":Ljava/lang/Object;
    .end local v4    # "parser":Lorg/icepdf/index/core/util/Parser;
    .end local v5    # "trailer":Lorg/icepdf/index/core/pobjects/PTrailer;
    :cond_3
    :goto_0
    if-nez v0, :cond_5

    .line 432
    new-instance v8, Ljava/lang/RuntimeException;

    const-string/jumbo v9, "Could not find document trailer"

    invoke-direct {v8, v9}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 423
    .restart local v3    # "obj":Ljava/lang/Object;
    .restart local v4    # "parser":Lorg/icepdf/index/core/util/Parser;
    .restart local v5    # "trailer":Lorg/icepdf/index/core/pobjects/PTrailer;
    :cond_4
    invoke-virtual {v0, v5}, Lorg/icepdf/index/core/pobjects/PTrailer;->addPreviousTrailer(Lorg/icepdf/index/core/pobjects/PTrailer;)V

    goto :goto_0

    .line 434
    .end local v3    # "obj":Ljava/lang/Object;
    .end local v4    # "parser":Lorg/icepdf/index/core/util/Parser;
    .end local v5    # "trailer":Lorg/icepdf/index/core/pobjects/PTrailer;
    :cond_5
    new-instance v1, Lorg/icepdf/index/core/util/LazyObjectLoader;

    iget-object v8, p0, Lorg/icepdf/index/core/pobjects/Document;->library:Lorg/icepdf/index/core/util/Library;

    invoke-virtual {v0}, Lorg/icepdf/index/core/pobjects/PTrailer;->getPrimaryCrossReference()Lorg/icepdf/index/core/pobjects/CrossReference;

    move-result-object v9

    invoke-direct {v1, v8, p1, v9}, Lorg/icepdf/index/core/util/LazyObjectLoader;-><init>(Lorg/icepdf/index/core/util/Library;Lorg/icepdf/index/core/io/SeekableInput;Lorg/icepdf/index/core/pobjects/CrossReference;)V

    .line 436
    .local v1, "lol":Lorg/icepdf/index/core/util/LazyObjectLoader;
    iget-object v8, p0, Lorg/icepdf/index/core/pobjects/Document;->library:Lorg/icepdf/index/core/util/Library;

    invoke-virtual {v8, v1}, Lorg/icepdf/index/core/util/Library;->setLazyObjectLoader(Lorg/icepdf/index/core/util/LazyObjectLoader;)V

    .line 438
    iput-object v0, p0, Lorg/icepdf/index/core/pobjects/Document;->pTrailer:Lorg/icepdf/index/core/pobjects/PTrailer;

    .line 439
    invoke-virtual {v0}, Lorg/icepdf/index/core/pobjects/PTrailer;->getRootCatalog()Lorg/icepdf/index/core/pobjects/Catalog;

    move-result-object v8

    iput-object v8, p0, Lorg/icepdf/index/core/pobjects/Document;->catalog:Lorg/icepdf/index/core/pobjects/Catalog;

    .line 440
    iget-object v8, p0, Lorg/icepdf/index/core/pobjects/Document;->library:Lorg/icepdf/index/core/util/Library;

    iget-object v9, p0, Lorg/icepdf/index/core/pobjects/Document;->catalog:Lorg/icepdf/index/core/pobjects/Catalog;

    invoke-virtual {v8, v9}, Lorg/icepdf/index/core/util/Library;->setCatalog(Lorg/icepdf/index/core/pobjects/Catalog;)V

    .line 442
    iget-object v8, p0, Lorg/icepdf/index/core/pobjects/Document;->catalog:Lorg/icepdf/index/core/pobjects/Catalog;

    if-nez v8, :cond_6

    .line 443
    new-instance v8, Ljava/lang/NullPointerException;

    const-string/jumbo v9, "Loading via xref failed to find catalog"

    invoke-direct {v8, v9}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 445
    :cond_6
    invoke-direct {p0, v0}, Lorg/icepdf/index/core/pobjects/Document;->makeSecurityManager(Lorg/icepdf/index/core/pobjects/PTrailer;)Z

    move-result v2

    .line 446
    .local v2, "madeSecurityManager":Z
    if-eqz v2, :cond_7

    .line 447
    invoke-direct {p0}, Lorg/icepdf/index/core/pobjects/Document;->attemptAuthorizeSecurityManager()V

    .line 448
    :cond_7
    return-void
.end method

.method private makeSecurityManager(Lorg/icepdf/index/core/pobjects/PTrailer;)Z
    .locals 6
    .param p1, "documentTrailer"    # Lorg/icepdf/index/core/pobjects/PTrailer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/icepdf/index/core/exceptions/PDFSecurityException;
        }
    .end annotation

    .prologue
    .line 633
    const/4 v2, 0x0

    .line 634
    .local v2, "madeSecurityManager":Z
    invoke-virtual {p1}, Lorg/icepdf/index/core/pobjects/PTrailer;->getEncrypt()Lorg/icepdf/index/core/util/Hashtable;

    move-result-object v0

    .line 635
    .local v0, "encryptDictionary":Lorg/icepdf/index/core/util/Hashtable;
    invoke-virtual {p1}, Lorg/icepdf/index/core/pobjects/PTrailer;->getID()Ljava/util/Vector;

    move-result-object v1

    .line 636
    .local v1, "fileID":Ljava/util/Vector;
    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    .line 638
    iget-object v3, p0, Lorg/icepdf/index/core/pobjects/Document;->library:Lorg/icepdf/index/core/util/Library;

    new-instance v4, Lorg/icepdf/index/core/pobjects/security/SecurityManager;

    iget-object v5, p0, Lorg/icepdf/index/core/pobjects/Document;->library:Lorg/icepdf/index/core/util/Library;

    invoke-direct {v4, v5, v0, v1}, Lorg/icepdf/index/core/pobjects/security/SecurityManager;-><init>(Lorg/icepdf/index/core/util/Library;Lorg/icepdf/index/core/util/Hashtable;Ljava/util/Vector;)V

    iput-object v4, v3, Lorg/icepdf/index/core/util/Library;->securityManager:Lorg/icepdf/index/core/pobjects/security/SecurityManager;

    .line 640
    const/4 v2, 0x1

    .line 642
    :cond_0
    return v2
.end method

.method private setInputStream(Lorg/icepdf/index/core/io/SeekableInput;)V
    .locals 4
    .param p1, "in"    # Lorg/icepdf/index/core/io/SeekableInput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/icepdf/index/core/exceptions/InvalidHeaderException;,
            Lorg/icepdf/index/core/exceptions/PDFException;,
            Lorg/icepdf/index/core/exceptions/PDFSecurityException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 314
    :try_start_0
    iput-object p1, p0, Lorg/icepdf/index/core/pobjects/Document;->documentSeekableInput:Lorg/icepdf/index/core/io/SeekableInput;

    .line 317
    new-instance v2, Lorg/icepdf/index/core/util/Library;

    invoke-direct {v2}, Lorg/icepdf/index/core/util/Library;-><init>()V

    iput-object v2, p0, Lorg/icepdf/index/core/pobjects/Document;->library:Lorg/icepdf/index/core/util/Library;
    :try_end_0
    .catch Lorg/icepdf/index/core/exceptions/PDFSecurityException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3

    .line 321
    :try_start_1
    invoke-direct {p0, p1}, Lorg/icepdf/index/core/pobjects/Document;->checkHeader(Lorg/icepdf/index/core/io/SeekableInput;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lorg/icepdf/index/core/exceptions/PDFSecurityException; {:try_start_1 .. :try_end_1} :catch_1

    .line 330
    const/4 v1, 0x0

    .line 332
    .local v1, "loaded":Z
    :try_start_2
    invoke-direct {p0, p1}, Lorg/icepdf/index/core/pobjects/Document;->loadDocumentViaXRefs(Lorg/icepdf/index/core/io/SeekableInput;)V
    :try_end_2
    .catch Lorg/icepdf/index/core/exceptions/PDFException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Lorg/icepdf/index/core/exceptions/PDFSecurityException; {:try_start_2 .. :try_end_2} :catch_4
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_5

    .line 333
    const/4 v1, 0x1

    .line 348
    :goto_0
    if-nez v1, :cond_2

    .line 350
    :try_start_3
    iget-object v2, p0, Lorg/icepdf/index/core/pobjects/Document;->catalog:Lorg/icepdf/index/core/pobjects/Catalog;

    if-eqz v2, :cond_0

    .line 351
    iget-object v2, p0, Lorg/icepdf/index/core/pobjects/Document;->catalog:Lorg/icepdf/index/core/pobjects/Catalog;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lorg/icepdf/index/core/pobjects/Catalog;->dispose(Z)V

    .line 352
    const/4 v2, 0x0

    iput-object v2, p0, Lorg/icepdf/index/core/pobjects/Document;->catalog:Lorg/icepdf/index/core/pobjects/Catalog;

    .line 354
    :cond_0
    iget-object v2, p0, Lorg/icepdf/index/core/pobjects/Document;->library:Lorg/icepdf/index/core/util/Library;

    if-eqz v2, :cond_1

    .line 355
    iget-object v2, p0, Lorg/icepdf/index/core/pobjects/Document;->library:Lorg/icepdf/index/core/util/Library;

    invoke-virtual {v2}, Lorg/icepdf/index/core/util/Library;->dispose()V

    .line 357
    :cond_1
    const/4 v2, 0x0

    iput-object v2, p0, Lorg/icepdf/index/core/pobjects/Document;->library:Lorg/icepdf/index/core/util/Library;

    .line 367
    :cond_2
    iget-object v2, p0, Lorg/icepdf/index/core/pobjects/Document;->catalog:Lorg/icepdf/index/core/pobjects/Catalog;

    if-eqz v2, :cond_3

    .line 368
    iget-object v2, p0, Lorg/icepdf/index/core/pobjects/Document;->catalog:Lorg/icepdf/index/core/pobjects/Catalog;

    invoke-virtual {v2}, Lorg/icepdf/index/core/pobjects/Catalog;->init()V

    .line 389
    :cond_3
    return-void

    .line 323
    .end local v1    # "loaded":Z
    :catch_0
    move-exception v0

    .line 324
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {p0}, Lorg/icepdf/index/core/pobjects/Document;->dispose()V

    .line 325
    new-instance v2, Lorg/icepdf/index/core/exceptions/InvalidHeaderException;

    const-string/jumbo v3, "Invalid Header. Unable to parse the document"

    invoke-direct {v2, v3}, Lorg/icepdf/index/core/exceptions/InvalidHeaderException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_3
    .catch Lorg/icepdf/index/core/exceptions/PDFSecurityException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3

    .line 370
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v0

    .line 371
    .local v0, "e":Lorg/icepdf/index/core/exceptions/PDFSecurityException;
    invoke-virtual {p0}, Lorg/icepdf/index/core/pobjects/Document;->dispose()V

    .line 372
    throw v0

    .line 336
    .end local v0    # "e":Lorg/icepdf/index/core/exceptions/PDFSecurityException;
    .restart local v1    # "loaded":Z
    :catch_2
    move-exception v0

    .line 337
    .local v0, "e":Lorg/icepdf/index/core/exceptions/PDFException;
    :try_start_4
    throw v0
    :try_end_4
    .catch Lorg/icepdf/index/core/exceptions/PDFSecurityException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3

    .line 385
    .end local v0    # "e":Lorg/icepdf/index/core/exceptions/PDFException;
    .end local v1    # "loaded":Z
    :catch_3
    move-exception v0

    .line 386
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {p0}, Lorg/icepdf/index/core/pobjects/Document;->dispose()V

    .line 387
    new-instance v2, Ljava/io/IOException;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 340
    .end local v0    # "e":Ljava/lang/Exception;
    .restart local v1    # "loaded":Z
    :catch_4
    move-exception v0

    .line 341
    .local v0, "e":Lorg/icepdf/index/core/exceptions/PDFSecurityException;
    :try_start_5
    throw v0
    :try_end_5
    .catch Lorg/icepdf/index/core/exceptions/PDFSecurityException; {:try_start_5 .. :try_end_5} :catch_1
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_3

    .line 342
    .end local v0    # "e":Lorg/icepdf/index/core/exceptions/PDFSecurityException;
    :catch_5
    move-exception v2

    goto :goto_0
.end method

.method private skipPastAnyPrefixJunk(Ljava/io/InputStream;)V
    .locals 9
    .param p1, "in"    # Ljava/io/InputStream;

    .prologue
    const/16 v8, 0x800

    .line 584
    invoke-virtual {p1}, Ljava/io/InputStream;->markSupported()Z

    move-result v7

    if-nez v7, :cond_1

    .line 624
    :cond_0
    :goto_0
    return-void

    .line 587
    :cond_1
    const/16 v6, 0x800

    .line 588
    .local v6, "scanLength":I
    :try_start_0
    const-string/jumbo v3, "%PDF-1."

    .line 589
    .local v3, "scanFor":Ljava/lang/String;
    const/4 v4, 0x0

    .line 590
    .local v4, "scanForIndex":I
    const/4 v5, 0x0

    .line 591
    .local v5, "scanForWhiteSpace":Z
    const/16 v7, 0x800

    invoke-virtual {p1, v7}, Ljava/io/InputStream;->mark(I)V

    .line 592
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    if-ge v2, v8, :cond_6

    .line 593
    invoke-virtual {p1}, Ljava/io/InputStream;->read()I

    move-result v0

    .line 594
    .local v0, "data":I
    if-gez v0, :cond_2

    .line 595
    invoke-virtual {p1}, Ljava/io/InputStream;->reset()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 617
    .end local v0    # "data":I
    .end local v2    # "i":I
    .end local v3    # "scanFor":Ljava/lang/String;
    .end local v4    # "scanForIndex":I
    .end local v5    # "scanForWhiteSpace":Z
    :catch_0
    move-exception v1

    .line 619
    .local v1, "e":Ljava/io/IOException;
    :try_start_1
    invoke-virtual {p1}, Ljava/io/InputStream;->reset()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 621
    :catch_1
    move-exception v7

    goto :goto_0

    .line 598
    .end local v1    # "e":Ljava/io/IOException;
    .restart local v0    # "data":I
    .restart local v2    # "i":I
    .restart local v3    # "scanFor":Ljava/lang/String;
    .restart local v4    # "scanForIndex":I
    .restart local v5    # "scanForWhiteSpace":Z
    :cond_2
    if-eqz v5, :cond_4

    .line 599
    int-to-char v7, v0

    :try_start_2
    invoke-static {v7}, Lorg/icepdf/index/core/util/Parser;->isWhitespace(C)Z

    move-result v7

    if-nez v7, :cond_0

    .line 592
    :cond_3
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 603
    :cond_4
    const-string/jumbo v7, "%PDF-1."

    invoke-virtual {v7, v4}, Ljava/lang/String;->charAt(I)C

    move-result v7

    if-ne v0, v7, :cond_5

    .line 604
    add-int/lit8 v4, v4, 0x1

    .line 605
    const-string/jumbo v7, "%PDF-1."

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    if-ne v4, v7, :cond_3

    .line 607
    const/4 v5, 0x1

    goto :goto_2

    .line 610
    :cond_5
    const/4 v4, 0x0

    goto :goto_2

    .line 615
    .end local v0    # "data":I
    :cond_6
    invoke-virtual {p1}, Ljava/io/InputStream;->reset()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0
.end method


# virtual methods
.method public dispose()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 710
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/Document;->catalog:Lorg/icepdf/index/core/pobjects/Catalog;

    if-eqz v0, :cond_0

    .line 711
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/Document;->catalog:Lorg/icepdf/index/core/pobjects/Catalog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lorg/icepdf/index/core/pobjects/Catalog;->dispose(Z)V

    .line 712
    iput-object v2, p0, Lorg/icepdf/index/core/pobjects/Document;->catalog:Lorg/icepdf/index/core/pobjects/Catalog;

    .line 714
    :cond_0
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/Document;->library:Lorg/icepdf/index/core/util/Library;

    if-eqz v0, :cond_1

    .line 715
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/Document;->library:Lorg/icepdf/index/core/util/Library;

    invoke-virtual {v0}, Lorg/icepdf/index/core/util/Library;->dispose()V

    .line 716
    iput-object v2, p0, Lorg/icepdf/index/core/pobjects/Document;->library:Lorg/icepdf/index/core/util/Library;

    .line 718
    :cond_1
    iput-object v2, p0, Lorg/icepdf/index/core/pobjects/Document;->pTrailer:Lorg/icepdf/index/core/pobjects/PTrailer;

    .line 719
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/Document;->documentSeekableInput:Lorg/icepdf/index/core/io/SeekableInput;

    if-eqz v0, :cond_2

    .line 721
    :try_start_0
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/Document;->documentSeekableInput:Lorg/icepdf/index/core/io/SeekableInput;

    invoke-interface {v0}, Lorg/icepdf/index/core/io/SeekableInput;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 726
    :goto_0
    iput-object v2, p0, Lorg/icepdf/index/core/pobjects/Document;->documentSeekableInput:Lorg/icepdf/index/core/io/SeekableInput;

    .line 729
    :cond_2
    return-void

    .line 723
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public getCatalog()Lorg/icepdf/index/core/pobjects/Catalog;
    .locals 1

    .prologue
    .line 835
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/Document;->catalog:Lorg/icepdf/index/core/pobjects/Catalog;

    return-object v0
.end method

.method public getNumberOfPages()I
    .locals 1

    .prologue
    .line 694
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/Document;->catalog:Lorg/icepdf/index/core/pobjects/Catalog;

    if-eqz v0, :cond_0

    .line 696
    :try_start_0
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/Document;->catalog:Lorg/icepdf/index/core/pobjects/Catalog;

    invoke-virtual {v0}, Lorg/icepdf/index/core/pobjects/Catalog;->getPageTree()Lorg/icepdf/index/core/pobjects/PageTree;

    move-result-object v0

    invoke-virtual {v0}, Lorg/icepdf/index/core/pobjects/PageTree;->getNumberOfPages()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 701
    :goto_0
    return v0

    .line 697
    :catch_0
    move-exception v0

    .line 701
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getPageText(I)Ljava/lang/StringBuilder;
    .locals 4
    .param p1, "pageNumber"    # I

    .prologue
    .line 745
    iget-object v3, p0, Lorg/icepdf/index/core/pobjects/Document;->catalog:Lorg/icepdf/index/core/pobjects/Catalog;

    invoke-virtual {v3}, Lorg/icepdf/index/core/pobjects/Catalog;->getPageTree()Lorg/icepdf/index/core/pobjects/PageTree;

    move-result-object v0

    .line 747
    .local v0, "pageTree":Lorg/icepdf/index/core/pobjects/PageTree;
    if-ltz p1, :cond_1

    invoke-virtual {v0}, Lorg/icepdf/index/core/pobjects/PageTree;->getNumberOfPages()I

    move-result v3

    if-ge p1, v3, :cond_1

    .line 749
    invoke-virtual {v0, p1, p0}, Lorg/icepdf/index/core/pobjects/PageTree;->getPage(ILjava/lang/Object;)Lorg/icepdf/index/core/pobjects/Page;

    move-result-object v1

    .line 750
    .local v1, "pg":Lorg/icepdf/index/core/pobjects/Page;
    const/4 v2, 0x0

    .line 751
    .local v2, "text":Ljava/lang/StringBuilder;
    if-eqz v1, :cond_0

    .line 752
    invoke-virtual {v1}, Lorg/icepdf/index/core/pobjects/Page;->getText()Ljava/lang/StringBuilder;

    move-result-object v2

    .line 754
    :cond_0
    iget-object v3, p0, Lorg/icepdf/index/core/pobjects/Document;->catalog:Lorg/icepdf/index/core/pobjects/Catalog;

    invoke-virtual {v3}, Lorg/icepdf/index/core/pobjects/Catalog;->getPageTree()Lorg/icepdf/index/core/pobjects/PageTree;

    move-result-object v3

    invoke-virtual {v3, v1, p0}, Lorg/icepdf/index/core/pobjects/PageTree;->releasePage(Lorg/icepdf/index/core/pobjects/Page;Ljava/lang/Object;)V

    .line 758
    .end local v1    # "pg":Lorg/icepdf/index/core/pobjects/Page;
    .end local v2    # "text":Ljava/lang/StringBuilder;
    :goto_0
    return-object v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public getPageTextContent(I)Ljava/lang/String;
    .locals 4
    .param p1, "pageNumber"    # I

    .prologue
    const/4 v2, 0x0

    .line 774
    iget-object v3, p0, Lorg/icepdf/index/core/pobjects/Document;->catalog:Lorg/icepdf/index/core/pobjects/Catalog;

    invoke-virtual {v3}, Lorg/icepdf/index/core/pobjects/Catalog;->getPageTree()Lorg/icepdf/index/core/pobjects/PageTree;

    move-result-object v0

    .line 776
    .local v0, "pageTree":Lorg/icepdf/index/core/pobjects/PageTree;
    if-ltz p1, :cond_0

    invoke-virtual {v0}, Lorg/icepdf/index/core/pobjects/PageTree;->getNumberOfPages()I

    move-result v3

    if-ge p1, v3, :cond_0

    .line 778
    invoke-virtual {v0, p1, p0}, Lorg/icepdf/index/core/pobjects/PageTree;->getPage(ILjava/lang/Object;)Lorg/icepdf/index/core/pobjects/Page;

    move-result-object v1

    .line 780
    .local v1, "pg":Lorg/icepdf/index/core/pobjects/Page;
    if-nez v1, :cond_1

    .line 790
    .end local v1    # "pg":Lorg/icepdf/index/core/pobjects/Page;
    :cond_0
    :goto_0
    return-object v2

    .line 783
    .restart local v1    # "pg":Lorg/icepdf/index/core/pobjects/Page;
    :cond_1
    invoke-virtual {v1}, Lorg/icepdf/index/core/pobjects/Page;->getTextContent()Ljava/lang/String;

    move-result-object v2

    .line 785
    .local v2, "text":Ljava/lang/String;
    iget-object v3, p0, Lorg/icepdf/index/core/pobjects/Document;->catalog:Lorg/icepdf/index/core/pobjects/Catalog;

    invoke-virtual {v3}, Lorg/icepdf/index/core/pobjects/Catalog;->getPageTree()Lorg/icepdf/index/core/pobjects/PageTree;

    move-result-object v3

    invoke-virtual {v3, v1, p0}, Lorg/icepdf/index/core/pobjects/PageTree;->releasePage(Lorg/icepdf/index/core/pobjects/Page;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public getPageTree()Lorg/icepdf/index/core/pobjects/PageTree;
    .locals 1

    .prologue
    .line 825
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/Document;->catalog:Lorg/icepdf/index/core/pobjects/Catalog;

    invoke-virtual {v0}, Lorg/icepdf/index/core/pobjects/Catalog;->getPageTree()Lorg/icepdf/index/core/pobjects/PageTree;

    move-result-object v0

    return-object v0
.end method

.method public getSecurityManager()Lorg/icepdf/index/core/pobjects/security/SecurityManager;
    .locals 1

    .prologue
    .line 802
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/Document;->library:Lorg/icepdf/index/core/util/Library;

    iget-object v0, v0, Lorg/icepdf/index/core/util/Library;->securityManager:Lorg/icepdf/index/core/pobjects/security/SecurityManager;

    return-object v0
.end method

.method public setFile(Ljava/lang/String;)V
    .locals 2
    .param p1, "filepath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/icepdf/index/core/exceptions/InvalidHeaderException;,
            Lorg/icepdf/index/core/exceptions/PDFException;,
            Lorg/icepdf/index/core/exceptions/PDFSecurityException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 132
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Lorg/icepdf/index/core/io/RandomAccessFileInputStream;->build(Ljava/io/File;)Lorg/icepdf/index/core/io/RandomAccessFileInputStream;

    move-result-object v0

    .line 154
    .local v0, "rafis":Lorg/icepdf/index/core/io/RandomAccessFileInputStream;
    invoke-direct {p0, v0}, Lorg/icepdf/index/core/pobjects/Document;->setInputStream(Lorg/icepdf/index/core/io/SeekableInput;)V

    .line 155
    return-void
.end method

.method public setInputStream(Ljava/io/InputStream;Ljava/lang/String;)V
    .locals 13
    .param p1, "in"    # Ljava/io/InputStream;
    .param p2, "pathOrURL"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/icepdf/index/core/exceptions/InvalidHeaderException;,
            Lorg/icepdf/index/core/exceptions/PDFException;,
            Lorg/icepdf/index/core/exceptions/PDFSecurityException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 174
    const/4 v2, 0x0

    .line 175
    .local v2, "byteArrayOutputStream":Lorg/icepdf/index/core/io/ConservativeSizingByteArrayOutputStream;
    const/4 v5, 0x0

    .line 177
    .local v5, "fileOutputStream":Ljava/io/FileOutputStream;
    :try_start_0
    sget-boolean v11, Lorg/icepdf/index/core/pobjects/Document;->isCachingEnabled:Z

    if-nez v11, :cond_5

    .line 181
    new-instance v3, Lorg/icepdf/index/core/io/ConservativeSizingByteArrayOutputStream;

    const v11, 0x19000

    const/4 v12, 0x0

    invoke-direct {v3, v11, v12}, Lorg/icepdf/index/core/io/ConservativeSizingByteArrayOutputStream;-><init>(ILorg/icepdf/index/core/util/MemoryManager;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 185
    .end local v2    # "byteArrayOutputStream":Lorg/icepdf/index/core/io/ConservativeSizingByteArrayOutputStream;
    .local v3, "byteArrayOutputStream":Lorg/icepdf/index/core/io/ConservativeSizingByteArrayOutputStream;
    const/16 v11, 0x1000

    :try_start_1
    new-array v0, v11, [B

    .line 189
    .local v0, "buffer":[B
    :goto_0
    const/4 v11, 0x0

    array-length v12, v0

    invoke-virtual {p1, v0, v11, v12}, Ljava/io/InputStream;->read([BII)I

    move-result v7

    .local v7, "length":I
    if-lez v7, :cond_2

    .line 190
    const/4 v11, 0x0

    invoke-virtual {v3, v0, v11, v7}, Lorg/icepdf/index/core/io/ConservativeSizingByteArrayOutputStream;->write([BII)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 241
    .end local v0    # "buffer":[B
    .end local v7    # "length":I
    :catchall_0
    move-exception v11

    move-object v2, v3

    .end local v3    # "byteArrayOutputStream":Lorg/icepdf/index/core/io/ConservativeSizingByteArrayOutputStream;
    .restart local v2    # "byteArrayOutputStream":Lorg/icepdf/index/core/io/ConservativeSizingByteArrayOutputStream;
    :goto_1
    if-eqz v2, :cond_0

    .line 243
    :try_start_2
    invoke-virtual {v2}, Lorg/icepdf/index/core/io/ConservativeSizingByteArrayOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    .line 248
    :cond_0
    :goto_2
    if-eqz v5, :cond_1

    .line 250
    :try_start_3
    invoke-virtual {v5}, Ljava/io/FileOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3

    .line 253
    :cond_1
    :goto_3
    throw v11

    .line 193
    .end local v2    # "byteArrayOutputStream":Lorg/icepdf/index/core/io/ConservativeSizingByteArrayOutputStream;
    .restart local v0    # "buffer":[B
    .restart local v3    # "byteArrayOutputStream":Lorg/icepdf/index/core/io/ConservativeSizingByteArrayOutputStream;
    .restart local v7    # "length":I
    :cond_2
    :try_start_4
    invoke-virtual {v3}, Lorg/icepdf/index/core/io/ConservativeSizingByteArrayOutputStream;->flush()V

    .line 195
    invoke-virtual {v3}, Lorg/icepdf/index/core/io/ConservativeSizingByteArrayOutputStream;->size()I

    move-result v9

    .line 196
    .local v9, "size":I
    invoke-virtual {v3}, Lorg/icepdf/index/core/io/ConservativeSizingByteArrayOutputStream;->trim()Z

    .line 197
    invoke-virtual {v3}, Lorg/icepdf/index/core/io/ConservativeSizingByteArrayOutputStream;->relinquishByteArray()[B

    move-result-object v4

    .line 202
    .local v4, "data":[B
    new-instance v1, Lorg/icepdf/index/core/io/SeekableByteArrayInputStream;

    const/4 v11, 0x0

    invoke-direct {v1, v4, v11, v9}, Lorg/icepdf/index/core/io/SeekableByteArrayInputStream;-><init>([BII)V

    .line 204
    .local v1, "byteArrayInputStream":Lorg/icepdf/index/core/io/SeekableByteArrayInputStream;
    invoke-direct {p0, v1}, Lorg/icepdf/index/core/pobjects/Document;->setInputStream(Lorg/icepdf/index/core/io/SeekableInput;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-object v2, v3

    .line 241
    .end local v1    # "byteArrayInputStream":Lorg/icepdf/index/core/io/SeekableByteArrayInputStream;
    .end local v3    # "byteArrayOutputStream":Lorg/icepdf/index/core/io/ConservativeSizingByteArrayOutputStream;
    .end local v4    # "data":[B
    .end local v9    # "size":I
    .restart local v2    # "byteArrayOutputStream":Lorg/icepdf/index/core/io/ConservativeSizingByteArrayOutputStream;
    :goto_4
    if-eqz v2, :cond_3

    .line 243
    :try_start_5
    invoke-virtual {v2}, Lorg/icepdf/index/core/io/ConservativeSizingByteArrayOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0

    .line 248
    :cond_3
    :goto_5
    if-eqz v5, :cond_4

    .line 250
    :try_start_6
    invoke-virtual {v5}, Ljava/io/FileOutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1

    .line 256
    :cond_4
    :goto_6
    return-void

    .line 211
    .end local v0    # "buffer":[B
    .end local v7    # "length":I
    :cond_5
    :try_start_7
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v12, "ICEpdfTempFile"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/Object;->hashCode()I

    move-result v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    const-string/jumbo v12, ".tmp"

    invoke-static {v11, v12}, Ljava/io/File;->createTempFile(Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object v10

    .line 214
    .local v10, "tempFile":Ljava/io/File;
    invoke-virtual {v10}, Ljava/io/File;->deleteOnExit()V

    .line 217
    new-instance v6, Ljava/io/FileOutputStream;

    invoke-virtual {v10}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x1

    invoke-direct {v6, v11, v12}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;Z)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 221
    .end local v5    # "fileOutputStream":Ljava/io/FileOutputStream;
    .local v6, "fileOutputStream":Ljava/io/FileOutputStream;
    const/16 v11, 0x1000

    :try_start_8
    new-array v0, v11, [B

    .line 224
    .restart local v0    # "buffer":[B
    :goto_7
    const/4 v11, 0x0

    array-length v12, v0

    invoke-virtual {p1, v0, v11, v12}, Ljava/io/InputStream;->read([BII)I

    move-result v7

    .restart local v7    # "length":I
    if-lez v7, :cond_6

    .line 225
    const/4 v11, 0x0

    invoke-virtual {v6, v0, v11, v7}, Ljava/io/FileOutputStream;->write([BII)V

    goto :goto_7

    .line 241
    .end local v0    # "buffer":[B
    .end local v7    # "length":I
    :catchall_1
    move-exception v11

    move-object v5, v6

    .end local v6    # "fileOutputStream":Ljava/io/FileOutputStream;
    .restart local v5    # "fileOutputStream":Ljava/io/FileOutputStream;
    goto :goto_1

    .line 228
    .end local v5    # "fileOutputStream":Ljava/io/FileOutputStream;
    .restart local v0    # "buffer":[B
    .restart local v6    # "fileOutputStream":Ljava/io/FileOutputStream;
    .restart local v7    # "length":I
    :cond_6
    invoke-virtual {v6}, Ljava/io/FileOutputStream;->flush()V

    .line 236
    invoke-static {v10}, Lorg/icepdf/index/core/io/RandomAccessFileInputStream;->build(Ljava/io/File;)Lorg/icepdf/index/core/io/RandomAccessFileInputStream;

    move-result-object v8

    .line 238
    .local v8, "rafis":Lorg/icepdf/index/core/io/RandomAccessFileInputStream;
    invoke-direct {p0, v8}, Lorg/icepdf/index/core/pobjects/Document;->setInputStream(Lorg/icepdf/index/core/io/SeekableInput;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    move-object v5, v6

    .end local v6    # "fileOutputStream":Ljava/io/FileOutputStream;
    .restart local v5    # "fileOutputStream":Ljava/io/FileOutputStream;
    goto :goto_4

    .line 244
    .end local v8    # "rafis":Lorg/icepdf/index/core/io/RandomAccessFileInputStream;
    .end local v10    # "tempFile":Ljava/io/File;
    :catch_0
    move-exception v11

    goto :goto_5

    .line 251
    :catch_1
    move-exception v11

    goto :goto_6

    .line 244
    .end local v0    # "buffer":[B
    .end local v7    # "length":I
    :catch_2
    move-exception v12

    goto :goto_2

    .line 251
    :catch_3
    move-exception v12

    goto :goto_3

    .line 241
    :catchall_2
    move-exception v11

    goto/16 :goto_1
.end method

.method public setSecurityCallback(Lorg/icepdf/index/core/SecurityCallback;)V
    .locals 0
    .param p1, "securityCallback"    # Lorg/icepdf/index/core/SecurityCallback;

    .prologue
    .line 814
    iput-object p1, p0, Lorg/icepdf/index/core/pobjects/Document;->securityCallback:Lorg/icepdf/index/core/SecurityCallback;

    .line 815
    return-void
.end method
