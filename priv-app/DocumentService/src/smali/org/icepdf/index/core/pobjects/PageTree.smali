.class public Lorg/icepdf/index/core/pobjects/PageTree;
.super Lorg/icepdf/index/core/pobjects/Dictionary;
.source "PageTree.java"


# instance fields
.field private cropBox:Lorg/icepdf/index/core/pobjects/PRectangle;

.field private inited:Z

.field protected isRotationFactor:Z

.field private kidsCount:I

.field private kidsPageAndPages:Ljava/util/Vector;

.field private kidsReferences:Ljava/util/Vector;

.field private mediaBox:Lorg/icepdf/index/core/pobjects/PRectangle;

.field private parent:Lorg/icepdf/index/core/pobjects/PageTree;

.field private resources:Lorg/icepdf/index/core/pobjects/Resources;

.field protected rotationFactor:F


# direct methods
.method public constructor <init>(Lorg/icepdf/index/core/util/Library;Lorg/icepdf/index/core/util/Hashtable;)V
    .locals 2
    .param p1, "l"    # Lorg/icepdf/index/core/util/Library;
    .param p2, "h"    # Lorg/icepdf/index/core/util/Hashtable;

    .prologue
    const/4 v1, 0x0

    .line 73
    invoke-direct {p0, p1, p2}, Lorg/icepdf/index/core/pobjects/Dictionary;-><init>(Lorg/icepdf/index/core/util/Library;Lorg/icepdf/index/core/util/Hashtable;)V

    .line 41
    iput v1, p0, Lorg/icepdf/index/core/pobjects/PageTree;->kidsCount:I

    .line 59
    const/4 v0, 0x0

    iput v0, p0, Lorg/icepdf/index/core/pobjects/PageTree;->rotationFactor:F

    .line 64
    iput-boolean v1, p0, Lorg/icepdf/index/core/pobjects/PageTree;->isRotationFactor:Z

    .line 74
    return-void
.end method

.method private declared-synchronized getPageOrPagesPotentiallyNotInitedFromReferenceAt(I)Ljava/lang/Object;
    .locals 3
    .param p1, "index"    # I

    .prologue
    .line 254
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lorg/icepdf/index/core/pobjects/PageTree;->kidsPageAndPages:Ljava/util/Vector;

    invoke-virtual {v2, p1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 255
    .local v0, "pageOrPages":Ljava/lang/Object;
    if-nez v0, :cond_0

    .line 256
    iget-object v2, p0, Lorg/icepdf/index/core/pobjects/PageTree;->kidsReferences:Ljava/util/Vector;

    invoke-virtual {v2, p1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/icepdf/index/core/pobjects/Reference;

    .line 257
    .local v1, "ref":Lorg/icepdf/index/core/pobjects/Reference;
    iget-object v2, p0, Lorg/icepdf/index/core/pobjects/PageTree;->library:Lorg/icepdf/index/core/util/Library;

    invoke-virtual {v2, v1}, Lorg/icepdf/index/core/util/Library;->getObject(Lorg/icepdf/index/core/pobjects/Reference;)Ljava/lang/Object;

    move-result-object v0

    .line 258
    iget-object v2, p0, Lorg/icepdf/index/core/pobjects/PageTree;->kidsPageAndPages:Ljava/util/Vector;

    invoke-virtual {v2, p1, v0}, Ljava/util/Vector;->set(ILjava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 260
    .end local v1    # "ref":Lorg/icepdf/index/core/pobjects/Reference;
    :cond_0
    monitor-exit p0

    return-object v0

    .line 254
    .end local v0    # "pageOrPages":Ljava/lang/Object;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method private declared-synchronized getPagePotentiallyNotInitedByRecursiveIndex(I)Lorg/icepdf/index/core/pobjects/Page;
    .locals 8
    .param p1, "globalIndex"    # I

    .prologue
    .line 271
    monitor-enter p0

    const/4 v2, 0x0

    .line 272
    .local v2, "globalIndexSoFar":I
    :try_start_0
    iget-object v7, p0, Lorg/icepdf/index/core/pobjects/PageTree;->kidsPageAndPages:Ljava/util/Vector;

    invoke-virtual {v7}, Ljava/util/Vector;->size()I

    move-result v5

    .line 273
    .local v5, "numLocalKids":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-ge v3, v5, :cond_4

    .line 274
    invoke-direct {p0, v3}, Lorg/icepdf/index/core/pobjects/PageTree;->getPageOrPagesPotentiallyNotInitedFromReferenceAt(I)Ljava/lang/Object;

    move-result-object v6

    .line 275
    .local v6, "pageOrPages":Ljava/lang/Object;
    instance-of v7, v6, Lorg/icepdf/index/core/pobjects/Page;

    if-eqz v7, :cond_2

    .line 276
    if-ne p1, v2, :cond_0

    .line 277
    check-cast v6, Lorg/icepdf/index/core/pobjects/Page;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 290
    .end local v6    # "pageOrPages":Ljava/lang/Object;
    :goto_1
    monitor-exit p0

    return-object v6

    .line 278
    .restart local v6    # "pageOrPages":Ljava/lang/Object;
    :cond_0
    add-int/lit8 v2, v2, 0x1

    .line 273
    :cond_1
    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 279
    :cond_2
    :try_start_1
    instance-of v7, v6, Lorg/icepdf/index/core/pobjects/PageTree;

    if-eqz v7, :cond_1

    .line 280
    move-object v0, v6

    check-cast v0, Lorg/icepdf/index/core/pobjects/PageTree;

    move-object v1, v0

    .line 281
    .local v1, "childPageTree":Lorg/icepdf/index/core/pobjects/PageTree;
    invoke-virtual {v1}, Lorg/icepdf/index/core/pobjects/PageTree;->init()V

    .line 282
    invoke-virtual {v1}, Lorg/icepdf/index/core/pobjects/PageTree;->getNumberOfPages()I

    move-result v4

    .line 283
    .local v4, "numChildPages":I
    if-lt p1, v2, :cond_3

    add-int v7, v2, v4

    if-ge p1, v7, :cond_3

    .line 284
    sub-int v7, p1, v2

    invoke-direct {v1, v7}, Lorg/icepdf/index/core/pobjects/PageTree;->getPagePotentiallyNotInitedByRecursiveIndex(I)Lorg/icepdf/index/core/pobjects/Page;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v6

    goto :goto_1

    .line 287
    :cond_3
    add-int/2addr v2, v4

    goto :goto_2

    .line 290
    .end local v1    # "childPageTree":Lorg/icepdf/index/core/pobjects/PageTree;
    .end local v4    # "numChildPages":I
    .end local v6    # "pageOrPages":Ljava/lang/Object;
    :cond_4
    const/4 v6, 0x0

    goto :goto_1

    .line 271
    .end local v3    # "i":I
    .end local v5    # "numLocalKids":I
    :catchall_0
    move-exception v7

    monitor-exit p0

    throw v7
.end method

.method private declared-synchronized indexOfKidReference(Lorg/icepdf/index/core/pobjects/Reference;)I
    .locals 3
    .param p1, "r"    # Lorg/icepdf/index/core/pobjects/Reference;

    .prologue
    .line 239
    monitor-enter p0

    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    :try_start_0
    iget-object v2, p0, Lorg/icepdf/index/core/pobjects/PageTree;->kidsReferences:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 240
    iget-object v2, p0, Lorg/icepdf/index/core/pobjects/PageTree;->kidsReferences:Ljava/util/Vector;

    invoke-virtual {v2, v0}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/icepdf/index/core/pobjects/Reference;

    .line 241
    .local v1, "ref":Lorg/icepdf/index/core/pobjects/Reference;
    invoke-virtual {v1, p1}, Lorg/icepdf/index/core/pobjects/Reference;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-eqz v2, :cond_0

    .line 244
    .end local v0    # "i":I
    .end local v1    # "ref":Lorg/icepdf/index/core/pobjects/Reference;
    :goto_1
    monitor-exit p0

    return v0

    .line 239
    .restart local v0    # "i":I
    .restart local v1    # "ref":Lorg/icepdf/index/core/pobjects/Reference;
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 244
    .end local v1    # "ref":Lorg/icepdf/index/core/pobjects/Reference;
    :cond_1
    const/4 v0, -0x1

    goto :goto_1

    .line 239
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method


# virtual methods
.method protected declared-synchronized dispose(Z)V
    .locals 3
    .param p1, "cache"    # Z

    .prologue
    .line 80
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lorg/icepdf/index/core/pobjects/PageTree;->kidsReferences:Ljava/util/Vector;

    if-eqz v2, :cond_0

    .line 81
    if-nez p1, :cond_0

    .line 82
    iget-object v2, p0, Lorg/icepdf/index/core/pobjects/PageTree;->kidsReferences:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->clear()V

    .line 85
    :cond_0
    iget-object v2, p0, Lorg/icepdf/index/core/pobjects/PageTree;->kidsPageAndPages:Ljava/util/Vector;

    if-eqz v2, :cond_4

    .line 86
    iget-object v2, p0, Lorg/icepdf/index/core/pobjects/PageTree;->kidsPageAndPages:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 87
    .local v1, "pageOrPages":Ljava/lang/Object;
    instance-of v2, v1, Lorg/icepdf/index/core/pobjects/Page;

    if-eqz v2, :cond_2

    .line 88
    check-cast v1, Lorg/icepdf/index/core/pobjects/Page;

    .end local v1    # "pageOrPages":Ljava/lang/Object;
    invoke-virtual {v1, p1}, Lorg/icepdf/index/core/pobjects/Page;->dispose(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 80
    .end local v0    # "i$":Ljava/util/Iterator;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 89
    .restart local v0    # "i$":Ljava/util/Iterator;
    .restart local v1    # "pageOrPages":Ljava/lang/Object;
    :cond_2
    :try_start_1
    instance-of v2, v1, Lorg/icepdf/index/core/pobjects/PageTree;

    if-eqz v2, :cond_1

    .line 90
    check-cast v1, Lorg/icepdf/index/core/pobjects/PageTree;

    .end local v1    # "pageOrPages":Ljava/lang/Object;
    invoke-virtual {v1, p1}, Lorg/icepdf/index/core/pobjects/PageTree;->dispose(Z)V

    goto :goto_0

    .line 92
    :cond_3
    if-nez p1, :cond_4

    .line 93
    iget-object v2, p0, Lorg/icepdf/index/core/pobjects/PageTree;->kidsPageAndPages:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->clear()V

    .line 96
    .end local v0    # "i$":Ljava/util/Iterator;
    :cond_4
    iget-object v2, p0, Lorg/icepdf/index/core/pobjects/PageTree;->resources:Lorg/icepdf/index/core/pobjects/Resources;

    if-eqz v2, :cond_5

    .line 97
    iget-object v2, p0, Lorg/icepdf/index/core/pobjects/PageTree;->resources:Lorg/icepdf/index/core/pobjects/Resources;

    invoke-virtual {v2, p1}, Lorg/icepdf/index/core/pobjects/Resources;->dispose(Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 99
    :cond_5
    monitor-exit p0

    return-void
.end method

.method public getCropBox()Lorg/icepdf/index/core/pobjects/PRectangle;
    .locals 1

    .prologue
    .line 168
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/PageTree;->cropBox:Lorg/icepdf/index/core/pobjects/PRectangle;

    return-object v0
.end method

.method public getMediaBox()Lorg/icepdf/index/core/pobjects/PRectangle;
    .locals 1

    .prologue
    .line 158
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/PageTree;->mediaBox:Lorg/icepdf/index/core/pobjects/PRectangle;

    return-object v0
.end method

.method public declared-synchronized getNumberOfPages()I
    .locals 1

    .prologue
    .line 307
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lorg/icepdf/index/core/pobjects/PageTree;->kidsCount:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getPage(ILjava/lang/Object;)Lorg/icepdf/index/core/pobjects/Page;
    .locals 2
    .param p1, "pageNumber"    # I
    .param p2, "user"    # Ljava/lang/Object;

    .prologue
    .line 329
    if-gez p1, :cond_1

    .line 330
    const/4 v0, 0x0

    .line 337
    :cond_0
    :goto_0
    return-object v0

    .line 331
    :cond_1
    invoke-direct {p0, p1}, Lorg/icepdf/index/core/pobjects/PageTree;->getPagePotentiallyNotInitedByRecursiveIndex(I)Lorg/icepdf/index/core/pobjects/Page;

    move-result-object v0

    .line 332
    .local v0, "p":Lorg/icepdf/index/core/pobjects/Page;
    if-eqz v0, :cond_0

    .line 334
    iget-object v1, p0, Lorg/icepdf/index/core/pobjects/PageTree;->library:Lorg/icepdf/index/core/util/Library;

    iget-object v1, v1, Lorg/icepdf/index/core/util/Library;->memoryManager:Lorg/icepdf/index/core/util/MemoryManager;

    invoke-virtual {v1, p2, v0}, Lorg/icepdf/index/core/util/MemoryManager;->lock(Ljava/lang/Object;Lorg/icepdf/index/core/util/MemoryManageable;)V

    goto :goto_0
.end method

.method public declared-synchronized getPageNumber(Lorg/icepdf/index/core/pobjects/Reference;)I
    .locals 14
    .param p1, "r"    # Lorg/icepdf/index/core/pobjects/Reference;

    .prologue
    const/4 v11, -0x1

    .line 200
    monitor-enter p0

    :try_start_0
    iget-object v12, p0, Lorg/icepdf/index/core/pobjects/PageTree;->library:Lorg/icepdf/index/core/util/Library;

    invoke-virtual {v12, p1}, Lorg/icepdf/index/core/util/Library;->getObject(Lorg/icepdf/index/core/pobjects/Reference;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lorg/icepdf/index/core/pobjects/Page;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 201
    .local v9, "pg":Lorg/icepdf/index/core/pobjects/Page;
    if-nez v9, :cond_1

    move v4, v11

    .line 229
    :cond_0
    :goto_0
    monitor-exit p0

    return v4

    .line 204
    :cond_1
    const/4 v4, 0x0

    .line 205
    .local v4, "globalIndex":I
    move-object v1, p1

    .line 206
    .local v1, "currChildRef":Lorg/icepdf/index/core/pobjects/Reference;
    :try_start_1
    invoke-virtual {v9}, Lorg/icepdf/index/core/pobjects/Page;->getParentReference()Lorg/icepdf/index/core/pobjects/Reference;

    move-result-object v3

    .line 207
    .local v3, "currParentRef":Lorg/icepdf/index/core/pobjects/Reference;
    invoke-virtual {v9}, Lorg/icepdf/index/core/pobjects/Page;->getParent()Lorg/icepdf/index/core/pobjects/PageTree;

    move-result-object v2

    .line 208
    .local v2, "currParent":Lorg/icepdf/index/core/pobjects/PageTree;
    :goto_1
    if-eqz v3, :cond_0

    if-eqz v2, :cond_0

    .line 209
    invoke-virtual {v2}, Lorg/icepdf/index/core/pobjects/PageTree;->init()V

    .line 210
    invoke-direct {v2, v1}, Lorg/icepdf/index/core/pobjects/PageTree;->indexOfKidReference(Lorg/icepdf/index/core/pobjects/Reference;)I

    move-result v10

    .line 211
    .local v10, "refIndex":I
    if-gez v10, :cond_2

    move v4, v11

    .line 212
    goto :goto_0

    .line 213
    :cond_2
    const/4 v6, 0x0

    .line 214
    .local v6, "localIndex":I
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_2
    if-ge v5, v10, :cond_5

    .line 215
    invoke-direct {v2, v5}, Lorg/icepdf/index/core/pobjects/PageTree;->getPageOrPagesPotentiallyNotInitedFromReferenceAt(I)Ljava/lang/Object;

    move-result-object v7

    .line 216
    .local v7, "pageOrPages":Ljava/lang/Object;
    instance-of v12, v7, Lorg/icepdf/index/core/pobjects/Page;

    if-eqz v12, :cond_4

    .line 217
    add-int/lit8 v6, v6, 0x1

    .line 214
    :cond_3
    :goto_3
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 218
    :cond_4
    instance-of v12, v7, Lorg/icepdf/index/core/pobjects/PageTree;

    if-eqz v12, :cond_3

    .line 219
    move-object v0, v7

    check-cast v0, Lorg/icepdf/index/core/pobjects/PageTree;

    move-object v8, v0

    .line 220
    .local v8, "peerPageTree":Lorg/icepdf/index/core/pobjects/PageTree;
    invoke-virtual {v8}, Lorg/icepdf/index/core/pobjects/PageTree;->init()V

    .line 221
    invoke-virtual {v8}, Lorg/icepdf/index/core/pobjects/PageTree;->getNumberOfPages()I

    move-result v12

    add-int/2addr v6, v12

    goto :goto_3

    .line 224
    .end local v7    # "pageOrPages":Ljava/lang/Object;
    .end local v8    # "peerPageTree":Lorg/icepdf/index/core/pobjects/PageTree;
    :cond_5
    add-int/2addr v4, v6

    .line 225
    move-object v1, v3

    .line 226
    iget-object v12, v2, Lorg/icepdf/index/core/pobjects/PageTree;->entries:Lorg/icepdf/index/core/util/Hashtable;

    const-string/jumbo v13, "Parent"

    invoke-virtual {v12, v13}, Lorg/icepdf/index/core/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "currParentRef":Lorg/icepdf/index/core/pobjects/Reference;
    check-cast v3, Lorg/icepdf/index/core/pobjects/Reference;

    .line 227
    .restart local v3    # "currParentRef":Lorg/icepdf/index/core/pobjects/Reference;
    iget-object v2, v2, Lorg/icepdf/index/core/pobjects/PageTree;->parent:Lorg/icepdf/index/core/pobjects/PageTree;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 228
    goto :goto_1

    .line 200
    .end local v1    # "currChildRef":Lorg/icepdf/index/core/pobjects/Reference;
    .end local v2    # "currParent":Lorg/icepdf/index/core/pobjects/PageTree;
    .end local v3    # "currParentRef":Lorg/icepdf/index/core/pobjects/Reference;
    .end local v4    # "globalIndex":I
    .end local v5    # "i":I
    .end local v6    # "localIndex":I
    .end local v9    # "pg":Lorg/icepdf/index/core/pobjects/Page;
    .end local v10    # "refIndex":I
    :catchall_0
    move-exception v11

    monitor-exit p0

    throw v11
.end method

.method public getParent()Lorg/icepdf/index/core/pobjects/PageTree;
    .locals 1

    .prologue
    .line 188
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/PageTree;->parent:Lorg/icepdf/index/core/pobjects/PageTree;

    return-object v0
.end method

.method public declared-synchronized getResources()Lorg/icepdf/index/core/pobjects/Resources;
    .locals 1

    .prologue
    .line 179
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/PageTree;->resources:Lorg/icepdf/index/core/pobjects/Resources;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized init()V
    .locals 8

    .prologue
    .line 105
    monitor-enter p0

    :try_start_0
    iget-boolean v5, p0, Lorg/icepdf/index/core/pobjects/PageTree;->inited:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v5, :cond_1

    .line 145
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 109
    :cond_1
    :try_start_1
    iget-object v5, p0, Lorg/icepdf/index/core/pobjects/PageTree;->library:Lorg/icepdf/index/core/util/Library;

    if-eqz v5, :cond_0

    .line 112
    iget-object v5, p0, Lorg/icepdf/index/core/pobjects/PageTree;->library:Lorg/icepdf/index/core/util/Library;

    iget-object v6, p0, Lorg/icepdf/index/core/pobjects/PageTree;->entries:Lorg/icepdf/index/core/util/Hashtable;

    const-string/jumbo v7, "Parent"

    invoke-virtual {v5, v6, v7}, Lorg/icepdf/index/core/util/Library;->getObject(Lorg/icepdf/index/core/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    .line 113
    .local v3, "parentTree":Ljava/lang/Object;
    instance-of v5, v3, Lorg/icepdf/index/core/pobjects/PageTree;

    if-eqz v5, :cond_2

    .line 114
    iget-object v5, p0, Lorg/icepdf/index/core/pobjects/PageTree;->library:Lorg/icepdf/index/core/util/Library;

    iget-object v6, p0, Lorg/icepdf/index/core/pobjects/PageTree;->entries:Lorg/icepdf/index/core/util/Hashtable;

    const-string/jumbo v7, "Parent"

    invoke-virtual {v5, v6, v7}, Lorg/icepdf/index/core/util/Library;->getObject(Lorg/icepdf/index/core/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/icepdf/index/core/pobjects/PageTree;

    iput-object v5, p0, Lorg/icepdf/index/core/pobjects/PageTree;->parent:Lorg/icepdf/index/core/pobjects/PageTree;

    .line 116
    :cond_2
    iget-object v5, p0, Lorg/icepdf/index/core/pobjects/PageTree;->library:Lorg/icepdf/index/core/util/Library;

    iget-object v6, p0, Lorg/icepdf/index/core/pobjects/PageTree;->entries:Lorg/icepdf/index/core/util/Hashtable;

    const-string/jumbo v7, "Count"

    invoke-virtual {v5, v6, v7}, Lorg/icepdf/index/core/util/Library;->getNumber(Lorg/icepdf/index/core/util/Hashtable;Ljava/lang/String;)Ljava/lang/Number;

    move-result-object v2

    .line 117
    .local v2, "n":Ljava/lang/Number;
    if-eqz v2, :cond_6

    .line 118
    invoke-virtual {v2}, Ljava/lang/Number;->intValue()I

    move-result v5

    iput v5, p0, Lorg/icepdf/index/core/pobjects/PageTree;->kidsCount:I

    .line 121
    :goto_1
    iget-object v5, p0, Lorg/icepdf/index/core/pobjects/PageTree;->library:Lorg/icepdf/index/core/util/Library;

    iget-object v6, p0, Lorg/icepdf/index/core/pobjects/PageTree;->entries:Lorg/icepdf/index/core/util/Hashtable;

    const-string/jumbo v7, "MediaBox"

    invoke-virtual {v5, v6, v7}, Lorg/icepdf/index/core/util/Library;->getObject(Lorg/icepdf/index/core/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/Vector;

    move-object v0, v5

    check-cast v0, Ljava/util/Vector;

    move-object v1, v0

    .line 122
    .local v1, "boxDimensions":Ljava/util/Vector;
    if-eqz v1, :cond_3

    .line 123
    new-instance v5, Lorg/icepdf/index/core/pobjects/PRectangle;

    invoke-direct {v5, v1}, Lorg/icepdf/index/core/pobjects/PRectangle;-><init>(Ljava/util/Vector;)V

    iput-object v5, p0, Lorg/icepdf/index/core/pobjects/PageTree;->mediaBox:Lorg/icepdf/index/core/pobjects/PRectangle;

    .line 126
    :cond_3
    iget-object v5, p0, Lorg/icepdf/index/core/pobjects/PageTree;->library:Lorg/icepdf/index/core/util/Library;

    iget-object v6, p0, Lorg/icepdf/index/core/pobjects/PageTree;->entries:Lorg/icepdf/index/core/util/Hashtable;

    const-string/jumbo v7, "CropBox"

    invoke-virtual {v5, v6, v7}, Lorg/icepdf/index/core/util/Library;->getObject(Lorg/icepdf/index/core/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/Vector;

    move-object v0, v5

    check-cast v0, Ljava/util/Vector;

    move-object v1, v0

    .line 127
    if-eqz v1, :cond_4

    .line 128
    new-instance v5, Lorg/icepdf/index/core/pobjects/PRectangle;

    invoke-direct {v5, v1}, Lorg/icepdf/index/core/pobjects/PRectangle;-><init>(Ljava/util/Vector;)V

    iput-object v5, p0, Lorg/icepdf/index/core/pobjects/PageTree;->cropBox:Lorg/icepdf/index/core/pobjects/PRectangle;

    .line 131
    :cond_4
    iget-object v5, p0, Lorg/icepdf/index/core/pobjects/PageTree;->library:Lorg/icepdf/index/core/util/Library;

    iget-object v6, p0, Lorg/icepdf/index/core/pobjects/PageTree;->entries:Lorg/icepdf/index/core/util/Hashtable;

    const-string/jumbo v7, "Resources"

    invoke-virtual {v5, v6, v7}, Lorg/icepdf/index/core/util/Library;->getResources(Lorg/icepdf/index/core/util/Hashtable;Ljava/lang/String;)Lorg/icepdf/index/core/pobjects/Resources;

    move-result-object v5

    iput-object v5, p0, Lorg/icepdf/index/core/pobjects/PageTree;->resources:Lorg/icepdf/index/core/pobjects/Resources;

    .line 132
    iget-object v5, p0, Lorg/icepdf/index/core/pobjects/PageTree;->library:Lorg/icepdf/index/core/util/Library;

    iget-object v6, p0, Lorg/icepdf/index/core/pobjects/PageTree;->entries:Lorg/icepdf/index/core/util/Hashtable;

    const-string/jumbo v7, "Kids"

    invoke-virtual {v5, v6, v7}, Lorg/icepdf/index/core/util/Library;->getObject(Lorg/icepdf/index/core/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/Vector;

    iput-object v5, p0, Lorg/icepdf/index/core/pobjects/PageTree;->kidsReferences:Ljava/util/Vector;

    .line 133
    new-instance v5, Ljava/util/Vector;

    iget-object v6, p0, Lorg/icepdf/index/core/pobjects/PageTree;->kidsReferences:Ljava/util/Vector;

    invoke-virtual {v6}, Ljava/util/Vector;->size()I

    move-result v6

    invoke-direct {v5, v6}, Ljava/util/Vector;-><init>(I)V

    iput-object v5, p0, Lorg/icepdf/index/core/pobjects/PageTree;->kidsPageAndPages:Ljava/util/Vector;

    .line 134
    iget-object v5, p0, Lorg/icepdf/index/core/pobjects/PageTree;->kidsPageAndPages:Ljava/util/Vector;

    iget-object v6, p0, Lorg/icepdf/index/core/pobjects/PageTree;->kidsReferences:Ljava/util/Vector;

    invoke-virtual {v6}, Ljava/util/Vector;->size()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/util/Vector;->setSize(I)V

    .line 137
    iget-object v5, p0, Lorg/icepdf/index/core/pobjects/PageTree;->library:Lorg/icepdf/index/core/util/Library;

    iget-object v6, p0, Lorg/icepdf/index/core/pobjects/PageTree;->entries:Lorg/icepdf/index/core/util/Hashtable;

    const-string/jumbo v7, "Rotate"

    invoke-virtual {v5, v6, v7}, Lorg/icepdf/index/core/util/Library;->getObject(Lorg/icepdf/index/core/util/Hashtable;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    .line 138
    .local v4, "tmpRotation":Ljava/lang/Object;
    if-eqz v4, :cond_5

    .line 139
    check-cast v4, Ljava/lang/Number;

    .end local v4    # "tmpRotation":Ljava/lang/Object;
    invoke-virtual {v4}, Ljava/lang/Number;->floatValue()F

    move-result v5

    iput v5, p0, Lorg/icepdf/index/core/pobjects/PageTree;->rotationFactor:F

    .line 141
    const/4 v5, 0x1

    iput-boolean v5, p0, Lorg/icepdf/index/core/pobjects/PageTree;->isRotationFactor:Z

    .line 144
    :cond_5
    const/4 v5, 0x1

    iput-boolean v5, p0, Lorg/icepdf/index/core/pobjects/PageTree;->inited:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    .line 105
    .end local v1    # "boxDimensions":Ljava/util/Vector;
    .end local v2    # "n":Ljava/lang/Number;
    .end local v3    # "parentTree":Ljava/lang/Object;
    :catchall_0
    move-exception v5

    monitor-exit p0

    throw v5

    .line 120
    .restart local v2    # "n":Ljava/lang/Number;
    .restart local v3    # "parentTree":Ljava/lang/Object;
    :cond_6
    const/4 v5, 0x0

    :try_start_2
    iput v5, p0, Lorg/icepdf/index/core/pobjects/PageTree;->kidsCount:I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_1
.end method

.method initRootPageTree()V
    .locals 0

    .prologue
    .line 149
    return-void
.end method

.method public releasePage(ILjava/lang/Object;)V
    .locals 2
    .param p1, "pageNumber"    # I
    .param p2, "user"    # Ljava/lang/Object;

    .prologue
    .line 381
    invoke-direct {p0, p1}, Lorg/icepdf/index/core/pobjects/PageTree;->getPagePotentiallyNotInitedByRecursiveIndex(I)Lorg/icepdf/index/core/pobjects/Page;

    move-result-object v0

    .line 384
    .local v0, "page":Lorg/icepdf/index/core/pobjects/Page;
    iget-object v1, p0, Lorg/icepdf/index/core/pobjects/PageTree;->library:Lorg/icepdf/index/core/util/Library;

    iget-object v1, v1, Lorg/icepdf/index/core/util/Library;->memoryManager:Lorg/icepdf/index/core/util/MemoryManager;

    invoke-virtual {v1, p2, v0}, Lorg/icepdf/index/core/util/MemoryManager;->release(Ljava/lang/Object;Lorg/icepdf/index/core/util/MemoryManageable;)V

    .line 385
    return-void
.end method

.method public releasePage(Lorg/icepdf/index/core/pobjects/Page;Ljava/lang/Object;)V
    .locals 1
    .param p1, "page"    # Lorg/icepdf/index/core/pobjects/Page;
    .param p2, "user"    # Ljava/lang/Object;

    .prologue
    .line 359
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/PageTree;->library:Lorg/icepdf/index/core/util/Library;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/PageTree;->library:Lorg/icepdf/index/core/util/Library;

    iget-object v0, v0, Lorg/icepdf/index/core/util/Library;->memoryManager:Lorg/icepdf/index/core/util/MemoryManager;

    if-eqz v0, :cond_0

    .line 360
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/PageTree;->library:Lorg/icepdf/index/core/util/Library;

    iget-object v0, v0, Lorg/icepdf/index/core/util/Library;->memoryManager:Lorg/icepdf/index/core/util/MemoryManager;

    invoke-virtual {v0, p2, p1}, Lorg/icepdf/index/core/util/MemoryManager;->release(Ljava/lang/Object;Lorg/icepdf/index/core/util/MemoryManageable;)V

    .line 361
    :cond_0
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 393
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "PAGES= "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/icepdf/index/core/pobjects/PageTree;->entries:Lorg/icepdf/index/core/util/Hashtable;

    invoke-virtual {v1}, Lorg/icepdf/index/core/util/Hashtable;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
