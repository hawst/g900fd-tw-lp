.class public Lorg/icepdf/index/core/pobjects/PObject;
.super Ljava/lang/Object;
.source "PObject.java"


# instance fields
.field private object:Ljava/lang/Object;

.field private objectReference:Lorg/icepdf/index/core/pobjects/Reference;


# direct methods
.method public constructor <init>(Ljava/lang/Object;Ljava/lang/Number;Ljava/lang/Number;)V
    .locals 1
    .param p1, "object"    # Ljava/lang/Object;
    .param p2, "objectNumber"    # Ljava/lang/Number;
    .param p3, "objectGeneration"    # Ljava/lang/Number;

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/icepdf/index/core/pobjects/PObject;->objectReference:Lorg/icepdf/index/core/pobjects/Reference;

    .line 38
    iput-object p1, p0, Lorg/icepdf/index/core/pobjects/PObject;->object:Ljava/lang/Object;

    .line 39
    new-instance v0, Lorg/icepdf/index/core/pobjects/Reference;

    invoke-direct {v0, p2, p3}, Lorg/icepdf/index/core/pobjects/Reference;-><init>(Ljava/lang/Number;Ljava/lang/Number;)V

    iput-object v0, p0, Lorg/icepdf/index/core/pobjects/PObject;->objectReference:Lorg/icepdf/index/core/pobjects/Reference;

    .line 40
    return-void
.end method

.method public constructor <init>(Ljava/lang/Object;Lorg/icepdf/index/core/pobjects/Reference;)V
    .locals 1
    .param p1, "object"    # Ljava/lang/Object;
    .param p2, "objectReference"    # Lorg/icepdf/index/core/pobjects/Reference;

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/icepdf/index/core/pobjects/PObject;->objectReference:Lorg/icepdf/index/core/pobjects/Reference;

    .line 51
    iput-object p1, p0, Lorg/icepdf/index/core/pobjects/PObject;->object:Ljava/lang/Object;

    .line 52
    iput-object p2, p0, Lorg/icepdf/index/core/pobjects/PObject;->objectReference:Lorg/icepdf/index/core/pobjects/Reference;

    .line 53
    return-void
.end method


# virtual methods
.method public getObject()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/PObject;->object:Ljava/lang/Object;

    return-object v0
.end method

.method public getReference()Lorg/icepdf/index/core/pobjects/Reference;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/PObject;->objectReference:Lorg/icepdf/index/core/pobjects/Reference;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 80
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lorg/icepdf/index/core/pobjects/PObject;->objectReference:Lorg/icepdf/index/core/pobjects/Reference;

    invoke-virtual {v1}, Lorg/icepdf/index/core/pobjects/Reference;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/icepdf/index/core/pobjects/PObject;->object:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
