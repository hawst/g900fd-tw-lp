.class public Lorg/icepdf/index/core/pobjects/security/CryptFilter;
.super Lorg/icepdf/index/core/pobjects/Dictionary;
.source "CryptFilter.java"


# instance fields
.field public cryptFilters:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lorg/icepdf/index/core/pobjects/Name;",
            "Lorg/icepdf/index/core/pobjects/security/CryptFilterEntry;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lorg/icepdf/index/core/util/Library;Lorg/icepdf/index/core/util/Hashtable;)V
    .locals 0
    .param p1, "library"    # Lorg/icepdf/index/core/util/Library;
    .param p2, "entries"    # Lorg/icepdf/index/core/util/Hashtable;

    .prologue
    .line 67
    invoke-direct {p0, p1, p2}, Lorg/icepdf/index/core/pobjects/Dictionary;-><init>(Lorg/icepdf/index/core/util/Library;Lorg/icepdf/index/core/util/Hashtable;)V

    .line 68
    return-void
.end method


# virtual methods
.method public declared-synchronized getCryptFilterByName(Lorg/icepdf/index/core/pobjects/Name;)Lorg/icepdf/index/core/pobjects/security/CryptFilterEntry;
    .locals 9
    .param p1, "cryptFilterName"    # Lorg/icepdf/index/core/pobjects/Name;

    .prologue
    .line 80
    monitor-enter p0

    :try_start_0
    iget-object v4, p0, Lorg/icepdf/index/core/pobjects/security/CryptFilter;->cryptFilters:Ljava/util/HashMap;

    if-nez v4, :cond_0

    .line 81
    new-instance v4, Ljava/util/HashMap;

    const/4 v5, 0x1

    invoke-direct {v4, v5}, Ljava/util/HashMap;-><init>(I)V

    iput-object v4, p0, Lorg/icepdf/index/core/pobjects/security/CryptFilter;->cryptFilters:Ljava/util/HashMap;

    .line 82
    iget-object v4, p0, Lorg/icepdf/index/core/pobjects/security/CryptFilter;->entries:Lorg/icepdf/index/core/util/Hashtable;

    invoke-virtual {v4}, Lorg/icepdf/index/core/util/Hashtable;->keySet()Ljava/util/Set;

    move-result-object v1

    .line 83
    .local v1, "cryptKeys":Ljava/util/Set;
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 84
    .local v3, "name":Ljava/lang/Object;
    iget-object v6, p0, Lorg/icepdf/index/core/pobjects/security/CryptFilter;->cryptFilters:Ljava/util/HashMap;

    move-object v0, v3

    check-cast v0, Lorg/icepdf/index/core/pobjects/Name;

    move-object v4, v0

    new-instance v7, Lorg/icepdf/index/core/pobjects/security/CryptFilterEntry;

    iget-object v8, p0, Lorg/icepdf/index/core/pobjects/security/CryptFilter;->library:Lorg/icepdf/index/core/util/Library;

    iget-object v5, p0, Lorg/icepdf/index/core/pobjects/security/CryptFilter;->entries:Lorg/icepdf/index/core/util/Hashtable;

    invoke-virtual {v5, v3}, Lorg/icepdf/index/core/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/icepdf/index/core/util/Hashtable;

    invoke-direct {v7, v8, v5}, Lorg/icepdf/index/core/pobjects/security/CryptFilterEntry;-><init>(Lorg/icepdf/index/core/util/Library;Lorg/icepdf/index/core/util/Hashtable;)V

    invoke-virtual {v6, v4, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 80
    .end local v1    # "cryptKeys":Ljava/util/Set;
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v3    # "name":Ljava/lang/Object;
    :catchall_0
    move-exception v4

    monitor-exit p0

    throw v4

    .line 88
    :cond_0
    :try_start_1
    iget-object v4, p0, Lorg/icepdf/index/core/pobjects/security/CryptFilter;->cryptFilters:Ljava/util/HashMap;

    invoke-virtual {v4, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/icepdf/index/core/pobjects/security/CryptFilterEntry;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-object v4
.end method
