.class public Lorg/icepdf/index/core/pobjects/graphics/text/PageText;
.super Ljava/lang/Object;
.source "PageText.java"


# instance fields
.field private currentLine:Lorg/icepdf/index/core/pobjects/graphics/text/LineText;

.field private pageLines:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lorg/icepdf/index/core/pobjects/graphics/text/LineText;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0x32

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lorg/icepdf/index/core/pobjects/graphics/text/PageText;->pageLines:Ljava/util/ArrayList;

    .line 51
    return-void
.end method


# virtual methods
.method public addGlyph(Lorg/icepdf/index/core/pobjects/graphics/text/GlyphText;)V
    .locals 1
    .param p1, "sprite"    # Lorg/icepdf/index/core/pobjects/graphics/text/GlyphText;

    .prologue
    .line 64
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/graphics/text/PageText;->currentLine:Lorg/icepdf/index/core/pobjects/graphics/text/LineText;

    if-nez v0, :cond_0

    .line 65
    invoke-virtual {p0}, Lorg/icepdf/index/core/pobjects/graphics/text/PageText;->newLine()V

    .line 67
    :cond_0
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/graphics/text/PageText;->currentLine:Lorg/icepdf/index/core/pobjects/graphics/text/LineText;

    invoke-virtual {v0, p1}, Lorg/icepdf/index/core/pobjects/graphics/text/LineText;->addText(Lorg/icepdf/index/core/pobjects/graphics/text/GlyphText;)V

    .line 68
    return-void
.end method

.method public applyXObjectTransform(Lorg/icepdf/index/java/awt/geom/AffineTransform;)V
    .locals 7
    .param p1, "transform"    # Lorg/icepdf/index/java/awt/geom/AffineTransform;

    .prologue
    .line 81
    iget-object v6, p0, Lorg/icepdf/index/core/pobjects/graphics/text/PageText;->pageLines:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/icepdf/index/core/pobjects/graphics/text/LineText;

    .line 82
    .local v4, "lineText":Lorg/icepdf/index/core/pobjects/graphics/text/LineText;
    invoke-virtual {v4}, Lorg/icepdf/index/core/pobjects/graphics/text/LineText;->clearBounds()V

    .line 83
    invoke-virtual {v4}, Lorg/icepdf/index/core/pobjects/graphics/text/LineText;->getWords()Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/icepdf/index/core/pobjects/graphics/text/WordText;

    .line 84
    .local v5, "wordText":Lorg/icepdf/index/core/pobjects/graphics/text/WordText;
    invoke-virtual {v5}, Lorg/icepdf/index/core/pobjects/graphics/text/WordText;->clearBounds()V

    .line 85
    invoke-virtual {v5}, Lorg/icepdf/index/core/pobjects/graphics/text/WordText;->getGlyphs()Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/icepdf/index/core/pobjects/graphics/text/GlyphText;

    .line 86
    .local v0, "glyph":Lorg/icepdf/index/core/pobjects/graphics/text/GlyphText;
    invoke-virtual {v0, p1}, Lorg/icepdf/index/core/pobjects/graphics/text/GlyphText;->normalizeToUserSpace(Lorg/icepdf/index/java/awt/geom/AffineTransform;)V

    goto :goto_0

    .line 90
    .end local v0    # "glyph":Lorg/icepdf/index/core/pobjects/graphics/text/GlyphText;
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v4    # "lineText":Lorg/icepdf/index/core/pobjects/graphics/text/LineText;
    .end local v5    # "wordText":Lorg/icepdf/index/core/pobjects/graphics/text/WordText;
    :cond_2
    return-void
.end method

.method public dispose()V
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/graphics/text/PageText;->pageLines:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 96
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/graphics/text/PageText;->pageLines:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 97
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/graphics/text/PageText;->pageLines:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->trimToSize()V

    .line 99
    :cond_0
    return-void
.end method

.method public getPageLines()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lorg/icepdf/index/core/pobjects/graphics/text/LineText;",
            ">;"
        }
    .end annotation

    .prologue
    .line 71
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/graphics/text/PageText;->pageLines:Ljava/util/ArrayList;

    return-object v0
.end method

.method public newLine()V
    .locals 2

    .prologue
    .line 55
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/graphics/text/PageText;->currentLine:Lorg/icepdf/index/core/pobjects/graphics/text/LineText;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/graphics/text/PageText;->currentLine:Lorg/icepdf/index/core/pobjects/graphics/text/LineText;

    invoke-virtual {v0}, Lorg/icepdf/index/core/pobjects/graphics/text/LineText;->getWords()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 61
    :goto_0
    return-void

    .line 59
    :cond_0
    new-instance v0, Lorg/icepdf/index/core/pobjects/graphics/text/LineText;

    invoke-direct {v0}, Lorg/icepdf/index/core/pobjects/graphics/text/LineText;-><init>()V

    iput-object v0, p0, Lorg/icepdf/index/core/pobjects/graphics/text/PageText;->currentLine:Lorg/icepdf/index/core/pobjects/graphics/text/LineText;

    .line 60
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/graphics/text/PageText;->pageLines:Ljava/util/ArrayList;

    iget-object v1, p0, Lorg/icepdf/index/core/pobjects/graphics/text/PageText;->currentLine:Lorg/icepdf/index/core/pobjects/graphics/text/LineText;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 102
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 103
    .local v0, "extractedText":Ljava/lang/StringBuilder;
    iget-object v5, p0, Lorg/icepdf/index/core/pobjects/graphics/text/PageText;->pageLines:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/icepdf/index/core/pobjects/graphics/text/LineText;

    .line 105
    .local v3, "lineText":Lorg/icepdf/index/core/pobjects/graphics/text/LineText;
    invoke-virtual {v3}, Lorg/icepdf/index/core/pobjects/graphics/text/LineText;->getWords()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/icepdf/index/core/pobjects/graphics/text/WordText;

    .line 106
    .local v4, "wordText":Lorg/icepdf/index/core/pobjects/graphics/text/WordText;
    invoke-virtual {v4}, Lorg/icepdf/index/core/pobjects/graphics/text/WordText;->getText()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 108
    .end local v4    # "wordText":Lorg/icepdf/index/core/pobjects/graphics/text/WordText;
    :cond_0
    const/16 v5, 0xa

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 110
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v3    # "lineText":Lorg/icepdf/index/core/pobjects/graphics/text/LineText;
    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    return-object v5
.end method
