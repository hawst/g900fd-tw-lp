.class public Lorg/icepdf/index/core/pobjects/graphics/text/LineText;
.super Lorg/icepdf/index/core/pobjects/graphics/text/AbstractText;
.source "LineText.java"


# instance fields
.field private currentWord:Lorg/icepdf/index/core/pobjects/graphics/text/WordText;

.field private words:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lorg/icepdf/index/core/pobjects/graphics/text/WordText;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 34
    invoke-direct {p0}, Lorg/icepdf/index/core/pobjects/graphics/text/AbstractText;-><init>()V

    .line 35
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0x10

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lorg/icepdf/index/core/pobjects/graphics/text/LineText;->words:Ljava/util/ArrayList;

    .line 36
    return-void
.end method

.method private addWord(Lorg/icepdf/index/core/pobjects/graphics/text/WordText;)V
    .locals 1
    .param p1, "wordText"    # Lorg/icepdf/index/core/pobjects/graphics/text/WordText;

    .prologue
    .line 114
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/graphics/text/LineText;->words:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 117
    iput-object p1, p0, Lorg/icepdf/index/core/pobjects/graphics/text/LineText;->currentWord:Lorg/icepdf/index/core/pobjects/graphics/text/WordText;

    .line 119
    return-void
.end method

.method private getCurrentWord()Lorg/icepdf/index/core/pobjects/graphics/text/WordText;
    .locals 2

    .prologue
    .line 127
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/graphics/text/LineText;->currentWord:Lorg/icepdf/index/core/pobjects/graphics/text/WordText;

    if-nez v0, :cond_0

    .line 128
    new-instance v0, Lorg/icepdf/index/core/pobjects/graphics/text/WordText;

    invoke-direct {v0}, Lorg/icepdf/index/core/pobjects/graphics/text/WordText;-><init>()V

    iput-object v0, p0, Lorg/icepdf/index/core/pobjects/graphics/text/LineText;->currentWord:Lorg/icepdf/index/core/pobjects/graphics/text/WordText;

    .line 129
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/graphics/text/LineText;->words:Ljava/util/ArrayList;

    iget-object v1, p0, Lorg/icepdf/index/core/pobjects/graphics/text/LineText;->currentWord:Lorg/icepdf/index/core/pobjects/graphics/text/WordText;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 131
    :cond_0
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/graphics/text/LineText;->currentWord:Lorg/icepdf/index/core/pobjects/graphics/text/WordText;

    return-object v0
.end method


# virtual methods
.method protected addText(Lorg/icepdf/index/core/pobjects/graphics/text/GlyphText;)V
    .locals 5
    .param p1, "sprite"    # Lorg/icepdf/index/core/pobjects/graphics/text/GlyphText;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 67
    invoke-static {p1}, Lorg/icepdf/index/core/pobjects/graphics/text/WordText;->detectWhiteSpace(Lorg/icepdf/index/core/pobjects/graphics/text/GlyphText;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 69
    new-instance v0, Lorg/icepdf/index/core/pobjects/graphics/text/WordText;

    invoke-direct {v0}, Lorg/icepdf/index/core/pobjects/graphics/text/WordText;-><init>()V

    .line 70
    .local v0, "newWord":Lorg/icepdf/index/core/pobjects/graphics/text/WordText;
    invoke-virtual {v0, v3}, Lorg/icepdf/index/core/pobjects/graphics/text/WordText;->setWhiteSpace(Z)V

    .line 71
    invoke-virtual {v0, p1}, Lorg/icepdf/index/core/pobjects/graphics/text/WordText;->addText(Lorg/icepdf/index/core/pobjects/graphics/text/GlyphText;)V

    .line 72
    invoke-direct {p0, v0}, Lorg/icepdf/index/core/pobjects/graphics/text/LineText;->addWord(Lorg/icepdf/index/core/pobjects/graphics/text/WordText;)V

    .line 74
    iput-object v4, p0, Lorg/icepdf/index/core/pobjects/graphics/text/LineText;->currentWord:Lorg/icepdf/index/core/pobjects/graphics/text/WordText;

    .line 103
    .end local v0    # "newWord":Lorg/icepdf/index/core/pobjects/graphics/text/WordText;
    :goto_0
    return-void

    .line 77
    :cond_0
    invoke-static {p1}, Lorg/icepdf/index/core/pobjects/graphics/text/WordText;->detectPunctuation(Lorg/icepdf/index/core/pobjects/graphics/text/GlyphText;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 79
    new-instance v0, Lorg/icepdf/index/core/pobjects/graphics/text/WordText;

    invoke-direct {v0}, Lorg/icepdf/index/core/pobjects/graphics/text/WordText;-><init>()V

    .line 80
    .restart local v0    # "newWord":Lorg/icepdf/index/core/pobjects/graphics/text/WordText;
    invoke-virtual {v0, v3}, Lorg/icepdf/index/core/pobjects/graphics/text/WordText;->setWhiteSpace(Z)V

    .line 81
    invoke-virtual {v0, p1}, Lorg/icepdf/index/core/pobjects/graphics/text/WordText;->addText(Lorg/icepdf/index/core/pobjects/graphics/text/GlyphText;)V

    .line 82
    invoke-direct {p0, v0}, Lorg/icepdf/index/core/pobjects/graphics/text/LineText;->addWord(Lorg/icepdf/index/core/pobjects/graphics/text/WordText;)V

    .line 84
    iput-object v4, p0, Lorg/icepdf/index/core/pobjects/graphics/text/LineText;->currentWord:Lorg/icepdf/index/core/pobjects/graphics/text/WordText;

    goto :goto_0

    .line 88
    .end local v0    # "newWord":Lorg/icepdf/index/core/pobjects/graphics/text/WordText;
    :cond_1
    invoke-direct {p0}, Lorg/icepdf/index/core/pobjects/graphics/text/LineText;->getCurrentWord()Lorg/icepdf/index/core/pobjects/graphics/text/WordText;

    move-result-object v2

    invoke-virtual {v2, p1}, Lorg/icepdf/index/core/pobjects/graphics/text/WordText;->detectSpace(Lorg/icepdf/index/core/pobjects/graphics/text/GlyphText;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 90
    iget-object v2, p0, Lorg/icepdf/index/core/pobjects/graphics/text/LineText;->currentWord:Lorg/icepdf/index/core/pobjects/graphics/text/WordText;

    invoke-virtual {v2, p1}, Lorg/icepdf/index/core/pobjects/graphics/text/WordText;->buildSpaceWord(Lorg/icepdf/index/core/pobjects/graphics/text/GlyphText;)Lorg/icepdf/index/core/pobjects/graphics/text/WordText;

    move-result-object v1

    .line 91
    .local v1, "spaceWord":Lorg/icepdf/index/core/pobjects/graphics/text/WordText;
    invoke-virtual {v1, v3}, Lorg/icepdf/index/core/pobjects/graphics/text/WordText;->setWhiteSpace(Z)V

    .line 93
    invoke-direct {p0, v1}, Lorg/icepdf/index/core/pobjects/graphics/text/LineText;->addWord(Lorg/icepdf/index/core/pobjects/graphics/text/WordText;)V

    .line 95
    iput-object v4, p0, Lorg/icepdf/index/core/pobjects/graphics/text/LineText;->currentWord:Lorg/icepdf/index/core/pobjects/graphics/text/WordText;

    .line 97
    invoke-virtual {p0, p1}, Lorg/icepdf/index/core/pobjects/graphics/text/LineText;->addText(Lorg/icepdf/index/core/pobjects/graphics/text/GlyphText;)V

    goto :goto_0

    .line 101
    .end local v1    # "spaceWord":Lorg/icepdf/index/core/pobjects/graphics/text/WordText;
    :cond_2
    invoke-direct {p0}, Lorg/icepdf/index/core/pobjects/graphics/text/LineText;->getCurrentWord()Lorg/icepdf/index/core/pobjects/graphics/text/WordText;

    move-result-object v2

    invoke-virtual {v2, p1}, Lorg/icepdf/index/core/pobjects/graphics/text/WordText;->addText(Lorg/icepdf/index/core/pobjects/graphics/text/GlyphText;)V

    goto :goto_0
.end method

.method public bridge synthetic clearBounds()V
    .locals 0

    .prologue
    .line 28
    invoke-super {p0}, Lorg/icepdf/index/core/pobjects/graphics/text/AbstractText;->clearBounds()V

    return-void
.end method

.method public getBounds()Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;
    .locals 4

    .prologue
    .line 40
    iget-object v2, p0, Lorg/icepdf/index/core/pobjects/graphics/text/LineText;->bounds:Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;

    if-nez v2, :cond_2

    .line 42
    iget-object v2, p0, Lorg/icepdf/index/core/pobjects/graphics/text/LineText;->words:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/icepdf/index/core/pobjects/graphics/text/WordText;

    .line 43
    .local v1, "word":Lorg/icepdf/index/core/pobjects/graphics/text/WordText;
    iget-object v2, p0, Lorg/icepdf/index/core/pobjects/graphics/text/LineText;->bounds:Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;

    if-nez v2, :cond_0

    .line 44
    new-instance v2, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;

    invoke-direct {v2}, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;-><init>()V

    iput-object v2, p0, Lorg/icepdf/index/core/pobjects/graphics/text/LineText;->bounds:Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;

    .line 45
    iget-object v2, p0, Lorg/icepdf/index/core/pobjects/graphics/text/LineText;->bounds:Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;

    invoke-virtual {v1}, Lorg/icepdf/index/core/pobjects/graphics/text/WordText;->getBounds()Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;

    move-result-object v3

    invoke-virtual {v2, v3}, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;->setRect(Lorg/icepdf/index/java/awt/geom/Rectangle2D;)V

    goto :goto_0

    .line 47
    :cond_0
    iget-object v2, p0, Lorg/icepdf/index/core/pobjects/graphics/text/LineText;->bounds:Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;

    invoke-virtual {v1}, Lorg/icepdf/index/core/pobjects/graphics/text/WordText;->getBounds()Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;

    move-result-object v3

    invoke-virtual {v2, v3}, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;->add(Lorg/icepdf/index/java/awt/geom/Rectangle2D;)V

    goto :goto_0

    .line 51
    .end local v1    # "word":Lorg/icepdf/index/core/pobjects/graphics/text/WordText;
    :cond_1
    iget-object v2, p0, Lorg/icepdf/index/core/pobjects/graphics/text/LineText;->bounds:Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;

    if-nez v2, :cond_2

    .line 52
    new-instance v2, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;

    invoke-direct {v2}, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;-><init>()V

    iput-object v2, p0, Lorg/icepdf/index/core/pobjects/graphics/text/LineText;->bounds:Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;

    .line 55
    .end local v0    # "i$":Ljava/util/Iterator;
    :cond_2
    iget-object v2, p0, Lorg/icepdf/index/core/pobjects/graphics/text/LineText;->bounds:Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;

    return-object v2
.end method

.method public getWords()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lorg/icepdf/index/core/pobjects/graphics/text/WordText;",
            ">;"
        }
    .end annotation

    .prologue
    .line 141
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/graphics/text/LineText;->words:Ljava/util/ArrayList;

    return-object v0
.end method

.method public bridge synthetic setHasSelected(Z)V
    .locals 0
    .param p1, "x0"    # Z

    .prologue
    .line 28
    invoke-super {p0, p1}, Lorg/icepdf/index/core/pobjects/graphics/text/AbstractText;->setHasSelected(Z)V

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 167
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/graphics/text/LineText;->words:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
