.class public Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;
.super Ljava/lang/Object;
.source "GraphicsState.java"


# static fields
.field private static enabledOverpaint:Z


# instance fields
.field private CTM:Landroid/graphics/Matrix;

.field private clip:Landroid/graphics/Rect;

.field private dashArray:[F

.field private dashPhase:F

.field private fillAlpha:F

.field private fillColor:I

.field private lineCap:Landroid/graphics/Paint$Cap;

.field private lineJoin:Landroid/graphics/Paint$Join;

.field private lineWidth:F

.field private miterLimit:F

.field private overprintMode:I

.field private overprintOther:Z

.field private overprintStroking:Z

.field private parentGraphicState:Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;

.field private shapes:Lorg/icepdf/index/core/pobjects/graphics/Shapes;

.field private strokeAlpha:F

.field private strokeColor:I

.field private textState:Lorg/icepdf/index/core/pobjects/graphics/TextState;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 214
    const-string/jumbo v0, "org.icepdf.core.overpaint"

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lorg/icepdf/index/core/util/Defs;->sysPropertyBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->enabledOverpaint:Z

    .line 217
    return-void
.end method

.method public constructor <init>(Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;)V
    .locals 4
    .param p1, "parentGraphicsState"    # Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;

    .prologue
    const/4 v3, 0x0

    const/high16 v2, -0x1000000

    const/high16 v1, 0x3f800000    # 1.0f

    .line 294
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 221
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->CTM:Landroid/graphics/Matrix;

    .line 224
    sget-object v0, Landroid/graphics/Paint$Cap;->BUTT:Landroid/graphics/Paint$Cap;

    iput-object v0, p0, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->lineCap:Landroid/graphics/Paint$Cap;

    .line 227
    iput v1, p0, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->lineWidth:F

    .line 230
    sget-object v0, Landroid/graphics/Paint$Join;->MITER:Landroid/graphics/Paint$Join;

    iput-object v0, p0, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->lineJoin:Landroid/graphics/Paint$Join;

    .line 233
    const/high16 v0, 0x41200000    # 10.0f

    iput v0, p0, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->miterLimit:F

    .line 236
    iput-object v3, p0, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->dashArray:[F

    .line 239
    const/4 v0, 0x0

    iput v0, p0, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->dashPhase:F

    .line 242
    iput v2, p0, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->fillColor:I

    .line 245
    iput v2, p0, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->strokeColor:I

    .line 248
    iput v1, p0, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->strokeAlpha:F

    .line 251
    iput v1, p0, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->fillAlpha:F

    .line 260
    new-instance v0, Lorg/icepdf/index/core/pobjects/graphics/TextState;

    invoke-direct {v0}, Lorg/icepdf/index/core/pobjects/graphics/TextState;-><init>()V

    iput-object v0, p0, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->textState:Lorg/icepdf/index/core/pobjects/graphics/TextState;

    .line 263
    iput-object v3, p0, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->parentGraphicState:Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;

    .line 298
    new-instance v0, Landroid/graphics/Matrix;

    iget-object v1, p1, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->CTM:Landroid/graphics/Matrix;

    invoke-direct {v0, v1}, Landroid/graphics/Matrix;-><init>(Landroid/graphics/Matrix;)V

    iput-object v0, p0, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->CTM:Landroid/graphics/Matrix;

    .line 300
    iget-object v0, p1, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->lineCap:Landroid/graphics/Paint$Cap;

    iput-object v0, p0, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->lineCap:Landroid/graphics/Paint$Cap;

    .line 301
    iget v0, p1, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->lineWidth:F

    iput v0, p0, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->lineWidth:F

    .line 302
    iget v0, p1, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->miterLimit:F

    iput v0, p0, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->miterLimit:F

    .line 303
    iget-object v0, p1, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->lineJoin:Landroid/graphics/Paint$Join;

    iput-object v0, p0, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->lineJoin:Landroid/graphics/Paint$Join;

    .line 305
    iget v0, p1, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->fillColor:I

    iput v0, p0, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->fillColor:I

    .line 307
    iget v0, p1, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->strokeColor:I

    iput v0, p0, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->strokeColor:I

    .line 308
    iget-object v0, p1, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->shapes:Lorg/icepdf/index/core/pobjects/graphics/Shapes;

    iput-object v0, p0, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->shapes:Lorg/icepdf/index/core/pobjects/graphics/Shapes;

    .line 309
    iget-object v0, p1, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->clip:Landroid/graphics/Rect;

    if-eqz v0, :cond_0

    .line 310
    new-instance v0, Landroid/graphics/Rect;

    iget-object v1, p1, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->clip:Landroid/graphics/Rect;

    invoke-direct {v0, v1}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    iput-object v0, p0, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->clip:Landroid/graphics/Rect;

    .line 315
    :cond_0
    new-instance v0, Lorg/icepdf/index/core/pobjects/graphics/TextState;

    iget-object v1, p1, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->textState:Lorg/icepdf/index/core/pobjects/graphics/TextState;

    invoke-direct {v0, v1}, Lorg/icepdf/index/core/pobjects/graphics/TextState;-><init>(Lorg/icepdf/index/core/pobjects/graphics/TextState;)V

    iput-object v0, p0, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->textState:Lorg/icepdf/index/core/pobjects/graphics/TextState;

    .line 316
    invoke-virtual {p1}, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->getDashPhase()F

    move-result v0

    iput v0, p0, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->dashPhase:F

    .line 317
    invoke-virtual {p1}, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->getDashArray()[F

    move-result-object v0

    iput-object v0, p0, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->dashArray:[F

    .line 320
    invoke-virtual {p1}, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->getOverprintMode()I

    move-result v0

    iput v0, p0, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->overprintMode:I

    .line 321
    invoke-virtual {p1}, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->isOverprintOther()Z

    move-result v0

    iput-boolean v0, p0, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->overprintOther:Z

    .line 322
    invoke-virtual {p1}, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->isOverprintStroking()Z

    move-result v0

    iput-boolean v0, p0, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->overprintStroking:Z

    .line 324
    return-void
.end method

.method public constructor <init>(Lorg/icepdf/index/core/pobjects/graphics/Shapes;)V
    .locals 4
    .param p1, "shapes"    # Lorg/icepdf/index/core/pobjects/graphics/Shapes;

    .prologue
    const/4 v3, 0x0

    const/high16 v2, -0x1000000

    const/high16 v1, 0x3f800000    # 1.0f

    .line 284
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 221
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->CTM:Landroid/graphics/Matrix;

    .line 224
    sget-object v0, Landroid/graphics/Paint$Cap;->BUTT:Landroid/graphics/Paint$Cap;

    iput-object v0, p0, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->lineCap:Landroid/graphics/Paint$Cap;

    .line 227
    iput v1, p0, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->lineWidth:F

    .line 230
    sget-object v0, Landroid/graphics/Paint$Join;->MITER:Landroid/graphics/Paint$Join;

    iput-object v0, p0, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->lineJoin:Landroid/graphics/Paint$Join;

    .line 233
    const/high16 v0, 0x41200000    # 10.0f

    iput v0, p0, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->miterLimit:F

    .line 236
    iput-object v3, p0, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->dashArray:[F

    .line 239
    const/4 v0, 0x0

    iput v0, p0, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->dashPhase:F

    .line 242
    iput v2, p0, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->fillColor:I

    .line 245
    iput v2, p0, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->strokeColor:I

    .line 248
    iput v1, p0, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->strokeAlpha:F

    .line 251
    iput v1, p0, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->fillAlpha:F

    .line 260
    new-instance v0, Lorg/icepdf/index/core/pobjects/graphics/TextState;

    invoke-direct {v0}, Lorg/icepdf/index/core/pobjects/graphics/TextState;-><init>()V

    iput-object v0, p0, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->textState:Lorg/icepdf/index/core/pobjects/graphics/TextState;

    .line 263
    iput-object v3, p0, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->parentGraphicState:Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;

    .line 285
    iput-object p1, p0, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->shapes:Lorg/icepdf/index/core/pobjects/graphics/Shapes;

    .line 286
    return-void
.end method


# virtual methods
.method public getCTM()Landroid/graphics/Matrix;
    .locals 1

    .prologue
    .line 565
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->CTM:Landroid/graphics/Matrix;

    return-object v0
.end method

.method public getClip()Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 561
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->clip:Landroid/graphics/Rect;

    return-object v0
.end method

.method public getDashArray()[F
    .locals 1

    .prologue
    .line 640
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->dashArray:[F

    return-object v0
.end method

.method public getDashPhase()F
    .locals 1

    .prologue
    .line 648
    iget v0, p0, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->dashPhase:F

    return v0
.end method

.method public getFillAlpha()F
    .locals 1

    .prologue
    .line 688
    iget v0, p0, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->fillAlpha:F

    return v0
.end method

.method public getFillColor()I
    .locals 1

    .prologue
    .line 664
    iget v0, p0, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->fillColor:I

    return v0
.end method

.method public getLineCap()Landroid/graphics/Paint$Cap;
    .locals 1

    .prologue
    .line 573
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->lineCap:Landroid/graphics/Paint$Cap;

    return-object v0
.end method

.method public getLineJoin()Landroid/graphics/Paint$Join;
    .locals 1

    .prologue
    .line 614
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->lineJoin:Landroid/graphics/Paint$Join;

    return-object v0
.end method

.method public getLineWidth()F
    .locals 1

    .prologue
    .line 598
    iget v0, p0, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->lineWidth:F

    return v0
.end method

.method public getMiterLimit()F
    .locals 1

    .prologue
    .line 656
    iget v0, p0, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->miterLimit:F

    return v0
.end method

.method public getOverprintMode()I
    .locals 1

    .prologue
    .line 705
    iget v0, p0, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->overprintMode:I

    return v0
.end method

.method public getStrokeAlpha()F
    .locals 1

    .prologue
    .line 680
    iget v0, p0, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->strokeAlpha:F

    return v0
.end method

.method public getStrokeColor()I
    .locals 1

    .prologue
    .line 672
    iget v0, p0, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->strokeColor:I

    return v0
.end method

.method public getTextState()Lorg/icepdf/index/core/pobjects/graphics/TextState;
    .locals 1

    .prologue
    .line 697
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->textState:Lorg/icepdf/index/core/pobjects/graphics/TextState;

    return-object v0
.end method

.method public isOverprintOther()Z
    .locals 1

    .prologue
    .line 713
    iget-boolean v0, p0, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->overprintOther:Z

    return v0
.end method

.method public isOverprintStroking()Z
    .locals 1

    .prologue
    .line 709
    iget-boolean v0, p0, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->overprintStroking:Z

    return v0
.end method

.method public restore()Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;
    .locals 3

    .prologue
    .line 475
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->parentGraphicState:Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;

    if-eqz v0, :cond_1

    .line 477
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->parentGraphicState:Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;

    iget-object v1, p0, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->parentGraphicState:Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;

    iget-object v1, v1, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->CTM:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->set(Landroid/graphics/Matrix;)V

    .line 479
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->parentGraphicState:Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->clip:Landroid/graphics/Rect;

    if-eqz v0, :cond_0

    .line 480
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->parentGraphicState:Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->shapes:Lorg/icepdf/index/core/pobjects/graphics/Shapes;

    new-instance v1, Landroid/graphics/Rect;

    iget-object v2, p0, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->parentGraphicState:Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;

    iget-object v2, v2, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->clip:Landroid/graphics/Rect;

    invoke-direct {v1, v2}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    invoke-virtual {v0, v1}, Lorg/icepdf/index/core/pobjects/graphics/Shapes;->add(Ljava/lang/Object;)V

    .line 499
    :cond_0
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->parentGraphicState:Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;

    iget-object v0, v0, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->shapes:Lorg/icepdf/index/core/pobjects/graphics/Shapes;

    iget-object v1, p0, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->parentGraphicState:Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;

    invoke-virtual {v1}, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->getFillColor()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/icepdf/index/core/pobjects/graphics/Shapes;->add(Ljava/lang/Object;)V

    .line 506
    :cond_1
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->parentGraphicState:Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;

    return-object v0
.end method

.method public save()Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;
    .locals 1

    .prologue
    .line 383
    new-instance v0, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;

    invoke-direct {v0, p0}, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;-><init>(Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;)V

    .line 384
    .local v0, "gs":Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;
    iput-object p0, v0, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->parentGraphicState:Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;

    .line 385
    return-object v0
.end method

.method public scale(FF)V
    .locals 3
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    .line 361
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->CTM:Landroid/graphics/Matrix;

    invoke-virtual {v0, p1, p2}, Landroid/graphics/Matrix;->preScale(FF)Z

    .line 362
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->shapes:Lorg/icepdf/index/core/pobjects/graphics/Shapes;

    new-instance v1, Landroid/graphics/Matrix;

    iget-object v2, p0, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->CTM:Landroid/graphics/Matrix;

    invoke-direct {v1, v2}, Landroid/graphics/Matrix;-><init>(Landroid/graphics/Matrix;)V

    invoke-virtual {v0, v1}, Lorg/icepdf/index/core/pobjects/graphics/Shapes;->add(Ljava/lang/Object;)V

    .line 363
    return-void
.end method

.method public set(Landroid/graphics/Matrix;)V
    .locals 3
    .param p1, "af"    # Landroid/graphics/Matrix;

    .prologue
    .line 372
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0, p1}, Landroid/graphics/Matrix;-><init>(Landroid/graphics/Matrix;)V

    iput-object v0, p0, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->CTM:Landroid/graphics/Matrix;

    .line 373
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->shapes:Lorg/icepdf/index/core/pobjects/graphics/Shapes;

    new-instance v1, Landroid/graphics/Matrix;

    iget-object v2, p0, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->CTM:Landroid/graphics/Matrix;

    invoke-direct {v1, v2}, Landroid/graphics/Matrix;-><init>(Landroid/graphics/Matrix;)V

    invoke-virtual {v0, v1}, Lorg/icepdf/index/core/pobjects/graphics/Shapes;->add(Ljava/lang/Object;)V

    .line 374
    return-void
.end method

.method public setCTM(Landroid/graphics/Matrix;)V
    .locals 0
    .param p1, "ctm"    # Landroid/graphics/Matrix;

    .prologue
    .line 569
    iput-object p1, p0, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->CTM:Landroid/graphics/Matrix;

    .line 570
    return-void
.end method

.method public setClip(Landroid/graphics/Rect;)V
    .locals 3
    .param p1, "newClip"    # Landroid/graphics/Rect;

    .prologue
    const/4 v1, 0x0

    .line 541
    if-eqz p1, :cond_1

    .line 543
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->clip:Landroid/graphics/Rect;

    if-eqz v0, :cond_0

    .line 544
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->clip:Landroid/graphics/Rect;

    invoke-virtual {p1, v0}, Landroid/graphics/Rect;->intersect(Landroid/graphics/Rect;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 545
    new-instance p1, Landroid/graphics/Rect;

    .end local p1    # "newClip":Landroid/graphics/Rect;
    invoke-direct {p1, v1, v1, v1, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 548
    .restart local p1    # "newClip":Landroid/graphics/Rect;
    :cond_0
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0, p1}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    iput-object v0, p0, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->clip:Landroid/graphics/Rect;

    .line 551
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->shapes:Lorg/icepdf/index/core/pobjects/graphics/Shapes;

    new-instance v1, Landroid/graphics/Rect;

    iget-object v2, p0, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->clip:Landroid/graphics/Rect;

    invoke-direct {v1, v2}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    invoke-virtual {v0, v1}, Lorg/icepdf/index/core/pobjects/graphics/Shapes;->add(Ljava/lang/Object;)V

    .line 558
    :goto_0
    return-void

    .line 555
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->clip:Landroid/graphics/Rect;

    goto :goto_0
.end method

.method public setDashArray([F)V
    .locals 0
    .param p1, "dashArray"    # [F

    .prologue
    .line 644
    iput-object p1, p0, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->dashArray:[F

    .line 645
    return-void
.end method

.method public setDashPhase(F)V
    .locals 0
    .param p1, "dashPhase"    # F

    .prologue
    .line 652
    iput p1, p0, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->dashPhase:F

    .line 653
    return-void
.end method

.method public setFillAlpha(F)V
    .locals 0
    .param p1, "alpha"    # F

    .prologue
    .line 684
    iput p1, p0, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->fillAlpha:F

    .line 685
    return-void
.end method

.method public setFillColor(I)V
    .locals 0
    .param p1, "fillColor"    # I

    .prologue
    .line 668
    iput p1, p0, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->fillColor:I

    .line 669
    return-void
.end method

.method public setLineCap(I)V
    .locals 1
    .param p1, "lineCapNum"    # I

    .prologue
    .line 577
    packed-switch p1, :pswitch_data_0

    .line 591
    :goto_0
    return-void

    .line 579
    :pswitch_0
    sget-object v0, Landroid/graphics/Paint$Cap;->BUTT:Landroid/graphics/Paint$Cap;

    iput-object v0, p0, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->lineCap:Landroid/graphics/Paint$Cap;

    goto :goto_0

    .line 583
    :pswitch_1
    sget-object v0, Landroid/graphics/Paint$Cap;->ROUND:Landroid/graphics/Paint$Cap;

    iput-object v0, p0, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->lineCap:Landroid/graphics/Paint$Cap;

    goto :goto_0

    .line 587
    :pswitch_2
    sget-object v0, Landroid/graphics/Paint$Cap;->SQUARE:Landroid/graphics/Paint$Cap;

    iput-object v0, p0, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->lineCap:Landroid/graphics/Paint$Cap;

    goto :goto_0

    .line 577
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public setLineCap(Landroid/graphics/Paint$Cap;)V
    .locals 0
    .param p1, "lineCap"    # Landroid/graphics/Paint$Cap;

    .prologue
    .line 594
    iput-object p1, p0, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->lineCap:Landroid/graphics/Paint$Cap;

    .line 595
    return-void
.end method

.method public setLineJoin(I)V
    .locals 1
    .param p1, "lineJoinNum"    # I

    .prologue
    .line 622
    packed-switch p1, :pswitch_data_0

    .line 636
    :goto_0
    return-void

    .line 624
    :pswitch_0
    sget-object v0, Landroid/graphics/Paint$Join;->MITER:Landroid/graphics/Paint$Join;

    iput-object v0, p0, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->lineJoin:Landroid/graphics/Paint$Join;

    goto :goto_0

    .line 628
    :pswitch_1
    sget-object v0, Landroid/graphics/Paint$Join;->ROUND:Landroid/graphics/Paint$Join;

    iput-object v0, p0, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->lineJoin:Landroid/graphics/Paint$Join;

    goto :goto_0

    .line 632
    :pswitch_2
    sget-object v0, Landroid/graphics/Paint$Join;->BEVEL:Landroid/graphics/Paint$Join;

    iput-object v0, p0, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->lineJoin:Landroid/graphics/Paint$Join;

    goto :goto_0

    .line 622
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public setLineJoin(Landroid/graphics/Paint$Join;)V
    .locals 0
    .param p1, "lineJoin"    # Landroid/graphics/Paint$Join;

    .prologue
    .line 618
    iput-object p1, p0, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->lineJoin:Landroid/graphics/Paint$Join;

    .line 619
    return-void
.end method

.method public setLineWidth(F)V
    .locals 1
    .param p1, "lineWidth"    # F

    .prologue
    .line 603
    const/4 v0, 0x1

    cmpg-float v0, p1, v0

    if-lez v0, :cond_0

    const v0, 0x7f7fffff    # Float.MAX_VALUE

    cmpl-float v0, p1, v0

    if-gez v0, :cond_0

    const/4 v0, 0x0

    cmpl-float v0, p1, v0

    if-nez v0, :cond_1

    .line 606
    :cond_0
    const v0, 0x3a83126f    # 0.001f

    iput v0, p0, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->lineWidth:F

    .line 611
    :goto_0
    return-void

    .line 608
    :cond_1
    iput p1, p0, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->lineWidth:F

    goto :goto_0
.end method

.method public setMiterLimit(F)V
    .locals 0
    .param p1, "miterLimit"    # F

    .prologue
    .line 660
    iput p1, p0, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->miterLimit:F

    .line 661
    return-void
.end method

.method public setOverprintMode(I)V
    .locals 0
    .param p1, "overprintMode"    # I

    .prologue
    .line 717
    iput p1, p0, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->overprintMode:I

    .line 718
    return-void
.end method

.method public setOverprintOther(Z)V
    .locals 0
    .param p1, "overprintOther"    # Z

    .prologue
    .line 725
    iput-boolean p1, p0, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->overprintOther:Z

    .line 726
    return-void
.end method

.method public setOverprintStroking(Z)V
    .locals 0
    .param p1, "overprintStroking"    # Z

    .prologue
    .line 721
    iput-boolean p1, p0, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->overprintStroking:Z

    .line 722
    return-void
.end method

.method public setShapes(Lorg/icepdf/index/core/pobjects/graphics/Shapes;)V
    .locals 0
    .param p1, "shapes"    # Lorg/icepdf/index/core/pobjects/graphics/Shapes;

    .prologue
    .line 332
    iput-object p1, p0, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->shapes:Lorg/icepdf/index/core/pobjects/graphics/Shapes;

    .line 333
    return-void
.end method

.method public setStrokeAlpha(F)V
    .locals 0
    .param p1, "alpha"    # F

    .prologue
    .line 676
    iput p1, p0, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->strokeAlpha:F

    .line 677
    return-void
.end method

.method public setStrokeColor(I)V
    .locals 0
    .param p1, "strokeColor"    # I

    .prologue
    .line 692
    iput p1, p0, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->strokeColor:I

    .line 693
    return-void
.end method

.method public setTextState(Lorg/icepdf/index/core/pobjects/graphics/TextState;)V
    .locals 0
    .param p1, "textState"    # Lorg/icepdf/index/core/pobjects/graphics/TextState;

    .prologue
    .line 701
    iput-object p1, p0, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->textState:Lorg/icepdf/index/core/pobjects/graphics/TextState;

    .line 702
    return-void
.end method

.method public translate(FF)V
    .locals 3
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    .line 346
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->CTM:Landroid/graphics/Matrix;

    invoke-virtual {v0, p1, p2}, Landroid/graphics/Matrix;->preTranslate(FF)Z

    .line 347
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->shapes:Lorg/icepdf/index/core/pobjects/graphics/Shapes;

    new-instance v1, Landroid/graphics/Matrix;

    iget-object v2, p0, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->CTM:Landroid/graphics/Matrix;

    invoke-direct {v1, v2}, Landroid/graphics/Matrix;-><init>(Landroid/graphics/Matrix;)V

    invoke-virtual {v0, v1}, Lorg/icepdf/index/core/pobjects/graphics/Shapes;->add(Ljava/lang/Object;)V

    .line 348
    return-void
.end method

.method public updateClipCM(Landroid/graphics/Matrix;)V
    .locals 7
    .param p1, "af"    # Landroid/graphics/Matrix;

    .prologue
    .line 521
    iget-object v2, p0, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->clip:Landroid/graphics/Rect;

    if-eqz v2, :cond_0

    .line 523
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    .line 527
    .local v0, "afInverse":Landroid/graphics/Matrix;
    new-instance v1, Landroid/graphics/RectF;

    iget-object v2, p0, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->clip:Landroid/graphics/Rect;

    invoke-direct {v1, v2}, Landroid/graphics/RectF;-><init>(Landroid/graphics/Rect;)V

    .line 528
    .local v1, "rf":Landroid/graphics/RectF;
    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 529
    iget-object v2, p0, Lorg/icepdf/index/core/pobjects/graphics/GraphicsState;->clip:Landroid/graphics/Rect;

    iget v3, v1, Landroid/graphics/RectF;->left:F

    float-to-int v3, v3

    iget v4, v1, Landroid/graphics/RectF;->top:F

    float-to-int v4, v4

    iget v5, v1, Landroid/graphics/RectF;->right:F

    float-to-int v5, v5

    iget v6, v1, Landroid/graphics/RectF;->bottom:F

    float-to-int v6, v6

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/graphics/Rect;->set(IIII)V

    .line 531
    .end local v0    # "afInverse":Landroid/graphics/Matrix;
    .end local v1    # "rf":Landroid/graphics/RectF;
    :cond_0
    return-void
.end method
