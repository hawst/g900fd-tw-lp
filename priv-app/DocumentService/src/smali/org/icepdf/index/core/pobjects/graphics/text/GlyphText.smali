.class public Lorg/icepdf/index/core/pobjects/graphics/text/GlyphText;
.super Lorg/icepdf/index/core/pobjects/graphics/text/AbstractText;
.source "GlyphText.java"


# instance fields
.field private cid:Ljava/lang/String;

.field private unicode:Ljava/lang/String;

.field private x:F

.field private y:F


# direct methods
.method public constructor <init>(FFLorg/icepdf/index/java/awt/geom/Rectangle2D$Float;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "x"    # F
    .param p2, "y"    # F
    .param p3, "bounds"    # Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;
    .param p4, "cid"    # Ljava/lang/String;
    .param p5, "unicode"    # Ljava/lang/String;

    .prologue
    .line 43
    invoke-direct {p0}, Lorg/icepdf/index/core/pobjects/graphics/text/AbstractText;-><init>()V

    .line 44
    iput p1, p0, Lorg/icepdf/index/core/pobjects/graphics/text/GlyphText;->x:F

    .line 45
    iput p2, p0, Lorg/icepdf/index/core/pobjects/graphics/text/GlyphText;->y:F

    .line 46
    iput-object p3, p0, Lorg/icepdf/index/core/pobjects/graphics/text/GlyphText;->bounds:Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;

    .line 47
    iput-object p4, p0, Lorg/icepdf/index/core/pobjects/graphics/text/GlyphText;->cid:Ljava/lang/String;

    .line 48
    iput-object p5, p0, Lorg/icepdf/index/core/pobjects/graphics/text/GlyphText;->unicode:Ljava/lang/String;

    .line 49
    return-void
.end method


# virtual methods
.method public bridge synthetic clearBounds()V
    .locals 0

    .prologue
    .line 29
    invoke-super {p0}, Lorg/icepdf/index/core/pobjects/graphics/text/AbstractText;->clearBounds()V

    return-void
.end method

.method public getBounds()Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/graphics/text/GlyphText;->bounds:Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;

    return-object v0
.end method

.method public getCid()Ljava/lang/String;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/graphics/text/GlyphText;->cid:Ljava/lang/String;

    return-object v0
.end method

.method public getUnicode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lorg/icepdf/index/core/pobjects/graphics/text/GlyphText;->unicode:Ljava/lang/String;

    return-object v0
.end method

.method public getX()F
    .locals 1

    .prologue
    .line 72
    iget v0, p0, Lorg/icepdf/index/core/pobjects/graphics/text/GlyphText;->x:F

    return v0
.end method

.method public getY()F
    .locals 1

    .prologue
    .line 76
    iget v0, p0, Lorg/icepdf/index/core/pobjects/graphics/text/GlyphText;->y:F

    return v0
.end method

.method public normalizeToUserSpace(Lorg/icepdf/index/java/awt/geom/AffineTransform;)V
    .locals 2
    .param p1, "af"    # Lorg/icepdf/index/java/awt/geom/AffineTransform;

    .prologue
    .line 58
    new-instance v0, Lorg/icepdf/index/java/awt/geom/GeneralPath;

    iget-object v1, p0, Lorg/icepdf/index/core/pobjects/graphics/text/GlyphText;->bounds:Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;

    invoke-direct {v0, v1}, Lorg/icepdf/index/java/awt/geom/GeneralPath;-><init>(Lorg/icepdf/index/java/awt/Shape;)V

    .line 59
    .local v0, "generalPath":Lorg/icepdf/index/java/awt/geom/GeneralPath;
    invoke-virtual {v0, p1}, Lorg/icepdf/index/java/awt/geom/GeneralPath;->transform(Lorg/icepdf/index/java/awt/geom/AffineTransform;)V

    .line 60
    invoke-virtual {v0}, Lorg/icepdf/index/java/awt/geom/GeneralPath;->getBounds2D()Lorg/icepdf/index/java/awt/geom/Rectangle2D;

    move-result-object v1

    check-cast v1, Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;

    iput-object v1, p0, Lorg/icepdf/index/core/pobjects/graphics/text/GlyphText;->bounds:Lorg/icepdf/index/java/awt/geom/Rectangle2D$Float;

    .line 61
    return-void
.end method

.method public bridge synthetic setHasSelected(Z)V
    .locals 0
    .param p1, "x0"    # Z

    .prologue
    .line 29
    invoke-super {p0, p1}, Lorg/icepdf/index/core/pobjects/graphics/text/AbstractText;->setHasSelected(Z)V

    return-void
.end method
