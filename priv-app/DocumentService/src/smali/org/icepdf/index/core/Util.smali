.class public Lorg/icepdf/index/core/Util;
.super Ljava/lang/Object;
.source "Util.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static createMatrixFromArray([F)Landroid/graphics/Matrix;
    .locals 6
    .param p0, "m6"    # [F

    .prologue
    .line 36
    const/4 v0, 0x0

    aget v0, p0, v0

    const/4 v1, 0x1

    aget v1, p0, v1

    const/4 v2, 0x2

    aget v2, p0, v2

    const/4 v3, 0x3

    aget v3, p0, v3

    const/4 v4, 0x4

    aget v4, p0, v4

    const/4 v5, 0x5

    aget v5, p0, v5

    invoke-static/range {v0 .. v5}, Lorg/icepdf/index/core/Util;->createMatrixFromValues(FFFFFF)Landroid/graphics/Matrix;

    move-result-object v0

    return-object v0
.end method

.method public static createMatrixFromValues(FFFFFF)Landroid/graphics/Matrix;
    .locals 7
    .param p0, "m00"    # F
    .param p1, "m01"    # F
    .param p2, "m10"    # F
    .param p3, "m11"    # F
    .param p4, "m02"    # F
    .param p5, "m12"    # F

    .prologue
    .line 31
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    .local v0, "result":Landroid/graphics/Matrix;
    move v1, p0

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move v6, p5

    .line 32
    invoke-static/range {v0 .. v6}, Lorg/icepdf/index/core/Util;->setMatFromValues(Landroid/graphics/Matrix;FFFFFF)V

    .line 33
    return-object v0
.end method

.method public static setMatFromArray(Landroid/graphics/Matrix;[F)V
    .locals 7
    .param p0, "mat"    # Landroid/graphics/Matrix;
    .param p1, "m6"    # [F

    .prologue
    .line 27
    const/4 v0, 0x0

    aget v1, p1, v0

    const/4 v0, 0x1

    aget v2, p1, v0

    const/4 v0, 0x2

    aget v3, p1, v0

    const/4 v0, 0x3

    aget v4, p1, v0

    const/4 v0, 0x4

    aget v5, p1, v0

    const/4 v0, 0x5

    aget v6, p1, v0

    move-object v0, p0

    invoke-static/range {v0 .. v6}, Lorg/icepdf/index/core/Util;->setMatFromValues(Landroid/graphics/Matrix;FFFFFF)V

    .line 28
    return-void
.end method

.method public static setMatFromValues(Landroid/graphics/Matrix;FFFFFF)V
    .locals 3
    .param p0, "mat"    # Landroid/graphics/Matrix;
    .param p1, "m00"    # F
    .param p2, "m01"    # F
    .param p3, "m10"    # F
    .param p4, "m11"    # F
    .param p5, "m02"    # F
    .param p6, "m12"    # F

    .prologue
    const/4 v2, 0x0

    .line 22
    const/16 v1, 0x9

    new-array v0, v1, [F

    const/4 v1, 0x0

    aput p1, v0, v1

    const/4 v1, 0x1

    aput p2, v0, v1

    const/4 v1, 0x2

    aput p5, v0, v1

    const/4 v1, 0x3

    aput p3, v0, v1

    const/4 v1, 0x4

    aput p4, v0, v1

    const/4 v1, 0x5

    aput p6, v0, v1

    const/4 v1, 0x6

    aput v2, v0, v1

    const/4 v1, 0x7

    aput v2, v0, v1

    const/16 v1, 0x8

    const/high16 v2, 0x3f800000    # 1.0f

    aput v2, v0, v1

    .line 23
    .local v0, "fs":[F
    invoke-virtual {p0, v0}, Landroid/graphics/Matrix;->setValues([F)V

    .line 24
    return-void
.end method

.method public static transform(Landroid/graphics/Matrix;Landroid/graphics/PointF;Landroid/graphics/PointF;)Landroid/graphics/PointF;
    .locals 4
    .param p0, "mat"    # Landroid/graphics/Matrix;
    .param p1, "src"    # Landroid/graphics/PointF;
    .param p2, "dst"    # Landroid/graphics/PointF;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 40
    const/4 v1, 0x2

    new-array v0, v1, [F

    iget v1, p1, Landroid/graphics/PointF;->x:F

    aput v1, v0, v2

    iget v1, p1, Landroid/graphics/PointF;->y:F

    aput v1, v0, v3

    .line 41
    .local v0, "pts":[F
    invoke-virtual {p0, v0}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 42
    if-nez p2, :cond_0

    .line 43
    new-instance p2, Landroid/graphics/PointF;

    .end local p2    # "dst":Landroid/graphics/PointF;
    aget v1, v0, v2

    aget v2, v0, v3

    invoke-direct {p2, v1, v2}, Landroid/graphics/PointF;-><init>(FF)V

    .line 45
    :goto_0
    return-object p2

    .line 44
    .restart local p2    # "dst":Landroid/graphics/PointF;
    :cond_0
    aget v1, v0, v2

    aget v2, v0, v3

    invoke-virtual {p2, v1, v2}, Landroid/graphics/PointF;->set(FF)V

    goto :goto_0
.end method
